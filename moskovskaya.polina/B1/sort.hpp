#ifndef B1_SORT_HPP
#define B1_SORT_HPP

#include <iostream>
#include <functional>
#include <string.h>

#include "accessTypes.hpp"

template<typename Element>
std::function<bool(Element, Element)> getDirection(const char * direction)
{
  if (strcmp(direction, "ascending") == 0)
  {
    return [](Element x, Element y)
    {
      return x < y;
    };
  }
  if (strcmp(direction, "descending") == 0)
  {
    return [](Element x, Element y)
    {
      return x > y;
    };
  }
  throw std::invalid_argument("Wrong direction");
}


template<template<class> class Access, class Container>
void sort(Container &container, std::function<bool(typename Container::value_type, typename Container::value_type)> cmp)
{
  const auto begin = Access<Container>::begin(container);
  const auto end = Access<Container>::end(container);

  for (auto i = begin; i != end; ++i)
  {
    for (auto j = i; j != end; ++j)
    {
      if (cmp(Access<Container>::get(container, j), Access<Container>::get(container, i)))
      {
        std::swap(Access<Container>::get(container, j), Access<Container>::get(container, i));
      }
    }
  }
}

#endif //B1_SORT_HPP
