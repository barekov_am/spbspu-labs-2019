#include <vector>
#include <forward_list>
#include <exception>

#include "sort.hpp"
#include "print.hpp"

void task1(const char * direction)
{
  if ((strcmp(direction, "ascending") != 0) && (strcmp(direction, "descending") != 0))
  {
    throw std::invalid_argument("No such direction");
  }

  std::vector<int> vector;

  int i = -1;
  while (std::cin && !(std::cin >> i).eof())
  {
    if (std::cin.fail())
    {
      throw std::invalid_argument("Wrong input data");
    }
    vector.push_back(i);
  }

  if (vector.empty())
  {
    return;
  }

  std::vector<int> vectorAt = vector;
  std::forward_list<int> list(vector.begin(), vector.end());

  auto sortDirection = getDirection<int>(direction);
  sort<AccessByBrackets>(vector, sortDirection);
  sort<AccessByAt>(vectorAt, sortDirection);
  sort<AccessByIterator>(list, sortDirection);

  print(vector);
  std::cout << "\n";
  print(vectorAt);
  std::cout << "\n";
  print(list);
}
