
#ifndef B1_ACCESSTYPES_HPP
#define B1_ACCESSTYPES_HPP


template <typename Container>
struct AccessByBrackets
{
  using valueType = typename Container::value_type;

  static unsigned int begin(const Container &)
  {
    return 0;
  }

  static unsigned int end(const Container & container)
  {
    return container.size();
  }

  static valueType & get(Container & container, unsigned int index)
  {
    return container[index];
  }
};

template <typename Container>
struct AccessByAt
{
  using valueType = typename Container::value_type;

  static unsigned int begin(const Container &)
  {
    return 0;
  }

  static unsigned int end(const Container & container)
  {
    return container.size();
  }

  static valueType & get(Container & container, unsigned int index)
  {
    return container.at(index);
  }
};

 template <typename Container>
struct AccessByIterator
{
  using valueType = typename Container::value_type;

  static auto begin(Container & container)
  {
    return container.begin();
  }

  static auto end(Container & container)
  {
    return container.end();
  }

  static valueType & get(Container &, typename Container::iterator iter)
  {
    return *iter;
  }
};


#endif //B1_ACCESSTYPES_HPP
