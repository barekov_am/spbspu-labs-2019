#ifndef B1_PRINT_HPP
#define B1_PRINT_HPP

#include <iostream>
#include "accessTypes.hpp"

template <typename Container>
void print(const Container & container)
{
  for (const auto & element : container)
  {
    std::cout << element << " ";
  }
}

#endif //B1_PRINT_HPP
