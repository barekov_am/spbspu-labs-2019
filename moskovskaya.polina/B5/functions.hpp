#ifndef B5_FUNCTIONS_HPP
#define B5_FUNCTIONS_HPP

#include "shape.hpp"

const int VERTICES_OF_TRIANGLE = 3;
const int VERTICES_OF_RECTANGLE = 4;
const int VERTICES_OF_PENTAGON = 5;

int getLength(const Point_t & lhs, const Point_t & rhs);
bool isSquare(const Shape & shape);
bool isRectangle(const Shape & shape);
void removePentagons(std::vector<Shape> & shapes);

#endif //B5_FUNCTIONS_HPP
