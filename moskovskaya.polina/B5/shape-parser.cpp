#include <string>
#include <iostream>
#include <stdexcept>

#include "shape.hpp"

std::vector<Shape> readShapes(std::istream & istream)
{
  std::vector<Shape> shapes;
  std::string line;

  while (std::getline(istream >> std::ws, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input fail\n");
    }

    auto pos = line.find_first_not_of(" \t");
    if (pos != std::string::npos)
    {
      line.erase(0, pos);
    }
    if (line.empty())
    {
      continue;
    }

    auto openBracket = line.find_first_of('(');
    if (openBracket == std::string::npos)
    {
      throw std::invalid_argument("Invalid separator\n");
    }

    int points = std::stoi(line.substr(0, openBracket));
    line.erase(0, openBracket);
    if (points < 1)
    {
      throw std::invalid_argument("Invalid number of vertices\n");
    }

    Shape shape;
    std::size_t separatorPos;
    std::size_t closeBracket;

    if (line.empty())
    {
      throw std::invalid_argument("No arguments\n");
    }

    for (int i = 0; i < points; i++)
    {
      openBracket = line.find_first_of('(');
      separatorPos = line.find_first_of(';');
      closeBracket = line.find_first_of(')');

      if ((openBracket == std::string::npos) || (separatorPos == std::string::npos)
          || (closeBracket == std::string::npos))
      {
        throw std::invalid_argument("Invalid format of entering vertices\n");
      }

      int x = std::stoi(line.substr(openBracket + 1, separatorPos - openBracket - 1));
      int y = std::stoi(line.substr(separatorPos + 1, closeBracket - separatorPos - 1));
      line.erase(0, closeBracket + 1);
      shape.push_back({x, y});
    }

    line.erase(line.find_last_not_of(" \t") + 1);
    if (!line.empty())
    {
      throw std::invalid_argument("Excess argumets\n");
    }
    shapes.push_back(shape);
  }
  return shapes;
}
