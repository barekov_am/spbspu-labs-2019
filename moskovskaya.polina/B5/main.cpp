#include <iostream>

void task1();
void task2();

int main(int argc, char * argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Wrong number of parameters\n";
      return 1;
    }
    const int task = atoi(argv[1]);
    switch (task)
    {
      case 1:
        task1();
        break;

      case 2:
        task2();
        break;

      default:
        std::cerr << "Wrong task number";
        return 1;
    }
  }
  catch (const std::exception& ex)
  {
    std::cerr << ex.what();
    return 1;
  }
  return 0;
}
