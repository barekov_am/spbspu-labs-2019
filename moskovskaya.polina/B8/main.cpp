#include <iostream>
#include <stdexcept>

#include "output-parser.hpp"
#include "input-handler.hpp"

int main(int argc, char* argv[])
{
  try
  {
    if ((argc != 3) && (argc != 1))
    {
      std::cerr << "Invalid number of arguments.\n";
      return 1;
    }

    size_t width = 40;

    if (argc == 3)
    {
      if (std::string(argv[1]) != "--line-width")
      {
        throw std::invalid_argument("Invalid parameter name\n");
      }
      width = std::stol(argv[2]);

      if (width < 25)
      {
        throw std::invalid_argument("Invalid width\n");
      }
    }

    Handler handler(std::cin);
    handler.parseInput();

    Parser parser(width, handler.getText());
    parser.parseOutput();
  }
  catch (const std::exception & ex)
  {
    std::cerr << ex.what();
    return 1;
  }
}
