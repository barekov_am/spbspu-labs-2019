#include <utility>

#include "output-parser.hpp"

Parser::Parser(unsigned int width, std::list<state_t> text)
{
  lineWidth_ = width;
  text_ = std::move(text);
  begin_ = text_.begin();
  end_ = text_.end();
  currWidth_ = 0;
}

void Parser::lineBreak(std::list<state_t> & line)
{
  currWidth_ = 0;
  std::list<state_t> newLine;
  while (!line.empty())
  {
    newLine.push_front(line.back());
    currWidth_ += line.back().value.size();
    line.pop_back();
    if (newLine.front().type == state_t::WORD || newLine.front().type == state_t::NUM)
    {
      break;
    }
  }
  if (newLine.front().value == " ")
  {
    newLine.front().value.erase(0, 1);
  }
  print(line);
  line.clear();
  line = newLine;
}

void Parser::parseOutput()
{
  std::list<state_t> line;
  for(auto it = begin_; it != end_; it++)
  {
    switch((*it).type)
    {
      case state_t::PUNCT:
        if(currWidth_ + 1 > lineWidth_)
        {
          lineBreak(line);
        }
        line.push_back(*it);
        currWidth_ += (*it).value.size();
        break;
      case state_t::DASH:
        if(currWidth_ + (*it).value.size() + 1 > lineWidth_)
        {
          lineBreak(line);
        }
        line.push_back(state_t{ " ", state_t::SPACE });
        line.push_back(*it);
        currWidth_ += (*it).value.size() + 1;
        break;
      case state_t::WORD:
      case state_t::NUM:
        if (currWidth_ + (*it).value.size() + 1 > lineWidth_)
        {
          print(line);
          line.clear();
          currWidth_ = 0;
        }
        else if (!line.empty())
        {
          line.push_back(state_t{ " ", state_t::SPACE });
          currWidth_++;
        }
        line.push_back(*it);
        currWidth_ += (*it).value.size();
        break;
      case state_t::SPACE:
        break;
    }

  }
  if (!line.empty())
  {
    print(line);
  }
}

void Parser::print(const std::list<state_t> & line)
{
  for(const auto & it : line)
  {
    std::cout << it.value;
  }
  std::cout << "\n";
}

