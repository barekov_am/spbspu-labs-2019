#ifndef B8_INPUT_HANDLER_HPP
#define B8_INPUT_HANDLER_HPP

#include <list>
#include <iostream>
#include "state.hpp"

class Handler
{
public:
  Handler(std::istream & in);
  void parseInput();
  std::list<state_t> getText();
private:
  std::istream & in_;
  std::list<state_t> text_;
  void getNumber();
  void getWord();
  void getDash();
  void getElem();
};

#endif //B8_INPUT_HANDLER_HPP
