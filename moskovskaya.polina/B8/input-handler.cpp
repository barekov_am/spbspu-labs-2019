#include <stdexcept>
#include <locale>
#include "input-handler.hpp"


Handler::Handler(std::istream & in) :
  in_(in)
{}

void Handler::getElem()
{
  char ch = in_.get();
  char next = in_.peek();
  if (std::isalpha(ch, in_.getloc()))
  {
    in_.unget();
    getWord();
  }
  else if (ch == '-')
  {
    if (next == '-')
    {
      in_.unget();
      getDash();
    }
    else if (std::isalpha(next, in_.getloc()))
    {
      throw std::invalid_argument("Word cannot start with '-'\n");
    } else
    {
      in_.unget();
      getNumber();
    }
  }
  else if ((ch == '+') || std::isdigit(ch, in_.getloc()))
  {
    in_.unget();
    getNumber();
  }
  else if (std::ispunct(ch, in_.getloc()))
  {
    if (text_.empty())
    {
      throw std::invalid_argument("Text cannot start with punctuation\n");
    }
    if (text_.back().type == state_t::PUNCT)
    {
      throw std::invalid_argument("Two punctuation marks together\n");
    }
    state_t state{ "", state_t::PUNCT };
    state.value.push_back(ch);
    text_.push_back(state);
  }
}


void Handler::getWord()
{
  char ch = in_.get();
  state_t state{ "", state_t::WORD};
  while (std::isalpha<char>(ch, in_.getloc()) || ch == '-')
  {
    if ((ch == '-') && (in_.peek() == '-'))
    {
      state.value.pop_back();
      break;
    }
    state.value.push_back(ch);
    ch = in_.get();
  }
  in_.unget();
  if (state.value.size() > 20)
  {
    throw std::out_of_range("Too long word\n");
  }
  text_.push_back(state);
}

void Handler::getNumber()
{
  char point = std::use_facet<std::numpunct<char>>(in_.getloc()).decimal_point();
  bool pointRead = false;
  state_t state{ "", state_t::NUM };
  do
  {
    char ch = in_.get();
    if (ch == '-' || ch == '+')
    {
      state.value.push_back(ch);
      continue;
    }
    if (ch == point)
    {
      if (pointRead)
      {
        in_.unget();
        break;
      }
      pointRead = true;
    }
    state.value.push_back(ch);
  } while (std::isdigit<char>(in_.peek(), in_.getloc()) || (in_.peek() == point));
  if (state.value.size() > 20)
  {
    throw std::out_of_range("Too long number\n");
  }
  text_.push_back(state);
}

void Handler::getDash()
{
  if (text_.empty())
  {
    throw std::invalid_argument("Dash in beginning\n");
  }
  if (text_.back().type == state_t::DASH || (text_.back().type == state_t::PUNCT
       && text_.back().value != ","))
  {
    throw std::invalid_argument("Two dashes/punctuation marks together\n");
  }
  char ch = in_.get();
  state_t state{ "", state_t::DASH };
  int counter = 0;
  while (ch == '-')
  {
    counter++;
    state.value.push_back(ch);
    ch = in_.get();
  }
  if (counter != 1 && counter != 3)
  {
    throw std::invalid_argument("Invalid number of dashes\n");
  }
  if (std::ispunct(ch, in_.getloc()))
  {
    throw std::invalid_argument("Two punctuation marks together\n");
  }
  in_.unget();
  text_.push_back(state);
}

void Handler::parseInput()
{
  while (in_)
  {
    getElem();
  }
}

std::list<state_t> Handler::getText()
{
  return text_;
}
