#ifndef B8_STATE_HPP
#define B8_STATE_HPP

#include <string>

struct state_t
{
  enum type_t
  {
    WORD,
    NUM,
    PUNCT,
    DASH,
    SPACE
  };
  std::string value;
  type_t type;
};

#endif //B8_STATE_HPP
