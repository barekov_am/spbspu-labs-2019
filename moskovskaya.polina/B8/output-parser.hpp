#ifndef B8_OUTPUT_PARSER_HPP
#define B8_OUTPUT_PARSER_HPP

#include "state.hpp"
#include <memory>
#include <iostream>
#include <list>

class Parser
{
public:
  Parser(unsigned int, std::list<state_t>);
  void parseOutput();
  void lineBreak(std::list<state_t> &);
  void print(const std::list<state_t> &);
private:
  unsigned int lineWidth_;
  unsigned int currWidth_;
  std::list<state_t> text_;
  std::list<state_t>::iterator begin_;
  std::list<state_t>::iterator end_;
};

#endif //B8_OUTPUT_PARSER_HPP
