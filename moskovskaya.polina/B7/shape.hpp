#ifndef B7_SHAPE_HPP
#define B7_SHAPE_HPP

#include <iostream>

class Shape
{
public:
  Shape(double x, double y);
  virtual ~Shape() = default;

  bool isMoreLeft(const Shape * shape) const;
  bool isUpper(const Shape * shape) const;
  virtual void draw(std::ostream & out) = 0;
protected:
  double x_, y_;
};

#endif //B7_SHAPE_HPP
