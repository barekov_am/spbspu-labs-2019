#include <stdexcept>
#include "shape.hpp"

Shape::Shape(double x, double y) :
  x_(x),
  y_(y)
{}

bool Shape::isMoreLeft(const Shape * shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("No shape to compare\n");
  }

  return x_ < shape->x_;
}

bool Shape::isUpper(const Shape * shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("No shape to compare\n");
  }

  return y_ > shape->y_;
}
