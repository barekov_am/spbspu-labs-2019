#ifndef B7_FIGURES_HPP
#define B7_FIGURES_HPP

#include <iostream>
#include "shape.hpp"

class Circle : public Shape
{
public:
  Circle(double x, double y);
  void draw(std::ostream & out) override;
};

class Triangle : public Shape
{
public:
  Triangle(double x, double y);
  void draw(std::ostream & out) override;
};

class Square : public Shape
{
public:
  Square(double x, double y);
  void draw(std::ostream & out) override;
};

#endif //B7_FIGURES_HPP
