#include <iostream>
#include <string>
#include <memory>
#include <algorithm>

#include "figures.hpp"

std::shared_ptr<Shape> readShape(std::string & line)
{
  if (line.empty())
  {
    throw std::invalid_argument("Empty input\n");
  }

  line.erase(std::remove_if(line.begin(), line.end(), [](const char c) { return std::isspace(c); }), line.end());
  auto openingBracket = line.find_first_of('(');
  std::string name = line.substr(0, openingBracket);
  line.erase(0, openingBracket);

  auto xPos = line.find_first_of(';');
  auto closingBracket = line.find_first_of(')');

  if ((xPos == std::string::npos) || (closingBracket == std::string::npos))
  {
    throw std::invalid_argument("Wrong center declaration\n");
  }

  int x = std::stoi(line.substr(1, xPos - 1));
  int y = std::stoi(line.substr(xPos + 1, closingBracket - xPos - 1));

  if (name == "CIRCLE")
  {
    return std::make_shared<Circle>(Circle(x, y));
  }
  if (name == "TRIANGLE")
  {
    return std::make_shared<Triangle>(Triangle(x, y));
  }
  if (name == "SQUARE")
  {
    return std::make_shared<Square>(Square(x, y));
  }
  throw std::invalid_argument("Wrong shape name\n");
}
