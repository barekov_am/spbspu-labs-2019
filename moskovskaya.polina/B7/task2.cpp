#include <memory>
#include <string>
#include <vector>
#include <algorithm>
#include "shape.hpp"

std::shared_ptr<Shape> readShape(std::string & line);
void printVector(const std::vector<std::shared_ptr<Shape>> & vector)
{
  for (const auto & shape : vector)
  {
    shape->draw(std::cout);
  }
}

void task2()
{
  std::vector<std::shared_ptr<Shape>> shapes;
  std::string line;
  while (getline(std::cin >> std::ws, line))
  {
    std::shared_ptr<Shape> shape = readShape(line);
    shapes.push_back(shape);
  }
  std::cout << "Original:\n";
  printVector(shapes);

  std::cout << "Left-Right:\n";
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape> & lhs, const std::shared_ptr<Shape> & rhs)
  {
    return lhs->isMoreLeft(rhs.get());
  });
  printVector(shapes);

  std::cout << "Right-Left:\n";
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape> & lhs, const std::shared_ptr<Shape> & rhs)
  {
    return !lhs->isMoreLeft(rhs.get());
  });
  printVector(shapes);

  std::cout << "Top-Bottom:\n";
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape> & lhs, const std::shared_ptr<Shape> & rhs)
  {
    return lhs->isUpper(rhs.get());
  });
  printVector(shapes);

  std::cout << "Bottom-Top:\n";
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape> & lhs, const std::shared_ptr<Shape> & rhs)
  {
    return !lhs->isUpper(rhs.get());
  });
  printVector(shapes);
}
