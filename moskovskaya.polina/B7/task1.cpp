#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <cmath>
#include <stdexcept>

void task1()
{
  std::vector<double> numbers((std::istream_iterator<double>(std::cin)), std::istream_iterator<double>());

  if (!std::cin.eof())
  {
    throw std::invalid_argument("Input failed\n");
  }

  std::vector<double> resNums(numbers.size());
  std::transform(numbers.begin(), numbers.end(), resNums.begin(),
       std::bind1st(std::multiplies<double>(), M_PI));

  for (double it : resNums)
  {
    std::cout << it << " ";
  }
  std::cout << '\n';
}
