#include <iostream>
#include <sstream>
#include <algorithm>
#include <map>

#include "commands.hpp"
#include "bookmarks-manager.hpp"
#include "functions.hpp"

void commandAdd(BookmarkManager &manager, std::stringstream &ss)
{
  std::string number;
  ss >> std::ws >> number;
  number = getNumber(number);

  std::string name;
  std::getline(ss >> std::ws, name);
  name = getName(name);

  if (name.empty() || number.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  manager.add({ number, name });
}

void commandStore(BookmarkManager &manager, std::stringstream &ss)
{
  std::string markName;
  ss >> std::ws >> markName;
  markName = getMarkName(markName);

  std::string newMarkName;
  ss >> std::ws >> newMarkName;
  newMarkName = getMarkName(newMarkName);

  if (markName.empty() || newMarkName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  manager.store(markName, newMarkName);
}

void commandInsert(BookmarkManager &manager, std::stringstream &ss)
{
  std::map<const char *, BookmarkManager::InsertDirection> directions =
    {
      {"before", BookmarkManager::InsertDirection::before},
      {"after", BookmarkManager::InsertDirection::after}
    };

  std::string dirData;
  ss >> std::ws >> dirData;

  auto predicate = [&](const std::pair<const char *, BookmarkManager::InsertDirection> &pair) { return pair.first == dirData; };
  auto direction = std::find_if(std::begin(directions), std::end(directions), predicate);

  if (direction == std::end(directions))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  std::string markName;
  ss >> std::ws >> markName;
  markName = getMarkName(markName);

  std::string number;
  ss >> std::ws >> number;
  number = getNumber(number);

  std::string name;
  std::getline(ss >> std::ws, name);
  name = getName(name);

  if (markName.empty() || number.empty() || name.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  manager.insert(direction->second, markName, { number, name });
}

void commandDelete(BookmarkManager &manager, std::stringstream &ss)
{
  std::string markName;
  ss >> std::ws >> markName;
  markName = getMarkName(markName);
  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  manager.remove(markName);
}

void commandShow(BookmarkManager &manager, std::stringstream &ss)
{
  std::string markName;
  ss >> std::ws >> markName;
  markName = getMarkName(markName);
  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  manager.show(markName);
}

void commandMove(BookmarkManager &manager, std::stringstream &ss)
{
  std::map<const char *, BookmarkManager::MoveDirection> directions =
  {
    {"first", BookmarkManager::MoveDirection::first},
    {"last", BookmarkManager::MoveDirection::last}
  };

  std::string markName;
  ss >> std::ws >> markName;
  markName = getMarkName(markName);
  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  std::string steps;
  ss >> std::ws >> steps;

  auto predicate = [&](const std::pair<const char *, BookmarkManager::MoveDirection> &pair) { return pair.first == steps; };
  auto direction = std::find_if(std::begin(directions), std::end(directions), predicate);

  int sign = 1;
  if (direction == std::end(directions))
  {
    if (steps.front() == '-')
    {
      sign = -1;
      steps.erase(steps.begin());
    }
    else if (steps.front() == '+')
    {
      steps.erase(steps.begin());
    }

    steps = getNumber(steps);
    if (steps.empty())
    {
      std::cout << "<INVALID STEP>\n";
      return;
    }
    manager.move(markName, std::stoi(steps) * sign);
  }
  else
  {
    manager.move(markName, direction->second);
  }
}
