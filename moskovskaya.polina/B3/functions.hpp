#ifndef B3_FUNCTIONS_HPP
#define B3_FUNCTIONS_HPP
#include <iostream>

std::string getNumber(std::string &number);
std::string getName(std::string &name);
std::string getMarkName(std::string &name);

#endif //B3_FUNCTIONS_HPP
