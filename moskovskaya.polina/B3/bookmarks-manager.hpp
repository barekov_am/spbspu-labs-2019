#ifndef B3_BOOKMARKS_MANAGER_HPP
#define B3_BOOKMARKS_MANAGER_HPP

#include <map>
#include "phone-book.hpp"

class BookmarkManager
{
public:
  enum class InsertDirection
  {
    before,
    after
  };

  enum class MoveDirection
  {
    first,
    last
  };

  BookmarkManager();
  void add(const PhoneBook::record_t &newRec);
  void store(const std::string &bookmark, const std::string &name);
  void insert(InsertDirection dir, const std::string &bookmark, const PhoneBook::record_t &newRec);
  void remove(const std::string &bookmark);
  void show(const std::string &bookmark);
  void move(const std::string &bookmark, int step);
  void move(const std::string &bookmark, MoveDirection position);

private:
  typedef std::map<std::string, PhoneBook::recIterator> bookmarkType;
  PhoneBook phonebook_;
  bookmarkType bookmarks_;
  bookmarkType::iterator getBookmarkIterator(const std::string &bookmark);
};

#endif //B3_BOOKMARKS_MANAGER_HPP
