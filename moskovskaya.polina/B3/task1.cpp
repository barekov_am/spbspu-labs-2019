#include <iostream>
#include <sstream>
#include <cstring>
#include <functional>
#include <algorithm>

#include "commands.hpp"
#include "bookmarks-manager.hpp"

struct command_t
{
  const char * name;
  std::function<void(BookmarkManager &, std::stringstream &)> execute;
};

void task1()
{
  BookmarkManager manager;
  std::string line;

  while (getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input failed");
    }

    std::stringstream out(line);
    std::string command;
    out >> command;

    static const command_t commands[] =
    {
      {"add", &commandAdd},
      {"store", &commandStore},
      {"insert", &commandInsert},
      {"delete", &commandDelete},
      {"show", &commandShow},
      {"move", &commandMove}
    };
    auto predicate = [&](const command_t &com) { return com.name == command; };
    auto action = std::find_if(std::begin(commands), std::end(commands), predicate);

    if (action != std::end(commands))
    {
      action->execute(manager, out);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
      return;
    }
  }
}
