#include <iostream>
#include "phone-book.hpp"

void PhoneBook::show(const recIterator position) const
{
  std::cout << position->name << " " << position->phone << "\n";
}

PhoneBook::recIterator PhoneBook::next(recIterator position)
{
  return (std::next(position) == records_.end()) ? position : ++position;
}

PhoneBook::recIterator PhoneBook::prev(recIterator position)
{
  return (position == records_.begin()) ? position : --position;
}

PhoneBook::recIterator PhoneBook::insert(const recIterator position, const record_t &newRec)
{
  return records_.insert(position, newRec);
}

PhoneBook::recIterator PhoneBook::move(recIterator position, int steps)
{
  if (steps >= 0)
  {
    while (std::next(position) != records_.end() && steps > 0)
    {
      position = std::next(position);
      --steps;
    }
  }
  else
  {
    while (position != records_.begin() && steps < 0)
    {
      position = std::prev(position);
      ++steps;
    }
  }
  return position;
}

PhoneBook::recIterator PhoneBook::remove(PhoneBook::recIterator position)
{
  return records_.erase(position);
}

PhoneBook::recIterator PhoneBook::begin()
{
  return records_.begin();
}

PhoneBook::recIterator PhoneBook::end()
{
  return records_.end();
}

std::size_t PhoneBook::size() const
{
  return records_.size();
}

bool PhoneBook::empty() const
{
  return records_.empty();
}
