#ifndef B3_PHONE_BOOK_HPP
#define B3_PHONE_BOOK_HPP

#include <string>
#include <list>

class PhoneBook
{
public:
  struct record_t
  {
    std::string name;
    std::string phone;
  };

  using recIterator = std::list<record_t>::iterator;

  void show(recIterator position) const;
  recIterator next(recIterator position);
  recIterator prev(recIterator position);
  recIterator insert(recIterator position, const record_t &newRec);
  recIterator move(recIterator position, int steps);
  recIterator remove(recIterator position);

  recIterator begin();
  recIterator end();

  std::size_t size() const;
  bool empty() const;

private:

  std::list<record_t> records_;
};

#endif //B3_PHONE_BOOK_HPP
