#ifndef B3_COMMANDS_HPP
#define B3_COMMANDS_HPP

#include "bookmarks-manager.hpp"

class BookmarkManager;
void commandAdd(BookmarkManager &manager, std::stringstream &ss);
void commandStore(BookmarkManager &manager, std::stringstream &ss);
void commandInsert(BookmarkManager &manager, std::stringstream &ss);
void commandDelete(BookmarkManager & manager, std::stringstream &ss);
void commandShow(BookmarkManager &manager, std::stringstream &ss);
void commandMove(BookmarkManager &manager, std::stringstream &ss);

#endif //B3_COMMANDS_HPP
