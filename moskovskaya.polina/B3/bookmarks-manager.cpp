#include <algorithm>
#include <iostream>

#include "bookmarks-manager.hpp"


BookmarkManager::BookmarkManager()
{
  bookmarks_["current"] = phonebook_.begin();
}

void BookmarkManager::add(const PhoneBook::record_t &newRec)
{
  phonebook_.insert(phonebook_.end(), newRec);
  if (phonebook_.begin() == std::prev(phonebook_.end()))
  {
    bookmarks_["current"] = phonebook_.begin();
  }
}

void BookmarkManager::store(const std::string &bookmark, const std::string &newMark)
{
  auto it = getBookmarkIterator(bookmark);
  if (it == bookmarks_.end())
  {
    return;
  }
  bookmarks_.emplace(newMark, it->second);
}

void BookmarkManager::insert(BookmarkManager::InsertDirection position, const std::string &bookmark,const PhoneBook::record_t &newRec)
{
  auto it = getBookmarkIterator(bookmark);
  if (it == bookmarks_.end())
  {
    return;
  }
  if (it->second == phonebook_.end())
  {
    add(newRec);
    return;
  }
  auto point = (position == InsertDirection::after) ? std::next(it->second) : it->second;
  phonebook_.insert(point, newRec);
}

void BookmarkManager::remove(const std::string &bookmark)
{
  auto it = getBookmarkIterator(bookmark);
  if (it == bookmarks_.end())
  {
    return;
  }

  auto tmp = it->second;

  std::for_each(bookmarks_.begin(), bookmarks_.end(), [&](auto &it)
  {
    if (it.second == tmp)
    {
      if (std::next(it.second) == phonebook_.end())
      {
        it.second = phonebook_.prev(tmp);
      }
      else
      {
        it.second = phonebook_.next(tmp);
      }
    }
  });

  phonebook_.remove(tmp);
}

void BookmarkManager::show(const std::string &bookmark)
{
  auto it = getBookmarkIterator(bookmark);
  if (it == bookmarks_.end())
  {
    return;
  }

  if (phonebook_.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  phonebook_.show(it->second);
}

void BookmarkManager::move(const std::string &bookmark, int step)
{
  auto it = getBookmarkIterator(bookmark);
  if (it == bookmarks_.end())
  {
    return;
  }

  it->second = phonebook_.move(it->second, step);
}

void BookmarkManager::move(const std::string &bookmark, BookmarkManager::MoveDirection dir)
{
  auto it = getBookmarkIterator(bookmark);
  if (it == bookmarks_.end())
  {
    return;
  }

  it->second = (dir == BookmarkManager::MoveDirection::first) ?
      phonebook_.begin() : std::prev(phonebook_.end());
}

BookmarkManager::bookmarkType::iterator BookmarkManager::getBookmarkIterator(const std::string &bookmark)
{
  auto it = bookmarks_.find(bookmark);
  if (it == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
  }
  return it;
}

