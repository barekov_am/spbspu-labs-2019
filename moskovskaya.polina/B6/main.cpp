#include <iostream>
#include <iterator>
#include <algorithm>

#include "functor.hpp"

int main()
{
  try
  {
    Functor functor;
    functor = std::for_each(std::istream_iterator<int>(std::cin),
        std::istream_iterator<int>(), functor);

    if (!std::cin.eof())
    {
      throw std::ios_base::failure("Input fail\n");
    }
    functor.accumulate();
    functor.printStatistics();
  }
  catch (const std::exception & ex)
  {
    std::cerr << ex.what();
    return 1;
  }
}
