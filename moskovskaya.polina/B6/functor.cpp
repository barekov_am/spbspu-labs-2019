#include "functor.hpp"

#include <limits>
#include <iostream>

Functor::Functor():
  max_(std::numeric_limits<int>::min()),
  min_(std::numeric_limits<int>::max()),
  count_(0),
  positive_(0),
  negative_(0),
  oddSum_(0),
  evenSum_(0),
  mean_(0)
{
}

void Functor::operator()(int number)
{
  numbers_.push_back(number);
}

void Functor::accumulate()
{
  positive_ = 0;
  negative_ = 0;
  oddSum_ = 0;
  evenSum_ = 0;
  count_ = numbers_.size();
  for (std::size_t i = 0; i < numbers_.size(); i++)
  {
    if (numbers_[i] > max_)
    {
      max_ = numbers_[i];
    }
    if (numbers_[i] < min_)
    {
      min_ = numbers_[i];
    }
    if (numbers_[i] % 2 != 0)
    {
      oddSum_ += numbers_[i];
    }
    else
    {
      evenSum_ += numbers_[i];
    }
    if (numbers_[i] > 0)
    {
      positive_++;
    }
    if (numbers_[i] < 0)
    {
      negative_++;
    }
  }
  mean_ = static_cast<double>(evenSum_ + oddSum_) / count_;
}

void Functor::printStatistics()
{
  if (numbers_.empty())
  {
    std::cout << "No Data\n";
    return;
  }
  std::cout << "Max: " << max_ << "\n";
  std::cout << "Min: " << min_ << "\n";
  std::cout << "Mean: " << mean_ << "\n";
  std::cout << "Positive: " << positive_ << "\n";
  std::cout << "Negative: " << negative_ << "\n";
  std::cout << "Odd Sum: " << oddSum_ << "\n";
  std::cout << "Even Sum: " << evenSum_ << "\n";
  std::cout << "First/Last Equal: " << (*numbers_.begin() == *(numbers_.end() - 1) ? "yes" : "no") << "\n";
}
