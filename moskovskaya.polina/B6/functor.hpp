#ifndef B6_FUNCTOR_HPP
#define B6_FUNCTOR_HPP

#include <vector>
class Functor
{
public:
  Functor();
  void operator()(int num);
  void accumulate();
  void printStatistics();

private:
  std::vector<int> numbers_;
  int max_;
  int min_;
  int count_;
  int positive_;
  int negative_;
  long long int oddSum_;
  long long int evenSum_;
  double mean_;
};

#endif //B6_FUNCTOR_HPP
