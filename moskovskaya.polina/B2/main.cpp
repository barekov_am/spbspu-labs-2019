#include "iostream"

void task1();
void task2();

int main(int argc, char* argv[])
{
  if (argc != 2)
  {
    std::cerr << "Incorrect number of arguments";
    return 1;
  }

  try
  {
    switch (atoi(argv[1]))
    {
      case 1:
      {
        task1();
        break;
      }
      case 2:
      {
        task2();
        break;
      }
      default:
        std::cerr << "Incorrect task number";
        return 1;
    }
  }
  catch (std::exception& e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}
