#include <list>
#include <iostream>
#include <stdexcept>


void print(std::list<int>::iterator begin, std::list<int>::iterator end)
{
  if (begin == end)
  {
    std::cout << "\n";
    return;
  }
  if (begin == std::prev(end))
  {
    std::cout << *begin << "\n";
    return;
  }

  std::cout << *begin << " " << *(std::prev(end)) << " ";
  ++begin;
  --end;
  print(begin, end);
}

void task2()
{
  const int max = 20;
  const int min = 1;
  const size_t maxSize = 20;

  std::list<int> list;
  std::size_t currSize = 0;
  int num = 0;

  while (std::cin && !(std::cin >> num).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Fail reading");
    }
    if ((num > max) || (num < min))
    {
      throw std::invalid_argument("Invalid number");
    }
    ++currSize;
    if (currSize > maxSize)
    {
      throw std::invalid_argument("Invalid amount of elements");
    }

    list.push_back(num);
  }

  print(list.begin(), list.end());
};
