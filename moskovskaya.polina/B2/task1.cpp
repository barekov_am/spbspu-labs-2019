#include <algorithm>
#include <sstream>
#include <map>
#include <iostream>

#include "queue.hpp"

template<typename Char, typename CharTraits>
std::basic_istream<Char, CharTraits>& blank(std::basic_istream<Char, CharTraits>& is);
void commandAdd(QueueWithPriority<std::string>&, std::istream&);
void commandGet(QueueWithPriority<std::string>&, std::istream&);
void commandAccelerate(QueueWithPriority<std::string>&, std::istream&);

void task1()
{
  using executeCommand = std::function<void (QueueWithPriority<std::string>&, std::istream&)>;

  QueueWithPriority<std::string> queue;

  std::map<const std::string, executeCommand> commands =
  {
    {"add", commandAdd},
    {"get", commandGet},
    {"accelerate", commandAccelerate}
  };

  std::string input;
  while (getline(std::cin, input))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading failed");
    }

    std::stringstream inStream(input);
    inStream >> std::noskipws;

    std::string command;
    inStream >> blank >> command;

    auto findCommand = std::find_if(std::begin(commands), std::end(commands),
        [&](const std::pair<const std::string, executeCommand>& pair) { return (pair.first == command); }
    );
    if (findCommand != std::end(commands))
    {
      findCommand->second(queue, inStream >> blank);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}

