#include <iostream>
#include <sstream>
#include <map>
#include <algorithm>
#include <locale>

#include "queue-implementation.hpp"

template<typename Char, typename CharTraits>
std::basic_istream<Char, CharTraits>& blank(std::basic_istream<Char, CharTraits>& is)
{
  while (std::isblank(static_cast<char>(is.peek()), std::locale("")))
  {
    is.ignore();
  }
  return is;
}

void commandAdd(QueueWithPriority<std::string>& queue, std::istream& in)
{
  using priorityType = QueueWithPriority<std::string>::ElementPriority;

  std::map<const std::string, priorityType> priorities =
  {
    {"low", priorityType::LOW},
    {"normal", priorityType::NORMAL},
    {"high", priorityType::HIGH}
  };


  std::string priority;
  in >> priority;

  auto findPriority = std::find_if(std::begin(priorities), std::end(priorities),
      [&](const std::pair<const std::string, priorityType>& pair){ return (pair.first == priority);});

  if (findPriority == std::end(priorities))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  std::string data;
  getline(in >> blank, data);
  if (data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.put(data, findPriority->second);
}

void commandGet(QueueWithPriority<std::string>& queue, std::istream& in)
{
  std::string data;
  std::getline(in >> blank, data);

  if (!data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (queue.isEmpty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  std::string element = queue.get();
  std::cout << element << "\n";
}

void commandAccelerate(QueueWithPriority<std::string>& queue, std::istream& in)
{
  if (!in.eof())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.accelerate();
}

