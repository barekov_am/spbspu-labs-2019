#ifndef B2_QUEUE_IMPLEMENTATION_HPP
#define B2_QUEUE_IMPLEMENTATION_HPP

#include "queue.hpp"

template <typename T>
void QueueWithPriority<T>::put(const T& element, ElementPriority priority)
{
  switch(priority)
  {
    case ElementPriority::LOW:
    {
      low_.push_back(element);
      break;
    }
    case ElementPriority::NORMAL:
    {
      normal_.push_back(element);
      break;
    }
    case ElementPriority::HIGH:
    {
      high_.push_back(element);
      break;
    }
  }
}

template <typename T>
T QueueWithPriority<T>::get()
{
  if (!high_.empty())
  {
    T elem = high_.front();
    high_.pop_front();
    return elem;
  }
  else if (!normal_.empty())
  {
    T elem = normal_.front();
    normal_.pop_front();
    return elem;
  }
  else  if (!low_.empty())
  {
    T elem = low_.front();
    low_.pop_front();
    return elem;
  }
  else
  {
    throw std::invalid_argument("Queue is empty");
  }
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  if (isEmpty())
  {
    throw std::invalid_argument("Queue is empty");
  }
  high_.splice(high_.end(), low_);
}

template <typename T>
bool QueueWithPriority<T>::isEmpty() const
{
  return (low_.empty() && normal_.empty() && high_.empty());
}

#endif //B2_QUEUE_IMPLEMENTATION_HPP
