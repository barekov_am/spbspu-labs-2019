#ifndef B2_QUEUE_HPP
#define B2_QUEUE_HPP

#include <list>
#include <stdexcept>
#include <functional>


template <typename T>
class QueueWithPriority
{
public:
  enum ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };
  QueueWithPriority();
  void put(const T& element, ElementPriority priority);
  T get();
  void accelerate();
  bool isEmpty() const;

private:
  std::list<T> low_;
  std::list<T> normal_;
  std::list<T> high_;
};


template <typename T>
QueueWithPriority<T>::QueueWithPriority() :
  low_(),
  normal_(),
  high_()
{}

#endif //B2_QUEUE_HPP
