#ifndef B4_DATA_STRUCT_HPP
#define B4_DATA_STRUCT_HPP

#include <iostream>
#include <vector>
struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

std::vector<DataStruct> readData(std::istream& istream);
void printData(const std::vector<DataStruct> &vector);


#endif //B4_DATA_STRUCT_HPP
