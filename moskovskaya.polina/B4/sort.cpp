#include "data-struct.hpp"
#include <algorithm>

void sort(std::vector<DataStruct> &vector)
{
  std::sort(vector.begin(), vector.end(), [&](DataStruct rhs, DataStruct lhs)
  {
    if (rhs.key1 != lhs.key1)
    {
      return rhs.key1 < lhs.key1;
    }
    else if (rhs.key2 != lhs.key2)
    {
      return rhs.key2 < lhs.key2;
    }
    else
    {
      return rhs.str.length() < lhs.str.length();
    }
  });
}
