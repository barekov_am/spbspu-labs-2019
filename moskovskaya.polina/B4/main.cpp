#include <iostream>
#include "data-struct.hpp"

void sort(std::vector<DataStruct> &vector);

int main()
{
  try
  {
    std::vector<DataStruct> vector = readData(std::cin);
    sort(vector);
    printData(vector);
  }
  catch (const std::exception& ex)
  {
    std::cerr << ex.what();
    return 1;
  }
  return 0;
}
