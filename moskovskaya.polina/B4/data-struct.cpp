#include <sstream>
#include <stdexcept>
#include "data-struct.hpp"

const int MAX_KEY = 5;
const int MIN_KEY = -5;

std::vector<DataStruct> readData(std::istream& istream)
{
  std::vector<DataStruct> vector;
  std::string line;

  while (getline(istream >> std::ws, line))
  {
    if (line.empty())
    {
      throw std::runtime_error("Reading failed\n");
    }
    auto pos = line.find_first_of(',');
    if (pos == std::string::npos || (!pos))
    {
      throw std::invalid_argument("Invalid separator\n");
    }

    int key1 = std::stoi(line.substr(0, pos));
    line.erase(0, pos + 1);
    pos = line.find_first_of(',');
    if (pos == std::string::npos || (!pos))
    {
      throw std::invalid_argument("Invalid separator\n");
    }

    int key2 = std::stoi(line.substr(0, pos));
    line.erase(0, pos + 1);

    auto firstNotWS = line.find_first_not_of(" \t");
    line.erase(0, firstNotWS);

    if (key1 < MIN_KEY || key1 > MAX_KEY || key2 < MIN_KEY || key2 > MAX_KEY)
    {
      throw std::out_of_range("Key value is not in range");
    }
    vector.push_back({key1, key2, line});
  }
  return vector;
}

void printData(const std::vector<DataStruct> &vector)
{
  for (const auto& el : vector)
  {
    std::cout << el.key1 << "," << el.key2 << "," << el.str << '\n';
  }
}
