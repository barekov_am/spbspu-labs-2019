#ifndef A4_MATRIX_HPP
#define A4_MATRIX_HPP

#include <memory>
#include <algorithm>
#include <stdexcept>

namespace moskovskaya
{
  template<typename T>
  class Matrix
  {
  public:
    using ptr = std::shared_ptr<T>;
    using ptrArr = std::unique_ptr<ptr[]>;

    class Layer
    {
    public:
      Layer(const Layer &rhs);
      Layer(Layer &&rhs);
      Layer(ptr *array, size_t size);
      ~Layer() = default;

      Layer &operator =(const Layer &rhs);
      Layer &operator =(Layer &&rhs);
      ptr operator [](size_t index) const;

      size_t getSize() const;

    private:
      size_t size_;
      ptrArr data_;
    };

    Matrix();
    Matrix(const Matrix &rhs);
    Matrix(Matrix &&rhs);
    ~Matrix() = default;

    Matrix &operator =(const Matrix &rhs);
    Matrix &operator =(Matrix &&rhs);
    Layer operator [](size_t index) const;
    bool operator ==(const Matrix &rhs) const;
    bool operator !=(const Matrix &rhs) const;

    void add(size_t row, ptr shape);
    size_t getRowsCount() const;
    size_t getMaxColSize() const;

  private:
    size_t rows_;
    size_t objCount_;
    std::unique_ptr<size_t[]> layersSizes_;
    ptrArr data_;
  };


  template<typename T>
  Matrix<T>::Matrix() :
    rows_(0),
    objCount_(0)
  {
  }

  template<typename T>
  Matrix<T>::Matrix(const Matrix<T> &rhs) :
    rows_(rhs.rows_),
    objCount_(rhs.objCount_),
    layersSizes_(std::make_unique<size_t[]>(rhs.rows_)),
    data_(std::make_unique<ptr[]>(rhs.objCount_))
  {
    for (size_t i = 0; i < rows_; ++i)
    {
      layersSizes_[i] = rhs.layersSizes_[i];
    }

    for (size_t i = 0; i < objCount_; ++i)
    {
      data_[i] = rhs.data_[i];
    }
  }

  template<typename T>
  Matrix<T>::Matrix(Matrix<T> &&rhs) :
    rows_(rhs.rows_),
    objCount_(rhs.objCount_),
    layersSizes_(std::move(rhs.layersSizes_)),
    data_(std::move(rhs.data_))
  {
    rhs.rows_ = 0;
    rhs.objCount_ = 0;
  }

  template<typename T>
  Matrix<T> &Matrix<T>::operator =(const Matrix<T> &rhs)
  {
    if (this != &rhs)
    {
      data_ = std::make_unique<ptr[]>(rhs.objCount_);
      layersSizes_ = std::make_unique<size_t[]>(rhs.rows_);
      objCount_ = rhs.objCount_;
      rows_ = rhs.rows_;
      for (size_t i = 0; i < objCount_; i++)
      {
        data_[i] = rhs.data_[i];
      }

      for (size_t i = 0; i < rows_; i++)
      {
        layersSizes_[i] = rhs.layersSizes_[i];
      }
    }

    return *this;
  }

  template<typename T>
  Matrix<T> &Matrix<T>::operator =(Matrix<T> &&rhs)
  {
    if (this != &rhs)
    {
      rows_ = rhs.rows_;
      rhs.rows_ = 0;

      objCount_ = rhs.objCount_;
      rhs.objCount_ = 0;

      layersSizes_ = std::move(rhs.layersSizes_);

      data_ = std::move(rhs.data_);
    }

    return *this;
  }

  template<typename T>
  typename Matrix<T>::Layer Matrix<T>::operator [](size_t index) const
  {
    if (index >= rows_)
    {
      throw std::out_of_range("Index is out of range");
    }

    size_t row = 0;
    for (size_t i = 0; i < index; ++i)
    {
      row += layersSizes_[i];
    }

    return Layer(&data_[row], layersSizes_[index]);
  }

  template<typename T>
  bool Matrix<T>::operator ==(const Matrix<T> &rhs) const
  {
    if ((rows_ != rhs.rows_) || (objCount_ != rhs.objCount_))
    {
      return false;
    }

    for (size_t i = 0; i < objCount_; ++i)
    {
      if (data_[i] != rhs.data_[i])
      {
        return false;
      }
    }

    for (size_t i = 0; i < rows_; ++i)
    {
      if (layersSizes_[i] != rhs.layersSizes_[i])
      {
        return false;
      }
    }

    return true;
  }

  template<typename T>
  bool Matrix<T>::operator !=(const Matrix<T> &rhs) const
  {
    return !(*this == rhs);
  }

  template<typename T>
  void Matrix<T>::add(size_t row, ptr shape)
  {
    if (shape == nullptr)
    {
      throw std::invalid_argument("Object is nullptr");
    }

    if (row > rows_)
    {
      throw std::out_of_range("Row is out of range");
    }

    if (row == rows_)
    {
      std::unique_ptr<size_t[]> tmpLayersSizes = std::make_unique<size_t[]>(rows_ + 1);
      ptrArr tmpData = std::make_unique<ptr[]>(objCount_ + 1);

      for (size_t i = 0; i < rows_; ++i)
      {
        tmpLayersSizes[i] = layersSizes_[i];
      }
      tmpLayersSizes[rows_] = 1;

      for (size_t i = 0; i < objCount_; ++i)
      {
        tmpData[i] = data_[i];
      }
      tmpData[objCount_] = shape;

      layersSizes_ = std::move(tmpLayersSizes);
      data_ = std::move(tmpData);
      objCount_++;
      rows_++;
    }
    else
    {
      ptrArr tmpData = std::make_unique<ptr[]>(objCount_ + 1);

      size_t tmpIndex = 0;
      size_t dataIndex = 0;
      for (size_t i = 0; i < rows_; i++)
      {
        for (size_t j = 0; j < layersSizes_[i]; j++)
        {
          tmpData[tmpIndex++] = data_[dataIndex++];
        }
        if (row == i)
        {
          tmpData[tmpIndex++] = shape;
        }
      }
      data_ = std::move(tmpData);
      layersSizes_[row]++;
      objCount_++;
    }
  }

  template<typename T>
  size_t Matrix<T>::getRowsCount() const
  {
    return rows_;
  }

  template<typename T>
  size_t Matrix<T>::getMaxColSize() const
  {
    if (rows_ == 0)
    {
      return 0;
    }
    size_t res = 0;
    const size_t size = sizeof(layersSizes_) / sizeof(layersSizes_[0]);
    for (size_t i = 0; i < size; i++)
    {
      if (layersSizes_[i] > res)
      {
        res = layersSizes_[i];
      }
    }
    return res;
  }

  template<typename T>
  Matrix<T>::Layer::Layer(ptr *array, size_t size) :
    size_(size),
    data_(std::make_unique<ptr[]>(size))
  {
    for (size_t i = 0; i < size_; ++i)
    {
      data_[i] = array[i];
    }
  }

  template<typename T>
  Matrix<T>::Layer::Layer(const Layer &rhs) :
    data_(std::make_unique<ptr[]>(rhs.size_)),
    size_(rhs.size_)
  {
    for (size_t i = 0; i < size_; ++i)
    {
      data_[i] = rhs.data_[i];
    }
  }

  template<typename T>
  Matrix<T>::Layer::Layer(Layer &&rhs) :
    size_(rhs.size_),
    data_(std::move(rhs.data_))
  {
    rhs.size_ = 0;
  }

  template<typename T>
  typename Matrix<T>::Layer &Matrix<T>::Layer::operator =(const Layer &rhs)
  {
    if (this != &rhs)
    {
      data_ = std::make_unique<ptr[]>(rhs.size_);
      for (size_t i = 0; i < rhs.size_; ++i)
      {
        data_[i] = rhs.data_[i];
      }

      size_ = rhs.size_;
    }
    return *this;
  }

  template<typename T>
  typename Matrix<T>::Layer &Matrix<T>::Layer::operator =(Layer &&rhs)
  {
    if (this != &rhs)
    {
      size_ = rhs.size_;
      rhs.size_ = 0;

      data_ = std::move(rhs.size_);
    }
    return *this;
  }

  template<typename T>
  typename Matrix<T>::ptr Matrix<T>::Layer::operator [](size_t index) const
  {
    if (index >= size_)
    {
      throw std::out_of_range("Column index out of range.");
    }
    return data_[index];
  }

  template<typename T>
  size_t Matrix<T>::Layer::getSize() const
  {
    return size_;
  }
}

#endif //A4_MATRIX_HPP
