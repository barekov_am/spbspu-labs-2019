#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

moskovskaya::Rectangle::Rectangle(const moskovskaya::point_t &center, double width, double height) :
  center_(center),
  width_(width),
  height_(height),
  angle_(0)
{
  if (width_ <= 0)
  {
    throw std::invalid_argument("Negative width");
  }
  if (height_ <= 0)
  {
    throw std::invalid_argument("Negative height");
  }
}

moskovskaya::Rectangle::Rectangle(const moskovskaya::point_t &center, double width, double height, double angle):
Rectangle(center, width, height)
{
  rotate(angle);
}

double moskovskaya::Rectangle::getArea() const
{
  return width_ * height_;
}

moskovskaya::rectangle_t moskovskaya::Rectangle::getFrameRect() const
{
  const double sin = std::sin(angle_ * M_PI / 180);
  const double cos = std::cos(angle_ * M_PI / 180);
  const double height = width_ * std::abs(sin) + height_ * std::abs(cos);
  const double width = width_ * std::abs(cos) + height_ * std::abs(sin);
  return {width, height, center_};
}

void moskovskaya::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void moskovskaya::Rectangle::move(const point_t &newCenter)
{
  center_ = newCenter;
}

void moskovskaya::Rectangle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Negative factor");
  }
  width_ *= factor;
  height_ *= factor;
}

void moskovskaya::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
