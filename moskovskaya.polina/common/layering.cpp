#include "layering.hpp"
#include <cmath>

moskovskaya::Matrix<moskovskaya::Shape> moskovskaya::split(const moskovskaya::CompositeShape &compShape)
{
  moskovskaya::Matrix<moskovskaya::Shape> matrix;

  for (size_t i = 0; i < compShape.size(); i++)
  {
    size_t row = 0;
    size_t j = matrix.getRowsCount();
    while (j-- > 0)
    {
      bool ifIntersect = false;
      for (size_t k = 0; k < matrix[j].getSize(); k++)
      {
        if (intersect(*compShape[i], *matrix[j][k]))
        {
          row = j + 1;
          ifIntersect = true;
          break;
        }
      }
      if (ifIntersect)
      {
        break;
      }
    }
    matrix.add(row, compShape[i]);
  }
  return matrix;
}

bool moskovskaya::intersect(const moskovskaya::Shape &shape1, const moskovskaya::Shape &shape2)
{
  const moskovskaya::rectangle_t frame1 = shape1.getFrameRect();
  const moskovskaya::rectangle_t frame2 = shape2.getFrameRect();

  if (std::abs(frame1.pos.y - frame2.pos.y) > (frame1.height + frame2.height) / 2)
  {
    return false;
  }

  return (std::abs(frame1.pos.x - frame2.pos.x) <= (frame1.width + frame2.width) / 2);
}
