#ifndef A4_LAYERING_HPP
#define A4_LAYERING_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace moskovskaya
{
  Matrix<Shape> split(const CompositeShape &compShape);
  bool intersect(const moskovskaya::Shape &shape1, const moskovskaya::Shape &shape2);
}

#endif //A4_LAYERING_HPP
