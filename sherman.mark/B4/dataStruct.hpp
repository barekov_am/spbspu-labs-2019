#ifndef B4_DATASTRUCT_HPP
#define B4_DATASTRUCT_HPP

#include <string>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

#endif
