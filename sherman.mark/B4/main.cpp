#include <algorithm>
#include <iostream>
#include "dataStruct.hpp"
#include "functions.hpp"

int main()
{
  try
  {
    std::vector<DataStruct> ourVector = getVector();
    if (ourVector.size() > 1)
    {
      std::sort(ourVector.begin(), ourVector.end(), compare);
    }
    if (!ourVector.empty())
    {
      for (const auto &vector: ourVector)
      {
        std::cout << vector.key1 << ',' << vector.key2 << ',' << vector.str << '\n';
      }
    }
  }
  catch (const std::exception &err)
  {
    std::cerr << err.what();
    return 1;
  }
  return 0;
}
