#include <iostream>
#include <sstream>
#include "functions.hpp"

std::vector<DataStruct> getVector()
{
  std::vector<DataStruct> dataVector;

  int key[2] = {0};
  std::string str;

  std::string data;
  std::string line;
  while (std::getline(std::cin >> std::ws, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading caused an error");
    }

    for (int i = 0; i < 2; ++i)
    {
      std::size_t comma = line.find_first_of(',');

      if (comma == std::string::npos)
      {
        throw std::invalid_argument("Invalid input data");
      }

      key[i] = std::stoi(line.substr(0, comma));

      if (std::abs(key[i]) > 5)
      {
        throw std::invalid_argument("Invalid input data");
      }
      line.erase(0, comma + 1);
    }

    if (line.empty())
    {
      throw std::invalid_argument("Invalid input data");
    }

    dataVector.push_back({key[0], key[1], line});

  }
  return dataVector;
}

bool compare(const DataStruct &left, const DataStruct &right)
{
  if (left.key1 < right.key1)
  {
    return true;
  }
  else if (left.key1 == right.key1)
  {
    if (left.key2 < right.key2)
    {
      return true;
    }
    else if (left.key2 == right.key2)
    {
      if (left.str.size() < right.str.size())
      {
        return true;
      }
    }
  }
  return false;
}
