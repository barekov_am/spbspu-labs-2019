#ifndef B4_FUNCTIONS_HPP
#define B4_FUNCTIONS_HPP

#include "dataStruct.hpp"
#include <vector>

std::vector<DataStruct> getVector();
bool compare(const DataStruct &, const DataStruct &);

#endif
