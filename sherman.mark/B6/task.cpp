#include <algorithm>
#include <iterator>
#include <iostream>
#include "functor.hpp"

void task()
{
  Functor functor;
  functor = std::for_each(std::istream_iterator<long long int>(std::cin), std::istream_iterator<long long int>(), functor);
  if (!std::cin.eof())
  {
    throw std::ios_base::failure("Reading caused an error");
  }
  if (functor.isEmpty())
  {
    std::cout << "No Data\n";
  }
  else
  {
    std::cout << "Max: " << functor.getMax() << '\n';
    std::cout << "Min: " << functor.getMin() << '\n';
    std::cout << "Mean: " << functor.getAverageValue() << '\n';
    std::cout << "Positive: " << functor.getAmountOfPositive() << '\n';
    std::cout << "Negative: " << functor.getAmountOfNegative() << '\n';
    std::cout << "Odd Sum: " << functor.getSumOfOddNumbers() << '\n';
    std::cout << "Even Sum: " << functor.getSumOfEvenNumbers() << '\n';
    std::cout << "First/Last Equal: ";
    if (functor.isEqualBorderElements())
    {
      std::cout << "yes\n";
    }
    else
    {
      std::cout << "no\n";
    }
  }
}
