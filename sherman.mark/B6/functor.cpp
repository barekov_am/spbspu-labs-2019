#include <limits>
#include "functor.hpp"

Functor::Functor() :
  max_(std::numeric_limits<long long int>::min()),
  min_(std::numeric_limits<long long int>::max()),
  amount_(0),
  positives_(0),
  negatives_(0),
  first_(0),
  oddSum_(0),
  evenSum_(0),
  equalBorderElements_(false)
{
}

void Functor::operator()(long long int number)
{
  if (amount_ == 0)
  {
    first_ = number;
  }

  ++amount_;

  if (number > max_)
  {
    max_ = number;
  }

  if (number < min_)
  {
    min_ = number;
  }

  if (number > 0)
  {
    ++positives_;
  }

  if(number < 0)
  {
    ++negatives_;
  }

  if (number % 2)
  {
    oddSum_ += number;
  }
  else
  {
    evenSum_ += number;
  }

  equalBorderElements_ = first_ == number;
}

long long int Functor::getMax()
{
  return max_;
}

long long int Functor::getMin()
{
  return min_;
}

double Functor::getAverageValue()
{
  return static_cast<double>(oddSum_ + evenSum_) / amount_;
}

long long int Functor::getAmountOfPositive()
{
  return positives_;
}

long long int Functor::getAmountOfNegative()
{
  return negatives_;
}

long long int Functor::getSumOfOddNumbers()
{
  return oddSum_;
}

long long int Functor::getSumOfEvenNumbers()
{
  return evenSum_;
}

bool Functor::isEqualBorderElements()
{
  return equalBorderElements_;
}

bool Functor::isEmpty()
{
  return amount_ == 0;
}
