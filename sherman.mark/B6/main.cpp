#include <iostream>

void task();

int main()
{
  try
  {
    task();
  }
  catch (const std::exception &err)
  {
    std::cerr << err.what();
    return 1;
  }
  return 0;
}
