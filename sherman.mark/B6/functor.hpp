#ifndef B6_FUNCTOR_HPP
#define B6_FUNCTOR_HPP

class Functor
{
  public:
    Functor();
    void operator()(long long int);
    long long int getMax();
    long long int getMin();
    double getAverageValue();
    long long int getAmountOfPositive();
    long long int getAmountOfNegative();
    long long int getSumOfOddNumbers();
    long long int getSumOfEvenNumbers();
    bool isEqualBorderElements();
    bool isEmpty();

  private:
    long long int max_;
    long long int min_;
    long long int amount_;
    long long int positives_;
    long long int negatives_;
    long long int first_;
    long long int oddSum_;
    long long int evenSum_;
    bool equalBorderElements_;

};

#endif
