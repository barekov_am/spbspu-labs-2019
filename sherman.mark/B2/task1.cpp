#include "tasks.hpp"
#include "queue-implementation.hpp"
#include <string>
#include <sstream>
#include <algorithm>

void task1()
{
  QueueWithPriority<std::string> queue;
  std::string line;
  std::string command;
  std::string commandPriority;
  std::string data;
  std::stringstream string;
  while (std::getline(std::cin >> std::ws, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading caused an error");
    }

    string.clear();
    string.str(line);
    string >> std::ws >> command;

    if (command == "add")
    {
      string >> std::ws >> commandPriority;
      transform(commandPriority.begin(), commandPriority.end(), commandPriority.begin(), tolower);
      std::getline(string >> std::ws, data);

      if (data.empty())
      {
        std::cout << "<INVALID COMMAND>\n";
      }

      if (commandPriority == "high")
      {
        queue.pushBackElement(data, ElementPriority::HIGH);
      }
      else if (commandPriority == "normal")
      {
        queue.pushBackElement(data, ElementPriority::NORMAL);
      }
      else if (commandPriority == "low")
      {
        queue.pushBackElement(data, ElementPriority::LOW);
      }
      else
      {
        std::cout << "<INVALID COMMAND>\n";
      }
    }
    else if (command == "get")
    {
      if(queue.empty())
      {
        std::cout << "<EMPTY>\n";
      }
      else
      {
        std::cout << queue.getElement() << '\n';
      }
    }
    else if (command == "accelerate")
    {
      if (queue.empty())
      {
        std::cout << "<EMPTY>\n";
      }
      else
      {
        queue.accelerate();
      }
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
