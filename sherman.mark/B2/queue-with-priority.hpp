#ifndef B2_QUEUE_WITH_PRIORITY_HPP
#define B2_QUEUE_WITH_PRIORITY_HPP

#include <list>
#include <stdexcept>

typedef enum
{
  LOW,
  NORMAL,
  HIGH
} ElementPriority;

template<typename T>
class QueueWithPriority
{
public:
  QueueWithPriority() = default;
  ~QueueWithPriority() = default;

  void pushBackElement(const T &element, ElementPriority priority);
  T getElement();
  void accelerate();
  bool empty() const;
private:
  std::list<T> high_;
  std::list<T> normal_;
  std::list<T> low_;
};

#endif
