#include "tasks.hpp"
#include <list>

const int MAX_VALUE = 20;
const int MIN_VALUE = 1;

void printList(std::list<int> &list)
{
  if (list.empty())
  {
    return;
  }

  std::list<int>::const_iterator left = list.cbegin();
  std::list<int>::const_iterator right = list.cend();
  --right;

   while (left != right)
   {
     std::cout << *left << " " << *right;
     ++left;
     if (left == right)
     {
       break;
     }
     else
     {
       std::cout << " ";
     }
     --right;
   }

   if (list.size() % 2 == 1)
   {
     std::cout << *left;
   }

  std::cout << '\n';
}

void task2()
{
  std::list<int> list;
  int currentElement = 0;
  while (std::cin && !(std::cin >> currentElement).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading the file caused an error");
    }
    if ((currentElement < MIN_VALUE) || (currentElement > MAX_VALUE))
    {
      throw std::invalid_argument("Input data must be between 1 and 20");
    }

    list.push_back(currentElement);

    if (list.size() > MAX_VALUE)
    {
      throw std::invalid_argument("Exceeded the size of the list");
    }
  }

  printList(list);
}
