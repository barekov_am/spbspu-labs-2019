#ifndef COMMON_COMPOSITESHAPE_HPP
#define COMMON_COMPOSITESHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace sherman
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&) noexcept;
    CompositeShape(const shape_ptr &);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &);
    CompositeShape &operator =(CompositeShape &&) noexcept;
    shape_ptr operator [](std::size_t) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    std::size_t size() const;
    void move(double, double) override;
    void move(const point_t &) override;
    void scale(double) override;
    void rotate(double) override;
    void add(const shape_ptr &);
    void remove(std::size_t);
    void printInformation() const override;
  private:
    std::size_t counter_;
    array_ptr arrayOfShapes_;
  };
}

#endif
