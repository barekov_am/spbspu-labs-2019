#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

const double MISCALCULATION = 0.001;

BOOST_AUTO_TEST_SUITE(testingCompositeShape)

BOOST_AUTO_TEST_CASE(testingCompositeShapeCopyConstructor)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  sherman::Circle testCircle({12.0, {8.0, 9.0}});
  sherman::CompositeShape testingCompositeShape(std::make_shared<sherman::Rectangle>(testRectangle));
  testingCompositeShape.add(std::make_shared<sherman::Circle>(testCircle));
  const sherman::rectangle_t frameRectangle = testingCompositeShape.getFrameRect();
  sherman::CompositeShape copiedCompositeShape(testingCompositeShape);

  BOOST_CHECK_CLOSE(copiedCompositeShape.getArea(), testingCompositeShape.getArea(), MISCALCULATION);
  BOOST_CHECK_EQUAL(copiedCompositeShape.size(), testingCompositeShape.size());
  sherman::rectangle_t copiedFrameRectangle = copiedCompositeShape.getFrameRect();
  BOOST_CHECK_CLOSE(copiedFrameRectangle.width, frameRectangle.width, MISCALCULATION);
  BOOST_CHECK_CLOSE(copiedFrameRectangle.height, frameRectangle.height, MISCALCULATION);
  BOOST_CHECK_CLOSE(copiedFrameRectangle.pos.x, frameRectangle.pos.x, MISCALCULATION);
  BOOST_CHECK_CLOSE(copiedFrameRectangle.pos.y, frameRectangle.pos.y, MISCALCULATION);
}

BOOST_AUTO_TEST_CASE(testingCompositeShapeMoveConstructor)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  sherman::Circle testCircle({12.0, {8.0, 9.0}});
  sherman::CompositeShape testingCompositeShape(std::make_shared<sherman::Rectangle>(testRectangle));
  testingCompositeShape.add(std::make_shared<sherman::Circle>(testCircle));
  const sherman::rectangle_t frameRectangle = testingCompositeShape.getFrameRect();
  const double compositeShapeArea = testingCompositeShape.getArea();
  const std::size_t countOfShapes = testingCompositeShape.size();
  sherman::CompositeShape movedCompositeShape(std::move(testingCompositeShape));

  BOOST_CHECK_CLOSE(movedCompositeShape.getArea(), compositeShapeArea, MISCALCULATION);
  sherman::rectangle_t movedFrameRectangle = movedCompositeShape.getFrameRect();
  BOOST_CHECK_CLOSE(movedFrameRectangle.width, frameRectangle.width, MISCALCULATION);
  BOOST_CHECK_CLOSE(movedFrameRectangle.height, frameRectangle.height, MISCALCULATION);
  BOOST_CHECK_CLOSE(movedFrameRectangle.pos.x, frameRectangle.pos.x, MISCALCULATION);
  BOOST_CHECK_CLOSE(movedFrameRectangle.pos.y, frameRectangle.pos.y, MISCALCULATION);
  BOOST_CHECK_EQUAL(movedCompositeShape.size(), countOfShapes);

  BOOST_CHECK_EQUAL(0, testingCompositeShape.size());
}

BOOST_AUTO_TEST_CASE(testingCompositeShapeCopyOperator)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  sherman::Circle testCircle({12.0, {8.0, 9.0}});
  sherman::CompositeShape testingCompositeShape(std::make_shared<sherman::Rectangle>(testRectangle));
  testingCompositeShape.add(std::make_shared<sherman::Circle>(testCircle));
  const sherman::rectangle_t frameRectangle = testingCompositeShape.getFrameRect();
  sherman::CompositeShape newCompositeShape = testingCompositeShape;

  BOOST_CHECK_CLOSE(newCompositeShape.getArea(), testingCompositeShape.getArea(), MISCALCULATION);
  BOOST_CHECK_EQUAL(newCompositeShape.size(), testingCompositeShape.size());
  sherman::rectangle_t copiedFrameRectangle = newCompositeShape.getFrameRect();
  BOOST_CHECK_CLOSE(copiedFrameRectangle.width, frameRectangle.width, MISCALCULATION);
  BOOST_CHECK_CLOSE(copiedFrameRectangle.height, frameRectangle.height, MISCALCULATION);
  BOOST_CHECK_CLOSE(copiedFrameRectangle.pos.x, frameRectangle.pos.x, MISCALCULATION);
  BOOST_CHECK_CLOSE(copiedFrameRectangle.pos.y, frameRectangle.pos.y, MISCALCULATION);
}

BOOST_AUTO_TEST_CASE(testingCompositeShapeMoveOperator)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  sherman::Circle testCircle({12.0, {8.0, 9.0}});
  sherman::CompositeShape testingCompositeShape(std::make_shared<sherman::Rectangle>(testRectangle));
  testingCompositeShape.add(std::make_shared<sherman::Circle>(testCircle));
  const sherman::rectangle_t frameRectangle = testingCompositeShape.getFrameRect();
  const double compositeShapeArea = testingCompositeShape.getArea();
  const std::size_t countOfShapes = testingCompositeShape.size();
  sherman::CompositeShape movedCompositeShape = std::move(testingCompositeShape);

  BOOST_CHECK_CLOSE(movedCompositeShape.getArea(), compositeShapeArea, MISCALCULATION);
  sherman::rectangle_t movedFrameRectangle = movedCompositeShape.getFrameRect();
  BOOST_CHECK_CLOSE(movedFrameRectangle.width, frameRectangle.width, MISCALCULATION);
  BOOST_CHECK_CLOSE(movedFrameRectangle.height, frameRectangle.height, MISCALCULATION);
  BOOST_CHECK_CLOSE(movedFrameRectangle.pos.x, frameRectangle.pos.x, MISCALCULATION);
  BOOST_CHECK_CLOSE(movedFrameRectangle.pos.y, frameRectangle.pos.y, MISCALCULATION);
  BOOST_CHECK_EQUAL(movedCompositeShape.size(), countOfShapes);

  BOOST_CHECK_EQUAL(0, testingCompositeShape.size());
}

BOOST_AUTO_TEST_CASE(testingCompositeShapeAfterMovingToThePoint)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  sherman::Circle testCircle({12.0, {8.0, 9.0}});
  sherman::CompositeShape testingCompositeShape(std::make_shared<sherman::Rectangle>(testRectangle));
  testingCompositeShape.add(std::make_shared<sherman::Circle>(testCircle));
  const double compositeShapeAreaBeforeMoving = testingCompositeShape.getArea();
  const sherman::rectangle_t frameRectangleBeforeMoving = testingCompositeShape.getFrameRect();
  testingCompositeShape.move({5.5, 6.6});

  BOOST_CHECK_CLOSE(testingCompositeShape.getArea(), compositeShapeAreaBeforeMoving, MISCALCULATION);
  sherman::rectangle_t frameRectangleAfterMoving = testingCompositeShape.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectangleAfterMoving.width, frameRectangleBeforeMoving.width, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectangleAfterMoving.height, frameRectangleBeforeMoving.height, MISCALCULATION);
}

BOOST_AUTO_TEST_CASE(testingCompositeShapeAfterMovingAlongAbscissaAndOrdinate)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  sherman::Circle testCircle({12.0, {8.0, 9.0}});
  sherman::CompositeShape testingCompositeShape(std::make_shared<sherman::Rectangle>(testRectangle));
  testingCompositeShape.add(std::make_shared<sherman::Circle>(testCircle));
  const double compositeShapeAreaBeforeMoving = testingCompositeShape.getArea();
  const sherman::rectangle_t frameRectangleBeforeMoving = testingCompositeShape.getFrameRect();
  testingCompositeShape.move(2.0, -1.8);

  BOOST_CHECK_CLOSE(testingCompositeShape.getArea(), compositeShapeAreaBeforeMoving, MISCALCULATION);
  sherman::rectangle_t frameRectangleAfterMoving = testingCompositeShape.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectangleAfterMoving.width, frameRectangleBeforeMoving.width, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectangleAfterMoving.height, frameRectangleBeforeMoving.height, MISCALCULATION);
}

BOOST_AUTO_TEST_CASE(testingCompositeShapeAfterScale)
{
  sherman::Rectangle testRectangleForIncrease({10.5, 6.9, {8.0, 9.0}});
  sherman::Circle testCircleForIncrease({12.0, {8.0, 9.0}});

  sherman::CompositeShape testingCompositeShapeForIncrease(std::make_shared<sherman::Rectangle>(testRectangleForIncrease));
  testingCompositeShapeForIncrease.add(std::make_shared<sherman::Circle>(testCircleForIncrease));
  const double compositeShapeAreaBeforeIncrease = testingCompositeShapeForIncrease.getArea();
  const sherman::rectangle_t frameRectangleBeforeIncrease = testingCompositeShapeForIncrease.getFrameRect();
  const double increase = 4;
  testingCompositeShapeForIncrease.scale(increase);

  BOOST_CHECK_CLOSE(testingCompositeShapeForIncrease.getArea(), compositeShapeAreaBeforeIncrease * increase * increase, MISCALCULATION);
  sherman::rectangle_t frameRectangleAfterIncrease = testingCompositeShapeForIncrease.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectangleAfterIncrease.width, frameRectangleBeforeIncrease.width * increase, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectangleAfterIncrease.height, frameRectangleBeforeIncrease.height * increase, MISCALCULATION);

  sherman::Rectangle testRectangleForDecrease({5.0, 8.1, {7.9, 0.5}});
  sherman::Circle testCircleForDecrease({6.7, {7.0, 5.0}});

  sherman::CompositeShape testingCompositeShapeForDecrease(std::make_shared<sherman::Rectangle>(testRectangleForDecrease));
  testingCompositeShapeForDecrease.add(std::make_shared<sherman::Circle>(testCircleForDecrease));
  const double compositeShapeAreaBeforeDecrease = testingCompositeShapeForDecrease.getArea();
  const sherman::rectangle_t frameRectangleBeforeDecrease = testingCompositeShapeForDecrease.getFrameRect();
  const double decrease = 0.25;
  testingCompositeShapeForDecrease.scale(decrease);

  BOOST_CHECK_CLOSE(testingCompositeShapeForDecrease.getArea(), compositeShapeAreaBeforeDecrease * decrease * decrease, MISCALCULATION);
  sherman::rectangle_t frameRectangleAfterDecrease = testingCompositeShapeForDecrease.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectangleAfterDecrease.width, frameRectangleBeforeDecrease.width * decrease, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectangleAfterDecrease.height, frameRectangleBeforeDecrease.height * decrease, MISCALCULATION);
}

BOOST_AUTO_TEST_CASE(testingCompositeShapeAfterRotation)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  sherman::Circle testCircle({12.0, {8.0, 9.0}});
  sherman::CompositeShape testingCompositeShape(std::make_shared<sherman::Rectangle>(testRectangle));
  testingCompositeShape.add(std::make_shared<sherman::Circle>(testCircle));
  const sherman::rectangle_t frameRectangleBeforeRotation = testingCompositeShape.getFrameRect();
  const double compositeShapeAreaBeforeRotation = testingCompositeShape.getArea();
  testingCompositeShape.rotate(60);

  BOOST_CHECK_CLOSE(testingCompositeShape.getArea(), compositeShapeAreaBeforeRotation, MISCALCULATION);
  sherman::rectangle_t frameRectangleAfterRotation = testingCompositeShape.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectangleAfterRotation.width, frameRectangleBeforeRotation.width, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectangleAfterRotation.height, frameRectangleBeforeRotation.height, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectangleAfterRotation.pos.x, frameRectangleBeforeRotation.pos.x, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectangleAfterRotation.pos.y, frameRectangleBeforeRotation.pos.y, MISCALCULATION);
}

BOOST_AUTO_TEST_CASE(testingCompositeShapeAreaAfterAddingNewShape)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  sherman::Circle testCircle({12.0, {8.0, 9.0}});
  sherman::CompositeShape testingCompositeShape(std::make_shared<sherman::Rectangle>(testRectangle));
  const double compositeShapeAreaBeforeAddingNewShape = testingCompositeShape.getArea();
  const int numberOfShapesBeforeAddingShape = testingCompositeShape.size();
  testingCompositeShape.add(std::make_shared<sherman::Circle>(testCircle));

  BOOST_CHECK_CLOSE(testingCompositeShape.getArea(), testCircle.getArea() + compositeShapeAreaBeforeAddingNewShape, MISCALCULATION);
  const int numberOfShapesAfterAddingShape = testingCompositeShape.size();
  BOOST_CHECK_EQUAL(numberOfShapesAfterAddingShape, numberOfShapesBeforeAddingShape + 1);
}

BOOST_AUTO_TEST_CASE(testingCompositeShapeAreaAfterDeletingShape)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  sherman::Circle testCircle({12.0, {8.0, 9.0}});
  sherman::CompositeShape testingCompositeShape(std::make_shared<sherman::Rectangle>(testRectangle));
  testingCompositeShape.add(std::make_shared<sherman::Circle>(testCircle));
  const double compositeShapeAreaBeforeDeletingShape = testingCompositeShape.getArea();
  const int numberOfShapesBeforeDeletingShape = testingCompositeShape.size();
  testingCompositeShape.remove(1);

  BOOST_CHECK_CLOSE(testingCompositeShape.getArea(), compositeShapeAreaBeforeDeletingShape - testCircle.getArea(), MISCALCULATION);
  const int numberOfShapesAfterDeletingShape = testingCompositeShape.size();
  BOOST_CHECK_EQUAL(numberOfShapesAfterDeletingShape, numberOfShapesBeforeDeletingShape - 1);
}

BOOST_AUTO_TEST_CASE(compositeShapeInvalidScaleValue)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  sherman::CompositeShape testingCompositeShape(std::make_shared<sherman::Rectangle>(testRectangle));

  BOOST_CHECK_THROW(testingCompositeShape.scale(-10.1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShapeInvalidDeleting)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  sherman::CompositeShape testingCompositeShape(std::make_shared<sherman::Rectangle>(testRectangle));

  BOOST_CHECK_THROW(testingCompositeShape.remove(8), std::out_of_range);
  BOOST_CHECK_THROW(testingCompositeShape.remove(-1), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(accessToTheShapeByTheInvalidIndex)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  sherman::CompositeShape testingCompositeShape(std::make_shared<sherman::Rectangle>(testRectangle));

  BOOST_CHECK_THROW(testingCompositeShape[-3], std::out_of_range);
  BOOST_CHECK_THROW(testingCompositeShape[5], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(invalidMovingAnEmptyCompositeShape)
{
  sherman::CompositeShape testingCompositeShape;

  BOOST_CHECK_THROW(testingCompositeShape.move(5.0, 5.0), std::logic_error);
  BOOST_CHECK_THROW(testingCompositeShape.move({-1.0, -1.0}), std::logic_error);
}

BOOST_AUTO_TEST_CASE(gettingParametersOfAnEmptyCompositeShape)
{
  sherman::CompositeShape testingCompositeShape;

  BOOST_CHECK_THROW(testingCompositeShape.getArea(), std::logic_error);
  BOOST_CHECK_THROW(testingCompositeShape.getFrameRect(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(invalidZoomingScaleOfAnEmptyCompositeShape)
{
  sherman::CompositeShape testingCompositeShape;

  BOOST_CHECK_THROW(testingCompositeShape.scale(3), std::logic_error);
}

BOOST_AUTO_TEST_SUITE_END()
