#include "partition.hpp"
#include <cmath>

sherman::Matrix<sherman::Shape> sherman::layer(const sherman::CompositeShape &compositeShape)
{
  sherman::Matrix<sherman::Shape> matrix;
  for (std::size_t i = 0; i < compositeShape.size(); ++i)
  {
    std::size_t row = 0;
    std::size_t j = matrix.getRows();
    while (j-- > 0)
    {
      for (std::size_t k = 0; k < matrix[j].size(); ++k)
      {
        if (intersect(*compositeShape[i], *matrix[j][k]))
        {
          row = j + 1;
          break;
        }
      }
      if (row)
      {
        break;
      }
    }
    matrix.add(row, compositeShape[i]);
  }
  return matrix;
}

bool sherman::intersect(const sherman::Shape &firstShape, const sherman::Shape &secondShape)
{
  const sherman::rectangle_t firstFrameRect = firstShape.getFrameRect();
  const sherman::rectangle_t secondFrameRect = secondShape.getFrameRect();
  if (std::abs(firstFrameRect.pos.x - secondFrameRect.pos.x) > (firstFrameRect.width + secondFrameRect.width) / 2)
  {
    return false;
  }
  if (std::abs(firstFrameRect.pos.y - secondFrameRect.pos.y) > (firstFrameRect.height + secondFrameRect.height) / 2)
  {
    return false;
  }
  return true;
}
