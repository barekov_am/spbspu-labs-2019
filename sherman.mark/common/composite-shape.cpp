#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <memory>
#include <algorithm>
#include <cmath>

sherman::CompositeShape::CompositeShape() :
  counter_(0),
  arrayOfShapes_(nullptr)
{
}

sherman::CompositeShape::CompositeShape(const sherman::CompositeShape &copiedCompositeShape) :
  counter_(copiedCompositeShape.counter_),
  arrayOfShapes_(std::make_unique<shape_ptr []>(copiedCompositeShape.counter_))
{
  for (std::size_t i = 0; i < counter_; ++i)
  {
    arrayOfShapes_[i] = copiedCompositeShape.arrayOfShapes_[i];
  }
}

sherman::CompositeShape::CompositeShape(sherman::CompositeShape &&movedCompositeShape) noexcept :
  counter_(movedCompositeShape.counter_),
  arrayOfShapes_(std::move(movedCompositeShape.arrayOfShapes_))
{
  movedCompositeShape.counter_ = 0;
}

sherman::CompositeShape::CompositeShape(const shape_ptr &shape) :
  counter_(1),
  arrayOfShapes_(std::make_unique<shape_ptr []>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("You cannot pass an empty pointer");
  }
  arrayOfShapes_[0] = shape;
}

sherman::CompositeShape &sherman::CompositeShape::operator =(const sherman::CompositeShape &copiedCompositeShape)
{
  if (this != &copiedCompositeShape)
  {
    counter_ = copiedCompositeShape.counter_;
    arrayOfShapes_ = std::make_unique<shape_ptr []>(copiedCompositeShape.counter_);
    for (std::size_t i = 0; i < counter_; ++i)
    {
      arrayOfShapes_[i] = copiedCompositeShape.arrayOfShapes_[i];
    }
  }
  return *this;
}

sherman::CompositeShape &sherman::CompositeShape::operator =(sherman::CompositeShape &&movedCompositeShape) noexcept
{
  if (this != &movedCompositeShape)
  {
    counter_ = movedCompositeShape.counter_;
    arrayOfShapes_ = std::move(movedCompositeShape.arrayOfShapes_);
  }
  return *this;
}

sherman::shape_ptr sherman::CompositeShape::operator [](std::size_t number) const
{
  if (number >= counter_)
  {
    throw std::out_of_range("Invalid shape number");
  }
  return arrayOfShapes_[number];
}

double sherman::CompositeShape::getArea() const
{
  if (counter_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  double areaOfTheShapes = 0.0;
  for (std::size_t i = 0; i < counter_ ; ++i)
  {
    areaOfTheShapes += arrayOfShapes_[i]->getArea();
  }
  return areaOfTheShapes;
}

sherman::rectangle_t sherman::CompositeShape::getFrameRect() const
{
  if (counter_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  sherman::rectangle_t currentFrameRectangle = arrayOfShapes_[0]->getFrameRect();
  double right = currentFrameRectangle.pos.x + currentFrameRectangle.width / 2;
  double left = currentFrameRectangle.pos.x - currentFrameRectangle.width /2;
  double top = currentFrameRectangle.pos.y + currentFrameRectangle.height / 2;
  double bottom = currentFrameRectangle.pos.y - currentFrameRectangle.height / 2;
  for (std::size_t i = 1; i < counter_; ++i)
  {
    currentFrameRectangle = arrayOfShapes_[i]->getFrameRect();
    right = std::max(currentFrameRectangle.pos.x + currentFrameRectangle.width /2 , right);
    left = std::min(currentFrameRectangle.pos.x - currentFrameRectangle.width /2 , left);
    top = std::max(currentFrameRectangle.pos.y + currentFrameRectangle.height /2 , top);
    bottom = std::min(currentFrameRectangle.pos.y - currentFrameRectangle.height /2 , bottom);
  }
  return {right - left, top - bottom, {(right + left) / 2, (top + bottom) / 2}};
}

std::size_t sherman::CompositeShape::size() const
{
  return counter_;
}

void sherman::CompositeShape::move(double changeX, double changeY)
{
  if (counter_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  for (std::size_t i = 0; i < counter_; ++i)
  {
    arrayOfShapes_[i]->move(changeX, changeY);
  }
}

void sherman::CompositeShape::move(const point_t &newcenter)
{
  point_t centerOfFrameRectangle = getFrameRect().pos;
  double changeX = newcenter.x - centerOfFrameRectangle.x;
  double changeY = newcenter.y - centerOfFrameRectangle.y;
  move(changeX, changeY);
}

void sherman::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient must be more than zero");
  }
  point_t centerOfFrameRectangle = getFrameRect().pos;
  for (std::size_t i = 0; i < counter_; ++i)
  {
    point_t shapeCenter = arrayOfShapes_[i]->getFrameRect().pos;
    arrayOfShapes_[i]->scale(coefficient);
    double changeX = (shapeCenter.x - centerOfFrameRectangle.x) * (coefficient - 1);
    double changeY = (shapeCenter.y - centerOfFrameRectangle.y) * (coefficient - 1);
    arrayOfShapes_[i]->move(changeX, changeY);
  }
}

void sherman::CompositeShape::rotate(double angle)
{
  const point_t center = getFrameRect().pos;
  const double sine = std::sin(angle * M_PI / 180);
  const double cosine = std::cos(angle * M_PI / 180);
  for (std::size_t i = 0; i < counter_; ++i)
  {
    const point_t centerOfTheShape = arrayOfShapes_[i]->getFrameRect().pos;
    const double changeX = centerOfTheShape.x - center.x;
    const double changeY = centerOfTheShape.y - center.y;
    arrayOfShapes_[i]->move(changeX * (cosine - 1) - changeY * sine, changeX * sine + changeY * (cosine - 1));
    arrayOfShapes_[i]->rotate(angle);
  }
}

void sherman::CompositeShape::add(const shape_ptr &shape)
{
  array_ptr tempArray(std::make_unique<shape_ptr []>(counter_ + 1));
  for (std::size_t i = 0; i < counter_; ++i)
  {
    tempArray[i] = arrayOfShapes_[i];
  }
  tempArray[counter_] = shape;
  arrayOfShapes_ = std::move(tempArray);
  counter_++;
}

void sherman::CompositeShape::remove(size_t number)
{
  if (number >= counter_)
  {
    throw std::out_of_range("Invalid shape number");
  }
  counter_--;
  for (std::size_t i = number; i < counter_; ++i)
  {
    arrayOfShapes_[i] = arrayOfShapes_[i + 1];
  }
  arrayOfShapes_[counter_] = nullptr;
}

void sherman::CompositeShape::printInformation() const
{
  std::cout << "Center of the composite shape is (" << getFrameRect().pos.x << "," << getFrameRect().pos.y << ")\n";
  std::cout << "Area of the composite shape is " << getArea() << '\n';
  std::cout << "Parameters of the frame rectangle: " << '\n';
  std::cout << "Width: " << getFrameRect().width << '\n';
  std::cout << "Height: " << getFrameRect().height << '\n';
  std::cout << "Number of shapes " << counter_ << '\n';
}
