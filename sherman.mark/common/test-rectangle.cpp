#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double MISCALCULATION = 0.001;

BOOST_AUTO_TEST_SUITE(testingRectangle)

BOOST_AUTO_TEST_CASE(testingRectangleAfterMovingToThePoint)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  const double rectangleAreaBeforeMoving = testRectangle.getArea();
  const sherman::rectangle_t frameRectBeforeMoving = testRectangle.getFrameRect();
  testRectangle.move({5.0, -3.5});

  BOOST_CHECK_CLOSE(testRectangle.getArea(), rectangleAreaBeforeMoving, MISCALCULATION);
  sherman::rectangle_t frameRectAfterMoving = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, MISCALCULATION);
}

BOOST_AUTO_TEST_CASE(testingRectangleAfterMovingAlongAbscissaAndOrdinate)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  const double rectangleAreaBeforeMoving = testRectangle.getArea();
  const sherman::rectangle_t frameRectBeforeMoving = testRectangle.getFrameRect();
  testRectangle.move(-2.1, 6.0);

  BOOST_CHECK_CLOSE(testRectangle.getArea(), rectangleAreaBeforeMoving, MISCALCULATION);
  sherman::rectangle_t frameRectAfterMoving = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, MISCALCULATION);
}

BOOST_AUTO_TEST_CASE(testingRectangleAfterScale)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  const double rectangleAreaBeforeZoomingScale = testRectangle.getArea();
  const sherman::rectangle_t frameRectBeforeZoomingScale = testRectangle.getFrameRect();
  const double coefficient = 10.0;
  testRectangle.scale(coefficient);

  BOOST_CHECK_CLOSE(testRectangle.getArea(), rectangleAreaBeforeZoomingScale * coefficient * coefficient, MISCALCULATION);
  sherman::rectangle_t frameRectAfterZoomingScale = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectAfterZoomingScale.width, frameRectBeforeZoomingScale.width * coefficient, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectAfterZoomingScale.height, frameRectBeforeZoomingScale.height * coefficient, MISCALCULATION);
}

BOOST_AUTO_TEST_CASE(testingRectangleAfterRotation)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  const double rectangleAreaBeforeRotation = testRectangle.getArea();
  const sherman::rectangle_t frameRectBeforeRotation = testRectangle.getFrameRect();
  testRectangle.rotate(90);

  BOOST_CHECK_CLOSE(testRectangle.getArea(), rectangleAreaBeforeRotation, MISCALCULATION);
  sherman::rectangle_t frameRectAfterRotation = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectAfterRotation.width, frameRectBeforeRotation.height, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectAfterRotation.height, frameRectBeforeRotation.width, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectAfterRotation.pos.x, frameRectBeforeRotation.pos.x, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectAfterRotation.pos.y, frameRectBeforeRotation.pos.y, MISCALCULATION);
}

BOOST_AUTO_TEST_CASE(rectangleInvalidWidthValue)
{
  BOOST_CHECK_THROW(sherman::Rectangle({-45.0, 6.9, {8.0, 9.0}}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleInvalidHeightValue)
{
  BOOST_CHECK_THROW(sherman::Rectangle({10.5, -67.0, {8.0, 9.0}}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleInvalidScaleValue)
{
  sherman::Rectangle testRectangle({10.5, 6.9, {8.0, 9.0}});
  BOOST_CHECK_THROW(testRectangle.scale(-6), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
