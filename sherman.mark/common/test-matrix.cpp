#include <memory>
#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "matrix.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"


BOOST_AUTO_TEST_SUITE(testingMatrix)

BOOST_AUTO_TEST_CASE(testingMatrixCopyConstructor)
{
  sherman::Matrix<int> testingMatrix;
  const std::size_t row = 5;
  const std::size_t column = 5;
  for (std::size_t i = 0; i < row; ++i)
  {
    for (std::size_t j = 0; j < column; ++j)
    {
      testingMatrix.add(i, std::make_shared<int> (i + j));
    }
  }
  sherman::Matrix<int> copiedMatrix(testingMatrix);

  BOOST_CHECK(copiedMatrix == testingMatrix);
}

BOOST_AUTO_TEST_CASE(testingMatrixMoveConstructor)
{
  sherman::Matrix<int> testingMatrix;
  const std::size_t row = 5;
  const std::size_t column = 5;
  for (std::size_t i = 0; i < row; ++i)
  {
    for (std::size_t j = 0; j < column; ++j)
    {
      testingMatrix.add(i, std::make_shared<int> (i + j));
    }
  }
  sherman::Matrix<int> temporaryMatrix(testingMatrix);
  sherman::Matrix<int> movedMatrix(std::move(testingMatrix));

  BOOST_CHECK(movedMatrix == temporaryMatrix);
}

BOOST_AUTO_TEST_CASE(testingMatrixCopyOperator)
{
  sherman::Matrix<int> testingMatrix;
  const std::size_t row = 5;
  const std::size_t column = 5;
  for (std::size_t i = 0; i < row; ++i)
  {
    for (std::size_t j = 0; j < column; ++j)
    {
      testingMatrix.add(i, std::make_shared<int>(i + j));
    }
  }
  sherman::Matrix<int> copiedMatrix = testingMatrix;

  BOOST_CHECK(copiedMatrix == testingMatrix);
}

BOOST_AUTO_TEST_CASE(testingMatrixMoveOperator)
{
  sherman::Matrix<int> testingMatrix;
  const std::size_t row = 5;
  const std::size_t column = 5;
  for (std::size_t i = 0; i < row; ++i)
  {
    for (std::size_t j = 0; j < column; ++j)
    {
      testingMatrix.add(i, std::make_shared<int> (i + j));
    }
  }
  sherman::Matrix<int> temporaryMatrix(testingMatrix);
  sherman::Matrix<int> movedMatrix = std::move(testingMatrix);

  BOOST_CHECK(movedMatrix == temporaryMatrix);
}

BOOST_AUTO_TEST_CASE(testingMatrixEqualityOperator)
{
  sherman::Matrix<int> testingMatrix;
  const std::size_t row = 5;
  const std::size_t column = 5;
  for (std::size_t i = 0; i < row; ++i)
  {
    for (std::size_t j = 0; j < column; ++j)
    {
      testingMatrix.add(i, std::make_shared<int> (i + j));
    }
  }
  sherman::Matrix<int> copiedMatrix(testingMatrix);
  sherman::Matrix<int> emptyMatrix;

  BOOST_CHECK_EQUAL(copiedMatrix == testingMatrix, true);
  BOOST_CHECK_EQUAL(emptyMatrix == testingMatrix, false);
}

BOOST_AUTO_TEST_CASE(testingMatrixInequalityOperator)
{
  sherman::Matrix<int> testingMatrix;
  const std::size_t row = 5;
  const std::size_t column = 5;
  for (std::size_t i = 0; i < row; ++i)
  {
    for (std::size_t j = 0; j < column; ++j)
    {
      testingMatrix.add(i, std::make_shared<int> (i + j));
    }
  }
  sherman::Matrix<int> copiedMatrix(testingMatrix);
  sherman::Matrix<int> emptyMatrix;

  BOOST_CHECK_EQUAL(copiedMatrix != testingMatrix, false);
  BOOST_CHECK_EQUAL(emptyMatrix != testingMatrix, true);
}

BOOST_AUTO_TEST_CASE(testingMatrixGetMethodByInvalidIndex)
{
  sherman::Matrix<int> testingMatrix;
  const std::size_t row = 5;
  const std::size_t column = 5;
  for (std::size_t i = 0; i < row; ++i)
  {
    for (std::size_t j = 0; j < column; ++j)
    {
      testingMatrix.add(i, std::make_shared<int> (i + j));
    }
  }

  BOOST_CHECK_THROW(testingMatrix[10][10], std::out_of_range);
  BOOST_CHECK_THROW(testingMatrix[-10][-10], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
