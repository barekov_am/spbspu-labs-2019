#ifndef COMMON_MATRIX_HPP
#define COMMON_MATRIX_HPP

#include <memory>
#include <stdexcept>

namespace sherman
{
  template <typename T>
  class Matrix
  {
  public:
    using ptrType = std::shared_ptr<T>;
    using arrayType = std::unique_ptr<ptrType []>;

    class Layer
    {
    public:
      Layer(const Layer &);
      Layer(Layer &&) noexcept;
      Layer(ptrType *, std::size_t);
      ~Layer() = default;

      Layer &operator =(const Layer &);
      Layer &operator =(Layer &&) noexcept;
      ptrType operator [](std::size_t) const;

      std::size_t size() const;
    private:
      std::size_t counter_;
      arrayType data_;
    };

    Matrix();
    Matrix(const Matrix &);
    Matrix(Matrix &&) noexcept;
    ~Matrix() = default;

    Matrix &operator =(const Matrix &);
    Matrix &operator =(Matrix &&) noexcept;

    Layer operator [](std::size_t) const;
    bool operator ==(const Matrix &) const;
    bool operator !=(const Matrix &) const;

    std::size_t getRows() const;
    void swap(Matrix&) noexcept;
    void add(std::size_t , ptrType);
  private:
    std::size_t rows_;
    std::size_t counter_;
    std::unique_ptr<std::size_t []> layerSize_;
    arrayType data_;
  };

  template <typename T>
  Matrix<T>::Matrix() :
    rows_(0),
    counter_(0)
  {
  }

  template <typename T>
  Matrix<T>::Matrix(const Matrix<T> &copiedMatrix) :
    rows_(copiedMatrix.rows_),
    counter_(copiedMatrix.counter_),
    layerSize_(std::make_unique<std::size_t []>(copiedMatrix.rows_)),
    data_(std::make_unique<ptrType []>(copiedMatrix.counter_))
  {
    for (std::size_t i = 0; i < rows_; ++i)
    {
      layerSize_[i] = copiedMatrix.layerSize_[i];
    }

    for (std::size_t i = 0; i < counter_; ++i)
    {
      data_[i] = copiedMatrix.data_[i];
    }
  }

  template <typename T>
  Matrix<T>::Matrix(Matrix<T> &&movedMatrix) noexcept :
    rows_(movedMatrix.rows_),
    counter_(movedMatrix.counter_),
    layerSize_(std::move(movedMatrix.layerSize_)),
    data_(std::move(movedMatrix.data_))
  {
      movedMatrix.rows_ = 0;
      movedMatrix.counter_ = 0;
  }

  template <typename T>
  Matrix<T> &Matrix<T>::operator =(const Matrix<T> &copiedMatrix)
  {
    if (this != &copiedMatrix)
    {
      swap(Matrix<T>(copiedMatrix));
    }
    return *this;
  }

  template <typename T>
  Matrix<T> &Matrix<T>::operator =(Matrix<T> &&movedMatrix) noexcept
  {
    if (this != &movedMatrix)
    {
      rows_ = movedMatrix.rows_;
      movedMatrix.rows_ = 0;
      counter_ = movedMatrix.counter_;
      movedMatrix.counter_ = 0;
      layerSize_ = std::move(movedMatrix.layerSize_);
      data_ = std::move(movedMatrix.data_);
    }
    return *this;
  }

  template <typename T>
  typename Matrix<T>::Layer Matrix<T>::operator [](std::size_t row) const
  {
    if (row >= rows_)
    {
      throw std::out_of_range("Invalid row number");
    }
    std::size_t rowNumber= 0;
    for (std::size_t i = 0; i < row; ++i)
    {
      rowNumber += layerSize_[i];
    }
    return Layer(&data_[rowNumber], layerSize_[row]);
  }

  template <typename T>
  bool Matrix<T>::operator ==(const Matrix<T> &rhs) const
  {
    if ((rows_ != rhs.rows_) || (counter_ != rhs.counter_))
    {
      return false;
    }
    for (std::size_t i = 0; i < rows_; ++i)
    {
      if (layerSize_[i] != rhs.layerSize_[i])
      {
        return false;
      }
    }
    for (std::size_t i = 0; i < counter_; ++i)
    {
      if (data_[i] != rhs.data_[i])
      {
        return false;
      }
    }
    return true;
  }

  template <typename T>
  bool Matrix<T>::operator !=(const Matrix<T> &rhs) const
  {
    return !(*this == rhs);
  }

  template <typename T>
  std::size_t Matrix<T>::getRows() const
  {
    return rows_;
  }

  template <typename T>
  void Matrix<T>::swap(Matrix<T> &swappedMatrix) noexcept
  {
    std::swap(rows_, swappedMatrix.rows_);
    std::swap(counter_, swappedMatrix.counter_);
    std::swap(layerSize_, swappedMatrix.layerSize_);
    std::swap(data_, swappedMatrix.data_);
  }

  template <typename T>
  void Matrix<T>::add(std::size_t row, ptrType object)
  {
    if (row > rows_)
    {
      throw std::out_of_range("Invalid row number");
    }
    if (object == nullptr)
    {
      throw std::invalid_argument("You can't add nullptr");
    }
    if (row == rows_)
    {
      std::unique_ptr<std::size_t []> tmpLayerSize = std::make_unique<std::size_t []>(rows_ + 1);
      arrayType tmpData = std::make_unique<ptrType []>(counter_ + 1);
      for (std::size_t i = 0; i < rows_; ++i)
      {
        tmpLayerSize[i] = layerSize_[i];
      }
      tmpLayerSize[rows_++] = 1;
      layerSize_ = std::move(tmpLayerSize);
      for (std::size_t i = 0; i < counter_; ++i)
      {
        tmpData[i] = data_[i];
      }
      tmpData[counter_++] = object;
      data_ = std::move(tmpData);
    }
    else
    {
      arrayType tmpData = std::make_unique<ptrType []>(counter_ + 1);
      std::size_t newIndexOfTheElement = 0;
      std::size_t oldIndexOfTheElement = 0;
      for (std::size_t i = 0; i < rows_; ++i)
      {
        for (std::size_t j = 0; j < layerSize_[i]; ++j)
        {
          tmpData[newIndexOfTheElement++] = data_[oldIndexOfTheElement++];
        }
        if (i == row)
        {
          tmpData[newIndexOfTheElement++] = object;
        }
      }
      data_ = std::move(tmpData);
      ++layerSize_[row];
      ++counter_;
    }
  }

  template <typename T>
  Matrix<T>::Layer::Layer(const Matrix<T>::Layer &copiedLayer) :
    counter_(copiedLayer.counter_),
    data_(std::make_unique<ptrType []>(copiedLayer.counter_))
  {
    for (std::size_t i = 0; i < counter_; ++i)
    {
      data_[i] = copiedLayer.data_[i];
    }
  }

  template <typename T>
  Matrix<T>::Layer::Layer(Matrix<T>::Layer &&movedLayer) noexcept :
    counter_(movedLayer.counter_),
    data_(std::move(movedLayer.data_))
  {
    movedLayer.counter_= 0;
  }

  template <typename T>
  Matrix<T>::Layer::Layer(Matrix<T>::ptrType *array, std::size_t size) :
    counter_(size),
    data_(std::make_unique<ptrType []>(size))
  {
    for (std::size_t i = 0; i < counter_; ++i)
    {
      data_[i] = array[i];
    }
  }

  template <typename T>
  typename Matrix<T>::Layer &Matrix<T>::Layer::operator =(const Matrix<T>::Layer &copiedLayer)
  {
    if (this != &copiedLayer)
    {
      data_ = std::make_unique<ptrType  []>(copiedLayer.counter_);
      for (std::size_t i = 0; i < copiedLayer.counter_; ++i)
      {
        data_[i] = copiedLayer.data_[i];
      }
      counter_ = copiedLayer.counter_;
    }
    return *this;
  }

  template <typename T>
  typename Matrix<T>::Layer &Matrix<T>::Layer::operator =(Matrix<T>::Layer &&movedLayer) noexcept
  {
    if (this != &movedLayer)
    {
      counter_ = movedLayer.counter_;
      movedLayer.counter_ = 0;
      data_ = std::move(movedLayer.data_);
    }
    return *this;
  }

  template <typename T>
  typename Matrix<T>::ptrType Matrix<T>::Layer::operator [](std::size_t number) const
  {
    if (number >= counter_)
    {
      throw std::out_of_range("Invalid column number");
    }
    return data_[number];
  }

  template <typename T>
  std::size_t Matrix<T>::Layer::size() const
  {
    return counter_;
  }

}
#endif
