#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "partition.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testingPartition)

BOOST_AUTO_TEST_CASE(testingPartitionForCorrectWorking)
{
  sherman::Rectangle rectan1({5, 6, {0, 0}});
  sherman::Rectangle rectan2({1, 4, {0, -4}});
  sherman::Circle circle({3, {20, 20}});
  sherman::CompositeShape compositeShape(std::make_shared<sherman::Rectangle>(rectan1));
  compositeShape.add(std::make_shared<sherman::Rectangle>(rectan2));
  compositeShape.add(std::make_shared<sherman::Circle>(circle));

  sherman::Matrix<sherman::Shape> matrix = sherman::layer(compositeShape);
  BOOST_CHECK_EQUAL(matrix.getRows(), 2);
  BOOST_CHECK_EQUAL(matrix[0].size(), 2);
  BOOST_CHECK_EQUAL(matrix[1].size(), 1);
  BOOST_CHECK_EQUAL(matrix[0][0], compositeShape[0]);
  BOOST_CHECK_EQUAL(matrix[0][1], compositeShape[2]);
  BOOST_CHECK_EQUAL(matrix[1][0], compositeShape[1]);
}

BOOST_AUTO_TEST_CASE(testingIntersection)
{
  sherman::Rectangle rectangle({5, 6, {0, 0}});
  sherman::Rectangle rectangle1({1, 4, {0, -4}});
  sherman::Circle circle1({2, {3, 0}});
  sherman::Rectangle rectangle2({1, 4, {0, 4}});
  sherman::Circle circle2({2, {-3, 0}});
  sherman:: Circle circle({4, {20, 20}});

  BOOST_CHECK(sherman::intersect(rectangle, rectangle1));
  BOOST_CHECK(sherman::intersect(rectangle, circle1));
  BOOST_CHECK(sherman::intersect(rectangle, rectangle2));
  BOOST_CHECK(sherman::intersect(rectangle, circle2));

  BOOST_CHECK(!sherman::intersect(rectangle, circle));
}

BOOST_AUTO_TEST_CASE(testingEmptyPartition)
{
  sherman::CompositeShape compositeShape;
  sherman::Matrix<sherman::Shape> matrix = sherman::layer(compositeShape);

  BOOST_CHECK_EQUAL(matrix.getRows(), 0);
}

BOOST_AUTO_TEST_SUITE_END()
