#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

const double MISCALCULATION = 0.001;

BOOST_AUTO_TEST_SUITE(testingCircle)

BOOST_AUTO_TEST_CASE(testingCicleAfterMovingToThePoint)
{
  sherman::Circle testCircle({12.0, {8.0, 9.0}});
  const double circleAreaBeforeMoving = testCircle.getArea();
  const sherman::rectangle_t frameRectBeforeMoving = testCircle.getFrameRect();
  testCircle.move({5.0, -3.5});

  BOOST_CHECK_CLOSE(testCircle.getArea(), circleAreaBeforeMoving, MISCALCULATION);
  sherman::rectangle_t frameRectAfterMoving = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, MISCALCULATION);
}

BOOST_AUTO_TEST_CASE(testingCircleAfterMovingAlongAbscissaAndOrdinate)
{
  sherman::Circle testCircle({12.0, {8.0, 9.0}});
  const double circleAreaBeforeMoving = testCircle.getArea();
  const sherman::rectangle_t frameRectBeforeMoving = testCircle.getFrameRect();
  testCircle.move(-2.1, 6.0);

  BOOST_CHECK_CLOSE(testCircle.getArea(), circleAreaBeforeMoving, MISCALCULATION);
  sherman::rectangle_t frameRectAfterMoving = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, MISCALCULATION);
}

BOOST_AUTO_TEST_CASE(testingCircleAfterScale)
{
  sherman::Circle testCircle({12.0, {8.0, 9.0}});
  const double circleAreaBeforeZoomingScale = testCircle.getArea();
  const sherman::rectangle_t frameRectBeforeZoomingScale = testCircle.getFrameRect();
  const double coefficient = 10.0;
  testCircle.scale(coefficient);

  BOOST_CHECK_CLOSE(testCircle.getArea(), circleAreaBeforeZoomingScale * coefficient * coefficient, MISCALCULATION);
  sherman::rectangle_t frameRectAfterZoomingScale = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectAfterZoomingScale.width, frameRectBeforeZoomingScale.width * coefficient, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectAfterZoomingScale.height, frameRectBeforeZoomingScale.height * coefficient, MISCALCULATION);
}

BOOST_AUTO_TEST_CASE(testingCircleAfterRotation)
{
  sherman::Circle testCircle({12.0, {8.0, 9.0}});
  const double circleAreaBeforeRotation = testCircle.getArea();
  const sherman::rectangle_t frameRectBeforeRotation = testCircle.getFrameRect();
  testCircle.rotate(60);

  BOOST_CHECK_CLOSE(testCircle.getArea(), circleAreaBeforeRotation, MISCALCULATION);
  sherman::rectangle_t frameRectAfterRotation = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectAfterRotation.width, frameRectBeforeRotation.height, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectAfterRotation.height, frameRectBeforeRotation.width, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectAfterRotation.pos.x, frameRectBeforeRotation.pos.x, MISCALCULATION);
  BOOST_CHECK_CLOSE(frameRectAfterRotation.pos.y, frameRectBeforeRotation.pos.y, MISCALCULATION);
}

BOOST_AUTO_TEST_CASE(circleInvalidRadiusValue)
{
  BOOST_CHECK_THROW(sherman::Circle({-19.0, {8.0, 9.0}}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circleInvalidScaleValue)
{
  sherman::Circle testCircle({12.0, {8.0, 9.0}});
  BOOST_CHECK_THROW(testCircle.scale(-7), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
