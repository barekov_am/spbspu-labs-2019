#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

sherman::Rectangle::Rectangle(double width, double height, const point_t &center) :
  width_(width),
  height_(height),
  angle_(0),
  center_(center)
{
  if (width_ <= 0)
  {
    throw std::invalid_argument("Width must be more than zero");
  }
  if (height_ <= 0)
  {
    throw std::invalid_argument("Height must be more than zero");
  }
}

sherman::Rectangle::Rectangle(double width, double height, double angle, const sherman::point_t &center) :
  Rectangle(width, height, center)
{
  rotate(angle);
}

double sherman::Rectangle::getArea() const
{
  return width_ * height_;
}

sherman::rectangle_t sherman::Rectangle::getFrameRect() const
{
  const double sine = std::sin(angle_ * M_PI / 180);
  const double cosine = std::cos(angle_  * M_PI /180);
  const double width = width_ * std::abs(cosine) + height_ * std::abs(sine);
  const double height = width_ * std::abs(sine) + height_ * std::abs(cosine);
  return {width, height, center_};
}

void sherman::Rectangle::move(double changeX, double changeY)
{
  center_.x += changeX;
  center_.y += changeY;
}

void sherman::Rectangle::move(const point_t & newCenter)
{
  center_ = newCenter;
}

void sherman::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient must be more than zero");
  }
  width_ *= coefficient;
  height_ *= coefficient;
}

void sherman::Rectangle::rotate(double angle)
{
  angle_ += angle;
}

void sherman::Rectangle::printInformation() const
{
  std::cout << "Center of the rectangle is (" << center_.x << "," << center_.y << ")\n";
  std::cout << "Rectangle\'s width is " << width_ << '\n';
  std::cout << "Rectangle\'s height is " << height_ << '\n';
  std::cout << "Area of the rectangle is " << getArea() << '\n';
  std::cout << "Parameters of the frame rectangle: " << '\n';
  std::cout << "Width: " << getFrameRect().width << '\n';
  std::cout << "Height: " << getFrameRect().height << '\n';
  std::cout << "Center: (" << getFrameRect().pos.x << "," << getFrameRect().pos.y << ")\n";
}
