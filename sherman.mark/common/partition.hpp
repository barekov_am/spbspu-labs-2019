#ifndef COMMON_PARTITION_HPP
#define COMMON_PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace sherman
{
  Matrix<Shape> layer(const CompositeShape &);
  bool intersect(const Shape &, const Shape &);
}

#endif
