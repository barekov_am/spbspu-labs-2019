#include "tasks.hpp"
#include <fstream>
#include <memory>
#include <iostream>

const std::size_t INITIAL_SIZE = 1024;

void task2(const char *file)
{
  std::ifstream input(file);
  if (!input)
  {
    throw std::invalid_argument("File with such name does not exist");
  }

  std::size_t sizeOfArray = INITIAL_SIZE;
  typedef std::unique_ptr<char[], decltype(&free)> arrayOfChars;
  arrayOfChars array(static_cast<char *>(malloc(sizeOfArray)), &free);

  if (!array)
  {
    throw std::runtime_error("Can not allocate memory for array");
  }

  std::size_t index = 0;

  while (input)
  {
    input.read(&array[index], INITIAL_SIZE);
    index += input.gcount();
    if (input.gcount() == INITIAL_SIZE)
    {
      sizeOfArray += INITIAL_SIZE;
      arrayOfChars tempArray(static_cast<char *>(realloc(array.get(), sizeOfArray)), &free);
      if (!tempArray)
      {
        throw std::runtime_error("Can not allocate memory for temporary array");
      }
      array.release();
      std::swap(array, tempArray);
    }
    if (!input.eof() && input.fail())
    {
      throw std::runtime_error("Reading has failed");
    }
  }

  std::vector<char> vect(&array[0], &array[index]);

  for (char i : vect)
  {
    std::cout << i;
  }

}
