#include "tasks.hpp"
#include "functions.hpp"
#include <ctime>

void fillRandom(double *array, int size)
{
  srand(time(nullptr));
  for (int i = 0; i < size; ++i)
  {
    array[i] = (rand() % 21 - 10) / 10.0;
  }
}

void task4(const char *direction, const char *size)
{
  auto compare = getDirection<double>(direction);

  int vectSize = atoi(size);

  if (vectSize <= 0)
  {
    throw std::invalid_argument("Size must be more than zero");
  }

  std::vector<double> vect(vectSize);
  fillRandom(vect.data(), vectSize);
  print(vect);
  sort<accessByAt>(vect, compare);
  print(vect);
}
