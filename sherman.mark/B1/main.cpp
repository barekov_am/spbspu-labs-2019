#include <iostream>
#include "tasks.hpp"

int main(int argc, char *argv[])
{
  if ((argc < 2) || (argc > 4))
  {
    std::cerr << "Incorect number of parameters";
    return 1;
  }

  try
  {
    switch (atoi(argv[1]))
    {
      case 1:
        if (argc != 3)
        {
          std::cerr << "Incorrect number of parameters";
          return 1;
        }
        task1(argv[2]);
        break;

      case 2:
        if (argc != 3)
        {
          std::cerr << "Incorrect number of parameters";
          return 1;
        }
        task2(argv[2]);
        break;

      case 3:
        task3();
        break;

      case 4:
        if (argc != 4)
        {
          std::cerr << "Incorrect number of parameters";
          return 1;
        }
        task4(argv[2], argv[3]);
        break;

      default:
        std::cerr << "Invalid number of task";
        return 1;
    }
  }

  catch(const std::exception &err)
  {
    std::cerr << err.what();
    return 1;
  }

  return 0;
}
