#include "tasks.hpp"
#include "functions.hpp"
#include <forward_list>

void task1(const char *direction)
{
  auto compare = getDirection<int>(direction);

  std::vector<int> vectByBrackets;
  int currentElement = 0;
  while (std::cin && !(std::cin >> currentElement).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading the file caused an error");
    }
    vectByBrackets.push_back(currentElement);
  }
  std::vector<int> vectByAt(vectByBrackets);
  std::forward_list<int> list(vectByAt.begin(), vectByAt.end());

  sort<accessByBrackets>(vectByBrackets, compare);
  print(vectByBrackets);

  sort<accessByAt>(vectByAt, compare);
  print(vectByAt);

  sort<accessByIterator>(list, compare);
  print(list);
}
