#ifndef B1_FUNCTIONS_HPP
#define B1_FUNCTIONS_HPP

#include "access.hpp"
#include <functional>
#include <iostream>
#include <cstring>

template <typename ContainerType>
std::function<bool(ContainerType, ContainerType)> getDirection(const char *direction)
{
  if (!std::strcmp(direction, "ascending"))
  {
    return std::function<bool(ContainerType, ContainerType)>(std::greater<ContainerType>());
  }
  if (!std::strcmp(direction, "descending"))
  {
    return std::function<bool(ContainerType, ContainerType)>(std::less<ContainerType>());
  }
  throw std::invalid_argument("Invalid sorting direction");
}

template <template <typename Container> class Access, typename Container>
void sort(Container& container, std::function<bool(typename Container::value_type, typename Container::value_type)> compare)
{
  typedef class Access<Container> access;
  const auto begin = access::begin(container);
  const auto end = access::end(container);

  for (auto i = begin; i != end; ++i)
  {
    for (auto j = access::next(i); j != end; ++j)
    {
      if (compare(access::element(container, i), access::element(container, j)))
      {
        std::swap(access::element(container, i), access::element(container, j));
      }
    }
  }
}

template <typename Container>
void print(const Container &container)
{
  if (container.empty())
  {
    return;
  }
  for (const auto element: container)
  {
    std::cout << element << " ";
  }
  std::cout << '\n';
}

#endif
