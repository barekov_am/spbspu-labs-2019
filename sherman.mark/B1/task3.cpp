#include "tasks.hpp"
#include "functions.hpp"

void task3()
{
  std::vector<int> vect;
  int currentElement = -1;
  while (std::cin && !(std::cin >> currentElement).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading the file caused an error");
    }
    if (currentElement == 0)
    {
      break;
    }
    vect.push_back(currentElement);
  }

  if (vect.empty())
  {
    return;
  }

  if (currentElement != 0)
  {
    throw std::invalid_argument("The terminating character must be zero");
  }

  std::vector<int>::iterator it = vect.end() - 1;
  switch (*it)
  {
    case 1:
      for (it = vect.begin(); it != vect.end();)
      {
        it = (*it % 2 == 0) ? vect.erase(it--) : ++it;
      }
      break;
    case 2:
      for (it = vect.begin(); it != vect.end();)
      {
        it = (*it % 3 == 0) ? (vect.insert(++it, 3, 1) + 2) : ++it;
      }
      break;
    default:
      break;
  }
  print(vect);
}
