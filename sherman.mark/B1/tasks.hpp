#ifndef B1_TASKS
#define B1_TASKS

#include <vector>

void task1(const char *);
void task2(const char *);
void task3();
void task4(const char *, const char *);
#endif
