#include "tasks.hpp"
#include "phoneBook.hpp"
#include "userInterface.hpp"

int task1()
{
  PhoneBook phoneBook;
  UserInterface phoneBookInterface(phoneBook);
  std::string line;
  std::stringstream string;
  while (std::getline(std::cin, line))
  {
    string.clear();
    string.str(line);
    if (std::cin.fail())
    {
      return 2;
    }
    if (std::cin.eof())
    {
      break;
    }
    phoneBookInterface.chooseCommand(string);
  }
  return 0;
}
