#ifndef B3_USERINTERFACE_HPP
#define B3_USERINTERFACE_HPP

#include "phoneBook.hpp"

class UserInterface
{
  public:
    UserInterface(PhoneBook &);
    ~UserInterface() = default;
    void chooseCommand(std::istream &);
    void add(std::istream &);
    void store(std::istream &);
    void insert(std::istream &);
    void delet(std::istream &);
    void show(std::istream &);
    void move(std::istream &);
    void makeHumanReadable(std::string &);
  private:
    PhoneBook &phoneBook_;
    UserInterface() = delete;
};

#endif
