#ifndef B3_PHONEBOOK_HPP
#define B3_PHONEBOOK_HPP

#include <utility>
#include <string>
#include <list>
#include <sstream>
#include <iostream>

class PhoneBook
{
  public:

    typedef std::pair<std::string, std::string> record;
    typedef std::list<record>::iterator iterator;
    typedef std::list<std::pair<iterator, std::string>> bookmark;
    typedef bookmark::iterator bookmarkIter;

    PhoneBook();
    ~PhoneBook() = default;
    record &getRecord() const;
    void addBookMark(const std::string &);
    bool makeBookMarkCurrent(const std::string &);
    void store(const std::string &, const std::string &);
    void nextRecord();
    void previousRecord();
    void insertBefore(const std::string &, const std::string &, const std::string &);
    void insertAfter(const std::string &, const std::string &, const std::string &);
    void deleteRecord(const std::string &);
    void replaceRecord(const std::string &, const std::string &);
    void insertAtTheEnd(const std::string &, const std::string &);
    void move(const std::string &, const std::string &);

    bool isEmpty() const;

  private:
    std::list<record> records_;
    bookmark bookmarks_;
    bookmarkIter currentBookmark_;
};
#endif
