#include "phoneBook.hpp"

PhoneBook::PhoneBook()
{
  bookmarks_.emplace_back(records_.end(), "current");
  currentBookmark_ = bookmarks_.begin();
}

PhoneBook::record &PhoneBook::getRecord() const
{
  return *(currentBookmark_->first);
}

void PhoneBook::addBookMark(const std::string &name)
{
  bookmarks_.emplace_back(currentBookmark_->first, name);
}

bool PhoneBook::makeBookMarkCurrent(const std::string &markName)
{
  bookmarkIter i = bookmarks_.begin();
  for (; i != bookmarks_.end(); ++i)
  {
    if (i->second == markName)
    {
      currentBookmark_ = i;
      return true;
    }
  }
  std::cout << "<INVALID BOOKMARK>\n";
  return false;
}

void PhoneBook::store(const std::string &markName, const std::string &newName)
{
  if (!makeBookMarkCurrent(markName))
  {
    return;
  }
  addBookMark(newName);
}

void PhoneBook::nextRecord()
{
  std::size_t distance = std::distance(records_.begin(), currentBookmark_->first);
  if (distance < records_.size())
  {
    currentBookmark_->first++;
  }
  else
  {
    return;
  }
}

void PhoneBook::previousRecord()
{
  std::size_t distance = std::distance(records_.begin(), currentBookmark_->first);
  if (distance > 0)
  {
    currentBookmark_->first--;
  }
  else
  {
    return;
  }
}

void PhoneBook::insertBefore(const std::string &name, const std::string &phoneNumber, const std::string &markName = "current")
{
  if (!makeBookMarkCurrent(markName))
  {
    return;
  }
  if (currentBookmark_->first == records_.end())
  {
    records_.emplace_back(name, phoneNumber);
    currentBookmark_->first--;
  }
  else
  {
    records_.emplace(currentBookmark_->first, name, phoneNumber);
  }
}

void PhoneBook::insertAfter(const std::string &name, const std::string &phoneNumber, const std::string &markName = "current")
{
  if (!makeBookMarkCurrent(markName))
  {
    return;
  }
  if (currentBookmark_->first == records_.end())
  {
    records_.emplace_back(name, phoneNumber);
    currentBookmark_->first--;
  }
  else
  {
    records_.emplace(std::next(currentBookmark_->first), name, phoneNumber);
  }
}

void PhoneBook::deleteRecord(const std::string &markName = "current")
{
  if (!makeBookMarkCurrent(markName))
  {
    return;
  }

  iterator ourRecord = currentBookmark_->first;

  for (bookmarkIter i = bookmarks_.begin(); i != bookmarks_.end(); ++i)
  {
    if (i->first == ourRecord)
    {
      if ((i->first == std::prev(records_.end())) && (records_.size() > 1))
      {
        i->first--;
      }
      else
      {
        i->first++;
      }
    }
  }
  records_.erase(ourRecord);
}

void PhoneBook::replaceRecord(const std::string &name, const std::string &phoneNumber)
{
  if ((currentBookmark_->first == records_.end()) && (records_.size() > 1))
  {
    deleteRecord();
    insertAtTheEnd(name, phoneNumber);
  }
  else
  {
    deleteRecord();
    insertBefore(name, phoneNumber);
  }
}

void PhoneBook::insertAtTheEnd(const std::string &name, const std::string &phoneNumber)
{
  records_.emplace_back(name, phoneNumber);
  if (currentBookmark_->first == records_.end())
  {
    currentBookmark_->first--;
  }
}

void PhoneBook::move(const std::string &steps, const std::string &markName = "current")
{
  if (!makeBookMarkCurrent(markName))
  {
    return;
  }
  if (steps == "first")
  {
    currentBookmark_->first = records_.begin();
    return;
  }
  if (steps == "last")
  {
    currentBookmark_->first = std::prev(records_.end());
    return;
  }

  std::istringstream string(steps);
  int distance = 0;

  string >> distance;

  if (string.fail())
  {
    std::cout << "<INVALID STEP>\n";
  }

  int minDistance = distance + std::distance(records_.begin(), currentBookmark_->first);
  unsigned int maxDistance = static_cast<unsigned int>(distance + (+std::distance(records_.begin(), currentBookmark_->first)));
  if ((minDistance < 0) || (maxDistance >= records_.size()))
  {
    return;
  }

  std::advance(currentBookmark_->first, distance);
}

bool PhoneBook::isEmpty() const
{
  return records_.empty();
}
