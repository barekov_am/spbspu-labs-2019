#include <algorithm>
#include <iterator>
#include <iostream>
#include <cmath>
#include "tasks.hpp"

void task1()
{
  std::transform(std::istream_iterator<double>(std::cin), std::istream_iterator<double >(),
                 std::ostream_iterator<double >(std::cout, "\n"), std::bind1st(std::multiplies<double>(), M_PI));
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Reading caused an error");
  }
}
