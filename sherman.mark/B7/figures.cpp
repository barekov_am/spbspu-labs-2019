#include <iostream>
#include "figures.hpp"

Circle::Circle(int x, int y) :
  Shape::Shape(x, y)
{
}

void Circle::draw() const
{
  std::cout << "CIRCLE (" << x_ << "; " << y_ <<")\n";
}

Triangle::Triangle(int x, int y) :
  Shape::Shape(x, y)
{
}

void Triangle::draw() const
{
  std::cout << "TRIANGLE (" << x_ << "; " << y_ <<")\n";
}

Square::Square(int x, int y) :
  Shape::Shape(x, y)
{
}

void Square::draw() const
{
  std::cout << "SQUARE (" << x_ << "; " << y_ <<")\n";
}
