#include <iostream>
#include <algorithm>
#include "tasks.hpp"
#include "functions.hpp"

void task2()
{
  ourVector vector = readData(std::cin);

  std::cout << "Original:\n";
  drawShapes(vector);

  std::cout << "Left-Right:\n";
  std::sort(vector.begin(), vector.end(), [](const std::shared_ptr<Shape> &left, const std::shared_ptr<Shape> &right)
  {
      return left->isMoreLeft(right);
  });
  drawShapes(vector);

  std::cout << "Right-Left:\n";
  std::sort(vector.begin(), vector.end(), [](const std::shared_ptr<Shape> &left, const std::shared_ptr<Shape> &right)
  {
      return !left->isMoreLeft(right);
  });
  drawShapes(vector);

  std::cout << "Top-Bottom:\n";
  std::sort(vector.begin(), vector.end(), [](const std::shared_ptr<Shape> &left, const std::shared_ptr<Shape> &right)
  {
      return left->isUpper(right);
  });
  drawShapes(vector);

  std::cout << "Bottom-Top:\n";
  std::sort(vector.begin(), vector.end(), [](const std::shared_ptr<Shape> &left, const std::shared_ptr<Shape> &right)
  {
      return !left->isUpper(right);
  });
  drawShapes(vector);
}
