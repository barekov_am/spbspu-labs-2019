#include <iostream>
#include <algorithm>
#include "functions.hpp"
#include "figures.hpp"

ourVector readData(std::istream &input)
{
  ourVector vector;
  std::string line;
  while (std::getline(input >> std::ws, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading caused an error");
    }
    
    if (line.empty())
    {
      continue;
    }

    std::size_t symbol = line.find_first_of('E');
    std::size_t openingParenthesis = line.find_first_of('(');
    std::size_t closingParenthesis = line.find_first_of(')');
    std::size_t semicolon = line.find_first_of(';');

    if ((openingParenthesis == std::string::npos) || (closingParenthesis == std::string::npos)
        || (semicolon == std::string::npos) || (symbol == std::string::npos))
    {
      throw std::invalid_argument("Invalid input\n");
    }

    std::string nameOfShape = line.substr(0, symbol + 1);
    int x = std::stoi(line.substr(openingParenthesis + 1, semicolon - openingParenthesis - 1));
    int y = std::stoi(line.substr(semicolon + 1, closingParenthesis - semicolon - 1));

    if (nameOfShape == "CIRCLE")
    {
      vector.push_back(std::make_shared<Circle>(x, y));
    }
    else if (nameOfShape == "TRIANGLE")
    {
      vector.push_back(std::make_shared<Triangle>(x, y));
    }
    else if (nameOfShape == "SQUARE")
    {
      vector.push_back(std::make_shared<Square>(x, y));
    }
    else
    {
      throw std::invalid_argument("Invalid name of shape\n");
    }
  }
  return vector;
}

void drawShapes(const ourVector &vector)
{
  std::for_each(vector.begin(), vector.end(), [](const std::shared_ptr<Shape> &shape)
  {
      return shape->draw();
  });
}
