#ifndef B7_SHAPE_HPP
#define B7_SHAPE_HPP

#include <cstddef>
#include <memory>

class Shape
{
  public:
   Shape(int, int);
   virtual ~Shape() = default;

   bool isMoreLeft(const std::shared_ptr<Shape> &) const;
   bool isUpper(const std::shared_ptr<Shape> &) const;
   virtual void draw() const = 0;
 protected:
   int x_, y_;
};

#endif
