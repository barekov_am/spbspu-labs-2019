#ifndef B7_FUNCTIONS_HPP
#define B7_FUNCTIONS_HPP

#include <vector>
#include "shape.hpp"

using ourVector = std::vector<std::shared_ptr<Shape>>;

ourVector readData(std::istream &);
void drawShapes(const ourVector &);

#endif
