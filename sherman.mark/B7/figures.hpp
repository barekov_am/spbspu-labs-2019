#ifndef B7_FIGURES_HPP
#define B7_FIGURES_HPP

#include "shape.hpp"

class Circle : public Shape
{
  public:
    Circle(int, int);
    void draw() const override;
};

class Triangle : public Shape
{
  public:
    Triangle(int, int);
    void draw() const override;
};

class Square : public Shape
{
  public:
    Square(int, int);
    void draw() const override;
};
#endif
