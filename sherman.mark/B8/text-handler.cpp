#include "text-handler.hpp"

#include <iostream>
#include <algorithm>

const std::size_t MAX_ELEMENT_WIDTH = 20u;
const std::size_t MAX_HYPHENS_NUMBER = 3u;

TextHandler::TextHandler(std::size_t lineWidth) :
  lineWidth_(lineWidth),
  text_(),
  formattedText_()
{
}

void TextHandler::readText()
{
  while (std::cin >> std::ws)
  {
    char ch = (std::cin >> std::ws).get();
    if (std::isalpha(ch))
    {
      std::cin.unget();
      readWord();
    }
    else if (((ch == '-' || ch == '+') && std::isdigit(std::cin.peek())) || std::isdigit(ch))
    {
      std::cin.unget();
      readNumber();
    }
    else if (std::ispunct(ch))
    {
      if (ch == '-')
      {
        std::cin.unget();
        readHyphen();
      }
      else
      {
        std::cin.unget();
        readPunctuation();
      }
    }
  }
}

void TextHandler::formatText()
{
  if (text_.empty())
  {
    return;
  }
  std::size_t numberOfElement = 0;
  std::size_t numberOfLine = 0;
  std::vector<element_t> string;
  while (numberOfElement < text_.size())
  {
    if (string.empty())
    {
      if ((text_.at(numberOfElement).typeOfElement == element_t::PUNCTUATION) || (text_.at(numberOfElement).typeOfElement == element_t::HYPHEN))
      {
        if ((text_.at(numberOfElement).typeOfElement == element_t::PUNCTUATION) && (getStringWidth(formattedText_.at(numberOfLine - 1)) < lineWidth_))
        {
          formattedText_.at(numberOfLine - 1).push_back(text_.at(numberOfElement));
          ++numberOfElement;
        }
        else
        {
          string.push_back(text_.at(numberOfElement - 1));
          if (text_.at(numberOfElement).typeOfElement == element_t::HYPHEN)
          {
            string.push_back({element_t::SPACE, " "});
          }
          string.push_back(text_.at(numberOfElement));
          if (formattedText_.at(numberOfLine - 1).back().typeOfElement == element_t::SPACE)
          {
            formattedText_.at(numberOfLine - 1).pop_back();
          }
          ++numberOfElement;
        }
      }
      else
      {
        string.push_back(text_.at(numberOfElement));
        ++numberOfElement;
      }
      continue;
    }
    std::size_t stringWidth = getStringWidth(string) + text_.at(numberOfElement).string_.size() + 1;
    bool exit = false;
    if (stringWidth <= lineWidth_)
    {
      if (text_.at(numberOfElement).typeOfElement == element_t::PUNCTUATION)
      {
        string.push_back(text_.at(numberOfElement));
        ++numberOfElement;
      }
      else
      {
        string.push_back({element_t::SPACE, " "});
        string.push_back(text_.at(numberOfElement));
        ++numberOfElement;
        if (numberOfElement < text_.size())
        {
          if ((text_.at(numberOfElement).typeOfElement == element_t::HYPHEN) && (stringWidth + text_.at(numberOfElement).string_.size() + 1 > lineWidth_))
          {
            exit = true;
            string.pop_back();
            string.pop_back();
            --numberOfElement;
          }
          else if ((text_.at(numberOfElement).typeOfElement == element_t::PUNCTUATION) && (stringWidth + text_.at(numberOfElement).string_.size() > lineWidth_))
          {
           exit = true;
           string.pop_back();
           string.pop_back();
           --numberOfElement;
         }
        }
       }
       if (!exit)
       {
         continue;
       }
    }
    formattedText_.push_back(string);
    string.clear();
    ++numberOfLine;
  }
  formattedText_.push_back(string);
}

void TextHandler::printText()
{
  std::for_each(formattedText_.begin(), formattedText_.end(), [](const std::vector<element_t> &string)
  {
      std::for_each(string.begin(), string.end(), [](const element_t &element)
      {
          std::cout << element.string_;
      });
      std::cout << '\n';
  });
}

void TextHandler::readWord()
{
  std::string word;
  while (std::cin)
  {
    if ((std::cin.peek() == '-') || (std::isalpha(std::cin.peek())))
    {
      word += std::cin.get();
      if ((word.back() == '-') && (std::cin.peek() == '-'))
      {
        throw std::invalid_argument("Invalid word\n");
      }
    }
    else
    {
      break;
    }
  }
  if (word.size() > MAX_ELEMENT_WIDTH)
  {
    throw std::invalid_argument("Invalid width of the word\n");
  }
 text_.push_back({element_t::WORD, word});
}

void TextHandler::readNumber()
{
  std::string number;
  bool isDot = false;
  bool isSign = false;
  while (std::cin)
  {
    if ((std::cin.peek() == '-') || (std::cin.peek() == '+'))
    {
      number += std::cin.get();
      if (isSign)
      {
        throw std::invalid_argument("Invalid input of the number\n");
      }
      isSign = true;
    }
    else if (std::isdigit(std::cin.peek()))
    {
      number += std::cin.get();
    }
    else if (std::cin.peek() == '.')
    {
      number += std::cin.get();
      if (isDot)
      {
        throw std::invalid_argument("Invalid input of the number\n");
      }
      isDot = true;
    }
    else
    {
      break;
    }
  }
  if (number.size() > MAX_ELEMENT_WIDTH)
  {
    throw std::invalid_argument("Invalid width of the number\n");
  }
  text_.push_back({element_t::NUMBER, number});
}

void TextHandler::readHyphen()
{
  if (text_.empty())
  {
    throw std::invalid_argument("Invalid input of the text\n");
  }
  std::string hyphen;
  std::size_t numberOfHyphens = 0;
  while (std::cin)
  {
    if (std::cin.peek() == '-')
    {
      ++numberOfHyphens;
      hyphen += std::cin.get();
      if (numberOfHyphens > MAX_HYPHENS_NUMBER)
      {
        throw std::invalid_argument("Invalid input of dash\n");
      }
      if ((numberOfHyphens == 1) && (text_.back().typeOfElement == element_t::PUNCTUATION) && (text_.back().string_ != ","))
      {
        throw std::invalid_argument("Invalid input of hyphen\n");
      }
    }
    else
    {
      break;
    }
  }
  text_.push_back({element_t::HYPHEN, hyphen});
}

void TextHandler::readPunctuation()
{
  if (text_.empty())
  {
    throw std::invalid_argument("Invalid input of the text\n");
  }
  if (text_.back().typeOfElement == element_t::HYPHEN)
  {
    throw std::invalid_argument("Invalid position of punctuation after hyphen\n");
  }
  if (text_.back().typeOfElement == element_t::PUNCTUATION)
  {
    throw std::invalid_argument("Invalid position of punctuation after punctuation\n");
  }
  std::string ch;
  ch += std::cin.get();
  text_.push_back({element_t::PUNCTUATION, ch});
}

std::size_t TextHandler::getStringWidth(std::vector<TextHandler::element_t> &string)
{
  std::size_t width = 0;
  std::for_each(string.begin(), string.end(), [&](const element_t &element)
  {
    width += element.string_.size();
  });
  return width;
}
