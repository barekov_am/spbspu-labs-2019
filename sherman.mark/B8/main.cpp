#include <iostream>
#include "text-handler.hpp"

const std::size_t MINIMUM_WIDTH_OF_LINE = 25u;
const std::size_t DEFAULT_WIDTH_OF_LINE = 40u;

int main(int argc, char *argv[])
{
  try
  {
    if ((argc == 2) || (argc > 3))
    {
      std::cerr << "Incorrect number of parameters\n";
      return 1;
    }
    std::size_t lineWidth = DEFAULT_WIDTH_OF_LINE;
    if (argc == 3)
    {
      if (std::string(argv[1]) != "--line-width")
      {
        std::cerr << "Incorrect input arguments\n";
        return 1;
      }
      lineWidth = std::stoi(argv[2]);
      if (lineWidth < MINIMUM_WIDTH_OF_LINE)
      {
        std::cerr << "Incorrect width of line";
        return 1;
      }
    }
    TextHandler text(lineWidth);
    text.readText();
    text.formatText();
    text.printText();
  }

  catch (const std::exception &err)
  {
    std::cerr << err.what();
    return 1;
  }
  return 0;
}
