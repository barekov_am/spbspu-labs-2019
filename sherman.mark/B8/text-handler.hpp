#ifndef B8_TEXT_HANDLER_HPP
#define B8_TEXT_HANDLER_HPP

#include <string>
#include <vector>

class TextHandler
{
public:
  struct element_t
  {
    enum
    {
      WORD,
      SPACE,
      NUMBER,
      HYPHEN,
      PUNCTUATION
    } typeOfElement;
    std::string string_;
  };

  TextHandler(std::size_t);

  void readText();
  void formatText();
  void printText();

private:
  std::size_t lineWidth_;
  std::vector<element_t> text_;
  std::vector<std::vector<element_t>> formattedText_;

  void readWord();
  void readNumber();
  void readHyphen();
  void readPunctuation();

  std::size_t getStringWidth(std::vector<element_t> &);
};
#endif
