#include <iostream>
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

int main()
{
  sherman::Rectangle rectan1({5, 4, {1, 5}});
  sherman::Rectangle rectan2({8, 7, {4, -6}});
  sherman::Circle circle1({6, {10, 10}});
  sherman::Circle circle2({9, {-7, 22}});

  sherman::CompositeShape compositeShape(std::make_shared<sherman::Rectangle> (rectan1));
  compositeShape.add(std::make_shared<sherman::Rectangle> (rectan2));
  compositeShape.add(std::make_shared<sherman::Circle> (circle1));
  compositeShape.add(std::make_shared<sherman::Circle> (circle2));

  std::cout << "Information about composite shape\n";
  compositeShape.printInformation();
  std::cout << "Moving to the point (1, 1)\n";
  compositeShape.move({1, 1});
  compositeShape.printInformation();
  std::cout << "Moving along abscissa for 5 and along ordinate for 6\n";
  compositeShape.move(5, 6);
  compositeShape.printInformation();
  std::cout << "Zoom scale in 0.25 times\n";
  compositeShape.scale(0.25);
  compositeShape.printInformation();
  std::cout << "Rotating composite shape by 30 degrees\n";
  compositeShape.rotate(30);
  compositeShape.printInformation();
  std::cout << "Delete shapes numbered 2 and 3\n";
  compositeShape.remove(3);
  compositeShape.remove(2);
  compositeShape.printInformation();
  std::cout << "Printing information about shape numbered 1\n";
  std::shared_ptr<sherman::Shape> selectedShape = compositeShape[1];
  selectedShape->printInformation();

  return 0;
}
