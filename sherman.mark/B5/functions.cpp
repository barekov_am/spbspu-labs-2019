#include <stdexcept>
#include <algorithm>
#include <iostream>
#include <sstream>
#include "functions.hpp"

int getLength(const Point &lhs, const Point &rhs)
{
  return (lhs.x - rhs.x) * (lhs.x - rhs.x) + (lhs.y - rhs.y) * (lhs.y - rhs.y);
}

bool isRectangle(const Shape &shape)
{
  return (getLength(shape[0], shape[2]) == getLength(shape[1], shape[3]));
}

bool isSquare(const Shape &shape)
{
  return (isRectangle(shape) && (getLength(shape[0], shape[1]) == getLength(shape[1], shape[2])));
}

std::vector<Shape> readData(std::istream &input)
{
  std::string line;
  std::vector<Shape> shapes;
  while (std::getline(input >> std::ws, line))
  {
    if (input.fail())
    {
      throw std::ios_base::failure("Reading caused an error\n");
    }

    if (line.empty())
    {
      continue;
    }

    std::stringstream string(line);
    int vertexes;
    string >> vertexes;

    if (vertexes < VERTICES_OF_TRIANGLE)
    {
      throw std::invalid_argument("Invalid number of vertexes\n");
    }

    Shape shape;
    std::size_t openingParenthesis;
    std::size_t closingParenthesis;
    std::size_t semicolon;
    for (int i = 0; i < vertexes; ++i)
    {
      openingParenthesis = line.find_first_of('(');
      closingParenthesis = line.find_first_of(')');
      semicolon = line.find_first_of(';');

      if ((openingParenthesis == std::string::npos) || (closingParenthesis == std::string::npos)
          || (semicolon == std::string::npos))
      {
        throw std::invalid_argument("Invalid input of a dot\n");
      }

      int x = std::stoi(line.substr(openingParenthesis + 1, semicolon - openingParenthesis - 1));
      int y = std::stoi(line.substr(semicolon + 1, closingParenthesis - semicolon - 1));

      shape.push_back({x, y});
      line.erase(0, closingParenthesis + 1);
    }
    shapes.push_back(shape);
  }
  return shapes;
}

void printPoints(const Shape &shape)
{
  for (const auto &point: shape)
  {
    std::cout << " (" << point.x << "; " << point.y << ") ";
  }
}
