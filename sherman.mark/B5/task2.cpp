#include <vector>
#include <algorithm>
#include "tasks.hpp"
#include "functions.hpp"

void task2()
{
  std::vector<Shape> shapes = readData(std::cin);
  int vertices = 0;
  int numberOfTriangles = 0;
  int numberOfRectangles = 0;
  int numberOfSquares = 0;
  std::for_each(shapes.begin(), shapes.end(), [&](const Shape &shape)
  {
    vertices += shape.size();
    if (shape.size() == VERTICES_OF_TRIANGLE)
    {
      ++numberOfTriangles;
    }
    else if (shape.size() == VERTICES_OF_RECTANGLE)
    {
      if (isRectangle(shape))
      {
        ++numberOfRectangles;
        if (isSquare(shape))
        {
          ++numberOfSquares;
        }
      }
    }
  });

  shapes.erase(std::remove_if(shapes.begin(), shapes.end(), [&](const Shape &shape)
  {
      return shape.size() == VERTICES_OF_PENTAGON;
  }), shapes.end());

  Shape points(shapes.size());

  std::transform(shapes.begin(), shapes.end(), points.begin(), [&](const Shape &shape)
  {
      return shape[0];
  });

  std::sort(shapes.begin(), shapes.end(), [&](const Shape &lhs, const Shape &rhs)
  {
    if (lhs.size() < rhs.size())
    {
      return true;
    }
    if ((lhs.size() == VERTICES_OF_RECTANGLE) && (rhs.size() == VERTICES_OF_RECTANGLE))
    {
      if (isSquare(lhs))
      {
        return !isSquare(rhs);
      }
    }

    return false;
  });

  std::cout << "Vertices: " << vertices << '\n';
  std::cout << "Triangles: " << numberOfTriangles << '\n';
  std::cout << "Squares: " << numberOfSquares << '\n';
  std::cout << "Rectangles: " << numberOfRectangles << '\n';

  std::cout << "Points: ";
  printPoints(points);
  std::cout << '\n';

  std::cout << "Shapes:\n";

 for (auto shape = shapes.begin(); shape != shapes.end(); ++shape)
  {
    std::cout << shape->size() << ' ';
    printPoints(*shape);
    std::cout << '\n';
  }
}
