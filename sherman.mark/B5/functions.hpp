#ifndef B5_FUNCTIONS_HPP
#define B5_FUNCTIONS_HPP

#include <string>
#include "shape.hpp"

const int VERTICES_OF_TRIANGLE = 3;
const int VERTICES_OF_RECTANGLE = 4;
const int VERTICES_OF_PENTAGON = 5;

int getLength(const Point &, const Point &);
bool isRectangle(const Shape &);
bool isSquare(const Shape &);
std::vector<Shape> readData(std::istream &);
void printPoints(const Shape &);

#endif
