#include <set>
#include <string>
#include "tasks.hpp"

void task1()
{
  std::set<std::string> words;
  std::string currentWord;

  while (std::cin >> currentWord)
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading caused an error");
    }
    words.insert(currentWord);
  }

  if (!words.empty())
  {
    for (const auto &i: words)
    {
      std::cout << i << '\n';
    }
  }
}
