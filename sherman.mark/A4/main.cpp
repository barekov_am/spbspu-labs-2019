#include <iostream>
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "matrix.hpp"
#include "partition.hpp"

int main()
{
  sherman::Rectangle rectan1({5, 4, {0, 0}});
  std::cout << "-----Making rectangle-----\n";
  rectan1.printInformation();
  std::cout << "-----Moving the rectangle-----\n";
  std::cout << "+++++Moving to the point (3,4)+++++\n";
  rectan1.move({3, 4});
  rectan1.printInformation();
  std::cout << "+++++Moving along abscissa for 1 and along ordinate for 2+++++\n";
  rectan1.move(1, 2);
  rectan1.printInformation();
  std::cout << "-----Zoom scale in three times-----\n";
  rectan1.scale(3);
  rectan1.printInformation();
  std::cout << "-----Rotated rectangle by 60 degrees-----\n";
  rectan1.rotate(60);
  rectan1.printInformation();
  std::cout << "-----First rectangle is done-----\n";
  std::cout << '\n';

  sherman::Circle circle1({6, {2, 0}});
  std::cout << "-----Making circle-----\n";
  circle1.printInformation();
  std::cout << "-----Moving the circle-----\n";
  std::cout << "+++++Moving to the point (45,9)+++++\n";
  circle1.move({45, 9});
  circle1.printInformation();
  std::cout << "+++++Moving along abscissa for 1 and along ordinate for -6+++++\n";
  circle1.move(1, -6);
  circle1.printInformation();
  std::cout << "-----Zoom scale in five times-----\n";
  circle1.scale(5);
  circle1.printInformation();
  std::cout << "-----Rotated circle by 60 degrees-----\n";
  circle1.rotate(60);
  circle1.printInformation();
  std::cout << "-----First circle is done-----\n";
  std::cout << '\n';

  sherman::Rectangle rectan2({5, 7, {0, 1}});
  sherman::Circle circle2({10, {1, 0}});

  sherman::CompositeShape compositeShape(std::make_shared<sherman::Rectangle>(rectan1));
  compositeShape.add(std::make_shared<sherman::Rectangle> (rectan2));
  compositeShape.add(std::make_shared<sherman::Circle> (circle1));
  compositeShape.add(std::make_shared<sherman::Circle> (circle2));

  std::cout << "Information about composite shape\n";
  compositeShape.printInformation();
  std::cout << "Moving to the point (1, 1)\n";
  compositeShape.move({1, 1});
  compositeShape.printInformation();
  std::cout << "Moving along abscissa for 5 and along ordinate for 6\n";
  compositeShape.move(5, 6);
  compositeShape.printInformation();
  std::cout << "Zoom scale in 0.25 times\n";
  compositeShape.scale(0.25);
  compositeShape.printInformation();
  std::cout << "Rotating composite shape by 30 degrees\n";
  compositeShape.rotate(30);
  compositeShape.printInformation();
  std::cout << "Delete shapes numbered 2 and 3\n";
  compositeShape.remove(3);
  compositeShape.remove(2);
  compositeShape.printInformation();
  std::cout << "Printing information about shape numbered 1\n";
  std::shared_ptr<sherman::Shape> selectedShape = compositeShape[0];
  selectedShape->printInformation();

  compositeShape.add(std::make_shared<sherman::Circle> (circle1));
  compositeShape.add(std::make_shared<sherman::Circle> (circle2));

  sherman::Matrix<sherman::Shape> matrix = sherman::layer(compositeShape);

  for (std::size_t i = 0; i < matrix.getRows(); ++i)
  {
    std::cout << "-----Current layer----- " << i + 1 << '\n';
    std::cout << "Shapes on this layer:\n";
    for (std::size_t j = 0; j < matrix[i].size(); ++j)
    {
      std::cout << j + 1 << '\n';
      std::cout << "Area of the shape " << matrix[i][j]->getArea() << '\n';
      const sherman::point_t centerOfTheShape = matrix[i][j]->getFrameRect().pos;
      std::cout << "Center of the shape (" << centerOfTheShape.x << "," << centerOfTheShape.y << ")\n";
    }
  }

  return 0;
}
