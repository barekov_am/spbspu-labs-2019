#include "matrix.hpp"

#include <stdexcept>
#include <utility>
#include <iostream>

stebnevsky::Matrix::Matrix():
  m_lines(0),
  m_columns(0)
{
}

stebnevsky::Matrix::Matrix(const Matrix & matrix):
  m_lines(matrix.m_lines),
  m_columns(matrix.m_columns),
  m_array_shapes(std::make_unique<shape_ptr[]>(matrix.m_lines * matrix.m_columns))
{
  for (size_t i = 0; i < m_lines * m_columns; i++)
  {
    m_array_shapes[i] = matrix.m_array_shapes[i];
  }
}

stebnevsky::Matrix::Matrix(Matrix && matrix):
  m_lines(matrix.m_lines),
  m_columns(matrix.m_columns),
  m_array_shapes(std::move(matrix.m_array_shapes))
{
}

stebnevsky::Matrix &stebnevsky::Matrix::operator =(const Matrix & matrix)
{
  if (this != & matrix)
  {
    m_lines = matrix.m_lines;
    m_columns = matrix.m_columns;
    array_ptr temp_array(std::make_unique<shape_ptr[]> (matrix.m_lines * matrix.m_columns));
    for (size_t i = 0; i < (m_lines * m_columns); i++)
    {
      temp_array[i] = matrix.m_array_shapes[i];
    }
    m_array_shapes.swap(temp_array);
  }
  return *this;
}

stebnevsky::Matrix &stebnevsky::Matrix::operator =(Matrix && matrix)
{
  if (this != & matrix)
  {
    m_lines = matrix.m_lines;
    m_columns = matrix.m_columns;
    m_array_shapes = std::move(matrix.m_array_shapes);
  }
  return *this;
}

stebnevsky::array_ptr stebnevsky::Matrix::operator [](size_t index) const
{
  if (index >= m_lines)
  {
    throw std::out_of_range("Index out of range");
  }

  array_ptr temp_array(std::make_unique<shape_ptr[]>(m_columns));
  for (size_t i = 0; i < m_columns; i++)
  {
    temp_array[i] = m_array_shapes[index * m_columns + i];
  }
  return temp_array;
}

bool stebnevsky::Matrix::operator ==(const Matrix & matrix) const
{
  if ((matrix.m_lines != m_lines) || (matrix.m_columns != m_columns))
  {
    return false;
  }

  for (size_t i = 0; i < (m_lines * m_columns); i++)
  {
    if (matrix.m_array_shapes[i] != m_array_shapes[i])
    {
      return false;
    }
  }

  return true;
}

bool stebnevsky::Matrix::operator !=(const Matrix & matrix) const
{
  return !(*this == matrix);
}

void stebnevsky::Matrix::add(const shape_ptr shape, size_t line, size_t column)
{
  size_t new_lines = (m_lines == line) ? (m_lines + 1) : (m_lines);
  size_t new_columns = (m_columns == column) ? (m_columns + 1) : (m_columns);

  array_ptr temp(std::make_unique<shape_ptr[]> (new_lines * new_columns));
  for (size_t i = 0; i < new_lines; i++)
  {
    for (size_t j = 0; j < new_columns; j++)
    {
      if ((i == m_lines) || (j == m_columns))
      {
        temp[i * new_columns + j] = nullptr;
      }
      else
      {
        temp[i * new_columns + j] = m_array_shapes[i * m_columns + j];
      }
    }
  }
  temp[line * new_columns + column] = shape;
  m_array_shapes.swap(temp);
  m_lines = new_lines;
  m_columns = new_columns;
}

size_t stebnevsky::Matrix::getLines() const
{
  return m_lines;
}

size_t stebnevsky::Matrix::getColumns() const
{
  return m_columns;
}

void stebnevsky::Matrix::printInfo() const
{
  std::cout << "    MATRIX     \n";
  std::cout << "Value of lines: " << m_lines << "\n";
  std::cout << "Value of columns: " << m_columns << "\n\n";

  for (size_t i = 0; i < m_lines; i++)
  {
    for (size_t j = 0; j < m_columns; j++)
    {
      if (m_array_shapes[i * m_columns + j] != nullptr)
      {
        std::cout << "Layer number: " << i << ", Figure number: " << j << "\n";
        std::cout << "Center: " << ((m_array_shapes[i * m_columns + j])->getCenter().x) << ", "
            <<((m_array_shapes[i * m_columns + j])->getCenter().y) << "\n";
      }
    }
  }
}
