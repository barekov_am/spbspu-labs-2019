#include "partition.hpp"
#include <cmath>

bool stebnevsky::intersect(const rectangle_t & first_shape, const rectangle_t & second_shape)
{
  bool first_condition = (fabs(first_shape.pos.x - second_shape.pos.x) < (first_shape.width + second_shape.width) / 2);
  bool second_condition = (fabs(first_shape.pos.y - second_shape.pos.y) < (first_shape.height + second_shape.height) / 2);

  return first_condition && second_condition;
}

stebnevsky::Matrix stebnevsky::part(const array_ptr & shapes, size_t size)
{
  Matrix temp_matrix;

  for (size_t i = 0; i < size; i++)
  {
    size_t lines = 0;
    size_t column = 0;

    for (size_t j = 0; j < temp_matrix.getLines(); j++)
    {

      for (size_t k = 0; k < temp_matrix.getColumns(); k++)
      {
        if (temp_matrix[j][k] == nullptr)
        {
          lines = j;
          column = k;
          break;
        }

        if (intersect(shapes[i]->getFrameRect(), temp_matrix[j][k]->getFrameRect()))
        {
          lines = j + 1;
          column = 0;
          break;
        }
        else
        {
          lines = j;
          column = k + 1;
        }
      }

      if (lines == j)
      {
        break;
      }
    }
    temp_matrix.add(shapes[i], lines, column);
  }

  return temp_matrix;
}

stebnevsky::Matrix stebnevsky::part(const stebnevsky::CompositeShape & composite_shape)
{
  return part(composite_shape.getList(), composite_shape.getSize());
}
