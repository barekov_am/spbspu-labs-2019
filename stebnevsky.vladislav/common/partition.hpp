#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace stebnevsky
{
  bool intersect(const rectangle_t & first_shape, const rectangle_t & second_shape);
  Matrix part(const array_ptr & shapes, size_t size);
  Matrix part(const CompositeShape & composite_shape);
}

#endif // PARTITION_HPP
