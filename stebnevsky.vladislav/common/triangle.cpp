#include "triangle.hpp"
#include <stdexcept>
#include <cmath>
#include <algorithm>

stebnevsky::Triangle::Triangle(const point_t & vertex1, const point_t & vertex2, const point_t & vertex3):
  m_vertex1(vertex1),
  m_vertex2(vertex2),
  m_vertex3(vertex3),
  m_center({(m_vertex1.x + m_vertex2.x + m_vertex3.x) / 3, (m_vertex1.y + m_vertex2.y + m_vertex3.y) / 3})
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("Points of triangle lie on one line");
  }
}

static double getLength(const stebnevsky::point_t & vertex1, const stebnevsky::point_t & vertex2)
{
  const stebnevsky::point_t vector = {(vertex2.x - vertex1.x), (vertex2.y - vertex1.y)};
  return sqrt(vector.x * vector.x + vector.y * vector.y);
 
}

double stebnevsky::Triangle::getArea() const
{
  const double side1 = getLength(m_vertex1, m_vertex2);
  const double side2 = getLength(m_vertex2, m_vertex3);
  const double side3 = getLength(m_vertex3, m_vertex1);
  const double half_p = (side1 + side2 + side3) / 2;
  return sqrt(half_p * (half_p - side1) * (half_p - side2) * (half_p - side3));
}

stebnevsky::rectangle_t stebnevsky::Triangle::getFrameRect() const
{
  double max_x = std::max(std::max(m_vertex1.x, m_vertex2.x), m_vertex3.x);
  double min_x = std::min(std::min(m_vertex1.x, m_vertex2.x), m_vertex3.x);
  double max_y = std::max(std::max(m_vertex1.y, m_vertex2.y), m_vertex3.y);
  double min_y = std::min(std::min(m_vertex1.y, m_vertex2.y), m_vertex3.y);
  double width = max_x - min_x;
  double height = max_y - min_y;
  point_t center = {(min_x + max_x) / 2, (min_y + max_y) / 2};

  return {center, width, height};
}

void stebnevsky::Triangle::move(const point_t & point)
{
  double dx = point.x - m_center.x;
  double dy = point.y - m_center.y;

  move(dx, dy);
}

void stebnevsky::Triangle::move(double dx, double dy)
{
  m_center.x += dx;
  m_center.y += dy;

  m_vertex1.x += dx;
  m_vertex1.y += dy;

  m_vertex2.x += dx;
  m_vertex2.y += dy;

  m_vertex3.x += dx;
  m_vertex3.y += dy;
}

void stebnevsky::Triangle::scale(double coefficient)
{
  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Coefficient of scaling must be greater than zero");
  }
  m_vertex1.x = m_center.x + coefficient * (m_vertex1.x - m_center.x);
  m_vertex1.y = m_center.y + coefficient * (m_vertex1.y - m_center.y);
  m_vertex2.x = m_center.x + coefficient * (m_vertex2.x - m_center.x);
  m_vertex2.y = m_center.y + coefficient * (m_vertex2.y - m_center.y);
  m_vertex3.x = m_center.x + coefficient * (m_vertex3.x - m_center.x);
  m_vertex3.y = m_center.y + coefficient * (m_vertex3.y - m_center.y);
}

stebnevsky::point_t stebnevsky::Triangle::getCenter() const
{
  return m_center;
}

static stebnevsky::point_t rotateOneVertex(stebnevsky::point_t & vertex, const stebnevsky::point_t & center, double angle)
{
  double angle_rad = angle * M_PI / 180;
  double new_x = (vertex.x - center.x) * cos(angle_rad) - (vertex.y - center.y) * sin(angle_rad) + center.x;
  double new_y = (vertex.x - center.x) * sin(angle_rad) + (vertex.y - center.y) * cos(angle_rad) + center.y;
  return {new_x, new_y};
}

void stebnevsky::Triangle::rotate(double angle)
{
  point_t temp_center = m_center;
  m_vertex1 = rotateOneVertex(m_vertex1, temp_center, angle);
  m_vertex2 = rotateOneVertex(m_vertex2, temp_center, angle);
  m_vertex3 = rotateOneVertex(m_vertex3, temp_center, angle);
}
