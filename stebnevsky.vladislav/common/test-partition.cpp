#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(partitionTest)

BOOST_AUTO_TEST_CASE(checkCorrectIntersect)
{
  stebnevsky::Rectangle rectangle({{4.5, 2.5}, 5.0, 3.0});
  stebnevsky::Rectangle intersecting_rectangle({{7.0, 4.5}, 2.0, 3.0});
  stebnevsky::Circle circle({3.0, 6.0}, 1.0);
  stebnevsky::Triangle triangle({2.0, 5.0}, {3.0, 5.0}, {3.0, 7.0});

  const bool true_result1 = stebnevsky::intersect(rectangle.getFrameRect(), intersecting_rectangle.getFrameRect());
  const bool false_result1 = stebnevsky::intersect(rectangle.getFrameRect(), circle.getFrameRect());
  const bool false_result2 = stebnevsky::intersect(circle.getFrameRect(), intersecting_rectangle.getFrameRect());
  const bool true_result2 = stebnevsky::intersect(circle.getFrameRect(), triangle.getFrameRect());

  BOOST_CHECK_EQUAL(true_result1, true);
  BOOST_CHECK_EQUAL(false_result1, false);
  BOOST_CHECK_EQUAL(false_result2, false);
  BOOST_CHECK_EQUAL(true_result2, true);

}

BOOST_AUTO_TEST_CASE(checkCorrectSeparation)
{
  stebnevsky::Rectangle rectangle({{4.5, 2.5}, 5.0, 3.0});
  stebnevsky::Rectangle intersecting_rectangle({{7.0, 4.5}, 2.0, 3.0});
  stebnevsky::Circle circle({3.0, 6.0}, 1.0);
  stebnevsky::Triangle triangle({2.0, 5.0}, {3.0, 5.0}, {3.0, 7.0});

  stebnevsky::shape_ptr rectangle_ptr = std::make_shared<stebnevsky::Rectangle>(rectangle);
  stebnevsky::shape_ptr intersecting_rectangle_ptr = std::make_shared<stebnevsky::Rectangle>(intersecting_rectangle);
  stebnevsky::shape_ptr circle_ptr = std::make_shared<stebnevsky::Circle>(circle);
  stebnevsky::shape_ptr triangle_ptr = std::make_shared<stebnevsky::Triangle>(triangle);

  stebnevsky::CompositeShape composite_shape(rectangle_ptr);
  composite_shape.add(intersecting_rectangle_ptr);
  composite_shape.add(circle_ptr);
  composite_shape.add(triangle_ptr);


  stebnevsky::Matrix matrix = stebnevsky::part(composite_shape);

  BOOST_CHECK(matrix[0][0] == rectangle_ptr);
  BOOST_CHECK(matrix[1][0] == intersecting_rectangle_ptr);
  BOOST_CHECK(matrix[0][1] == circle_ptr);
  BOOST_CHECK(matrix[1][1] == triangle_ptr);
}

BOOST_AUTO_TEST_SUITE_END()
