#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

namespace stebnevsky
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix & matrix);
    Matrix(Matrix && matrix);
    ~Matrix() = default;

    Matrix & operator =(const Matrix & matrix);
    Matrix & operator =(Matrix && matrix);
    array_ptr operator [](size_t index) const;
    bool operator ==(const Matrix & matrix) const;
    bool operator !=(const Matrix & matrix) const;

    void add(const shape_ptr shape, size_t line, size_t column);

    size_t getLines() const;
    size_t getColumns() const;
    void printInfo() const;
  private:
    size_t m_lines;
    size_t m_columns;
    array_ptr m_array_shapes;
  };
}

#endif // MATRIX_HPP
