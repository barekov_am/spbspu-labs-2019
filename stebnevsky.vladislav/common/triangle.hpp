#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace stebnevsky
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t & vertex1, const point_t & vertex2, const point_t & vertex3);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & point) override;
    void move(double dx, double dy) override;
    void scale(double coefficient) override;
    point_t getCenter() const override;
    void rotate(double) override;
  private:
    point_t m_vertex1;
    point_t m_vertex2;
    point_t m_vertex3;
    point_t m_center;
  };
}

#endif // TRIANGLE_HPP
