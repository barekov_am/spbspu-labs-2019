#include "rectangle.hpp"
#include <cmath>
#include <stdexcept>

stebnevsky::Rectangle::Rectangle(const rectangle_t & rect_init):
  m_rect(rect_init),
  m_angle(0)
{
  if ((m_rect.width <= 0.0) || (m_rect.height <= 0.0))
  {
    throw std::invalid_argument("Width and height of rectangle must be greater than zero");
  }
}

double stebnevsky::Rectangle::getArea() const
{
  return m_rect.height * m_rect.width;
}

stebnevsky::rectangle_t stebnevsky::Rectangle::getFrameRect() const
{
  double angle_rad = m_angle * M_PI / 180;

  double width = m_rect.width * fabs(std::cos(angle_rad)) + m_rect.height * fabs(std::sin(angle_rad));
  double height = m_rect.width * fabs(std::sin(angle_rad)) + m_rect.height * fabs(std::cos(angle_rad));

  return {m_rect.pos, width, height};
}

void stebnevsky::Rectangle::move(const point_t & point)
{
  m_rect.pos = point;
}

void stebnevsky::Rectangle::move(double dx, double dy)
{
  m_rect.pos.x += dx;
  m_rect.pos.y += dy;
}

void stebnevsky::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient of scaling must be greater than zero");
  }
  m_rect.width *= coefficient;
  m_rect.height *= coefficient;
}

stebnevsky::point_t stebnevsky::Rectangle::getCenter() const
{
  return m_rect.pos;
}

void stebnevsky::Rectangle::rotate(double angle)
{
  m_angle += angle;
}
