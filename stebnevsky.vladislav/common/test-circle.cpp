#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include <boost/test/floating_point_comparison.hpp>
#include "circle.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testCircle)

  BOOST_AUTO_TEST_CASE(unchangedMeasurements)
  {
    stebnevsky::Circle circle({1.1, 2.3}, 5);
    const stebnevsky::rectangle_t initial_circle = circle.getFrameRect();
    const double initial_area = circle.getArea();

    circle.move(12.0, 21.0);
    BOOST_CHECK_CLOSE(initial_circle.width, circle.getFrameRect().width, ACCURACY);
    BOOST_CHECK_CLOSE(initial_circle.height, circle.getFrameRect().height, ACCURACY);
    BOOST_CHECK_CLOSE(initial_area, circle.getArea(), ACCURACY);

    circle.move({12.0, 21.0});
    BOOST_CHECK_CLOSE(initial_circle.width, circle.getFrameRect().width, ACCURACY);
    BOOST_CHECK_CLOSE(initial_circle.height, circle.getFrameRect().height, ACCURACY);
    BOOST_CHECK_CLOSE(initial_area, circle.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(squareAreaChange)
  {
    stebnevsky::Circle circle({1.1, 2.3}, 5);
    const double initial_area = circle.getArea();
    const double scale_parameter = 4.5;
    circle.scale(scale_parameter);
    BOOST_CHECK_CLOSE(initial_area * scale_parameter * scale_parameter, circle.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(ProcessingIncorrectParameters)
  {
    BOOST_CHECK_THROW(stebnevsky::Circle circle({12, -12}, -2), std::invalid_argument);
    BOOST_CHECK_THROW(stebnevsky::Circle circle({12, -12}, 0), std::invalid_argument);

    stebnevsky::Circle circle({12, -12}, 2);
    BOOST_CHECK_THROW(circle.scale(0), std::invalid_argument);
    BOOST_CHECK_THROW(circle.scale(-2), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(areaAfterRotating)
  {
  stebnevsky::Circle circle({1.1, 2.3}, 5);
  const double initial_area = circle.getArea();
  double angle = 56;

  circle.rotate(angle);

  BOOST_CHECK_CLOSE(initial_area, circle.getArea(), ACCURACY);
  }

BOOST_AUTO_TEST_SUITE_END()
