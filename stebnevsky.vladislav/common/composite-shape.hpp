#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace stebnevsky 
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape & composite_shape);
    CompositeShape(CompositeShape && composite_shape);
    CompositeShape(const shape_ptr & shape);
    ~CompositeShape() = default;

    CompositeShape & operator =(const CompositeShape & composite_shape);
    CompositeShape & operator =(CompositeShape && composite_shape);
    shape_ptr operator [](size_t index) const;

    size_t getSize() const;
    void add(const shape_ptr & shape);
    void remove(size_t index);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & point) override;
    void move (double dx, double dy) override;
    void scale (double coefficient) override;
    point_t getCenter() const override;
    void rotate(double angle) override;
    array_ptr getList() const;
  private:
    size_t m_size;
    array_ptr m_array_shapes;
  };
}

#endif // COMPOSITE_SHAPE_HPP
