#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include <boost/test/floating_point_comparison.hpp>
#include "triangle.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testTriangle)

  BOOST_AUTO_TEST_CASE(unchangedMeasurements)
  {
    stebnevsky::Triangle triangle({1.1, 2.3}, {5.0, 4.0}, {7.3, 7.5});
    const stebnevsky::rectangle_t initial_rectangle = triangle.getFrameRect();
    const double initial_area = triangle.getArea();

    triangle.move(12.0, 21.0);
    BOOST_CHECK_CLOSE(initial_rectangle.width, triangle.getFrameRect().width, ACCURACY);
    BOOST_CHECK_CLOSE(initial_rectangle.height, triangle.getFrameRect().height, ACCURACY);
    BOOST_CHECK_CLOSE(initial_area, triangle.getArea(), ACCURACY);

    triangle.move({12.0, 21.0});
    BOOST_CHECK_CLOSE(initial_rectangle.width, triangle.getFrameRect().width, ACCURACY);
    BOOST_CHECK_CLOSE(initial_rectangle.height, triangle.getFrameRect().height, ACCURACY);
    BOOST_CHECK_CLOSE(initial_area, triangle.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(squareAreaChange)
  {
    stebnevsky::Triangle triangle({1.1, 2.3}, {5.0, 4.0}, {7.3, 7.5});
    const double initial_area = triangle.getArea();
    const double scale_parameter = 4.5;
    triangle.scale(scale_parameter);
    BOOST_CHECK_CLOSE(initial_area * scale_parameter * scale_parameter, triangle.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(processingIncorrectParameters)
  {
    BOOST_CHECK_THROW(stebnevsky::Triangle triangle({1.0, 1.0}, {2.0, 2.0}, {3.0, 3.0}), std::invalid_argument);
    BOOST_CHECK_THROW(stebnevsky::Triangle triangle({1.0, 1.0}, {1.0, 1.0}, {1.0, 1.0}), std::invalid_argument);
    BOOST_CHECK_THROW(stebnevsky::Triangle triangle({1.0, 1.0}, {1.0, 1.0}, {3.0, 3.0}), std::invalid_argument);
    BOOST_CHECK_THROW(stebnevsky::Triangle triangle({4.0, 1.0}, {4.0, 4.0}, {4.0, 6.0}), std::invalid_argument);

    stebnevsky::Triangle triangle({1.1, 2.3}, {5.0, 4.0}, {7.3, 7.5});
    BOOST_CHECK_THROW(triangle.scale(0), std::invalid_argument);
    BOOST_CHECK_THROW(triangle.scale(-2), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(arearAfterRotating)
  {
  stebnevsky::Triangle triangle({1.1, 2.3}, {5.0, 4.0}, {7.3, 7.5});
  const double initial_area = triangle.getArea();
  double angle = 56;

  triangle.rotate(angle);

  BOOST_CHECK_CLOSE(initial_area, triangle.getArea(), ACCURACY);
  }

BOOST_AUTO_TEST_SUITE_END()
