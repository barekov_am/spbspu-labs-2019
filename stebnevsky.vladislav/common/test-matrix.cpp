#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(lineAndColumnMatching1)
{
  const size_t lines = 2;
  const size_t columns = 2;

  stebnevsky::Rectangle rectangle({{4.5, 2.5}, 5.0, 3.0});
  stebnevsky::Circle circle({3.0, 6.0}, 1.0);
  stebnevsky::Triangle triangle({2.0, 5.0}, {3.0, 5.0}, {3.0, 7.0});

  stebnevsky::shape_ptr rectangle_ptr = std::make_shared<stebnevsky::Rectangle>(rectangle);
  stebnevsky::shape_ptr circle_ptr = std::make_shared<stebnevsky::Circle>(circle);
  stebnevsky::shape_ptr triangle_ptr = std::make_shared<stebnevsky::Triangle>(triangle);

  stebnevsky::CompositeShape composite_shape(rectangle_ptr);

  composite_shape.add(circle_ptr);
  composite_shape.add(triangle_ptr);

  stebnevsky::Matrix matrix = stebnevsky::part(composite_shape);

  BOOST_CHECK_EQUAL(lines, matrix.getLines());
  BOOST_CHECK_EQUAL(columns, matrix.getColumns());
}

BOOST_AUTO_TEST_CASE(lineAndColumnMatching2)
{
  const size_t lines = 1;
  const size_t columns = 3;

  stebnevsky::Rectangle rectangle({{4.5, 2.5}, 5.0, 3.0});
  stebnevsky::Circle circle({10.0, 6.0}, 1.0);
  stebnevsky::Triangle triangle({2.0, 5.0}, {3.0, 5.0}, {3.0, 7.0});

  stebnevsky::shape_ptr rectangle_ptr = std::make_shared<stebnevsky::Rectangle>(rectangle);
  stebnevsky::shape_ptr circle_ptr = std::make_shared<stebnevsky::Circle>(circle);
  stebnevsky::shape_ptr triangle_ptr = std::make_shared<stebnevsky::Triangle>(triangle);

  stebnevsky::CompositeShape composite_shape(rectangle_ptr);

  composite_shape.add(circle_ptr);
  composite_shape.add(triangle_ptr);

  stebnevsky::Matrix matrix = stebnevsky::part(composite_shape);

  BOOST_CHECK_EQUAL(lines, matrix.getLines());
  BOOST_CHECK_EQUAL(columns, matrix.getColumns());
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  stebnevsky::Rectangle rectangle({{4.5, 2.5}, 5.0, 3.0});
  stebnevsky::Circle circle({10.0, 6.0}, 1.0);
  stebnevsky::Triangle triangle({2.0, 5.0}, {3.0, 5.0}, {3.0, 7.0});

  stebnevsky::shape_ptr rectangle_ptr = std::make_shared<stebnevsky::Rectangle>(rectangle);
  stebnevsky::shape_ptr circle_ptr = std::make_shared<stebnevsky::Circle>(circle);
  stebnevsky::shape_ptr triangle_ptr = std::make_shared<stebnevsky::Triangle>(triangle);

  stebnevsky::CompositeShape composite_shape(rectangle_ptr);

  composite_shape.add(circle_ptr);
  composite_shape.add(triangle_ptr);

  stebnevsky::Matrix matrix = stebnevsky::part(composite_shape);

  BOOST_CHECK_THROW(matrix[6][0], std::out_of_range);
  BOOST_CHECK_THROW(matrix[-1][0], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(constructorCopyAndMove)
{
  stebnevsky::Rectangle rectangle({{4.5, 2.5}, 5.0, 3.0});
  stebnevsky::Circle circle({10.0, 6.0}, 1.0);
  stebnevsky::Triangle triangle({2.0, 5.0}, {3.0, 5.0}, {3.0, 7.0});

  stebnevsky::shape_ptr rectangle_ptr = std::make_shared<stebnevsky::Rectangle>(rectangle);
  stebnevsky::shape_ptr circle_ptr = std::make_shared<stebnevsky::Circle>(circle);
  stebnevsky::shape_ptr triangle_ptr = std::make_shared<stebnevsky::Triangle>(triangle);

  stebnevsky::CompositeShape composite_shape(rectangle_ptr);

  composite_shape.add(circle_ptr);
  composite_shape.add(triangle_ptr);

  stebnevsky::Matrix matrix = stebnevsky::part(composite_shape);
  stebnevsky::Matrix copy_matrix(matrix);
  stebnevsky::Matrix move_matrix(std::move(matrix));

  BOOST_CHECK_EQUAL(move_matrix.getLines(), copy_matrix.getLines());
  BOOST_CHECK_EQUAL(move_matrix.getColumns(), copy_matrix.getColumns());
  BOOST_CHECK(move_matrix == copy_matrix);
}

BOOST_AUTO_TEST_SUITE_END()
