#include "composite-shape.hpp"
#include <cmath>
#include <stdexcept>

stebnevsky::CompositeShape::CompositeShape():
  m_size(0)
{
}

stebnevsky::CompositeShape::CompositeShape(const CompositeShape & shape):
  m_size(shape.m_size),
  m_array_shapes(std::make_unique<shape_ptr[]> (shape.m_size))
{
  for(size_t i = 0; i < m_size; i++)
  {
    m_array_shapes[i] = shape.m_array_shapes[i];
  }
}

stebnevsky::CompositeShape::CompositeShape(CompositeShape && shape):
  m_size(shape.m_size),
  m_array_shapes(std::move(shape.m_array_shapes))
{
  shape.m_size = 0;
}

stebnevsky::CompositeShape::CompositeShape(const shape_ptr & shape):
  CompositeShape()
{
  add(shape);
}

stebnevsky::CompositeShape &stebnevsky::CompositeShape::operator =(const CompositeShape & shape)
{
  if (this != & shape)
  {
    m_size = shape.m_size;
    array_ptr temp_array(std::make_unique<shape_ptr[]> (m_size));
    for(size_t i = 0; i < m_size; i++)
    {
      temp_array[i] = shape.m_array_shapes[i];
    }
    m_array_shapes.swap(temp_array);
  }

  return *this;
}

stebnevsky::CompositeShape &stebnevsky::CompositeShape::operator =(CompositeShape && shape)
{
  if (this != & shape)
  {
    m_size = shape.m_size;
    m_array_shapes = std::move(shape.m_array_shapes);
    shape.m_size = 0;
  }

  return *this;
}

stebnevsky::shape_ptr stebnevsky::CompositeShape::operator [](size_t index) const
{
  if (index >= m_size)
  {
    throw std::out_of_range("Index out of range");
  }

  return m_array_shapes[index];
}

size_t stebnevsky::CompositeShape::getSize() const
{
  return m_size;
}

void stebnevsky::CompositeShape::add(const shape_ptr & shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Invalid pointer");
  }

  m_size++;
  array_ptr temp_shapes(std::make_unique<shape_ptr[]> (m_size));

  for(size_t i = 0; i < (m_size - 1); i++)
  {
    temp_shapes[i] = m_array_shapes[i];
  }

  temp_shapes[m_size-1] = shape;
  m_array_shapes.swap(temp_shapes);
}

void stebnevsky::CompositeShape::remove(size_t index)
{
  if (index >= m_size)
  {
    throw std::out_of_range("Index out of range");
  }

  for(size_t i = index; i < (m_size - 1); i++)
  {
    m_array_shapes[i] = m_array_shapes[i+1];
  }

  m_size--;
}

double stebnevsky::CompositeShape::getArea() const
{
  if (m_size == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  double common_area = 0.0;
  for (size_t i = 0; i < m_size; i++)
  {
    common_area += m_array_shapes[i]->getArea();
  }

  return common_area;
}

stebnevsky::rectangle_t stebnevsky::CompositeShape::getFrameRect() const
{
  if (m_size == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  rectangle_t temp_rect = m_array_shapes[0]->getFrameRect();

  double max_x = temp_rect.pos.x + (temp_rect.width / 2);
  double min_x = temp_rect.pos.x - (temp_rect.width / 2);
  double max_y = temp_rect.pos.y + (temp_rect.height / 2);
  double min_y = temp_rect.pos.y - (temp_rect.height / 2);

  for (size_t i = 1; i < m_size; i++)
  {
    rectangle_t temp_rect = m_array_shapes[i]->getFrameRect();
    max_x = std::max(temp_rect.pos.x + (temp_rect.width / 2), max_x);
    min_x = std::min(temp_rect.pos.x - (temp_rect.width / 2), min_x);
    max_y = std::max(temp_rect.pos.y + (temp_rect.height / 2), max_y);
    min_y = std::min(temp_rect.pos.y - (temp_rect.height / 2), min_y);
  }

  return {{(max_x + min_x) / 2, (max_y + min_y) / 2}, max_x - min_x, max_y - min_y};
}

void stebnevsky::CompositeShape::move(const point_t & point)
{
  point_t common_center = getFrameRect().pos;
  double dx = point.x - common_center.x;
  double dy = point.y - common_center.y;
  move(dx, dy);
}

void stebnevsky::CompositeShape::move(double dx, double dy)
{
  if (m_size == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  for (size_t i = 0; i < m_size; i++)
  {
    m_array_shapes[i]->move(dx, dy);
  }  
}

void stebnevsky::CompositeShape::scale(double coefficient)
{
  if (m_size == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Coefficient of scaling must be greater than zero");
  }

  point_t common_center = getCenter();

  for(size_t i = 0; i < m_size; i++)
  {
    point_t shape_center = m_array_shapes[i]->getCenter();

    double dx = (shape_center.x - common_center.x) * (coefficient - 1);
    double dy = (shape_center.y - common_center.y) * (coefficient - 1);
    m_array_shapes[i]->move(dx, dy);
    m_array_shapes[i]->scale(coefficient);
  }
}

stebnevsky::point_t stebnevsky::CompositeShape::getCenter() const
{
  return getFrameRect().pos;
}

void stebnevsky::CompositeShape::rotate(double angle)
{
  const double angle_rad = angle * M_PI / 180;
  const point_t common_center = getCenter();

  for (size_t i = 0; i < m_size; i++)
  {
    point_t shape_center = m_array_shapes[i]->getCenter();
    double new_x = common_center.x + (shape_center.x - common_center.x) * fabs(cos(angle_rad)) - (shape_center.y - common_center.y) * fabs(sin(angle_rad));
    double new_y = common_center.y + (shape_center.x - common_center.x) * fabs(sin(angle_rad)) + (shape_center.y - common_center.y) * fabs(cos(angle_rad));

    m_array_shapes[i]->move({new_x, new_y});
    m_array_shapes[i]->rotate(angle);
  }
}

stebnevsky::array_ptr stebnevsky::CompositeShape::getList() const
{
  array_ptr temp_array(std::make_unique<shape_ptr[]>(m_size));

  for (unsigned int i = 0; i < m_size; i++)
  {
    temp_array[i] = m_array_shapes[i];
  }

  return temp_array;

}
