#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include <boost/test/floating_point_comparison.hpp>
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testCompositeShape)

BOOST_AUTO_TEST_CASE(unchangedMeasurements)
  {
    stebnevsky::Rectangle rectangle({{1.1, 2.3}, 5, 7});
    stebnevsky::Circle circle({1.1, 2.3}, 5);
    stebnevsky::Triangle triangle({1.1, 2.3}, {5.0, 4.0}, {7.3, 7.5});

    stebnevsky::CompositeShape composite_shape(std::make_shared<stebnevsky::Rectangle>(rectangle));

    composite_shape.add(std::make_shared<stebnevsky::Circle>(circle));
    composite_shape.add(std::make_shared<stebnevsky::Triangle>(triangle));

    const stebnevsky::rectangle_t initial_rectangle = composite_shape.getFrameRect();
    const double initial_area = composite_shape.getArea();

    composite_shape.move(12.0, 21.0);

    BOOST_CHECK_CLOSE(initial_rectangle.width, composite_shape.getFrameRect().width, ACCURACY);
    BOOST_CHECK_CLOSE(initial_rectangle.height, composite_shape.getFrameRect().height, ACCURACY);
    BOOST_CHECK_CLOSE(initial_area, composite_shape.getArea(), ACCURACY);

    composite_shape.move({12.0, 21.0});

    BOOST_CHECK_CLOSE(initial_rectangle.width, composite_shape.getFrameRect().width, ACCURACY);
    BOOST_CHECK_CLOSE(initial_rectangle.height, composite_shape.getFrameRect().height, ACCURACY);
    BOOST_CHECK_CLOSE(initial_area, composite_shape.getArea(), ACCURACY);
  }

BOOST_AUTO_TEST_CASE(squareAreaChangeIncreasing)
  {
    stebnevsky::Rectangle rectangle({{1.1, 2.3}, 5, 7});
    stebnevsky::Circle circle({1.1, 2.3}, 5);
    stebnevsky::Triangle triangle({1.1, 2.3}, {5.0, 4.0}, {7.3, 7.5});

    stebnevsky::CompositeShape composite_shape(std::make_shared<stebnevsky::Rectangle>(rectangle));

    composite_shape.add(std::make_shared<stebnevsky::Circle>(circle));
    composite_shape.add(std::make_shared<stebnevsky::Triangle>(triangle));

    const double initial_area = composite_shape.getArea();
    const double scale_parameter = 4.5;
  
    composite_shape.scale(scale_parameter);

    BOOST_CHECK_CLOSE(initial_area * scale_parameter * scale_parameter, composite_shape.getArea(), ACCURACY);
  }

BOOST_AUTO_TEST_CASE(squareAreaChangeDecreasing)
  {
    stebnevsky::Rectangle rectangle({{1.1, 2.3}, 5, 7});
    stebnevsky::Circle circle({1.1, 2.3}, 5);
    stebnevsky::Triangle triangle({1.1, 2.3}, {5.0, 4.0}, {7.3, 7.5});

    stebnevsky::CompositeShape composite_shape(std::make_shared<stebnevsky::Rectangle>(rectangle));

    composite_shape.add(std::make_shared<stebnevsky::Circle>(circle));
    composite_shape.add(std::make_shared<stebnevsky::Triangle>(triangle));

    const double initial_area = composite_shape.getArea();
    const double scale_parameter = 0.5;
  
    composite_shape.scale(scale_parameter);

    BOOST_CHECK_CLOSE(initial_area * scale_parameter * scale_parameter, composite_shape.getArea(), ACCURACY);
  }

BOOST_AUTO_TEST_CASE(ProcessingIncorrectParameters)
  {
    stebnevsky::Rectangle rectangle({{1.1, 2.3}, 5, 7});
    stebnevsky::Circle circle({1.1, 2.3}, 5);
    stebnevsky::Triangle triangle({1.1, 2.3}, {5.0, 4.0}, {7.3, 7.5});

    stebnevsky::CompositeShape composite_shape(std::make_shared<stebnevsky::Rectangle>(rectangle));

    composite_shape.add(std::make_shared<stebnevsky::Circle>(circle));
    composite_shape.add(std::make_shared<stebnevsky::Triangle>(triangle));

    BOOST_CHECK_THROW(composite_shape.scale(-3.0), std::invalid_argument);
    BOOST_CHECK_THROW(composite_shape.scale(0.0), std::invalid_argument);

    BOOST_CHECK_THROW(composite_shape.remove(-2), std::out_of_range);
    BOOST_CHECK_THROW(composite_shape.remove(23), std::out_of_range);

    BOOST_CHECK_THROW(composite_shape[3], std::out_of_range);
    BOOST_CHECK_THROW(composite_shape[-4], std::out_of_range);
  }

BOOST_AUTO_TEST_CASE(AreaAfterAddAndAfterRemove)
  {
    stebnevsky::Rectangle rectangle({{1.1, 2.3}, 5, 7});
    stebnevsky::Circle circle({1.1, 2.3}, 5);

    stebnevsky::CompositeShape composite_shape(std::make_shared<stebnevsky::Rectangle>(rectangle));

    const double initial_area = composite_shape.getArea();
    const double rectangle_area = rectangle.getArea();
    const double circle_area = circle.getArea();

    composite_shape.add(std::make_shared<stebnevsky::Circle>(circle));

    const double second_area = composite_shape.getArea();

    BOOST_CHECK_CLOSE(initial_area + circle_area, second_area, ACCURACY);

    composite_shape.remove(0);

    BOOST_CHECK_CLOSE(second_area - rectangle_area, circle_area, ACCURACY);
  }

BOOST_AUTO_TEST_CASE(checkCopyConstructor)
  {
    stebnevsky::Rectangle rectangle({{1.1, 2.3}, 5, 7});
    stebnevsky::Circle circle({1.1, 2.3}, 5);
    stebnevsky::Triangle triangle({1.1, 2.3}, {5.0, 4.0}, {7.3, 7.5});

    stebnevsky::CompositeShape composite_shape(std::make_shared<stebnevsky::Rectangle>(rectangle));
    composite_shape.add(std::make_shared<stebnevsky::Circle>(circle));
    composite_shape.add(std::make_shared<stebnevsky::Triangle>(triangle));

    const stebnevsky::rectangle_t initial_rectangle = composite_shape.getFrameRect();
    const double initial_area = composite_shape.getArea();
    const unsigned int composite_shape_size = composite_shape.getSize();

    stebnevsky::CompositeShape copy_composite_shape(composite_shape);

    const stebnevsky::rectangle_t copy_rectangle = copy_composite_shape.getFrameRect();

    BOOST_CHECK_CLOSE(initial_rectangle.width, copy_rectangle.width, ACCURACY);
    BOOST_CHECK_CLOSE(initial_rectangle.height, copy_rectangle.height, ACCURACY);
    BOOST_CHECK_CLOSE(initial_rectangle.pos.x, copy_rectangle.pos.x, ACCURACY);
    BOOST_CHECK_CLOSE(initial_rectangle.pos.y, copy_rectangle.pos.y, ACCURACY);
    BOOST_CHECK_CLOSE(initial_area, copy_composite_shape.getArea(), ACCURACY);
    BOOST_CHECK_EQUAL(composite_shape_size, copy_composite_shape.getSize());
  }

BOOST_AUTO_TEST_CASE(checkMoveConstructor)
  {
    stebnevsky::Rectangle rectangle({{1.1, 2.3}, 5, 7});
    stebnevsky::Circle circle({1.1, 2.3}, 5);
    stebnevsky::Triangle triangle({1.1, 2.3}, {5.0, 4.0}, {7.3, 7.5});

    stebnevsky::CompositeShape composite_shape(std::make_shared<stebnevsky::Rectangle>(rectangle));
    composite_shape.add(std::make_shared<stebnevsky::Circle>(circle));
    composite_shape.add(std::make_shared<stebnevsky::Triangle>(triangle));

    const stebnevsky::rectangle_t initial_rectangle = composite_shape.getFrameRect();
    const double initial_area = composite_shape.getArea();
    const unsigned int composite_shape_size = composite_shape.getSize();

    stebnevsky::CompositeShape move_composite_shape(std::move(composite_shape));

    const stebnevsky::rectangle_t move_rectangle = move_composite_shape.getFrameRect();

    BOOST_CHECK_CLOSE(initial_rectangle.width, move_rectangle.width, ACCURACY);
    BOOST_CHECK_CLOSE(initial_rectangle.height, move_rectangle.height, ACCURACY);
    BOOST_CHECK_CLOSE(initial_rectangle.pos.x, move_rectangle.pos.x, ACCURACY);
    BOOST_CHECK_CLOSE(initial_rectangle.pos.y, move_rectangle.pos.y, ACCURACY);
    BOOST_CHECK_CLOSE(initial_area, move_composite_shape.getArea(), ACCURACY);
    BOOST_CHECK_EQUAL(composite_shape_size, move_composite_shape.getSize());
  }

  BOOST_AUTO_TEST_CASE(AreaAfterRotating)
  {
  stebnevsky::Rectangle rectangle({{1.1, 2.3}, 5, 7});
  stebnevsky::Circle circle({1.1, 2.3}, 5);
  stebnevsky::Triangle triangle({1.1, 2.3}, {5.0, 4.0}, {7.3, 7.5});

  stebnevsky::CompositeShape composite_shape(std::make_shared<stebnevsky::Rectangle>(rectangle));
    composite_shape.add(std::make_shared<stebnevsky::Circle>(circle));
    composite_shape.add(std::make_shared<stebnevsky::Triangle>(triangle));

  const double initial_area = composite_shape.getArea();
  double angle = 56;

  composite_shape.rotate(angle);

  BOOST_CHECK_CLOSE(initial_area, composite_shape.getArea(), ACCURACY);
  }

BOOST_AUTO_TEST_SUITE_END()
