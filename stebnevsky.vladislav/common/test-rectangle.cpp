#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include <boost/test/floating_point_comparison.hpp>
#include "rectangle.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testRectangle)

  BOOST_AUTO_TEST_CASE(unchangedMeasurements)
  {
    stebnevsky::Rectangle rectangle({{1.1, 2.3}, 5, 7});
    const stebnevsky::rectangle_t initial_rectangle = rectangle.getFrameRect();
    const double initial_area = rectangle.getArea();

    rectangle.move(12.0, 21.0);
    BOOST_CHECK_CLOSE(initial_rectangle.width, rectangle.getFrameRect().width, ACCURACY);
    BOOST_CHECK_CLOSE(initial_rectangle.height, rectangle.getFrameRect().height, ACCURACY);
    BOOST_CHECK_CLOSE(initial_area, rectangle.getArea(), ACCURACY);

    rectangle.move({12.0, 21.0});
    BOOST_CHECK_CLOSE(initial_rectangle.width, rectangle.getFrameRect().width, ACCURACY);
    BOOST_CHECK_CLOSE(initial_rectangle.height, rectangle.getFrameRect().height, ACCURACY);
    BOOST_CHECK_CLOSE(initial_area, rectangle.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(squareAreaChange)
  {
    stebnevsky::Rectangle rectangle({{1.1, 2.3}, 5, 7});
    const double initial_area = rectangle.getArea();
    const double scale_parameter = 4.5;
    rectangle.scale(scale_parameter);
    BOOST_CHECK_CLOSE(initial_area * scale_parameter * scale_parameter, rectangle.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(processingIncorrectParameters)
  {
    BOOST_CHECK_THROW(stebnevsky::Rectangle rectangle({{12, -12}, -2, 1}), std::invalid_argument);
    BOOST_CHECK_THROW(stebnevsky::Rectangle rectangle({{12, -12}, 2, -1}), std::invalid_argument);
    BOOST_CHECK_THROW(stebnevsky::Rectangle rectangle({{12, -12}, 0, 1}), std::invalid_argument);
    BOOST_CHECK_THROW(stebnevsky::Rectangle rectangle({{12, -12}, 1, 0}), std::invalid_argument);

    stebnevsky::Rectangle rectangle({{12, -12}, 2, 1});
    BOOST_CHECK_THROW(rectangle.scale(0), std::invalid_argument);
    BOOST_CHECK_THROW(rectangle.scale(-2), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(areaAfterRotating)
  {
  stebnevsky::Rectangle rectangle({{1.1, 2.3}, 5, 7});
  const double initial_area = rectangle.getArea();
  double angle = 56;

  rectangle.rotate(angle);

  BOOST_CHECK_CLOSE(initial_area, rectangle.getArea(), ACCURACY);
  }

BOOST_AUTO_TEST_SUITE_END()
