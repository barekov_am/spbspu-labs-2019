#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "triangle.hpp"

void printInfo(const stebnevsky::Shape & shape)
{
  stebnevsky::rectangle_t temp_rect = shape.getFrameRect();
  stebnevsky::point_t temp_point = shape.getCenter();

  std::cout << std::endl;
  std::cout << "Area: " << shape.getArea() << std::endl;
  std::cout << "Center of frame rectangle:" << std::endl;
  std::cout << "x = " << temp_rect.pos.x << std::endl;
  std::cout << "y = " << temp_rect.pos.y << std::endl;
  std::cout << "Center of shape:" << std::endl;
  std::cout << "x = " << temp_point.x << std::endl;
  std::cout << "y = " << temp_point.y << std::endl;
  std::cout << "Other Info:" << std::endl;
  std::cout << "width = " << temp_rect.width << std::endl;
  std::cout << "height = " << temp_rect.height << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
}

int main()
{
  stebnevsky::Circle circle({15.0, 13.0}, 2.0);

  std::cout << "CIRCLE" << std::endl;
  printInfo(circle);
  std::cout << "after movement:" << std::endl;
  circle.move(15.0, 17.0);
  printInfo(circle);
  std::cout << "after movement:" << std::endl;
  circle.move({20.0, 20.0});
  printInfo(circle);
  std::cout << "after scale:" << std::endl;
  circle.scale(2.0);
  printInfo(circle);

  stebnevsky::Rectangle rectangle({{3.0, 23.0}, 3.0, 2.0});

  std::cout << "RECTANGLE" << std::endl;
  printInfo(rectangle);
  std::cout << "after movement:" << std::endl;
  rectangle.move(7.0, 17.0);
  printInfo(rectangle);
  std::cout << "after movement:" << std::endl;
  rectangle.move({20.0, 10.0});
  printInfo(rectangle);
  std::cout << "after scale:" << std::endl;
  rectangle.scale(2.0);
  printInfo(rectangle);

  stebnevsky::Triangle triangle({1.0, 1.0}, {12.0, 3.0}, {3.0, 30.0});

  std::cout << "TRIANGLE" << std::endl;
  printInfo(triangle);
  std::cout << "after movement:" << std::endl;
  triangle.move(7.0, 17.0);
  printInfo(triangle);
  std::cout << "after movement:" << std::endl;
  triangle.move({20.0, 20.0});
  printInfo(triangle);
  std::cout << "after scale:" << std::endl;
  triangle.scale(2.0);
  printInfo(triangle);

  stebnevsky::shape_ptr rectangle_ptr = std::make_shared<stebnevsky::Rectangle>(rectangle);
  stebnevsky::shape_ptr circle_ptr = std::make_shared<stebnevsky::Circle>(circle);
  stebnevsky::shape_ptr triangle_ptr = std::make_shared<stebnevsky::Triangle>(triangle);
  stebnevsky::CompositeShape composite_shape(rectangle_ptr);

  std::cout << "COMPOSITE-SHAPE" << std::endl;
  printInfo(composite_shape);
  std::cout << "Added circle:" << std::endl;
  composite_shape.add(circle_ptr);
  printInfo(composite_shape);
  std::cout << "Added triangle:" << std::endl;
  composite_shape.add(triangle_ptr);
  printInfo(composite_shape);
  std::cout << "After scaling" << std::endl;
  composite_shape.scale(3);
  printInfo(composite_shape);
  std::cout << "After moving" << std::endl;
  composite_shape.move(10.0, 20.0);
  printInfo(composite_shape);
  std::cout << "After removing 1st shape" << std::endl;
  composite_shape.remove(0);
  printInfo(composite_shape);

  return 0;
}
