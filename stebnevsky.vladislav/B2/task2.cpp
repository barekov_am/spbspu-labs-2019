#include <iostream>
#include <list>

const int MIN = 1;
const int MAX = 20;
const int MAX_COUNT = 20;

void printElements(std::list<int> &list)
{
  std::list<int>::iterator begin = list.begin();
  std::list<int>::iterator end = list.end();
  while (begin != end)
  {
    std::cout << *begin << " ";
    if (std::next(begin) == end)
    {
      break;
    }
    begin++;
    end--;
    std::cout << *end << " ";
  }
  std::cout << "\n";
}


void task2()
{
  std::list<int> list;
  int element;
  while (std::cin >> element, !std::cin.eof())
  {
    if ((element < MIN) || (element > MAX) || (list.size() + 1 > MAX_COUNT))
    {
      throw std::invalid_argument("Invalid value or number of values");
    }

    list.push_back(element);
  }

  if (list.empty())
  {
    return;
  }
  if(!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Can`t read file");
  }
  printElements(list);
}
