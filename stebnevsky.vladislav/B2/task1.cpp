#include <iostream>
#include <sstream>

#include "priorityqueue.hpp"

void task1()
{
  QueueWithPriority<std::string> queue;
  std::string string;

  while (std::getline(std::cin, string))
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Fail while reading data");
    }
    std::stringstream input(string);

    std::string task;
    input >> task;
    std::string data;
    std::string priority;

    if (task == "add")
    {
      input >> priority >> data;

      if (!input.eof())
      {
        std::getline(input, string);
        data += string;
      }

      if ((!input.eof()) || (data.empty()))
      {
        std::cout << "<INVALID COMMAND>\n";
        break;
      }

      if (priority == "high")
      {
        queue.addToQueue(ElementPriority::HIGH, data);
      }
      else if (priority == "normal")
      {
        queue.addToQueue(ElementPriority::NORMAL, data);
      }
      else if (priority == "low")
      {
        queue.addToQueue(ElementPriority::LOW, data);
      }
      else
      {
        std::cout << "<INVALID COMMAND>\n";
      }
    }
    else if (task == "get")
    {
      if (queue.empty())
      {
        std::cout << "<EMPTY>\n";
      }
      else
      {
        std::cout << queue.getElementFromQueue() << "\n";
      }
    }
    else if (task == "accelerate")
    {
      queue.accelerate();
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
