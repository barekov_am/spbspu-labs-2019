#ifndef SORTING_HPP
#define SORTING_HPP

#include <cstring>
#include <stdexcept>
#include <functional>

#include "accessMethod.hpp"

enum class Direction
{
  ascending,
  descending
};

template <template <class T> class Access, typename T>
void bubbleSort(T& collection, Direction direction)
{
  const auto begin = Access<T>::begin(collection);
  const auto end = Access<T>::end(collection);

  for (auto i = begin; i != end; ++i)
  {
    for (auto j = i; j != end; ++j)
    {
      if (Access<T>::get(collection, i) > Access<T>::get(collection, j))
      {
        std::swap(Access<T>::get(collection, i), Access<T>::get(collection, j));
      }
    }
  }

  if (direction == Direction::descending)
  {
    Access<T>::reverse(collection);
  }
}

Direction getOrder(const char* direction);

#endif
