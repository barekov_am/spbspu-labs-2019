#ifndef ACESSMETHOD_HPP
#define ACESSMETHOD_HPP

#include <iterator>
#include <algorithm>

template <typename T>
struct BracketAccess
{
  static size_t begin(const T&)
  {
    return 0;
  }

  static size_t end(const T& collection)
  {
    return collection.size();
  }

  static typename T::reference get(T& collection, size_t i)
  {
    if (i > collection.size())
    {
      throw std::invalid_argument("Out of bounds array");
    }

    return collection[i];
  }

  static void reverse(T& collection)
  {
    std::reverse(collection.begin(), collection.end());
  }
};

template <typename T>
struct AtAccess
{
  static size_t begin(const T&)
  {
    return 0;
  }

  static size_t end(const T& collection)
  {
    return collection.size();
  }

  static typename T::reference get(T& collection, size_t i)
  {
    return collection.at(i);
  }

  static void reverse(T& collection)
  {
    std::reverse(collection.begin(), collection.end());
  }

};

template <typename T>
struct IteratorAccess
{
  typedef typename T::iterator iterator;

  static iterator begin(T& collection)
  {
    return collection.begin();
  }

  static iterator end(T& collection)
  {
    return collection.end();
  }

  static typename T::reference get(T&, iterator& it)
  {
    return *it;
  }

  static void reverse(T& collection)
  {
    collection.reverse();
  }

};

#endif
