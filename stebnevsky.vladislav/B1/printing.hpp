#ifndef PRINTING_HPP
#define PRINTING_HPP

#include <iostream>

template <typename T>
void print(const T& collection)
{
  if (collection.empty())
  {
    return;
  }

  for (const auto& i: collection)
  {
    std::cout << i << " ";
  }

  std::cout << '\n';
}

#endif
