#include <fstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <vector>

void task2(const char* file)
    
{
  std::ifstream input(file);
    
  if (!input)
  {
    throw std::ios_base::failure("Trouble with file.");
  }



  size_t capacity = 1024;
  size_t i = 0;
  std::unique_ptr<char[], decltype(&free)> charArray(static_cast<char*>(malloc(capacity)), &free);

  while (input)
  {
    input.read(&charArray[i], capacity - i);
    i += input.gcount();

    if (i == capacity)
    {
      capacity *= 2;
      std::unique_ptr<char[], decltype(&free)> tempArray(static_cast<char*>(realloc(charArray.get(), capacity)), &free);

      if (tempArray)
      {
        charArray.release();
        std::swap(charArray, tempArray);
      }
      else
      {
        throw std::runtime_error("Failed to reallocate.");
      }
    }
  }
  input.close();

  if (input.is_open())
  {
    throw std::ios_base::failure("Failed to close file.");
  }
  
  std::vector<char> vector(&charArray[0], &charArray[i]);

  for (const auto& iterator: vector)
  {
    std::cout << iterator;
  }
}
