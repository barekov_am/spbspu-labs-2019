#include <forward_list>
#include <vector>

#include "printing.hpp"
#include "sorting.hpp"

void task1(const char* direction)
{
  Direction order = getOrder(direction);

  std::vector<int> vectorBracket;
  int value;
  while (std::cin >> value)
  {
    vectorBracket.push_back(value);
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::ios_base::failure("Fail while reading data.");
  }

  if (vectorBracket.empty())
  {
    return;
  }

  std::vector<int> vectorAt = vectorBracket;
  std::forward_list<int> listIterator(vectorBracket.begin(), vectorBracket.end());

  bubbleSort<BracketAccess>(vectorBracket, order);
  print(vectorBracket);

  bubbleSort<AtAccess>(vectorAt, order);
  print(vectorAt);

  bubbleSort<IteratorAccess>(listIterator, order);
  print(listIterator);
}
