#include <random>
#include <vector>
#include <time.h>

#include "sorting.hpp"
#include "printing.hpp"


void fillRandom(double* array, size_t size)
{
  for (size_t i = 0; i < size; ++i)
  {
    array[i] = ((rand() % 21) - 10) / 10.0;
  }
}

void task4(const char* direction, size_t size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size must be >= 0.");
  }

  Direction dir = getOrder(direction);

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);

  print(vector);
  bubbleSort<BracketAccess>(vector, dir);
  print(vector);
}
