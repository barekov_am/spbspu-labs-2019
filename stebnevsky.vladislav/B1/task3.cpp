#include <vector>

#include "printing.hpp"

void task3()
{
  std::vector<int> vector;
  int value = 0;
  bool endZero = false;

  while (std::cin >> value)
  {
    if (value == 0)
    {
      endZero = true;
      break;
    }
    vector.push_back(value);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Reading failed");
  }

  if (vector.empty())
  {
    return;
  }

  if (!endZero)
  {
    throw std::runtime_error("Input must be ended by zero");
  }

  auto it = vector.begin();
  switch (vector.back())
  {
  case 1:
    while (it != vector.end())
    {
      it = ((*it % 2) == 0) ? vector.erase(it) : ++it;
    }
    break;

  case 2:
    while (it != vector.end())
    {
      it = ((*it % 3) == 0) ? (vector.insert(++it, 3, 1) + 3) : ++it;
    }
    break;

  default:
    break;
  }

  print(vector);
}
