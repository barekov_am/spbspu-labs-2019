#ifndef MATRIX_HPP
#define MATRIX_HPP
#include <memory>
#include "stddef.h"

namespace utin
{
  template <typename Type>
  class Matrix
  {
  public:
    using ptr = std::shared_ptr<Type>;

    class Array
    {
    public:
      Array(const Array &);
      Array(Array &&) noexcept;
      Array(ptr *, size_t);
      ~Array() = default;
      Array &operator =(const Array &);
      Array &operator =(Array &&) noexcept;
      ptr operator [](size_t) const;
      size_t size() const;
      void swap(Array &) noexcept;
    private:
      size_t size_;
      std::unique_ptr<ptr []> array_;
    };

    Matrix();
    Matrix(const Matrix<Type> &);
    Matrix(Matrix<Type> &&) noexcept;
    ~Matrix() = default;
    Matrix<Type> &operator =(const Matrix<Type> &);
    Matrix<Type> &operator =(Matrix<Type> &&) noexcept;
    Array operator [](size_t) const;
    bool operator ==(const Matrix<Type> &) const;
    bool operator !=(const Matrix<Type> &) const;
    size_t getRows() const;
    void add(ptr, size_t);
    void swap(Matrix<Type> &) noexcept;
  private:
    size_t rows_;
    size_t count_;
    std::unique_ptr<size_t []> columns_;
    std::unique_ptr<ptr []> data_;
  };
  template <typename Type>
utin::Matrix<Type>::Matrix():
  rows_(0),
  count_(0)
{
}


template <typename Type>
utin::Matrix<Type>::Matrix(const utin::Matrix<Type> &other):
  rows_(other.rows_),
  count_(other.count_),
  columns_(std::make_unique<size_t []>(other.rows_)),
  data_(std::make_unique<ptr []>(other.count_))
{
  for (size_t i = 0; i < rows_; ++i)
  {
    columns_[i] = other.columns_[i];
  }

  for (size_t i = 0; i < count_; ++i)
  {
    data_[i] = other.data_[i];
  }
}

template <typename Type>
utin::Matrix<Type>::Matrix(utin::Matrix<Type> &&other) noexcept:
  rows_(other.rows_),
  count_(other.count_),
  columns_(std::move(other.columns_)),
  data_(std::move(other.data_))
{
  other.rows_ = 0;
  other.count_ = 0;
}

template <typename Type>
utin::Matrix<Type> &utin::Matrix<Type>::operator =(const utin::Matrix<Type> &other)
{
  if (this != &other)
  {
    Matrix<Type>(other).swap(*this);
  }

  return *this;
}

template <typename Type>
utin::Matrix<Type> &utin::Matrix<Type>::operator =(utin::Matrix<Type> &&other) noexcept
{
  if (this != &other)
  {
    rows_ = other.rows_;
    count_ = other.count_;
    columns_ = std::move(other.columns_);
    data_ = std::move(other.data_);

    other.rows_ = 0;
    other.count_ = 0;
  }

  return *this;
}

template <typename Type>
typename utin::Matrix<Type>::Array utin::Matrix<Type>::operator [](size_t row) const
{
  if (row >= rows_)
  {
    throw std::out_of_range("Index out of range!");
  }

  size_t startIndex = 0;
  for (size_t i = 0; i < row; ++i)
  {
    startIndex += columns_[i];
  }
  return utin::Matrix<Type>::Array(&data_[startIndex], columns_[row]);
}

template <typename Type>
bool utin::Matrix<Type>::operator ==(const utin::Matrix<Type> &matrix) const
{
  if ((rows_ != matrix.rows_) || (count_ != matrix.count_))
  {
    return false;
  }
  for (size_t i = 0; i < rows_; i++)
  {
    if (columns_[i] != matrix.columns_[i])
    {
      return false;
    }
  }
  for (size_t i = 0; i < count_; ++i)
  {
    if (data_[i] != matrix.data_[i])
    {
      return false;
    }
  }

  return true;
}

template <typename Type>
bool utin::Matrix<Type>::operator !=(const utin::Matrix<Type> &matrix) const
{
  return !(*this == matrix);
}

template <typename Type>
size_t utin::Matrix<Type>::getRows() const
{
  return rows_;
}

template <typename Type>
void utin::Matrix<Type>::add(ptr type, size_t row)
{
  if (row > rows_)
  {
    throw std::out_of_range("Index out of range");
  }

  if (!type)
  {
    throw std::invalid_argument("Pointer have to be not equal nullptr");
  }

  std::unique_ptr<ptr []> newData = std::make_unique<ptr []>(count_ + 1);
  size_t newIndex = 0;
  size_t oldIndex = 0;
  for (size_t i = 0; i < rows_; ++i)
  {
    for (size_t j = 0; j < columns_[i]; ++j)
    {
      newData[newIndex++] = data_[oldIndex++];
    }
    if (row == i)
    {
      newData[newIndex++] = type;
      ++columns_[row];
    }
  }

  if (row == rows_)
  {
    std::unique_ptr<size_t []> newColumns = std::make_unique<size_t []>(rows_ + 1);
    for (size_t i = 0; i < rows_; ++i)
    {
      newColumns[i] = columns_[i];
    }
    newColumns[row] = 1;
    newData[count_] = type;
    ++rows_;
    columns_.swap(newColumns);
  }

  data_.swap(newData);
  ++count_;
}

template <typename Type>
void utin::Matrix<Type>::swap(utin::Matrix<Type> &other) noexcept
{
  std::swap(rows_, other.rows_);
  std::swap(count_, other.count_);
  std::swap(columns_, other.columns_);
  std::swap(data_, other.data_);
}

template <typename Type>
utin::Matrix<Type>::Array::Array(const utin::Matrix<Type>::Array &other):
size_(other.size_),
array_(std::make_unique<ptr []>(other.size_))
{
  for (size_t i = 0; i < size_; ++i)
  {
    array_[i] = other.array_[i];
  }
}

template <typename Type>
utin::Matrix<Type>::Array::Array(utin::Matrix<Type>::Array &&other) noexcept :
size_(other.size_),
array_(std::move(other.array_))
{
  other.size_ = 0;
}

template <typename Type>
utin::Matrix<Type>::Array::Array(ptr * array, size_t size):
size_(size),
array_(std::make_unique<ptr []>(size))
{
  for (size_t i = 0; i < size_; ++i)
  {
    array_[i] = array[i];
  }
}

template <typename Type>
typename utin::Matrix<Type>::Array &utin::Matrix<Type>::Array::operator =(const utin::Matrix<Type>::Array &other)
{
  if (this != &other)
  {
    Matrix<Type>::Array(other).swap(*this);
  }

  return *this;
}

template <typename Type>
typename utin::Matrix<Type>::Array &utin::Matrix<Type>::Array::operator =(utin::Matrix<Type>::Array &&other) noexcept
{
  if (this != &other)
  {
    size_ = other.size_;
    other.size_ = 0;
    array_ = std::move(other.array_);
  }
}

template <typename Type>
typename utin::Matrix<Type>::ptr utin::Matrix<Type>::Array::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Index have to be less than size");
  }

  return array_[index];
}

template <typename Type>
size_t utin::Matrix<Type>::Array::size() const
{
  return size_;
}

template <typename Type>
void utin::Matrix<Type>::Array::swap(Array &swappintArray) noexcept
{
  std::swap(size_, swappintArray.size_);
  std::swap(array_, swappintArray.array_);
}

}
#endif
