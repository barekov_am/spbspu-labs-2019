#include <memory>
#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "matrix.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

const double MISTAKE = 0.0001;

BOOST_AUTO_TEST_SUITE(matrixMethodsTesting)

BOOST_AUTO_TEST_CASE(testMatrixCopyConstructor)
{
  utin::Matrix<double> testMatrix;
  const size_t rows = 3;
  const size_t columns = 5;

  for (size_t i = 0; i < rows; ++i)
  {
    for (size_t j = 0; j < columns; ++j)
    {
      testMatrix.add(std::make_shared<double>((i + j) / 2.0), i);
    }
  }
  utin::Matrix<double> copyMatrix(testMatrix);
  for (size_t i = 0; i < rows; ++i)
  {
    for (size_t j = 0; j < columns; ++j)
    {
      BOOST_CHECK_CLOSE(*copyMatrix[i][j], (i + j) / 2.0, MISTAKE);
    }
  }
}

BOOST_AUTO_TEST_CASE(testMatrixCopyOperator)
{
  utin::Matrix<double> testMatrix;
  const size_t rows = 3;
  const size_t columns = 5;

  for (size_t i = 0; i < rows; ++i)
  {
    for (size_t j = 0; j < columns; ++j)
    {
      testMatrix.add(std::make_shared<double>((i * j) / 4.0), i);
    }
  }
  utin::Matrix<double> copyMatrix;
  copyMatrix = testMatrix;

  for (size_t i = 0; i < rows; ++i)
  {
    for (size_t j = 0; j < columns; ++j)
    {
      BOOST_CHECK_CLOSE(*copyMatrix[i][j], (i * j) / 4.0, MISTAKE);
    }
  }
}

BOOST_AUTO_TEST_CASE(testMatrixMoveConstructor)
{
  utin::Matrix<double> testMatrix;
  const size_t rows = 3;
  const size_t columns = 5;

  for (size_t i = 0; i < rows; ++i)
  {
    for (size_t j = 0; j < columns; ++j)
    {
      testMatrix.add(std::make_shared<double>((i + j) / 3.0), i);
    }
  }
  utin::Matrix<double> moveMatrix(std::move(testMatrix));
  for (size_t i = 0; i < rows; ++i)
  {
    for (size_t j = 0; j < columns; ++j)
    {
      BOOST_CHECK_CLOSE(*moveMatrix[i][j], (i + j) / 3.0, MISTAKE);
    }
  }
}

BOOST_AUTO_TEST_CASE(testMatrixMoveOperator)
{
  utin::Matrix<double> testMatrix;
  const size_t rows = 3;
  const size_t columns = 5;

  for (size_t i = 0; i < rows; ++i)
  {
    for (size_t j = 0; j < columns; ++j)
    {
      testMatrix.add(std::make_shared<double>((i * j) / 5.0), i);
    }
  }

  utin::Matrix<double> moveMatrix;
  moveMatrix = std::move(testMatrix);

  for (size_t i = 0; i < rows; ++i)
  {
    for (size_t j = 0; j < columns; ++j)
    {
      BOOST_CHECK_CLOSE(*moveMatrix[i][j], (i * j) / 5.0, MISTAKE);
    }
  }
}

BOOST_AUTO_TEST_CASE(testMatrixEqualOperator)
{
  utin::Matrix<double> testMatrix;
  const size_t rows = 3;
  const size_t columns = 5;
  for (size_t i = 0; i < rows; ++i)
  {
    for (size_t j = 0; j < columns; ++j)
    {
      testMatrix.add(std::make_shared<double>((i + j) / 3.0), i);
    }
  }
  utin::Matrix<double> testEqualMatrix(testMatrix);
  utin::Matrix<double> testUnequalMatrix;
  testUnequalMatrix.add(std::make_shared<double>(2.1), 0);
  BOOST_CHECK_EQUAL(testEqualMatrix == testMatrix, true);
  BOOST_CHECK_EQUAL(testUnequalMatrix == testMatrix, false);
}

BOOST_AUTO_TEST_CASE(testMatrixUnequalOperator)
{
  utin::Matrix<double> testMatrix;
  const size_t rows = 3;
  const size_t columns = 5;

  for (size_t i = 0; i < rows; ++i)
  {
    for (size_t j = 0; j < columns; ++j)
    {
      testMatrix.add(std::make_shared<double>((i + j) / 3.0), i);
    }
  }
  utin::Matrix<double> testEqualMatrix(testMatrix);
  utin::Matrix<double> testUnequalMatrix;
  testUnequalMatrix.add(std::make_shared<double>(2.1), 0);
  BOOST_CHECK_EQUAL(testEqualMatrix != testMatrix, false);
  BOOST_CHECK_EQUAL(testUnequalMatrix != testMatrix, true);
}

BOOST_AUTO_TEST_CASE(testMatrixGetMethodException)
{
  utin::Matrix<double> testMatrix;
  const size_t rows = 3;
  const size_t columns = 5;
  for (size_t i = 0; i < rows; ++i)
  {
    for (size_t j = 0; j < columns; ++j)
    {
      testMatrix.add(std::make_shared<double>((i + j) / 3.0), i);
    }
  }
  BOOST_CHECK_THROW(testMatrix[7][3], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[3][8], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
