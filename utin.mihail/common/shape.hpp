#ifndef SHAPE_HPP
#define SHAPE_HPP
#include "base-types.hpp"
#include <memory>

namespace utin
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const double &, const double &) = 0;
    virtual void move(const point_t &) = 0;
    virtual void scale(double) = 0;
    virtual void rotate(double) = 0;
    virtual void printInfo() const = 0;
  };
  using shape_ptr = std::shared_ptr<Shape>;
  using shapes_array = std::unique_ptr<shape_ptr []>;
}

#endif
