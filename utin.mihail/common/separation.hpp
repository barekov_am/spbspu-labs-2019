#ifndef SEPARATION_HPP
#define SEPARATION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"
#include "stddef.h"

namespace utin
{
  Matrix<Shape> part(const CompositeShape &);
  bool intersect(const rectangle_t &, const rectangle_t &);
}

#endif
