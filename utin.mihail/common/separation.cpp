#include "separation.hpp"
#include <stddef.h>
#include "matrix.hpp"

utin::Matrix<utin::Shape> utin::part(const CompositeShape &compositeShape)
{
  utin::Matrix<utin::Shape> matrix;

  size_t count = compositeShape.getCount();
  for (size_t i = 0; i < count; ++ i)
  {
    size_t layer = 0;
    size_t matrixRows = matrix.getRows();
    for (size_t j = matrixRows; j-- > 0;)
    {
      size_t matrixColumns = matrix[j].size();
      for (size_t k = 0; k < matrixColumns; ++k)
      {
        if (intersect(matrix[j][k]->getFrameRect(), compositeShape[i]->getFrameRect()))
        {
          layer = j + 1;
          break;
        }
      }

      if (layer > j)
      {
        break;
      }
    }

    matrix.add(compositeShape[i], layer);
  }

  return matrix;
}

bool utin::intersect(const rectangle_t & lftRect, const rectangle_t & rhtRect)
{
  const utin::point_t lftRectLftBottom = {lftRect.pos.x - lftRect.width / 2, lftRect.pos.y - lftRect.height / 2};
  const utin::point_t lftRectRhtTop = {lftRect.pos.x + lftRect.width / 2, lftRect.pos.y + lftRect.height / 2};

  const utin::point_t rhtRectLftBottom = {rhtRect.pos.x - rhtRect.width / 2, rhtRect.pos.y - rhtRect.height / 2};
  const utin::point_t rhtRectRhtTop = {rhtRect.pos.x + rhtRect.width / 2, rhtRect.pos.y + rhtRect.height / 2};

  const bool firstCondition = (rhtRectLftBottom.y < lftRectRhtTop.y) && (rhtRectLftBottom.x < lftRectRhtTop.x);
  const bool secondCondition = (rhtRectRhtTop.y > lftRectLftBottom.y) && (rhtRectRhtTop.x > lftRectLftBottom.x);

  return firstCondition && secondCondition;
}
