#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include "shape.hpp"
#include "stddef.h"

namespace utin
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&) noexcept;
    CompositeShape(const shape_ptr &);
    ~CompositeShape() = default;
    CompositeShape &operator =(const CompositeShape &);
    CompositeShape &operator =(CompositeShape &&) noexcept;
    shape_ptr operator [](size_t) const;
    void add(const shape_ptr &);
    void remove(const size_t &);
    size_t getCount() const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const double &, const double &) override;
    void move(const point_t &) override;
    void rotate(double) override;
    void scale(double) override;
    void printInfo() const override;
  private:
    shapes_array arrayShapes_;
    size_t count_;
    void checkEmpty() const;
    void checkRange(const size_t &) const;
    void swap(CompositeShape &) noexcept;
  };
}

#endif
