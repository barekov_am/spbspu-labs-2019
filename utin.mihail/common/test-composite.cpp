#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

const double MISTAKE = 0.001;
utin::Circle testCircle({2.1, 3.9}, 4.5);
utin::Rectangle testRectangle({4.1, 0.2}, 1.5, 5.6);

BOOST_AUTO_TEST_SUITE(compositeShapeTesting)

BOOST_AUTO_TEST_CASE(compositeShapeTestMoving)
{
  utin::CompositeShape testCompositeShape(std::make_shared<utin::Circle>(testCircle));
  testCompositeShape.add(std::make_shared<utin::Rectangle>(testRectangle));
  const double areaBeforeMoving = testCompositeShape.getArea();
  const utin::rectangle_t frameRectBeforeMoving = testCompositeShape.getFrameRect();
  testCompositeShape.move(-1.1, 2.1);
  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), areaBeforeMoving, MISTAKE);
  const utin::rectangle_t frameRectAfterMoving = testCompositeShape.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, MISTAKE);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestMovingTo)
{
  utin::CompositeShape testCompositeShape(std::make_shared<utin::Circle>(testCircle));
  testCompositeShape.add(std::make_shared<utin::Rectangle>(testRectangle));
  const double areaBeforeMoving = testCompositeShape.getArea();
  const utin::rectangle_t frameRectBeforeMoving = testCompositeShape.getFrameRect();
  testCompositeShape.move({-1.0, 2.0});
  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), areaBeforeMoving, MISTAKE);
  const utin::rectangle_t frameRectAfterMoving = testCompositeShape.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, MISTAKE);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestScaleArea)
{
  utin::CompositeShape testCompositeShape(std::make_shared<utin::Circle>(testCircle));
  testCompositeShape.add(std::make_shared<utin::Rectangle>(testRectangle));
  const double areaBeforeScale = testCompositeShape.getArea();
  const double multiplier = 1.5;
  testCompositeShape.scale(multiplier);
  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), areaBeforeScale * multiplier * multiplier, MISTAKE);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestScaleFrameRect)
{
  utin::CompositeShape testCompositeShape(std::make_shared<utin::Circle>(testCircle));
  testCompositeShape.add(std::make_shared<utin::Rectangle>(testRectangle));
  const double multiplier = 0.5;
  const utin::rectangle_t originFrameRect = testCompositeShape.getFrameRect();
  testCompositeShape.scale(multiplier);
  const utin::rectangle_t newFrameRect = testCompositeShape.getFrameRect();
  BOOST_CHECK_CLOSE(originFrameRect.width * multiplier, newFrameRect.width, MISTAKE);
  BOOST_CHECK_CLOSE(originFrameRect.height * multiplier, newFrameRect.height, MISTAKE);
  BOOST_CHECK_CLOSE(originFrameRect.pos.x, newFrameRect.pos.x, MISTAKE);
  BOOST_CHECK_CLOSE(originFrameRect.pos.y, newFrameRect.pos.y, MISTAKE);
}
BOOST_AUTO_TEST_CASE(compositeShapeTestCopy)
{
  utin::CompositeShape testCompositeShape(std::make_shared<utin::Circle>(testCircle));
  testCompositeShape.add(std::make_shared<utin::Rectangle>(testRectangle));
  const double area = testCompositeShape.getArea();
  const utin::rectangle_t frameRect = testCompositeShape.getFrameRect();
  utin::CompositeShape testCompositeShapeCopy;
  testCompositeShapeCopy = testCompositeShape;
  BOOST_CHECK_CLOSE(area, testCompositeShapeCopy.getArea(), MISTAKE);
  BOOST_CHECK_CLOSE(frameRect.width, testCompositeShapeCopy.getFrameRect().width, MISTAKE);
  BOOST_CHECK_CLOSE(frameRect.height, testCompositeShapeCopy.getFrameRect().height, MISTAKE);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestConstructorCopy)
{
  utin::CompositeShape testCompositeShape(std::make_shared<utin::Circle>(testCircle));
  testCompositeShape.add(std::make_shared<utin::Rectangle>(testRectangle));
  const double area = testCompositeShape.getArea();
  const utin::rectangle_t frameRect = testCompositeShape.getFrameRect();
  utin::CompositeShape testCompositeShapeCopy(testCompositeShape);
  BOOST_CHECK_CLOSE(area, testCompositeShapeCopy.getArea(), MISTAKE);
  BOOST_CHECK_CLOSE(frameRect.width, testCompositeShapeCopy.getFrameRect().width, MISTAKE);
  BOOST_CHECK_CLOSE(frameRect.height, testCompositeShapeCopy.getFrameRect().height, MISTAKE);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestTrowExceptionGettingEmptyShape)
{
  utin::CompositeShape testCompositeShape;
  BOOST_CHECK_THROW(testCompositeShape[0], std::logic_error);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestTrowExceptionMovingEmpty)
{
  utin::CompositeShape testCompositeShape;
  BOOST_CHECK_THROW(testCompositeShape.move(2, 3), std::logic_error);
  BOOST_CHECK_THROW(testCompositeShape.move({2, 3}), std::logic_error);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestTrowExceptionGettingEmptyArea)
{
  utin::CompositeShape testCompositeShape;
  BOOST_CHECK_THROW(testCompositeShape.getArea(), std::logic_error);
}


BOOST_AUTO_TEST_CASE(compositeShapeTestTrowExceptionGettingEmptyFrameRect)
{
  utin::CompositeShape testCompositeShape;
  BOOST_CHECK_THROW(testCompositeShape.getFrameRect(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestTrowExceptionGettingEmpty)
{
  utin::CompositeShape testCompositeShape(std::make_shared<utin::Circle>(testCircle));
  testCompositeShape.remove(0);
  BOOST_CHECK_THROW(testCompositeShape[0], std::logic_error);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestTrowExceptionScale)
{
  utin::CompositeShape testCompositeShape(std::make_shared<utin::Circle>(testCircle));
  testCompositeShape.add(std::make_shared<utin::Rectangle>(testRectangle));
  BOOST_CHECK_THROW(testCompositeShape.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestTrowExceptionGetShapeInvalidArgument)
{
  utin::CompositeShape testCompositeShape(std::make_shared<utin::Circle>(testCircle));
  testCompositeShape.add(std::make_shared<utin::Rectangle>(testRectangle));
  BOOST_CHECK_THROW(testCompositeShape[2], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeTestRotate)
{
  utin::CompositeShape testComposite(std::make_shared<utin::Rectangle>(testRectangle));
  testComposite.add(std::make_shared<utin::Circle>(testCircle));

  const utin::rectangle_t frameRectBeforeRotate = testComposite.getFrameRect();
  const double areaBeforeRotate = testComposite.getArea();

  testComposite.rotate(90);
  utin::rectangle_t frameRectAfterRotate = testComposite.getFrameRect();
  BOOST_CHECK_CLOSE(areaBeforeRotate, testComposite.getArea(), MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, MISTAKE);

  testComposite.rotate(-180);
  frameRectAfterRotate = testComposite.getFrameRect();
  BOOST_CHECK_CLOSE(areaBeforeRotate, testComposite.getArea(), MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, MISTAKE);
}

BOOST_AUTO_TEST_SUITE_END()
