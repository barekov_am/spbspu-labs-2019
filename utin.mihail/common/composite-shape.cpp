#include "composite-shape.hpp"
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <memory>
#include <cmath>

utin::CompositeShape::CompositeShape():
  count_(0)
{
}

utin::CompositeShape::CompositeShape(const utin::CompositeShape &other):
arrayShapes_(std::make_unique<shape_ptr []>(other.count_)),
count_(other.count_)
{
  for (size_t i = 0; i < count_; ++i)
  {
    arrayShapes_[i] = other.arrayShapes_[i];
  }
}

utin::CompositeShape::CompositeShape(utin::CompositeShape &&other) noexcept:
arrayShapes_(std::move(other.arrayShapes_)),
count_(other.count_)
{
  other.count_ = 0;
}


utin::CompositeShape::CompositeShape(const shape_ptr &shape):
  arrayShapes_(std::make_unique<shape_ptr []>(1)),
  count_(1)
{
  arrayShapes_[0] = shape;
}

utin::CompositeShape &utin::CompositeShape::operator =(const utin::CompositeShape &other)
{
  if (this != &other)
  {
    utin::CompositeShape(other).swap(*this);
  }
  return *this;
}

utin::CompositeShape &utin::CompositeShape::operator =(utin::CompositeShape &&other) noexcept
{
  if (this != &other)
  {
    count_ = other.count_;
    other.count_ = 0;
    arrayShapes_ = std::move(other.arrayShapes_);
  }
  return *this;
}

utin::shape_ptr utin::CompositeShape::operator [](size_t index) const
{
  checkRange(index);
  return arrayShapes_[index];
}

void utin::CompositeShape::add(const utin::shape_ptr &newShape)
{
  utin::shapes_array tempArray(std::make_unique<std::shared_ptr<utin::Shape> []>(count_ + 1));
  for (size_t i = 0; i < count_; ++i)
  {
    tempArray[i] = arrayShapes_[i];
  }
  tempArray[count_] = newShape;
  ++count_;
  arrayShapes_.swap(tempArray);
}

void utin::CompositeShape::remove(const size_t &index)
{
  checkRange(index);
  --count_;
  utin::shapes_array tempArray(std::make_unique<std::shared_ptr<utin::Shape> []>(count_));
  for (size_t i = index; i < count_; ++i)
  {
    arrayShapes_[i] = arrayShapes_[i + 1];
  }
  for (size_t i = 0; i < count_; ++i)
  {
    tempArray[i] = arrayShapes_[i];
  }
  std::swap(arrayShapes_, tempArray);
}

size_t utin::CompositeShape::getCount() const
{
  return count_;
}

double utin::CompositeShape::getArea() const
{
  checkEmpty();
  double compositeShapeArea = 0.0;
  for (size_t i = 0; i < count_; ++i)
  {
    compositeShapeArea += arrayShapes_[i]->getArea();
  }
  return compositeShapeArea;
}

utin::rectangle_t utin::CompositeShape::getFrameRect() const
{
  checkEmpty();
  utin::rectangle_t frameRectangle = arrayShapes_[0]->getFrameRect();
  double maxX = frameRectangle.pos.x + frameRectangle.width / 2.0;
  double maxY = frameRectangle.pos.y + frameRectangle.height / 2.0;
  double minX = frameRectangle.pos.x - frameRectangle.width / 2.0;
  double minY = frameRectangle.pos.y - frameRectangle.height / 2.0;
  for (size_t i = 1; i < count_; ++i)
  {
    frameRectangle = arrayShapes_[i]->getFrameRect();
    maxX = std::max(frameRectangle.pos.x + frameRectangle.width / 2.0, maxX);
    maxY = std::max(frameRectangle.pos.y + frameRectangle.height / 2.0, maxY);
    minX = std::min(frameRectangle.pos.x - frameRectangle.width / 2.0, minX);
    minY = std::min(frameRectangle.pos.y - frameRectangle.height / 2.0, minY);
  }
  return {{(maxX + minX) / 2.0, (maxY + minY) / 2.0}, maxX - minX, maxY - minY};
}

void utin::CompositeShape::move(const double &dx, const double &dy)
{
  checkEmpty();
  for (size_t i = 0; i < count_; ++i)
  {
    arrayShapes_[i]->move(dx, dy);
  }
}

void utin::CompositeShape::move(const point_t &point)
{
  checkEmpty();
  const utin::rectangle_t frameRectangle = getFrameRect();
  const double dx = point.x - frameRectangle.pos.x;
  const double dy = point.y - frameRectangle.pos.y;
  move(dx, dy);
}

void utin::CompositeShape::printInfo() const
{
  checkEmpty();
  const utin::rectangle_t frameRectangle = getFrameRect();
  std::cout << "Composite shape:" << "\n"
            << "Center: (" << frameRectangle.pos.x << ", " << frameRectangle.pos.y << ")" << "\n"
            << "Area: " << getArea() << "\n"
            << "Count: " << count_ << "\n"
            << "Frame rectangle: (width: " << frameRectangle.width << ", height: " << frameRectangle.height << ")\n";
}

void utin::CompositeShape::scale(double multiplier)
{
  checkEmpty();
  if (multiplier <= 0)
  {
    throw std::invalid_argument("Multiplier must be greater that zero");
  }
  const utin::rectangle_t frameRectangleComposite = getFrameRect();
  for (size_t i = 0; i < count_; ++i)
  {
    utin::rectangle_t frameRectangle = arrayShapes_[i]->getFrameRect();
    double dx = frameRectangle.pos.x - frameRectangleComposite.pos.x;
    double dy = frameRectangle.pos.y - frameRectangleComposite.pos.y;
    arrayShapes_[i]->move(dx * (multiplier - 1), dy * (multiplier - 1));
    arrayShapes_[i]->scale(multiplier);
  }
}

void utin::CompositeShape::rotate(double angle)
{
  const utin::point_t center = getFrameRect().pos;
  const double sin = std::abs(std::sin(angle * M_PI / 180));
  const double cos = std::abs(std::cos(angle * M_PI / 180));
  for (size_t i = 0; i < count_; ++i)
  {
    const utin::point_t shapeCenter = arrayShapes_[i]->getFrameRect().pos;
    const double pointX = center.x + (shapeCenter.x - center.x) * cos - (shapeCenter.y - center.y) * sin;
    const double pointY = center.y + (shapeCenter.x - center.x) * sin + (shapeCenter.y - center.y) * cos;
    arrayShapes_[i]->move({pointX, pointY});
    arrayShapes_[i]->rotate(angle);
  }
}

void utin::CompositeShape::checkEmpty() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
}

void utin::CompositeShape::checkRange(const size_t &index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }
}

void utin::CompositeShape::swap(utin::CompositeShape &other) noexcept
{
  std::swap(arrayShapes_, other.arrayShapes_);
  std::swap(count_, other.count_);
}
