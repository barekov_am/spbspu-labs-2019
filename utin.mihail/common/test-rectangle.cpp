#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

const double MISTAKE = 0.001;

BOOST_AUTO_TEST_SUITE(rectangleTesting)

BOOST_AUTO_TEST_CASE(rectangleTestMoving)
{
  utin::Rectangle testRectangle({2.1, 3.9}, 4.5, 5.5);
  const double areaBeforeMoving = testRectangle.getArea();
  const utin::rectangle_t frameRectBeforeMoving = testRectangle.getFrameRect();
  testRectangle.move(-1.1, 2.1);
  BOOST_CHECK_CLOSE(testRectangle.getArea(), areaBeforeMoving, MISTAKE);
  const utin::rectangle_t frameRectAfterMoving = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, MISTAKE);
}

BOOST_AUTO_TEST_CASE(rectangleTestMovingTo)
{
  utin::Rectangle testRectangle({2.1, 3.9}, 4.5, 5.5);
  const double areaBeforeMoving = testRectangle.getArea();
  const utin::rectangle_t frameRectBeforeMoving = testRectangle.getFrameRect();
  testRectangle.move({-1.0, 2.0});
  BOOST_CHECK_CLOSE(testRectangle.getArea(), areaBeforeMoving, MISTAKE);
  const utin::rectangle_t frameRectAfterMoving = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, MISTAKE);
}

BOOST_AUTO_TEST_CASE(rectangleTestScale)
{
  utin::Rectangle testRectangle({2.1, 3.9}, 4.5, 5.5);
  const double areaBeforeScale = testRectangle.getArea();
  const double multiplier = 1.5;
  testRectangle.scale(multiplier);
  BOOST_CHECK_CLOSE(testRectangle.getArea(), areaBeforeScale * multiplier * multiplier, MISTAKE);
}

BOOST_AUTO_TEST_CASE(rectangleTestWidthTrowException)
{
  BOOST_CHECK_THROW(utin::Rectangle({1.1, 1.2}, -1.1, 1.2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleTestHeigthTrowException)
{
  BOOST_CHECK_THROW(utin::Rectangle({1.1, 1.2}, 1.1, -1.2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleTestTrowExceptionScale)
{
  utin::Rectangle testRectangle({2.1, 3.9}, 4.5, 5.5);
  BOOST_CHECK_THROW(testRectangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleTestRotate)
{
  utin::Rectangle testRectangle({2.1, 3.9}, 4.5, 5.5);
  const double rectangleAreaBefore = testRectangle.getArea();
  const utin::rectangle_t frameRectBeforeRotate = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, 5.5, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, 4.5, MISTAKE);
  double angle = 90.0;
  testRectangle.rotate(angle);
  double rectangleAreaAfter = testRectangle.getArea();
  utin::rectangle_t frameRectAfterRotate = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.width, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.height, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, MISTAKE);
  BOOST_CHECK_CLOSE(rectangleAreaBefore, rectangleAreaAfter, MISTAKE);

  angle = -90;
  testRectangle.rotate(angle);
  rectangleAreaAfter = testRectangle.getArea();
  frameRectAfterRotate = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.height, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.width, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, MISTAKE);
  BOOST_CHECK_CLOSE(rectangleAreaBefore, rectangleAreaAfter, MISTAKE);

  angle = 180;
  testRectangle.rotate(angle);
  rectangleAreaAfter = testRectangle.getArea();
  frameRectAfterRotate = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.height, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.width, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, MISTAKE);
  BOOST_CHECK_CLOSE(rectangleAreaBefore, rectangleAreaAfter, MISTAKE);
}

BOOST_AUTO_TEST_SUITE_END()
