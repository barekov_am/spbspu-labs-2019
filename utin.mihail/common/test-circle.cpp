#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double MISTAKE = 0.001;

BOOST_AUTO_TEST_SUITE(circleTesting)

BOOST_AUTO_TEST_CASE(circleTestMoving)
{
  utin::Circle testCircle({2.1, 3.9}, 4.5);
  const double areaBeforeMoving = testCircle.getArea();
  const utin::rectangle_t frameRectBeforeMoving = testCircle.getFrameRect();
  testCircle.move(-1.1, 2.1);
  BOOST_CHECK_CLOSE(testCircle.getArea(), areaBeforeMoving, MISTAKE);
  const utin::rectangle_t frameRectAfterMoving = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, MISTAKE);
}

BOOST_AUTO_TEST_CASE(circleTestMovingTo)
{
  utin::Circle testCircle({2.1, 3.9}, 4.5);
  const double areaBeforeMoving = testCircle.getArea();
  const utin::rectangle_t frameRectBeforeMoving = testCircle.getFrameRect();
  testCircle.move({-1.0, 2.0});
  BOOST_CHECK_CLOSE(testCircle.getArea(), areaBeforeMoving, MISTAKE);
  const utin::rectangle_t frameRectAfterMoving = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, MISTAKE);
}

BOOST_AUTO_TEST_CASE(circleTestScale)
{
  utin::Circle testCircle({2.1, 3.9}, 4.5);
  const double areaBeforeScale = testCircle.getArea();
  const double multiplier = 1.5;
  testCircle.scale(multiplier);
  BOOST_CHECK_CLOSE(testCircle.getArea(), areaBeforeScale * multiplier * multiplier, MISTAKE);
}

BOOST_AUTO_TEST_CASE(circleTestTrowException)
{
  BOOST_CHECK_THROW(utin::Circle testCircle({1.1, 1.2}, -1.1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circleTestTrowExceptionScale)
{
  utin::Circle testCircle({2.1, 3.9}, 4.5);
  BOOST_CHECK_THROW(testCircle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circleTestRotate)
{
  utin::Circle testCircle({2.1, 3.9}, 4.5);
  const double cirlceAreaBefore = testCircle.getArea();
  const utin::rectangle_t frameRectBeforeRotate = testCircle.getFrameRect();
  double angle = 90.0;
  testCircle.rotate(angle);
  double circleAreaAfter = testCircle.getArea();
  utin::rectangle_t frameRectAfterRotate = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.width, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.height, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, MISTAKE);
  BOOST_CHECK_CLOSE(cirlceAreaBefore, circleAreaAfter, MISTAKE);

  angle = -90.0;
  testCircle.rotate(angle);
  circleAreaAfter = testCircle.getArea();
  frameRectAfterRotate = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.height, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.width, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, MISTAKE);
  BOOST_CHECK_CLOSE(cirlceAreaBefore, circleAreaAfter, MISTAKE);

  angle = 180.0;
  testCircle.rotate(angle);
  circleAreaAfter = testCircle.getArea();
  frameRectAfterRotate = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.height, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.width, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, MISTAKE);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, MISTAKE);
  BOOST_CHECK_CLOSE(cirlceAreaBefore, circleAreaAfter, MISTAKE);
}

BOOST_AUTO_TEST_SUITE_END()
