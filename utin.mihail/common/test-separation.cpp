#include <memory>

#include <boost/test/auto_unit_test.hpp>

#include "matrix.hpp"
#include "separation.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testPartitionMethods)

BOOST_AUTO_TEST_CASE(testPartition)
{
  const utin::Circle circle1({0.0, 0.0}, 3.0);
  const utin::Circle circle2({5.0, 5.0}, 1.0);
  const utin::Rectangle rect1({-2.0, 1.0}, 4.0, 4.0);
  const utin::Rectangle rect2({-5.0, 4.0}, 3.0, 6.0);

  std::shared_ptr<utin::Shape> circlePtr = std::make_shared<utin::Circle>(circle1);
  std::shared_ptr<utin::Shape> circlePtr2 = std::make_shared<utin::Circle>(circle2);
  std::shared_ptr<utin::Shape> rectPtr1 = std::make_shared<utin::Rectangle>(rect1);
  std::shared_ptr<utin::Shape> rectPtr2 = std::make_shared<utin::Rectangle>(rect2);

  utin::CompositeShape compositeShape(circlePtr);
  compositeShape.add(rectPtr1);
  compositeShape.add(circlePtr2);
  compositeShape.add(rectPtr2);

  utin::Matrix<utin::Shape> matrix = utin::part(compositeShape);

  BOOST_CHECK(matrix[0][0] == circlePtr);
  BOOST_CHECK(matrix[0][1] == circlePtr2);
  BOOST_CHECK(matrix[1][0] == rectPtr1);
  BOOST_CHECK(matrix[2][0] == rectPtr2);

  //check that we have different size of rows
  BOOST_CHECK_EQUAL(matrix.getRows(), 3);
  BOOST_CHECK_EQUAL(matrix[0].size(), 2);
  BOOST_CHECK_EQUAL(matrix[1].size(), 1);
  BOOST_CHECK_EQUAL(matrix[2].size(), 1);

  const utin::Rectangle rect3({6.0, 6.0}, 2.0, 2.0);
  std::shared_ptr<utin::Shape> rectPtr3 = std::make_shared<utin::Rectangle>(rect3);

  compositeShape.add(rectPtr3);
  matrix = utin::part(compositeShape);

  BOOST_CHECK(matrix[1][1] == rectPtr3);
  //check that we have different size of rows once again(after adding of new shape)
  BOOST_CHECK_EQUAL(matrix.getRows(), 3);
  BOOST_CHECK_EQUAL(matrix[0].size(), 2);
  BOOST_CHECK_EQUAL(matrix[1].size(), 2);
  BOOST_CHECK_EQUAL(matrix[2].size(), 1);
}

BOOST_AUTO_TEST_CASE(testIntersect)
{
  const utin::Rectangle rect({0.0, 0.0}, 6.0, 5.0);

  const utin::Rectangle rect1({3.0, 3.0}, 4.0, 4.0);
  const utin::Rectangle rect2({-3.0, 2.0}, 4.0, 2.0);
  const utin::Rectangle rect3({-3.0, -2.0}, 2.0, 2.0);
  const utin::Rectangle rect4({3.0, -2.0}, 2.0, 2.0);
  const utin::Circle circle({30.0, -10.0}, 4.0);

  BOOST_CHECK(utin::intersect(rect.getFrameRect(), rect1.getFrameRect()));
  BOOST_CHECK(utin::intersect(rect.getFrameRect(), rect2.getFrameRect()));
  BOOST_CHECK(utin::intersect(rect.getFrameRect(), rect3.getFrameRect()));
  BOOST_CHECK(utin::intersect(rect.getFrameRect(), rect4.getFrameRect()));
  BOOST_CHECK(!utin::intersect(rect.getFrameRect(), circle.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
