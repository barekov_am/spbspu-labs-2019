#include "rectangle.hpp"
#include <iostream>
#include <cmath>

utin::Rectangle::Rectangle(const utin::point_t &center, const double &width, const double &height):
  center_(center),
  width_(width),
  height_(height),
  angle_(0)
{
  if (height_ <= 0.0)
  {
    throw std::invalid_argument("Height must be greater that zero");
  }
  if (width_ <= 0.0)
  {
    throw std::invalid_argument("Width must be greater that zero");
  }
}

utin::Rectangle::Rectangle(const utin::point_t &center, const double &width, const double &height, const double &angle):
  Rectangle(center, width, height)
{
  rotate(angle);
}

double utin::Rectangle::getArea() const
{
  return width_ * height_;
}

utin::rectangle_t utin::Rectangle::getFrameRect() const
{
  const double sin = std::sin(angle_ * M_PI / 180);
  const double cos = std::cos(angle_ * M_PI / 180);
  const double height = width_ * std::abs(sin) + height_ * std::abs(cos);
  const double width = width_ * std::abs(cos) + height_ * std::abs(sin);
  return {center_, width, height};
}

void utin::Rectangle::move(const double &dx, const double &dy)
{
  center_.x += dx;
  center_.y += dy;
}

void utin::Rectangle::move(const point_t &point)
{
  center_ = point;
}

void utin::Rectangle::scale(double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("Multiplier must be greater that zero");
  }
  width_ *= multiplier;
  height_ *=multiplier;
}

void utin::Rectangle::rotate(double angle)
{
  angle_ += angle;
}

void utin::Rectangle::printInfo() const
{
  std::cout << "Width of rectangle is " << width_ << '\n'
            << "Height of rectangle is " << height_ << '\n'
            << "Center of rectangle is (" << center_.x << ";" << center_.y << ")" << '\n'
            << "Angle of recntangle is " << angle_ << '\n';
}
