#include <iostream>
#include "phoneBookInterface.hpp"
PhoneBookInterface::PhoneBookInterface(PhoneBook &phoneBook) :
  phoneBook_(phoneBook)
{
}

void PhoneBookInterface::add(std::istream &input)
{
  std::string phoneNumber;
  input >> phoneNumber;
  std::string name;
  input.ignore();
  std::getline(input, name);

  if ((name.empty()) || (phoneNumber.empty()))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if ((name.front() != '"') || (name.back() != '"'))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  makeReadable(name);

  phoneBook_.insertAtTheEnd(name, phoneNumber);
}

void PhoneBookInterface::store(std::istream &input)
{
  std::string name;
  std::string newName;
  input >> name >> newName;

  if ((name.empty()) || (newName.empty()))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  phoneBook_.store(name, newName);
}

void PhoneBookInterface::insert(std::istream &input)
{
  std::string position;
  std::string mark;
  std::string phoneNumber;
  input >> position >> mark >> phoneNumber;
  std::string name;
  input.ignore();
  std::getline(input, name);

  if ((position.empty()) || (mark.empty()) || (phoneNumber.empty()) || (name.empty()))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  if ((name.front() != '"') || (name.back() != '"'))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  makeReadable(name);

  if (position == "before")
  {
    phoneBook_.insertBefore(name, phoneNumber, mark);
  }
  else if(position == "after")
  {
    phoneBook_.insertAfter(name, phoneNumber, mark);
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
  }
}

void PhoneBookInterface::remove(std::istream &input)
{
  std::string bookMark;
  input >> bookMark;
  phoneBook_.deleteRecord(bookMark);
}

void PhoneBookInterface::show(std::istream &input)
{
  std::string bookMark;
  input >> bookMark;
  if (!phoneBook_.makeCurrent(bookMark))
  {
    return;
  }
  else if (phoneBook_.isEmpty())
  {
    std::cout << "<EMPTY>\n";
  }
  else
  {
    std::cout << phoneBook_.getRecord().second << " " << phoneBook_.getRecord().first << '\n';
  }
}

void PhoneBookInterface::move(std::istream &input)
{
  std::string bookMark;
  std::string steps;
  input >> bookMark >> steps;
  phoneBook_.move(steps, bookMark);
}

void PhoneBookInterface::makeReadable(std::string &name)
{
  name = name.substr(1, name.size() - 2);
  for (std::size_t i = 0; i < name.size(); ++i)
  {
    if ((name[i] == '\\') && ((name[i + 1] == '"') || (name[i + 1] == '\\')))
    {
      name.erase(i, 1);
    }
    else if (name[i] == '"')
    {
      name.erase(i, 1);
    }
  }
}
