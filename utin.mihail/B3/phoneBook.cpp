#include "phoneBook.hpp"
#include <stdexcept>
PhoneBook::PhoneBook()
{
  bookmarks_.insert(std::pair<std::string, std::list<record>::iterator>("current", records_.end()));
  currentBookmark_ = bookmarks_.begin()->second;
}

PhoneBook::record PhoneBook::getRecord() const
{
  return *currentBookmark_;
}

void PhoneBook::store(const std::string &markName, const std::string &newName)
{
  if (!makeCurrent(markName))
  {
    return;
  }
    std::list<record>::iterator iterator = bookmarks_.at(markName);
    bookmarks_.emplace(newName, iterator);
}

bool PhoneBook::makeCurrent(const std::string &markName)
{
  try {

    std::list<record>::iterator it = bookmarks_.at(markName);
    currentBookmark_ = it;
    return true;
  }
  catch (std::out_of_range)
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return false;
  }
}


void PhoneBook::nextRecord(std::string &markName)
{
  if (!makeCurrent(markName))
  {
    return;
  }
  std::size_t distance = std::distance(records_.begin(), currentBookmark_);
  if (distance < records_.size())
  {
    currentBookmark_++;
    bookmarks_.at(markName) = currentBookmark_;
  }
  else
  {
    return;
  }
}

void PhoneBook::previousRecord(std::string &markName)
{
  if (!makeCurrent(markName))
  {
    return;
  }
  std::size_t distance = std::distance(records_.begin(), currentBookmark_);
  if (distance < records_.size())
  {
    currentBookmark_++;
    bookmarks_.at(markName) = currentBookmark_;
  }
  else
  {
    return;
  }
}

void PhoneBook::insertBefore(const std::string &name, const std::string &phoneNumber, const std::string &markName)
{
  if (records_.empty())
  {
    init(name, phoneNumber);
    return;
  }
  if (!makeCurrent(markName))
  {
    return;
  }
  std::list<record>::iterator iterator = bookmarks_.at(markName);
  records_.emplace(iterator, name, phoneNumber);
}

void PhoneBook::insertAfter(const std::string &name, const std::string &phoneNumber, const std::string &markName)
{
  if (records_.empty())
  {
    init(name, phoneNumber);
    return;
  }
  if (!makeCurrent(markName))
  {
    return;
  }
  std::list<record>::iterator iterator = bookmarks_.at(markName);
  records_.emplace(++iterator, name, phoneNumber);
}

void PhoneBook::deleteRecord(const std::string &markName = "current")
{
  if (!makeCurrent(markName))
  {
    return;
  }

  std::list<record>::iterator iterator = bookmarks_.at(markName);
  std::list<record>::iterator nextIterator = std::next(iterator);
  if (nextIterator == records_.end())
  {
    nextIterator = records_.begin();
  }
  for (std::map<std::string, std::list<record>::iterator>::iterator it = bookmarks_.begin(); it != bookmarks_.end(); ++it)
  {
    if (it->second == iterator)
    {
      it->second = nextIterator;
    }
  }
  if (currentBookmark_ == iterator)
  {
    currentBookmark_ = nextIterator;
  }
  records_.erase(iterator);
}
  

void PhoneBook::replaceRecord(const std::string &name, const std::string &phoneNumber)
{
  if ((currentBookmark_ == records_.end()) && (records_.size() > 1))
  {
    deleteRecord();
    insertAtTheEnd(name, phoneNumber);
  }
  else
  {
    deleteRecord();
    insertBefore(name, phoneNumber, currentBookmark_->first);
  }
}

void PhoneBook::insertAtTheEnd(const std::string &name, const std::string &phoneNumber)
{
  if (records_.empty())
  {
    init(name, phoneNumber);
    return;
  }
  records_.emplace_back(name, phoneNumber);
  if (currentBookmark_ == records_.end())
  {
    currentBookmark_--; 
  }
}

void PhoneBook::move(const std::string &steps, const std::string &markName = "current")
{
  if (!makeCurrent(markName))
  {
    return;
  }
  if (steps == "first")
  {
    currentBookmark_ = records_.begin();
    bookmarks_.at(markName) = currentBookmark_;
    
    return;
  }
  if (steps == "last")
  {
    currentBookmark_ = std::prev(records_.end());
    bookmarks_.at(markName) = currentBookmark_;
    return;
  }

  std::istringstream string(steps);
  int distance = 0;

  string >> distance;

  if (string.fail())
  {
    std::cout << "<INVALID STEP>\n";
  }

  int minDistance = distance + std::distance(records_.begin(), currentBookmark_);
  unsigned int maxDistance = static_cast<unsigned int>(distance + (std::distance(records_.begin(), currentBookmark_)));
  if ((minDistance < 0) || (maxDistance >= records_.size()))
  {
    return;
  }

  std::advance(currentBookmark_, distance);
  bookmarks_.at(markName) = currentBookmark_;
}

bool PhoneBook::isEmpty() const
{
  return records_.empty();
}

void PhoneBook::init(const std::string &name, const std::string &phoneNumber)
{
  records_.emplace_back(name, phoneNumber);
  bookmarks_.at("current") = records_.begin();
  currentBookmark_ = records_.begin();
}
