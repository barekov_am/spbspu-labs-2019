#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <utility>
#include <string>
#include <list>
#include <sstream>
#include <iostream>
#include <map>

class PhoneBook
{
  public:

    typedef std::pair<std::string, std::string> record;

    PhoneBook();
    ~PhoneBook() = default;
    
    
    record getRecord() const;
    
    bool makeCurrent(const std::string &);
    void nextRecord(std::string &);
    void previousRecord(std::string &markName);
    void store(const std::string &, const std::string &);
    void insertBefore(const std::string &, const std::string &, const std::string &);
    void insertAfter(const std::string &, const std::string &, const std::string &);
    void deleteRecord(const std::string &);
    void replaceRecord(const std::string &, const std::string &);
    void move(const std::string &, const std::string &);
    void insertAtTheEnd(const std::string &, const std::string &);
    bool isEmpty() const;
    void init(const std::string &, const std::string &);
  private:
    std::list<record> records_;
    std::map<std::string, std::list<record>::iterator> bookmarks_;   
    std::list<record>::iterator currentBookmark_;
};
#endif
