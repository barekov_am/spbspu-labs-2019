#ifndef PHONEBOOK_INTERFACE_HPP
#define PHONEBOOK_INTERFACE_HPP

#include "phoneBook.hpp"

class PhoneBookInterface
{
  public:
    PhoneBookInterface(PhoneBook &);
    PhoneBookInterface() = delete;
    ~PhoneBookInterface() = default;
    
   
    void add(std::istream &);
    void store(std::istream &);
    void insert(std::istream &);
    void remove(std::istream &);
    void show(std::istream &);
    void move(std::istream &);
    void makeReadable(std::string &);
  private:
    PhoneBook &phoneBook_;
    
};

#endif
