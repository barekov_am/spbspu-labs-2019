#include "tasks.hpp"
#include "phoneBook.hpp"
#include "phoneBookInterface.hpp"

int task1()
{
  PhoneBook phoneBook;
  PhoneBookInterface interface(phoneBook);
  std::string line;
  std::stringstream string;
  while (std::getline(std::cin, line))
  {
    string.clear();
    string.str(line);
    if (std::cin.fail())
    {
      return 2;
    }
    if (std::cin.eof())
    {
      break;
    }
    std::string command;
    string >> command;
    if (command.empty())
    {
      return 0;
    }
    else if (command == "add")
    {
      interface.add(string);
    }
    else if (command == "store")
    {
      interface.store(string);
    }
    else if (command == "insert")
    {
      interface.insert(string);
    }
    else if (command == "delete")
    {
      interface.remove(string);
    }
    else if (command == "show")
    {
      interface.show(string);
    }
    else if (command == "move")
    {
      interface.move(string);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
  return 0;
}
