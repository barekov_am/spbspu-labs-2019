#include <iostream>
#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "separation.hpp"
#include "stddef.h"

int main()
{
  std::cout << "----Rectangle----\n";
  utin::Rectangle rect({5.3, 5.2}, 15, 10);
  utin::Shape * shape = &rect;
  shape->printInfo();
  std::cout << "Shift to (2.3,2.5)!\n";
  shape->move(2.3, 2.5);
  shape->printInfo();
  std::cout << "Move to (10,24)!\n";
  shape->move({10, 24});
  shape->printInfo();
  std::cout << "Scale in twice!\n";
  shape->scale(2);
  shape->printInfo();
  std::cout << "Rotate by 45 degrees!\n";
  shape->rotate(45);
  shape->printInfo();

  std::cout << "----Circle----\n";
  utin::Circle circle({3.0, 3.5}, 5.7);
  shape = &circle;
  shape->printInfo();
  std::cout << "Shift to (7,1)!\n";
  shape->move(7, 1);
  shape->printInfo();
  std::cout << "Move to (-27,9)!\n";
  shape->move({-27, 9});
  shape->printInfo();
  std::cout << "Scale in 0.5 times!\n";
  shape->scale(0.5);
  shape->printInfo();
  std::cout << "Rotate by 45 degrees!\n";
  shape->rotate(45);
  shape->printInfo();

  //create some shapes
  utin::Circle circle1({1.2, 1.2}, 3.0);
  utin::Circle circle2({-0.2, 5.3}, 2.0);
  utin::Rectangle rect1({1.6, 1.6}, 5.0, 3.0);
  utin::Rectangle rect2({-7.3, 2.6}, 8.2, 5.3);

  std::cout << "Create composite shape:\n";
  utin::CompositeShape compositeShape(std::make_shared<utin::Circle>(circle1));

  compositeShape.printInfo();

  std::cout << "Add other shapes in composite:\n";
  compositeShape.add(std::make_shared<utin::Rectangle>(rect1));
  compositeShape.add(std::make_shared<utin::Rectangle>(rect2));
  compositeShape.add(std::make_shared<utin::Circle>(circle2));
  compositeShape.printInfo();

  std::cout << "Shift at (3,3)\n";
  compositeShape.move(3, 3);
  compositeShape.printInfo();

  std::cout << "Remove shapes which have index 2 and 3\n";
  compositeShape.remove(3);
  compositeShape.remove(2);
  compositeShape.printInfo();

  std::cout << "Move to {-4,7}\n";
  compositeShape.move({-4, 7});
  compositeShape.printInfo();

  std::cout << "Get shape which has index 0 and print information about it:\n";
  std::shared_ptr<utin::Shape> shapeFromComposite = compositeShape[0];
  shapeFromComposite->printInfo();

  std::cout << "Scale composite shape in 0,8 times\n\n";
  compositeShape.scale(0.8);
  compositeShape.printInfo();

  std::cout << "Add new shape\n";
  compositeShape.add(std::make_shared<utin::Circle>(circle2));
  compositeShape.printInfo();

  std::cout << "Rotate composite shape by 30 degrees\n";
  compositeShape.rotate(30.0);
  compositeShape.printInfo();

  std::cout << "Get partition of composite shape\n";
  utin::Matrix<utin::Shape> matrix = utin::part(compositeShape);
  size_t layer = matrix.getRows();
  for (std::size_t i = 0; i < layer; ++i)
  {
    std::cout << "Layer: " << i + 1 << "\n";
    std::cout << "Shapes in the layer:\n";
    size_t columns = matrix[i].size();
    for (size_t j = 0; j < columns; ++j)
    {
      if (matrix[i][j] != nullptr)
      {
        std::cout << j + 1 << ":\n";
        std::cout << "Area: " << matrix[i][j]->getArea() << "\n";
        const utin::rectangle_t frameRect = matrix[i][j]->getFrameRect();
        std::cout << "Position: (" << frameRect.pos.x << "," << frameRect.pos.y << ")\n";
      }
    }
  }

  return 0;
}
