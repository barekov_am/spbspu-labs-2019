#include <fstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <vector>
#include "tasks.hpp"

void utin::task2(char* filename)
{
  const size_t InitialSize = 1024;

  std::ifstream file(filename);
  if (!file)
  {
    throw std::ios_base::failure("File does not exist.\n");
  }

  size_t size = InitialSize;

  std::unique_ptr<char[], decltype(&free)> arr(static_cast<char*>(malloc(size)), &free);

  size_t i = 0;
  while (file)
  {
    file.read(&arr[i], InitialSize);
    i += file.gcount();

    if (file.gcount() == InitialSize) 
    {
      size += InitialSize;
      std::unique_ptr<char[], decltype(&free)> temp(static_cast<char*>(realloc(arr.get(), size)), &free);

      if (temp)
      {
        arr.release();
        std::swap(arr, temp);
      }
      else
      {
        throw std::runtime_error("Failed to reallocate.\n");
      }
    }
  }
  file.close();

  if (file.is_open())
  {
    throw std::ios_base::failure("Failed to close file.\n");
  }
  
  std::vector<char> vectChar(&arr[0], &arr[i]);

  for (char c : vectChar)
  {
    std::cout << c;
  }
}
