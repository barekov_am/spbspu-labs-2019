#include "tasks.hpp"

int main(int argc, char* argv[])
{
  
  try
  {
    if (argc <= 1)
    {
      throw std::invalid_argument("No argument.\n");
    }
    if (*argv[1] == '1' && argc == 3)
    {
      utin::task1(argv[2], std::cin);
    }
    else if (*argv[1] == '2' && argc == 3)
    {
      utin::task2(argv[2]);
    }
    else if (*argv[1] == '3' && argc == 2)
    {
      utin::task3();
    }
    else if (*argv[1] == '4' && argc == 4)
    {
      utin::task4(argv[2],argv[3]);
    }
    else
    {
      throw std::invalid_argument("invalid input data.\n");
    }

  }
  catch (const std::exception &exc)
  {
    std::cerr << exc.what();
    return 1;
  }
  return 0;
}
