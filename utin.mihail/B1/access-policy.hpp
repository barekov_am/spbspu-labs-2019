#ifndef ACCESS_POLICY_HPP
#define ACCESS_POLICY_HPP

#include <cstddef>

namespace utin
{
  template<class Container>
  struct operator_access
  {
    typedef typename Container::value_type value_type;
    typedef typename std::size_t index_type;

    static index_type begin(const Container &)
    {
      return 0;
    }

    static index_type end(const Container &cont)
    {
      return cont.size();
    }

    static value_type &getElem(Container &cont, index_type index)
    {
      return cont[index];
    }

    static index_type next(index_type index)
    {
      return ++index;
    }
  };

  template<class Container>
  struct at_access
  {
    typedef typename Container::value_type value_type;
    typedef typename std::size_t index_type;

    static index_type begin(const Container &)
    {
      return 0;
    }

    static index_type end(const Container &cont)
    {
      return cont.size();
    }

    static value_type &getElem(Container &cont, index_type index)
    {
      return cont.at(index);
    }

    static index_type next(index_type index)
    {
      return ++index;
    }
  };

  template<class Container>
  struct iterator_access
  {
    typedef typename Container::value_type value_type;
    typedef typename Container::iterator index_type;

    static index_type begin(Container &cont)
    {
      return cont.begin();
    }

    static index_type end(Container &cont)
    {
      return cont.end();
    }

    static value_type &getElem(Container &, index_type index)
    {
      return *index;
    }

    static index_type next(index_type index)
    {
      return ++index;
    }
  };
};
#endif 
