#ifndef TASKS_HPP
#define TASKS_HPP
#include <iostream>

namespace utin
{

  void task1(char *, std::istream &);
  void task2(char *);
  void task3();
  void task4(char *, char *);

};

#endif
