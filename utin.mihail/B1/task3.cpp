#include <vector>
#include <iostream>
#include "tasks.hpp"
#include "print.hpp"

void utin::task3()
{
  std::vector<int> vect;
  int number;
  bool checkZero = false;
  while (std::cin && !(std::cin >> number).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Wrong input\n");
    }
    if (number != 0)
    {
      vect.push_back(number);
    }
    else
    {
      checkZero = true;
      break;
    }
  }

  if (vect.empty())
  {
    return;
  }
  else if (!checkZero && std::cin.eof())
  {
    throw std::invalid_argument("Wrong input\n");
  }
  else 
  {
    std::vector<int>::iterator it = vect.end() - 1;
    {
      if (*it == 1)
      {
        for (it = vect.begin(); it < vect.end();)
        {
          if ((*it % 2) == 0)
          {
            it = vect.erase(it);
          }
          else
          {
            ++it;
          }
        }
      }
      else if (*it == 2)
      {
        for (it = vect.begin(); it < vect.end();)
        {
          if ((*it % 3) == 0)
          {

            for (std::size_t i = 0; i < 3; ++i)
            {
              it = vect.insert(it + 1, 1);
            }
          }
          else
          {
            ++it;
          }
        }
      }
    }
  }
  
  printContainer(vect);
}
