#ifndef SORT_HPP
#define SORT_HPP

#include "access-policy.hpp"
#include <functional>
#include <cstring>

namespace utin
{ 
  template <class T>
  bool more(T a, T b)
  {
    return b < a;
  }

  template <class T>
  bool less(T a, T b)
  {
    return a < b;
  }

  template<class T>
  std::function<bool(T, T)> getDirection(char *direction)
  {
    if (!std::strcmp(direction, "ascending"))
    {
      return more<T>;
    }
    else if (!strcmp(direction, "descending"))
    {
      return less<T>;
    }

    throw std::invalid_argument("Invalid direction\n");
  }

  template<template<class> class AccessPolicy, typename Container>
  void sort(Container &container, std::function<bool(typename Container::value_type, typename Container::value_type)> compare)
  {
    typedef AccessPolicy<Container> Policy;

    for (auto i = Policy::begin(container); i != Policy::end(container); ++i)
    {
      for (auto j = Policy::next(i); j != Policy::end(container); ++j)
      {
        if (compare(Policy::getElem(container, i), Policy::getElem(container, j)))
        {
          std::swap(Policy::getElem(container, i), Policy::getElem(container, j));
        }
      }
    }
  }
};
#endif
