#ifndef PRINT_HPP
#define PRINT_HPP
#include <iostream>
#include <iomanip>

namespace utin
{
  template <class T>
  void printContainer(const T &container, bool space = true, std::size_t precision = 1)
  {
    if (space)
    {
      for (auto i = container.begin(); i != container.end(); ++i)
      {
        std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(precision) << *i << " ";
      }
    }
    else
    {
      for (auto i = container.begin(); i != container.end(); ++i)
      {
        std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(precision) << *i;
      }
    }

    std::cout << std::endl;
  }
 };
#endif
