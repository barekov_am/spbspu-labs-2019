#include <vector>
#include <string>
#include "tasks.hpp"
#include "print.hpp"
#include "sort.hpp"

namespace utin
{
  void fillRandom(double *array, std::size_t size)
  {
    for (std::size_t i = 0; i < size; ++i)
    {
      array[i] = (rand() % 21 - 10) / 10.0;
    }
  }
}

void utin::task4(char *direction, char *size)
{
  std::size_t number;
  try
  {
    number = std::stoi(size);
  }
  catch (...)
  {
    throw std::invalid_argument("Wrong lenght\n");
  }
  srand(time(0));
  std::unique_ptr<double[]> array = std::make_unique<double[]>(number);
  fillRandom(array.get(), number);
  std::vector<double> vect(array.get(),array.get() + number);
  
  auto compare = getDirection<double>(direction);
  printContainer(vect);
  sort<at_access>(vect, compare);
  printContainer(vect);
}
