#include <string>
#include <vector>
#include <forward_list>
#include "tasks.hpp"
#include "print.hpp"
#include "sort.hpp"

void utin::task1(char *direction, std::istream &data)
{
  auto compare = getDirection<int>(direction);
  std::vector<int> vectorAt;
  int number;
  while (data >> number)
  {
    vectorAt.push_back(number);
    
  };
  if (!data.eof())
  {
    throw std::ios_base::failure("Wrong input\n");
  }
  std::forward_list<int> list(vectorAt.begin(), vectorAt.end());
  std::vector<int> vectorOperator(vectorAt.begin(), vectorAt.end());
  
  sort<at_access>(vectorAt, compare);
  printContainer(vectorAt);

  sort<operator_access>(vectorOperator, compare);
  printContainer(vectorOperator);
  
  sort<iterator_access>(list, compare);
  printContainer(list);
}
