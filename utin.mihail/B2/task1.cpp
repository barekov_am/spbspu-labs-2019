#include "tasks.hpp"
#include "queue-implementation.hpp"
#include <sstream>
#include <iostream>
#include <string>

void utin::task1() {

  std::string word;
  std::string command;
  utin::ElementPriority priority;
  utin::QueueWithPriority<std::string> queue;
  std::stringstream buffer;

  while (std::getline(std::cin >> std::ws, command)) {
    buffer.clear();
    buffer.str(command);
    word.clear();
    buffer >> std::ws >> word;

    if (word == "add") {

      buffer >> std::ws >> word;

      if (word == "high") {
        priority = utin::HIGH;
      }
      else if (word == "normal") {
        priority = utin::NORMAL;
      }
      else if (word == "low") {
        priority = utin::LOW;
      }
      else {
        std::cout << "<INVALID COMMAND>" << std::endl;
        continue;
      }

      word.clear();
      buffer.ignore();
      std::getline(buffer >> std::ws, word);

      if (word.empty()) {
        std::cout << "<INVALID COMMAND>" << std::endl;
      }
      else
      {
        queue.PutElementToQueue(word, priority);
      }
    }
    else if ((word == "get") && ((buffer >> std::ws).eof())) {
      std::cout << queue.GetElementFromQueue() << std::endl;
    }
    else if ((word == "accelerate") && ((buffer >> std::ws).eof())) {
      queue.Accelerate();
    }
    else {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }
  }
}
