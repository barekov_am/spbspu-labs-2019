#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <list>
#include <stdexcept>

namespace utin {

  typedef enum {
    LOW,
    NORMAL,
    HIGH
  }
  ElementPriority;


  template<typename Element_t>
  class QueueWithPriority {
  public:
    QueueWithPriority() = default;
    ~QueueWithPriority() = default;

    void PutElementToQueue(const Element_t & element, utin::ElementPriority priority);

    Element_t GetElementFromQueue();

    void Accelerate();

  private:
    std::list<Element_t> high_;
    std::list<Element_t> normal_;
    std::list<Element_t> low_;
  };

}
#endif 
