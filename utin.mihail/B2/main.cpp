#include "tasks.hpp"
#include <iostream>
#include <stdexcept>

int main(int argc, char * argv[]) {

  try {
    if (argc != 2) {
      throw(std::invalid_argument("Wrong number of paramrters."));
    }
    switch (*argv[1]) {
    case '1':
      utin::task1();
      break;

    case '2':
      utin::task2();
      break;

    default:
      throw(std::invalid_argument("Wrong task number."));
    }
  }
  catch (const std::exception &err) {
    std::cerr << err.what();
    return 1;
  }
  return 0;

}
