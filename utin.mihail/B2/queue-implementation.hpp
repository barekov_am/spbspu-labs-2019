#ifndef QUEUE_IMPLEMENTATION_HPP
#define QUEUE_IMPLEMENTATION_HPP
#include "queue.hpp"

template<typename Element_t>
void utin::QueueWithPriority<Element_t>::PutElementToQueue(const Element_t &element, utin::ElementPriority priority) {
  switch (priority) {
  case utin::HIGH: {
    std::list<Element_t> & data_h = high_;
    data_h.push_back(element);
    break;
  }

  case utin::NORMAL: {
    std::list<Element_t> & data_n = normal_;
    data_n.push_back(element);
    break;
  }

  case utin::LOW: {
    std::list<Element_t> & data_l = low_;
    data_l.push_back(element);
    break;
  }

  default: {
    throw(std::invalid_argument("Unknown priority type."));
  }
  }
}
#endif

template<typename Element_t>
Element_t utin::QueueWithPriority<Element_t>::GetElementFromQueue() {
  if (!high_.empty()) {
    Element_t front = high_.front();
    high_.pop_front();
    return front;
  }
  else if (!normal_.empty()) {
    Element_t front = normal_.front();
    normal_.pop_front();
    return front;
  }
  else if (!low_.empty()) {
    Element_t front = low_.front();
    low_.pop_front();
    return front;
  }
  else
  {
    return "<EMPTY>";
  }
}

template <typename Element_t>
void utin::QueueWithPriority<Element_t>::Accelerate() {
  std::move(low_.begin(), low_.end(), std::back_inserter(high_));
  low_.clear();
}
