#include "tasks.hpp"
#include <list>
#include <iostream>
#include <stdexcept>

const int MAX_AMOUNT = 20;
const int MIN_AMOUNT = 1;

void utin::task2() {
  std::list<int> list;
  int element;

  while ((std::cin >> element) && (!std::cin.eof())) {
    if ((element < MIN_AMOUNT) || (element > MAX_AMOUNT)) {
      throw(std::invalid_argument("Value does not match allowed range."));
    }
    list.push_back(element);
  }

  if (!std::cin.eof()) {
    throw(std::invalid_argument("Wrong input!"));
  }

  if (list.size() > MAX_AMOUNT) {
    throw(std::invalid_argument("Too many numbers!"));
  }

  if (list.empty()) {
    return;
  }
  else
  {
    std::list<int>::iterator begin_it = list.begin();
    std::list<int>::iterator end_iter = list.end();
    end_iter--;

    if (list.size() == MIN_AMOUNT) {
      std::cout << *begin_it << std::endl;
    }
    else
    {
      do {
        std::cout << *begin_it << " " << *end_iter;
        begin_it++;
        if (begin_it == end_iter) {
          break;
        }
        else
        {
          std::cout << " ";
        }
        end_iter--;
      } while (begin_it != end_iter);
      if (list.size() % 2 == 1) {
        std::cout << *begin_it;
      }
      std::cout << std::endl;

    }

  }
}
