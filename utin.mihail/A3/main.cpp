#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include <memory>

int main()
{
  std::cout << "Rectangle\n";
  utin::Rectangle rect1({4.3, 8.0}, 6.4, 7.5);
  utin::CompositeShape comShape(std::make_shared<utin::Rectangle>(rect1));
  comShape.printInfo();

  std::cout << "\nCircle\n";
  comShape.remove(0);
  utin::Circle circle1({5.0, 4.5}, 3.1);
  comShape.add(std::make_shared<utin::Circle>(circle1));
  comShape.printInfo();

  std::cout << "\nRectangle + 2 Circle\n";
  comShape.add(std::make_shared<utin::Rectangle>(rect1));
  utin::Circle circle2({3.0, 6.1}, 5);
  comShape.add(std::make_shared<utin::Circle>(circle2));
  comShape.printInfo();

  std::cout << "\nShift at (2;-3)\n";
  comShape.move(2, -3);
  comShape.printInfo();

  std::cout << "\nMove to (-2;3)\n";
  comShape.move(-2, 3);
  comShape.printInfo();

  std::cout << "\nScale in 1.5\n";
  comShape.scale(1.5);
  comShape.printInfo();

  std::cout << "\nCopy composite shape\n";
  utin::CompositeShape comShapeCopy(comShape);
  comShapeCopy.printInfo();

  std::cout << "\nGet shape (rectangle)\n";
  std::shared_ptr<utin::Shape> shape = comShape[1];
  shape->printInfo();
  
  return 0;
}
