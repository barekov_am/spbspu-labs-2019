#include "tasks.hpp"

#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <functional>
#include <stdexcept>

#include "shape.hpp"

bool sortCompare(const lab::Shape& lhs, const lab::Shape& rhs);

void lab::task2()
{
  typedef std::istream_iterator< lab::Shape > inputIterator;
  std::vector< lab::Shape > shapes = std::vector< lab::Shape >(inputIterator(std::cin), inputIterator());
  
  if (!std::cin.eof())
  {
    throw std::runtime_error("Stream reading failed");
  }

  std::cout << "Vertices: " << std::accumulate(shapes.begin(), shapes.end(), 0,
      [](int verticies, lab::Shape shape) { return verticies + shape.size(); }) << "\n";

  std::cout << "Triangles: " << std::count_if(shapes.begin(), shapes.end(),
      [](const lab::Shape& shape) { return shape.size() == 3; }) << "\n";
  std::cout << "Squares: " << std::count_if(shapes.begin(), shapes.end(), lab::isSquare) << "\n";
  std::cout << "Rectangles: " << std::count_if(shapes.begin(), shapes.end(), lab::isRectangle) << "\n";

  shapes.erase(std::remove_if(shapes.begin(), shapes.end(), 
      [](const lab::Shape& shape) { return shape.size() == 5; }), shapes.end());

  std::vector< lab::Point > points;
  auto func = [](const lab::Shape& shape, std::vector< lab::Point >& points) { points.push_back(shape[0]); };
  std::for_each(shapes.begin(), shapes.end(), std::bind(func, std::placeholders::_1, std::ref(points)));

  std::cout << "Points: ";
  std::copy(points.begin(), points.end(), std::ostream_iterator< lab::Point >(std::cout, " "));
  std::cout << "\n";

  std::sort(shapes.begin(), shapes.end(), sortCompare);
  std::cout << "Shapes:\n";
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator< lab::Shape >(std::cout, "\n"));
}

bool sortCompare(const lab::Shape& lhs, const lab::Shape& rhs)
{
  size_t lhsSize = lhs.size();
  size_t rhsSize = rhs.size();

  if (lhsSize != 4 && lhsSize != 3)
  {
    return false;
  }

  if (lhsSize != rhsSize)
  {
    return lhsSize < rhsSize;
  }

  if (isSquare(lhs) && !isSquare(rhs))
  {
    return true;
  }

  if (isRectangle(lhs) && !isRectangle(rhs))
  {
    return true;
  }

  return false;
}
