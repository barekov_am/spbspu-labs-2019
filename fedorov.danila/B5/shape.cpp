#include "shape.hpp"

#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>

#include "manipulators.hpp"
#include "stream-state-saver.hpp"

lab::Vector lab::operator-(const Point& lhs, const Point& rhs)
{
  return Vector{ lhs.x - rhs.x, lhs.y - rhs.y };
}

std::istream& lab::operator>>(std::istream& stream, Shape& shape)
{
  shape.clear();

  if (stream.fail())
  {
    return stream;
  }

  size_t amount = 0;
  stream >> std::ws >> amount;

  if (stream.fail())
  {
    return stream;
  }

  Shape temp;
  for (size_t i = 0; i < amount; ++i)
  {
    Point point;
    stream >> point; 

    if (stream.fail())
    {
      return stream;
    }

    temp.push_back(point);
  }

  stream >> lab::skipDelimiter('\n');

  if (stream.fail() && !stream.eof())
  {
    return stream;
  }

  stream.clear();

  shape = std::move(temp);
  return stream;
}

std::ostream& lab:: operator<<(std::ostream& stream, const Shape& shape)
{
  stream << shape.size() << " ";

  std::copy(shape.begin(), shape.end(), std::ostream_iterator< Point >(stream, " "));

  return stream;
}

bool lab::isRectangle(const Shape& shape)
{
  if (shape.size() != 4)
  {
    return false;
  }

  Vector AB = shape[1] - shape[0];
  Vector BC = shape[2] - shape[1];
  Vector DC = shape[3] - shape[2];
  Vector AD = shape[3] - shape[0];

  Vector AC = shape[2] - shape[0];
  Vector BD = shape[3] - shape[1];
  
  return compareVectorsLength(AB, DC) && compareVectorsLength(BC, AD) && compareVectorsLength(AC, BD);
}

bool lab::isSquare(const Shape& shape)
{
  if (!isRectangle(shape))
  {
    return false;
  }
  Vector AB = shape[1] - shape[0];
  Vector BC = shape[2] - shape[1];

  return compareVectorsLength(AB, BC);
}

bool lab::compareVectorsLength(const Vector& first, const Vector& second)
{
  return (first.x * first.x + first.y * first.y) == (second.x * second.x + second.y * second.y);
}
