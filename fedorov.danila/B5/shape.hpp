#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>
#include <vector>

#include "point.hpp"

namespace lab
{
  struct Vector
  {
    int x, y;
  };

  Vector operator-(const Point& lhs, const Point& rhs);

  using Shape = std::vector< Point >;

  std::istream& operator>>(std::istream&, Shape&);
  
  std::ostream& operator<<(std::ostream&, const Shape&);

  bool isRectangle(const Shape&);
  bool isSquare(const Shape&);

  bool compareVectorsLength(const Vector&, const Vector&);
}


#endif // !SHAPE_HPP
