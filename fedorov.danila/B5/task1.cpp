#include "tasks.hpp"

#include <iostream>
#include <iterator>
#include <unordered_set>
#include <stdexcept>

void lab::task1()
{
  typedef std::istream_iterator< std::string > inputIterator;
  std::unordered_set< std::string > words = std::unordered_set< std::string >(inputIterator(std::cin), inputIterator());

  if (!std::cin.eof())
  {
    throw std::runtime_error("Stream reading failed");
  }

  std::copy(words.begin(), words.end(), std::ostream_iterator< std::string >(std::cout, "\n"));
}
