#ifndef ACCESS_TYPES_HPP
#define ACCESS_TYPES_HPP

#include <cstddef>

namespace detail
{
  template< class Container >
  class AccessByBrackets
  {
  public:
    typedef size_t index;
    typedef typename Container::reference element;

    static index begin(Container&);
    static index end(Container& container);
    static element getElement(Container& container, const index i);
  };

  template< class Container >
  class AccessByAt
  {
  public:
    typedef size_t index;
    typedef typename Container::reference element;

    static index begin(Container&);
    static index end(Container& container);
    static element getElement(Container& container, const index i);
  };

  template< class Container >
  class AccessByIterator
  {
  public:
    typedef typename Container::iterator index;
    typedef typename Container::reference element;

    static index begin(Container& container);
    static index end(Container& container);
    static element getElement(Container&, const index i);
  };
}


template < class Container >
typename detail::AccessByBrackets< Container >::index detail::AccessByBrackets< Container >::begin(Container&)
{
  return 0;
}

template < class Container >
typename detail::AccessByBrackets< Container >::index detail::AccessByBrackets< Container >::end(Container& container)
{
  return container.size();
}

template < class Container >
typename detail::AccessByBrackets< Container >::element detail::AccessByBrackets< Container >::getElement(Container& container, const index i)
{
  return container[i];
}

template < class Container >
typename detail::AccessByAt< Container >::index detail::AccessByAt< Container >::begin(Container&)
{
  return 0;
}

template < class Container >
typename detail::AccessByAt< Container >::index detail::AccessByAt< Container >::end(Container& container)
{
  return container.size();
}

template < class Container >
typename detail::AccessByAt< Container >::element detail::AccessByAt< Container >::getElement(Container& container, const index i)
{
  return container.at(i);
}

template < class Container >
typename detail::AccessByIterator< Container >::index detail::AccessByIterator< Container >::begin(Container& container)
{
  return container.begin();
}

template < class Container >
typename detail::AccessByIterator< Container >::index detail::AccessByIterator< Container >::end(Container& container)
{
  return container.end();
}

template < class Container >
typename detail::AccessByIterator< Container >::element detail::AccessByIterator< Container >::getElement(Container&, const index i)
{
  return *i;
}

#endif // !ACCESS_TYPES_HPP
