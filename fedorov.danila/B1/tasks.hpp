#ifndef TASKS_HPP
#define TASKS_HPP

#include <iostream>

namespace lab
{
  void task1(const std::string& order);
  void task2(const std::string& fileName);
  void task3();
  void task4(const std::string& order, const std::string& amount);
}

#endif // !TASKS_HPP
