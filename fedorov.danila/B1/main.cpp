#include <iostream>
#include <vector>
#include <stdexcept>
#include <ctime>

#include "tasks.hpp"

int main(int argc, char* argv[])
{
  srand(time(0));

  try
  {
    if (argc < 2)
    {
      throw std::invalid_argument("not enough arguments");
    }
    switch (*argv[1]) {
    case '1':
      if (argc < 3)
      {
        throw std::invalid_argument("not enough arguments");
      }
      lab::task1(argv[2]);
      break;
    case '2':
      if (argc < 3)
      {
        throw std::invalid_argument("not enough arguments");
      }
      lab::task2(argv[2]);
      break;
    case '3':
      lab::task3();
      break;
    case '4':
      if (argc < 4)
      {
        throw std::invalid_argument("not enough arguments");
      }
      lab::task4(argv[2], argv[3]);
      break;
    default:
      throw std::invalid_argument("incorrect arguments");
      break;
    }
  }
  catch (const std::invalid_argument& exeption)
  {
    std::cerr << exeption.what();
    return 1;
  }
  catch (const std::runtime_error& exeption)
  {
    std::cerr << exeption.what();
    return 2;
  }
  catch (const std::bad_alloc& exeption)
  {
    std::cerr << exeption.what();
    return 2;
  }

  return 0;
}
