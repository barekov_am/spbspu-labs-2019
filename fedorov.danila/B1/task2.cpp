#include "tasks.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <stdexcept>

#include "common-functions.hpp"

void lab::task2(const std::string& fileName)
{
  if (fileName.size() == 0)
  {
    throw std::invalid_argument ("empty file name");
  }
  std::ifstream stream(fileName);

  if (!stream)
  {
    throw std::runtime_error("file not opened");
  }

  size_t dataSize = 0;
  size_t memorySize = 10;
  auto data = std::unique_ptr< char[], decltype(&free) >(
      static_cast< char* >(malloc(sizeof(char) * memorySize)), free);
  
  if (!data)
  {
    throw std::bad_alloc();
  }

  while (!stream.eof())
  {
    stream.read(data.get() + dataSize, memorySize - dataSize);
    
    dataSize += stream.gcount();

    if (stream.fail() && !stream.eof())
    {
      throw std::runtime_error("file reading is failed");
    }

    if (dataSize == memorySize)
    {
      memorySize *= 1.6;

      
      auto temp = std::unique_ptr< char[], decltype(&free) >(
          static_cast< char* >(realloc(data.get(), sizeof(char) * memorySize)), free);

      if (!temp)
      {
        throw std::bad_alloc();
      }
      data.release();
      data.swap(temp);
    }
  }
  
  stream.close();
  
  if (dataSize == 0)
  {
    return;
  }

  std::vector< char > vector(data.get(), data.get() + dataSize);
  detail::printContainer(vector, "");
}
