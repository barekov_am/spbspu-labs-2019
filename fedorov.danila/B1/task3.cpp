#include "tasks.hpp"

#include <iostream>
#include <vector>
#include <stdexcept>

#include "common-functions.hpp"

void lab::task3()
{
  std::vector< int > vector;
  int temp = -1;
  while (std::cin >> temp && temp != 0)
  {
    vector.push_back(static_cast< int >(temp));
  }
  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::runtime_error("data input error");
  } 
  if (vector.empty())
  {
    return;
  }
  if (temp != 0)
  {
    throw std::invalid_argument("incorrect end of data input");
  }
  
  if (vector.back() == 1)
  {
    std::vector< int >::iterator i = vector.begin();
    while (i != vector.end())
    {
      if (*i % 2 == 0)
      {
        i = vector.erase(i);
      }
      else
      {
        ++i;
      }
    }
  }
  else if (vector.back() == 2)
  {
    for (std::vector< int >::iterator i = vector.begin(); i != vector.end(); ++i)
    {
      if (*i % 3 == 0)
      {
        i = vector.insert(i + 1, 3, 1) + 2;
      }
    }
  }

  detail::printContainer(vector);
}
