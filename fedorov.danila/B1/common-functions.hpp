#ifndef COMMON_FUNCTIONS_HPP
#define COMMON_FUNCTIONS_HPP

#include <functional>
#include <iostream>
#include <cstddef>

namespace detail
{
  template < template < class Container > class AccessType, class Container, class Comparator >
  void sortContainer(Container& container, const Comparator& comp);

  template < class Container >
  void printContainer(const Container& container, const char* delimiter = " ");

  template < typename T >
  std::function< bool (T, T) > getComparator(const std::string& sortingOreder);
}

template < template < class > class AccessType, class Container, class Comparator>
void detail::sortContainer(Container& container, const Comparator& comparator)
{
  typedef AccessType< Container > thisAccess;

  for (typename thisAccess::index i = thisAccess::begin(container); i != thisAccess::end(container); ++i)
  {
    for (typename thisAccess::index j = i; j != thisAccess::end(container); ++j)
    {
      if (comparator(thisAccess::getElement(container, i), thisAccess::getElement(container, j)))
      {
        std::swap(thisAccess::getElement(container, i), thisAccess::getElement(container, j));
      }
    }
  }
}

template < class Container >
void detail::printContainer(const Container& container, const char* delimiter)
{
  for (auto i : container)
  {
      std::cout << i << delimiter;
  }
}

template < typename T >
std::function< bool (T, T) > detail::getComparator(const std::string& sortingOreder)
{
  if (sortingOreder == "ascending")
  {
    return std::greater<>();
  }
  else if (sortingOreder == "descending")
  {
    return std::less<>();
  }
  else
  {
    throw (std::invalid_argument("incorrect sorting order"));
  }
}

#endif // !COMMON_FUNCTIONS_HPP
