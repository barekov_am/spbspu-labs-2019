#include "tasks.hpp"

#include <iostream>
#include <vector>
#include <forward_list>
#include <stdexcept>

#include "common-functions.hpp"
#include "access-types.hpp"

void lab::task1(const std::string& sortingOrder)
{
  std::vector< int > vector;

  auto comparator = detail::getComparator< int >(sortingOrder);
  int temp = 0;

  while (std::cin >> temp)
  {
    vector.push_back(temp);
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::runtime_error("data input error");
  }

  if (vector.empty())
  {
    return;
  }
  
  std::vector< int > vector2(vector);
  std::forward_list< int > list(vector.begin(), vector.end());

  detail::sortContainer< detail::AccessByBrackets >(vector, comparator);
  detail::printContainer(vector);
  std::cout << "\n";

  detail::sortContainer< detail::AccessByAt >(vector2, comparator);
  detail::printContainer(vector2);
  std::cout << "\n";

  detail::sortContainer< detail::AccessByIterator >(list, comparator);
  detail::printContainer(list);
  std::cout << "\n";
}
