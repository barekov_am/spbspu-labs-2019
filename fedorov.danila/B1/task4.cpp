#include "tasks.hpp"

#include <iostream>
#include <sstream>
#include <vector>
#include <stdexcept>

#include "common-functions.hpp"
#include "access-types.hpp"

void fillRandom(double* array, const size_t size);

void lab::task4(const std::string& sortingOrder, const std::string& numbersAmount)
{
  std::stringstream ss(numbersAmount);
  size_t amount = 0;
  ss >> amount;
  if (ss.fail() || amount < 1)
  {
    throw std::invalid_argument("incorrect amount");
  }

  std::vector< double > vector(amount);
  fillRandom(vector.data(), amount);

  std::cout << std::fixed;
  std::cout.precision(1);
  detail::printContainer(vector);
  std::cout << "\n";

  detail::sortContainer< detail::AccessByIterator >(vector, detail::getComparator< double >(sortingOrder));
  detail::printContainer(vector);
  std::cout << "\n";
}

void fillRandom(double* array, const size_t size)
{
  for (size_t i = 0; i < size; ++i)
  {
    array[i] = (static_cast< double >(rand()) / RAND_MAX) * 2 - 1;
  }
}
