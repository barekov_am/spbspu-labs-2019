#include <iostream>
#include <stdexcept>
#include <memory>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

void workWithFigure(fedorov::Shape* figure);

int main()
{
  try
  {
    fedorov::Rectangle rectangle({ 1.5, 2.5 }, 5.5, 4.5);
    workWithFigure(&rectangle);

    fedorov::Circle circle({ 3.4, 5.3 }, 2.0);
    workWithFigure(&circle);

    fedorov::CompositeShape compositeShape(std::make_shared<fedorov::Rectangle>(rectangle),
      std::make_shared<fedorov::Circle>(circle));
    workWithFigure(&compositeShape);

    std::cout << "Created 1 more rectangle" << std::endl;
    fedorov::Rectangle anotherRectangle({ 0, 0 }, 50, 50);
    anotherRectangle.printInfo();

    std::cout << "It is added to Composite shape" << std::endl;
    compositeShape.add(std::make_shared<fedorov::Rectangle>(anotherRectangle));
    compositeShape.printInfo();

    std::cout << "Removed first rectangle from Composite shape" << std::endl;
    compositeShape.remove(1);
    compositeShape.printInfo();
  }
  catch (std::invalid_argument error)
  {
    std::cout << error.what() << std::endl;
    return 1;
  }
  catch (std::out_of_range error)
  {
    std::cout << error.what() << std::endl;
    return 1;
  }
  catch (std::logic_error error)
  {
    std::cout << error.what() << std::endl;
    return 2;
  }

  return 0;
}

void workWithFigure(fedorov::Shape* figure)
{
  if (figure == nullptr)
  {
    throw std::invalid_argument("Empty pointer");
  }

  figure->printInfo();

  figure->move(2.1, -1.2);
  std::cout << "\tmoved figure for  +2.1, -1.2" << std::endl;
  figure->printInfo();

  figure->scale(2.5);
  std::cout << "\tscaled figure with coefficient 2.5" << std::endl;
  figure->printInfo();

  figure->move(fedorov::point_t{ 1.0, 2.0 });
  std::cout << "\tmoved figure to   (1;2)" << std::endl;
  figure->printInfo();

  figure->scale(0.5);
  std::cout << "\tscaled figure with coefficient 0.5" << std::endl;
  figure->printInfo();

  figure->move(fedorov::vector_t{ 3.1, -2.5 });
  std::cout << "\tmoved figure for  +3.1, -2.5" << std::endl;
  figure->printInfo();
}
