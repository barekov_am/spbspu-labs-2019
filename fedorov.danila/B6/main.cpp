#include <iostream>
#include <algorithm>
#include <iterator>

#include "numbers-statistic.hpp"

int main()
{
  typedef std::istream_iterator< int > inputIterator;
  lab::NumbersStatistic statistic =
      std::for_each(inputIterator(std::cin), inputIterator(), lab::NumbersStatistic());

  if (!std::cin.eof())
  {
    std::cerr << "Stream reading failed\n";
    return 2;
  }

  std::cout << statistic;
  
  return 0;
}
