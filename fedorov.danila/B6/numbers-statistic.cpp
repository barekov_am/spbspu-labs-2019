#include "numbers-statistic.hpp"

#include <algorithm>

lab::NumbersStatistic::NumbersStatistic():
    oddSum_(0),
    evenSum_(0),
    posAmount_(0),
    negAmount_(0),
    amount_(0)
{}

void lab::NumbersStatistic::operator()(int number)
{
  if (amount_ == 0)
  {
    min_ = number;
    max_ = number;
    first_ = number;
  }
  else
  {
    min_ = std::min(min_, number);
    max_ = std::max(max_, number);
  }

  if (number < 0)
  {
    ++negAmount_;
  }
  else if (number > 0)
  {
    ++posAmount_;
  }
  ++amount_;

  (number % 2 == 0 ? evenSum_ : oddSum_) += number;

  lastAndFirstEqual_ = (number == first_);
}

std::ostream& lab::operator<<(std::ostream& stream, const NumbersStatistic& statistic)
{
  if (statistic.amount_ == 0)
  {
    stream << "No Data\n";
    return stream;
  }

  stream << "Max: " << statistic.max_ << "\n"
         << "Min: " << statistic.min_ << "\n"
         << "Mean: " << static_cast<double>(statistic.oddSum_ + statistic.evenSum_) / (statistic.amount_) << "\n"
         << "Positive: " << statistic.posAmount_ << "\n"
         << "Negative: " << statistic.negAmount_ << "\n"
         << "Odd Sum: " << statistic.oddSum_ << "\n"
         << "Even Sum: " << statistic.evenSum_ << "\n"
         << "First/Last Equal: " << (statistic.lastAndFirstEqual_ ? "yes" : "no") << "\n";

  return stream;
}
