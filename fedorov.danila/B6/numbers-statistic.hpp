#ifndef NUMBERS_STATISTIC_HPP
#define NUMBERS_STATISTIC_HPP

#include <iostream>

namespace lab
{
  class NumbersStatistic
  {
  public:
    NumbersStatistic();

    void operator()(int number);

  private:
    long long oddSum_, evenSum_;
    int max_, min_, first_;
    size_t posAmount_, negAmount_, amount_;
    bool lastAndFirstEqual_;

    friend std::ostream& operator<<(std::ostream& stream, const NumbersStatistic& statistic);

  };

  std::ostream& operator<<(std::ostream& stream, const NumbersStatistic& statistic);
}

#endif // !NUMBERS_STATISTIC_HPP
