#ifndef TASKS_HPP
#define TASKS_HPP

namespace lab
{
  void task1();
  void task2();
}
#endif // !TASKS_HPP
