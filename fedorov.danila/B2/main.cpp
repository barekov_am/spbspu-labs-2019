#include <iostream>
#include <stdexcept>

#include "tasks.hpp"

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      throw std::invalid_argument("Incorrect amount of arguments");
    }

    if (static_cast<std::string>(argv[1]) == "1")
    {
      lab::task1();
    }
    else if (static_cast<std::string>(argv[1]) == "2")
    {
      lab::task2();
    }
    else
    {
      throw std::invalid_argument("Incorrect parameter\n");
    }
  }
  catch (std::invalid_argument e)
  {
    std::cerr << e.what();
    return 1;
  }
  catch (std::overflow_error e)
  {
    std::cerr << e.what();
    return 1;
  }
  catch (std::runtime_error e)
  {
    std::cerr << e.what();
    return 2;
  }

  return 0;
}
