#include "tasks.hpp"

#include <iostream>
#include <list>
#include <stdexcept>

void lab::task2()
{
  std::list< int > list;
  int temp = 0;

  while (std::cin >> temp)
  {
    if ((temp < 1) || (temp > 20))
    {
      throw std::invalid_argument("Incorrect number");
    }

    list.push_back(temp);

    if (list.size() > 20)
    {
      throw std::overflow_error("Too much numbers");
    }
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::runtime_error("Input error");
  }

  if (list.empty())
  {
    return;
  }

  for (std::list< int >::iterator i = list.begin(), j = list.end();
      (i != j); i++)
  {
    std::cout << *i << " ";
    
    if (i == --j)
    {
      break;
    }

    std::cout << *j << " ";
  }
  std::cout << "\n";
}
