#include "command-parsing.hpp"

#include <iostream>
#include <unordered_map>
#include <functional>
#include <string>
#include <cctype>
#include <stdexcept>

#include "manipulators.hpp"
#include "QueueWithPriority.hpp"


lab::CommandParsing::FunctionPointerType lab::CommandParsing::processQueueCommand(std::istream& stream)
{
  const MapType commands{ { "add", std::bind(parseAddCommand, std::ref(stream)) },
                          { "get", std::bind(parseGetCommand, std::ref(stream)) },
                          { "accelerate", std::bind(parseAccelerateCommand, std::ref(stream)) } };

  std::string word;
  stream >> std::noskipws >> std::ws >> word;


  if (stream.fail() && !stream.eof())
  {
    throw std::runtime_error("Stream reading failed");
  }

  auto func = commands.find(word);

  if (func == commands.end())
  {
    std::string temp;
    std::getline(stream, temp);
    return failedCommand;
  }
  else
  {
    return func->second();
  }
}

lab::CommandParsing::FunctionPointerType lab::CommandParsing::parseAddCommand(std::istream& stream)
{
  std::string priorityString;
  stream >> myws >> priorityString;

  if (stream.fail())
  {
    stream.clear();
    std::string temp;
    std::getline(stream, temp);
    return failedCommand;
  }

  QueueType::ElementPriority priority;

  try
  {
    priority = stringToPriority(priorityString);
  }
  catch(std::invalid_argument e)
  {
    std::string temp;
    std::getline(stream, temp);
    return failedCommand;
  }

  std::string word;
  stream >> myws >> word;
  if (stream.fail())
  {
    stream.clear();
    std::string temp;
    std::getline(stream, temp);
    return failedCommand;
  }

  std::string elementString;
  std::getline(stream, elementString);

  if (stream.fail() && !stream.eof())
  {
    throw std::runtime_error("Stream reading failed");
  }

  elementString = word + elementString;

  return std::bind(&QueueType::add, std::placeholders::_1, elementString, priority);
}

lab::CommandParsing::FunctionPointerType lab::CommandParsing::parseGetCommand(std::istream& stream)
{
  std::string temp;
  stream >> myws >> temp;

  if (stream.fail())
  {
    stream.clear();
    stream.get();

    return std::bind(&QueueType::processElement< void(*)(std::string) >,
        std::placeholders::_1,
        [](std::string string) { std::cout << string << "\n"; });
  }
  else
  {
    std::getline(stream, temp);
    return failedCommand;
  }
}

lab::CommandParsing::FunctionPointerType lab::CommandParsing::parseAccelerateCommand(std::istream& stream)
{
  std::string temp;
  stream >> myws >> temp;

  if (stream.fail())
  {
    stream.clear();
    return std::bind(&QueueType::accelerate, std::placeholders::_1);
  }
  else
  {
    std::string temp;
    std::getline(stream, temp);
    return failedCommand;
  }
}

lab::CommandParsing::QueueType::ElementPriority lab::CommandParsing::stringToPriority(const std::string& word)
{
  lab::CommandParsing::QueueType::ElementPriority priority;

  if (word == "high")
  {
    priority = QueueType::ElementPriority::HIGH;
  }
  else if (word == "normal")
  {
    priority = QueueType::ElementPriority::NORMAL;
  }
  else if (word == "low")
  {
    priority = QueueType::ElementPriority::LOW;
  }
  else
  {
    throw std::invalid_argument("incorrect priority");
  }

  return priority;
}

void lab::CommandParsing::failedCommand(QueueType&)
{
  std::cout << "<INVALID COMMAND>\n";
}
