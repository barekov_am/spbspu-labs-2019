#include "tasks.hpp"

#include <iostream>
#include <string>
#include <stdexcept>

#include "QueueWithPriority.hpp"
#include "command-parsing.hpp"

void lab::task1()
{
  lab::QueueWithPriority< std::string > queue;

  while (std::cin && (std::cin >> std::ws).peek() != EOF)
  {
    auto method = lab::CommandParsing::processQueueCommand(std::cin);

    if (std::cin.fail() && !std::cin.eof())
    {
      throw std::runtime_error("Reading was failed");
    }

    try
    {
      method(queue);
    }
    catch (std::out_of_range&)
    {
      std::cout << "<EMPTY>\n";
    }
  }
}
