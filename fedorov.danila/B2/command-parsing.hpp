#ifndef COMMAND_PARSING_HPP
#define COMMAND_PARSING_HPP

#include <iostream>
#include <unordered_map>
#include <functional>

#include "QueueWithPriority.hpp"

namespace lab
{
  class CommandParsing
  {
  public:
    typedef lab::QueueWithPriority< std::string > QueueType;
    typedef std::function< void(QueueType&) > FunctionPointerType;

    static FunctionPointerType processQueueCommand(std::istream& stream);

  private:
    typedef std::unordered_map< std::string, std::function< FunctionPointerType() > > MapType;

    static FunctionPointerType parseAddCommand(std::istream& stream);
    static FunctionPointerType parseGetCommand(std::istream& stream);
    static FunctionPointerType parseAccelerateCommand(std::istream& stream);
    
    static QueueType::ElementPriority stringToPriority(const std::string&);

    static void failedCommand(QueueType&);
  };
}


#endif // !COMMAND_PARSING_HPP
