#ifndef QUEUEWITHPRIORITY_HPP
#define QUEUEWITHPRIORITY_HPP

#include <iostream>
#include <list>
#include <memory>
#include <stdexcept>

namespace lab
{
  template < typename T >
  class QueueWithPriority
  {
  public:
    typedef enum
    {
      HIGH,
      NORMAL,
      LOW
    } ElementPriority;

    void add(const T& element, const ElementPriority& priority);

    template < typename Task >
    void processElement(Task task);

    void accelerate();
    bool empty() const;

  private:
    std::list< T > queueHigh_;
    std::list< T > queueNormal_;
    std::list< T > queueLow_;
  };  
}

template < typename T >
void lab::QueueWithPriority< T >::add(const T& element, const ElementPriority& priority)
{
  switch (priority)
  {
  case ElementPriority::HIGH:
    queueHigh_.push_back(element);
    break;
  case ElementPriority::NORMAL:
    queueNormal_.push_back(element);
    break;
  case ElementPriority::LOW:
    queueLow_.push_back(element);
    break;
  default:
    throw std::invalid_argument("incorrect priority");
  }
}

template < typename T >
template < typename Task >
void lab::QueueWithPriority< T >::processElement(Task task)
{
  if (!queueHigh_.empty())
  {
    task(queueHigh_.front());
    queueHigh_.pop_front();
  }
  else if (!queueNormal_.empty())
  {
    task(queueNormal_.front());
    queueNormal_.pop_front();
  }
  else if (!queueLow_.empty())
  {
    task(queueLow_.front());
    queueLow_.pop_front();
  }
  else
  {
    throw std::out_of_range("Try to process element in empty queue");
  }
}

template < typename T >
void lab::QueueWithPriority< T >::accelerate()
{
  queueHigh_.splice(queueHigh_.end(), queueLow_);
}

template < typename T >
bool lab::QueueWithPriority< T >::empty() const
{
  return queueLow_.empty() && queueNormal_.empty() && queueHigh_.empty();
}

#endif // !QUEUEWITHPRIORITY_HPP
