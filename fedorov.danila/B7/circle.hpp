#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include <iostream>

#include "shape.hpp"

namespace lab7
{
  class Circle :
    public Shape
  {
  public:
    static const std::string NAME;

    Circle(const lab::Point& center);

    void draw(std::ostream& stream) const override;
  };
}

#endif // !CIRCLE_HPP
