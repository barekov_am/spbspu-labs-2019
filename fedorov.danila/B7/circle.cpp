#include "circle.hpp"

const std::string lab7::Circle::NAME = "CIRCLE";

lab7::Circle::Circle(const lab::Point& center):
    Shape(center)
{}

void lab7::Circle::draw(std::ostream& stream) const
{
  stream << Circle::NAME << " " << center_ << "\n";
}
