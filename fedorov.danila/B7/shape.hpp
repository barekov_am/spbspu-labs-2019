#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>
#include <memory>

#include "point.hpp"

namespace lab7
{
  class Shape
  {
  public:
    typedef std::shared_ptr< const Shape > ConstShapePointer;
    typedef std::shared_ptr< Shape > ShapePointer;

    Shape(const lab::Point& center);
    virtual ~Shape() = default;

    bool isMoreLeft(const ConstShapePointer& shape) const;
    bool isUpper(const ConstShapePointer& shape) const;

    virtual void draw(std::ostream& stream) const = 0;

  protected:
    lab::Point center_;

  };

  std::istream& operator>>(std::istream& stream, Shape::ShapePointer& shape);
  std::ostream& operator<<(std::ostream& stream, const Shape::ConstShapePointer& shape);
}

#endif // !SHAPE_HPP
