#include "tasks.hpp"

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <functional>
#include <memory>
#include <stdexcept>

#include "shape.hpp"

template <class Container, typename Sorter>
void sortAndPrintShapes(Container& shapes, std::ostream& stream, const Sorter& sorter);

void lab7::task2()
{
  typedef std::istream_iterator< Shape::ShapePointer > inputIterator;
  std::vector< Shape::ShapePointer > shapes =
      std::vector< Shape::ShapePointer >(inputIterator(std::cin), inputIterator());
  
  if (!std::cin.eof())
  {
    throw std::runtime_error("Stream reading failed");
  }

  if (shapes.empty())
  {
    return;
  }

  std::cout << "Original:\n";
  std::copy(shapes.begin(), shapes.end(),
      std::ostream_iterator< Shape::ConstShapePointer >(std::cout));

  using namespace std::placeholders;
  std::cout << "Left-Right:\n";
  sortAndPrintShapes(shapes, std::cout, std::bind(&Shape::isMoreLeft, _1, _2));

  std::cout << "Right-Left:\n";
  sortAndPrintShapes(shapes, std::cout, std::bind(&Shape::isMoreLeft, _2, _1));

  std::cout << "Top-Bottom:\n";
  sortAndPrintShapes(shapes, std::cout, std::bind(&Shape::isUpper, _1, _2));

  std::cout << "Bottom-Top:\n";
  sortAndPrintShapes(shapes, std::cout, std::bind(&Shape::isUpper, _2, _1));
}

template <class Container, typename Sorter>
void sortAndPrintShapes(Container& shapes, std::ostream& stream, const Sorter& sorter)
{
  std::sort(shapes.begin(), shapes.end(), sorter);
  std::copy(shapes.begin(), shapes.end(),
      std::ostream_iterator< lab7::Shape::ConstShapePointer >(stream));
}
