#include "shape.hpp"

#include <iostream>
#include <memory>
#include <unordered_map>
#include <functional>

#include "manipulators.hpp"
#include "stream-state-saver.hpp"
#include "shape.hpp"
#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"

template <typename T>
static lab7::Shape::ShapePointer getShape(const lab::Point& center);

lab7::Shape::Shape(const lab::Point& center):
    center_(center)
{}

bool lab7::Shape::isMoreLeft(const ConstShapePointer& shape) const
{
  return center_.x < shape->center_.x;
}

bool lab7::Shape::isUpper(const ConstShapePointer& shape) const
{
  return center_.y > shape->center_.y;
}

std::istream& lab7::operator>>(std::istream& stream, Shape::ShapePointer& shape)
{
  if (stream.fail())
  {
    return stream;
  }

  const std::unordered_map< std::string, decltype(&getShape< lab7::Circle >) > getters{
      { Circle::NAME,   getShape< lab7::Circle > },
      { Triangle::NAME, getShape< lab7::Triangle > },
      { Square::NAME,   getShape< lab7::Square > } };

  std::string shapeType;
  lab::Point center;

  lab::StreamStateSaver safe(stream);
  stream >> std::noskipws >> std::ws >> lab::getWord(shapeType) >> lab::myws >> center;
  if (stream.fail())
  {
    return stream;
  }

  stream >> lab::skipDelimiter('\n');
  if (stream.fail())
  {
    if (!stream.eof())
    {
      return stream;
    }

    stream.clear();
  }

  auto getter = getters.find(shapeType);
  if (getter == getters.end())
  {
    stream.setstate(std::ios_base::failbit);
    return stream;
  }

  shape = getter->second(center);
  return stream;
}

std::ostream& lab7::operator<<(std::ostream& stream, const Shape::ConstShapePointer& shape)
{
  shape->draw(stream);
  return stream;
}

template <typename T>
lab7::Shape::ShapePointer getShape(const lab::Point& center)
{
  return lab7::Shape::ShapePointer(new T(center));
}
