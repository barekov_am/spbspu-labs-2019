#include "tasks.hpp"

#define _USE_MATH_DEFINES

#include <iostream>
#include <deque>
#include <algorithm>
#include <iterator>
#include <functional>
#include <cmath>
#include <stdexcept>

void lab7::task1()
{
  typedef std::istream_iterator< double > inputStream;
  std::transform(inputStream(std::cin), inputStream(), std::ostream_iterator< double >(std::cout, " "),
      std::bind(std::multiplies< double >(), std::placeholders::_1, M_PI));

  if (!std::cin.eof())
  {
    throw std::runtime_error("Stream reading error");
  }
}
