#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "shape.hpp"

namespace lab7
{
  class Square :
    public Shape
  {
  public:
    static const std::string NAME;

    Square(const lab::Point& center);

    void draw(std::ostream& stream) const override;
  };
}

#endif // !SQUARE_HPP
