#include "square.hpp"

const std::string lab7::Square::NAME = "SQUARE";

lab7::Square::Square(const lab::Point& center):
    Shape(center)
{}

void lab7::Square::draw(std::ostream& stream) const
{
  stream << Square::NAME << " " << center_ << "\n";
}
