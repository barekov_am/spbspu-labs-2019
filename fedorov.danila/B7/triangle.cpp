#include "triangle.hpp"

const std::string lab7::Triangle::NAME = "TRIANGLE";

lab7::Triangle::Triangle(const lab::Point& center):
    Shape(center)
{}

void lab7::Triangle::draw(std::ostream& stream) const
{
  stream << Triangle::NAME << " " << center_ << "\n";
}
