#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace lab7
{
  class Triangle :
    public Shape
  {
  public:
    static const std::string NAME;

    Triangle(const lab::Point& center);

    void draw(std::ostream& stream) const override;
  };
}

#endif // !TRIANGLE_HPP
