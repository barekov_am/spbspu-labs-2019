#include "composite-shape.hpp"

#include <stdexcept>
#include <iostream>
#include <cmath>
#include <memory>

fedorov::CompositeShape::CompositeShape():
    amount_(0),
    figureList_(nullptr)
{}

fedorov::CompositeShape::CompositeShape(const fedorov::CompositeShape& figure):
    Shape(),
    amount_(figure.amount_),
    figureList_(figure.getArrayCopy())
{}

fedorov::CompositeShape::CompositeShape(fedorov::CompositeShape&& figure):
    Shape(),
    amount_(figure.amount_),
    figureList_(std::move(figure.figureList_))
{
  figure.clear();
}

fedorov::CompositeShape::CompositeShape(figurePointerType figure1, figurePointerType figure2):
    amount_(2),
    figureList_(new figurePointerType[2])
{
  if ((figure1 == nullptr) || (figure2 == nullptr))
  {
    throw std::invalid_argument("Empty pointer");
  }

  figureList_[0] = figure1;
  figureList_[1] = figure2;
}

fedorov::CompositeShape& fedorov::CompositeShape::operator=(const CompositeShape& figure)
{
  if (this != &figure)
  {
    amount_ = figure.amount_;
    figureList_ = figure.getArrayCopy();
  }

  return *this;
}

fedorov::CompositeShape& fedorov::CompositeShape::operator=(CompositeShape&& figure)
{
  if (this != &figure)
  {
    amount_ = figure.amount_;
    figureList_ = std::move(figure.figureList_);
    figure.clear();
  }

  return *this;
}

fedorov::Shape& fedorov::CompositeShape::operator[](unsigned int index) const
{
  if (index >= amount_)
  {
    throw std::out_of_range("Try to get element out of range");
  }
  return *figureList_[index];
}

fedorov::rectangle_t fedorov::CompositeShape::getFrameRect() const
{
  if (amount_ == 0)
  {
    throw std::logic_error("Empty composite shape doesn't have a frame");
  }
  rectangle_t frame = figureList_[0]->getFrameRect();
  
  for (unsigned int i = 1; i < amount_; i++)
  {
    frame += figureList_[i]->getFrameRect();
  }

  return frame;
}

fedorov::point_t fedorov::CompositeShape::getPosition() const
{
  return getFrameRect().pos;
}

double fedorov::CompositeShape::getArea() const
{
  double area = 0;

  for (unsigned int i = 0; i < amount_; i++)
  {
    area += figureList_[i]->getArea();
  }

  return area;
}

unsigned int fedorov::CompositeShape::getAmount() const
{
  return amount_;
}

void fedorov::CompositeShape::printInfo() const
{
  std::cout << "Composite shape:" << std::endl
      << "\t center: " << getPosition() << std::endl
      << "\t area: " << getArea() << std::endl
      << "\t frame:" << std::endl
      << getFrameRect() << std::endl;
  for (unsigned int i = 0; i < amount_; i++)
  {
    std::cout << "Figure " << i << std::endl;
    figureList_[i]->printInfo();
  }
}

void fedorov::CompositeShape::scale(double scale)
{
  if (scale <= 0)
  {
    throw std::invalid_argument("Incorrect scale");
  }

  position_ = getPosition();
  for (unsigned int i = 0; i < amount_; i++)
  {
    // (scale - 1) used because we need to substruct initial shift from figure center
    vector_t shift = (figureList_[i]->getPosition() - position_) * (scale - 1);
    
    figureList_[i]->move(shift);
    figureList_[i]->scale(scale);
  }
}

void fedorov::CompositeShape::move(const point_t& point)
{
  vector_t shift = (point - getPosition());
  move(shift);
}

void fedorov::CompositeShape::move(const vector_t &shiftVector)
{
  for (unsigned int i = 0; i < amount_; i++)
  {
    figureList_[i]->move(shiftVector);
  }
}

void fedorov::CompositeShape::move(const double dx, const double dy)
{
  for (unsigned int i = 0; i < amount_; i++)
  {
    figureList_[i]->move(dx, dy);
  }
}

void fedorov::CompositeShape::rotate(const double angle)
{
  position_ = getPosition();

  for (unsigned int i = 0; i < amount_; i++)
  {
    figureList_[i]->rotate(angle);

    vector_t shift = figureList_[i]->getPosition() - position_;
    rotateVector(shift, angle);
    figureList_[i]->move(position_ + shift);
  }
}

void fedorov::CompositeShape::add(figurePointerType figure)
{
  if (figure == nullptr)
  {
    throw std::invalid_argument("Empty pointer");
  }

  
  figureArrayType newFigureList(new figurePointerType[amount_ + 1]);
  for (unsigned int i = 0; i < amount_; i++)
  {
    newFigureList[i] = figureList_[i];
  }
  newFigureList[amount_] = figure;
  amount_++;

  figureList_.swap(newFigureList);
}


void fedorov::CompositeShape::remove(const unsigned int figureNumber)
{
  if (figureNumber >= amount_)
  {
    throw std::out_of_range("Try to remove element out of range");
  }
  amount_--;

  if (amount_ == 0)
  {
    figureList_.reset();
  }
  else
  {
    figureArrayType newFigureList(new figurePointerType[amount_]);

    for (unsigned int i = 0; i < figureNumber; i++)
    {
      newFigureList[i] = figureList_[i];
    }
    for (unsigned int i = figureNumber; i < amount_; i++)
    {
      newFigureList[i] = figureList_[i + 1];
    }

    figureList_.swap(newFigureList);
  }
}

void fedorov::CompositeShape::clear()
{
  amount_ = 0;
  figureList_.reset();
}

fedorov::CompositeShape::figureArrayType fedorov::CompositeShape::getArrayCopy() const
{
  figureArrayType arrayCopy(new figurePointerType[amount_]);
  for (unsigned int i = 0; i < amount_; i++)
  {
    arrayCopy[i] = figureList_[i];
  }

  return arrayCopy;
}
