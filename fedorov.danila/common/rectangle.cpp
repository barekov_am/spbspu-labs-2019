#include "rectangle.hpp"

#include <iostream>
#include <stdexcept>

#include <cmath>

fedorov::Rectangle::Rectangle(const point_t &position, const double width, const double height):
    Shape(position),
    width_(width),
    height_(height),
    angle_(0)
{
  if ((width_ <= 0) || (height_ <= 0))
  {
    throw std::invalid_argument("Incorrect width or height");
  }
}

fedorov::rectangle_t fedorov::Rectangle::getFrameRect() const
{ 
  double angle = getAngleInRadians(angle_);
  return { position_,
      std::abs(width_ * cos(angle)) + abs(height_ * std::sin(angle)),
      std::abs(height_* cos(angle)) + abs(width_ * std::sin(angle)) };
}

double fedorov::Rectangle::getArea() const
{
  return width_ * height_;
}

double fedorov::Rectangle::getWidth() const
{
  return width_;
}

double fedorov::Rectangle::getHeight() const
{
  return height_;
}

void fedorov::Rectangle::printInfo() const
{
  std::cout << " Rectangle" << std::endl;
  Shape::printInfo();
  std::cout << "\t width: " << width_ << " \t height: " << height_ << std::endl
      << "\t area: " << getArea() << std::endl
      << "\t frame:" << std::endl
      << getFrameRect() << std::endl
      << std::endl;
}

void fedorov::Rectangle::scale(double scale)
{
  if (scale <= 0)
  {
    throw std::invalid_argument("Incorrect scale");
  }
  width_ *= scale;
  height_ *= scale;
}


void fedorov::Rectangle::rotate(const double angle)
{
  angle_ += angle;
}
