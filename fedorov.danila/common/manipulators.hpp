#ifndef MANIPULATORS_HPP
#define MANIPULATORS_HPP

#include <iostream>
#include <cctype>
#include <functional>

namespace lab
{
  typedef std::function< bool(unsigned char) > symbolCheck;

  //Skips only tabs and spaces
  std::istream& myws(std::istream& stream);

  //Works similar to myws but reqieres spaces to be in stream
  std::istream& skipSpaceDelimiter(std::istream& stream);

  struct skipDelimiter
  {
    char delimiter_;
    skipDelimiter(char ch) : delimiter_(ch) {};
  };

  std::istream& operator>>(std::istream& stream, skipDelimiter&& manip);

  struct getSpecialWord
  {
    std::string& word_;
    symbolCheck isCorrectSymbol;
    getSpecialWord(std::string& word, symbolCheck isCorrectSymbol):
        word_(word), isCorrectSymbol(isCorrectSymbol) {};
  };

  std::istream& operator>>(std::istream& stream, getSpecialWord&& manip);

  //Supports only numbers in input
  struct getNumber
  {
    std::string& number_;
    getNumber(std::string& number) : number_(number) {};
  };

  std::istream& operator>>(std::istream& stream, getNumber&& manip);

  //For name between "..."  with special symbols '\"', '\\'
  struct getLongName
  {
    std::string& name_;
    getLongName(std::string& name) : name_(name) {};
  };

  std::istream& operator>>(std::istream& stream, getLongName&& manip);

  //Supports alphabet, numbers and '-'
  struct getName
  {
    std::string& name_;
    getName(std::string& name) : name_(name){};
  };

  std::istream& operator>>(std::istream& stream, getName&& manip);

  //Supports only alphabet
  struct getWord
  {
    std::string& word_;
    getWord(std::string& name) : word_(name) {};
  };

  std::istream& operator>>(std::istream& stream, getWord&& manip);
}
#endif // !MANIPULATORS_HPP
