#include <stdexcept>

#include "boost/test/auto_unit_test.hpp"

#include "rectangle.hpp"

#define COMPARE_TOLERANCE 0.001

BOOST_AUTO_TEST_SUITE(rectangle) // rectangle

BOOST_AUTO_TEST_SUITE(move) // rectangle/move

BOOST_AUTO_TEST_CASE(widthNotChanged)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 3.5, 4.5);
  double width = rectangle.getWidth();

  rectangle.move(1.5, 2.5);
  BOOST_CHECK_EQUAL(rectangle.getWidth(), width);

  rectangle.move(fedorov::point_t{ 4.1, 2.6 });
  BOOST_CHECK_EQUAL(rectangle.getWidth(), width);

  rectangle.move(fedorov::vector_t{ 1.2, -2.3 });
  BOOST_CHECK_EQUAL(rectangle.getWidth(), width);
}

BOOST_AUTO_TEST_CASE(heightNotChanged)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 3.5, 4.5);
  double height = rectangle.getHeight();

  rectangle.move(1.5, 2.5);
  BOOST_CHECK_EQUAL(rectangle.getHeight(), height);

  rectangle.move(fedorov::point_t{ 4.1, 2.6 });
  BOOST_CHECK_EQUAL(rectangle.getHeight(), height);

  rectangle.move(fedorov::vector_t{ 1.2, -2.3 });
  BOOST_CHECK_EQUAL(rectangle.getHeight(), height);
}

BOOST_AUTO_TEST_CASE(areaNotChanged)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 3.5, 4.5);
  double area = rectangle.getArea();

  rectangle.move(1.5, 2.5);
  BOOST_CHECK_EQUAL(rectangle.getArea(), area);

  rectangle.move(fedorov::point_t{ 4.1, 2.6 });
  BOOST_CHECK_EQUAL(rectangle.getArea(), area);

  rectangle.move(fedorov::vector_t{ 1.2, -2.3 });
  BOOST_CHECK_EQUAL(rectangle.getArea(), area);
}

BOOST_AUTO_TEST_SUITE_END() // rectangle/move

BOOST_AUTO_TEST_SUITE(scale) // rectangle/scale

BOOST_AUTO_TEST_CASE(areaScaledCorrectly)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 3.5, 4.5);
  double area = rectangle.getArea();

  double scale = 2.5;
  double areaScaled = area * scale * scale;

  rectangle.scale(scale);

  BOOST_CHECK_CLOSE(rectangle.getArea(), areaScaled, COMPARE_TOLERANCE);

  BOOST_CHECK_CLOSE(rectangle.getArea() / area, scale * scale, COMPARE_TOLERANCE);
}

BOOST_AUTO_TEST_SUITE_END() // rectangle/scale

BOOST_AUTO_TEST_SUITE(parametersCheck) // rectangle/parametersCheck

BOOST_AUTO_TEST_CASE(wrongWidth)
{
  BOOST_CHECK_THROW(fedorov::Rectangle({ 2, 4 }, -3, 5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(wrongHeight)
{
  BOOST_CHECK_THROW(fedorov::Rectangle({ 2, 4 }, 3, -5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(wrongScaleCoefficient)
{
  fedorov::Rectangle rectangle({ 2, 4 }, 3, 5);
  BOOST_CHECK_THROW(rectangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END() // rectangle/parametersCheck

BOOST_AUTO_TEST_SUITE_END() // rectangle
