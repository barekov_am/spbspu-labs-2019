#include "shape.hpp"

#include <iostream>

#define _USE_MATH_DEFINES
#include <math.h>

fedorov::Shape::Shape(const point_t& position):
    position_(position)
{}

fedorov::point_t fedorov::Shape::getPosition() const
{
  return position_;
}

void fedorov::Shape::printInfo() const
{
  std::cout << "\t center: (" << position_.x << ";" << position_.y << ")" << std::endl;
}

void fedorov::Shape::move(const point_t& point)
{
  position_ = point;
}

void fedorov::Shape::move(const vector_t& shiftVector)
{
  position_ += shiftVector;
}

void fedorov::Shape::move(const double dx, const double dy)
{
  position_ += { dx, dy };
}
