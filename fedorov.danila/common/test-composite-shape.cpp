#include <stdexcept>
#include <memory>

#include "boost/test/auto_unit_test.hpp"

#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

const double TOLERANCE = 0.001;

BOOST_AUTO_TEST_SUITE(compositeShape) // compositeShape

BOOST_AUTO_TEST_SUITE(move) // compositeShape/move

BOOST_AUTO_TEST_CASE(frameRectangleNotChanged)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 20, 20);
  fedorov::Circle circle({ 10, 10 }, 10);
  fedorov::CompositeShape compositeShape(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));

  const fedorov::rectangle_t frame = compositeShape.getFrameRect();

  compositeShape.move(10, 10);

  BOOST_CHECK_EQUAL(frame.width, compositeShape.getFrameRect().width);
  BOOST_CHECK_EQUAL(frame.width, compositeShape.getFrameRect().width);

  compositeShape.move(fedorov::point_t{ 0, 0 });

  BOOST_CHECK_EQUAL(frame.height, compositeShape.getFrameRect().height);
  BOOST_CHECK_EQUAL(frame.height, compositeShape.getFrameRect().height);

  compositeShape.move(fedorov::vector_t{ 0, 0 });

  BOOST_CHECK_EQUAL(frame.height, compositeShape.getFrameRect().height);
  BOOST_CHECK_EQUAL(frame.height, compositeShape.getFrameRect().height);
}

BOOST_AUTO_TEST_CASE(areaNotChanged)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 20, 20);
  fedorov::Circle circle({ 10, 10 }, 10);
  fedorov::CompositeShape compositeShape(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));

  const double area = compositeShape.getArea();

  compositeShape.move(10, 10);

  BOOST_CHECK_EQUAL(area, compositeShape.getArea());

  compositeShape.move(fedorov::point_t{ 0, 0 });

  BOOST_CHECK_EQUAL(area, compositeShape.getArea());

  compositeShape.move(fedorov::vector_t{ 0, 0 });

  BOOST_CHECK_EQUAL(area, compositeShape.getArea());
}

BOOST_AUTO_TEST_SUITE_END() // compositeShape/move

BOOST_AUTO_TEST_SUITE(scale) // compositeShape/scale

BOOST_AUTO_TEST_CASE(areaScaledCorrectly)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 20, 20);
  fedorov::Circle circle({ 10, 10 }, 10);
  fedorov::CompositeShape compositeShape(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));

  const double area = compositeShape.getArea();
  const double scale = 2.5;

  compositeShape.scale(scale);

  BOOST_CHECK_CLOSE(compositeShape.getArea() / area, scale * scale, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(positionNotChanged)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 20, 20);
  fedorov::Circle circle({ 10, 10 }, 10);
  fedorov::CompositeShape compositeShape(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));

  const fedorov::point_t position = compositeShape.getPosition();

  compositeShape.scale(2);

  BOOST_CHECK_CLOSE(compositeShape.getPosition().x, position.x, TOLERANCE);
  BOOST_CHECK_CLOSE(compositeShape.getPosition().y, position.y, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(frameScaledProperly)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 20, 20);
  fedorov::Circle circle({ 10, 10 }, 10);
  fedorov::CompositeShape compositeShape(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));

  const fedorov::rectangle_t frame = compositeShape.getFrameRect();
  const double scale = 0.5;

  compositeShape.scale(scale);

  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().width / frame.width, scale, TOLERANCE);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().height / frame.height, scale, TOLERANCE);
}

BOOST_AUTO_TEST_SUITE_END() // compositeShape/scale

BOOST_AUTO_TEST_SUITE(rotation) // compositeShape/rotation

BOOST_AUTO_TEST_CASE(centerNotChanged)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 20, 20);
  fedorov::Circle circle({ 10, 10 }, 10);
  fedorov::CompositeShape compositeShape(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));

  const fedorov::point_t center = compositeShape.getPosition();

  rectangle.rotate(30);

  BOOST_CHECK_CLOSE(compositeShape.getPosition().x, center.x, TOLERANCE);
  BOOST_CHECK_CLOSE(compositeShape.getPosition().y, center.y, TOLERANCE);
}

BOOST_AUTO_TEST_SUITE_END() // compositeShape/rotation

BOOST_AUTO_TEST_SUITE(addingAndRemoving) // compositeShape/addingAndRemovingFigure

BOOST_AUTO_TEST_CASE(addingWorksProperly)
{
  std::shared_ptr<fedorov::Rectangle> rectangle(new fedorov::Rectangle({ 0, 0 }, 20, 20));
  std::shared_ptr<fedorov::Circle> circle(new fedorov::Circle({ 10, 10 }, 10));
  fedorov::CompositeShape compositeShape(rectangle, circle);

  std::shared_ptr<fedorov::Rectangle> newRectangle(new fedorov::Rectangle({ 0, 0 }, 20, 20));
  compositeShape.add(newRectangle);

  const unsigned int amount = 3;

  BOOST_CHECK_EQUAL(compositeShape.getAmount(), amount);
  BOOST_CHECK_EQUAL(&compositeShape[2], newRectangle.get());
}

BOOST_AUTO_TEST_CASE(removingWorksProperly)
{
  std::shared_ptr<fedorov::Rectangle> rectangle(new fedorov::Rectangle({ 0, 0 }, 20, 20));
  std::shared_ptr<fedorov::Circle> circle(new fedorov::Circle({ 10, 10 }, 10));
  fedorov::CompositeShape compositeShape(rectangle, circle);

  compositeShape.remove(1);

  const unsigned int amount = 1;

  BOOST_CHECK_EQUAL(compositeShape.getAmount(), amount);
  BOOST_CHECK_EQUAL(&compositeShape[0], rectangle.get());
}

BOOST_AUTO_TEST_CASE(clearWorksProperly)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 20, 20);
  fedorov::Circle circle({ 10, 10 }, 10);
  fedorov::CompositeShape compositeShape(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));

  compositeShape.clear();

  const unsigned int amount = 0;

  BOOST_CHECK_EQUAL(compositeShape.getAmount(), amount);
}

BOOST_AUTO_TEST_SUITE_END() // compositeShape/addingAndRemovingFigure

BOOST_AUTO_TEST_SUITE(exceptionsCheck) // compositeShape/parametersCheck

BOOST_AUTO_TEST_CASE(emptyPointers)
{
  BOOST_CHECK_THROW(fedorov::CompositeShape compositeShape(nullptr, nullptr), std::invalid_argument);

  fedorov::Rectangle rectangle({ 0, 0 }, 20, 20);
  fedorov::Circle circle({ 10, 10 }, 10);
  fedorov::CompositeShape compositeShape(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));

  BOOST_CHECK_THROW(compositeShape.add(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(removeWrongFigure)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 20, 20);
  fedorov::Circle circle({ 10, 10 }, 10);
  fedorov::CompositeShape compositeShape(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));

  BOOST_CHECK_THROW(compositeShape.remove(3), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(wrongScaleCoefficient)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 20, 20);
  fedorov::Circle circle({ 10, 10 }, 10);
  fedorov::CompositeShape compositeShape(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));

  BOOST_CHECK_THROW(compositeShape.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(indexOutOfRange)
{
  fedorov::CompositeShape compositeShape;

  BOOST_CHECK_THROW(compositeShape[0], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END() // compositeShape/parametersCheck

BOOST_AUTO_TEST_SUITE(constructors) // compositeShape/constructors

BOOST_AUTO_TEST_CASE(copyingConstructor)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 20, 20);
  fedorov::Circle circle({ 10, 10 }, 10);
  fedorov::CompositeShape compositeShape(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));

  fedorov::CompositeShape anotherCompositeShape(compositeShape);

  BOOST_CHECK_EQUAL(anotherCompositeShape.getArea(), compositeShape.getArea());
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().pos.x, compositeShape.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().pos.y, compositeShape.getFrameRect().pos.y);
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().width, compositeShape.getFrameRect().width);
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().height, compositeShape.getFrameRect().height);
}

BOOST_AUTO_TEST_CASE(movingConstructor)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 20, 20);
  fedorov::Circle circle({ 10, 10 }, 10);
  fedorov::CompositeShape compositeShape(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));

  const double area = compositeShape.getArea();
  const fedorov::rectangle_t frame = compositeShape.getFrameRect();

  fedorov::CompositeShape anotherCompositeShape(std::move(compositeShape));

  BOOST_CHECK_EQUAL(anotherCompositeShape.getArea(), area);
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().pos.x, frame.pos.x);
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().pos.y, frame.pos.y);
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().width, frame.width);
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().height, frame.height);
}

BOOST_AUTO_TEST_SUITE_END() // compositeShape/constructors

BOOST_AUTO_TEST_SUITE(operators) // compositeShape/operators

BOOST_AUTO_TEST_CASE(copyingOperator)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 20, 20);
  fedorov::Circle circle({ 10, 10 }, 10);
  fedorov::CompositeShape compositeShape(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));

  fedorov::CompositeShape anotherCompositeShape = compositeShape;

  BOOST_CHECK_EQUAL(anotherCompositeShape.getArea(), compositeShape.getArea());
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().pos.x, compositeShape.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().pos.y, compositeShape.getFrameRect().pos.y);
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().width, compositeShape.getFrameRect().width);
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().height, compositeShape.getFrameRect().height);
}

BOOST_AUTO_TEST_CASE(movingOperator)
{
  fedorov::Rectangle rectangle({ 0, 0 }, 20, 20);
  fedorov::Circle circle({ 10, 10 }, 10);
  fedorov::CompositeShape compositeShape(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));

  const double area = compositeShape.getArea();
  fedorov::rectangle_t frame = compositeShape.getFrameRect();

  fedorov::CompositeShape anotherCompositeShape = std::move(compositeShape);

  BOOST_CHECK_EQUAL(anotherCompositeShape.getArea(), area);
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().pos.x, frame.pos.x);
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().pos.y, frame.pos.y);
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().width, frame.width);
  BOOST_CHECK_EQUAL(anotherCompositeShape.getFrameRect().height, frame.height);
}

BOOST_AUTO_TEST_CASE(indexingOperator)
{
  std::shared_ptr<fedorov::Rectangle> rectangle(new fedorov::Rectangle({ 0, 0 }, 20, 20));
  std::shared_ptr<fedorov::Circle> circle(new fedorov::Circle({ 10, 10 }, 10));
  fedorov::CompositeShape compositeShape(rectangle, circle);

  BOOST_CHECK_EQUAL(&compositeShape[0], rectangle.get());
  BOOST_CHECK_EQUAL(&compositeShape[1], circle.get());
}

BOOST_AUTO_TEST_SUITE_END() // compositeShape/operators

BOOST_AUTO_TEST_SUITE_END() // compositeShape
