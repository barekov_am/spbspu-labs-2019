#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

#include <iostream>

namespace fedorov
{
  struct point_t
  {
    double x, y;
  };

  struct vector_t
  {
    double x, y;
  };
  
  point_t& operator+=(point_t& point, const vector_t& shiftVector);
  point_t& operator-=(point_t& point, const vector_t& shiftVector);

  point_t operator+(const point_t& point, const vector_t& shiftVector);
  point_t operator-(const point_t& point, const vector_t& shiftVector);
  
  vector_t& operator+=(vector_t& vector, const vector_t& shiftVector);
  vector_t& operator-=(vector_t& vector, const vector_t& shiftVector);

  vector_t operator+(const vector_t& vector, const vector_t& shiftVector);
  vector_t operator-(const vector_t& vector, const vector_t& shiftVector);
  vector_t operator-(const point_t& point1, const point_t& point2);
  vector_t operator*(const vector_t& pointVector, const double& scaleCoef);

  std::ostream& operator<<(std::ostream& stream, const point_t& point);
  
  struct rectangle_t
  {
    point_t pos;
    double width, height;
  };

  rectangle_t& operator+=(rectangle_t& frame, const rectangle_t& newFrame);

  rectangle_t operator+(const fedorov::rectangle_t& frame1, const fedorov::rectangle_t& frame2);

  std::ostream& operator<<(std::ostream& stream, const rectangle_t& frame);

  double getAngleInRadians(double angle);
  void rotateVector(vector_t& vector, const double angle);
}

#endif //BASE_TYPES_HPP
