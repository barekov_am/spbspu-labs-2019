#include "manipulators.hpp"

#include <iostream>
#include <cctype>

std::istream& lab::myws(std::istream& stream)
{
  while (std::isblank(stream.peek()))
  {
    stream.get();
  }

  return stream;
}

std::istream& lab::skipSpaceDelimiter(std::istream& stream)
{
  if (!std::isblank(stream.peek()))
  {
    stream.setstate(std::ios_base::failbit);
    return stream;
  }

  stream >> lab::myws;
  return stream;
}

std::istream& lab::operator>>(std::istream& stream, skipDelimiter&& manip)
{
  stream >> lab::myws;
  if (stream.peek() == manip.delimiter_)
  {
    stream.get();
  }
  else
  {
    stream.setstate(std::ios_base::failbit);
  }
  return stream;
}

std::istream& lab::operator>>(std::istream& stream, getSpecialWord&& manip)
{
  manip.word_.clear();

  if (stream.fail())
  {
    return stream;
  }

  std::string word;

  while (manip.isCorrectSymbol(stream.peek()))
  {
    word += stream.get();
  }

  if (!word.empty())
  {
    manip.word_ = word;
  }
  else
  {
    stream.setstate(std::ios_base::failbit);
  }

  return stream;
}

std::istream& lab::operator>>(std::istream& stream, getNumber&& manip)
{
  stream >> getSpecialWord(manip.number_, [](unsigned char ch) -> bool { return std::isdigit(ch); });

  return stream;
}

std::istream& lab::operator>>(std::istream& stream, getLongName&& manip)
{
  manip.name_.clear();

  if (stream.fail())
  {
    return stream;
  }

  std::string name;

  if (stream.peek() != '\"')
  {
    stream.setstate(std::ios_base::failbit);
    return stream;
  }

  stream.get();

  unsigned char symbol;

  while ((symbol = stream.peek()) && symbol != '\"' && symbol != '\n')
  {
    if (symbol == '\\')
    {
      stream.get();
      symbol = stream.peek();

      if (symbol != '\"' && symbol != '\\')
      {
        stream.setstate(std::ios_base::failbit);
        return stream;
      }
    }

    name += stream.get();
  }

  if (symbol != '\"')
  {
    stream.setstate(std::ios_base::failbit);
  }
  else
  {
    stream.get();
    manip.name_ = name;
  }

  return stream;
}

std::istream& lab::operator>>(std::istream& stream, getName&& manip)
{
  stream >> getSpecialWord(manip.name_, [](unsigned char ch) -> bool { return std::isalnum(ch) || ch == '-'; });

  return stream;
}

std::istream& lab::operator>>(std::istream& stream, getWord&& manip)
{
  stream >> getSpecialWord(manip.word_, [](unsigned char ch) -> bool { return std::isalpha(ch); });

  return stream;
}
