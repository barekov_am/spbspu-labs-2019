#ifndef POINT_HPP
#define POINT_HPP

#include <iostream>

namespace lab
{
  struct Point
  {
    int x, y;
  };

  std::istream& operator>>(std::istream& stream, Point& point);

  std::ostream& operator<<(std::ostream& stream, const Point& point);
}

#endif // !POINT_HPP
