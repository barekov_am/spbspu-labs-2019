#ifndef STREAM_STATE_SAVER_HPP
#define STREAM_STATE_SAVER_HPP

#include <iostream>

namespace lab
{
  class StreamStateSaver
  {
  public:
    StreamStateSaver(std::ios_base&);
    ~StreamStateSaver();

  private:
    std::ios_base& stream_;
    std::ios_base::fmtflags flags_;
    
  };
}



#endif // !STREAM_STATE_SAVER_HPP
