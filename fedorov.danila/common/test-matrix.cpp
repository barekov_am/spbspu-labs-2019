#include <stdexcept>
#include <memory>

#include "boost/test/auto_unit_test.hpp"

#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "matrix.hpp"

const double TOLERANCE = 0.001;

BOOST_AUTO_TEST_SUITE(matrix) // matrix

BOOST_AUTO_TEST_SUITE(mainMethods) // matrix/mainMethods

BOOST_AUTO_TEST_CASE(addingFigureProperly)
{
  fedorov::Matrix matrix;

  fedorov::Rectangle rectangle({ 0, 0 }, 10, 10);
  matrix.add(std::make_shared<fedorov::Rectangle>(rectangle));
  BOOST_CHECK_EQUAL(matrix.getAmount(), 1);

  fedorov::Circle circle({ 7, 7 }, 5);
  matrix.add(std::make_shared<fedorov::Circle>(circle));
  BOOST_CHECK_EQUAL(matrix.getAmount(), 2);
  BOOST_CHECK_EQUAL(matrix.getRows(), 2);

  fedorov::Circle anotherCircle({ 0, 0 }, 1);
  matrix.add(std::make_shared<fedorov::Circle>(anotherCircle));
  BOOST_CHECK_EQUAL(matrix.getAmount(), 3);
  BOOST_CHECK_EQUAL(matrix.getRows(), 2);
  BOOST_CHECK_EQUAL(matrix.getRowColumn(1), 2);

  fedorov::CompositeShape composite(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));
  matrix.add(std::make_shared<fedorov::CompositeShape>(composite));
  BOOST_CHECK_EQUAL(matrix.getAmount(), 4);
  BOOST_CHECK_EQUAL(matrix.getRows(), 3);
}

BOOST_AUTO_TEST_CASE(removingFigureProperly)
{
  fedorov::Matrix matrix;

  fedorov::Rectangle rectangle({ 0, 0 }, 10, 10);
  matrix.add(std::make_shared<fedorov::Rectangle>(rectangle));
  fedorov::Circle circle({ 7, 7 }, 5);
  matrix.add(std::make_shared<fedorov::Circle>(circle));
  fedorov::CompositeShape composite(std::make_shared<fedorov::Rectangle>(rectangle), std::make_shared<fedorov::Circle>(circle));
  matrix.add(std::make_shared<fedorov::CompositeShape>(composite));
  fedorov::Circle anotherCircle({ 0, 0 }, 1);
  matrix.add(std::make_shared<fedorov::Circle>(anotherCircle));

  matrix.remove(1, 0);
  BOOST_CHECK_EQUAL(matrix.getAmount(), 3);
  BOOST_CHECK_EQUAL(matrix.getRows(), 3);

  matrix.remove(1);
  BOOST_CHECK_EQUAL(matrix.getAmount(), 2);
  BOOST_CHECK_EQUAL(matrix.getRows(), 2);

  matrix.remove(1);
  matrix.remove(0);
  BOOST_CHECK_EQUAL(matrix.getAmount(), 0);
  BOOST_CHECK_EQUAL(matrix.getRows(), 0);
}

BOOST_AUTO_TEST_CASE(divideFigures)
{
  std::shared_ptr<fedorov::Circle> circle1(new fedorov::Circle({ 5, 5 }, 5));
  std::shared_ptr<fedorov::Circle> circle2(new fedorov::Circle({ 5, 5 }, 5));
  std::shared_ptr<fedorov::Circle> circle3(new fedorov::Circle({ 20, 20 }, 5));

  fedorov::CompositeShape figures(circle1, circle2);
  figures.add(circle3);

  fedorov::Matrix matrix = fedorov::divideFigures(figures);

  BOOST_CHECK_EQUAL(matrix[0][0], circle1);
  BOOST_CHECK_EQUAL(matrix[1][0], circle2);
  BOOST_CHECK_EQUAL(matrix[0][1], circle3);
}

BOOST_AUTO_TEST_SUITE_END() // matrix/mainMethods

BOOST_AUTO_TEST_SUITE(operators) // matrix/operators

BOOST_AUTO_TEST_CASE(indexingOperator)
{
  fedorov::Matrix matrix;
  std::shared_ptr<fedorov::Circle> circle1(new fedorov::Circle({ 5, 5 }, 5));
  matrix.add(circle1);
  std::shared_ptr<fedorov::Circle> circle2(new fedorov::Circle({ 5, 5 }, 5));
  matrix.add(circle2);
  std::shared_ptr<fedorov::Circle> circle3(new fedorov::Circle({ 20, 20 }, 5));
  matrix.add(circle3);

  BOOST_CHECK_EQUAL(matrix[0][0], circle1);
  BOOST_CHECK_EQUAL(matrix[1][0], circle2);
  BOOST_CHECK_EQUAL(matrix[0][1], circle3);
}

BOOST_AUTO_TEST_CASE(copyingOperator)
{
  fedorov::Matrix matrix;
  std::shared_ptr<fedorov::Circle> circle1(new fedorov::Circle({ 5, 5 }, 5));
  matrix.add(circle1);
  std::shared_ptr<fedorov::Circle> circle2(new fedorov::Circle({ 5, 5 }, 5));
  matrix.add(circle2);

  fedorov::Matrix newMatrix(matrix);

  BOOST_CHECK_EQUAL(newMatrix.getAmount(), matrix.getAmount());
  BOOST_CHECK_EQUAL(newMatrix.getRows(), matrix.getRows());
  BOOST_CHECK_EQUAL(newMatrix[0][0], matrix[0][0]);
  BOOST_CHECK_EQUAL(newMatrix[1][0], matrix[1][0]);
}

BOOST_AUTO_TEST_CASE(movingOperator)
{
  fedorov::Matrix matrix;
  std::shared_ptr<fedorov::Circle> circle1(new fedorov::Circle({ 5, 5 }, 5));
  matrix.add(circle1);
  std::shared_ptr<fedorov::Circle> circle2(new fedorov::Circle({ 5, 5 }, 5));
  matrix.add(circle2);

  const unsigned int figureAmount = matrix.getAmount();
  const unsigned int figureRows = matrix.getRows();

  fedorov::Matrix newMatrix(std::move(matrix));

  BOOST_CHECK_EQUAL(newMatrix.getAmount(), figureAmount);
  BOOST_CHECK_EQUAL(newMatrix.getRows(), figureRows);
  BOOST_CHECK_EQUAL(newMatrix[0][0], circle1);
  BOOST_CHECK_EQUAL(newMatrix[1][0], circle2);
}

BOOST_AUTO_TEST_SUITE_END() // matrix/operators

BOOST_AUTO_TEST_SUITE(argumentsCheck) // matrix/argumentsCheck

BOOST_AUTO_TEST_CASE(addingEmpryFigure)
{
  fedorov::Matrix matrix;

  BOOST_CHECK_THROW(matrix.add(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(indexingWrongRow)
{
  fedorov::Matrix matrix;

  std::shared_ptr<fedorov::Circle> circle(new fedorov::Circle({ 5, 5 }, 5));
  matrix.add(circle);

  BOOST_CHECK_THROW(matrix[1], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(removingWrongFigure)
{
  fedorov::Matrix matrix;

  std::shared_ptr<fedorov::Circle> circle(new fedorov::Circle({ 5, 5 }, 5));
  matrix.add(circle);

  BOOST_CHECK_THROW(matrix.remove(1), std::out_of_range);
  BOOST_CHECK_THROW(matrix.remove(0, 1), std::out_of_range);
  BOOST_CHECK_THROW(matrix.remove(1, 0), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(gettingEmptyRow)
{
  fedorov::Matrix matrix;

  std::shared_ptr<fedorov::Circle> circle(new fedorov::Circle({ 5, 5 }, 5));
  matrix.add(circle);

  BOOST_CHECK_THROW(matrix.getRowColumn(1), std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END() // matrix/argumentsCheck

BOOST_AUTO_TEST_SUITE_END() // matrix
