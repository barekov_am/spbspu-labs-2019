#include "stream-state-saver.hpp"

lab::StreamStateSaver::StreamStateSaver(std::ios_base& stream) :
  stream_(stream),
  flags_(stream.flags())
{}

lab::StreamStateSaver::~StreamStateSaver()
{
  stream_.setf(flags_);
}
