#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>

#include "shape.hpp"
#include "composite-shape.hpp"

namespace fedorov
{
  class Matrix
  {
  public:
    Matrix();

    Matrix(const Matrix& matrix);
    Matrix(Matrix&& matrix);
    
    ~Matrix() = default;

    Matrix& operator=(const Matrix& matrix);
    Matrix& operator=(Matrix&& matrix);

    CompositeShape::figurePointerType* operator[](const unsigned int index) const;

    unsigned int getAmount() const;
    unsigned int getRows() const;
    unsigned int getRowColumn(const unsigned int row) const;

    void print() const;
    void add(CompositeShape::figurePointerType figure);
    void remove(const unsigned int index);
    void remove(const unsigned int row, const unsigned int column);
    void clear();

  private:
    unsigned int rows_;
    std::unique_ptr<unsigned int[]> columns_;
    CompositeShape::figureArrayType figureList_;

    CompositeShape::figureArrayType getArrayCopy() const;
    std::unique_ptr<unsigned int[]> getColumnsCopy() const;

    unsigned int getIndexRow(const unsigned int index) const;

    bool figuresIntersets(const CompositeShape::figurePointerType figure1, const CompositeShape::figurePointerType figure2) const;
  };

  Matrix divideFigures(CompositeShape& figures);
}

#endif // MATRIX_HPP
