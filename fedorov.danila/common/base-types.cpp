#include "base-types.hpp"

#include <algorithm>
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>


fedorov::point_t& fedorov::operator+=(point_t& point, const vector_t& shiftVector)
{
  point.x += shiftVector.x;
  point.y += shiftVector.y;
  return point;
}

fedorov::point_t& fedorov::operator-=(point_t& point, const vector_t& shiftVector)
{
  point.x -= shiftVector.x;
  point.y -= shiftVector.y;
  return point;
}

fedorov::point_t fedorov::operator+(const point_t& point, const vector_t& shiftVector)
{
  point_t newPoint = point;
  return newPoint += shiftVector;
}

fedorov::point_t fedorov::operator-(const point_t& point, const vector_t& shiftVector)
{
  point_t newPoint = point;
  return newPoint -= shiftVector;
}


fedorov::vector_t& fedorov::operator+=(vector_t& vector, const vector_t& shiftVector)
{
  vector.x += shiftVector.x;
  vector.y += shiftVector.y;
  return vector;
}

fedorov::vector_t& fedorov::operator-=(vector_t& vector, const vector_t& shiftVector)
{
  vector.x -= shiftVector.x;
  vector.y -= shiftVector.y;
  return vector;
}

fedorov::vector_t fedorov::operator+(const vector_t& vector, const vector_t& shiftVector)
{
  vector_t newVector = vector;
  return newVector += shiftVector;
}

fedorov::vector_t fedorov::operator-(const vector_t& vector, const vector_t& shiftVector)
{
  vector_t newVector = vector;
  return newVector -= shiftVector;
}

fedorov::vector_t fedorov::operator-(const point_t& point1, const point_t& point2)
{
  return { point1.x - point2.x, point1.y - point2.y };
}

fedorov::vector_t fedorov::operator*(const vector_t& pointVector, const double& scaleCoef)
{
  return { pointVector.x * scaleCoef, pointVector.y * scaleCoef };
}

std::ostream& fedorov::operator<<(std::ostream& stream, const point_t& point)
{
  stream << "(" << point.x << "; " << point.y << ")";
  return stream;
}


fedorov::rectangle_t& fedorov::operator+=(rectangle_t& frame, const rectangle_t& newFrame)
{
  double bottom = std::min(frame.pos.y - frame.height / 2, newFrame.pos.y - newFrame.height / 2);
  double upper = std::max(frame.pos.y + frame.height / 2, newFrame.pos.y + newFrame.height / 2);
  double left = std::min(frame.pos.x - frame.width / 2, newFrame.pos.x - newFrame.width / 2);
  double right = std::max(frame.pos.x + frame.width / 2, newFrame.pos.x + newFrame.width / 2);

  frame = { { (left + right) / 2, (bottom + upper) / 2 }, right - left, upper - bottom };
  return frame;
}

fedorov::rectangle_t fedorov::operator+(const fedorov::rectangle_t& frame1, const fedorov::rectangle_t& frame2)
{
  rectangle_t newFrame = frame1;
  return newFrame += frame2;
}

std::ostream& fedorov::operator<<(std::ostream& stream, const rectangle_t& frame)
{
  stream << "\t\tcenter: " << frame.pos << std::endl
    << "\t\twidth: " << frame.width << "\theight: " << frame.height;
  return stream;
}

void fedorov::rotateVector(vector_t& vector, const double angle)
{
  double correctAngle = getAngleInRadians(angle);
  vector = { vector.x * cos(correctAngle) - vector.y * sin(correctAngle),
      vector.x * sin(correctAngle) + vector.y * cos(correctAngle) };
}

double fedorov::getAngleInRadians(double angle)
{
  return angle * M_PI / 180;;
}
