#include "matrix.hpp"

#include "circle.hpp"

fedorov::Matrix::Matrix():
    rows_(0),
    columns_(),
    figureList_()
{}

fedorov::Matrix::Matrix(const Matrix& matrix):
    rows_(matrix.rows_),
    columns_(matrix.getColumnsCopy()),
    figureList_(matrix.getArrayCopy())
{}

fedorov::Matrix::Matrix(Matrix&& matrix):
    rows_(matrix.rows_),
    columns_(std::move(matrix.columns_)),
    figureList_(std::move(matrix.figureList_))
{
  matrix.rows_ = 0;
}

fedorov::Matrix& fedorov::Matrix::operator=(const Matrix& matrix)
{
  if (this != &matrix)
  {
    rows_ = matrix.rows_;
    columns_ = matrix.getColumnsCopy();
    figureList_ = matrix.getArrayCopy();
  }
  return *this;
}

fedorov::Matrix& fedorov::Matrix::operator=(Matrix&& matrix)
{
  if (this != &matrix)
  {
    rows_ = matrix.rows_;
    columns_ = std::move(matrix.columns_);
    figureList_ = std::move(matrix.figureList_);

    matrix.rows_ = 0;;
  }
  return *this;
}

fedorov::CompositeShape::figurePointerType* fedorov::Matrix::operator[](const unsigned int index) const
{
  if (index >= rows_)
  {
    throw std::out_of_range("Index out of range");
  }

  unsigned int figureIndex = 0;
  for (unsigned int i = 0; i < index; i++)
  {
    figureIndex += columns_[i];
  }

  return &(figureList_[figureIndex]);
}

unsigned int fedorov::Matrix::getAmount() const
{
  unsigned int amount = 0;
  for (unsigned int i = 0; i < rows_; i++)
  {
    amount += columns_[i];
  }
  return amount;
}

unsigned int fedorov::Matrix::getRows() const
{
  return rows_;
}

unsigned int fedorov::Matrix::getRowColumn(const unsigned int row) const
{
  if (row >= rows_)
  {
    throw std::out_of_range("Try to get empty row");
  }

  return columns_[row];
}

void fedorov::Matrix::print() const
{
  std::cout << "Matrix:" << std::endl;
  for (unsigned int i = 0; i < rows_; i++)
  {
    std::cout << "row "  << i << std::endl;
    for (unsigned int j = 0; j < columns_[i]; j++)
    {
      std::cout << "figure " << j << std::endl;
      (*this)[i][j]->printInfo();
    }
    std::cout << std::endl;
  }
}

void fedorov::Matrix::add(CompositeShape::figurePointerType figure)
{
  if (figure == nullptr)
  {
    throw std::invalid_argument("Empty pointer");
  }

  if (rows_ == 0)
  {
    rows_ = 1;
    std::unique_ptr<unsigned int[]> column(new unsigned int[1]);
    column[0] = 1;
    columns_.swap(column);

    CompositeShape::figureArrayType array(new std::shared_ptr<Shape>[1]);
    array[0] = figure;
    figureList_.swap(array);
  }
  else
  {
    unsigned int currentIndex = getAmount() - 1;
    unsigned int row = 0;


    for (int i = rows_ - 1; i >= 0; i--)
    {
      for (int j = columns_[i] - 1; j >= 0; j--, currentIndex--)
      {
        if (figuresIntersets(figureList_[currentIndex], figure) || figuresIntersets(figure, figureList_[currentIndex]))
        {
          row = i + 1;
          break;
        }
      }

      if (row > 0)
      {
        break;
      }
    }

    if (row == rows_)
    {
      std::unique_ptr<unsigned int[]> newColumns(new unsigned int[rows_ + 1]);

      for (unsigned int i = 0; i < rows_; i++)
      {
        newColumns[i] = columns_[i];
      }
      newColumns[row] = 0;

      columns_.swap(newColumns);
      rows_++;
    }

    CompositeShape::figureArrayType tmp(new CompositeShape::figurePointerType[getAmount() + 1]);

    currentIndex = 0;
    for (unsigned int i = 0; i <= row; i++)
    {
      for (unsigned int j = 0; j < columns_[i]; j++, currentIndex++)
      {
        tmp[currentIndex] = figureList_[currentIndex];
      }
    }

    tmp[currentIndex] = figure;

    for (unsigned int i = row + 1; i < rows_; i++)
    {
      for (unsigned int j = 0; j < columns_[i]; j++, currentIndex++)
      {
        tmp[currentIndex + 1] = figureList_[currentIndex];
      }
    }

    columns_[row]++;
    figureList_.swap(tmp);
  }
}

void fedorov::Matrix::remove(const unsigned int index)
{
  unsigned int newAmount = getAmount();
  if (index >= newAmount)
  {
    throw std::out_of_range("Try to remove empty figure");
  }

  newAmount--;

  if (newAmount == 0)
  {
    clear();
  }
  else
  {
    CompositeShape::figureArrayType tmp(new CompositeShape::figurePointerType[newAmount]);

    for (unsigned int i = 0; i < index; i++)
    {
      tmp[i] = figureList_[i];
    }

    for (unsigned int i = index; i < newAmount; i++)
    {
      tmp[i] = figureList_[i + 1];
    }

    figureList_.swap(tmp);

    unsigned int row = getIndexRow(index);
    columns_[row]--;

    if (columns_[row] == 0)
    {
      std::unique_ptr<unsigned int[]> newColumns(new unsigned int[rows_ - 1]);

      for (unsigned int i = 0; i < row; i++)
      {
        newColumns[i] = columns_[i];
      }

      for (unsigned int i = row; i < rows_ - 1; i++)
      {
        newColumns[i] = columns_[i + 1];
      }

      columns_.swap(newColumns);
      rows_--;
    }
  }
}

void fedorov::Matrix::remove(const unsigned int row, const unsigned int column)
{
  if (row >= rows_)
  {
    throw std::out_of_range("Try to remove figure from empty row");
  }

  if (column >= columns_[row])
  {
    throw std::out_of_range("Try to remove empty figure");
  }
  unsigned int index = 0;

  for (unsigned int i = 0; i < row; i++)
  {
    index += columns_[i];
  }

  index += column;
  
  remove(index);
}

void fedorov::Matrix::clear()
{
  rows_ = 0;
  columns_.reset();
  figureList_.reset();
}

fedorov::CompositeShape::figureArrayType fedorov::Matrix::getArrayCopy() const
{
  unsigned int amount = getAmount();
  CompositeShape::figureArrayType tmp(new CompositeShape::figurePointerType[amount]);
  

  for (unsigned int i = 0; i < amount; i++)
  {
    tmp[i] = figureList_[i];
  }

  return tmp;
}

std::unique_ptr<unsigned int[]> fedorov::Matrix::getColumnsCopy() const
{
  std::unique_ptr<unsigned int[]> tmp(new unsigned int [rows_]);
  
  for (unsigned int i = 0; i < rows_; i++)
  {
    tmp[i] = columns_[i];
  }

  return tmp;
}

unsigned int fedorov::Matrix::getIndexRow(const unsigned int index) const
{
  unsigned int indexes = 0;
  for (unsigned int i = 0; i < rows_; i++)
  {
    indexes += columns_[i];
    if (indexes > index)
    {
      return i;
    }
  }

  throw std::out_of_range("try to find n index");
}

bool fedorov::Matrix::figuresIntersets(const CompositeShape::figurePointerType figure1, const CompositeShape::figurePointerType figure2) const
{
  rectangle_t frame1 = figure1->getFrameRect(), frame2 = figure2->getFrameRect();

  if (((frame1.pos.y + frame1.height / 2) <= (frame2.pos.y + frame2.height / 2))
      && ((frame1.pos.y + frame1.height / 2) >= (frame2.pos.y - frame2.height / 2)))
  {
    if (((frame1.pos.x + frame1.width / 2) <= (frame2.pos.x + frame2.width / 2))
        && ((frame1.pos.x + frame1.width / 2) >= (frame2.pos.x - frame2.width / 2)))
    {
      return true;
    }

    if (((frame1.pos.x - frame1.width / 2) <= (frame2.pos.x + frame2.width / 2))
        && ((frame1.pos.x - frame1.width / 2) >= (frame2.pos.x - frame2.width / 2)))
    {
      return true;
    }
  }

  return false;
}

fedorov::Matrix fedorov::divideFigures(fedorov::CompositeShape& figures)
{
  Matrix matrix;
  unsigned int amount = figures.getAmount();

  for (unsigned int i = 0; i < amount; i++)
  {
    matrix.add(figures.figureList_[i]);
  }
  
  return matrix;
}
