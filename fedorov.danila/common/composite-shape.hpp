#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>

#include "shape.hpp"

namespace fedorov
{
  class Matrix;

  class CompositeShape:
      public Shape
  {
  public:
    using figurePointerType = std::shared_ptr<Shape>;
    using figureArrayType = std::unique_ptr<figurePointerType[]>;

    CompositeShape();
    CompositeShape(const CompositeShape& figure);
    CompositeShape(CompositeShape&& figure);
    // Constructor used as a fast way of creating elementary composit shape
    CompositeShape(figurePointerType figure1, figurePointerType figure2);
    ~CompositeShape() = default;

    CompositeShape& operator=(const CompositeShape& figure);
    CompositeShape& operator=(CompositeShape&& figure);
    Shape& operator[](unsigned int index) const;

    rectangle_t getFrameRect() const override;
    point_t getPosition() const override;
    double getArea() const override;
    unsigned int getAmount() const;

    void printInfo() const override;
    void scale(const double scale) override;
    void move(const point_t& point) override;
    void move(const vector_t& shiftVector);
    void move(const double dx, const double dy) override;
    void rotate(const double angle) override;
    void add(figurePointerType figure);
    void remove(const unsigned int figureNumber);
    void clear();

  private:
    unsigned int amount_;
    figureArrayType figureList_;

    figureArrayType getArrayCopy() const;

    friend Matrix divideFigures(CompositeShape& figures);
  };
}

#endif // COMPOSITE_SHAPE_HPP
