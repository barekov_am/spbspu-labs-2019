#include <stdexcept>

#include "boost/test/auto_unit_test.hpp"

#include "circle.hpp"

#define COMPARE_TOLERANCE 0.001

BOOST_AUTO_TEST_SUITE(circle) // circle

BOOST_AUTO_TEST_SUITE(move) // circle/move

BOOST_AUTO_TEST_CASE(radiusNotChanged)
{
  fedorov::Circle circle({ 0, 0 }, 3.5);
  double radius = circle.getRadius();

  circle.move(1.5, 2.5);
  BOOST_CHECK_EQUAL(circle.getRadius(), radius);

  circle.move(fedorov::point_t{ 4.1, 2.6 });
  BOOST_CHECK_EQUAL(circle.getRadius(), radius);

  circle.move(fedorov::vector_t{ 1.2, -2.3 });
  BOOST_CHECK_EQUAL(circle.getRadius(), radius);
}

BOOST_AUTO_TEST_CASE(areaNotChanged)
{
  fedorov::Circle circle({ 0, 0 }, 3.5);
  double area = circle.getArea();

  circle.move(1.5, 2.5);
  BOOST_CHECK_EQUAL(circle.getArea(), area);

  circle.move(fedorov::point_t{ 4.1, 2.6 });
  BOOST_CHECK_EQUAL(circle.getArea(), area);

  circle.move(fedorov::vector_t{ 1.2, -2.3 });
  BOOST_CHECK_EQUAL(circle.getArea(), area);
}

BOOST_AUTO_TEST_SUITE_END() // circle/move

BOOST_AUTO_TEST_SUITE(scale) // circle/scale

BOOST_AUTO_TEST_CASE(areaScaledCorrectly)
{
  fedorov::Circle circle({ 0, 0 }, 3.5);
  double area = circle.getArea();

  double scale = 2.5;
  double areaScaled = area * scale * scale;

  circle.scale(scale);

  BOOST_CHECK_CLOSE(circle.getArea(), areaScaled, COMPARE_TOLERANCE);

  BOOST_CHECK_CLOSE(circle.getArea() / area, scale * scale, COMPARE_TOLERANCE);
}

BOOST_AUTO_TEST_SUITE_END() // circle/scale

BOOST_AUTO_TEST_SUITE(parametersCheck) // circle/parametersCheck

BOOST_AUTO_TEST_CASE(wrongRadius)
{
  BOOST_CHECK_THROW(fedorov::Circle circle({ 0, 0 }, -3); , std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(wrongScaleCoefficient)
{
  fedorov::Circle circle({ 0, 0 }, 3);
  BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END() // circle/parametersCheck

BOOST_AUTO_TEST_SUITE_END() // circle
