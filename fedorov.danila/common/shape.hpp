#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"

namespace fedorov
{
  class Shape
  {
  public:
    Shape() = default;
    Shape(const point_t &position);
    virtual ~Shape() = default;

    virtual rectangle_t getFrameRect() const = 0;
    virtual point_t getPosition() const;
    virtual double getArea() const = 0;

    virtual void printInfo() const;
    virtual void scale(double) = 0;
    virtual void move(const point_t& point);
    virtual void move(const vector_t& shiftVector);
    virtual void move(const double dx, const double dy);
    virtual void rotate(const double) = 0;

  protected:
    point_t position_;
  };
}

#endif //SHAPE_HPP
