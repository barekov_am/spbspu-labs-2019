#include "point.hpp"

#include "manipulators.hpp"
#include "stream-state-saver.hpp"

std::istream& lab::operator>>(std::istream& stream, Point& point)
{
  if (stream.fail())
  {
    return stream;
  }

  lab::StreamStateSaver safe(stream);

  int cordX, cordY;
  stream >> std::noskipws >> lab::skipDelimiter('(')
         >> lab::myws >> cordX >> lab::skipDelimiter(';')
         >> lab::myws >> cordY >> lab::skipDelimiter(')');

  if (stream.fail())
  {
    return stream;
  }

  point = { cordX, cordY };
  return stream;
}

std::ostream& lab::operator<<(std::ostream& stream, const Point& point)
{
  stream << "(" << point.x << ";" << point.y << ")";
  return stream;
}
