#include "factorial.hpp"

#include <iostream>
#include <limits>
#include <iterator>
#include <memory>
#include <stdexcept>


lab::Factorial::Factorial(const unsigned short max):
    Factorial(1, max)
{}

lab::Factorial::Factorial(const unsigned short min, const unsigned short max):
    beginIndex_(min),
    endIndex_(max + 1)
{
  if (beginIndex_ > MAX_INDEX || endIndex_ > MAX_INDEX || beginIndex_ > endIndex_)
  {
    throw std::invalid_argument("Incorrect factorial edges");
  }

  minValue_ = factorial(min);
  maxValue_ = factorial(max + 1);
}

unsigned long long lab::Factorial::factorial(const size_t number) const
{
  return number == 0 ? 1 : factorial(number - 1) * number;
}

lab::Factorial::FactorialIterator lab::Factorial::begin()
{
  return FactorialIterator(minValue_, beginIndex_, endIndex_, beginIndex_);
}

lab::Factorial::FactorialIterator lab::Factorial::end()
{
  return FactorialIterator(maxValue_, beginIndex_, endIndex_, endIndex_);
}

std::reverse_iterator< lab::Factorial::FactorialIterator > lab::Factorial::rbegin()
{
  return std::reverse_iterator< lab::Factorial::FactorialIterator >(end());
}

std::reverse_iterator< lab::Factorial::FactorialIterator > lab::Factorial::rend()
{
  return std::reverse_iterator< lab::Factorial::FactorialIterator >(begin());
}

lab::Factorial::FactorialIterator::FactorialIterator(unsigned long long value, const unsigned short beginIndex,
    const unsigned short endIndex, const unsigned short index):
    value_(value),
    beginIndex_(beginIndex),
    endIndex_(endIndex),
    index_(index)
{}

bool lab::Factorial::FactorialIterator::operator==(const FactorialIterator& rhs) const
{
  return (beginIndex_ == rhs.beginIndex_) && (endIndex_ == rhs.endIndex_) && (index_ == rhs.index_);
}

bool lab::Factorial::FactorialIterator::operator!=(const FactorialIterator& rhs) const
{
  return !(*this == rhs);
}

unsigned long long lab::Factorial::FactorialIterator::operator*() const
{
  return value_;
}

lab::Factorial::FactorialIterator& lab::Factorial::FactorialIterator::operator++()
{
  if (index_ == endIndex_)
  {
    throw std::out_of_range("Increment of end iterator is invalid");
  }

  ++index_;
  value_ *= index_;
  return *this;
}

lab::Factorial::FactorialIterator lab::Factorial::FactorialIterator::operator++(int)
{
  auto temp = *this;

  ++(*this);

  return temp;
}

lab::Factorial::FactorialIterator& lab::Factorial::FactorialIterator::operator--()
{
  if (index_ == beginIndex_)
  {
    throw std::out_of_range("Decrement of begin iterator is invalid");
  }

  value_ /= index_;
  --index_;
  return *this;
}

lab::Factorial::FactorialIterator lab::Factorial::FactorialIterator::operator--(int)
{
  auto temp = *this;

  --(*this);

  return temp;
}
