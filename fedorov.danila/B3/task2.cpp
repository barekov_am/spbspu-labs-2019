#include "tasks.hpp"

#include <iostream>
#include <iterator>

#include "factorial.hpp"

void lab::task2()
{
  lab::Factorial factorial(10);

  std::copy(factorial.begin(), factorial.end(), std::ostream_iterator< unsigned long long >(std::cout, " "));
  std::cout << "\n";
  std::copy(factorial.rbegin(), factorial.rend(), std::ostream_iterator< unsigned long long >(std::cout, " "));
  std::cout << "\n";
}
