#include "book-interface.hpp"

#include <iostream>
#include <string>

#include "manipulators.hpp"

lab::CommandParsing::functionPointer lab::CommandParsing::parseCommand(std::istream& inputStream)
{
  const mapType commands{
      { "add",    parseAddCommand },
      { "store",  parseStoreCommand },
      { "insert", parseInsertCommand },
      { "delete", parseDeleteCommand },
      { "show",   parseShowCommand },
      { "move",   parseMoveCommand } };

  std::string command;

  inputStream >> std::noskipws >> std::ws >> command;

  if (inputStream.fail())
  {
    if (inputStream.eof())
    {
      return [](BookInterface*, std::ostream&) {};
    }

    throw std::runtime_error("Stream reading failed");
  }

  auto mapElement = commands.find(command);
  functionPointer func;

  if (mapElement == commands.end())
  {
    func = &lab::BookInterface::failedCommand;
  }
  else
  {
    func = mapElement->second(inputStream);
  }

  if (inputStream.fail())
  {
    inputStream.clear();
  }
  std::getline(inputStream, command);

  return func;
}

lab::CommandParsing::functionPointer lab::CommandParsing::parseAddCommand(std::istream& inputStream)
{
  std::string number, name;

  inputStream >> lab::myws >> lab::getNumber(number) >> skipSpaceDelimiter
              >> lab::myws >> lab::getLongName(name) >> lab::myws;

  if (number.empty() || name.empty() || inputStream.peek() != '\n')
  {
    return &lab::BookInterface::failedCommand;
  }

  return std::bind(&lab::BookInterface::executeAddCommand, std::placeholders::_1, std::placeholders::_2, number, name);
}

lab::CommandParsing::functionPointer lab::CommandParsing::parseStoreCommand(std::istream& inputStream)
{
  std::string markName, newMarkName;

  inputStream >> lab::myws >> lab::getName(markName) >> skipSpaceDelimiter
              >> lab::getName(newMarkName) >> lab::myws;

  if (markName.empty() || newMarkName.empty() || inputStream.peek() != '\n')
  {
    return &lab::BookInterface::failedCommand;
  }

  return std::bind(&lab::BookInterface::executeStoreCommand, std::placeholders::_1, std::placeholders::_2, markName, newMarkName);
}

lab::CommandParsing::functionPointer lab::CommandParsing::parseInsertCommand(std::istream& inputStream)
{
  std::string insertPosition, markName, number, name;

  inputStream >> lab::myws >> lab::getWord(insertPosition) >> skipSpaceDelimiter
              >> lab::getName(markName) >> skipSpaceDelimiter
              >> lab::getNumber(number) >> skipSpaceDelimiter
              >> lab::getLongName(name) >> lab::myws;

  if (insertPosition.empty() || markName.empty() || number.empty() || name.empty() || inputStream.peek() != '\n')
  {
    return &lab::BookInterface::failedCommand;
  }

  return std::bind(&lab::BookInterface::executeInsertCommand, std::placeholders::_1, std::placeholders::_2, insertPosition, markName, number, name);
}

lab::CommandParsing::functionPointer lab::CommandParsing::parseDeleteCommand(std::istream& inputStream)
{
  std::string markName;
  inputStream >> lab::myws >> lab::getName(markName) >> lab::myws;

  if (markName.empty() || inputStream.peek() != '\n')
  {
    return &lab::BookInterface::failedCommand;
  }

  return std::bind(&lab::BookInterface::executeDeleteCommand, std::placeholders::_1, std::placeholders::_2, markName);
}

lab::CommandParsing::functionPointer lab::CommandParsing::parseShowCommand(std::istream& inputStream)
{
  std::string markName;
  inputStream >> lab::myws >> lab::getName(markName) >> lab::myws;

  if (markName.empty() || inputStream.peek() != '\n')
  {
    return &lab::BookInterface::failedCommand;
  }
  return std::bind(&lab::BookInterface::executeShowCommand, std::placeholders::_1, std::placeholders::_2, markName);
}

lab::CommandParsing::functionPointer lab::CommandParsing::parseMoveCommand(std::istream& inputStream)
{
  std::string markName, stepString;
  inputStream >> lab::myws >> lab::getName(markName) >> skipSpaceDelimiter >> stepString >> lab::myws;

  if (markName.empty() || stepString.empty() || inputStream.peek() != '\n')
  {
    return &lab::BookInterface::failedCommand;
  }

  return std::bind(&lab::BookInterface::executeMoveCommand, std::placeholders::_1, std::placeholders::_2, markName, stepString);
}
