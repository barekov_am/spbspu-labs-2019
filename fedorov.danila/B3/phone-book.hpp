#ifndef PHONE_BOOK_HPP
#define PHONE_BOOK_HPP

#include <iostream>
#include <list>
#include <vector>

namespace lab
{
  class PhoneBook
  {
  public:
    typedef struct
    {
      std::string phone;
      std::string name;
    } Record_t;

    typedef std::list< Record_t >::iterator iterator;

    iterator begin();
    iterator end();
    iterator insertBefore(const iterator& iter, const Record_t& record);
    iterator insertAfter(const iterator& iter, const Record_t& record);
    iterator erase(const iterator& iter);

    bool empty() const;

    void pushBack(const Record_t& iter);
    void changeRecord(const iterator& iter, const Record_t& record);
    
  private:
    std::list< Record_t > records;

    friend std::ostream& operator<<(std::ostream& stream, const lab::PhoneBook::Record_t record);

  };

  std::ostream& operator<<(std::ostream&, const lab::PhoneBook::Record_t);
}



#endif // !PHONE_BOOK_HPP
