#ifndef BOOK_INTERFACE_HPP
#define BOOK_INTERFACE_HPP

#include <iostream>
#include <unordered_map>
#include <vector>
#include <functional>

#include "phone-book.hpp"


namespace lab
{
  class CommandParsing;

  class BookInterface
  {
  public:
    BookInterface();

    void processBookCommand(std::istream& inputStream, std::ostream& outputStream);

  private:
    typedef struct
    {
      std::string name;
      PhoneBook::iterator mark;
    } BookMark;

    typedef  std::vector< BookMark >::iterator iterator;

    PhoneBook book_;
    std::vector< BookMark > bookMarks_;

    void executeAddCommand(std::ostream& outputStream, const std::string& number, const std::string& name);
    void executeStoreCommand(std::ostream& outputStream, const std::string& markName, const std::string& newMarkName);
    void executeInsertCommand(std::ostream& outputStream, const std::string& position, const std::string& markName,
        const std::string& number, const std::string& name);
    void executeDeleteCommand(std::ostream& outputStream, const std::string& markName);
    void executeShowCommand(std::ostream& outputStream, const std::string& markName);
    void executeMoveCommand(std::ostream& outputStream, const std::string& markName, const std::string& stepString);

    void failedCommand(std::ostream& outputStream) const;
    void invalidBookMark(std::ostream& outputStream) const;
    void emptyBook(std::ostream& outputStream) const;
    void invalidStep(std::ostream& outStream) const;

    iterator findBookMark(const std::string& markName);
    void resetMarks();

    friend lab::CommandParsing;
  };

  class CommandParsing
  {
  public:
    typedef std::function< void(BookInterface*, std::ostream&) > functionPointer;

    static functionPointer parseCommand(std::istream& stream);

  private:
    typedef std::function < functionPointer(std::istream&) > parser;
    typedef std::unordered_map < std::string, parser > mapType;

    static functionPointer parseAddCommand(std::istream& inputStream);
    static functionPointer parseStoreCommand(std::istream& inputStream);
    static functionPointer parseInsertCommand(std::istream& inputStream);
    static functionPointer parseDeleteCommand(std::istream& inputStream);
    static functionPointer parseShowCommand(std::istream& inputStream);
    static functionPointer parseMoveCommand(std::istream& inputStream);
  };
}

#endif // !BOOK_INTERFACE_HPP
