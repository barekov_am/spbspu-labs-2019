#ifndef FACTORIAL_HPP
#define FACTORIAL_HPP

#include <climits>
#include <iterator>
#include <memory>
#include <cstddef>

namespace lab
{
  static constexpr unsigned short getMaxFactorialIndex(unsigned short index, unsigned long long factorial)
  {
    if (factorial / index > 1)
    {
      return getMaxFactorialIndex(index + 1, factorial / index);
    }

    return index;
  }

  class Factorial
  {
  public:
    class FactorialIterator;

    Factorial(const unsigned short max);
    Factorial(const unsigned short min, const unsigned short max);

    FactorialIterator begin();
    FactorialIterator end();

    std::reverse_iterator< FactorialIterator > rbegin();
    std::reverse_iterator< FactorialIterator > rend();

  private:
    static constexpr unsigned short MAX_INDEX = getMaxFactorialIndex(1, ULLONG_MAX) - 1;

    unsigned long long minValue_;
    unsigned long long maxValue_;
    unsigned short beginIndex_;
    unsigned short endIndex_;

    unsigned long long factorial(const size_t number) const;
  };

  class Factorial::FactorialIterator:
      public std::iterator< std::bidirectional_iterator_tag,
                            unsigned long long,
                            std::ptrdiff_t,
                            unsigned long long,
                            unsigned long long >
  {
  public:
    bool operator==(const FactorialIterator& rhs) const;
    bool operator!=(const FactorialIterator& rhs) const;

    value_type operator*() const;

    FactorialIterator& operator++();
    FactorialIterator operator++(int);

    FactorialIterator& operator--();
    FactorialIterator operator--(int);

  private:
    unsigned long long value_; 
    unsigned short beginIndex_;
    unsigned short endIndex_;
    unsigned short index_;

    FactorialIterator(unsigned long long value, const unsigned short beginIndex, 
        const unsigned short endIndex, const unsigned short index);

    friend class Factorial;

  };

}


#endif // !FACTORIAL_HPP
