#include "tasks.hpp"

#include <iostream>

#include "book-interface.hpp"
#include "manipulators.hpp"

void lab::task1()
{
  lab::BookInterface interface;

  while (!std::cin.eof())
  {
    interface.processBookCommand(std::cin, std::cout);
  }
}
