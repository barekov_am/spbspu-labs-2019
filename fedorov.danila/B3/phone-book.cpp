#include "phone-book.hpp"

#include <stdexcept>
#include <iostream>
#include <list>
#include <vector>


lab::PhoneBook::iterator lab::PhoneBook::begin()
{
  auto i = records.begin();
  return i;
}

lab::PhoneBook::iterator lab::PhoneBook::end()
{
  auto i = records.end();
  return i;
}

lab::PhoneBook::iterator lab::PhoneBook::insertBefore(const iterator& iter, const Record_t& record)
{
  return records.insert(iter, record);
}

lab::PhoneBook::iterator lab::PhoneBook::insertAfter(const iterator& iter, const Record_t& record)
{
  if (iter == records.end())
  {
    throw std::out_of_range("Try to add element after list.end()");
  }

  iterator temp = iter;
  ++temp;

  return records.insert(temp, record);
}

lab::PhoneBook::iterator lab::PhoneBook::erase(const iterator& iter)
{
  return records.erase(iter);
}

bool lab::PhoneBook::empty() const
{
  return records.empty();
}

void lab::PhoneBook::pushBack(const Record_t& record)
{
  records.push_back(record);
}

void lab::PhoneBook::changeRecord(const iterator& iter, const Record_t& record)
{
  *iter = record;
}

std::ostream& lab::operator<<(std::ostream& stream, const lab::PhoneBook::Record_t record)
{
  stream << record.phone << " " << record.name;
  return stream;
}
