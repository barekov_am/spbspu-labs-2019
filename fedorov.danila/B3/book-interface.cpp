#include "book-interface.hpp"

#include <iostream>
#include <unordered_map>
#include <vector>
#include <functional>
#include <iterator>
#include <string>
#include <stdexcept>

#include "phone-book.hpp"
#include "manipulators.hpp"


lab::BookInterface::BookInterface()
{
  BookMark mark{ "current", book_.end() };
  bookMarks_.push_back(mark);
}

void lab::BookInterface::processBookCommand(std::istream& inputStream, std::ostream& outputStream)
{
  auto func = CommandParsing::parseCommand(inputStream);

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::runtime_error("Stream reading failed");
  }

  func(this, outputStream);

  if (outputStream.fail())
  {
    throw std::runtime_error("Stream writing failed");
  }
}

void lab::BookInterface::executeAddCommand(std::ostream&, const std::string& number, const std::string& name)
{
  bool reset = book_.empty();

  book_.pushBack({ number, name });

  if (reset)
  {
    resetMarks();
  }
}

void lab::BookInterface::executeStoreCommand(std::ostream& outputStream, const std::string& markName, const std::string& newMarkName)
{
  auto bookMark = findBookMark(markName);

  if (bookMark == bookMarks_.end())
  {
    invalidBookMark(outputStream);
    return;
  }

  auto newBookMark = findBookMark(newMarkName);

  if (newBookMark != bookMarks_.end())
  {
    bookMarks_.erase(newBookMark);
  }

  bookMarks_.push_back({ newMarkName, bookMark->mark });
}

void lab::BookInterface::executeInsertCommand(std::ostream& outputStream, const std::string& position, const std::string& markName,
    const std::string& number, const std::string& name)
{
  typedef std::function< PhoneBook::iterator(PhoneBook::iterator) > function;

  function func;

  if (position == "before")
  {
    func = std::bind(&lab::PhoneBook::insertBefore, std::ref(book_), std::placeholders::_1, PhoneBook::Record_t{ number, name });
  }
  else if (position == "after")
  {
    if (book_.empty())
    {
      func = std::bind(&lab::PhoneBook::insertBefore, std::ref(book_), std::placeholders::_1, PhoneBook::Record_t{ number, name });
    }
    else
    {
      func = std::bind(&lab::PhoneBook::insertAfter, std::ref(book_), std::placeholders::_1, PhoneBook::Record_t{ number, name });
    }
  }
  else
  {
    failedCommand(outputStream);
    return;
  }

  auto bookMark = findBookMark(markName);

  if (bookMark == bookMarks_.end())
  {
    invalidBookMark(outputStream);
    return;
  }

  
  func(bookMark->mark);
  resetMarks();
}

void lab::BookInterface::executeDeleteCommand(std::ostream& outputStream, const std::string& markName)
{
  if (book_.empty())
  {
    failedCommand(outputStream);
    return;
  }

  auto bookMark = findBookMark(markName);

  if (bookMark == bookMarks_.end())
  {
    invalidBookMark(outputStream);
    return;
  }

  auto markForErase = bookMark->mark;

  for (auto i = bookMarks_.begin(); i != bookMarks_.end(); ++i)
  {
    if (i->mark == markForErase)
    {
      ++(i->mark);
    }
  }

  book_.erase(markForErase);

  resetMarks();
}

void lab::BookInterface::executeShowCommand(std::ostream& outputStream, const std::string& markName)
{
  auto bookMark = findBookMark(markName);

  if (bookMark == bookMarks_.end())
  {
    invalidBookMark(outputStream);
    return;
  }

  if (book_.empty())
  {
    emptyBook(outputStream);
    return;
  }

  outputStream << *(bookMark->mark) << "\n";
}

void lab::BookInterface::executeMoveCommand(std::ostream& outputStream, const std::string& markName, const std::string& stepString)
{
  typedef std::function< PhoneBook::iterator() > function;

  function func = nullptr;

  int step = 0;

  if (stepString == "first")
  {
    func = std::bind(&lab::PhoneBook::begin, std::ref(book_));
  }
  else if (stepString == "last")
  {
    func = std::bind([](PhoneBook& book) { return book.empty() ? book.end() : --book.end(); }, std::ref(book_));
  }
  else
  {
    try
    {
      size_t index;
      step = std::stoi(stepString, &index);

      if (index != stepString.size())
      {
        invalidStep(outputStream);
        return;
      }
    }
    catch (const std::invalid_argument&)
    {
      invalidStep(outputStream);
      return;
    }
  }

  auto bookMark = findBookMark(markName);

  if (bookMark == bookMarks_.end())
  {
    invalidBookMark(outputStream);
    return;
  }

  if (func != nullptr)
  {
    bookMark->mark = func();
    return;
  }
  
  auto distance = step > 0 ? std::distance(bookMark->mark, book_.end()) : std::distance(book_.begin(), bookMark->mark);

  if (distance < std::abs(step))
  {
    bookMark->mark = step > 0 ? book_.end() : book_.begin();
  }
  else
  {
    std::advance(bookMark->mark, step);
  }
  
  if (!book_.empty() && bookMark->mark == book_.end())
  {
    --(bookMark->mark);
  }
}

void lab::BookInterface::failedCommand(std::ostream& outputStream) const
{
  outputStream << "<INVALID COMMAND>\n";
}

void lab::BookInterface::invalidBookMark(std::ostream& outputStream) const
{
  outputStream << "<INVALID BOOKMARK>\n";
}

void lab::BookInterface::emptyBook(std::ostream& outputStream) const
{
  outputStream << "<EMPTY>\n";
}

void lab::BookInterface::invalidStep(std::ostream& outputStream) const
{
  outputStream << "<INVALID STEP>\n";
}

lab::BookInterface::iterator lab::BookInterface::findBookMark(const std::string& markName)
{
  for (auto i = bookMarks_.begin(); i != bookMarks_.end(); ++i)
  {
    if (i->name == markName)
    {
      return i;
    }
  }

  return bookMarks_.end();
}

void lab::BookInterface::resetMarks()
{
  if (!book_.empty())
  {
    for (auto i = bookMarks_.begin(); i != bookMarks_.end(); ++i)
    {
      if (i->mark == book_.end())
      {
        --(i->mark);
      }
    }
  }
}
