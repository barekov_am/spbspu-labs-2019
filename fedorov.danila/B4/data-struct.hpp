#ifndef DATA_STRUCT_HPP
#define DATA_STRUCT_HPP

#include <iostream>

namespace lab
{
  struct DataStruct
  {
    int key1;
    int key2;
    std::string str;

    DataStruct() = default;
    DataStruct(int key1, int key2, std::string str);
  };

  std::istream& operator>>(std::istream& stream, DataStruct& data);
  std::ostream& operator<<(std::ostream& stream, const DataStruct& data);
}

#endif // !DATA_STRUCT_HPP
