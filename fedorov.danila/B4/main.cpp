#include <vector>
#include <algorithm>
#include <iterator>
#include <exception>

#include "data-struct.hpp"

bool compare(const lab::DataStruct& lhs, const lab::DataStruct& rhs);

int main()
{
  std::vector< lab::DataStruct > vector;
  try
  {
    typedef std::istream_iterator< lab::DataStruct > inputIterator;
    vector = std::vector< lab::DataStruct >(inputIterator(std::cin), inputIterator());
  }
  catch (const std::invalid_argument& e)
  {
    std::cerr << e.what();
    return 1;
  }

  if (!std::cin.eof())
  {
    std::cerr << "Stream reading failed\n";
    return 2;
  }

  std::sort(vector.begin(), vector.end(), compare);
  std::copy(vector.begin(), vector.end(), std::ostream_iterator< lab::DataStruct >(std::cout, "\n"));

  return 0;
}

bool compare(const lab::DataStruct& lhs, const lab::DataStruct& rhs)
{
  if (lhs.key1 != rhs.key1)
  {
    return lhs.key1 < rhs.key1;
  }

  if (lhs.key2 != rhs.key2)
  {
    return lhs.key2 < rhs.key2;
  }

  return lhs.str.size() < rhs.str.size();
}

