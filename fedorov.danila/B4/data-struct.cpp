#include "data-struct.hpp"

#include <iostream>
#include <string>
#include <cmath>
#include <exception>

#include "manipulators.hpp"
#include "stream-state-saver.hpp"

lab::DataStruct::DataStruct(int key1, int key2, std::string str) : key1(key1), key2(key2), str(str)
{
  if (std::abs(key1) > 5 || std::abs(key2) > 5 || str.size() == 0)
  {
    throw std::invalid_argument("Invalid DataStruct parameters");
  }
}

std::istream& lab::operator>>(std::istream& stream, DataStruct& data)
{
  if (stream.fail())
  {
    return stream;
  }

  lab::StreamStateSaver safe(stream);

  int key1 = 0, key2 = 0;
  stream >> std::noskipws >> std::ws >> key1;

  if (stream.fail())
  {
    return stream;
  }

  stream >> lab::skipDelimiter(',') >> lab::myws >> key2 >> lab::skipDelimiter(',') >> lab::myws;

  if (stream.fail())
  {
    if (stream.eof())
    {
      stream.clear();
      stream.setstate(std::ios_base::failbit);
    }
    return stream;
  }

  std::string str;
  std::getline(stream, str);
  
  if (str.empty())
  {
    stream.setstate(std::ios_base::failbit);
    return stream;
  }

  data = { key1, key2, str };

  return stream;
}

std::ostream& lab::operator<<(std::ostream& stream, const DataStruct& data)
{
  stream << data.key1 << "," << data.key2 << "," << data.str;
  return stream;
}
