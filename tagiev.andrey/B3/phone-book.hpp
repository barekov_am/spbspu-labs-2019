#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <list>
#include <functional>

class PhoneBook
{
public:
  struct record_t
  {
    std::string name;
    std::string number;
  };
  using container = std::list<record_t>;
  using iterator = container::iterator;

  void showCurrentRecord(iterator iter) const;
  iterator begin() noexcept;
  iterator end() noexcept;
  iterator insert(iterator iter, const record_t &record);
  iterator remove(iterator iter);
  iterator next(iterator iter);
  iterator prev(iterator iter);
  iterator replaceCurrentRecord(iterator iter, const record_t &record);
  void pushBack(const record_t &record);
  iterator move(iterator iter, int num);
  bool empty() const noexcept;
private:
  container book_;
};


#endif
