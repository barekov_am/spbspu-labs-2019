#include <iostream>
#include <sstream>
#include <cstring>
#include <functional>
#include <algorithm>

#include "commands.hpp"
#include "bookmarksManager.hpp"

struct command_t
{
  const char *name;
  std::function<void(BookmarksManager &, std::stringstream &)> execute;
};

void task1()
{
  BookmarksManager manager;
  std::string line;

  while (getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input failed");
    }

    std::stringstream string(line);
    std::string command;
    string >> command;

    command_t commands[] =
    {
      {"add", &add},
      {"store", &store},
      {"insert", &insert},
      {"delete", &deleteBookmark},
      {"show", &show},
      {"move", &move}
    };
    auto predicate = [&](const command_t &com) { return com.name == command; };
    auto action = std::find_if(std::begin(commands), std::end(commands), predicate);

    if (action != std::end(commands))
    {
      action->execute(manager, string);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
      return;
    }
  }
}
