#ifndef BOOKMARKSMANAGER_HPP
#define BOOKMARKSMANAGER_HPP

#include <map>
#include "phone-book.hpp"

class BookmarksManager
{
public:
  enum class movePosition
  {
    first,
    last
  };
  enum class insertPosition
  {
    before,
    after
  };
  BookmarksManager();

  void add(const PhoneBook::record_t &record);
  void nextRecord(const std::string &bookmark);
  void prevRecord(const std::string &bookmark);
  void store(const std::string &bookmark, const std::string &name);
  void show(const std::string &bookmark);
  void insert(const std::string &bookmark, const PhoneBook::record_t &record, insertPosition point);
  void remove(const std::string &bookmark);
  void move(const std::string &bookmark, movePosition position);
  void move(const std::string &bookmark, int n);

private:
  using bookmarkType = std::map<std::string, PhoneBook::iterator>;
  using bookmarkIterator = bookmarkType::iterator;

  PhoneBook records_;
  bookmarkType bookmarks_;

  bookmarkIterator getBookmarkIterator(const std::string &bookmark);
};

#endif
