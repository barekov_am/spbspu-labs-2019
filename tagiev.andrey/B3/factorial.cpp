#include "factorial.hpp"

const std::size_t MIN = 1;
const std::size_t MAX = 11;


Factorial::FactorialIterator::FactorialIterator() :
  FactorialIterator(1)
{
}

Factorial::FactorialIterator::FactorialIterator(std::size_t index) :
index_(index)
{
  if (index < MIN || index > MAX)
  {
      throw std::invalid_argument("Index is out of range");
  }
}

std::size_t Factorial::FactorialIterator::getFactorial(std::size_t number) const
{
  std::size_t result = 1;
  for (std::size_t i = 1; i <= number; ++i)
  {
    result *= i;
  }
  return result;
}

std::size_t Factorial::FactorialIterator::operator ->() const
{
  return getFactorial(index_);
}

std::size_t Factorial::FactorialIterator::operator *() const
{
  return getFactorial(index_);
}

bool Factorial::FactorialIterator::operator ==(const Factorial::FactorialIterator &rhs) const
{
  return (index_ == rhs.index_);
}

bool Factorial::FactorialIterator::operator !=(const Factorial::FactorialIterator &rhs) const
{
  return (index_ != rhs.index_);
}

Factorial::FactorialIterator &Factorial::FactorialIterator::operator --()
{
  if (index_ <= MIN)
  {
    throw std::out_of_range("Index is out of range");
  }
  --index_;
  return *this;
}

Factorial::FactorialIterator Factorial::FactorialIterator::operator --(int)
{
  Factorial::FactorialIterator tmp = *this;
  --(*this);
  return tmp;
}

Factorial::FactorialIterator &Factorial::FactorialIterator::operator ++()
{
  if (index_ > MAX) {
    throw std::out_of_range("Index is out of range");
  }
  ++index_;
  return *this;
}

Factorial::FactorialIterator Factorial::FactorialIterator::operator ++(int)
{
  Factorial::FactorialIterator tmp = *this;
  ++(*this);
  return tmp;
}

Factorial::FactorialIterator Factorial::begin()
{
  return FactorialIterator(MIN);
}

Factorial::FactorialIterator Factorial::end()
{
  return FactorialIterator(MAX);
}
