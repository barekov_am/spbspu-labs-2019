#include "commands.hpp"

#include <iostream>
#include <sstream>
#include <cstring>
#include <cctype>
#include <algorithm>

#include "bookmarksManager.hpp"

std::string getNumber(std::string &num)
{
  if (num.empty())
  {
    return "";
  }
  for (size_t i = 0; i < num.size(); i++)
  {
    if (!std::isdigit(num[i]))
    {
      return "";
    }
  }
  return num;
}

std::string getName(std::string &name)
{
  if (name.empty())
  {
    return "";
  }
  if (name.front() != '\"')
  {
    return "";
  }
  name.erase(name.begin());

  size_t i = 0;
  while (i < name.size() && name[i] != '\"')
  {
    if (name[i] == '\\')
    {
      if (name[i + 1] == '\"' && i + 2 < name.size())
      {
        name.erase(i, 1);
      }
      else
      {
        return "";
      }
    }
    i++;
  }

  if (i == name.size())
  {
    return "";
  }

  name.erase(i);

  if (name.empty())
  {
    return "";
  }
  return name;
}

std::string getMarkName(std::string &name)
{
  for (size_t i = 0; i < name.size(); i++)
  {
    if (isalnum(name[i]) || (name[i] != '-'))
    {
      return name;
    }
  }
  return "";
}

void add(BookmarksManager &manager, std::stringstream &ss)
{
  std::string number;
  ss >> std::ws >> number;
  number = getNumber(number);
  std::string name;
  std::getline(ss >> std::ws, name);
  name = getName(name);

  if (name.empty() || number.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  manager.add({ number, name });
}

void store(BookmarksManager &manager, std::stringstream &ss)
{
  std::string markName;
  ss >> std::ws >> markName;
  markName = getMarkName(markName);
  std::string newMarkName;
  ss >> std::ws >> newMarkName;
  newMarkName = getMarkName(newMarkName);

  if (markName.empty() || newMarkName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  manager.store(markName, newMarkName);
}

void insert(BookmarksManager &manager, std::stringstream &ss)
{
  struct insertPosition_t
  {
    const char *name;
    BookmarksManager::insertPosition position;
  };

  std::string positionData;
  ss >> std::ws >> positionData;

  insertPosition_t positions[] =
  {
    { "before", BookmarksManager::insertPosition::before },
    { "after", BookmarksManager::insertPosition::after }
  };

  auto predicate = [&](const insertPosition_t &pos) { return pos.name == positionData; };
  auto position = std::find_if(std::begin(positions), std::end(positions), predicate);

  if (position == std::end(positions))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::string markName;
  ss >> std::ws >> markName;
  markName = getMarkName(markName);
  std::string number;
  ss >> std::ws >> number;
  number = getNumber(number);
  std::string name;
  std::getline(ss >> std::ws, name);
  name = getName(name);

  if (markName.empty() || number.empty() || name.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  manager.insert(markName, { number, name }, position->position);
}

void deleteBookmark(BookmarksManager &manager, std::stringstream &ss)
{
  std::string markName;
  ss >> std::ws >> markName;
  markName = getMarkName(markName);
  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  manager.remove(markName);
}

void show(BookmarksManager &manager, std::stringstream &ss)
{
  std::string markName;
  ss >> std::ws >> markName;
  markName = getMarkName(markName);
  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  manager.show(markName);
}

void move(BookmarksManager &manager, std::stringstream &ss)
{
  struct movePosition_t
  {
    const char *name;
    BookmarksManager::movePosition position;
  };

  std::string markName;
  ss >> std::ws >> markName;
  markName = getMarkName(markName);
  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  std::string steps;
  ss >> std::ws >> steps;

  movePosition_t positions[] =
  {
    { "first", BookmarksManager::movePosition::first },
    { "last", BookmarksManager::movePosition::last }
  };

  auto predicate = [&](const movePosition_t &pos) { return pos.name == steps; };
  auto position = std::find_if(std::begin(positions), std::end(positions), predicate);

  int sign = 1;
  if (position == std::end(positions))
  {
    if (steps.front() == '-')
    {
      sign = -1;
      steps.erase(steps.begin());
    }
    else if (steps.front() == '+')
    {
      steps.erase(steps.begin());
    }

    steps = getNumber(steps);
    if (steps.empty())
    {
      std::cout << "<INVALID STEP>\n";
      return;
    }
    manager.move(markName, std::stoi(steps) * sign);
  }
  else
  {
    manager.move(markName, position->position);
  }
}
