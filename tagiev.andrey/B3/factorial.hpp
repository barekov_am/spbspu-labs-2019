#ifndef B3_FACTORIAL
#define B3_FACTORIAL

#include <iterator>

class Factorial
{
  public:
    class FactorialIterator : public std::iterator<std::bidirectional_iterator_tag, std::size_t>
    {
      public:
      FactorialIterator();
      FactorialIterator(std::size_t);

      std::size_t getFactorial(std::size_t) const;

      std::size_t operator ->() const;
      std::size_t operator *() const;

      bool operator ==(const FactorialIterator &rhs) const;
      bool operator !=(const FactorialIterator &rhs) const;
      
      FactorialIterator &operator --();
      FactorialIterator operator --(int);

      FactorialIterator &operator ++();
      FactorialIterator operator ++(int);

    private:
      std::size_t index_;
    };

    Factorial() = default;
    ~Factorial() = default;
    FactorialIterator begin();
    FactorialIterator end();
};

#endif
