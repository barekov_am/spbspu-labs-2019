#include <iostream>
#include <algorithm>
#include "tasks.hpp"
#include "factorial.hpp"

void task2()
{
  Factorial factorial;

  std::copy(factorial.begin(), factorial.end(), std::ostream_iterator<std::size_t>(std::cout, " "));
  std::cout << '\n';

  std::reverse_copy(factorial.begin(), factorial.end(), std::ostream_iterator<std::size_t>(std::cout, " "));
  std::cout << '\n';

  return;
}
