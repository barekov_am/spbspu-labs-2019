#ifndef CONTAINER
#define CONTAINER

#include <cstddef>

class ContainerStat
{
public:
  ContainerStat() noexcept;
  void operator()(int value);
  void printStat();

private:
  int max_;
  int min_;
  std::size_t positive_;
  std::size_t negative_;
  long long int oddSum_;
  long long int evenSum_;
  int first_;
  std::size_t countOfValue_;
  bool equals_;
};

#endif
