#include <iostream>
#include <list>
#include <string>
#include <algorithm>

#include "container.hpp"

int main()
{
  try
  {
    ContainerStat stat;
    std::list<int> container;
    std::string line;

    while (std::cin >> line)
    {
      if (std::cin.fail())
      {
        std::cerr << "Reading failed\n";
        return 1;
      }

      char *end;
      int value = std::strtol(line.c_str(), & end, 10);
      if (* end != '\0')
      {
        std::cerr << "Invalid input\n";
        return 1;
      }

      container.push_back(value);
    }

    stat = std::for_each(container.begin(), container.end(), stat);

    stat.printStat();

  }
  catch (std::exception & err)
  {
    std::cerr << err.what() << '\n';
  }

}
