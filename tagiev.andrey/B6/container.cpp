#include "container.hpp"
#include <limits>
#include <iostream>

ContainerStat::ContainerStat() noexcept :
  max_(std::numeric_limits<int>::min()),
  min_(std::numeric_limits<int>::max()),
  positive_(0),
  negative_(0),
  oddSum_(0),
  evenSum_(0),
  first_(0),
  countOfValue_(0),
  equals_(false)
{
}

void ContainerStat::operator()(int value)
{
  if (countOfValue_ == 0)
  {
    first_ = value;
  }

  countOfValue_++;
  max_ = std::max(value, max_);
  min_ = std::min(value, min_);
  
  if (value > 0)
  {
    positive_++;
  }
  else if (value < 0)
  {
    negative_++;
  }
  if (first_ == value)
  {
    equals_ = true;
  }
  else
  {
    equals_ = false;
  }

  if ((value % 2) == 0)
  {
    evenSum_ += value;
  }
  else
  {
    oddSum_ += value;
  }
}

void ContainerStat::printStat()
{
  if (countOfValue_ < 1)
  {
    std::cout << "No Data\n";
    return;
  }

  std::cout << "Max: " << max_ << '\n';
  std::cout << "Min: " << min_ << '\n';
  std::cout << "Mean: " << double(static_cast<double>(oddSum_ + evenSum_) / static_cast<double>(countOfValue_)) << '\n';
  std::cout << "Positive: " << positive_ << '\n';
  std::cout << "Negative: " << negative_ << '\n';
  std::cout << "Odd Sum: " << oddSum_ << '\n';
  std::cout << "Even Sum: " << evenSum_ << '\n';
  std::cout << "First/Last Equal: " << (equals_ ? "yes" : "no") << '\n';
}
