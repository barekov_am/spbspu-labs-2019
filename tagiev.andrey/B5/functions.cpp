#include <string>
#include <iostream>
#include <exception>
#include <cmath>
#include "shape.hpp"

Shape getPoints(std::string line, std::size_t vertices)
{
  Shape shape;
  for (std::size_t i = 0; i < vertices; i++)
  {
    if (line.empty())
    {
      throw std::invalid_argument("Invalid number of vertices");
    }

    while (line.find_first_of(" \t") == 0)
    {
      line.erase(0, 1);
    }

    std::size_t posOfOpenBracket = line.find_first_of('(');
    std::size_t posOfSemicolon = line.find_first_of(';');
    std::size_t posOfCloseBracket = line.find_first_of(')');

    if ((posOfOpenBracket == std::string::npos) || (posOfSemicolon == std::string::npos) || (posOfCloseBracket == std::string::npos))
    {
      throw std::invalid_argument("Invalid point declaration");
    }

    Point_t point
        {
            std::stoi(line.substr(posOfOpenBracket + 1, posOfSemicolon - posOfOpenBracket - 1)),
            std::stoi(line.substr(posOfSemicolon + 1, posOfCloseBracket - posOfSemicolon -1))
        };
    line.erase(0, posOfCloseBracket + 1);
    shape.push_back(point);
  }

  while (line.find_first_of(" \t") == 0)
  {
    line.erase(0, 1);
  }
  if (!line.empty())
  {
    throw std::invalid_argument("Too much points");
  }
  return shape;
}

int getDistance(const Point_t & lhs, const Point_t & rhs)
{
  return (rhs.x - lhs.x)*(rhs.x - lhs.x) + (rhs.y - lhs.y)*(rhs.y - lhs.y);
}

bool isRectangle(const Shape & shape)
{
  int d1 = getDistance(shape[0], shape[1]);
  int d2 = getDistance(shape[1], shape[2]);
  int d3 = getDistance(shape[2], shape[3]);
  int d4 = getDistance(shape[3], shape[0]);
  int l1 = getDistance(shape[0], shape[2]);
  int l2 = getDistance(shape[1], shape[3]);
  return (l1 == l2) && (d1 == d3) && (d2 == d4);
}

bool isSquare(const Shape & shape)
{
  if (isRectangle(shape))
  {
    int d1 = getDistance(shape[0], shape[1]);
    int d2 = getDistance(shape[1], shape[2]);
    int d3 = getDistance(shape[2], shape[3]);
    int d4 = getDistance(shape[3], shape[0]);
    return (d1 == d2) && (d2 == d3) && (d3 == d4);
  }
  return false;
}
