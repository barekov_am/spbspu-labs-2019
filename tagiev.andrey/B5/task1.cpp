#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

void task1()
{
  std::string line;
  std::vector<std::string> vector;
  std::size_t pos = std::string::npos;

  while(std::getline(std::cin, line))
  {
    if(std::cin.fail())
    {
      throw std::ios_base::failure("Reading failed");
    }

    while(line.find_first_of(" \t\n") == 0)
    {
      line.erase(0, 1);
    }

    while(!line.empty())
    {
      pos = line.find_first_of(" \t\n");
      vector.push_back(line.substr(0, pos));

      if (pos != std::string::npos)
      {
        line.erase(0, pos + 1);
      }
      else
      {
        line.clear();
      }

      while (line.find_first_of(" \t\n") == 0)
      {
        line.erase(0, 1);
      }
    }
  }
  std::sort(vector.begin(), vector.end());

  vector.erase(std::unique(vector.begin(), vector.end()), vector.end());

  for(auto & i : vector)
  {
    std::cout << i << '\n';
  }
}
