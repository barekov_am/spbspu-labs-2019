#include <iostream>
#include <cassert>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separation.hpp"


void printInfo(const tagiev::Shape *shape)
{
  assert(shape != nullptr);

  const tagiev::rectangle_t frame = shape->getFrameRect();

  std::cout << "Area of shape: " << shape->getArea() << '\n';
  std::cout << "Width of frame: " << frame.width << '\n';
  std::cout << "Height of frame: " << frame.height << '\n';
  std::cout << "Center: " << frame.pos.x << ", " << frame.pos.y << "\n\n";
}

void printMatrix(const tagiev::Matrix<tagiev::Shape> matrix)
{
  int layer = matrix.getRows();
  for (int i = 0; i < layer; i++)
  {
    std::cout << "Layer: " << i + 1 << "\n";
    std::cout << "Shapes in the layer:\n";
    int columns = matrix[i].size();
    for (int j = 0; j < columns; j++)
    {
      if (matrix[i][j] != nullptr)
      {
        std::cout << j + 1 << ":\n";
        std::cout << "Area: " << matrix[i][j]->getArea() << "\n";
        const tagiev::rectangle_t frameRect = matrix[i][j]->getFrameRect();
        std::cout << "Position: (" << frameRect.pos.x << "," << frameRect.pos.y << ")\n";
      }
    }
   }
}


int main()
{
  tagiev::Rectangle rectangle({123.0, 100.0}, 5.0, 3.0, 0);

  printInfo(&rectangle);
  const tagiev::point_t newCenter1 = {1.0, 2.0};
  rectangle.move(newCenter1);
  rectangle.move(10.0, 20.0);
  rectangle.scale(2.0);
  rectangle.rotate(90);
  printInfo(&rectangle);

  tagiev::Circle circle({11.1, 12.2}, 4.4);

  printInfo(&circle);
  const tagiev::point_t newCenter2 = {300.0, 400.0};
  circle.move(newCenter2);
  circle.move(1.0, 2.0);
  circle.scale(2.0);
  circle.rotate(123);
  printInfo(&circle);

  tagiev::Triangle triangle({0.0, 0.0}, {0.0, 4.0}, {3.0, 4.0});

  printInfo(&triangle);
  const tagiev::point_t newCenter3 = {100.0, 200.0};
  triangle.move(newCenter3);
  triangle.move(0.1, 0.2);
  triangle.scale(2.0);
  triangle.rotate(30);
  printInfo(&triangle);

  tagiev::CompositeShape comShape(std::make_shared<tagiev::Triangle>(triangle));

  comShape.add(std::make_shared<tagiev::Rectangle>(rectangle));
  printInfo(&comShape);
  comShape.scale(2.0);
  comShape.move(1.0, 2.0);
  comShape.remove(2);
  comShape.rotate(90);
  printInfo(&comShape);

  tagiev::Matrix<tagiev::Shape> matrix = tagiev::section(comShape);

  printMatrix(matrix);
  comShape.add(std::make_shared<tagiev::Circle>(circle));
  matrix = tagiev::section(comShape);
  printMatrix(matrix);

  return 0;
}
