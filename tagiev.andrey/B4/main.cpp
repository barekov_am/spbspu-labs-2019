#include <exception>
#include <iostream>
#include <string>
#include <list>
#include "data_struct.hpp"

DataStruct getDataStruct(std::string & line);
bool compareDataStruct(const DataStruct & lhs, const DataStruct & rhs);

int main(int , char *  [])
{
  try
  {
    std::string line;
    std::list< DataStruct > list;
    
    while(getline(std::cin, line))
    {
      if (std::cin.fail())
      {
        std::cerr << "Reading failed" << '\n';
        return 1;
      }

      DataStruct dataStruct = getDataStruct(line);
      list.push_back(dataStruct);
    }

    if(!list.empty())
    {
      list.sort(compareDataStruct);
    }

    for(DataStruct &i : list)
    {
      std::cout << i.key1 << ", " << i.key2 << ", " << i.str << '\n';
    }
  }
  catch (std::exception & err)
  {
    std::cerr << err.what() << '\n';
    return 1;
  }
  return 0;
}
