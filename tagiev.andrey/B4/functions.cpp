
#include <string>
#include <stdexcept>
#include "data_struct.hpp"

const int MIN = -5;
const int MAX = 5;

DataStruct getDataStruct(std::string & line)
{
  if (line.empty())
  {
    throw std::invalid_argument("Empty input");
  }

  std::size_t pos = line.find_first_of(',');
  int key1 = std::stoi(line.substr(0, pos));

  line.erase(0, pos + 1);
  while (line.find_first_of(" \t") == 0)
  {
    line.erase(0, 1);
  }

  pos = line.find_first_of(',');
  if (pos == std::string::npos)
  {
    throw std::invalid_argument("Invalid input");
  }
  int key2 = std::stoi(line.substr(0, pos));

  line.erase(0, pos + 1);
  while (line.find_first_of(" \t") == 0)
  {
    line.erase(0, 1);
  }

  if ((key1 < MIN) || (key1 > MAX) || (key2 < MIN) || (key2 > MAX) || line.empty())
  {
    throw std::invalid_argument("Invalid input");
  }

  return {key1, key2, line};
}

bool compareDataStruct(const DataStruct & lhs, const DataStruct & rhs)
{
  if (lhs.key1 < rhs.key1)
  {
    return true;
  }
  if (lhs.key1 == rhs.key1)
  {
    if (lhs.key2 < rhs.key2)
    {
      return true;
    }
    if (lhs.key2 == rhs.key2)
    {
      if (rhs.str.length() > lhs.str.length())
      {
        return true;
      }
    }
  }
  return false;
}
