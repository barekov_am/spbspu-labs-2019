#ifndef B7_SHAPES
#define B7_SHAPES

struct Point
{
  int x, y;
};

class Shape
{
public:
  Shape(double x, double y);
  virtual ~Shape() = default;

  bool isMoreLeft(const Shape * shape) const;
  bool isUpper(const Shape * shape) const;
  virtual void draw() = 0;
protected:
  Point center_;
};

class Circle : public Shape
{
public:
  Circle(double x, double y);
  void draw() override;
};

class Triangle : public Shape
{
public:
  Triangle(double x, double y);
  void draw() override;
};

class Square : public Shape
{
public:
  Square(double x, double y);
  void draw() override;
};

#endif
