#include <iostream>
#include <string>
#include <memory>
#include <vector>

#include "shapes.hpp"

void printVector(std::string typeOfSort, const std::vector<std::shared_ptr<Shape>> & vector)
{
  std::cout << typeOfSort <<":\n";
  for (const auto & shape : vector)
  {
    shape->draw();
  }
}

std::shared_ptr<Shape> readShape(std::string & line)
{
  auto posE = line.find_first_of('E');
  if (posE == std::string::npos)
  {
    throw std::invalid_argument("Wrong shape!\n");
  }
    
  auto posS = line.find_first_of('(');
  auto posX = line.find_first_of(';');
  auto posY = line.find_first_of(')');

  if ((posX == std::string::npos) || (posY == std::string::npos))
  {
    throw std::invalid_argument("Wrong center\n");
  }
  std::string shape = line.substr(0, posE + 1);
  int x = std::stoi(line.substr(posS+1, posX - 1));
  int y = std::stoi(line.substr(posX + 1, posY - posX - 1));

  if (shape == "CIRCLE")
  {
    return std::make_shared<Circle>(Circle(x, y));
  }
  else if (shape == "TRIANGLE")
  {
    return std::make_shared<Triangle>(Triangle(x, y));
  }
  else if (shape == "SQUARE")
  {
    return std::make_shared<Square>(Square(x, y));
  }
  else
  {
    throw std::invalid_argument("Wrong shape\n");
  }
}
