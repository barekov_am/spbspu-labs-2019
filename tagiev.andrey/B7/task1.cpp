#include <iostream>
#include <list>
#include <algorithm>
#include <cmath>

void task1()
{
    std::list<float> list;
    float in;
    while (std::cin >> in)
    {
        list.push_back(in);
    }

    if (!std::cin.eof() && std::cin.fail())
        throw std::ios::failure("Failed reading input stream");

    std::for_each(list.begin(), list.end(),[](float &x){ x *= M_PI; });

    for (auto &elem: list)
        std::cout << elem << '\n';
}
