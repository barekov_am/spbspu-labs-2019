#include <memory>
#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include "shapes.hpp"

std::shared_ptr<Shape> readShape(std::string & line);
void printVector(std::string typeOfSort, const std::vector<std::shared_ptr<Shape>> & vector);

void task2()
{
  std::vector<std::shared_ptr<Shape>> shapes;
  std::string line;
  while (getline(std::cin >> std::ws, line))
  {
    std::shared_ptr<Shape> shape = readShape(line);
    shapes.push_back(shape);
  }
  printVector("Original", shapes);

  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape> & lhs, const std::shared_ptr<Shape> & rhs)
  {
    return lhs->isMoreLeft(rhs.get());
  });
  printVector("Left-Right", shapes);

  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape> & lhs, const std::shared_ptr<Shape> & rhs)
  {
    return !lhs->isMoreLeft(rhs.get());
  });
  printVector("Right-Left", shapes);

  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape> & lhs, const std::shared_ptr<Shape> & rhs)
  {
    return lhs->isUpper(rhs.get());
  });
  printVector("Top-Bottom", shapes);

  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape> & lhs, const std::shared_ptr<Shape> & rhs)
  {
    return !lhs->isUpper(rhs.get());
  });
  printVector("Bottom-Top", shapes);
}
