#include <stdexcept>
#include <iostream>

#include "shapes.hpp"

Shape::Shape(double x, double y)
{
    center_.x = x;
    center_.y = y;
}

bool Shape::isMoreLeft(const Shape * shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("No shape to compare\n");
  }
  return center_.x < shape->center_.x;
}

bool Shape::isUpper(const Shape * shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("No shape to compare\n");
  }
  return center_.y > shape->center_.y;
}

Circle::Circle(double x, double y) :
  Shape(x, y)
{}

void Circle::draw()
{
  std::cout << "CIRCLE (" << center_.x << ";" << center_.y << ")\n";
}

Triangle::Triangle(double x, double y) :
  Shape(x, y)
{}

void Triangle::draw()
{
  std::cout << "TRIANGLE (" << center_.x << ";" << center_.y << ")\n";
}

Square::Square(double x, double y) :
  Shape(x, y)
{}

void Square::draw()
{
  std::cout << "SQUARE (" << center_.x << ";" << center_.y << ")\n";
}

