#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

tagiev::Rectangle::Rectangle(const tagiev::point_t &center, const double &width, const double &height, const double &angle) :
  center_(center),
  width_(width),
  height_(height),
  angle_(angle)
{
  if ((height_ <= 0.0) || (width_ <= 0.0))
  {
    throw std::invalid_argument("W or H <= 0");
  }
}

double tagiev::Rectangle::getArea() const
{
  return (width_ * height_);
}

tagiev::rectangle_t tagiev::Rectangle::getFrameRect() const
{
  const double cos = std::abs(std::cos(angle_ * M_PI / 180));
  const double sin = std::abs(std::sin(angle_ * M_PI / 180));
  const double width = fabs(cos * width_) + fabs(sin * height_);
  const double height = fabs(cos * height_) + fabs(sin * width_);
  return {width, height, center_};
}

void tagiev::Rectangle::move(const double &dx, const double &dy)
{
  center_.x += dx;
  center_.y += dy;
}

void tagiev::Rectangle::move(const tagiev::point_t &newPoint)
{
  center_ = newPoint;
}

void tagiev::Rectangle::scale(double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("K <= 0");
  }

  width_ *= k;
  height_ *= k;
}

void tagiev::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
