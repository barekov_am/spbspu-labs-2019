#include "separation.hpp"
#include <cmath>

bool tagiev::intersect(const rectangle_t frame1, const rectangle_t frame2)
{
  if (((fabs(frame1.pos.x - frame2.pos.x)) > ((frame1.width + frame2.width) / 2))
        || ((fabs(frame1.pos.y - frame2.pos.y)) > ((frame1.height + frame2.height) / 2)))
  {
    return false;
  }
  return true;
}

tagiev::Matrix<tagiev::Shape> tagiev::section(const tagiev::CompositeShape comShape)
{
  tagiev::Matrix<tagiev::Shape> matrix;

  int count = comShape.getSize();
  for (int i = 0; i < count; i++)
  {
    int layer = 0;
    int matrixRows = matrix.getRows();
    for (int j = matrixRows; j-- > 0;)
    {
      int matrixColumns = matrix[j].size();
      for (int k = 0; k < matrixColumns; k++)
      {
        if (intersect(matrix[j][k]->getFrameRect(), comShape[i]->getFrameRect()))
        {
          layer = j + 1;
          break;
        }
      }

      if (layer > j)
      {
        break;
      }
    }

    matrix.add(comShape[i], layer);
  }

  return matrix;
}
