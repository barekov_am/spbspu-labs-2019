#include "composite-shape.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

tagiev::CompositeShape::CompositeShape():
  size_(0),
  shapes_(nullptr)
{
}

tagiev::CompositeShape::CompositeShape(tagiev::CompositeShape &&newShape):
  size_(newShape.size_),
  shapes_(std::move(newShape.shapes_))
{
  newShape.size_ = 0;
}

tagiev::CompositeShape::CompositeShape(const tagiev::CompositeShape &newShape):
  size_(newShape.size_),
  shapes_(std::make_unique<std::shared_ptr<tagiev::Shape> []>(newShape.size_))
{
  for (int i = 0; i < size_; i++)
  {
    shapes_[i] = newShape.shapes_[i];
  }
}

tagiev::CompositeShape::CompositeShape(const std::shared_ptr<tagiev::Shape> newShape):
  CompositeShape()
{
  add(newShape);
}

tagiev::CompositeShape &tagiev::CompositeShape::operator =(const tagiev::CompositeShape &newShape)
{
  if (&newShape != this)
  {
    size_ = newShape.size_;
    shapes_ = std::make_unique<std::shared_ptr<tagiev::Shape> []>(size_);
    for (int i = 0; i < size_; i++)
    {
      shapes_[i] = newShape.shapes_[i];
    }
  }
  return *this;
}

tagiev::CompositeShape &tagiev::CompositeShape::operator =(tagiev::CompositeShape &&newShape)
{
  if (&newShape != this)
  {
    size_ = newShape.size_;
    newShape.size_ = 0;
    shapes_ = std::move(newShape.shapes_);
  }
  return *this;
}

std::shared_ptr<tagiev::Shape> &tagiev::CompositeShape::operator [](const int index) const
{
  if ((index >= size_) || (index < 0))
  {
     throw std::out_of_range("Index > size or < 0");
  }

  return shapes_[index];
}

int tagiev::CompositeShape::getSize() const
{
  return size_;
}

void tagiev::CompositeShape::add(std::shared_ptr<tagiev::Shape> newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("It's not a shape");
  }

  size_++;
  std::unique_ptr<std::shared_ptr<tagiev::Shape> []> array(std::make_unique<std::shared_ptr<tagiev::Shape>[]>(size_));
  for (int i = 0; i < size_ - 1; i++)
  {
    array[i] = shapes_[i];
  }
  shapes_ = std::move(array);
  shapes_[size_ - 1] = newShape;
}


void tagiev::CompositeShape::remove(const int index)
{
  if ((index >= size_) || (index < 0))
  {
    throw std::out_of_range("Index > size or < 0");
  }

  size_--;
  std::unique_ptr<std::shared_ptr<tagiev::Shape> []> array(std::make_unique<std::shared_ptr<tagiev::Shape>[]>(size_));
  for (int i = 0; i < index; i++)
  {
    array[i] = shapes_[i];
  }
  for (int i = index + 1; i < size_; i++)
  {
    array[i - 1] = shapes_[i];
  }
  shapes_ = std::move(array);
}

double tagiev::CompositeShape::getArea() const
{
  if (size_ == 0)
  {
    throw std::logic_error("CompositeShape is empty");
  }

  double area = 0.0;
  for (int i = 0; i < size_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

tagiev::rectangle_t tagiev::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::logic_error("CompositeShape is empty");
  }

  tagiev::rectangle_t frameRect = shapes_[0]->getFrameRect();
  double minX = frameRect.pos.x - frameRect.width / 2;
  double maxX = frameRect.pos.x + frameRect.width / 2;
  double minY = frameRect.pos.y - frameRect.height / 2;
  double maxY = frameRect.pos.y + frameRect.height / 2;
  for (int i = 1; i < size_; i++)
  {
    frameRect = shapes_[i]->getFrameRect();
    minX = std::min(frameRect.pos.x - frameRect.width / 2, minX);
    maxX = std::max(frameRect.pos.x + frameRect.width / 2, maxX);
    minY = std::min(frameRect.pos.y - frameRect.height / 2, minY);
    maxY = std::max(frameRect.pos.y - frameRect.height / 2, maxY);
  }
  return {maxX - minX, maxY - minY, {(maxX + minX) / 2, (maxY + minY) / 2}};
}

void tagiev::CompositeShape::move(const double &dx, const double &dy)
{
  for (int i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}
void tagiev::CompositeShape::move(const tagiev::point_t &newPoint)
{
  tagiev::rectangle_t frameRect = getFrameRect();
  double dx = newPoint.x - frameRect.pos.x;
  double dy = newPoint.y - frameRect.pos.y;
  move(dx, dy);
}

void tagiev::CompositeShape::scale(double scale)
{
  if (scale <= 0.0)
  {
    throw std::invalid_argument("Scale value <= 0");
  }

  tagiev::point_t frameRectCenter = getFrameRect().pos;
  double newCenterX = 0.0;
  double newCenterY = 0.0;

  for (int i = 0; i < size_; i++)
  {
    newCenterX = shapes_[i]->getFrameRect().pos.x - frameRectCenter.x;
    newCenterY = shapes_[i]->getFrameRect().pos.y - frameRectCenter.y;
    shapes_[i]->move({newCenterX * scale + frameRectCenter.x, newCenterY * scale + frameRectCenter.y});
    shapes_[i]->scale(scale);
  }
}

void tagiev::CompositeShape::rotate(double angle)
{
  const double cos = std::abs(std::cos(angle * M_PI / 180));
  const double sin = std::abs(std::sin(angle * M_PI / 180));
  const tagiev::point_t center = getFrameRect().pos;

  for (int i = 0; i < size_; i++)
  {
    tagiev::point_t curCenter = shapes_[i]->getFrameRect().pos;
    const double dx = center.x + (curCenter.x - center.x) * cos - (curCenter.y - center.y) * sin;
    const double dy = center.y + (curCenter.x - center.x) * sin + (curCenter.y - center.y) * cos;
    shapes_[i]->move(dx, dy);
    shapes_[i]->rotate(angle);
  }
}
