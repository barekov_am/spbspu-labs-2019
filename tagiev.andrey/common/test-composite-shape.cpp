#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

// composite

BOOST_AUTO_TEST_SUITE(testsForComShape)

const double EPS = 0.01;

BOOST_AUTO_TEST_CASE(testOfImmutability)
{
  tagiev::Rectangle testRect({1.0, 2.0}, 3.0, 4.0, 0);
  tagiev::CompositeShape testComShape(std::make_shared<tagiev::Rectangle>(testRect));
  tagiev::Circle testCircle({10.0, 20.0}, 3.0);
  testComShape.add(std::make_shared<tagiev::Circle>(testCircle));

  const tagiev::rectangle_t frameRect = testComShape.getFrameRect();
  const double area = testComShape.getArea();

  const double width = frameRect.width;
  const double height = frameRect.height;

  testComShape.move(1.0, 2.0);
  BOOST_CHECK_CLOSE(testComShape.getFrameRect().width, width, EPS);
  BOOST_CHECK_CLOSE(testComShape.getFrameRect().height, height, EPS);
  BOOST_CHECK_CLOSE(testComShape.getArea(), area, EPS);

  testComShape.move({10.0, 20.0});
  BOOST_CHECK_CLOSE(testComShape.getFrameRect().width, width, EPS);
  BOOST_CHECK_CLOSE(testComShape.getFrameRect().height, height, EPS);
  BOOST_CHECK_CLOSE(testComShape.getArea(), area, EPS);
}
BOOST_AUTO_TEST_CASE(testOfCorrectScaling)
{
  tagiev::Rectangle testRect({1.0, 2.0}, 3.0, 4.0, 0);
  tagiev::CompositeShape testComShape(std::make_shared<tagiev::Rectangle>(testRect));
  tagiev::Circle testCircle({10.0, 20.0}, 3.0);
  testComShape.add(std::make_shared<tagiev::Circle>(testCircle));

  const double area = testComShape.getArea();
  const double scale = 5.0;
  testComShape.scale(scale);
  BOOST_CHECK_CLOSE(testComShape.getArea(), area * scale * scale, EPS);
}
BOOST_AUTO_TEST_CASE(testInvalidValues)
{
  tagiev::Rectangle testRect({1.0, 2.0}, 3.0, 4.0, 0);
  tagiev::CompositeShape testComShape(std::make_shared<tagiev::Rectangle>(testRect));
  tagiev::Circle testCircle({10.0, 20.0}, 3.0);
  testComShape.add(std::make_shared<tagiev::Circle>(testCircle));

  BOOST_CHECK_THROW(testComShape.scale(-10), std::invalid_argument);
  BOOST_CHECK_THROW(testComShape.remove(9), std::out_of_range);
  BOOST_CHECK_THROW(testComShape[10], std::out_of_range);

  tagiev::CompositeShape testComShape2(std::make_shared<tagiev::Rectangle>(testRect));
  testComShape2.remove(0);
  BOOST_CHECK_THROW(testComShape2.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(testComShape2.getArea(), std::logic_error);
}
BOOST_AUTO_TEST_CASE(testCopyConstructor)
{
  tagiev::Rectangle testRect({1.0, 2.0}, 3.0, 4.0, 0);
  tagiev::CompositeShape testComShape(std::make_shared<tagiev::Rectangle>(testRect));
  tagiev::Circle testCircle({10.0, 20.0}, 3.0);
  testComShape.add(std::make_shared<tagiev::Circle>(testCircle));
  const tagiev::rectangle_t frameRect = testComShape.getFrameRect();

  tagiev::CompositeShape copyComShape(testComShape);
  const tagiev::rectangle_t copyFrameRect = copyComShape.getFrameRect();

  BOOST_CHECK_CLOSE(testComShape.getArea(), copyComShape.getArea(), EPS);
  BOOST_CHECK_EQUAL(testComShape.getSize(), copyComShape.getSize());
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, EPS);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, EPS);
}
BOOST_AUTO_TEST_CASE(testMoveConstructor)
{
  tagiev::Rectangle testRect({1.0, 2.0}, 3.0, 4.0, 0);
  tagiev::CompositeShape testComShape(std::make_shared<tagiev::Rectangle>(testRect));
  tagiev::Circle testCircle({10.0, 20.0}, 3.0);
  testComShape.add(std::make_shared<tagiev::Circle>(testCircle));
  const tagiev::rectangle_t frameRect = testComShape.getFrameRect();
  const double compositeArea = testComShape.getArea();
  const int compositeSize = testComShape.getSize();

  tagiev::CompositeShape moveComShape(std::move(testComShape));
  const tagiev::rectangle_t moveFrameRect = moveComShape.getFrameRect();
  BOOST_CHECK_CLOSE(compositeArea, moveComShape.getArea(), EPS);
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, EPS);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, EPS);
  BOOST_CHECK_EQUAL(compositeSize, moveComShape.getSize());
  BOOST_CHECK_EQUAL(testComShape.getSize(), 0);

}
BOOST_AUTO_TEST_CASE(testMoveOperator)
{
  tagiev::Rectangle testRect({1.0, 2.0}, 3.0, 4.0, 0);
  tagiev::CompositeShape testComShape(std::make_shared<tagiev::Rectangle>(testRect));
  tagiev::Circle testCircle({10.0, 20.0}, 3.0);
  testComShape.add(std::make_shared<tagiev::Circle>(testCircle));
  const tagiev::rectangle_t frameRect = testComShape.getFrameRect();
  const double compositeArea = testComShape.getArea();
  const int compositeSize = testComShape.getSize();

  tagiev::Triangle testTriangle({0.0, 0.0}, {3.0, 0.0}, {3.0, 4.0});
  tagiev::CompositeShape moveComShape(std::make_shared<tagiev::Triangle>(testTriangle));
  moveComShape = std::move(testComShape);
  const tagiev::rectangle_t moveFrameRect = moveComShape.getFrameRect();
  BOOST_CHECK_CLOSE(compositeArea, moveComShape.getArea(), EPS);
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, EPS);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, EPS);
  BOOST_CHECK_EQUAL(compositeSize, moveComShape.getSize());
  BOOST_CHECK_EQUAL(testComShape.getSize(), 0);
}
BOOST_AUTO_TEST_CASE(testCopyOperator)
{
  tagiev::Rectangle testRect({1.0, 2.0}, 3.0, 4.0, 0);
  tagiev::CompositeShape testComShape(std::make_shared<tagiev::Rectangle>(testRect));
  tagiev::Circle testCircle({10.0, 20.0}, 3.0);
  testComShape.add(std::make_shared<tagiev::Circle>(testCircle));
  const tagiev::rectangle_t frameRect = testComShape.getFrameRect();

  tagiev::Triangle testTriangle({0.0, 0.0}, {3.0, 0.0}, {3.0, 4.0});
  tagiev::CompositeShape copyComShape(std::make_shared<tagiev::Triangle>(testTriangle));
  copyComShape = testComShape;
  const tagiev::rectangle_t copyFrameRect = copyComShape.getFrameRect();

  BOOST_CHECK_CLOSE(testComShape.getArea(), copyComShape.getArea(), EPS);
  BOOST_CHECK_EQUAL(testComShape.getSize(), copyComShape.getSize());
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, EPS);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, EPS);
}

BOOST_AUTO_TEST_CASE(testOfRotate)
{
  tagiev::Circle testCircle({1, 2}, 3);
  tagiev::Rectangle testRect({4, 5}, 6, 7, 0);
  tagiev::Triangle testTriangle({0.0, 0.0}, {4.0, 0.0}, {4.0, 3.0});
  tagiev::CompositeShape testComShape(std::make_shared<tagiev::Rectangle>(testRect));
  testComShape.add(std::make_shared<tagiev::Circle>(testCircle));
  testComShape.add(std::make_shared<tagiev::Triangle>(testTriangle));
  double area = testComShape.getArea();

  double angle = -90.0;
  BOOST_CHECK_NO_THROW(testComShape.rotate(angle));

  testCircle.rotate(45);
  BOOST_CHECK_CLOSE(testComShape.getArea(), area, EPS);
}

BOOST_AUTO_TEST_SUITE_END();
