#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "separation.hpp"
#include "matrix.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(testForMatrix)

  BOOST_AUTO_TEST_CASE(testOfConstructors)
  {
    tagiev::Rectangle testRect({1, 2}, 3, 4, 0);
    tagiev::Circle testCircle({1, 2}, 3);
    tagiev::Triangle testTriangle({1.0, 0.0}, {2.0, 0.0}, {3.0, 3.0});
    tagiev::CompositeShape testCom(std::make_shared<tagiev::Rectangle>(testRect));
    testCom.add(std::make_shared<tagiev::Circle>(testCircle));
    testCom.add(std::make_shared<tagiev::Triangle>(testTriangle));
    tagiev::Matrix<tagiev::Shape> testMatrix = tagiev::section(testCom);

    BOOST_CHECK_NO_THROW(tagiev::Matrix<tagiev::Shape> testMatrix1(testMatrix));
    BOOST_CHECK_NO_THROW(tagiev::Matrix<tagiev::Shape> testMatrix2(std::move(testMatrix)));
  }

  BOOST_AUTO_TEST_CASE(testsForOperators)
  {
    tagiev::Rectangle testRect({1, 2}, 3, 4, 0);
    tagiev::Circle testCircle({1, 2}, 3);
    tagiev::Triangle testTriangle({1.0, 0.0}, {2.0, 0.0}, {3.0, 3.0});
    tagiev::CompositeShape testCom(std::make_shared<tagiev::Rectangle>(testRect));
    testCom.add(std::make_shared<tagiev::Circle>(testCircle));
    tagiev::Matrix<tagiev::Shape> testMatrix = tagiev::section(testCom);

    BOOST_CHECK_THROW(testMatrix[5], std::out_of_range);
    BOOST_CHECK_NO_THROW(tagiev::Matrix<tagiev::Shape> testMatrix1 = testMatrix);
    BOOST_CHECK_NO_THROW(tagiev::Matrix<tagiev::Shape> testMatrix2 = std::move(testMatrix));

    tagiev::CompositeShape testCom1(std::make_shared<tagiev::Triangle>(testTriangle));
    tagiev::Matrix<tagiev::Shape> testMatrix2 = tagiev::section(testCom1);
    tagiev::Matrix<tagiev::Shape> testMatrix3 = tagiev::section(testCom1);

    BOOST_CHECK(testMatrix2 != testMatrix);
    BOOST_CHECK(testMatrix3 != testMatrix);
    BOOST_CHECK(testMatrix2 == testMatrix3);
  }

BOOST_AUTO_TEST_SUITE_END()
