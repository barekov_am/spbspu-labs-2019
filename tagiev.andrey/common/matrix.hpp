#ifndef MATRIX_H
#define MATRIX_H

#include <memory>

namespace tagiev
{
  template <typename T>
  class Matrix
  {
  public:
    using ptr = std::shared_ptr<T>;

    class Array
    {
    public:
      Array(const Array &);
      Array(Array &&) noexcept;
      Array(ptr *, int);
      ~Array() = default;

      Array &operator =(const Array &);
      Array &operator =(Array &&) noexcept;
      ptr operator [](int) const;

      int size() const;
      void swap(Array &) noexcept;
    private:
      int size_;
      std::unique_ptr<ptr []> array_;
    };


    Matrix();
    Matrix(const Matrix<T> &);
    Matrix(Matrix<T> &&) noexcept;
    ~Matrix() = default;
    Matrix &operator =(const Matrix<T> &);
    Matrix &operator =(Matrix<T> &&) noexcept;
    Array operator [](int) const;
    bool operator ==(const Matrix<T> &) const;
    bool operator !=(const Matrix<T> &) const;
    void add(ptr, int);
    int getRows() const;
    void swap(Matrix<T> &) noexcept;

  private:
    int count_;
    int rows_;
    std::unique_ptr<int []> columns_;
    std::unique_ptr<ptr []> data_;
  };

  template<typename T>
  tagiev::Matrix<T>::Matrix() :
    count_(0),
    rows_(0)
  {
  }

  template<typename T>
  tagiev::Matrix<T>::Matrix(const tagiev::Matrix<T> &newMatrix):
    count_(newMatrix.count_),
    rows_(newMatrix.rows_),
    columns_(std::make_unique<int []>(newMatrix.rows_)),
    data_(std::make_unique<ptr []>(newMatrix.count_))
  {
    for (int i = 0; i < rows_; i++)
    {
      columns_[i] = newMatrix.columns_[i];
    }

    for (int i = 0; i < count_; i++)
    {
      data_[i] = newMatrix.data_[i];
    }
  }

  template<typename T>
  tagiev::Matrix<T>::Matrix(tagiev::Matrix<T> &&newMatrix) noexcept :
    count_(newMatrix.count_),
    rows_(newMatrix.rows_),
    columns_(std::move(newMatrix.columns_)),
    data_(std::move(newMatrix.data_))
  {
    newMatrix.rows_ = 0;
    newMatrix.count_ = 0;
  }

  template<typename T>
  tagiev::Matrix<T> &tagiev::Matrix<T>::operator =(const tagiev::Matrix<T> &newMatrix)
  {
    if (this != &newMatrix)
    {
      Matrix<T>(newMatrix).swap(*this);
    }
    return *this;
  }

  template<typename T>
  tagiev::Matrix<T> &tagiev::Matrix<T>::operator =(tagiev::Matrix<T> &&newMatrix) noexcept
  {
    if (this != &newMatrix)
    {
      rows_ = newMatrix.rows_;
      count_ = newMatrix.count_;
      columns_ = std::move(newMatrix.columns_);
      data_ = std::move(newMatrix.data_);
      newMatrix.rows_ = 0;
      newMatrix.count_ = 0;
    }
    return *this;
  }

  template <typename T>
  typename Matrix<T>::Array Matrix<T>::operator [](int row) const
  {
    if (row >= rows_)
    {
      throw std::out_of_range("Index is out of range!");
    }

    int startIndex = 0;
    for (int i = 0; i < row; i++)
    {
      startIndex += columns_[i];
    }
    return Matrix<T>::Array(&data_[startIndex], columns_[row]);
  }

  template<typename T>
  bool Matrix<T>::operator ==(const Matrix<T> &matrix) const
  {
    if ((rows_ != matrix.rows_) || (count_ != matrix.count_))
    {
      return false;
    }

    for (int i = 0; i < rows_; i++)
    {
      if (columns_[i] != matrix.columns_[i])
      {
        return false;
      }
    }

    for (int i = 0; i < count_; i++)
    {
      if (data_[i] != matrix.data_[i])
      {
        return false;
      }
    }

    return true;
  }


  template<typename T>
  bool tagiev::Matrix<T>::operator !=(const tagiev::Matrix<T> &matrix) const
  {
    return !(*this == matrix);
  }

  template<typename T>
  int tagiev::Matrix<T>::getRows() const
  {
    return rows_;
  }

  template <typename T>
  void Matrix<T>::add(ptr type, int row)
  {
    if (row > rows_)
    {
      throw std::out_of_range("Index is out of range");
    }

    if (!type)
    {
      throw std::invalid_argument("Pointer have to be not equal nullptr");
    }

    std::unique_ptr<ptr []> newData = std::make_unique<ptr []>(count_ + 1);
    int newIndex = 0;
    int oldIndex = 0;
    for (int i = 0; i < rows_; i++)
    {
      for (int j = 0; j < columns_[i]; j++)
      {
        newData[newIndex++] = data_[oldIndex++];
      }
      if (row == i)
      {
        newData[newIndex++] = type;
        ++columns_[row];
      }
    }

    if (row == rows_)
    {
      std::unique_ptr<int []> newColumns = std::make_unique<int []>(rows_ + 1);
      for (int i = 0; i < rows_; i++)
      {
        newColumns[i] = columns_[i];
      }
      newColumns[row] = 1;
      newData[count_] = type;
      rows_++;
      columns_.swap(newColumns);
    }
    data_.swap(newData);
    count_++;
  }

  template <typename T>
  void Matrix<T>::swap(Matrix<T> &swapMatrix) noexcept
  {
    std::swap(rows_, swapMatrix.rows_);
    std::swap(count_, swapMatrix.count_);
    std::swap(columns_, swapMatrix.columns_);
    std::swap(data_, swapMatrix.data_);
  }

  template <typename T>
  Matrix<T>::Array::Array(const Matrix<T>::Array &copyArray):
    size_(copyArray.size_),
    array_(std::make_unique<ptr []>(copyArray.size_))
  {
    for (int i = 0; i < size_; i++)
    {
      array_[i] = copyArray.array_[i];
    }
  }

  template <typename T>
  Matrix<T>::Array::Array(Matrix<T>::Array &&movedArray) noexcept :
    size_(movedArray.size_),
    array_(std::move(movedArray.array_))
  {
    movedArray.size_ = 0;
  }

  template <typename T>
  Matrix<T>::Array::Array(ptr *array, int size):
    size_(size),
    array_(std::make_unique<ptr []>(size))
  {
    for (int i = 0; i < size_; i++)
    {
      array_[i] = array[i];
    }
  }

  template <typename T>
  typename Matrix<T>::Array &Matrix<T>::Array::operator =(const Matrix<T>::Array &copyArray)
  {
    if (this != &copyArray)
    {
      Matrix<T>::Array(copyArray).swap(*this);
    }

    return *this;
  }

  template <typename T>
  typename Matrix<T>::Array &Matrix<T>::Array::operator =(Matrix<T>::Array &&movedArray) noexcept
  {
    if (this != &movedArray)
    {
      size_ = movedArray.size_;
      movedArray.size_ = 0;
      array_ = std::move(movedArray.array_);
    }
  }

  template <typename T>
  typename Matrix<T>::ptr Matrix<T>::Array::operator [](int index) const
  {
    if (index >= size_)
    {
      throw std::out_of_range("Index less than size");
    }
    return array_[index];
  }

  template <typename T>
  int Matrix<T>::Array::size() const
  {
    return size_;
  }

  template <typename T>
  void Matrix<T>::Array::swap(Array &swapArray) noexcept
  {
    std::swap(size_, swapArray.size_);
    std::swap(array_, swapArray.array_);
  }
}
#endif
