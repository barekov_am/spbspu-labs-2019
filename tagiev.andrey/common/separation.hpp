#ifndef SEPARATION_H
#define SEPARATION_H
#include "composite-shape.hpp"
#include "matrix.hpp"

namespace tagiev
{
  Matrix<Shape> section(const CompositeShape);
  bool intersect(const rectangle_t, const rectangle_t);
}

#endif
