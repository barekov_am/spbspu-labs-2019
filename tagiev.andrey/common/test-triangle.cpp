#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"

//triangle

BOOST_AUTO_TEST_SUITE(testsForTriangle)

const double EPS = 0.01;

BOOST_AUTO_TEST_CASE(testOfImmutability)
{
  tagiev::Triangle testTriangle({0.0, 0.0}, {0.0, 4.0}, {3.0, 4.0});

  const double width = testTriangle.getFrameRect().width;
  const double height = testTriangle.getFrameRect().height;
  const double area = testTriangle.getArea();

  testTriangle.move(5.0, 6.0);
  BOOST_CHECK_CLOSE(testTriangle.getFrameRect().width, width, EPS);
  BOOST_CHECK_CLOSE(testTriangle.getFrameRect().height, height, EPS);
  BOOST_CHECK_CLOSE(testTriangle.getArea(), area, EPS);

  testTriangle.move({7.0, 8.0});
  BOOST_CHECK_CLOSE(testTriangle.getFrameRect().width, width, EPS);
  BOOST_CHECK_CLOSE(testTriangle.getFrameRect().height, height, EPS);
  BOOST_CHECK_CLOSE(testTriangle.getArea(), area, EPS);
}
BOOST_AUTO_TEST_CASE(testOfCorrectScaling)
{
  tagiev::Triangle testTriangle({0.0, 0.0}, {0.0, 4.0}, {3.0, 4.0});

  const double area = testTriangle.getArea();
  const double scale = 5.0;
  testTriangle.scale(scale);
  BOOST_CHECK_CLOSE(testTriangle.getArea(), area * scale * scale, EPS);
}
BOOST_AUTO_TEST_CASE(testInvalidValues)
{
  BOOST_CHECK_THROW(tagiev::Triangle testTriangle({0.0, 0.0}, {0.0, 0.0}, {3.0, 4.0}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testOfRotate)
{
  tagiev::Triangle testTriangle({0.0, 0.0}, {4.0, 0.0}, {4.0, 3.0});
  double area = testTriangle.getArea();

  double angle = -45.0;
  BOOST_CHECK_NO_THROW(testTriangle.rotate(angle));

  angle = 90.0;
  testTriangle.rotate(angle);
  BOOST_CHECK_CLOSE(testTriangle.getArea(), area, EPS);
}

BOOST_AUTO_TEST_SUITE_END();
