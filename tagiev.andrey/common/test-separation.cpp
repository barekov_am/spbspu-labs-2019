#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "separation.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(testSeparation)
  BOOST_AUTO_TEST_CASE(testIntersect)
  {
    tagiev::Rectangle testRect({0, 0}, 1, 1, 0);
    tagiev::Circle testCircle({1, 2}, 3);
    tagiev::Triangle testTriangle({2.0, 0.0}, {3.0, 1.0}, {3.0, 0.0});
    BOOST_CHECK(tagiev::intersect(testRect.getFrameRect(), testCircle.getFrameRect()));
    BOOST_CHECK(tagiev::intersect(testCircle.getFrameRect(), testTriangle.getFrameRect()));
    BOOST_CHECK(!tagiev::intersect(testTriangle.getFrameRect(), testRect.getFrameRect()));
  }
BOOST_AUTO_TEST_CASE(testSection)
{
  tagiev::CompositeShape testCom;
  tagiev::Rectangle testRect({0, 0}, 1, 1, 0);
  tagiev::Circle testCircle({1, 2}, 3);
  tagiev::Triangle testTriangle({2.0, 0.0}, {3.0, 1.0}, {3.0, 0.0});
  testCom.add(std::make_shared<tagiev::Triangle>(testTriangle));
  testCom.add(std::make_shared<tagiev::Rectangle>(testRect));
  testCom.add(std::make_shared<tagiev::Circle>(testCircle));

  tagiev::Matrix<tagiev::Shape> testMatrix = tagiev::section(testCom);
  BOOST_CHECK_EQUAL(testMatrix.getRows(), 2);
  BOOST_CHECK_EQUAL(testMatrix[0][0], testCom[0]);
  BOOST_CHECK_EQUAL(testMatrix[0][1], testCom[1]);
  BOOST_CHECK_EQUAL(testMatrix[1][0], testCom[2]);
}
BOOST_AUTO_TEST_SUITE_END()
