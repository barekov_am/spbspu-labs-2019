#ifndef COMPOSITE_SHAPE_H
#define COMPOSITE_SHAPE_H

#include "shape.hpp"
#include <iostream>
#include <memory>

namespace tagiev
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&);
    CompositeShape(const std::shared_ptr<tagiev::Shape>);
    ~CompositeShape() override = default;

    CompositeShape &operator =(const CompositeShape &);
    CompositeShape &operator =(CompositeShape &&);
    std::shared_ptr<tagiev::Shape> &operator [](const int) const;

    int getSize() const;
    void add(std::shared_ptr<tagiev::Shape>);
    void remove(const int);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const double &, const double &) override;
    void move(const point_t &) override;
    void scale(double) override;
    void rotate(double) override;

  private:
    int size_;
    std::unique_ptr<std::shared_ptr<Shape>[]> shapes_;
  };
}

#endif
