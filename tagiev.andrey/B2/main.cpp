#include "tasks.hpp"

int main(int argc, char *argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Incorrect count of arguments";
      return 1;
    }

    switch (atoi(argv[1]))
    {
      case 1:
        task1();
        break;

      case 2:
        task2();
        break;

      default:
        std::cerr << "Wrong number of task!";
        return 1;
    }
  }

  catch (const std::exception &err)
  {
    std::cerr << err.what();
    return 1;
  }

  return 0;
}
