#ifndef B2_QUEUE_PRIORITY
#define B2_QUEUE_PRIORITY

#include <list>
#include <stdexcept>

typedef enum
{
  LOW,
  NORMAL,
  HIGH
} ElementPriority;

template<typename QueueELement>
class QueueWithPriority
{
public:
  QueueWithPriority() = default;
  ~QueueWithPriority() = default;

  void accelerate();
  bool empty() const;
  void pushBackElement(const QueueELement &element, ElementPriority priority);
  QueueELement getElement();
  
private:
  std::list<QueueELement> high_;
  std::list<QueueELement> normal_;
  std::list<QueueELement> low_;
};

#endif
