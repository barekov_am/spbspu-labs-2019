#ifndef B2_QUEUE
#define B2_QUEUE

#include "queue-priority.hpp"

template<typename QueueELement>
void QueueWithPriority<QueueELement>::pushBackElement(const QueueELement &element, ElementPriority priority)
{
  switch (priority)
  {
    case ElementPriority::LOW:
      low_.push_back(element);
      break;

    case ElementPriority::NORMAL:
      normal_.push_back(element);
      break;

    case ElementPriority::HIGH:
      high_.push_back(element);
      break;

    default:
      throw std::invalid_argument("Wrong priority");
  }
}

template<typename QueueELement>
QueueELement QueueWithPriority<QueueELement>::getElement()
{
  if (!high_.empty())
  {
    QueueELement element = high_.front();
    high_.pop_front();
    return element;
  }
  else if (!normal_.empty())
  {
    QueueELement element = normal_.front();
    normal_.pop_front();
    return element;
  }
  else
  {
    QueueELement element = low_.front();
    low_.pop_front();
    return element;
  }
}

template<typename QueueELement>
void QueueWithPriority<QueueELement>::accelerate()
{
  high_.splice(high_.end(), low_);
}

template<typename QueueELement>
bool QueueWithPriority<QueueELement>::empty() const
{
  return high_.empty() && normal_.empty() && low_.empty();
}

#endif
