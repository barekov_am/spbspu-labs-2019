#include "tasks.hpp"
#include <fstream>
#include <memory>
#include <vector>


const std::size_t initSize = 1024;

void secondTask(const std::string &filename) {
  using charsArr = std::unique_ptr<char[], decltype(&free)>;

  std::ifstream file(filename);
  if (!file) {
   throw std::invalid_argument("No such file");
  }
  std::size_t size = initSize;

  charsArr array(static_cast<char *>(malloc(size)), &free);
  if (!array) {
    throw std::runtime_error("Memory couldn't be allocated!\n");
  }
  std::size_t count = 0;
  while (file) {
    file.read(&array[count], initSize);
    count += file.gcount();
    if (file.gcount() == initSize) {
      size += initSize;
      charsArr tmp_array(static_cast<char *>(realloc(array.get(),size)), &free);
      if (!tmp_array) {
        throw std::runtime_error("Could not allocate memory");
      }
      array.release();
      std::swap(tmp_array, array);
    }
    if (!file.eof() && file.fail()) {
      throw std::runtime_error("Reading failed!\n");
    }
  }

  std::vector<char> vector(&array[0], &array[count]);
  for (const auto& it: vector)
  {
    std::cout << it;
  }
}
