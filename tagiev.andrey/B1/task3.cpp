#include "tasks.hpp"
#include <vector>
#include "functions.hpp"

void insertAfter(std::vector<int> &vector) {
  if (vector.back() == 1) {
    for (auto it = vector.begin(); it != vector.end();) 
    {
      if ((*it % 2) == 0) {
        it = vector.erase(it);
        continue;
      }
      it++;
    }
  } else if (vector.back() == 2) {
    for (auto it = vector.begin(); it != vector.end();)
    {
      if ((*it % 3) == 0) {
        it = vector.insert(++it, 3, 1);
        std::advance(it, 3);
        continue;
      }
      it++;
    }
  }
}

void thirdTask() {
  std::vector<int> vector;
  int num = -1;
  while (std::cin >> num)
  {
    if (num == 0) {
      break;
    }
    vector.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail()) {
    throw std::runtime_error("Input failed.\n");
  }

  if (vector.empty()) {
    return;
  }

  if (num != 0) {
    throw std::runtime_error("Expected zero at the end.\n");
  }

  insertAfter(vector);

  print(vector);
}
