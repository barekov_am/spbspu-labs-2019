#include "tasks.hpp"
#include <forward_list>
#include "functions.hpp"

void firstTask(const std::string &order) {
  const bool reverse = isDescending(order);
  std::vector<int> vectorAt;
  int num = -1;

  while (std::cin && !(std::cin >> num).eof())
  {
    if (std::cin.fail()) {
      throw std::ios_base::failure("Error while reading");
    }
    vectorAt.push_back(num);
  }

  std::vector<int> vectorBrackets(vectorAt);
  std::forward_list<int> list(vectorBrackets.begin(), vectorBrackets.end());

  sortSelection<BracketsAccess>(vectorBrackets, reverse);
  sortSelection<AtAccess>(vectorAt, reverse);
  sortSelection<IteratorAccess>(list, reverse);

  print(vectorBrackets);
  print(vectorAt);
  print(list);
}
