#include <iostream>
#include "tasks.hpp"

int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    std::cerr << "No task number.\n";
    return 1;
  }

  try
  {
    switch (atoi(argv[1]))
    {
    case 1:
    {
      if (argc != 3)
      {
        std::cerr << "No sorting order";
        return 1;
      }
      firstTask(argv[2]);
      break;
    }
    case 2:
    {
      if (argc < 3)
      {
        std::cerr << "No filename specified";
        return 1;
      }
      secondTask(argv[2]);
      break;
    }
    case 3:
    {
      thirdTask();
      break;
    }
    case 4:
    {
      if (argc != 4) {
        std::cerr << "Wrong number of arguments";
        return 1;
      }
      if ((argv[2] == nullptr) || (argv[3] == nullptr)) {
        std::cerr << "Wrong parameters";
        return 1;
      }
      fourthTask(argv[2], std::stoi(argv[3]));
      break;
    }
    default:
      std::cerr << "Unknown task number";
      return 1;
    }
  }
  catch (std::exception& e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}
