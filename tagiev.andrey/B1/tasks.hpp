#ifndef B1_TASKS_HPP
#define B1_TASKS_HPP
#include <iostream>

void firstTask(const std::string &order);
void secondTask(const std::string &filename);
void thirdTask();
void fourthTask(const std::string &order, int size);

#endif
