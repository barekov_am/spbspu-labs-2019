#include "functions.hpp"

bool isDescending(const std::string &order)
{
  if (order == "ascending") {
    return false;
  }
  else if (order == "descending") {
    return true;
  }
  else {
    throw std::invalid_argument("Invalid second argument");
    return false;
  }
}

void fillRandom(double *array, int size)
{
  for (int i = 0; i < size; i++)
  {
    array[i] = (rand() % 21 - 10) / 10.0;
  }
}
