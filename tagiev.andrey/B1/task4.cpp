#include "functions.hpp"

void fourthTask(const std::string &order, int size)
{
  const bool reverse = isDescending(order);
  if (size <= 0) {
    throw std::runtime_error("Invalid size, must be more than 0!");
  }
  std::vector<double> vector(size);
  fillRandom(vector.data(), size);
  print(vector);
  sortSelection<IteratorAccess>(vector, reverse);
  print(vector);
}
