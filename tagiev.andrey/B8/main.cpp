#include "text-parser.hpp"

const int MIN_WIDTH = 25;

int main(int argc, char * argv[])
{
  try
  {
    if ((argc != 3) && (argc != 1))
    {
      throw std::invalid_argument("Invalid arguments!");
    }

    size_t width = 40;

    if (argc == 3)
    {
      if (std::string(argv[1]) != "--line-width")
      {
        throw std::invalid_argument("Invalid arguments!");
      }

      char *str_end;
      width = std::strtol(argv[2], &str_end, 10);
      if ((*str_end) || (width < MIN_WIDTH))
      {
        throw std::invalid_argument("Invalid arguments!");
      }
    }

    TextParser parser(width);
    parser.readText();
    parser.printText();
  }
  catch (std::exception & err)
  {
    std::cerr << err.what() << '\n';
    return 1;
  }
  return 0;
}
