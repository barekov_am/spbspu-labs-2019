#include "text-parser.hpp"

const size_t MAX_LENGTH_WORD = 20;
const size_t LENGTH_HYPHEN = 3;

TextParser::TextParser(size_t width) :
  in_(std::cin),
  out_(std::cout),
  width_(width),
  text_(),
  lastElement_()
{}

void TextParser::readText()
{
  std::locale locale(in_.getloc());
  while (in_)
  {
      char ch = (in_ >> std::ws).get();
      if (isalpha(ch))
      {
          in_.unget();
          readWord();
      }
      else if (ch == '-')
      {
          if (in_.peek() == '-')
          {
              in_.unget();
              readHyphen();
          }
          else
          {
              in_.unget();
              readNumber();
          }
      }
      else if ((isdigit(ch)) || (ch == '+'))
      {
          in_.unget();
          readNumber();
      }
      else if (ispunct(ch))
      {
          in_.unget();
          readPunctuation();
      }
  }
}

void TextParser::readWord()
{
  TextElement word {
    TextElement::WORD,
    ""
  };

  while (std::isalpha(in_.peek()) || (in_.peek() == '-'))
  {
    char ch = in_.get();
    if (ch == '-' && in_.peek() == '-')
    {
      in_.unget();
      break;
    }
    word.content.push_back(ch);
  }
  if (word.content.length() > MAX_LENGTH_WORD)
  {
    throw std::invalid_argument("Length of \"" + word.content + "\" more than 20 symbols");
  }

  lastElement_ = word;
  text_.push_back(word);
}

void TextParser::readPunctuation()
{
  if (text_.empty())
  {
    throw std::invalid_argument("Text can't start whit punctuation");
  }

  TextElement punct {
    TextElement::PUNCTUATION,
    ""
  };

  punct.content.push_back(in_.get());

  if (lastElement_.type == TextElement::HYPHEN)
  {
    throw std::invalid_argument("Punctuation can't be after hyphen");
  }
  if (lastElement_.type == TextElement::PUNCTUATION)
  {
    throw std::invalid_argument("Punctuation can't be after another punctuation");
  }

  lastElement_ = punct;
  text_.push_back(punct);
}

void TextParser::readNumber()
{
  TextElement number {
    TextElement::NUMBER,
    ""
  };

  bool dotFound = false;
  number.content.push_back(in_.get());

  while (std::isdigit(in_.peek()) || (in_.peek() == '.'))
  {
    char sym = in_.get();
    if (sym == '.')
    {
      if (dotFound)
      {
        in_.unget();
        break;
      }
      dotFound = true;
    }
    number.content.push_back(sym);
  }

  if (number.content.length() > MAX_LENGTH_WORD)
  {
    throw std::invalid_argument("Length of number > 20 symbols!");
  }

  lastElement_ = number;
  text_.push_back(number);
}

void TextParser::readHyphen()
{
  if (text_.empty())
  {
    throw std::invalid_argument("Text can't start whit hyphen");
  }

  TextElement hyphen {
    TextElement::HYPHEN,
    ""
  };

  while (in_.peek() == '-')
  {
    hyphen.content.push_back(in_.get());
  }
  if (lastElement_.type == TextElement::PUNCTUATION && text_.back().content != ",")
  {
    throw std::invalid_argument("Hyphen after punctuation");
  }
  else if (hyphen.content.length() != LENGTH_HYPHEN)
  {
    throw std::invalid_argument("Invalid symbol: " + hyphen.content);
  }
  else if (lastElement_.type == TextElement::HYPHEN)
  {
    throw std::invalid_argument("Hyphen after hyphen");
  }

  lastElement_ = hyphen;
  text_.push_back(hyphen);
}

void TextParser::printText()
{
  size_t lengthLine = 0;
  std::list<TextElement> line;

  for (auto & iter : text_)
  {
    switch (iter.type)
    {
    case TextElement::NUMBER :
    case TextElement::WORD :
    {
      if ((lengthLine + iter.content.length() + 1) > width_)
      {
        printLine(line);
        line.clear();
        lengthLine = 0;
      }
      else if (!line.empty())
      {
        line.push_back(TextElement{TextElement::SPACE, " "});
        lengthLine++;
      }
      line.push_back(iter);
      lengthLine += iter.content.length();
      break;
    }
    case TextElement::PUNCTUATION :
    {
      if (lengthLine + 1 > width_)
      {
        reformatLine(line, lengthLine);
      }
      line.push_back(iter);
      lengthLine += iter.content.length();
      break;
    }
    case TextElement::HYPHEN :
    {
      if (lengthLine + LENGTH_HYPHEN + 1 > width_)
      {
        reformatLine(line, lengthLine);
      }
      line.push_back(TextElement{TextElement::SPACE, " "});
      line.push_back(iter);
      lengthLine += iter.content.length() + 1;
      break;
    }
    case TextElement::SPACE :
    {
      break;
    }
    }
  }
  if (!line.empty())
  {
    printLine(line);
  }
}

void TextParser::printLine(const std::list<TextElement> & line)
{
  for (auto & iter : line)
  {
    out_ << iter.content;
  }
  out_ << '\n';
}

void TextParser::reformatLine(std::list<TextElement> &line, size_t &lengthLine)
{
  size_t tmpLength = 0;
  std::list<TextElement> tmpLine;

  while (!line.empty())
  {
    tmpLine.push_front(line.back());
    tmpLength += line.back().content.length();
    line.pop_back();

    if (tmpLine.front().type == TextElement::WORD || tmpLine.front().type == TextElement::NUMBER)
    {
      break;
    }
  }
  printLine(line);
  line = tmpLine;
  lengthLine = tmpLength;
}


