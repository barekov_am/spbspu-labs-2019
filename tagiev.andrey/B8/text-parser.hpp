#ifndef TEXT_PARSER_HPP
#define TEXT_PARSER_HPP

#include <iostream>
#include <list>

struct TextElement
{
  enum {
    WORD,
    PUNCTUATION,
    NUMBER,
    HYPHEN,
    SPACE
  } type;
  std::string content;
};

class TextParser
{
public:
  TextParser(size_t width);
  void readText();
  void printText();

private:
  std::istream & in_;
  std::ostream & out_;
  size_t width_;
  std::list<TextElement> text_;
  TextElement lastElement_;

  void readWord();
  void readPunctuation();
  void readNumber();
  void readHyphen();
  void printLine(const std::list<TextElement> & line);
  void reformatLine(std::list<TextElement> & line, size_t & lengthLine);
};

#endif
