#ifndef MATRIX_HPP
#define MATRIX_HPP
#include <memory>
#include <cstddef>
#include "shape.hpp"
#include "composite-shape.hpp"


namespace smirnova
{
  class Matrix
    {
    public:
      Matrix();
      Matrix(const Matrix&);
      Matrix(Matrix&&);
      ~Matrix() = default;
        
      Matrix &operator =(const Matrix&);
      Matrix &operator =(Matrix&&);
      CompositeShape::array_ operator [](const size_t index) const;
      bool operator ==(const Matrix &newMatrix) const;
      bool operator !=(const Matrix &newMatrix) const;
        
      void add(CompositeShape::ptr shape, size_t line, size_t column);
      size_t getLines() const;
      size_t getColumns() const;

        
    private:
      size_t lines_;
      size_t columns_;
      CompositeShape::array_ shapes_;
    };
}
    
#endif
