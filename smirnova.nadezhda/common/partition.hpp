#ifndef PARTITION_HPP
#define PARTITION_HPP
#include "composite-shape.hpp"
#include "matrix.hpp"

namespace smirnova
{
  bool cross(const Shape &, const Shape &);
  Matrix part(const CompositeShape &);
}

#endif
