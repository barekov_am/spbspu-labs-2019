#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "shape.hpp"

namespace smirnova
{
  class Polygon : public Shape
  {
  public:
    Polygon(const Polygon& other);
    Polygon(Polygon&& other);
    Polygon(int count_t, const point_t* vertex);
    virtual ~Polygon();
    Polygon& operator =(const Polygon& other);
    Polygon& operator =(Polygon&& other);
 
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &pos) override;
    void scale(double coef) override;
    point_t getCenter() const;
    void rotate(double angle) override;
    bool checkBump() const;

  private:
    int count_;
    point_t* vertex_;
    point_t center_;
    };
}

#endif
