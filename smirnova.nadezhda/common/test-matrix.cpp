#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testMatrix)

const double EPSILON = 0.01;

BOOST_AUTO_TEST_CASE(testOfCopyAndMoveConstructor)
{
  smirnova::Rectangle testRectangle(3.4, 4.2, {5.0, 6.4});
  smirnova::Circle testCircle({5.1, 4.2}, 4.0);

  smirnova::CompositeShape testShapes(std::make_shared<smirnova::Rectangle>(testRectangle));
  testShapes.add(std::make_shared<smirnova::Circle>(testCircle));
  smirnova::Matrix testMatrix = smirnova::part(testShapes);

  BOOST_CHECK_NO_THROW(smirnova::Matrix testMatrix2(testMatrix));
  BOOST_CHECK_NO_THROW(smirnova::Matrix testMatrix3(std::move(testMatrix)));
}

BOOST_AUTO_TEST_CASE(testOfOperator)
{
  smirnova::Rectangle r1(3.4, 4.2, {5.0, 6.4});
  smirnova::Circle c1({5.1, 4.2}, 4.0);

  smirnova::CompositeShape testShapes1(std::make_shared<smirnova::Rectangle>(r1));
  testShapes1.add(std::make_shared<smirnova::Circle>(c1));
  smirnova::Matrix matrix1 = smirnova::part(testShapes1);

  BOOST_CHECK_THROW(matrix1[100], std::out_of_range);
  BOOST_CHECK_THROW(matrix1[-1], std::out_of_range);

  BOOST_CHECK_NO_THROW(smirnova::Matrix matrix2 = matrix1);
  BOOST_CHECK_NO_THROW(smirnova::Matrix matrix3 = std::move(matrix1));

  smirnova::Rectangle r2(2.0, 4.8, {5.3, 3.6});
  smirnova::CompositeShape testShapes2(std::make_shared<smirnova::Rectangle>(r2));

  smirnova::Matrix matrix4 = smirnova::part(testShapes2);
  smirnova::Matrix matrix5 = smirnova::part(testShapes2);

  BOOST_CHECK(matrix4 == matrix5);

  BOOST_CHECK(matrix4 != matrix1);
  BOOST_CHECK(matrix5 != matrix1);
}

BOOST_AUTO_TEST_SUITE_END()
