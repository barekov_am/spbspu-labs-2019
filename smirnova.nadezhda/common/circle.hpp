#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace smirnova
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t &pos, double radius);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &pos) override;
    void scale(double coef) override;
    void rotate(double) override;
  private:
    point_t center_;
    double radius_;
  };
}
#endif
