#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testForRectangle)

const double EPSILON = 0.01;

BOOST_AUTO_TEST_CASE(testOfImmutability)
{
  smirnova::Rectangle testRectangle(3.4, 4.2, {5.0, 6.4});
  const double area = testRectangle.getArea();
  const double width = testRectangle.getFrameRect().width;
  const double height = testRectangle.getFrameRect().height;

  testRectangle.move(3.5, -2.2);
  BOOST_CHECK_CLOSE(area, testRectangle.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(width, testRectangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height, testRectangle.getFrameRect().height, EPSILON);

  testRectangle.move({11.0, 14.2});
  BOOST_CHECK_CLOSE(area, testRectangle.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(width, testRectangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height, testRectangle.getFrameRect().height, EPSILON);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterScaling)
{
  smirnova::Rectangle testRectangle(3.4, 4.2, {5.0, 6.4});
  double area = testRectangle.getArea();
  double coef = 3.7;

  testRectangle.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, testRectangle.getArea(), EPSILON);
  area = testRectangle.getArea();
  coef = 0.6;
  testRectangle.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, testRectangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterRotation)
{
  smirnova::Rectangle testRectangle(8.7, 15.3, {6.8, 1.2});
  const double area = testRectangle.getArea();
  const double angle = 54.3;
    
  testRectangle.rotate(angle);
  BOOST_CHECK_CLOSE( area, testRectangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  BOOST_CHECK_THROW(smirnova::Rectangle(-3.4, -4.2, {5.0, -6.4}), std::invalid_argument);
  smirnova::Rectangle testRectangle(-3.4, 4.2, {5.0, 6.4});
  BOOST_CHECK_THROW(testRectangle.scale(-2.4), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
