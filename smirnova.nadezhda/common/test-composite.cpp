#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(testCompositeShape)

const int count = 5;
const double EPSILON = 0.01;

BOOST_AUTO_TEST_CASE(testOfCopyConstructor)
{
  smirnova::Rectangle testRectangle(3.4, 4.2, {5.0, 6.4});
  smirnova::Circle testCircle({5.1, 4.2}, 4.0);
  smirnova::CompositeShape CompShape1(std::make_shared<smirnova::Rectangle>(testRectangle));
  CompShape1.add(std::make_shared<smirnova::Circle>(testCircle));
  smirnova::CompositeShape CompShape2(CompShape1);

  BOOST_CHECK_CLOSE(CompShape1.getFrameRect().pos.x, CompShape2.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(CompShape1.getFrameRect().pos.y, CompShape2.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(CompShape1.getFrameRect().width, CompShape2.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(CompShape1.getFrameRect().height, CompShape2.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(CompShape1.getArea(), CompShape2.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(testOfMoveConstructor)
{
  smirnova::Rectangle testRectangle(3.4, 4.2, {5.0, 6.4});
  smirnova::Circle testCircle({5.1, 4.2}, 4.0);
  smirnova::CompositeShape CompShape1(std::make_shared<smirnova::Rectangle>(testRectangle));
  CompShape1.add(std::make_shared<smirnova::Circle>(testCircle));

  const smirnova::rectangle_t testFrame = CompShape1.getFrameRect();
  const double testArea = CompShape1.getArea();

  smirnova::CompositeShape CompShape2(std::move(CompShape1));

  BOOST_CHECK_CLOSE(testFrame.pos.x, CompShape2.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.pos.y, CompShape2.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.width, CompShape2.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.height, CompShape2.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(testArea, CompShape2.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(testOfCopyOperator)
{
  smirnova::Rectangle testRectangle(3.4, 4.2, {5.0, 6.4});
  smirnova::Circle testCircle({5.1, 4.2}, 4.0);
  smirnova::CompositeShape CompShape1(std::make_shared<smirnova::Rectangle>(testRectangle));
  CompShape1.add(std::make_shared<smirnova::Circle>(testCircle));
  smirnova::CompositeShape CompShape2;
  CompShape2 = CompShape1;

  BOOST_CHECK_CLOSE(CompShape1.getFrameRect().pos.x, CompShape2.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(CompShape1.getFrameRect().pos.y, CompShape2.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(CompShape1.getFrameRect().width, CompShape2.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(CompShape1.getFrameRect().height, CompShape2.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(CompShape1.getArea(), CompShape2.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(testOfMoveOperator)
{
  smirnova::Rectangle testRectangle(3.4, 4.2, {5.0, 6.4});
  smirnova::Circle testCircle({5.1, 4.2}, 4.0);
  smirnova::CompositeShape CompShape1(std::make_shared<smirnova::Rectangle>(testRectangle));
  CompShape1.add(std::make_shared<smirnova::Circle>(testCircle));
  const smirnova::rectangle_t testFrame = CompShape1.getFrameRect();
  const double testArea = CompShape1.getArea();

  smirnova::CompositeShape CompShape2;
  CompShape2 = std::move(CompShape1);

  BOOST_CHECK_CLOSE(testFrame.pos.x, CompShape2.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.pos.y, CompShape2.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.width, CompShape2.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.height, CompShape2.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(testArea, CompShape2.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(testsOfMove)
{
  smirnova::Rectangle testRectangle(3.4, 4.2, {5.0, 6.4});
  smirnova::Circle testCircle({5.1, 4.2}, 4.0);
  smirnova::Triangle testTriangle({6.0, 6.0}, {1.0, 3.0}, {4.0, 5.0});
  smirnova::point_t points[count] = {{7, 3}, {4, 1}, {8, 5}, {2, 3}, {3, 4}};
  smirnova::Polygon testPolygon(count, points);

  smirnova::CompositeShape testComposite(std::make_shared<smirnova::Rectangle>(testRectangle));
  testComposite.add(std::make_shared<smirnova::Circle>(testCircle));
  testComposite.add(std::make_shared<smirnova::Triangle>(testTriangle));
  testComposite.add(std::make_shared<smirnova::Polygon>(testPolygon));

  const smirnova::rectangle_t getFrameBeforeMove = testComposite.getFrameRect();
  const double getAreaBeforeMove = testComposite.getArea();

  testComposite.move({3, 4});
  smirnova::rectangle_t getFrameAfterMove = testComposite.getFrameRect();
  double getAreaAfterMove = testComposite.getArea();

  BOOST_CHECK_CLOSE(getFrameBeforeMove.width, getFrameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(getFrameBeforeMove.height, getFrameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(getAreaBeforeMove, getAreaAfterMove, EPSILON);

  testComposite.move(3, 2);
  getFrameAfterMove = testComposite.getFrameRect();
  getAreaAfterMove = testComposite.getArea();

  BOOST_CHECK_CLOSE(getFrameBeforeMove.width, getFrameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(getFrameBeforeMove.height, getFrameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(getAreaBeforeMove, getAreaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(testOfScale)
{
  smirnova::Rectangle testRectangle(3.4, 4.2, {5.0, 6.4});
  smirnova::Circle testCircle({5.1, 4.2}, 4.0);
  smirnova::Triangle testTriangle({6.0, 6.0}, {1.0, 3.0}, {4.0, 5.0});
  smirnova::point_t points[count] = {{7, 3}, {4, 1}, {8, 5}, {2, 3}, {3, 4}};
  smirnova::Polygon testPolygon(count, points);

  smirnova::CompositeShape testComposite(std::make_shared<smirnova::Rectangle>(testRectangle));
  testComposite.add(std::make_shared<smirnova::Circle>(testCircle));
  testComposite.add(std::make_shared<smirnova::Triangle>(testTriangle));
  testComposite.add(std::make_shared<smirnova::Polygon>(testPolygon));

  const double getAreaBeforeScale = testComposite.getArea();
  const double coef = 3;
  testComposite.scale(coef);
  double getAreaAfterScale = testComposite.getArea();

  BOOST_CHECK_CLOSE(getAreaBeforeScale * coef * coef, getAreaAfterScale, EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidParametrs)
{
  smirnova::Rectangle testRectangle(3.4, 4.2, {5.0, 6.4});
  smirnova::Circle testCircle({5.1, 4.2}, 4.0);
  smirnova::Triangle testTriangle({6.0, -6.0}, {-1.0, -3.0}, {-4.0, 5.0});

  smirnova::CompositeShape testComposite(std::make_shared<smirnova::Rectangle>(testRectangle));
  testComposite.add(std::make_shared<smirnova::Triangle>(testTriangle));

  BOOST_CHECK_THROW(testComposite.scale(-3), std::invalid_argument);
  BOOST_CHECK_THROW(testComposite[8], std::out_of_range);
  BOOST_CHECK_THROW(testComposite[-6], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
