#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include <memory>
#include "shape.hpp"

namespace smirnova
{
  class CompositeShape : public Shape
  {
  public:
    using ptr = std::shared_ptr<Shape>;
    using array_ = std::unique_ptr<ptr[]>;

    CompositeShape();
    CompositeShape(const CompositeShape& rhs);
    CompositeShape(CompositeShape&& rhs);
    CompositeShape(const ptr &shape);

    ~CompositeShape() override = default;

    CompositeShape& operator =(const CompositeShape& rhs);
    CompositeShape& operator =(CompositeShape&& rhs);
    ptr operator [](std::size_t index) const;

    bool operator ==(const CompositeShape& rhs);
    bool operator !=(const CompositeShape& rhs);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t& pos) override;
    void scale(double coef) override;
    void printInfo() const;
    void rotate(double angle) override;
    void add(const ptr &shape);
    void remove(std::size_t index);
    std::size_t getQuantity() const;
    array_ getShapes() const;

  private:
    std::size_t count_;
    array_ arrayOfShapes_;
  };
}

#endif
