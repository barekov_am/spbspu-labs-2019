#include "polygon.hpp"
#include <cstddef>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <cmath>

smirnova::Polygon::Polygon(const Polygon &other):
count_(other.count_),
vertex_(new point_t[other.count_]),
center_(other.center_)
{
  for (int i = 0; i < count_; i++)
  {
    vertex_[i] = other.vertex_[i];
  }
}

smirnova::Polygon::Polygon(Polygon &&other):
count_(other.count_),
vertex_(other.vertex_),
center_(other.center_)
{
  other.count_ = 0;
  other.center_ = {0.0, 0.0};
  other.vertex_ = nullptr;
}

smirnova::Polygon::Polygon(int count, const point_t* vertex):
count_(count),
vertex_(new point_t[count]),
center_(getCenter())
{
  if (count <= 2)
  {
    delete[] vertex_;
    throw std::invalid_argument("Area must be > 0, there should be 3 or more corners");
    }

  if (vertex == nullptr)
  {
    delete[] vertex_;
    throw std::invalid_argument("Pointer is null");
  }

  for (int i = 0; i < count_; ++i)
  {
    vertex_[i] = vertex[i];
  }
  center_ = getCenter();

  if (!checkBump())
  {
    delete[] vertex_;
    throw std::invalid_argument("Polygon must be convex");
  }

  if (getArea() == 0.0)
  {
    delete[] vertex_;
    throw std::invalid_argument("Area must be more then 0");
  }
}

smirnova::Polygon::~Polygon()
{
  delete[] vertex_;
}

smirnova::Polygon& smirnova::Polygon::operator =(const Polygon &other)
{
  if (this == &other)
  {
    return *this;
  }
  delete[] vertex_;
  count_ = other.count_;
  center_ = other.center_;
  vertex_ = new point_t[count_];

  for (int i = 0; i < count_; ++i)
  {
    vertex_[i] = other.vertex_[i];
  }
  return *this;
}

smirnova::Polygon& smirnova::Polygon::operator =(Polygon &&other)
{
  if (this == &other)
  {
    return *this;
  }
  count_ = other.count_;
  center_ = other.center_;
  delete[] vertex_;

  other.count_ = 0;
  other.center_ = {0.0, 0.0};
  vertex_ = other.vertex_;
  other.vertex_ = nullptr;

  return *this;
}

double smirnova::Polygon::getArea() const
{
  double sum = 0;
  for (int i = 0; i < count_ - 1; i++)
  {
    sum += vertex_[i].x * vertex_[i + 1].y;
    sum -= vertex_[i + 1].x * vertex_[i].y;
  }
  sum += vertex_[count_ - 1].x * vertex_[0].y;
  sum -= vertex_[0].x * vertex_[count_ - 1].y;
  return (fabs(sum) / 2);
}

smirnova::rectangle_t
smirnova::Polygon::getFrameRect() const
{
  point_t min = vertex_[0];
  point_t max = vertex_[0];

  for (int i = 1; i < count_; ++i)
  {
    min.x = std::min(vertex_[i].x, min.x);
    max.x = std::max(vertex_[i].x, max.x);
    min.y = std::min(vertex_[i].y, min.y);
    max.y = std::max(vertex_[i].y, max.y);
  }
  double width = max.x - min.x;
  double height = max.y - min.y;
  point_t centerRect = {(max.x + min.x) / 2, (max.y + min.y) / 2};

  return{width, height, centerRect};
}

void smirnova::Polygon::move(double dx, double dy)
{
  for (int i = 0; i < count_; ++i)
  {
    vertex_[i].x += dx;
    vertex_[i].y += dy;
  }
  center_.x += dx;
  center_.y += dy;
}

void smirnova::Polygon::move(const point_t &pos)
{
  move(pos.x - center_.x, pos.y - center_.y);
}

void smirnova::Polygon::scale(double coef)
{
  if (coef <= 0)
  {
    throw std::invalid_argument("Coefficient must be > 0");
  }

  for (int i = 0; i <= count_-1; i++)
  {
    vertex_[i] = { center_.x + coef * (vertex_[i].x - center_.x), center_.y + coef * (vertex_[i].y - center_.y) };
  }
}

smirnova::point_t smirnova::Polygon::getCenter() const
{
    point_t sum = { 0, 0 };
    for (int i = 0; i < count_; ++i)
    {
        sum.x += vertex_[i].x;
        sum.y += vertex_[i].y;
    }
    return { sum.x / count_, sum.y / count_ };
}

void smirnova::Polygon::rotate(double angle)
{
    const point_t center = getCenter();
    
    for (int i = 0; i < count_; i++)
    {
        smirnova::rotatePoint(center, vertex_[i], angle);
    }
}

bool smirnova::Polygon::checkBump() const
{
  point_t sideA = {0, 0};
  point_t sideB = {0, 0};
  for (int i = 0; i <= count_ - 2; i++)
  {
    if (i == count_ - 2)
    {
      sideA.x = vertex_[i + 1].x - vertex_[i].x;
      sideA.y = vertex_[i + 1].y - vertex_[i].y;
      sideB.x = vertex_[0].x - vertex_[i + 1].x;
      sideB.y = vertex_[0].y - vertex_[i + 1].y;
    }
    else
    {
      sideA.x = vertex_[i + 1].x - vertex_[i].x;
      sideA.y = vertex_[i + 1].y - vertex_[i].y;
      sideB.x = vertex_[i + 2].x - vertex_[i + 1].x;
      sideB.y = vertex_[i + 2].y - vertex_[i + 1].y;
    }
    if ((sideA.x * sideB.y) - (sideA.y * sideB.x) < 0)
    {
      return true;
    }
  }
  return false;
}
