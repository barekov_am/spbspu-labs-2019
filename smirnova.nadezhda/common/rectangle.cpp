#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

smirnova::Rectangle::Rectangle(double width, double height, const point_t &pos):
  width_(width),
  height_(height),
  angle_(0),
  center_(pos)
{
  if ((width_ < 0) && (height_ < 0))
  {
    throw std::invalid_argument("Width or height must be > 0");
  }
}

double smirnova::Rectangle::getArea() const
{
  return (width_ * height_);
}

smirnova::rectangle_t
smirnova::Rectangle::getFrameRect() const
{
  const double cos_ = cos(angle_ * M_PI / 180);
  const double sin_ = sin(angle_ * M_PI / 180);
  const double width = height_ * std::fabs(sin_) + width_ * std::fabs(cos_);
  const double height = height_ * std::fabs(cos_) + width_ * std::fabs(sin_);

  return {width, height, center_};
}

void smirnova::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void smirnova::Rectangle::move(const point_t &pos)
{
  center_ = pos;
}

void smirnova::Rectangle::scale(const double coef)
{
  if (coef <= 0.0)
  {
    throw std::invalid_argument("Coefficient must be > 0");
  }
  width_ *= coef;
  height_ *= coef;
}

void smirnova::Rectangle::rotate(double angle)
{
  angle_ += angle;
  while (angle_ >= 360.0)
  {
     angle_-= 360.0;
  }
}
