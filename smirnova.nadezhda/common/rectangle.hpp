#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace smirnova
{
  class Rectangle: public Shape
  {
  public:
    Rectangle(double width, double height, const point_t &pos);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &pos) override;
    void scale(double coef) override;
    void rotate(double angle) override;

  private:
    double width_;
    double height_;
    double angle_;
    point_t center_;
  };
}
#endif
