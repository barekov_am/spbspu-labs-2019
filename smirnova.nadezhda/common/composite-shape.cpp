#include "composite-shape.hpp"
#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <cmath>

smirnova::CompositeShape::CompositeShape():
count_(0),
arrayOfShapes_(nullptr)
{
}

smirnova::CompositeShape::CompositeShape(const CompositeShape& rhs):
count_(rhs.count_),
arrayOfShapes_(std::make_unique<ptr[]>(rhs.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i] = rhs.arrayOfShapes_[i];
  }
}

smirnova::CompositeShape::CompositeShape(CompositeShape&& rhs):
count_(rhs.count_),
arrayOfShapes_(std::move(rhs.arrayOfShapes_))
{
  rhs.count_ = 0;
  rhs.arrayOfShapes_.reset();
}

smirnova::CompositeShape::CompositeShape(const ptr &shape):
count_(1),
arrayOfShapes_(std::make_unique<ptr[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape: not exist");
  }
  arrayOfShapes_[0] = shape;
}

smirnova::CompositeShape&
smirnova::CompositeShape::operator =(const CompositeShape& rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    arrayOfShapes_=std::make_unique<CompositeShape::ptr[]>(rhs.count_);
    for (size_t i = 0; i < rhs.count_; i++)
    {
      arrayOfShapes_[i] = rhs.arrayOfShapes_[i];
    }
  }
  return *this;
}

smirnova::CompositeShape&
smirnova::CompositeShape::operator=(CompositeShape&& rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    arrayOfShapes_ = std::move(rhs.arrayOfShapes_);;
    rhs.count_ = 0;
  }
  return *this;
}

smirnova::CompositeShape::ptr
smirnova::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }
  return arrayOfShapes_[index];
}

bool smirnova::CompositeShape::operator ==(const CompositeShape& rhs)
{
  if (count_ != rhs.count_)
  {
    return false;
  }
  
  for (size_t i = 0; i < count_; i++)
  {
    if (arrayOfShapes_[i] != rhs.arrayOfShapes_[i])
    {
      return false;
    }
  }
  return true;
}

bool smirnova::CompositeShape::operator !=(const CompositeShape& rhs)
{
  return !(*this == rhs);
}

double smirnova::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    area += arrayOfShapes_[i]->getArea();
  }
  return area;
}

smirnova::rectangle_t
smirnova::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  rectangle_t tempFrameRect = arrayOfShapes_[0]->getFrameRect();

  double minX = tempFrameRect.pos.x - tempFrameRect.width / 2;
  double maxX = tempFrameRect.pos.x + tempFrameRect.width / 2;
  double minY = tempFrameRect.pos.y - tempFrameRect.height / 2;
  double maxY = tempFrameRect.pos.y + tempFrameRect.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    tempFrameRect = arrayOfShapes_[i]->getFrameRect();

    double temp = tempFrameRect.pos.x - tempFrameRect.width / 2;

    minX = std::min(temp, minX);
    temp = tempFrameRect.pos.x + tempFrameRect.width / 2;
    maxX = std::max(temp, maxX);
    temp = tempFrameRect.pos.y - tempFrameRect.height / 2;
    minY = std::min(temp, minY);
    temp = tempFrameRect.pos.y + tempFrameRect.height / 2;
    maxY = std::max(temp, maxY);
  }
  return { maxX - minX, maxY - minY, {(maxX + minX) / 2, (maxY + minY) / 2} };
}

void smirnova::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i]->move(dx, dy);
  }
}

void smirnova::CompositeShape::move(const point_t& pos)
{
  point_t center = getFrameRect().pos;
  for ( size_t i = 0; i < count_ ; i++)
  {
    arrayOfShapes_[i]->move(pos.x - center.x, pos.y - center.y);
  }
}

void smirnova::CompositeShape::scale(double coef)
{
  if (coef <= 0.0)
  {
    throw std::invalid_argument("Composite shape multiplier must be a positive number");
  }

  point_t tempFrame = getFrameRect().pos;
  for (size_t i = 0; i < count_; i++)
  {
    point_t currFrame = arrayOfShapes_[i]->getFrameRect().pos;
    double dx = (currFrame.x - tempFrame.x) * (coef - 1);
    double dy = (currFrame.y - tempFrame.y) * (coef - 1);
    arrayOfShapes_[i]->move(dx, dy);
    arrayOfShapes_[i]->scale(coef);
    }
}

void smirnova::CompositeShape::rotate(double angle)
{
  const point_t compCenter = getFrameRect().pos;
  
  for (size_t i = 0; i < count_; i++)
  {
    const point_t shapeCenter = arrayOfShapes_[i]->getFrameRect().pos;
    arrayOfShapes_[i]->move(smirnova::rotatePoint(compCenter, shapeCenter, angle));
    arrayOfShapes_[i]->rotate(angle);
  }
}

void smirnova::CompositeShape::add(const ptr& shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Adding shape shouldn't be nullptr");
  }
  
  CompositeShape::array_ newShape(std::make_unique<CompositeShape::ptr[]>(count_ + 1));
  for (size_t i = 0; i < count_; i++)
  {
    newShape[i] = arrayOfShapes_[i];
  }
  
  newShape[count_] = shape;
  count_++;
  arrayOfShapes_.swap(newShape);
}

void smirnova::CompositeShape::remove(std::size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }
  if (count_ == 0)
  {
    throw std::out_of_range("Composite shape is empty, nothing to delete");
  }
 
  for (std::size_t i = index; i < count_ - 1; i++)
  {
    arrayOfShapes_[i] = arrayOfShapes_[i + 1];
  }
  count_--;
}

std::size_t smirnova::CompositeShape::getQuantity() const
{
    return(count_);
}

void smirnova::CompositeShape::printInfo() const
{
  std::cout << "Composite Shape: " << std::endl;
  std::cout << "Area: " << getArea() << std::endl;
  rectangle_t frame = getFrameRect();
  std::cout << "Center: (" << frame.pos.x << " , " << frame.pos.y << ")" << std::endl;
  std::cout << "Frame width: " << frame.width << std::endl;
  std::cout << "Frame height: " << frame.height << std::endl;
}
