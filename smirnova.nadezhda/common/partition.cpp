#include "partition.hpp"
#include <cmath>

bool smirnova::cross(const smirnova::Shape &lhs, const smirnova::Shape &rhs)
{
  const smirnova::rectangle_t firstFrame = lhs.getFrameRect();
  const smirnova::rectangle_t secondFrame = rhs.getFrameRect();
  if (std::abs(firstFrame.pos.x - secondFrame.pos.x) > (firstFrame.width + secondFrame.width) / 2)
  {
    return false;
  }
  if (std::abs(firstFrame.pos.y - secondFrame.pos.y) > (firstFrame.height + secondFrame.height) / 2)
  {
    return false;
  }
  return true;
}
    
smirnova::Matrix smirnova::part(const smirnova::CompositeShape &composite)
{
  smirnova::Matrix tmpMatrix;
  for (size_t i = 0; i < composite.getQuantity(); ++i)
  {
    size_t lines = 0;
    size_t columns = 0;
    for (size_t j = tmpMatrix.getLines(); j-- > 0;)
    {
      bool between = false;
      for (size_t k = 0; k < tmpMatrix.getColumns(); ++k)
      {
        if (tmpMatrix[j][k] == nullptr)
        {
          columns = k;
          break;
        }
        if (cross(*composite[i], *tmpMatrix[j][k]))
        {
          lines = j + 1;
          between = true;
          break;
        }
        if (k == tmpMatrix.getColumns() - 1)
        {
          columns = k + 1;
        }
      }
      if (between)
      {
        break;
      }
    }
    tmpMatrix.add(composite[i], lines, columns);
  }
  return tmpMatrix;
}
