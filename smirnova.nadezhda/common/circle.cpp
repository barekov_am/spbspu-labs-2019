#include "circle.hpp"
#include <iostream>
#include <stdexcept>
#include <math.h>

smirnova::Circle::Circle(const point_t &pos, double radius):
  center_(pos),
  radius_(radius)
{
  if (radius_ <= 0.0)
  {
    throw std::invalid_argument("Radius must be > 0");
  }
}

double smirnova::Circle::getArea() const
{
  return (M_PI * radius_ * radius_);
}

smirnova::rectangle_t
smirnova::Circle::getFrameRect() const
{
  return {2 * radius_, 2 * radius_, center_};
}

void smirnova::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void smirnova::Circle::move(const point_t &pos)
{
  center_ = pos;
}

void smirnova::Circle::scale(const double coef)
{
  if (coef <= 0.0)
  {
    throw std::invalid_argument("Coefficient must be > 0");
  }
  radius_ *= coef;
}

void smirnova::Circle::rotate(double)
{
}
