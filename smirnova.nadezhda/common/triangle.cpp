#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

smirnova::Triangle::Triangle(const point_t pointA, const point_t pointB, const point_t pointC):
  pointA_(pointA),
  pointB_(pointB),
  pointC_(pointC),
  center_({(pointA.x + pointB.x + pointC.x)/3,(pointA.y + pointB.y + pointC.y)/3})
{
  if (getArea() <= 0)
  {
    throw std::invalid_argument("Area must be > 0");
  }
}

double smirnova::Triangle::getArea() const
{
  return 0.5 * std::fabs((pointB_.x - pointA_.x) * (pointC_.y - pointA_.y) - (pointC_.x - pointA_.x) * (pointB_.y - pointA_.y));
}

smirnova::rectangle_t
smirnova::Triangle::getFrameRect() const
{
  rectangle_t rect;
  rect.width = fmax(fmax(pointA_.x, pointB_.x), pointC_.x) - fmin(fmin(pointA_.x, pointB_.x), pointC_.x);
  rect.pos.x = (fmax(fmax(pointA_.x, pointB_.x), pointC_.x) + fmin(fmin(pointA_.x, pointB_.x), pointC_.x)) / 2;
  rect.height = fmax(fmax(pointA_.y, pointB_.y), pointC_.y) - fmin(fmin(pointA_.y, pointB_.y), pointC_.x);
  rect.pos.y = (fmax(fmax(pointA_.y, pointB_.y), pointC_.y) + fmin(fmin(pointA_.y, pointB_.y), pointC_.x)) / 2;
  return rect;
}

void smirnova::Triangle::move(double dx, double dy)
{
  pointA_.x += dx;
  pointA_.y += dy;
  pointB_.x += dx;
  pointB_.y += dy;
  pointC_.x += dx;
  pointC_.y += dy;
  center_.x += dx;
  center_.y += dy;
}

void smirnova::Triangle::move(const point_t &pos)
{
  double dx = pos.x - center_.x;
  double dy = pos.y - center_.y;
  move(dx, dy);
}

void smirnova::Triangle::scale(const double coef)
{
  if (coef <= 0.0)
  {
    throw std::invalid_argument("Coefficient must be > 0");
  }
  pointA_.x = center_.x + (pointA_.x - center_.x) * coef;
  pointA_.y = center_.y + (pointA_.y - center_.y) * coef;
  pointB_.x = center_.x + (pointB_.x - center_.x) * coef;
  pointB_.y = center_.y + (pointB_.y - center_.y) * coef;
  pointC_.x = center_.x + (pointC_.x - center_.x) * coef;
  pointC_.y = center_.y + (pointC_.y - center_.y) * coef;
}

void smirnova::Triangle::rotate(double angle)
{
  pointA_ = smirnova::rotatePoint(center_, pointA_, angle);
  pointB_ = smirnova::rotatePoint(center_, pointB_, angle);
  pointC_ = smirnova::rotatePoint(center_, pointC_, angle);
}
