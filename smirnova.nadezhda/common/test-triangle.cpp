#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(testForTriangle)

const double EPSILON = 0.01;

BOOST_AUTO_TEST_CASE(testOfImmutability)
{
  smirnova::Triangle testTriangle({4.5, -1.2}, {1.3, 3.5}, {5.6, 3.2});
  const double area = testTriangle.getArea();
  const double width = testTriangle.getFrameRect().width;
  const double height = testTriangle.getFrameRect().height;

  testTriangle.move({3.4, 4.3});
  BOOST_CHECK_CLOSE(area, testTriangle.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(width, testTriangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height, testTriangle.getFrameRect().height, EPSILON);

  testTriangle.move(6, -2.1);
  BOOST_CHECK_CLOSE(area, testTriangle.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(width, testTriangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height, testTriangle.getFrameRect().height, EPSILON);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterScaling)
{
  smirnova::Triangle testTriangle({4.5, -1.2}, {1.3, 3.5}, {5.6, 3.2});
  double area = testTriangle.getArea();
  double coef = 6.7;

  testTriangle.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, testTriangle.getArea(), EPSILON);

  area = testTriangle.getArea();
  coef = 0.8;

  testTriangle.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, testTriangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterRotation)
{
  smirnova::Triangle testTriangle({4.5, -1.2}, {1.3, 3.5}, {5.6, 3.2});
  const double area = testTriangle.getArea();
  const double angle = 23.6;

  testTriangle.rotate(angle);
  BOOST_CHECK_CLOSE(area, testTriangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  BOOST_CHECK_THROW(smirnova::Triangle({0, 0}, {2, 3.5}, {0, 0}), std::invalid_argument);
  smirnova::Triangle testTriangle({0.0, 0.0}, {6.3, -3.5}, {4.9, 3.5});
  BOOST_CHECK_THROW(testTriangle.scale(-3.5), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
