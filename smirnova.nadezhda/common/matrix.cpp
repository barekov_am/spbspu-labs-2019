#include "matrix.hpp"
#include <stdexcept>
#include <iostream>

smirnova::Matrix::Matrix() :
lines_(0),
columns_(0),
shapes_()
{
}

smirnova::Matrix::Matrix(const Matrix &newMatrix) :
lines_(newMatrix.lines_),
columns_(newMatrix.columns_),
shapes_(std::make_unique<CompositeShape::ptr[]>(newMatrix.lines_ * newMatrix.columns_))
{
  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    shapes_[i] = newMatrix.shapes_[i];
  }
}

smirnova::Matrix::Matrix(Matrix &&newMatrix) :
lines_(newMatrix.lines_),
columns_(newMatrix.columns_),
shapes_(std::move(newMatrix.shapes_))
{
  newMatrix.lines_ = 0;
  newMatrix.columns_ = 0;
}

smirnova::Matrix &smirnova::Matrix::operator =(const Matrix &newMatrix)
{
  if (this != &newMatrix)
  {
    lines_ = newMatrix.lines_;
    columns_ = newMatrix.columns_;
    CompositeShape::array_ tempMatrix(std::make_unique<CompositeShape::ptr[]>(newMatrix.lines_ * newMatrix.columns_));
    for (size_t i = 0; i < (lines_ * columns_); i++)
    {
      tempMatrix[i] = newMatrix.shapes_[i];
    }
    shapes_.swap(tempMatrix);
  }
  return *this;
}

smirnova::Matrix& smirnova::Matrix::operator =(Matrix &&newMatrix)
{
  if (this != &newMatrix)
  {
    lines_ = newMatrix.lines_;
    columns_ = newMatrix.columns_;
    shapes_ = std::move(newMatrix.shapes_);
    newMatrix.lines_ = 0;
    newMatrix.columns_ = 0;
  }
  return *this;
}

std::unique_ptr<std::shared_ptr<smirnova::Shape>[]> smirnova::Matrix::operator [](size_t index) const
{
  if (lines_ <= index)
  {
    throw std::out_of_range("Index is out of range");
  }

  CompositeShape::array_ tempMatrix(std::make_unique<std::shared_ptr<Shape>[]>(columns_));
  for (size_t i = 0; i < columns_; i++)
  {
    tempMatrix[i] = shapes_[index * columns_ + i];
  }
  return tempMatrix;
}

bool smirnova::Matrix::operator ==(const Matrix &newMatrix) const
{
  if ((lines_ != newMatrix.lines_) || (columns_ != newMatrix.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    if (shapes_[i] != newMatrix.shapes_[i])
    {
      return false;
    }
  }
  return true;
}

bool smirnova::Matrix::operator !=(const Matrix &newMatrix) const
{
  return !(*this == newMatrix);
}

size_t smirnova::Matrix::getLines() const
{
  return lines_;
}

size_t smirnova::Matrix::getColumns() const
{
  return columns_;
}

void smirnova::Matrix::add(std::shared_ptr<smirnova::Shape> shape, size_t line, size_t column)
{
  if (!shape)
  {
    throw std::invalid_argument("Shouldn't add nullptr");
  }

  size_t newLines = (line == lines_) ? (lines_ + 1) : (lines_);
  size_t newColumns = (column == columns_) ? (columns_ + 1) : (columns_);

  CompositeShape::array_ tempMatrix(std::make_unique<CompositeShape::ptr[]>(newLines * newColumns));
  for (size_t i = 0; i < newLines; i++)
  {
    for (size_t j = 0; j < newColumns; j++)
    {
      if ((i != lines_) && (j != columns_))
      {
        tempMatrix[i * newColumns + j] = shapes_[i * columns_ + j];
      }
      else
      {
        tempMatrix[i * newColumns + j] = nullptr;
      }
    }
  }

  tempMatrix[line * newColumns + column] = shape;
  shapes_.swap(tempMatrix);
  lines_ = newLines;
  columns_ = newColumns;
}
