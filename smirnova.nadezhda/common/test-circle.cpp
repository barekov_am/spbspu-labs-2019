#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testForCircle)

const double EPSILON = 0.01;

BOOST_AUTO_TEST_CASE(testOfImmutability)
{
  smirnova::Circle testCircle({7.2, 6.5}, 4.3);
  const double area = testCircle.getArea();
  const double width = testCircle.getFrameRect().width;
  const double height = testCircle.getFrameRect().height;

  testCircle.move({7.4, 4.3});
  BOOST_CHECK_CLOSE(area, testCircle.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(width, testCircle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height, testCircle.getFrameRect().height, EPSILON);

  testCircle.move(2.0, -2.1);
  BOOST_CHECK_CLOSE(area, testCircle.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(width, testCircle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height, testCircle.getFrameRect().height, EPSILON);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterScaling)
{
  smirnova::Circle testCircle({7.2, 6.5}, 4.3);
  double area = testCircle.getArea();
  double coef = 2.5;

  testCircle.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, testCircle.getArea(), EPSILON);

  area = testCircle.getArea();
  coef = 0.5;

  testCircle.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, testCircle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  BOOST_CHECK_THROW(smirnova::Circle({5.3, 3.4}, -5.3), std::invalid_argument);
  smirnova::Circle testCircle({2.3, 4.5}, 5.3);
  BOOST_CHECK_THROW(testCircle.scale(-3.5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterRotation)
{
  smirnova::Circle testCircle({7.2, 6.5}, 4.3);
  double area = testCircle.getArea();
  double angle = 90;

  testCircle.rotate(angle);
  BOOST_CHECK_CLOSE(area, testCircle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_SUITE_END();

