#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "polygon.hpp"

const double EPSILON = 0.01;

BOOST_AUTO_TEST_SUITE(testForPolygon)

BOOST_AUTO_TEST_CASE(testOfImmutability)
{
  const int count = 5;
  const smirnova::point_t points[count] = { {7, 3}, {4, 1}, {8, 5}, {2, 3}, {3, 4} };
  smirnova::Polygon tempPolygon(count, points);
  const double area = tempPolygon.getArea();
  smirnova::rectangle_t frameRect = tempPolygon.getFrameRect();

  tempPolygon.move({ 4.2, 5.4 });
  BOOST_CHECK_CLOSE(frameRect.width, tempPolygon.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, tempPolygon.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area, tempPolygon.getArea(), EPSILON);
  
  tempPolygon.move(1, 9);
  BOOST_CHECK_CLOSE(frameRect.width, tempPolygon.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, tempPolygon.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area, tempPolygon.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterScaling)
{
  const int count = 5;
  const smirnova::point_t points[count] = { {7, 3}, {4, 1}, {8, 5}, {2, 3}, {3, 4} };
  smirnova::Polygon tempPolygon(count, points);
  double area = tempPolygon.getArea();
  double coef = 3.6;

  tempPolygon.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, tempPolygon.getArea(), EPSILON);
  area = tempPolygon.getArea();
  coef = 0.6;
  tempPolygon.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, tempPolygon.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  const int count = 5;
  const smirnova::point_t points[count] = { {7, 3}, {4, 1}, {8, 5}, {2, 3}, {3, 4} };
  smirnova::Polygon tempPolygon(count, points);
  BOOST_CHECK_THROW(tempPolygon.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(tempPolygon.scale(-2.4), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
