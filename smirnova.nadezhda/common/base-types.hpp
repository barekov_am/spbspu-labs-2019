#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace smirnova
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    double width;
    double height;
    point_t pos;
  };
    
  smirnova::point_t rotatePoint(const smirnova::point_t& center, const smirnova::point_t& point, double angle);
}

#endif
