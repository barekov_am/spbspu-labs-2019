#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testPartition)

const double EPSILON = 0.01;

BOOST_AUTO_TEST_CASE(testOfOverlapingOfShapes)
{
  smirnova::Rectangle r1(5.0, 5.0, {1.0, 1.0});
  smirnova::Circle c1({-6.0, 2.0}, 2.0);
  smirnova::Rectangle r2(5.0, 5.0, {-3.0, 2.0});
  smirnova::Circle c2({-3.0, 1.0}, 2.0);

  BOOST_CHECK(smirnova::cross(r1, c2));
  BOOST_CHECK(smirnova::cross(r1, r2));
  BOOST_CHECK(smirnova::cross(r2, c1));
  BOOST_CHECK(smirnova::cross(r2, c2));
  BOOST_CHECK(!smirnova::cross(r1, c1));
}

BOOST_AUTO_TEST_CASE(testOfEmptyCompositePartition)
{
  smirnova::Matrix testMatrix = smirnova::part(smirnova::CompositeShape());

  BOOST_CHECK_EQUAL(testMatrix.getLines(), 0);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(correctCompositeSeparation)
{
  smirnova::Rectangle r1(3.1, 2.4, {-2.0, -2.0});
  smirnova::Circle c1({3.0, 2.0}, 1.0);
  smirnova::Rectangle r2(3.1, 2.4, {1.0, 2.0});
  smirnova::Rectangle r3(3.1, 2.4, {1.0, -4.0});

  smirnova::CompositeShape testShapes(std::make_shared<smirnova::Rectangle>(r1));
  testShapes.add(std::make_shared<smirnova::Circle>(c1));
  testShapes.add(std::make_shared<smirnova::Rectangle>(r2));
  testShapes.add(std::make_shared<smirnova::Rectangle>(r3));

  smirnova::Matrix testMatrix = smirnova::part(testShapes);

  BOOST_CHECK_EQUAL(testMatrix.getLines(), 2);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 2);
  BOOST_CHECK_EQUAL(testMatrix[0][0], testShapes[0]);
  BOOST_CHECK_EQUAL(testMatrix[0][1], testShapes[1]);
  BOOST_CHECK_EQUAL(testMatrix[1][0], testShapes[2]);
  BOOST_CHECK_EQUAL(testMatrix[1][1], testShapes[3]);
}

BOOST_AUTO_TEST_SUITE_END()
