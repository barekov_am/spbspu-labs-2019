#include <iostream>
#include "base-types.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

int main()
{
  smirnova::Rectangle rect(4.2, 5.1, {4.0, 5.5});
  smirnova::Shape * shape = &rect;

  std::cout << "Rectangle:\n";
  std::cout << "Area: " << shape->getArea() << '\n';
  smirnova::rectangle_t data = shape->getFrameRect();
  std::cout << "Width: " << data.width << '\t' << "Height: " << data.height << '\n';
  std::cout << "Position: " << data.pos.x << "; " << data.pos.y << '\n';

  shape->move(4.0, -2.7);
  shape->move({12.0, 16.2});
  shape->scale(1.8);
  data = shape->getFrameRect();
  std::cout << "Position after move: " << data.pos.x << "; " << data.pos.y << '\n';

  std::cout << "Circle:\n";
  smirnova::Circle c1({3.6, 4.1}, 2.0);
  shape = &c1;
  std::cout << "Area: " << shape->getArea() << '\n';
  data = shape->getFrameRect();
  std::cout << "Width: " << data.width << '\t' << "Height: " << data.height << '\n';
  std::cout << "Position: " << data.pos.x << "; "  << data.pos.y << '\n';

  shape->move({8.5, 4.2});
  shape->move(1.0, 1.0);
  shape->scale(2.5);
  data = shape->getFrameRect();
  std::cout << "Position after move: " << data.pos.x << "; " << data.pos.y << '\n';

  std::cout << "Triangle:\n";
  smirnova::Triangle t1({ 5,3 }, { 6,4 }, { 7,2 });
  shape = &t1;
  std::cout << "Area: " << shape->getArea() << '\n';
  data = shape->getFrameRect();
  std::cout << "Width: " << data.width << '\t' << "Height: " << data.height << '\n';
  std::cout << "Position: " << data.pos.x << "; "  << data.pos.y << '\n';
 
  shape->move(2.5, 2.5);
  shape->move({7.3, 1.9});
  shape->scale(0.8);
  data = shape->getFrameRect();
  std::cout << "Position after move: " << data.pos.x << "; " << data.pos.y << '\n';

  std::cout << "Polygon:\n";
  smirnova::point_t points[] = {{4, 2}, {3, 2}, {4, 8}, {2, 6}, {7, 1}};
  smirnova::Polygon p1(5, points);
  shape = &p1;
  std::cout << "Area: " << shape->getArea() << '\n';
  data = shape->getFrameRect();
  std::cout << "Width: " << data.width << '\t' << "Height: " << data.height << '\n';
  std::cout << "Position: " << data.pos.x << "; "  << data.pos.y << '\n';

  shape->move(8, 10);
  shape->move({7, 5});
  shape->scale(0.4);
  data = shape->getFrameRect();
  std::cout << "Position after move: " << data.pos.x << "; " << data.pos.y << '\n';
    
  std::cout << "Composite shape\n" << std::endl;
  smirnova::CompositeShape compShape(std::make_shared<smirnova::Rectangle>(rect));
  compShape.printInfo();
  std::cout << "New composite shape after adding: " << "\n";
  compShape.add(std::make_shared<smirnova::Circle>(c1));
  compShape.printInfo();
  std::cout << "New composite shape after adding: " << "\n";
  compShape.add(std::make_shared<smirnova::Triangle>(t1));
  compShape.printInfo();
  std::cout << "New composite shape after adding: " << "\n";
  compShape.add(std::make_shared<smirnova::Polygon>(p1));
  compShape.printInfo();
  std::cout << "Composite shape after scale" << "\n";
  compShape.scale(0.4);
  compShape.printInfo();
  std::cout << "Remove first element from array of shapes: " << "\n";
  compShape.remove(0);
  compShape.printInfo();

  return 0;
}
