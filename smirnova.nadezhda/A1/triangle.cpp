#include "triangle.hpp"
#include <iostream>
#include <cmath>
#include <cassert>
#include <algorithm>

Triangle::Triangle(const point_t pointA, const point_t pointB, const point_t pointC):
  pointA_(pointA),
  pointB_(pointB),
  pointC_(pointC),
  center_({(pointA.x + pointB.x + pointC.x)/3,(pointA.y + pointB.y + pointC.y)/3})
{
    assert(getArea() > 0);
}

double Triangle::getArea() const
{
  return 0.5 * std::fabs((pointB_.x - pointA_.x) * (pointC_.y - pointA_.y) - (pointC_.x - pointA_.x) * (pointB_.y - pointA_.y));
}

rectangle_t Triangle::getFrameRect() const
{
  rectangle_t rect;
  rect.width = fmax(fmax(pointA_.x, pointB_.x), pointC_.x) - fmin(fmin(pointA_.x, pointB_.x), pointC_.x);
  rect.pos.x = (fmax(fmax(pointA_.x, pointB_.x), pointC_.x) + fmin(fmin(pointA_.x, pointB_.x), pointC_.x)) / 2;
  rect.height = fmax(fmax(pointA_.y, pointB_.y), pointC_.y) - fmin(fmin(pointA_.y, pointB_.y), pointC_.x);
  rect.pos.y = (fmax(fmax(pointA_.y, pointB_.y), pointC_.y) + fmin(fmin(pointA_.y, pointB_.y), pointC_.x)) / 2;
  return rect;
}

void Triangle::move(double dx, double dy)
{
  pointA_.x += dx;
  pointA_.y += dy;
  pointB_.x += dx;
  pointB_.y += dy;
  pointC_.x += dx;
  pointC_.y += dy;
  center_.x += dx;
  center_.y += dy;
}

void Triangle::move(const point_t &pos)
{
  double dx = pos.x - center_.x;
  double dy = pos.y - center_.y;
  move(dx, dy);
}
