#include "rectangle.hpp"
#include <iostream>
#include <cassert>

Rectangle::Rectangle(double width, double height, const point_t &pos):
  width_(width),
  height_(height),
  center_(pos)
{
  assert((width_ > 0) && (height_ > 0));
}

double Rectangle::getArea() const
{
  return (width_ * height_);
}

rectangle_t Rectangle::getFrameRect() const
{
  return {width_, height_, center_};
}

void Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Rectangle::move(const point_t &pos)
{
  center_.x = pos.x;
  center_.y = pos.y;
}
