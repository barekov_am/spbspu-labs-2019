#include <iostream>
#include "base-types.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

int main()
{
    Rectangle rect(4.2, 5.1, {4.0, 5.5});
    Shape * shape = &rect;
    
    std::cout << "Rectangle:\n";
    std::cout << "Area: " << shape->getArea() << '\n';
    rectangle_t data = shape->getFrameRect();
    std::cout << "Width: " << data.width << '\t' << "Height: " << data.height << '\n';
    std::cout << "Position: " << data.pos.x << "; " << data.pos.y << '\n';
    
    shape->move(4.0, -2.7);
    shape->move({12.0, 16.2});
    data = shape->getFrameRect();
    std::cout << "Position after move: " << data.pos.x << "; " << data.pos.y << '\n';
    
    std::cout << "Circle:\n";
    Circle c1({3.6, 4.1}, 2.0);
    shape = &c1;
    std::cout << "Area: " << shape->getArea() << '\n';
    data = shape->getFrameRect();
    std::cout << "Width: " << data.width << '\t' << "Height: " << data.height << '\n';
    std::cout << "Position: " << data.pos.x << "; "  << data.pos.y << '\n';
    
    shape->move({8.5, 4.2});
    shape->move(1.0, 1.0);
    data = shape->getFrameRect();
    std::cout << "Position after move: " << data.pos.x << "; " << data.pos.y << '\n';
    
    std::cout << "Triangle:\n";
    Triangle t1({ 5,3 }, { 6,4 }, { 7,2 });
    shape = &t1;
    std::cout << "Area: " << shape->getArea() << '\n';
    data = shape->getFrameRect();
    std::cout << "Width: " << data.width << '\t' << "Height: " << data.height << '\n';
    std::cout << "Position: " << data.pos.x << "; "  << data.pos.y << '\n';
    
    shape->move(2.5, 2.5);
    shape->move({7.3, 1.9});
    data = shape->getFrameRect();
    std::cout << "Position after move: " << data.pos.x << "; " << data.pos.y << '\n';
    
    return 0;
}
