#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"


int main()
{

  pryanichnikova::Rectangle rectangle({ 12.0, 15.0 }, 10.0, 25.0);
  pryanichnikova::Circle circle({ 6.0, 7.0 }, 5.0);
  pryanichnikova::Triangle tria({11.0, -10.0}, {15.0, 4.0}, {12.0, 7.0});
  pryanichnikova::point_t mas[4] = {{1.0, 1.0}, {1.0, 5.0}, {2.0, 5.0}, {1.0, 4.0}};
  pryanichnikova::Polygon poly(4, mas);

  pryanichnikova::shapePtr rectanglePtr = std::make_shared<pryanichnikova::Rectangle>(rectangle);
  pryanichnikova::shapePtr circlePtr = std::make_shared<pryanichnikova::Circle>(circle);
  pryanichnikova::shapePtr trianglePtr = std::make_shared<pryanichnikova::Triangle>(tria);
  pryanichnikova::shapePtr polygonPtr = std::make_shared<pryanichnikova::Polygon>(poly);

  pryanichnikova::CompositeShape compositeShape;
  compositeShape.add(rectanglePtr);
  compositeShape.printData();

  compositeShape.add(circlePtr);
  std::cout << "After adding new figure: " << std::endl;
  compositeShape.printData();

  compositeShape.move(3.7, 1.0);
  std::cout << "Let's move the shape 3.7 units right and 1 units up : " << std::endl;
  compositeShape.printData();

  compositeShape.move({ 10.0, 2.3 });
  std::cout << "Let's move CompositeShape at the point (10, 2.3): " << std::endl;
  compositeShape.printData();

  compositeShape.scale(2.0);
  std::cout << "Let's scaling the shape by factor = 2: " << std::endl;
  compositeShape.printData();

  compositeShape.rotate(30.0);
  std::cout << "After 30 degree rotation: " << std::endl;
  compositeShape.printData();

  std::cout << "After deleting the first figure: " << std::endl;
  compositeShape.remove(0);
  compositeShape.printData();

  pryanichnikova::Rectangle rectangleNew({ 8.0, 6.0 },6.0, 2.0 );
  pryanichnikova::shapePtr rectangleNewPtr = std::make_shared<pryanichnikova::Rectangle>(rectangleNew);

  pryanichnikova::Circle circleNew({ 11.0, 5.0 }, 1.0);
  pryanichnikova::shapePtr circleNewPtr = std::make_shared<pryanichnikova::Circle>(circleNew);

  pryanichnikova::Triangle triaNew({ 14.0, -10.0 }, { 5.0, 4.0 }, { 12.0, 17.0 });
  pryanichnikova::shapePtr triangleNewPtr = std::make_shared<pryanichnikova::Triangle>(triaNew);

  pryanichnikova::point_t mas2[4] = { {3.0, 1.0}, {1.0, 2.0}, {2.0, 5.0}, {1.0, 4.0} };
  pryanichnikova::Polygon polyNew(4, mas2);
  pryanichnikova::shapePtr polygonNewPtr = std::make_shared<pryanichnikova::Polygon>(polyNew);

  compositeShape.add(rectangleNewPtr);
  compositeShape.add(circleNewPtr);
  compositeShape.add(triangleNewPtr);
  compositeShape.add(polygonNewPtr);

  pryanichnikova::Matrix matrix;
  matrix = pryanichnikova::part(compositeShape);
  matrix.printData();

  return 0;
}
