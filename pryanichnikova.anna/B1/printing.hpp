#ifndef B1_PRINTING_HPP
#define B1_PRINTING_HPP

#include <iostream>

template <template <typename Container> typename AccessMethod, typename Container>
void printContainer(AccessMethod<Container> method, Container& rhs)
{
  for (auto i = method.begin(rhs); i != method.end(rhs); i++)
  {
    std::cout << method.get(rhs, i) << " ";
  }
  std::cout << "\n";
}

#endif
