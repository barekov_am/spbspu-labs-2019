#include <iostream>
#include <ctime>

void task1(const char* order);
void task2(const char* file);
void task3();
void task4(const char* order, int size);

int main(int argc, char* argv[])
{
  srand(static_cast<unsigned int>(time(0)));
  try
  {
    if (argc <= 1)
    {
      std::cerr << "Not arguments";
      return 1;
    }

    char* end = nullptr;
    int task = strtol(argv[1], &end, 10);
    switch (task)
    {
      case 1:
      {
        if (argc != 3)
        {
          std::cerr << "Wrong number of arguments";
          return 1;
        }

       task1(argv[2]);
       break;
      }
      case 2:
      {
        if (argc != 3)
        {
          std::cerr << "Wrong number of arguments";
          return 1;
        }

        task2(argv[2]);
        break;
      }
      case 3:
      {
        if (argc != 2)
        {
          std::cerr << "Wrong number of arguments";
          return 1;
        }

        task3();
        break;
      }
      case 4:
      {
        if (argc != 4)
          {
            std::cerr << "Wrong number of arguments";
            return 1;
          }

        int size = strtol(argv[3], &end, 10);
        task4(argv[2], size);
        break;
        }
      default:
      {
        std::cerr << "Wrong number of task";
        return 1;
      }
    }
  }
  catch (const std::exception & e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}
