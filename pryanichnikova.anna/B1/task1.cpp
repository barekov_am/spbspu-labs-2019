#include "accessMethod.hpp"
#include "sorting.hpp"
#include "printing.hpp"

#include <forward_list>
#include <vector>

void task1(const char* order)
{
  checkOrder(order);

  std::vector<int> vector;
  std::vector<int> vector1;

  int value;
  while (std::cin >> value)
  {
    vector.push_back(value);
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::ios_base::failure("Reading failed");
  }

  if (vector.empty())
  {
    return;
  }

  vector1 = vector;
  std::forward_list<int> list(vector.begin(), vector.end());

  sorting(accessBrackets<std::vector<int>>(), vector, order);
  printContainer(accessBrackets<std::vector<int>>(), vector);

  sorting(accessAt<std::vector<int>>(), vector1, order);
  printContainer(accessAt<std::vector<int>>(), vector1);

  sorting(accessIterator<std::forward_list<int>>(), list, order);
  printContainer(accessIterator<std::forward_list<int>>(), list);
}
