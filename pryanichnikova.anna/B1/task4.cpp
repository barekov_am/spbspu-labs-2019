#include "accessMethod.hpp"
#include "sorting.hpp"
#include "printing.hpp"

#include <random>
#include <vector>

void fillRandom(double* array, int size)
{
  for (int i = 0; i < size; i++)
  {
    array[i] = ((rand() % 21) / 10.0) - 1;
  }
}

void task4(const char* order, int size)
{
  checkOrder(order);
  
  if (size < 0)
  {
    throw std::invalid_argument("Size of array must be positive");
  }

  if (!size)
  {
    throw std::invalid_argument("Size must be a number");
  }

  double* arr = new double[size];
  fillRandom(arr, size);

  std::vector<double> vector;
  for (int i = 0; i != size; i++)
  {
    vector.push_back(arr[i]);
  }

  delete[] arr;
  
  printContainer(accessAt<std::vector<double>>(), vector);
  sorting(accessAt<std::vector<double>>(), vector, order);
  printContainer(accessAt<std::vector<double>>(), vector);
}
