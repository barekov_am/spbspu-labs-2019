#include "circle.hpp"
#include <iostream>
#include <cmath>

Circle::Circle(const point_t &centre, double radius) :
  centre_(centre),
  radius_(radius)
{
  if (radius <= 0)
  {
    throw std::invalid_argument("Invalid circle's parameters");
  }
}

void Circle::move(const point_t &centre)
{
  centre_ = centre;
}

void Circle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

double Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

rectangle_t Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, centre_};
}

void Circle::printData() const
{
  std::cout << "ABOUT CIRCLE \n";
  std::cout << "Centre : (" << centre_.x << ", " << centre_.y << ")\n";
  std::cout << "Area : " << getArea() << "\n";
  std::cout << "Width : " << getFrameRect().width << " Height : " << getFrameRect().height << "\n\n";
}
