#ifndef POLYGON_HPP
#define POLYGON_HPP
#include "shape.hpp"

class Polygon : public Shape
{
public:
  Polygon(const Polygon & value);
  Polygon(Polygon && value);
  Polygon(int quantity, point_t * cord);
  ~Polygon() override;
  Polygon & operator = (const Polygon & value);
  Polygon & operator = (Polygon && value);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &point) override;
  void move(double dx, double dy) override;
  point_t getCentre() const;
  bool checkConvex() const;
  void printData() const override;
private:
  int quantity_;
  point_t * cord_;
};
#endif
