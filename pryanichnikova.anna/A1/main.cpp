#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

int main()
{
  Rectangle rect({1, 1}, 10, 7);
  Circle circ({2, 2}, 15);
  Triangle tria({10, 10}, {20, 20}, {20, 10});
  point_t mas[5] = {{9, 1}, {1, 1}, {2, 3}, {1, 6}, {4, 3}};
  Polygon poly(5, mas);
  Shape * shapes[4] = {&rect, &circ, &tria, &poly};
  for (Shape * shape : shapes) 
  {
    shape->printData();
  }
  rect.move(5, 6);
  circ.move(5, 6);
  tria.move(5, 6);
  poly.move(5, 6);
  std::cout << "Location after moving\n\n";
  for (Shape * shape : shapes) 
  {
    shape->printData();
  }
  return 0;
}
