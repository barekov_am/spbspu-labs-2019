#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

class Rectangle : public Shape
{
public:
  Rectangle(const point_t &centre_p, double width, double height);
  void move(const point_t &centre) override;
  void move(double dx, double dy) override;
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void printData() const override;
private:
  point_t centre_;
  double width_;
  double height_; 
};
#endif
