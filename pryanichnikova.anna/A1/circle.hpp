#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

class Circle : public Shape
{
public:
  Circle(const point_t &centre, double radius);
  void move(const point_t &centre) override;
  void move(double dx, double dy) override;
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void printData() const override;
private:
  point_t centre_;  
  double radius_;
};
#endif
