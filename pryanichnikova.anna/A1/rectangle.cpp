#include "rectangle.hpp"
#include <iostream>

Rectangle::Rectangle(const point_t &centre, double width, double height):
  centre_(centre),
  width_(width),
  height_(height)  
{
  if ((width <= 0) || (height <= 0))
  {
    throw std::invalid_argument("Invalid rectangle's parameters");
  }
}

void Rectangle::move(const point_t &centre)
{
  centre_ = centre;
}

void Rectangle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return {width_, height_, centre_};
}

void Rectangle::printData() const
{
  std::cout << "ABOUT RECTANGLE\n";
  std::cout << "Centre : (" << centre_.x << ", " << centre_.y << ")\n";
  std::cout << "Area : " << getArea() << "\n";
  std::cout << "Width : " << getFrameRect().width << " Height : " << getFrameRect().height << "\n\n";
}
