#include "polygon.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>

Polygon::Polygon(const Polygon & value) :
  quantity_(value.quantity_),
  cord_(new point_t[quantity_])
{
  for (int i = 0; i < quantity_; i++)
  {
    cord_[i] = value.cord_[i];
  }
}

Polygon::Polygon(Polygon && value) :
  quantity_(value.quantity_),
  cord_(value.cord_)
{
  value.cord_ = nullptr;
  value.quantity_ = 0;
}

Polygon::Polygon(int quantity, point_t * cord) :
  quantity_(quantity),
  cord_(new point_t[quantity])
{
  if (quantity_ < 3)
  {
    delete[] cord_;
    throw std::invalid_argument("Invalid polygon's parameters");
  }
  if (cord_ == nullptr)
  {
    delete[] cord_;
    throw std::invalid_argument("Pointer to points mustn't be nullptr");
  }

  for (int i = 0; i < quantity_; i++)
  {
    cord_[i] = cord[i];
  }
  if (getArea() <= 0)
  {
    delete[] cord_;
    throw std::invalid_argument("Invalid polygon's area");
  }
  if (!checkConvex())
  {
    delete[] cord_;
    throw std::invalid_argument("Polygon must be convex");
  }
}

Polygon::~Polygon()
{
  delete[] cord_;
}

Polygon & Polygon::operator =(const Polygon & value)
{
  if (this == &value)
  {
    return *this;
  }
  quantity_ = value.quantity_;
  delete[] cord_;
  cord_ = new point_t[quantity_];
  for (int i = 0; i < quantity_; i++)
  {
    cord_[i] = value.cord_[i];
  }
  return *this;
}

Polygon & Polygon::operator =(Polygon && value)
{
  if (this == &value)
  {
    return *this;
  }
  quantity_ = value.quantity_;
  value.quantity_ = 0;
  if (cord_ != nullptr)
  {
    delete[] cord_;
  }
  cord_ = value.cord_;
  value.cord_ = nullptr;
  return *this;
}

double Polygon::getArea() const
{
  double area_poly = 0;
  for (int i = 0; i < quantity_ - 1; i++)
  {
    area_poly += cord_[i].x * cord_[i + 1].y;
    area_poly -= cord_[i + 1].x * cord_[i].y;
  }
  area_poly += cord_[quantity_ - 1].x * cord_[0].y;
  area_poly -= cord_[0].x * cord_[quantity_ - 1].y;
  return (std::fabs(area_poly) /  2);
}

rectangle_t Polygon::getFrameRect() const
{
  double maxX = cord_[0].x;
  double minX = cord_[0].x;
  double maxY = cord_[0].y;
  double minY = cord_[0].y;
  for (int i = 1; i < quantity_; i++)
  {
    maxX = std::max(maxX, cord_[i].x);
    minX = std::min(minX, cord_[i].x);
    maxY = std::max(maxY, cord_[i].y);
    minY = std::min(minY, cord_[i].y);
  }
  const double height = (maxY - minY);
  const double width = (maxX - minX);
  const point_t centre = {minX + (width / 2), minY + (height / 2)};
  return {width, height, centre};
}

void Polygon::move(const point_t &pos)
{
  const point_t center = getCentre();
  move(pos.x - center.x, pos.y - center.y);
}

void Polygon::move(double dx, double dy)
{
  for (int i = 0; i < quantity_; i++)
  {
    cord_[i].x += dx;
    cord_[i].y += dy;
  }
}

point_t Polygon::getCentre() const
{
  point_t center = { 0, 0 };
  for (int i = 0; i < quantity_; i++)
  {
    center.x += cord_[i].x / quantity_;
    center.y += cord_[i].y / quantity_;
  }
  return center;
}

bool Polygon::checkConvex() const
{
  point_t cordA = {0, 0};
  point_t cordB = {0, 0};
  for (int i = 0; i <= quantity_ - 2; i++)
  {
    cordA.x = cord_[i + 1].x - cord_[i].x;
    cordA.y = cord_[i + 1].y - cord_[i].y;
    if (i == quantity_ - 2)
    {
      cordB.x = cord_[0].x - cord_[i + 1].x;
      cordB.y = cord_[0].y - cord_[i + 1].y;
    }
    else
    {
      cordB.x = cord_[i + 2].x - cord_[i + 1].x;
      cordB.y = cord_[i + 2].y - cord_[i + 1].y;
    }
    if ((cordA.x * cordB.y) - (cordA.y * cordB.x) < 0)
    {
    return true;
    }
  }
  return false;
}

void Polygon::printData() const
{
  const point_t center_ = getCentre();
  std::cout << "ABOUT POLYGON:\n";
  std::cout << "Center : (" << center_.x << ", " << center_.y << ")\n";
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "Width: " << getFrameRect().width << "  Height: " << getFrameRect().height <<"\n\n";
}
