#include "triangle.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>

Triangle::Triangle(const point_t &cordA, const point_t &cordB, const point_t &cordC) :
  cordA_(cordA),
  cordB_(cordB),
  cordC_(cordC),
  centre_{(cordA.x + cordB.x + cordC.x) / 3, (cordA.y + cordB.y + cordC.y) / 3}
{
  if (getArea() == 0)
  {
    throw std::invalid_argument("Invalid trianle's area");
  }
}

void Triangle::move(const point_t &centre)
{
  move(centre.x - centre_.x, centre.y - centre_.y);
}

void Triangle::move(double dx, double dy)
{
  cordA_.x += dx;
  cordB_.x += dx;
  cordC_.x += dx;
  cordA_.y += dy;
  cordB_.y += dy;
  cordC_.y += dy;
  centre_.x += dx;
  centre_.y += dy;
}

double Triangle::getArea() const
{
  double sideA = sqrt(pow(cordA_.x - cordB_.x, 2) + pow(cordA_.y - cordB_.y, 2));
  double sideB = sqrt(pow(cordB_.x - cordC_.x, 2) + pow(cordB_.y - cordC_.y, 2));
  double sideC = sqrt(pow(cordC_.x - cordA_.x, 2) + pow(cordC_.y - cordA_.y, 2));
  double p = (sideA + sideB + sideC) / 2;
  return sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
}

rectangle_t Triangle::getFrameRect() const
{
  const double maxX = std::max(std::max(cordA_.x, cordB_.x), cordC_.x);
  const double minX = std::min(std::min(cordA_.x, cordB_.x), cordC_.x);
  const double maxY = std::max(std::max(cordA_.y, cordB_.y), cordC_.y);
  const double minY = std::min(std::min(cordA_.y, cordB_.y), cordC_.y);
  const double width = maxX - minX;
  const double height = maxY - minY;
  const point_t centre = {minX + width / 2, minY + height / 2};
  return {width, height, centre};
}

void Triangle::printData() const
{
  std::cout << "ABOUT TRIANGLE\n";
  std::cout << "Centre : (" << centre_.x << ", " << centre_.y << ")\n";
  std::cout << "Area : " << getArea() << "\n";
  std::cout << "Width : " << getFrameRect().width << " Height : " << getFrameRect().height << "\n\n";
}
