#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

class Triangle : public Shape
{
public:
  Triangle(const point_t &cordA, const point_t &cordB, const point_t &cordC);
  void move(const point_t &centre) override;
  void move(double dx, double dy) override;
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void printData() const override;
private:
  point_t cordA_;
  point_t cordB_;
  point_t cordC_;
  point_t centre_;
};
#endif
