#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>



BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithCrossing)
{
  const size_t layersValue = 2;
  const size_t columnsValue = 1;

  pryanichnikova::Rectangle testRectangle({ 5.0, 5.0 }, 4.0, 2.0);
  pryanichnikova::Circle testCircle({ 5.0, 5.0 }, 2.0);
  pryanichnikova::shapePtr rectanglePtr = std::make_shared<pryanichnikova::Rectangle>(testRectangle);
  pryanichnikova::shapePtr circlePtr = std::make_shared<pryanichnikova::Circle>(testCircle);

  pryanichnikova::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);
  pryanichnikova::Matrix testMatrix = pryanichnikova::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithoutCrossing)
{
  const size_t layersValue = 2;
  const size_t columnsValue = 1;

  pryanichnikova::Rectangle testRectangle({ 5.0, 5.0 }, 4.0, 2.0);
  pryanichnikova::Circle testCircle({ 5.0, 5.0 }, 2.0);
  pryanichnikova::shapePtr rectanglePtr = std::make_shared<pryanichnikova::Rectangle>(testRectangle);
  pryanichnikova::shapePtr circlePtr = std::make_shared<pryanichnikova::Circle>(testCircle);

  pryanichnikova::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  pryanichnikova::Matrix testMatrix = pryanichnikova::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  pryanichnikova::Rectangle testRectangle({ 5.0, 5.0 }, 4.0, 2.0);
  pryanichnikova::Circle testCircle({ 5.0, 5.0 }, 2.0);
  pryanichnikova::shapePtr rectanglePtr = std::make_shared<pryanichnikova::Rectangle>(testRectangle);
  pryanichnikova::shapePtr circlePtr = std::make_shared<pryanichnikova::Circle>(testCircle);

  pryanichnikova::CompositeShape testCompositeShape(rectanglePtr);
  
  testCompositeShape.add(circlePtr);

  pryanichnikova::Matrix testMatrix = pryanichnikova::part(testCompositeShape);

  BOOST_CHECK_THROW(testMatrix[5][0], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-5][0], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkCopyAndMove)
{
  pryanichnikova::Rectangle testRectangle({ 5.0, 5.0 }, 4.0, 2.0);
  pryanichnikova::Circle testCircle({ 5.0, 5.0 }, 2.0);
  pryanichnikova::shapePtr rectanglePtr = std::make_shared<pryanichnikova::Rectangle>(testRectangle);
  pryanichnikova::shapePtr circlePtr = std::make_shared<pryanichnikova::Circle>(testCircle);

  pryanichnikova::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  pryanichnikova::Matrix testMatrix = pryanichnikova::part(testCompositeShape);
  pryanichnikova::Matrix copyMatrix(testMatrix);
  pryanichnikova::Matrix moveMatrix(std::move(testMatrix));

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyMatrix);
}

BOOST_AUTO_TEST_SUITE_END()
