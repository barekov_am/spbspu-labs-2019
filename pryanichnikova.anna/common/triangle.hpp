#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

namespace pryanichnikova
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &cordA, const point_t &cordB, const point_t &cordC);
    void move(const point_t &centre) override;
    void move(double dx, double dy) override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void scale(double scaleCoef) override;
    void rotate(const double) override;
    point_t rotate(const point_t &, double) const;

    void printData() const override;
  private:
    point_t pointA_;
    point_t pointB_;
    point_t pointC_;
    point_t centre_;
    double angle_;
  };
}
#endif
