#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteShapes)

const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(RectangleMoveDxy)
{
  pryanichnikova::Rectangle rectangle({ 5.0, 6.0 }, 7.0, 8.0);
  const double widthBefore = rectangle.getFrameRect().width;
  const double heightBefore = rectangle.getFrameRect().height;
  const double areaBefore = rectangle.getArea();
  rectangle.move(7, 8);
  BOOST_CHECK_CLOSE(widthBefore, rectangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(heightBefore, rectangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(RectangleMovePoint)
{
  pryanichnikova::Rectangle rectangle({ 9.0, 10.0 }, 11.0, 12.0);
  const double widthBefore = rectangle.getFrameRect().width;
  const double heightBefore = rectangle.getFrameRect().height;
  const double areaBefore = rectangle.getArea();
  rectangle.move({ 13, 14 });
  BOOST_CHECK_CLOSE(widthBefore, rectangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(heightBefore, rectangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), EPSILON);
}


BOOST_AUTO_TEST_CASE(scalingRectangle)
{
  pryanichnikova::Rectangle rectangle({ 5.0, 5.0 }, 3.5, 6.5);
  const double areaBefore = rectangle.getArea();
  const double scaleCoef = 3.0;
  rectangle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(rectangle.getArea(), areaBefore * scaleCoef * scaleCoef, EPSILON);
}

BOOST_AUTO_TEST_CASE(validRectangle)
{
  BOOST_CHECK_THROW(pryanichnikova::Rectangle({ 1, 1 }, -2, 5), std::invalid_argument);
  BOOST_CHECK_THROW(pryanichnikova::Rectangle({ 1, 1 }, 2, -5), std::invalid_argument);

  pryanichnikova::Rectangle rectangle({ 2, 2 }, 2, 3);
  BOOST_CHECK_THROW(rectangle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rotationRectangle)
{
  pryanichnikova::Rectangle rectangle({ 1.0, 1.0 }, 1.0, 5.0);
  const double testArea = rectangle.getArea();
  const double angle = 90;
  
  rectangle.rotate(angle);

  BOOST_CHECK_CLOSE(testArea, rectangle.getArea(), EPSILON);

}


BOOST_AUTO_TEST_SUITE_END()
