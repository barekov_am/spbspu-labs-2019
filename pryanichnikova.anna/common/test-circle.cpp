#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteShapes)

const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(CircleMoveDxy)
{
  pryanichnikova::Circle circle({ 10, 9 }, 8);
  const double widthBefore = circle.getFrameRect().width;
  const double heightBefore = circle.getFrameRect().height;
  const double areaBefore = circle.getArea();
  circle.move(7, 0);
  BOOST_CHECK_CLOSE(widthBefore, circle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(heightBefore, circle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(CircleMovePoint)
{
  pryanichnikova::Circle circle({ 6, 5 }, 4);
  const double widthBefore = circle.getFrameRect().width;
  const double heightBefore = circle.getFrameRect().height;
  const double areaBefore = circle.getArea();
  circle.move({ 3, 2 });
  BOOST_CHECK_CLOSE(widthBefore, circle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(heightBefore, circle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(scalingCircle)
{
  pryanichnikova::Circle circle({ 5, 5 }, 6.5);
  const double areaBefore = circle.getArea();

  const double scaleCoef = 3;
  circle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(circle.getArea(), areaBefore * scaleCoef * scaleCoef, EPSILON);
}

BOOST_AUTO_TEST_CASE(validCircle)
{
  BOOST_CHECK_THROW(pryanichnikova::Circle({ 2, 6 }, -5), std::invalid_argument);

  pryanichnikova::Circle circle({ 2, 7 }, 6.5);
  BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rotationCircle)
{
  pryanichnikova::Circle circle({ 2, 7 }, 6.5);
  const double testArea = circle.getArea();
  const double angle = 90;

  circle.rotate(angle);

  BOOST_CHECK_CLOSE(testArea, circle.getArea(), EPSILON);

}


BOOST_AUTO_TEST_SUITE_END()
