#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace pryanichnikova
{
  bool cross(const rectangle_t &shape1, const rectangle_t &shape2);
  Matrix part(const dynamicArray &arr, size_t size);
  Matrix part(const CompositeShape &composite);
}

#endif //PARTITION_HPP
