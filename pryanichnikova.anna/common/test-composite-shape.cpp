#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"


BOOST_AUTO_TEST_SUITE(CompositeShapeTests)

const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(CompositeShapeMoveDxy)
{
  pryanichnikova::Circle circle({ 10, 9 }, 8);
  pryanichnikova::Rectangle rectangle({ 5, 6 }, 7, 8);
  pryanichnikova::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  pryanichnikova::point_t *cord = new pryanichnikova::point_t[4]{ {4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0} };
  pryanichnikova::Polygon polygon(4, cord);
  delete[] cord;
  pryanichnikova::shapePtr rectanglePtr = std::make_shared<pryanichnikova::Rectangle>(rectangle);
  pryanichnikova::shapePtr circlePtr = std::make_shared<pryanichnikova::Circle>(circle);
  pryanichnikova::shapePtr trianglePtr = std::make_shared<pryanichnikova::Triangle>(triangle);
  pryanichnikova::shapePtr polygonPtr = std::make_shared<pryanichnikova::Polygon>(polygon);
  pryanichnikova::CompositeShape compositeShape(rectanglePtr);
  compositeShape.add(circlePtr);
  compositeShape.add(trianglePtr);
  compositeShape.add(polygonPtr);

  const pryanichnikova::rectangle_t FrameRect = compositeShape.getFrameRect();
  const double areaBefore = compositeShape.getArea();
  compositeShape.move(10.0, 10.0);
  BOOST_CHECK_CLOSE(FrameRect.width, compositeShape.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(FrameRect.height, compositeShape.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, compositeShape.getArea(), EPSILON);

}

BOOST_AUTO_TEST_CASE(CompositeShapeMovePoint)
{
  pryanichnikova::Circle circle({ 10, 9 }, 8);
  pryanichnikova::Rectangle rectangle({ 5, 6 }, 7, 8);
  pryanichnikova::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  pryanichnikova::point_t *cord = new pryanichnikova::point_t[4]{ {4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0} };
  pryanichnikova::Polygon polygon(4, cord);
  delete[] cord;
  pryanichnikova::shapePtr rectanglePtr = std::make_shared<pryanichnikova::Rectangle>(rectangle);
  pryanichnikova::shapePtr circlePtr = std::make_shared<pryanichnikova::Circle>(circle);
  pryanichnikova::shapePtr trianglePtr = std::make_shared<pryanichnikova::Triangle>(triangle);
  pryanichnikova::shapePtr polygonPtr = std::make_shared<pryanichnikova::Polygon>(polygon);
  pryanichnikova::CompositeShape compositeShape(rectanglePtr);
  compositeShape.add(circlePtr);
  compositeShape.add(trianglePtr);
  compositeShape.add(polygonPtr);

  const pryanichnikova::rectangle_t FrameRect = compositeShape.getFrameRect();
  const double areaBefore = compositeShape.getArea();
  compositeShape.move({ 50.0, 50.0 });
  BOOST_CHECK_CLOSE(FrameRect.width, compositeShape.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(FrameRect.height, compositeShape.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, compositeShape.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(CompositeShapeScale)
{
  pryanichnikova::Circle circle({ 10, 9 }, 8);
  pryanichnikova::Rectangle rectangle({ 5, 6 }, 7, 8);
  pryanichnikova::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  pryanichnikova::point_t *cord = new pryanichnikova::point_t[4]{ {4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0} };
  pryanichnikova::Polygon polygon(4, cord);
  delete[] cord;
  pryanichnikova::shapePtr rectanglePtr = std::make_shared<pryanichnikova::Rectangle>(rectangle);
  pryanichnikova::shapePtr circlePtr = std::make_shared<pryanichnikova::Circle>(circle);
  pryanichnikova::shapePtr trianglePtr = std::make_shared<pryanichnikova::Triangle>(triangle);
  pryanichnikova::shapePtr polygonPtr = std::make_shared<pryanichnikova::Polygon>(polygon);
  pryanichnikova::CompositeShape compositeShape(rectanglePtr);
  compositeShape.add(circlePtr);
  compositeShape.add(trianglePtr);
  compositeShape.add(polygonPtr);
  const double areaBefore = compositeShape.getArea();
  const double k = 9;
  compositeShape.scale(9);
  BOOST_CHECK_CLOSE(areaBefore * k * k, compositeShape.getArea(), EPSILON);

}


BOOST_AUTO_TEST_CASE(errValues)
{
  pryanichnikova::Circle circle({ 10, 9 }, 8);
  pryanichnikova::Rectangle rectangle({ 5, 6 }, 7, 8);
  pryanichnikova::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  pryanichnikova::point_t *cord = new pryanichnikova::point_t[4]{ {4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0} };
  pryanichnikova::Polygon polygon(4, cord);
  delete[] cord;
  pryanichnikova::shapePtr rectanglePtr = std::make_shared<pryanichnikova::Rectangle>(rectangle);
  pryanichnikova::shapePtr circlePtr = std::make_shared<pryanichnikova::Circle>(circle);
  pryanichnikova::shapePtr trianglePtr = std::make_shared<pryanichnikova::Triangle>(triangle);
  pryanichnikova::shapePtr polygonPtr = std::make_shared<pryanichnikova::Polygon>(polygon);
  pryanichnikova::CompositeShape compositeShape(rectanglePtr);
  compositeShape.add(circlePtr);
  compositeShape.add(trianglePtr);
  compositeShape.add(polygonPtr);

  BOOST_CHECK_THROW(compositeShape.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.remove(5), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  pryanichnikova::Circle circle({ 10.0, 9.0 }, 8.0);
  pryanichnikova::Rectangle rectangle({ 5.0, 6.0 }, 7.0, 8.0);
  pryanichnikova::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  pryanichnikova::point_t *cord = new pryanichnikova::point_t[4]{ {4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0} };
  pryanichnikova::Polygon polygon(4, cord);
  delete[] cord;
  pryanichnikova::shapePtr rectanglePtr = std::make_shared<pryanichnikova::Rectangle>(rectangle);
  pryanichnikova::shapePtr circlePtr = std::make_shared<pryanichnikova::Circle>(circle);
  pryanichnikova::shapePtr trianglePtr = std::make_shared<pryanichnikova::Triangle>(triangle);
  pryanichnikova::shapePtr polygonPtr = std::make_shared<pryanichnikova::Polygon>(polygon);
  pryanichnikova::CompositeShape compositeShape(rectanglePtr);
  compositeShape.add(circlePtr);
  compositeShape.add(trianglePtr);
  compositeShape.add(polygonPtr);

  const double testArea = compositeShape.getArea();
  const double angle = 90;

  compositeShape.rotate(angle);

  BOOST_CHECK_CLOSE(testArea, compositeShape.getArea(), EPSILON);

}

BOOST_AUTO_TEST_SUITE_END()
