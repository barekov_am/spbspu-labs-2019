#define _USE_MATH_DEFINES
#include "circle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>

pryanichnikova::Circle::Circle(const point_t &centre, double radius) :
  centre_(centre),
  radius_(radius)
{
  if (radius <= 0.0)
  {
    throw std::invalid_argument("Invalid circle's parameters");
  }
}

void pryanichnikova::Circle::move(const point_t &centre)
{
  centre_ = centre;
}

void pryanichnikova::Circle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

double pryanichnikova::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

pryanichnikova::rectangle_t pryanichnikova::Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, centre_};
}

void pryanichnikova::Circle::scale(double scaleCoef)
{
  if (scaleCoef <= 0.0)
  {
    throw std::invalid_argument("Enter scaleCoef correctly");
  }
  else
  {
    radius_ *= scaleCoef;
  }
}

void pryanichnikova::Circle::rotate(double)
{}

void pryanichnikova::Circle::printData() const
{
  std::cout << "ABOUT CIRCLE \n";
  std::cout << "Centre : (" << centre_.x << ", " << centre_.y << ")\n";
  std::cout << "Area : " << getArea() << "\n";
  std::cout << "Width : " << getFrameRect().width << " Height : " << getFrameRect().height << "\n\n";
}
