#define _USE_MATH_DEFINES
#include "rectangle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>

const double FullAngle = 360.0;

pryanichnikova::Rectangle::Rectangle(const point_t &centre, double width, double height):
  centre_(centre),
  width_(width),
  height_(height),
  angle_(0)
{
  if ((width <= 0.0) || (height <= 0.0))
  {
    throw std::invalid_argument("Invalid rectangle's parameters");
  }
}

void pryanichnikova::Rectangle::move(const point_t &centre)
{
  centre_ = centre;
}

void pryanichnikova::Rectangle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

double pryanichnikova::Rectangle::getArea() const
{
  return width_ * height_;
}

pryanichnikova::rectangle_t pryanichnikova::Rectangle::getFrameRect() const
{
  const double radAngle = angle_ * M_PI / 180;
  const double width = width_ * fabs(cos(radAngle)) + height_ * fabs(sin(radAngle));
  const double height = width_ * fabs(sin(radAngle)) + height_ * fabs(cos(radAngle));

  return { width, height, centre_ };

}

void pryanichnikova::Rectangle::scale(double scaleCoef)
{
  if (scaleCoef <= 0.0)
  {
    throw std::invalid_argument("Invalid parametr, multiplier must be more then 0");
  }
  height_ *= scaleCoef;
  width_ *= scaleCoef;
}

void pryanichnikova::Rectangle::rotate(const double angle)
{
  angle_ += angle;
}

void pryanichnikova::Rectangle::printData() const
{
  std::cout << "ABOUT RECTANGLE\n";
  std::cout << "Centre : (" << centre_.x << ", " << centre_.y << ")\n";
  std::cout << "Area : " << getArea() << "\n";
  std::cout << "Width : " << getFrameRect().width << " Height : " << getFrameRect().height << "\n\n";
}
