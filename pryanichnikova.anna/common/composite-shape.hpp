#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include <cstdlib>
#include "shape.hpp"

namespace pryanichnikova
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape& other);
    CompositeShape(CompositeShape&& other) noexcept;
    CompositeShape(const shapePtr &shape);
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape& other);
    CompositeShape& operator =(CompositeShape&& other) noexcept;
    shapePtr operator [](size_t i) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &pos) override;
    void move(double dx, double dy) override;
    void scale(double factor) override;
    void printData() const override;
    void rotate(double angle) override;

    int size() const;
    void add(const shapePtr &other);
    void remove(size_t index);
    dynamicArray getList() const;

  private:
    size_t size_;
    dynamicArray array_;
    double angle_;
  };
}

#endif //COMPOSITE_SHAPE_HPP
