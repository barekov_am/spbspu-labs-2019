#ifndef SHAPE_HPP
#define SHAPE_HPP
#include "base-types.hpp"
#include <memory>

namespace pryanichnikova
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t &centre_p) = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void scale(const double scaleCoef) = 0;
    virtual void rotate(const double) = 0;
    virtual void printData() const = 0;

  };
  using shapePtr = std::shared_ptr<Shape>;
  using dynamicArray = std::unique_ptr<shapePtr[]>;

}
#endif
