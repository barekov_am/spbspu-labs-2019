#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteShapes)

const double EPSILON = 0.001;


BOOST_AUTO_TEST_CASE(TriangleMoveDxy)
{
  pryanichnikova::Triangle triangle({ 29, 30 }, { 20,30 }, { 15,25 });
  const double widthBefore = triangle.getFrameRect().width;
  const double heightBefore = triangle.getFrameRect().height;
  const double areaBefore = triangle.getArea();
  triangle.move(5, 10);
  BOOST_CHECK_CLOSE(widthBefore, triangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(heightBefore, triangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, triangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(TriangleMovePoint)
{
  pryanichnikova::Triangle triangle({ 23, 27 }, { 30, 24 }, { 60, 90 });
  const double widthBefore = triangle.getFrameRect().width;
  const double heightBefore = triangle.getFrameRect().height;
  const double areaBefore = triangle.getArea();
  triangle.move({ 20, -25 });
  BOOST_CHECK_CLOSE(widthBefore, triangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(heightBefore, triangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, triangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(scalingTriangle)
{
  pryanichnikova::Triangle triangle({ 1, 1 }, { 3, 3 }, { 0, 5 });
  const double areaBefore = triangle.getArea();

  const double scaleCoef = 3;
  triangle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(triangle.getArea(), areaBefore * scaleCoef * scaleCoef, EPSILON);
}

BOOST_AUTO_TEST_CASE(validTriangle)
{
  BOOST_CHECK_THROW(pryanichnikova::Triangle({ 1, 1 }, { 2, 2 }, { 3, 3 }), std::invalid_argument);

  pryanichnikova::Triangle triangle({ 3, 1 }, { 2, 2 }, { 4, 3 });
  BOOST_CHECK_THROW(triangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rotationTriangle)
{
  pryanichnikova::Triangle triangle({ 13,20 }, { 15, 22 }, { 12, 12 });
  const double areaBefore = triangle.getArea();
  const double angle = 90;

  triangle.rotate(angle);

  BOOST_CHECK_CLOSE(areaBefore, triangle.getArea(), EPSILON);

}

BOOST_AUTO_TEST_SUITE_END()
