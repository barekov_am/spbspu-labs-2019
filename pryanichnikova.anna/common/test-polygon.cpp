#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "polygon.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteShapes)

const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(PolygonMoveDxy)
{
  pryanichnikova::point_t *mas = new pryanichnikova::point_t[4]{{4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0}};
  pryanichnikova::Polygon polygon(4, mas);
  delete[] mas;
  const pryanichnikova::rectangle_t heightBefore = polygon.getFrameRect();
  const double areaBefore = polygon.getArea();
  polygon.move(5, 10);
  const pryanichnikova::rectangle_t newFrame = polygon.getFrameRect();
  const double areaAfterMove = polygon.getArea();
  BOOST_CHECK_CLOSE(heightBefore.width, newFrame.width, EPSILON);
  BOOST_CHECK_CLOSE(heightBefore.height, newFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, areaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(PolygonMovePoint)
{
  pryanichnikova::point_t *mas = new pryanichnikova::point_t[4]{{4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0}};
  pryanichnikova::Polygon polygon(4, mas);
  delete[] mas;
  const pryanichnikova::rectangle_t heightBefore = polygon.getFrameRect();
  const double areaBefore = polygon.getArea();
  polygon.move({ 20, -25 });
  const pryanichnikova::rectangle_t newFrame = polygon.getFrameRect();
  const double areaAfterMove = polygon.getArea();
  BOOST_CHECK_CLOSE(heightBefore.width, newFrame.width, EPSILON);
  BOOST_CHECK_CLOSE(heightBefore.height, newFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, areaAfterMove, EPSILON);
}


BOOST_AUTO_TEST_CASE(scalingPolygon)
{
  const double scale = 5.0;
  const double declineScale = 0.6;
  pryanichnikova::point_t *cord = new pryanichnikova::point_t[4]{{4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0}};
  pryanichnikova::Polygon Polygon(4, cord);
  delete[] cord;

  double areaBeforeScale = Polygon.getArea();
  Polygon.scale(scale);
  double areaAfterScale = Polygon.getArea();
  BOOST_CHECK_CLOSE((areaBeforeScale * scale * scale), areaAfterScale, EPSILON);

  areaBeforeScale = Polygon.getArea();
  Polygon.scale(declineScale);
  areaAfterScale = Polygon.getArea();
  BOOST_CHECK_CLOSE((areaBeforeScale * declineScale * declineScale), areaAfterScale, EPSILON);

}

BOOST_AUTO_TEST_CASE(validPolygon)
{
  pryanichnikova::point_t *mas = new  pryanichnikova::point_t[2]{{4.0, 10.0}, {15.0, 6.0}};
  BOOST_CHECK_THROW(pryanichnikova::Polygon(2, mas), std::invalid_argument);
  delete[] mas;

}

BOOST_AUTO_TEST_CASE(constructorCopy)
{
  pryanichnikova::point_t *cord = new pryanichnikova::point_t[4]{{4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0}};
  pryanichnikova::Polygon Polygon(4, cord);
  delete[] cord;
  pryanichnikova::Polygon copiedPolygon(Polygon);

  BOOST_CHECK_CLOSE(copiedPolygon.getArea(), Polygon.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(copiedPolygon.getFrameRect().height, Polygon.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(copiedPolygon.getFrameRect().width, Polygon.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(copiedPolygon.getFrameRect().pos.x, Polygon.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(copiedPolygon.getFrameRect().pos.y, Polygon.getFrameRect().pos.y, EPSILON);

}

BOOST_AUTO_TEST_CASE(constructorMove)
{
  pryanichnikova::point_t *cord = new pryanichnikova::point_t[4]{{4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0}};
  pryanichnikova::Polygon Polygon(4, cord);
  delete[] cord;
  double areaBefore = Polygon.getArea();
  pryanichnikova::rectangle_t frameRectBefore = Polygon.getFrameRect();
  pryanichnikova::Polygon movedPolygon(std::move(Polygon));

  BOOST_CHECK_CLOSE(areaBefore, movedPolygon.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height, movedPolygon.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.width, movedPolygon.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, movedPolygon.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, movedPolygon.getFrameRect().pos.y, EPSILON);

}

BOOST_AUTO_TEST_CASE(operatorCopy)
{
  pryanichnikova::point_t *cord = new pryanichnikova::point_t[4]{{4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0}};
  pryanichnikova::Polygon Polygon(4, cord);
  delete[] cord;
  pryanichnikova::point_t *cord1 = new pryanichnikova::point_t[3]{{4.0, 12.0}, {15.0, 6.0}, {1.0, 1.0}};
  pryanichnikova::Polygon copiedPolygon(3, cord1);
  delete[] cord1;
  copiedPolygon = Polygon;

  BOOST_CHECK_CLOSE(Polygon.getArea(), copiedPolygon.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(Polygon.getFrameRect().height, copiedPolygon.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(Polygon.getFrameRect().width, copiedPolygon.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(Polygon.getFrameRect().pos.x, copiedPolygon.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(Polygon.getFrameRect().pos.y, copiedPolygon.getFrameRect().pos.y, EPSILON);

}

BOOST_AUTO_TEST_CASE(operatorMove)
{
  pryanichnikova::point_t *cord = new pryanichnikova::point_t[4]{{4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0}};
  pryanichnikova::Polygon Polygon(4, cord);
  delete[] cord;

  double areaBefore = Polygon.getArea();
  pryanichnikova::rectangle_t frameRectBefore = Polygon.getFrameRect();
  pryanichnikova::point_t *cord1 = new pryanichnikova::point_t[3]{{4.0, 12.0}, {15.0, 6.0}, {1.0, 1.0}};
  pryanichnikova::Polygon movedPolygon(3, cord1);
  delete[] cord1;
  movedPolygon = std::move(Polygon);
  BOOST_CHECK_CLOSE(areaBefore, movedPolygon.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height, movedPolygon.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.width, movedPolygon.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, movedPolygon.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, movedPolygon.getFrameRect().pos.y, EPSILON);

}

BOOST_AUTO_TEST_CASE(rotationPolygon)
{
  pryanichnikova::point_t *cord = new pryanichnikova::point_t[4]{{4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0}};
  pryanichnikova::Polygon polygon(4, cord);
  delete[] cord;
  const double testArea = polygon.getArea();
  const double angle = 90;

  polygon.rotate(angle);

  BOOST_CHECK_CLOSE(testArea, polygon.getArea(), EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()
