#define _USE_MATH_DEFINES
#include "triangle.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>
#include <stdexcept>

pryanichnikova::Triangle::Triangle(const point_t &pointA, const point_t &pointB, const point_t &pointC) :
  pointA_(pointA),
  pointB_(pointB),
  pointC_(pointC),
  centre_{(pointA.x + pointB.x + pointC.x) / 3, (pointA.y + pointB.y + pointC.y) / 3},
  angle_(0)
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("Invalid trianle's area");
  }
}

void pryanichnikova::Triangle::move(const point_t &centre)
{
  move(centre.x - centre_.x, centre.y - centre_.y);
}

void pryanichnikova::Triangle::move(double dx, double dy)
{
  pointA_.x += dx;
  pointB_.x += dx;
  pointC_.x += dx;
  pointA_.y += dy;
  pointB_.y += dy;
  pointC_.y += dy;
  centre_.x += dx;
  centre_.y += dy;
}

double pryanichnikova::Triangle::getArea() const
{
  double sideA = sqrt(pow(pointA_.x - pointB_.x, 2) + pow(pointA_.y - pointB_.y, 2));
  double sideB = sqrt(pow(pointB_.x - pointC_.x, 2) + pow(pointB_.y - pointC_.y, 2));
  double sideC = sqrt(pow(pointC_.x - pointA_.x, 2) + pow(pointC_.y - pointA_.y, 2));
  double p = (sideA + sideB + sideC) / 2;
  return sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
}

pryanichnikova::rectangle_t pryanichnikova::Triangle::getFrameRect() const
{
  const double maxX = std::max(std::max(pointA_.x, pointB_.x), pointC_.x);
  const double minX = std::min(std::min(pointA_.x, pointB_.x), pointC_.x);
  const double maxY = std::max(std::max(pointA_.y, pointB_.y), pointC_.y);
  const double minY = std::min(std::min(pointA_.y, pointB_.y), pointC_.y);
  const double width = maxX - minX;
  const double height = maxY - minY;
  const point_t centre = {minX + width / 2, minY + height / 2};
  return {width, height, centre};
}

void pryanichnikova::Triangle::scale(double scaleCoef)
{
  if (scaleCoef <= 0.0)
  {
    throw std::invalid_argument("Triangle's argument of scale is invalid.");
  }
  else
  {
    pointA_ = { centre_.x + (pointA_.x - centre_.x) * scaleCoef, centre_.y + (pointA_.y - centre_.y) * scaleCoef };
    pointB_ = { centre_.x + (pointB_.x - centre_.x) * scaleCoef, centre_.y + (pointB_.y - centre_.y) * scaleCoef };
    pointC_ = { centre_.x + (pointC_.x - centre_.x) * scaleCoef, centre_.y + (pointC_.y - centre_.y) * scaleCoef };
  }
}


void pryanichnikova::Triangle::rotate(const double angle)
{
  pointA_ = rotate(pointA_, angle);
  pointB_ = rotate(pointB_, angle);
  pointC_ = rotate(pointC_, angle);
}

pryanichnikova::point_t pryanichnikova::Triangle::rotate(const point_t & point, double angle) const
{
  angle *= M_PI / 180;
  return point_t{ centre_.x + (point.x - centre_.x) * cos(angle) - (point.y - centre_.y) * sin(angle),
    centre_.y + (point.y - centre_.y) * cos(angle) + (point.x - centre_.x) * sin(angle) };

}

void pryanichnikova::Triangle::printData() const
{
  std::cout << "ABOUT TRIANGLE\n";
  std::cout << "Centre : (" << centre_.x << ", " << centre_.y << ")\n";
  std::cout << "Area : " << getArea() << "\n";
  std::cout << "Width : " << getFrameRect().width << " Height : " << getFrameRect().height << "\n\n";
}
