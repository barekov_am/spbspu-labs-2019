#define _USE_MATH_DEFINES
#include "polygon.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>
#include <stdexcept>

pryanichnikova::Polygon::Polygon(const Polygon & value) :
  quantity_(value.quantity_),
  point_(new point_t[quantity_])
{
  for (int i = 0; i < quantity_; i++)
  {
    point_[i] = value.point_[i];
  }
}

pryanichnikova::Polygon::Polygon(Polygon && value) :
  quantity_(value.quantity_),
  point_(value.point_)
{
  value.point_ = nullptr;
  value.quantity_ = 0;
}

pryanichnikova::Polygon::Polygon(int quantity, point_t * point) :
  quantity_(quantity),
  point_(new point_t[quantity])
{
  if (quantity_ < 3)
  {
    delete[] point_;
    throw std::invalid_argument("Invalid polygon's parameters");
  }
  if (point_ == nullptr)
  {
    delete[] point_;
    throw std::invalid_argument("Pointer to points mustn't be nullptr");
  }

  for (int i = 0; i < quantity_; i++)
  {
    point_[i] = point[i];
  }
  if (getArea() <= 0.0)
  {
    delete[] point_;
    throw std::invalid_argument("Invalid polygon's area");
  }
  if (!checkConvex())
  {
    delete[] point_;
    throw std::invalid_argument("Polygon must be convex");
  }
}

pryanichnikova::Polygon::~Polygon()
{
  delete[] point_;
}

pryanichnikova::Polygon & pryanichnikova::Polygon::operator =(const Polygon & value)
{
  if (this == &value)
  {
    return *this;
  }
  quantity_ = value.quantity_;
  delete[] point_;
  point_ = new point_t[quantity_];
  for (int i = 0; i < quantity_; i++)
  {
    point_[i] = value.point_[i];
  }
  return *this;
}

pryanichnikova::Polygon & pryanichnikova::Polygon::operator =(Polygon && value)
{
  if (this == &value)
  {
    return *this;
  }
  quantity_ = value.quantity_;
  value.quantity_ = 0;
  if (point_ != nullptr)
  {
    delete[] point_;
  }
  point_ = value.point_;
  value.point_ = nullptr;
  return *this;
}

double pryanichnikova::Polygon::getArea() const
{
  double area_poly = 0;
  for (int i = 0; i < quantity_ - 1; i++)
  {
    area_poly += point_[i].x * point_[i + 1].y;
    area_poly -= point_[i + 1].x * point_[i].y;
  }
  area_poly += point_[quantity_ - 1].x * point_[0].y;
  area_poly -= point_[0].x * point_[quantity_ - 1].y;
  return (std::fabs(area_poly) /  2);
}

pryanichnikova::rectangle_t pryanichnikova::Polygon::getFrameRect() const
{
  double maxX = point_[0].x;
  double minX = point_[0].x;
  double maxY = point_[0].y;
  double minY = point_[0].y;
  for (int i = 1; i < quantity_; i++)
  {
    maxX = std::max(maxX, point_[i].x);
    minX = std::min(minX, point_[i].x);
    maxY = std::max(maxY, point_[i].y);
    minY = std::min(minY, point_[i].y);
  }
  const double height = (maxY - minY);
  const double width = (maxX - minX);
  const point_t centre = {minX + (width / 2), minY + (height / 2)};
  return {width, height, centre};
}

void pryanichnikova::Polygon::move(const point_t &pos)
{
  const point_t center = getCentre();
  move(pos.x - center.x, pos.y - center.y);
}

void pryanichnikova::Polygon::move(double dx, double dy)
{
  for (int i = 0; i < quantity_; i++)
  {
    point_[i].x += dx;
    point_[i].y += dy;
  }
}

pryanichnikova::point_t pryanichnikova::Polygon::getCentre() const
{
  point_t center = { 0, 0 };
  for (int i = 0; i < quantity_; i++)
  {
    center.x += point_[i].x / quantity_;
    center.y += point_[i].y / quantity_;
  }
  return center;
}

bool pryanichnikova::Polygon::checkConvex() const
{
  point_t pointA = {0, 0};
  point_t pointB = {0, 0};
  for (int i = 0; i <= quantity_ - 2; i++)
  {
    pointA.x = point_[i + 1].x - point_[i].x;
    pointA.y = point_[i + 1].y - point_[i].y;
    if (i == quantity_ - 2)
    {
      pointB.x = point_[0].x - point_[i + 1].x;
      pointB.y = point_[0].y - point_[i + 1].y;
    }
    else
    {
      pointB.x = point_[i + 2].x - point_[i + 1].x;
      pointB.y = point_[i + 2].y - point_[i + 1].y;
    }
    if ((pointA.x * pointB.y) - (pointA.y * pointB.x) < 0)
    {
    return true;
    }
  }
  return false;
}

void pryanichnikova::Polygon::scale(double scaleCoef)
{
  if (scaleCoef <= 0)
  {
    throw std::invalid_argument("scale multiplier can't be <=0");
  }
  else
  {
    point_t centre_ = getCentre();
    for (int i = 0; i < quantity_; i++)
    {
      point_[i].x = centre_.x + scaleCoef * (point_[i].x - centre_.x);
      point_[i].y = centre_.y + scaleCoef * (point_[i].y - centre_.y);
    }
  }
}

void pryanichnikova::Polygon::rotate(double angle)
{
  const point_t center = getCentre();
  angle = angle * M_PI / 180;
  for (int i = 0; i < quantity_; i++)
  {
    const point_t tempPoint = point_[i];
    point_[i].x = center.x + (tempPoint.x - center.x) * cos(angle) -
      (tempPoint.y - center.y) * sin(angle);
    point_[i].y = center.y + (tempPoint.x - center.x) * sin(angle) +
      (tempPoint.y - center.y) * cos(angle);
  }
}

void pryanichnikova::Polygon::printData() const
{
  const point_t center_ = getCentre();
  std::cout << "ABOUT POLYGON:\n";
  std::cout << "Center : (" << center_.x << ", " << center_.y << ")\n";
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "Width: " << getFrameRect().width << "  Height: " << getFrameRect().height <<"\n\n";
}
