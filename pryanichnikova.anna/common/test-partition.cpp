#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>



BOOST_AUTO_TEST_SUITE(separationTest)

BOOST_AUTO_TEST_CASE(checkCorrectCrossing)
{
  pryanichnikova::Rectangle testRectangle({ 5.0, 5.0 },4.0, 2.0);
  pryanichnikova::Rectangle crossingTestRectangle({ 8.0, 5.0 }, 6.0, 2.0 );
  pryanichnikova::Circle testCircle({ 11.0, 5.0 }, 1.0);

  const bool falseResult = pryanichnikova::cross(testRectangle.getFrameRect(), testCircle.getFrameRect());
  const bool trueResult1 = pryanichnikova::cross(testRectangle.getFrameRect(), crossingTestRectangle.getFrameRect());
  const bool trueResult2 = pryanichnikova::cross(testCircle.getFrameRect(), crossingTestRectangle.getFrameRect());

  BOOST_CHECK_EQUAL(falseResult, false);
  BOOST_CHECK_EQUAL(trueResult1, true);
  BOOST_CHECK_EQUAL(trueResult2, true);

}

BOOST_AUTO_TEST_CASE(checkCorrectSeparation)
{
  pryanichnikova::Rectangle testRectangle({ 5.0, 5.0 }, 4.0, 2.0);
  pryanichnikova::Rectangle crossingTestRectangle({ 8.0, 5.0 }, 6.0, 2.0);
  pryanichnikova::Circle testCircle({ 11.0, 5.0 }, 1.0);
  pryanichnikova::shapePtr rectanglePtr = std::make_shared<pryanichnikova::Rectangle>(testRectangle);
  pryanichnikova::shapePtr rectangleCrossingPtr = std::make_shared<pryanichnikova::Rectangle>(crossingTestRectangle);
  pryanichnikova::shapePtr circlePtr = std::make_shared<pryanichnikova::Circle>(testCircle);

  pryanichnikova::CompositeShape testCompositeShape(rectanglePtr);
  testCompositeShape.add(rectangleCrossingPtr);
  testCompositeShape.add(circlePtr);

  pryanichnikova::Matrix testMatrix = pryanichnikova::part(testCompositeShape);

  BOOST_CHECK(testMatrix[0][0] == rectanglePtr);
  BOOST_CHECK(testMatrix[0][1] == circlePtr);
  BOOST_CHECK(testMatrix[1][0] == rectangleCrossingPtr);
}

BOOST_AUTO_TEST_SUITE_END()
