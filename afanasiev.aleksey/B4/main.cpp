#include <vector>
#include <algorithm>

#include "dataStruct.hpp"

void sort(std::vector<DataStruct> &vector);

int main()
{
  try
  {
    std::vector<DataStruct> vector;
    std::string str;

    while (std::getline(std::cin, str))
    {
      if (std::cin.fail())
      {
        throw std::runtime_error("Invalid reading");
      }

      vector.push_back(readData(str));
    }

    sort(vector);
    std::for_each(vector.begin(), vector.end(), printData);
  }

  catch (std::exception &exception)
  {
    std::cerr << exception.what() << std::endl;

    return 1;
  }

  return 0;
}
