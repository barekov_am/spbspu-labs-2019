#include <vector>
#include <algorithm>

#include "dataStruct.hpp"

struct compare
{
  bool operator()(const DataStruct &lhs, const DataStruct &rhs)
  {
    if (lhs.key1 < rhs.key1)
    {
      return true;
    }

    if (lhs.key1 == rhs.key1)
    {
      if (lhs.key2 < rhs.key2)
      {
        return true;
      }

      if (lhs.key2 == rhs.key2)
      {
        return lhs.str.length() < rhs.str.length();
      }
    }

    return false;
  }
};

void sort(std::vector<DataStruct> &vector)
{
  std::sort(vector.begin(), vector.end(), compare());
}
