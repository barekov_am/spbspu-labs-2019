#include <sstream>
#include "dataStruct.hpp"

DataStruct readData(const std::string &line)
{
  int key1 = 0;
  int key2 = 0;
  std::string str;

  std::stringstream stream(line);
  stream >> key1;
  stream.ignore(line.length(), ',');
  stream >> key2;
  stream.ignore(line.length(), ',');
  std::getline(stream, str);

  if (key1 < -5 || key1 > 5 || key2 < -5 || key2 > 5 || str.empty() || stream.fail())
  {
    throw std::invalid_argument("Read failed");
  }

  return {key1, key2, str};
}

void printData(const DataStruct &data)
{
  std::cout << data.key1 << ',' << data.key2 << ',' << data.str << std::endl;
}
