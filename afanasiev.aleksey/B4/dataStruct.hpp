#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP

#include <iostream>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

DataStruct readData(const std::string &line);
void printData(const DataStruct &data);

#endif //DATASTRUCT_HPP
