#ifndef TEXTHANDLER_HPP
#define TEXTHANDLER_HPP

#include <iostream>
#include <vector>

class TextHandler
{
public:
  TextHandler(size_t width);

  void read();
  void format();
  void output() const;

private:
  std::vector<std::string> components_;
  std::vector<std::string> text_;
  size_t width_;

  std::string readWord(std::istream &input);
  std::string readPunctuation(std::istream &input);
  std::string readNumber(std::istream &input);
  bool isPunctuation(const std::string &input);
};

#endif //TEXTHANDLER_HPP
