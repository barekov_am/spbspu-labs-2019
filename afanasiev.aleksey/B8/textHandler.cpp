#include <iterator>

#include "textHandler.hpp"

const size_t MAX_WIDTH_COMPONENT = 20;
const size_t WIDTH_DASH = 3;

TextHandler::TextHandler(size_t width):
  width_(width)
{
}

void TextHandler::read()
{
  std::istream &input = std::cin;

  while (!input.eof())
  {
    if (input.peek() == std::char_traits<char>::eof())
    {
      return;
    }

    char tmp;
    input.get(tmp);

    if (std::isalpha(tmp))
    {
      input.unget();
      components_.push_back(readWord(input));
    }
    else if (std::ispunct(tmp) && !(((tmp == '+') || (tmp == '-')) && std::isdigit(input.peek())))
    {
      if (components_.empty())
      {
        throw std::invalid_argument("Invalid read");
      }

      input.unget();
      std::string punctuation = readPunctuation(input);

      if ((isPunctuation(components_.back()) && isPunctuation(punctuation)) && !((components_.back() == ",") &&
          (punctuation == "---")))
      {
        throw std::invalid_argument("Invalid read");
      }

      components_.push_back(punctuation);
    }
    else if ((std::isdigit(tmp)) || (tmp == '+') || (tmp == '-'))
    {
      input.unget();
      components_.push_back(readNumber(input));
    }
    else if (std::isspace(tmp))
    {
      while (std::isspace(input.peek()))
      {
        input.get();
      }
    }
  }
}

void TextHandler::format()
{
  if (components_.empty())
  {
    return;
  }

  std::string str = components_.front();
  std::string prevComponent;
  std::string currentComponent;
  size_t lengthSpace;

  for (auto it = std::next(components_.begin()); it < components_.end(); ++it)
  {
    prevComponent = *std::prev(it);
    currentComponent = *it;
    lengthSpace = (isPunctuation(currentComponent) && (currentComponent != "---")) ? 0 : 1;

    if ((currentComponent.size() + str.size() + lengthSpace) > width_)
    {
      if (isPunctuation(currentComponent))
      {
        std::size_t lengthPrevSpace = (isPunctuation(prevComponent) && (prevComponent != "---")) ? 0 : 1;
        str.erase(str.size() - (prevComponent.size() + lengthPrevSpace), prevComponent.size() + lengthPrevSpace);
        text_.push_back(str);
        str.erase(str.begin(), str.end());
        str.append(prevComponent);
      }
      else
      {
        text_.push_back(str);
        str.erase(str.begin(), str.end());
      }
    }

    if (!str.empty() && lengthSpace == 1)
    {
      str.append(" ");
    }

    str.append(currentComponent);
    prevComponent = currentComponent;
    currentComponent.erase(currentComponent.begin(), currentComponent.end());
  }

  text_.push_back(str);
}

void TextHandler::output() const
{
  for (auto it = text_.begin(); it < text_.end(); ++it)
  {
    std::cout << *it << std::endl;
  }
}

std::string TextHandler::readWord(std::istream &input)
{
  std::string outputStr;
  char tmp;

  while (std::isalpha(input.peek()) || (input.peek() == '-'))
  {
    input.get(tmp);

    if ((tmp == '-') && ((outputStr == "/n") || (input.peek() == '-')))
    {
      throw std::invalid_argument("That's not a word");
    }

    outputStr.push_back(tmp);

    if (outputStr.size() > MAX_WIDTH_COMPONENT)
    {
      throw std::invalid_argument("That's a big word");
    }
  }

  return outputStr;
}

std::string TextHandler::readPunctuation(std::istream &input)
{
  std::string outputStr;
  char tmp;

  input.get(tmp);

  if (std::ispunct(tmp))
  {
    outputStr.push_back(tmp);

    if (tmp == '-')
    {
      while (input.peek() == '-')
      {
        input.get(tmp);
        outputStr.push_back(tmp);
      }

      if (outputStr.size() != WIDTH_DASH)
      {
        throw std::invalid_argument("That's not a dash");
      }
    }
  }

  return outputStr;
}

std::string TextHandler::readNumber(std::istream &input)
{
  std::string outputStr;
  char tmp;

  input.get(tmp);

  if (std::isdigit(tmp) || (tmp == '+') || (tmp == '-'))
  {
    outputStr.push_back(tmp);
  }

  if (!(std::isdigit(outputStr.front())) && !(std::isdigit(input.peek())))
  {
    throw std::invalid_argument("Incorrect number");
  }

  size_t countDecSeparate = 0;

  while (std::isdigit(input.peek()) || (input.peek() == '.'))
  {
    input.get(tmp);

    if (tmp == '.')
    {
      countDecSeparate++;

      if (countDecSeparate > 1 )
      {
        throw std::invalid_argument("Incorrect number");
      }
    }

    outputStr.push_back(tmp);

    if (outputStr.size() > MAX_WIDTH_COMPONENT)
    {
      throw std::invalid_argument("That's a long number");
    }
  }

  return outputStr;
}

bool TextHandler::isPunctuation(const std::string &input)
{
  return std::ispunct(input.front()) && std::ispunct(input.back());
}
