#include "textHandler.hpp"

const size_t DEFAULT_LINE_WIDTH = 40;
const size_t MIN_LINE_WIDTH  = 25;
const char* LINE_ARGV_1 = "--line-width";

int main(int argc, char* argv[])
{
  try
  {
    if ((argc != 1) && (argc != 3))
    {
      std::cerr << "Incorrect number of agruments" << std::endl;
      return 1;
    }

    size_t width = DEFAULT_LINE_WIDTH;

    if (argc == 3)
    {
      if ((std::string(argv[1]) != LINE_ARGV_1) || (std::stoul(argv[2]) < MIN_LINE_WIDTH))
      {
        std::cerr << "Incorrect argument" << std::endl;
        return 1;
      }

      width = static_cast<size_t>(std::stoi(argv[2]));
    }

    TextHandler handler(width);
    handler.read();
    handler.format();
    handler.output();

  }
  catch(const std::exception &exception)
  {
    std::cerr << exception.what() << std::endl;
    return 2;
  }

  return 0;
}
