#include <iostream>
#include <set>

void task1()
{
  std::set<std::string> setWords;
  std::string str;

  while (std::getline(std::cin, str))
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Invalid reading");
    }

    while (str.find_first_of(" \t\n") == 0)
    {
      str.erase(0, 1);
    }

    while (!str.empty())
    {
      size_t pos = str.find_first_of(" \t\n");

      setWords.emplace(str.substr(0, pos));
      str.erase(0, pos);

      while (str.find_first_of(" \t\n") == 0)
      {
        str.erase(0, 1);
      }
    }
  }

  for (std::set<std::string>::const_iterator it = setWords.begin(); it != setWords.end(); it++)
  {
    std::cout << *it << std::endl;
  }
}
