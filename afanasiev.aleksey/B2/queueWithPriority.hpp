#ifndef QUEUE_WITH_PRIORITY_HPP
#define QUEUE_WITH_PRIORITY_HPP

#include "queueWithPriorityInterface.hpp"

template<typename T>
void QueueWithPriority<T>::putElementToQueue(const T &element, ElementPriority priority)
{
  switch(priority)
  {
  case ElementPriority::LOW:
    lowPriority_.push_back(element);
    break;
  case ElementPriority::NORMAL:
    normalPriority_.push_back(element);
    break;
  case ElementPriority::HIGH:
    highPriority_.push_back(element);
    break;
  }
}

template<typename T>
T QueueWithPriority<T>::getElementFromQueue()
{
  if (empty())
  {
    throw std::out_of_range("Queue is empty");
  }

  if (!highPriority_.empty())
  {
    T element = highPriority_.front();
    highPriority_.pop_front();

    return element;
  }
  else if (!normalPriority_.empty())
  {
    T element = normalPriority_.front();
    normalPriority_.pop_front();

    return element;
  }
  else
  {
    T element = lowPriority_.front();
    lowPriority_.pop_front();

    return element;
  }
}

template<typename T>
void QueueWithPriority<T>::accelerate()
{
  highPriority_.splice(highPriority_.end(), lowPriority_);
}

template<typename T>
bool QueueWithPriority<T>::empty() const
{
  return lowPriority_.empty() && normalPriority_.empty() && highPriority_.empty();
}

#endif //QUEUE_WITH_PRIORITY_HPP
