#include <iostream>
#include <sstream>
#include <cstring>

#include "queueWithPriority.hpp"

void task1()
{
  QueueWithPriority<std::string> queue;
  std::string str;
  std::string command;
  std::string priority;
  std::string data;

  while (std::getline(std::cin, str))
  {
    std::stringstream stream(str);
    stream >> command;

    if (command == "add")
    {
      stream >> priority >> data;

      if (!stream.eof())
      {
        std::getline(stream, str);
        data += str;
      }

      if (!stream.eof())
      {
        std::cout << "<INVALID COMMAND>\n";
      }

      if (data.empty())
      {
        std::cout << "<INVALID COMMAND>\n";
      }
      else if (priority == "low")
      {
        queue.putElementToQueue(data, QueueWithPriority<std::string>::ElementPriority::LOW);
      }
      else if (priority == "normal")
      {
        queue.putElementToQueue(data, QueueWithPriority<std::string>::ElementPriority::NORMAL);
      }
      else if (priority == "high")
      {
        queue.putElementToQueue(data, QueueWithPriority<std::string>::ElementPriority::HIGH);
      }
      else
      {
        std::cout << "<INVALID COMMAND>\n";
      }
    }
    else if (command == "get")
    {
      if (queue.empty())
      {
        std::cout << "<EMPTY>\n";
      }
      else
      {
        std::cout << queue.getElementFromQueue() << "\n";
      }
    }
    else if (command == "accelerate")
    {
      queue.accelerate();
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
