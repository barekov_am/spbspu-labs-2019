#include <iostream>
#include <list>

#include "queueWithPriority.hpp"

void task2()
{
  std::list<int> list;
  int value = 0;
  const int min = 1;
  const int max = 20;
  const int maxSize = 20;

  while (std::cin >> value, !std::cin.eof())
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Couldn't read input file successfully");
    }

    if ((value < min) || (value > max))
    {
      throw std::out_of_range("Invalid input value");
    }

    if (list.size() + 1 > maxSize)
    {
      throw std::out_of_range("Overflow");
    }

    list.push_back(value);
  }

  std::list<int>::iterator begin = list.begin();
  std::list<int>::iterator end = list.end();

  while (begin != end)
  {
    std::cout << *begin << " ";

    if (std::next(begin) == end)
    {
      break;
    }

    begin++;
    end--;
    std::cout << *end << " ";
  }

  std::cout << "\n";
}
