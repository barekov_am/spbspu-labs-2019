#ifndef QUEUE_WITH_PRIORITY_INTERFACE_HPP
#define QUEUE_WITH_PRIORITY_INTERFACE_HPP

#include <iostream>
#include <list>

template<typename T>
class QueueWithPriority
{
public:
  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  void putElementToQueue(const T &element, ElementPriority priority);
  T getElementFromQueue();
  void accelerate();
  bool empty() const;

private:
  std::list<T> lowPriority_;
  std::list<T> normalPriority_;
  std::list<T> highPriority_;
};

#endif //QUEUE_WITH_PRIORITY_INTERFACE_HPP
