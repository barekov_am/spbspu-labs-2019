#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace afanasiev
{
  Matrix part(const afanasiev::Shape::shape_array array, size_t size);
  Matrix part(const CompositeShape &composite);

  bool isOverlap(const afanasiev::Shape::shape_ptr &firstShape, const afanasiev::Shape::shape_ptr &secondShape);
}

#endif //PARTITION_HPP
