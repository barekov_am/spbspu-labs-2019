#include <stdexcept>
#include <cmath>
#include "rectangle.hpp"

const double FULLROTATION = 360;

afanasiev::Rectangle::Rectangle(const point_t &position, double width, double height):
  position_(position),
  width_(width),
  height_(height),
  angle_(0)
{
  if ((width <= 0) || (height <= 0))
  {
    throw std::invalid_argument("Width and height must be positive");
  }
}

double afanasiev::Rectangle::getArea() const
{
  return width_ * height_;
}

afanasiev::rectangle_t afanasiev::Rectangle::getFrameRect() const
{
  const double radAngle = angle_ * M_PI / 180;
  const double sinA = std::sin(radAngle);
  const double cosA = std::cos(radAngle);
  const double width = fabs(sinA) * height_ + fabs(cosA) * width_;
  const double height = fabs(sinA) * width_ + fabs(cosA) * height_;

  return {position_, width, height};
}

void afanasiev::Rectangle::move(const point_t &newPosition)
{
  position_ = newPosition;
}

void afanasiev::Rectangle::move(double dx, double dy)
{
  position_.x += dx;
  position_.y += dy;
}

void afanasiev::Rectangle::scale(double zoomFactor)
{
  if (zoomFactor <= 0)
  {
    throw std::invalid_argument("Zoom factor must be positive");
  }
  else
  {
    width_ *= zoomFactor;
    height_ *= zoomFactor;
  }
}

void afanasiev::Rectangle::rotate(double angle)
{
  angle_ += angle;

  angle_ = (angle_ > 0) ? fmod(angle_, FULLROTATION) : FULLROTATION - fmod(angle_, FULLROTATION);
}
