#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace afanasiev
{
  class Shape
  {
  public:
    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;

    virtual void move(const point_t &newPosition) = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void scale(double zoomFactor) = 0;
    virtual void rotate(double angle) = 0;

    using shape_ptr = std::shared_ptr<Shape>;
    using shape_array = std::unique_ptr<shape_ptr[]>;
  };
} //namespace afanasiev

#endif //SHAPE_HPP
