#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double ACCURACY = 0.001;
const double ANGLE = 45;

BOOST_AUTO_TEST_SUITE(TestRectangle)

BOOST_AUTO_TEST_CASE(constRectangleAfterMovingByIncrements)
{
  afanasiev::Rectangle testRectangle({1, 1}, 1, 1);
  const afanasiev::rectangle_t initialRect = testRectangle.getFrameRect();
  const double initialArea = testRectangle.getArea();

  testRectangle.move(5, 5);

  BOOST_CHECK_CLOSE(initialRect.width, testRectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialRect.height, testRectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(constRectangleAfterMovingToPoint)
{
  afanasiev::Rectangle testRectangle({1, 1}, 1, 1);
  const afanasiev::rectangle_t initialRect = testRectangle.getFrameRect();
  const double initialArea = testRectangle.getArea();

  testRectangle.move({5, 5});

  BOOST_CHECK_CLOSE(initialRect.width, testRectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialRect.height, testRectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(quadraticChangeRectangleAreaAfterScaling)
{
  afanasiev::Rectangle testRectangle({1, 1}, 1, 1);

  const double zoomFactor_1 = 2;
  const double zoomFactor_2 = 0.5;
  double initialArea = testRectangle.getArea();

  testRectangle.scale(zoomFactor_1);

  BOOST_CHECK_CLOSE(testRectangle.getArea(), initialArea * zoomFactor_1 * zoomFactor_1, ACCURACY);

  initialArea = testRectangle.getArea();

  testRectangle.scale(zoomFactor_2);

  BOOST_CHECK_CLOSE(testRectangle.getArea(), initialArea * zoomFactor_2 * zoomFactor_2, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testRectangleAfterRotating)
{
  const double radAngle = ANGLE * M_PI / 180;
  const double sinA = sin(radAngle);
  const double cosA = cos(radAngle);

  afanasiev::Rectangle testRectangle({1, 1}, 1, 1);
  const afanasiev::rectangle_t initialRectangle = testRectangle.getFrameRect();
  const double initialArea = testRectangle.getArea();

  testRectangle.rotate(ANGLE);

  const  double requiredWidth = (initialRectangle.width * cosA) + (initialRectangle.height * sinA);
  const  double requiredHeight = (initialRectangle.width * sinA) + (initialRectangle.height * cosA);

  BOOST_CHECK_CLOSE(requiredHeight, testRectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(requiredWidth, testRectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialRectangle.pos.x, testRectangle.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialRectangle.pos.y, testRectangle.getFrameRect().pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidRectangleParameteres)
{
  BOOST_CHECK_THROW(afanasiev::Rectangle({1, 1}, -1, 1), std::invalid_argument);
  BOOST_CHECK_THROW(afanasiev::Rectangle({1, 1}, 1, -1), std::invalid_argument);

  afanasiev::Rectangle testRectangle({1, 1}, 1, 1);

  BOOST_CHECK_THROW(testRectangle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
