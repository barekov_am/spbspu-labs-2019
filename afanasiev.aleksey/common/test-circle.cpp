#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(TestCircle)

BOOST_AUTO_TEST_CASE(constCircleAfterMovingByIncrements)
{
  afanasiev::Circle testCircle({1, 1}, 1);
  const afanasiev::rectangle_t initialCircle = testCircle.getFrameRect();
  const double initialArea = testCircle.getArea();

  testCircle.move(5, 5);

  BOOST_CHECK_CLOSE(initialCircle.width, testCircle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialCircle.height, testCircle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testCircle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(constCircleAfterMovingToPoint)
{
  afanasiev::Circle testCircle({1, 1}, 1);
  const afanasiev::rectangle_t initialCircle = testCircle.getFrameRect();
  const double initialArea = testCircle.getArea();

  testCircle.move({5, 5});

  BOOST_CHECK_CLOSE(initialCircle.width, testCircle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialCircle.height, testCircle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testCircle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(quadraticChangeCircleAreaAfterScaling)
{
  afanasiev::Circle testCircle({1, 1}, 1);

  const double zoomFactor_1 = 2;
  const double zoomFactor_2 = 0.5;
  double initialArea = testCircle.getArea();

  testCircle.scale(zoomFactor_1);

  BOOST_CHECK_CLOSE(testCircle.getArea(), initialArea * zoomFactor_1 * zoomFactor_1, ACCURACY);

  initialArea = testCircle.getArea();

  testCircle.scale(zoomFactor_2);

  BOOST_CHECK_CLOSE(testCircle.getArea(), initialArea * zoomFactor_2 * zoomFactor_2, ACCURACY);
}

BOOST_AUTO_TEST_CASE(constCircleAfterRotation)
{
  afanasiev::Circle testCircle({1, 1}, 1);
  const afanasiev::rectangle_t initialCircle = testCircle.getFrameRect();
  const double initialArea = testCircle.getArea();

  testCircle.rotate(30);

  BOOST_CHECK_CLOSE(initialCircle.width, testCircle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialCircle.height, testCircle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialCircle.pos.x, testCircle.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialCircle.pos.y, testCircle.getFrameRect().pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testCircle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidCircleParameteres)
{
  BOOST_CHECK_THROW(afanasiev::Circle({1, 1}, -1), std::invalid_argument);

  afanasiev::Circle testCircle({1, 1}, 1);

  BOOST_CHECK_THROW(testCircle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
