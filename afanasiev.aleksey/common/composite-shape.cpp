#include <stdexcept>
#include <cmath>
#include "composite-shape.hpp"

afanasiev::CompositeShape::CompositeShape():
  count_(0)
{
}

afanasiev::CompositeShape::CompositeShape(const CompositeShape &source):
  count_(source.count_),
  shapeArr_(std::make_unique<shape_ptr[]>(source.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    shapeArr_[i] = source.shapeArr_[i];
  }
}

afanasiev::CompositeShape::CompositeShape(CompositeShape &&source):
  count_(source.count_),
  shapeArr_(std::move(source.shapeArr_))
{
  source.count_ = 0;
}

afanasiev::CompositeShape::CompositeShape(shape_ptr shape):
  count_(1),
  shapeArr_(std::make_unique<shape_ptr[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Invalid pointer");
  }

  shapeArr_[0] = shape;
}

afanasiev::CompositeShape &afanasiev::CompositeShape::operator =(const CompositeShape &rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    shape_array shapes(std::make_unique<shape_ptr[]>(count_));

    for (size_t i = 0; i < count_; i++)
    {
      shapes[i] = rhs.shapeArr_[i];
    }

  shapeArr_.swap(shapes);
  }
  
  return *this;
}

afanasiev::CompositeShape &afanasiev::CompositeShape::operator =(CompositeShape &&rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    shapeArr_ = std::move(rhs.shapeArr_);
    rhs.count_ = 0;
  }
  
  return *this;
}

afanasiev::Shape::shape_ptr afanasiev::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::invalid_argument("Invalid index");
  }

  return shapeArr_[index];
}

double afanasiev::CompositeShape::getArea() const
{
  double compositeArea = 0;

  for (size_t i = 0; i < count_; i++)
  {
    compositeArea += shapeArr_[i]->getArea();
  }

  return compositeArea;
}

afanasiev::rectangle_t afanasiev::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Object doesn't exist");
  }

  rectangle_t rect = shapeArr_[0]->getFrameRect();
  double maxY = rect.pos.y + rect.height / 2;
  double minY = rect.pos.y - rect.height / 2;
  double maxX = rect.pos.x + rect.width / 2;
  double minX = rect.pos.x - rect.width / 2;

  for (size_t i = 1; i < count_; i++)
  {
    rect = shapeArr_[i]->getFrameRect();
    maxY = std::max(maxY, rect.pos.y + rect.height / 2);
    minY = std::min(minY, rect.pos.y - rect.height / 2);
    maxX = std::max(maxX, rect.pos.x + rect.width / 2);
    minX = std::min(minX, rect.pos.x - rect.width / 2);
  }

  return rectangle_t{{(maxX + minX) / 2, (maxY + minY) / 2}, maxX - minX, maxY - minY};
}

void afanasiev::CompositeShape::move(const point_t &newPosition)
{
  const double dx = newPosition.x - getFrameRect().pos.x;
  const double dy = newPosition.y - getFrameRect().pos.y;

  move(dx, dy);
}

void afanasiev::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < count_; i++)
  {
    shapeArr_[i]->move(dx, dy);
  }
}

void afanasiev::CompositeShape::scale(double zoomFactor)
{
  if (count_ == 0)
  {
    throw std::logic_error("Object doesn't exist");
  }

  if (zoomFactor <= 0)
  {
    throw std::invalid_argument("Zoom factor must be positive");
  }

  point_t center = getFrameRect().pos;

  for (size_t i = 0; i < count_; i++)
  {
    shapeArr_[i]->scale(zoomFactor);
    point_t shapeCenter = shapeArr_[i]->getFrameRect().pos;
    shapeArr_[i]->move((shapeCenter.x - center.x) * (zoomFactor - 1), (shapeCenter.y - center.y) * (zoomFactor - 1));
  }
}

void afanasiev::CompositeShape::rotate(double angle)
{
  double radAngle = angle * M_PI / 180;
  const double sinA = sin(radAngle);
  const double cosA = cos(radAngle);
  const point_t compositeCenter = getFrameRect().pos;

  for (size_t i = 0; i < count_; i++)
  {
    shapeArr_[i]->rotate(angle);
    const point_t shapeCenter = shapeArr_[i]->getFrameRect().pos;
    const double beforeX = shapeCenter.x - compositeCenter.x;
    const double beforeY = shapeCenter.y - compositeCenter.y;
    const double afterX = beforeX * cosA - beforeY * sinA;
    const double afterY = beforeX * sinA + beforeY * cosA;
    shapeArr_[i]->move({afterX + compositeCenter.x, afterY + compositeCenter.y});
  }

}

size_t afanasiev::CompositeShape::getSize() const
{
  return count_;
}

afanasiev::Shape::shape_array afanasiev::CompositeShape::getShapes() const
{
  shape_array shapes(std::make_unique<shape_ptr[]>(count_));

  for (size_t i = 0; i < count_; i++)
  {
    shapes[i] = shapeArr_[i];
  }

  return shapes;
}

void afanasiev::CompositeShape::add(shape_ptr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Invalid pointer");
  }

  shape_array shapes(std::make_unique<shape_ptr[]>(count_ + 1));

  for (size_t i = 0; i < count_; i++)
  {
    shapes[i] = shapeArr_[i];
  }

  shapes[count_] = shape;
  count_++;
  shapeArr_.swap(shapes);
}

void afanasiev::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::invalid_argument("Invalid index");
  }

  for (size_t i = index; i < (count_ - 1); i++)
  {
    shapeArr_[i] = shapeArr_[i + 1];
  }

  count_--;
}
