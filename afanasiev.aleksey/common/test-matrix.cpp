#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(testMatrixCopyConstructor)
{
  afanasiev::Shape::shape_ptr testRectanglePtr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {2, 2}, 2, 3);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {1, 1}, 1);

  afanasiev::CompositeShape testComposition(testRectanglePtr);
  testComposition.add(testCirclePtr);

  afanasiev::Matrix testMatrix = part(testComposition);

  afanasiev::Matrix copyMatrix(testMatrix);

  BOOST_CHECK(testMatrix == copyMatrix);
  BOOST_CHECK_EQUAL(testMatrix.getRows(), copyMatrix.getRows());
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), copyMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(testMatrixMoveConstructor)
{
  afanasiev::Shape::shape_ptr testRectanglePtr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {2, 2}, 2, 3);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {1, 1}, 1);

  afanasiev::CompositeShape testComposition(testRectanglePtr);
  testComposition.add(testCirclePtr);

  afanasiev::Matrix testMatrix = part(testComposition);

  afanasiev::Matrix copyMatrix(testMatrix);
  afanasiev::Matrix moveMatrix(std::move(testMatrix));

  BOOST_CHECK(copyMatrix == moveMatrix);
  BOOST_CHECK_EQUAL(copyMatrix.getRows(), moveMatrix.getRows());
  BOOST_CHECK_EQUAL(copyMatrix.getColumns(), moveMatrix.getColumns());
  BOOST_CHECK_EQUAL(testMatrix.getRows(), 0);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(testMatrixCopyOperator)
{
  afanasiev::Shape::shape_ptr testRectanglePtr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {2, 2}, 2, 3);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {1, 1}, 1);

  afanasiev::CompositeShape testComposition(testRectanglePtr);
  testComposition.add(testCirclePtr);

  afanasiev::Matrix testMatrix = part(testComposition);

  afanasiev::Matrix copyMatrix;
  copyMatrix = testMatrix;

  BOOST_CHECK(testMatrix == copyMatrix);
  BOOST_CHECK_EQUAL(testMatrix.getRows(), copyMatrix.getRows());
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), copyMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(testMatrixMoveOperator)
{
  afanasiev::Shape::shape_ptr testRectanglePtr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {2, 2}, 2, 3);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {1, 1}, 1);

  afanasiev::CompositeShape testComposition(testRectanglePtr);
  testComposition.add(testCirclePtr);

  afanasiev::Matrix testMatrix = part(testComposition);

  afanasiev::Matrix copyMatrix(testMatrix);
  afanasiev::Matrix moveMatrix;
  moveMatrix = std::move(testMatrix);

  BOOST_CHECK(copyMatrix == moveMatrix);
  BOOST_CHECK_EQUAL(copyMatrix.getRows(), moveMatrix.getRows());
  BOOST_CHECK_EQUAL(copyMatrix.getColumns(), moveMatrix.getColumns());
  BOOST_CHECK_EQUAL(testMatrix.getRows(), 0);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(invalidMatrixParameteres)
{
  afanasiev::Shape::shape_ptr testRectanglePtr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {2, 2}, 2, 3);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {1, 1}, 1);

  afanasiev::CompositeShape testComposition(testRectanglePtr);
  testComposition.add(testCirclePtr);

  afanasiev::Matrix testMatrix = part(testComposition);

  BOOST_CHECK_THROW(testMatrix[10], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END();
