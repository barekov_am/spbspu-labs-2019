#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(TestCompositeShape)

BOOST_AUTO_TEST_CASE(testCompositeShapeCopyConstructor)
{
  afanasiev::Shape::shape_ptr testRectanglePtr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {2, 2}, 2, 3);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {1, 1}, 1);

  afanasiev::CompositeShape testComposition(testRectanglePtr);
  testComposition.add(testCirclePtr);

  const afanasiev::rectangle_t initialComposition = testComposition.getFrameRect();
  const double initialArea = testComposition.getArea();

  afanasiev::CompositeShape copyComposition(testComposition);

  BOOST_CHECK_CLOSE(initialComposition.pos.x, copyComposition.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.pos.y, copyComposition.getFrameRect().pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.width, copyComposition.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.height, copyComposition.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, copyComposition.getArea(), ACCURACY);
  BOOST_CHECK_EQUAL(testComposition.getSize(), copyComposition.getSize());
}

BOOST_AUTO_TEST_CASE(testCompositeShapeMoveConstructor)
{
  afanasiev::Shape::shape_ptr testRectanglePtr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {2, 2}, 2, 3);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {1, 1}, 1);

  afanasiev::CompositeShape testComposition(testRectanglePtr);
  testComposition.add(testCirclePtr);

  const afanasiev::rectangle_t initialComposition = testComposition.getFrameRect();
  const double initialArea = testComposition.getArea();
  const size_t initialSize = testComposition.getSize();

  afanasiev::CompositeShape moveComposition(std::move(testComposition));

  BOOST_CHECK_CLOSE(initialComposition.pos.x, moveComposition.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.pos.y, moveComposition.getFrameRect().pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.width, moveComposition.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.height, moveComposition.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, moveComposition.getArea(), ACCURACY);
  BOOST_CHECK_EQUAL(initialSize, moveComposition.getSize());
  BOOST_CHECK_CLOSE(testComposition.getArea(), 0, ACCURACY);
  BOOST_CHECK_EQUAL(testComposition.getSize(), 0);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeCopyOperator)
{
  afanasiev::Shape::shape_ptr testRectanglePtr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {2, 2}, 2, 3);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {1, 1}, 1);

  afanasiev::CompositeShape testComposition(testRectanglePtr);
  testComposition.add(testCirclePtr);

  const afanasiev::rectangle_t initialComposition = testComposition.getFrameRect();
  const double initialArea = testComposition.getArea();

  afanasiev::CompositeShape copyComposition;
  copyComposition = testComposition;

  BOOST_CHECK_CLOSE(initialComposition.pos.x, copyComposition.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.pos.y, copyComposition.getFrameRect().pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.width, copyComposition.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.height, copyComposition.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, copyComposition.getArea(), ACCURACY);
  BOOST_CHECK_EQUAL(testComposition.getSize(), copyComposition.getSize());
}

BOOST_AUTO_TEST_CASE(testCompositeShapeMoveOperator)
{
  afanasiev::Shape::shape_ptr testRectanglePtr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {2, 2}, 2, 3);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {1, 1}, 1);

  afanasiev::CompositeShape testComposition(testRectanglePtr);
  testComposition.add(testCirclePtr);

  const afanasiev::rectangle_t initialComposition = testComposition.getFrameRect();
  const double initialArea = testComposition.getArea();
  const size_t initialSize = testComposition.getSize();

  afanasiev::CompositeShape moveComposition;
  moveComposition = std::move(testComposition);

  BOOST_CHECK_CLOSE(initialComposition.pos.x, moveComposition.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.pos.y, moveComposition.getFrameRect().pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.width, moveComposition.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.height, moveComposition.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, moveComposition.getArea(), ACCURACY);
  BOOST_CHECK_EQUAL(initialSize, moveComposition.getSize());
  BOOST_CHECK_CLOSE(testComposition.getArea(), 0, ACCURACY);
  BOOST_CHECK_EQUAL(testComposition.getSize(), 0);
}

BOOST_AUTO_TEST_CASE(constCompositeShapeAfterMovingByIncrements)
{
  afanasiev::Shape::shape_ptr testRectanglePtr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {2, 2}, 2, 3);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {1, 1}, 1);

  afanasiev::CompositeShape testComposition(testRectanglePtr);
  testComposition.add(testCirclePtr);

  const afanasiev::rectangle_t initialComposition = testComposition.getFrameRect();
  const double initialArea = testComposition.getArea();

  testComposition.move(2, 3);

  BOOST_CHECK_CLOSE(initialComposition.width, testComposition.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.height, testComposition.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testComposition.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(constCompositeShapeAfterMovingToPoint)
{
  afanasiev::Shape::shape_ptr testRectanglePtr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {2, 2}, 2, 3);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {1, 1}, 1);

  afanasiev::CompositeShape testComposition(testRectanglePtr);
  testComposition.add(testCirclePtr);

  const afanasiev::rectangle_t initialComposition = testComposition.getFrameRect();
  const double initialArea = testComposition.getArea();

  testComposition.move({5, 10});

  BOOST_CHECK_CLOSE(initialComposition.width, testComposition.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.height, testComposition.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testComposition.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(quadraticChangeCompositeShapeAreaAfterScaling)
{
  afanasiev::Shape::shape_ptr testRectanglePtr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {2, 2}, 2, 3);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {1, 1}, 1);

  afanasiev::CompositeShape testComposition(testRectanglePtr);
  testComposition.add(testCirclePtr);

  const double zoomFactor_1 = 2;
  const double zoomFactor_2 = 0.5;
  double initialArea = testComposition.getArea();

  testComposition.scale(zoomFactor_1);

  BOOST_CHECK_CLOSE(testComposition.getArea(), initialArea * zoomFactor_1 * zoomFactor_1, ACCURACY);

  initialArea = testComposition.getArea();

  testComposition.scale(zoomFactor_2);

  BOOST_CHECK_CLOSE(testComposition.getArea(), initialArea * zoomFactor_2 * zoomFactor_2, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeAfterRotating)
{
  afanasiev::Shape::shape_ptr testRectangle_1_ptr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {-1, 1}, 2, 2);
  afanasiev::Shape::shape_ptr testRectangle_2_ptr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {1, -1}, 2, 2);

  afanasiev::CompositeShape testComposition(testRectangle_1_ptr);
  testComposition.add(testRectangle_2_ptr);

  const afanasiev::rectangle_t initialComposition = testComposition.getFrameRect();
  const double initialArea = testComposition.getArea();

  testComposition.rotate(45);

  BOOST_CHECK_CLOSE(initialComposition.width * sqrt(2), testComposition.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.height * sqrt(2) / 2, testComposition.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.pos.x, testComposition.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.pos.y, testComposition.getFrameRect().pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testComposition.getArea(), ACCURACY);

  testComposition.rotate(-45);

  const double d = testRectangle_1_ptr->getFrameRect().width * (sqrt(2) - 1) / 2;

  testRectangle_1_ptr->rotate(45);

  BOOST_CHECK_CLOSE(initialComposition.width + d, testComposition.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialComposition.height + d, testComposition.getFrameRect().height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(changeCompositeShapeAreaAfterAddingShape)
{
  afanasiev::Shape::shape_ptr testRectangle_1_ptr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {2, 2}, 2, 3);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {1, 1}, 1);
  afanasiev::Shape::shape_ptr testRectangle_2_ptr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {4, 4}, 4, 6);

  afanasiev::CompositeShape testComposition(testRectangle_1_ptr);
  testComposition.add(testCirclePtr);

  const double initialArea = testComposition.getArea();

  testComposition.add(testRectangle_2_ptr);

  BOOST_CHECK_CLOSE(testComposition.getArea(), initialArea + testRectangle_2_ptr->getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(changeCompositeShapeAreaAfterDeletingShape)
{
  afanasiev::Shape::shape_ptr testRectangle_1_ptr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {2, 2}, 2, 3);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {1, 1}, 1);
  afanasiev::Shape::shape_ptr testRectangle_2_ptr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {4, 4}, 4, 6);

  afanasiev::CompositeShape testComposition(testRectangle_1_ptr);
  testComposition.add(testCirclePtr);
  testComposition.add(testRectangle_2_ptr);

  const double initialArea = testComposition.getArea();

  testComposition.remove(1);

  BOOST_CHECK_CLOSE(testComposition.getArea(), initialArea - testCirclePtr->getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidCompositeShapeParameteres)
{
  afanasiev::CompositeShape testComposition;

  BOOST_CHECK_THROW(testComposition.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(testComposition.scale(2), std::logic_error);

  afanasiev::Shape::shape_ptr testRectanglePtr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {2, 2}, 2, 3);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {1, 1}, 1);

  testComposition.add(testRectanglePtr);
  testComposition.add(testCirclePtr);

  BOOST_CHECK_THROW(testComposition[4], std::invalid_argument);
  BOOST_CHECK_THROW(testComposition.scale(-2), std::invalid_argument);
  BOOST_CHECK_THROW(testComposition.remove(4), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
