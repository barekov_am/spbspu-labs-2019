#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <iostream>
#include "shape.hpp"

namespace afanasiev
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &source);
    CompositeShape(CompositeShape &&source);
    CompositeShape(shape_ptr shape);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &rhs);
    CompositeShape &operator =(CompositeShape &&rhs);
    shape_ptr operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &newPosition) override;
    void move(double dx, double dy) override;
    void scale(double zoomFactor) override;
    void rotate(double angle) override;
    size_t getSize() const;
    shape_array getShapes() const;
    void add(shape_ptr shape);
    void remove(size_t index);

  private:
    size_t count_;
    shape_array shapeArr_;
  };
} //namespace afanasiev

#endif //COMPOSITE_SHAPE_HPP
