#include "matrix.hpp"

afanasiev::Matrix::Matrix():
  rows_(0),
  columns_(0)
{
}

afanasiev::Matrix::Matrix(const Matrix &source):
  rows_(source.rows_),
  columns_(source.columns_),
  list_(std::make_unique<afanasiev::Shape::shape_ptr[]>(source.rows_ * source.columns_))
{
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    list_[i] = source.list_[i];
  }
}

afanasiev::Matrix::Matrix(Matrix &&source):
  rows_(source.rows_),
  columns_(source.columns_),
  list_(std::move(source.list_))
{
  source.rows_ = 0;
  source.columns_ = 0;
}

afanasiev::Matrix &afanasiev::Matrix::operator =(const Matrix &rhs)
{
  if (this != &rhs)
  {
    afanasiev::Shape::shape_array shapes(std::make_unique<afanasiev::Shape::shape_ptr[]>(rhs.rows_ * rhs.columns_));
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;

    for (size_t i = 0; i < (rows_ * columns_); i++)
    {
      shapes[i] = rhs.list_[i];
    }

    list_.swap(shapes);
  }

  return *this;
}

afanasiev::Matrix &afanasiev::Matrix::operator =(Matrix &&rhs)
{
  if (this != &rhs)
  {
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
    list_ = std::move(rhs.list_);
    rhs.rows_ = 0;
    rhs.columns_ = 0;
    rhs.list_ = nullptr;
  }

  return *this;
}

afanasiev::Shape::shape_array afanasiev::Matrix::operator [](size_t index) const
{
  if (index >= rows_)
  {
    throw std::out_of_range("Invalid index");
  }

  afanasiev::Shape::shape_array shapes(std::make_unique<afanasiev::Shape::shape_ptr[]>(columns_));

  for (size_t i = 0; i < columns_; i++)
  {
    shapes[i] = list_[index * columns_ + i];
  }

  return shapes;
}

bool afanasiev::Matrix::operator ==(const Matrix &rhs) const
{
  if ((rows_ != rhs.rows_) || (columns_ != rhs.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (list_[i] != rhs.list_[i])
    {
      return false;
    }
  }

  return true;
}

bool afanasiev::Matrix::operator !=(const Matrix &rhs) const
{
  return !(*this == rhs);
}

size_t afanasiev::Matrix::getRows() const
{
  return rows_;
}

size_t afanasiev::Matrix::getColumns() const
{
  return rows_;
}

size_t afanasiev::Matrix::getLayerSize(size_t layer) const
{
  if (layer >= rows_)
  {
    return 0;
  }

  for (size_t i = 0; i < columns_; i++)
  {
    if (list_[layer * columns_ + i] == nullptr)
    {
      return i;
    }
  }

  return columns_;
}

void afanasiev::Matrix::add(afanasiev::Shape::shape_ptr shape, size_t row, size_t column)
{
  if ((row > rows_) || (column > columns_))
  {
    std::out_of_range("Invalid indexes");
  }

  size_t rows = (row == rows_) ? (rows_ + 1) : rows_;
  size_t columns = (column == columns_) ? (columns_ + 1) : columns_;

  afanasiev::Shape::shape_array shapes(std::make_unique<afanasiev::Shape::shape_ptr[]>(rows * columns));

  for (size_t i = 0; i < rows; i++)
  {
    for (size_t j = 0; j < columns; j++)
    {
      if ((i == rows_) || (j == columns_))
      {
        shapes[i * columns + j] = nullptr;
      }
      else
      {
        shapes[i * columns + j] = list_[i * columns_ + j];
      }
    }
  }

  shapes[row * columns + column] = shape;
  list_.swap(shapes);
  rows_ = rows;
  columns_ = columns;
}
