#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

namespace afanasiev
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &source);
    Matrix(Matrix &&source);
    ~Matrix() = default;

    Matrix &operator =(const Matrix &rhs);
    Matrix &operator =(Matrix &&rhs);
    Shape::shape_array operator [](size_t index) const;
    bool operator ==(const Matrix &rhs) const;
    bool operator !=(const Matrix &rhs) const;

    size_t getRows() const;
    size_t getColumns() const;
    size_t getLayerSize(size_t layer) const;
    void add(Shape::shape_ptr shape, size_t row, size_t column);

  private:
    size_t rows_;
    size_t columns_;
    Shape::shape_array list_;
  };
} //namespace afanasiev

#endif //MATRIX_HPP
