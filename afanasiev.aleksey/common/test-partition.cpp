#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testPartition)

BOOST_AUTO_TEST_CASE(testCorrectPartition)
{
  afanasiev::Shape::shape_ptr testRectangle_1_ptr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {5, 5}, 1, 1);
  afanasiev::Shape::shape_ptr testRectangle_2_ptr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {15, 15}, 1, 1);
  afanasiev::Shape::shape_ptr testCircle_1_ptr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {4, 5}, 3);
  afanasiev::Shape::shape_ptr testCircle_2_ptr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {14, 15}, 2);

  afanasiev::CompositeShape testComposition(testRectangle_1_ptr);
  testComposition.add(testRectangle_2_ptr);
  testComposition.add(testCircle_1_ptr);
  testComposition.add(testCircle_2_ptr);

  afanasiev::Matrix testMatrix = part(testComposition);

  BOOST_REQUIRE_EQUAL(testMatrix.getRows(), 2);
  BOOST_REQUIRE_EQUAL(testMatrix.getLayerSize(0), 2);
  BOOST_REQUIRE_EQUAL(testMatrix.getLayerSize(1), 2);

  BOOST_CHECK_EQUAL(testMatrix[0][0], testRectangle_1_ptr);
  BOOST_CHECK_EQUAL(testMatrix[0][1], testRectangle_2_ptr);
  BOOST_CHECK_EQUAL(testMatrix[1][0], testCircle_1_ptr);
  BOOST_CHECK_EQUAL(testMatrix[1][1], testCircle_2_ptr);
}

BOOST_AUTO_TEST_CASE(testOverlap)
{
  afanasiev::Shape::shape_ptr testRectangle_1_ptr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {5, 5}, 1, 1);
  afanasiev::Shape::shape_ptr testRectangle_2_ptr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {15, 15}, 1, 1);
  afanasiev::Shape::shape_ptr testCirclePtr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {4, 5}, 3);

  BOOST_CHECK(afanasiev::isOverlap(testRectangle_1_ptr, testCirclePtr));
  BOOST_CHECK(!(afanasiev::isOverlap(testRectangle_2_ptr, testCirclePtr)));
}

BOOST_AUTO_TEST_SUITE_END();
