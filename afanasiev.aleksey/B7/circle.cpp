#include <iostream>

#include "circle.hpp"

Circle::Circle():
  Shape()
{
}

Circle::Circle(const point_t &center):
  Shape(center)
{
}

void Circle::draw() const
{
  std::cout << "CIRCLE (" << center_.x << ";" << center_.y << ")" << std::endl;
}
