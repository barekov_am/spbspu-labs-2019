#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

class Triangle : public Shape
{
public:
  Triangle();
  Triangle(const point_t &center);
  void draw() const override;
};

#endif //TRIANGLE_HPP
