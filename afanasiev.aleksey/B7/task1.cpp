#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <cmath>

void task1()
{
  std::vector<double> vector1{std::istream_iterator<double>(std::cin), std::istream_iterator<double>()};
  std::vector<double> vector2;
  vector2.resize(vector1.size());

  if (!std::cin.eof())
  {
    throw std::runtime_error("Reading is failed\n");
  }

  auto multiplicationByPI = std::bind(std::multiplies<>(), std::placeholders::_1, M_PI);
  std::transform(vector1.begin(), vector1.end(), vector2.begin(), multiplicationByPI);
  std::copy(vector2.begin(), vector2.end(), std::ostream_iterator<double>(std::cout, "\n"));
}
