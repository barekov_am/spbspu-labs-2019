#include <iostream>

#include "triangle.hpp"

Triangle::Triangle():
  Shape()
{
}

Triangle::Triangle(const point_t &center):
  Shape(center)
{
}

void Triangle::draw() const
{
  std::cout << "TRIANGLE (" << center_.x << ";" << center_.y << ")" << std::endl;
}
