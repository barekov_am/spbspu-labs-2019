#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

class Circle : public Shape
{
public:
  Circle();
  Circle(const point_t &center);
  void draw() const override;
};

#endif //CIRCLE_HPP
