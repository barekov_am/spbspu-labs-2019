#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "shape.hpp"

class Square : public Shape
{
public:
  Square();
  Square(const point_t &center);
  void draw() const override;
};

#endif //SQUARE_HPP
