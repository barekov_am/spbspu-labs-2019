#include <iostream>

#include "square.hpp"

Square::Square():
  Shape()
{
}

Square::Square(const point_t &center):
  Shape(center)
{
}

void Square::draw() const
{
  std::cout << "SQUARE (" << center_.x << ";" << center_.y << ")" << std::endl;
}
