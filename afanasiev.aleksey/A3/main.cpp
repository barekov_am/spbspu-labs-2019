#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

void viewShapeInfo(const afanasiev::Shape::shape_ptr &figure)
{
  afanasiev::rectangle_t frameRect = figure->getFrameRect();
  std::cout << "~~~~~" << std::endl;
  std::cout << "Position of center: (";
  std::cout << frameRect.pos.x << ", ";
  std::cout << frameRect.pos.y << ")" << std::endl;
  std::cout << "Frame Width: " << frameRect.width << std::endl;
  std::cout << "Frame Height: " << frameRect.height << std::endl;
  std::cout << "Area is " << figure->getArea() << std::endl;
  std::cout << "~~~~~" << std::endl;
}

void viewCompositeShapeInfo(const afanasiev::CompositeShape &composition)
{
  afanasiev::rectangle_t frameRect = composition.getFrameRect();
  std::cout << "Info about a COMPOSITE SHAPE" << std::endl;
  std::cout << "//////////" << std::endl;
  std::cout << "Position of center: (" << frameRect.pos.x << ", ";
  std::cout << frameRect.pos.y << ")" << std::endl;
  std::cout << "Frame Width: " << frameRect.width << std::endl;
  std::cout << "Frame Height: " << frameRect.height << std::endl;
  std::cout << "Area is " << composition.getArea() << std::endl;

  for (size_t i = 0; i < composition.getSize(); i++)
  {
    std::cout << "\n Info about " << i << " shape" << std::endl;
    viewShapeInfo(composition[i]);
  }
  
  std::cout << "//////////" << std::endl;
}

int main()
{
  afanasiev::Shape::shape_ptr rectangle_1_ptr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {15, 20}, 50, 60);
  afanasiev::Shape::shape_ptr circle_1_ptr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {20, 25}, 55);
  afanasiev::Shape::shape_ptr rectangle_2_ptr = std::make_shared<afanasiev::Rectangle>(afanasiev::point_t {10, 30}, 25, 25);
  afanasiev::Shape::shape_ptr circle_2_ptr = std::make_shared<afanasiev::Circle>(afanasiev::point_t {30, 10}, 25);

  std::cout << "CREATING COMPOSITE SHAPE..." << std::endl;
  afanasiev::CompositeShape composition(rectangle_1_ptr);
  viewCompositeShapeInfo(composition);

  std::cout << "MOVING ON INCREMENT..." << std::endl;
  composition.move(15, 20);
  viewCompositeShapeInfo(composition);

  std::cout << "ADDING NEW SHAPES..." << std::endl;
  composition.add(circle_1_ptr);
  composition.add(rectangle_2_ptr);
  composition.add(circle_2_ptr);
  viewCompositeShapeInfo(composition);

  std::cout << "MOVING TO A NEW POINT..." << std::endl;
  afanasiev::point_t newPoint = {40, 50};
  composition.move(newPoint);
  viewCompositeShapeInfo(composition);

  std::cout << "DELETING SOME SHAPES..." << std::endl;
  composition.remove(1);
  composition.remove(2);
  viewCompositeShapeInfo(composition);

  std::cout << "SCALING..." << std::endl;
  composition.scale(2);
  viewCompositeShapeInfo(composition);

  return 0;
}
