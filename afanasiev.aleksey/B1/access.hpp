#ifndef ACCESS_HPP
#define ACCESS_HPP

#include <cstddef>
#include <iterator>

template<typename Container>
struct Access
{
  struct Operator
  {
    static typename Container::reference getValue(Container &collection, size_t index)
    {
      return collection[index];
    }

    static size_t getBegin(Container &)
    {
      return 0;
    }

    static size_t getEnd(Container &collection)
    {
      return collection.size();
    }

    static size_t getNext(size_t index)
    {
      return index + 1;
    }
  };

  struct At
  {
    static typename Container::reference getValue(Container &collection, size_t index)
    {
      return collection.at(index);
    }

    static size_t getBegin(Container &)
    {
      return 0;
    }

    static size_t getEnd(Container &collection)
    {
      return collection.size();
    }

    static size_t getNext(size_t index)
    {
      return index + 1;
    }
  };

  struct Iterator
  {
    static typename Container::reference getValue(Container &, typename Container::iterator iterator)
    {
      return *iterator;
    }

    static typename Container::iterator getBegin(Container &collection)
    {
      return collection.begin();
    }

    static typename Container::iterator getEnd(Container &collection)
    {
      return collection.end();
    }

    static typename Container::iterator getNext(typename Container::iterator iterator)
    {
      return iterator++;
    }
  };
};

#endif //ACCESS_HPP
