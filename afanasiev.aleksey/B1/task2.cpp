#include <iostream>
#include <fstream>
#include <vector>
#include <memory>

#include "additions.hpp"

void task2(const char *filename)
{
  std::ifstream inputFile(filename, std::ios_base::binary);

  if (!inputFile)
  {
    throw std::ios_base::failure("Couldn't open input file\n");
  }

  size_t capacity = 16;
  size_t size = 0;
  std::unique_ptr<char[], decltype(&free)> data(static_cast<char*>(malloc(capacity)), free);

  if (!data)
  {
    throw std::runtime_error("Error allocate memory");
  }

  while (inputFile)
  {
    inputFile.read(&data[size], capacity - size);
    size += inputFile.gcount();

    if (size == capacity)
    {
      capacity *= 2;
      std::unique_ptr<char[], decltype(&free)> tmp(static_cast<char*>(realloc(data.get(), capacity)), free);

      if (tmp)
      {
        data.release();
        swap(data, tmp);
      }
      else
      {
        throw std::runtime_error("Error reallocate memory");
      }
    }
  }

  if (!inputFile.eof() && inputFile.fail())
  {
    throw std::runtime_error("Couldn't read input file successfully\n");
  }

  std::vector<char> vector(&data[0], &data[size]);

  for (decltype(vector)::const_iterator i = vector.begin(); i != vector.end(); i++)
  {
    std::cout << *i;
  }

  inputFile.close();
}
