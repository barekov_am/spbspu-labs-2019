#ifndef ADDITIONS_HPP
#define ADDITIONS_HPP

#include <cstring>
#include <iostream>

#include "access.hpp"

template<typename Access, typename Container>
void sort(Container &collection, const char *typeSorting)
{
  const auto begin = Access::getBegin(collection);
  const auto end = Access::getEnd(collection);

  for (auto i = begin; i != end; i++)
  {
    for (auto j = Access::getNext(i); j != end; j++)
    {
      typename Container::value_type &a = Access::getValue(collection, i);
      typename Container::value_type &b = Access::getValue(collection, j);

      if ((std::strcmp(typeSorting, "ascending") == 0 && (a > b))
          || (std::strcmp(typeSorting, "descending") == 0 && (a < b)))
      {
        std::swap(a, b);
      }
    }
  }
}

template<typename Container>
void print(const Container &collection)
{
  for (typename Container::const_iterator i = collection.begin(); i != collection.end(); i++)
  {
    std::cout << *i << ' ';
  }

  std::cout << std::endl;
}

#endif //ADDITIONS_HPP
