#include <vector>
#include <forward_list>

#include "additions.hpp"

void task1(const char *typeSorting)
{
  if ((std::strcmp(typeSorting, "ascending") != 0)
      && (std::strcmp(typeSorting, "descending") != 0))
  {
    throw std::invalid_argument("Incorrect type of sorting\n");
  }

  std::vector<int> vector1;
  int value = 0;

  while (std::cin >> value, !std::cin.eof())
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Couldn't read input file successfully\n");
    }

    vector1.push_back(value);
  }

  if (vector1.empty())
  {
    return;
  }

  std::vector<int> vector2 = vector1;
  std::forward_list<int> simplyList(vector1.begin(), vector1.end());

  sort<Access<decltype(vector1)>::Operator>(vector1, typeSorting);
  print(vector1);

  sort<Access<decltype(vector2)>::At>(vector2, typeSorting);
  print(vector2);

  sort<Access<decltype(simplyList)>::Iterator>(simplyList, typeSorting);
  print(simplyList);
}
