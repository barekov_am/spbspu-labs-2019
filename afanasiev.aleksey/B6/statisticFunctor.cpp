#include <limits>

#include "statisticFunctor.hpp"

StatisticFunctor::StatisticFunctor() :
  max_(std::numeric_limits<int>::min()),
  min_(std::numeric_limits<int>::max()),
  count_(0),
  positive_(0),
  negative_(0),
  oddSum_(0),
  evenSum_(0),
  first_(0),
  last_(0)
{
}

void StatisticFunctor::operator()(long long int num)
{
  count_++;

  if (num > max_)
  {
    max_ = num;
  }

  if (num < min_)
  {
    min_ = num;
  }

  if (num > 0)
  {
    positive_++;
  }

  if (num < 0)
  {
    negative_++;
  }

  if (num % 2 != 0)
  {
    oddSum_+= num;
  }
  else
  {
    evenSum_ += num;
  }

  if (count_ == 1)
  {
    first_ = num;
  }

  last_ = num;
}

long long int StatisticFunctor::getMin() const
{
  return min_;
}

long long int StatisticFunctor::getMax() const
{
  return max_;
}

long double StatisticFunctor::getMean() const
{
  return static_cast<double>(oddSum_+ evenSum_) / static_cast<double>(count_);
}

long long int StatisticFunctor::getPositive() const
{
  return positive_;
}

long long int StatisticFunctor::getNegative() const
{
  return negative_;
}

long long int StatisticFunctor::getOddSum() const
{
  return oddSum_;
}

long long int StatisticFunctor::getEvenSum() const
{
  return evenSum_;
}

bool StatisticFunctor::compare() const
{
  return first_ == last_;
}

long long int StatisticFunctor::getCount() const
{
  return count_;
}
