#include <iostream>
#include <vector>
#include <algorithm>

#include "statisticFunctor.hpp"

int main()
{
  try
  {
    std::vector<int> vector;
    int value = 0;

    while (std::cin >> value, !std::cin.eof())
    {
      if (std::cin.fail())
      {
        throw std::runtime_error("Couldn't read input file successfully\n");
      }

      vector.push_back(value);
    }

    StatisticFunctor stat = std::for_each(vector.begin(), vector.end(), StatisticFunctor());

    if (stat.getCount() == 0)
    {
      std::cout << "No Data" << std::endl;
      return 0;
    }

    std::cout << "Max: " << stat.getMax() << std::endl;
    std::cout << "Min: " << stat.getMin() << std::endl;
    std::cout << "Mean: " << stat.getMean() << std::endl;
    std::cout << "Positive: " << stat.getPositive() << std::endl;
    std::cout << "Negative: " << stat.getNegative() << std::endl;
    std::cout << "Odd Sum: " << stat.getOddSum() << std::endl;
    std::cout << "Even Sum: " << stat.getEvenSum() << std::endl;
    std::cout << "First/Last Equal: " << (stat.compare() ? "yes" : "no") << std::endl;

  }

  catch(std::exception &exception)
  {
    std::cerr << exception.what() << std::endl;
    return 1;
  }

  return 0;
}
