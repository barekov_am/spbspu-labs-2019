#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <string>
#include <list>

class PhoneBook
{
public:
  struct writting_t
  {
    std::string phone;
    std::string name;
  };

  using iterator = std::list<writting_t>::iterator;

  void viewCurrentWritting(iterator iter) const;
  iterator next(iterator iter);
  iterator prev(iterator iter);
  iterator insertAfter(iterator iter, const writting_t &writting);
  iterator insertBefore(iterator iter, const writting_t &writting);
  iterator replacementCurrentWritting(iterator iter, const writting_t &writting);
  void pushBack(const writting_t &writting);
  iterator move(iterator iter, int n);
  iterator begin();
  iterator end();
  bool empty() const;
  iterator remove(iterator iter);

private:
  std::list<writting_t> writtings_;
};

#endif //PHONEBOOK_HPP
