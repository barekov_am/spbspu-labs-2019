#include <iostream>

#include "phoneBook.hpp"


void PhoneBook::viewCurrentWritting(iterator iter) const
{
  std::cout << iter->phone << " " << iter->name << std::endl;
}

PhoneBook::iterator PhoneBook::next(iterator iter)
{
  return ++iter;
}

PhoneBook::iterator PhoneBook::prev(iterator iter)
{
  return --iter;
}

PhoneBook::iterator PhoneBook::insertAfter(iterator iter, const writting_t &writting)
{
  return writtings_.insert(std::next(iter), writting);
}

PhoneBook::iterator PhoneBook::insertBefore(iterator iter, const writting_t &writting)
{
  return writtings_.insert(iter, writting);
}

PhoneBook::iterator PhoneBook::replacementCurrentWritting(iterator iter, const writting_t &writting)
{
  return writtings_.insert(writtings_.erase(iter), writting);
}

void PhoneBook::pushBack(const writting_t &writting)
{
  return writtings_.push_back(writting);
}

PhoneBook::iterator PhoneBook::move(PhoneBook::iterator iter, int n)
{
  if (n > 0)
  {
    while ((std::next(iter) != writtings_.end()) && (n > 0))
    {
      iter = std::next(iter);
      --n;
    }
  }

  if (n < 0)
  {
    while ((iter != writtings_.begin()) && (n < 0))
    {
      iter = std::prev(iter);
      ++n;
    }
  }

  return iter;
}

PhoneBook::iterator PhoneBook::begin()
{
  return writtings_.begin();
}

PhoneBook::iterator PhoneBook::end()
{
  return writtings_.end();
}

bool PhoneBook::empty() const
{
  return writtings_.empty();
}

PhoneBook::iterator PhoneBook::remove(iterator iter)
{
  return writtings_.erase(iter);
}
