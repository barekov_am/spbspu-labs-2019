#ifndef FACTORIALCONTAINER_HPP
#define FACTORIALCONTAINER_HPP

#include "factorialIterator.hpp"

class FactorialContainer
{
public:
  FactorialContainer() = default;
  FactorialIterator begin();
  FactorialIterator end();
};

#endif //FACTORIALCONTAINER_HPP
