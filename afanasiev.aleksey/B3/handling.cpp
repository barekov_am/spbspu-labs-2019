#include <iostream>
#include <cstring>
#include <sstream>

#include "handling.hpp"
#include "userInterface.hpp"

std::string readPhone(std::string &phone)
{
  if (phone.empty())
  {
    return "";
  }

  for (size_t i = 0; i < phone.length(); i++)
  {
    if (!std::isdigit(phone[i]))
    {
      return "";
    }
  }

  return phone;
}

std::string readName(std::string &name)
{
  if (name.empty())
  {
    return "";
  }
  
  if (name.front() != '\"')
  {
    return "";
  }
  
  name.erase(name.begin());

  size_t i = 0;
  
  while (i < name.size() && name[i] != '\"')
  {
    if (name[i] == '\\')
    {
      if (name[i + 1] == '\"' && i + 2 < name.size())
      {
        name.erase(i, 1);
      }
      else
      {
        return "";
      }
    }
    ++i;
  }

  if (i == name.size())
  {
    return "";
  }

  name.erase(i);

  if (name.empty())
  {
    return "";
  }
  
  return name;
}

std::string readMark(std::string &mark)
{
  if (mark.empty())
  {
    return "";
  }
  
  for (size_t i = 0; i < mark.length(); i++)
  {
    if ((!isalnum(mark[i])) && (mark[i] != '-'))
    {
      return "";
    }
  }

  return mark;
}

void execute(UserInterface &interface, std::string &str)
{
  std::string command;
  std::stringstream stream(str);
  stream >> command;

  if (command == "add")
  {
    std::string phone;
    std::string name;
    stream >> phone >> name;
    phone = readPhone(phone);

    if (!stream.eof())
    {
      std::getline(stream, str);
      name += str;
    }

    name = readName(name);

    if (!stream.eof() || phone.empty() || name.empty())
    {
      std::cout << "<INVALID COMMAND>" << std::endl;

      return;
    }

    PhoneBook::writting_t writting = {phone, name};
    interface.add(writting);
  }
  else if (command == "store")
  {
    std::string mark;
    std::string newMark;
    stream >> mark >> newMark;
    mark = readMark(mark);
    newMark = readMark(newMark);

    if (!stream.eof() || mark.empty() || newMark.empty())
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }

    interface.store(mark, newMark);
  }
  else if (command == "insert")
  {
    std::string position;
    std::string mark;
    std::string phone;
    std::string name;
    stream >> position >> mark >> phone >> name;
    mark = readMark(mark);
    phone = readPhone(phone);

    if (!stream.eof())
    {
      std::getline(stream, str);
      name += str;
    }

    name = readName(name);

    if (!stream.eof() || ((position != "before") && (position != "after")) || mark.empty() || phone.empty() || name.empty())
    {
      std::cout << "<INVALID COMMAND>" << std::endl;

      return;
    }

    PhoneBook::writting_t writting = {phone, name};
    interface.insert(position, mark, writting);
  }
  else if (command == "delete")
  {
    std::string mark;
    stream >> mark;
    mark = readMark(mark);

    if (!stream.eof() || mark.empty())
    {
      std::cout << "<INVALID COMMAND>" << std::endl;

      return;
    }

    interface.remove(mark);
  }
  else if (command == "show")
  {
    std::string mark;
    stream >> mark;
    mark = readMark(mark);

    if (!stream.eof() || mark.empty())
    {
      std::cout << "<INVALID COMMAND>" << std::endl;

      return;
    }

    interface.show(mark);
  }
  else if (command == "move")
  {
    std::string mark;
    std::string steps;
    stream >> mark >> steps;
    mark = readMark(mark);

    if (!stream.eof())
    {
      std::cout << "<INVALID COMMAND>" << std::endl;

      return;
    }

    if (steps == "first")
    {
      interface.move(mark, steps);
    }
    else if (steps == "last")
    {
      interface.move(mark, steps);
    }
    else
    {
      int num = 1;

      if (steps[0] == '+')
      {
        steps.erase(0, 1);
      }

      if (steps[0] == '-')
      {
        num = -1;
        steps.erase(0, 1);
      }

      steps = readPhone(steps);

      if (steps.empty())
      {
        std::cout << "<INVALID STEP>" << std::endl;

        return;
      }

      num *= stoi(steps);
      interface.move(mark, num);
    }
  }
  else
  {
    std::cout << "<INVALID COMMAND>" << std::endl;
  }
}
