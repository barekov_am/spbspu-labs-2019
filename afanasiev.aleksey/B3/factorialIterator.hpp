#ifndef FACTORIAL_HPP
#define FACTORIAL_HPP

#include <iterator>

class FactorialIterator: public std::iterator<std::bidirectional_iterator_tag, long long>
{
public:
  FactorialIterator();
  FactorialIterator(int index);
  reference operator *();
  pointer operator ->();
  FactorialIterator &operator ++();
  FactorialIterator operator ++(int);
  FactorialIterator &operator --();
  FactorialIterator operator --(int);
  bool operator ==(FactorialIterator iter) const;
  bool operator !=(FactorialIterator iter) const;

private:
  long long value_;
  int index_;
  long long getValue(int index) const;
};


#endif //FACTORIAL_HPP
