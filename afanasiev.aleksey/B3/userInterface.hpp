#ifndef USERINTERFACE_HPP
#define USERINTERFACE_HPP

#include <map>

#include "phoneBook.hpp"

class UserInterface
{
public:
  UserInterface();

  void add(const PhoneBook::writting_t &writting);
  void store(const std::string &mark, const std::string &newMark);
  void insert(const std::string position, const std::string &mark, PhoneBook::writting_t &writting);
  void remove(const std::string &mark);
  void show(const std::string &mark);
  void move(const std::string &mark, int n);
  void move(const std::string &mark, const std::string position);

private:
  PhoneBook book_;
  std::map<std::string, PhoneBook::iterator> marks_;
};

#endif //USERINTERFACE_HPP
