#ifndef HANDLING_HPP
#define HANDLING_HPP

#include "userInterface.hpp"

std::string readPhone(std::string &phone);
std::string readName(std::string &name);
std::string readMark(std::string &mark);
void execute(UserInterface &interface, std::string &str);

#endif //HANDLING_HPP
