#include <iostream>
#include "handling.hpp"

void task1()
{
  UserInterface interface;
  std::string str;

  while (std::getline(std::cin, str))
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Invalid reading");
    }

    execute(interface, str);
  }
}
