#include <iostream>
#include <string>

#include "userInterface.hpp"

UserInterface::UserInterface()
{
  marks_["current"] = book_.begin();
}

void UserInterface::add(const PhoneBook::writting_t &writting)
{
  book_.pushBack(writting);

  if (std::next(book_.begin()) == book_.end())
  {
    marks_["current"] = book_.begin();
  }
}

void UserInterface::store(const std::string &mark, const std::string &newMark)
{
  auto iter = marks_.find(mark);

  if (iter == marks_.end())
  {
    std::cout << "<INVALID BOOKMARK>" << std::endl;

    return;
  }

  marks_.emplace(newMark, iter->second);
}

void UserInterface::insert(const std::string position, const std::string &mark, PhoneBook::writting_t &writting)
{
  auto iter = marks_.find(mark);

  if (iter == marks_.end())
  {
    std::cout << "<INVALID BOOKMARK>" << std::endl;

    return;
  }

  if (iter->second == book_.end())
  {
    add(writting);
  }

  if (position == "before")
  {
    book_.insertBefore(iter->second, writting);
  }
  else
  {
    book_.insertAfter(iter->second, writting);
  }
}

void UserInterface::remove(const std::string &mark)
{
  auto iter = marks_.find(mark);

  if (iter == marks_.end())
  {
    std::cout << "<INVALID BOOKMARK>" << std::endl;

    return;
  }

  auto tmp = iter->second;

  for (auto i = marks_.begin(); i != marks_.end(); i++)
  {
    if (i->second == tmp)
    {
      if (std::next(i->second) == book_.end())
      {
        i->second = book_.prev(tmp);
      }
      else
      {
        i->second = book_.next(tmp);
      }
    }
  }
  
  book_.remove(tmp);
}

void UserInterface::show(const std::string &mark)
{
  auto iter = marks_.find(mark);

  if (iter == marks_.end())
  {
    std::cout << "<INVALID BOOKMARK>" << std::endl;

    return;
  }
  
  if (book_.empty())
  {
    std::cout << "<EMPTY>" << std::endl;

    return;
  }

  book_.viewCurrentWritting(iter->second);
}

void UserInterface::move(const std::string &mark, int n)
{
  auto iter = marks_.find(mark);

  if (iter == marks_.end())
  {
    std::cout << "<INVALID BOOKMARK>" << std::endl;

    return;
  }

  iter->second = book_.move(iter->second, n);
}

void UserInterface::move(const std::string &mark, const std::string position)
{
  auto iter = marks_.find(mark);

  if (iter == marks_.end())
  {
    std::cout << "<INVALID BOOKMARK>" << std::endl;

    return;
  }

  if (position == "first")
  {
    iter->second = book_.begin();
  }
  else
  {
    iter->second = book_.prev(book_.end());
  }
}
