#define _USE_MATH_DEFINES

#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

Triangle::Triangle(const point_t &point0, const point_t &point1, const point_t &point2) :
  point0_(point0),
  point1_(point1),
  point2_(point2),
  center_({ (point0_.x + point1_.x + point2_.x) / 3, (point0_.y + point1_.y + point2_.y) / 3 })
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("It is a straight line, triangle cannot be located on the line.");
  }
}

double Triangle::getArea() const
{
  return sqrt(fabs((point0_.x - point2_.x) * (point1_.y - point2_.y) - (point0_.y - point2_.y) * (point1_.x - point2_.x)) / 2);
}

rectangle_t Triangle::getFrameRect() const
{
  const double maxX = std::max({point0_.x, point1_.x, point2_.x});
  const double maxY = std::max({point0_.y, point1_.y, point2_.y});
  const double minX = std::min({point0_.x, point1_.x, point2_.x});
  const double minY = std::min({point0_.y, point1_.y, point2_.y});

  const double height = maxY - minY;
  const double width = maxX - minX;

  const point_t center = { minX + width / 2, minY + height / 2 };

  return { center , height, width };
}

void Triangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;

  point0_.x += dx;
  point0_.y += dy;

  point1_.x += dx;
  point1_.y += dy;

  point2_.x += dx;
  point2_.y += dy;
}

void Triangle::move(const point_t &center)
{
  move((center.x - center_.x), (center.y - center_.y));
}

void Triangle::showInformation() const
{
  rectangle_t boundingRect = getFrameRect();
  std::cout << "\nTriangle"
            << "\nBounding rectangle: "
            << "\nHeight: " << boundingRect.height
            << "\nWidth: " << boundingRect.width
            << "\nArea: " << getArea()
            << "\nCenter position: "
            << "\nX: " << center_.x
            << "\nY: " << center_.y;
}
