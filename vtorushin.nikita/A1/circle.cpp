#define _USE_MATH_DEFINES

#include "circle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

Circle::Circle(const point_t &center, double radius) :
  center_(center),
  radius_(radius)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("It is not a circle.");
  }
}

double Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

void Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Circle::move(const point_t &center)
{
  center_ = center;
}

rectangle_t Circle::getFrameRect() const
{
  return { center_, radius_ * 2, radius_ * 2 };
}

void Circle::showInformation() const
{
  rectangle_t boundingRect = getFrameRect();
  std::cout << "\nCircle: "
            << "\nBounding rectangle: "
            << "\nHeight: " << boundingRect.height
            << "\nWidth: " << boundingRect.width
            << "\nArea: " << getArea()
            << "\nCenter position: "
            << "\nX: " << center_.x
            << "\nY: " << center_.y;
}
