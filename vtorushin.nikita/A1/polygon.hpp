#ifndef POLYGON_HPP
#define POLYGON_HPP
#include <memory>
#include "shape.hpp"

class Polygon : public Shape
{
public:
  using point_array = std::unique_ptr<point_t[]>;

  Polygon(const Polygon &other);
  Polygon(Polygon &&other);
  Polygon(size_t quantity, point_t *vertices);

  ~Polygon() = default;

  Polygon &operator =(const Polygon &other);
  Polygon &operator =(Polygon &&other);

  point_t getCenter() const;
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  
  void move(double moveX, double moveY) override;
  void move(const point_t &newPoint) override;
  void showInformation() const override;
private:
  size_t quantity_;
  point_array vertices_;

  bool isConvex() const;
};

#endif
