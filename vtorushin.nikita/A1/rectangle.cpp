#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>

Rectangle::Rectangle(const point_t &center, double height, double width) :
  center_(center),
  height_(height),
  width_(width)
{
  if ((height_ <= 0.0) || (width_ <= 0.0))
  {
    throw std::invalid_argument("It is not a rectangle.");
  }
}

double Rectangle::getArea() const
{
  return height_ * width_;
}

void Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Rectangle::move(const point_t &center)
{
  center_ = center;
}

rectangle_t Rectangle::getFrameRect() const
{
  return { center_, height_, width_ };
}

void Rectangle::showInformation() const
{
  rectangle_t boundingRect = getFrameRect();
  std::cout << "\nRectangle: "
            << "\nBounding rectangle: "
            << "\nHeight: " << boundingRect.height
            << "\nWidth: " << boundingRect.width
            << "\nArea: " << getArea()
            << "\nCenter position: "
            << "\nX: " << center_.x
            << "\nY: " << center_.y;
}
