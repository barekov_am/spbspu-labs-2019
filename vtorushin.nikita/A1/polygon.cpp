#include "polygon.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

// COPY CONSTRUCTOR
Polygon::Polygon(const Polygon &other) :
  quantity_(other.quantity_),
  vertices_(std::make_unique<point_t[]>(other.quantity_))
{
  for (size_t i = 0; i < quantity_; i++)
  {
    vertices_[i] = other.vertices_[i];
  }
}

// MOVE CONSTRUCTOR
Polygon::Polygon(Polygon &&other) :
  quantity_(other.quantity_),
  vertices_(std::move(other.vertices_))
{
  other.quantity_ = 0;
}

// SIMPLE CONSTRUCTOR
Polygon::Polygon(size_t quantity, point_t *vertices) :
  quantity_(quantity),
  vertices_(std::make_unique<point_t[]>(quantity))
{
  if ((quantity < 3) || (vertices == nullptr))
  {
    throw std::invalid_argument("Incorrect data");
  }

  for (size_t i = 0; i < quantity_; i++)
  {
    vertices_[i] = vertices[i];
  }

  if (!isConvex() || (getArea() == 0.0))
  {
    throw std::invalid_argument("Such a set of points does not create the desired polygon");
  }
}

// COPY ASSIGNMENT OPERATOR
Polygon &Polygon::operator =(const Polygon &other)
{
  if (&other != this)
  {
    quantity_ = other.quantity_;
    point_array temp(std::make_unique<point_t[]>(other.quantity_));

    for (size_t i = 0; i < quantity_; i++)
    {
      temp[i] = other.vertices_[i];
    }
    vertices_.swap(temp);
  }
  return *this;
}

// MOVE ASSIGNMENT OPERATOR
Polygon &Polygon::operator =(Polygon &&other)
{
  if (&other != this)
  {
    quantity_ = other.quantity_;
    vertices_ = std::move(other.vertices_);
    other.quantity_ = 0;
  }
  return *this;
}

point_t Polygon::getCenter() const
{
  point_t center = { 0.0, 0.0 };

  for (size_t i = 0; i < quantity_; i++)
  {
    center.x += vertices_[i].x;
    center.y += vertices_[i].y;
  }
  return { center.x / quantity_, center.y / quantity_ };
}

double Polygon::getArea() const
{
  if (quantity_ == 0)
  {
    throw std::invalid_argument("Quantity equals zero");
  }

  double sum = 0.0;

  for (size_t i = 0; i < quantity_ - 1; i++)
  {
    sum += vertices_[i].x * vertices_[i + 1].y;
    sum -= vertices_[i + 1].x * vertices_[i].y;
  }

  sum += vertices_[quantity_ - 1].x * vertices_[0].y;
  sum -= vertices_[0].x * vertices_[quantity_ - 1].y;

  return (fabs(sum) / 2);
}

rectangle_t Polygon::getFrameRect() const
{
  if (quantity_ == 0)
  {
    throw std::invalid_argument("Quantity equals zero");
  }
  point_t max = { vertices_[0].x, vertices_[0].y };
  point_t min = { vertices_[0].x, vertices_[0].y };
  for (size_t i = 1; i < quantity_; i++)
  {
    max = { std::max(max.x, vertices_[i].x), std::max(max.y, vertices_[i].y) };
    min = { std::min(min.x, vertices_[i].x), std::min(min.y, vertices_[i].y) };
  }
  
  double height = max.y - min.y;
  double width = max.x - min.x;
  point_t center = { (max.x - min.x) / 2, (max.y - min.y) / 2 };

  return { center, height, width };
}

void Polygon::move(double moveX, double moveY)
{
  for (size_t i = 0; i < quantity_; i++)
  {
    vertices_[i].x += moveX;
    vertices_[i].y += moveY;
  }
}

void Polygon::move(const point_t &center)
{
  move(center.x - getCenter().x, center.y - getCenter().y);
}

void Polygon::showInformation() const
{
  rectangle_t rect = getFrameRect();
  std::cout << "\nPolygon with "
            << quantity_ << " vertices"
            << "\nBounding rectangle: "
            << "\nHeight: " << rect.height
            << "\nWidth: " << rect.width
            << "\nArea: " << getArea()
            << "\nCenter position: "
            << "\nX: " << getCenter().x
            << "\nY: " << getCenter().y;
}

bool Polygon::isConvex() const
{
  point_t line1 = { 0, 0 };
  point_t line2 = { 0, 0 };

  for (size_t i = 0; i < quantity_; i++)
  {
    line1.x = vertices_[(i + 1) % quantity_].x - vertices_[i].x;
    line1.y = vertices_[(i + 1) % quantity_].y - vertices_[i].y;

    line2.x = vertices_[(i + 2) % quantity_].x - vertices_[(i + 1) % quantity_].x;
    line2.y = vertices_[(i + 2) % quantity_].y - vertices_[(i + 1) % quantity_].y;

    if ((line1.x * line2.y - line1.y * line2.x) > 0.0)
    {
      return false;
    }
  }
  return true;
}
