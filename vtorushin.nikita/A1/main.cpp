#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void showParam(Shape *shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer is nullptr");
  }
  std::cout << "\nArea of Shape = "
            << shape->getArea()
            << "\nFrame: " << "\n";

  rectangle_t frame = shape->getFrameRect();

  std::cout << "\nHeight = " << frame.height
            << "\nWidth = " << frame.width
            << "\nX Point of Center: " << frame.pos.x
            << "\nY Point of Center: " << frame.pos.y << "\n";

  shape->move(2, 7);
  shape->showInformation();

  shape->move({ 2, 7 });
  shape->showInformation();

  std::cout << "\n";
}

int main()
{
  Circle circle({ 1, 2 }, 6);
  showParam(&circle);

  Triangle triangle({ 4, 5 }, { 2, 3 }, { 7, 3 });
  showParam(&triangle);

  Rectangle rectangle({ 4, 5 }, 7, 9);
  showParam(&rectangle);

  const size_t quantity = 6;
  point_t vertices[quantity] = { { -2, -2 }, { -1, 4 }, { 3, 8 }, { 6, 8 }, { 8, 7 }, { 3, 0 } };

  Polygon polygon(quantity, vertices);
  showParam(&polygon);

  return 0;
}
