#include <vector>
#include <iostream>
#include <stdexcept>
#include "detail.hpp"

void task3()
{
  std::vector<int> vec;
  int temp = 1;
  while (std::cin && !(std::cin >> temp).eof())
  {
    if (std::cin.bad() || std::cin.fail())
    {
      throw std::ios_base::failure("Incorrect input - expected integer type");
    }
    if (temp == 0)
    {
      break;
    }
    vec.push_back(temp);
  }
  if (vec.empty())
  {
    return;
  }

  if (temp != 0)
  {
    throw std::invalid_argument("The input sequence must end with 0");
  }

  switch (vec.back())
  {
    case 1:
    {
      auto iter = vec.begin();
      while (iter != vec.end())
      {
        iter = ((*iter) % 2 == 0) ? vec.erase(iter) : ++iter;
      }
      break;
    }

    case 2:
    {
      auto iter = vec.begin();
      while (iter != vec.end())
      {
        iter = ((*iter) % 3 == 0) ? vec.insert(iter + 1, 3, 1) : ++iter;
      }
      break;
    }
    default:
      break;
  }

  printContainer(vec);
}
