#include <vector>
#include <random>
#include "detail.hpp"

void fillRandom(double *array, int size)
{
  std::mt19937 rng(std::random_device{}());
  std::uniform_real_distribution<double> dist(-1.0, 1.0);
  for (int i = 0; i < size; i++)
  {
    array[i] = dist(rng);
  }
}

void task4(const char *argvSort, size_t size)
{
  sortType_t type = getSortType(argvSort);

  std::vector<double> vec(size);
  if (!vec.empty())
  {
    fillRandom(&vec[0], vec.size());

    printContainer(vec);

    sort<BracketAccess>(vec, type);

    printContainer(vec);
  }
}
