#ifndef DETAIL_HPP
#define DETAIL_HPP

#include <iostream>
#include <vector>
#include <forward_list>
#include <functional>

enum sortType_t
{
  ascending, descending
};

template<typename C>
class BracketAccess
{
public:
  typedef std::size_t indexType;

  static typename C::value_type &getElement(C &cont, size_t index)
  {
    return cont[index];
  }

  static size_t getBegin(const C &)
  {
    return 0;
  }

  static size_t getEnd(const C &cont)
  {
    return cont.size();
  }

  static size_t getNextIndex(size_t cur)
  {
    return cur + 1;
  }
};

template<typename C>
class AtAccess
{
public:
  typedef std::size_t indexType;

  static typename C::value_type &getElement(C &cont, size_t index)
  {
    return cont.at(index);
  }

  static size_t getBegin(const C &)
  {
    return 0;
  }

  static size_t getEnd(const C &cont)
  {
    return cont.size();
  }

  static size_t getNextIndex(size_t cur)
  {
    return cur + 1;
  }
};

template<typename C>
class IteratorAccess
{
public:
  typedef typename C::iterator indexType;

  static typename C::value_type & getElement(C &, typename C::iterator &it)
  {
    return *it;
  }

  static typename C::iterator getBegin(C &cont)
  {
    return cont.begin();
  }

  static typename C::iterator getEnd(C &cont)
  {
    return cont.end();
  }

  static typename C::iterator getNextIndex(typename C::iterator &it)
  {
    return std::next(it);
  }
};

template<template<typename C> class Cont, typename C>
void sort(C &container, sortType_t type)
{
  std::function<bool(typename C::value_type, typename C::value_type)> comp;
  if (type == ascending)
  {
    comp = std::greater<typename C::value_type>();
  }
  else if (type == descending)
  {
    comp = std::less<typename C::value_type>();
  }
  else
  {
    throw std::invalid_argument("Incorrect sort type parameter");
  }

  using indexType = typename Cont<C>::indexType;
  const indexType containerEnd = Cont<C>::getEnd(container);
  for (indexType i = Cont<C>::getBegin(container); i != containerEnd; i++)
  {
    for (indexType j = Cont<C>::getNextIndex(i); j != containerEnd; j++)
    {
      typename C::value_type &thisEl = Cont<C>::getElement(container, i);
      typename C::value_type &nextEl = Cont<C>::getElement(container, j);
      if (comp(thisEl, nextEl))
      {
        std::swap(thisEl, nextEl);
      }
    }
  }
}

template<typename T>
void printContainer(const T &v)
{
  for (typename T::const_iterator it = v.cbegin(); it != v.cend(); it++)
  {
    std::cout << *it << " ";
  }
  std::cout << std::endl;
}

sortType_t getSortType(const std::string &param);

#endif
