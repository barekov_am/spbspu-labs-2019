#include <iostream>
#include <cstring>
#include "tasks.hpp"
#include "detail.hpp"

int main(int argc, char *argv[])
{
  if ((argc < 2) || (argc > 4))
  {
    std::cerr << "Problem with number of parameters\n";
    return 1;
  }
  try
  {
    char * tail = nullptr;
    int taskNumber = std::strtol(argv[1], &tail, 10);
    if (*tail)
    {
      std::cerr << "Invalid task";
      return 1;
    }

    switch (taskNumber)
    {
      case 1:
        if (argc != 3)
        {
          std::cerr << "Some problems with parameters in Task 1\n";
          return 1;
        }
        task1(argv[2]);
        break;

      case 2:
        if (argc != 3)
        {
          std::cerr << "Wrong argc for Task 1\n";
          return 1;
        }
        task2(argv[2]);
        break;

      case 3:
        if (argc != 2)
        {
          std::cerr << "Wrong argc for Task 1\n";
          return 1;
        }
        task3();
        break;

      case 4:
        {
          if ((argc != 4) || ((getSortType(argv[2]) != sortType_t::descending) && (getSortType(argv[2]) != sortType_t::ascending)))
          {
            std::cerr << "Problems with parameters in Task 4\n";
            return 1;
          }
          char * size_tail = nullptr;
          int size = std::strtol(argv[3], &size_tail, 10);
          if (*size_tail)
          {
            std::cerr << "Invalid size";
            return 1;
          }
          task4(argv[2], size);
          break;
        }

      default:
        std::cerr << "Incorrect number of task\n";
        return 1;
    }
  }

  catch (std::invalid_argument & err)
  {
    std::cerr << err.what() << '\n';
    return 1;
  }
  catch (std::exception &err)
  {
    std::cerr << err.what() << "\n";
    return 2;
  }

  return 0;
}
