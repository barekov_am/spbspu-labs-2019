#ifndef TASKS_HPP
#define TASKS_HPP

#include <cstddef>

void task1(const char *argv);
void task2(const char *argv);
void task3();
void task4(const char *argvSort, size_t size);

#endif
