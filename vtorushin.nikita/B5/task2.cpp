#include <iostream>
#include <string>
#include "functions.hpp"

void task2()
{
  std::vector<Shape> shapeContainer;
  std::string line;
  ShapeInfo_t shapeInfo;
  std::vector<Point_t> points;

  readWords(shapeContainer, line);
  receiveInfo(shapeInfo, shapeContainer);
  removePentagons(shapeContainer);
  createVectorPoints(shapeContainer, points);
  std::sort(shapeContainer.begin(), shapeContainer.end(), isLess);
  printInfo(shapeContainer, points, shapeInfo);
}
