#include <set>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

void task1()
{
  std::string inputLine;
  std::set<std::string> setOfWords;

  while (std::getline(std::cin, inputLine))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed during reading!\n");
    }

    std::stringstream strStream(inputLine);
    std::string word;
    while (strStream >> word)
    {
      setOfWords.insert(word);
    }
  }

  for (const auto& word : setOfWords)
  {
    std::cout << word << "\n";
  }
}
