#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <string>
#include <algorithm>

#include "shape.hpp"

Shape readPoints(std::string &, std::size_t);
void readWords(std::vector<Shape> &, std::string &);
void receiveInfo(ShapeInfo_t &, std::vector<Shape> &);
void removePentagons(std::vector<Shape> &);
void createVectorPoints(std::vector<Shape> &, std::vector<Point_t>);
bool isRectangle(const Shape &);
bool isSquare(const Shape &);
bool isLess(const Shape &, const Shape &);
void printInfo(std::vector<Shape> &, std::vector<Point_t>, ShapeInfo_t &);

#endif
