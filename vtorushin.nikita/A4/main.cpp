#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

void showParam(vtorushin::Shape *shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer is nullptr");
  }
  std::cout << "\nArea of Shape = "
            << shape->getArea()
            << "\nFrame: " << "\n";

  vtorushin::rectangle_t frame = shape->getFrameRect();

  std::cout << "\nHeight = " << frame.height
            << "\nWidth = " << frame.width
            << "\nX Point of Center: " << frame.pos.x
            << "\nY Point of Center: " << frame.pos.y << "\n";

  shape->showInformation();

  shape->move(2, 2);
  shape->showInformation();

  shape->move({ 4, 4 });
  shape->showInformation();

  shape->scale(2);
  shape->showInformation();

  shape->scale(0.5);
  shape->showInformation();

  shape->rotate(90);
  shape->showInformation();

  std::cout << "\n";
}

int main()
{
  vtorushin::Circle circle({ 3, 8 }, 1);
  showParam(&circle);

  vtorushin::Triangle triangle({ 4, 4 }, { 6, 4 }, { 4, 6 });
  showParam(&triangle);

  vtorushin::Rectangle rectangle({ 3, 3 }, 2, 2);
  showParam(&rectangle);

  const size_t quantity = 4;
  vtorushin::point_t vertices[quantity]
      = { { -5, -6 }, { -6, 5 }, { -5, 4 }, { 4, -5 } };

  vtorushin::Polygon polygon(quantity, vertices);
  showParam(&polygon);

  // Creating the first composite shape

  vtorushin::CompositeShape::pointer circle_
      = std::make_shared<vtorushin::Circle>(circle);

  vtorushin::CompositeShape composite(circle_);

  vtorushin::CompositeShape::pointer polygon_
      = std::make_shared<vtorushin::Polygon>(polygon);
  composite.add(polygon_);

  vtorushin::CompositeShape::pointer triangle_
      = std::make_shared<vtorushin::Triangle>(triangle);
  composite.add(triangle_);

  vtorushin::CompositeShape::pointer rectangle_
      = std::make_shared<vtorushin::Rectangle>(rectangle);
  composite.add(rectangle_);

  showParam(&composite);

  vtorushin::Matrix matrix = vtorushin::part(composite);

  return 0;
}
