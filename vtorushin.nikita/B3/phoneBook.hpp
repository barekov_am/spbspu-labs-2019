#ifndef PHONE_BOOK_HPP
#define PHONE_BOOK_HPP

#include <iterator>
#include <list>
#include <string>

class PhoneBook
{
public:
  struct record_t
  {
    std::string name;
    std::string number;
  };

  using iterator = std::list<record_t>::iterator;

  void showTemporaryRecord(iterator);
  iterator next(iterator);
  iterator previous(iterator);
  iterator move(iterator, int);

  iterator insert(iterator, const record_t &);
  void pushBack(const record_t &);
  iterator replaceTemporaryRecord(iterator, const record_t &);
  iterator remove(iterator);

  iterator begin();
  iterator end();

  bool empty();

private:
  std::list<record_t> list_;
};

#endif
