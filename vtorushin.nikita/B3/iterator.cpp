#include "iterator.hpp"

const int MIN = 1;
const int MAX = 10;

Iterator::Iterator():
        value_(1),
        index_(1)
{}

Iterator::Iterator(int index):
        value_(getValue(index)),
        index_(index)
{
  if ((index < MIN) || (index > MAX + 1))
  {
    throw std::out_of_range("Index is out of range!\n");
  }
}

unsigned long long Iterator::getValue(int index, unsigned long long acc) const
{
  return (index > MIN) ? getValue(index - 1, acc * index) : acc;
}

long long & Iterator::operator*()
{
  return value_;
}

long long * Iterator::operator->()
{
  return &value_;
}

Iterator & Iterator::operator++()
{
  if (index_ > MAX)
  {
    throw std::out_of_range("Index is out of range!\n");
  }

  ++index_;
  value_ *= index_;
  return *this;
}

Iterator Iterator::operator++(int)
{
  Iterator tempIterator = *this;
  ++(*this);
  return tempIterator;
}

Iterator & Iterator::operator--()
{
  if (index_ <= MIN)
  {
    throw std::out_of_range("Index is out of range!\n");
  }

  value_ /= index_;
  --index_;
  return *this;
}

Iterator Iterator::operator--(int)
{
  Iterator tempIterator = *this;
  --(*this);
  return tempIterator;
}

bool Iterator::operator ==(Iterator iter) const
{
  return (value_ == iter.value_) && (index_ == iter.index_);
}

bool Iterator::operator !=(Iterator iter) const
{
  return !(*this == iter);
}
