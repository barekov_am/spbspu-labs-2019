#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <string>
#include "phoneBookInterface.hpp"

std::string getName(std::string &);
bool checkNumber(const std::string &);
bool checkMarkName(const std::string &);
void add(PhoneBookInterface &, std::stringstream &);
void store(PhoneBookInterface &, std::stringstream &);
void insert(PhoneBookInterface &, std::stringstream &);
void deleteRecord(PhoneBookInterface &, std::stringstream &);
void show(PhoneBookInterface &, std::stringstream &);
void move(PhoneBookInterface &, std::stringstream &);

#endif
