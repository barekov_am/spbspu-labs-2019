#define _USE_MATH_DEFINES

#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

vtorushin::Triangle::Triangle(const point_t &point0, const point_t &point1, const point_t &point2) :
  point0_(point0),
  point1_(point1),
  point2_(point2),
  center_({ (point0_.x + point1_.x + point2_.x) / 3, (point0_.y + point1_.y + point2_.y) / 3 })
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("It is a straight line, triangle cannot be located on the line.");
  }
}

double vtorushin::Triangle::getArea() const
{
  return fabs((point0_.x - point2_.x) * (point1_.y - point2_.y)
      - (point0_.y - point2_.y) * (point1_.x - point2_.x)) / 2;
}

vtorushin::rectangle_t vtorushin::Triangle::getFrameRect() const
{
  const double maxX = std::max({point0_.x, point1_.x, point2_.x});
  const double maxY = std::max({point0_.y, point1_.y, point2_.y});
  const double minX = std::min({point0_.x, point1_.x, point2_.x});
  const double minY = std::min({point0_.y, point1_.y, point2_.y});

  const double height = maxY - minY;
  const double width = maxX - minX;

  const point_t center = { minX + width / 2, minY + height / 2 };

  return { center, height, width };
}

void vtorushin::Triangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;

  point0_.x += dx;
  point0_.y += dy;

  point1_.x += dx;
  point1_.y += dy;

  point2_.x += dx;
  point2_.y += dy;
}

void vtorushin::Triangle::move(const point_t &center)
{
  move((center.x - center_.x), (center.y - center_.y));
}

void vtorushin::Triangle::scale(double scaleFactor)
{
  if (scaleFactor <= 0.0)
  {
    throw std::invalid_argument("Invalid scale factor.");
  }
  rectangle_t tempRect = getFrameRect();
  point0_.x += (point0_.x - tempRect.pos.x) * (scaleFactor - 1);
  point0_.y += (point0_.y - tempRect.pos.y) * (scaleFactor - 1);

  point1_.x += (point1_.x - tempRect.pos.x) * (scaleFactor - 1);
  point1_.y += (point1_.y - tempRect.pos.y) * (scaleFactor - 1);
  
  point2_.x += (point2_.x - tempRect.pos.x) * (scaleFactor - 1);
  point2_.y += (point2_.y - tempRect.pos.y) * (scaleFactor - 1);
}

void vtorushin::Triangle::rotate(double angle)
{
  if (angle <= 0)
  {
    throw std::invalid_argument("Angle must be positive for rotation");
  }

  const double cos = std::cos((2 * M_PI * angle) / 360);
  const double sin = std::sin((2 * M_PI * angle) / 360);

  const point_t center = getFrameRect().pos;
  point_t tempPoint = point0_;

  tempPoint.x = center.x + (point0_.x - center.x) * cos - (point0_.y - center.y) * sin;
  tempPoint.y = center.y + (point0_.x - center.x) * sin + (point0_.y - center.y) * cos;
  point0_.x = tempPoint.x;
  point0_.y = tempPoint.y;

  tempPoint.x = center.x + (point1_.x - center.x) * cos - (point1_.y - center.y) * sin;
  tempPoint.y = center.y + (point1_.x - center.x) * sin + (point1_.y - center.y) * cos;
  point1_.x = tempPoint.x;
  point1_.y = tempPoint.y;

  tempPoint.x = center.x + (point2_.x - center.x) * cos - (point2_.y - center.y) * sin;
  tempPoint.y = center.y + (point2_.x - center.x) * sin + (point2_.y - center.y) * cos;
  point2_.x = tempPoint.x;
  point2_.y = tempPoint.y;
}

void vtorushin::Triangle::showInformation() const
{
  rectangle_t boundingRect = getFrameRect();
  std::cout << "\nTriangle"
            << "\nBounding rectangle: "
            << "\nHeight: " << boundingRect.height
            << "\nWidth: " << boundingRect.width
            << "\nArea: " << getArea()
            << "\nCenter position: "
            << "\nX: " << boundingRect.pos.x
            << "\nY: " << boundingRect.pos.y
            << "\nVERTICES:"
            << "\n1) " << point0_.x << " ," << point0_.y
            << "\n2) " << point1_.x << " ," << point1_.y
            << "\n3) " << point2_.x << " ," << point2_.y;
}
