#include "polygon.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

// COPY CONSTRUCTOR
vtorushin::Polygon::Polygon(const Polygon &other) :
  quantity_(other.quantity_),
  vertices_(std::make_unique<point_t[]>(other.quantity_))
{
  for (size_t i = 0; i < quantity_; i++)
  {
    vertices_[i] = other.vertices_[i];
  }
}

// MOVE CONSTRUCTOR
vtorushin::Polygon::Polygon(Polygon &&other) :
  quantity_(other.quantity_),
  vertices_(std::move(other.vertices_))
{
  other.quantity_ = 0;
}

// SIMPLE CONSTRUCTOR
vtorushin::Polygon::Polygon(size_t quantity, point_t *vertices) :
  quantity_(quantity),
  vertices_(std::make_unique<point_t[]>(quantity))
{
  if (!vertices || (quantity < 3))
  {
    throw std::invalid_argument("Incorrect data");
  }

  for (size_t i = 0; i < quantity_; i++)
  {
    vertices_[i] = vertices[i];
  }

  if (!isConvex() || (getArea() == 0.0))
  {
    throw std::invalid_argument("Such a set of points does not create the desired polygon");
  }
}

// COPY ASSIGNMENT OPERATOR
vtorushin::Polygon &vtorushin::Polygon::operator =(const Polygon &other)
{
  if (&other != this)
  {
    quantity_ = other.quantity_;
    point_array temp(std::make_unique<point_t[]>(other.quantity_));

    for (size_t i = 0; i < quantity_; i++)
    {
      temp[i] = other.vertices_[i];
    }
    vertices_.swap(temp);
  }
  return *this;
}

// MOVE ASSIGNMENT OPERATOR
vtorushin::Polygon &vtorushin::Polygon::operator =(Polygon &&other)
{
  if (&other != this)
  {
    quantity_ = other.quantity_;
    vertices_ = std::move(other.vertices_);
    other.quantity_ = 0;
  }
  return *this;
}

vtorushin::point_t vtorushin::Polygon::getCenter() const
{
  point_t center = { 0.0, 0.0 };
  
  if (quantity_ == 0)
  {
    return center;
  }
  
  for (size_t i = 0; i < quantity_; i++)
  {
    center.x += vertices_[i].x;
    center.y += vertices_[i].y;
  }
  return { center.x / quantity_, center.y / quantity_ };
}

double vtorushin::Polygon::getArea() const
{
  if (quantity_ == 0)
  {
    return 0;
  }

  double sum = 0.0;

  for (size_t i = 0; i < quantity_ - 1; i++)
  {
    sum += vertices_[i].x * vertices_[i + 1].y;
    sum -= vertices_[i + 1].x * vertices_[i].y;
  }

  sum += vertices_[quantity_ - 1].x * vertices_[0].y;
  sum -= vertices_[0].x * vertices_[quantity_ - 1].y;

  return (fabs(sum) / 2);
}

vtorushin::rectangle_t vtorushin::Polygon::getFrameRect() const
{
  if (quantity_ == 0)
  {
    return { { 0, 0 }, 0, 0 };
  }
  point_t max = { vertices_[0].x, vertices_[0].y };
  point_t min = { vertices_[0].x, vertices_[0].y };
  for (size_t i = 1; i < quantity_; i++)
  {
    max = { std::max(max.x, vertices_[i].x), std::max(max.y, vertices_[i].y) };
    min = { std::min(min.x, vertices_[i].x), std::min(min.y, vertices_[i].y) };
  }
  
  double height = max.y - min.y;
  double width = max.x - min.x;
  point_t center = { min.x + (max.x - min.x) / 2, min.y + (max.y - min.y) / 2 };

  return { center, height, width };
}

void vtorushin::Polygon::move(double moveX, double moveY)
{
  for (size_t i = 0; i < quantity_; i++)
  {
    vertices_[i].x += moveX;
    vertices_[i].y += moveY;
  }
}

void vtorushin::Polygon::move(const point_t &center)
{
  move(center.x - getCenter().x, center.y - getCenter().y);
}

void vtorushin::Polygon::scale(double scaleFactor)
{
  if (scaleFactor <= 0.0)
  {
    throw std::invalid_argument("Invalid scale factor.");
  }

  rectangle_t tempRect = getFrameRect();
  for (size_t i = 0; i < quantity_; i++)
  {
    vertices_[i].x += (vertices_[i].x - tempRect.pos.x) * (scaleFactor - 1);
    vertices_[i].y += (vertices_[i].y - tempRect.pos.y) * (scaleFactor - 1);
  }
}

void vtorushin::Polygon::rotate(double angle)
{
  if (angle <= 0)
  {
    throw std::invalid_argument("Angle must be positive for rotation");
  }

  const double cos = std::cos((2 * M_PI * angle) / 360);
  const double sin = std::sin((2 * M_PI * angle) / 360);

  const point_t center = getFrameRect().pos;
  point_t tempPoint = center;

  for (size_t i = 0; i < quantity_; i++)
  {
    tempPoint.x = center.x + (vertices_[i].x - center.x) * cos - (vertices_[i].y - center.y) * sin;
    tempPoint.y = center.y + (vertices_[i].x - center.x) * sin + (vertices_[i].y - center.y) * cos;

    vertices_[i].x = tempPoint.x;
    vertices_[i].x = tempPoint.y;
  }
}

void vtorushin::Polygon::showInformation() const
{
  rectangle_t rect = getFrameRect();
  std::cout << "\nPolygon with "
            << quantity_ << " vertices"
            << "\nBounding rectangle: "
            << "\nHeight: " << rect.height
            << "\nWidth: " << rect.width
            << "\nArea: " << getArea()
            << "\nCenter position: "
            << "\nX: " << rect.pos.x
            << "\nY: " << rect.pos.y;
}

bool vtorushin::Polygon::isConvex() const
{
  point_t line1 = { 0, 0 };
  point_t line2 = { 0, 0 };

  for (size_t i = 0; i < quantity_; i++)
  {
    line1.x = vertices_[(i + 1) % quantity_].x - vertices_[i].x;
    line1.y = vertices_[(i + 1) % quantity_].y - vertices_[i].y;

    line2.x = vertices_[(i + 2) % quantity_].x - vertices_[(i + 1) % quantity_].x;
    line2.y = vertices_[(i + 2) % quantity_].y - vertices_[(i + 1) % quantity_].y;

    if ((line1.x * line2.y - line1.y * line2.x) > 0.0)
    {
      return false;
    }
  }
  return true;
}
