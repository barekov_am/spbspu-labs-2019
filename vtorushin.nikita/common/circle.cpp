#define _USE_MATH_DEFINES

#include "circle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

vtorushin::Circle::Circle(const point_t &center, double radius) :
  center_(center),
  radius_(radius)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("It is not a circle.");
  }
}

double vtorushin::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

void vtorushin::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void vtorushin::Circle::move(const point_t &center)
{
  center_ = center;
}

vtorushin::rectangle_t vtorushin::Circle::getFrameRect() const
{
  return { center_, radius_ * 2, radius_ * 2 };
}

void vtorushin::Circle::scale(double scaleFactor)
{
  if (scaleFactor <= 0.0)
  {
    throw std::invalid_argument("Invalid scale factor.");
  }

  radius_ *= scaleFactor;
}

void vtorushin::Circle::rotate(double angle)
{
  if (angle <= 0)
  {
    throw std::invalid_argument("Angle must be positive for rotation");
  }
}

void vtorushin::Circle::showInformation() const
{
  rectangle_t boundingRect = getFrameRect();
  std::cout << "\nCircle: "
            << "\nBounding rectangle: "
            << "\nHeight: " << boundingRect.height
            << "\nWidth: " << boundingRect.width
            << "\nArea: " << getArea()
            << "\nCenter position: "
            << "\nX: " << boundingRect.pos.x
            << "\nY: " << boundingRect.pos.y;
}
