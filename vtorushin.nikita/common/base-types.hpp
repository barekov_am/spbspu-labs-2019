#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace vtorushin
{
  struct point_t
  {
    bool operator !=(const point_t &other) const;
    bool operator ==(const point_t &other) const;
    double x;
    double y;
  };

  struct rectangle_t
  {
    bool operator !=(const rectangle_t &other) const;
    bool operator ==(const rectangle_t &other) const;
    point_t pos;
    double height;
    double width;
  };

  bool intersect(const rectangle_t &lhs, const rectangle_t &rhs);
}

#endif
