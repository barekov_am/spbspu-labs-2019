#include <stdexcept>
#include <utility>

#include <boost/test/auto_unit_test.hpp>

#include "polygon.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

using pointer = std::shared_ptr<vtorushin::Shape>;

BOOST_AUTO_TEST_SUITE(matrixTestSuite)

  BOOST_AUTO_TEST_CASE(copyAndMove)
  {
    pointer circle = std::make_shared<vtorushin::Circle>(vtorushin::point_t{ 1, 2 }, 3);
    vtorushin::CompositeShape composite;
    vtorushin::CompositeShape::pointer triangle_
        = std::make_shared<vtorushin::Triangle>(vtorushin::Triangle { { 1, -1 }, { 2, 2 }, { 3, -2 } });

    composite.add(circle);
    composite.add(triangle_);

    vtorushin::Matrix matrix = vtorushin::part(composite);

    BOOST_CHECK_NO_THROW(vtorushin::Matrix matrix2(matrix));
    BOOST_CHECK_NO_THROW(vtorushin::Matrix matrix2(std::move(matrix)));

    vtorushin::Matrix matrix2 = vtorushin::part(composite);
    vtorushin::Matrix matrix3;

    BOOST_CHECK_NO_THROW(matrix3 = matrix2);
    BOOST_CHECK_NO_THROW(matrix3 = std::move(matrix2));

    vtorushin::Matrix matrix4 = vtorushin::part(composite);
    vtorushin::Matrix matrix5;

    matrix5 = matrix4;
    BOOST_CHECK(matrix5 == matrix4);
    matrix5 = std::move(matrix4);
    BOOST_CHECK(matrix5 == matrix3);

    vtorushin::Matrix matrix6(matrix3);
    BOOST_CHECK(matrix6 == matrix3);
    vtorushin::Matrix matrix7(std::move(matrix3));
    BOOST_CHECK(matrix7 == matrix6);
  }

  BOOST_AUTO_TEST_CASE(exceptionThrow)
  {
    pointer circle = std::make_shared<vtorushin::Circle>(vtorushin::point_t{ -3, 2.5 }, 5);
    pointer rectangle = std::make_shared<vtorushin::Rectangle>(vtorushin::point_t{ 2, -4.5 }, 2, 6);

    vtorushin::point_t pointArray[] { { 4, 5 }, { 6, 4 }, { 7, -1 }, { 5, -3 }, { -2, -1 } };
    vtorushin::CompositeShape::pointer polygon_
        = std::make_shared<vtorushin::Polygon>(vtorushin::Polygon { 5, pointArray });

    vtorushin::CompositeShape composite;
    composite.add(circle);
    composite.add(rectangle);
    composite.add(polygon_);

    vtorushin::Matrix matrix = vtorushin::part(composite);

    BOOST_CHECK_THROW(matrix[3], std::invalid_argument);
    BOOST_CHECK_THROW(matrix[-1], std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
