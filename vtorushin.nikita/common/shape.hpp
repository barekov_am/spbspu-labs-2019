#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"

namespace vtorushin
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void move(const point_t &center) = 0;
    virtual void scale(double scaleFactor) = 0;
    virtual void rotate(double angle) = 0;
    virtual void showInformation() const = 0;
  };
}

#endif
