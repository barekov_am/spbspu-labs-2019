#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "triangle.hpp"

const double ErrorValue = 1e-10;

BOOST_AUTO_TEST_SUITE(triangleTestSuite)

  BOOST_AUTO_TEST_CASE(invariabilityAfterMoving)
  {
    vtorushin::Triangle testTriangle({ 6, 2 }, { 2, 2 }, { 6, 4 });
    const vtorushin::rectangle_t frameRectBefore = testTriangle.getFrameRect();
    const double areaBefore = testTriangle.getArea();

    testTriangle.move({ 5.5, -5.6 });
    BOOST_CHECK_CLOSE(frameRectBefore.width, testTriangle.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectBefore.height, testTriangle.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(areaBefore, testTriangle.getArea(), ErrorValue);

    testTriangle.move(7, -7);
    BOOST_CHECK_CLOSE(frameRectBefore.width, testTriangle.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectBefore.height, testTriangle.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(areaBefore, testTriangle.getArea(), ErrorValue);
  }

  BOOST_AUTO_TEST_CASE(areaAfterScaling)
  {
    vtorushin::Triangle testTriangle({ 0, 2 }, { -2, 0 }, { 2, 0 });
    double areaBefore = testTriangle.getArea();

    double scaleFactor = 2;
    testTriangle.scale(scaleFactor);
    BOOST_CHECK_CLOSE(testTriangle.getArea(), scaleFactor * scaleFactor * areaBefore, ErrorValue);
    areaBefore = testTriangle.getArea();

    scaleFactor = 0.5;
    testTriangle.scale(scaleFactor);
    BOOST_CHECK_CLOSE(testTriangle.getArea(), scaleFactor * scaleFactor * areaBefore, ErrorValue);
  }

  BOOST_AUTO_TEST_CASE(testTriangleRotation)
  {
    vtorushin::Triangle testTriangle({ 0, 0 }, { 4, 0 }, { 0, 3 });
    const double areaBefore = testTriangle.getArea();
    const vtorushin::rectangle_t frameRectBefore = testTriangle.getFrameRect();

    double angle = 90;
    testTriangle.rotate(angle);

    double areaAfter = testTriangle.getArea();
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, ErrorValue);

    angle = 270;
    testTriangle.rotate(angle);

    vtorushin::rectangle_t frameRectAfter = testTriangle.getFrameRect();
    areaAfter = testTriangle.getArea();

    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, ErrorValue);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, ErrorValue);
  }

  BOOST_AUTO_TEST_CASE(exceptionThrow)
  {
    BOOST_CHECK_THROW(vtorushin::Triangle({ 3, 3 }, { 2, 2 }, { 5, 5 }), std::invalid_argument);

    vtorushin::Triangle testTriangle({ 6, 2 }, { 2, 2 }, { 6, 4 });
    BOOST_CHECK_THROW(testTriangle.rotate(-30), std::invalid_argument);
    BOOST_CHECK_THROW(testTriangle.scale(-2), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
