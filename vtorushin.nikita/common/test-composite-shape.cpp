#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteCompositeShape)

  const double ErrorValue = 1e-10;

  BOOST_AUTO_TEST_CASE(CompositeShapeCopyConstructor)
  {
    vtorushin::CompositeShape::pointer rectangle_
        = std::make_shared<vtorushin::Rectangle>(vtorushin::point_t { 2, 2 }, 2, 3);

    vtorushin::CompositeShape::pointer circle_
        = std::make_shared<vtorushin::Circle>(vtorushin::point_t { 1, 1 }, 1);

    vtorushin::CompositeShape composite1_(rectangle_);
    composite1_.add(circle_);

    const vtorushin::rectangle_t frame1_ = composite1_.getFrameRect();
    const double area1_ = composite1_.getArea();

    vtorushin::CompositeShape composite2_(composite1_);

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(area1_, composite2_.getArea(), ErrorValue);
    BOOST_CHECK_EQUAL(composite1_.getSize(), composite2_.getSize());
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeMoveConstructor)
  {
    vtorushin::CompositeShape::pointer rectangle_
        = std::make_shared<vtorushin::Rectangle>(vtorushin::point_t { 2, 2 }, 2, 3);
    vtorushin::CompositeShape::pointer circle_
        = std::make_shared<vtorushin::Circle>(vtorushin::point_t { 1, 1 }, 1);
    vtorushin::CompositeShape composite1_(rectangle_);
    composite1_.add(circle_);

    const vtorushin::rectangle_t frame1_ = composite1_.getFrameRect();
    const double area_ = composite1_.getArea();
    const size_t size_ = composite1_.getSize();

    vtorushin::CompositeShape composite2_(std::move(composite1_));

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(area_, composite2_.getArea(), ErrorValue);
    BOOST_CHECK_EQUAL(size_, composite2_.getSize());
    BOOST_CHECK_CLOSE(composite1_.getArea(), 0, ErrorValue);
    BOOST_CHECK_EQUAL(composite1_.getSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeCopyOperator)
  {
    vtorushin::CompositeShape::pointer rectangle_
        = std::make_shared<vtorushin::Rectangle>(vtorushin::point_t { 2, 2 }, 2, 3);
    vtorushin::CompositeShape::pointer circle_
        = std::make_shared<vtorushin::Circle>(vtorushin::point_t { 1, 1 }, 1);
    vtorushin::CompositeShape composite_(rectangle_);
    composite_.add(circle_);

    const vtorushin::rectangle_t frame1_ = composite_.getFrameRect();
    const double area_ = composite_.getArea();

    vtorushin::CompositeShape composite2_;
    composite2_ = composite_;

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(area_, composite2_.getArea(), ErrorValue);
    BOOST_CHECK_EQUAL(composite_.getSize(), composite2_.getSize());
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeMoveOperator)
  {
    vtorushin::CompositeShape::pointer rectangle_
        = std::make_shared<vtorushin::Rectangle>(vtorushin::point_t { 2, 2 }, 2, 3);
    vtorushin::CompositeShape::pointer circle_
        = std::make_shared<vtorushin::Circle>(vtorushin::point_t { 1, 1 }, 1);
    vtorushin::CompositeShape composite1_(rectangle_);
    composite1_.add(circle_);

    const vtorushin::rectangle_t frame1_ = composite1_.getFrameRect();
    const double area_ = composite1_.getArea();
    const size_t size_ = composite1_.getSize();

    vtorushin::CompositeShape composite2_;
    composite2_ = std::move(composite1_);

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(area_, composite2_.getArea(), ErrorValue);
    BOOST_CHECK_EQUAL(size_, composite2_.getSize());
    BOOST_CHECK_CLOSE(composite1_.getArea(), 0, ErrorValue);
    BOOST_CHECK_EQUAL(composite1_.getSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeConstAfterMoving)
  {
    vtorushin::CompositeShape composite_;

    vtorushin::CompositeShape::pointer circle_
        = std::make_shared<vtorushin::Circle>(vtorushin::point_t { 1, -1 }, 2);

    vtorushin::CompositeShape::pointer rectangle_
        = std::make_shared<vtorushin::Rectangle>(vtorushin::point_t { 1, -1 }, 2, 3);

    vtorushin::CompositeShape::pointer triangle_
        = std::make_shared<vtorushin::Triangle>(vtorushin::Triangle { { 1, -1 }, { 2, 2 }, { 3, -2 } });

    vtorushin::point_t pointArray[] { { 4, 5 }, { 6, 4 }, { 7, -1 }, { 5, -3 }, { -2, -1 } };
    vtorushin::CompositeShape::pointer polygon_
        = std::make_shared<vtorushin::Polygon>(vtorushin::Polygon { 5, pointArray });

    composite_.add(circle_);
    composite_.add(rectangle_);
    composite_.add(triangle_);
    composite_.add(polygon_);

    const double firstArea = composite_.getArea();
    const vtorushin::rectangle_t firstRect = composite_.getFrameRect();

    composite_.move({ 4, 5 });

    double secondArea = composite_.getArea();
    vtorushin::rectangle_t secondRect = composite_.getFrameRect();

    BOOST_CHECK_CLOSE(firstArea, secondArea, ErrorValue);
    BOOST_CHECK_CLOSE(firstRect.height, secondRect.height, ErrorValue);
    BOOST_CHECK_CLOSE(firstRect.width, secondRect.width, ErrorValue);

    composite_.move(-2, 3);

    secondArea = composite_.getArea();
    secondRect = composite_.getFrameRect();

    BOOST_CHECK_CLOSE(firstArea, secondArea, ErrorValue);
    BOOST_CHECK_CLOSE(firstRect.height, secondRect.height, ErrorValue);
    BOOST_CHECK_CLOSE(firstRect.width, secondRect.width, ErrorValue);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeScaling)
  {
    vtorushin::CompositeShape composite_;
    vtorushin::CompositeShape::pointer circle_
        = std::make_shared<vtorushin::Circle>(vtorushin::point_t { 1, -1 }, 2);

    vtorushin::CompositeShape::pointer rectangle_
        = std::make_shared<vtorushin::Rectangle>(vtorushin::point_t { 1, -1 }, 2, 3);

    vtorushin::CompositeShape::pointer triangle_
        = std::make_shared<vtorushin::Triangle>(vtorushin::Triangle { { 1, -1 }, { 2, 2 }, { 3, -2 } });

    vtorushin::point_t pointArray[] { { 4, 5 }, { 6, 4 }, { 7, -1 }, { 5, -3 }, { 2, 1 } };
    vtorushin::CompositeShape::pointer polygon_
        = std::make_shared<vtorushin::Polygon>(vtorushin::Polygon { 5, pointArray });

    composite_.add(circle_);
    composite_.add(rectangle_);
    composite_.add(triangle_);
    composite_.add(polygon_);

    const double firstArea = composite_.getArea();
    const vtorushin::rectangle_t firstRect = composite_.getFrameRect();

    const double scaleFactor = 6;

    composite_.scale(scaleFactor);

    double secondArea = composite_.getArea();
    vtorushin::rectangle_t secondRect = composite_.getFrameRect();

    BOOST_CHECK_CLOSE(firstArea *  scaleFactor * scaleFactor, secondArea, ErrorValue);
    BOOST_CHECK_CLOSE(firstRect.width * scaleFactor, secondRect.width, ErrorValue);
    BOOST_CHECK_CLOSE(firstRect.height * scaleFactor, secondRect.height, ErrorValue);
  }

  BOOST_AUTO_TEST_CASE(operatorTests)
  {
    vtorushin::CompositeShape::pointer rectangle1_
        = std::make_shared<vtorushin::Rectangle>(vtorushin::point_t { 1, 1 }, 2, 2);
    vtorushin::CompositeShape compShape1_(rectangle1_);

    const vtorushin::rectangle_t testFrameRect = rectangle1_->getFrameRect();
    const double area = rectangle1_->getArea();

    vtorushin::CompositeShape::pointer circle1_
        = std::make_shared<vtorushin::Circle>(vtorushin::point_t { 2, 2 }, 2);
    vtorushin::CompositeShape compShape2_(circle1_);

    BOOST_CHECK_NO_THROW(vtorushin::CompositeShape compShape2_(compShape1_));
    BOOST_CHECK_NO_THROW(compShape2_= compShape1_);
    BOOST_CHECK_NO_THROW(compShape2_= std::move(compShape1_));
    BOOST_CHECK_NO_THROW(vtorushin::CompositeShape compShape2_(std::move(compShape1_)));

    compShape1_.add(rectangle1_);
    BOOST_CHECK_CLOSE(testFrameRect.width, compShape1_.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(testFrameRect.height, compShape1_.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(area, compShape1_.getArea(), ErrorValue);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeAfterRotate)
  {
    vtorushin::CompositeShape composite_;
    vtorushin::CompositeShape::pointer circle_
        = std::make_shared<vtorushin::Circle>(vtorushin::point_t { 1, -1 }, 2);

    vtorushin::CompositeShape::pointer rectangle_
        = std::make_shared<vtorushin::Rectangle>(vtorushin::point_t { 1, -1 }, 2, 3);

    vtorushin::CompositeShape::pointer triangle_
        = std::make_shared<vtorushin::Triangle>(vtorushin::Triangle { { 1, -1 }, { 2, 2 }, { 3, -2 } });

    vtorushin::point_t pointArray[] { { 4, 5 }, { 6, 4 }, { 7, -1 }, { 5, -3 }, { 2, 1 } };
    vtorushin::CompositeShape::pointer polygon_
        = std::make_shared<vtorushin::Polygon>(vtorushin::Polygon { 5, pointArray });

    composite_.add(circle_);
    composite_.add(rectangle_);
    composite_.add(triangle_);
    composite_.add(polygon_);

    const vtorushin::rectangle_t frameRectBefore = composite_.getFrameRect();
    const double areaBefore = composite_.getArea();
    const vtorushin::rectangle_t frameRectAfter = composite_.getFrameRect();

    composite_.rotate(90);
    BOOST_CHECK_CLOSE(areaBefore, composite_.getArea(), ErrorValue);
    BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.width, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.height, ErrorValue);
  }


  BOOST_AUTO_TEST_CASE(exceptionThrow)
  {
    vtorushin::CompositeShape::pointer rectangle1_
        = std::make_shared<vtorushin::Rectangle>(vtorushin::point_t { 1, 1 }, 2, 2);

    vtorushin::CompositeShape composite_(rectangle1_);

    BOOST_CHECK_THROW(vtorushin::CompositeShape composite2(nullptr), std::invalid_argument);
    BOOST_CHECK_THROW(composite_.scale(-2), std::invalid_argument);
    BOOST_CHECK_THROW(composite_.add(nullptr), std::invalid_argument);
    BOOST_CHECK_THROW(composite_.remove(5), std::invalid_argument);
    BOOST_CHECK_THROW(composite_.rotate(-180), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
