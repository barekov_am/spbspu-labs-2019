#include "base-types.hpp"

bool vtorushin::point_t::operator !=(const point_t &other) const
{
  return ((this->x != other.x) && (this->y != other.y));
}

bool vtorushin::point_t::operator ==(const point_t &other) const
{
  return !(*this != other);
}

bool vtorushin::rectangle_t::operator !=(const rectangle_t &other) const
{
  return ((this->width != other.width) && (this->height != other.height) && (this->pos != other.pos));
}

bool vtorushin::rectangle_t::operator ==(const rectangle_t &other) const
{
  return !(*this != other);
}

bool vtorushin::intersect(const rectangle_t &lhs, const rectangle_t &rhs)
{
  const point_t lhsTopLft = { lhs.pos.x - lhs.width / 2, lhs.pos.y + lhs.height / 2 };
  const point_t lhsBtmRht = { lhs.pos.x + lhs.width / 2, lhs.pos.y - lhs.height / 2 };
  const point_t rhsTopLft = { rhs.pos.x - rhs.width / 2, rhs.pos.y + rhs.height / 2 };
  const point_t rhsBtmRht = { rhs.pos.x + rhs.width / 2, rhs.pos.y - rhs.height / 2 };

  const bool topLft = (lhsTopLft.x < rhsBtmRht.x) && (rhsBtmRht.y < lhsTopLft.y);
  const bool btmRht = (rhsTopLft.x < lhsBtmRht.x) && (lhsBtmRht.y < rhsTopLft.y);

  return topLft && btmRht;
}
