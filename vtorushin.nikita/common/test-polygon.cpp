#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "polygon.hpp"

const double ErrorValue = 1e-10;

BOOST_AUTO_TEST_SUITE(polygonTestSuite)

  BOOST_AUTO_TEST_CASE(invariabilityAfterMoving)
  {
    const size_t quantity = 6; 
    vtorushin::point_t vertices[quantity] = { { -2, -2 }, { -1, 4 }, { 3, 8 }, { 6, 8 }, { 8, 7 }, { 3, 0 } };

    vtorushin::Polygon testPolygon(quantity, vertices);
    const vtorushin::rectangle_t frameRectBefore = testPolygon.getFrameRect();
    const double areaBefore = testPolygon.getArea();

    testPolygon.move({ -7, -7 });
    BOOST_CHECK_CLOSE(frameRectBefore.width, testPolygon.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectBefore.height, testPolygon.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(areaBefore, testPolygon.getArea(), ErrorValue);

    testPolygon.move(5, -7);
    BOOST_CHECK_CLOSE(frameRectBefore.width, testPolygon.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectBefore.height, testPolygon.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(areaBefore, testPolygon.getArea(), ErrorValue);
  }

  BOOST_AUTO_TEST_CASE(areaAfterScaling)
  {
    const size_t quantity = 6; 
    vtorushin::point_t vertices[quantity] = { { -2, -2 }, { -1, 4 }, { 3, 8 }, { 6, 8 }, { 8, 7 }, { 3, 0 } };

    vtorushin::Polygon testPolygon(quantity, vertices);
    double areaBefore = testPolygon.getArea();

    double scaleFactor = 2.75;
    testPolygon.scale(scaleFactor);
    BOOST_CHECK_CLOSE(testPolygon.getArea(), scaleFactor * scaleFactor * areaBefore, ErrorValue);
    areaBefore = testPolygon.getArea();

    scaleFactor = 0.75;
    testPolygon.scale(scaleFactor);
    BOOST_CHECK_CLOSE(testPolygon.getArea(), scaleFactor * scaleFactor * areaBefore, ErrorValue);
  }

  BOOST_AUTO_TEST_CASE(testPolygonRotation)
  {
    const size_t quantity = 6;
    vtorushin::point_t vertices[quantity] = { { -2, -2 }, { -1, 4 }, { 3, 8 }, { 6, 8 }, { 8, 7 }, { 3, 0 } };
    vtorushin::Polygon testPolygon(quantity, vertices);
    const double areaBefore = testPolygon.getArea();
    const vtorushin::rectangle_t frameRectBefore = testPolygon.getFrameRect();

    double angle = 90;
    testPolygon.rotate(angle);

    double areaAfter = testPolygon.getArea();

    BOOST_CHECK_CLOSE(areaAfter, areaBefore, ErrorValue);

    angle = 270;
    testPolygon.rotate(angle);
    vtorushin::rectangle_t frameRectAfter = testPolygon.getFrameRect();
    areaAfter = testPolygon.getArea();

    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, ErrorValue);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, ErrorValue);
  }

  BOOST_AUTO_TEST_CASE(exceptionThrow)
  {
    const size_t quantity1 = 6; 
    BOOST_CHECK_THROW(vtorushin::Polygon(quantity1, nullptr), std::invalid_argument);

    vtorushin::point_t vertices1[quantity1]
        = { { -2, -2 }, { 2, 2 }, { 3, 8 }, { 6, 8 }, { 8, 7 }, { 3, 0 } };
    BOOST_CHECK_THROW(vtorushin::Polygon(quantity1, vertices1), std::invalid_argument);

    vtorushin::point_t vertices2[quantity1] = { { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 } };
    BOOST_CHECK_THROW(vtorushin::Polygon(quantity1, vertices2), std::invalid_argument);

    const size_t quantity2 = 2; 
    vtorushin::point_t vertices3[quantity2] = { { -2, -2 }, { -1, 4 } };
    BOOST_CHECK_THROW(vtorushin::Polygon(quantity2, vertices3), std::invalid_argument);
  
    vtorushin::point_t vertices4[quantity1] = { { -2, -2 }, { -1, 4 }, { 3, 8 }, { 6, 8 }, { 8, 7 }, { 3, 0 } };
    vtorushin::Polygon testPolygon(quantity1, vertices4);
    BOOST_CHECK_THROW(testPolygon.scale(-6), std::invalid_argument);
    BOOST_CHECK_THROW(testPolygon.rotate(-30), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
