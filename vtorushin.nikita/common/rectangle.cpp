#include "rectangle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>

vtorushin::Rectangle::Rectangle(const point_t &center, double height, double width) :
  center_(center),
  height_(height),
  width_(width),
  vertices_{ { center.x - width / 2, center.y + height / 2 },
           { center.x + width / 2, center.y + height / 2 },
           { center.x + width / 2, center.y - height / 2},
           { center.x - width / 2, center.y - height / 2 } }
{
  if ((height_ <= 0.0) || (width_ <= 0.0))
  {
    throw std::invalid_argument("It is not a rectangle.");
  }
}

double vtorushin::Rectangle::getArea() const
{
  return height_ * width_;
}

void vtorushin::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;

  for (int i = 0; i < 4; i++)
  {
    vertices_[i].x += dx;
    vertices_[i].y += dy;
  }
}

void vtorushin::Rectangle::move(const point_t &newCenter)
{
  for (int i = 0; i < 4; i++)
  {
    vertices_[i] = { newCenter.x + vertices_[i].x - center_.x, newCenter.y + vertices_[i].y - center_.y };
  }
  
  center_ = newCenter;
}

void vtorushin::Rectangle::scale(double scaleFactor)
{
  if (scaleFactor <= 0.0)
  {
    throw std::invalid_argument("Invalid scale factor.");
  }

  width_ *= scaleFactor;
  height_ *= scaleFactor;

  for (size_t i = 0; i < 4; i++)
  {
    vertices_[i]
        = { center_.x + (vertices_[i].x - center_.x) * scaleFactor,
            center_.y + (vertices_[i].y - center_.y) * scaleFactor };
  }
}

void vtorushin::Rectangle::rotate(double angle)
{
  if (angle <= 0)
  {
    throw std::invalid_argument("Angle must be positive for rotation");
  }

  const double sin = std::sin(M_PI * angle / 180);
  const double cos = std::cos(M_PI * angle / 180);

  for (size_t i = 0; i < 4; i++)
  {
    vertices_[i]
        = { center_.x + (vertices_[i].x - center_.x) * cos - (vertices_[i].y - center_.y) * sin,
            center_.y + (vertices_[i].y - center_.y) * cos + (vertices_[i].x - center_.x) * sin };
  }
}


vtorushin::rectangle_t vtorushin::Rectangle::getFrameRect() const
{
  point_t topLft = vertices_[0];
  point_t btmRht = vertices_[2];

  for (int i = 0; i < 4; i++)
  {
    topLft.x = std::min(vertices_[i].x, topLft.x);
    topLft.y = std::max(vertices_[i].y, topLft.y);
    btmRht.x = std::max(vertices_[i].x, btmRht.x);
    btmRht.y = std::min(vertices_[i].y, btmRht.y);
  }

  point_t  center = { (topLft.x + btmRht.x) / 2, (topLft.y + btmRht.y) / 2 };

  return { center, topLft.y - btmRht.y , btmRht.x - topLft.x };
}

void vtorushin::Rectangle::showInformation() const
{
  rectangle_t boundingRect = getFrameRect();
  std::cout << "\nRectangle: "
            << "\nBounding rectangle: "
            << "\nHeight: " << boundingRect.height
            << "\nWidth: " << boundingRect.width
            << "\nArea: " << getArea()
            << "\nCenter position: "
            << "\nX: " << boundingRect.pos.x
            << "\nY: " << boundingRect.pos.y
            << "\nCoordinates of the corners of the rectangle: "
            << "\nTop lft: " << vertices_[0].x << ", " << vertices_[0].y
            << "\nTop rht: " << vertices_[1].x << ", " << vertices_[1].y
            << "\nBtm rht: " << vertices_[2].x << ", " << vertices_[2].y
            << "\nBtm lft: " << vertices_[3].x << ", " << vertices_[3].y;
}
