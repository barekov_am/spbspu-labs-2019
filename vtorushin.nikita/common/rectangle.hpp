#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace vtorushin
{
  class Rectangle: public Shape
  {
  public:
    Rectangle(const point_t &center, double height, double width);
    double getArea() const override;
    void move(double dx, double dy) override;
    void move(const point_t &newCenter) override;
    void scale(double scaleFactor) override;
    void rotate(double angle) override;
    rectangle_t getFrameRect() const override;
    void showInformation() const override;
  private:
    point_t center_;
    double height_;
    double width_;
    point_t vertices_[4];
  };
}
#endif
