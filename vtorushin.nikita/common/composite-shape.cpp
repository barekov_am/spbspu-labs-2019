#include "composite-shape.hpp"

#include <algorithm>
#include <iostream>
#include <cmath>
#include <stdexcept>

vtorushin::CompositeShape::CompositeShape():
  quantity_(0)
{ }

vtorushin::CompositeShape::CompositeShape(const CompositeShape &other):
  quantity_(other.quantity_),
  shapes_(std::make_unique<pointer[]>(other.quantity_))
{
  for (size_t i = 0; i < quantity_; i++)
  {
    shapes_[i] = other.shapes_[i];
  }
}

vtorushin::CompositeShape::CompositeShape(CompositeShape &&other) noexcept :
  quantity_(other.quantity_),
  shapes_(std::move(other.shapes_))
{
  other.quantity_ = 0;
}

vtorushin::CompositeShape::CompositeShape(const pointer &shape):
  CompositeShape()
{
  add(shape);
}

vtorushin::CompositeShape &vtorushin::CompositeShape::operator =(const CompositeShape &other)
{
  if (this != &other)
  {
    shapes_ = std::make_unique<vtorushin::CompositeShape::pointer[]>(other.quantity_);
    quantity_ = other.quantity_;
    for (size_t i = 0; i < quantity_; i++)
    {
      shapes_[i] = other.shapes_[i];
    }
  }

  return *this;
}

vtorushin::CompositeShape &vtorushin::CompositeShape::operator =(CompositeShape &&other) noexcept
{
  if (this != &other)
  {
    quantity_ = other.quantity_;
    shapes_ = std::move(other.shapes_);
    other.quantity_ = 0;
  }

  return *this;
}

vtorushin::CompositeShape::pointer vtorushin::CompositeShape::operator [](size_t index) const
{
  if (quantity_ <= index)
  {
    throw std::invalid_argument("Index is out of range");
  }

  return shapes_[index];
}

bool vtorushin::CompositeShape::operator ==(const CompositeShape &other) const
{
  if (quantity_ != other.quantity_)
  {
    return false;
  }

  for (size_t i = 0; i < quantity_; i++)
  {
    if (shapes_[i] != other.shapes_[i])
    {
      return false;
    }
  }

  return true;
}

bool vtorushin::CompositeShape::operator !=(const CompositeShape &other) const
{
  return !(other == *this);
}

void vtorushin::CompositeShape::showInformation() const
{
  if (quantity_ == 0)
  {
    std::cout << "Composite shape is empty";
  }
  else
  {
    rectangle_t rect = getFrameRect();

    std::cout << "\nComposite with "
              << quantity_ << " shapes"
              << "\nBounding rectangle: "
              << "\nHeight: " << rect.height
              << "\nWidth: " << rect.width
              << "\nArea: " << getArea()
              << "\nCenter position: "
              << "\nX: " << rect.pos.x
              << "\nY: " << rect.pos.y
              << "\nElements: " << "\n";

    for (size_t i = 0; i < quantity_; i++)
    {
      shapes_[i]->showInformation();
      std::cout << "\n";
    }

  }
}

double vtorushin::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < quantity_; i++)
  {
    area += shapes_[i]->getArea();
  }

  return area;
}

vtorushin::rectangle_t vtorushin::CompositeShape::getFrameRect() const
{
  if (quantity_ == 0)
  {
    return rectangle_t{ { 0, 0 }, 0, 0 };
  }

  rectangle_t tempRect = shapes_[0]->getFrameRect();

  double minX = tempRect.pos.x - tempRect.width / 2;
  double maxX = tempRect.pos.x + tempRect.width / 2;
  double minY = tempRect.pos.y - tempRect.height / 2;
  double maxY = tempRect.pos.y + tempRect.height / 2;

  for (size_t i = 1; i < quantity_; i++)
  {
    tempRect = shapes_[i]->getFrameRect();

    minX = std::min(minX, tempRect.pos.x - tempRect.width / 2);
    maxX = std::max(maxX, tempRect.pos.x + tempRect.width / 2);
    minY = std::min(minY, tempRect.pos.y - tempRect.height / 2);
    maxY = std::max(maxY, tempRect.pos.y + tempRect.height / 2);
  }

  return rectangle_t{ {(maxX + minX) / 2, (maxY + minY) / 2 }, maxY - minY, maxX - minX };
}

void vtorushin::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < quantity_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void vtorushin::CompositeShape::move(const point_t &center)
{
  rectangle_t frameRect = getFrameRect();
  double dx = center.x - frameRect.pos.x;
  double dy = center.y - frameRect.pos.y;
  move(dx, dy);
}

void vtorushin::CompositeShape::scale(double scaleFactor)
{
  if (scaleFactor <= 0)
  {
    throw std::invalid_argument("scaling factor can not be less than zero");
  }

  if (quantity_ != 0)
  {
    rectangle_t frameRect = getFrameRect();

    for (size_t i = 0; i < quantity_; i++)
    {
      rectangle_t currentFrameRect = shapes_[i]->getFrameRect();
      double dx = (currentFrameRect.pos.x - frameRect.pos.x) * (scaleFactor - 1);
      double dy = (currentFrameRect.pos.y - frameRect.pos.y) * (scaleFactor - 1);
      shapes_[i]->move(dx, dy);
      shapes_[i]->scale(scaleFactor);
    }
  }
}

void vtorushin::CompositeShape::rotate(double angle)
{
  if (angle <= 0)
  {
    throw std::invalid_argument("Angle must be positive for rotation");
  }
  const double sin = std::sin(M_PI * angle / 180);
  const double cos = std::cos(M_PI * angle / 180);

  const point_t compositeCenter = this->getFrameRect().pos;

  for (size_t i = 0; i < quantity_; i++)
  {
    const point_t figureCenter
        = shapes_[i]->getFrameRect().pos;

    double dx =
        compositeCenter.x + (figureCenter.x - compositeCenter.x) * cos - (figureCenter.y - compositeCenter.y) * sin;

    double dy =
        compositeCenter.y + (figureCenter.x - compositeCenter.x) * sin + (figureCenter.y - compositeCenter.y) * cos;

    shapes_[i]->move({ dx, dy });
    shapes_[i]->rotate(angle);
  }
}

size_t vtorushin::CompositeShape::getSize() const
{
  return(quantity_);
}

vtorushin::CompositeShape::array vtorushin::CompositeShape::list() const
{
  array tmpArray(std::make_unique<pointer[]>(quantity_));
  for (unsigned int i = 0; i < quantity_; i++)
  {
    tmpArray[i] = shapes_[i];
  }

  return tmpArray;
}


void vtorushin::CompositeShape::add(const pointer &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer must not be null");
  }

  array tmpShapes = std::make_unique<pointer[]>(quantity_ + 1);

  for (size_t i = 0; i < quantity_; i++)
  {
    tmpShapes[i] = shapes_[i];
  }

  tmpShapes[quantity_] = shape;
  quantity_++;
  shapes_ = std::move(tmpShapes);
}

void vtorushin::CompositeShape::remove(size_t index)
{
  if (quantity_ <= index)
  {
    throw std::invalid_argument("Can not delete shape that does not exist");
  }

  for (size_t i = index; i < (quantity_ - 1); i++)
  {
    shapes_[i] = shapes_[i+1];
  }

  quantity_--;
}
