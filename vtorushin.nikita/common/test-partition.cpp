#include <boost/test/auto_unit_test.hpp>

#include "matrix.hpp"
#include "partition.hpp"
#include "rectangle.hpp"
#include "polygon.hpp"
#include "triangle.hpp"
#include "circle.hpp"

using pointer = std::shared_ptr<vtorushin::Shape>;

BOOST_AUTO_TEST_SUITE(partitionTestSuite)

  BOOST_AUTO_TEST_CASE(correctPartition)
  {
    vtorushin::Circle circle({ 3, 8 }, 1);
    vtorushin::CompositeShape::pointer circle_
        = std::make_shared<vtorushin::Circle>(circle);

    vtorushin::CompositeShape composite(circle_);

    vtorushin::Triangle triangle({ 4, 4 }, { 6, 4 }, { 4, 6 });
    vtorushin::CompositeShape::pointer triangle_
        = std::make_shared<vtorushin::Triangle>(triangle);

    composite.add(triangle_);

    vtorushin::Rectangle rectangle({ 3, 3 }, 2, 2);
    vtorushin::CompositeShape::pointer rectangle_
        = std::make_shared<vtorushin::Rectangle>(rectangle);

    composite.add(rectangle_);


    const size_t quantity = 4;
    vtorushin::point_t vertices[quantity]
        = { { -5, -6 }, { -6, 5 }, { -5, 4 }, { 4, -5 } };
    vtorushin::Polygon polygon(quantity, vertices);
    vtorushin::CompositeShape::pointer polygon_
        = std::make_shared<vtorushin::Polygon>(polygon);

    composite.add(polygon_);

    vtorushin::Matrix testMatrix = vtorushin::part(composite);

    BOOST_CHECK_EQUAL(testMatrix.getRows(), 2);
    BOOST_CHECK_EQUAL(testMatrix.getColumns(), 3);

    BOOST_CHECK(testMatrix[0][0] == circle_);
    BOOST_CHECK(testMatrix[0][1] == triangle_);
    BOOST_CHECK(testMatrix[0][2] == rectangle_);
    BOOST_CHECK(testMatrix[1][0] == polygon_);
  }

BOOST_AUTO_TEST_SUITE_END()
