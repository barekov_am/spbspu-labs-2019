#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "circle.hpp"

const double ErrorValue = 1e-10;

BOOST_AUTO_TEST_SUITE(circleTestSuite)

BOOST_AUTO_TEST_CASE(invariabilityAfterMoving)
{
  vtorushin::Circle testCircle({ 4, 4 }, 5.2);
  const vtorushin::rectangle_t frameRectBefore = testCircle.getFrameRect();
  const double areaBefore = testCircle.getArea();

  testCircle.move({ 2.3, 1.5 });
  BOOST_CHECK_CLOSE(frameRectBefore.width, testCircle.getFrameRect().width, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectBefore.height, testCircle.getFrameRect().height, ErrorValue);
  BOOST_CHECK_CLOSE(areaBefore, testCircle.getArea(), ErrorValue);

  testCircle.move(7, -7);
  BOOST_CHECK_CLOSE(frameRectBefore.width, testCircle.getFrameRect().width, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectBefore.height, testCircle.getFrameRect().height, ErrorValue);
  BOOST_CHECK_CLOSE(areaBefore, testCircle.getArea(), ErrorValue);
}

BOOST_AUTO_TEST_CASE(areaAfterScaling)
{
  vtorushin::Circle testCircle({ 4, 4 }, 4);
  double areaBefore = testCircle.getArea();

  double scaleFactor = 2.75;
  testCircle.scale(scaleFactor);
  BOOST_CHECK_CLOSE(testCircle.getArea(), scaleFactor * scaleFactor * areaBefore, ErrorValue);
  areaBefore = testCircle.getArea();

  scaleFactor = 0.75;
  testCircle.scale(scaleFactor);
  BOOST_CHECK_CLOSE(testCircle.getArea(), scaleFactor * scaleFactor * areaBefore, ErrorValue);
}

BOOST_AUTO_TEST_CASE(testCircleRotation)
{
  vtorushin::Circle testCircle({ 5.0, 5.0 }, 2.0);
  const double areaBefore = testCircle.getArea();
  const vtorushin::rectangle_t frameRectBefore = testCircle.getFrameRect();

  double angle = 45;
  testCircle.rotate(angle);

  double areaAfter = testCircle.getArea();
  vtorushin::rectangle_t frameRectAfter = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, ErrorValue);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ErrorValue);
}

BOOST_AUTO_TEST_CASE(exceptionThrow)
{
  BOOST_CHECK_THROW(vtorushin::Circle({ 1, 2 }, -3), std::invalid_argument);

  vtorushin::Circle testCircle({ 1, 2 }, 3);
  BOOST_CHECK_THROW(testCircle.scale(-2), std::invalid_argument);
  BOOST_CHECK_THROW(testCircle.rotate(-170), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
