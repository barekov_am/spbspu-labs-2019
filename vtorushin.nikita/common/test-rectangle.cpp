#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "rectangle.hpp"

const double ErrorValue = 1e-10;

BOOST_AUTO_TEST_SUITE(rectangleTestSuite)

  BOOST_AUTO_TEST_CASE(invariabilityAfterMoving)
  {
    vtorushin::Rectangle testRectangle({ 4, 5 }, 7, 9);
    const vtorushin::rectangle_t frameRectBefore = testRectangle.getFrameRect();
    const double areaBefore = testRectangle.getArea();

    testRectangle.move({ -4, -5 });
    BOOST_CHECK_CLOSE(frameRectBefore.width, testRectangle.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectBefore.height, testRectangle.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(areaBefore, testRectangle.getArea(), ErrorValue);

    testRectangle.move(7, -7);
    BOOST_CHECK_CLOSE(frameRectBefore.width, testRectangle.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectBefore.height, testRectangle.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(areaBefore, testRectangle.getArea(), ErrorValue);
}

  BOOST_AUTO_TEST_CASE(areaAfterScaling)
  {
    vtorushin::Rectangle testRectangle({ 4, 5 }, 7, 9);
    double areaBefore = testRectangle.getArea();

    double scaleFactor = 2.75;
    testRectangle.scale(scaleFactor);
    BOOST_CHECK_CLOSE(testRectangle.getArea(), scaleFactor * scaleFactor * areaBefore, ErrorValue);
    areaBefore = testRectangle.getArea();

    scaleFactor = 0.75;
    testRectangle.scale(scaleFactor);
    BOOST_CHECK_CLOSE(testRectangle.getArea(), scaleFactor * scaleFactor * areaBefore, ErrorValue);
  }

  BOOST_AUTO_TEST_CASE(testRectangleRotation)
  {
    vtorushin::Rectangle testRectangle({ 0, 0 }, 3, 4);
    const double areaBefore = testRectangle.getArea();
    const vtorushin::rectangle_t frameRectBefore = testRectangle.getFrameRect();

    double angle = 90;
    testRectangle.rotate(angle);

    double areaAfter = testRectangle.getArea();

    BOOST_CHECK_CLOSE(areaAfter, areaBefore, ErrorValue);

    angle = 270;
    testRectangle.rotate(angle);

    vtorushin::rectangle_t frameRectAfter = testRectangle.getFrameRect();
    areaAfter = testRectangle.getArea();

    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, ErrorValue);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, ErrorValue);
  }

  BOOST_AUTO_TEST_CASE(exceptionThrow)
  {
    BOOST_CHECK_THROW(vtorushin::Rectangle({ 4, 5 }, 7, -9), std::invalid_argument);
    BOOST_CHECK_THROW(vtorushin::Rectangle({ 4, 5 }, -7, 9), std::invalid_argument);

    vtorushin::Rectangle testRectangle({ 4, 5 }, 7, 9);
    BOOST_CHECK_THROW(testRectangle.scale(-2), std::invalid_argument);
    BOOST_CHECK_THROW(testRectangle.rotate(-250), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
