#include <list>
#include <iostream>
#include <stdexcept>

void print(std::list<int>::iterator begin, std::list<int>::iterator end)
{
  if (begin == end)
  {
    std::cout << '\n';
    return;
  }

  if (begin == std::prev(end))
  {
    std::cout << *begin << '\n';
    return;
  }

  std::cout << *begin << " " << *std::prev(end) << " ";
  ++begin;
  --end;

  print(begin, end);
}

void task2()
{
  const int min = 1;
  const int max = 20;
  const size_t maxSize = 20;

  std::list<int> intList;
  std::size_t currSize = 0;

  int number;
  while (std::cin && !(std::cin >> number).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Incorrect input!\n");
    }

    if ((number < min) || (number > max))
    {
      throw std::invalid_argument("The number must be from 1 to 20!\n");
    }

    if (currSize < maxSize)
    {
      intList.push_back(number);
      ++currSize;
    }
    else
    {
      throw std::invalid_argument("The amount of list's elements is up to 20!\n");
    }
  }

  print(intList.begin(), intList.end());
}
