#include "functor.hpp"
#include <cmath>
#include <algorithm>
#include <iostream>

void Functor::operator()(double num)
{
  double numAfter = M_PI * num;
  vec_.push_back(numAfter);
}

void Functor::showAll()
{
  if (vec_.empty())
  {
    return;
  }
  std::for_each(vec_.begin(), vec_.end(),[](double num){std::cout << num << " ";});
}
