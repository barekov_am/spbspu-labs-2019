#include <iostream>
#include <stdexcept>

void part1();
void part2();

int main(int argc, char* argv[])
{
  if (argc == 1)
  {
    std::cerr << "Trouble with count of parameters\n";
    return 1;
  }
  try
  {
    char * tail = nullptr;
    size_t numTask = std::strtol(argv[1], &tail, 10);
    if (*tail)
    {
      std::cerr << "Invalid type of task-argument\n";
      return 1;
    }

    switch (numTask)
    {
      case 1:
        part1();
        break;
      case 2:
        part2();
        break;
      default:
        std::cerr << "Trouble with number of part\n";
        return 1;
    }
  }
  catch (std::exception &exception)
  {
    std::cerr << exception.what() << '\n';
    return 2;
  }
  return 0;
}
