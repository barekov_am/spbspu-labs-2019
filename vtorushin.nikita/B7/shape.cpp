#include "shape.hpp"

Shape::Shape():
 x_(0),
 y_(0)
{}

Shape::Shape(int x, int y):
 x_(x),
 y_(y)
{}

bool Shape::isMoreLeft(const std::shared_ptr<Shape> &rhs) const
{
  return (x_ < rhs->x_);
}

bool Shape::isUpper(const std::shared_ptr<Shape> &rhs) const
{
  return (y_ > rhs->y_);
}
