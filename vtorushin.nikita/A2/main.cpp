#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void showParam(vtorushin::Shape *shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer is nullptr");
  }
  std::cout << "\nArea of Shape = "
            << shape->getArea()
            << "\nFrame: " << "\n";

  vtorushin::rectangle_t frame = shape->getFrameRect();

  std::cout << "\nHeight = " << frame.height
            << "\nWidth = " << frame.width
            << "\nX Point of Center: " << frame.pos.x
            << "\nY Point of Center: " << frame.pos.y << "\n";

  shape->move(2, 7);
  shape->showInformation();

  shape->move({ 2, 7 });
  shape->showInformation();

  shape->scale(2.8);
  shape->showInformation();

  std::cout << "\n";
}

int main()
{
  vtorushin::Circle circle({ 1, 2 }, 6);
  showParam(&circle);

  vtorushin::Triangle triangle({ 4, 5 }, { 2, 3 }, { 7, 3 });
  showParam(&triangle);

  vtorushin::Rectangle rectangle({ 4, 5 }, 7, 9);
  showParam(&rectangle);

  size_t quantity = 6;
  vtorushin::point_t vertices[quantity] = { { -2, -2 }, { -1, 4 }, { 3, 8 }, { 6, 8 }, { 8, 7 }, { 3, 0 } };

  vtorushin::Polygon polygon(quantity, vertices);
  showParam(&polygon);

  return 0;
}
