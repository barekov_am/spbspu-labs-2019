#include <sstream>
#include "DataStruct.hpp"

void readVector(std::vector<DataStruct> &vector)
{
  std::string line;
  while (getline(std::cin, line))
  {
    std::istringstream input(line);
    int key[2];
    std::string str;
    char trash = 0;
    size_t i = 0;
    while(i < 2)
    {
      input >> key[i];
      input >> trash;
      if (trash != ',')
      {
        throw std::invalid_argument("Wrong with data\n");
      }
      i++;
    }
    getline(input, str);
    if (str.empty())
    {
      throw std::invalid_argument("Wrong with Data\n");
    }
    
    while ((str.at(0) == ' ') || (str.at(0) == '\t'))
    {
      str.erase(0, 1);
    }

    DataStruct dataStruct{key[0], key[1], str};
    if (outOfRange(dataStruct))
    {
      throw std::invalid_argument("Wrong with Data\n");
    }
    vector.push_back(dataStruct);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Trouble with cin\n");
  }
}

bool outOfRange(const DataStruct &data)
{
  const int MAX = 5;
  return ((std::abs(data.key1) > MAX) || (std::abs(data.key2) > MAX));
}

bool less(const DataStruct &dataLft, const DataStruct &dataRhs)
{
  if (dataLft.key1 < dataRhs.key1)
  {
    return true;
  }
  else if (dataLft.key1 == dataRhs.key1)
  {
    if (dataLft.key2 < dataRhs.key2)
    {
      return true;
    }
    else if (dataLft.key2 == dataRhs.key2)
    {
      if (dataLft.str.size() < dataRhs.str.size())
      {
        return true;
      }
    }
  }
  return false;
}

void sortVector(std::vector<DataStruct> &vector)
{
  std::sort(vector.begin(), vector.end(), less);
}

void printVector(const DataStruct &data)
{
  std::cout << data.key1 << "," << data.key2 << "," << data.str << "\n";
}
