#include "factorial.hpp"

const int MIN_SIZE = 1;
const int MAX_SIZE = 11;

FactorialIterator::FactorialIterator():
  value_(1),
  index_(1)
{
}

FactorialIterator::FactorialIterator(int index)
{
  if (index < MIN_SIZE || index > MAX_SIZE)
  {
    throw std::invalid_argument("Index is out of range");
  }

  index_ = index;
  value_ = calculateFact(index);
}

bool FactorialIterator::operator ==(FactorialIterator &iter) const
{
  return (value_ == iter.value_) && (index_ == iter.index_);
}

bool FactorialIterator::operator !=(FactorialIterator &iter) const
{
  return !(*this == iter);
}

FactorialIterator &FactorialIterator::operator ++()
{
  if (index_ > MAX_SIZE)
  {
    throw std::out_of_range("Index is out of range");
  }

  ++index_;
  value_ *= index_;
  return *this;
}

FactorialIterator FactorialIterator::operator ++(int)
{
  FactorialIterator tmpIter = *this;
  ++(*this);
  return tmpIter;
}

FactorialIterator &FactorialIterator::operator --()
{
  if (index_ <= MIN_SIZE)
  {
    throw std::out_of_range("Index is out of range");
  }

  value_ /= index_;
  --index_;
  return *this;
}

FactorialIterator FactorialIterator::operator --(int)
{
  FactorialIterator tmpIter = *this;
  --(*this);
  return tmpIter;
}

FactorialIterator::reference FactorialIterator::operator *()
{
  return value_;
}

FactorialIterator::pointer FactorialIterator::operator ->()
{
  return &value_;
}

unsigned long long FactorialIterator::calculateFact(int index) const
{
  return (index <= 1) ? 1 : (index * calculateFact(index - 1));
}

FactorialIterator FactorialContainer::begin() const
{
  return FactorialIterator(MIN_SIZE);
}

FactorialIterator FactorialContainer::end() const
{
  return FactorialIterator(MAX_SIZE);
}
