#ifndef B3_FACTORIAL_HPP
#define B3_FACTORIAL_HPP

#include <iterator>

class FactorialIterator: public std::iterator<std::bidirectional_iterator_tag, unsigned long long>
{
public:
  FactorialIterator();
  FactorialIterator(int index);

  bool operator ==(FactorialIterator &iter) const;
  bool operator !=(FactorialIterator &iter) const;

  FactorialIterator &operator ++();
  FactorialIterator operator ++(int);
  FactorialIterator &operator --();
  FactorialIterator operator --(int);

  pointer operator ->();
  reference operator *();

private:
  unsigned long long value_;
  int index_;
  unsigned long long calculateFact(int index) const;
};

class FactorialContainer
{
public:
  FactorialContainer() = default;
  FactorialIterator begin() const;
  FactorialIterator end() const;
};

#endif //B3_FACTORIAL_HPP
