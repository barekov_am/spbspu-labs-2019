#ifndef B3_BOOKMARKS_MANAGER_HPP
#define B3_BOOKMARKS_MANAGER_HPP

#include <map>
#include "phonebook.hpp"

class BookmarksManager
{
public:
  enum class insertPosition
  {
    before,
    after
  };

  enum class movePosition
  {
    first,
    last
  };


  BookmarksManager();
  void add(const PhoneBook::record_t &newRecord);
  void store(const std::string &mark, const std::string &name);
  void insert(const std::string &mark, const PhoneBook::record_t &newRecord, insertPosition direction);
  void remove(const std::string &mark);
  void show(const std::string &mark);
  void move(const std::string &mark, int steps);
  void move(const std::string &mark, movePosition position);
  void next(const std::string &mark);
  void prev(const std::string &mark);

private:
  using bookmarkType = std::map<std::string, PhoneBook::iterator>;
  using bookmarkIter = bookmarkType::iterator;

  PhoneBook phoneBook_;
  bookmarkType bookmarks_;
  bookmarkIter getIter(const std::string &mark);
};

#endif //B3_BOOKMARKS_MANAGER_HPP
