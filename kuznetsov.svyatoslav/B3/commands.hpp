#ifndef B3_COMMANDS_HPP
#define B3_COMMANDS_HPP

#include <sstream>

class BookmarksManager;

void add(BookmarksManager &manager, std::stringstream &stream);
void store(BookmarksManager &manager, std::stringstream &stream);
void insert(BookmarksManager &manager, std::stringstream &stream);
void deleteBookmark(BookmarksManager &manager, std::stringstream &stream);
void show(BookmarksManager &manager, std::stringstream &stream);
void move(BookmarksManager &manager, std::stringstream &stream);

#endif //B3_COMMANDS_HPP
