#include "functions.hpp"
#include <cctype>

std::string getNumber(std::string &number)
{
  if (number.empty())
  {
    return "";
  }

  for (size_t i = 0; i < number.size(); ++i)
  {
    if (!std::isdigit(number[i]))
    {
      return "";
    }
  }

  return number;
}

std::string getName(std::string &name)
{
  if (name.empty())
  {
    return "";
  }

  if (name.front() != '\"')
  {
    return "";
  }

  name.erase(name.begin());
  size_t i = 0;
  while ((i < name.size()) && (name[i] != '\"'))
  {
    if (name[i] == '\\')
    {
      if ((name[i + 1] == '\"') && (i + 2) < name.size())
      {
        name.erase(i, 1);
      }
      else
      {
        return "";
      }
    }

    ++i;
  }

  if (i == name.size())
  {
    return "";
  }

  name.erase(i);
  if (name.empty())
  {
    return "";
  }

  return name;
}

std::string getMarkName(std::string &name)
{
  for (size_t i = 0; i < name.size(); ++i)
  {
    if (isalnum(name[i]) || name[i] != '-')
    {
      return name;
    }
  }

  return "";
}
