#include "tasks.hpp"
#include <iostream>
#include <functional>
#include <algorithm>
#include "commands.hpp"
#include "bookmarks-manager.hpp"

struct command_t
{
  const char *name;
  std::function<void(BookmarksManager &, std::stringstream &)> function;
};


void task1()
{
  BookmarksManager manager;
  std::string line;
  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error while reading data!");
    }

    std::stringstream stream(line);
    std::string command;
    stream >> command;
    command_t commands[] =
      {
        {"add", &add},
        {"store", &store},
        {"insert", &insert},
        {"delete", &deleteBookmark},
        {"show", &show},
        {"move", &move}
      };

    auto predicate = [&](const command_t &cmnd) {return cmnd.name == command;};
    auto iter = std::find_if(std::begin(commands), std::end(commands), predicate);
    if (iter != std::end(commands))
    {
      iter->function(manager, stream);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
      return;
    }
  }
}
