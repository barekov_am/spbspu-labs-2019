#include "commands.hpp"
#include <iostream>
#include <algorithm>
#include "functions.hpp"
#include "bookmarks-manager.hpp"

void add(BookmarksManager &manager, std::stringstream &stream)
{
  std::string number;
  std::string name;
  stream >> number >> std::ws;
  number = getNumber(number);
  std::getline(stream, name);
  name = getName(name);

  if (name.empty() || number.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  manager.add({number, name});
}

void store(BookmarksManager &manager, std::stringstream &stream)
{
  std::string markName;
  stream >> std::ws >> markName;
  markName = getMarkName(markName);
  std::string newMarkName;
  stream >> std::ws >> newMarkName;
  newMarkName = getMarkName(newMarkName);

  if (markName.empty() || newMarkName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  manager.store(markName, newMarkName);
}

void insert(BookmarksManager &manager, std::stringstream &stream)
{
  struct insertPos_t
  {
    const char *name;
    BookmarksManager::insertPosition position;
  };

  insertPos_t positions[]
  {
    {"before", BookmarksManager::insertPosition::before},
    {"after", BookmarksManager::insertPosition::after}
  };

  std::string insertPos;
  std::string markName;
  std::string number;
  std::string name;
  stream >> std::ws >> insertPos;
  stream >> std::ws >> markName;
  markName = getMarkName(markName);
  stream >> std::ws >> number >> std::ws;
  number = getNumber(number);
  getline(stream, name);
  name = getName(name);

  auto predicate = [&](const insertPos_t &pos) {return pos.name == insertPos;};
  auto position = std::find_if(std::begin(positions), std::end(positions), predicate);

  if (position == std::end(positions))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (markName.empty() || number.empty() || name.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  manager.insert(markName, {number, name}, position->position);
}

void deleteBookmark(BookmarksManager &manager, std::stringstream &stream)
{
  std::string markName;
  stream >> std::ws >> markName;
  markName = getMarkName(markName);
  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  manager.remove(markName);
}

void show(BookmarksManager &manager, std::stringstream &stream)
{
  std::string markName;
  stream >> std::ws >> markName;
  markName = getMarkName(markName);
  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  manager.show(markName);
}

void move(BookmarksManager &manager, std::stringstream &stream)
{
  struct movePosition_t
  {
    const char *name;
    BookmarksManager::movePosition position;
  };

  std::string markName;
  stream >> std::ws >> markName;
  markName = getMarkName(markName);
  std::string steps;
  stream >> std::ws >> steps;

  movePosition_t positions[] =
    {
      { "first", BookmarksManager::movePosition::first },
      { "last", BookmarksManager::movePosition::last }
    };

  auto predicate = [&](const movePosition_t &pos) {return pos.name == steps;};
  auto position = std::find_if(std::begin(positions), std::end(positions), predicate);

  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (position == std::end(positions))
  {
    int sign = 1;
    if (steps.front() == '-')
    {
      sign = -1;
      steps.erase(steps.begin());
    }
    else if (steps.front() == '+')
    {
      steps.erase(steps.begin());
    }

    steps = getNumber(steps);
    if (steps.empty())
    {
      std::cout << "<INVALID STEP>\n";
      return;
    }

    manager.move(markName, std::stoi(steps) * sign);
  }
  else
  {
    manager.move(markName, position->position);
  }
}
