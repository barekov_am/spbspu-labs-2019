#include "bookmarks-manager.hpp"
#include <iostream>
#include <algorithm>
#include <functional>

BookmarksManager::BookmarksManager()
{
  bookmarks_["current"] = phoneBook_.begin();
}

void BookmarksManager::add(const PhoneBook::record_t &newRecord)
{
  phoneBook_.insert(phoneBook_.end(), newRecord);
  if (phoneBook_.begin() == std::prev(phoneBook_.end()))
  {
    bookmarks_["current"] = phoneBook_.begin();
  }
}

void BookmarksManager::store(const std::string &mark, const std::string &newMark)
{
  bookmarkIter iter = getIter(mark);
  if (iter == bookmarks_.end())
  {
    return;
  }

  bookmarks_.emplace(newMark, iter->second);
}

void BookmarksManager::insert(const std::string &mark, const PhoneBook::record_t &newRecord, BookmarksManager::insertPosition direction)
{
  bookmarkIter iter = getIter(mark);
  if (iter == bookmarks_.end())
  {
    return;
  }

  if (iter->second == phoneBook_.end())
  {
    add(newRecord);
    return;
  }

  auto pos = (direction == insertPosition::after) ? std::next(iter->second) : iter->second;
  phoneBook_.insert(pos, newRecord);
}

void BookmarksManager::remove(const std::string &mark)
{
  auto it = getIter(mark);
  if (it == bookmarks_.end())
  {
    return;
  }

  PhoneBook::iterator tmpIter = it->second;

  std::for_each(bookmarks_.begin(), bookmarks_.end(), [&](auto &it)
  {
    if (it.second == tmpIter)
    {
      if (std::next(it.second) == phoneBook_.end())
      {
        it.second = phoneBook_.prev(tmpIter);
      }
      else
      {
        it.second = phoneBook_.next(tmpIter);
      }
    }
  });

  phoneBook_.remove(tmpIter);
}

void BookmarksManager::show(const std::string &mark)
{
  bookmarkIter iter = getIter(mark);
  if (iter == bookmarks_.end())
  {
    return;
  }

  if (phoneBook_.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  phoneBook_.show(iter->second);
}

void BookmarksManager::move(const std::string &mark, int steps)
{
  bookmarkIter iter = getIter(mark);
  if (iter == bookmarks_.end())
  {
    return;
  }

  iter->second = phoneBook_.move(iter->second, steps);
}

void BookmarksManager::move(const std::string &mark, movePosition position)
{
  bookmarkIter iter = getIter(mark);
  if (iter == bookmarks_.end())
  {
    return;
  }

  iter->second = (position == BookmarksManager::movePosition::first) ? phoneBook_.begin() : std::prev(phoneBook_.end());
}

void BookmarksManager::next(const std::string &mark)
{
  bookmarkIter iter = getIter(mark);
  if (iter == bookmarks_.end())
  {
    return;
  }

  iter->second = phoneBook_.next(iter->second);
}

void BookmarksManager::prev(const std::string &mark)
{
  bookmarkIter iter = getIter(mark);
  if (iter == bookmarks_.end())
  {
    return;
  }

  iter->second = phoneBook_.prev(iter->second);
}

BookmarksManager::bookmarkType::iterator BookmarksManager::getIter(const std::string &mark)
{
  bookmarkIter iter = bookmarks_.find(mark);
  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
  }

  return iter;
}
