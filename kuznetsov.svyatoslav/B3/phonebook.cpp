#include "phonebook.hpp"
#include <iostream>

void PhoneBook::show(iterator position) const
{
  std::cout << position->name << " " << position->phone << "\n";
}

PhoneBook::iterator PhoneBook::insert(iterator position, const record_t &newRecord)
{
  return listOfRecords_.insert(position, newRecord);
}

PhoneBook::iterator PhoneBook::remove(iterator position)
{
  return listOfRecords_.erase(position);
}

PhoneBook::iterator PhoneBook::move(iterator position, int steps)
{
  if (steps >= 0)
  {
    while (std::next(position) != listOfRecords_.end() && steps > 0)
    {
      position = std::next(position);
      --steps;
    }
  }
  else
  {
    while (position != listOfRecords_.begin() && steps < 0)
    {
      position = std::prev(position);
      ++steps;
    }
  }

  return position;
}

PhoneBook::iterator PhoneBook::next(iterator position)
{
  return (std::next(position) == listOfRecords_.end() ? position : ++position);
}

PhoneBook::iterator PhoneBook::prev(iterator position)
{
  return (position == listOfRecords_.begin() ? position : --position);
}

PhoneBook::iterator PhoneBook::begin()
{
  return listOfRecords_.begin();
}

PhoneBook::iterator PhoneBook::end()
{
  return listOfRecords_.end();
}

bool PhoneBook::empty() const
{
  return listOfRecords_.empty();
}
