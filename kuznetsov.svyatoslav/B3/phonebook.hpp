#ifndef B3_PHONEBOOK_HPP
#define B3_PHONEBOOK_HPP

#include <list>
#include <string>

class PhoneBook
{
public:
  struct record_t
  {
    std::string name;
    std::string phone;
  };

  using iterator = std::list<record_t>::iterator;

  void show(iterator position) const;
  iterator insert(iterator position, const record_t &newRecord);
  iterator remove(iterator position);
  iterator move(iterator position, int steps);
  iterator next(iterator position);
  iterator prev(iterator position);

  iterator begin();
  iterator end();

  bool empty() const;

private:
  std::list<record_t> listOfRecords_;
};

#endif //B3_PHONEBOOK_HPP
