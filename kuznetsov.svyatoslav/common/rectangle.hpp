#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace kuznetsov
{
  class Rectangle: public Shape
  {
  public:
    Rectangle(const point_t &, double, double);
    Rectangle(const point_t &, double, double, double);

    double getArea() const;
    rectangle_t getFrameRect() const;
    void move(const point_t &position);
    void move(double, double);
    void printInfo() const;
    void scale(double);
    void rotate(double);

  private:
    double width_;
    double height_;
    double angle_;
    point_t center_;
  };
}

#endif //RECTANGLE_HPP
