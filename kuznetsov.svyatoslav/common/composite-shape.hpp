#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace kuznetsov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&) noexcept;
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &);
    CompositeShape &operator =(CompositeShape &&) noexcept;
    shape_ptr operator [](std::size_t) const;

    double getArea() const;
    rectangle_t getFrameRect() const;
    void move(const point_t &);
    void move(double, double);
    void printInfo() const;
    void scale(double);
    std::size_t getCount() const;
    void add(const shape_ptr &);
    void remove(std::size_t);
    void rotate(double);

  private:
    shapes_array shapeList_;
    std::size_t size_;
    std::size_t count_;
  };
}

#endif //COMPOSITESHAPE_HPP
