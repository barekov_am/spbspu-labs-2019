#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double INACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(testingMethods)

BOOST_AUTO_TEST_CASE(compositeShapeTestImmutabilityAfterMoving)
{
  kuznetsov::Rectangle testRect({1.1, 4.2}, 5.0, 3.4);
  kuznetsov::Circle testCircle({4.1, 5.1}, 2.0);
  kuznetsov::CompositeShape compShape;
  compShape.add(std::make_shared<kuznetsov::Rectangle>(testRect));
  compShape.add(std::make_shared<kuznetsov::Circle>(testCircle));

  const double areaBefore = compShape.getArea();
  const kuznetsov::rectangle_t frameRectBefore = compShape.getFrameRect();

  compShape.move(2.0, 8.2);

  kuznetsov::rectangle_t frameRectAfter = compShape.getFrameRect();
  BOOST_CHECK_CLOSE(compShape.getArea(), areaBefore, INACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, INACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestImmutabilityAfterMovingTo)
{
  kuznetsov::Rectangle testRect({1.1, 4.2}, 5.0, 3.4);
  kuznetsov::Circle testCircle({4.1, 5.1}, 2.0);
  kuznetsov::CompositeShape compShape;
  compShape.add(std::make_shared<kuznetsov::Rectangle>(testRect));
  compShape.add(std::make_shared<kuznetsov::Circle>(testCircle));

  const double areaBefore = compShape.getArea();
  const kuznetsov::rectangle_t frameRectBefore = compShape.getFrameRect();

  compShape.move({2.0, 8.2});

  kuznetsov::rectangle_t frameRectAfter = compShape.getFrameRect();
  BOOST_CHECK_CLOSE(compShape.getArea(), areaBefore, INACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, INACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestScale)
{
  kuznetsov::Rectangle testRect({1.1, 4.2}, 5.0, 3.4);
  kuznetsov::Circle testCircle({4.1, 5.1}, 2.0);
  kuznetsov::CompositeShape compShape;
  compShape.add(std::make_shared<kuznetsov::Rectangle>(testRect));
  compShape.add(std::make_shared<kuznetsov::Circle>(testCircle));

  const double areaBefore = compShape.getArea();
  const double coef = 2.3;

  compShape.scale(coef);

  BOOST_CHECK_CLOSE(areaBefore * coef * coef, compShape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestActionsWithEmptyList)
{
  kuznetsov::CompositeShape compShape;

  BOOST_CHECK_THROW(compShape.remove(1), std::out_of_range);
  BOOST_CHECK_THROW(compShape.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(compShape.getArea(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestAreaChangeAfterAdding)
{
  kuznetsov::Rectangle testRect({1.1, 2.2}, 3.3, 4.4);
  kuznetsov::Circle testCircle({4.1, 2.2}, 5.0);
  kuznetsov::CompositeShape compShape;
  compShape.add(std::make_shared<kuznetsov::Rectangle>(testRect));

  const double area = compShape.getArea();
  compShape.add(std::make_shared<kuznetsov::Circle>(testCircle));
  const double newArea = compShape.getArea();

  BOOST_CHECK_CLOSE(newArea, area + testCircle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestAreaChangeAfterDeleting)
{
  kuznetsov::Rectangle testRect({1.1, 2.2}, 3.3, 4.4);
  kuznetsov::Circle testCircle({4.1, 2.2}, 5.0);
  kuznetsov::CompositeShape compShape;
  compShape.add(std::make_shared<kuznetsov::Rectangle>(testRect));
  compShape.add(std::make_shared<kuznetsov::Circle>(testCircle));

  const double area = compShape.getArea();
  compShape.remove(0);
  const double newArea = compShape.getArea();

  BOOST_CHECK_CLOSE(newArea, area - testRect.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestCopyConstructor)
{
  kuznetsov::Rectangle testRect({1.1, 4.2}, 4.0, 5.0);
  kuznetsov::Circle testCircle({5.1, 2.5}, 3.0);
  kuznetsov::CompositeShape compShape;
  compShape.add(std::make_shared<kuznetsov::Circle>(testCircle));
  const kuznetsov::rectangle_t frameRect = compShape.getFrameRect();

  kuznetsov::CompositeShape copyComp(compShape);
  const kuznetsov::rectangle_t copyFrameRect = copyComp.getFrameRect();

  BOOST_CHECK_CLOSE(compShape.getArea(), copyComp.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, INACCURACY);
  BOOST_CHECK_EQUAL(compShape.getCount(), copyComp.getCount());
}

BOOST_AUTO_TEST_CASE(compositeShapeTestMoveConstructor)
{
  kuznetsov::Rectangle testRect({1.1, 4.2}, 4.0, 5.0);
  kuznetsov::Circle testCircle({5.1, 2.5}, 3.0);
  kuznetsov::CompositeShape compShape;
  compShape.add(std::make_shared<kuznetsov::Rectangle>(testRect));
  compShape.add(std::make_shared<kuznetsov::Circle>(testCircle));
  const kuznetsov::rectangle_t frameRect = compShape.getFrameRect();
  const int compCount = compShape.getCount();
  const double compArea = compShape.getArea();
  kuznetsov::CompositeShape moveComp(std::move(compShape));
  const kuznetsov::rectangle_t moveFrameRect = moveComp.getFrameRect();

  BOOST_CHECK_CLOSE(compArea, moveComp.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, INACCURACY);
  BOOST_CHECK_EQUAL(compCount, moveComp.getCount());
}

BOOST_AUTO_TEST_CASE(compositeShapeTestMoveOperator)
{
  kuznetsov::Circle testCircle({5.1, 2.5}, 3.0);
  kuznetsov::Rectangle testRect({1.1, 2.2}, 3.3, 4.4);
  kuznetsov::CompositeShape compShape;
  compShape.add(std::make_shared<kuznetsov::Rectangle>(testRect));
  compShape.add(std::make_shared<kuznetsov::Circle>(testCircle));
  const kuznetsov::rectangle_t frameRect = compShape.getFrameRect();
  const double compArea = compShape.getArea();
  const int compCount = compShape.getCount();

  kuznetsov::Circle testCircle2({2.2, 1.1}, 4.9);
  kuznetsov::CompositeShape moveShape;
  moveShape.add(std::make_shared<kuznetsov::Circle>(testCircle2));
  moveShape = std::move(compShape);
  const kuznetsov::rectangle_t moveFrameRect = moveShape.getFrameRect();

  BOOST_CHECK_CLOSE(compArea, moveShape.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, INACCURACY);
  BOOST_CHECK_EQUAL(compCount, moveShape.getCount());
  BOOST_CHECK_EQUAL(compShape.getCount(), 0);
  BOOST_CHECK_THROW(compShape.getArea(), std::logic_error);

  moveShape = std::move(moveShape);
  const kuznetsov::rectangle_t moveFrameRect1 = moveShape.getFrameRect();

  BOOST_CHECK_CLOSE(compArea, moveShape.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect1.height, INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect1.width, INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect1.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect1.pos.y, INACCURACY);
  BOOST_CHECK_EQUAL(compCount, moveShape.getCount());
}

BOOST_AUTO_TEST_CASE(compositeShapeTestCopyOperator)
{
  kuznetsov::Rectangle testRect({4.4, 3.3}, 3.3, 4.4);
  kuznetsov::Circle testCircle({1.5, 5.2}, 3.0);
  kuznetsov::CompositeShape compShape;
  compShape.add(std::make_shared<kuznetsov::Rectangle>(testRect));
  compShape.add(std::make_shared<kuznetsov::Circle>(testCircle));
  const kuznetsov::rectangle_t frameRect = compShape.getFrameRect();
  kuznetsov::CompositeShape copyComp = compShape;
  const kuznetsov::rectangle_t copyFrameRect = copyComp.getFrameRect();

  BOOST_CHECK_CLOSE(compShape.getArea(), copyComp.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, INACCURACY);
  BOOST_CHECK_EQUAL(compShape.getCount(), copyComp.getCount());
}

BOOST_AUTO_TEST_CASE(testThrowExceptionAfterScale)
{
  kuznetsov::Circle testCircle({2.1, 2.0}, 4.0);
  kuznetsov::CompositeShape compShape;
  compShape.add(std::make_shared<kuznetsov::Circle>(testCircle));

  BOOST_CHECK_THROW(compShape.scale(-2.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testThrowExceptionOutOfRangeHandling)
{
  kuznetsov::Rectangle testRect({4.1, 2.0}, 5.0, 2.5);
  kuznetsov::Circle testCircle({2.2, 3.3}, 4.0);
  kuznetsov::CompositeShape compShape;
  compShape.add(std::make_shared<kuznetsov::Rectangle>(testRect));
  compShape.add(std::make_shared<kuznetsov::Circle>(testCircle));

  BOOST_CHECK_THROW(compShape.remove(3), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(testThrowExceptionAfterUsingOperator)
{
  kuznetsov::Circle testCircle({1.1, 2.2}, 3.3);
  kuznetsov::CompositeShape compShape;
  compShape.add(std::make_shared<kuznetsov::Circle>(testCircle));

  BOOST_CHECK_THROW(compShape[4], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeTestMethodRotate)
{
  kuznetsov::Rectangle testRect({0.2, 8.1}, 2.3, 2.8);

  const kuznetsov::rectangle_t rectFrameBefore = testRect.getFrameRect();
  const double areaBeforeRotate = testRect.getArea();

  testRect.rotate(90.0);
  kuznetsov::rectangle_t rectFrameAfter = testRect.getFrameRect();

  BOOST_CHECK_CLOSE(areaBeforeRotate, testRect.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE(rectFrameBefore.width, rectFrameAfter.height, INACCURACY);
  BOOST_CHECK_CLOSE(rectFrameBefore.height, rectFrameAfter.width, INACCURACY);

  /*compShape.rotate(-180.0);
  rectFrameAfter = compShape.getFrameRect();

  BOOST_CHECK_CLOSE(areaBeforeRotate, compShape.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE(rectFrameBefore.pos.x, rectFrameAfter.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(rectFrameBefore.pos.y, rectFrameAfter.pos.y, INACCURACY);*/
}

BOOST_AUTO_TEST_SUITE_END()
