#include <memory>
#include <boost/test/auto_unit_test.hpp>

#include "matrix.hpp"
#include "splitting.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testSplittingMethods)

BOOST_AUTO_TEST_CASE(testSplitting)
{
  const kuznetsov::Circle circle1({0.0, 0.0}, 3.0);
  const kuznetsov::Circle circle2({5.0, 5.0}, 1.0);
  const kuznetsov::Rectangle rect1({-2.0, 1.0}, 4.0, 4.0);
  const kuznetsov::Rectangle rect2({-5.0, 4.0}, 3.0, 6.0);

  std::shared_ptr<kuznetsov::Shape> circlePtr1 = std::make_shared<kuznetsov::Circle>(circle1);
  std::shared_ptr<kuznetsov::Shape> circlePtr2 = std::make_shared<kuznetsov::Circle>(circle2);
  std::shared_ptr<kuznetsov::Shape> rectPtr1 = std::make_shared<kuznetsov::Rectangle>(rect1);
  std::shared_ptr<kuznetsov::Shape> rectPtr2 = std::make_shared<kuznetsov::Rectangle>(rect2);

  kuznetsov::CompositeShape compositeShape;
  compositeShape.add(circlePtr1);
  compositeShape.add(rectPtr1);
  compositeShape.add(circlePtr2);
  compositeShape.add(rectPtr2);

  kuznetsov::Matrix<kuznetsov::Shape> matrix = kuznetsov::split(compositeShape);

  BOOST_CHECK(matrix[0][0] == circlePtr1);
  BOOST_CHECK(matrix[0][1] == circlePtr2);
  BOOST_CHECK(matrix[1][0] == rectPtr1);
  BOOST_CHECK(matrix[2][0] == rectPtr2);

  BOOST_CHECK_EQUAL(matrix.getRows(), 3);
  BOOST_CHECK_EQUAL(matrix[0].getSize(), 2);
  BOOST_CHECK_EQUAL(matrix[1].getSize(), 1);
  BOOST_CHECK_EQUAL(matrix[2].getSize(), 1);

  const kuznetsov::Rectangle rect3({6.0, 6.0}, 2.0, 2.0);
  std::shared_ptr<kuznetsov::Shape> rectPointer3 = std::make_shared<kuznetsov::Rectangle>(rect3);

  compositeShape.add(rectPointer3);
  matrix = kuznetsov::split(compositeShape);

  BOOST_CHECK(matrix[1][1] == rectPointer3);
  BOOST_CHECK_EQUAL(matrix.getRows(), 3);
  BOOST_CHECK_EQUAL(matrix[0].getSize(), 2);
  BOOST_CHECK_EQUAL(matrix[1].getSize(), 2);
  BOOST_CHECK_EQUAL(matrix[2].getSize(), 1);
}

BOOST_AUTO_TEST_CASE(testIntersect)
{
  const kuznetsov::Rectangle rect({0.0, 0.0}, 6.0, 5.0);
  const kuznetsov::Rectangle rect1({2.0, 2.0}, 4.0, 4.0);
  const kuznetsov::Rectangle rect2({-2.0, 3.0}, 4.0, 2.0);
  const kuznetsov::Rectangle rect3({-2.0, -3.0}, 2.0, 2.0);
  const kuznetsov::Rectangle rect4({2.0, -3.0}, 2.0, 2.0);
  const kuznetsov::Circle circle({10.0, -20.0}, 4.0);

  BOOST_CHECK(kuznetsov::intersect(rect.getFrameRect(), rect1.getFrameRect()));
  BOOST_CHECK(kuznetsov::intersect(rect.getFrameRect(), rect2.getFrameRect()));
  BOOST_CHECK(kuznetsov::intersect(rect.getFrameRect(), rect3.getFrameRect()));
  BOOST_CHECK(kuznetsov::intersect(rect.getFrameRect(), rect4.getFrameRect()));
  BOOST_CHECK(!kuznetsov::intersect(rect.getFrameRect(), circle.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
