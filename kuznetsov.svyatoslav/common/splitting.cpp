#include "splitting.hpp"

kuznetsov::Matrix<kuznetsov::Shape> kuznetsov::split(const CompositeShape &compShape)
{
  kuznetsov::Matrix<kuznetsov::Shape> matrix;
  std::size_t count = compShape.getCount();

  for (std::size_t i = 0; i < count; ++ i)
  {
    std::size_t row = 0;
    std::size_t matrixRows = matrix.getRows();

    for (std::size_t j = matrixRows; j-- > 0;)
    {
      std::size_t matrixColumns = matrix[j].getSize();
      for (std::size_t k = 0; k < matrixColumns; ++k)
      {
        if (intersect(matrix[j][k]->getFrameRect(), compShape[i]->getFrameRect()))
        {
          row = j + 1;
          break;
        }
      }
      if (row > j)
      {
        break;
      }
    }
    matrix.add(compShape[i], row);
  }

  return matrix;
}

bool kuznetsov::intersect(const rectangle_t & lftRect, const rectangle_t & rhtRect)
{
  const kuznetsov::point_t lftRectLftBottom = {lftRect.pos.x - lftRect.width / 2, lftRect.pos.y - lftRect.height / 2};
  const kuznetsov::point_t lftRectRhtTop = {lftRect.pos.x + lftRect.width / 2, lftRect.pos.y + lftRect.height / 2};

  const kuznetsov::point_t rhtRectLftBottom = {rhtRect.pos.x - rhtRect.width / 2, rhtRect.pos.y - rhtRect.height / 2};
  const kuznetsov::point_t rhtRectRhtTop = {rhtRect.pos.x + rhtRect.width / 2, rhtRect.pos.y + rhtRect.height / 2};

  const bool first = (rhtRectLftBottom.y < lftRectRhtTop.y) && (rhtRectLftBottom.x < lftRectRhtTop.x);
  const bool second = (rhtRectRhtTop.y > lftRectLftBottom.y) && (rhtRectRhtTop.x > lftRectLftBottom.x);

  return first && second;
}

