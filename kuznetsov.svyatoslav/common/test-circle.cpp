#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "circle.hpp"

const double INACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(testingMethods)

BOOST_AUTO_TEST_CASE(circleTestImmutabilityAfterMoving)
{
  kuznetsov::Circle testCircle({4.9, 6.3}, 2.2);
  const double circleAreaBeforeMoving = testCircle.getArea();
  const kuznetsov::rectangle_t circleFrameBeforeMoving = testCircle.getFrameRect();

  testCircle.move(1.8, -4.7);

  BOOST_CHECK_CLOSE(testCircle.getArea(), circleAreaBeforeMoving, INACCURACY);

  kuznetsov::rectangle_t rectFrameAfterMoving = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(circleFrameBeforeMoving.height, rectFrameAfterMoving.height, INACCURACY);
  BOOST_CHECK_CLOSE(circleFrameBeforeMoving.width, rectFrameAfterMoving.width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(circleTestImmutabilityAfterMovingTo)
{
  kuznetsov::Circle testCircle({4.9, 6.3}, 2.2);
  const double circleAreaBeforeMovingTo = testCircle.getArea();
  const kuznetsov::rectangle_t rectFrameBeforeMovingTo = testCircle.getFrameRect();

  testCircle.move({2.7, -1.4});

  BOOST_CHECK_CLOSE(testCircle.getArea(), circleAreaBeforeMovingTo, INACCURACY);

  kuznetsov::rectangle_t rectFrameAfterMovingTo = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(rectFrameBeforeMovingTo.height, rectFrameAfterMovingTo.height, INACCURACY);
  BOOST_CHECK_CLOSE(rectFrameBeforeMovingTo.width, rectFrameAfterMovingTo.width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(circleTestCale)
{
  kuznetsov::Circle testCircle1({4.0, 6.0}, 2.0);
  kuznetsov::Circle testCircle2({2.1, 4.2}, 3.0);
  const double rectAreaBeforeScale1 = testCircle1.getArea();
  const double rectAreaBeforeScale2 = testCircle2.getArea();
  const double coef1 = 3.2;
  const double coef2 = 0.5;

  testCircle1.scale(coef1);
  testCircle2.scale(coef2);

  BOOST_CHECK_CLOSE(testCircle1.getArea(), rectAreaBeforeScale1 * coef1 * coef1, INACCURACY);
  BOOST_CHECK_CLOSE(testCircle2.getArea(), rectAreaBeforeScale2 * coef2 * coef2, INACCURACY);
}

BOOST_AUTO_TEST_CASE(circleTestRotate)
{
  kuznetsov::Circle testCircle({1.1, 2.2}, 3.3);
  const double circleAreaBeforeRotate = testCircle.getArea();
  const kuznetsov::rectangle_t frameRectBeforeRotate = testCircle.getFrameRect();
  double angle = 90.0;

  testCircle.rotate(angle);

  double circleAreaAfterRotate = testCircle.getArea();
  kuznetsov::rectangle_t frameRectAfterRotate = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.width, INACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.height, INACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, INACCURACY);
  BOOST_CHECK_CLOSE(circleAreaBeforeRotate, circleAreaAfterRotate, INACCURACY);

  angle = -90.0;

  testCircle.rotate(angle);
  circleAreaAfterRotate = testCircle.getArea();
  frameRectAfterRotate = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.height, INACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.width, INACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, INACCURACY);
  BOOST_CHECK_CLOSE(circleAreaBeforeRotate, circleAreaAfterRotate, INACCURACY);

  angle = 180.0;
  testCircle.rotate(angle);
  circleAreaAfterRotate = testCircle.getArea();
  frameRectAfterRotate = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.height, INACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.width, INACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, INACCURACY);
  BOOST_CHECK_CLOSE(circleAreaBeforeRotate, circleAreaAfterRotate, INACCURACY);
}


BOOST_AUTO_TEST_CASE(invalidCircleValues)
{
  kuznetsov::Circle testCircle({2.8, 2.9}, 4.5);
  BOOST_CHECK_THROW(kuznetsov::Circle({3.2, 4.1}, -2.0), std::invalid_argument);

  BOOST_CHECK_THROW(testCircle.scale(-2.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
