#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace kuznetsov
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t &, double);

    double getArea() const;
    rectangle_t getFrameRect() const;
    void move(const point_t &position);
    void move(double, double);
    void printInfo() const;
    void scale(double);
    void rotate(double);

  private:
    double radius_;
    point_t center_;
  };
}

#endif //CIRCLE_HPP
