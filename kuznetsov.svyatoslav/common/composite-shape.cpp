#include "composite-shape.hpp"
#include <iostream>
#include <algorithm>
#include <stdexcept>
#include <cmath>
#include <memory>

kuznetsov::CompositeShape::CompositeShape() :
  size_(0),
  count_(0)
{
}

kuznetsov::CompositeShape::CompositeShape(const kuznetsov::CompositeShape &rhs) :
  shapeList_(std::make_unique<shape_ptr []>(rhs.count_)),
  size_(rhs.size_),
  count_(rhs.count_)
{
  for (std::size_t i = 0; i < size_; ++i)
  {
    shapeList_[i] = rhs.shapeList_[i];
  }
}

kuznetsov::CompositeShape::CompositeShape(kuznetsov::CompositeShape &&rhs) noexcept :
  shapeList_(std::move(rhs.shapeList_)),
  size_(rhs.size_),
  count_(rhs.count_)
{
  rhs.count_ = 0;
  rhs.size_ = 0;
}

kuznetsov::CompositeShape &kuznetsov::CompositeShape::operator =(const kuznetsov::CompositeShape &rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    shapeList_ = std::make_unique<shape_ptr []>(rhs.count_);
    for (std::size_t i = 0; i < size_; ++i)
    {
      shapeList_[i] = rhs.shapeList_[i];
    }
  }

  return *this;
}

kuznetsov::CompositeShape &kuznetsov::CompositeShape::operator =(kuznetsov::CompositeShape &&rhs) noexcept
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    size_ = rhs.size_;
    rhs.count_ = 0;
    rhs.size_ = 0;
    shapeList_ = std::move(rhs.shapeList_);
  }

  return *this;
}

kuznetsov::shape_ptr kuznetsov::CompositeShape::operator [](std::size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }

  return shapeList_[index];
}

double kuznetsov::CompositeShape::getArea() const
{
  if (count_ == 0)
  {
    throw std::logic_error("You try to get frame rect of empty composite shape");
  }

  double area = 0.0;
  for (std::size_t i = 0; i < count_; ++i)
  {
    area += shapeList_[i]->getArea();
  }

  return area;
}

kuznetsov::rectangle_t kuznetsov::CompositeShape::getFrameRect() const
{
  if (shapeList_ == nullptr)
  {
    throw std::logic_error("You try to get frame rect of empty composite shape");
  }

  kuznetsov::rectangle_t currRect = shapeList_[0]->getFrameRect();

  double min_X = currRect.pos.x - currRect.width / 2.0;
  double max_X = currRect.pos.x + currRect.width / 2.0;
  double min_Y = currRect.pos.y - currRect.height / 2.0;
  double max_Y = currRect.pos.y + currRect.height / 2.0;

  for (std::size_t i = 1; i < count_; ++i)
  {
    currRect = shapeList_[i]->getFrameRect();
    min_X = std::min(currRect.pos.x - currRect.width / 2.0, min_X);
    max_X = std::max(currRect.pos.x + currRect.width / 2.0, max_X);
    min_Y = std::min(currRect.pos.y - currRect.height / 2.0, min_Y);
    max_Y = std::max(currRect.pos.y + currRect.height / 2.0, max_Y);
  }

  return {(max_X - min_X), (max_Y - min_Y), {(max_X + min_X) / 2.0, (max_Y + max_Y) / 2.0}};
}

void kuznetsov::CompositeShape::move(const kuznetsov::point_t &position)
{
  const kuznetsov::rectangle_t rect = getFrameRect();

  double dx = position.x - rect.pos.x;
  double dy = position.y - rect.pos.y;

  move(dx, dy);
}

void kuznetsov::CompositeShape::move(double dx, double dy)
{
  for (std::size_t i = 0; i < count_; ++i)
  {
    shapeList_[i]->move(dx, dy);
  }
}

void kuznetsov::CompositeShape::printInfo() const
{
  const kuznetsov::rectangle_t rect = getFrameRect();

  std::cout << "Center of composite shape: " << rect.pos.x << ", " << rect.pos.y << "\n";
  std::cout << "Count of shapes: " << count_ << "\n";
  std::cout << "Frame rect: \n";
  std::cout << "Width: " << rect.width << "\n";
  std::cout << "Height: " << rect.height << "\n";
  std::cout << "Area of composite shape: " << getArea() << "\n\n";
}

void kuznetsov::CompositeShape::scale(double coef)
{
  if (coef <= 0)
  {
    throw std::invalid_argument("Coefficient must be positive");
  }
  const kuznetsov::point_t posFrameRect = getFrameRect().pos;
  for (std::size_t i = 0; i < count_; ++i)
  {
    kuznetsov::point_t shapeCenter = shapeList_[i]->getFrameRect().pos;

    shapeList_[i]->scale(coef);
    double dx = (shapeCenter.x - posFrameRect.x) * (coef - 1);
    double dy = (shapeCenter.y - posFrameRect.y) * (coef - 1);
    shapeList_[i]->move(dx, dy);
  }
}

std::size_t kuznetsov::CompositeShape::getCount() const
{
  return count_;
}

void kuznetsov::CompositeShape::add(const std::shared_ptr<kuznetsov::Shape> &newShape)
{
  for (std::size_t i = 0; i < count_; ++i)
  {
    if (shapeList_[i] == newShape)
    {
      return;
    }
  }
  ++count_;
  if (count_ > size_)
  {
    shapes_array tmpArr(std::make_unique<std::shared_ptr<kuznetsov::Shape> []> (count_));

    for (std::size_t i = 0; i < size_; ++i)
    {
      tmpArr[i] = shapeList_[i];
    }
    shapeList_ = std::move(tmpArr);
    ++size_;
  }
  shapeList_[count_ - 1] = newShape;
}

void kuznetsov::CompositeShape::remove(std::size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }
  --count_;
  for (std::size_t i = index; i < count_; ++i)
  {
    shapeList_[i] = shapeList_[i + 1];
  }
  shapeList_[count_] = nullptr;
}

void kuznetsov::CompositeShape::rotate(double angle)
{
  const kuznetsov::point_t center = getFrameRect().pos;
  const double cos = std::abs(std::cos(angle * M_PI / 180));
  const double sin = std::abs(std::sin(angle * M_PI / 180));

  for (std::size_t i = 0; i < count_; ++i)
  {
    const kuznetsov::point_t shapeCenter = shapeList_[i]->getFrameRect().pos;
    const double projection_x = shapeCenter.x - center.x;
    const double projection_y = shapeCenter.y - center.y;
    const double dx = projection_x * (cos - 1) - projection_y * sin;
    const double dy = projection_x * sin + projection_y * (cos - 1);
    shapeList_[i]->move(dx, dy);
    shapeList_[i]->rotate(angle);
  }
}

