#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "rectangle.hpp"

const double INACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(testingMethods)

BOOST_AUTO_TEST_CASE(rectangleTestImmutabilityAfterMoving)
{
  kuznetsov::Rectangle testRect({4.1, 6.4}, 7.0, 2.1);
  const double rectAreaBeforeMoving = testRect.getArea();
  const kuznetsov::rectangle_t rectFrameBeforeMoving = testRect.getFrameRect();

  testRect.move(7.0, 2.1);

  BOOST_CHECK_CLOSE(testRect.getArea(), rectAreaBeforeMoving, INACCURACY);

  kuznetsov::rectangle_t rectFrameAfterMoving = testRect.getFrameRect();

  BOOST_CHECK_CLOSE(rectFrameAfterMoving.height, rectFrameBeforeMoving.height, INACCURACY);
  BOOST_CHECK_CLOSE(rectFrameAfterMoving.width, rectFrameBeforeMoving.width, INACCURACY);

}

BOOST_AUTO_TEST_CASE(rectangleTestImmutabilityAfterMovingTo)
{
  kuznetsov::Rectangle testRect({4.1, 6.4}, 7.0, 2.1);
  const double rectAreaBeforeMovingTo = testRect.getArea();
  const kuznetsov::rectangle_t rectFrameBeforeMovingTo = testRect.getFrameRect();

  testRect.move({2.7, -1.4});

  BOOST_CHECK_CLOSE(testRect.getArea(), rectAreaBeforeMovingTo, INACCURACY);

  kuznetsov::rectangle_t rectFrameAfterMovingTo = testRect.getFrameRect();

  BOOST_CHECK_CLOSE(rectFrameAfterMovingTo.height, rectFrameBeforeMovingTo.height, INACCURACY);
  BOOST_CHECK_CLOSE(rectFrameAfterMovingTo.width, rectFrameBeforeMovingTo.width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(rectangleTestScale)
{
  kuznetsov::Rectangle testRect({3.6, 3.5}, 3.0, 3.1);
  const double rectAreaBeforeScale = testRect.getArea();
  const double coef = 2.5;

  testRect.scale(coef);

  BOOST_CHECK_CLOSE(testRect.getArea(), rectAreaBeforeScale * coef * coef, INACCURACY);
}

  BOOST_AUTO_TEST_CASE(rectangleTestRotate)
  {
    kuznetsov::Rectangle testRect({3.3, 2.2}, 1.1, 8.0);
    const double circleAreaBeforeRotate = testRect.getArea();
    const kuznetsov::rectangle_t frameRectBeforeRotate = testRect.getFrameRect();
    double angle = 90.0;

    testRect.rotate(angle);

    double circleAreaAfterRotate = testRect.getArea();
    kuznetsov::rectangle_t frameRectAfterRotate = testRect.getFrameRect();

    BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.width, INACCURACY);
    BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.height, INACCURACY);
    BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, INACCURACY);
    BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, INACCURACY);
    BOOST_CHECK_CLOSE(circleAreaBeforeRotate, circleAreaAfterRotate, INACCURACY);

    angle = -90.0;

    testRect.rotate(angle);
    circleAreaAfterRotate = testRect.getArea();
    frameRectAfterRotate = testRect.getFrameRect();

    BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.height, INACCURACY);
    BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.width, INACCURACY);
    BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, INACCURACY);
    BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, INACCURACY);
    BOOST_CHECK_CLOSE(circleAreaBeforeRotate, circleAreaAfterRotate, INACCURACY);

    angle = 180.0;
    testRect.rotate(angle);
    circleAreaAfterRotate = testRect.getArea();
    frameRectAfterRotate = testRect.getFrameRect();

    BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.height, INACCURACY);
    BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.width, INACCURACY);
    BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, INACCURACY);
    BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, INACCURACY);
    BOOST_CHECK_CLOSE(circleAreaBeforeRotate, circleAreaAfterRotate, INACCURACY);
  }

BOOST_AUTO_TEST_CASE(invalidRectangleValues)
{
  BOOST_CHECK_THROW(kuznetsov::Rectangle({4.1, 2.5}, -2.0, 1.5), std::invalid_argument);
  BOOST_CHECK_THROW(kuznetsov::Rectangle({4.1, 2.9}, 3.4, -2.0), std::invalid_argument);

  kuznetsov::Rectangle testRect({3.0, 3.0}, 1.0, 1.0);

  BOOST_CHECK_THROW(testRect.scale(-2.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
