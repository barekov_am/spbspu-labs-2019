#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>

namespace kuznetsov {
  template<typename T>
  class Matrix {
  public:
    using ptr = std::shared_ptr<T>;
    using arr = std::unique_ptr<ptr[]>;

    class Layer {
    public:
      Layer(const Layer &);
      Layer(Layer &&) noexcept;
      Layer(ptr *, std::size_t);
      ~Layer() = default;

      Layer &operator =(const Layer &);
      Layer &operator =(Layer &&) noexcept;
      ptr operator [](std::size_t) const;
      std::size_t getSize() const;

    private:
      std::size_t size_;
      std::unique_ptr<ptr[]> array_;
    };

    Matrix();
    Matrix(const Matrix<T> &);
    Matrix(Matrix<T> &&) noexcept;
    ~Matrix() = default;

    Matrix<T> &operator =(const Matrix<T> &);
    Matrix<T> &operator =(Matrix<T> &&) noexcept;
    Layer operator [](std::size_t) const;
    bool operator ==(const Matrix<T> &) const;
    bool operator !=(const Matrix<T> &) const;

    std::size_t getRows() const;
    void add(ptr, std::size_t);
    void swap(Matrix<T> &) noexcept;

  private:
    std::size_t rows_;
    std::size_t count_;
    std::unique_ptr<std::size_t[]> columns_;
    arr data_;
  };

  template <typename T>
  Matrix<T>::Matrix() :
    rows_(0),
    count_(0)
  {
  }

  template <typename T>
  Matrix<T>::Matrix(const Matrix<T> &rhs) :
    rows_(rhs.rows_),
    count_(rhs.count_),
    columns_(std::make_unique<std::size_t []>(rhs.rows_)),
    data_(std::make_unique<ptr []>(rhs.count_))
  {
    for (std::size_t i = 0; i < rows_; ++i)
    {
      columns_[i] = rhs.columns_[i];
    }

    for (std::size_t i = 0; i < count_; ++i)
    {
      data_[i] = rhs.data_[i];
    }
  }

  template <typename T>
  Matrix<T>::Matrix(Matrix<T> &&rhs) noexcept :
    rows_(rhs.rows_),
    count_(rhs.count_),
    columns_(std::move(rhs.columns_)),
    data_(std::move(rhs.data_))
  {
    rhs.rows_ = 0;
    rhs.count_ = 0;
  }

  template <typename T>
  Matrix<T> &Matrix<T>::operator =(const Matrix<T> &rhs)
  {
    if (this != &rhs)
    {
      Matrix<T>(rhs).swap(*this);
    }

    return *this;
  }

  template <typename T>
  Matrix<T> &Matrix<T>::operator =(Matrix<T> &&rhs) noexcept
  {
    if (this != &rhs)
    {
      rows_ = rhs.rows_;
      rhs.rows_ = 0;
      count_ = rhs.count_;
      rhs.count_ = 0;
      columns_ = std::move(rhs.columns_);
      data_ = std::move(rhs.data_);
    }

    return *this;
  }

  template <typename T>
  typename Matrix<T>::Layer Matrix<T>::operator [](std::size_t row) const
  {
    if (row >= rows_)
    {
      throw std::out_of_range("Index out of range!");
    }

    std::size_t startRow = 0;

    for (std::size_t i = 0; i < row; ++i)
    {
      startRow += columns_[i];
    }

    return Matrix<T>::Layer(&data_[startRow], columns_[row]);
  }

  template <typename T>
  bool Matrix<T>::operator ==(const Matrix<T> &matrix) const
  {
    if ((rows_ != matrix.rows_) || (count_ != matrix.count_))
    {
      return false;
    }

    for (std::size_t i = 0; i < rows_; ++i)
    {
      if (columns_[i] != matrix.columns_[i])
      {
        return false;
      }
    }

    for (std::size_t i = 0; i < count_; ++i)
    {
      if (data_[i] != matrix.data_[i])
      {
        return false;
      }
    }

    return true;
  }

  template <typename T>
  bool Matrix<T>::operator !=(const Matrix<T> &matrix) const
  {
    return !(*this == matrix);
  }

  template <typename T>
  std::size_t Matrix<T>::getRows() const
  {
    return rows_;
  }

  template <typename T>
  void Matrix<T>::add(ptr obj, std::size_t row)
  {
    if (row > rows_)
    {
      throw std::out_of_range("Index out of range");
    }

    if (!obj)
    {
      throw std::invalid_argument("Pointer is nullptr");
    }

    std::unique_ptr<ptr []> newData = std::make_unique<ptr []>(count_ + 1);
    std::size_t newIndex = 0;
    std::size_t oldIndex = 0;

    for (std::size_t i = 0; i < rows_; ++i)
    {
      for (std::size_t j = 0; j < columns_[i]; ++j)
      {
        newData[newIndex++] = data_[oldIndex++];
      }
      if (row == i)
      {
        newData[newIndex++] = obj;
        ++columns_[row];
      }
    }

    if (row == rows_)
    {
      std::unique_ptr<std::size_t []> newColumns = std::make_unique<std::size_t []>(rows_ + 1);

      for (std::size_t i = 0; i < rows_; ++i)
      {
        newColumns[i] = columns_[i];
      }
      newColumns[row] = 1;
      newData[count_] = obj;
      ++rows_;
      columns_.swap(newColumns);
    }

    data_.swap(newData);
    ++count_;
  }

  template <typename T>
  void Matrix<T>::swap(Matrix<T> &rhs) noexcept
  {
    std::swap(rows_, rhs.rows_);
    std::swap(count_, rhs.count_);
    std::swap(columns_, rhs.columns_);
    std::swap(data_, rhs.data_);
  }

  template <typename T>
  Matrix<T>::Layer::Layer(const Matrix<T>::Layer &rhs) :
    size_(rhs.size_),
    array_(std::make_unique<ptr []>(rhs.size_))
  {
    for (std::size_t i = 0; i < size_; ++i)
    {
      array_[i] = rhs.array_[i];
    }
  }

  template <typename T>
  Matrix<T>::Layer::Layer(Matrix<T>::Layer &&rhs) noexcept :
    size_(rhs.size_),
    array_(std::move(rhs.array_))
  {
    rhs.size_ = 0;
  }

  template <typename T>
  Matrix<T>::Layer::Layer(ptr *array, std::size_t size):
    size_(size),
    array_(std::make_unique<ptr []>(size))
  {
    for (std::size_t i = 0; i < size_; ++i)
    {
      array_[i] = array[i];
    }
  }

  template <typename T>
  typename Matrix<T>::Layer &Matrix<T>::Layer::operator =(const Matrix<T>::Layer &rhs)
  {
    if (this != &rhs)
    {
      array_ = std::make_unique<ptr[]>(rhs.size_);
      for (size_t i = 0; i < rhs.size_; ++i)
      {
        array_[i] = rhs.data_[i];
      }
      size_ = rhs.size_;
    }

    return *this;
  }

  template <typename T>
  typename Matrix<T>::Layer &Matrix<T>::Layer::operator =(Matrix<T>::Layer &&rhs) noexcept
  {
    if (this != &rhs)
    {
      size_ = rhs.size_;
      rhs.size_ = 0;
      array_ = std::move(rhs.array_);
    }
  }

  template <typename T>
  typename Matrix<T>::ptr Matrix<T>::Layer::operator [](std::size_t index) const
  {
    if (index >= size_)
    {
      throw std::out_of_range("Index out of range");
    }

    return array_[index];
  }

  template<typename T>
  size_t Matrix<T>::Layer::getSize() const
  {
    return size_;
  }


}

#endif //MATRIX_HPP
