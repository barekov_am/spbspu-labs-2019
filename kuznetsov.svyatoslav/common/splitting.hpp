#ifndef A3_SPLITTING_HPP
#define A3_SPLITTING_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace kuznetsov
{
  Matrix<Shape> split(const CompositeShape &);
  bool intersect(const rectangle_t &, const rectangle_t &);
}

#endif //A3_SPLITTING_HPP
