#include <memory>
#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "matrix.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

const double INACCURACY = 0.0001;

BOOST_AUTO_TEST_SUITE(matrixMethodsTesting)

BOOST_AUTO_TEST_CASE(testMatrixCopyConstructor)
{
  kuznetsov::Matrix<int> testMatrix;
  for (int i = 0; i < 10; ++i)
  {
    testMatrix.add(std::make_shared<int>(i), i);
  }

  kuznetsov::Matrix<int> copyMatrix(testMatrix);

  BOOST_CHECK(testMatrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(testMatrixCopyOperator)
{
  kuznetsov::Matrix<int> testMatrix;
  for (int i = 0; i < 10; ++i)
  {
    testMatrix.add(std::make_shared<int>(i), i);
  }

  kuznetsov::Matrix<int> copyMatrix = testMatrix;

  BOOST_CHECK(testMatrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(testMatrixMoveConstructor)
{
  kuznetsov::Matrix<int> testMatrix;
  for (int i = 0; i < 10; ++i)
  {
    testMatrix.add(std::make_shared<int>(i), i);
  }
  kuznetsov::Matrix<int> copyMatrix(testMatrix);

  kuznetsov::Matrix<int> newMatrix(std::move(testMatrix));

  BOOST_CHECK(copyMatrix == newMatrix);
  BOOST_CHECK(kuznetsov::Matrix<int>() == testMatrix);

}

BOOST_AUTO_TEST_CASE(testMatrixMoveOperator)
{
  kuznetsov::Matrix<int> testMatrix;
  for (int i = 0; i < 10; ++i)
  {
    testMatrix.add(std::make_shared<int>(i), i);
  }

  kuznetsov::Matrix<int> copyMatrix = testMatrix;

  BOOST_CHECK(testMatrix == copyMatrix);

}

BOOST_AUTO_TEST_CASE(testMatrixEqualOperator)
{
  kuznetsov::Matrix<int> testMatrix;
  for (int i = 0; i < 10; ++i)
  {
    testMatrix.add(std::make_shared<int>(i), i);
  }

  kuznetsov::Matrix<int> testEqual(testMatrix);
  kuznetsov::Matrix<int> testUnequal;
  testUnequal.add(std::make_shared<int>(2), 0);

  BOOST_CHECK_EQUAL(testEqual == testMatrix, true);
  BOOST_CHECK_EQUAL(testUnequal == testMatrix, false);
}

BOOST_AUTO_TEST_CASE(testMatrixUnequalOperator)
{
  kuznetsov::Matrix<int> testMatrix;
  for (int i = 0; i < 10; ++i)
  {
    testMatrix.add(std::make_shared<int>(i), i);
  }

  kuznetsov::Matrix<int> testEqual(testMatrix);
  kuznetsov::Matrix<int> testUnequal;
  testUnequal.add(std::make_shared<int>(2), 0);

  BOOST_CHECK_EQUAL(testEqual != testMatrix, false);
  BOOST_CHECK_EQUAL(testUnequal != testMatrix, true);
}

BOOST_AUTO_TEST_CASE(testMatrixThrowGetMethod)
{
  kuznetsov::Matrix<int> testMatrix;
  for (int i = 0; i < 10; ++i)
  {
    testMatrix.add(std::make_shared<int>(i), i);
  }

  BOOST_CHECK_THROW(testMatrix[7][3], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[15][0], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
