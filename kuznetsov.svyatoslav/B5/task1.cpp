#include <iostream>
#include <set>
#include <sstream>

void task1()
{
  std::string line;
  std::set<std::string> setOfWords;
  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error while reading data!");
    }

    std::stringstream stream(line);
    std::string word;
    while (stream >> word)
    {
      if (setOfWords.find(word) == setOfWords.end())
      {
        setOfWords.insert(word);
      }
    }
  }

  if (!setOfWords.empty())
  {
    for (auto i = setOfWords.begin(); i != setOfWords.end(); ++i)
    {
      std::cout << *i << '\n';
    }
  }
}
