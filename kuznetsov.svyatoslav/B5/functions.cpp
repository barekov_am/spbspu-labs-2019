#include "functions.hpp"

int getLength(const Point_t &lhs, const Point_t &rhs)
{
  return (lhs.x - rhs.x) * (lhs.x - rhs.x) + (lhs.y - rhs.y) * (lhs.y - rhs.y);
}

bool isRectangle(const Shape &shape)
{
  int diag1 = getLength(shape[0], shape[2]);
  int diag2 = getLength(shape[1], shape[3]);
  if (diag1 == diag2)
  {
    return true;
  }

  return false;
}

bool isSquare(const Shape &shape)
{
  if (isRectangle(shape))
  {
    int side1 = getLength(shape[0], shape[1]);
    int side2 = getLength(shape[1], shape[2]);
    if (side1 == side2)
    {
      return true;
    }
  }

  return false;
}

Shape readPoints(std::string &line, int vertexes)
{
  Shape shape;
  std::size_t openBracket;
  std::size_t closeBracket;
  std::size_t semicolon;
  for (int i = 0; i < vertexes; ++i)
  {
    openBracket = line.find_first_of('(');
    closeBracket = line.find_first_of(')');
    semicolon = line.find_first_of(';');
    if ((openBracket == std::string::npos) || (closeBracket == std::string::npos) || (semicolon == std::string::npos))
    {
      throw std::invalid_argument("Incorrect line!");
    }

    Point_t point
      {
      std::stoi(line.substr(openBracket + 1, semicolon - openBracket - 1)),
      std::stoi(line.substr(semicolon + 1, closeBracket - semicolon - 1))
      };
    line.erase(0, closeBracket + 1);
    shape.push_back(point);
  }

  return shape;
}

void removePentagons(std::vector<Shape> &shapes)
{
  shapes.erase(std::remove_if(std::begin(shapes), std::end(shapes),[&](const Shape &shape)
  {
    return shape.size() == AMOUNT_OF_PENTAGON_VERTEX;
  }), std::end(shapes));
}
