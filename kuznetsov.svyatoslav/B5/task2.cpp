#include <sstream>
#include "functions.hpp"

void task2()
{
  std::string line;
  std::vector<Shape> shapes;
  std::size_t vertexes = 0;
  std::size_t squaresNum = 0;
  std::size_t triangleNum = 0;
  std::size_t rectangleNum = 0;
  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error while reading data!");
    }

    std::stringstream stream(line);
    std::size_t amountOfVertexes;
    if (!(stream >> amountOfVertexes))
    {
      continue;
    }

    if (amountOfVertexes < AMOUNT_OF_TRIANGLE_VERTEX)
    {
      throw std::invalid_argument("Invalid number of vertexes!");
    }

    shapes.push_back(readPoints(line, amountOfVertexes));
  }

  std::for_each(std::begin(shapes), std::end(shapes), [&](const Shape &shape)
  {
    vertexes += shape.size();
    if (shape.size() == AMOUNT_OF_TRIANGLE_VERTEX)
    {
      ++triangleNum;
    }
    else if (shape.size() == AMOUNT_OF_RECTANGLE_VERTEX) {
      if (isRectangle(shape)) {
        ++rectangleNum;
        if (isSquare(shape)) {
          ++squaresNum;
        }
      }
    }
  });

  removePentagons(shapes);
  Shape newPoints(shapes.size());
  std::transform(std::begin(shapes), std::end(shapes), std::begin(newPoints), [&](const Shape &shape) { return shape[0];});
  std::sort(std::begin(shapes), std::end(shapes), [&](const Shape &lhs, const Shape &rhs)
  {
    if (lhs.size() < rhs.size())
    {
      return true;
    }

    if ((lhs.size() == AMOUNT_OF_RECTANGLE_VERTEX) && (rhs.size() == AMOUNT_OF_RECTANGLE_VERTEX))
    {
      if (isSquare(lhs))
      {
        if (isSquare(rhs))
        {
          return false;
        }
        return true;
      }
    }

    return false;
  });

  std::cout << "Vertices: " << vertexes << '\n';
  std::cout << "Triangles: " << triangleNum << '\n';
  std::cout << "Squares: " << squaresNum << '\n';
  std::cout << "Rectangles: " << rectangleNum << '\n';

  std::cout << "Points: ";
  for (Point_t point : newPoints)
  {
    std::cout << " (" << point.x << "; " << point.y << ')';
  }

  std::cout << "\nShapes:\n";
  for (Shape shape : shapes)
  {
    std::cout << shape.size();
    for (Point_t point : shape)
    {
      std::cout << " (" << point.x << "; " << point.y << ')';
    }

    std::cout << '\n';
  }
}
