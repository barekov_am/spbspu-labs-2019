#ifndef B5_FUNCTIONS_HPP
#define B5_FUNCTIONS_HPP

#include <string>
#include <iostream>
#include <algorithm>
#include "shape.hpp"

const int AMOUNT_OF_TRIANGLE_VERTEX = 3;
const int AMOUNT_OF_RECTANGLE_VERTEX = 4;
const int AMOUNT_OF_PENTAGON_VERTEX = 5;

int getLength(const Shape &shape);
bool isRectangle(const Shape &shape);
bool isSquare(const Shape &shape);
Shape readPoints(std::string &line, int vertexes);
void removePentagons(std::vector<Shape> &shapes);

#endif //B5_FUNCTIONS_HPP
