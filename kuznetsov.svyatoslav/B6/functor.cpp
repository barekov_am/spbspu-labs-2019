#include "functor.hpp"
#include <algorithm>

Functor::Functor()
{
  max_ = std::numeric_limits<int>::min(),
  min_ = std::numeric_limits<int>::max(),
  positives_ = 0,
  negatives_ = 0,
  first_ = 0,
  oddSum_ = 0,
  evenSum_ = 0,
  amount_ = 0,
  equal_ = false;
}

void Functor::operator()(int number)
{
  if (amount_ == 0)
  {
    first_ = number;
  }

  if (number > max_)
  {
    max_ = number;
  }

  if (number < min_)
  {
    min_ = number;
  }

  if (number > 0)
  {
    ++positives_;
  }

  if (number < 0)
  {
    ++negatives_;
  }

  if (number % 2 == 0)
  {
    evenSum_ += number;
  }
  else
  {
    oddSum_ += number;
  }

  equal_ = (first_ == number);
  ++amount_;
}

long long int Functor::getMax()
{
  return max_;
}

long long int Functor::getMin()
{
  return min_;
}

double Functor::getAverage()
{
  return static_cast<double>(oddSum_ + evenSum_) / amount_;
}

long int Functor::getAmountOfPositive()
{
  return positives_;
}

long int Functor::getAmountOfNegative()
{
  return negatives_;
}

long long int Functor::getOddSum()
{
  return oddSum_;
}

long long int Functor::getEvenSum()
{
  return evenSum_;
}

bool Functor::isFirstEqualLast()
{
  return equal_;
}

bool Functor::empty()
{
  return amount_ == 0;
}
