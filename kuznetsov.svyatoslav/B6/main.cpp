#include <iostream>

void task();

int main() {
  try
  {
    task();
  }
  catch (const std::exception &ex)
  {
    std::cerr << ex.what();
    return 1;
  }

  return 0;
}
