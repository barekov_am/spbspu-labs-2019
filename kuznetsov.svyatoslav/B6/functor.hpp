#ifndef B6_FUNCTOR_HPP
#define B6_FUNCTOR_HPP

class Functor
{
public:
  Functor();
  void operator()(int number);
  long long int getMax();
  long long int getMin();
  double getAverage();
  long int getAmountOfPositive();
  long int getAmountOfNegative();
  long long int getOddSum();
  long long int getEvenSum();
  bool isFirstEqualLast();
  bool empty();

private:
  long long int max_;
  long long int min_;
  long int positives_;
  long int negatives_;
  long long int first_;
  long long int oddSum_;
  long long int evenSum_;
  long int amount_;
  bool equal_;
};

#endif //B6_FUNCTOR_HPP
