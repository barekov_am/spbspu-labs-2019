#include <iostream>
#include <iterator>
#include <algorithm>
#include "functor.hpp"

void task()
{
  Functor functor;
  functor = std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), functor);
  if (!std::cin.eof())
  {
    throw std::ios_base::failure("Error while reading data\n");
  }

  if (functor.empty())
  {
    std::cout << "No Data\n";
  }
  else
  {
    std::cout << "Max: " << functor.getMax() << "\n";
    std::cout << "Min: " << functor.getMin() << "\n";
    std::cout << "Mean: " << functor.getAverage() << "\n";
    std::cout << "Positive: " << functor.getAmountOfPositive() << "\n";
    std::cout << "Negative: " << functor.getAmountOfNegative() << "\n";
    std::cout << "Odd Sum: " << functor.getOddSum() << "\n";
    std::cout << "Even Sum: " << functor.getEvenSum() << "\n";
    std::cout << "First/Last Equal: " << (functor.isFirstEqualLast() ? "yes" : "no") << "\n";
  }
}
