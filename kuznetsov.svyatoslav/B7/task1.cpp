#include <iterator>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>

void task1()
{
  std::vector<double> numbers((std::istream_iterator<double>(std::cin)), std::istream_iterator<double>());
  if (!std::cin.eof())
  {
    throw std::invalid_argument("Error while reading data!");
  }

  std::transform(numbers.begin(), numbers.end(), numbers.begin(), std::bind1st(std::multiplies<double>(), M_PI));
  for (double num : numbers)
  {
    std::cout << num << " ";
  }

  std::cout << '\n';
}
