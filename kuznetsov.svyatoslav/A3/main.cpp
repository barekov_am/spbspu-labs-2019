#include <iostream>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

int main ()
{
  kuznetsov::Rectangle rect1({1.0, 2.5}, 4.1, 2.2);
  kuznetsov::Rectangle rect2({-1.0, 2.0}, 2.2, 5.3);
  kuznetsov::Circle circle1({4.1, 2.5}, 4.0);
  kuznetsov::Circle circle2({-1.0, 3.2}, 3.1);

  std::cout << "Initialisation of composite shape \n";
  kuznetsov::CompositeShape compShape;
  compShape.add(std::make_shared<kuznetsov::Circle>(circle1));
  compShape.printInfo();

  std::cout << "Adding other shapes in composite shape \n";
  compShape.add(std::make_shared<kuznetsov::Rectangle>(rect1));
  compShape.add(std::make_shared<kuznetsov::Rectangle>(rect2));
  compShape.add(std::make_shared<kuznetsov::Circle>(circle2));
  compShape.printInfo();

  std::cout << "Move to {1.6, 2.5} \n";
  compShape.move({1.6, 2.5});
  compShape.printInfo();

  std::cout << "Shift at (2.1, 5.0) \n";
  compShape.move(2.1, 5.0);
  compShape.printInfo();

  std::cout << "Scale in 2.1 times \n";
  compShape.scale(2.1);
  compShape.printInfo();

  std::cout << "Remove shape with index 3 \n";
  compShape.remove(2);
  compShape.printInfo();

  std::cout << "Accessing a shape with index 0 through composite shape \n";
  std::shared_ptr<kuznetsov::Shape> shape = compShape[0];
  shape->printInfo();

  return 0;
}
