#ifndef B8_PARSER_H
#define B8_PARSER_H

#include <vector>
#include <iostream>

struct element_t
{
  enum
  {
    WORD,
    PUNCTUATION,
    NUMBER,
    DASH
  } types;
  std::string content;
};

class Parser {
public:
  Parser(size_t width = 40);
  void read();
  void print();

private:
  size_t width_;
  std::vector<element_t> lines_;
  void readWord(std::string &line);
  void readNumber(std::string &line);
  void readDash(std::string &line);
};


#endif //B8_PARSER_H
