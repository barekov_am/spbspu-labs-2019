#include <iostream>
#include "parser.h"

const size_t DEFAULT_LINE_WIDTH = 40;
const size_t MIN_LINE_WIDTH = 25;

int main(int argc, char *argv[])
{
  try
  {
    if (argc != 1 && argc != 3)
    {
      throw std::invalid_argument("Invalid number of parameters");
    }

    size_t lineWidth = DEFAULT_LINE_WIDTH;
    if (argc == 3)
    {
      if (std::string(argv[1]) != "--line-width")
      {
        throw std::invalid_argument("Invalid first parameter");
      }

      lineWidth = std::stoi(argv[2]);
      if (lineWidth < MIN_LINE_WIDTH)
      {
        throw std::invalid_argument("Invalid width");
      }
    }

    Parser parser(lineWidth);
    parser.read();
    parser.print();
  }
  catch (std::exception &ex)
  {
    std::cerr << ex.what() << '\n';
    return 1;
  }

  return 0;
}
