#include "parser.h"
#include <algorithm>

const size_t MAX_WIDTH = 20;
const size_t MAX_NUMBER_OF_DASH = 3;

Parser::Parser(size_t width) :
  width_(width),
  lines_()
{
}

void Parser::read()
{
  while (std::cin)
  {
    std::cin >> std::noskipws;
    char firstChar = std::cin.get();
    if (std::isalpha(firstChar))
    {
      std::string line;
      line += firstChar;
      readWord(line);
      element_t element{element_t::WORD, line};
      lines_.push_back(element);
    }
    else if (((firstChar == '-' || firstChar == '+') && (std::isdigit(std::cin.peek()))) || std::isdigit(firstChar))
    {
      std::string line;
      line += firstChar;
      readNumber(line);
      element_t element{element_t::NUMBER, line};
      lines_.push_back(element);
    }
    else if (std::ispunct(firstChar))
    {
      if (firstChar == '-' && std::cin.peek() == '-')
      {
        std::string line;
        line += firstChar;
        readDash(line);
        element_t element{element_t::DASH, line};
        lines_.push_back(element);
      }
      else if (!std::ispunct(std::cin.peek()))
      {
        std::string string;
        string += firstChar;
        element_t token{element_t::PUNCTUATION, string};
        lines_.push_back(token);
      }
      else
      {
        throw std::invalid_argument("Invalid data");
      }
    }
  }
}

void Parser::print()
{
  if (lines_.empty())
  {
    return;
  }

  if (lines_.at(0).types == element_t::DASH || lines_.at(0).types == element_t::PUNCTUATION)
  {
    throw std::invalid_argument("Invalid data with first element");
  }

  for (size_t i = 0; i < lines_.size() - 1; ++i)
  {
    if (lines_.at(i).types == element_t::PUNCTUATION && lines_.at(i + 1).types == element_t::PUNCTUATION)
    {
      throw std::invalid_argument("Invalid data with two punctuation in a row");
    }
  }

  size_t countOfElements = 0;
  size_t numberOfLines = 0;
  std::vector<std::string> tmpLines;
  std::string line;
  while (countOfElements < lines_.size())
  {
    if (line.empty())
    {
      if (lines_.at(countOfElements).types == element_t::PUNCTUATION || lines_.at(countOfElements).types == element_t::DASH)
      {
        if (lines_.at(countOfElements).types == element_t::PUNCTUATION && tmpLines.at(numberOfLines - 1).size() < width_)
        {
          tmpLines.at(numberOfLines - 1) += lines_.at(countOfElements).content;
          ++countOfElements;
        }
        else
        {
          line += lines_.at(countOfElements - 1).content;
          (lines_.at(countOfElements).types == element_t::DASH) ? (line += ' ' + lines_.at(countOfElements).content)
                                                                : (line += lines_.at(countOfElements).content);
          auto position = tmpLines.at(numberOfLines - 1).find_last_of(' ');
          tmpLines.at(numberOfLines - 1).erase(position);
          ++countOfElements;
        }
      }
      else
      {
        line += lines_.at(countOfElements).content;
        ++countOfElements;
      }

      continue;
    }

    if (line.size() + lines_.at(countOfElements).content.size() + 1 <= width_)
    {
      if (lines_.at(countOfElements).types == element_t::PUNCTUATION)
      {
        line += lines_.at(countOfElements).content;
        ++countOfElements;
      }
      else
      {
        line += ' ' + lines_.at(countOfElements).content;
        ++countOfElements;
      }

      continue;
    }

    tmpLines.push_back(line);
    line.clear();
    ++numberOfLines;
  }
  tmpLines.push_back(line);

  std::for_each(tmpLines.begin(), tmpLines.end(), [](std::string lineForPrint)
  {
    std::cout << lineForPrint << '\n';
  });
}

void Parser::readWord(std::string &line)
{
  while (std::cin)
  {
    if (std::cin.peek() == '-' || std::isalpha(std::cin.peek()))
    {
      line += std::cin.get();
      if (line.back() == '-' && std::cin.peek() == '-')
      {
        throw std::invalid_argument("Invalid data");
      }
    }
    else
    {
      break;
    }
  }

  if (line.size() > MAX_WIDTH)
  {
    throw std::invalid_argument("Line is too long");
  }
}

void Parser::readNumber(std::string &line)
{
  bool wasDot = false;
  while (std::cin)
  {
    if (std::cin.peek() == '.' || std::isdigit(std::cin.peek()))
    {
      line += std::cin.get();
      if (line.back() == '.')
      {
        if (wasDot)
        {
          throw std::invalid_argument("You entered more than 1 dot in a row");
        }
        wasDot = true;
      }
    }
    else
    {
      break;
    }
  }

  if (line.size() > MAX_WIDTH)
  {
    throw std::invalid_argument("Line is too long");
  }
}

void Parser::readDash(std::string &line)
{
  size_t numberOfDash = 1;
  while (std::cin)
  {
    if (std::cin.peek() == '-')
    {
      line += std::cin.get();
      ++numberOfDash;
    }
    else
    {
      break;
    }
  }

  if (numberOfDash > MAX_NUMBER_OF_DASH)
  {
    throw std::invalid_argument("Too many dashes");
  }
}
