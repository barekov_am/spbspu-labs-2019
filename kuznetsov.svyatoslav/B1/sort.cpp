#include "sort.hpp"

bool isDescending(const std::string &direction)
{
  if (direction == "ascending")
  {
    return false;
  }
  else if (direction != "descending")
  {
    throw std::invalid_argument("Invalid sort direction");
  }

  return true;
}
