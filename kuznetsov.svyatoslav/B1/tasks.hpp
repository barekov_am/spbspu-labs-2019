#ifndef B1_TASKS_HPP
#define B1_TASKS_HPP

#include <iostream>
#include <vector>

void task1(const std::string &direction);
void task2(const std::string &fileName);
void task3();
void task4(const std::string &direction, int size);

#endif //B1_TASKS_HPP
