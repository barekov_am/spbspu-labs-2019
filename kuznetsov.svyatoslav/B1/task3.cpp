#include "tasks.hpp"
#include "print.hpp"

void adding(std::vector<int> &vector)
{
  if (vector.back() == 1)
  {
    for (auto it = vector.begin(); it != vector.end();)
    {
      if ((*it) % 2 == 0)
      {
        it = vector.erase(it);
        continue;
      }

      ++it;
    }
  }
  else if (vector.back() == 2)
  {
    for (auto it = vector.begin(); it != vector.end();)
    {
      if ((*it) % 3 == 0)
      {
        it = vector.insert(it + 1, 3, 1);
        it += 2;
      }

      ++it;
    }
  }
}

void task3()
{
  std::vector<int> vector;
  int currentNumber = -1;

  while ((std::cin) && (!(std::cin >> currentNumber).eof()))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error while reading data");
    }

    if (currentNumber == 0)
    {
      break;
    }

    vector.push_back(currentNumber);
  }

  if (vector.empty())
  {
    return;
  }

  if (std::cin.eof() && currentNumber != 0)
  {
    throw std::invalid_argument("You did not enter zero at the end of input");
  }

  adding(vector);
  print(vector);
}
