#include "tasks.hpp"
#include <forward_list>
#include "sort.hpp"
#include "print.hpp"

void task1(const std::string &direction)
{
  const bool dir = isDescending(direction);
  std::vector<int> vectorAt;
  int currentNumber = -1;

  while ((std::cin) && (!(std::cin >> currentNumber).eof())) {
    if (std::cin.fail()) {
      throw std::ios_base::failure("Error while reading data");
    }

    if (currentNumber == 0) {
      break;
    }

    vectorAt.push_back(currentNumber);
  }

  std::vector<int> vectorBrackets(vectorAt);
  std::forward_list<int> list(vectorAt.begin(), vectorAt.end());

  sort<AccessWithAt>(vectorAt, dir);
  sort<AccessWithBrackets>(vectorBrackets, dir);
  sort<AccessWithIterator>(list, dir);

  print(vectorAt);
  print(vectorBrackets);
  print(list);
}
