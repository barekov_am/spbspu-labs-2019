#ifndef B1_ACCESS_TO_ELEMENTS_HPP
#define B1_ACCESS_TO_ELEMENTS_HPP

#include <iterator>

template <typename Container>
struct AccessWithBrackets
{
  typedef typename std::size_t index_type;
  typedef typename Container::value_type value_type;

  static value_type &getElement(Container &container, index_type index)
  {
    return container[index];
  }

  static index_type begin(const Container &)
  {
    return 0;
  }

  static index_type end(const Container &container)
  {
    return container.size();
  }
};

template <typename Container>
struct AccessWithAt
{
  typedef typename std::size_t index_type;
  typedef typename Container::value_type value_type;

  static value_type &getElement(Container &container, index_type index)
  {
    return container.at(index);
  }

  static index_type begin(const Container &)
  {
    return 0;
  }

  static index_type end(const Container &container)
  {
    return container.size();
  }
};

template <typename Container>
struct AccessWithIterator
{
  typedef typename Container::iterator index_type;
  typedef typename Container::value_type value_type;

  static value_type &getElement(Container &, index_type iter)
  {
    return *iter;
  }

  static index_type begin(Container &container)
  {
    return container.begin();
  }

  static index_type end(Container &container)
  {
    return container.end();
  }
};

#endif //B1_ACCESS_TO_ELEMENTS_HPP
