#include "tasks.hpp"
#include "print.hpp"
#include "sort.hpp"

void fillRandom(double *array, int size)
{
  for (int i = 0; i < size; ++i)
  {
    array[i] = (rand() % 21 - 10) * 1.0 / 10;
  }
}

void task4(const std::string &direction, int size)
{
  const bool dir = isDescending(direction);

  if (size <= 0)
  {
    throw std::invalid_argument("Invalid size of vector");
  }

  std::vector<double> vector(size);
  fillRandom(vector.data(), size);
  print(vector);
  sort<AccessWithAt>(vector, dir);
  print(vector);
}
