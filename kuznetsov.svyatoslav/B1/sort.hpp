#ifndef B1_SORT_HPP
#define B1_SORT_HPP

#include "accessToElements.hpp"

bool isDescending(const std::string &direction);

template <template <class T> class AccessType, typename ContainerType>
void sort(ContainerType &container, const bool descending)
{
  typedef AccessType<ContainerType> Policy;
  typedef typename AccessType<ContainerType>::index_type index_type;

  for (index_type i = Policy::begin(container); i != Policy::end(container); ++i)
  {
    index_type currentIndex = i;
    for (index_type j = i; j != Policy::end(container); j++)
    {
      if (descending ? Policy::getElement(container, currentIndex) < Policy::getElement(container, j)
                     : Policy::getElement(container, currentIndex) > Policy::getElement(container, j))
      {
        currentIndex = j;
      }
    }
    std::swap(Policy::getElement(container, i), Policy::getElement(container, currentIndex));
  }
}

#endif //B1_SORT_HPP
