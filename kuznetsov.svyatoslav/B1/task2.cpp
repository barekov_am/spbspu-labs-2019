#include "tasks.hpp"
#include <fstream>
#include <memory>

const std::size_t initialSize = 1024;

void task2(const std::string &fileName)
{
  using charsArr = std::unique_ptr<char[], decltype(&free)>;

  std::ifstream file(fileName);

  if (!file)
  {
    throw std::invalid_argument("No such file.");
  }

  std::size_t size = initialSize;
  charsArr array(static_cast<char *>(malloc(size)), &free);

  if (!array) {
    throw std::runtime_error("Memory couldn't be allocated!\n");
  }

  std::size_t ind = 0;

  while (file)
  {
    file.read(&array[ind], initialSize);
    ind += file.gcount();

    if (file.gcount() == initialSize)
    {
      size += initialSize;
      charsArr tmp_array(static_cast<char *>(realloc(array.get(),size)), &free);

      if (!tmp_array)
      {
        throw std::runtime_error("Unable to allocate memory");
      }

      array.release();
      std::swap(tmp_array, array);
    }

    if (!file.eof() && file.fail())
    {
      throw std::runtime_error("Error while reading");
    }
  }

  std::vector<char> vector(&array[0], &array[ind]);

  for (char it : vector)
  {
    std::cout << it;
  }
}
