#ifndef B2_QUEUE_WITH_PRIORITY_HPP
#define B2_QUEUE_WITH_PRIORITY_HPP

#include <list>

template <typename QueueElementType>
class QueueWithPriority
{
public:
  enum ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  template <typename Handler>
  void get(Handler handler);
  void add(const QueueElementType &element, ElementPriority priority);
  void accelerate();
  bool empty();

private:
  std::list<QueueElementType> low_;
  std::list<QueueElementType> normal_;
  std::list<QueueElementType> high_;
};

template <typename QueueElementType>
template <typename Handler>
void QueueWithPriority<QueueElementType>::get(Handler handler)
{
  if (!high_.empty())
  {
    handler(high_.front());
    high_.pop_front();
  }
  else if (!normal_.empty())
  {
    handler(normal_.front());
    normal_.pop_front();
  }
  else if (!low_.empty())
  {
    handler(low_.front());
    low_.pop_front();
  }
}

template <typename QueueElementType>
void QueueWithPriority<QueueElementType>::add(const QueueElementType &element, ElementPriority priority)
{
  switch (priority)
  {
  case ElementPriority::LOW:
    low_.push_back(element);
    break;
  case ElementPriority::NORMAL:
    normal_.push_back(element);
    break;
  case ElementPriority::HIGH:
    high_.push_back(element);
    break;
  }
}

template <typename QueueElementType>
void QueueWithPriority<QueueElementType>::accelerate()
{
  high_.splice(high_.end(), low_);
}

template <typename QueueElementType>
bool QueueWithPriority<QueueElementType>::empty()
{
  return (low_.empty() && normal_.empty() && high_.empty());
}

#endif //B2_QUEUE_WITH_PRIORITY_HPP
