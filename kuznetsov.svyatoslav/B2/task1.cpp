#include "tasks.hpp"
#include <stdexcept>
#include "queue-with-priority.hpp"
#include "commands.hpp"

void task1()
{
  QueueWithPriority<std::string> queue;
  static const struct
  {
    const std::string name;
    std::function<void(QueueWithPriority<std::string> &queue, std::stringstream &args)> function;
  } commands[] = {
    {"add", &add},
    {"get", &get},
    {"accelerate", &accelerate}
    };
  std::string line;

  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error while reading data!");
    }

    std::stringstream inputStream(line);
    std::string command;
    inputStream >> command;
    auto findCommand = std::find_if(std::begin(commands), std::end(commands), [&](const auto &elem) {return elem.name == command;});
    inputStream >> std::ws;

    if (findCommand != std::end(commands))
    {
      findCommand->function(queue, inputStream);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
