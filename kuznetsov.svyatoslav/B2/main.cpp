#include <iostream>
#include "tasks.hpp"

int main(int argc, char *argv[])
{
  if (argc != 2)
  {
    std::cerr << "Invalid number of arguments!\n";
    return 1;
  }

  try
  {
    switch (atoi(argv[1]))
    {
    case (1):
    {
      task1();
      break;
    }
    case (2):
    {
      task2();
      break;
    }
    default:
    {
      std::cerr << "Invalid task number!\n";
      return 1;
    }
    }
  }
  catch (std::exception &e)
  {
    std::cerr << e.what() << "\n";
    return 1;
  }

  return 0;
}
