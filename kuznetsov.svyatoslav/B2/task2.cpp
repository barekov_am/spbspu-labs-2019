#include <iostream>
#include <list>
#include "commands.hpp"

void task2()
{
  const int min = 1;
  const int max = 20;
  const int maxLength = 20;
  int number = 0;
  std::list<int> list;

  while (std::cin && !(std::cin >> number).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error while reading!");
    }

    if (number < min || number > max)
    {
      throw std::invalid_argument("Wrong number!");
    }

    if (list.size() == maxLength)
    {
      throw std::invalid_argument("The list is full!");
    }

    list.push_back(number);
  }

  printNumbers(list.begin(), list.end());
}
