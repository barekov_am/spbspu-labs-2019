#ifndef B2_COMMANDS_HPP
#define B2_COMMANDS_HPP

#include <sstream>
#include <functional>
#include <list>
#include <algorithm>
#include <iostream>

template <typename QueueElementType>
class QueueWithPriority;

void add(QueueWithPriority<std::string> &, std::stringstream &);
void get(QueueWithPriority<std::string> &, std::stringstream &);
void accelerate(QueueWithPriority<std::string> &, std::stringstream &);
void printNumbers(std::list<int>::iterator left, std::list<int>::iterator right);

#endif //B2_COMMANDS_HPP
