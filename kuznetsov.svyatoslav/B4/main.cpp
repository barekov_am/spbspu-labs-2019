#include <sstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include "data-struct.hpp"

DataStruct readData(std::istream &line);
bool compare(DataStruct &lhs, DataStruct &rhs);

int main() {
  try
  {
    std::vector<DataStruct> vector;
    std::string line;
    while (std::getline(std::cin, line))
    {
      std::stringstream stream(line);
      vector.push_back(readData(stream));
    }

    if (!vector.empty())
    {
      std::sort(vector.begin(), vector.end(), compare);
    }

    for (auto i = vector.begin(); i != vector.end(); ++i)
    {
      std::cout << i->key1 << ',' << i->key2 << ',' << i->str << '\n';
    }
  }
  catch (const std::exception &ex)
  {
    std::cerr << ex.what();
    return 1;
  }

  return 0;
}
