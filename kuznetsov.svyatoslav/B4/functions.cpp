#include <iostream>
#include "data-struct.hpp"

const int MIN_VALUE = -5;
const int MAX_VALUE = 5;

DataStruct readData(std::istream &line)
{
  int key1;
  int key2;
  std::string firstKey;
  std::string secondKey;
  std::string str;
  char ch;

  while (!line.eof())
  {
    ch = line.get();
    if (std::isdigit(ch))
    {
      firstKey.push_back(ch);
    }
    else if ((ch == '+') || (ch == '-'))
    {
      if (!firstKey.empty())
      {
        throw std::invalid_argument("Invalid key1!");
      }
      firstKey.push_back(ch);
    }
    else if (ch == ',')
    {
      break;
    }
    else if (!std::isspace(ch))
    {
      throw std::invalid_argument("Invalid key1!");
    }
  }

  while (!line.eof())
  {
    ch = line.get();
    if (std::isdigit(ch))
    {
      secondKey.push_back(ch);
    }
    else if ((ch == '+') || (ch == '-'))
    {
      if (!secondKey.empty())
      {
        throw std::invalid_argument("Invalid key2!");
      }
      secondKey.push_back(ch);
    }
    else if (ch == ',')
    {
      break;
    }
    else if (!std::isspace(ch))
    {
      throw std::invalid_argument("Invalid key2!");
    }
  }

  std::getline(line, str);
  if (firstKey.empty() || secondKey.empty() || str.empty())
  {
    throw std::invalid_argument("You entered an empty string!");
  }

  key1 = std::stoi(firstKey);
  key2 = std::stoi(secondKey);
  if ((key1 < MIN_VALUE) || (key1 > MAX_VALUE) || (key2 < MIN_VALUE) || (key2 > MAX_VALUE))
  {
    throw std::invalid_argument("One key is out of range!");
  }
  
  DataStruct tmpData = {key1, key2, str};
  return tmpData;
}

bool compare(DataStruct &lhs, DataStruct &rhs)
{
  if (lhs.key1 < rhs.key1)
  {
    return true;
  }
  else if (lhs.key1 == rhs.key1)
  {
    if (lhs.key2 == rhs.key2)
    {
      if (lhs.str.size() < rhs.str.size())
      {
        return true;
      }
    }
    else if (lhs.key2 < rhs.key2)
    {
      return true;
    }
  }

  return false;
}
