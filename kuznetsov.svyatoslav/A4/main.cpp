#include <iostream>
#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "splitting.hpp"


int main()
{
  std::cout << "-----Rectangle-----\n";
  kuznetsov::Rectangle rect({1.1, 2.2}, 3.3, 4.0);
  kuznetsov::Shape *shape = &rect;
  shape->printInfo();
  std::cout << "Shift to (2.5, 3.5)!\n";
  shape->move(2.5, 3.5);
  shape->printInfo();
  std::cout << "Move to {5.1, 6.2}!\n";
  shape->move({5.1, 6.2});
  shape->printInfo();
  std::cout << "Scale in 4 times!\n";
  shape->scale(4);
  shape->printInfo();
  std::cout << "Rotate by 45 degrees!\n";
  shape->rotate(45);
  shape->printInfo();

  std::cout << "-----Circle-----\n";
  kuznetsov::Circle circle({2.5, 3.5}, 4.5);
  shape = &circle;
  shape->printInfo();
  std::cout << "Shift to (11.1, 2.1)!\n";
  shape->move(11.1, 2.1);
  shape->printInfo();
  std::cout << "Move to {1.5, 6.2}!\n";
  shape->move({1.5, 6.2});
  shape->printInfo();
  std::cout << "Scale in 0.5 times!\n";
  shape->scale(0.5);
  shape->printInfo();
  std::cout << "Rotate by 45 degrees!\n";
  shape->rotate(45);
  shape->printInfo();

  kuznetsov::Circle circle1({1.0, 2.0}, 3.0);
  kuznetsov::Circle circle2({3.7, -1.2}, 6.6);
  kuznetsov::Rectangle rect1({2.2, 2.2}, 2.8, 3.0);
  kuznetsov::Rectangle rect2({8.8, 1.6}, 4.4, 5.0);

  std::cout << "Create composite shape:\n";
  kuznetsov::CompositeShape compositeShape;
  compositeShape.add(std::make_shared<kuznetsov::Circle>(circle1));
  compositeShape.printInfo();

  std::cout << "Add other shapes in composite:\n";
  compositeShape.add(std::make_shared<kuznetsov::Rectangle>(rect1));
  compositeShape.add(std::make_shared<kuznetsov::Rectangle>(rect2));
  compositeShape.add(std::make_shared<kuznetsov::Circle>(circle2));
  compositeShape.printInfo();

  std::cout << "Shift at (3.3, 3.2)\n";
  compositeShape.move(3.3, 3.2);
  compositeShape.printInfo();

  std::cout << "Remove shapes which have index 2 and 1\n";
  compositeShape.remove(2);
  compositeShape.remove(1);
  compositeShape.printInfo();

  std::cout << "Move to {1.5, -8.9}\n";
  compositeShape.move({1.5, -8.9});
  compositeShape.printInfo();

  std::cout << "Get shape which has index 0 and print information about it\n";
  std::shared_ptr<kuznetsov::Shape> shapeFromComposite = compositeShape[0];
  shapeFromComposite->printInfo();

  std::cout << "Scale composite shape in 0,9 times\n\n";
  compositeShape.scale(0.9);
  compositeShape.printInfo();

  std::cout << "Add new shape\n";
  compositeShape.add(std::make_shared<kuznetsov::Circle>(circle2));
  compositeShape.printInfo();

  std::cout << "Rotate composite shape by 40 degrees\n";
  compositeShape.rotate(40.0);
  compositeShape.printInfo();

  std::cout << "Get partition of composite shape\n";
  kuznetsov::Matrix<kuznetsov::Shape> matrix = kuznetsov::split(compositeShape);
  std::size_t layer = matrix.getRows();
  for (std::size_t i = 0; i < layer; ++i)
  {
    std::cout << "Layer: " << i + 1 << "\n";
    std::cout << "Shapes in the layer:\n";
    std::size_t columns = matrix[i].getSize();
    for (std::size_t j = 0; j < columns; ++j)
    {
      if (matrix[i][j] != nullptr)
      {
        std::cout << j + 1 << ":\n";
        std::cout << "Area: " << matrix[i][j]->getArea() << "\n";
        const kuznetsov::rectangle_t frameRect = matrix[i][j]->getFrameRect();
        std::cout << "Position: (" << frameRect.pos.x << "," << frameRect.pos.y << ")\n";
      }
    }
  }

  return 0;
}
