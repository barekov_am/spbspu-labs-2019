//
// Created by Andrei Soprachev on 2019-04-23.
//

#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

int main()
{

  //прямоугольник по центру, высоте и ширине
  soprachev::Rectangle rectangleByPosWH = soprachev::Rectangle(soprachev::point_t{1, 5}, 10, 10);

  //прямоугольник по двум вершинам
  soprachev::Rectangle rectangleByLeftDownRightUp = soprachev::Rectangle(soprachev::point_t{4, 4},
    soprachev::point_t{5, 5});

  //круг по центру и радиусу
  soprachev::Circle circle = soprachev::Circle(soprachev::point_t{3, 4}, 2);

  //треугольник по трём точкам
  soprachev::Triangle triangleByWorldVert = soprachev::Triangle(soprachev::point_t{4, 5}, soprachev::point_t{-1, 9},
    soprachev::point_t{0, 3});

  soprachev::point_t pentagonPoints[5] = {
    soprachev::point_t{0, 0},
    soprachev::point_t{4, 0},
    soprachev::point_t{5.23, 3.8},
    soprachev::point_t{2, 6.16},
    soprachev::point_t{-1.23, 3.8}
  };
  //около правильный пятиугольник
  soprachev::Polygon polygon = soprachev::Polygon(pentagonPoints, 5);

  soprachev::Shape *shapes[5] = {
    &rectangleByPosWH,
    &rectangleByLeftDownRightUp,
    &circle,
    &triangleByWorldVert,
    &polygon
  };

  for (soprachev::Shape *shape : shapes)
  {
    shape->print(std::cout) << "\n\n";
  }

  shapes[0]->move(1, 2); //перемещение на вектор
  shapes[1]->move(soprachev::point_t{1, 1}); //перемещение в точку
  shapes[2]->move(-1, -3); //перемещение на вектор
  shapes[3]->move(1, 2); //перемещение на вектор
  shapes[4]->move(1, 2); //перемещение на вектор

  std::cout << "after move: \n\n";
  for (soprachev::Shape *shape : shapes)
  {
    shape->print(std::cout) << "\n\n";
  }

  //scaled
  for (soprachev::Shape *shape : shapes)
  {
    shape->scale(2);
  }

  std::cout << "after scale: \n\n";
  for (soprachev::Shape *shape : shapes)
  {
    shape->print(std::cout) << "\n\n";
  }
  return 0;
}
