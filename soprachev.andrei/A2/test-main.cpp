//
// Created by Andrei Soprachev on 2019-04-24.
//
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE A2

#include <stdexcept>

#include <boost/test/included/unit_test.hpp>

#include "triangle.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "polygon.hpp"

BOOST_AUTO_TEST_SUITE(soprachevA2TestSuite)

  const double EPSILON = 0.00001;

  BOOST_AUTO_TEST_CASE(circleConstantAfterMove)
  {
    soprachev::Circle circle({5, 5}, 4);
    const soprachev::rectangle_t frameBefore = circle.getFrameRect();
    const double areaBefore = circle.getArea();

    circle.move({0, 0});
    soprachev::rectangle_t frameAfter = circle.getFrameRect();
    double areaAfter = circle.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    circle.move(5, 5);
    frameAfter = circle.getFrameRect();
    areaAfter = circle.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(rectangleConstantAfterMove)
  {
    soprachev::Rectangle rectangle({5, 5}, 4, 6);
    const soprachev::rectangle_t frameBefore = rectangle.getFrameRect();
    const double areaBefore = rectangle.getArea();

    rectangle.move({0, 0});
    soprachev::rectangle_t frameAfter = rectangle.getFrameRect();
    double areaAfter = rectangle.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    rectangle.move(5, 5);
    frameAfter = rectangle.getFrameRect();
    areaAfter = rectangle.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(triangleConstantAfterMove)
  {
    soprachev::Triangle triangle({5, 5}, {3, 6}, {7, 8});
    const soprachev::rectangle_t frameBefore = triangle.getFrameRect();
    const double areaBefore = triangle.getArea();

    triangle.move({5, 5});
    soprachev::rectangle_t frameAfter = triangle.getFrameRect();
    double areaAfter = triangle.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    triangle.move(5, 5);
    frameAfter = triangle.getFrameRect();
    areaAfter = triangle.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(polygonConstantAfterMove)
  {
    //похожий на правильный пятиугольник
    soprachev::point_t pentagonPoints[5] = {
      soprachev::point_t{0, 0},
      soprachev::point_t{4, 0},
      soprachev::point_t{5.23, 3.8},
      soprachev::point_t{2, 6.16},
      soprachev::point_t{-1.23, 3.8}
    };
    soprachev::Polygon polygon(pentagonPoints, 5);
    const soprachev::rectangle_t frameBefore = polygon.getFrameRect();
    const double areaBefore = polygon.getArea();

    polygon.move({5, 5});
    soprachev::rectangle_t frameAfter = polygon.getFrameRect();
    double areaAfter = polygon.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    polygon.move(5, 5);
    frameAfter = polygon.getFrameRect();
    areaAfter = polygon.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(circleScale)
  {
    soprachev::Circle circle({5, 5}, 2);
    const double areaBefore = circle.getArea();

    const int scaleMultiplier = 2;
    circle.scale(scaleMultiplier);
    double areaAfter = circle.getArea();
    BOOST_CHECK_CLOSE(areaBefore * scaleMultiplier * scaleMultiplier, areaAfter, EPSILON);

    circle.scale(1.0 / scaleMultiplier);
    areaAfter = circle.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    BOOST_CHECK_THROW(circle.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(rectangleScale)
  {
    soprachev::Rectangle rectangle({5, 5}, 2, 6);
    const double areaBefore = rectangle.getArea();

    const int scaleMultiplier = 2;
    rectangle.scale(scaleMultiplier);
    double areaAfter = rectangle.getArea();
    BOOST_CHECK_CLOSE(areaBefore * scaleMultiplier * scaleMultiplier, areaAfter, EPSILON);

    rectangle.scale(1.0 / scaleMultiplier);
    areaAfter = rectangle.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    BOOST_CHECK_THROW(rectangle.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(triangleScale)
  {
    soprachev::Triangle triangle(soprachev::point_t{4, 5}, soprachev::point_t{-1, 9}, soprachev::point_t{0, 3});
    const double areaBefore = triangle.getArea();

    const int scaleMultiplier = 2;
    triangle.scale(scaleMultiplier);
    double areaAfter = triangle.getArea();
    BOOST_CHECK_CLOSE(areaBefore * scaleMultiplier * scaleMultiplier, areaAfter, EPSILON);

    triangle.scale(1.0 / scaleMultiplier);
    areaAfter = triangle.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    BOOST_CHECK_THROW(triangle.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(polygonScale)
  {
    soprachev::point_t pentagonPoints[5] = {
      soprachev::point_t{0, 0},
      soprachev::point_t{4, 0},
      soprachev::point_t{5.23, 3.8},
      soprachev::point_t{2, 6.16},
      soprachev::point_t{-1.23, 3.8}
    };
    soprachev::Polygon polygon(pentagonPoints, 5);
    const double areaBefore = polygon.getArea();

    const int scaleMultiplier = 2;
    polygon.scale(scaleMultiplier);
    double areaAfter = polygon.getArea();
    BOOST_CHECK_CLOSE(areaBefore * scaleMultiplier * scaleMultiplier, areaAfter, EPSILON);

    polygon.scale(1.0 / scaleMultiplier);
    areaAfter = polygon.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    BOOST_CHECK_THROW(polygon.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(polygonMoveAndCopy)
  {

    soprachev::point_t pentagonPoints[5] = {
      soprachev::point_t{0, 0},
      soprachev::point_t{4, 0},
      soprachev::point_t{5.23, 3.8},
      soprachev::point_t{2, 6.16},
      soprachev::point_t{-1.23, 3.8}
    };

    soprachev::Polygon polygon = soprachev::Polygon(pentagonPoints, 5);
    const double areaBefore = polygon.getArea();

    soprachev::Polygon polygon2 = soprachev::Polygon(polygon); //copy constructor
    BOOST_CHECK_CLOSE(areaBefore, polygon2.getArea(), EPSILON);

    soprachev::Polygon polygon3 = soprachev::Polygon(std::move(polygon2)); //move constructor
    BOOST_CHECK_CLOSE(areaBefore, polygon3.getArea(), EPSILON);

    polygon3 = polygon; //copy-assigned
    BOOST_CHECK_CLOSE(areaBefore, polygon3.getArea(), EPSILON);

    polygon3 = std::move(polygon); //move-assigned
    BOOST_CHECK_CLOSE(areaBefore, polygon3.getArea(), EPSILON);

  }
  BOOST_AUTO_TEST_CASE(throwingExceptionsCircle)
  {
    BOOST_CHECK_THROW(soprachev::Circle({5, 5}, -10), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(throwingExceptionsRectangle)
  {
    BOOST_CHECK_THROW(soprachev::Rectangle(soprachev::point_t{5, 5}, 2, -6), std::invalid_argument);
    BOOST_CHECK_THROW(soprachev::Rectangle(soprachev::point_t{5, 5}, -2, 6), std::invalid_argument);
    BOOST_CHECK_THROW(soprachev::Rectangle(soprachev::point_t{5, 5}, -2, -6), std::invalid_argument);

    BOOST_CHECK_THROW(soprachev::Rectangle(soprachev::point_t{5, 5}, soprachev::point_t{6, 4}), std::invalid_argument);
    BOOST_CHECK_THROW(soprachev::Rectangle(soprachev::point_t{5, 5}, soprachev::point_t{4, 6}), std::invalid_argument);
    BOOST_CHECK_THROW(soprachev::Rectangle(soprachev::point_t{5, 5}, soprachev::point_t{4, 4}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(throwingExceptionsTriangle)
  {
    BOOST_CHECK_THROW(soprachev::Triangle(soprachev::point_t{5, 5}, soprachev::point_t{5, 5}, soprachev::point_t{5, 5}),
                      std::invalid_argument);

    BOOST_CHECK_THROW(soprachev::Triangle(soprachev::point_t{5, 5}, soprachev::point_t{6, 5}, soprachev::point_t{7, 5}),
                      std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(throwingExceptionsPolygon)
  {
    //похожая на стрклку GPS фигура
    soprachev::point_t nonConvexPoints[4] = {
      soprachev::point_t{0, 0},
      soprachev::point_t{2, 1},
      soprachev::point_t{4, 0},
      soprachev::point_t{2, 4}
    };
    BOOST_CHECK_THROW(soprachev::Polygon(nonConvexPoints, 4), std::invalid_argument);

    //вырожденный треугольник
    soprachev::point_t nonConvexTriangle[3] = {
      soprachev::point_t{0, 0},
      soprachev::point_t{2, 0},
      soprachev::point_t{4, 0}
    };
    BOOST_CHECK_THROW(soprachev::Polygon(nonConvexTriangle, 3), std::invalid_argument);

    soprachev::point_t linePoints[2] = {
      soprachev::point_t{0, 0},
      soprachev::point_t{2, 1}
    };
    BOOST_CHECK_THROW(soprachev::Polygon(linePoints, 2), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
