//
// Created by Andrei Soprachev on 2019-12-26.
//

#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP


#include <cstdlib>
#include <string>

namespace constants
{
  const size_t defaultLineWidth = 40;
  const size_t maxNumberLength = 20;
  const size_t maxWordLength = 20;

  const std::string beforeDashWhiteList[] = {","};

  namespace symbols
  {
    const char hyphen = '-';
    const char plus = '+';
    const char minus = '-';
    const std::string dash = "---";
    const std::string space = " ";
  }


}


#endif //CONSTANTS_HPP
