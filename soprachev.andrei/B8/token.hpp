//
// Created by Andrei Soprachev on 2019-12-26.
//

#ifndef TOKEN_HPP
#define TOKEN_HPP


#include <string>

#include "constants.hpp"

struct token
{

  enum tokenType
  {
    word,
    punct,
    dash,
    number,
    space,
    invalid,
    empty
  } tokenType;

  std::string data;

  bool isWord() { return tokenType == tokenType::word; }
  bool isPunct() { return tokenType == tokenType::punct; }
  bool isDash() { return tokenType == tokenType::dash; }
  bool isPunctOrDash() { return isPunct() || isDash(); }
  bool isNumber() { return tokenType == tokenType::number; }
  bool isSpace() { return tokenType == tokenType::space; }
  bool isInvalid() { return tokenType == tokenType::invalid; }
  bool isEmpty() { return tokenType == tokenType::empty; }

  static token makeWord(std::string data) { return token{token::tokenType::word, data}; }
  static token makePunct(std::string data) { return token{token::tokenType::punct, data}; }
  static token makeDash(std::string data) { return token{token::tokenType::dash, data}; }
  static token makeNumber(std::string data) { return token{token::tokenType::number, data}; }
  static token makeSpace() { return token{token::tokenType::space, constants::symbols::space}; }
  static token makeInvalid(std::string data) { return token{token::tokenType::invalid, data}; }
  static token makeEmpty() { return token{token::tokenType::empty}; }
};


std::istream &operator>>(std::istream &input, token &token_);
std::ostream &operator<<(std::ostream &output, const token &token_);


#endif //TOKEN_HPP
