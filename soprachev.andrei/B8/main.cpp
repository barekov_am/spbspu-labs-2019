//
// Created by Andrei Soprachev on 2019-12-26.
//

#include <cstring>
#include <iostream>
#include <algorithm>

#include "constants.hpp"

void task(std::istream &input, std::ostream &output, size_t line_width = constants::defaultLineWidth);

int main(int argc, char *argv[])
{
  try
  {
    switch (argc)
    {
      case 1:
        task(std::cin, std::cout);
        break;

      case 3:
        if (std::strcmp(argv[1], "--line-width") != 0)
        {
          std::cerr << "Incorrect argument\n";
          return 1;
        }

        if (std::stoi(argv[2]) < 25)
        {
          std::cerr << "Incorrect argument\n";
          return 1;
        }

        task(std::cin, std::cout, std::stoi(argv[2]));
        break;

      default:
        std::cerr << "Invalid number of arguments\n";
        return 1;
    }
  }
  catch (std::exception &exception)
  {
    std::cerr << exception.what() << '\n';
    return 1;
  }
  return 0;
}
