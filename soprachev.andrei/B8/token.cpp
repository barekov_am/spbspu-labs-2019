//
// Created by Andrei Soprachev on 2019-12-26.
//

#include <iostream>
#include <array>

#include "constants.hpp"
#include "token.hpp"


std::string getString(char c)
{
  std::string s(1, c);

  return s;
}

token readSpace(std::istream &input)
{
  while (std::isspace(input.peek()))
  {
    input.get();
  }

  return token::makeSpace();
}

token readPunct(std::istream &input)
{
  std::string result = "";
  char c = 0;
  input >> c;

  if (c != constants::symbols::hyphen)
  {
    if (std::ispunct(c))
    {
      return token::makePunct(getString(c));
    }
    else
    {
      return token::makeInvalid(getString(c));
    }
  }
  else
  {
    result += c;
    size_t i = 1;

    while (input.peek() == constants::symbols::hyphen && i < constants::symbols::dash.size())
    {
      i++;
      result += input.get();
    }

    if (i == constants::symbols::dash.size())
    {
      return token::makeDash(result);
    }
    else
    {
      return token::makeInvalid(result);
    }
  }
}

bool isNumber(std::istream &input)
{
  if (isdigit(input.peek()))
  {
    return true;
  }
  else if (input.peek() == constants::symbols::plus || input.peek() == constants::symbols::minus)
  {
    char c = 0;
    input >> c;
    if (isdigit(input.peek()))
    {
      input.putback(c);
      return true;
    }
    else
    {
      input.putback(c);
    }
  }
  return false;
}

token readNumber(std::istream &input)
{
  std::string result;
  char decimalPoint = std::use_facet<std::numpunct<char>>(input.getloc()).decimal_point();
  bool hasPoint = false;

  if (input.peek() == constants::symbols::plus || input.peek() == constants::symbols::minus)
  {
    result += input.get();
  }

  while (std::isdigit(input.peek()) || (!hasPoint && input.peek() == decimalPoint))
  {
    if (input.peek() == decimalPoint)
    {
      hasPoint = true;
    }
    result += input.get();
  }

  if (result.size() > constants::maxNumberLength)
  {
    return token::makeInvalid(result); //1.d
  }

  return token::makeNumber(result);
}

token readWord(std::istream &input)
{
  std::string result;

  bool lastIsHyphen = false;
  if (input.peek() == constants::symbols::hyphen)
  {
    return token::makeInvalid(result); //1.b
  }

  while (std::isalpha(input.peek()) || input.peek() == constants::symbols::hyphen)
  {
    if (input.peek() == constants::symbols::hyphen)
    {
      if (lastIsHyphen)
      {
        return token::makeInvalid(result); //два дефиса подряд не могут (с) Бареков
      }
      else
      {
        lastIsHyphen = true;
      }
    }
    else
    {
      lastIsHyphen = false;
    }
    result += input.get();
  }


  if (result.size() > constants::maxWordLength)
  {
    return token::makeInvalid(result); //1.a
  }

  return token::makeWord(result);
}

std::istream &operator>>(std::istream &input, token &token_)
{

  if (isNumber(input))
  {
    token_ = readNumber(input);
  }
  else if (std::isalpha(input.peek()))
  {
    token_ = readWord(input);
  }
  else if (std::ispunct(input.peek()))
  {
    token_ = readPunct(input);
  }
  else if (std::isspace(input.peek()))
  {
    token_ = readSpace(input);
  }
  else
  {
    token_ = token::makeInvalid(getString(input.get()));
  }

  return input;

}

std::ostream &operator<<(std::ostream &output, const token &token)
{
  std::string tokenTypeNames[] = {"word",
                                  "punct",
                                  "dash",
                                  "number",
                                  "space",
                                  "invalid",
                                  "empty"};

  output << tokenTypeNames[token.tokenType] << " [" << token.data << "]";
  return output;
}
