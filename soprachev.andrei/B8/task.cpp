//
// Created by Andrei Soprachev on 2019-12-26.
//


#include <istream>
#include <algorithm>

#include "constants.hpp"
#include "token.hpp"


bool containsBeforeDashWhiteList(std::string s)
{
  return std::find(
    std::begin(constants::beforeDashWhiteList),
    std::end(constants::beforeDashWhiteList),
    s) != std::end(constants::beforeDashWhiteList);

}

token readToken(std::istream &input, token &token_)
{
  if (input.eof())
  {
    token_ = token::makeEmpty();
    return token_;
  }
  else
  {
    input >> token_;
    if (token_.isInvalid())
    {
      throw std::invalid_argument("Invalid token");
    }
    return token_;
  }
}

void checkCorrectSpace(token &first, token &second, std::istream &input)
{
  if ((first.isWord() && second.isNumber()) || (first.isNumber() && second.isWord()))
  {
    throw std::invalid_argument("Between word and number must be space");
  }

  if (second.isSpace())
  {
    readToken(input, second);
  }
}

void checkCorrectPunctOrder(token &first, token &second)
{
  if (first.isPunctOrDash() && second.isPunct())
  {
    throw std::invalid_argument("Punct cannot be next to each other");
  }

  if (first.isPunctOrDash() && second.isDash() && !containsBeforeDashWhiteList(first.data))
  {
    throw std::invalid_argument("Dash can be after punct only if the this from the white list");
  }
}

void processToken(std::string &str, token &lhs, token &mid, token &rhs, std::ostream &output, size_t line_width)
{

  checkCorrectPunctOrder(lhs, mid);

  std::string added = "";
  size_t fullSize = 0;

  if (lhs.isWord() || lhs.isNumber())
  {
    if (!mid.isPunctOrDash())
    {
      added = (str.size() ? constants::symbols::space : "") + lhs.data; //[ ][word] or [ ][number]
      fullSize = added.size();
    }
    else
    {
      if (mid.isPunct())
      {
        if (rhs.isDash())
        {
          added = (str.size() ? constants::symbols::space : "") + lhs.data;
          fullSize =
            added.size() + mid.data.size() + constants::symbols::space.size() + rhs.data.size(); //[word][wl][dash]
        }
        else
        {
          added = (str.size() ? constants::symbols::space : "") + lhs.data;//[word][dash]
          fullSize = added.size() + mid.data.size();
        }
      }
      else if (mid.isDash())
      {
        added = (str.size() ? constants::symbols::space : "") + lhs.data;//[word][dash]
        fullSize = added.size() + constants::symbols::space.size() + mid.data.size();
      }
    }
  }
  else
  {
    if (lhs.isPunct())
    {
      added = lhs.data;//[punct]
    }
    else if (lhs.isDash())
    {
      added = constants::symbols::space + lhs.data;//[ ][dash]
    }

    fullSize = added.size();

  }

  if (str.size() + fullSize > line_width)
  {
    output << str << "\n";
    str = lhs.data;
  }
  else
  {
    str += added;
  }
}

void task(std::istream &input, std::ostream &output, size_t line_width = constants::defaultLineWidth)
{
  if (input.peek() == EOF)
  {
    return;
  }

  token lhs = token::makeEmpty();
  token mid = token::makeEmpty();
  token rhs = token::makeEmpty();
  std::string str = "";

  readToken(input, lhs);
  if (lhs.isPunctOrDash() || lhs.isInvalid())
  {
    throw std::invalid_argument("First token can not be punct");
  }


  readToken(input, mid);
  checkCorrectSpace(lhs, mid, input);


  while (!lhs.isEmpty())
  {
    readToken(input, rhs);
    checkCorrectSpace(mid, rhs, input);

    processToken(str, lhs, mid, rhs, output, line_width);

    lhs = mid;
    mid = rhs;
  }

  if (str.size())
  {
    output << str << "\n";
  }


}


