//
// Created by Andrei Soprachev on 2019-11-27.
//


#include <iostream>
#include <iterator>
#include <numeric>
#include <algorithm>

#include <utility>
#include <cmath>

#include "geometryContainer.hpp"

const size_t tetragonVert = 4;

bool GeometryContainer::isRectangle(const GeometryContainer::shape_ &shape)
{
  if (shape.size() != tetragonVert)
  {
    return false;
  }

  const std::pair<int, int> side1 = std::make_pair((shape[0].x - shape[1].x), (shape[0].y - shape[1].y));
  const std::pair<int, int> side2 = std::make_pair((shape[0].x - shape[3].x), (shape[0].y - shape[3].y));
  const std::pair<int, int> side3 = std::make_pair((shape[1].x - shape[2].x), (shape[1].y - shape[2].y));

  const bool adjacentSidesPerpendicular = (side1.first * side2.first + side1.second * side2.second) == 0;
  const bool oppositeSidesEqual = std::hypot(side2.first, side2.second) == std::hypot(side3.first, side3.second);

  return adjacentSidesPerpendicular && oppositeSidesEqual;
}

bool GeometryContainer::isSquare(const GeometryContainer::shape_ &shape)
{
  if (!isRectangle(shape))
  {
    return false;
  }

  const std::pair<int, int> diag1 = std::make_pair((shape[0].x - shape[2].x), (shape[0].y - shape[2].y));
  const std::pair<int, int> diag2 = std::make_pair((shape[1].x - shape[3].x), (shape[1].y - shape[3].y));

  return (diag1.first * diag2.first + diag1.second * diag2.second) == 0;

}

void GeometryContainer::printShapes(std::ostream &output)
{
  output << "Shapes:\n";
  for (auto &shape : shapes)
  {
    output << shape.size() << " ";
    std::copy(shape.begin(), shape.end(), std::ostream_iterator<Point>(output, " "));
    output << "\n";
  }
}

void GeometryContainer::add(GeometryContainer::shape_ &shape)
{
  shapes.push_back(shape);
}

std::shared_ptr<GeometryContainer::shape_> GeometryContainer::createPointVector()
{
  std::shared_ptr<shape_> pointVector = std::make_shared<shape_>(shapes.size());

  std::transform(shapes.begin(), shapes.end(), pointVector->begin(),
                 [](auto &element) { return element.front(); });

  return pointVector;
}

void GeometryContainer::sort()
{
  shapes.sort([](const GeometryContainer::shape_ &lhs, const GeometryContainer::shape_ &rhs)
            {
              if ((lhs.size() == tetragonVert) && (rhs.size() == tetragonVert))
              {
                if (GeometryContainer::isSquare(lhs))
                {
                  return true;
                }

                if (GeometryContainer::isSquare(rhs))
                {
                  return false;
                }

                return !GeometryContainer::isRectangle(rhs);
              }

              return lhs.size() < rhs.size();
            }
  );

}

size_t GeometryContainer::countSquares()
{
  return std::count_if(shapes.begin(), shapes.end(),
                       [](const auto &element) { return GeometryContainer::isSquare(element); });
}

size_t GeometryContainer::countRectangles()
{
  return std::count_if(shapes.begin(), shapes.end(),
                       [](const auto &element) { return GeometryContainer::isRectangle(element); });
}

size_t GeometryContainer::countNGons(size_t n)
{
  return std::count_if(shapes.begin(), shapes.end(),
                       [=](const auto &element) { return element.size() == n; });
}

void GeometryContainer::deleteNGons(size_t n)
{
  shapes.erase(std::remove_if(shapes.begin(), shapes.end(),
                              [n](const auto &element) { return element.size() == n; }), shapes.end());
}

size_t GeometryContainer::countVertices()
{
  return std::accumulate(shapes.begin(), shapes.end(), 0,
                         [](size_t lhs, const auto &rhs) { return lhs + rhs.size(); });
}
