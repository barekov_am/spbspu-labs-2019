//
// Created by Andrei Soprachev on 2019-11-27.
//

#include "point.hpp"

std::istream &operator>>(std::istream &input, Point &point)
{
  char delim = 0;

  input >> std::ws >> delim
        >> std::ws >> point.x
        >> std::ws >> delim
        >> std::ws >> point.y
        >> std::ws >> delim;

  if (!input)
  {
    input.setstate(std::ios_base::failbit);
    return input;
  }

  return input;
}

std::ostream &operator<<(std::ostream &output, const Point &point)
{
  output << '(' << point.x << ';' << point.y << ')';

  return output;
}
