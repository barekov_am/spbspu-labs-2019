//
// Created by Andrei Soprachev on 2019-11-26.
//


#include <iostream>
#include <iterator>
#include <sstream>
#include "geometryContainer.hpp"
#include "point.hpp"

#include <algorithm>

const size_t countVert = 3;
const size_t removeVert = 5;

void task2(std::istream &input, std::ostream &output)
{
  GeometryContainer container;
  std::string inputLine;
  while (getline(input, inputLine))
  {
    while (!inputLine.empty() && ((inputLine.at(0) == ' ') || (inputLine.at(0) == '\t')))
    {
      inputLine.erase(0,1);
    }


    if (inputLine.empty())
    {
      continue;
    }

    size_t countPoints = 0;
    std::stringstream ss(inputLine);
    ss >> countPoints >> std::ws;

    if (countPoints <= 2)
    {
      throw std::ios_base::failure("Wrong shape");
    }

    std::vector<Point> points{std::istream_iterator<Point>(ss), std::istream_iterator<Point>()};
    if (points.size() != countPoints)
    {
      throw std::ios_base::failure("Not enough points");
    }
    container.add(points);
  }

  output << "Vertices: " << container.countVertices() << "\n"
            << "Triangles: " << container.countNGons(countVert) << "\n"
            << "Squares: " << container.countSquares() << "\n"
            << "Rectangles: " << container.countRectangles() << "\n";


  container.deleteNGons(removeVert);

  std::shared_ptr<GeometryContainer::shape_> pointVector = container.createPointVector();

  output << "Points: ";
  std::copy(pointVector->begin(), pointVector->end(), std::ostream_iterator<Point>(output, " "));
  output << "\n";

  container.sort();
  container.printShapes(output);
}
