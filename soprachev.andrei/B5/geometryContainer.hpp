//
// Created by Andrei Soprachev on 2019-11-27.
//

#ifndef GEOMETRYCONTAINER_HPP
#define GEOMETRYCONTAINER_HPP

#include <vector>
#include <list>
#include <memory>
#include "point.hpp"



class GeometryContainer
{
public:
  using shape_ = std::vector<Point>;

  void add(shape_ &shape);
  void printShapes(std::ostream &output);

  size_t countVertices();
  size_t countRectangles();
  size_t countSquares();
  size_t countNGons(size_t n);
  void deleteNGons(size_t n);
  void sort();
  std::shared_ptr<GeometryContainer::shape_> createPointVector();

  static bool isRectangle(const GeometryContainer::shape_ &shape);
  static bool isSquare(const GeometryContainer::shape_ &shape);

private:
  std::list<shape_> shapes;
};


#endif //GEOMETRYCONTAINER_HPP
