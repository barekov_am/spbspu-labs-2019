//
// Created by Andrei Soprachev on 2019-11-26.
//

#include <algorithm>
#include <iostream>
#include <iterator>
#include <unordered_set>


void task1(std::istream &input, std::ostream &output)
{
  std::unordered_set<std::string> words{std::istream_iterator<std::string>(input), std::istream_iterator<std::string>()};

  if (!input.good() && !input.eof())
  {
    throw std::ios_base::failure("Fail when read");
  }

  std::copy(words.begin(), words.end(), std::ostream_iterator<std::string>(output, "\n"));

}
