//
// Created by Andrei Soprachev on 2019-11-27.
//

#ifndef LAB1_POINT_HPP
#define LAB1_POINT_HPP

#include <iostream>

struct Point
{
  int x;
  int y;
};


std::istream &operator>>(std::istream &input, Point &point);
std::ostream &operator<<(std::ostream &output, const Point &point);


#endif //LAB1_POINT_HPP
