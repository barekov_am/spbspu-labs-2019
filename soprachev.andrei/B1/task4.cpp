//
// Created by Andrei Soprachev on 2019-10-22.
//

#include <cstdio>
#include <random>

#include "printCollection.hpp"
#include "sort.hpp"

bool dirIsAscending(const char *direction);

void fillRandom(double *array, size_t size)
{
  std::random_device random{};
  std::default_random_engine engine{random()};
  std::uniform_real_distribution<double> distribution{-1.0, 1.0};
  for (size_t i = 0; i < size; i++)
  {
    array[i] = distribution(engine);
  }
}


void task4(const char *direction, size_t size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size must be positive");
  }

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);

  print(vector);
  bubbleSort<AccessIn>(vector, dirIsAscending(direction));
  print(vector);
}

