//
// Created by Andrei Soprachev on 2019-10-22.
//

#include <vector>

#include "printCollection.hpp"

void task3()
{
  std::vector<int> vector;
  int num = 0;

  while (std::cin >> num && num != 0)
  {
    vector.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Fail while reading data");
  }

  if (vector.empty())
  {
    return;
  }

  if (num != 0)
  {
    throw std::runtime_error("The last number is not zero");
  }

  if (vector.back() == 1)
  {
    for (auto i = vector.begin(); i != vector.end();)
    {
      i = ((*i % 2) == 0) ? vector.erase(i) : ++i;
    }

  } else if (vector.back() == 2)
  {
    for (auto i = vector.begin(); i != vector.end();)
    {
      i = ((*i % 3) == 0) ? (vector.insert(++i, 3, 1) + 3) : ++i;
    }
  }


  print(vector);
}

