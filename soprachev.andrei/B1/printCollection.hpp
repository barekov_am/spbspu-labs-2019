//
// Created by Andrei Soprachev on 2019-10-22.
//

#ifndef PRINTCOLLECTION_HPP
#define PRINTCOLLECTION_HPP

#include <iostream>

template<typename T>
void print(const T &collection)
{

  for (const auto &i: collection)
  {
    std::cout << i << " ";
  }
  std::cout << '\n';
}


#endif //PRINTCOLLECTION_HPP
