//
// Created by Andrei Soprachev on 2019-10-22.
//

#include <cstring>
#include <stdexcept>

bool dirIsAscending(const char *direction)
{
  if (std::strcmp(direction, "ascending") == 0)
  {
    return true;
  }

  if (std::strcmp(direction, "descending") == 0)
  {
    return false;
  }

  throw std::invalid_argument("Invalid direction.");
}
