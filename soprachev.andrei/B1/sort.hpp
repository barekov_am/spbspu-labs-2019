//
// Created by Andrei Soprachev on 2019-10-22.
//

#ifndef SORT_HPP
#define SORT_HPP

#include "accesses.hpp"

template<template<class T> class Access, class T>
void bubbleSort(T &collection, const bool dir)
{
  const auto begin = Access<T>::begin(collection);
  const auto end = Access<T>::end(collection);

  for (auto i = begin; i != end; i++)
  {
    for (auto j = i; j != end; j++)
    {
      typename T::value_type &a = Access<T>::element(collection, i);
      typename T::value_type &b = Access<T>::element(collection, j);

      if (dir ? b < a : a < b)
      {
        std::swap(a, b);
      }
    }
  }
}

#endif //SORT_HPP
