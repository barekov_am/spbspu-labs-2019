//
// Created by Andrei Soprachev on 2019-10-22.
//

#include <vector>
#include <forward_list>

#include "sort.hpp"
#include "printCollection.hpp"


bool dirIsAscending(const char *direction);

void task1(const char *direction)
{
  bool dir = dirIsAscending(direction);

  std::vector<int> vector;
  int num = 0;
  while (std::cin >> num)
  {
    vector.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Fail when reading");
  }

  if (vector.empty())
  {
    return;
  }

  std::vector<int> vectorAt = vector;
  std::forward_list<int> forwardList(vector.begin(), vector.end());

  bubbleSort<AccessIn>(vector, dir);
  bubbleSort<AccessAt>(vectorAt, dir);
  bubbleSort<AccessIter>(forwardList, dir);


  print(vector);
  print(vectorAt);
  print(forwardList);
}

