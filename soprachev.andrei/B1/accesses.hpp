//
// Created by Andrei Soprachev on 2019-10-22.
//

#ifndef ACCESSES_HPP
#define ACCESSES_HPP

#include <iterator>

template<typename T>
struct AccessIn
{

  static size_t begin(const T &)
  {
    return 0;
  }

  static size_t end(const T &collection)
  {
    return collection.size();
  }

  static typename T::reference element(T &collection, size_t i)
  {
    return collection[i];
  }

  static size_t next(size_t i)
  {
    return i + 1;
  }
};

template<typename T>
struct AccessAt
{

  static size_t begin(const T &)
  {
    return 0;
  }

  static size_t end(const T &collection)
  {
    return collection.size();
  }

  static typename T::reference element(T &collection, size_t i)
  {
    return collection.at(i);
  }

  static size_t next(size_t i)
  {
    return i + 1;
  }
};

template<typename T>
struct AccessIter
{

  static typename T::iterator begin(T &collection)
  {
    return collection.begin();
  }

  static typename T::iterator end(T &collection)
  {
    return collection.end();
  }

  static typename T::reference element(T &, typename T::iterator &iterator)
  {
    return *iterator;
  }

  static size_t next(typename T::iterator &iterator)
  {
    return std::next(iterator);
  }
};


#endif //ACCESSES_HPP
