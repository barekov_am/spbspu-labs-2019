//
// Created by Andrei Soprachev on 2019-10-22.
//

#include <fstream>
#include <memory>
#include <vector>
#include <iostream>


void task2(const char *filename)
{
  std::ifstream file(filename);
  if (!file)
  {
    throw std::ios_base::failure("Fail when open file");
  }

  const int BUFFERSIZE = 512;
  size_t count = BUFFERSIZE;
  size_t i = 0;
  std::unique_ptr<char[], decltype(&free)> array(static_cast<char *>(malloc(count)), &free);

  while (file)
  {
    file.read(&array[i], BUFFERSIZE);
    i += file.gcount();
    if (file.gcount() == BUFFERSIZE)
    {
      count += BUFFERSIZE;
      std::unique_ptr<char[], decltype(&free)> temp(static_cast<char *>(realloc(array.get(), count)), &free);
      if (!temp)
      {
        throw std::ios_base::failure("Fail when reallocation");
      }
      array.release();
      std::swap(array, temp);
    }
  }

  file.close();
  if (file.is_open())
  {
    throw std::ios_base::failure("Fail when close file");
  }

  std::vector<char> vector(&array[0], &array[i]);

  for (std::size_t i = 0; i < vector.size(); ++i)
  {
    std::cout << vector.at(i);
  }


}

