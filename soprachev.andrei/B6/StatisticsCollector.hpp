//
// Created by Andrei Soprachev on 2019-12-12.
//

#ifndef STATISTICSCOLLECTOR_HPP
#define STATISTICSCOLLECTOR_HPP


#include <cstddef>

class StatisticsCollector
{
public:
  StatisticsCollector();
  void operator()(int i);

  double mean();
  bool isEmpty();
  bool firstEqLast();

  int max(){ return max_;}
  int min(){ return min_;}
  int first(){ return first_;}
  int last(){ return last_;}
  size_t countPositive(){ return countPositive_;}
  size_t countNegative(){ return countNegative_;}
  size_t countZero(){ return countZero_;}
  long int oddSum(){ return oddSum_;}
  long int evenSum(){ return evenSum_;}

private:
  int max_;
  int min_;
  int first_;
  int last_;
  size_t countPositive_;
  size_t countNegative_;
  size_t countZero_;
  long int oddSum_;
  long int evenSum_;

};


#endif //STATISTICSCOLLECTOR_HPP
