//
// Created by Andrei Soprachev on 2019-12-12.
//

#include <algorithm>
#include <iostream>
#include <iterator>

#include "StatisticsCollector.hpp"


int main()
{
  StatisticsCollector stats = std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), StatisticsCollector());

  if (!std::cin.eof() && std::cin.fail())
  {
    std::cerr << "Fail when read";

    return 1;
  }

  if (stats.isEmpty())
  {
    std::cout << "No Data\n";
  }
  else
  {
    std::cout << "Max: " << stats.max() << "\nMin: " << stats.min()
              << "\nMean: " << stats.mean()
              << "\nPositive: " << stats.countPositive()
              << "\nNegative: " << stats.countNegative()
              << "\nOdd Sum: " << stats.oddSum()
              << "\nEven Sum: " << stats.evenSum()
              << "\nFirst/Last Equal: " << (stats.firstEqLast() ? "yes\n" : "no\n");
  }

  return 0;
}
