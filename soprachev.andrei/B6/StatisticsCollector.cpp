//
// Created by Andrei Soprachev on 2019-12-12.
//

#include <algorithm>

#include "StatisticsCollector.hpp"


StatisticsCollector::StatisticsCollector() :
  max_(0),
  min_(0),
  first_(0),
  last_(0),
  countPositive_(0),
  countNegative_(0),
  countZero_(0),
  oddSum_(0),
  evenSum_(0)
{}


void StatisticsCollector::operator()(int i)
{
  last_ = i;

  if (isEmpty())
  {
    min_ = i;
    max_ = i;
    first_ = i;
  }

  max_ = std::max(max_, i);
  min_ = std::min(min_, i);

  if (i > 0)
  {
    countPositive_++;
  }
  else if (i < 0)
  {
    countNegative_++;
  }
  else
  {
    countZero_++;
  }


  ((i % 2 == 0) ? evenSum_ : oddSum_) += i;


}

double StatisticsCollector::mean()
{
  return (static_cast<double>(oddSum_ + evenSum_)) / (countNegative_ + countZero_ + countPositive_);
}

bool StatisticsCollector::isEmpty()
{
  return (countNegative_ + countZero_ + countPositive_) == 0;
}

bool StatisticsCollector::firstEqLast()
{
  return first_ == last_;
}
