//
// Created by Andrei Soprachev on 2019-11-05.
//

#include <algorithm>
#include <istream>
#include <iostream>

#include "PhoneBook.hpp"


const std::string currentBookMark = "current";

PhoneBook::PhoneBook()
{
  map_[currentBookMark] = list_.end();
}

void PhoneBook::toFirst(const std::string &bookMark)
{
  auto iter = getIter(bookMark);
  iter->second = list_.begin();
}

void PhoneBook::toLast(const std::string &bookMark)
{
  auto iter = getIter(bookMark);
  iter->second = std::prev(list_.end());
}

void PhoneBook::show(const std::string &bookMark)
{
  auto iter = getIter(bookMark);
  if (iter->second == list_.end())
  {
    std::cout << "<EMPTY>\n";
    return;
  }
  std::cout << iter->second->number << " " << iter->second->name << "\n";
}

void PhoneBook::add(const PhoneBook::record_t &record)
{
  list_.push_back(record);
  if (list_.size() == 1)
  {
    map_[currentBookMark] = list_.begin();
  }
}

void PhoneBook::insert(const std::string &bookMark, const record_t &record, bool after)
{
  auto iter = getIter(bookMark);
  insert(iter, record, after);
}

void PhoneBook::next(const std::string &bookMark)
{
  move(bookMark, 1);
}

void PhoneBook::prev(const std::string &bookMark)
{
  move(bookMark, -1);
}

void PhoneBook::move(const std::string &bookMark, int n)
{
  std::advance(getIter(bookMark)->second, n);
}

void PhoneBook::update(const std::string &bookMark, const record_t &record)
{
  *(getIter(bookMark)->second) = record;
}

void PhoneBook::remove(const std::string &bookMark)
{
  auto iter = getIter(bookMark);
  if (iter->second != list_.end())
  {
    auto removeElem = iter->second;
    auto iterMap = map_.begin();
    for (size_t i = 0; i < map_.size(); i++)
    {
      if (iterMap->second == removeElem)
      {
        if ((removeElem == std::prev(list_.end())) && (list_.size() > 1))
        {
          iterMap->second = std::prev(removeElem);
        } else
        {
          iterMap->second = std::next(removeElem);
        }
      }
      ++iterMap;
    }
    list_.erase(removeElem);
  }
}

void PhoneBook::newBookMark(const std::string &prevBookMark, const std::string &newBookMark)
{
  auto iter = getIter(prevBookMark);
  auto result = map_.emplace(newBookMark, iter->second);
  if (!result.second)
  {
    std::cout << "Already exists\n";
  }
}

size_t PhoneBook::getSize()
{
  return list_.size();
}

bool PhoneBook::checkBookMark(const std::string &bookMark)
{
  return map_.find(bookMark) != map_.end();
}

PhoneBook::mapIter PhoneBook::getIter(const std::string &bookMark)
{
  auto iter = map_.find(bookMark);
  if (iter != map_.end())
  {
    return iter;
  }
  throw std::invalid_argument("<INVALID BOOKMARK>");
}

void PhoneBook::insert(PhoneBook::mapIter &iter, const PhoneBook::record_t &record, bool after)
{
  if (list_.empty())
  {
    add(record);
    return;
  }

  if (!after)
  {
    list_.insert(iter->second, record);
  } else
  {
    list_.insert(std::next(iter->second), record);
  }
}


