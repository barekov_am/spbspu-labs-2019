//
// Created by Andrei Soprachev on 2019-11-05.
//

#include <iostream>
#include <stdexcept>

void task1();

void task2();


int main(int argc, char *argv[])
{

  try
  {
    if (argc == 1)
    {
      std::cerr << "Incorrect parameters count\n";
      return 1;
    }

    switch (std::stoi(argv[1]))
    {
      case 1:
        task1();
        break;
      case 2:
        task2();
        break;
      default:
        std::cerr << "Incorrect task number\n";
        return 1;
    }
  }
  catch (const std::exception &e)
  {
    std::cerr << e.what() << '\n';
    return 1;
  }

  return 0;
}
