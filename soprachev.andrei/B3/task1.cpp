//
// Created by Andrei Soprachev on 2019-11-05.
//

#include <iostream>
#include <sstream>

#include "BookCommand.hpp"
#include "PhoneBook.hpp"


std::pair<bool, std::string> parseBookMark(std::istream &input)
{
  std::string bookMark;
  input >> bookMark;

  bool error = false;
  for (auto i : bookMark)
  {
    if (!(std::isalnum(i) || (i == '-')))
    {
      std::cout << "<INVALID BOOKMARK>";
      error = true;
    }
  }
  return std::make_pair(!error, bookMark);
}


std::pair<bool, std::string> parseAndCheckBookMark(std::istream &input, PhoneBook &phoneBook)
{
  auto parser = parseBookMark(input);

  if(parser.first)
  {
    if (!phoneBook.checkBookMark(parser.second))
    {
      std::cout << "<INVALID BOOKMARK>\n";
      parser.first = false;
    }
  }

  return parser;

}

void task1()
{
  PhoneBook phoneBook;
  std::string inputStr;

  while (std::getline(std::cin, inputStr))
  {
    BookCommand bookCommand;
    std::istringstream input(inputStr);
    std::string commandStr;

    input >> commandStr;
    if (commandStr == "add")
    {
      bookCommand.add(phoneBook, input);
    } else if (commandStr == "insert")
    {
      std::string directionStr;
      input >> directionStr;

      if (directionStr != "before" && directionStr != "after")
      {
        std::cout << "<INVALID COMMAND>\n";
        continue;
      }

      auto bookMarkP = parseAndCheckBookMark(input, phoneBook);
      if (!bookMarkP.first) { continue; }


      bookCommand.insert(bookMarkP.second, phoneBook, directionStr == "after", input);
    } else if (commandStr == "delete")
    {
      auto bookMarkP = parseAndCheckBookMark(input, phoneBook);
      if (!bookMarkP.first) { continue; }

      bookCommand.remove(bookMarkP.second, phoneBook);
    } else if (commandStr == "store")
    {

      auto oldBookMarkP = parseAndCheckBookMark(input, phoneBook);
      if (!oldBookMarkP.first) { continue; }

      auto newBookMarkP = parseBookMark(input);
      if (!newBookMarkP.first) { continue; }

      bookCommand.store(oldBookMarkP.second, newBookMarkP.second, phoneBook);
    } else if (commandStr == "move")
    {
      auto bookMarkP = parseAndCheckBookMark(input, phoneBook);
      if (!bookMarkP.first) { continue; }

      bookCommand.move(bookMarkP.second, phoneBook, input);
    } else if (commandStr == "show")
    {
      auto bookMarkP = parseAndCheckBookMark(input, phoneBook);
      if (!bookMarkP.first) { continue; }
      bookCommand.show(bookMarkP.second, phoneBook);
    } else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios::failure("Fail when read");
  }
}
