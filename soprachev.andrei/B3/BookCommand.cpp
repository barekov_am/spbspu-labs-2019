//
// Created by Andrei Soprachev on 2019-11-12.
//

#include <algorithm>
#include <istream>
#include <iostream>
#include <regex>

#include "BookCommand.hpp"


void BookCommand::add(PhoneBook &phoneBook, std::istream &input)
{
  auto parse = parseRecord(input);
  if (parse.first)
  {
    phoneBook.add(parse.second);
  }
}

void BookCommand::show(const std::string &bookmark, PhoneBook &phoneBook)
{
  phoneBook.show(bookmark);
}

void BookCommand::insert(const std::string &bookmark, PhoneBook &phoneBook, bool after, std::istream &input)
{
  auto parse = parseRecord(input);
  if (parse.first)
  {
    phoneBook.insert(bookmark, parse.second, after);
  }
}

void BookCommand::remove(const std::string &bookmark, PhoneBook &phoneBook)
{
  phoneBook.remove(bookmark);
}

void BookCommand::move(const std::string &bookmark, PhoneBook &phoneBook, std::istream &input)
{
  std::string step;
  input >> step >> std::ws;
  if (step == "first")
  {
    phoneBook.toFirst(bookmark);
  } else if (step == "last")
  {
    phoneBook.toLast(bookmark);
  } else
  {
    try
    {
      int n = std::stoi(step);
      phoneBook.move(bookmark, n);
    }
    catch (const std::exception &e)
    {
      std::cout << "<INVALID STEP>\n";
      return;
    }

  }
}

void BookCommand::store(const std::string &bookmark, const std::string &newBookmark, PhoneBook &phoneBook)
{
  phoneBook.newBookMark(bookmark, newBookmark);
}

std::pair<bool, PhoneBook::record_t> BookCommand::parseRecord(std::istream &input)
{
  std::string number = parseNumber(input);
  if (!number.empty())
  {
    std::string name = parseName(input);
    return std::make_pair(!(number.empty() || name.empty()), PhoneBook::record_t{name, number});
  } else
  {
    return std::make_pair(false, PhoneBook::record_t{"", number});
  }
}

std::string BookCommand::parseName(std::istream &input)
{
  std::string name;
  std::getline(input, name);

  if (name.empty())
  {
    return name;
  }

  if ((name.front() != '\"') || (name.back() != '\"'))
  {
    std::cout << "<INVALID COMMAND>\n";
    name.clear();
    return name;
  }

  auto iter = name.begin();
  while (iter != name.end())
  {
    if ((*iter == '\\') && ((*(iter + 1) == '\\') || (*(iter + 1) == '"')))
    {
      iter = name.erase(iter) + 1;
    } else if (*iter == '"')
    {
      iter = name.erase(iter);
    } else
    {
      ++iter;
    }
  }

  return name;
}

std::string BookCommand::parseNumber(std::istream &input)
{
  std::string number;
  input >> number >> std::ws;

  if (!std::regex_match(number, std::regex("[0-9]*")))
  {
    std::cout << "<INVALID COMMAND>\n";
    number.clear();
  }
  return number;
}
