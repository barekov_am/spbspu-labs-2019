//
// Created by Andrei Soprachev on 2019-11-12.
//

#ifndef BOOKCOMMAND_HPP
#define BOOKCOMMAND_HPP


#include "PhoneBook.hpp"

class BookCommand
{
public:
  void add(PhoneBook &phoneBook, std::istream &input);

  void insert(const std::string &bookmark, PhoneBook &phoneBook, bool after, std::istream &input);

  void remove(const std::string &bookmark, PhoneBook &phoneBook);

  void show(const std::string &bookmark, PhoneBook &phoneBook);

  void move(const std::string &bookmark, PhoneBook &phoneBook, std::istream &input);

  void store(const std::string &bookmark, const std::string &newBookmark, PhoneBook &phoneBook);


private:
  std::pair<bool, PhoneBook::record_t> parseRecord(std::istream &input);

  std::string parseName(std::istream &input);

  std::string parseNumber(std::istream &input);
};


#endif //BOOKCOMMAND_HPP
