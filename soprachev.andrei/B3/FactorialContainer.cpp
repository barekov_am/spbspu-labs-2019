//
// Created by Andrei Soprachev on 2019-11-13.
//

#include "FactorialContainer.hpp"

FactorialContainer::Iterator::Iterator(size_t index, unsigned long long int value) : index_(index), value_(value)
{}

FactorialContainer::Iterator::Iterator(size_t index) : index_(index), value_(calculate(index))
{}

unsigned long long int FactorialContainer::Iterator::calculate(size_t index) const
{
  if (index <= 1)
  {
    return 1;
  }
  else
  {
    return index * calculate(index - 1);
  }
}

const unsigned long long int *FactorialContainer::Iterator::operator->() const
{
  return &value_;
}

const unsigned long long int &FactorialContainer::Iterator::operator*() const
{
  return value_;
}

FactorialContainer::Iterator FactorialContainer::begin()
{
  return FactorialContainer::Iterator(1);
}

FactorialContainer::Iterator FactorialContainer::end()
{
  return FactorialContainer::Iterator(11);
}

FactorialContainer::Iterator &FactorialContainer::Iterator::operator++()
{
  if (index_ < 11)
  {
    index_++;
    value_ *= index_;
  }
  return *this;
}

FactorialContainer::Iterator FactorialContainer::Iterator::operator++(int)
{
  Iterator prev = *this;
  ++(*this);
  return prev;
}

FactorialContainer::Iterator &FactorialContainer::Iterator::operator--()
{
  if (index_ > 1)
  {
    value_ /= index_;
    index_--;
  }
  return *this;
}

FactorialContainer::Iterator FactorialContainer::Iterator::operator--(int)
{
  Iterator prev = *this;
  --(*this);
  return prev;
}

bool FactorialContainer::Iterator::operator==(const Iterator &other) const
{
  return (index_ == other.index_);
}

bool FactorialContainer::Iterator::operator!=(const Iterator &other) const
{
  return (!(other == *this));
}
