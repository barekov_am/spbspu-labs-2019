//
// Created by Andrei Soprachev on 2019-11-13.
//
#include <iostream>
#include <algorithm>

#include "FactorialContainer.hpp"

void task2()
{
  FactorialContainer container;

  std::copy(container.begin(), container.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;

  std::reverse_copy(container.begin(), container.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;
}
