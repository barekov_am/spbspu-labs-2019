//
// Created by Andrei Soprachev on 2019-11-05.
//

#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP


#include <list>
#include <map>
#include <string>
#include <stdexcept>

class PhoneBook
{
public:

  struct record_t
  {
    std::string name;
    std::string number;
  };

  PhoneBook();

  void toFirst(const std::string &bookMark);

  void toLast(const std::string &bookMark);

  void show(const std::string &bookMark);

  void add(const record_t &record);

  void insert(const std::string &bookMark, const record_t &record, bool after);

  void next(const std::string &bookMark);

  void prev(const std::string &bookMark);

  void move(const std::string &bookMark, int n);

  void update(const std::string &bookMark, const record_t &record);

  void remove(const std::string &bookMark);

  void newBookMark(const std::string &prevBookMark, const std::string &newBookMark);

  bool checkBookMark(const std::string &bookMark);

  size_t getSize();

private:
  using mapIter = std::map<std::string, std::list<record_t>::iterator>::iterator;
  std::list<record_t> list_;
  std::map<std::string, std::list<record_t>::iterator> map_;

  mapIter getIter(const std::string &bookMark);

  void insert(mapIter &iter, const record_t &record, bool after = true);
};


#endif //PHONEBOOK_HPP
