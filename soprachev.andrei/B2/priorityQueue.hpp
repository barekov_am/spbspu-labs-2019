//
// Created by Andrei Soprachev on 2019-10-29.
//

#ifndef PRIORITYQUEUE_HPP
#define PRIORITYQUEUE_HPP

#include <list>
#include <iostream>

enum ElementPriority
{
  HIGH,
  NORMAL,
  LOW
};

int parsePriority(std::string priority, ElementPriority &elementPriority);


template<typename T>
class QueueWithPriority
{
public:


  void addToQueue(ElementPriority priority, const T &element);

  T getElementFromQueue();

  void accelerate();

  bool empty();

private:
  std::list<T> highPriority_;
  std::list<T> normalPriority_;
  std::list<T> lowPriority_;
};

#endif //PRIORITYQUEUE_HPP
