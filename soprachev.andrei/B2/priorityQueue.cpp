//
// Created by Andrei Soprachev on 2019-10-30.
//

#include "priorityQueue.hpp"

int parsePriority(std::string priority, ElementPriority &elementPriority)
{
  if (priority == "low")
  {
    elementPriority = LOW;
    return 0;
  }

  if (priority == "normal")
  {
    elementPriority = NORMAL;
    return 0;
  }

  if (priority == "high")
  {
    elementPriority = HIGH;
    return 0;
  }

  return 1;
}
