//
// Created by Andrei Soprachev on 2019-10-30.
//


#include <iostream>
#include <list>

void printElem(std::list<int> &list, bool isLast = false)
{
  std::cout << (isLast ? list.back() : list.front()) << " ";
  isLast ? list.pop_back() : list.pop_front();
  if (!list.empty())
  {
    printElem(list, !isLast);
  }
}


void task2()
{
  const int MIN_VAL = 1;
  const int MAX_VAL = 20;
  const size_t MAX_COUNT = 20;

  std::list<int> list;
  int value;
  size_t currentSize = 0;
  while (std::cin >> value)
  {
    currentSize++;
    if ((value > MAX_VAL) || (value < MIN_VAL) || (currentSize > MAX_COUNT))
    {
      throw std::invalid_argument("Error value or number of values");
    }
    list.push_back(value);
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::ios_base::failure("Fail when reading");
  }

  if (list.empty())
  {
    return;
  }


  if (list.empty())
  {
    return;
  }

  printElem(list);
  std::cout << "\n";


}

