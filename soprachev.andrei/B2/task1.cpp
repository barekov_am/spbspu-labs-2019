//
// Created by Andrei Soprachev on 2019-10-29.
//

#include <sstream>
#include "priorityQueueImpl.hpp"

void add(std::stringstream &stringstream, QueueWithPriority<std::string> &queue)
{
  std::string priority;
  stringstream >> priority >> std::ws;
  std::string data;
  std::getline(stringstream, data);

  if (data.empty() || priority.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  ElementPriority prior;
  if (parsePriority(priority, prior) == 0)
  {
    queue.addToQueue(prior, data);
  } else
  {
    std::cout << "<INVALID COMMAND>\n";
  }
}


void task1()
{
  QueueWithPriority<std::string> queue;
  std::string inputLine;

  while (std::getline(std::cin, inputLine))
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Fail when reading file");
    }

    std::stringstream stringstream(inputLine);
    std::string command;

    stringstream >> command;

    if (command == "add")
    {
      add(stringstream, queue);

    } else if (command == "get")
    {
      if (queue.empty())
      {
        std::cout << "<EMPTY>\n";
      } else
      {
        std::cout << queue.getElementFromQueue() << "\n";
      }
    } else if (command == "accelerate")
    {
      queue.accelerate();
    } else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}

