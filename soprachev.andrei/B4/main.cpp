//
// Created by Andrei Soprachev on 2019-11-16.
//

#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>
#include <sstream>


#include "DataStruct.hpp"

int main()
{
  try
  {
    std::vector<DataStruct> dataVector;
    DataStruct data;

    std::string inputLine = "";
    while (getline(std::cin, inputLine))
    {
      std::stringstream ss(inputLine);
      ss >> data;
      dataVector.push_back(data);
    }

    if (std::cin.fail() && !std::cin.eof())
    {
      throw std::ios_base::failure("Fail when reading");
    }

    std::sort(dataVector.begin(), dataVector.end(), [](const DataStruct &lhs, const DataStruct &rhs)
              {
                if (lhs.key1 != rhs.key1)
                {
                  return lhs.key1 < rhs.key1;
                }

                if (lhs.key2 != rhs.key2)
                {
                  return lhs.key2 < rhs.key2;
                }

                return lhs.str.size() < rhs.str.size();
              }
    );


    std::copy(dataVector.begin(), dataVector.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));
  }
  catch (std::invalid_argument &err)
  {
    std::cerr << err.what();
    return 1;
  }
  return 0;
}
