//
// Created by Andrei Soprachev on 2019-11-20.
//

#include <istream>

#include "DataStruct.hpp"

const char separator = ',';
const char keySize = 2;
const std::pair<int, int> keyRange(-5, 5);

std::istream &operator>>(std::istream &input, DataStruct &dataStruct)
{
  int keys[keySize];
  char tempSeparator = 0;

  for (size_t i = 0; i < keySize; i++)
  {
    input >> keys[i];
    input >> tempSeparator;
    if (tempSeparator != ',')
    {
      throw std::invalid_argument("Bad separator\n");
    }
  }

  std::string inputLine = "";
  getline(input, inputLine);
  if (inputLine.empty())
  {
    throw std::invalid_argument("Bad string\n");
  }

  while ((inputLine.at(0) == ' ') || (inputLine.at(0) == '\t'))
  {
    inputLine.erase(0, 1);
  }


  for (int key : keys)
  {
    if (key < keyRange.first || key > keyRange.second)
    {
      throw std::invalid_argument("Out of range.");
    }
  }


  dataStruct = DataStruct{keys[0], keys[1], inputLine};

  return input;
}

std::ostream &operator<<(std::ostream &output, const DataStruct &data)
{
  output << data.key1 << separator << data.key2 << separator << data.str;
  return output;
}
