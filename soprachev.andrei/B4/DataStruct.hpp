//
// Created by Andrei Soprachev on 2019-11-20.
//

#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP

#include <string>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

std::istream &operator>>(std::istream &input, DataStruct &dataStruct);

std::ostream &operator<<(std::ostream &output, const DataStruct &dataStruct);


#endif //DATASTRUCT_HPP
