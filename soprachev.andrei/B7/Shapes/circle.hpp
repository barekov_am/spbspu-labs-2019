//
// Created by Andrei Soprachev on 2019-12-24.
//

#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace shapes
{

  class Circle : public shapes::Shape
  {
  public:
    static constexpr const char *name = "CIRCLE";
    Circle(const soprachev::Vector2 &point);

    void draw(std::ostream &output) const override;
  };

}


#endif //CIRCLE_HPP
