//
// Created by Andrei Soprachev on 2019-12-23.
//

#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>

#include "base-types.hpp"

namespace shapes
{

  class Shape
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;

    Shape(const soprachev::Vector2 &pos);
    virtual ~Shape() = default;

    virtual void draw(std::ostream &output) const = 0;
    bool isMoreLeft(const Shape &rhs) const;
    bool isUpper(const Shape &rhs) const;

  protected:
    soprachev::Vector2 pos_;
  };

  std::istream &operator>>(std::istream &input, Shape::shape_ptr &shape_ptr);
  std::ostream &operator<<(std::ostream &output, const Shape::shape_ptr &shape);

}


#endif //SHAPE_HPP
