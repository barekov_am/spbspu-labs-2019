//
// Created by Andrei Soprachev on 2019-12-23.
//

#include <algorithm>
#include <iostream>
#include <iterator>


#include "shape.hpp"

#include "square.hpp"
#include "circle.hpp"
#include "triangle.hpp"


shapes::Shape::Shape(const soprachev::Vector2 &pos) :
  pos_(pos)
{}

bool shapes::Shape::isMoreLeft(const shapes::Shape &rhs) const
{
  return pos_.x_ < rhs.pos_.x_;
}

bool shapes::Shape::isUpper(const shapes::Shape &rhs) const
{
  return rhs.pos_.y_ < pos_.y_;
}

std::istream &shapes::operator>>(std::istream &input, Shape::shape_ptr &shape_ptr)
{
  input >> std::ws;
  std::string shapeName = "";

  while (std::isalpha(input.peek()))
  {
    shapeName += input.get();
  }

  if (input.fail())
  {
    return input;
  }

  input >> std::noskipws;
  soprachev::Vector2 pos;
  input >> pos;


  if (input.fail())
  {
    input.clear(~std::ios_base::eofbit);
    return input;
  }

  if (shapeName == Square::name)
  {
    shape_ptr = std::make_shared<shapes::Square>(shapes::Square(pos));
  }
  else if (shapeName == Circle::name)
  {
    shape_ptr = std::make_shared<shapes::Circle>(shapes::Circle(pos));
  }
  else if (shapeName == Triangle::name)
  {
    shape_ptr = std::make_shared<shapes::Triangle>(shapes::Triangle(pos));
  }
  else
  {
    throw std::invalid_argument("Incorrect shape name");
  }

  return input;
}

std::ostream &shapes::operator<<(std::ostream &output, const Shape::shape_ptr &shape)
{
  shape->draw(output);
  return output;
}
