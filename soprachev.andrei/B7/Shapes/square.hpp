//
// Created by Andrei Soprachev on 2019-12-24.
//

#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "shape.hpp"

namespace shapes
{

  class Square : public shapes::Shape
  {
  public:
    static constexpr const char *name = "SQUARE";
    Square(const soprachev::Vector2 &point);

    void draw(std::ostream &output) const override;
  };

}


#endif //SQUARE_HPP
