//
// Created by Andrei Soprachev on 2019-12-24.
//

#include "circle.hpp"

shapes::Circle::Circle(const soprachev::Vector2 &point) :
  Shape(point)
{}

void shapes::Circle::draw(std::ostream &output) const
{
  output << name << " " << pos_;
}
