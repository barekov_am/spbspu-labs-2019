//
// Created by Andrei Soprachev on 2019-12-24.
//

#include "square.hpp"

shapes::Square::Square(const soprachev::Vector2 &point) :
  Shape(point)
{}

void shapes::Square::draw(std::ostream &output) const
{
  output << name << " " << pos_;
}
