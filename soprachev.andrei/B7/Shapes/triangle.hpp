//
// Created by Andrei Soprachev on 2019-12-23.
//

#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace shapes
{

  class Triangle : public shapes::Shape
  {
  public:
    static constexpr const char *name = "TRIANGLE";
    Triangle(const soprachev::Vector2 &point);

    void draw(std::ostream &output) const override;
  };

}

#endif //TRIANGLE_HPP
