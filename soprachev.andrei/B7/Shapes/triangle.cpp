//
// Created by Andrei Soprachev on 2019-12-23.
//

#include "triangle.hpp"

shapes::Triangle::Triangle(const soprachev::Vector2 &point) :
  Shape(point)
{}

void shapes::Triangle::draw(std::ostream &output) const
{
  output << name << " " << pos_;
}


