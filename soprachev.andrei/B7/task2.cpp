//
// Created by Andrei Soprachev on 2019-12-16.
//

#include <algorithm>
#include <iostream>
#include <iterator>
#include <list>

#include "Shapes/shape.hpp"

void printList(const std::string &name, const std::list<shapes::Shape::shape_ptr> &list, std::ostream &output)
{
  std::cout << name << "\n";
  std::copy(list.begin(), list.end(), std::ostream_iterator<shapes::Shape::shape_ptr>(output, "\n"));
}

void task2(std::istream &input, std::ostream &output)
{

  std::list<shapes::Shape::shape_ptr> list{std::istream_iterator<shapes::Shape::shape_ptr>(input),
                                           std::istream_iterator<shapes::Shape::shape_ptr>()};

  if (!input.eof() && input.fail())
  {
    throw std::invalid_argument("Fail while reading data");
  }

  printList("Original:", list, output);

  list.sort([](const auto &lhs, const auto &rhs) { return lhs->isMoreLeft(*rhs); });
  printList("Left-Right:", list, output);

  list.sort([](const auto &lhs, const auto &rhs) { return rhs->isMoreLeft(*lhs); });
  printList("Right-Left:", list, output);

  list.sort([](const auto &lhs, const auto &rhs) { return lhs->isUpper(*rhs); });
  printList("Top-Bottom:", list, output);

  list.sort([](const auto &lhs, const auto &rhs) { return rhs->isUpper(*lhs); });
  printList("Bottom-Top:", list, output);

}
