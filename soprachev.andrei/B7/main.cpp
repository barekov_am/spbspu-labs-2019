//
// Created by Andrei Soprachev on 2019-12-16.
//


#include <iostream>

void task1(std::istream &input, std::ostream &output);
void task2(std::istream &input, std::ostream &output);

int main(int argc, char *argv[])
{
  if (argc != 2)
  {
    std::cerr << "Incorrect parameters count\n";
    return 1;
  }
  try
  {
    switch (std::stoi(argv[1]))
    {
      case 1:
        task1(std::cin, std::cout);
        break;
      case 2:
        task2(std::cin, std::cout);
        break;
      default:
        std::cerr << "Incorrect task num\n";
        return 1;

    }
  }
  catch (std::exception &exception)
  {
    std::cerr << exception.what() << '\n';
    return 1;
  }
  return 0;
}

