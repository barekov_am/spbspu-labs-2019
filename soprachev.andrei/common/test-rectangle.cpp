//
// Created by Andrei Soprachev on 2019-06-04.
//

#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "rectangle.hpp"

const double EPSILON = 1e-10;

BOOST_AUTO_TEST_SUITE(rectangleTestSuite)

  BOOST_AUTO_TEST_CASE(rectangleConstantAfterMove)
  {
    soprachev::Rectangle rectangle({5, 5}, 4, 6);
    const soprachev::rectangle_t frameBefore = rectangle.getFrameRect();
    const double areaBefore = rectangle.getArea();

    rectangle.move({0, 0});
    soprachev::rectangle_t frameAfter = rectangle.getFrameRect();
    double areaAfter = rectangle.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    rectangle.move(5, 5);
    frameAfter = rectangle.getFrameRect();
    areaAfter = rectangle.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(rectangleRotate)
  {
    soprachev::Rectangle rectangle({5, 5}, 2, 6);
    const soprachev::rectangle_t frameBefore = rectangle.getFrameRect();
    const double areaBefore = rectangle.getArea();

    rectangle.rotate(90);

    soprachev::rectangle_t frameAfter = rectangle.getFrameRect();
    double areaAfter = rectangle.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    rectangle.rotate(270);
    frameAfter = rectangle.getFrameRect();
    areaAfter = rectangle.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(rectangleScale)
  {
    soprachev::Rectangle rectangle({5, 5}, 2, 6);
    const double areaBefore = rectangle.getArea();

    const int scaleMultiplier = 2;
    rectangle.scale(scaleMultiplier);
    double areaAfter = rectangle.getArea();
    BOOST_CHECK_CLOSE(areaBefore * scaleMultiplier * scaleMultiplier, areaAfter, EPSILON);

    rectangle.scale(1.0 / scaleMultiplier);
    areaAfter = rectangle.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    BOOST_CHECK_THROW(rectangle.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(throwingExceptionsRectangle)
  {
    BOOST_CHECK_THROW(soprachev::Rectangle(soprachev::point_t{5, 5}, 2, -6), std::invalid_argument);
    BOOST_CHECK_THROW(soprachev::Rectangle(soprachev::point_t{5, 5}, -2, 6), std::invalid_argument);
    BOOST_CHECK_THROW(soprachev::Rectangle(soprachev::point_t{5, 5}, -2, -6), std::invalid_argument);

    BOOST_CHECK_THROW(soprachev::Rectangle(soprachev::point_t{5, 5}, soprachev::point_t{6, 4}), std::invalid_argument);
    BOOST_CHECK_THROW(soprachev::Rectangle(soprachev::point_t{5, 5}, soprachev::point_t{4, 6}), std::invalid_argument);
    BOOST_CHECK_THROW(soprachev::Rectangle(soprachev::point_t{5, 5}, soprachev::point_t{4, 4}), std::invalid_argument);
  }



BOOST_AUTO_TEST_SUITE_END()





