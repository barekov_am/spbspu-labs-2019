//
// Created by Andrei Soprachev on 2019-06-04.
//

#ifndef SEQUENCES_COMPOSITE_SHAPE_HPP
#define SEQUENCES_COMPOSITE_SHAPE_HPP

#include <memory>

#include "shape.hpp"

namespace soprachev
{
  class CompositeShape : public Shape
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    using shape_array = std::unique_ptr<shape_ptr[]>;

    CompositeShape();
    CompositeShape(soprachev::point_t pos);
    CompositeShape(const CompositeShape &source);
    CompositeShape(CompositeShape &&source);

    ~CompositeShape() = default;

    CompositeShape &operator=(const CompositeShape &rhs);
    CompositeShape &operator=(CompositeShape &&rhs);

    ///координаты фигуры локальные
    shape_ptr operator[](size_t size) const;
    bool operator==(const CompositeShape &rhs) const;
    bool operator!=(const CompositeShape &rhs) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    rectangle_t getRelativeFrameRect(Transform relativeTransform) const override;
    std::string getName() const override;

    ///добавит фигуру переведя её координаты из глобальных в локальные
    void addAsGlobal(shape_ptr shape);
    ///добавит фигуру как есть
    void addAsLocal(shape_ptr shape);
    ///вернёт координаты фигуры обратно к глобальным
    void remove(size_t index);
    size_t shapesCount() const;
    shape_array getShapes() const;

    void move(double dx, double dy) override;
    void move(const point_t &pos) override;
    void rotate(double dAngle) override;
    void setRotation(double angle) override;
    void setScale(double scale) override;
    void scale(double scale) override;


  protected:
    void setRelativeTransform(const Transform &transform) override;

  private:
    size_t count_;
    shape_array arrayOfShapes_;
    Transform rt_;
  };
}

#endif //SEQUENCES_COMPOSITE_SHAPE_HPP
