//
// Created by Andrei Soprachev on 2019-06-04.
//

#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "circle.hpp"

const double EPSILON = 1e-10;

BOOST_AUTO_TEST_SUITE(circleTestSuite)

  BOOST_AUTO_TEST_CASE(circleConstantAfterMove)
  {
    soprachev::Circle circle({5, 5}, 4);
    const soprachev::rectangle_t frameBefore = circle.getFrameRect();
    const double areaBefore = circle.getArea();

    circle.move({0, 0});
    soprachev::rectangle_t frameAfter = circle.getFrameRect();
    double areaAfter = circle.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    circle.move(5, 5);
    frameAfter = circle.getFrameRect();
    areaAfter = circle.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(circleRotate)
  {
    soprachev::Circle circle({5, 5}, 4);
    const soprachev::rectangle_t frameBefore = circle.getFrameRect();
    const double areaBefore = circle.getArea();

    circle.rotate(90);

    soprachev::rectangle_t frameAfter = circle.getFrameRect();
    double areaAfter = circle.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    circle.rotate(-160);
    frameAfter = circle.getFrameRect();
    areaAfter = circle.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(circleScale)
  {
    soprachev::Circle circle({5, 5}, 2);
    const double areaBefore = circle.getArea();

    const int scaleMultiplier = 2;
    circle.scale(scaleMultiplier);
    double areaAfter = circle.getArea();
    BOOST_CHECK_CLOSE(areaBefore * scaleMultiplier * scaleMultiplier, areaAfter, EPSILON);

    circle.scale(1.0 / scaleMultiplier);
    areaAfter = circle.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    BOOST_CHECK_THROW(circle.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(throwingExceptionsCircle)
  {
    BOOST_CHECK_THROW(soprachev::Circle({5, 5}, -10), std::invalid_argument);
  }


BOOST_AUTO_TEST_SUITE_END()
