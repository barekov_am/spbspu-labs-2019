//
// Created by Andrei Soprachev on 2019-06-04.
//

#include "composite-shape.hpp"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <utility>

soprachev::CompositeShape::CompositeShape() :
  Shape(Vector2(0, 0)),
  count_(0)
{}

soprachev::CompositeShape::CompositeShape(soprachev::point_t pos) :
  Shape(pos),
  count_(0),
  rt_(pos)
{}

soprachev::CompositeShape::CompositeShape(const CompositeShape &source) :
  Shape(source.transform_),
  count_(source.count_),
  arrayOfShapes_(std::make_unique<shape_ptr[]>(source.count_)),
  rt_(source.rt_)
{
  for (size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i] = source.arrayOfShapes_[i];
  }
}

soprachev::CompositeShape::CompositeShape(CompositeShape &&source) :
  Shape(source.transform_),
  count_(source.count_),
  arrayOfShapes_(std::move(source.arrayOfShapes_)),
  rt_(source.rt_)
{}

soprachev::CompositeShape &soprachev::CompositeShape::operator=(const CompositeShape &rhs)
{
  if (this != &rhs)
  {
    transform_ = rhs.transform_;
    count_ = rhs.count_;
    rt_ = rhs.rt_;
    shape_array temp = std::make_unique<shape_ptr[]>(rhs.count_);
    for (size_t i = 0; i < count_; i++)
    {
      temp[i] = rhs.arrayOfShapes_[i];
    }
    arrayOfShapes_.swap(temp);
  }

  return *this;
}

soprachev::CompositeShape &soprachev::CompositeShape::operator=(CompositeShape &&rhs)
{
  if (this != &rhs)
  {
    transform_ = std::move(rhs.transform_);
    count_ = rhs.count_;
    arrayOfShapes_ = std::move(rhs.arrayOfShapes_);
    rt_ = std::move(rhs.rt_);
  }

  return *this;
}

std::shared_ptr<soprachev::Shape> soprachev::CompositeShape::operator[](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }

  return arrayOfShapes_[index];
}

bool soprachev::CompositeShape::operator==(const CompositeShape &rhs) const
{
  if (count_ != rhs.count_)
  {
    return false;
  }

  for (size_t i = 0; i < count_; i++)
  {
    if (arrayOfShapes_[i] != rhs.arrayOfShapes_[i])
    {
      return false;
    }
  }

  return true;
}

bool soprachev::CompositeShape::operator!=(const CompositeShape &rhs) const
{
  return !(*this == rhs);
}


void soprachev::CompositeShape::move(const soprachev::point_t &pos)
{
  Shape::move(pos);
  setRelativeTransform(Transform(pos, rt_.rot_, rt_.scale_));
}

void soprachev::CompositeShape::move(double dx, double dy)
{
  move((transform_.pos_ + Vector2(dx, dy)).getPoint());
}

void soprachev::CompositeShape::rotate(const double dAngle)
{
  setRotation(transform_.rot_ + dAngle);
}

void soprachev::CompositeShape::setRotation(const double angle)
{
  Shape::setRotation(angle);
  setRelativeTransform(Transform(rt_.pos_, angle, rt_.scale_));
}

void soprachev::CompositeShape::scale(double scale)
{
  if (scale == 0)
  {
    throw std::invalid_argument("scale can not be 0");
  }
  setScale(transform_.scale_ * scale);
}

void soprachev::CompositeShape::setScale(double scale)
{
  Shape::setScale(scale);
  setRelativeTransform(Transform(rt_.pos_, rt_.rot_, scale));
}

double soprachev::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    area += arrayOfShapes_[i]->getArea();
  }
  return area * transform_.scale_ * transform_.scale_;
}

soprachev::rectangle_t soprachev::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    return soprachev::rectangle_t
      {
        transform_.pos_.getPoint(),
        0,
        0
      };
  }

  soprachev::rectangle_t rectangle = arrayOfShapes_[0]->getRelativeFrameRect(rt_);

  Vector2 leftDown = Vector2(rectangle.pos) - Vector2(rectangle.width, rectangle.height) / 2;
  Vector2 rightUp = Vector2(rectangle.pos) + Vector2(rectangle.width, rectangle.height) / 2;

  for (size_t i = 1; i < count_; i++)
  {
    rectangle = arrayOfShapes_[i]->getRelativeFrameRect(rt_);

    leftDown.x_ = std::min(leftDown.x_, rectangle.pos.x - rectangle.width / 2);
    leftDown.y_ = std::min(leftDown.y_, rectangle.pos.y - rectangle.height / 2);

    rightUp.x_ = std::max(rightUp.x_, rectangle.pos.x + rectangle.width / 2);
    rightUp.y_ = std::max(rightUp.y_, rectangle.pos.y + rectangle.height / 2);
  }

  return soprachev::rectangle_t
    {
      ((rightUp + leftDown) / 2).getPoint(),
      (rightUp.x_ - leftDown.x_),
      (rightUp.y_ - leftDown.y_)
    };
}

soprachev::rectangle_t soprachev::CompositeShape::getRelativeFrameRect(soprachev::Transform) const
{
  soprachev::rectangle_t rectangle = getFrameRect();
  return rectangle;
}

std::string soprachev::CompositeShape::getName() const
{
  return "CompositeShape";
}

void soprachev::CompositeShape::addAsLocal(soprachev::CompositeShape::shape_ptr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Null ptr");
  }

  shape_array temp = std::make_unique<shape_ptr[]>(count_ + 1);
  for (size_t i = 0; i < count_; i++)
  {
    temp[i] = arrayOfShapes_[i];
  }
  temp[count_] = shape;
  count_++;
  arrayOfShapes_.swap(temp);
}

void soprachev::CompositeShape::addAsGlobal(std::shared_ptr<Shape> shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Null ptr");
  }

  if (rt_.scale_ == 0)
  {
    throw std::logic_error("You cant add shape to zeroSize composite");
  }

  Transform transform = shape->getTransform();
  //на случай если в композит шейп добавляем композит шейп, вложенному необходимо знать о своём положение в
  //глобальной системе координат
  shape->setRelativeTransform(
    Transform(rt_.pos_ + transform.pos_ * rt_.scale_, transform.rot_ - rt_.rot_, rt_.scale_ * transform.scale_));

  //перевожу координаты в локальные относительно композит шейпа
  shape->move(((transform.pos_ - rt_.pos_) * rt_.scale_).getPoint());
  shape->setRotation(transform.rot_ - rt_.rot_);
  shape->setScale(transform.scale_ / rt_.scale_);

  addAsLocal(shape);
}

void soprachev::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }

  //при удаление фигуры из композит шейпа координаты возвращаются к глобальным
  arrayOfShapes_[index]->move((rt_.pos_ + arrayOfShapes_[index]->getTransform().pos_ * rt_.scale_).getPoint());
  arrayOfShapes_[index]->setRotation(rt_.rot_ + arrayOfShapes_[index]->getTransform().rot_);
  arrayOfShapes_[index]->setScale(rt_.scale_ * arrayOfShapes_[index]->getTransform().scale_);

  for (size_t i = index; i < count_ - 1; i++)
  {
    arrayOfShapes_[i] = arrayOfShapes_[i + 1];
  }
  count_--;
}

size_t soprachev::CompositeShape::shapesCount() const
{
  return count_;
}

std::unique_ptr<std::shared_ptr<soprachev::Shape>[]> soprachev::CompositeShape::getShapes() const
{
  shape_array temp = std::make_unique<shape_ptr[]>(count_);
  for (size_t i = 0; i < count_; i++)
  {
    temp[i] = arrayOfShapes_[i];
  }

  return temp;
}

void soprachev::CompositeShape::setRelativeTransform(const soprachev::Transform &transform)
{
  rt_ = transform;
  for (size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i]->setRelativeTransform(Transform(
      rt_.pos_ + arrayOfShapes_[i]->getTransform().pos_ * arrayOfShapes_[i]->getTransform().scale_,
      arrayOfShapes_[i]->getTransform().rot_ - rt_.rot_,
      rt_.scale_ * arrayOfShapes_[i]->getTransform().scale_));
  }
}




