//
// Created by Andrei Soprachev on 2019-04-01.
//

#include "triangle.hpp"

#include <algorithm> //need for std::min({})
#include <cmath>

static soprachev::point_t centerOfMassByPoints(const soprachev::point_t &point0, const soprachev::point_t &point1,
  const soprachev::point_t &point2)
{
  return soprachev::point_t
    {
      (point0.x + point1.x + point2.x) / 3,
      (point0.y + point1.y + point2.y) / 3,
    };
}

//треугольник по трём вершиам
soprachev::Triangle::Triangle(const point_t &globalVertex0, const point_t &globalVertex1, const point_t &globalVertex2) :
  Shape(centerOfMassByPoints(globalVertex0, globalVertex1, globalVertex2)),
  vertex0_(Vector2(globalVertex0) - transform_.pos_),
  vertex1_(Vector2(globalVertex1) - transform_.pos_),
  vertex2_(Vector2(globalVertex2) - transform_.pos_)
{
  if (getArea() <= 0)
  {
    throw std::invalid_argument("Triangle must be convex");
  }
}

//площадь треугольника через определитель матрицы
double soprachev::Triangle::getArea() const
{
  double a = (vertex0_.x_ - vertex2_.x_) * (vertex1_.y_ - vertex2_.y_);
  double b = (vertex1_.x_ - vertex2_.x_) * (vertex0_.y_ - vertex2_.y_);
  double det = a - b;
  return std::abs(det / 2) * transform_.scale_ * transform_.scale_;
}

soprachev::rectangle_t soprachev::Triangle::getFrameRect() const
{
  Vector2 v0 = Vector2::translate(vertex0_, transform_.rot_, transform_.scale_);
  Vector2 v1 =  Vector2::translate(vertex1_, transform_.rot_, transform_.scale_);
  Vector2 v2 = Vector2::translate(vertex2_, transform_.rot_, transform_.scale_);

  double left = std::min({v0.x_, v1.x_, v2.x_});
  double right = std::max({v0.x_, v1.x_, v2.x_});
  double down = std::min({v0.y_, v1.y_, v2.y_});
  double up = std::max({v0.y_, v1.y_, v2.y_});

  return rectangle_t
    {
      point_t
        {
          transform_.pos_.x_ + left + ((right - left) / 2),
          transform_.pos_.y_ + down + ((up - down) / 2)
        },
      (right - left),
      (up - down)
    };
}

soprachev::rectangle_t soprachev::Triangle::getRelativeFrameRect(soprachev::Transform relativeTransform) const
{
  Vector2 v0 = Vector2::translate(vertex0_, transform_.rot_ - relativeTransform.rot_, transform_.scale_ * relativeTransform.scale_);
  Vector2 v1 = Vector2::translate(vertex1_, transform_.rot_ - relativeTransform.rot_, transform_.scale_ * relativeTransform.scale_);
  Vector2 v2 = Vector2::translate(vertex2_, transform_.rot_ - relativeTransform.rot_, transform_.scale_ * relativeTransform.scale_);

  double left = std::min({v0.x_, v1.x_, v2.x_});
  double right = std::max({v0.x_, v1.x_, v2.x_});
  double down = std::min({v0.y_, v1.y_, v2.y_});
  double up = std::max({v0.y_, v1.y_, v2.y_});

  Vector2 newPos = Vector2(transform_.pos_).rotate(relativeTransform.rot_) + relativeTransform.pos_;
  return rectangle_t
    {
      point_t
        {
          newPos.x_ + left + ((right - left) / 2),
          newPos.y_ + down + ((up - down) / 2)
        },
      (right - left),
      (up - down)
    };
}

std::string soprachev::Triangle::getName() const
{
  return "Triangle";
}


