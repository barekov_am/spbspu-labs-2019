//
// Created by Andrei Soprachev on 2019-06-06.
//

#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

using shape_ptr = std::shared_ptr<soprachev::Shape>;
using shape_array = std::unique_ptr<shape_ptr[]>;

namespace soprachev
{
  Matrix part(const shape_array &shapes, size_t size);
  Matrix part(const CompositeShape &composite);
}

#endif //PARTITION_HPP
