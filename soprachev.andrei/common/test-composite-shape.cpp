//
// Created by Andrei Soprachev on 2019-06-04.
//

#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

const double EPSILON = 1e-10;

const soprachev::point_t pentagonPoints[5] = {
  soprachev::point_t{0, 0},
  soprachev::point_t{4, 0},
  soprachev::point_t{5.23, 3.8},
  soprachev::point_t{2, 6.16},
  soprachev::point_t{-1.23, 3.8}
};

BOOST_AUTO_TEST_SUITE(compositeShapeTestSuite)

  static soprachev::CompositeShape createCompositeShapeHierarchy()
  {
    soprachev::CompositeShape root = soprachev::CompositeShape(soprachev::point_t{1, 1});
    soprachev::CompositeShape compositeShape1 = soprachev::CompositeShape(soprachev::point_t{2, 2});
    soprachev::CompositeShape compositeShape2 = soprachev::CompositeShape(soprachev::point_t{-2, 2});
    soprachev::CompositeShape compositeShape3 = soprachev::CompositeShape(soprachev::point_t{10, 10});

    soprachev::Circle circle = soprachev::Circle(soprachev::point_t{6, 3}, 2);
    soprachev::Rectangle rectangle = soprachev::Rectangle(soprachev::point_t{4, 4}, 2, 2);
    soprachev::Triangle triangle = soprachev::Triangle({5, 5}, {3, 6}, {7, 8});
    soprachev::Polygon polygon = soprachev::Polygon(pentagonPoints, 5);

    compositeShape3.addAsGlobal(std::make_shared<soprachev::Polygon>(polygon));
    compositeShape3.addAsGlobal(std::make_shared<soprachev::Triangle>(triangle));

    compositeShape2.addAsGlobal(std::make_shared<soprachev::CompositeShape>(compositeShape3));
    compositeShape2.addAsGlobal(std::make_shared<soprachev::Rectangle>(rectangle));

    compositeShape1.addAsGlobal(std::make_shared<soprachev::Circle>(circle));

    root.addAsGlobal(std::make_shared<soprachev::CompositeShape>(compositeShape2));
    root.addAsGlobal(std::make_shared<soprachev::CompositeShape>(compositeShape1));

    return root;
  }

  BOOST_AUTO_TEST_CASE(compositeShapeConstantAfterMove)
  {
    soprachev::CompositeShape compositeShape = createCompositeShapeHierarchy();

    const soprachev::rectangle_t frameBefore = compositeShape.getFrameRect();
    const double areaBefore = compositeShape.getArea();

    compositeShape.move({2, 2});
    soprachev::rectangle_t frameAfter = compositeShape.getFrameRect();
    double areaAfter = compositeShape.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    compositeShape.move(5, 5);
    frameAfter = compositeShape.getFrameRect();
    areaAfter = compositeShape.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

  }

  BOOST_AUTO_TEST_CASE(compositeShapeScale)
  {
    soprachev::CompositeShape compositeShape = createCompositeShapeHierarchy();

    const double areaBefore = compositeShape.getArea();

    const int scaleMultiplier = 2;
    compositeShape.scale(scaleMultiplier);
    double areaAfter = compositeShape.getArea();
    BOOST_CHECK_CLOSE(areaBefore * scaleMultiplier * scaleMultiplier, areaAfter, EPSILON);

    compositeShape.scale(1.0 / scaleMultiplier);
    areaAfter = compositeShape.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    BOOST_CHECK_THROW(compositeShape.scale(0), std::invalid_argument);

  }

  BOOST_AUTO_TEST_CASE(compositeShapeRotateCircle)
  {
    soprachev::CompositeShape compositeShape = soprachev::CompositeShape(soprachev::point_t{1, 1});
    soprachev::Circle circle1 = soprachev::Circle(soprachev::point_t{6, 3}, 2);
    soprachev::Circle circle2 = soprachev::Circle(soprachev::point_t{2, 2}, 2);

    compositeShape.addAsGlobal(std::make_shared<soprachev::Circle>(circle1));
    compositeShape.addAsGlobal(std::make_shared<soprachev::Circle>(circle2));

    const soprachev::rectangle_t frameBefore = compositeShape.getFrameRect();
    const double areaBefore = compositeShape.getArea();

    compositeShape.rotate(90);

    soprachev::rectangle_t frameAfter = compositeShape.getFrameRect();
    double areaAfter = compositeShape.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    compositeShape.rotate(270);
    frameAfter = compositeShape.getFrameRect();
    areaAfter = compositeShape.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

  }

  BOOST_AUTO_TEST_CASE(compositeShapeRotateRectangle)
  {
    soprachev::CompositeShape compositeShape = soprachev::CompositeShape(soprachev::point_t{1, 1});
    soprachev::Triangle triangle1 = soprachev::Triangle({5, 5}, {3, 6}, {7, 8});
    soprachev::Triangle triangle2 = soprachev::Triangle({2, 5}, {0, 6}, {4, 8});

    compositeShape.addAsGlobal(std::make_shared<soprachev::Triangle>(triangle1));
    compositeShape.addAsGlobal(std::make_shared<soprachev::Triangle>(triangle2));

    const soprachev::rectangle_t frameBefore = compositeShape.getFrameRect();
    const double areaBefore = compositeShape.getArea();

    compositeShape.rotate(90);

    soprachev::rectangle_t frameAfter = compositeShape.getFrameRect();
    double areaAfter = compositeShape.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    compositeShape.rotate(270);
    frameAfter = compositeShape.getFrameRect();
    areaAfter = compositeShape.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

  }

  BOOST_AUTO_TEST_CASE(compositeShapeRotateTriangle)
  {
    soprachev::CompositeShape compositeShape = soprachev::CompositeShape(soprachev::point_t{1, 1});
    soprachev::Rectangle rectangle1 = soprachev::Rectangle(soprachev::point_t{4, 4}, 2, 2);
    soprachev::Rectangle rectangle2 = soprachev::Rectangle(soprachev::point_t{-2, -5}, 2, 2);

    compositeShape.addAsGlobal(std::make_shared<soprachev::Rectangle>(rectangle1));
    compositeShape.addAsGlobal(std::make_shared<soprachev::Rectangle>(rectangle2));

    const soprachev::rectangle_t frameBefore = compositeShape.getFrameRect();
    const double areaBefore = compositeShape.getArea();

    compositeShape.rotate(90);

    soprachev::rectangle_t frameAfter = compositeShape.getFrameRect();
    double areaAfter = compositeShape.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    compositeShape.rotate(270);
    frameAfter = compositeShape.getFrameRect();
    areaAfter = compositeShape.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

  }

  BOOST_AUTO_TEST_CASE(compositeShapeRotatePolygon)
  {
    soprachev::point_t pentagonPoints2[5] = {
      soprachev::point_t{-2, 1},
      soprachev::point_t{2, 1},
      soprachev::point_t{3.23, 4.8},
      soprachev::point_t{0, 7.16},
      soprachev::point_t{-3.23, 4.8}
    };

    soprachev::CompositeShape compositeShape = soprachev::CompositeShape(soprachev::point_t{1, 1});
    soprachev::Polygon polygon1 = soprachev::Polygon(pentagonPoints, 5);
    soprachev::Polygon polygon2 = soprachev::Polygon(pentagonPoints2, 5);

    compositeShape.addAsGlobal(std::make_shared<soprachev::Polygon>(polygon1));
    compositeShape.addAsGlobal(std::make_shared<soprachev::Polygon>(polygon2));

    const soprachev::rectangle_t frameBefore = compositeShape.getFrameRect();
    const double areaBefore = compositeShape.getArea();

    compositeShape.rotate(90);

    soprachev::rectangle_t frameAfter = compositeShape.getFrameRect();
    double areaAfter = compositeShape.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    compositeShape.rotate(270);
    frameAfter = compositeShape.getFrameRect();
    areaAfter = compositeShape.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

  }

  BOOST_AUTO_TEST_CASE(compositeShapeRotate)
  {
    soprachev::CompositeShape compositeShape = createCompositeShapeHierarchy();
    const double areaBefore = compositeShape.getArea();

    compositeShape.rotate(90);
    double areaAfter = compositeShape.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    compositeShape.rotate(50);
    areaAfter = compositeShape.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    compositeShape.rotate(500.43);
    areaAfter = compositeShape.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    compositeShape.rotate(-51);
    areaAfter = compositeShape.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    compositeShape.rotate(-578);
    areaAfter = compositeShape.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    compositeShape.rotate(0);
    areaAfter = compositeShape.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

  }

  BOOST_AUTO_TEST_CASE(throwingExceptions)
  {
    soprachev::CompositeShape compositeShape = createCompositeShapeHierarchy();
    compositeShape.setScale(0);

    BOOST_CHECK_THROW(
      compositeShape.addAsGlobal(std::make_shared<soprachev::Circle>(soprachev::Circle(soprachev::point_t{6, 3}, 2))),
      std::logic_error);

    BOOST_CHECK_THROW(compositeShape.addAsGlobal(nullptr), std::invalid_argument);
    BOOST_CHECK_THROW(compositeShape.addAsLocal(nullptr), std::invalid_argument);
    BOOST_CHECK_THROW(compositeShape[2], std::out_of_range);
    BOOST_CHECK_THROW(compositeShape.remove(2), std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(compositeShapeMoveAndCopy)
  {

    soprachev::CompositeShape compositeShape = createCompositeShapeHierarchy();
    const double areaBefore = compositeShape.getArea();

    soprachev::CompositeShape compositeShape2 = soprachev::CompositeShape(compositeShape); //copy constructor
    BOOST_CHECK_CLOSE(areaBefore, compositeShape2.getArea(), EPSILON);

    soprachev::CompositeShape compositeShape3 = soprachev::CompositeShape(std::move(compositeShape2)); //move constructor
    BOOST_CHECK_CLOSE(areaBefore, compositeShape3.getArea(), EPSILON);

    compositeShape3 = compositeShape; //copy-assigned
    BOOST_CHECK_CLOSE(areaBefore, compositeShape3.getArea(), EPSILON);

    compositeShape3 = std::move(compositeShape); //move-assigned
    BOOST_CHECK_CLOSE(areaBefore, compositeShape3.getArea(), EPSILON);

  }

  BOOST_AUTO_TEST_CASE(compositeShapeEquality)
  {
    soprachev::CompositeShape compositeShape = createCompositeShapeHierarchy();

    soprachev::CompositeShape compositeShape1 = compositeShape;
    BOOST_CHECK(compositeShape == compositeShape1);

    compositeShape.addAsGlobal(std::make_shared<soprachev::Circle>(soprachev::Circle(soprachev::point_t{6, 3}, 2)));
    BOOST_CHECK(compositeShape != compositeShape1);

  }

BOOST_AUTO_TEST_SUITE_END()
