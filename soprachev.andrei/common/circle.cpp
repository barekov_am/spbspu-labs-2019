//
// Created by Andrei Soprachev on 2019-03-31.
//
#include "circle.hpp"

#include <cmath>

soprachev::Circle::Circle(const point_t &pos, double radius) :
  Shape(pos),
  radius_(radius)
{
  if (radius <= 0)
  {
    throw std::invalid_argument("Radius must be positive");
  }
}

double soprachev::Circle::getArea() const
{
  return M_PI * radius_ * radius_ * transform_.scale_ * transform_.scale_;
}

soprachev::rectangle_t soprachev::Circle::getFrameRect() const
{
  return rectangle_t
    {
      transform_.pos_.getPoint(),
      std::abs(radius_ * 2 * transform_.scale_) ,
      std::abs(radius_ * 2 * transform_.scale_)
    };
}

soprachev::rectangle_t soprachev::Circle::getRelativeFrameRect(soprachev::Transform relativeTransform) const
{
  return rectangle_t
    {
      (Vector2::translate(transform_.pos_, relativeTransform.rot_, relativeTransform.scale_) + relativeTransform.pos_).getPoint(),
      std::abs(radius_ * 2 * relativeTransform.scale_) ,
      std::abs(radius_ * 2 * relativeTransform.scale_)
    };
}

std::string soprachev::Circle::getName() const
{
  return "Circle";
}


