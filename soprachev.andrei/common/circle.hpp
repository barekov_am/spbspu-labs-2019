//
// Created by Andrei Soprachev on 2019-03-31.
//

#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace soprachev
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t &pos, double radius);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    rectangle_t getRelativeFrameRect(Transform relativeTransform) const override;
    std::string getName() const override;

  private:
    double radius_;
  };
}

#endif //CIRCLE_HPP
