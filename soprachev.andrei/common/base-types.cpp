//
// Created by Andrei Soprachev on 2019-06-04.
//

#include "base-types.hpp"
#include "manipulators.hpp"

#include <iostream>
#include <cmath>

//Vector2

soprachev::Vector2::Vector2() :
  x_(0), y_(0)
{}

soprachev::Vector2::Vector2(double x, double y) :
  x_(x), y_(y)
{}

soprachev::Vector2::Vector2(const point_t &point) :
  x_(point.x), y_(point.y)
{}

soprachev::Vector2 soprachev::Vector2::operator+(const Vector2 &rhs) const
{
  return Vector2(x_ + rhs.x_, y_ + rhs.y_);
}

soprachev::Vector2 soprachev::Vector2::operator-(const Vector2 &rhs) const
{
  return Vector2(x_ - rhs.x_, y_ - rhs.y_);
}

soprachev::Vector2 soprachev::Vector2::operator*(const double &rhs) const
{
  return Vector2{x_ * rhs, y_ * rhs};
}

soprachev::Vector2 soprachev::Vector2::operator/(const double &rhs) const
{
  if (rhs == 0)
  {
    throw std::invalid_argument("Division by zero");
  }
  return Vector2{x_ / rhs, y_ / rhs};
}

soprachev::Vector2 &soprachev::Vector2::operator-()
{
  x_ = -x_;
  y_ = -y_;
  return *this;
}

soprachev::Vector2 &soprachev::Vector2::operator+=(const Vector2 &rhs)
{
  x_ += rhs.x_;
  y_ += rhs.y_;
  return *this;
}

soprachev::Vector2 &soprachev::Vector2::operator-=(const Vector2 &rhs)
{
  x_ -= rhs.x_;
  y_ -= rhs.y_;
  return *this;
}

soprachev::Vector2 &soprachev::Vector2::operator*=(const double &rhs)
{
  x_ *= rhs;
  y_ *= rhs;
  return *this;
}

soprachev::Vector2 &soprachev::Vector2::operator/=(const double &rhs)
{
  if (rhs == 0)
  {
    throw std::invalid_argument("Division by zero");
  }
  x_ /= rhs;
  y_ /= rhs;
  return *this;
}

soprachev::Vector2 soprachev::Vector2::rotate(double angle)
{
  angle = angle * M_PI / 180;
  double oldX = x_;
  x_ = x_ * cos(angle) - y_ * sin(angle);
  y_ = oldX * sin(angle) + y_ * cos(angle);
  return *this;
}

std::istream &soprachev::operator>>(std::istream &input, soprachev::Vector2 &vector2)
{
  char openBr = 0;
  char closeBr = 0;
  char sep = 0;

  input >> manipulators::blank >> openBr
        >> manipulators::blank >> vector2.x_
        >> manipulators::blank >> sep
        >> manipulators::blank >> vector2.y_
        >> manipulators::blank >> closeBr;

  if (!input || openBr != openBracket || sep != vectorSeparator || closeBr != closeBracket)
  {
    input.setstate(std::ios_base::failbit);
  }
  return input;
}

std::ostream &soprachev::operator<<(std::ostream &output, const soprachev::Vector2 &vector2)
{
  output << openBracket << vector2.x_ << vectorSeparator << vector2.y_ << closeBracket;
  return output;
}

soprachev::Vector2 soprachev::Vector2::translate(soprachev::Vector2 vector2, double rot, double scale)
{
  return vector2.rotate(rot) * scale;
}

soprachev::point_t soprachev::Vector2::getPoint() const
{
  return point_t{x_, y_};
}


//Transform

soprachev::Transform::Transform(const Vector2 &pos, double angle, double scale) :
  pos_(pos),
  rot_(angle),
  scale_(scale)
{}

