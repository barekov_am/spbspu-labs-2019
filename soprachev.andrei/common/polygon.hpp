//
// Created by Andrei Soprachev on 2019-04-30.
//

#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "shape.hpp"

namespace soprachev
{
  class Polygon : public Shape
  {
  public:
    Polygon(const point_t *points, std::size_t size);
    Polygon(const Polygon &source);
    Polygon(Polygon &&source);

    ~Polygon();

    Polygon &operator=(const Polygon &rhs);
    Polygon &operator=(Polygon &&rhs);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    rectangle_t getRelativeFrameRect(Transform relativeTransform) const override;
    std::string getName() const override;

  private:
    //in local space
    Vector2 *vertices_;
    std::size_t verticesCount_;

    static Vector2 centerOfMass(const point_t *points, std::size_t size);
    template <typename T>
    static Vector2 *createVectorArray(const T *vertices, std::size_t size, Vector2 center);
    static bool isConvex(const soprachev::point_t *points, std::size_t size);
  };
}

#endif //POLYGON_HPP
