//
// Created by Andrei Soprachev on 2019-06-04.
//

#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "base-types.hpp"

const double EPSILON = 1e-10;

BOOST_AUTO_TEST_SUITE(vector2TestSuite)

  BOOST_AUTO_TEST_CASE(addition)
  {
    const double x1 = 1, y1 = 2;
    const double x2 = 3, y2 = 4;

    soprachev::Vector2 a = soprachev::Vector2(x1, y1);
    soprachev::Vector2 b = soprachev::Vector2(x2, y2);

    soprachev::Vector2 result = a + b;

    BOOST_CHECK_CLOSE(result.x_, x1 + x2, EPSILON);
    BOOST_CHECK_CLOSE(result.y_, y1 + y2, EPSILON);

    a += b;

    BOOST_CHECK_CLOSE(a.x_, x1 + x2, EPSILON);
    BOOST_CHECK_CLOSE(a.y_, y1 + y2, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(subtraction)
  {
    const double x1 = 1, y1 = 2;
    const double x2 = 3, y2 = 4;

    soprachev::Vector2 a = soprachev::Vector2(x1, y1);
    soprachev::Vector2 b = soprachev::Vector2(x2, y2);

    soprachev::Vector2 result = a - b;

    BOOST_CHECK_CLOSE(result.x_, x1 - x2, EPSILON);
    BOOST_CHECK_CLOSE(result.y_, y1 - y2, EPSILON);

    a -= b;

    BOOST_CHECK_CLOSE(a.x_, x1 - x2, EPSILON);
    BOOST_CHECK_CLOSE(a.y_, y1 - y2, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(multiplication)
  {
    const double x = 1, y = 2;
    const double k = 3;

    soprachev::Vector2 a = soprachev::Vector2(x, y);

    soprachev::Vector2 result = a * k;

    BOOST_CHECK_CLOSE(result.x_, x * k, EPSILON);
    BOOST_CHECK_CLOSE(result.y_, y * k, EPSILON);

    a *= k;

    BOOST_CHECK_CLOSE(a.x_, x * k, EPSILON);
    BOOST_CHECK_CLOSE(a.y_, y * k, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(division)
  {
    const double x = 1, y = 2;
    const double k = 3;

    soprachev::Vector2 a = soprachev::Vector2(x, y);

    soprachev::Vector2 result = a / k;

    BOOST_CHECK_CLOSE(result.x_, x / k, EPSILON);
    BOOST_CHECK_CLOSE(result.y_, y / k, EPSILON);

    a /= k;

    BOOST_CHECK_CLOSE(a.x_, x / k, EPSILON);
    BOOST_CHECK_CLOSE(a.y_, y / k, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(rotation)
  {

    soprachev::Vector2 vectorAfter = soprachev::Vector2(1, 1);
    soprachev::Vector2 vectorBefore = soprachev::Vector2(1, 1);

    vectorBefore.rotate(40);
    vectorBefore.rotate(50);
    vectorBefore.rotate(70);
    vectorBefore.rotate(20);

    //после поворота на 180 мы получаем противоположный вектор
    BOOST_CHECK_CLOSE(vectorBefore.x_, -vectorAfter.x_, EPSILON);
    BOOST_CHECK_CLOSE(vectorBefore.y_, -vectorAfter.y_, EPSILON);

    vectorBefore.rotate(180);

    BOOST_CHECK_CLOSE(vectorBefore.x_, vectorAfter.x_, EPSILON);
    BOOST_CHECK_CLOSE(vectorBefore.y_, vectorAfter.y_, EPSILON);

    //пять поворотов на 360 градусов
    vectorBefore.rotate(360 * 5);

    BOOST_CHECK_CLOSE(vectorBefore.x_, vectorAfter.x_, EPSILON);
    BOOST_CHECK_CLOSE(vectorBefore.y_, vectorAfter.y_, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(throwingExceptions)
  {
    BOOST_CHECK_THROW(soprachev::Vector2(1, 1) / 0, std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
