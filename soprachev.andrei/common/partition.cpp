//
// Created by Andrei Soprachev on 2019-06-06.
//

#include "partition.hpp"

#include <cmath>

//фигуры не пересекуются если расстояние между центрами больше полосуммы сторон
static bool intersect(soprachev::rectangle_t frameRect1, soprachev::rectangle_t frameRect2)
{
  const double distanceBetweenCentersX = std::abs(frameRect1.pos.x - frameRect2.pos.x);
  const double distanceBetweenCentersY = std::abs(frameRect1.pos.y - frameRect2.pos.y);

  const double lengthX = (frameRect1.width + frameRect2.width) / 2;
  const double lengthY = (frameRect1.height + frameRect2.height) / 2;

  const bool firstCondition = (distanceBetweenCentersX < lengthX);
  const bool secondCondition = (distanceBetweenCentersY < lengthY);

  return firstCondition && secondCondition;

}

soprachev::Matrix soprachev::part(const shape_array& shapes, size_t size)
{
  Matrix matrix;

  for (size_t i = 0; i < size; i++)
  {
    size_t rightLine = 0;
    size_t rightColumn = 0;
    for (size_t j = 0; j < matrix.getLines(); j++)
    {
      for (size_t k = 0; k < matrix.getColumns(); k++)
      {
        if (matrix[j][k] == nullptr)
        {
          rightLine = j;
          rightColumn = k;
          break;
        }

        if (intersect(shapes[i]->getFrameRect(), matrix[j][k]->getFrameRect()))
        {
          rightLine = j + 1;
          rightColumn = 0;
          break;
        }
        else
        {
          rightLine = j;
          rightColumn = k + 1;
        }
      }

      if (rightLine == j) //если дошли до края матрицы
      {
        break;
      }
    }

    matrix.add(shapes[i], rightLine, rightColumn);
  }

  return matrix;
}

soprachev::Matrix soprachev::part(const CompositeShape& composite)
{
  return part(composite.getShapes(), composite.shapesCount());
}
