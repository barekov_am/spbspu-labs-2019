//
// Created by Andrei Soprachev on 2019-06-06.
//

#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "partition.hpp"
#include "matrix.hpp"

const soprachev::point_t pentagonPoints[5] = {
  soprachev::point_t{0, 0},
  soprachev::point_t{4, 0},
  soprachev::point_t{5.23, 3.8},
  soprachev::point_t{2, 6.16},
  soprachev::point_t{-1.23, 3.8}
};

BOOST_AUTO_TEST_SUITE(partitionMetodsTestsSuite)

  static soprachev::CompositeShape createCompositeShapeHierarchy()
  {
    soprachev::CompositeShape root = soprachev::CompositeShape(soprachev::point_t{1, 1});
    soprachev::CompositeShape compositeShape1 = soprachev::CompositeShape(soprachev::point_t{2, 2});
    soprachev::CompositeShape compositeShape2 = soprachev::CompositeShape(soprachev::point_t{-2, 2});
    soprachev::CompositeShape compositeShape3 = soprachev::CompositeShape(soprachev::point_t{10, 10});

    soprachev::Circle circle = soprachev::Circle(soprachev::point_t{6, 3}, 2);
    soprachev::Rectangle rectangle = soprachev::Rectangle(soprachev::point_t{4, 4}, 2, 2);
    soprachev::Triangle triangle = soprachev::Triangle({5, 5}, {3, 6}, {7, 8});
    soprachev::Polygon polygon = soprachev::Polygon(pentagonPoints, 5);

    compositeShape3.addAsGlobal(std::make_shared<soprachev::Polygon>(polygon));
    compositeShape3.addAsGlobal(std::make_shared<soprachev::Triangle>(triangle));

    compositeShape2.addAsGlobal(std::make_shared<soprachev::CompositeShape>(compositeShape3));
    compositeShape2.addAsGlobal(std::make_shared<soprachev::Rectangle>(rectangle));

    compositeShape1.addAsGlobal(std::make_shared<soprachev::Circle>(circle));

    root.addAsGlobal(std::make_shared<soprachev::CompositeShape>(compositeShape2));
    root.addAsGlobal(std::make_shared<soprachev::CompositeShape>(compositeShape1));

    return root;
  }

  BOOST_AUTO_TEST_CASE(partitionTest)
  {
    soprachev::CompositeShape compositeShape = createCompositeShapeHierarchy();
    soprachev::Matrix matrix = soprachev::part(compositeShape);

    BOOST_CHECK_EQUAL(matrix.getLines(), 2);
    BOOST_CHECK_EQUAL(matrix.getColumns(), 1);
  }

BOOST_AUTO_TEST_SUITE_END()
