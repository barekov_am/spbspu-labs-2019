//
// Created by Andrei Soprachev on 2019-06-04.
//

#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "triangle.hpp"

const double EPSILON = 1e-10;

BOOST_AUTO_TEST_SUITE(triangleTestSuite)

  BOOST_AUTO_TEST_CASE(triangleConstantAfterMove)
  {
    soprachev::Triangle triangle({5, 5}, {3, 6}, {7, 8});
    const soprachev::rectangle_t frameBefore = triangle.getFrameRect();
    const double areaBefore = triangle.getArea();

    triangle.move({5, 5});
    soprachev::rectangle_t frameAfter = triangle.getFrameRect();
    double areaAfter = triangle.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    triangle.move(5, 5);
    frameAfter = triangle.getFrameRect();
    areaAfter = triangle.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(triangleRotate)
  {
    soprachev::Triangle triangle({5, 5}, {3, 6}, {7, 8});
    const soprachev::rectangle_t frameBefore = triangle.getFrameRect();
    const double areaBefore = triangle.getArea();

    triangle.rotate(90);

    soprachev::rectangle_t frameAfter = triangle.getFrameRect();
    double areaAfter = triangle.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    triangle.rotate(270);
    frameAfter = triangle.getFrameRect();
    areaAfter = triangle.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(triangleScale)
  {
    soprachev::Triangle triangle(soprachev::point_t{4, 5}, soprachev::point_t{-1, 9}, soprachev::point_t{0, 3});
    const double areaBefore = triangle.getArea();

    const int scaleMultiplier = 2;
    triangle.scale(scaleMultiplier);
    double areaAfter = triangle.getArea();
    BOOST_CHECK_CLOSE(areaBefore * scaleMultiplier * scaleMultiplier, areaAfter, EPSILON);

    triangle.scale(1.0 / scaleMultiplier);
    areaAfter = triangle.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    BOOST_CHECK_THROW(triangle.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(throwingExceptionsTriangle)
  {
    BOOST_CHECK_THROW(soprachev::Triangle(soprachev::point_t{5, 5}, soprachev::point_t{5, 5}, soprachev::point_t{5, 5}),
      std::invalid_argument);

    BOOST_CHECK_THROW(soprachev::Triangle(soprachev::point_t{5, 5}, soprachev::point_t{6, 5}, soprachev::point_t{7, 5}),
      std::invalid_argument);
  }


BOOST_AUTO_TEST_SUITE_END()
