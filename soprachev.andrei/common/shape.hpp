//
// Created by Andrei Soprachev on 2019-03-19.
//

#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>

#include "base-types.hpp"

namespace soprachev
{
  class Shape
  {
  public:
    Shape(const point_t &pos, double rot = 0, double scale = 1);
    virtual ~Shape() = default;
    virtual std::string getName() const = 0;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual rectangle_t getRelativeFrameRect(Transform relativeTransform) const = 0;
    virtual void move(double dx, double dy);
    virtual void move(const point_t &pos);
    virtual void rotate(const double dAngle);
    virtual void setRotation(const double angle);
    virtual void scale(double scale);
    virtual void setScale(double scale);
    virtual void setRelativeTransform(const Transform &transform);
    std::ostream &print(std::ostream &out) const;

    Transform getTransform() const;
  protected:
    Shape(const Vector2 &pos, double rot = 0, double scale = 1);
    Shape(const Transform &transform);

    Transform transform_;
  };
}

#endif //SHAPE_HPP
