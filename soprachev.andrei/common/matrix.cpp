//
// Created by Andrei Soprachev on 2019-06-06.
//

#include "matrix.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

soprachev::Matrix::Matrix():
  lines_(0),
  columns_(0)
{}

soprachev::Matrix::Matrix(const Matrix &rhs):
  lines_(rhs.lines_),
  columns_(rhs.columns_),
  shapes_(std::make_unique<shape_ptr[]>(rhs.lines_ * rhs.columns_))
{
  for (size_t i = 0; i < (lines_  * columns_); i++)
  {
    shapes_[i] = rhs.shapes_[i];
  }
}

soprachev::Matrix::Matrix(soprachev::Matrix &&rhs):
  lines_(rhs.lines_),
  columns_(rhs.columns_),
  shapes_(std::move(rhs.shapes_))
{
  rhs.lines_ = 0;
  rhs.columns_ = 0;
}

soprachev::Matrix &soprachev::Matrix::operator=(const Matrix &rhs)
{
  if (this != &rhs)
  {
    lines_ = rhs.lines_;
    columns_ = rhs.columns_;
    shape_array temp(std::make_unique<shape_ptr[]>(lines_ * columns_));
    for (size_t i = 0; i < (lines_ * columns_); i++)
    {
      temp[i] = rhs.shapes_[i];
    }

    shapes_.swap(temp);
  }

  return *this;
}

soprachev::Matrix &soprachev::Matrix::operator=(Matrix &&rhs)
{
  if (this != &rhs)
  {
    lines_ = rhs.lines_;
    columns_ = rhs.columns_;
    shapes_ = std::move(rhs.shapes_);
    rhs.lines_ = 0;
    rhs.columns_ = 0;
  }

  return *this;
}

soprachev::Matrix::shape_array soprachev::Matrix::operator[](size_t index) const
{
  if (index >= lines_)
  {
    throw std::out_of_range("Index out of range");
  }

  shape_array temp(std::make_unique<shape_ptr []>(columns_));
  for (size_t i = 0; i < getLineSize(index); i++)
  {
    temp[i] = shapes_[index * columns_ + i];
  }

  return temp;
}

bool soprachev::Matrix::operator==(const Matrix &shapesMatrix) const
{
  if ((lines_ != shapesMatrix.lines_) || (columns_ != shapesMatrix.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    if (shapes_[i] != shapesMatrix.shapes_[i])
    {
      return false;
    }
  }

  return true;
}

bool soprachev::Matrix::operator!=(const Matrix &shapesMatrix) const
{
  return !(*this == shapesMatrix);
}

void soprachev::Matrix::add(shape_ptr shape, size_t line, size_t column)
{
  size_t newLine = (line == lines_) ? (lines_ + 1) : (lines_);
  size_t newColumn = (column == columns_) ? (columns_ + 1) : (columns_);

  shape_array temp(std::make_unique<shape_ptr []>(newColumn * newLine));

  for (size_t i = 0; i < lines_; i++)
  {
    for (size_t j = 0; j < columns_; j++)
    {
      temp[i * newColumn + j] = shapes_[i * columns_ + j];
    }
  }
  temp[newColumn * line + column] = shape;
  shapes_ = std::move(temp);
  lines_ = newLine;
  columns_ = newColumn;
}

std::size_t soprachev::Matrix::getLines() const
{
  return lines_;
}

std::size_t soprachev::Matrix::getColumns() const
{
  return columns_;
}

std::size_t soprachev::Matrix::getLineSize(size_t line) const
{
  if (line >= lines_)
  {
    return 0;
  }

  for (size_t i = 0; i < columns_; i++)
  {
    if (shapes_[line * columns_ + i] == nullptr)
    {
      return i;
    }
  }

  return columns_;
}
