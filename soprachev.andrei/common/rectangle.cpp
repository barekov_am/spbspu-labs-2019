//
// Created by Andrei Soprachev on 2019-03-19.
//

#include "rectangle.hpp"

#include <cmath>

//прямоугольник по центру, ширине и высоте
soprachev::Rectangle::Rectangle(const point_t &pos, double width, double height) :
  Shape(pos),
  rightUp_(Vector2(width, height) / 2)
{
  if (width <= 0)
  {
    throw std::invalid_argument("Width must be positive");
  }
  if (height <= 0)
  {
    throw std::invalid_argument("Height must be positive");
  }
}

//прямоугольник по левой нижней и правой верхней вершине
soprachev::Rectangle::Rectangle(const point_t &leftDown, const point_t &rightUp) :
  Shape(Vector2(leftDown) + (Vector2(rightUp) - Vector2(leftDown)) / 2),
  rightUp_(Vector2(rightUp) - transform_.pos_)
{
  if (rightUp.x <= leftDown.x)
  {
    throw std::invalid_argument("rightUp.x must be bigger leftDown.x");
  }
  if (rightUp.y <= leftDown.y)
  {
    throw std::invalid_argument("rightUp.y must be bigger leftDown.y");
  }
}

double soprachev::Rectangle::getArea() const
{
  return getWidth() * getHeight() * transform_.scale_ * transform_.scale_;
}

soprachev::rectangle_t soprachev::Rectangle::getFrameRect() const
{
  double sinus = std::abs(sin(transform_.rot_ * M_PI / 180));
  double cosinus = std::abs(cos(transform_.rot_ * M_PI / 180));
  return rectangle_t
    {
      transform_.pos_.getPoint(),
      (rightUp_.x_ * 2 * cosinus + rightUp_.y_ * 2 * sinus) * transform_.scale_,
      (rightUp_.y_ * 2 * cosinus + rightUp_.x_ * 2 * sinus) * transform_.scale_
    };
}

soprachev::rectangle_t soprachev::Rectangle::getRelativeFrameRect(soprachev::Transform relativeTransform) const
{

  double sinus = std::abs(sin((transform_.rot_ - relativeTransform.rot_) * M_PI / 180));
  double cosinus = std::abs(cos((transform_.rot_ - relativeTransform.rot_) * M_PI / 180));

  return rectangle_t
    {
      (Vector2::translate(transform_.pos_, relativeTransform.rot_, relativeTransform.scale_) + relativeTransform.pos_).getPoint(),
      (rightUp_.x_ * 2 * cosinus + rightUp_.y_ * 2 * sinus) * relativeTransform.scale_,
      (rightUp_.y_ * 2 * cosinus + rightUp_.x_ * 2 * sinus) * relativeTransform.scale_
    };
}

double soprachev::Rectangle::getHeight() const
{
  return rightUp_.y_ * 2;
}

double soprachev::Rectangle::getWidth() const
{
  return rightUp_.x_ * 2;
}

std::string soprachev::Rectangle::getName() const
{
  return "Rectangle";
}




