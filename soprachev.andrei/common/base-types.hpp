//
// Created by Andrei Soprachev on 2019-03-19.
//

#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

#include <iostream>

namespace soprachev
{
  const char openBracket = '(';
  const char closeBracket = ')';
  const char vectorSeparator = ';';

  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    point_t pos;
    double width;
    double height;
  };

  class Vector2
  {
  public:
    double x_;
    double y_;

    Vector2();
    Vector2(double x, double y);
    Vector2(const point_t &point);

    Vector2 operator+(const Vector2 &rhs) const;
    Vector2 operator-(const Vector2 &rhs) const;
    Vector2 operator*(const double &rhs) const;
    Vector2 operator/(const double &rhs) const;
    Vector2 &operator-();
    Vector2 &operator+=(const Vector2 &rhs);
    Vector2 &operator-=(const Vector2 &rhs);
    Vector2 &operator*=(const double &rhs);
    Vector2 &operator/=(const double &rhs);

    static Vector2 translate(Vector2 vector2, double rot, double scale);
    Vector2 rotate(double angle);
    point_t getPoint() const;
  };

  std::istream &operator>>(std::istream &input, Vector2 &vector2);
  std::ostream &operator<<(std::ostream &output, const Vector2 &vector2);


  class Transform
  {
  public:
    Vector2 pos_;
    double rot_;
    double scale_;

    Transform(const Vector2 &pos = Vector2(0, 0), double angle = 0, double scale = 1);
  };
}
#endif //BASE_TYPES_HPP
