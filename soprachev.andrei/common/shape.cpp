//
// Created by Andrei Soprachev on 2019-04-01.
//

#include "shape.hpp"

soprachev::Shape::Shape(const point_t &pos, double rot, double scale) :
  transform_(Transform(Vector2(pos), rot, scale))
{ }

soprachev::Shape::Shape(const Vector2 &pos, double rot, double scale) :
  transform_(Transform(pos, rot, scale))
{ }

soprachev::Shape::Shape(const soprachev::Transform &transform) :
  transform_(transform)
{ }

void soprachev::Shape::move(const point_t &pos)
{
  transform_.pos_ = Vector2(pos);
}

void soprachev::Shape::move(double dx, double dy)
{
  transform_.pos_ += Vector2(dx, dy);
}

void soprachev::Shape::rotate(const double dAngle)
{
  transform_.rot_ += dAngle;
}

void soprachev::Shape::setRotation(const double angle)
{
  transform_.rot_ = angle;
}

void soprachev::Shape::scale(double scale)
{
  if (scale == 0)
  {
    throw std::invalid_argument("scale can not be 0");
  }
  transform_.scale_ *= scale;

}

void soprachev::Shape::setScale(double scale)
{
  transform_.scale_ = scale;
}

std::ostream &soprachev::Shape::print(std::ostream &out) const
{
  rectangle_t rectangle = getFrameRect();

  out << getName() << "\n"
    << "pos = (" << transform_.pos_.x_ << "; " << transform_.pos_.y_ << ")\n"
    << "scale = " << transform_.scale_ << "\n"
    << "area = " << getArea() << "\n"
    << "frameRect pos = (" << rectangle.pos.x << "; " << rectangle.pos.y << ")\n"
    << "width = " << rectangle.width << "\n"
    << "height = " << rectangle.height;
  return out;
}

void soprachev::Shape::setRelativeTransform(const soprachev::Transform &)
{ }

soprachev::Transform soprachev::Shape::getTransform() const
{
  return transform_;
}

