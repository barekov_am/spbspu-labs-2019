//
// Created by Andrei Soprachev on 2019-04-30.
//

#include "polygon.hpp"

#include <cmath>

soprachev::Polygon::Polygon(const soprachev::point_t *points, const std::size_t size) :
  Shape(centerOfMass(points, size)),
  vertices_(createVectorArray(points, size, transform_.pos_)),
  verticesCount_(size)
{ }

soprachev::Polygon::Polygon(const soprachev::Polygon &source) :
  Shape(source.transform_),
  vertices_(createVectorArray(source.vertices_, source.verticesCount_, Vector2())),
  verticesCount_(source.verticesCount_)
{ }

soprachev::Polygon::Polygon(soprachev::Polygon &&source) :
  Shape(std::move(source.transform_)),
  vertices_(std::move(source.vertices_)),
  verticesCount_(source.verticesCount_)
{
  source.vertices_ = nullptr;
}

soprachev::Polygon::~Polygon()
{
  delete[](vertices_);
}

soprachev::Polygon &soprachev::Polygon::operator=(const soprachev::Polygon &rhs)
{
  if (this != &rhs)
  {
    delete[](vertices_);
    transform_ = rhs.transform_;
    verticesCount_ = rhs.verticesCount_;
    vertices_ = createVectorArray(rhs.vertices_, verticesCount_, Vector2());
  }

  return *this;
}

soprachev::Polygon &soprachev::Polygon::operator=(soprachev::Polygon &&rhs)
{
  if (this != &rhs)
  {
    delete[](vertices_);
    transform_ = rhs.transform_;
    verticesCount_ = rhs.verticesCount_;
    vertices_ = std::move(rhs.vertices_);
    rhs.vertices_ = nullptr;
  }

  return *this;
}

//не использую разбиение на треугольники тк в его конструкторе рассчитывается центр - лишняя операция для поиска площади полигона
double soprachev::Polygon::getArea() const
{
  double summ = 0;
  for (std::size_t i = 0; i < verticesCount_ - 1; ++i)
  {
    summ += (vertices_[i].x_ + vertices_[i + 1].x_) * (vertices_[i].y_ - vertices_[i + 1].y_);
  }
  summ += (vertices_[verticesCount_ - 1].x_ + vertices_[0].x_) * (vertices_[verticesCount_ - 1].y_ - vertices_[0].y_);

  return std::abs(summ / 2) * transform_.scale_ * transform_.scale_;
}

soprachev::rectangle_t soprachev::Polygon::getFrameRect() const
{

  double left = vertices_[0].x_;
  double down = vertices_[0].y_;
  double right = left;
  double up = down;

  for (std::size_t i = 1; i < verticesCount_; ++i)
  {
    Vector2 vertex = Vector2::translate(vertices_[i], transform_.rot_, transform_.scale_);
    left = std::min(vertex.x_, left);
    down = std::min(vertex.y_, down);
    right = std::max(vertex.x_, right);
    up = std::max(vertex.y_, up);
  }

  //тут тоже самое что и в треуголнике.
  //left - X координата крайней левой вершины относительно центра, right - крайней правой
  //(right - left) - ширина, ((right - left) / 2) - половина ширины
  //pos_.x_ - X координата центра полигона относительно начала отсчета
  //(pos_.x_ + left) - X координата крайней левой вершины относительно начала отсчёта
  //((pos_.x_ + left) + ((right - left) / 2)) - X координата центра описанного прямоугольника относительно начала отсчёта
  //аналогично для Y
  return rectangle_t
    {
      point_t
        {
          left + ((right - left) / 2),
          down + ((up - down) / 2)
        },
      (right - left),
      (up - down)
    };
}

soprachev::rectangle_t soprachev::Polygon::getRelativeFrameRect(soprachev::Transform relativeTransform) const
{

  double left = Vector2::translate(vertices_[0],
    transform_.rot_ - relativeTransform.rot_,
    transform_.scale_ * relativeTransform.scale_).x_;

  double down = Vector2::translate(vertices_[0],
    transform_.rot_ - relativeTransform.rot_,
    transform_.scale_ * relativeTransform.scale_).y_;

  double right = left;
  double up = down;

  for (std::size_t i = 1; i < verticesCount_; ++i)
  {
    Vector2 vertex = Vector2::translate(vertices_[i],
      transform_.rot_ - relativeTransform.rot_,
      transform_.scale_ * relativeTransform.scale_);

    left = std::min(vertex.x_, left);
    down = std::min(vertex.y_, down);
    right = std::max(vertex.x_, right);
    up = std::max(vertex.y_, up);
  }

  Vector2 newPos = Vector2(transform_.pos_).rotate(relativeTransform.rot_) + relativeTransform.pos_;

  return rectangle_t
    {
      point_t
        {
          newPos.x_ + left + ((right - left) / 2),
          newPos.y_ + down + ((up - down) / 2)
        },
      (right - left),
      (up - down)
    };
}

std::string soprachev::Polygon::getName() const
{
  return "Polygon";
}

soprachev::Vector2 soprachev::Polygon::centerOfMass(const soprachev::point_t *points, std::size_t size)
{
  if (size < 3)
  {
    throw std::invalid_argument("Size must be >= 3");
  }

  if (!isConvex(points, size))
  {
    throw std::invalid_argument("Polygon must be convex");
  }

  Vector2 summ = Vector2{0, 0};
  for (std::size_t i = 0; i < size; ++i)
  {
    summ += points[i];
  }
  return summ / size;
}

template <typename T>
soprachev::Vector2 *soprachev::Polygon::createVectorArray(const T *vertices, std::size_t size, Vector2 center)
{
  Vector2 *result = new Vector2[size];
  for (std::size_t i = 0; i < size; ++i)
  {
    result[i] = Vector2(vertices[i]) - center;
  }
  return result;
}

//Если одно ребро многоугольника соответствует вектору AB, а следующее за ним ребро соответствует вектору BC,
//то направление поворота в этой паре последовательных ребер будет задаваться знаком выражения:
//AB.x * BC.y - AB.y * BC.x
bool soprachev::Polygon::isConvex(const soprachev::point_t *points, std::size_t size)
{
  Vector2 ab = Vector2(points[0]) - Vector2(points[size - 1]);
  Vector2 bc = Vector2(points[1]) - Vector2(points[0]);

  double product = (ab.x_ * bc.y_ - ab.y_ * bc.x_);
  if (product == 0)
  {
    return false;
  }

  bool isPositive = product > 0;
  for (std::size_t i = 1; i < size - 1; ++i)
  {
    ab = Vector2(points[i]) - Vector2(points[i - 1]);
    bc = Vector2(points[i + 1]) - Vector2(points[i]);

    product = (ab.x_ * bc.y_ - ab.y_ * bc.x_);

    if ((isPositive != (product > 0)) || (product == 0))
    {
      return false;
    }
  }

  ab = Vector2(points[size - 1]) - Vector2(points[size - 2]);
  bc = Vector2(points[0]) - Vector2(points[size - 1]);

  product = (ab.x_ * bc.y_ - ab.y_ * bc.x_);

  return (isPositive == (product > 0)) && (product != 0);
}


