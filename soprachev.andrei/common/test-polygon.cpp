//
// Created by Andrei Soprachev on 2019-06-04.
//

#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "polygon.hpp"

const double EPSILON = 1e-10;

const soprachev::point_t pentagonPoints[5] = {
  soprachev::point_t{0, 0},
  soprachev::point_t{4, 0},
  soprachev::point_t{5.23, 3.8},
  soprachev::point_t{2, 6.16},
  soprachev::point_t{-1.23, 3.8}
};

BOOST_AUTO_TEST_SUITE(polygonTestSuite)

  BOOST_AUTO_TEST_CASE(polygonConstantAfterMove)
  {
    soprachev::Polygon polygon(pentagonPoints, 5);
    const soprachev::rectangle_t frameBefore = polygon.getFrameRect();
    const double areaBefore = polygon.getArea();

    polygon.move({5, 5});
    soprachev::rectangle_t frameAfter = polygon.getFrameRect();
    double areaAfter = polygon.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    polygon.move(5, 5);
    frameAfter = polygon.getFrameRect();
    areaAfter = polygon.getArea();
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(polygonRotate)
  {
    soprachev::Polygon polygon(pentagonPoints, 5);
    const soprachev::rectangle_t frameBefore = polygon.getFrameRect();
    const double areaBefore = polygon.getArea();

    polygon.rotate(90);

    soprachev::rectangle_t frameAfter = polygon.getFrameRect();
    double areaAfter = polygon.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    polygon.rotate(270);
    frameAfter = polygon.getFrameRect();
    areaAfter = polygon.getArea();

    BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(polygonScale)
  {

    soprachev::Polygon polygon(pentagonPoints, 5);
    const double areaBefore = polygon.getArea();

    const int scaleMultiplier = 2;
    polygon.scale(scaleMultiplier);
    double areaAfter = polygon.getArea();
    BOOST_CHECK_CLOSE(areaBefore * scaleMultiplier * scaleMultiplier, areaAfter, EPSILON);

    polygon.scale(1.0 / scaleMultiplier);
    areaAfter = polygon.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

    BOOST_CHECK_THROW(polygon.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(polygonMoveAndCopy)
  {

    soprachev::Polygon polygon = soprachev::Polygon(pentagonPoints, 5);
    const double areaBefore = polygon.getArea();

    soprachev::Polygon polygon2 = soprachev::Polygon(polygon); //copy constructor
    BOOST_CHECK_CLOSE(areaBefore, polygon2.getArea(), EPSILON);

    soprachev::Polygon polygon3 = soprachev::Polygon(std::move(polygon2)); //move constructor
    BOOST_CHECK_CLOSE(areaBefore, polygon3.getArea(), EPSILON);

    polygon3 = polygon; //copy-assigned
    BOOST_CHECK_CLOSE(areaBefore, polygon3.getArea(), EPSILON);

    polygon3 = std::move(polygon); //move-assigned
    BOOST_CHECK_CLOSE(areaBefore, polygon3.getArea(), EPSILON);

  }

  BOOST_AUTO_TEST_CASE(throwingExceptionsPolygon)
  {
    //похожая на стрклку GPS фигура
    soprachev::point_t nonConvexPoints[4] = {
      soprachev::point_t{0, 0},
      soprachev::point_t{2, 1},
      soprachev::point_t{4, 0},
      soprachev::point_t{2, 4}
    };
    BOOST_CHECK_THROW(soprachev::Polygon(nonConvexPoints, 4), std::invalid_argument);

    //вырожденный треугольник
    soprachev::point_t nonConvexTriangle[3] = {
      soprachev::point_t{0, 0},
      soprachev::point_t{2, 0},
      soprachev::point_t{4, 0}
    };
    BOOST_CHECK_THROW(soprachev::Polygon(nonConvexTriangle, 3), std::invalid_argument);

    soprachev::point_t linePoints[2] = {
      soprachev::point_t{0, 0},
      soprachev::point_t{2, 1}
    };
    BOOST_CHECK_THROW(soprachev::Polygon(linePoints, 2), std::invalid_argument);
  }


BOOST_AUTO_TEST_SUITE_END()
