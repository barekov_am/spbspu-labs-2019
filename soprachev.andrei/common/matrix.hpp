//
// Created by Andrei Soprachev on 2019-06-06.
//

#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"
#include <memory>

namespace soprachev
{
  class Matrix
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    using shape_array = std::unique_ptr<shape_ptr[]>;

    Matrix();
    Matrix(const Matrix &rhs);
    Matrix(Matrix &&rhs);
    ~Matrix() = default;

    Matrix &operator=(const Matrix &rhs);
    Matrix &operator=(Matrix &&rhs);
    shape_array operator[](size_t index) const;
    bool operator==(const Matrix &shapesMatrix) const;
    bool operator!=(const Matrix &shapesMatrix) const;

    void add(shape_ptr shape, size_t line, size_t column);
    size_t getLines() const;
    size_t getColumns() const;
    size_t getLineSize(size_t line) const;

  private:
    size_t lines_;
    size_t columns_;
    shape_array shapes_;
  };
};

#endif //MATRIX_HPP
