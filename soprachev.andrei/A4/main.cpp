//
// Created by Andrei Soprachev on 2019-06-05.
//

#include <iostream>
#include <composite-shape.hpp>
#include "circle.hpp"
#include "polygon.hpp"
#include "matrix.hpp"
#include "partition.hpp"

int main()
{
  //создание нового композит шейп в координатах (1; 1)
  soprachev::CompositeShape compositeShape = soprachev::CompositeShape(soprachev::point_t{1, 1});

  //добавление фигуры с переводом координат в локальные
  compositeShape.addAsGlobal(std::make_shared<soprachev::Circle>(soprachev::Circle(soprachev::point_t{6, 3}, 2)));

  //добавление фигуры без перевода координат
  compositeShape.addAsLocal(std::make_shared<soprachev::Circle>(soprachev::Circle(soprachev::point_t{6, 3}, 2)));

  //фигура по индексу
  std::shared_ptr<soprachev::Shape> shape = compositeShape[0];
  shape->print(std::cout) << "\n\n";

  //перебор всех фигур
  for (size_t i = 0; i < compositeShape.shapesCount(); ++i)
  {
    compositeShape[i]->print(std::cout) << "\n\n";
  }

  //разбиение фигуры на слои
  soprachev::Matrix matrix = soprachev::part(compositeShape);

  std::cout << matrix.getColumns() << "x" << matrix.getLines();

  //удаление фигуры по индексу
  compositeShape.remove(0);

  return 0;
}
