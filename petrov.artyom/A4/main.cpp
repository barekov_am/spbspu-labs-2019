#include <iostream>
#include "../common/shape.hpp"
#include "../common/rectangle.hpp"
#include "../common/circle.hpp"
#include "../common/matrix.hpp"
#include "../common/division.hpp"
#include "../common/composite-shape.hpp"

int main()
{
  petrov::Shape *shapes[5];
  petrov::Rectangle rectangle1({4, 3}, 4, 2);
  petrov::Circle circle2({6, 4}, 1);
  petrov::Rectangle rectangle3({3, 8}, 2, 4);
  petrov::Rectangle rectangle4({7, 0}, 6, 2);
  petrov::Circle circle5({10, 5}, 1);
  petrov::CompositeShape compositeShape;
  compositeShape.addShape(&rectangle4);
  compositeShape.addShape(&circle5);
  petrov::Circle circle6({7, 4}, 1);

  shapes[0] = &rectangle1;
  shapes[1] = &circle2;
  shapes[2] = &rectangle3;
  shapes[3] = &compositeShape;
  shapes[4] = &circle6;

  petrov::Matrix<petrov::Shape *> matrix = petrov::divide(shapes, 5);

  for (int i = 0; i < matrix.getRowsCount(); i++)
  {
    int rowLength;
    petrov::Shape **row = matrix.getRow(0, &rowLength);
    for (int j = 0; j < rowLength; j++)
    {
      std::cout << "Centerd of shape #" << j << " in row #" << i << ": "
                << "(" << row[0]->getCentre().x << ";"
                << row[0]->getCentre().y << ")" << std::endl;
    }
  }

  return 0;
}
