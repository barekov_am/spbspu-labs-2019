#include <iostream>
#include "../common/composite-shape.hpp"
#include "../common/rectangle.hpp"
#include "../common/circle.hpp"

int main()
{

  petrov::CompositeShape compositeShape = petrov::CompositeShape();

  petrov::Rectangle rectangle({15, 20}, 10, 15);
  compositeShape.addShape(&rectangle);

  std::cout << "Area of CompositeShape (of: Rectangle {(15, 20), 10, 25}): "
            << compositeShape.getArea() << std::endl;

  compositeShape.scale(2);

  std::cout << "Area of CompositeShape (of: Rectangle {(15, 20), 10, 25}), scaled by 2:"
            << compositeShape.getArea() << std::endl;

  compositeShape.removeShape(&rectangle);

  petrov::Rectangle firstPart({14, 18}, 20, 30);
  compositeShape.addShape(&firstPart);
  petrov::Circle secondPart({5, 5}, 3);
  compositeShape.addShape(&secondPart);

  petrov::CompositeShape superShape(&compositeShape);
  petrov::Rectangle extraPart({20, 20}, 40, 80);
  compositeShape.addShape(&extraPart);

  std::cout << "Area of super shape: "
            << superShape.getArea() << std::endl;

  superShape.scale(3);

  std::cout << "Area of super shape, scaled by 3: "
            << superShape.getArea() << std::endl;
}
