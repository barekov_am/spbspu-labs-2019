#include "composite-shape.hpp"

#include <limits>
#include <stdexcept>
#include <math.h>

petrov::CompositeShape::CompositeShape() :
    shapes_(std::unique_ptr<Shape*[]>(new Shape*[INITIAL_CAPACITY])),
    capacity_(INITIAL_CAPACITY),
    shapeCount_(0),
    isFrameRectCalculated_(false)
{
}

petrov::CompositeShape::CompositeShape(petrov::CompositeShape &other) :
  capacity_(other.capacity_),
  shapeCount_(other.shapeCount_),
  frameRect_(other.frameRect_),
  isFrameRectCalculated_(other.isFrameRectCalculated_)
{
  for (int i = 0; i < shapeCount_; i++)
  {
    shapes_[i] = other.shapes_[i];
  }
}

petrov::CompositeShape::CompositeShape(petrov::CompositeShape &&other) noexcept :
  shapes_(std::move(other.shapes_)),
  capacity_(other.capacity_),
  shapeCount_(other.shapeCount_),
  frameRect_(other.frameRect_),
  isFrameRectCalculated_(other.isFrameRectCalculated_)
{
}

petrov::CompositeShape::CompositeShape(petrov::Shape *initialShape) :
  CompositeShape()
{
  if (initialShape == nullptr)
  {
    throw std::invalid_argument("Shape cannot be nullptr");
  }
  addShape(initialShape);
}

petrov::CompositeShape& petrov::CompositeShape::operator=(const petrov::CompositeShape &other)
{
  if (this == &other)
  {
    return *this;
  }

  capacity_ = other.capacity_;
  shapeCount_ = other.shapeCount_;
  frameRect_ = other.frameRect_;
  isFrameRectCalculated_ = other.isFrameRectCalculated_;
  shapes_ = std::unique_ptr<Shape*[]>(new Shape*[capacity_]);
  for (int i = 0; i < shapeCount_; i++)
  {
    shapes_[i] = other.shapes_[i];
  }

  return *this;
}

petrov::CompositeShape& petrov::CompositeShape::operator=(petrov::CompositeShape &&other) noexcept
{
  if (this == &other)
  {
    return *this;
  }

  shapeCount_ = other.shapeCount_;
  capacity_ = other.capacity_;
  shapes_ = std::move(other.shapes_);
  frameRect_ = other.frameRect_;
  isFrameRectCalculated_ = other.isFrameRectCalculated_;

  return *this;
}

petrov::Shape* petrov::CompositeShape::operator[](int index) const
{
  if (index >= shapeCount_) {
    throw std::invalid_argument("Index is out of bounds");
  }

  return shapes_[index];
}

void petrov::CompositeShape::addShape(petrov::Shape *shape)
{
  if (shapeCount_ == capacity_) {

    int newCapacity = int(capacity_ * SCALE_FACTOR);

    std::unique_ptr<Shape*[]> newArray(new Shape*[newCapacity]);

    for (int i = 0; i < capacity_; i++)
    {
      newArray[i] = shapes_[i];
    }
    shapes_.swap(newArray);
    capacity_ = newCapacity;
  }

  shapes_[shapeCount_++] = shape;

  isFrameRectCalculated_ = false;
}

void petrov::CompositeShape::removeShape(petrov::Shape *shape)
{
  int index;
  for (index = 0; index < shapeCount_; index++)
  {
    if (shapes_[index] == shape)
    {
      break;
    }
  }

  if (index == shapeCount_)
  {
    throw std::invalid_argument("Shape not found");
  }

  while (index < shapeCount_ - 1)
  {
    shapes_[index] = shapes_[index + 1];
    index++;
  }
  shapeCount_--;

  isFrameRectCalculated_ = false;
}

double petrov::CompositeShape::getArea() const
{
  double area = 0;
  for (int i = 0; i < shapeCount_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

petrov::rectangle_t petrov::CompositeShape::getFrameRect() const
{
  if(isFrameRectCalculated_) {
    return frameRect_;
  }

  if (shapeCount_ == 0)
  {
    throw std::logic_error("No shapes detected inside CompositeShape");
  }

  double left = std::numeric_limits<double>::max();
  double right = std::numeric_limits<double>::min();
  double top = std::numeric_limits<double>::min();
  double bottom = std::numeric_limits<double>::max();

  for (int i = 0; i < shapeCount_; i++)
  {
    rectangle_t currentFrameRect = shapes_[i]->getFrameRect();

    double leftBorder = currentFrameRect.pos.x - currentFrameRect.width / 2;
    if (left > leftBorder)
    {
      left = leftBorder;
    }

    double rightBorder = currentFrameRect.pos.x + currentFrameRect.width / 2;
    if (right < rightBorder)
    {
      right = rightBorder;
    }

    double topBorder = currentFrameRect.pos.y + currentFrameRect.height / 2;
    if (top < topBorder)
    {
      top = topBorder;
    }

    double bottomBorder = currentFrameRect.pos.y - currentFrameRect.height / 2;
    if (bottom > bottomBorder)
    {
      bottom = bottomBorder;
    }
  }

  frameRect_ = petrov::rectangle_t{right - left, top - bottom, {(right + left) / 2, (top + bottom) / 2}};
  isFrameRectCalculated_ = true;
  return frameRect_;
}

void petrov::CompositeShape::move(const petrov::point_t &centre)
{
  if (shapeCount_ == 0)
  {
    return;
  }

  point_t currentCentre = getCentre();

  move(centre.x - currentCentre.x, centre.y - currentCentre.y);
}

void petrov::CompositeShape::move(double dx, double dy)
{
  isFrameRectCalculated_ = false;

  for (int i = 0; i < shapeCount_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void petrov::CompositeShape::scale(double scaleCoef)
{

  for (int i = 0; i < shapeCount_; i++)
  {
    Shape *currentShape = shapes_[i];

    point_t newCentre{
        getCentre().x + (currentShape->getCentre().x - getCentre().x) * scaleCoef,
        getCentre().y + (currentShape->getCentre().y - getCentre().y) * scaleCoef
    };

    currentShape->move(newCentre);
    currentShape->scale(scaleCoef);
  }
}

petrov::point_t petrov::CompositeShape::getCentre() const
{
  if (!isFrameRectCalculated_)
  {
    getFrameRect();
    isFrameRectCalculated_ = true;
  }
  return frameRect_.pos;
}

int petrov::CompositeShape::getShapesCount() const
{
  return shapeCount_;
}

petrov::Shape *petrov::CompositeShape::getShape(int index)
{
  return shapes_[index];
}

void petrov::CompositeShape::rotate(double angle)
{
  double angleRad = angle * M_PI / 180;
  double centreX = getCentre().x;
  double centreY = getCentre().y;
  for (int i = 0; i < shapeCount_; i++)
  {
    double shapeX = shapes_[i]->getCentre().x;
    double shapeY = shapes_[i]->getCentre().y;

    shapes_[i]->move({
        centreX + (shapeX - centreX) * std::cos(angleRad) - (shapeY - centreY) * std::sin(angleRad),
        centreY + (shapeX - centreX) * std::sin(angleRad) + (shapeY - centreY) * std::cos(angleRad)
    });
    shapes_[i]->rotate(angle);
  }

  isFrameRectCalculated_ = false;
}
