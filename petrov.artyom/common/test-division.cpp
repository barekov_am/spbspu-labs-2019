#include <boost/test/auto_unit_test.hpp>
#include "division.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(DividionTest)

  BOOST_AUTO_TEST_CASE(areOverlapping_throwsWhenEitherOfParametersIsNullptr)
  {
    petrov::Rectangle rectangle({0, 0}, 10, 10);
    BOOST_CHECK_THROW(petrov::areOverlapping(nullptr, &rectangle), std::invalid_argument)
    BOOST_CHECK_THROW(petrov::areOverlapping(&rectangle, nullptr), std::invalid_argument)
  }

  BOOST_AUTO_TEST_CASE(areOverlapping_WorksCorrectly_TruePositive)
  {
    petrov::Rectangle rectangle({3, 2}, 4, 2);
    petrov::Circle circle({5, 3}, 1);
    BOOST_CHECK(petrov::areOverlapping(&rectangle, &circle));

    petrov::Circle circle2({0, 5}, 1);
    petrov::CompositeShape compositeShape;
    compositeShape.addShape(&rectangle);
    compositeShape.addShape(&circle);
    compositeShape.addShape(&circle2);

    petrov::Circle circle3({3, 6}, 1);
    BOOST_CHECK(petrov::areOverlapping(&compositeShape, &circle3));
  }

  BOOST_AUTO_TEST_CASE(areOverlapping_WorksCorrectly_TrueNegative)
  {
    petrov::Rectangle rectangle({3, 2}, 4, 2);
    petrov::Circle circle({3, 6}, 1);
    BOOST_CHECK(!petrov::areOverlapping(&rectangle, &circle));

    petrov::CompositeShape compositeShape;
    compositeShape.addShape(&rectangle);
    compositeShape.addShape(&circle);

    petrov::Circle circle2({-2, 2}, 1);
    BOOST_CHECK(!petrov::areOverlapping(&compositeShape, &circle2));
  }

  BOOST_AUTO_TEST_CASE(divide_WorksCorrectly)
  {
    petrov::Shape* shapes[5];
    petrov::Rectangle rectangle1({4, 3}, 4, 2);
    petrov::Circle circle2({6, 4}, 1);
    petrov::Rectangle rectangle3({3, 8}, 2, 4);
    petrov::Rectangle rectangle4({7, 0}, 6, 2);
    petrov::Circle circle5({10, 5}, 1);
    petrov::CompositeShape compositeShape;
    compositeShape.addShape(&rectangle4);
    compositeShape.addShape(&circle5);
    petrov::Circle circle6({7, 4}, 1);

    shapes[0] = &rectangle1;
    shapes[1] = &circle2;
    shapes[2] = &rectangle3;
    shapes[3] = &compositeShape;
    shapes[4] = &circle6;

    petrov::Matrix<petrov::Shape*> matrix = petrov::divide(shapes, 5);

    int rowLength;
    petrov::Shape** row = matrix.getRow(0, &rowLength);
    BOOST_CHECK_EQUAL(3, rowLength);
    BOOST_CHECK_EQUAL(&rectangle1, row[0]);
    BOOST_CHECK_EQUAL(&rectangle3, row[1]);
    BOOST_CHECK_EQUAL(&circle6, row[2]);

    row = matrix.getRow(1, &rowLength);
    BOOST_CHECK_EQUAL(1, rowLength);
    BOOST_CHECK_EQUAL(&circle2, row[0]);

    row = matrix.getRow(2, &rowLength);
    BOOST_CHECK_EQUAL(1, rowLength);
    BOOST_CHECK_EQUAL(&compositeShape, row[0]);
  }

BOOST_AUTO_TEST_SUITE_END()
