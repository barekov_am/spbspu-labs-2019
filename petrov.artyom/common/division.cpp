#include "division.hpp"
#include <math.h>

bool petrov::areOverlapping(const petrov::Shape *shape1, const petrov::Shape *shape2)
{
  if (shape1 == nullptr || shape2 == nullptr)
  {
    throw std::invalid_argument("Shape cannot be nullptr");
  }

  petrov::rectangle_t frameRect1 = shape1->getFrameRect();
  petrov::rectangle_t frameRect2 = shape2->getFrameRect();

  return (std::abs(frameRect1.pos.x - frameRect2.pos.x) < (frameRect1.width + frameRect2.width) / 2
          && std::abs(frameRect1.pos.y - frameRect2.pos.y) < (frameRect1.height + frameRect2.height) / 2
  );
}

petrov::Matrix<petrov::Shape *> petrov::divide(petrov::Shape **shapes, int shapeCount)
{
  Matrix<Shape*> matrix(shapeCount);
  for (int i = 0; i < shapeCount; i++)
  {
    bool overlapped = true;
    for (int j = 0; j < matrix.getRowsCount(); j++)
    {
      overlapped = false;
      int rowLength;
      Shape** row = matrix.getRow(j, &rowLength);

      for (int k = 0; k < rowLength; k++)
      {
        if (areOverlapping(shapes[i], row[k])) {
          overlapped = true;
          break;
        }
      }

      if (!overlapped) {
        matrix.add(j, shapes[i]);
        break;
      }
    }

    if (overlapped) {
      matrix.add(matrix.getRowsCount(), shapes[i]);
    }
  }

  return matrix;
}
