#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>
#include <iostream>

#include "circle.hpp"
#include "rectangle.hpp"

const double INACCURACY = 0.0001;

BOOST_AUTO_TEST_SUITE(TestRect)

  BOOST_AUTO_TEST_CASE(constructor_ThrowsOnInvalidRadius)
  {
    BOOST_CHECK_THROW(petrov::Rectangle({5, 5}, -10, 20), std::invalid_argument);
    BOOST_CHECK_THROW(petrov::Rectangle({5, 5}, 10, -20), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(move_WorksCorrectly)
  {
    petrov::point_t centre{10, 20};
    double width = 10;
    double height = 20;
    petrov::Rectangle rectangle = petrov::Rectangle(centre, width, height);

    double dx = 40;
    double dy = 30;
    rectangle.move(dx, dy);

    BOOST_CHECK_EQUAL(centre.x + dx, rectangle.getCentre().x);
    BOOST_CHECK_EQUAL(centre.y + dy, rectangle.getCentre().y);

    double x = 40;
    double y = 30;
    petrov::point_t destinationPoint {x, y};
    rectangle.move(destinationPoint);

    BOOST_CHECK_EQUAL(destinationPoint.x, rectangle.getCentre().x);
    BOOST_CHECK_EQUAL(destinationPoint.y, rectangle.getCentre().y);
  }

  BOOST_AUTO_TEST_CASE(move_DoesNotChangeDimentions)
  {
    petrov::point_t centre{10, 20};
    double width = 10;
    double height = 20;
    petrov::Rectangle rectangle = petrov::Rectangle(centre, width, height);

    double dx = 40;
    double dy = 30;
    rectangle.move(dx, dy);

    BOOST_CHECK_EQUAL(width, rectangle.getWidth());
    BOOST_CHECK_EQUAL(height, rectangle.getHeight());

    double x = 40;
    double y = 30;
    petrov::point_t destinationPoint {x, y};
    rectangle.move(destinationPoint);

    BOOST_CHECK_EQUAL(width, rectangle.getWidth());
    BOOST_CHECK_EQUAL(height, rectangle.getHeight());
  }

  BOOST_AUTO_TEST_CASE(scale_AreaGrowsInSquarePropotion)
  {
    petrov::Rectangle rectangle = petrov::Rectangle({1, 2}, 10, 20);
    double scaleFactor = 5;

    double areaBeforeScaling = rectangle.getArea();
    rectangle.scale(scaleFactor);
    double areaAfterScaling = rectangle.getArea();

    BOOST_CHECK_CLOSE(scaleFactor * scaleFactor,
                      areaAfterScaling / areaBeforeScaling,
                      INACCURACY);
  }

  BOOST_AUTO_TEST_CASE(rotate_WorksCorrectly)
  {
    petrov::point_t centre{1, 2};
    double width = 10;
    double height = 20;
    petrov::Rectangle rectangle = petrov::Rectangle(centre, width, height);

    double areaBeforeRotation = rectangle.getArea();

    rectangle.rotate(90);

    BOOST_CHECK_EQUAL(centre.x, rectangle.getCentre().x);
    BOOST_CHECK_EQUAL(centre.y, rectangle.getCentre().y);
    BOOST_CHECK_EQUAL(width, rectangle.getWidth());
    BOOST_CHECK_EQUAL(height, rectangle.getHeight());
    BOOST_CHECK_EQUAL(areaBeforeRotation, rectangle.getArea());
    BOOST_CHECK_CLOSE(width, rectangle.getFrameRect().height, INACCURACY);
    BOOST_CHECK_CLOSE(height, rectangle.getFrameRect().width, INACCURACY);
    BOOST_CHECK_EQUAL(centre.x, rectangle.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(centre.y, rectangle.getFrameRect().pos.y);
  }

BOOST_AUTO_TEST_SUITE_END()
