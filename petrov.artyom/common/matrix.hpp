#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>

namespace petrov
{
  template <typename T>
  class Matrix
  {
  public:

    explicit Matrix(int capacity);

    Matrix(const Matrix &other);

    Matrix(Matrix &&other) noexcept;

    ~Matrix();

    Matrix<T>& operator=(const Matrix &other);

    Matrix<T>& operator=(Matrix &&other) noexcept;

    void add(int row, const T element);

    T* getRow(int row, int *rowLength) const;

    T get(int row, int column) const;

    int getRowsCount() const;

  private:
    T* array_;
    std::unique_ptr<int[]> itemsInRows_;

    int capacity_;
    int length_;
    int rowsCount_;
  };

  template<typename T>
  Matrix<T>::Matrix(int capacity) :
  itemsInRows_(std::unique_ptr<int[]>(new int[0])),
    capacity_(capacity),
    length_(0),
    rowsCount_(0)
  {
    if (capacity < 0)
    {
      throw std::invalid_argument("Number of elements cannot be less than 0");
    }
    array_ = new T[capacity];
  }

  template<typename T>
  Matrix<T>::Matrix(const Matrix &other) :
    array_(new T[other.capacity_]),
    itemsInRows_(std::unique_ptr<int[]>(new int[other.rowsCount_])),
    capacity_(other.capacity_),
    length_(other.length_),
    rowsCount_(other.rowsCount_)
  {
    for (int i = 0; i < length_; i++)
    {
      array_[i] = other.array_[i];
    }
    for (int i = 0; i < rowsCount_; i++)
    {
      itemsInRows_[i] = other.itemsInRows_[i];
    }
  }

  template<typename T>
  Matrix<T>::Matrix(Matrix &&other) noexcept :
      array_(other.array_),
      itemsInRows_(std::move(other.itemsInRows_)),
      capacity_(other.capacity_),
      length_(other.length_),
      rowsCount_(other.rowsCount_)
  {
    other.array_ = nullptr;
    other.itemsInRows_ = nullptr;
  }

  template<typename T>
  Matrix<T>::~Matrix()
  {
    delete[](array_);
  }

  template<typename T>
  Matrix<T> &Matrix<T>::operator=(const Matrix &other)
  {
    if (this == &other) {
      return *this;
    }

    array_ = new T[other.capacity_];
    for (int i = 0; i < other.length_; i++)
    {
      array_[i] = other.array_[i];
    }

    itemsInRows_ = new int[other.rowsCount_];
    for (int i = 0; i < other.rowsCount_; i++)
    {
      itemsInRows_[i] = other.itemsInRows_[i];
    }

    capacity_ = other.capacity_;
    length_ = other.length_;
    rowsCount_ = other.rowsCount_;

    return *this;
  }

  template<typename T>
  Matrix<T>& Matrix<T>::operator=(Matrix &&other) noexcept
  {
    if (this == &other) {
      return *this;
    }

    array_ = other.array_;
    itemsInRows_ = other.itemsInRows_;
    capacity_ = other.capacity_;
    length_ = other.length_;
    rowsCount_ = other.rowsCount_;

    other.array_ = nullptr;
    other.itemsInRows_ = nullptr;

    return *this;
  }

  template<typename T>
  void Matrix<T>::add(int row, const T element)
  {
    if (length_ >= capacity_)
    {
      throw std::out_of_range("Matrix limit exceeded");
    }

    if (row >= rowsCount_)
    {
      std::unique_ptr<int[]> itemsInRows = std::unique_ptr<int[]>(new int[row + 1]);

      for (int i = 0; i < rowsCount_; i++)
      {
        itemsInRows[i] = itemsInRows_[i];
      }
      for (int i = rowsCount_; i <= row; i++)
      {
        itemsInRows[i] = 0;
      }
      itemsInRows_ = std::move(itemsInRows);
      rowsCount_ = row + 1;
    }

    int index = 0;
    for (int i = 0; i <= row; i++)
    {
     index += itemsInRows_[i];
    }

    length_++;
    itemsInRows_[row]++;
    for (int i = length_ - 2; i >= index; i--)
    {
      array_[i + 1] = array_[i]; 
    }
    array_[index] = element;
  }

  template<typename T>
  T* Matrix<T>::getRow(int row, int *rowLength) const
  {
    if (row >= rowsCount_)
    {
      throw std::invalid_argument("No row with index");
    }

    int index = 0;
    for (int i = 0; i < row; i++)
    {
      index += itemsInRows_[i];
    }

    *rowLength = itemsInRows_[row];
    return array_ + index;
  }

  template<typename T>
  T Matrix<T>::get(int row, int column) const
  {
    int rowLength;
    T* arrayRow = getRow(row, &rowLength);

    if (column >= rowLength) {
      throw std::invalid_argument("Column index out of bounds");
    }

    return arrayRow[column];
  }

  template<typename T>
  int Matrix<T>::getRowsCount() const
  {
    return rowsCount_;
  }
}

#endif //LOCAL_MATRIX_HPP
