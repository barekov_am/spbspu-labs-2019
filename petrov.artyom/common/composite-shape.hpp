#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include <memory>

namespace petrov
{
  class CompositeShape : public Shape
  {
  public:

    CompositeShape();
    CompositeShape(CompositeShape &other);
    CompositeShape(CompositeShape &&other) noexcept;
    CompositeShape(Shape * initialShape);

    ~CompositeShape() override = default;

    CompositeShape& operator=(const CompositeShape &other);
    CompositeShape& operator=(CompositeShape &&other) noexcept;

    Shape* operator[](int index) const;

    void addShape(Shape * shape);

    void removeShape(Shape * shape);

    double getArea() const override;

    rectangle_t getFrameRect() const override;

    void move(const point_t & centre) override;

    void move(double dx, double dy) override;

    void scale(double scaleCoef) override;

    point_t getCentre() const override;

    int getShapesCount() const;

    Shape * getShape(int index);

    void rotate(double angle) override;

  private:

    static const int INITIAL_CAPACITY = 5;
    static constexpr double SCALE_FACTOR = 1.5f;

    std::unique_ptr<Shape*[]> shapes_;
    int capacity_;
    int shapeCount_;

    mutable rectangle_t frameRect_;
    mutable bool isFrameRectCalculated_;
  };
}

#endif //COMPOSITE_SHAPE_HPP
