#ifndef LOCAL_DIVISION_HPP
#define LOCAL_DIVISION_HPP

#include "matrix.hpp"
#include "shape.hpp"

namespace petrov
{
  bool areOverlapping(const Shape *shape1, const Shape *shape2);

  Matrix<Shape*> divide(Shape** shapes, int shapeCount);
}

#endif //LOCAL_DIVISION_HPP
