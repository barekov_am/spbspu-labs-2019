#include <boost/test/auto_unit_test.hpp>
#include "matrix.hpp"

BOOST_AUTO_TEST_SUITE(MatrixTest)

  BOOST_AUTO_TEST_CASE(constructor_ThrowsOnInvalidParameter)
  {
    BOOST_CHECK_THROW(petrov::Matrix<int>(-1), std::invalid_argument)
  }

  BOOST_AUTO_TEST_CASE(constructor_DoesNotThrowOnValidParameter)
  {
    BOOST_CHECK_NO_THROW(petrov::Matrix<int>(0))
    BOOST_CHECK_NO_THROW(petrov::Matrix<int>(1))
    BOOST_CHECK_NO_THROW(petrov::Matrix<int>(5))
  }

  BOOST_AUTO_TEST_CASE(add_WorksCorrectly)
  {
    petrov::Matrix<int> matrix(5);
    BOOST_CHECK_EQUAL(0, matrix.getRowsCount());

    int item1 = 42;
    matrix.add(0, item1);
    BOOST_CHECK_EQUAL(item1, matrix.get(0, 0));
    BOOST_CHECK_EQUAL(1, matrix.getRowsCount());

    int item2 = 146;
    matrix.add(1, item2);
    BOOST_CHECK_EQUAL(item1, matrix.get(0, 0));
    BOOST_CHECK_EQUAL(item2, matrix.get(1, 0));
    BOOST_CHECK_EQUAL(2, matrix.getRowsCount());

    int item3 = 282;
    matrix.add(0, item3);
    BOOST_CHECK_EQUAL(item1, matrix.get(0, 0));
    BOOST_CHECK_EQUAL(item2, matrix.get(1, 0));
    BOOST_CHECK_EQUAL(item3, matrix.get(0, 1));
    BOOST_CHECK_EQUAL(2, matrix.getRowsCount());

    int item4 = 1337;
    matrix.add(3, item4);
    BOOST_CHECK_EQUAL(item1, matrix.get(0, 0));
    BOOST_CHECK_EQUAL(item2, matrix.get(1, 0));
    BOOST_CHECK_EQUAL(item3, matrix.get(0, 1));
    BOOST_CHECK_EQUAL(item4, matrix.get(3, 0));
    BOOST_CHECK_EQUAL(4, matrix.getRowsCount());

    int item5 = 24324;
    matrix.add(0, item5);
    BOOST_CHECK_EQUAL(item1, matrix.get(0, 0));
    BOOST_CHECK_EQUAL(item2, matrix.get(1, 0));
    BOOST_CHECK_EQUAL(item3, matrix.get(0, 1));
    BOOST_CHECK_EQUAL(item4, matrix.get(3, 0));
    BOOST_CHECK_EQUAL(item5, matrix.get(0, 2));
    BOOST_CHECK_EQUAL(4, matrix.getRowsCount());
  }

  BOOST_AUTO_TEST_CASE(add_ThrowsOnExceedingElementsCount)
  {
    petrov::Matrix<int> matrix(1);

    matrix.add(0, 42);

    BOOST_CHECK_THROW(matrix.add(1, 1337), std::out_of_range)
  }

  BOOST_AUTO_TEST_CASE(getRow_ThrowsOnRowExceedsRowsInMatrix)
  {
    petrov::Matrix<int> matrix(3);
    int rowLength;

    BOOST_CHECK_THROW(matrix.getRow(0, &rowLength), std::invalid_argument)

    matrix.add(0, 42);

    BOOST_CHECK_THROW(matrix.getRow(1, &rowLength), std::invalid_argument)
  }

  BOOST_AUTO_TEST_CASE(getRow_WorksCorrectly)
  {
    petrov::Matrix<int> matrix(4);
    matrix.add(0, 42);
    matrix.add(0, 146);
    matrix.add(1, 282);
    matrix.add(3, 1337);
    int rowLength;

    int* row = matrix.getRow(0, &rowLength);
    BOOST_CHECK_EQUAL(2, rowLength);
    BOOST_CHECK_EQUAL(42, row[0]);
    BOOST_CHECK_EQUAL(146, row[1]);

    row = matrix.getRow(1, &rowLength);
    BOOST_CHECK_EQUAL(1, rowLength);
    BOOST_CHECK_EQUAL(282, row[0]);

    matrix.getRow(2, &rowLength);
    BOOST_CHECK_EQUAL(0, rowLength);

    row = matrix.getRow(3, &rowLength);
    BOOST_CHECK_EQUAL(1, rowLength);
    BOOST_CHECK_EQUAL(1337, row[0]);
  }

  BOOST_AUTO_TEST_CASE(get_ThrowsOnInvalidParameters)
  {
    petrov::Matrix<int> matrix(3);

    matrix.add(0, 42);
    matrix.add(0, 146);
    matrix.add(1, 282);

    BOOST_CHECK_THROW(matrix.get(0, 2), std::invalid_argument)
    BOOST_CHECK_THROW(matrix.get(2, 0), std::invalid_argument)
  }

BOOST_AUTO_TEST_SUITE_END()
