#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "shape.hpp"

const double INACCURACY = 0.0001;

BOOST_AUTO_TEST_SUITE(CompositeShapeTest)

  BOOST_AUTO_TEST_CASE(constructor_NoParam_WorksCorrectly)
  {
    petrov::CompositeShape compositeShape;

    BOOST_CHECK_EQUAL(0, compositeShape.getShapesCount());
  }

  BOOST_AUTO_TEST_CASE(constructor_ShapeParam_WorksCorrectly)
  {
    petrov::Rectangle rectangle({5, 5}, 20, 10);
    petrov::CompositeShape compositeShape(&rectangle);

    BOOST_CHECK_EQUAL(1, compositeShape.getShapesCount());
    BOOST_CHECK_EQUAL(&rectangle, compositeShape.getShape(0));
  }


  BOOST_AUTO_TEST_CASE(constructor_ShapeListParam_WorksCorrectly)
  {
    petrov::Rectangle item0({5, 5}, 20, 10);
    petrov::Circle item1({5, 5}, 20);
    petrov::Rectangle item2({5, 5}, 20, 10);
    petrov::Circle item3({5, 5}, 10);

    petrov::CompositeShape compositeShape;
    compositeShape.addShape(&item0);
    compositeShape.addShape(&item1);
    compositeShape.addShape(&item2);
    compositeShape.addShape(&item3);

    BOOST_CHECK_EQUAL(4, compositeShape.getShapesCount());
    BOOST_CHECK_EQUAL(&item0, compositeShape.getShape(0));
    BOOST_CHECK_EQUAL(&item1, compositeShape.getShape(1));
    BOOST_CHECK_EQUAL(&item2, compositeShape.getShape(2));
    BOOST_CHECK_EQUAL(&item3, compositeShape.getShape(3));
  }

  BOOST_AUTO_TEST_CASE(addShape_WorksCorrectly)
  {
    petrov::CompositeShape compositeShape;

    petrov::Rectangle item0({1, 2}, 5, 10);

    compositeShape.addShape(&item0);
    BOOST_CHECK_EQUAL(1, compositeShape.getShapesCount());
    BOOST_CHECK_EQUAL(&item0, compositeShape.getShape(0));

    petrov::Circle item1({1, 3}, 14);

    compositeShape.addShape(&item1);
    BOOST_CHECK_EQUAL(2, compositeShape.getShapesCount());
    BOOST_CHECK_EQUAL(&item1, compositeShape.getShape(1));
  }

  BOOST_AUTO_TEST_CASE(operatorBraces_ThrowsOnIndexGreaterThanLength)
  {
    petrov::CompositeShape compositeShape;
    BOOST_CHECK_THROW(compositeShape[0], std::invalid_argument);
    BOOST_CHECK_THROW(compositeShape[1], std::invalid_argument);

    petrov::Rectangle item({1, 2}, 5, 10);
    compositeShape.addShape(&item);

    BOOST_CHECK_THROW(compositeShape[1], std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(operatorBraces_WorksCorrectly)
  {
    petrov::CompositeShape compositeShape;

    petrov::Rectangle item0({1, 2}, 5, 10);
    petrov::Circle item1({1, 2}, 5);
    compositeShape.addShape(&item0);
    compositeShape.addShape(&item1);

    BOOST_CHECK_EQUAL(&item0, compositeShape[0]);
    BOOST_CHECK_EQUAL(&item1, compositeShape[1]);
  }

  BOOST_AUTO_TEST_CASE(removeShape_ThrowsOnNoSuchShape)
  {
    petrov::CompositeShape compositeShape;

    petrov::Rectangle itemToRemove({1, 2}, 5, 10);

    BOOST_CHECK_THROW(compositeShape.removeShape(&itemToRemove), std::invalid_argument);

    petrov::Rectangle item0({1, 2}, 5, 10);
    compositeShape.addShape(&item0);

    BOOST_CHECK_THROW(compositeShape.removeShape(&itemToRemove), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(removeShape_WorksCorrectly)
  {
    petrov::Rectangle item0({5, 5}, 20, 30);
    petrov::Circle item1({5, 5}, 10);
    petrov::Circle itemToRemove({1, 5}, 30);
    petrov::Rectangle item3({10, 5}, 30, 10);
    petrov::Circle item4({5, 10}, 15);

    petrov::CompositeShape compositeShape;
    compositeShape.addShape(&item0);
    compositeShape.addShape(&item1);
    compositeShape.addShape(&itemToRemove);
    compositeShape.addShape(&item3);
    compositeShape.addShape(&item4);

    compositeShape.removeShape(&itemToRemove);

    BOOST_CHECK_EQUAL(4, compositeShape.getShapesCount());
    BOOST_CHECK_EQUAL(&item0, compositeShape.getShape(0));
    BOOST_CHECK_EQUAL(&item1, compositeShape.getShape(1));
    BOOST_CHECK_EQUAL(&item3, compositeShape.getShape(2));
    BOOST_CHECK_EQUAL(&item4, compositeShape.getShape(3));
  }

  BOOST_AUTO_TEST_CASE(getArea_ReturnsZeroOnNoShapesInside)
  {
    petrov::CompositeShape compositeShape;

    BOOST_CHECK_EQUAL(0, compositeShape.getArea());
  }

  BOOST_AUTO_TEST_CASE(getArea_WorksCorrectly)
  {
    petrov::Rectangle item0({5, 5}, 20, 10);
    petrov::Circle item1({5, 5}, 20);
    petrov::Rectangle item2({5, 5}, 20, 10);
    petrov::Circle item3({5, 5}, 10);

    petrov::CompositeShape compositeShape;
    compositeShape.addShape(&item0);
    compositeShape.addShape(&item1);
    compositeShape.addShape(&item2);
    compositeShape.addShape(&item3);

    double expectedArea = item0.getArea() + item1.getArea() + item2.getArea() + item3.getArea();
    BOOST_CHECK_CLOSE(expectedArea, compositeShape.getArea(), INACCURACY);
  }

  BOOST_AUTO_TEST_CASE(getFrameRect_ThrowsOnNoShapesInside)
  {
    petrov::CompositeShape compositeShape;

    BOOST_CHECK_THROW(compositeShape.getFrameRect(), std::logic_error);
  }

  BOOST_AUTO_TEST_CASE(getFrameRect_WorksCorrectly)
  {
    petrov::Rectangle item0({5, 5}, 20, 30);
    petrov::Circle item1({5, 5}, 10);
    petrov::Rectangle item2({10, 5}, 30, 10);
    petrov::Circle item3({5, 10}, 15);

    petrov::CompositeShape compositeShape;
    compositeShape.addShape(&item0);
    compositeShape.addShape(&item1);
    compositeShape.addShape(&item2);
    compositeShape.addShape(&item3);

    petrov::rectangle_t frameRect = compositeShape.getFrameRect();

    BOOST_CHECK_EQUAL(35, frameRect.width);
    BOOST_CHECK_EQUAL(35, frameRect.height);
    BOOST_CHECK_EQUAL(7.5, frameRect.pos.x);
    BOOST_CHECK_EQUAL(7.5, frameRect.pos.y);



    item0 = petrov::Rectangle({-1, 4}, 8, 6);
    item1 = petrov::Circle({2, 12}, 1);
    item2 = petrov::Rectangle({3, 5}, 4, 14);

    compositeShape = petrov::CompositeShape();
    compositeShape.addShape(&item0);
    compositeShape.addShape(&item1);
    compositeShape.addShape(&item2);

    frameRect = compositeShape.getFrameRect();

    BOOST_CHECK_EQUAL(10, frameRect.width);
    BOOST_CHECK_EQUAL(15, frameRect.height);
    BOOST_CHECK_EQUAL(0, frameRect.pos.x);
    BOOST_CHECK_EQUAL(5.5, frameRect.pos.y);
  }

  BOOST_AUTO_TEST_CASE(move_DoesNotingWhenNoShapesInside)
  {
    petrov::CompositeShape compositeShape = petrov::CompositeShape();

    BOOST_CHECK_NO_THROW(compositeShape.move(5, 10));

    BOOST_CHECK_NO_THROW(compositeShape.move({5, 10}));
  }

  BOOST_AUTO_TEST_CASE(move_Relatively_WorksCorrectly)
  {
    petrov::Rectangle item0({5, 5}, 20, 10);
    petrov::Circle item1({5, 5}, 20);
    petrov::Rectangle item2({5, 5}, 20, 10);
    petrov::Circle item3({5, 5}, 10);
    petrov::CompositeShape compositeShape;
    compositeShape.addShape(&item0);
    compositeShape.addShape(&item1);
    compositeShape.addShape(&item2);
    compositeShape.addShape(&item3);

    compositeShape.move(10, 15);

    BOOST_CHECK_EQUAL(15, compositeShape.getShape(0)->getCentre().x);
    BOOST_CHECK_EQUAL(20, compositeShape.getShape(0)->getCentre().y);
    BOOST_CHECK_EQUAL(15, compositeShape.getShape(1)->getCentre().x);
    BOOST_CHECK_EQUAL(20, compositeShape.getShape(1)->getCentre().y);
    BOOST_CHECK_EQUAL(15, compositeShape.getShape(2)->getCentre().x);
    BOOST_CHECK_EQUAL(20, compositeShape.getShape(2)->getCentre().y);
    BOOST_CHECK_EQUAL(15, compositeShape.getShape(3)->getCentre().x);
    BOOST_CHECK_EQUAL(20, compositeShape.getShape(3)->getCentre().y);

  }

  BOOST_AUTO_TEST_CASE(move_ToPoint_WorksCorrectly)
  {
    petrov::Rectangle item0({5, 5}, 20, 10);
    petrov::Circle item1({5, 5}, 20);
    petrov::Rectangle item2({5, 5}, 20, 10);
    petrov::Circle item3({5, 5}, 10);

    petrov::CompositeShape compositeShape;
    compositeShape.addShape(&item0);
    compositeShape.addShape(&item1);
    compositeShape.addShape(&item2);
    compositeShape.addShape(&item3);

    compositeShape.move({15, 20});

    BOOST_CHECK_EQUAL(15, compositeShape.getShape(0)->getCentre().x);
    BOOST_CHECK_EQUAL(20, compositeShape.getShape(0)->getCentre().y);
    BOOST_CHECK_EQUAL(15, compositeShape.getShape(1)->getCentre().x);
    BOOST_CHECK_EQUAL(20, compositeShape.getShape(1)->getCentre().y);
    BOOST_CHECK_EQUAL(15, compositeShape.getShape(2)->getCentre().x);
    BOOST_CHECK_EQUAL(20, compositeShape.getShape(2)->getCentre().y);
    BOOST_CHECK_EQUAL(15, compositeShape.getShape(3)->getCentre().x);
    BOOST_CHECK_EQUAL(20, compositeShape.getShape(3)->getCentre().y);
  }

  BOOST_AUTO_TEST_CASE(getCenter_ThrowsOnNoShapesInside)
  {
    petrov::CompositeShape compositeShape;

    BOOST_CHECK_THROW(compositeShape.getCentre(), std::logic_error);
  }

  BOOST_AUTO_TEST_CASE(getCenter_WorksCorrectly)
  {
    petrov::Rectangle item0 = petrov::Rectangle({3, 2}, 4, 2);
    petrov::Circle item1 = petrov::Circle({4, 6}, 1);
    petrov::Rectangle item2 = petrov::Rectangle({9, 4}, 4, 2);

    petrov::CompositeShape compositeShape;
    compositeShape.addShape(&item0);
    compositeShape.addShape(&item1);
    compositeShape.addShape(&item2);

    BOOST_CHECK_EQUAL(6, compositeShape.getCentre().x);
    BOOST_CHECK_EQUAL(4, compositeShape.getCentre().y);
  }

  BOOST_AUTO_TEST_CASE(scale_DoesNotingWhenNoShapesInside)
  {
    petrov::CompositeShape compositeShape;

    BOOST_CHECK_NO_THROW(compositeShape.scale(10));
  }


  BOOST_AUTO_TEST_CASE(scale_WorksCorrectly)
  {
    petrov::Rectangle item0 = petrov::Rectangle({3, 2}, 4, 2);
    petrov::Circle item1 = petrov::Circle({4, 6}, 1);
    petrov::Rectangle item2 = petrov::Rectangle({9, 4}, 4, 2);

    petrov::CompositeShape compositeShape;
    compositeShape.addShape(&item0);
    compositeShape.addShape(&item1);
    compositeShape.addShape(&item2);

    compositeShape.scale(2);

    BOOST_CHECK_EQUAL(8, compositeShape.getShape(0)->getFrameRect().width);
    BOOST_CHECK_EQUAL(4, compositeShape.getShape(0)->getFrameRect().height);
    BOOST_CHECK_EQUAL(0, compositeShape.getShape(0)->getCentre().x);
    BOOST_CHECK_EQUAL(0, compositeShape.getShape(0)->getCentre().y);

    BOOST_CHECK_EQUAL(4, compositeShape.getShape(1)->getFrameRect().width);
    BOOST_CHECK_EQUAL(4, compositeShape.getShape(1)->getFrameRect().height);
    BOOST_CHECK_EQUAL(2, compositeShape.getShape(1)->getCentre().x);
    BOOST_CHECK_EQUAL(8, compositeShape.getShape(1)->getCentre().y);

    BOOST_CHECK_EQUAL(8, compositeShape.getShape(2)->getFrameRect().width);
    BOOST_CHECK_EQUAL(4, compositeShape.getShape(2)->getFrameRect().height);
    BOOST_CHECK_EQUAL(12, compositeShape.getShape(2)->getCentre().x);
    BOOST_CHECK_EQUAL(4, compositeShape.getShape(2)->getCentre().y);
  }

  BOOST_AUTO_TEST_CASE(rotate_WorksCorrectly)
  {
    petrov::Rectangle rectangle = petrov::Rectangle({4.5, 2.5}, 5, 3);
    petrov::Circle circle = petrov::Circle({7, 4}, 1);
    petrov::CompositeShape compositeShape;
    compositeShape.addShape(&rectangle);
    compositeShape.addShape(&circle);

    petrov::point_t centreBeforeRotation = compositeShape.getCentre();
    compositeShape.rotate(90);

    BOOST_CHECK_EQUAL(centreBeforeRotation.x, compositeShape.getCentre().x);
    BOOST_CHECK_EQUAL(centreBeforeRotation.y, compositeShape.getCentre().y);
    BOOST_CHECK_CLOSE(4, compositeShape.getFrameRect().width,INACCURACY);
    BOOST_CHECK_CLOSE(6, compositeShape.getFrameRect().height,INACCURACY);
    BOOST_CHECK_CLOSE(5.5, rectangle.getCentre().x, INACCURACY);
    BOOST_CHECK_CLOSE(2.5, rectangle.getCentre().y, INACCURACY);
    BOOST_CHECK_CLOSE(3, rectangle.getFrameRect().width, INACCURACY);
    BOOST_CHECK_CLOSE(5, rectangle.getFrameRect().height, INACCURACY);
    BOOST_CHECK_CLOSE(4, circle.getCentre().x, INACCURACY);
    BOOST_CHECK_CLOSE(5, circle.getCentre().y, INACCURACY);
  }

BOOST_AUTO_TEST_SUITE_END()
