#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>
#include <iostream>

#include "circle.hpp"
#include "rectangle.hpp"

const double INACCURACY = 0.0001;

BOOST_AUTO_TEST_SUITE(CircleTest)

  BOOST_AUTO_TEST_CASE(constructor_ThrowsOnInvalidRadius)
  {
    BOOST_CHECK_THROW(petrov::Circle({5, 5}, -1), std::invalid_argument);
    BOOST_CHECK_THROW(petrov::Circle({5, 5}, -0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(move_WorksCorrectly)
  {
    petrov::point_t centre{10, 20};
    double radius = 15;
    petrov::Circle circle = petrov::Circle(centre, radius);

    double dx = 40;
    double dy = 30;
    circle.move(dx, dy);

    BOOST_CHECK_EQUAL(centre.x + dx, circle.getCentre().x);
    BOOST_CHECK_EQUAL(centre.y + dy, circle.getCentre().y);

    double x = 40;
    double y = 30;
    petrov::point_t destinationPoint {x, y};
    circle.move(destinationPoint);

    BOOST_CHECK_EQUAL(x, circle.getCentre().x);
    BOOST_CHECK_EQUAL(y, circle.getCentre().y);
  }

  BOOST_AUTO_TEST_CASE(move_DoesNotChangeDimentions)
  {
    petrov::point_t centre{10, 20};
    double radius = 15;
    petrov::Circle circle = petrov::Circle(centre, radius);

    double dx = 40;
    double dy = 30;
    circle.move(dx, dy);

    BOOST_CHECK_EQUAL(radius, circle.getRadius());

    double x = 40;
    double y = 30;
    petrov::point_t destinationPoint {x, y};
    circle.move(destinationPoint);

    BOOST_CHECK_EQUAL(radius, circle.getRadius());
  }

  BOOST_AUTO_TEST_CASE(scale_AreaGrowsInSquarePropotion)
  {
    petrov::Circle circle = petrov::Circle({1, 2}, 10);
    double scaleFactor = 5;

    double areaBeforeScaling = circle.getArea();
    circle.scale(scaleFactor);
    double areaAfterScaling = circle.getArea();

    BOOST_CHECK_CLOSE(scaleFactor * scaleFactor,
                      areaAfterScaling / areaBeforeScaling,
                      INACCURACY);
  }

  BOOST_AUTO_TEST_CASE(rotate_NothingChanges)
  {
    petrov::point_t centre{1, 2};
    double radius = 10;
    petrov::Circle circle = petrov::Circle(centre, radius);

    double areaBeforeRotation = circle.getArea();
    petrov::rectangle_t frameRectBeforeRotation = circle.getFrameRect();

    circle.rotate(40);

    BOOST_CHECK_EQUAL(centre.x, circle.getCentre().x);
    BOOST_CHECK_EQUAL(centre.y, circle.getCentre().y);
    BOOST_CHECK_EQUAL(radius, circle.getRadius());
    BOOST_CHECK_EQUAL(areaBeforeRotation, circle.getArea());
    BOOST_CHECK_EQUAL(frameRectBeforeRotation.height, circle.getFrameRect().height);
    BOOST_CHECK_EQUAL(frameRectBeforeRotation.width, circle.getFrameRect().width);
    BOOST_CHECK_EQUAL(frameRectBeforeRotation.pos.x, circle.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(frameRectBeforeRotation.pos.y, circle.getFrameRect().pos.y);
  }

BOOST_AUTO_TEST_SUITE_END()

