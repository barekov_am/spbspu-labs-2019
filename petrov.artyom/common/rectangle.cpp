#include "rectangle.hpp"
#include <stdexcept>
#include <limits>
#include <math.h>

petrov::Rectangle::Rectangle(rectangle_t rectangle) :
  rectangle_(rectangle),
  angle_(0)
{
  if (rectangle.width <= 0)
  {
    throw std::invalid_argument("Width must be greater than 0");
  }
  if (rectangle.height <= 0)
  {
    throw std::invalid_argument("Height must be greater than 0");
  }
}

petrov::Rectangle::Rectangle(point_t centre, double width, double height) :
  Rectangle({width, height, centre})
{

}

double petrov::Rectangle::getArea() const
{
  return rectangle_.height * rectangle_.width;
}

petrov::rectangle_t petrov::Rectangle::getFrameRect() const
{

  double centreX = getCentre().x;
  double centreY = getCentre().y;

  point_t points[4];

  points[0] = {
    centreX + rectangle_.width / 2,
    centreY + rectangle_.height / 2
  };
  points[1] = {
      centreX - rectangle_.width / 2,
      centreY + rectangle_.height / 2
  };
  points[2] = {
      centreX - rectangle_.width / 2,
      centreY - rectangle_.height / 2
  };
  points[3] = {
      centreX + rectangle_.width / 2,
      centreY - rectangle_.height / 2
  };

  double maxX = std::numeric_limits<double>::min();
  double maxY = std::numeric_limits<double>::min();
  double minX = std::numeric_limits<double>::max();
  double minY = std::numeric_limits<double>::max();

  for (point_t point : points)
  {
    maxX = std::max(
        maxX,
        centreX + (point.x - centreX) * std::cos(angle_) - (point.y - centreY) * std::sin(angle_)
        );
    minX = std::min(
        minX,
        centreX + (point.x - centreX) * std::cos(angle_) - (point.y - centreY) * std::sin(angle_)
    );
    maxY = std::max(
        maxY,
        centreY + (point.x - centreX) * std::sin(angle_) + (point.y - centreY) * std::cos(angle_)
    );
    minY = std::min(
        minY,
        centreY + (point.x - centreX) * std::sin(angle_) + (point.y - centreY) * std::cos(angle_)
    );
  }

  point_t centre {(maxX + minX) / 2, (maxY + minY) / 2};
  double width = maxX - minX;
  double height = maxY - minY;

  return rectangle_t{width, height, centre};
}

void petrov::Rectangle::move(const point_t & centre)
{
  rectangle_.pos = centre;
}

void petrov::Rectangle::move(double dx, double dy)
{
  rectangle_.pos.x += dx;
  rectangle_.pos.y += dy;
}

void petrov::Rectangle::scale(double scaleCoef)
{
  rectangle_.width *= scaleCoef;
  rectangle_.height *= scaleCoef;
}

petrov::point_t petrov::Rectangle::getCentre() const
{
  return rectangle_.pos;
}

double petrov::Rectangle::getWidth() const
{
  return rectangle_.width;
}

double petrov::Rectangle::getHeight() const
{
  return rectangle_.height;
}

void petrov::Rectangle::rotate(double angle)
{
  angle_ += angle * M_PI / 180;
}
