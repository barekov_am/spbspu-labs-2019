#include <iostream>
#include <sstream>
#include <algorithm>
#include "token_t.hpp"
#include "textInput.hpp"

void formatAndPrint(std::vector<token_t> &text_, size_t outStringWidth_)
{
  if (text_.empty())
  {
    return;
  }
  if ((text_.front().type == token_t::PUNCT) || (text_.front().type == token_t::DASH))
  {
    throw std::invalid_argument("Begin with punctuation");
  }

  for (auto it = text_.begin(); it != text_.end() - 1; ++it)
  {
    if (it->type == token_t::PUNCT)
    {
      if ((it->value == ",") && ((it+1)->type == token_t::DASH))
      {
        (it+1)->value.insert(0, " ");
      }
      else if ((it+1)->type == token_t::PUNCT || (it+1)->type == token_t::DASH)
      {
        throw std::invalid_argument("2 or more punctuation symbols nearly");
      }
    }
  }

  std::for_each(text_.begin(), text_.end(), [&](token_t &token)
  {
    if (isalpha(token.value[0]) || isdigit(token.value[0]) || (token.value[0] == '+') || ((token.value[0] == '-')))
    {
      token.value.insert(0, " ");
    }
  });

  size_t tmpWidth = 0;
  bool isBeginOfLine = true;

  for (auto it = text_.begin(); it != text_.end(); ++it)
  {
    if (isBeginOfLine)
    {
      it->value.erase(0, 1);
      isBeginOfLine = false;
    }

    if (((it != text_.end() - 1)) && ((it+1)->type == token_t::DASH || (it+1)->type == token_t::PUNCT))
      if (it->value.size() + (it+1)->value.size() + tmpWidth > outStringWidth_)
      {
        std:: cout << "\n" << it->value.erase(0, 1);
        tmpWidth = it->value.size();
        continue;
      }

    if ((tmpWidth + it->value.size()) < outStringWidth_)
    {
      std::cout << it->value;
      tmpWidth += it->value.size();
      isBeginOfLine = false;
    }
    else if (((tmpWidth + it->value.size()) > outStringWidth_))
    {
      std::cout << "\n" << it->value.erase(0, 1);
      tmpWidth = it->value.size();
      isBeginOfLine = false;
    }
    else if (((tmpWidth + it->value.size()) == outStringWidth_))
    {
      std::cout << it->value << "\n";
      tmpWidth = 0;
      isBeginOfLine = true;
    }
  }
  std::cout << "\n";
}
