#include <iostream>
#include <sstream>
#include <algorithm>
#include "textInput.hpp"

void TextInput::readWord(char c)
{
  std::string tmp;
  tmp.push_back(c);
  while (std::isalpha(std::cin.peek()) || (std::cin.peek() == '-'))
  {
    char ch = static_cast<char>(std::cin.get());
    if (ch == '-' && std::cin.peek() == '-')
    {
      throw std::invalid_argument("More than one '-' nearly");
    }
    tmp.push_back(ch);
  }
  if (tmp.length() > WORD_MAX_LEN)
  {
    throw std::invalid_argument("Length of number more than 20");
  }
  text_.push_back(token_t{tmp, token_t::WORD});
}

void TextInput::readNumber(char c)
{
  std::string tmp;
  tmp.push_back(c);
  int counterOfDots = 0;
  while (std::isdigit(std::cin.peek()) || (std::cin.peek() == '.'))
  {
    char ch = static_cast<char>(std::cin.get());
    if (ch == '.')
    {
      ++counterOfDots;
      if (counterOfDots > 1)
      {
        throw std::invalid_argument("More than one dot in number");
      }
    }
    tmp.push_back(ch);
  }
  if (tmp.length() > WORD_MAX_LEN)
  {
    throw std::invalid_argument("Length of number more than 20");
  }
  text_.push_back(token_t{tmp, token_t::NUMBER});
}

void TextInput::readDash(char c)
{
  std::string tmp;
  tmp.push_back(c);
  while (std::cin.peek() == '-')
  {
    tmp.push_back(static_cast<char>(std::cin.get()));
  }
  if (tmp.length() != DASH_MAX_LEN)
  {
    throw std::invalid_argument("Length of Dash != 3");
  }
  text_.push_back(token_t{tmp, token_t::DASH});
}

std::vector<token_t> TextInput::readText()
{
  while (std::cin)
  {
    std::cin >> std::ws;
    char firstChar = static_cast<char>(std::cin.get());
    char secondChar = static_cast<char>(std::cin.peek());

    if (std::isalpha(firstChar))
    {
      readWord(firstChar);
    }
    else if ((std::isdigit(firstChar)) || ((firstChar == '+' || firstChar == '-') && std::isdigit(secondChar)))
    {
      readNumber(firstChar);
    }
    else if (std::ispunct(firstChar))
    {
      if (firstChar == '-')
      {
        readDash(firstChar);
      }
      else
      {
        text_.push_back(token_t{std::string(1, firstChar), token_t::PUNCT});
      }
    }
  }
  return text_;
}
