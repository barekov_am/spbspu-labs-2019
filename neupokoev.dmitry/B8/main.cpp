#include <iostream>
#include <stdexcept>
#include "textInput.hpp"

const size_t MIN_WIDTH = 25;
const size_t DEFAULT_WIDTH = 40;

int main(int argc, char * argv[])
{
  try
  {
    if ((argc != 1) && (argc != 3))
    {
      std::cerr << "Wrong amount of arguments";
      return 1;
    }

    size_t width = DEFAULT_WIDTH;

    if (argc == 3)
    {
      width = std::stoi(argv[2]);
      if ((std::string(argv[1]) != "--line-width") || (width < MIN_WIDTH))
      {
        std::cerr << "Wrong arguments";
        return 1;
      }
    }

    TextInput text;
    std::vector<token_t> textOut_ = text.readText();
    formatAndPrint(textOut_, width);

  }
  catch (const std::exception &exception)
  {
    std::cerr << exception.what();
    return 2;
  }

  return 0;
}
