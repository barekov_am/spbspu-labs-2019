#ifndef TEXT_INPUT_HPP
#define TEXT_INPUT_HPP

#include <string>
#include <vector>
#include "token_t.hpp"

const std::size_t WORD_MAX_LEN = 20;
const std::size_t DASH_MAX_LEN = 3;


class TextInput
{
public:
  std::vector<token_t> readText();

private:
  void readWord(char c);
  void readNumber(char c);
  void readDash(char c);
  std::vector<token_t> text_;
};

void formatAndPrint(std::vector<token_t> &text_, size_t width);

#endif
