#ifndef TOKEN_T_HPP
#define TOKEN_T_HPP

#include <string>

struct token_t
{
  enum token_type
  {
    WORD,
    NUMBER,
    PUNCT,
    DASH,
  };
  std::string value;
  token_type type;
};
#endif
