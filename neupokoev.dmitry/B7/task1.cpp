#include <iostream>
#include <vector>
#include <iterator>
#include <functional>
#include <algorithm>
#include <cmath>

void task1()
{
  std::vector<double> vector{std::istream_iterator<double>(std::cin), std::istream_iterator<double>()};

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::ios_base::failure("Failed during reading\n");
  }

  std::transform(vector.begin(), vector.end(), vector.begin(), std::bind(std::multiplies<double>(), std::placeholders::_1, M_PI));
  std::copy(vector.begin(), vector.end(), std::ostream_iterator<double>(std::cout, "\n"));
}
