#include <limits>
#include <stdexcept>
#include "statistics.hpp"

Statistics::Statistics() :
  count_(0),
  max_(std::numeric_limits<int>::min()),
  min_(std::numeric_limits<int>::max()),
  positive_(0),
  negative_(0),
  oddSum_(0),
  evenSum_(0),
  first_(0),
  equals_(false),
  mean_(0.0)
{}

void Statistics::operator()(int num)
{
  count_++;
  if (num > max_)
  {
    max_ = num;
  }

  if (num < min_)
  {
    min_ = num;
  }

  if (num > 0)
  {
    positive_++;
  }
  else if (num < 0)
  {
    negative_++;
  }

  if (num % 2)
  {
    oddSum_ += num;
  }
  else
  {
    evenSum_ += num;
  }

  if (count_ == 1)
  {
    first_ = num;
  }

  equals_ = (first_ == num);
  mean_ = static_cast<double>(evenSum_ + oddSum_) / count_;
}

void Statistics::print(std::ostream &stream)
{
  if (count_ == 0)
  {
    stream << "No Data\n";
    return;
  }

  stream << "Max: " << max_ << std::endl
         << "Min: " << min_ << std::endl
         << "Mean: " << mean_ << std::endl
         << "Positive: " << positive_ << std::endl
         << "Negative: " << negative_ << std::endl
         << "Odd Sum: " << oddSum_ << std::endl
         << "Even Sum: " << evenSum_ << std::endl
         << "First/Last Equal: ";
  equals_ ? stream <<"yes\n" : stream <<"no\n";
}
