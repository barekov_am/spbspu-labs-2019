#include <algorithm>
#include <functional>
#include <iterator>
#include "statistics.hpp"

int main()
{
  try
  {
    Statistics stat_ = std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), Statistics());

    if (!std::cin.eof())
    {
      std::cerr << "Failed during reading data";
      return 1;
    }

    stat_.print(std::cout);
  }
  catch (const std::exception &exception)
  {
    std::cerr << exception.what();
    return 2;
  }

  return 0;
}
