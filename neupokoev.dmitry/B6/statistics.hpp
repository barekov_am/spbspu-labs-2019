#ifndef B6_STATISTICS_HPP
#define B6_STATISTICS_HPP

#include <iostream>
#include <cstdio>

class Statistics
{
  public:
    Statistics();
    void operator()(int num);
    void print(std::ostream &stream);

  private:
    size_t count_;
    int max_;
    int min_;
    size_t positive_;
    size_t negative_;
    long long int oddSum_;
    long long int evenSum_;
    int first_;
    bool equals_;
    double  mean_;
};

#endif
