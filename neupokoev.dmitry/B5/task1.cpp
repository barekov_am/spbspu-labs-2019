#include <algorithm>
#include <iostream>
#include <string>
#include <set>
#include <vector>
#include <sstream>

void task1()
{
  std::string line;
  std::set<std::string> words;

  while (std::getline(std::cin, line))
  {
    std::stringstream stream(line);
    std::string word;
    while (stream >> word)
    {
      words.insert(word);
    }
  }

  for (auto i : words)
  {
    std::cout << i << '\n';
  }
}
