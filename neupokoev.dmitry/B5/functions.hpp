#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <string>
#include <algorithm>

#include "shape.hpp"

Shape readPoints(std::string &line, std::size_t vertices);
void readWords(std::vector<Shape> &vector, std::string &line);
void recieveInfo(ShapeInfo &shapeInfo_, std::vector<Shape> &shapeCont);
bool isSquare(const Shape &shape);
bool isRectangle(const Shape &shape);
bool isLess(const Shape &lhs, const Shape &rhs);
void removePentagons(std::vector<Shape> &shapes);
void createVectorPoints(std::vector<Shape> &shapeCont, std::vector<Point_t> points);
void printInfo(std::vector<Shape> &shapeCont, std::vector<Point_t> points, ShapeInfo &shapeInfo_);

#endif
