#ifndef COMANDS_HANDLER
#define COMANDS_HANDLER

#include <string>
#include "phoneBookInterface.hpp"

std::string getName(std::string &name);
std::string getNumber(std::string &number);
std::string getMarkName(std::string &markname);
void add(PhoneBookInterface &phBookInter, std::stringstream &stream);
void store(PhoneBookInterface &phBookInter, std::stringstream &stream);
void insert(PhoneBookInterface &phBookInter, std::stringstream &stream);
void deleteRecord(PhoneBookInterface &phBookInter, std::stringstream &stream);
void show(PhoneBookInterface &phBookInter, std::stringstream &stream);
void move(PhoneBookInterface &phBookInter, std::stringstream &stream);


#endif
