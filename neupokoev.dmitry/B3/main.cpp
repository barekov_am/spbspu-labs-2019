#include <iostream>

void task1();
void task2();

int main(int argc, char * argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Incorrect number of arguments.\n";
      return 1;
    }


    switch (std::stoi(argv[1]))
    {
    case 1:
    {
      task1();
      break;
    }
    case 2:
    {
      task2();
      break;
    }
    default:
    {
      std::cerr << "Wrong number of task\n";
      return 1;
    }
    }
  }

  catch (const std::exception &exception)
  {
    std::cerr << exception.what();
    return 2;
  }

  return 0;
}
