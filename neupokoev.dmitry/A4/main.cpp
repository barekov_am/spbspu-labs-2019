#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "partition.hpp"




int main()
{
  neupokoev::Circle myCircle1({ 5.0, 4.0 }, 6.0);
  neupokoev::Rectangle myRectangle2({ { -7.0, 4.0 }, 3.0, 1.0 });
  neupokoev::Triangle myTriangle1({ 1.0, 10.0 }, { 5.0, 16.0 }, { 10.0, 0.0 });

  neupokoev::CompositeShape::shape_ptr part1 = std::make_shared<neupokoev::Rectangle>(myRectangle2);
  neupokoev::CompositeShape::shape_ptr part2 = std::make_shared<neupokoev::Triangle>(myTriangle1);
  neupokoev::CompositeShape::shape_ptr part3 = std::make_shared<neupokoev::Circle>(myCircle1);

  myTriangle1.writeParameters();
  myTriangle1.rotate(390);
  myTriangle1.writeParameters();

  neupokoev::CompositeShape composite_shape;
  composite_shape.add(part1);
  composite_shape.add(part2);
  composite_shape.add(part3);

  composite_shape.move({ -7, 4 });
  composite_shape.writeParameters();

  composite_shape.move(2, 9);
  composite_shape.writeParameters();

  composite_shape.rotate(30);
  composite_shape.writeParameters();

  composite_shape.scale(2);
  composite_shape.writeParameters();

  composite_shape.remove(1);
  composite_shape.writeParameters();

  neupokoev::Matrix matrix = neupokoev::split(composite_shape);
  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    for (size_t j = 0; j < matrix.getNumberOfFigures(); j++)
    {
      if (matrix[i][j] != nullptr)
      {
        std::cout << "Layer no. " << i << ":\n" << "Figure no. " << j << ":\n";
        std::cout << "Position: (" << matrix[i][j]->getPos().x << "; "
          << matrix[i][j]->getPos().y << ")\n";
      }
    }
  }

  return 0;
}
