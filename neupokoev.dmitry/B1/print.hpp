#ifndef PRINT_HPP
#define PRINT_HPP

#include <iostream>

template <typename T>
void print(const T& container)
{
  for (const auto& element : container)
  {
    std::cout << element << " ";
  }
  std::cout << std::endl;
}

#endif
