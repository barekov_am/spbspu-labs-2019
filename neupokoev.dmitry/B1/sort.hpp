#ifndef SORT_HPP
#define SORT_HPP

#include <functional>
#include "accesses.hpp"

bool getDirection(const char* direction);

template <template <class T> class Access, typename T>
void selectionSort(T& collection, bool direction)
{
  const auto begin = Access<T>::begin(collection);
  const auto end = Access<T>::end(collection);

  for (auto i = begin; i != end; ++i)
  {
    auto temp = i;
    for (auto j = Access<T>::next(i); j != end; ++j)
    {
      if ((Access<T>::element(collection, j) < Access<T>::element(collection, temp)) == direction)
      {
        temp = j;
      }
    }

    if (temp != i)
    {
      std::swap(Access<T>::element(collection, temp), Access<T>::element(collection, i));
    }
  }
}

#endif
