#ifndef B4_DATA_STRUCT_HPP
#define B4_DATA_STRUCT_HPP

#include <string>
#include <vector>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

#endif

