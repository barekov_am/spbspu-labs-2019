#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include "dataStruct.hpp"

void getData(std::vector<DataStruct> &vector);
void sortVector(std::vector<DataStruct> &vector);
void printVector(std::vector<DataStruct> &vector);

#endif

