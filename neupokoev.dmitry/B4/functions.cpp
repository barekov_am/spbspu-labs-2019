#include <algorithm>
#include <iostream>
#include <sstream>
#include "dataStruct.hpp"
#include "functions.hpp"

const int MAX = 5;
const int MIN = -5;

int getKey(std::stringstream &line)
{
  int key = MAX + 1;
  line >> key;
  if ((key > MAX) || (key < MIN))
  {
    throw std::invalid_argument("Wrong argument");
  }

  std::string string;
  std::getline(line, string, ',');
  if (!string.empty() || line.eof())
  {
    throw std::invalid_argument("Wrong argument");
  }

  return key;
}

void getData(std::vector<DataStruct> &vector)
{
  int key1;
  int key2;
  std::string str;

  std::string line;
  std::getline(std::cin, line);
  std::stringstream stream(line);

  key1 = getKey(stream);
  key2 = getKey(stream);
  stream >> std::ws;
  std::getline(stream, str);

  if (str.empty())
  {
    throw std::invalid_argument("Wrong argument");
  }

  vector.push_back({key1, key2, str});
}

bool isLess(const DataStruct &lhs, const DataStruct &rhs)
{
  if (lhs.key1 < rhs.key1)
  {
    return true;
  }
  if (lhs.key1 == rhs.key1)
  {
    if (lhs.key2 < rhs.key2)
    {
      return true;
    }
    else if ((lhs.key2 == rhs.key2) && (lhs.str.size() < rhs.str.size()))
    {
      return true;
    }
  }
  return false;
}

void sortVector(std::vector<DataStruct> &vector)
{
  if (!vector.empty())
  {
    std::sort(vector.begin(), vector.end(), isLess);
  }
}

void printVector(std::vector<DataStruct> &vector)
{
  for (auto i = vector.begin(); i != vector.end(); i++)
  {
    std::cout << i->key1 << "," << i->key2 << "," << i->str << "\n";
  }

}

