#include <iostream>

void task1();
void task2();

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Incorrect number of arguments.\n";
      return 1;
    }

    char *endPtr = nullptr;
    int num = std::strtol(argv[1], &endPtr, 10);

    if (*endPtr != 0x00)
    {
      std::cerr << "Incorrect argument.\n";
      return 1;
    }

    switch (num)
    {
      case 1:
      {
        task1();
        break;
      }
      case 2:
      {
        task2();
        break;
      }
      default:
      {
        std::cerr << "Wrong number of task\n";
        return 1;
      }
    }
  }

  catch (const std::exception &exception)
  {
    std::cerr << exception.what();
    return 1;
  }

  return 0;
}
