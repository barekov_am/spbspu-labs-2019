#include <iostream>
#include <list>

const int MIN_NUM = 1;
const int MAX_NUM = 20;
const int MAX_SIZE = 20;

void task2()
{
  std::list<int> list;
  int num = 0;

  while (std::cin >> num)
  {
    if ((num > MAX_NUM) || (num < MIN_NUM))
    {
      throw std::invalid_argument("Number should be between 0 and 20");
    }

    if (list.size() + 1 > MAX_SIZE)
    {
      throw std::out_of_range("List max size is 20");
    }

    list.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Failed during reading");
  }

  while (!list.empty())
  {
    std::cout << list.front() << " ";
    list.pop_front();
    if (list.empty())
    {
      break;
    }
    std::cout << list.back() << " ";
    list.pop_back();
  }

  std::cout << "\n";
}
