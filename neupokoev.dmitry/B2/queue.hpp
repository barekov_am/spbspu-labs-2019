#ifndef QUEUE_WITH_PRIORITY_HPP
#define QUEUE_WITH_PRIORITY_HPP

#include <stdexcept>
#include <list>

template <typename T>
class QueueWithPriority
{
public:

  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  T get();
  void add(ElementPriority priority, const T &element);
  void accelerate();

private:
  struct queueElement_t
  {
    T element;
    ElementPriority priority;
  };
  std::list<queueElement_t> queue_;
};


template <typename T>
void QueueWithPriority<T>::add(ElementPriority priority, const T &element)
{
  queueElement_t queueElement;
  queueElement.priority = priority;
  queueElement.element = element;

  decltype(queue_.begin()) i = queue_.begin();
  while (i != queue_.end())
  {
    if ((*i).priority < priority) break;
    i++;
  }
  queue_.insert(i, queueElement);
}

template <typename T>
T QueueWithPriority<T>::get()
{
  if (queue_.empty())
  {
    throw std::invalid_argument("Cant get element from the empty queue ");
  }

  T tmp = queue_.front().element;
  queue_.pop_front();
  return tmp;
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  auto highEnd = queue_.begin();
  auto lowBegin = --queue_.end();

  while ((lowBegin != queue_.begin()) && ((*lowBegin).priority == ElementPriority::LOW))
  {
    lowBegin--;
  }
  lowBegin++;

  while ((highEnd != queue_.end()) && ((*highEnd).priority == ElementPriority::HIGH))
  {
    highEnd++;
  }

  for (auto i = lowBegin; i != queue_.end(); i++)
  {
    (*i).priority = ElementPriority::HIGH;
  }

  queue_.splice(highEnd, queue_, lowBegin, queue_.end());
}

#endif
