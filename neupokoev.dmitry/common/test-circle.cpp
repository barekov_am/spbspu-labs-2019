#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteCircle)

const double accuracy = 0.01;

BOOST_AUTO_TEST_CASE(moveCircleTest)
{
  neupokoev::Circle testCircle({ 3, 4 }, 5);
  const neupokoev::rectangle_t InitialFrameRect = testCircle.getFrameRect();
  const double InitialArea = testCircle.getArea();

  testCircle.move({ 7, 8 });
  BOOST_CHECK_CLOSE(InitialFrameRect.width, testCircle.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(InitialFrameRect.height, testCircle.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(InitialArea, testCircle.getArea(), accuracy);

  testCircle.move(-5, -6);
  BOOST_CHECK_CLOSE(InitialFrameRect.width, testCircle.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(InitialFrameRect.height, testCircle.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(InitialArea, testCircle.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(areaAfterScalingCircleTest)
{
  neupokoev::Circle testCircle({ 3, 4 }, 5);
  const double InitialArea = testCircle.getArea();
  const double scaleFactor = 4;

  testCircle.scale(scaleFactor);
  BOOST_CHECK_CLOSE(testCircle.getArea(), scaleFactor * scaleFactor * InitialArea, accuracy);
}


BOOST_AUTO_TEST_CASE(invalidScaleArgumentsCircle)
{
  BOOST_CHECK_THROW(neupokoev::Circle({ 1, 2 }, -3), std::invalid_argument);

  neupokoev::Circle testCircle({ 1, 2 }, 3);
  BOOST_CHECK_THROW(testCircle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testCircleRotation)
{
  neupokoev::Circle testCircle({ 5.0, 5.0 }, 2.0);
  const double areaBefore = testCircle.getArea();
  const neupokoev::rectangle_t frameRectBefore = testCircle.getFrameRect();

  double angle = 90;
  testCircle.rotate(angle);
  double areaAfter = testCircle.getArea();
  neupokoev::rectangle_t frameRectAfter = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, accuracy);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, accuracy);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, accuracy);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, accuracy);

  angle = -150;
  testCircle.rotate(angle);
  areaAfter = testCircle.getArea();
  frameRectAfter = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, accuracy);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, accuracy);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, accuracy);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, accuracy);
}


BOOST_AUTO_TEST_SUITE_END()
