#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteTriangle)

const double accuracy = 0.01;

BOOST_AUTO_TEST_CASE(moveTriangleTest)
{
  neupokoev::Triangle testTriangle({ 0, 0 }, { 4, 1 }, { 3, 7 });
  const neupokoev::rectangle_t InitialFrameRect = testTriangle.getFrameRect();
  const double InitialArea = testTriangle.getArea();

  testTriangle.move({ 7, 8 });
  BOOST_CHECK_CLOSE(InitialFrameRect.width, testTriangle.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(InitialFrameRect.height, testTriangle.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(InitialArea, testTriangle.getArea(), accuracy);

  testTriangle.move(-5, -6);
  BOOST_CHECK_CLOSE(InitialFrameRect.width, testTriangle.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(InitialFrameRect.height, testTriangle.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(InitialArea, testTriangle.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(areaAfterScalingTriangleTest)
{
  neupokoev::Triangle testTriangle({ 0, 0 }, { 4, 1 }, { 3, 7 });
  const double InitialArea = testTriangle.getArea();
  const double scaleFactor = 4;

  testTriangle.scale(scaleFactor);
  BOOST_CHECK_CLOSE(testTriangle.getArea(), scaleFactor * scaleFactor * InitialArea, accuracy);
}

BOOST_AUTO_TEST_CASE(invalidScaleArgumentsTriangle)
{
  neupokoev::Triangle testTriangle({ 0, 0 }, { 4, 1 }, { 3, 7 });
  BOOST_CHECK_THROW(testTriangle.scale(-5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidPointsTriangle)
{
  BOOST_CHECK_THROW(neupokoev::Triangle({ 0, 0 }, { 4, 0 }, { 8, 0 }), std::invalid_argument);
  BOOST_CHECK_THROW(neupokoev::Triangle({ 0, 0 }, { 0, 0 }, { 0, 0 }), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
