#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <cmath>
#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <utility>

const double FullCircle = 360.0;

neupokoev::CompositeShape::CompositeShape() :
  shapeCount_(0),
  angle_(0.0)
{ }

neupokoev::CompositeShape::CompositeShape(const CompositeShape &source) :
  shapeCount_(source.shapeCount_),
  angle_(source.angle_),
  arrayOfShapes_(std::make_unique<shape_ptr[]>(source.shapeCount_))
{
  for (size_t i = 0; i < shapeCount_; i++)
  {
    arrayOfShapes_[i] = source.arrayOfShapes_[i];
  }
}

neupokoev::CompositeShape::CompositeShape(CompositeShape &&source) :
  shapeCount_(source.shapeCount_),
  angle_(source.angle_),
  arrayOfShapes_(std::move(source.arrayOfShapes_))
{
  source.shapeCount_ = 0;
}

neupokoev::CompositeShape::CompositeShape(const shape_ptr& shape) :
  CompositeShape()
{
  add(shape);
}


neupokoev::CompositeShape::shape_ptr neupokoev::CompositeShape::operator [](size_t rhs) const
{
  if (rhs >= shapeCount_)
  {
    throw std::out_of_range("Index is out of range");
  }

  return arrayOfShapes_[rhs];
}


neupokoev::CompositeShape &neupokoev::CompositeShape::operator =(const CompositeShape &rhs)
{
  if (this != &rhs)
  {
    shapeCount_ = rhs.shapeCount_;
    angle_ = rhs.angle_;
    shape_array tmpArray(std::make_unique<shape_ptr[]>(rhs.shapeCount_));
    for (size_t i = 0; i < shapeCount_; i++)
    {
      tmpArray[i] = rhs.arrayOfShapes_[i];
    }
    arrayOfShapes_.swap(tmpArray);
  }

  return *this;
}


neupokoev::CompositeShape& neupokoev::CompositeShape::operator =(CompositeShape &&rhs)
{
  if (this != &rhs)
  {
    shapeCount_ = rhs.shapeCount_;
    angle_ = rhs.angle_;
    arrayOfShapes_ = std::move(rhs.arrayOfShapes_);
  }

  return *this;
}


double neupokoev::CompositeShape::getArea() const
{
  double totalArea = 0;
  for (size_t i = 0; i < shapeCount_; i++)
  {
    totalArea += arrayOfShapes_[i]->getArea();
  }
  return totalArea;
}

neupokoev::point_t neupokoev::CompositeShape::getPos() const
{
  return getFrameRect().pos;
}


neupokoev::rectangle_t neupokoev::CompositeShape::getFrameRect() const
{
  if (shapeCount_ == 0)
  {
    return { { 0, 0 }, 0, 0 };
  }

  rectangle_t tempFrameRect = arrayOfShapes_[0]->getFrameRect();

  double minX = tempFrameRect.pos.x - tempFrameRect.width / 2;
  double maxX = tempFrameRect.pos.x + tempFrameRect.width / 2;
  double minY = tempFrameRect.pos.y - tempFrameRect.height / 2;
  double maxY = tempFrameRect.pos.y + tempFrameRect.height / 2;

  for (size_t i = 1; i < shapeCount_; i++)
  {
    tempFrameRect = arrayOfShapes_[i]->getFrameRect();

    double tempValue = tempFrameRect.pos.x - tempFrameRect.width / 2;
    minX = std::min(tempValue, minX);

    tempValue = tempFrameRect.pos.x + tempFrameRect.width / 2;
    maxX = std::max(tempValue, maxX);

    tempValue = tempFrameRect.pos.y - tempFrameRect.height / 2;
    minY = std::min(tempValue, minY);

    tempValue = tempFrameRect.pos.y + tempFrameRect.height / 2;
    maxY = std::max(tempValue, maxY);
  }

  return { { (minX + maxX) / 2, (minY + maxY) / 2 }, maxX - minX, maxY - minY };
}

size_t neupokoev::CompositeShape::getShapeCount() const
{
  return shapeCount_;
}


void neupokoev::CompositeShape::move(const point_t &pos)
{
  const double dX = pos.x - getFrameRect().pos.x;
  const double dY = pos.y - getFrameRect().pos.y;
  move(dX, dY);
}

void neupokoev::CompositeShape::move(double dX, double dY)
{
  for (size_t i = 0; i < shapeCount_; i++)
  {
    arrayOfShapes_[i]->move(dX, dY);
  }
}

void neupokoev::CompositeShape::scale(double scaleFactor)
{
  if (scaleFactor <= 0.0)
  {
    throw std::invalid_argument("Composite shape scale factor must be a positive number");
  }

  const point_t centre = getFrameRect().pos;
  for (size_t i = 0; i < shapeCount_; i++)
  {
    const double deltaX = arrayOfShapes_[i]->getFrameRect().pos.x - centre.x;
    const double deltaY = arrayOfShapes_[i]->getFrameRect().pos.y - centre.y;
    arrayOfShapes_[i]->move({ centre.x + deltaX * scaleFactor, centre.y + deltaY * scaleFactor });
    arrayOfShapes_[i]->scale(scaleFactor);
  }
}

void neupokoev::CompositeShape::add(shape_ptr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer must not be null");
  }

  shape_array tmpArray(std::make_unique<shape_ptr[]>(shapeCount_ + 1));
  for (size_t i = 0; i < shapeCount_; i++)
  {
    tmpArray[i] = arrayOfShapes_[i];
  }
  tmpArray[shapeCount_] = shape;
  shapeCount_++;
  arrayOfShapes_.swap(tmpArray);
}


void neupokoev::CompositeShape::remove(size_t index)
{

  if (index >= shapeCount_)
  {
    throw std::out_of_range("Index is out of range");
  }

  for (size_t i = index; i < shapeCount_ - 1; i++)
  {
    arrayOfShapes_[i] = arrayOfShapes_[i + 1];
  }
  shapeCount_--;
}



void neupokoev::CompositeShape::remove(shape_ptr shape)
{
  for (size_t i = 0; i < shapeCount_; ++i)
  {
    if (shape == arrayOfShapes_[i])
    {
      remove(i);
      return;
    }
  }

  throw std::invalid_argument("This shape doesn't exist");
}

void neupokoev::CompositeShape::rotate(double angle)
{
  angle_ += angle;
  angle_ = (angle_ < 0.0) ? (FullCircle + fmod(angle_, FullCircle)) : fmod(angle_, FullCircle);

  const point_t centre = getFrameRect().pos;
  const double sine = sin(angle * M_PI / 180);
  const double cosine = cos(angle * M_PI / 180);

  for (size_t i = 0; i < shapeCount_; i++)
  {
    const double oldX = arrayOfShapes_[i]->getFrameRect().pos.x - centre.x;
    const double oldY = arrayOfShapes_[i]->getFrameRect().pos.y - centre.y;

    const double newX = oldX * fabs(cosine) - oldY * fabs(sine);
    const double newY = oldX * fabs(sine) + oldY * fabs(cosine);

    arrayOfShapes_[i]->move({ centre.x + newX, centre.y + newY });
    arrayOfShapes_[i]->rotate(angle);
  }

}

size_t neupokoev::CompositeShape::size() const
{
  return shapeCount_;
}

neupokoev::CompositeShape::shape_array neupokoev::CompositeShape::list() const
{
  shape_array tmpArray(std::make_unique<shape_ptr[]>(shapeCount_));
  for (size_t i = 0; i < shapeCount_; i++)
  {
    tmpArray[i] = arrayOfShapes_[i];
  }

  return tmpArray;
}


void neupokoev::CompositeShape::writeParameters() const
{
  neupokoev::rectangle_t rectangle = getFrameRect();
  std::cout << "CompositeShape's centre is (" << getFrameRect().pos.x << ","
    << getFrameRect().pos.x << ")\n"
    << "Frame rectangle width = " << rectangle.width
    << ", height = " << rectangle.height << "\n"
    << "Area = " << getArea() << "\n"
    << "Amount of shapes is " << shapeCount_ << "\n\n";
}
