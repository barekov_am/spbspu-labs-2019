#ifndef A3_COMPOSITE_SHAPE
#define A3_COMPOSITE_SHAPE

#include <memory>
#include "shape.hpp"

namespace neupokoev
{
  class CompositeShape : public Shape
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    using shape_array = std::unique_ptr<shape_ptr[]>;
    CompositeShape();
    CompositeShape(const CompositeShape &source);
    CompositeShape(CompositeShape &&source);
    CompositeShape(const shape_ptr& shape);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &rhs);
    CompositeShape &operator =(CompositeShape &&rhs);

    shape_ptr operator [](size_t rhs) const;


    double getArea() const override;
    rectangle_t getFrameRect() const override;
    size_t getShapeCount() const;
    void move(const point_t &pos) override;
    void move(const double dX, const double dY) override;
    void scale(double scaleFactor) override;
    void rotate(double) override;
    point_t getPos() const override;
    void writeParameters() const override;

    void add(shape_ptr shape);
    void remove(size_t index);
    void remove(shape_ptr shape);
    size_t size() const;
    shape_array list() const;


  private:
    size_t shapeCount_;
    double angle_;
    shape_array arrayOfShapes_;

  };
}

#endif
