#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <memory>
#include "base-types.hpp"

namespace neupokoev
{
  class Shape
  {
  public:

    using ptr = std::shared_ptr<Shape>;
    using arrayPtr = std::unique_ptr<ptr[]>;

    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t &pos) = 0;
    virtual void move(const double dx, const double dy) = 0;
    virtual void scale(double scaleFactor) = 0;
    virtual void rotate(double angle) = 0;
    virtual point_t getPos() const = 0;
    virtual void writeParameters() const = 0;

    int intersection(const ptr shape) const;
  };
}

#endif

