#include <stdexcept>
#include <utility>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "circle.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"

using shape_ptr = std::shared_ptr<neupokoev::Shape>;

const double ErrorValue = 1e-10;

BOOST_AUTO_TEST_SUITE(compositeTestSuite)

BOOST_AUTO_TEST_CASE(invariabilityAfterMoving)
{
  neupokoev::Circle myCircle1({ 4, 4 }, 8.7);
  shape_ptr testCircle = std::make_shared<neupokoev::Circle>(myCircle1);
  neupokoev::Rectangle myRectangle2({ { 5, 6 }, 7.8, 8.9 });
  shape_ptr testRectangle = std::make_shared<neupokoev::Rectangle>(myRectangle2);
  neupokoev::CompositeShape testComposite;
  testComposite.add(testCircle);
  testComposite.add(testRectangle);
  const neupokoev::rectangle_t frameRectBefore = testComposite.getFrameRect();
  const double areaBefore = testComposite.getArea();

  testComposite.move({ 1.9, 1.9 });
  BOOST_CHECK_CLOSE(frameRectBefore.width, testComposite.getFrameRect().width, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectBefore.height, testComposite.getFrameRect().height, ErrorValue);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), ErrorValue);

  testComposite.move(5, -7);
  BOOST_CHECK_CLOSE(frameRectBefore.width, testComposite.getFrameRect().width, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectBefore.height, testComposite.getFrameRect().height, ErrorValue);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), ErrorValue);
}

BOOST_AUTO_TEST_CASE(areaAfterScaling)
{
  neupokoev::Circle myCircle1({ 4, 4 }, 3);
  shape_ptr testCircle = std::make_shared<neupokoev::Circle>(myCircle1);
  neupokoev::Rectangle myRectangle2({ { 5, 6 }, 2.1, 1 });
  shape_ptr testRectangle = std::make_shared<neupokoev::Rectangle>(myRectangle2);
  neupokoev::CompositeShape testComposite;
  testComposite.add(testCircle);
  testComposite.add(testRectangle);
  double areaBefore = testComposite.getArea();

  double scaleFactor = 2.75;
  testComposite.scale(scaleFactor);
  BOOST_CHECK_CLOSE(testComposite.getArea(), scaleFactor * scaleFactor * areaBefore, ErrorValue);
  areaBefore = testComposite.getArea();

  scaleFactor = 0.75;
  testComposite.scale(scaleFactor);
  BOOST_CHECK_CLOSE(testComposite.getArea(), scaleFactor * scaleFactor * areaBefore, ErrorValue);
}

BOOST_AUTO_TEST_CASE(areaAfterRotating)
{
  neupokoev::Circle myCircle1({ 4, 7 }, 1);
  shape_ptr testCircle = std::make_shared<neupokoev::Circle>(myCircle1);
  neupokoev::Rectangle myRectangle2({ { 5, 6 }, 2.1, 1 });
  shape_ptr testRectangle = std::make_shared<neupokoev::Rectangle>(myRectangle2);
  neupokoev::CompositeShape testComposite;
  testComposite.add(testCircle);
  testComposite.add(testRectangle);
  double areaBefore = testComposite.getArea();

  double angle = 48.2;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), ErrorValue);
  areaBefore = testComposite.getArea();

  angle = -30.88;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), ErrorValue);
  areaBefore = testComposite.getArea();

  angle = 765.2;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), ErrorValue);
  areaBefore = testComposite.getArea();

  angle = -987.3;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), ErrorValue);
}

BOOST_AUTO_TEST_CASE(copyAndMove)
{
  neupokoev::Circle myCircle1({ 1, 2 }, 3);
  shape_ptr testCircle = std::make_shared<neupokoev::Circle>(myCircle1);
  neupokoev::CompositeShape testComposite;
  testComposite.add(testCircle);

  BOOST_CHECK_NO_THROW(neupokoev::CompositeShape testComposite2(testComposite));
  BOOST_CHECK_NO_THROW(neupokoev::CompositeShape testComposite2(std::move(testComposite)));

  neupokoev::CompositeShape testComposite2;
  testComposite2.add(testCircle);

  neupokoev::CompositeShape testComposite3;

  BOOST_CHECK_NO_THROW(testComposite3 = testComposite2);
  BOOST_CHECK_NO_THROW(testComposite3 = std::move(testComposite2));
}

BOOST_AUTO_TEST_CASE(exceptionThrow)
{
  neupokoev::Circle myCircle1({ 1, 2 }, 3);
  shape_ptr testCircle = std::make_shared<neupokoev::Circle>(myCircle1);
  neupokoev::Rectangle myRectangle2({ { 4, 5 }, 6, 7 });
  shape_ptr testRectangle = std::make_shared<neupokoev::Rectangle>(myRectangle2);
  neupokoev::CompositeShape testComposite;
  testComposite.add(testCircle);
  testComposite.add(testRectangle);
  BOOST_CHECK_THROW(testComposite.scale(-2), std::invalid_argument);

  BOOST_CHECK_THROW(testComposite.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(testComposite.remove(4), std::out_of_range);
  BOOST_CHECK_THROW(testComposite.remove(-2), std::out_of_range);

  BOOST_CHECK_THROW(testComposite[100], std::out_of_range);
  BOOST_CHECK_THROW(testComposite[-1], std::out_of_range);

  testComposite.remove(1);
  testComposite.remove(0);
  BOOST_CHECK_THROW(testComposite.remove(0), std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
