#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteRectangle)

const double accuracy = 0.01;

BOOST_AUTO_TEST_CASE(moveRectangleTest)
{
  neupokoev::Rectangle testRectangle({ { 3, 4 }, 5, 6 });
  const neupokoev::rectangle_t InitialFrameRect = testRectangle.getFrameRect();
  const double InitialArea = testRectangle.getArea();

  testRectangle.move({ 7, 8 });
  BOOST_CHECK_CLOSE(InitialFrameRect.width, testRectangle.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(InitialFrameRect.height, testRectangle.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(InitialArea, testRectangle.getArea(), accuracy);

  testRectangle.move(-5, -6);
  BOOST_CHECK_CLOSE(InitialFrameRect.width, testRectangle.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(InitialFrameRect.height, testRectangle.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(InitialArea, testRectangle.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(areaAfterScalingRectangleTest)
{
  neupokoev::Rectangle testRectangle({ { 4, 5 }, 6, 7 });
  const double InitialArea = testRectangle.getArea();
  const double scaleFactor = 3;

  testRectangle.scale(scaleFactor);
  BOOST_CHECK_CLOSE(testRectangle.getArea(), scaleFactor * scaleFactor * InitialArea, accuracy);
}

BOOST_AUTO_TEST_CASE(invalidScaleArgumentsRectangle)
{
  BOOST_CHECK_THROW(neupokoev::Rectangle({ { 1, 2 }, 3, -4 }), std::invalid_argument);
  BOOST_CHECK_THROW(neupokoev::Rectangle({ { 1, 2 }, -3, 4 }), std::invalid_argument);

  neupokoev::Rectangle testRectangle({ { 1, 2 }, 3, 4 });
  BOOST_CHECK_THROW(testRectangle.scale(-5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testRectangleRotation)
{
  neupokoev::Rectangle testRectangle({ { 7, 2 }, 7.1, 2 });
  const double areaBefore = testRectangle.getArea();
  const neupokoev::rectangle_t frameRectBefore = testRectangle.getFrameRect();

  double angle = -90;
  testRectangle.rotate(angle);
  double areaAfter = testRectangle.getArea();
  neupokoev::rectangle_t frameRectAfter = testRectangle.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, accuracy);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, accuracy);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, accuracy);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, accuracy);

  angle = 40;
  testRectangle.rotate(angle);
  angle = 140;
  testRectangle.rotate(angle);

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, accuracy);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, accuracy);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, accuracy);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, accuracy);
}


BOOST_AUTO_TEST_SUITE_END()
