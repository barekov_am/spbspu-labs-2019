#define _USE_MATH_DEFINES
#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

const double FullCircle = 360.0;

neupokoev::Rectangle::Rectangle(const rectangle_t &parameters) :
  rect_(parameters),
  angle_(0.0)
{
  if ((rect_.width <= 0.0) || (rect_.height <= 0.0))
  {
    throw std::invalid_argument("Width and height must be positive numbers");
  }
}

neupokoev::point_t neupokoev::Rectangle::getPos() const
{
  return rect_.pos;
}

double neupokoev::Rectangle::getArea() const
{
  return rect_.width * rect_.height;
}

neupokoev::rectangle_t neupokoev::Rectangle::getFrameRect() const
{
  const double sinus = sin(angle_ * M_PI / 180);
  const double cosinus = cos(angle_ * M_PI / 180);
  const double width = rect_.height * abs(sinus) + rect_.width * abs(cosinus);
  const double height = rect_.height * abs(cosinus) + rect_.width * abs(sinus);

  return { rect_.pos, width, height };
}

void neupokoev::Rectangle::move(const point_t &pos)
{
  rect_.pos = pos;
}

void neupokoev::Rectangle::move(const double dx, const double dy)
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

void neupokoev::Rectangle::scale(double scaleFactor)
{
  if (scaleFactor <= 0.0)
  {
    throw std::invalid_argument("Rectangle scale factor must be a positive number");
  }
  else
  {
    rect_.width *= scaleFactor;
    rect_.height *= scaleFactor;
  }
}

void neupokoev::Rectangle::rotate(double angle)
{
  angle_ += angle;
  angle_ = (angle_ < 0.0) ? (FullCircle + fmod(angle_, FullCircle)) : fmod(angle_, FullCircle);
}


void neupokoev::Rectangle::writeParameters() const
{
  rectangle_t rectangle = getFrameRect();
  std::cout << "Rectangle centre is (" << rect_.pos.x << ","
      << rect_.pos.y << ")\n" << "Angle is" << angle_ 
      << "\n Frame rectangle width = " << rectangle.width
      << ", height = " << rectangle.height << "\n"
      << "Area = " << getArea() << "\n\n";
}
