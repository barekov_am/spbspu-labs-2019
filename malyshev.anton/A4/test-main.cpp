#define BOOST_TEST_MODULE A4

#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "splitting.hpp"

const double EPSILON = 0.01;
using point_ptr = std::shared_ptr<malyshev::point_t>;
using shape_ptr = std::shared_ptr<malyshev::Shape>;

BOOST_AUTO_TEST_SUITE(TestsForMatrix)

  BOOST_AUTO_TEST_CASE(throwExceptionForMatrix)
  {
    shape_ptr p_circle = std::make_shared<malyshev::Circle>(2, malyshev::point_t {3, 3});
    shape_ptr p_rectangle1 = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {6, 3}, 0);
    shape_ptr p_rectangle2 = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {20, 20}, 0);

    malyshev::CompositeShape compositeShape;
    compositeShape.add(p_rectangle1);
    compositeShape.add(p_circle);
    compositeShape.add(p_rectangle2);
    malyshev::Matrix matrix = malyshev::split(compositeShape);

    BOOST_CHECK_THROW(matrix[matrix.getRows()], std::invalid_argument);
    BOOST_CHECK_THROW(matrix[-1], std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TestCopyOperator)
  {
    shape_ptr p_circle = std::make_shared<malyshev::Circle>(2, malyshev::point_t {3, 3});
    shape_ptr p_rectangle1 = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {6, 3}, 0);
    shape_ptr p_rectangle2 = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {20, 20}, 0);

    malyshev::CompositeShape compositeShape;
    compositeShape.add(p_rectangle1);
    compositeShape.add(p_circle);
    compositeShape.add(p_rectangle2);
    malyshev::Matrix matrix = malyshev::split(compositeShape);
    malyshev::CompositeShape compositeShape1;

    compositeShape1.add(p_rectangle1);
    compositeShape1.add(p_circle);
    compositeShape1.add(p_rectangle2);

    malyshev::Matrix matrix1 = malyshev::split(compositeShape1);
    const malyshev::Matrix& matrix2 = matrix1;
    BOOST_CHECK(matrix1 == matrix2);
  }

  BOOST_AUTO_TEST_CASE(TestMoveOperator)
  {
    shape_ptr p_circle = std::make_shared<malyshev::Circle>(2, malyshev::point_t {3, 3});
    shape_ptr p_rectangle1 = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {6, 3}, 0);
    shape_ptr p_rectangle2 = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {20, 20}, 0);

    malyshev::CompositeShape compositeShape;
    compositeShape.add(p_rectangle1);
    compositeShape.add(p_circle);
    compositeShape.add(p_rectangle2);
    malyshev::Matrix matrix1 = malyshev::split(compositeShape);

    malyshev::Matrix matrix2;
    matrix1 = std::move(matrix2);
    malyshev::Matrix matrix3 = matrix1;

    BOOST_CHECK(matrix2 == matrix3);
  }

  BOOST_AUTO_TEST_CASE(TestCopyConstructor)
  {
    shape_ptr p_circle = std::make_shared<malyshev::Circle>(2, malyshev::point_t {3, 3});
    shape_ptr p_rectangle1 = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {6, 3}, 0);
    shape_ptr p_rectangle2 = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {20, 20}, 0);

    malyshev::CompositeShape compositeShape;
    compositeShape.add(p_rectangle1);
    compositeShape.add(p_circle);
    compositeShape.add(p_rectangle2);
    malyshev::Matrix matrix1 = malyshev::split(compositeShape);
    malyshev::Matrix matrix2(matrix1);

    BOOST_CHECK(matrix1 == matrix2);
  }

  BOOST_AUTO_TEST_CASE(TestMoveConstructor)
  {
    shape_ptr p_circle = std::make_shared<malyshev::Circle>(2, malyshev::point_t {3, 3});
    shape_ptr p_rectangle1 = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {6, 3}, 0);
    shape_ptr p_rectangle2 = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {20, 20}, 0);

    malyshev::CompositeShape compositeShape;
    compositeShape.add(p_rectangle1);
    compositeShape.add(p_circle);
    compositeShape.add(p_rectangle2);
    malyshev::Matrix matrix1 = malyshev::split(compositeShape);
    malyshev::Matrix matrix3(matrix1);
    malyshev::Matrix matrix2(std::move(matrix1));

    BOOST_CHECK(matrix2 == matrix3);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestingForSplitting)

  BOOST_AUTO_TEST_CASE(checkSplitting)
  {
    shape_ptr p_circle = std::make_shared<malyshev::Circle>(2, malyshev::point_t {3, 3});
    shape_ptr p_rectangle1 = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {6, 3}, 0);
    shape_ptr p_rectangle2 = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {20, 20}, 0);

    malyshev::CompositeShape compositeShape;
    compositeShape.add(p_rectangle1);
    compositeShape.add(p_circle);
    compositeShape.add(p_rectangle2);

    malyshev::Matrix matrix = malyshev::split(compositeShape);

    BOOST_REQUIRE_EQUAL(2, matrix.getRows());
    BOOST_REQUIRE_EQUAL(2, matrix.getColumns());
    BOOST_CHECK(matrix[0][0] == p_rectangle1);
    BOOST_CHECK(matrix[0][1] == p_rectangle2);
    BOOST_CHECK(matrix[1][0] == p_circle);
    BOOST_CHECK(matrix[1][1] == nullptr);
    BOOST_CHECK(matrix[0][0] != p_rectangle2);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestingForRotating)

  BOOST_AUTO_TEST_CASE(areaAfterRotatingComposite)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

    malyshev::Polygon polygon;
    polygon.add(point1);
    polygon.add(point2);
    polygon.add(point3);
    polygon.add(point4);

    shape_ptr p_rectangle = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {8, 3}, 0);
    shape_ptr p_circle = std::make_shared<malyshev::Circle>(2, malyshev::point_t {3, 3});
    shape_ptr p_triangle =
        std::make_shared<malyshev::Triangle>(malyshev::point_t {1, 1}, malyshev::point_t {3, 1}, malyshev::point_t {2, 3});
    shape_ptr p_polygon = std::make_shared<malyshev::Polygon>(polygon);

    malyshev::CompositeShape compositeShape;
    compositeShape.add(p_rectangle);
    compositeShape.add(p_circle);
    compositeShape.add(p_triangle);
    compositeShape.add(p_polygon);

    const double areaBefore = compositeShape.getArea();
    compositeShape.rotate(60.30);

    BOOST_CHECK_CLOSE(areaBefore, compositeShape.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(frameRectAfterRotatingComposite)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

    malyshev::Polygon polygon;
    polygon.add(point1);
    polygon.add(point2);
    polygon.add(point3);
    polygon.add(point4);

    shape_ptr p_rectangle = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {8, 3}, 0);
    shape_ptr p_circle = std::make_shared<malyshev::Circle>(2, malyshev::point_t {3, 3});
    shape_ptr p_triangle =
      std::make_shared<malyshev::Triangle>(malyshev::point_t {1, 1}, malyshev::point_t {3, 1}, malyshev::point_t {2, 3});
    shape_ptr p_polygon = std::make_shared<malyshev::Polygon>(polygon);

    malyshev::CompositeShape compositeShape;
    compositeShape.add(p_rectangle);
    compositeShape.add(p_circle);
    compositeShape.add(p_triangle);
    compositeShape.add(p_polygon);

    compositeShape.rotate(90);

    const malyshev::rectangle_t frameRectBefore = compositeShape.getFrameRect();
    compositeShape.rotate(90);

    BOOST_CHECK_CLOSE(compositeShape.getCentre().x, frameRectBefore.pos.x, EPSILON);
    BOOST_CHECK_CLOSE(compositeShape.getCentre().y, frameRectBefore.pos.y, EPSILON);
    BOOST_CHECK_CLOSE(compositeShape.getFrameRect().width, frameRectBefore.height, EPSILON);
    BOOST_CHECK_CLOSE(compositeShape.getFrameRect().height, frameRectBefore.width, EPSILON);
  }

BOOST_AUTO_TEST_SUITE_END()
