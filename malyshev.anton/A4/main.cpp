#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "splitting.hpp"

using point_ptr = std::shared_ptr<malyshev::point_t>;

void printFrameRect(const malyshev::Shape &shape)
{
  malyshev::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "Width: " << frameRect.width << "  ";
  std::cout << "Height: " << frameRect.height << "  ";
  std::cout << "Center: ( " << frameRect.pos.x << " , " << frameRect.pos.y << " )" << std::endl;
}

void printArea(const malyshev::Shape &shape)
{
  std::cout << "Area: " << shape.getArea() << std::endl;
}

void printCenter(const malyshev::Shape &shape)
{
  std::cout << "Center: ( " << shape.getCentre().x << " , " << shape.getCentre().y << " )" << std::endl;
}

void printAngle(const malyshev::Rectangle &rectangle)
{
  std::cout << "Angle: " << rectangle.getAngle()  << std::endl;
}

void printSize(const malyshev::CompositeShape &compositeShape)
{
  std::cout << "Size composite shape: " << compositeShape.size() << std::endl;
}

int main()
{
  point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
  point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
  point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
  point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

  malyshev::Polygon polygon;
  polygon.add(point1);
  polygon.add(point2);
  polygon.add(point3);
  polygon.add(point4);

  malyshev::Shape::shape_ptr p_rectangle = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {8, 3}, 0);
  malyshev::Shape::shape_ptr p_circle = std::make_shared<malyshev::Circle>(2, malyshev::point_t {3, 3});
  malyshev::Shape::shape_ptr p_triangle = std::make_shared<malyshev::Triangle>
      (malyshev::point_t {1, 1}, malyshev::point_t {3, 1}, malyshev::point_t {2, 3});
  malyshev::Shape::shape_ptr p_polygon = std::make_shared<malyshev::Polygon>(polygon);
  const double factor = 2.5;

  std::cout << "Composite Shape" << std::endl;
  malyshev::CompositeShape compositeShape;
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  compositeShape.add(p_triangle);
  compositeShape.add(p_polygon);
  printSize(compositeShape);
  printFrameRect(compositeShape);
  printArea(compositeShape);
  compositeShape.scale(factor);
  printArea(compositeShape);
  printFrameRect(compositeShape);
  compositeShape.move({5.0, 5.0});
  printCenter(compositeShape);
  printFrameRect(compositeShape);

  std::cout << "Composite Shape rotate 90" << std::endl;
  compositeShape.rotate(90);
  printSize(compositeShape);
  printFrameRect(compositeShape);
  printArea(compositeShape);
  printCenter(compositeShape);

  std::cout << "Composite Shape rotate 90 + 90" << std::endl;
  compositeShape.rotate(90);
  printSize(compositeShape);
  printFrameRect(compositeShape);
  printArea(compositeShape);
  printCenter(compositeShape);

  std::cout << "Composite Shape rotate 90 + 90 + 540 = 0" << std::endl;
  compositeShape.rotate(540);
  printSize(compositeShape);
  printFrameRect(compositeShape);
  printArea(compositeShape);
  printCenter(compositeShape);

  compositeShape.remove(1);
  printSize(compositeShape);

  malyshev::Circle circle2(2, {3, 3});
  std::cout << "Circle rotate 0" << std::endl;
  printFrameRect(circle2);
  printArea(circle2);
  printCenter(circle2);

  circle2.rotate(90);
  std::cout << "Circle rotate 90" << std::endl;
  printFrameRect(circle2);
  printArea(circle2);
  printCenter(circle2);

  std::cout << "Rectangle rotate 0" << std::endl;
  malyshev::Rectangle rectangle2(6, 4, {8, 3}, 0);
  printArea(rectangle2);
  printFrameRect(rectangle2);
  printAngle(rectangle2);

  rectangle2.rotate(90);
  std::cout << "Rectangle rotate 90" << std::endl;
  printArea(rectangle2);
  printFrameRect(rectangle2);
  printAngle(rectangle2);

  rectangle2.rotate(90);
  std::cout << "Rectangle rotate 90 + 90" << std::endl;
  printArea(rectangle2);
  printFrameRect(rectangle2);
  printAngle(rectangle2);

  std::cout << "Triangle rotate 0" << std::endl;
  malyshev::Triangle triangle2({1, 1}, {4, 1}, {2, 3});
  printArea(triangle2);
  printFrameRect(triangle2);
  printCenter(triangle2);

  triangle2.rotate(45);
  std::cout << "Triangle rotate 45" << std::endl;
  printArea(triangle2);
  printFrameRect(triangle2);
  printCenter(triangle2);

  triangle2.rotate(45);
  std::cout << "Triangle rotate 45 + 45" << std::endl;
  printArea(triangle2);
  printFrameRect(triangle2);
  printCenter(triangle2);

  triangle2.rotate(90);
  std::cout << "Triangle rotate 45 + 45 + 90" << std::endl;
  printArea(triangle2);
  printFrameRect(triangle2);
  printCenter(triangle2);

  triangle2.rotate(540);
  std::cout << "Triangle rotate 45 + 45 + 90 + 540 = 0" << std::endl;
  printArea(triangle2);
  printFrameRect(triangle2);
  printCenter(triangle2);

  std::cout << "Polygon rotate 0" << std::endl;
  point_ptr point5 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
  point_ptr point6 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 1});
  point_ptr point7 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
  point_ptr point8 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

  malyshev::Polygon polygon2;
  polygon2.add(point5);
  polygon2.add(point6);
  polygon2.add(point7);
  polygon2.add(point8);

  printArea(polygon2);
  printFrameRect(polygon2);
  printCenter(polygon2);

  polygon2.rotate(45);
  std::cout << "Polygon rotate 45" << std::endl;
  printArea(polygon2);
  printFrameRect(polygon2);
  printCenter(polygon2);

  polygon2.rotate(45);
  std::cout << "Polygon rotate 45 + 45" << std::endl;
  printArea(polygon2);
  printFrameRect(polygon2);
  printCenter(polygon2);

  polygon2.rotate(270);
  std::cout << "Polygon rotate 45 + 45 + 270 = 0" << std::endl;
  printArea(polygon2);
  printFrameRect(polygon2);
  printCenter(polygon2);

  //matrix
  point_ptr point9 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 9});
  point_ptr point10 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 10});
  point_ptr point11 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 13});
  point_ptr point12 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 12});

  malyshev::Polygon polygon3;
  polygon3.add(point9);
  polygon3.add(point10);
  polygon3.add(point11);
  polygon3.add(point12);

  malyshev::Shape::shape_ptr p_rectangle3 = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {6, 3}, 0);
  malyshev::Shape::shape_ptr p_rectangle4 = std::make_shared<malyshev::Rectangle>(6, 4, malyshev::point_t {20, 20}, 0);
  malyshev::Shape::shape_ptr p_circle3 = std::make_shared<malyshev::Circle>(2, malyshev::point_t {3, 3});
  malyshev::Shape::shape_ptr p_triangle3 = std::make_shared<malyshev::Triangle>
      (malyshev::point_t {1, 9}, malyshev::point_t {3, 9}, malyshev::point_t {2, 12});
  malyshev::Shape::shape_ptr p_polygon3 = std::make_shared<malyshev::Polygon>(polygon3);

  malyshev::CompositeShape compositeShape2;
  compositeShape2.add(p_rectangle3);
  compositeShape2.add(p_circle3);
  compositeShape2.add(p_rectangle4);
  compositeShape2.add(p_triangle3);
  compositeShape2.add(p_polygon3);

  malyshev::Matrix matrix = malyshev::split(compositeShape2);
  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    for (size_t j = 0; j < matrix.getColumns(); j++)
    {
      if (matrix.get(i, j) != nullptr)
      {
        std::cout << "[" << i << ", " << j << "] FrameRect : ";
        printFrameRect(*matrix.get(i, j));
      }
    }
  }

  return 0;
}
