#define BOOST_TEST_MODULE A2

#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

const double EPSILON = 0.01;
using point_ptr = std::shared_ptr<malyshev::point_t>;

BOOST_AUTO_TEST_SUITE(TestSuiteA2)

  BOOST_AUTO_TEST_CASE(frameRectOfRectangleNotChanged)
  {
    malyshev::Rectangle rectangle(4, 4, {5, 5}, 0);
    const double previousArea = rectangle.getArea();
    const malyshev::rectangle_t previousFrameRect = rectangle.getFrameRect();

    rectangle.move(2, 2);
    BOOST_CHECK_CLOSE(previousFrameRect.width, rectangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(previousFrameRect.height, rectangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(previousArea, rectangle.getArea(), EPSILON);

    rectangle.move({12, 12});
    BOOST_CHECK_CLOSE(previousFrameRect.width, rectangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(previousFrameRect.height, rectangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(previousArea, rectangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(frameRectOfCircleNotChanged)
  {
    malyshev::Circle circle(6, {5, 5});
    const double previousArea = circle.getArea();
    const malyshev::rectangle_t previousFrameRect = circle.getFrameRect();

    circle.move(2, 2);
    BOOST_CHECK_CLOSE(previousFrameRect.width, circle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(previousFrameRect.height, circle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(previousArea, circle.getArea(), EPSILON);

    circle.move({12, 12});
    BOOST_CHECK_CLOSE(previousFrameRect.width, circle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(previousFrameRect.height, circle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(previousArea, circle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(frameRectOfTriangleNotChanged)
  {
    malyshev::Triangle triangle({1, 1}, {3, 1}, {2, 3});
    const double previousArea = triangle.getArea();
    const malyshev::rectangle_t previousFrameRect = triangle.getFrameRect();

    triangle.move(2, 2);
    BOOST_CHECK_CLOSE(previousFrameRect.width, triangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(previousFrameRect.height, triangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(previousArea, triangle.getArea(), EPSILON);

    triangle.move({12, 12});
    BOOST_CHECK_CLOSE(previousFrameRect.width, triangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(previousFrameRect.height, triangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(previousArea, triangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(frameRectOfPolygonNotChanged)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

    malyshev::Polygon polygon;
    polygon.add(point1);
    polygon.add(point2);
    polygon.add(point3);
    polygon.add(point4);
    const double previousArea = polygon.getArea();
    const malyshev::rectangle_t previousFrameRect = polygon.getFrameRect();

    polygon.move(2, 2);
    BOOST_CHECK_CLOSE(previousFrameRect.width, polygon.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(previousFrameRect.height, polygon.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(previousArea, polygon.getArea(), EPSILON);

    polygon.move({12, 12});
    BOOST_CHECK_CLOSE(previousFrameRect.width, polygon.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(previousFrameRect.height, polygon.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(previousArea, polygon.getArea(), EPSILON);
  }
  BOOST_AUTO_TEST_CASE(rectangleAreaChanging)
  {
    malyshev::Rectangle rectangle(5.0, 5.0, {10.0, 10.0}, 0);
    const double previousArea = rectangle.getArea();
    const double factor = 3.2;

    rectangle.scale(factor);
    BOOST_CHECK_CLOSE(previousArea * factor * factor, rectangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(circleAreaChanging)
  {
    malyshev::Circle circle(6, {5, 5});
    const double previousArea = circle.getArea();
    const double factor = 3.2;

    circle.scale(factor);
    BOOST_CHECK_CLOSE(previousArea * factor * factor, circle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(triangleAreaChanging)
  {
    malyshev::Triangle triangle({1, 1}, {3, 1}, {2, 3});
    const double previousArea = triangle.getArea();
    const double factor = 3.2;

    triangle.scale(factor);
    BOOST_CHECK_CLOSE(previousArea * factor * factor, triangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(polygonAreaChanging)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

    malyshev::Polygon polygon;
    polygon.add(point1);
    polygon.add(point2);
    polygon.add(point3);
    polygon.add(point4);
    const double previousArea = polygon.getArea();
    const double factor = 3.2;

    polygon.scale(factor);
    BOOST_CHECK_CLOSE(previousArea * factor * factor, polygon.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(wrongRectangleParametrsAndScaling)
  {
    BOOST_CHECK_THROW(malyshev::Rectangle rectangle(-4, 4, {5, 5}, 0), std::invalid_argument);
    BOOST_CHECK_THROW(malyshev::Rectangle rectangle(4, -4, {5, 5}, 0), std::invalid_argument);

    const double factor = -3;
    malyshev::Rectangle rectangle(4, 4, {5, 5}, 0);
    BOOST_CHECK_THROW(rectangle.scale(factor), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(wrongCircleParametersAndScaling)
  {
    BOOST_CHECK_THROW(malyshev::Circle circle(-6, {5, 5}), std::invalid_argument);

    const double factor = -3;
    malyshev::Circle circle(6, {5, 5});
    BOOST_CHECK_THROW(circle.scale(factor), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(wrongTriangleScaling)
  {
    const double factor = -3;
    malyshev::Triangle triangle({1, 1}, {3, 1}, {2, 3});
    BOOST_CHECK_THROW(triangle.scale(factor), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(wrongPolygonParametersAndScaling)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});
    point_ptr point22 = std::make_shared<malyshev::point_t> (malyshev::point_t {1.1, 2.8});

    malyshev::Polygon polygon1;
    polygon1.add(point1);
    polygon1.add(point2);

    BOOST_CHECK_THROW(polygon1.scale(2.2), std::invalid_argument);

    malyshev::Polygon polygon2;
    polygon2.add(point1);
    polygon2.add(point1);
    BOOST_CHECK_THROW(polygon2.add(point1), std::invalid_argument);

    malyshev::Polygon polygon3;
    polygon3.add(point1);
    polygon3.add(point22);
    BOOST_CHECK_THROW(polygon3.add(point3), std::invalid_argument);

    malyshev::Polygon polygon4;
    polygon4.add(point1);
    polygon4.add(point2);
    polygon4.add(point3);
    polygon4.add(point4);

    const double factor = -3;
    BOOST_CHECK_THROW(polygon4.scale(factor), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestingForPolygonCopyAndMove)

  void equalOfShapes(malyshev::Shape& shape1, malyshev::Shape& shape2)
  {
    malyshev::point_t center1 = shape1.getCentre();
    malyshev::point_t center2 = shape2.getCentre();
    malyshev::rectangle_t frameRect1 = shape1.getFrameRect();
    malyshev::rectangle_t frameRect2 = shape2.getFrameRect();

    BOOST_CHECK_CLOSE(shape1.getArea(), shape2.getArea(), EPSILON);
    BOOST_CHECK_CLOSE(center1.x, center2.x, EPSILON);
    BOOST_CHECK_CLOSE(center1.y, center2.y, EPSILON);
    BOOST_CHECK_CLOSE(frameRect1.height, frameRect2.height, EPSILON);
    BOOST_CHECK_CLOSE(frameRect1.width, frameRect2.width, EPSILON);
    BOOST_CHECK_CLOSE(frameRect1.pos.x, frameRect2.pos.x, EPSILON);
    BOOST_CHECK_CLOSE(frameRect1.pos.y, frameRect2.pos.y, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(TestCopyConstructor)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

    malyshev::Polygon polygon1;
    polygon1.add(point1);
    polygon1.add(point2);
    polygon1.add(point3);
    polygon1.add(point4);

    malyshev::Polygon polygon2(polygon1);
    equalOfShapes(polygon1, polygon2);
  }

  BOOST_AUTO_TEST_CASE(TestCopyOperatorEmpty)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

    malyshev::Polygon polygon1;
    polygon1.add(point1);
    polygon1.add(point2);
    polygon1.add(point3);
    polygon1.add(point4);

    malyshev::Polygon polygon2;
    polygon2 = polygon1;
    equalOfShapes(polygon1, polygon2);
  }

  BOOST_AUTO_TEST_CASE(TestCopyOperatorNotEmpty)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});
    point_ptr point5 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 11});
    point_ptr point6 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 12});
    point_ptr point7 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 14});
    point_ptr point8 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 13});

    malyshev::Polygon polygon1;
    polygon1.add(point1);
    polygon1.add(point2);
    polygon1.add(point3);
    polygon1.add(point4);

    malyshev::Polygon polygon2;
    polygon2.add(point5);
    polygon2.add(point6);
    polygon2.add(point7);
    polygon2.add(point8);

    polygon2 = polygon1;
    equalOfShapes(polygon1, polygon2);
  }

  BOOST_AUTO_TEST_CASE(TestMoveConstructor)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

    malyshev::Polygon polygon1;
    polygon1.add(point1);
    polygon1.add(point2);
    polygon1.add(point3);
    polygon1.add(point4);

    malyshev::Polygon copyOfPolygon1 = polygon1;
    malyshev::Polygon polygon2(std::move(polygon1));

    equalOfShapes(copyOfPolygon1, polygon2);
  }

  BOOST_AUTO_TEST_CASE(TestMoveOperator)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

    malyshev::Polygon polygon1;
    polygon1.add(point1);
    polygon1.add(point2);
    polygon1.add(point3);
    polygon1.add(point4);

    malyshev::Polygon polygon2;
    malyshev::Polygon polygon3 = polygon1;
    polygon2 = std::move(polygon1);

    equalOfShapes(polygon3, polygon2);
  }

BOOST_AUTO_TEST_SUITE_END()

