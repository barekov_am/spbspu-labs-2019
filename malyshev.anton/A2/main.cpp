#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

using point_ptr = std::shared_ptr<malyshev::point_t>;

void printShapeInfo(const  malyshev::Shape &shape)
{
  std::cout << "Area: " << shape.getArea() << std::endl;
  malyshev::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "Width of a frame rectangle: " << frameRect.width << ", ";
  std::cout << "Height of a frame rectangle: " << frameRect.height << ", ";
  std::cout << "Centre of a frame rectangle: (" << frameRect.pos.x << ", " << frameRect.pos.y << ")" << std::endl;
}

int main()
{
  malyshev::Rectangle rectangle(5, 5, {10, 10}, 0);
  std::cout << "_________________Rectangle_________________" << std::endl;
  printShapeInfo(rectangle);
  rectangle.move({6, 6});
  std::cout << "Rectangle after first move:" << std::endl;
  std::cout << "Centre of a figure: (" << rectangle.getFrameRect().pos.x;
  std::cout << ", " << rectangle.getFrameRect().pos.y << ")" << std::endl;
  printShapeInfo(rectangle);
  rectangle.move(2.3, 2.4);
  std::cout << "Rectangle after second move:" << std::endl;
  std::cout << "Centre of a figure: (" << rectangle.getFrameRect().pos.x;
  std::cout << ", " << rectangle.getFrameRect().pos.y << ")" << std::endl;
  printShapeInfo(rectangle);

  malyshev::Circle circle(6, {3, 3});
  std::cout << "_________________Circle_________________:" << std::endl;
  printShapeInfo(circle);
  circle.move({5, 5});
  std::cout << "Circle after first move:" << std::endl;
  std::cout << "Centre of a figure: (" << circle.getFrameRect().pos.x;
  std::cout << ", " << circle.getFrameRect().pos.y << ")" << std::endl;
  printShapeInfo(circle);
  circle.move(1.1, 1.2);
  std::cout << "Circle after second move:" << std::endl;
  std::cout << "Centre of a figure: (" << circle.getFrameRect().pos.x;
  std::cout << ", " << circle.getFrameRect().pos.y << ")" << std::endl;
  printShapeInfo(circle);

  malyshev::Triangle triangle({1, 1}, {3, 1}, {2, 3});
  std::cout << "_________________triangle_________________:" << std::endl;
  printShapeInfo(triangle);
  triangle.move({4, 4});
  std::cout << "Triangle after first move:" << std::endl;
  std::cout << "Centre of a figure: (" << triangle.getCentre().x << ", " << triangle.getCentre().y << ")" << std::endl;
  printShapeInfo(triangle);
  triangle.move(4.4, -4.4);
  std::cout << "Triangle after second move:" << std::endl;
  std::cout << "Centre of a figure: (" << triangle.getCentre().x << ", " << triangle.getCentre().y << ")" << std::endl;
  printShapeInfo(triangle);

  std::cout << "_________________polygon_________________:" << std::endl;
  point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
  point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
  point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
  point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

  malyshev::Polygon polygon;
  polygon.add(point1);
  polygon.add(point2);
  polygon.add(point3);
  polygon.add(point4);

  printShapeInfo(polygon);
  polygon.move({4, 4});
  std::cout << "Polygon after first move:" << std::endl;
  std::cout << "Centre of a figure: (" << polygon.getCentre().x << ", " << polygon.getCentre().y << ")" << std::endl;
  printShapeInfo(polygon);
  polygon.move(4.4, -4.4);
  std::cout << "Polygon after second move:" << std::endl;
  std::cout << "Centre of a figure: (" << polygon.getCentre().x << ", " << polygon.getCentre().y << ")" << std::endl;
  printShapeInfo(polygon);
  return 0;
}
