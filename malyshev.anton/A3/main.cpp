#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

using point_ptr = std::shared_ptr<malyshev::point_t>;

void printFrameRect(const malyshev::Shape& shape)
{
  malyshev::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "Width: " << frameRect.width << "  ";
  std::cout << "Height: " << frameRect.height << "  ";
  std::cout << "Center: ( " << shape.getCentre().x << " , " << shape.getCentre().y << " )" << std::endl;
}

void printArea(const malyshev::Shape& shape)
{
  std::cout << "Area: " << shape.getArea() << std::endl;
}

void printCenter(const malyshev::Shape& shape)
{
  std::cout << "Center: ( " << shape.getCentre().x << " , " << shape.getCentre().y << " )" << std::endl;
}

void printSize(const malyshev::CompositeShape& compositeShape)
{
  std::cout << "Size composite shape: " << compositeShape.size() << std::endl;
}

int main()
{
  point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
  point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
  point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
  point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

  malyshev::Polygon polygon;
  polygon.add(point1);
  polygon.add(point2);
  polygon.add(point3);
  polygon.add(point4);

  malyshev::Shape::shape_ptr p_rectangle = std::make_shared<malyshev::Rectangle>(5, 5, malyshev::point_t {10, 10}, 0);
  malyshev::Shape::shape_ptr p_circle = std::make_shared<malyshev::Circle>(6, malyshev::point_t {3, 3});
  malyshev::Shape::shape_ptr p_triangle = std::make_shared<malyshev::Triangle>
      (malyshev::point_t {1, 1}, malyshev::point_t {3, 1}, malyshev::point_t {2, 3});
  malyshev::Shape::shape_ptr p_polygon = std::make_shared<malyshev::Polygon>(polygon);
  const double factor = 2.5;

  std::cout << "Composite Shape" << std::endl;
  malyshev::CompositeShape compositeShape;
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  compositeShape.add(p_triangle);
  compositeShape.add(p_polygon);
  printSize(compositeShape);
  printFrameRect(compositeShape);
  printArea(compositeShape);
  compositeShape.scale(factor);
  printArea(compositeShape);
  compositeShape.move({5.0, 5.0});
  printCenter(compositeShape);
  compositeShape.remove(1);
  printSize(compositeShape);

  return 0;
}
