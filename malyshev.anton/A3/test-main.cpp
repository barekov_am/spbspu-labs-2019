#define BOOST_TEST_MODULE A3

#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

const double EPSILON = 0.01;
using point_ptr = std::shared_ptr<malyshev::point_t>;

BOOST_AUTO_TEST_SUITE(TestSuiteA3)

  void equalOfShapes(malyshev::Shape& shape1, malyshev::Shape& shape2)
  {
    malyshev::point_t center1 = shape1.getCentre();
    malyshev::point_t center2 = shape2.getCentre();
    malyshev::rectangle_t frameRect1 = shape1.getFrameRect();
    malyshev::rectangle_t frameRect2 = shape2.getFrameRect();

    BOOST_CHECK_CLOSE(shape1.getArea(), shape2.getArea(), EPSILON);
    BOOST_CHECK_CLOSE(center1.x, center2.x, EPSILON);
    BOOST_CHECK_CLOSE(center1.y, center2.y, EPSILON);
    BOOST_CHECK_CLOSE(frameRect1.height, frameRect2.height, EPSILON);
    BOOST_CHECK_CLOSE(frameRect1.width, frameRect2.width, EPSILON);
    BOOST_CHECK_CLOSE(frameRect1.pos.x, frameRect2.pos.x, EPSILON);
    BOOST_CHECK_CLOSE(frameRect1.pos.y, frameRect2.pos.y, EPSILON);
  }

  void equalsCompositeShape(malyshev::CompositeShape& compositeShape1, malyshev::CompositeShape& compositeShape2)
  {
    BOOST_CHECK_EQUAL(compositeShape1.size(), compositeShape2.size());
    equalOfShapes(compositeShape1, compositeShape2);
    for(unsigned int i = 0; i < compositeShape1.size(); i++)
    {
      equalOfShapes(*compositeShape1[i], *compositeShape2[i]);
    }
  }

  BOOST_AUTO_TEST_CASE(frameCompositeConstantAfterMove)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

    malyshev::Polygon polygon;
    polygon.add(point1);
    polygon.add(point2);
    polygon.add(point3);
    polygon.add(point4);

    //{{1, 1}, {3, 2}, {3, 4}, {1, 3}}
   // malyshev::point_t {1, 1}, malyshev::point_t{3, 2}, malyshev::point_t{3, 4}, malyshev::point_t{1, 3}
    malyshev::Shape::shape_ptr p_rectangle = std::make_shared<malyshev::Rectangle>(5, 5, malyshev::point_t {10, 10}, 0);
    malyshev::Shape::shape_ptr p_circle = std::make_shared<malyshev::Circle>(6, malyshev::point_t {3, 3});
    malyshev::Shape::shape_ptr p_triangle =
        std::make_shared<malyshev::Triangle>(malyshev::point_t {1, 1}, malyshev::point_t {3, 1}, malyshev::point_t {2, 3});
    malyshev::Shape::shape_ptr p_polygon = std::make_shared<malyshev::Polygon>(polygon);

    malyshev::CompositeShape compositeShape;
    compositeShape.add(p_rectangle);
    compositeShape.add(p_circle);
    compositeShape.add(p_triangle);
    compositeShape.add(p_polygon);
    const malyshev::rectangle_t frameRectBefore = compositeShape.getFrameRect();
    const double areaBefore = compositeShape.getArea();

    compositeShape.move(5, 5);
    BOOST_CHECK_CLOSE(frameRectBefore.width, compositeShape.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(frameRectBefore.height, compositeShape.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, compositeShape.getArea(), EPSILON);

    compositeShape.move({20, 20});
    BOOST_CHECK_CLOSE(frameRectBefore.width, compositeShape.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(frameRectBefore.height, compositeShape.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, compositeShape.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(CompositeScaleChange)
  {
    malyshev::Rectangle rectangle(4, 4, {4, 4}, 0);
    malyshev::Circle circle(2, {10, 4});

    malyshev::Shape::shape_ptr p_rectangle = std::make_shared<malyshev::Rectangle>(rectangle);
    malyshev::Shape::shape_ptr p_circle = std::make_shared<malyshev::Circle>(circle);

    malyshev::CompositeShape compositeShape;
    compositeShape.add(p_rectangle);
    compositeShape.add(p_circle);
    const double areaBefore = compositeShape.getArea();
    const double factor = 2.0;
    malyshev::point_t newShapeCenters[] = {{1, 4}, {13, 4}};

    compositeShape.scale(factor);
    for (size_t i = 0; i < 2; i++)
    {
      BOOST_CHECK_CLOSE(newShapeCenters[i].x, compositeShape[i]->getCentre().x, EPSILON);
      BOOST_CHECK_CLOSE(newShapeCenters[i].y, compositeShape[i]->getCentre().y, EPSILON);
    }
    BOOST_CHECK_CLOSE(areaBefore * factor * factor, compositeShape.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(wrongParameteresComposite)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

    malyshev::Polygon polygon;
    polygon.add(point1);
    polygon.add(point2);
    polygon.add(point3);
    polygon.add(point4);

    malyshev::Shape::shape_ptr p_rectangle = std::make_shared<malyshev::Rectangle>(5, 5, malyshev::point_t {10, 10}, 0);
    malyshev::Shape::shape_ptr p_circle = std::make_shared<malyshev::Circle>(6, malyshev::point_t {3, 3});
    malyshev::Shape::shape_ptr p_triangle =
      std::make_shared<malyshev::Triangle>(malyshev::point_t {1, 1}, malyshev::point_t {3, 1}, malyshev::point_t {2, 3});
    malyshev::Shape::shape_ptr p_polygon = std::make_shared<malyshev::Polygon>(polygon);

    malyshev::CompositeShape compositeShape;
    compositeShape.add(p_rectangle);
    compositeShape.add(p_circle);
    compositeShape.add(p_triangle);
    compositeShape.add(p_polygon);
    const double factor = -10;
    BOOST_CHECK_THROW(compositeShape.scale(factor), std::invalid_argument);
    BOOST_CHECK_THROW(compositeShape.remove(compositeShape.size()), std::invalid_argument);
    BOOST_CHECK_THROW(compositeShape.remove(-10), std::invalid_argument);
    compositeShape.remove(3);
    compositeShape.remove(2);
    compositeShape.remove(1);
    compositeShape.remove(0);
    BOOST_CHECK_THROW(compositeShape.remove(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TestCopyOperator)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

    malyshev::Polygon polygon;
    polygon.add(point1);
    polygon.add(point2);
    polygon.add(point3);
    polygon.add(point4);
    //Пустой
    malyshev::Shape::shape_ptr p_rectangle = std::make_shared<malyshev::Rectangle>(5, 5, malyshev::point_t {10, 10}, 0);
    malyshev::Shape::shape_ptr p_circle = std::make_shared<malyshev::Circle>(6, malyshev::point_t {3, 3});
    malyshev::Shape::shape_ptr p_triangle =
      std::make_shared<malyshev::Triangle>(malyshev::point_t {1, 1}, malyshev::point_t {3, 1}, malyshev::point_t {2, 3});
    malyshev::Shape::shape_ptr p_polygon = std::make_shared<malyshev::Polygon>(polygon);

    malyshev::CompositeShape compositeShape1;
    compositeShape1.add(p_rectangle);
    compositeShape1.add(p_circle);
    compositeShape1.add(p_triangle);
    compositeShape1.add(p_polygon);
    malyshev::CompositeShape compositeShape2;
    compositeShape2 = compositeShape1;
    equalsCompositeShape(compositeShape1, compositeShape2);
    //Не пустой
    malyshev::CompositeShape compositeShape3;
    compositeShape3.add(p_triangle);
    compositeShape3.add(p_circle);
    malyshev::CompositeShape compositeShape4;
    compositeShape4.add(p_polygon);
    compositeShape4 = compositeShape3;
    equalsCompositeShape(compositeShape3, compositeShape4);
  }

  BOOST_AUTO_TEST_CASE(TestCopyConstructor)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

    malyshev::Polygon polygon;
    polygon.add(point1);
    polygon.add(point2);
    polygon.add(point3);
    polygon.add(point4);

    malyshev::Shape::shape_ptr p_rectangle = std::make_shared<malyshev::Rectangle>(5, 5, malyshev::point_t {10, 10}, 0);
    malyshev::Shape::shape_ptr p_circle = std::make_shared<malyshev::Circle>(6, malyshev::point_t {3, 3});
    malyshev::Shape::shape_ptr p_triangle =
      std::make_shared<malyshev::Triangle>(malyshev::point_t {1, 1}, malyshev::point_t {3, 1}, malyshev::point_t {2, 3});
    malyshev::Shape::shape_ptr p_polygon = std::make_shared<malyshev::Polygon>(polygon);

    malyshev::CompositeShape compositeShape1;
    compositeShape1.add(p_rectangle);
    compositeShape1.add(p_circle);
    compositeShape1.add(p_triangle);
    compositeShape1.add(p_polygon);

    malyshev::CompositeShape compositeShape2(compositeShape1);
    equalsCompositeShape(compositeShape1, compositeShape2);
  }

  BOOST_AUTO_TEST_CASE(TestMoveConstructor)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

    malyshev::Polygon polygon;
    polygon.add(point1);
    polygon.add(point2);
    polygon.add(point3);
    polygon.add(point4);

    malyshev::Shape::shape_ptr p_rectangle = std::make_shared<malyshev::Rectangle>(5, 5, malyshev::point_t {10, 10}, 0);
    malyshev::Shape::shape_ptr p_circle = std::make_shared<malyshev::Circle>(6, malyshev::point_t {3, 3});
    malyshev::Shape::shape_ptr p_triangle =
      std::make_shared<malyshev::Triangle>(malyshev::point_t {1, 1}, malyshev::point_t {3, 1}, malyshev::point_t {2, 3});
    malyshev::Shape::shape_ptr p_polygon = std::make_shared<malyshev::Polygon>(polygon);

    malyshev::CompositeShape compositeShape1;
    compositeShape1.add(p_rectangle);
    compositeShape1.add(p_circle);
    compositeShape1.add(p_triangle);
    compositeShape1.add(p_polygon);

    malyshev::CompositeShape copyCompositeShape1 = compositeShape1;
    malyshev::CompositeShape compositeShape2(std::move(compositeShape1));
    equalsCompositeShape(copyCompositeShape1, compositeShape2);
  }

  BOOST_AUTO_TEST_CASE(TestMoveOperator)
  {
    point_ptr point1 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 1});
    point_ptr point2 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 2});
    point_ptr point3 = std::make_shared<malyshev::point_t> (malyshev::point_t {3, 4});
    point_ptr point4 = std::make_shared<malyshev::point_t> (malyshev::point_t {1, 3});

    malyshev::Polygon polygon;
    polygon.add(point1);
    polygon.add(point2);
    polygon.add(point3);
    polygon.add(point4);

    malyshev::Shape::shape_ptr p_rectangle = std::make_shared<malyshev::Rectangle>(5, 5, malyshev::point_t {10, 10}, 0);
    malyshev::Shape::shape_ptr p_circle = std::make_shared<malyshev::Circle>(6, malyshev::point_t {3, 3});
    malyshev::Shape::shape_ptr p_triangle =
      std::make_shared<malyshev::Triangle>(malyshev::point_t {1, 1}, malyshev::point_t {3, 1}, malyshev::point_t {2, 3});
    malyshev::Shape::shape_ptr p_polygon = std::make_shared<malyshev::Polygon>(polygon);

    malyshev::CompositeShape compositeShape1;
    compositeShape1.add(p_rectangle);
    compositeShape1.add(p_circle);
    compositeShape1.add(p_triangle);
    compositeShape1.add(p_polygon);

    malyshev::CompositeShape compositeShape2;
    malyshev::CompositeShape compositeShape3 = compositeShape1;
    compositeShape2 = std::move(compositeShape1);
    equalsCompositeShape(compositeShape3, compositeShape2);
  }


BOOST_AUTO_TEST_SUITE_END()
