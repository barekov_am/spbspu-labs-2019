#include "circle.hpp"
#include <cassert>
#include <cmath>

Circle::Circle(double radius, const point_t &centre):
  radius_(radius),
  centre_(centre)
{
  assert(radius_ > 0.0);
}

double Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

rectangle_t Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, centre_};
}

void Circle::move(const point_t &newCentrePoint)
{
  centre_ = newCentrePoint;
}

void Circle::move(const double dx, const double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}
