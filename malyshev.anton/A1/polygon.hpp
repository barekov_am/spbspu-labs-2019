#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "shape.hpp"

class Polygon : public Shape
{
public:
  Polygon(int points, point_t *const vertices);

  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &newCentrePoint) override;
  void move(double dx, double dy) override;
  point_t getCentre() const;

private:
  int points_;
  point_t *vertices_;
};

#endif
