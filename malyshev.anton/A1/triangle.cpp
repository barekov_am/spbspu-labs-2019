#include "triangle.hpp"
#include <cassert>
#include <cmath>

Triangle::Triangle(const point_t &point1, const point_t &point2, const point_t &point3):
  point1_(point1),
  point2_(point2),
  point3_(point3),
  centre_({(point1_.x + point2_.x + point3_.x) / 3, (point1_.y + point2_.y + point3_.y) / 3})
{
  assert(getArea() > 0);
}

double Triangle::getArea() const
{
  return fabs(0.5 * (((point1_.x - point3_.x) * (point2_.y - point3_.y))
      - ((point2_.x - point3_.x) * (point1_.y - point3_.y))));
}

rectangle_t Triangle::getFrameRect() const
{
  const double right = fmax(point1_.x, fmax(point2_.x, point3_.x));
  const double left = fmin(point1_.x, fmin(point2_.x, point3_.x));
  const double top = fmax(point1_.y, fmax(point2_.y, point3_.y));
  const double bottom = fmin(point1_.y, fmin(point2_.y, point3_.y));
  const double width = right - left;
  const double height = top - bottom;
  const double xOfCentre = (left + right) / 2;
  const double yOfCentre = (top + bottom) / 2;
  return {width, height, {xOfCentre, yOfCentre}};
}

void Triangle::move(const point_t &newCentrePoint)
{
  move(newCentrePoint.x - centre_.x, newCentrePoint.y - centre_.y);
  centre_ = newCentrePoint;
}

void Triangle::move(const double dx, const double dy)
{
  point1_.x += dx;
  point1_.y += dy;
  point2_.x += dx;
  point2_.y += dy;
  point3_.x += dx;
  point3_.y += dy;
  centre_.x += dx;
  centre_.y += dy;
}

point_t Triangle::getCentre() const
{
  return centre_;
}
