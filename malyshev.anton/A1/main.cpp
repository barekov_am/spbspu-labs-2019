#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void printShapeInfo(const Shape &shape)
{
  std::cout << "Area: " << shape.getArea() << std::endl;
  rectangle_t const frameRect = shape.getFrameRect();
  std::cout << "Width of a frame rectangle: " << frameRect.width << ", ";
  std::cout << "Height of a frame rectangle: " << frameRect.height << ", ";
  std::cout << "Centre of a frame rectangle: (" << frameRect.pos.x << ", " << frameRect.pos.y << ")" << std::endl;
}

int main()
{
  Rectangle rectangle(5, 5, {10, 10});
  std::cout << "_________________Rectangle_________________" << std::endl;
  printShapeInfo(rectangle);
  rectangle.move({6, 6});
  std::cout << "Rectangle after first move:" << std::endl;
  std::cout << "Centre of a figure: (" << rectangle.getFrameRect().pos.x;
  std::cout << ", " << rectangle.getFrameRect().pos.y << ")" << std::endl;
  printShapeInfo(rectangle);
  rectangle.move(2.3, 2.4);
  std::cout << "Rectangle after second move:" << std::endl;
  std::cout << "Centre of a figure: (" << rectangle.getFrameRect().pos.x;
  std::cout << ", " << rectangle.getFrameRect().pos.y << ")" << std::endl;
  printShapeInfo(rectangle);

  Circle circle(3, {3, 3});
  std::cout << "_________________Circle_________________:" << std::endl;
  printShapeInfo(circle);
  circle.move({5, 5});
  std::cout << "Circle after first move:" << std::endl;
  std::cout << "Centre of a figure: (" << circle.getFrameRect().pos.x;
  std::cout << ", " << circle.getFrameRect().pos.y << ")" << std::endl;
  printShapeInfo(circle);
  circle.move(1.1, 1.2);
  std::cout << "Circle after second move:" << std::endl;
  std::cout << "Centre of a figure: (" << circle.getFrameRect().pos.x;
  std::cout << ", " << circle.getFrameRect().pos.y << ")" << std::endl;
  printShapeInfo(circle);

  Triangle triangle({1, 1}, {3, 1}, {2, 3});
  std::cout << "_________________triangle_________________:" << std::endl;
  printShapeInfo(triangle);
  triangle.move({4, 4});
  std::cout << "Triangle after first move:" << std::endl;
  std::cout << "Centre of a figure: (" << triangle.getCentre().x << ", " << triangle.getCentre().y << ")" << std::endl;
  printShapeInfo(triangle);
  triangle.move(4.4, -4.4);
  std::cout << "Triangle after second move:" << std::endl;
  std::cout << "Centre of a figure: (" << triangle.getCentre().x << ", " << triangle.getCentre().y << ")" << std::endl;
  printShapeInfo(triangle);

  point_t vertices[] = {{1, 1}, {3, 2}, {3, 4}, {1, 3}}; //координаты многоугольника задаются по порядку, против часовой стрелки
  Polygon polygon(4, vertices);
  std::cout << "_________________polygon_________________:" << std::endl;
  printShapeInfo(polygon);
  polygon.move({4, 4});
  std::cout << "Polygon after first move:" << std::endl;
  std::cout << "Centre of a figure: (" << polygon.getCentre().x << ", " << polygon.getCentre().y << ")" << std::endl;
  printShapeInfo(polygon);
  polygon.move(4.4, -4.4);
  std::cout << "Polygon after second move:" << std::endl;
  std::cout << "Centre of a figure: (" << polygon.getCentre().x << ", " << polygon.getCentre().y << ")" << std::endl;
  printShapeInfo(polygon);
  return 0;
}
