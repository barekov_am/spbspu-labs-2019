#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"

namespace malyshev
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix& other);
    Matrix(Matrix&& other) noexcept;
    ~Matrix() = default;
    Matrix& operator =(const Matrix& other);
    Matrix& operator =(Matrix&& other) noexcept;
    std::unique_ptr<Shape::shape_ptr []> operator [](size_t index) const;
    bool operator ==(const Matrix& other) const;
    bool operator !=(const Matrix& other) const;

    Shape::shape_ptr get(size_t i, size_t j) const;

    void add(Shape::shape_ptr shape, size_t row, size_t column);
    size_t getRows() const;
    size_t getColumns() const;

  private:
    size_t rows_;
    size_t columns_;
    std::unique_ptr<Shape::shape_ptr []> list_;
  };
}

#endif
