#include <utility>

#include "matrix.hpp"
#include <stdexcept>

malyshev::Matrix::Matrix() :
  rows_(0),
  columns_(0)
{
}

malyshev::Matrix::Matrix(const Matrix& other) :
  rows_(other.rows_),
  columns_(other.columns_),
  list_(std::make_unique<Shape::shape_ptr[]>(other.rows_ * other.columns_))
{
  for (size_t i = 0; i < rows_ * columns_; i++)
  {
    list_[i] = other.list_[i];
  }
}

malyshev::Matrix::Matrix(Matrix&& other) noexcept :
  rows_(other.rows_),
  columns_(other.columns_),
  list_(std::move(other.list_))
{
}

malyshev::Matrix& malyshev::Matrix::operator =(const Matrix& other)
{
  if (this != &other)
  {
    std::unique_ptr<Shape::shape_ptr[]> tmpList(std::make_unique<Shape::shape_ptr[]>(other.rows_ * other.columns_));
    rows_ = other.rows_;
    columns_ = other.columns_;
    for (size_t i = 0; i < rows_ * columns_; i++)
    {
      list_[i] = other.list_[i];
    }
    list_.swap(tmpList);
  }
  return *this;
}

malyshev::Matrix& malyshev::Matrix::operator =(Matrix&& other) noexcept
{
  if (this != &other)
  {
    rows_ = other.rows_;
    columns_ = other.columns_;
    list_ = std::move(other.list_);
  }
  return *this;
}

std::unique_ptr<malyshev::Shape::shape_ptr[]> malyshev::Matrix::operator [](size_t index) const
{
  if (index >= rows_)
  {
    throw std::invalid_argument("Wrong index");
  }

  std::unique_ptr<Shape::shape_ptr[]> tmpList(std::make_unique<Shape::shape_ptr[]>(columns_));
  for (size_t i = 0; i < columns_; i++)
  {
    tmpList[i] = list_[index * columns_ + i];
  }
  return tmpList;
}

bool malyshev::Matrix::operator ==(const Matrix& other) const
{
  if ((rows_ != other.rows_) || (columns_ != other.columns_))
  {
    return false;
  }
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (list_[i] != other.list_[i])
    {
      return false;
    }
  }
  return true;
}

bool malyshev::Matrix::operator !=(const Matrix& other) const
{
  return !(*this == other);
}

malyshev::Shape::shape_ptr malyshev::Matrix::get(size_t i, size_t j) const
{
  size_t index = i * columns_ + j;
  if (index > rows_ * columns_)
  {
    throw std::invalid_argument("Wrong index");
  }

  return list_[index];
}

void malyshev::Matrix::add(Shape::shape_ptr shape, size_t row, size_t column)
{
  size_t tmpRows = rows_;
  if (rows_ == row)
  {
    tmpRows = rows_ + 1;
  }
  size_t tmpColumns = columns_;
  if (columns_ == column)
  {
    tmpColumns = columns_ + 1;
  }
  std::unique_ptr<Shape::shape_ptr[]> tmpList(std::make_unique<Shape::shape_ptr[]>(tmpRows * tmpColumns));

  for (size_t i = 0; i < tmpRows; i++)
  {
    for (size_t j = 0; j < tmpColumns; j++)
    {
      if ((i != rows_) && (j != columns_))
      {
        tmpList[i * tmpColumns + j] = list_[i * columns_ + j];
      }
    }
  }
  tmpList[row * tmpColumns + column] = std::move(shape);
  list_.swap(tmpList);
  rows_ = tmpRows;
  columns_ = tmpColumns;
}

size_t malyshev::Matrix::getRows() const
{
  return rows_;
}

size_t malyshev::Matrix::getColumns() const
{
  return columns_;
}
