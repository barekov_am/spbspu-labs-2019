#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace malyshev
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape& other);
    CompositeShape(CompositeShape&& other) noexcept;
    CompositeShape& operator =(const CompositeShape& other);
    CompositeShape& operator =(CompositeShape&& other) noexcept;
    shape_ptr operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &newCentrePoint) override;
    void move(double dx, double dy) override;
    void scale(double factor) override;
    point_t getCentre() const override;
    void rotate(double alpha) override;

    void add(shape_ptr newShape);
    void remove(size_t index);
    size_t size() const;

  private:
    size_t size_;
    std::unique_ptr<shape_ptr []> shapes_;
  };
}

#endif
