#include "shape.hpp"

bool malyshev::Shape::intersect(const Shape::shape_ptr shape1, Shape::shape_ptr shape2)
{
  if ((shape1 == nullptr) || (shape2 == nullptr))
  {
    return false;
  }
  const rectangle_t rect1 = shape1->getFrameRect();
  const rectangle_t rect2 = shape2->getFrameRect();
  point_t horizon1 = {rect1.pos.x - rect1.width / 2, rect1.pos.x + rect1.width / 2};
  point_t horizon2 = {rect2.pos.x - rect2.width / 2, rect2.pos.x + rect2.width / 2};
  point_t vertical1 = {rect1.pos.y - rect1.height / 2, rect1.pos.y + rect1.height / 2};
  point_t vertical2 = {rect2.pos.y - rect2.height / 2, rect2.pos.y + rect2.height / 2};

  bool horizonIntersect = (horizon1.x <= horizon2.y && horizon1.y >= horizon2.x) ||
                          (horizon2.x <= horizon1.y && horizon2.y >= horizon1.x);
  bool verticalIntersect = (vertical1.x <= vertical2.y && vertical1.y >= vertical2.x) ||
                           (vertical2.x <= vertical1.y && vertical2.y >= vertical1.x);
  return horizonIntersect && verticalIntersect;
}
