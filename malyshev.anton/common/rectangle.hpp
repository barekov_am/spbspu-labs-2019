#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace malyshev
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(double width, double height, const point_t &centre, double angle);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &newCentrePoint) override;
    void move(double dx, double dy) override;
    void scale(double factor) override;
    point_t getCentre() const override;
    void rotate(double alpha) override;
    double getAngle() const;

  private:
    double width_, height_;
    point_t centre_;
    double angle_;
  };
}

#endif
