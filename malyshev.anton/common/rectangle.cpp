#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

malyshev::Rectangle::Rectangle(const double width, const double height, const point_t &centre, double angle):
  width_(width),
  height_(height),
  centre_(centre),
  angle_(angle)
{
  if ((width_ < 0.0) || (height_ < 0.0))
  {
    throw std::invalid_argument("width or height <= 0");
  };
}

double malyshev::Rectangle::getArea() const
{
  return width_ * height_;
}

malyshev::rectangle_t malyshev::Rectangle::getFrameRect() const
{
  double frameRectWidth = fabs(width_ * cos(angle_ * (M_PI / 180)) + height_ * sin(angle_ * (M_PI / 180)));
  double frameRectHeight = fabs(height_ * cos(angle_ * (M_PI / 180)) + width_ * sin(angle_ * (M_PI / 180)));
  return {frameRectWidth, frameRectHeight, centre_};
}

void malyshev::Rectangle::move(const point_t &newCentrePoint)
{
  centre_ = newCentrePoint;
}

void malyshev::Rectangle::move(const double dx, const double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

malyshev::point_t malyshev::Rectangle::getCentre() const
{
  return centre_;
}

void malyshev::Rectangle::scale(const double factor)
{
  if (factor <= 0.0)
  {
    throw std::invalid_argument("Factor of scaling < 0");
  }
  width_ *= factor;
  height_ *= factor;
}

void malyshev::Rectangle::rotate(double alpha)
{
  angle_ += alpha;
  angle_ = fmod(angle_, 360);
}

double malyshev::Rectangle::getAngle() const
{
  return angle_;
}
