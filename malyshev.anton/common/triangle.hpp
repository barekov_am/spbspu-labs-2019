#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace malyshev
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &point1, const point_t &point2, const point_t &point3);
    double getArea() const override;

    rectangle_t getFrameRect() const override;
    void move(const point_t &newCentrePoint) override;
    void move(double dx, double dy) override;
    point_t getCentre() const override;
    void scale(double factor) override;
    void rotate(double alpha) override;

  private:
    point_t point1_, point2_, point3_, centre_;
  };
}

#endif
