#include "splitting.hpp"
#include <cmath>

malyshev::Matrix malyshev::split(const malyshev::CompositeShape& compositeShape)
{
  Matrix matrix;
  size_t correctRow = 0;
  size_t correctColumn = 0;
  for (size_t i = 0; i < compositeShape.size(); i++)
  {
    for (size_t j = 0; j < matrix.getRows(); j++)
    {
      for (size_t k = 0; k < matrix.getColumns(); k++)
      {
        if (matrix.get(j, k) == nullptr)
        {
          correctRow = j;
          correctColumn = k;
          break;
        }

        if (CompositeShape::intersect(compositeShape[i], matrix.get(j, k)))
        {
          correctRow = j + 1;
          correctColumn = 0;
          break;
        }
        else
        {
          correctRow = j;
          correctColumn = k + 1;
        }
      }

      if (correctRow == j)
      {
        break;
      }
    }
    matrix.add(compositeShape[i], correctRow, correctColumn);
  }
  return matrix;
}
