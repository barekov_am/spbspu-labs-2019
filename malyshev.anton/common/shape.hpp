#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"
#include <memory>

namespace malyshev
{
  class Shape
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t &newCentrePoint) = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void scale(double factor) = 0;
    virtual point_t getCentre() const = 0;
    virtual void rotate(double alpha) = 0;

    static bool intersect(shape_ptr shape1, shape_ptr shape2);
  };
}

#endif
