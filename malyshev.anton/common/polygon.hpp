#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "shape.hpp"

namespace malyshev
{
  class Polygon : public Shape
  {
  public:
    using point_ptr = std::shared_ptr<point_t>;

    Polygon();
    Polygon(const Polygon& poly);
    Polygon(Polygon&& poly) noexcept;
    Polygon& operator =(const Polygon& poly);
    Polygon& operator =(Polygon&& poly) noexcept;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &newCentrePoint) override;
    void move(double dx, double dy) override;
    point_t getCentre() const override;
    void scale(double factor) override;
    void rotate(double alpha) override;

    void add(point_ptr newPoint);
    void remove(size_t index);

  private:
    size_t points_;
    std::unique_ptr<point_ptr []> vertices_;
  };
}

#endif
