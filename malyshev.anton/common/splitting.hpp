#ifndef SPLITTING_HPP
#define SPLITTING_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace malyshev
{
  Matrix split(const CompositeShape& compositeShape);
}

#endif
