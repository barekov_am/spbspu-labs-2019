#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace malyshev
{
  class Circle : public Shape
  {
  public:
    Circle(double radius, const point_t &centre);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &newCentrePoint) override;
    void move(double dx, double dy) override;
    void scale(double factor) override;
    point_t getCentre() const override;
    void rotate(double) override;

  private:
    double radius_;
    point_t centre_;
  };
}

#endif
