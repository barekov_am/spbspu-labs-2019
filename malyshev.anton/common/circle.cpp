#include "circle.hpp"
#include <cmath>
#include <stdexcept>

malyshev::Circle::Circle(const double radius, const point_t &centre):
  radius_(radius),
  centre_(centre)
{
  if (radius_ < 0.0)
  {
    throw std::invalid_argument("Radius <= 0");
  };
}

double malyshev::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

malyshev::rectangle_t malyshev::Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, centre_};
}

void malyshev::Circle::move(const point_t &newCentrePoint)
{
  centre_ = newCentrePoint;
}

void malyshev::Circle::move(const double dx, const double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

void malyshev::Circle::scale(const double factor)
{
  if (factor <= 0.0)
  {
    throw std::invalid_argument("Factor of scaling < 0");
  }
  radius_ *= factor;
}

malyshev::point_t malyshev::Circle::getCentre() const
{
  return centre_;
}

void malyshev::Circle::rotate(double)
{
}
