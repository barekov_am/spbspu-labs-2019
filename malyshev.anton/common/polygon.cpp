#include "polygon.hpp"
#include <stdexcept>
#include <cmath>

malyshev::Polygon::Polygon():
  points_(0)
{
}

malyshev::Polygon::Polygon(const Polygon& poly):
  points_(poly.points_),
  vertices_(std::make_unique<point_ptr []>(poly.points_))
{
  std::copy(poly.vertices_.get(), poly.vertices_.get() + poly.points_, vertices_.get());
}

malyshev::Polygon::Polygon(Polygon&& poly) noexcept:
  points_(poly.points_),
  vertices_(std::move(poly.vertices_))
{
  poly.points_ = 0;
}

malyshev::Polygon& malyshev::Polygon::operator =(const Polygon& poly)
{
  if (this != &poly)
  {
    std::unique_ptr<point_ptr []> tmpArray(std::make_unique<point_ptr []>(poly.points_));
    for (size_t i = 0; i < poly.points_; i++)
    {
      tmpArray[i] = poly.vertices_[i];
    }
    vertices_.swap(tmpArray);
    points_ = poly.points_;
  }
  return *this;
}

malyshev::Polygon& malyshev::Polygon::operator =(Polygon&& poly) noexcept
{
  if (this != &poly)
  {
    points_ = poly.points_;
    vertices_ = std::move(poly.vertices_);
    poly.points_ = 0;
  }
  return *this;
}

double malyshev::Polygon::getArea() const
{
  if (points_ < 3)
  {
    throw std::invalid_argument("this is not a polygon, number of vertices < 3");
  }

  double sum = (vertices_[points_ - 1]->x * vertices_[0]->y) - (vertices_[0]->x * vertices_[points_ - 1]->y);
  for (size_t i = 0; (i < (points_ - 1)); ++i)
  {
    sum += (vertices_[i]->x * vertices_[i + 1]->y) - (vertices_[i + 1]->x * vertices_[i]->y);
  }
  return sum / 2;
}

malyshev::rectangle_t malyshev::Polygon::getFrameRect() const
{
  if (points_ < 3)
  {
    throw std::invalid_argument("this is not a polygon, number of vertices < 3");
  }

  double right = vertices_[0]->x;
  double left = vertices_[0]->x;
  double top = vertices_[0]->y;
  double bottom = vertices_[0]->y;
  for (size_t i = 1; (i < points_); ++i)
  {
    left = std::fmin(vertices_[i]->x, left);
    right = std::fmax(vertices_[i]->x, right);
    bottom = std::fmin(vertices_[i]->y, bottom);
    top = std::fmax(vertices_[i]->y, top);
  }
  const double width = right - left;
  const double height = top - bottom;
  const double xOfCentre = (left + right) / 2;
  const double yOfCentre = (top + bottom) / 2;
  return {width, height, {xOfCentre, yOfCentre}};
}

void malyshev::Polygon::move(const point_t &newCentrePoint)
{
  if (points_ < 3)
  {
    throw std::invalid_argument("this is not a polygon, number of vertices < 3");
  }

  move(newCentrePoint.x - getCentre().x, newCentrePoint.y - getCentre().y);
}

void malyshev::Polygon::move(const double dx, const double dy)
{
  if (points_ < 3)
  {
    throw std::invalid_argument("this is not a polygon, number of vertices < 3");
  }

  for (size_t i = 0; (i < points_); ++i)
  {
    vertices_[i]->x += dx;
    vertices_[i]->y += dy;
  }
}

malyshev::point_t malyshev::Polygon::getCentre() const
{
  if (points_ < 3)
  {
    throw std::invalid_argument("this is not a polygon, number of vertices < 3");
  }

  double sumX = 0.0;
  double sumY = 0.0;
  for (size_t i = 0; (i < points_); ++i)
  {
    sumX += vertices_[i]->x;
    sumY += vertices_[i]->y;
  }
  return {(sumX / points_), (sumY / points_)};
}


void malyshev::Polygon::scale(const double factor)
{
  if (points_ < 3)
  {
    throw std::invalid_argument("this is not a polygon, number of vertices < 3");
  }

  if (factor <= 0.0)
  {
    throw std::invalid_argument("Factor of scaling < 0");
  }

  const point_t centre = getCentre();
  for (size_t i = 0; i < points_; ++i)
  {
    vertices_[i]->x = centre.x + (vertices_[i]->x - centre.x) * factor;
    vertices_[i]->y = centre.y + (vertices_[i]->y - centre.y) * factor;
  }
}

void malyshev::Polygon::rotate(double alpha)
{
  if (points_ < 3)
  {
    throw std::invalid_argument("this is not a polygon, number of vertices < 3");
  }

  alpha = fmod(alpha, 360);
  point_t center = getCentre();
  for (size_t i = 0; i < points_; ++i)
  {
    point_t newPoint = {0, 0};
    point_t vectorLength = {(vertices_[i]->x - center.x), (vertices_[i]->y - center.y)};
    newPoint.x = vectorLength.x * cos(alpha * (M_PI / 180))
        - vectorLength.y * sin(alpha * (M_PI / 180)) + center.x;
    newPoint.y = vectorLength.x * sin(alpha * (M_PI / 180))
        + vectorLength.y * cos(alpha * (M_PI / 180)) + center.y;
    vertices_[i]->x = newPoint.x;
    vertices_[i]->y = newPoint.y;
  }
}

void malyshev::Polygon::add(point_ptr newPoint)
{
  if (newPoint == nullptr)
  {
    throw std::invalid_argument("newPoint is nullptr");
  }

  std::unique_ptr<point_ptr []> tmpArray(std::make_unique<point_ptr []>(points_ + 1));

  for (size_t i = 0; i < points_; i++)
  {
    tmpArray[i] = vertices_[i];
  }
  tmpArray[points_++] = newPoint;
  vertices_.swap(tmpArray);
  if (points_ > 2)
  {
    for (size_t i = 0; (i < points_); ++i)
    {
      if (((vertices_[(i + 1) % points_]->x - vertices_[i % points_]->x)
           * (vertices_[(i + 2) % points_]->y - vertices_[(i + 1) % points_]->y)
           - ((vertices_[(i + 2) % points_]->x - vertices_[(i + 1) % points_]->x)
              * (vertices_[(i + 1) % points_]->y - vertices_[i % points_]->y))) < 0)
      {
        throw std::invalid_argument("polygon are not convex");
      }
    }
    if (getArea() <= 0)
    {
      throw std::invalid_argument("Area = 0");
    }
  }
}

void malyshev::Polygon::remove(size_t index)
{
  if (points_ == 0)
  {
    throw std::invalid_argument("Size = 0");
  }
  if (index >= points_)
  {
    throw std::invalid_argument("Index is outside of the array boundaries");
  }
  for (size_t i = index; i < points_ - 1; i++)
  {
    vertices_[i] = vertices_[i + 1];
  }
  points_--;
}
