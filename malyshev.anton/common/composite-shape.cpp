#include "composite-shape.hpp"
#include <stdexcept>
#include <cmath>

malyshev::CompositeShape::CompositeShape() :
  size_(0)
{
}

malyshev::CompositeShape::CompositeShape(const CompositeShape& other) :
  size_(other.size_),
  shapes_(std::make_unique<shape_ptr []>(other.size_))
{
  std::copy(other.shapes_.get(), other.shapes_.get() + other.size_, shapes_.get());
}

malyshev::CompositeShape::CompositeShape(CompositeShape&& other) noexcept :
  size_(other.size_),
  shapes_(std::move(other.shapes_))
{
  other.size_ = 0;
}

malyshev::CompositeShape& malyshev::CompositeShape::operator =(const CompositeShape& other)
{
  if (this != &other)
  {
    std::unique_ptr<shape_ptr []> tmpArray(std::make_unique<shape_ptr []>(other.size_));
    for (size_t i = 0; i < other.size_; i++)
    {
      tmpArray[i] = other.shapes_[i];
    }
    shapes_.swap(tmpArray);
    size_ = other.size_;
  }
  return *this;
}

malyshev::CompositeShape& malyshev::CompositeShape::operator =(CompositeShape&& other) noexcept
{
  if (this != &other)
  {
    size_ = other.size_;
    shapes_ = std::move(other.shapes_);
    other.size_ = 0;
  }
  return *this;
}

malyshev::Shape::shape_ptr malyshev::CompositeShape::operator [](size_t index) const
{
  if (size_ <= index)
  {
    throw std::invalid_argument("Wrong index");
  }
  return shapes_[index];
}

double malyshev::CompositeShape::getArea() const
{
  double areaSum = 0;
  for (size_t i = 0; i < size_; i++)
  {
    areaSum += shapes_[i]->getArea();
  }
  return areaSum;
}

malyshev::rectangle_t malyshev::CompositeShape::getFrameRect() const
{
  if (shapes_ == nullptr)
  {
    return {0, 0, {0, 0}};
  }
  else
  {
    rectangle_t tempFrameRect = shapes_[0]->getFrameRect();
    double left = tempFrameRect.pos.x - tempFrameRect.width / 2;
    double right = tempFrameRect.pos.x + tempFrameRect.width / 2;
    double top = tempFrameRect.pos.y + tempFrameRect.height / 2;
    double bottom = tempFrameRect.pos.y - tempFrameRect.height / 2;
    for (size_t i = 1; i < size_; i++)
    {
      tempFrameRect = shapes_[i]->getFrameRect();
      left = std::min(left, tempFrameRect.pos.x - tempFrameRect.width / 2);
      right = std::max(right, tempFrameRect.pos.x + tempFrameRect.width / 2);
      top = std::max(top, tempFrameRect.pos.y + tempFrameRect.height / 2);
      bottom = std::min(bottom, tempFrameRect.pos.y - tempFrameRect.height / 2);
    }
    return {right - left, top - bottom, {(right + left) / 2, (top + bottom) / 2}};
  }
}

void malyshev::CompositeShape::move(const malyshev::point_t &newCentrePoint)
{
  point_t Centre = getCentre();
  double deltaX = newCentrePoint.x - Centre.x;
  double deltaY = newCentrePoint.y - Centre.y;
  move(deltaX, deltaY);
}

void malyshev::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void malyshev::CompositeShape::scale(double factor)
{
  point_t centreOfCompositeShape = getCentre();
  for (size_t i = 0; i < size_; i++)
  {
    point_t newShapeCentre = {0, 0};
    newShapeCentre.x = centreOfCompositeShape.x + (shapes_[i]->getCentre().x - centreOfCompositeShape.x) * factor;
    newShapeCentre.y = centreOfCompositeShape.y + (shapes_[i]->getCentre().y - centreOfCompositeShape.y) * factor;
    shapes_[i]->move(newShapeCentre);
    shapes_[i]->scale(factor);
  }
}

malyshev::point_t malyshev::CompositeShape::getCentre() const
{
  return getFrameRect().pos;
}

void malyshev::CompositeShape::add(shape_ptr newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Shape is nullptr");
  }
  std::unique_ptr<shape_ptr []> tmpArray(std::make_unique<shape_ptr []>(size_ + 1));

  for (size_t i = 0; i < size_; i++)
  {
    tmpArray[i] = shapes_[i];
  }
  tmpArray[size_++] = newShape;
  shapes_.swap(tmpArray);
}


void malyshev::CompositeShape::remove(const size_t index)
{
  if (size_ == 0)
  {
    throw std::invalid_argument("Size = 0");
  }
  if (index >= size_)
  {
    throw std::invalid_argument("Index is outside of the array boundaries");
  }
  for (size_t i = index; i < size_ - 1; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  size_--;
}

size_t malyshev::CompositeShape::size() const
{
  return size_;
}

void malyshev::CompositeShape::rotate(double alpha)
{
  point_t compositeCenter = getCentre();
  for (size_t i = 0; i < size_; i++)
  {
    point_t shapeCenter = shapes_[i]->getCentre();
    point_t vectorLength = {shapeCenter.x - compositeCenter.x, shapeCenter.y - compositeCenter.y};
    point_t newShapeCenter = {0,0};
    newShapeCenter.x = vectorLength.x * cos(alpha * (M_PI / 180))
        - vectorLength.y * sin(alpha * (M_PI / 180)) + compositeCenter.x;
    newShapeCenter.y =  vectorLength.x * sin(alpha * (M_PI / 180))
        + vectorLength.y * cos(alpha * (M_PI / 180)) + compositeCenter.y;
    shapes_[i]->move(newShapeCenter);
    shapes_[i]->rotate(alpha);
  }
}
