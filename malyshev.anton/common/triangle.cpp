#include "triangle.hpp"
#include <stdexcept>
#include <cmath>

malyshev::Triangle::Triangle(const point_t &point1, const point_t &point2, const point_t &point3):
  point1_(point1),
  point2_(point2),
  point3_(point3),
  centre_({(point1_.x + point2_.x + point3_.x) / 3, (point1_.y + point2_.y + point3_.y) / 3})
{
  if (getArea() < 0)
  {
    throw std::invalid_argument("Area = 0");
  }
}

double malyshev::Triangle::getArea() const
{
  return fabs(0.5 * ((point1_.x - point3_.x) * (point2_.y - point3_.y)
      - (point2_.x - point3_.x) * (point1_.y - point3_.y)));
}

malyshev::rectangle_t malyshev::Triangle::getFrameRect() const
{
  const double right = fmax(point1_.x, fmax(point2_.x, point3_.x));
  const double left = fmin(point1_.x, fmin(point2_.x, point3_.x));
  const double top = fmax(point1_.y, fmax(point2_.y, point3_.y));
  const double bottom = fmin(point1_.y, fmin(point2_.y, point3_.y));
  const double width = right - left;
  const double height = top - bottom;
  const double xOfCentre = (left + right) / 2;
  const double yOfCentre = (top + bottom) / 2;
  return {width, height, {xOfCentre, yOfCentre}};
}

void malyshev::Triangle::move(const point_t &newCentrePoint)
{
  move(newCentrePoint.x - centre_.x, newCentrePoint.y - centre_.y);
  centre_ = newCentrePoint;
}

void malyshev::Triangle::move(const double dx, const double dy)
{
  point1_.x += dx;
  point1_.y += dy;
  point2_.x += dx;
  point2_.y += dy;
  point3_.x += dx;
  point3_.y += dy;
  centre_.x += dx;
  centre_.y += dy;
}

malyshev::point_t malyshev::Triangle::getCentre() const
{
  return centre_;
}

void malyshev::Triangle::scale(const double factor)
{
  if (factor <= 0.0)
  {
    throw std::invalid_argument("Factor of scaling < 0");
  }
  point1_.x = centre_.x + (point1_.x - centre_.x) * factor;
  point1_.y = centre_.y + (point1_.y - centre_.y) * factor;
  point2_.x = centre_.x + (point2_.x - centre_.x) * factor;
  point2_.y = centre_.y + (point2_.y - centre_.y) * factor;
  point3_.x = centre_.x + (point3_.x - centre_.x) * factor;
  point3_.y = centre_.y + (point3_.y - centre_.y) * factor;
}

void malyshev::Triangle::rotate(double alpha)
{
  alpha = fmod(alpha, 360);
  point_t p1 = {0, 0};
  point_t p2 = {0, 0};
  point_t p3 = {0, 0};
  double sinus = sin(alpha * (M_PI / 180));
  double cosinus = cos(alpha * (M_PI / 180));

  p1.x = (point1_.x - centre_.x) * cosinus - (point1_.y - centre_.y) * sinus + centre_.x;
  p1.y = (point1_.x - centre_.x) * sinus + (point1_.y - centre_.y) * cosinus + centre_.y;

  p2.x = (point2_.x - centre_.x) * cosinus - (point2_.y - centre_.y) * sinus + centre_.x;
  p2.y = (point2_.x - centre_.x) * sinus + (point2_.y - centre_.y) * cosinus + centre_.y;

  p3.x = (point3_.x - centre_.x) * cosinus - (point3_.y - centre_.y) * sinus + centre_.x;
  p3.y = (point3_.x - centre_.x) * sinus + (point3_.y - centre_.y) * cosinus + centre_.y;

  point1_ = p1;
  point2_ = p2;
  point3_ = p3;
}
