#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
  malinin::Circle circle(20.0, {2.0, -3.0});
  malinin::Rectangle rectangle(19.0, 7.0, {-15.0, -17.0});
  malinin::Triangle triangle({1.0, 1.0}, {2.0, 2.0}, {3.0, 1.0});

  malinin::Shape * figure = &rectangle;

  std::cout << "\nRECTANGLE\n";
  figure->showInfo();
  std::cout << "\nMove (to new position using point) X: -7.3, Y: 3.7\n";
  figure->move({-7.3, 3.7});
  figure->showCords();
  std::cout << "\nMove (using coordinates) X: -3.2, Y: 5.9\n";
  figure->move(-3.2, 5.9);
  figure->showCords();
  figure->showFrameRect();
  std::cout << "\nScaling (Factor = 5.1)\n";
  figure->scale(5.1);
  figure->showInfo();

  std::cout << "\n==================================================\n";

  figure = &circle;

  std::cout << "\nCIRCLE\n";
  figure->showInfo();
  std::cout << "\nMove (to new position using point) X: 5.1, Y: -2.2\n";
  figure->move({5.1, -2.2});
  figure->showCords();
  std::cout << "\nMove (using coordinates) X: -1.0, Y: 2.0\n";
  figure->move(-1.0, 2.0);
  figure->showCords();
  figure->showFrameRect();
  std::cout << "\nScaling (Factor = 4.3)\n";
  figure->scale(4.3);
  figure->showInfo();

  std::cout << "\n==================================================\n";

  figure = &triangle;

  std::cout << "\nTRIANGLE\n";
  figure->showInfo();
  figure->showCords();
  std::cout << "\nMove (to new position using point) X: 7.2, Y: -1.0\n";
  figure->move({7.2, -1.0});
  figure->showCords();
  std::cout << "\nMove (using coordinates) X: 2.4, Y: 3.5\n";
  figure->move(2.4, 3.5);
  figure->showCords();
  figure->showFrameRect();
  std::cout << "\nScaling (Factor = 1.5)\n";
  figure->scale(1.5);
  figure->showInfo();

  std::cout << "\n==================================================\n";
  std::cout << "\nCOMPOSITE-SHAPE\n";

  malinin::shape_ptr rectanglePtr = std::make_shared<malinin::Rectangle>(rectangle);
  malinin::shape_ptr circlePtr = std::make_shared<malinin::Circle>(circle);
  malinin::shape_ptr trianglePtr = std::make_shared<malinin::Triangle>(triangle);

  malinin::CompositeShape composite1;
  std::cout << "\nEmpty composite-shape info\n";
  composite1.showInfo();

  composite1.add(trianglePtr);
  composite1.add(rectanglePtr);
  std::cout << "\nAfter adding of rectangle and triangle\n";
  composite1.showInfo();

  composite1.remove(1);
  composite1.add(circlePtr);
  std::cout << "\nAfter removing of triangle and adding of circle\n";
  composite1.showInfo();

  std::cout << "\nMove (to new position using point) X: 2.9, Y: -3.3\n";
  composite1.move({2.9, -3.3});
  composite1.showInfo();

  std::cout << "\nMove (using coordinates) X: 3.1, Y: 5.3\n";
  composite1.move({2.9, -3.3});
  composite1.showInfo();

  std::cout << "\nScaling (Factor = 1.1)\n";
  composite1.scale(1.1);
  composite1.showInfo();

  malinin::CompositeShape composite2;
  composite2.add(trianglePtr);
  composite2.add(rectanglePtr);
  composite1 = composite2;
  std::cout << "\nCopy composite2 to composite1\n";
  composite1.showInfo();

  return (0);
}
