#include <boost/test/auto_unit_test.hpp>

#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double ACCURACY = 0.0001;

BOOST_AUTO_TEST_SUITE(constructorsTestSuite)

BOOST_AUTO_TEST_CASE(compositeDefaultConstructorTest)
{
  malinin::CompositeShape testComposite;
  BOOST_CHECK_EQUAL(testComposite.getQuantity(), 0);
}

BOOST_AUTO_TEST_CASE(compositeCopyConstructorTest)
{
  malinin::shape_ptr circle = std::make_shared<malinin::Circle>(malinin::Circle (1.9, {2.1, 0.4}));
  malinin::CompositeShape composite1;
  composite1.add(circle);
  const unsigned int initQty = composite1.getQuantity();

  malinin::CompositeShape composite2(composite1);

  BOOST_CHECK_EQUAL(composite1.getQuantity(), initQty);
  BOOST_CHECK_EQUAL(composite1.getQuantity(), composite2.getQuantity());
}

BOOST_AUTO_TEST_CASE(compositeMoveConstructorTest)
{
  malinin::shape_ptr circle = std::make_shared<malinin::Circle>(malinin::Circle (1.9, {2.1, 0.4}));
  malinin::shape_ptr rectangle = std::make_shared<malinin::Rectangle>(malinin::Rectangle (2.4, 1.1, {0.0, 0.1}));
  malinin::CompositeShape composite1;
  composite1.add(circle);
  composite1.add(rectangle);
  const unsigned int initQty = composite1.getQuantity();

  malinin::CompositeShape composite2(std::move(composite1));

  BOOST_CHECK_EQUAL(composite1.getQuantity(), 0);
  BOOST_CHECK_EQUAL(composite2.getQuantity(), initQty);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(compositeOperatorsTestSuite)

BOOST_AUTO_TEST_CASE(compositeCopyingOperatorTest)
{
  malinin::CompositeShape composite1;
  malinin::shape_ptr circle = std::make_shared<malinin::Circle>(malinin::Circle (2.0, {-1.5, 2.0}));
  composite1.add(circle);
  const int initQty = composite1.getQuantity();
  malinin::CompositeShape composite2;

  composite2 = composite1;

  BOOST_CHECK_EQUAL(composite2.getQuantity(), initQty);
  BOOST_CHECK_EQUAL(composite1.getQuantity(), composite2.getQuantity());
}

BOOST_AUTO_TEST_CASE(compositeMovementOperatorTest)
{
  malinin::CompositeShape composite1;
  malinin::shape_ptr circle = std::make_shared<malinin::Circle>(malinin::Circle (2.0, {-1.5, 2.0}));
  const int initQty = composite1.getQuantity();
  malinin::CompositeShape composite2;

  composite2 = std::move(composite1);

  BOOST_CHECK_EQUAL(composite1.getQuantity(), 0);
  BOOST_CHECK_EQUAL(composite2.getQuantity(), initQty);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(movementsTestSuite)

BOOST_AUTO_TEST_CASE(compositeToPointMovementTest)
{
  malinin::CompositeShape composite;
  malinin::shape_ptr circle = std::make_shared<malinin::Circle>(malinin::Circle (4.0, {2.3, 4.0}));
  malinin::shape_ptr rectangle = std::make_shared<malinin::Rectangle>( malinin::Rectangle (3.0, 4.0, {2.5, 3.5}));
  composite.add(circle);
  composite.add(rectangle);
  const double initArea = composite.getArea();
  const malinin::rectangle_t initFrame = composite.getFrameRect();

  composite.move({2.4, 5.6});

  const malinin::rectangle_t frameAfterMoveTo = composite.getFrameRect();
  BOOST_CHECK_CLOSE(initArea, composite.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initFrame.width, frameAfterMoveTo.width, ACCURACY);
  BOOST_CHECK_CLOSE(initFrame.height, frameAfterMoveTo.height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShiftMovementTest)
{
  malinin::CompositeShape composite;
  malinin::shape_ptr circle = std::make_shared<malinin::Circle>(malinin::Circle (4.0, {2.3, 4.0}));
  malinin::shape_ptr rectangle = std::make_shared<malinin::Rectangle>( malinin::Rectangle (3.0, 4.0, {2.5, 3.5}));

  composite.add(circle);
  composite.add(rectangle);

  const double initArea = composite.getArea();
  const malinin::rectangle_t initFrame = composite.getFrameRect();

  composite.move(2.4, 5.6);

  const malinin::rectangle_t newFrameShift = composite.getFrameRect();
  BOOST_CHECK_CLOSE(initArea, composite.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initFrame.width, newFrameShift.width, ACCURACY);
  BOOST_CHECK_CLOSE(initFrame.height, newFrameShift.height, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(scaleTestSuite)

BOOST_AUTO_TEST_CASE(compositeScaleTest)
{
  malinin::CompositeShape composite;
  malinin::shape_ptr circle = std::make_shared<malinin::Circle>(malinin::Circle (4.0, {2.3, 4.0}));
  malinin::shape_ptr rectangle = std::make_shared<malinin::Rectangle>( malinin::Rectangle (3.0, 4.0, {2.5, 3.5}));

  composite.add(circle);
  composite.add(rectangle);

  const double initArea = composite.getArea();
  const double factor = 3.0;

  composite.scale(factor);

  BOOST_CHECK_CLOSE(initArea * factor * factor, composite.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(rotateTestSuite)

BOOST_AUTO_TEST_CASE(compositeRotateTest)
{
  malinin::CompositeShape composite;
  malinin::shape_ptr circle = std::make_shared<malinin::Circle>(malinin::Circle (4.0, {2.3, 4.0}));
  malinin::shape_ptr rectangle = std::make_shared<malinin::Rectangle>( malinin::Rectangle (3.0, 4.0, {2.5, 3.5}));

  composite.add(circle);
  composite.add(rectangle);

  const double initArea = composite.getArea();
  const malinin::rectangle_t initFrame = composite.getFrameRect();

  composite.rotate(60.0);

  BOOST_CHECK_CLOSE(initArea, composite.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initFrame.pos.x, composite.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initFrame.pos.y, composite.getFrameRect().pos.y, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(invalidParametersTestSuite)

BOOST_AUTO_TEST_CASE(compositeInvalidArgumentsHandlingTest)
{
  malinin::CompositeShape composite;
  malinin::shape_ptr circle = std::make_shared<malinin::Circle>(malinin::Circle (4.0, {2.3, 4.0}));

  composite.add(circle);
  
  BOOST_CHECK_THROW(composite.scale(-3), std::invalid_argument);
  BOOST_CHECK_THROW(composite.remove(10), std::invalid_argument);
  BOOST_CHECK_THROW(composite.remove(-10), std::invalid_argument);
  BOOST_CHECK_THROW(composite[1], std::out_of_range);
  BOOST_CHECK_THROW(composite[-2], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END();
