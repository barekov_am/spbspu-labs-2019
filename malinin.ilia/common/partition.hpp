#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace malinin
{
  Matrix split(const shapes_array &, size_t);
  Matrix split(const CompositeShape &compositeShape);
  bool intersect(const rectangle_t &, const rectangle_t &);
}

#endif
