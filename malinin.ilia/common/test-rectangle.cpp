#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

const double ACCURACY = 0.0001;

BOOST_AUTO_TEST_SUITE(movementsTestSuite)

BOOST_AUTO_TEST_CASE(rectangleToPointMovementTest)
{
  const double initWidth = 3.0;
  const double initHeight = 4.0;
  malinin::Rectangle testRect (initWidth, initHeight, {0.0, 0.0});
  const double initArea = testRect.getArea();

  testRect.move({1.0, 5.0});

  BOOST_CHECK_CLOSE(initWidth, testRect.getWidth(), ACCURACY);
  BOOST_CHECK_CLOSE(initHeight, testRect.getHeight(), ACCURACY);
  BOOST_CHECK_CLOSE(initArea, testRect.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(rectangleShiftMovementsTest)
{
  const double initWidth = 3.0;
  const double initHeight = 4.0;
  malinin::Rectangle testRect (initWidth, initHeight, {0.0, 0.0});
  const double initArea = testRect.getArea();

  testRect.move(-1.5, 5.4);

  BOOST_CHECK_CLOSE(initWidth, testRect.getWidth(), ACCURACY);
  BOOST_CHECK_CLOSE(initHeight, testRect.getHeight(), ACCURACY);
  BOOST_CHECK_CLOSE(initArea, testRect.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(scaleTestSuite)

BOOST_AUTO_TEST_CASE(rectangleScaleTest)
{
  const double factor = 2.3;
  const double initWidth = 7.2;
  const double initHeight = 4.9;
  malinin::Rectangle testRect (initWidth, initHeight, {0.0, 0.0});
  const double initArea = testRect.getArea();

  testRect.scale(factor);

  BOOST_CHECK_CLOSE(initWidth * factor, testRect.getWidth(), ACCURACY);
  BOOST_CHECK_CLOSE(initHeight * factor, testRect.getHeight(), ACCURACY);
  BOOST_CHECK_CLOSE(initArea * factor * factor, testRect.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(rotateTestSuite)

BOOST_AUTO_TEST_CASE(rectangleRotateTest)
{
  const double initWidth = 7.2;
  const double initHeight = 4.9;
  malinin::Rectangle testRect (initWidth, initHeight, {0.0, 0.0});
  const malinin::rectangle_t initFrame = testRect.getFrameRect();
  const double initArea = testRect.getArea();

  testRect.rotate(60.0);

  BOOST_CHECK_CLOSE(initWidth, testRect.getWidth(), ACCURACY);
  BOOST_CHECK_CLOSE(initHeight, testRect.getHeight(), ACCURACY);
  BOOST_CHECK_CLOSE(initArea, testRect.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initFrame.pos.x, testRect.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initFrame.pos.y, testRect.getFrameRect().pos.y, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END();


BOOST_AUTO_TEST_SUITE(invalidParametersTestSuite)

BOOST_AUTO_TEST_CASE(rectangleInvalidArgumentsHandlingTest)
{
  BOOST_CHECK_THROW(malinin::Rectangle(-4.3, 1.0, {0.0, 0.0}), std::invalid_argument);
  BOOST_CHECK_THROW(malinin::Rectangle(0.0, 2.3, {0.0, 0.0}), std::invalid_argument);
  BOOST_CHECK_THROW(malinin::Rectangle(0.0, 0.0, {0.0, 0.0}), std::invalid_argument);
  BOOST_CHECK_THROW(malinin::Rectangle(0.0, -1.0, {0.0, 0.0}), std::invalid_argument);
  BOOST_CHECK_THROW(malinin::Rectangle(2.5, -1.0, {0.0, 0.0}), std::invalid_argument);
  BOOST_CHECK_THROW(malinin::Rectangle(3.1, 0.0, {0.0, 0.0}), std::invalid_argument);

  BOOST_CHECK_THROW(malinin::Rectangle(5.6, 2.9, {0.0, 0.0}).scale(-1.0), std::invalid_argument);
  BOOST_CHECK_THROW(malinin::Rectangle(7.8, 4.4, {0.0, 0.0}).scale(0.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END();
