#include "rectangle.hpp"
#include <iostream>
#include <math.h>

malinin::Rectangle::Rectangle(const double & width, const double & height, const point_t & point) :
  rect_({width, height, point}),
  angle_(0.0)
{
  if (width <= 0.0)
  {
    throw std::invalid_argument("Invalid width");
  }

  if (height <= 0.0)
  {
    throw std::invalid_argument("Invalid height");
  }
}

double malinin::Rectangle::getArea() const
{
  return (rect_.width * rect_.height);
}

malinin::rectangle_t malinin::Rectangle::getFrameRect() const
{
  return {rect_.width, rect_.height, rect_.pos};
}

void malinin::Rectangle::move(const double & dX, const double & dY)
{
  rect_.pos.x += dX;
  rect_.pos.y += dY;
}

void malinin::Rectangle::move(const point_t & point)
{
  rect_.pos = point;
}

void malinin::Rectangle::showInfo() const
{
  std::cout << "\nInformation about rectangle";
  std::cout << "\nWidth: " << getWidth();
  std::cout << "\nHeight: " << getHeight() << "\n";
  std::cout << "\nArea: " << getArea() << "\n";
  std::cout << "\nAngle: "  << angle_ << "\n";
  showCords();
}

void malinin::Rectangle::showCords() const
{
  std::cout << "Center: ";
  std::cout << "\n X: " << rect_.pos.x;
  std::cout << "\n Y: " << rect_.pos.y << "\n";
}

void malinin::Rectangle::showFrameRect() const
{
  rectangle_t rect = getFrameRect();
  std::cout << "\nBounding rectangle";
  std::cout << "\n Width: " << rect.width;
  std::cout << "\n Height: " << rect.height << "\n";
  showCords();
}

double malinin::Rectangle::getWidth() const
{
  return (rect_.width);
}

double malinin::Rectangle::getHeight() const
{
  return (rect_.height);
}

void malinin::Rectangle::scale(double factor)
{
  if (factor <= 0.0)
  {
    throw std::invalid_argument("Invalid factor");
  }
  else
  {
    rect_.width *= factor;
    rect_.height *= factor;
  }
}

void malinin::Rectangle::rotate(double angle)
{
  angle_ += angle;
  if (angle_ < 0)
  {
    angle_ = 360 + fmod(angle_, 360);
  }
}
