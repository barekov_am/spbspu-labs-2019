#include <boost/test/auto_unit_test.hpp>

#include "triangle.hpp"

const double ACCURACY = 0.0001;

BOOST_AUTO_TEST_SUITE(movementsTestSuite)

BOOST_AUTO_TEST_CASE(triangleToPointMovementTest)
{
  malinin::Triangle testTriangle = {{2.3, -8.6}, {-1.1, -5.3}, {3.2, 7.8}};
  const double initArea = testTriangle.getArea();

  testTriangle.move({4.9, -2.3});

  BOOST_CHECK_CLOSE(initArea, testTriangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(triangleShiftMovementsTest)
{
  malinin::Triangle testTriangle = {{5.3, 2.6}, {7.1, 6.3}, {1.2, 3.8}};
  const double initArea = testTriangle.getArea();

  testTriangle.move(-2.7, 5.8);

  BOOST_CHECK_CLOSE(initArea, testTriangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(scaleTestSuite)

BOOST_AUTO_TEST_CASE(triangleScaleTest)
{
  const double factor = 2.1;
  malinin::Triangle testTriangle({4.9, -1.6}, {5.1, 5.3}, {-3.8, 5.9});
  const double initArea = testTriangle.getArea();

  testTriangle.scale(factor);

  BOOST_CHECK_CLOSE(initArea * factor * factor, testTriangle.getArea(), ACCURACY);
}
BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(invalidParametersTestSuite)

BOOST_AUTO_TEST_CASE(triangleInvalidArgumentsHandlingTest)
{
  BOOST_CHECK_THROW(malinin::Triangle({1.0, 1.0}, {2.0, 1.0}, {3.0, 1.0}), std::invalid_argument);

  BOOST_CHECK_THROW(malinin::Triangle({1.0, 1.0}, {2.0, 2.0}, {3.0, 1.0}).scale(-1.1), std::invalid_argument);
  BOOST_CHECK_THROW(malinin::Triangle({1.0, 1.0}, {2.0, 2.0}, {3.0, 1.0}).scale(0.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END();
