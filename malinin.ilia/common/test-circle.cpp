#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

const double ACCURACY = 0.0001;

BOOST_AUTO_TEST_SUITE(movementsTestSuite)

BOOST_AUTO_TEST_CASE(circleToPointMovementTest)
{
  const double initRadius = 3.14;
  malinin::Circle testCircle = {initRadius, {0.0, 0.0}};
  const double initArea = testCircle.getArea();

  testCircle.move({4.1, 6.3});

  BOOST_CHECK_CLOSE(initRadius, testCircle.getRadius(), ACCURACY);
  BOOST_CHECK_CLOSE(initArea, testCircle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(circleShiftMovementsTest)
{
  const double initRadius = 2.9;
  malinin::Circle testCircle = {initRadius, {0.0, 0.0}};
  const double initArea = testCircle.getArea();

  testCircle.move(-3.0, -1.6);

  BOOST_CHECK_CLOSE(initRadius, testCircle.getRadius(), ACCURACY);
  BOOST_CHECK_CLOSE(initArea, testCircle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(scaleTestSuite)

BOOST_AUTO_TEST_CASE(circleScaleTest)
{
  const double factor = 4.4;
  const double initRadius = 2.4;
  malinin::Circle testCircle (initRadius, {0.0, 0.0});
  const double initArea = testCircle.getArea();

  testCircle.scale(factor);

  BOOST_CHECK_CLOSE(initRadius * factor, testCircle.getRadius(), ACCURACY);
  BOOST_CHECK_CLOSE(initArea * factor * factor, testCircle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(rotateTestSuite)

BOOST_AUTO_TEST_CASE(circleRotateTest)
{
  const double initRadius = 2.4;
  malinin::point_t initPoint = {0.0, 0.0};
  malinin::Circle testCircle(initRadius, initPoint);
  const double initArea = testCircle.getArea();
  const malinin::rectangle_t initFrame = testCircle.getFrameRect();

  testCircle.rotate(60.3);

  BOOST_CHECK_CLOSE(initRadius, testCircle.getRadius(), ACCURACY);
  BOOST_CHECK_CLOSE(initArea, testCircle.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initFrame.pos.x, testCircle.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initFrame.pos.y, testCircle.getFrameRect().pos.y, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(invalidParametersTestSuite)

BOOST_AUTO_TEST_CASE(circleInvalidArgumentsHandlingTest)
{
  BOOST_CHECK_THROW(malinin::Circle(-2.3, {0.0, 0.0}), std::invalid_argument);
  BOOST_CHECK_THROW(malinin::Circle(0.0, {0.0, 0.0}), std::invalid_argument);

  BOOST_CHECK_THROW(malinin::Circle(7.5, {0.0, 0.0}).scale(-2.5), std::invalid_argument);
  BOOST_CHECK_THROW(malinin::Circle(4.2, {0.0, 0.0}).scale(0.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END();
