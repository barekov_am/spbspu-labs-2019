#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <math.h>

malinin::CompositeShape::CompositeShape():
  qty_(0)
{
}

malinin::CompositeShape::CompositeShape(const CompositeShape & rhs):
  qty_(rhs.qty_),
  shapes_(std::make_unique<shape_ptr[]>(rhs.qty_))
{
  for (size_t i = 0; i < qty_; i++)
  {
    shapes_[i] = rhs.shapes_[i];
  }
}

malinin::CompositeShape::CompositeShape(CompositeShape && rhs):
  qty_(rhs.qty_),
  shapes_(std::move(rhs.shapes_))
{
  rhs.qty_ = 0;
}

malinin::CompositeShape& malinin::CompositeShape::operator =(const CompositeShape & rhs)
{
  if (this != &rhs)
  {
    shapes_array shapesArray(std::make_unique<shape_ptr[]>(rhs.qty_));
    qty_ = rhs.qty_;
    for (size_t i = 0; i < qty_; i++)
    {
      shapesArray[i] = rhs.shapes_[i];
    }

    shapes_.swap(shapesArray);
  }

  return *this;
}

malinin::CompositeShape& malinin::CompositeShape::operator =(CompositeShape && rhs)
{
  if (this != &rhs)
  {
    qty_ = rhs.qty_;
    shapes_ = std::move(rhs.shapes_);
    rhs.qty_ = 0;
  }

  return *this;
}

malinin::shape_ptr malinin::CompositeShape::operator [](size_t index) const
{
  if (index >= qty_)
  {
    throw std::out_of_range("Index out of range");
  }

  return shapes_[index];
}

double malinin::CompositeShape::getArea() const
{
  double area = 0.0;
  for (size_t i = 0; i < qty_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

malinin::rectangle_t malinin::CompositeShape::getFrameRect() const
{
  if (qty_ <= 0)
  {
    return {0.0, 0.0, {0.0, 0.0}};
  }

  rectangle_t frameShape = shapes_[0]->getFrameRect();

  point_t min = {frameShape.pos.x - frameShape.width / 2, frameShape.pos.y - frameShape.height / 2};
  point_t max = {frameShape.pos.x + frameShape.width / 2, frameShape.pos.y + frameShape.height / 2};

  for (size_t i = 1; i < qty_; i++)
  {
    frameShape = shapes_[i]->getFrameRect();
    min.x = std::min((frameShape.pos.x - frameShape.width / 2), min.x);
    min.y = std::min((frameShape.pos.y - frameShape.height / 2), min.y);

    max.x = std::max((frameShape.pos.x + frameShape.width / 2), max.x);
    max.y = std::max((frameShape.pos.y + frameShape.height / 2), max.y);
  }
  const double width = max.x - min.x;
  const double height = max.y - min.y;

  return {width, height, {min.x + width / 2, min.y + height / 2}
  };
}

void malinin::CompositeShape::move(const double & dX, const double & dY)
{
  for (size_t i = 0; i < qty_; i++)
  {
    shapes_[i]->move(dX, dY);
  }
}

void malinin::CompositeShape::move(const point_t& newPos)
{
  const point_t posComposite = getPos();
  move(newPos.x - posComposite.x, newPos.y - posComposite.y);
}

void malinin::CompositeShape::showInfo() const
{
  if (qty_ != 0)
  {
    std::cout << "\nInformation about Composite-Shape\n";
    showCords();
    std::cout << "\nArea: " << getArea() << "\n";
    showFrameRect();
    std::cout << "\nQuantity of shapes: " << qty_;
    std::cout << "\nContent";
    showContent();
  }
  else
  {
    std::cout << "\nComposite-Shape is empty.";
  }
}

void malinin::CompositeShape::showCords() const
{
  point_t pos = getFrameRect().pos;

  std::cout << "\nCenter: ";
  std::cout << "\n X: " << pos.x;
  std::cout << "\n Y: " << pos.y << "\n";
}

void malinin::CompositeShape::showFrameRect() const
{
  rectangle_t rect = getFrameRect();
  std::cout << "\nBounding rectangle";
  std::cout << "\n Width: " << rect.width;
  std::cout << "\n Height: " << rect.height << "\n";
  showCords();
}

void malinin::CompositeShape::showContent() const
{
  if (qty_ != 0)
  {
    for (size_t i = 0; i < qty_; i++)
    {
      std::cout << "\n============";
      std::cout << "\nShape No.: " << i + 1 << "\n";
      shapes_[i]->showInfo();
    }
    std::cout << "\n============";
  }
  else
  {
    std::cout << "\nComposite-Shape is empty.";
  }
}

void malinin::CompositeShape::scale(double factor)
{
  if (factor <= 0.0) {
    throw std::invalid_argument("Invalid Factor");
  }

  const point_t compositePos = getPos();
  for (size_t i = 0; i < qty_; i++)
  {
    const point_t shapePos = shapes_[i]->getFrameRect().pos;
    double dX = (factor - 1) * (shapePos.x - compositePos.x);
    double dY = (factor - 1) * (shapePos.y - compositePos.y);

    shapes_[i]->move(dX, dY);
    shapes_[i]->scale(factor);
  }
}

malinin::point_t malinin::CompositeShape::getPos() const
{
  return getFrameRect().pos;
}

void malinin::CompositeShape::add(shape_ptr &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer can't be null");
  }

  shapes_array shapesArray(std::make_unique<shape_ptr[]>(qty_ + 1));
  for(size_t i = 0; i < qty_; i++)
  {
    shapesArray[i] = shapes_[i];
  }

  shapesArray[qty_] = shape;
  qty_++;
  shapes_.swap(shapesArray);
}

void malinin::CompositeShape::remove(size_t indexRem)
{
  if (indexRem >= qty_)
  {
    throw std::invalid_argument("Index out of range");
  }

  qty_--;
  for (size_t i = indexRem; i < qty_ ; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }

  shapes_[qty_] = nullptr;
}

size_t malinin::CompositeShape::getQuantity() const
{
  return qty_;
}

malinin::shapes_array malinin::CompositeShape::getFigures() const
{
  shapes_array tmpFigures(std::make_unique<shape_ptr[]>(qty_));

  for (size_t i = 0; i < qty_; i++)
  {
    tmpFigures[i] = shapes_[i];
  }

  return tmpFigures;
}

void malinin::CompositeShape::rotate(double angle)
{
  const double cosA = std::abs(std::cos(angle * M_PI / 180));
  const double sinA = std::abs(std::sin(angle * M_PI / 180));
  const point_t center = getFrameRect().pos;

  for (size_t i = 0; i < qty_; i++)
  {
    point_t tmpCenter = shapes_[i]->getFrameRect().pos;
    const double dX = (tmpCenter.x - center.x) * (cosA - 1) - (tmpCenter.y - center.y)  * sinA;
    const double dY = (tmpCenter.x - center.x) * sinA + (tmpCenter.y - center.y)  * (cosA - 1);

    shapes_[i]->move(dX, dY);
    shapes_[i]->rotate(angle);
  }
}
