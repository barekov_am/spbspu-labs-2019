#include "triangle.hpp"
#include <iostream>
#include <math.h>

malinin::Triangle::Triangle(const malinin::point_t &A, const malinin::point_t &B, const malinin::point_t &C) :
  angles_ {A, B, C}
{
  if (getArea() <= 0.0)
  {
    throw std::invalid_argument("Invalid init point");
  }
}

double malinin::Triangle::getArea() const
{
  return fabs(((angles_[1].x - angles_[0].x) * (angles_[2].y - angles_[0].y)
    - (angles_[2].x - angles_[0].x)
      * (angles_[1].y - angles_[0].y) / 2.0));
}

malinin::rectangle_t malinin::Triangle::getFrameRect() const
{
  point_t right_bottom_angle = angles_[0];
  point_t left_top_angle = angles_[0];
  for (int i = 1; i < 3; ++i)
  {
    right_bottom_angle.x = std::max(right_bottom_angle.x, angles_[i].x);
    right_bottom_angle.y = std::min(right_bottom_angle.y, angles_[i].y);

    left_top_angle.x = std::min(left_top_angle.x, angles_[i].x);
    left_top_angle.y = std::max(left_top_angle.y, angles_[i].y);
  }

  return {right_bottom_angle.x - left_top_angle.x,
    left_top_angle.y - right_bottom_angle.y,
      {(right_bottom_angle.x + left_top_angle.x) / 2.0,
        (right_bottom_angle.y + left_top_angle.y) / 2.0}};
}

void malinin::Triangle::move(const double & dX, const double & dY)
{
  for (int i = 0; i < 3; ++i)
  {
    angles_[i].x += dX;
    angles_[i].y += dY;
  }
}

void malinin::Triangle::move(const malinin::point_t &point)
{
  malinin::point_t center = getCenter();
  move(point.x - center.x, point.y - center.y);
}

malinin::point_t malinin::Triangle::getCenter() const
{
  return {(angles_[0].x + angles_[1].x + angles_[2].x) / 3.0,
    (angles_[0].y + angles_[1].y + angles_[2].y) / 3.0};
}

void malinin::Triangle::showInfo() const
{
  std::cout << "Information about triangle\n";
  std::cout << "Area: " << getArea() << "\n";
  showCords();
  showFrameRect();
}

void malinin::Triangle::showCords() const
{
  std::cout << "Angle A";
  std::cout << "\n X: " << angles_[0].x;
  std::cout << "\n Y: " << angles_[0].y;
  std::cout << "\nAngle B";
  std::cout << "\n X: " << angles_[1].x;
  std::cout << "\n Y: " << angles_[1].y;
  std::cout << "\nAngle C";
  std::cout << "\n X: " << angles_[2].x;
  std::cout << "\n Y: " << angles_[2].y << "\n";
}

void malinin::Triangle::showFrameRect() const
{
  rectangle_t rect = getFrameRect();
  std::cout << "\nBounding rectangle";
  std::cout << "\n Width: " << rect.width;
  std::cout << "\n Height: " << rect.height;
  std::cout << "\nCenter";
  std::cout << "\n X: " << rect.pos.x;
  std::cout << "\n Y: " << rect.pos.y << "\n";
}

void malinin::Triangle::scale(double factor)
{
  if (factor <= 0.0)
  {
    throw std::invalid_argument("Invalid factor");
  }
  else
  {
    point_t center = getCenter();

    for (int i = 0; i < 3; ++i)
    {
      angles_[i].x = center.x + factor * (angles_[i].x - center.x);
      angles_[i].y = center.y + factor * (angles_[i].y - center.y);
    }
  }
}

void malinin::Triangle::rotate(double angle)
{
  for (int i = 0; i < 3; ++i)
  {
    angles_[i].x = angles_[i].x * cos(M_PI * angle / 180) - angles_[i].y * sin(M_PI * angle / 180);
    angles_[i].y = angles_[i].y * cos(M_PI * angle / 180) + angles_[i].x * sin(M_PI * angle / 180);
  }
}
