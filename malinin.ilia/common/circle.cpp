#include "circle.hpp"
#include <iostream>
#include <math.h>

malinin::Circle::Circle(const double & radius, const point_t & point) :
  radius_(radius),
  pos_(point)
{
  if (radius <= 0.0)
  {
    throw std::invalid_argument("Incorrect radius");
  }
}

double malinin::Circle::getArea() const
{
  return (M_PI * (radius_ * radius_));
}

malinin::rectangle_t malinin::Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, pos_};
}

void malinin::Circle::move(const double & dX, const double & dY)
{
  pos_.x += dX;
  pos_.y += dY;
}

void malinin::Circle::move(const point_t &point)
{
  pos_ = point;
}

void malinin::Circle::showInfo() const
{
  std::cout << "Information about circle\n";
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "Angle: " << "0.0 degrees (this is circle!)";
  showCords();
  showFrameRect();
}

void malinin::Circle::showCords() const
{
  std::cout << "Center: ";
  std::cout << "\n X: " << pos_.x;
  std::cout << "\n Y: " << pos_.y << "\n";
}

void malinin::Circle::showFrameRect() const
{
  rectangle_t rect = getFrameRect();
  std::cout << "\nBounding rectangle";
  std::cout << "\n Width: " << rect.width;
  std::cout << "\n Height: " << rect.height << "\n";
  showCords();
}

double malinin::Circle::getRadius() const
{
  return (radius_);
}

void malinin::Circle::scale(double factor)
{
  if (factor <= 0.0)
  {
    throw std::invalid_argument("Invalid scale factor");
  }
  else
  {
    radius_ *= factor;
  }
}

void malinin::Circle::rotate(double )
{
}
