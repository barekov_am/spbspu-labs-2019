#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "base-types.hpp"
#include "shape.hpp"

namespace malinin
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape & rhs);
    CompositeShape(CompositeShape && rhs);
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape & rhs);
    CompositeShape& operator =(CompositeShape && rhs);
    shape_ptr operator [](size_t) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const double & dX, const double & dY) override;
    void move(const point_t & point) override;
    void showInfo() const override;
    void showCords() const override;
    void showFrameRect() const override;

    void scale(double factor) override;

    void showContent() const;
    point_t getPos() const;

    void rotate(double angle) override;

    void add(shape_ptr & shape);
    void remove(size_t index);
    size_t getQuantity() const;
    shapes_array getFigures() const;

  private:
    size_t qty_;
    std::unique_ptr<shape_ptr[]> shapes_;
  };
}

#endif
