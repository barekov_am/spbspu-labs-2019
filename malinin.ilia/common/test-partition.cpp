#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(partitionMetodsTests)

BOOST_AUTO_TEST_CASE(partitionTest)
{
  const malinin::Rectangle rectangle1(2.0, 1.0, {0.0, 0.0});
  malinin::shape_ptr rectOnePtr = std::make_shared<malinin::Rectangle>(rectangle1);
  const malinin::Rectangle rectangle2(2.0, 3.0, {15.0, 10.0});
  malinin::shape_ptr rectTwoPtr = std::make_shared<malinin::Rectangle>(rectangle2);
  const malinin::Circle circ(3.0, {1.0, -1.0});
  malinin::shape_ptr circPtr = std::make_shared<malinin::Circle>(circ);

  malinin::CompositeShape composite;
  composite.add(rectOnePtr);
  malinin::Matrix matrix1 = malinin::split(composite);

  composite.add(circPtr);
  malinin::Matrix matrix2 = malinin::split(composite);

  composite.add(rectTwoPtr);
  malinin::Matrix matrix3 = malinin::split(composite);

  BOOST_CHECK_EQUAL(matrix1.getLines(), 1);
  BOOST_CHECK_EQUAL(matrix1.getColumns(), 1);

  BOOST_CHECK_EQUAL(matrix2.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix2.getColumns(), 1);

  BOOST_CHECK_EQUAL(matrix3.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix3.getColumns(), 2);
}

BOOST_AUTO_TEST_CASE(intersectionTest)
{
  const malinin::Rectangle rectangle1(5.0, 4.0, {1.0, 2.0});
  const malinin::Rectangle rectangle2(3.0, 1.0, {7.0, 7.0});
  const malinin::Circle circle1(2.0, {-1.0, 2.0});
  const malinin::Circle circle2(4.0, {0.0, 1.0});

  BOOST_CHECK(intersect(rectangle1.getFrameRect(), circle1.getFrameRect()));
  BOOST_CHECK(intersect(rectangle1.getFrameRect(), circle2.getFrameRect()));
  BOOST_CHECK(!intersect(rectangle1.getFrameRect(), rectangle2.getFrameRect()));
  BOOST_CHECK(!intersect(circle1.getFrameRect(), rectangle2.getFrameRect()));
  BOOST_CHECK(!intersect(circle2.getFrameRect(), rectangle2.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
