#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace malinin
{
  class Shape
  {
  public:
    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const double & dX, const double & dY) = 0;
    virtual void move(const point_t & point) = 0;
    virtual void showInfo() const = 0;
    virtual void showCords() const = 0;
    virtual void showFrameRect() const = 0;
    virtual void scale(double factor) = 0;
    virtual void rotate(double angle) = 0;
  };

  using shape_ptr = std::shared_ptr<Shape>;
  using shapes_array = std::unique_ptr<shape_ptr[]>;
}

#endif
