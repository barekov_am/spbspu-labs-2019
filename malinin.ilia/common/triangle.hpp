#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "base-types.hpp"
#include "shape.hpp"

namespace malinin
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t & A, const point_t & B, const point_t & C);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const double & dX, const double & dY) override;
    void move(const point_t & point) override;

    point_t getCenter() const;
    void showInfo() const override;
    void showCords() const override;
    void showFrameRect() const override;

    void scale(double factor) override;

    void rotate(double angle) override;

  private:
    point_t angles_[3];
  };
}

#endif
