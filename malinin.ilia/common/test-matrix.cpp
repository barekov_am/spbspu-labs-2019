#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(matrixTests)

BOOST_AUTO_TEST_CASE(matrixCopyConstructor)
{
  malinin::shape_ptr rectangle = std::make_shared<malinin::Rectangle>(malinin::Rectangle (1.2, 3.5, {0.0, 3.2}));
  malinin::CompositeShape composite;
  composite.add(rectangle);
  malinin::Matrix copiedMatrix = split(composite);

  malinin::Matrix matrix(copiedMatrix);

  BOOST_CHECK_EQUAL(matrix.getLines(), copiedMatrix.getLines());
  BOOST_CHECK_EQUAL(matrix.getColumns(), copiedMatrix.getColumns());
  BOOST_CHECK(matrix == copiedMatrix);
}

BOOST_AUTO_TEST_CASE(matrixCopyOperator)
{
  malinin::shape_ptr rectangle = std::make_shared<malinin::Rectangle>(malinin::Rectangle (1.2, 3.5, {0.0, 3.2}));
  malinin::CompositeShape composite;
  composite.add(rectangle);
  malinin::Matrix copiedMatrix = split(composite);

  malinin::Matrix matrix;
  matrix = copiedMatrix;

  BOOST_CHECK_EQUAL(matrix.getLines(), copiedMatrix.getLines());
  BOOST_CHECK_EQUAL(matrix.getColumns(), copiedMatrix.getColumns());
  BOOST_CHECK(matrix == copiedMatrix);
}

BOOST_AUTO_TEST_CASE(matrixMoveConstructor)
{
  malinin::shape_ptr rectangle = std::make_shared<malinin::Rectangle>(malinin::Rectangle (1.2, 3.5, {0.0, 3.2}));
  malinin::CompositeShape composite;
  composite.add(rectangle);
  malinin::Matrix matrix = split(composite);

  malinin::Matrix copyOfMatrix(matrix);
  malinin::Matrix moveMatrix(std::move(matrix));

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyOfMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyOfMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyOfMatrix);
  BOOST_CHECK_EQUAL(matrix.getLines(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(matrixMoveOperator)
{
  malinin::shape_ptr rectangle = std::make_shared<malinin::Rectangle>(malinin::Rectangle (1.2, 3.5, {0.0, 3.2}));
  malinin::CompositeShape composite;
  composite.add(rectangle);
  malinin::Matrix matrix = split(composite);

  malinin::Matrix copyOfMatrix(matrix);
  malinin::Matrix moveMatrix;
  moveMatrix = std::move(matrix);

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyOfMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyOfMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyOfMatrix);
  BOOST_CHECK_EQUAL(matrix.getLines(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(testMatrixUsingOfEqualOperator)
{
  malinin::shape_ptr rectangle1 = std::make_shared<malinin::Rectangle>(malinin::Rectangle (1.2, 3.5, {0.0, 3.2}));
  malinin::shape_ptr rectangle2 = std::make_shared<malinin::Rectangle>(malinin::Rectangle (7.6, 4.9, {4.9, 2.2}));
  malinin::shape_ptr circle1 = std::make_shared<malinin::Circle>(malinin::Circle (1.9, {2.7, 3.5}));
  malinin::shape_ptr circle2 = std::make_shared<malinin::Circle>(malinin::Circle (5.1, {5.3, 1.9}));

  malinin::CompositeShape composite;
  composite.add(rectangle1);
  composite.add(circle1);
  composite.add(rectangle2);

  malinin::Matrix matrix = split(composite);
  malinin::Matrix equalMatrix(matrix);

  malinin::Matrix unequalMatrix(matrix);
  unequalMatrix.add(circle2, 1, 1);

  BOOST_CHECK(matrix == equalMatrix);
  BOOST_CHECK(matrix != unequalMatrix);
}

BOOST_AUTO_TEST_CASE(matrixThrowExceptionAfterUsingOfOperator)
{
  malinin::shape_ptr rectangle1 = std::make_shared<malinin::Rectangle>(malinin::Rectangle (1.2, 3.5, {0.0, 3.2}));
  malinin::shape_ptr rectangle2 = std::make_shared<malinin::Rectangle>(malinin::Rectangle (7.6, 4.9, {4.9, 2.2}));
  malinin::shape_ptr circle1 = std::make_shared<malinin::Circle>(malinin::Circle (1.9, {2.7, 3.5}));
  malinin::shape_ptr circle2 = std::make_shared<malinin::Circle>(malinin::Circle (5.1, {5.3, 1.9}));

  malinin::CompositeShape composite;
  composite.add(rectangle1);
  composite.add(circle1);
  composite.add(rectangle2);
  composite.add(circle2);

  malinin::Matrix matrix = split(composite);

  BOOST_CHECK_THROW(matrix[10][5], std::out_of_range);
  BOOST_CHECK_NO_THROW(matrix[2][1]);
}

BOOST_AUTO_TEST_SUITE_END()
