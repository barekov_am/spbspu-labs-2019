#include "matrix.hpp"
#include <stdexcept>
#include <cmath>

malinin::Matrix::Matrix() :
  lines_(0),
  columns_(0)
{
}

malinin::Matrix::Matrix(const Matrix &copied) :
  lines_(copied.lines_),
  columns_(copied.columns_),
  figures_(std::make_unique<shape_ptr[]>(copied.lines_ * copied.columns_))
{
  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    figures_[i] = copied.figures_[i];
  }
}

malinin::Matrix::Matrix(malinin::Matrix &&moved) :
  lines_(moved.lines_),
  columns_(moved.columns_),
  figures_(std::move(moved.figures_))
{
  moved.lines_ = 0;
  moved.columns_ = 0;
}

malinin::Matrix &malinin::Matrix::operator =(const Matrix &copied)
{
  if (this != &copied)
  {
    shapes_array tmpFigures(std::make_unique<shape_ptr[]>(copied.lines_ * copied.columns_));
    lines_ = copied.lines_;
    columns_ = copied.columns_;
    for (size_t i = 0; i < (lines_ * columns_); i++)
    {
      tmpFigures[i] = copied.figures_[i];
    }

    figures_.swap(tmpFigures);
  }

  return *this;
}

malinin::Matrix &malinin::Matrix::operator =(Matrix &&moved)
{
  if (this != &moved)
  {
    lines_ = moved.lines_;
    columns_ = moved.columns_;
    figures_ = std::move(moved.figures_);
    moved.lines_ = 0;
    moved.columns_ = 0;
  }

  return *this;
}

malinin::shapes_array malinin::Matrix::operator [](size_t index) const
{
  if (index >= lines_)
  {
    throw std::out_of_range("Index out of range");
  }

  shapes_array tmpFigures(std::make_unique<shape_ptr[]>(columns_));
  for (size_t i = 0; i < columns_; i++)
  {
    tmpFigures[i] = figures_[index * columns_ + i];
  }

  return tmpFigures;
}

bool malinin::Matrix::operator==(const Matrix &shapesMatrix) const
{
  if ((lines_ != shapesMatrix.lines_) || (columns_ != shapesMatrix.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    if (figures_[i] != shapesMatrix.figures_[i])
    {
      return false;
    }
  }

  return true;
}

bool malinin::Matrix::operator!=(const Matrix &shapesMatrix) const
{
  return !(*this == shapesMatrix);
}

void malinin::Matrix::add(shape_ptr shape, size_t line, size_t column)
{
  if (!shape)
  {
    throw std::invalid_argument("Shape is a null pointer!");
  }

  if (column > columns_)
  {
    throw std::out_of_range("Column index is out of range");
  }

  if (line > lines_)
  {
    throw std::out_of_range("Line index is out of range");
  }

  size_t newLine = (line == lines_) ? (lines_ + 1) : (lines_);
  size_t newColumn = (column == columns_) ? (columns_ + 1) : (columns_);

  shapes_array tmpFigures(std::make_unique<shape_ptr[]>(newLine * newColumn));

  for (size_t i = 0; i < newLine; i++)
  {
    for (size_t j = 0; j < newColumn; ++j)
    {
      if ((lines_ == i) || (columns_ == j))
      {
        tmpFigures[i * newColumn + j] = nullptr;
      }
      else
      {
        tmpFigures[i * newColumn + j] = figures_[i * columns_ + j];
      }
    }
  }

  tmpFigures[line * newColumn + column] = shape;
  figures_.swap(tmpFigures);
  lines_ = newLine;
  columns_ = newColumn;
}

std::size_t malinin::Matrix::getLines() const
{
  return lines_;
}

std::size_t malinin::Matrix::getColumns() const
{
  return columns_;
}
