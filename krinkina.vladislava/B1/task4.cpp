#include <iostream>
#include <stdexcept>
#include <vector>
#include <random>
#include <ctime>
#include <cmath>
#include <cstring>
#include "complement.hpp"

void fillRandom(double * array, int size)
{
  std::mt19937 Rng(time(0));
  std::uniform_real_distribution<double> distrib(-1.0, 1.0);
  for (int i = 0; i < size; ++i)
  {
    array[i] = distrib(Rng);
  }
}

void task4(const char* direction, const char* array_size)
{
  int size = atoi(array_size);
  if (size == 0)
  {
    throw std::invalid_argument("Invalid array size");
  }

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);

  bool(*pt_comp) (double&, double&) = nullptr;
  if (std::strcmp(direction, "ascending") == 0) {
    pt_comp = complement::sort_ascending;
  }
  else if (std::strcmp(direction, "descending") == 0) {
    pt_comp = complement::sort_descending;
  }
  else {
    throw std::invalid_argument("Invalid sorting direction");
  }

  complement::printContainer<std::vector<double> >(vector);
  complement::sort<complement::BracketsAccess, std::vector<double> >(vector, pt_comp);
  complement::printContainer<std::vector<double> >(vector);
}
