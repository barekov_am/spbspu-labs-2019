#include <fstream>
#include <memory>
#include <stdexcept>
#include "complement.hpp"

void task2(const char *filename)
{
  std::ifstream input(filename);
  if (!input.is_open())
  {
    throw std::invalid_argument("Incorrect filename for task 2");
  }

  size_t size = 0;
  size_t initial_size = 365;
  std::unique_ptr<char[], decltype(&free)> contents(static_cast<char *>(malloc(initial_size)), free);

  while (input)
  {
    input.read(&contents[size], initial_size - size);
    size += input.gcount();
    if (size == initial_size)
    {
      initial_size *= 2;
      auto temp_content = contents.release();
      contents.reset(static_cast<char *>(std::realloc(temp_content, initial_size)));
      if (!contents) {
        throw std::runtime_error("Error allocate memory");
      }
    }
  }

  input.close();
  if (input.is_open())
  {
    throw std::ios_base::failure("Error close file");
  }

  std::vector<char> vector(&contents[0], &contents[size]);
  for (char &it : vector)
  {
    std::cout << it;
  }
}
