#include <iostream>
#include <stdexcept>
#include <vector>
#include <forward_list>
#include <stdlib.h>
#include <cstring>
#include "complement.hpp"

void task1(const char* direction)
{
  std::vector<int> vector1;

  int num = 0;
  while (std::cin >> num)
  {
    vector1.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Cannot read data properly");
  }

  std::vector<int> vector2 = vector1;
  std::forward_list<int> list1(vector1.begin(), vector1.end());

  bool(*pt_comp) (int&, int&) = nullptr;
  if (std::strcmp(direction, "ascending") == 0) {
    pt_comp = complement::sort_ascending;
  }
  else if (std::strcmp(direction, "descending") == 0) {
    pt_comp = complement::sort_descending;
  }
  else {
    throw std::invalid_argument("Invalid sorting direction.");
  }

  complement::sort <complement::BracketsAccess, std::vector<int> >(vector1, pt_comp);
  complement::printContainer<std::vector<int> >(vector1);

  complement::sort <complement::AtAccess, std::vector<int> >(vector2, pt_comp);
  complement::printContainer<std::vector<int> >(vector2);

  complement::sort <complement::IteratorAccess, std::forward_list<int> >(list1, pt_comp);
  complement::printContainer<std::forward_list<int> >(list1);
}
