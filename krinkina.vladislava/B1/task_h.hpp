#ifndef TASK_HEADERS_HPP
#define TASK_HEADERS_HPP

void task1(const char* direction);
void task2(const char* filename);
void task3();
void task4(const char* direction, const char* array_size);

#endif
