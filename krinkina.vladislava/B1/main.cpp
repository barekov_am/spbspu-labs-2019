#include <iostream>
#include <stdexcept>
#include "task_h.hpp"

int main(int argc, char** argv)
{
  try {
    if (argc < 2)
    {
      throw std::invalid_argument("Invalid number of arguments");
    }
    switch (*argv[1])
    {
    case '1':
      if (argc == 3) {
        if (argv[2] == nullptr)
        {
          std::cerr << "Invalid";
          return 1;
        }
        task1(argv[2]);
      }
      else {
        throw std::invalid_argument("Invalid number of arguments");
      }
      break;

    case '2':
      if (argc == 3) {
        if (argv[2] == nullptr)
        {
          std::cerr << "Invalid";
          return 1;
        }
        task2(argv[2]);
      }
      else {
        throw std::invalid_argument("Invalid number of arguments");
      }
      break;

    case '3':
      if (argc == 2) {
        task3();
      }
      else {
        throw std::invalid_argument("Invalid number of arguments");
      }
      break;

    case '4':
      if (argc == 4) {
        if (argv[2] == nullptr)
        {
          std::cerr << "Invalid";
          return 1;
        }
        task4(argv[2], argv[3]);
      }
      else {
        throw std::invalid_argument("Invalid number of arguments");
      }
      break;

    default:
      throw std::invalid_argument("Invalid first input argument");
      break;
    }
  }
  catch (std::exception & e)
  {
    std::cerr << e.what() << "\n";
    return 1;
  }
  
  return 0;
}
