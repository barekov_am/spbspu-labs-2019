#ifndef DETAILS_HPP
#define DETAILS_HPP
#include <iostream>
#include <vector>

namespace complement
{
  template <typename T>
  bool sort_ascending(T a, T b)
  {
    return a > b;
  }

  template <typename T>
  bool sort_descending(T a, T b)
  {
    return a < b;
  }

  template <typename C>
  struct BracketsAccess
  {
    typedef typename C::size_type indextype;
    static indextype getBegin(const C& /**container**/)
    {
      return 0;
    };
    static indextype getEnd(const C& cont)
    {
      return cont.size();
    };
    static typename C::reference get(C& cont, indextype i)
    {
      return cont[i];
    };
  };

  template <typename C>
  struct AtAccess
  {
    typedef typename C::size_type indextype;
    static indextype getBegin(C& /**container**/)
    {
      return 0;
    };
    static indextype getEnd(const C& cont)
    {
      return cont.size();
    };
    static typename C::reference get(C& cont, indextype i)
    {
      return cont.at(i);
    };
  };

  template <typename C>
  struct IteratorAccess
  {
    typedef typename C::iterator indextype;
    static indextype getBegin(C& cont)
    {
      return cont.begin();
    };
    static indextype getEnd(C& cont)
    {
      return cont.end();
    };
    static typename C::reference get(C& /**container**/, indextype i)
    {
      return *i;
    };
  };

  template <template <typename C> class Traits, typename C>
  void sort(C& cont, bool(*compare)(typename C::value_type&, typename C::value_type&))
  {
    typedef typename Traits<C>::indextype index;
    for (index i = Traits<C>::getBegin(cont); i != Traits<C>::getEnd(cont); ++i)
    {
      for (index j = i; j != Traits<C>::getEnd(cont); ++j)
      {
        if (compare(Traits<C>::get(cont, i), Traits<C>::get(cont, j)))
        {
          std::swap(Traits<C>::get(cont, i), Traits<C>::get(cont, j));
        }
      }
    }
  }

  template <typename C>
  void printContainer(const C& cont)
  {
    for (typename C::const_iterator it = cont.begin(); it != cont.end(); it++)
    {
      std::cout << *it << " ";
    }
    std::cout << "\n";
  }
}

#endif
