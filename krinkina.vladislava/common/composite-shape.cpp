#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

krinkina::CompositeShape::CompositeShape() :
  shape_count_(0),
  shape_array_()
{}

krinkina::CompositeShape::CompositeShape(const krinkina::CompositeShape &cs) :
  shape_count_(cs.shape_count_),
  shape_array_(new krinkina::Shape*[cs.shape_count_])
{
  for (int i = 0; i < shape_count_; ++i)
  {
    shape_array_[i] = cs.shape_array_[i];
  }
}

krinkina::CompositeShape::CompositeShape(krinkina::CompositeShape &&cs) :
  shape_count_(cs.shape_count_),
  shape_array_(std::move(cs.shape_array_))
{
  cs.shape_count_ = 0;
}

krinkina::CompositeShape::CompositeShape(krinkina::Shape *shape) :
  shape_count_(1),
  shape_array_(new krinkina::Shape*[1])
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("shape equals null");
  }
  shape_array_[0] = shape;
}

krinkina::CompositeShape& krinkina::CompositeShape::operator =(const CompositeShape& rh)
{
  if (this != &rh)
  {
    shape_count_ = rh.shape_count_;
    std::unique_ptr<Shape*[]> new_shape_array(new Shape*[rh.shape_count_]);
    for (int i = 0; i < shape_count_; i++)
    {
      new_shape_array[i] = rh.shape_array_[i];
    }
    shape_array_.swap(new_shape_array);
  }

  return *this;
}

krinkina::CompositeShape& krinkina::CompositeShape::operator =(CompositeShape&& rh)
{
  if (this != &rh)
  {
    shape_count_ = rh.shape_count_;
    shape_array_.swap(rh.shape_array_);
    rh.shape_array_.reset();
    rh.shape_count_ = 0;
  }

  return *this;
}

krinkina::Shape* krinkina::CompositeShape::operator [](int i)
{
  return getShape(i);
}

double krinkina::CompositeShape::getArea() const
{
  double sum_shapes_area = 0;
  for (int i = 0; i < shape_count_; ++i)
  {
    sum_shapes_area += shape_array_[i]->getArea();
  }
  return sum_shapes_area;
}

krinkina::rectangle_t krinkina::CompositeShape::getFrameRect() const
{
  if (shape_count_ != 0)
  {
    rectangle_t firstFrameRect = shape_array_[0]->getFrameRect();

    double max_x = firstFrameRect.pos.x + firstFrameRect.width / 2;
    double min_x = firstFrameRect.pos.x - firstFrameRect.width / 2;
    double max_y = firstFrameRect.pos.y + firstFrameRect.height / 2;
    double min_y = firstFrameRect.pos.y - firstFrameRect.height / 2;

    for (int i = 1; i < shape_count_; ++i)
    {
      rectangle_t temp_rect = shape_array_[i]->getFrameRect();

      double bottom_side = temp_rect.pos.y - temp_rect.height / 2;
      min_y = std::min(min_y, bottom_side);

      double top_side = temp_rect.pos.y + temp_rect.height / 2;
      max_y = std::max(max_y, top_side);

      double left_side = temp_rect.pos.x - temp_rect.width / 2;
      min_x = std::min(min_x, left_side);

      double right_side = temp_rect.pos.x + temp_rect.width / 2;
      max_x = std::max(max_x, right_side);
    }

    return rectangle_t{max_x - min_x, max_y - min_y,  point_t{(max_x + min_x) / 2, (max_y + min_y) / 2}};
  }
  else
  {
    throw std::invalid_argument("emply size");
  }
}

void krinkina::CompositeShape::move(double dx, double dy)
{
  for (int i = 0; i < shape_count_; ++i)
  {
    shape_array_[i]->move(dx, dy);
  }
}

void krinkina::CompositeShape::move(const krinkina::point_t &center)
{
  double dx = center.x - getFrameRect().pos.x;
  double dy = center.y - getFrameRect().pos.y;
  move(dx, dy);
}

void krinkina::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }
  const point_t pos = getFrameRect().pos;
  for (int i = 0; i < shape_count_; ++i)
  {
    double dx = shape_array_[i]->getFrameRect().pos.x - pos.x;
    double dy = shape_array_[i]->getFrameRect().pos.y - pos.y;
    shape_array_[i]->move((coefficient - 1) * dx, (coefficient - 1) * dy);
    shape_array_[i]->scale(coefficient);
  }
}

void krinkina::CompositeShape::printInfo() const
{
  krinkina::rectangle_t rect = getFrameRect();
  std::cout << "Number of shapes in CompositeShape = " << shape_count_ << '\n';
  std::cout << "Area = " << getArea() << '\n';
  std::cout << "Frame rectangle width = " << rect.width << '\n';
  std::cout << "Frame rectangle height = " << rect.height << '\n';
  std::cout << "Centre: (" << rect.pos.x << ";" << rect.pos.y << ")\n";
}

int krinkina::CompositeShape::getShapeCount() const
{
  return shape_count_;
}

void krinkina::CompositeShape::addShape(krinkina::Shape *shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("shape equals null");
  }

  std::unique_ptr<krinkina::Shape*[]> new_shape_array(new krinkina::Shape *[shape_count_ + 1]);
  for (int i = 0; i < shape_count_; ++i)
  {
    new_shape_array[i] = shape_array_[i];
  }
  new_shape_array[shape_count_] = shape;
  shape_count_++;
  shape_array_.swap(new_shape_array);
}

void krinkina::CompositeShape::removeShape(int index)
{
  if (index >= shape_count_)
  {
    throw std::out_of_range("Index out of range");
  }

  std::unique_ptr<krinkina::Shape *[]> new_shape_array(new krinkina::Shape *[shape_count_ - 1]);

  for (int i = 0; i < index; ++i)
  {
    new_shape_array[i] = shape_array_[i];
  }

  shape_count_--;

  for (int i = index; i < shape_count_; ++i)
  {
    new_shape_array[i] = shape_array_[i + 1];
  }

  shape_array_.swap(new_shape_array);
}

void krinkina::CompositeShape::removeShape(krinkina::Shape *shape)
{
  for (int i = 0; i < shape_count_; ++i)
  {
    if (shape == shape_array_[i])
    {
      removeShape(i);
      return;
    }
  }
  throw std::invalid_argument("shape doesn't exist");
}

krinkina::Shape* krinkina::CompositeShape::getShape(int index) const
{
  if (index >= shape_count_)
  {
    throw std::out_of_range("Index out of range");
  }
  return shape_array_[index];
}

void krinkina::CompositeShape::rotate(double angle)
{
  const double cos = std::cos((2 * M_PI * angle) / 360);
  const double sin = std::sin((2 * M_PI * angle) / 360);

  const point_t compCentre = getFrameRect().pos;

  for (int i = 0; i < shape_count_; i++)
  {
    const point_t center = shape_array_[i]->getFrameRect().pos;
    const double dx = (center.x - compCentre.x) * cos - (center.y - compCentre.y) * sin;
    const double dy = (center.x - compCentre.x) * sin + (center.y - compCentre.y) * cos;
    shape_array_[i]->move({ compCentre.x + dx, compCentre.y + dy });
    shape_array_[i]->rotate(angle);
  }
}
