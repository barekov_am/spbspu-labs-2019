#ifndef A3_COMPOSITE_SHAPE_HPP
#define A3_COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace krinkina
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &cs);
    CompositeShape(CompositeShape &&cs);
    CompositeShape(krinkina::Shape *shape);
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape& rh);
    CompositeShape& operator =(CompositeShape&& rh);
    Shape* operator [](int i);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const krinkina::point_t &center) override;
    void scale(double coefficient) override;
    void printInfo() const override;
    int getShapeCount() const;
    void addShape(Shape *shape);
    void removeShape(Shape *shape);
    void removeShape(int index);
    void rotate(double angle) override;
    Shape* getShape(int index) const;

  private:
    int shape_count_;
    std::unique_ptr<Shape *[]> shape_array_;
  };
}

#endif //A3_COMPOSITE_SHAPE_HPP
