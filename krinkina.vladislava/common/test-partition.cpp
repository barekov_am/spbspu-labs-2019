#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(partitionmetodstests)

  BOOST_AUTO_TEST_CASE(partitiontest)
  {
    krinkina::Rectangle rectangle({2, 4.5}, 1, 2);
    krinkina::Rectangle rectangle1({3.5, 2}, 2, 5);
    krinkina::Circle circle({6, 4}, 2);

    krinkina::CompositeShape compositeShape(&rectangle);
    compositeShape.addShape(&rectangle1);
    compositeShape.addShape(&circle);

    krinkina::Matrix matrix = krinkina::division(compositeShape);

    BOOST_CHECK_EQUAL(matrix.getLines(), 2);
    BOOST_CHECK_EQUAL(matrix.getColumns(), 2);
  }

  BOOST_AUTO_TEST_CASE(intersectiontest)
  {
    krinkina::Rectangle rectangle({2, 4.5}, 1, 2);
    krinkina::Rectangle rectangle1({3.5, 2}, 2, 5);
    krinkina::Circle circle({6, 4}, 2);

    BOOST_CHECK(!krinkina::checkIntersect(rectangle.getFrameRect(), rectangle1.getFrameRect()));
    BOOST_CHECK(!krinkina::checkIntersect(rectangle.getFrameRect(), circle.getFrameRect()));
    BOOST_CHECK(krinkina::checkIntersect(rectangle1.getFrameRect(), circle.getFrameRect()));
  }

BOOST_AUTO_TEST_SUITE_END()
