#define _USE_MATH_DEFINES
#include "triangle.hpp"
#include <iostream>
#include <math.h>
#include <stdexcept>
#include <algorithm>

krinkina::Triangle::Triangle(const point_t &topA, const point_t &topB, const point_t &topC) :
  center_({(topA.x + topB.x + topC.x) / 3, (topA.y + topB.y + topC.y) / 3}),
  tA_(topA),
  tB_(topB),
  tC_(topC)
{
  if (getArea() <= 0.0)
  {
    throw std::invalid_argument("Triangle's argument is invalid.");
  }
}

double krinkina::Triangle::getArea() const
{
  return (abs((tB_.x - tA_.x) * (tC_.y - tA_.y) - (tC_.x - tA_.x) * (tB_.y - tA_.y)) / 2);
}

krinkina::rectangle_t krinkina::Triangle::getFrameRect() const
{
  const double max_x = std::max(std::max(tA_.x, tB_.x), tC_.x);
  const double max_y = std::max(std::max(tA_.y, tB_.y), tC_.y);
  const double min_x = std::min(std::min(tA_.x, tB_.x), tC_.x);
  const double min_y = std::min(std::min(tA_.y, tB_.y), tC_.y);
  const double width = max_x - min_x;
  const double height = max_y - min_y;
  return {width, height, {min_x + width / 2, min_y + height / 2}};
}

void krinkina::Triangle::move(const point_t &center)
{
  const double dx = center.x - center_.x;
  const double dy = center.y - center_.y;
  move(dx, dy);
}

void krinkina::Triangle::move(double dx, double dy)
{
  center_.x += dx;
  tA_.x += dx;
  tB_.x += dx;
  tC_.x += dx;
  center_.y += dy;
  tA_.y += dy;
  tB_.y += dy;
  tC_.y += dy;
}

void krinkina::Triangle::scale(double coefficient)
{
  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Coefficient must be positive number");
  }
  else
  {
    tA_ = { center_.x + (tA_.x - center_.x) * coefficient, center_.y + (tA_.y - center_.y) * coefficient };
    tB_ = { center_.x + (tB_.x - center_.x) * coefficient, center_.y + (tB_.y - center_.y) * coefficient };
    tC_ = { center_.x + (tC_.x - center_.x) * coefficient, center_.y + (tC_.y - center_.y) * coefficient };
  }
}

void krinkina::Triangle::printInfo() const
{
  std::cout << "Center: (" << center_.x << ", " << center_.y << ")\n";
  std::cout << "A: (" << tA_.x << ", " << tA_.y << ")\n";
  std::cout << "B: (" << tB_.x << ", " << tB_.y << ")\n";
  std::cout << "C: (" << tC_.x << ", " << tC_.y << ")\n";
  std::cout << "Triangle area: " << getArea() << '\n';
  std::cout << "Parameters FrameRect:\n";
  std::cout << "Center: (" << getFrameRect().pos.x << ", " << getFrameRect().pos.y << ")\n";
  std::cout << "Width: " << getFrameRect().width << '\n';
  std::cout << "Height: " << getFrameRect().height << '\n';
  std::cout << std::endl;
}

krinkina::point_t rotateOneCoordinate(krinkina::point_t point, double angle)
{
  double x = point.x * cos(M_PI * angle / 180) - point.y * sin(M_PI * angle / 180);
  double y = point.y * cos(M_PI * angle / 180) + point.x * sin(M_PI * angle / 180);
  return { x, y };
}

void krinkina::Triangle::rotate(double angle)
{
  if (angle < 0)
  {
    angle = (360 + fmod(angle, 360));
  }
  tA_ = rotateOneCoordinate(tA_, angle);
  tB_ = rotateOneCoordinate(tB_, angle);
  tC_ = rotateOneCoordinate(tC_, angle);
}
