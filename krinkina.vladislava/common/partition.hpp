#ifndef PARTITION_HPP
#define PARTITION_HPP

#include <memory>
#include "composite-shape.hpp"
#include "matrix.hpp"

namespace krinkina
{
  bool checkIntersect(krinkina::rectangle_t shape1, krinkina::rectangle_t shape2);
  Matrix division(std::unique_ptr<Shape*[]>, size_t size);
  Matrix division(CompositeShape &compositeShape);
}

#endif
