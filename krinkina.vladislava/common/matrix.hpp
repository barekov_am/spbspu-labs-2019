#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"
#include <memory>

namespace krinkina
{
 class Matrix
 {
 public:
   Matrix();
   Matrix(const Matrix &rhs);
   Matrix(Matrix &&rhs);
   ~Matrix() = default;

   Matrix &operator =(const Matrix &rhs);
   Matrix &operator =(Matrix &&rhs);
   std::unique_ptr<krinkina::Shape*[]>  operator [](size_t index) const;
   bool operator ==(const Matrix &shapesMatrix) const;
   bool operator !=(const Matrix &shapesMatrix) const;

   void add(Shape* shape, size_t line, size_t column);
   void showAll() const;
   size_t getLines() const;
   size_t getColumns() const;
   size_t getLineSize(size_t line) const;

 private:
   size_t lines_;
   size_t columns_;
   std::unique_ptr<Shape*[]> shapes_;
 };
}

#endif
