#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "triangle.hpp"

const double INACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(TestTriangle)

BOOST_AUTO_TEST_CASE(InvariabilityAfterMovingCenterTest)
{
  krinkina::Triangle triangle({6, 7}, {3, 2}, {10, 1});
  krinkina::rectangle_t frameBefore = triangle.getFrameRect();
  double areaBefore = triangle.getArea();
  triangle.move({12, -8});
  krinkina::rectangle_t frameAfter = triangle.getFrameRect();
  double areaAfter = triangle.getArea();
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, INACCURACY);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, INACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, INACCURACY);
}

BOOST_AUTO_TEST_CASE(InvariabilityAfterMovingCenterOnDxAndDyTest)
{
  krinkina::Triangle triangle({6, 7}, {3, 2}, {10, 1});
  krinkina::rectangle_t frameBefore = triangle.getFrameRect();
  double areaBefore = triangle.getArea();
  triangle.move(-12, 8);
  krinkina::rectangle_t frameAfter = triangle.getFrameRect();
  double areaAfter = triangle.getArea();
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, INACCURACY);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, INACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, INACCURACY);
}

BOOST_AUTO_TEST_CASE(AreaScalingIncrease)
{
  krinkina::Triangle triangle({6, 7}, {3, 2}, {10, 1});
  double areaBefore = triangle.getArea();
  const double increase_value = 2;
  triangle.scale(increase_value);
  double areaAfter = triangle.getArea();
  BOOST_CHECK_CLOSE(areaBefore * increase_value * increase_value, areaAfter, INACCURACY);
}

BOOST_AUTO_TEST_CASE(AreaScalingDecrease)
{
  krinkina::Triangle triangle({6, 7}, {3, 2}, {10, 1});
  double areaBefore = triangle.getArea();
  const double decrease_value = 0.5;
  triangle.scale(decrease_value);
  double areaAfter = triangle.getArea();
  BOOST_CHECK_CLOSE(areaBefore * decrease_value * decrease_value, areaAfter, INACCURACY);
}

BOOST_AUTO_TEST_CASE(TestOfIncorrectInitialisationLineX)
{
  BOOST_CHECK_THROW(krinkina::Triangle({1, -12}, {1, 2}, {1, 3}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestOfIncorrectInitialisationLineY)
{
  BOOST_CHECK_THROW(krinkina::Triangle({1, 2}, {-13, 2}, {1, 2}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestOfIncorrectScaleArgument)
{
  krinkina::Triangle triangle({12, -12}, {13, 5}, {1, 23});
  BOOST_CHECK_THROW(triangle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(triangleRotate)
{
  krinkina::Triangle triangleTest({ 4, 5 }, { 6, 7 }, { 9, 8 });
  double areaTest = triangleTest.getArea();
  triangleTest.rotate(45);
  BOOST_CHECK_CLOSE(areaTest, triangleTest.getArea(), INACCURACY);
}


BOOST_AUTO_TEST_SUITE_END()
