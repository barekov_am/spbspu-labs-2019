#ifndef TriangleHPP_A3
#define TriangleHPP_A3
#include "shape.hpp"

namespace krinkina
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &topA, const point_t &topB, const point_t &topC);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &center) override;
    void move(double dx, double dy) override;
    void scale(double coefficient) override;
    void printInfo() const override;
    void rotate(double angle) override;

  private:
    point_t center_;
    point_t tA_;
    point_t tB_;
    point_t tC_;
  };
}

#endif
