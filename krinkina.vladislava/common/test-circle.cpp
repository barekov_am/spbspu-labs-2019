#include <iostream>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

#define INACCURACY 0.001

BOOST_AUTO_TEST_SUITE(TestCircle)

BOOST_AUTO_TEST_CASE(invarianceOfRadiusAfterMovingCenter)
{
  krinkina::Circle circle({4, 4}, 6);
  const double radius = circle.getRadius();
  circle.move({10, 10});
  BOOST_CHECK_CLOSE(circle.getRadius(), radius, INACCURACY);

  circle.move(25, 25);
  BOOST_CHECK_CLOSE(circle.getRadius(), radius, INACCURACY);
}

BOOST_AUTO_TEST_CASE(invarianceOfAreaAfterMovingCenter)
{
  krinkina::Circle circle({4, 4}, 6);
  const double area = circle.getArea();
  circle.move({10, 10});
  BOOST_CHECK_CLOSE(circle.getArea(), area, INACCURACY);

  circle.move(25, 25);
  BOOST_CHECK_CLOSE(circle.getArea(), area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(ScalingIncreaseOfArea)
{
  krinkina::Circle circle({4, 4}, 6);
  const double area = circle.getArea();
  const double increase_value = 2;
  circle.scale(increase_value);
  BOOST_CHECK_CLOSE(circle.getArea(), increase_value * increase_value * area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(ScalingDecreaseOfArea)
{
  krinkina::Circle circle({4, 4}, 6);
  const double area = circle.getArea();
  const double decrease_value = 0.5;
  circle.scale(decrease_value);
  BOOST_CHECK_CLOSE(circle.getArea(), area * decrease_value * decrease_value, INACCURACY);
}

BOOST_AUTO_TEST_CASE(ScaleTest)
{
  krinkina::Circle circle({4, 4}, 6);
  BOOST_CHECK_THROW(circle.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestOfIncorrectRadius)
{
  BOOST_CHECK_THROW(krinkina::Circle circle({4, 4}, -6), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rotate_NothingChanges)
{
  krinkina::point_t centre{ 1, 2 };
  double radius = 10;
  krinkina::Circle circle = krinkina::Circle(centre, radius);

  double areaBeforeRotation = circle.getArea();
  krinkina::rectangle_t frameRectBeforeRotation = circle.getFrameRect();

  circle.rotate(40);

  BOOST_CHECK_EQUAL(radius, circle.getRadius());
  BOOST_CHECK_EQUAL(areaBeforeRotation, circle.getArea());
  BOOST_CHECK_EQUAL(frameRectBeforeRotation.height, circle.getFrameRect().height);
  BOOST_CHECK_EQUAL(frameRectBeforeRotation.width, circle.getFrameRect().width);
  BOOST_CHECK_EQUAL(frameRectBeforeRotation.pos.x, circle.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(frameRectBeforeRotation.pos.y, circle.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
