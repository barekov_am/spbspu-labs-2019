#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

const double INACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testForMatrix)

BOOST_AUTO_TEST_CASE(copyAndMove)
{
  krinkina::Rectangle rectangle({4, 6}, 6, 7);
  krinkina::CompositeShape compositeShape(&rectangle);
  krinkina::Matrix matrix = krinkina::division(compositeShape);

  krinkina::Matrix matrix3 = matrix;

  BOOST_CHECK_EQUAL(matrix3.getLines(), 1);
  BOOST_CHECK_EQUAL(matrix3.getColumns(),1);
  BOOST_CHECK_NO_THROW(krinkina::Matrix matrix1(matrix));
  BOOST_CHECK_NO_THROW(krinkina::Matrix matrix2(std::move(matrix)));

  krinkina::Matrix matrix4;
  krinkina::Matrix matrix5;

  BOOST_CHECK_NO_THROW(matrix4 = matrix);
  BOOST_CHECK_NO_THROW(matrix5 = std::move(matrix));
}

BOOST_AUTO_TEST_CASE(testForThrow)
{
  krinkina::Rectangle rectangle({2, 4.5}, 1, 2);
  krinkina::Rectangle rectangle1({3.5, 2}, 2, 5);
  krinkina::Circle circle({6, 4}, 2);
  krinkina::Triangle triangle({3, 2}, {2, 0}, {4, 0});
  krinkina::Triangle triangle1({7, 4}, {10, 3}, {10, 5});
  krinkina::CompositeShape compositeShape(&rectangle);
  compositeShape.addShape(&rectangle1);
  compositeShape.addShape(&circle);
  compositeShape.addShape(&triangle);
  compositeShape.addShape(&triangle1);

  krinkina::Matrix matrix = krinkina::division(compositeShape);
  BOOST_CHECK_THROW(matrix[10][10], std::out_of_range);
  BOOST_CHECK_NO_THROW(matrix[0][1]);

}

BOOST_AUTO_TEST_CASE(testCorrectWorkingOperators)
{
  krinkina::point_t center1 = { 14, 56 };
  krinkina::point_t center2 = { -53, 123 };
  krinkina::point_t center3 = { 39, -58 };
  krinkina::point_t center4 = { 59, -68 };

  krinkina::Circle circle1(center1, 5);
  krinkina::Circle circle2(center2, 9);
  krinkina::Circle circle3(center4, 13);
  krinkina::Circle circle4(center1, 10);
  krinkina::Rectangle rect1(center1, 2, 7);
  krinkina::Rectangle rect2(center3, 8, 10);
  krinkina::Rectangle rect3(center4, 5, 15);
  krinkina::Rectangle rect4(center1, 3, 5);

  krinkina::Shape *p_circle1 = &circle1;
  krinkina::Shape *p_circle2 = &circle2;
  krinkina::Shape *p_circle3 = &circle3;
  krinkina::Shape *p_circle4 = &circle4;
  krinkina::Shape *p_rect1 = &rect1;
  krinkina::Shape *p_rect2 = &rect2;
  krinkina::Shape *p_rect3 = &rect3;
  krinkina::Shape *p_rect4 = &rect4;

  krinkina::CompositeShape composite_shape;
  composite_shape.addShape(p_circle1);
  composite_shape.addShape(p_rect1);
  composite_shape.addShape(p_circle2);
  composite_shape.addShape(p_rect2);
  composite_shape.addShape(p_circle3);
  composite_shape.addShape(p_rect3);
  krinkina::Matrix matrix = krinkina::division(composite_shape);

  BOOST_CHECK(matrix[0][0] == p_circle1);
  BOOST_CHECK(matrix[0][1] == p_circle2);
  BOOST_CHECK(matrix[0][2] == p_rect2);
  BOOST_CHECK(matrix[0][3] == p_circle3);
  BOOST_CHECK(matrix[1][0] == p_rect1);
  BOOST_CHECK(matrix[1][1] == p_rect3);
  BOOST_CHECK(matrix[0][0] != p_rect3);
  BOOST_CHECK(matrix[1][0] != p_rect4);
  BOOST_CHECK(matrix[0][3] != p_circle4);

  krinkina::CompositeShape composite_shape1;
  krinkina::CompositeShape composite_shape2;
  composite_shape.addShape(p_circle1);
  composite_shape.addShape(p_rect1);
  composite_shape1.addShape(p_circle2);
  composite_shape.addShape(p_rect2);
  composite_shape.addShape(p_circle3);
  composite_shape.addShape(p_rect3);
  krinkina::Matrix matrix1 = krinkina::division(composite_shape1);
  krinkina::Matrix matrix2 = krinkina::division(composite_shape2);

  BOOST_CHECK(matrix1 != matrix2);
  BOOST_CHECK(matrix1 != matrix);
  BOOST_CHECK(matrix != matrix2);

  matrix1 = matrix;
  BOOST_CHECK(matrix1 == matrix);
}

BOOST_AUTO_TEST_SUITE_END()
