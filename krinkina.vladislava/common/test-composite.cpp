#include <stdexcept>
#include <iostream>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(testCompositeShape)

const double INACCURACY = 0.001;

BOOST_AUTO_TEST_CASE(invarianceOfAreaAfterMovingCenter)
{
  krinkina::Circle circle({6, 6}, 6);
  krinkina::Rectangle rectangle({2, 2}, 4, 4);
  krinkina::Triangle triangle({13, 11}, {15, 8}, {11, 8});

  krinkina::Shape *p_circle = &circle;
  krinkina::Shape *p_rect = &rectangle;
  krinkina::Shape *p_tr = &triangle;


  krinkina::CompositeShape composite_shape(p_circle);
  composite_shape.addShape(p_rect);
  composite_shape.addShape(p_tr);

  const double area = composite_shape.getArea();
  composite_shape.move({30, 30});
  BOOST_CHECK_CLOSE(area, composite_shape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(invarianceOfAreaAfterMovingCenterOnDxDy)
{
  krinkina::Circle circle({6, 6}, 6);
  krinkina::Rectangle rectangle({2, 2}, 4, 4);
  krinkina::Triangle triangle({13, 11}, {15, 8}, {11, 8});

  krinkina::Shape *p_circle = &circle;
  krinkina::Shape *p_rect = &rectangle;

  krinkina::CompositeShape composite_shape(p_circle);
  composite_shape.addShape(p_rect);

  const double area = composite_shape.getArea();
  composite_shape.move(30, 30);
  BOOST_CHECK_CLOSE(area, composite_shape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestThrowingExceptions)
{
  krinkina::Shape *shape = nullptr;
  BOOST_CHECK_THROW(krinkina::CompositeShape composite_shape(shape), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestThrowingExceptions1)
{
  krinkina::Circle circle({6, 6}, 6);
  krinkina::Rectangle rectangle({2, 2}, 4, 4);
  krinkina::Triangle triangle({13, 11}, {15, 8}, {11, 8});

  krinkina::Shape *p_circle = &circle;
  krinkina::Shape *p_rect = &rectangle;
  krinkina::Shape *shape = nullptr;

  krinkina::CompositeShape composite_shape(p_circle);
  composite_shape.addShape(p_rect);
  BOOST_CHECK_THROW(composite_shape.addShape(shape), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(checktWorkingRemove)
{
  krinkina::Circle circle({6, 6}, 6);
  krinkina::Rectangle rectangle1({1, 1}, 2, 2);
  krinkina::Rectangle rectangle2({ 3, 3 }, 4, 4);
  krinkina::Rectangle rectangle3({ 5, 5 }, 6, 6);
  krinkina::Triangle triangle({ 13, 11 }, { 15, 8 }, { 11, 8 });

  krinkina::Shape *p_circle = &circle;
  krinkina::Shape *p_rect1 = &rectangle1;
  krinkina::Shape *p_rect2 = &rectangle2;
  krinkina::Shape *p_rect3 = &rectangle3;
  krinkina::Shape *p_tr = &triangle;

  krinkina::CompositeShape composite_shape(p_circle);
  composite_shape.addShape(p_rect1);
  composite_shape.addShape(p_rect2);
  composite_shape.addShape(p_rect3);
  composite_shape.addShape(p_tr);
  const int quantity_shape = composite_shape.getShapeCount();

  composite_shape.removeShape(p_rect1);
  const int quantity_shape_after_remove1 = composite_shape.getShapeCount();

  BOOST_CHECK_EQUAL(quantity_shape - 1, quantity_shape_after_remove1);
  BOOST_CHECK_THROW(composite_shape.removeShape(p_rect1), std::invalid_argument);
  BOOST_CHECK_THROW(composite_shape.removeShape(quantity_shape), std::out_of_range);

  composite_shape.removeShape(p_rect2);
  const int quantity_shape_after_remove2 = composite_shape.getShapeCount();

  BOOST_CHECK_EQUAL(quantity_shape_after_remove1 - 1, quantity_shape_after_remove2);
  BOOST_CHECK_THROW(composite_shape.removeShape(p_rect2), std::invalid_argument);
  BOOST_CHECK_THROW(composite_shape.removeShape(quantity_shape_after_remove1), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(changingAreaAfterScaleAnIncrease)
{
  krinkina::Circle circle({6, 6}, 6);
  krinkina::Rectangle rectangle({2, 2}, 4, 4);
  krinkina::Triangle triangle({13, 11}, {15, 8}, {11, 8});

  krinkina::Shape *p_circle = &circle;
  krinkina::Shape *p_rect = &rectangle;
  krinkina::Shape *p_tr = &triangle;

  krinkina::CompositeShape composite_shape(p_circle);
  composite_shape.addShape(p_rect);
  composite_shape.addShape(p_tr);

  const double area = composite_shape.getArea();
  const double increase_value = 3;
  composite_shape.scale(increase_value);
  BOOST_CHECK_CLOSE(area * increase_value * increase_value, composite_shape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(changingAreaAfterScaleAnDecrease)
{
  krinkina::Circle circle({6, 6}, 6);
  krinkina::Rectangle rectangle({2, 2}, 4, 4);
  krinkina::Triangle triangle({13, 11}, {15, 8}, {11, 8});

  krinkina::Shape *p_circle = &circle;
  krinkina::Shape *p_rect = &rectangle;
  krinkina::Shape *p_tr = &triangle;

  krinkina::CompositeShape composite_shape(p_circle);
  composite_shape.addShape(p_rect);
  composite_shape.addShape(p_tr);

  const double area = composite_shape.getArea();
  const double decrease_value = 0.5;
  composite_shape.scale(decrease_value);
  BOOST_CHECK_CLOSE(area * decrease_value * decrease_value, composite_shape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(testWorkingConstructor)
{
  krinkina::Rectangle rectangle({2, 2}, 4, 4);
  krinkina::Shape *p_rect = &rectangle;
  krinkina::CompositeShape composite_shape(p_rect);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, rectangle.getFrameRect().pos.y, INACCURACY);
}

BOOST_AUTO_TEST_CASE(throwExceptionDuaIncorrectCoefficient)
{
  krinkina::Circle circle({6, 6}, 6);
  krinkina::Rectangle rectangle({2, 2}, 4, 4);
  krinkina::Triangle triangle({13, 11}, {15, 8}, {11, 8});

  krinkina::Shape *p_circle = &circle;
  krinkina::Shape *p_rect = &rectangle;
  krinkina::Shape *p_tr = &triangle;

  krinkina::CompositeShape composite_shape(p_circle);
  composite_shape.addShape(p_rect);
  composite_shape.addShape(p_tr);

  BOOST_CHECK_THROW(composite_shape.scale(-5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(checkCenterAfterScale)
{
  krinkina::Rectangle rectangle({ 12, 17 }, 8, 4);
  krinkina::Circle circle({ 12, 17 }, 4);
  
  krinkina::Shape *p_rect = &rectangle;
  krinkina::Shape *p_circle = &circle;
  
  krinkina::CompositeShape composite_shape(p_circle);
  composite_shape.addShape(p_rect);
  
  const double coefficient = 2;
  krinkina::point_t firstPosition = composite_shape.getFrameRect().pos;
  composite_shape.scale(coefficient);
  krinkina::point_t afterScalePosition = composite_shape.getFrameRect().pos;
  BOOST_CHECK_CLOSE(firstPosition.x, afterScalePosition.x, INACCURACY);
  BOOST_CHECK_CLOSE(firstPosition.y, afterScalePosition.y, INACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
