#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
  krinkina::Circle circle{{6, 5}, 2};
  krinkina::Shape * shape = &circle;

  std::cout << "THE PARAMETERS OF THE CIRCLE\n";
  shape->printInfo();

  shape->move(0, 1);
  std::cout << "the parameters of the circle after move dx = 0, dy = 1 :\n";
  shape->printInfo();

  shape->move({-3, -2});
  std::cout << "the parameters of the circle after move centre to the point (-3,-2) :\n";
  shape->printInfo();

  std::cout << "the parameters of the scale circle with coefficient = 2 :\n";
  shape->scale(2);
  shape->printInfo();

  std::cout << '\n';

  krinkina::Rectangle rect{{1, 5}, 15, 10};
  shape = &rect;

  std::cout << "THE PARAMETERS OF THE RACTANGLE \n";
  shape->printInfo();

  shape->move(-1, -5);
  std::cout << "the parameters of the rectangle after move dx = -1, dy = -5:\n";
  shape->printInfo();

  shape->move({3, 1});
  std::cout << "the parameters of the rectangle after move centre to the point (3,1) :\n";
  shape->printInfo();

  std::cout << "the parameters of the scale rectangle with coefficient = 2 :\n";
  shape->scale(2);
  shape->printInfo();

  krinkina::Triangle triangle({13, 11}, {15, 8}, {11, 8});
  shape = &triangle;

  std::cout << '\n';

  std::cout << "THE PARAMETERS OF THE TRIANGLE\n";
  shape->printInfo();

  shape->move(0, 1);
  std::cout << "the parameters of the triangle after move dx = 0, dy = 1 :\n";
  shape->printInfo();

  shape->move({-5, -6});
  std::cout << "the parameters of the triangle after move centre to the point (-5,-6) :\n";
  shape->printInfo();

  std::cout << "the parameters of the scale triangle with coefficient = 2 :\n";
  shape->scale(2);
  shape->printInfo();

  std::cout << std::endl << "---------------------------------------------\n";

  krinkina::Shape *p_circle = &circle;
  krinkina::Shape *p_rect = &rect;
  krinkina::Shape *p_triangle = &triangle;

  krinkina::CompositeShape composite_shape(p_circle);
  composite_shape.addShape(p_rect);
  composite_shape.addShape(p_triangle);
  composite_shape.printInfo();

  std::cout << "move compositeshape in point with center: (40,40)\n";
  composite_shape.move({40, 40});
  composite_shape.printInfo();

  std::cout << "move compositeshape at the dx = 5, dy = -6\n";
  composite_shape.move(5, -6);
  composite_shape.printInfo();

  std::cout << "scale CompositeShape with coefficient = 2\n";
  composite_shape.scale(2);
  composite_shape.printInfo();

  composite_shape.removeShape(p_triangle);
  composite_shape.removeShape(1);
  composite_shape.printInfo();

  return 0;
}
