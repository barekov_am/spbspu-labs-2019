#ifndef PHONEBOOK_IMPLEMENTATION_HPP
#define PHONEBOOK_IMPLEMENTATION_HPP

#include <iostream>
#include "phbook.hpp"

void openPhonebookImplementation(std::istream &in, std::ostream &out);

#endif
