#include <iostream>
#include "../common/rectangle.hpp"
#include "../common/circle.hpp"
#include "../common/triangle.hpp"
#include "../common/composite-shape.hpp"
#include "../common/partition.hpp"
#include "../common/matrix.hpp"


int main()
{
  krinkina::Rectangle rectangle({4, 5}, 6, 7);
  krinkina::Circle circle({4, 5}, 6);
  krinkina::Triangle triangle({4, 6}, {6, 7}, {1, 10});
  std::cout << rectangle.getArea() << std::endl;
  std::cout << circle.getArea() << std::endl;
  std::cout << triangle.getArea() << std::endl;
  krinkina::CompositeShape compositeShape(&rectangle);
  compositeShape.addShape(&circle);
  compositeShape.addShape(&triangle);
  std::cout << compositeShape.getArea() << std::endl;
  std::unique_ptr<krinkina::Shape*[]> shapes = std::unique_ptr<krinkina::Shape*[]>(new krinkina::Shape*[5]);
  krinkina::Rectangle rectangle1({2, 4.5}, 1, 2);
  krinkina::Rectangle rectangle2({3.5, 2}, 2, 5);
  krinkina::Circle circle1({6, 4}, 2);
  krinkina::Triangle triangle1({3, 2}, {2, 0}, {4, 0});
  krinkina::Triangle triangle2({7, 4}, {9, 3}, {9, 5});
  shapes[0] = &rectangle1;
  shapes[1] = &rectangle2;
  shapes[2] = &circle1;
  shapes[3] = &triangle1;
  shapes[4] = &triangle2;
  krinkina::Matrix matrix = krinkina::division(std::move(shapes), 5);
  matrix.showAll();
  return 0;
}
