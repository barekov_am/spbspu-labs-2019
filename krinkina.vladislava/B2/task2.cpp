#include <iostream>
#include <list>

const std::size_t MAX_SIZE = 20;
const int maxValue = 20;
const int minValue = 1;

void recursiveWrite(std::list<int>::iterator begin, std::list<int>::iterator end)
{
  if (begin == end)
  {
    std::cout << "\n";
    return;
  }
  if (begin == std::prev(end))
  {
    std::cout << *begin << "\n";
    return;
  }
  std::cout << *begin << " " << *(std::prev(end)) << " ";

  ++begin;
  --end;

  recursiveWrite(begin, end);
}

void task2()
{
  std::list<int> list;

  std::size_t size = 0;
  int temp = 0;

  while (std::cin && !(std::cin >> temp).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("reading error\n");
    }

    if ((temp > maxValue) || (temp < minValue))
    {
      throw std::ios_base::failure("invalid number\n");
    }

    ++size;
    if (size > MAX_SIZE)
    {
      throw std::ios_base::failure("invalid amount of number\n");
    }

    list.push_back(temp);
  }

  recursiveWrite(list.begin(), list.end());
};
