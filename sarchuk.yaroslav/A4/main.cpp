#include <iostream>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "partition.hpp"

void printInfo(const sarchuk::CompositeShape::pointer & shapePointer)
{
  if (!shapePointer) {
    throw std::invalid_argument("Pointer can't be null!");
  }

  sarchuk::rectangle_t frameRect = shapePointer->getFrameRect();
  std::cout << "Area is " << shapePointer->getArea() << ";\nCentre is [" << frameRect.pos.x << ", ";
  std::cout << frameRect.pos.y << "];\n";
  std::cout << "Width of frame rectangle is " << frameRect.width << ", height is " << frameRect.height << ";\n";
}

int main()
{


  sarchuk::CompositeShape::pointer circlePtr = std::make_shared<sarchuk::Circle>(sarchuk::point_t { 1, 5 }, 4);

  sarchuk::CompositeShape::pointer rectanglePtr = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t { 3, -1 }, 5, 4);

  sarchuk::CompositeShape::pointer squarePtr = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t { 6, 5 }, 4,4);
  sarchuk::CompositeShape composition;

  composition.add(circlePtr);

  std::cout << "\nNumber of shapes in composite shape : " << composition.getSize() << ";\n";
  printInfo(std::make_shared<sarchuk::CompositeShape>(composition));

  std::cout << "\nScale composite shape:\n";
  composition.scale(2);
  printInfo(std::make_shared<sarchuk::CompositeShape>(composition));

  std::cout << "\nReturn:\n";
  composition.scale(0.5);
  printInfo(std::make_shared<sarchuk::CompositeShape>(composition));

  composition.add(rectanglePtr);

  std::cout << "\nNumber of shapes in composite shape : " << composition.getSize() << ";\n";
  printInfo(std::make_shared<sarchuk::CompositeShape>(composition));

  composition.add(squarePtr);

  printInfo(std::make_shared<sarchuk::CompositeShape>(composition));

  composition.add(squarePtr);
  composition.add(circlePtr);
  composition.add(rectanglePtr);


  composition.rotate(90);
  printInfo(std::make_shared<sarchuk::CompositeShape>(composition));


  composition.rotate(-450);
  printInfo(std::make_shared<sarchuk::CompositeShape>(composition));

  composition.move(2,3);
  printInfo(std::make_shared<sarchuk::CompositeShape>(composition));

  composition.move({ 0, 0 });
  printInfo(std::make_shared<sarchuk::CompositeShape>(composition));

  sarchuk::Matrix matrix = sarchuk::part(composition);

  std::cout << "\nThere are " << matrix.getRows() << " layers and " << matrix.getColumns() << " columns;\n";

  for (size_t i = 0; i < matrix.getRows(); i++) {
    for (size_t j = 0; j < matrix.getLayerSize(i); j++) {
      std::cout << "\nLayer " << i << ";\n" << "Figure " << j << ";\n";
      std::cout << "Centre is [";
      std::cout << matrix.getElement(i,j)->getFrameRect().pos.x << ", " << matrix.getElement(i,j)->getFrameRect().pos.y << "];\n";
    }
  }

  std::cout << "\nNumber of shapes in composite shape : " << composition.getSize() << ";\n";
  printInfo(std::make_shared<sarchuk::CompositeShape>(composition));

  std::cout << "\nRemove first element from composite shape.\n";
  composition.remove(0);
  std::cout << "\nNumber of shapes in composite shape : " << composition.getSize() << ";\n";
  printInfo(std::make_shared<sarchuk::CompositeShape>(composition));

  return 0;
}
