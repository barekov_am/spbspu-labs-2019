#include "partition.hpp"
#include <string.h>
#include <iostream>


sarchuk::Matrix sarchuk::part(const sarchuk::CompositeShape &composition)
{

  const size_t size = composition.getSize();
  size_t maxR = 0, maxC = 0;
  size_t tmpL[size], tmpC[size];
  memset(tmpC, 0, sizeof(tmpC));
  memset(tmpL, 0, sizeof(tmpL));
  for (size_t i = 0; i < size; i ++)
  {
    size_t layer = 0;
    for (size_t j = 0; j < maxR; j ++)
    {
      bool intersect = false;
      for (size_t k = 0; k < i; k ++)
      {
        if (tmpL[k] == j && isIntersected(composition[i]->getFrameRect(), composition[k]->getFrameRect()))
        {
          layer ++;
          intersect = true;
          break;
        }

      }
      if (! intersect)
      {
        break;
      }
    }
    tmpL[i] = layer;
    tmpC[layer] ++;
    maxC = std::max(tmpC[layer], maxC);
    if (tmpC[layer] == 1)
      maxR ++;

  }
  CompositeShape::array tmpData = std::make_unique<CompositeShape::pointer[]>(maxR * maxC);


  for (size_t i = 0; i < maxR; i ++)
  {
    for (size_t j = 0; j < maxC; j ++)
    {
      tmpData[i * maxC + j] = nullptr;
    }
  }

  for (size_t i = 0; i < size; ++ i)
  {
    for (size_t j = 0; j < maxC; ++ j)
    {
      if (tmpData[tmpL[i] * maxC + j] == nullptr)
      {
        tmpData[tmpL[i] * maxC + j] = composition[i];
        break;
      }
    }

  }


  return Matrix(tmpData, maxR, maxC);
}
