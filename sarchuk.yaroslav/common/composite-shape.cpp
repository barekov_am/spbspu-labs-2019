#include "composite-shape.hpp"

#include <math.h>
#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <utility>


sarchuk::CompositeShape::CompositeShape() :
  count_(0)
{
}

sarchuk::CompositeShape::CompositeShape(const CompositeShape &obj) :
  count_(obj.count_),
  shapes_(std::make_unique<sarchuk::CompositeShape::pointer[]>(obj.count_))
{
  for (std::size_t i = 0; i < count_; i ++)
  {
    shapes_[i] = obj.shapes_[i];
  }
}

sarchuk::CompositeShape::CompositeShape(CompositeShape &&obj) :
  count_(obj.count_),
  shapes_(std::move(obj.shapes_))
{
  obj.count_ = 0;
}


int sarchuk::CompositeShape::getSize() const
{
  return count_;
}

sarchuk::CompositeShape &sarchuk::CompositeShape::operator=(const CompositeShape &opd)
{
  if (this != &opd)
  {
    count_ = opd.count_;
    shapes_ = std::make_unique<sarchuk::CompositeShape::pointer[]>(opd.count_);


    for (std::size_t i = 0; i < count_; i ++)
    {
      shapes_[i] = opd.shapes_[i];
    }
  }
  return *this;

}

sarchuk::CompositeShape &sarchuk::CompositeShape::operator=(CompositeShape &&opd)
{
  if (this != &opd)
  {
    std::swap(count_, opd.count_);
    shapes_.swap(opd.shapes_);
  }

  return *this;
}

double sarchuk::CompositeShape::getArea() const
{
  double totalArea = 0;
  for (std::size_t i = 0; i < count_; i ++)
  {
    totalArea += shapes_[i]->getArea();
  }
  return totalArea;
}

sarchuk::rectangle_t sarchuk::CompositeShape::getFrameRect() const
{
  if (! count_)
  {
    return {{0, 0}, 0, 0};
  }

  rectangle_t tmpRect = shapes_[0]->getFrameRect();

  double minX = tmpRect.pos.x - (tmpRect.width) / 2;
  double maxX = tmpRect.pos.x + (tmpRect.width) / 2;

  double minY = tmpRect.pos.y - (tmpRect.height) / 2;
  double maxY = tmpRect.pos.y + (tmpRect.height) / 2;

  for (std::size_t i = 1; i < count_; i++) {
    tmpRect = shapes_[i]->getFrameRect();

    minX = std::min(minX, tmpRect.pos.x - (tmpRect.width) / 2);
    maxX = std::max(maxX, tmpRect.pos.x + (tmpRect.width) / 2);

    minY = std::min(minY, tmpRect.pos.y - (tmpRect.height) / 2);
    maxY = std::max(maxY, tmpRect.pos.y + (tmpRect.height) / 2);
  }

  return { { (maxX + minX) / 2, (maxY + minY) / 2 }, (maxX - minX), (maxY - minY) };


}

void sarchuk::CompositeShape::move(double dx, double dy)
{
  for (std::size_t i = 0; i < count_; i ++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void sarchuk::CompositeShape::move(point_t point)
{
  const rectangle_t tmpRect = getFrameRect();
  const double dx = point.x - tmpRect.pos.x;
  const double dy = point.y - tmpRect.pos.y;
  move(dx, dy);
}

void sarchuk::CompositeShape::writeInfo() const
{
  const rectangle_t tmpRect = getFrameRect();
  std::cout << "Composite Shape info:\n"
            << "Elements count - " << getSize() << "\n"
            << "Width - " << tmpRect.width << "\n"
            << "Height - " << tmpRect.height << "\n"
            << "Center position - x = " << tmpRect.pos.x << ", y = " << tmpRect.pos.y << "\n"
            << "Area - " << getArea() << "\n\n";
}

void sarchuk::CompositeShape::scale(double mult)
{
  if (mult <= 0.0)
  {
    throw std::invalid_argument("Multiplier must be more than 0");
  }

  const point_t center = getFrameRect().pos;
  for (std::size_t i = 0; i < count_; i ++)
  {
    const double dx = shapes_[i]->getFrameRect().pos.x - center.x;
    const double dy = shapes_[i]->getFrameRect().pos.y - center.y;
    shapes_[i]->move({center.x + dx * mult, center.y + dy * mult});
    shapes_[i]->scale(mult);
  }
}

void sarchuk::CompositeShape::add(const CompositeShape::pointer shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Pointer does not indicate anything");
  }

  CompositeShape::array tmp = std::make_unique<CompositeShape::pointer[]>(count_ + 1);
  for (std::size_t i = 0; i < count_; i ++)
  {
    tmp[i] = shapes_[i];
  }
  tmp[count_] = shape;
  count_ ++;
  shapes_.swap(tmp);
}

void sarchuk::CompositeShape::remove(std::size_t idx)
{
  if (idx >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }

  for (std::size_t i = idx; i < count_ - 1; i ++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  count_ --;
}

sarchuk::CompositeShape::pointer sarchuk::CompositeShape::operator[](std::size_t n) const
{
  if (n < count_)
  {
    return shapes_[n];
  }
  throw std::out_of_range{"Index is out of range"};
}

void sarchuk::CompositeShape::rotate(double angle)
{

  const double cosA = std::cos((2 * M_PI * angle) / 360.0);
  const double sinA = std::sin((2 * M_PI * angle) / 360.0);

  const point_t compCentre = getFrameRect().pos;

  for (std::size_t i = 0; i < count_; i ++)
  {
    const point_t shapeCenter = shapes_[i]->getFrameRect().pos;

    const double dx = (shapeCenter.x - compCentre.x) * cosA - (shapeCenter.y - compCentre.y) * sinA;
    const double dy = (shapeCenter.x - compCentre.x) * sinA + (shapeCenter.y - compCentre.y) * cosA;

    shapes_[i]->move({compCentre.x + dx, compCentre.y + dy});
    shapes_[i]->rotate(angle);
  }
}
