#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

const double Error = 0.01;

BOOST_AUTO_TEST_SUITE(rectangleTest)

  BOOST_AUTO_TEST_CASE(resistanceToMovement)
  {
    sarchuk::Rectangle rectangle({2, 5}, 4, 6);
    const sarchuk::rectangle_t rectFrame = rectangle.getFrameRect();
    const double rectArea = rectangle.getArea();
    rectangle.move({1, 1});
    BOOST_CHECK_EQUAL(rectFrame.width, rectangle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectFrame.height, rectangle.getFrameRect().height);
    BOOST_CHECK_CLOSE(rectArea, rectangle.getArea(), Error);
    rectangle.move(1, 2);
    BOOST_CHECK_EQUAL(rectFrame.width, rectangle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectFrame.height, rectangle.getFrameRect().height);
    BOOST_CHECK_CLOSE(rectArea, rectangle.getArea(), Error);
  }

  BOOST_AUTO_TEST_CASE(areaScaling)
  {
    sarchuk::Rectangle rect({2, 2}, 4, 8);
    const double area = rect.getArea();
    const double mult = 2.5;
    rect.scale(mult);
    BOOST_CHECK_CLOSE(rect.getArea(), mult * mult * area, Error);
  }

  BOOST_AUTO_TEST_CASE(inncorrectData)
  {
    BOOST_CHECK_THROW(sarchuk::Rectangle rectangle({1, 1}, - 1, 1), std::invalid_argument);
    BOOST_CHECK_THROW(sarchuk::Rectangle rectangle({1, 1}, 1, - 1), std::invalid_argument);

    const sarchuk::rectangle_t rectIncorrectWidth = {{1, 1}, - 1, 1};
    BOOST_CHECK_THROW(sarchuk::Rectangle rectangle(rectIncorrectWidth), std::invalid_argument);

    const sarchuk::rectangle_t rectIncorrectHeight = {{1, 1}, 1, - 1};
    BOOST_CHECK_THROW(sarchuk::Rectangle rectangle(rectIncorrectHeight), std::invalid_argument);

    sarchuk::Rectangle rect({1, 1}, 1, 1);
    BOOST_CHECK_THROW(rect.scale(0), std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(testRectangleRotation)
  {
    sarchuk::Rectangle testRectangle({ 7, 2 }, 7.1, 2);
    const double areaBefore = testRectangle.getArea();
    const sarchuk::rectangle_t frameRectBefore = testRectangle.getFrameRect();

    double angle = -90;
    testRectangle.rotate(angle);
    double areaAfter = testRectangle.getArea();
    sarchuk::rectangle_t frameRectAfter = testRectangle.getFrameRect();

    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, Error);
    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, Error);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, Error);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, Error);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, Error);

    angle = 40;
    testRectangle.rotate(angle);
    angle = 140;
    testRectangle.rotate(angle);

    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, Error);
    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, Error);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, Error);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, Error);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, Error);
  }

BOOST_AUTO_TEST_SUITE_END()
