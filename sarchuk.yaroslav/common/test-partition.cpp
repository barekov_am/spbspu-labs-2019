#include <boost/test/auto_unit_test.hpp>

#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

const double Error = 0.00001;

BOOST_AUTO_TEST_SUITE(testPartition)

BOOST_AUTO_TEST_CASE(intersectCorrectness)
{
  sarchuk::Circle testCircle({ 2, 0 }, 3);
  sarchuk::Rectangle testRectangle({ 2, 4 }, 9, 5);

  BOOST_CHECK(sarchuk::isIntersected(testCircle.getFrameRect(),testRectangle.getFrameRect()));
}

BOOST_AUTO_TEST_CASE(partitionCorrectness)
{
  sarchuk::CompositeShape::pointer testCircle = std::make_shared<sarchuk::Circle>(sarchuk::point_t{ 1, 1 }, 2);
  sarchuk::CompositeShape::pointer testRectangle1 = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t{ 3, -1 }, 5, 4);
  sarchuk::CompositeShape::pointer testSquare1 = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t{ 6, 15 }, 4,4);
  sarchuk::CompositeShape::pointer testRectangle2 = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t{ 10, 2 }, 2, 3);
  sarchuk::CompositeShape::pointer testRectangle3 = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t{ 8, 15 }, 6, 30);
  sarchuk::CompositeShape::pointer testSquare2 = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t{ -20, 15 }, 5,5);

  sarchuk::CompositeShape testComposition;
  testComposition.add(testCircle);
  testComposition.add(testRectangle1);
  testComposition.add(testSquare1);
  testComposition.add(testRectangle2);
  testComposition.add(testRectangle3);
  testComposition.add(testSquare2);

  sarchuk::Matrix testMatrix = sarchuk::part(testComposition);

  const std::size_t correctRows = 3;
  const std::size_t correctColumns = 4;

  BOOST_CHECK_EQUAL(testMatrix.getRows(), correctRows);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), correctColumns);
  BOOST_CHECK(testMatrix.getElement(0,0) == testCircle);
  BOOST_CHECK(testMatrix.getElement(0,1) == testSquare1);
  BOOST_CHECK(testMatrix.getElement(0,2) == testRectangle2);
  BOOST_CHECK(testMatrix.getElement(0,3) == testSquare2);
  BOOST_CHECK(testMatrix.getElement(1,0) == testRectangle1);
  BOOST_CHECK(testMatrix.getElement(2,0) == testRectangle3);


  BOOST_CHECK_THROW(testMatrix.getElement(1,10000000), std::out_of_range);
}


BOOST_AUTO_TEST_SUITE_END()
