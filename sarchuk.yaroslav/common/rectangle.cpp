#include "rectangle.hpp"
#include <stdexcept>
#include <iostream>
#include <math.h>

sarchuk::Rectangle::Rectangle(point_t position, double width, double height) :
  pos_(position),
  width_(width),
  height_(height),
  rotationAngle_(0)
{
  if ((width <= 0) || (height <= 0))
  {
    throw std::invalid_argument("Width and height must be more than 0");
  }
}

sarchuk::Rectangle::Rectangle(rectangle_t info) :
  pos_(info.pos),
  width_(info.width),
  height_(info.height),
  rotationAngle_(0)
{
  if ((info.width <= 0) || (info.height <= 0))
  {
    throw std::invalid_argument("Width and height must be more than 0");
  }
}

double sarchuk::Rectangle::getArea() const
{
  return width_ * height_;
}

void sarchuk::Rectangle::writeInfo() const
{
  std::cout << "Rectangle info:\n"
            << "Width - " << width_ << "\n"
            << "Height - " << height_ << "\n"
            << "Center position - x = " << pos_.x << ", y = " << pos_.y << "\n"
            << "Area - " << getArea() << "\n\n";
}

sarchuk::rectangle_t sarchuk::Rectangle::getFrameRect() const
{
  const double cosA = std::cos((2 * M_PI * rotationAngle_) / 360);
  const double sinA = std::sin((2 * M_PI * rotationAngle_) / 360);
  const double width = width_ * std::fabs(cosA) + height_ * std::fabs(sinA);
  const double height = height_ * std::fabs(cosA) + width_ * std::fabs(sinA);
  return {pos_, width, height};

}

void sarchuk::Rectangle::move(point_t pos)
{
  pos_ = pos;
}

void sarchuk::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void sarchuk::Rectangle::scale(double mult)
{
  if (mult <= 0)
  {
    throw std::invalid_argument("Multiplier must be more than 0");
  }
  width_ *= mult;
  height_ *= mult;
}

void sarchuk::Rectangle::rotate(double angle)
{
  rotationAngle_ += angle;

  while (std::abs(rotationAngle_) >= 360)
  {
    rotationAngle_ = (angle > 0) ? rotationAngle_ - 360 : rotationAngle_ + 360;
  }

}


