#include <boost/test/auto_unit_test.hpp>
#include <iostream>
#include <stdexcept>
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

const double Error = 0.1;

BOOST_AUTO_TEST_SUITE(compositeTest)

  BOOST_AUTO_TEST_CASE(resistanceToMovement)
  {
    sarchuk::CompositeShape::pointer tmpCircle = std::make_shared<sarchuk::Circle>(sarchuk::point_t{1, 2}, 3);
    sarchuk::CompositeShape::pointer tmpRectangle = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t{1, 2}, 3, 4);
    sarchuk::CompositeShape testComposite;
    testComposite.add((tmpCircle));
    testComposite.add(tmpRectangle);
    const sarchuk::rectangle_t startRect = testComposite.getFrameRect();
    const double startArea = testComposite.getArea();

    testComposite.move({5, - 5});
    sarchuk::rectangle_t rect = testComposite.getFrameRect();
    double CurrentArea = testComposite.getArea();
    BOOST_CHECK_CLOSE(startRect.width, rect.width, Error);
    BOOST_CHECK_CLOSE(startRect.height, rect.height, Error);
    BOOST_CHECK_CLOSE(startArea, CurrentArea, Error);

    testComposite.move(3, 5);
    rect = testComposite.getFrameRect();
    CurrentArea = testComposite.getArea();
    BOOST_CHECK_CLOSE(startRect.width, rect.width, Error);
    BOOST_CHECK_CLOSE(startRect.height, rect.height, Error);
    BOOST_CHECK_CLOSE(startArea, CurrentArea, Error);

  }

  BOOST_AUTO_TEST_CASE(areaScaling)
  {

    sarchuk::Circle testCircle({1, 2}, 3);
    sarchuk::Rectangle testRectangle({1, 2}, 3, 4);
    sarchuk::CompositeShape testComposite;
    sarchuk::CompositeShape::pointer tmpCircle = std::make_shared<sarchuk::Circle>(sarchuk::point_t{1, 2}, 3);
    sarchuk::CompositeShape::pointer tmpRectangle = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t{1, 2}, 3, 4);
    const double startArea = testComposite.getArea();
    const double mult = 2.5;
    testComposite.scale(mult);
    double CurrentArea = testComposite.getArea();
    BOOST_CHECK_CLOSE(startArea * mult * mult, CurrentArea, Error);

  }

  BOOST_AUTO_TEST_CASE(copyCompositeShape)
  {
    sarchuk::CompositeShape testComposite1;
    sarchuk::CompositeShape::pointer tmpCircle = std::make_shared<sarchuk::Circle>(sarchuk::point_t{1, 2}, 3);
    sarchuk::CompositeShape::pointer tmpRectangle = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t{1, 2}, 3, 4);
    testComposite1.add(tmpRectangle);
    testComposite1.add(tmpCircle);
    sarchuk::CompositeShape testComposite2;
    sarchuk::CompositeShape tmp(testComposite1);
    for (int i = 0; i < tmp.getSize(); i ++)
    {
      BOOST_CHECK_EQUAL(tmp[i], testComposite1[i]);
    }

    testComposite2 = testComposite1;
    for (int i = 0; i < testComposite2.getSize(); i ++)
    {
      BOOST_CHECK_EQUAL(testComposite2[i], testComposite1[i]);
    }
  }

  BOOST_AUTO_TEST_CASE(inncorrectData)
  {

    sarchuk::CompositeShape::pointer tmpCircle = std::make_shared<sarchuk::Circle>(sarchuk::point_t{1, 2}, 3);
    sarchuk::CompositeShape::pointer tmpRectangle = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t{1, 2}, 3, 4);

    sarchuk::CompositeShape testComposite;
    testComposite.add(tmpCircle);
    testComposite.add(tmpRectangle);

    BOOST_CHECK_THROW(testComposite.add(nullptr), std::invalid_argument);

    testComposite.remove(0);
    testComposite.remove(0);

    BOOST_CHECK_THROW(testComposite.remove(0), std::out_of_range);
    BOOST_CHECK_THROW(testComposite[1337], std::out_of_range);

  }

  BOOST_AUTO_TEST_CASE(testCompositeShapeRotation)
  {
    sarchuk::CompositeShape::pointer testCircle = std::make_shared<sarchuk::Circle>(sarchuk::point_t{3, 2.2}, 3);
    sarchuk::CompositeShape::pointer testRectangle = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t{4, 5}, 9, 2);
    sarchuk::CompositeShape testComposition;

    testComposition.add(testCircle);
    testComposition.add(testRectangle);

    const double areaBefore = testComposition.getArea();
    const sarchuk::rectangle_t frameRectBefore = testComposition.getFrameRect();

    double angle = 360;
    testComposition.rotate(angle);

    double areaAfter = testComposition.getArea();
    sarchuk::rectangle_t frameRectAfter = testComposition.getFrameRect();

    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, Error);
    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, Error);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, Error);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, Error);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, Error);

    angle = 90;
    testComposition.rotate(angle);

    areaAfter = testComposition.getArea();
    frameRectAfter = testComposition.getFrameRect();

    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, Error);
    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, Error);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, Error);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, Error);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, Error);

  }

BOOST_AUTO_TEST_SUITE_END()
