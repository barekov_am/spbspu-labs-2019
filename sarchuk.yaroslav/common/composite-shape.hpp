#ifndef A3_COMPOSITE_SHAPE
#define A3_COMPOSITE_SHAPE

#include <memory>

#include "shape.hpp"
#include "base-types.hpp"

namespace sarchuk
{
  class CompositeShape : public Shape
  {
  public:
    using pointer = std::shared_ptr<Shape>;
    using array = std::unique_ptr<pointer[]>;

    CompositeShape();
    CompositeShape(const CompositeShape&);
    CompositeShape(CompositeShape&&);
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape&);
    CompositeShape& operator =(CompositeShape&&);


    CompositeShape::pointer operator [](std::size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void writeInfo() const override;
    void move(double, double) override;
    void move(point_t) override;
    void scale(double) override;
    void rotate(double) override;
    void add(const CompositeShape::pointer);
    void remove(std::size_t);
    int getSize() const;

  private:
    std::size_t count_;
    array shapes_;
  };
}

#endif
