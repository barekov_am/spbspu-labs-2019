#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace sarchuk {

  Matrix part(const CompositeShape & composition);

}

#endif
