#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "composite-shape.hpp"

namespace sarchuk {

  class Matrix {
  public:
    Matrix();
    Matrix(const CompositeShape::array &data,size_t rows,size_t columns);
    Matrix(const Matrix & rhs);
    Matrix(Matrix && rhs);
    ~Matrix() = default;

    Matrix & operator =(const Matrix & rhs);
    Matrix & operator =(Matrix && rhs);

    CompositeShape::array operator [](size_t layer) const;
    CompositeShape::pointer getElement (size_t x, size_t y) const;
    bool operator ==(const Matrix & rhs) const;
    bool operator !=(const Matrix & rhs) const;

    std::size_t getRows() const;
    std::size_t getColumns() const;
    std::size_t getLayerSize(size_t layer) const;

    void add(const CompositeShape::pointer & shape, std::size_t layer);

  private:
    CompositeShape::array data_;
    std::size_t rows_;
    std::size_t columns_;
  };

}

#endif
