#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double Error = 0.01;

BOOST_AUTO_TEST_SUITE(circleTest)

  BOOST_AUTO_TEST_CASE(resistanceToMovement)
  {
    sarchuk::Circle circle({2, 5}, 5);
    const sarchuk::rectangle_t rectFrame = circle.getFrameRect();
    const double rectArea = circle.getArea();
    circle.move({1, 1});
    BOOST_CHECK_EQUAL(rectFrame.width, circle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectFrame.height, circle.getFrameRect().height);
    BOOST_CHECK_CLOSE(rectArea, circle.getArea(), Error);

    circle.move(1, 2);
    BOOST_CHECK_EQUAL(rectFrame.width, circle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectFrame.height, circle.getFrameRect().height);
    BOOST_CHECK_CLOSE(rectArea, circle.getArea(), Error);
  }

  BOOST_AUTO_TEST_CASE(areaScaling)
  {
    sarchuk::Circle circle({2, 2}, 5);
    const double area = circle.getArea();
    const double mult = 2.5;
    circle.scale(mult);
    BOOST_CHECK_CLOSE(circle.getArea(), mult * mult * area, Error);
  }

  BOOST_AUTO_TEST_CASE(incorrectData)
  {
    BOOST_CHECK_THROW(sarchuk::Circle circle({1, 1}, - 2), std::invalid_argument);

    sarchuk::Circle circle({1, 2}, 8);
    BOOST_CHECK_THROW(circle.scale(0), std::invalid_argument);
  }


BOOST_AUTO_TEST_SUITE_END()
