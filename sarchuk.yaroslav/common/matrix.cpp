#include "matrix.hpp"
#include "composite-shape.hpp"

sarchuk::Matrix::Matrix():
  rows_(0),
  columns_(0)
{
}

sarchuk::Matrix::Matrix(const CompositeShape::array &data,size_t rows,size_t columns):
  rows_(rows),
  columns_(columns)

{
  data_ =  std::make_unique<CompositeShape::pointer[]>(rows * columns);
  for (size_t i = 0; i < rows * columns; ++ i)
  {
    data_[i] = data[i];
  }
}

sarchuk::Matrix::Matrix(const Matrix & rhs):
  data_(std::make_unique<CompositeShape::pointer[]>(rhs.rows_ * rhs.columns_)),
  rows_(rhs.rows_),
  columns_(rhs.columns_)
{
  for (std::size_t i = 0; i < (rows_ * columns_); i++) {
    data_[i] = rhs.data_[i];
  }
}

sarchuk::Matrix::Matrix(Matrix && rhs):
  data_(std::move(rhs.data_)),
  rows_(rhs.rows_),
  columns_(rhs.columns_)
{
  rhs.rows_ = 0;
  rhs.columns_ = 0;
}

sarchuk::Matrix & sarchuk::Matrix::operator =(const Matrix & rhs)
{
  if (&rhs == this) {
    return *this;
  }

  CompositeShape::array tmpData = std::make_unique<CompositeShape::pointer[]>(rhs.rows_ * rhs.columns_);
  for (std::size_t i = 0; i < (rhs.rows_ * rhs.columns_); i++) {
    tmpData[i] = rhs.data_[i];
  }

  data_.swap(tmpData);
  rows_ = rhs.rows_;
  columns_ = rhs.columns_;

  return *this;
}

sarchuk::Matrix & sarchuk::Matrix::operator =(Matrix && rhs)
{
  if (&rhs == this) {
    return *this;
  }
  rows_ = rhs.rows_;
  columns_ = rhs.columns_;
  data_ = std::move(rhs.data_);
  rhs.rows_ = 0;
  rhs.columns_ = 0;

  return *this;
}

sarchuk::CompositeShape::array sarchuk::Matrix::operator [](size_t layer) const
{
  if (layer >= rows_) {
    throw std::out_of_range("Index is out of range!\n");
  }

  CompositeShape::array tmpData = std::make_unique<CompositeShape::pointer[]>(getLayerSize(layer));

  for (std::size_t i = 0; i < getLayerSize(layer); i++) {
    tmpData[i] = data_[layer * columns_ + i];
  }

  return tmpData;
}
sarchuk::CompositeShape::pointer sarchuk::Matrix::getElement (size_t x, size_t y) const
{
  if (x >= rows_ || y >= columns_) {
    throw std::out_of_range("Index is out of range!\n");
  }

  return  data_[x * columns_ + y];
}

bool sarchuk::Matrix::operator ==(const Matrix & rhs) const
{
  if ((rows_ != rhs.rows_) || (columns_ != rhs.columns_)) {
    return false;
  }

  for (std::size_t i = 0; i < (rows_ * columns_); i++){
    if (data_[i] != rhs.data_[i]) {
      return false;
    }
  }

  return true;
}

bool sarchuk::Matrix::operator !=(const Matrix &rhs) const
{
  return !(*this == rhs);
}

std::size_t sarchuk::Matrix::getRows() const
{
  return rows_;
}

std::size_t sarchuk::Matrix::getColumns() const
{
  return columns_;
}

std::size_t sarchuk::Matrix::getLayerSize(size_t layer) const
{
  if (layer >= rows_) {
    return 0;
  }

  for (std::size_t i = 0; i < columns_; i++) {
    if (data_[layer * columns_ + i] == nullptr) {
      return i;
    }
  }

  return columns_;
}

void sarchuk::Matrix::add(const CompositeShape::pointer & shape, std::size_t layer)
{
  if (shape == nullptr) {
    throw std::invalid_argument("Pointer can't be null!\n");
  }
  const std::size_t tmpRows = (layer >= rows_) ? (rows_ + 1) : rows_;
  const std::size_t tmpColumns = (getLayerSize(layer) == columns_) ? (columns_ + 1) : columns_;
  const std::size_t column = (layer < rows_) ? getLayerSize(layer) : 0;

  CompositeShape::array tmpData = std::make_unique<CompositeShape::pointer[]>(tmpRows * tmpColumns);

  for (std::size_t i = 0; i < tmpRows; i++) {
    for (std::size_t j = 0; j < tmpColumns; j++) {
      if ((i == rows_) || (j == columns_)) {
        tmpData[i * tmpColumns + j] = nullptr;
      } else {
        tmpData[i * tmpColumns + j] = data_[i * columns_ + j];
      }
    }
  }

  if (layer < rows_) {
    tmpData[layer * tmpColumns + column] = shape;
  } else {
    tmpData[rows_ * tmpColumns + column] = shape;
  }

  data_.swap(tmpData);
  rows_ = tmpRows;
  columns_ = tmpColumns;
}
