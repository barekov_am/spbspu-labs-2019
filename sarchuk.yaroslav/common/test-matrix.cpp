#include <boost/test/auto_unit_test.hpp>

#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

const double Error = 0.00001;

BOOST_AUTO_TEST_SUITE(testMatrix)

  BOOST_AUTO_TEST_CASE(testCopyConstructor)
  {
    sarchuk::CompositeShape::pointer testCircle = std::make_shared<sarchuk::Circle>(sarchuk::point_t { 3.5, 4.5 }, 3);
    sarchuk::CompositeShape::pointer testRectangle = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t { 1, 0 }, 1, 4);
    sarchuk::CompositeShape testComposition;

    testComposition.add(testCircle);
    testComposition.add(testRectangle);

    sarchuk::Matrix testMatrix = sarchuk::part(testComposition);
    sarchuk::Matrix testMatrixCopy(testMatrix);

    BOOST_CHECK(testMatrix == testMatrixCopy);
    BOOST_CHECK_EQUAL(testMatrixCopy.getRows(), testMatrix.getRows());
    BOOST_CHECK_EQUAL(testMatrixCopy.getColumns(), testMatrix.getColumns());
  }

  BOOST_AUTO_TEST_CASE(testMoveConstructor)
  {
    sarchuk::CompositeShape::pointer testCircle = std::make_shared<sarchuk::Circle>(sarchuk::point_t { 1, 2 }, 3);
    sarchuk::CompositeShape::pointer testRectangle = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t { 1, 2 }, 1, 6);
    sarchuk::CompositeShape testComposition;

    testComposition.add(testCircle);
    testComposition.add(testRectangle);

    sarchuk::Matrix testMatrix = sarchuk::part(testComposition);
    sarchuk::Matrix testMatrixCopy(testMatrix);
    sarchuk::Matrix testMatrixMove = std::move(testMatrix);

    BOOST_CHECK(testMatrixMove == testMatrixCopy);
    BOOST_CHECK_EQUAL(testMatrixMove.getRows(), testMatrixCopy.getRows());
    BOOST_CHECK_EQUAL(testMatrixMove.getColumns(), testMatrixCopy.getColumns());;
  }

  BOOST_AUTO_TEST_CASE(testCopyOperator)
  {
    sarchuk::CompositeShape::pointer testCircle = std::make_shared<sarchuk::Circle>(sarchuk::point_t { 3, 7.2 }, 11);
    sarchuk::CompositeShape::pointer testRectangle = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t { 2, 4 }, 9, 2);
    sarchuk::CompositeShape testComposition;

    testComposition.add(testCircle);
    testComposition.add(testRectangle);

    sarchuk::Matrix testMatrix = sarchuk::part(testComposition);
    sarchuk::Matrix testMatrixCopy;
    testMatrixCopy = testMatrix;

    BOOST_CHECK(testMatrixCopy == testMatrix);
    BOOST_CHECK_EQUAL(testMatrixCopy.getRows(), testMatrix.getRows());
    BOOST_CHECK_EQUAL(testMatrixCopy.getColumns(), testMatrix.getColumns());;
  }

  BOOST_AUTO_TEST_CASE(testMoveOperator)
  {
    sarchuk::CompositeShape::pointer testCircle = std::make_shared<sarchuk::Circle>(sarchuk::point_t { 2, 0 }, 3);
    sarchuk::CompositeShape::pointer testRectangle = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t { 2, 4 }, 9, 5);
    sarchuk::CompositeShape testComposition;

    testComposition.add(testCircle);
    testComposition.add(testRectangle);

    sarchuk::Matrix testMatrix = sarchuk::part(testComposition);
    sarchuk::Matrix testMatrixCopy(testMatrix);
    sarchuk::Matrix testMatrixMove;
    testMatrixMove = std::move(testMatrix);

    BOOST_CHECK(testMatrixMove == testMatrixCopy);
    BOOST_CHECK_EQUAL(testMatrixMove.getRows(), testMatrixCopy.getRows());
    BOOST_CHECK_EQUAL(testMatrixMove.getColumns(), testMatrixCopy.getColumns());;
  }

  BOOST_AUTO_TEST_CASE(exceptOutOfRange)
  {
    sarchuk::CompositeShape::pointer testCircle = std::make_shared<sarchuk::Circle>(sarchuk::point_t { 2, 0 }, 3);
    sarchuk::CompositeShape::pointer testRectangle = std::make_shared<sarchuk::Rectangle>(sarchuk::point_t { 2, 4 }, 9, 5);
    sarchuk::CompositeShape testComposition;

    testComposition.add(testCircle);
    testComposition.add(testRectangle);

    sarchuk::Matrix testMatrix = sarchuk::part(testComposition);

    BOOST_CHECK_THROW(testMatrix[100], std::out_of_range);
    BOOST_CHECK_THROW(testMatrix[-2], std::out_of_range);
  }

BOOST_AUTO_TEST_SUITE_END()
