#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <iterator>
#include <memory>

#include "print.hpp"

void task2(const std::string &filePath)
{
  std::ifstream file(filePath);
  if (!file.is_open())
  {
    throw std::runtime_error{ "Сan not get access to file."};
  }
  typedef std::unique_ptr<char[], decltype(&free)> cArrPtr;
  size_t size = 0;
  size_t length = 10;
  cArrPtr cArr{static_cast<char *>(realloc(nullptr, length)), free};
  if (!cArr)
  {
    throw std::bad_alloc();
  }
  while (!file.eof())
  {
    file.read(&(cArr[size]), length - size);
    size += file.gcount();
    if (size == length)
    {
      length *= 2;
      cArrPtr cArrBuf(static_cast< char * >(realloc(cArr.get(), length)), free);
      if (! cArrBuf)
      {
        throw std::bad_alloc();
      }
      cArr.swap(cArrBuf);
      cArrBuf.release();
    }
  }
  if (! file.eof() && file.fail())
  {
    throw std::runtime_error{"Failed reading from file"};
  }
  file.close();

  std::vector<char> data{&cArr[0], &cArr[size]};
  print(data, "", "");
}
