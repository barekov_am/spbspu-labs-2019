#include <vector>
#include <string>
#include <iostream>
#include <random>

#include "sort.hpp"
#include "print.hpp"

void fillRandom(double *data, const size_t size)
{
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-1.0, 1.0);

  for (size_t i = 0; i < size; i ++)
  {
    data[i] = dis(gen);
  }
}

void task4(const std::string &inputCompare, const std::string &stringSize)
{
  size_t size = std::stoi(stringSize);
  if (size <= 0)
  {
    throw std::invalid_argument("Incorrect vector size");
  }
  std::vector<double> numbers(size);
  fillRandom(numbers.data(), size);
  auto comparator = getComparator<double>(inputCompare);
  print(numbers);
  sort<IteratorAccess>(numbers, comparator);
  print(numbers);
}
