#include <iostream>
#include <stdexcept>

void task1(const std::string &);
void task2(const std::string &);
void task3();
void task4(const std::string &, const std::string &);

bool chekArgsCount(const int &argc, const int &task){
  switch (task) {
    case 1: case 2:
    {
      if (argc == 3)
      {
        return true;
      }
      break;
    }
    case 3:
    {
      if (argc == 2)
      {
        return true;
      }
      break;
    }
    case 4:
    {
      if (argc == 4)
      {
        return true;
      }
      break;
    }
  }
  return false;
}

int main(int argc, char* argv[])
{
  try {
    if ((argc < 2) || (argc > 4)) {
      std::cerr << "Incorrect number of arguments." << std::endl;
      return 1;
    }

    const int task = std::stoi(argv[1]);

    if(!chekArgsCount(argc, task)){
      std::cerr << "Incorrect number of arguments for "<< task << " task."<<std::endl;
      return  1;
    }
    switch (task) {
      case 1: {
        task1(argv[2]);
        break;
      }

      case 2: {
        task2(argv[2]);
        break;
      }

      case 3: {
        task3();
        break;
      }

      case 4: {
        task4(argv[2], argv[3]);
        break;
      }

      default: {
        std::cerr << "Incorrect task number.\n";
        return 1;
      }
    }
  } catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
    return 1;
  }
  return 0;
}
