#include "Statistics.hpp"
#include <limits>
#include <stdexcept>

Statistics::Statistics() :
  max_(std::numeric_limits<int>::min()),
  min_(std::numeric_limits<int>::max()),
  oddSum_(0),
  evenSum_(0),
  first_(0),
  positiveCount_(0),
  negativeCount_(0),
  count_(0),
  firstLastEqual(false)
{}

void Statistics::operator()(long long int value)
{
  if (count_ == 0)
  {
    first_ = value;
  }

  if (value > max_)
  {
    max_ = value;
  }

  if (value < min_)
  {
    min_ = value;
  }

  if (value > 0)
  {
    positiveCount_++;
  }

  if (value < 0)
  {
    negativeCount_++;
  }

  if (value % 2 == 0)
  {
    evenSum_ += value;
  }
  else
  {
    oddSum_ += value;
  }

  firstLastEqual = (value == first_);

  count_++;
}

long long int Statistics::getMax() const
{
  if (count_ < 1)
  {
    throw std::logic_error("Not enough numbers for statistics");
  }
  return max_;
}

long long int Statistics::getMin() const
{
  if (count_ < 1)
  {
    throw std::logic_error("Not enough numbers for statistics");
  }
  return min_;
}

long double Statistics::getMean() const
{
  if (count_ < 1)
  {
    throw std::logic_error("Not enough numbers for statistics");
  }
  return static_cast<double> (oddSum_ + evenSum_) / static_cast<double>(count_);
}

std::size_t Statistics::getPositiveCount() const
{
  if (count_ < 1)
  {
    throw std::logic_error("Not enough numbers for statistics");
  }
  return positiveCount_;
}

std::size_t Statistics::getNegativeCount() const
{
  if (count_ < 1)
  {
    throw std::logic_error("Not enough numbers for statistics");
  }
  return negativeCount_;
}

long long int Statistics::getOddSum() const
{
  if (count_ < 1)
  {
    throw std::logic_error("Not enough numbers for statistics");
  }
  return oddSum_;
}

long long int Statistics::getEvenSum() const
{
  if (count_ < 1)
  {
    throw std::logic_error("Not enough numbers for statistics");
  }
  return evenSum_;
}

bool Statistics::isFirstLastEqual() const
{
  if (count_ < 1)
  {
    throw std::logic_error("Not enough numbers for statistics");
  }
  return firstLastEqual;
}
