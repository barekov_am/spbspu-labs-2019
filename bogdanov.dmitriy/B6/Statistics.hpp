#ifndef B_STATISTICS_HPP
#define B_STATISTICS_HPP

#include <cstdio>

class Statistics
{
public:
  Statistics();
  void operator ()(long long int value);
  long long int getMax() const;
  long long int getMin() const;
  long double getMean() const;
  size_t getPositiveCount() const;
  size_t getNegativeCount() const;
  long long int getOddSum() const;
  long long int getEvenSum() const;
  bool isFirstLastEqual() const;

private:
  long long int max_, min_, oddSum_, evenSum_, first_;
  size_t positiveCount_, negativeCount_, count_;
  bool firstLastEqual;
};

#endif //B_STATISTICS_HPP
