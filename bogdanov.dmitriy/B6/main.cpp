#include <algorithm>
#include <functional>
#include <iostream>
#include <iterator>
#include "Statistics.hpp"

int main()
{
  Statistics Stat;

  std::for_each(std::istream_iterator<long long int>(std::cin), std::istream_iterator<long long int>(), std::ref(Stat));

  if (!std::cin.eof())
  {
    std::cerr << "Non integral value" << std::endl;
    return 1;
  }

  try
  {
    std::cout << "Max: " << Stat.getMax() << std::endl;
    std::cout << "Min: " << Stat.getMin() << std::endl;
    std::cout << "Mean: " << Stat.getMean() << std::endl;
    std::cout << "Positive: " << Stat.getPositiveCount() << std::endl;
    std::cout << "Negative: " << Stat.getNegativeCount() << std::endl;
    std::cout << "Odd Sum: " << Stat.getOddSum() << std::endl;
    std::cout << "Even Sum: " << Stat.getEvenSum() << std::endl;
    std::cout << "First/Last Equal: ";
    Stat.isFirstLastEqual()? std::cout << "yes\n" : std::cout << "no\n";
  }
  catch (std::logic_error)
  {
    std::cout << "No Data" << std::endl;
    return 0;
  }

  return 0;
}
