#include "matrix.hpp"

bogdanov::Matrix::Matrix() :
  data_(),
  rows_(0),
  columns_(0)
{}

bogdanov::Matrix::Matrix(const Matrix & rhs) :
  data_(std::make_unique<Shape::pointer []>(rhs.rows_ * rhs.columns_)),
  rows_(rhs.rows_),
  columns_(rhs.columns_)
{
  for (std::size_t i = 0; i < (rows_ * columns_); ++i) {
    data_[i] = rhs.data_[i];
  }
}

bogdanov::Matrix::Matrix(bogdanov::Matrix && rhs) :
  data_(std::move(rhs.data_)),
  rows_(rhs.rows_),
  columns_(rhs.columns_)
{
  rhs.rows_ = 0;
  rhs.columns_ = 0;
}

bogdanov::Matrix & bogdanov::Matrix::operator =(const Matrix & rhs)
{
  if (this != &rhs) {
    Shape::arrayPointer arrTmp = std::make_unique<Shape::pointer []>(rhs.rows_ * rhs.columns_);
    for (std::size_t i = 0; i < (rhs.rows_ * rhs.columns_); ++i) {
      arrTmp[i] = rhs.data_[i];
    }

    data_.swap(arrTmp);
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
  }

  return *this;
}

bogdanov::Matrix & bogdanov::Matrix::operator =(Matrix && rhs)
{
  if (this != &rhs) {
    data_ = std::move(rhs.data_);
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
    rhs.rows_ = 0;
    rhs.columns_ = 0;
  }

  return *this;
}

bogdanov::Shape::arrayPointer bogdanov::Matrix::operator [](std::size_t layer) const
{
  if (layer >= rows_) {
    throw std::out_of_range("Index is out of range!\n");
  }

  Shape::arrayPointer arrTmp = std::make_unique<Shape::pointer []>(getLayerSize(layer));

  for (std::size_t i = 0; i < getLayerSize(layer); ++i) {
    arrTmp[i] = data_[layer * columns_ + i];
  }

  return arrTmp;
}

bogdanov::Shape::pointer bogdanov::Matrix::operator ()(std::size_t layer, std::size_t column) const
{
  if (layer >= rows_) {
    throw std::out_of_range("Index is out of range!\n");
  }

  if (column >= columns_) {
    throw std::out_of_range("Index is out of range!\n");
  }

  return data_[layer * columns_ + column];
}

bool bogdanov::Matrix::operator ==(const bogdanov::Matrix & rhs) const
{
  if ((rows_ != rhs.rows_) || (columns_ != rhs.columns_)) {
    return false;
  }

  for (std::size_t i = 0; i < (columns_ * rows_); ++i) {
    if (data_[i] != rhs.data_[i]) {
      return false;
    }
  }

  return true;
}

bool bogdanov::Matrix::operator !=(const bogdanov::Matrix & rhs) const
{
  return !(*this == rhs);
}

std::size_t bogdanov::Matrix::getRows() const
{
  return rows_;
}

std::size_t bogdanov::Matrix::getColumns() const
{
  return columns_;
}

std::size_t bogdanov::Matrix::getLayerSize(size_t layer) const
{
  if (layer >= rows_) {
    return 0;
  }

  for (std::size_t i = 0; i < columns_; ++i) {
    if (data_[layer * columns_ + i] == nullptr) {
      return i;
    }
  }

  return columns_;
}

void bogdanov::Matrix::add(const bogdanov::Shape::pointer &shape, std::size_t layer)
{
  if (shape == nullptr) {
    throw std::invalid_argument("Pointer can't be null!\n");
  }

  std::size_t tmpRows = (layer >= rows_) ? (rows_ + 1) : rows_;
  std::size_t tmpColumns = (getLayerSize(layer) == columns_) ? (columns_ + 1) : columns_;

  const std::size_t column = (layer < rows_) ? getLayerSize(layer) : 0;

  Shape::arrayPointer tmpData = std::make_unique<Shape::pointer[]>(tmpRows * tmpColumns);

  for (std::size_t i = 0; i < tmpRows; i++) {
    for (std::size_t j = 0; j < tmpColumns; j++) {
      if ((i != rows_) && (j != columns_)) {
        tmpData[i * tmpColumns + j] = data_[i * columns_ + j];
      }
    }
  }

  if (layer < rows_) {
    tmpData[layer * tmpColumns + column] = shape;
  } else {
    tmpData[rows_ * tmpColumns + column] = shape;
  }

  data_.swap(tmpData);
  rows_ = tmpRows;
  columns_ = tmpColumns;
}
