#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace bogdanov
{
  struct point_t
  {
    double x, y;
  };

  struct rectangle_t
  {
    point_t pos;
    double width, height;
  };

  bool isIntersected(const rectangle_t & lhs, const rectangle_t & rhs);

}  //namespace bogdanov

#endif
