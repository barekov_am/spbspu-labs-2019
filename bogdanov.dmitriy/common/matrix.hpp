#ifndef LABA3_MATRIX_HPP
#define LABA3_MATRIX_HPP

#include "shape.hpp"

namespace bogdanov {

  class Matrix {
  public:
    Matrix();
    Matrix(const Matrix & rhs);
    Matrix(Matrix && rhs);
    ~Matrix() = default;

    Matrix & operator =(const Matrix & rhs);
    Matrix & operator =(Matrix && rhs);

    Shape::arrayPointer operator [](std::size_t layer) const;
    Shape::pointer operator ()(std::size_t layer, std::size_t column) const;
    bool operator ==(const Matrix & rhs) const;
    bool operator !=(const Matrix & rhs) const;

    std::size_t getRows() const;
    std::size_t getColumns() const;
    std::size_t getLayerSize(size_t layer) const;

    void add(const Shape::pointer & shape, std::size_t layer);

  private:
    Shape::arrayPointer data_;
    std::size_t rows_;
    std::size_t columns_;
  };

}

#endif //LABA3_MATRIX_HPP
