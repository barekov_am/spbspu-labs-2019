#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>
#include <math.h>

#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

#define ACCURACY 0.001
const double FULLCIRCLE = 360;

BOOST_AUTO_TEST_SUITE(TestComposite)

BOOST_AUTO_TEST_CASE(testWorkingConstructor)
{
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t {7.0, 7.0}, 10.0, 6.0);
  bogdanov::CompositeShape compositeShape(rectangle);

  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().width, rectangle->getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().height, rectangle->getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, rectangle->getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, rectangle->getFrameRect().pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testWorkingConstructorCopy)
{
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t {5.0, 5.0}, 6.0, 7.0);
  bogdanov::CompositeShape compositeShape(rectangle);
  bogdanov::CompositeShape compositeShapeCopy(compositeShape);

  BOOST_CHECK(&compositeShape != &compositeShapeCopy);
  BOOST_CHECK(compositeShape.getSize() == compositeShapeCopy.getSize());

  BOOST_CHECK(compositeShape[0] == compositeShapeCopy[0]);
}

BOOST_AUTO_TEST_CASE(testWorkingOperatorCopy)
{
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t {5.0, 5.0}, 6.0, 7.0);
  bogdanov::CompositeShape compositeShape(rectangle);
  bogdanov::CompositeShape compositeShapeCopy;
  compositeShapeCopy = compositeShape;

  BOOST_CHECK(&compositeShape != &compositeShapeCopy);
  BOOST_CHECK(compositeShape.getSize() == compositeShapeCopy.getSize());

  BOOST_CHECK(compositeShape[0] == compositeShapeCopy[0]);
}

BOOST_AUTO_TEST_CASE(testWorkingConstructorMoving)
{
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t {5.0, 5.0}, 6.0, 7.0);
  bogdanov::CompositeShape compositeShape(rectangle);
  bogdanov::CompositeShape compositeShapeCopy(compositeShape);
  bogdanov::CompositeShape compositeShapeMoving(std::move(compositeShape));

  BOOST_CHECK(&compositeShapeMoving != &compositeShapeCopy);
  BOOST_CHECK(compositeShapeMoving.getSize() == compositeShapeCopy.getSize());
  BOOST_CHECK(compositeShapeMoving[0] == compositeShapeCopy[0]);

  BOOST_CHECK(&compositeShape != &compositeShapeMoving);
  BOOST_CHECK(compositeShape.getSize() != compositeShapeMoving.getSize());

  BOOST_CHECK(compositeShape.getSize() == 0);
  BOOST_CHECK_THROW(compositeShape[0]->getFrameRect(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(testWorkingOperatorMoving)
{
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t {5.0, 5.0}, 6.0, 7.0);
  bogdanov::CompositeShape compositeShape(rectangle);
  bogdanov::CompositeShape compositeShapeCopy(compositeShape);
  bogdanov::CompositeShape compositeShapeMoving;
  compositeShapeMoving = std::move(compositeShape);

  BOOST_CHECK(&compositeShapeMoving != &compositeShapeCopy);
  BOOST_CHECK(compositeShapeMoving.getSize() == compositeShapeCopy.getSize());
  BOOST_CHECK(compositeShapeMoving[0] == compositeShapeCopy[0]);

  BOOST_CHECK(&compositeShape != &compositeShapeMoving);
  BOOST_CHECK(compositeShape.getSize() != compositeShapeMoving.getSize());

  BOOST_CHECK(compositeShape.getSize() == 0);
  BOOST_CHECK_THROW(compositeShape[0]->getFrameRect(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(pointCenterShouldCoincideWithOnlyRectangle)
{
  bogdanov::point_t centerRectangle = {7.0, 6.0};
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(centerRectangle, 10.0, 6.0);
  bogdanov::CompositeShape compositeShape(rectangle);

  const double deltaFirst = 5.0;
  compositeShape.move(deltaFirst, deltaFirst);

  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, rectangle->getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, rectangle->getFrameRect().pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(pointCenterShouldConstAfterMovingDxDy)
{
  bogdanov::point_t centerRectangle = {7.0, 6.0};
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(centerRectangle, 10.0, 6.0);
  bogdanov::CompositeShape compositeShape(rectangle);

  const double deltaFirst = 5.0;
  bogdanov::rectangle_t frameRectBefore = compositeShape.getFrameRect();
  compositeShape.move(deltaFirst, deltaFirst);

  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, frameRectBefore.pos.x + deltaFirst, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, frameRectBefore.pos.y + deltaFirst, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle->getFrameRect().pos.y, centerRectangle.y + deltaFirst, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle->getFrameRect().pos.x, centerRectangle.x + deltaFirst, ACCURACY);

  centerRectangle.x += deltaFirst;
  centerRectangle.y += deltaFirst;

  bogdanov::point_t centerCircle = {-4.0, -6.0};
  bogdanov::Shape::pointer circle = std::make_shared<bogdanov::Circle>(centerCircle, 10.0);
  compositeShape.add(circle);

  frameRectBefore = compositeShape.getFrameRect();
  const double deltaSecond = 8.0;
  compositeShape.move(deltaSecond, deltaSecond);

  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, frameRectBefore.pos.y + deltaSecond, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, frameRectBefore.pos.x + deltaSecond, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle->getFrameRect().pos.x, centerRectangle.x + deltaSecond, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle->getFrameRect().pos.y, centerRectangle.y + deltaSecond, ACCURACY);
  BOOST_CHECK_CLOSE(circle->getFrameRect().pos.x, centerCircle.x + deltaSecond, ACCURACY);
  BOOST_CHECK_CLOSE(circle->getFrameRect().pos.y, centerCircle.y + deltaSecond, ACCURACY);
}

BOOST_AUTO_TEST_CASE(pointCenterShouldConstAfterMovingCenter)
{
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t {7.0, 7.0}, 10.0, 6.0);
  bogdanov::CompositeShape compositeShape(rectangle);

  compositeShape.move({0, 0});
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, rectangle->getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, rectangle->getFrameRect().pos.y, ACCURACY);

  bogdanov::Shape::pointer circle = std::make_shared<bogdanov::Circle>(bogdanov::point_t {-4.0, -4.0}, 10.0);
  compositeShape.add(circle);

  bogdanov::point_t pointCenter({20.0, 20.0});
  compositeShape.move(pointCenter);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, pointCenter.x, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, pointCenter.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(frameRectangleShouldConstAfterMovingCentre)
{
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t {5.0, 5.0}, 2.0, 5.0);
  bogdanov::Shape::pointer circle = std::make_shared<bogdanov::Circle>(bogdanov::point_t {7.0, 7.0}, 10.0);
  bogdanov::CompositeShape compositeShape;

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  const bogdanov::rectangle_t frameRectBefore = compositeShape.getFrameRect();
  const double areaBefore = compositeShape.getArea();

  rectangle->move(5.0, 5.0);
  BOOST_CHECK_CLOSE(frameRectBefore.width, compositeShape.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height, compositeShape.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, compositeShape.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(squareShouldChangeQuadratically)
{
  bogdanov::Shape::pointer circle = std::make_shared<bogdanov::Circle>(bogdanov::point_t {7.0, 7.0}, 10.0);
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t {5.0, 5.0}, 2.0, 5.0);
  bogdanov::CompositeShape compositeShape;

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  const double areaBefore = compositeShape.getArea();
  const double scaleFactor = 2.0;

  compositeShape.scale(scaleFactor);
  BOOST_CHECK_CLOSE(areaBefore * scaleFactor * scaleFactor, compositeShape.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(wrongParametersScaleCompositeShape)
{
  bogdanov::Shape::pointer circle = std::make_shared<bogdanov::Circle>(bogdanov::point_t {7.0, 7.0}, 10.0);
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t {5.0, 5.0}, 2.0, 5.0);
  bogdanov::CompositeShape compositeShape;

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  double scaleFactor = 0;
  BOOST_CHECK_THROW(compositeShape.scale(scaleFactor), std::invalid_argument);
  scaleFactor = -7;
  BOOST_CHECK_THROW(compositeShape.scale(scaleFactor), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(wrongParametersRemoveCompositeShape)
{
  bogdanov::Shape::pointer circle = std::make_shared<bogdanov::Circle>(bogdanov::point_t {7.0, 7.0}, 10.0);
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t {5.0, 5.0}, 2.0, 5.0);
  bogdanov::CompositeShape compositeShape;

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  BOOST_CHECK_THROW(compositeShape.remove(compositeShape.getSize()), std::invalid_argument);

  compositeShape.remove(1);
  compositeShape.remove(0);
  BOOST_CHECK_THROW(compositeShape.remove(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(movementCompositeShapeShuldReturnNull)
{
  bogdanov::Shape::pointer circle = std::make_shared<bogdanov::Circle>(bogdanov::point_t {7.0, 7.0}, 10.0);
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t {5.0, 5.0}, 2.0, 5.0);
  bogdanov::CompositeShape compositeShape;
  bogdanov::CompositeShape compositeShapeMovement;

  compositeShapeMovement.add(circle);
  compositeShapeMovement.add(rectangle);

  const size_t sizeComShapeMov = compositeShapeMovement.getSize();

  compositeShape = std::move(compositeShapeMovement);

  BOOST_CHECK_EQUAL(compositeShape.getSize(), sizeComShapeMov);

  BOOST_CHECK_THROW(compositeShapeMovement.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(compositeShapeMovement.remove(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeRotation)
{
  bogdanov::Shape::pointer circle = std::make_shared<bogdanov::Circle>(bogdanov::point_t { 3, 2.2 }, 3);
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t { 4, 5 }, 9, 2);
  bogdanov::CompositeShape compositeShape;

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  const double areaBefore = compositeShape.getArea();
  const bogdanov::rectangle_t frameRectBefore = compositeShape.getFrameRect();

  double angle = 360;
  compositeShape.rotate(angle);

  double areaAfter = compositeShape.getArea();
  bogdanov::rectangle_t frameRectAfter = compositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ACCURACY);

  angle = 90;
  compositeShape.rotate(angle);

  areaAfter = compositeShape.getArea();
  frameRectAfter = compositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ACCURACY);

  angle = -450;
  compositeShape.rotate(angle);

  areaAfter = compositeShape.getArea();
  frameRectAfter = compositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ACCURACY);
}

BOOST_AUTO_TEST_CASE(checkingFormulaInRotate)
{
  bogdanov::Shape::pointer circle = std::make_shared<bogdanov::Circle>(bogdanov::point_t { 3, 2.2 }, 3);
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t { 4, 5 }, 9, 2);
  bogdanov::CompositeShape compositeShape;

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  const bogdanov::point_t centerCompShape = compositeShape.getFrameRect().pos;
  const double angle = FULLCIRCLE / 10;
  const double sinA = std::sin((2 * angle * M_PI) / FULLCIRCLE);
  const double cosA = std::cos((2 * angle * M_PI) / FULLCIRCLE);

  const double deltaXForCircle = (circle->getFrameRect().pos.x - centerCompShape.x) * cosA - (circle->getFrameRect().pos.y - centerCompShape.y) * sinA;
  const double deltaYForCircle = (circle->getFrameRect().pos.x - centerCompShape.x) * sinA + (circle->getFrameRect().pos.y - centerCompShape.y) * cosA;

  const double deltaXForRectangle = (rectangle->getFrameRect().pos.x - centerCompShape.x) * cosA - (rectangle->getFrameRect().pos.y - centerCompShape.y) * sinA;
  const double deltaYForRectangle = (rectangle->getFrameRect().pos.x - centerCompShape.x) * sinA + (rectangle->getFrameRect().pos.y - centerCompShape.y) * cosA;

  compositeShape.rotate(angle);
  BOOST_CHECK_CLOSE(compositeShape[0]->getFrameRect().pos.x, centerCompShape.x + deltaXForCircle, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape[0]->getFrameRect().pos.y, centerCompShape.y + deltaYForCircle, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape[1]->getFrameRect().pos.x, centerCompShape.x + deltaXForRectangle, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape[1]->getFrameRect().pos.y, centerCompShape.y + deltaYForRectangle, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
