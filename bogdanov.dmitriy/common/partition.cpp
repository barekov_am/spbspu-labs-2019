#include "partition.hpp"

bogdanov::Matrix bogdanov::part(const bogdanov::CompositeShape & compositeShape)
{
  Matrix tmpMatrix;
  const std::size_t size = compositeShape.getSize();

  for (std::size_t i = 0; i < size; i++) {
    std::size_t layer = 0;

    for (std::size_t j = 0; j < tmpMatrix.getRows(); j++) {
      bool intersect = false;

      for (std::size_t k = 0; k < tmpMatrix.getLayerSize(j); k++) {
        if (isIntersected(compositeShape[i]->getFrameRect(), tmpMatrix(j, k)->getFrameRect())) {
          layer++;
          intersect = true;
          break;
        }
      }

      if (!intersect) {
        break;
      }
    }

    tmpMatrix.add(compositeShape[i], layer);
  }

  return tmpMatrix;
}
