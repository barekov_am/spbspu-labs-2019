#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace bogdanov {

  bogdanov::Matrix part(const bogdanov::CompositeShape & compositeShape);

}

#endif
