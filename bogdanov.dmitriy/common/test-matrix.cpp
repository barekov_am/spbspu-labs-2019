#include <boost/test/auto_unit_test.hpp>

#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(testCopyConstructor)
{
  bogdanov::Shape::pointer circle = std::make_shared<bogdanov::Circle>(bogdanov::point_t { 5.5, 4.5 }, 7);
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t { 2, 0 }, 3, 1);
  bogdanov::CompositeShape compositeShape;

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  bogdanov::Matrix matrix = bogdanov::part( compositeShape);
  bogdanov::Matrix matrixCopy(matrix);

  BOOST_CHECK(matrix == matrixCopy);
  BOOST_CHECK_EQUAL(matrixCopy.getRows(), matrix.getRows());
  BOOST_CHECK_EQUAL(matrixCopy.getColumns(), matrix.getColumns());
}

BOOST_AUTO_TEST_CASE(testMoveConstructor)
{
  bogdanov::Shape::pointer circle = std::make_shared<bogdanov::Circle>(bogdanov::point_t { 3, 2 }, 5);
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t { 2, 2 }, 3, 6);
  bogdanov::CompositeShape testComposition;

  testComposition.add(circle);
  testComposition.add(rectangle);

  bogdanov::Matrix matrix = bogdanov::part(testComposition);
  bogdanov::Matrix matrixCopy(matrix);
  bogdanov::Matrix matrixMove = std::move(matrix);

  BOOST_CHECK(matrixMove == matrixCopy);
  BOOST_CHECK_EQUAL(matrixMove.getRows(), matrixCopy.getRows());
  BOOST_CHECK_EQUAL(matrixMove.getColumns(), matrixCopy.getColumns());;
}

BOOST_AUTO_TEST_CASE(testCopyOperator)
{
  bogdanov::Shape::pointer circle = std::make_shared<bogdanov::Circle>(bogdanov::point_t { 3, 7 }, 12);
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t { 8, 5 }, 7, 2);
  bogdanov::CompositeShape compositeShape;

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  bogdanov::Matrix matrix = bogdanov::part(compositeShape);
  bogdanov::Matrix matrixCopy;
  matrixCopy = matrix;

  BOOST_CHECK(matrixCopy == matrix);
  BOOST_CHECK_EQUAL(matrixCopy.getRows(), matrix.getRows());
  BOOST_CHECK_EQUAL(matrixCopy.getColumns(), matrix.getColumns());;
}

BOOST_AUTO_TEST_CASE(testMoveOperator)
{
  bogdanov::Shape::pointer circle = std::make_shared<bogdanov::Circle>(bogdanov::point_t { 2, 1 }, 5);
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t { 3, 2 }, 9, 5);
  bogdanov::CompositeShape compositeShape;

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  bogdanov::Matrix matrix = bogdanov::part(compositeShape);
  bogdanov::Matrix matrixCopy(matrix);
  bogdanov::Matrix matrixMove;
  matrixMove = std::move(matrix);

  BOOST_CHECK(matrixMove == matrixCopy);
  BOOST_CHECK_EQUAL(matrixMove.getRows(), matrixCopy.getRows());
  BOOST_CHECK_EQUAL(matrixMove.getColumns(), matrixCopy.getColumns());;
}

BOOST_AUTO_TEST_CASE(exceptOutOfRange)
{
  bogdanov::Shape::pointer circle = std::make_shared<bogdanov::Circle>(bogdanov::point_t { 2, 0 }, 3);
  bogdanov::Shape::pointer rectangle = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t { 3, 6 }, 8, 4);
  bogdanov::CompositeShape compositeShape;

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  bogdanov::Matrix matrix = bogdanov::part(compositeShape);

  BOOST_CHECK_THROW(matrix[105], std::out_of_range);
  BOOST_CHECK_THROW(matrix[-2], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
