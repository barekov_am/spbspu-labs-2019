#include <boost/test/auto_unit_test.hpp>

#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testPartition)

BOOST_AUTO_TEST_CASE(intersectCorrectness)
{
  bogdanov::Circle circle({2, 0}, 3);
  bogdanov::Rectangle rectangle({2, 4}, 9, 5);

  BOOST_CHECK(bogdanov::isIntersected(circle.getFrameRect(),rectangle.getFrameRect()));
}

BOOST_AUTO_TEST_CASE(partitionCorrectness)
{
  bogdanov::Shape::pointer circle = std::make_shared<bogdanov::Circle>(bogdanov::point_t{1, 1}, 2);
  bogdanov::Shape::pointer rectangle1 = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t{3, -1}, 5, 4);
  bogdanov::Shape::pointer square1 = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t{6, 15}, 4, 8);
  bogdanov::Shape::pointer rectangle2 = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t{10, 2}, 2, 3);
  bogdanov::Shape::pointer rectangle3 = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t{8, 15}, 6, 30);
  bogdanov::Shape::pointer square2   = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t{-20, 15}, 5, 9);

  bogdanov::CompositeShape compositeShape;
  compositeShape.add(circle);
  compositeShape.add(rectangle1);
  compositeShape.add(square1);
  compositeShape.add(rectangle2);
  compositeShape.add(rectangle3);
  compositeShape.add(square2);

  bogdanov::Matrix matrix = bogdanov::part(compositeShape);

  const std::size_t correctRows = 3;
  const std::size_t correctColumns = 4;

  BOOST_CHECK_EQUAL(matrix.getRows(), correctRows);
  BOOST_CHECK_EQUAL(matrix.getColumns(), correctColumns);
  BOOST_CHECK(matrix(0, 0) == circle);
  BOOST_CHECK(matrix(0, 1) == square1);
  BOOST_CHECK(matrix(0, 2) == rectangle2);
  BOOST_CHECK(matrix(0, 3) == square2);
  BOOST_CHECK(matrix(1, 0) == rectangle1);
  BOOST_CHECK(matrix(2, 0) == rectangle3);
}

BOOST_AUTO_TEST_SUITE_END()
