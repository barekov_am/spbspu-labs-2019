#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace bogdanov
{
  class CompositeShape: public Shape {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &comShape);
    CompositeShape(CompositeShape &&comShape);
    CompositeShape(Shape::pointer &firstShape);
    ~CompositeShape() override = default;

    CompositeShape &operator =(const CompositeShape &comShape);
    CompositeShape &operator =(CompositeShape &&comShape);
    Shape::pointer operator [](std::size_t) const;

    void move(const point_t &newCenter) override;
    void move(double dx, double dy) override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void writeInfo() const override;
    void scale(double scalingFactor) override;
    void rotate(double angle) override;

    void swap(CompositeShape& shape);
    void add(Shape::pointer &newShape);
    void remove(size_t index);
    std::size_t getSize() const;
    std::size_t getIndex(const Shape::pointer & shape) const;

  private:
    Shape::arrayPointer shapes_;
    std::size_t size_;
  };
}

#endif
