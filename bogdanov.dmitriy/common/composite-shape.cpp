#include "composite-shape.hpp"

#include <iostream>
#include <math.h>

const double FULLCIRCLE = 360;

bogdanov::CompositeShape::CompositeShape() :
  shapes_(nullptr),
  size_(0)
{}

bogdanov::CompositeShape::CompositeShape(const bogdanov::CompositeShape &comShape) :
  shapes_(std::make_unique<bogdanov::Shape::pointer []>(comShape.size_)),
  size_(comShape.size_)
{
  for (size_t i = 0; i < size_; i++) {
    shapes_[i] = comShape.shapes_[i];
  }
}

bogdanov::CompositeShape::CompositeShape(bogdanov::CompositeShape &&comShape) :
  shapes_(nullptr),
  size_(0)
{
  swap(comShape);
}

bogdanov::CompositeShape::CompositeShape(bogdanov::Shape::pointer &firstShape) :
  shapes_(std::make_unique<bogdanov::Shape::pointer []>(1)),
  size_(1)
{
  shapes_[0] = firstShape;
}

bogdanov::CompositeShape &bogdanov::CompositeShape::operator =(const bogdanov::CompositeShape &comShape)
{
  if (this != &comShape) {
    shapes_ = std::make_unique<bogdanov::Shape::pointer []>(comShape.size_);
    size_ = comShape.size_;

    for(size_t i = 0; i < size_; i++) {
      shapes_[i] = comShape.shapes_[i];
    }
  }

  return *this;
}

bogdanov::CompositeShape &bogdanov::CompositeShape::operator =(bogdanov::CompositeShape &&comShape)
{
  if (this != &comShape) {
    shapes_.reset();
    size_ = 0;
    swap(comShape);
  }

  return *this;
}

void bogdanov::CompositeShape::swap(bogdanov::CompositeShape& shape)
{
  std::swap(shapes_, shape.shapes_);
  std::swap(size_, shape.size_);
}

double bogdanov::CompositeShape::getArea() const
{
  double sumArea = 0;
  for (size_t i = 0; i < size_; i++) {
    sumArea += shapes_[i]->getArea();
  }

  return sumArea;
}

bogdanov::rectangle_t bogdanov::CompositeShape::getFrameRect() const
{
  if ((!shapes_) || (size_ == 0)) {
    throw std::logic_error("In this CompositeShape null shapes ");
  }

  rectangle_t tempFrameRect = shapes_[0]->getFrameRect();
  double minX = tempFrameRect.pos.x - tempFrameRect.width / 2;
  double maxX = tempFrameRect.pos.x + tempFrameRect.width / 2;
  double maxY = tempFrameRect.pos.y + tempFrameRect.height / 2;
  double minY = tempFrameRect.pos.y - tempFrameRect.height / 2;

  for (size_t i = 1; i < size_; i++) {
    tempFrameRect = shapes_[i]->getFrameRect();
    minX = std::min(minX, tempFrameRect.pos.x - tempFrameRect.width / 2);
    maxX = std::max(maxX, tempFrameRect.pos.x + tempFrameRect.width / 2);
    maxY = std::max(maxY, tempFrameRect.pos.y + tempFrameRect.height / 2);
    minY = std::min(minY, tempFrameRect.pos.y - tempFrameRect.height / 2);
  }

  return {{(maxX + minX) / 2, (maxY + minY) / 2}, maxX - minX, maxY - minY};
}

void bogdanov::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; i++) {
    shapes_[i]->move(dx, dy);
  }
}

void bogdanov::CompositeShape::move(const bogdanov::point_t &newCenter)
{
  double const dx = newCenter.x - getFrameRect().pos.x;
  double const dy = newCenter.y - getFrameRect().pos.y;
  move(dx, dy);
}

void bogdanov::CompositeShape::scale(double scalingFactor)
{
  if (scalingFactor <= 0) {
    throw std::invalid_argument("Scaling factor must be positive");
  }

  const point_t centerComposite = getFrameRect().pos;
  for (size_t i= 0; i < size_; i++) {
    const double dx = shapes_[i]->getFrameRect().pos.x - centerComposite.x;
    const double dy = shapes_[i]->getFrameRect().pos.y - centerComposite.y;
    shapes_[i]->move({centerComposite.x + dx * scalingFactor, centerComposite.y + dy * scalingFactor});
    shapes_[i]->scale(scalingFactor);
  }
}

void bogdanov::CompositeShape::writeInfo() const
{
  if ((!shapes_) || (size_ == 0)) {
    std::cout << "This CompositeShape is empty: \n";
    return;
  }

  const bogdanov::rectangle_t rectangle = getFrameRect();

  std::cout << "Information about the CompositeShape: \n";
  std::cout << "Number of shapes: " << size_ << std::endl;
  std::cout << "Center: (" << rectangle.pos.x << "," << rectangle.pos.y << ") \n";
  std::cout << "Frame Rectangle: width = " << rectangle.width << ", height = " << rectangle.height << "\n";
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "\nInformation about the Shape in CompositeShape\n" << std::endl;
}

size_t bogdanov::CompositeShape::getSize() const
{
  return size_;
}

void bogdanov::CompositeShape::add(bogdanov::Shape::pointer &newShape)
{
  if (getIndex(newShape) != getSize()) {
    return;
  }

  bogdanov::Shape::arrayPointer arrNew = std::make_unique<bogdanov::Shape::pointer []>(size_ + 1);

  for (size_t i = 0; i < size_; i++) {
    arrNew[i] = shapes_[i];
  }

  arrNew[size_++] = newShape;
  shapes_.swap(arrNew);
}

void bogdanov::CompositeShape::remove(std::size_t index)
{
  if (size_ == 0) {
    throw std::invalid_argument("Composite shape is empty");
  }
  if (index >= size_) {
    throw std::invalid_argument("Index is outside the array");
  }

  for (size_t i = index; i < size_ - 1; i++) {
    shapes_[i] = shapes_[i + 1];
  }

  bogdanov::Shape::arrayPointer arrNew = std::make_unique<bogdanov::Shape::pointer []>(size_--);

  for (size_t i = 0; i < size_; i++) {
    arrNew[i] = shapes_[i];
  }

  shapes_.swap(arrNew);
}

bogdanov::Shape::pointer bogdanov::CompositeShape::operator [](std::size_t number) const
{
  if (number < size_) {
    return shapes_[number];
  }
  throw std::out_of_range("Index was out of range in CompositeShape::operator[]");
}

void bogdanov::CompositeShape::rotate(double angle)
{
  const double cosA = std::cos((2 * M_PI * angle) / FULLCIRCLE);
  const double sinA = std::sin((2 * M_PI * angle) / FULLCIRCLE);

  const point_t compCentre = getFrameRect().pos;

  for (std::size_t i = 0; i < size_; i++) {
    const point_t shapeCenter = shapes_[i]->getFrameRect().pos;

    const double dx = (shapeCenter.x - compCentre.x) * cosA - (shapeCenter.y - compCentre.y) * sinA;
    const double dy = (shapeCenter.x - compCentre.x) * sinA + (shapeCenter.y - compCentre.y) * cosA;

    shapes_[i]->move({ compCentre.x + dx, compCentre.y + dy });
    shapes_[i]->rotate(angle);
  }

}

std::size_t bogdanov::CompositeShape::getIndex(const bogdanov::Shape::pointer &shape) const
{
  if (!shape) {
    throw std::invalid_argument("Pointer can't be null!\n");
  }

  for (std::size_t i = 0; i < size_; i++) {
    if (shapes_[i] == shape) {
      return i;
    }
  }

  return size_;
}
