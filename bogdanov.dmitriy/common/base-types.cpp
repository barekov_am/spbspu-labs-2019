#include "base-types.hpp"
#include <math.h>

bool bogdanov::isIntersected(const rectangle_t & lhs, const rectangle_t & rhs)
{
  const bool widthCondition = std::fabs(lhs.pos.x - rhs.pos.x) < (std::fabs(lhs.width + rhs.width) / 2);
  const bool heightCondition = std::fabs(lhs.pos.y - rhs.pos.y) < (std::fabs(lhs.height + rhs.height) / 2);

  return widthCondition && heightCondition;
}
