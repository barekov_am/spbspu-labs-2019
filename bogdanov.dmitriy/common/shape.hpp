#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace bogdanov
{
  class Shape {
  public:
    virtual ~Shape() = default;

    virtual void move(const point_t &newCenter) = 0;
    virtual void move(double dx, double dy) = 0;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void writeInfo() const = 0;
    virtual void scale(double scalingFactor) = 0;
    virtual void rotate(double angle) = 0;

    using pointer = std::shared_ptr<Shape>;
    using arrayPointer = std::unique_ptr<pointer[]>;

  };
} // namespace bogdanov

#endif
