#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "rectangle.hpp"

#define ACCURACY 0.001

BOOST_AUTO_TEST_SUITE(TestRectangle)

  struct FixtureCreationRectangle
  {
    FixtureCreationRectangle() :
      rectangle({1.0, 1.0}, 20.0, 30.0)
    {}
    bogdanov::Rectangle rectangle;
  };

BOOST_FIXTURE_TEST_CASE(widthHeightAreaOfTheRectangleShouldBePreservedAfterMovingOnDxDy, FixtureCreationRectangle)
{
  const bogdanov::rectangle_t initialRectangle = rectangle.getFrameRect();
  const double initialAreaRectangle = rectangle.getArea();

  rectangle.move(6.0, 6.0);

  BOOST_CHECK_CLOSE(initialRectangle.width, rectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialRectangle.height, rectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialAreaRectangle, rectangle.getArea(), ACCURACY);
}

BOOST_FIXTURE_TEST_CASE(widthHeightAreaOfTheRectangleShouldBePreservedAfterMovingInNewCenter, FixtureCreationRectangle)
{
  const bogdanov::rectangle_t initialRectangle = rectangle.getFrameRect();
  const double initialAreaRectangle = rectangle.getArea();

  rectangle.move({7.0,7.0});

  BOOST_CHECK_CLOSE(initialRectangle.width, rectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialRectangle.height, rectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialAreaRectangle, rectangle.getArea(), ACCURACY);
}

BOOST_FIXTURE_TEST_CASE(areaOfRectangleShouldChangeQuadraticallyAfterScaling,FixtureCreationRectangle)
{
  const double initialAreaRectangle = rectangle.getArea();
  const double scalingFactor = 0.5;

  rectangle.scale(scalingFactor);

  BOOST_CHECK_CLOSE(initialAreaRectangle * scalingFactor * scalingFactor, rectangle.getArea(), ACCURACY);
}

BOOST_FIXTURE_TEST_CASE(shouldReturnAnErrorDueToIncorrectParametersFromRectangle, FixtureCreationRectangle)
{
  BOOST_CHECK_THROW(bogdanov::Rectangle rec({1.0, 1.0}, -1.0, 1.0), std::invalid_argument);
  BOOST_CHECK_THROW(bogdanov::Rectangle rec({1.0, 1.0}, 1.0, -1.0), std::invalid_argument);
  BOOST_CHECK_THROW(bogdanov::Rectangle rec({1.0, 1.0}, -1.0, -1.0), std::invalid_argument);
  BOOST_CHECK_THROW(bogdanov::Rectangle rec({1.0, 1.0}, 0, 1.0), std::invalid_argument);
  BOOST_CHECK_THROW(bogdanov::Rectangle rec({1.0, 1.0}, 1.0, 0), std::invalid_argument);
  BOOST_CHECK_THROW(bogdanov::Rectangle rec({1.0, 1.0}, 0, 0), std::invalid_argument);

  BOOST_CHECK_THROW(rectangle.scale(-2.0), std::invalid_argument);
}

  BOOST_FIXTURE_TEST_CASE(testRectangleRotation, FixtureCreationRectangle)
{
  const double areaBefore = rectangle.getArea();
  const bogdanov::rectangle_t frameRectBefore = rectangle.getFrameRect();

  double angle = -90;
  rectangle.rotate(angle);
  double areaAfter = rectangle.getArea();
  bogdanov::rectangle_t frameRectAfter = rectangle.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ACCURACY);

  angle = 50;
  rectangle.rotate(angle);
  angle = 130;
  rectangle.rotate(angle);

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
