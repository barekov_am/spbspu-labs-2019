#include <fstream>
#include <memory>
#include <vector>
#include <iostream>

const size_t SIZE_READING = 1 << 20;

void task2 (const char *file)
{
  std::ifstream inFile;
  inFile.open(file);

  if (!inFile.is_open()) {
    throw std::ios_base::failure("Exception: Could not open the file");
  }

  size_t size = SIZE_READING;

  using charArray = std::unique_ptr<char[], decltype(&free)>;
  charArray chArray(static_cast<char *>(malloc(size)), &free);

  size_t n = 0;
  while(inFile) {
    inFile.read(&chArray[n], SIZE_READING);
    n += inFile.gcount();

    if (inFile.gcount() == SIZE_READING) {
      size += SIZE_READING;
      charArray tmp(static_cast<char *>(realloc(chArray.get(), size)), &free);

      chArray.release();
      std::swap(chArray, tmp);
    }

  }

  inFile.close();

  std::vector<char> vector(&chArray[0], &chArray[n]);

  for (char & i : vector) {
    std::cout << i;
  }
}
