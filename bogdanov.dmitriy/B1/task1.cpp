#include <vector>
#include "sort.hpp"
#include "print.hpp"
#include "access.hpp"

void task1(const char *direction)
{
  const Direction direction1 = getDirection(direction);

  std::vector<int> vector;
  int in = 0;
  while (std::cin >> in)
  {
    vector.push_back(in);
  }

  if (!std::cin.eof() && std::cin.fail()) {
    throw std::ios_base::failure("Exception: invalid character entered");
  }

  if (vector.empty()) {
    return;
  }

  std::vector<int> vectorBracket(vector);
  std::vector<int> vectorAt(vector.begin(), vector.end());
  std::vector<int> vectorIterator(vector.begin(), vector.end());

  sort<Bracket>(vectorBracket, direction1);
  sort<At>(vectorAt, direction1);
  sort<Iterator>(vectorIterator, direction1);

  print(vectorBracket);
  print(vectorAt);
  print(vectorIterator);
}
