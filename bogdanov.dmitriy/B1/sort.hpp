#ifndef SORT_HPP
#define SORT_HPP

#include <algorithm>
#include <functional>
#include <cstring>

enum Direction
{
  ascending,
  descending
};

Direction getDirection (const char* direction);

template <template <class T> class Access, typename T>
void sort (T &collection, Direction direction)
{
  typedef typename T::value_type value_type;

  const auto begin = Access<T>::begin(collection);
  const auto end = Access<T>::end(collection);

  std::function<bool(value_type, value_type)> compare;

  if (direction == ascending) {
    compare = std::less<value_type>();
  } else {
    compare = std::greater<value_type>();
  }

  for (auto i = begin; i != end; ++i) {
    auto min = i;

    for (auto j = Access<T>::next(i); j != end; ++j) {
      if (compare(Access<T>::get(collection, j), Access<T>::get(collection, min))) {
        min = j;
      }
    }

    if (min != i) {
      std::swap(Access<T>::get(collection, i), Access<T>::get(collection, min));
    }
  }

}

#endif //SORT_HPP
