#include <iostream>
#include <ctime>

void task1(const char *direction);
void task2(const char *file);
void task3();
void task4(const char *direction, size_t size);

int main(int args, char *argv[])
{
  if ((args < 2) || (args > 4)) {
    std::cerr << "Error: Invalid count args";
    return 1;
  }

  try {
    char *ptr = nullptr;
    int task = std::strtol(argv[1], &ptr, 10);

    if (*ptr) {
      std::cerr << "Error: Invalid argument of direction";
      return 1;
    }

    switch (task) {
    case 1:
      if (args != 3) {
        std::cerr << "Error: Wrong number args for task1";
        return 1;
      }
      task1(argv[2]);
      break;

    case 2:
      if (args != 3) {
        std::cerr << "Error: Wrong number args for task2";
        return 1;
      }
      task2(argv[2]);
      break;

    case 3:
      if (args != 2) {
        std::cerr << "Error: Wrong number of arguments";
        return 1;
      }
      task3();
      break;

    case 4:
      if (args != 4) {
        std::cerr << "Error: Wrong number of arguments";
        return 1;
      }
      srand(time(nullptr));
      task4(argv[2], std::stoi(argv[3]));
      break;

    default:
      std::cerr << "Error: Wrong number task";
      return 1;
    }
  } catch (const std::exception &e) {
    std::cerr << e.what() << "\n";
    return 1;
  }

  return 0;
}
