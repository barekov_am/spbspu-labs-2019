#include <iostream>

#include "../common/shape.hpp"
#include "../common/circle.hpp"
#include "../common/rectangle.hpp"
#include "../common/composite-shape.hpp"
#include "../common/partition.hpp"

void printInfo(const bogdanov::Shape::pointer & shapePointer)
{
  shapePointer->writeInfo();
}

int main()
{
  std::cout << "\n************** COMPOSITE_SHAPE **************\n";

  /** Create pointer circle with center (2,6) and radius (3)*/
  bogdanov::Shape::pointer circlePtr = std::make_shared<bogdanov::Circle>(bogdanov::point_t {2, 6}, 3);

  /*Create pointer rectangle with center (2,-3), width (6), height (8)*/
  bogdanov::Shape::pointer rectanglePtr = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t {2, -3}, 6, 8);
  /*Create pointer rectangle with center (0,-5), width (13), height (16)*/
  bogdanov::Shape::pointer rectanglePtrIntersect = std::make_shared<bogdanov::Rectangle>(bogdanov::point_t {0, -5}, 13, 16);

  std::cout << "\n************** CREATE composite shape **************\n";
  std::cout << "\nCreate composite shape with constructor with first Shape:\n";
  bogdanov::CompositeShape compositeShape(circlePtr);
  std::cout << "\nNew element of composite shape:\n";
  printInfo(compositeShape[compositeShape.getIndex(circlePtr)]);

  std::cout << "\nCreate composite shape with usually constructor:\n";
  bogdanov::Shape::pointer compositeShapeExpPtr = std::make_shared<bogdanov::CompositeShape>();
  std::cout << "\nNew element of composite shape:\n";
  printInfo(compositeShapeExpPtr);

  std::cout << "\n************** ADD and SIZE **************\n";
  // Shape add only one
  compositeShape.add(circlePtr);
  compositeShape.add(rectanglePtr);

  printInfo(compositeShape[compositeShape.getIndex(rectanglePtr)]);
  printInfo(compositeShape[compositeShape.getIndex(circlePtr)]);

  std::cout << "\nNumber of shapes in composite shape : " << compositeShape.getSize() << ";\n";

  std::cout << "\nCheck information about compositeShape:\n";
  printInfo(std::make_shared<bogdanov::CompositeShape>(compositeShape));

  std::cout << "\n************** SCALE composite shape **************\n";
  compositeShape.scale(4);
  printInfo(std::make_shared<bogdanov::CompositeShape>(compositeShape));

  std::cout << "\nComposite shape after return scale:\n";
  compositeShape.scale(0.25);
  printInfo(std::make_shared<bogdanov::CompositeShape>(compositeShape));

  std::cout << "\n************** MOVE composite shape by value (4,6): **************\n";
  compositeShape.move(4,6);
  printInfo(std::make_shared<bogdanov::CompositeShape>(compositeShape));

  std::cout << "\n************** MOVE composite to the point {1, 1}: **************\n";
  compositeShape.move({1, 1});
  printInfo(std::make_shared<bogdanov::CompositeShape>(compositeShape));

  std::cout << "\n************** ROTATE composite shape on 90 degrees: **************\n";
  compositeShape.rotate(90);
  printInfo(std::make_shared<bogdanov::CompositeShape>(compositeShape));

  std::cout << "\nComposite shape after return degrees:\n";
  compositeShape.rotate(-450);
  printInfo(std::make_shared<bogdanov::CompositeShape>(compositeShape));

  std::cout << "\n************** PARTITION **************\n";
  compositeShape.add(rectanglePtrIntersect);
  bogdanov::Matrix matrix = bogdanov::part(compositeShape);

  std::cout << "\nThere are " << matrix.getRows() << " layers and " << matrix.getColumns() << " columns;\n";

  for (size_t i = 0; i < matrix.getRows(); i++) {
    for (size_t j = 0; j < matrix.getLayerSize(i); j++) {
      std::cout << "\nLayer " << i << ";\n" << "Figure " << j << ";\n";
      std::cout << "Centre is [";
      std::cout << matrix[i][j]->getFrameRect().pos.x << ", " << matrix[i][j]->getFrameRect().pos.y << "];\n";
    }
  }

  std::cout << "\n**************REMOVE rectangle from composite shape.**************\n";
  compositeShape.remove(compositeShape.getIndex(rectanglePtr));
  std::cout << "\nNumber of shapes in composite shape : " << compositeShape.getSize() << ";\n";
  printInfo(std::make_shared<bogdanov::CompositeShape>(compositeShape));

  std::cout << "\nRemove first element from composite shape.\n";
  compositeShape.remove(0);
  std::cout << "\nNumber of shapes in composite shape : " << compositeShape.getSize() << ";\n";
  printInfo(std::make_shared<bogdanov::CompositeShape>(compositeShape));

  return 0;
}
