#include<iostream>
#include <vector>
#include <unordered_set>
#include <iterator>

void task1()
{
  std::unordered_set<std::string> words_unique{};
  std::string word;

  while(std::cin >> word)
  {
    words_unique.insert(word);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Input failed.\n");
  }

  std::copy(words_unique.begin(), words_unique.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
}
