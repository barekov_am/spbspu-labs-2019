#ifndef B_SHAPE_HPP
#define B_SHAPE_HPP

#include <vector>
#include <ostream>

struct point_t
{
  int x, y;
};

using Shape = std::vector< point_t >;

Shape readShape(const std::string &str);
void printPoint(std::ostream& out, const point_t& point);
void printShape(std::ostream& out, const Shape& shape);
int getDistancePoints(const point_t& a, const point_t& b);
bool isTriangle(const Shape& shape);
bool isRectangle(const Shape& shape);
bool isSquare(const Shape& shape);

#endif //B_SHAPE_HPP
