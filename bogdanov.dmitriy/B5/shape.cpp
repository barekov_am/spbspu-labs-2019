#include <sstream>
#include <limits>
#include <algorithm>
#include "shape.hpp"

Shape readShape(const std::string &str)
{
  std::stringstream stream(str);

  int n;
  stream >> n;

  Shape shape;

  for (int i = 0; i < n; ++i)
  {
    point_t point;
    stream.ignore(std::numeric_limits<std::streamsize>::max(),'(');
    stream >> point.x;

    stream.ignore(std::numeric_limits<std::streamsize>::max(),';');
    stream >> point.y;

    stream.ignore(std::numeric_limits<std::streamsize>::max(),')');
    shape.push_back(point);
  }

  if (stream.fail())
  {
    throw std::invalid_argument("Wrong value");
  }

  std::string restOfLine;
  getline(stream, restOfLine);
  if ((std::find_if(restOfLine.begin(), restOfLine.end(),
    [](char c) { return !std::isspace(c); })) != restOfLine.end())
  {
    throw std::invalid_argument("Invalid input, extra characters present");
  }

  return shape;
}

void printPoint(std::ostream& out, const point_t& point)
{
  out << " (" << point.x << "; " << point.y << ") ";
}

void printShape(std::ostream& out, const Shape& shape)
{
  out << shape.size() << " ";
  for_each(shape.begin(), shape.end(), [&out] (const point_t point) { printPoint(out, point); });
  out << '\n';
}

int getDistancePoints(const point_t& a, const point_t& b)
{
  return ((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
}

bool isTriangle(const Shape& shape)
{
  return shape.size() == 3;
}

bool isRectangle(const Shape& shape)
{
  if (shape.size() != 4)
  {
    return false;
  }

  bool compareDiag = getDistancePoints(shape[0],shape[2]) == getDistancePoints(shape[1], shape[3]);
  bool compareSideOne = getDistancePoints(shape[0],shape[1]) == getDistancePoints(shape[2], shape[3]);
  bool compareSideTwo = getDistancePoints(shape[1],shape[2]) == getDistancePoints(shape[3], shape[0]);

  return compareDiag && compareSideOne && compareSideTwo;
}

bool isSquare(const Shape& shape)
{
  bool rectangle = isRectangle(shape);
  if (rectangle)
  {
    return (getDistancePoints(shape[0], shape[1]) == getDistancePoints(shape[1], shape[2]));
  }
  return false;
}
