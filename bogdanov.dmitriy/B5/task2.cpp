#include <iostream>
#include <list>
#include <numeric>
#include <algorithm>
#include "shape.hpp"

void task2()
{
  std::list<Shape> figures;
  std::string line;

  while(getline(std::cin, line))
  {
    if (line.empty() || (std::all_of(line.begin(), line.end(), [](char c) { return std::isspace(c); })))
    {
      continue;
    }

    figures.push_back(readShape(line));
  }


  int countVertices = std::accumulate(figures.begin(), figures.end(), 0,
                                      [] (int sum, const Shape& shape) { return sum + shape.size(); });
  std::cout << "Vertices: " << countVertices << std::endl;

  int countTriangles = std::count_if(figures.begin(), figures.end(), isTriangle);
  int countSquare = std::count_if(figures.begin(), figures.end(), isSquare);
  int countRectangle = std::count_if(figures.begin(), figures.end(), isRectangle);

  std::cout << "Triangles: " << countTriangles << std::endl;
  std::cout << "Squares: " << countSquare << std::endl;
  std::cout << "Rectangles: " << countRectangle << std::endl;

  figures.remove_if([] (const Shape& shape) { return shape.size() == 5; });

  std::vector<point_t> verticesFigures;
  std::transform(figures.begin(), figures.end(), std::back_inserter(verticesFigures),
    [] (const Shape& shape) { return shape.front(); });

  std::cout << "Points: ";
  for_each(verticesFigures.begin(), verticesFigures.end(),
    [] (const point_t& point) { printPoint(std::cout, point); });


  auto labelTrianglesEnd = std::partition(figures.begin(), figures.end(), isTriangle);
  auto labelSquareEnd = std::partition(labelTrianglesEnd, figures.end(), isSquare);
  std::partition(labelSquareEnd, figures.end(), isRectangle);

  std::cout << "\nShapes:\n";
  for_each(figures.begin(), figures.end(), [] (const Shape& shape) { printShape(std::cout, shape); });
}
