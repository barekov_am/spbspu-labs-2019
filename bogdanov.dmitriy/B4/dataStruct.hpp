#ifndef B_DATASTRUCT_HPP
#define B_DATASTRUCT_HPP

#include <string>

const int RANGE_UP = 5;
const int RANGE_DOWN = -5;

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

DataStruct getData();
bool isDataValid(const DataStruct &data);
void printData(const DataStruct &data);
bool isLessThen(const DataStruct &first, const DataStruct &second);

#endif //B_DATASTRUCT_HPP
