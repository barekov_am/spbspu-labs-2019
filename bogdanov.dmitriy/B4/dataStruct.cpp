#include "dataStruct.hpp"
#include <sstream>
#include <algorithm>
#include <iostream>

DataStruct getData()
{
  std::string line;
  std::getline(std::cin, line);

  std::stringstream stream(line);
  DataStruct data;

  stream >> data.key1;

  stream.ignore(std::numeric_limits<std::streamsize>::max(), ',');
  stream >> data.key2;

  stream.ignore(std::numeric_limits<std::streamsize>::max(), ',');
  std::getline(stream, data.str);

  if (stream.fail())
  {
    throw std::invalid_argument("Invalid input");
  }

  return data;
}

bool isDataValid(const DataStruct &data)
{
  return ((data.key1 >= RANGE_DOWN) && (data.key1 <= RANGE_UP) && (data.key2 >= RANGE_DOWN) && (data.key2 <= RANGE_UP));
}

void printData(const DataStruct &data)
{
  std::cout << data.key1 << ", " << data.key2 << ", " << data.str << std::endl;
}

bool isLessThen(const DataStruct &first, const DataStruct &second)
{
  if (first.key1 == second.key1)
  {

    if (first.key2 == second.key2)
    {
      return (first.str.length() < second.str.length());
    }

    return (first.key2 < second.key2);
  }

  return (first.key1 < second.key1);
}
