#include <vector>
#include <iostream>
#include <algorithm>
#include "dataStruct.hpp"

int main()
{
  try
  {
    std::vector<DataStruct> vectorData;

    while (std::cin.peek() != EOF)
    {
      if (std::cin.fail())
      {
        std::cerr << "Wrong input";
        return 1;
      }

      vectorData.push_back(getData());
    }

    if (vectorData.empty())
    {
      return 0;
    }

    if (std::none_of(std::begin(vectorData), std::end(vectorData), isDataValid))
    {
      std::cerr << "Data value not in range!";
      return 1;
    }

    std::sort(vectorData.begin(), vectorData.end(), isLessThen);

    std::for_each(vectorData.begin(), vectorData.end(), printData);
  }

  catch (const std::exception &e)
  {
    std::cerr << e.what();
    return 1;
  }

  return 0;
}
