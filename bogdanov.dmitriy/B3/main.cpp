#include <iostream>

void task1();
void task2();

int main(int argc, char* argv[])
{

  try {

    if ((argc != 1) && (argc != 2)) {
      std::cerr << "invalid amount of arguments\n";
      return 1;
    }

    const int task = std::stoi(argv[1]);

    switch (task) {
    case 1:
    {
      task1();
      break;
    }

    case 2:
    {
      task2();
      break;
    }

    default:
    {
      std::cerr << "invalid argument\n";
      return 1;
    }
    }

  } catch (const std::exception e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }

  return 0;
}
