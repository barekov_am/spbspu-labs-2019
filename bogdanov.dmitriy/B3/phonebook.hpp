#ifndef B_PHONEBOOK_HPP
#define B_PHONEBOOK_HPP

#include <list>
#include <string>

class Phonebook
{
public:
  struct record_t {
    std::string name;
    std::string number;
  };

  using record_list = std::list<record_t>;

  using valueType = record_list::value_type;

  using iterator = record_list::iterator;
  using const_iterator = record_list::const_iterator;

  iterator begin() noexcept;
  const_iterator cbegin() const noexcept;

  iterator end() noexcept;
  const_iterator cend() const noexcept;

  iterator insert(iterator pos, const valueType& value);
  iterator erase(iterator pos);

  void push_bask(const valueType& value);

  bool empty() const noexcept;
  std::size_t size() const noexcept;

private:
  record_list list_;
};

#endif //B_PHONEBOOK_HPP
