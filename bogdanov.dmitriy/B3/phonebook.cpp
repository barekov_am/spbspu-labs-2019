#include "phonebook.hpp"

Phonebook::iterator Phonebook::begin() noexcept
{
  return list_.begin();
}

Phonebook::const_iterator Phonebook::cbegin() const noexcept
{
  return list_.cbegin();
}

Phonebook::iterator Phonebook::end() noexcept
{
  return list_.end();
}

Phonebook::const_iterator Phonebook::cend() const noexcept
{
  return list_.cend();
}

void Phonebook::push_bask(const valueType &value)
{
  return list_.push_back(value);
}

Phonebook::iterator Phonebook::insert(iterator pos, const valueType &value)
{
  return list_.insert(pos, value);
}

Phonebook::iterator Phonebook::erase(iterator pos)
{
  return list_.erase(pos);
}

bool Phonebook::empty() const noexcept
{
  return list_.empty();
}

std::size_t Phonebook::size() const noexcept
{
  return list_.size();
}
