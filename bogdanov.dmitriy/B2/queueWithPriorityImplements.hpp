#ifndef QUEUE_WITH_PRIORITY_IMPLEMENTS_HPP
#define QUEUE_WITH_PRIORITY_IMPLEMENTS_HPP

#include "queueWithPriority.hpp"

template<typename T>
QueueWithPriority<T>::QueueWithPriority() :
  low(),
  normal(),
  high()
{}

template<typename T>
void QueueWithPriority<T>::putElement(const T &element, QueueWithPriority::ElementPriority priority)
{
  switch (priority) {
    case ElementPriority::HIGH: {
      high.push_back(element);
      break;
    }
    case ElementPriority::NORMAL: {
      normal.push_back(element);
      break;
    }
    case ElementPriority::LOW: {
      low.push_back(element);
      break;
    }
  }
}

template<typename T>
bool QueueWithPriority<T>::empty()
{
  return (low.empty() && normal.empty() && high.empty());
}

template<typename T>
T QueueWithPriority<T>::getElement()
{
  if (!high.empty()) {
    T temp = high.front();
    high.pop_front();
    return temp;

  } else if (!normal.empty()) {
    T temp = normal.front();
    normal.pop_front();
    return temp;

  } else {
    T temp = low.front();
    low.pop_front();
    return temp;
  }
}

template<typename T>
void QueueWithPriority<T>::accelerate()
{
  high.splice(high.end(), low);
}


#endif //QUEUE_WITH_PRIORITY_IMPLEMENTS_HPP
