#include <functional>
#include <map>
#include <algorithm>
#include <sstream>
#include "queueWithPriority.hpp"
#include "queueWithPriorityImplements.hpp"

void parseAddArguments(QueueWithPriority<std::string> &queue, std::stringstream &input);
void parseGetArguments(QueueWithPriority<std::string> &queue, std::stringstream &input);
void parseAccelerateArguments(QueueWithPriority<std::string> &queue, std::stringstream &input);

void task1()
{
  using executeCommand = std::function<void(QueueWithPriority<std::string> &, std::stringstream &)>;

  std::map<const std::string, executeCommand> commands =
    {
      {"add", parseAddArguments},
      {"get", parseGetArguments},
      {"accelerate", parseAccelerateArguments}
    };

  QueueWithPriority<std::string> queue;

  std::string lineInput;

  while (std::getline(std::cin, lineInput))
  {
    std::stringstream lineStream(lineInput);
    std::string command_name;
    lineStream >> command_name;

    auto check = [&](const std::pair<const std::string, executeCommand> &pair)
    {
      return (pair.first == command_name);
    };

    auto command = std::find_if(commands.begin(), commands.end(), check);

    if (command != std::end(commands))
    {
      command->second(queue, lineStream);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }

  }

}
