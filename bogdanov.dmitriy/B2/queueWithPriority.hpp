#ifndef QUEUE_WITH_PRIORITY_HPP
#define QUEUE_WITH_PRIORITY_HPP

#include <iostream>
#include <list>

template <typename T>
class QueueWithPriority
{
public:

  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH,
  };

  QueueWithPriority();
  QueueWithPriority(const QueueWithPriority &queue) = default;
  QueueWithPriority(QueueWithPriority &&queue) noexcept = default;
  ~QueueWithPriority() = default;
  QueueWithPriority &operator=(const QueueWithPriority<T> &queue) = default;
  QueueWithPriority &operator=(QueueWithPriority<T> &&queue) noexcept = default;

  void putElement(const T & element, ElementPriority priority);
  T getElement();
  void accelerate();
  bool empty();

private:
  std::list <T> low;
  std::list <T> normal;
  std::list <T> high;
};

#endif //QUEUE_WITH_PRIORITY_HPP
