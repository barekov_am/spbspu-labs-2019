#include <iostream>

#include "../common/shape.hpp"
#include "../common/circle.hpp"
#include "../common/rectangle.hpp"
#include "../common/composite-shape.hpp"

void printInfo(const bogdanov::Shape::pointer & shapePointer)
{
  shapePointer->writeInfo();
}

void printCenter(const bogdanov::Shape::pointer & shapePointer)
{
  std::cout << "Center: ( " << shapePointer->getFrameRect().pos.x << " , "
      << shapePointer->getFrameRect().pos.y << " )\n";
}

int main()
{
  bogdanov::point_t pointCenter = {1.0, 1.0};
  bogdanov::Shape::pointer circlePtr = std::make_shared<bogdanov::Circle>(bogdanov::point_t (pointCenter), 10.0);
  bogdanov::Shape::pointer rectanglePtr =
      std::make_shared<bogdanov::Rectangle>(bogdanov::point_t (pointCenter), 20.0, 30.0);
  double scaleFactor = 3.0;
  printInfo(circlePtr);
  printInfo(rectanglePtr);

  std::cout << "Circle: Center(1.0, 1.0) move (2.0,2.0) -> center(3.0,3.0)" << std::endl;
  circlePtr->move(2.0, 2.0);
  std::cout << "Circle radius(10.0) do increase in " << scaleFactor << "-> radius (30.0)" << std::endl;
  circlePtr->scale(scaleFactor);
  printInfo(circlePtr);
  std::cout << "Rectangle: Center(1.0, 1.0), move (4.0,4.0) -> center(5.0,5.0)" << std::endl;
  rectanglePtr->move(4.0, 4.0);
  std::cout << "Rectangle width(20.0) and height(30.0) do increase in " << scaleFactor << "-> width(60.0) and height(90.0)" << std::endl;
  rectanglePtr->scale(scaleFactor);
  printInfo(rectanglePtr);

  scaleFactor = 1 / scaleFactor;
  std::cout << "Rectangle and Circle -> new center(2.0,2.0)" << std::endl;
  pointCenter = {2.0, 2.0};
  circlePtr->move(pointCenter);
  rectanglePtr->move(pointCenter);
  std::cout << "Circle radius(30.0) do increase in " << scaleFactor << "-> radius (10.0)" << std::endl;
  circlePtr->scale(scaleFactor);
  std::cout << "Rectangle width(60.0) and height(90.0) do increase in " << scaleFactor << "-> width(20.0) and height(30.0)" << std::endl;
  rectanglePtr->scale(scaleFactor);
  printInfo(circlePtr);
  printInfo(rectanglePtr);

  scaleFactor = 2;
  bogdanov::CompositeShape compositeShape;
  std::cout << "Create CompositeShape, initial information:\n " << std::endl;
  printInfo(std::make_shared<bogdanov::CompositeShape>(compositeShape));

  std::cout << "Adding shapes to CompositeShape:\n " << std::endl;
  compositeShape.add(rectanglePtr);
  compositeShape.add(circlePtr);
  printInfo(std::make_shared<bogdanov::CompositeShape>(compositeShape));

  std::cout << "Move CompositeShape on (4.0, 4.0)\n " << std::endl;
  compositeShape.move(4.0, 4.0);
  printCenter(std::make_shared<bogdanov::CompositeShape>(compositeShape));

  std::cout << "\nMove CompositeShape in (8.0, 8.0)\n " << std::endl;
  compositeShape.move({8.0, 8.0});
  printCenter(std::make_shared<bogdanov::CompositeShape>(compositeShape));

  std::cout << "\nScale CompositeShape do increase in: " << scaleFactor << std::endl;
  compositeShape.scale(scaleFactor);
  printInfo(std::make_shared<bogdanov::CompositeShape>(compositeShape));

  std::cout << "Deleting a figure by index in to CompositeShape:\n " << std::endl;
  compositeShape.remove(0);
  printInfo(std::make_shared<bogdanov::CompositeShape>(compositeShape));

  return 0;
}
