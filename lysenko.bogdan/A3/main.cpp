#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
  lysenko::Circle circle(5, {0, 4});
  lysenko::Shape * figure = &circle;
  std::cout << "---CIRCLE:---\n";
  figure->print();
  std::cout << "-Moving circle along Ox +5 and Oy +2\n";
  figure->move(5, 2);
  figure->print();
  std::cout << "-Moving circle to the point (8, 6)\n";
  figure->move({-8, 6});
  figure->print();
  std::cout << "-Scaling circle with coefficient 1.8\n";
  figure->scale(1.8);
  figure->print();

  lysenko::Rectangle rectangle({4, 6, {4, 3}});
  figure = &rectangle;
  std::cout << "\n---RECTANGLE:---\n";
  figure-> print();
  std::cout << "-Moving rectangle along Ox +4 and Oy -7\n";
  figure-> move(4, -7);
  figure-> print();
  std::cout << "-Moving rectangle to the point (8, 6)\n";
  figure-> move({8, 6});
  figure-> print();
  std::cout << "-Scaling rectangle with coefficient 2.3\n";
  figure->scale(2.3);
  figure->print();

  lysenko::Triangle triangle({2, 4}, {7, 10}, {15,-6});
  figure = &triangle;
  std::cout << "\n---TRIANGLE:---\n";
  figure->print();
  std::cout << "-Moving traingle along Ox +3 and Oy -7\n";
  figure->move(3, -7);
  figure->print();
  std::cout << "-Moving traingle to the point (12, 2)\n";
  figure->move({12, 2});
  figure->print();
  std::cout << "-Scaling traingle with coefficient 0.6\n";
  figure->scale(0.6);
  figure->print();

  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(rectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(circle);

  lysenko::CompositeShape compositeShape(circlePtr);
  std::cout << "\n---COMPOSITE SHAPE:---\n";
  compositeShape.print();
  std::cout << "-After adding rectangle:\n";
  compositeShape.addShape(rectanglePtr);
  compositeShape.print();
  std::cout << "-Moving to the point (10, 12):\n";
  compositeShape.move({10, 12});
  compositeShape.print();
  std::cout << "-Scaling with coefficient 1.2\n";
  compositeShape.scale(1.2);
  compositeShape.print();
  std::cout << "-Moving along Ox +7 and Oy -9\n";
  compositeShape.move(7, -9);
  compositeShape.print();
  std::cout << "- +30 degree Rotating\n";
  compositeShape.rotate(30.0);
  compositeShape.print();
  std::cout << "-Deleting the first figure:\n";
  compositeShape.deleteShape(0);
  compositeShape.print();

  return 0;
}
