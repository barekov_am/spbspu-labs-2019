#ifndef COMPOSITE_SHAPE
#define COMPOSITE_SHAPE
#include "shape.hpp"

namespace lysenko
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(shapePtr& shape);
    CompositeShape(const CompositeShape& otherShape);
    CompositeShape(CompositeShape&& otherShape);
    CompositeShape& operator =(const CompositeShape &otherShape);
    CompositeShape& operator =(CompositeShape&&);
    Shape &operator [](size_t) const;
    ~CompositeShape() = default;
    size_t getSize() const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(const double dx, const double dy) override;
    void print() const override;
    void addShape(const shapePtr &shape);
    void deleteShape(size_t numShape);
    void scale(const double coefficient) override;
    void rotate(const double) override;
    dynamicArray getList() const;

  private:
    size_t size_;
    dynamicArray shapeArray_;
    double angle_;
  };
}

#endif
