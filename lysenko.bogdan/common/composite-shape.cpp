#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>  
#define _USE_MATH_DEFINES
#include <math.h>

const double FullAngle = 360.0;

lysenko::CompositeShape::CompositeShape() :
  size_(0),
  shapeArray_(nullptr),
  angle_(0.0)
{
}

lysenko::CompositeShape::CompositeShape(shapePtr &shape) :
  size_(1),
  shapeArray_(std::make_unique<shapePtr[]>(size_)),
  angle_(0.0)
{
  shapeArray_[0] = shape;
}

lysenko::CompositeShape::CompositeShape(const CompositeShape &otherShape):
  size_(otherShape.size_),
  shapeArray_(std::make_unique<shapePtr[]>(otherShape.size_)),
  angle_(0.0)
{
  for (size_t i = 0; i < size_; ++i)
  {
    shapeArray_[i] = otherShape.shapeArray_[i];
  }
}

lysenko::CompositeShape::CompositeShape(CompositeShape &&otherShape):
  size_(otherShape.size_),
  shapeArray_(std::move(otherShape.shapeArray_)),
  angle_(otherShape.angle_)
{
  otherShape.size_ = 0;
}

lysenko::CompositeShape &lysenko::CompositeShape::operator=(const CompositeShape &otherShape)
{
  if (this != &otherShape)
  {
    size_ = otherShape.size_;
    angle_ = otherShape.angle_;
    dynamicArray tempArray = std::make_unique<shapePtr[]>(size_);

    for (size_t i = 0; i < size_; i++)
    {
      tempArray[i] = otherShape.shapeArray_[i];
    }
    shapeArray_.swap(tempArray);
  }
  return *this;
}

lysenko::CompositeShape &lysenko::CompositeShape::operator=(CompositeShape  &&otherShape)
{
  if (this != &otherShape)
  {
    size_ = otherShape.size_;
    angle_ = otherShape.size_;
    shapeArray_ = std::move(otherShape.shapeArray_);
    otherShape.size_ = 0;
  }
  return *this;
}

lysenko::Shape& lysenko::CompositeShape::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Error.Index is out of range.");
  }

  return *shapeArray_[index];
}

size_t lysenko::CompositeShape::getSize() const
{
  return size_;
}

double lysenko::CompositeShape::getArea() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Error. Composite shape is empty.");
  }

  double allArea = 0.0;

  for (size_t i = 0; i < size_; ++i)
  {
    allArea += shapeArray_[i]->getArea();
  }
  return allArea;
}

lysenko::rectangle_t lysenko::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Error. Composite shape is empty.");
  }

  rectangle_t tempFrame = shapeArray_[0]->getFrameRect();
  double minX = tempFrame.pos.x - tempFrame.width / 2;
  double minY = tempFrame.pos.y - tempFrame.height / 2;
  double maxX = tempFrame.pos.x + tempFrame.width / 2;
  double maxY = tempFrame.pos.y + tempFrame.height / 2;

  for (size_t i = 0; i < size_; i++)
  {
    tempFrame = shapeArray_[i]->getFrameRect();
    minX = std::min(tempFrame.pos.x - tempFrame.width / 2, minX);
    minY = std::min(tempFrame.pos.y - tempFrame.height / 2, minY);
    maxX = std::max(tempFrame.pos.x + tempFrame.width / 2, maxX);
    maxY = std::max(tempFrame.pos.y + tempFrame.height / 2, maxY);
  }
  return rectangle_t{(maxY - minY), (maxX - minX), {(minX + maxX) / 2, (minY + maxY) / 2}};
}

void lysenko::CompositeShape::move(const point_t &point) 
{
  if (size_ == 0)
  {
    throw std::logic_error("Error. Composite shape is empty.");
  }

  point_t centerCompositeShape = getFrameRect().pos;
  double dx = point.x - centerCompositeShape.x;
  double dy = point.y - centerCompositeShape.y;

  for (size_t i = 0; i < size_; i++)
  {
    shapeArray_[i]->move(dx, dy);
  }
}

void lysenko::CompositeShape::move(const double dx, const double dy)
{
  if (size_ == 0)
  {
    throw std::logic_error("Error. Composite shape is empty.");
  }

  for (size_t i = 0; i < size_; i++)
  {
    shapeArray_[i]->move(dx, dy);
  }
}

void lysenko::CompositeShape::print() const
{
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "Amount of shapes: " << size_ << "\n";
  std::cout << "Width of frame rectangle: " << getFrameRect().width << "\n";
  std::cout << "Height of frame rectangle: " << getFrameRect().height << "\n";
  std::cout << "Center point of frame rectangle: (" << getFrameRect().pos.x
            << "; " << getFrameRect().pos.y << ")" << "\n";
}

void lysenko::CompositeShape::addShape(const shapePtr& shape)
{
  dynamicArray tempShape = std::make_unique<shapePtr[]>(size_ + 1);

  for (size_t i = 0; i < size_; i++)
  {
    tempShape[i] = shapeArray_[i];
  }

  tempShape[size_] = shape;
  size_++;
  shapeArray_.swap(tempShape);
}

void lysenko::CompositeShape::deleteShape(size_t numShape)
{
  if (numShape >= size_)
  {
    throw std::out_of_range("Error. Out of range.");
  }

  size_ --;

  for (size_t i = numShape; i < size_; i++)
  {
    shapeArray_[i] = shapeArray_[i + 1];
  }
}

void lysenko::CompositeShape::scale(const double coefficient)
{
  if (size_ == 0)
  {
    throw std::logic_error("Error. Composite shape is empty.");
  }

  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Error.Incorrect coefficient of scale.");
  }

  point_t centerCompositeShape = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++)
  {
    point_t centerShape = shapeArray_[i]->getFrameRect().pos;
    double x = centerCompositeShape.x + (centerCompositeShape.x - centerShape.x) * coefficient;
    double y = centerCompositeShape.y + (centerCompositeShape.y - centerShape.y) * coefficient;
    shapeArray_[i]->move({x, y});
    shapeArray_[i]->scale(coefficient);
  }
}

void lysenko::CompositeShape::rotate(const double angle)
{
  angle_ += angle;

  if (angle_ < 0.0)
  {
    angle_ = FullAngle + fmod(angle_, FullAngle);
  }
  else
  {
    angle_ = fmod(angle_, FullAngle);
  }

  const double radAngle = angle_ * M_PI / 180;
  const point_t center = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++)
  {
    const double oldX = shapeArray_[i]->getFrameRect().pos.x - center.x;
    const double oldY = shapeArray_[i]->getFrameRect().pos.y - center.y;

    const double newX = oldX * fabs(cos(radAngle)) - oldY * fabs(sin(radAngle));
    const double newY = oldX * fabs(sin(radAngle)) + oldY * fabs(cos(radAngle));

    shapeArray_[i]->move({center.x + newX, center.y + newY});
    shapeArray_[i]->rotate(angle);
  }
}

lysenko::dynamicArray lysenko::CompositeShape::getList() const
{
  dynamicArray tempArray(std::make_unique<shapePtr[]>(size_));

  for (size_t i = 0; i < size_; i++)
  {
    tempArray[i] = shapeArray_[i];
  }

  return tempArray;
}
