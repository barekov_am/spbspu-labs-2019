#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

const double DEVIATION = 0.0001;

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(constParametersAfterMovingByDistance)
{
  lysenko::Rectangle testRectangle({4, 6, {2, 3}});
  const double testRectangleArea = testRectangle.getArea();
  const lysenko::rectangle_t testFrameRect = testRectangle.getFrameRect();

  testRectangle.move(4, 2);

  BOOST_CHECK_CLOSE(testRectangle.getArea(), testRectangleArea, DEVIATION);
  BOOST_CHECK_CLOSE(testRectangle.getFrameRect().height, testFrameRect.height, DEVIATION);
  BOOST_CHECK_CLOSE(testRectangle.getFrameRect().width, testFrameRect.width, DEVIATION);
}

BOOST_AUTO_TEST_CASE(constParametersAfterMovingToPoint)
{
  lysenko::Rectangle testRectangle({4, 2, {2, 3}});
  const double testRectangleArea = testRectangle.getArea();
  const lysenko::rectangle_t testFrameRect = testRectangle.getFrameRect();

  testRectangle.move({1.5, 7});
  BOOST_CHECK_CLOSE(testRectangle.getArea(), testRectangleArea, DEVIATION);
  BOOST_CHECK_CLOSE(testRectangle.getFrameRect().height, testFrameRect.height, DEVIATION);
  BOOST_CHECK_CLOSE(testRectangle.getFrameRect().width, testFrameRect.width, DEVIATION);
}

BOOST_AUTO_TEST_CASE(squareChangeAfterScale)
{
  lysenko::Rectangle testRectangle({1, 4, {2, 7}});
  const double testArea = testRectangle.getArea();
  const double coefficient = 3.5;

  testRectangle.scale(coefficient);

  BOOST_CHECK_CLOSE(testRectangle.getArea(), (testArea * coefficient * coefficient), DEVIATION);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  lysenko::Rectangle testRectangle({4, 2, {5, 5}});
  const double testArea = testRectangle.getArea();
  const lysenko::rectangle_t testFrameRect = testRectangle.getFrameRect();
  const double angle = 90;

  testRectangle.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testRectangle.getFrameRect().width * 2, DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.height, testRectangle.getFrameRect().height / 2, DEVIATION);
  BOOST_CHECK_CLOSE(testArea, testRectangle.getArea(), DEVIATION);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testRectangle.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testRectangle.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  BOOST_CHECK_THROW(lysenko::Rectangle testRectangle({ 1, 0, {2, 4} }), std::invalid_argument);
  BOOST_CHECK_THROW(lysenko::Rectangle testRectangle({ 0, 2, {3, 4.1} }), std::invalid_argument);
  BOOST_CHECK_THROW(lysenko::Rectangle testRectangle({ -4, 7, {2, 4} }), std::invalid_argument);
  BOOST_CHECK_THROW(lysenko::Rectangle testRectangle({ 3, -1, {3, 4.1} }), std::invalid_argument);

  lysenko::Rectangle testRectangle({ 2, 1, {1, 8.2} });

  BOOST_CHECK_THROW(testRectangle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(testRectangle.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
