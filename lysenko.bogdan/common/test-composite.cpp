#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

const double DEVIATION = 0.0001;

BOOST_AUTO_TEST_SUITE(compositeShapeTest)

BOOST_AUTO_TEST_CASE(constantParametersAfterMovingToDistance)
{
  lysenko::Circle testCircle(4, {2, 3});
  lysenko::Rectangle testRectangle({ 4, 6, {2, 3} });
  lysenko::Triangle testTriangle({0, 2}, {7,15}, {12,5});
  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(testRectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(testCircle);
  lysenko::shapePtr trianglePtr = std::make_shared<lysenko::Triangle>(testTriangle);
  
  lysenko::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const double testCompositeArea = testCompositeShape.getArea();
  const lysenko::rectangle_t testFrameRect = testCompositeShape.getFrameRect();

  testCompositeShape.move(1.0, 8.3);
  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea, DEVIATION);
  BOOST_CHECK_CLOSE(testCompositeShape.getFrameRect().width, testFrameRect.width, DEVIATION);
  BOOST_CHECK_CLOSE(testCompositeShape.getFrameRect().height, testFrameRect.height, DEVIATION);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterMovingToPoint)
{
  lysenko::Circle testCircle(4, {2, 3});
  lysenko::Rectangle testRectangle({4, 6, {2, 3}});
  lysenko::Triangle testTriangle({0, 2}, {7,15}, {12,5});
  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(testRectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(testCircle);
  lysenko::shapePtr trianglePtr = std::make_shared<lysenko::Triangle>(testTriangle);
  
  lysenko::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const double testCompositeArea = testCompositeShape.getArea();
  const lysenko::rectangle_t testFrameRect = testCompositeShape.getFrameRect();

  testCompositeShape.move({10, 8});

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea, DEVIATION);
  BOOST_CHECK_CLOSE(testCompositeShape.getFrameRect().width, testFrameRect.width, DEVIATION);
  BOOST_CHECK_CLOSE(testCompositeShape.getFrameRect().height, testFrameRect.height, DEVIATION);
}

BOOST_AUTO_TEST_CASE(squareChangeAreaAfterPositiveScale)
{
  lysenko::Circle testCircle(4, {2, 3});
  lysenko::Rectangle testRectangle({4, 6, {2, 3}});
  lysenko::Triangle testTriangle({0, 2}, {7,15}, {12,5});
  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(testRectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(testCircle);
  lysenko::shapePtr trianglePtr = std::make_shared<lysenko::Triangle>(testTriangle);
  
  lysenko::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const double testArea = testCompositeShape.getArea();
  const double coefficient = 3.5;

  testCompositeShape.scale(coefficient);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testArea * coefficient * coefficient, DEVIATION);
}

BOOST_AUTO_TEST_CASE(squareChangeAreaAfterNegativeScale)
{
  lysenko::Circle testCircle(4, {2, 3});
  lysenko::Rectangle testRectangle({4, 6, {2, 3}});
  lysenko::Triangle testTriangle({0, 2}, {7,15}, {12,5});
  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(testRectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(testCircle);
  lysenko::shapePtr trianglePtr = std::make_shared<lysenko::Triangle>(testTriangle);
  
  lysenko::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const double testArea = testCompositeShape.getArea();
  const double coefficient = 0.5;

  testCompositeShape.scale(coefficient);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testArea * coefficient * coefficient, DEVIATION);
}

BOOST_AUTO_TEST_CASE(parametersAfterAddingAndDeletion)
{
  lysenko::Circle testCircle(4, {2, 3});
  lysenko::Rectangle testRectangle({4, 6, {2, 3}});
  lysenko::Triangle testTriangle({0, 2}, {7,15}, {12,5});
  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(testRectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(testCircle);
  lysenko::shapePtr trianglePtr = std::make_shared<lysenko::Triangle>(testTriangle);
  
  lysenko::CompositeShape testCompositeShape(circlePtr);

  const double testCompositeArea = testCompositeShape.getArea();

  testCompositeShape.addShape(rectanglePtr);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea + testRectangle.getArea(), DEVIATION);

  testCompositeShape.deleteShape(1);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea, DEVIATION);
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  lysenko::Circle testCircle(4, {2, 3});
  lysenko::Rectangle testRectangle({4, 6, {2, 3}});
  lysenko::Triangle testTriangle({0, 2}, {7,15}, {12,5});
  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(testRectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(testCircle);
  lysenko::shapePtr trianglePtr = std::make_shared<lysenko::Triangle>(testTriangle);
  
  lysenko::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  BOOST_CHECK_THROW(testCompositeShape.scale(-3.0), std::invalid_argument);
  BOOST_CHECK_THROW(testCompositeShape.scale(0.0), std::invalid_argument);

  BOOST_CHECK_THROW(testCompositeShape.deleteShape(4), std::out_of_range);
  BOOST_CHECK_THROW(testCompositeShape.deleteShape(-2), std::out_of_range);

  BOOST_CHECK_THROW(testCompositeShape[4], std::out_of_range);
  BOOST_CHECK_THROW(testCompositeShape[-2], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkCopyConstructor)
{
  lysenko::Circle testCircle(4, {2, 3});
  lysenko::Rectangle testRectangle({4, 6, {2, 3}});
  lysenko::Triangle testTriangle({0, 2}, {7,15}, {12,5});
  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(testRectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(testCircle);
  lysenko::shapePtr trianglePtr = std::make_shared<lysenko::Triangle>(testTriangle);
  
  lysenko::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const lysenko::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getSize();

  lysenko::CompositeShape copyCompositeShape(testCompositeShape);

  const lysenko::rectangle_t copyFrameRect = copyCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, copyCompositeShape.getArea(), DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.width, copyFrameRect.width, DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.height, copyFrameRect.height, DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, copyFrameRect.pos.x, DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, copyFrameRect.pos.y, DEVIATION);
  BOOST_CHECK_EQUAL(testCompositeSize, copyCompositeShape.getSize());
}

BOOST_AUTO_TEST_CASE(checkMoveConstructor)
{
  lysenko::Circle testCircle(4, {2, 3});
  lysenko::Rectangle testRectangle({4, 6, {2, 3}});
  lysenko::Triangle testTriangle({0, 2}, {7,15}, {12,5});
  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(testRectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(testCircle);
  lysenko::shapePtr trianglePtr = std::make_shared<lysenko::Triangle>(testTriangle);
  
  lysenko::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const lysenko::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getSize();

  lysenko::CompositeShape moveCompositeShape(std::move(testCompositeShape));

  const lysenko::rectangle_t moveFrameRect = moveCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, moveCompositeShape.getArea(), DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.width, moveFrameRect.width, DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.height, moveFrameRect.height, DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, moveFrameRect.pos.x, DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, moveFrameRect.pos.y, DEVIATION);
  BOOST_CHECK_EQUAL(testCompositeSize, moveCompositeShape.getSize());
}

BOOST_AUTO_TEST_CASE(checkCopyOperator)
{
  lysenko::Circle testCircle(4, {2, 3});
  lysenko::Rectangle testRectangle({4, 6, {2, 3}});
  lysenko::Triangle testTriangle({0, 2}, {7,15}, {12,5});
  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(testRectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(testCircle);
  lysenko::shapePtr trianglePtr = std::make_shared<lysenko::Triangle>(testTriangle);
  
  lysenko::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const lysenko::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getSize();

  lysenko::Circle testCircleNew(3.1, {3.1, 4.7});
  lysenko::shapePtr circlePtrNew = std::make_shared<lysenko::Circle>(testCircleNew);

  lysenko::CompositeShape copyCompositeShape(circlePtrNew);

  copyCompositeShape = testCompositeShape;

  const lysenko::rectangle_t copyFrameRect = copyCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, copyCompositeShape.getArea(), DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.width, copyFrameRect.width, DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.height, copyFrameRect.height, DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, copyFrameRect.pos.x, DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, copyFrameRect.pos.y, DEVIATION);
  BOOST_CHECK_EQUAL(testCompositeSize, copyCompositeShape.getSize());
}

BOOST_AUTO_TEST_CASE(checkMoveOperator)
{
  lysenko::Circle testCircle(4, { 2, 3 });
  lysenko::Rectangle testRectangle({ 4, 6, {2, 3} });
  lysenko::Triangle testTriangle({ 0, 2 }, { 7,15 }, { 12,5 });
  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(testRectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(testCircle);
  lysenko::shapePtr trianglePtr = std::make_shared<lysenko::Triangle>(testTriangle);
  
  lysenko::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const lysenko::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getSize();

  lysenko::CompositeShape moveCompositeShape;

  moveCompositeShape = std::move(testCompositeShape);

  const lysenko::rectangle_t moveFrameRect = moveCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, moveCompositeShape.getArea(), DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.width, moveFrameRect.width, DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.height, moveFrameRect.height, DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, moveFrameRect.pos.x, DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, moveFrameRect.pos.y, DEVIATION);
  BOOST_CHECK_EQUAL(testCompositeSize, moveCompositeShape.getSize());
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  lysenko::Circle testCircle(1, { 5, 5 });
  lysenko::Rectangle testRectangle({ 4, 2, {5, 5} });
  lysenko::Triangle testTriangle({ 0, 2 }, { 7,15 }, { 12,5 });
  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(testRectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(testCircle);
  lysenko::shapePtr trianglePtr = std::make_shared<lysenko::Triangle>(testTriangle);
  
  lysenko::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.addShape(circlePtr);

  const double testArea = testCompositeShape.getArea();
  const lysenko::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double angle = 90;

  testCompositeShape.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testCompositeShape.getFrameRect().width / 2, DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCompositeShape.getFrameRect().height * 2, DEVIATION);
  BOOST_CHECK_CLOSE(testArea, testCompositeShape.getArea(), DEVIATION);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testCompositeShape.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testCompositeShape.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
