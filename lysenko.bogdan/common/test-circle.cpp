#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double DEVIATION = 0.0001;

BOOST_AUTO_TEST_SUITE(circleTest)

BOOST_AUTO_TEST_CASE(constParametersAfterMovingByDistance)
{
  lysenko::Circle testCircle(4, { 2, 3 });
  const double testCircleArea = testCircle.getArea();
  const lysenko::rectangle_t testFrameRect = testCircle.getFrameRect();

  testCircle.move(4, 2);
  BOOST_CHECK_CLOSE(testCircle.getArea(), testCircleArea, DEVIATION);
  BOOST_CHECK_CLOSE(testCircle.getFrameRect().height, testFrameRect.height, DEVIATION);
  BOOST_CHECK_CLOSE(testCircle.getFrameRect().width, testFrameRect.width, DEVIATION);
}

BOOST_AUTO_TEST_CASE(constParametersAfterMovingToPoint)
{
  lysenko::Circle testCircle(4, { 2, 3 });
  const double testCircleArea = testCircle.getArea();
  const lysenko::rectangle_t testFrameRect = testCircle.getFrameRect();

  testCircle.move({ 1.5, 7 });
  BOOST_CHECK_CLOSE(testCircle.getArea(), testCircleArea, DEVIATION);
  BOOST_CHECK_CLOSE(testCircle.getFrameRect().height, testFrameRect.height, DEVIATION);
  BOOST_CHECK_CLOSE(testCircle.getFrameRect().width, testFrameRect.width, DEVIATION);
}

BOOST_AUTO_TEST_CASE(squareChangeAfterScale)
{
  lysenko::Circle testCircle(4, { 2, 7 });
  const double testArea = testCircle.getArea();
  const double coefficient = 3.5;

  testCircle.scale(coefficient);

  BOOST_CHECK_CLOSE(testCircle.getArea(), (testArea * coefficient * coefficient), DEVIATION);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  lysenko::Circle testCircle(2.5, { 5.9, 3.0 });
  const double testArea = testCircle.getArea();
  const lysenko::rectangle_t testFrameRect = testCircle.getFrameRect();
  const double angle = 90;

  testCircle.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testCircle.getFrameRect().width, DEVIATION);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCircle.getFrameRect().height, DEVIATION);
  BOOST_CHECK_CLOSE(testArea, testCircle.getArea(), DEVIATION);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testCircle.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testCircle.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  BOOST_CHECK_THROW(lysenko::Circle testCircle(0, { 2, 4 }), std::invalid_argument);
  BOOST_CHECK_THROW(lysenko::Circle testCircle(-4, { 3, 4.1 }), std::invalid_argument);

  lysenko::Circle testCircle(6, { 1, 8.2 });

  BOOST_CHECK_THROW(testCircle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(testCircle.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
