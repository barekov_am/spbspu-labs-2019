#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"

const double DEVIATION = 0.0001;

BOOST_AUTO_TEST_SUITE(triangleTest)

BOOST_AUTO_TEST_CASE(constParametersAfterMovingByDistance)
{
  lysenko::Triangle testTriangle({0, 2}, {7,15}, {12,5});
  const double testTriangleArea = testTriangle.getArea();
  const lysenko::rectangle_t testFrameRect = testTriangle.getFrameRect();

  testTriangle.move(4, 2);
  BOOST_CHECK_CLOSE(testTriangle.getArea(), testTriangleArea, DEVIATION);
  BOOST_CHECK_CLOSE(testTriangle.getFrameRect().height, testFrameRect.height, DEVIATION);
  BOOST_CHECK_CLOSE(testTriangle.getFrameRect().width, testFrameRect.width, DEVIATION);
}

BOOST_AUTO_TEST_CASE(constParametersAfterMovingToPoint)
{
  lysenko::Triangle testTriangle({-4, -2}, {5,5}, {3,1});
  const double testTriangleArea = testTriangle.getArea();
  const lysenko::rectangle_t testFrameRect = testTriangle.getFrameRect();

  testTriangle.move({ 1.5, 7 });
  BOOST_CHECK_CLOSE(testTriangle.getArea(), testTriangleArea, DEVIATION);
  BOOST_CHECK_CLOSE(testTriangle.getFrameRect().height, testFrameRect.height, DEVIATION);
  BOOST_CHECK_CLOSE(testTriangle.getFrameRect().width, testFrameRect.width, DEVIATION);
}

BOOST_AUTO_TEST_CASE(squareChangeAfterScale)
{
  lysenko::Triangle testTriangle({1, 4}, {15,7}, {12,-4});
  const double testTriangleArea = testTriangle.getArea();
  const double coefficient = 3.5;

  testTriangle.scale(coefficient);

  BOOST_CHECK_CLOSE(testTriangle.getArea(), (testTriangleArea * coefficient * coefficient), DEVIATION);
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  BOOST_CHECK_THROW(lysenko::Triangle testTriangle({ 1, 1 }, { 2,2 }, {5,5 }), std::invalid_argument);

  lysenko::Triangle testTriangle({ 1, 4 }, { 15,7 }, { 12,-4 });

  BOOST_CHECK_THROW(testTriangle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(testTriangle.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
