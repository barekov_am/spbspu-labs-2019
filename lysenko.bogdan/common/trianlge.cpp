#include "triangle.hpp"
#include <iostream>
#include <algorithm>
#define _USE_MATH_DEFINES
#include <math.h>

static lysenko::point_t rotatePoint(lysenko::point_t& point, const lysenko::point_t& center, double angle)
{
  double angle_rad = angle * M_PI / 180;
  double temp_x = (point.x - center.x) * cos(angle_rad) - (point.y - center.y) * sin(angle_rad) + center.x;
  double temp_y = (point.x - center.x) * sin(angle_rad) + (point.y - center.y) * cos(angle_rad) + center.y;
  return { temp_x, temp_y };
}

lysenko::Triangle::Triangle(const point_t& pointA, const point_t& pointB, const point_t& pointC) :
  pointA_(pointA),
  pointB_(pointB),
  pointC_(pointC),
  center_({(pointA.x + pointB.x + pointC.x) / 3 , (pointA.y + pointB.y + pointC.y) / 3})
{
  if (getArea() == 0)
  {
    throw std::invalid_argument("Error. Incorect inputs data.");
  }
}

double lysenko::Triangle::getArea() const
{
  return ((std::abs(((pointA_.x - pointC_.x) * (pointB_.y - pointC_.y)) - ((pointA_.y
    - pointC_.y) * (pointB_.x - pointC_.x))) / 2));
}

lysenko::rectangle_t lysenko::Triangle::getFrameRect() const
{
  point_t min = {std::min(std::min(pointA_.x, pointB_.x), pointC_.x),
    std::min(std::min(pointA_.y, pointB_.y), pointC_.y)};
  point_t max = {std::max(std::max(pointA_.x, pointB_.x), pointC_.x),
    std::max(std::max(pointA_.y, pointB_.y), pointC_.y)};
  return rectangle_t{max.x - min.x, max.y - min.y, {(max.x + min.x) / 2, (max.y + min.y) / 2 }};
}

void lysenko::Triangle::move(const point_t & position_)
{
  const double dx = position_.x - center_.x;
  const double dy = position_.y - center_.y;

  move(dx, dy);
}

void lysenko::Triangle::move(double dx, double dy)
{
  center_.x += dx;
  pointA_.x += dx;
  pointB_.x += dx;
  pointC_.x += dx;
  center_.y += dy;
  pointA_.y += dy;
  pointB_.y += dy;
  pointC_.y += dy;
}

void lysenko::Triangle::print() const
{
  std::cout << "Area of triangle: " << getArea() << "\n";
  std::cout << "Width of frame rectangle: " << getFrameRect().width << "\n";
  std::cout << "Height of frame rectangle: " << getFrameRect().height << "\n";
  std::cout << "Center point of frame rectangle: (" << getFrameRect().pos.x
            << "; " << getFrameRect().pos.y << ")" << "\n";
}

void lysenko::Triangle::scale(const double coefficient)
{
  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Error.Incorrect coefficient of scale.");
  }
  pointA_.x = center_.x + (pointA_.x - center_.x) * coefficient;
  pointA_.y = center_.y + (pointA_.y - center_.y) * coefficient;
  pointB_.x = center_.x + (pointB_.x - center_.x) * coefficient;
  pointB_.y = center_.y + (pointB_.y - center_.y) * coefficient;
  pointC_.x = center_.x + (pointC_.x - center_.x) * coefficient;
  pointC_.y = center_.y + (pointC_.y - center_.y) * coefficient;
}

void lysenko::Triangle::rotate(const double angle)
{
  pointA_ = rotatePoint(pointA_, center_, angle);
  pointB_ = rotatePoint(pointB_, center_, angle);
  pointC_ = rotatePoint(pointC_, center_, angle);
}

