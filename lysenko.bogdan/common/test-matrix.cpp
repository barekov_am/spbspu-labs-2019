#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "split.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithCrossing)
{
  const size_t layersValue = 2;
  const size_t columnsValue = 1;

  lysenko::Rectangle testRectangle({ 4.0, 2.0, {5.0, 5.0} });
  lysenko::Circle testCircle(2.0, {5.0, 5.0});
  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(testRectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(testCircle);

  lysenko::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.addShape(circlePtr);

  lysenko::Matrix testMatrix = lysenko::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithoutCrossing)
{
  const size_t layersValue = 1;
  const size_t columnsValue = 2;

  lysenko::Rectangle testRectangle({ 4.0, 2.0, {5.0, 5.0} });
  lysenko::Circle testCircle(2.0, {10.0, 5.0});
  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(testRectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(testCircle);

  lysenko::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.addShape(circlePtr);

  lysenko::Matrix testMatrix = lysenko::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  lysenko::Rectangle testRectangle({ 4.0, 2.0, {5.0, 5.0} });
  lysenko::Circle testCircle(2.0, {5.0, 5.0});
  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(testRectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(testCircle);

  lysenko::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.addShape(circlePtr);

  lysenko::Matrix testMatrix = lysenko::part(testCompositeShape);

  BOOST_CHECK_THROW(testMatrix[5][0], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-5][0], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkCopyAndMove)
{
  lysenko::Rectangle testRectangle({ 4.0, 2.0, {5.0, 5.0} });
  lysenko::Circle testCircle(2.0, {5.0, 5.0});
  lysenko::shapePtr rectanglePtr = std::make_shared<lysenko::Rectangle>(testRectangle);
  lysenko::shapePtr circlePtr = std::make_shared<lysenko::Circle>(testCircle);

  lysenko::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.addShape(circlePtr);

  lysenko::Matrix testMatrix = lysenko::part(testCompositeShape);
  lysenko::Matrix copyMatrix(testMatrix);
  lysenko::Matrix moveMatrix(std::move(testMatrix));

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyMatrix);
}

BOOST_AUTO_TEST_SUITE_END()
