#include "rectangle.hpp"
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>

lysenko::Rectangle::Rectangle(const rectangle_t & rect):
  angle_(0.0),
  rect_(rect)
{
  if (rect_.height <= 0.0)
  {
    throw std::invalid_argument("Error. Incorrect height of rectangle.");
  }
  if (rect_.width <= 0.0)
  {
    throw std::invalid_argument("Error. Incorrect width of rectangle.");
  }
}

double lysenko::Rectangle::getArea() const
{
  return (rect_.height * rect_.width);
}

lysenko::rectangle_t lysenko::Rectangle::getFrameRect() const
{
  const double radAngle = angle_ * M_PI / 180;
  double width = rect_.width * fabs(cos(radAngle)) + rect_.height * fabs(sin(radAngle));
  double height = rect_.width * fabs(sin(radAngle)) + rect_.height * fabs(cos(radAngle));
  return {width, height, rect_.pos};
}

void lysenko::Rectangle::move(const point_t & point)
{
  rect_.pos = point;
}

void lysenko::Rectangle::move(const double dx, const double dy)
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

void lysenko::Rectangle::print() const
{
  std::cout << "Area of rectangle: " << getArea() << "\n";
  std::cout << "Width of frame rectangle: " << getFrameRect().width << "\n";
  std::cout << "Height of frame rectangle: " << getFrameRect().height << "\n";
  std::cout << "Center point of frame rectangle: (" << getFrameRect().pos.x
            << "; " << getFrameRect().pos.y << ")" << "\n";
}

void lysenko::Rectangle::scale(const double coefficient)
{
  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Error.Incorrect coefficient of scale.");
  }
  else
  {
    rect_.height *= coefficient;
    rect_.width *= coefficient;
  }
}

void lysenko::Rectangle::rotate(const double angle)
{
  angle_ += angle;
}
