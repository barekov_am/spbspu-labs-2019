#ifndef SHAPE_HPP
#define SHAPE_HPP
#include "base-types.hpp"
#include <memory>

namespace lysenko
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t & point) = 0;
    virtual void move(const double dx, const double dy) = 0;
    virtual void print() const = 0;
    virtual void scale(const double) = 0;
    virtual void rotate(double) = 0;
  };
  using shapePtr = std::shared_ptr<Shape>;
  using dynamicArray = std::unique_ptr<shapePtr[]>;
}

#endif
