#ifndef SPLIT_HPP
#define SPLIT_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace lysenko
{
  bool cross(const rectangle_t&, const rectangle_t&);
  Matrix part(const dynamicArray&, size_t);
  Matrix part(const CompositeShape&);
}

#endif
