#include <iostream>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

void printShape(const gluckhov::Shape *shape)
{
  if (shape == nullptr) {
    throw std::invalid_argument("Shape's pointer is null.");
  }
  shape->printData();
  std::cout << "\nArea: " << shape->getArea();
  gluckhov::rectangle_t frameRect = shape->getFrameRect();
  std::cout << "\n***Frame Rectangle";
  std::cout << "\nWidth: " << frameRect.width  << "\tHeight: " << frameRect.height;
  std::cout << "\nPosition: (" << frameRect.pos.x << ';' << frameRect.pos.y << ')';
}

int main()
{
  gluckhov::Rectangle rect1(5.2, 4.0, {2.4, -3.0});
  printShape(&rect1);
  const double dx = 1.6;
  const double dy = 1.4;
  rect1.move(dx, dy);
  std::cout << "\n===After move: + (" << dx << ';' << dy << ")\n";
  rect1.printData();
  const double coefficientRectangle = 3.0;
  rect1.scale(coefficientRectangle);
  std::cout << "\n===After scale: " << coefficientRectangle << "*********";
  std::cout << "\nArea of rectangle: " << rect1.getArea();

  std::cout << std::endl;

  gluckhov::Circle circ1(4.0, {3.2, 2.4});
  printShape(&circ1);
  const gluckhov::point_t newPosCirc = {4.2, 3.0};
  circ1.move(newPosCirc);
  std::cout << "\n===After move: to (" << newPosCirc.x << ';' << newPosCirc.y << ")\n";
  circ1.printData();
  const double coefficientCircle = 2.0;
  circ1.scale(coefficientCircle);
  std::cout << "\n===After scale: " << coefficientCircle << "*********";
  std::cout << "\nArea of circle: " << circ1.getArea();

  std::cout << std::endl;

  gluckhov::Triangle trian1({1.0, 0.5}, {2.0, 2.0}, {3.0, 0.5});
  printShape(&trian1);
  const gluckhov::point_t newPosTrian = {1.0, 6.7};
  trian1.move(newPosTrian);
  std::cout << "\n===After move: to (" << newPosTrian.x << ';' << newPosTrian.y << ")\n";
  trian1.printData();
  const double coefficientTriangle = 2.0;
  trian1.scale(coefficientTriangle);
  std::cout << "\n*********After scale: " << coefficientTriangle << "*********";
  std::cout << "\nArea of triangle: " << trian1.getArea();

  std::cout << std::endl;

  return 0;
}
