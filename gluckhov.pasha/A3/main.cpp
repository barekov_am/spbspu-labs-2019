#include <iostream>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

void printDataShape(const gluckhov::Shape* shape)
{
  if (shape == nullptr) {
    throw std::invalid_argument("\nShape's pointer is null.");
  }
  shape->printData();
  std::cout << "\nArea: " << shape->getArea() << std::endl;

  gluckhov::rectangle_t frameRect = shape->getFrameRect();

  std::cout << "***Frame Rectangle";
  std::cout << "\nWidth: " << frameRect.width  << "\tHeight: " << frameRect.height;
  std::cout << "\nPosition: (" << frameRect.pos.x << ';' << frameRect.pos.y << ')' << std::endl;
}

int main()
{
  try {
    gluckhov::CompositeShape composite1;
    std::cout << "\nСreated an empty first composition.";
    composite1.printData();

    // start circle demonstration
    gluckhov::Circle circle1(5, {6, 7.2});
    composite1.add(circle1);
    const gluckhov::point_t position = {-1.8, 2.4};
    composite1[1]->move(position);
    std::cout << "\n===After move to (-1.8, 2.4)===";
    composite1[1]->printData();
    const double coefficientScaleCirc = 4.0;
    composite1[1]->scale(coefficientScaleCirc);
    std::cout << "=====After scale x" << coefficientScaleCirc << "=====";
    std::cout << "\nArea: " << composite1[1]->getArea();
    std::cout << std::endl;
    // end circle

    // start rectangle demonstration
    gluckhov::Rectangle rectangle1(7.2, 5.7, {5, -2.4});
    printDataShape(&rectangle1);
    double dx = 2.5, dy = -4.1;
    rectangle1.move(dx, dy);
    std::cout << "\n===After move: + (" << dx << ';' << dy << ")===";
    rectangle1.printData();
    const double coefficientScaleRect = 2.0;
    rectangle1.scale(coefficientScaleRect);
    std::cout << "=====After scale x" << coefficientScaleRect << "=====";
    std::cout << "\nArea: " << rectangle1.getArea();
    std::cout << std::endl;
    // end rectangle

    // start triangle demonstration
    gluckhov::Triangle triangle1({1.0, 0.5}, {2.0, 2.0}, {3.0, 0.5});
    printDataShape(&triangle1);
    const gluckhov::point_t newPosTrian = {1.0, 6.7};
    triangle1.move(newPosTrian);
    std::cout << "\n===After move: to (" << newPosTrian.x << ';' << newPosTrian.y << ")\n";
    triangle1.printData();
    const double coefficientScaleTriangle = 2.0;
    triangle1.scale(coefficientScaleTriangle);
    std::cout << "\n=====After scale: " << coefficientScaleTriangle << "=====";
    std::cout << "\nArea: " << triangle1.getArea();
    std::cout << std::endl;
    // end triangle

    composite1.add(rectangle1);
    composite1.add(triangle1);

    std::cout << "\nAfter add(circle1), add(rectangle1), add(rectangle2), in first composite:";
    printDataShape(&composite1);

    gluckhov::CompositeShape composite2(std::move(composite1));
    std::cout << "\nCreated a new composition that took the values from the first composition.";
    std::cout << "\nQuantity of shapes in the first composition: " << composite1.getAmount();
    std::cout << "\nQuantity of shapes in the second composition: " << composite2.getAmount();

    std::cout << "\nСopy assignment demonstration";
    composite1 = composite2;
    std::cout << "\nQuantity of shapes in the first composition: " << composite1.getAmount();
    std::cout << "\nQuantity of shapes in the second composition: " << composite2.getAmount();

    composite2.remove(2);
    dx = 4.3;
    dy = 10.0;
    composite2.move(dx, dy);
    std::cout << "\nAfter removing the first rectangle and moving the composition +(" << dx << ';' << dy << ')';
    std::cout << "\nAbout second composite";
    printDataShape(&composite2);

    gluckhov::CompositeShape composite3(composite2);
    std::cout << "\nCreated a new composite that copy the values from the second composition.";
    std::cout << "\nQuantity of shapes in the second composition: " << composite2.getAmount();
    std::cout << "\nQuantity of shapes in the third composition: " << composite3.getAmount();

    const double coefficientScale = 2.0;
    composite3.scale(coefficientScale);
    std::cout << "\nАfter scaling the third composition in " << coefficientScale;
    composite3.printData();
    composite3.move({dx, dy});
    std::cout << "\nAfter moving the composition on new position: (" << dx << ';' << dy << ')';
    composite3.printData();
    std::cout << std::endl;
    // end CompositeShape

    return 0;
  } catch (const std::invalid_argument& ex) {
    std::cerr << "\nError invalid-argument: \"" << ex.what() << "\"" << std::endl;

    return 1;
  } catch (const std::out_of_range& ex) {
    std::cerr << "\nError in index out of range: \"" << ex.what() << "\"" << std::endl;

    return 1;
  } catch (const std::logic_error& ex) {
    std::cerr << "\nError in logic of programm: \"" << "\"" << std::endl;

    return 1;
  } catch (const std::bad_alloc& ex) {
    std::cerr << "\nIncorrect use of memory: \"" << ex.what() << "\"" << std::endl;

    return 2;
  } catch (const std::exception& ex) {
    std::cerr << "\nSystem error:  \"" << ex.what() << "\"" << std::endl;

    return 2;
  }
}
