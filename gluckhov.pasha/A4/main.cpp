#include <iostream>
#include <memory>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

void displayMatrix(const gluckhov::Matrix& matrix);

int main()
{
  gluckhov::CompositeShape composite1;
  std::cout << "\nСreated an empty first composition.";
  composite1.printData();

  // start circle demonstration
  gluckhov::Circle circle1(5, {6, 7.2});
  composite1.add(circle1);
  const gluckhov::point_t position = {-1.8, 2.4};
  composite1[1]->move(position);
  std::cout << "\n===After move to (-1.8, 2.4)===";
  composite1[1]->rotate(20);
  composite1[1]->printData();
  const double coefficientScaleCirc = 4.0;
  composite1[1]->scale(coefficientScaleCirc);
  std::cout << "=====After scale x" << coefficientScaleCirc << "=====";
  std::cout << "\nArea: " << composite1[1]->getArea();
  std::cout << std::endl;
  // end circle

  // start rectangle demonstration
  gluckhov::Rectangle rectangle1(7.2, 5.7, {5, -17.2});
  composite1.add(rectangle1);
  double dx = 2.5, dy = -4.1;
  composite1[2]->move(dx, dy);
  std::cout << "\n===After move: + (" << dx << ';' << dy << ")===";
  composite1[2]->rotate(40);
  composite1[2]->printData();
  const double coefficientScaleRect = 2.0;
  composite1[2]->scale(coefficientScaleRect);
  std::cout << "=====After scale x" << coefficientScaleRect << "=====";
  std::cout << "\nArea: " << composite1[2]->getArea();
  std::cout << std::endl;
  // end rectangle
  gluckhov::Rectangle rectangle2(3.0, 5.0, {-1.0, -3.0});
  composite1.add(rectangle2);

  gluckhov::Triangle triangle1({1.0, 0.5}, {2.0, 2.0}, {3.0, 0.5});
  const gluckhov::point_t newPosTrian = {1.0, 6.7};
  triangle1.move(newPosTrian);
  std::cout << "\n===After move: to (" << newPosTrian.x << ';' << newPosTrian.y << ")\n";
  triangle1.rotate(30);
  triangle1.printData();
  const double coefficientScaleTriangle = 2.0;
  triangle1.scale(coefficientScaleTriangle);
  std::cout << "\n=====After scale: " << coefficientScaleTriangle << "=====";
  std::cout << "\nArea: " << triangle1.getArea();
  std::cout << std::endl;
  composite1.add(triangle1);

  std::cout << "\nMatrix:";
  gluckhov::Matrix matrix = gluckhov::part(composite1);
  displayMatrix(matrix);

  return 0;
}

void displayMatrix(const gluckhov::Matrix& matrix)
{
  std::cout << "\n\t";
  for (size_t index = 0; index < matrix.getColumns(); index++) {
    std::cout << index + 1 << "\t";
  }
  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    std::cout << "\n" << i + 1 << "\t";
    for (size_t j = 0; j < matrix.getColumns(); j++)
    {
      if (matrix[i][j] != nullptr)
      {
        std::cout << "*       ";
      } else {
        std::cout << "\t";
      }
    }
  }
  std::cout << "\n";
}
