#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"

const double ACCURACY = 0.001;
const double INCORRECT_ARGUMENT = -5.0;

BOOST_AUTO_TEST_SUITE(testTriangle)

  BOOST_AUTO_TEST_CASE(testCorrectnessAfterMove)
  {
    gluckhov::Triangle triangle({1.0, 0.5}, {2.0, 2.0}, {3.0, 0.5});

    const int qtyVertices = 3;
    double sidesBeforeMove[qtyVertices];
    for (int index = 0; index < qtyVertices; index++)
    {
      sidesBeforeMove[index] = triangle.getSide(index);
    }

    const double areaBeforeMove = triangle.getArea();
    triangle.move(5.4, 3.2);
    const double areaAfterMove = triangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ACCURACY);


    for (int index = 0; index < qtyVertices; index++)
    {
      BOOST_CHECK_CLOSE(sidesBeforeMove[index], triangle.getSide(index), ACCURACY);
    }
  } // testCorrectnessAfterMove

  BOOST_AUTO_TEST_CASE(testCorrectnessAfterScale)
  {
    gluckhov::Triangle triangle({1.0, 0.5}, {2.0, 2.0}, {3.0, 0.5});

    const double areaBeforeScale = triangle.getArea();
    const double coefficientScale = 2.0;
    triangle.scale(coefficientScale);
    const double areaAfterScale = triangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale * coefficientScale * coefficientScale, areaAfterScale, ACCURACY);
  } // testCorrectnessAfterScale

  BOOST_AUTO_TEST_CASE(testIncorrectParameters)
  {
    const gluckhov::point_t point = {0.0, 0.0};
    BOOST_CHECK_THROW(gluckhov::Triangle(point, point, {3.0, 0.5}), std::invalid_argument);
    BOOST_CHECK_THROW(gluckhov::Triangle(point, point, point), std::invalid_argument);

    gluckhov::Triangle triangle({1.0, 0.5}, {2.0, 2.0}, {3.0, 0.5});
    BOOST_CHECK_THROW(triangle.scale(INCORRECT_ARGUMENT), std::invalid_argument);
    BOOST_CHECK_THROW(triangle.getSide(INCORRECT_ARGUMENT), std::out_of_range);
  } // testIncorrectParameters

BOOST_AUTO_TEST_SUITE_END() // testTriangle
