#define _USE_MATH_DEFINES
#include "circle.hpp"
#include <cmath>
#include <stdexcept>
#include <iostream>

gluckhov::Circle::Circle(double radius, const point_t& position):
  pos_(position),
  radius_(radius)
{
  if (radius_ <= 0.0) {
    throw std::invalid_argument("Circle's argument is invalid.");
  }
}

void gluckhov::Circle::printData() const
{
  std::cout << "\nCircle"
            << "\nRadius: " << radius_
            << "\nPosition: (" << pos_.x << ';' << pos_.y << ')';
}

gluckhov::point_t gluckhov::Circle::getPos() const
{
  return pos_;
}

double gluckhov::Circle::getArea() const
{
  return (M_PI * radius_ * radius_);
}

gluckhov::rectangle_t gluckhov::Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, pos_};
}

void gluckhov::Circle::rotate(double)
{}

void gluckhov::Circle::scale(double coefficient)
{
  if (coefficient <= 0.0) {
    throw std::invalid_argument("Circle's argument of scale is invalid.");
  }
  radius_ *= coefficient;
}

void gluckhov::Circle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void gluckhov::Circle::move(const point_t& newPos)
{
  pos_ = newPos;
}

std::shared_ptr<gluckhov::Shape> gluckhov::Circle::clone() const
{
  return std::make_shared<Circle>(*this);
}

double gluckhov::Circle::getRadius() const
{
  return radius_;
}
