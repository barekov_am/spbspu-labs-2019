#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace gluckhov
{
  class CompositeShape: public Shape
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    using shape_array = std::unique_ptr<shape_ptr[]>;

    CompositeShape();

    CompositeShape(const CompositeShape&);
    CompositeShape(CompositeShape&&);

    ~CompositeShape() = default;

    shape_ptr operator [](size_t) const;
    CompositeShape& operator =(const CompositeShape&);
    CompositeShape& operator =(CompositeShape&&);

    void printData() const override;
    double getArea() const override;
    point_t getPos() const override;
    rectangle_t getFrameRect() const override;
    void scale(double) override;
    void rotate(double) override;
    void move(const point_t&) override;
    void move(double, double) override;
    std::shared_ptr<Shape> clone() const override;

    void add(const Shape&);
    void remove(size_t);
    size_t getAmount() const;
    shape_array getList() const;
  private:
    size_t amount_;
    shape_array shapes_;
  };
}
#endif // COMPOSITE_SHAPE_HPP
