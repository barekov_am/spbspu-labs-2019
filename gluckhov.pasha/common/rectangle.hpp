#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace gluckhov
{
  class Rectangle: public Shape
  {
  public:
    Rectangle(double, double, const point_t &);

    void printData() const override;
    point_t getPos() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void rotate(double) override;
    void scale(double) override;
    void move(double, double) override;
    void move(const point_t&) override;
    std::shared_ptr<Shape> clone() const override;
  private:
    point_t pos_;
    double width_;
    double height_;
    double angle_;
  };
}
#endif // RECTANGLE_HPP
