#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

const double ACCURACY = 0.001;
const double INCORRECT_ARGUMENT = -10.0;

BOOST_AUTO_TEST_SUITE(testsRectangle)

  BOOST_AUTO_TEST_CASE(testCorrectnessAfterMove)
  {
    gluckhov::Rectangle rectangle(5.0, 3.0, {4.3, 1.0});
    const gluckhov::rectangle_t rectBeforeMove = rectangle.getFrameRect();
    const double areaBeforeMove = rectangle.getArea();

    rectangle.move(2.4, 5.6);

    const gluckhov::rectangle_t rectAfterMove = rectangle.getFrameRect();
    const double areaAfterMove = rectangle.getArea();

    BOOST_CHECK_CLOSE(rectBeforeMove.width, rectAfterMove.width, ACCURACY);
    BOOST_CHECK_CLOSE(rectBeforeMove.height, rectAfterMove.height, ACCURACY);
    BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ACCURACY);
  } // testCorrectnessAfterMove

  BOOST_AUTO_TEST_CASE(testCorrectnessAfterScale)
  {
    gluckhov::Rectangle rectangle(5.0, 3.0, {4.3, 1.0});
    const double areaBeforeScale = rectangle.getArea();

    const double coefficientScaleRect = 4.0;
    rectangle.scale(coefficientScaleRect);

    const double areaAfterScale = rectangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale * coefficientScaleRect * coefficientScaleRect, areaAfterScale, ACCURACY);
  } // testCorrectnessAfterScale

  BOOST_AUTO_TEST_CASE(testIncorrectParameters)
  {
    BOOST_CHECK_THROW(gluckhov::Rectangle(INCORRECT_ARGUMENT, 3.0, {4.3, 1.0}), std::invalid_argument);
    BOOST_CHECK_THROW(gluckhov::Rectangle(5.0, INCORRECT_ARGUMENT, {4.3, 1.0}), std::invalid_argument);

    gluckhov::Rectangle rectangle(5.0, 3.0, {4.3, 1.0});
    BOOST_CHECK_THROW(rectangle.scale(INCORRECT_ARGUMENT), std::invalid_argument);
  } // testIncorrectParameters

BOOST_AUTO_TEST_SUITE_END() // testsRectangle
