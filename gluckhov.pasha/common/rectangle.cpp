#define _USE_MATH_DEFINES
#include "rectangle.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

gluckhov::Rectangle::Rectangle(double width, double height, const point_t &position):
  pos_(position),
  width_(width),
  height_(height),
  angle_(0.0)
{
  if ((width <= 0.0) || (height <= 0.0)) {
    throw std::invalid_argument("Rectangle's argument is invalid.");
  }
}

void gluckhov::Rectangle::printData() const
{
  std::cout << "\nRectangle"
            << "\nWidth: " << width_ << "\tHeight: " << height_
            << "\nPosition: (" << pos_.x << ';' << pos_.y << ')';
}

gluckhov::point_t gluckhov::Rectangle::getPos() const
{
  return pos_;
}

double gluckhov::Rectangle::getArea() const
{
  return width_ * height_;
}

gluckhov::rectangle_t gluckhov::Rectangle::getFrameRect() const
{
  const double cosin = fabs(cos(angle_ * M_PI / 180));
  const double sinus = fabs(sin(angle_ * M_PI / 180));
  const double width = height_ * sinus + width_ * cosin;
  const double height = height_ * cosin + width_ * sinus;

  return {width, height, pos_};
}

void gluckhov::Rectangle::rotate(double angle)
{
  angle_ = fmod(angle_ + angle, 360.0);
}

void gluckhov::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0.0) {
    throw std::invalid_argument("Rectangle's argument of scale is invalid.");
  }
  width_ *= coefficient;
  height_ *= coefficient;
}

void gluckhov::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void gluckhov::Rectangle::move(const point_t& newPos)
{
  pos_ = newPos;
}

std::shared_ptr<gluckhov::Shape> gluckhov::Rectangle::clone() const
{
  return std::make_shared<Rectangle>(*this);
}
