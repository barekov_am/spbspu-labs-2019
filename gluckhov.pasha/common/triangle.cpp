#define _USE_MATH_DEFINES
#include "triangle.hpp"
#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <cmath>

gluckhov::Triangle::Triangle(const point_t &vertex1, const point_t &vertex2, const point_t &vertex3):
  pos_({(vertex1.x + vertex2.x + vertex3.x) / 3, (vertex1.y + vertex2.y + vertex3.y) / 3}),
  vertices_({vertex1, vertex2, vertex3})
{
  if (getArea() == 0.0) {
    throw std::invalid_argument("The specified triangle's vertices is not valid.");
  }
}

void gluckhov::Triangle::printData() const
{
  std::cout << "\nTriangle";
  for (int index = 0, qty = sizeof(vertices_) / sizeof(point_t); index < qty; index++)
  {
    std::cout << "\nVertex №" << index + 1 << " coordinates: " << vertices_[index].x << "; " << vertices_[index].y;
  }
  std::cout << "\nCenter: " << pos_.x << "; " << pos_.y;
}

gluckhov::point_t gluckhov::Triangle::getPos() const
{
  return pos_;
}

double gluckhov::Triangle::getArea() const
{
  return 0.5 * (fabs(vertices_[0].x - vertices_[2].x) * (vertices_[1].y - vertices_[2].y)
        - fabs(vertices_[1].x - vertices_[2].x) * (vertices_[0].y - vertices_[2].y));
}

gluckhov::rectangle_t gluckhov::Triangle::getFrameRect() const
{
  const double minX = std::min(std::min(vertices_[0].x, vertices_[1].x), vertices_[2].x);
  const double maxX = std::max(std::max(vertices_[0].x, vertices_[1].x), vertices_[2].x);
  const double minY = std::min(std::min(vertices_[0].y, vertices_[1].y), vertices_[2].y);
  const double maxY = std::max(std::max(vertices_[0].y, vertices_[1].y), vertices_[2].y);

  const double width = (maxX - minX);
  const double height = (maxY - minY);
  const point_t pos = {minX + width / 2, minY + height / 2};

  return {width, height, pos};
}

void gluckhov::Triangle::rotate(double angle)
{
  const double sinus = sin(angle * M_PI / 180);
  const double cosine = cos(angle * M_PI / 180);

  for (int index = 0; index < 3; index++) {
    vertices_[index].x = pos_.x + cosine * (vertices_[index].x - pos_.x) - sinus * (vertices_[index].y - pos_.y);
    vertices_[index].y = pos_.y + cosine * (vertices_[index].y - pos_.y) + sinus * (vertices_[index].x - pos_.x);
  }
}

void gluckhov::Triangle::scale(double coefficient)
{
  if (coefficient <= 0.0) {
    throw std::invalid_argument("Triangle's argument of scale is invalid.");
  }
  for (int index = 0, qty = sizeof(vertices_) / sizeof(point_t); index < qty; index++) {
    vertices_[index].x = pos_.x + (vertices_[index].x - pos_.x) * coefficient;
    vertices_[index].y = pos_.y + (vertices_[index].y - pos_.y) * coefficient;
  }
}

void gluckhov::Triangle::move(const point_t &newPos)
{
  const point_t offset = {newPos.x - pos_.x, newPos.y - pos_.y};

  move(offset.x, offset.y);
}

void gluckhov::Triangle::move(double dx, double dy)
{
  for (int index = 0, qty = sizeof(vertices_) / sizeof(point_t); index < qty; index++) {
    vertices_[index].x += dx;
    vertices_[index].y += dy;
  }
  pos_.x += dx;
  pos_.y += dy;
}

std::shared_ptr<gluckhov::Shape> gluckhov::Triangle::clone() const
{
  return std::make_shared<Triangle>(*this);
}

double gluckhov::Triangle::getSide(int index) const
{
  if ((index < 0) || (index >= 3)) {
    throw std::out_of_range("Index out of range.");
  }
  return fabs(std::sqrt(std::pow(vertices_[(index + 1) % 3].x - vertices_[index].x, 2)
  + std::pow(vertices_[(index + 1) % 3].y - vertices_[index].y, 2)));
}
