#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"

namespace gluckhov
{
  class Matrix
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    using shape_array = std::unique_ptr<shape_ptr[]>;

    Matrix();
    Matrix(const Matrix&);
    Matrix(Matrix&&);
    ~Matrix() = default;

    Matrix& operator =(const Matrix&);
    Matrix& operator =(Matrix&&);

    shape_array operator[](size_t) const;

    void add(shape_ptr, size_t, size_t);
    size_t getRows() const;
    size_t getColumns() const;

  private:
    size_t rows_;
    size_t columns_;
    shape_array shapes_;
  };
}

#endif // MATRIX_HPP
