#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double ACCURACY = 0.001;
const unsigned int ZERO_QUANTITY = 0;
const double INCORRECT_ARGUMENT = -10.0;

BOOST_AUTO_TEST_SUITE(testsCompositeShape)

  BOOST_AUTO_TEST_CASE(testDefaultConstructor)
  {
    gluckhov::CompositeShape emptyComposite;
    BOOST_CHECK_EQUAL(emptyComposite.getAmount(), ZERO_QUANTITY);
  } // testDefaultConstructor

  BOOST_AUTO_TEST_CASE(testMoveAndCopyConstructors)
  {
    gluckhov::Circle circle(2.0, {-1.5, 2.0});
    gluckhov::CompositeShape composite1;
    composite1.add(circle);
    unsigned int qtyBefore = composite1.getAmount();

    gluckhov::CompositeShape composite2(composite1);
    BOOST_CHECK_EQUAL(composite1.getAmount(), qtyBefore);
    BOOST_CHECK_EQUAL(composite1.getAmount(), composite2.getAmount());

    qtyBefore = composite2.getAmount();
    gluckhov::CompositeShape composite3(std::move(composite2));
    BOOST_CHECK_EQUAL(composite2.getAmount(), ZERO_QUANTITY);
    BOOST_CHECK_EQUAL(composite3.getAmount(), qtyBefore);
  } // testMoveAndCopyConstructors

  BOOST_AUTO_TEST_CASE(testAssigmentOperators)
  {
    gluckhov::CompositeShape composite1;
    gluckhov::Circle circle(2.0, {-1.5, 2.0});
    composite1.add(circle);
    const int qtyBeforeCopy = composite1.getAmount();
    gluckhov::CompositeShape composite2;

    composite2 = composite1;

    BOOST_CHECK_EQUAL(composite2.getAmount(), qtyBeforeCopy);
    BOOST_CHECK_EQUAL(composite1.getAmount(), composite2.getAmount());

    const int qtyBeforeRemove = composite1.getAmount();
    composite2 = std::move(composite1);
    BOOST_CHECK_EQUAL(composite1.getAmount(), ZERO_QUANTITY);
    BOOST_CHECK_EQUAL(composite2.getAmount(), qtyBeforeRemove);
  } // testAssigmentOperators

  BOOST_AUTO_TEST_CASE(testCorrectnessMove)
  {
    gluckhov::CompositeShape composite;
    gluckhov::Circle circle(4.0, {2.3, 4.0});
    gluckhov::Rectangle rectangle(3.0, 4.0, {2.5, 3.5});
    composite.add(circle);
    composite.add(rectangle);
    const double areaBeforeMove = composite.getArea();
    const gluckhov::rectangle_t frameBeforeMove = composite.getFrameRect();

    composite.move(2.4, 5.6);

    const gluckhov::rectangle_t frameAfterMoveOn = composite.getFrameRect();
    BOOST_CHECK_CLOSE(areaBeforeMove, composite.getArea(), ACCURACY);
    BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMoveOn.width, ACCURACY);
    BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMoveOn.height, ACCURACY);

    composite.move({2.4, 5.6});

    const gluckhov::rectangle_t frameAfterMoveTo = composite.getFrameRect();
    BOOST_CHECK_CLOSE(areaBeforeMove, composite.getArea(), ACCURACY);
    BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMoveTo.width, ACCURACY);
    BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMoveTo.height, ACCURACY);
  } // testCorrectnessMove

  BOOST_AUTO_TEST_CASE(testCorrectnessScale)
  {
    gluckhov::CompositeShape composite;
    gluckhov::Circle circle(4.0, {2.3, 4.0});
    gluckhov::Rectangle rectangle(3.0, 4.0, {2.5, 3.5});
    composite.add(circle);
    composite.add(rectangle);

    const double areaBeforeScale = composite.getArea();
    const double coefficientScale = 3.0;
    composite.scale(coefficientScale);
    const double areaAfterScale = composite.getArea();

    BOOST_CHECK_CLOSE(areaBeforeScale * coefficientScale * coefficientScale, areaAfterScale, ACCURACY);
  } // testCorrectnessScale

  BOOST_AUTO_TEST_CASE(testsCorrectnessThrowMethods)
  {
    gluckhov::CompositeShape composite;
    gluckhov::Circle circle(4.0, {2.3, 4.0});
    composite.add(circle);
    BOOST_CHECK_THROW(composite[2]->getArea(), std::out_of_range);
    BOOST_CHECK_THROW(composite.remove(2), std::out_of_range);
    BOOST_CHECK_THROW(composite.scale(INCORRECT_ARGUMENT), std::invalid_argument);
  } // testsCorrectnessThrowMethods

BOOST_AUTO_TEST_SUITE_END() // testsCompositeShape
