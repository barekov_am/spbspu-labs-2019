#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <algorithm>

gluckhov::CompositeShape::CompositeShape():
  amount_(0)
{}

gluckhov::CompositeShape::CompositeShape(const CompositeShape& rhs):
  amount_(rhs.amount_),
  shapes_(std::make_unique<shape_ptr[]>(amount_))
{
  for (size_t index = 0; index < amount_; index++) {
    shapes_[index] = rhs.shapes_[index];
  }
}

gluckhov::CompositeShape::CompositeShape(CompositeShape&& rhs):
  amount_(rhs.amount_),
  shapes_(std::move(rhs.shapes_))
{
  rhs.amount_ = 0;
}

gluckhov::CompositeShape::shape_ptr gluckhov::CompositeShape::operator [](size_t index) const
{
  if (index <= 0 || index > amount_) {
    throw std::out_of_range("Item Access Index is invalid.");
  }
  return shapes_[index - 1];
}

gluckhov::CompositeShape& gluckhov::CompositeShape::operator =(const CompositeShape& rhs)
{
  if (&rhs != this) {
    amount_ = rhs.amount_;
    shapes_ = std::make_unique<shape_ptr[]>(amount_);
    for (size_t index = 0; index < amount_; index++) {
      shapes_[index] = rhs.shapes_[index];
    }
  }
  return *this;
}

gluckhov::CompositeShape& gluckhov::CompositeShape::operator =(CompositeShape&& rhs)
{
  if (&rhs != this) {
    amount_ = rhs.amount_;
    shapes_ = std::move(rhs.shapes_);

    rhs.amount_ = 0;
  }
  return *this;
}

void gluckhov::CompositeShape::printData() const
{
  std::cout << "\nComposite-Shape";
  const point_t pos = getPos();
  std::cout << "\nPosition: (" << pos.x << ';' << pos.y << ')'
            << "\nQuantity of shapes: " << amount_;
  for (size_t index = 0; index < amount_; index++) {
    std::cout << "\n* * * * *"
              << "\nShape - №" << index + 1;
    shapes_[index]->printData();
  }
  std::cout << "\n* * * * *";
}

double gluckhov::CompositeShape::getArea() const
{
  double area = 0.0;
  for (size_t index = 0; index < amount_; index++) {
    area += shapes_[index]->getArea();
  }
  return area;
}

gluckhov::point_t gluckhov::CompositeShape::getPos() const
{
  return getFrameRect().pos;
}

gluckhov::rectangle_t gluckhov::CompositeShape::getFrameRect() const
{
  if (amount_ <= 0) {
    return {0.0, 0.0, {0.0, 0.0}};
  }

  rectangle_t frameShape = shapes_[0]->getFrameRect();

  point_t min = {frameShape.pos.x - frameShape.width / 2, frameShape.pos.y - frameShape.height / 2};
  point_t max = {frameShape.pos.x + frameShape.width / 2, frameShape.pos.y + frameShape.height / 2};

  for (size_t index = 1; index < amount_; index++) {
    frameShape = shapes_[index]->getFrameRect();
    min.x = std::min((frameShape.pos.x - frameShape.width / 2), min.x);
    min.y = std::min((frameShape.pos.y - frameShape.height / 2), min.y);

    max.x = std::max((frameShape.pos.x + frameShape.width / 2), max.x);
    max.y = std::max((frameShape.pos.y + frameShape.height / 2), max.y);
  }
  const double width = max.x - min.x;
  const double height = max.y - min.y;

  return {width, height, {min.x + width / 2, min.y + height / 2}
};
}

void gluckhov::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0.0) {
    throw std::invalid_argument("The Composite Shape scaling coefficient is entered incorrectly.");
  }

  const point_t compositePos = getFrameRect().pos;
  for (size_t index = 0; index < amount_; index++) {
    const point_t shapePos = shapes_[index]->getFrameRect().pos;
    double dx = (coefficient - 1) * (shapePos.x - compositePos.x);
    double dy = (coefficient - 1) * (shapePos.y - compositePos.y);

    shapes_[index]->move(dx, dy);
    shapes_[index]->scale(coefficient);
  }
}

void gluckhov::CompositeShape::rotate(double angle)
{
  const rectangle_t frameRect = getFrameRect();

  const double angleRad = M_PI * angle / 180;
  for (size_t index = 0; index < amount_; index++) {
    point_t delta = shapes_[index]->getPos();
    delta.x -= frameRect.pos.x;
    delta.y -= frameRect.pos.y;
    double dx = delta.x * (cos(angleRad) - 1) - delta.y * sin(angleRad);
    double dy = delta.y * (cos(angleRad) - 1) + delta.x * sin(angleRad);

    shapes_[index]->rotate(angle);
    shapes_[index]->move(dx, dy);
  }
}

void gluckhov::CompositeShape::move(double dx, double dy)
{
  for (size_t index = 0; index < amount_; index++) {
    shapes_[index]->move(dx, dy);
  }
}

void gluckhov::CompositeShape::move(const point_t& newPos)
{
  const point_t posComposite = getPos();
  move(newPos.x - posComposite.x, newPos.y - posComposite.y);
}

std::shared_ptr<gluckhov::Shape> gluckhov::CompositeShape::clone() const
{
  return std::make_shared<CompositeShape>(*this);
}

void gluckhov::CompositeShape::add(const Shape& newShape)
{
  shape_array tmpArr = std::make_unique<shape_ptr[]>(amount_ + 1);
  for (size_t index = 0; index < amount_; index++) {
    tmpArr[index] = shapes_[index];
  }
  shapes_.swap(tmpArr);
  shapes_[amount_++] = newShape.clone();
}

void gluckhov::CompositeShape::remove(size_t indexRem)
{
  if (indexRem <= 0 || indexRem > amount_) {
    throw std::out_of_range("When deleting, the index on the shape is incorrect.");
  }

  for (size_t index = indexRem; index < amount_ - 1; index++) {
    std::swap(shapes_[index], shapes_[index + 1]);
  }
  shapes_[--amount_].reset();
}

size_t gluckhov::CompositeShape::getAmount() const
{
  return amount_;
}

gluckhov::CompositeShape::shape_array gluckhov::CompositeShape::getList() const
{
  shape_array tmpArray(std::make_unique<shape_ptr[]>(amount_));
  for (size_t index = 0; index < amount_; index++) {
    tmpArray[index] = shapes_[index];
  }

  return tmpArray;
}
