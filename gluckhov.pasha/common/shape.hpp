#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace gluckhov
{
  class Shape
  {
  public:
    virtual ~Shape() = default;

    virtual void printData() const = 0;
    virtual point_t getPos() const = 0;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void rotate(double) = 0;
    virtual void scale(double) = 0;
    virtual void move(double, double) = 0;
    virtual void move(const point_t&) = 0;
    virtual std::shared_ptr<Shape> clone() const = 0;
  };
}

#endif // SHAPE_HPP
