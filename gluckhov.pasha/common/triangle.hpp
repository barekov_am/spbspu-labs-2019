#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace gluckhov
{
  class Triangle: public Shape
  {
  public:
    Triangle(const point_t&, const point_t&, const point_t&);

    void printData() const override;
    point_t getPos() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void rotate(double) override;
    void scale(double) override;
    void move(double, double) override;
    void move(const point_t&) override;
    std::shared_ptr<Shape> clone() const override;

    double getSide(int) const;
  private:
    point_t pos_;
    point_t vertices_[3];
  };
}
#endif // TRIANGLE_HPP
