#include <stdexcept>
#include <utility>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include "rectangle.hpp"

using shape_ptr = std::shared_ptr<gluckhov::Shape>;

BOOST_AUTO_TEST_SUITE(matrixTestSuite)

  BOOST_AUTO_TEST_CASE(testCorrectnessMoveAndCopy)
  {
    gluckhov::Circle testCircle(5, {6, 7.2});
    gluckhov::CompositeShape testComposite;
    testComposite.add(testCircle);

    gluckhov::Matrix testMatrix = gluckhov::part(testComposite);

    BOOST_CHECK_NO_THROW(gluckhov::Matrix testMatrix2(testMatrix));
    BOOST_CHECK_NO_THROW(gluckhov::Matrix testMatrix2(std::move(testMatrix)));

    gluckhov::Matrix testMatrix2 = gluckhov::part(testComposite);
    gluckhov::Matrix testMatrix3;

    BOOST_CHECK_NO_THROW(testMatrix3 = testMatrix2);
    BOOST_CHECK_NO_THROW(testMatrix3 = std::move(testMatrix2));
  }

  BOOST_AUTO_TEST_CASE(testsCorrectnessThrowMethods)
  {
    gluckhov::Circle testCircle(5, {6, 7.2});
    gluckhov::CompositeShape testComposite;
    testComposite.add(testCircle);

    gluckhov::Matrix testMatrix = gluckhov::part(testComposite);

    BOOST_CHECK_THROW(testMatrix[2], std::out_of_range);
    BOOST_CHECK_THROW(testMatrix[-1], std::out_of_range);
  }

BOOST_AUTO_TEST_SUITE_END()
