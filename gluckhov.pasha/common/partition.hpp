#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

using shape_ptr = std::shared_ptr<gluckhov::Shape>;
using shape_array = std::unique_ptr<shape_ptr[]>;

namespace gluckhov
{
  bool intersect(const rectangle_t&, const rectangle_t&);
  Matrix part(const shape_array&, size_t);
  Matrix part(const CompositeShape&);
}

#endif // PARTITION_HPP
