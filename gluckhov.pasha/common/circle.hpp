#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace gluckhov
{
  class Circle: public Shape
  {
  public:
    Circle(double, const point_t&);

    void printData() const override;
    point_t getPos() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void rotate(double) override;
    void scale(double) override;
    void move(double, double) override;
    void move(const point_t&) override;
    std::shared_ptr<Shape> clone() const override;

    double getRadius() const;
  private:
    point_t pos_;
    double radius_;
  };
}
#endif // CIRCLE_HPP
