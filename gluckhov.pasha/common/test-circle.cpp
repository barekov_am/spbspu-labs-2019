#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double ACCURACY = 0.001;
const double INCORRECT_ARGUMENT = -10.0;

BOOST_AUTO_TEST_SUITE(testsCircle)

  BOOST_AUTO_TEST_CASE(testCorrectnessAfterMove)
  {
    gluckhov::Circle circle(5.0, {4.3, 1.0});
    const double radiusBeforeMove = circle.getRadius();
    const double areaBeforeMove = circle.getArea();

    circle.move(2.4, 5.6);

    const double radiusAfterMove = circle.getRadius();
    const double areaAfterMove = circle.getArea();

    BOOST_CHECK_CLOSE(radiusBeforeMove, radiusAfterMove, ACCURACY);
    BOOST_CHECK_CLOSE(radiusBeforeMove, radiusAfterMove, ACCURACY);
    BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ACCURACY);
  } // testCorrectnessAfterMove

  BOOST_AUTO_TEST_CASE(testCorrectnessAfterScale)
  {
    gluckhov::Circle circle(5.0, {4.3, 1.0});
    const double areaBeforeScale = circle.getArea();

    const double coefficientScaleCirc = 4.0;
    circle.scale(coefficientScaleCirc);

    const double areaAfterScale = circle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale * coefficientScaleCirc * coefficientScaleCirc, areaAfterScale, ACCURACY);
  } // testCorrectnessAfterScale

  BOOST_AUTO_TEST_CASE(testIncorrectParameters)
  {
    BOOST_CHECK_THROW(gluckhov::Circle(INCORRECT_ARGUMENT, {4.3, 1.0}), std::invalid_argument);

    gluckhov::Circle circle(5.0, {4.3, 1.0});
    BOOST_CHECK_THROW(circle.scale(INCORRECT_ARGUMENT), std::invalid_argument);
  } // testIncorrectParameters

BOOST_AUTO_TEST_SUITE_END() // testsCircle
