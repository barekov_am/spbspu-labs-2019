#ifndef B3_FACTORIALCONTAINER_H
#define B3_FACTORIALCONTAINER_H

#include <iterator>


class FactorialContainer
{

public:
    class FactorialIterator: public std::iterator<std::bidirectional_iterator_tag, unsigned>
    {
    public:
      FactorialIterator();
      FactorialIterator(int index);

      unsigned& operator *();
      unsigned* operator ->();

      FactorialIterator& operator ++();
      FactorialIterator operator ++(int);
      FactorialIterator& operator --();
      FactorialIterator operator --(int);

      bool operator ==(const FactorialIterator&) const;
      bool operator !=(const FactorialIterator&) const;

    private:
      unsigned value_;
      int index_;

      unsigned getValue(int) const;
    };
  FactorialContainer() = default;

  FactorialIterator begin();
  FactorialIterator end();

};

#endif //B3_FACTORIALCONTAINER_H
