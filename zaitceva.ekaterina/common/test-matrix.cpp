#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(testOfCopyConstructor)
{
  zaitceva::Shape::ptr rectangle = std::make_shared<zaitceva::Rectangle>(6, 9, zaitceva::point_t{3, 4});
  zaitceva::Shape::ptr circle = std::make_shared<zaitceva::Circle>(zaitceva::point_t{5, 5}, 4);
  zaitceva::CompositeShape compShape(rectangle);
  compShape.add(circle);
  zaitceva::Matrix matrix = part(compShape);
  zaitceva::Matrix matrix2(matrix);
  BOOST_CHECK(matrix == matrix2);
  BOOST_CHECK_EQUAL(matrix.getRows(), matrix2.getRows());
  BOOST_CHECK_EQUAL(matrix.getColumns(), matrix2.getColumns());
}

BOOST_AUTO_TEST_CASE(testOfMoveConstructor)
{
  zaitceva::Shape::ptr rectangle = std::make_shared<zaitceva::Rectangle>(6, 9, zaitceva::point_t{3, 4});
  zaitceva::Shape::ptr circle = std::make_shared<zaitceva::Circle>(zaitceva::point_t{5, 5}, 4);
  zaitceva::CompositeShape compShape(rectangle);
  compShape.add(circle);
  zaitceva::Matrix matrix = part(compShape);
  zaitceva::Matrix matrix1(matrix);
  zaitceva::Matrix matrix2(std::move(matrix));
  BOOST_CHECK(matrix2 == matrix1);
  BOOST_CHECK_EQUAL(matrix2.getRows(), matrix1.getRows());
  BOOST_CHECK_EQUAL(matrix2.getColumns(), matrix1.getColumns());
}

BOOST_AUTO_TEST_CASE(testOfCopyOperator)
{
  zaitceva::Shape::ptr rectangle = std::make_shared<zaitceva::Rectangle>(6, 9, zaitceva::point_t{3, 4});
  zaitceva::Shape::ptr circle = std::make_shared<zaitceva::Circle>(zaitceva::point_t{5, 5}, 4);
  zaitceva::CompositeShape compShape(rectangle);
  compShape.add(circle);
  zaitceva::Matrix matrix = part(compShape);
  zaitceva::Matrix matrix2 = matrix;
  BOOST_CHECK(matrix == matrix2);
  BOOST_CHECK_EQUAL(matrix.getRows(), matrix2.getRows());
  BOOST_CHECK_EQUAL(matrix.getColumns(), matrix2.getColumns());
}

BOOST_AUTO_TEST_CASE(testOfMoveOperator)
{
  zaitceva::Shape::ptr rectangle = std::make_shared<zaitceva::Rectangle>(6, 9, zaitceva::point_t{3, 4});
  zaitceva::Shape::ptr circle = std::make_shared<zaitceva::Circle>(zaitceva::point_t{5, 5}, 4);
  zaitceva::CompositeShape compShape(rectangle);
  compShape.add(circle);
  zaitceva::Matrix matrix = part(compShape);
  zaitceva::Matrix matrix1(matrix);
  zaitceva::Matrix matrix2 = std::move(matrix);
  BOOST_CHECK(matrix2 == matrix1);
  BOOST_CHECK_EQUAL(matrix2.getRows(), matrix1.getRows());
  BOOST_CHECK_EQUAL(matrix2.getColumns(), matrix1.getColumns());
}

BOOST_AUTO_TEST_CASE(testOfInvalidParametrs)
{
  zaitceva::Shape::ptr rectangle = std::make_shared<zaitceva::Rectangle>(6, 9, zaitceva::point_t{3, 4});
  zaitceva::Shape::ptr circle = std::make_shared<zaitceva::Circle>(zaitceva::point_t{5, 5}, 4);
  zaitceva::CompositeShape compShape(rectangle);
  compShape.add(circle);
  zaitceva::Matrix matrix = part(compShape);
  BOOST_CHECK_THROW(matrix[11][2], std::out_of_range);
  BOOST_CHECK_THROW(matrix[-2][1], std::out_of_range);
  BOOST_CHECK_NO_THROW(matrix[1][1]);
}

BOOST_AUTO_TEST_SUITE_END();
