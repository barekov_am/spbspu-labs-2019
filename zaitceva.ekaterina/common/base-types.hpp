#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace zaitceva
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    double width;
    double height;
    point_t pos;
  };

  point_t movePoint(point_t point, point_t centre, double angle);

}

#endif
