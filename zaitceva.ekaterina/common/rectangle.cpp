#include "rectangle.hpp"
#include <cassert>
#include <stdexcept>
#include <iostream>
#include <cmath>

zaitceva::Rectangle::Rectangle(double width, double height, const point_t &centre):
  width_(width),
  height_(height),
  centre_(centre),
  angle_(0)
{
  if ((width_ <= 0) || (height_ <= 0))
  {
    throw std::invalid_argument("Width and height must be positive");
  }
}

double zaitceva::Rectangle::getArea() const
{
  return width_ * height_;
}

zaitceva::rectangle_t zaitceva::Rectangle::getFrameRect() const
{
  const double sinA = std::fabs(std::sin(angle_ * M_PI / 180));
  const double cosA = std::fabs(std::cos(angle_ * M_PI / 180));
  const double width = height_ * sinA + width_ * cosA;
  const double height = height_ * cosA + width_ * sinA;
  return {width, height, centre_};
}

void zaitceva::Rectangle::move(const point_t &point)
{
  centre_ = point;
}

void zaitceva::Rectangle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

void zaitceva::Rectangle::scale(double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("Сoefficient must be positive");
  }
  width_ *= k;
  height_ *= k;
}

void zaitceva::Rectangle::rotate(double angle)
{
  angle_ += angle;
}

void zaitceva::Rectangle::printInfo() const
{
  std::cout << "FrameRect: " << width_ << " " << height_;
  std::cout << " (" << centre_.x << ", " << centre_.y << ")" << std::endl;
}
