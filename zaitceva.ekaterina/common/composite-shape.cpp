#include "composite-shape.hpp"
#include <stdexcept>
#include <cmath>

zaitceva::CompositeShape::CompositeShape():
  count_(0),
  figures_(nullptr)
{
}

zaitceva::CompositeShape::CompositeShape(const CompositeShape &compShape):
  count_(compShape.count_),
  figures_(std::make_unique<ptr[]>(compShape.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    figures_[i] = compShape.figures_[i];
  }
}

zaitceva::CompositeShape::CompositeShape(CompositeShape &&compShape):
  count_(compShape.count_),
  figures_(std::move(compShape.figures_))
{
  compShape.count_ = 0;
}

zaitceva::CompositeShape::CompositeShape(const ptr &shape):
  count_(1),
  figures_(std::make_unique<ptr[]>(1))
{
  figures_[0] = shape;
}

zaitceva::CompositeShape &zaitceva::CompositeShape::operator =(const CompositeShape &compShape)
{
  if (this != &compShape)
  {
    count_ = compShape.count_;
    array shapes(std::make_unique<ptr[]>(count_));
    for (size_t i = 0; i < count_; i++)
    {
      shapes[i] = compShape.figures_[i];
    }
    figures_.swap(shapes);
  }
  return *this;
}

zaitceva::CompositeShape &zaitceva::CompositeShape::operator =(CompositeShape &&compShape)
{
  if (this != &compShape)
  {
    count_ = compShape.count_;
    figures_ = std::move(compShape.figures_);
    compShape.count_ = 0;
  }
  return *this;
}

zaitceva::CompositeShape::ptr zaitceva::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::invalid_argument("Index is out of range");
  }
  return figures_[index];
}

double zaitceva::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    area += figures_[i]->getArea();
  }
  return area;
}

zaitceva::rectangle_t zaitceva::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("There is nothing in composite shape");
  }
  rectangle_t rect = figures_[0]->getFrameRect();
  double max_x = rect.pos.x + rect.width / 2;
  double min_x = rect.pos.x - rect.width / 2;
  double max_y = rect.pos.y + rect.height / 2;
  double min_y = rect.pos.y - rect.height / 2;
  for (size_t i = 1; i < count_; i++)
  {
    rect = figures_[i]->getFrameRect();
    max_x = std::max(max_x, rect.pos.x + rect.width / 2);
    min_x = std::min(min_x, rect.pos.x - rect.width / 2);
    max_y = std::max(max_y, rect.pos.y + rect.height / 2);
    min_y = std::min(min_y, rect.pos.y - rect.height / 2);
  }
  return{max_x - min_x, max_y - min_y, {(max_x + min_x) / 2, (max_y + min_y) / 2}};
}

void zaitceva::CompositeShape::move(const zaitceva::point_t &point)
{
  double movement_x = point.x - getFrameRect().pos.x;
  double movement_y = point.y - getFrameRect().pos.y;
  move(movement_x, movement_y);
}

void zaitceva::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < count_; i++)
  {
    figures_[i]->move(dx, dy);
  }
}

void zaitceva::CompositeShape::scale(double k)
{
  if (k < 0)
  {
    throw std::invalid_argument("Coefficient must be positive");
  }
  const zaitceva::point_t centre = getFrameRect().pos;
  for (size_t i = 0; i < count_; i++)
  {
    zaitceva::point_t shapeCentre = figures_[i]->getFrameRect().pos;
    figures_[i]->move((shapeCentre.x - centre.x) * (k - 1), (shapeCentre.y - centre.y) * (k - 1));
    figures_[i]->scale(k);
  }
}

void zaitceva::CompositeShape::rotate(double angle)
{
  const point_t centre = getFrameRect().pos;
  for (size_t i = 0; i < count_; i++)
  {
    const point_t shapeCentre = figures_[i]->getFrameRect().pos;
    figures_[i]->move(movePoint(shapeCentre, centre, angle));
    figures_[i]->rotate(angle);
  }
}

size_t zaitceva::CompositeShape::getSize() const
{
  return count_;
}

void zaitceva::CompositeShape::add(ptr &shape)
{
  array shapes(std::make_unique<ptr[]>(count_+1));
  for (size_t i = 0; i < count_; i++)
  {
    shapes[i] = figures_[i];
  }
  shapes[count_] = shape;
  count_++;

  figures_.swap(shapes);
}

void zaitceva::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }
  count_--;
  for (size_t i = index; i < count_; i++)
  {
    figures_[i] = figures_[i + 1];
  }
  figures_[count_] = nullptr;
}

void zaitceva::CompositeShape::printInfo() const
{
  std::cout << "Centre: " << "(" << getFrameRect().pos.x << "," << getFrameRect().pos.y << ")" << std::endl;
  std::cout << "Width: " << getFrameRect().width << std::endl;
  std::cout << "Height: " << getFrameRect().height << std::endl;
  std::cout << "Number of shapes: " << getSize() <<  std::endl;
}
