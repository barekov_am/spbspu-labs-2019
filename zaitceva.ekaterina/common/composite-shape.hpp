#ifndef LAB2_COMPOSITE_SHAPE_H
#define LAB2_COMPOSITE_SHAPE_H
#include "shape.hpp"
#include <iostream>

namespace zaitceva
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &compShape);
    CompositeShape(CompositeShape &&compShape);
    CompositeShape(const ptr &shape);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &compShape);
    CompositeShape &operator =(CompositeShape &&compShape);
    ptr operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(double dx, double dy) override;
    void scale(double k) override;
    void rotate(double angle) override;
    size_t getSize() const;
    void add(ptr &shape);
    void remove(size_t index);
    void printInfo() const override;

  private:
    size_t count_;
    array figures_;
  };
}

#endif //LAB2_COMPOSITE_SHAPE_H
