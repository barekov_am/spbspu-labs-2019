#include <stdexcept>
#include <memory>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(testsForCompositeShape)

const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(testOfImmutability)
{
  zaitceva::Shape::ptr rectangle = std::make_shared<zaitceva::Rectangle>(6, 9, zaitceva::point_t {3, 4});
  zaitceva::Shape::ptr circle = std::make_shared<zaitceva::Circle>(zaitceva::point_t {5, 5}, 4);
  zaitceva::CompositeShape compShape(rectangle);
  compShape.add(circle);
  const double width = compShape.getFrameRect().width;
  const double height = compShape.getFrameRect().height;
  const double area = compShape.getArea();
  compShape.move({2, 3});
  BOOST_CHECK_CLOSE(compShape.getFrameRect().width, width, EPSILON);
  BOOST_CHECK_CLOSE(compShape.getFrameRect().height, height, EPSILON);
  BOOST_CHECK_CLOSE(compShape.getArea(), area, EPSILON);
  compShape.move(1, 4);
  BOOST_CHECK_CLOSE(compShape.getFrameRect().width, width, EPSILON);
  BOOST_CHECK_CLOSE(compShape.getFrameRect().height, height, EPSILON);
  BOOST_CHECK_CLOSE(compShape.getArea(), area, EPSILON);
}

BOOST_AUTO_TEST_CASE(testOfCorrectScaling)
{
  zaitceva::Shape::ptr rectangle = std::make_shared<zaitceva::Rectangle>(6, 9, zaitceva::point_t {3, 4});
  zaitceva::Shape::ptr circle = std::make_shared<zaitceva::Circle>(zaitceva::point_t {5, 5}, 4);
  zaitceva::CompositeShape compShape(rectangle);
  compShape.add(circle);
  const double area = compShape.getArea();
  const double k = 2;
  const zaitceva::point_t centre = compShape.getFrameRect().pos;
  compShape.scale(k);
  const zaitceva::point_t compCentre = compShape.getFrameRect().pos;
  const zaitceva::point_t shapeCentre1 = compShape[0]->getFrameRect().pos;
  const zaitceva::point_t shapeCentre2 = compShape[1]->getFrameRect().pos;
  BOOST_CHECK_CLOSE(compShape.getArea(), area * k * k, EPSILON);
  BOOST_CHECK_CLOSE(centre.x, compCentre.x, EPSILON);
  BOOST_CHECK_CLOSE(centre.y, compCentre.y, EPSILON);
  BOOST_CHECK_CLOSE(shapeCentre1.x, 3 + (3 - compCentre.x) * (k - 1), EPSILON);
  BOOST_CHECK_CLOSE(shapeCentre1.y, 4 + (4 - compCentre.y) * (k - 1), EPSILON);
  BOOST_CHECK_CLOSE(shapeCentre2.x, 5 + (5 - compCentre.x) * (k - 1), EPSILON);
  BOOST_CHECK_CLOSE(shapeCentre2.y, 5 + (5 - compCentre.y) * (k - 1), EPSILON);
}

BOOST_AUTO_TEST_CASE(testOfCorrectRotation)
{
  zaitceva::Shape::ptr rectangle = std::make_shared<zaitceva::Rectangle>(6, 10, zaitceva::point_t {3, 4});
  zaitceva::Shape::ptr circle = std::make_shared<zaitceva::Circle>(zaitceva::point_t {7, 4}, 3);
  zaitceva::CompositeShape compShape(rectangle);
  compShape.add(circle);
  const double area = compShape.getArea();
  const zaitceva::rectangle_t frameRect = compShape.getFrameRect();
  compShape.rotate(90);
  const zaitceva::point_t shapeCentre1 = compShape[0]->getFrameRect().pos;
  const zaitceva::point_t shapeCentre2 = compShape[1]->getFrameRect().pos;
  BOOST_CHECK_CLOSE(area, compShape.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, compShape.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, compShape.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.x, compShape.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.y, compShape.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(shapeCentre1.x, 5, EPSILON);
  BOOST_CHECK_CLOSE(shapeCentre1.y, 2, EPSILON);
  BOOST_CHECK_CLOSE(shapeCentre2.x, 5, EPSILON);
  BOOST_CHECK_CLOSE(shapeCentre2.y, 6, EPSILON);
}

BOOST_AUTO_TEST_CASE(testOfInvalidValues)
{
  zaitceva::Shape::ptr rectangle = std::make_shared<zaitceva::Rectangle>(6, 9, zaitceva::point_t {3, 4});
  zaitceva::Shape::ptr circle = std::make_shared<zaitceva::Circle>(zaitceva::point_t {5, 5}, 4);
  zaitceva::CompositeShape compShape(rectangle);
  compShape.add(circle);
  BOOST_CHECK_THROW(compShape.scale(-2), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.remove(3), std::out_of_range);
  BOOST_CHECK_THROW(compShape[3], std::invalid_argument);
  zaitceva::CompositeShape compShape2;
  BOOST_CHECK_THROW(compShape2.getFrameRect(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(testOfCopyConstructor)
{
  zaitceva::Shape::ptr rectangle = std::make_shared<zaitceva::Rectangle>(6, 9, zaitceva::point_t {3, 4});
  zaitceva::Shape::ptr circle = std::make_shared<zaitceva::Circle>(zaitceva::point_t {5, 5}, 4);
  zaitceva::CompositeShape compShape1(rectangle);
  compShape1.add(circle);
  const zaitceva::rectangle_t frameRect1 = compShape1.getFrameRect();

  zaitceva::CompositeShape compShape2(compShape1);
  const zaitceva::rectangle_t frameRect2 = compShape2.getFrameRect();

  BOOST_CHECK_CLOSE(compShape1.getArea(), compShape2.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(compShape1.getSize(), compShape2.getSize());
  BOOST_CHECK_CLOSE(frameRect1.width, frameRect2.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.height, frameRect2.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.pos.x, frameRect2.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.pos.y, frameRect2.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(testOfMoveConstructor)
{
  zaitceva::Shape::ptr rectangle = std::make_shared<zaitceva::Rectangle>(6, 9, zaitceva::point_t {3, 4});
  zaitceva::Shape::ptr circle = std::make_shared<zaitceva::Circle>(zaitceva::point_t {5, 5}, 4);
  zaitceva::CompositeShape compShape1(rectangle);
  compShape1.add(circle);
  const zaitceva::rectangle_t frameRect1 = compShape1.getFrameRect();
  const double area1 = compShape1.getArea();
  const size_t count1 = compShape1.getSize();

  zaitceva::CompositeShape compShape2(std::move(compShape1));
  const zaitceva::rectangle_t frameRect2 = compShape2.getFrameRect();

  BOOST_CHECK_CLOSE(area1, compShape2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.width, frameRect2.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.height, frameRect2.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.pos.x, frameRect2.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.pos.y, frameRect2.pos.y, EPSILON);
  BOOST_CHECK_EQUAL(count1, compShape2.getSize());
  BOOST_CHECK_EQUAL(compShape1.getSize(), 0);
}

BOOST_AUTO_TEST_CASE(testOfMoveOperator)
{
  zaitceva::Shape::ptr rectangle = std::make_shared<zaitceva::Rectangle>(6, 9, zaitceva::point_t {3, 4});
  zaitceva::Shape::ptr circle = std::make_shared<zaitceva::Circle>(zaitceva::point_t {5, 5}, 4);
  zaitceva::CompositeShape compShape1(rectangle);
  compShape1.add(circle);
  const zaitceva::rectangle_t frameRect1 = compShape1.getFrameRect();
  const double area1 = compShape1.getArea();
  const size_t count1 = compShape1.getSize();

  zaitceva::Shape::ptr triangle = std::make_shared<zaitceva::Triangle>(zaitceva::point_t {3, 6}, zaitceva::point_t {6, 3}, zaitceva::point_t{3, 3});
  zaitceva::CompositeShape compShape2(triangle);
  compShape2 = std::move(compShape1);
  const zaitceva::rectangle_t frameRect2 = compShape2.getFrameRect();

  BOOST_CHECK_CLOSE(area1, compShape2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.width, frameRect2.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.height, frameRect2.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.pos.x, frameRect2.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.pos.y, frameRect2.pos.y, EPSILON);
  BOOST_CHECK_EQUAL(count1, compShape2.getSize());
  BOOST_CHECK_EQUAL(compShape1.getSize(), 0);
}

BOOST_AUTO_TEST_CASE(testOfCopyOperator)
{
  zaitceva::Shape::ptr rectangle = std::make_shared<zaitceva::Rectangle>(6, 9, zaitceva::point_t {3, 4});
  zaitceva::Shape::ptr circle = std::make_shared<zaitceva::Circle>(zaitceva::point_t {5, 5}, 4);
  zaitceva::CompositeShape compShape1(rectangle);
  compShape1.add(circle);
  const zaitceva::rectangle_t frameRect1 = compShape1.getFrameRect();

  zaitceva::Shape::ptr triangle = std::make_shared<zaitceva::Triangle>(zaitceva::point_t {3, 6}, zaitceva::point_t {6, 3}, zaitceva::point_t{3, 3});
  zaitceva::CompositeShape compShape2(triangle);
  compShape2 = compShape1;
  const zaitceva::rectangle_t frameRect2 = compShape2.getFrameRect();

  BOOST_CHECK_CLOSE(compShape1.getArea(), compShape2.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(compShape1.getSize(), compShape2.getSize());
  BOOST_CHECK_CLOSE(frameRect1.width, frameRect2.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.height, frameRect2.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.pos.x, frameRect2.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.pos.y, frameRect2.pos.y, EPSILON);
}

BOOST_AUTO_TEST_SUITE_END();
