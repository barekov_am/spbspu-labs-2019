#include "triangle.hpp"
#include <iostream>
#include <cmath>
#include <cassert>
#include <algorithm>

zaitceva::Triangle::Triangle(const point_t &point1, const point_t &point2, const point_t &point3):
  point1_(point1),
  point2_(point2),
  point3_(point3),
  centre_(getCentre())
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("Points can't lie on the same line");
  }
}

zaitceva::point_t zaitceva::Triangle::getCentre() const
{
  return {(point1_.x + point2_.x + point3_.x) / 3, (point1_.y + point2_.y + point3_.y) / 3};
}

double zaitceva::Triangle::getArea() const
{
  return std::fabs(0.5 * (point1_.x * (point2_.y - point3_.y)
      + point2_.x * (point3_.y - point1_.y) + point3_.x * (point1_.y - point2_.y)));
}

zaitceva::rectangle_t zaitceva::Triangle::getFrameRect() const
{
  double max_x = std::max({point1_.x, point2_.x, point3_.x});
  double max_y = std::max({point1_.y, point2_.y, point3_.y});
  double min_x = std::min({point1_.x, point2_.x, point3_.x});
  double min_y = std::min({point1_.y, point2_.y, point3_.y});
  point_t cenRect = {(max_x + min_x)/2, (max_y + min_y)/2};
  return {max_x - min_x, max_y - min_y, cenRect};
}

void zaitceva::Triangle::move(const point_t &point)
{
  double movement_x = point.x - centre_.x;
  double movement_y = point.y - centre_.y;
  move(movement_x, movement_y);
}

void zaitceva::Triangle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
  point1_.x += dx;
  point1_.y += dy;
  point2_.x += dx;
  point2_.y += dy;
  point3_.x += dx;
  point3_.y += dy;
}

void zaitceva::Triangle::scale(double coef)
{
  if (coef <= 0)
  {
    throw std::invalid_argument("Coefficient must be positive");
  }
  point1_.x = centre_.x + coef * (point1_.x - centre_.x);
  point1_.y = centre_.y + coef * (point1_.y - centre_.y);
  point2_.x = centre_.x + coef * (point2_.x - centre_.x);
  point2_.y = centre_.y + coef * (point2_.y - centre_.y);
  point3_.x = centre_.x + coef * (point3_.x - centre_.x);
  point3_.y = centre_.y + coef * (point3_.y - centre_.y);
}

void zaitceva::Triangle::rotate(double angle)
{
  point1_ = movePoint(point1_, centre_, angle);
  point2_ = movePoint(point2_, centre_, angle);
  point3_ = movePoint(point3_, centre_, angle);
}

void zaitceva::Triangle::printInfo() const
{
  std::cout << "Point1:" << " (" << point1_.x << "," << point1_.y << ")" << std::endl;
  std::cout << "Point2:" << " (" << point2_.x << "," << point2_.y << ")" << std::endl;
  std::cout << "Point3:" << " (" << point3_.x << "," << point3_.y << ")" << std::endl;
  std::cout << "Centre of gravity:" << " (" << centre_.x << "," << centre_.y << ")" << std::endl;
}
