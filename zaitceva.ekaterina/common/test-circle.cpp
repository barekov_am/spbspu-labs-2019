#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testsForCircle)

const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(testOfImmutability)
{
  zaitceva::Circle testingCircle({5, 5}, 4);
  const double radius = testingCircle.getFrameRect().height / 2;
  const double area = testingCircle.getArea();
  testingCircle.move({2, 1});
  BOOST_CHECK_CLOSE(testingCircle.getFrameRect().height / 2, radius, EPSILON);
  BOOST_CHECK_CLOSE(testingCircle.getArea(), area, EPSILON);
  testingCircle.move(2, 8);
  BOOST_CHECK_CLOSE(testingCircle.getFrameRect().height / 2, radius, EPSILON);
  BOOST_CHECK_CLOSE(testingCircle.getArea(), area, EPSILON);
}

BOOST_AUTO_TEST_CASE(testOfCorrectScaling)
{
  zaitceva::Circle testingCircle({5, 5}, 4);
  const double area = testingCircle.getArea();
  const double k = 2;
  testingCircle.scale(k);
  BOOST_CHECK_CLOSE(testingCircle.getArea(), area * k * k, EPSILON);
}

BOOST_AUTO_TEST_CASE(testOfInvalidValues)
{
  BOOST_CHECK_THROW(zaitceva::Circle testingCircle({5, 5}, -4), std::invalid_argument);
  zaitceva::Circle testingCircle({5, 5}, 4);
  BOOST_CHECK_THROW(testingCircle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END();
