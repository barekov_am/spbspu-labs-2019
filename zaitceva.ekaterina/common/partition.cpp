#include <cmath>
#include "partition.hpp"

zaitceva::Matrix zaitceva::part(const zaitceva::CompositeShape &compShape)
{
  Matrix matrix;
  const std::size_t size = compShape.getSize();
  for (std::size_t i = 0; i < size; i++)
  {
    std::size_t layer = 0;
    for (std::size_t j = 0; j < matrix.getRows(); j++)
    {
      bool intersect1 = false;
      for (std::size_t k = 0; k < matrix.getLayerSize(j); k++)
      {
        if (intersect(compShape[i]->getFrameRect(), matrix[j][k]->getFrameRect()))
        {
          layer++;
          intersect1 = true;
          break;
        }
      }
      if (!intersect1)
      {
        break;
      }
    }
    matrix.add(compShape[i], layer);
  }
  return matrix;
}

bool zaitceva::intersect(const zaitceva::rectangle_t &shape1, const zaitceva::rectangle_t &shape2)
{
  const double distanceX = fabs(shape1.pos.x - shape2.pos.x);
  const double distanceY = fabs(shape1.pos.y - shape2.pos.y);

  const double lengthX = (shape1.width + shape2.width) / 2;
  const double lengthY = (shape1.height + shape2.height) / 2;

  const bool firstCondition = (distanceX < lengthX);
  const bool secondCondition = (distanceY < lengthY);

  return firstCondition && secondCondition;
}
