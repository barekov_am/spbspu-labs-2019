#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testsForRectangle)

const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(testOfImmutability)
{
  zaitceva::Rectangle testingRect(6, 9, {3, 4});
  const double width = testingRect.getFrameRect().width;
  const double height = testingRect.getFrameRect().height;
  const double area = testingRect.getArea();
  testingRect.move({2, 1});
  BOOST_CHECK_CLOSE(testingRect.getFrameRect().width, width, EPSILON);
  BOOST_CHECK_CLOSE(testingRect.getFrameRect().height, height, EPSILON);
  BOOST_CHECK_CLOSE(testingRect.getArea(), area, EPSILON);
  testingRect.move(2, 8);
  BOOST_CHECK_CLOSE(testingRect.getFrameRect().width, width, EPSILON);
  BOOST_CHECK_CLOSE(testingRect.getFrameRect().height, height, EPSILON);
  BOOST_CHECK_CLOSE(testingRect.getArea(), area, EPSILON);
}

BOOST_AUTO_TEST_CASE(testOfCorrectScaling)
{
  zaitceva::Rectangle testingRect(6, 9, {3, 4});
  const double area = testingRect.getArea();
  const double k = 2;
  testingRect.scale(k);
  BOOST_CHECK_CLOSE(testingRect.getArea(), area * k * k, EPSILON);
}

BOOST_AUTO_TEST_CASE(testOfCorrectRotation)
{
  zaitceva::Rectangle testingRect(6, 9, {3, 4});
  const double area = testingRect.getArea();
  const zaitceva::rectangle_t frameRect = testingRect.getFrameRect();
  testingRect.rotate(90);
  BOOST_CHECK_CLOSE(area, testingRect.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, testingRect.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, testingRect.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.x, testingRect.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.y, testingRect.getFrameRect().pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(testOfInvalidValues)
{
  BOOST_CHECK_THROW(zaitceva::Rectangle testingRect(-6, 9, {3, 4}), std::invalid_argument);
  BOOST_CHECK_THROW(zaitceva::Rectangle testingRect(6, -9, {3, 4}), std::invalid_argument);
  zaitceva::Rectangle testingRect(6, 9, {3, 4});
  BOOST_CHECK_THROW(testingRect.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END();
