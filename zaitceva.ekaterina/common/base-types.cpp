#include "base-types.hpp"
#include <cmath>

zaitceva::point_t zaitceva::movePoint(zaitceva::point_t point, point_t centre, double angle)
{
  const double cosA = std::fabs(std::cos(angle * M_PI / 180));
  const double sinA = std::fabs(std::sin(angle * M_PI / 180));
  const double dx = (point.x - centre.x) * cosA - (point.y - centre.y) * sinA;
  const double dy = (point.x - centre.x) * sinA + (point.y - centre.y) * cosA;
  point.x = centre.x + dx;
  point.y = centre.y + dy;
  return point;
}
