#include "circle.hpp"
#include <cmath>
#include <cassert>
#include <stdexcept>
#include <iostream>

zaitceva::Circle::Circle(const point_t &centre, double radius):
  centre_(centre),
  radius_(radius)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("Radius must be positive");
  }
}

double zaitceva::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

zaitceva::rectangle_t zaitceva::Circle::getFrameRect() const
{
  return {2 * radius_, 2 * radius_, centre_};
}

void zaitceva::Circle::move(const point_t &point)
{
  centre_ = point;
}

void zaitceva::Circle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

void zaitceva::Circle::scale(double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("coefficient must be positive");
  }
  radius_ *= k;
}

void zaitceva::Circle::rotate(double)
{
}

void zaitceva::Circle::printInfo() const
{
  std::cout << "Radius: " << radius_ << std:: endl;
  std::cout << "Centre: (" << centre_.x << ", " << centre_.y << ")" << std::endl;
}
