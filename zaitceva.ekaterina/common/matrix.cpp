#include "matrix.hpp"

zaitceva::Matrix::Matrix():
  rows_(0),
  columns_(0)
{
}

zaitceva::Matrix::Matrix(const Matrix &matrix):
  figures_(std::make_unique<Shape::ptr[]>(matrix.rows_ * matrix.columns_)),
  rows_(matrix.rows_),
  columns_(matrix.columns_)
{
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    figures_[i] = matrix.figures_[i];
  }
}

zaitceva::Matrix::Matrix(Matrix &&matrix):
  figures_(std::move(matrix.figures_)),
  rows_(matrix.rows_),
  columns_(matrix.columns_)
{
  matrix.rows_ = 0;
  matrix.columns_ = 0;
}

zaitceva::Matrix &zaitceva::Matrix::operator=(const Matrix &matrix)
{
  if (this != &matrix)
  {
    Shape::array shapes = std::make_unique<Shape::ptr[]>(matrix.rows_ * matrix.columns_);
    for (size_t i = 0; i < (matrix.rows_ * matrix.columns_); i++)
    {
      shapes[i] = matrix.figures_[i];
    }
    figures_.swap(shapes);
    rows_ = matrix.rows_;
    columns_ = matrix.columns_;
  }
  return *this;
}

zaitceva::Matrix &zaitceva::Matrix::operator =(Matrix &&matrix)
{
  if (this != &matrix)
  {
    rows_ = matrix.rows_;
    columns_ = matrix.columns_;
    figures_ = std::move(matrix.figures_);
    matrix.rows_ = 0;
    matrix.columns_ = 0;
  }
  return *this;
}

zaitceva::Shape::array zaitceva::Matrix::operator[](size_t index) const
{
  if (index >= rows_)
  {
    throw std::out_of_range("Index is out of range");
  }
  Shape::array shapes = std::make_unique<Shape::ptr[]>(columns_);
  for (size_t i = 0; i < columns_; i++)
  {
    shapes[i] = figures_[index * columns_ + i];
  }
  return shapes;
}

bool zaitceva::Matrix::operator ==(const Matrix &matrix) const
{
  if ((rows_ != matrix.rows_) || (columns_ != matrix.columns_))
  {
    return false;
  }
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (figures_[i] != matrix.figures_[i])
    {
      return false;
    }
  }
  return true;
}

bool zaitceva::Matrix::operator!=(const Matrix &matrix) const
{
  return !(*this == matrix);
}

size_t zaitceva::Matrix::getRows() const
{
  return rows_;
}

size_t zaitceva::Matrix::getColumns() const
{
  return columns_;
}

size_t zaitceva::Matrix::getLayerSize(size_t layer) const
{
  if (layer >= rows_)
  {
    return 0;
  }
  for (size_t i = 0; i < columns_; i++)
  {
    if (figures_[layer * columns_ + i] == nullptr)
    {
      return i;
    }
  }
  return columns_;
}

void zaitceva::Matrix::add(zaitceva::Shape::ptr shape, size_t layer)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Pointer can't be null");
  }
  size_t tmpRows = (layer >= rows_) ? (rows_ + 1) : rows_;
  size_t tmpColumns = (getLayerSize(layer) == columns_) ? (columns_ + 1) : columns_;
  const size_t column = (layer < rows_) ? getLayerSize(layer) : 0;
  Shape::array shapes = std::make_unique<Shape::ptr[]>(tmpRows * tmpColumns);
  for (size_t i = 0; i < tmpRows; i++)
  {
    for (size_t j = 0; j < tmpColumns; j++)
    {
      if ((i == rows_) || (j == columns_))
      {
        shapes[i * tmpColumns + j] = nullptr;
      }
      else
      {
        shapes[i * tmpColumns + j] = figures_[i * columns_ + j];
      }
    }
  }
  if (layer < rows_)
  {
    shapes[layer * tmpColumns + column] = shape;
  }
  else
  {
    shapes[rows_ * tmpColumns + column] = shape;
  }
  figures_.swap(shapes);
  rows_ = tmpRows;
  columns_ = tmpColumns;
}
