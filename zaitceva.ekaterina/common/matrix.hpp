#ifndef LAB2_MATRIX_H
#define LAB2_MATRIX_H

#include <memory>
#include "shape.hpp"

namespace zaitceva
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &matrix);
    Matrix(Matrix &&matrix);
    ~Matrix() = default;

    Matrix &operator =(const Matrix &matrix);
    Matrix &operator =(Matrix &&matrix);
    Shape::array operator [](size_t index) const;
    bool operator ==(const Matrix &matrix) const;
    bool operator !=(const Matrix &matrix) const;

    size_t getRows() const;
    size_t  getColumns() const;
    size_t getLayerSize(size_t layer) const;
    void add(Shape::ptr shape, size_t layer);

  private:
    Shape::array figures_;
    size_t rows_;
    size_t columns_;
  };
}

#endif //LAB2_MATRIX_H
