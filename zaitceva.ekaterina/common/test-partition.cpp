#include <boost/test/auto_unit_test.hpp>
#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testPartition)

const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(testOfCorrectPartition)
{
  zaitceva::Shape::ptr rect1 = std::make_shared<zaitceva::Rectangle>(6, 10, zaitceva::point_t {3, 4});
  zaitceva::Shape::ptr circ1 = std::make_shared<zaitceva::Circle>(zaitceva::point_t {5, 5}, 4);
  zaitceva::Shape::ptr triangle = std::make_shared<zaitceva::Triangle>(zaitceva::point_t {3, 6}, zaitceva::point_t {6, 3}, zaitceva::point_t{3, 3});
  zaitceva::Shape::ptr circ2 = std::make_shared<zaitceva::Circle>(zaitceva::point_t {12, 14}, 2);
  zaitceva::Shape::ptr square = std::make_shared<zaitceva::Rectangle>(4, 4, zaitceva::point_t {-1, 5});
  zaitceva::CompositeShape compShape(rect1);
  compShape.add(circ1);
  compShape.add(triangle);
  compShape.add(circ2);
  compShape.add(square);
  zaitceva::Matrix matrix = zaitceva::part(compShape);

  const size_t rows = 3;
  const size_t columns = 2;

  BOOST_CHECK_EQUAL(matrix.getRows(), rows);
  BOOST_CHECK_EQUAL(matrix.getColumns(), columns);
  BOOST_CHECK(matrix[0][0] == rect1);
  BOOST_CHECK(matrix[1][0] == circ1);
  BOOST_CHECK(matrix[2][0] == triangle);
  BOOST_CHECK(matrix[0][1] == circ2);
  BOOST_CHECK(matrix[1][1] == square);
}

BOOST_AUTO_TEST_CASE(intersectCorrectness)
{
  zaitceva::Circle circle({2, 0}, 3);
  zaitceva::Rectangle rectangle(9, 5, {2, 4});
  zaitceva::Rectangle square(1, 1, {12, 21});

  BOOST_CHECK(zaitceva::intersect(circle.getFrameRect(),rectangle.getFrameRect()));
  BOOST_CHECK(!(zaitceva::intersect(square.getFrameRect(),rectangle.getFrameRect())));
  BOOST_CHECK(!(zaitceva::intersect(circle.getFrameRect(),square.getFrameRect())));
}

BOOST_AUTO_TEST_SUITE_END();
