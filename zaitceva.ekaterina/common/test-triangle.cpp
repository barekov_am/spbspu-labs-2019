#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(testsForTriangle)

const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(testOfImmutability)
{
  zaitceva::Triangle testingTriangle({3, 6}, {6, 3}, {3, 3});
  const double width = testingTriangle.getFrameRect().width;
  const double height = testingTriangle.getFrameRect().height;
  const double area = testingTriangle.getArea();
  testingTriangle.move({1, 1});
  BOOST_CHECK_CLOSE(testingTriangle.getFrameRect().width, width, EPSILON);
  BOOST_CHECK_CLOSE(testingTriangle.getFrameRect().height, height, EPSILON);
  BOOST_CHECK_CLOSE(testingTriangle.getArea(), area, EPSILON);
  testingTriangle.move(2, 8);
  BOOST_CHECK_CLOSE(testingTriangle.getFrameRect().width, width, EPSILON);
  BOOST_CHECK_CLOSE(testingTriangle.getFrameRect().height, height, EPSILON);
  BOOST_CHECK_CLOSE(testingTriangle.getArea(), area, EPSILON);
}

BOOST_AUTO_TEST_CASE(testOfCorrectScaling)
{
  zaitceva::Triangle testingTriangle({-1, -1}, {-1, -4}, {-5, -1});
  const double area = testingTriangle.getArea();
  const double k = 2;
  testingTriangle.scale(k);
  BOOST_CHECK_CLOSE(testingTriangle.getArea(), area * k * k, EPSILON);
}

BOOST_AUTO_TEST_CASE(testOfCorrectRotation)
{
  zaitceva::Triangle testingTriangle({-1, -1}, {-1, -4}, {-5, -1});
  const double area = testingTriangle.getArea();
  const zaitceva::rectangle_t frameRect = testingTriangle.getFrameRect();
  const zaitceva::point_t centre = testingTriangle.getCentre();
  testingTriangle.rotate(90);
  BOOST_CHECK_CLOSE(testingTriangle.getArea(), area, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, testingTriangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, testingTriangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(centre.x, testingTriangle.getCentre().x, EPSILON);
  BOOST_CHECK_CLOSE(centre.y, testingTriangle.getCentre().y, EPSILON);
}


BOOST_AUTO_TEST_CASE(testOfInvalidValues)
{
  BOOST_CHECK_THROW(zaitceva::Triangle testingTriangle({-1, -1}, {-1, -4}, {-1, -5}), std::invalid_argument);
  zaitceva::Triangle testingTriangle({-1, -1}, {-1, -4}, {-5, -1});
  BOOST_CHECK_THROW(testingTriangle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END();
