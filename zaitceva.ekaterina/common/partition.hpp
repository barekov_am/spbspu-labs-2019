#ifndef LAB2_PARTITION_H
#define LAB2_PARTITION_H

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace zaitceva
{
  zaitceva::Matrix part(const zaitceva::CompositeShape &compShape);
  bool intersect(const rectangle_t &shape1, const rectangle_t &shape2);
}

#endif //LAB2_PARTITION_H
