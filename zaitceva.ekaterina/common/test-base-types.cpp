#include <boost/test/auto_unit_test.hpp>
#include "base-types.hpp"

BOOST_AUTO_TEST_SUITE(testsBaseTypes)

const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(testForMovePoint)
{
  const zaitceva::point_t point = {4, 0};
  const zaitceva::point_t centre = {0, 0};
  const zaitceva::point_t point1 = zaitceva::movePoint(point, centre, 30);
  BOOST_CHECK_CLOSE(point1.x, 3.464102, EPSILON);
  BOOST_CHECK_CLOSE(point1.y, 2, EPSILON);

  const zaitceva::point_t point2 = zaitceva::movePoint(point, centre, 45);
  BOOST_CHECK_CLOSE(point2.x, 2.828427, EPSILON);
  BOOST_CHECK_CLOSE(point2.y, 2.828427, EPSILON);
}

BOOST_AUTO_TEST_SUITE_END();
