#ifndef B6_STATISTICS_H
#define B6_STATISTICS_H

class Statistics
{
public:
  Statistics();
  void operator()(const int &);
  void printStatistics();

private:
  int max_;
  int min_;
  double mean_;
  int positive_;
  int negative_;
  long long int evenSum_;
  long long int oddSum_;
  bool equalFirstLast_;
  bool empty_;
  int first_;
  int total_;
};

#endif //B6_STATISTICS_H
