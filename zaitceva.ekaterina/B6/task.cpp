#include <iterator>
#include <iostream>
#include <algorithm>
#include <list>
#include "statistics.hpp"

void task()
{
  std::list<int> numbers(std::istream_iterator<int>(std::cin), std::istream_iterator<int>());
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Invalid data\n");
  }

  Statistics stat = std::for_each(numbers.begin(), numbers.end(), Statistics());

  stat.printStatistics();

}
