#include <iostream>

#include "statistics.hpp"

Statistics::Statistics():
  max_(0),
  min_(0),
  mean_(0),
  positive_(0),
  negative_(0),
  evenSum_(0),
  oddSum_(0),
  equalFirstLast_(false),
  empty_(true),
  first_(0),
  total_(0)
{
}

void Statistics::operator()(const int &num)
{
  if (empty_)
  {
    max_ = num;
    min_ = num;
    first_ = num;
    empty_ = false;
  }

  if (num > max_)
  {
    max_ = num;
  }

  if (num < min_)
  {
    min_ = num;
  }

  if (num > 0)
  {
    positive_++;
  }
  else if (num < 0)
  {
    negative_++;
  }

  total_++;

  if (num % 2 == 0)
  {
    evenSum_ += num;
  }
  else
  {
    oddSum_ += num;
  }

  mean_ = (evenSum_ + oddSum_)/total_;

  equalFirstLast_ = (first_ == num);
}

void Statistics::printStatistics()
{
  if (empty_)
  {
    std::cout << "No Data" << std::endl;

    return;
  }

  std::cout << "Max: " << max_ << std::endl;
  std::cout << "Min: " << min_ << std::endl;
  std::cout << "Mean: " << mean_ << std::endl;
  std::cout << "Positive: " << positive_ << std::endl;
  std::cout << "Negative: " << negative_ << std::endl;
  std::cout << "Odd Sum: " << oddSum_ << std::endl;
  std::cout << "Even Sum: " << evenSum_ << std::endl;
  std::cout << "First/Last Equal: " << (equalFirstLast_ ? "yes" : "no") << std::endl;
}
