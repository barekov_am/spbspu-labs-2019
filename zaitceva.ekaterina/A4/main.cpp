#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "partition.hpp"

void showProgramWork(zaitceva::Shape &shape)
{
  shape.printInfo();
  std::cout << "AREA = " << shape.getArea() << std::endl;
  shape.move({2, 1});
  std::cout << "AFTER MOVING TO A POINT: " << std::endl;
  shape.printInfo();
  shape.move(2, 8);
  std::cout << "AFTER AXIAL MOVEMENT: " << std::endl;
  shape.printInfo();
  shape.scale(2);
  std::cout << "AFTER SCALING: " << std::endl;
  shape.printInfo();
  shape.rotate(45);
  std::cout << "AFTER ROTATION: " << std::endl;
  shape.printInfo();
}

int main()
{
  std::cout << "-----Rectangle-----" << std::endl;
  zaitceva::Rectangle rectangle(6, 9, {3, 4});
  showProgramWork(rectangle);

  std::cout << "-----Circle-----" << std::endl;
  zaitceva::Circle circle({5, 5}, 4);
  showProgramWork(circle);

  std::cout << "-----Triangle-----" << std::endl;
  zaitceva::Triangle triangle({3, 6}, {6, 3}, {3, 3});
  showProgramWork(triangle);

  std::cout << "-----Composite shape-----" << std::endl;
  zaitceva::Shape::ptr rectangle1 = std::make_shared<zaitceva::Rectangle>(6, 9, zaitceva::point_t {3, 4});
  zaitceva::CompositeShape composition(rectangle1);
  showProgramWork(composition);
  zaitceva::Shape::ptr circle1 = std::make_shared<zaitceva::Circle>(zaitceva::point_t {5, 5}, 4);
  zaitceva::Shape::ptr triangle1 = std::make_shared<zaitceva::Triangle>(zaitceva::point_t {3, 6}, zaitceva::point_t {6, 3}, zaitceva::point_t{3, 3});
  composition.add(circle1);
  composition.add(triangle1);
  std::cout << "After adding shapes: " << std::endl;
  composition.printInfo();
  composition.remove(2);
  std::cout << "After deleting shapes: " << std::endl;
  composition.printInfo();

  std::cout << "-----Partition-----" << std::endl;
  composition.add(triangle1);
  zaitceva::Shape::ptr circle2 = std::make_shared<zaitceva::Circle>(zaitceva::point_t {12, 14}, 2);
  composition.add(circle2);
  zaitceva::Matrix matrix = zaitceva::part(composition);
  std::cout << "Layers: " << matrix.getRows() << std::endl;
  std::cout << "Columns: " << matrix.getColumns() << std::endl;
  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    for (size_t j = 0; j < matrix.getLayerSize(i); j++)
    {
      std::cout << "Layer " << i << std::endl;
      std::cout << "Centre is (";
      std::cout << matrix[i][j]->getFrameRect().pos.x << ", " << matrix[i][j]->getFrameRect().pos.y << ")" << std::endl;
    }
  }
  return 0;
}
