#include <iostream>
#include <vector>
#include <algorithm>

void task1()
{
  std::vector<std::string> words;
  std::string line;
  std::string sep = " \t\n";
  size_t pos = std::string::npos;

  while (std::getline(std::cin, line))
  {
    while (line.find_first_of(sep) == 0)
    {
      line.erase(0, 1);
    }

    while (!line.empty())
    {
      pos = line.find_first_of(sep);
      words.push_back(line.substr(0, pos));
      if (pos == std::string::npos)
      {
        line.clear();
        break;
      }
      line.erase(0, pos + 1);

      while (line.find_first_of(sep) == 0)
      {
        line.erase(0, 1);
      }
    }
  }

  std::sort(words.begin(), words.end());
  words.erase(std::unique(words.begin(), words.end()), words.end());

  for (auto &i : words)
  {
    std::cout << i << std::endl;
  }
}
