#ifndef B5_SHAPE_H
#define B5_SHAPE_H

#include <vector>

struct Point_t
{
    int x, y;
};

using Shape = std::vector<Point_t>;

#endif //B5_SHAPE_H
