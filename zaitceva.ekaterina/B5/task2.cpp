#include <iostream>
#include <exception>
#include <string>
#include <algorithm>

#include "shape.hpp"

const int VERTICES_OF_TRIANGLE = 3;
const int VERTICES_OF_RECTANGLE = 4;
const int VERTICES_OF_PENTAGON = 5;

Shape readPoints(std::string line, std::size_t vertices);
bool isSquare(const Shape &shape);
bool isRectangle(const Shape &shape);

void task2()
{
  std::vector<Shape> shapeContainer;

  std::string line;
  std::string sep = " \t";
  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading failed");
    }

    while (line.find_first_of(sep) == 0)
    {
      line.erase(0, 1);
    }

    if (line.empty())
    {
      continue;
    }

    std::size_t pos = line.find_first_of('(');
    if (pos == std::string::npos)
    {
      throw std::invalid_argument("Invalid shape");
    }

    std::size_t verticesNum = std::stoi(line.substr(0, pos));
    line.erase(0, pos);
    if (verticesNum < 1)
    {
      throw std::invalid_argument("Invalid number of vertices");
    }

    shapeContainer.push_back(readPoints(line, verticesNum));
  }

  std::size_t countOfVertices = 0;
  std::size_t countOfTriangles = 0;
  std::size_t countOfSquares = 0;
  std::size_t countOfRectangle = 0;

  std::for_each(shapeContainer.begin(), shapeContainer.end(), [&](const Shape &shape) {
      countOfVertices += shape.size();
      if (shape.size() == VERTICES_OF_TRIANGLE)
      {
        ++countOfTriangles;
      }
      else if (shape.size() == VERTICES_OF_RECTANGLE)
      {
        if (isRectangle(shape))
        {
          ++countOfRectangle;
          if (isSquare(shape))
          {
            ++countOfSquares;
          }
        }
      }
  });

  shapeContainer.erase(std::remove_if(shapeContainer.begin(), shapeContainer.end(),
      [](const Shape &shape) { return shape.size() == VERTICES_OF_PENTAGON; } ), shapeContainer.end());

  Shape points(shapeContainer.size());

  std::transform(shapeContainer.begin(), shapeContainer.end(), points.begin(),
      [](const Shape &shape) { return shape[0]; });

  std::sort(shapeContainer.begin(), shapeContainer.end(),[](const Shape &shape1, const Shape &shape2) {
      if (shape1.size() < shape2.size())
      {
        return true;
      }
      if ((shape1.size() == VERTICES_OF_RECTANGLE) && (shape2.size() == VERTICES_OF_RECTANGLE))
      {
        if (isSquare(shape1))
        {
          if (isSquare(shape2))
          {
            return shape1[0].x < shape2[0].x;
          }
          return true;
        }
      }
      return false;
  });

  std::cout << "Vertices: " << countOfVertices << std::endl;
  std::cout << "Triangles: " << countOfTriangles << std::endl;
  std::cout << "Squares: " << countOfSquares << std::endl;
  std::cout << "Rectangles: " << countOfRectangle << std::endl;

  std::cout << "Points: ";
  for (auto &i : points)
  {
    std::cout << '(' << i.x << ';' << i.y << ") ";
  }
  std::cout << std::endl;

  std::cout << "Shapes: " << std::endl;
  for (const auto &shape : shapeContainer)
  {
    std::cout << shape.size();
    for (const auto point : shape)
    {
      std::cout << " (" << point.x << ';' << point.y << ") ";
    }
    std::cout << std::endl;
  }
}


