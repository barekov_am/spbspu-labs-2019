#include <string>
#include <iostream>
#include <exception>
#include <cmath>

#include "shape.hpp"

Shape readPoints(std::string line, std::size_t vertices)
{
  Shape shape;
  std::size_t openingBracket;
  std::size_t semicolon;
  std::size_t closingBracket;
  std::string sep = " \t";
  for (std::size_t i = 0; i < vertices; i++)
  {
    if (line.empty())
    {
      throw std::invalid_argument("Invalid number of vertices");
    }

    while (line.find_first_of(sep) == 0)
    {
      line.erase(0, 1);
    }

    openingBracket = line.find_first_of('(');
    semicolon = line.find_first_of(';');
    closingBracket = line.find_first_of(')');

    if ((openingBracket == std::string::npos) || (semicolon == std::string::npos)
        || (closingBracket == std::string::npos))
    {
      throw std::invalid_argument("Invalid point declaration");
    }

    Point_t point {std::stoi(line.substr(openingBracket + 1, semicolon - openingBracket - 1)),
       std::stoi(line.substr(semicolon + 1, closingBracket - semicolon -1))};

    line.erase(0, closingBracket + 1);

    shape.push_back(point);
  }

  while (line.find_first_of(sep) == 0)
  {
    line.erase(0, 1);
  }

  if (!line.empty())
  {
    throw std::invalid_argument("Too many points");
  }

  return shape;
}

int getSquareDistance(const Point_t &point1, const Point_t &point2)
{
  return (point1.x - point2.x)*(point1.x - point2.x) + (point1.y - point2.y)*(point1.y - point2.y);
}

bool isRectangle(const Shape &shape)
{
  int diagonal1 = getSquareDistance(shape[0], shape[2]);
  int diagonal2 = getSquareDistance(shape[1], shape[3]);

  return diagonal1 == diagonal2;
}

bool isSquare(const Shape &shape)
{
  if (isRectangle(shape))
  {
    int diff1 = getSquareDistance(shape[0], shape[1]);
    int diff2 = getSquareDistance(shape[1], shape[2]);
    int diff3 = getSquareDistance(shape[2], shape[3]);
    int diff4 = getSquareDistance(shape[3], shape[0]);

    if ((diff1 == diff2) && (diff2 == diff3) && (diff3 == diff4) && (diff4 == diff1))
    {
      return true;
    }
  }
  return false;
}
