#include <iostream>
#include <fstream>
#include <memory>
#include <stdexcept>
#include <vector>

const size_t init_size = 8;

void task2 (const char *file)
{
  std::ifstream inp(file);
  if (!inp)
  {
    throw std::ios_base::failure("Cannot open file");
  }

  size_t size = init_size;

  using array_ptr = std::unique_ptr<char[], decltype(&free)>;
  array_ptr arr(static_cast<char *>(malloc(size)), &free);

  if (!arr)
  {
    throw std::runtime_error("Memory cannot be allocated");
  }

  size_t count = 0;

  while (!inp.eof())
  {

    inp.read(&arr[count], init_size);
    count += inp.gcount();

    if (inp.gcount() == init_size)
    {

      size += init_size;
      array_ptr tmp_arr(static_cast<char *>(realloc(arr.get(), size)), &free);

      if (!tmp_arr)
      {
        throw std::runtime_error("Memory cannot be reallocated");
      }

      arr.release();
      std::swap(arr, tmp_arr);

    }

    if (!inp.eof() && inp.fail())
    {
      throw std::runtime_error("Failed reading data");
    }

  }

  inp.close();

  if (inp.is_open())
  {
    throw std::runtime_error("File was not closed");
  }

  std::vector<char>(&arr[0], &arr[count]);

  for(size_t i = 0; i < count; i++)
  {
    std::cout << arr[i];
  }
}

