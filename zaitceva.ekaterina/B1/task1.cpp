#include <forward_list>
#include <vector>
#include <fstream>
#include "details.hpp"

void task1(const char *direction)
{
  bool dir = checkDirection(direction);
  std::vector<int> vector1;
  int num = 0;

  while (std::cin >> num)
  {
    vector1.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Failed input");
  }

  if (vector1.empty())
  {
    return;
  }

  std::vector<int> vector2 = vector1;
  std::forward_list<int> vector3(vector1.begin(), vector1.end());

  sort<Brackets>(vector1, dir);
  sort<At>(vector2, dir);
  sort<Iterator>(vector3, dir);

  print(vector1);
  print(vector2);
  print(vector3);
}
