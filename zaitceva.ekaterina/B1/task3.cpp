#include <vector>
#include <iterator>
#include <iostream>
#include "details.hpp"

void task3()
{
  int num = -1;
  std::vector<int> vector;

  while (std::cin >> num)
  {
    if (num == 0)
    {
      break;
    }
    vector.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Failed input");
  }

  if (vector.empty())
  {
    return;
  }

  if (num != 0)
  {
    throw std::invalid_argument("Sequence must end with zero");
  }

  std::vector<int>::iterator i = vector.begin();
  switch(vector.back())
  {
    case 1:
    {
      while (i != vector.end())
      {
        if ((*i % 2) == 0)
        {
          vector.erase(i);
        }
        else
        {
          ++i;
        }
      }
      break;
    }
    case 2:
    {
      while (i != vector.end())
      {
        if ((*i % 3) == 0)
        {
          i = vector.insert(++i, 3, 1) + 3;
        }
        else
        {
          ++i;
        }
      }
      break;
    }
  }

  print(vector);
}
