#include <iostream>
#include "square.hpp"

Square::Square(const point_t &centre):
  Shape(centre)
{
}

void Square::draw() const
{
  std::cout << "SQUARE (" << centre_.x << ";" << centre_.y << ")" << std::endl;
}
