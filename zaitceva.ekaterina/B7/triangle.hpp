#ifndef B7_TRIANGLE_H
#define B7_TRIANGLE_H

#include "shape.hpp"

class Triangle : public Shape
{
public:
  Triangle(const point_t &);
  void draw() const override;
};

#endif //B7_TRIANGLE_H
