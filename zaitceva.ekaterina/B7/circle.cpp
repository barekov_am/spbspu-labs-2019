#include <iostream>
#include "circle.hpp"

Circle::Circle(const point_t &centre):
  Shape(centre)
{
}

void Circle::draw() const
{
  std::cout << "CIRCLE (" << centre_.x << ";" << centre_.y << ")" << std::endl;
}
