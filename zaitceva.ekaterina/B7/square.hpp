#ifndef B7_SQUARE_H
#define B7_SQUARE_H

#include "shape.hpp"

class Square : public Shape
{
public:
  Square(const point_t &);
  void draw() const override;
};

#endif //B7_SQUARE_H
