#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <cmath>

void task1()
{
  std::vector<double> vector((std::istream_iterator<double>(std::cin)), std::istream_iterator<double>());

  if (!std::cin.eof())
  {
    throw std::runtime_error("Reading is failed\n");
  }

  auto multiplicationByPI = std::bind1st(std::multiplies<double>(), M_PI);
  std::transform(vector.begin(), vector.end(), vector.begin(), multiplicationByPI);
  std::copy(vector.begin(), vector.end(), std::ostream_iterator<double>(std::cout, "\n"));
}
