#include <iostream>
#include "triangle.hpp"

Triangle::Triangle(const point_t &centre):
  Shape(centre)
{
}

void Triangle::draw() const
{
  std::cout << "TRIANGLE (" << centre_.x << ";" << centre_.y << ")" << std::endl;
}
