#include <vector>
#include <memory>
#include <iostream>
#include <sstream>
#include <algorithm>

#include "circle.hpp"
#include "triangle.hpp"
#include "square.hpp"

void task2()
{
  using shapePtr = std::shared_ptr<Shape>;
  std::vector<shapePtr> vector;
  std::string str;

  while (std::getline(std::cin, str))
  {
    str.erase(std::remove_if(str.begin(), str.end(), [](const char sym){ return std::isspace(sym); }), str.end());

    if (str.empty())
    {
      continue;
    }

    size_t openBr = str.find_first_of("(");
    size_t closeBr = str.find_first_of(")");
    size_t semicolon = str.find_first_of(";");

    if ((closeBr == std::string::npos) || (closeBr < semicolon) || (semicolon < openBr) || (openBr == 0))
    {
      throw std::invalid_argument("Invalid input");
    }

    std::string nameShape = str.substr(0, openBr);
    int x = std::stoi(str.substr(openBr + 1, semicolon));
    int y = std::stoi(str.substr(semicolon + 1, closeBr));

    if (nameShape == "TRIANGLE")
    {
      vector.push_back(std::make_shared<Triangle>(Triangle({x, y})));
    }
    else if (nameShape == "CIRCLE")
    {
      vector.push_back(std::make_shared<Circle>(Circle({x, y})));
    }
    else if (nameShape == "SQUARE")
    {
      vector.push_back(std::make_shared<Square>(Square({x, y})));
    }
    else
    {
      throw std::invalid_argument("Invalid name of shape");
    }

  }

  auto print = [](const shapePtr &shape){shape->draw();};

  std::cout << "Original:" << std::endl;
  std::for_each(vector.begin(), vector.end(), print);

  auto sortLeft = [](const shapePtr &shape1, const shapePtr &shape2){return shape1->isMoreLeft(shape2);};

  std::cout << "Left-Right:" << std::endl;
  std::sort(vector.begin(), vector.end(), sortLeft);
  std::for_each(vector.begin(), vector.end(), print);

  std::cout << "Right-Left:" << std::endl;
  std::for_each(vector.rbegin(), vector.rend(), print);

  auto sortUp = [](const shapePtr &shape1, const shapePtr &shape2){return shape1->isUpper(shape2);};

  std::cout << "Top-Bottom:" << std::endl;
  std::sort(vector.begin(), vector.end(), sortUp);
  std::for_each(vector.begin(), vector.end(), print);

  std::cout << "Bottom-Top:" << std::endl;
  std::for_each(vector.rbegin(), vector.rend(), print);
}
