#ifndef B7_SHAPE_H
#define B7_SHAPE_H

#include <memory>

struct point_t
{
  int x, y;
};

class Shape
{
public:
  Shape(const point_t &);
  virtual ~Shape() = default;
  bool isMoreLeft(const std::shared_ptr<Shape> &) const;
  bool isUpper(const std::shared_ptr<Shape> &) const;
  virtual void draw() const = 0;

protected:
  point_t centre_;
};

#endif //B7_SHAPE_H
