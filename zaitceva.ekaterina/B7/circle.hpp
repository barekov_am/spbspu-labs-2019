#ifndef B7_CIRCLE_H
#define B7_CIRCLE_H

#include "shape.hpp"

class Circle : public Shape
{
public:
  Circle(const point_t &);
  void draw() const override;
};

#endif //B7_CIRCLE_H
