#include "shape.hpp"

Shape::Shape(const point_t &centre):
  centre_(centre)
{
}

bool Shape::isMoreLeft(const std::shared_ptr<Shape> &shape) const
{
  return centre_.x < shape->centre_.x;
}

bool Shape::isUpper(const std::shared_ptr<Shape> &shape) const
{
  return centre_.y  > shape->centre_.y;
}

