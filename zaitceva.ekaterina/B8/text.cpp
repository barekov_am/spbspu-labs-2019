#include "text.hpp"
#include <iostream>
#include <sstream>
#include <algorithm>
#include "text.hpp"

const char HYPHEN = '-';
const char PLUS = '+';
const char COMMA = ',';
const char DOT = '.';
const std::size_t WORD_MAX_LEN = 20;
const std::size_t HYPHEN_MAX_LEN = 3;

Text::Text(size_t width):
  text_(),
  outStringWidth_(width)
{}

void Text::readText()
{
  std::string line;

  while (std::cin)
  {
    std::cin >> std::ws;
    char ch = std::cin.get();

    if (std::isalpha(ch))
    {
      text_.push_back(readWord(ch));
    }
    else if ((std::isdigit(ch)) || ((ch == PLUS || ch == HYPHEN) && std::isdigit(std::cin.peek())))
    {
      text_.push_back(readNumber(ch));
    }
    else if (std::ispunct(ch))
    {
      if (ch == HYPHEN)
      {
        text_.push_back(readHyphen(ch));
      }
      else
      {
        text_.push_back(readPunctuation(ch));
      }
    }
  }
}

void Text::formatText()
{
  readText();
  if (text_.empty())
  {
    return;
  }

  if ((std::ispunct(text_[0][0]) && (text_[0][0] != PLUS) && (text_[0][0] != HYPHEN)) || ((text_[0][0] == HYPHEN) && (text_[0][1] == HYPHEN)))
  {
    throw std::invalid_argument("Text can't begin with punctuation");
  }

  if (isNotOnePunctuation(text_))
  {
    throw std::invalid_argument("In the text there can't be 2 or more punctuation symbols nearly");
  }

  std::for_each(text_.begin(), text_.end(), [&](std::string &data)
  {
    if (std::isalpha(data[0]) || std::isdigit(data[0]) || (data[0] == PLUS) || (data[0] == HYPHEN))
    {
      data.insert(0, " ");
    }
  });

  size_t tmpWidth = 0;
  bool isBeginOfLine = true;
  size_t j = text_.size() - 1;
  for (size_t i = 0; i < j; i++)
  {
    if (isBeginOfLine)
    {
      text_[i].erase(0, 1);
      isBeginOfLine = false;
    }

    if ((text_[i + 1][1] == HYPHEN) && (text_[i].size() + text_[i + 1].size() + tmpWidth > outStringWidth_))
    {
      std:: cout << "\n" << text_[i].erase(0, 1);
      tmpWidth = text_[i].size();
      continue;
    }
    else if (ispunct(text_[i + 1][0]) && (text_[i].size() + text_[i + 1].size() + tmpWidth > outStringWidth_))
    {
      std:: cout << "\n" << text_[i].erase(0, 1);
      tmpWidth = text_[i].size();
      continue;
    }

    if ((tmpWidth + text_[i].size()) < outStringWidth_)
    {
      std::cout << text_[i];
      tmpWidth += text_[i].size();
      isBeginOfLine = false;
    }
    else if (((tmpWidth + text_[i].size()) > outStringWidth_))
    {
      std::cout << "\n" << text_[i].erase(0, 1);
      tmpWidth = text_[i].size();
      isBeginOfLine = false;
    }
    else if (((tmpWidth + text_[i].size()) == outStringWidth_))
    {
      std::cout << text_[i] << "\n";
      tmpWidth = 0;
      isBeginOfLine = true;
    }
  }

  if (text_[text_.size() - 1].size() + tmpWidth <= outStringWidth_)
  {
    isBeginOfLine ? std::cout << text_[text_.size() - 1].erase(0, 1) : std::cout << text_.back();
  }
  else if (text_[text_.size() - 1].size() + tmpWidth > outStringWidth_)
  {
    std::cout << "\n" << text_[text_.size() - 1].erase(0, 1);
  }

  std::cout << "\n";
}

std::string readWord(char c)
{
  std::string tmp;
  tmp.push_back(c);
  while (std::isalpha(std::cin.peek()) || (std::cin.peek() == HYPHEN))
  {
    char ch = std::cin.get();
    if (ch == '-' && std::cin.peek() == HYPHEN)
    {
      throw std::invalid_argument("More than one '-' nearly");
    }
    tmp.push_back(ch);
  }

  if (tmp.length() > WORD_MAX_LEN)
  {
    throw std::invalid_argument("Length of word should be less than 20");
  }

  return  tmp;
}

std::string readNumber(char c)
{
  std::string tmp;
  tmp.push_back(c);
  int counterOfDots = 0;
  while (std::isdigit(std::cin.peek()) || (std::cin.peek() == '.'))
  {
    char ch = std::cin.get();
    if (ch == DOT)
    {
      ++counterOfDots;
      if (counterOfDots > 1)
      {
        throw std::invalid_argument("More than 0ne dot in number");
      }
    }
    tmp.push_back(ch);
  }

  if (tmp.length() > WORD_MAX_LEN)
  {
    throw std::invalid_argument("Length of number should be less than 20");
  }

  return tmp;
}

std::string readHyphen(char c)
{
  std::string tmp;
  tmp.push_back(c);
  while (std::cin.peek() == HYPHEN)
  {
    tmp.push_back(std::cin.get());
  }

  if (tmp.length() != HYPHEN_MAX_LEN)
  {
    throw std::invalid_argument("Length of hyphen should no more than 3");
  }

  return tmp;
}

std::string readPunctuation(char c)
{
  std::string tmp;
  tmp.push_back(c);
  return tmp;
}

bool isNotOnePunctuation(std::vector<std::string> &vector)
{
  size_t j = vector.size() - 1;
  for (size_t i = 0; i < j; i++)
  {
    if ((vector[i][0] == COMMA) && (vector[i + 1][0] == HYPHEN))
    {
      vector[i + 1].insert(0, " ");
    }
    else if (ispunct(vector[i][0]) && (ispunct(vector[i + 1][0])))
    {
      return true;
    }
  }
  return false;
}
