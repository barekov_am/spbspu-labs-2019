#include <iostream>

void task();

int main()
{
  try
  {
    task();
  }
  catch (std::exception &exc)
  {
    std::cerr << exc.what();
    return 1;
  }

  return 0;
}
