#ifndef B4_DATASTRUCT_H
#define B4_DATASTRUCT_H

#include <string>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

#endif //B4_DATASTRUCT_H
