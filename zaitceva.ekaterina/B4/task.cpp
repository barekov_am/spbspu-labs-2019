#include <iostream>
#include <vector>
#include <algorithm>
#include "dataStruct.hpp"

DataStruct inputData();
bool checkKey(const DataStruct& data1, const DataStruct& data2);

void task()
{
  std::vector<DataStruct> vector;

  while (std::cin.peek() != EOF)
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input failed");
    }
    vector.push_back(inputData());
  }
  std::sort(vector.begin(), vector.end(), checkKey);

  for (auto i = vector.begin(); i != vector.end(); i++)
  {
    std::cout << i->key1 << "," << i->key2 << "," << i->str << std::endl;
  }
}
