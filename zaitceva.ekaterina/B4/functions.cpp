#include <iostream>
#include "dataStruct.hpp"

const int MAX_KEY = 5;
const int MIN_KEY = -5;

DataStruct inputData()
{
  std::string line;
  std::getline(std::cin, line);

  int key[2] = {0};
  for (int i = 0; i < 2; i++)
  {
    size_t ind = line.find_first_of(',');

    if (ind == std::string::npos)
    {
      throw std::invalid_argument("Invalid data");
    }

    key[i] = std::stoi(line.substr(0, ind));

    if ((key[i] > MAX_KEY) || (key[i] < MIN_KEY))
    {
      throw std::invalid_argument("Invalid key");
    }

    line = line.erase(0, ind + 1);
  }

  while (line.find_first_of(' ') == 0)
  {
    line.erase(0, 1);
  }

  if (line.empty())
  {
    throw std::invalid_argument("Invalid data");
  }

  return { key[0], key[1], line };
}

bool checkKey(const DataStruct& data1, const DataStruct& data2)
{
  if (data1.key1 < data2.key1)
  {
    return true;
  }
  if (data1.key1 == data2.key1)
  {
    if (data1.key2 < data2.key2)
    {
      return true;
    }
    if (data1.key2 == data2.key2)
    {
      if (data1.str.size() < data2.str.size())
      {
        return true;
      }
    }
  }
  return false;
}
