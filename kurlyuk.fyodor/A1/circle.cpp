#include "circle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>

Circle::Circle(double r, const point_t &dot):
  centre_(dot),
  r_(r)
{
  if (r_ <= 0.0)
  {
    throw std::invalid_argument("Radius is incorrect.");
  }
}

double Circle::getArea() const
{
  return M_PI * r_ * r_;
}

rectangle_t Circle::getFrameRect() const
{
  return {2 * r_, 2 * r_, centre_};
}

void Circle::move(const point_t &dot)
{
  centre_ = dot;
}

void Circle::move(double dX, double dY)
{
  centre_.x += dX;
  centre_.y += dY;
}

void Circle::writeInfo() const
{
  std::cout << "Center coordinates (" << centre_.x << ","
      << centre_.y << ")" << '\n';
  std::cout << "Area: " << getArea() << '\n';
  std::cout << "Frame's height: " << getFrameRect().height << '\n';
  std::cout << "Frame's width: " << getFrameRect().width << '\n';
  std::cout << "Frame's centre coordinates: (" << getFrameRect().pos.x << "," 
      << getFrameRect().pos.y << ")" << "\n\n";
}

