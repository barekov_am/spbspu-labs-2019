#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

int main()
{
  const double dX = 2.1;
  const double dY = 3.3;
  const point_t dot = {30, 20};

  std::cout << "   ___ ___ ___  ___ _    ___ " << '\n';
  std::cout << "  / __|_ _| _ \\/ __| |  | __|" << '\n';
  std::cout << " | (__ | ||   / (__| |__| _| " << '\n';
  std::cout << "  \\___|___|_|_\\\\___|____|___|" << '\n';

  Circle circle(2.0, {0, 0});
  Shape *ptrShape = &circle;
  ptrShape->writeInfo();
  std::cout << "> Moving along axes. dX: " << dX << " dY: " << dY << '\n';
  ptrShape->move(dX, dY);
  ptrShape->writeInfo();
  std::cout << "> Moving to a point. X: " << dot.x << " Y: " << dot.y << '\n';
  ptrShape->move(dot);
  ptrShape->writeInfo();

  std::cout << "  ___ ___ ___ _____ _   _  _  ___ _    ___" << '\n';
  std::cout << " | _ \\ __/ __|_   _/_\\ | \\| |/ __| |  | __|" << '\n';
  std::cout << " |   / _| (__  | |/ _ \\| .` | (_ | |__| _| " << '\n';
  std::cout << " |_|_\\___\\___| |_/_/ \\_\\_|\\_|\\___|____|___|" << '\n';

  Rectangle rectangle({3.0, 2.0, {0, 0}});
  ptrShape = &rectangle;
  ptrShape->writeInfo();
  std::cout << "> Moving along axes. dX: " << dX << " dY: " << dY << '\n';
  ptrShape->move(dX, dY);
  ptrShape->writeInfo();
  std::cout << "> Moving to a point. X: " << dot.x << " Y: " << dot.y << '\n';
  ptrShape->move(dot);
  ptrShape->writeInfo();

  std::cout << "  _____ ___ ___   _   _  _  ___ _    ___ " << '\n';
  std::cout << " |_   _| _ \\_ _| /_\\ | \\| |/ __| |  | __|" << '\n';
  std::cout << "   | | |   /| | / _ \\| .` | (_ | |__| _| " << '\n';
  std::cout << "   |_| |_|_\\___/_/ \\_\\_|\\_|\\___|____|___|" << '\n';

  Triangle triangle({1, 1}, {2, 2}, {6, 4});
  ptrShape = &triangle;
  ptrShape->writeInfo();
  std::cout << "> Moving along axes. dX: " << dX << " dY: " << dY << '\n';
  ptrShape->move(dX, dY);
  ptrShape->writeInfo();
  std::cout << "> Moving to a point. X: " << dot.x << " Y: " << dot.y << '\n';
  ptrShape->move(dot);
  ptrShape->writeInfo();

  std::cout << "  ___  ___  _ __   _____  ___  _  _" << '\n';
  std::cout << " | _ \\/ _ \\| |\\ \\ / / __|/ _ \\| \\| |" << '\n';
  std::cout << " |  _/ (_) | |_\\ V / (_ | (_) | .` |" << '\n';
  std::cout << " |_|  \\___/|____|_| \\___|\\___/|_|\\_|" << '\n';
 
  const point_t points[] = {{2, 2}, {6, 2}, {7, 3}, {7, 6}, {3, 6}, {1, 4}};
  const size_t size = sizeof(points) / sizeof(point_t); 
  std::unique_ptr<point_t[]> pointArray = std::make_unique<point_t[]>(size);

  for (size_t i = 0; i < size; i++)
  {
    pointArray[i] = points[i];
  }

  Polygon polygon(6, pointArray);
  ptrShape = &polygon;
  ptrShape->writeInfo();
  std::cout << "> Moving along axes. dX: " << dX << " dY: " << dY << '\n';
  ptrShape->move(dX, dY);
  ptrShape->writeInfo();
  std::cout << "> Moving to a point. X: " << dot.x << " Y: " << dot.y << '\n';
  ptrShape->move(dot);
  ptrShape->writeInfo();

  return 0;
}
