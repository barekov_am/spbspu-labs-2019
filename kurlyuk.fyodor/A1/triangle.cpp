#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <cmath>

Triangle::Triangle(const point_t &a, const point_t &b, const point_t &c) :
  a_(a),
  b_(b),
  c_(c)
{
  if (getArea() <= 0)
  {
    throw std::invalid_argument("Triangle is incorrect.");
  }
}

double Triangle::getArea() const
{
  return std::fabs((b_.x - a_.x) * (c_.y - a_.y) - (c_.x - a_.x) * (b_.y - a_.y)) / 2;
}

rectangle_t Triangle::getFrameRect() const
{
  const point_t min = {std::min(std::min(a_.x, b_.x), c_.x), std::min(std::min(a_.y, b_.y), c_.y)};
  const point_t max = {std::max(std::max(a_.x, b_.x), c_.x), std::max(std::max(a_.y, b_.y), c_.y)};
  
  return {max.x - min.x, max.y - min.y, {(max.x + min.x) / 2, (max.y + min.y) / 2}};  
}

void Triangle::move(const point_t &dot)
{
  move(dot.x - getCenter().x, dot.y - getCenter().y);
}

void Triangle::move(double dX, double dY)
{
  a_.x += dX;
  b_.x += dX;
  c_.x += dX;
  a_.y += dY;
  b_.y += dY;
  c_.y += dY;
}

point_t Triangle::getCenter() const
{
  return {(a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3};
}

void Triangle::writeInfo() const
{
  std::cout << "Center coordinates (" << getCenter().x << ","
      << getCenter().y << ")" << '\n';
  std::cout << "Area: " << getArea() << '\n';
  std::cout << "Frame's height: " << getFrameRect().height << '\n';
  std::cout << "Frame's width: " << getFrameRect().width << '\n';
  std::cout << "Frame's centre coordinates: (" << getFrameRect().pos.x << "," 
      << getFrameRect().pos.y << ")" << "\n\n";
}
