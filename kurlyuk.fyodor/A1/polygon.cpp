#include "polygon.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <cmath>
#include "triangle.hpp"

Polygon::Polygon(const Polygon &rhs) :
  count_(rhs.count_),
  vertices_(std::make_unique<point_t[]>(rhs.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    vertices_[i] = rhs.vertices_[i];
  }
}

Polygon::Polygon(Polygon &&rhs) :
  count_(rhs.count_),
  vertices_(std::move(rhs.vertices_))
{ }

Polygon::Polygon(size_t count, const point_array &vertices) :
  count_(count),
  vertices_(std::make_unique<point_t[]>(count))
{
  if ((vertices == nullptr) || (count <= 3))
  {
    throw std::invalid_argument("Polygon is incorrect.");
  }
  
  for (size_t i = 0; i < count_; i++)
  {
    vertices_[i] = vertices[i];
  }

  if (!convexity())
  {
    throw std::logic_error("Polygon must be convex.");
  }
  if (getArea() == 0)
  {
    throw std::logic_error("Polygon must have an area more than 0.");
  }
}

Polygon &Polygon::operator =(const Polygon &rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    point_array temp(std::make_unique<point_t[]>(rhs.count_));

    for (size_t i = 0; i < count_; i++)
    {
      vertices_[i] = rhs.vertices_[i];
    }

    vertices_.swap(temp);
  }

  return *this;
}

Polygon &Polygon::operator =(Polygon &&rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    vertices_ = std::move(rhs.vertices_);
  }

  return *this;
}

double Polygon::getArea() const
{
  double area = 0.0;
  for (size_t i = 1; i < count_ - 1; i++)
  {
    Triangle triangle({vertices_[0], vertices_[i], vertices_[i + 1]});
    area += triangle.getArea();
  }
  return area;
}

rectangle_t Polygon::getFrameRect() const
{
  point_t max = {vertices_[0].x, vertices_[0].y};
  point_t min = {vertices_[0].x, vertices_[0].y};
  for (size_t i = 1; i < count_ - 1; i++)
  {
    max = {std::max(max.x, vertices_[i].x), std::max(max.y, vertices_[i].y)};
    min = {std::min(min.x, vertices_[i].x), std::min(min.y, vertices_[i].y)};
  }
  return {max.x - min.x, max.y - min.y, {(max.x + min.x) / 2, (max.y + min.y) / 2}};  
}

void Polygon::move(const point_t &dot)
{
  move(dot.x - getCenter().x, dot.y - getCenter().y);
}

void Polygon::move(double dX, double dY)
{
  for (size_t i = 0; i < count_; i++)
  {
    vertices_[i].x += dX;
    vertices_[i].y += dY;
  }
}

point_t Polygon::getCenter() const
{
  double x = 0.0;
  double y = 0.0;
  for (size_t i = 0; i < count_; i++)
  {
    x += vertices_[i].x;
    y += vertices_[i].y;
  }
  return {x / count_, y / count_};
}

void Polygon::writeInfo() const
{
  std::cout << "Center coordinates (" << getCenter().x << ","
      << getCenter().y << ")" << '\n';
  std::cout << "Area: " << getArea() << '\n';
  std::cout << "Frame's height: " << getFrameRect().height << '\n';
  std::cout << "Frame's width: " << getFrameRect().width << '\n';
  std::cout << "Frame's centre coordinates: (" << getFrameRect().pos.x << "," 
      << getFrameRect().pos.y << ")" << "\n\n";
}

bool Polygon::convexity() const
{
  point_t ab = {vertices_[0].x - vertices_[count_ - 1].x, vertices_[0].y - vertices_[count_ - 1].y};
  point_t bc = {vertices_[1].x - vertices_[0].x, vertices_[1].y - vertices_[0].y};
  bool convex = (ab.x * bc.y - ab.y * bc.x) > 0;
  for (size_t i = 1; i < count_ - 1; i++)
  {
    ab = {vertices_[i].x - vertices_[i - 1].x, vertices_[i].y - vertices_[i - 1].y};
    bc = {vertices_[i + 1].x - vertices_[i].x, vertices_[i + 1].y - vertices_[i].y};
    if (convex != ((ab.x * bc.y - ab.y * bc.x) > 0))
    {
      return false;
    }
  }

  return true;
}
