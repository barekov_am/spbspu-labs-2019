#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

class Rectangle: public Shape
{
public:
  Rectangle(const rectangle_t &rect);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &dot) override;
  void move(double dX, double dY) override;
  void writeInfo() const override;
private:
  rectangle_t rect_;
};

#endif

