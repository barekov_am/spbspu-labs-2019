#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>

Rectangle::Rectangle(const rectangle_t &rect) :
  rect_(rect)
{
  if ((rect_.width <= 0.0) || (rect_.height <= 0.0))
  {
    throw std::invalid_argument("Rectangle dimensions are incorrect.");
  }
}

double Rectangle::getArea() const
{
  return rect_.width * rect_.height;
}

rectangle_t Rectangle::getFrameRect() const
{
  return rect_;
}

void Rectangle::move(const point_t &dot)
{
  rect_.pos = dot;
}

void Rectangle::move(double dX, double dY)
{
  rect_.pos.x += dX;
  rect_.pos.y += dY;
}

void Rectangle::writeInfo() const
{
  std::cout << "Center coordinates (" << rect_.pos.x << ","
      << rect_.pos.y << ")" << '\n';
  std::cout << "Area: " << getArea() << '\n';
  std::cout << "Frame's height: " << getFrameRect().height << '\n';
  std::cout << "Frame's width: " << getFrameRect().width << '\n';
  std::cout << "Frame's centre coordinates: (" << getFrameRect().pos.x << "," 
      << getFrameRect().pos.y << ")" << "\n\n";
}

