#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

class Circle: public Shape
{
public:
  Circle(double r, const point_t &dot);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &dot) override;
  void move(double dX, double dY) override;
  void writeInfo() const override;
private:
  point_t centre_;
  double r_;
};

#endif

