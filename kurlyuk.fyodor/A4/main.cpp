#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

void showParam(kurlyuk::Shape *shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer is nullptr");
  }
  std::cout << "\nArea of Shape = "
            << shape->getArea()
            << "\nFrame: " << "\n";

  kurlyuk::rectangle_t frame = shape->getFrameRect();

  std::cout << "\nHeight = " << frame.height
            << "\nWidth = " << frame.width
            << "\nX Point of Center: " << frame.pos.x
            << "\nY Point of Center: " << frame.pos.y << "\n\n";

  std::cout << ">> Происходит: shape->move(2, 7) <<" << "\n";
  shape->move(2, 7);
  shape->writeInfo();

  std::cout << ">> Происходит: shape->move({ 2, 7 }) <<" << "\n";
  shape->move({ 2, 7 });
  shape->writeInfo();

  std::cout << ">> Происходит: shape->scale(2) <<" << "\n";
  shape->scale(2);
  shape->writeInfo();

  std::cout << ">> Происходит: shape->scale(0.5) <<" << "\n";
  shape->scale(0.5);
  shape->writeInfo();

  std::cout << ">> Происходит: shape->rotate(45) <<" << "\n";
  shape->rotate(45);
  shape->writeInfo();

  std::cout << ">> Происходит: shape->rotate(-45) <<" << "\n";
  shape->rotate(-45);
  shape->writeInfo();

  std::cout << "\n";
}

int main()
{
  std::cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n# Circle" << '\n';

  kurlyuk::Circle circle(6, { 1, 2 });
  showParam(&circle);

  std::cout << "# Rectangle" << '\n';

  kurlyuk::Rectangle rectangle({7, 9, { 4, 5 }});
  showParam(&rectangle);

  std::cout << "# Triangle" << '\n';

  kurlyuk::Triangle triangle({ 4, 5 }, { 2, 3 }, { 7, 3 });
  showParam(&triangle);

  std::cout << "# Polygon" << '\n';

  size_t quantity = 6;

  kurlyuk::point_t vertices[quantity]
      = { { -2, -2 }, { -1, 4 }, { 3, 8 }, { 6, 8 }, { 8, 7 }, { 3, 0 } };

  kurlyuk::Polygon polygon(quantity, vertices);
  showParam(&polygon);

  std::cout << "# CompositeShape" << '\n';
                                               
  // Creating composite shape
  kurlyuk::CompositeShape composite;

  kurlyuk::CompositeShape::pointer circle_
      = std::make_shared<kurlyuk::Circle>(circle);
  composite.add(circle_);

  kurlyuk::CompositeShape::pointer polygon_
      = std::make_shared<kurlyuk::Polygon>(polygon);
  composite.add(polygon_);

  kurlyuk::CompositeShape::pointer triangle_
      = std::make_shared<kurlyuk::Triangle>(triangle);
  composite.add(triangle_);

  kurlyuk::CompositeShape::pointer rectangle_
      = std::make_shared<kurlyuk::Rectangle>(rectangle);
  composite.add(rectangle_);

  showParam(&composite);

  composite.remove(3);
  showParam(&composite);

  return 0;
}
