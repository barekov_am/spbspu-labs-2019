#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

namespace kurlyuk
{
  class Triangle: public Shape
  {
  public:
    Triangle(const point_t &a, const point_t &b, const point_t &c);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &dot) override;
    void move(double dX, double dY) override;
    void scale(double scaleFactor) override;
    void rotate(double angle) override;
    void writeInfo() const override;

    point_t getCenter() const;
  private:
    void rotatePoint(point_t &dot, const point_t &center, const double &cos, const double &sin);
    point_t a_;
    point_t b_;
    point_t c_;
  };
}

#endif

