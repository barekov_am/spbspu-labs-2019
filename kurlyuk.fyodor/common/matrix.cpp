#include "matrix.hpp"

#include <stdexcept>
#include <utility>

kurlyuk::Matrix::Matrix() :
  rows_(0),
  columns_(0)
{ }

kurlyuk::Matrix::Matrix(const Matrix &other) :
  rows_(other.rows_),
  columns_(other.columns_),
  list_(std::make_unique<pointer[]>(other.rows_ * other.columns_))
{
  for (unsigned int i = 0; i < (rows_ * columns_); i++)
  {
    list_[i] = other.list_[i];
  }
}

kurlyuk::Matrix::Matrix(Matrix &&other) noexcept :
  rows_(other.rows_),
  columns_(other.columns_),
  list_(std::move(other.list_))
{ }

kurlyuk::Matrix &kurlyuk::Matrix::operator =(const Matrix &other)
{
  if (this != &other)
  {
    rows_ = other.rows_;
    columns_ = other.columns_;
    array tmpList(std::make_unique<pointer[]>(other.rows_ *other.columns_));
    for (unsigned int i = 0; i < (rows_ * columns_); i++)
    {
      tmpList[i] = other.list_[i];
    }
    list_.swap(tmpList);
  }

  return *this;
}

kurlyuk::Matrix &kurlyuk::Matrix::operator =(Matrix &&other) noexcept
{
  if (this != &other)
  {
    rows_ = other.rows_;
    columns_ = other.columns_;
    list_ = std::move(other.list_);
  }

  return *this;
}

kurlyuk::Matrix::array kurlyuk::Matrix::operator [](unsigned int index) const
{
  if (index >= rows_)
  {
    throw std::invalid_argument("Index is out of range");
  }

  array tmpList(std::make_unique<pointer[]>(columns_));
  for (unsigned int i = 0; i < columns_; i++)
  {
    tmpList[i] = list_[index * columns_ + i];
  }

  return tmpList;
}

bool kurlyuk::Matrix::operator ==(const Matrix &other) const
{
  if ((rows_ != other.rows_) || (columns_ != other.columns_))
  {
    return false;
  }

  for (unsigned int i = 0; i < (rows_ * columns_); i++)
  {
    if (list_[i] != other.list_[i])
    {
      return false;
    }
  }

  return true;
}

bool kurlyuk::Matrix::operator !=(const Matrix &other) const
{
  return !(*this == other);
}

void kurlyuk::Matrix::add(pointer shape, unsigned int row, unsigned int column)
{
  unsigned int tmpRows = (row == rows_) ? (rows_ + 1) : (rows_);
  unsigned int tmpColumns = (column == columns_) ? (columns_ + 1) : (columns_);

  array tmpList(std::make_unique<pointer[]>(tmpRows * tmpColumns));

  for (unsigned int i = 0; i < rows_; i++)
  {
    for (unsigned int j = 0; j < columns_; j++)
    {
      tmpList[i * tmpColumns + j] = list_[i * columns_ + j];
    }
  }

  tmpList[row * tmpColumns + column] = shape;
  list_.swap(tmpList);
  rows_ = tmpRows;
  columns_ = tmpColumns;
}

unsigned int kurlyuk::Matrix::getRows() const
{
  return rows_;
}

unsigned int kurlyuk::Matrix::getColumns() const
{
  return columns_;
}

unsigned int kurlyuk::Matrix::getSizeLayer(unsigned int row) const
{
  if (rows_ <= row)
  {
    return 0;
  }

  for (unsigned int i = 0; i < columns_; i++)
  {
    if (list_[row * columns_ + i] == nullptr)
    {
      return i;
    }
  }
  
  return columns_;
}

