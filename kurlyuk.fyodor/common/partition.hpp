#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

using pointer = std::shared_ptr<kurlyuk::Shape>;
using array = std::unique_ptr<pointer[]>;

namespace kurlyuk
{
  Matrix part(const array &arr, unsigned int size);
  Matrix part(const CompositeShape &composite);
}

#endif

