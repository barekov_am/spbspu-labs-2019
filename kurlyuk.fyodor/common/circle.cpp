#include "circle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>

kurlyuk::Circle::Circle(double r, const point_t &dot):
  center_(dot),
  r_(r)
{
  if (r_ <= 0.0)
  {
    throw std::invalid_argument("Radius is incorrect.");
  }
}

double kurlyuk::Circle::getArea() const
{
  return M_PI * r_ * r_;
}

kurlyuk::rectangle_t kurlyuk::Circle::getFrameRect() const
{
  return {2 * r_, 2 * r_, center_};
}

void kurlyuk::Circle::move(const point_t &dot)
{
  center_ = dot;
}

void kurlyuk::Circle::move(double dX, double dY)
{
  center_.x += dX;
  center_.y += dY;
}

void kurlyuk::Circle::scale(double scaleFactor)
{
  if (scaleFactor <= 0.0)
  {
    throw std::invalid_argument("Invalid scale factor.");
  }

  r_ *= scaleFactor;
}

void kurlyuk::Circle::rotate(double)
{}

void kurlyuk::Circle::writeInfo() const
{
  const rectangle_t frameRect = getFrameRect();
  std::cout << "Center coordinates (" << center_.x << ","
      << center_.y << ")" << '\n';
  std::cout << "Area: " << getArea() << '\n';
  std::cout << "Frame's height: " << frameRect.height << '\n';
  std::cout << "Frame's width: " << frameRect.width << '\n';
  std::cout << "Frame's centre coordinates: (" << frameRect.pos.x << "," 
      << frameRect.pos.y << ")" << "\n\n";
}

