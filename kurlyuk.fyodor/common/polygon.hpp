#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <memory>
#include "shape.hpp"

namespace kurlyuk
{
  class Polygon: public Shape
  {
  public:
    using point_array = std::unique_ptr<point_t[]>;

    Polygon(const Polygon &rhs);
    Polygon(Polygon &&rhs);
    Polygon(size_t count, point_t *vertices);
    ~Polygon() = default;

    Polygon &operator =(const Polygon &rhs);
    Polygon &operator =(Polygon &&rhs);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &dot) override;
    void move(double dX, double dY) override;
    void scale(double scaleFactor) override;
    void rotate(double angle) override;
    void writeInfo() const override;

    point_t getCenter() const;
  private:
    size_t count_;
    point_array vertices_;

    bool isConvex() const;
  };
}

#endif

