#include <boost/test/auto_unit_test.hpp>

#include "matrix.hpp"
#include "partition.hpp"
#include "rectangle.hpp"
#include "polygon.hpp"
#include "triangle.hpp"
#include "circle.hpp"

using pointer = std::shared_ptr<kurlyuk::Shape>;

BOOST_AUTO_TEST_SUITE(partitionTestSuite)

  BOOST_AUTO_TEST_CASE(correctPartition)
  {
    kurlyuk::Circle circle(1, { 3, 8 });
    kurlyuk::CompositeShape::pointer circle_
        = std::make_shared<kurlyuk::Circle>(circle);

    kurlyuk::CompositeShape composite(circle_);

    kurlyuk::Triangle triangle({ 4, 4 }, { 6, 4 }, { 4, 6 });
    kurlyuk::CompositeShape::pointer triangle_
        = std::make_shared<kurlyuk::Triangle>(triangle);

    composite.add(triangle_);

    kurlyuk::Rectangle rectangle(kurlyuk::rectangle_t{2, 2, { 3, 3 }});
    kurlyuk::CompositeShape::pointer rectangle_
        = std::make_shared<kurlyuk::Rectangle>(rectangle);

    composite.add(rectangle_);


    const size_t quantity = 4;
    kurlyuk::point_t vertices[quantity]
        = { { -5, -6 }, { -6, 5 }, { -5, 4 }, { 4, -5 } };
    kurlyuk::Polygon polygon(quantity, vertices);
    kurlyuk::CompositeShape::pointer polygon_
        = std::make_shared<kurlyuk::Polygon>(polygon);

    composite.add(polygon_);

    kurlyuk::Matrix testMatrix = kurlyuk::part(composite);

    BOOST_CHECK_EQUAL(testMatrix.getRows(), 2);
    BOOST_CHECK_EQUAL(testMatrix.getColumns(), 3);

    BOOST_CHECK(testMatrix[0][0] == circle_);
    BOOST_CHECK(testMatrix[0][1] == triangle_);
    BOOST_CHECK(testMatrix[0][2] == rectangle_);
    BOOST_CHECK(testMatrix[1][0] == polygon_);
  }

BOOST_AUTO_TEST_SUITE_END()

