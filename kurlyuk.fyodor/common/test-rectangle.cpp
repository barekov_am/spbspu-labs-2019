#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "rectangle.hpp"

const double ErrorValue = 1e-10;

BOOST_AUTO_TEST_SUITE(rectangleTestSuite)

BOOST_AUTO_TEST_CASE(invariabilityAfterMoving)
{
  kurlyuk::Rectangle testRectangle({3.0, 2.0, { 0, 0 }});
  const kurlyuk::rectangle_t frameRectBefore = testRectangle.getFrameRect();
  const double areaBefore = testRectangle.getArea();

  testRectangle.move({1.9, 1.9});
  BOOST_CHECK_CLOSE(frameRectBefore.width, testRectangle.getFrameRect().width, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectBefore.height, testRectangle.getFrameRect().height, ErrorValue);
  BOOST_CHECK_CLOSE(areaBefore, testRectangle.getArea(), ErrorValue);

  testRectangle.move(5, -7);
  BOOST_CHECK_CLOSE(frameRectBefore.width, testRectangle.getFrameRect().width, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectBefore.height, testRectangle.getFrameRect().height, ErrorValue);
  BOOST_CHECK_CLOSE(areaBefore, testRectangle.getArea(), ErrorValue);
}

BOOST_AUTO_TEST_CASE(areaAfterScaling)
{
  kurlyuk::Rectangle testRectangle({3.0, 2.0, { 0, 0 }});
  double areaBefore = testRectangle.getArea();

  double scaleFactor = 2.75;
  testRectangle.scale(scaleFactor);
  BOOST_CHECK_CLOSE(testRectangle.getArea(), scaleFactor * scaleFactor * areaBefore, ErrorValue);
  areaBefore = testRectangle.getArea();

  scaleFactor = 0.75;
  testRectangle.scale(scaleFactor);
  BOOST_CHECK_CLOSE(testRectangle.getArea(), scaleFactor * scaleFactor * areaBefore, ErrorValue);
}

BOOST_AUTO_TEST_CASE(testRectangleRotation)
{
  kurlyuk::Rectangle testRectangle({3.0, 4.0, { 0, 0 }});
  const double areaBefore = testRectangle.getArea();
  const kurlyuk::rectangle_t frameRectBefore = testRectangle.getFrameRect();

  double angle = -30;
  testRectangle.rotate(angle);

  double areaAfter = testRectangle.getArea();

  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ErrorValue);

  angle = 30;
  testRectangle.rotate(angle);

  kurlyuk::rectangle_t frameRectAfter = testRectangle.getFrameRect();
  areaAfter = testRectangle.getArea();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, ErrorValue);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ErrorValue);
}


BOOST_AUTO_TEST_CASE(exceptionThrow)
{
  BOOST_CHECK_THROW(kurlyuk::Rectangle({-3.0, 2.0, { 0, 0 }}), std::invalid_argument);

  kurlyuk::Rectangle testRectangle({3.0, 2.0, { 0, 0 }});
  BOOST_CHECK_THROW(testRectangle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

