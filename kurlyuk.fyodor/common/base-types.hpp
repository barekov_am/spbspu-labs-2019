#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace kurlyuk
{
  struct point_t
  {
    bool operator ==(const point_t &other) const;
    bool operator !=(const point_t &other) const;
    double x;
    double y;
  };

  struct rectangle_t
  {
    bool operator ==(const rectangle_t &other) const;
    bool operator !=(const rectangle_t &other) const;
    double width;
    double height;
    point_t pos;
  };

  bool intersect(const rectangle_t &lhs, const rectangle_t &rhs);
}

#endif //BASE_TYPES_HPP
