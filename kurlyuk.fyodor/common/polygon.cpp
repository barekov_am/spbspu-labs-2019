#include "polygon.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <cmath>
#include "triangle.hpp"

kurlyuk::Polygon::Polygon(const Polygon &rhs) :
  count_(rhs.count_),
  vertices_(std::make_unique<point_t[]>(rhs.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    vertices_[i] = rhs.vertices_[i];
  }
}

kurlyuk::Polygon::Polygon(Polygon &&rhs) :
  count_(rhs.count_),
  vertices_(std::move(rhs.vertices_))
{
  rhs.count_ = 0;
}

kurlyuk::Polygon::Polygon(size_t count, point_t *vertices) :
  count_(count),
  vertices_(std::make_unique<point_t[]>(count))
{
  if ((vertices == nullptr) || (count <= 3))
  {
    throw std::invalid_argument("Polygon is incorrect.");
  }

  for (size_t i = 0; i < count_; i++)
  {
    vertices_[i] = vertices[i];
  }

  if (!isConvex())
  {
    throw std::logic_error("Polygon must be convex.");
  }
  if (getArea() == 0)
  {
    throw std::logic_error("Polygon must have an area more than 0.");
  }
}

kurlyuk::Polygon &kurlyuk::Polygon::operator =(const Polygon &rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    point_array temp(std::make_unique<point_t[]>(rhs.count_));

    for (size_t i = 0; i < count_; i++)
    {
      vertices_[i] = rhs.vertices_[i];
    }

    vertices_.swap(temp);
  }

  return *this;
}

kurlyuk::Polygon &kurlyuk::Polygon::operator =(Polygon &&rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    vertices_ = std::move(rhs.vertices_);
    rhs.count_ = 0;
  }

  return *this;
}

double kurlyuk::Polygon::getArea() const
{
  double area = 0.0;
  for (size_t i = 1; i < count_ - 1; i++)
  {
    Triangle triangle({vertices_[0], vertices_[i], vertices_[i + 1]});
    area += triangle.getArea();
  }
  return area;
}

kurlyuk::rectangle_t kurlyuk::Polygon::getFrameRect() const
{
  point_t max = {vertices_[0].x, vertices_[0].y};
  point_t min = {vertices_[0].x, vertices_[0].y};
  for (size_t i = 1; i < count_; i++)
  {
    max = {std::max(max.x, vertices_[i].x), std::max(max.y, vertices_[i].y)};
    min = {std::min(min.x, vertices_[i].x), std::min(min.y, vertices_[i].y)};
  }
  point_t center = { min.x + (max.x - min.x) / 2, min.y + (max.y - min.y) / 2 };
  return {max.x - min.x, max.y - min.y, center};
}

void kurlyuk::Polygon::move(const point_t &dot)
{
  const point_t center = getCenter();
  move(dot.x - center.x, dot.y - center.y);
}

void kurlyuk::Polygon::move(double dX, double dY)
{
  for (size_t i = 0; i < count_; i++)
  {
    vertices_[i].x += dX;
    vertices_[i].y += dY;
  }
}

kurlyuk::point_t kurlyuk::Polygon::getCenter() const
{
  double x = 0.0;
  double y = 0.0;
  for (size_t i = 0; i < count_; i++)
  {
    x += vertices_[i].x;
    y += vertices_[i].y;
  }
  return {x / count_, y / count_};
}

void kurlyuk::Polygon::scale(double scaleFactor)
{
  if (scaleFactor <= 0.0)
  {
    throw std::invalid_argument("Invalid scale factor.");
  }

  const point_t centre = getCenter();
  for (size_t i = 0; i < count_; i++)
  {
    vertices_[i].x += (vertices_[i].x - centre.x) * (scaleFactor - 1);;
    vertices_[i].y += (vertices_[i].y - centre.y) * (scaleFactor - 1);
  }
}

void kurlyuk::Polygon::rotate(double angle)
{
  const double cos = std::cos(M_PI * angle / 180);
  const double sin = std::sin(M_PI * angle / 180);

  const point_t center = getCenter();

  for (size_t i = 0; i < count_; i++)
  {
    vertices_[i]
        = { center.x + (vertices_[i].x - center.x) * cos - (vertices_[i].y - center.y) * sin,
            center.y + (vertices_[i].y - center.y) * cos + (vertices_[i].x - center.x) * sin };
  }

}

void kurlyuk::Polygon::writeInfo() const
{
  const point_t center = getCenter();
  const rectangle_t frameRect = getFrameRect();
  std::cout << "Center coordinates (" << center.x << ","
      << center.y << ")" << '\n';
  std::cout << "Vertices array: " << '\n';
  for (size_t i = 0; i < count_; i++)
  {
    std::cout << "Verctice # " << i << ". x: " << vertices_[i].x << " y: " << vertices_[i].y << "\n";
  }

  std::cout << "\nPaste this straight into Input on https://www.geogebra.org/classic to see the shape\n";
  std::cout << "Execute[{";

  for (size_t i = 0; i < count_; i++)
  {
    std::cout << "\"(" << vertices_[i].x << ", " << vertices_[i].y << ")\", ";
  }

  std::cout << "\"Polygon({(" << vertices_[0].x << ", " << vertices_[0].y << ")";

  for (size_t i = 1; i < count_; i++)
  {
    std::cout << ", (" << vertices_[i].x << ", " << vertices_[i].y << ")";
  }

  std::cout << "})\"}]\n\n";
  std::cout << "Area: " << getArea() << '\n';
  std::cout << "Frame's height: " << frameRect.height << '\n';
  std::cout << "Frame's width: " << frameRect.width << '\n';
  std::cout << "Frame's centre coordinates: (" << frameRect.pos.x << ","
      << frameRect.pos.y << ")" << "\n\n";
}

bool kurlyuk::Polygon::isConvex() const
{
  point_t ab = {vertices_[0].x - vertices_[count_ - 1].x, vertices_[0].y - vertices_[count_ - 1].y};
  point_t bc = {vertices_[1].x - vertices_[0].x, vertices_[1].y - vertices_[0].y};
  bool convex = (ab.x * bc.y - ab.y * bc.x) > 0;
  for (size_t i = 1; i < count_ - 1; i++)
  {
    ab = {vertices_[i].x - vertices_[i - 1].x, vertices_[i].y - vertices_[i - 1].y};
    bc = {vertices_[i + 1].x - vertices_[i].x, vertices_[i + 1].y - vertices_[i].y};
    if (convex != ((ab.x * bc.y - ab.y * bc.x) > 0))
    {
      return false;
    }
  }

  return true;
}
