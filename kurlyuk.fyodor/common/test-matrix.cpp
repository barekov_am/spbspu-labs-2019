#include <stdexcept>
#include <utility>

#include <boost/test/auto_unit_test.hpp>

#include "polygon.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

using pointer = std::shared_ptr<kurlyuk::Shape>;

BOOST_AUTO_TEST_SUITE(matrixTestSuite)

  BOOST_AUTO_TEST_CASE(copyAndMove)
  {
    pointer circle = std::make_shared<kurlyuk::Circle>(3, kurlyuk::point_t{ 1, 2 });
    kurlyuk::CompositeShape composite;
    kurlyuk::CompositeShape::pointer triangle_
        = std::make_shared<kurlyuk::Triangle>(kurlyuk::Triangle { { 1, -1 }, { 2, 2 }, { 3, -2 } });

    composite.add(circle);
    composite.add(triangle_);

    kurlyuk::Matrix matrix = kurlyuk::part(composite);

    BOOST_CHECK_NO_THROW(kurlyuk::Matrix matrix2(matrix));
    BOOST_CHECK_NO_THROW(kurlyuk::Matrix matrix2(std::move(matrix)));

    kurlyuk::Matrix matrix2 = kurlyuk::part(composite);
    kurlyuk::Matrix matrix3;

    BOOST_CHECK_NO_THROW(matrix3 = matrix2);
    BOOST_CHECK_NO_THROW(matrix3 = std::move(matrix2));

    kurlyuk::Matrix matrix4 = kurlyuk::part(composite);
    kurlyuk::Matrix matrix5;

    matrix5 = matrix4;
    BOOST_CHECK(matrix5 == matrix4);
    matrix5 = std::move(matrix4);
    BOOST_CHECK(matrix5 == matrix3);

    kurlyuk::Matrix matrix6(matrix3);
    BOOST_CHECK(matrix6 == matrix3);
    kurlyuk::Matrix matrix7(std::move(matrix3));
    BOOST_CHECK(matrix7 == matrix6);
  }

  BOOST_AUTO_TEST_CASE(exceptionThrow)
  {
    pointer circle = std::make_shared<kurlyuk::Circle>(5, kurlyuk::point_t{ -3, 2.5 });
    pointer rectangle = std::make_shared<kurlyuk::Rectangle>(kurlyuk::rectangle_t{2, 6, { 2, -4.5 }});

    kurlyuk::point_t pointArray[] { { 4, 5 }, { 6, 4 }, { 7, -1 }, { 5, -3 }, { -2, -1 } };
    kurlyuk::CompositeShape::pointer polygon_
        = std::make_shared<kurlyuk::Polygon>(kurlyuk::Polygon { 5, pointArray });

    kurlyuk::CompositeShape composite;
    composite.add(circle);
    composite.add(rectangle);
    composite.add(polygon_);

    kurlyuk::Matrix matrix = kurlyuk::part(composite);

    BOOST_CHECK_THROW(matrix[3], std::invalid_argument);
    BOOST_CHECK_THROW(matrix[-1], std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

