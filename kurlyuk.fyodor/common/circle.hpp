#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

namespace kurlyuk
{
  class Circle: public Shape
  {
  public:
    Circle(double r, const point_t &dot);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &dot) override;
    void move(double dX, double dY) override;
    void scale(double scaleFactor) override;
    void rotate(double angle) override;
    void writeInfo() const override;
  private:
    point_t center_;
    double r_;
  };
}

#endif

