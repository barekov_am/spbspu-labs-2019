#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>

#include "shape.hpp"

namespace kurlyuk
{
  class Matrix
  {
  public:
    using pointer = std::shared_ptr<Shape>;
    using array = std::unique_ptr<pointer[]>;

    Matrix();
    Matrix(const Matrix &other);
    Matrix(Matrix  &&other) noexcept;
    ~Matrix() = default;

    Matrix &operator =(const Matrix &other);
    Matrix &operator =(Matrix &&other) noexcept;

    array operator [](unsigned int index) const;
    bool operator ==(const Matrix &other) const;
    bool operator !=(const Matrix &other) const;

    void add(pointer shape, unsigned int row, unsigned int column);

    unsigned int getRows() const;
    unsigned int getColumns() const;
    unsigned int getSizeLayer(unsigned int) const;

  private:
    unsigned int rows_;
    unsigned int columns_;
    array list_;
  };
}

#endif
