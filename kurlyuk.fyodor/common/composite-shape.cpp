#include "composite-shape.hpp"

#include <algorithm>
#include <iostream>
#include <cmath>
#include <stdexcept>

const kurlyuk::rectangle_t ZeroObjectRectangle = kurlyuk::rectangle_t{0, 0, {0, 0}};

kurlyuk::CompositeShape::CompositeShape():
  quantity_(0)
{ }

kurlyuk::CompositeShape::CompositeShape(const CompositeShape &other):
  quantity_(other.quantity_),
  shapes_(std::make_unique<pointer[]>(other.quantity_))
{
  for (size_t i = 0; i < quantity_; i++)
  {
    shapes_[i] = other.shapes_[i];
  }
}

kurlyuk::CompositeShape::CompositeShape(CompositeShape &&other):
  quantity_(other.quantity_),
  shapes_(std::move(other.shapes_))
{
  other.quantity_ = 0;
}

kurlyuk::CompositeShape::CompositeShape(const pointer &shape):
  CompositeShape()
{
  add(shape);
}

kurlyuk::CompositeShape &kurlyuk::CompositeShape::operator =(const CompositeShape &other)
{
  if (this != &other)
  {
    shapes_ = std::make_unique<kurlyuk::CompositeShape::pointer[]>(other.quantity_);
    quantity_ = other.quantity_;
    for (size_t i = 0; i < quantity_; i++)
    {
      shapes_[i] = other.shapes_[i];
    }
  }
  return *this;
}

kurlyuk::CompositeShape &kurlyuk::CompositeShape::operator =(CompositeShape &&other)
{
  if (this != &other)
  {
    quantity_ = other.quantity_;
    shapes_ = std::move(other.shapes_);
    other.quantity_ = 0;
  }
  return *this;
}

kurlyuk::CompositeShape::pointer kurlyuk::CompositeShape::operator [](size_t index) const
{
  if (quantity_ <= index)
  {
    throw std::invalid_argument("Index is out of range");
  }

  return shapes_[index];
}

bool kurlyuk::CompositeShape::operator ==(const CompositeShape &other) const
{
  if (quantity_ != other.quantity_)
  {
    return false;
  }

  for (size_t i = 0; i < quantity_; i++)
  {
    if (shapes_[i] != other.shapes_[i])
    {
      return false;
    }
  }

  return true;
}

bool kurlyuk::CompositeShape::operator !=(const CompositeShape &other) const
{
  return !(other == *this);
}

void kurlyuk::CompositeShape::writeInfo() const
{
  if (quantity_ == 0)
  {
    std::cout << "Composite shape is empty";
  }
  else
  {
    rectangle_t rect = getFrameRect();

    std::cout << "\nComposite with "
              << quantity_ << " shapes"
              << "\nBounding rectangle: "
              << "\nHeight: " << rect.height
              << "\nWidth: " << rect.width
              << "\nArea: " << getArea()
              << "\nCenter position: "
              << "\nX: " << getFrameRect().pos.x
              << "\nY: " << getFrameRect().pos.y
              << "\nElements: " << "\n";

    for (size_t i = 0; i < quantity_; i++)
    {
      shapes_[i]->writeInfo();
      std::cout << "\n";
    }

  }
}

double kurlyuk::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < quantity_; i++)
  {
    area += shapes_[i]->getArea();
  }

  return area;
}

kurlyuk::rectangle_t kurlyuk::CompositeShape::getFrameRect() const
{
  if (quantity_ == 0)
  {
    return ZeroObjectRectangle;
  }

  rectangle_t tempRect = ZeroObjectRectangle;

  size_t i = 0;
  while ((tempRect == ZeroObjectRectangle) && (i < quantity_))
  {
    tempRect = shapes_[i]->getFrameRect();
    i++;
  }

  double minX = tempRect.pos.x - tempRect.width / 2;
  double maxX = tempRect.pos.x + tempRect.width / 2;
  double minY = tempRect.pos.y - tempRect.height / 2;
  double maxY = tempRect.pos.y + tempRect.height / 2;

  for (i++; i < quantity_; i++)
  {
    tempRect = shapes_[i]->getFrameRect();

    if (tempRect == ZeroObjectRectangle)
    {
      continue;
    }

    minX = std::min(minX, tempRect.pos.x - tempRect.width / 2);
    maxX = std::max(maxX, tempRect.pos.x + tempRect.width / 2);
    minY = std::min(minY, tempRect.pos.y - tempRect.height / 2);
    maxY = std::max(maxY, tempRect.pos.y + tempRect.height / 2);
  }

  return rectangle_t{maxY - minY, maxX - minX, (maxX + minX) / 2, (maxY + minY) / 2};
}

void kurlyuk::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < quantity_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void kurlyuk::CompositeShape::move(const point_t &center)
{
  rectangle_t frameRect = getFrameRect();
  double dx = center.x - frameRect.pos.x;
  double dy = center.y - frameRect.pos.y;
  move(dx, dy);
}

void kurlyuk::CompositeShape::scale(double scaleFactor)
{
  if (scaleFactor <= 0)
  {
    throw std::invalid_argument("scaling factor can not be less than zero");
  }

  if (quantity_ != 0)
  {
    rectangle_t frameRect = getFrameRect();
    size_t scaleOffset = scaleFactor - 1;

    for (size_t i = 0; i < quantity_; i++)
    {
      rectangle_t currentFrameRect = shapes_[i]->getFrameRect();
      double dx = (currentFrameRect.pos.x - frameRect.pos.x) * scaleOffset;
      double dy = (currentFrameRect.pos.y - frameRect.pos.y) * scaleOffset;
      shapes_[i]->move(dx, dy);
      shapes_[i]->scale(scaleFactor);
    }
  }
}

void kurlyuk::CompositeShape::rotate(double angle)
{
  const double sin = std::sin(M_PI * angle / 180);
  const double cos = std::cos(M_PI * angle / 180);

  const point_t compositeCenter = getFrameRect().pos;

  for (size_t i = 0; i < quantity_; i++)
  {
    const point_t figureCenter
        = shapes_[i]->getFrameRect().pos;

    double dx =
        compositeCenter.x + (figureCenter.x - compositeCenter.x) * cos - (figureCenter.y - compositeCenter.y) * sin;

    double dy =
        compositeCenter.y + (figureCenter.x - compositeCenter.x) * sin + (figureCenter.y - compositeCenter.y) * cos;

    shapes_[i]->move({ dx, dy });
    shapes_[i]->rotate(angle);
  }
}

size_t kurlyuk::CompositeShape::getSize() const
{
  return(quantity_);
}

kurlyuk::CompositeShape::array kurlyuk::CompositeShape::list() const
{
  array tmpArray(std::make_unique<pointer[]>(quantity_));
  for (unsigned int i = 0; i < quantity_; i++)
  {
    tmpArray[i] = shapes_[i];
  }

  return tmpArray;
}

void kurlyuk::CompositeShape::add(const pointer &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer must not be null");
  }

  array tmpShapes = std::make_unique<pointer[]>(quantity_ + 1);

  for (size_t i = 0; i < quantity_; i++)
  {
    tmpShapes[i] = shapes_[i];
  }

  tmpShapes[quantity_] = shape;
  quantity_++;
  shapes_ = std::move(tmpShapes);
}

void kurlyuk::CompositeShape::remove(size_t index)
{
  if (quantity_ <= index)
  {
    throw std::invalid_argument("Can not delete shape that does not exist");
  }

  for (size_t i = index; i < (quantity_ - 1); i++)
  {
    shapes_[i] = shapes_[i + 1];
  }

  shapes_[quantity_--] = nullptr;
}
