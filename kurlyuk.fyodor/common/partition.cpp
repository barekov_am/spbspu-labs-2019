#include "partition.hpp"

kurlyuk::Matrix kurlyuk::part(const array &arr, unsigned int size)
{
  Matrix matrix;

  for (unsigned int i = 0; i < size; i++)
  {
    unsigned int rightRow = 0;
    unsigned int rightColumn = 0;
    unsigned int necessaryColumn = 0;

    for (unsigned int j = 0; j < matrix.getRows(); j++)
    {
      necessaryColumn = matrix.getSizeLayer(j);
      for (unsigned int k = 0; k < matrix.getColumns(); k++)
      {
        if (intersect(arr[i]->getFrameRect(), matrix[j][k]->getFrameRect()))
        {
          rightRow++;
          break;
        }

        if (k == necessaryColumn - 1)
        {
          rightRow = j;
          rightColumn = necessaryColumn;
        }

      }
      if (rightRow == j)
      {
        break;
      }
    }

    matrix.add(arr[i], rightRow, rightColumn);
  }

  return matrix;
}

kurlyuk::Matrix kurlyuk::part(const CompositeShape &composite)
{
  return part(composite.list(), composite.getSize());
}
