#ifndef SHAPE_HPP
#define SHAPE_HPP
#include "base-types.hpp"

namespace kurlyuk
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t &dot) = 0;
    virtual void move(double dX, double dY) = 0;
    virtual void scale(double scaleFactor) = 0;
    virtual void rotate(double angle) = 0;
    virtual void writeInfo() const = 0;
  };
}

#endif //SHAPE_HPP

