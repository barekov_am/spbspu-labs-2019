#include "base-types.hpp"
#include <cmath>
#include <limits>

bool is_equal(double x, double y) {
    return std::fabs(x - y) < std::numeric_limits<double>::epsilon();
}

bool kurlyuk::point_t::operator ==(const point_t &other) const
{
  return ((is_equal(x, other.x)) && (is_equal(y, other.y)));
}

bool kurlyuk::point_t::operator !=(const point_t &other) const
{
  return !(*this == other);
}

bool kurlyuk::rectangle_t::operator ==(const rectangle_t &other) const
{
  return ((is_equal(width, other.width)) && (is_equal(height, other.height)) && (pos == other.pos));
}

bool kurlyuk::rectangle_t::operator !=(const rectangle_t &other) const
{
  return !(*this == other);
}

bool kurlyuk::intersect(const rectangle_t &lhs, const rectangle_t &rhs)
{
  const bool widthCondition = std::fabs(lhs.pos.x - rhs.pos.x) < (std::fabs(lhs.width + rhs.width) / 2);
  const bool heightCondition = std::fabs(lhs.pos.y - rhs.pos.y) < (std::fabs(lhs.height + rhs.height) / 2);

  return widthCondition && heightCondition;
}

