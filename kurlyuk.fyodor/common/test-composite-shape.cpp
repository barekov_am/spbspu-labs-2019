#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteCompositeShape)

  const double ErrorValue = 1e-10;

  BOOST_AUTO_TEST_CASE(CompositeShapeCopyConstructor)
  {
    kurlyuk::CompositeShape::pointer rectangle_
        = std::make_shared<kurlyuk::Rectangle>(kurlyuk::rectangle_t {1, 3, { 2, 2 }});

    kurlyuk::CompositeShape::pointer circle_
        = std::make_shared<kurlyuk::Circle>(1, kurlyuk::point_t { 1, 1 });

    kurlyuk::CompositeShape composite1_(rectangle_);
    composite1_.add(circle_);

    const kurlyuk::rectangle_t frame1_ = composite1_.getFrameRect();
    const double area1_ = composite1_.getArea();

    kurlyuk::CompositeShape composite2_(composite1_);

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(area1_, composite2_.getArea(), ErrorValue);
    BOOST_CHECK_EQUAL(composite1_.getSize(), composite2_.getSize());
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeMoveConstructor)
  {
    kurlyuk::CompositeShape::pointer rectangle_
        = std::make_shared<kurlyuk::Rectangle>(kurlyuk::rectangle_t {2, 3, { 2, 2 }});

    kurlyuk::CompositeShape::pointer circle_
        = std::make_shared<kurlyuk::Circle>(1, kurlyuk::point_t { 1, 1 });

    kurlyuk::CompositeShape composite1_(rectangle_);
    composite1_.add(circle_);

    const kurlyuk::rectangle_t frame1_ = composite1_.getFrameRect();
    const double area_ = composite1_.getArea();
    const size_t size_ = composite1_.getSize();

    kurlyuk::CompositeShape composite2_(std::move(composite1_));

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(area_, composite2_.getArea(), ErrorValue);
    BOOST_CHECK_EQUAL(size_, composite2_.getSize());
    BOOST_CHECK_CLOSE(composite1_.getArea(), 0, ErrorValue);
    BOOST_CHECK_EQUAL(composite1_.getSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeCopyOperator)
  {
    kurlyuk::CompositeShape::pointer rectangle_
        = std::make_shared<kurlyuk::Rectangle>(kurlyuk::rectangle_t {2, 3, { 2, 2 }});

    kurlyuk::CompositeShape::pointer circle_
        = std::make_shared<kurlyuk::Circle>(1, kurlyuk::point_t { 1, 1 });

    kurlyuk::CompositeShape composite_(rectangle_);
    composite_.add(circle_);

    const kurlyuk::rectangle_t frame1_ = composite_.getFrameRect();
    const double area_ = composite_.getArea();

    kurlyuk::CompositeShape composite2_;
    composite2_ = composite_;

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(area_, composite2_.getArea(), ErrorValue);
    BOOST_CHECK_EQUAL(composite_.getSize(), composite2_.getSize());
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeMoveOperator)
  {
    kurlyuk::CompositeShape::pointer rectangle_
        = std::make_shared<kurlyuk::Rectangle>(kurlyuk::rectangle_t {2, 3, { 2, 2 }});

    kurlyuk::CompositeShape::pointer circle_
        = std::make_shared<kurlyuk::Circle>(1, kurlyuk::point_t { 1, 1 });

    kurlyuk::CompositeShape composite1_(rectangle_);
    composite1_.add(circle_);

    const kurlyuk::rectangle_t frame1_ = composite1_.getFrameRect();
    const double area_ = composite1_.getArea();
    const size_t size_ = composite1_.getSize();

    kurlyuk::CompositeShape composite2_;
    composite2_ = std::move(composite1_);

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(area_, composite2_.getArea(), ErrorValue);
    BOOST_CHECK_EQUAL(size_, composite2_.getSize());
    BOOST_CHECK_CLOSE(composite1_.getArea(), 0, ErrorValue);
    BOOST_CHECK_EQUAL(composite1_.getSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeConstAfterMoving)
  {
    kurlyuk::CompositeShape composite_;
    kurlyuk::CompositeShape::pointer circle_
        = std::make_shared<kurlyuk::Circle>(2, kurlyuk::point_t { 1, 1 });

    kurlyuk::CompositeShape::pointer rectangle_
        = std::make_shared<kurlyuk::Rectangle>(kurlyuk::rectangle_t {2, 3, { 1, -1 }});

    kurlyuk::CompositeShape::pointer triangle_
        = std::make_shared<kurlyuk::Triangle>(kurlyuk::Triangle { { 1, -1 }, { 2, 2 }, { 3, -2 } });

    kurlyuk::point_t pointArray[] { { 4, 5 }, { 6, 4 }, { 7, -1 }, { 5, -3 }, { 2, 1 } };
    kurlyuk::CompositeShape::pointer polygon_
        = std::make_shared<kurlyuk::Polygon>(kurlyuk::Polygon { 5, pointArray });

    composite_.add(circle_);
    composite_.add(rectangle_);
    composite_.add(triangle_);
    composite_.add(polygon_);
    const double firstArea = composite_.getArea();

    composite_.move({ 4, 5 });
    double secondArea = composite_.getArea();
    BOOST_CHECK_CLOSE(firstArea, secondArea, ErrorValue);

    composite_.move(-2, 3);
    secondArea = composite_.getArea();
    BOOST_CHECK_CLOSE(firstArea, secondArea, ErrorValue);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeScaling)
  {
    kurlyuk::CompositeShape composite_;
    kurlyuk::CompositeShape::pointer circle_
        = std::make_shared<kurlyuk::Circle>(2, kurlyuk::point_t { 1, -1 });

    kurlyuk::CompositeShape::pointer rectangle_
        = std::make_shared<kurlyuk::Rectangle>(kurlyuk::rectangle_t {2, 3, { 1, -1 }});

    kurlyuk::CompositeShape::pointer triangle_
        = std::make_shared<kurlyuk::Triangle>(kurlyuk::Triangle { { 1, -1 }, { 2, 2 }, { 3, -2 } });

    kurlyuk::point_t pointArray[] { { 4, 5 }, { 6, 4 }, { 7, -1 }, { 5, -3 }, { 2, 1 } };
    kurlyuk::CompositeShape::pointer polygon_
        = std::make_shared<kurlyuk::Polygon>(kurlyuk::Polygon { 5, pointArray });

    composite_.add(circle_);
    composite_.add(rectangle_);
    composite_.add(triangle_);
    composite_.add(polygon_);

    const double firstArea = composite_.getArea();
    const double scaleFactor = 6;
    composite_.scale(scaleFactor);
    double secondArea = composite_.getArea();

    BOOST_CHECK_CLOSE(firstArea *  scaleFactor * scaleFactor, secondArea, ErrorValue);
  }

  BOOST_AUTO_TEST_CASE(operatorTests)
  {
    kurlyuk::CompositeShape::pointer rectangle1_
        = std::make_shared<kurlyuk::Rectangle>(kurlyuk::rectangle_t {2, 2, { 1, 1 }});
    kurlyuk::CompositeShape compShape1_(rectangle1_);

    const kurlyuk::rectangle_t testFrameRect = rectangle1_->getFrameRect();
    const double area = rectangle1_->getArea();

    kurlyuk::CompositeShape::pointer circle1_
        = std::make_shared<kurlyuk::Circle>(2, kurlyuk::point_t { 2, 2 });
    kurlyuk::CompositeShape compShape2_(circle1_);

    BOOST_CHECK_NO_THROW(kurlyuk::CompositeShape compShape2_(compShape1_));
    BOOST_CHECK_NO_THROW(compShape2_= compShape1_);
    BOOST_CHECK_NO_THROW(compShape2_= std::move(compShape1_));
    BOOST_CHECK_NO_THROW(kurlyuk::CompositeShape compShape2_(std::move(compShape1_)));

    compShape1_.add(rectangle1_);
    BOOST_CHECK_CLOSE(testFrameRect.width, compShape1_.getFrameRect().width, ErrorValue);
    BOOST_CHECK_CLOSE(testFrameRect.height, compShape1_.getFrameRect().height, ErrorValue);
    BOOST_CHECK_CLOSE(area, compShape1_.getArea(), ErrorValue);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeAfterRotate)
  {
    kurlyuk::CompositeShape composite_;
    kurlyuk::CompositeShape::pointer circle_
        = std::make_shared<kurlyuk::Circle>(2, kurlyuk::point_t { 1, -1 });

    kurlyuk::CompositeShape::pointer rectangle_
        = std::make_shared<kurlyuk::Rectangle>(kurlyuk::rectangle_t {2, 3, { 1, -1 }});

    kurlyuk::CompositeShape::pointer triangle_
        = std::make_shared<kurlyuk::Triangle>(kurlyuk::Triangle { { 1, -1 }, { 2, 2 }, { 3, -2 } });

    kurlyuk::point_t pointArray[] { { 4, 5 }, { 6, 4 }, { 7, -1 }, { 5, -3 }, { 2, 1 } };
    kurlyuk::CompositeShape::pointer polygon_
        = std::make_shared<kurlyuk::Polygon>(kurlyuk::Polygon { 5, pointArray });

    composite_.add(circle_);
    composite_.add(rectangle_);
    composite_.add(triangle_);
    composite_.add(polygon_);

    const kurlyuk::rectangle_t frameRectBefore = composite_.getFrameRect();
    const double areaBefore = composite_.getArea();
    const kurlyuk::rectangle_t frameRectAfter = composite_.getFrameRect();

    composite_.rotate(90);
    BOOST_CHECK_CLOSE(areaBefore, composite_.getArea(), ErrorValue);
    BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.width, ErrorValue);
    BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.height, ErrorValue);
  }


  BOOST_AUTO_TEST_CASE(exceptionThrow)
  {
    kurlyuk::CompositeShape::pointer rectangle1_
        = std::make_shared<kurlyuk::Rectangle>(kurlyuk::rectangle_t {2, 2, { 1, 1 }});

    kurlyuk::CompositeShape composite_(rectangle1_);

    BOOST_CHECK_THROW(composite_.scale(-2), std::invalid_argument);
    BOOST_CHECK_THROW(composite_.add(nullptr), std::invalid_argument);
    BOOST_CHECK_THROW(composite_.remove(5), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
