#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace kurlyuk
{
  class Rectangle: public Shape
  {
  public:
    Rectangle(const rectangle_t &rect);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &dot) override;
    void move(double dX, double dY) override;
    void scale(double scaleFactor) override;
    void rotate(double angle) override;
    void writeInfo() const override;

    point_t getCenter() const;
  private:
    point_t vertices_[4];
  };
}

#endif

