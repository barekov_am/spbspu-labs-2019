#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include <memory>

#include "shape.hpp"

namespace kurlyuk
{
  class CompositeShape: public Shape
  {
  public:
  
    using pointer = std::shared_ptr<Shape>;
    using array = std::unique_ptr<pointer[]>;

    CompositeShape();

    CompositeShape(const CompositeShape &other);
    CompositeShape(CompositeShape &&other);
    CompositeShape(const pointer &shape);
    ~CompositeShape() override = default;

    CompositeShape &operator =(const CompositeShape &other);
    CompositeShape &operator =(CompositeShape &&other);

    pointer operator [](size_t index) const;
    bool operator ==(const CompositeShape &other) const;
    bool operator !=(const CompositeShape &other) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void writeInfo() const override;
    void move(const point_t &center) override;
    void move(double dx, double dy) override;
    void scale(double scaleFactor) override;
    void rotate(double angle) override;

    size_t getSize() const;
    array list() const;

    void add(const pointer &shape);
    void remove(size_t index);

  private:
    size_t quantity_;
    array shapes_;
  };
}

#endif
