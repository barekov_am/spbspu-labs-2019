#include "rectangle.hpp"
#include "triangle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>

const double VerticesCount = 4;

kurlyuk::Rectangle::Rectangle(const rectangle_t &rect) :
  vertices_{ { rect.pos.x - rect.width / 2, rect.pos.y + rect.height / 2 },
           { rect.pos.x + rect.width / 2, rect.pos.y + rect.height / 2 },
           { rect.pos.x + rect.width / 2, rect.pos.y - rect.height / 2 },
           { rect.pos.x - rect.width / 2, rect.pos.y - rect.height / 2 } }

{
  if ((rect.width <= 0.0) || (rect.height <= 0.0))
  {
    throw std::invalid_argument("Rectangle dimensions are incorrect.");
  }
}

kurlyuk::point_t kurlyuk::Rectangle::getCenter() const
{
  return {(vertices_[0].x + vertices_[1].x + vertices_[2].x + vertices_[3].x) / 4, (vertices_[0].y + vertices_[1].y + vertices_[2].y + vertices_[3].y) / 4};
}

double kurlyuk::Rectangle::getArea() const
{
  double area = 0.0;
  for (size_t i = 1; i < VerticesCount - 1; i++)
  {
    Triangle triangle({vertices_[0], vertices_[i], vertices_[i + 1]});
    area += triangle.getArea();
  }
  return area;
}

kurlyuk::rectangle_t kurlyuk::Rectangle::getFrameRect() const
{
  point_t max = {vertices_[0].x, vertices_[0].y};
  point_t min = {vertices_[0].x, vertices_[0].y};
  for (size_t i = 1; i < VerticesCount; i++)
  {
    max = {std::max(max.x, vertices_[i].x), std::max(max.y, vertices_[i].y)};
    min = {std::min(min.x, vertices_[i].x), std::min(min.y, vertices_[i].y)};
  }
  point_t center = { min.x + (max.x - min.x) / 2, min.y + (max.y - min.y) / 2 };
  return {max.x - min.x, max.y - min.y, center};
}

void kurlyuk::Rectangle::move(const point_t &dot)
{
  const point_t center = getCenter();

  move(dot.x - center.x, dot.y - center.y);
}

void kurlyuk::Rectangle::move(double dX, double dY)
{
  for (int i = 0; i < VerticesCount; i++)
  {
    vertices_[i].x += dX;
    vertices_[i].y += dY;
  }
}

void kurlyuk::Rectangle::scale(double scaleFactor)
{
  if (scaleFactor <= 0.0)
  {
    throw std::invalid_argument("Invalid scale factor.");
  }

  const point_t center = getCenter();

  for (size_t i = 0; i < VerticesCount; i++)
  {
    vertices_[i]
        = { center.x + (vertices_[i].x - center.x) * scaleFactor,
            center.y + (vertices_[i].y - center.y) * scaleFactor };
  }
}

void kurlyuk::Rectangle::rotate(double angle)
{
  const double sin = std::sin(M_PI * angle / 180);
  const double cos = std::cos(M_PI * angle / 180);

  const point_t center = getCenter();

  for (size_t i = 0; i < VerticesCount; i++)
  {
    vertices_[i]
        = { center.x + (vertices_[i].x - center.x) * cos - (vertices_[i].y - center.y) * sin,
            center.y + (vertices_[i].y - center.y) * cos + (vertices_[i].x - center.x) * sin };
  }
}

void kurlyuk::Rectangle::writeInfo() const
{
  const rectangle_t frameRect = getFrameRect();
  const point_t center = getCenter();
  std::cout << "Center coordinates (" << center.x << ","
      << center.y << ")" << '\n';
  std::cout << "Area: " << getArea() << '\n';
  std::cout << "Vertices array: " << '\n';
  for (int i = 0; i < VerticesCount; i++)
  {
    std::cout << "Verctice # " << i << ". x: " << vertices_[i].x << " y: " << vertices_[i].y << "\n";
  }

  std::cout << "\nPaste this straight into Input on https://www.geogebra.org/classic to see the shape\n";
  std::cout << "Execute[{";

  for (int i = 0; i < VerticesCount; i++)
  {
    std::cout << "\"(" << vertices_[i].x << ", " << vertices_[i].y << ")\", ";
  }

  std::cout << "\"Polygon({(" << vertices_[0].x << ", " << vertices_[0].y << ")";

  for (int i = 1; i < VerticesCount; i++)
  {
    std::cout << ", (" << vertices_[i].x << ", " << vertices_[i].y << ")";
  }

  std::cout << "})\"}]\n\n";

  std::cout << "Frame's height: " << frameRect.height << '\n';
  std::cout << "Frame's width: " << frameRect.width << '\n';
  std::cout << "Frame's centre coordinates: (" << frameRect.pos.x << ","
      << frameRect.pos.y << ")" << "\n\n";
}

