#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <cmath>

kurlyuk::Triangle::Triangle(const point_t &a, const point_t &b, const point_t &c) :
  a_(a),
  b_(b),
  c_(c)
{
  if (getArea() == 0)
  {
    throw std::invalid_argument("Triangle is incorrect.");
  }
}

double kurlyuk::Triangle::getArea() const
{
  return std::fabs((b_.x - a_.x) * (c_.y - a_.y) - (c_.x - a_.x) * (b_.y - a_.y)) / 2;
}

kurlyuk::rectangle_t kurlyuk::Triangle::getFrameRect() const
{
  const point_t min = {std::min(std::min(a_.x, b_.x), c_.x), std::min(std::min(a_.y, b_.y), c_.y)};
  const point_t max = {std::max(std::max(a_.x, b_.x), c_.x), std::max(std::max(a_.y, b_.y), c_.y)};

  return {max.x - min.x, max.y - min.y, {(max.x + min.x) / 2, (max.y + min.y) / 2}};
}

void kurlyuk::Triangle::move(const point_t &dot)
{
  const point_t center = getCenter();
  move(dot.x - center.x, dot.y - center.y);
}

void kurlyuk::Triangle::move(double dX, double dY)
{
  a_.x += dX;
  b_.x += dX;
  c_.x += dX;
  a_.y += dY;
  b_.y += dY;
  c_.y += dY;
}

kurlyuk::point_t kurlyuk::Triangle::getCenter() const
{
  return {(a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3};
}

void kurlyuk::Triangle::scale(double scaleFactor)
{
  if (scaleFactor <= 0.0)
  {
    throw std::invalid_argument("Invalid scale factor.");
  }

  const point_t centre = getCenter();
  a_.x += (a_.x - centre.x) * (scaleFactor - 1);
  a_.y += (a_.y - centre.y) * (scaleFactor - 1);
  b_.x += (b_.x - centre.x) * (scaleFactor - 1);
  b_.y += (b_.y - centre.y) * (scaleFactor - 1);
  c_.x += (c_.x - centre.x) * (scaleFactor - 1);
  c_.y += (c_.y - centre.y) * (scaleFactor - 1);
}

void kurlyuk::Triangle::rotatePoint(point_t &dot, const point_t &center, const double &cos, const double &sin)
{ 
  point_t tempPoint;
  tempPoint.x = center.x + (dot.x - center.x) * cos - (dot.y - center.y) * sin;
  tempPoint.y = center.y + (dot.x - center.x) * sin + (dot.y - center.y) * cos;
  dot.x = tempPoint.x;
  dot.y = tempPoint.y;
} 

void kurlyuk::Triangle::rotate(double angle)
{
  const double cos = std::cos(M_PI * angle / 180);
  const double sin = std::sin(M_PI * angle / 180);
  const point_t center = getCenter();

  rotatePoint(a_, center, cos, sin);
  rotatePoint(b_, center, cos, sin);
  rotatePoint(c_, center, cos, sin);
}

void kurlyuk::Triangle::writeInfo() const
{
  const point_t center = getCenter();
  const rectangle_t frameRect = getFrameRect();
  std::cout << "Center coordinates (" << center.x << ","
      << center.y << ")" << '\n';
  std::cout << "Area: " << getArea() << '\n';
  std::cout << "Frame's height: " << frameRect.height << '\n';
  std::cout << "Frame's width: " << frameRect.width << '\n';
  std::cout << "Frame's centre coordinates: (" << frameRect.pos.x << ","
      << frameRect.pos.y << ")" << "\n\n";
}
