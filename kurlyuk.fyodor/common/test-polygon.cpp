#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "polygon.hpp"

const double ErrorValue = 1e-10;

BOOST_AUTO_TEST_SUITE(polygonTestSuite)

BOOST_AUTO_TEST_CASE(invariabilityAfterMoving)
{
  const size_t size = 6; 
  kurlyuk::point_t points[size] = { { 2, 2 }, { 6, 2 }, { 7, 3 }, { 7, 6 }, { 3, 6 }, { 1, 4 }};

  kurlyuk::Polygon testPolygon(size, points);
  const kurlyuk::rectangle_t frameRectBefore = testPolygon.getFrameRect();
  const double areaBefore = testPolygon.getArea();

  testPolygon.move({ 1.9, 1.9 });
  BOOST_CHECK_CLOSE(frameRectBefore.width, testPolygon.getFrameRect().width, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectBefore.height, testPolygon.getFrameRect().height, ErrorValue);
  BOOST_CHECK_CLOSE(areaBefore, testPolygon.getArea(), ErrorValue);

  testPolygon.move(5, -7);
  BOOST_CHECK_CLOSE(frameRectBefore.width, testPolygon.getFrameRect().width, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectBefore.height, testPolygon.getFrameRect().height, ErrorValue);
  BOOST_CHECK_CLOSE(areaBefore, testPolygon.getArea(), ErrorValue);
}

BOOST_AUTO_TEST_CASE(areaAfterScaling)
{
  const size_t size = 4; 
  kurlyuk::point_t points[size] = { { -5, -6 }, { -6, 5 }, { -5, 4 }, { 4, -5 } };

  kurlyuk::Polygon testPolygon(size, points);
  double areaBefore = testPolygon.getArea();

  double scaleFactor = 2.75;
  testPolygon.scale(scaleFactor);
  BOOST_CHECK_CLOSE(testPolygon.getArea(), scaleFactor * scaleFactor * areaBefore, ErrorValue);
  areaBefore = testPolygon.getArea();

  scaleFactor = 0.75;
  testPolygon.scale(scaleFactor);
  BOOST_CHECK_CLOSE(testPolygon.getArea(), scaleFactor * scaleFactor * areaBefore, ErrorValue);
}

BOOST_AUTO_TEST_CASE(testPolygonRotation)
{
 const size_t quantity = 6;
  kurlyuk::point_t vertices[quantity] = { { -2, -2 }, { -1, 4 }, { 3, 8 }, { 6, 8 }, { 8, 7 }, { 3, 0 } };
  kurlyuk::Polygon testPolygon(quantity, vertices);
  const double areaBefore = testPolygon.getArea();
  const kurlyuk::rectangle_t frameRectBefore = testPolygon.getFrameRect();

  double angle = -48;
  testPolygon.rotate(angle);

  double areaAfter = testPolygon.getArea();

  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ErrorValue);

  angle = 48;
  testPolygon.rotate(angle);
  kurlyuk::rectangle_t frameRectAfter = testPolygon.getFrameRect();
  areaAfter = testPolygon.getArea();

  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, ErrorValue);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, ErrorValue);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ErrorValue);
}


BOOST_AUTO_TEST_CASE(exceptionThrow)
{
  const size_t size1 = 6; 
  BOOST_CHECK_THROW(kurlyuk::Polygon(size1, nullptr), std::invalid_argument);
  
  kurlyuk::point_t points1[size1] = { { 2, 2 }, { 1, 2 }, { 7, 3 }, { 7, 6 }, { 3, 6 }, { 1, 4 }};
  BOOST_CHECK_THROW(kurlyuk::Polygon(size1, points1), std::logic_error);
  
  kurlyuk::point_t points2[size1] = { { 2, 2 }, { 2, 2 }, { 2, 2 }, { 2, 2 }, { 2, 2 }, { 2, 2 }};
  BOOST_CHECK_THROW(kurlyuk::Polygon(size1, points2), std::logic_error);

  const size_t size2 = 2; 
  kurlyuk::point_t points3[size2] = { { 2, 2 }, { 6, 2 } };
  BOOST_CHECK_THROW(kurlyuk::Polygon(size2, points3), std::invalid_argument);
  
  kurlyuk::point_t points4[size1] = { { 2, 2 }, { 6, 2 }, { 7, 3 }, { 7, 6 }, { 3, 6 }, { 1, 4 } };
  kurlyuk::Polygon testPolygon(size1, points4);
  BOOST_CHECK_THROW(testPolygon.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()


