#ifndef WEIRDSORT_HPP
#define WEIRDSORT_HPP

#include <algorithm>

template <template <class T> class Access, typename T>
void weirdSort(T& collection, bool ascending) {
  auto start = Access<T>::begin(collection);
  auto end = Access<T>::end(collection);
  bool swapped;

  while (start != end) {
    swapped = false;
    auto inner = start;
    while (inner != end) {
      typename Access<T>::valueType& previous = Access<T>::element(collection, inner);
      typename Access<T>::valueType& current = Access<T>::element(collection, start);

      if (previous == current) {
        swapped = true;
      }

      if (ascending) {
        if (previous < current) {
          std::swap(previous, current);
          swapped = true;
        }
      } else {
        if (previous > current) {
          std::swap(previous, current);
          swapped = true;
        }
      }
      inner++;
    }
    start++;
    if (!swapped) {
      break;
    }
  }
}

#endif
