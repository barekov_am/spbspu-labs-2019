// B1, kurlyuk.fv
#include <iostream>
#include <sstream>  // for std::stringstream
#include <string>

void ex1(const char *direction);
void ex2(const char *filename);
void ex3();
void ex4(const char *direction, size_t size);

int main(int argc, char *argv[]) {
  if (argc <= 1) {
    if (argv[0])
      std::cerr << "Usage: " << argv[0] << " [1-4] [args]" << '\n';
    else
      std::cerr << "Usage: <program name> [1-4] [args]" << '\n';

    return 1;
  }

  std::stringstream convert(argv[1]);
  int task;
  if (!(convert >> task) || task < 1 || task > 4) {
    std::cerr << "Error: incorrect task number." << '\n';
    return 1;
  }

  try {
    switch (task) {
      case 1:
        if (argc != 3) {
          std::cerr << "Incorrect number of arguments.\n";
          return 1;
        }

        ex1(argv[2]);
        break;

      case 2:
        if (argc != 3) {
          std::cerr << "Incorrect number of arguments.\n";
          return 1;
        }

        ex2(argv[2]);
        break;

      case 3:
        if (argc != 2) {
          std::cerr << "Incorrect number of arguments.\n";
          return 1;
        }

        ex3();
        break;

      case 4:
        if (argc != 4) {
          std::cerr << "Incorrect number of arguments.\n";
          return 1;
        }

        srand(static_cast<unsigned int>(time(NULL)));
        ex4(argv[2], std::stoi(argv[3]));
        break;
    }
  } catch (const std::exception &e) {
    std::cerr << e.what() << '\n';
    return 1;
  }

  return 0;
}
