#ifndef ENDPOINTS_HPP
#define ENDPOINTS_HPP
#include <iterator>

template <typename T>
struct BrAccess
{
  using valueType = typename T::value_type;
  typedef size_t index;

  static index begin(const T&) 
  {
    return 0;
  }

  static index end(const T& collection) 
  {
    return collection.size();
  }
  
  static typename T::reference element(T& collection, index i)
  {
    return collection[i];
  }
};

template <typename T>
struct AtAccess
{
  using valueType = typename T::value_type;
  typedef size_t index;

  static index begin(const T&) 
  {
    return 0;
  }

  static index end(const T& collection) 
  {
    return collection.size();
  }

  static typename T::reference element(T& collection, index i)
  {
    return collection.at(i);
  }
};

template <typename T>
struct ItAccess
{
  using valueType = typename T::value_type;
  typedef typename T::iterator index;

  static index begin(T& collection)
  {
    return collection.begin();
  }

  static index end(T& collection)
  {
    return collection.end();
  }

  static typename T::reference element(T&, index& iterator)
  {
    return *iterator;
  }
};
#endif
