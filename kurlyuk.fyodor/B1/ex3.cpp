#include <cstring>
#include <forward_list>
#include <iostream>
#include <iterator>
#include <vector>
#include "utils.hpp"
#include "endpoints.hpp"

void ex3() {
  std::vector<int> vector;
  int nm = 0;
  bool isZero = false;
  while (std::cin && !(std::cin >> nm).eof()) {
    if (nm == 0)
    {
      isZero = true;
      break;
    }
    
    vector.push_back(nm);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Fail while reading data.");
  }

  if (vector.empty())
  {
    return;
  }

  if (!isZero) {
    throw std::runtime_error("Cin must be ended by zero");
  }

  switch (vector.back()) {
    case 1:
      for (std::vector<int>::iterator i = vector.begin(); i != vector.end();) {
        if (*i % 2 == 0) {
          i = vector.erase(i);
        } else {
          ++i;
        };
      }
      break;

    case 2:
      for (std::vector<int>::iterator i = vector.begin(); i != vector.end();) {
        if (*i % 3 == 0) {
          i = vector.insert(++i, 3, 1) + 3;
        } else {
          ++i;
        };
      }
      break;
  }

  print(vector);
}
