#include <cstring>
#include <iostream>
#include <random>
#include <vector>
#include "weirdsort.hpp"
#include "endpoints.hpp"
#include "utils.hpp"

double generateGaussian(double mean, double stdDev) {
  static double spare;
  static bool hasSpare = false;

  if (hasSpare) {
    hasSpare = false;
    return spare * stdDev + mean;
  } else {
    double u, v, s;
    do {
      u = (rand() / ((double)RAND_MAX)) * 2.0 - 1.0;
      v = (rand() / ((double)RAND_MAX)) * 2.0 - 1.0;
      s = u * u + v * v;
    } while (s >= 1.0 || s == 0.0);
    s = sqrt(-2.0 * log(s) / s);
    spare = v * s;
    hasSpare = true;
    return mean + stdDev * u * s;
  }
}

void fillRandom(double *array, int size) {
  for (auto i = 0; i < size; i++) {
    array[i] = generateGaussian(0, 0.49);
  }
}

void ex4(const char *direction, size_t size) {
  bool ascending = isAscending(direction);

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);
  print(vector);
  weirdSort<BrAccess>(vector, ascending);
  print(vector);
}
