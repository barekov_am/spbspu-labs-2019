#ifndef UTILS_HPP
#define UTILS_HPP
#include <iostream>

bool isAscending(const char *direction);

template <typename T>
void print(const T &collection) {
  for (auto i : collection) {
    std::cout << i << " ";
  }

  std::cout << "\n";
}

#endif
