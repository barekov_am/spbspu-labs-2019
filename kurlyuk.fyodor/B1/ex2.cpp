#include <fstream>
#include <iostream>
#include <memory>
#include <vector>

void ex2(const char *filename) {
  const size_t initialSize = 1024;
  using charArray = std::unique_ptr<char[], decltype(&free)>;

  std::ifstream file(filename);

  if (!file) {
    throw std::runtime_error("File could not be opened.");
  }
  size_t size = initialSize;

  charArray awesomeCharArray(static_cast<char *>(malloc(size)), &free);
  size_t i = 0;
  while (file) {
    file.read(&awesomeCharArray[i], initialSize);
    i += file.gcount();

    if (file.gcount() == initialSize) {
      size += initialSize;
      try
      {
        charArray temp(static_cast<char *>(realloc(awesomeCharArray.get(), size)), &free);
        awesomeCharArray.release();
        std::swap(awesomeCharArray, temp);
      }
      catch(const std::exception& e)
      {
        throw std::runtime_error(e.what());
      }
    }
  }

  std::vector<char> vector(&awesomeCharArray[0], &awesomeCharArray[i]);

  for (char i : vector) {
    std::cout << i;
  }
};
