#include <cstring>
#include <forward_list>
#include <iostream>
#include <vector>
#include "weirdsort.hpp"
#include "endpoints.hpp"
#include "utils.hpp"

void ex1(const char *direction) {
  bool ascending = isAscending(direction);

  std::vector<int> vectorBr;
  int nm = 0;
  while (std::cin && !(std::cin >> nm).eof()) {
    if (!std::cin.eof() && std::cin.fail()) {
      throw std::invalid_argument("Could not read input data from cin");
    }
    vectorBr.push_back(nm);
  }

  if (vectorBr.empty()) {
    return;
  }

  std::vector<int> vectorAt = vectorBr;
  std::forward_list<int> listIt(vectorAt.begin(), vectorAt.end());

  weirdSort<BrAccess>(vectorBr, ascending);
  weirdSort<AtAccess>(vectorAt, ascending);
  weirdSort<ItAccess>(listIt, ascending);

  print(vectorBr);
  print(vectorAt);
  print(listIt);
}
