#include <cstring>
#include "utils.hpp"

bool isAscending(const char *direction) {
  if (!std::strcmp(direction, "ascending"))
    return true;
  else if (!std::strcmp(direction, "descending"))
    return false;
  else {
    throw std::invalid_argument("Incorrect sort direction");
  }
}
