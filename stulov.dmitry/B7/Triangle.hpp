#ifndef B7_TRIANGLE_HPP
#define B7_TRIANGLE_HPP

#include "Shape.hpp"

class Triangle : public Shape {
public:
Triangle(int x, int y);

void printer() const override;
};

#endif 
