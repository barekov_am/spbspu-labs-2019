#include <sstream>
#include <memory>
#include <algorithm>
#include <vector>
#include "Circle.hpp"
#include "Triangle.hpp"
#include "Square.hpp"

void printShapes(const std::vector<std::shared_ptr<Shape>> &shapes) {
  auto predicate = [](const std::shared_ptr<Shape> &shape) { shape->printer(); };
  std::for_each(shapes.begin(), shapes.end(), predicate);
}

void task2() {
  std::vector<std::shared_ptr<Shape>> shapes;
  std::string line;
  while (std::getline(std::cin, line)) {
    auto predicate = [](const char ch) { return std::isspace(ch); };
    line.erase(std::remove_if(line.begin(), line.end(), predicate), line.end());
    if (line.empty()) {
      continue;
    }

    size_t openingBracket = line.find_first_of('(');
    std::string name = line.substr(0, openingBracket);
    line.erase(0, openingBracket);
    openingBracket = line.find_first_of('(');
    size_t semicolon = line.find_first_of(';');
    size_t closingBracket = line.find_first_of(')');
    if ((openingBracket != 0) || (semicolon == std::string::npos) || (closingBracket == std::string::npos)) {
      throw std::invalid_argument("Invalid point");
    }

    int x = std::stoi(line.substr(1, semicolon - 1));
    int y = std::stoi(line.substr(semicolon + 1, closingBracket - semicolon - 1));
    if (name == "CIRCLE") {
      shapes.push_back(std::make_shared<Circle>(Circle({x, y})));
    }

    if (name == "TRIANGLE") {
      shapes.push_back(std::make_shared<Triangle>(Triangle({x, y})));
    }

    if (name == "SQUARE") {
      shapes.push_back(std::make_shared<Square>(Square({x, y})));
    }
  }

  std::cout << "Original:\n";
  printShapes(shapes);

  std::cout << "Left-Right:\n";
  auto leftRightSort = [](const std::shared_ptr<Shape> &lhs, const std::shared_ptr<Shape> &rhs) {
    return lhs->isMoreLeft(rhs);
  };
  std::sort(shapes.begin(), shapes.end(), leftRightSort);
  printShapes(shapes);

  std::cout << "Right-Left:\n";
  auto rightLeftSort = [](const std::shared_ptr<Shape> &lhs, const std::shared_ptr<Shape> &rhs) {
    return !lhs->isMoreLeft(rhs);
  };
  std::sort(shapes.begin(), shapes.end(), rightLeftSort);
  printShapes(shapes);

  std::cout << "Top-Bottom:\n";
  auto topBottomSort = [](const std::shared_ptr<Shape> &lhs, const std::shared_ptr<Shape> &rhs) {
    return lhs->isUpper(rhs);
  };
  std::sort(shapes.begin(), shapes.end(), topBottomSort);
  printShapes(shapes);

  std::cout << "Bottom-Top:\n";
  auto bottomTopSort = [](const std::shared_ptr<Shape> &lhs, const std::shared_ptr<Shape> &rhs) {
    return !lhs->isUpper(rhs);
  };
  std::sort(shapes.begin(), shapes.end(), bottomTopSort);
  printShapes(shapes);
}
