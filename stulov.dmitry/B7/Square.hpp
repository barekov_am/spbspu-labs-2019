#ifndef B7_SQUARE_HPP
#define B7_SQUARE_HPP

#include "Shape.hpp"

class Square : public Shape {
public:
Square(int x, int y);

void printer() const override;
};

#endif 
