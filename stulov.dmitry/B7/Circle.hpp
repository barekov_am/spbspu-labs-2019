#ifndef B7_CIRCLE_HPP
#define B7_CIRCLE_HPP

#include "Shape.hpp"

class Circle : public Shape {
public:
Circle(int x, int y);

void printer() const override;
};

#endif
