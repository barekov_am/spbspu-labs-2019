#include <iostream>
#include <iterator>
#include <list>
#include <cmath>

void task1() {
  std::list<double> list((std::istream_iterator<double>(std::cin)), std::istream_iterator<double>());

  if (std::cin.fail() && !std::cin.eof()) {
    throw std::ios_base::failure("fail reade\n");
  }

  for (auto value : list) {
    value *= M_PI;
    std::cout << value << '\n';
  }
}
