#include <list>
#include <iostream>

void task2()
{
    std::list<int> list;

    int nextElem;

    while(std::cin >> nextElem)
    {
        if((nextElem < 1) || (nextElem > 20))
        {
            throw std::out_of_range("Value not in range");
        }

        list.push_back(nextElem);
    }

    if(std::cin.fail() && !std::cin.eof())
    {
        throw std::invalid_argument("incorrect input");
    }

    if (list.size() > 20)
    {
        throw std::invalid_argument("the number of elements can't exceed 20");
    }

    auto it_next = list.begin();
    auto it_prev = list.rbegin();

    for (size_t i = 0; i < list.size() / 2; ++i)
    {
        std::cout << *it_next << " " << *it_prev << " ";
        it_next++;
        it_prev++;
    }

    if (list.size() % 2 == 1)
    {
        std::cout << *it_next;
    }

    std::cout << std::endl;
}
