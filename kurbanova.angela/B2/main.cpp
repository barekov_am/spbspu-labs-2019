#include <iostream>
#include <string>

void task1();
void task2();

int main(int args, char *argv[])
{
    try
    {
        if (args != 2)
        {
            std::cerr << "Number parameters args incorrect \n";
            return 1;
        }

        auto number = std::stoi(argv[1]);

        switch(number)
        {
            case 1:
                task1();
                break;

            case 2:
                task2();
                break;

            default:
                std::cerr << "Wrong task number";
                return 1;
        }
    }

    catch(const std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}
