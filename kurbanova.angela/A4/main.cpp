#define _USE_MATH_DEFINES
#include <iostream>
#include <memory>
#include <cmath>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

int main()
{
  std::cout << "Circle with center: (32; -15) and radius = 7" << std::endl;
  kurbanova::Circle circle1({ 32, -15 }, 7);
  kurbanova::CompositeShape::p_shape p_circle1 = std::make_shared<kurbanova::Circle>(circle1);
  std::cout << "Circle with center: (13; 13) and radius = 5" << std::endl;
  kurbanova::Circle circle2({ 13, 13 }, 5);
  kurbanova::CompositeShape::p_shape p_circle2 = std::make_shared<kurbanova::Circle>(circle2);
  std::cout << "Circle with center: (-4; 8) and radius = 2" << std::endl;
  kurbanova::Circle circle3({ -4, 8 }, 2);
  kurbanova::CompositeShape::p_shape p_circle3 = std::make_shared<kurbanova::Circle>(circle3);
  std::cout << "new Circle with center: (-78;-98) and radius = 10" << std::endl;
  kurbanova::Circle circle4({ -78, -98 }, 10);
  kurbanova::CompositeShape::p_shape p_circle4 = std::make_shared<kurbanova::Circle>(circle4);

  std::cout << "" << std::endl;

  std::cout << "Rectangle with center (32; -15) and width = 5, height = 4" << std::endl;
  kurbanova::Rectangle rect1({ 32, -15 }, 5, 4);
  kurbanova::CompositeShape::p_shape p_rect1 = std::make_shared<kurbanova::Rectangle>(rect1);
  std::cout << "Rectangle with center (12; 16) and width = 3, height = 2" << std::endl;
  kurbanova::Rectangle rect2({ 12, 16 }, 3, 2);
  kurbanova::CompositeShape::p_shape p_rect2 = std::make_shared<kurbanova::Rectangle>(rect2);
  std::cout << "Rectangle with center (-3; 15) and width = 6, height = 6" << std::endl;
  kurbanova::Rectangle rect3({ -3, 15 }, 6, 6);
  kurbanova::CompositeShape::p_shape p_rect3 = std::make_shared<kurbanova::Rectangle>(rect3);
  std::cout << "Rectangle with center (0; 0) and width = 3, height = 5" << std::endl;
  kurbanova::Rectangle rect4({ 0, 0 }, 3, 5);
  kurbanova::CompositeShape::p_shape p_rect4 = std::make_shared<kurbanova::Rectangle>(rect4);

  std::cout << "" << std::endl;

  kurbanova::CompositeShape composite_shape;
  composite_shape.add(p_circle1);
  composite_shape.add(p_circle2);
  composite_shape.add(p_circle3);
  composite_shape.add(p_circle4);
  composite_shape.add(p_rect1);
  composite_shape.add(p_rect2);
  composite_shape.add(p_rect3);
  composite_shape.add(p_rect4);
  composite_shape.printInfo();


  kurbanova::Matrix matrix = kurbanova::part(composite_shape);
  matrix.printInfo();

  std::cout << std::endl << "---------------------------------------------" << std::endl;

  composite_shape.rotate(M_PI / 2);
  composite_shape.printInfo();
  composite_shape.scale(2);
  composite_shape.printInfo();

  return 0;
}
