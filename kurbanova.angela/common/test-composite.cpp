#define _USE_MATH_DEFINES
#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include <iostream>
#include <cmath>
#include "circle.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"

const double INACCURACY = 0.001;

using p_shape = std::shared_ptr<kurbanova::Shape>;

BOOST_AUTO_TEST_SUITE(testImplementationOfCompositeShape)

BOOST_AUTO_TEST_CASE(immutabilityAreaAfterMovingCenter)

{
  kurbanova::Circle circle({ 5, 5 }, 5);
  kurbanova::Rectangle rect({ 5, 5 }, 10, 5);
  p_shape p_circle = std::make_shared<kurbanova::Circle>(circle);
  p_shape p_rect = std::make_shared<kurbanova::Rectangle>(rect);

  kurbanova::CompositeShape composite_shape;
  composite_shape.add(p_circle);
  composite_shape.add(p_rect);

  const double area = composite_shape.getArea();
  composite_shape.move({ 20, 20 });
  BOOST_CHECK_CLOSE(area, composite_shape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkWorkOfDelete)
{
  kurbanova::Circle circle({ 5, 5 }, 5);
  kurbanova::Rectangle rect1({ 5, 5 }, 7, 7);
  kurbanova::Rectangle rect2({ 5, 5 }, 11, 4);
  kurbanova::Rectangle rect3({ 5, 5 }, 12, 1);
  p_shape p_circle = std::make_shared<kurbanova::Circle>(circle);
  p_shape p_rect1 = std::make_shared<kurbanova::Rectangle>(rect1);
  p_shape p_rect2 = std::make_shared<kurbanova::Rectangle>(rect2);
  p_shape p_rect3 = std::make_shared<kurbanova::Rectangle>(rect3);

  kurbanova::CompositeShape composite_shape;
  composite_shape.add(p_circle);
  composite_shape.add(p_rect1);
  composite_shape.add(p_rect2);
  composite_shape.add(p_rect3);

  const int quantity_shape = composite_shape.getShapeCount();

  composite_shape.removeShape(p_rect1);
  const int quantity_shape_delete = composite_shape.getShapeCount();

  BOOST_CHECK_EQUAL(quantity_shape - 1, quantity_shape_delete);
  BOOST_CHECK_THROW(composite_shape.removeShape(p_rect1), std::invalid_argument);
  BOOST_CHECK_THROW(composite_shape.removeShape(quantity_shape), std::out_of_range);


  composite_shape.removeShape(p_rect2);
  const int quantity_shape_delete2 = composite_shape.getShapeCount();

  BOOST_CHECK_EQUAL(quantity_shape_delete - 1, quantity_shape_delete2);
  BOOST_CHECK_THROW(composite_shape.removeShape(p_rect2), std::invalid_argument);
  BOOST_CHECK_THROW(composite_shape.removeShape(quantity_shape_delete), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(invariabilityAreaAfterMoveDxDy)
{
  kurbanova::Circle circle({ 5, 5 },5);
  kurbanova::Rectangle rect({ 5, 5 }, 10, 5);
  p_shape p_circle = std::make_shared<kurbanova::Circle>(circle);
  p_shape p_rect = std::make_shared<kurbanova::Rectangle>(rect);


  kurbanova::CompositeShape composite_shape;
  composite_shape.add(p_circle);
  composite_shape.add(p_rect);

  const double area = composite_shape.getArea();
  composite_shape.move(5, 5);
  BOOST_CHECK_CLOSE(area, composite_shape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(changingAreaAfterScaleAnIncrease)
{
  kurbanova::Circle circle({ 5, 5 }, 5);
  kurbanova::Rectangle rect({ 5, 5 }, 10, 5);
  p_shape p_circle = std::make_shared<kurbanova::Circle>(circle);
  p_shape p_rect = std::make_shared<kurbanova::Rectangle>(rect);

  kurbanova::CompositeShape composite_shape;
  composite_shape.add(p_circle);
  composite_shape.add(p_rect);

  const double area = composite_shape.getArea();
  const double increase_coef = 2;
  composite_shape.scale(increase_coef);
  BOOST_CHECK_CLOSE(area * increase_coef * increase_coef, composite_shape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(changingAreaAfterScaleAnDecrease)
{
  kurbanova::Circle circle({ 5, 5 }, 5);
  kurbanova::Rectangle rect({ 5, 5 }, 10, 5);
  p_shape p_circle = std::make_shared<kurbanova::Circle>(circle);
  p_shape p_rect = std::make_shared<kurbanova::Rectangle>(rect);

  kurbanova::CompositeShape composite_shape;
  composite_shape.add(p_circle);
  composite_shape.add(p_rect);

  const double area = composite_shape.getArea();
  const double decrease_coef = 0.5;
  composite_shape.scale(decrease_coef);
  BOOST_CHECK_CLOSE(area * decrease_coef * decrease_coef, composite_shape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(throwExceptionDuaEntryNull)
{
  kurbanova::CompositeShape composite_shape;
  BOOST_CHECK_THROW(composite_shape.add(nullptr), std::invalid_argument);

}

BOOST_AUTO_TEST_CASE(throwExceptionDuaAddNull)
{
  kurbanova::Circle circle({ 5, 5 }, 5);
  kurbanova::Rectangle rect({ 5, 5 }, 10, 5);
  p_shape p_circle = std::make_shared<kurbanova::Circle>(circle);
  p_shape p_rect = std::make_shared<kurbanova::Rectangle>(rect);

  kurbanova::CompositeShape composite_shape;
  composite_shape.add(p_circle);
  composite_shape.add(p_rect);
  BOOST_CHECK_THROW(composite_shape.add(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(throwExceptionCoefficient)
{
  kurbanova::Circle circle({ 5, 5 }, 5);
  kurbanova::Rectangle rect({ 5, 5 }, 10, 5);
  p_shape p_circle = std::make_shared<kurbanova::Circle>(circle);
  p_shape p_rect = std::make_shared<kurbanova::Rectangle>(rect);

  kurbanova::CompositeShape composite_shape;
  composite_shape.add(p_circle);
  composite_shape.add(p_rect);

  BOOST_CHECK_THROW(composite_shape.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(immutabilityShapeQuantityAfterRotating)
{
  kurbanova::Circle circle({ 0, 0 }, 5);
  kurbanova::Rectangle rect({ 10, 10 }, 20, 30);
  p_shape p_circle = std::make_shared<kurbanova::Circle>(circle);
  p_shape p_rect = std::make_shared<kurbanova::Rectangle>(rect);
  kurbanova::CompositeShape composite_shape;
  composite_shape.add(p_circle);
  composite_shape.add(p_rect);
  const int quantity_shape = composite_shape.getShapeCount();

  composite_shape.rotate(M_PI / 2);
  BOOST_CHECK_EQUAL(quantity_shape, composite_shape.getShapeCount());
}

BOOST_AUTO_TEST_CASE(immutabilityCenterAfterRotating)
{
  kurbanova::Circle circle({ 0, 0 }, 5);
  kurbanova::Rectangle rect({ 10, 10 }, 20, 30);
  p_shape p_circle = std::make_shared<kurbanova::Circle>(circle);
  p_shape p_rect = std::make_shared<kurbanova::Rectangle>(rect);
  kurbanova::CompositeShape composite_shape;
  composite_shape.add(p_circle);
  composite_shape.add(p_rect);
  const kurbanova::point_t center = composite_shape.getCenter();

  composite_shape.rotate(M_PI / 2);
  BOOST_CHECK_CLOSE(center.x, composite_shape.getCenter().x, INACCURACY);
  BOOST_CHECK_CLOSE(center.y, composite_shape.getCenter().y, INACCURACY);
}

BOOST_AUTO_TEST_CASE(immutabilityAreaAfterRotating)
{
  kurbanova::Circle circle({ 0, 0 }, 5);
  kurbanova::Rectangle rect({ 10, 10 }, 20, 30);
  p_shape p_circle = std::make_shared<kurbanova::Circle>(circle);
  p_shape p_rect = std::make_shared<kurbanova::Rectangle>(rect);
  kurbanova::CompositeShape composite_shape;
  composite_shape.add(p_circle);
  composite_shape.add(p_rect);
  const double area = composite_shape.getArea();

  composite_shape.rotate(M_PI / 2);
  BOOST_CHECK_CLOSE(area, composite_shape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(immutabilityFrameRectAfterRotating360)
{
  kurbanova::Circle circle({ 0, 0 }, 5);
  kurbanova::Rectangle rect({ 10, 10 }, 20, 30);
  p_shape p_circle = std::make_shared<kurbanova::Circle>(circle);
  p_shape p_rect = std::make_shared<kurbanova::Rectangle>(rect);
  kurbanova::CompositeShape composite_shape;
  composite_shape.add(p_circle);
  composite_shape.add(p_rect);
  const double width = composite_shape.getFrameRect().width;
  const double height = composite_shape.getFrameRect().height;

  composite_shape.rotate(2 * M_PI);
  BOOST_CHECK_CLOSE(width, composite_shape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(height, composite_shape.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(swapSidesOfFrameRectAfterRotating90)
{
  kurbanova::Circle circle({ 0, 0 }, 5);
  kurbanova::Rectangle rect({ 10, 10 }, 20, 30);
  p_shape p_circle = std::make_shared<kurbanova::Circle>(circle);
  p_shape p_rect = std::make_shared<kurbanova::Rectangle>(rect);
  kurbanova::CompositeShape composite_shape;
  composite_shape.add(p_circle);
  composite_shape.add(p_rect);
  const double width = composite_shape.getFrameRect().width;
  const double height = composite_shape.getFrameRect().height;

  composite_shape.rotate(M_PI / 2);
  BOOST_CHECK_CLOSE(width, composite_shape.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(height, composite_shape.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
