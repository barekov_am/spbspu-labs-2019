#ifndef A3_COMPOSITE_SHAPE_HPP
#define A3_COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace kurbanova
{
  class  CompositeShape : public Shape
  {
  public:
    using p_shape = std::shared_ptr<Shape>;

    CompositeShape();
    CompositeShape(const CompositeShape &cShape);
    CompositeShape(CompositeShape &&cShape);
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape &rhs);
    CompositeShape& operator =(CompositeShape &&rhs);
  
    p_shape operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &pos) override;
    void printInfo() const override;
    void scale(double coefficient) override;
    size_t getShapeCount() const;
    void add(p_shape shape);
    void removeShape(p_shape shape);
    void removeShape(size_t index);
    void rotate(const double angle) override;
    point_t getCenter() const override;


  private:
    size_t shape_count_;
    std::unique_ptr<p_shape[]> arrayOfShapes_;

  };
}
#endif // A3_COMPOSITE_SHAPE_HPP
