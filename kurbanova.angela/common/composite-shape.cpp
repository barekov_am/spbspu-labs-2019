#include <iostream>
#include <stdexcept>
#include <cmath>
#include "composite-shape.hpp"

kurbanova::CompositeShape::CompositeShape() :
  shape_count_(0)
{}

kurbanova::CompositeShape::CompositeShape(const CompositeShape &cShape) :
  shape_count_(cShape.shape_count_),
  arrayOfShapes_(std::make_unique<p_shape[]>(cShape.shape_count_))
{
  for (size_t i = 0; i < shape_count_; ++i)
  {
    arrayOfShapes_[i] = cShape.arrayOfShapes_[i];
  }
}

kurbanova::CompositeShape::CompositeShape(CompositeShape &&cShape) :
  shape_count_(cShape.shape_count_),
  arrayOfShapes_(std::move(cShape.arrayOfShapes_))
{
    cShape.shape_count_ = 0;
}

kurbanova::CompositeShape& kurbanova::CompositeShape::operator =(const CompositeShape &rhs)
{
  if (this != &rhs)
  {
    shape_count_ = rhs.shape_count_;
    std::unique_ptr<p_shape[]> new_shape_array(std::make_unique<p_shape[]>(rhs.shape_count_));
    for (size_t i = 0; i < shape_count_; i++)
    {
      new_shape_array[i] = rhs.arrayOfShapes_[i];
    }
    arrayOfShapes_.swap(new_shape_array);
  }

  return *this;
}

kurbanova::CompositeShape& kurbanova::CompositeShape::operator =(CompositeShape &&rhs)
{
  if (this != &rhs)
  {
    shape_count_ = rhs.shape_count_;
    arrayOfShapes_ = std::move(rhs.arrayOfShapes_);
  }

  return *this;
}

kurbanova::CompositeShape::p_shape kurbanova::CompositeShape::operator[](size_t index) const
{
  if (index >= shape_count_)
  {
    throw std::out_of_range("Index out of range");
  }
  return arrayOfShapes_[index];
}

double kurbanova::CompositeShape::getArea() const
{
  double sum_area = 0;
  for (size_t i = 0; i < shape_count_; ++i)
  {
    sum_area += arrayOfShapes_[i]->getArea();
  }

  return sum_area;
}

kurbanova::rectangle_t kurbanova::CompositeShape::getFrameRect() const
{
  double max_x = arrayOfShapes_[0]->getFrameRect().pos.x;
  double max_y = arrayOfShapes_[0]->getFrameRect().pos.y;
  double min_x = arrayOfShapes_[0]->getFrameRect().pos.x;
  double min_y = arrayOfShapes_[0]->getFrameRect().pos.y;

  for (size_t i = 1; i < shape_count_; ++i)
  {
    rectangle_t temp_rect = arrayOfShapes_[i]->getFrameRect();

    double top = temp_rect.pos.y + temp_rect.height / 2;
    max_y = std::max(max_y, top);

    double right = temp_rect.pos.x + temp_rect.width / 2;
    max_x = std::max(max_x, right);

    double bottom = temp_rect.pos.y - temp_rect.height / 2;
    min_y = std::min(min_y, bottom);

    double left = temp_rect.pos.x - temp_rect.width / 2;
    min_x = std::min(min_x, left);
  }

  return rectangle_t{ point_t{(max_x + min_x) / 2, (max_y + min_y) / 2}, max_x - min_x, max_y - min_y };
}

void kurbanova::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < shape_count_; ++i)
  {
    arrayOfShapes_[i]->move(dx, dy);
  }
}

void kurbanova::CompositeShape::move(const point_t &pos)
{
  double dx = pos.x - getFrameRect().pos.x;
  double dy = pos.y - getFrameRect().pos.y;
  move(dx, dy);
}

void kurbanova::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }

  const point_t pos = getFrameRect().pos;
  for (size_t i = 0; i < shape_count_; ++i)
  {
    double dx = arrayOfShapes_[i]->getFrameRect().pos.x - pos.x;
    double dy = arrayOfShapes_[i]->getFrameRect().pos.y - pos.y;
    arrayOfShapes_[i]->move((coefficient - 1) * dx, (coefficient - 1) * dy);
    arrayOfShapes_[i]->scale(coefficient);
  }
}

void kurbanova::CompositeShape::printInfo() const
{
  kurbanova::rectangle_t temp_rect = getFrameRect();
  std::cout << std::endl
    << "Quantity shape = " << shape_count_ << "\n"
    << "Area = " << getArea() << "\n"
    << "Frame rectangle height = " << temp_rect.height
    << ", width = " << temp_rect.width
    << ", center (" << temp_rect.pos.x << ";"
    << temp_rect.pos.y << ")" << std::endl;
}

size_t kurbanova::CompositeShape::getShapeCount() const
{
  return shape_count_;
}

void kurbanova::CompositeShape::add(p_shape shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("shape can't be zero");
  }

  std::unique_ptr<p_shape[]> new_shape_array(std::make_unique<p_shape[]>(shape_count_ + 1));

  for (size_t i = 0; i < shape_count_; ++i)

  {
    new_shape_array[i] = arrayOfShapes_[i];
  }
  new_shape_array[shape_count_] = shape;
  shape_count_++;
  arrayOfShapes_.swap(new_shape_array);
}

void kurbanova::CompositeShape::removeShape(size_t index)
{
  if (index >= shape_count_)
  {
    throw std::out_of_range("Index out of range");
  }

  std::unique_ptr<p_shape[]> new_shape_array(std::make_unique<p_shape[]>(shape_count_ - 1));

  for (size_t i = 0; i < index; ++i)
  {
    new_shape_array[i] = arrayOfShapes_[i];
  }

  shape_count_--;

  for (size_t i = index; i < shape_count_; ++i)
  {
    new_shape_array[i] = arrayOfShapes_[i + 1];
  }

  arrayOfShapes_.swap(new_shape_array);
}

void kurbanova::CompositeShape::removeShape(p_shape shape)
{
  for (size_t i = 0; i < shape_count_; ++i)
  {
    if (shape == arrayOfShapes_[i])
    {
      removeShape(i);
      return;
    }
  }

  throw std::invalid_argument("Shape doesn't exist");
}

void kurbanova::CompositeShape::rotate(double angle)
{
  const point_t center = getCenter();
  const double cos_angle = cos(angle);
  const double sin_angle = sin(angle);

  for (size_t i = 0; i < shape_count_; ++i)
  {
    const point_t other_center = arrayOfShapes_[i]->getCenter();
    double point_x = { center.x + (other_center.x - center.x) * cos_angle
        - (other_center.y - center.y) * sin_angle };
    double point_y = { center.y + (other_center.x - center.x) * sin_angle
        - (other_center.y - center.y) * cos_angle };

    arrayOfShapes_[i]->move({ point_x, point_y });
    arrayOfShapes_[i]->rotate(angle);
  }
}

kurbanova::point_t kurbanova::CompositeShape::getCenter() const
{
  kurbanova::rectangle_t frame_rect = arrayOfShapes_[0]->getFrameRect();
  double max_x = frame_rect.pos.x + frame_rect.width / 2;
  double min_x = frame_rect.pos.x - frame_rect.width / 2;
  double max_y = frame_rect.pos.y + frame_rect.height / 2;
  double min_y = frame_rect.pos.y - frame_rect.height / 2;

  for (size_t i = 1; i < shape_count_; ++i)
  {
    frame_rect = arrayOfShapes_[i]->getFrameRect();
    max_x = std::max(max_x, frame_rect.pos.x + frame_rect.width / 2);
    min_x = std::min(min_x, frame_rect.pos.x - frame_rect.width / 2);
    max_y = std::max(max_y, frame_rect.pos.y + frame_rect.height / 2);
    min_y = std::min(min_y, frame_rect.pos.y - frame_rect.height / 2);
  }
  return { (max_x + min_x) / 2, (max_y + min_y) / 2 };
}
