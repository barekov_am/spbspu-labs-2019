#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

#include <memory>

namespace kurbanova
{
  struct point_t
  {
    double x, y;
  };

  struct rectangle_t
  {
    point_t pos;
    double width, height;
  };

  struct pair_t
  {
    size_t line, column;
  };
}
#endif // !BASE_TYPES_HPP
