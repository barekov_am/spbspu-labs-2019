#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include "rectangle.hpp"

using p_shape = std::shared_ptr<kurbanova::Shape>;

BOOST_AUTO_TEST_SUITE(testImplementationOfPartition)

BOOST_AUTO_TEST_CASE(correctWorkingIsOverlapping)
{
  kurbanova::point_t center1 = { -100, -100 };
  kurbanova::point_t center2 = { 100, 100 };

  kurbanova::Rectangle rectangle1(center1, 2, 4);
  kurbanova::Rectangle rectangle2(center1, 4, 5);
  kurbanova::Circle circle(center2, 5);

  p_shape p_rect1 = std::make_shared<kurbanova::Rectangle>(rectangle1);
  p_shape p_rect2 = std::make_shared<kurbanova::Rectangle>(rectangle2);
  p_shape p_circle = std::make_shared<kurbanova::Circle>(circle);

  BOOST_CHECK(!kurbanova::isOverlapping(p_rect1, p_circle));
  BOOST_CHECK(kurbanova::isOverlapping(p_rect1, p_rect2));
}

BOOST_AUTO_TEST_CASE(correctWorkingOfPart)
{
  kurbanova::point_t center1 = { 14, 56 };
  kurbanova::point_t center2 = { -53, 123 };
  kurbanova::point_t center3 = { 59, -68 };
  kurbanova::point_t center4 = { 39, -58 };

  kurbanova::Circle circle1(center1, 5);
  kurbanova::Circle circle2(center2, 9);
  kurbanova::Circle circle3(center3, 13);
  kurbanova::Rectangle rectangle1(center1, 2, 7);
  kurbanova::Rectangle rectangle2(center4, 8, 10);
  kurbanova::Rectangle rectangle3(center3, 5, 15);
  const size_t lines = 2;
  const size_t columns = 4;

  p_shape p_rect1 = std::make_shared<kurbanova::Rectangle>(rectangle1);
  p_shape p_rect2 = std::make_shared<kurbanova::Rectangle>(rectangle2);
  p_shape p_rect3 = std::make_shared<kurbanova::Rectangle>(rectangle3);
  p_shape p_circle1 = std::make_shared<kurbanova::Circle>(circle1);
  p_shape p_circle2 = std::make_shared<kurbanova::Circle>(circle2);
  p_shape p_circle3 = std::make_shared<kurbanova::Circle>(circle3);

  kurbanova::CompositeShape composite_shape;
  composite_shape.add(p_circle1);
  composite_shape.add(p_circle2);
  composite_shape.add(p_circle3);
  composite_shape.add(p_rect1);
  composite_shape.add(p_rect2);
  composite_shape.add(p_rect3);
  kurbanova::Matrix matrix = kurbanova::part(composite_shape);

  BOOST_CHECK_EQUAL(lines, matrix.getLines());
  BOOST_CHECK_EQUAL(columns, matrix.getColumns());

  BOOST_CHECK(matrix[0][0] == p_circle1);
  BOOST_CHECK(matrix[0][1] == p_circle2);
  BOOST_CHECK(matrix[0][2] == p_circle3);
  BOOST_CHECK(matrix[0][3] == p_rect2);
  BOOST_CHECK(matrix[1][0] == p_rect1);
  BOOST_CHECK(matrix[1][1] == p_rect3);
}

BOOST_AUTO_TEST_SUITE_END()
