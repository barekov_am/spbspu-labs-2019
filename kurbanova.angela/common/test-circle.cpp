#define _USE_MATH_DEFINES
#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include <iostream>
#include <cmath>
#include "circle.hpp"
#define INACCURACY 0.001

BOOST_AUTO_TEST_SUITE(testCircle)

BOOST_AUTO_TEST_CASE(invariabilityRadAfterMoveInPoint)
{
  kurbanova::Circle test_circle({ 12, -17 }, 4);
  const double rad = test_circle.getRad();
  test_circle.move({ 6, 20 });
  BOOST_CHECK_CLOSE(test_circle.getRad(), rad, INACCURACY);
}

BOOST_AUTO_TEST_CASE(invariabilityRadAfterMoveDxDy)
{
  kurbanova::Circle test_circle({ 12, -17 }, 4);
  const double rad = test_circle.getRad();
  test_circle.move(5, 5);
  BOOST_CHECK_CLOSE(test_circle.getRad(), rad, INACCURACY);
}

BOOST_AUTO_TEST_CASE(invariabilityAreaAfterMoveInPoint)
{
  kurbanova::Circle test_circle({ 12, -17 }, 4);
  const double area = test_circle.getArea();
  test_circle.move({ 6, 20 });
  BOOST_CHECK_CLOSE(test_circle.getArea(), area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(invariabilityAreaAfterMoveDxDy)
{
  kurbanova::Circle test_circle({ 12, -17 }, 4);
  const double area = test_circle.getArea();
  test_circle.move(5, 5);
  BOOST_CHECK_CLOSE(test_circle.getArea(), area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(changingAreaAfterScaleAnIncrease)
{
  kurbanova::Circle test_circle({ 10, 10 }, 4);
  const double area = test_circle.getArea();
  const double increase_coef = 2;
  test_circle.scale(increase_coef);
  BOOST_CHECK_CLOSE(test_circle.getArea(), increase_coef * increase_coef * area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(changingAreaAfterScaleAnDecrease)
{
  kurbanova::Circle test_circle({ 10, 10 }, 4);
  const double area = test_circle.getArea();
  const double decrease_coef = 0.5;
  test_circle.scale(decrease_coef);
  BOOST_CHECK_CLOSE(test_circle.getArea(), decrease_coef * decrease_coef * area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(throwExceptionRad)
{
  BOOST_CHECK_THROW(kurbanova::Circle test_circle({ 10, 10 }, -4), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(throwExceptionAfterScale)
{
  kurbanova::Circle test_circle({ 10, 10 }, 4);
  BOOST_CHECK_THROW(test_circle.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(immutabilityCenterAfterRotating)
{
  kurbanova::Circle circle({ 10, 10 }, 20);
  const kurbanova::point_t center = circle.getCenter();

  circle.rotate(M_PI / 2);
  BOOST_CHECK_CLOSE(center.x, circle.getCenter().x, INACCURACY);
  BOOST_CHECK_CLOSE(center.y, circle.getCenter().y, INACCURACY);
}

BOOST_AUTO_TEST_CASE(immutabilityAreaAfterRotating)
{
  kurbanova::Circle circle({ 10, 10 }, 20);
  const double area = circle.getArea();

  circle.rotate(M_PI / 2);
  BOOST_CHECK_CLOSE(area, circle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(immutabilityFrameRectAfterRotating360)
{
  kurbanova::Circle circle({ 10, 10 }, 30);
  const double width = circle.getFrameRect().width;
  const double height = circle.getFrameRect().height;

  circle.rotate(2 * M_PI);
  BOOST_CHECK_CLOSE(width, circle.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(height, circle.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(swapSidesOfFrameRectAfterRotating90)
{
  kurbanova::Circle circle({ 10, 10 }, 20);
  const double width = circle.getFrameRect().width;
  const double height = circle.getFrameRect().height;

  circle.rotate(M_PI / 2);
  BOOST_CHECK_CLOSE(width, circle.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(height, circle.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(correctWorkingMethodGetCenter)
{
  kurbanova::point_t center = { 10, 10 };
  kurbanova::Circle circle(center, 20);

  BOOST_CHECK_CLOSE(center.x, circle.getCenter().x, INACCURACY);
  BOOST_CHECK_CLOSE(center.y, circle.getCenter().y, INACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
