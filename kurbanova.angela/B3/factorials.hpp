#ifndef FACTORIALS_HPP
#define FACTORIALS_HPP

#include <iostream>

void printFactorials(std::ostream &out);

#endif
