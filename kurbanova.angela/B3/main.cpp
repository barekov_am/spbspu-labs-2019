#include <iostream>
#include "phonebookInterface.hpp"
#include "factorials.hpp"

int main(int args, char *argv[])
{
    if(args != 2)
    {
        std::cerr << "number parameters args incorrect \n";
        return 1;
    }

    try
    {
        auto number = std::stoi(argv[1]);

        switch(number)
        {
            case 1:
                openPhonebookInterface(std::cin, std::cout);
                break;

            case 2:
                printFactorials(std::cout);
                break;

            default:

                std::cerr << "Wrong task number";
                return 1;
        }

    }

    catch(const std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}
