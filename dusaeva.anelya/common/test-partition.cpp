#include <boost/test/auto_unit_test.hpp>

#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "polygon.hpp"
#include "triangle.hpp"
#include "partition.hpp"

const double DELTA = 1e-10;

BOOST_AUTO_TEST_SUITE(testPartition)
  
  BOOST_AUTO_TEST_CASE(intersectCorrectness)
  {
    dusaeva::Circle testCircle({5.0, 6.0}, 3.0);
    dusaeva::Rectangle testRectangle({5.0, 6.0}, 10.0, 4.5);
    dusaeva::Circle testCircle2 ({20.0, 10.0}, 1.0);
  
    BOOST_CHECK(dusaeva::intersect(testCircle.getFrameRect(),testRectangle.getFrameRect()));
    BOOST_CHECK(!(dusaeva::intersect(testCircle2.getFrameRect(),testRectangle.getFrameRect())));
    BOOST_CHECK(!(dusaeva::intersect(testCircle.getFrameRect(),testCircle2.getFrameRect())));
  }
  
  BOOST_AUTO_TEST_CASE(partitionCorrectness)
  {
    dusaeva::Shape::ptr_type circlePtr = std::make_shared<dusaeva::Circle>(dusaeva::point_t{20.0, 10.0}, 1.0);
    dusaeva::Shape::ptr_type rectanglePtr = std::make_shared<dusaeva::Rectangle>(dusaeva::point_t{1.0, 2.0}, 4.0, 5.0);
    dusaeva::Shape::ptr_type trianglePtr = std::make_shared<dusaeva::Triangle>(dusaeva::point_t{4.0, 10.0},
                                                                               dusaeva::point_t{15.0, 6.0}, dusaeva::point_t{1.0, 1.0});
    dusaeva::point_t *points = new dusaeva::point_t[3]{{4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}};
    dusaeva::Shape::ptr_type polygonPtr = std::make_shared<dusaeva::Polygon>(3, points);
    delete[] points;
    
    dusaeva::CompositeShape compositeShape;
    
    compositeShape.add(circlePtr);
    compositeShape.add(rectanglePtr);
    compositeShape.add(trianglePtr);
    compositeShape.add(polygonPtr);
    
    dusaeva::Matrix testMatrix = dusaeva::partition(compositeShape);
    
    const std::size_t correctRows = 3;
    const std::size_t correctColumns = 2;
    
    BOOST_CHECK_EQUAL(testMatrix.getRows(), correctRows);
    BOOST_CHECK_EQUAL(testMatrix.getColumns(), correctColumns);
    BOOST_CHECK(testMatrix[0][0] == circlePtr);
    BOOST_CHECK(testMatrix[0][1] == rectanglePtr);
    BOOST_CHECK(testMatrix[1][0] == trianglePtr);
    BOOST_CHECK(testMatrix[2][0] == polygonPtr);
  }

BOOST_AUTO_TEST_SUITE_END()
