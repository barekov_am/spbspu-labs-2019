#include "composite-shape.hpp"
#include <stdexcept>
#include <cmath>
#include <memory>

shestakova::CompositeShape::CompositeShape() :
  count_(0)
{}

shestakova::CompositeShape::CompositeShape(const CompositeShape& copiedCompositeShape) :
  count_(copiedCompositeShape.count_),
  figures_(std::make_unique<ptr[]>(copiedCompositeShape.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    figures_[i] = copiedCompositeShape.figures_[i];
  }
}

shestakova::CompositeShape::CompositeShape(CompositeShape &&movedCompositeShape) :
  count_(movedCompositeShape.count_),
  figures_(std::move(movedCompositeShape.figures_))
{
  movedCompositeShape.count_ = 0;
}

shestakova::CompositeShape::CompositeShape(const ptr &shape) :
  count_(1),
  figures_(std::make_unique<ptr[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer can't be null");
  }

  figures_[0] = shape;
}

shestakova::CompositeShape &shestakova::CompositeShape::operator =(const CompositeShape &copiedCompositeShape)
{
  if (this != &copiedCompositeShape)
  {
    array shapesArray(std::make_unique<ptr[]>(copiedCompositeShape.count_));
    count_ = copiedCompositeShape.count_;
    for (size_t i = 0; i < count_; i++)
    {
      shapesArray[i] = copiedCompositeShape.figures_[i];
    }

    figures_.swap(shapesArray);
  }

  return *this;
}

shestakova::CompositeShape &shestakova::CompositeShape::operator =(CompositeShape &&movedCompositeShape)
{
  if (this != &movedCompositeShape)
  {
    count_ = movedCompositeShape.count_;
    figures_ = std::move(movedCompositeShape.figures_);
    movedCompositeShape.count_ = 0;
  }

  return *this;
}

shestakova::Shape::ptr shestakova::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }

  return figures_[index];
}

double shestakova::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    area += figures_[i]->getArea();
  }

  return area;
}

shestakova::rectangle_t shestakova::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty!");
  }

  rectangle_t tmpShape = figures_[0]->getFrameRect();
  double minX = tmpShape.pos.x - tmpShape.width / 2;
  double minY = tmpShape.pos.y - tmpShape.height / 2;
  double maxX = tmpShape.pos.x + tmpShape.width / 2;
  double maxY = tmpShape.pos.y + tmpShape.height / 2;

  for (size_t i = 0; i < count_; i++)
  {
    tmpShape = figures_[i]->getFrameRect();
    double tmpValue = tmpShape.pos.x - tmpShape.width / 2;
    minX = std::min(tmpValue, minX);
    tmpValue = tmpShape.pos.y - tmpShape.height / 2;
    minY = std::min(tmpValue, minY);
    tmpValue = tmpShape.pos.x + tmpShape.width / 2;
    maxX = std::max(tmpValue, maxX);
    tmpValue = tmpShape.pos.y + tmpShape.height / 2;
    maxY = std::max(tmpValue, maxY);
  }

  return {(maxX - minX), (maxY - minY), {(maxX + minX) / 2, (maxY + minY) / 2}};
}

void shestakova::CompositeShape::move(const point_t &point)
{
  point_t frameRectPos = getFrameRect().pos;
  double dx = point.x - frameRectPos.x;
  double dy = point.y - frameRectPos.y;

  move(dx, dy);
}

void shestakova::CompositeShape::move(double dx, double dy)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  for (size_t i = 0; i < count_; i++)
  {
    figures_[i]->move(dx, dy);
  }
}

void shestakova::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient can't be negative.");
  }

  const shestakova::point_t frameRectPos = getFrameRect().pos;
  for (size_t i = 0; i < count_; i++)
  {
    shestakova::point_t shapeCenter = figures_[i]->getFrameRect().pos;
    const double dx = (shapeCenter.x - frameRectPos.x) * (coefficient - 1);
    const double dy = (shapeCenter.y - frameRectPos.y) * (coefficient - 1);

    figures_[i]->move(dx, dy);
    figures_[i]->scale(coefficient);
  }
}

void shestakova::CompositeShape::add(ptr &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer can't be null");
  }

  array shapesArray(std::make_unique<ptr[]>(count_ + 1));
  for(size_t i = 0; i < count_; i++)
  {
    shapesArray[i] = figures_[i];
  }

  shapesArray[count_ ] = shape;
  count_++;
  figures_.swap(shapesArray);
}

void shestakova::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::invalid_argument("Index out of range");
  }

  count_--;
  for (size_t i = index; i < count_ ; i++)
  {
    figures_[i] = figures_[i + 1];
  }

  figures_[count_] = nullptr;
}

size_t shestakova::CompositeShape::getCount() const
{
  return count_;
}

shestakova::Shape::array shestakova::CompositeShape::getFigures() const
{
  array tmpFigures(std::make_unique<ptr[]>(count_));

  for (size_t i = 0; i < count_; i++)
  {
    tmpFigures[i] = figures_[i];
  }

  return tmpFigures;
}

void shestakova::CompositeShape::rotate(double angle)
{
  const double cosA = std::abs(std::cos(angle * M_PI / 180));
  const double sinA = std::abs(std::sin(angle * M_PI / 180));
  const point_t center = getFrameRect().pos;

  for (size_t i = 0; i < count_; i++)
  {
    point_t tmpCenter = figures_[i]->getFrameRect().pos;
    const double dx = (tmpCenter.x - center.x) * (cosA - 1) - (tmpCenter.y - center.y)  * sinA;
    const double dy = (tmpCenter.x - center.x) * sinA + (tmpCenter.y - center.y)  * (cosA - 1);

    figures_[i]->move(dx, dy);
    figures_[i]->rotate(angle);
  }
}
