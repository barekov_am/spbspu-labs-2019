#include "matrix.hpp"
#include <stdexcept>
#include <cmath>

shestakova::Matrix::Matrix():
  lines_(0),
  columns_(0)
{}

shestakova::Matrix::Matrix(const Matrix &copiedMatrix):
  lines_(copiedMatrix.lines_),
  columns_(copiedMatrix.columns_),
  figures_(std::make_unique<Shape::ptr[]>(copiedMatrix.lines_ * copiedMatrix.columns_))
{
  for (size_t i = 0; i < (lines_  * columns_); i++)
  {
    figures_[i] = copiedMatrix.figures_[i];
  }
}

shestakova::Matrix::Matrix(shestakova::Matrix &&movedMatrix):
  lines_(movedMatrix.lines_),
  columns_(movedMatrix.columns_),
  figures_(std::move(movedMatrix.figures_))
{
  movedMatrix.lines_ = 0;
  movedMatrix.columns_ = 0;
}

shestakova::Matrix &shestakova::Matrix::operator =(const Matrix &copiedMatrix)
{
  if (this != &copiedMatrix)
  {
    Shape::array tmpFigures(std::make_unique<Shape::ptr[]>(copiedMatrix.lines_ * copiedMatrix.columns_));
    lines_ = copiedMatrix.lines_;
    columns_ = copiedMatrix.columns_;
    for (size_t i = 0; i < (lines_ * columns_); i++)
    {
      tmpFigures[i] = copiedMatrix.figures_[i];
    }

    figures_.swap(tmpFigures);
  }

  return *this;
}

shestakova::Matrix &shestakova::Matrix::operator =(Matrix &&movedMatrix)
{
  if (this != &movedMatrix)
  {
    lines_ = movedMatrix.lines_;
    columns_ = movedMatrix.columns_;
    figures_ = std::move(movedMatrix.figures_);
    movedMatrix.lines_ = 0;
    movedMatrix.columns_ = 0;
  }

  return *this;
}

shestakova::Shape::array shestakova::Matrix::operator [](size_t index) const
{
  if (index >= lines_)
  {
    throw std::out_of_range("Index out of range");
  }

  Shape::array tmpFigures(std::make_unique<Shape::ptr[]>(columns_));
  for (size_t i = 0; i < columns_; i++)
  {
    tmpFigures[i] = figures_[index * columns_ + i];
  }

  return tmpFigures;
}

bool shestakova::Matrix::operator==(const Matrix &shapesMatrix) const
{
  if ((lines_ != shapesMatrix.lines_) || (columns_ != shapesMatrix.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    if (figures_[i] != shapesMatrix.figures_[i])
    {
      return false;
    }
  }

  return true;
}

bool shestakova::Matrix::operator!=(const Matrix &shapesMatrix) const
{
  return !(*this == shapesMatrix);
}

void shestakova::Matrix::add(Shape::ptr shape, size_t line, size_t column)
{
  if (line > lines_)
  {
    throw std::out_of_range("Line index out of range");
  }

  if (column > columns_)
  {
    throw std::out_of_range("Column index out of range");
  }

  size_t newLine = (line == lines_) ? (lines_ + 1) : (lines_);
  size_t newColumn = (column == columns_) ? (columns_ + 1) : (columns_);

  Shape::array tmpFigures(std::make_unique<Shape::ptr[]> (newLine * newColumn));

  for (size_t i = 0; i < newLine; i++)
  {
    for (size_t j = 0; j < newColumn; ++j)
    {
      if ((lines_ == i) || (columns_ == j))
      {
        tmpFigures[i * newColumn + j] = nullptr;
      }
      else
      {
        tmpFigures[i * newColumn + j] = figures_[i * columns_ + j];
      }
    }
  }

  tmpFigures[line * newColumn + column] = shape;
  figures_.swap(tmpFigures);
  lines_ = newLine;
  columns_ = newColumn;
}

std::size_t shestakova::Matrix::getLines() const
{
  return lines_;
}

std::size_t shestakova::Matrix::getColumns() const
{
  return columns_;
}

bool shestakova::Matrix::containsObject(size_t line, size_t column) const
{
  if ((line >= lines_) || (column >= columns_))
  {
    return false;
  }

  if (figures_[line * columns_ + column] == nullptr)
  {
    return false;
  }

  return true;
}

