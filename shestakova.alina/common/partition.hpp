#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace shestakova
{
  Matrix part(const shestakova::Shape::array &figures, size_t count);
  Matrix part(const CompositeShape &compositeShape);

  bool intersect(const rectangle_t &firstShape, const rectangle_t &secondShape);
}

#endif
