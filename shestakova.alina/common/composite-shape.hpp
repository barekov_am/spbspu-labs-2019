#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <boost/mpl/size_t.hpp>
#include <memory>
#include "base-types.hpp"
#include "shape.hpp"

namespace shestakova
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &copiedCompositeShape);
    CompositeShape(CompositeShape &&movedCompositeShape);
    CompositeShape(const ptr &shape);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &copiedCompositeShape);
    CompositeShape &operator =(CompositeShape &&movedCompositeShape);
    ptr operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(double dx, double dy) override;
    void scale(double coefficient) override;
    void add(ptr &shape);
    void remove(size_t index);
    size_t getCount() const;
    array getFigures() const;
    void rotate(double angle) override;

  private:
    size_t count_;
    array figures_;
  };
}

#endif
