#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(matrixTests)

BOOST_AUTO_TEST_CASE(matrixCopyConstructor)
{
  shestakova::Shape::ptr rectPtr = std::make_shared<shestakova::Rectangle>(shestakova::point_t {4, 7}, 2, 12);
  shestakova::CompositeShape compSh(rectPtr);
  shestakova::Matrix copiedMatrix = part(compSh);

  shestakova::Matrix matrix(copiedMatrix);

  BOOST_CHECK_EQUAL(matrix.getLines(), copiedMatrix.getLines());
  BOOST_CHECK_EQUAL(matrix.getColumns(), copiedMatrix.getColumns());
  BOOST_CHECK(matrix == copiedMatrix);
}

BOOST_AUTO_TEST_CASE(matrixCopyOperator)
{
  shestakova::Shape::ptr rectPtr = std::make_shared<shestakova::Rectangle>(shestakova::point_t {4, 7}, 2, 12);
  shestakova::CompositeShape compSh(rectPtr);
  shestakova::Matrix copiedMatrix = part(compSh);

  shestakova::Matrix matrix;
  matrix = copiedMatrix;

  BOOST_CHECK_EQUAL(matrix.getLines(), copiedMatrix.getLines());
  BOOST_CHECK_EQUAL(matrix.getColumns(), copiedMatrix.getColumns());
  BOOST_CHECK(matrix == copiedMatrix);
}

BOOST_AUTO_TEST_CASE(matrixMoveConstructor)
{
  shestakova::Shape::ptr rectPtr = std::make_shared<shestakova::Rectangle>(shestakova::point_t {4, 7}, 2, 12);
  shestakova::CompositeShape compSh(rectPtr);
  shestakova::Matrix matrix = part(compSh);

  shestakova::Matrix copyOfMatrix(matrix);
  shestakova::Matrix moveMatrix(std::move(matrix));

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyOfMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyOfMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyOfMatrix);
  BOOST_CHECK_EQUAL(matrix.getLines(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(matrixMoveOperator)
{
  shestakova::Shape::ptr rectPtr = std::make_shared<shestakova::Rectangle>(shestakova::point_t {4, 7}, 2, 12);
  shestakova::CompositeShape compSh(rectPtr);
  shestakova::Matrix matrix = part(compSh);

  shestakova::Matrix copyOfMatrix(matrix);
  shestakova::Matrix moveMatrix;
  moveMatrix = std::move(matrix);

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyOfMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyOfMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyOfMatrix);
  BOOST_CHECK_EQUAL(matrix.getLines(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(testMatrixUsingOfEqualOperator)
{
  shestakova::Shape::ptr rect1Ptr = std::make_shared<shestakova::Rectangle>(shestakova::point_t {1, 2}, 5, 4);
  shestakova::Shape::ptr rect2Ptr = std::make_shared<shestakova::Rectangle>(shestakova::point_t {7, 7}, 3, 1);
  shestakova::Shape::ptr circ1Ptr = std::make_shared<shestakova::Circle>(shestakova::point_t {-1, 2}, 2);
  shestakova::Shape::ptr circ2Ptr = std::make_shared<shestakova::Circle>(shestakova::point_t {0, 1}, 4);

  shestakova::CompositeShape compSh(rect1Ptr);
  compSh.add(circ1Ptr);
  compSh.add(rect2Ptr);

  shestakova::Matrix matrix = part(compSh);
  shestakova::Matrix equalMatrix(matrix);

  shestakova::Matrix unequalMatrix(matrix);
  unequalMatrix.add(circ2Ptr, 1, 1);

  BOOST_CHECK(matrix == equalMatrix);
  BOOST_CHECK(matrix != unequalMatrix);
}

BOOST_AUTO_TEST_CASE(matrixThrowExceptionAfterUsingOfOperator)
{
  shestakova::Shape::ptr rect1Ptr = std::make_shared<shestakova::Rectangle>(shestakova::point_t {1, 2}, 5, 4);
  shestakova::Shape::ptr rect2Ptr = std::make_shared<shestakova::Rectangle>(shestakova::point_t {7, 7}, 3, 1);
  shestakova::Shape::ptr circ1Ptr = std::make_shared<shestakova::Circle>(shestakova::point_t {-1, 2}, 2);
  shestakova::Shape::ptr circ2Ptr = std::make_shared<shestakova::Circle>(shestakova::point_t {0, 1}, 4);

  shestakova::CompositeShape compSh(rect1Ptr);
  compSh.add(circ1Ptr);
  compSh.add(rect2Ptr);
  compSh.add(circ2Ptr);

  shestakova::Matrix matrix = part(compSh);

  BOOST_CHECK_THROW(matrix[10][5], std::out_of_range);
  BOOST_CHECK_NO_THROW(matrix[2][1]);
}

BOOST_AUTO_TEST_SUITE_END()
