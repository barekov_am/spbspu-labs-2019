#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(partitionMetodsTests)

BOOST_AUTO_TEST_CASE(partitionTest)
{
  const shestakova::Rectangle rect1({0, 0}, 2, 1);
  shestakova::Shape::ptr rect1Ptr = std::make_shared<shestakova::Rectangle>(rect1);
  const shestakova::Rectangle rect2({15, 10}, 2, 3);
  shestakova::Shape::ptr rect2Ptr = std::make_shared<shestakova::Rectangle>(rect2);
  const shestakova::Circle circ({1, -1}, 3);
  shestakova::Shape::ptr circPtr = std::make_shared<shestakova::Circle>(circ);

  shestakova::CompositeShape compSh(rect1Ptr);
  shestakova::Matrix matrix1 = shestakova::part(compSh);

  compSh.add(circPtr);
  shestakova::Matrix matrix2 = shestakova::part(compSh);

  compSh.add(rect2Ptr);
  shestakova::Matrix matrix3 = shestakova::part(compSh);

  BOOST_CHECK_EQUAL(matrix1.getLines(), 1);
  BOOST_CHECK_EQUAL(matrix1.getColumns(), 1);

  BOOST_CHECK_EQUAL(matrix2.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix2.getColumns(), 1);

  BOOST_CHECK_EQUAL(matrix3.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix3.getColumns(), 2);
}

BOOST_AUTO_TEST_CASE(intersectionTest)
{
  const shestakova::Rectangle rect1({1, 2}, 5, 4);
  const shestakova::Rectangle rect2({7, 7}, 3, 1);
  const shestakova::Circle circ1({-1, 2}, 2);
  const shestakova::Circle circ2({0, 1}, 4);

  BOOST_CHECK(intersect(rect1.getFrameRect(), circ1.getFrameRect()));
  BOOST_CHECK(intersect(rect1.getFrameRect(), circ2.getFrameRect()));
  BOOST_CHECK(!intersect(rect1.getFrameRect(), rect2.getFrameRect()));
  BOOST_CHECK(!intersect(circ1.getFrameRect(), rect2.getFrameRect()));
  BOOST_CHECK(!intersect(circ2.getFrameRect(), rect2.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
