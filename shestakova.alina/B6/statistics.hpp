#ifndef B6_STATISTICS_HPP
#define B6_STATISTICS_HPP

#include <iostream>
#include <cstdio>

class Statistics
{
  public:
    Statistics();
    void operator()(long long int num);
    void print(std::ostream &stream);

  private:
    size_t count_;
    long long int max_;
    long long int min_;
    size_t positive_;
    size_t negative_;
    long long int oddSum_;
    long long int evenSum_;
    long long int first_;
    bool firstAndLastElementEquality_;
    long double  mean_;
};

#endif
