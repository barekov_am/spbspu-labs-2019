#include <algorithm>
#include <functional>
#include <iterator>
#include "statistics.hpp"

int main()
{
  try
  {
    Statistics stat;
    std::for_each(std::istream_iterator<long long int>(std::cin), std::istream_iterator<long long int>(), std::ref(stat));

    if (!std::cin.eof())
    {
      std::cerr << "Failed during reading data";
      return 1;
    }

    stat.print(std::cout);
  }
  catch (const std::exception &exception)
  {
    std::cerr << exception.what();
    return 1;
  }

  return 0;
}
