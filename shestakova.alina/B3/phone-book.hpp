#ifndef B3_PHONE_BOOK_HPP
#define B3_PHONE_BOOK_HPP

#include <iterator>
#include <list>
#include <string>

class PhoneBook
{
  public:

    struct record_t
    {
      std::string name;
      std::string number;
    };


    using iterator = std::list<record_t>::iterator;

    void showTemporaryRecord(iterator iter);
    iterator next(iterator iter);
    iterator previous(iterator iter);
    iterator move(iterator iter, int steps);

    iterator insert(iterator iter, record_t &record);
    void pushBack(record_t &record);
    iterator replaceTemporaryRecord(iterator iter, record_t &record);
    iterator remove(iterator iter);

    iterator begin();
    iterator end();

    bool empty();

  private:
    std::list<record_t> list_;
};

#endif
