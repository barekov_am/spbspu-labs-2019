#ifndef B3_ITERATOR_HPP
#define B3_ITERATOR_HPP

#include <iterator>

class FactorialIterator: public std::iterator<std::bidirectional_iterator_tag, unsigned long long>
{
  public:
    FactorialIterator();
    FactorialIterator(int index);

    unsigned long long &operator *();
    unsigned long long *operator ->();

    FactorialIterator &operator ++();
    FactorialIterator operator ++(int);

    FactorialIterator &operator --();
    FactorialIterator operator --(int);

    bool operator ==(FactorialIterator iter) const;
    bool operator !=(FactorialIterator iter) const;

  private:
    unsigned long long value_;
    int index_;

    unsigned long long getValue(int index) const;
};

#endif
