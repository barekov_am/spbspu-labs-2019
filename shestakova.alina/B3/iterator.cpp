#include "iterator.hpp"

const int MIN = 1;
const int MAX = 10;

FactorialIterator::FactorialIterator():
  value_(1),
  index_(1)
{}

FactorialIterator::FactorialIterator(int index):
  value_(getValue(index)),
  index_(index)
{
  if ((index < MIN) || (index > MAX + 1))
  {
    throw std::out_of_range("Index is out of range");
  }
}

unsigned long long &FactorialIterator::operator*()
{
  return value_;
}

unsigned long long *FactorialIterator::operator->()
{
  return &value_;
}

FactorialIterator &FactorialIterator::operator++()
{
  if (index_ > MAX)
  {
    throw std::out_of_range("Index is out of range");
  }

  ++index_;
  value_ *= index_;
  return *this;
}

FactorialIterator FactorialIterator::operator++(int)
{
  FactorialIterator tempIterator = *this;
  ++(*this);
  return tempIterator;
}

FactorialIterator &FactorialIterator::operator--()
{
  if (index_ <= MIN)
  {
    throw std::out_of_range("Index is out of range");
  }

  value_ /= index_;
  --index_;
  return *this;
}

FactorialIterator FactorialIterator::operator--(int)
{
  FactorialIterator tempIterator = *this;
  --(*this);
  return tempIterator;
}

bool FactorialIterator::operator ==(FactorialIterator iter) const
{
  return (value_ == iter.value_) && (index_ == iter.index_);
}

bool FactorialIterator::operator !=(FactorialIterator iter) const
{
  return !(*this == iter);
}

unsigned long long FactorialIterator::getValue(int index) const
{
  if (index <= MIN)
  {
    return 1;
  }

  return index * getValue(index - 1);
}
