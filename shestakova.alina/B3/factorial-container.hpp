#ifndef B3_FACTORIAL_CONTAINER_HPP
#define B3_FACTORIAL_CONTAINER_HPP

#include "iterator.hpp"

class FactorialContainer
{
  public:
    FactorialContainer() = default;

    FactorialIterator begin();
    FactorialIterator end();
};

#endif
