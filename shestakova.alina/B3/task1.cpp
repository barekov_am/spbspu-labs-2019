#include <iostream>
#include <sstream>

#include "phone-book-interface.hpp"

void executeCommand(PhoneBookInterface &phBookInter, std::string &ssCommand, std::stringstream &ss);

void task1()
{
  PhoneBookInterface bookInter;
  std::string line;

  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed during reading");
    }

    std::stringstream input(line);
    std::string command;
    input >> command;

    executeCommand(bookInter, command, input);
  }
}
