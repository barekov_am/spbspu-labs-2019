#include <iostream>
#include <sstream>
#include "phone-book-interface.hpp"

std::string getName(std::string &name)
{
  if ((name.front() != '\"') || (name.back() != '\"') || name.empty())
  {
    return "";
  }

  name.erase(name.begin());

  size_t i = 0;
  while ((i < name.size()) && (name[i] != '\"'))
  {
    if (name[i] == '\\')
    {
      if (name[i + 1] == '\"' && i + 2 < name.size())
      {
        name.erase(i, 1);
      }
      else
      {
        return "";
      }
    }
    ++i;
  }

  if (i == name.size())
  {
    return "";
  }

  name.erase(i);

  if (name.empty())
  {
    return "";
  }
  return name;
}


std::string getNumber(std::string &number)
{
  if (number.empty())
  {
    return "";
  }

  for (size_t i = 0; i < number.size(); i++)
  {
    if (!std::isdigit(number[i]))
    {
      return "";
    }
  }

  return number;
}

std::string getMarkName(std::string &name)
{
  if (name.empty())
  {
    return "";
  }

  for (size_t i = 0; i < name.size(); i++)
  {
    if (!isalnum(name[i]) && (name[i] != '-'))
    {
      return "";
    }
  }

  return name;
}



void executeCommand(PhoneBookInterface &phBookInter, std::string &ssCommand, std::stringstream &ss)
{
  if (ssCommand == "add")
  {
    std::string number;
    std::string name;
    std::string line;

    ss >> std::ws >> number;
    std::getline(ss >> std::ws, name);

    name = getName(name);
    number = getNumber(number);

    if (name.empty() || number.empty())
    {
      std::cout << "<INVALID COMMAND>\n";
      return;
    }

    PhoneBook::record_t record = {name, number};
    phBookInter.add(record);
  }
  else if (ssCommand == "store")
  {
    std::string markName;
    std::string newMarkName;

    ss >> std::ws >> markName;
    markName = getMarkName(markName);
    ss >> std::ws >> newMarkName;
    newMarkName = getMarkName(newMarkName);

    if (markName.empty() || newMarkName.empty())
    {
      std::cout << "<INVALID COMMAND>\n";
      return;
    }

    phBookInter.store(markName, newMarkName);
  }
  else if (ssCommand == "insert")
  {
    std::string direction;
    std::string markName;
    std::string number;
    std::string name;

    ss >> std::ws >> direction;
    ss >> std::ws >> markName;
    markName = getMarkName(markName);
    ss >> std::ws >> number;
    number = getNumber(number);
    std::getline(ss >> std::ws, name);
    name = getName(name);

    PhoneBookInterface::InsertPosition position;

    if (markName.empty() || name.empty() || number.empty())
    {
      std::cout << "<INVALID COMMAND>\n";
      return;
    }

    PhoneBook::record_t record = {name, number};

    if (direction == "before")
    {
      position = PhoneBookInterface::InsertPosition::before;
      phBookInter.insert(position, markName, record);
    }
    else if (direction == "after")
    {
      position = PhoneBookInterface::InsertPosition::after;
      phBookInter.insert(position, markName, record);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
  else if (ssCommand == "delete")
  {
    std::string markName;
    ss >> std::ws >> markName;

    markName = getMarkName(markName);

    if (markName.empty())
    {
      std::cout << "<INVALID COMMAND>\n";
      return;
    }

    phBookInter.remove(markName);
  }
  else if (ssCommand == "show")
  {
    std::string markName;
    ss >> std::ws >> markName;
    markName = getMarkName(markName);
    if (markName.empty())
    {
      std::cout << "<INVALID COMMAND>\n";
      return;
    }

    phBookInter.show(markName);
  }
  else if (ssCommand == "move")
  {
    std::string markName;
    std::string num;
    ss >> std::ws >> markName;
    markName = getMarkName(markName);

    if (markName.empty())
    {
      std::cout << "<INVALID COMMAND>\n";
      return;
    }

    ss >> std::ws >> num;

    if (num == "first")
    {
      PhoneBookInterface::MovePosition position = PhoneBookInterface::MovePosition::first;
      phBookInter.move(markName, position);
    }
    else if (num == "last")
    {
      PhoneBookInterface::MovePosition position = PhoneBookInterface::MovePosition::last;
      phBookInter.move(markName, position);
    }
    else
    {
      int sign = 1;
      if (num[0] == '-')
      {
        sign = -1;
        num.erase(num.begin());
      }
      else if (num[0] == '+')
      {
        num.erase(num.begin());
      }
      num = getNumber(num);

      if (num.empty())
      {
        std::cout << "<INVALID STEP>\n";
        return;
      }

      phBookInter.move(markName, (stoi(num) * sign));
    }
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}
