#include "factorial-container.hpp"

const int MIN = 1;
const int MAX =10;


FactorialIterator FactorialContainer::begin()
{
  return FactorialIterator(MIN);
}

FactorialIterator FactorialContainer::end()
{
  return FactorialIterator(MAX + 1);
}
