#include <iostream>
#include <stdexcept>
#include "text.hpp"

const int MIN_WIDTH = 25;

int main(int argc, char * argv[])
{
  try
  {
    if ((argc > 3) || (argc == 2))
    {
      throw std::invalid_argument("Wrong number of options");
    }

    size_t width = 40;

    if (argc == 3)
    {
      width = std::stoul(argv[2]);
      if ((std::string(argv[1]) != "--line-width") || (width < MIN_WIDTH))
      {
        throw std::invalid_argument("Wrong arguments");
      }
    }
    Text text(width);
    text.formatText();
  }
  catch (const std::exception &exception)
  {
    std::cerr << exception.what() << '\n';
    return 1;
  }

  return 0;
}
