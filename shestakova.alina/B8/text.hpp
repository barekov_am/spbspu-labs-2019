#ifndef B8_TEXT_HPP
#define B8_TEXT_HPP

#include <string>
#include <vector>

class Text
{
  public:
    Text(size_t width);

    void readText();
    void formatText();

  private:
    std::vector<std::string> text_;
    size_t outStringWidth_;
};

std::string readWord(char c);
std::string readNumber(char c);
std::string readHyphen(char c);
std::string readPunctuation(char c);
bool isNotOnePunctuation(std::vector<std::string> &vector);

#endif
