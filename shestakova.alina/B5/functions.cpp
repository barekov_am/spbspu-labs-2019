#include <iostream>
#include "shape.hpp"

const std::size_t RECT_VERTICES = 4;

Shape readPoints(std::string &line, std::size_t vertices)
{
  Shape shape;
  std::size_t openBracket;
  std::size_t semicolon;
  std::size_t closeBracket;

  for (std::size_t i = 0; i < vertices; i++)
  {
    if (line.empty())
    {
      throw std::invalid_argument("Invalid number of vertices");
    }

    while (line.find_first_of(" \t") == 0)
    {
      line.erase(0, 1);
    }

    openBracket = line.find_first_of('(');
    semicolon = line.find_first_of(';');
    closeBracket = line.find_first_of(')');

    if ((openBracket == std::string::npos) || (semicolon == std::string::npos) || (closeBracket == std::string::npos))
    {
      throw std::invalid_argument("Invalid point declaration");
    }

    Point_t point
      {
        std::stoi(line.substr(openBracket + 1, semicolon - openBracket - 1)),
        std::stoi(line.substr(semicolon + 1, closeBracket - semicolon -1))
      };

    line.erase(0, closeBracket + 1);

    shape.push_back(point);
  }

  while (line.find_first_of(" \t") == 0)
  {
    line.erase(0, 1);
  }

  if (!line.empty())
  {
    throw std::invalid_argument("Too big amount of points");
  }

  return shape;
}

void readWords(std::vector<Shape> &vector, std::string &line)
{
  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed during reading");
    }

    while (line.find_first_of(" \t") == 0)
    {
      line.erase(0, 1);
    }

    if (line.empty())
    {
      continue;
    }

    std::size_t pos = line.find_first_of('(');
    if (pos == std::string::npos)
    {
      throw std::invalid_argument("Invalid shape");
    }

    std::size_t numOfVertices = std::stoi(line.substr(0, pos));
    line.erase(0, pos);
    if (numOfVertices < 1)
    {
      throw std::invalid_argument("Invalid number of vertices");
    }

    Shape shape = readPoints(line, numOfVertices);

    vector.push_back(shape);
  }
}

bool isRectangle(const Shape &shape)
{
  int diag1 = (shape[0].x - shape[2].x) * (shape[0].x - shape[2].x) + (shape[0].y - shape[2].y) * (shape[0].y - shape[2].y);
  int diag2 = (shape[1].x - shape[3].x) * (shape[1].x - shape[3].x) + (shape[1].y - shape[3].y) * (shape[1].y - shape[3].y);

  return diag1 == diag2;
}

bool isSquare(const Shape &shape)
{
  if (!isRectangle(shape))
  {
    return false;
  }

  if (shape[1].x != shape[0].x)
  {
    if (std::abs(shape[1].x - shape[0].x) != std::abs(shape[2].y - shape[1].y))
    {
      return false;
    }
  }
  else
  {
    if (std::abs(shape[2].x - shape[1].x) != std::abs(shape[1].y - shape[0].y))
    {
      return false;
    }
  }
  return true;
}

bool isLess(const Shape &lhs, const Shape &rhs)
{
  if (lhs.size() < rhs.size())
  {
    return true;
  }
  if ((lhs.size() == RECT_VERTICES) && (rhs.size() == RECT_VERTICES))
  {
    if (isSquare(lhs))
    {
      if (isSquare(rhs))
      {
        return lhs[0].x < rhs[0].x;
      }
      return true;
    }
  }
  return false;
}
