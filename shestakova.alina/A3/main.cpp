#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

void printInfo(const shestakova::Shape::ptr &shape)
{
  shestakova::rectangle_t frame = shape->getFrameRect();
  std::cout << "Figure's center (" << frame.pos.x
            << "," << frame.pos.y << ")\n"
            <<"Rectangle frame width = " << frame.width
            <<"\nRectangle frame height = " << frame.height
            << ";\nArea = " << shape->getArea() << "\n";
}

int main()
{
  shestakova::Shape::ptr rectPtr = std::make_shared<shestakova::Rectangle>(shestakova::point_t {10, -2}, 8, 3);
  std::cout << "Created rectangle's parameters:\n";
  printInfo(rectPtr);
  std::cout << "\nRectangle with a new centre at (4, 3):\n";
  rectPtr->move({4, 3});
  printInfo(rectPtr);
  std::cout << "\nRectangle shifted with dx=11, dy=0.9:\n";
  rectPtr->move(11, 0.9);
  printInfo(rectPtr);
  std::cout << "\nScaled rectangle with coefficient=3:\n";
  rectPtr->scale(3);
  printInfo(rectPtr);

  shestakova::Shape::ptr circPtr = std::make_shared<shestakova::Circle>(shestakova::point_t {0.9, 2.3}, 3);
  std::cout << "\nCreated circle's parameters:\n";
  printInfo(circPtr);
  std::cout << "\nCircle with a new centre at (12, 11):\n";
  circPtr->move({12, 11});
  printInfo(circPtr);
  std::cout << "\nCircle shifted with dx=2.3, dy=4:\n";
  circPtr->move(2.3, 4);
  printInfo(circPtr);
  std::cout << "\nScaled circle with coefficient=0.5:\n";
  circPtr->scale(0.5);
  printInfo(circPtr);

  shestakova::CompositeShape compSh(rectPtr);
  std::cout << "\nCreated composite shape's parameters after adding figures:\n";
  compSh.add(circPtr);
  printInfo(std::make_shared<shestakova::CompositeShape>(compSh));
  std::cout << "\nComposite shape with a new center at (2, -3):\n";
  compSh.move({2, -3});
  printInfo(std::make_shared<shestakova::CompositeShape>(compSh));
  std::cout << "\nScaled composite shape with coefficient=3:\n";
  compSh.scale(3);
  printInfo(std::make_shared<shestakova::CompositeShape>(compSh));
  std::cout << "\nDelete composite shape's 1st component:\n";
  compSh.remove(0);
  printInfo(std::make_shared<shestakova::CompositeShape>(compSh));

  return 0;
}
