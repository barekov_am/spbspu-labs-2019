#include <algorithm>
#include <sstream>
#include <vector>
#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"

std::vector<shape_ptr> getShapes()
{
  std::vector<shape_ptr> vector;
  std::string line;

  while (std::getline(std::cin, line))
  {
    if (line.empty())
    {
      continue;
    }

    line.erase(remove(line.begin(), line.end(), ' '), line.end());
    line.erase(remove(line.begin(), line.end(), '\t'), line.end());

    if (line.empty() || line.size() == 1)
    {
      continue;
    }

    auto open_bracket = line.find_first_of('(');
    auto semicolon = line.find_first_of(';');
    auto close_bracket = line.find_first_of(')');

    if (open_bracket == std::string::npos || semicolon == std::string::npos || close_bracket == std::string::npos)
    {
      throw std::runtime_error("Failed during reading\n");
    }

    auto shape = line.substr(0, open_bracket);
    auto x = std::stod(line.substr(open_bracket + 1, semicolon - open_bracket + 1));
    auto y = std::stod(line.substr(semicolon + 1, close_bracket - semicolon + 1));


    if (shape == "TRIANGLE")
    {
      vector.emplace_back(std::make_shared<Triangle>(Triangle(x, y)));
    }
    else if (shape == "SQUARE")
    {
      vector.emplace_back(std::make_shared<Square>(Square(x, y)));
    }
    else if (shape == "CIRCLE")
    {
      vector.emplace_back(std::make_shared<Circle>(Circle(x, y)));
    }
    else
    {
      throw std::invalid_argument("invalid input");
    }
  }
  return vector;
}
