#ifndef B7_SHAPE_HPP
#define B7_SHAPE_HPP

#include <iostream>
#include <memory>

class Shape
{
  public:
    Shape(double x, double y);
    bool isMoreLeft(const Shape &shape) const;
    bool isUpper(const Shape &shape) const;
    virtual void draw(std::ostream&) const = 0;

  protected:
    double x_;
    double y_;
};

using shape_ptr = std::shared_ptr<Shape>;

#endif
