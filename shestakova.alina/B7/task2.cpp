#include <algorithm>
#include <iostream>
#include <vector>
#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"

std::vector<shape_ptr> getShapes();

void task2()
{
  std::vector <shape_ptr> vector = getShapes();

  std::cout << "Original:\n";
  std::for_each(vector.begin(), vector.end(),[](const shape_ptr shape){shape->draw(std::cout);});

  std::cout << "Left-Right:\n";
  std::sort(vector.begin(), vector.end(),[](const shape_ptr &shape1, const shape_ptr &shape2){return shape1->isMoreLeft(*shape2);});
  std::for_each(vector.begin(), vector.end(),[](const shape_ptr shape){shape->draw(std::cout);});

  std::cout << "Right-Left:\n";
  std::for_each(vector.rbegin(), vector.rend(),[](const shape_ptr shape){shape->draw(std::cout);});

  std::cout << "Top-Bottom:\n";
  std::sort(vector.begin(), vector.end(),[](const shape_ptr shape1, const shape_ptr shape2){return (shape1->isUpper(*shape2));});
  std::for_each(vector.begin(), vector.end(),[](const shape_ptr shape){shape->draw(std::cout);});

  std::cout << "Bottom-Top:\n";
  std::for_each(vector.rbegin(), vector.rend(),[](const shape_ptr shape){shape->draw(std::cout);});
}
