#include <iostream>
#include <sstream>
#include "data-struct.hpp"

void getData(std::vector<DataStruct> &vector);
bool isLess(const DataStruct &lhs, const DataStruct &rhs);
void sortVector(std::vector<DataStruct> &vector);
void printVector(std::vector<DataStruct> &vector);

void task()
{
  std::vector<DataStruct> vector;
  std::string line;

  while (std::cin.peek() != EOF)
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed during reading");
    }

    getData(vector);
  }

  sortVector(vector);
  printVector(vector);
}
