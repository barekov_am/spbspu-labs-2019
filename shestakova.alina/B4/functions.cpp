#include <algorithm>
#include <iostream>
#include <sstream>
#include "data-struct.hpp"

int getKey(std::stringstream &line)
{
  int key = 10;
  line >> key;
  if ((key > 5) || (key < -5))
  {
    throw std::invalid_argument("Wrong argument");
  }

  std::string string;
  std::getline(line, string, ',');
  if (!string.empty() || line.eof())
  {
    throw std::invalid_argument("Wrong argument");
  }

  return key;
}

void getData(std::vector<DataStruct> &vector)
{
  int key1;
  int key2;
  std::string str;

  std::string line;
  std::getline(std::cin, line);
  std::stringstream tmpLine(line);

  key1 = getKey(tmpLine);
  key2 = getKey(tmpLine);
  tmpLine >> std::ws;
  std::getline(tmpLine, str);

  if (str.empty())
  {
    throw std::invalid_argument("Wrong argument");
  }

  vector.push_back({key1, key2, str});
}

bool isLess(const DataStruct &lhs, const DataStruct &rhs)
{
  if (lhs.key1 < rhs.key1)
  {
    return true;
  }
  else if (lhs.key1 == rhs.key1)
  {
    if (lhs.key2 < rhs.key2)
    {
      return true;
    }
    else if (lhs.key2 == rhs.key2)
    {
      if (lhs.str.size() < rhs.str.size())
      {
        return true;
      }
    }
  }
  return false;
}

void sortVector(std::vector<DataStruct> &vector)
{
  if (!vector.empty())
  {
    std::sort(vector.begin(), vector.end(), isLess);
  }
}

void printVector(std::vector<DataStruct> &vector)
{
  for (auto i = vector.begin(); i != vector.end(); i++)
  {
    std::cout << i->key1 << "," << i->key2 << "," << i->str << "\n";
  }
}
