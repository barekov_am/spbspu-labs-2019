#ifndef B1_FUNCTIONS_HPP
#define B1_FUNCTIONS_HPP

#include <iostream>
#include "strategy.hpp"

bool isAscending(const char *direction);
void fillRandom(double *array, int size);

template <template <class T> class Access, typename T>
void sort(T &container, bool isAscendingDirection)
{
  auto begin = Access<T>::begin(container);
  auto end = Access<T>::end(container);

  for (auto i = begin; i != end; i++)
  {
    auto tmp = i;
    for (auto j = Access<T>::next(i); j != end; j++)
    {
      if (((Access<T>::get(container, tmp) > Access<T>::get(container, j)) && (isAscendingDirection))
        || ((Access<T>::get(container, tmp) < Access<T>::get(container, j)) && (!isAscendingDirection)))
      {
        tmp = j;
      }
    }

    if (tmp != i)
    {
      std::swap(Access<T>::get(container, i), Access<T>::get(container, tmp));
    }
  }
}


template <typename T>
void print(T &container)
{
  if (container.empty())
  {
    return;
  }

  for (const auto &i: container)
  {
    std::cout << i << " ";
  }

  std::cout << std::endl;
}

#endif
