#include <iostream>
#include <ctime>

void task1(const char *direction);
void task2(const char * inputFile);
void task3();
void task4(const char *direction, const int size);

const int BASE = 10;

int main(int argc, char *argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4))
    {
      std::cerr << "Wrong amount of arguments";
      return 1;
    }

    char *endPtr = nullptr;
    int num = std::strtol(argv[1], &endPtr, BASE);

    switch (num)
    {
      case 1:
      {
        if (argc != 3)
        {
          std::cerr << "You should input 3 arguments";
          return 1;
        }

        task1(argv[2]);
        break;
      }

      case 2:
      {
        if (argc != 3)
        {
          std::cerr << "You should input 3 arguments";
          return 1;
        }

        task2(argv[2]);
        break;
      }

      case 3:
      {
        if (argc != 2)
        {
          std::cerr << "You should input 2 arguments";
          return 1;
        }

        task3();
        break;
      }

      case 4:
      {
        if (argc != 4)
        {
          std::cerr << "You should input 4 arguments";
          return 1;
        }

        srand(time(NULL));
        task4(argv[2], std::stoi(argv[3]));
        break;
      }

      default:
      {
        std::cerr << "Wrong number of task";
        return 1;
      }
    }
  }

  catch (const std::exception &exception)
  {
    std::cerr << exception.what();
    return 1;
  }

  return 0;
}
