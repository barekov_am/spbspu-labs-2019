#include <cstring>
#include "functions.hpp"

bool isAscending(const char *direction)
{
  if (std::strcmp(direction, "ascending") == 0)
  {
    return true;
  }

  if (std::strcmp(direction, "descending") == 0)
  {
    return false;
  }

  throw std::invalid_argument("Wrong direction");
}

void fillRandom(double *array, int size)
{
  for (int i = 0; i < size; i++)
  {
    array[i] = (rand() % 21 - 10) / 10.0;
  }
}
