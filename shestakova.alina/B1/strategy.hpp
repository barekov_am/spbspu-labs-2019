#ifndef B1_STRATEGY_HPP
#define B1_STRATEGY_HPP

#include <iterator>

template <typename T>
struct AccessByBreckets
{
  static size_t begin(const T &)
  {
    return 0;
  }

  static size_t next(size_t index)
  {
    return index + 1;
  }

  static size_t end(const T &container)
  {
    return container.size();
  }

  static typename T::reference get(T &container, size_t index)
  {
    return container[index];
  }
};

template <typename T>
struct AccessByAt
{
  static size_t begin(const T &)
  {
    return 0;
  }

  static size_t next(size_t index)
  {
    return index + 1;
  }

  static size_t end(const T &container)
  {
    return container.size();
  }

  static typename T::reference get(T &container, size_t index)
  {
    return container.at(index);
  }
};

template <typename T>
struct AccessByIterator
{
  static typename T::iterator begin(T &container)
  {
    return container.begin();
  }

  static typename T::iterator next(typename T::iterator &iterator)
  {
    return std::next(iterator);
  }

  static typename T::iterator end(T &container)
  {
    return container.end();
  }

  static typename T::reference get(T &, typename T::iterator &iterator)
  {
    return *iterator;
  }

};


#endif
