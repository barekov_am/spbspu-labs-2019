#include <forward_list>
#include <vector>
#include "functions.hpp"

void task1(const char *direction)
{
  bool isAsc = isAscending(direction);
  std::vector<int> vector1;
  int nextNum = -1;

  while (std::cin && !(std::cin >> nextNum).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input failed");
    }

    vector1.push_back(nextNum);
  }

  std::vector<int> vector2 = vector1;
  std::forward_list<int> listIt(vector1.begin(), vector1.end());

  sort<AccessByBreckets>(vector1, isAsc);
  sort<AccessByAt>(vector2, isAsc);
  sort<AccessByIterator>(listIt, isAsc);

  print(vector1);
  print(vector2);
  print(listIt);
}
