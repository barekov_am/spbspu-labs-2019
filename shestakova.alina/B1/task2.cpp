#include <iostream>
#include <fstream>
#include <memory>
#include <vector>

const size_t SIZE = 512;

void task2(const char *inputFile)
{
  std::ifstream input(inputFile);

  if (!input)
  {
    throw std::invalid_argument("Troubles during reading from the file\n");
  }

  size_t size = SIZE;

  using arrayOfChar = std::unique_ptr<char[], decltype(&free)>;
  arrayOfChar ch_arr(static_cast<char *>(malloc(size)), &free);

  if (!ch_arr)
  {
    throw std::runtime_error("Failed during memory allocation");
  }

  size_t count = 0;

  while (input)
  {
    input.read(&ch_arr[count], SIZE);
    count += input.gcount();

    if (input.gcount() == SIZE)
    {
      size += SIZE;
      arrayOfChar tmp_array(static_cast<char *>(realloc(ch_arr.get(), size)), &free);

      if (!tmp_array)
      {
        throw std::runtime_error("Failed during memory reallocation");
      }

      ch_arr.release();
      std::swap(ch_arr, tmp_array);
    }

    if (!input.eof() && input.fail())
    {
      throw std::runtime_error("Failed reading the file.");
    }

  }

  std::vector<char> vector(&ch_arr[0], &ch_arr[count]);
  for (const auto &it: vector)
  {
    std::cout << it;
  }

}
