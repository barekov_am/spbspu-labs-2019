#include <vector>
#include "functions.hpp"

void task3()
{
  std::vector<int> vector;
  int nextNum = -1;

  while (std::cin && !(std::cin >> nextNum).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error in input\n");
    }
    if (nextNum == 0)
    {
      break;
    }
    vector.push_back(nextNum);
  }

  if (vector.empty())
  {
    return;
  }

  if (std::cin.eof() && (nextNum != 0))
  {
    throw std::invalid_argument("In the end of line there should be '0' ");
  }

  auto i = vector.begin();

  switch (vector.back())
  {
    case 1:
    {
      while (i != vector.end())
      {
        if ((*i % 2) == 0)
        {
          i = vector.erase(i);
        }
        else
        {
          ++i;
        }
      }
      break;
    }

    case 2:
    {
      while (i != vector.end())
      {
        if ((*i % 3) == 0)
        {
          i = vector.insert(++i, 3, 1);
          std::advance(i, 3);
        }
        else
        {
          ++i;
        }
      }
      break;
    }
  }

  print(vector);
}
