#include <vector>
#include "functions.hpp"

void task4(const char *direction, const int size)
{
  bool isAsc = isAscending(direction);

  if (size <= 0)
  {
    throw std::invalid_argument("Incorrect size");
  }

  std::vector<double> vector(size);
  fillRandom(vector.data(), size);
  print(vector);

  sort<AccessByBreckets>(vector, isAsc);
  print(vector);
}
