#ifndef B2_QUEUE_WITH_PRIORITY_HPP
#define B2_QUEUE_WITH_PRIORITY_HPP

#include <stdexcept>
#include <list>

template <typename T>
class QueueWithPriority
{
public:

  enum class priority_t
  {
    LOW,
    NORMAL,
    HIGH
  };


  void add(priority_t priority, const T &element);
  T get();
  void accelerate();


private:
  struct queueElement_t
  {
    T element;
    priority_t priority;
  };

  std::list<queueElement_t> queue_;
};


template <typename T>
void QueueWithPriority<T>::add(priority_t priority, const T &element)
{
  queueElement_t queueElement;
  queueElement.priority = priority;
  queueElement.element = element;

  if (priority == QueueWithPriority<T>::priority_t ::LOW)
  {
    queue_.push_back(queueElement);
  }
  else
  {
    decltype(queue_.begin()) i = queue_.begin();
    while (i != queue_.end())
    {
      if ((*i).priority < priority) break;
      i++;
    }
    queue_.insert(i, queueElement);
  }
}

template <typename T>
T QueueWithPriority<T>::get()
{
  if (queue_.empty())
  {
    throw std::invalid_argument("Cant get element from the empty queue ");
  }

  T tmp = queue_.front().element;
  queue_.pop_front();
  return tmp;
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  auto endOfHighBlock = queue_.begin();
  auto beginOfLowBlock = --queue_.end();

  while ((beginOfLowBlock != queue_.begin()) && ((*beginOfLowBlock).priority == priority_t::LOW))
  {
    beginOfLowBlock--;
  }
  beginOfLowBlock++;

  while ((endOfHighBlock != queue_.end()) && ((*endOfHighBlock).priority == priority_t::HIGH))
  {
    endOfHighBlock++;
  }

  for (auto i = beginOfLowBlock; i != queue_.end(); i++)
  {
    (*i).priority = priority_t::HIGH;
  }

  queue_.splice(endOfHighBlock, queue_, beginOfLowBlock, queue_.end());
}

#endif
