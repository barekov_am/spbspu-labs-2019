#include <iostream>
#include <cstring>
#include <sstream>

#include "queue-with-priority.hpp"


void task1()
{
  QueueWithPriority<std::string> queue;
  std::string line;

  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed during reading");
    }

    std::string command;
    std::stringstream input(line);

    input >> command;

    if (command == "add")
    {
      std::string priority;
      std::string data;
      input >> priority >> data;

      if (!input.eof())
      {
        std::getline(input, line);
        data += line;
      }

      if ((!input.eof()) || (data.empty()))
      {
        std::cout << "<INVALID COMMAND>\n";
        continue;
      }

      if (priority == "low")
      {
        queue.add(QueueWithPriority<std::string>::priority_t::LOW, data);
      }
      else if (priority == "normal")
      {
        queue.add(QueueWithPriority<std::string>::priority_t::NORMAL, data);
      }
      else if (priority == "high")
      {
        queue.add(QueueWithPriority<std::string>::priority_t::HIGH, data);
      }
      else
      {
        std::cout << "<INVALID COMMAND>\n";
      }
    }

    else if (command == "get")
    {
      try
      {
        std::cout << queue.get() << '\n';
      }
      catch (std::invalid_argument)
      {
        std::cout << "<EMPTY>\n";
      }
    }
    else if (command == "accelerate")
    {
      queue.accelerate();
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
