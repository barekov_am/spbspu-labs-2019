#ifndef B2_QUEUE_HPP
#define B2_QUEUE_HPP

#include <list>
#include <stdexcept>

template <typename T>
class QueueWithPriority
{
public:
  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };
  void add(ElementPriority priority, const T& element);
  T get();
  void accelerate();

private:
  struct queueElement
  {
    T element;
    ElementPriority priority;
  };
  std::list<queueElement> queue_;
};

template <typename T>
void QueueWithPriority<T>::add(ElementPriority priority, const T& element)
{
  queueElement newElement;
  newElement.element = element;
  newElement.priority = priority;

  if (priority == QueueWithPriority<T>::ElementPriority::LOW)
  {
    queue_.push_back(newElement);
  }
  else
  {
    decltype(queue_.begin()) i = queue_.begin();
    while (i != queue_.end())
    {
      if (i->priority < priority) 
        break;
      i++;
    }
    queue_.insert(i, newElement);
  }
}

template <typename T>
T QueueWithPriority<T>::get()
{
  if (queue_.empty())
  {
    throw std::invalid_argument("Queue is empty");
  }

  T tmp = queue_.front().element;
  queue_.pop_front();
  return tmp;
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  auto endOfHIGH = queue_.begin();
  auto beginOfLOW = --queue_.end();

  while ((beginOfLOW != queue_.begin()) && (beginOfLOW->priority == ElementPriority::LOW))
  {
    beginOfLOW--;
  }
  beginOfLOW++;

  while ((endOfHIGH != queue_.end()) && (endOfHIGH->priority == ElementPriority::HIGH))
  {
    endOfHIGH++;
  }

  for (auto i = beginOfLOW; i != queue_.end(); i++)
  {
    i->priority = ElementPriority::HIGH;
  }

  queue_.splice(endOfHIGH, queue_, beginOfLOW, queue_.end());
}

#endif
