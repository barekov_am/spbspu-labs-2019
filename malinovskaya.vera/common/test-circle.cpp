#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteCircle)

  const double TOLERANCE = 0.01;

  BOOST_AUTO_TEST_CASE(CircleConstAfterMoving)
  {
    malinovskaya::Circle circle_({1.0, -1.0}, 2.0);
    const malinovskaya::rectangle_t firstFrame = circle_.getFrameRect();
    const double first = circle_.getArea();

    circle_.move({4.0, 5.0});
    malinovskaya::rectangle_t secondFrame = circle_.getFrameRect();
    double secondArea = circle_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, TOLERANCE);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, TOLERANCE);
    BOOST_CHECK_CLOSE(first, secondArea, TOLERANCE);

    circle_.move(-2.0, 3.0);
    secondFrame = circle_.getFrameRect();
    secondArea = circle_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, TOLERANCE);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, TOLERANCE);
    BOOST_CHECK_CLOSE(first, secondArea, TOLERANCE);
  }

  BOOST_AUTO_TEST_CASE(CircleScaling)
  {
    malinovskaya::Circle circle_({1.0, -1.0}, 2.0);
    double firstArea = circle_.getArea();

    double coeff = 2.2;
    circle_.scale(coeff);
    double secondArea = circle_.getArea();

    BOOST_CHECK_CLOSE(firstArea * coeff * coeff, secondArea, TOLERANCE);

    firstArea = secondArea;
    coeff = 0.3;
    circle_.scale(coeff);
    secondArea = circle_.getArea();

    BOOST_CHECK_CLOSE(firstArea *  coeff * coeff, secondArea, TOLERANCE);
  }

  BOOST_AUTO_TEST_CASE(CircleThrowingExceptions)
  {
    BOOST_CHECK_THROW(malinovskaya::Circle({3.0, 4.0}, -2.0), std::invalid_argument);

    malinovskaya::Circle circle_({6.0, 5.0}, 7.5);
    BOOST_CHECK_THROW(circle_.scale(0.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(testCircleRotation)
  {
    malinovskaya::Circle testCircle({5.0, 5.0}, 2.0);
    const double areaBefore = testCircle.getArea();
    const malinovskaya::rectangle_t frameRectBefore = testCircle.getFrameRect();

    double angle = 90;
    testCircle.rotate(angle);
    double areaAfter = testCircle.getArea();
    malinovskaya::rectangle_t frameRectAfter = testCircle.getFrameRect();

    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, TOLERANCE);
    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, TOLERANCE);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, TOLERANCE);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, TOLERANCE);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, TOLERANCE);

    angle = -150;
    testCircle.rotate(angle);
    areaAfter = testCircle.getArea();
    frameRectAfter = testCircle.getFrameRect();

    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, TOLERANCE);
    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, TOLERANCE);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, TOLERANCE);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, TOLERANCE);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, TOLERANCE);
  }

BOOST_AUTO_TEST_SUITE_END()
