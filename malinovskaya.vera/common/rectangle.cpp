#include "rectangle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>

malinovskaya::Rectangle::Rectangle(const rectangle_t &rectangle) :
  rectangle_(rectangle),
  rotationDegree_(0)
{
  if ((rectangle_.width <= 0.0) || (rectangle_.height <= 0.0))
  {
    throw std::invalid_argument("Width and height should be positive");
  }
}

double malinovskaya::Rectangle::getArea() const
{
  return rectangle_.width * rectangle_.height;
}

malinovskaya::rectangle_t malinovskaya::Rectangle::getFrameRect() const
{
  const double cos = std::cos((2 * M_PI * rotationDegree_) / 360);
  const double sin = std::sin((2 * M_PI * rotationDegree_) / 360);
  const double width = rectangle_.width * std::fabs(cos) + rectangle_.height * std::fabs(sin);
  const double height = rectangle_.height * std::fabs(cos) + rectangle_.width * std::fabs(sin);
  return {width, height, rectangle_.pos};
}

void malinovskaya::Rectangle::move(double dx, double dy)
{
  rectangle_.pos.x += dx;
  rectangle_.pos.y += dy;
}

void malinovskaya::Rectangle::move(const point_t& point)
{
  rectangle_.pos = point;
}

void malinovskaya::Rectangle::print() const
{
  std::cout << "Center: (" << getFrameRect().pos.x << "; " << getFrameRect().pos.y << ")" << std::endl;
}

void malinovskaya::Rectangle::scale(double coeff)
{
  if (coeff <= 0.0)
  {
    throw std::invalid_argument("Scale coefficient should be positive");
  }
  rectangle_.width *= coeff;
  rectangle_.height *= coeff;
}

void malinovskaya::Rectangle::rotate(double angle)
{
  rotationDegree_ += angle;
  while (std::abs(rotationDegree_) >= 360)
  {
    rotationDegree_ = (angle > 0) ? rotationDegree_ - 360 : rotationDegree_ + 360;
  }
}
