#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <memory>
#include "base-types.hpp"

namespace malinovskaya
{
  class Shape
  {
  public:
    using pointer = std::shared_ptr<Shape>;
    using array = std::unique_ptr<pointer[]>;
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void move(const point_t&) = 0;
    virtual void print() const = 0;
    virtual void scale(double coeff) = 0;
    virtual void rotate(double angle) = 0;
  };
}

#endif
