#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteTriangle)

  const double TOLERANCE = 0.01;

  BOOST_AUTO_TEST_CASE(TriangleConstAfterMoving)
  {
    malinovskaya::Triangle triangle_({1, -1}, {2, 2}, {3, -2});
    const malinovskaya::rectangle_t firstFrame = triangle_.getFrameRect();
    const double firstArea = triangle_.getArea();

    triangle_.move({4, 5});
    malinovskaya::rectangle_t secondFrame = triangle_.getFrameRect();
    double secondArea = triangle_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, TOLERANCE);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, TOLERANCE);
    BOOST_CHECK_CLOSE(firstArea, secondArea, TOLERANCE);

    triangle_.move(-2, 3);
    secondFrame = triangle_.getFrameRect();
    secondArea = triangle_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, TOLERANCE);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, TOLERANCE);
    BOOST_CHECK_CLOSE(firstArea, secondArea, TOLERANCE);
  }

  BOOST_AUTO_TEST_CASE(TriangleScaling)
  {
    malinovskaya::Triangle triangle_({1, -1}, {2, 2}, {3, -2});
    const double firstArea = triangle_.getArea();

    const double coeff = 4;
    triangle_.scale(coeff);
    double secondArea = triangle_.getArea();

    BOOST_CHECK_CLOSE(firstArea *  coeff * coeff, secondArea, TOLERANCE);
  }

  BOOST_AUTO_TEST_CASE(TriangleThrowingExceptions)
  {
    BOOST_CHECK_THROW(malinovskaya::Triangle({0, 0}, {-1, 0}, {1, 0}), std::invalid_argument);

    malinovskaya::Triangle triangle_({3, 5}, {2, 2}, {-1, -3});
    BOOST_CHECK_THROW(triangle_.scale(0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
