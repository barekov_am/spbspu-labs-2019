#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP
#include "shape.hpp"

namespace malinovskaya
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape& compositeShape);
    CompositeShape(CompositeShape&& compositeShape);
    CompositeShape(const Shape::pointer& compositeShape);
    ~CompositeShape() override = default;
    CompositeShape& operator =(const CompositeShape& compositeShape);
    CompositeShape& operator =(CompositeShape&& compositeShape);
    Shape::pointer operator [](size_t index) const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t& point) override;
    void print() const override;
    void scale(double coeff) override;
    void rotate(double angle) override;
    size_t getSize() const;
    void add(const Shape::pointer& shape);
    void remove(size_t index);
  private:
    size_t size_;
    Shape::array shapes_;
  };
}

#endif

