#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

namespace malinovskaya
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t& vertex1, const point_t& vertex2, const point_t& vertex3);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t& point) override;
    void print() const override;
    void scale(double coeff) override;
    void rotate(double angle) override;
  private:
    point_t vertex_[3];
    point_t getCenter() const;
  };
}

#endif
