#include "composite-shape.hpp"
#include <iostream>
#include <cmath>
#include <math.h>

malinovskaya::CompositeShape::CompositeShape():
  size_(0),
  shapes_(nullptr)
{ }

malinovskaya::CompositeShape::CompositeShape(const CompositeShape &compositeShape) :
  size_(compositeShape.size_),
  shapes_(std::make_unique<malinovskaya::Shape::pointer[]>(compositeShape.size_))
{
  for (std::size_t i = 0; i < size_; i++)
  {
    shapes_[i] = compositeShape.shapes_[i];
  }
}

malinovskaya::CompositeShape::CompositeShape(CompositeShape &&compositeShape) :
  size_(compositeShape.size_),
  shapes_(std::move(compositeShape.shapes_))
{
  compositeShape.size_ = 0;
  compositeShape.shapes_ = nullptr;
}

malinovskaya::CompositeShape::CompositeShape(const Shape::pointer &compositeShape) :
  CompositeShape()
{
  add(compositeShape);
}

malinovskaya::CompositeShape& malinovskaya::CompositeShape::operator =(const CompositeShape &compositeShape)
{
  if (this != &compositeShape)
  {
    shapes_ = std::make_unique<malinovskaya::Shape::pointer[]>(compositeShape.size_);
    size_ = compositeShape.size_;
    for (std::size_t i = 0; i < size_; i++)
    {
      shapes_[i] = compositeShape.shapes_[i];
    }
  }
  return *this;
}

malinovskaya::CompositeShape& malinovskaya::CompositeShape::operator =(CompositeShape&& compositeShape)
{
  if (this != &compositeShape)
  {
    size_ = compositeShape.size_;
    shapes_ = std::move(compositeShape.shapes_);
    compositeShape.size_ = 0;
    compositeShape.shapes_ = nullptr;
  }
  return *this;
}

malinovskaya::Shape::pointer malinovskaya::CompositeShape::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Index out of range");
  }
  return shapes_[index];
}

double malinovskaya::CompositeShape::getArea() const
{
  double area = 0.0;
  for (size_t i = 0; i < size_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

malinovskaya::rectangle_t malinovskaya::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Empty composite shape");
  }
  rectangle_t tmpFrameRect = shapes_[0]->getFrameRect();
  double minX = tmpFrameRect.pos.x - tmpFrameRect.width / 2;
  double maxX = tmpFrameRect.pos.x + tmpFrameRect.width / 2;
  double minY = tmpFrameRect.pos.y - tmpFrameRect.height / 2;
  double maxY = tmpFrameRect.pos.y + tmpFrameRect.height / 2;
  for (size_t i = 1; i < size_; i++)
  {
    tmpFrameRect = shapes_[i]->getFrameRect();
    minX = std::min(minX, tmpFrameRect.pos.x - tmpFrameRect.width / 2);
    maxX = std::max(maxX, tmpFrameRect.pos.x + tmpFrameRect.width / 2);
    minY = std::min(minY, tmpFrameRect.pos.y - tmpFrameRect.height / 2);
    maxY = std::max(maxY, tmpFrameRect.pos.y + tmpFrameRect.height / 2);
  }
  return {maxX - minX, maxY - minY, {(maxX + minX) / 2, (maxY + minY) / 2}};
}

void malinovskaya::CompositeShape::move(double dx, double dy)
{
  if (size_ == 0)
  {
    throw std::logic_error("Empty composite shape");
  }
  for (std::size_t i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void malinovskaya::CompositeShape::move(const point_t &point)
{
  if (size_ == 0)
  {
    throw std::logic_error("Empty composite shape");
  }
  point_t tmpFramePos = getFrameRect().pos;
  move(point.x - tmpFramePos.x, point.y - tmpFramePos.y);
}

void malinovskaya::CompositeShape::print() const
{
  std::cout << "Center: (" << getFrameRect().pos.x << "; " << getFrameRect().pos.y << ")" << std::endl;
  std::cout << "Frame (width): " << getFrameRect().width << std::endl;
  std::cout << "Frame (height): " << getFrameRect().height << std::endl;
  std::cout << "Area: " << getArea() << std::endl;
}

void malinovskaya::CompositeShape::scale(double coeff)
{
  if (coeff <= 0.0)
  {
    throw std::invalid_argument("Scale coefficient should be positive");
  }
  point_t tmpFramePos = getFrameRect().pos;
  for (std::size_t i = 0; i < size_; i++)
  {
    point_t tmpCenter = shapes_[i]->getFrameRect().pos;
    shapes_[i]->move((tmpCenter.x - tmpFramePos.x) * (coeff - 1), (tmpCenter.y - tmpFramePos.y) * (coeff - 1));
    shapes_[i]->scale(coeff);
  }
}

void malinovskaya::CompositeShape::rotate(double angle)
{
  const double cos = std::cos((2 * M_PI * angle) / 360);
  const double sin = std::sin((2 * M_PI * angle) / 360);
  const point_t compCenter = getFrameRect().pos;
  for (std::size_t i = 0; i < size_; i++)
  {
    const point_t shapeCenter = shapes_[i]->getFrameRect().pos;
    const double dx = (shapeCenter.x - compCenter.x) * cos - (shapeCenter.y - compCenter.y) * sin;
    const double dy = (shapeCenter.x - compCenter.x) * sin + (shapeCenter.y - compCenter.y) * cos;
    shapes_[i]->move({compCenter.x + dx, compCenter.y + dy});
    shapes_[i]->rotate(angle);
  }
}

std::size_t malinovskaya::CompositeShape::getSize() const
{
  return size_;
}

void malinovskaya::CompositeShape::add(const Shape::pointer& shape)
{
  Shape::array shapes = std::make_unique<Shape::pointer[]>(size_ + 1);
  for (size_t i = 0; i < size_; i++)
  {
    shapes[i] = shapes_[i];
  }
  shapes[size_] = shape;
  size_++;
  shapes_ = std::move(shapes);
}

void malinovskaya::CompositeShape::remove(size_t index)
{
  if (index >= size_)
  {
    throw std::logic_error("Index out of range");
  }
  for (size_t i = index; i < (size_ - 1); i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  shapes_[--size_] = nullptr;
}
