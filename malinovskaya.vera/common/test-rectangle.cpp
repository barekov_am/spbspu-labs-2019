#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double TOLERANCE = 0.01;

BOOST_AUTO_TEST_SUITE(testRectangle)

BOOST_AUTO_TEST_CASE(testRectangleMove)
{
  malinovskaya::rectangle_t rectangle1 = {10.0, 7.0, {4.0, 2.0}};
  malinovskaya::Rectangle rectangleTest(rectangle1);
  double areaBefore = rectangleTest.getArea();
  malinovskaya::rectangle_t frameBefore = rectangleTest.getFrameRect();
  rectangleTest.move(7.0, -2.0);
  BOOST_CHECK_CLOSE(areaBefore, rectangleTest.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.width, rectangleTest.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.height, rectangleTest.getFrameRect().height, TOLERANCE);
  rectangleTest.move({-3.0, -10.0});
  BOOST_CHECK_CLOSE(areaBefore, rectangleTest.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.width, rectangleTest.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.height, rectangleTest.getFrameRect().height, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(testRectangleScale)
{
  malinovskaya::rectangle_t rectangle1 = {10.0, 7.0, {4.0, 2.0}};
  malinovskaya::Rectangle rectangleTest(rectangle1);
  double areaBefore = rectangleTest.getArea();
  malinovskaya::rectangle_t frameBefore = rectangleTest.getFrameRect();
  double scaleCoeff = 8.0;
  rectangleTest.scale(scaleCoeff);
  BOOST_CHECK_CLOSE(areaBefore * scaleCoeff * scaleCoeff, rectangleTest.getArea(), TOLERANCE);
  areaBefore = rectangleTest.getArea();
  frameBefore = rectangleTest.getFrameRect();
  scaleCoeff = 0.8;
  rectangleTest.scale(scaleCoeff);
  BOOST_CHECK_CLOSE(areaBefore * scaleCoeff * scaleCoeff, rectangleTest.getArea(), TOLERANCE);
}

BOOST_AUTO_TEST_CASE(testRectangleIncorrectParameters)
{
  malinovskaya::rectangle_t rec = {-10.0, 4.0, {1.0, -5.0}};
  BOOST_CHECK_THROW(malinovskaya::Rectangle rectangleTest(rec), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testRectangleIncorrectScale)
{
  malinovskaya::rectangle_t rec = {10.0, 4.0, {1.0, -5.0}};
  malinovskaya::Rectangle rectangleTest(rec);
  BOOST_CHECK_THROW(rectangleTest.scale(-3.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
