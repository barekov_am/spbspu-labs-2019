#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteCompositeShape)

  const double TOLERANCE = 0.01;

  BOOST_AUTO_TEST_CASE(CompositeShapeCopyConstructor)
  {
    malinovskaya::rectangle_t rectangle1 = {2.0, 4.0, {-1.0, 3.0}};
    malinovskaya::Shape::pointer rectangle_ = std::make_shared<malinovskaya::Rectangle>(rectangle1);
    malinovskaya::point_t point1 = {1.0, 1.0};
    malinovskaya::Shape::pointer circle_ = std::make_shared<malinovskaya::Circle>(point1, 1.0);
    malinovskaya::CompositeShape composite1_(rectangle_);
    composite1_.add(circle_);

    const malinovskaya::rectangle_t frame1_ = composite1_.getFrameRect();
    const double area1_ = composite1_.getArea();

    malinovskaya::CompositeShape composite2_(composite1_);

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, TOLERANCE);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, TOLERANCE);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, TOLERANCE);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, TOLERANCE);
    BOOST_CHECK_CLOSE(area1_, composite2_.getArea(), TOLERANCE);
    BOOST_CHECK_EQUAL(composite1_.getSize(), composite2_.getSize());
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeMoveConstructor)
  {
    malinovskaya::rectangle_t rectangle1 = {2.0, 3.0, {2.0, 2.0}};
    malinovskaya::Shape::pointer rectangle_ = std::make_shared<malinovskaya::Rectangle>(rectangle1);
    malinovskaya::point_t point1 = {1.0, 1.0};
    malinovskaya::Shape::pointer circle_ = std::make_shared<malinovskaya::Circle>(point1, 1.0);
    malinovskaya::CompositeShape composite1_(rectangle_);
    composite1_.add(circle_);

    const malinovskaya::rectangle_t frame1_ = composite1_.getFrameRect();
    const double area_ = composite1_.getArea();
    const size_t size_ = composite1_.getSize();

    malinovskaya::CompositeShape composite2_(std::move(composite1_));

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, TOLERANCE);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, TOLERANCE);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, TOLERANCE);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, TOLERANCE);
    BOOST_CHECK_CLOSE(area_, composite2_.getArea(), TOLERANCE);
    BOOST_CHECK_EQUAL(size_, composite2_.getSize());
    BOOST_CHECK_CLOSE(composite1_.getArea(), 0, TOLERANCE);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeCopyOperator)
  {
    malinovskaya::rectangle_t rectangle1 = {2.0, 3.0, {2.0, 2.0}};
    malinovskaya::Shape::pointer rectangle_ = std::make_shared<malinovskaya::Rectangle>(rectangle1);
    malinovskaya::point_t point1 = {1.0, 1.0};
    malinovskaya::Shape::pointer circle_ = std::make_shared<malinovskaya::Circle>(point1, 1.0);
    malinovskaya::CompositeShape composite_(rectangle_);
    composite_.add(circle_);

    const malinovskaya::rectangle_t frame1_ = composite_.getFrameRect();
    const double area_ = composite_.getArea();

    malinovskaya::CompositeShape composite2_;
    composite2_ = composite_;

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, TOLERANCE);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, TOLERANCE);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, TOLERANCE);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, TOLERANCE);
    BOOST_CHECK_CLOSE(area_, composite2_.getArea(), TOLERANCE);
    BOOST_CHECK_EQUAL(composite_.getSize(), composite2_.getSize());
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeMoveOperator)
  {
    malinovskaya::rectangle_t rectangle1 = {2.0, 3.0, {2.0, 2.0}};
    malinovskaya::Shape::pointer rectangle_ = std::make_shared<malinovskaya::Rectangle>(rectangle1);
    malinovskaya::point_t point1 = {1.0, 1.0};
    malinovskaya::Shape::pointer circle_ = std::make_shared<malinovskaya::Circle>(point1, 1.0);
    malinovskaya::CompositeShape composite1_(rectangle_);
    composite1_.add(circle_);

    const malinovskaya::rectangle_t frame1_ = composite1_.getFrameRect();
    const double area_ = composite1_.getArea();
    const size_t size_ = composite1_.getSize();

    malinovskaya::CompositeShape composite2_;
    composite2_ = std::move(composite1_);

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, TOLERANCE);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, TOLERANCE);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, TOLERANCE);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, TOLERANCE);
    BOOST_CHECK_CLOSE(area_, composite2_.getArea(), TOLERANCE);
    BOOST_CHECK_EQUAL(size_, composite2_.getSize());
    BOOST_CHECK_CLOSE(composite1_.getArea(), 0, TOLERANCE);
    BOOST_CHECK_EQUAL(composite1_.getSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeConstAfterMoving)
  {
    malinovskaya::CompositeShape composite_;
    malinovskaya::point_t point1 = {1, -1};
    malinovskaya::Shape::pointer circle_ = std::make_shared<malinovskaya::Circle>(point1, 2.0);
    malinovskaya::rectangle_t rectangle1 = {2.0, 3.0, {1.0, -1.0}};
    malinovskaya::Shape::pointer rectangle_ = std::make_shared<malinovskaya::Rectangle>(rectangle1);
    malinovskaya::Shape::pointer triangle_ = std::make_shared<malinovskaya::Triangle>(malinovskaya::Triangle {{1.0, -1.0}, {2.0, 2.0}, {3.0, -2.0}});
    composite_.add(circle_);
    composite_.add(rectangle_);
    composite_.add(triangle_);

    const malinovskaya::rectangle_t firstFrame = composite_.getFrameRect();
    const double firstArea = composite_.getArea();

    composite_.move({4.0, 5.0});
    malinovskaya::rectangle_t secondFrame = composite_.getFrameRect();
    double secondArea = composite_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, TOLERANCE);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, TOLERANCE);
    BOOST_CHECK_CLOSE(firstArea, secondArea, TOLERANCE);

    composite_.move(-2.0, 3.0);
    secondFrame = composite_.getFrameRect();
    secondArea = composite_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, TOLERANCE);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, TOLERANCE);
    BOOST_CHECK_CLOSE(firstArea, secondArea, TOLERANCE);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeScaling)
  {
    malinovskaya::CompositeShape composite_;
    malinovskaya::point_t point1 = {1.0, -1.0};
    malinovskaya::Shape::pointer circle_ = std::make_shared<malinovskaya::Circle>(point1, 2.0);
    malinovskaya::rectangle_t rectangle1 = {2.0, 3.0, {1.0, -1.0}};
    malinovskaya::Shape::pointer rectangle_ = std::make_shared<malinovskaya::Rectangle>(rectangle1);
    malinovskaya::Shape::pointer triangle_ = std::make_shared<malinovskaya::Triangle>(malinovskaya::Triangle {{1.0, -1.0}, {2.0, 2.0}, {3.0, -2.0}});
    composite_.add(circle_);
    composite_.add(rectangle_);
    composite_.add(triangle_);

    const double firstArea = composite_.getArea();
    const double scaleCoeff = 6.0;
    composite_.scale(scaleCoeff);
    double secondArea = composite_.getArea();

    BOOST_CHECK_CLOSE(firstArea *  scaleCoeff * scaleCoeff, secondArea, TOLERANCE);
  }

  BOOST_AUTO_TEST_CASE(operatorTests)
  {
    malinovskaya::rectangle_t rectangle1 = {2.0, 2.0, {1.0, 1.0}};
    malinovskaya::Shape::pointer rectangle1_ = std::make_shared<malinovskaya::Rectangle>(rectangle1);
    malinovskaya::CompositeShape compShape1_(rectangle1_);

    const malinovskaya::rectangle_t testFrameRect = rectangle1_->getFrameRect();
    const double area = rectangle1_->getArea();

    malinovskaya::point_t point1 = {2.0, 2.0};
    malinovskaya::Shape::pointer circle1_ = std::make_shared<malinovskaya::Circle>(point1, 2.0);
    malinovskaya::CompositeShape compShape2_(circle1_);

    BOOST_CHECK_NO_THROW(malinovskaya::CompositeShape compShape2_(compShape1_));
    BOOST_CHECK_NO_THROW(compShape2_= compShape1_);
    BOOST_CHECK_NO_THROW(compShape2_= std::move(compShape1_));
    BOOST_CHECK_NO_THROW(malinovskaya::CompositeShape compShape2_(std::move(compShape1_)));

    compShape1_.add(rectangle1_);
    BOOST_CHECK_CLOSE(testFrameRect.width, compShape1_.getFrameRect().width, TOLERANCE);
    BOOST_CHECK_CLOSE(testFrameRect.height, compShape1_.getFrameRect().height, TOLERANCE);
    BOOST_CHECK_CLOSE(area, compShape1_.getArea(), TOLERANCE);
  }

BOOST_AUTO_TEST_SUITE_END()
