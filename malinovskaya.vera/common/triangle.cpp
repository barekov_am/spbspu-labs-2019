#include "triangle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>

malinovskaya::Triangle::Triangle(const point_t& vertexA, const point_t& vertexB, const point_t& vertexC):
    vertex_{vertexA, vertexB, vertexC}
{
  double precision = 0.001;

  if (abs((vertexC.x - vertexA.x) * (vertexB.y - vertexA.y) - (vertexC.y - vertexA.y) * (vertexB.x - vertexA.x))
      < precision)
  {
    throw std::invalid_argument("vertexes of triangle can't lie on the same straight line or match");
  }
}

malinovskaya::point_t malinovskaya::Triangle::getCenter() const
{
  return {(vertex_[0].x + vertex_[1].x + vertex_[2].x) / 3, ((vertex_[0].y + vertex_[1].y + vertex_[2].y) / 3)};
}

double malinovskaya::Triangle::getArea() const
{
  return fabs((vertex_[0].x - vertex_[2].x) * (vertex_[1].y - vertex_[2].y)
      - (vertex_[1].x - vertex_[2].x) * (vertex_[0].y - vertex_[2].y)) / 2;
}

malinovskaya::rectangle_t malinovskaya::Triangle::getFrameRect() const
{
  double maxX = std::max(std::max(vertex_[0].x, vertex_[1].x), vertex_[2].x);
  double maxY = std::max(std::max(vertex_[0].y, vertex_[1].y), vertex_[2].y);
  double minX = std::min(std::min(vertex_[0].x, vertex_[1].x), vertex_[2].x);
  double minY = std::min(std::min(vertex_[0].y, vertex_[1].y), vertex_[2].y);

  return {maxX - minX, maxY - minY, {minX + (maxX - minX) / 2, minY + (maxY - minY) / 2}};
}

void malinovskaya::Triangle::move(double dx, double dy)
{
  for (int i = 0; i <= 2; i++)
  {
    vertex_[i].x += dx;
    vertex_[i].y += dy;
  }
}

void malinovskaya::Triangle::move(const point_t& point)
{
  malinovskaya::point_t oldCenter = getCenter();
  double dx = point.x - oldCenter.x;
  double dy = point.y - oldCenter.y;
  move(dx, dy);
}

void malinovskaya::Triangle::print() const
{
  std::cout << "Center: (" << getCenter().x << "; " << getCenter().y << ")" << std::endl;
}

void malinovskaya::Triangle::scale(double coeff)
{
  if (coeff <= 0)
  {
    throw std::invalid_argument("Scale coefficient should be positive");
  }
  point_t center = getCenter();
  for (int i = 0; i <= 2; i++)
  {
    vertex_[i].x = center.x + coeff * (vertex_[i].x - center.x);
    vertex_[i].y = center.y + coeff * (vertex_[i].y - center.y);
  }
}

void malinovskaya::Triangle::rotate(double angle)
{
  const double cos = std::cos((2 * M_PI * angle) / 360);
  const double sin = std::sin((2 * M_PI * angle) / 360);
  const point_t center = getCenter();
  for (std::size_t i = 0; i < 2; i++)
  {
    vertex_[i].x = center.x + (vertex_[i].x - center.x) * cos - (vertex_[i].y - center.y) * sin;
    vertex_[i].y = center.y + (vertex_[i].x - center.x) * sin + (vertex_[i].y - center.y) * cos;
  }
}
