#ifndef A3_PARTITION_HPP
#define A3_PARTITION_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace malinovskaya
{
  malinovskaya::Matrix part(const malinovskaya::CompositeShape& composition);
}

#endif
