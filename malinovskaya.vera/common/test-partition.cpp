#include <boost/test/auto_unit_test.hpp>

#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

const double TOLERANCE = 0.01;

BOOST_AUTO_TEST_SUITE(testPartition)

  BOOST_AUTO_TEST_CASE(intersectCorrectness)
  {
    malinovskaya::Circle testCircle({ 2, 0 }, 3);
    malinovskaya::rectangle_t rectangle1 = {9, 5, {2, 4}};
    malinovskaya::Rectangle testRectangle(rectangle1);
    rectangle1 = {1, 1, {12, 21}};
    malinovskaya::Rectangle testSquare(rectangle1);

    BOOST_CHECK(malinovskaya::isIntersected(testCircle.getFrameRect(),testRectangle.getFrameRect()));
    BOOST_CHECK(!(malinovskaya::isIntersected(testSquare.getFrameRect(),testRectangle.getFrameRect())));
    BOOST_CHECK(!(malinovskaya::isIntersected(testCircle.getFrameRect(),testSquare.getFrameRect())));
  }

  BOOST_AUTO_TEST_CASE(partitionCorrectness)
  {
    malinovskaya::Shape::pointer testCircle = std::make_shared<malinovskaya::Circle>(malinovskaya::point_t{ 1, 1 }, 2);
    malinovskaya::rectangle_t rectangle1 = {5, 4, {3, -1}};
    malinovskaya::Shape::pointer testRectangle1 = std::make_shared<malinovskaya::Rectangle>(rectangle1);
    rectangle1 = {4, 4, {6, 15}};
    malinovskaya::Shape::pointer testSquare1 = std::make_shared<malinovskaya::Rectangle>(rectangle1);
    rectangle1 = {2, 3, {10, 2}};
    malinovskaya::Shape::pointer testRectangle2 = std::make_shared<malinovskaya::Rectangle>(rectangle1);
    rectangle1 = {6, 30, {8, 15}};
    malinovskaya::Shape::pointer testRectangle3 = std::make_shared<malinovskaya::Rectangle>(rectangle1);
    rectangle1 = {5, 5, {-20, 15}};
    malinovskaya::Shape::pointer testSquare2 = std::make_shared<malinovskaya::Rectangle>(rectangle1);

    malinovskaya::CompositeShape testComposition;
    testComposition.add(testCircle);
    testComposition.add(testRectangle1);
    testComposition.add(testSquare1);
    testComposition.add(testRectangle2);
    testComposition.add(testRectangle3);
    testComposition.add(testSquare2);

    malinovskaya::Matrix testMatrix = malinovskaya::part(testComposition);

    const std::size_t correctRows = 3;
    const std::size_t correctColumns = 4;

    BOOST_CHECK_EQUAL(testMatrix.getRows(), correctRows);
    BOOST_CHECK_EQUAL(testMatrix.getColumns(), correctColumns);
    BOOST_CHECK(testMatrix[0][0] == testCircle);
    BOOST_CHECK(testMatrix[0][1] == testSquare1);
    BOOST_CHECK(testMatrix[0][2] == testRectangle2);
    BOOST_CHECK(testMatrix[0][3] == testSquare2);
    BOOST_CHECK(testMatrix[1][0] == testRectangle1);
    BOOST_CHECK(testMatrix[2][0] == testRectangle3);
  }

BOOST_AUTO_TEST_SUITE_END()
