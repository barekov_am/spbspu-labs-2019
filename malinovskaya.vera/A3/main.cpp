#include <iostream>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

void print(const malinovskaya::Shape &shape)
{
  std::cout << "Area: " << shape.getArea() << std::endl;
  std::cout << "Frame (width): " << shape.getFrameRect().width << std::endl;
  std::cout << "Frame (height): " << shape.getFrameRect().height << std::endl;
}

int main()
{
  malinovskaya::Circle objectCircle({4.0, 2.0}, 3.0);
  std::cout << "Circle:" << std::endl;
  std::cout << std::endl;
  objectCircle.print();
  print(objectCircle);
  std::cout << std::endl;
  std::cout << "Shift: dx = " << -2 << ", dy = " << 4 << std::endl;
  objectCircle.move(-2.0, 4.0);
  objectCircle.print();
  print(objectCircle);
  std::cout << std::endl;
  std::cout << "Move to (" << 0 << ", " << -6 << ")" << std::endl;
  objectCircle.move({0.0, -6.0});
  print(objectCircle);
  std::cout << std::endl;
  std::cout << "Scale with a coefficient of 3" << std::endl;
  objectCircle.scale(3.0);
  objectCircle.print();
  print(objectCircle);

  std::cout << std::endl;
  std::cout << std::endl;

  malinovskaya::Rectangle objectRectangle({3.0, 4.0, {5.0, 8.0}});
  std::cout << "Rectangle:" << std::endl;
  std::cout << std::endl;
  objectRectangle.print();
  print(objectRectangle);
  std::cout << std::endl;
  std::cout << "Shift: dx = " << 8 << ", dy = " << 2 << std::endl;
  objectRectangle.move(-8.0, 2.0);
  objectRectangle.print();
  print(objectRectangle);
  std::cout << std::endl;
  std::cout << "Move to (" << -5 << ", " << -10 << ")" << std::endl;
  objectRectangle.move({-5.0, -10.0});
  print(objectRectangle);
  std::cout << std::endl;
  std::cout << "Scale with a coefficient of 4.5" << std::endl;
  objectRectangle.scale(4.5);
  objectRectangle.print();
  print(objectRectangle);

  std::cout << std::endl;
  std::cout << std::endl;

  malinovskaya::Triangle objectTriangle({0.0, 0.0}, {-1.0, 5.0}, {3.0, 6.0});
  std::cout << "Triangle:" << std::endl;
  std::cout << std::endl;
  objectTriangle.print();
  print(objectTriangle);
  std::cout << std::endl;
  std::cout << "Shift: dx = " << 8 << ", dy = " << 2 << std::endl;
  objectTriangle.move(-8.0, 2.0);
  objectTriangle.print();
  print(objectTriangle);
  std::cout << std::endl;
  std::cout << "Move to (" << -5 << ", " << -10 << ")" << std::endl;
  objectTriangle.move({ -5.0, -10.0 });
  print(objectTriangle);
  std::cout << std::endl;
  std::cout << "Scale with a coefficient of 4.5" << std::endl;
  objectTriangle.scale(4.5);
  objectTriangle.print();
  print(objectTriangle);

  std::cout << std::endl;
  std::cout << std::endl;

  malinovskaya::Shape::pointer shape1 = std::make_shared<malinovskaya::Rectangle>(objectRectangle);
  malinovskaya::Shape::pointer shape3 = std::make_shared<malinovskaya::Circle>(objectCircle);

  malinovskaya::CompositeShape compShape;
  std::cout << std::endl;
  std::cout << "Composite Shape:" << std::endl;
  std::cout << std::endl;
  compShape.add(shape1);
  compShape.print();
  std::cout << std::endl;
  std::cout << "Add rectangle" << std::endl;
  compShape.add(shape3);
  compShape.print();
  std::cout << std::endl;
  std::cout << "Shift: dx = " << -2 << ", dy = " << 4 << std::endl;
  compShape.move(-2.0, 4.0);
  compShape.print();
  std::cout << std::endl;
  std::cout << "Move to (" << -5 << ", " << -10 << ")" << std::endl;
  compShape.move({-5.0, -10.0});
  compShape.print();
  std::cout << std::endl;
  std::cout << "Scale with a coefficient of 4.5" << std::endl;
  compShape.scale(4.5);
  compShape.print();
  std::cout << std::endl;
  std::cout << "Delete rectangle" << std::endl;
  compShape.remove(1);
  compShape.print();

  return 0;
}
