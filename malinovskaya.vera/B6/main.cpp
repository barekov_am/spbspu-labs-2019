#include <iostream>
#include <algorithm>
#include <iterator>
#include "functor.hpp"

int main()
{
  try
  {
    Functor functor;
    functor = std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), functor);

    if (std::cin.fail() && !std::cin.eof())
    {
      std::cerr << "Reading failed" << std::endl;
      return 1;
    }
    functor.print();
  }

  catch (std::exception& exception)
  {
    exception.what();
    return 1;
  }

  return 0;
}
