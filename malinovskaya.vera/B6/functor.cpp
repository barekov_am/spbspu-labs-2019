#include "functor.hpp"

Functor::Functor() :
  max_(0),
  min_(0),
  sum_(0.0),
  counter_(0),
  positive_(0),
  negative_(0),
  oddsum_(0),
  evensum_(0),
  first_(0),
  last_(0)
{}

void Functor::operator ()(const int number)
{
  if (counter_ == 0)
  {
    first_ = number;
    min_ = number;
    max_ = number;
  }

  last_ = number;
  ++counter_;

  sum_ += number;

  if (number < min_)
  {
    min_ = number;
  }
  else if (number > max_)
  {
    max_ = number;
  }
  if (number < 0)
  {
    ++negative_;
  }
  else if (number > 0)
  {
    ++positive_;
  }

  (number % 2 ? oddsum_ : evensum_) += number;
}

void Functor::print() const
{
  if (counter_ == 0)
  {
    std::cout << "No Data" << std::endl;
    return;
  }
  std::cout << "Max: " << max_ << std::endl
    << "Min: " << min_ << std::endl
    << "Mean: " << sum_ / counter_ << std::endl
    << "Positive: " << positive_ << std::endl
    << "Negative: " << negative_ << std::endl
    << "Odd Sum: " << oddsum_ << std::endl
    << "Even Sum: " << evensum_ << std::endl
    << "First/Last Equal: " << ((first_ == last_) ? "yes" : "no") << std::endl;
}
