#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <iostream>

struct Functor
{
public:
  Functor();
  void operator()(int number);
  void print() const;

private:
  int max_;
  int min_;
  double sum_;
  unsigned int counter_;
  unsigned int positive_;
  unsigned int negative_;
  long long int oddsum_;
  long long int evensum_;
  int first_;
  int last_;
};

#endif
