#include <vector>
#include <sstream>
#include <numeric>
#include <iterator>
#include <iostream>
#include <algorithm>

#include "shape.hpp"

const int TRIANGLE_VERTICES = 3;
const int RECTANGLE_VERTICES = 4;
const int PENTAGON_VERTICES = 5;

Shape readShapeData(const std::string& string)
{
  std::stringstream strStream(string);

  int verticesCount;
  strStream >> verticesCount;

  Shape shape;
  const size_t strLength = string.length();
  for (int i = 0; i < verticesCount; ++i)
  {
    Point point{ 0, 0 };
    strStream.ignore(strLength, '(');
    strStream >> point.x;

    strStream.ignore(strLength, ';');
    strStream >> point.y;

    strStream.ignore(strLength, ')');
    shape.push_back(point);
  }

  if (strStream.fail())
  {
    throw std::invalid_argument("Invalid value of points!");
  }

  std::string extraPoints;
  getline(strStream >> std::ws, extraPoints);

  if (!extraPoints.empty())
  {
    throw std::invalid_argument("Invalid number of points!");
  }

  return shape;
}

bool isRectangle(const Shape& shape)
{
  if (shape.size() != RECTANGLE_VERTICES)
  {
    return false;
  }

  int d1 = (shape[0].x - shape[2].x) * (shape[0].x - shape[2].x) + (shape[0].y - shape[2].y) * (shape[0].y - shape[2].y);
  int d2 = (shape[1].x - shape[3].x) * (shape[1].x - shape[3].x) + (shape[1].y - shape[3].y) * (shape[1].y - shape[3].y);

  return d1 == d2;
}


bool isTriangle(const Shape& shape)
{
  return (shape.size() == TRIANGLE_VERTICES);
}

bool isSquare(const Shape& shape)
{
   if (!isRectangle(shape))
  {
    return false;
  }

  if (shape[1].x != shape[0].x)
  {
    if (std::abs(shape[1].x - shape[0].x) != std::abs(shape[2].y - shape[1].y))
    {
      return false;
    }
  }
  else
  {
    if (std::abs(shape[2].x - shape[1].x) != std::abs(shape[1].y - shape[0].y))
    {
      return false;
    }
  }
  return true;
}

void deletePentagon(std::vector < Shape >& vector)
{
  vector.erase(std::remove_if(vector.begin(), vector.end(), 
    [](const Shape& shape) { return shape.size() == PENTAGON_VERTICES; }), vector.end());
}

void sort(std::vector< Shape >& vector)
{
  std::sort(vector.begin(), vector.end(), [](const Shape& lhs, const Shape& rhs) 
    { 
      if (lhs.size() < rhs.size()) 
      { 
        return true; 
      } 
      if ((lhs.size() == RECTANGLE_VERTICES) && (rhs.size() == RECTANGLE_VERTICES)) 
      { 
        if (isSquare(lhs)) 
        { 
          if (isSquare(rhs)) 
          { 
            return lhs[0].x < rhs[0].x; 
          } 
          return true; 
        } 
      } 
      return false; 
    });
}

Shape createPointVector(const std::vector < Shape >& vector)
{
  Shape pointVector;

  for (const auto& it : vector)
  {
    pointVector.push_back(it.at(0));
  }

  return pointVector;
}

void task2()
{
  std::vector < Shape > vector;

  std::string str;
  while (std::getline(std::cin >> std::ws, str))
  {
    if (str.empty())
    {
      continue;
    }

    vector.push_back(readShapeData(str));
  }

  size_t verticesCount = 0, squaresCount = 0, rectanglesCount = 0, trianglesCount = 0;

  auto iter = vector.begin();
  while (iter != vector.end())
  {
    verticesCount += iter->size();
    if (isRectangle(*iter))
    {
      ++rectanglesCount;
      if (isSquare(*iter))
      {
        ++squaresCount;
      }
    }
    else if (isTriangle(*iter))
    {
      ++trianglesCount;
    }
    ++iter;
  }

  std::cout << "Vertices: " << verticesCount << "\n";
  std::cout << "Triangles: " << trianglesCount << "\n";
  std::cout << "Squares: " << squaresCount << "\n";
  std::cout << "Rectangles: " << rectanglesCount << "\n";

  deletePentagon(vector);

  const Shape pointVector = createPointVector(vector);
  std::cout << "Points:";
  for (auto it : pointVector)
  {
    std::cout << " (" << it.x << ";" << it.y << ")";
  }

  sort(vector);

  std::cout << "\nShapes:\n";
  for (auto& it : vector)
  {
    std::cout << it.size();
    for (auto& sIt : it)
    {
      std::cout << " (" << sIt.x << ";" << sIt.y << ")";
    }
    std::cout << "\n";
  }
}
