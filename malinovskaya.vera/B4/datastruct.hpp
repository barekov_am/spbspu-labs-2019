#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP

#include <string>
#include <vector>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

DataStruct read(std::istream& input);
bool compare(DataStruct& struct1, DataStruct& struct2);
void print(std::vector<DataStruct>& vector);

#endif
