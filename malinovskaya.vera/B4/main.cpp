#include <algorithm>
#include <iostream>
#include <sstream>
#include "datastruct.hpp"

int main()
{
  try
  {
    std::vector<DataStruct> vector;
    std::string inputLine;
    while (std::getline(std::cin, inputLine))
    {
      if (std::cin.fail())
      {
        throw std::ios_base::failure("Reading failed");
      }
      std::stringstream stream(inputLine);
      DataStruct dataStruct = read(stream);
      vector.push_back(dataStruct);
    }

    std::sort(vector.begin(), vector.end(), compare);
    print(vector);
  }

  catch (const std::exception& exception)
  {
    std::cerr << exception.what();
    return 1;
  }

  return 0;
}
