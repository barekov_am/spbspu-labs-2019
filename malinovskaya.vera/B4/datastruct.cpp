#include <iostream>
#include "datastruct.hpp"

DataStruct read(std::istream& input)
{
  if (input.eof())
  {
    throw std::invalid_argument("Empty line");
  }

  DataStruct dataStruct;
  input >> dataStruct.key1;
  if (input.fail())
  {
    throw std::invalid_argument("Reading failed");
  }
  if ((abs(dataStruct.key1) > 5) || input.get() != ',' || input.eof())
  {
    throw std::invalid_argument("Invalid argument");
  }

  input >> dataStruct.key2;
  if (input.fail())
  {
    throw std::invalid_argument("Reading failed");
  }
  if ((abs(dataStruct.key2) > 5) || input.get() != ',' || input.eof())
  {
    throw std::invalid_argument("Invalid argument");
  }

  std::getline(input >> std::ws, dataStruct.str);
  if (input.fail())
  {
    throw std::invalid_argument("Reading failed");
  }

  return dataStruct;
}

bool compare(DataStruct& struct1, DataStruct& struct2)
{
  if (struct1.key1 == struct2.key1 && struct1.key2 == struct2.key2)
  {
    return (struct1.str.length() < struct2.str.length());
  }
  else if (struct1.key1 == struct2.key1)
  {
    return (struct1.key2 < struct2.key2);
  }
  else
  {
    return (struct1.key1 < struct2.key1);
  }
};

void print(std::vector<DataStruct>& vector)
{
  for (auto& i : vector)
  {
    std::cout << i.key1 << ", " << i.key2 << ", " << i.str << std::endl;
  }
}
