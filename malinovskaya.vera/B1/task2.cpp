#include <fstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <sys/stat.h>
#include <vector>
#include "sort.hpp"

const size_t capacity = 256;

void task2(const char* file)
{
  std::ifstream fileIn(file);

  if (fileIn.fail())
  {
    throw std::ios_base::failure("File is not open");
  }

  std::unique_ptr<char[], decltype(&free)> array(static_cast<char*>(malloc(capacity)), free);
  if (!array)
  {
    throw std::runtime_error("Memory allocation error");
  }

  size_t size = 0;
  size_t counter = 1;
  while (!fileIn.eof())
  {
    fileIn.read(&array[size], capacity);
    size += fileIn.gcount();

    if (size == (capacity * counter)) 
    {
      counter++;
      std::unique_ptr<char[], decltype(&free)> tmpArray(static_cast<char*>(realloc(array.get(), capacity * counter)), free);

      if (!tmpArray)
      {
        throw std::runtime_error("Memory reallocate error");
      }

      array.release();
      std::swap(array, tmpArray);
    }
  }

  fileIn.close();
  if (fileIn.is_open())
  {
    throw std::ios_base::failure("File is not closed");
  }

  std::vector<char> vector(&array[0], &array[size]);
  for (const auto & element : vector)
  {
    std::cout << element;
  }
}
