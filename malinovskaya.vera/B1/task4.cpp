#include <ctime>
#include <random>
#include <stdexcept>
#include <vector>
#include "sort.hpp"
#include "strategy.hpp"

void fillRandom(double* array, size_t size)
{
  std::mt19937 gen(time(0));
  std::uniform_real_distribution<> dis(-1.0, 1.0);
  for (size_t i = 0; i < size; i++)
  {
    array[i] = dis(gen);
  }
}

void task4(size_t size, const char* dir)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Wrong size");
  }
  std::vector<double> vector;
  vector.resize(size);
  fillRandom(&vector[0], size);
  print(vector);
  sort<AccessAt>(vector, dir);
  print(vector);
}
