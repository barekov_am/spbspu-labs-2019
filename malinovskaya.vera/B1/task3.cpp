#include <iostream>
#include <stdexcept>
#include <vector>
#include "sort.hpp"

void task3()
{
  std::vector<int> vector;
  int num = -1;

  while(std::cin >> num)
  {
    if (num == 0)
    {
      break;
    }
    vector.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Reading failed");
  }

  if (vector.empty())
  {
    return;
  }

  if (num != 0)
  {
    throw std::runtime_error("Zero at the end is expected");
  }

  auto i = vector.begin();
  switch(vector.back())
  {
    case 1:
    {
      while (i != vector.end())
      {
        i = (*i % 2 == 0) ? vector.erase(i) : ++i;
      }
      break;
    }

    case 2:
    {
      while (i != vector.end())
      {
        i = (*i % 3 == 0) ? (vector.insert(++i, 3, 1) + 3) : ++i;
      }
      break;
    }

    default:
    {
      break;
    }
  }

  print(vector);
}
