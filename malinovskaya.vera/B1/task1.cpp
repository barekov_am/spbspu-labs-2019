#include <forward_list>
#include <iostream>
#include <stdexcept>
#include <vector>
#include "sort.hpp"
#include "strategy.hpp"

void task1(const char* dir)
{
  std::vector<int> vectorBraces;

  int tmp = 0;
  while (std::cin >> tmp)
  {
    vectorBraces.push_back(tmp);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Reading failure");
  }

  std::vector<int> vectorAt = vectorBraces;
  std::forward_list<int> list(vectorBraces.begin(), vectorBraces.end());

  sort<AccessBraces>(vectorBraces, dir);
  sort<AccessAt>(vectorAt, dir);
  sort<AccessIterator>(list, dir);

  print(vectorBraces);
  print(vectorAt);
  print(list);
}
