#ifndef B1_SORT_HPP
#define B1_SORT_HPP

#include <functional>
#include <iostream>
#include <stdexcept>
#include "strategy.hpp"

template <typename T>
void print(const T& container)
{
  for(const auto& element : container)
  {
    std::cout << element << " ";
  }
  std::cout << std::endl;
}

template <template <typename> class Strategy, typename T>
void sort(T& container, const std::string& dir)
{
  using valueType = typename T::value_type;
  const auto begin = Strategy<T>::begin(container);
  const auto end = Strategy<T>::end(container);

  std::function<bool (valueType, valueType)> compare;
  if (dir == "ascending")
  {
    compare = [](valueType x, valueType y){ return x > y; };
  }
  else if (dir == "descending")
  {
    compare = [](valueType x, valueType y){ return x < y; };
  }
  else
  {
    throw std::invalid_argument("Wrong direction argument");
  }

  for(auto i = begin; i != end; ++i)
  {
    auto tmp = i;

    for(auto j = i; j != end; ++j)
    {
      if (compare(Strategy<T>::get(container, tmp), Strategy<T>::get(container, j)))
      {
        tmp = j;
      }
    }

    if(tmp != i)
    {
      std::swap(Strategy<T>::get(container, i), Strategy<T>::get(container, tmp));
    }
  }
}

#endif
