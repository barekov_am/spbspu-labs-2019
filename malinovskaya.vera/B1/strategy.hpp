#ifndef B1_STRATEGY_HPP
#define B1_STRATEGY_HPP
#include <cstdlib>

template <typename Container>
struct AccessBraces
{
  using valueType = typename Container::value_type;

  static size_t begin(Container&)
  {
    return 0;
  }

  static size_t end(Container& container)
  {
    return container.size();
  }

  static valueType& get(Container& container, size_t index)
  {
    return container[index];
  }
};

template <typename Container>
struct AccessAt
{
  using valueType = typename Container::value_type;

  static size_t begin(Container&)
  {
    return 0;
  }

  static size_t end(Container& container)
  {
    return container.size();
  }

  static valueType& get(Container& container, size_t index)
  {
    return container.at(index);
  }
};

template <typename Container>
struct AccessIterator
{
  using valueType = typename Container::value_type;

  static auto begin(Container& container)
  {
    return container.begin();
  }

  static auto end(Container& container)
  {
    return container.end();
  }

  template <typename Iterator>
  static valueType& get(Container&, Iterator iterator)
  {
    return *iterator;
  }
};

#endif
