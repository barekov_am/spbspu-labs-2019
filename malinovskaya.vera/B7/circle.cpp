#include "circle.hpp"

#include <iostream>

Circle::Circle(const point_t center) :
  Shape(center)
{}

void Circle::draw() const
{
  std::cout << "CIRCLE (" << center_.x << "; " << center_.y << ")" << std::endl;
}
