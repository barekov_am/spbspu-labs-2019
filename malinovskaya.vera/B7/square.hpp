#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "shape.hpp"

class Square : public Shape
{
public:
  Square(point_t center);
  ~Square() override = default;
  void draw() const override;
};

#endif 
