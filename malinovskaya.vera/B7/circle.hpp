#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

class Circle : public Shape
{
public:
  Circle(point_t center);
  ~Circle() override = default;
  void draw() const override;
};

#endif
