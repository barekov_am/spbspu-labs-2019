#include <algorithm>
#include <cmath>
#include <iostream>
#include <iterator>
#include <vector>

void task1()
{
  std::vector<double> vector{ std::istream_iterator<double>(std::cin), std::istream_iterator<double>() };

  if (!std::cin.eof())
  {
    throw std::invalid_argument("Reading failed");
  }
  if (vector.empty())
  {
    return;
  }

  std::transform(vector.begin(), vector.end(), std::ostream_iterator<double>(std::cout, "\n"), [](double& list) { return list * M_PI; });
}
