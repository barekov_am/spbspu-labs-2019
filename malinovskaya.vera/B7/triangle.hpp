#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

class Triangle : public Shape
{
public:
  Triangle(point_t center);
  ~Triangle() override = default;
  void draw() const override;
};

#endif
