#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <istream>
#include <memory>

struct point_t
{
  double x;
  double y;
};


class Shape
{
public:
  using shape_ptr = std::shared_ptr<Shape>;
  Shape(point_t center_);

  virtual ~Shape() = default;
  bool isMoreLeft(const Shape & shape) const;
  bool isUpper(const Shape & shape) const;
  virtual void draw() const = 0;

protected:
  point_t center_;

};

#endif
