#include <iostream>
#include <algorithm>
#include <string>
#include <list>

#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"

void task2()
{
  Shape::shape_ptr shape_ptr;
  std::list<Shape::shape_ptr> list;
  std::string line;

  while (std::getline(std::cin, line, '\n'))
  {

    line.erase(remove(line.begin(), line.end(), ' '), line.end());
    line.erase(remove(line.begin(), line.end(), '\t'), line.end());

    if (line.empty())
    {
      continue;
    }

    size_t openBracket = line.find_first_of('(');
    size_t semicolon = line.find_first_of(';');
    size_t closeBracket = line.find_first_of(')');

    if (openBracket == std::string::npos || semicolon == std::string::npos || closeBracket == std::string::npos)
    {
      throw std::runtime_error("Invalid input data!\n");
    }

    std::string shape = line.substr(0, openBracket);

    point_t point{};
    point.x = std::stod(line.substr(openBracket + 1, semicolon - openBracket + 1));
    point.y = std::stod(line.substr(semicolon + 1, closeBracket - semicolon + 1));

    if (shape == "CIRCLE")
    {
      shape_ptr = std::make_shared<Circle>(point);
      list.push_back(shape_ptr);
    }
    else if (shape == "SQUARE")
    {
      shape_ptr = std::make_shared<Square>(point);
      list.push_back(shape_ptr);
    }
    else if (shape == "TRIANGLE")
    {
      shape_ptr = std::make_shared<Triangle>(point);
      list.push_back(shape_ptr);
    }
    else
    {
      throw std::runtime_error("Invalid shape");
    }
  }

  auto compareLeftRight = [](const Shape::shape_ptr& leftFigure, const Shape::shape_ptr& rightFigure)
  { return leftFigure->isMoreLeft(*rightFigure); };
  auto compareTopBottom = [](const Shape::shape_ptr& topFigure, const Shape::shape_ptr& bottomFigure)
  { return topFigure->isUpper(*bottomFigure); };

  std::cout << "Original: " << std::endl;
  std::for_each(list.begin(), list.end(), [](const Shape::shape_ptr& ptr) { ptr->draw(); });

  std::cout << "Left-Right: " << std::endl;
  list.sort(compareLeftRight);
  std::for_each(list.begin(), list.end(), [](const Shape::shape_ptr& ptr) { ptr->draw(); });

  std::cout << "Right-Left: " << std::endl;
  std::for_each(list.rbegin(), list.rend(), [](const Shape::shape_ptr& ptr) { ptr->draw(); });

  std::cout << "Top-Bottom: " << std::endl;
  list.sort(compareTopBottom);
  std::for_each(list.begin(), list.end(), [](const Shape::shape_ptr& ptr) { ptr->draw(); });

  std::cout << "Bottom-Top: " << std::endl;
  std::for_each(list.rbegin(), list.rend(), [](const Shape::shape_ptr& ptr) { ptr->draw(); });
}
