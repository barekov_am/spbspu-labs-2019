#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

int main()
{
  malinovskaya::Circle myCircle1({4.0, 2.0}, 3.0);
  malinovskaya::Rectangle myRectangle2({3.0, 4.0, {5.0, 8.0}});
  malinovskaya::Triangle myTriangle1({0.0, 0.0}, {-1.0, 5.0}, {3.0, 6.0});
  std::cout << std::endl;
  malinovskaya::Shape::pointer shape1 = std::make_shared<malinovskaya::Rectangle>(myRectangle2);
  malinovskaya::Shape::pointer shape2 = std::make_shared<malinovskaya::Triangle>(myTriangle1);
  malinovskaya::Shape::pointer shape3 = std::make_shared<malinovskaya::Circle>(myCircle1);
  std::cout << std::endl;
  std::cout << "triangle" << std::endl;
  myTriangle1.print();
  myTriangle1.rotate(390);
  myTriangle1.print();
  std::cout << std::endl;
  std::cout << "composite shape" << std::endl;
  malinovskaya::CompositeShape composite_shape;
  composite_shape.add(shape1);
  composite_shape.add(shape2);
  composite_shape.add(shape3);
  std::cout << std::endl;
  composite_shape.move({-5.0, -10.0});
  composite_shape.print();
  std::cout << std::endl;
  composite_shape.move(-8.0, 2.0);
  composite_shape.print();
  std::cout << std::endl;
  composite_shape.rotate(30);
  composite_shape.print();
  std::cout << std::endl;
  composite_shape.scale(2);
  composite_shape.print();
  std::cout << std::endl;
  composite_shape.remove(1);
  composite_shape.print();
  std::cout << std::endl;
  malinovskaya::Matrix matrix = malinovskaya::part(composite_shape);
  std::cout << "matrix" << std::endl;
  std::cout << "rows " << matrix.getRows() << std::endl;
  std::cout << "columns " << matrix.getColumns() << std::endl;

  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    for (size_t j = 0; j < matrix.getLevelSize(i); j++)
    {
      std::cout << "level " << i << ";" << std::endl;
      std::cout << "center (";
      std::cout << matrix[i][j]->getFrameRect().pos.x << ", " << matrix[i][j]->getFrameRect().pos.y << ");"<< std::endl;
    }
    std::cout << std::endl;
  }

  return 0;
}
