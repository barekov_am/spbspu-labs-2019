#include <iostream>
#include <sstream>

#include "phone-book-interface.hpp"
#include "commands.hpp"

void task1()
{
  PhoneBookInterface bookInter;
  std::string inputLine;

  while (std::getline(std::cin, inputLine)) 
  {
    if (std::cin.fail()) 
    {
      throw std::ios_base::failure("Reading failed");
    }

    std::stringstream stream(inputLine);
    std::string streamCommand;
    stream >> streamCommand;

    if (streamCommand == "add") 
    {
      add(bookInter, stream);
    }
    else if (streamCommand == "store") 
    {
      store(bookInter, stream);
    }
    else if (streamCommand == "insert") 
    {
      insert(bookInter, stream);
    }
    else if (streamCommand == "delete") 
    {
      deleteRecord(bookInter, stream);
    }
    else if (streamCommand == "show") 
    {
      show(bookInter, stream);
    }
    else if (streamCommand == "move") 
    {
      move(bookInter, stream);
    }
    else 
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
