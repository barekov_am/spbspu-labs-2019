#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <string>
#include "phone-book-interface.hpp"

std::string getName(std::string& name);
std::string getNumber(std::string& number);
std::string getMarkName(std::string& markname);

void add(PhoneBookInterface& phBookInter, std::stringstream& stream);
void store(PhoneBookInterface& phBookInter, std::stringstream& stream);
void insert(PhoneBookInterface& phBookInter, std::stringstream& stream);
void deleteRecord(PhoneBookInterface& phBookInter, std::stringstream& stream);
void show(PhoneBookInterface& phBookInter, std::stringstream& stream);
void move(PhoneBookInterface& phBookInter, std::stringstream& stream);

#endif
