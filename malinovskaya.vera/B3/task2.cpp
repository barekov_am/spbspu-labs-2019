#include <algorithm>
#include <iostream>

#include "factorial-container.hpp"

void task2()
{
  FactorialContainer container;
  std::copy(container.begin(), container.end(), std::ostream_iterator<long long>(std::cout, " "));
  std::cout << std::endl;
  std::reverse_copy(container.begin(), container.end(), std::ostream_iterator<long long>(std::cout, " "));
  std::cout << std::endl;
}
