#ifndef FACTORIAL_CONTAINER_HPP
#define FACTORIAL_CONTAINER_HPP

#include "iterator.hpp"

class FactorialContainer 
{
public:
  FactorialContainer() = default;
  Iterator begin();
  Iterator end();
};

#endif
