#include "iterator.hpp"

const int MIN = 1;
const int MAX = 10;

Iterator::Iterator() :
  Iterator(1)
{}

Iterator::Iterator(int index) :
  value_(getValue(index)),
  index_(index)
{
  if ((index < MIN) || (index > MAX + 1)) 
  {
    throw std::out_of_range("Index is out of range");
  }
}

size_t& Iterator::operator*()
{
  return value_;
}

size_t* Iterator::operator->()
{
  return &value_;
}

Iterator& Iterator::operator++()
{
  if (index_ > MAX) 
  {
    throw std::out_of_range("Index is out of range");
  }
  ++index_;
  value_ *= index_;
  return *this;
}

Iterator& Iterator::operator--()
{
  if (index_ <= MIN)
  {
    throw std::out_of_range("Index is out of range");
  }
  value_ /= index_;
  --index_;
  return *this;
}

Iterator Iterator::operator++(int)
{
  Iterator tmp = *this;
  ++(*this);
  return tmp;
}

Iterator Iterator::operator--(int)
{
  Iterator tmp = *this;
  --(*this);
  return tmp;
}

bool Iterator::operator ==(Iterator iterator) const
{
  return (value_ == iterator.value_) && (index_ == iterator.index_);
}

bool Iterator::operator !=(Iterator iterator) const
{
  return !(*this == iterator);
}

size_t Iterator::getValue(size_t index) const
{
  if (index <= MIN)
  {
    return 1;
  }
  return index * getValue(index - 1);
}
