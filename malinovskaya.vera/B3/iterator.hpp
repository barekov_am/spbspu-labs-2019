#ifndef ITERATOR_HPP
#define ITERATOR_HPP

#include <iterator>

class Iterator : public std::iterator<std::bidirectional_iterator_tag, size_t>
{
public:
  Iterator();
  Iterator(int index);
  size_t& operator *();
  size_t* operator ->();
  Iterator& operator ++();
  Iterator& operator --();  
  Iterator operator ++(int);
  Iterator operator --(int);
  bool operator ==(Iterator iterator) const;
  bool operator !=(Iterator iterator) const;

private:
  size_t value_;
  size_t index_;
  size_t getValue(size_t index) const;
};

#endif
