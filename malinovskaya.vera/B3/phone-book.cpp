#include <iostream>
#include "phone-book.hpp"

void PhoneBook::show(iterator iter)
{
  std::cout << iter->number << " " << iter->name << std::endl;
}

PhoneBook::iterator PhoneBook::next(iterator iter)
{
  if (iter != list_.end())
  {
    return ++iter;
  }
  else
  {
    return iter;
  }
}

PhoneBook::iterator PhoneBook::prev(iterator iter)
{
  if (iter != list_.begin())
  {
    return --iter;
  }
  else
  {
    return iter;
  }
}

PhoneBook::iterator PhoneBook::move(iterator iter, const int steps)
{
  int counter = 0;
  if (steps >= 0) 
  {
    while ((std::next(iter) != list_.end()) && (counter != steps)) 
    {
      ++iter;
      ++counter;
    }
  }
  else 
  {
    while ((iter != list_.begin()) && (counter != steps)) 
    {
      --iter;
      --counter;
    }
  }
  return iter;
}

void PhoneBook::insert(iterator iter, record_t& record)
{
  list_.insert(iter, record);
}

void PhoneBook::pushBack(record_t& record)
{
  list_.push_back(record);
}

void PhoneBook::replace(iterator iter, record_t& record)
{
  *iter = record;
}

PhoneBook::iterator PhoneBook::remove(iterator iter)
{
  return list_.erase(iter);
}

PhoneBook::iterator PhoneBook::begin()
{
  return list_.begin();
}

PhoneBook::iterator PhoneBook::end()
{
  return list_.end();
}

bool PhoneBook::empty()
{
  return list_.empty();
}
