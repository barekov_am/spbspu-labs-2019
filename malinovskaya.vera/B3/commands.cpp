#include <iostream>
#include <sstream>

#include "phone-book-interface.hpp"

std::string checkName(std::string& name)
{
  if ((name.front() != '\"') || (name.back() != '\"') || name.empty()) 
  {
    return "";
  }

  name.erase(name.begin());

  size_t i = 0;
  while ((i < name.size()) && (name[i] != '\"')) 
  {
    if (name[i] == '\\') 
    {
      if (name[i + 1] == '\"' && i + 2 < name.size()) 
      {
        name.erase(i, 1);
      }
      else 
      {
        return "";
      }
    }
    ++i;
  }

  if (i == name.size()) 
  {
    return "";
  }
  name.erase(i);

  if (name.empty()) 
  {
    return "";
  }

  return name;
}

std::string checkNumber(std::string& number)
{
  if (number.empty()) 
  {
    return "";
  }

  for (size_t i = 0; i < number.size(); i++) 
  {
    if (!std::isdigit(number[i])) 
    {
      return "";
    }
  }

  return number;
}

std::string checkMarkName(std::string& name)
{
  if (name.empty())
  {
    return "";
  }

  for (size_t i = 0; i < name.size(); i++) 
  {
    if (!isalnum(name[i]) && (name[i] != '-')) 
    {
      return "";
    }
  }

  return name;
}

void add(PhoneBookInterface& bookInter, std::stringstream& stream)
{
  std::string number, name, line;

  stream >> std::ws >> number;
  std::getline(stream >> std::ws, name);

  name = checkName(name);
  number = checkNumber(number);

  if (name.empty() || number.empty()) 
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  PhoneBook::record_t record = {name, number};
  bookInter.add(record);
}

void store(PhoneBookInterface& bookInter, std::stringstream& stream)
{
  std::string markName, newMarkName;

  stream >> std::ws >> markName;
  markName = checkMarkName(markName);

  stream >> std::ws >> newMarkName;
  newMarkName = checkMarkName(newMarkName);

  if (markName.empty() || newMarkName.empty()) 
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  bookInter.store(markName, newMarkName);
}

void insert(PhoneBookInterface& bookInter, std::stringstream& stream)
{
  std::string direction, markName, number, name;

  stream >> std::ws >> direction;
  stream >> std::ws >> markName;

  markName = checkMarkName(markName);

  stream >> std::ws >> number;
  number = checkNumber(number);

  std::getline(stream >> std::ws, name);
  name = checkName(name);

  PhoneBookInterface::InsertPosition position;

  if (markName.empty() || name.empty() || number.empty()) 
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  PhoneBook::record_t record = {name, number};

  if (direction == "before") 
  {
    position = PhoneBookInterface::InsertPosition::before;
    bookInter.insert(position, markName, record);
  }
  else if (direction == "after") 
  {
    position = PhoneBookInterface::InsertPosition::after;
    bookInter.insert(position, markName, record);
  }
  else 
  {
    std::cout << "<INVALID COMMAND>\n";
  }
}

void deleteRecord(PhoneBookInterface& bookInter, std::stringstream& stream)
{
  std::string markName;
  stream >> std::ws >> markName;

  markName = checkMarkName(markName);

  if (markName.empty()) 
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  bookInter.deleteNote(markName);
}

void show(PhoneBookInterface& bookInter, std::stringstream& stream)
{
  std::string markName;
  stream >> std::ws >> markName;

  markName = checkMarkName(markName);

  if (markName.empty()) 
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  bookInter.show(markName);
}

void move(PhoneBookInterface& bookInter, std::stringstream& stream)
{
  std::string markName, pos;

  stream >> std::ws >> markName;
  markName = checkMarkName(markName);

  if (markName.empty()) 
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  stream >> std::ws >> pos;

  if (pos == "first")
  {
    PhoneBookInterface::MovePosition position = PhoneBookInterface::MovePosition::first;
    bookInter.move(markName, position);
  }
  else if (pos == "last")
  {
    PhoneBookInterface::MovePosition position = PhoneBookInterface::MovePosition::last;
    bookInter.move(markName, position);
  }
  else 
  {
    int sign = 1;
    if (pos[0] == '-')
    {
      sign = -1;
      pos.erase(pos.begin());
    }
    else if (pos[0] == '+')
    {
      pos.erase(pos.begin());
    }

    pos = checkNumber(pos);

    if (pos.empty())
    {
      std::cout << "<INVALID STEP>\n";
      return;
    }

    bookInter.move(markName, (stoi(pos) * sign));
  }
}
