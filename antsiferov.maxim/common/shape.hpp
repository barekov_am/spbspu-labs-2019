#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"

namespace antsiferov
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t &position_) = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void printShapeInfo() const = 0;
    virtual void scale(double coef) = 0;
    virtual void rotate(double) = 0;
  };
}

#endif
