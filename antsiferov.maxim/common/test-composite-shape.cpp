#include <memory>

#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(CompositeShapeTestSuite)

BOOST_AUTO_TEST_CASE(invariabilityAfterMovingCompositeShape)
{
  antsiferov::Rectangle rec({0.0, 0.0}, 1.0, 5.0);
  antsiferov::Circle cir({8.0, 1.5}, 7.0);
  antsiferov::CompositeShape comp(std::make_unique<antsiferov::Rectangle>(rec));

  comp.add(std::make_unique<antsiferov::Circle>(cir));

  const antsiferov::rectangle_t compBefore = comp.getFrameRect();
  const double areaBefore = comp.getArea();
  const size_t countBefore = comp.getCount();

  comp.move(2.7, 5.8);
  BOOST_CHECK_CLOSE(compBefore.width, comp.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(compBefore.height, comp.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, comp.getArea(), ACCURACY);
  BOOST_CHECK_EQUAL(countBefore, comp.getCount());

  comp.move({3.2, 7.4});
  BOOST_CHECK_CLOSE(compBefore.width, comp.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(compBefore.height, comp.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, comp.getArea(), ACCURACY);
  BOOST_CHECK_EQUAL(countBefore, comp.getCount());
}

BOOST_AUTO_TEST_CASE(CompositeShapeAreaAfterScale)
{
  antsiferov::Rectangle rec({0.0, 0.0}, 4.9, 5.6);
  antsiferov::Circle cir({8.0, 1.5}, 7.0);
  antsiferov::CompositeShape comp(std::make_unique<antsiferov::Rectangle>(rec));

  comp.add(std::make_unique<antsiferov::Circle>(cir));

  const double areaBefore1 = comp.getArea();
  const double scaleFactor1 = 25.6;

  comp.scale(scaleFactor1);
  BOOST_CHECK_CLOSE(areaBefore1 * scaleFactor1 * scaleFactor1, comp.getArea(), ACCURACY);

  const double areaBefore2 = comp.getArea();
  const double scaleFactor2 = 0.1;

  comp.scale(scaleFactor2);
  BOOST_CHECK_CLOSE(areaBefore2 * scaleFactor2 * scaleFactor2, comp.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(CompositeShapeFrameRectAfterScale)
{
  antsiferov::Rectangle rec({0.0, 0.0}, 6.0, 7.0);
  antsiferov::Circle cir({8.0, 1.5}, 7.0);
  antsiferov::CompositeShape comp(std::make_unique<antsiferov::Circle>(cir));

  comp.add(std::make_unique<antsiferov::Circle>(cir));

  const antsiferov::rectangle_t frameRectBefore = comp.getFrameRect();
  const double scaleFactor = 8.9;

  comp.scale(scaleFactor);
  const antsiferov::rectangle_t frameRectAfter = comp.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBefore.width * scaleFactor, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height * scaleFactor, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(CompositeShapeIncorrectParameters)
{
  antsiferov::Rectangle rec({0.0, 0.0}, 4.1, 1.4);
  antsiferov::Circle cir({8.0, 1.5}, 7.0);
  antsiferov::CompositeShape comp(std::make_unique<antsiferov::Rectangle>(rec));

  comp.add(std::make_unique<antsiferov::Circle>(cir));

  BOOST_CHECK_THROW(comp.scale(-1.0), std::invalid_argument);
  BOOST_CHECK_THROW(comp.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(comp.remove(5), std::invalid_argument);
  BOOST_CHECK_THROW(comp[2], std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(CompositeShapeAfterAddShape)
{
  antsiferov::Rectangle rec({0.0, 0.0}, 4.0, 7.0);
  antsiferov::Circle cir({8.0, 1.5}, 7.0);
  antsiferov::CompositeShape comp(std::make_unique<antsiferov::Rectangle>(rec));

  const double areaBefore = comp.getArea();
  const size_t countBefore = comp.getCount();

  comp.add(std::make_unique<antsiferov::Circle>(cir));
  
  const double areaCir = comp[1]->getArea();
  BOOST_CHECK_CLOSE(areaBefore + areaCir, comp.getArea(), ACCURACY);
  BOOST_CHECK_EQUAL(countBefore + 1, comp.getCount());
}

BOOST_AUTO_TEST_CASE(CompositeShapeAfterDeleteByIndexShape)
{
  antsiferov::Rectangle rec({9.8, 7.6}, 5.4, 3.2);
  antsiferov::Circle cir({8.0, 1.5}, 7.0);
  antsiferov::CompositeShape comp(std::make_unique<antsiferov::Rectangle>(rec));

  comp.add(std::make_unique<antsiferov::Circle>(cir));

  const double areaBefore = comp.getArea();
  const double areaSecondShape = comp[1]->getArea();
  const size_t countBefore = comp.getCount();

  comp.remove(1);
  BOOST_CHECK_CLOSE(comp.getArea(), areaBefore - areaSecondShape, ACCURACY);
  BOOST_CHECK_EQUAL(countBefore - 1, comp.getCount());
}

BOOST_AUTO_TEST_CASE(CopyConstructor)
{
  antsiferov::Rectangle rec({7.7, 8.8}, 8.8, 8.8);
  antsiferov::Circle cir({8.0, 1.5}, 7.0);
  antsiferov::CompositeShape comp(std::make_unique<antsiferov::Rectangle>(rec));

  comp.add(std::make_unique<antsiferov::Circle>(cir));

  const double compArea = comp.getArea();
  const antsiferov::rectangle_t compFrameRect = comp.getFrameRect();
  const size_t compCount = comp.getCount();

  antsiferov::CompositeShape test_comp(comp);

  const double testCompArea = test_comp.getArea();
  const antsiferov::rectangle_t testCompFrameRect = test_comp.getFrameRect();
  const size_t testCompCount = test_comp.getCount();

  BOOST_CHECK_CLOSE(compArea, testCompArea, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.width, testCompFrameRect.width, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.height, testCompFrameRect.height, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.pos.x, testCompFrameRect.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.pos.y, testCompFrameRect.pos.y, ACCURACY);
  BOOST_CHECK_EQUAL(compCount, testCompCount);
}

BOOST_AUTO_TEST_CASE(CopyOperator)
{
  antsiferov::Rectangle rec({0.0, 0.0}, 5.0, 6.0);
  antsiferov::Circle cir({8.0, 1.5}, 7.0);
  antsiferov::CompositeShape comp(std::make_unique<antsiferov::Rectangle>(rec));

  comp.add(std::make_unique<antsiferov::Circle>(cir));

  const double compArea = comp.getArea();
  const antsiferov::rectangle_t compFrameRect = comp.getFrameRect();
  const size_t compCount = comp.getCount();

  antsiferov::CompositeShape test_comp = comp;

  const double testCompArea = test_comp.getArea();
  const antsiferov::rectangle_t testCompFrameRect = test_comp.getFrameRect();
  const size_t testCompCount = test_comp.getCount();

  BOOST_CHECK_CLOSE(compArea, testCompArea, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.width, testCompFrameRect.width, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.height, testCompFrameRect.height, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.pos.x, testCompFrameRect.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.pos.y, testCompFrameRect.pos.y, ACCURACY);
  BOOST_CHECK_EQUAL(compCount, testCompCount);
}

BOOST_AUTO_TEST_SUITE_END()
