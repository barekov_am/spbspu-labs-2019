#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "spliting.hpp"
#include "matrix.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(layeringTestSuite)

BOOST_AUTO_TEST_CASE(emptyCompositeSplitingTest)
{
  const antsiferov::CompositeShape emptyComposite;
  antsiferov::Matrix<antsiferov::Shape> testMatrix = antsiferov::split(emptyComposite);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 0);
  BOOST_CHECK_THROW(testMatrix[0], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeSplitingTest)
{
  antsiferov::Rectangle rectangle1({2, 2}, 6, 4);
  antsiferov::Rectangle rectangle2({-10, -1}, 3, 3);
  antsiferov::Rectangle rectangle3({0, -1}, 2, 2);
  antsiferov::Circle circle1({0, 0}, 3);
  antsiferov::CompositeShape compositeShape;
  compositeShape.add(std::make_shared<antsiferov::Rectangle>(rectangle1));   
  compositeShape.add(std::make_shared<antsiferov::Circle>(circle1));         
  compositeShape.add(std::make_shared<antsiferov::Rectangle>(rectangle2));   
  compositeShape.add(std::make_shared<antsiferov::Rectangle>(rectangle3));   

  antsiferov::Matrix<antsiferov::Shape> matrix = antsiferov::split(compositeShape);

  BOOST_CHECK_EQUAL(matrix.getRows(), 3);
  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), 2);
  BOOST_CHECK_EQUAL(matrix[0][0], compositeShape[0]);
  BOOST_CHECK_EQUAL(matrix[0][1], compositeShape[2]);
  BOOST_CHECK_EQUAL(matrix[1][0], compositeShape[1]);
  BOOST_CHECK_EQUAL(matrix[2][0], compositeShape[3]);
  BOOST_CHECK_EQUAL(matrix[0].getSize(), 2);
  BOOST_CHECK_EQUAL(matrix[1].getSize(), 1);
  BOOST_CHECK_EQUAL(matrix[2].getSize(), 1);

  const int sizeOfComp = compositeShape.getCount();
  antsiferov::CompositeShape::array_ptr shapes(std::make_unique<antsiferov::CompositeShape::ptr[]>(sizeOfComp));
  //shapes = compositeShape.getList();
  antsiferov::Matrix<antsiferov::Shape> matrixSecond = antsiferov::split(compositeShape);

  BOOST_CHECK_EQUAL(matrixSecond.getRows(), 3);
  BOOST_CHECK_EQUAL(matrixSecond.getMaxColSize(), 2);
  BOOST_CHECK_EQUAL(matrixSecond[0][0], compositeShape[0]);
  BOOST_CHECK_EQUAL(matrixSecond[0][1], compositeShape[2]);
  BOOST_CHECK_EQUAL(matrixSecond[1][0], compositeShape[1]);
  BOOST_CHECK_EQUAL(matrixSecond[2][0], compositeShape[3]);

  antsiferov::CompositeShape::ptr circlePtr = std::make_shared<antsiferov::Circle>(antsiferov::Circle({10, 10}, 2));
  size_t placeRow = 0;
  matrixSecond.add(placeRow, circlePtr);
  BOOST_CHECK_EQUAL(matrixSecond[placeRow][2], circlePtr);
  BOOST_CHECK_EQUAL(matrixSecond.getRows(), 3);
  BOOST_CHECK_EQUAL(matrixSecond.getMaxColSize(), 3);

  antsiferov::CompositeShape::ptr rectanglePtr = std::make_shared<antsiferov::Rectangle>(antsiferov::Rectangle({10, 10}, 2, 2));
  placeRow = 3;
  matrixSecond.add(placeRow, rectanglePtr);
  BOOST_CHECK_EQUAL(matrixSecond[placeRow][0], rectanglePtr);
  BOOST_CHECK_EQUAL(matrixSecond.getRows(), 4);
  BOOST_CHECK_EQUAL(matrixSecond.getMaxColSize(), 3);
}

BOOST_AUTO_TEST_CASE(intersectTest)
{
  antsiferov::Rectangle rectangle({5, 5}, 6, 4);
  antsiferov::Circle circle1({3, 2}, 2);
  antsiferov::Circle circle2({-4, -3}, 2);

  BOOST_CHECK(antsiferov::intersect(rectangle, circle1));
  BOOST_CHECK(!antsiferov::intersect(rectangle, circle2));
}

BOOST_AUTO_TEST_SUITE_END()
