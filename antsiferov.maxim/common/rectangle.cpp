#include "rectangle.hpp"
#include <iostream>
#include <cmath>

antsiferov::Rectangle::Rectangle(const point_t &centre, double width, double height) :
  pos_(centre),
  width_(width),
  height_(height),
  angle_(0)
{
  if ((height_ < 0.0) || (width_ < 0.0))
  {
    throw std::invalid_argument("height / width can not be < 0");
  }
}

double antsiferov::Rectangle::getArea() const
{
  return (width_ * height_);
}

antsiferov::rectangle_t antsiferov::Rectangle::getFrameRect() const
{
  const double sin = std::sin(angle_ * M_PI / 180);
  const double cos = std::cos(angle_ * M_PI / 180);
  const double width = std::abs(width_ * cos) + std::abs(height_ * sin);
  const double height = std::abs(width_ * sin) + std::abs(height_ * cos);
  return {width, height, pos_};
}

void antsiferov::Rectangle::move(const point_t &position_)
{
  pos_ = position_;
}

void antsiferov::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void antsiferov::Rectangle::printShapeInfo() const
{
  rectangle_t framingRectangle = getFrameRect();
  std::cout << "\nRectangle frame width is " << framingRectangle.width;
  std::cout << "\nRectangle frame height is " << framingRectangle.height;
  std::cout << "\nRectangle centre is (" << pos_.x << ";" << pos_.y << ")";
  std::cout << "\nRectangle area is " << getArea() << "\n\n";
}

void antsiferov::Rectangle::scale(double coef)
{
  if (coef <= 0)
  {
    throw std::invalid_argument("Invalid scale coefficient.");
  }
  width_ *= coef;
  height_ *= coef;
}

void antsiferov::Rectangle::rotate(double angle)
{
  angle_ += angle;
  angle_ = fmod(angle_, FULL_ANGLE);
}
