#include "composite-shape.hpp"

#include <utility>
#include <iostream>
#include <algorithm>
#include <stdexcept>
#include <cmath>

antsiferov::CompositeShape::CompositeShape() :
  m_count(0),
  m_figures()
{ }

antsiferov::CompositeShape::CompositeShape(const ptr& shape) :
  m_count(1),
  m_figures(std::make_unique<ptr[]>(1))
{
  if (shape == nullptr) {
    throw std::invalid_argument("Shape pointer is empty");
  }

  m_figures[0] = shape;
}

antsiferov::CompositeShape::CompositeShape(const CompositeShape & rhs) :
  m_count(rhs.m_count),
  m_figures(std::make_unique<ptr[]>(rhs.m_count))
{
  for (size_t i = 0; i < m_count; i++)
  {
    m_figures[i] = rhs.m_figures[i];
  }
}

antsiferov::CompositeShape::CompositeShape(CompositeShape && rhs) :
  m_count(rhs.m_count),
  m_figures(std::move(rhs.m_figures))
{ 
  rhs.m_count = 0;
  rhs.m_figures = nullptr;
}

antsiferov::CompositeShape & antsiferov::CompositeShape::operator=(const CompositeShape & rhs)
{
  if (this != & rhs) {
    m_count = rhs.m_count;
    array_ptr m_array(std::make_unique<ptr[]>(rhs.m_count));
    for (size_t i = 0; i < m_count; i++)
    {
      m_array[i] = rhs.m_figures[i];
    }

    m_figures.swap(m_array);
  }

  return *this;
}

antsiferov::CompositeShape & antsiferov::CompositeShape::operator=(CompositeShape && rhs)
{
  if (this != &rhs) {
    m_count = rhs.m_count;
    m_figures = std::move(rhs.m_figures);
  }
  rhs.m_count = 0;
  rhs.m_figures = nullptr;
  return *this;
}

antsiferov::CompositeShape::ptr antsiferov::CompositeShape::operator [](size_t index) const
{
  if (index >= m_count) {
    throw std::invalid_argument("Index is out of array");
  }

  return (m_figures[index]);
}

size_t antsiferov::CompositeShape::getCount() const
{
  return m_count;
}

double antsiferov::CompositeShape::getArea() const
{
  double totalArea = 0;
  for (size_t i = 0; i < m_count; i++)
  {
    totalArea += m_figures[i]->getArea();
  }

  return totalArea;
}

antsiferov::rectangle_t antsiferov::CompositeShape::getFrameRect() const
{
  double maxX = 0;
  double maxY = 0;
  double minX = 0;
  double minY = 0;

  if (m_figures != nullptr) {
    rectangle_t rectangle = m_figures[0]->getFrameRect();
    maxX = rectangle.pos.x + rectangle.width / 2;
    minX = rectangle.pos.x - rectangle.width / 2;
    maxY = rectangle.pos.y + rectangle.height / 2;
    minY = rectangle.pos.y - rectangle.height / 2;

    for (size_t i = 1; i < m_count; i++)
    {
      rectangle = m_figures[i]->getFrameRect();

      point_t position = rectangle.pos;
      maxX = std::max(position.x + rectangle.width / 2, maxX);
      minX = std::min(position.x - rectangle.width / 2, minX);
      maxY = std::max(position.y + rectangle.height / 2, maxY);
      minY = std::min(position.y - rectangle.height / 2, minY);
    }
  }

  return {maxX - minX, maxY - minY, {(maxX + minX) / 2, (maxY + minY) / 2}};
}

void antsiferov::CompositeShape::add(const ptr& shape)
{
  if (shape == nullptr) {
    throw std::invalid_argument("Shape is empty");
  }

  array_ptr m_array(std::make_unique<ptr[]>(m_count + 1));

  for (size_t i = 0; i < m_count; i++)
  {
    m_array[i] = m_figures[i];
  }

  m_array[m_count] = shape;
  m_count++;
  m_figures.swap(m_array);
}

void antsiferov::CompositeShape::scale(double scaleFactor)
{
  if (scaleFactor <= 0.0) {
    throw std::invalid_argument("Scale factor must be greater than zero.");
  }

  point_t position = getFrameRect().pos;

  for (size_t i = 0; i < m_count; i++)
  {
    m_figures[i]->move({position.x + (m_figures[i]->getFrameRect().pos.x - position.x) * scaleFactor,
        position.y + (m_figures[i]->getFrameRect().pos.y - position.y) * scaleFactor});
    m_figures[i]->scale(scaleFactor);
  }
}

void antsiferov::CompositeShape::move(const point_t & pos)
{
  double dx = pos.x - getFrameRect().pos.x;
  double dy = pos.y - getFrameRect().pos.y;

  move(dx, dy);
}

void antsiferov::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < m_count; i++)
  {
    m_figures[i]->move(dx, dy);
  }
}

void antsiferov::CompositeShape::remove(size_t index)
{
  if (index >= m_count) {
    throw std::invalid_argument("Index is out of array");
  }

  if (m_figures == nullptr) {
    throw std::invalid_argument("The Composite shape is empty");
  }

  for (size_t i = index; i < m_count - 1; i++)
  {
    m_figures[i] = m_figures[i + 1];
  }
  m_count--;
}

antsiferov::point_t antsiferov::CompositeShape::getPos() const
{
  return getFrameRect().pos;
}

void antsiferov::CompositeShape::printShapeInfo() const
{
  std::cout << "Composite shape Parameters:\n"
      << "Center - {" << getFrameRect().pos.x << ", " << getFrameRect().pos.y <<"}\n"
      << "Count: " << m_count << '\n'
      << "Width - " << getFrameRect().width << '\n'
      << "Height - " << getFrameRect().height << '\n'
      << "Area - " << getArea() << "\n\n";
}

void antsiferov::CompositeShape::rotate(double angle)
{
  const point_t centre = getPos();
  for (size_t i = 0; i < m_count; ++i)
  {
    m_figures[i]->rotate(angle);
    point_t centreShape = m_figures[i]->getFrameRect().pos;
    antsiferov::rotatePoint(centre, centreShape, angle);
  }
}
