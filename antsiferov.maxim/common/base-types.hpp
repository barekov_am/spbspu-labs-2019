#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace antsiferov
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    double width;
    double height;
    point_t pos;
  };
  antsiferov::point_t rotatePoint(const antsiferov::point_t& centre, const antsiferov::point_t& point,
    double angle);
  const double FULL_ANGLE = 360.0;
  const double RADIX_GRAD = 57.296;
}

#endif
