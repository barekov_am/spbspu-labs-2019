#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testsForRectangle)

const double EPS = 0.01;

BOOST_AUTO_TEST_CASE(testOfImmutability)
{
  antsiferov::Rectangle testRect({ 1.0, 2.0 }, 3.0, 4.0);

  const double width = testRect.getFrameRect().width;
  const double height = testRect.getFrameRect().height;
  const double area = testRect.getArea();

  testRect.move(5.0, 6.0);
  BOOST_CHECK_CLOSE(testRect.getFrameRect().width, width, EPS);
  BOOST_CHECK_CLOSE(testRect.getFrameRect().height, height, EPS);
  BOOST_CHECK_CLOSE(testRect.getArea(), area, EPS);

  testRect.move({ 7.0, 8.0 });
  BOOST_CHECK_CLOSE(testRect.getFrameRect().width, width, EPS);
  BOOST_CHECK_CLOSE(testRect.getFrameRect().height, height, EPS);
  BOOST_CHECK_CLOSE(testRect.getArea(), area, EPS);
}
BOOST_AUTO_TEST_CASE(testOfCorrectScaling)
{
  antsiferov::Rectangle testRect({ 1.0, 2.0 }, 3.0, 4.0);

  const double area = testRect.getArea();
  const double scale = 5.0;
  testRect.scale(scale);
  BOOST_CHECK_CLOSE(testRect.getArea(), area * scale * scale, EPS);
}
BOOST_AUTO_TEST_CASE(testInvalidValues)
{
  BOOST_CHECK_THROW(antsiferov::Rectangle testRect({ 1.0, -2.0 }, -3.0, 0.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testScaleInvalidArgument)
{
  antsiferov::Rectangle rectangle({ 1.0, 2.0 }, 3.0, 4.0);
  BOOST_CHECK_THROW(rectangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END();
