#include "triangle.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>

antsiferov::Triangle::Triangle(const point_t &firstPt, const point_t &secondPt, const point_t &thirdPt) :
  pos_({ (firstPt.x + secondPt.x + thirdPt.x) / 3 , (firstPt.y + secondPt.y + thirdPt.y) / 3 }),
  firstPt_(firstPt),
  secondPt_(secondPt),
  thirdPt_(thirdPt)
{
  if (getArea() <= 0.0)
  {
    throw std::invalid_argument("area can not be <= 0.0");
  }
}

double antsiferov::Triangle::getArea() const
{
  return abs(((firstPt_.x - thirdPt_.x) * (secondPt_.y - thirdPt_.y) - (firstPt_.y 
      - thirdPt_.y) * (secondPt_.x - thirdPt_.x)) / 2);
}

antsiferov::rectangle_t antsiferov::Triangle::getFrameRect() const
{
  point_t min = { std::min(std::min(firstPt_.x, secondPt_.x), thirdPt_.x), 
      std::min(std::min(firstPt_.y, secondPt_.y), thirdPt_.y) };
  point_t max = { std::max(std::max(firstPt_.x, secondPt_.x), thirdPt_.x), 
      std::max(std::max(firstPt_.y, secondPt_.y), thirdPt_.y) };
  return rectangle_t{ max.x - min.x, max.y - min.y, { (max.x + min.x) / 2, (max.y + min.y) / 2 } };
}

void antsiferov::Triangle::move(const point_t &position_)
{
  const double dx = position_.x - pos_.x;
  const double dy = position_.y - pos_.y;

  move(dx, dy);
}

void antsiferov::Triangle::move(double dx, double dy)
{
  pos_.x += dx;
  firstPt_.x += dx;
  secondPt_.x += dx;
  thirdPt_.x += dx;
  pos_.y += dy;
  firstPt_.y += dy;
  secondPt_.y += dy;
  thirdPt_.y += dy;
}

void antsiferov::Triangle::printShapeInfo() const
{
  rectangle_t framingRectangle = getFrameRect();
  std::cout << "\nfirst party.x = " << firstPt_.x << " ; first party.y = " << firstPt_.y;
  std::cout << "\nsecond party.x = " << secondPt_.x << " ; second party.y = " << secondPt_.y;
  std::cout << "\nthird party.x = " << thirdPt_.x << " ; third party.y = " << thirdPt_.y;
  std::cout << "\nTriangle centre is (" << pos_.x << ";" << pos_.y << ")";
  std::cout << "\nTriangle frame width is " << framingRectangle.width;
  std::cout << "\nTriangle frame height is " << framingRectangle.height;
  std::cout << "\nTriangle area is " << getArea() << "\n\n";
}

void antsiferov::Triangle::scale(double coef)
{
  if (coef <= 0)
  {
    throw std::invalid_argument("Invalid scale coefficient.");
  }
  firstPt_.x = coef * firstPt_.x + pos_.x * (1 - coef);
  firstPt_.y = coef * firstPt_.y + pos_.y * (1 - coef);
  secondPt_.x = coef * secondPt_.x + pos_.x * (1 - coef);
  secondPt_.y = coef * secondPt_.y + pos_.y * (1 - coef);
  thirdPt_.x = coef * thirdPt_.x + pos_.x * (1 - coef);
  thirdPt_.y = coef * thirdPt_.y + pos_.y * (1 - coef);
}

void antsiferov::Triangle::rotate(double angle)
{
  const point_t centre = pos_;
  firstPt_ = antsiferov::rotatePoint(centre, firstPt_, angle);
  secondPt_ = antsiferov::rotatePoint(centre, secondPt_, angle);
  thirdPt_ = antsiferov::rotatePoint(centre, thirdPt_, angle);
}
