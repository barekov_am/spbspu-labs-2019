#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testsForCircle)

const double EPS = 0.01;

BOOST_AUTO_TEST_CASE(testOfImmutability)
{
  antsiferov::Circle testCircle({ 1.0, 2.0 }, 3.0);
  const double width = testCircle.getFrameRect().width;
  const double height = testCircle.getFrameRect().height;
  const double area = testCircle.getArea();

  testCircle.move(4.0, 5.0);
  BOOST_CHECK_CLOSE(testCircle.getFrameRect().width, width, EPS);
  BOOST_CHECK_CLOSE(testCircle.getFrameRect().height, height, EPS);
  BOOST_CHECK_CLOSE(testCircle.getArea(), area, EPS);

  testCircle.move({6.0, 7.0});
  BOOST_CHECK_CLOSE(testCircle.getFrameRect().width, width, EPS);
  BOOST_CHECK_CLOSE(testCircle.getFrameRect().height, height, EPS);
  BOOST_CHECK_CLOSE(testCircle.getArea(), area, EPS);
}

BOOST_AUTO_TEST_CASE(testOfCorrectScaling)
{
  antsiferov::Circle testCircle({ 1.0, 2.0 }, 3.0);

  const double area = testCircle.getArea();
  const double scale = 4.0;

  testCircle.scale(scale);
  BOOST_CHECK_CLOSE(testCircle.getArea(), area * scale * scale, EPS);
}

BOOST_AUTO_TEST_CASE(testInvalidValues)
{
  BOOST_CHECK_THROW(antsiferov::Circle testCircle({ -1.0, -2.0 }, -3.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testScaleInvalidArgument)
{
  antsiferov::Circle circle({ 1.0, 2.0 }, 3.0);
  BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END();
