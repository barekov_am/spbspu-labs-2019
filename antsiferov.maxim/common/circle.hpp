#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace antsiferov
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t &centre, double radius);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &position_) override;
    void move(double dx, double dy) override;
    void printShapeInfo() const override;
    void scale(double coef);
    void rotate(double) override;

  private:
    point_t pos_;
    double radius_;
  };
}

#endif
