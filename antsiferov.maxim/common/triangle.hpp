#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"
namespace antsiferov
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &firstPt, const point_t &secondPt, const point_t &thirdPt);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &centre) override;
    void move(double dx, double dy) override;
    void printShapeInfo() const override;
    void scale(double coef);
    void rotate(double angle) override;

  private:
    point_t pos_;
    point_t firstPt_;
    point_t secondPt_;
    point_t thirdPt_;
  };
}
#endif
