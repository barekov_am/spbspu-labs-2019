#ifndef COMPOSITE_SHAPE
#define COMPOSITE_SHAPE

#include <memory>

#include "shape.hpp"

namespace antsiferov
{
  class CompositeShape : public Shape
  {
  public:
    using ptr = std::shared_ptr<Shape>;
    using array_ptr = std::unique_ptr<ptr[]>;
    
    CompositeShape();
    CompositeShape(const CompositeShape & rhs);
    CompositeShape(CompositeShape && rhs);
    CompositeShape(const ptr& shape);

    ~CompositeShape() = default;

    CompositeShape & operator=(const CompositeShape & rhs);
    CompositeShape & operator=(CompositeShape && rhs);
    ptr operator [](size_t index) const;

    size_t getCount() const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;

    void add(const ptr& shape);

    void scale(double scaleFactor) override;

    void move(const point_t & pos) override;
    void move(double dx, double dy) override;

    void remove(size_t index);

    point_t getPos() const;
    void printShapeInfo() const override;
    void rotate(double angle);
  private:
    size_t m_count;
    array_ptr m_figures;
  };
}

#endif
