#ifndef MAIN_SPLITING_HPP
#define MAIN_SPLITING_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace antsiferov
{
  Matrix<Shape> split(const CompositeShape &compositeShape);
  Matrix<Shape> split(const antsiferov::CompositeShape::array_ptr &shapes, size_t size);
  bool intersect(const antsiferov::Shape &shape1, const antsiferov::Shape &shape2);
}
#endif //MAIN_SPLITING_HPP
