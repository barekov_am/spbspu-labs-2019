#include "spliting.hpp"
#include <cmath>

antsiferov::Matrix<antsiferov::Shape> antsiferov::split(const antsiferov::CompositeShape &compositeShape)
{
  antsiferov::Matrix<antsiferov::Shape> matrix;

  for (size_t i = 0; i < compositeShape.getCount(); i++)
  {
    size_t row = 0;
    size_t j = matrix.getRows();
    while (j-- > 0)
    {
      bool ifIntersect = false;
      for (size_t k = 0; k < matrix[j].getSize(); k++)
      {
        if (intersect(*compositeShape[i], *matrix[j][k]))
        {
          row = j + 1;
          ifIntersect = true;
          break;
        }
      }
      if (ifIntersect)
      {
        break;
      }
    }
    matrix.add(row, compositeShape[i]);
  }
  return matrix;
}

antsiferov::Matrix<antsiferov::Shape> antsiferov::split(const antsiferov::CompositeShape::array_ptr &shapes, size_t size)
{
  antsiferov::CompositeShape compositeShape;
  for (size_t i = 0; i < size; i++)
  {
    compositeShape.add(shapes[i]);
  }
  return split(compositeShape);
}

bool antsiferov::intersect(const antsiferov::Shape &shape1, const antsiferov::Shape &shape2)
{
  const antsiferov::rectangle_t frameFirst = shape1.getFrameRect();
  const antsiferov::rectangle_t frameSecond = shape2.getFrameRect();


  if (std::abs(frameFirst.pos.y - frameSecond.pos.y) > (frameFirst.height + frameSecond.height) / 2)
  {
    return false;
  }
  
  return (std::abs(frameFirst.pos.x - frameSecond.pos.x) <= (frameFirst.width + frameSecond.width) / 2);
}
