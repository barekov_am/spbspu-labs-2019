#define _USE_MATH_DEFINES
#include "circle.hpp"
#include <iostream>
#include <cmath>


antsiferov::Circle::Circle(const point_t &centre, double radius) :
  pos_(centre),
  radius_(radius)
{
  if (radius_ < 0.0)
  {
    throw std::invalid_argument("radius can not be < 0.0");
  }
}

double antsiferov::Circle::getArea() const
{
  return (radius_ * radius_ * M_PI);
}

antsiferov::rectangle_t antsiferov::Circle::getFrameRect() const
{
  return rectangle_t{ radius_, radius_, pos_ };
}

void antsiferov::Circle::move(const point_t &position_)
{
  pos_ = position_;
}

void antsiferov::Circle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void antsiferov::Circle::printShapeInfo() const
{
  rectangle_t framingRectangle = getFrameRect();
  std::cout << "\nCircle frame width is " << framingRectangle.width;
  std::cout << "\nCircle frame height is " << framingRectangle.height;
  std::cout << "\nCircle centre is (" << pos_.x << ";" << pos_.y << ")";
  std::cout << "\nCircle area is " << getArea() << "\n\n";
}

void antsiferov::Circle::scale(double coef)
{
  if (coef <= 0)
  {
    throw std::invalid_argument("Invalid scale coefficient.");
  }
  radius_ *= coef;
}

void antsiferov::Circle::rotate(double)
{ }
