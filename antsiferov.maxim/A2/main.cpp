#include <iostream>

#include "base-types.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

int main()
{
  std::cout << "        RECTANGLE\n";
  antsiferov::Rectangle rectangle({ { 1, 1 }, 2, 3 });
  antsiferov::Shape *shapePtr = &rectangle;
  shapePtr->printShapeInfo();

  std::cout << "Move rectangle by dx and dy\n";
  shapePtr->move(5, 5);
  shapePtr->printShapeInfo();

  std::cout << "Move rectangle by coordinate\n";
  shapePtr->move({ 2, 3 });
  shapePtr->printShapeInfo();

   std::cout << "Scale rectangle by 2\n";
  shapePtr->scale(2);
  shapePtr->printShapeInfo();

  std::cout << "        CIRCLE\n";
  antsiferov::Circle circle({ 1, 1 }, 6);
  shapePtr = &circle;
  shapePtr->printShapeInfo();

  std::cout << "Move circle by dx and dy\n";
  shapePtr->move(5, 5);
  shapePtr->printShapeInfo();

  std::cout << "Move circle by coordinate\n";
  shapePtr->move({ 2, 3 });
  shapePtr->printShapeInfo();

  std::cout << "Scale circle by 2\n";
  shapePtr->scale(2);
  shapePtr->printShapeInfo();

  std::cout << "        TRIANGLE\n";
  antsiferov::Triangle triangle({ 0, 0 }, { 9, 9 }, { 18, 0 });
  shapePtr = &triangle;
  shapePtr->printShapeInfo();

  std::cout << "Move triangle by dx and dy\n";
  shapePtr->move(5, 5);
  shapePtr->printShapeInfo();

  std::cout << "Move triangle by coordinate\n";
  shapePtr->move({ 2, 3 });
  shapePtr->printShapeInfo();

  std::cout << "Scale triangle by 2\n";
  shapePtr->scale(2);
  shapePtr->printShapeInfo();
  
  return 0;
}
