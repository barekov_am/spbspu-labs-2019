#include "triangle.hpp"
#include <iostream>
#include <cassert>
#include <algorithm>
#include <cmath>

Triangle::Triangle(const point_t &firstPt, const point_t &secondPt, const point_t &thirdPt) :
  pos_({ (firstPt.x + secondPt.x + thirdPt.x) / 3 , (firstPt.y + secondPt.y + thirdPt.y) / 3 }),
  firstPt_(firstPt),
  secondPt_(secondPt),
  thirdPt_(thirdPt)
{
  assert(getArea() > 0);
}

double Triangle::getArea() const
{
  return ((std::abs(((firstPt_.x - thirdPt_.x) * (secondPt_.y - thirdPt_.y)) - ((firstPt_.y 
      - thirdPt_.y) * (secondPt_.x - thirdPt_.x))) / 2));
}

rectangle_t Triangle::getFrameRect() const
{
  point_t min = { std::min(std::min(firstPt_.x, secondPt_.x), thirdPt_.x), 
      std::min(std::min(firstPt_.y, secondPt_.y), thirdPt_.y) };
  point_t max = { std::max(std::max(firstPt_.x, secondPt_.x), thirdPt_.x), 
      std::max(std::max(firstPt_.y, secondPt_.y), thirdPt_.y) };
  return rectangle_t{ max.x - min.x, max.y - min.y, { (max.x + min.x) / 2, (max.y + min.y) / 2 } };
}

void Triangle::move(const point_t &position_)
{
  const double dx = position_.x - pos_.x;
  const double dy = position_.y - pos_.y;

  move(dx, dy);
}

void Triangle::move(double dx, double dy)
{
  pos_.x += dx;
  firstPt_.x += dx;
  secondPt_.x += dx;
  thirdPt_.x += dx;
  pos_.y += dy;
  firstPt_.y += dy;
  secondPt_.y += dy;
  thirdPt_.y += dy;
}

void Triangle::printShapeInfo() const
{
  rectangle_t framingRectangle = getFrameRect();
  std::cout << "\nfirst party.x = " << firstPt_.x << " ; first party.y = " << firstPt_.y;
  std::cout << "\nsecond party.x = " << secondPt_.x << " ; second party.y = " << secondPt_.y;
  std::cout << "\nthird party.x = " << thirdPt_.x << " ; third party.y = " << thirdPt_.y;
  std::cout << "\nTriangle centre is (" << pos_.x << ";" << pos_.y << ")";
  std::cout << "\nTriangle frame width is " << framingRectangle.width;
  std::cout << "\nTriangle frame height is " << framingRectangle.height;
  std::cout << "\nTriangle area is " << getArea() << "\n\n";
}
