#define _USE_MATH_DEFINES
#include "circle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>


Circle::Circle(const point_t &centre, double radius) :
  pos_(centre),
  radius_(radius)
{
  assert(radius >= 0.0);
}

double Circle::getArea() const
{
  return (radius_ * radius_ * M_PI);
}

rectangle_t Circle::getFrameRect() const
{
  return rectangle_t{ radius_, radius_, pos_ };
}

void Circle::move(const point_t &position_)
{
  pos_ = position_;
}

void Circle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void Circle::printShapeInfo() const
{
  rectangle_t framingRectangle = getFrameRect();
  std::cout << "\nCircle frame width is " << framingRectangle.width;
  std::cout << "\nCircle frame height is " << framingRectangle.height;
  std::cout << "\nCircle centre is (" << pos_.x << ";" << pos_.y << ")";
  std::cout << "\nCircle area is " << getArea() << "\n\n";
}
