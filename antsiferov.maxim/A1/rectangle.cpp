#include "rectangle.hpp"
#include <iostream>
#include <cassert>

Rectangle::Rectangle(const point_t &centre, double width, double height) :
  pos_(centre),
  width_(width),
  height_(height)
{
  assert((width_ >= 0.0) && (height_ >= 0.0));
}

double Rectangle::getArea() const
{
  return (width_ * height_);
}

rectangle_t Rectangle::getFrameRect() const
{
  return rectangle_t{ width_, height_, pos_ };
}

void Rectangle::move(const point_t &position_)
{
  pos_ = position_;
}

void Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void Rectangle::printShapeInfo() const
{
  rectangle_t framingRectangle = getFrameRect();
  std::cout << "\nRectangle frame width is " << framingRectangle.width;
  std::cout << "\nRectangle frame height is " << framingRectangle.height;
  std::cout << "\nRectangle centre is (" << pos_.x << ";" << pos_.y << ")";
  std::cout << "\nRectangle area is " << getArea() << "\n\n";
}
