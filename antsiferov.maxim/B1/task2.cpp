#include <fstream>
#include <iterator>
#include <iostream>
#include <memory>
#include <vector>

void task2(const std::string& filePath)
{
  std::ifstream fin{ filePath };
  if (!fin.is_open()) {
    throw std::runtime_error{ "could not open \"" + filePath + "\"" };
  }

  size_t contentSize = 0, contentCapacity = 16;
  std::unique_ptr<char[], decltype(&free)> fileContent{ static_cast<char*>(malloc(contentCapacity)), free };

  while (fin) {
    fin.read(&fileContent[contentSize], contentCapacity - contentSize);
    contentSize += fin.gcount();
    if (contentSize == contentCapacity) {
      contentCapacity *= 2;
      auto old = fileContent.release();
      fileContent.reset(static_cast<char*>(std::realloc(old, contentCapacity)));
      if (!fileContent) {
        throw std::runtime_error{ "could not allocate memory for file content" };
      }
    }
  }

  fin.close();
  if (fin.is_open()) {
    throw std::runtime_error{ "could not close \"" + filePath + "\"" };
  }
  std::vector<char> content{ &fileContent[0], &fileContent[contentSize] };
  std::copy(content.begin(), content.end(), std::ostream_iterator<char>{ std::cout });
}
