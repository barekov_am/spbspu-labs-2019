#include <forward_list>
#include <iostream>
#include <iterator>
#include <vector>

#include "tasks.hpp"
#include "sort.hpp"
#include "access.hpp"

void task1(const std::string& ordering)
{
  std::function<bool(const int&, const int&)> comparator = getComparator<int>(ordering);
  std::vector<int> seq{};
  int curr;
  while (std::cin >> curr, !std::cin.eof()) {
    if (std::cin.fail()) {
      throw std::runtime_error{ "In task #1 Integer sequence required." };
    }
    seq.push_back(curr);
  }
  if (seq.empty()) {
    return;
  }
  std::vector<int> idCollection{ seq.begin(), seq.end() };
  std::vector<int> atCollection{ seq.begin(), seq.end() };
  std::forward_list<int> itCollection{ seq.begin(), seq.end() };

  sort<IndexAccess>(idCollection, comparator);
  sort<AtAccess>(atCollection, comparator);
  sort<IteratorAccess>(itCollection, comparator);

  std::move(idCollection.begin(), idCollection.end(), std::ostream_iterator<int>{ std::cout, " " }); std::cout << "\n";
  std::move(atCollection.begin(), atCollection.end(), std::ostream_iterator<int>{ std::cout, " " }); std::cout << "\n";
  std::move(itCollection.begin(), itCollection.end(), std::ostream_iterator<int>{ std::cout, " " });
}
