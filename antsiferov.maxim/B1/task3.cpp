#include <iostream>
#include <vector>
#include <iterator>

void task3()
{
  std::vector<int> seq{};
  int curr = 0;
  while (std::cin >> curr, !std::cin.eof()) {
    if (std::cin.fail()) {
      throw std::runtime_error{ "In task #3 Integer sequence required." };
    }
    if (curr == 0) {
      break;
    }
    seq.push_back(curr);
  }
  if (curr != 0) {
    throw std::runtime_error{ "In task #3 terminating '0' expected." };
  }
  if (seq.empty()) {
    return;
  }
  if (seq.back() == 1) {
    auto iter = seq.begin();
    while (iter != seq.end()) {
      if (*iter % 2 == 0) {
        iter = seq.erase(iter);
      }
      else {
        ++iter;
      }
    }
  }
  else if (seq.back() == 2) {
    auto iter = seq.begin();
    while (iter != seq.end()) {
      if (*iter % 3 == 0) {
        iter = seq.insert(iter + 1, 3, 1) + 3;
      }
      else {
        ++iter;
      }
    }
  }
  std::copy(seq.begin(), seq.end(), std::ostream_iterator<int>{ std::cout, " " });
}
