#include <iostream>
#include <iterator>
#include <random>
#include <chrono>
#include <vector>
#include <string>

#include "tasks.hpp"
#include "sort.hpp"
#include "access.hpp"

void fillRandom(double* arr, size_t size)
{
  std::uniform_real_distribution<double> distr{ -1, 1 };
  std::default_random_engine engine{ static_cast<unsigned>(std::chrono::system_clock::now().time_since_epoch().count()) };
  for (size_t i = 0; i < size; i++) {
    arr[i] = distr(engine);
  }
}

void task4(const std::string& ordering, const std::string& sizeStr)
{
  std::function<bool(const double&, const double&)> comparator = getComparator<double>(ordering);
  int size = std::stoi(sizeStr);
  if (size <= 0) {
    throw std::invalid_argument{ "vector size must be positive" };
  }
  std::vector<double> randSeq(size);
  fillRandom(randSeq.data(), size);
  std::copy(randSeq.begin(), randSeq.end(), std::ostream_iterator<double>{ std::cout, " " }); std::cout << "\n";
  sort<IteratorAccess>(randSeq, comparator);
  std::move(randSeq.begin(), randSeq.end(), std::ostream_iterator<double>{ std::cout, " " }); std::cout << "\n";
}
