#ifndef TASKS_HPP
#define TASKS_HPP

#include "sort.hpp"
#include "access.hpp"


void task1(const std::string&);
void task2(const std::string&);
void task3();
void task4(const std::string&, const std::string&);

template <typename T> 
std::function<bool(const  T&, const T&)> getComparator(const std::string &order)
{
  std::function<bool(const T&, const T&)> comparator;
  if (order == "ascending")
  {
    comparator = std::less<T>();
  }
  else if (order == "descending")
  {
    comparator = std::greater<T>();
  }
  else
  {
    throw std::invalid_argument{ "usage: > lab1 1 (ascending|descending)" };
  }
  return comparator;
}

#endif // TASKS_HPP

