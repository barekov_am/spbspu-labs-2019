#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "spliting.hpp"

void printInfoAboutShape(const antsiferov::Shape &shape)
{
  const double area = shape.getArea();
  const antsiferov::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "Shape area is " << area << '\n';
  std::cout << "Shape frame rectangle width and height:" << '\n';
  std::cout << "Width = " << frameRect.width << '\n';
  std::cout << "Height = " << frameRect.height << '\n';
  std::cout << "Position of rectangle: x = " << frameRect.pos.x << ", y = " << frameRect.pos.y << "\n\n";
}

void printInfoAboutMatrix(const antsiferov::Matrix<antsiferov::Shape> &matrix)
{
  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    std::cout << "Row " << i + 1 << ":" << '\n';
    size_t shapesInLayer = matrix[i].getSize();
    std::cout << "Shapes in layer: " << shapesInLayer << '\n';
    for (size_t j = 0; j < shapesInLayer; j++)
    {
      std::cout << "Shape " << j + 1 << ":" << '\n';
      printInfoAboutShape(*matrix[i][j]);
    }
  }
}

int main()
{
  antsiferov::Circle circle({15.5, 5.3}, 3.4);
  printInfoAboutShape(circle);
  circle.move({3.5, 14.4});
  printInfoAboutShape(circle);
  circle.move(5.2, 4.3);
  printInfoAboutShape(circle);
  const double scaleRatioForCircle = 2.5;
  circle.scale(scaleRatioForCircle);
  printInfoAboutShape(circle);
  circle.rotate(45);
  printInfoAboutShape(circle);

  antsiferov::Rectangle rectangle({14.3, 15.2}, 5.1, 4.3);
  printInfoAboutShape(rectangle);
  rectangle.move({5.4, 3.3});
  printInfoAboutShape(rectangle);
  rectangle.move(3.6, 7.7);
  printInfoAboutShape(rectangle);
  const double scaleRatioForRectangle = 4;
  rectangle.scale(scaleRatioForRectangle);
  printInfoAboutShape(rectangle);
  rectangle.rotate(60);
  printInfoAboutShape(rectangle);

  antsiferov::Rectangle rectangleForComp({4, 5}, 2, 5);
  antsiferov::Circle circleForComp({0, 0}, 2);

  antsiferov::CompositeShape firstComposite;
  firstComposite.add(std::make_shared<antsiferov::Rectangle>(rectangleForComp));
  printInfoAboutShape(firstComposite);
  firstComposite.add(std::make_shared<antsiferov::Circle>(circleForComp));
  printInfoAboutShape(firstComposite);
  const double scaleRatio = 3;
  firstComposite.scale(scaleRatio);
  printInfoAboutShape(firstComposite);
  firstComposite.rotate(30);
  printInfoAboutShape(firstComposite);

  std::cout << "Count of shapes in first composite shape = " << firstComposite.getCount() << '\n';

  firstComposite.remove(1);
  printInfoAboutShape(firstComposite);

  firstComposite.add(std::make_shared<antsiferov::Circle>(circleForComp));
  printInfoAboutShape(firstComposite);

  firstComposite.move({1.8, 4.5});
  printInfoAboutShape(firstComposite);

  antsiferov::CompositeShape secondComposite(firstComposite);
  printInfoAboutShape(secondComposite);

  antsiferov::CompositeShape thirdComposite = firstComposite;
  printInfoAboutShape(thirdComposite);

  antsiferov::Rectangle rectangleInComp({4, 5}, 2, 3);
  antsiferov::Circle circleInComp({0, 0}, 2);
  antsiferov::CompositeShape compositeShape;
  compositeShape.add(std::make_shared<antsiferov::Rectangle>(rectangleInComp));
  compositeShape.add(std::make_shared<antsiferov::Circle>(circleInComp));
  printInfoAboutShape(*compositeShape[0]);

  antsiferov::CompositeShape compForSplit;
  compForSplit.add(std::make_shared<antsiferov::Rectangle>(antsiferov::Rectangle({0, 0}, 1, 5)));
  compForSplit.add(std::make_shared<antsiferov::Rectangle>(antsiferov::Rectangle({1, 2}, 4, 3)));
  compForSplit.add(std::make_shared<antsiferov::Circle>(antsiferov::Circle({12, 12}, 1)));       
  compForSplit.add(std::make_shared<antsiferov::CompositeShape>(thirdComposite));               

  
  antsiferov::Matrix<antsiferov::Shape> matrix;
  matrix = antsiferov::split(compForSplit);
  printInfoAboutMatrix(matrix);
  matrix.add(0, std::make_shared<antsiferov::Circle>(circleInComp));
  printInfoAboutMatrix(matrix);
  return 0;
}
