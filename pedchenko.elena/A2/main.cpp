#include <iostream>

#include "shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"


void showInfo(const pedchenko::Shape &shape)
{
  shape.writeInfo();
}


int main()
{
  pedchenko::point_t pointCenter = {1.0, 1.0};
  pedchenko::Circle circle(pointCenter, 10.0);
  pedchenko::Rectangle rectangle(pointCenter, 20.0, 30.0);
  double scaleFactor = 4.0;
  showInfo(circle);
  showInfo(rectangle);

  std::cout << "For Circle. From center (1.0, 1.0) move (2.0,2.0) = center(3.0,3.0)" << std::endl;
  circle.move(2.0, 2.0);
  std::cout << "Radius (10.0) of Circle increases in " << scaleFactor << " = radius (30.0)" << std::endl;
  circle.scale(scaleFactor);
  showInfo(circle);
  std::cout << "For Rectangle. From center(1.0, 1.0) move (4.0,4.0) = center(5.0,5.0)" << std::endl;
  rectangle.move(4.0, 4.0);
  std::cout << "Width(20.0) and height(30.0) of Rectangle increase in " << scaleFactor << "= width(60.0) and height(90.0)" << std::endl;
  rectangle.scale(scaleFactor);
  showInfo(rectangle);

  scaleFactor = 1 / scaleFactor;
  std::cout << "For Rectangle and Circle -> new center (2.0,2.0)" << std::endl;
  pointCenter = {2.0, 2.0};
  circle.move(pointCenter);
  rectangle.move(pointCenter);
  std::cout << "Radius (30.0) of Circle decreases in " << scaleFactor << " = radius (10.0)" << std::endl;
  circle.scale(scaleFactor);
  std::cout << "Width(60.0) and height(90.0) of Rectangle decrease in " << scaleFactor << "= width(20.0) and height(30.0)" << std::endl;
  rectangle.scale(scaleFactor);
  showInfo(circle);
  showInfo(rectangle);

  return 0;
}
