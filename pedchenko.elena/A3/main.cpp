#include <iostream>

#include "../common/shape.hpp"
#include "../common/circle.hpp"
#include "../common/rectangle.hpp"
#include "../common/composite-shape.hpp"

void showInfo(const pedchenko::Shape &shape)
{
  shape.writeInfo();
}

void printCenter(const pedchenko::Shape &shape)
{
  std::cout << "Center: ( " << shape.getFrameRect().pos.x << " , " << shape.getFrameRect().pos.y << " )\n";
}

int main()
{
  pedchenko::point_t pointCenter = {1.0, 1.0};
  pedchenko::Circle circle(pointCenter, 10.0);
  pedchenko::Rectangle rectangle(pointCenter, 20.0, 30.0);
  double scaleFactor = 3.0;
  showInfo(circle);
  showInfo(rectangle);

  std::cout << "Circle: Center(1.0, 1.0) move (2.0,2.0) -> center(3.0,3.0)" << std::endl;
  circle.move(2.0, 2.0);
  std::cout << "Circle radius(10.0) do increase in " << scaleFactor << "-> radius (30.0)" << std::endl;
  circle.scale(scaleFactor);
  showInfo(circle);
  std::cout << "Rectangle: Center(1.0, 1.0), move (4.0,4.0) -> center(5.0,5.0)" << std::endl;
  rectangle.move(4.0, 4.0);
  std::cout << "Rectangle width(20.0) and height(30.0) do increase in " << scaleFactor << "-> width(60.0) and height(90.0)" << std::endl;
  rectangle.scale(scaleFactor);
  showInfo(rectangle);

  scaleFactor = 1 / scaleFactor;
  std::cout << "Rectangle and Circle -> new center(2.0,2.0)" << std::endl;
  pointCenter = {2.0, 2.0};
  circle.move(pointCenter);
  rectangle.move(pointCenter);
  std::cout << "Circle radius(30.0) do increase in " << scaleFactor << "-> radius (10.0)" << std::endl;
  circle.scale(scaleFactor);
  std::cout << "Rectangle width(60.0) and height(90.0) do increase in " << scaleFactor << "-> width(20.0) and height(30.0)" << std::endl;
  rectangle.scale(scaleFactor);
  showInfo(circle);
  showInfo(rectangle);

  scaleFactor = 2;
  pedchenko::CompositeShape compositeShape;
  std::cout << "Create CompositeShape, initial information:\n " << std::endl;
  showInfo(compositeShape);

  std::cout << "Adding shapes to CompositeShape:\n " << std::endl;
  compositeShape.add(rectangle);
  compositeShape.add(circle);
  showInfo(compositeShape);

  std::cout << "Move CompositeShape on (4.0, 4.0)\n " << std::endl;
  compositeShape.move(4.0, 4.0);
  printCenter(compositeShape);

  std::cout << "\nMove CompositeShape in (8.0, 8.0)\n " << std::endl;
  compositeShape.move({8.0, 8.0});
  printCenter(compositeShape);

  std::cout << "\nScale CompositeShape do increase in: " << scaleFactor << std::endl;
  compositeShape.scale(scaleFactor);
  showInfo(compositeShape);

  std::cout << "Deleting a figure by index in to CompositeShape:\n " << std::endl;
  compositeShape.remove(0);
  showInfo(compositeShape);

  std::cout << "Adding CompositeShape to CompositeShape:\n " << std::endl;
  pedchenko::CompositeShape compositeShapeIn(rectangle);
  compositeShape.add(compositeShapeIn);
  showInfo(compositeShape);

  return 0;
}
