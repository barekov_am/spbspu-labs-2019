#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "circle.hpp"

#define ACCURACY 0.001

BOOST_AUTO_TEST_SUITE(TestCircle)
  struct FixtureCreationCircle
  {
    FixtureCreationCircle() :
      circle({1.0, 1.0}, 10.0)
    {}
    pedchenko::Circle circle;
  };

BOOST_FIXTURE_TEST_CASE(widthHeightAreaOfTheCircleShouldBePreservedAfterMovingOnDxDy, FixtureCreationCircle)
{
  const pedchenko::rectangle_t initialCircle = circle.getFrameRect();
  const double initialAreaCircle= circle.getArea();

  circle.move(6.0,6.0);

  BOOST_CHECK_CLOSE(initialCircle.width, circle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialCircle.height, circle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialAreaCircle, circle.getArea(), ACCURACY);
}

BOOST_FIXTURE_TEST_CASE(widthHeightAreaOfTheCircleShouldBePreservedAfterMovingInNewCenter, FixtureCreationCircle)
{
  const pedchenko::rectangle_t initialCircle = circle.getFrameRect();
  const double initialAreaCircle= circle.getArea();

  circle.move({7.0,7.0});

  BOOST_CHECK_CLOSE(initialCircle.width, circle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialCircle.height, circle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialAreaCircle, circle.getArea(), ACCURACY);
}

BOOST_FIXTURE_TEST_CASE(areaOfCircleShouldChangeQuadraticallyAfterScaling,FixtureCreationCircle)
{
  const double initialAreaCircle = circle.getArea();
  const double scalingFactor = 2.0;

  circle.scale(scalingFactor);

  BOOST_CHECK_CLOSE(initialAreaCircle * scalingFactor * scalingFactor, circle.getArea(), ACCURACY);
}

BOOST_FIXTURE_TEST_CASE(shouldReturnAnErrorDueToIncorrectParametersFromCircle, FixtureCreationCircle)
{
  BOOST_CHECK_THROW(pedchenko::Circle circle1({1.0, 1.0}, -1.0), std::invalid_argument);
  BOOST_CHECK_THROW(pedchenko::Circle circle1({1.0, 1.0}, 0), std::invalid_argument);

  BOOST_CHECK_THROW(circle.scale(-2), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()
