#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>

#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

#define ACCURACY 0.001

BOOST_AUTO_TEST_SUITE(TestComposite)

BOOST_AUTO_TEST_CASE(testWorkingConstructor)
{
  pedchenko::Rectangle rectangle({7.0, 7.0}, 10.0, 6.0);
  pedchenko::CompositeShape compositeShape(rectangle);

  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().width, rectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().height, rectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, rectangle.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, rectangle.getFrameRect().pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testWorkingConstructorCopy)
{
  pedchenko::Rectangle rectangle({5.0,5.0}, 6.0, 7.0);
  pedchenko::CompositeShape compositeShape(rectangle);
  pedchenko::CompositeShape compositeShapeCopy(compositeShape);

  BOOST_CHECK(&compositeShape != &compositeShapeCopy);
  BOOST_CHECK(compositeShape.getSize() == compositeShapeCopy.getSize());

  BOOST_CHECK(&compositeShape[0] == &compositeShapeCopy[0]);
}

BOOST_AUTO_TEST_CASE(testWorkingOperatorCopy)
{
  pedchenko::Rectangle rectangle({5.0,5.0}, 6.0, 7.0);
  pedchenko::CompositeShape compositeShape(rectangle);
  pedchenko::CompositeShape compositeShapeCopy;
  compositeShapeCopy = compositeShape;

  BOOST_CHECK(&compositeShape != &compositeShapeCopy);
  BOOST_CHECK(compositeShape.getSize() == compositeShapeCopy.getSize());

  BOOST_CHECK(&compositeShape[0] == &compositeShapeCopy[0]);
}

BOOST_AUTO_TEST_CASE(testWorkingConstructorMoving)
{
  pedchenko::Rectangle rectangle({5.0,5.0}, 6.0, 7.0);
  pedchenko::CompositeShape compositeShape(rectangle);
  pedchenko::CompositeShape compositeShapeCopy(compositeShape);
  pedchenko::CompositeShape compositeShapeMoving(std::move(compositeShape));

  BOOST_CHECK(&compositeShapeMoving != &compositeShapeCopy);
  BOOST_CHECK(compositeShapeMoving.getSize() == compositeShapeCopy.getSize());
  BOOST_CHECK(&compositeShapeMoving[0] == &compositeShapeCopy[0]);

  BOOST_CHECK(&compositeShape != &compositeShapeMoving);
  BOOST_CHECK(compositeShape.getSize() != compositeShapeMoving.getSize());

  BOOST_CHECK(compositeShape.getSize() == 0);
  BOOST_CHECK_THROW(compositeShape[0].getFrameRect(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(testWorkingOperatorMoving)
{
  pedchenko::Rectangle rectangle({5.0,5.0}, 6.0, 7.0);
  pedchenko::CompositeShape compositeShape(rectangle);
  pedchenko::CompositeShape compositeShapeCopy(compositeShape);
  pedchenko::CompositeShape compositeShapeMoving;
  compositeShapeMoving = std::move(compositeShape);

  BOOST_CHECK(&compositeShapeMoving != &compositeShapeCopy);
  BOOST_CHECK(compositeShapeMoving.getSize() == compositeShapeCopy.getSize());
  BOOST_CHECK(&compositeShapeMoving[0] == &compositeShapeCopy[0]);

  BOOST_CHECK(&compositeShape != &compositeShapeMoving);
  BOOST_CHECK(compositeShape.getSize() != compositeShapeMoving.getSize());

  BOOST_CHECK(compositeShape.getSize() == 0);
  BOOST_CHECK_THROW(compositeShape[0].getFrameRect(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(pointCenterShouldCoincideWithOnlyRectangle)
{
  pedchenko::point_t centerRectangle = {7.0, 6.0};
  pedchenko::Rectangle rectangle(centerRectangle, 10.0, 6.0);
  pedchenko::CompositeShape compositeShape(rectangle);


  const double deltaFirst = 5.0;
  compositeShape.move(deltaFirst, deltaFirst);

  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, rectangle.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, rectangle.getFrameRect().pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(pointCenterShouldConstAfterMovingDxDy)
{
  pedchenko::point_t centerRectangle = {7.0, 6.0};
  pedchenko::Rectangle rectangle(centerRectangle, 10.0, 6.0);
  pedchenko::CompositeShape compositeShape(rectangle);

  const double deltaFirst = 5.0;
  pedchenko::rectangle_t frameRectBefore = compositeShape.getFrameRect();
  compositeShape.move(deltaFirst, deltaFirst);

  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, frameRectBefore.pos.x + deltaFirst, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, frameRectBefore.pos.y + deltaFirst, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.y, centerRectangle.y + deltaFirst, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.x, centerRectangle.x + deltaFirst, ACCURACY);

  centerRectangle.x += deltaFirst;
  centerRectangle.y += deltaFirst;

  pedchenko::point_t centerCircle = {-4.0, -6.0};
  pedchenko::Circle circle(centerCircle, 10.0);
  compositeShape.add(circle);

  frameRectBefore = compositeShape.getFrameRect();
  const double deltaSecond = 8.0;
  compositeShape.move(deltaSecond, deltaSecond);

  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, frameRectBefore.pos.y + deltaSecond, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, frameRectBefore.pos.x + deltaSecond, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.x, centerRectangle.x + deltaSecond, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.y, centerRectangle.y + deltaSecond, ACCURACY);
  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.x, centerCircle.x + deltaSecond, ACCURACY);
  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.y, centerCircle.y + deltaSecond, ACCURACY);
}

BOOST_AUTO_TEST_CASE(pointCenterShouldConstAfterMovingCenter)
{
  pedchenko::Rectangle rectangle({7.0, 7.0}, 10.0, 6.0);
  pedchenko::CompositeShape compositeShape(rectangle);

  compositeShape.move({0, 0});
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, rectangle.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, rectangle.getFrameRect().pos.y, ACCURACY);

  pedchenko::Circle circle({-4.0, -4.0}, 10.0);
  compositeShape.add(circle);

  pedchenko::point_t pointCenter({20.0, 20.0});
  compositeShape.move(pointCenter);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, pointCenter.x, ACCURACY);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, pointCenter.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(frameRectangleShouldConstAfterMovingCentre)
{
  pedchenko::Circle circle({7.0, 7.0}, 10.0);
  pedchenko::Rectangle rectangle({5.0, 5.0}, 2.0, 5.0);
  pedchenko::CompositeShape compositeShape;

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  const pedchenko::rectangle_t frameRectBefore = compositeShape.getFrameRect();
  const double areaBefore = compositeShape.getArea();

  rectangle.move(5.0, 5.0);
  BOOST_CHECK_CLOSE(frameRectBefore.width, compositeShape.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height, compositeShape.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, compositeShape.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(squareShouldChangeQuadratically)
{
  pedchenko::Circle circle({7.0, 7.0}, 10.0);
  pedchenko::Rectangle rectangle({5.0, 5.0}, 2.0, 5.0);
  pedchenko::CompositeShape compositeShape;

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  const double areaBefore = compositeShape.getArea();
  const double scaleFactor = 2.0;

  compositeShape.scale(scaleFactor);
  BOOST_CHECK_CLOSE(areaBefore * scaleFactor * scaleFactor, compositeShape.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(wrongParametersScaleCompositeShape)
{
  pedchenko::Circle circle({7.0, 7.0}, 10.0);
  pedchenko::Rectangle rectangle({5.0, 5.0}, 2.0, 5.0);
  pedchenko::CompositeShape compositeShape;

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  double scaleFactor = 0;
  BOOST_CHECK_THROW(compositeShape.scale(scaleFactor), std::invalid_argument);
  scaleFactor = -7;
  BOOST_CHECK_THROW(compositeShape.scale(scaleFactor), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(wrongParametersRemoveCompositeShape)
{
  pedchenko::Circle circle({7.0, 7.0}, 10.0);
  pedchenko::Rectangle rectangle({5.0, 5.0}, 2.0, 5.0);
  pedchenko::CompositeShape compositeShape;

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  BOOST_CHECK_THROW(compositeShape.remove(compositeShape.getSize()), std::invalid_argument);

  compositeShape.remove(1);
  compositeShape.remove(0);
  BOOST_CHECK_THROW(compositeShape.remove(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(movementCompositeShapeShuldReturnNull)
{
  pedchenko::Circle circle({7.0, 7.0}, 10.0);
  pedchenko::Rectangle rectangle({5.0, 5.0}, 2.0, 5.0);
  pedchenko::CompositeShape compositeShape;
  pedchenko::CompositeShape compositeShapeMovement;

  compositeShapeMovement.add(circle);
  compositeShapeMovement.add(rectangle);

  const size_t sizeComShapeMov = compositeShapeMovement.getSize();

  compositeShape = std::move(compositeShapeMovement);

  BOOST_CHECK_EQUAL(compositeShape.getSize(), sizeComShapeMov);

  BOOST_CHECK_THROW(compositeShapeMovement.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(compositeShapeMovement.remove(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
