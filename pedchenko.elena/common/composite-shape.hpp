#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace pedchenko
{
  class CompositeShape: public Shape {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &comShape);
    CompositeShape(CompositeShape &&comShape);
    CompositeShape(Shape &firstShape);
    ~CompositeShape() override;

    CompositeShape &operator =(const CompositeShape &comShape);
    CompositeShape &operator =(CompositeShape &&comShape);
    Shape &operator [](std::size_t) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & centerPoint) override;
    void move(double dx, double dy) override;
    void writeInfo() const override;
    void scale(double scalingFactor) override;

    void swap(CompositeShape& shape);
    void add(Shape &newShape);
    void remove(size_t index);
    std::size_t getSize() const;

  private:
    std::unique_ptr<Shape *[]> shapes_;
    std::size_t size_;
  };
}

#endif
