#include "rectangle.hpp"

#include <iostream>
#include <stdexcept>

pedchenko::Rectangle::Rectangle(const point_t & center, double width, double height):
  rectangle_({center, width, height})
{
  if ((rectangle_.width <= 0) || (rectangle_.height <= 0)) {
    throw std::invalid_argument("The width and height values must be positive");
 }
}

double pedchenko::Rectangle::getArea() const
{
  return rectangle_.height * rectangle_.width;
}

pedchenko::rectangle_t pedchenko::Rectangle::getFrameRect() const
{
  return rectangle_;
}

void pedchenko::Rectangle::move(double dx, double dy)
{
  rectangle_.pos.x += dx;
  rectangle_.pos.y += dy;
}


void pedchenko::Rectangle::move(const point_t & center)
{
  rectangle_.pos = center;
}

void pedchenko::Rectangle::writeInfo() const
{
  rectangle_t rectangle = getFrameRect();
  std::cout << "Rectangle: \n";
  std::cout << "1) Center: (" << rectangle.pos.x << "," << rectangle.pos.y << ") \n";
  std::cout << "2) Width: " << rectangle.width << "\n";
  std::cout << "3) Heigth: " << rectangle.height << "\n";
  std::cout << "4) Area: " << getArea() << "\n";
  std::cout << "Frame Rectangle: width = " << rectangle.width << " ,height = " << rectangle.height << "\n"
            << std::endl;
}

void pedchenko::Rectangle::scale(double scalingFactor)
{
  if (scalingFactor <= 0) {
    throw std::invalid_argument("Scaling factor must be positive");
  } else {
    rectangle_.width *= scalingFactor;
    rectangle_.height *= scalingFactor;
  }
}
