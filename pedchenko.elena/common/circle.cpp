#include "circle.hpp"
#include <iostream>
#include <math.h>
#include <stdexcept>

pedchenko::Circle::Circle(const point_t &center, double radius):
  center_(center),
  radius_(radius)
{
  if (radius_ <= 0) {
    throw std::invalid_argument("The radius values must be positive");
  }
}

double pedchenko::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}



pedchenko::rectangle_t pedchenko::Circle::getFrameRect() const
{
  return {center_, radius_ * 2, radius_ * 2};
}

void pedchenko::Circle::move(const point_t & centerPoint)
{
  center_ = centerPoint;
}

void pedchenko::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void pedchenko::Circle::writeInfo() const
{
  std::cout << "Circle: \n";
  std::cout << "1) Center: (" << center_.x << "," << center_.y << ") \n";
  std::cout << "2) Radius: " << radius_ << "\n";
  std::cout << "3) Area: " << getArea() << "\n";
  rectangle_t rectFrame = getFrameRect();
  std::cout << "Frame Rectangle: width = " << rectFrame.width << ", height = " << rectFrame.height << "\n"
            << std::endl;
}

void pedchenko::Circle::scale(double scalingFactor)
{
  if (scalingFactor <= 0) {
    throw std::invalid_argument("Scaling factor must be positive");
  } else {
    radius_ *= scalingFactor;
  }
}
