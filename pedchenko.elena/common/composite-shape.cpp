#include "composite-shape.hpp"

#include <iostream>
#include "shape.hpp"
#include "rectangle.hpp"

pedchenko::CompositeShape::CompositeShape() :
  shapes_(),
  size_(0)
{}

pedchenko::CompositeShape::CompositeShape(const pedchenko::CompositeShape &comShape) :
  shapes_(new pedchenko::Shape *[comShape.size_]),
  size_(comShape.size_)
{
  for (size_t i = 0; i < size_; i++)  {
    shapes_[i] = comShape.shapes_[i];
  }
}

pedchenko::CompositeShape::CompositeShape(pedchenko::CompositeShape &&comShape) :
  shapes_(nullptr),
  size_(0)
{
  swap(comShape);
}

pedchenko::CompositeShape::CompositeShape(pedchenko::Shape &firstShape) :
  shapes_(new Shape *[1]),
  size_(1)
{
  shapes_[0] = &firstShape;
}

pedchenko::CompositeShape::~CompositeShape()
{}

pedchenko::CompositeShape &pedchenko::CompositeShape::operator =(const pedchenko::CompositeShape &comShape)
{
  if (this != &comShape) {
    std::unique_ptr<Shape *[]> arrTmp(new Shape *[comShape.size_]);
    size_ = comShape.size_;

    for(size_t i = 0; i < size_; i++) {
      arrTmp[i] = comShape.shapes_[i];
    }

    shapes_.swap(arrTmp);
  }

  return *this;
}



pedchenko::CompositeShape &pedchenko::CompositeShape::operator =(pedchenko::CompositeShape &&comShape)
{
  if (this != &comShape)  {
    shapes_.reset();
    size_ = 0;
    swap(comShape);
  }

  return *this;
}



void pedchenko::CompositeShape::swap(pedchenko::CompositeShape& shape)
{
  std::swap(shapes_, shape.shapes_);
  std::swap(size_, shape.size_);
}

double pedchenko::CompositeShape::getArea() const
{
  double sumArea = 0;
  for (size_t i = 0; i < size_; i++) {
    sumArea += shapes_[i]->getArea();
  }

  return sumArea;
}


pedchenko::rectangle_t pedchenko::CompositeShape::getFrameRect() const
{
  if ((!shapes_) && (size_ == 0)) {
    throw std::logic_error("In this CompositeShape null shapes ");
  }

  rectangle_t tempFrameRect = shapes_[0]->getFrameRect();
  double minX = tempFrameRect.pos.x - tempFrameRect.width / 2;
  double maxX = tempFrameRect.pos.x + tempFrameRect.width / 2;
  double maxY = tempFrameRect.pos.y + tempFrameRect.height / 2;
  double minY = tempFrameRect.pos.y - tempFrameRect.height / 2;

  for (size_t i = 1; i < size_; i++) {
    tempFrameRect = shapes_[i]->getFrameRect();
    minX = std::min(minX, tempFrameRect.pos.x - tempFrameRect.width / 2);
    maxX = std::max(maxX, tempFrameRect.pos.x + tempFrameRect.width / 2);
    maxY = std::max(maxY, tempFrameRect.pos.y + tempFrameRect.height / 2);
    minY = std::min(minY, tempFrameRect.pos.y - tempFrameRect.height / 2);
  }

  return {{(maxX + minX) / 2, (maxY + minY) / 2}, maxX - minX, maxY - minY};
}

void pedchenko::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; i++) {
    shapes_[i]->move(dx, dy);
  }
}

void pedchenko::CompositeShape::move(const pedchenko::point_t & centerPoint)
{
  double const dx = centerPoint.x - getFrameRect().pos.x;
  double const dy = centerPoint.y - getFrameRect().pos.y;
  move(dx, dy);
}

void pedchenko::CompositeShape::scale(double scalingFactor)
{
  if (scalingFactor <= 0) {
    throw std::invalid_argument("Scaling factor must be positive");
  }

  const point_t centerComposite = getFrameRect().pos;
  for (size_t i= 0; i < size_; i++) {
    const double dx = shapes_[i]->getFrameRect().pos.x - centerComposite.x;
    const double dy = shapes_[i]->getFrameRect().pos.y - centerComposite.y;
    shapes_[i]->move({centerComposite.x + dx * scalingFactor, centerComposite.y + dy * scalingFactor});
    shapes_[i]->scale(scalingFactor);
  }
}

void pedchenko::CompositeShape::writeInfo() const
{
  pedchenko::rectangle_t rectangle = getFrameRect();

  std::cout << "Information about the CompositeShape: \n";
  std::cout << "Number of shapes: " << size_ << std::endl;
  std::cout << "Center: (" << rectangle.pos.x << "," << rectangle.pos.y << ") \n";
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "Frame Rectangle: width = " << rectangle.width << ", height = " << rectangle.height << "\n";
  std::cout << "\nInformation about the Shape in CompositeShape\n" << std::endl;

  for (size_t i = 0; i < size_; i++) {
    std::cout << "__ITEM: " << i << std::endl;
    shapes_[i]->writeInfo();
  }
  std::cout << std::endl;
}

size_t pedchenko::CompositeShape::getSize() const
{
  return size_;
}

void pedchenko::CompositeShape::add(pedchenko::Shape &newShape)
{
  std::unique_ptr<Shape *[]> arrNew(new Shape *[size_ + 1]);

  for (size_t i = 0; i < size_; i++) {
    arrNew[i] = shapes_[i];
  }

  arrNew[size_++] = &newShape;
  shapes_.swap(arrNew);
}

void pedchenko::CompositeShape::remove(std::size_t index)
{
  if (size_ == 0) {
    throw std::invalid_argument("Composite shape is empty");
  }
  if (index >= size_) {
    throw std::invalid_argument("Index is outside the array");
  }

  for (size_t i = index; i < size_ - 1; i++) {
    shapes_[i] = shapes_[i + 1];
  }

  std::unique_ptr<Shape *[]> arrNew(new Shape *[size_--]);

  for (size_t i = 0; i < size_; i++) {
    arrNew[i] = shapes_[i];
  }

  shapes_.swap(arrNew);
}

pedchenko::Shape &pedchenko::CompositeShape::operator [](std::size_t number) const
{
  if (number < size_) {
    return *shapes_[number];
  }
  throw std::out_of_range("Index was out of range in CompositeShape::operator[]");
}
