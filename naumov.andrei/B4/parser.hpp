#ifndef PARSER_HPP
#define PARSER_HPP

#include <string>

#include "data_struct.hpp"

class Parser {
public:
  Parser();
  explicit operator bool() const;
  DataStruct parseLine();
private:
  const std::string separators_;
};

#endif //!PARSER_HPP
