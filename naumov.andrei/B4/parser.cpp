#include "parser.hpp"
#include <iostream>
#include <exception>

Parser::Parser():
  separators_(" \t")
{}

Parser::operator bool() const
{
  return !(std::cin.peek() == EOF);
}

DataStruct Parser::parseLine()
{
  std::string line;
  std::getline(std::cin, line);
  int key[2];
  size_t pos = line.find_first_of(',');
  key[0] = std::stoi(line.substr(0, pos));
  line = line.substr(pos);
  line = line.substr(1);
  while (line.find_first_of(separators_) == 0){
    line.erase(0, 1);
  }
  pos = line.find_first_of(',');
  if (pos == std::string::npos) {
    throw std::invalid_argument("Invalid data");
  }
  key[1] = std::stoi(line.substr(0, pos));
  for (size_t i = 0; i < 2; ++i) {
    if (key[i] < -5 || key[i] > 5) {
      throw std::invalid_argument("Invalid key");
    }
  }
  if (pos == std::string::npos) {
    throw std::invalid_argument("Invalid data");
  }
  line = line.substr(pos + 1);
  while (line.find_first_of(separators_) == 0){
    line.erase(0, 1);
  }
  if (line.empty()) {
    throw std::invalid_argument("Invalid data");
  }
  return {key[0], key[1], line};
}
