#ifndef COMPARE_DATA_STRUCT_HPP
#define COMPARE_DATA_STRUCT_HPP

#include "data_struct.hpp"

class compare_data_struct {
public:
  bool operator()( const DataStruct& lhs, const DataStruct& rhs ) const;
};

#endif //!COMPARE_DATA_STRUCT_HPP
