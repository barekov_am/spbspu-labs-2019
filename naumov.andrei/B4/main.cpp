#include <iostream>
#include <exception>
#include <vector>
#include <algorithm>
#include "parser.hpp"
#include "compare_data_struct.hpp"

int main(int , char*[])
{
  try {
    std::vector<DataStruct> data;
    Parser parser;
    while (parser) {
      data.push_back(parser.parseLine());
    }
    if (!data.empty()) {
      std::sort(data.begin(), data.end(), compare_data_struct());
      for (size_t i = 0; i < data.size(); ++i) {
        std::cout << data[i].key1 << "," << data[i].key2 << "," << data[i].str << '\n';
      }
    }
  }
  catch (std::exception & error) {
    std::cerr << error.what() << '\n';
    return 1;
  }
  return 0;
}
