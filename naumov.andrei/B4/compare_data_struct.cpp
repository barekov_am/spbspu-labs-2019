#include "compare_data_struct.hpp"

bool compare_data_struct::operator()(const DataStruct& lhs, const DataStruct& rhs) const
{
  if (lhs.key1 < rhs.key1) {
    return true;
  }
  if (lhs.key1 == rhs.key1) {
    if (lhs.key2 < rhs.key2) {
      return true;
    }
    if (lhs.key2 == rhs.key2) {
      if (lhs.str.size() < rhs.str.size()) {
        return true;
      }
    }
  }
  return false;
}
