#include "mark_collection.hpp"

void MarkCollection::pushBack(const Mark & newMark)
{
  marks_.push_back(newMark);
}

void MarkCollection::rewrite(iterator pos, const Mark & newMark)
{
  *pos = newMark;
}

std::pair<bool, MarkCollection::iterator> MarkCollection::doesMarkExist(std::string markName)
{
  for (iterator i = marks_.begin(); i != marks_.end(); ++i) {
    if (i->name_.compare(markName) == 0) {
      return std::pair<bool, iterator>(true, i);
    }
  }
  return std::pair<bool, iterator>(false, marks_.end());
}

MarkCollection::iterator MarkCollection::begin()
{
  return marks_.begin();
}

MarkCollection::iterator MarkCollection::end()
{
  return marks_.end();
}
