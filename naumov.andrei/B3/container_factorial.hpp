#ifndef CONTAINER_FACTORIAL_HPP
#define CONTAINER_FACTORIAL_HPP

#include "iterator.hpp"

class ContainerFactorial {
public:
  ContainerFactorial() = default;
  ContainerFactorial(const ContainerFactorial &) = delete;
  ContainerFactorial(ContainerFactorial &&) = delete;
  ~ContainerFactorial() = default;
  ContainerFactorial & operator=(const ContainerFactorial &) = delete;
  ContainerFactorial & operator=(ContainerFactorial &&) = delete;
  Iterator begin();
  Iterator end();
private:
};

#endif //!CONTAINER_FACTORIAL_HPP
