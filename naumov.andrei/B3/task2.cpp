#include <algorithm>
#include <iostream>
#include "container_factorial.hpp"

void task2()
{
  ContainerFactorial containerFactorial;
  std::for_each(containerFactorial.begin(), containerFactorial.end(),
    [](unsigned long long value){std::cout << value << ' ';});
  std::cout << '\n';
  std::reverse_copy(containerFactorial.begin(), containerFactorial.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << "\n";
}
