#include "phone_book_control_unit.hpp"
#include <stdexcept>
#include <iostream>

PhoneBookControlUnit::PhoneBookControlUnit()
{
  markCollection_.pushBack(Mark("current", phoneBook_.begin()));
}


void PhoneBookControlUnit::addRecord(std::string number, std::string name)
{
  phoneBook_.pushBack(PhoneRecord(number, name));
  if (markCollection_.begin()->pRecord_ == phoneBook_.end()) {
    (*(markCollection_.begin())).pRecord_ = phoneBook_.begin();
  }
}

bool PhoneBookControlUnit::storeMark(std::string mark, std::string name)
{
  auto doesExist = markCollection_.doesMarkExist(mark);
  if (doesExist.first) {
    if (doesExist.second->pRecord_ == phoneBook_.end()) {
      return false;
    }
    if (markCollection_.doesMarkExist(name).first) {
      return false;
    }
    markCollection_.pushBack(Mark(name, doesExist.second->pRecord_));
    return true;
  }
  return false;
}

bool PhoneBookControlUnit::insertAfter(std::string mark, std::string number, std::string name)
{
  auto doesExist = markCollection_.doesMarkExist(mark);
  if (doesExist.first) {
    if (doesExist.second->pRecord_ == phoneBook_.end()) {
      phoneBook_.pushBack(PhoneRecord(number, name));
      if (markCollection_.begin()->pRecord_ == phoneBook_.end()) {
        markCollection_.begin()->pRecord_ = phoneBook_.begin();
      }
      return true;
    }
    phoneBook_.insertAfter(doesExist.second->pRecord_, PhoneRecord(number, name));
    return true;
  }
  return false;
}

bool PhoneBookControlUnit::insertBefore(std::string mark, std::string number, std::string name)
{
  auto doesExist = markCollection_.doesMarkExist(mark);
  if (doesExist.first) {
    if (doesExist.second->pRecord_ == phoneBook_.begin()) {
      phoneBook_.pushFront(PhoneRecord(number, name));
      if (markCollection_.begin()->pRecord_ == phoneBook_.end()) {
        markCollection_.begin()->pRecord_ = phoneBook_.begin();
      }
      return true;
    }
    phoneBook_.insertBefore(doesExist.second->pRecord_, PhoneRecord(number, name));
    return true;
  }
  return false;
}

bool PhoneBookControlUnit::deleteRecordByMark(std::string mark)
{
  auto doesExist = markCollection_.doesMarkExist(mark);
  if (doesExist.first) {
    if (doesExist.second->pRecord_ == phoneBook_.end()) {
      return false;
    }
    recordIterator tempValue = doesExist.second->pRecord_;
    if ((doesExist.second->pRecord_ == --phoneBook_.end()) && (doesExist.second->pRecord_ != --phoneBook_.begin())) {
      for (markIterator i = markCollection_.begin(); i != markCollection_.end(); ++i)
      {
        if (i->pRecord_ == tempValue) {
          --(i->pRecord_);
        }
      }
    } else {
      for (markIterator i = markCollection_.begin(); i != markCollection_.end(); ++i)
      {
        if (i->pRecord_ == tempValue) {
          ++(i->pRecord_);
        }
      }
    }
    phoneBook_.remove(tempValue);
    return true;
  }
  return false;
}

std::pair<bool, bool> PhoneBookControlUnit::showRecordByMark(std::string mark)
{
  auto doesExist = markCollection_.doesMarkExist(mark);
  if (doesExist.first) {
    if (doesExist.second->pRecord_ == phoneBook_.end()) {
      return std::pair<bool, bool>(false, true);
    }
    std::cout << doesExist.second->pRecord_->number_ << ' ' << doesExist.second->pRecord_->name_ << '\n';
    return std::pair<bool, bool>(true, false);
  }
  return std::pair<bool, bool>(false, false);
}

bool PhoneBookControlUnit::moveMark(std::string mark, std::pair<MoveMode, int> steps)
{
  auto doesExist = markCollection_.doesMarkExist(mark);
  if (doesExist.first) {
    switch (steps.first) {
      case FIRST:
        jumpToFirstRecord(doesExist.second);
        return true;
      case LAST:
        jumpToLastRecord(doesExist.second);
        return true;
      case BY_STEPS:
        if (steps.second < 0) {
          for (int i = 0; i < -steps.second; ++i) {
            jumpToPreviousRecord(doesExist.second);
          }
        } else {
          for (int i = 0; i < steps.second; ++i) {
            jumpToNextRecord(doesExist.second);
          }
        }
        return true;
    }
  }
  return false;
}


void PhoneBookControlUnit::jumpToNextRecord(markIterator pos)
{
  if (pos->pRecord_ == phoneBook_.end()) {
    throw std::out_of_range("Next record does not exist");
  }
  ++(pos->pRecord_);
}

void PhoneBookControlUnit::jumpToPreviousRecord(markIterator pos)
{
  if (pos->pRecord_ == phoneBook_.begin()) {
    throw std::out_of_range("Previous record does not exist");
  }
  --(pos->pRecord_);
}

void PhoneBookControlUnit::jumpToFirstRecord(markIterator pos)
{
  pos->pRecord_ = phoneBook_.begin();
}

void PhoneBookControlUnit::jumpToLastRecord(markIterator pos)
{
  pos->pRecord_ = --phoneBook_.end();
}
