#include "parser.hpp"
#include <iostream>
#include <exception>

Parser::Parser():
  string_(),
  isOk_(false),
  separators_(" \t")
{}

Parser::operator bool() const
{
  return isOk_;
}

CommandSet Parser::parseCommand()
{
  if (string_.empty()) {
    isOk_ = false;
    return INVALID_COMMAND;
  }
  auto pos = string_.find_first_of(separators_);
  if (pos == std::string::npos) {
    string_.erase();
    isOk_ = false;
    return INVALID_COMMAND;
  }
  std::string command = string_.substr(0, pos);
  setString(string_.substr(pos));
  if (command.compare("add") == 0) {
    return ADD;
  } else if (command.compare("store") == 0) {
    return STORE;
  } else if (command.compare("insert") == 0) {
    auto pos = string_.find_first_of(separators_);
    if (pos == std::string::npos) {
      string_.erase();
      isOk_ = false;
      return INVALID_COMMAND;
    }
    std::string direction = string_.substr(0, pos);
    setString(string_.substr(pos));
    if (direction.compare("before") == 0) {
      return INSERT_BEFORE;
    } else if (direction.compare("after") == 0) {
      return INSERT_AFTER;
    }
    isOk_ = false;
    return INVALID_COMMAND;
  } else if (command.compare("delete") == 0) {
    return DELETE;
  } else if (command.compare("show") == 0) {
    return SHOW;
  } else if (command.compare("move") == 0) {
    return MOVE;
  } else {
    string_.erase();
    isOk_ = false;
    return INVALID_COMMAND;
  }
}

std::string Parser::parseNumber()
{
  if (string_.empty()) {
    printInvalidCommand();
    isOk_ = false;
    return string_;
  }
  auto pos = string_.find_first_of(separators_);
  std::string number;
  if (pos == std::string::npos) {
    string_.erase();
    printInvalidCommand();
    isOk_ = false;
    return number;
  }
  number = string_.substr(0, pos);
  setString(string_.substr(pos));
  for (unsigned int i = 0; i < number.size(); ++i) {
    if ((number[i] < '0') || (number[i] > '9')) {
      number.erase();
      printInvalidCommand();
      string_.erase();
      isOk_ = false;
      return number;
    }
  }
  return number;
}

std::string Parser::parseName()
{
  if (!isOk_) {
    return string_;
  }
  if (string_.empty()) {
    printInvalidCommand();
    isOk_ = false;
    return string_;
  }
  if ((string_.find_first_of('\"') != 0) || (string_.find_last_of('\"') != string_.size() - 1) || (string_.size() == 1)) {
    printInvalidCommand();
    isOk_ = false;
    string_.erase();
    return string_;
  }
  std::string name = string_;
  string_.erase();
  name.erase(0, 1);
  name.pop_back();
  std::string criteria("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ,.");
  auto pos = name.find_first_not_of(criteria);
  while (pos != std::string::npos) {
    if (pos == name.size() - 1) {
      name.erase();
      return name;
    }
    if ((name[pos] == '\\') && ((name[pos + 1] == '\\') || (name[pos + 1] == '\"'))) {
      name.erase(pos, 1);
      ++pos;
    } else {
      name.erase();
      isOk_ = false;
      return name;
    }
    pos = name.find_first_not_of(criteria, pos);
  }
  if (name.empty()) {
    printInvalidCommand();
    string_.erase();
    isOk_ = false;
    return name;
  }
  return name;
}

std::string Parser::parseMark()
{
  if (string_.empty()) {
    printInvalidCommand();
    isOk_ = false;
    return string_;
  }
  auto pos = string_.find_first_of(separators_);
  std::string mark;
  if (pos == std::string::npos) {
    mark = string_;
    string_.erase();
    return mark;
  }
  mark = string_.substr(0, pos);
  string_ = string_.substr(pos);
  setString(string_);
  return mark;
}

std::pair<MoveMode, int> Parser::parseSteps()
{
  if (string_.empty()) {
    printInvalidStep();
    isOk_ = false;
    return std::pair<MoveMode , int>(BY_STEPS, 0);
  }
  std::string stepsStr;
  auto pos = string_.find_first_of(separators_);
  if (pos == std::string::npos) {
    stepsStr = string_;
    string_.erase();
  } else {
    stepsStr = string_.substr(0, pos);
    setString(string_.substr(pos));
  }
  if (stepsStr.compare("first") == 0) {
    return std::pair<MoveMode, int>(FIRST, 0);
  } else if (stepsStr.compare("last") == 0) {
    return std::pair<MoveMode, int>(LAST, 0);
  } else {
    try {
      int steps = std::stoi(stepsStr);
      return std::pair<MoveMode, int>(BY_STEPS, steps);
    } catch (std::invalid_argument &) {
      printInvalidStep();
      isOk_ = false;
      return std::pair<MoveMode, int>(BY_STEPS, 0);
    }
  }
}

void Parser::printInvalidCommand()
{
  std::cout << "<INVALID COMMAND>" << '\n';
}

void Parser::printInvalidStep()
{
  std::cout << "<INVALID STEP>" << '\n';
}

void Parser::printEmpty()
{
  std::cout << "<EMPTY>" << '\n';
}

void Parser::printInvalidBookmark()
{
  std::cout << "<INVALID BOOKMARK>" << '\n';
}

bool Parser::empty()
{
  return (std::cin.peek() == EOF);
}

void Parser::setString(std::string string)
{
  if (string.empty()) {
    isOk_ = false;
    string_.erase();
    return;
  }
  while (string.find_first_of(separators_) == 0){
    string.erase(0, 1);
  }
  while ((string.find_last_of(separators_) == string.size() - 1) && (!string.empty())) {
    string.pop_back();
  }
  isOk_ = true;
  string_ = string;
}
