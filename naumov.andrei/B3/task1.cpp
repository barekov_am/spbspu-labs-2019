#include <iostream>
#include "phone_book_control_unit.hpp"
#include "parser.hpp"

void task1()
{
  PhoneBookControlUnit phoneBookControlUnit;
  Parser parser;
  std::string string;
  while (!parser.empty()) {
    std::getline(std::cin, string);
    parser.setString(string);
    switch (parser.parseCommand()) {
      case ADD: {
        std::string number = parser.parseNumber();
        std::string name = parser.parseName();
        if (parser) {
          phoneBookControlUnit.addRecord(number, name);
        }
        break;
      }
      case STORE: {
        std::string mark = parser.parseMark();
        std::string name = parser.parseMark();
        if (parser) {
          if (!phoneBookControlUnit.storeMark(mark, name)) {
            parser.printInvalidBookmark();
          }
        }
        break;
      }
      case INSERT_BEFORE: {
        std::string mark = parser.parseMark();
        std::string number = parser.parseNumber();
        std::string name = parser.parseName();
        if (parser) {
          if (!phoneBookControlUnit.insertBefore(mark, number, name)) {
            parser.printInvalidBookmark();
          }
        }
        break;
      }
      case INSERT_AFTER: {
        std::string mark = parser.parseMark();
        std::string number = parser.parseNumber();
        std::string name = parser.parseName();
        if (parser) {
          if (!phoneBookControlUnit.insertAfter(mark, number, name)) {
            parser.printInvalidBookmark();
          }
        }
        break;
      }
      case DELETE: {
        std::string mark = parser.parseMark();
        if (parser) {
          if (!phoneBookControlUnit.deleteRecordByMark(mark)) {
            parser.printInvalidBookmark();
          }
        }
        break;
      }
      case SHOW: {
        std::string mark = parser.parseMark();
        if (parser) {
          auto showResult = phoneBookControlUnit.showRecordByMark(mark);
          if (!showResult.first) {
            if (!showResult.second) {
              parser.printInvalidBookmark();
            } else {
              parser.printEmpty();
            }
          }
        }
        break;
      }
      case MOVE: {
        std::string mark = parser.parseMark();
        auto steps = parser.parseSteps();
        if (parser) {
          if (!phoneBookControlUnit.moveMark(mark, steps)) {
            parser.printInvalidBookmark();
          }
        }
        break;
      }
      case INVALID_COMMAND:
        parser.printInvalidCommand();
        break;
    }
  }
}
