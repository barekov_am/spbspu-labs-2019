#include "phone_book.hpp"
#include <stdexcept>


PhoneRecord PhoneBook::get(iterator pos)
{
  if (records_.empty()) {
    throw std::out_of_range("No records in phone book");
  }
  return *pos;
}

PhoneBook::iterator PhoneBook::remove(iterator pos)
{
  if (records_.empty()) {
    throw std::out_of_range("No records in phone book");
  }
  return records_.erase(pos);
}

void PhoneBook::rewrite(iterator pos, const PhoneRecord & newRecord)
{
  *pos = newRecord;
}

PhoneBook::iterator PhoneBook::insertAfter(iterator pos, const PhoneRecord & newRecord)
{
  return records_.insert(++pos, newRecord);
}

PhoneBook::iterator PhoneBook::insertBefore(iterator pos, const PhoneRecord & newRecord)
{
  return records_.insert(pos, newRecord);
}

void PhoneBook::pushBack(const PhoneRecord & newRecord)
{
  records_.push_back(newRecord);
}

void PhoneBook::pushFront(const PhoneRecord & newRecord)
{
  records_.push_front(newRecord);
}

PhoneBook::iterator PhoneBook::begin()
{
  return records_.begin();
}

PhoneBook::iterator PhoneBook::end()
{
  return records_.end();
}

int PhoneBook::size() const
{
  return records_.size();
}

bool PhoneBook::empty() const
{
  return records_.empty();
}
