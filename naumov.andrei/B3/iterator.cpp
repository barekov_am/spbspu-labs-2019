#include "iterator.hpp"

Iterator::Iterator():
        value_(1),
        index_(1)
{ }

Iterator::Iterator(int index):
        value_(getValue(index)),
        index_(index)
{ }

Iterator & Iterator::operator++()
{
  ++index_;
  value_ *= index_;
  return *this;
}

Iterator & Iterator::operator--()
{
  if (index_ > 1)
  {
    value_ /= index_;
    --index_;
  }
  return *this;
}

Iterator Iterator::operator++(int)
{
  Iterator tempIterator = *this;
  ++(*this);
  return tempIterator;
}

Iterator Iterator::operator--(int)
{
  Iterator tempIterator = *this;
  --(*this);
  return tempIterator;
}

bool Iterator::operator==(const Iterator & rhs) const
{
  return ((value_ == rhs.value_) && (index_ == rhs.index_));
}

bool Iterator::operator!=(const Iterator & rhs) const
{
  return !(*this == rhs);
}

unsigned long long & Iterator::operator*()
{
  return value_;
}

unsigned long long * Iterator::operator->()
{
  return &value_;
}

unsigned long long Iterator::getValue(int index) const
{
  if (index <= 1)
  {
    return 1;
  }
  else
  {
    return index * getValue(index - 1);
  }
}



