#ifndef PHONE_BOOK_CONTROL_UNIT_HPP
#define PHONE_BOOK_CONTROL_UNIT_HPP

#include "mark_collection.hpp"

enum CommandSet {
  ADD,
  STORE,
  INSERT_BEFORE,
  INSERT_AFTER,
  DELETE,
  SHOW,
  MOVE,
  INVALID_COMMAND
};

enum MoveMode {
  FIRST,
  LAST,
  BY_STEPS
};

class PhoneBookControlUnit {
public:
  using recordIterator = PhoneBook::iterator;
  using markIterator = MarkCollection::iterator;

  PhoneBookControlUnit();

  void addRecord(std::string number, std::string name);
  bool storeMark(std::string mark, std::string name);
  bool insertAfter(std::string mark, std::string number, std::string name); //if mark points to record next to last then push back
  bool insertBefore(std::string mark, std::string number, std::string name);
  bool deleteRecordByMark(std::string mark);
  std::pair<bool, bool> showRecordByMark(std::string mark); //first - is ok, second - is empty
  bool moveMark(std::string mark, std::pair<MoveMode, int> steps);
private:
  PhoneBook phoneBook_;
  MarkCollection markCollection_;

  void jumpToNextRecord(markIterator pos);
  void jumpToPreviousRecord(markIterator pos);
  void jumpToFirstRecord(markIterator pos);
  void jumpToLastRecord(markIterator pos);
};

#endif //!PHONE_BOOK_CONTROL_UNIT_HPP
