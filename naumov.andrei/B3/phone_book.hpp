#ifndef PHONE_BOOK_HPP
#define PHONE_BOOK_HPP

#include <list>
#include <string>
#include <utility>

class PhoneRecord {
public:
  std::string number_;
  std::string name_;
  PhoneRecord(std::string number, std::string name):
    number_(number),
    name_(name)
  {};
};

class PhoneBook {
public:
  using iterator = std::list<PhoneRecord>::iterator;

  PhoneRecord get(iterator pos);
  iterator remove(iterator pos);
  void rewrite(iterator pos, const PhoneRecord & newRecord);
  iterator insertAfter(iterator pos, const PhoneRecord & newRecord);
  iterator insertBefore(iterator pos, const PhoneRecord & newRecord);

  void pushBack(const PhoneRecord & newRecord);
  void pushFront(const PhoneRecord & newRecord);

  iterator begin();
  iterator end();

  int size() const;
  bool empty() const;
private:
  std::list<PhoneRecord> records_;
};

#endif //!PHONE_BOOK_HPP
