#ifndef ITERATOR_HPP
#define ITERATOR_HPP

#include <iterator>

class Iterator: public std::iterator<std::bidirectional_iterator_tag, unsigned long long> {
public:
  Iterator();
  Iterator(int index);

  Iterator & operator ++();
  Iterator & operator --();
  Iterator operator ++(int);
  Iterator operator --(int);
  bool operator ==(const Iterator & rhs) const;
  bool operator !=(const Iterator & rhs) const;
  unsigned long long & operator *();
  unsigned long long * operator ->();
private:
  unsigned long long value_;
  int index_;

  unsigned long long getValue(int index) const;
};

#endif //!ITERATOR_HPP
