#ifndef PARSER_HPP
#define PARSER_HPP

#include "phone_book_control_unit.hpp"

class Parser {
public:
  Parser();
  explicit operator bool() const;

  CommandSet parseCommand();
  std::string parseNumber();
  std::string parseName();
  std::string parseMark();
  std::pair<MoveMode , int> parseSteps();

  void printInvalidCommand();
  void printInvalidStep();
  void printEmpty();
  void printInvalidBookmark();
  bool empty();
  void setString(std::string string);
private:
  std::string string_;
  bool isOk_;
  const std::string separators_;
};

#endif //!PARSER_HPP
