#ifndef MARK_COLLECTION_HPP
#define MARK_COLLECTION_HPP

#include "phone_book.hpp"

class Mark {
public:
  using iterator = std::list<PhoneRecord>::iterator;
  std::string name_;
  iterator pRecord_;
  Mark(std::string name, iterator pRecord):
    name_(name),
    pRecord_(pRecord)
  {};
};

class MarkCollection {
public:
  using iterator = std::list<Mark>::iterator;

  MarkCollection() = default;
  ~MarkCollection() = default;

  void pushBack(const Mark & newMark);
  void rewrite(iterator pos, const Mark & newMark);
  std::pair<bool, iterator> doesMarkExist(std::string markName);
  iterator begin();
  iterator end();
private:
  std::list<Mark> marks_;
};

#endif //!MARK_COLLECTION_HPP
