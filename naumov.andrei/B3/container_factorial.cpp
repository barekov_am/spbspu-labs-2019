#include "container_factorial.hpp"

Iterator ContainerFactorial::begin()
{
  return Iterator(1);
}

Iterator ContainerFactorial::end()
{
  return Iterator(11);
}
