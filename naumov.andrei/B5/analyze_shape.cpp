#include <cmath>
#include "analyze_shape.hpp"

analyze_shape::analyze_shape():
  countOfVertices_(0),
  countOfTriangles_(0),
  countOfSquares_(0),
  countOfRectangles_(0)
{}

void analyze_shape::operator()(const Shape& shape)
{
  countOfVertices_ += shape.size();
  if (shape.size() == 3) {
    ++countOfTriangles_;
    return;
  }
  if (shape.size() == 4) {
    unsigned int diffX1 = std::abs(shape[0].x - shape[1].x);
    unsigned int diffY1 = std::abs(shape[0].y - shape[1].y);
    unsigned int diffX2 = std::abs(shape[2].x - shape[3].x);
    unsigned int diffY2 = std::abs(shape[2].y - shape[3].y);
    if (diffX1 == diffX2 && diffY1 == diffY2) {
      ++countOfRectangles_;
      auto first = std::sqrt(std::pow(shape[0].x - shape[1].x, 2) + std::pow(shape[0].y - shape[1].y, 2));
      auto second = std::sqrt(std::pow(shape[0].x - shape[2].x, 2) + std::pow(shape[0].y - shape[2].y, 2));
      auto third = std::sqrt(std::pow(shape[0].x - shape[3].x, 2) + std::pow(shape[0].y - shape[3].y, 2));
      if (std::min(first, second) == std::min(second, third)) {
        ++countOfSquares_;
      }
    }
  }
}

size_t analyze_shape::getCountOfVertices() const
{
  return countOfVertices_;
}

size_t analyze_shape::getCountOfTriangles() const
{
  return countOfTriangles_;
}

size_t analyze_shape::getCountOfSquares() const
{
  return countOfSquares_;
}

size_t analyze_shape::getCountOfRectangles() const
{
  return countOfRectangles_;
}
