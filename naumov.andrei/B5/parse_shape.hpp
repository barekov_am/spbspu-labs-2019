#ifndef PARSE_SHAPE_HPP
#define PARSE_SHAPE_HPP

#include <string>
#include <functional>
#include "shape.hpp"

class parse_shape: public std::unary_function<std::string&, Shape> {
public:
  const std::string delimiters_;
  parse_shape();
  Shape operator()(std::string& line) const;
};

#endif //!PARSE_SHAPE_HPP
