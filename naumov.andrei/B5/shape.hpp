#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <vector>

struct Point {
  int x, y;
  Point(const Point& rhs) = default;
  Point(Point&& rhs);
  Point(int x, int y);
  ~Point() = default;
  Point& operator=(const Point& rhs) = default;
  Point& operator=(Point&& rhs);
  bool operator==(const Point& rhs) const;
  bool operator!=(const Point& rhs) const;
};

using Shape = std::vector<Point>;

#endif //!SHAPE_HPP
