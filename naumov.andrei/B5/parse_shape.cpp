#include "parse_shape.hpp"

#include <stdexcept>

parse_shape::parse_shape():
  delimiters_(" \t")
{}

Shape parse_shape::operator()(std::string& line) const
{
  while (line.find_first_of(delimiters_) == 0) {
    line.erase(0, 1);
  }
  size_t pos = line.find_first_of(delimiters_);
  if (pos == std::string::npos) {
    throw std::invalid_argument("Invalid shape");
  }
  if (std::stoi(line.substr(0, pos)) <= 0) {
    throw std::invalid_argument("Invalid number of vertices");
  }
  size_t numberOfVertices = std::stoi(line.substr(0, pos));
  line = line.substr(pos);

  Shape shape;
  int tempX, tempY;
  size_t posOfOpeningParenthesis;
  size_t posOfSemicolon;
  size_t posOfClosingParenthesis;
  for (size_t i = 0; i < numberOfVertices; ++i) {
    if (line.empty()) {
      throw std::invalid_argument("Invalid number of vertices");
    }
    while (line.find_first_of(delimiters_) == 0) {
      line.erase(0, 1);
    }
    posOfOpeningParenthesis = line.find_first_of('(');
    posOfSemicolon = line.find_first_of(';');
    posOfClosingParenthesis = line.find_first_of(')');
    if (posOfOpeningParenthesis != 0 && posOfSemicolon == std::string::npos
      && posOfClosingParenthesis == std::string::npos
      && !( (posOfOpeningParenthesis < posOfSemicolon) && (posOfSemicolon < posOfClosingParenthesis))) {
      throw std::invalid_argument("Invalid point's declaration");
    }
    tempX = std::stoi(line.substr(1, posOfSemicolon - 1));
    line = line.substr(posOfSemicolon + 1);
    tempY = std::stoi(line.substr(0, posOfClosingParenthesis - posOfSemicolon - 1));
    line = line.substr(posOfClosingParenthesis - posOfSemicolon);
    shape.push_back(Point(tempX, tempY));
  }
  while (line.find_first_of(delimiters_) == 0) {
    line.erase(0, 1);
  }
  if (!line.empty()) {
    throw std::invalid_argument("Invalid number of vertices");
  }
  return shape;
}
