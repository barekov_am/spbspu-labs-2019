#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>
#include "parse_shape.hpp"
#include "analyze_shape.hpp"
#include "compare_shapes.hpp"

void task2()
{
  std::vector<Shape> shapes;
  parse_shape parser;
  std::string buffer;
  while (std::cin) {
    std::getline(std::cin, buffer);
    while (buffer.find_first_of(parser.delimiters_) == 0) {
      buffer.erase(0, 1);
    }
    if (!buffer.empty()) {
      shapes.push_back(parser(buffer));
    }
  }
  analyze_shape statistics = std::for_each(shapes.begin(), shapes.end(), analyze_shape());
  shapes.erase(std::remove_if(shapes.begin(), shapes.end(), [](const Shape& shape) -> bool
    {
      return (shape.size() == 5);
    }), shapes.end());
  Shape points;
  std::transform(shapes.begin(), shapes.end(), std::back_inserter(points), [](const Shape& shape) -> Point
    {
      return shape[0];
    });
  std::stable_sort(shapes.begin(), shapes.end(), compare_shapes());
  std::cout << "Vertices: " << statistics.getCountOfVertices() << '\n';
  std::cout << "Triangles: " << statistics.getCountOfTriangles() << '\n';
  std::cout << "Squares: " << statistics.getCountOfSquares() << '\n';
  std::cout << "Rectangles: " << statistics.getCountOfRectangles() << '\n';
  std::cout << "Points: ";
  for (size_t i = 0; i != points.size(); ++i) {
    std::cout << '(' << points[i].x << ';' << points[i].y << ')';
    if (i != points.size() - 1) {
      std::cout << ' ';
    }
  }
  std::cout << "\nShapes:";
  for (size_t i = 0; i != shapes.size(); ++i) {
    std::cout << '\n';
    std::cout << shapes[i].size() << ' ';
    for (size_t j = 0; j != shapes[i].size(); j++) {
      std::cout << " (" << shapes[i][j].x << "; " << shapes[i][j].y << ") ";
    }
  }
}
