#include "compare_shapes.hpp"
#include "analyze_shape.hpp"

bool compare_shapes::operator()(const Shape& lhs, const Shape& rhs) const
{
  analyze_shape statLhs;
  statLhs(lhs);
  analyze_shape statRhs;
  statRhs(rhs);
  if (statLhs.getCountOfTriangles() && !statRhs.getCountOfTriangles()) {
    return true;
  }
  if (statLhs.getCountOfSquares() && !statRhs.getCountOfSquares() && !statRhs.getCountOfTriangles()) {
    return true;
  }
  if (statLhs.getCountOfRectangles()
    && !statRhs.getCountOfRectangles() && !statRhs.getCountOfSquares() && !statRhs.getCountOfTriangles()) {
    return true;
  }
  return false;
}
