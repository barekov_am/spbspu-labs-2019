#include "parse_text_fragment.hpp"

parse_text_fragment::parse_text_fragment():
  delimiters_(" \t\n")
{}

std::vector<std::string> parse_text_fragment::operator()(std::string textFragment) const
{
  std::vector<std::string> words;
  size_t pos;
  while (textFragment.find_first_of(delimiters_) == 0) {
    textFragment.erase(0, 1);
  }
  while (!textFragment.empty()) {
    pos = textFragment.find_first_of(delimiters_);
    words.push_back(textFragment.substr(0, pos));
    if (pos == std::string::npos) {
      textFragment.erase();
    } else {
      textFragment = textFragment.substr(pos);
    }
    while (textFragment.find_first_of(delimiters_) == 0) {
      textFragment.erase(0, 1);
    }
  }
  return words;
}
