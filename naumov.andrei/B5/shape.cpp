#include "shape.hpp"

Point::Point(Point&& rhs):
  Point(rhs)
{
  rhs.x = 0;
  rhs.y = 0;
}

Point::Point(int x, int y):
  x(x),
  y(y)
{}

Point& Point::operator=(Point &&rhs)
{
  x = rhs.x;
  y = rhs.y;
  rhs.x = 0;
  rhs.y = 0;
  return *this;
}

bool Point::operator==(const Point &rhs) const
{
  return (x == rhs.x && y == rhs.y);
}

bool Point::operator!=(const Point &rhs) const
{
  return !operator==(rhs);
}
