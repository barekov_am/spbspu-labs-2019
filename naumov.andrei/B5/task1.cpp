#include <iostream>
#include <functional>
#include <algorithm>
#include <iterator>

#include "parse_text_fragment.hpp"

void task1()
{
  if (std::cin.peek() == EOF) {
    return;
  }
  parse_text_fragment parser;
  std::vector<std::string> words = std::move(parser(std::string((std::istreambuf_iterator<char>(std::cin)),
    std::istreambuf_iterator<char>())));
  std::sort(words.begin(), words.end(), std::less<std::string>());
  words.erase(std::unique(words.begin(), words.end()), words.end());
  std::cout << words[0];
  for (size_t i = 1; i < words.size(); ++i) {
    std::cout << '\n' << words[i];
  }
}
