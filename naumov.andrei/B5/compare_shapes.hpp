#ifndef COMPARE_SHAPES_HPP
#define COMPARE_SHAPES_HPP

#include <functional>
#include "shape.hpp"

class compare_shapes: public std::binary_function<const Shape&, const Shape&, bool> {
public:
  bool operator()(const Shape& lhs, const Shape& rhs) const;
};

#endif //!COMPARE_SHAPES_HPP
