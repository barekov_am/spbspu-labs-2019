#ifndef PARSE_TEXT_FRAGMENT_HPP
#define PARSE_TEXT_FRAGMENT_HPP

#include <string>
#include <vector>
#include <functional>

class parse_text_fragment: public std::unary_function<std::string, std::vector<std::string>> {
public:
  parse_text_fragment();
  std::vector<std::string> operator()(std::string textFragment) const;
private:
  const std::string delimiters_;
};

#endif //!PARSE_TEXT_FRAGMENT_HPP
