#ifndef ANALYZE_SHAPE_HPP
#define ANALYZE_SHAPE_HPP

#include <functional>

#include "shape.hpp"

class analyze_shape: public std::unary_function<const Shape&, void> {
public:
  analyze_shape();
  void operator()(const Shape& shape);
  size_t getCountOfVertices() const;
  size_t getCountOfTriangles() const;
  size_t getCountOfSquares() const;
  size_t getCountOfRectangles() const;
private:
  size_t countOfVertices_, countOfTriangles_, countOfSquares_, countOfRectangles_;
};

#endif //!ANALYZE_SHAPE_HPP
