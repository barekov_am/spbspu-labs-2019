#ifndef QUEUE_WITH_PRIORITY_HPP
#define QUEUE_WITH_PRIORITY_HPP

#include <utility>
#include <algorithm>
#include <list>
#include <stdexcept>

typedef enum {
  LOW,
  NORMAL,
  HIGH
} ElementPriority;

template <typename T>
class QueueWithPriority {
public:
  using iterator = typename std::list<std::pair<T, ElementPriority> >::iterator;
  using QueueElement = std::pair<T, ElementPriority>;

  QueueWithPriority();
  ~QueueWithPriority() = default;
  void putElementToQueue(const T & element, ElementPriority priority);
  T getElementFromQueue();
  void accelerate();
  bool empty();
private:
  std::list<QueueElement> elements_;
  iterator highNormalBorder_;
  iterator normalLowBorder_;
};

template <typename T>
QueueWithPriority<T>::QueueWithPriority():
  elements_(),
  highNormalBorder_(elements_.begin()),
  normalLowBorder_(elements_.begin())
{};

template <typename T>
void QueueWithPriority<T>::putElementToQueue(const T & element, ElementPriority priority)
{
  QueueElement newElement = QueueElement(element, priority);
  switch (priority) {
    case LOW:
      elements_.push_back(newElement);
      if (normalLowBorder_ == elements_.end()) {
        normalLowBorder_ = --elements_.end();
      }
      if (highNormalBorder_ == elements_.end()) {
        highNormalBorder_ = --elements_.end();
      }
      break;
    case NORMAL: {
      iterator tempIterator = elements_.insert(normalLowBorder_, newElement);
      if (highNormalBorder_ == normalLowBorder_) {
        highNormalBorder_ = tempIterator;
      }
      break;
    }
    case HIGH:
      elements_.insert(highNormalBorder_, newElement);
      break;
  }
}

template <typename T>
T QueueWithPriority<T>::getElementFromQueue()
{
  if (elements_.empty()) {
    throw std::out_of_range("No elements in queue");
  }
  T tempValue = elements_.front().first;
  switch (elements_.front().second) {
    case LOW:
      ++highNormalBorder_;
      ++normalLowBorder_;
      break;
    case NORMAL:
      ++highNormalBorder_;
      break;
    case HIGH:
      break;
  }
  elements_.pop_front();
  return tempValue;
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  std::for_each(normalLowBorder_, elements_.end(),
                [](QueueElement & element){element.second = HIGH;});
  elements_.insert(highNormalBorder_, normalLowBorder_, elements_.end());
  normalLowBorder_ = elements_.erase(normalLowBorder_, elements_.end());
}

template <typename T>
bool QueueWithPriority<T>::empty()
{
  return elements_.empty();
}

#endif //!QUEUE_WITH_PRIORITY_HPP
