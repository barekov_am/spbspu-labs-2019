#include <iostream>
#include <list>
#include <exception>
#include <string>

void outputFirstLastList(std::list<int>::iterator first, std::list<int>::iterator last, bool isBeginning = false)
{
  if (first == last) {
    if (!isBeginning) {
      std::cout << ' ';
    }
    std::cout << *first << ' ';
    return;
  }
  if (--first == last) {
    return;
  }
  if (!isBeginning) {
    std::cout << ' ';
  }
  std::cout << *(++first) << ' ' << *last;
  outputFirstLastList(++first, --last);
}

void task2()
{
  std::list<int> list;
  std::string tempValue;
  for (int i = 0; (i < 20) && (std::cin >> tempValue); ++i ) {
    if ((std::stoi(tempValue) < 1) || (std::stoi(tempValue) > 20)) {
      throw std::invalid_argument("Invalid number");
    }
    list.push_back(std::stoi(tempValue));
  }
  if (std::cin >> tempValue) {
    throw std::invalid_argument("Invalid count of numbers");
  }
  if (tempValue.empty()) {
    return;
  }
  outputFirstLastList(list.begin(), --list.end(), true);
  std::cout << '\n';
}
