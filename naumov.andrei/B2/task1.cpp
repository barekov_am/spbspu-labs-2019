#include <string>
#include <iostream>
#include "queue-with-priority.hpp"

void task1()
{
  QueueWithPriority<std::string> queue;
  std::string command;
  while (std::cin >> command) {
    if (command.compare("add") == 0) {
      std::string priority;
      std::string data;
      if (std::cin.peek() != '\n') {
        std::cin >> priority;
        std::getline(std::cin, data, '\n');
        while ((data.find(' ') == 0) || (data.find('\t') == 0)){
          data.erase(0, 1);
        }
      }
      if (data.empty()) {
        std::cout << "<INVALID COMMAND>" << '\n';
        continue;
      }
      if (priority.compare("low") == 0) {
        queue.putElementToQueue(data, LOW);
      } else if (priority.compare("normal") == 0) {
        queue.putElementToQueue(data, NORMAL);
      } else if (priority.compare("high") == 0) {
        queue.putElementToQueue(data, HIGH);
      } else {
        std::cout << "<INVALID COMMAND>" << '\n';
      }
      continue;
    } else if (command.compare("get") == 0) {
      std::cout << (queue.empty() ? "<EMPTY>" : queue.getElementFromQueue()) << '\n';
      continue;
    } else if (command.compare("accelerate") == 0) {
      queue.accelerate();
      continue;
    } else {
      std::getline(std::cin, command, '\n');
      std::cout << "<INVALID COMMAND>" << '\n';
    }
  }
}
