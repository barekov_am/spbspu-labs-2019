#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"


namespace naumov
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix& matrix);
    Matrix(Matrix&& matrix);
    ~Matrix() = default;
    Matrix& operator=(const Matrix& matrix);
    Matrix& operator=(Matrix&& matrix);
    bool operator==(const Matrix& matrix) const;
    bool operator!=(const Matrix& matrix) const;
    std::shared_ptr<Shape>* operator[](int index) const;

    int getQuantity() const;
    void printInformation() const;
    void addShape(std::shared_ptr<Shape>& shape, bool isNewLine = false);
  protected:
    std::unique_ptr<std::shared_ptr <Shape>[]> shapes_;
    int lines_;
    std::unique_ptr<int[]> columns_;
  private:
  };
}

#endif // !MATRIX_HPP
