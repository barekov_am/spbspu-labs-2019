#include "matrix.hpp"
#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "parcel.hpp"
#include "circle.hpp"


BOOST_AUTO_TEST_SUITE(matrixTest)



BOOST_AUTO_TEST_CASE(CorrectCtor)
{
  naumov::Matrix matrix;

  BOOST_REQUIRE_EQUAL(matrix.getQuantity(), 0);
  BOOST_CHECK_THROW(matrix[0], std::logic_error);
}

BOOST_AUTO_TEST_CASE(CorrectCopyCtor)
{
  naumov::Circle circle({ 0, 0 }, 3);
  std::shared_ptr<naumov::Shape> pShape = std::make_shared<naumov::Circle>(circle);
  naumov::CompositeShape compShape(pShape);

  naumov::Matrix matrix1 = naumov::parcel(compShape);
  naumov::Matrix matrix2(matrix1);
  BOOST_REQUIRE_EQUAL(matrix1.getQuantity(), matrix2.getQuantity());
  BOOST_REQUIRE(matrix1[0][0] == matrix2[0][0]);
}

BOOST_AUTO_TEST_CASE(CorrectMoveCtor)
{
  naumov::Circle circle({ 0, 0 }, 3);
  std::shared_ptr<naumov::Shape> pShape = std::make_shared<naumov::Circle>(circle);
  naumov::CompositeShape compShape(pShape);

  naumov::Matrix matrix1 = naumov::parcel(compShape);
  naumov::Matrix matrix2(std::move(matrix1));
  BOOST_REQUIRE_EQUAL(matrix2.getQuantity(), 1);
  BOOST_REQUIRE(matrix2[0][0] == pShape);
  BOOST_REQUIRE_EQUAL(matrix1.getQuantity(), 0);
  BOOST_CHECK_THROW(matrix1[0], std::logic_error);
}

BOOST_AUTO_TEST_CASE(CorrectEqualOperators)
{
  naumov::Circle circle({ 0, 0 }, 3);
  std::shared_ptr<naumov::Shape> pShape = std::make_shared<naumov::Circle>(circle);
  naumov::CompositeShape compShape(pShape);

  naumov::Matrix matrix1 = naumov::parcel(compShape);
  BOOST_REQUIRE_EQUAL(matrix1.getQuantity(), 1);
  BOOST_REQUIRE(matrix1[0][0] == pShape);
  naumov::Matrix matrix2;
  matrix2 = std::move(matrix1);
  BOOST_REQUIRE_EQUAL(matrix2.getQuantity(), 1);
  BOOST_REQUIRE(matrix2[0][0] == pShape);
  BOOST_REQUIRE_EQUAL(matrix1.getQuantity(), 0);
  BOOST_CHECK_THROW(matrix1[0], std::logic_error);
}

BOOST_AUTO_TEST_CASE(CorrectSquareBrackets)
{
  naumov::Matrix matrix;
  BOOST_CHECK_THROW(matrix[0], std::logic_error);

  naumov::Circle circle({ 0, 0 }, 3);
  std::shared_ptr<naumov::Shape> pShape = std::make_shared<naumov::Circle>(circle);
  naumov::CompositeShape compShape(pShape);

  matrix = naumov::parcel(compShape);
  BOOST_REQUIRE(matrix[0][0] == pShape);
}

BOOST_AUTO_TEST_CASE(CorrectAddShape)
{
  naumov::Circle circle({ 0, 0 }, 3);
  std::shared_ptr<naumov::Shape> pShape = std::make_shared<naumov::Circle>(circle);
  naumov::CompositeShape compShape(pShape);

  naumov::Matrix matrix1 = naumov::parcel(compShape);
  BOOST_REQUIRE_EQUAL(matrix1.getQuantity(), 1);
  BOOST_REQUIRE(matrix1[0][0] == pShape);
}



BOOST_AUTO_TEST_SUITE_END()
