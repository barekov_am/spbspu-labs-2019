#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace naumov
{
  struct point_t
  {
    double x;
    double y;
    bool operator==(const point_t& point) const;
    bool operator!=(const point_t& point) const;
  };

  struct rectangle_t
  {
    double width;
    double height;
    point_t pos;
    bool operator==(const rectangle_t& rectangle) const;
    bool operator!=(const rectangle_t& rectangle) const;
  };
}

#endif // !BASE_TYPES_HPP
