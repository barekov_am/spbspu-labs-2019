#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace naumov
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t& centre, double width, double height);
    double getArea() const;
    rectangle_t getFrameRect() const;
    void move(const point_t& newCentre);
    void move(double dX, double dY);
    void setWidth(double newWidth);
    void setHeight(double newHeight);
    void scale(double coefficient);
    void getInformation() const;
    void rotate(const double angle);
  protected:
    point_t centre_;
    double width_;
    double height_;
    double angle_;
  private:
  };
}

#endif // !RECTANGLE_HPP
