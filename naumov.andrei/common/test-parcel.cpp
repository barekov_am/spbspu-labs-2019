#include "parcel.hpp"
#include <boost/test/auto_unit_test.hpp>
#include "matrix.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"



BOOST_AUTO_TEST_SUITE(parcelTest)



BOOST_AUTO_TEST_CASE(coorectIsPointInRectangle)
{
  naumov::rectangle_t rec = { 3, 3, { 0, 0 } };
  naumov::point_t point1 = { 0, 0 };
  naumov::point_t point2 = { 0, 5 };
  naumov::point_t point3 = { 5, 0 };
  naumov::point_t point4 = { 5, 5 };

  BOOST_REQUIRE(naumov::isPointInRectangle(rec, point1));
  BOOST_REQUIRE(!naumov::isPointInRectangle(rec, point2));
  BOOST_REQUIRE(!naumov::isPointInRectangle(rec, point3));
  BOOST_REQUIRE(!naumov::isPointInRectangle(rec, point4));
}

BOOST_AUTO_TEST_CASE(correctIntersectRectangles)
{
  naumov::rectangle_t rec1 = { 2, 2, { 0, 0 } };
  naumov::rectangle_t rec2 = { 1, 1, { 0, 0 } };
  naumov::rectangle_t rec3 = { 3, 3, { 0, 0 } };
  naumov::rectangle_t rec4 = { 2, 2, { 6, 6 } };

  BOOST_REQUIRE(naumov::intersectRectangles(rec1, rec2));
  BOOST_REQUIRE(naumov::intersectRectangles(rec1, rec3));
  BOOST_REQUIRE(!naumov::intersectRectangles(rec1, rec4));
}

BOOST_AUTO_TEST_CASE(correctParcel)
{
  naumov::Rectangle testRectangle({ 2, 2 }, 3, 3);
  naumov::Circle testCircle({ -2, -2 }, 2);
  naumov::Circle testCircle2({ 0, 0 }, 12);
  std::shared_ptr<naumov::Shape> pShape = std::make_shared<naumov::Rectangle>(testRectangle);
  std::shared_ptr<naumov::Shape> pShape2 = std::make_shared<naumov::Circle>(testCircle);
  std::shared_ptr<naumov::Shape> pShape3 = std::make_shared<naumov::Circle>(testCircle2);

  naumov::CompositeShape testCompositeShape1(pShape);
  testCompositeShape1 += pShape3;
  testCompositeShape1 += pShape2;
  naumov::CompositeShape testCompositeShape2(pShape);
  testCompositeShape2 += pShape2;
  testCompositeShape2 += pShape3;

  naumov::Matrix matrix1 = naumov::parcel(testCompositeShape1);
  naumov::Matrix matrix2 = naumov::parcel(testCompositeShape2);

  BOOST_REQUIRE(matrix1 == matrix2);
  BOOST_REQUIRE(matrix1[0][0] == pShape);
  BOOST_REQUIRE(matrix1[0][1] == pShape2);
  BOOST_REQUIRE(matrix1[1][0] == pShape3);
}



BOOST_AUTO_TEST_SUITE_END()
