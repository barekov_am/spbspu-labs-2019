#define IMPRECISION 0.000000001

#include "base-types.hpp"
#include <cmath>

bool naumov::point_t::operator==(const point_t& point) const
{
  return ((std::fabs(x - point.x) <= IMPRECISION)
    && (std::fabs(y - point.y) <= IMPRECISION));
}

bool naumov::point_t::operator!=(const point_t& point) const
{
  return !(*this == point);
}

bool naumov::rectangle_t::operator==(const rectangle_t& rectangle) const
{
  return ((std::fabs(width - rectangle.width) <= IMPRECISION)
    && (std::fabs(height - rectangle.height) <= IMPRECISION)
    && (pos == rectangle.pos));
}

bool naumov::rectangle_t::operator!=(const rectangle_t& rectangle) const
{
  return !(*this == rectangle);
}
