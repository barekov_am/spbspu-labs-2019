#include "parcel.hpp"

bool naumov::isPointInRectangle(rectangle_t rectangle, point_t point)
{
  return ((rectangle.pos.x - rectangle.width / 2 <= point.x) && (rectangle.pos.x + rectangle.width / 2 >= point.x)
    && (rectangle.pos.y - rectangle.height / 2 <= point.y) && (rectangle.pos.y + rectangle.height / 2 >= point.y));
}

bool naumov::intersectRectangles(rectangle_t rectangle1, rectangle_t rectangle2)
{
  return ((isPointInRectangle(rectangle1, { rectangle2.pos.x - rectangle2.width / 2, rectangle2.pos.y - rectangle2.height / 2 }))
    || (isPointInRectangle(rectangle1, { rectangle2.pos.x + rectangle2.width / 2, rectangle2.pos.y - rectangle2.height / 2 }))
    || (isPointInRectangle(rectangle1, { rectangle2.pos.x - rectangle2.width / 2, rectangle2.pos.y + rectangle2.height / 2 }))
    || (isPointInRectangle(rectangle1, { rectangle2.pos.x + rectangle2.width / 2, rectangle2.pos.y + rectangle2.height / 2 }))
    || (isPointInRectangle(rectangle2, { rectangle1.pos.x - rectangle1.width / 2, rectangle1.pos.y - rectangle1.height / 2 }))
    || (isPointInRectangle(rectangle2, { rectangle1.pos.x + rectangle1.width / 2, rectangle1.pos.y - rectangle1.height / 2 }))
    || (isPointInRectangle(rectangle2, { rectangle1.pos.x - rectangle1.width / 2, rectangle1.pos.y + rectangle1.height / 2 }))
    || (isPointInRectangle(rectangle2, { rectangle1.pos.x + rectangle1.width / 2, rectangle1.pos.y + rectangle1.height / 2 })));
}

naumov::Matrix naumov::parcel(CompositeShape shapes)
{
  int quantity = shapes.getNumberOfShapes();
  Matrix matrix;
  bool isNewLine = false;
  for (int i = 0; i < quantity; i++)
  {
    matrix.addShape(shapes[0], isNewLine);
    isNewLine = true;
    for (int j = 1; j < quantity - i; j++)
    {
      if (!intersectRectangles(shapes[0]->getFrameRect(), shapes[j]->getFrameRect()))
      {
        matrix.addShape(shapes[j]);
        shapes.deleteShape(j);
        i++;
        j--;
      }
    }
    shapes.deleteShape(0);
  }
  return matrix;
}
