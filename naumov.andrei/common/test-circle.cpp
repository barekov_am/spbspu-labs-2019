#define IMPRECISION 0.000000001

#include "circle.hpp"
#include <boost/test/auto_unit_test.hpp>
#include <cmath>
#include <stdexcept>



BOOST_AUTO_TEST_SUITE(circleTest)



BOOST_AUTO_TEST_SUITE(ctorTest)
BOOST_AUTO_TEST_CASE(correctCtor)
{
  naumov::point_t centre = { 1, 2 };
  double radius = 3;
  naumov::rectangle_t rect = { radius * 2, radius * 2, centre };

  naumov::Circle circle(centre, radius);
  BOOST_REQUIRE(circle.getCentre() == centre);
  BOOST_REQUIRE(circle.getFrameRect() == rect);
  BOOST_CHECK_THROW(naumov::Circle circle({ 0.11, 0.11 }, -3.4), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(getAreaTest)
BOOST_AUTO_TEST_CASE(correctAreaCalculation)
{
  naumov::point_t centre = { 1, 2 };
  double radius = 3;
  naumov::Circle circle(centre, radius);

  BOOST_CHECK_CLOSE(circle.getArea(), radius * radius * M_PI, IMPRECISION);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(getFrameRectTest)
BOOST_AUTO_TEST_CASE(correctFrameRectangleCalculation)
{
  naumov::point_t centre = { 1, 2 };
  double radius = 3;
  naumov::rectangle_t rect = { radius * 2, radius * 2, centre };
  naumov::Circle circle(centre, radius);

  BOOST_REQUIRE(circle.getFrameRect() == rect);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(moveTest)
BOOST_AUTO_TEST_CASE(correctMovingToPoint)
{
  naumov::point_t centre = { 1, 2 };
  double radius = 3;
  naumov::Circle circle(centre, radius);
  naumov::point_t newCentre = { 0, -1 };

  circle.move(newCentre);
  BOOST_REQUIRE(circle.getCentre() == newCentre);
}

BOOST_AUTO_TEST_CASE(correctMovingWithDisplacement)
{
  naumov::point_t centre = { 1, 2 };
  double radius = 3;
  naumov::Circle circle(centre, radius);
  double dX = 0.1;
  double dY = -0.1;

  circle.move(dX, dY);
  BOOST_CHECK_CLOSE(circle.getCentre().x, centre.x + dX, IMPRECISION);
  BOOST_CHECK_CLOSE(circle.getCentre().y, centre.y + dY, IMPRECISION);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(setTest)
BOOST_AUTO_TEST_CASE(correctSetingRadius)
{
  naumov::point_t centre = { 1, 2 };
  double radius = 3;
  naumov::Circle circle(centre, radius);
  double newRadius = 14.2;

  circle.setRadius(newRadius);
  BOOST_CHECK_CLOSE(circle.getFrameRect().width / 2, newRadius, IMPRECISION);
  BOOST_CHECK_CLOSE(circle.getFrameRect().height / 2, newRadius, IMPRECISION);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(scaleTest)
BOOST_AUTO_TEST_CASE(correctScaling)
{
  naumov::point_t centre = { 1, 2 };
  double radius = 3;
  naumov::Circle circle1(centre, radius);
  naumov::Circle circle2(centre, radius);
  double area = circle1.getArea();
  double coefficientMoreOne = 3;
  double coefficientLessOne = 0.5;
  double negativeCoefficient = -1.2;

  circle1.scale(coefficientMoreOne);
  BOOST_CHECK_CLOSE(circle1.getFrameRect().height / 2, radius * coefficientMoreOne, IMPRECISION);
  BOOST_CHECK_CLOSE(circle1.getFrameRect().width / 2, radius * coefficientMoreOne, IMPRECISION);
  BOOST_CHECK_CLOSE(circle1.getArea() / area, coefficientMoreOne * coefficientMoreOne, IMPRECISION);
  circle2.scale(coefficientLessOne);
  BOOST_CHECK_CLOSE(circle2.getFrameRect().height / 2, radius * coefficientLessOne, IMPRECISION);
  BOOST_CHECK_CLOSE(circle2.getFrameRect().width / 2, radius * coefficientLessOne, IMPRECISION);
  BOOST_CHECK_CLOSE(circle2.getArea() / area, coefficientLessOne * coefficientLessOne, IMPRECISION);
  BOOST_CHECK_THROW(circle1.scale(negativeCoefficient), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE_END()
