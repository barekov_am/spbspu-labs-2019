#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

naumov::Rectangle::Rectangle(const point_t& centre, double width, double height):
  centre_(centre),
  width_(width),
  height_(height),
  angle_(0)
{
  if ((width <= 0) || (height <= 0))
  {
    throw std::invalid_argument ("Invalid rectangle's parameters");
  }
}

double naumov::Rectangle::getArea() const
{
  return width_ * height_;
}

naumov::rectangle_t naumov::Rectangle::getFrameRect() const
{
  const double cosA = std::abs(std::cos(angle_ * M_PI / 180));
  const double sinA = std::abs(std::sin(angle_ * M_PI / 180));

  double frameWidth = height_ * sinA + width_ * cosA;
  double frameHeight = height_ * cosA + width_ * sinA;

  return { frameWidth, frameHeight, centre_ };
}

void naumov::Rectangle::move(const point_t& newCentre)
{
  centre_ = newCentre;
}

void naumov::Rectangle::move(double dX, double dY)
{
  centre_.x += dX;
  centre_.y += dY;
}

void naumov::Rectangle::setWidth(double newWidth)
{
  width_ = newWidth;
}

void naumov::Rectangle::setHeight(double newHeight)
{
  height_ = newHeight;
}

void naumov::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument ("Invalid coefficient");
  }
  width_ *= coefficient;
  height_ *= coefficient;
}

void naumov::Rectangle::getInformation() const
{
  std::cout << "rectangle's frame rect: w " << this->getFrameRect().width
    << " h " << this->getFrameRect().height << std::endl;
  std::cout << "rectangle's centre: x " << this->getCentre().x
    << " y " << this->getCentre().y << std::endl;
  std::cout << "rectangle's area " << this->getArea() << std::endl;
  std::cout << "rectangle's angle " << angle_ << std::endl << std::endl;
}

void naumov::Rectangle::rotate(const double angle)
{
  angle_ += angle;
}
