#include "composite-shape.hpp"
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <cmath>

naumov::CompositeShape::CompositeShape():
  arrayOfShapes_(nullptr),
  numberOfShapes_(0)
{
}

naumov::CompositeShape::CompositeShape(const CompositeShape& compositeShape):
  arrayOfShapes_(compositeShape.numberOfShapes_ != 0 ? std::make_unique<std::shared_ptr<Shape>[]>(compositeShape.numberOfShapes_) : nullptr),
  numberOfShapes_(compositeShape.numberOfShapes_)
{
  for (int i = 0; i < numberOfShapes_; i++)
  {
    arrayOfShapes_[i] = compositeShape.arrayOfShapes_[i];
  }
}

naumov::CompositeShape::CompositeShape(CompositeShape&& compositeShape):
  arrayOfShapes_(nullptr),
  numberOfShapes_(compositeShape.numberOfShapes_)
{
  arrayOfShapes_.swap(compositeShape.arrayOfShapes_);
  compositeShape.numberOfShapes_ = 0;
}

naumov::CompositeShape::CompositeShape(std::shared_ptr<Shape>& shape):
  arrayOfShapes_(std::make_unique<std::shared_ptr<Shape>[]>(1)),
  numberOfShapes_(1)
{
  arrayOfShapes_[0] = shape;
}

bool naumov::CompositeShape::operator==(const CompositeShape& compositeShape) const
{
  if (numberOfShapes_ == compositeShape.numberOfShapes_)
  {
    for (int i = 0; i < numberOfShapes_; i++)
    {
      if (arrayOfShapes_[i] != compositeShape.arrayOfShapes_[i])
      {
        return false;
      }
    }
    return 1;
  }
  return 0;
}

naumov::CompositeShape& naumov::CompositeShape::operator=(const CompositeShape& compositeShape)
{
  if (this != &compositeShape)
  {
    arrayOfShapes_ = std::make_unique<std::shared_ptr<Shape>[]>(compositeShape.numberOfShapes_);
    for (int i = 0; i < compositeShape.numberOfShapes_; i++)
    {
      arrayOfShapes_[i] = compositeShape.arrayOfShapes_[i];
    }
    numberOfShapes_ = compositeShape.numberOfShapes_;
  }
  return *this;
}

naumov::CompositeShape& naumov::CompositeShape::operator=(CompositeShape&& compositeShape)
{
  if (this != &compositeShape)
  {
    arrayOfShapes_ = std::move(compositeShape.arrayOfShapes_);
    numberOfShapes_ = compositeShape.numberOfShapes_;
    compositeShape.numberOfShapes_ = 0;
  }
  return *this;
}

naumov::CompositeShape naumov::CompositeShape::operator+(std::shared_ptr<Shape>& addedShape) const
{
  CompositeShape compositeShape(*this);
  compositeShape.addShape(addedShape);
  return compositeShape;
}

naumov::CompositeShape naumov::CompositeShape::operator-(std::shared_ptr<Shape>& shape) const
{
  CompositeShape compositeShape(*this);
  compositeShape.deleteShape(shape);
  return compositeShape;
}

naumov::CompositeShape& naumov::CompositeShape::operator+=(std::shared_ptr<Shape>& addedShape)
{
  addShape(addedShape);
  return *this;
}

naumov::CompositeShape& naumov::CompositeShape::operator-=(std::shared_ptr<Shape>& shape)
{
  deleteShape(shape);
  return *this;
}

std::shared_ptr<naumov::Shape>& naumov::CompositeShape::operator[](int index) const
{
  if (index > numberOfShapes_ - 1 || index < 0)
  {
    throw std::out_of_range("No such shape");
  }
  return arrayOfShapes_[index];
}

double naumov::CompositeShape::getArea() const
{
  double areaOfCompositeShape(0.0);
  for (int i = 0; i < numberOfShapes_; i++)
  {
    areaOfCompositeShape += arrayOfShapes_[i]->getArea();
  }
  return areaOfCompositeShape;
}

naumov::rectangle_t naumov::CompositeShape::getFrameRect() const
{
  if (numberOfShapes_ == 0)
  {
    return rectangle_t{ 0, 0, { 0, 0 } };
  }
  double maxX(arrayOfShapes_[0]->getCentre().x), maxY(arrayOfShapes_[0]->getCentre().y), minX(maxX), minY(maxY);
  for (int i = 0; i < numberOfShapes_; i++)
  {
    double xOfTemporaryShape = arrayOfShapes_[i]->getCentre().x;
    double yOfTemporaryShape = arrayOfShapes_[i]->getCentre().y;
    double displacementOfX = arrayOfShapes_[i]->getFrameRect().width / 2;
    double displacementOfY = arrayOfShapes_[i]->getFrameRect().height / 2;
    maxX = std::max(xOfTemporaryShape + displacementOfX, maxX);
    minX = std::min(xOfTemporaryShape - displacementOfX, minX);
    maxY = std::max(yOfTemporaryShape + displacementOfY, maxY);
    minY = std::min(yOfTemporaryShape - displacementOfY, minY);
  }
  return naumov::rectangle_t{ maxX - minX, maxY - minY, { (maxX + minX) / 2, (maxY + minY) / 2 } };
}

void naumov::CompositeShape::move(const point_t & newCentre)
{
  double dX(newCentre.x - getCentre().x), dY(newCentre.y - getCentre().y);
  move(dX, dY);
}

void naumov::CompositeShape::move(double dX, double dY)
{
  for (int i = 0; i < numberOfShapes_; i++)
  {
    arrayOfShapes_[i]->move(dX, dY);
  }
}

void naumov::CompositeShape::scale(double coefficient)
{
  double dX(getCentre().x * (coefficient + 1)), dY(getCentre().y * (coefficient + 1));
  for (int i = 0; i < numberOfShapes_; i++)
  {
    arrayOfShapes_[i]->scale(coefficient);
    arrayOfShapes_[i]->move({ arrayOfShapes_[i]->getCentre().x * coefficient - dX,
      arrayOfShapes_[i]->getCentre().y * coefficient - dY });
  }
}

void naumov::CompositeShape::getInformation() const
{
  std::cout << "------------------" << std::endl;
  std::cout << "composite shape's frame rect: w " << getFrameRect().width
    << " h " << getFrameRect().height << std::endl;
  std::cout << "composite shape's centre: x " << getCentre().x
    << " y " << getCentre().y << std::endl;
  std::cout << "composite shape's area " << getArea() << std::endl;
  std::cout << "number of shapes " << numberOfShapes_ << std::endl;
  for (int i = 0; i < numberOfShapes_; i++)
  {
    std::cout << "is unique: " << arrayOfShapes_[i].unique() << std::endl;
  }
}

void naumov::CompositeShape::addShape(std::shared_ptr<Shape>& newShape)
{
  if (newShape != nullptr)
  {
    std::unique_ptr<std::shared_ptr<Shape>[]> newArray = std::make_unique<std::shared_ptr<Shape>[]>(numberOfShapes_ + 1);
    for (int i = 0; i < numberOfShapes_; i++)
    {
      newArray[i].swap(arrayOfShapes_[i]);
    }
    newArray[numberOfShapes_] = newShape;
    numberOfShapes_++;
    arrayOfShapes_.swap(newArray);
  }
  else
  {
    throw std::invalid_argument("Inavlid AddShape paremeter");
  }
}

void naumov::CompositeShape::deleteShape(int index)
{
  if ((numberOfShapes_ != 0) && (index >= 0) && (index < numberOfShapes_))
  {
    for (int i = index; i < numberOfShapes_ - 1; i++)
    {
      arrayOfShapes_[i] = arrayOfShapes_[i + 1];
    }
    arrayOfShapes_[numberOfShapes_ - 1] = nullptr;
    numberOfShapes_--;
  }
  else
  {
    throw std::logic_error("Can't delete shape");
  }
}

void naumov::CompositeShape::deleteShape(std::shared_ptr<Shape>& shape)
{
  for (int i = 0; i < numberOfShapes_; i++)
  {
    if (arrayOfShapes_[i] == shape)
    {
      deleteShape(i);
      return;
    }
  }
  throw std::logic_error("Can't delete shape");
}

void naumov::CompositeShape::rotate(const double angle)
{
  const double cosA = std::abs(std::cos(angle * M_PI / 180));
  const double sinA = std::abs(std::sin(angle * M_PI / 180));
  const point_t center = getCentre();

  for (int i = 0; i < numberOfShapes_; i++)
  {
    point_t tempCenter = arrayOfShapes_[i]->getFrameRect().pos;
    const double dX = (tempCenter.x - center.x) * (cosA - 1) - (tempCenter.y - center.y)  * sinA;
    const double dY = (tempCenter.x - center.x) * sinA + (tempCenter.y - center.y)  * (cosA - 1);

    arrayOfShapes_[i]->move(dX, dY);
    arrayOfShapes_[i]->rotate(angle);
  }
}
