#define IMPRECISION 0.000000001

#include "composite-shape.hpp"
#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"



BOOST_AUTO_TEST_SUITE(CompositeShapeTest)



BOOST_AUTO_TEST_SUITE(CtorsTest)
BOOST_AUTO_TEST_CASE(CorrectCopyCtor)
{
  naumov::Rectangle rectangle({ 1, 2 }, 3, 4);
  naumov::Circle circle({ 5, 6 }, 7);
  std::shared_ptr<naumov::Shape> pShape1 = std::make_shared<naumov::Rectangle>(rectangle);
  std::shared_ptr<naumov::Shape> pShape2 = std::make_shared<naumov::Circle>(circle);
  naumov::CompositeShape compositeShape(pShape1);
  compositeShape += pShape2;

  naumov::CompositeShape compositeShape2(compositeShape);
  BOOST_REQUIRE(compositeShape2 == compositeShape);
}

BOOST_AUTO_TEST_CASE(CorrectMoveCtor)
{
  naumov::Rectangle rectangle({ 1, 1 }, 2, 2);
  naumov::Circle circle({ -1, -1 }, 1);
  std::shared_ptr<naumov::Shape> pShape1 = std::make_shared<naumov::Rectangle>(rectangle);
  std::shared_ptr<naumov::Shape> pShape2 = std::make_shared<naumov::Circle>(circle);
  naumov::CompositeShape compositeShape(pShape1);
  compositeShape += pShape2;

  naumov::CompositeShape compositeShape2(std::move(compositeShape));
  BOOST_CHECK_CLOSE(compositeShape2.getFrameRect().height,
    rectangle.getFrameRect().height + circle.getFrameRect().height, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape2.getFrameRect().width,
    rectangle.getFrameRect().width + circle.getFrameRect().width, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape2.getFrameRect().pos.x,
    rectangle.getCentre().x + circle.getCentre().x, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape2.getFrameRect().pos.y,
    rectangle.getCentre().y + circle.getCentre().y, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape2.getArea(),
    rectangle.getArea() + circle.getArea(), IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().height, 0, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().width, 0, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, 0, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, 0, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getArea(), 0, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(CorrectDefaultCtor)
{
  naumov::CompositeShape compositeShape;
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().height, 0, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().width, 0, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, 0, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, 0, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getArea(), 0, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(CorrectOneShapeCtor)
{
  naumov::Rectangle rectangle({ 1, 2 }, 3, 4);
  std::shared_ptr<naumov::Shape> pShape1 = std::make_shared<naumov::Rectangle>(rectangle);

  naumov::CompositeShape compositeShape(pShape1);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().height, rectangle.getFrameRect().height, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().width, rectangle.getFrameRect().width, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, rectangle.getCentre().x, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, rectangle.getCentre().y, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getArea(), rectangle.getArea(), IMPRECISION);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(OperatorsTest)
BOOST_AUTO_TEST_CASE(CorrectPlusAndPlusEquals)
{
  naumov::Rectangle rectangle({ 1, 2 }, 3, 4);
  naumov::CompositeShape compositeShape;
  std::shared_ptr<naumov::Shape> pShape1 = std::make_shared<naumov::Rectangle>(rectangle);

  compositeShape = compositeShape + pShape1;
  BOOST_CHECK_EQUAL(compositeShape[0], pShape1);

  naumov::Circle circle({ 5, 6 }, 7);
  std::shared_ptr<naumov::Shape> pShape2 = std::make_shared<naumov::Circle>(circle);
  compositeShape += pShape2;
  BOOST_CHECK_EQUAL(compositeShape[1], pShape2);
}

BOOST_AUTO_TEST_CASE(CorrectMinusAndMinusEquals)
{
  naumov::Rectangle rectangle({ 1, 2 }, 3, 4);
  naumov::Circle circle({ 5, 6 }, 7);
  std::shared_ptr<naumov::Shape> pShape1 = std::make_shared<naumov::Rectangle>(rectangle);
  std::shared_ptr<naumov::Shape> pShape2 = std::make_shared<naumov::Circle>(circle);
  naumov::CompositeShape compositeShape(pShape1);
  compositeShape += pShape2;


  compositeShape = compositeShape - pShape1;
  BOOST_CHECK_EQUAL(compositeShape[0], pShape2);

  compositeShape -= pShape2;
  BOOST_CHECK_THROW(compositeShape[0], std::out_of_range);

  naumov::CompositeShape compositeShape2(compositeShape);
  BOOST_REQUIRE(compositeShape2 == compositeShape);
}

BOOST_AUTO_TEST_CASE(CorrectSquareBrackets)
{
  naumov::Rectangle rectangle({ 1, 2 }, 3, 4);
  std::shared_ptr<naumov::Shape> pShape1 = std::make_shared<naumov::Rectangle>(rectangle);
  naumov::CompositeShape compositeShape(pShape1);


  BOOST_CHECK_CLOSE(compositeShape[0]->getFrameRect().pos.x, rectangle.getFrameRect().pos.x, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape[0]->getFrameRect().pos.y, rectangle.getFrameRect().pos.y, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape[0]->getFrameRect().width, rectangle.getFrameRect().width, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape[0]->getFrameRect().height, rectangle.getFrameRect().height, IMPRECISION);

  BOOST_CHECK_THROW(compositeShape[1], std::out_of_range);
  BOOST_CHECK_THROW(compositeShape[-1], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(CorrectCopyAndMoveAssigment)
{
  naumov::Rectangle rectangle({ 1, 2 }, 3, 4);
  naumov::Circle circle({ 5, 6 }, 7);
  std::shared_ptr<naumov::Shape> pShape1 = std::make_shared<naumov::Rectangle>(rectangle);
  std::shared_ptr<naumov::Shape> pShape2 = std::make_shared<naumov::Circle>(circle);
  naumov::CompositeShape compositeShape(pShape1);
  compositeShape += pShape2;
  naumov::CompositeShape compositeShape2;

  compositeShape2 = compositeShape;
  BOOST_REQUIRE(compositeShape2 == compositeShape);

  naumov::CompositeShape compositeShape3;
  compositeShape3 = std::move(compositeShape2);
  BOOST_REQUIRE(compositeShape3 == compositeShape);
  BOOST_CHECK_CLOSE(compositeShape2.getFrameRect().height, 0, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape2.getFrameRect().width, 0, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape2.getFrameRect().pos.x, 0, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape2.getFrameRect().pos.y, 0, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape2.getArea(), 0, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(CorrectEqual)
{
  naumov::Rectangle rectangle({ 1, 2 }, 3, 4);
  naumov::Circle circle({ 5, 6 }, 7);
  std::shared_ptr<naumov::Shape> pShape1 = std::make_shared<naumov::Rectangle>(rectangle);
  std::shared_ptr<naumov::Shape> pShape2 = std::make_shared<naumov::Circle>(circle);
  naumov::CompositeShape compositeShape(pShape1);
  compositeShape += pShape2;
  naumov::CompositeShape compositeShape2(pShape1);
  compositeShape2 += pShape2;
  naumov::CompositeShape compositeShape3(compositeShape);

  BOOST_REQUIRE(compositeShape3 == compositeShape);
  BOOST_REQUIRE(compositeShape2 == compositeShape);
  BOOST_REQUIRE(compositeShape3 == compositeShape2);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(getAreaTest)
BOOST_AUTO_TEST_CASE(correctAreaCalculation)
{
  naumov::Rectangle rectangle({ 1, 2 }, 3, 4);
  naumov::Circle circle({ 5, 6 }, 7);
  std::shared_ptr<naumov::Shape> pShape1 = std::make_shared<naumov::Rectangle>(rectangle);
  std::shared_ptr<naumov::Shape> pShape2 = std::make_shared<naumov::Circle>(circle);
  naumov::CompositeShape compositeShape(pShape1);
  compositeShape += pShape2;

  BOOST_CHECK_CLOSE(compositeShape.getArea(),
    rectangle.getArea() + circle.getArea(), IMPRECISION);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(getFrameRectTest)
BOOST_AUTO_TEST_CASE(correctFrameRectangleCalculation)
{
  naumov::Rectangle rectangle({ 1, 1 }, 2, 2);
  naumov::Circle circle({ -1, -1 }, 1);
  std::shared_ptr<naumov::Shape> pShape1 = std::make_shared<naumov::Rectangle>(rectangle);
  std::shared_ptr<naumov::Shape> pShape2 = std::make_shared<naumov::Circle>(circle);
  naumov::CompositeShape compositeShape(pShape1);
  compositeShape += pShape2;

  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().height,
    rectangle.getFrameRect().height + circle.getFrameRect().height, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().width,
    rectangle.getFrameRect().width + circle.getFrameRect().width, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x,
    rectangle.getCentre().x + circle.getCentre().x, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y,
    rectangle.getCentre().y + circle.getCentre().y, IMPRECISION);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(moveTest)
BOOST_AUTO_TEST_CASE(correctMovingToPoint)
{
  naumov::Rectangle rectangle({ 1, 1 }, 2, 2);
  naumov::Circle circle({ -1, -1 }, 1);
  naumov::Rectangle rectangle2({ 1, 1 }, 2, 2);
  naumov::Circle circle2({ -1, -1 }, 1);
  std::shared_ptr<naumov::Shape> pShape1 = std::make_shared<naumov::Rectangle>(rectangle);
  std::shared_ptr<naumov::Shape> pShape2 = std::make_shared<naumov::Circle>(circle);
  naumov::CompositeShape compositeShape(pShape1);
  compositeShape += pShape2;
  naumov::point_t newCentre{ 3, 3 };

  compositeShape.move(newCentre);
  BOOST_CHECK_CLOSE(compositeShape[0]->getCentre().x, newCentre.x + rectangle2.getCentre().x, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape[0]->getCentre().y, newCentre.y + rectangle2.getCentre().y, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape[1]->getCentre().x, newCentre.x + circle2.getCentre().x, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape[1]->getCentre().y, newCentre.y + circle2.getCentre().y, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(correctMovingWithDisplacement)
{
  naumov::Rectangle rectangle({ 1, 1 }, 2, 2);
  naumov::Circle circle({ -1, -1 }, 1);
  naumov::Rectangle rectangle2({ 1, 1 }, 2, 2);
  naumov::Circle circle2({ -1, -1 }, 1);
  std::shared_ptr<naumov::Shape> pShape1 = std::make_shared<naumov::Rectangle>(rectangle);
  std::shared_ptr<naumov::Shape> pShape2 = std::make_shared<naumov::Circle>(circle);
  naumov::CompositeShape compositeShape(pShape1);
  compositeShape += pShape2;

  compositeShape.move(3, 3);
  BOOST_CHECK_CLOSE(compositeShape[0]->getCentre().x, 3 + rectangle2.getCentre().x, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape[0]->getCentre().y, 3 + rectangle2.getCentre().y, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape[1]->getCentre().x, 3 + circle2.getCentre().x, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape[1]->getCentre().y, 3 + circle2.getCentre().y, IMPRECISION);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(scaleTest)
BOOST_AUTO_TEST_CASE(correctScaling)
{
  naumov::Rectangle rectangle1({ 1, 2 }, 3, 4);
  naumov::Circle circle1({ 5, 6 }, 7);
  std::shared_ptr<naumov::Shape> pShape1 = std::make_shared<naumov::Rectangle>(rectangle1);
  std::shared_ptr<naumov::Shape> pShape2 = std::make_shared<naumov::Circle>(circle1);
  naumov::CompositeShape compositeShape1(pShape1);
  compositeShape1 += pShape2;
  naumov::Rectangle rectangle2({ 1, 2 }, 3, 4);
  naumov::Circle circle2({ 5, 6 }, 7);
  std::shared_ptr<naumov::Shape> pShape3 = std::make_shared<naumov::Rectangle>(rectangle2);
  std::shared_ptr<naumov::Shape> pShape4 = std::make_shared<naumov::Circle>(circle2);
  naumov::CompositeShape compositeShape2(pShape3);
  compositeShape2 += pShape4;
  double area1 = compositeShape1.getArea();
  double area2 = compositeShape2.getArea();
  double coefficientMoreOne(4);
  double coefficientLessOne(0.7);
  double negativeCoefficient(-1);

  compositeShape1.scale(coefficientMoreOne);
  BOOST_CHECK_CLOSE(compositeShape1.getArea() / area1, coefficientMoreOne * coefficientMoreOne, IMPRECISION);
  compositeShape2.scale(coefficientLessOne);
  BOOST_CHECK_CLOSE(compositeShape2.getArea() / area2, coefficientLessOne * coefficientLessOne, IMPRECISION);
  BOOST_CHECK_THROW(compositeShape1.scale(negativeCoefficient), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(addShapeTest)
BOOST_AUTO_TEST_CASE(correctAddingShape)
{
  naumov::Circle circle({ 5, 6 }, 7);
  naumov::CompositeShape compositeShape;
  std::shared_ptr<naumov::Shape> pShape = std::make_shared<naumov::Circle>(circle);

  compositeShape.addShape(pShape);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().height, circle.getFrameRect().height, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().width, circle.getFrameRect().width, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, circle.getCentre().x, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, circle.getCentre().y, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getArea(), circle.getArea(), IMPRECISION);

  std::shared_ptr<naumov::Shape> pShape2 = nullptr;
  BOOST_CHECK_THROW(compositeShape.addShape(pShape2), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(deleteShapeTest)
BOOST_AUTO_TEST_CASE(correctDeletingShape)
{
  naumov::Rectangle rectangle({ 1, 1 }, 2, 2);
  naumov::Circle circle({ -1, -1 }, 1);
  std::shared_ptr<naumov::Shape> pShape1 = std::make_shared<naumov::Rectangle>(rectangle);
  std::shared_ptr<naumov::Shape> pShape2 = std::make_shared<naumov::Circle>(circle);
  naumov::CompositeShape compositeShape(pShape1);
  compositeShape += pShape2;
  compositeShape.deleteShape(0);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().height, circle.getFrameRect().height, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().width, circle.getFrameRect().width, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.x, circle.getCentre().x, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, circle.getCentre().y, IMPRECISION);
  BOOST_CHECK_CLOSE(compositeShape.getArea(), circle.getArea(), IMPRECISION);

  BOOST_CHECK_THROW(compositeShape.deleteShape(1), std::logic_error);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(rotateTest)
BOOST_AUTO_TEST_CASE(correctRotate)
{
  naumov::Rectangle rectangle({ 20, 0 }, 10, 10);
  naumov::Rectangle rectangle2({ -20, 0 }, 10, 10);
  std::shared_ptr<naumov::Shape> pShape1 = std::make_shared<naumov::Rectangle>(rectangle);
  std::shared_ptr<naumov::Shape> pShape2 = std::make_shared<naumov::Rectangle>(rectangle2);
  naumov::CompositeShape compositeShape(pShape1);
  compositeShape += pShape2;
  naumov::rectangle_t frameRect = { 50, 10, { 0, 0 } };
  naumov::rectangle_t newFrameRect = { 10, 50, { 0, 0 } };

  compositeShape.rotate(0);
  BOOST_REQUIRE(compositeShape.getFrameRect() == frameRect);
  compositeShape.rotate(180);
  BOOST_REQUIRE(compositeShape.getFrameRect() == frameRect);
  compositeShape.rotate(90);
  BOOST_REQUIRE(compositeShape.getFrameRect() == newFrameRect);
  compositeShape.rotate(-90);
  BOOST_REQUIRE(compositeShape.getFrameRect() == frameRect);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE_END()
