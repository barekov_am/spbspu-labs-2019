#include "matrix.hpp"
#include <iostream>
#include <stdexcept>

naumov::Matrix::Matrix():
  shapes_(nullptr),
  lines_(0),
  columns_(nullptr)
{
}

naumov::Matrix::Matrix(const Matrix& matrix):
  shapes_(nullptr),
  lines_(matrix.lines_),
  columns_(std::make_unique<int[]>(lines_ + 1))
{
  int quantity = matrix.getQuantity();
  shapes_ = std::make_unique<std::shared_ptr<Shape>[]>(quantity);
  for (int i = 0; i < quantity; i++)
  {
    shapes_[i] = matrix.shapes_[i];
  }
  for (int i = 0; i < lines_ + 1; i++)
  {
    columns_[i] = matrix.columns_[i];
  }
}

naumov::Matrix::Matrix(Matrix&& matrix):
  shapes_(nullptr),
  lines_(matrix.lines_),
  columns_(nullptr)
{
  shapes_.swap(matrix.shapes_);
  columns_.swap(matrix.columns_);
  matrix.lines_ = 0;
}

naumov::Matrix& naumov::Matrix::operator=(const Matrix& matrix)
{
  if (this != &matrix)
  {
    lines_ = matrix.lines_;
    int quantity = matrix.getQuantity();
    shapes_ = std::make_unique<std::shared_ptr<naumov::Shape>[]>(quantity);
    for (int i = 0; i < quantity; i++)
    {
      shapes_[i] = matrix.shapes_[i];
    }
    columns_ = std::make_unique<int[]>(lines_ + 1);
    for (int i = 0; i < lines_ + 1; i++)
    {
      columns_[i] = matrix.columns_[i];
    }
  }
  return *this;
}

naumov::Matrix& naumov::Matrix::operator=(Matrix&& matrix)
{
  if (this != &matrix)
  {
    shapes_ = nullptr;
    columns_ = nullptr;
    lines_ = matrix.lines_;
    shapes_.swap(matrix.shapes_);
    columns_.swap(matrix.columns_);
    matrix.lines_ = 0;
  }
  return *this;
}

bool naumov::Matrix::operator==(const Matrix& matrix) const
{
  int quantity = getQuantity();
  if (lines_ == matrix.lines_)
  {
    for (int i = 0; i < lines_ + 1; i++)
    {
      if (columns_[i] != matrix.columns_[i])
      {
        return false;
      }
    }
    for (int i = 0; i < quantity; i++)
    {
      if (shapes_[i].get() != matrix.shapes_[i].get())
      {
        return false;
      }
    }
    return true;
  }
  return false;
}

bool naumov::Matrix::operator!=(const Matrix& matrix) const
{
  return !(*this == matrix);
}

void naumov::Matrix::printInformation() const
{
  std::cout << "--------------------" << std::endl;
  int quantity = 0;
  for (int i = 0; i < lines_ + 1; i++)
  {
    std::cout <<"line " << i << std::endl;
    for (int j = 0; j < columns_[i]; j++)
    {
      std::cout << "is unique: " <<shapes_[quantity + j].unique() << std::endl;
      shapes_[quantity + j]->getInformation();
    }
    quantity += columns_[i];
  }
}

std::shared_ptr<naumov::Shape>* naumov::Matrix::operator[](int index) const
{
  if ((index < 0) || (index > lines_))
  {
    throw std::out_of_range("Index out of range");
  }
  if (columns_ != nullptr)
  {
    int quantity = 0;
    for (int i = 0; i < index; i++)
    {
      quantity += columns_[i];
    }
    return &shapes_[quantity];
  }
  throw std::logic_error("Isn't member of matrix");
}

void naumov::Matrix::addShape(std::shared_ptr<Shape>& shape, bool isNewLine)
{
  if (isNewLine)
  {
    lines_++;
    std::unique_ptr<int[]> tempColumns = std::make_unique<int[]>(lines_ + 1);
    for (int j = 0; j < lines_; j++)
    {
      tempColumns[j] = columns_[j];
    }
    tempColumns[lines_] = 0;
    columns_.swap(tempColumns);
  }
  if (columns_ == nullptr)
  {
    columns_ = std::make_unique<int[]>(1);
    columns_[0] = 0;
  }
  int quantity = getQuantity();
  std::unique_ptr<std::shared_ptr <Shape>[]> tempShapes = std::make_unique<std::shared_ptr<naumov::Shape>[]>(quantity + 1);
  for (int i = 0; i < quantity; i++)
  {
    tempShapes[i] = shapes_[i];
  }
  tempShapes[quantity] = shape;
  shapes_.swap(tempShapes);
  columns_[lines_]++;
}

int naumov::Matrix::getQuantity() const
{
  int quantity = 0;
  if (columns_ != nullptr)
  {
    for (int i = 0; i < lines_ + 1; i++)
    {
      quantity += columns_[i];
    }
  }
  return quantity;
}
