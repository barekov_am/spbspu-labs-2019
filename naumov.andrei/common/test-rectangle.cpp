#define IMPRECISION 0.000000001

#include "rectangle.hpp"
#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>



BOOST_AUTO_TEST_SUITE(rectangleTest)



BOOST_AUTO_TEST_SUITE(ctorTest)
BOOST_AUTO_TEST_CASE(correctCtor)
{
  naumov::rectangle_t rect = { 3, 4, { 1, 2 } };

  naumov::Rectangle rectangle(rect.pos, rect.width, rect.height);
  BOOST_REQUIRE(rectangle.getCentre() == rect.pos);
  BOOST_REQUIRE(rectangle.getFrameRect() == rect);
  BOOST_CHECK_THROW(naumov::Rectangle rectangle({ 0.11, 0.11 }, -3.4, 6), std::invalid_argument);
  BOOST_CHECK_THROW(naumov::Rectangle rectangle({ 0.11, 0.11 }, 3.4, -6), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(getAreaTest)
BOOST_AUTO_TEST_CASE(correctAreaCalculation)
{
  naumov::rectangle_t rect = { 3, 4, { 1, 2 } };
  naumov::Rectangle rectangle(rect.pos, rect.width, rect.height);

  BOOST_CHECK_CLOSE(rectangle.getArea(), rect.width * rect.height, IMPRECISION);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(getFrameRectTest)
BOOST_AUTO_TEST_CASE(correctFrameRectangleCalculation)
{
  naumov::rectangle_t rect = { 3, 4, { 1, 2 } };
  naumov::Rectangle rectangle(rect.pos, rect.width, rect.height);

  BOOST_REQUIRE(rectangle.getFrameRect() == rect);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(moveTest)
BOOST_AUTO_TEST_CASE(correctMovingToPoint)
{
  naumov::rectangle_t rect = { 3, 4, { 1, 2 } };
  naumov::Rectangle rectangle(rect.pos, rect.width, rect.height);
  naumov::point_t newCentre = { 0, -1 };

  rectangle.move(newCentre);
  BOOST_REQUIRE(rectangle.getCentre() == newCentre);
}

BOOST_AUTO_TEST_CASE(correctMovingWithDisplacement)
{
  naumov::rectangle_t rect = { 3, 4, { 1, 2 } };
  naumov::Rectangle rectangle(rect.pos, rect.width, rect.height);
  double dX = 0.1;
  double dY = -0.1;

  rectangle.move(dX, dY);
  BOOST_CHECK_CLOSE(rectangle.getCentre().x, rect.pos.x + dX, IMPRECISION);
  BOOST_CHECK_CLOSE(rectangle.getCentre().y, rect.pos.y + dY, IMPRECISION);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(setTest)
BOOST_AUTO_TEST_CASE(correctSetingWidth)
{
  naumov::rectangle_t rect = { 3, 4, { 1, 2 } };
  naumov::Rectangle rectangle(rect.pos, rect.width, rect.height);
  double newWidth = 1.11;

  rectangle.setWidth(newWidth);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, newWidth, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(correctSetingHeight)
{
  naumov::rectangle_t rect = { 3, 4, { 1, 2 } };
  naumov::Rectangle rectangle(rect.pos, rect.width, rect.height);
  double newHeight = 1.11;

  rectangle.setHeight(newHeight);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, newHeight, IMPRECISION);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(scaleTest)
BOOST_AUTO_TEST_CASE(correctScaling)
{
  naumov::rectangle_t rect = { 3, 4, { 1, 2 } };
  naumov::Rectangle rectangle1(rect.pos, rect.width, rect.height);
  naumov::Rectangle rectangle2(rect.pos, rect.width, rect.height);
  double area = rectangle1.getArea();
  double coefficientMoreOne = 3.1;
  double coefficientLessOne = 0.5;
  double negativeCoefficient = -1.2;

  rectangle1.scale(coefficientMoreOne);
  BOOST_CHECK_CLOSE(rectangle1.getFrameRect().height, rect.height * coefficientMoreOne, IMPRECISION);
  BOOST_CHECK_CLOSE(rectangle1.getFrameRect().width, rect.width * coefficientMoreOne, IMPRECISION);
  BOOST_CHECK_CLOSE(rectangle1.getArea() / area, coefficientMoreOne * coefficientMoreOne, IMPRECISION);
  rectangle2.scale(coefficientLessOne);
  BOOST_CHECK_CLOSE(rectangle2.getFrameRect().height, rect.height * coefficientLessOne, IMPRECISION);
  BOOST_CHECK_CLOSE(rectangle2.getFrameRect().width, rect.width * coefficientLessOne, IMPRECISION);
  BOOST_CHECK_CLOSE(rectangle2.getArea() / area, coefficientLessOne * coefficientLessOne, IMPRECISION);
  BOOST_CHECK_THROW(rectangle1.scale(negativeCoefficient), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(rotateTest)
BOOST_AUTO_TEST_CASE(correctRotate)
{
  naumov::Rectangle rectangle({ 20, 0 }, 10, 20);
  naumov::rectangle_t frameRect = { 10, 20, { 20, 0 } };
  naumov::rectangle_t newFrameRect = { 20, 10, { 20, 0 } };

  rectangle.rotate(0);
  BOOST_REQUIRE(rectangle.getFrameRect() == frameRect);
  rectangle.rotate(180);
  BOOST_REQUIRE(rectangle.getFrameRect() == frameRect);
  rectangle.rotate(90);
  BOOST_REQUIRE(rectangle.getFrameRect() == newFrameRect);
  rectangle.rotate(-90);
  BOOST_REQUIRE(rectangle.getFrameRect() == frameRect);
}
BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE_END()
