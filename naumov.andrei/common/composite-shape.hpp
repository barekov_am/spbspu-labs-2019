#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace naumov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape& compositeShape);
    CompositeShape(CompositeShape&& compositeShape);
    CompositeShape(std::shared_ptr<Shape>& shape);
    ~CompositeShape() = default;

    bool operator==(const CompositeShape& compositeShape) const;
    CompositeShape& operator=(const CompositeShape& compositeShape);
    CompositeShape& operator=(CompositeShape&& compositeShape);
    CompositeShape operator+(std::shared_ptr<Shape>& addedShape) const;
    CompositeShape operator-(std::shared_ptr<Shape>& shape) const;
    CompositeShape& operator+=(std::shared_ptr<Shape>& addedShape);
    CompositeShape& operator-=(std::shared_ptr<Shape>& shape);
    std::shared_ptr<Shape>& operator[](int index) const;

    double getArea() const;
    rectangle_t getFrameRect() const;
    void move(const point_t& newCentre);
    void move(double dX, double dY);
    void scale(double coefficient);
    void getInformation() const;

    void addShape(std::shared_ptr<Shape>& newShape);
    void deleteShape(int index);
    void deleteShape(std::shared_ptr<Shape>& shape);
    int getNumberOfShapes() const
    {
      return numberOfShapes_;
    }
    void rotate(const double angle);
  protected:
    std::unique_ptr<std::shared_ptr<Shape>[]> arrayOfShapes_;
    int numberOfShapes_;
  private:
  };
}

#endif // !COMPOSITE_SHAPE_HPP
