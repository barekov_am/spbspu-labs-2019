#ifndef PARCEL_HPP
#define PARCEL_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace naumov
{
  bool isPointInRectangle(rectangle_t rectangle, point_t point);
  bool intersectRectangles(rectangle_t rectangle1, rectangle_t rectangle2);
  naumov::Matrix parcel(CompositeShape shapes);
}


#endif // !PARCEL_HPP
