#include <memory>
#include <exception>
#include <vector>
#include "output.hpp"
#include "sort.hpp"

void fillRandom(double * array, int size)
{
  for (int i = 0; i < size; ++i)
  {
    array[i] = (- 10 + std::rand() % 21) / 10.0;
  }
}

void task4(int size, bool isAscending = true)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Invalid size of array");
  }
  std::unique_ptr<double[]> array = std::make_unique<double[]>(size);
  fillRandom(array.get(), size);
  std::vector<double> resultArray;
  for (int i = 0; i < size; ++i)
  {
    resultArray.push_back(array[i]);
  }
  output<std::vector<double> >(resultArray);
  if (isAscending)
  {
    sort<std::vector<double>, vector_access_policy_t, std::greater>(resultArray);
  }
  else
  {
    sort<std::vector<double>, vector_access_policy_t, std::less>(resultArray);
  }
  output<std::vector<double> >(resultArray);
}
