#ifndef OUTPUT_HPP
#define OUTPUT_HPP

#include <iostream>
#include "container_access_policy.hpp"

template <typename T>
void output(const T & container, char separator = ' ')
{
  for (typename T::const_iterator i = container.begin() ; i != container.end() ; ++i)
  {
    if (separator == '\0')
    {
      std::cout << *i;
    }
    else
    {
      std::cout << *i << separator;
    }
  }
  if (separator != '\0')
  {
    std::cout << '\n';
  }
}

#endif // !OUTPUT_HPP
