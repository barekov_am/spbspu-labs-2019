#ifndef SORT_HPP
#define SORT_HPP

#include "container_access_policy.hpp"

template <typename T,
          template <typename> typename Access,
          template <typename> typename Comparator>
void sort(T & container)
{
  Comparator<typename T::value_type> comparator;
  using index = typename Access<T>::index_type;
  for (index i = Access<T>::begin(container); i != Access<T>::end(container); ++i)
  {
    for (index j = i; j != Access<T>::end(container); ++j )
    {
      if (comparator(Access<T>::access(container, i), Access<T>::access(container, j)))
      {
        std::swap(Access<T>::access(container, i), Access<T>::access(container, j));
      }
    }
  }
}

#endif // !SORT_HPP
