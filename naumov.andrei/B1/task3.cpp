#include <iostream>
#include <vector>
#include "output.hpp"

void task3()
{
  int tempValue = 0;
  std::vector<int> container;
  while (std::cin >> tempValue)
  {
    if (tempValue == 0)
    {
      break;
    }
    container.push_back(tempValue);
  }
  if (((!std::cin.eof()) && (std::cin.fail())) || (tempValue != 0))
  {
    throw std::runtime_error("Invalid data");
  }
  if (container.empty())
  {
    return;
  }
  auto tempIterator = container.begin();
  switch (container.back())
  {
    case 1:
      while (tempIterator != container.end())
      {
        if (*tempIterator % 2 == 0)
        {
          tempIterator = container.erase(tempIterator);
        }
        else
        {
          ++tempIterator;
        }
      }
      break;
    case 2:
      while (tempIterator != container.end())
      {
        if (*tempIterator % 3 == 0)
        {
          tempIterator = container.insert(++tempIterator, 3, 1);
          ++tempIterator;
          ++tempIterator;
        }
        ++tempIterator;
      }
      break;
  }
  output<std::vector<int> >(container);
}
