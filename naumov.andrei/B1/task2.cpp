#include <vector>
#include <fstream>
#include <exception>
#include <memory>
#include <new>
#include "output.hpp"

void task2(const char * fileName)
{
  std::ifstream source(fileName);
  if (!source.is_open())
  {
    throw std::invalid_argument("Invalid file");
  }
  int capacity = 10;
  char * tempArray = static_cast<char *>(std::calloc(capacity, 1));
  if (tempArray == nullptr)
  {
    throw std::bad_alloc();
  }
  std::unique_ptr<char[], decltype(free)*> array(tempArray, free);
  int i = 0;
  while (!source.eof())
  {
    source.read(&array[i], capacity - i);
    i += source.gcount();
    int tempCapacity = static_cast<int>(capacity * 1.8);
    char * tempArray = static_cast<char *>(std::realloc(array.get(), tempCapacity));
    if (tempArray == nullptr)
    {
      throw std::bad_alloc();
    }
    array.release();
    array.reset(tempArray);
    capacity = tempCapacity;
  }
  source.close();
  std::vector<char> resultVector(&array[0], &array[i]);
  output<std::vector<char> >(resultVector, '\0');
}
