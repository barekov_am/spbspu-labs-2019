#ifndef CONTAINER_ACCESS_POLICY_HPP
#define CONTAINER_ACCESS_POLICY_HPP

#include <vector>
#include <forward_list>

template <typename T>
struct container_access_policy_t
{
  using index_type = int;
  static typename T::reference access(T & container, index_type index)
  {
    return container[index];
  }
  static index_type begin(T &)
  {
    return 0;
  }
  static index_type end(T & container)
  {
    return container.size();
  }
};

template < typename T >
struct vector_access_policy_t : container_access_policy_t<T>
{
  static void insert(T & container, typename T::value_type & value)
  {
    container.push_back(value);
  }
};

template < typename T >
struct vector_at_access_policy_t : vector_access_policy_t<T>
{
  using index_type = int;
  static typename T::reference access(T & container, index_type index)
  {
    return container.at(index);
  }
};

template < typename T >
struct forward_list_access_policy_t : container_access_policy_t<T>
{
  using index_type = typename T::iterator;
  static typename T::reference access(T &, index_type index)
  {
    return *index;
  }
  static index_type begin(T & container)
  {
    return container.begin();
  }
  static index_type end(T & container)
  {
    return container.end();
  }
  static void insert(T & container, typename T::value_type & value)
  {
    container.insert_after(--container.end(), value);
  }
};

#endif // !CONTAINER_ACCESS_POLICY_HPP
