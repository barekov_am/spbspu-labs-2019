#include <functional>
#include "sort.hpp"
#include "output.hpp"

void task1(bool isAscending = true)
{
  std::vector<int> vectorBrackets;
  std::vector<int> vectorAt;
  std::forward_list<int> forwardList;
  int tempValue = 0;
  while (std::cin >> tempValue)
  {
    vectorBrackets.push_back(tempValue);
  }
  if ((!std::cin.eof()) && (std::cin.fail()))
  {
    throw std::runtime_error("Invalid data");
  }
  for (int i = 0; i < static_cast<int>(vectorBrackets.size()); ++i)
  {
    vectorAt.push_back(vectorBrackets[i]);
    forwardList.push_front(vectorBrackets[vectorBrackets.size() - 1 - i]);
  }
  if (isAscending)
  {
    sort<std::vector<int>, vector_access_policy_t, std::greater>(vectorBrackets);
    sort<std::vector<int>, vector_at_access_policy_t, std::greater>(vectorAt);
    sort<std::forward_list<int>, forward_list_access_policy_t, std::greater>(forwardList);
  }
  else
  {
    sort<std::vector<int>, vector_access_policy_t, std::less>(vectorBrackets);
    sort<std::vector<int>, vector_at_access_policy_t, std::less>(vectorAt);
    sort<std::forward_list<int>, forward_list_access_policy_t, std::less>(forwardList);
  }
  output<std::vector<int> >(vectorBrackets);
  output<std::vector<int> >(vectorAt);
  output<std::forward_list<int> >(forwardList);
}
