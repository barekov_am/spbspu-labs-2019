#include <iostream>
#include <exception>
#include <cstring>
#include <ctime>

void task1(bool = true);
void task2(const char *);
void task3();
void task4(int, bool = true);

int main(int argc, char * argv[])
{
  try
  {
    if ((argc <= 1) || (argc >= 5))
    {
      std::cerr << "Invalid count of arguments" << '\n';
      return 1;
    }
    std::srand(std::time(nullptr));
    switch (std::stoi(argv[1]))
    {
      case 1:
        if (argc != 3)
        {
          std::cerr << "Invalid count of arguments" << '\n';
          return 1;
        }
        if (static_cast<std::string>(argv[2]) == "ascending")
        {
          task1(true);
        }
        else if (static_cast<std::string>(argv[2]) == "descending")
        {
          task1(false);
        }
        else
        {
          std::cerr << "No such mode" << '\n';
          return 1;
        }
        break;
      case 2:
        if (argc != 3)
        {
          std::cerr << "Invalid count of arguments" << '\n';
          return 1;
        }
        task2(argv[2]);
        break;
      case 3:
        if (argc > 2)
        {
          std::cerr << "Invalid count of arguments" << '\n';
          return 1;
        }
        task3();
        break;
      case 4:
        if (argc != 4)
        {
          std::cerr << "Invalid count of arguments" << '\n';
          return 1;
        }
        if (static_cast<std::string>(argv[2]) == "ascending")
        {
          task4(std::stoi(argv[3]), true);
        }
        else if (static_cast<std::string>(argv[2]) == "descending")
        {
          task4(std::stoi(argv[3]), false);
        }
        else
        {
          std::cerr << "No such mode" << '\n';
          return 1;
        }
        break;
      default:
        std::cerr << "No such task" << '\n';
        return 1;
    }
  }
  catch (std::bad_alloc &)
  {
    return 2;
  }
  catch (std::exception & error)
  {
    std::cerr << error.what() << '\n';
    return 1;
  }
  return 0;
}
