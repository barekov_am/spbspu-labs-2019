#include "get_statistics.hpp"

get_statistics::get_statistics():
  max_(0),
  min_(0),
  mean_(0),
  positive_(0),
  negative_(0),
  oddSum_(0),
  evenSum_(0),
  firstLastEqual_(false),
  first_(0),
  zero_(0)
{}

void get_statistics::operator()(const int value)
{
  if (empty()) {
    first_ = value;
    max_ = value;
    min_ = value;
  }
  if (value > max_) {
    max_ = value;
  }
  if (value < min_) {
    min_ = value;
  }
  if (first_ == value) {
    firstLastEqual_ = true;
  } else {
    firstLastEqual_ = false;
  }
  if (value == 0) {
    ++zero_;
  } else if (value > 0) {
    ++positive_;
  } else {
    ++negative_;
  }
  if (value % 2 == 0) {
    evenSum_ += value;
  } else {
    oddSum_ += value;
  }
  mean_ = (oddSum_ + evenSum_)/ static_cast<double>(positive_ + negative_ + zero_);
}

int get_statistics::getMax() const
{
  return max_;
}

int get_statistics::getMin() const
{
  return min_;
}

double get_statistics::getMean() const
{
  return mean_;
}

size_t get_statistics::getPositive() const
{
  return positive_;
}

size_t get_statistics::getNegative() const
{
  return negative_;
}

long long int get_statistics::getOddSum() const
{
  return oddSum_;
}

long long int get_statistics::getEvenSum() const
{
  return evenSum_;
}

bool get_statistics::getFirstLastEqual() const
{
  return firstLastEqual_;
}

bool get_statistics::empty() const
{
  return (positive_ == 0 && negative_ == 0 && zero_ == 0);
}


