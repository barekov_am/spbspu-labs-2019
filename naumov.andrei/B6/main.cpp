#include <exception>
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <vector>
#include "get_statistics.hpp"

int main(int, char*[])
{
  try {
    std::vector<int> sequence;
    std::string value;
    while (std::cin >> value) {
      double check = std::stod(value);
      if (check - static_cast<int>(check) != 0) {
        std::cerr << "Floating Point\n";
        return 1;
      }
      sequence.push_back(std::stoi(value));
    }
    std::cout << (std::for_each(sequence.begin(), sequence.end(), get_statistics()));
    return 0;
  } catch (std::bad_alloc&) {
    return 2;
  } catch (std::invalid_argument& except) {
    std::cerr << except.what() << '\n';
    return 1;
  }
}

