#ifndef GET_STATISTICS_HPP
#define GET_STATISTICS_HPP

#include <functional>
#include <iostream>
#include <string>

class get_statistics: public std::unary_function<int, void> {
public:
  get_statistics();
  void operator()(const int value);
  friend std::ostream& operator<<(std::ostream& out, const get_statistics& statistics) {
    if (statistics.empty()) {
      return out << "No Data\n";
    }
    return out << "Max: " << statistics.getMax() << "\nMin: " << statistics.getMin() <<
      "\nMean: " << statistics.getMean() << "\nPositive: " << statistics.getPositive() <<
      "\nNegative: " << statistics.getNegative() << "\nOdd Sum: " << statistics.getOddSum() <<
      "\nEven Sum: " << statistics.getEvenSum() << "\nFirst/Last Equal: " <<
      (statistics.getFirstLastEqual() ? "yes" : "no");
  };

  int getMax() const;
  int getMin() const;
  double getMean() const;
  size_t getPositive() const;
  size_t getNegative() const;
  long long int getOddSum() const;
  long long int getEvenSum() const;
  bool getFirstLastEqual() const;

  bool empty() const;
private:
  int max_;
  int min_;
  double mean_;
  size_t positive_;
  size_t negative_;
  long long int oddSum_;
  long long int evenSum_;
  bool firstLastEqual_;

  int first_;
  size_t zero_;
};

#endif //!GET_STATISTICS_HPP
