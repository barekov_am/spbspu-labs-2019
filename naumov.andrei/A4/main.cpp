#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "parcel.hpp"

int main()
{
  try
  {
    naumov::Rectangle testRectangle({ 40,20 }, 11.5, 10);

    testRectangle.getInformation();
    testRectangle.setHeight(2);
    testRectangle.setWidth(2);
    testRectangle.move(-42, -22);

    std::shared_ptr<naumov::Shape> pShape = std::shared_ptr<naumov::Shape>(&testRectangle);

    pShape->getInformation();
    testRectangle.scale(1);
    testRectangle.getInformation();

    naumov::Circle testCircle({ 20,40 }, 12);

    testCircle.getInformation();
    testCircle.setRadius(1);
    testCircle.move({ 2, 2 });

    std::shared_ptr<naumov::Shape> pShape2 = std::shared_ptr<naumov::Shape>(&testCircle);

    pShape2->getInformation();
    testCircle.scale(1);
    testCircle.getInformation();

    naumov::Circle testCircle2({ 0, 0 }, 12);

    std::shared_ptr<naumov::Shape> pShape3 = std::shared_ptr<naumov::Shape>(&testCircle2);

    naumov::CompositeShape testCompositeShape(pShape2);

    testCompositeShape += pShape;
    testCompositeShape.move(2, 3.1);
    testCompositeShape.move({ 0, 0 });
    testCompositeShape.scale(2);
    testCompositeShape.deleteShape(1);

    testCompositeShape += pShape3;
    testCompositeShape += pShape;

    testCompositeShape.getInformation();

    naumov::Matrix testMatrix = parcel(testCompositeShape);

    testMatrix.printInformation();
  }
  catch (std::invalid_argument& error)
  {
    std::cerr << error.what();
    return 1;
  }
  catch (std::logic_error& error)
  {
    std::cerr << error.what();
    return 2;
  }
  catch (std::bad_alloc&)
  {
    return 2;
  }
  return 0;
}
