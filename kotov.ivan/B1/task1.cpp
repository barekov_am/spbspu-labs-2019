#include <forward_list>
#include <vector>
#include "access_types.hpp"
#include "sorts.hpp"
#include "input_output.hpp"

bool compareAsc(int left, int right)
{
  return left < right;
}

bool compareDesc(int left, int right)
{
  return left > right;
}

void task1(bool sortType)
{
  std::vector< int > vector, sortResult;
  kotov::readInVector(vector, std::cin);
  if (vector.empty()) return;
  sortResult.resize(vector.size());
  kotov::BracketAccess< std::vector< int >, int> bracketAccess(vector), bracketAccessInSortResult(sortResult);
  kotov::AtAccess< std::vector< int >, int> atAccess(vector), atAccessInSortResult(sortResult);
  bool (*compare)(int left, int right);
  if (sortType)
  {
    compare = compareAsc;
  }
  else
  {
    compare = compareDesc;
  }
  kotov::quickSort< kotov::BracketAccess< std::vector< int >, int >, int >(bracketAccess, compare,
      vector.size(), bracketAccessInSortResult);
  kotov::writeFromVector(sortResult, std::cout, ' ');
  kotov::quickSort< kotov::AtAccess< std::vector< int >, int >, int >(atAccess, compare,
      vector.size(), atAccessInSortResult);
  kotov::writeFromVector(sortResult, std::cout, ' ');
  kotov::IteratorAccess< std::vector< int >::iterator, int > iteratorAccess(vector.begin()),
      iteratorAccessInSortResult(sortResult.begin());
  kotov::quickSort< kotov::IteratorAccess< std::vector< int >::iterator, int >, int >(iteratorAccess, compare,
      vector.size(), iteratorAccessInSortResult);
  kotov::writeFromVector(sortResult, std::cout, ' ');
}
