#include <forward_list>
#include <vector>
#include <random>
#include "access_types.hpp"
#include "sorts.hpp"
#include "input_output.hpp"

void fillRandom(double * array, long size)
{
  if (size < 1)
  {
    throw std::invalid_argument("array size is too small\n");
  }
  std::random_device device;
  std::mt19937 merschene(device());
  double max = merschene.max();
  for (long i = 0; i < size; i++)
  {
    double rand = merschene();
    array[i] = rand / max;
  }
}

bool compareAsc(double left, double right)
{
  return  left < right;
}

bool compareDesc(double left, double right)
{
  return left > right;
}

void task4(bool sortType, long size)
{
  std::vector< double > vector, sortResult;
  vector.resize(size);
  sortResult.resize(size);
  fillRandom(vector.data(), size);
  kotov::BracketAccess< std::vector< double >, double > bracketAccess(vector), bracketAccessSortResult(sortResult);
  bool (*compare)(double left, double right);
  if (sortType)
  {
    compare = compareAsc;
  }
  else
  {
    compare = compareDesc;
  }
  kotov::writeFromVector(vector, std::cout, ' ');
  kotov::quickSort< kotov::BracketAccess< std::vector< double >, double >, double >(bracketAccess, compare,
      vector.size(), bracketAccessSortResult);
  kotov::writeFromVector(sortResult, std::cout, ' ');
}
