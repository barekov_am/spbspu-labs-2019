#ifndef INPUT_OUTPUT_HPP
#define INPUT_OUTPUT_HPP
#include <iostream>
#include <vector>

namespace kotov
{
  template< typename Container, typename T >
  void readInContainer(Container container, std::istream & input, long length)
  {
    if (length < 1)
    {
      throw std::invalid_argument("lenght mustn't be negative");
    }
    T tmp;
    if (input >> tmp)
    {
      (*container) = tmp;
      for (long i = 1; (i < length) && (input >> tmp); ++i)
      {
        *(++container) = tmp;
      }
    }
    if (!input.eof() && input.fail())
    {
      throw std::invalid_argument("reading fail");
    }
  }

  template< typename Container, typename T >
  int readInContainer(Container container, std::istream & input)
  {
    int readed = 0;
    T tmp;
    if (input >> tmp)
    {
      readed++;
      (*container) = tmp;
      while (input >> tmp)
      {
        *(++container) = tmp;
        readed++;
      }
      if (!input.eof() && input.fail())
      {
        throw std::invalid_argument("reading fail");
      }
    }
    return readed;
  }

  template<typename T >
  void readInVector(std::vector< T > & container, std::istream & input, long length)
  {
    if (length < 1)
    {
      throw std::invalid_argument("lenght mustn't be negative");
    }
    T tmp;
    for (long i = 0; (i < length) && (input >> tmp); ++i)
    {
      container.push_back(tmp);
    }
    if (!input.eof() && input.fail())
    {
      throw std::invalid_argument("reading fail");
    }
  }

  template<typename T >
  void readInVector(std::vector< T > & container, std::istream & input)
  {
    T tmp;
    while (input >> tmp)
    {
      container.push_back(tmp);
    }
    if (!input.eof() && input.fail())
    {
      throw std::invalid_argument("reading fail");
    }
  }

  template< typename T >
  void readInVector(std::vector< T > & container, const T stopT, std::istream & input = std::cin)
  {
    T tmp;
    bool empty = true;
    while (input >> tmp)
    {
      empty = false;
      if (tmp == stopT) break;
      container.push_back(tmp);
    }
    if (!empty && (tmp != stopT))
    {
      throw std::invalid_argument("reading fail");
    }
    if (!input.eof() && input.fail())
    {
      throw std::invalid_argument("reading fail");
    }
  }

  template< typename T >
  void writeFromVector(std::vector< T > & container, std::ostream & output = std::cout, const char separator = 0)
  {
    if (container.empty())
    {
      throw std::invalid_argument("size mustn't be negative");
    }
    if (separator != 0)
    {
      for (unsigned long i = 0; (i < container.size()) && (!output.fail()); ++i)
      {
        output << container[i] << separator;
      }
      output << std::endl;
    }
    else
    {
      for (unsigned long i = 0; (i < container.size()) && (!output.fail()); ++i)
      {
        output << container[i];
      }
    }
    if (output.fail())
    {
      throw std::invalid_argument("printing fail");
    }
  }
}

#endif // INPUT_OUTPUT_HPP
