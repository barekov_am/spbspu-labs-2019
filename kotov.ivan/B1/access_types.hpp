#ifndef ACCESSTYPES_HPP
#define ACCESSTYPES_HPP
#include <iterator>
#include "conceptions.hpp"

namespace kotov
{
  template< typename ContainerType, typename T, typename = void >
  class BracketAccess : public kotov::AccessBase< T >
  {
  public:
    BracketAccess(ContainerType & container):
      m_container(&container),
      m_position(0)
    {
    }

    virtual T & operator *() override
    {
      return (*m_container)[m_position];
    }

    virtual T * operator ->() override
    {
      return &(*m_container)[m_position];
    }

    virtual T & get(long index) override
    {
      m_position = index;
      return (*m_container)[index];
    }

    virtual AccessBase< T > & operator ++() override
    {
      ++m_position;
      return *this;
    }

    virtual AccessBase< T > & operator --() override
    {
      --m_position;
      return *this;
    }

  private:
    ContainerType * m_container;
    long m_position;
  };

  template< typename T >
  class BracketAccess< T *, T >: public kotov::AccessBase< T >
  {
  public:
    BracketAccess(T * container):
      m_container(container),
      m_position(0)
    {
    }

    virtual T & operator *() override
    {
      return m_container[m_position];
    }

    virtual T * operator ->() override
    {
      return &m_container[m_position];
    }

    virtual T & get(long index) override
    {
      m_position = index;
      return m_container[index];
    }

    virtual AccessBase< T > & operator ++() override
    {
      ++m_position;
      return *this;
    }

    virtual AccessBase< T > & operator --() override
    {
      --m_position;
      return *this;
    }

  private:
    T * m_container;
    long m_position;
  };

  template< typename ContainerType, typename T >
  class AtAccess : public kotov::AccessBase< T >
  {
  public:
    AtAccess(ContainerType & container):
      m_container(&container),
      m_position(0)
    {
    }

    virtual T & operator *() override
    {
      return (*m_container).at(m_position);
    }

    virtual T * operator ->() override
    {
      return &(*m_container).at(m_position);
    }

    virtual T & get(long index) override
    {
      m_position = index;
      return (*m_container).at(index);
    }

    virtual AccessBase< T > &  operator ++() override
    {
      m_position++;
      return *this;
    }

    virtual AccessBase< T > & operator --() override
    {
      m_position--;
      return *this;
    }

  private:
    ContainerType * m_container;
    long m_position;
  };

  template< typename Iterator, typename T >
  class IteratorAccess : public kotov::AccessBase< T >
  {
  public:
    IteratorAccess(Iterator iterator):
      m_container(iterator),
      m_position(0)
    {
    }

    virtual T & operator *() override
    {
      return *m_container;
    }

    virtual T * operator ->() override
    {
      return m_container.operator ->();
    }

    virtual T & get(long index) override
    {
      if (m_position < index)
      {
        for (; m_position < index; ++m_position)
        {
          ++m_container;
        }
      }
      else
      {
        for (; m_position > index; --m_position)
        {
          --m_container;
        }
      }
      return *m_container;
    }

    virtual AccessBase< T > & operator ++() override
    {
      ++m_position;
      ++m_container;
      return *this;
    }

    virtual AccessBase< T > & operator --() override
    {
      --m_position;
      --m_container;
      return *this;
    }

  private:
    Iterator m_container;
    long m_position;
  };
}
#endif // ACCESSTYPES_HPP
