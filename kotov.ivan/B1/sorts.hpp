#ifndef SORTS_HPP
#define SORTS_HPP
#include <iostream>

namespace kotov
{
  template < typename T >
  T findMedianByThreePoints(const T first, const T middle, const T end)
  {
    if (first < middle)
    {
      if (end > middle)
      {
        return middle;
      }
      if (end < first)
      {
        return first;
      }
      else
      {
        return end;
      }
    }
    else
    {
      if (end > first)
      {
        return first;
      }
      if (middle < end)
      {
        return end;
      }
      else
      {
        return middle;
      }
    }
  }

  template< typename ContainerType, typename T >
  void quickSort(ContainerType container, bool (*compare)(T, T), long start, long end)
  {
    long middle = (start + end) / 2;
    ContainerType leftContainer = container, rightContainer = container;
    T median = findMedianByThreePoints(leftContainer.get(start), rightContainer.get(middle), rightContainer.get(end));
    rightContainer.get(end);
    long i = start, j = end;
    T tmp;
    do
    {
      if (compare(*leftContainer, median))
      {
        for (++i; (compare(*(++leftContainer), median)) && (i < end); i++)
        {
        }
      }
      if (compare(median, *rightContainer))
      {
        for (--j; (compare(median, *(--rightContainer)) && (start < j)); j--)
        {
        }
      }
      if (j >= i)
      {
        tmp = *rightContainer;
        *rightContainer = *leftContainer;
        *leftContainer = tmp;
        --rightContainer;
        ++leftContainer;
        j--;
        i++;
      }
    } while (i < j);
    if (start < j)
    {
      quickSort(container, compare, start, j);
    }
    if (i < end)
    {
      quickSort(leftContainer, compare, i, end);
    }
  }

  template< typename ContainerType, typename T >
  void quickSort(ContainerType container, bool (*compare)(T, T), long n, ContainerType containerOut)
  {
    container.get(0);
    containerOut.get(0);
    if (n > 0)
    {
      (*containerOut) = (*container);
      for (int i = 1; i < n; ++i)
      {
        *(++containerOut) = *(++container);
      }
      container.get(0);
      containerOut.get(0);
      quickSort(containerOut, compare, 0, n - 1);
    }
  }

  template< typename ContainerType, typename T >
  void quickSort(ContainerType container, bool (*compare)(T, T), long n)
  {
    container.get(0);
    quickSort(container, compare, 0, n - 1);
  }
}

#endif
