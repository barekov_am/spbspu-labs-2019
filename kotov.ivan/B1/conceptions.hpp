#ifndef INTERFACES_HPP
#define INTERFACES_HPP

namespace kotov
{
  template< typename T >
  class AccessBase
  {
  public:
    virtual T & operator *() = 0;
    virtual T * operator ->() = 0;
    virtual T & get(long index) = 0;
    virtual AccessBase & operator ++() = 0;
    virtual AccessBase & operator --() = 0;
 };
}
#endif // INTERFACES_HPP
