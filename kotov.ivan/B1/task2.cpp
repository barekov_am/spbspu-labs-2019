#include <forward_list>
#include <vector>
#include <fstream>
#include <memory>
#include "access_types.hpp"
#include "input_output.hpp"

void task2(const char * fileName)
{
  std::ifstream fileIn(fileName);
  if (!fileIn.is_open())
  {
    throw  std::invalid_argument("file not found");
  };
  long curLength = 0, length = 10;
  std::unique_ptr<char[], decltype (&free)> data(static_cast< char * >(realloc( nullptr, length)), free);
  while (fileIn)
  {
    fileIn.read(&(data[curLength]), length - curLength);
    curLength += fileIn.gcount();
    if (curLength == length)
    {
      length = length * 2;
      std::unique_ptr<char[], decltype (&free)> newData(static_cast< char * >(realloc( data.get(), length)), free);
      data.swap(newData);
      newData.release();
    }
  }
  if (!fileIn.eof() && fileIn.fail())
  {
    fileIn.close();
    throw std::invalid_argument("reading fail");
  }
  fileIn.close();
  if (curLength > 0)
  {
    std::vector< char > vector(&data[0], &data[curLength]);
    kotov::writeFromVector(vector);
  }
}
