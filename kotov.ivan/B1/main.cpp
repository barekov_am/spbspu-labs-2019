#include <iostream>
#include <cstring>
#include "sorts.hpp"

void task1(bool isAscendingSort);
void task2(const char * fileName);
void task3();
void task4(bool isAscendingSort, long length);

int main(int argc, char **argv)
{
  try
  {
    if (argc < 2)
    {
      std::cerr << "arguments not found";
      return 1;
    }
    if ((argv[1][1] != 0x00) || (argv[1][0] < '1') || (argv[1][0] > '4'))
    {
      std::cerr << "invalid task number";
      return 1;
    }
    switch (argv[1][0])
    {
    case '1':

      if (argc < 3)
      {
        std::cerr << "sort type not found";
        return 1;
      }
      if (std::strcmp(argv[2], "ascending") == 0)
      {
        task1(true);
      }
      else
      {
        if (std::strcmp(argv[2], "descending") == 0)
        {
          task1(false);
        }
        else
        {
          std::cerr << "sort type is incorrect";
          return 1;
        }
      }
      break;
    case '2':
      if (argc < 3)
      {
        std::cerr << "arguments not found";
        return 1;
      }
      task2(argv[2]);
      break;
    case '3':
      task3();
      break;
    default:
      if (argc < 4)
      {
        std::cerr << "sort type or vector length not found";
        return 1;
      }
      long length = 0;
      sscanf(argv[3], "%ld", &length);
      if (length < 1)
      {
        std::cerr << "vector length is too small";
        return 1;
      }
      if (std::strcmp(argv[2], "ascending") == 0)
      {
        task4(true, length);
      }
      else
      {
        if (std::strcmp(argv[2], "descending") == 0)
        {
          task4(false, length);
        }
        else
        {
          std::cerr << "sort type is incorrect";
          return 1;
        }
      }
    }
  }
  catch (std::invalid_argument ex)
  {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    return 2;
  }
  return 0;
}
