#include <vector>
#include "input_output.hpp"

void task3()
{
  std::vector< int > vector;
  kotov::readInVector(vector, 0);
  if (vector.empty())
  {
    return;
  }
  if ((vector.back()) == 1)
  {
    for (auto it = vector.begin(); it != vector.end();)
    {
      if ((*it % 2 ) == 0)
      {
        it = vector.erase(it);
      }
      else
      {
        ++it;
      }
    }
  }
  else if (vector.back() == 2)
  {
    for (auto it = vector.begin(); it != vector.end(); it++)
    {
      if (((*it) % 3) == 0)
      {
        it = vector.insert(++it, 3, 1);
        it += 2;
      }
    }
  }
  kotov::writeFromVector(vector, std::cout, ' ');
}
