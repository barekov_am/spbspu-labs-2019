#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "utils.hpp"

int main()
{
  try
  {
    kotov::Circle circle({25, 25}, 5);
    kotov::Rectangle rect({60, 60}, 10, 10);
    kotov::CompositeShape composite;
    std::cout << "before rotation of " << circle.getShapeName() << std::endl;
    std::cout << "position of " << circle.getShapeName() << ": " << circle.getFrameRect().pos.x;
    std::cout << ", " << circle.getFrameRect().pos.y << std::endl;
    std::cout << "width and height of " << circle.getShapeName() << " frameRect is: " << circle.getFrameRect().width;
    std::cout << ", " << circle.getFrameRect().height << std::endl;
    circle.rotate(3.7);
    std::cout << "after rotation of " << circle.getShapeName() << std::endl;
    std::cout << "position of " << circle.getShapeName() << ": " << circle.getFrameRect().pos.x;
    std::cout << ", " << circle.getFrameRect().pos.y << std::endl;
    std::cout << "width and height of " << circle.getShapeName() << " frameRect is: " << circle.getFrameRect().width;
    std::cout << ", " << circle.getFrameRect().height << std::endl;
    (composite += &circle) += &rect;
    std::cout << "before rotation of " << rect.getShapeName() << std::endl;
    std::cout << "position of " << rect.getShapeName() << ": " << rect.getFrameRect().pos.x;
    std::cout << ", " << rect.getFrameRect().pos.y << std::endl;
    std::cout << "width and height of " << rect.getShapeName() << " frameRect is: " << rect.getFrameRect().width;
    std::cout << ", " << rect.getFrameRect().height << std::endl;
    rect.rotate(0.7);
    std::cout << "after rotation of " << rect.getShapeName() << std::endl;
    std::cout << "position of " << rect.getShapeName() << ": " << rect.getFrameRect().pos.x;
    std::cout << ", " << rect.getFrameRect().pos.y << std::endl;
    std::cout << "width and height of " << rect.getShapeName() << " frameRect is: " << rect.getFrameRect().width;
    std::cout << ", " << rect.getFrameRect().height << std::endl;
    std::cout << "before rotation of " << composite.getShapeName() << std::endl;
    std::cout << "position of " << composite.getShapeName() << ": " << composite.getFrameRect().pos.x;
    std::cout << ", " << composite.getFrameRect().pos.y << std::endl;
    std::cout << "width and height of " << composite.getShapeName() << " frameRect is: " << composite.getFrameRect().width;
    std::cout << ", " << composite.getFrameRect().height << std::endl;
    composite.rotate(2.7);
    std::cout << "after rotation of " << composite.getShapeName() << std::endl;
    std::cout << "position of " << composite.getShapeName() << ": " << composite.getFrameRect().pos.x;
    std::cout << ", " << composite.getFrameRect().pos.y << std::endl;
    std::cout << "width and height of " << composite.getShapeName() << " frameRect is: " << composite.getFrameRect().width;
    std::cout << ", " << composite.getFrameRect().height << std::endl;
    kotov::Matrix matrix;
    kotov::CompositeShape tmp;
    tmp = tmp + &circle + &rect + &composite;
    matrix = kotov::separate(tmp);
    for (unsigned int i = 0; i < matrix.getNumLayers(); i++)
    {
      for (unsigned int j = 0; j < matrix.getLayerSize(i); j++)
      {
        std::cout << "shape in layer " << i << " in position " << j << " is " << matrix[i][j]->getShapeName() << std::endl;
      }
    }
  }
  catch (std::invalid_argument ex)
  {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  catch (std::out_of_range ex)
  {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    return 2;
  }
  return 0;
}
