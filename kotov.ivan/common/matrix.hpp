#ifndef MATRIX_HPP
#define MATRIX_HPP
#include <memory>
#include "shape.hpp"
#include "composite-shape.hpp"

namespace kotov
{
  class Matrix
  {
  public:
    Matrix();

    Matrix(const kotov::Matrix &matrix);
    Matrix(kotov::Matrix &&matrix);

    ~Matrix() = default;

    Matrix &operator =(const kotov::Matrix &matrix);
    Matrix &operator =(kotov::Matrix &&matrix);

    Matrix operator -(kotov::Shape *shape);
    Matrix &operator -=(kotov::Shape *shape);

    bool operator ==(const kotov::Matrix &matrix) const;
    bool operator !=(const kotov::Matrix &matrix) const;

    kotov::Shape **operator [](unsigned int layer) const;

    //This metod call delete for all shapes, which are stored in Matrix
    void clear();
    void addNewLayer();
    void addShapeInLayer(unsigned int layer, kotov::Shape *shape);

    void replace(kotov::Shape *oldShape, kotov::Shape *newShape);
    void remove(kotov::Shape *shape);
    void remove(unsigned int layer, unsigned int index);
    unsigned int getNumLayers() const;
    unsigned int getLayerSize(unsigned int layer) const;

  private:
    unsigned int layers_,  maxLayerSize_;
    std::unique_ptr<kotov::Shape*[]> buffer_;
    std::unique_ptr<unsigned int[]> layerLengths_;

    void resize(unsigned int newMaxLayerSize, unsigned int newNumberOfLayers);
  };
}

#endif // MATRIX_HPP
