#include "matrix.hpp"
#include <math.h>

kotov::Matrix::Matrix():
  layers_(1),
  maxLayerSize_(1),
  buffer_(new Shape*[1]),
  layerLengths_(new unsigned int[1])
{
  layerLengths_[0] = 0;
  buffer_[0] = nullptr;
}

kotov::Matrix::Matrix(const kotov::Matrix &matrix):
  layers_(matrix.layers_),
  maxLayerSize_(matrix.maxLayerSize_),
  buffer_(new Shape*[layers_*maxLayerSize_]),
  layerLengths_(new unsigned int[layers_])
{
  for (unsigned int i = 0; i < layers_; i++)
  {
    layerLengths_[i] = matrix.layerLengths_[i];
  }
  for (unsigned int i = 0; i < layers_ * maxLayerSize_; i++)
  {
    buffer_[i] = matrix.buffer_[i];
  }
}

kotov::Matrix::Matrix(kotov::Matrix &&matrix):
  layers_(matrix.layers_),
  maxLayerSize_(matrix.maxLayerSize_),
  buffer_(std::move(matrix.buffer_)),
  layerLengths_(std::move(matrix.layerLengths_))
{
}

kotov::Matrix &kotov::Matrix::operator =(const kotov::Matrix &matrix)
{
  if (this != &matrix)
  {
    layers_ = matrix.layers_;
    maxLayerSize_ = matrix.maxLayerSize_;
    buffer_ = std::unique_ptr<Shape*[]>(new Shape*[layers_*maxLayerSize_]);
    layerLengths_ = std::unique_ptr<unsigned int[]>(new unsigned int[layers_]);
    for (unsigned int i = 0; i < layers_; i++)
    {
      layerLengths_[i] = matrix.layerLengths_[i];
    }
     for (unsigned int i = 0; i < layers_ * maxLayerSize_; i++)
    {
      buffer_[i] = matrix.buffer_[i];
    }
  }
  return *this;
}
kotov::Matrix &kotov::Matrix::operator =(kotov::Matrix &&matrix)
{
  if (&matrix != this)
  {
    layers_ = matrix.layers_;
    maxLayerSize_ = matrix.maxLayerSize_;
    buffer_.swap(matrix.buffer_);
    layerLengths_.swap(matrix.layerLengths_);
  }
  return *this;
}

kotov::Matrix kotov::Matrix::operator -(kotov::Shape *shape)
{
  Matrix matrix(*this);
  return matrix -= shape;
}

kotov::Matrix &kotov::Matrix::operator -=(kotov::Shape *shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("object mustn't be null");
  }
  for (unsigned int i = 0; i < layers_; i++)
  {
    unsigned int j;
    for (j = 0; j < layerLengths_[i]; j++)
    {
      if (buffer_[i * maxLayerSize_ + j] == shape)
      {
        remove(i, j);
      }
    }
    if (j != layerLengths_[i])
    {
      break;
    }
  }
  return *this;
}

kotov::Shape **kotov::Matrix::operator [](unsigned int layer) const
{
  if (layer >= layers_)
  {
    throw std::out_of_range("index of layer is begger then number of layers");
  }
  if (layerLengths_[layer] == 0)
  {
    throw std::invalid_argument("layer is empty");
  }
  return &buffer_[layer * maxLayerSize_];
}

//This metod call delete for all shapes, which are stored in Matrix
void kotov::Matrix::clear()
{
  layers_ = 1;
  layerLengths_[0] = 0;
  resize(1, 1);
}

void kotov::Matrix::addNewLayer()
{
  resize(maxLayerSize_, layers_ + 1);
  layerLengths_[layers_ - 1] = 0;
}

void kotov::Matrix::addShapeInLayer(unsigned int layer, kotov::Shape *shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("object mustn't be null");
  }
  if (layer >= layers_)
  {
    throw std::out_of_range("index of layer is biger then number of layers");
  }
  if (maxLayerSize_ == layerLengths_[layer])
  {
    resize(maxLayerSize_ + 1, layers_);
  }
  buffer_[layer * maxLayerSize_ + layerLengths_[layer]] = shape;
  layerLengths_[layer]++;
}

void kotov::Matrix::replace(kotov::Shape *oldShape, kotov::Shape *newShape)
{
  if (oldShape == nullptr || newShape == nullptr)
  {
    throw std::invalid_argument("object mustn't be null");
  }
  for (unsigned int i = 0; i < layers_; i++)
  {
    unsigned int j;
    for (j = 0; j < layerLengths_[i]; j++)
    {
      if (buffer_[i * maxLayerSize_ + j] == oldShape)
      {
        buffer_[i * maxLayerSize_ + j] = newShape;
      }
    }
    if (j != layerLengths_[i])
    {
      break;
    }
  }
}

void kotov::Matrix::remove(kotov::Shape *shape)
{
  operator -=(shape);
}

void kotov::Matrix::remove(unsigned int layer, unsigned int index)
{
  for (unsigned int k = index; k < layerLengths_[layer] - 1; k++)
  {
    buffer_[layer * maxLayerSize_ + k] = buffer_[layer * maxLayerSize_ + k + 1];
  }
  layerLengths_[layer]--;
}

unsigned int kotov::Matrix::getNumLayers() const
{
  return layers_;
}

unsigned int kotov::Matrix::getLayerSize(unsigned int layer) const
{
  return layerLengths_[layer];
}

void kotov::Matrix::resize(unsigned int newMaxLayerSize, unsigned int newNumberOfLayers)
{
  std::unique_ptr<kotov::Shape*[]> newBuffer(new kotov::Shape*[newMaxLayerSize * newNumberOfLayers]);
  for (unsigned int i = 0; i < layers_; i++)
  {
    for (unsigned int j = 0; j < layerLengths_[i]; j++)
    {
      newBuffer[i * newMaxLayerSize + j] = buffer_[i * maxLayerSize_ + j];
    }
  }
  buffer_.swap(newBuffer);
  if (newNumberOfLayers != layers_)
  {
    std::unique_ptr<unsigned int[]> newLayerLengthBuffer(new unsigned int[newNumberOfLayers]);
    for (unsigned int i = 0; i < layers_; i++)
    {
      newLayerLengthBuffer[i] = layerLengths_[i];
    }
    layerLengths_.swap(newLayerLengthBuffer);
  }
  maxLayerSize_ = newMaxLayerSize;
  layers_ = newNumberOfLayers;
}
