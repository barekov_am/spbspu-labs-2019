#include <stdexcept>
#include <math.h>
#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testForCompositeShape)

BOOST_AUTO_TEST_CASE(scaleTest)
{
  kotov::Rectangle rect({50.0, 40.0}, 10.0, 9.0);
  kotov::Circle circle({0.0, 0.0}, 8.0);
  kotov::CompositeShape compShape;
  compShape = compShape + &circle + &rect;
  const double scale = 7.9;
  double area = compShape.getArea() * scale * scale;
  compShape.scale(scale);
  BOOST_CHECK_CLOSE_FRACTION(area, compShape.getArea(), std::numeric_limits<double>::epsilon());
}

BOOST_AUTO_TEST_CASE(displacementMovingTest)
{
  kotov::Rectangle rect({-50, 39}, 10.0, 9.0);
  kotov::Circle circle({0.0, 0.0}, 7.0);
  kotov::CompositeShape compShape;
  compShape = compShape + &circle + &rect;
  const kotov::rectangle_t frame = compShape.getFrameRect();
  compShape.move(56, -45);
  BOOST_CHECK_CLOSE(frame.height, compShape.getFrameRect().height, std::numeric_limits<double>::epsilon());
  BOOST_CHECK_CLOSE(frame.width, compShape.getFrameRect().width, std::numeric_limits<double>::epsilon());
}

BOOST_AUTO_TEST_CASE(movingInPositionTest)
{
  kotov::Rectangle rect({52, -47}, 10.0, 9.0);
  kotov::Circle circle({0.0, 0.0}, 6.0);
  kotov::CompositeShape compShape;
  compShape = compShape + &circle + &rect;
  const kotov::rectangle_t frame = compShape.getFrameRect();
  compShape.move({-54, 29});
  BOOST_CHECK_CLOSE(frame.height, compShape.getFrameRect().height, std::numeric_limits<double>::epsilon());
  BOOST_CHECK_CLOSE(frame.width, compShape.getFrameRect().width, std::numeric_limits<double>::epsilon());
}
BOOST_AUTO_TEST_CASE(rotationTest)
{
  kotov::Rectangle rect({52, -47}, 10.0, 9.0);
  kotov::Circle circle({0.0, 0.0}, 6.0);
  kotov::CompositeShape composite;
  (composite += &circle) += &rect;
  kotov::point_t oldPos = composite.getFrameRect().pos;
  composite.rotate(M_PI);
  kotov::point_t newPos = composite.getFrameRect().pos;
  BOOST_CHECK_CLOSE_FRACTION(oldPos.x, newPos.x, std::numeric_limits<double>::epsilon());
  BOOST_CHECK_CLOSE_FRACTION(oldPos.y, newPos.y, std::numeric_limits<double>::epsilon());
}

BOOST_AUTO_TEST_CASE(invalidArgumentsTest)
{
  kotov::Rectangle rect({50, 51}, 10.0, 9.0);
  kotov::Circle circle({0.0, 0.0}, 8.0);
  kotov::CompositeShape compShape;
  compShape += &circle;
  BOOST_CHECK_THROW(compShape[11], std::out_of_range);
  BOOST_CHECK_THROW(compShape.scale(-3.5), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.scale(0), std::invalid_argument);
  kotov::Shape *shape = nullptr;
  BOOST_CHECK_THROW(compShape = compShape + shape, std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
