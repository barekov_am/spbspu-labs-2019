#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP
#include <memory>
#include "shape.hpp"

namespace kotov
{
  class CompositeShape : public Shape
  {

  public:

    CompositeShape();
    CompositeShape(const kotov::CompositeShape &composite);
    CompositeShape(kotov::CompositeShape &&composite);

    ~CompositeShape() override = default;

    CompositeShape &operator =(const kotov::CompositeShape &composite);
    CompositeShape &operator =(kotov::CompositeShape &&composite);

    CompositeShape operator +(kotov::Shape *shape);
    CompositeShape operator -(kotov::Shape *shape);
    CompositeShape &operator +=(kotov::Shape *shape);
    CompositeShape &operator -=(kotov::Shape *shape);

    kotov::Shape *operator [](int index) const;

    //This metod call delete for all shapes, which are stored in CompositeShape
    void clear();
    void add(kotov::Shape *shape);
    //if shape don't stored in buffer this function return -1
    int indexOf(kotov::Shape *shape) const;
    void replace(int index, kotov::Shape *shape);
    void remove(kotov::Shape *shape);
    void remove(int index);
    int size() const;
    const char *getShapeName() const override;

    //this function return amount of area shapes
    double getArea() const override;

    kotov::rectangle_t getFrameRect() const override;

    void move(const kotov::point_t &newCenter) override;

    void move(double dX, double dY) override;

    void scale(double scale) override;
    void rotate(double radian) override;

  private:
    int size_, bufferLength_;
    std::unique_ptr<kotov::Shape*[]> buffer_;
    void changeBufferSize(int newSize);
    double rotation_;
  };
}

#endif // COMPOSITESHAPE_HPP
