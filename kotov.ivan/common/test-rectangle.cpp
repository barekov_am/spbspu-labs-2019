#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testForRectangle)

BOOST_AUTO_TEST_CASE(scaleTest)
{
  kotov::Rectangle rectangle({0.0, 0.0}, 10.0, 9.0);
  const double scale = 6.7;
  const double areaRect = rectangle.getArea() * scale * scale;
  rectangle.scale(scale);
  BOOST_CHECK_CLOSE_FRACTION(areaRect, rectangle.getArea(), std::numeric_limits<double>::epsilon());
}

BOOST_AUTO_TEST_CASE(displacementMovingTest)
{
  kotov::Rectangle rectangle({0.0, 0.0}, 10.0, 9.0);
  const kotov::rectangle_t frameRect = rectangle.getFrameRect();
  rectangle.move(25, -12);
  BOOST_CHECK_CLOSE(frameRect.height, rectangle.getFrameRect().height, std::numeric_limits<double>::epsilon());
  BOOST_CHECK_CLOSE(frameRect.width, rectangle.getFrameRect().width, std::numeric_limits<double>::epsilon());
}

BOOST_AUTO_TEST_CASE(movingInPositionTest)
{
  kotov::Rectangle rectangle({0.0, 0.0}, 10.0, 9.0);
  const kotov::rectangle_t frameRect = rectangle.getFrameRect();
  rectangle.move({65, -7});
  BOOST_CHECK_CLOSE(frameRect.height, rectangle.getFrameRect().height, std::numeric_limits<double>::epsilon());
  BOOST_CHECK_CLOSE(frameRect.width, rectangle.getFrameRect().width, std::numeric_limits<double>::epsilon());
}

BOOST_AUTO_TEST_CASE(rotationTest)
{
  kotov::Rectangle rect({0.0, 0.0},9.0, 10.0);
  kotov::point_t pos = rect.getFrameRect().pos;
  rect.rotate(2.5);
  BOOST_CHECK_CLOSE_FRACTION(pos.x, rect.getFrameRect().pos.x, std::numeric_limits<double>::epsilon());
  BOOST_CHECK_CLOSE_FRACTION(pos.y, rect.getFrameRect().pos.y, std::numeric_limits<double>::epsilon());
}

BOOST_AUTO_TEST_CASE(invalidArgumentsTest)
{
  BOOST_CHECK_THROW(kotov::Rectangle({0, 0}, 0, 1), std::invalid_argument);
  BOOST_CHECK_THROW(kotov::Rectangle({0, 0}, 2, 0), std::invalid_argument);
  BOOST_CHECK_THROW(kotov::Rectangle({0, 0}, 3, -1), std::invalid_argument);
  BOOST_CHECK_THROW(kotov::Rectangle({0, 0}, -2, 4), std::invalid_argument);
  BOOST_CHECK_THROW(kotov::Rectangle({0, 0}, 5, 6).scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(kotov::Rectangle({0, 0}, 7, 8).scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
