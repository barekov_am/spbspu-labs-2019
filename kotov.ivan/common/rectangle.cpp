#include "rectangle.hpp"
#include <stdexcept>
#include <math.h>

kotov::Rectangle::Rectangle(const kotov::point_t &center, double widht, double heigth) :
  rect_({ center, widht, heigth}),
  rotation_(0)
{
  if ((rect_.width <= 0) || (rect_.height <= 0))
  {
    throw std::invalid_argument("Invalid width or height of rectangle");
  }
}

const char *kotov::Rectangle::getShapeName() const
{
  return "rectangle";
}

double kotov::Rectangle::getArea() const
{
  return rect_.height * rect_.width;
}

kotov::rectangle_t kotov::Rectangle::getFrameRect() const
{
  rectangle_t frameRect = rect_;
  double radius = std::sqrt(frameRect.width * frameRect.width + frameRect.height * frameRect.height) / 2;
  double corner = std::acos(frameRect.width / 2 / radius);
  int tmp = std::round(rotation_ / (M_PI / 2));
  double smallCorner = rotation_ - tmp * (M_PI / 2);
  if (smallCorner < 0) smallCorner += M_PI / 2;
  frameRect.width = 2 * radius * cos(smallCorner - corner);
  frameRect.height = 2 * radius * sin(corner + smallCorner);
  if (frameRect.width < 0)
  {
    frameRect.width *= -1;
  }
  if (frameRect.height < 0)
  {
    frameRect.height *= -1;
  }
  return frameRect;
}

void kotov::Rectangle::move(const kotov::point_t &newCenter)
{
  rect_.pos = newCenter;
}

void kotov::Rectangle::move(double dX, double dY)
{
  rect_.pos.x += dX;
  rect_.pos.y += dY;
}

void kotov::Rectangle::scale(double scale)
{
  if (scale > 0)
  {
    rect_.width *= scale;
    rect_.height *= scale;
  }
  else
  {
    throw std::invalid_argument("Invalid scale");
  }
}

void kotov::Rectangle::rotate(double radian)
{
  rotation_ = radian;
}
