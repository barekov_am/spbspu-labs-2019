#ifndef UTILS_HPP
#define UTILS_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace kotov
{
  kotov::Matrix separate(kotov::CompositeShape &composite);
}
#endif // UTILS_HPP
