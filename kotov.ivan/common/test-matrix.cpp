#include <stdexcept>
#include <math.h>
#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "utils.hpp"

BOOST_AUTO_TEST_SUITE(testForMatrix)

BOOST_AUTO_TEST_CASE(separateTest)
{
  kotov::Matrix matrix;
  kotov::Rectangle rect({50, -46}, 11.0, 12.0);
  kotov::Circle circle({0.0, 0.0}, 7.0);
  kotov::CompositeShape compShape;
  kotov::Rectangle rect2({7, 8}, 1, 3);
  compShape = compShape + &rect2 + &rect + &circle;
  matrix = kotov::separate(compShape);
  BOOST_CHECK((matrix[0][0] == &rect2) && (matrix[0][1] == &rect) && (matrix[1][0] == &circle));
}

BOOST_AUTO_TEST_CASE(removingTest)
{
  kotov::Matrix matrix;
  kotov::Rectangle rect({20, -20}, 10.0, 9.0);
  kotov::Circle circle({0.0, 0.0}, 6.0);
  matrix.addShapeInLayer(0, &rect);
  matrix.addShapeInLayer(0, &circle);
  const unsigned int sizeLayer = matrix.getLayerSize(0);
  matrix.remove(&circle);
  BOOST_CHECK(matrix.getLayerSize(0) == sizeLayer - 1);
}

BOOST_AUTO_TEST_CASE(invalidArgumentsTest)
{
  kotov::Rectangle rect({50, 51}, 10.0, 9.0);
  kotov::Circle circle({0.0, 0.0}, 8.0);
  kotov::Matrix matrix;
  matrix.addShapeInLayer(0, &circle);
  BOOST_CHECK_THROW(matrix[11], std::out_of_range);
  kotov::Shape *shape = nullptr;
  BOOST_CHECK_THROW(matrix.addShapeInLayer(0, shape), std::invalid_argument);
  BOOST_CHECK_THROW(matrix.replace(&rect, nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
