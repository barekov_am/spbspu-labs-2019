#include "utils.hpp"
#include "math.h"
kotov::Matrix kotov::separate(kotov::CompositeShape &composite)
{
  kotov::Matrix matr;
  for (int k = 0; k < composite.size(); ++k)
  {
    rectangle_t frame = composite[k]->getFrameRect();
    unsigned int i;
    for (i = 0; i < matr.getNumLayers(); ++i)
    {
      unsigned int j;
      for (j = 0; j < matr.getLayerSize(i); j++)
      {
        rectangle_t frameRect = matr[i][j]->getFrameRect();
        if ((std::abs(frameRect.pos.x - frame.pos.x) < (frameRect.width + frame.width)/2)
            && (std::abs(frameRect.pos.y - frame.pos.y) < (frameRect.height + frame.height)/2))
        {
          break;
        }
      }
      if (j == matr.getLayerSize(i))
      {
        matr.addShapeInLayer(i, composite[k]);
        break;
      }
    }
    if (i == matr.getNumLayers())
    {
      matr.addNewLayer();
      matr.addShapeInLayer(i, composite[k]);
    }
  }
  return matr;
}
