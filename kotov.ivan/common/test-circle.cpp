#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testForCircle)

BOOST_AUTO_TEST_CASE(scaleTest)
{
  kotov::Circle circle({0.0, 0.0}, 10.0);
  const double scale = 8.9;
  const double areaCircle = circle.getArea() * scale * scale;
  circle.scale(scale);
  BOOST_CHECK_CLOSE_FRACTION(areaCircle, circle.getArea(), std::numeric_limits<double>::epsilon());
}

BOOST_AUTO_TEST_CASE(displacementMovingTest)
{
  kotov::Circle circle({0.0, 0.0}, 10.0);
  const kotov::rectangle_t frameCircle = circle.getFrameRect();
  circle.move(63, -89);
  BOOST_CHECK_CLOSE(frameCircle.height, circle.getFrameRect().height, std::numeric_limits<double>::epsilon());
  BOOST_CHECK_CLOSE(frameCircle.width, circle.getFrameRect().width, std::numeric_limits<double>::epsilon());
}

BOOST_AUTO_TEST_CASE(movingInPositionTest)
{
  kotov::Circle circle({0.0, 0.0}, 10.0);
  const kotov::rectangle_t frameCircle = circle.getFrameRect();
  circle.move({-45, 30});
  BOOST_CHECK_CLOSE(frameCircle.height, circle.getFrameRect().height, std::numeric_limits<double>::epsilon());
  BOOST_CHECK_CLOSE(frameCircle.width, circle.getFrameRect().width, std::numeric_limits<double>::epsilon());
}

BOOST_AUTO_TEST_CASE(rotationTest)
{
  kotov::Circle circle({0.0, 0.0}, 10.0);
  kotov::point_t pos = circle.getFrameRect().pos;
  circle.rotate(2.5);
  BOOST_CHECK_CLOSE(pos.x, circle.getFrameRect().pos.x, std::numeric_limits<double>::epsilon());
  BOOST_CHECK_CLOSE(pos.y, circle.getFrameRect().pos.y, std::numeric_limits<double>::epsilon());
}

BOOST_AUTO_TEST_CASE(invalidArgumentsTest)
{
  BOOST_CHECK_THROW(kotov::Circle({0, 0}, 0), std::invalid_argument);
  BOOST_CHECK_THROW(kotov::Circle({0, 0}, -1), std::invalid_argument);
  BOOST_CHECK_THROW(kotov::Circle({0, 0}, 1).scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(kotov::Circle({0, 0}, 2).scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
