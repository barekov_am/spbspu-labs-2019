#include "composite-shape.hpp"
#include <stdexcept>
#include <math.h>

kotov::CompositeShape::CompositeShape():
  size_(0),
  bufferLength_(10),
  buffer_(new kotov::Shape*[bufferLength_]),
  rotation_(0)
{
}

kotov::CompositeShape::CompositeShape(const kotov::CompositeShape &composite) :
  size_(composite.size_),
  bufferLength_(composite.bufferLength_),
  buffer_(new kotov::Shape*[bufferLength_]),
  rotation_(composite.rotation_)
{
  for (int i = 0; i < size_; i++)
  {
    buffer_[i] = composite.buffer_[i];
  }
}

kotov::CompositeShape::CompositeShape(kotov::CompositeShape &&composite) :
  size_(composite.size_),
  bufferLength_(composite.bufferLength_),
  buffer_(std::move(composite.buffer_)),
  rotation_(composite.rotation_)
{
}

kotov::CompositeShape &kotov::CompositeShape::operator =(const kotov::CompositeShape &composite)
{
  if (this != &composite)
  {
    size_ = composite.size_;
    bufferLength_ = composite.bufferLength_;
    buffer_ = std::unique_ptr<kotov::Shape*[]>(new Shape*[bufferLength_]);
    rotation_ = composite.rotation_;
    for (int i = 0; i < size_; i++)
    {
      buffer_[i] = composite.buffer_[i];
    }
  }
  return *this;
}

kotov::CompositeShape &kotov::CompositeShape::operator =(kotov::CompositeShape &&composite)
{
  if (this != &composite)
  {
    size_ = composite.size_;
    bufferLength_ = composite.bufferLength_;
    buffer_.swap(composite.buffer_);
    rotation_ = composite.rotation_;
  }
  return *this;
}

kotov::CompositeShape kotov::CompositeShape::operator +(kotov::Shape *shape)
{
  kotov::CompositeShape tmp = *this;
  return tmp += shape;
}

kotov::CompositeShape kotov::CompositeShape::operator -(kotov::Shape *shape)
{
  kotov::CompositeShape tmp = *this;
  return tmp -= shape;
}

kotov::CompositeShape &kotov::CompositeShape::operator +=(kotov::Shape *shape)
{
  if (shape == this)
  {
    throw std::invalid_argument("you are trying to add a pointer to the CompositShape itself");
  }
  if (shape == nullptr)
  {
    throw std::invalid_argument("object mustn't be null");
  }
  if (indexOf(shape) == -1)
  {
    if (bufferLength_ == size_)
    {
      changeBufferSize(bufferLength_ * 2);
    }
    buffer_[size_] = shape;
    size_++;
  }
  return *this;
}

kotov::CompositeShape &kotov::CompositeShape::operator -=(kotov::Shape *shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("object mustn't be null");
  }
  int i = 0;
  for (; (i < size_) && (buffer_[i] != shape); i++)
    ;
  if (i < size_)
  {
    for ( int j = i; j < size_ - 1; j++)
    {
      buffer_[j] = buffer_[j + 1];
    }
    size_--;
    if (((size_ * 4) < bufferLength_) && (bufferLength_ >= 20))
    {
      changeBufferSize(bufferLength_ / 2);
    }
  }
  return *this;
}

void kotov::CompositeShape::add(kotov::Shape *shape)
{
  operator +=(shape);
}

kotov::Shape *kotov::CompositeShape::operator [](int index) const
{
  if ((index >= 0) && (index < size_))
  {
    return buffer_[index];
  }
  else
  {
    throw std::out_of_range("index out of range");
  }
}

int kotov::CompositeShape::indexOf(kotov::Shape *shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("object mustn't be null");
  }
  int i = 0;
  for (; (i < size_) && (buffer_[i] != shape); i++)
    ;
  if (i == size_)
  {
    return -1;
  }
  return i;
}

void kotov::CompositeShape::clear()
{
  size_ = 0;
  bufferLength_ = 0;
  changeBufferSize(10);
}

const char *kotov::CompositeShape::getShapeName() const
{
  return "composite shape";
}

void kotov::CompositeShape::remove(kotov::Shape *shape)
{
  operator -=(shape);
}

void kotov::CompositeShape::remove(int index)
{
  if ((index >= 0) && (index < size_))
  {
    for (int i = index; i < size_ - 1; i++)
    {
      buffer_[i] = buffer_[i + 1];
    }
  }
  else
  {
    throw std::out_of_range("index out of range");
  }
}

int kotov::CompositeShape::size() const
{
  return size_;
}

//this function return amount of area shapes
double kotov::CompositeShape::getArea() const
{
  double area = 0;
  for (int i = 0; i < size_; i++)
  {
    area += buffer_[i]->getArea();
  }
  return area;
}

kotov::rectangle_t kotov::CompositeShape::getFrameRect() const
{
  if (size_ != 0)
  {
    rectangle_t m_frame = buffer_[0]->getFrameRect();
    for (int i = 1; i < size_; i++)
    {
      const double leftEdge = buffer_[i]->getFrameRect().pos.x - buffer_[i]->getFrameRect().width / 2;
      const double rightEdge = buffer_[i]->getFrameRect().pos.x + buffer_[i]->getFrameRect().width / 2;
      const double topEdge = buffer_[i]->getFrameRect().pos.y + buffer_[i]->getFrameRect().height / 2;
      const double bottomEdge = buffer_[i]->getFrameRect().pos.y - buffer_[i]->getFrameRect().height / 2;
      if ((m_frame.pos.x - m_frame.width / 2) > leftEdge)
      {
        m_frame.pos.x = (m_frame.pos.x + m_frame.width / 2 + leftEdge) / 2;
        m_frame.width = 2 * (m_frame.pos.x - leftEdge);
      }
      if ((m_frame.pos.x + m_frame.width / 2) < rightEdge)
      {
        m_frame.pos.x = (m_frame.pos.x - m_frame.width / 2 + rightEdge) / 2;
        m_frame.width = 2 * (rightEdge - m_frame.pos.x);
      }
      if ((m_frame.pos.y + m_frame.height / 2) < topEdge)
      {
        m_frame.pos.y = (m_frame.pos.y - m_frame.height / 2 + topEdge) / 2;
        m_frame.height = 2 * (topEdge - m_frame.pos.y);
      }
      if ((m_frame.pos.y - m_frame.height / 2) > bottomEdge)
      {
        m_frame.pos.y = (m_frame.pos.y + m_frame.height / 2 + bottomEdge) / 2;
        m_frame.height = 2 * (m_frame.pos.y - bottomEdge);
      }
    }
    return m_frame;
  }
  throw std::out_of_range("error: CompisiteShape doesn't contain shapes");
}

void kotov::CompositeShape::move(const kotov::point_t &newCenter)
{
  rectangle_t m_frame = getFrameRect();
  for (int i = 0; i < size_; i++)
  {
    if (buffer_[i] != nullptr)
    {
      buffer_[i]->move(newCenter.x - m_frame.pos.x, newCenter.y - m_frame.pos.y);
    }
  }
}

void kotov::CompositeShape::move(double dispX, double dispY)
{
  for (int i = 0; i < size_; i++)
  {
    if (buffer_[i] != nullptr)
    {
      buffer_[i]->move(dispX, dispY);
    }
  }
}

void kotov::CompositeShape::scale(double scale)
{
  if (scale > 0)
  {
    rectangle_t frame_ = getFrameRect();
    for (int i = 0; i < size_; i++)
    {
      const rectangle_t figureFrame_ = buffer_[i]->getFrameRect();
      buffer_[i]->move((scale - 1) * figureFrame_.pos.x - (scale - 1) * frame_.pos.x,
          (scale - 1) * figureFrame_.pos.y - (scale - 1) * frame_.pos.y);
      buffer_[i]->scale(scale);
    }
  }
  else
  {
    throw std::invalid_argument("scale must be positive");
  }
}

void kotov::CompositeShape::rotate(double radian)
{
  point_t pos = getFrameRect().pos;
  for (int i = 0; i < size_; i++)
  {
    point_t figurePos = buffer_[i]->getFrameRect().pos;
    figurePos.x = figurePos.x - pos.x;
    figurePos.y = figurePos.y - pos.y;
    if ((std::abs(figurePos.x) > 0.000000001) || (std::abs(figurePos.y) > 0.000000001))
    {
      double radius = std::sqrt(figurePos.x * figurePos.x + figurePos.y * figurePos.y);
      double corner = std::acos(figurePos.x / radius);
      if (figurePos.y < 0) corner = -corner;
      corner += radian;
      buffer_[i]->move(radius * cos(corner) - figurePos.x, radius * sin(corner) - figurePos.y);
    }
    buffer_[i]->rotate(radian);
  }
}

void kotov::CompositeShape::changeBufferSize(int newSize)
{
  if (newSize >= size_)
  {
    std::unique_ptr<kotov::Shape*[]> tmpBuffer(new Shape*[newSize]);
    for (int i = 0; i < size_; i++)
    {
      tmpBuffer[i] = buffer_[i];
    }
    buffer_.swap(tmpBuffer);
    bufferLength_ = newSize;
  }
  else
  {
    throw std::invalid_argument("newSize must be bigger or equal current size");
  }
}
