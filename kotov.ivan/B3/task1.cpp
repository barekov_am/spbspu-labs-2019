#include <forward_list>
#include <iostream>
#include <string>
#include <map>
#include <cstring>
#include <algorithm>
#include "telephonebook.hpp"

std::string nextWord(char *& begin, char *& end)
{
  while ((*begin == ' ' || *begin == '\t') && *begin != 0) begin++;
  if (*begin == 0)
    throw std::invalid_argument("<INVALID COMMAND>\n");
  end = begin + 1;
  while (*end != ' ' && *end != '\t' && *end != 0) end++;
  char tmp = *end;
  *end = 0;
  std::string result(begin);
  *end = tmp;
  return result;
}

void task1()
{
  typedef std::pair< std::string, kotov::TelephoneBook::iterator > Element;
  std::map< std::string, kotov::TelephoneBook::iterator > bookMarks;
  kotov::TelephoneBook telBook;
  bookMarks.insert(Element("current", telBook.begin()));
  std::string command, current;
  char line[255];
  char * beginWord;
  char * endWord;
  kotov::node_t node;
  while (!std::cin.eof())
  {
    std::cin.getline(line, 255);
    if (std::cin.eof()) break;
    beginWord = line;
    try
    {
      command = nextWord(beginWord, endWord);
    }
    catch (std::invalid_argument ex)
    {
      std::cout << ex.what();
      continue;
    }

    if (command.compare("add") == 0)
    {
      try
      {
        bool wereEmpty = telBook.isEmpty();
        telBook.pushBack(kotov::createNodeFromCharArr(endWord));
        if (wereEmpty)
        {
          auto iter = bookMarks.begin();
          while(iter != bookMarks.cend())
          {
            iter->second = telBook.begin();
            iter++;
          }
        }
      }
      catch (std::invalid_argument ex)
      {
        std::cout << ex.what();
      }
    }
    else if (command.compare("store") == 0)
    {
      try
      {
        command = nextWord(endWord, beginWord);
        kotov::TelephoneBook::iterator tmp = bookMarks.at(command);
        command = nextWord(beginWord, endWord);
        bookMarks.insert(Element(command, tmp));
      }
      catch (std::out_of_range)
      {
        std::cout << "<INVALID BOOKMARK>\n";
      }
      catch (std::invalid_argument)
      {
        std::cout << "<INVALID BOOKMARK>\n";
      }
    }
    else if (command.compare("insert") == 0)
    {
      try
      {
        command = nextWord(endWord, beginWord);
        if (command.compare("after") == 0)
        {
          command = nextWord(beginWord, endWord);
          try
          {
            bookMarks.at(command);
            if (telBook.isEmpty())
            {
              telBook.pushBack(kotov::createNodeFromCharArr(endWord));
              for(auto elem: bookMarks)
              {
                bookMarks.at(elem.first) = telBook.begin();
              }
            }
            else
            {
              telBook.insertAfter(bookMarks.at(command), kotov::createNodeFromCharArr(endWord));
            }
          }
          catch (std::out_of_range)
          {
            std::cout << "<INVALID BOOKMARK>\n";
          }
          catch (std::invalid_argument ex)
          {
            std::cout << ex.what();
          }
        }
        else if (command.compare("before") == 0)
        {
          command = nextWord(beginWord, endWord);
          try
          {
            bookMarks.at(command);
            if (telBook.isEmpty())
            {
              telBook.pushBack(kotov::createNodeFromCharArr(endWord));
              for(auto elem: bookMarks)
              {
                bookMarks.at(elem.first) = telBook.begin();
              }
            }
            else
            {
              telBook.insertBefore(bookMarks.at(command), kotov::createNodeFromCharArr(endWord));
            }
          }
          catch (std::out_of_range)
          {
            std::cout << "<INVALID BOOKMARK>\n";
          }
          catch (std::invalid_argument ex)
          {
            std::cout << ex.what();
          }
        }
        else
        {
          std::cout << "<INVALID COMMAND>\n";
        }
      }
      catch (std::invalid_argument ex)
      {

      }
    }
    else if (command.compare("delete") == 0)
    {
      try
      {
        command = nextWord(endWord, beginWord);
        kotov::TelephoneBook::iterator old = bookMarks.at(command);
        kotov::TelephoneBook::iterator newB = telBook.remove(old);
        auto it = bookMarks.begin();
        while (it != bookMarks.cend())
        {
          if ((*it).second == old)
          {
            (*it).second = newB;
          }
          it++;
        }
      }
      catch (std::out_of_range)
      {
        std::cout << "<INVALID BOOKMARK>\n";
      }
      catch (std::invalid_argument)
      {
        std::cout << "<INVALID BOOKMARK>\n";
      }
    }
    else if (command.compare("show") == 0)
    {
      try
      {
        command = nextWord(endWord, beginWord);
        bookMarks.at(command);
        if (!telBook.isEmpty())
        {
          auto iter = bookMarks.at(command);
          kotov::node_t tmp = *iter;
          std::cout << tmp.phoneNumber << " " << tmp.name << std::endl;
        }
        else
        {
          std::cout << "<EMPTY>\n";
        }
      }
      catch (std::out_of_range)
      {
        std::cout << "<INVALID BOOKMARK>\n";
      }
      catch (std::invalid_argument ex)
      {
        std::cout << ex.what();
      }
    }
    else if (command.compare("move") == 0)
    {
      try
      {
        command = nextWord(endWord, beginWord);
        kotov::TelephoneBook::iterator * tmp = &bookMarks.at(command);
        command = nextWord(beginWord, endWord);
        if (command.compare("first") == 0)
        {
          *tmp = telBook.begin();
        }
        else if (command.compare("last") == 0)
        {
          *tmp = telBook.end();
        }
        else
        {
          if (kotov::checkNumber(beginWord))
          {
            try
            {
              int delta;
              sscanf(beginWord, "%d", &delta);
              *tmp = telBook.changePosition(*tmp, delta);
            }
            catch (std::out_of_range)
            {
              std::cout << "<INVALID STEP>\n";
            }
          }
          else
          {
            std::cout << "<INVALID STEP>\n";
          }
        }
      }
      catch (std::out_of_range)
      {
        std::cout << "<INVALID BOOKMARK>\n";
      }
      catch (std::invalid_argument ex)
      {
        std::cout << ex.what();
      }
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
  if (!std::cin.eof() && std::cin.fail()) throw std::invalid_argument("read error");
}
