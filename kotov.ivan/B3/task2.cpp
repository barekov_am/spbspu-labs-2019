#include <forward_list>
#include <iostream>
#include "factorialconteiner.hpp"

void task2()
{
  int res[10];
  kotov::factorialConteiner container;
  kotov::iterator iterator = container.begin();
  while (iterator != container.end())
  {
    std::cout << *iterator << ' ';
    iterator++;
  }
  std::cout << std::endl;
  std::copy(container.begin(), container.end(), res);
  for (int i = 9; i >= 0; i--)
  {
    std::cout << res[i] << ' ';
  }
  std::cout << std::endl;
}
