#ifndef FACTORIALCONTEINER_HPP
#define FACTORIALCONTEINER_HPP
#include <iterator>

namespace kotov
{
  class factorialConteiner;

class iterator: public std::iterator<std::bidirectional_iterator_tag, int>
{
public:

  iterator(int pos, const factorialConteiner * container);

  int operator *();

  int * operator ->();

  bool operator ==(const iterator &) const;
  bool operator !=(const iterator &) const;
  iterator & operator ++();
  iterator operator ++(int);
  iterator & operator --();
  iterator operator --(int);

private:
  int m_pos;
  const factorialConteiner * m_container;
  int m_cachedFactorial;
};

class factorialConteiner
{
public:
  factorialConteiner();

  int get(int n) const;

  iterator begin() const;

  iterator end() const;

  iterator cbegin() const;

  iterator cend() const;
};

}
#endif // FACTORIALCONTEINER_HPP
