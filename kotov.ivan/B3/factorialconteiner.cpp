#include "factorialconteiner.hpp"
int factorial(int num)
{
  int out = 1;
  for (;num > 1; num--)
  {
    out *= num;
  }
  return out;
}
kotov::iterator::iterator(int pos, const kotov::factorialConteiner * container):
  m_pos(pos),
  m_container(container)
{
}

int kotov::iterator::operator *()
{
  return m_container->get(m_pos);
}

int * kotov::iterator::operator ->()
{
  m_cachedFactorial = m_container->get(m_pos);
  return &m_cachedFactorial;
}

bool kotov::iterator::operator ==(const iterator & iter) const
{
  return (m_pos == iter.m_pos) && (m_container == iter.m_container);
}

bool kotov::iterator::operator !=(const iterator & iter) const
{
  return !this->operator==(iter);
}

kotov::iterator & kotov::iterator::operator ++()
{
  m_pos++;
  return *this;
}

kotov::iterator kotov::iterator::operator ++(int)
{
  iterator tmp = *this;
  m_pos++;
  return tmp;
}

kotov::iterator & kotov::iterator::operator --()
{
  m_pos--;
  return *this;
}

kotov::iterator kotov::iterator::operator --(int)
{
  iterator tmp = *this;
  m_pos--;
  return tmp;
}

kotov::factorialConteiner::factorialConteiner()
{
}

int kotov::factorialConteiner::get(int n) const
{
  if (n > 0 && n < 11)
  {
    return factorial(n);
  }
  else
  {
    throw std::out_of_range("out of range");
  }
}

kotov::iterator kotov::factorialConteiner::begin() const
{
  return kotov::iterator(1, this);
}

kotov::iterator kotov::factorialConteiner::end() const
{
  return kotov::iterator(11, this);
}

kotov::iterator kotov::factorialConteiner::cbegin() const
{
  return kotov::iterator(1, this);
}

kotov::iterator kotov::factorialConteiner::cend() const
{
  return kotov::iterator(11, this);
}
