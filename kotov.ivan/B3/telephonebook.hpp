#ifndef TELEPHONEBOOK_HPP
#define TELEPHONEBOOK_HPP
#include <list>
#include <string>
#include <memory>
#include <iterator>

namespace kotov
{
  struct node_t
  {
    std::string name;
    std::string phoneNumber;
  };


  node_t createNodeFromCharArr(char *& position);
  bool checkNumber(char * str);

  class TelephoneBook
  {
  public:
    TelephoneBook();
    TelephoneBook(const TelephoneBook &);
    TelephoneBook(TelephoneBook &&);
    ~TelephoneBook() = default;
    TelephoneBook & operator =(const TelephoneBook &);
    TelephoneBook & operator =(TelephoneBook &&);
    bool operator ==(const TelephoneBook &) const;

    typedef std::list< node_t >::iterator iterator;
    typedef std::list< node_t >::const_iterator const_iterator;

    const_iterator cbegin() const;
    const_iterator cend() const;
    iterator begin() const;
    iterator end() const;


    bool isEmpty() const;
    void insertBefore(const iterator & iter, node_t node);
    void insertAfter(iterator iter, node_t node);
    void pushBack(node_t node);
    iterator changePosition(iterator iter, int offset) const;
    iterator remove(iterator iter);

  private:
    std::shared_ptr< std::list< node_t > > m_container;
  };

}
#endif // TELEPHONEBOOK_HPP
