#include "telephonebook.hpp"

bool kotov::checkNumber(char * str)
{
  while ((*str != 0) && (*str == ' ' || *str == '\t')) str++;
  if (*str == 0) return false;
  if (*str == '+' || *str == '-') str++;
  while (*str != 0 && *str != ' ' && *str != '\t')
  {
    if (!std::isdigit(*str)) return false;
    str++;
  }
  return true;
}

kotov::node_t kotov::createNodeFromCharArr(char *& position)
{
  kotov::node_t result;
  char tmp;
  while ((*position == ' ' || *position == '\t') && *position != 0) position++;
  if (*position == 0)
    throw std::invalid_argument("<INVALID COMMAND>\n");
  char * end = position;
  while (*end != ' ' && *end != '\t' && *end != 0) end++;
  if (*end == 0)
    throw std::invalid_argument("<INVALID COMMAND>\n");
  tmp = *end;
  *end = 0;
  if (checkNumber(position))
  {
    result.phoneNumber = std::string(position);
    *end = tmp;
    while ((*end == ' ' || *end == '\t') && *end != '0') end++;
    if (*end != '"')
      throw std::invalid_argument("<INVALID COMMAND>\n");
    position = ++end;
    int delta = 0;
    while (*end != 0 && *end != '"')
    {
      if (*end == '\\')
      {
        delta++;
        end++;
      }
      tmp = end[0];
      *(end - delta) = tmp;
      end++;
    }
    if (*end == 0)
      throw std::invalid_argument("<INVALID COMMAND>\n");
    end = end - delta;
    if (end != position)
    {
      tmp = *end;
      *end = 0;
      result.name = std::string(position);
      *end = tmp;
      position = end;
      return result;
    }
  }
  throw std::invalid_argument("<INVALID COMMAND>\n");
}

kotov::TelephoneBook::TelephoneBook():
  m_container(new std::list< node_t >)
{
}

kotov::TelephoneBook::TelephoneBook(const TelephoneBook & book)
{
  m_container = book.m_container;
}

kotov::TelephoneBook::TelephoneBook(TelephoneBook && book)
{
  m_container = std::move(book.m_container);
}

kotov::TelephoneBook & kotov::TelephoneBook::operator =(const TelephoneBook & book)
{
  m_container = book.m_container;
  return *this;
}

kotov::TelephoneBook & kotov::TelephoneBook::operator =(TelephoneBook && book)
{
  m_container = std::move(book.m_container);
  return *this;
}

bool kotov::TelephoneBook::operator ==(const TelephoneBook & book) const
{
  return book.m_container == m_container;
}

kotov::TelephoneBook::const_iterator kotov::TelephoneBook::cbegin() const
{
  return m_container->cbegin();
}
kotov::TelephoneBook::const_iterator kotov::TelephoneBook::cend() const
{
  if (!m_container->empty())
  {
    return --m_container->end();
  }
  else
  {
    return m_container->cbegin();
  }
}
kotov::TelephoneBook::iterator kotov::TelephoneBook::begin() const
{
  return m_container->begin();
}
kotov::TelephoneBook::iterator kotov::TelephoneBook::end() const
{
  if (!m_container->empty())
  {
    return --m_container->end();
  }
  else
  {
    return m_container->begin();
  }
}

bool kotov::TelephoneBook::isEmpty() const
{
  return m_container->empty();
}

void kotov::TelephoneBook::insertBefore(const kotov::TelephoneBook::iterator & iter, node_t node)
{
  if (m_container->empty())
  {
    pushBack(node);
  }
  else
  {
    m_container->insert(iter, node);
  }
}

void kotov::TelephoneBook::insertAfter(kotov::TelephoneBook::iterator iter, node_t node)
{
  if (m_container->empty())
  {
    pushBack(node);
  }
  else
  {
    m_container->insert(++iter, node);
  }
}

void kotov::TelephoneBook::pushBack(node_t node)
{
  m_container->push_back(node);
}

kotov::TelephoneBook::iterator kotov::TelephoneBook::changePosition(kotov::TelephoneBook::iterator iter, int delta) const
{
  if (delta > 0)
  {
    for (int i = 0; i < delta; ++i)
    {
      if (iter++ == m_container->cend()) throw std::out_of_range("выход за границу");
    }
  }
  else
  {
    for (int i = 0; delta < i; --i)
    {
      if (iter-- == m_container->cbegin()) throw std::out_of_range("выход за границу");
    }
  }
  return iter;
}

kotov::TelephoneBook::iterator kotov::TelephoneBook::remove(kotov::TelephoneBook::iterator iter)
{
  iter = m_container->erase(iter);
  if (iter == m_container->cend() && !m_container->empty()) iter--;
  return iter;
}
