#include <forward_list>
#include <iostream>
#include <string>
#include <set>
#include <cstring>
#include <algorithm>

std::string nextWord(char *& begin, char *& end)
{
  while ((*begin == ' ' || *begin == '\t') && *begin != 0) begin++;
  if (*begin == 0)
    throw std::invalid_argument("<INVALID COMMAND>\n");
  end = begin + 1;
  while (*end != ' ' && *end != '\t' && *end != 0) end++;
  char tmp = *end;
  *end = 0;
  std::string result(begin);
  *end = tmp;
  return result;
}

void task1()
{
  std::set< std::string > words;
  std::string currentWord;
  char line[255];
  char * beginWord;
  char * endWord;
  while (!std::cin.eof())
  {
    std::cin.getline(line, 255);
    if (std::cin.eof()) break;
    endWord=line;
    beginWord=line;
    while (*beginWord != 0)
    {
      try
      {
        currentWord = nextWord(beginWord, endWord);
      }
      catch(std::invalid_argument ex)
      {
        break;
      }
      beginWord = endWord;
      if (words.find(currentWord) == words.end())
      {
        std::cout << currentWord << std::endl;
        words.insert(currentWord);
      }
    }
  }
}
