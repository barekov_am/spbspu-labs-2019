#include <forward_list>
#include <iostream>
#include <math.h>
#include <limits>
#include <list>
#include "shape.hpp"

char*& nextSymbol(char *& pos, char symbol)
{
  while (*pos != symbol && *pos != 0) pos++;
  return pos;
}

bool isWSLine(char * line)
{
  while ((*line == ' ' || *line == '\t' || *line == '\v') && *line != 0) line++;
  if (*line != 0) return false;
  return true;
}

char*& nextNumber(char *& pos)
{
  while ((*pos < '0' || *pos > '9')  && *pos != '-' && *pos != '+' && *pos != 0) pos++;
  return pos;
}

void task2()
{
  char line[255];
  char * curPos;
  std::list< Shape > shapes;
  std::vector< Point > outPoints;
  while (!std::cin.eof())
  {
    std::cin.getline(line, 255);
    curPos = line;
    if (isWSLine(line)) continue;
    int points = -1;
    if (sscanf(line, "%d", &points) != 1 || points < 1)
    {
      throw std::invalid_argument("invalid argument");
    }
    Shape curShape;
    for (int foundedPoints = 0; foundedPoints < points; foundedPoints++)
    {
      Point curPoint;
      nextSymbol(curPos, '(');
      if (*curPos == 0 || sscanf(nextNumber(curPos), "%d", &curPoint.x) != 1
          || sscanf(nextNumber(nextSymbol(curPos, ';')), "%d", &curPoint.y) != 1 || *nextSymbol(curPos, ')') == 0)
            throw std::invalid_argument("invalid argument");
      curShape.push_back(curPoint);
    }
    nextSymbol(curPos, '(');
    if (*curPos != 0)
    {
      throw std::invalid_argument("invalid argument");
    }
    shapes.push_back(curShape);
  }
  int sumPoints = 0, numTreangles = 0, numSquares = 0, numRectangles = 0;
  auto iter = shapes.begin(), endTri = shapes.begin(), endSqu = shapes.begin(), endRect = shapes.begin();
  while (iter != shapes.end())
  {
    Shape & shape = *iter;
    sumPoints += shape.size();
    switch (shape.size())
    {
    case 3:
      {
        if (iter != endTri)
        {
          auto tmpIter = iter++;
          shapes.splice(endTri, shapes, tmpIter);
          iter--;
        }
        else
        {
          {
            endTri++;
            endSqu++;
            endRect++;
          }
        }
        outPoints.push_back(shape.front());
        numTreangles++;
        break;
      }
    case 4:
      {
        Point a = shape[0], b = shape[1], c = shape[2], d = shape[3];
        double eps = std::numeric_limits< double >().epsilon();
        bool parallel = (abs(a.x - b.x) == abs(c.x - d.x) && abs(a.y - b.y) == abs(c.y - d.y));
        bool equality = (abs(sqrt(sqr(a.x - b.x) + sqr(a.y - b.y)) - sqrt(sqr(b.x - c.x) + sqr(b.y - c.y))) < eps);
        if (parallel)
        {
          if (equality)
          {
            if (iter != endSqu)
            {
              auto tmpIter = iter++;
              shapes.splice(endSqu, shapes, tmpIter);
              iter--;
              if (numSquares == 0)
              {
                endTri = endSqu;
                endTri--;
              }
            }
            else
            {
              endSqu++;
              endRect++;
            }
            numSquares++;
          }
          else
          {
            if (iter != endRect)
            {
              auto tmpIter = iter++;
              shapes.splice(endRect, shapes, tmpIter);
              iter--;
              if (numRectangles == 0)
              {
                endSqu = endRect;
                endSqu--;
                if (numSquares == 0) endTri = endSqu;
              }
            }
            else
            {
              endRect++;
            }
            numRectangles++;
          }
          outPoints.push_back(shape.front());
        }
        break;
      }
    case 5:
      if (iter == endSqu) endSqu++;
      if (iter == endTri) endTri++;
      if (iter == endRect) endRect++;
      iter = shapes.erase(iter);
      iter--;
      break;
    default:
      outPoints.push_back(shape.front());
    }
    iter++;
  }
  std::cout << "Vertices: " << sumPoints << std::endl;
  std::cout << "Triangles: " << numTreangles << std::endl;
  std::cout << "Squares: " << numSquares << std::endl;
  std::cout << "Rectangles: " << numRectangles + numSquares << std::endl;
  std::cout << "Points:";
  for (auto point: outPoints)
  {
    std::cout << " (" << point.x << "; " << point.y << ')';
  }
  std::cout << std::endl << "Shapes: ";
  for (auto shape: shapes)
  {
    std::cout << std::endl << shape.size();
    for (auto point: shape)
    {
      std::cout << " (" << point.x << "; " << point.y << ')';
    }
  }
}
