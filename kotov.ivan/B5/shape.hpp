#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <vector>
#include <functional>
#include <limits>

struct Point
{
  int x, y;
};

using Shape = std::vector< Point >;

template < typename T >
T sqr(T num)
{
  return num * num;
}

template < typename T >
T abs(T num)
{
  return std::less< T >()(num , static_cast< T >(0)) ? (-num): num;
}

#endif // SHAPE_HPP
