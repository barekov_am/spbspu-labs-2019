#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

int main()
{
  try
  {
    kotov::Circle circle({25, 25}, 5);
    kotov::CompositeShape composite;
    composite += &circle;
    composite = composite + new kotov::Rectangle({55, 55}, 10, 10);
    if (composite.indexOf(&circle) != -1)
    {
      std::cout << "index of " << circle.getShapeName() << " in compositeShape is " << composite.indexOf(&circle) << std::endl;
    }
    std::cout << std::endl << std::endl;
    std::cout << "Area of " << composite[0]->getShapeName() << ": " << composite[0]->getArea() << std::endl;
    std::cout << "Area of " << composite[1]->getShapeName() << ": " << composite[1]->getArea() << std::endl;
    std::cout << "Area of " << composite.getShapeName() << ": " << composite.getArea() << std::endl;
    std::cout << "position of " << composite[0]->getShapeName() << ": " << composite[0]->getFrameRect().pos.x;
    std::cout << ", " << composite[0]->getFrameRect().pos.y << std::endl;
    std::cout << "position of " << composite[1]->getShapeName() << ": " << composite[1]->getFrameRect().pos.x;
    std::cout << ", " << composite[1]->getFrameRect().pos.y << std::endl;
    std::cout << "position of " << composite.getShapeName() << ": " << composite.getFrameRect().pos.x;
    std::cout << ", " << composite.getFrameRect().pos.y << std::endl;
    std::cout << "widht and height frame of " << composite.getShapeName() << ": " << composite.getFrameRect().width;
    std::cout << ", " << composite.getFrameRect().height << std::endl << std::endl;
    std::cout << "moving circle and rectangle" << std::endl << std::endl;
    composite[0]->move(-5, -5);
    composite[1]->move({5, 5});
    std::cout << "Area of " << composite[0]->getShapeName() << ": " << composite[0]->getArea() << std::endl;
    std::cout << "Area of " << composite[1]->getShapeName() << ": " << composite[1]->getArea() << std::endl;
    std::cout << "Area of " << composite.getShapeName() << ": " << composite.getArea() << std::endl;
    std::cout << "position of " << composite[0]->getShapeName() << ": ";
    std::cout << composite[0]->getFrameRect().pos.x << ", " << composite[0]->getFrameRect().pos.y << std::endl;
    std::cout << "position of " << composite[1]->getShapeName() << ": ";
    std::cout << composite[1]->getFrameRect().pos.x << ", " << composite[1]->getFrameRect().pos.y << std::endl;
    std::cout << "position of " << composite.getShapeName() << " : ";
    std::cout << composite.getFrameRect().pos.x << ", " << composite.getFrameRect().pos.y << std::endl;
    std::cout << "widht and height frame of " << composite.getShapeName() << ": " << composite.getFrameRect().width;
    std::cout << ", " << composite.getFrameRect().height << std::endl << std::endl;
    std::cout << "moving compositeShape" << std::endl << std::endl;
    composite.move(5, 5);
    std::cout << "Area of " << composite[0]->getShapeName() << ": " << composite[0]->getArea() << std::endl;
    std::cout << "Area of " << composite[1]->getShapeName() << ": " << composite[1]->getArea() << std::endl;
    std::cout << "Area of " << composite.getShapeName() << ": ";
    std::cout << composite.getArea() << std::endl;
    std::cout << "position of " << composite[0]->getShapeName() << ": ";
    std::cout << composite[0]->getFrameRect().pos.x << ", " << composite[0]->getFrameRect().pos.y << std::endl;
    std::cout << "position of " << composite[1]->getShapeName() << ": ";
    std::cout << composite[1]->getFrameRect().pos.x << ", " << composite[1]->getFrameRect().pos.y << std::endl;
    std::cout << "position of " << composite.getShapeName() << " : ";
    std::cout << composite.getFrameRect().pos.x << ", " << composite.getFrameRect().pos.y << std::endl;
    std::cout << "widht and height frame of " << composite.getShapeName() << ": " << composite.getFrameRect().width;
    std::cout << ", " << composite.getFrameRect().height << std::endl << std::endl;
    std::cout << "scale circle and rectangle" << std::endl << std::endl;
    const double scale = 4.0;
    composite[0]->scale(scale);
    composite[1]->scale(scale);
    std::cout << "Area of " << composite[0]->getShapeName() << ": " << composite[0]->getArea() << std::endl;
    std::cout << "Area of " << composite[1]->getShapeName() << ": " << composite[1]->getArea() << std::endl;
    std::cout << "Area of " << composite.getShapeName() << ": " << composite.getArea() << std::endl;
    std::cout << "position of " << composite[0]->getShapeName() << ": ";
    std::cout << composite[0]->getFrameRect().pos.x << ", " << composite[0]->getFrameRect().pos.y << std::endl;
    std::cout << "position of " << composite[1]->getShapeName() << ": ";
    std::cout << composite[1]->getFrameRect().pos.x << ", " << composite[1]->getFrameRect().pos.y << std::endl;
    std::cout << "position of " << composite.getShapeName() << ": ";
    std::cout << composite.getFrameRect().pos.x << ", " << composite.getFrameRect().pos.y << std::endl;
    std::cout << "widht and height frame of " << composite.getShapeName() << ": " << composite.getFrameRect().width;
    std::cout << ", " << composite.getFrameRect().height << std::endl << std::endl;
    std::cout << "scale compositeShape" << std::endl << std::endl;
    composite.scale(1.0 / scale);
    std::cout << "Area of " << composite[0]->getShapeName() << ": " << composite[0]->getArea() << std::endl;
    std::cout << "Area of " << composite[1]->getShapeName() << ": " << composite[1]->getArea() << std::endl;
    std::cout << "Area of " << composite.getShapeName() << ": " << composite.getArea() << std::endl;
    std::cout << "position of " << composite[0]->getShapeName() << ": ";
    std::cout << composite[0]->getFrameRect().pos.x << ", " << composite[0]->getFrameRect().pos.y << std::endl;
    std::cout << "position of " << composite[1]->getShapeName() << ": ";
    std::cout << composite[1]->getFrameRect().pos.x << ", " << composite[1]->getFrameRect().pos.y << std::endl;
    std::cout << "position of " << composite.getShapeName() << ": ";
    std::cout << composite.getFrameRect().pos.x << ", " << composite.getFrameRect().pos.y << std::endl;
    std::cout << "widht and height frame of " << composite.getShapeName() << ": " << composite.getFrameRect().width;
    std::cout << ", " << composite.getFrameRect().height << std::endl << std::endl;
    composite -= &circle;
    if (composite.indexOf(&circle) == -1)
    {
      std::cout << "circle deleted from compositeShape" << std::endl;
    }
    delete  composite[0];
    composite.clear();
    if (composite.size() == 0)
    {
      std::cout << "clear has been finished successfully" << std::endl;
    }
  }
  catch (std::invalid_argument ex)
  {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  catch (std::out_of_range ex)
  {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    return 2;
  }
  return 0;
}
