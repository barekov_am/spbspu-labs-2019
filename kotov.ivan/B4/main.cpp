#include <forward_list>
#include <iostream>
#include <type_traits>
#include <cstring>

void task();

int main()
{
  try
    {
      task();
    }
    catch (std::invalid_argument ex)
    {
      std::cerr << ex.what() << std::endl;
      return 1;
    }
    catch (...)
    {
      std::cerr << "program failed\n";
      return 2;
    }
    return 0;
}
