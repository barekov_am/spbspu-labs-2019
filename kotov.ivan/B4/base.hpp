#ifndef BASE_HPP
#define BASE_HPP
#include <string>
#include <iostream>

namespace kotov
{
  struct DataStruct
  {
    int key1;
    int key2;
    std::string str;

    friend std::ostream & operator << (std::ostream & stream, const DataStruct & data);
  };

  DataStruct readDataStruct(std::istream & stream);
}


#endif // BASE_HPP
