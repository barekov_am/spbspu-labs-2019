#include "base.hpp"

namespace kotov
{

std::ostream & operator << (std::ostream & stream, const kotov::DataStruct & data)
{
  stream << data.key1 << ',' << data.key2 << ',' << data.str;
  return stream;
}

kotov::DataStruct readDataStruct(std::istream & stream)
{
  kotov::DataStruct data;
  if (stream.eof()) throw std::invalid_argument("invalid argument");
  stream >> data.key1;
  if ((data.key1 > 5) || (data.key1 < -5) || stream.eof() || stream.get() != ',' || stream.eof())
      throw std::invalid_argument("invalid argument");
  char tmpc = stream.get() % 255;
  if (stream.eof() || tmpc == '\r' || tmpc == '\n')
      throw std::invalid_argument("invalid argument");
  stream.unget();
  stream >> data.key2;
  if ((data.key2 > 5) || (data.key2 < -5) || stream.eof() || stream.get() != ',' || stream.eof())
      throw std::invalid_argument("invalid argument");
  std::getline(stream, data.str);
  if (stream.eof())
      throw std::invalid_argument("invalid argument");
  return data;
}

}

