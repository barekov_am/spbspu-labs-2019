#include <forward_list>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <math.h>
#include "base.hpp"

void task()
{
  std::vector< kotov::DataStruct > vector;
  std::cin.get();
  while (!std::cin.eof() && !std::cin.fail())
  {
    std::cin.unget();
    try
    {
      vector.push_back(kotov::readDataStruct(std::cin));
    }
    catch (std::invalid_argument ex)
    {
      std::cin.sync();
      throw ex;
    }
    char tmpc = std::cin.get() % 255;
    while ( !std::cin.eof() && (tmpc == 10 || tmpc == 13))
        tmpc = std::cin.get() % 255;
  }
  std::sort(vector.begin(), vector.end(), [](kotov::DataStruct _first, kotov::DataStruct _second)
  {
    if (_first.key1 < _second.key1) return true;
    if (_first.key1 == _second.key1)
    {
      if (_first.key2 < _second.key2) return true;
      if (_first.key2 == _second.key2)
      {
        if (_first.str.length() < _second.str.length()) return true;
      }
    }
    return false;
  });
  auto iter = vector.cbegin();
  while (iter != vector.cend())
  {
    std::cout << *iter << std::endl;
    iter++;
  }
}
