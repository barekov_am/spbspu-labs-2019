#include <forward_list>
#include <iostream>
#include <vector>
#include <algorithm>

bool checkLine(char * line)
{
  char * curpos = line;
  while (*curpos != 0)
  {
    if ((*curpos < '0' || *curpos > '9') && (*curpos != ' ') && (*curpos != '\t') && (*curpos != '\v')
        && (*curpos != '+') && (*curpos != '-')) return false;
    curpos++;
  }
  return true;
}

char *& nextWord(char *& curpos)
{
  while ((*curpos >= '0' && *curpos <= '9') || (*curpos == '+') || (*curpos == '-')) curpos++;
  while (*curpos != 0 && (*curpos == ' ' || *curpos == '\t' || *curpos == '\v'))
  {
    curpos++;
  }
  return curpos;
}

void task1()
{
  std::vector< int > array;
  static bool hasFirst = false;
  static int min = 0x7FFFFFFF, max = 0x80000000, numPositive = 0, numNegative = 0, first, last;
  static long long sum = 0,  sumOdd = 0, sumEven = 0;
  char line[255];
  while (!std::cin.eof())
  {
    int tmp;
    std::cin.getline(line, 255);
    if (!checkLine(line)) throw std::invalid_argument("contain invalid symbol");
    char * curpos = line;
    while (*curpos != 0)
    {
      tmp = std::atoi(curpos);
      array.push_back(tmp);
      nextWord(curpos);
    }
  }
  if (array.empty())
  {
    std::cout << "No Data\n";
    return;
  }
  std::for_each(array.cbegin(), array.cend(), [](int a)
  {
    if (!hasFirst)
    {
      first = a;
      hasFirst = true;
    }
    last = a;
    if (a < min)
    {
      min = a;
    }
    if (max < a)
    {
      max = a;
    }
    sum += a;
    if (a > 0)
    {
      numPositive++;
    }
    else if (a < 0)
    {
      numNegative++;
    }
    if (std::abs(a % 2) == 1)
    {
      sumOdd += a;
    }
    else
    {
      sumEven += a;
    }
  });
  std::cout << "Max: " << max;
  std::cout << "\nMin: " << min;
  std::cout << "\nMean: ";
  double mean = static_cast< double >(sum) / array.size() ;
  printf("%f", mean);
  std::cout << "\nPositive: " << numPositive;
  std::cout << "\nNegative: " << numNegative;
  std::cout << "\nOdd Sum: " << sumOdd;
  std::cout << "\nEven Sum: " << sumEven;
  std::cout << "\nFirst/Last Equal: ";
  if (first == last)
  {
    std::cout << "yes";
  }
  else
  {
    std::cout << "no";
  }
}
