#include <forward_list>
#include <iostream>
#include <cstring>

void task1();

int main()
{
  try
    {
      task1();
    }
    catch (std::invalid_argument ex)
    {
      std::cerr << ex.what() << std::endl;
      return 1;
    }
    catch (...)
    {
      std::cerr << "program failed\n";
      return 2;
    }
    return 0;
}
