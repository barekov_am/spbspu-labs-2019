#include "circle.hpp"
#include <stdexcept>

baho::Circle::Circle(const baho::point_t &center, double radius):
  Shape(center),
  m_radius(radius)
{
  if (m_radius <= 0)
  {
    throw std::invalid_argument("Invalid radius of circle");
  }
}

const char * baho::Circle::getShapeName() const
{
  return "CIRCLE";
}

void baho::Circle::draw(std::ostream & stream) const
{
  stream << getShapeName() << " (" << m_center.x << ";" << m_center.y << ")\n";
}
