#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <ostream>
#include "base-types.hpp"

namespace baho
{
  class Shape
  {
  public:

    Shape(const baho::point_t & center);

    virtual ~Shape() = default;

    virtual const char * getShapeName() const = 0;
    bool isMoreLeft(const Shape *) const;
    bool isUpper(const Shape *) const;
    virtual void draw(std::ostream & stream) const = 0;

    protected:
    baho::point_t m_center;
  };
}

#endif // SHAPE_HPP
