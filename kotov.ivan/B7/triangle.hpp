#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"
#include <math.h>

namespace baho
{
  class Triangle : public baho::Shape
  {
  public:
    Triangle(const baho::point_t & center, const baho::point_t & vector1 = {0, 1},
        const baho::point_t & vector2 ={1, 1},
          const baho::point_t & vector3 = {1, 0});
    virtual const char * getShapeName() const override;
    virtual void draw(std::ostream & stream) const override;

  private:
    baho::point_t m_vectors[3];
  };
}

#endif // TRIANGLE_HPP
