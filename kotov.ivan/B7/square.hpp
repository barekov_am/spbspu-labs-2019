#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace baho
{
  class Square : public Shape
  {
  public:

    Square(const baho::point_t & center, double width = 1, double height = 1);

    virtual const char * getShapeName() const override;
    virtual void draw(std::ostream & stream) const override;

  protected:
    double m_width;
    double m_height;
  };
}
#endif // RECTANGLE_HPP
