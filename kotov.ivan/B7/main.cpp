#include <forward_list>
#include <iostream>
#include <cstring>

void task1();
void task2();

int main(int argc, char ** argv)
{
  try
      {
        if (argc != 2)
        {
          std::cerr << "invalid arguments";
          return 1;
        }
        if (argv[1][1] != 0 || (argv[1][0] - '0' < 1) || (argv[1][0] - '0' > 2))
        {
          std::cerr << "invalid task number";
          return 1;
        }
        switch (argv[1][0])
        {
        case '1':
          task1();
          break;
        case '2':
          task2();
        }
      }
      catch (std::invalid_argument ex)
      {
        std::cerr << ex.what() << std::endl;
        return 1;
      }
      catch (...)
      {
        std::cerr << "program failed\n";
        return 2;
      }
      return 0;
}
