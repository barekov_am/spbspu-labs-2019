#include "square.hpp"
#include <stdexcept>
#include <math.h>

baho::Square::Square(const baho::point_t & center, double width, double height):
  Shape(center),
  m_width(width),
  m_height(height)
{
  if ((m_width <= 0) || (m_height <= 0))
  {
    throw std::invalid_argument("Invalid width or height of rectangle");
  }
}

const char *baho::Square::getShapeName() const
{
  return "SQUARE";
}

void baho::Square::draw(std::ostream & stream) const
{
  stream << getShapeName() << " (" << m_center.x << ";" << m_center.y << ")\n";
}
