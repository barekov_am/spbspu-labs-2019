#include "triangle.hpp"

baho::Triangle::Triangle(const baho::point_t & center,
                         const baho::point_t & vector1,
                         const baho::point_t & vector2,
                         const baho::point_t & vector3):
  Shape(center),
  m_vectors
  {
    vector1, vector2, vector3
  }
{
}

const char * baho::Triangle::getShapeName() const
{
  return "TRIANGLE";
}

void baho::Triangle::draw(std::ostream & stream) const
{
  stream << getShapeName() << " (" << m_center.x << ";" << m_center.y << ")\n";
}
