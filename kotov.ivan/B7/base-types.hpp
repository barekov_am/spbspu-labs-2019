#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace baho
{
  struct point_t
  {
    int x;
    int y;
  };
}

#endif // BASE_TYPES_HPP
