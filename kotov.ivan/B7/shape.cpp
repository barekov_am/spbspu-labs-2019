#include "shape.hpp"
#include "base-types.hpp"

baho::Shape::Shape(const baho::point_t & center):
  m_center(center)
{
}

bool baho::Shape::isMoreLeft(const Shape * other) const
{
  if (other->m_center.x > m_center.x) return true;
  return false;
}

bool baho::Shape::isUpper(const Shape * other) const
{
  if (m_center.y > other->m_center.y) return true;
  return false;
}

