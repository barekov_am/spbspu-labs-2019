#include <forward_list>
#include <iostream>
#include <list>
#include <algorithm>
#include <math.h>
#include <cstring>
#include "shape.hpp"
#include "square.hpp"
#include "circle.hpp"
#include "triangle.hpp"


bool isEmptyLine(char * curpos)
{
  while (*curpos != 0 && ((*curpos == ' ' || *curpos == '\t' || *curpos == '\v'))) curpos++;
  if (*curpos != 0) return false;
  return true;
}
std::string nextWord(char *& curpos)
{
  while (*curpos != 0 && (*curpos == ' ' || *curpos == '\t' || *curpos == '\v'))
  {
    curpos++;
  }
  char * begin = curpos;
  while (*curpos != 0 && ((*curpos >= '0' && *curpos <= '9') || ((*curpos >= 'a') && (*curpos <= 'z'))
      || ((*curpos >= 'A') && (*curpos <= 'Z')))) curpos++;
  char tmp = *curpos;
  *curpos = 0;
  std::string result(begin);
  *curpos = tmp;
  return result;
}

baho::point_t nextPoint(char *& curpos)
{
  while (*curpos != 0 && (*curpos == ' ' || *curpos == '\t' || *curpos == '\v'))
  {
    curpos++;
  }
  if (*curpos != '(') throw std::invalid_argument("invalid point");
  curpos++;
  while (*curpos != 0 && (*curpos == ' ' || *curpos == '\t' || *curpos == '\v'))
  {
    curpos++;
  }
  if ((*curpos < '0' || *curpos > '9') && (*curpos != '-') && (*curpos != '+')) throw std::invalid_argument("invalid x");
  int x = 0;
  sscanf(curpos, "%d", &x);
  curpos++;
  while (*curpos >= '0' && *curpos <= '9') curpos++;
  while (*curpos != 0 && (*curpos == ' ' || *curpos == '\t' || *curpos == '\v'))
  {
    curpos++;
  }
  if (*curpos != ';') throw std::invalid_argument("invalid point");
  curpos++;
  while (*curpos != 0 && (*curpos == ' ' || *curpos == '\t' || *curpos == '\v'))
  {
    curpos++;
  }
  if ((*curpos < '0' || *curpos > '9') && (*curpos != '-') && (*curpos != '+')) throw std::invalid_argument("invalid y");
  int y = 0;
  sscanf(curpos,"%d", &y);
  curpos++;
  while (*curpos >= '0' && *curpos <= '9') curpos++;
  while (*curpos != 0 && (*curpos == ' ' || *curpos == '\t' || *curpos == '\v'))
  {
    curpos++;
  }
  if (*curpos != ')') throw std::invalid_argument("invalid point");
  curpos++;
  return {x,y};
}

void clearShapesList(std::list< baho::Shape * > list)
{
  std::for_each(list.begin(),list.end(), [](baho::Shape * shape)
  {
    if (strcmp(shape->getShapeName(), "CIRCLE"))
    {
      delete static_cast< baho::Circle * >(shape);
    }
    else if (strcmp(shape->getShapeName(), "SQUARE"))
    {
      delete static_cast< baho::Square * >(shape);
    }
    else if (strcmp(shape->getShapeName(), "TRIANGLE"))
    {
      delete static_cast< baho::Triangle * >(shape);
    }
    else
    {
      delete shape;
    }
  });
}

void printShapes(std::list< baho::Shape * > list)
{
  std::for_each(list.cbegin(), list.cend(), [](baho::Shape * shape)
  {
    shape->draw(std::cout);
  });
}

void task2()
{
  std::list< baho::Shape * > array;
  char line[255];
  while (!std::cin.eof())
  {
    char * curpos = line;
    baho::Shape * tmp;
    std::cin.getline(line, 255);
    if (isEmptyLine(curpos)) continue;
    std::string shape_name = nextWord(curpos);
    baho::point_t center = nextPoint(curpos);
    if (shape_name == "CIRCLE")
    {
      tmp = new baho::Circle(center);
    }
    else if (shape_name == "SQUARE")
    {
      tmp = new baho::Square(center);
    }
    else if (shape_name == "TRIANGLE")
    {
      tmp = new baho::Triangle(center);
    }
    else
    {
      throw std::invalid_argument("unknown shape");
    }
    array.push_back(tmp);
  }
  std::cout << "Original:\n";
  printShapes(array);
  array.sort([](baho::Shape * first, baho::Shape * second)
  {
    return first->isMoreLeft(second);
  });
  std::cout << "Left-Right:\n";
  printShapes(array);
  array.sort([](baho::Shape * first, baho::Shape * second)
  {
    return !first->isMoreLeft(second);
  });
  std::cout << "Right-Left:\n";
  printShapes(array);
  array.sort([](baho::Shape * first, baho::Shape * second)
  {
    return first->isUpper(second);
  });
  std::cout << "Top-Bottom:\n";
  printShapes(array);
  array.sort([](baho::Shape * first, baho::Shape * second)
  {
    return !first->isUpper(second);
  });
  std::cout << "Bottom-Top:\n";
  printShapes(array);
  clearShapesList(array);
}
