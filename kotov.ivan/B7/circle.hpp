#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "base-types.hpp"
#include "shape.hpp"

namespace baho
{
  class Circle : public Shape
  {
  public:

    Circle(const baho::point_t &center, double radius = 1);

    const char * getShapeName() const override;
    virtual void draw(std::ostream & stream) const override;

  private:
    double m_radius;
  };
}

#endif // CIRCLE_HPP
