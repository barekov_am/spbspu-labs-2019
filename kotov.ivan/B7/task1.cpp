#include <forward_list>
#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>

bool checkLine(char * line)
{
  char * curpos = line;
  while (*curpos != 0)
  {
    if ((*curpos < '0' || *curpos > '9') && (*curpos != '.') && (*curpos != ' ') && (*curpos != '\t') && (*curpos != '\v')
        && (*curpos != '+') && (*curpos != '-')) return false;
    curpos++;
  }
  return true;
}

char *& nextWord(char *& curpos)
{
  while (*curpos != 0 && (*curpos != ' ' && *curpos != '\t' && *curpos != '\v')) curpos++;
  while (*curpos != 0 && (*curpos == ' ' || *curpos == '\t' || *curpos == '\v'))
  {
    curpos++;
  }
  return curpos;
}

void task1()
{
  std::vector< double > array;
  char line[2048];
  while (!std::cin.eof())
  {
    double tmp;
    std::cin.getline(line, 2048);
    if (!checkLine(line)) throw std::invalid_argument("contain invalid symbol");
    char * curpos = line;
    while (*curpos != 0)
    {
      tmp = std::atof(curpos);
      if (*curpos == 0) break;
      array.push_back(tmp);
      nextWord(curpos);
    }
  }
  if (array.empty())
  {
    return;
  }
  std::for_each(array.begin(), array.end(), [](double & a)
  {
    a = a * M_PI;
  });
  std::for_each(array.cbegin(), array.cend(), [](double a)
  {
    std::cout << a << ' ';
  });
  std::cout << std::endl;
}
