#ifndef QUEUEWITHPRIORITY_HPP
#define QUEUEWITHPRIORITY_HPP
#include <mutex>
#include <memory>
#include <list>
#include <iterator>

namespace kotov
{

  typedef enum
  {
    LOW,
    NORMAL,
    HIGH
  } ElementPriority;

  template< typename T >
  class QueueWithPriority
  {
  public:
    QueueWithPriority()
    {
    }

    ~QueueWithPriority() = default;

    void putElementToQueue(const T & element, ElementPriority priority)
    {
      node_t node;
      node.element = element;
      node.priority = priority;
      std::lock_guard<std::mutex> lock(m_mutex);
      if (priority == ElementPriority::LOW)
      {
        m_list.push_back(node);
      }
      else
      {
        decltype (m_list.begin()) iterator = m_list.begin();
        while (iterator != m_list.cend())
        {
          if ((*iterator).priority < priority) break;
          iterator++;
        }
        m_list.insert(iterator, node);
      }
    }

    T getElementFromQueue()
    {
      std::lock_guard<std::mutex> lock(m_mutex);
      if (m_list.empty())
      {
        throw std::invalid_argument("list is empty");
      }
      T tmp = m_list.front().element;
      m_list.pop_front();
      return tmp;
    }

    void accelerate()
    {
      std::lock_guard<std::mutex> lock(m_mutex);
      decltype (m_list.begin()) endHigh = m_list.begin(), startLow = m_list.end(), iterator;
      startLow--;
      while (startLow != m_list.cbegin() && (*startLow).priority == ElementPriority::LOW)
      {
        startLow--;
      }
      startLow++;
      while (endHigh != m_list.cend() && (*endHigh).priority == ElementPriority::HIGH)
      {
        endHigh++;
      }
      iterator = startLow;
      while (iterator != m_list.cend())
      {
        (*iterator).priority = ElementPriority::HIGH;
        iterator++;
      }
      m_list.splice(endHigh, m_list, startLow, m_list.end());
    }

  private:
    struct node_t
    {
      ElementPriority priority;
      T element;
    };
    std::mutex m_mutex;
    std::list< node_t > m_list;
  };
}

#endif // QUEUEWITHPRIORITY_HPP
