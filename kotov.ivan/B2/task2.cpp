#include <forward_list>
#include <iostream>
#include <list>
#include <math.h>

void task2()
{
  std::list< int > list;
  int count = 0;
  char str[255];
  char *subStr;
  int value = 0;
  int result = 0;
  while (count <= 20)
  {
    result = scanf("%s", str);
    if (result == EOF || count == 20) break;
    for (int i = 0; str[i] != 0 && i < 255; ++i)
    {
      if ((str[i] != ' ' && str[i] != ' ' && str[i] < '0') || str[i] > '9') throw std::invalid_argument("invalid number");
    }
    subStr = str;
    while (count < 20 && (*subStr != 0))
    {
      result = sscanf(subStr, "%d", &value);
      if (result == 0 || result == EOF) break;
      if (value < 1 || value > 20) throw std::invalid_argument("value out of range");
      list.push_back(value);
      count++;
      while(*subStr >= '0' && *subStr <= '9')
      {
        subStr++;
      }
      if(*subStr == 0) break;
      while((*subStr != 0) && (*subStr < '0' || *subStr > '9'))
      {
        subStr++;
      }
    }
  }
  if (count > 19 && result != EOF) throw std::invalid_argument("error: bigger than 20 numbers");
  if (!list.empty())
  {
    decltype (list.end()) begin = list.begin(), end = list.end();
    --end;
    while (begin != end)
    {
      std::cout << (*begin) << " ";
      ++begin;
      if (begin == end) break;
      std::cout << (*end) << " ";
      --end;
    }
    std::cout << (*begin) << "\n";
  }
}
