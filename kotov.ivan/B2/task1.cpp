#include <forward_list>
#include <iostream>
#include <string>
#include <memory>
#include "queuewithpriority.hpp"

void task1()
{
  kotov::QueueWithPriority< std::string > queue;
  std::string command;
  char line[255];
  while (std::cin >> command)
  {
    if (command.compare("add") == 0)
    {
      std::cin >> command;
      std::cin.getline(line, 255);
      char * skipWSLine = line;
      while (skipWSLine[0] == ' ' || skipWSLine[0] == '\t')
      {
        skipWSLine++;
      }
      if (line[0] == 0)
      {
        std::cout << "<INVALID COMMAND>\n";
      }
      else
      {
        if (command.compare("low") == 0)
        {
          command = std::string(skipWSLine);
          queue.putElementToQueue(command, kotov::ElementPriority::LOW);
        }
        else if (command.compare("normal") == 0)
        {
          command = std::string(skipWSLine);
          queue.putElementToQueue(command, kotov::ElementPriority::NORMAL);
        }
        else if (command.compare("high") == 0)
        {
          command = std::string(skipWSLine);
          queue.putElementToQueue(command, kotov::ElementPriority::HIGH);
        }
        else
        {
          std::cout << "<INVALID COMMAND>\n";
        }
      }
      std::cin.sync();
      continue;
    }
    else if (command.compare("get") == 0)
    {
      try
      {
        std::cout << queue.getElementFromQueue() << '\n';
      }
      catch (std::invalid_argument)
      {
        std::cout << "<EMPTY>\n";
      }
    }
    else if (command.compare("accelerate") == 0)
    {
      queue.accelerate();
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
    std::cin.sync();
    continue;
  }
  if (!std::cin.eof() && std::cin.failbit) throw std::invalid_argument("read error");
}
