#include <forward_list>
#include <iostream>
#include <cstring>

void task(unsigned long);

int main(int argc, char ** argv)
{
  try
  {
    unsigned long charsOnLine = 40;
    constexpr unsigned long minChars = 25;
    if ((argc == 3) && (strcmp(argv[1], "--line-width") == 0))
    {
      charsOnLine = std::stoi(argv[2]);
      if (charsOnLine < minChars) return 1;
    }
    else if (argc != 1)
    {
      std::cerr << "invalid arguments\n";
      return 1;
    }
    task(charsOnLine);
  }
  catch(std::invalid_argument ex)
  {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    return 2;
  }
  return 0;
}
