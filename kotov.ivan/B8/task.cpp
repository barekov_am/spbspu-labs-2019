#include <forward_list>
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <cstring>
#include <algorithm>

static constexpr int TYPE_NONE = 0;
static constexpr int TYPE_WORD = 1;
static constexpr int TYPE_NUMBER = 2;
static constexpr int TYPE_SYMBOL_INTERRUPTION = 4;
static constexpr int TYPE_DEFIS = 5;

struct word_t
{
  std::string word;
  int type;
};

word_t getNextWord(const char *& position)
{
  word_t result;
  constexpr int maxWordAndNumberLenght = 20;
  while ((*position != 0) && ((*position == ' ') || (*position == '\t') || (*position == '\v')
      || (*position == '\r') || (*position == '\n'))) position++;
  if (*position == 0)
  {
    result.type = TYPE_NONE;
    return result;
  }
  if (((*position >= '0') && (*position <= '9'))
      || (((*position == '-') || (*position == '+')) && (((*(position + 1) >= '0') && (*(position + 1) <= '9')))))
  {
    result.type = TYPE_NUMBER;
    result.word.push_back(*position++);
    int wereDelimiter = false;
    while (*position != 0)
    {
      if ((*position == ',') || (*position == '.'))
      {
        if (wereDelimiter) break;
        wereDelimiter = true;
      }
      else if ((*position < '0') || (*position > '9'))
      {
        break;
      }
      result.word.push_back(*position);
      position++;
    }
    if (result.word.size() > maxWordAndNumberLenght) throw std::invalid_argument("number is longer then that is applies");
  }
  else if (((*position >= 'a') && (*position <= 'z'))
      || ((*position >= 'A') && (*position <= 'Z')))
  {
    result.type = TYPE_WORD;
    result.word.push_back(*position++);
    while ((*position != 0) && (((*position >= 'a') && (*position <= 'z')) || ((*position >= 'A') && (*position <= 'Z'))
        || ((*position == '-') && (*(position - 1) != '-'))))
    {
      result.word.push_back(*position);
      position++;
    }
    if (result.word.length() > maxWordAndNumberLenght) throw std::invalid_argument("word is longer then that is applies");
  }
  else
  {
    result.word.push_back(*position++);
    if (*position == '-')
    {
      if (((*(position - 1) == '-') && (*(position + 1) == '-')))
      {
        result.type = TYPE_DEFIS;
        result.word += "--";
        position += 2;
      }
      else
      {
        throw std::invalid_argument("invalid symbol");
      }
    }
    else
    {
      if (result.word == "-") throw std::invalid_argument("bad symbol");
      result.type = TYPE_SYMBOL_INTERRUPTION;
    }
  }
  return result;
}

void task(unsigned long charsOnLine)
{
  std::list< word_t > listWord;
  std::string line;
  while (!std::cin.eof())
  {
    std::getline(std::cin, line);
    const char * position = line.c_str();
    while (*position != 0)
    {
      word_t tmp = getNextWord(position);
      if (listWord.empty() && ((tmp.type & TYPE_SYMBOL_INTERRUPTION) != 0))
          throw std::invalid_argument("begin with interruption symbol");
      if (((tmp.type & TYPE_SYMBOL_INTERRUPTION) != 0) && (((listWord.back().type & TYPE_SYMBOL_INTERRUPTION) != 0))
          && ((listWord.back().word.compare(",") != 0) || (tmp.word.compare("---") != 0)))
      {
        throw std::invalid_argument("INVALID SET OF SYMBOLS");
      }
      if (tmp.type != TYPE_NONE) listWord.push_back(tmp);
    }
  }
  std::vector< std::string > editedText;
  while (!listWord.empty())
  {
    std::string curLine = listWord.front().word;
    unsigned long long curLength = curLine.size();
    listWord.pop_front();
    while (!listWord.empty() && ((listWord.front().type == TYPE_SYMBOL_INTERRUPTION)
        || (listWord.front().type == TYPE_DEFIS)))
    {
      if (listWord.front().type == TYPE_DEFIS)
      {
        curLength += 3;
        curLine += ' ';
      }
      curLength += 1;
      curLine += listWord.front().word;
      listWord.pop_front();
    }
    if (curLength > charsOnLine) throw std::invalid_argument("invaild string length");
    while (!listWord.empty())
    {
      auto iter = listWord.cbegin();
      unsigned long long deltaSize = iter->word.size() + 1;
      int deltaElementInLine = 1;
      while (((++iter) != listWord.cend()) && ((iter->type == TYPE_SYMBOL_INTERRUPTION) || (iter->type == TYPE_DEFIS)))
      {
        if ((*iter).type == TYPE_DEFIS)
        {
          deltaSize += 3;
        }
        deltaSize += + 1;
        deltaElementInLine++;
      }
      if ((curLength + deltaSize) <= charsOnLine)
      {
        curLength += deltaSize;
        for (int i = 0; i < deltaElementInLine; i++)
        {
          if (listWord.front().type != TYPE_SYMBOL_INTERRUPTION) curLine += ' ';
          curLine += listWord.front().word;
          listWord.pop_front();
        }
      }
      else
      {
        break;
      }
    }
    editedText.push_back(curLine);
  }
  std::for_each(editedText.cbegin(), editedText.cend(), [](const std::string & str)
  {
    std::cout << str << std::endl;
  });
}
