#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

int main()
{
  kazantsev::Rectangle rectangle({ { 6, 7 }, 4, 5 });
  kazantsev::Circle circle({ 6, 7 }, 5);

  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(rectangle);
  kazantsev::shapePtr circlePtr = std::make_shared<kazantsev::Circle>(circle);

  kazantsev::CompositeShape compositeShape;
  compositeShape.add(rectanglePtr);
  compositeShape.writeParameters();

  compositeShape.add(circlePtr);
  std::cout << "After adding new figure: " << std::endl;
  compositeShape.writeParameters();

  compositeShape.move(3.7, 1.0);
  std::cout << "Let's move the shape 3.7 units right and 1 units up : " << std::endl;
  compositeShape.writeParameters();

  compositeShape.move({ 10.0, 2.3 });
  std::cout << "Let's move CompositeShape at the point (10, 2.3): " << std::endl;
  compositeShape.writeParameters();

  compositeShape.scale(2.0);
  std::cout << "Let's scaling the shape by factor = 2: " << std::endl;
  compositeShape.writeParameters();

  compositeShape.rotate(30.0);
  std::cout << "After 30 degree rotation: " << std::endl;
  compositeShape.writeParameters();

  std::cout << "After deleting the first figure: " << std::endl;
  compositeShape.remove(1);
  compositeShape.writeParameters();

  kazantsev::Rectangle rectangleNew({ { 8.0, 6.0 },6.0, 2.0 });
  kazantsev::shapePtr rectangleNewPtr = std::make_shared<kazantsev::Rectangle>(rectangleNew);
  kazantsev::Circle circleNew({ 11.0, 5.0 }, 1.0);
  kazantsev::shapePtr circleNewPtr = std::make_shared<kazantsev::Circle>(circleNew);

  compositeShape.add(rectangleNewPtr);
  compositeShape.add(circleNewPtr);

  kazantsev::Matrix matrix;
  matrix = kazantsev::part(compositeShape);
  matrix.writeParameters();

  return 0;
}
