#ifndef BOOK_HPP
#define BOOK_HPP

#include <list>
#include <map>

namespace kazantsev
{
  namespace details
  {
    struct PhoneBookEntry_t
    {
      std::string number, name;
    };

    namespace constants
    {
      const std::string CURRENT_BOOKMARK = "current";
    }

    class PhoneBook
    {
    public:
      PhoneBook();
      PhoneBook(const std::string& number, const std::string& name);
      PhoneBook(const PhoneBookEntry_t& number_name);
      ~PhoneBook() = default;

      void move(const std::string& steps, const std::string& bookmark = kazantsev::details::constants::CURRENT_BOOKMARK);
      void insertBefore(const std::string& number, const std::string& name,
        const std::string& bookMark = kazantsev::details::constants::CURRENT_BOOKMARK);
      void insertAfter(const std::string& number, const std::string& name,
        const std::string& bookMark = kazantsev::details::constants::CURRENT_BOOKMARK);
      void insertEnd(const std::string& number, const std::string& name);
      void deleteNote(const std::string& bookMark = kazantsev::details::constants::CURRENT_BOOKMARK);
      void addNote(const std::string& bookMark_old, const std::string& bookMark_new);

      PhoneBookEntry_t getRecord(const std::string& bookMark = kazantsev::details::constants::CURRENT_BOOKMARK) const;

      typedef std::list<PhoneBookEntry_t> RegisterContainer;
      typedef RegisterContainer::iterator RegisterIterator;
      typedef std::map<std::string, RegisterIterator> Bookmarks;

    private:
      RegisterContainer register_;
      Bookmarks bookmarks_;

      RegisterIterator getBookmarkIter(const std::string& bookMark);
    };
  }
}
#endif
