#include <iostream>
#include <cstring>

#include "tasks.hpp"

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      throw std::invalid_argument("Number of arguments must be 2!");
    }
    if (strcmp(argv[1], "1") == 0)
    {
      kazantsev::tasks::task1();
    }
    else if (strcmp(argv[1], "2") == 0)
    {
      kazantsev::tasks::task2();
    }
    else
    {
      throw std::invalid_argument("Invalid input!");
    }
  }
  catch (std::exception & exc)
  {
    std::cerr << exc.what() << std::endl;
    return 2;
  }

  return 0;
}
