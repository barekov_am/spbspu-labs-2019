#ifndef PHONE_BOOK_EXCEPTIONS_HPP
#define PHONE_BOOK_EXCEPTIONS_HPP

#include <exception>

namespace kazantsev
{
  namespace details
  {
    class EmptyBookException : public std::exception
    {
      const char* what() const noexcept
      {
        return "<EMPTY>";
      }
    };

    class IncorrectStepException : public std::exception
    {
      const char* what() const noexcept
      {
        return "<INVALID STEP>";
      }
    };

    class IncorrectBookmarkException : public std::exception
    {
      const char* what() const noexcept
      {
        return "<INVALID BOOKMARK>";
      }
    };

    class IncorrectCommandException : public std::exception
    {
      const char* what() const noexcept
      {
        return "<INVALID COMMAND>";
      }
    };
  }
}
#endif
