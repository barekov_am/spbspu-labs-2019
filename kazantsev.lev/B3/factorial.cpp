#include "factorial.hpp"

kazantsev::details::Factorial::FactIterator::FactIterator() : FactIterator(1)
{
}

kazantsev::details::Factorial::FactIterator::FactIterator(size_t pos) :
  position_(pos),
  value_(factor(pos))
{
}

const size_t* kazantsev::details::Factorial::FactIterator::operator->() const
{
  return &value_;
}

const size_t& kazantsev::details::Factorial::FactIterator::operator*() const
{
  return value_;
}

kazantsev::details::Factorial::FactIterator& kazantsev::details::Factorial::FactIterator::operator++()
{
  if (position_ < MAX_POSITION)
  {
    ++position_;
    value_ *= position_;
  }

  return *this;
}

kazantsev::details::Factorial::FactIterator kazantsev::details::Factorial::FactIterator::operator++(int)
{
  Factorial::FactIterator temp = *this;
  ++(*this);

  return temp;
}

kazantsev::details::Factorial::FactIterator& kazantsev::details::Factorial::FactIterator::operator--()
{
  if (position_ > MIN_POSITION)
  {
    value_ /= position_;
    --position_;
  }

  return *this;
}

kazantsev::details::Factorial::FactIterator kazantsev::details::Factorial::FactIterator::operator--(int)
{
  Factorial::FactIterator temp = *this;
  --(*this);

  return temp;
}

bool kazantsev::details::Factorial::FactIterator::operator==(const FactIterator& rhs) const
{
  return (position_ == rhs.position_);
}

bool kazantsev::details::Factorial::FactIterator::operator!=(const FactIterator & rhs) const
{
  return !(*this == rhs);
}

inline size_t kazantsev::details::Factorial::FactIterator::factor(size_t number)
{
  size_t res = 1;
  for (size_t i = 1; i <= number; i++)
  {
    res *= i;
  }

  return res;
}

kazantsev::details::Factorial::FactIterator kazantsev::details::Factorial::begin()
{
  return FactIterator(FactIterator::MIN_POSITION);
}

kazantsev::details::Factorial::FactIterator kazantsev::details::Factorial::end()
{
  return FactIterator(FactIterator::MAX_POSITION);
}
