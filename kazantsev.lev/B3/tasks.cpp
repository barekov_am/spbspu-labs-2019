#include <iostream>
#include <algorithm>

#include "tasks.hpp"

void kazantsev::tasks::task1()
{
  kazantsev::details::PhoneBook book;
  std::string input_line;

  while (std::getline(std::cin, input_line))
  {
    try
    {
      kazantsev::details::command_function cmd = kazantsev::details::handleCommand(input_line);
      cmd(book);
    }
    catch (std::invalid_argument & exc)
    {
      std::cerr << exc.what() << std::endl;
    }
    catch (std::exception & exc)
    {
      std::cout << exc.what() << std::endl;
    }
  }
}

void kazantsev::tasks::task2()
{
  kazantsev::details::Factorial fact;

  std::copy(fact.begin(), fact.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;

  std::reverse_copy(fact.begin(), fact.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;

}
