#define _USE_MATH_DEFINES
#include "triangle.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

const double INNACURACY = 0.00001;

kazantsev::Triangle::Triangle(const point_t& pointA, const point_t& pointB, const point_t& pointC) :
  pos_({ (pointA.x + pointB.x + pointC.x) / 3, (pointA.y + pointB.y + pointC.y) / 3 }),
  point1_(pointA),
  point2_(pointB),
  point3_(pointC),
  angle_(0)
{
  if (((std::abs(point1_.x - point2_.x) < INNACURACY) && (std::abs(point1_.y - point2_.y) < INNACURACY)) 
    || ((std::abs(point2_.x - point3_.x) < INNACURACY) && (std::abs(point2_.y - point3_.y) < INNACURACY)) 
    || ((std::abs(point1_.x - point3_.x) < INNACURACY) && (std::abs(point1_.y - point3_.y) < INNACURACY))
    )
  {
    throw std::invalid_argument("two matching points detected");
  }
}

double kazantsev::Triangle::getArea() const
{
  return (std::fabs((point1_.x - point3_.x) * (point2_.y - point3_.y)
    - (point2_.x - point3_.x) * (point1_.y - point3_.y))) / 2;
}

kazantsev::point_t kazantsev::Triangle::getCenter() const
{
  return { (point1_.x + point2_.x + point3_.x) / 3, (point1_.y + point2_.y + point3_.y) / 3 };
}

kazantsev::rectangle_t kazantsev::Triangle::getFrameRect() const
{
  const double maxX = std::fmax(std::fmax(point1_.x, point2_.x), point3_.x);
  const double minX = std::fmin(std::fmin(point1_.x, point2_.x), point3_.x);

  const double maxY = std::fmax(std::fmax(point1_.y, point2_.y), point3_.y);
  const double minY = std::fmin(std::fmin(point1_.y, point2_.y), point3_.y);
  const double width = maxX - minX;
  const double height = maxY - minY;
  const point_t position = { minX + width / 2, minY + height / 2 };

  return { position, width, height };
}

void kazantsev::Triangle::move(const point_t & newPos)
{
  const point_t difference = { newPos.x - pos_.x, newPos.y - pos_.y };

  move(difference.x, difference.y);
}

void kazantsev::Triangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;

  point1_.x += dx;
  point1_.y += dy;

  point2_.x += dx;
  point2_.y += dy;

  point3_.x += dx;
  point3_.y += dy;
}

double scaleCoordinates(double point, double position, const double factor)
{
  return (position + point * factor - factor * position);
}


void kazantsev::Triangle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Scaling factor must be greater than zero.");
  }
  point1_.x = scaleCoordinates(point1_.x, pos_.x, factor);
  point1_.y = scaleCoordinates(point1_.y, pos_.y, factor);
  point2_.x = scaleCoordinates(point2_.x, pos_.x, factor);
  point2_.y = scaleCoordinates(point2_.y, pos_.y, factor);
  point3_.x = scaleCoordinates(point3_.x, pos_.x, factor);
  point3_.y = scaleCoordinates(point3_.y, pos_.y, factor);
}

void kazantsev::Triangle::rotate(double angle)
{
  point_t center = getFrameRect().pos;
  const double radAngle = angle * M_PI / 180;
  const double cosAngle = cos(radAngle);
  const double sinAngle = sin(radAngle);
  point1_ = { center.x + (point1_.x - center.x) * cosAngle - (point1_.y - center.y) * sinAngle,
    center.y + (point1_.x - center.x) * sinAngle + (point1_.y - center.y) * cosAngle };
  point2_ = { center.x + (point2_.x - center.x) * cosAngle - (point2_.y - center.y) * sinAngle,
    center.y + (point2_.x - center.x) * sinAngle + (point2_.y - center.y) * cosAngle };
  point3_ = { center.x + (point3_.x - center.x) * cosAngle - (point3_.y - center.y) * sinAngle,
    center.y + (point3_.x - center.x) * sinAngle + (point3_.y - center.y) * cosAngle };
}


void kazantsev::Triangle::writeParameters() const
{
  kazantsev::rectangle_t rectangle = getFrameRect();
  std::cout << "Triangle point A is (" << point1_.x << ';' << point1_.y << ")";
  std::cout << "\n point B is " << point2_.x << ';' << point2_.y << ")";
  std::cout << "\n point C is " << point3_.x << ';' << point3_.y << ")";
  std::cout << "\n Triangle centre is (" << pos_.x << "," << pos_.y << ")";
  std::cout << "\n Frame rectangle width = " << rectangle.width;
  std::cout << ", height = " << rectangle.height;
  std::cout << "\n Area = " << getArea() << "\n\n";
}
