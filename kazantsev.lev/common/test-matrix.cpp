#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithCrossing)
{
  const size_t layersValue = 2;
  const size_t columnsValue = 1;

  kazantsev::Rectangle testRectangle({ { 5.0, 5.0 },4.0, 2.0 });
  kazantsev::Circle testCircle({ 5.0, 5.0 },2.0);
  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::shapePtr circlePtr = std::make_shared<kazantsev::Circle>(testCircle);

  kazantsev::CompositeShape compositeTest;
  compositeTest.add(rectanglePtr);
  compositeTest.add(circlePtr);

  kazantsev::Matrix testMatrix = kazantsev::part(compositeTest);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithoutCrossing)
{
  const size_t layersValue = 1;
  const size_t columnsValue = 2;

  kazantsev::Rectangle testRectangle({{ 5.0, 5.0 },4.0, 2.0});
  kazantsev::Circle testCircle({ 10.0, 5.0 }, 2.0);
  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::shapePtr circlePtr = std::make_shared<kazantsev::Circle>(testCircle);

  kazantsev::CompositeShape compositeTest(rectanglePtr);
  compositeTest.add(circlePtr);

  kazantsev::Matrix testMatrix = kazantsev::part(compositeTest);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  kazantsev::Rectangle testRectangle({{5.0, 5.0 },4.0, 2.0});
  kazantsev::Circle testCircle({ 5.0, 5.0 },2.0);
  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::shapePtr circlePtr = std::make_shared<kazantsev::Circle>(testCircle);

  kazantsev::CompositeShape compositeTest(rectanglePtr);

  compositeTest.add(circlePtr);

  kazantsev::Matrix testMatrix = kazantsev::part(compositeTest);

  BOOST_CHECK_THROW(testMatrix[5][0], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-5][0], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
