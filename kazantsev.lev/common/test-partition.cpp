#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(separationTest)

BOOST_AUTO_TEST_CASE(checkCorrectCrossing)
{
  kazantsev::Rectangle testRectangle({{ 5.0, 5.0 },4.0, 2.0});
  kazantsev::Rectangle crossingTestRectangle({ { 8.0, 5.0 },6.0, 2.0 });
  kazantsev::Circle testCircle({ 11.0, 5.0 },1.0);

  const bool falseResult = kazantsev::cross(testRectangle.getFrameRect(), testCircle.getFrameRect());
  const bool trueResult1 = kazantsev::cross(testRectangle.getFrameRect(), crossingTestRectangle.getFrameRect());
  const bool trueResult2 = kazantsev::cross(testCircle.getFrameRect(), crossingTestRectangle.getFrameRect());

  BOOST_CHECK_EQUAL(falseResult, false);
  BOOST_CHECK_EQUAL(trueResult1, true);
  BOOST_CHECK_EQUAL(trueResult2, true);
}

BOOST_AUTO_TEST_CASE(checkCorrectSeparation)
{
  kazantsev::Rectangle testRectangle({{ 5.0, 5.0 },4.0, 2.0});
  kazantsev::Rectangle crossingTestRectangle({ { 8.0, 5.0 },6.0, 2.0 });
  kazantsev::Circle testCircle({ 11.0, 5.0 } , 1.0);
  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::shapePtr rectangleCrossingPtr = std::make_shared<kazantsev::Rectangle>(crossingTestRectangle);
  kazantsev::shapePtr circlePtr = std::make_shared<kazantsev::Circle>(testCircle);

  kazantsev::CompositeShape compositeTest;
  compositeTest.add(rectanglePtr);
  compositeTest.add(rectangleCrossingPtr);
  compositeTest.add(circlePtr);

  kazantsev::Matrix testMatrix = kazantsev::part(compositeTest);

  BOOST_CHECK(testMatrix[0][0] == rectanglePtr);
  BOOST_CHECK(testMatrix[0][1] == circlePtr);
  BOOST_CHECK(testMatrix[1][0] == rectangleCrossingPtr);
}

BOOST_AUTO_TEST_SUITE_END()
