#define _USE_MATH_DEFINES
#include <iostream>
#include <algorithm>
#include <stdexcept>
#include <cmath>
#include "polygon.hpp"
#include "triangle.hpp"

kazantsev::Polygon::Polygon(const size_t size, const point_t* points) :
  size_(size),
  points_(std::unique_ptr<point_t[]>(new point_t[size]))
{
  for (size_t index = 0; index < size_; index++)
  {
    points_[index] = points[index];
  }

  if (correctionTest() == false)
  {
    throw std::invalid_argument("Ammout of point must be bigger than 3!");
  }
}

kazantsev::Polygon::Polygon(const Polygon& other) :
  size_(other.size_),
  points_(new point_t[size_])
{
  for (size_t index = 0; index < size_; index++)
  {
    points_[index] = other.points_[index];
  }
}

kazantsev::Polygon::Polygon(Polygon&& other) :
  size_(other.size_),
  points_(std::move(other.points_))
{
  other.size_ = 0;
  other.points_ = nullptr;
}

kazantsev::Polygon& kazantsev::Polygon::operator =(const Polygon& other)
{
  if (this == &other)
  {
    return *this;
  }
  size_ = other.size_;
  points_ = std::unique_ptr<point_t[]>(new point_t[size_]);

  for (size_t index = 0; index < size_; index++)
  {
    points_[index] = other.points_[index];
  }
  return *this;
}

kazantsev::Polygon& kazantsev::Polygon::operator =(Polygon&& other) noexcept
{
  if (this != &other)
  {
    size_ = other.size_;
    points_ = std::move(other.points_);
    other.size_ = 0;
  }
  return *this;
}

double kazantsev::Polygon::getArea() const
{
  double area = 0;
  for (size_t index = 1; index < size_ - 1; index++)
  {
    kazantsev::Triangle triangle(points_[0], points_[index], points_[index + 1]);
    area += triangle.getArea();
  }
  return area;
}

kazantsev::rectangle_t kazantsev::Polygon::getFrameRect() const
{
  double maxX = points_[0].x;
  double maxY = points_[0].y;
  double minX = points_[0].x;
  double minY = points_[0].y;
  for (size_t index = 1; index < size_; index++)
  {
    maxX = std::fmax(maxX, points_[index].x);
    maxY = std::fmax(maxY, points_[index].y);
    minX = std::fmin(minX, points_[index].x);
    minY = std::fmin(minY, points_[index].y);
  }
  return { { (minX + maxX) / 2, (minY + maxY) / 2 }, maxX - minX , maxY - minY};
}

void kazantsev::Polygon::move(const point_t& NewPos)
{
  point_t center = getCenter();
  move(NewPos.x - center.x, NewPos.y - center.y);
}

void kazantsev::Polygon::move(const double dx, const double dy)
{
  for (size_t index = 0; index < size_; index++)
  {
    //point_t point_before = points_[index];
    points_[index].x += dx;
    points_[index].y += dy;
    //point_t point_after = points_[index];
  }
}

void kazantsev::Polygon::writeParameters() const
{
  kazantsev::rectangle_t rectangle = getFrameRect();
  point_t center = getCenter();
  std::cout << "\n Polygon" << std::endl;
  for (size_t index = 0; index < size_; index++)
  {
    std::cout << "Point[" << index + 1 << "] is: x = " << points_[index].x << " y = " << points_[index].y << std::endl;
  }
  std::cout << "Centre is ( " << center.x << ";" << center.y << ")" << std::endl;
  std::cout << "Frame rectangle width = " << rectangle.width << std::endl;
  std::cout <<"Frame rectangle height = " << rectangle.height << std::endl;
  std::cout << "Area = " << getArea() << std::endl;
}

void kazantsev::Polygon::scale(const double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("factorficient must be > 0");
  }
  point_t center = getCenter();
  for (size_t index = 0; index < size_; index++)
  {
    points_[index].x = points_[index].x * factor - center.x * (factor - 1);
    points_[index].y = points_[index].y * factor - center.y * (factor - 1);
  }
}

kazantsev::point_t kazantsev::Polygon::getCenter() const
{
  point_t center = { 0, 0 };
  for (size_t index = 0; index < size_; index++)
  {
    center.x += points_[index].x;
    center.y += points_[index].y;
  }
  center.x /= size_;
  center.y /= size_;
  return center;
}

void kazantsev::Polygon::rotate(double angle)
{
  point_t center = getFrameRect().pos;
  const double radAngle = angle * M_PI / 180;
  const double cosAngle = cos(radAngle);
  const double sinAngle = sin(radAngle);
  for (size_t i = 0; i < size_; i++)
  {
    points_[i] = { center.x + (points_[i].x - center.x) * cosAngle - (points_[i].y - center.y) * sinAngle,
      center.y + (points_[i].x - center.x) * sinAngle + (points_[i].y - center.y) * cosAngle };
  }
}


bool kazantsev::Polygon::correctionTest() const
{
  if ((size_ < 3) || (points_ == nullptr) || (getArea() <= 0))
  {
    throw std::invalid_argument("Polygon is not correct");
  }
  bool result = true;
  double sign1 = (points_[0].x - points_[size_ - 1].x) * (points_[1].y - points_[0].y)
    - (points_[1].x - points_[0].x) * (points_[0].y - points_[size_ - 1].y);
  for (size_t index = 1; index < size_; index++)
  {
    double sign2 = (points_[index].x - points_[index - 1].x) * (points_[(index + 1) % size_].y - points_[index].y)
      - (points_[(index + 1) % size_].x - points_[index].x) * (points_[index].y - points_[index - 1].y);
    if ((sign1 * sign2) < 0)
    {
      result = false;
      break;
    }
  }
  return result;
}
