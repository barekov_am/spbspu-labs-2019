#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

kazantsev::CompositeShape::CompositeShape() :
  count_(0),
  shapesArray_(nullptr),
  angle_(0)
{}

kazantsev::CompositeShape::CompositeShape(const kazantsev::CompositeShape& shape) :
  count_(shape.count_),
  shapesArray_(std::make_unique<shapePtr[]>(shape.count_)),
  angle_(shape.angle_)
{
  for (size_t i = 0; i < count_; ++i)
  {
    shapesArray_[i] = shape.shapesArray_[i];
  }
}

kazantsev::CompositeShape::CompositeShape(kazantsev::CompositeShape&& shape) noexcept :
  count_(shape.count_),
  shapesArray_(std::move(shape.shapesArray_))
{}

kazantsev::CompositeShape::CompositeShape(const shapePtr &shape) :
  count_(1),
  shapesArray_(std::make_unique<shapePtr[]>(count_)),
  angle_(0)
{
  if (shape == nullptr)
  {
    count_ = 0;
    throw std::invalid_argument("There is no shape to add");
  }
  shapesArray_[0] = shape;
}

kazantsev::CompositeShape& kazantsev::CompositeShape::operator =(const CompositeShape& rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    angle_ = rhs.angle_;
    arrayPtr tempArray = std::make_unique<shapePtr[]>(count_);
    for (size_t i = 0; i < count_; i++)
    {
      tempArray[i] = rhs.shapesArray_[i];
    }
    shapesArray_.swap(tempArray);
  }

  return *this;
}

kazantsev::CompositeShape& kazantsev::CompositeShape::operator =(CompositeShape&& rhs) noexcept
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    angle_ = rhs.angle_;
    shapesArray_ = std::move(rhs.shapesArray_);
    rhs.count_ = 0;
  }

  return *this;
}

kazantsev::shapePtr kazantsev::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }
  return shapesArray_[index];
}

double kazantsev::CompositeShape::getArea() const
{
  double area = 0.0;
  for (size_t i = 0; i < count_; ++i)
  {
    area += shapesArray_[i]->getArea();
  }

  return area;
}

kazantsev::rectangle_t kazantsev::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    return { {0.0, 0.0}, 0.0, 0.0 };
  }
  rectangle_t frameRect = shapesArray_[0]->getFrameRect();

  point_t maxPoint{ frameRect.pos.x + frameRect.width / 2, frameRect.pos.y + frameRect.height / 2 };
  point_t minPoint{ frameRect.pos.x - frameRect.width / 2, frameRect.pos.y - frameRect.height / 2};

  for (size_t i = 1; i < count_; ++i)
  {
    rectangle_t frameRect = shapesArray_[i]->getFrameRect();

    double left = frameRect.pos.x - frameRect.width / 2;
    minPoint.x = std::fmin(minPoint.x, left);

    double right = frameRect.pos.x + frameRect.width / 2;
    maxPoint.x = std::fmax(maxPoint.x, right);

    double bottom = frameRect.pos.y - frameRect.height / 2;
    minPoint.y = std::fmin(minPoint.y, bottom);

    double top = frameRect.pos.y + frameRect.height / 2;
    maxPoint.y = std::fmax(maxPoint.y, top);
  }

  return rectangle_t{ {(maxPoint.x + minPoint.x) / 2, (maxPoint.y + minPoint.y) / 2},(maxPoint.x - minPoint.x),
    (maxPoint.y - minPoint.y)};                        
}

void kazantsev::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < count_; ++i)
  {
    shapesArray_[i]->move(dx, dy);
  }
}

void kazantsev::CompositeShape::move(const kazantsev::point_t & center)
{
  const double dx = center.x - getFrameRect().pos.x;
  const double dy = center.y - getFrameRect().pos.y;
  for (size_t i = 0; i < count_; ++i)
  {
    shapesArray_[i]->move(dx, dy);
  }
}

void kazantsev::CompositeShape::scale(double scale)
{
  if (scale <= 0)
  {
    throw std::invalid_argument("Scale factor must be greater than zero");
  }

  const point_t pos = getFrameRect().pos;
  for (size_t i = 0; i < count_; ++i)
  {
    double dx = shapesArray_[i]->getFrameRect().pos.x - pos.x;
    double dy = shapesArray_[i]->getFrameRect().pos.y - pos.y;
    shapesArray_[i]->move((scale - 1) * dx, (scale - 1) * dy);
    shapesArray_[i]->scale(scale);
  }
}

void kazantsev::CompositeShape::writeParameters() const
{
  kazantsev::rectangle_t temp_rect = getFrameRect();
  std::cout << "Area = " << getArea() << std::endl
    << "Frame rectangle width = " << temp_rect.width
    << ", height = " << temp_rect.height
    << ", and frameRectangle center (" << temp_rect.pos.x << ", "
    << temp_rect.pos.y << ") " << std::endl << std::endl;
}

void kazantsev::CompositeShape::add(const shapePtr &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Pointer on shape is null");
  }

  arrayPtr tempArray  = std::make_unique<shapePtr[]>(count_ + 1);
  for (size_t i = 0; i < count_; ++i)
  {
    tempArray[i] = shapesArray_[i];
  }
  tempArray[count_] = shape;
  count_++;
  shapesArray_.swap(tempArray);
}

void kazantsev::CompositeShape::remove(size_t index)
{
  arrayPtr tempArray = std::make_unique<shapePtr[]>(count_ - 1);

  for (size_t i = 0; i < index; ++i)
  {
    tempArray[i] = shapesArray_[i];
  }

  count_--;

  for (size_t i = index; i < count_; ++i)
  {
    tempArray[i] = shapesArray_[i + 1];
  }

  shapesArray_.swap(tempArray);
}

void kazantsev::CompositeShape::rotate(double angle)
{
  const point_t frameRectCentre = getFrameRect().pos;
  const double radAngle = angle * M_PI / 180;
  const double cosAngle = cos(radAngle);
  const double sinAngle = sin(radAngle);
  for (size_t i = 0; i < count_; i++)
  {
    const double dx = shapesArray_[i]->getFrameRect().pos.x - frameRectCentre.x;
    const double dy = shapesArray_[i]->getFrameRect().pos.y - frameRectCentre.y;
    const double moveDX = dx * cosAngle - dy * sinAngle;
    const double moveDY = dx * sinAngle + dy * cosAngle;
    shapesArray_[i]->move({ frameRectCentre.x + moveDX, frameRectCentre.y + moveDY });
    shapesArray_[i]->rotate(angle);
  }
}

kazantsev::arrayPtr kazantsev::CompositeShape::getList() const
{
  arrayPtr tmpArray(std::make_unique<shapePtr[]>(count_));

  for (size_t i = 0; i < count_; i++)
  {
    tmpArray[i] = shapesArray_[i];
  }

  return tmpArray;
}


size_t kazantsev::CompositeShape::getCount() const
{
  return count_;
}
