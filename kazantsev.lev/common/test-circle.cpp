#define _USE_MATH_DEFINES
#include <math.h>
#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double TOLERANCE = 0.0001;

BOOST_AUTO_TEST_SUITE(testCircle)

BOOST_AUTO_TEST_CASE(move)
{
  kazantsev::Circle circle({ 2.0, -1.0 }, 3.0);
  circle.move(20.0, 5.0);

  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.x, 22.0, TOLERANCE);
  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.y, 4.0, TOLERANCE);
  BOOST_CHECK_CLOSE(circle.getRadius(), 3, TOLERANCE);
  BOOST_CHECK_CLOSE(circle.getArea(), M_PI * 9, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(scale)
{
  kazantsev::Circle circle({ -10.0, 2.0 }, 5.0);

  double factor = 2;
  circle.scale(factor);

  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.x, -10.0, TOLERANCE);
  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.y, 2.0, TOLERANCE);
  BOOST_CHECK_CLOSE(circle.getRadius(), 10.0, TOLERANCE);
  BOOST_CHECK_CLOSE(circle.getArea(), M_PI * 25.0 * factor * factor, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(invalid_radius)
{
  BOOST_CHECK_THROW(kazantsev::Circle circle({ 0, 0 }, -1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalid_scale)
{
  kazantsev::Circle circle({ 0, 0 }, 1);
  BOOST_CHECK_THROW(circle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(CircleParametersAfterRotation)
{
  kazantsev::Circle circleTest({ 5.9, 3.0 }, 2.5);
  const double testArea = circleTest.getArea();
  const kazantsev::rectangle_t testFrameRect = circleTest.getFrameRect();
  const double angle = 90;

  circleTest.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, circleTest.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(testFrameRect.height, circleTest.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE(testArea, circleTest.getArea(), TOLERANCE);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, circleTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, circleTest.getFrameRect().pos.y);
}


BOOST_AUTO_TEST_SUITE_END()
