#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

namespace kazantsev
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t& pos, double radius);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    double getRadius() const;
    void move(const point_t& pos) override;
    void move(double dx, double dy) override;
    void writeParameters() const override;
    void scale(double factor) override;
    void rotate(double angle) override;

    private:
    point_t pos_;
    double radius_;
  };
}

#endif
