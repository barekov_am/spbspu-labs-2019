#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

#define EPSILON 1e-10

BOOST_AUTO_TEST_SUITE(testSuiteForCompositeShape)

BOOST_AUTO_TEST_CASE(testCopyConstructorForCompositeShape)
{
  kazantsev::Rectangle testRectangle({ { 5.0, 2.0 }, 6.9, 7.0 });
  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::CompositeShape testComposite(rectanglePtr);

  kazantsev::CompositeShape copiedComposite(testComposite);
  BOOST_CHECK_CLOSE(testComposite.getArea(), copiedComposite.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(testMoveConstructorForCompositeShape)
{
  kazantsev::Rectangle testRectangle({ { 5.0, 2.0 }, 6.9, 7.0 });
  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::CompositeShape testComposite(rectanglePtr);

  const double initialArea = testComposite.getArea();

  kazantsev::CompositeShape movedComposite(std::move(testComposite));
  BOOST_CHECK_CLOSE(initialArea, movedComposite.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(copyAssigmentOperatorForCompositeShape)
{
  kazantsev::Rectangle testRectangle({ { 5.0, 2.0 }, 6.9, 7.0 });
  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::CompositeShape testComposite(rectanglePtr);


  kazantsev::CompositeShape copiedEmptyComposite;
  copiedEmptyComposite = testComposite;
  BOOST_CHECK_CLOSE(testComposite.getArea(), copiedEmptyComposite.getArea(), EPSILON);

  kazantsev::Circle testCircle({ 5, 5 }, 5);
  kazantsev::shapePtr circlePtr = std::make_shared<kazantsev::Circle>(testCircle);

  kazantsev::CompositeShape copiedComposite(circlePtr);
  copiedComposite = copiedEmptyComposite;
  BOOST_CHECK_CLOSE(copiedComposite.getArea(), copiedEmptyComposite.getArea(), EPSILON);

}

BOOST_AUTO_TEST_CASE(moveAssigmentOperatorForCompositeShape)
{
  kazantsev::Rectangle testRectangle({ { 5.0, 2.0 }, 6.9, 7.0 });
  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::CompositeShape testComposite(rectanglePtr);
  const double initialArea = testComposite.getArea();

  kazantsev::CompositeShape movedEmptyComposite;
  movedEmptyComposite = std::move(testComposite);

  BOOST_CHECK_CLOSE(initialArea, movedEmptyComposite.getArea(), EPSILON);

  kazantsev::Circle testCircle({ 5, 5 }, 5);
  kazantsev::shapePtr circlePtr = std::make_shared<kazantsev::Circle>(testCircle);
  kazantsev::CompositeShape movedComposite(circlePtr);

  movedComposite = std::move(movedEmptyComposite);
  BOOST_CHECK_CLOSE(initialArea, movedComposite.getArea(), EPSILON);

}
BOOST_AUTO_TEST_CASE(testConstructorForCompositeShape)
{
  kazantsev::Rectangle testRectangle({{ 5.0, 2.0 }, 6.9, 7.0});
  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::CompositeShape testComposite(rectanglePtr);
  BOOST_CHECK_CLOSE(testComposite.getFrameRect().pos.x, testRectangle.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(testComposite.getFrameRect().pos.y, testRectangle.getFrameRect().pos.y, EPSILON);

  kazantsev::shapePtr shape = nullptr;
  BOOST_CHECK_THROW(kazantsev::CompositeShape wrongComposite(shape), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(checkRangeOperatorforCompositeShape)
{
  kazantsev::Rectangle testRectangle({ { 5.0, 2.0 }, 6.9, 7.0 });
  kazantsev::Circle testCircle({ 2.0, -6.0 }, 4.3);
  kazantsev::Triangle testTriangle({ 6.9, 7.0 }, { 1.2, 0.3 }, { 3.1, 1.6 });
  const int size = 4;
  kazantsev::point_t points[size] = { {4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0} };
  kazantsev::Polygon testPolygon(size ,points);
 
  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::shapePtr circlePtr = std::make_shared<kazantsev::Circle>(testCircle);
  kazantsev::shapePtr trianglePtr = std::make_shared<kazantsev::Triangle>(testTriangle);
  kazantsev::shapePtr polygonPtr = std::make_shared<kazantsev::Polygon>(testPolygon);

  kazantsev::CompositeShape testComposite(circlePtr);
  testComposite.add(rectanglePtr);
  testComposite.add(polygonPtr);
  testComposite.add(trianglePtr);

  BOOST_CHECK_CLOSE(testRectangle.getArea(), testComposite[1]->getArea(), EPSILON);
  BOOST_CHECK_CLOSE(testRectangle.getFrameRect().pos.x, testComposite[1]->getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(testRectangle.getFrameRect().pos.y, testComposite[1]->getFrameRect().pos.y, EPSILON);

  BOOST_CHECK_THROW(testComposite[testComposite.getCount() + 1], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeShapeAfterShiftMove)
{
  kazantsev::Rectangle testRectangle({ { 4.0, 4.0 }, 4.0, 4.0 });
  kazantsev::Circle testCircle({ 4.0, -4.0 }, 2.0);
  kazantsev::Triangle testTriangle({ -6.0, 6.0 }, { -6.0, 2.0 }, { -3.0, 4.0 });
  kazantsev::point_t points[4] = { {-6.0, -6.0}, {-5.0, -3.0}, {-2.0, -2.0}, {-3.0, -5.0} };
  kazantsev::Polygon testPolygon(4, points);
  
  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::shapePtr circlePtr = std::make_shared<kazantsev::Circle>(testCircle);
  kazantsev::shapePtr trianglePtr = std::make_shared<kazantsev::Triangle>(testTriangle);
  kazantsev::shapePtr polygonPtr = std::make_shared<kazantsev::Polygon>(testPolygon);

  kazantsev::CompositeShape testComposite(circlePtr);
  testComposite.add(rectanglePtr);
  testComposite.add(polygonPtr);
  testComposite.add(trianglePtr);

  const double beforeArea = testComposite.getArea();
  const kazantsev::rectangle_t beforeFrameRect = testComposite.getFrameRect();
  testComposite.move(1.0, 1.0);
  BOOST_CHECK_CLOSE(beforeArea, testComposite.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(testComposite.getFrameRect().width, beforeFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(testComposite.getFrameRect().height, beforeFrameRect.height, EPSILON);

}

BOOST_AUTO_TEST_CASE(compositeShapeAfterPointMove)
{
  kazantsev::Rectangle testRectangle({{ 4.0, 4.0 }, 4.0, 4.0});
  kazantsev::Circle testCircle({ 4.0, -4.0 }, 2.0);
  kazantsev::Triangle testTriangle({ -6.0, 6.0 }, { -6.0, 2.0 }, { -3.0, 4.0 });
  kazantsev::point_t points[4] = { {-6.0, -6.0}, {-5.0, -3.0}, {-2.0, -2.0}, {-3.0, -5.0} };
  kazantsev::Polygon testPolygon(4, points);
  
  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::shapePtr circlePtr = std::make_shared<kazantsev::Circle>(testCircle);
  kazantsev::shapePtr trianglePtr = std::make_shared<kazantsev::Triangle>(testTriangle);
  kazantsev::shapePtr polygonPtr = std::make_shared<kazantsev::Polygon>(testPolygon);

  kazantsev::CompositeShape testComposite(circlePtr);
  testComposite.add(rectanglePtr);
  testComposite.add(polygonPtr);
  testComposite.add(trianglePtr);

  const double beforeArea = testComposite.getArea();
  const kazantsev::rectangle_t beforeFrameRect = testComposite.getFrameRect();
  testComposite.move({ 0.0, 0.0 });
  BOOST_CHECK_CLOSE(beforeArea, testComposite.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(testComposite.getFrameRect().width, beforeFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(testComposite.getFrameRect().height, beforeFrameRect.height, EPSILON);
}

BOOST_AUTO_TEST_CASE(CompositeShapeScale)
{
  kazantsev::Rectangle testRectangle({ { 5.0, 2.0 }, 6.9, 7.0 });
  kazantsev::Circle testCircle({ 2.0, -5.0 }, 3.0);
  kazantsev::Triangle testTriangle({ 6.9, 7.0 }, { 1.2, 0.3 }, { 3.1, 1.6 });
  kazantsev::point_t points[4] = { {4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0} };
  kazantsev::Polygon testPolygon(4, points);
  
  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::shapePtr circlePtr = std::make_shared<kazantsev::Circle>(testCircle);
  kazantsev::shapePtr trianglePtr = std::make_shared<kazantsev::Triangle>(testTriangle);
  kazantsev::shapePtr polygonPtr = std::make_shared<kazantsev::Polygon>(testPolygon);

  kazantsev::CompositeShape testComposite(circlePtr);
  testComposite.add(rectanglePtr);
  testComposite.add(polygonPtr);
  testComposite.add(trianglePtr);

  double beforeArea = testComposite.getArea();
  double scale = 3;
  testComposite.scale(scale);
  BOOST_CHECK_CLOSE(beforeArea * scale * scale, testComposite.getArea(), EPSILON);

  beforeArea = testComposite.getArea();
  scale = 0.5;
  testComposite.scale(scale);
  BOOST_CHECK_CLOSE(beforeArea * scale * scale, testComposite.getArea(), EPSILON);

  BOOST_CHECK_THROW(testComposite.scale(-10), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShapeAdd)
{
  kazantsev::Rectangle testRectangle({ { 5.0, 2.0 }, 6.9, 7.0 });
  kazantsev::Circle testCircle({ 2.0, -6.0 }, 4.3);
  kazantsev::Triangle testTriangle({ 6.9, 7.0 }, { 1.2, 0.3 }, { 3.1, 1.6 });
  kazantsev::point_t points[4] = { {4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0} };
  kazantsev::Polygon testPolygon(4, points);
  
  
  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::shapePtr circlePtr = std::make_shared<kazantsev::Circle>(testCircle);
  kazantsev::shapePtr trianglePtr = std::make_shared<kazantsev::Triangle>(testTriangle);
  kazantsev::shapePtr polygonPtr = std::make_shared<kazantsev::Polygon>(testPolygon);

  kazantsev::CompositeShape testComposite(circlePtr);

  BOOST_CHECK_CLOSE(testComposite.getArea(), testCircle.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(testComposite.getFrameRect().pos.x, testCircle.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(testComposite.getFrameRect().pos.y, testCircle.getFrameRect().pos.y, EPSILON);
  
  testComposite.add(rectanglePtr);
  testComposite.add(polygonPtr);
  testComposite.add(trianglePtr);

  BOOST_CHECK_EQUAL(testComposite.getCount(), 4);

  kazantsev::shapePtr shape = nullptr;
  BOOST_CHECK_THROW(testComposite.add(shape), std::invalid_argument);
}


BOOST_AUTO_TEST_CASE(compositeShapeRemove)
{
  kazantsev::Rectangle testRectangle({ { 5.0, 2.0 }, 6.9, 7.0 });
  kazantsev::Circle testCircle({ 2.0, -6.0 }, 4.3);
  kazantsev::Triangle testTriangle({ 6.9, 7.0 }, { 1.2, 0.3 }, { 3.1, 1.6 });
  kazantsev::point_t points[4] = { {4.0, 10.0}, {15.0, 6.0}, {1.0, 1.0}, {-2.0, 6.0} };
  kazantsev::Polygon testPolygon(4, points);
  

  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::shapePtr circlePtr = std::make_shared<kazantsev::Circle>(testCircle);
  kazantsev::shapePtr trianglePtr = std::make_shared<kazantsev::Triangle>(testTriangle);
  kazantsev::shapePtr polygonPtr = std::make_shared<kazantsev::Polygon>(testPolygon);

  kazantsev::CompositeShape testComposite(circlePtr);
  testComposite.add(rectanglePtr);
  testComposite.add(polygonPtr);
  testComposite.add(trianglePtr);
  const int count = testComposite.getCount();
  //by index
  testComposite.remove(3);//remove polygon
  const int countAfterRemove = testComposite.getCount();

  BOOST_CHECK_EQUAL(count - 1, countAfterRemove);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  kazantsev::Rectangle testRectangle({{ 5, 5 }, 4, 2});
  kazantsev::Circle testCircle({ 5, 5 }, 1);
  kazantsev::shapePtr rectanglePtr = std::make_shared<kazantsev::Rectangle>(testRectangle);
  kazantsev::shapePtr circlePtr = std::make_shared<kazantsev::Circle>(testCircle);

  kazantsev::CompositeShape compositeTest(rectanglePtr);

  compositeTest.add(circlePtr);

  const double testArea = compositeTest.getArea();
  const kazantsev::rectangle_t testFrameRect = compositeTest.getFrameRect();
  const double angle = 90;

  compositeTest.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, compositeTest.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(testFrameRect.height, compositeTest.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(testArea, compositeTest.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, compositeTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, compositeTest.getFrameRect().pos.y);
}


BOOST_AUTO_TEST_SUITE_END()
