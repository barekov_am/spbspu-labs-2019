#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include "rectangle.hpp"

const double TOLERANCE = 0.0001;

BOOST_AUTO_TEST_SUITE(testRectangle)

BOOST_AUTO_TEST_CASE(move)
{
  kazantsev::Rectangle rectangle({ {0.0, 0.0}, 3.0, 6.0 });
  rectangle.move({ 10.0, 20.0 });

  BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.x, 10, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.y, 20, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getWidth(), 3.0, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getHeight(), 6.0, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getArea(), 18.0, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(scale)
{
  kazantsev::Rectangle rectangle({ {0.0, 0.0}, 2.0, 3.0 });

  double factor = 2;
  rectangle.scale(factor);

  BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.x, 0, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.y, 0, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getWidth(), 4.0, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getHeight(), 6.0, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getArea(), 6.0 * factor * factor, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(invalid_width)
{
  BOOST_CHECK_THROW(kazantsev::Rectangle rectangle({ {0.0, 0.0}, -10.0, 3.0 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalid_height)
{
  BOOST_CHECK_THROW(kazantsev::Rectangle rectangle({ {0.0, 0.0}, 3.0, 0.0 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalid_scale)
{
  kazantsev::Rectangle rectangle({ {0.0, 0.0}, 5.0, 5.0 });
  BOOST_CHECK_THROW(rectangle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  kazantsev::Rectangle rectangleTest({ { 5.0, 5.0 },4.0, 2.0 });
  const double testArea = rectangleTest.getArea();
  const kazantsev::rectangle_t testFrameRect = rectangleTest.getFrameRect();
  const double angle = 90;

  rectangleTest.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, rectangleTest.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE(testFrameRect.height, rectangleTest.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(testArea, rectangleTest.getArea(), TOLERANCE);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, rectangleTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, rectangleTest.getFrameRect().pos.y);
}


BOOST_AUTO_TEST_SUITE_END()
