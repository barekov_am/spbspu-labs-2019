#include <boost/test/auto_unit_test.hpp>
#include "polygon.hpp"

const double TOLERANCE = 0.0001;

BOOST_AUTO_TEST_SUITE(correctionTest)

BOOST_AUTO_TEST_CASE(move)
{
  const int size = 5;
  const kazantsev::point_t points[size] = { { 2, 2 }, { 2, 6 }, { 7, 7 }, { 9, 4 }, { 6, 1 } };
  kazantsev::Polygon polygon(size, points);
  kazantsev::rectangle_t frameRect = polygon.getFrameRect();
  double area = polygon.getArea();
  polygon.move({ 3, 5 });
  BOOST_CHECK_CLOSE(frameRect.width, polygon.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(frameRect.height, polygon.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE(area, polygon.getArea(), TOLERANCE);
  polygon.move(1, 9);
  BOOST_CHECK_CLOSE(frameRect.width, polygon.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(frameRect.height, polygon.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE(area, polygon.getArea(), TOLERANCE);
}

BOOST_AUTO_TEST_CASE(scale)
{
  double factor = 2.0;
  const int size = 5;
  const kazantsev::point_t points[size] = { { 2, 2 }, { 2, 6 }, { 7, 7 }, { 9, 4 }, { 6, 1 } };
  kazantsev::Polygon polygon(size, points);
  double area = polygon.getArea();
  polygon.scale(factor);
  BOOST_CHECK_CLOSE(area * factor * factor, polygon.getArea(), TOLERANCE);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  const int sizeOne = 5;
  const kazantsev::point_t points[sizeOne] = { { 2, 2 }, { 5, 4 }, { 4, 7 }, { 9, 7 }, { 6, 3 } };
  BOOST_CHECK_THROW(kazantsev::Polygon polygon(sizeOne, points), std::invalid_argument);
  const int sizeTwo = 2; 
  const kazantsev::point_t testPoints[sizeTwo] = { { 2, 2 }, { 6, 8 } };
  BOOST_CHECK_THROW(kazantsev::Polygon polygon(sizeTwo, testPoints), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalid_scale)
{
  const int size = 5;
  const kazantsev::point_t points[size] = { { 2, 2 }, { 2, 6 }, { 7, 7 }, { 9, 4 }, { 5, 1 } };
  kazantsev::Polygon polygon(size, points);
  BOOST_CHECK_THROW(polygon.scale(-2.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestPolygonCopyConstructor)
{
  const int size = 5;
  const kazantsev::point_t points[]{ {-3, 0}, {-1, 3}, {1, 3}, {3, 0}, {0, -3} };
  kazantsev::Polygon polygon1(size, points);
  kazantsev::Polygon polygon2 = polygon1;

  const kazantsev::rectangle_t rectangle1 = polygon1.getFrameRect();
  const kazantsev::rectangle_t rectangle2 = polygon2.getFrameRect();

  BOOST_CHECK_CLOSE(rectangle1.width, rectangle2.width, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle1.height, rectangle2.height, TOLERANCE);
  BOOST_CHECK_CLOSE(polygon1.getArea(), polygon2.getArea(), TOLERANCE);

}

BOOST_AUTO_TEST_CASE(TestPolygonCopyAssignmentOperator)
{
  const int sizeOne = 5;
  const int sizeTwo = 3;
  const kazantsev::point_t points1[]{ {-3, 0}, {-1, 3}, {1, 3}, {3, 0}, {0, -3} };
  const kazantsev::point_t points2[]{ {-3, 0}, {-1, 3}, {1, 3} };
  kazantsev::Polygon polygon1(sizeOne, points1);
  kazantsev::Polygon polygon2(sizeTwo, points2);
  polygon2 = polygon1;

  const kazantsev::rectangle_t rectangle1 = polygon1.getFrameRect();
  const kazantsev::rectangle_t rectangle2 = polygon2.getFrameRect();

  BOOST_CHECK_CLOSE(rectangle1.width, rectangle2.width, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle1.height, rectangle2.height, TOLERANCE);
  BOOST_CHECK_CLOSE(polygon1.getArea(), polygon2.getArea(), TOLERANCE);
}

BOOST_AUTO_TEST_CASE(TestPolygonMoveConstructor)
{
  const int size = 5;
  const kazantsev::point_t points[]{ {-3, 0}, {-1, 3}, {1, 3}, {3, 0}, {0, -3} };
  kazantsev::Polygon polygon1(size, points);
  const double areaBeforeMove = polygon1.getArea();
  const kazantsev::rectangle_t rectangle1 = polygon1.getFrameRect();
  kazantsev::Polygon polygon2 = std::move(polygon1);

  const kazantsev::rectangle_t rectangle2 = polygon2.getFrameRect();
  const double areaAfterMove = polygon2.getArea();

  BOOST_CHECK_CLOSE(rectangle1.width, rectangle2.width, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle1.height, rectangle2.height, TOLERANCE);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(TestPolygonMoveAssignmentOperator)
{
  const int sizeOne = 5;
  const int sizeTwo = 3;
  const kazantsev::point_t points1[]{ {-3, 0}, {-1, 3}, {1, 3}, {3, 0}, {0, -3} };
  const kazantsev::point_t points2[]{ {-3, 0}, {-1, 3}, {1, 3} };
  kazantsev::Polygon polygon1(sizeOne, points1);
  kazantsev::Polygon polygon2(sizeTwo, points2);
  
  const double areaBeforeMove = polygon1.getArea();
  const kazantsev::rectangle_t rectangle1 = polygon1.getFrameRect();

  polygon2 = std::move(polygon1);
  const kazantsev::rectangle_t rectangle2 = polygon2.getFrameRect();
  const double areaAfterMove = polygon2.getArea();

  BOOST_CHECK_CLOSE(rectangle1.width, rectangle2.width, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle1.height, rectangle2.height, TOLERANCE);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  kazantsev::point_t vertices[] = { {20, 10}, {20, -10}, {-10, -10}, {-10, 10} };
  size_t size = sizeof(vertices) / sizeof(vertices[0]);
  kazantsev::Polygon polygonTest(size, vertices);
  const double testArea = polygonTest.getArea();
  const double widthBefore = polygonTest.getFrameRect().width;
  const double heightBefore = polygonTest.getFrameRect().height;
  const kazantsev::rectangle_t testFrameRect = polygonTest.getFrameRect();
  const double angle = 90;

  polygonTest.rotate(angle);

  BOOST_CHECK_CLOSE(widthBefore, polygonTest.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE(heightBefore, polygonTest.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(testArea, polygonTest.getArea(), TOLERANCE);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, polygonTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, polygonTest.getFrameRect().pos.y);
}


BOOST_AUTO_TEST_SUITE_END()
