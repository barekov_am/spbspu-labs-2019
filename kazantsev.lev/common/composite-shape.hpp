#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include <cstdlib>
#include "shape.hpp"

namespace kazantsev
{
  class CompositeShape : public Shape
  {
  public:
    //using arrayPtr = std::unique_ptr<Shape* []>;

    CompositeShape();
    CompositeShape(const CompositeShape&);
    CompositeShape(CompositeShape&&) noexcept;
    CompositeShape(const shapePtr &shape);
    ~CompositeShape() override = default;

    CompositeShape& operator =(const CompositeShape&);
    CompositeShape& operator =(CompositeShape&&) noexcept;
    shapePtr operator [](size_t) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double, double) override;
    void move(const point_t&) override;
    void scale(double) override;
    void writeParameters() const override;
    void rotate(double angle) override;

    void add(const shapePtr&);
    void remove(size_t);
    arrayPtr getList() const;

    size_t getCount() const;

  private:
    size_t count_;
    arrayPtr shapesArray_;
    double angle_;
  };

}

#endif
