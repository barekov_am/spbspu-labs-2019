#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include "triangle.hpp"

const double TOLERANCE = 0.0001;

BOOST_AUTO_TEST_SUITE(testTriangle)

BOOST_AUTO_TEST_CASE(move)
{
  kazantsev::Triangle triangle({ -3.0, 0.0 }, { 0.0, 9.0 }, { 3.0, 0.0 });
  triangle.move(5.0, 6.0);

  BOOST_CHECK_CLOSE(triangle.getCenter().x, 5.0, TOLERANCE);
  BOOST_CHECK_CLOSE(triangle.getCenter().y, 9.0, TOLERANCE);
  BOOST_CHECK_CLOSE(triangle.getArea(), 27.0, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(scale)
{
  kazantsev::point_t pos_a = { 1.0, 0.0 };
  kazantsev::point_t pos_b = { 2.0, 3.0 };
  kazantsev::point_t pos_c = { 3.0, 0.0 };

  kazantsev::Triangle triangle(pos_a, pos_b, pos_c);

  double factor = 3;
  triangle.scale(factor);

  BOOST_CHECK_CLOSE(triangle.getCenter().x, 2.0, TOLERANCE);
  BOOST_CHECK_CLOSE(triangle.getCenter().y, 1.0, TOLERANCE);
  BOOST_CHECK_CLOSE(triangle.getArea(), (fabs(((pos_a.x - pos_c.x) * (pos_b.y - pos_c.y)
    - (pos_b.x - pos_c.x) * (pos_a.y - pos_c.y))) / 2) * factor * factor, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(invalid_constructor)
{
  BOOST_CHECK_THROW(kazantsev::Triangle triangle({ 1.0, 0.0 }, { 2.0, 1.0 }, { 1.0, 0.0 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalid_scale)
{
  kazantsev::Triangle triangle({ 6., 2.0 }, { 0.0, 3.0 }, { 5.0, 2.0 });
  BOOST_CHECK_THROW(triangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TriangleParametersAfterRotation)
{
  kazantsev::Triangle triangleTest({ 13,20 }, { 15, 22 }, { 12, 12 });
  const double areaBefore = triangleTest.getArea();
  const double widthBefore = triangleTest.getFrameRect().width;
  const double heightBefore = triangleTest.getFrameRect().height;
  const kazantsev::rectangle_t testFrameRect = triangleTest.getFrameRect();
  const double angle = 90;

  triangleTest.rotate(angle);

  BOOST_CHECK_CLOSE(widthBefore, triangleTest.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE(heightBefore, triangleTest.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(areaBefore, triangleTest.getArea(), TOLERANCE);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, triangleTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, triangleTest.getFrameRect().pos.y);
}


BOOST_AUTO_TEST_SUITE_END()
