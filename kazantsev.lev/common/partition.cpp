#include "partition.hpp"

#include <cmath>

bool kazantsev::cross(const rectangle_t& shape1, const rectangle_t& shape2)
{
  const bool firstCondition = (fabs(shape1.pos.x - shape2.pos.x) < (shape1.width + shape2.width) / 2);
  const bool secondCondition = (fabs(shape1.pos.y - shape2.pos.y) < (shape1.height + shape2.height) / 2);

  return firstCondition && secondCondition;
}

kazantsev::Matrix kazantsev::part(const arrayPtr & arr, size_t size)
{
  Matrix tmpMatrix;

  for (size_t i = 0; i < size; i++)
  {
    size_t rightLine = 0;
    size_t rightColumn = 0;

    for (size_t j = 0; j < tmpMatrix.getLines(); j++)
    {

      for (size_t k = 0; k < tmpMatrix.getColumns(); k++)
      {

        if (tmpMatrix[j][k] == nullptr)
        {
          rightLine = j;
          rightColumn = k;
          break;
        }

        if (cross(arr[i]->getFrameRect(), tmpMatrix[j][k]->getFrameRect()))
        {
          rightLine = j + 1;
          rightColumn = 0;
          break;
        }
        else
        {
          rightLine = j;
          rightColumn = k + 1;
        }
      }

      if (rightLine == j)
      {
        break;
      }
    }

    tmpMatrix.add(arr[i], rightLine, rightColumn);
  }

  return tmpMatrix;
}

kazantsev::Matrix kazantsev::part(const CompositeShape & composite)
{
  return part(composite.getList(), composite.getCount());
}
