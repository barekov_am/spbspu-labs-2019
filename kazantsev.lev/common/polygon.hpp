#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <memory>
#include "shape.hpp"

namespace kazantsev
{
  class Polygon : public Shape
  {
  public:
    Polygon(const size_t size, const point_t* points);
    Polygon(const Polygon& other);
    Polygon(Polygon&& other);
    ~Polygon() = default;
    Polygon& operator =(const Polygon& other);
    Polygon& operator =(Polygon&& other) noexcept;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& pos) override;
    void move(double dx, double dy) override;
    void writeParameters() const override;
    void scale(const double coef) override;
    void rotate(double angle) override;

  private:
    size_t size_;
    std::unique_ptr<point_t[]> points_;
    point_t getCenter() const;
    bool correctionTest() const;
  };
}

#endif // !POLYGON_HPP
