#define _USE_MATH_DEFINES
#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>


kazantsev::Rectangle::Rectangle(const rectangle_t& parameters) :
  rect_(parameters),
  angle_(0)
{
  if ((rect_.height <= 0) || (rect_.width <= 0))
  {
    throw std::invalid_argument("Height and width must be bigger than 0");
  }
}

double kazantsev::Rectangle::getWidth() const
{
  return rect_.width;
}

double kazantsev::Rectangle::getHeight() const
{
  return rect_.height;
}

double kazantsev::Rectangle::getArea() const
{
  return rect_.width * rect_.height;
}

kazantsev::rectangle_t kazantsev::Rectangle::getFrameRect() const
{
  const double radAngle = angle_ * M_PI / 180;
  const double width = rect_.width * fabs(cos(radAngle)) + rect_.height * fabs(sin(radAngle));
  const double height = rect_.width * fabs(sin(radAngle)) + rect_.height * fabs(cos(radAngle));

  return { rect_.pos ,width, height };

}

void kazantsev::Rectangle::move(const point_t& pos)
{
  rect_.pos = pos;
}

void kazantsev::Rectangle::move(double dx, double dy)
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

void kazantsev::Rectangle::rotate(double angle)
{
  angle_ += angle;
}


void kazantsev::Rectangle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Scaling factor must be greater than zero.");
  }
  rect_.width = rect_.width * factor;
  rect_.height = rect_.height * factor;
}


void kazantsev::Rectangle::writeParameters() const
{
  rectangle_t rectangle = getFrameRect();
  std::cout << "Rectangle centre is (" << rect_.pos.x << ","
    << rect_.pos.y << ")\n"
    << "Frame rectangle width = " << rectangle.width
    << ", height = " << rectangle.height << "\n"
    << "Area = " << getArea() << "\n\n";
}
