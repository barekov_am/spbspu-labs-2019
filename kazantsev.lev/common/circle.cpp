#define _USE_MATH_DEFINES
#include "circle.hpp"
#include <math.h>
#include <stdexcept>
#include <iostream> 

kazantsev::Circle::Circle(const point_t& pos, double radius) :
  pos_(pos),
  radius_(radius)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("radius must be greater than 0");
  }
}

double kazantsev::Circle::getArea() const
{
  return M_PI * radius_* radius_;
}

double kazantsev::Circle::getRadius() const
{
  return radius_;
}

kazantsev::rectangle_t kazantsev::Circle::getFrameRect() const
{
  return { pos_, radius_ * 2, radius_ * 2 };
}

void kazantsev::Circle::move(const point_t& pos)
{
  pos_ = pos;
}

void kazantsev::Circle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void kazantsev::Circle::rotate(double)
{}


void kazantsev::Circle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Scaling factor must be greater than zero.");
  }
  radius_ = radius_ * factor;
}

void kazantsev::Circle::writeParameters() const
{
  rectangle_t rectangle = getFrameRect();
  std::cout << "Circle radius is (" << radius_
    << ") Centre is (" << pos_.x << ","
    << pos_.y << ")\n"
    << "Frame rectangle width = " << rectangle.width
    << ", height = " << rectangle.height << "\n"
    << "Area = " << getArea() << "\n\n";
}
