#include <iostream>
#include <list>

const int MIN = 1;
const int MAX = 20;
const int MAX_SIZE = 20;
void task2()
{
  std::list<int> list;
  int element;
  while (std::cin >> element, !std::cin.eof())
  {
    if ((element < MIN) || (element > MAX))
    {
      throw std::invalid_argument("Number must be between 1 and 20");
    }

    if (list.size() + 1 > MAX_SIZE)
    {
      throw std::out_of_range("Max size of list is 20");
    }

    list.push_back(element);
  }

  if ((!std::cin.eof()) && (std::cin.fail()))
  {
    throw std::runtime_error("Can`t read file");
  }

  std::list<int>::iterator begin = list.begin();
  std::list<int>::iterator end = list.end();

  while (begin != end)
  {
    std::cout << *begin << " ";

    if (std::next(begin) == end)
    {
      break;
    }

    begin++;
    end--;
    std::cout << *end << " ";
  }
  std::cout << "\n";
}
