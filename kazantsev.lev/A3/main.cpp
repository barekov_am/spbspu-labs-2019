#include <cassert>
#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
int main()
{
  kazantsev::Circle circle({ 3.0, 2.0 }, 7.0);

  kazantsev::Rectangle rectangle({ {5.0, 11.0}, 3.0, 6.0 });

  kazantsev::Triangle triangle({ 2.0, 1.0 }, { 2.0, 5.0 }, { 5.0, 1.0 });

  const int size = 5;
  const kazantsev::point_t points[size] = { { 2.0, 2.0 }, { 2.0, 6.0 }, { 7.0, 7.0 }, { 9.0, 4.0 }, { 5.0, 1.0 } };
  kazantsev::Polygon polygon(size, points);

  kazantsev::CompositeShape compositeShape;

  compositeShape.add(std::make_shared<kazantsev::Circle>(circle));
  compositeShape.add(std::make_shared<kazantsev::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<kazantsev::Triangle>(triangle));
  compositeShape.add(std::make_shared<kazantsev::Polygon>(polygon));

  std::cout << "Composite shape:\n";
  compositeShape.writeParameters();

  compositeShape.move(5.0, 6.5);
  std::cout << "Shift move on (5, 6.5):\n";
  compositeShape.writeParameters();

  compositeShape.move({ 1.0, 2.5 });
  std::cout << "Move to point (1, 2.5):\n";
  compositeShape.writeParameters();

  compositeShape.scale(3.5);
  std::cout << "Scale by 3.5:\n";
  compositeShape.writeParameters();

  std::cout << "Delete circle and rectangle:\n";
  compositeShape.remove(0);
  compositeShape.remove(0);
  compositeShape.writeParameters();

  size_t count = compositeShape.getCount();
  std::cout << "Count of shapes:" << count << std::endl;
  std::cout << "Get any shape and print its info:\n";
  compositeShape[0]->writeParameters();

  return 0;

}
