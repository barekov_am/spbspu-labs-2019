#ifndef STATISTIC_HPP
#define STATISTIC_HPP

class Statistic
{
public:
  Statistic();

  void operator()(long long int nextNumber);
  void comparator(long long int number);

  long long int getMax() const;
  long long int getMin() const;
  long long int getPositiveDigits() const;
  long long int getNegativeDigits() const;
  long long int getOddSum() const;
  long long int getEvenSum() const;
  long long int getCounter() const;

  double getAverage() const;

  bool areSame() const;
  bool isEmpty() const;

private:
  long long int max_;
  long long int min_;
  long long int positiveDigits_;
  long long int negativeDigits_;
  long long int oddSum_;
  long long int evenSum_;
  long long int counter_;
  long long int firstElement_;
  long long int lastElement_;

  bool equal_;
};

#endif
