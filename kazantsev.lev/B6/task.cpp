#include "statistic.hpp"

#include <iostream>
#include <exception>
#include <algorithm>
#include <iterator>
#include <iomanip>

void printData(Statistic& stat)
{
  std::cout << "Max: " << stat.getMax() << "\n";
  std::cout << "Min: " << stat.getMin() << "\n";
  std::cout << "Mean: " << std::setprecision(std::numeric_limits<double>::digits10 + 2) << stat.getAverage() << "\n";
  std::cout << "Positive: " << stat.getPositiveDigits() << "\n";
  std::cout << "Negative: " << stat.getNegativeDigits() << "\n";
  std::cout << "Odd Sum: " << stat.getOddSum() << "\n";
  std::cout << "Even Sum: " << stat.getEvenSum() << "\n";

  if (stat.getCounter() == 1)
  {
    std::cout << "First/Last Equal: yes\n";
  }
  else
  {
    if (stat.areSame())
    {
      std::cout << "First/Last Equal: yes\n";
    }
    else
    {
      std::cout << "First/Last Equal: no\n";
    }
  }
}

void task()
{
  Statistic statistic;

  statistic = std::for_each(std::istream_iterator<long long int>(std::cin),
    std::istream_iterator<long long int>(), statistic);

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Incorrect reading data");
  }

  if (!statistic.isEmpty())
  {
    printData(statistic);
  }
  else
  {
    std::cout << "No Data\n";
    return;
  }
}
