#include "statistic.hpp"

#include <limits>

Statistic::Statistic() :
  max_(std::numeric_limits<long long int>::min()),
  min_(std::numeric_limits<long long int>::max()),
  positiveDigits_(0),
  negativeDigits_(0),
  oddSum_(0),
  evenSum_(0),
  counter_(0),
  firstElement_(0),
  lastElement_(0),
  equal_(false)
{
}

void Statistic::operator()(long long int number)
{
  comparator(number);
}

void Statistic::comparator(long long int num)
{
  counter_ == 0 ? firstElement_ = num : lastElement_ = num;

  if (num > max_)
  {
    max_ = num;
  }

  if (num < min_)
  {
    min_ = num;
  }

  if (num != 0)
  {
    num > 0 ? ++positiveDigits_ : ++negativeDigits_;
  }

  num % 2 == 0 ? evenSum_ += num : oddSum_ += num;
  firstElement_ == lastElement_ ? equal_ = true : equal_ = false;
  ++counter_;
}

long long int Statistic::getMax() const
{
  return max_;
}

long long int Statistic::getMin() const
{
  return min_;
}

long long int Statistic::getPositiveDigits() const
{
  return positiveDigits_;
}

long long int Statistic::getNegativeDigits() const
{
  return negativeDigits_;
}

long long int Statistic::getOddSum() const
{
  return oddSum_;
}

long long int Statistic::getEvenSum() const
{
  return evenSum_;
}

long long int Statistic::getCounter() const
{
  return counter_;
}

double Statistic::getAverage() const
{
  return (evenSum_ + oddSum_) / static_cast<double>(counter_);
}

bool Statistic::areSame() const
{
  return equal_;
}

bool Statistic::isEmpty() const
{
  return counter_ == 0;
}
