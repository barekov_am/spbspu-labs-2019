#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP

#include <algorithm>
#include <vector>
#include <string>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

DataStruct giveElement(const std::string& input);
void optionVector(std::vector<DataStruct>& vector);

#endif // DATASTRUCT_HPP
