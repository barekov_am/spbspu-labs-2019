#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include "shape.hpp"

const int triangleVertices = 3;
const int squareVertices = 4;
const int pentagonVertices = 5;

int getLength(const Point& a, const Point& b);
bool isTriangle(const Shape& shape);
bool isSquare(const Shape& shape);
bool isRectangle(const Shape& shape);
bool comparison(const Shape& a, const Shape& b);
void deletePentagons(std::vector<Shape>& shapes);


#endif //FUNCTIONS_HPP
