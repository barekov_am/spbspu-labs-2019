#include <cassert>
#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

void Demonstartion(kazantsev::Shape* figure)
{
  assert(figure != nullptr);
  figure->writeParameters();
  figure->move(2.0, 3.0);
  std::cout << "Move 2 by Ox, 3 by Oy :" << std::endl;
  figure->writeParameters();
  figure->move({ 0.0,0.0 });
  std::cout << "Move to (0,0) :" << std::endl;
  figure->writeParameters();
  figure->scale(3);
  std::cout << "Scale :" << std::endl;
  figure->writeParameters();
  std::cout << "---" << std::endl;
}

int main()
{
  kazantsev::Circle cir({ 3.0, 2.0 }, 7.0);
  Demonstartion(&cir);

  kazantsev::Rectangle rect({ {5.0, 11.0}, 3.0, 6.0 });
  Demonstartion(&rect);

  kazantsev::Triangle trg({ 2.0, 1.0 }, { 2.0, 5.0 }, { 5.0, 1.0 });
  Demonstartion(&trg);

  return 0;
}
