#ifndef QUEUE_B2
#define QUEUE_B2

#include <list>
#include <string>
#include <stdexcept>

template <typename ElementType>
class QueueWithPriority
{
public:
  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  void PutElementToQueue(const ElementType& element, ElementPriority priority);
  ElementType GetElementFromQueue();
  void Accelerate();
  bool empty() const;

private:
  std::list<ElementType> low_;
  std::list<ElementType> normal_;
  std::list<ElementType> high_;
};


#endif
