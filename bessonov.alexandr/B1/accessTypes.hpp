#ifndef TYPEACCESS_HPP
#define TYPEACCESS_HPP

#include <vector>
template <typename T>
struct BracketAccess
{
  typedef typename T::size_type iterator;
  typedef typename T::value_type value_type;
  static value_type& getElem(T &collection, int ind)
  {
    return collection[ind];
  }
  static iterator begin (T &)
  {
    return 0;
  }
  static iterator end (T &collection)
  {
    return collection.size();
  }
};

template <typename T>
struct AtAccess
{
  typedef typename T::size_type iterator;
  typedef typename T::value_type value_type;
  static value_type& getElem(T &collection, iterator ind)
  {
    return collection.at(ind);
  }
  static iterator begin(T &)
  {
    return 0;
  }
  static iterator end (T &collection)
  {
    return collection.size();
  }
};

template <typename T>
struct IteratorAccess
{
  typedef typename T::iterator iterator;
  typedef typename T::value_type value_type;
  static value_type& getElem(T &, iterator &iter)
  {
    return *iter;
  }
  static iterator begin(T & collection)
  {
    return collection.begin();
  }
  static iterator end(T & collection)
  {
    return collection.end();
  }
};

#endif
