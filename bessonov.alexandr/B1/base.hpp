#ifndef BASE_HPP
#define BASE_HPP

#include <forward_list>
#include <iostream>
#include <iterator>
#include <stdexcept>
#include <string>
#include <vector>
#include <functional>

#include "howToSort.hpp"
#include "accessTypes.hpp"

template <template <typename> class Access, typename T, typename Comparator>
void sort (T &collection, Comparator comp)
{
  typedef Access<T> AccessTypes;
  typedef typename AccessTypes::iterator counter;
  for (counter i = AccessTypes::begin(collection); i != AccessTypes::end(collection); i++)
  {
    for (counter j = AccessTypes::begin(collection); j != AccessTypes::end(collection); j++)
    {
      if (comp(AccessTypes::getElem(collection, i), AccessTypes::getElem(collection, j)))
      {
        std::swap(AccessTypes::getElem(collection, i), AccessTypes::getElem(collection,j));
      }
    }
  }
}

template <typename TIter>
void print(TIter beg, TIter end)
{
  for (auto i = beg; i != end; ++i)
  {
    std::cout << *i << " ";
  }
};

template<typename T>
std::function<bool(T,T)> compareElem(const howToSort way)
{
  if (way)
  {
    return std::greater<T>();
  }
  else
  {
    return std::less<T>();
  }
}

#endif
