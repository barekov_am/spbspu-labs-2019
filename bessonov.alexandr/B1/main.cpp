#include <iostream>
#include <stdexcept>
#include <memory>

#include "first.hpp"
#include "second.hpp"
#include "third.hpp"
#include "fourth.hpp"

int main(const int argc, const char *argv[])
{
  if (argc < 2 || argc > 4)
  {
    std::cerr << "сhoose task number (1/2/3/4)\n";
    return 1;
  }
  int task = std::stoi(argv[1]);
  try
  {
    switch (task)
    {
      case 1:
        if (argc != 3)
        {
          throw std::invalid_argument("incorrect parameters input");
        }
        first(argv);
        break;
      case 2:
        if (argc != 3)
        {
          throw std::invalid_argument("incorrect parameters input");
        }
        if (argv == nullptr)
        {
          throw std::invalid_argument("incorrect parametr");
        }
        second(argv);
        break;
      case 3:
        third();
        break;
      case 4:
        if (argc != 4)
        {
          throw std::invalid_argument("incorrect parameters input");
        }
        fourth(argv);
        break;
      default:
        throw std::invalid_argument("incorrect parameters input");
    }
  }
  catch (const std::exception & error)
  {
    std::cerr << error.what();
    return 1;
  }
  catch (...)
  {
    std::cerr << "error";
    return 1;
  }
  return 0;
};
