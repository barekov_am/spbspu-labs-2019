#ifndef THIRD_HPP
#define THIRD_HPP
#include <vector>
#include <iterator>

#include "base.hpp"
#include "howToSort.hpp"

void third()
{
  std::vector<int> vector {};
  std::vector<int>::iterator iter = vector.begin();
  int i = 1;
  size_t k = 0;
  while (std::cin && !(std::cin >> i).eof())
  {
    if ((std::cin.fail() || std::cin.bad()))
    {
      throw std::ios_base::failure("reading error");
    }
    if (i == 0)
    {
      break;
    }
    if (i % 3 == 0)
    {
      k++;
    }
    vector.push_back(i);
  }
  if (vector.empty())
  {
    return;
  }
  if (i != 0)
  {
    throw std::invalid_argument("last element must be 0");
  }
  switch (vector.back())
  {
    case 1:
    {
      for (iter = vector.begin(); iter != vector.end(); ++iter)
      {
        if (*iter % 2 == 0)
        {
          iter = vector.erase(iter);
          iter--;
        }
      }
    }
    break;
    case 2:
    {
      vector.reserve(vector.size() + k * 3);
      for (iter = vector.begin(); iter != vector.end(); ++iter)
      {
        if (*iter % 3 == 0)
        {
          vector.insert(iter + 1, 3, 1);
          iter += 2;
        }
      }
    }
    break;
  }
  print (vector.begin(), vector.end());
};

#endif
