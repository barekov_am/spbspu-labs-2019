#ifndef SECOND_HPP
#define SECOND_HPP

#include <iostream>
#include <memory>
#include <iterator>
#include <vector>
#include <stdexcept>
#include <fstream>

#include "base.hpp"
#include "howToSort.hpp"

void second(const char *argv[])
{
  const std::size_t max_size = 4096;
  std::size_t size = 0;
  std::size_t array_size = max_size;
  std::ifstream file (argv[2]);
  if (!file)
  {
    throw std::invalid_argument("open error");
  }
  std::unique_ptr<char[], decltype(&free)> info(static_cast<char *>(malloc(array_size)), &free);
  while (file)
  {
    file.read(info.get() + size, max_size);
    size += file.gcount();
    if (file)
    {
      array_size += max_size;
      std::unique_ptr<char[], decltype(&free)> temp(static_cast<char *>(realloc(info.get(),
                                                                                array_size)),
                                                                                &std::free);
      if (temp)
      {
        info.release();
        std::swap(info, temp);
      }
      else
      {
        throw std::runtime_error("error with array increasing");
      }
    }
  }
  if (!file.eof())
  {
    throw std::ios_base::failure("failure with file reading");
  }
  std::vector<char> vector1(&info[0], &info[size]);
  std::cout.write(&vector1[0], vector1.size());
};

#endif
