#ifndef FOURTH_HPP
#define FOURTH_HPP

#include <random>
#include <stdexcept>
#include <vector>
#include <iostream>
#include <ctime>

#include "base.hpp"
#include "howToSort.hpp"

void fillRandom(double * array, int size)
{
  std::mt19937 gen (time(0));
  std::uniform_real_distribution<double> randomize(-1.0, 1.0);
  for (int i = 0; i!= size; i++)
  {
    array[i] = randomize(gen);
  }
}

void fourth(const char *argv[])
{
  int size = std::stoi(argv[3]);
  if (size <= 0)
  {
    throw std::invalid_argument("size must be >= 0");
  }
  using Vector = std::vector<double>;
  Vector vector1 (size);
  fillRandom(vector1.data(), size);
  print(vector1.begin(), vector1.end());
  std::cout << "\n";
  std::function<bool(double,double)> comp = compareElem<double>(chooseWay(argv[2]));
  sort <BracketAccess>(vector1, comp);
  print(vector1.begin(), vector1.end());
};

#endif
