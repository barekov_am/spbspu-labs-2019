#ifndef HOW_TO_SORT_HPP
#define HOW_TO_SORT_HPP

#include <stdexcept>
#include <string>

enum howToSort {ascending, descending};

howToSort chooseWay (const std::string& dir)
{
  if (dir == "ascending")
  {
    return ascending;
  } 
  else if (dir == "descending")
  {
    return descending;
  } 
  else
  {
    throw std::invalid_argument("incorrect parameters input");
  }
}

#endif
