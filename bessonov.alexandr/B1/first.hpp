#ifndef FIRST_HPP
#define FIRST_HPP

#include <stdexcept>
#include <vector>
#include <forward_list>
#include <iostream>

#include "base.hpp"
#include "howToSort.hpp"

void first(const char *argv[])
{
  std::function<bool(int,int)> comp = compareElem<int>(chooseWay(argv[2]));
  using List = std::forward_list<int>;
  std::vector<int> vector1;
  int n = 1;
  size_t size = 0;
  while (std::cin && !(std::cin >> n).eof())
  {
    if (std::cin.fail() || std::cin.bad()) //написано, чтобы было исключение при ошибке во время ввода
    {
      throw std::ios_base::failure("reading error");
    }
    vector1.push_back(n);
    size++;
  }

  std::vector<int> vector2 (&vector1[0], &vector1[size]);
  List list1 (&vector1[0], &vector1[size]);

  if (vector1.empty() || vector2.empty() || list1.empty())
  {
    return;
  }

  sort <AtAccess>(vector1, comp);
  sort <BracketAccess>(vector2, comp);
  sort <IteratorAccess>(list1, comp);

  print (vector1.begin(), vector1.end());
  std::cout << "\n";
  print (vector2.begin(), vector2.end());
  std::cout << "\n";
  print (list1.begin(), list1.end());
};

#endif
