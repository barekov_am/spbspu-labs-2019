#include <stdexcept>
#include <algorithm>
#include <boost/test/auto_unit_test.hpp>
#include "layering.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(matrixTesting)

BOOST_AUTO_TEST_CASE(copyConstructorTest)
{
  bessonov::Rectangle rec1({1, 1}, 4, 7);
  bessonov::Circle cir1({2, 2}, 4);

  bessonov::CompositeShape compShape;
  compShape.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape.add(std::make_shared<bessonov::Circle>(cir1));

  bessonov::Matrix<bessonov::Shape> matrix;
  matrix = bessonov::split(compShape);
  bessonov::Matrix<bessonov::Shape> matrix1(matrix);
  BOOST_CHECK_EQUAL(matrix.getRowsCount(), matrix1.getRowsCount());
  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), matrix1.getMaxColSize());
  BOOST_CHECK(matrix[0][0] == matrix1[0][0]);
  BOOST_CHECK(matrix[1][0] == matrix1[1][0]);
}

BOOST_AUTO_TEST_CASE(moveConstructorTest)
{
  bessonov::Rectangle rec1({1, 1}, 4, 7);
  bessonov::Circle cir1({2, 2}, 4);


  bessonov::CompositeShape compShape;
  compShape.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape.add(std::make_shared<bessonov::Circle>(cir1));

  bessonov::Matrix<bessonov::Shape> matrix;
  matrix = bessonov::split(compShape);
  bessonov::Matrix<bessonov::Shape> matrixClone(matrix);

  bessonov::Matrix<bessonov::Shape> matrix1(std::move(matrix));
  BOOST_CHECK_EQUAL(matrixClone.getRowsCount(), matrix1.getRowsCount());
  BOOST_CHECK_EQUAL(matrixClone.getMaxColSize(), matrix1.getMaxColSize());
  BOOST_CHECK(matrixClone[0][0] == matrix1[0][0]);
  BOOST_CHECK(matrixClone[1][0] == matrix1[1][0]);
  BOOST_CHECK_EQUAL(matrix.getRowsCount(), 0);
  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), 0);
}

BOOST_AUTO_TEST_CASE(copyOperatorTest)
{
  bessonov::Rectangle rec1({1, 1}, 4, 7);
  bessonov::Circle cir1({2, 2}, 4);
  bessonov::Rectangle rec2({10, 14}, 4, 4);
  bessonov::CompositeShape compShape;
  compShape.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape.add(std::make_shared<bessonov::Circle>(cir1));

  bessonov::Matrix<bessonov::Shape> matrix = bessonov::split(compShape);
  compShape.add(std::make_shared<bessonov::Rectangle>(rec2));
  bessonov::Matrix<bessonov::Shape> matrix1 = bessonov::split(compShape);

  matrix1 = matrix;

  BOOST_CHECK_EQUAL(matrix.getRowsCount(), matrix1.getRowsCount());
  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), matrix1.getMaxColSize());
  BOOST_CHECK(matrix[0][0] == matrix1[0][0]);
  BOOST_CHECK(matrix[1][0] == matrix1[1][0]);
}

BOOST_AUTO_TEST_CASE(moveOperatorTest)
{
  bessonov::Rectangle rec1({1, 1}, 4, 7);
  bessonov::Circle cir1({2, 2}, 4);
  bessonov::Rectangle rec2({10, 14}, 1, 1);

  bessonov::CompositeShape compShape;
  compShape.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape.add(std::make_shared<bessonov::Circle>(cir1));

  bessonov::Matrix<bessonov::Shape> matrix;
  matrix = bessonov::split(compShape);
  compShape.add(std::make_shared<bessonov::Rectangle>(rec2));
  bessonov::Matrix<bessonov::Shape> matrixClone(matrix);
  bessonov::Matrix<bessonov::Shape> matrix1;
  matrix1 = bessonov::split(compShape);
  matrix1 = std::move(matrix);

  BOOST_CHECK_EQUAL(matrixClone.getRowsCount(), matrix1.getRowsCount());
  BOOST_CHECK_EQUAL(matrixClone.getMaxColSize(), matrix1.getMaxColSize());
  BOOST_CHECK(matrixClone[0][0] == matrix1[0][0]);
  BOOST_CHECK(matrixClone[1][0] == matrix1[1][0]);
  BOOST_CHECK_EQUAL(matrix.getRowsCount(), 0);
  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), 0);
}

BOOST_AUTO_TEST_CASE(addingTesting)
{
  bessonov::Rectangle rec1({1, 1}, 4, 7);
  bessonov::Circle cir1({2, 2}, 4);

  bessonov::CompositeShape compShape;
  compShape.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape.add(std::make_shared<bessonov::Circle>(cir1));

  bessonov::Matrix<bessonov::Shape> matrix;
  matrix = bessonov::split(compShape);
  size_t rowsBefore = matrix.getRowsCount();
  size_t maxBefore = matrix.getMaxColSize();

  bessonov::Rectangle rec2({10, 10}, 4, 4);
  matrix.add(0, std::make_shared<bessonov::Rectangle>(rec2));

  BOOST_CHECK_EQUAL(rowsBefore, matrix.getRowsCount());
  BOOST_CHECK_EQUAL(maxBefore + 1, matrix.getMaxColSize());
}

BOOST_AUTO_TEST_CASE(errorsDetectionTesting)
{
  bessonov::Rectangle rec1({1, 1}, 4, 7);
  bessonov::Circle cir1({2, 2}, 4);

  bessonov::CompositeShape compShape;
  compShape.add(std::make_shared<bessonov::Rectangle>(rec1));

  bessonov::Matrix<bessonov::Shape> matrix;
  matrix = bessonov::split(compShape);

  BOOST_CHECK_THROW(matrix[2], std::out_of_range);
  BOOST_CHECK_THROW(matrix[0][2], std::out_of_range);
  BOOST_CHECK_THROW(matrix.add(4, std::make_shared<bessonov::Circle>(cir1)), std::out_of_range);
  BOOST_CHECK_THROW(matrix.add(4, nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
