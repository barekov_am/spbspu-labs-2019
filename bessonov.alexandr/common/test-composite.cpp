#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double ERROR_VALUE = 0.001;

BOOST_AUTO_TEST_SUITE(compositeShape)

BOOST_AUTO_TEST_CASE(copyConstructor)
{
  bessonov::Rectangle rec1({8, 8}, 4, 6);
  bessonov::Circle cir1({3, 3}, 4);
  bessonov::CompositeShape compShape1;
  compShape1.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape1.add(std::make_shared<bessonov::Circle>(cir1));

  bessonov::CompositeShape compShape2(compShape1);
  const double area1 = compShape1.getArea();
  const double area2 = compShape2.getArea();
  const bessonov::rectangle_t frame1 = compShape1.getFrameRect();
  const bessonov::rectangle_t frame2 = compShape2.getFrameRect();

  BOOST_CHECK_CLOSE(area1, area2, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.height, frame2.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.width, frame2.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.pos.x, frame2.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.pos.y, frame2.pos.y, ERROR_VALUE);
  BOOST_CHECK_EQUAL(compShape1.size(), compShape2.size());
}

BOOST_AUTO_TEST_CASE(moveConstructor)
{
  bessonov::Rectangle rec1({8, 8}, 4, 6);
  bessonov::Circle cir1({3, 3}, 4);
  bessonov::CompositeShape compShape1;
  compShape1.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape1.add(std::make_shared<bessonov::Circle>(cir1));

  const double area1 = compShape1.getArea();
  const bessonov::rectangle_t frame1 = compShape1.getFrameRect();
  const int amount1 = compShape1.size();

  bessonov::CompositeShape compShape2(std::move(compShape1));
  const double area2 = compShape2.getArea();
  const bessonov::rectangle_t frame2 = compShape2.getFrameRect();
  const int amount2 = compShape2.size();

  BOOST_CHECK_CLOSE(area1, area2, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.height, frame2.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.width, frame2.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.pos.x, frame2.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.pos.y, frame2.pos.y, ERROR_VALUE);
  BOOST_CHECK_EQUAL(amount1, amount2);

  BOOST_CHECK_EQUAL(compShape1.size(), 0);
  BOOST_CHECK_THROW(compShape1.getArea(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(copyOperator)
{
  bessonov::Rectangle rec1({8, 8}, 4, 6);
  bessonov::Circle cir1({3, 3}, 4);
  bessonov::CompositeShape compShape1;
  compShape1.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape1.add(std::make_shared<bessonov::Circle>(cir1));

  bessonov::CompositeShape compShape2;
  bessonov::Rectangle rec2({7, 3}, 8, 10);
  compShape1.add(std::make_shared<bessonov::Rectangle>(rec2));

  compShape2 = compShape1;
  const double area1 = compShape1.getArea();
  const double area2 = compShape2.getArea();
  const bessonov::rectangle_t frame1 = compShape1.getFrameRect();
  const bessonov::rectangle_t frame2 = compShape2.getFrameRect();

  BOOST_CHECK_CLOSE(area1, area2, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.height, frame2.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.width, frame2.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.pos.x, frame2.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.pos.y, frame2.pos.y, ERROR_VALUE);
  BOOST_CHECK_EQUAL(compShape1.size(), compShape2.size());
}

BOOST_AUTO_TEST_CASE(moveOperator)
{
  bessonov::Rectangle rec1({8, 8}, 4, 6);
  bessonov::Circle cir1({3, 3}, 4);
  bessonov::CompositeShape compShape1;
  compShape1.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape1.add(std::make_shared<bessonov::Circle>(cir1));

  const double area1 = compShape1.getArea();
  const bessonov::rectangle_t frame1 = compShape1.getFrameRect();
  const size_t amount1 = compShape1.size();

  bessonov::CompositeShape compShape2 = std::move(compShape1);
  const double area2 = compShape2.getArea();
  const bessonov::rectangle_t frame2 = compShape2.getFrameRect();
  const size_t amount2 = compShape2.size();

  BOOST_CHECK_CLOSE(area1, area2, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.height, frame2.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.width, frame2.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.pos.x, frame2.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.pos.y, frame2.pos.y, ERROR_VALUE);
  BOOST_CHECK_EQUAL(amount1, amount2);

  BOOST_CHECK_EQUAL(compShape1.size(), 0);
  BOOST_CHECK_THROW(compShape1.getArea(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(comShapeInvariabilityAfterMovingInItself)
{
  bessonov::Rectangle rec1({8, 8}, 4, 6);
  bessonov::Circle cir1({3, 3}, 4);
  bessonov::CompositeShape compShape1;
  compShape1.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape1.add(std::make_shared<bessonov::Circle>(cir1));

  const double area1 = compShape1.getArea();
  const bessonov::rectangle_t frame1 = compShape1.getFrameRect();
  const size_t amount1 = compShape1.size();

  compShape1 = std::move(compShape1);

  BOOST_CHECK_CLOSE(area1, compShape1.getArea(), ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.height, compShape1.getFrameRect().height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.width, compShape1.getFrameRect().width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.pos.x, compShape1.getFrameRect().pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frame1.pos.y, compShape1.getFrameRect().pos.y, ERROR_VALUE);
  BOOST_CHECK_EQUAL(amount1, compShape1.size());
}

BOOST_AUTO_TEST_CASE(immutabilityOfcompositeShapeAfterChangingCenter)
{
  bessonov::Rectangle rec1({8, 8}, 4, 6);
  bessonov::Circle cir1({3, 3}, 4);
  bessonov::CompositeShape compShape;
  compShape.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape.add(std::make_shared<bessonov::Circle>(cir1));

  const double areaBeforeChangingCenter = compShape.getArea();
  const bessonov::rectangle_t frameBeforeChangingCenter = compShape.getFrameRect();
  compShape.move({2, 2});
  const double areaAfterChangingCenter = compShape.getArea();
  const bessonov::rectangle_t frameAfterChangingCenter = compShape.getFrameRect();
  BOOST_CHECK_CLOSE(areaBeforeChangingCenter, areaAfterChangingCenter, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBeforeChangingCenter.width, frameAfterChangingCenter.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBeforeChangingCenter.height, frameAfterChangingCenter.height, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(immutabilityOfcompositeShapeAfterMoving)
{
  bessonov::Rectangle rec1({8, 8}, 4, 6);
  bessonov::Circle cir1({3, 3}, 4);
  bessonov::CompositeShape compShape;
  compShape.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape.add(std::make_shared<bessonov::Circle>(cir1));

  const double areaBeforeDisplacement = compShape.getArea();
  const bessonov::rectangle_t frameBeforeDisplacement = compShape.getFrameRect();
  compShape.move(7, 8);
  const double areaAfterDisplacement = compShape.getArea();
  const bessonov::rectangle_t frameAfterDisplacement = compShape.getFrameRect();
  BOOST_CHECK_CLOSE(areaBeforeDisplacement, areaAfterDisplacement, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBeforeDisplacement.width, frameAfterDisplacement.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBeforeDisplacement.height, frameAfterDisplacement.height, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(changecompositeShapeAreaAfterScaling)
{
  bessonov::Rectangle rec1({6, 4}, 2, 6);
  bessonov::Circle cir1({1, 1}, 2);
  bessonov::CompositeShape compShape;
  compShape.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape.add(std::make_shared<bessonov::Circle>(cir1));

  const double areaBeforeScaling = compShape.getArea();
  const double factor1 = 2;
  compShape.scale(factor1);

  const double areaAfterScaling = compShape.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScaling * factor1 * factor1, areaAfterScaling, ERROR_VALUE);

  const double areaBeforeScaling1 = compShape.getArea();
  const double factor2 = 0.25;
  compShape.scale(factor2);

  const double areaAfterScaling1 = compShape.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScaling1 * factor2 * factor2, areaAfterScaling1, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(compShapeFrameChangeAfterScaling)
{
  bessonov::Rectangle rec1({ 6, 6 }, 4, 8);
  bessonov::Circle cir1({ 3, 3 }, 4);
  bessonov::CompositeShape compShape;
  compShape.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape.add(std::make_shared<bessonov::Circle>(cir1));

  const bessonov::rectangle_t frameBeforeScaling = compShape.getFrameRect();
  double factor = 1.6;
  compShape.scale(factor);

  const bessonov::rectangle_t frameAfterScaling = compShape.getFrameRect();
  BOOST_CHECK_CLOSE(frameBeforeScaling.width * factor, frameAfterScaling.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBeforeScaling.height * factor, frameAfterScaling.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBeforeScaling.pos.x, frameAfterScaling.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBeforeScaling.pos.y, frameAfterScaling.pos.y, ERROR_VALUE);

  factor = 0.6;
  compShape.scale(factor);

  const bessonov::rectangle_t frameAfterScaling1 = compShape.getFrameRect();
  BOOST_CHECK_CLOSE(frameAfterScaling.width * factor, frameAfterScaling1.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameAfterScaling.height * factor, frameAfterScaling1.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameAfterScaling.pos.x, frameAfterScaling1.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameAfterScaling.pos.y, frameAfterScaling1.pos.y, ERROR_VALUE);

}

BOOST_AUTO_TEST_CASE(compShapeAfterAdding)
{
  bessonov::Rectangle rec1({4, 4}, 2, 6);
  bessonov::Circle cir1({1, 1}, 2);
  bessonov::CompositeShape compShape;
  compShape.add(std::make_shared<bessonov::Rectangle>(rec1));
  const size_t amountWithoutCircle = compShape.size();

  const double areaBeforeAdding = compShape.getArea();
  compShape.add(std::make_shared<bessonov::Circle>(cir1));
  const size_t amountWithCircle = compShape.size();
  const double areaOfAddedShape = cir1.getArea();
  const double areaAfterAdding = compShape.getArea();
  BOOST_CHECK_CLOSE(areaBeforeAdding + areaOfAddedShape, areaAfterAdding, ERROR_VALUE);
  BOOST_CHECK_EQUAL(amountWithoutCircle + 1, amountWithCircle);
}

BOOST_AUTO_TEST_CASE(compShapeAfterDeleting)
{
  bessonov::Rectangle rec1({4, 4}, 2, 6);
  bessonov::Circle cir1({1, 1}, 2);
  bessonov::CompositeShape compShape;
  compShape.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape.add(std::make_shared<bessonov::Circle>(cir1));
  const size_t amountWithCircle = compShape.size();

  const double areaBeforeDeleting = compShape.getArea();
  const double areaOfDeletedShape = cir1.getArea();
  compShape.remove(1);
  const size_t amountWithoutCircle = compShape.size();
  const double areaAfterDeleting = compShape.getArea();
  BOOST_CHECK_CLOSE(areaBeforeDeleting - areaOfDeletedShape, areaAfterDeleting, ERROR_VALUE);
  BOOST_CHECK_EQUAL(amountWithCircle - 1, amountWithoutCircle);
}

BOOST_AUTO_TEST_CASE(logicErrorsDetection)
{
  bessonov::CompositeShape compShape;

  BOOST_CHECK_THROW(compShape.getArea(), std::logic_error);
  BOOST_CHECK_THROW(compShape.remove(0), std::logic_error);
  BOOST_CHECK_THROW(compShape.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(compShape.move(4, 4), std::logic_error);
  BOOST_CHECK_THROW(compShape.move({4, 4}), std::logic_error);
  BOOST_CHECK_THROW(compShape.scale(4), std::logic_error);
}

BOOST_AUTO_TEST_CASE(invalidArgumentsErrorsDetection)
{
  bessonov::Rectangle rec2({2, 2}, 2, 4);

  bessonov::CompositeShape compShape;
  compShape.add(std::make_shared<bessonov::Rectangle>(rec2));
  BOOST_CHECK_THROW(compShape.scale(-2), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.add(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(outOfRangeErrorsDetection)
{
  bessonov::Rectangle rec1({4, 4}, 2, 3);
  bessonov::Circle cir1({1, 1}, 2);
  bessonov::CompositeShape compShape;
  compShape.add(std::make_shared<bessonov::Rectangle>(rec1));
  compShape.add(std::make_shared<bessonov::Circle>(cir1));

  BOOST_CHECK_THROW(compShape[3], std::out_of_range);
  BOOST_CHECK_THROW(compShape.remove(-1), std::out_of_range);
  BOOST_CHECK_THROW(compShape.remove(10), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compShapeAfterRotation)
{
  bessonov::CompositeShape compShape;
  compShape.add(std::make_shared<bessonov::Circle>(bessonov::point_t{2,2}, 1));
  compShape.add(std::make_shared<bessonov::Rectangle>(bessonov::point_t{4,2}, 2, 2));

  const bessonov::rectangle_t frameBefore = compShape.getFrameRect();
  const double areaBefore = compShape.getArea();
  compShape.rotate(90);
  const double areaAfter = compShape.getArea();
  const bessonov::rectangle_t frameAfter = compShape.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.pos.x, frameAfter.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.pos.y, frameAfter.pos.y, ERROR_VALUE);
}

BOOST_AUTO_TEST_SUITE_END()
