#ifndef A4_LAYERING_HPP
#define A4_LAYERING_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace bessonov
{
  Matrix<Shape> split(const CompositeShape &compShape);
  bool intersect(const bessonov::Shape &shape1, const bessonov::Shape &shape2);
}

#endif
