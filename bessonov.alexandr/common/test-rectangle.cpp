#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "rectangle.hpp"

const double ERROR_VALUE = 0.001;

BOOST_AUTO_TEST_SUITE(rectangleTesting);

BOOST_AUTO_TEST_CASE(immutabilityOfRectAfterChangingCenter)
{
  bessonov::Rectangle recTest1({8, 8}, 10, 10);
  const bessonov::rectangle_t framesBeforeChange = recTest1.getFrameRect();
  recTest1.move({6, 20});
  const bessonov::rectangle_t framesAfterChange = recTest1.getFrameRect();
  BOOST_CHECK_CLOSE(framesBeforeChange.height, framesAfterChange.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(framesBeforeChange.width, framesAfterChange.width, ERROR_VALUE);

  bessonov::Rectangle recTest2({7, 18}, 16, 10);
  const double areaBeforeChange = recTest2.getArea();
  recTest2.move({10, 20});
  const double areaAfterChange = recTest2.getArea();
  BOOST_CHECK_CLOSE(areaBeforeChange, areaAfterChange, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(immutabilityOfRectAfterMoving)
{
  bessonov::Rectangle recTest1({5, 5}, 10, 10);
  const bessonov::rectangle_t framesBeforeDisp = recTest1.getFrameRect();
  recTest1.move(12, 5);
  const bessonov::rectangle_t framesAfterDisp = recTest1.getFrameRect();
  BOOST_CHECK_CLOSE(framesBeforeDisp.height, framesAfterDisp.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(framesBeforeDisp.width, framesAfterDisp.width, ERROR_VALUE);

  bessonov::Rectangle recTest2({2, 5}, 8, 5);
  const double areaBeforeDisp = recTest2.getArea();
  recTest2.move(4, 3);
  const double areaAfterDisp = recTest2.getArea();
  BOOST_CHECK_CLOSE(areaBeforeDisp, areaAfterDisp, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(changeRectAreaAfterScaling)
{
  bessonov::Rectangle recTest1({6, 6}, 10, 12);
  const double areaBeforeScaling = recTest1.getArea();
  const double testFactor = 4;
  const double squareDiff = testFactor * testFactor;
  recTest1.scale(testFactor);
  const double areaAfterScaling = recTest1.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScaling * squareDiff, areaAfterScaling, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(checkRectArg)
{
  BOOST_CHECK_THROW(bessonov::Rectangle({6, 9}, -5, 7), std::invalid_argument);
  BOOST_CHECK_THROW(bessonov::Rectangle({6, 9}, 9, -15), std::invalid_argument);
  bessonov::Rectangle recTest({9, 10}, 5, 8.5);
  const double factor = -3;
  BOOST_CHECK_THROW(recTest.scale(factor), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectParametersAfterRotation)
{
  bessonov::Rectangle rectangle({ 1, 1 }, 2, 5);
  const bessonov::rectangle_t frameBefore = rectangle.getFrameRect();
  const double areaBefore = rectangle.getArea();
  rectangle.rotate(90);
  const double areaAfter = rectangle.getArea();
  const bessonov::rectangle_t frameAfter = rectangle.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.pos.x, frameAfter.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.pos.y, frameAfter.pos.y, ERROR_VALUE);
}

BOOST_AUTO_TEST_SUITE_END()
