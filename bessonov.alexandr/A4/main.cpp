#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "layering.hpp"

void printRectFrames(const bessonov::Shape &shape)
{
  bessonov::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "{" << frameRect.pos.x << ","
    << frameRect.pos.y << "},"
    << frameRect.width << ","
    << frameRect.height << "\n";
}

void printMatrixInfo(const bessonov::Matrix<bessonov::Shape> &matrix)
{
  for (size_t i = 0; i < matrix.getRowsCount(); i++)
  {
    std::cout << "Layer number " << i + 1 << " : \n";
    std::cout << "Amount of shapes : " << matrix[i].getSize() << "\n";
    for (size_t j = 0; j < matrix[i].getSize(); j++)
    {
      std::cout << "Shape number " << j + 1 << " with parameters : ";
      printRectFrames(*matrix[i][j]);
    }
  }
}

int main()
{
  bessonov::Rectangle rec1({ 3, 2 }, 1, 8);
  rec1.move(5, 8);
  std::cout << rec1.getArea() << "\n";
  printRectFrames(rec1);

  bessonov::Rectangle rec2({ 2, 1 }, 4, 2);
  rec2.move({ 2, 1 });
  printRectFrames(rec2);

  bessonov::Rectangle rec3({ 1, 4 }, 2, 2);
  rec3.scale(5);
  printRectFrames(rec3);

  bessonov::Circle cir1({ 1, 7 }, 5);
  cir1.move({ 3, 5 });
  std::cout << cir1.getArea() << "\n";
  printRectFrames(cir1);

  bessonov::Circle cir2({ 8, 4 }, 4);
  cir2.move(4, 3);
  printRectFrames(cir2);

  bessonov::Circle cir3({ 3, 5 }, 4);
  cir3.scale(1.6);
  printRectFrames(cir3);


  std::cout << "Composite shapes : " << "\n";

  bessonov::Rectangle rec4({ 7, 9 }, 4, 3);
  std::cout << "Area of rectangle is " << rec4.getArea() << "\n";
  bessonov::Circle cir4({ 1, 1 }, 1);
  std::cout << "Area of circle is " << cir4.getArea() << "\n";

  bessonov::CompositeShape compShape1;
  compShape1.add(std::make_shared<bessonov::Rectangle>(rec4));
  printRectFrames(compShape1);
  compShape1.add(std::make_shared<bessonov::Circle>(cir4));
  printRectFrames(compShape1);
  std::cout << "General area of 1 composite shape is " << compShape1.getArea() << "\n";

  compShape1.move(5, 5);
  printRectFrames(compShape1);
  compShape1.move({ 6, 8 });
  printRectFrames(compShape1);
  compShape1.scale(2);
  std::cout << "Area of 1 composite shape after multiplication by 2 is " << compShape1.getArea() << "\n";
  printRectFrames(compShape1);
  compShape1.scale(0.19);
  std::cout << "Area of 1 composite shape after multiplication by 0.19 is " << compShape1.getArea() << "\n";
  printRectFrames(compShape1);

  compShape1.remove(0);
  std::cout << "General area of 1 composite shape after removing rectangle is " << compShape1.getArea() << "\n";

  bessonov::CompositeShape compShape2(compShape1);
  std::cout << "Area of 2 composite shape is " << compShape2.getArea() << "\n";
  bessonov::Rectangle rec5({ 15, 15 }, 2, 4);
  std::cout << "Area of rectangle is " << rec5.getArea() << "\n";

  compShape2.add(std::make_shared<bessonov::Rectangle>(rec5));
  std::cout << "Area of 2 composite shape after adding rectangle is " << compShape2.getArea() << "\n";
  std::cout << "There are " << compShape2.size() << " shape_s left" << "\n";
  std::cout << "The area of last shape is " << compShape2[1]->getArea() << "\n";


  std::cout << "Layering composite shape :" << "\n";
  bessonov::CompositeShape compShape3;
  bessonov::Rectangle rec6({ 1, 1 }, 4, 8);
  bessonov::Circle cir6({ 2, 2 }, 1);
  bessonov::Circle cir7({ 8, 8 }, 3);
  compShape3.add(std::make_shared<bessonov::Rectangle>(rec6));
  compShape3.add(std::make_shared<bessonov::Circle>(cir6));
  compShape3.add(std::make_shared<bessonov::Circle>(cir7));

  bessonov::Matrix<bessonov::Shape> matrix;
  matrix = bessonov::split(compShape3);
  printMatrixInfo(matrix);
  std::cout << "The biggest number of shapes in a layer is " << matrix.getMaxColSize() << " shapes \n";

  return 0;
}
