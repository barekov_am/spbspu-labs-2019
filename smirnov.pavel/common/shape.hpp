#ifndef SHAPE_H
#define SHAPE_H
#include "base-types.hpp"
#include <memory>

namespace smirnov
{
  class Shape
  {
  public:
    using shape_pointer = std::shared_ptr<Shape>;
    using array_pointer = std::unique_ptr<shape_pointer[]>;

    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t & new_point) = 0;
    virtual void move(double x_delta, double y_delta) = 0;
    virtual void scale(double coefficient) = 0;
    virtual void rotate(double d_angle) = 0;
    virtual void getInfo() const = 0;
    virtual point_t getPos() const = 0;
  };
}

#endif
