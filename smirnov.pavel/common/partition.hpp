#ifndef PARTITION_H
#define PARTITION_H

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace smirnov
{
  Matrix partition(const Shape::array_pointer & array, size_t size);
  Matrix partition(const CompositeShape & comp_shape);
}

#endif
