#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"

namespace smirnov
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &);
    Matrix(Matrix &&);
    ~Matrix() = default;

    Matrix & operator =(const Matrix &);
    Matrix & operator =(Matrix &&);
    Shape::array_pointer operator [](size_t) const;
    bool operator ==(const Matrix &) const;
    bool operator !=(const Matrix &) const;

    void add(Shape::shape_pointer, size_t, size_t);
    size_t getRows() const;
    size_t getColumns() const;
    size_t getSizeLayer(size_t) const;

  private:
    size_t rows_;
    size_t columns_;
    Shape::array_pointer list_;
  };
}

#endif
