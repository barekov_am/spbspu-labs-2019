#define _USE_MATH_DEFINES
#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "shape.hpp"

namespace smirnov
{
  class Rectangle: public Shape
  {
  public:
    Rectangle(double width, double height, const point_t & center);
    double getArea() const;
    rectangle_t getFrameRect() const override;
    void move(const point_t & new_point) override;
    void move(double x_delta, double y_delta) override;
    void scale(double coefficient) override;
    void rotate(double d_angle) override;
    void getInfo() const override;
    point_t getPos() const override;

  private:
    double width_;
    double height_;
    point_t center_;
    double angle_;
  };
}

#endif
