#define _USE_MATH_DEFINES
#ifndef COMPOSITE_SHAPE_H
#define COMPOSITE_SHAPE_H
#include "shape.hpp"
#include <memory>

namespace smirnov
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape & rhs);
    CompositeShape(CompositeShape && rhs);
    CompositeShape(Shape::shape_pointer shape);
    ~CompositeShape() = default;

    CompositeShape & operator =(const CompositeShape & rhs);
    CompositeShape & operator =(CompositeShape && rhs);
    Shape::shape_pointer operator [](size_t number) const;

    void add(Shape::shape_pointer shape);
    void remove(size_t number);
    void remove(Shape::shape_pointer shape);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & new_point) override;
    void move(double x_delta, double y_delta) override;
    void scale(double coefficient) override;
    void rotate(double d_angle) override;
    size_t getCount() const;
    void getInfo() const override;
    point_t getPos() const override;
    Shape::array_pointer list() const;

  private:
    size_t count_;
    Shape::array_pointer array_of_figures_;
  };
}

#endif
