#include "matrix.hpp"
#include <stdexcept>
#include <utility>

smirnov::Matrix::Matrix():
  rows_(0),
  columns_(0)
{}

smirnov::Matrix::Matrix(const Matrix & source):
  rows_(source.rows_),
  columns_(source.columns_),
  list_(std::make_unique<Shape::shape_pointer[]>(source.rows_ * source.columns_))
{
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    list_[i] = source.list_[i];
  }
}

smirnov::Matrix::Matrix(Matrix && source):
  rows_(source.rows_),
  columns_(source.columns_),
  list_(std::move(source.list_))
{}

smirnov::Matrix & smirnov::Matrix::operator =(const Matrix & rhs)
{
  if (this != &rhs)
  {
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
    Shape::array_pointer tmp_list(std::make_unique<Shape::shape_pointer[]>(rhs.rows_ * rhs.columns_));
    for (size_t i = 0; i < (rows_ * columns_); i++)
    {
      tmp_list[i] = rhs.list_[i];
    }
    list_.swap(tmp_list);
  }
  return *this;
}

smirnov::Matrix & smirnov::Matrix::operator =(Matrix && rhs)
{
  if (this != &rhs)
  {
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
    list_ = std::move(rhs.list_);
  }
  return *this;
}

smirnov::Shape::array_pointer smirnov::Matrix::operator [](size_t rhs) const
{
  if (rhs >= rows_)
  {
    throw std::out_of_range("Index is out of range!");
  }
  Shape::array_pointer tmp_list(std::make_unique<Shape::shape_pointer[]>(columns_));
  for (size_t i = 0; i < columns_; i++)
  {
    tmp_list[i] = list_[rhs * columns_ + i];
  }
  return tmp_list;
}

bool smirnov::Matrix::operator ==(const Matrix & rhs) const
{
  if ((rows_ != rhs.rows_) || (columns_ != rhs.columns_))
  {
    return false;
  }
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (list_[i] != rhs.list_[i])
    {
      return false;
    }
  }
  return true;
}

bool smirnov::Matrix::operator !=(const Matrix & rhs) const
{
  return !(*this == rhs);
}

void smirnov::Matrix::add(Shape::shape_pointer shape, size_t row, size_t column)
{
  size_t tmp_rows = (row == rows_) ? (rows_ + 1) : (rows_);
  size_t tmp_columns = (column == columns_) ? (columns_ + 1) : (columns_);
  Shape::array_pointer tmp_list(std::make_unique<Shape::shape_pointer[]>(tmp_rows * tmp_columns));
  for (size_t i = 0; i < rows_; i++)
  {
    for (size_t j = 0; j < columns_; j++)
    {
      tmp_list[i * tmp_columns + j] = list_[i * columns_ + j];
    }
  }
  tmp_list[row * tmp_columns + column] = shape;
  list_.swap(tmp_list);
  rows_ = tmp_rows;
  columns_ = tmp_columns;
}

size_t smirnov::Matrix::getRows() const
{
  return rows_;
}

size_t smirnov::Matrix::getColumns() const
{
  return columns_;
}

size_t smirnov::Matrix::getSizeLayer(size_t layer) const
{
  if (layer >= rows_)
  {
    return 0;
  }
  for (size_t i = 0; i < columns_; i++)
  {
    if (list_[layer * columns_ + i] == nullptr)
    {
      return 1;
    }
  }
  return columns_;
}
