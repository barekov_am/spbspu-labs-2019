#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testOfRectangle)

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_CASE(testOfRectangleMoving)
{
  smirnov::Rectangle rect(15, 10, {0, 0});
  const double area_before_moving = rect.getArea();
  const smirnov::rectangle_t rect_before_moving = rect.getFrameRect();
  rect.move({-4, 7});
  BOOST_CHECK_CLOSE(rect_before_moving.width, rect.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(rect_before_moving.height, rect.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(area_before_moving, rect.getArea(), ACCURACY);
  rect.move(15, -3);
  BOOST_CHECK_CLOSE(rect_before_moving.width, rect.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(rect_before_moving.height, rect.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(area_before_moving, rect.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(testOfRectangleScaling)
{
  smirnov::Rectangle rect(4, 6, {0, 0});
  const double area_before_moving = rect.getArea();
  const double scaling_coef = 3;
  rect.scale(scaling_coef);
  BOOST_CHECK_CLOSE(area_before_moving * scaling_coef * scaling_coef, rect.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(testOfAreaAfterRotating)
{
  smirnov::Rectangle rect(4, 4, {5, 8});
  double old_area = rect.getArea();
  const double angle = 45;
  rect.rotate(angle);
  BOOST_CHECK_CLOSE(old_area, rect.getArea(), ACCURACY);
  old_area = rect.getArea();
  rect.rotate(-50);
  BOOST_CHECK_CLOSE(old_area, rect.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(testOfInvalidscalingcoefInRectangle)
{
  smirnov::Rectangle rect(10, 5, {0, 0});
  BOOST_CHECK_THROW(rect.scale(-2), std::invalid_argument);
  BOOST_CHECK_THROW(rect.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testOfInvalidParametersOfRectangle)
{
  BOOST_CHECK_THROW(smirnov::Rectangle rect(-5, 3, {0, 0}), std::invalid_argument);
  BOOST_CHECK_THROW(smirnov::Rectangle rect(5, -3, {0, 0}), std::invalid_argument);
  BOOST_CHECK_THROW(smirnov::Rectangle rect(-5, -3, {0, 0}), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
