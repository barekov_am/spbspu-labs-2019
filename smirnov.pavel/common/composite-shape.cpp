#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <utility>
#include <cmath>

smirnov::CompositeShape::CompositeShape():
  count_(0)
{}

smirnov::CompositeShape::CompositeShape(const CompositeShape & rhs):
  count_(rhs.count_),
  array_of_figures_(std::make_unique<shape_pointer[]>(count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    array_of_figures_[i] = rhs.array_of_figures_[i];
  }
}

smirnov::CompositeShape::CompositeShape(CompositeShape && rhs):
  count_(rhs.count_),
  array_of_figures_(std::move(rhs.array_of_figures_))
{
  rhs.count_ = 0;
}

smirnov::CompositeShape::CompositeShape(Shape::shape_pointer shape):
  count_(1),
  array_of_figures_(std::make_unique<Shape::shape_pointer[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Null pointer is not figure!");
  }
  array_of_figures_[0] = shape;
}

smirnov::CompositeShape & smirnov::CompositeShape::operator =(const CompositeShape & rhs)
{
  if (this == &rhs)
  {
    return *this;
  }
  count_ = rhs.count_;
  Shape::array_pointer new_array(std::make_unique<Shape::shape_pointer[]>(count_));
  for (size_t i = 0; i < count_; i++)
  {
    new_array[i] = rhs.array_of_figures_[i];
  }
  array_of_figures_.swap(new_array);

  return *this;
}

smirnov::CompositeShape & smirnov::CompositeShape::operator =(CompositeShape && rhs)
{
  if (this == & rhs)
  {
    return *this;
  }
  count_ = rhs.count_;
  array_of_figures_ = std::move(rhs.array_of_figures_);
  rhs.count_ = 0;

  return *this;
}

smirnov::Shape::shape_pointer smirnov::CompositeShape::operator [](size_t number) const
{
  if (number >= count_)
  {
    throw std::invalid_argument("Enter valid number!");
  }
  return array_of_figures_[number];
}

void smirnov::CompositeShape::add(Shape::shape_pointer shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Null pointer is not figure!");
  }
  Shape::array_pointer new_array(std::make_unique<Shape::shape_pointer[]>(count_ + 1));
  for (size_t i = 0; i < count_; i++)
  {
    new_array[i] = array_of_figures_[i];
  }
  new_array[count_] = shape;
  count_ ++;
  array_of_figures_.swap(new_array);
}

void smirnov::CompositeShape::remove(size_t number)
{
  if (number >= count_)
  {
    throw std::invalid_argument("Enter valid number!");
  }
  count_ --;
  for (size_t i = number; i < count_; i++)
  {
    array_of_figures_[i] = array_of_figures_[i + 1];
  }
  array_of_figures_[count_] = nullptr;
}

void smirnov::CompositeShape::remove(Shape::shape_pointer shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Null pointer is not figure!");
  }
  size_t i = 0;
  while ((array_of_figures_[i] != shape) && (i < count_))
  {
    i++;
  }
  if (i == count_)
  {
    throw std::invalid_argument("There is not such figure in composite shape!");
  }
    remove(i);
}

double smirnov::CompositeShape::getArea() const
{
  double total_area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    total_area += array_of_figures_[i]->getArea();
  }

  return total_area;
}

smirnov::rectangle_t smirnov::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    return {0, 0, {0, 0}};
  }
  rectangle_t frame_rect = array_of_figures_[0]->getFrameRect();
  double max_x = frame_rect.pos.x + frame_rect.width / 2;
  double max_y = frame_rect.pos.y + frame_rect.height / 2;
  double min_x = frame_rect.pos.x - frame_rect.width / 2;
  double min_y = frame_rect.pos.y - frame_rect.height / 2;
  for (size_t i = 1; i < count_; i++)
  {
    frame_rect = array_of_figures_[1]->getFrameRect();
    max_x = std::max(max_x, frame_rect.pos.x + frame_rect.width / 2);
    max_y = std::max(max_y, frame_rect.pos.y + frame_rect.height / 2);
    min_x = std::min(min_x, frame_rect.pos.x - frame_rect.width / 2);
    min_y = std::min(min_y, frame_rect.pos.y - frame_rect.height / 2);
  }
  double width = max_x - min_x;
  double height = max_y - min_y;
  point_t center = {min_x + width/ 2, min_y + height / 2};

  return {width, height, center};
}

void smirnov::CompositeShape::move(const point_t & new_point)
{
  double x_delta = new_point.x - getFrameRect().pos.x;
  double y_delta = new_point.y - getFrameRect().pos.y;
  move(x_delta, y_delta);
}

void smirnov::CompositeShape::move(double x_delta, double y_delta)
{
  for (size_t i = 0; i < count_; i++)
  {
    array_of_figures_[i]->move(x_delta, y_delta);
  }
}

void smirnov::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient must be positive!");
  }
  point_t center = getFrameRect().pos;
  for (size_t i = 0; i < count_; i++)
  {
    double new_x = array_of_figures_[i]->getFrameRect().pos.x * coefficient + center.x * (1-coefficient);
    double new_y = array_of_figures_[i]->getFrameRect().pos.y * coefficient + center.y * (1-coefficient);
    array_of_figures_[i]->move({new_x, new_y});
    array_of_figures_[i]->scale(coefficient);
  }
}

void smirnov::CompositeShape::rotate(double d_angle)
{
  const double cos_a = fabs(cos(d_angle * M_PI / 180));
  const double sin_a = fabs(sin(d_angle * M_PI / 180));
  const point_t cs_center = getFrameRect().pos;
  for (size_t i = 0; i < count_; i++)
  {
    double x = array_of_figures_[i]->getFrameRect().pos.x;
    double y = array_of_figures_[i]->getFrameRect().pos.y;
    double new_x = cs_center.x + (x - cs_center.x) * cos_a - (y - cs_center.y) * sin_a;
    double new_y = cs_center.y + (x - cs_center.x) * sin_a + (y - cs_center.y) * cos_a;
    array_of_figures_[i]->move({new_x, new_y});
    array_of_figures_[i]->rotate(d_angle);
  }
}

size_t smirnov::CompositeShape::getCount() const
{
  return count_;
}

void smirnov::CompositeShape::getInfo() const
{
  rectangle_t frame_rect = getFrameRect();
  std::cout << "Number of figures in composite shape is " << count_ << "\n";
  std::cout << "Area of composite shape is " << getArea() << "\n";
  std::cout << "Center of composite shape is (" << frame_rect.pos.x << ";" << frame_rect.pos.y << ")\n";
  std::cout << "Frame rectangle's width is " << frame_rect.width << " \n";
  std::cout << "Frame rectangle's height is " << frame_rect.height << " \n";
}

smirnov::point_t smirnov::CompositeShape::getPos() const
{
  return getFrameRect().pos;
}

smirnov::Shape::array_pointer smirnov::CompositeShape::list() const
{
  Shape::array_pointer tmp_array(std::make_unique<Shape::shape_pointer[]>(count_));
  for (size_t i = 0; i < count_; i++)
  {
    tmp_array[i] = array_of_figures_[i];
  }

  return tmp_array;
}
