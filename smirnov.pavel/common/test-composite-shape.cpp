#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(testOfCompositeShape)

using shape_ptr = std::shared_ptr<smirnov::Shape>;
const double ACCURACY = 0.01;

BOOST_AUTO_TEST_CASE(testOfCompositeShapeMoving)
{
  shape_ptr circ = std::make_shared<smirnov::Circle>(5, smirnov::point_t {0, 0});
  shape_ptr rect = std::make_shared<smirnov::Rectangle>(15, 10, smirnov::point_t {10, 15});
  smirnov::CompositeShape comp_shape(circ);
  comp_shape.add(rect);
  const smirnov::rectangle_t frame_rect = comp_shape.getFrameRect();
  const double area_before_moving = comp_shape.getArea();
  comp_shape.move({-4, 7});
  BOOST_CHECK_CLOSE(frame_rect.width, comp_shape.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(frame_rect.height, comp_shape.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(area_before_moving, comp_shape.getArea(), ACCURACY);
  comp_shape.move(15, -3);
  BOOST_CHECK_CLOSE(frame_rect.width, comp_shape.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(frame_rect.height, comp_shape.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(area_before_moving, comp_shape.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(testOfCompositeShapeScaling)
{
  shape_ptr circ = std::make_shared<smirnov::Circle>(5, smirnov::point_t {0, 0});
  shape_ptr rect = std::make_shared<smirnov::Rectangle>(15, 10, smirnov::point_t {10, 15});
  smirnov::CompositeShape comp_shape(circ);
  comp_shape.add(rect);
  const double area_before_moving = comp_shape.getArea();
  const double scaling_coef = 3;
  comp_shape.scale(scaling_coef);
  BOOST_CHECK_CLOSE(area_before_moving * scaling_coef * scaling_coef, comp_shape.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(testOfMethodsOfCompositeShape)
{
  shape_ptr circ = std::make_shared<smirnov::Circle>(5, smirnov::point_t {0, 0});
  shape_ptr rect = std::make_shared<smirnov::Rectangle>(15, 10, smirnov::point_t {10, 15});
  smirnov::CompositeShape comp_shape(circ);
  comp_shape.add(rect);
  BOOST_CHECK_THROW(comp_shape.add(nullptr), std::invalid_argument);
  BOOST_CHECK_NO_THROW(comp_shape.add(rect));
  BOOST_CHECK_NO_THROW(comp_shape.remove(1));
  BOOST_CHECK_THROW(comp_shape.remove(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(comp_shape.remove(10), std::invalid_argument);
  BOOST_CHECK_THROW(comp_shape.remove(-3), std::invalid_argument);
  BOOST_CHECK_THROW(comp_shape.scale(-5), std::invalid_argument);
  BOOST_CHECK_NO_THROW(comp_shape.scale(4));
  BOOST_CHECK_NO_THROW(comp_shape.scale(0.4));
  BOOST_CHECK_NO_THROW(comp_shape[0]->getInfo());
  BOOST_CHECK_THROW(comp_shape[-5]->getInfo(), std::invalid_argument);
  BOOST_CHECK_THROW(comp_shape[5]->getInfo(), std::invalid_argument);
  BOOST_CHECK_NO_THROW(comp_shape.rotate(50));
  BOOST_CHECK_NO_THROW(comp_shape.rotate(-80));
}

BOOST_AUTO_TEST_CASE(testOfAreaAfterRotating)
{
  shape_ptr circ = std::make_shared<smirnov::Circle>(5, smirnov::point_t {0, 0});
  shape_ptr rect = std::make_shared<smirnov::Rectangle>(15, 10, smirnov::point_t {10, 15});
  smirnov::CompositeShape comp;
  comp.add(circ);
  comp.add(rect);
  double old_area = comp.getArea();
  double angle = 40;
  comp.rotate(angle);
  BOOST_CHECK_CLOSE(old_area, comp.getArea(), ACCURACY);
  old_area = comp.getArea();
  angle = -30;
  comp.rotate(angle);
  BOOST_CHECK_CLOSE(old_area, comp.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(testOfCopyingAndMoving)
{
  shape_ptr circ = std::make_shared<smirnov::Circle>(5, smirnov::point_t {0, 0});
  BOOST_CHECK_NO_THROW(smirnov::CompositeShape composite_shape());
  BOOST_CHECK_NO_THROW(smirnov::CompositeShape composite_shape(circ));
  BOOST_CHECK_THROW(smirnov::CompositeShape comp_shape(nullptr), std::invalid_argument);
  smirnov::CompositeShape comp_shape(circ);
  BOOST_CHECK_NO_THROW(smirnov::CompositeShape composite_shape(comp_shape));
  BOOST_CHECK_NO_THROW(smirnov::CompositeShape composite_shape(std::move(comp_shape)));
  shape_ptr rect = std::make_shared<smirnov::Rectangle>(15, 10, smirnov::point_t {10, 15});
  smirnov::CompositeShape composite_shape(rect);
  BOOST_CHECK_NO_THROW(comp_shape = composite_shape);
  BOOST_CHECK_NO_THROW(comp_shape = std::move(composite_shape));
}

BOOST_AUTO_TEST_SUITE_END()
