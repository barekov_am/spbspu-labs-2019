#include "partition.hpp"
#include "shape.hpp"

smirnov::Matrix smirnov::partition(const Shape::array_pointer & array, size_t size)
{
  Matrix matrix;
  for (size_t i = 0; i < size; i++)
  {
    size_t right_row = 0;
    size_t right_column = 0;
    size_t necessary_column = 0;
    for (size_t j = 0; j < matrix.getRows(); j++)
    {
      necessary_column = matrix.getSizeLayer(j);
      for (size_t k = 0; k < matrix.getColumns(); k++)
      {
        if (intersect(array[i]->getFrameRect(), matrix[j][k]->getFrameRect()))
        {
          right_row++;
          break;
        }
        if (k == necessary_column - 1)
        {
          right_row = j;
          right_column = necessary_column;
        }
      }
      if (right_row == j)
      {
        break;
      }
    }
    matrix.add(array[i], right_row, right_column);
  }
  return matrix;
}

smirnov::Matrix smirnov::partition(const CompositeShape & comp_shape)
{
  return partition(comp_shape.list(), comp_shape.getCount());
}
