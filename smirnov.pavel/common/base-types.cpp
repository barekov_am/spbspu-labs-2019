#include "base-types.hpp"
#include <math.h>

bool smirnov::intersect(const rectangle_t & lhs, const rectangle_t & rhs)
{
  const bool width_condition = std::fabs(lhs.pos.x - rhs.pos.x) < (std::fabs(lhs.width + rhs.width) / 2);
  const bool height_condition = std::fabs(lhs.pos.y - rhs.pos.y) < (std::fabs(lhs.height + rhs.height) / 2);

  return width_condition && height_condition;
}
