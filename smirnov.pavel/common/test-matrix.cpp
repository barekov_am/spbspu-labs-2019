#include <stdexcept>
#include <utility>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(matrixTestSuite)

BOOST_AUTO_TEST_CASE(copyAndMove)
{
  smirnov::Shape::shape_pointer testCircle = std::make_shared<smirnov::Circle>(1, smirnov::point_t {2, 3});
  smirnov::CompositeShape testComposite;
  testComposite.add(testCircle);

  smirnov::Matrix testMatrix = smirnov::partition(testComposite);
  BOOST_CHECK_NO_THROW(smirnov::Matrix testMatrix2(testMatrix));
  BOOST_CHECK_NO_THROW(smirnov::Matrix testMatrix2(std::move(testMatrix)));

  smirnov::Matrix testMatrix2 = smirnov::partition(testComposite);
  smirnov::Matrix testMatrix3;
  BOOST_CHECK_NO_THROW(testMatrix3 = testMatrix2);
  BOOST_CHECK_NO_THROW(testMatrix3 = std::move(testMatrix2));

  smirnov::Matrix testMatrix4 = smirnov::partition(testComposite);
  smirnov::Matrix testMatrix5;
  testMatrix5 = testMatrix4;
  BOOST_CHECK(testMatrix5 == testMatrix4);
  testMatrix5 = std::move(testMatrix4);
  BOOST_CHECK(testMatrix5 == testMatrix3);

  smirnov::Matrix testMatrix6(testMatrix3);
  BOOST_CHECK(testMatrix6 == testMatrix3);
  smirnov::Matrix testMatrix7(std::move(testMatrix3));
  BOOST_CHECK(testMatrix7 == testMatrix6);
}

BOOST_AUTO_TEST_CASE(exceptionThrow)
{
  smirnov::Shape::shape_pointer testCircle = std::make_shared<smirnov::Circle>(3, smirnov::point_t {2.5, 5});
  smirnov::Shape::shape_pointer testRectangle = std::make_shared<smirnov::Rectangle>(2, 4, smirnov::point_t {2, 6});
  smirnov::CompositeShape testComposite;
  testComposite.add(testCircle);
  testComposite.add(testRectangle);

  smirnov::Matrix testMatrix = smirnov::partition(testComposite);
  BOOST_CHECK_THROW(testMatrix[3], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-1], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
