#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testOfCircle)

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_CASE(testOfCircleMoving)
{
  smirnov::Circle circ(5, {0, 0});
  const double area_before_moving = circ.getArea();
  const smirnov::rectangle_t circ_before_moving = circ.getFrameRect();
  circ.move({-4, 7});
  BOOST_CHECK_CLOSE(circ_before_moving.width, circ.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(circ_before_moving.height, circ.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(area_before_moving, circ.getArea(), ACCURACY);
  circ.move(15, -3);
  BOOST_CHECK_CLOSE(circ_before_moving.width, circ.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(circ_before_moving.height, circ.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(area_before_moving, circ.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(testOfCircleScaling)
{
  smirnov::Circle circ(7, {0, 0});
  const double area_before_moving = circ.getArea();
  const double scaling_coef = 0.5;
  circ.scale(scaling_coef);
  BOOST_CHECK_CLOSE(area_before_moving * scaling_coef * scaling_coef, circ.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(testOfInvalidscaling_coefInCircle)
{
smirnov::Circle circ(3, {0, 0});
BOOST_CHECK_THROW(circ.scale(-2), std::invalid_argument);
BOOST_CHECK_THROW(circ.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testOfInvalidParametersOfCircle)
{
  BOOST_CHECK_THROW(smirnov::Circle circ(-5, {0, 0}), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
