#include <algorithm>
#include <iostream>
#include <vector>
#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"

std::vector<std::shared_ptr<Shape>> getShapes()
{
  std::vector<std::shared_ptr<Shape>> vector;
  std::string line;

  while (std::getline(std::cin, line))
  {
    line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
    if (line.empty() || line.size() == 1)
    {
      continue;
    }
    auto openBracketPos = line.find_first_of('(');
    auto semicolonPos = line.find_first_of(';');
    auto closeBracketPos = line.find_first_of(')');
    if ((openBracketPos == std::string::npos) || (semicolonPos == std::string::npos) || (closeBracketPos == std::string::npos))
    {
      throw std::invalid_argument("Incorrect line!");
    }

    std::string shapeName = line.substr(0, openBracketPos);
    int x = std::stoi(line.substr(openBracketPos + 1, semicolonPos - openBracketPos + 1));
    int y = std::stoi(line.substr(semicolonPos + 1, closeBracketPos - semicolonPos + 1));

    if (shapeName == "TRIANGLE")
    {
      vector.push_back(std::make_shared<Triangle>(Triangle(x, y)));
    }
    else if (shapeName == "SQUARE")
    {
      vector.push_back(std::make_shared<Square>(Square(x, y)));
    }
    else if (shapeName == "CIRCLE")
    {
      vector.push_back(std::make_shared<Circle>(Circle(x, y)));
    }
    else
    {
      throw std::invalid_argument("invalid input");
    }

  }
  return vector;
}

void straightPrint(const std::vector<std::shared_ptr<Shape>> & vector)
{
  std::for_each(vector.begin(), vector.end(),[] (const std::shared_ptr<Shape> shape){shape->draw();});
}

void reversePrint(const std::vector<std::shared_ptr<Shape>> & vector)
{
  std::for_each(vector.rbegin(), vector.rend(),[] (const std::shared_ptr<Shape> shape){shape->draw();});
}

void task2()
{
  std::vector <std::shared_ptr<Shape>> vector = getShapes();

  std::cout << "Original:\n";
  straightPrint(vector);

  std::cout << "Left-Right:\n";
  std::sort(vector.begin(), vector.end(), [](const std::shared_ptr<Shape> shape1, const std::shared_ptr<Shape> shape2)
  {
    return shape1->isMoreLeft(*shape2);
  });
  straightPrint(vector);

  std::cout << "Right-Left:\n";
  reversePrint(vector);

  std::cout << "Top-Bottom:\n";
  std::sort(vector.begin(), vector.end(), [](const std::shared_ptr<Shape> shape1, const std::shared_ptr<Shape> shape2)
  {
    return (shape1->isUpper(*shape2));
  });
  straightPrint(vector);

  std::cout << "Bottom-Top:\n";
  reversePrint(vector);
}
