#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>
#include <cmath>

void task1()
{
  std::vector<double> vector (std::istream_iterator<double>(std::cin), std::istream_iterator<double>());

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Reading failed");
  }

  std::transform(vector.begin(), vector.end(), std::ostream_iterator<double>(std::cout, " "), [](double & vector) { return vector * M_PI; });
  std::cout << std::endl;
}
