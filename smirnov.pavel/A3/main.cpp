#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  std::cout << "WORK WITH RECTANGLE\n";
  smirnov::Rectangle rect(3, 6, {5, 7});
  smirnov::Shape *basePointer = &rect;
  std::cout << "Before first moving:\n";
  basePointer->getInfo();
  basePointer->move({ 4, 9 });
  std::cout << "After first moving:\n";
  basePointer->getInfo();
  basePointer->move(6, 1);
  std::cout << "After second moving:\n";
  basePointer->getInfo();
  basePointer->scale(2);
  std::cout << "After scaling:\n";
  basePointer->getInfo();
  std::cout << "\n";

  std::cout << "WORK WITH CIRCLE\n";
  smirnov::Circle circ(4, { 15, 20 });
  basePointer = &circ;
  std::cout << "Before first moving:\n";
  basePointer->getInfo();
  basePointer->move({-5, 10});
  std::cout << "After first moving:\n";
  basePointer->getInfo();
  basePointer->move(5, -10);
  std::cout << "After second moving:\n";
  basePointer->getInfo();
  basePointer->scale(0.5);
  std::cout << "After scaling:\n";
  basePointer->getInfo();
  std::cout << "\n";

  std::cout << "WORK WITH COMPOSITE SHAPE\n";
  smirnov::CompositeShape comp_shape(std::make_shared<smirnov::Rectangle>(rect));
  comp_shape.add(std::make_shared<smirnov::Circle>(circ));
  std::cout << comp_shape.getCount() << " figures in composite shape\n";
  std::cout << "Before first moving:\n";
  comp_shape.getInfo();
  comp_shape.move({12, 6});
  std::cout << "After first moving:\n";
  comp_shape.getInfo();
  comp_shape.move(-6.5, 1);
  std::cout << "After second moving:\n";
  comp_shape.getInfo();
  comp_shape.scale(2);
  std::cout << "After scaling:\n";
  comp_shape.getInfo();
  comp_shape.remove(0);
  std::cout << "After first removing:\n";
  comp_shape.getInfo();
  comp_shape.add(std::make_shared<smirnov::Rectangle>(rect));
  comp_shape.getInfo();
  comp_shape.remove(std::make_shared<smirnov::Circle>(circ));
  std::cout << "After second removing:\n";
  comp_shape.getInfo();
  std::cout << "Operator [] overloading:\n";
  comp_shape[0]->getInfo();

  return 0;
}
