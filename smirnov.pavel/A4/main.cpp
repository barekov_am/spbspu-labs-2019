#include<iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

int main()
{
  std::cout << "WORK WITH RECTANGLE: \n";
  smirnov::Rectangle rect(9, 4, {1.5, 1.5});
  rect.move({4, 8});
  rect.getInfo();
  rect.scale(3);
  rect.getInfo();
  rect.rotate(77);
  rect.getInfo();

  std::cout << "WORK WITH CIRCLE: \n";
  smirnov::Circle circ(5, {3, 7});
  circ.move(-8, 5);
  circ.getInfo();
  circ.scale(0.25);
  circ.getInfo();
  rect.rotate(39);
  circ.getInfo();

  std::cout << "WORK WITH COMPOSITE SHAPE: \n";
  smirnov::Rectangle rect2(11, 3, {6, 6});
  smirnov::Shape::shape_pointer part1 = std::make_shared<smirnov::Rectangle>(rect);
  smirnov::Shape::shape_pointer part2 = std::make_shared<smirnov::Rectangle>(rect2);
  smirnov::Shape::shape_pointer part3 = std::make_shared<smirnov::Circle>(circ);
  smirnov::CompositeShape comp_shape;
  comp_shape.add(part1);
  comp_shape.add(part2);
  comp_shape.add(part3);
  comp_shape.getInfo();
  comp_shape.scale(0.5);
  comp_shape.getInfo();
  comp_shape.rotate(30);
  comp_shape.getInfo();
  comp_shape.move({3, 4});
  comp_shape.getInfo();

  std::cout << "WORK WITH MATRIX: \n";
  smirnov::Matrix matrix = smirnov::partition(comp_shape);
  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    for (size_t j = 0; j < matrix.getColumns(); j++)
    {
      if (matrix[i][j] != nullptr)
      {
        std::cout << "Layer no. " << i << ":\n" << "Figure no. " << j << ":\n";
        std::cout << "Position: (" << matrix[i][j]->getPos().x << "; "
            << matrix[i][j]->getPos().y << ")\n";
      }
    }
  }

  return 0;
}
