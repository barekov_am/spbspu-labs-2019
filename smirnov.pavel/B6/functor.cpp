#include <iostream>
#include <limits>

#include "functor.hpp"

Functor::Functor()
{
  firstElem_ = 0;
  max_ = std::numeric_limits<int>::min();
  min_ = std::numeric_limits<int>::max();
  quantityPositive_ = 0;
  quantityNegative_ = 0;
  oddSum_ = 0;
  evenSum_ = 0;
  equal_ = false;
  quantityOfElem_ = 0;
}

void Functor::operator()(const long long int & elem)
{
  if (quantityOfElem_ == 0)
  {
    firstElem_ = elem;
  }

  if (elem > max_)
  {
    max_ = elem;
  }

  if (elem < min_)
  {
    min_ = elem;
  }

  if (elem > 0)
  {
    quantityPositive_++;
  }

  if (elem < 0)
  {
    quantityNegative_++;
  }

  if (elem % 2 != 0)
  {
    oddSum_ += elem;
  }
  else
  {
    evenSum_ += elem;
  }

  if (firstElem_ == elem)
  {
    equal_ = true;
  }
  else
  {
    equal_ = false;
  }

  quantityOfElem_++;
}

void Functor::printInfo()
{
  if (quantityOfElem_ == 0)
  {
    std::cout << "No Data" << "\n";
    return;
  }

  std::cout << "Max: " << max_ << "\n";
  std::cout << "Min: " << min_ << "\n";
  std::cout << "Mean: " << (static_cast<double>(evenSum_) + static_cast<double>(oddSum_))
    / static_cast<double>(quantityOfElem_) << "\n";
  std::cout << "Positive: " << quantityPositive_ << "\n";
  std::cout << "Negative: " << quantityNegative_ << "\n";
  std::cout << "Odd Sum: " << oddSum_ << "\n";
  std::cout << "Even Sum: " << evenSum_ << "\n";

  if (equal_ == true)
  {
    std::cout << "First/Last Equal: " << "yes" << "\n";
  }
  else
  {
    std::cout << "First/Last Equal: " << "no" << "\n";
  }
}
