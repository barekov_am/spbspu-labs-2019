#include <algorithm>
#include <iterator>
#include <functional>
#include <cstdio>

#include "functor.hpp"

int main()
{
  try
  {

  Functor functor;

  std::for_each(std::istream_iterator<long long int>(std::cin), std::istream_iterator<long long int>(), std::ref(functor));

  if ((!std::cin.eof()) && (std::cin.fail()))
  {
    throw std::runtime_error("Can`t read data!");
  }

  functor.printInfo();
  }

  catch (std::exception & exc)
  {
    std::cerr << exc.what() << "\n";
    return 2;
  }

  return 0;
}
