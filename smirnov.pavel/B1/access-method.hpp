#ifndef ACCESS_METHOD_HPP
#define ACCESS_METHOD_HPP

#include <iostream>
#include <iterator>

template <typename Container>
struct BracketAccess
{
  static std::size_t begin(const Container &)
  {
    return 0;
  }

  static std::size_t end(const Container & cont)
  {
    return cont.size();
  }

  static std::size_t next(std::size_t index)
  {
    return index++;
  }

  static typename Container::reference getElement(Container & cont, std::size_t index)
  {
    return cont[index];
  }
};

template <typename Container>
struct AtAccess
{
  static std::size_t begin(const Container &)
  {
    return 0;
  }

  static std::size_t end(const Container & cont)
  {
    return cont.size();
  }

  static std::size_t next(std::size_t index)
  {
    return index++;
  }

  static typename Container::reference getElement(Container & cont, std::size_t index)
  {
    return cont.at(index);
  }
};

template <typename Container>
struct IteratorAccess
{
  static typename Container::iterator begin(Container & cont)
  {
    return cont.begin();
  }

  static typename Container::iterator end(Container & cont)
  {
    return cont.end();
  }

  static typename Container::iterator next(typename Container::iterator iter)
  {
    return ++iter;
  }

  static typename Container::reference getElement(Container &, typename Container::iterator iter)
  {
    return *iter;
  }
};

#endif
