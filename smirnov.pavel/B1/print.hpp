#ifndef PRINT_HPP
#define PRINT_HPP

#include <iostream>

template <typename Container>
void print(const Container & cont)
{
  for (const auto & item : cont)
  {
    std::cout << item << " ";
  }
  std::cout << "\n";
}

#endif
