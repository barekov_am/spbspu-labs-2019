#include "sort.hpp"

#include <cstring>
#include <stdexcept>

bool getDirection(const char * word)
{
  if (std::strcmp(word, "ascending") == 0)
  {
    return true;
  }
  if (std::strcmp(word, "descending") == 0)
  {
    return false;
  }
  throw std::invalid_argument("Invalid direction. Enter descending or ascending.");
}
