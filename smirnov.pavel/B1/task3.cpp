#include <vector>

#include "print.hpp"

void task3()
{
  std::vector<int> vect;
  int elem = 0;
  while (std::cin >> elem)
  {
    if (elem == 0)
    {
      break;
    }
    vect.push_back(elem);
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::runtime_error("Incorrect input data");
  }

  if (vect.empty())
  {
    return;
  }

  if (elem != 0)
  {
    throw std::runtime_error("It must be 0");
  }

  std::vector<int>::iterator it;
  int key = vect.back();
  switch (key)
  {
  case 1:
    for (it = vect.begin(); it != vect.end();)
    {
      if (*it % 2 == 0)
      {
        it = vect.erase(it);
      }
      else
      {
        ++it;
      }
    }
    break;

  case 2:
    for (it = vect.begin(); it != vect.end();)
    {
      if (*it % 3 == 0)
      {
        it = vect.insert(++it, 3, 1);
      }
      else
      {
        ++it;
      }
    }
    break;
  }
  print(vect);
}
