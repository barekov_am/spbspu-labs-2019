﻿#include <iostream>
#include <cstdlib>
#include <exception>

void task1(const char * word);
void task2(const char * filename);
void task3();
void task4(const char * word, int size);

int main(int argc, char * argv[])
{
  try
  {
    if (argc <= 1)
    {
      std::cerr << "Invalid number of arguments";
      return 1;
    }

    char * tail = nullptr;
    int task = std::strtol(argv[1], &tail, 10);

    if (*tail)
    {
      std::cerr << "Invalid task";
      return 1;
    }


    switch (task)
    {
    case 1:
      if (argc != 3)
      {
        std::cerr << "Incorrect number of parameters";
        return 1;
      }
      task1(argv[2]);
      break;

    case 2:
      if (argc != 3)
      {
        std::cerr << "Incorrect number of parameters";
        return 1;
      }
      task2(argv[2]);
      break;

    case 3:
      if (argc != 2)
      {
        std::cerr << "Incorrect number of parameters\n";
        return 1;
      }
      task3();
      break;

    case 4:
    {
      if (argc != 4)
      {
        std::cerr << "Incorrect number of parameters\n";
        return 1;
      }
      std::size_t vectSize = std::strtol(argv[3], &tail, 10);
      if (*tail)
      {
        std::cerr << "Incorrect vector size.\n";
        return 1;
      }
      task4(argv[2], vectSize);
      break;
    }

    default:
      std::cerr << "Incorrect argument.\n";
      return 1;
    }
  }

  catch (const std::exception & exc)
  {
    std::cerr << exc.what();
    return 1;
  }
  return 0;
}
