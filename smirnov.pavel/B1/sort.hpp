#ifndef SORT_HPP
#define SORT_HPP

#include "access-method.hpp"

bool getDirection(const char * word);

template <template <typename> typename AccessMethod, typename Container>
void sort(Container & cont, const bool Direction)
{
  auto begin = AccessMethod<Container>::begin(cont);
  auto end = AccessMethod<Container>::end(cont);
  for (auto i = begin; i != end; i++)
  {
    auto tmp = i;
    for (auto j = AccessMethod<Container>::next(i); j != end; j++)
    {
      bool less = (AccessMethod<Container>::getElement(cont, j) < AccessMethod<Container>::getElement(cont, tmp));
      if ((Direction && less) || (!Direction && !less))
      {
        tmp = j;
      }
    }

    if (tmp != i)
    {
      std::swap(AccessMethod<Container>::getElement(cont, tmp), AccessMethod<Container>::getElement(cont, i));
    }
  }
}

#endif
