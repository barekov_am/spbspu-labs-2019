#include <fstream>
#include <vector>
#include <memory>
#include <iostream>

const std::size_t MEMORY_SIZE = 1024;

void task2(const char * filename)
{
  std::ifstream file(filename);
  if (!file.is_open())
  {
    throw std::runtime_error("File reading error");
  }

  std::size_t size = MEMORY_SIZE;
  std::unique_ptr<char [], decltype(&free)> arr(static_cast<char *>(malloc(size)), &free);

  std::size_t index = 0;
  while (file)
  {
    file.read(&arr[index], MEMORY_SIZE);
    index += file.gcount();
    if (file.gcount() == MEMORY_SIZE)
    {
      size += MEMORY_SIZE;
      std::unique_ptr<char[], decltype(&free)> tmpArr(static_cast<char *>(realloc(arr.get(), size)), &free);
      if (!tmpArr)
      {
        throw std::runtime_error("Could not allocate new array");
      }
      arr.release();
      std::swap(arr, tmpArr);
    }
    if (!file.eof() && file.fail())
    {
      throw std::runtime_error("Input error");
    }
  }

  std::vector<char> vect(&arr[0], &arr[index]);
  for (const auto & item : vect)
  {
    std::cout << item;
  }
}
