#include <vector>
#include <cstdlib>
#include <random>

#include "sort.hpp"
#include "print.hpp"

void fillRandom(double * array, int size)
{
  for (int i = 0; i != size; i++)
  {
    array[i] = (rand() % 200 - 100) / 100.0;
  }
}

void task4(const char * word, int size)
{
  if (size <= 0)
  {
    std::invalid_argument("Incorrect parameter");
  }

  std::vector<double> vect(size);
  fillRandom(&vect[0], size);
  bool direction = getDirection(word);
  print(vect);
  sort<BracketAccess>(vect, direction);
  print(vect);
}
