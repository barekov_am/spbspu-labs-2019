#include <vector>
#include <forward_list>

#include "sort.hpp"
#include "print.hpp"

void task1(const char * word)
{
  bool direction = getDirection(word);
  std::vector<int> firstVect;
  int elem = 0;
  while (!(std::cin >> elem).eof())
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Incorrect input information");
    }
    firstVect.push_back(elem);
  }

  std::vector<int> secondVect = firstVect;
  std::forward_list<int> list(firstVect.begin(), firstVect.end());

  sort<BracketAccess>(firstVect, direction);
  sort<AtAccess>(secondVect, direction);
  sort<IteratorAccess>(list, direction);

  print(firstVect);
  print(secondVect);
  print(list);
}
