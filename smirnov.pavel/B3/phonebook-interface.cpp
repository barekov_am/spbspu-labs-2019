#include "phonebook-interface.hpp"
#include <iostream>

PhoneBookInterface::PhoneBookInterface()
{
  bookmark_["current"] = phonebook_.begin();
}

void PhoneBookInterface::add(const PhoneBook::record_t & rec)
{
  phonebook_.pushBack(rec);
  if (std::next(phonebook_.begin()) == phonebook_.end())
  {
    bookmark_["current"] = phonebook_.begin();
  }
}

void PhoneBookInterface::store(std::string & markname, std::string & new_markname)
{
  auto iter = getIterator(markname);
  if (iter == bookmark_.end())
  {
    return;
  }
  bookmark_.emplace(new_markname, iter->second);
}

void PhoneBookInterface::insert(insert_direction & direction, PhoneBook::record_t & rec, std::string & markname)
{
  auto iter = getIterator(markname);

  if (iter->second == phonebook_.end())
  {
    add(rec);
  }

  if (direction == insert_direction::before)
  {
    phonebook_.insert(iter->second, rec);
  }
  else
  {
    phonebook_.insert(std::next(iter->second), rec);
  }

}

void PhoneBookInterface::deleteRecord(std::string & markname)
{
  auto iter = getIterator(markname);

  if (iter == bookmark_.end())
  {
    return;
  }

  auto old_iter = iter->second;
  for (auto it = bookmark_.begin(); it != bookmark_.end(); ++it)
  {
    if (it->second == old_iter)
    {
      if (std::next(it->second) == phonebook_.end())
      {
        it->second = phonebook_.prev(old_iter);
      }
      else
      {
        it->second = phonebook_.next(old_iter);
      }
    }
  }
  phonebook_.remove(old_iter);
}

void PhoneBookInterface::show(std::string & markname)
{
  auto iter = getIterator(markname);

  if (iter == bookmark_.end())
  {
    return;
  }

  if (phonebook_.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  phonebook_.show(iter->second);
}

void PhoneBookInterface::move(std::string & markname, int steps)
{
  auto iter = getIterator(markname);

  if (iter == bookmark_.end())
  {
    return;
  }

  iter->second = phonebook_.move(iter->second, steps);
}

void PhoneBookInterface::move(std::string & markname, move_direction & direction)
{
  auto iter = getIterator(markname);

  if (iter == bookmark_.end())
  {
    return;
  }

  if (direction == move_direction::first)
  {
    iter->second = phonebook_.begin();
  }
  else
  {
    iter->second = --phonebook_.end();
  }
}

PhoneBookInterface::bookmark::iterator PhoneBookInterface::getIterator(std::string & markname)
{
  auto iter = bookmark_.find(markname);
  if (iter == bookmark_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return bookmark_.end();
  }
  return iter;
}
