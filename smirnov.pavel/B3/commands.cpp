#include "commands.hpp"
#include "functions.hpp"
#include "phonebook-interface.hpp"

#include <iostream>
#include <string>

void add(PhoneBookInterface & phonebook, std::stringstream & stream)
{
  std::string number;
  stream >> std::ws >> number >> std::ws;
  std::string name;
  getline(stream, name);

  number = checkNumber(number);
  name = checkName(name);

  if (number.empty() || name.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  phonebook.add({name, number});
}

void store(PhoneBookInterface & phonebook, std::stringstream & stream)
{
  std::string markname;
  stream >> std::ws >> markname >> std::ws;
  std::string new_markname;
  getline(stream, new_markname);

  markname = checkBookmark(markname);
  new_markname = checkBookmark(new_markname);

  if (markname.empty() || new_markname.empty())
  {
    std::cout << "<INVALID COMMAND\n>";
    return;
  }
  phonebook.store(markname, new_markname);
}

void insert(PhoneBookInterface & phonebook, std::stringstream & stream)
{

  std::string direction, markname, number;
  stream >> std::ws >> direction >> markname >> number >> std::ws;
  std::string name;
  getline(stream, name);

  markname = checkBookmark(markname);
  number = checkNumber(number);
  name = checkName(name);

  if (markname.empty() || number.empty() || name.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  PhoneBook::record_t rec = {name, number};
  PhoneBookInterface::insert_direction dir;
  if (direction == "before")
  {
    dir = PhoneBookInterface::insert_direction::before;
  }
  else if (direction == "after")
  {
    dir = PhoneBookInterface::insert_direction::after;
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  phonebook.insert(dir, rec, markname);
}

void deleteRec(PhoneBookInterface & phonebook, std::stringstream & stream)
{
  std::string markname;
  stream >> std::ws >> markname;

  markname = checkBookmark(markname);
  if (markname.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  phonebook.deleteRecord(markname);
}

void show(PhoneBookInterface & phonebook, std::stringstream & stream)
{
  std::string markname;
  stream >> std::ws >> markname;

  markname = checkBookmark(markname);
  if (markname.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  phonebook.show(markname);
}

void move(PhoneBookInterface & phonebook, std::stringstream & stream)
{
  std::string markname;
  stream >> std::ws >> markname >> std::ws;
  std::string steps;
  getline(stream, steps);

  markname = checkBookmark(markname);
  if (markname.empty() || steps.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (steps == "first")
  {
    PhoneBookInterface::move_direction step = PhoneBookInterface::move_direction::first;
    phonebook.move(markname, step);
  }
  else if (steps == "last")
  {
    PhoneBookInterface::move_direction step = PhoneBookInterface::move_direction::last;
    phonebook.move(markname, step);
  }

  else if ((steps[0] == '-') || (steps[0] == '+') || (isdigit(steps[0])))
  {
    for (size_t i = 1; i < steps.size(); i++)
    {
      if (!isdigit(steps[i]))
      {
        std::cout << "<INVALID STEP>\n";
        return;
      }
    }
    int step = std::stoi(steps);
    phonebook.move(markname, step);
  }
  else
  {
    std::cout << "<INVALID STEP>\n";
    return;
  }
}
