#ifndef PHONEBOOK_INTERFACE
#define PHONEBOOK_INTERFACE

#include <map>
#include <string>
#include "phonebook.hpp"

class PhoneBookInterface
{
public:
  enum class insert_direction
  {
    before,
    after
  };

  enum class move_direction
  {
    first,
    last
  };

  PhoneBookInterface();
  void add(const PhoneBook::record_t & rec);
  void store(std::string & markname, std::string & new_markname);
  void insert(insert_direction & direction, PhoneBook::record_t & rec, std::string & markname);
  void deleteRecord(std::string & markname);
  void show(std::string & markname);
  void move(std::string & markname, int steps);
  void move(std::string & markname, move_direction & direction);

private:
  using bookmark = std::map<std::string, PhoneBook::iterator>;

  bookmark bookmark_;
  PhoneBook phonebook_;
  bookmark::iterator getIterator(std::string & markname);
};

#endif
