#include "commands.hpp"
#include <iostream>
#include <sstream>

void task1()
{
  PhoneBookInterface phonebook;
  std::string line;

  while (std::getline(std::cin, line))
  {
    std::stringstream stream(line);
    std::string command;
    stream >> command;

    if (std::cin.fail())
    {
      throw std::runtime_error("Incorrect input information\n");
    }

    if (command == "add")
    {
      add(phonebook, stream);
    }
    else if (command == "store")
    {
      store(phonebook, stream);
    }
    else if (command == "insert")
    {
      insert(phonebook, stream);
    }
    else if (command == "delete")
    {
      deleteRec(phonebook, stream);
    }
    else if (command == "show")
    {
      show(phonebook, stream);
    }
    else if (command == "move")
    {
      move(phonebook, stream);
    }
    else
    {
      std::cout << "<INVALID COMMAND>";
    }
  }
}
