#include <iostream>
#include <algorithm>

#include "factorial.hpp"

void task2()
{
  FactorialContainer container;

  std::copy(container.begin(), container.end(), std::ostream_iterator<unsigned long long>(std::cout, " "));
  std::cout << "\n";

  std::reverse_copy(container.begin(), container.end(), std::ostream_iterator<unsigned long long>(std::cout, " "));
  std::cout << "\n";
}
