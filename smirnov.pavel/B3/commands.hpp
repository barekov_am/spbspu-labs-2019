#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <sstream>
#include "phonebook-interface.hpp"

void add(PhoneBookInterface & phonebook, std::stringstream & stream);
void store(PhoneBookInterface & phonebook, std::stringstream & stream);
void insert(PhoneBookInterface & phonebook, std::stringstream & stream);
void deleteRec(PhoneBookInterface & phonebook, std::stringstream & stream);
void show(PhoneBookInterface & phonebook, std::stringstream & stream);
void move(PhoneBookInterface & phonebook, std::stringstream & stream);

#endif
