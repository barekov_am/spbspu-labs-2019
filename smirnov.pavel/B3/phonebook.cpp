#include "phonebook.hpp"
#include <iostream>

void PhoneBook::show(iterator iter) const
{
  std::cout << iter->phone << ' ' << iter->name << '\n';
}

void PhoneBook::insert(iterator iter, const record_t & rec)
{
  book_.insert(iter, rec);
}

void PhoneBook::replace(iterator iter, const record_t & rec)
{
  *iter = rec;
}

void PhoneBook::pushBack(const record_t & rec)
{
  book_.push_back(rec);
}

PhoneBook::iterator PhoneBook::begin()
{
  return book_.begin();
}

PhoneBook::iterator PhoneBook::end()
{
  return book_.end();
}

PhoneBook::iterator PhoneBook::next(iterator iter)
{
  if (iter != book_.end())
  {
    return ++iter;
  }
  else
  {
    return iter;
  }
}

PhoneBook::iterator PhoneBook::prev(iterator iter)
{
  if (iter != book_.begin())
  {
    return --iter;
  }
  else
  {
    return iter;
  }
}

PhoneBook::iterator PhoneBook::move(iterator iter, int n)
{
  if (n >= 0)
  {
    for (int i = 0; i < n; ++i)
    {
      if (iter++ == book_.end())
      {
        throw std::out_of_range("Too big n");
      }
    }
  }

  if (n < 0)
  {
    for (int i = 0; n < i; --i)
    {
      if (iter-- == book_.begin())
      {
        throw std::out_of_range("Too small n");
      }
    }
  }
  return iter;
}

void PhoneBook::remove(iterator iter)
{
  book_.erase(iter);
}

bool PhoneBook::empty() const
{
  return book_.empty();
}
