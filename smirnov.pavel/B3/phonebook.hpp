#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <string>
#include <list>

class PhoneBook
{
public:
  struct record_t
  {
    std::string name, phone;
  };

  using iterator = std::list<record_t>::iterator;

  void show(iterator iter) const;
  void insert(iterator iter, const record_t & rec);
  void replace(iterator iter, const record_t & rec);
  void pushBack(const record_t & rec);

  iterator begin();
  iterator end();
  iterator next(iterator iter);
  iterator prev(iterator iter);
  iterator move(iterator iter, int n);
  void remove(iterator iter);

  bool empty() const;

private:
  std::list<record_t> book_;
};

#endif
