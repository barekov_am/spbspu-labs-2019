#include <stdexcept>
#include "factorial.hpp"

const int MIN = 1;
const int MAX = 11;

FactorialIterator::FactorialIterator():
  value_(1),
  argument_(1)
{}

FactorialIterator::FactorialIterator(int arg)
{
  if (arg < MIN || arg > MAX)
  {
    throw std::invalid_argument("Argument is out of range");
  }
  value_ = getData(arg);
  argument_ = arg;
}

FactorialIterator::reference FactorialIterator::operator *()
{
  return value_;
}

FactorialIterator::pointer FactorialIterator::operator ->()
{
  return & value_;
}

FactorialIterator & FactorialIterator::operator ++()
{
  if (argument_ > MAX)
  {
    throw std::out_of_range("Argument is out of range");
  }
  ++argument_;
  value_ *= argument_;
  return *this;
}

FactorialIterator FactorialIterator::operator ++(int)
{
  FactorialIterator tmp = *this;
  ++(*this);
  return tmp;
}

FactorialIterator & FactorialIterator::operator --()
{
  if (argument_ <= MIN)
  {
    throw std::out_of_range("Index is out of range");
  }
  value_ /= argument_;
  --argument_;
  return *this;
}

FactorialIterator FactorialIterator::operator --(int)
{
  FactorialIterator tmp = *this;
  --(*this);
  return tmp;
}

bool FactorialIterator::operator ==(FactorialIterator iter) const
{
  return (value_ == iter.value_) && (argument_ == iter.argument_);
}

bool FactorialIterator::operator !=(FactorialIterator iter) const
{
  return (value_ != iter.value_) || (argument_ != iter.argument_);
}

unsigned long long FactorialIterator::getData(int index) const
{
  return (index <= MIN) ? 1 : (index * getData(index - 1));
}

FactorialIterator FactorialContainer::begin()
{
  return FactorialIterator(MIN);
}

FactorialIterator FactorialContainer::end()
{
  return FactorialIterator(MAX);
}
