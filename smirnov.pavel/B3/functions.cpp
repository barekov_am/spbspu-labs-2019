#include "functions.hpp"

std::string checkNumber(std::string & number)
{
  if (number.empty())
  {
    return "";
  }

  for (size_t i = 0; i < number.size(); ++i)
  {
    if (!isdigit(number[i]))
    {
      return "";
    }
  }

  return number;
}

std::string checkName(std::string & name)
{
  if (name.empty())
  {
    return "";
  }

  if ((name.front() != '\"') || (name.back() != '\"'))
  {
    return "";
  }

  name.erase(name.begin());
  name.erase(name.end() - 1);

  for (size_t i = 0; i < name.size(); ++i)
  {
    if (name[i] == '\\')
    {
      if ((name[i + 1] == '\\') || (name[i + 1] == '\"'))
      {
        name.erase(i, 1);
      }
      else
      {
        return "";
      }
    }
  }

  return name;
}

std::string checkBookmark(std::string & bookmark)
{
  if (bookmark.empty())
  {
    return "";
  }

  size_t i = bookmark[0];
  while (i < bookmark.size())
  {
    if (!isalnum(bookmark[i]) && (bookmark[i] != '-'))
    {
      return "";
    }
    i++;
  }

  return bookmark;
}
