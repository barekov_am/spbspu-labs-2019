#ifndef FACTORIAL_HPP
#define FACTORIAL_HPP

#include <iterator>

class FactorialIterator: public std::iterator<std::bidirectional_iterator_tag, unsigned long long>
{
public:
  FactorialIterator();
  FactorialIterator(int arg);

  reference operator *();
  pointer operator ->();
  FactorialIterator &operator ++();
  FactorialIterator operator ++(int);
  FactorialIterator &operator --();
  FactorialIterator operator --(int);
  bool operator ==(FactorialIterator iter) const;
  bool operator !=(FactorialIterator iter) const;

private:
  unsigned long long value_;
  int argument_;
  unsigned long long getData(int index) const;
};

class FactorialContainer
{
public:
  FactorialContainer() = default;
  FactorialIterator begin();
  FactorialIterator end();
};


#endif
