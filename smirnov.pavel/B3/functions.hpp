#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <string>

std::string checkNumber(std::string & number);
std::string checkName(std::string & name);
std::string checkBookmark(std::string & bookmark);

#endif
