#ifndef QUEUE_CPP
#define QUEUE_CPP

#include <list>
#include <stdexcept>

enum class ElementPriority
{
  LOW,
  NORMAL,
  HIGH
};

template <typename T>
class QueueWithPriority
{
public:
  void putElement(const T & element, ElementPriority priority);
  T getElement();
  void accelerate();
  bool empty();

private:
  std::list<T> low_;
  std::list<T> normal_;
  std::list<T> high_;
};

template <typename T>
void QueueWithPriority<T>::putElement(const T & element, ElementPriority priority)
{
  if (priority == ElementPriority::LOW)
  {
    low_.push_back(element);
  }

  if (priority == ElementPriority::NORMAL)
  {
    normal_.push_back(element);
  }

  if (priority == ElementPriority::HIGH)
  {
    high_.push_back(element);
  }
}


template <typename T>
T QueueWithPriority<T>::getElement()
{
  if (!high_.empty())
  {
    T elem = high_.front();
    high_.pop_front();
    return elem;
  }

  if (!normal_.empty())
  {
    T elem = normal_.front();
    normal_.pop_front();
    return elem;
  }

  if (!low_.empty())
  {
    T elem = low_.front();
    low_.pop_front();
    return elem;
  }
  else
  {
    throw std::runtime_error("Incorrect input information\n");
  }
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  if (!low_.empty())
  {
    high_.splice(high_.end(), low_);
  }
}

template <typename T>
bool QueueWithPriority<T>::empty()
{
  if (low_.empty() && normal_.empty() && high_.empty())
  {
    return true;
  }
  else
  {
    return false;
  }
}

#endif
