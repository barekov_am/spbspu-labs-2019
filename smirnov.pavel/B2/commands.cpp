#include "commands.hpp"
#include <iostream>
#include <string>


void add(QueueWithPriority<std::string> & queue, std::stringstream & stream)
{
  std::string priority;
  stream >> priority >> std::ws;
  std::string data;
  getline(stream, data);

  if ((priority.empty()) || (data.empty()))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  ElementPriority ElemPrior;
  if (priority == "low")
  {
    ElemPrior = ElementPriority::LOW;
  }
  else if (priority == "normal")
  {
    ElemPrior = ElementPriority::NORMAL;
  }
  else if (priority == "high")
  {
    ElemPrior = ElementPriority::HIGH;
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  
  queue.putElement(data, ElemPrior);
}

void get(QueueWithPriority<std::string> & queue, std::stringstream & stream)
{
  if (queue.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  std::string data;
  stream >> data;
  if (!data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  std::cout << queue.getElement() << "\n";
}

void accelerate(QueueWithPriority<std::string> & queue, std::stringstream & stream)
{
  std::string data;
  stream >> data;
  if (!data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  queue.accelerate();
}
