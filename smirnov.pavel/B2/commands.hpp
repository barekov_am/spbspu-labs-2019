#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <sstream>
#include "queue.hpp"

void add(QueueWithPriority<std::string> & queue, std::stringstream & stream);
void get(QueueWithPriority<std::string> & queue, std::stringstream & stream);
void accelerate(QueueWithPriority<std::string> & queue, std::stringstream & stream);

#endif
