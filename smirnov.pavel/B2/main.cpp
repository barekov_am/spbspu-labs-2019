﻿#include <iostream>
#include <exception>

void task1();
void task2();

int main(int argc, char * argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Invalid number of arguments\n";
      return 1;
    }

    char * tail = nullptr;
    int task = std::strtol(argv[1], &tail, 10);
    if (*tail)
    {
      std::cerr << "Invalid task\n";
      return 1;
    }

    switch (task)
    {
    case 1:
      task1();
      break;

    case 2:
      task2();
      break;

    default:
      std::cerr << "Incorrect argument.\n";
      return 1;
    }
  }
    catch (const std::exception & exc)
    {
      std::cerr << exc.what();
      return 1;
    }
}
