#include "commands.hpp"
#include <iostream>
#include <sstream>

void task1()
{
  QueueWithPriority<std::string> queue;
  std::string line;

  while (std::getline(std::cin, line))
  {
    std::stringstream stream(line);
    std::string command;
    stream >> command;

    if (std::cin.fail())
    {
      throw std::runtime_error("Incorrect input information\n");
    }

    if (command == "add")
    {
      add(queue, stream);
    }
    else if (command == "get")
    {
      get(queue, stream);
    }
    else if (command == "accelerate")
    {
      accelerate(queue, stream);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
