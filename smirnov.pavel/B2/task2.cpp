#include <iostream>
#include <list>

void task2()
{
  const int MAX_SIZE = 20;
  const int MIN = 1;
  const int MAX = 20;
  std::list<int> myList;
  int elem = 0;

  while (std::cin >> elem)
  {
    if ((elem >= MIN) && (elem <= MAX))
    {
      myList.push_back(elem);
    }
    else
    {
      throw std::invalid_argument("Number must be from 1 to 20\n");
    }

    if (myList.size() > MAX_SIZE)
    {
      throw std::invalid_argument("List mustn't be more 20\n");
    }
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::invalid_argument("Incorrect input information\n");
  }

  while (!myList.empty())
  {
    std::cout << myList.front() << " ";
    myList.pop_front();
    if (!myList.empty())
    {
      std::cout << myList.back() << " ";
      myList.pop_back();
    }
    else
    {
      break;
    }
  }
  std::cout << "\n";
}
