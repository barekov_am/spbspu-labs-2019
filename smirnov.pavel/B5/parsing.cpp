#include <string>
#include <iostream>
#include <stdexcept>

#include "shape.hpp"

std::vector<Shape> readShapes(std::istream & istream)
{
  std::vector<Shape> shapes;
  std::string line;

  while (std::getline(istream >> std::ws, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input fail\n");
    }

    if (line.empty())
    {
      continue;
    }

    auto openingBracket = line.find_first_of('(');
    if (openingBracket == std::string::npos)
    {
      throw std::invalid_argument("Invalid separator\n");
    }

    int points = std::stoi(line.substr(0, openingBracket));
    line.erase(0, openingBracket);
    if (points < 1)
    {
      throw std::invalid_argument("Invalid number of vertices\n");
    }

    Shape shape;
    std::size_t separatorPos;
    std::size_t closingBracket;

    if (line.empty())
    {
      throw std::invalid_argument("No arguments\n");
    }

    for (int i = 0; i < points; i++)
    {
      openingBracket = line.find_first_of('(');
      separatorPos = line.find_first_of(';');
      closingBracket = line.find_first_of(')');

      if ((openingBracket == std::string::npos) || (separatorPos == std::string::npos)
          || (closingBracket == std::string::npos))
      {
        throw std::invalid_argument("Invalid format of entering vertices\n");
      }

      int x = std::stoi(line.substr(openingBracket + 1, separatorPos - openingBracket - 1));
      int y = std::stoi(line.substr(separatorPos + 1, closingBracket - separatorPos - 1));
      line.erase(0, closingBracket + 1);
      shape.push_back({x, y});
    }

    line.erase(line.find_last_not_of(" \t") + 1);
    if (!line.empty())
    {
      throw std::invalid_argument("Excess argumets\n");
    }

    shapes.push_back(shape);
  }
  return shapes;
}
