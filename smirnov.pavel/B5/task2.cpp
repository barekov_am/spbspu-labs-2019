#include <iostream>
#include <vector>
#include <algorithm>
#include "functions.hpp"

std::vector<Shape> readShapes(std::istream & istream);
void task2()
{
  std::vector<Shape> shapes = readShapes(std::cin);

  std::size_t vertices = 0;
  std::size_t triangles = 0;
  std::size_t squares = 0;
  std::size_t rectangles = 0;

  std::for_each(shapes.begin(), shapes.end(), [&](const Shape & shape)
  {
    vertices += shape.size();
    if (shape.size() == VERTICES_OF_TRIANGLE)
    {
      ++triangles;
    }
    else if (shape.size() == VERTICES_OF_RECTANGLE)
    {
      if (isRectangle(shape))
      {
        ++rectangles;
        if (isSquare(shape))
        {
          ++squares;
        }
      }
    }
  });
  std::cout << "Vertices: " << vertices << "\n"
      << "Triangles: " << triangles << "\n"
      << "Squares: " << squares << "\n"
      << "Rectangles: " << rectangles << "\n";

  removePentagons(shapes);

  Shape points(shapes.size());
  std::transform(shapes.begin(), shapes.end(), points.begin(), [&](const Shape & shape) { return shape[0]; });

  std::sort(shapes.begin(), shapes.end(), [&](const Shape & shape1, const Shape & shape2)
  {
    if (shape1.size() < shape2.size())
    {
      return true;
    }
    if ((shape1.size() == VERTICES_OF_RECTANGLE) && (shape2.size() == VERTICES_OF_RECTANGLE))
    {
      if (isSquare(shape1))
      {
        return !isSquare(shape2);
      }
    }
    return false;
  });


  std::cout << "Points: ";
  for (const auto & point : points)
  {
    std::cout << '(' << point.x << "; " << point.y << ") ";
  }

  std::cout << "\nShapes: \n";
  for (const auto & shape : shapes)
  {
    std::cout << shape.size() << " ";
    for (const auto & vertex : shape)
    {
      std::cout << "(" << vertex.x << "; " << vertex.y << ") ";
    }
    std::cout << '\n';
  }
}
