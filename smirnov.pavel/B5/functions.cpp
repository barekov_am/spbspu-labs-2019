#include <algorithm>
#include <iostream>
#include "functions.hpp"

int getLength(const Point_t & lhs, const Point_t & rhs)
{
  return (lhs.x - rhs.x) * (lhs.x - rhs.x) + (lhs.y - rhs.y) * (lhs.y - rhs.y);
}

bool isRectangle(const Shape & shape)
{
  return ((shape.size() == VERTICES_OF_RECTANGLE) && (getLength(shape[0], shape[2]) == getLength(shape[1], shape[3])));
}

bool isSquare(const Shape & shape)
{
  return isRectangle(shape) && getLength(shape[0], shape[1]) == getLength(shape[1], shape[2]);
}

void removePentagons(std::vector<Shape> & shapes)
{
  shapes.erase(std::remove_if(shapes.begin(), shapes.end(),
      [&](const Shape& shape) { return shape.size() == VERTICES_OF_PENTAGON; }), shapes.end());
}
