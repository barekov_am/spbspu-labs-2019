#include <iostream>
#include "task.hpp"

int main()
{
  try
  {
    task();
  }
  catch (const std::exception & exc)
  {
    std::cerr << exc.what() << '\n';
    return 2;
  }

  return 0;
}
