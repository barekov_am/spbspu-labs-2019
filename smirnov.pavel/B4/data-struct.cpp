#include <sstream>
#include <vector>
#include <iostream>
#include "data-struct.hpp"

const int MIN_KEY = -5;
const int MAX_KEY = 5;

int getKey(std::stringstream & line)
{
  int key = MAX_KEY + 1;
  line >> key;
  if ((key > MAX_KEY) || (key < MIN_KEY))
  {
    throw std::invalid_argument("Incorrect argument");
  }

  std::string string;
  std::getline(line, string, ',');
  if (!string.empty() || line.eof())
  {
    throw std::invalid_argument("Incorrect argument");
  }

  return key;
}

void getData(std::vector<DataStruct> & vector)
{
  int key1;
  int key2;
  std::string str;

  std::string line;
  std::getline(std::cin, line);
  std::stringstream stream(line);

  key1 = getKey(stream);
  key2 = getKey(stream);
  stream >> std::ws;
  std::getline(stream, str);

  if (str.empty())
  {
    throw std::invalid_argument("Wrong argument");
  }

  vector.push_back({ key1, key2, str });
}

bool compare(const DataStruct & first, const DataStruct & second)
{
  if (first.key1 < second.key1)
  {
    return true;
  }

  if (first.key1 == second.key1)
  {
    if (first.key2 < second.key2)
    {
      return true;
    }
    else if ((first.key2 == second.key2) && (first.str.size() < second.str.size()))
    {
      return true;
    }
  }
  return false;
}

void print(std::vector<DataStruct> & vector)
{
  for (auto elem : vector)
  {
    std::cout << elem.key1 << ", " << elem.key2 << ", " << elem.str << '\n';
  }
}
