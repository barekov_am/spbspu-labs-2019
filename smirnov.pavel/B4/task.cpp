#include "task.hpp"

void task()
{
  std::vector<DataStruct> vector;
  while (std::cin.peek() != EOF)
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed during reading");
    }

    getData(vector);
  }

  std::sort(vector.begin(), vector.end(), compare);
  print(vector);
}
