#include "triangle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>
#include <algorithm>

Triangle::Triangle(const point_t first, const point_t second, const point_t thrid) :
  first_(first),
  second_(second),
  third_(thrid),
  center_(getCenter())
{
  assert(getArea() > 0.0);
}

double getDistance(const point_t first_, const point_t second_)
{
  return sqrt((pow((second_.x - first_.x), 2)) + (pow((second_.y - first_.y), 2)));
}

double Triangle::getArea() const
{
  double firstVector = getDistance(first_, second_); 
  double secondVector = getDistance(second_, third_);
  double thirdVector = getDistance(third_, first_);
  double p = ((firstVector + secondVector + thirdVector) / 2);
  return (sqrt(p * (p - firstVector) * (p - secondVector) * (p - thirdVector)));
}

rectangle_t Triangle::getFrameRect() const
{
  double left = std::min({ first_.x, second_.x, third_.x });
  double bot = std::min({ first_.y, second_.y, third_.y });
  double height = std::max({ first_.x, second_.x, third_.x }) - left;
  double width = std::max({ first_.y, second_.y, third_.y }) - bot;
  point_t pos = { left + (height / 2), bot + (width / 2) };

  return { pos, width, height };
}

void Triangle::move(const double dx, const double dy)
{
  first_.x += dx;
  first_.y += dy;
  second_.x += dx;
  second_.y += dy;
  third_.x += dx;
  third_.y += dy;
  center_.x += dx;
  center_.y += dy;
}

void Triangle::move(const point_t &newPos)
{
  move((newPos.x - center_.x), (newPos.y - center_.y));
}

point_t Triangle::getCenter() const
{
  return { (first_.x + second_.x + third_.x) / 3, (first_.y + second_.y + third_.y) / 3 };
}

void Triangle::printCenter() const
{
  std::cout << "triangle pos-{ " << center_.x << ", " << center_.y << " }" << std::endl ;
}
