#define _USE_MATH_DEFINES
#include "circle.hpp"
#include <cassert>
#include <cmath>

Circle::Circle(const point_t center, const double radius) :
  center_(center),
  radius_(radius)
{
  assert(radius_ > 0.0);
}

double Circle::getArea() const
{
  return radius_ * radius_ * M_PI;
}

rectangle_t Circle::getFrameRect() const
{
  return { center_, radius_ * 2, radius_ * 2 };
}

void Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Circle::move(const point_t &newPos)
{
  center_ = newPos;
}
