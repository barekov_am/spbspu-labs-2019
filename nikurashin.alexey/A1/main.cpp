#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void printData(const double Area, const rectangle_t rect)
{
  std::cout << "Area = " << Area << "\n";
  std::cout << "pos-{ " << rect.pos.x << ", " << rect.pos.y << " }";
  std::cout << " frameRect: width-" << rect.width << "  height-" << rect.height << std::endl;
};

int main()
{
  Rectangle rect({ 12, -4 }, 5, 5);
  std::cout << "Created Rectangle\n";
  printData(rect.getArea(), rect.getFrameRect());
  std::cout << "Rectangle with a new centre  (-7, 7):\n";
  rect.move({ -7, 7 });
  printData(rect.getArea(), rect.getFrameRect());
  std::cout << "Rectangle moved  dx=10.5, dy=0:\n";
  rect.move(10.5, 0);
  printData(rect.getArea(), rect.getFrameRect());

  Circle circ({ 1, 5 }, 3);
  std::cout << "\nCreated Circle\n";
  printData(circ.getArea(), circ.getFrameRect());
  std::cout << "Circle with a new centre (8, 17):\n";
  circ.move({ 8, 17 });
  printData(circ.getArea(), circ.getFrameRect());
  std::cout << "Circle moved = -5, dy= 5:\n";
  circ.move(-5, 5);
  printData(circ.getArea(), circ.getFrameRect());

  Circle newCircle({ 9, 9 }, 9);
  std::cout << "\nCreated new Circle\n";
  Shape * shape = &newCircle;
  printData(newCircle.getArea(), newCircle.getFrameRect());
  std::cout << "Circle with a new centre (6, 6):\n";
  shape->move({ 6, 6 });
  printData(newCircle.getArea(), newCircle.getFrameRect());
  std::cout << "Circle moved = 3.5 dy= 2.8:\n";
  shape->move(3.5, 2.8);
  printData(newCircle.getArea(), newCircle.getFrameRect());

  Rectangle newRectangle({ 4, 5 }, 7, 3);
  std::cout << "\nCreated new Rectangle\n";
  shape = &newRectangle;
  printData(newRectangle.getArea(), newRectangle.getFrameRect());
  std::cout << "Rectangle with a new centre  (-16, 2):\n";
  shape->move({-16, 2 });
  printData(newRectangle.getArea(), newRectangle.getFrameRect());
  std::cout << "Rectangle moved  dx=12, dy=3:\n";
  shape->move(12, 3);
  printData(newRectangle.getArea(), newRectangle.getFrameRect());
  
  Triangle triang({ 1, 1 }, { 2, 3 }, { 3, 1 });
  std::cout << "\nCreated Triangle\n";
  triang.printCenter();
  printData(triang.getArea(), triang.getFrameRect());
  std::cout << "Triangle with a new centre (7, 2):\n";
  triang.move({ 7, 2 });
  triang.printCenter();
  printData(triang.getArea(), triang.getFrameRect());
  std::cout << "Triangle moved = 10, dy= 15:\n";
  triang.move(10, 15);
  triang.printCenter();
  printData(triang.getArea(), triang.getFrameRect());

  const int size = 5;
  const point_t points[size] = {{9, 1}, {1, 1}, {2, 3}, {1, 6}, {4, 3}};
  Polygon polygon(points, size);
  std::cout << "\nCreated Polygon\n";
  printData(polygon.getArea(), polygon.getFrameRect());
  polygon.printCenter();
  std::cout << "Polygon with a new centre (7, 2):\n";
  polygon.move({7, 2});
  printData(polygon.getArea(), polygon.getFrameRect());
  polygon.printCenter();
  std::cout << "Polygon moved = 10, dy= 15:\n";
  polygon.move(10, 15);
  printData(polygon.getArea(), polygon.getFrameRect());
  polygon.printCenter();
  
  return 0;
}
