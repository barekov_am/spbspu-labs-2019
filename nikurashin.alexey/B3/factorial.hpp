#ifndef FACTORIAL_HPP
#define FACTORIAL_HPP

#include <iterator>

class ContainerFactorial
{
public:
  class Iterator : public std::iterator<std::bidirectional_iterator_tag, long int>
  {
  public:

    Iterator(int index);

    long int operator*();

    Iterator& operator++();
    Iterator operator++(int);

    Iterator& operator--();
    Iterator operator--(int);

    bool operator==(const Iterator& iter) const;
    bool operator!=(const Iterator& iter) const;
  private:
    long int value_;
    int index_;
    int limit = 11;
    long int calculate(int index) const;
  };

  Iterator begin() const;
  Iterator end() const;
};

#endif
