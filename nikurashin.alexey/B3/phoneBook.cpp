#include "phoneBook.hpp"
#include <iostream>

void PhoneBook::insert(iterator pos, const PhoneContact& phoneContact_)
{
  records_.insert(pos, phoneContact_);
}

void PhoneBook::removeMark(iterator pos)
{
  records_.erase(pos);
}

void PhoneBook::show(iterator pos) const
{
  std::cout << pos->number << " " << pos->name << "\n";
}

void PhoneBook::pushBack(const PhoneContact& phoneContact_)
{
  records_.push_back(phoneContact_);
}

bool PhoneBook::empty() const
{
  return records_.empty();
}

size_t PhoneBook::size()
{
  return records_.size();
}

PhoneBook::iterator PhoneBook::begin()
{
  return records_.begin();
}

PhoneBook::iterator PhoneBook::end()
{
  return records_.end();
}

PhoneBook::iterator PhoneBook::next(iterator pos)
{
  return { pos == records_.end() ? pos : ++pos };
}

PhoneBook::iterator PhoneBook::prev(iterator pos)
{
  return { pos == records_.begin() ? pos : --pos };
}

PhoneBook::iterator PhoneBook::moveTo(iterator pos, int n)
{
  std::advance(pos, n);
  return pos;
}
