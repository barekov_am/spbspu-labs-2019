#include "phoneBook.hpp"
#include "command.hpp"

#include <iostream>
#include <string>

std::string getNumber(std::string& number)
{
  if (number.empty())
  {
    return "";
  }

  auto it = number.begin();
  while (it != number.end())
  {
    if (!isdigit(*it))
    {
      return "";
    }
    ++it;
  }

  return number;
}

std::string getName(std::string& name)
{
  if (name.empty())
  {
    return "";
  }

  if ((name.front() != '\"') || (name.back() != '\"'))
  {
    return "";
  }

  name.erase(name.begin());
  name.erase(name.end() - 1);

  auto it = name.begin();
  while (it != name.end())
  {
    if (*it == '\\')
    {
      if (((it + 1) != name.end()) && ((*(it + 1) == '\\') || (*(it + 1) == '"')))
      {
        it = name.erase(it) + 1;
      }
      else
      {
        return "";
      }
    }
    else if (*it == '"')
    {
      return "";
    }
    else
    {
      ++it;
    }
  }
  return name;
}

std::string getMark(std::string& markName)
{
  if (markName.empty())
  {
    return "";
  }

  auto it = markName.begin();
  while (it != markName.end())
  {
    if ((!isalnum(*it)) && ((*it) != '-'))
    {
      return "";
    }
    ++it;
  }
  return markName;
}

void add(BookmarkManager& bookmarkManager, std::stringstream& stringStream)
{
  std::string number;
  stringStream >> number >> std::ws;
  number = getNumber(number);

  std::string name;
  getline(stringStream, name);
  name = getName(name);

  if (number.empty() || name.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  bookmarkManager.add({ name, number });
}

void store(BookmarkManager& bookmarkManager, std::stringstream& stringStream)
{
  std::string markName;
  stringStream >> markName >> std::ws;
  markName = getMark(markName);

  std::string newMark;
  getline(stringStream, newMark);
  newMark = getMark(newMark);

  if (markName.empty() || newMark.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  bookmarkManager.store(markName, newMark);
}

void insert(BookmarkManager& bookmarkManager, std::stringstream& stringStream)
{
  std::string position;
  std::string markName;
  std::string number;
  std::string name;
  stringStream >> std::ws >> position >> std::ws;
  stringStream >> markName >> std::ws;
  stringStream >> number >> std::ws;
  getline(stringStream, name);
  markName = getMark(markName);
  number = getNumber(number);
  name = getName(name);

  if (position.empty() || markName.empty() || number.empty() || name.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (bookmarkManager.empty())
  {
    bookmarkManager.add({ name, number });
    return;
  }

  if (position == "before")
  {
    bookmarkManager.insertBefore(markName, { name, number });
  }
  else if (position == "after")
  {
    bookmarkManager.insertAfter(markName, { name, number });
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}

void removeMark(BookmarkManager& bookmarkManager, std::stringstream& stringStream)
{
  std::string markName;
  stringStream >> markName >> std::ws;
  markName = getMark(markName);

  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  bookmarkManager.removeMark(markName);
}

void show(BookmarkManager& bookmarkManager, std::stringstream& stringStream)
{
  std::string markName;
  stringStream >> markName;
  markName = getMark(markName);

  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  bookmarkManager.show(markName);
}

void move(BookmarkManager& bookmarkManager, std::stringstream& stringStream)
{
  std::string markName;
  stringStream >> markName >> std::ws;
  markName = getMark(markName);
  std::string step;
  getline(stringStream, step);

  if ((markName.empty()) || (step.empty()))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if ((step == "first") || (step == "last"))
  {
    bookmarkManager.move(markName, step);
  }
  else if (step.at(0) == '-' || step.at(0) == '+' || (isdigit(step.at(0))))
  {
    for (size_t i = 1; i < step.size(); i++)
    {
      if (!isdigit(step.at(i)))
      {
        std::cout << "<INVALID COMMAND>\n";
        return;
      }
    }
    int num = std::stoi(step);
    bookmarkManager.move(markName, num);
  }
  else
  {
    std::cout << "<INVALID STEP>\n";
    return;
  }
}
