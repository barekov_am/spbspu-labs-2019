#ifndef PHONEBOOKCOMMAND_HPP
#define PHONEBOOKCOMMAND_HPP

#include "phoneBook.hpp"
#include <map>

class BookmarkManager
{
public:
  BookmarkManager();
  void add(const PhoneBook::PhoneContact& phoneContact_);
  void store(const std::string& markName, const std::string& newMark);
  void insertBefore(const std::string& markName, const PhoneBook::PhoneContact& phoneContact_);
  void insertAfter(const std::string& markName, const PhoneBook::PhoneContact& phoneContact_);
  void removeMark(const std::string& markName);
  void show(const std::string& markName);
  void move(const std::string& markName, int step);
  void move(const std::string& markName, const std::string& pos);

  bool empty();
private:
  std::map<std::string, PhoneBook::iterator> bookmarks;
  using bookmarkIter = std::map<std::string, PhoneBook::iterator>::iterator;

  PhoneBook phoneBook_;
  bookmarkIter getIter(const std::string& markName);
};

#endif
