#ifndef COMMAND_HPP
#define COMMAND_HPP

#include "phoneBookCommand.hpp"
#include <sstream>

void add(BookmarkManager& bookmarkManager, std::stringstream& stringStream);
void store(BookmarkManager& bookmarkManager, std::stringstream& stringStream);
void insert(BookmarkManager& bookmarkManager, std::stringstream& stringStream);
void removeMark(BookmarkManager& bookmarkManager, std::stringstream& stringStream);
void show(BookmarkManager& bookmarkManager, std::stringstream& stringStream);
void move(BookmarkManager& bookmarkManager, std::stringstream& stringStream);

#endif
