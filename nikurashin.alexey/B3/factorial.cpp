#include "factorial.hpp"

ContainerFactorial::Iterator::Iterator(int index)
{
  if (index < 1 || index > limit)
  {
    throw std::out_of_range("out of range");
  }

  index_ = index;
  value_ = calculate(index);
}

long int ContainerFactorial::Iterator::operator*()
{
  return value_;
}

ContainerFactorial::Iterator& ContainerFactorial::Iterator::operator++()
{
  if (index_ > limit)
  {
    throw std::out_of_range("out of range");
  }
  ++index_;
  value_ *= index_;
  return *this;
}
ContainerFactorial::Iterator ContainerFactorial::Iterator::operator++(int)
{
  Iterator oldIter = *this;
  ++(*this);
  return oldIter;
}

ContainerFactorial::Iterator& ContainerFactorial::Iterator::operator--()
{
  if (index_ < 1)
  {
    throw std::out_of_range("Index negative");
  }
  value_ /= index_;
  --index_;
  return *this;
}

ContainerFactorial::Iterator ContainerFactorial::Iterator::operator--(int)
{
  Iterator oldIter = *this;
  --(*this);
  return oldIter;
}

bool ContainerFactorial::Iterator::operator==(const Iterator& iter) const
{
  return ((index_ == iter.index_) && (value_ == iter.value_));
}

bool ContainerFactorial::Iterator::operator!=(const Iterator& iter) const
{
  return !(*this == iter);
}

long int ContainerFactorial::Iterator::calculate(int index) const
{
  int value = 1;
  for (int i = 2; i <= index; i++)
  {
    value *= i;
  }
  return value;
}

ContainerFactorial::Iterator ContainerFactorial::begin() const
{
  return Iterator(1);
}

ContainerFactorial::Iterator ContainerFactorial::end() const
{
  return Iterator(11);
}
