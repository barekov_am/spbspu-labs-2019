#include "phoneBookCommand.hpp"
#include <iostream>

BookmarkManager::BookmarkManager()
{
  bookmarks["current"] = phoneBook_.begin();
}

void BookmarkManager::add(const PhoneBook::PhoneContact& phoneContact_)
{
  phoneBook_.pushBack(phoneContact_);
  if (phoneBook_.size() == 1)
  {
    bookmarks["current"] = phoneBook_.begin();
  }
}

void BookmarkManager::store(const std::string& markName, const std::string& newMark)
{
  auto it = getIter(markName);
  if (it == bookmarks.end())
  {
    return;
  }
  bookmarks.emplace(newMark, it->second);
}

void BookmarkManager::insertBefore(const std::string& markName, const PhoneBook::PhoneContact& phoneContact_)
{
  auto it = getIter(markName);
  phoneBook_.insert(it->second, phoneContact_);
}

void BookmarkManager::insertAfter(const std::string& markName, const PhoneBook::PhoneContact& phoneContact_)
{
  auto it = getIter(markName);
  phoneBook_.insert(std::next(it->second), phoneContact_);
}

void BookmarkManager::removeMark(const std::string& markName)
{
  auto it = getIter(markName);

  if (it == bookmarks.end())
  {
    return;
  }

  auto curIt = bookmarks.begin();
  auto removeIt = it->second;

  while (curIt != bookmarks.end())
  {
    if (curIt->second == removeIt)
    {
      if (std::next(curIt->second) == phoneBook_.end())
      {
        curIt->second = phoneBook_.prev(removeIt);
      }
      else
      {
        curIt->second = phoneBook_.next(removeIt);
      }
    }
    ++curIt;
  }
  phoneBook_.removeMark(removeIt);
}

void BookmarkManager::show(const std::string& markName)
{
  auto it = getIter(markName);

  if (it == bookmarks.end())
  {
    return;
  }

  if (phoneBook_.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  phoneBook_.show(it->second);
}

void BookmarkManager::move(const std::string& markName, int step)
{
  auto it = getIter(markName);
  it->second = phoneBook_.moveTo(it->second, step);
}

void BookmarkManager::move(const std::string& markName, const std::string& pos)
{
  auto it = getIter(markName);
  if (pos == "first")
  {
    it->second = phoneBook_.begin();
  }
  else
  {
    it->second = --phoneBook_.end();
  }
}

bool BookmarkManager::empty()
{
  return phoneBook_.empty();
}

BookmarkManager::bookmarkIter BookmarkManager::getIter(const std::string& markName)
{
  auto it = bookmarks.find(markName);
  if (it == bookmarks.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
  }
  return it;
}
