#include "phoneBookCommand.hpp"
#include "command.hpp"

#include <iostream>
#include <functional>

void task1()
{
  BookmarkManager bookmarkManager;
  std::string string;

  while (std::getline(std::cin, string))
  {
    if (std::cin.fail() && !std::cin.eof())
    {
      throw std::ios_base::failure("Reading failed\n");
    }

    std::stringstream stringStream(string);
    std::string command;
    stringStream >> command;

    std::map<std::string, std::function<void(BookmarkManager&, std::stringstream&)>> functions;
    functions = { {"add", add},
                 {"store", store},
                 {"insert", insert},
                 {"delete", removeMark},
                 {"show", show},
                 {"move", move} };

    if (functions.find(command) != functions.end())
    {
      functions.at(command)(bookmarkManager, stringStream);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
      return;
    }
  }
}
