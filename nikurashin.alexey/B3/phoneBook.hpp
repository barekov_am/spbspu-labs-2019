#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <list>
#include <map>
#include <string>

class PhoneBook
{
public:
  struct PhoneContact
  {
    std::string name;
    std::string number;
  };

  using iterator = std::list<PhoneContact>::iterator;

  void insert(iterator pos, const PhoneContact& phoneContact_);
  void removeMark(iterator pos);
  void show(iterator pos) const;
  void pushBack(const PhoneContact& phoneContact_);

  iterator begin();
  iterator end();
  iterator next(iterator pos);
  iterator prev(iterator pos);
  iterator moveTo(iterator pos, int n);

  bool empty() const;
  size_t size();

private:
  std::list<PhoneContact> records_;
};

#endif
