#include "dataStruct.hpp"

void read(std::vector<dataStruct>& vector)
{
  std::string line;
  while (getline(std::cin, line))
  {
    if (line.empty())
    {
      return;
    }
    std::istringstream input(line);
    int key1;
    int key2;
    std::string str;
    char wrongSymbol = 0;

    input >> key1;
    input >> wrongSymbol;
    if (wrongSymbol != ',')
    {
      throw std::invalid_argument("Wrong with data\n");
    }
    input >> key2;
    input >> wrongSymbol;
    if (wrongSymbol != ',')
    {
      throw std::invalid_argument("Wrong with data\n");
    }

    getline(input, str);
    if (str.empty())
    {
      throw std::invalid_argument("Wrong with Data1\n");
    }

    while ((str.at(0) == ' ') || (str.at(0) == '\t'))
    {
      str.erase(0, 1);
    }


    dataStruct dataStruct{key1, key2, str};
    if (outOfBounds(dataStruct))
    {
      throw std::invalid_argument("Wrong with Data2\n");
    }
    vector.push_back(dataStruct);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Trouble with cin\n");
  }
}

bool compare(const dataStruct& lhs, const dataStruct& rhs)
{
  if (lhs.key1 < rhs.key1)
  {
    return true;
  }
  else if (lhs.key1 == rhs.key1)
  {
    if (lhs.key2 < rhs.key2)
    {
      return true;
    }
    else if (lhs.key2 == rhs.key2)
    {
      if (lhs.str.size() < rhs.str.size())
      {
        return true;
      }
    }
  }
  return false;
}

bool outOfBounds(const dataStruct& data)
{
  const int MAX = 5;
  return ((std::abs(data.key1) > MAX) || (std::abs(data.key2) > MAX));
}

void print(const dataStruct& data)
{
  std::cout << data.key1 << "," << data.key2 << "," << data.str << "\n";
}
