#ifndef  dataStruct_HPP
#define  dataStruct_HPP

#include <string>
#include <vector>
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <sstream>

struct dataStruct
{
  int key1;
  int key2;
  std::string str;
};

void read(std::vector<dataStruct>& vector);
bool compare(const dataStruct& lhs, const dataStruct& rhs);
bool outOfBounds(const dataStruct& data);
void print(const dataStruct& data);

#endif 
