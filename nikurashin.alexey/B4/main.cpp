#include "dataStruct.hpp"

int main()
{
  std::vector<dataStruct> data;
  try
  {
    read(data);
  }
  catch (std::invalid_argument & err)
  {
    std::cerr << err.what();
    return 1;
  }
  std::sort(data.begin(), data.end(), compare);
  std::for_each(data.begin(), data.end(), print);
  return 0;
}
