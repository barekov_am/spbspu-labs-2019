#include "functor.hpp"

Functor::Functor() :
  maxNumber_(0),
  minNumber_(0),
  oddSum_(0),
  evenSum_(0),
  firstElement_(0),
  lastElement_(0),
  positiveNumberCounter_(0),
  negativeNumberCounter_(0),
  elementCounter_(0),
  average_(0.0),
  isFirst_(true)
{}

void Functor::operator() (long int number)
{
  if (isFirst_)
  {
    firstElement_ = number;
    maxNumber_ = number;
    minNumber_ = maxNumber_;
    isFirst_ = false;
  }

  if (number > 0)
  {
    positiveNumberCounter_++;
  }
  else if (number < 0)
  {
    negativeNumberCounter_++;
  }

  if (number % 2 == 0)
  {
    evenSum_ += number;
  }
  else
  {
    oddSum_ += number;
  }

  maxNumber_ = std::max(maxNumber_, number);
  minNumber_ = std::min(minNumber_, number);
  lastElement_ = number;
  elementCounter_++;
  average_ = static_cast<double>(evenSum_ + oddSum_) / elementCounter_;
}

bool Functor::empty() const
{
  return (elementCounter_ == 0);
}

void Functor::printInfo()
{
  std::cout << "Max: " << maxNumber_ << std::endl
    << "Min: " << minNumber_ << std::endl
    << "Mean: " << average_ << std::endl
    << "Positive: " << positiveNumberCounter_ << std::endl
    << "Negative: " << negativeNumberCounter_ << std::endl
    << "Odd Sum: " << oddSum_ << std::endl
    << "Even Sum: " << evenSum_ << std::endl
    << "First/Last Equal: " << (firstElement_ == lastElement_ ? "yes" : "no") << std::endl;
}
