#include"functor.hpp"

#include <iterator>
#include <list>

int main()
{
  try
  {
    std::list<int> list((std::istream_iterator<int>(std::cin)), std::istream_iterator<int>());

    if (std::cin.fail() && !std::cin.eof())
    {
      throw std::ios_base::failure("Reading failed\n");
    }

    Functor Func;
    Func = for_each(list.begin(), list.end(), Functor());

    if (Func.empty())
    {
      std::cout << "No Data\n";
      return 0;
    }
    Func.printInfo();
  }
  catch (const std::exception & e)
  {
    std::cerr << e.what();
    return 1;
  }
}
