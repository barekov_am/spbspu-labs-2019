#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <iostream>
#include <algorithm>

class Functor
{
public:
  Functor();
  void operator() (long int number);
  void printInfo();
  bool empty() const;

private:
  long int maxNumber_;
  long int minNumber_;
  long long int oddSum_;
  long long int evenSum_;
  int firstElement_;
  int lastElement_;
  long int positiveNumberCounter_;
  long int negativeNumberCounter_;
  int elementCounter_;
  double average_;
  bool isFirst_;
};

#endif 
