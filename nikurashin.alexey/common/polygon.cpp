#define _USE_MATH_DEFINES
#include "polygon.hpp"

#include <iostream>
#include <cmath>
#include <algorithm>
#include <stdexcept>

nikurashin::Polygon::Polygon(const Polygon& other) :
  vertex_(new point_t[other.quantity_]),
  quantity_(other.quantity_),
  centre_(other.centre_)
{
  for (int i = 0; i < quantity_; i++)
  {
    vertex_[i] = other.vertex_[i];
  }
}

nikurashin::Polygon::Polygon(Polygon&& other) :
  vertex_(other.vertex_),
  quantity_(other.quantity_),
  centre_(other.centre_)
{
  other.vertex_ = nullptr;
  other.quantity_ = 0;
  other.centre_ = { 0.0, 0.0 };
}

nikurashin::Polygon::Polygon(const point_t* vertex, int quantity) :
  vertex_(new point_t[quantity]),
  quantity_(quantity),
  centre_(getCenter())
{
  if (quantity <= 2)
  {
    delete[] vertex_;
    throw std::invalid_argument("Area must be >0,there should be 3+ corners");
  }

  if (vertex == nullptr)
  {
    delete[] vertex_;
    throw std::invalid_argument("pointer to vertex is null");
  }

  for (int i = 0; i < quantity_; ++i)
  {
    vertex_[i] = vertex[i];
  }
  centre_ = getCenter();

  if (!checkBump())
  {
    delete[] vertex_;
    throw std::invalid_argument("polygon must be convex.");
  }

  if (getArea() == 0.0)
  {
    delete[] vertex_;
    throw std::invalid_argument("Area must be more then 0.");
  }
}

nikurashin::Polygon::~Polygon()
{
  delete[] vertex_;
}

nikurashin::Polygon& nikurashin::Polygon::operator =(const Polygon& other)
{
  if (this != &other)
  {
    delete[] vertex_;
    quantity_ = other.quantity_;
    centre_ = other.centre_;
    vertex_ = new point_t[quantity_];

    for (int i = 0; i < quantity_; ++i)
    {
      vertex_[i] = other.vertex_[i];
    }
  }
  return *this;
}

nikurashin::Polygon& nikurashin::Polygon::operator =(Polygon&& other)
{
  if (this != &other)
  {
    quantity_ = other.quantity_;
    centre_ = other.centre_;
    delete[] vertex_;

    other.quantity_ = 0;
    other.centre_ = { 0.0, 0.0 };
    vertex_ = other.vertex_;
    other.vertex_ = nullptr;
  }
  return *this;
}

nikurashin::rectangle_t nikurashin::Polygon::getFrameRect() const
{
  point_t min = vertex_[0];
  point_t max = vertex_[0];

  for (int i = 1; i < quantity_; ++i)
  {
    min.x = std::min(vertex_[i].x, min.x);
    min.y = std::min(vertex_[i].y, min.y);
    max.x = std::max(vertex_[i].x, max.x);
    max.y = std::max(vertex_[i].y, max.y);
  }
  double width = max.x - min.x;
  double height = max.y - min.y;
  point_t recCentre = { (max.x + min.x) / 2, (max.y + min.y) / 2 };

  return{ recCentre, width, height };
}

double nikurashin::Polygon::getArea() const
{
  double sum = 0;
  for (int i = 0; i < quantity_ - 1; i++)
  {
    sum += vertex_[i].x * vertex_[i + 1].y;
    sum -= vertex_[i + 1].x * vertex_[i].y;
  }
  sum += vertex_[quantity_ - 1].x * vertex_[0].y;
  sum -= vertex_[0].x * vertex_[quantity_ - 1].y;
  return (fabs(sum) / 2);
}

nikurashin::point_t nikurashin::Polygon::getCenter() const
{
  point_t sum = { 0, 0 };
  for (int i = 0; i < quantity_; ++i)
  {
    sum.x += vertex_[i].x;
    sum.y += vertex_[i].y;
  }
  return { sum.x / quantity_, sum.y / quantity_ };
}

void nikurashin::Polygon::move(const point_t & newPos)
{
  double dx = newPos.x - centre_.x;
  double dy = newPos.y - centre_.y;

  move(dx, dy);
}

void nikurashin::Polygon::move(const double dx, const double dy)
{
  centre_.x += dx;
  centre_.y += dy;
  for (int i = 0; i < quantity_; ++i)
  {
    vertex_[i].x += dx;
    vertex_[i].y += dy;
  }
}

bool nikurashin::Polygon::checkBump() const
{
  point_t sideA = { 0, 0 };
  point_t sideB = { 0, 0 };
  for (int i = 0; i <= quantity_ - 2; i++)
  {
    sideA.x = vertex_[i + 1].x - vertex_[i].x;
    sideA.x = vertex_[i + 1].x - vertex_[i].x;

    if (i == quantity_ - 2)
    {
      sideB.x = vertex_[0].x - vertex_[i + 1].x;
      sideB.y = vertex_[0].y - vertex_[i + 1].y;
    }
    else
    {
      sideB.x = vertex_[i + 2].x - vertex_[i + 1].x;
      sideB.y = vertex_[i + 2].y - vertex_[i + 1].y;
    }
    if ((sideA.x * sideB.y) - (sideA.y * sideB.x) < 0)
    {
      return true;
    }
  }
  return false;
}

void nikurashin::Polygon::printCenter() const
{
  std::cout << "Polygon pos-{ " << centre_.x << ", " << centre_.y << " }" << std::endl;
}

void nikurashin::Polygon::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("factor cannot be");
  }

  for (int i = 0; i <= quantity_-1; i++)
  {
    vertex_[i] = { centre_.x + factor * (vertex_[i].x - centre_.x), centre_.y + factor * (vertex_[i].y - centre_.y) };
  }
}

void nikurashin::Polygon::rotate(double angle)
{
  const point_t center = getCenter();
  angle = angle * M_PI / 180;
  for (int i = 0; i < quantity_; i++)
  {
    const point_t tempPoint = vertex_[i];
    vertex_[i].x = center.x + (tempPoint.x - center.x) * cos(angle) -
      (tempPoint.y - center.y) * sin(angle);
    vertex_[i].y = center.y + (tempPoint.x - center.x) * sin(angle) +
      (tempPoint.y - center.y) * cos(angle);
  }
}
