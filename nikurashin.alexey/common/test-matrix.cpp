//#include <stdexcept>
//#include <boost/test/auto_unit_test.hpp>
//
//#include "circle.hpp"
//#include "rectangle.hpp"
//#include "triangle.hpp"
//#include "composite-shape.hpp"
//#include "matrix.hpp"
//#include "partition.hpp"
//
//const double Inaccuracy = 0.01;
//
//BOOST_AUTO_TEST_SUITE(testForMatrix)
//
//BOOST_AUTO_TEST_CASE(copyAndMove)
//{
//  nikurashin::Rectangle rectangle({ 4, 6 }, 6, 7);
//  nikurashin::CompositeShape compositeShape(std::make_shared<nikurashin::Rectangle>(rectangle));
//  nikurashin::Matrix matrix = nikurashin::division(compositeShape);
//
//  nikurashin::Matrix matrix3 = matrix;
//
//  BOOST_CHECK_EQUAL(matrix3.getLines(), 1);
//  BOOST_CHECK_EQUAL(matrix3.getColumns(), 1);
//  BOOST_CHECK_NO_THROW(nikurashin::Matrix matrix1(matrix));
//  BOOST_CHECK_NO_THROW(nikurashin::Matrix matrix2(std::move(matrix)));
//
//  nikurashin::Matrix matrix4;
//  nikurashin::Matrix matrix5;
//
//  BOOST_CHECK_NO_THROW(matrix4 = matrix);
//  BOOST_CHECK_NO_THROW(matrix5 = std::move(matrix));
//}
//
//BOOST_AUTO_TEST_CASE(testForThrow)
//{
//  nikurashin::Rectangle rectangle({ 2, 4.5 }, 1, 2);
//  nikurashin::Rectangle rectangle1({ 3.5, 2 }, 2, 5);
//  nikurashin::Circle circle({ 6, 4 }, 2);
//  nikurashin::Triangle triangle({ 3, 2 }, { 2, 0 }, { 4, 0 });
//  nikurashin::Triangle triangle1({ 7, 4 }, { 10, 3 }, { 10, 5 });
//  nikurashin::CompositeShape compositeShape(std::make_shared<nikurashin::Rectangle>(rectangle));
//  compositeShape.add(std::make_shared<nikurashin::Rectangle>(rectangle1));
//  compositeShape.add(std::make_shared<nikurashin::Circle>(circle));
//  compositeShape.add(std::make_shared<nikurashin::Triangle>(triangle));
//  compositeShape.add(std::make_shared<nikurashin::Triangle>(triangle1));
//
//  nikurashin::Matrix matrix = nikurashin::division(compositeShape);
//  BOOST_CHECK_THROW(matrix[10][10], std::out_of_range);
//  BOOST_CHECK_NO_THROW(matrix[0][1]);
//
//}
//
//BOOST_AUTO_TEST_SUITE_END()
