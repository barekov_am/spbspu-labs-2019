#define _USE_MATH_DEFINES
#include "rectangle.hpp"

#include <cassert>
#include <stdexcept>
#include <cmath>

nikurashin::Rectangle::Rectangle(const point_t pos, const double width, const double height) :
  rect_({ pos, width, height }),
  angle_(0)
{
  if (width <= 0)
  {
    throw std::invalid_argument("Width has invalid value");
  }
  if (height <= 0)
  {
    throw std::invalid_argument("Height has invalid value");
  }
}

double nikurashin::Rectangle::getArea() const
{
  return rect_.width* rect_.height;
}

nikurashin::rectangle_t nikurashin::Rectangle::getFrameRect() const
{
  double width = rect_.width * std::fabs(cos((M_PI * angle_) / 180)) + rect_.height * std::fabs(sin((M_PI * angle_) / 180));
  double height = rect_.height * std::fabs(cos((M_PI * angle_) / 180)) + rect_.width * std::fabs(sin((M_PI * angle_) / 180));
  return { rect_.pos, width, height };

}

void nikurashin::Rectangle::move(const double dx, const double dy)
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

void nikurashin::Rectangle::move(const point_t& newPos)
{
  rect_.pos = newPos;
}

void nikurashin::Rectangle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("factor cannot be <= 0 ");
  }
  rect_.height *= factor;
  rect_.width *= factor;
}

void nikurashin::Rectangle::rotate(double angle)
{
  angle_ += angle;
  if (angle_ < 0)
  {
    angle_ = 360 + fmod(angle_, 360);
  }
}
