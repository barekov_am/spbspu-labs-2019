#define _USE_MATH_DEFINES
#include "circle.hpp"

#include <cassert>
#include <cmath>
#include <stdexcept>

nikurashin::Circle::Circle(const point_t center, const double radius) :
  center_(center),
  radius_(radius)
{
  if (radius_ <= 0.0)
  {
    throw std::invalid_argument("Radius has invalid value");
  }
}

double nikurashin::Circle::getArea() const
{
  return radius_ * radius_* M_PI;
}

nikurashin::rectangle_t nikurashin::Circle::getFrameRect() const
{
  return { center_, radius_ * 2, radius_ * 2 };
}

void nikurashin::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void nikurashin::Circle::move(const point_t& newPos)
{
  center_ = newPos;
}

void nikurashin::Circle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("factor cannot be <= 0 ");
  }
  radius_ *= factor;
}

void nikurashin::Circle::rotate(double)
{
}
