//#include <stdexcept>
//
//#include <boost/test/auto_unit_test.hpp>
//
//#include "triangle.hpp"
//
//const double fault = 0.01;
//
//BOOST_AUTO_TEST_SUITE(triangleTest)
//
//BOOST_AUTO_TEST_CASE(invarianceRectParametersAfterMovingTo)
//{
//  nikurashin::Triangle tempTriangle({ 2.3, 6.7 }, { 7.8, 3.4 }, { 4.5, 5.6 });
//  const double initialArea = tempTriangle.getArea();
//  const nikurashin::rectangle_t initialRectangle = tempTriangle.getFrameRect();
//
//  tempTriangle.move({ 4.2, 5.4 });
//
//  BOOST_CHECK_CLOSE(initialRectangle.width, tempTriangle.getFrameRect().width, fault);
//  BOOST_CHECK_CLOSE(initialRectangle.height, tempTriangle.getFrameRect().height, fault);
//  BOOST_CHECK_CLOSE(initialArea, tempTriangle.getArea(), fault);
//}
//
//BOOST_AUTO_TEST_CASE(invarianceRectParametersAfterMovingBy)
//{
//  nikurashin::Triangle tempTriangle({ 2.3, 6.7 }, { 7.8, 3.4 }, { 4.5, 5.6 });
//  const double initialArea = tempTriangle.getArea();
//  const nikurashin::rectangle_t initialRectangle = tempTriangle.getFrameRect();
//
//  tempTriangle.move(3.3, 17.5);
//
//  BOOST_CHECK_CLOSE(initialRectangle.width, tempTriangle.getFrameRect().width, fault);
//  BOOST_CHECK_CLOSE(initialRectangle.height, tempTriangle.getFrameRect().height, fault);
//  BOOST_CHECK_CLOSE(initialArea, tempTriangle.getArea(), fault);
//}
//
//BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterScale)
//{
//  nikurashin::Triangle tempTriangle({ 2.3, 6.7 }, { 7.8, 3.4 }, { 4.5, 5.6 });
//  const double initialArea = tempTriangle.getArea();
//  double factor = 3.0;
//
//  tempTriangle.scale(factor);
//
//  BOOST_CHECK_CLOSE(initialArea * factor * factor, tempTriangle.getArea(), fault);
//}
//
//BOOST_AUTO_TEST_CASE(invalidValues)
//{
//  nikurashin::Triangle tempTriangle({ 3.4, 4.5 }, { 2.3, 7.8 }, { 14.5, 3.4 });
//
//  BOOST_CHECK_THROW(tempTriangle.scale(0.0), std::invalid_argument);
//  BOOST_CHECK_THROW(tempTriangle.scale(-1.5), std::invalid_argument);
//}
//
//BOOST_AUTO_TEST_SUITE_END()
