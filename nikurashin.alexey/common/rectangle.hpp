#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace nikurashin
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t pos, double width, const double height);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const double dx, const double dy) override;
    void move(const point_t& newPos) override;
    void scale(double factor) override;
    void rotate(double angle) override;
  private:
    rectangle_t rect_;
    double angle_;
  };
}

#endif
