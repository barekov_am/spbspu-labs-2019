//#include <stdexcept>
//
//#include <boost/test/auto_unit_test.hpp>
//
//#include "polygon.hpp"
//
//const double fault = 0.01;
//
//BOOST_AUTO_TEST_SUITE(PolygonTests)
//
//BOOST_AUTO_TEST_CASE(invarianceRectParametersAfterMovingTo)
//{
//  const int size = 5;
//  const nikurashin::point_t points[size] = { {9, 1}, {1, 1}, {2, 3}, {1, 6}, {4, 3} };
// 
//  nikurashin::Polygon tempPolygon(points, size);
//  const double initialArea = tempPolygon.getArea();
//  const nikurashin::rectangle_t initialRectangle = tempPolygon.getFrameRect();
//
//  tempPolygon.move({ 4.2, 5.4 });
//
//  BOOST_CHECK_CLOSE(initialRectangle.width, tempPolygon.getFrameRect().width, fault);
//  BOOST_CHECK_CLOSE(initialRectangle.height, tempPolygon.getFrameRect().height, fault);
//  BOOST_CHECK_CLOSE(initialArea, tempPolygon.getArea(), fault);
//}
//
//BOOST_AUTO_TEST_CASE(invarianceRectParametersAfterMovingBy)
//{
//  const int size = 5;
//  const nikurashin::point_t points[size] = { {9, 1}, {1, 1}, {2, 3}, {1, 6}, {4, 3} };
//
//  nikurashin::Polygon tempPolygon(points, size);
//  const double initialArea = tempPolygon.getArea();
//  const nikurashin::rectangle_t initialRectangle = tempPolygon.getFrameRect();
//
//  tempPolygon.move(3.3, 17.5);
//
//  BOOST_CHECK_CLOSE(initialRectangle.width, tempPolygon.getFrameRect().width, fault);
//  BOOST_CHECK_CLOSE(initialRectangle.height, tempPolygon.getFrameRect().height, fault);
//  BOOST_CHECK_CLOSE(initialArea, tempPolygon.getArea(), fault);
//}
//
//BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterScale)
//{
//  const int size = 5;
//  const nikurashin::point_t points[size] = { {9, 1}, {1, 1}, {2, 3}, {1, 6}, {4, 3} };
// 
//  nikurashin::Polygon tempPolygon(points, size);
//  const double initialArea = tempPolygon.getArea();
//  double factor = 3.0;
//
//  tempPolygon.scale(factor);
//    
//  BOOST_CHECK_CLOSE(initialArea * factor * factor, tempPolygon.getArea(), fault);
//}
//
//BOOST_AUTO_TEST_CASE(invalidValues)
//{
//  const int size = 5;
//  const nikurashin::point_t points[size] = { {9, 1}, {1, 1}, {2, 3}, {1, 6}, {4, 3} };
//
//  nikurashin::Polygon tempPolygon(points, size);
//
//  BOOST_CHECK_THROW(tempPolygon.scale(0.0), std::invalid_argument);
//  BOOST_CHECK_THROW(tempPolygon.scale(-1.5), std::invalid_argument);
//}
//
//BOOST_AUTO_TEST_SUITE_END()
