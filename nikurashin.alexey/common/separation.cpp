#include "separation.hpp"
#include <cmath>

bool nikurashin::checkIntersect(nikurashin::rectangle_t shape1, nikurashin::rectangle_t shape2)
{
  const double distanceBetweenCentersX = fabs(shape1.pos.x - shape2.pos.x);
  const double distanceBetweenCentersY = fabs(shape1.pos.y - shape2.pos.y);

  const double lengthX = (shape1.width + shape2.width) / 2;
  const double lengthY = (shape1.height + shape2.height) / 2;

  const bool firstCondition = (distanceBetweenCentersX < lengthX);
  const bool secondCondition = (distanceBetweenCentersY < lengthY);

  return firstCondition && secondCondition;

}

nikurashin::Matrix nikurashin::division(std::unique_ptr<nikurashin::Shape::shapePtr[]>& shapes, size_t size)
{
  Matrix matrix;

  for (size_t i = 0; i < size; i++)
  {
    size_t lines = 0;
    size_t columns = 0;
    size_t needfulColumns = 0;
    for (size_t j = 0; j < matrix.getLines(); j++)
    {
      needfulColumns = matrix.getLineSize(j);
      for (size_t k = 0; k < needfulColumns; k++)
      {
        if (checkIntersect(shapes[i]->getFrameRect(), matrix[j][k]->getFrameRect()))
        {
          lines++;
          break;
        }
        if (k == needfulColumns - 1)
        {
          lines = j;
          columns = needfulColumns;
        }
      }
      if (lines == j)
      {
        break;
      }
    }
    matrix.add(shapes[i], lines, columns);
  }
  return matrix;
}

nikurashin::Matrix nikurashin::division(nikurashin::CompositeShape& compositeShape)
{
  std::unique_ptr<Shape::shapePtr[]> temp(std::make_unique<Shape::shapePtr[]>(compositeShape.getCount()));
  for (size_t i = 0; i < compositeShape.getCount(); i++)
  {
    temp[i] = compositeShape[i];
  }
  Matrix matrix = division(temp, compositeShape.getCount());
  return matrix;
}
