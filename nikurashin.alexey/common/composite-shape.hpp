#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace nikurashin
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape& rhs);
    CompositeShape(CompositeShape&& rhs);
    CompositeShape(shapePtr shape);
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape& rhs);
    CompositeShape& operator =(CompositeShape&& ehs);
    shapePtr operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& point) override;
    void move(const double, const double) override;
    void printInfo() const;
    void scale(const double) override;
    void rotate(double angle) override;
    void add(shapePtr shape);
    void remove(size_t i);
    void remove(shapePtr shape);
    size_t getCount() const;

  private:

    size_t size_;
    std::unique_ptr<shapePtr[]> shapeArray_;

  };
}
#endif

