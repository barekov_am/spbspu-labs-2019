//#include <boost/test/auto_unit_test.hpp>
//#include <stdexcept>
//#include "circle.hpp"
//#include "rectangle.hpp"
//#include "triangle.hpp"
//#include "composite-shape.hpp"
//#include "matrix.hpp"
//#include "separation.hpp"
//
//BOOST_AUTO_TEST_SUITE(partitionMetodsTests)
//
//BOOST_AUTO_TEST_CASE(partitionTest)
//{
//  nikurashin::Rectangle rectangle({ 2, 4.5 }, 1, 2);
//  nikurashin::Rectangle rectangle1({ 3.5, 2 }, 2, 5);
//  nikurashin::Circle circle({ 6, 4 }, 2);
//
//  nikurashin::CompositeShape compositeShape(std::make_shared<nikurashin::Rectangle>(rectangle));
//  compositeShape.add(std::make_shared<nikurashin::Rectangle>(rectangle1));
//  compositeShape.add(std::make_shared<nikurashin::Circle>(circle));
//
//  nikurashin::Matrix matrix = nikurashin::division(compositeShape);
//
//  BOOST_CHECK_EQUAL(matrix.getLines(), 2);
//  BOOST_CHECK_EQUAL(matrix.getColumns(), 2);
//}
//
//BOOST_AUTO_TEST_CASE(intersectionTest)
//{
//  nikurashin::Rectangle rectangle({ 2, 4.5 }, 1, 2);
//  nikurashin::Rectangle rectangle1({ 3.5, 2 }, 2, 5);
//  nikurashin::Circle circle({ 6, 4 }, 2);
//
//  BOOST_CHECK(!nikurashin::checkIntersect(rectangle.getFrameRect(), rectangle1.getFrameRect()));
//  BOOST_CHECK(!nikurashin::checkIntersect(rectangle.getFrameRect(), circle.getFrameRect()));
//  BOOST_CHECK(nikurashin::checkIntersect(rectangle1.getFrameRect(), circle.getFrameRect()));
//}
//
//BOOST_AUTO_TEST_SUITE_END()
