#ifndef SEPARATION_HPP
#define SEPARATION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"
#include <memory>

namespace nikurashin
{
  bool checkIntersect(nikurashin::rectangle_t shape1, nikurashin::rectangle_t shape2);
  Matrix division(std::unique_ptr<nikurashin::Shape::shapePtr[]>& shapes, size_t size);
  Matrix division(CompositeShape& compositeShape);
}


#endif
