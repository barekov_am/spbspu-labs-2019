#ifndef POLYGON_HPP
#define POLYGON_HPP
#include "shape.hpp"

namespace nikurashin
{
  class Polygon : public Shape
  {
  public:
    Polygon(const Polygon& other);
    Polygon(Polygon&& other);
    Polygon(const point_t* vertex, int quantity);
    virtual ~Polygon();
    Polygon& operator =(const Polygon& other);
    Polygon& operator =(Polygon&& other);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const double dx, const double dy) override;
    void move(const point_t& newPos) override;
    void scale(double factor) override;
    point_t getCenter() const;
    bool checkBump() const;
    void printCenter() const;
    void rotate(double) override;
  private:
    point_t* vertex_;
    int quantity_;
    point_t centre_;
  };
}

#endif
