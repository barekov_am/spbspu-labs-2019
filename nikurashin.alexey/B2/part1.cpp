#include "queue.hpp"

#include <iostream>
#include <sstream>

void add(QueueWithPriority<std::string> &queue, std::string priority, std::string data)
{
  if (data.empty() || priority.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  elementPriority elementsPriority;

  if (priority == "high")
  {
    elementsPriority = elementPriority::HIGH;
  }
  else if (priority == "normal")
  {
    elementsPriority = elementPriority::NORMAL;
  }
  else if (priority == "low")
  {
    elementsPriority = elementPriority::LOW;
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  queue.putElementToQueue(data, elementsPriority);
}

void get(QueueWithPriority<std::string>& priority)
{
  if (priority.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  std::cout << priority.getElementFromQueue() << "\n";
}

void task1()
{
  QueueWithPriority<std::string> queue;
  std::string input;

  while (std::getline(std::cin, input))
  {
    std::stringstream stringStream(input);
    std::string command;
    stringStream >> command;

    if (std::cin.fail() && !std::cin.eof())
    {
      throw std::ios_base::failure("Reading failed\n");
    }

    if (command == "add")
    {
      std::string priority;
      stringStream >> priority >> std::ws;
      std::string data;
      std::getline(stringStream, data);

      add(queue, priority, data);
    }
    else if (command == "get")
    {
      get(queue);
    }
    else if (command == "accelerate")
    {
      queue.accelerate();
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
