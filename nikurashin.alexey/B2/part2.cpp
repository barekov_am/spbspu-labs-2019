#include <iostream>
#include <list>

void task2()
{
  std::list<int> list;
  const int minValue = 1;
  const int maxValue = 20;
  const size_t maxSize = 20;
  int value;

  while (std::cin >> value)
  {
    if ((value >= minValue ) && (value <= maxValue))
    {
      list.push_back(value);
    }
    else
    {
      throw std::invalid_argument("Wrong value\n");
    }
    
    if (list.size() > maxSize)
    {
      throw std::invalid_argument("Wrong size of list\n");
    }
  }
  
  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::ios_base::failure("Reading failed\n");
  }

  if (list.empty())
  {
    return;
  }

  auto first = list.begin();
  auto last_ = list.end();
  size_t iter = 0;
  while (iter < list.size() / 2)
  {
    std::cout << *first << " " << *std::prev(last_) << " ";
    ++first;
    --last_;
    iter++;
  }
  if (list.size() % 2 != 0)
  {
    std::cout << *first;
  }
  std::cout << "\n";
}
