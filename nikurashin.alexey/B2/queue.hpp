#ifndef queue_hpp
#define queue_hpp

#include "InterfaceQueue.hpp"

template<typename T>
void QueueWithPriority<T>::putElementToQueue(const T& element, elementPriority priority)
{
  switch (priority)
  {
  case elementPriority::LOW:
  {
    low.push_back(element);
    break;
  }
  case elementPriority::NORMAL:
  {
    normal.push_back(element);
    break;
  }
  case elementPriority::HIGH:
  {
    high.push_back(element);
    break;
  }
  default:
  {
    throw std::invalid_argument("Wrong priority\n");
  }
  }
}

template<typename T>
T QueueWithPriority<T>::getElementFromQueue()
{
  if (!high.empty())
  {
    T element = high.front();
    high.pop_front();
    return (element);
  }

  if (!normal.empty())
  {
    T element = normal.front();
    normal.pop_front();
    return (element);
  }

  if (!low.empty())
  {
    T element = low.front();
    low.pop_front();
    return (element);
  }
  throw std::out_of_range("EMPTY\n");
}

template<typename T>
void QueueWithPriority<T>::accelerate()
{
  high.splice(high.end(), low);
}

template <typename T>
bool QueueWithPriority<T>::empty()
{
  return (high.empty() && normal.empty() && low.empty());
}

#endif
