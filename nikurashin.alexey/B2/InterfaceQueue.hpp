#ifndef InterfaceQueue_hpp
#define InterfaceQueue_hpp

#include <iostream>
#include <list>


enum elementPriority
{
  LOW,
  NORMAL,
  HIGH
};

template<typename T>
class QueueWithPriority
{
public:
  void putElementToQueue(const T& element, elementPriority priority);
  T getElementFromQueue();
  void accelerate();
  bool empty();

private:
  std::list<T> low;
  std::list<T> normal;
  std::list<T> high;
};

#endif 
