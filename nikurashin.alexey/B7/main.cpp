#include <iostream>
#include "functions.hpp"

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Not arguments\n";
      return 1;
    }

    int task = std::atoi(argv[1]);

    switch (task)
    {
    case 1:
      task1();
      break;
    case 2:
      task2();
      break;
    default:
    {
      std::cerr << "Wrong number of task\n";
      return 1;
    }
    }
  }
  catch (const std::exception & e)
  {
    std::cerr << e.what();
    return 1;
  }

  return 0;
}
