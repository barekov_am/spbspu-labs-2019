#include "Circle.hpp"

Circle::Circle(int x, int y) :
  Shape(x, y)
{}

void Circle::draw(std::ostream &stream) const
{
  stream << "CIRCLE " << '(' << x_ << "; " << y_ << ")\n";
}
