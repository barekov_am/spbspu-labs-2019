#include "functor.hpp"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

void task1()
{
  std::vector <double> vec((std::istream_iterator<double>(std::cin)), std::istream_iterator<double>());

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::ios_base::failure("Reading failed\n");
  }

  for_each(vec.begin(), vec.end(), Functor());
  for (double& num : vec)
  {
    std::cout << num << ' ';
  }
  std::cout << '\n';
}
