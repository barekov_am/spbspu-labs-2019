#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP
#define _USE_MATH_DEFINES

#include <math.h>

struct Functor
{
  void operator() (double& num)
  {
    num *= M_PI;
  }
};

#endif
