#include "Triangle.hpp"

Triangle::Triangle(int x, int y) :
  Shape(x, y)
{}

void Triangle::draw(std::ostream &stream) const
{
  stream << "TRIANGLE " << '(' << x_ << "; " << y_ << ")\n";
}
