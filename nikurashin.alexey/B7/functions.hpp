#ifndef FUNTIONS_HPP
#define FUNTIONS_HPP

#include <vector>
#include <string>
#include <iterator>
#include <algorithm>
#include <stdexcept>
#include <sstream>
#include "Circle.hpp"
#include "Triangle.hpp"
#include "Square.hpp"

void task1();
void task2();
std::vector<Shape *> createShapesVector();

#endif
