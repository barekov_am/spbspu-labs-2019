#include <iostream>
#include <set>
#include <sstream>

void Task1()
{
  std::string Buffer;
  std::set<std::string> wordSet;

  while (std::cin >> Buffer)
  {
    wordSet.emplace(Buffer);
  }

  if (!wordSet.empty())
  {
    for (const auto& word : wordSet)
    {
      std::cout << word << std::endl;
    }
  }
}
