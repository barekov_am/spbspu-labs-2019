#include "shape.hpp"
#include <iostream>
#include <numeric>
#include <sstream>
#include <algorithm>

void Task2()
{
  std::string line;
  std::vector<Shape> shapes;
  std::vector<Point> points;

  while (std::getline(std::cin, line))
  {
    if (std::cin.fail() && !std::cin.eof())
    {
      throw std::ios_base::failure("Read failed\n");
    }

    bool Space = true;
    for (auto it = line.begin(); it != line.end(); ++it)
    {
      if (!(*it == ' ' || *it == '\t'))
      {
        Space = false;
        break;
      }
    }

    if (Space)
    {
      continue;
    }

    Shape shape;
    std::stringstream stringStream(line);
    stringStream >> shape;
    shapes.push_back(shape);
    
  }

  size_t amountOfTriangles = std::count_if(shapes.begin(), shapes.end(), Triangle);
  size_t amountOfRectangle = std::count_if(shapes.begin(), shapes.end(), Rectangle);
  size_t amountOfSquares = std::count_if(shapes.begin(), shapes.end(), Square);
  size_t amountOfVertices = std::accumulate(shapes.begin(), shapes.end(), 0, sumVertices);

  shapes.erase(std::remove_if(shapes.begin(), shapes.end(), Pentagon), shapes.end());

  std::cout << "Vertices: " << amountOfVertices << std::endl;
  std::cout << "Triangles: " << amountOfTriangles << std::endl;
  std::cout << "Squares: " << amountOfSquares << std::endl;
  std::cout << "Rectangles: " << amountOfRectangle << std::endl;

  for (Shape shape : shapes)
  {
    points.push_back(shape.at(0));
  }

  std::cout << "Points: ";

  for (auto& point : points)
  {
    std::cout << " (" << point.x << "; " << point.y << ')';
  }

  std::sort(shapes.begin(), shapes.end(), compare);

  std::cout << std::endl << "Shapes:" << std::endl;

  for (const auto& shape : shapes)
  {
    std::cout << shape.size();

    for (const auto& point : shape)
    {
      std::cout << " (" << point.x << ";" << point.y <<")";
    }

    std::cout << std::endl;
  }
}
