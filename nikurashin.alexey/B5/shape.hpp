#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <vector>
#include <istream>

struct Point
{
  int x, y;
};

using Shape = std::vector<Point>;

std::istream& operator >> (std::istream& instream, Shape& shape);
std::ostream& operator<<(std::ostream& ostream, const Point& point);


bool Pentagon(const Shape& shape);
bool Triangle(const Shape& shape);
bool Square(const Shape& shape);
bool Rectangle(const Shape& shape);

int sumVertices(int sum, const Shape& shape);
bool compare(const Shape& shape1, const Shape& shape2);

#endif 
