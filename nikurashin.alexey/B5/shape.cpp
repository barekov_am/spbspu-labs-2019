#include "shape.hpp"

#include <cmath>
#include <iostream>

std::ostream& operator<<(std::ostream& out, const Point& point)
{
  std::cout << "(" << point.x << ";" << point.y << ")";
  return out;
}

std::istream& operator>>(std::istream& instream, Shape& shape)
{
  std::size_t vertices = 0;
  instream >> vertices;
  if (vertices < 3)
  {
    throw std::invalid_argument("Wrong data");
  }

  for (std::size_t i = 0; i < vertices; i++)
  {
    char openBkt = 0;
    int x;
    char semicolon = 0;
    int y;
    char closeBkt = 0;

    instream >> openBkt >> x >> semicolon >> y >> closeBkt;

    if (openBkt != '(' || semicolon != ';' || closeBkt != ')' || instream.eof())
    {
      throw std::invalid_argument("Wrong data");
    }
    shape.push_back({ x, y });
  }
  return instream;
}

double Distance(const Point& p1, const Point& p2)
{
  return sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2));
}

bool Triangle(const Shape& shape)
{
  return (shape.size() == 3);
}

bool Rectangle(const Shape& shape)
{
  if (shape.size() != 4)
  {
    return false;
  }
  return (Distance(shape.at(0), shape.at(2)) == Distance(shape.at(1), shape.at(3)));
}

bool Square(const Shape& shape)
{
  if (!Rectangle(shape))
  {
    return false;
  }
  return (Distance(shape.at(0), shape.at(1)) == Distance(shape.at(1), shape.at(2)));
}

bool Pentagon(const Shape& shape)
{
  return (shape.size() == 5);
}

bool compare(const Shape& shape1, const Shape& shape2)
{
  if (shape1.size() != shape2.size())
  {
    return shape1.size() < shape2.size();
  }

  if (Rectangle(shape1) && Rectangle(shape2))
  {
    if (Square(shape1) && Square(shape2))
    {
      return false;
    }
    else if (Square(shape1) && Rectangle(shape2))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  return false;
}

int sumVertices(int sum, const Shape& shape)
{
  return sum += shape.size();
}
