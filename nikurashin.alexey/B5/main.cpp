#include <iostream>

void Task1();
void Task2();

int main(int argc, char* argv[]) {
  try
  {
    if (argc != 2)
    {
      std::cerr << "Invalid number of arguments!";
      return 1;
    }

    switch (atoi(argv[1]))
    {
    case 1:
      Task1();
      break;

    case 2:
      Task2();
      break;

    default:
      std::cerr << "Invalid task number!";
      return 1;
    }
  }
  catch (const std::exception & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }

  return 0;
}
