#include "access.hpp"
#include "print.hpp"

#include <iostream>
#include <vector>

void task3()
{
  std::vector<int> vector;

  int value = 0;
  while (std::cin >> value)
  {
    if (value == 0)
    {
      break;
    }
    vector.push_back(value);
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::ios_base::failure("Reading failed");
  }

  if (vector.empty())
  {
    return;
  }

  if (value != 0)
  {
    throw std::invalid_argument("Must be zero in the end");
  }

  auto it = vector.begin();
  switch (vector.back())
  {
  case 1:
  {
    while (it != vector.end())
    {
      it = ((*it) % 2 == 0) ? vector.erase(it) : ++it;
    }
    break;
  }
  case 2:
  {
    while (it != vector.end())
    {
      it = ((*it) % 3 == 0) ? vector.insert(it + 1, 3, 1) : ++it;
    }
    break;
  }
  default:
    break;
  }

  print(vector);

}
