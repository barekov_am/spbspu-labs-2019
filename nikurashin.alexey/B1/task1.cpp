#include "sort.hpp"
#include "print.hpp"

#include <forward_list>
#include <vector>


void task1(const char* order)
{
  checkOrder(order);
  std::vector<int> vectorBr;

  int num = 0;
  while (std::cin >> num)
  {
    vectorBr.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Fail while reading data.");
  }

  if (vectorBr.empty())
  {
    return;
  }

  std::vector<int> vectorAt = vectorBr;
  std::forward_list<int> listIterator(vectorBr.begin(), vectorBr.end());

  sorting(accessBr<std::vector<int>>(), vectorBr, order);
  print(vectorBr);
  sorting(accessAt<std::vector<int>>(), vectorAt, order);
  print(vectorAt);
  sorting(accessIter<std::forward_list<int>>(), listIterator, order);
  print(listIterator);
}
