#ifndef SORT_HPP
#define SORT_HPP

#include "access.hpp"

#include <functional>
#include <cstring>
#include <algorithm>

template <typename input>
void checkOrder(input order)
{
  if ((strcmp(order, "ascending") != 0) && (strcmp(order, "descending") != 0))
  {
    throw std::invalid_argument("Wrong argument");
  }
}

template <typename Element>
bool compare(Element& a, Element& b, const char* order)
{
  if (strcmp(order, "ascending") == 0)
  {
    return a > b;
  }
  else
  {
    return a < b;
  }
}

template <template <typename T> typename access, typename T>
void sorting(access<T> method, T& collections, const char* order)
{
  for (auto i = method.begin(collections); i != method.end(collections); i++)
  {
    for (auto j = i; j != method.end(collections); j++)
    {
      if (compare(method.get(collections, i), method.get(collections, j), order))
      {
        std::swap(method.get(collections, i), method.get(collections, j));
      }
    }
  }
}

#endif
