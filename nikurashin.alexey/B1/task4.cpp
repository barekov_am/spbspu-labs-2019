#include "sort.hpp"
#include "print.hpp"

#include <random>
#include <vector>


void fillRandom(double* array, int size)
{
  std::mt19937 rng(std::random_device{}());
  std::uniform_real_distribution<double> date(-1.0, 1.0);
  for (int i = 0; i < size; i++)
  {
    array[i] = date(rng);
  }
}


void task4(const char* order, int size)
{
  checkOrder(order);

  if (size < 0)
  {
    throw std::invalid_argument("Size of array must be positive");
  }

  if (!size)
  {
    throw std::invalid_argument("Size must be a number");
  }

  double* arr = new double[size];
  fillRandom(arr, size);

  std::vector<double> vector;
  for (int i = 0; i != size; i++)
  {
    vector.push_back(arr[i]);
  }

  delete[] arr;

  print(vector);
  sorting(accessAt<std::vector<double>>(), vector, order);
  print(vector);
}
