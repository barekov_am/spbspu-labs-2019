#ifndef ACCESS_HPP
#define ACCESS_HPP

#include <iostream>

template<typename T>
struct accessBr
{
  size_t begin(T&)
  {
    return 0;
  }

  size_t end(T& collections)
  {
    return collections.size();
  }

  typename T::value_type& get(T& collections, size_t index)
  {
    if (index > collections.size())
    {
      throw std::invalid_argument("Out of bounds array");
    }

    return collections[index];
  }
};

template <typename T>
struct accessAt
{
  size_t begin(T&)
  {
    return 0;
  }

  size_t end(T& collections)
  {
    return collections.size();
  }

  typename T::value_type& get(T& collections, size_t index)
  {
    return collections.at(index);
  }
};

template <typename T>
struct accessIter
{
  typename T::iterator begin(T& collections)
  {
    return collections.begin();
  }

  typename T::iterator end(T& collections)
  {
    return collections.end();
  }

  typename T::value_type& get(T&, typename T::iterator it)
  {
    return *it;
  }
};

#endif 
