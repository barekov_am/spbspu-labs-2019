#include"access.hpp"

#include <fstream> 
#include <iostream>
#include <memory>
#include <vector>

void task2(const char* filename)
{
  std::ifstream file(filename);

  if (!file)
  {
    throw std::ios_base::failure("No such file");
  }

  size_t length = 10;
  size_t size = 0;

  std::unique_ptr<char[], decltype(&free)> array(static_cast<char*>(malloc(length)), &free);

  while (file)
  {
    file.read(&array[size], length - size);
    size += file.gcount();
    if (size == length)
    {
      length *= 2;
      std::unique_ptr<char[], decltype(&free)> temp(static_cast<char*>(realloc(array.get(), length)), &free);
      if (!temp)
      {
        throw std::ios_base::failure("Failed to reallocate memory");
      }
      else
      {
        array.release();
        std::swap(array, temp);
      }
    }
  }
  file.close();

  std::vector<char> vector(&array[0], &array[size]);
  for (const auto& it : vector)
  {
    std::cout << it;
  }
}
