#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

void printInfo(const potapova::CompositeShape *shape)
{
  potapova::rectangle_t frameRect = shape->getFrameRect();
  std::cout << "Center of shape " << frameRect.pos.x << ", " << frameRect.pos.y << "\n";
  std::cout << "Frame rectangle: width - " << frameRect.width << ", height - " << frameRect.height << "\n";
  std::cout << "Area of shape - " << shape->getArea() << "\n";
}

int main()
{
  potapova::CompositeShape::p_shape p_circle = std::make_shared<potapova::Circle>(potapova::point_t { 4, 9 }, 4);
  potapova::CompositeShape::p_shape p_rectangle = std::make_shared<potapova::Rectangle>(potapova::point_t { 4, 9 }, 1, 3);
  potapova::CompositeShape compShape;
  compShape.add(p_rectangle);
  printInfo(&compShape);
  std::cout << "New composite shape after adding: " << "\n";
  compShape.add(p_circle);
  printInfo(&compShape);
  std::cout << "Composite shape after move in (5, 6)" << "\n";
  compShape.move({ 5, 6 });
  printInfo(&compShape);
  std::cout << "Composite shape after move on dx = 6, dy = -2" << "\n";
  compShape.move(6, -2);
  printInfo(&compShape);
  std::cout << "Composite shape after scale" << "\n";
  compShape.scale(3);
  printInfo(&compShape);
  std::cout << "Delete second elem: " << "\n";
  compShape.remove(1);
  printInfo(&compShape);

  return 0;
}
