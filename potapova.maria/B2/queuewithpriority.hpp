#ifndef B2_QUEUEWITHPRIORITY_HPP
#define B2_QUEUEWITHPRIORITY_HPP

#include <list>
#include <iostream>

template<typename T>
class QueueWithPriority
{
public:
  enum class ElementPriority
  {
    HIGH,
    NORMAL,
    LOW
  };

  void addToQueue(ElementPriority prirority, const T &element);
  T getElementFromQueue();
  void accelerate();
  bool empty();

private:
  std::list<T> highPriority_;
  std::list<T> normalPriority_;
  std::list<T> lowPriority_;
};

#endif // B2_QUEUEWITHPRIORITY_HPP
