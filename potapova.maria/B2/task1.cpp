#include <iostream>
#include <sstream>
#include <cstring>

#include "qwp-implementation.hpp"

void task1()
{
  QueueWithPriority<std::string> queue;
  std::string string;

  while (std::getline(std::cin, string))
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Can`t read file");
    }
    std::stringstream ss(string);

    std::string command;
    std::string data;
    std::string priority;

    ss >> command;

    if (command == "add")
    {
      ss >> priority >> data;

      if (!ss.eof())
      {
        std::getline(ss, string);
        data += string;
      }


      if ((!ss.eof()) || (data.empty()))
      {
        std::cout << "<INVALID COMMAND>\n";
        break;
      }


      if (priority == "low")
      {
        queue.addToQueue(QueueWithPriority<std::string>::ElementPriority::LOW, data);
      }

      else if (priority == "normal")
      {
        queue.addToQueue(QueueWithPriority<std::string>::ElementPriority::NORMAL, data);
      }

      else if (priority == "high")
      {
        queue.addToQueue(QueueWithPriority<std::string>::ElementPriority::HIGH, data);
      }

      else
      {
        std::cout << "<INVALID COMMAND>\n";
      }
    }
    else if (command == "get")
    {
      if (queue.empty())
      {
        std::cout << "<EMPTY>\n";
      }
      else
      {
        std::cout << queue.getElementFromQueue() << "\n";
      }
    }
    else if (command == "accelerate")
    {
      queue.accelerate();
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
