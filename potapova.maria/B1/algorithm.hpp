#ifndef ALGORITHM
#define ALGORITHM

#include <iterator>
#include <cstddef>

template <typename T>
struct SortOperator
{
  using index = std::size_t;

  static int begin(const T&)
  {
    return 0;
  }

  static int end(T& array)
  {
    return array.size();
  }

  static typename T::value_type& getIndex(T& array,index i)
  {
    return array[i];
  }
};

template <typename T>
struct SortAt
{
  using index = std::size_t;

  static int begin(T&)
  {
    return 0;
  }

  static int end(T& array)
  {
    return array.size();
  }

  static typename T::value_type& getIndex(T& array, index i)
  {
    return array.at(i);
  }
};

template <typename T>
struct SortIterator
{
  using index = typename T::iterator;

  static index begin(T& array)
  {
    return array.begin();
  }

  static index end(T& array)
  {
    return array.end();
  }

  static typename T::value_type& getIndex(T&, index& it)
  {
    return *it;
  }

};

#endif
