#include <random>
#include <vector>

#include "sort.hpp"

void fillRandom(double* array, int size)
{
  for (int i = 0; i < size; i++)
  {
    array[i] = (rand() % 21 - 10) / 10.0;
  }
}

void task4(const char* direction, int size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size of array must be more than 0!");
  }

  auto order = getDirection<double>(direction);

  std::vector<double> vector(static_cast<unsigned int>(size));
  fillRandom(&vector[0], size);
  print(vector);

  sort<SortOperator>(vector, order);

  print(vector);
}

