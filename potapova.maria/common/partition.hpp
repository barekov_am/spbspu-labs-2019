#ifndef PARTITION_H
#define PARTITION_H

#include "matrix.hpp"
#include "composite-shape.hpp"

using p_shape = std::shared_ptr<potapova::Shape>;

namespace potapova
{
  Matrix part(const CompositeShape &composite_shape);
  bool isOverlapping(p_shape shape1, p_shape shape2);
}

#endif // PARTITION_H
