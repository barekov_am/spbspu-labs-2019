#include "partition.hpp"
#include <cmath>

potapova::Matrix potapova::part(const CompositeShape &comp_shape)
{
  Matrix matrix;

  for (unsigned int i = 0; i < comp_shape.getSize(); ++i)
  {
    unsigned int line = 0;
    unsigned int column = 0;

    for (unsigned int j = 0; j < matrix.getLines(); ++j)
    {
      for (unsigned int k = 0; k < matrix.getColumns(); ++k)
      {
        if (matrix[j][k] == nullptr)
        {
          column = k;
          line = j;
          break;
        }

        if (isOverlapping(comp_shape[i], matrix[j][k]))
        {
          column = 0;
          line = j + 1;
          break;
        }

        column = k + 1;
        line = j;
      }

      if (line == j)
      {
        break;
      }
    }

    matrix.add(comp_shape[i], line, column);
  }

  return matrix;
}

bool potapova::isOverlapping(p_shape shape1, p_shape shape2)
{
  if ((shape1 == nullptr) || (shape2 == nullptr))
  {
    return false;
  }

  bool overlapping_x = fabs(shape1->getFrameRect().pos.x - shape2->getFrameRect().pos.x)
      < ((shape1->getFrameRect().width / 2) + (shape2->getFrameRect().width / 2));
  bool overlapping_y = fabs(shape1->getFrameRect().pos.y - shape2->getFrameRect().pos.y)
      < ((shape1->getFrameRect().height / 2) + (shape2->getFrameRect().height / 2));

  return overlapping_x && overlapping_y;
}

