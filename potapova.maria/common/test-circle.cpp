#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testForCircle)

BOOST_AUTO_TEST_CASE(methodsOfShape)
{
  potapova::Circle circle({ 1, 5 }, 4);
  const potapova::rectangle_t circleFrameBeforeMove = circle.getFrameRect();
  const double areaBefore = circle.getArea();

  circle.move(4, 4);
  potapova::rectangle_t circleFrameAfterMove = circle.getFrameRect();
  double areaAfter = circle.getArea();

  BOOST_CHECK_CLOSE(circleFrameBeforeMove.height, circleFrameAfterMove.height, ACCURACY);
  BOOST_CHECK_CLOSE(circleFrameBeforeMove.width, circleFrameAfterMove.width, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);

  circle.move({ 3, 5 });
  circleFrameAfterMove = circle.getFrameRect();
  areaAfter = circle.getArea();

  BOOST_CHECK_CLOSE(circleFrameBeforeMove.height, circleFrameAfterMove.height, ACCURACY);
  BOOST_CHECK_CLOSE(circleFrameBeforeMove.width, circleFrameAfterMove.width, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
}

BOOST_AUTO_TEST_CASE(squareChange)
{
  potapova::Circle circle({ 4, 5 }, 4);
  const double areaBeforeScale = circle.getArea();
  const double testScale  = 2;
  circle.scale(testScale);
  const double areaAfterScale = circle.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScale * testScale * testScale, areaAfterScale, ACCURACY);
}

BOOST_AUTO_TEST_CASE(circleAfterRotate)
{
  potapova::Circle circle({ 4, 5 }, 4);
  potapova::Circle circleBeforeRotate = circle;
  for (int i = 1; i < 5; i++)
  {
    circle.rotate(M_PI / 2);
  }
  potapova::Circle circleAfterRotate = circle;

  BOOST_CHECK_CLOSE(circleBeforeRotate.getCenter().x, circleAfterRotate.getCenter().x, ACCURACY);
  BOOST_CHECK_CLOSE(circleBeforeRotate.getCenter().y, circleAfterRotate.getCenter().y, ACCURACY);
  BOOST_CHECK_CLOSE(circleBeforeRotate.getArea(), circleAfterRotate.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(availabilityOfProcessingOfInvalidParameters)
{
  BOOST_CHECK_THROW(potapova::Circle circle({ 5, 8 }, -3), std::invalid_argument);
  potapova::Circle circle({ 1, 6 }, 3);
  BOOST_CHECK_THROW(circle.scale(-2), std::invalid_argument)
}

BOOST_AUTO_TEST_SUITE_END()
