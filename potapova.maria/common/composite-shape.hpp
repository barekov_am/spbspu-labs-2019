#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace potapova
{
  class CompositeShape : public Shape
  {
  public:
    using p_shape = std::shared_ptr<Shape>;

    CompositeShape();
    CompositeShape(const CompositeShape &rhs);
    CompositeShape(CompositeShape &&rhs);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &rhs);
    CompositeShape &operator =(CompositeShape &&rhs);
    p_shape operator [](unsigned int index) const;

    double getArea() const override;
    unsigned int getSize() const;
    void move(const point_t &center) override;
    void move(double dx, double dy) override;
    void scale(double coefficient) override;
    rectangle_t getFrameRect() const override;
    void add(p_shape shape);
    void remove(unsigned int index);
    void rotate(double angle) override;
    point_t getCenter() const override;
    void printInfo() const override;

  private:
    std::unique_ptr<p_shape[]> shape_array_;
    unsigned int count_;
  };
}
#endif // COMPOSITESHAPE_HPP
