#include "matrix.hpp"

#include <stdexcept>
#include <iostream>
#include <cmath>

potapova::Matrix::Matrix() :
  lines_(0),
  columns_(0),
  list_()
{
}

potapova::Matrix::Matrix(const Matrix &rhs) :
  lines_(rhs.getLines()),
  columns_(rhs.getColumns()),
  list_(std::make_unique<p_shape[]>(rhs.lines_ * rhs.columns_))
{
  for  (unsigned int i = 0; i < (lines_ * columns_); ++i)
  {
    list_[i] = rhs.list_[i];
  }
}

potapova::Matrix::Matrix(Matrix &&rhs) :
  lines_(rhs.lines_),
  columns_(rhs.columns_),
  list_(std::move(rhs.list_))
{
  rhs.lines_ = 0;
  rhs.columns_ = 0;
}

potapova::Matrix &potapova::Matrix::operator =(const Matrix &rhs)
{
  if (this != &rhs)
  {
    std::unique_ptr<p_shape[]> new_list(std::make_unique<p_shape[]>(rhs.lines_ * rhs.columns_));
    lines_ = rhs.lines_;
    columns_ = rhs.columns_;
    for (unsigned int i = 0; i < (lines_ * columns_); ++i)
    {
      new_list[i] = rhs.list_[i];
    }
    list_.swap(new_list);
  }
  return *this;
}

potapova::Matrix &potapova::Matrix::operator =(Matrix &&rhs)
{
  if (this != &rhs)
  {
    lines_ = rhs.lines_;
    columns_ = rhs.columns_;
    list_ = std::move(rhs.list_);
    rhs.lines_ = 0;
    rhs.columns_ = 0;
  }
  return *this;
}

std::unique_ptr<potapova::Matrix::p_shape[]> potapova::Matrix::operator[](unsigned int index) const
{
  if (index >= lines_)
  {
    throw std::out_of_range("Index must be less");
  }

  std::unique_ptr<p_shape[]> new_shape_matrix(std::make_unique<p_shape[]>(columns_));

  for (unsigned int i = 0; i < columns_; ++i)
  {
    new_shape_matrix[i] = list_[index * columns_ + i];
  }

  return new_shape_matrix;
}

bool potapova::Matrix::operator ==(const Matrix &rhs) const
{
  if ((lines_ != rhs.lines_) || (columns_ != rhs.columns_))
  {
    return false;
  }

  for (unsigned int i = 0; i < (lines_ * columns_); ++i)
  {
    if (list_[i] != rhs.list_[i])
    {
      return false;
    }
  }
  return true;
}

bool potapova::Matrix::operator !=(const Matrix &rhs) const
{
  if (this == &rhs)
  {
    return false;
  }
  return true;
}

void potapova::Matrix::add(p_shape shape, const unsigned int line, const unsigned int column)
{
  unsigned int lines = (lines_ == line) ? (lines_ + 1) : (lines_);
  unsigned int columns = (columns_ == column) ? (columns_ + 1) : (columns_);

  std::unique_ptr<p_shape[]> new_matrix(std::make_unique<p_shape[]>(lines * columns));
  for (unsigned int i = 0; i < lines; ++i)
  {
    for (unsigned int j = 0; j < columns; ++j)
    {
      if ((lines_ == i) || (columns_ == j))
      {
        new_matrix[i * columns + j] = nullptr;
      }
      else
      {
        new_matrix[i * columns + j] = list_[i * columns_ + j];
      }
    }
  }

  new_matrix[line * columns + column] = shape;
  list_.swap(new_matrix);
  lines_ = lines;
  columns_ = columns;
}

unsigned int potapova::Matrix::getLines() const
{
  return lines_;
}

unsigned int potapova::Matrix::getColumns() const
{
  return columns_;
}

void potapova::Matrix::writeInfo() const
{
  std::cout << "In matrix exist " << lines_ << " lines and "
            << columns_ << " columns" << std::endl;
  for (unsigned int i = 0; i < lines_; ++i)
  {
    for (unsigned int j = 0; j < columns_; ++j)
    {
      if (list_[i * columns_ + j] != nullptr)
      {
        std::cout << "On " << i + 1 << " layer, "
                  << j + 1 << " position is : " << std::endl;
        list_[i * columns_ + j]->printInfo();
        std::cout << std::endl << "*********************************" << std::endl << std::endl;
      }
    }
  }
}


