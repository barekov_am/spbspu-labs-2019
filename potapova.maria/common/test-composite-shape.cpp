#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

const double ACCURACY = 0.01;

using p_shape = std::shared_ptr<potapova::Shape>;

BOOST_AUTO_TEST_SUITE(testForShapes)

BOOST_AUTO_TEST_CASE(checkConstantParametrs)
{
  p_shape p_rectangle = std::make_shared<potapova::Rectangle>(potapova::point_t { 3, 5 }, 3, 10);
  p_shape p_circle = std::make_shared<potapova::Circle>(potapova::point_t { 1, 5 }, 4);

  potapova::CompositeShape compShape;

  compShape.add(p_circle);
  compShape.add(p_rectangle);
  const double areaBeforeMoving = compShape.getArea();
  const potapova::rectangle_t frameRect = compShape.getFrameRect();

  compShape.move({ 3, 6 });
  potapova::rectangle_t frameRectBefore = compShape.getFrameRect();
  double areaAfterMovingIn = compShape.getArea();

  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMovingIn, ACCURACY);
  BOOST_CHECK_CLOSE(frameRect.height, frameRectBefore.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRect.width, frameRectBefore.width, ACCURACY);

  compShape.move(3, 6);
  potapova::rectangle_t frameRectBeforeOn = compShape.getFrameRect();
  double areaAfterMovingOn = compShape.getArea();

  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMovingOn, ACCURACY);
  BOOST_CHECK_CLOSE(frameRect.height, frameRectBeforeOn.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRect.width, frameRectBeforeOn.width, ACCURACY);
}

BOOST_AUTO_TEST_CASE(checkCopyConstructor)
{
  p_shape p_rectangle = std::make_shared<potapova::Rectangle>(potapova::point_t { 3, 5 }, 3, 10);
  p_shape p_circle = std::make_shared<potapova::Circle>(potapova::point_t { 1, 5 }, 4);

  potapova::CompositeShape compShape;

  compShape.add(p_circle);
  compShape.add(p_rectangle);

  potapova::CompositeShape testShape(compShape);
  BOOST_CHECK_EQUAL(testShape.getSize(), compShape.getSize());
  BOOST_CHECK_CLOSE(compShape.getArea(), testShape.getArea(), ACCURACY);

  potapova::Rectangle rectangle1({{ 1, 1 }, 5, 6});
  p_shape p_rectangle1 = std::make_shared<potapova::Rectangle>(rectangle1);
  testShape.add(p_rectangle1);
  BOOST_CHECK_EQUAL(testShape.getSize(), compShape.getSize() + 1);
}

BOOST_AUTO_TEST_CASE(checkCopyOperator)
{
  p_shape p_rectangle = std::make_shared<potapova::Rectangle>(potapova::point_t { 3, 5 }, 3, 10);
  p_shape p_circle = std::make_shared<potapova::Circle>(potapova::point_t { 1, 5 }, 4);

  potapova::CompositeShape compShape;

  compShape.add(p_circle);
  compShape.add(p_rectangle);

  potapova::CompositeShape testShape;
  compShape = testShape;
  BOOST_CHECK_EQUAL(testShape.getSize(), compShape.getSize());
  BOOST_CHECK_CLOSE(compShape.getArea(), testShape.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(checkMoveConstructor)
{
  p_shape p_rectangle = std::make_shared<potapova::Rectangle>(potapova::point_t { 3, 5 }, 3, 10);
  p_shape p_circle = std::make_shared<potapova::Circle>(potapova::point_t { 1, 5 }, 4);

  potapova::CompositeShape compShape;

  compShape.add(p_circle);
  compShape.add(p_rectangle);

  unsigned int countBeforeMove = compShape.getSize();

  potapova::CompositeShape testShape = std::move(compShape);

  BOOST_CHECK_EQUAL(testShape.getSize(), countBeforeMove);
  BOOST_CHECK_EQUAL(compShape.getSize(), 0);
}

BOOST_AUTO_TEST_CASE(checkMoveOperator)
{
  p_shape p_rectangle = std::make_shared<potapova::Rectangle>(potapova::point_t { 3, 5 }, 3, 10);
  p_shape p_circle = std::make_shared<potapova::Circle>(potapova::point_t { 1, 5 }, 4);

  potapova::CompositeShape compShape;

  compShape.add(p_circle);
  compShape.add(p_rectangle);

  unsigned int countBeforeMove = compShape.getSize();
  potapova::Rectangle rectan({{ 4, 9 }, 1, 8});
  p_shape p_rectan = std::make_shared<potapova::Rectangle>(rectan);

  potapova::CompositeShape testShape;

  testShape.add(p_rectan);
  testShape = std::move(compShape);

  BOOST_CHECK_EQUAL(testShape.getSize(), countBeforeMove);
  BOOST_CHECK_EQUAL(compShape.getSize(), 0);
}

BOOST_AUTO_TEST_CASE(checkScaleCompositeShapeMoreThanOne)
{
  p_shape p_rectangle = std::make_shared<potapova::Rectangle>(potapova::point_t { 3, 5 }, 3, 10);
  p_shape p_circle = std::make_shared<potapova::Circle>(potapova::point_t { 1, 5 }, 4);

  potapova::CompositeShape compShape;

  compShape.add(p_circle);
  compShape.add(p_rectangle);

  const double areaBeforeScale = compShape.getArea();
  const double coef = 2;

  compShape.scale(coef);
  double areaAfterScale = compShape.getArea();

  BOOST_CHECK_CLOSE(areaBeforeScale * coef * coef, areaAfterScale, ACCURACY);
}

BOOST_AUTO_TEST_CASE(checkScaleCompositeShapeLessThanOne)
{
  p_shape p_rectangle = std::make_shared<potapova::Rectangle>(potapova::point_t { 3, 5 }, 3, 10);
  p_shape p_circle = std::make_shared<potapova::Circle>(potapova::point_t { 1, 5 }, 4);

  potapova::CompositeShape compShape;

  compShape.add(p_circle);
  compShape.add(p_rectangle);

  const double areaBeforeScale = compShape.getArea();
  const double coef = 0.4;

  compShape.scale(coef);
  double areaAfterScale = compShape.getArea();

  BOOST_CHECK_CLOSE(areaBeforeScale * coef * coef, areaAfterScale, ACCURACY);
}

BOOST_AUTO_TEST_CASE(checkCompositeShapeAfterAddAndDelete)
{
  p_shape p_rectangle = std::make_shared<potapova::Rectangle>(potapova::point_t { 3, 5 }, 3, 10);
  p_shape p_circle = std::make_shared<potapova::Circle>(potapova::point_t { 1, 5 }, 4);

  potapova::CompositeShape compShape;

  compShape.add(p_rectangle);
  const double areaBeforeAdd = compShape.getArea();
  const double circleArea = p_circle->getArea();

  compShape.add(p_circle);
  double areaAfterAdd = compShape.getArea();
  BOOST_CHECK_CLOSE(areaBeforeAdd + circleArea, areaAfterAdd, ACCURACY);

  compShape.remove(1);
  double areaAfterDelete = compShape.getArea();

  BOOST_CHECK_CLOSE(areaAfterAdd - circleArea, areaAfterDelete, ACCURACY);
}

BOOST_AUTO_TEST_CASE(checkIncorrectParameters)
{
  p_shape p_rectangle = std::make_shared<potapova::Rectangle>(potapova::point_t { 3, 5 }, 3, 10);
  p_shape p_circle = std::make_shared<potapova::Circle>(potapova::point_t { 1, 5 }, 4);

  potapova::CompositeShape compShape;

  compShape.add(p_circle);
  compShape.add(p_rectangle);

  BOOST_CHECK_THROW(compShape.remove(4), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(emptyCompositeShapeThrowingExeptions)
{
  potapova::CompositeShape empComp;

  BOOST_CHECK_THROW(empComp.move(1, 4), std::logic_error);
  BOOST_CHECK_THROW(empComp.move({7, 9}), std::logic_error);
  BOOST_CHECK_THROW(empComp.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(empComp.scale(8), std::logic_error);
}

BOOST_AUTO_TEST_CASE(checkRotateForCompositeShape)
{
  p_shape p_rectangle1 = std::make_shared<potapova::Rectangle>(potapova::point_t { 1, 1 }, 2, 2);
  p_shape p_rectangle2 = std::make_shared<potapova::Rectangle>(potapova::point_t { 3, 1 }, 2, 2);
  p_shape p_rectangle3 = std::make_shared<potapova::Rectangle>(potapova::point_t { 3, 3 }, 2, 2);
  p_shape p_rectangle4 = std::make_shared<potapova::Rectangle>(potapova::point_t { 1, 3 }, 2, 2);
  potapova::CompositeShape compShape;
  compShape.add(p_rectangle1);
  compShape.add(p_rectangle2);
  compShape.add(p_rectangle3);
  compShape.add(p_rectangle4);

  const double widthBefore = compShape.getFrameRect().width;
  const double heightBefore = compShape.getFrameRect().height;
  std::cout << widthBefore << " " << heightBefore << std::endl;
  compShape.rotate(45);
  BOOST_CHECK_CLOSE(compShape.getFrameRect().width, widthBefore * sqrt(2), ACCURACY);
  BOOST_CHECK_CLOSE(compShape.getFrameRect().height, heightBefore * sqrt(2), ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeExceptionThrow)
{
  p_shape p_rectangle = std::make_shared<potapova::Rectangle>(potapova::point_t { 3, 5 }, 3, 10);
  p_shape p_circle = std::make_shared<potapova::Circle>(potapova::point_t { 1, 5 }, 4);

  potapova::CompositeShape compShape;

  compShape.add(p_circle);
  compShape.add(p_rectangle);
  BOOST_CHECK_THROW(compShape.scale(-1), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(compShape[2], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeShapeThrowingExeptions)
{
  p_shape p_rectangle = std::make_shared<potapova::Rectangle>(potapova::point_t { 3, 5 }, 3, 10);
  p_shape p_circle = std::make_shared<potapova::Circle>(potapova::point_t { 1, 5 }, 4);

  potapova::CompositeShape compShape;

  compShape.add(p_circle);
  compShape.add(p_rectangle);

  BOOST_CHECK_THROW(compShape.remove(10), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.remove(-10), std::invalid_argument);

  BOOST_CHECK_THROW(compShape[2], std::out_of_range);
  BOOST_CHECK_THROW(compShape[-2], std::out_of_range);

  compShape.remove(1);
  BOOST_CHECK_THROW(compShape[1], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
