#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

using p_shape = std::shared_ptr<potapova::Shape>;

BOOST_AUTO_TEST_SUITE(testForMatrix)

BOOST_AUTO_TEST_CASE(quantityOfLines)
{
  p_shape p_circle = std::make_shared<potapova::Circle>(potapova::point_t { 0, 0 }, 6);
  p_shape p_rectangle = std::make_shared<potapova::Rectangle>(potapova::point_t { 0, 0 }, 4, 10);

  unsigned int quantity_lines = 2;

  potapova::CompositeShape compShape;
  compShape.add(p_rectangle);
  compShape.add(p_circle);

  potapova::Matrix matrix = potapova::part(compShape);


  BOOST_CHECK_EQUAL(quantity_lines, matrix.getLines());
}

BOOST_AUTO_TEST_CASE(quantityOfColumns)
{
  p_shape p_circle = std::make_shared<potapova::Circle>(potapova::point_t {-1000, -1000}, 1);
  p_shape p_rectangle1 = std::make_shared<potapova::Rectangle>(potapova::point_t {1000, 1000}, 1, 1);
  p_shape p_rectangle2 = std::make_shared<potapova::Rectangle>(potapova::point_t {-1000, 1000}, 1, 1);

  const unsigned int quantity_columns = 3;

  potapova::CompositeShape compShape;
  compShape.add(p_rectangle1);
  compShape.add(p_rectangle2);
  compShape.add(p_circle);

  potapova::Matrix matrix = potapova::part(compShape);

  BOOST_CHECK_EQUAL(quantity_columns, matrix.getColumns());
}

BOOST_AUTO_TEST_CASE(extendsColumnsAndLines)
{
  p_shape p_circle1 = std::make_shared<potapova::Circle>(potapova::point_t { 100, 100 }, 4);
  p_shape p_circle2 = std::make_shared<potapova::Circle>(potapova::point_t { -1100, 100 }, 7);
  p_shape p_rectangle1 = std::make_shared<potapova::Rectangle>(potapova::point_t { 1000,1000 }, 6, 8);
  p_shape p_rectangle2 = std::make_shared<potapova::Rectangle>(potapova::point_t { 100, 100 }, 5, 10);

  const unsigned int lines = 2;
  const unsigned int columns = 3;

  potapova::CompositeShape compShape;
  compShape.add(p_circle2);
  compShape.add(p_rectangle1);
  compShape.add(p_circle1);
  compShape.add(p_rectangle2);

  potapova::Matrix matrix = potapova::part(compShape);

  BOOST_CHECK_EQUAL(lines, matrix.getLines());
  BOOST_CHECK_EQUAL(columns, matrix.getColumns());
}
BOOST_AUTO_TEST_CASE(throwExceptionForMatrix)
{
  p_shape p_circle1 = std::make_shared<potapova::Circle>(potapova::point_t { 100, 100 }, 4);
  p_shape p_circle2 = std::make_shared<potapova::Circle>(potapova::point_t { -1100, 100 }, 7);
  p_shape p_rectangle1 = std::make_shared<potapova::Rectangle>(potapova::point_t { 1000,1000 }, 6, 8);
  p_shape p_rectangle2 = std::make_shared<potapova::Rectangle>(potapova::point_t { 100, 100 }, 5, 10);

  potapova::CompositeShape compShape;
  compShape.add(p_circle2);
  compShape.add(p_rectangle1);
  compShape.add(p_circle1);
  compShape.add(p_rectangle2);

  potapova::Matrix matrix = potapova::part(compShape);

  BOOST_CHECK_THROW(matrix[matrix.getLines() + 7][matrix.getColumns() + 5], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(copyingAndMovingConstructor)
{
  p_shape p_circle1 = std::make_shared<potapova::Circle>(potapova::point_t { 100, 100 }, 4);
  p_shape p_circle2 = std::make_shared<potapova::Circle>(potapova::point_t { -1100, 100 }, 7);
  p_shape p_rectangle1 = std::make_shared<potapova::Rectangle>(potapova::point_t { 1000,1000 }, 6, 8);
  p_shape p_rectangle2 = std::make_shared<potapova::Rectangle>(potapova::point_t { 100, 100 }, 5, 10);

  potapova::CompositeShape compShape;
  compShape.add(p_circle2);
  compShape.add(p_rectangle1);
  compShape.add(p_circle1);
  compShape.add(p_rectangle2);

  potapova::Matrix matrix = potapova::part(compShape);

  potapova::Matrix matrix_copying(matrix);

  BOOST_CHECK_EQUAL(matrix_copying.getLines(), matrix.getLines());
  BOOST_CHECK_EQUAL(matrix_copying.getColumns(), matrix.getColumns());
  BOOST_CHECK(matrix == matrix_copying);

  potapova::Matrix matrix_moving(std::move(matrix));

  BOOST_CHECK_EQUAL(matrix_copying.getLines(), matrix_moving.getLines());
  BOOST_CHECK_EQUAL(matrix_copying.getColumns(), matrix_moving.getColumns());
  BOOST_CHECK(matrix_moving == matrix_copying);
}

BOOST_AUTO_TEST_CASE(copyingOperator)
{
  p_shape p_circle1 = std::make_shared<potapova::Circle>(potapova::point_t { 100, 100 }, 4);
  p_shape p_circle2 = std::make_shared<potapova::Circle>(potapova::point_t { -1100, 100 }, 7);
  p_shape p_rectangle1 = std::make_shared<potapova::Rectangle>(potapova::point_t { 1000,1000 }, 6, 8);
  p_shape p_rectangle2 = std::make_shared<potapova::Rectangle>(potapova::point_t { 100, 100 }, 5, 10);

  potapova::CompositeShape compShape;
  compShape.add(p_circle2);
  compShape.add(p_rectangle1);
  compShape.add(p_circle1);
  compShape.add(p_rectangle2);

  potapova::Matrix matrix = potapova::part(compShape);

  BOOST_CHECK(matrix[0][0] == p_circle2);
  BOOST_CHECK(matrix[0][1] == p_rectangle1);
  BOOST_CHECK(matrix[0][2] == p_circle1);
  BOOST_CHECK(matrix[1][0] == p_rectangle2);
  BOOST_CHECK(matrix[0][0] != p_circle1);

  potapova::CompositeShape compShape1;
  potapova::CompositeShape compShape2;

  compShape1.add(p_circle2);
  compShape1.add(p_rectangle1);
  compShape1.add(p_circle1);
  compShape1.add(p_rectangle2);

  potapova::Matrix matrix1 = potapova::part(compShape1);
  potapova::Matrix matrix2 = potapova::part(compShape2);
  matrix1 = matrix2;
  BOOST_CHECK_EQUAL(matrix1.getColumns(), matrix2.getColumns());
  BOOST_CHECK_EQUAL(matrix1.getLines(), matrix2.getLines());

}

BOOST_AUTO_TEST_CASE(movingOperator)
{
  p_shape p_circle1 = std::make_shared<potapova::Circle>(potapova::point_t { 100, 100 }, 4);
  p_shape p_circle2 = std::make_shared<potapova::Circle>(potapova::point_t { -1100, 100 }, 7);
  p_shape p_rectangle1 = std::make_shared<potapova::Rectangle>(potapova::point_t { 1000,1000 }, 6, 8);
  p_shape p_rectangle2 = std::make_shared<potapova::Rectangle>(potapova::point_t { 100, 100 }, 5, 10);

  potapova::CompositeShape compShape;
  compShape.add(p_circle2);
  compShape.add(p_rectangle1);
  compShape.add(p_circle1);
  compShape.add(p_rectangle2);

  potapova::Matrix matrix1 = potapova::part(compShape);
  potapova::Matrix matrix2;

  matrix1 = std::move(matrix2);

  BOOST_CHECK_EQUAL(matrix1.getColumns(), 0);
  BOOST_CHECK_EQUAL(matrix1.getLines(), 0);
}

BOOST_AUTO_TEST_CASE(comparingOperator)
{
  p_shape p_circle1 = std::make_shared<potapova::Circle>(potapova::point_t { 100, 100 }, 4);
  p_shape p_circle2 = std::make_shared<potapova::Circle>(potapova::point_t { -1100, 100 }, 7);
  p_shape p_rectangle1 = std::make_shared<potapova::Rectangle>(potapova::point_t { 1000,1000 }, 6, 8);
  p_shape p_rectangle2 = std::make_shared<potapova::Rectangle>(potapova::point_t { 100, 100 }, 5, 10);

  potapova::CompositeShape compShape;
  compShape.add(p_circle2);
  compShape.add(p_rectangle1);
  compShape.add(p_circle1);

  potapova::Matrix matrix1 = potapova::part(compShape);
  potapova::Matrix matrix2 = potapova::part(compShape);
  potapova::CompositeShape compShape1;

  compShape.add(p_rectangle2);
  potapova::Matrix matrix3 = potapova::part(compShape1);
  BOOST_CHECK(matrix1 == matrix2);
  BOOST_CHECK(matrix1 != matrix3);
}
BOOST_AUTO_TEST_SUITE_END()
