#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"


namespace potapova
{
  class Matrix
  {
  public:
    using p_shape = std::shared_ptr<Shape>;

    Matrix();
    Matrix(const Matrix &rhs);
    Matrix(Matrix &&rhs);
    ~Matrix() = default;

    Matrix &operator =(const Matrix &rhs);
    Matrix &operator =(Matrix &&rhs);

    std::unique_ptr<p_shape[]> operator [](unsigned int index) const;
    bool operator ==(const Matrix &rhs) const;
    bool operator !=(const Matrix &rhs) const;

    void add(p_shape shape, const unsigned int line, const unsigned int column);

    unsigned int getLines() const;
    unsigned int getColumns() const;

    void writeInfo() const;
  private:
    unsigned int lines_;
    unsigned int columns_;
    std::unique_ptr<p_shape[]> list_;
  };
}
#endif // MATRIX_HPP
