#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testForRectangle)

BOOST_AUTO_TEST_CASE(methodsOfShape)
{
  potapova::Rectangle rectangle({{ 3, 5 }, 3, 10});
  const potapova::rectangle_t rectangleFrameBeforeMove = rectangle.getFrameRect();
  const double areaBefore = rectangle.getArea();

  rectangle.move(1, 4);

  potapova::rectangle_t rectangleFrameAfterMove = rectangle.getFrameRect();
  double areaAfter = rectangle.getArea();

  BOOST_CHECK_CLOSE(rectangleFrameBeforeMove.height, rectangleFrameAfterMove.height, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleFrameBeforeMove.width, rectangleFrameAfterMove.width, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);

  rectangle.move({ 1, 6 });
  rectangleFrameAfterMove = rectangle.getFrameRect();
  areaAfter = rectangle.getArea();

  BOOST_CHECK_CLOSE(rectangleFrameBeforeMove.height, rectangleFrameAfterMove.height, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleFrameBeforeMove.width, rectangleFrameAfterMove.width, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
}

BOOST_AUTO_TEST_CASE(squareChange)
{
  potapova::Rectangle rectangle({{ 1, 5 }, 4, 3});
  const double areaBeforeScale = rectangle.getArea();
  const double testScale  = 3;
  rectangle.scale(testScale);
  const double areaAfterScale = rectangle.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScale * testScale * testScale, areaAfterScale, ACCURACY);
}

BOOST_AUTO_TEST_CASE(rectangleAfterRotate)
{
  potapova::Rectangle rectangle({{ 4, 5 }, 3 , 3});
  potapova::Rectangle rectangleBeforeRotate = rectangle;
  const double widthBefore = rectangle.getFrameRect().width;
  const double heightBefore = rectangle.getFrameRect().height;
  for (int i = 1; i < 5; i++)
  {
    rectangle.rotate(90);
  }
  potapova::Rectangle rectangleAfterRotate = rectangle;

  BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, rectangleBeforeRotate.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, rectangleBeforeRotate.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeRotate.getCenter().x, rectangleAfterRotate.getCenter().x, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeRotate.getCenter().x, rectangleAfterRotate.getCenter().x, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeRotate.getArea(), rectangleAfterRotate.getArea(), ACCURACY);

  rectangleAfterRotate.rotate(45);
  BOOST_CHECK_CLOSE(widthBefore * sqrt(2), rectangleAfterRotate.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(heightBefore * sqrt(2), rectangleAfterRotate.getFrameRect().height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(availabilityOfProcessingOfInvalidParameters)
{
  BOOST_CHECK_THROW(potapova::Rectangle rectangle({{ 1, 6 }, 5, -3}), std::invalid_argument);
  potapova::Rectangle rectangle({{ 1, 6 }, 3, 5});
  BOOST_CHECK_THROW(rectangle.scale(-3), std::invalid_argument)
}

BOOST_AUTO_TEST_SUITE_END()
