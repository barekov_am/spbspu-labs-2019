#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

using p_shape = std::shared_ptr<potapova::Shape>;

BOOST_AUTO_TEST_SUITE(testForPartition)

BOOST_AUTO_TEST_CASE(checkPartition)
{
  p_shape p_circle1 = std::make_shared<potapova::Circle>(potapova::point_t { 100, 100 }, 4);
  p_shape p_circle2 = std::make_shared<potapova::Circle>(potapova::point_t { -1100, 100 }, 7);
  p_shape p_rectangle1 = std::make_shared<potapova::Rectangle>(potapova::point_t { 1000,1000 }, 6, 8);
  p_shape p_rectangle2 = std::make_shared<potapova::Rectangle>(potapova::point_t { 100, 100 }, 5, 10);

  potapova::CompositeShape compShape;
  compShape.add(p_circle2);
  compShape.add(p_rectangle1);
  compShape.add(p_circle1);
  compShape.add(p_rectangle2);

  potapova::Matrix matrix = potapova::part(compShape);


  BOOST_CHECK(matrix[0][0] == p_circle2);
  BOOST_CHECK(matrix[0][1] == p_rectangle1);
  BOOST_CHECK(matrix[0][2] == p_circle1);
  BOOST_CHECK(matrix[1][0] == p_rectangle2);
  BOOST_CHECK(matrix[0][0] != p_circle1);
}

BOOST_AUTO_TEST_SUITE_END()
