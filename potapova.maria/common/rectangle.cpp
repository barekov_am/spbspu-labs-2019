#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

potapova::Rectangle::Rectangle(const point_t &center, double width, double height) :
  pos_(center),
  height_(height),
  width_(width),
  rotationAngle_(0)
{
  if ((width <= 0) || (height <= 0))
  {
    throw std::invalid_argument("Width and height must be more than 0");
  }
}

double potapova::Rectangle::getArea() const
{
  return width_ * height_;
}

void potapova::Rectangle::move(const point_t &center)
{
  pos_ = center;
}

void potapova::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

potapova::rectangle_t potapova::Rectangle::getFrameRect() const
{
  const double cosA = std::cos((2 * M_PI * rotationAngle_) / 360);
  const double sinA = std::sin((2 * M_PI * rotationAngle_) / 360);
  const double width = width_ * std::fabs(cosA) + height_ * std::fabs(sinA);
  const double height = height_ * std::fabs(cosA) + width_ * std::fabs(sinA);
  return {pos_, width, height};

}

void potapova::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Multiplier must be more than 0");
  }
  width_ *= coefficient;
  height_ *= coefficient;
}

void potapova::Rectangle::rotate(double angle)
{
  rotationAngle_ += angle;

  while (std::abs(rotationAngle_) >= 360)
  {
    rotationAngle_ = (angle > 0) ? rotationAngle_ - 360 : rotationAngle_ + 360;
  }
}

potapova::point_t potapova::Rectangle::getCenter() const
{
  return pos_;
}

void potapova::Rectangle::printInfo() const
{
  std::cout << "Rectangle info:\n"
            << "Width - " << width_ << "\n"
            << "Height - " << height_ << "\n"
            << "Center position - x = " << pos_.x << ", y = " << pos_.y << "\n"
            << "Area - " << getArea() << "\n\n";
}
