#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

potapova::CompositeShape::CompositeShape() :
  count_(0)
{
}

potapova::CompositeShape::CompositeShape(const CompositeShape &rhs) :
  shape_array_(std::make_unique<p_shape[]>(rhs.count_)),
  count_(rhs.count_)
{
  for (unsigned int i = 0; i < count_; i++)
  {
    shape_array_[i] = rhs.shape_array_[i];
  }
}


potapova::CompositeShape::CompositeShape(CompositeShape &&rhs) :
  shape_array_(std::move(rhs.shape_array_)),
  count_(rhs.count_)
{
  rhs.count_ = 0;
}

potapova::CompositeShape &potapova::CompositeShape::operator =(const CompositeShape &rhs)
{
  if (this != &rhs)
  {
    std::unique_ptr<p_shape[]>new_shape_array_(std::make_unique<p_shape[]>(rhs.count_));
    count_ = rhs.count_;
    if (count_ != 0)
    {
      for (unsigned int i = 0; i < count_; i++)
      {
        new_shape_array_[i] = rhs.shape_array_[i];
      }
      shape_array_.swap(new_shape_array_);
    }
  }
  return *this;
}

potapova::CompositeShape &potapova::CompositeShape::operator =(CompositeShape &&rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    shape_array_ = std::move(rhs.shape_array_);
    rhs.count_ = 0;
    rhs.shape_array_ = nullptr;
  }
  return *this;
}

potapova::CompositeShape::p_shape potapova::CompositeShape::operator [](unsigned int index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }
  return shape_array_[index];
}

double potapova::CompositeShape::getArea() const
{
  double area = 0;
  for (unsigned int i = 0; i < count_; i++)
  {
    area += shape_array_[i]->getArea();
  }
  return area;
}

unsigned int potapova::CompositeShape::getSize() const
{
  return count_;
}

void potapova::CompositeShape::move(const point_t &center)
{
  rectangle_t rectFrame = getFrameRect();
  double dx = center.x - rectFrame.pos.x;
  double dy = center.y - rectFrame.pos.y;

  move(dx, dy);
}

void potapova::CompositeShape::move(double dx, double dy)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  for (unsigned int i = 0; i < count_; i++)
  {
    shape_array_[i]->move(dx, dy);
  }
}

void potapova::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient must be more than 0");
  }
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  potapova::point_t rectFrame = getFrameRect().pos;
  for (unsigned int i = 0; i < count_; i++)
  {
    potapova::point_t shapeCenter = shape_array_[i]->getFrameRect().pos;
    double x = (shapeCenter.x - rectFrame.x) * (coefficient - 1);
    double y = (shapeCenter.y - rectFrame.y) * (coefficient - 1);
    shape_array_[i]->move(x, y);
    shape_array_[i]->scale(coefficient);
  }
}

potapova::rectangle_t potapova::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  rectangle_t tmpRectangle = shape_array_[0]->getFrameRect();
  double minX = tmpRectangle.pos.x - tmpRectangle.width / 2;
  double minY = tmpRectangle.pos.y - tmpRectangle.height / 2;
  double maxX = tmpRectangle.pos.x + tmpRectangle.width / 2;
  double maxY = tmpRectangle.pos.y + tmpRectangle.height / 2;

  for (unsigned int i = 1; i < count_; i++)
  {
    tmpRectangle = shape_array_[i]->getFrameRect();

    double tmp = tmpRectangle.pos.x - tmpRectangle.width / 2;
    minX = std::min(tmp, minX);
    tmp = tmpRectangle.pos.y - tmpRectangle.height / 2;
    minY = std::min(tmp, minY);
    tmp = tmpRectangle.pos.x + tmpRectangle.width / 2;
    maxX = std::max(tmp, maxX);
    tmp = tmpRectangle.pos.y + tmpRectangle.height / 2;
    maxY = std::max(tmp, maxY);
  }
  rectangle_t rect{{(minX + maxX) / 2, (minY + maxY) / 2}, (maxY - minY), (maxX - minX)};
  return rect;
}


void potapova::CompositeShape::add(p_shape shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape is nullptr");
  }

  std::unique_ptr<p_shape[]> new_shape_array_(std::make_unique<p_shape[]>(count_ + 1));

  for (unsigned int i = 0; i < count_; ++i)
  {
    new_shape_array_[i] = shape_array_[i];
  }
  new_shape_array_[count_] = shape;
  count_++;
  shape_array_.swap(new_shape_array_);
}

void potapova::CompositeShape::remove(unsigned int index)
{
  if (index >= count_)
  {
    throw std::invalid_argument("Ivalid index");
  }

  std::unique_ptr<p_shape[]> new_shape_array_(std::make_unique<p_shape[]>(count_ - 1));

  for (unsigned int i = 0; i < index; ++i)
  {
    new_shape_array_[i] = shape_array_[i];
  }

  count_--;

  for (unsigned int i = index; i < count_; ++i)
  {
     new_shape_array_[i] = shape_array_[i + 1];
  }
  shape_array_.swap(new_shape_array_);
}

void potapova::CompositeShape::rotate(double angle)
{
  const double cosA = cos((2 * M_PI * angle) / 360.0);
  const double sinA = sin((2 * M_PI * angle) / 360.0);

  const point_t compCentre = getFrameRect().pos;

  for (unsigned int i = 0; i < count_; i ++)
  {
    const point_t shapeCenter = shape_array_[i]->getFrameRect().pos;

    const double dx = (shapeCenter.x - compCentre.x) * cosA - (shapeCenter.y - compCentre.y) * sinA;
    const double dy = (shapeCenter.x - compCentre.x) * sinA + (shapeCenter.y - compCentre.y) * cosA;

    shape_array_[i]->move({compCentre.x + dx, compCentre.y + dy});
    shape_array_[i]->rotate(angle);

  }
}

potapova::point_t potapova::CompositeShape::getCenter() const
{
  rectangle_t rectan = getFrameRect();
  return rectan.pos;
}


void potapova::CompositeShape::printInfo() const
{
  potapova::rectangle_t frameRect = getFrameRect();
  std::cout << "Center of shape " << frameRect.pos.x << ", " << frameRect.pos.y << "\n";
  std::cout << "Frame rectangle: width - " << frameRect.width << ", height - " << frameRect.height << "\n";
  std::cout << "Area of shape - " << getArea() << "\n";
}
