#ifndef FUNCTOR_HPP_B7
#define FUNCTOR_HPP_B7

#include <iostream>
#include <vector>

namespace potapova
{
  class Functor
  {
  public:
    void operator()(double elem);

    void printInfo();
  private:
    std::vector<double> numbers;
  };
}

#endif
