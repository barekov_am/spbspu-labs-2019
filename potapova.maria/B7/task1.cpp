#include <functional>
#include <iostream>
#include <iterator>
#include <algorithm>

#include "functor.hpp"

void task1()
{
  potapova::Functor functor;

  std::for_each(std::istream_iterator<double>(std::cin), std::istream_iterator<double>(), std::ref(functor));

  if (!std::cin.eof())
  {
      throw std::ios_base::failure("Can`t read data\n");
  }

  functor.printInfo();
}
