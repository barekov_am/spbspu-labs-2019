#include <cmath>
#include <algorithm>
#include <iostream>

#include "functor.hpp"

void potapova::Functor::operator()(double elem)
{
  numbers.push_back(elem * M_PI);
}

void potapova::Functor::printInfo()
{
  if (numbers.empty())
  {
    return;
  }

  std::for_each(numbers.begin(), numbers.end(),[](double elem){std::cout << elem << " ";});
}
