#include <stdexcept>
#include "shape.hpp"

potapova::Shape::Shape():
  x_(0),
  y_(0)
{
}

potapova::Shape::Shape(double x, double y) :
  x_(x),
  y_(y)
{
}

bool potapova::Shape::isMoreLeft(const Shape * shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shapes are empty\n");
  }

  return x_ < shape->x_;
}

bool potapova::Shape::isUpper(const Shape * shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shapes are empty\n");
  }

  return y_ > shape->y_;
}
