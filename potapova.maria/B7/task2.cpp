#include <memory>
#include <string>
#include <vector>
#include <algorithm>

#include "shape.hpp"

std::shared_ptr<potapova::Shape> readShape(std::string & line);

void printVector(const std::vector<std::shared_ptr<potapova::Shape>> & vector)
{
  for (const auto & shape : vector)
  {
    shape->draw(std::cout);
  }
}

void task2()
{
  std::vector<std::shared_ptr<potapova::Shape>> shapes;
  std::string ss;

  while (getline(std::cin >> std::ws, ss))
  {
    std::shared_ptr<potapova::Shape> shape = readShape(ss);
    shapes.push_back(shape);
  }

  std::cout << "Original:" << std::endl;
  printVector(shapes);

  std::cout << "Left-Right:" << std::endl;
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<potapova::Shape> & left, const std::shared_ptr<potapova::Shape> & right)
  {
    return left->isMoreLeft(right.get());
  });
  printVector(shapes);

  std::cout << "Right-Left:" <<std::endl;
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<potapova::Shape> & left, const std::shared_ptr<potapova::Shape> & right)
  {
    return !left->isMoreLeft(right.get());
  });
  printVector(shapes);

  std::cout << "Top-Bottom:" << std::endl;
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<potapova::Shape> & left, const std::shared_ptr<potapova::Shape> & right)
  {
    return left->isUpper(right.get());
  });
  printVector(shapes);

  std::cout << "Bottom-Top:" << std::endl;
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<potapova::Shape> & left, const std::shared_ptr<potapova::Shape> & right)
  {
    return !left->isUpper(right.get());
  });
  printVector(shapes);
}
