#ifndef SHAPE_HPP_B7
#define SHAPE_HPP_B7

#include <iostream>
namespace potapova
{
  class Shape
  {
  public:
    Shape();
    Shape(double x, double y);
    virtual ~Shape() = default;

    bool isMoreLeft(const Shape * shape) const;

    bool isUpper(const Shape * shape) const;

    virtual void draw(std::ostream & out) = 0;

  protected:
    double x_;
    double y_;
  };
}
#endif
