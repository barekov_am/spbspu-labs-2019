#ifndef TYPEOFSHAPE_HPP_B7
#define TYPEOFSHAPE_HPP_B7

#include "shape.hpp"

class Triangle : public potapova::Shape
{
public:
  Triangle(double x, double y);
  void draw(std::ostream & out) override;
};

class Square : public potapova::Shape
{
public:
  Square(double x, double y);
  void draw(std::ostream & out) override;
};

class Circle : public potapova::Shape
{
public:
  Circle(double x, double y);
  void draw(std::ostream & out) override;
};
#endif
