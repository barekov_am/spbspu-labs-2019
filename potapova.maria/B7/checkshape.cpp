#include <iostream>
#include <string>
#include <memory>
#include <algorithm>

#include "typeofshape.hpp"

std::shared_ptr<potapova::Shape> readShape(std::string & line)
{
  if (line.empty())
  {
    throw std::invalid_argument("No data\n");
  }

  line.erase(std::remove_if(line.begin(), line.end(), [](const char c) { return std::isspace(c); }), line.end());
  auto firstBracket = line.find_first_of('(');
  std::string shapeType = line.substr(0, firstBracket);
  line.erase(0, firstBracket);

  auto pos = line.find_first_of(';');
  auto secondBracket = line.find_first_of(')');

  if ((pos == std::string::npos) || (secondBracket == std::string::npos) || (firstBracket == std::string::npos))
  {
    throw std::invalid_argument("Wrong center declaration\n");
  }

  int x = std::stoi(line.substr(1, pos - 1));
  int y = std::stoi(line.substr(pos + 1, secondBracket - pos - 1));

  if (shapeType == "CIRCLE")
  {
    return std::make_shared<Circle>(Circle(x, y));
  }
  else if (shapeType == "TRIANGLE")
  {
    return std::make_shared<Triangle>(Triangle(x, y));
  }
  else if (shapeType == "SQUARE")
  {
    return std::make_shared<Square>(Square(x, y));
  }
  else
  {
    throw std::invalid_argument("Wrong type of shape\n");
  }
}
