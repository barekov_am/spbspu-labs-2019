#ifndef TEXT_HPP_B8
#define TEXT_HPP_B8

#include <string>
#include <vector>

enum TokenTypes
{
  WORD,
  PUNCT,
  DASH,
  NUMBER,
};

struct token_t
{
  TokenTypes type;
  std::string string;
};

class Text
{
public:
  Text(size_t width = 40);
  void read();
  void print();
  void readWord(std::string &string);
  void readDash(std::string &string);
  void readNumber(std::string &string);
private:
  size_t width_;
  std::vector<token_t> lines_;
};

#endif
