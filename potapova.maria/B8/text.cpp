#include <iostream>
#include <algorithm>
#include "text.hpp"

const int MAXWIDTH = 20;

Text::Text(size_t width) :
  width_(width),
  lines_()
{
}

void Text::read()
{
  while(std::cin)
  {
    std::cin >> std::noskipws;
    char firstChar = std::cin.get();

    if (std::isalpha(firstChar))
    {
      std::string string;
      readWord(string += firstChar);
      token_t token = {TokenTypes::WORD, string};
      lines_.push_back(token);
    }

    else if ((((firstChar == '+') || (firstChar == '-')) && (std::isdigit(std::cin.peek()))) || (std::isdigit(firstChar)))
    {
      std::string string;
      readNumber(string += firstChar);
      token_t token = {TokenTypes::NUMBER, string};
      lines_.push_back(token);
    }

    else if (std::ispunct(firstChar))
    {
      if((firstChar == '-') && (std::cin.peek() == '-'))
      {
         std::string string;
         readDash(string += firstChar);
         token_t token = {TokenTypes::DASH, string};
         lines_.push_back(token);
      }
      else if (!std::ispunct(std::cin.peek()))
      {
        std::string string;
        string += firstChar;
        token_t token = {TokenTypes::PUNCT, string};
        lines_.push_back(token);
      }
      else
      {
        throw std::invalid_argument("Incorrect data!");
      }
    }
  }
}

void Text::print()
{
  if (lines_.empty())
  {
    return;
  }

  if ((lines_.at(0).type == TokenTypes::PUNCT) || (lines_.at(0).type == TokenTypes::DASH))
  {
    throw std::invalid_argument("Incorrect data, first element is punct!");
  }

  for (size_t i = 0; i < lines_.size() - 1; i++)
  {
    if ((lines_.at(i).type == TokenTypes::PUNCT) && (lines_.at(i + 1).type == TokenTypes::PUNCT))
    {
      throw std::invalid_argument("Two punct!");
    }
  }

  std::vector<std::string> lines;
  std::string line;

  size_t count = 0;
  size_t numberLines = 0;

  while (count < lines_.size())
  {
    if (line.empty())
    {
      if ((lines_.at(count).type == TokenTypes::PUNCT) || (lines_.at(count).type == TokenTypes::DASH))
      {
        if ((lines_.at(count).type == TokenTypes::PUNCT) && (lines.at(numberLines - 1).size() < width_))
        {
          lines.at(numberLines - 1) += lines_.at(count).string;
          count++;
          continue;
        }
        line += lines_.at(count - 1).string;
        (lines_.at(count).type == TokenTypes::DASH) ? line += " " + lines_.at(count).string : line += lines_.at(count).string;
        auto pos = lines.at(numberLines - 1).find_last_of(' ');
        lines.at(numberLines - 1).erase(pos);
        count++;
        continue;
      }
      line += lines_.at(count).string;
      count++;
      continue;
    }

    if (line.size() + lines_.at(count).string.size() + 1 <= width_)
    {
      if (lines_.at(count).type == TokenTypes::PUNCT)
      {
        line += lines_.at(count).string;
        count++;
        continue;
      }

      line += " " + lines_.at(count).string;
      count++;
      continue;
    }
    lines.push_back(line);
    numberLines++;
    line.clear();
  }
  lines.push_back(line);

  std::for_each(lines.begin(), lines.end(), [](std::string lineEdit)
       {
         std::cout << lineEdit << "\n";
       });
}

void Text::readWord(std::string &string)
{
  while(std::cin)
  {
    if ((std::isalpha(std::cin.peek())) || (std::cin.peek() == '-'))
    {
      string += std::cin.get();
      if ((string.back() == '-') && (std::cin.peek() == '-'))
      {
         throw std::invalid_argument("Incorrect data!");
      }
    }

    else
    {
      break;
    }
  }

  if (string.size() > MAXWIDTH)
  {
    throw std::invalid_argument("Width must be less than 20!");
  }
}

void Text::readDash(std::string &string)
{
  int numberDash = 1;
  while (std::cin)
  {
    if (std::cin.peek() == '-')
    {
      string += std::cin.get();
      numberDash++;
    }
    else
    {
      break;
    }
  }

  if (numberDash > 3)
  {
    throw std::invalid_argument("Too many dashes for a hyphen\n");
  }

}

void Text::readNumber(std::string &string)
{
  int dot = 0;
  while(std::cin)
  {
    if ((std::cin.peek() == '.') || (std::isdigit(std::cin.peek())))
    {
      string += std::cin.get();
      if (string.back() == '.')
      {
        dot++;
        if (dot > 1)
        {
          throw std::invalid_argument("It can be just 1 dot!");
        }
      }
    }
    else
    {
      break;
    }
  }

  if (string.size() > MAXWIDTH)
  {
    throw std::invalid_argument("Too much width of string!");
  }
}

