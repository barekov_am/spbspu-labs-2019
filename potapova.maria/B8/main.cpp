#include <iostream>
#include <cstring>
#include "text.hpp"

const int MINWIDTH = 25;

int main(int argc, char * argv[])
{
  try
  {
    if ((argc == 2) || (argc > 3))
    {
      std::cerr << "Number of parameters is incorrect!" << std::endl;
      return 1;
    }
    size_t width = 40;
    if (argc == 3)
    {
      if (strcmp(argv[1], "--line-width") != 0)
      {
        throw std::invalid_argument("Invalid input!");
      }

      width = std::stoi(argv[2]);

      if (width < MINWIDTH)
      {
        throw std::invalid_argument("Invalid line width! It must be greater than 25!");
      }
    }

    Text text(width);
    text.read();
    text.print();
  }

  catch (std::exception& exc)
  {
    std::cerr << exc.what() << std::endl;
    return 2;
  }

  return 0;
}

