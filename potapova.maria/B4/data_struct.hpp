#ifndef DATA_STRUCT_B4
#define DATA_STRUCT_B4

#include <string>
#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>

namespace potapova
{
  struct DataStruct
  {
    int key1;
    int key2;
    std::string str;
  };

  void task();
}

#endif
