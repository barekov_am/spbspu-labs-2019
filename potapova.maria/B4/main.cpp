#include <exception>

#include "data_struct.hpp"

int main()
{
  try
  {
    potapova::task();
  }

  catch (std::exception& exc)
  {
    std::cerr << exc.what() << std::endl;
    return 2;
  }

  return 0;
}

