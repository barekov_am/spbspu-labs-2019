#include "data_struct.hpp"

const int COMMAS_NUMBER = 2;
const int KEY_MIN_VAL = -5;
const int KEY_MAX_VAL = 5;

bool checkData(const int data)
{
  return data >= KEY_MIN_VAL && data <= KEY_MAX_VAL;
}

bool checkNumberOfCommas(const std::string& str)
{
  int commas = 0;
  for (unsigned int i = 0; i < str.length(); i++)
  {
    if (str[i] == ',')
    {
      commas++;
    }
  }
  return commas == COMMAS_NUMBER;
}

potapova::DataStruct inputNewString(const std::string& str)
{
  std::istringstream stream(str);

  if (!checkNumberOfCommas(str))
  {
    throw std::invalid_argument("Reading failed!");
  }

  int key1 = 0;
  int key2 = 0;

  char comma;
  std::string s;

  stream >> key1 >> comma >> key2 >> comma;

  s = str.substr(str.find_last_of(',') + 1);

  if (stream.fail())
  {
    throw std::ios::failure("Reading failed!");
  }

  if (!checkData(key1) || !checkData(key2))
  {
    throw std::invalid_argument("Incorrect data!");
  }

  return potapova::DataStruct{ key1, key2, s };
}

void readVector(std::vector<potapova::DataStruct>& vector)
{
  std::string line;
  while (std::getline(std::cin, line))
  {
    vector.push_back(inputNewString(line));
  }
}

void printVector(std::vector<potapova::DataStruct>& vector)
{
  for (auto i : vector)
  {
    std::cout << i.key1 << "," << i.key2 << "," << i.str << std::endl;
  }
}


bool comparator(const potapova::DataStruct& str1, const potapova::DataStruct& str2)
{
  if (str1.key1 < str2.key1)
  {
    return true;
  }

  if (str1.key1 == str2.key1)
  {
    if (str1.key2 == str2.key2)
    {
      if (str1.str.size() == str2.str.size())
      {
        return (str1.str < str2.str);
      }
      return (str1.str.size() < str2.str.size());
    }
    return (str1.key2 < str2.key2);
  }
  return false;
}

void potapova::task()
{
  std::vector<potapova::DataStruct> vector;
  readVector(vector);
  std::sort(vector.begin(), vector.end(), comparator);
  printVector(vector);
}
