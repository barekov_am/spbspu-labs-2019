#include <iostream>
#include <limits>

#include "functor.hpp"

potapova::Functor::Functor()
{
  firstElem_ = 0;
  max_ = std::numeric_limits<int>::min();
  min_ = std::numeric_limits<int>::max();
  quantityPositive_ = 0;
  quantityNegative_ = 0;
  oddSum_ = 0;
  evenSum_ = 0;
  equal_ = false;
  quantityOfElem_ = 0;
}

void potapova::Functor::operator()(const long long int &elem)
{
  if (quantityOfElem_ == 0)
  {
    firstElem_ = elem;
  }

  if (elem > max_)
  {
    max_ = elem;
  }

  if (elem < min_)
  {
    min_ = elem;
  }

  if (elem > 0)
  {
    quantityPositive_++;
  }

  if (elem < 0)
  {
    quantityNegative_++;
  }

  if (elem % 2 != 0)
  {
    oddSum_ += elem;
  }
  else
  {
    evenSum_ += elem;
  }

  if (firstElem_ == elem)
  {
    equal_ = true;
  }
  else
  {
    equal_ = false;
  }

  quantityOfElem_++;
}

void potapova::Functor::printInfo()
{
  if (quantityOfElem_ == 0)
  {
    std::cout << "No Data" << std::endl;
    return;
  }

  std::cout << "Max: " << max_ << std::endl;
  std::cout << "Min: " << min_ << std::endl;
  std::cout << "Mean: " << (static_cast<double>(evenSum_) + static_cast<double>(oddSum_)) / static_cast<double>(quantityOfElem_) <<std::endl;
  std::cout << "Positive: " << quantityPositive_ << std::endl;
  std::cout << "Negative: " << quantityNegative_ << std::endl;
  std::cout << "Odd Sum: " << oddSum_ << std::endl;
  std::cout << "Even Sum: " << evenSum_ << std::endl;
  if (equal_ == true)
  {
    std::cout << "First/Last Equal: " << "yes" << std::endl;
  }
  else
  {
    std::cout << "First/Last Equal: " << "no" << std::endl;
  }
}
