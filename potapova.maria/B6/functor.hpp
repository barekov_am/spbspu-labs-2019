#ifndef FUNCTOR_HPP_B6
#define FUNCTOR_HPP_B6

#include <iostream>

namespace potapova
{
  class Functor
  {
  public:
    Functor();

    void operator()(const long long int &elem);
    void printInfo();
  private:
    long long int firstElem_;
    long long int max_;
    long long int min_;
    size_t quantityPositive_;
    size_t quantityNegative_;
    long long int oddSum_;
    long long evenSum_;
    bool equal_;
    size_t quantityOfElem_;
  };
}
#endif
