#include <iostream>
#include <algorithm>

#include "tasks.hpp"

void potapova::tasks::task1()
{
  potapova::details::PhoneBook book;
  std::string input_line;

  while (std::getline(std::cin, input_line))
  {
    try
    {
      potapova::details::command_function cmd = potapova::details::handleCommand(input_line);
      cmd(book);
    }
    catch (std::invalid_argument& exc)
    {
      std::cerr << exc.what() << std::endl;
    }
    catch (std::exception& exc)
    {
      std::cout << exc.what() << std::endl;
    }
  }
}

void potapova::tasks::task2()
{
  potapova::details::Factorial fact;

  std::copy(fact.begin(), fact.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;

  std::reverse_copy(fact.begin(), fact.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;

}
