#ifndef TASKS_HPP
#define TASKS_HPP

#include "command_handler.hpp"
#include "factorial.hpp"

namespace potapova
{
  namespace tasks
  {
    void task1();
    void task2();
  }
}

#endif
