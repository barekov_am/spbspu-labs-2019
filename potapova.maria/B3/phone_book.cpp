#include <sstream>

#include "phone_book.hpp"
#include "phone_book_exceptions.hpp"

using potapova::details::constants::CURRENT_BOOKMARK;

potapova::details::PhoneBook::PhoneBook()
{
  bookmarks_[CURRENT_BOOKMARK] = register_.end();
}

potapova::details::PhoneBook::PhoneBook(const std::string& number, const std::string& name)
{
  bookmarks_[CURRENT_BOOKMARK] = register_.end();
  insertEnd(number, name);
}

potapova::details::PhoneBook::PhoneBook(const PhoneBookEntry_t& number_name) : PhoneBook(number_name.number, number_name.name)
{
}

void potapova::details::PhoneBook::move(const std::string& steps, const std::string& bookmark)
{
  if (bookmarks_.find(bookmark) == bookmarks_.end())
  {
    throw details::IncorrectBookmarkException();
  }

  if (steps == "first")
  {
    bookmarks_[bookmark] = register_.begin();
    return;
  }

  if (steps == "last")
  {
    bookmarks_[bookmark] = std::prev(register_.end());
    return;
  }

  std::istringstream str(steps);
  int distance = 0;

  str >> distance;
  if (str.fail())
  {
    throw details::IncorrectStepException();
  }

  if (distance + static_cast<int>(std::distance(register_.begin(), bookmarks_[bookmark])) < 0
    || distance + +static_cast<int>(std::distance(register_.begin(), bookmarks_[bookmark])) >= static_cast<int>(register_.size()))
  {
    return;
  }
  std::advance(bookmarks_[bookmark], distance);
}

void potapova::details::PhoneBook::insertBefore(const std::string& number, const std::string& name, const std::string& bookMark)
{
  if (bookmarks_.find(bookMark) == bookmarks_.end())
  {
    throw potapova::details::IncorrectBookmarkException();
  }

  PhoneBookEntry_t tmp_contact;
  tmp_contact.number = number;
  tmp_contact.name = name;

  if (bookmarks_[bookMark] == register_.end())
  {
    register_.push_back(tmp_contact);
    bookmarks_[bookMark] = std::prev(register_.end());
  }
  else
  {
    register_.insert(bookmarks_[bookMark], tmp_contact);
  }
}

void potapova::details::PhoneBook::insertAfter(const std::string& number, const std::string& name, const std::string& bookMark)
{
  if (bookmarks_.find(bookMark) == bookmarks_.end())
  {
    throw potapova::details::IncorrectBookmarkException();
  }

  PhoneBookEntry_t tmp_contact;
  tmp_contact.number = number;
  tmp_contact.name = name;

  if (bookmarks_[bookMark] == register_.end())
  {
    register_.push_back(tmp_contact);
    bookmarks_[bookMark] = std::prev(register_.end());
  }
  else
  {
    register_.insert(std::next(bookmarks_[bookMark]), tmp_contact);
  }
}

void potapova::details::PhoneBook::insertEnd(const std::string& number, const std::string& name)
{
  PhoneBookEntry_t tmp_contact;
  tmp_contact.number = number;
  tmp_contact.name = name;

  if (register_.empty())
  {
    register_.push_back(tmp_contact);
    bookmarks_[CURRENT_BOOKMARK] = register_.begin();
  }
  else
  {
    register_.push_back(tmp_contact);
  }
}

void potapova::details::PhoneBook::deleteNote(const std::string& bookmark)
{
  if (bookmarks_.find(bookmark) == bookmarks_.end())
  {
    throw potapova::details::IncorrectBookmarkException();
  }

  auto current = getBookmarkIter(bookmark);

  if (register_.empty() || current == register_.end())
  {
    throw std::invalid_argument("Phone book is empty!");
  }

  for (auto& item : bookmarks_)
  {
    if (item.second == current)
    {
      item.second = std::next(current);
    }
  }

  register_.erase(std::prev(bookmarks_[bookmark]));

  for (auto& item : bookmarks_)
  {
    if (item.second == register_.end())
    {
      item.second = std::prev(register_.end());
    }
  }
}

void potapova::details::PhoneBook::addNote(const std::string& bookMark_old, const std::string& bookMark_new)
{
  if (bookMark_new == CURRENT_BOOKMARK || bookmarks_.find(bookMark_old) == bookmarks_.end())
  {
    throw details::IncorrectBookmarkException();
  }

  bookmarks_[bookMark_new] = bookmarks_[bookMark_old];
}

potapova::details::PhoneBookEntry_t potapova::details::PhoneBook::getRecord(const std::string& bookMark) const
{
  if (bookmarks_.find(bookMark) == bookmarks_.end())
  {
    throw potapova::details::IncorrectBookmarkException();
  }

  if (register_.empty())
  {
    throw potapova::details::EmptyBookException();
  }

  return *bookmarks_.at(bookMark);
}

potapova::details::PhoneBook::RegisterIterator potapova::details::PhoneBook::getBookmarkIter(const std::string& bookMark)
{
  if (bookmarks_.find(bookMark) == bookmarks_.end())
  {
    throw details::IncorrectBookmarkException();
  }

  if (register_.empty())
  {
    throw details::EmptyBookException();
  }

  return bookmarks_[bookMark];
}
