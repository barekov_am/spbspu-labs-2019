#include <cstring>

#include "details.hpp"

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Number of parameters must be 2!" << std::endl;
      return 1;
    }

    if (strcmp(argv[1], "1") == 0)
     {
       details::task1();
     }
     else if (strcmp(argv[1], "2") == 0)
     {
       details::task2();
     }
     else
     {
       throw std::invalid_argument("Invalid input!");
     }
  }
  catch (const std::exception& exc)
  {
    std::cerr << exc.what() << std::endl;
    return 2;
  }

  return 0;
}
