#include <set>
#include <string>

#include "details.hpp"

void readFile(std::set<std::string>& file)
{
  std::string stream;

  while (std::cin >> stream)
  {
    file.emplace(stream);
  }
}

void writeWords(const std::set<std::string>& file)
{
  for (const auto& word : file)
  {
    std::cout << word << std::endl;
  }
}

void details::task1()
{
  std::set<std::string> words;
  readFile(words);
  writeWords(words);
}
