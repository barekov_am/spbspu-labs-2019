#ifndef DETAILS_HPP_B5
#define DETAILS_HPP_B5

#include <vector>
#include <list>
#include <iostream>

namespace details
{
  struct Point
  {
    int x, y;
  };

  using Shape = std::vector< Point >;
  using ShapesContainer = std::list<Shape>;

  void task1();
  void task2();
}

inline bool isRectangleShape(const details::Shape& shape);
inline bool isSquareShape(const details::Shape& shape);

#endif
