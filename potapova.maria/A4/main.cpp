#include <iostream>
#include <memory>
#include <cmath>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

int main()
{
  potapova::CompositeShape::p_shape p_circle1 = std::make_shared<potapova::Circle>(potapova::point_t { -45, -98 }, 6);
  p_circle1->printInfo();
  potapova::CompositeShape::p_shape p_circle2 = std::make_shared<potapova::Circle>(potapova::point_t { 3, 8 }, 6);
  p_circle2->printInfo();

  std::cout << std::endl << "---------------------------------------------" << std::endl;

  potapova::CompositeShape::p_shape p_rectangle1 = std::make_shared<potapova::Rectangle>(potapova::point_t { 1, 1 }, 2, 2);
  p_rectangle1->printInfo();
  potapova::CompositeShape::p_shape p_rectangle2 = std::make_shared<potapova::Rectangle>(potapova::point_t { 5, 1 }, 3, 2);
  p_rectangle2->printInfo();


  potapova::CompositeShape compsh;

  compsh.add(p_circle1);
  compsh.add(p_circle2);

  compsh.add(p_rectangle1);
  compsh.add(p_rectangle2);

  compsh.printInfo();

  std::cout << std::endl << "---------------------------------------------" << std::endl;

  potapova::Matrix matrix = potapova::part(compsh);
  matrix.writeInfo();

  std::cout << std::endl << "---------------------------------------------" << std::endl;

  compsh.rotate(45);
  compsh.printInfo();
  compsh.scale(2);
  compsh.printInfo();

  return 0;
}
