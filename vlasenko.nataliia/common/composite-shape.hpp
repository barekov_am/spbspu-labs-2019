#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace vlasenko
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&) noexcept;
    CompositeShape(const shape_ptr &);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &);
    CompositeShape &operator =(CompositeShape &&) noexcept;
    shape_ptr operator [](std::size_t) const;

    double getArea() const;
    rectangle_t getFrameRect() const;
    std::size_t getCount() const;
    void printInfo() const;
    void move(const point_t &);
    void move(double, double);
    void scale(double);
    void rotate(double);
    void add(const shape_ptr &);
    void remove(std::size_t);
    void swap(CompositeShape &) noexcept;

  private:
    shapes_array shapesArray_;
    std::size_t count_;
    std::size_t size_;
  };
}

#endif
