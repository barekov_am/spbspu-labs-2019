#include "composite-shape.hpp"
#include <stdexcept>
#include <iostream>
#include <memory>
#include <algorithm>
#include <cmath>

vlasenko::CompositeShape::CompositeShape(const vlasenko::CompositeShape &copyOfCompositeShape):
  shapesArray_(std::make_unique<shape_ptr []>(copyOfCompositeShape.count_)),
  count_(copyOfCompositeShape.count_),
  size_(copyOfCompositeShape.size_)

{
  for (std::size_t i = 0; i < size_; ++i)
  {
    shapesArray_[i] = copyOfCompositeShape.shapesArray_[i];
  }
}

vlasenko::CompositeShape::CompositeShape(vlasenko::CompositeShape &&movedCompositeShape) noexcept :
  shapesArray_(std::move(movedCompositeShape.shapesArray_)),
  count_(movedCompositeShape.count_),
  size_(movedCompositeShape.size_)

{
  movedCompositeShape.size_ = 0;
  movedCompositeShape.count_ = 0;
}

vlasenko::CompositeShape::CompositeShape(const shape_ptr &shape) :
  shapesArray_(std::make_unique<shape_ptr []>(16)),
  count_(1),
  size_(1)

{
if (shape == nullptr)
  {
    throw std::invalid_argument("Pointer mustn't be null\n");
  }
  shapesArray_[0] = shape;
}

vlasenko::CompositeShape &vlasenko::CompositeShape::operator =(const vlasenko::CompositeShape &copyOfCompositeShape)
{
  if (this != &copyOfCompositeShape)
  {
    vlasenko::CompositeShape(copyOfCompositeShape).swap(*this);
  }
  return *this;
}

vlasenko::CompositeShape &vlasenko::CompositeShape::operator =(vlasenko::CompositeShape &&movedCompositeShape) noexcept
{
  if (this != &movedCompositeShape)
  {
    size_ = movedCompositeShape.size_;
    movedCompositeShape.size_ = 0;

    count_ = movedCompositeShape.count_;
    movedCompositeShape.count_ = 0;

    shapesArray_ = std::move(movedCompositeShape.shapesArray_);
  }

  return *this;
}

vlasenko::shape_ptr vlasenko::CompositeShape::operator [](std::size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range\n");
  }

  return shapesArray_[index];
}

double vlasenko::CompositeShape::getArea() const
{
  double totalArea_ = 0.0;
  for (std::size_t i = 0; i < count_; ++i)
  {
    totalArea_ += shapesArray_[i]->getArea();
  }

  return totalArea_;
}

vlasenko::rectangle_t vlasenko::CompositeShape::getFrameRect() const
{
  vlasenko::rectangle_t currentRectangle = shapesArray_[0]->getFrameRect();

  double max_X = currentRectangle.pos.x + currentRectangle.width / 2.0;
  double min_X = currentRectangle.pos.x - currentRectangle.width / 2.0;
  double max_Y = currentRectangle.pos.y + currentRectangle.height / 2.0;
  double min_Y = currentRectangle.pos.y - currentRectangle.height / 2.0;

  for (std::size_t i = 1; i < count_; ++i)
  {
    currentRectangle = shapesArray_[i]->getFrameRect();

    max_X = std::max(currentRectangle.pos.x + currentRectangle.width / 2.0, max_X);
    min_X = std::min(currentRectangle.pos.x - currentRectangle.width / 2.0, min_X);
    max_Y = std::max(currentRectangle.pos.y + currentRectangle.height / 2.0, max_Y);
    min_Y = std::min(currentRectangle.pos.y - currentRectangle.height / 2.0, min_Y);
  }

  return {(max_X - min_X), (max_Y - min_Y), {(max_X + min_X) / 2.0, (max_Y + min_Y) / 2.0}};
}

std::size_t vlasenko::CompositeShape::getCount() const
{
  return count_;
}

void vlasenko::CompositeShape::move(double shift_x, double shift_y)
{
  for (std::size_t i = 0; i < count_; ++i)
  {
    shapesArray_[i]->move(shift_x, shift_y);
  }
}

void vlasenko::CompositeShape::move(const vlasenko::point_t &position)
{
  const vlasenko::rectangle_t frameRect = getFrameRect();
  double shift_x = position.x - frameRect.pos.x;
  double shift_y = position.y - frameRect.pos.y;

  move(shift_x, shift_y);
}

void vlasenko::CompositeShape::scale(double value)
{
  if (value <= 0.0)
  {
    throw std::invalid_argument("Value must be positive\n");
  }
  const vlasenko::point_t posFrameRect = getFrameRect().pos;
  for (std::size_t i = 0; i < count_; ++i)
  {
    vlasenko::point_t shapeCenter = shapesArray_[i]->getFrameRect().pos;

    shapesArray_[i]->scale(value);

    double shift_X = (shapeCenter.x - posFrameRect.x) * (value - 1);
    double shift_Y = (shapeCenter.y - posFrameRect.y) * (value - 1);

    shapesArray_[i]->move(shift_X, shift_Y);
  }
}

void vlasenko::CompositeShape::add(const std::shared_ptr<vlasenko::Shape> &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape's pointer mustn't be null\n");
  }

  ++count_;

  if (size_ < count_)
  {
    shapes_array tempArray(std::make_unique<std::shared_ptr<vlasenko::Shape> []>(count_));

    for (std::size_t i = 0; i < size_; ++i)
    {
      tempArray[i] = shapesArray_[i];
    }

    ++size_;
    shapesArray_.swap(tempArray);
  }

  shapesArray_[count_ - 1] = shape;
}

void vlasenko::CompositeShape::remove(std::size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range\n");
  }
  if (count_ == 1)
  {
    throw std::invalid_argument("Cannot delete the last shape\n");
  }

  count_--;
  for (std::size_t i = index; i < count_; ++i)
  {
    shapesArray_[i] = shapesArray_[i + 1];
  }

  shapesArray_[count_] = nullptr;
}

void vlasenko::CompositeShape::printInfo() const
{
  const vlasenko::rectangle_t frameRect = getFrameRect();

  std::cout << "The center of composite shape is (" << frameRect.pos.x << "," << frameRect.pos.y << ")" << "\n";
  std::cout << "count_ of shapes is " << count_ << "\n";
  std::cout << "Total area of composite shape is " << getArea() << "\n";
  std::cout << "Frame rectangle \n";
  std::cout << "Width is " << frameRect.width << "\n";
  std::cout << "Height is " << frameRect.height << "\n";
}

void vlasenko::CompositeShape::swap(vlasenko::CompositeShape &swappingShape) noexcept
{
  std::swap(shapesArray_, swappingShape.shapesArray_);
  std::swap(count_, swappingShape.count_);
  std::swap(size_, swappingShape.size_);
}

void vlasenko::CompositeShape::rotate(double angle)
{
  const vlasenko::point_t center = getFrameRect().pos;
  const double cosinus = std::abs(std::cos(angle * M_PI / 180));
  const double sinus = std::abs(std::sin(angle * M_PI / 180));

  for (std::size_t i = 0; i < count_; ++i)
  {
    const vlasenko::point_t currientCenter = shapesArray_[i]->getFrameRect().pos;
    const double projection_x = currientCenter.x - center.x;
    const double projection_y = currientCenter.y - center.y;
    const double shift_x = projection_x * (cosinus - 1) - projection_y * sinus;
    const double shift_y = projection_x * sinus + projection_y * (cosinus - 1);
    shapesArray_[i]->move(shift_x, shift_y);
    shapesArray_[i]->rotate(angle);
  }
}
