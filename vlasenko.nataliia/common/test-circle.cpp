#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

const double FAULT = 0.01;

BOOST_AUTO_TEST_SUITE(circleMethodsTesting)

BOOST_AUTO_TEST_CASE(circleAfterTransferingTest)
{
  vlasenko::Circle circleTest({ 4.5, 7.3 }, 4.0);

  const double circleAreaBeforeMoving = circleTest.getArea();
  const vlasenko::rectangle_t frameRectBeforeMoving = circleTest.getFrameRect();

  circleTest.move(2.2, 3.3);

  BOOST_CHECK_CLOSE(circleTest.getArea(), circleAreaBeforeMoving, FAULT);

  vlasenko::rectangle_t frameRectAfterMoving = circleTest.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, FAULT);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, FAULT);
}

BOOST_AUTO_TEST_CASE(circleAfterMovingTest)
{
  vlasenko::Circle circleTest({ 4.5, 7.3 }, 4.0);

  const double circleAreaBeforeMoving = circleTest.getArea();
  const vlasenko::rectangle_t frameRectBeforeMoving = circleTest.getFrameRect();

  circleTest.move({ -1.5, 2.5 });

  BOOST_CHECK_CLOSE(circleTest.getArea(), circleAreaBeforeMoving, FAULT);

  vlasenko::rectangle_t frameRectAfterMoving = circleTest.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, FAULT);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, FAULT);
}

BOOST_AUTO_TEST_CASE(circleScaleTesting)
{
  vlasenko::Circle circleTest({ 4.0, 3.0 }, 5.0);

  const double circleAreaBeforeScale = circleTest.getArea();
  const double testScale = 2.5;

  circleTest.scale(testScale);

  BOOST_CHECK_CLOSE(circleTest.getArea(), circleAreaBeforeScale * testScale * testScale, FAULT);
}

BOOST_AUTO_TEST_CASE(circleExceptionAfterScaleTesting)
{
  vlasenko::Circle circleTest({ 2.3, 4.4 }, 2.2);
  BOOST_CHECK_THROW(circleTest.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circleTestThrowException)
{
  BOOST_CHECK_THROW(vlasenko::Circle({ 4.3,2.1 }, -2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circleRotateTesting)
{
  vlasenko::Circle circleTest({5.0, 5.0}, 2.0);
  const double cirlceAreaBefore = circleTest.getArea();
  const vlasenko::rectangle_t frameRectBeforeRotate = circleTest.getFrameRect();
  double angle = 90.0;
  circleTest.rotate(angle);
  double circleAreaAfter = circleTest.getArea();
  vlasenko::rectangle_t frameRectAfterRotate = circleTest.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.width, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.height, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, FAULT);
  BOOST_CHECK_CLOSE(cirlceAreaBefore, circleAreaAfter, FAULT);

  angle = -90;
  circleTest.rotate(angle);
  circleAreaAfter = circleTest.getArea();
  frameRectAfterRotate = circleTest.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.height, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.width, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, FAULT);
  BOOST_CHECK_CLOSE(cirlceAreaBefore, circleAreaAfter, FAULT);

  angle = 180;
  circleTest.rotate(angle);
  circleAreaAfter = circleTest.getArea();
  frameRectAfterRotate = circleTest.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.height, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.width, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, FAULT);
  BOOST_CHECK_CLOSE(cirlceAreaBefore, circleAreaAfter, FAULT);
}

BOOST_AUTO_TEST_SUITE_END()
