#include <boost/test/auto_unit_test.hpp>

#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double FAULT = 0.01;

BOOST_AUTO_TEST_SUITE(compositeShapeMethodsTesting)

/*BOOST_AUTO_TEST_CASE(ConstructorTesting)
{
  vlasenko::Rectangle rectangle({5, 5}, 10, 5);
  vlasenko::Shape *p_rectangle = &rectangle;
  vlasenko::CompositeShape compositeShape(p_rectangle);
  
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().pos.y, rectangle.getFrameRect().pos.y, FAULT);
}

BOOST_AUTO_TEST_CASE(throwExceptionDuaEntryNull)
{
  vlasenko::Shape *shape = nullptr;
  
  BOOST_CHECK_THROW(vlasenko::CompositeShape composite_shape(shape), std::invalid_argument);
}*/

BOOST_AUTO_TEST_CASE(compositeAfterTransferingTest)
{
  vlasenko::Circle circleTest({-2.4, 10.2}, 1.2);
  vlasenko::Rectangle rectangleTest({5.4, 7.2}, 2.5, 4.5);
  vlasenko::CompositeShape compositeTest(std::make_shared<vlasenko::Rectangle>(rectangleTest));

  compositeTest.add(std::make_shared<vlasenko::Circle>(circleTest));

  const double areaBeforeMoving = compositeTest.getArea();
  const vlasenko::rectangle_t frameRectBeforeMoving = compositeTest.getFrameRect();

  compositeTest.move(7.0, 5.0);

  BOOST_CHECK_CLOSE(frameRectBeforeMoving.height, frameRectBeforeMoving.height, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.width, frameRectBeforeMoving.width, FAULT);
  BOOST_CHECK_CLOSE(compositeTest.getArea(), areaBeforeMoving, FAULT);

  vlasenko::rectangle_t frameRectAfterMoving = compositeTest.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectBeforeMoving.height, frameRectAfterMoving.height, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.width, frameRectAfterMoving.width, FAULT);
  BOOST_CHECK_CLOSE(compositeTest.getArea(), areaBeforeMoving, FAULT);
}

BOOST_AUTO_TEST_CASE(compositeAfterMovingTest)
{
  vlasenko::Circle circleTest({-2.4, 10.2}, 1.2);
  vlasenko::Rectangle rectangleTest({5.4, 7.2}, 2.5, 4.5);
  vlasenko::CompositeShape compositeTest(std::make_shared<vlasenko::Rectangle>(rectangleTest));

  compositeTest.add(std::make_shared<vlasenko::Circle>(circleTest));

  const double areaBeforeMoving = compositeTest.getArea();
  const vlasenko::rectangle_t frameRectBeforeMoving = compositeTest.getFrameRect();

  compositeTest.move({-8.2, 4.5});

  BOOST_CHECK_CLOSE(compositeTest.getArea(), areaBeforeMoving, FAULT);

  vlasenko::rectangle_t frameRectAfterMoving = compositeTest.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectBeforeMoving.height, frameRectAfterMoving.height, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.width, frameRectAfterMoving.width, FAULT);
}



BOOST_AUTO_TEST_CASE(compositeScaleTest)
{
  vlasenko::Circle circleTest({-2.4, 10.2}, 1.2);
  vlasenko::Rectangle rectangleTest({5.4, 7.2}, 2.5, 4.5);
  vlasenko::CompositeShape compositeTest(std::make_shared<vlasenko::Rectangle>(rectangleTest));

  compositeTest.add(std::make_shared<vlasenko::Circle>(circleTest));

  const double areaBeforeMoving = compositeTest.getArea();
  const double testScale = 2.5;

  compositeTest.scale(testScale);

  BOOST_CHECK_CLOSE(areaBeforeMoving * testScale * testScale, compositeTest.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(compositeTestChangingAreaAfterAddingAndDeleting)
{
  vlasenko::Circle circleTest({-5.1, 3.4}, 2.0);
  vlasenko::Rectangle rectangleTest({5.4, 7.2}, 2.5, 4.5);
  vlasenko::Rectangle rectangleTest2({-2.3, -7.3}, 4.3, 4.7);
  vlasenko::CompositeShape compositeTest(std::make_shared<vlasenko::Rectangle>(rectangleTest));

  const double compositeAreaBeforeAdding = compositeTest.getArea();
  const int countOfShapesBeforeAdding = compositeTest.getCount();
  const int testIndex = 2;
  const double circleArea = circleTest.getArea();
  const double rectangle2Area = rectangleTest2.getArea();

  compositeTest.add(std::make_shared<vlasenko::Circle>(circleTest));
  compositeTest.add(std::make_shared<vlasenko::Rectangle>(rectangleTest2));

  double compositeAreaAfterAdding = compositeTest.getArea();
  const int countOfShapesAfterAdding = compositeTest.getCount();

  BOOST_CHECK_CLOSE(compositeAreaBeforeAdding + circleArea + rectangle2Area, compositeAreaAfterAdding, FAULT);
  BOOST_CHECK_EQUAL(countOfShapesBeforeAdding + 2, countOfShapesAfterAdding);

  const double areaOfDeletingShape = compositeTest[testIndex]->getArea();

  compositeTest.remove(testIndex);

  BOOST_CHECK_CLOSE(compositeAreaAfterAdding - areaOfDeletingShape, compositeTest.getArea(), FAULT);
  BOOST_CHECK_EQUAL(countOfShapesAfterAdding - 1, compositeTest.getCount());
}

BOOST_AUTO_TEST_CASE(compositeTestThrowExceptionAfterScale)
{
  vlasenko::Rectangle rectangleTest({-7.7, 1.5}, 1.5, 4.7);
  vlasenko::CompositeShape compositeTest(std::make_shared<vlasenko::Rectangle>(rectangleTest));

  BOOST_CHECK_THROW(compositeTest.scale(-3.5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeTestThrowExceptionAfterDeleting)
{
  vlasenko::Circle circleTest({-2.4, 10.2}, 1.2);
  vlasenko::Rectangle rectangleTest({5.4, 7.2}, 2.5, 4.5);
  vlasenko::CompositeShape compositeTest(std::make_shared<vlasenko::Rectangle>(rectangleTest));

  BOOST_CHECK_THROW(compositeTest.remove(0), std::invalid_argument);

  compositeTest.add(std::make_shared<vlasenko::Circle>(circleTest));

  BOOST_CHECK_THROW(compositeTest.remove(5), std::out_of_range);
  BOOST_CHECK_THROW(compositeTest.remove(-8), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeTestThrowExceptionAfterUsingOperator)
{
  vlasenko::Circle circleTest({-2.4, 10.2}, 1.2);
  vlasenko::Rectangle rectangleTest({5.4, 7.2}, 2.5, 4.5);
  vlasenko::CompositeShape compositeTest(std::make_shared<vlasenko::Rectangle>(rectangleTest));

  compositeTest.add(std::make_shared<vlasenko::Circle>(circleTest));

  BOOST_CHECK_THROW(compositeTest[5], std::out_of_range);
  BOOST_CHECK_THROW(compositeTest[-7], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeTestThrowExceptionWithNullPtr)
{
  BOOST_CHECK_THROW(vlasenko::CompositeShape(nullptr), std::invalid_argument);

  vlasenko::Rectangle rectangleTest({-5.1, 8.4}, 6.1, 0.7);
  vlasenko::CompositeShape compositeTest(std::make_shared<vlasenko::Rectangle>(rectangleTest));

  BOOST_CHECK_THROW(compositeTest.add(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeTestCopyOfConstructor)
{
  vlasenko::Circle circleTest({1.6, 8.4}, 4.0);
  vlasenko::Rectangle rectangleTest({1.3, 9.5}, 4.2, 6.3);
  vlasenko::CompositeShape compositeTest(std::make_shared<vlasenko::Rectangle>(rectangleTest));

  compositeTest.add(std::make_shared<vlasenko::Circle>(circleTest));

  const vlasenko::rectangle_t frameRect = compositeTest.getFrameRect();

  vlasenko::CompositeShape copyComposite(compositeTest);

  const vlasenko::rectangle_t copyFrameRect = copyComposite.getFrameRect();

  BOOST_CHECK_CLOSE(compositeTest.getArea(), copyComposite.getArea(), FAULT);
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, FAULT);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, FAULT);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, FAULT);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, FAULT);
  BOOST_CHECK_EQUAL(compositeTest.getCount(), copyComposite.getCount());
}

BOOST_AUTO_TEST_CASE(compositeTestCopyOfOperator)
{
  vlasenko::Circle circleTest({2.4, 7.2}, 5.0);
  vlasenko::Rectangle rectangleTest({1.5, 6.7}, 8.3, 4.1);
  vlasenko::CompositeShape compositeTest(std::make_shared<vlasenko::Rectangle>(rectangleTest));

  compositeTest.add(std::make_shared<vlasenko::Circle>(circleTest));

  const vlasenko::rectangle_t frameRect = compositeTest.getFrameRect();

  vlasenko::Circle circleTest2({4.4, 2.5}, 6.4);
  vlasenko::CompositeShape copyComposite(std::make_shared<vlasenko::Circle>(circleTest2));

  copyComposite = compositeTest;

  const vlasenko::rectangle_t copyFrameRect = copyComposite.getFrameRect();

  BOOST_CHECK_CLOSE(compositeTest.getArea(), copyComposite.getArea(), FAULT);
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, FAULT);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, FAULT);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, FAULT);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, FAULT);
  BOOST_CHECK_EQUAL(compositeTest.getCount(), copyComposite.getCount());
}

BOOST_AUTO_TEST_CASE(compositeTestMoveConstructor)
{
  vlasenko::Circle circleTest({2.7, 6.3}, 7.0);
  vlasenko::Rectangle rectangleTest({2.4, 7.4}, 2.2, 7.4);
  vlasenko::CompositeShape compositeTest(std::make_shared<vlasenko::Rectangle>(rectangleTest));

  compositeTest.add(std::make_shared<vlasenko::Circle>(circleTest));

  const vlasenko::rectangle_t frameRect = compositeTest.getFrameRect();
  const double compositeArea = compositeTest.getArea();
  const int compositeCount = compositeTest.getCount();

  vlasenko::CompositeShape moveComposite(std::move(compositeTest));

  const vlasenko::rectangle_t moveFrameRect = moveComposite.getFrameRect();

  BOOST_CHECK_CLOSE(compositeArea, moveComposite.getArea(), FAULT);
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, FAULT);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, FAULT);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, FAULT);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, FAULT);
  BOOST_CHECK_EQUAL(compositeCount, moveComposite.getCount());
  BOOST_CHECK_EQUAL(compositeTest.getCount(), 0);
  BOOST_CHECK_CLOSE(compositeTest.getArea(), 0, FAULT);
}

BOOST_AUTO_TEST_CASE(compositeTestMoveOperator)
{
  vlasenko::Circle circleTest({2.7, 6.3}, 7.0);
  vlasenko::Rectangle rectangleTest({2.4, 7.4}, 2.2, 7.4);
  vlasenko::CompositeShape compositeTest(std::make_shared<vlasenko::Rectangle>(rectangleTest));

  compositeTest.add(std::make_shared<vlasenko::Circle>(circleTest));

  const vlasenko::rectangle_t frameRect = compositeTest.getFrameRect();
  const double compositeArea = compositeTest.getArea();
  const int compositeCount = compositeTest.getCount();

  vlasenko::Circle circleTest2({5.2, 1.5}, 8.2);
  vlasenko::CompositeShape moveComposite(std::make_shared<vlasenko::Circle>(circleTest2));

  moveComposite = std::move(compositeTest);

  const vlasenko::rectangle_t moveFrameRect = moveComposite.getFrameRect();

  BOOST_CHECK_CLOSE(compositeArea, moveComposite.getArea(), FAULT);
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, FAULT);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, FAULT);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, FAULT);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, FAULT);
  BOOST_CHECK_EQUAL(compositeCount, moveComposite.getCount());
  BOOST_CHECK_EQUAL(compositeTest.getCount(), 0);
  BOOST_CHECK_CLOSE(compositeTest.getArea(), 0, FAULT);

  moveComposite = std::move(moveComposite);

  BOOST_CHECK_CLOSE(compositeArea, moveComposite.getArea(), FAULT);
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, FAULT);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, FAULT);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, FAULT);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, FAULT);
  BOOST_CHECK_EQUAL(compositeCount, moveComposite.getCount());
}

BOOST_AUTO_TEST_CASE(compositeTestMethodRotate)
{
  vlasenko::Circle circleTest({2.3, 5.7}, 3.0);
  vlasenko::Rectangle rectangleTest({0.2, 8.1}, 2.3, 2.8);
  vlasenko::CompositeShape compositeTest(std::make_shared<vlasenko::Rectangle>(rectangleTest));
  compositeTest.add(std::make_shared<vlasenko::Circle>(circleTest));

  const vlasenko::rectangle_t frameRectBeforeRotate = compositeTest.getFrameRect();
  const double areaBeforeRotate = compositeTest.getArea();

  compositeTest.rotate(90);
  vlasenko::rectangle_t frameRectAfterRotate = compositeTest.getFrameRect();
  BOOST_CHECK_CLOSE(areaBeforeRotate, compositeTest.getArea(), FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, FAULT);

  compositeTest.rotate(-180);
  frameRectAfterRotate = compositeTest.getFrameRect();
  BOOST_CHECK_CLOSE(areaBeforeRotate, compositeTest.getArea(), FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, FAULT);
}

BOOST_AUTO_TEST_SUITE_END()
