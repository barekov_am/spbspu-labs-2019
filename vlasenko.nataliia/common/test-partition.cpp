#include <memory>

#include <boost/test/auto_unit_test.hpp>

#include "matrix.hpp"
#include "partition.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testPartitionMethods)

BOOST_AUTO_TEST_CASE(testPartition)
{
  const vlasenko::Circle circle1({0.0, 0.0}, 3.0);
  const vlasenko::Circle circle2({5.0, 5.0}, 1.0);
  const vlasenko::Rectangle rectangle1({-2.0, 1.0}, 4.0, 4.0);
  const vlasenko::Rectangle rectangle2({-5.0, 4.0}, 3.0, 6.0);

  std::shared_ptr<vlasenko::Shape> circlePointer = std::make_shared<vlasenko::Circle>(circle1);
  std::shared_ptr<vlasenko::Shape> circlePointer2 = std::make_shared<vlasenko::Circle>(circle2);
  std::shared_ptr<vlasenko::Shape> rectanglePointer1 = std::make_shared<vlasenko::Rectangle>(rectangle1);
  std::shared_ptr<vlasenko::Shape> rectanglePointer2 = std::make_shared<vlasenko::Rectangle>(rectangle2);

  vlasenko::CompositeShape compositeShape(circlePointer);
  compositeShape.add(rectanglePointer1);
  compositeShape.add(circlePointer2);
  compositeShape.add(rectanglePointer2);

  vlasenko::Matrix<vlasenko::Shape> matrix = vlasenko::part(compositeShape);

  BOOST_CHECK(matrix[0][0] == circlePointer);
  BOOST_CHECK(matrix[0][1] == circlePointer2);
  BOOST_CHECK(matrix[1][0] == rectanglePointer1);
  BOOST_CHECK(matrix[2][0] == rectanglePointer2);

  //check that we have different size of rows
  BOOST_CHECK_EQUAL(matrix.getRows(), 3);
  BOOST_CHECK_EQUAL(matrix[0].size(), 2);
  BOOST_CHECK_EQUAL(matrix[1].size(), 1);
  BOOST_CHECK_EQUAL(matrix[2].size(), 1);

  const vlasenko::Rectangle rectangle3({6.0, 6.0}, 2.0, 2.0);
  std::shared_ptr<vlasenko::Shape> rectanglePointer3 = std::make_shared<vlasenko::Rectangle>(rectangle3);

  compositeShape.add(rectanglePointer3);
  matrix = vlasenko::part(compositeShape);

  BOOST_CHECK(matrix[1][1] == rectanglePointer3);
  //check that we have different size of rows once again(after adding of new shape)
  BOOST_CHECK_EQUAL(matrix.getRows(), 3);
  BOOST_CHECK_EQUAL(matrix[0].size(), 2);
  BOOST_CHECK_EQUAL(matrix[1].size(), 2);
  BOOST_CHECK_EQUAL(matrix[2].size(), 1);
}

BOOST_AUTO_TEST_CASE(testIntersect)
{
  const vlasenko::Rectangle rect({0.0, 0.0}, 6.0, 5.0);

  const vlasenko::Rectangle rectangle1({3.0, 3.0}, 4.0, 4.0);
  const vlasenko::Rectangle rectangle2({-3.0, 2.0}, 4.0, 2.0);
  const vlasenko::Rectangle rectangle3({-3.0, -2.0}, 2.0, 2.0);
  const vlasenko::Rectangle rectangle4({3.0, -2.0}, 2.0, 2.0);
  const vlasenko::Circle circle({30.0, -10.0}, 4.0);

  //check that frame rectangles intersect from different sides
  BOOST_CHECK(vlasenko::intersect(rect.getFrameRect(), rectangle1.getFrameRect()));
  BOOST_CHECK(vlasenko::intersect(rect.getFrameRect(), rectangle2.getFrameRect()));
  BOOST_CHECK(vlasenko::intersect(rect.getFrameRect(), rectangle3.getFrameRect()));
  BOOST_CHECK(vlasenko::intersect(rect.getFrameRect(), rectangle4.getFrameRect()));

  //check that frame rectangles don't intersect
  BOOST_CHECK(!vlasenko::intersect(rect.getFrameRect(), circle.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
