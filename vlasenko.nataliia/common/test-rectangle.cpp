#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

const double FAULT = 0.01;

BOOST_AUTO_TEST_SUITE(rectangleMethodsTesting)

BOOST_AUTO_TEST_CASE(rectangleAfterTransferingTesting)
{
  vlasenko::Rectangle rectangleTest({ 2.5, 7.5 }, 5.0, 2.0);

  const double rectangleAreaBeforeMoving = rectangleTest.getArea();
  const vlasenko::rectangle_t frameRectBeforeMoving = rectangleTest.getFrameRect();

  rectangleTest.move(7.2, 2.3);

  BOOST_CHECK_CLOSE(rectangleTest.getArea(), rectangleAreaBeforeMoving, FAULT);

  vlasenko::rectangle_t frameRectAfterMoving = rectangleTest.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, FAULT);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, FAULT);
}

BOOST_AUTO_TEST_CASE(rectangleAfterMovingTesting)
{
  vlasenko::Rectangle rectangleTest({ 2.5, 7.5 }, 5.0, 2.0);

  const double rectangleAreaBeforeMoving = rectangleTest.getArea();
  const vlasenko::rectangle_t frameRectBeforeMoving = rectangleTest.getFrameRect();

  rectangleTest.move({ 2.5,-8.1 });

  BOOST_CHECK_CLOSE(rectangleTest.getArea(), rectangleAreaBeforeMoving, FAULT);

  vlasenko::rectangle_t frameRectAfterMoving = rectangleTest.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, FAULT);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, FAULT);
}

BOOST_AUTO_TEST_CASE(rectangleScaleTesting)
{
  vlasenko::Rectangle rectangleTest({ 2.5, 2.5 }, 3.0, 5.0);

  const double rectangleAreaBeforeScale = rectangleTest.getArea();
  const vlasenko::rectangle_t frameRectBeforeScale = rectangleTest.getFrameRect();
  const double scaleTesting = 2.5;

  rectangleTest.scale(scaleTesting);

  BOOST_CHECK_CLOSE(rectangleTest.getArea(), rectangleAreaBeforeScale * scaleTesting * scaleTesting, FAULT);

  const vlasenko::rectangle_t frameRectAfterScale = rectangleTest.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfterScale.height, frameRectBeforeScale.height * scaleTesting, FAULT);
  BOOST_CHECK_CLOSE(frameRectAfterScale.width, frameRectBeforeScale.width * scaleTesting, FAULT);
}

BOOST_AUTO_TEST_CASE(rectangleThrowExceptionAfterScaleTesting)
{
  vlasenko::Rectangle rectangleTest({2.1, 5.3}, 5.5, 6.2);

  BOOST_CHECK_THROW(rectangleTest.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleWidthExceptionTesting)
{
  BOOST_CHECK_THROW(vlasenko::Rectangle({ 2.1,2.4 }, -2.2, -5.8), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleHeightExceptionTesting)
{
  BOOST_CHECK_THROW(vlasenko::Rectangle({ 2.1,2.4 }, 2.2, -5.8), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleRotateTesting)
{
  vlasenko::Rectangle rectangleTest({5.0, 5.0}, 2.0, 5.0);
  const double rectangleAreaBefore = rectangleTest.getArea();
  const vlasenko::rectangle_t frameRectBeforeRotate = rectangleTest.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, 5.0, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, 2.0, FAULT);
  double angle = 90.0;
  rectangleTest.rotate(angle);
  double rectangleAreaAfter = rectangleTest.getArea();
  vlasenko::rectangle_t frameRectAfterRotate = rectangleTest.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.width, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.height, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, FAULT);
  BOOST_CHECK_CLOSE(rectangleAreaBefore, rectangleAreaAfter, FAULT);

  angle = -90;
  rectangleTest.rotate(angle);
  rectangleAreaAfter = rectangleTest.getArea();
  frameRectAfterRotate = rectangleTest.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.height, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.width, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, FAULT);
  BOOST_CHECK_CLOSE(rectangleAreaBefore, rectangleAreaAfter, FAULT);

  angle = 180;
  rectangleTest.rotate(angle);
  rectangleAreaAfter = rectangleTest.getArea();
  frameRectAfterRotate = rectangleTest.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.height, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.width, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, FAULT);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, FAULT);
  BOOST_CHECK_CLOSE(rectangleAreaBefore, rectangleAreaAfter, FAULT);
}

BOOST_AUTO_TEST_SUITE_END()
