#include <iostream>
#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "partition.hpp"

int main()
{
  std::cout << "\t  //////////Rectangle//////////\n";
  vlasenko::Rectangle rectangle({5.3, 5.2}, 15, 10);
  vlasenko::Shape * shape = &rectangle;
  shape->printInfo();
  std::cout << "Transferinging to (2.3,2.5)...\n";
  shape->move(2.3, 2.5);
  shape->printInfo();
  std::cout << "Moving to (10.0,24.0)...\n";
  shape->move({10.0, 24.0});
  shape->printInfo();
  std::cout << "Scale in 2.0...\n";
  shape->scale(2.0);
  shape->printInfo();
  std::cout << "Rotate by 45 degrees...\n";
  shape->rotate(45.0);
  shape->printInfo();

  std::cout << "\t  //////////Circle//////////\n";
  vlasenko::Circle circle({3.0, 3.5}, 5.7);
  shape = &circle;
  shape->printInfo();
  std::cout << "\t  Transfering to (7.0,1.0)...\n";
  shape->move(7.0, 1.0);
  shape->printInfo();
  std::cout << "\t  Moving to (-27.0,9.0)...\n";
  shape->move({-27.0, 9.0});
  shape->printInfo();
  std::cout << "\t  Scale in 0.5 times...\n";
  shape->scale(0.5);
  shape->printInfo();
  std::cout << "\t  Rotate by 45 degrees...\n";
  shape->rotate(45.0);
  shape->printInfo();

  vlasenko::Circle circle1({1.2, 1.2}, 3.0);
  vlasenko::Circle circle2({-0.2, 5.3}, 2.0);
  vlasenko::Rectangle rect1({1.6, 1.6}, 5.0, 3.0);
  vlasenko::Rectangle rect2({-7.3, 2.6}, 8.2, 5.3);

  std::cout << "\t  Create composite shape:\n";
  vlasenko::CompositeShape compositeShape(std::make_shared<vlasenko::Circle>(circle1));

  compositeShape.printInfo();

  std::cout << "\t  Add other shapes in composite:\n";
  compositeShape.add(std::make_shared<vlasenko::Rectangle>(rect1));
  compositeShape.add(std::make_shared<vlasenko::Rectangle>(rect2));
  compositeShape.add(std::make_shared<vlasenko::Circle>(circle2));
  compositeShape.printInfo();

  std::cout << "\t  Transfering at (3,3)\n";
  compositeShape.move(3.0, 3.0);
  compositeShape.printInfo();

  std::cout << "\t  Removing shapes which have index 2 and 3\n";
  compositeShape.remove(3);
  compositeShape.remove(2);
  compositeShape.printInfo();

  std::cout << "\t  Moving to {-4.0,7.0}\n";
  compositeShape.move({-4.0, 7.0});
  compositeShape.printInfo();

  std::cout << "\t  Get shape which has index 0 and print information about it:\n";
  std::shared_ptr<vlasenko::Shape> shapeFromComposite = compositeShape[0];
  shapeFromComposite->printInfo();

  std::cout << "\t  Scale composite shape in 0,8 times\n\n";
  compositeShape.scale(0.8);
  compositeShape.printInfo();

  std::cout << "\t  Add new shape\n";
  compositeShape.add(std::make_shared<vlasenko::Circle>(circle2));
  compositeShape.printInfo();

  std::cout << "\t  Rotate composite shape by 30 degrees\n";
  compositeShape.rotate(30.0);
  compositeShape.printInfo();

  std::cout << "\t  Get partition of composite shape\n";
  vlasenko::Matrix<vlasenko::Shape> matrix = vlasenko::part(compositeShape);
  std::size_t layer = matrix.getRows();
  for (std::size_t i = 0; i < layer; ++i)
  {
    std::cout << "\t  Layer: " << i + 1 << "\n";
    std::cout << "\t  Shapes in the layer:\n";
    std::size_t columns = matrix[i].size();
    for (std::size_t j = 0; j < columns; ++j)
    {
      if (matrix[i][j] != nullptr)
      {
        std::cout << j + 1 << ":\n";
        std::cout << "\t  Area: " << matrix[i][j]->getArea() << "\n";
        const vlasenko::rectangle_t frameRect = matrix[i][j]->getFrameRect();
        std::cout << "\t  Position: (" << frameRect.pos.x << "," << frameRect.pos.y << ")\n";
      }
    }
  }

  return 0;
}
