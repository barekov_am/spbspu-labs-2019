#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

class Circle : public Shape{

public:
  Circle(const Point &center);
  virtual void draw(std::ostream &outstream) const;
};
#endif
