#include <iostream>

void task1();
void task2();

int main(int argc, char *argv[])
{
  if (argc != 2)
  {
    std::cerr << "Incorrect count of arguments"<<std::endl;
    return 1;
  }

  try {
    char* ptr = nullptr;
    const int switcher = std::strtoll(argv[1], &ptr, 10);

    if (*ptr != '\x00')
    {
      std::cerr << "...wrong argument..."<<std::endl;
      return 1;
    }

    switch(switcher) {
    case 1: {
      task1();
      break;
    }
    case 2: {
      task2();
      break;
    }
    default: {
      std::cerr << "...wrong number of task..."<<std::endl;
      return 1;
    }
    }
  } catch (const std::exception& ex) {
    std::cerr << ex.what();
    return 1;
  }
  return 0;
}
