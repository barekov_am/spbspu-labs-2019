#ifndef SQUARE_HPP
#define SQUARE_HPP
#include "shape.hpp"

class Square : public Shape {

public:
  Square(const Point &center);
  virtual void draw(std::ostream &outstream) const;
};
#endif
