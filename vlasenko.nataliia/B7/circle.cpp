#include "circle.hpp"

Circle::Circle(const Point &center) :
  Shape(center)
{}

void Circle::draw(std::ostream &outstream) const {
    outstream << "CIRCLE (" << getPoint().x << ';' << getPoint().y << ")"<<std::endl;
}
