#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <memory>
#include <list>

#include "triangle.hpp"
#include "circle.hpp"
#include "square.hpp"

std::shared_ptr<Shape> create(const std::string &type, const Point &center);
std::shared_ptr<Shape> parse(std::string &line);

int checkingNum(std::string &number);
Point getPoint(std::string &coordinates);
void printShapes(const std::list<std::shared_ptr<Shape>> &shapes);
#endif
