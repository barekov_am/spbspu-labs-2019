#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>
#include "point.hpp"

class Shape{

public:
  Shape(const Point &center);
  virtual ~Shape() = default;

  bool isUpper(const Shape &shape) const;
  bool isMoreLeft(const Shape &shape) const;
  Point getPoint() const;

  virtual void draw(std::ostream &outstream) const = 0;

private:
  Point center_;

};
#endif
