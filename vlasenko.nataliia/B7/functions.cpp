#include <algorithm>

#include "functions.hpp"

void printShapes(const std::list<std::shared_ptr<Shape>> &shapes) {

    std::for_each(std::begin(shapes), std::end(shapes), [&](const std::shared_ptr<Shape> &shape) {
        shape->draw(std::cout);});
}

int checkingNum(std::string &number){

    number.erase(std::remove_if(std::begin(number), std::end(number), [&](char c)
                     { return ((c == ' ') || (c == '\t')); }), std::end(number));

    char* ptr = nullptr;
    const int num = std::strtoll(number.c_str(), &ptr, 10);

    if (*ptr != '\x00'){
        throw std::invalid_argument("...ptr isnt NULL...\n");
        return 1;
    }

    return num;
}

std::shared_ptr<Shape> create(std::string &type, const Point &center) {

  if (type == "TRIANGLE") {
    return std::make_shared<Triangle>(Triangle(center));
  }

  if (type == "CIRCLE") {
    return std::make_shared<Circle>(Circle(center));
  }

  if (type == "SQUARE") {
    return std::make_shared<Square>(Square(center));
  }

  throw std::invalid_argument("...unknown shape...\n");
}

std::shared_ptr<Shape> parse(std::string &line) {

  std::size_t position = line.find_first_of('E');

  if (position == std::string::npos){
    throw std::invalid_argument("...unknown shape form...\n");
  }

  std::string type = line.substr(0, position + 1);
  std::size_t openBracket = line.find_first_of('(');
  std::size_t closeBracket = line.find_first_of(')');

  if ((openBracket == std::string::npos) || (closeBracket == std::string::npos)){
    throw std::invalid_argument("...unknown point form...\n");
  }

  std::string pointString = line.substr(openBracket, closeBracket - openBracket + 1);
  Point center = getPoint(pointString);
  return create(type, center);
}

Point getPoint(std::string &coordinates) {

  int coordPosition = coordinates.find_first_of(";");
  std::string xCoordinate = coordinates.substr(1, coordPosition - 1);
  std::string yCoordinate = coordinates.substr(coordPosition + 1, coordinates.length() - coordPosition - 2);

  return {checkingNum(xCoordinate), checkingNum(yCoordinate)};
}
