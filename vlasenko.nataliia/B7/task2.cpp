#include "functions.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "square.hpp"

void task2(){

  std::list<std::shared_ptr<Shape>> shapes;
  std::string nextLine;
  std::cin >> std::ws;

  while(std::getline(std::cin, nextLine)){

    if (std::cin.fail()){
      throw std::ios_base::failure("...input failed...\n");
    }

    if (nextLine == ""){
      continue;
    } else {
      shapes.push_back(parse(nextLine));
      std::cin >> std::ws;
    }

  }

  std::cout << "Original:" << std::endl;
  printShapes(shapes);

  shapes.sort([](const std::shared_ptr<Shape> &shape1, const std::shared_ptr<Shape> shape2) {
    return shape1->isMoreLeft(*shape2); });

  std::cout << "Left-Right:" << std::endl;
  printShapes(shapes);

  shapes.sort([](const std::shared_ptr<Shape> &shape1, const std::shared_ptr<Shape> shape2) {
    return shape2->isMoreLeft(*shape1); });

  std::cout << "Right-Left:" << std::endl;
  printShapes(shapes);

  shapes.sort([](const std::shared_ptr<Shape> &shape1, const std::shared_ptr<Shape> shape2) {
    return shape1->isUpper(*shape2); });

  std::cout << "Top-Bottom:" << std::endl;
  printShapes(shapes);

  shapes.sort([](const std::shared_ptr<Shape> &shape1, const std::shared_ptr<Shape> shape2) {
    return shape2->isUpper(*shape1);  });

  std::cout << "Bottom-Top:" << std::endl;
  printShapes(shapes);

}
