#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

class Triangle : public Shape {

public:
  Triangle(const Point &center);
  virtual void draw(std::ostream &outstream) const;
};
#endif
