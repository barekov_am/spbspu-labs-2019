#include "triangle.hpp"

Triangle::Triangle(const Point &center) :
  Shape(center)
{}

void Triangle::draw(std::ostream &outstream) const{
    outstream << "TRIANGLE (" << getPoint().x << ";" << getPoint().y << ")"<< std::endl;
}
