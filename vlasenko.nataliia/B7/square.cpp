#include "square.hpp"

Square::Square(const Point &center) :
  Shape(center)
{}

void Square::draw(std::ostream &outstream) const {
    outstream << "SQUARE (" << getPoint().x << ";" << getPoint().y << ")"<<std::endl;
}
