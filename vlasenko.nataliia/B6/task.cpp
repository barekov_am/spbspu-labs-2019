#include <iostream>
#include <algorithm>
#include <iterator>
#include "statistic.hpp"

void task(){
  Statistic statistic;
  statistic = std::for_each(std::istream_iterator<long long>(std::cin),
                                    std::istream_iterator<long long>(), statistic);
  if(!std::cin.eof()){
    throw std::ios_base::failure("Input failed\n");
  }

  if (statistic.empty()){
    std::cout << "No Data\n";
  }
  else{
    std::cout << "Max: " << statistic.getMax() << std::endl;
    std::cout << "Min: " << statistic.getMin() << std::endl;
    std::cout << "Mean: " << statistic.getMean() << std::endl;
    std::cout << "Positive: " << statistic.getAmountOfPositive() << std::endl;
    std::cout << "Negative: " << statistic.getAmountOfNegative() << std::endl;
    std::cout << "Odd Sum: " << statistic.getOddSum() << std::endl;
    std::cout << "Even Sum: " << statistic.getEvenSum() << std::endl;
    std::cout << "First/Last Equal: " << (statistic.equalBorderElements() ? "yes" : "no") << std::endl;
  }
}
