#include <limits>
#include "statistic.hpp"

Statistic::Statistic() :

  max(std::numeric_limits<long long int>::min()),
  min(std::numeric_limits<long long int>::max()),

  sum(0),
  amount(0),
  positives(0),
  negatives(0),
  oddSum(0),
  evenSum(0),
  first(0),

  equalElements(false){}

void Statistic::operator ()(long long nextNumber){

  if (amount == 0){
    first = nextNumber;
  }

  ++amount;
  sum += nextNumber;

  if (nextNumber > max){
    max = nextNumber;
  }

  if (nextNumber < min){
    min = nextNumber;
  }

  if (nextNumber > 0){
    ++positives;
  }

  if (nextNumber < 0){
    ++negatives;
  }

  if (nextNumber % 2){
    oddSum += nextNumber;
  }
  else{
    evenSum += nextNumber;
  }

  equalElements = first == nextNumber;
}

long long Statistic::getMax(){
  return max;
}

long long Statistic::getMin(){
  return min;
}

double Statistic::getMean(){
  return static_cast<double>(oddSum + evenSum) / amount;
}

long long Statistic::getAmountOfPositive(){
  return positives;
}

long long Statistic::getAmountOfNegative(){
  return negatives;
}

long long Statistic::getOddSum(){
  return oddSum;
}

long long Statistic::getEvenSum(){
  return evenSum;
}

bool Statistic::equalBorderElements(){
  return equalElements;
}

bool Statistic::empty(){
  return amount == 0;
}
