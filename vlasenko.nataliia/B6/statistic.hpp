#ifndef STATISTIC_HPP
#define STATISTIC_HPP

class Statistic{
public:
  Statistic();
  void operator()(long long nextNumber);
  long long getMax();
  long long getMin();
  double getMean();
  long long getAmountOfPositive();
  long long getAmountOfNegative();
  long long getOddSum();
  long long getEvenSum();
  bool equalBorderElements();
  bool empty();

private:

  long long max;
  long long min;
  long long sum;
  long long amount;
  long long positives;
  long long negatives;
  long long oddSum;
  long long evenSum;
  long long first;
  bool equalElements;
};

#endif
