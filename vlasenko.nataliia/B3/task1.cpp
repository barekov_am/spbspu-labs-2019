#include <iostream>
#include <sstream>
#include "phonebook-bookmarks.hpp"
#include "commands.hpp"

void task1()
{
  BookmarksControl manager;

  std::string line;

  while (std::getline(std::cin, line))
  {
    std::stringstream input(line);

    commands::parseTask(input)(manager, std::cout);
  }
}
