#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <functional>
#include <istream>
#include "phonebook-bookmarks.hpp"

namespace commands
{
  using insert_position = BookmarksControl::InsertPosition;
  using move_position = BookmarksControl::MovePosition;
  using execute_command = std::function<void(BookmarksControl&, std::ostream&)>;

  execute_command parseTask(std::istream& input);

  execute_command parseAdd(std::istream& input);
  execute_command parseStore(std::istream& input);
  execute_command parseInsert(std::istream& input);
  execute_command parseDelete(std::istream& input);
  execute_command parseShow(std::istream& input);
  execute_command parseMove(std::istream& input);
}

namespace detail
{
  std::string readBookmarkFromStream(std::istream& input);
  std::string readNumberFromStream(std::istream& input);
  std::string readNameFromStream(std::istream& input);
}

namespace controllers
{
    template <typename Char, typename Traits>
    std::basic_istream<Char, Traits>& blank(std::basic_istream<Char, Traits>& input)
    {
        using istream_type = std::basic_istream<Char, Traits>;
        using streambuf_type = std::basic_streambuf<Char, Traits>;
        using int_type = typename istream_type::int_type;
        using ctype_type = std::ctype<Char>;

        const ctype_type& ct = std::use_facet<ctype_type>(input.getloc());
        const int_type eof = Traits::eof();
        streambuf_type* sb = input.rdbuf();
        int_type c = sb->sgetc();

        while (!Traits::eq_int_type(c, eof) && ct.is(std::ctype_base::blank, Traits::to_char_type(c)))
        {
            c = sb->snextc();
        }

        if (Traits::eq_int_type(c, eof))
        {
            input.setstate(std::ios_base::eofbit);
        }

        return input;
    }
}


#endif
