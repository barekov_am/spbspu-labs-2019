#include "phonebook-bookmarks.hpp"
#include <algorithm>
#include <iostream>

BookmarksControl::BookmarksControl()
{
  bookmarks_["current"] = records_.end();
}

void BookmarksControl::add(const Phonebook::record_t& record)
{
  records_.push_back(record);

  if (std::next(records_.begin()) == records_.end())
  {
    bookmarks_["current"] = records_.begin();
  }
}

void BookmarksControl::store(const std::string& bookmark, const std::string& newBookmark)
{
  auto it = getElementIterator(bookmark);

  bookmarks_.emplace(newBookmark, it->second);
}

void BookmarksControl::insert(const std::string& bookmark, const Phonebook::record_t& record, InsertPosition position)
{
  auto it = getElementIterator(bookmark);

  if (it->second == records_.end())
  {
    add(record);

    return;
  }

  auto pos = (position == InsertPosition::before) ? it->second : std::next(it->second);

  records_.insert(pos, record);
}

void BookmarksControl::remove(const std::string& bookmark)
{
  auto it = getElementIterator(bookmark);

  if (it->second == records_.end())
  {
    throw std::invalid_argument("The element to remove does not exist.");
  }

  auto current = it->second;

  std::for_each(bookmarks_.begin(), bookmarks_.end(),
                [&](auto& bookmark){ if (bookmark.second == current) bookmark.second = std::next(current); });

  records_.erase(current);

  std::for_each(bookmarks_.begin(), bookmarks_.end(),
                [&](auto& bookmark){ if (bookmark.second == records_.end()) bookmark.second = std::prev(records_.end()); });
}

void BookmarksControl::show(const std::string& bookmark, std::ostream& output)
{
  auto it = getElementIterator(bookmark);

  if (it->second == records_.end())
  {
    throw std::invalid_argument("The records list is empty.");
  }

  output << it->second->number << " " << it->second->name << "\n";
}

void BookmarksControl::move(const std::string& bookmark, int steps)
{
  auto it = getElementIterator(bookmark);

  std::advance(it->second, steps);
}

void BookmarksControl::move(const std::string& bookmark, MovePosition position)
{
  auto it = getElementIterator(bookmark);

  auto newPos = (position == MovePosition::first) ? records_.begin() : std::prev(records_.end());

  it->second = newPos;
}

void BookmarksControl::moveNext(const std::string& bookmark)
{
  move(bookmark, 1);
}

void BookmarksControl::movePrev(const std::string& bookmark)
{
  move(bookmark, -1);
}

void BookmarksControl::replace(const std::string& bookmark, const Phonebook::record_t& record)
{
  auto it = getElementIterator(bookmark);

  *(it->second) = record;
}

bool BookmarksControl::bookmarkExists(const std::string& bookmark) const
{
  auto it = bookmarks_.find(bookmark);

  return it != bookmarks_.end();
}

bool BookmarksControl::bookmarkValid(const std::string& bookmark)
{
  auto it = getElementIterator(bookmark);

  return it->second != records_.end();
}

bool BookmarksControl::recordsEmpty() const
{
  return records_.empty();
}

BookmarksControl::bookmarks::iterator BookmarksControl::getElementIterator(const std::string& bookmark)
{
  auto it = bookmarks_.find(bookmark);

  if (it != bookmarks_.end())
  {
    return it;
  }

  throw std::invalid_argument("The bookmark does not exist.");
}
