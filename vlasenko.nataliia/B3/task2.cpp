#include <algorithm>
#include <iostream>
#include "factorial-container.hpp"

void task2()
{
  FactorialContainer fcontainer;

  std::copy(fcontainer.begin(), fcontainer.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << "\n";

  std::reverse_copy(fcontainer.begin(), fcontainer.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << "\n";
}
