#include <iostream>
#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

int main()
{
  std::cout << "\t  ///////////Rectangle///////////\n";
  vlasenko::Rectangle rectangle({5.3, 5.2}, 15, 10);
  vlasenko::Shape * shape = &rectangle;
  shape->printInfo();
  std::cout << "\t  Transfering to (2.3,2.5)...\n";
  shape->move(2.3, 2.5);
  shape->printInfo();
  std::cout << "\t  Moving to (10,24)...\n";
  shape->move({10, 24});
  shape->printInfo();
  std::cout << "\t  Scale in twice...\n";
  shape->scale(2);
  shape->printInfo();

  std::cout << "\t  ////////////Circle////////////\n";
  vlasenko::Circle circle({3.0, 3.5}, 5.7);
  shape = &circle;
  shape->printInfo();
  std::cout << "\t  Transfering to (7,1)...\n";
  shape->move(7, 1);
  shape->printInfo();
  std::cout << "\t  Moving to (-27,9)...\n";
  shape->move({-27, 9});
  shape->printInfo();
  std::cout << "\t  Scale in 0.5 times...\n";
  shape->scale(0.5);
  shape->printInfo();
  vlasenko::Circle circle1({5.5, 5.0}, 1.5);
  vlasenko::Circle circle2({-0.5, -5.5}, 2.5);
  vlasenko::Rectangle rectangle1({-0.7, 0.7}, 2.0, 4.0);
  vlasenko::Rectangle rectangle2({5.7, 1.7}, 7.7, 5.7);

  std::cout << "\t Creating the composite shape...\n";
  vlasenko::CompositeShape compositeShape(std::make_shared<vlasenko::Circle>(circle1));

  compositeShape.printInfo();

  std::cout << "\t Adding other shapes...\n";
  compositeShape.add(std::make_shared<vlasenko::Rectangle>(rectangle2));
  compositeShape.add(std::make_shared<vlasenko::Circle>(circle1));
  compositeShape.add(std::make_shared<vlasenko::Circle>(circle2));
  compositeShape.printInfo();

  std::cout << "\t Transfering shapes to (7,9)...\n";
  compositeShape.move(7, 9);
  compositeShape.printInfo();

  std::cout << "\t Removing shapes with index 1 and 3...\n";
  compositeShape.remove(1);
  compositeShape.remove(3);
  compositeShape.printInfo();

  std::cout << "\t Moving shapes on {-7,7}...\n";
  compositeShape.move({-7, 7});
  compositeShape.printInfo();

  std::cout << "\t Print information about it shape with index 2...\n";
  std::shared_ptr<vlasenko::Shape> shapeFromComposite = compositeShape[0];
  shapeFromComposite->printInfo();

  std::cout << "\t Scale composite shape in 0,5 times\n";
  compositeShape.scale(0.5);
  compositeShape.printInfo();

  return 0;
}
