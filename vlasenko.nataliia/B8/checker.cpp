#include "checker.hpp"

Checker::Checker(std::ostream &out, token_list &list) :
  out_(out),
  list_(list)
{
  if (!isValid()) {
    throw std::ios_base::failure("...incorrect input...\n");
  }
}

void Checker::print(size_t lineWidth) {

  token_list line;
  size_t currentWidth = 0;

  for (auto &element : list_) {
    switch(element.type) {

      case tokenType::PUNCTUATION:

        if (currentWidth + lengthOfPunct > lineWidth) {
          currentWidth = dashAndPunctuationPrint(line);
        }

        line.push_back(element);
        currentWidth += (element).value.size();
        break;

      case tokenType::DASH:

        if (currentWidth + lengthOfDash + lengthOfSpace > lineWidth) {
          currentWidth = dashAndPunctuationPrint(line);
        }

        line.push_back(token_t{" ", tokenType::WS});
        line.push_back(element);
        currentWidth += element.value.size() + 1;
        break;

      case tokenType::WORD:

      case tokenType::NUMBER:

        if (currentWidth + element.value.size() + lengthOfSpace > lineWidth) {
          printLine(line);
          line.clear();
          currentWidth = 0;

        } else if (!line.empty()) {
          line.push_back(token_t{" ", tokenType::WS});
          currentWidth++;
        }

        line.push_back(element);
        currentWidth += element.value.size();
        break;

      case tokenType::WS:

        break;
    }
  }

  if (!line.empty()) {
    printLine(line);
  }
}

void Checker::printLine(const token_list &line) {

  for (const auto &element : line) {
    out_ << element.value;
  }
  out_ << "\n";
}

int Checker::dashAndPunctuationPrint(token_list &list) {
  size_t width = 0;
  token_list line;
  while (!list.empty()) {

    line.push_front(list.back());
    width += list.back().value.size();
    list.pop_back();
    if (line.front().type == tokenType::WORD || line.front().type == tokenType::NUMBER) {

      break;
    }
  }
  printLine(list);
  list = line;
  return width;
}

bool Checker::isValid() {

  if (!list_.empty() && (list_.front().type != tokenType::WORD) && (list_.front().type != tokenType::NUMBER)) {
    return false;
  }

  for (auto element = list_.begin(); element != list_.end(); element++) {

    switch(element->type) {

      case tokenType::WORD:

      case tokenType::NUMBER:

        if (element->value.size() > 20) {
          return false;
        }
        break;

      case tokenType::DASH:

        if (element->value.size() != 3) {
          return false;
        }

        if (element != list_.begin()) {
          const token_t &prev = *std::prev(element);

          if ((prev.type == tokenType::DASH) || ((prev.type == tokenType::PUNCTUATION) && (prev.value != ","))) {
            return false;
          }
        }
        break;
      case tokenType::PUNCTUATION:

        if (element != list_.begin()) {
          const token_t &prev = *std::prev(element);

          if ((prev.type == tokenType::DASH) || (prev.type == tokenType::PUNCTUATION)) {
            return false;
          }
        }
        break;
      case tokenType::WS:
        break;
    }
  }
  return true;
}
