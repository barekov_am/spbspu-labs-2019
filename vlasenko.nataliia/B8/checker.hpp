#ifndef CHECKER_HPP
#define CHECKER_HPP

#include <iostream>

#include "token.hpp"

class Checker {

public:
  Checker(std::ostream &, token_list &);
  void print(size_t);
  
private:
  std::ostream & out_;
  token_list &list_;
  
  int dashAndPunctuationPrint(token_list &);
  void printLine(const token_list &);
  bool isValid();
  
};

#endif
