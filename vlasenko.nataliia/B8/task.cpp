#include "parser.hpp"
#include "checker.hpp"

void task1(const size_t lineWidth) {
  Parser parser(std::cin);

  parser.parseInput();
    
  token_list tokenList(parser.begin(), parser.end());
  Checker output(std::cout, tokenList);
  output.print(lineWidth);
}
