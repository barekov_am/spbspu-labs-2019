#ifndef TOKEN_HPP
#define TOKEN_HPP

#include <string>
#include <list>

constexpr size_t lengthOfDash = 3;
constexpr size_t lengthOfSpace = 1;
constexpr size_t lengthOfPunct = 1;

enum tokenType {
  DASH,
  NUMBER,
  PUNCTUATION,
  WORD,
  WS
};

struct token_t {
  std::string value;
  tokenType type;
};

typedef std::list<token_t> token_list;

#endif
