#include <fstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <vector>

const size_t bufferSize = 1024;

void task2(const char* filename){
  using charArray = std::unique_ptr<char[], decltype(&free)>;

  std::ifstream inputFile(filename);
  if (!inputFile){
    throw std::ios_base::failure("Cannot open file");
  }

  size_t size = bufferSize;

  charArray arr(static_cast<char*>(malloc(size)), &free);

  size_t count = 0;
  while (inputFile){
    inputFile.read(&arr[count], bufferSize);
    count += inputFile.gcount();

    if (inputFile.gcount() == bufferSize){
      size += bufferSize;
      charArray temp(static_cast<char*>(realloc(arr.get(), size)), &free);

      if (temp){
        arr.release();
        std::swap(arr, temp);
      }
      else{
        throw std::runtime_error("Cannot reallocate");
      }
    }
  }
  inputFile.close();

  if (inputFile.is_open()){
    throw std::ios_base::failure("Cannot close file.");
  }
  
  std::vector<char> vector(&arr[0], &arr[count]);

  for (const auto& it: vector){
    std::cout << it;
  } 
}
