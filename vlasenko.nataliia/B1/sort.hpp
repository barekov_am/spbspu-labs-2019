#ifndef SORT_HPP
#define SORT_HPP

#include <functional>

#include "accesses.hpp"

enum class Direction
{
  ascending,
  descending
};

Direction getDirection(const char* direction);

template <template <class T> class Access, typename T>
void sort(T& vector, Direction direction)
{
  using value_type = typename T::value_type;

  std::function<bool(value_type, value_type)> compare = (direction == Direction::ascending)
      ? std::function<bool(value_type, value_type)>(std::less<value_type>())
      : std::function<bool(value_type, value_type)>(std::greater<value_type>());

  const auto begin = Access<T>::begin(vector);
  const auto end = Access<T>::end(vector);

  for (auto i = begin; i != end; ++i)
  {
    auto tmp = i;
    for (auto j = Access<T>::next(i); j != end; ++j)
    {
      if (compare(Access<T>::element(vector, j), Access<T>::element(vector, tmp)))
      {
        tmp = j;
      }
    }

    if (tmp != i)
    {
      std::swap(Access<T>::element(vector, tmp), Access<T>::element(vector, i));
    }
  }
}

#endif
