#include <forward_list>
#include <vector>

#include "print.hpp"
#include "sort.hpp"

void task1(const char* direction){
  std::vector<int> vectorBrackets;
  Direction dir = getDirection(direction);
  int num = 0;

  while (std::cin >> num){
    vectorBrackets.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail()){
   throw std::ios_base::failure("Some problems with reading data");
  }

  if (vectorBrackets.empty()){
    return;
  }

  std::vector<int> vectorAt = vectorBrackets;
  std::forward_list<int> listIterator(vectorBrackets.begin(), vectorBrackets.end());

  sort<BracketsAccess>(vectorBrackets, dir);
  print(vectorBrackets);

  sort<AtAccess>(vectorAt, dir);
  print(vectorAt);

  sort<IteratorAccess>(listIterator, dir);
  print(listIterator);
}
