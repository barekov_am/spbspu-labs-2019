#ifndef PRINT_HPP
#define PRINT_HPP

#include <iostream>

template <typename T>
void print(const T& vector){
  if (vector.empty()){
    return;
  }

  for (const auto& i: vector){
    std::cout << i << " ";
  }

  std::cout << '\n';
}

#endif
