#ifndef ACCESSES_HPP
#define ACCESSES_HPP

#include <iterator>

template <typename T>
struct BracketsAccess{
  typedef size_t index;

  static index begin(const T&){
    return 0;
  }

  static index end(const T& vector){
    return vector.size();
  }

  static typename T::reference element(T& vector, index i){
    return vector[i];
  }

  static index next(index i){
    return i + 1;
  }
};

template <typename T>
struct AtAccess{
  typedef size_t index;

  static index begin(const T&){
    return 0;
  }

  static index end(const T& vector){
    return vector.size();
  }

  static typename T::reference element(T& vector, index i){
    return vector.at(i);
  }

  static index next(index i){
    return i + 1;
  }
};

template <typename T>
struct IteratorAccess{
  typedef typename T::iterator index;

  static index begin(T& vector){
    return vector.begin();
  }

  static index end(T& vector){
    return vector.end();
  }

  static typename T::reference element(T&, index& iterator){
    return *iterator;
  }

  static index next(index& iterator){
    return std::next(iterator);
  }
};

#endif
