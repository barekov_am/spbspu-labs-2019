#include <random>
#include <vector>
#include <ctime>
#include "print.hpp"
#include "sort.hpp"

void fillRandom(double* array, size_t size)
{
  std::mt19937 mersenne;
  std::uniform_real_distribution<double> distribution(-1.0, 1.0);
  mersenne.seed(time(0));
  for (size_t i = 0; i < size; ++i)
  {
    array[i] = distribution(mersenne);
  }
}

void task4(const char* direction, size_t size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size must be more than 0");
  }

  Direction dir = getDirection(direction);

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);
  print(vector);

  sort<BracketsAccess>(vector, dir);
  print(vector);
}
