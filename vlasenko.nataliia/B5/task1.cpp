#include "tasks.hpp"

#include <string>
#include <iostream>
#include <sstream>
#include <set>

void task1()
{
  std::string nextLine;
  std::set<std::string> setOfWords;

  while (std::getline(std::cin, nextLine)){
    if (std::cin.fail()){
      throw std::ios_base::failure("Input failed\n");
    }

    std::stringstream stream(nextLine);
    std::string nextWord;
    while (stream >> nextWord){
      if (setOfWords.find(nextWord) == setOfWords.end()){
        setOfWords.insert(nextWord);
      }
    }
  }

  for (std::string word : setOfWords){
    std::cout << word << "\n";
  }
}
