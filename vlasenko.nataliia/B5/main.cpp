#include <iostream>
#include "tasks.hpp"

enum tasks {
  FIRST = 1,
  SECOND
};

int main(int argc, char *argv[])
{
  if (argc != 2){
    std::cerr << "Wrong count of arguments";
    return 1;
  }
  try {
    char* ptr = nullptr;
    const int switcher = std::strtoll(argv[1], &ptr, 10);
    if (*ptr != '\x00'){
      std::cerr << "The argument is incorrect!\n";
      return 1;
    }

    switch(switcher) {
    case FIRST:{
      task1();
      break;
    }
    case SECOND:{
      task2();
      break;
    }
    default: {
      std::cerr << "The wrong number of task!\n";
      return 1;
    }
    }
  } catch (const std::exception& e) {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}



