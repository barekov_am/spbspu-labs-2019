#include <sstream>
#include <iostream>
#include <algorithm>

#include "functions.hpp"
#include "shape.hpp"
#include "tasks.hpp"

void task2()
{
  std::string nextLine;
  std::list<Shape> shapes;

  while (std::getline(std::cin, nextLine)){
    if (std::cin.fail()){
      throw std::ios_base::failure("Input failed\n");
    }

    std::stringstream stream(nextLine);
    std::string str;
    stream >> str;

    if (str == ""){
      continue;
    }

    int numberOfVertices = checkNumber(str);
    std::vector<point_t> points = formPointVector(stream, numberOfVertices);

    shapes.push_back(points);
  }

  int totalNumberOfVertices = 0;
  int numberOfTriangles = 0;
  int numberOfSquares = 0;
  int numberOfRectangles = 0;
  std::for_each(std::begin(shapes), std::end(shapes), [&](const Shape &shape) {
    totalNumberOfVertices += shape.size();
    if (shape.size() == NUMBER_OF_TRIANGLE_VERTICES){
      ++numberOfTriangles;
    }
    else if (isRectangle(shape)){
      ++numberOfRectangles;
      if (isSquare(shape)){
        ++numberOfSquares;
      }
    }
  });

  removePentagons(shapes);

  std::vector<point_t> oneOfShapePoints = formPointVector(shapes);

  shapes.sort([&](const Shape &shape1, const Shape &shape2) {
    if (shape1.size() < shape2.size()){
      return true;
    }
    if ((shape1.size() == NUMBER_OF_RECTANGLE_VERTICES) && (shape2.size() == NUMBER_OF_RECTANGLE_VERTICES) && isSquare(shape1)){
      return true;
    }

    return false;
  });

  std::cout << "Vertices: " << totalNumberOfVertices << "\n";
  std::cout << "Triangles: " << numberOfTriangles << "\n";
  std::cout << "Squares: " << numberOfSquares << "\n";
  std::cout << "Rectangles: " << numberOfRectangles << "\n";

  std::cout << "Points:";
  for (point_t nextPoint : oneOfShapePoints){
    std::cout << " (" << nextPoint.x << "; " << nextPoint.y << ")";
  }
  std::cout << "\n";

  std::cout << "Shapes:\n";
  for (Shape nextShape : shapes){
    std::cout << nextShape.size();
    for (point_t nextPoint : nextShape){
      std::cout << " (" << nextPoint.x << "; " << nextPoint.y << ")";
    }
    std::cout << "\n";
  }
}
