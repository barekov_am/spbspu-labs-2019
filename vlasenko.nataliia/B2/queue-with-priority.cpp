#include "queue-with-priority.hpp"

template < typename T >
bool QueueWithPriority< T >::putElement(const T & element, priority_t priority)
{
    switch (priority) {
        case priority_t::HIGH:
        {
            high_.insert(high_.end(), element);
            return true;
        }
        case priority_t::NORMAL:
        {
            normal_.insert(normal_.end(), element);
            return true;
        }
        case priority_t::LOW:
        {
            low_.insert(low_.end(), element);
            return true;
        }
        default:
        {
            return false;
        }
    }
}

template < typename T >
template < typename HandlerType >
bool QueueWithPriority< T >::pullFront(HandlerType handler)
{
    if (!high_.empty()) {
        handler(high_.front());
        high_.pop_front();
        return true;
    }

    if (!normal_.empty()) {
        handler(normal_.front());
        normal_.pop_front();
        return true;
    }

    if (!low_.empty()) {
        handler(low_.front());
        low_.pop_front();
        return true;
    }

    return false;
}

template < typename T >
void QueueWithPriority< T >::accelerate()
{
    high_.splice(high_.end(), low_);
}

template < typename T >
bool QueueWithPriority< T >::empty() const
{
    return high_.empty() && normal_.empty() && low_.empty();
}

template < typename T >
void QueueWithPriority< T >::clear()
{
    high_.clear();
    normal_.clear();
    low_.clear();
}
