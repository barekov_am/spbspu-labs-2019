#include "recognizer.hpp"

#include <algorithm>
#include <sstream>

const Recognizer::commandExecution Recognizer::invalidCommand = [](stringQueue &, std::ostream & out)
    { out << "<INVALID COMMAND>\n"; };

Recognizer::commandExecution Recognizer::parseCommand(std::string & args) {
  using recognizerExecution = std::function< Recognizer::commandExecution(std::string &) >;

  struct recognizers_t {
    const char * name;
    recognizerExecution execution;
  };

  static const recognizers_t recognizers[] {
    { "add", &executeAdd },
    { "get", &executeGet },
    { "accelerate", &executeAccelerate }
  };

  skipSpaces(args);
  std::string recognizerWord = extractWord(args);

  auto recognizer = std::find_if(recognizers, std::end(recognizers),
      [&](const recognizers_t & recognizer) { return recognizer.name == recognizerWord; });

  if (recognizer == std::end(recognizers)) {
    return invalidCommand;
  } else {
    return recognizer->execution(args);
  }
}

void Recognizer::skipSpaces(std::string & str)
{
  size_t numOfSpaces = 0;
  while (isblank(str[numOfSpaces])) {
    ++numOfSpaces;
  }

  str.erase(0, numOfSpaces);
}

std::string Recognizer::extractWord(std::string & str)
{
  if (str.empty()) {
    return "";
  }

  size_t lengthOfWord = 0;

  while (!isblank(str[lengthOfWord])) {
    ++lengthOfWord;
    if (lengthOfWord == str.length()) {
      break;
    }
  }

  std::string word = str.substr(0, lengthOfWord);
  str.erase(0, lengthOfWord);

  return word;
}

Recognizer::commandExecution Recognizer::executeAdd(std::string & args)
{
  using queuePriorities = stringQueue::priority_t;

  struct priority_t {
    const char * name;
    queuePriorities priority;
  };

  static const priority_t priorities[] {
    { "low", queuePriorities::LOW },
    { "normal", queuePriorities::NORMAL },
    { "high", queuePriorities::HIGH }
  };

  skipSpaces(args);
  std::string argPriority = extractWord(args);

  auto priority = std::find_if(priorities, std::end(priorities),
      [&](const priority_t & priority) { return priority.name == argPriority; });

  if (priority == std::end(priorities)) {
    return invalidCommand;
  }

  skipSpaces(args);
  std::string data = args;

  if (data.empty()) {
    return invalidCommand;
  }

  return [=](stringQueue & queue, std::ostream &) { queue.putElement(data, priority->priority); };
}

Recognizer::commandExecution Recognizer::executeGet(std::string & args)
{
  skipSpaces(args);
  std::string line = args;

  if (!line.empty()) {
    return invalidCommand;
  }

  return [](stringQueue & queue, std::ostream & out) {
      if (queue.empty()) {
        out << "<EMPTY>\n";
      } else {
        queue.pullFront([&](std::string element) { out << element << "\n"; });
      }
  };
}

Recognizer::commandExecution Recognizer::executeAccelerate(std::string & args)
{
  skipSpaces(args);
  std::string line = args;

  if (!line.empty()) {
    return invalidCommand;
  }

  return [](stringQueue & queue, std::ostream & out) {
      if (queue.empty()) {
        out << "<EMPTY>\n";
      } else {
        queue.accelerate();
      }
  };
}
