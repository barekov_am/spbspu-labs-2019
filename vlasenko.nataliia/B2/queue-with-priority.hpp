#ifndef QUEUEWITHPRIORITY_HPP
#define QUEUEWITHPRIORITY_HPP

#include <list>

template < typename T >
class QueueWithPriority {
public:
  enum class priority_t {
    LOW,
    NORMAL,
    HIGH
  };

  bool putElement(const T & element, priority_t priority);

  template < typename HandlerType >
  bool pullFront(HandlerType handler);

  void accelerate();
  bool empty() const;
  void clear();

private:
  std::list< T > high_;
  std::list< T > normal_;
  std::list< T > low_;
};

#endif
