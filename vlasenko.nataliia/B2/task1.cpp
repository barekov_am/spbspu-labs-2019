#include <sstream>
#include <iostream>

#include "recognizer.hpp"

void task1()
{
  QueueWithPriority< std::string > queue;
  std::string line;

  while (getline(std::cin, line)) {
    if (std::cin.fail() && !std::cin.eof()) {
      throw std::ios_base::failure("Fail reading data");
    }

    Recognizer::parseCommand(line)(queue, std::cout);
  }
}
