#ifndef Recognizer_HPP
#define Recognizer_HPP

#include <functional>

#include "queue-with-priority.cpp"

class Recognizer {
public:
  using stringQueue = QueueWithPriority< std::string >;
  using commandExecution = std::function< void(stringQueue &, std::ostream &) >;

  static commandExecution parseCommand(std::string & args);

private:
  static void skipSpaces(std::string &);
  static std::string extractWord(std::string &);

  static commandExecution executeAdd(std::string &);
  static commandExecution executeGet(std::string &);
  static commandExecution executeAccelerate(std::string &);
  static const commandExecution invalidCommand;
};

#endif
