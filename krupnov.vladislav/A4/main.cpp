#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "partition.hpp"
#include "matrix.hpp"


int main()
{
  krupnov::Rectangle rectangle({4, 5}, 6, 7);
  krupnov::Circle circle({4, 5}, 6);
  krupnov::Triangle triangle({4, 6}, {6, 7}, {1, 10});
  std::cout << rectangle.getArea() << std::endl;
  std::cout << circle.getArea() << std::endl;
  std::cout << triangle.getArea() << std::endl;
  krupnov::CompositeShape compositeShape(std::make_shared<krupnov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<krupnov::Circle>(circle));
  compositeShape.add(std::make_shared<krupnov::Triangle>(triangle));
  std::cout << compositeShape.getArea() << std::endl;
  std::unique_ptr<krupnov::Shape::shapePtr []> shape = std::make_unique<krupnov::Shape::shapePtr []>(5);
  krupnov::Rectangle rectangle1({2, 4.5}, 1, 2);
  krupnov::Rectangle rectangle2({3.5, 2}, 2, 5);
  krupnov::Circle circle1({6, 4}, 2);
  krupnov::Triangle triangle1({3, 2}, {2, 0}, {4, 0});
  krupnov::Triangle triangle2({7, 4}, {9, 3}, {9, 5});
  shape[0] = std::make_shared<krupnov::Rectangle>(rectangle1);
  shape[1] = std::make_shared<krupnov::Rectangle>(rectangle2);
  shape[2] = std::make_shared<krupnov::Circle>(circle1);
  shape[3] = std::make_shared<krupnov::Triangle>(triangle1);
  shape[4] = std::make_shared<krupnov::Triangle>(triangle2);
  krupnov::Matrix matrix = krupnov::division(shape, 5);
  matrix.showAll();
  return 0;
}
