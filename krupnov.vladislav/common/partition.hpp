#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"
#include <memory>

namespace krupnov
{
  bool checkIntersect(krupnov::rectangle_t shape1, krupnov::rectangle_t shape2);
  Matrix division(std::unique_ptr<krupnov::Shape::shapePtr []> &shapes, size_t size);
  Matrix division(CompositeShape &compositeShape);
}

#endif
