#include "composite-shape.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>

krupnov::CompositeShape::CompositeShape():
  countShapes_(0),
  shapes_(nullptr)
{}

krupnov::CompositeShape::CompositeShape(const krupnov::CompositeShape &rhs):
  countShapes_(rhs.countShapes_),
  shapes_(std::make_unique<shapePtr []>(rhs.countShapes_))
{
  for (size_t i = 0; i < countShapes_; i++)
  {
    shapes_[i] = rhs.shapes_[i];
  }
}

krupnov::CompositeShape::CompositeShape(krupnov::CompositeShape &&rhs):
  countShapes_(rhs.countShapes_),
  shapes_(std::move(rhs.shapes_))
{
  rhs.countShapes_ = 0;
}

krupnov::CompositeShape::CompositeShape(shapePtr shape):
  countShapes_(1),
  shapes_(std::make_unique<shapePtr []>(countShapes_))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Your shape is nullptr!");
  }
  shapes_[0] = shape;
}

krupnov::CompositeShape& krupnov::CompositeShape::operator =(const krupnov::CompositeShape &rhs)
{
  if (this != &rhs)
  {
    countShapes_ = rhs.countShapes_;
    std::unique_ptr<shapePtr []> temp(std::make_unique<shapePtr []>(rhs.countShapes_));
    for (size_t i = 0; i < countShapes_; i++)
    {
      temp[i] = rhs.shapes_[i];
    }
    shapes_.swap(temp);
  }
  return *this;
}

krupnov::CompositeShape& krupnov::CompositeShape::operator =(krupnov::CompositeShape &&rhs)
{
  if (this != &rhs)
  {
    countShapes_ = rhs.countShapes_;
    shapes_ = std::move(rhs.shapes_);
    rhs.countShapes_ = 0;
  }
  return *this;
}

krupnov::CompositeShape::shapePtr krupnov::CompositeShape::operator [](size_t index) const
{
  if (index >= countShapes_)
  {
    throw std::out_of_range("There is no figure!");
  }
  return shapes_[index];
}

void krupnov::CompositeShape::add(shapePtr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("You didn't add shape in Constructor or new object is nullptr!!!");
  }
  countShapes_++;
  std::unique_ptr<shapePtr []> temp(std::make_unique<shapePtr []>(countShapes_));
  for (size_t i = 0; i < countShapes_ - 1; i++)
  {
    temp[i] = shapes_[i];
  }
  temp[countShapes_ - 1] = shape;
  shapes_ = std::move(temp);
}

void krupnov::CompositeShape::remove(shapePtr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("You gave nullptr!");
  }
  size_t i = 0;
  while ((i < countShapes_) && (shape != shapes_[i]))
  {
    i++;
  }
  if (i == countShapes_)
  {
    throw std::invalid_argument("There is no such figure in the array");
  }
  remove(i);
}

void krupnov::CompositeShape::remove(size_t i)
{
  if (i > countShapes_ - 1)
  {
    throw std::out_of_range("The index is greater than number of figures!");
  }

  for (size_t j = i; j < countShapes_ - 1; j++)
  {
    shapes_[j] = shapes_[j + 1];
  }
  countShapes_--;
}

size_t krupnov::CompositeShape::getCount() const
{
  return countShapes_;
}

void krupnov::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < countShapes_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void krupnov::CompositeShape::move(const krupnov::point_t &center)
{
  point_t shift = {center.x - getFrameRect().pos.x, center.y - getFrameRect().pos.y};
  move(shift.x, shift.y);
}

double krupnov::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < countShapes_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

krupnov::point_t getBotAndTop(krupnov::rectangle_t frame, int factor)
{
  double x = frame.pos.x + (factor * (frame.width / 2));
  double y = frame.pos.y + (factor * (frame.height / 2));
  return {x, y};
}

krupnov::rectangle_t krupnov::CompositeShape::getFrameRect() const
{
  if (countShapes_ == 0)
  {
    throw std::logic_error("You didn't add shape in array");
  }

  point_t maxXAndY = getBotAndTop(shapes_[0]->getFrameRect(), 1);
  point_t minXAndY = getBotAndTop(shapes_[0]->getFrameRect(), (-1));
  for (size_t i = 1; i < countShapes_; i++)
  {
    maxXAndY.x = std::max(getBotAndTop(shapes_[i]->getFrameRect(), 1).x, maxXAndY.x);
    maxXAndY.y = std::max(getBotAndTop(shapes_[i]->getFrameRect(), 1).y, maxXAndY.y);
    minXAndY.x = std::min(getBotAndTop(shapes_[i]->getFrameRect(), (-1)).x, minXAndY.x);
    minXAndY.y = std::min(getBotAndTop(shapes_[i]->getFrameRect(), (-1)).y, minXAndY.y);
  }
  double height = maxXAndY.y - minXAndY.y;
  double width = maxXAndY.x - minXAndY.x;
  point_t center = {minXAndY.x + width / 2, minXAndY.y + height / 2};
  return {center, height, width};
}

void krupnov::CompositeShape::showPoint() const
{
  for (size_t i = 0; i < countShapes_; i++)
  {
    std::cout << "Center= {" << shapes_[i]->getFrameRect().pos.x << ", " << shapes_[i]->getFrameRect().pos.y << "}\n";
  }
}

void krupnov::CompositeShape::scale(double factor)
{
  point_t point = getFrameRect().pos;
  for (size_t i = 0; i < countShapes_; i++)
  {
    point_t shift = {shapes_[i]->getFrameRect().pos.x - point.x, shapes_[i]->getFrameRect().pos.y - point.y};
    shapes_[i]->move(shift.x * (factor - 1), shift.y * (factor - 1));
    shapes_[i]->scale(factor);
  }
}

void krupnov::CompositeShape::rotate(double angle)
{
  const double cos = std::cos((2 * M_PI * angle) / 360);
  const double sin = std::sin((2 * M_PI * angle) / 360);

  const point_t compCentre = getFrameRect().pos;

  for (size_t i = 0; i < countShapes_; i++)
  {
    const point_t center = shapes_[i]->getFrameRect().pos;
    const double dx = (center.x - compCentre.x) * cos - (center.y - compCentre.y) * sin;
    const double dy = (center.x - compCentre.x) * sin + (center.y - compCentre.y) * cos;
    shapes_[i]->move({compCentre.x + dx, compCentre.y + dy});
    shapes_[i]->rotate(angle);
  }
}
