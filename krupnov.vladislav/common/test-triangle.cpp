#include <boost/test/unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"

const double Inaccuracy = 0.01;

BOOST_AUTO_TEST_SUITE(testForTriangle)

  BOOST_AUTO_TEST_CASE(widthAndHeightAfterMovingToPoint)
  {
    krupnov::Triangle triangle({3, 4}, {5, 2}, {1, 5});
    krupnov::rectangle_t frame = triangle.getFrameRect();
    triangle.move({2, 2});
    BOOST_CHECK_CLOSE(frame.width, triangle.getFrameRect().width, Inaccuracy);
    BOOST_CHECK_CLOSE(frame.height, triangle.getFrameRect().height, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(widthAndHeightAfterMoving)
  {
    krupnov::Triangle triangle({3, 4}, {5, 2}, {1, 5});
    krupnov::rectangle_t frame = triangle.getFrameRect();
    triangle.move(2, 2);
    BOOST_CHECK_CLOSE(frame.width, triangle.getFrameRect().width, Inaccuracy);
    BOOST_CHECK_CLOSE(frame.height, triangle.getFrameRect().height, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(areaAfterMovingToPoint)
  {
    krupnov::Triangle triangle({3, 4}, {5, 2}, {1, 5});
    double area = triangle.getArea();
    triangle.move({2, 2});
    BOOST_CHECK_CLOSE(area, triangle.getArea(), Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(areaAfterMoving)
  {
    krupnov::Triangle triangle({3, 4}, {5, 2}, {1, 5});
    double area = triangle.getArea();
    triangle.move(3, 3);
    BOOST_CHECK_CLOSE(area, triangle.getArea(), Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(areaCheckAfterScalingOfTriangle)
  {
    krupnov::Triangle triangle({3, 4}, {6, 7}, {1, 5});
    const double areaBeforScaling = triangle.getArea();
    const double testScale = 4;
    triangle.scale(testScale);
    BOOST_CHECK_CLOSE(areaBeforScaling * testScale * testScale, triangle.getArea(), Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(invalidArgumentsInTriangle)
  {
    BOOST_CHECK_THROW(krupnov::Triangle triangle({4, 5}, {4, 6}, {4, 7}), std::invalid_argument);
    BOOST_CHECK_THROW(krupnov::Triangle triangle({5, 6}, {1, 6}, {4, 6}), std::invalid_argument);
    krupnov::Triangle triangle({4, 5}, {1, 4}, {6, 7});
    BOOST_CHECK_THROW(triangle.scale(-4), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(checkRotate)
  {
    krupnov::Triangle triangle({4, 5}, {6, 7}, {9, 8});
    double area = triangle.getArea();
    triangle.rotate(45);
    BOOST_CHECK_CLOSE(area, triangle.getArea(), Inaccuracy);
   }

BOOST_AUTO_TEST_SUITE_END()
