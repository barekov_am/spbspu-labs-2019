#include "matrix.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

krupnov::Matrix::Matrix():
  lines_(0),
  columns_(0)
{}

krupnov::Matrix::Matrix(const Matrix &rhs):
  lines_(rhs.lines_),
  columns_(rhs.columns_),
  shapes_(std::make_unique<Shape::shapePtr []>(rhs.lines_ * rhs.columns_))
{
  for (size_t i = 0; i < (lines_  * columns_); i++)
  {
    shapes_[i] = rhs.shapes_[i];
  }
}

krupnov::Matrix::Matrix(krupnov::Matrix &&rhs):
  lines_(rhs.lines_),
  columns_(rhs.columns_),
  shapes_(std::move(rhs.shapes_))
{
  rhs.lines_ = 0;
  rhs.columns_ = 0;
}

krupnov::Matrix &krupnov::Matrix::operator =(const Matrix &rhs)
{
  if (this != &rhs)
  {
    lines_ = rhs.lines_;
    columns_ = rhs.columns_;
    std::unique_ptr<Shape::shapePtr []> temp(std::make_unique<Shape::shapePtr []>(lines_ * columns_));
    for (size_t i = 0; i < (lines_ * columns_); i++)
    {
      temp[i] = rhs.shapes_[i];
    }

    shapes_.swap(temp);
  }

  return *this;
}

krupnov::Matrix &krupnov::Matrix::operator =(Matrix &&rhs)
{
  if (this != &rhs)
  {
    lines_ = rhs.lines_;
    columns_ = rhs.columns_;
    shapes_ = std::move(rhs.shapes_);
    rhs.lines_ = 0;
    rhs.columns_ = 0;
  }

  return *this;
}

std::unique_ptr<krupnov::Shape::shapePtr []> krupnov::Matrix::operator [](size_t index) const
{
  if (index >= lines_)
  {
    throw std::out_of_range("Index out of range");
  }

  std::unique_ptr<Shape::shapePtr []> temp(std::make_unique<Shape::shapePtr []>(columns_));
  for (size_t i = 0; i < getLineSize(index); i++)
  {
    temp[i] = shapes_[index * columns_ + i];
  }

  return temp;
}

bool krupnov::Matrix::operator==(const Matrix &shapesMatrix) const
{
  if ((lines_ != shapesMatrix.lines_) || (columns_ != shapesMatrix.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    if (shapes_[i] != shapesMatrix.shapes_[i])
    {
      return false;
    }
  }

  return true;
}

bool krupnov::Matrix::operator!=(const Matrix &shapesMatrix) const
{
  return !(*this == shapesMatrix);
}

void krupnov::Matrix::add(Shape::shapePtr shape, size_t line, size_t column)
{
  size_t newLine = (line == lines_) ? (lines_ + 1) : (lines_);
  size_t newColumn = (column == columns_) ? (columns_ + 1) : (columns_);

  std::unique_ptr<Shape::shapePtr []> temp(std::make_unique<Shape::shapePtr []>(newColumn * newLine));

  for (size_t i = 0; i < lines_; i++)
  {
    for (size_t j = 0; j < columns_; j++)
    {
      temp[i * newColumn + j] = shapes_[i * columns_ + j];
    }
  }
  temp[newColumn * line + column] = shape;
  shapes_ = std::move(temp);
  lines_ = newLine;
  columns_ = newColumn;
}

std::size_t krupnov::Matrix::getLines() const
{
  return lines_;
}

std::size_t krupnov::Matrix::getColumns() const
{
  return columns_;
}

std::size_t krupnov::Matrix::getLineSize(size_t line) const
{
  if (line >= lines_)
  {
    return 0;
  }

  for (size_t i = 0; i < columns_; i++)
  {
    if (shapes_[line * columns_ + i] == nullptr)
    {
      return i;
    }
  }

  return columns_;
}

void krupnov::Matrix::showAll() const
{
  for (size_t line = 0; line < lines_; line++)
  {
    std::cout << line + 1 << "-Layer  ";
    for (size_t column = 0; column < getLineSize(line); column++)
    {
      std::cout << shapes_[line * columns_ + column]->getArea() << " ";
    }
    std::cout << "\n";
  }
}
