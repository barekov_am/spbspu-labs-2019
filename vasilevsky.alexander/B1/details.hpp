#ifndef DETAILS_HPP
#define DETAILS_HPP

#include <iostream>
#include <vector>
#include <forward_list>
#include <string.h>
#include <stdexcept>

template<template <typename T> class getAccess, class T>
void sort(T& collection, char* direction)
{
  auto begin = getAccess<T>::begin(collection);
  auto end = getAccess<T>::end(collection);
  if (strcmp(direction, "ascending"))
  {
    for (auto iter1 = begin; iter1 != end; iter1++)
    {
      for (auto iter2 = begin; iter2 != end; iter2++)
      {
        if (getAccess<T>::getElement(collection, iter2) < getAccess<T>::getElement(collection, iter1))
        {
          std::swap(getAccess<T>::getElement(collection, iter1), getAccess<T>::getElement(collection, iter2));
        }
      }
    }
  }
  else
  if (strcmp(direction, "descending"))
  {
    for (auto iter1 = begin; iter1 != end; iter1++)
    {
      for (auto iter2 = begin; iter2 != end; iter2++)
      {
        if (getAccess<T>::getElement(collection, iter2) > getAccess<T>::getElement(collection, iter1))
        {
          std::swap(getAccess<T>::getElement(collection, iter1), getAccess<T>::getElement(collection, iter2));
        }
      }
    }
  }
}

template <typename T>
void checkDirection(T direction)
{
  if ((strcmp(direction, "ascending") != 0) && (strcmp(direction, "descending") != 0))
  {
    throw std::invalid_argument("Invalid argument");
  }
}

namespace vasilevsky
{
  template <typename T>
  struct opAccess
  {
    static typename T::reference getElement(T& collection, size_t index)
    {
      if (index > collection.size())
      {
        throw std::invalid_argument("Going beyond the borders of the vector");
      }
      return collection[index];
    }
    static size_t begin(T)
    {
      return 0;
    }
    static size_t end(T collection)
    {
      return collection.size();
    }
  };

  template <typename T>
  struct atAccess
  {
    static typename T::reference getElement(T& collection, size_t index)
    {
      return collection.at(index);
    }
    static size_t begin(T)
    {
      return 0;
    }
    static size_t end(T collection)
    {
      return collection.size();
    }
  };

  template <typename T>
  struct iterAccess
  {
    static typename T::reference getElement(T&, typename T::iterator iter)
    {
      return *iter;
    }
    static typename T::iterator begin(T& collection)
    {
      return collection.begin();
    }
    static typename T::iterator end(T& collection)
    {
      return collection.end();
    }
  };
}

#endif
