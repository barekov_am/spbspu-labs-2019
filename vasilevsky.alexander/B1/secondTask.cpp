#include <memory>
#include <fstream>
#include "tasks.hpp"

const size_t BUFFERSIZE = 1024;

void secondTask(char *fileName)
{
  std::ifstream file(fileName);

  if (!file)
  {
    throw std::ios_base::failure("File do not read");
  }

  size_t capacity = BUFFERSIZE;
  size_t i = 0;
  std::unique_ptr<char[], decltype(&free)> array(static_cast<char *>(malloc(capacity)), &free);

  while (file)
  {
    file.read(&array[i], BUFFERSIZE);
    i += file.gcount();
    if (file.gcount() == BUFFERSIZE)
    {
      capacity += BUFFERSIZE;
      std::unique_ptr<char[], decltype(&free)> temp(static_cast<char *>(realloc(array.get(), capacity)), &free);
      if (!temp)
      {
        throw std::ios_base::failure("Fail with reallocation");
      }
      array.release();
      std::swap(array, temp);
    }
  }

  file.close();

  if (file.is_open())
  {
    throw std::ios_base::failure("Fail with closing");
  }

  std::vector<char> vector(&array[0], &array[i]);

  for (size_t i = 0; i != vector.size(); i++)
  {
    std::cout << vector.at(i);
  }
}
