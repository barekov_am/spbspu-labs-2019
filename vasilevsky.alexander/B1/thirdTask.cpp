#include "tasks.hpp"

void thirdTask()
{
  std::vector<int> vector;
  int num = 0;
  bool check = 0;

  while (std::cin >> num)
  {
    if (num == 0)
    {
      check = 1;
      break;
    }   
    vector.push_back(num);
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::invalid_argument("Input is not integer");
  }

  if (vector.empty())
  {
    return;
  }

  if (!check)
  {
    throw std::invalid_argument("Input did not end with zero");
  }

  if (*--vector.end() == 1)
  {
    for (auto iter = vector.begin(); iter != vector.end();)
    {
      if (*iter % 2 == 0)
      {
        iter = vector.erase(iter);
      }
      else
      {
        iter++;
      }
    }
  }
  if (*--vector.end() == 2)
  {
    for (auto iter = vector.begin(); iter != vector.end();)
    {
      if (*iter % 3 == 0)
      {
        for (int i = 0; i < 3; i++)
        {
          iter = vector.insert(iter + 1, 1);
        }
      }
      else
      {
        iter++;
      }
    }
  }
  printCollection(vector);
}
