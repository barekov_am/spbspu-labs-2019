#include "tasks.hpp"

int main(int argc, char* argv[])
{
  try 
  {
    if ((argc < 2) || (argc > 4)) 
    {
      throw std::invalid_argument("Wrong number of parameters");
    }

    switch (std::atoi(argv[1])) 
    {
      case 1:
        if (argc != 3) 
        {
          throw std::invalid_argument("Wrong number of parameters");
        }

        firstTask(argv[2]);
        break;

      case 2:
        if (argc != 3) 
        {
          throw std::invalid_argument("Wrong number of parameters");
        }

        secondTask(argv[2]);
        break;

      case 3:
        if (argc != 2) 
        {
          throw std::invalid_argument("Wrong number of parameters");
        }

        thirdTask();
        break;

      case 4:
        if (argc != 4) 
        {
          throw std::invalid_argument("Wrong number of parameters");
        }
        
        fourthTask(argv[2], std::atoi(argv[3]));
        break;

      default:
        throw std::invalid_argument("Unknown task");
    }
  }
  catch(std::exception & ex)
  {
    std::cout << ex.what() << std::endl;
    return 1;
  }
  
    return 0;
}
