#ifndef PRINT_HPP
#define PRINT_HPP

#include <iostream>

template <typename T>
void printCollection(T collection)
{
  auto iter = collection.begin();
  while (iter != collection.end())
  {
    std::cout << *iter << " ";
    iter++;
  }
  std::cout << std::endl;
}

#endif
