#include <random>
#include <ctime>
#include "tasks.hpp"

void fillRandom(double* array, int size)
  {
    std::mt19937 gen(time(0));
    std::uniform_real_distribution<double> distribution(-1.0, 1.0);
    for (int i = 0; i < size; i++)
    {
      array[i] = distribution(gen);
    }
  }

void fourthTask(char* direction, int size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Invalid size");
  }

  checkDirection(direction);

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);
  printCollection(vector);

  sort<vasilevsky::iterAccess>(vector, direction);
  printCollection(vector);
}
