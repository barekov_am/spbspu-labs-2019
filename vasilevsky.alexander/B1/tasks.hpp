#ifndef TASKS_HPP
#define TASKS_HPP

#include "details.hpp"
#include "print.hpp"

void firstTask(char* direction);
void secondTask(char* fileName);
void thirdTask();
void fourthTask(char* direction, int size);

#endif
