#include "tasks.hpp"

void firstTask(char *direction)
{
  checkDirection(direction);

  std::vector<int> vectorOp;
  int data;

  while (std::cin >> data)
  {
    if (std::cin.fail())
    {
      throw std::invalid_argument("Invalid data input");
    }
    vectorOp.push_back(data);
  }

  if (!std::cin.eof())
  {
    throw std::invalid_argument("Format error");
  }

  std::forward_list<int> list(vectorOp.begin(), vectorOp.end());
  std::vector<int> vectorAt = vectorOp;

  sort<vasilevsky::opAccess>(vectorOp, direction);
  sort<vasilevsky::atAccess>(vectorAt, direction);
  sort<vasilevsky::iterAccess>(list, direction);

  printCollection(vectorOp);
  printCollection(vectorAt);
  printCollection(list);
}
