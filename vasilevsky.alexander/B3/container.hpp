#ifndef CONTAINER_HPP
#define CONTAINER_HPP

#include <iterator>

class Container
{
public:
  class iterator;
  Container() = default;
  iterator begin();
  iterator end();
};

class Container::iterator : public std::iterator<std::bidirectional_iterator_tag, size_t>
{
public:
  iterator(unsigned int value, unsigned int index);
  iterator(const iterator &other) = default;

  iterator &operator=(const iterator &other) = default;
  iterator &operator++();
  iterator operator++(int);
  iterator &operator--();
  iterator operator--(int);

  bool operator==(const iterator &other) const;
  bool operator!=(const iterator &other) const;

  const unsigned int &operator*() const;
  const unsigned int *operator->() const;

private:
  unsigned int value_;
  unsigned int index_;
};

#endif
