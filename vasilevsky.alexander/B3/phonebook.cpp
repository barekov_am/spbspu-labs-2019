#include "phonebook.hpp"

Phonebook::Phonebook()
{
  bookmarks_["current"] = entities_.end();
}

Phonebook::Phonebook(const Phonebook &other)
{
  *this = other;
}

Phonebook& Phonebook::operator=(const Phonebook &other)
{
  if(this != &other)
  {
    entities_ = other.entities_;
    bookmarks_.clear();

    for(auto it = other.bookmarks_.cbegin(); it != other.bookmarks_.cend(); it++)
    {
      std::list<EntityElem>::const_iterator bookmarked = it->second;
      bookmarks_[it->first] = std::next(entities_.begin(), std::distance(other.entities_.cbegin(), bookmarked));
    }
  }
  return *this;
}

void Phonebook::addEntity(const PhonebookEntity &entity)
{
  EntityElem newElement;
  newElement.entity = entity;

  if(entities_.empty())
  {
    newElement.bookmarks.insert("current");
    entities_.push_back(newElement);
    bookmarks_["current"] = entities_.begin();
  }
  else
  {
    entities_.push_back(newElement);
  }
}
void Phonebook::addBookmark(const std::string &currentBookmark, const std::string &newBookmark)
{
  auto it = getIterator(currentBookmark);
  bookmarks_[newBookmark] = it->second;

  if(it->second != entities_.end())
  {
    it->second->bookmarks.insert(newBookmark);
  }
}
void Phonebook::insertBefore(const std::string &bookmark, const PhonebookEntity &entity)
{
  if(entities_.empty())
  {
    addEntity(entity);
    return;
  }
  auto it = getIterator(bookmark);
  if(it->second != entities_.end())
  {
    entities_.insert(it->second, EntityElem{ entity, });
  }
  else
  {
    entities_.push_back(EntityElem{ entity, });
  }
}

void Phonebook::insertAfter(const std::string &bookmark, const PhonebookEntity &entity)
{
  if(entities_.empty())
  {
    addEntity(entity);
    return;
  }

  auto it = getIterator(bookmark);
  if(it->second != entities_.end())
  {
    entities_.insert(std::next(it->second), EntityElem{entity});
  }
  else
  {
    entities_.push_back(EntityElem{entity});
  }
}

void Phonebook::deleteEntity(const std::string &bookmark)
{
  auto it = getIterator(bookmark);
  if(it->second != entities_.end())
  {
    auto clistElem = it->second;
    for(auto j = clistElem->bookmarks.begin(); j != clistElem->bookmarks.end(); j++)
    {
      if((std::next(clistElem) == entities_.end()) && (entities_.size() > 1))
      {
        bookmarks_[*j] = std::prev(clistElem);
        std::prev(clistElem)->bookmarks.insert(*j);
      }
      else
      {
        bookmarks_[*j] = std::next(clistElem);
        if(std::next(clistElem) != entities_.end())
        {
          std::next(clistElem)->bookmarks.insert(*j);
        }
      }
    }
    entities_.erase(clistElem);
  }
}

void Phonebook::showEntityName(const std::string &bookmark, std::ostream &out)
{
  auto it = getIterator(bookmark);
  if(it->second == entities_.end())
  {
    out << "<EMPTY>" << std::endl;
  }
  else
  {
    out << it->second->entity.number << " " << it->second->entity.name << std::endl;
  }
}

void Phonebook::replaceEntity(const std::string &bookmark, const PhonebookEntity &entity)
{
  auto it = getIterator(bookmark);
  it->second->entity = entity;
}

void Phonebook::moveBookmark(const std::string &bookmark, int steps)
{
  auto it = getIterator(bookmark);

  if(it->second != entities_.end())
  {
    it->second->bookmarks.erase(bookmark);
  }

  std::advance(it->second, steps);

  if(it->second != entities_.end())
  {
    it->second->bookmarks.insert(bookmark);
  }

}
void Phonebook::moveBookmark(const std::string &bookmark, const std::string &location)
{
  auto it = getIterator(bookmark);
  if(location == "first")
  {
    if(it->second != entities_.end())
    {
      it->second->bookmarks.erase(bookmark);
    }

    if(!entities_.empty())
    {
      it->second = entities_.begin();
      it->second->bookmarks.insert(bookmark);
    }
    else
    {
      it->second = entities_.end();
    }
  }
  else if(location == "last")
  {
    if(it->second != entities_.end())
    {
      it->second->bookmarks.erase(bookmark);
    }
    if(!entities_.empty())
    {
      it->second = std::prev(entities_.end());
      it->second->bookmarks.insert(bookmark);
    }
    else
    {
      it->second = entities_.end();
    }
  }
  else
  {
    throw std::invalid_argument("<INVALID STEP>");
  }
}

void Phonebook::moveToNext(const std::string &bookmark)
{
  moveBookmark(bookmark, 1);
}

void Phonebook::moveToPrev(const std::string &bookmark)
{
  moveBookmark(bookmark, -1);
}

Phonebook::bookmarksIter_type Phonebook::getIterator(const std::string &bookmark)
{
  auto it = bookmarks_.find(bookmark);
  if(it == bookmarks_.end())
  {
    throw std::invalid_argument("<INVALID BOOKMARK>");
  }
  return it;
}
