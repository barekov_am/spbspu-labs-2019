#include "factorialtask.hpp"
#include <iostream>
#include <algorithm>
#include "container.hpp"

void calculateFactorials(std::ostream &out)
{
  Container cont;

  std::copy(cont.begin(), cont.end(), std::ostream_iterator<unsigned int>(out, " "));
  out << std::endl;

  std::reverse_copy(cont.begin(), cont.end(), std::ostream_iterator<unsigned int>(out, " "));
  out << std::endl;
}

