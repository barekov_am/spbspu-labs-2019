#ifndef FACTORIALTASK_HPP
#define FACTORIALTASK_HPP

#include <iostream>

void calculateFactorials(std::ostream &out);

#endif

