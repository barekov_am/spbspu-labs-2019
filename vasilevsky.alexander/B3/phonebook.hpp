#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <iostream>
#include <string>
#include <list>
#include <map>
#include <set>

struct PhonebookEntity
{
  std::string number;
  std::string name;
};

class Phonebook
{
public:

  Phonebook();
  Phonebook(const Phonebook &other);
  Phonebook(Phonebook &&other) = default;

  Phonebook &operator=(const Phonebook &other);
  Phonebook &operator=(Phonebook &&other) = default;

  void addEntity(const PhonebookEntity &entity);
  void addBookmark(const std::string &currentBookmark, const std::string &newBookmark);
  void insertBefore(const std::string &bookmark, const PhonebookEntity &entity);
  void insertAfter(const std::string &bookmark, const PhonebookEntity &entity);
  void deleteEntity(const std::string &bookmark);
  void replaceEntity(const std::string &bookmark, const PhonebookEntity &entity);
  void showEntityName(const std::string &bookmark, std::ostream &out);
  void moveBookmark(const std::string &bookmark, int steps);
  void moveBookmark(const std::string &bookmark, const std::string &location);
  void moveToNext(const std::string &bookmark);
  void moveToPrev(const std::string &bookmark);

private:
  struct EntityElem
  {
    PhonebookEntity entity;
    std::set<std::string> bookmarks;
  };

  typedef std::list<EntityElem> entity_type;
  typedef std::map<std::string, entity_type::iterator> bookmarks_type;
  typedef bookmarks_type::iterator bookmarksIter_type;

  entity_type entities_;
  bookmarks_type bookmarks_;

  bookmarksIter_type getIterator(const std::string &bookmark);
};

#endif

