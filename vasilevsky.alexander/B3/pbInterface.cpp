#include "pbInterface.hpp"

#include <iostream>
#include <sstream>
#include <string>

std::string formatName(const std::string &name)
{
  if(name.empty())
  {
    return name;
  }

  if((name.front() != '\"') || (name.back() != '\"'))
  {
    throw std::invalid_argument("<INVALID COMMAND>");
  }

  std::string formatted;
  for(size_t i = 1; i < name.size() - 1; i++)
  {
    if(name[i] != '\\')
    {
      formatted.push_back(name[i]);
    }
  }

  return formatted;
}

std::string readNumber(std::stringstream &input)
{
  std::string number;
  input >> number;

  for(auto it = number.begin(); it != number.end(); it++)
  {
    if(!isdigit(*it))
    {
      throw std::invalid_argument("<INVALID COMMAND>");
    }
  }
  return number;
}
std::string readName(std::stringstream &input)
{
  input.ignore();
  std::string name;
  std::getline(input, name);
  return formatName(name);
}

std::string readMarkName(std::stringstream &input)
{
  std::string markName;
  input >> markName;
  for(auto it = markName.begin(); it != markName.end(); it++)
  {
    if((!isalnum(*it)) && (*it != '-'))
    {
      throw std::invalid_argument("<INVALID BOOKMARK>");
    }
  }
  return markName;
}

void executeCommand(const std::string &line, std::ostream &output, Phonebook &pb)
{
  std::stringstream stream(line);

  std::string command;
  stream >> command;

  if(command == "add")
  {
    std::string number = readNumber(stream);
    std::string name = readName(stream);
    pb.addEntity(PhonebookEntity{number, name});
  }
  else if(command == "store")
  {
    std::string oldMarkName = readMarkName(stream);
    std::string newMarkName = readMarkName(stream);
    pb.addBookmark(oldMarkName, newMarkName);
  }
  else if(command == "insert")
  {
    std::string place;
    stream >> place;

    if((place != "before") && (place != "after"))
    {
      throw std::invalid_argument("<INVALID COMMAND>");
    }
    std::string markName = readMarkName(stream);
    std::string number = readNumber(stream);
    std::string name = readName(stream);

    if(place == "after")
    {
      pb.insertAfter(markName, PhonebookEntity{number, name});
    }
    else
    {
      pb.insertBefore(markName, PhonebookEntity{number, name});
    }
  }
  else if(command == "delete")
  {
    std::string markName = readMarkName(stream);
    pb.deleteEntity(markName);
  }
  else if(command == "show")
  {
    std::string markName = readMarkName(stream);
    pb.showEntityName(markName, output);
  }
  else if(command == "move")
  {
    std::string markName = readMarkName(stream);
    std::string steps;
    stream >> steps;
    try
    {
      int stepsInt = std::stoi(steps);
      pb.moveBookmark(markName, stepsInt);
    }
    catch(const std::invalid_argument&)
    {
      pb.moveBookmark(markName, steps);
    }
  }
  else
  {
    throw std::invalid_argument("<INVALID COMMAND>");
  }
}

void openInterface(std::istream &input, std::ostream &output)
{
  Phonebook pb;
  std::string line;
  while(std::getline(input, line))
  {
    try
    {
      executeCommand(line, output, pb);
    }
    catch(const std::invalid_argument &error)
    {
      output << error.what() << std::endl;
    }
  }
}
