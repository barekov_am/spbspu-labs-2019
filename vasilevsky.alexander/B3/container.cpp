#include "container.hpp"

const int MAX_FACTORIAL = 39916800;
const int MIN_FACTORIAL = 1;
const int MAX_VALUE = 11;
const int MIN_VALUE = 1;

Container::iterator Container::begin()
{
  return Container::iterator(MIN_FACTORIAL, MIN_VALUE);
}

Container::iterator Container::end()
{
  return Container::iterator(MAX_FACTORIAL, MAX_VALUE);
}

Container::iterator::iterator(unsigned int value, unsigned int index)
{
  value_ = value;
  index_ = index;
}

Container::iterator &Container::iterator::operator++()
{
  if(index_ < MAX_VALUE)
  {
    index_ ++;
    value_ *= index_;
  }
  return *this;
}

Container::iterator Container::iterator::operator++(int)
{
  iterator old = *this;
  if (index_ < MAX_VALUE)
  {
    index_++;
    value_ *= index_;
  }
  return old;
}

Container::iterator &Container::iterator::operator--()
{
  if(index_ > MIN_VALUE)
  {
    value_ /= index_;
    index_--;
  }
  return *this;
}

Container::iterator Container::iterator::operator--(int)
{
  iterator old = *this;
  if(index_ > MIN_VALUE)
  {
    value_ /= index_;
    index_--;
  }
  return old;
}

bool Container::iterator::operator==(const iterator &other) const
{
  return (index_ == other.index_);
}

bool Container::iterator::operator!=(const iterator &other) const
{
  return (index_ != other.index_);
}

const unsigned int &Container::iterator::operator*() const
{
  return value_;
}

const unsigned int *Container::iterator::operator->() const
{
  return &value_;
}
