#ifndef DETAILS_HPP
#define DETAILS_HPP

#include <string>
#include <iostream>
#include <algorithm>

#include "shape.hpp"

const int TRIANGLE_VERTEX = 3;
const int RECTANGLE_VERTEX = 4;
const int PENTAGON_VERTEX = 5;

int getSquaredLength(const Point_t &point1, const Point_t &point2);
bool isRectangle(const Shape &shape);
bool isSquare(const Shape &shape);
bool isLess(const Shape &shape1, const Shape &shape2);
void readShapes(std::vector<Shape> &vector, std::string &line);
Shape readPoints(std::string &line, size_t vertexes);

#endif
