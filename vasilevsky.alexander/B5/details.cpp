#include "details.hpp"

int getSquaredLength(const Point_t &point1, const Point_t &point2)
{
  int lengthX = point1.x - point2.x;
  int lengthY = point1.y - point2.y;
  return lengthX * lengthX + lengthY * lengthY;
}

bool isRectangle(const Shape &shape)
{
  int diag1 = getSquaredLength(shape[0], shape[2]);
  int diag2 = getSquaredLength(shape[1], shape[3]);
  int side1 = getSquaredLength(shape[0], shape[1]);
  int side2 = getSquaredLength(shape[2], shape[3]);

  if ((diag1 == diag2) && (side1 == side2))
  {
    return true;
  }

  return false;
}

bool isSquare(const Shape &shape)
{
  if (isRectangle(shape))
  {
    int side1 = getSquaredLength(shape[0], shape[1]);
    int side2 = getSquaredLength(shape[1], shape[2]);

    if (side1 == side2)
    {
      return true;
    }
  }

  return false;
}

bool isLess(const Shape &shape1, const Shape &shape2)
{
  if (shape1.size() < shape2.size())
  {
    return true;
  }

  if ((shape1.size() == RECTANGLE_VERTEX) && (shape2.size() == RECTANGLE_VERTEX))
  {
    if (isSquare(shape1))
    {
      if (isSquare(shape2))
      {
        return (shape1[0].x < shape2[0].x);
      }
      return true;
    }
  }

  return false;
}

void readShapes(std::vector<Shape> &vector, std::string &line)
{
  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error with reading data");
    }

    while (line.find_first_of(" \t") == 0)
    {
      line.erase(0, 1);
    }

    if (line.empty())
    {
      continue;
    }

    size_t location = line.find_first_of('(');

    if (location == std::string::npos)
    {
      throw std::invalid_argument("Invalid type of shape");
    }

    size_t vertexesNum = stoi(line.substr(0, location));
    line.erase(0, location);

    if (vertexesNum < TRIANGLE_VERTEX)
    {
      throw std::invalid_argument("Invalid number of vertexes");
    }

    vector.push_back(readPoints(line, vertexesNum));
  }
}

Shape readPoints(std::string &line, size_t vertexes)
{
  Shape shape;
  size_t openBracket;
  size_t closeBracket;
  size_t semicolon;

  for (size_t i = 0; i < vertexes; i++)
  {
    openBracket = line.find_first_of('(');
    closeBracket = line.find_first_of(')');
    semicolon = line.find_first_of(';');

    if ((openBracket == std::string::npos) || (closeBracket == std::string::npos) || (semicolon == std::string::npos))
    {
      throw std::invalid_argument("Invalid point");
    }

    Point_t point
      {
        std::stoi(line.substr(openBracket + 1, semicolon - openBracket - 1)),
        std::stoi(line.substr(semicolon + 1, closeBracket - semicolon - 1))
      };

    line.erase(0, closeBracket + 1);
    shape.push_back(point);
  }

  return shape;
}
