#include "details.hpp"

#include <sstream>

void secondTask()
{
  std::string line;
  std::vector<Shape> shapes;
  std::vector<Point_t> points;

  size_t vertexes = 0;
  size_t squares = 0;
  size_t triangles = 0;
  size_t rectangles = 0;

  readShapes(shapes, line);

  std::for_each(shapes.begin(), shapes.end(), [&](const Shape &shape)
  {
    vertexes += shape.size();

    if (shape.size() == TRIANGLE_VERTEX)
    {
      triangles++;
    }

    else if (shape.size() == RECTANGLE_VERTEX)
    {

      if (isRectangle(shape))
      {
        rectangles++;

        if (isSquare(shape))
        {
          squares++;
        }
      }
    }
  });

  shapes.erase(remove_if(shapes.begin(), shapes.end(), [&](const Shape &shape)
  { 
    return (shape.size() == PENTAGON_VERTEX);
  }), shapes.end());

  for (auto i = shapes.begin(); i != shapes.end(); i++)
  {
    points.push_back((*i).front());
  }

  std::sort(shapes.begin(), shapes.end(), isLess);

  std::cout << "Vertices: " << vertexes << std::endl;
  std::cout << "Triangles: " << triangles << std::endl;
  std::cout << "Squares: " << squares << std::endl;
  std::cout << "Rectangles: " << rectangles << std::endl;
  std::cout << "Points: ";

  for (auto &point: points)
  {
    std::cout << " (" << point.x << "; " << point.y << ')';
  }

  std::cout << std::endl << "Shapes:" << std::endl;

  for (const auto &shape: shapes)
  {
    std::cout << shape.size();

    for (const auto &point: shape)
    {
      std::cout << " (" << point.x << "; " << point.y << ')';
    }

    std::cout << std::endl;
  }
}
