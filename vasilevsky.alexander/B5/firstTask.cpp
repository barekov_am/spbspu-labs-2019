#include <iostream>
#include <set>
#include <sstream>

void firstTask()
{
  std::string line;
  std::set<std::string> setWords;

  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Some errors with reading data");
    }

    std::stringstream stream(line);
    std::string word;

    while (stream >> word)
    {
      if (setWords.find(word) == setWords.end())
      {
        setWords.insert(word);
      }
    }
  }

  if (!setWords.empty())
  {
    for (auto i = setWords.begin(); i != setWords.end(); ++i)
    {
      std::cout << *i << std::endl;
    }
  }
}

