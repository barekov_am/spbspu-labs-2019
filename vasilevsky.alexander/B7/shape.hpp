#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>

class Shape
{
public:
  Shape(const int x, const int y);
  virtual ~Shape() = default;
  bool isMoreLeft(const Shape &comparedShape) const;
  bool isUpper(const Shape &comparedShape) const;
  virtual void draw(std::ostream &stream) const = 0;

protected:
  int x_;
  int y_;
};




#endif

