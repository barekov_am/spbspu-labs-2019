#include <vector>
#include <algorithm>
#include <sstream>

#include "tasks.hpp"

#include "circle.hpp"
#include "triangle.hpp"
#include "square.hpp"

void removeFromString(std::string &input, const std::vector<char> &chars)
{
  for(auto c: chars)
  {
    input.erase(std::remove(input.begin(), input.end(), c), input.end());
  }
}

std::vector<Shape *> createShapesVector()
{
  std::vector<Shape *> shapes;
  std::string fileString, name;
  char c = ' ';
  int x, y;

  while (std::getline(std::cin, fileString))
  {
    removeFromString(fileString, { '\t', ' ' });

    if (fileString.empty())
    {
      continue;
    }

    std::istringstream in(fileString);
    std::getline(in, name, '(');
    name.erase(std::remove(name.begin(), name.end(), ' '), name.end());

    if (in)
    {
      in >> x;
      if (!(in))
      {
        throw std::invalid_argument("Format error - X");
      }
      in >> c;

      if (c != ';')
      {
        throw std::invalid_argument("Format error - ;");
      }

      in >> y;

      if (!in)
      {
        throw std::invalid_argument("Format error - Y");
      }

      in >> c;

      if (!(in && (c == ')')))
      {
        throw std::invalid_argument("Format error - )");
      }

      if (name == "TRIANGLE")
      {
        shapes.push_back(new Triangle(x, y));
      }

      else if (name == "SQUARE")
      {
        shapes.push_back(new Square(x, y));
      }

      else if (name == "CIRCLE")
      {
        shapes.push_back(new Circle(x, y));
      }

      else
      {
        throw std::invalid_argument("Format error - Figure name or (");
      }
    }
  }
  return shapes;
}

void printVector(const std::vector<Shape *> & shapesVector)
{
  std::for_each(shapesVector.begin(), shapesVector.end(),
      [](const Shape *shape) {shape->draw(std::cout); });
}

void runTaskTwo()
{
  auto shapes = createShapesVector();
  std::cout << "Original:\n";
  printVector(shapes);

  std::sort(shapes.begin(), shapes.end(),
      [](const Shape *firstShape, const Shape *secondShape)
        { return (firstShape->isMoreLeft(*secondShape)); });
  std::cout << "Left-Right:\n";
  printVector(shapes);

  std::sort(shapes.begin(), shapes.end(),
      [](const Shape *firstShape, const Shape *secondShape)
        { return !(firstShape->isMoreLeft(*secondShape)); });
  std::cout << "Right-Left:\n";
  printVector(shapes);

  std::sort(shapes.begin(), shapes.end(),
      [](const Shape *firstShape, const Shape *secondShape)
        { return (firstShape->isUpper(*secondShape)); });
  std::cout << "Top-Bottom:\n";
  printVector(shapes);

  std::sort(shapes.begin(), shapes.end(),
      [](const Shape *firstShape, const Shape *secondShape)
        { return !(firstShape->isUpper(*secondShape)); });
  std::cout << "Bottom-Top:\n";
  printVector(shapes);

  for (auto &shape : shapes)
  {
    delete shape;
  }
}
