#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

class Triangle: public Shape
{
public:
  Triangle(const int x, const int y);
  void draw(std::ostream & stream) const override;
};

#endif
