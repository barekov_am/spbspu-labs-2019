#include <iostream>

#include "tasks.hpp"

int main(int args, char *argv[])
{
  try
  {
    if (args != 2)
    {
      std::cerr << "Incorrect number of input parameters.\n";
      return 1;
    }

    switch (std::atoi(argv[1]))
    {
      case 1:
        runTaskOne();
        break;

      case 2:
        runTaskTwo();
        break;

      default:
        std::cerr << "Incorrect first argument - it must be 1 or 2\n";
        return 1;
    }
  }

  catch (const std::exception & err)
  {
    std::cout << err.what() << std::endl;
    return 1;
  }

  return 0;
}
