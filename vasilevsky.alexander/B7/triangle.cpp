#include "triangle.hpp"

Triangle::Triangle(const int x, const int y):
  Shape(x, y)
{
}

void Triangle::draw(std::ostream & stream) const
{
  stream << "TRIANGLE " << '(' << x_ << "; " << y_ << ")\n";
}
