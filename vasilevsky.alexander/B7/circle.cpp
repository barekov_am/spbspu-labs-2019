#include "circle.hpp"

Circle::Circle(const int x, const int y):
  Shape(x, y)
{
}

void Circle::draw(std::ostream &stream) const
{
  stream << "CIRCLE " << '(' << x_ << "; " << y_ << ")\n";
}

