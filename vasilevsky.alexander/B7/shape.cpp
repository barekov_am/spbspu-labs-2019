#include "shape.hpp"

Shape::Shape(const int x, const int y):
  x_(x),
  y_(y)
{
}

bool Shape::isMoreLeft(const Shape &comparedShape) const
{
  return (x_ < comparedShape.x_);
}

bool Shape::isUpper(const Shape &comparedShape) const
{
  return (y_ > comparedShape.y_);
}
