#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

class Circle: public Shape
{
public:
  Circle(const int x, const int y);
  void draw(std::ostream &stream) const override;
};

#endif
