#include <iostream>
#include "tasks.hpp"
#include "queue-with-priority-interface.hpp"

const int MIN_VALUE = 1;
const int MAX_VALUE = 20;
const size_t MAX_LENGTH = 20;

void secondTask()
{
  std::list<int> list;

  int following_number;
  while(std::cin >> following_number)
  {
    if((following_number < MIN_VALUE) || (following_number > MAX_VALUE))
    {
      throw std::out_of_range("Values can be only in range from 1 to 20");
    }
    list.push_back(following_number);
  }

  if(std::cin.fail() && !std::cin.eof())
  {
    throw std::invalid_argument("Input data is incorrect");
  }

  if(list.size() > MAX_LENGTH)
  {
    throw std::invalid_argument("Too many elements in list");
  }

  auto i = list.begin();
  auto j = std::prev(list.end());

  if(list.size() == MIN_VALUE)
  {
    std::cout << *i << std::endl;
  }
  else
  {
    while(i != j)
    {
      std::cout << *i << " " << *j << " ";
      i++;
      if(i == j)
      {
        break;
      }
      j--;
    }

    if(list.size() % 2 == 1)
    {
      std::cout << *i;
    }

    std::cout << std::endl;
  }
}
