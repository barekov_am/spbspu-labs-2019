#ifndef QUEUEWITHPRIORITY_HPP
#define QUEUEWITHPRIORITY_HPP

#include <list>
#include <stdexcept>

typedef enum
{
  LOW,
  NORMAL,
  HIGH
} ElementPriority;

namespace detail
{
  template <typename T>
  class QueueWithPriority
  {
  public:
    QueueWithPriority();
    ~QueueWithPriority() = default;
    void put(const T &element, ElementPriority priority);
    T get();
    void accelerate();
    bool empty() const;

  private:
    std::list<T> queue_;
    typename std::list<T>::iterator high_area, normal_area;
  };
}

#endif
