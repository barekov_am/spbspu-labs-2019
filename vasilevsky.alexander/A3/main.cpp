#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  //demonstration for Rectangle
  vasilevsky::Rectangle rectangle(4.0, 2.0, {4.0, 2.0});
  rectangle.printInfo();

  rectangle.move(5.3, 8.0);
  std::cout << "After moving dx = 5.3, dy = 8.0: \n";
  rectangle.printInfo();

  rectangle.move({7.0, 9.0});
  std::cout << "After moving to a point with coordinates {7.0, 9.0}: \n";
  rectangle.printInfo();

  rectangle.scale(2.0);
  std::cout << "After scaling with the coefficient 2.0: \n";
  rectangle.printInfo();

  //demonstration for Circle
  vasilevsky::Circle circle(2.0, {9.0, 5.0});
  circle.printInfo();

  circle.move(2.0, 6.5);
  std::cout << "After moving dx = 2.0, dy = 6.5: \n";
  circle.printInfo();

  circle.move({10.0, 5.0});
  std::cout << "After moving to a point with coordinates {10.0, 5.0}: \n";
  circle.printInfo();

  circle.scale(1.2);
  std::cout << "After scaling with the coefficient 1.2: \n";
  circle.printInfo();

  //demonstration for Composite Shape
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(rectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(circle);

  vasilevsky::CompositeShape compositeShape(rectanglePtr);
  compositeShape.printInfo();

  compositeShape.add(circlePtr);
  std::cout << "After adding new figure: \n";
  compositeShape.printInfo();

  compositeShape.move(3.7, 1.0);
  std::cout << "After moving dx = 3.7, dy = 1.0: \n";
  compositeShape.printInfo();

  compositeShape.move({10.0, 2.3});
  std::cout << "After moving to a point with coordinates {10.0, 2.3}: \n";
  compositeShape.printInfo();

  compositeShape.scale(2.0);
  std::cout << "After scaling with the coefficient 2.0: \n";
  compositeShape.printInfo();

  std::cout << "After deleting the first figure: \n";
  compositeShape.remove(0);
  compositeShape.printInfo();

  return 0;
}
