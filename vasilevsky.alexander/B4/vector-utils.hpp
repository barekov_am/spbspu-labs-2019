#ifndef VECTOR_UTILS_HPP
#define VECTOR_UTILS_HPP

#include <iostream>
#include <vector>

#include "data-struct.hpp"

class VectorUtils
{
public:
  VectorUtils();
  void readFromStream(std::istream &);
  void sort();
  void printToStream(std::ostream &);

private:
  std::vector<DataStruct> vector_;
  static bool compare(const DataStruct &, const DataStruct &);
  size_t readKeyFromString(const std::string &, int &);
};

#endif
