#ifndef DATA_STRUCT_HPP
#define DATA_STRUCT_HPP

#include <string>

const int KEY_MIN_VALUE = -5;
const int KEY_MAX_VALUE = 5;

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

#endif
