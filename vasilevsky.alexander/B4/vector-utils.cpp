#include "vector-utils.hpp"

#include <algorithm>

VectorUtils::VectorUtils()
{
}

void VectorUtils::readFromStream(std::istream &istr)
{
  std::string inputString;
  DataStruct dataStruct;

  while (std::getline(istr, inputString))
  {
    inputString = inputString.substr(readKeyFromString(inputString, dataStruct.key1) + 1);
    dataStruct.str = inputString.substr(readKeyFromString(inputString, dataStruct.key2) + 1);

    while (isspace(dataStruct.str.front()))
    {
      dataStruct.str.erase(0, 1);
    }

    vector_.push_back(dataStruct);
  }
}

void VectorUtils::sort()
{
  std::sort(vector_.begin(), vector_.end(), compare);
}

void VectorUtils::printToStream(std::ostream & ostr)
{
  for (auto dataStruct: vector_)
  {
    ostr << dataStruct.key1 << "," << dataStruct.key2 << "," << dataStruct.str << std::endl;
  }
}

bool VectorUtils::compare(const DataStruct & firstDataStruct,
    const DataStruct & secondDataStruct)
{
  if (firstDataStruct.key1 < secondDataStruct.key1)
  {
    return true;
  }

  if (firstDataStruct.key1 == secondDataStruct.key1)
  {
    if (firstDataStruct.key2 == secondDataStruct.key2)
    {
      if (firstDataStruct.str.size() == secondDataStruct.str.size())
      {
        return (firstDataStruct.str < secondDataStruct.str);
      }

      return (firstDataStruct.str.size() < secondDataStruct.str.size());
    }

    return (firstDataStruct.key2 < secondDataStruct.key2);
  }

  return false;
}

size_t VectorUtils::readKeyFromString(const std::string &line, int &key)
{
  size_t delimeterPosition = line.find_first_of(',');

  if (delimeterPosition == std::string::npos)
  {
    throw std::invalid_argument("Numbers should be separated with comma");
  }

  size_t nonDigitCharacterPosition;
  key = std::stoi(line.substr(0, delimeterPosition), &nonDigitCharacterPosition);

  if (nonDigitCharacterPosition != delimeterPosition)
  {
    throw std::invalid_argument("Wrong input. Only numbers from -5 to 5 and comma are supported");
  }

  if ((key < KEY_MIN_VALUE) || (key > KEY_MAX_VALUE))
  {
    throw std::invalid_argument("Key must be in between -5 and 5");
  }

  return delimeterPosition;
}
