#include "vector-utils.hpp"

int main()
{
  VectorUtils vectorUtils;

  try
  {
    vectorUtils.readFromStream(std::cin);
    vectorUtils.sort();
    vectorUtils.printToStream(std::cout);
  }
  catch (const std::exception &error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }

  return 0;
}
