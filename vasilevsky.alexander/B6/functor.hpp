#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <iostream>

class Functor
{
public:
  Functor();
  void operator ()(int number);
  int getMax();
  int getMin();
  double getMean();
  size_t getNumPositive();
  size_t getNumNegative();
  long int getOddSum();
  long int getEvenSum();
  bool isEqual();
  void print();

private:
  int max_;
  int min_;
  double mean_;
  size_t positive_;
  size_t negative_;
  long int oddSum_;
  long int evenSum_;
  bool equal_;
  int firstNum_;
  size_t total_;
};

#endif
