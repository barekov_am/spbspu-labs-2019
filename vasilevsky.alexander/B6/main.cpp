#include <iterator>
#include <algorithm>

#include "functor.hpp"

int main()
{
  try
  {
    Functor functor;
    
    functor = std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), functor);

    if (!std::cin.eof())
    {
      throw std::ios_base::failure("Error with reading data");
    }

    functor.print();
  }
  catch(const std::exception &error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }

  return 0;
}
