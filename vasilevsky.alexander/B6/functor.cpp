#include "functor.hpp"

#include <limits>

Functor::Functor()
{
  max_ = std::numeric_limits<int>::min(),
  min_ = std::numeric_limits<int>::max(),
  mean_ = 0.0,
  positive_ = 0,
  negative_ = 0,
  evenSum_ = 0,
  oddSum_ = 0,
  firstNum_ = 0,
  total_ = 0,
  equal_ = false;
}

void Functor::operator() (int number)
{
  if (total_ == 0)
  {
    firstNum_ = number;
  }

  if (number > max_)
  {
    max_ = number;
  }

  if (number < min_)
  {
    min_ = number;
  }

  if (number > 0)
  {
    positive_++;
  }
  else if (number < 0)
  {
    negative_++;
  }

  if (number % 2 == 0)
  {
    evenSum_ += number;
  }
  else
  {
    oddSum_ += number;
  }

  equal_ = (firstNum_ == number);

  total_ ++;
}

int Functor::getMax()
{
  return max_;
}

int Functor::getMin()
{
  return min_;
}

double Functor::getMean()
{
  return mean_ = static_cast<double>(oddSum_ + evenSum_) / total_;
}

size_t Functor::getNumPositive()
{
  return positive_;
}

size_t Functor::getNumNegative()
{
  return negative_;
}

long int Functor::getOddSum()
{
  return oddSum_;
}

long int Functor::getEvenSum()
{
  return evenSum_;
}

bool Functor::isEqual()
{
  return equal_;
}

void Functor::print()
{
  if (total_ == 0)
  {
    std::cout << "No Data" << std::endl;
  }
  else
  {
    std::cout << "Max: " << getMax() << std::endl;
    std::cout << "Min: " << getMin() << std::endl;
    std::cout << "Mean: " << getMean() << std::endl;
    std::cout << "Positive: " << getNumPositive() << std::endl;
    std::cout << "Negative: " << getNumNegative() << std::endl;
    std::cout << "Odd Sum: " << getOddSum() << std::endl;
    std::cout << "Even Sum: " << getEvenSum() << std::endl;
    std::cout << "First/Last Equal: " << (isEqual() ? "yes" : "no") << std::endl;
  }
}
