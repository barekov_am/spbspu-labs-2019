#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

const double fault = 0.001;

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(constantParametersAfterMovingToDistance)
{
  vasilevsky::Rectangle testRectangle(2.1, 3.3, {5.0, 3.9});
  const double testArea = testRectangle.getArea();
  const vasilevsky::rectangle_t testFrameRect = testRectangle.getFrameRect();

  testRectangle.move(1.0, 8.3);

  BOOST_CHECK_CLOSE(testFrameRect.width, testRectangle.getFrameRect().width, fault);
  BOOST_CHECK_CLOSE(testFrameRect.height, testRectangle.getFrameRect().height, fault);
  BOOST_CHECK_CLOSE(testArea, testRectangle.getArea(), fault);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterMovingToPoint)
{
  vasilevsky::Rectangle testRectangle(2.1, 3.3, {5.0, 3.9});
  const double testArea = testRectangle.getArea();
  const vasilevsky::rectangle_t testFrameRect = testRectangle.getFrameRect();

  testRectangle.move({3.1, 5.0});

  BOOST_CHECK_CLOSE(testFrameRect.width, testRectangle.getFrameRect().width, fault);
  BOOST_CHECK_CLOSE(testFrameRect.height, testRectangle.getFrameRect().height, fault);
  BOOST_CHECK_CLOSE(testArea, testRectangle.getArea(), fault);
}

BOOST_AUTO_TEST_CASE(squareIncreaseAreaAfterScale)
{
  vasilevsky::Rectangle testRectangle(2.1, 3.3, {5.0, 3.9});
  const double testArea = testRectangle.getArea();
  const double coefficient = 2.1;

  testRectangle.scale(coefficient);

  BOOST_CHECK_CLOSE(testRectangle.getArea(), testArea * coefficient * coefficient, fault);
}

BOOST_AUTO_TEST_CASE(squareDecreaseAreaAfterScale)
{
  vasilevsky::Rectangle testRectangle(2.1, 3.3, {5.0, 3.9});
  const double testArea = testRectangle.getArea();
  const double coefficient = 0.6;

  testRectangle.scale(coefficient);

  BOOST_CHECK_CLOSE(testRectangle.getArea(), testArea * coefficient * coefficient, fault);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  vasilevsky::Rectangle testRectangle(4.0, 2.0, {5.0, 5.0});
  const double testArea = testRectangle.getArea();
  const vasilevsky::rectangle_t testFrameRect = testRectangle.getFrameRect();
  const double angle = 90;

  testRectangle.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testRectangle.getFrameRect().width * 2, fault);
  BOOST_CHECK_CLOSE(testFrameRect.height, testRectangle.getFrameRect().height / 2, fault);
  BOOST_CHECK_CLOSE(testArea, testRectangle.getArea(), fault);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testRectangle.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testRectangle.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  BOOST_CHECK_THROW(vasilevsky::Rectangle(-2.1, 3.3, {5.0, 3.9}), std::invalid_argument);
  BOOST_CHECK_THROW(vasilevsky::Rectangle(2.1, -3.3, {5.0, 3.9}), std::invalid_argument);
  BOOST_CHECK_THROW(vasilevsky::Rectangle(0.0, 3.3, {5.0, 3.9}), std::invalid_argument);
  BOOST_CHECK_THROW(vasilevsky::Rectangle(2.1, 0.0, {5.0, 3.9}), std::invalid_argument);

  vasilevsky::Rectangle testRectangle(2.1, 3.3, {5.0, 3.9});

  BOOST_CHECK_THROW(testRectangle.scale(-3.0), std::invalid_argument);
  BOOST_CHECK_THROW(testRectangle.scale(0.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
