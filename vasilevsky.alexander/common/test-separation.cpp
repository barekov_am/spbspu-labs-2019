#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separation.hpp"

BOOST_AUTO_TEST_SUITE(separationTest)

BOOST_AUTO_TEST_CASE(checkCorrectCrossing)
{
  vasilevsky::Rectangle testRectangle(4.0, 2.0, {5.0, 5.0});
  vasilevsky::Rectangle crossingTestRectangle(6.0, 2.0, {8.0, 5.0});
  vasilevsky::Circle testCircle(1.0, {11.0, 5.0});

  const bool falseResult = vasilevsky::cross(testRectangle.getFrameRect(), testCircle.getFrameRect());
  const bool trueResult1 = vasilevsky::cross(testRectangle.getFrameRect(), crossingTestRectangle.getFrameRect());
  const bool trueResult2 = vasilevsky::cross(testCircle.getFrameRect(), crossingTestRectangle.getFrameRect());

  BOOST_CHECK_EQUAL(falseResult, false);
  BOOST_CHECK_EQUAL(trueResult1, true);
  BOOST_CHECK_EQUAL(trueResult2, true);

}

BOOST_AUTO_TEST_CASE(checkCorrectSeparation)
{
  vasilevsky::Rectangle testRectangle(4.0, 2.0, {5.0, 5.0});
  vasilevsky::Rectangle crossingTestRectangle(6.0, 2.0, {8.0, 5.0});
  vasilevsky::Circle testCircle(1.0, {11.0, 5.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr rectangleCrossingPtr = std::make_shared<vasilevsky::Rectangle>(crossingTestRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);
  testCompositeShape.add(rectangleCrossingPtr);
  testCompositeShape.add(circlePtr);

  vasilevsky::Matrix testMatrix = vasilevsky::part(testCompositeShape);

  BOOST_CHECK(testMatrix[0][0] == rectanglePtr);
  BOOST_CHECK(testMatrix[0][1] == circlePtr);
  BOOST_CHECK(testMatrix[1][0] == rectangleCrossingPtr);
}

BOOST_AUTO_TEST_SUITE_END()
