#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

const double fault = 0.001;

BOOST_AUTO_TEST_SUITE(circleTest)

BOOST_AUTO_TEST_CASE(constantParametersAfterMovingToDistance)
{
  vasilevsky::Circle testCircle(2.5, {5.9, 3.0});
  const double testArea = testCircle.getArea();
  const vasilevsky::rectangle_t testFrameRect = testCircle.getFrameRect();

  testCircle.move(1.0, 8.3);

  BOOST_CHECK_CLOSE(testFrameRect.width, testCircle.getFrameRect().width, fault);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCircle.getFrameRect().height, fault);
  BOOST_CHECK_CLOSE(testArea, testCircle.getArea(), fault);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterMovingToPoint)
{
  vasilevsky::Circle testCircle(2.5, {5.9, 3.0});
  const double testArea = testCircle.getArea();
  const vasilevsky::rectangle_t testFrameRect = testCircle.getFrameRect();

  testCircle.move({3.1, 5.0});

  BOOST_CHECK_CLOSE(testFrameRect.width, testCircle.getFrameRect().width, fault);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCircle.getFrameRect().height, fault);
  BOOST_CHECK_CLOSE(testArea, testCircle.getArea(), fault);
}

BOOST_AUTO_TEST_CASE(squareIncreaseAreaAfterScale)
{
  vasilevsky::Circle testCircle(2.5, {5.9, 3.0});
  const double testArea = testCircle.getArea();
  const double coefficient = 2.1;

  testCircle.scale(coefficient);

  BOOST_CHECK_CLOSE(testCircle.getArea(), testArea * coefficient * coefficient, fault);
}

BOOST_AUTO_TEST_CASE(squareDecreaseAreaAfterScale)
{
  vasilevsky::Circle testCircle(2.5, {5.9, 3.0});
  const double testArea = testCircle.getArea();
  const double coefficient = 0.6;

  testCircle.scale(coefficient);

  BOOST_CHECK_CLOSE(testCircle.getArea(), testArea * coefficient * coefficient, fault);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  vasilevsky::Circle testCircle(2.5, {5.9, 3.0});
  const double testArea = testCircle.getArea();
  const vasilevsky::rectangle_t testFrameRect = testCircle.getFrameRect();
  const double angle = 90;

  testCircle.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testCircle.getFrameRect().width, fault);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCircle.getFrameRect().height, fault);
  BOOST_CHECK_CLOSE(testArea, testCircle.getArea(), fault);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testCircle.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testCircle.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  BOOST_CHECK_THROW(vasilevsky::Circle(-2.5, {5.0, 3.9}), std::invalid_argument);
  BOOST_CHECK_THROW(vasilevsky::Circle(0.0, {5.0, 3.9}), std::invalid_argument);

  vasilevsky::Circle testCircle(2.5, {5.0, 3.9});

  BOOST_CHECK_THROW(testCircle.scale(-3.0), std::invalid_argument);
  BOOST_CHECK_THROW(testCircle.scale(0.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
