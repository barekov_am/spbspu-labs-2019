#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace vasilevsky
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&);
    CompositeShape(const shapePtr &);
    ~CompositeShape() = default;
    CompositeShape &operator =(const CompositeShape &);
    CompositeShape &operator =(CompositeShape &&);
    shapePtr operator [](size_t) const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(const double, const double) override;
    void printInfo() const override;
    void scale(const double) override;
    void rotate(const double) override;
    size_t getSize() const;
    void add(const shapePtr &);
    void remove(size_t);
    dynamicArray getList() const;

  private:
    size_t size_;
    dynamicArray shapeArray_;
    double angle_;
  };
}

#endif
