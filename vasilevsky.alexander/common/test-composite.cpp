#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

const double fault = 0.001;

BOOST_AUTO_TEST_SUITE(compositeShapeTest)

BOOST_AUTO_TEST_CASE(constantParametersAfterMovingToDistance)
{
  vasilevsky::Rectangle testRectangle(2.1, 3.3, {5.0, 3.9});
  vasilevsky::Circle testCircle(2.5, {5.9, 3.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const double testArea = testCompositeShape.getArea();
  const vasilevsky::rectangle_t testFrameRect = testCompositeShape.getFrameRect();

  testCompositeShape.move(1.0, 8.3);

  BOOST_CHECK_CLOSE(testFrameRect.width, testCompositeShape.getFrameRect().width, fault);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCompositeShape.getFrameRect().height, fault);
  BOOST_CHECK_CLOSE(testArea, testCompositeShape.getArea(), fault);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterMovingToPoint)
{
  vasilevsky::Rectangle testRectangle(2.1, 3.3, {5.0, 3.9});
  vasilevsky::Circle testCircle(2.5, {5.9, 3.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const double testArea = testCompositeShape.getArea();
  const vasilevsky::rectangle_t testFrameRect = testCompositeShape.getFrameRect();

  testCompositeShape.move({3.1, 5.0});

  BOOST_CHECK_CLOSE(testFrameRect.width, testCompositeShape.getFrameRect().width, fault);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCompositeShape.getFrameRect().height, fault);
  BOOST_CHECK_CLOSE(testArea, testCompositeShape.getArea(), fault);
}

BOOST_AUTO_TEST_CASE(squareIncreaseAreaAfterScale)
{
  vasilevsky::Rectangle testRectangle(2.1, 3.3, {5.0, 3.9});
  vasilevsky::Circle testCircle(2.5, {5.9, 3.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const double testArea = testCompositeShape.getArea();
  const double coefficient = 2.1;

  testCompositeShape.scale(coefficient);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testArea * coefficient * coefficient, fault);
}

BOOST_AUTO_TEST_CASE(squareDecreaseAreaAfterScale)
{
  vasilevsky::Rectangle testRectangle(2.1, 3.3, {5.0, 3.9});
  vasilevsky::Circle testCircle(2.5, {5.9, 3.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const double testArea = testCompositeShape.getArea();
  const double coefficient = 0.6;

  testCompositeShape.scale(coefficient);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testArea * coefficient * coefficient, fault);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  vasilevsky::Rectangle testRectangle(4.0, 2.0, {5.0, 5.0});
  vasilevsky::Circle testCircle(1.0, {5.0, 5.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const double testArea = testCompositeShape.getArea();
  const vasilevsky::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double angle = 90;

  testCompositeShape.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testCompositeShape.getFrameRect().width * 2, fault);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCompositeShape.getFrameRect().height / 2, fault);
  BOOST_CHECK_CLOSE(testArea, testCompositeShape.getArea(), fault);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testCompositeShape.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testCompositeShape.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_CASE(parametersAfterAddingAndDeletion)
{
  vasilevsky::Rectangle testRectangle(2.1, 3.3, {5.0, 3.9});
  vasilevsky::Circle testCircle(2.5, {5.9, 3.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);

  const double testCompositeArea = testCompositeShape.getArea();
  const double testCircleArea = testCircle.getArea();

  testCompositeShape.add(circlePtr);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea + testCircleArea, fault);

  testCompositeShape.remove(1);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea, fault);
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  vasilevsky::Rectangle testRectangle(2.1, 3.3, {5.0, 3.9});
  vasilevsky::Circle testCircle(2.5, {5.9, 3.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  BOOST_CHECK_THROW(testCompositeShape.add(nullptr), std::invalid_argument);

  BOOST_CHECK_THROW(testCompositeShape.scale(-3.0), std::invalid_argument);
  BOOST_CHECK_THROW(testCompositeShape.scale(0.0), std::invalid_argument);

  BOOST_CHECK_THROW(testCompositeShape.remove(4), std::out_of_range);
  BOOST_CHECK_THROW(testCompositeShape.remove(-2), std::out_of_range);

  BOOST_CHECK_THROW(testCompositeShape[4], std::out_of_range);
  BOOST_CHECK_THROW(testCompositeShape[-2], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkCopyConstructor)
{
  vasilevsky::Rectangle testRectangle(2.1, 3.3, {5.0, 3.9});
  vasilevsky::Circle testCircle(2.5, {5.9, 3.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const vasilevsky::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getSize();

  vasilevsky::CompositeShape copyCompositeShape(testCompositeShape);

  const vasilevsky::rectangle_t copyFrameRect = copyCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, copyCompositeShape.getArea(), fault);
  BOOST_CHECK_CLOSE(testFrameRect.width, copyFrameRect.width, fault);
  BOOST_CHECK_CLOSE(testFrameRect.height, copyFrameRect.height, fault);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, copyFrameRect.pos.x, fault);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, copyFrameRect.pos.y, fault);
  BOOST_CHECK_EQUAL(testCompositeSize, copyCompositeShape.getSize());
}

BOOST_AUTO_TEST_CASE(checkMoveConstructor)
{
  vasilevsky::Rectangle testRectangle(2.1, 3.3, {5.0, 3.9});
  vasilevsky::Circle testCircle(2.5, {5.9, 3.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const vasilevsky::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getSize();

  vasilevsky::CompositeShape moveCompositeShape(std::move(testCompositeShape));

  const vasilevsky::rectangle_t moveFrameRect = moveCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, moveCompositeShape.getArea(), fault);
  BOOST_CHECK_CLOSE(testFrameRect.width, moveFrameRect.width, fault);
  BOOST_CHECK_CLOSE(testFrameRect.height, moveFrameRect.height, fault);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, moveFrameRect.pos.x, fault);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, moveFrameRect.pos.y, fault);
  BOOST_CHECK_EQUAL(testCompositeSize, moveCompositeShape.getSize());
}

BOOST_AUTO_TEST_CASE(checkCopyOperator)
{
  vasilevsky::Rectangle testRectangle(2.1, 3.3, {5.0, 3.9});
  vasilevsky::Circle testCircle(2.5, {5.9, 3.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const vasilevsky::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getSize();

  vasilevsky::Circle testCircleNew(3.1, {3.1, 4.7});
  vasilevsky::shapePtr circleNewPtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape copyCompositeShape(circleNewPtr);

  copyCompositeShape = testCompositeShape;

  const vasilevsky::rectangle_t copyFrameRect = copyCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, copyCompositeShape.getArea(), fault);
  BOOST_CHECK_CLOSE(testFrameRect.width, copyFrameRect.width, fault);
  BOOST_CHECK_CLOSE(testFrameRect.height, copyFrameRect.height, fault);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, copyFrameRect.pos.x, fault);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, copyFrameRect.pos.y, fault);
  BOOST_CHECK_EQUAL(testCompositeSize, copyCompositeShape.getSize());
}

BOOST_AUTO_TEST_CASE(checkMoveOperator)
{
  vasilevsky::Rectangle testRectangle(2.1, 3.3, {5.0, 3.9});
  vasilevsky::Circle testCircle(2.5, {5.9, 3.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const vasilevsky::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getSize();

  vasilevsky::CompositeShape moveCompositeShape;

  moveCompositeShape = std::move(testCompositeShape);

  const vasilevsky::rectangle_t moveFrameRect = moveCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, moveCompositeShape.getArea(), fault);
  BOOST_CHECK_CLOSE(testFrameRect.width, moveFrameRect.width, fault);
  BOOST_CHECK_CLOSE(testFrameRect.height, moveFrameRect.height, fault);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, moveFrameRect.pos.x, fault);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, moveFrameRect.pos.y, fault);
  BOOST_CHECK_EQUAL(testCompositeSize, moveCompositeShape.getSize());
}

BOOST_AUTO_TEST_SUITE_END()
