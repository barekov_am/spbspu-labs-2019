#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

namespace vasilevsky
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &);
    Matrix(Matrix &&);
    ~Matrix() = default;
    Matrix &operator =(const Matrix &);
    Matrix &operator =(Matrix &&);
    dynamicArray operator [](size_t) const;
    bool operator ==(const Matrix &) const;
    bool operator !=(const Matrix &) const;
    size_t getLines() const;
    size_t getColumns() const;
    void add(const shapePtr &, size_t, size_t);
    void printInfo() const;

  private:
    size_t lines_;
    size_t columns_;
    dynamicArray list_;
  };
}

#endif
