#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separation.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithCrossing)
{
  const size_t layersValue = 2;
  const size_t columnsValue = 1;

  vasilevsky::Rectangle testRectangle(4.0, 2.0, {5.0, 5.0});
  vasilevsky::Circle testCircle(2.0, {5.0, 5.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  vasilevsky::Matrix testMatrix = vasilevsky::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithoutCrossing)
{
  const size_t layersValue = 1;
  const size_t columnsValue = 2;

  vasilevsky::Rectangle testRectangle(4.0, 2.0, {5.0, 5.0});
  vasilevsky::Circle testCircle(2.0, {10.0, 5.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  vasilevsky::Matrix testMatrix = vasilevsky::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  vasilevsky::Rectangle testRectangle(4.0, 2.0, {5.0, 5.0});
  vasilevsky::Circle testCircle(2.0, {5.0, 5.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  vasilevsky::Matrix testMatrix = vasilevsky::part(testCompositeShape);

  BOOST_CHECK_THROW(testMatrix[5][0], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-5][0], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkCopyAndMove)
{
  vasilevsky::Rectangle testRectangle(4.0, 2.0, {5.0, 5.0});
  vasilevsky::Circle testCircle(2.0, {5.0, 5.0});
  vasilevsky::shapePtr rectanglePtr = std::make_shared<vasilevsky::Rectangle>(testRectangle);
  vasilevsky::shapePtr circlePtr = std::make_shared<vasilevsky::Circle>(testCircle);

  vasilevsky::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  vasilevsky::Matrix testMatrix = vasilevsky::part(testCompositeShape);
  vasilevsky::Matrix copyMatrix(testMatrix);
  vasilevsky::Matrix moveMatrix(std::move(testMatrix));

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyMatrix);
}

BOOST_AUTO_TEST_SUITE_END()
