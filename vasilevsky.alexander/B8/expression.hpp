#ifndef EXPRESSION_HPP
#define EXPRESSION_HPP

#include <string>

struct expression_t
{
  enum type_t
  {
    WORD,
    NUMBER,
    PUNCT,
    DASH,
    WHITESPACE
  };
  std::string value;
  type_t type;
};

#endif
