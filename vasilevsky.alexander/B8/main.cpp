#include <iostream>

#include "input.hpp"
#include "output.hpp"

const int MIN_WIDTH_OF_STRING = 25;
const int DEFAULT_WIDTH_OF_STRING = 40;

int main(int args, char *argv[])
{
  try
  { 
    int width_line = DEFAULT_WIDTH_OF_STRING;

    if ((args != 1) && (args != 3))
    {
      std::cerr << "Invalid input parameters";
      return 1;
    }

    if (args == 3)
    {
      width_line = std::stoi(argv[2]);

      if (std::string(argv[1]) != "--line-width")
      {
        std::cerr << "You should write --line-width";
        return 1;
      }

      if (width_line < MIN_WIDTH_OF_STRING)
      {
        std::cerr << "Invalid line width";
        return 1;
      }
    }
    
    InputParser parser(std::cin);
    OutputWriter writer(std::cout);
    parser.parsing();
    std::list<expression_t> text(parser.begin(), parser.end());

    if(!parser.textGood(text))
    {
      std::cerr << "Errors in input text";
      return 1;
    }

    writer.outPrinting(width_line, text);
  }

  catch(std::invalid_argument &error)
  {
    std::cerr << error.what();
    return 1;
  }

  return 0;
}
