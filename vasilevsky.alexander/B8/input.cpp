#include "input.hpp"

#include <locale>

const int MAX_LENGTH = 20;
const int LENGTH_OF_DASH = 3;

InputParser::InputParser(std::istream &inp_stream) :
  inp_stream_(inp_stream)
{
}

void InputParser::parsing()
{
  while(inp_stream_)
  {
    getExpression();
  }
}

void InputParser::getExpression()
{
  char c = inp_stream_.get();

  while(std::isspace(c, std::locale()))
  {
    c = inp_stream_.get();
  }

  if(isalpha(c, std::locale()))
  {
    inp_stream_.unget();
    getWord();
  }

  else if(c == '-')
  {

    if(inp_stream_.peek() == '-')
    {
      inp_stream_.unget();
      getDash();
    }

    else
    {
      inp_stream_.unget();
      getNumber();
    }
  }

  else if((c == '+') || (isdigit(c, std::locale())))
  {
    inp_stream_.unget();
    getNumber();
  }

  else if(ispunct(c, std::locale()))
  {
    expression_t expression{"", expression_t::PUNCT};
    expression.value.push_back(c);
    inp_text_.push_back(expression);
  }
}

void InputParser::getWord()
{
  expression_t expression{"", expression_t::WORD};

  do
  {
    char c = inp_stream_.get();
    expression.value.push_back(c);

    if(c == '-' && inp_stream_.peek() == '-')
    {
      expression.value.pop_back();
      inp_stream_.unget();
      break;
    }
  } while((std::isalpha<char>(inp_stream_.peek(), std::locale())) || (inp_stream_.peek() == '-'));

  inp_text_.push_back(expression);
}

void InputParser::getNumber()
{
  expression_t expression{"", expression_t::NUMBER};
  char decimal_point = std::use_facet<std::numpunct<char>>(std::locale()).decimal_point();
  bool decimal_point_read = false;

  do
  {
    char c = inp_stream_.get();

    if(c == decimal_point)
    {

      if(decimal_point_read)
      {
        inp_stream_.unget();
        break;
      }

      decimal_point_read = true;
    }

    expression.value.push_back(c);
  } while((std::isdigit<char>(inp_stream_.peek(), std::locale()) || (inp_stream_.peek() == decimal_point)));

  inp_text_.push_back(expression);
}

void InputParser::getDash()
{
  expression_t expression{"", expression_t::DASH};

  while(inp_stream_.peek() == '-')
  {
    char c = inp_stream_.get();
    expression.value.push_back(c);
  }

  inp_text_.push_back(expression);
}

bool InputParser::textGood(std::list<expression_t> &vector)
{
  if(!vector.empty() && (vector.front().type != expression_t::WORD) && (vector.front().type != expression_t::NUMBER))
  {
    return false;
  }

  for(auto iter = vector.begin(); iter != vector.end(); iter++)
  {
    switch((*iter).type)
    {
      case expression_t::WORD:
      {
      }

      case expression_t::NUMBER:
      {
        if((*iter).value.size() > MAX_LENGTH)
        {
          return false;
        }
        break;
      }

      case expression_t::DASH:
      {
        if((*iter).value.size() != LENGTH_OF_DASH)
        {
          return false;
        }

        if(iter != vector.begin())
        {
          const expression_t &prev = *std::prev(iter);
          if((prev.type == expression_t::DASH) || ((prev.type == expression_t::PUNCT) && (prev.value != ",")))
          {
            return false;
          }
        }
        break;
      }

      case expression_t::PUNCT:
      {
        if(iter != vector.begin())
        {
          const expression_t &prev = *std::prev(iter);
          if((prev.type == expression_t::DASH) || (prev.type == expression_t::PUNCT))
          {
            return false;
          }
        }
        break;
      }

      case expression_t::WHITESPACE:
      {
        break;
      }
    }
  }
  return true;
}

std::list<expression_t>::iterator InputParser::begin()
{
  return inp_text_.begin();
}

std::list<expression_t>::iterator InputParser::end()
{
  return inp_text_.end();
}
