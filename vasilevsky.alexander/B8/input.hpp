#ifndef INPUT_HPP
#define INPUT_HPP

#include <iostream>
#include <list>
#include <memory>

#include "expression.hpp"

class InputParser{

  public:
    InputParser(std::istream &inp_stream);
    void parsing();
    bool textGood(std::list<expression_t> &vector);
    std::list<expression_t>::iterator begin();
    std::list<expression_t>::iterator end();

  private:
    std::istream &inp_stream_;
    std::list<expression_t> inp_text_;
    void getExpression();
    void getWord();
    void getNumber();
    void getDash();
};

#endif
