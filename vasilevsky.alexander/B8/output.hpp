#ifndef OUTPUT_HPP
#define OUTPUT_HPP

#include <iostream>
#include <list>

#include "expression.hpp"

class OutputWriter
{
  public:
    OutputWriter(std::ostream &out_stream);
    void outPrinting(unsigned int width_line, std::list<expression_t> &vector);
    void print(std::list<expression_t> &line);
    int printRebuild(std::list<expression_t> &line);

  private:
    std::ostream &out_stream_;
};

#endif
