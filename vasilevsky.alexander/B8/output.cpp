#include "output.hpp"

#include <list>

OutputWriter::OutputWriter(std::ostream &out_stream):
  out_stream_(out_stream)
{
}

void OutputWriter::print(std::list<expression_t> &line)
{
  for(auto iter = line.begin(); iter != line.end(); ++iter)
  {
    out_stream_ << (*iter).value;
  }

  out_stream_ << "\n";
}

int OutputWriter::printRebuild(std::list<expression_t> &line)
{
  std::size_t width = 0;
  std::list<expression_t> new_line;

  while(!line.empty())
  {
    new_line.push_front(line.back());
    width += line.back().value.size();
    line.pop_back();

    if(new_line.front().type == expression_t::WORD || new_line.front().type == expression_t::NUMBER)
    {
      break;
    }
  }

  print(line);
  line = new_line;
  return width;
}

void OutputWriter::outPrinting(unsigned int width_line, std::list<expression_t> &vector)
{
  std::size_t current_width = 0;
  std::list<expression_t> line;

  for(auto iter = vector.begin(); iter != vector.end(); iter++)
  {
    switch((*iter).type)
    {
      case expression_t::PUNCT:
      {
        if(current_width + 1 > width_line)
        {
          current_width = printRebuild(line);
        }
        line.push_back(*iter);
        current_width += (*iter).value.size();
        break;
      }

      case expression_t::DASH:
      {
        if(current_width + 4 > width_line)
        {
          current_width = printRebuild(line);
        }

        line.push_back(expression_t{" ", expression_t::WHITESPACE});
        line.push_back(*iter);
        current_width += (*iter).value.size() + 1;
        break;
      }

      case expression_t::WORD:
      {
      }

      case expression_t::NUMBER:
      {
        if(current_width + (*iter).value.size() + 1 > width_line)
        {
          print(line);
          line.clear();
          current_width = 0;
        }

        else if(!line.empty())
        {
          line.push_back(expression_t{" ", expression_t::WHITESPACE});
          current_width++;
        }
        line.push_back(*iter);
        current_width += (*iter).value.size();
        break;
      }

      case expression_t::WHITESPACE:
      {
        break;
      }
    }
  }

  if(!line.empty())
  {
    print(line);
  }
}
