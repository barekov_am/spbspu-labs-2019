#include <iostream>
#include <cmath>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

void printInfo(const chizhov::Shape& shape)
{
  chizhov::rectangle_t rectTmp = shape.getFrameRect();
  std::cout << "Pos: ("
      << rectTmp.pos.x << ";" << rectTmp.pos.y << ")\nDim: "
      << rectTmp.width << "x" << rectTmp.height << '\n'
      << "Area: " << shape.getArea() << "\n\n";
}

void movingAndScaling()
{
  chizhov::Circle c1 = chizhov::Circle({1, 2}, 1);
  chizhov::Rectangle r1 = chizhov::Rectangle({2, 1}, 2);

  chizhov::shape_ptr c1_ptr = std::make_shared<chizhov::Circle>(c1);
  chizhov::shape_ptr r1_ptr = std::make_shared<chizhov::Rectangle>(r1);

  std::cout << " ##### Moving and scaling shapes #####\n";
  std::cout << " ----- Composite Shape -----\n";
  chizhov::CompositeShape cs1;
  cs1.add(c1_ptr);
  cs1.add(r1_ptr);
  printInfo(cs1);

  cs1.move(1, 1);
  printInfo(cs1);

  cs1.scale(2);
  printInfo(cs1);

  cs1.remove(c1_ptr);
  cs1.move({1.5, 1.5});
  printInfo(cs1);

  chizhov::Circle c2(4, 7, 2);
  c2.scale(1.5);

  chizhov::Rectangle r2({6, 2}, 3);
  r2.move(2, 5);

  chizhov::Rectangle r3(6, 2, 3, 3);
  r3.move(2, 5);

  // Вписываем круг в квадрат
  chizhov::rectangle_t rect_r2 = r2.getFrameRect();
  chizhov::Circle c3(rect_r2.pos, rect_r2.width / 2);

  // Выводим информацию о фигурах
  std::cout << " ----- Shape loop -----\n";
  chizhov::Shape* shapes[] = {&c1, &c2, &c3, &r1, &r2, &r3, &cs1,};
  for (chizhov::Shape * shape : shapes) {
    printInfo(*shape);
  }
}

void rotation()
{
  chizhov::Circle circle({std::sqrt(2), 0}, 1);
  chizhov::shape_ptr circlePtr = std::make_shared<chizhov::Circle>(circle);
  chizhov::Rectangle rectangle({-std::sqrt(2), 0}, 2);
  chizhov::shape_ptr rectanglePtr = std::make_shared<chizhov::Rectangle>(rectangle);

  chizhov::CompositeShape composite(circlePtr);
  composite.add(rectanglePtr);

  std::cout << " ##### Rotation of Composite #####\n";
  std::cout << " --- -45 degrees ---\n";
  composite.rotate(-45);
  printInfo(composite);
  printInfo(*circlePtr);
  printInfo(*rectanglePtr);
  composite.rotate(45);

  for (int angle = 0; angle <= 405; angle += 45) {
    std::cout << " --- " << angle << " degrees ---\n";
    printInfo(composite);
    printInfo(*circlePtr);
    printInfo(*rectanglePtr);
    composite.rotate(45);
  }
}

void matrix()
{
  chizhov::Circle circle({0, 0}, 1);
  chizhov::Rectangle rectangle({1, 1}, 2);
  chizhov::Rectangle rectangle2({2, 2}, 2);
  chizhov::shape_ptr shapes[] = {std::make_shared<chizhov::Circle>(circle),
      std::make_shared<chizhov::Rectangle>(rectangle),
      std::make_shared<chizhov::Rectangle>(rectangle2),};

  std::cout << " ##### Matrix of Shapes #####\n";
  chizhov::Matrix matrix = chizhov::partition(
      shapes,
      sizeof(shapes) / sizeof(chizhov::shape_ptr));

  std::cout << " --- Matrix info ---\n";
  std::cout << "Total layers: " << matrix.getLayersCount() << '\n'
      << "Total shapes: " << matrix.getShapesCount() << "\n\n";

  for (size_t i = 0; i < matrix.getLayersCount(); i++) {
    std::cout << " --- Layer " << i << " ---\n";
    chizhov::Matrix::Layer layer = matrix[i];
    std::cout << "Total shapes: " << layer.getSize() << "\n\n";
    for (size_t j = 0; j < layer.getSize(); j++) {
      printInfo(*layer[j]);
    }
  }
}

int main()
{
  movingAndScaling();
  rotation();
  matrix();

  return 0;
}
