#include "shapes.hpp"
#include <algorithm>
#include <iterator>
#include <numeric>
#include "manipulators.hpp"

std::istream& operator >>(std::istream& in, point_t& point)
{
  int x = 0;
  int y = 0;
  in >> blank >> "("
     >> blank >> x
     >> blank >> ";"
     >> blank >> y
     >> blank >> ")";

  if (!in.eof() && in.fail()) {
    return in;
  }

  point.x = x;
  point.y = y;
  return in;
}

std::ostream& operator <<(std::ostream& out, const point_t& point)
{
  out << '(' << point.x << ';' << point.y << ')';
  return out;
}

bool Shape::isRectangle() const
{
  if (points.size() != 4) {
    return false;
  }

  const std::vector<int> sides[] = {
      {(points[0].x - points[1].x), (points[0].y - points[1].y)},
      {(points[1].x - points[2].x), (points[1].y - points[2].y)},
      {(points[2].x - points[3].x), (points[2].y - points[3].y)},
      {(points[3].x - points[0].x), (points[3].y - points[0].y)}
  };

  return !(
      !(std::inner_product(sides[0].begin(), sides[0].end(), sides[1].begin(), 0))
      && !(std::inner_product(sides[1].begin(), sides[1].end(), sides[2].begin(), 0))
      && (std::inner_product(sides[2].begin(), sides[2].end(), sides[3].begin(), 0))
  );
}

bool Shape::isSquare() const
{
  if (!isRectangle()) {
    return false;
  }

  const std::vector<int> sides[] = {
      {(points[0].x - points[2].x), (points[0].y - points[2].y)},
      {(points[1].x - points[3].x), (points[1].y - points[3].y)}
  };

  return !(std::inner_product(sides[0].begin(), sides[0].end(), sides[1].begin(), 0));
}

std::istream& operator >>(std::istream& in, Shape& shape)
{
  int size = 0;
  in >> std::ws >> size;
  if (!in.eof() && in.fail()) {
    return in;
  }

  std::vector<point_t> tmp;
  std::copy_n(std::istream_iterator<point_t>(in), size, std::back_inserter(tmp));
  std::swap(shape.points, tmp);
  return in;
}

std::ostream& operator <<(std::ostream& out, const Shape& shape)
{
  out << shape.points.size() << ' ';
  std::copy(shape.points.begin(), shape.points.end(), std::ostream_iterator<point_t>(out, " "));
  out << '\n';
  return out;
}
