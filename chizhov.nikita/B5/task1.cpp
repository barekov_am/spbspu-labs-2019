#include <algorithm>
#include <iterator>
#include <unordered_set>

void task1(std::istream& in, std::ostream& out)
{
  std::unordered_set<std::string> dictionary((std::istream_iterator<std::string>(in)), std::istream_iterator<std::string>());
  if (!in.eof() && in.fail()) {
    throw std::runtime_error("Input failed.");
  }
  std::copy(dictionary.begin(), dictionary.end(), std::ostream_iterator<std::string>(out, "\n"));
}
