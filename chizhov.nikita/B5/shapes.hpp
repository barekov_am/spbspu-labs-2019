#ifndef CN_B5_SHAPES
#define CN_B5_SHAPES

#include <iostream>
#include <vector>

struct point_t {
  int x;
  int y;
};
std::istream& operator >>(std::istream&, point_t&);
std::ostream& operator <<(std::ostream&, const point_t&);

struct Shape {
  std::vector<point_t> points;

  bool isTriangle() const
  {
    return points.size() == 3; // There is no invalid triangles, right?
  }
  bool isRectangle() const;
  bool isTetragon() const
  {
    return points.size() == 4;
  }
  bool isSquare() const;
  bool isPentagon() const
  {
    return points.size() == 5; // And pentagons
  }
};
std::istream& operator >>(std::istream&, Shape&);
std::ostream& operator <<(std::ostream&, const Shape&);

#endif
