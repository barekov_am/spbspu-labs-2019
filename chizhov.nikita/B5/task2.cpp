#include <algorithm>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>
#include "shapes.hpp"

auto compareShapes = [](const Shape& lhs, const Shape& rhs)
    {
      if (lhs.isTetragon() && rhs.isTetragon()) {
        if (lhs.isSquare()) {
          return true;
        }

        if (rhs.isSquare()) {
          return false;
        }

        return !lhs.isRectangle();
      }

      return lhs.points.size() < rhs.points.size();
    };

void task2(std::istream& in, std::ostream& out)
{
  std::list<Shape> shapes((std::istream_iterator<Shape>(in >> std::noskipws)), std::istream_iterator<Shape>());
  if (!in.eof() && in.fail()) {
    throw std::runtime_error("Input failed.");
  }

  const int verticesCount = std::accumulate(shapes.begin(), shapes.end(), 0, [](int sum, const Shape& shape)
      {
        return sum += shape.points.size();
      }
  );
  const int trianglesCount = std::count_if(shapes.begin(), shapes.end(), [](const Shape& shape)
      {
        return shape.isTriangle();
      }
  );
  const int squaresCount = std::count_if(shapes.begin(), shapes.end(), [](const Shape& shape)
      {
        return shape.isSquare();
      }
  );
  const int rectanglesCount = std::count_if(shapes.begin(), shapes.end(), [](const Shape& shape)
      {
        return shape.isRectangle();
      }
  );

  out << "Vertices: " << verticesCount << '\n'
      << "Triangles: " << trianglesCount << '\n'
      << "Squares: " << squaresCount << '\n'
      << "Rectangles: " << rectanglesCount << '\n';

  shapes.erase(std::remove_if(shapes.begin(), shapes.end(), [](const Shape& shape)
      {
        return shape.isPentagon();
      }
  ), shapes.end());

  std::vector<point_t> points;
  std::for_each(shapes.begin(), shapes.end(), [&points](const Shape& shape)
      {
        points.push_back(shape.points[0]);
      }
  );
  out << "Points: ";
  std::copy(points.begin(), points.end(), std::ostream_iterator<point_t>(out, " "));
  out << '\n';

  shapes.sort(compareShapes);
  out << "Shapes:\n";
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator<Shape>(out));
}
