#ifndef CN_B4_DS
#define CN_B4_DS

#include <string>

struct dataStruct {
  std::string str;
  int key1;
  int key2;
};

std::istream& operator >>(std::istream&, dataStruct&);
std::ostream& operator <<(std::ostream&, const dataStruct&);
bool comparator(const dataStruct&, const dataStruct&);

#endif
