#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

#include "data-struct.hpp"

int main()
{
  std::cin >> std::noskipws;
  std::vector<dataStruct> vector((std::istream_iterator<dataStruct>(std::cin)), std::istream_iterator<dataStruct>());
  if (!std::cin.eof() && std::cin.fail()) {
    std::cerr << "Data read failed!\n";
    return 1;
  }

  std::sort(vector.begin(), vector.end(), comparator);
  std::copy(vector.begin(), vector.end(), std::ostream_iterator<dataStruct>(std::cout));
  return 0;
}
