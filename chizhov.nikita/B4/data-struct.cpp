#include "data-struct.hpp"

#include <iostream>

#include "manipulators.hpp"

struct str_t {
  std::string& str;
  str_t(std::string& str):
      str(str)
  { }
};

std::istream& operator >>(std::istream& in, str_t&& str)
{
  std::istream::sentry sentry(in);
  if (sentry) {
    getline(in, str.str);
  }

  if (str.str.empty()) {
    in.setstate(std::ios_base::failbit);
  }

  return in;
}

std::istream& delim(std::istream& in)
{
  if (in.peek() != ',') {
    in.setstate(std::ios_base::failbit);
  } else {
    in.get();
  }

  return in;
}

std::istream& operator >>(std::istream& in, dataStruct& ds)
{
  int key1;
  int key2;
  std::string str;
  in >> blank >> key1 >> delim >>
        blank >> key2 >> delim >>
        blank >> str_t(str);

  if (in.fail()) {
    in.setstate(std::ios_base::failbit);
    return in;
  }

  if ((abs(key1) > 5) || (abs(key2) > 5)) {
    in.setstate(std::ios_base::failbit);
    return in;
  }

  ds.key1 = key1;
  ds.key2 = key2;
  ds.str = str;
  return in;
}

std::ostream& operator <<(std::ostream& out, const dataStruct& ds)
{
  out << ds.key1 << ',' << ds.key2 << ',' << ds.str << '\n';
  return out;
}

bool comparator(const dataStruct& lhs, const dataStruct& rhs)
{
  if (lhs.key1 != rhs.key1) {
    return (lhs.key1 < rhs.key1);
  }

  if (lhs.key2 != rhs.key2) {
    return (lhs.key2 < rhs.key2);
  }

  return (lhs.str.size() < rhs.str.size());
}
