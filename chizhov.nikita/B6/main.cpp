#include <iostream>
#include <algorithm>
#include <iterator>
#include <list>

#include "stats.hpp"

int main()
{
  std::list<int> nums((std::istream_iterator<int>(std::cin)), std::istream_iterator<int>());
  if (!std::cin.eof() && std::cin.fail()) {
    std::cerr << "Invalid sequence. Is there is only integers?\n";
    return 1;
  }

  std::cout << std::for_each(nums.begin(), nums.end(), StatisticsCollector());
  return 0;
}
