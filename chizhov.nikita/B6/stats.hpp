#ifndef CN_B6_STATS
#define CN_B6_STATS

#include <ostream>

struct StatisticsCollector {
  int max;
  int min;
  double mean;
  unsigned int posCount;
  unsigned int negCount;
  long long int oddSum;
  long long int evenSum;

  bool firstLast; // Мы же не пишем is у bool?
  bool empty;

  StatisticsCollector();
  void operator ()(int);
private:
  int first;
  size_t count;
};
std::ostream& operator <<(std::ostream&, const StatisticsCollector&);

#endif
