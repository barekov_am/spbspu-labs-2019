#include "stats.hpp"
#include <algorithm>

#include <iostream>

StatisticsCollector::StatisticsCollector() :
    max(0),
    min(0),
    mean(0),
    posCount(0),
    negCount(0),
    oddSum(0),
    evenSum(0),
    firstLast(false),
    empty(true),
    first(0),
    count(0)
{ }

void StatisticsCollector::operator ()(int num)
{
  if (empty) {
    first = num;
    min = num;
    max = num;
    empty = false;
  }

  max = std::max(max, num);
  min = std::min(min, num);

  if (num) {
    (num > 0 ? posCount : negCount)++;
  }

  ((num % 2) ? oddSum : evenSum) += num;
  mean = static_cast<double>(oddSum + evenSum) / ++count;
  firstLast = first == num;
}

std::ostream& operator <<(std::ostream& out, const StatisticsCollector& stats)
{
  if (stats.empty) {
    out << "No Data\n";
    return out;
  }

  out << "Max: " << stats.max << "\n"
      << "Min: " << stats.min << "\n"
      << "Mean: " << std::fixed << stats.mean << "\n"
      << "Positive: " << stats.posCount << "\n"
      << "Negative: " << stats.negCount << "\n"
      << "Odd Sum: " << stats.oddSum << "\n"
      << "Even Sum: " << stats.evenSum << "\n"
      << "First/Last Equal: ";
  stats.firstLast ? out << "yes" : out << "no";
  out << '\n';

  return out;
}
