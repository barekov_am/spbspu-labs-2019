#include <random>
#include <ctime>
#include <vector>

#include "output.hpp"
#include "sort.hpp"
#include "strategy.hpp"

void fillRandom(double* array, int size)
{
  std::mt19937 gen;
  std::uniform_real_distribution<> dis(-1, 1);
  gen.seed(time(0));
  for (size_t i = 0; i < unsigned(size); ++i) {
    array[i] = dis(gen);
  }
}

void task4(const char* direction, size_t size)
{
  auto comparator = getComparator<double>(direction);

  if (!size) {
    return;
  }

  std::vector<double> vector(size, 0);
  fillRandom(&vector[0], size);
  std::cout << vector << '\n';

  sort<ViaAt>(vector, comparator);
  std::cout << vector << '\n';
}
