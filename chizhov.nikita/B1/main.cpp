#include <iostream>
#include <stdexcept>

void task1(const char* direction);
void task2(const char* filename);
void task3();
void task4(const char* direction, size_t size);

int main(int argc, char* argv[])
{
  try {
    if ((argc < 2) || (argc > 4)) {
      std::cerr << "Incorrect number of arguments: " << argc << ".\n";
      return 1;
    }

    char* ptr = nullptr;
    int requestedTask = std::strtol(argv[1], &ptr, 10);
    if (*ptr) {
      std::cerr << "Illegal task number. Is it even a number?\n";
      return 1;
    }

    switch (requestedTask) {
      case 1: {
        if (argc != 3) {
          std::cerr << "Illegal number of arguments for task 1. Must be 1.\n";
          return 1;
        }

        task1(argv[2]);
        break;
      }

      case 2: {
        if (argc != 3) {
          std::cerr << "Illegal number of arguments for task 2. Must be 1.\n";
          return 1;
        }

        task2(argv[2]);
        break;
      }

      case 3: {
        if (argc != 2) {
          std::cerr << "Illegal number of arguments for task 3. Must be 0.\n";
          return 1;
        }

        task3();
        break;
      }

      case 4: {
        if (argc != 4) {
          std::cerr << "Illegal number of arguments for task 4. Must be 2.\n";
          return 1;
        }

        int size = std::strtol(argv[3], &ptr, 10);
        if (*ptr) {
          std::cerr << "Illegal size. Is it even a number?\n";
          return 1;
        }

        task4(argv[2], size);
        break;
      }

      default: {
        std::cerr << "Illegal task number.\n";
        return 1;
      }
    }
  } catch (const std::exception& e) {
    std::cerr << "Task handler raised exception:\n";
    std::cerr << e.what() << '\n';
    return 1;
  }
  return 0;
}
