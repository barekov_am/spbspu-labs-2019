#ifndef CN_B1_STRATEGY
#define CN_B1_STRATEGY

#include <iterator>

template <typename T>
struct ViaBrackets
{
  using valueType = typename T::value_type;
  using containerType = T;

  static size_t begin(T&)
  {
    return 0;
  }

  static size_t end(T& vector)
  {
    return vector.size();
  }

  static valueType& get(T& vector, size_t index)
  {
    return vector[index];
  }
};

template <typename T>
struct ViaAt
{
  using valueType = typename T::value_type;
  using containerType = T;

  static size_t begin(T&)
  {
    return 0;
  }

  static size_t end(T& vector)
  {
    return vector.size();
  }

  static valueType& get(T& vector, size_t index)
  {
    return vector.at(index);
  }
};

template <typename T>
struct ViaIterator
{
  using valueType = typename T::value_type;
  using containerType = T;
  using iteratorType = typename T::iterator;

  static iteratorType begin(T& list)
  {
    return list.begin();
  }

  static iteratorType end(T& list)
  {
    return list.end();
  }

  static typename T::reference get(T&, iteratorType iterator)
  {
    return *iterator;
  }
};

#endif
