#ifndef CN_COMMON_MANIP
#define CN_COMMON_MANIP

#include <iosfwd>

std::istream& operator >>(std::istream&, const char*);
std::istream& blank(std::istream&);

#endif
