#ifndef CN_A3_COMPOSITESHAPE
#define CN_A3_COMPOSITESHAPE

#include "shape.hpp"

namespace chizhov
{
  class CompositeShape : public Shape {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape&);
    CompositeShape(CompositeShape&&);
    CompositeShape(const shape_ptr&);

    CompositeShape& operator =(const CompositeShape&);
    CompositeShape& operator =(CompositeShape&&);
    shape_ptr operator [](int i) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double, double) override;
    void move(point_t) override;
    void scale(double) override;
    void rotate(double) override;

    void add(const shape_ptr& shape);
    void remove(const shape_ptr&);
    void remove(int);
    inline int getCount() const
    {
      return count_;
    }
    /* Если я верно понял документацию, геттеры имеет смысл делать inline как раз из-за того, что
     * не будет производиться вызов оператора, а просто возьмется значение напрямую, даже если private */

  private:
    shapes_array shapes_;
    int count_;

    static shapes_array copyShapes(const CompositeShape&, int, int);
  };
}

#endif
