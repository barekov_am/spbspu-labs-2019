#include "composite-shape.hpp"

#include <cmath>
#include <stdexcept>
#include <algorithm>

// Конструкторы
chizhov::CompositeShape::CompositeShape() :
    count_(0)
{ }

chizhov::CompositeShape::CompositeShape(const CompositeShape& source) :
    shapes_(copyShapes(source, source.count_, source.count_)),
    count_(source.count_)
{ }

chizhov::CompositeShape::CompositeShape(CompositeShape&& source) :
    shapes_(std::move(source.shapes_)),
    count_(source.count_)
{
  source.count_ = 0;
}

chizhov::CompositeShape::CompositeShape(const shape_ptr& shape) :
    shapes_(std::make_unique<shape_ptr[]>(1)),
    count_(1)
{
  if (!shape) {
    throw std::invalid_argument("Shape cannot be null!");
  }

  shapes_[0] = shape;
}

// Операторы
chizhov::CompositeShape& chizhov::CompositeShape::operator =(const CompositeShape& rhs)
{
  if (this != &rhs) {
    shapes_ = copyShapes(rhs, rhs.count_, rhs.count_);
    count_ = rhs.count_;
  }

  return *this;
}

chizhov::CompositeShape& chizhov::CompositeShape::operator =(CompositeShape&& rhs)
{
  if (this != &rhs) {
    shapes_ = std::move(rhs.shapes_);
    count_ = rhs.count_;
    rhs.count_ = 0;
  }

  return *this;
}

chizhov::shape_ptr chizhov::CompositeShape::operator [](int index) const
{
  if ((index < 0) || (index >= count_)) {
    throw std::out_of_range("Index out of range!");
  }

  return shapes_[index];
}

// Методы
double chizhov::CompositeShape::getArea() const
{
  double area = 0;
  for (int i = 0; i < count_; i++) {
    area += shapes_[i]->getArea();
  }

  return area;
}

chizhov::rectangle_t chizhov::CompositeShape::getFrameRect() const
{
  if (count_ == 0) {
    return rectangle_t{0, 0, {0, 0}};
  }

  rectangle_t rectTmp = {0, 0, {0, 0}};
  int i = 0;
  for (; !rectTmp && (i < count_); i++) {
    rectTmp = shapes_[i]->getFrameRect();
  }
  double minX = rectTmp.pos.x - rectTmp.width / 2;
  double maxX = rectTmp.pos.x + rectTmp.width / 2;
  double minY = rectTmp.pos.y - rectTmp.height / 2;
  double maxY = rectTmp.pos.y + rectTmp.height / 2;

  if (!rectTmp) {
    return rectTmp;
  }

  for (i++; i < count_; i++) {
    rectTmp = shapes_[i]->getFrameRect();
    if (!rectTmp) {
      continue;
    }

    double tmp = rectTmp.pos.x - rectTmp.width / 2;
    minX = std::min(tmp, minX);

    tmp = rectTmp.pos.x + rectTmp.width / 2;
    maxX = std::max(tmp, maxX);

    tmp = rectTmp.pos.y - rectTmp.height / 2;
    minY = std::min(tmp, minY);

    tmp = rectTmp.pos.y - rectTmp.height / 2;
    maxY = std::max(tmp, maxY);
  }

  return rectangle_t{maxX - minX, maxY - minY, point_t{(maxX + minX) / 2, (maxY + minY) / 2}};
}

void chizhov::CompositeShape::move(double dx, double dy)
{
  for (int i = 0; i < count_; i++) {
    shapes_[i]->move(dx, dy);
  }
}

void chizhov::CompositeShape::move(point_t position)
{
  rectangle_t frameRect = getFrameRect();
  if (!frameRect) {
    return;
  }

  double dx = position.x - frameRect.pos.x;
  double dy = position.y - frameRect.pos.y;
  move(dx, dy);
}

void chizhov::CompositeShape::scale(double scale)
{
  if (scale <= 0) {
    throw std::invalid_argument("You cannot scale by non-positive multiplier");
  }

  rectangle_t frameRect = getFrameRect();

  for (int i = 0; i < count_; i++) {
    double dx = shapes_[i]->getFrameRect().pos.x - frameRect.pos.x;
    double dy = shapes_[i]->getFrameRect().pos.y - frameRect.pos.y;
    shapes_[i]->move(dx * (scale - 1), dy * (scale - 1));
    shapes_[i]->scale(scale);
  }
}

void chizhov::CompositeShape::rotate(double angle)
{
  rectangle_t frameRect = getFrameRect();

  double angleRad = M_PI * angle / 180;
  for (int i = 0; i < count_; i++) {
    point_t delta = shapes_[i]->getFrameRect().pos;
    delta.x -= frameRect.pos.x;
    delta.y -= frameRect.pos.y;
    shapes_[i]->rotate(angle);
    double dx = delta.x * (std::cos(angleRad) - 1) - delta.y * std::sin(angleRad);
    double dy = delta.y * (std::cos(angleRad) - 1) + delta.x * std::sin(angleRad);
    shapes_[i]->move(dx, dy);
  }
}

void chizhov::CompositeShape::add(const shape_ptr& shape)
{
  if (!shape) {
    throw std::invalid_argument("Shape cannot be null!");
  }

  for (int i = 0; i < count_; i++) {
    if (shapes_[i].get() == shape.get()) {
      return;
    }
  }

  shapes_ = copyShapes(*this, count_, count_ + 1);
  shapes_[count_++] = shape;
}

void chizhov::CompositeShape::remove(const shape_ptr& shape)
{
  if (!shape) {
    return;
  }

  int j = 0;
  bool removed = false;
  for (int i = 0; i < count_; i++) {
    if (shapes_[i].get() == shape.get()) {
      removed = true;
      continue;
    }

    shapes_[j] = shapes_[i];
    j++;
  }

  if (removed) {
    shapes_[--count_] = nullptr;
  }
}

void chizhov::CompositeShape::remove(int index)
{
  if ((index < 0) || (index >= count_)) {
    throw std::out_of_range("Index out of range!");
  }

  int j = 0;
  for (int i = 0; i < count_; i++) {
    if (i == index) {
      continue;
    }

    shapes_[j] = shapes_[i];
    j++;
  }

  shapes_[--count_] = nullptr;
}

chizhov::shapes_array chizhov::CompositeShape::copyShapes(const CompositeShape& source, int count, int size)
{
  shapes_array shapes = std::make_unique<shape_ptr[]>(size);
  for (int i = 0; i < count; i++) {
    shapes[i] = source.shapes_[i];
  }
  return shapes;
}
