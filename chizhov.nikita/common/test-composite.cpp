#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(chizhovCompositeTestSuite)

const double EPSILON = 0.01;

BOOST_AUTO_TEST_CASE(compositeConstantAfterMove)
{
  chizhov::Circle circle({1, 2}, 1);
  chizhov::Rectangle rectangle({2 ,1}, 2);

  chizhov::CompositeShape composite(std::make_shared<chizhov::Circle>(circle));
  composite.add(std::make_shared<chizhov::Rectangle>(rectangle));
  const chizhov::rectangle_t frameBefore = composite.getFrameRect();
  const double areaBefore = composite.getArea();

  composite.move({0, 0});
  chizhov::rectangle_t frameAfter = composite.getFrameRect();
  double areaAfter = composite.getArea();
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);

  composite.move(5, 5);
  frameAfter = composite.getFrameRect();
  areaAfter = composite.getArea();
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
}

BOOST_AUTO_TEST_CASE(compositeScale)
{
  chizhov::Circle circle({1, 2}, 1);
  chizhov::Rectangle rectangle({2 ,1}, 2);

  chizhov::CompositeShape composite(std::make_shared<chizhov::Circle>(circle));
  composite.add(std::make_shared<chizhov::Rectangle>(rectangle));
  const double areaBefore = composite.getArea();

  const int scaleMultiplier = 2;
  composite.scale(scaleMultiplier);
  double areaAfter = composite.getArea();
  BOOST_CHECK_CLOSE(areaBefore * scaleMultiplier * scaleMultiplier, areaAfter, EPSILON);
}

BOOST_AUTO_TEST_CASE(compositeRotate)
{
  const double x = 1;
  const double size = 2;
  chizhov::Circle circle({x, 0}, size / 2);
  chizhov::Rectangle rectangle({-x, 0}, size);

  chizhov::CompositeShape composite(std::make_shared<chizhov::Circle>(circle));
  composite.add(std::make_shared<chizhov::Rectangle>(rectangle));
  const double areaBefore = composite.getArea();
  const chizhov::rectangle_t frameBefore = composite.getFrameRect();

  const double angle = 90;
  composite.rotate(angle);
  double areaAfter = composite.getArea();
  chizhov::rectangle_t frameAfter = composite.getFrameRect();
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  BOOST_CHECK_CLOSE(frameBefore.pos.x, frameAfter.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameBefore.pos.y, frameAfter.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(scaleZeroException)
{
  chizhov::Circle circle({1, 2}, 1);
  chizhov::CompositeShape composite(std::make_shared<chizhov::Circle>(circle));
  BOOST_CHECK_THROW(composite.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(nullShapeException)
{
  chizhov::CompositeShape emptyComposite;
  BOOST_CHECK_THROW(emptyComposite.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(chizhov::CompositeShape(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(outOfRangeExceptionA3)
{
  chizhov::CompositeShape emptyComposite;
  BOOST_CHECK_THROW(emptyComposite[-1], std::out_of_range);
  BOOST_CHECK_THROW(emptyComposite[0], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeWithEmptyComposite)
{
  chizhov::CompositeShape emptyComposite;
  chizhov::shape_ptr emptyCompositePtr = std::make_shared<chizhov::CompositeShape>(emptyComposite);
  chizhov::Circle circle({5, 5}, 1);
  chizhov::CompositeShape composite;
  composite.add(emptyCompositePtr);
  composite.add(emptyCompositePtr);
  composite.add(std::make_shared<chizhov::Circle>(circle));
  composite.add(emptyCompositePtr);
  composite.add(emptyCompositePtr);

  const chizhov::rectangle_t frameCircle = circle.getFrameRect();
  chizhov::rectangle_t frameComposite = composite.getFrameRect();

  BOOST_CHECK_CLOSE(frameCircle.width, frameComposite.width, EPSILON);
  BOOST_CHECK_CLOSE(frameCircle.height, frameComposite.height, EPSILON);
  BOOST_CHECK_CLOSE(frameCircle.pos.x, frameComposite.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameCircle.pos.y, frameComposite.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(compositeGetAndDelete)
{
  chizhov::Circle circle({1, 2}, 1);
  chizhov::Rectangle rectangle({2 ,1}, 2);

  chizhov::CompositeShape composite(std::make_shared<chizhov::Circle>(circle));
  const chizhov::rectangle_t frameBefore = composite.getFrameRect();
  const double areaBefore = composite.getArea();

  composite.add(std::make_shared<chizhov::Rectangle>(rectangle));

  int count = composite.getCount();
  std::shared_ptr<chizhov::Shape> shapeToDelete = composite[count - 1];
  composite.remove(shapeToDelete);

  chizhov::rectangle_t frameAfter = composite.getFrameRect();
  double areaAfter = composite.getArea();
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
}

BOOST_AUTO_TEST_CASE(compositeDeleteByIndex)
{
  chizhov::Circle circle({1, 2}, 1);
  chizhov::Rectangle rectangle({2 ,1}, 2);

  chizhov::CompositeShape composite(std::make_shared<chizhov::Circle>(circle));
  const chizhov::rectangle_t frameBefore = composite.getFrameRect();
  const double areaBefore = composite.getArea();

  composite.add(std::make_shared<chizhov::Rectangle>(rectangle));

  int count = composite.getCount();
  composite.remove(count - 1);

  chizhov::rectangle_t frameAfter = composite.getFrameRect();
  double areaAfter = composite.getArea();
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
}

BOOST_AUTO_TEST_CASE(moveSemanticsComposite)
{
  chizhov::Circle circle({1, 2}, 1);
  chizhov::Rectangle rectangle({2 ,1}, 2);

  chizhov::CompositeShape compositeFrom(std::make_shared<chizhov::Circle>(circle));
  chizhov::CompositeShape compositeTo(std::make_shared<chizhov::Rectangle>(rectangle));

  const chizhov::rectangle_t frameBefore = compositeFrom.getFrameRect();
  const double areaBefore = compositeFrom.getArea();

  compositeTo = std::move(compositeFrom);

  chizhov::rectangle_t frameAfter = compositeTo.getFrameRect();
  double areaAfter = compositeTo.getArea();
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
}

BOOST_AUTO_TEST_CASE(copySemanticsComposite)
{
  chizhov::Circle circle({1, 2}, 1);
  chizhov::Rectangle rectangle({2 ,1}, 2);

  chizhov::CompositeShape compositeFrom(std::make_shared<chizhov::Circle>(circle));
  chizhov::CompositeShape compositeTo(std::make_shared<chizhov::Rectangle>(rectangle));

  const double targetArea = circle.getArea() + rectangle.getArea();

  compositeTo = compositeFrom;
  compositeTo.add(std::make_shared<chizhov::Rectangle>(rectangle));

  BOOST_CHECK_CLOSE(targetArea, compositeTo.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(compositeAddDouble)
{
  chizhov::Circle circle({1, 2}, 1);
  auto circle_ptr = std::make_shared<chizhov::Circle>(circle);
  chizhov::CompositeShape composite(circle_ptr);
  const int countBefore = composite.getCount();
  composite.add(circle_ptr);

  BOOST_CHECK_EQUAL(countBefore, composite.getCount());
}

BOOST_AUTO_TEST_SUITE_END()
