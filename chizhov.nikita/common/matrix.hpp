#ifndef CN_A4_MATRIX
#define CN_A4_MATRIX

#include "shape.hpp"

namespace chizhov
{
  class Matrix;
  Matrix partition(const shape_ptr*, size_t);

  class Matrix
  {
  public:
    class Layer
    {
    public:
      Layer(const Layer&);
      Layer(Layer&&);

      Layer& operator =(const Layer&);
      Layer& operator =(Layer&&);
      shape_ptr operator [](size_t) const;

      inline size_t getSize() const
      {
        return size_;
      }
    private:
      Layer(const shape_ptr*, size_t);

      shapes_array shapes_;
      size_t size_;

      static shapes_array copyLayer(const Layer&);
      friend chizhov::Matrix;
    };

    Matrix(const Matrix&);
    Matrix(Matrix&&);

    Matrix& operator =(const Matrix&);
    Matrix& operator =(Matrix&&);
    Layer operator [](size_t) const;
    bool operator ==(const Matrix&) const;
    bool operator !=(const Matrix&) const;

    inline size_t getShapesCount() const
    {
      return shapesCount_;
    }

    inline size_t getLayersCount() const
    {
      return layersCount_;
    }

    inline size_t getLayerSize(size_t index) const
    {
      return (*this)[index].getSize();
    }

  private:
    Matrix();

    shapes_array shapes_;
    size_t shapesCount_;
    size_t layerSize_;
    size_t layersCount_;

    static shapes_array copyMatrix(const Matrix&);
    friend Matrix partition(const shape_ptr*, size_t);
  };
}

#endif
