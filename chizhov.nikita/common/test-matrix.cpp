#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

BOOST_AUTO_TEST_SUITE(chizhovCompositeTestSuite)

const double EPSILON = 0.01;

BOOST_AUTO_TEST_CASE(matrixCreationWithoutOverlap)
{
  chizhov::Circle circle({0, 0}, 1);
  chizhov::Rectangle rectangle({10, 10}, 2);
  chizhov::shape_ptr shapes[] = {std::make_shared<chizhov::Circle>(circle),
      std::make_shared<chizhov::Rectangle>(rectangle)};

  chizhov::Matrix matrix = chizhov::partition(shapes, sizeof(shapes) / sizeof(chizhov::shape_ptr));

  BOOST_CHECK_EQUAL(matrix.getShapesCount(), 2);
  BOOST_CHECK_EQUAL(matrix.getLayersCount(), 1);
}

BOOST_AUTO_TEST_CASE(matrixCreationWithOverlap)
{
  chizhov::Circle circle({0, 0}, 1);
  chizhov::Rectangle rectangle({0, 0}, 2);
  chizhov::shape_ptr shapes[] = {std::make_shared<chizhov::Circle>(circle),
      std::make_shared<chizhov::Rectangle>(rectangle)};

  chizhov::Matrix matrix = chizhov::partition(shapes, sizeof(shapes) / sizeof(chizhov::shape_ptr));

  BOOST_CHECK_EQUAL(matrix.getShapesCount(), 2);
  BOOST_CHECK_EQUAL(matrix.getLayersCount(), 2);
}

BOOST_AUTO_TEST_CASE(layerCorrectSizeWithoutOverlap)
{
  chizhov::Circle circle({0, 0}, 1);
  chizhov::Rectangle rectangle({10, 10}, 2);
  chizhov::shape_ptr shapes[] = {std::make_shared<chizhov::Circle>(circle),
    std::make_shared<chizhov::Rectangle>(rectangle)};

  chizhov::Matrix matrix = chizhov::partition(shapes, sizeof(shapes) / sizeof(chizhov::shape_ptr));
  chizhov::Matrix::Layer layer = matrix[0];

  BOOST_CHECK_EQUAL(matrix.getShapesCount(), layer.getSize());
}

BOOST_AUTO_TEST_CASE(outOfRangeExceptionA4)
{
  chizhov::Circle circle({0, 0}, 1);
  chizhov::Rectangle rectangle({10, 10}, 2);
  chizhov::shape_ptr shapes[] = {std::make_shared<chizhov::Circle>(circle),
                                   std::make_shared<chizhov::Rectangle>(rectangle)};

  chizhov::Matrix matrix = chizhov::partition(shapes, sizeof(shapes) / sizeof(chizhov::shape_ptr));
  chizhov::Matrix::Layer layer = matrix[0];

  BOOST_CHECK_THROW(matrix[10], std::out_of_range);
  BOOST_CHECK_THROW(layer[10], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
