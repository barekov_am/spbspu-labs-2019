#include "base-types.hpp"

chizhov::rectangle_t::operator bool() const
{
  return ((width != 0) || (height != 0) || (pos.x != 0) || (pos.y != 0));
}

bool chizhov::intersect(const rectangle_t& lhs, const rectangle_t& rhs)
{
  const point_t lhsLB = {lhs.pos.x - lhs.width / 2, lhs.pos.y - lhs.height / 2};
  const point_t lhsRT = {lhs.pos.x + lhs.width / 2, lhs.pos.y + lhs.height / 2};
  const point_t rhsLB = {rhs.pos.x - rhs.width / 2, rhs.pos.y - rhs.height / 2};
  const point_t rhsRT = {rhs.pos.x + rhs.width / 2, rhs.pos.y + rhs.height / 2};

  return ((lhsLB.x < rhsRT.x && lhsLB.y < rhsRT.y) && (rhsLB.x < lhsRT.x && rhsLB.y < lhsRT.y));
}
