#include "manipulators.hpp"
#include <iostream>

std::istream& operator >>(std::istream& in, const char* literal)
{
  int i = 0;
  while (literal[i] != '\0') {
    if (in.peek() == literal[i]) {
      in.get();
      i++;
    } else {
      in.setstate(std::ios_base::failbit);
      return in;
    }
  }

  return in;
}

std::istream& blank(std::istream& in)
{
  while (std::isblank(in.peek())) {
    in.get();
  }
  return in;
}
