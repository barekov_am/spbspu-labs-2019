#include "matrix.hpp"

#include <stdexcept>
#include <algorithm>

// chizhov::Matrix
// Конструкторы
chizhov::Matrix::Matrix():
    shapesCount_(0),
    layerSize_(0),
    layersCount_(0)
{ }

chizhov::Matrix::Matrix(const Matrix& source):
    shapes_(copyMatrix(source)),
    shapesCount_(source.shapesCount_),
    layerSize_(source.layerSize_),
    layersCount_(source.layersCount_)
{ }

chizhov::Matrix::Matrix(Matrix&& source):
    shapes_(std::move(source.shapes_)),
    shapesCount_(source.shapesCount_),
    layerSize_(source.layerSize_),
    layersCount_(source.layersCount_)
{
  source.shapesCount_ = 0;
  source.layerSize_ = 0;
  source.layersCount_ = 0;
}

// Операторы
chizhov::Matrix& chizhov::Matrix::operator =(const Matrix& rhs)
{
  if (this != &rhs) {
    shapes_ = copyMatrix(rhs);
    shapesCount_ = rhs.shapesCount_;
    layerSize_ = rhs.layerSize_;
    layersCount_ = rhs.layersCount_;
  }

  return *this;
}

chizhov::Matrix& chizhov::Matrix::operator =(Matrix&& rhs)
{
  if (this != &rhs) {
    shapes_ = std::move(rhs.shapes_);
    shapesCount_ = rhs.shapesCount_;
    rhs.shapesCount_ = 0;
    layerSize_ = rhs.layerSize_;
    rhs.layerSize_ = 0;
    layersCount_ = rhs.layersCount_;
    rhs.layersCount_ = 0;
  }

  return *this;
}

chizhov::Matrix::Layer chizhov::Matrix::operator [](size_t index) const
{
  if (index >= layersCount_) {
    throw std::out_of_range("Index out of range!");
  }

  size_t layerSize = 0;
  for (size_t i = 0; i < layerSize_; i++) {
    if (!shapes_[index * layerSize_ + i]) {
      break;
    }

    layerSize++;
  }

  shape_ptr layer[layerSize];
  for (size_t i = 0; i < layerSize; i++) {
    layer[i] = shapes_[index * layerSize_ + i];
  }

  return Layer(layer, layerSize);
}

bool chizhov::Matrix::operator ==(const chizhov::Matrix& rhs) const
{
  if ((layersCount_ == rhs.layersCount_) && (shapesCount_ == rhs.shapesCount_)) {
    if (this == &rhs) {
      return true;
    } else {
      for (size_t i = 0; i < shapesCount_; i++) {
        if (shapes_[i] != rhs.shapes_[i]) {
          break;
        }
      }
    }
  }

  return false;
}

bool chizhov::Matrix::operator !=(const chizhov::Matrix& rhs) const
{
  return !(*this == rhs);
}

// Методы
chizhov::shapes_array chizhov::Matrix::copyMatrix(const Matrix& source)
{
  shapes_array shapes = std::make_unique<shape_ptr[]>(source.shapesCount_);
  for (size_t i = 0; i < source.shapesCount_; i++) {
    shapes[i] = source.shapes_[i];
  }
  return shapes;
}

// chizhov::Layer
// Конструкторы
chizhov::Matrix::Layer::Layer(const shape_ptr* shapes, size_t size):
    shapes_(std::make_unique<shape_ptr[]>(size)),
    size_(size)
{
  for (size_t i = 0; i < size; i++) {
    shapes_[i] = shapes[i];
  }
}

chizhov::Matrix::Layer::Layer(const Layer& source):
    shapes_(copyLayer(source)),
    size_(source.size_)
{ }

chizhov::Matrix::Layer::Layer(Layer&& source):
    shapes_(std::move(source.shapes_)),
    size_(source.size_)
{ }

// Операторы
chizhov::Matrix::Layer& chizhov::Matrix::Layer::operator =(const Layer& rhs)
{
  if (this != &rhs) {
    shapes_ = copyLayer(rhs);
    size_ = rhs.size_;
  }

  return *this;
}

chizhov::Matrix::Layer& chizhov::Matrix::Layer::operator =(Layer&& rhs)
{
  if (this != &rhs) {
    shapes_ = std::move(rhs.shapes_);
    size_ = rhs.size_;
  }

  return *this;
}

chizhov::shape_ptr chizhov::Matrix::Layer::operator [](size_t index) const
{
  if (index >= size_) {
    throw std::out_of_range("Index out of range!");
  }

  return shapes_[index];
}

// Методы
chizhov::shapes_array chizhov::Matrix::Layer::copyLayer(const Layer& source)
{
  shapes_array shapes = std::make_unique<shape_ptr[]>(source.size_);
  for (size_t i = 0; i < source.size_; i++) {
    shapes[i] = source.shapes_[i];
  }
  return shapes;
}

// chizhov
chizhov::Matrix chizhov::partition(const chizhov::shape_ptr* shapes, size_t size)
{
  if (!shapes) {
    throw std::logic_error("Shapes array is empty!");
  }

  chizhov::Matrix matrix;
  chizhov::shapes_array layers[size];
  size_t layersSizes[size];
  for (size_t i = 0; i < size; i++) {
    layersSizes[i] = 0;
  }

  size_t layersCount = 1;

  for (size_t i = 0; i < size; i++) {
    size_t layer = 0;
    for (int j = layersCount - 1; j >= 0; j--) {
      bool intersected = false;

      for (size_t k = 0; k < layersSizes[j]; k++) {
        if (intersect(shapes[i]->getFrameRect(), layers[j][k]->getFrameRect())) {
          intersected = true;
          break;
        }
      }

      if (intersected) {
        layer = j + 1;
        break;
      }
    }

    if (layersCount == layer) {
      layersCount++;
    }

    chizhov::shapes_array tmpLayer = std::make_unique<chizhov::shape_ptr[]>(layersSizes[layer] + 1);
    for (size_t j = 0; j < layersSizes[layer]; j++) {
      tmpLayer[j] = layers[layer][j];
    }

    tmpLayer[layersSizes[layer]++] = shapes[i];
    layers[layer] = std::move(tmpLayer);
  }

  matrix.layersCount_ = layersCount;
  for (size_t i = 0; i < matrix.layersCount_; i++) {
    matrix.layerSize_ = matrix.layerSize_ < layersSizes[i] ? layersSizes[i] : matrix.layerSize_;
  }

  matrix.shapesCount_ = size;
  matrix.shapes_ = std::make_unique<chizhov::shape_ptr[]>(matrix.layersCount_ * matrix.layerSize_);

  for (size_t i = 0; i < matrix.layersCount_; i++) {
    for (size_t j = 0; j < layersSizes[i]; j++) {
      matrix.shapes_[matrix.layerSize_ * i + j] = layers[i][j];
    }
  }

  return matrix;
}
