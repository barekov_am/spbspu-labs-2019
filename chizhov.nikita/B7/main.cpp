#include <iostream>
#include <stdexcept>

void task1(std::istream&, std::ostream&);
void task2(std::istream&, std::ostream&);

int main(int argc, char* argv[])
{
  try {
    if (argc != 2) {
      std::cerr << "Incorrect number of arguments: " << argc << ". Must be 2.\n";
      return 1;
    }

    char* ptr = nullptr;
    int requestedTask = std::strtol(argv[1], &ptr, 10);
    if (*ptr) {
      std::cerr << "Illegal task number. Is it even a number?\n";
      return 1;
    }

    switch (requestedTask) {
      case 1: {
        task1(std::cin, std::cout);
        break;
      }

      case 2: {
        task2(std::cin, std::cout);
        break;
      }

      default: {
        std::cerr << "Incorrect task number.\n";
        return 1;
      }
    }
  } catch (const std::invalid_argument& e) {
    std::cerr << "Invalid arguments were given to task handler:\n";
    std::cerr << e.what() << '\n';
    return 1;
  } catch (const std::exception& e) {
    std::cerr << "Task handler raised exception:\n";
    std::cerr << e.what() << '\n';
    return 2;
  }
  return 0;
}
