#include "figures.hpp"
#include <algorithm>
#include "manipulators.hpp"

std::istream& operator >>(std::istream& in, point_t& point)
{
  int x = 0;
  int y = 0;
  in >> blank >> "("
     >> blank >> x
     >> blank >> ";"
     >> blank >> y
     >> blank >> ")";

  if (!in.eof() && in.fail()) {
    return in;
  }

  point.x = x;
  point.y = y;
  return in;
}

std::ostream& operator <<(std::ostream& out, const point_t& point)
{
  out << '(' << point.x << ';' << point.y << ')';
  return out;
}

Shape::Shape(const point_t& center) :
    center_(center)
{ }

bool Shape::isMoreLeft(const class Shape& rhs)
{
  return center_.x < rhs.center_.x;
}

bool Shape::isUpper(const class Shape& rhs)
{
  return center_.y > rhs.center_.y;
}

std::ostream& operator <<(std::ostream& out, const Shape::shape_ptr& ptr)
{
  ptr->draw(out);
  return out;
}

static Shape::shape_ptr createCirclePtr(const point_t& center)
{
  return std::make_shared<Circle>(center);
}
static Shape::shape_ptr createTrianglePtr(const point_t& center)
{
  return std::make_shared<Triangle>(center);
}
static Shape::shape_ptr createSquarePtr(const point_t& center)
{
  return std::make_shared<Square>(center);
}

void Circle::draw(std::ostream& out) const
{
  out << "CIRCLE " << center_ << '\n';
}

void Triangle::draw(std::ostream& out) const
{
  out << "TRIANGLE " << center_ << '\n';
}

void Square::draw(std::ostream& out) const
{
  out << "SQUARE " << center_ << '\n';
}

std::istream& operator >>(std::istream& in, Shape::shape_ptr& ptr)
{
  const static struct {
    const char* name;
    Shape::shape_ptr (*create)(const point_t&);
  } accordance[] = {
      {"CIRCLE", &createCirclePtr},
      {"TRIANGLE", &createTrianglePtr},
      {"SQUARE", &createSquarePtr}
  };

  in >> std::ws;
  std::string shapeType;
  char tmp;
  while (isalpha(in.peek())) {
    in.get(tmp);
    shapeType += tmp;
  }
  auto pointerCreator = std::find_if(accordance, std::end(accordance), [&shapeType](const auto& elem)
      {
        return elem.name == shapeType;
      }
  );
  if (pointerCreator == std::end(accordance)) {
    in.setstate(std::ios_base::failbit);
    return in;
  }

  point_t center;
  in >> blank >> center;
  if (!in.eof() && in.fail()) {
    return in;
  }
  ptr = pointerCreator->create(center);
  return in;
}
