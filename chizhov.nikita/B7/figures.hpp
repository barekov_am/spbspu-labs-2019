#ifndef CN_B7_FIGURES
#define CN_B7_FIGURES

#include <iostream>
#include <memory>

struct point_t {
  int x;
  int y;
};
std::istream& operator >>(std::istream&, point_t&);
std::ostream& operator <<(std::ostream&, const point_t&);

class Shape {
public:
  using shape_ptr = std::shared_ptr<Shape>;

  Shape(const point_t&);
  virtual ~Shape() = default;

  bool isMoreLeft(const Shape&);
  bool isUpper(const Shape&);

  virtual void draw(std::ostream&) const = 0;
protected:
  point_t center_;
};
std::ostream& operator <<(std::ostream&, const Shape::shape_ptr&);

class Circle : public Shape {
public:
  using Shape::Shape;
  void draw(std::ostream&) const override;
};

class Triangle : public Shape {
public:
  using Shape::Shape;
  void draw(std::ostream&) const override;
};

class Square : public Shape {
public:
  using Shape::Shape;
  void draw(std::ostream&) const override;
};

std::istream& operator >>(std::istream&, Shape::shape_ptr&);

#endif
