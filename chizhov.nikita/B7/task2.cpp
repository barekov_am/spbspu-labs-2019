#include <algorithm>
#include <functional>
#include <list>
#include <iostream>
#include <iterator>
#include "figures.hpp"
#include <boost/io/ios_state.hpp>

void task2(std::istream& in, std::ostream& out)
{
  using shape_istream_iterator = std::istream_iterator<Shape::shape_ptr>;
  using shape_ostream_iterator = std::ostream_iterator<Shape::shape_ptr>;

  boost::io::ios_flags_saver saver_in(in);
  boost::io::ios_flags_saver saver_out(out);

  std::list<Shape::shape_ptr> shapes(shape_istream_iterator(in >> std::noskipws), shape_istream_iterator());
  if (!in.eof() && in.fail()) {
    throw std::invalid_argument("Invalid shapes sequence.");
  }

  std::cout << "Original:\n";
  std::copy(shapes.begin(), shapes.end(), shape_ostream_iterator(std::cout));

  std::cout << "Left-Right:\n";
  shapes.sort([](const Shape::shape_ptr& lhs, const Shape::shape_ptr& rhs)
      {
        return lhs->isMoreLeft(*rhs);
      }
  );
  std::copy(shapes.begin(), shapes.end(), shape_ostream_iterator(std::cout));

  std::cout << "Right-Left:\n";
  shapes.sort([](const Shape::shape_ptr& lhs, const Shape::shape_ptr& rhs)
      {
        return rhs->isMoreLeft(*lhs);
      }
  );
  std::copy(shapes.begin(), shapes.end(), shape_ostream_iterator(std::cout));

  std::cout << "Top-Bottom:\n";
  shapes.sort([](const Shape::shape_ptr& lhs, const Shape::shape_ptr& rhs)
      {
        return lhs->isUpper(*rhs);
      }
  );
  std::copy(shapes.begin(), shapes.end(), shape_ostream_iterator(std::cout));

  std::cout << "Bottom-Top:\n";
  shapes.sort([](const Shape::shape_ptr& lhs, const Shape::shape_ptr& rhs)
      {
        return rhs->isUpper(*lhs);
      }
  );
  std::copy(shapes.begin(), shapes.end(), shape_ostream_iterator(std::cout));
}
