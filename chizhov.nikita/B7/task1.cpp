#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <iterator>
#include <list>

void task1(std::istream& in, std::ostream& out)
{
  std::list<double> nums((std::istream_iterator<double>(in)), std::istream_iterator<double>());
  if (!in.eof() && in.fail()) {
    throw std::invalid_argument("Invalid sequence. Is there is only integers?\n");
  }
  std::transform(nums.begin(), nums.end(), std::ostream_iterator<double>(out, " "), std::bind(
      std::multiplies<>(), M_PI, std::placeholders::_1)
  );
  out << '\n';
}
