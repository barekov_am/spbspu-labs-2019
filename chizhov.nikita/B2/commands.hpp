#ifndef CN_B2_CMDS
#define CN_B2_CMDS

#include <algorithm>
#include <functional>
#include <memory>
#include <sstream>

#include "priorityQueue.hpp"

namespace commands
{
  class Command;
  using stringQueue = QueueWithPriority<std::string>;
  using commandPtr = std::unique_ptr<Command>;

  class Command : std::unary_function<stringQueue&, std::string> {
  public:
    virtual ~Command() = default;
    virtual std::string operator ()(stringQueue&) const = 0;
  };

  std::istream& operator >>(std::istream&, commandPtr&);
}

#endif
