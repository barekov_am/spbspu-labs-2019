#include "commands.hpp"

#include "command-functors.hpp"

const std::string MSG_INVALID = "<INVALID COMMAND>\n";

namespace commands
{
  using handlerType = std::function<commandPtr(std::istream&)>;

  struct data_t
  {
    std::string& data;
    data_t(std::string& string):
        data(string)
    { }
  };

  std::istream& operator >>(std::istream&, stringQueue::ElementPriority&);
  std::istream& operator >>(std::istream&, data_t&);

  static commandPtr handleAdd(std::istream&);
  static commandPtr handleGet(std::istream&);
  static commandPtr handleAccelerate(std::istream&);

  std::istream& operator >>(std::istream& input, stringQueue::ElementPriority& priority)
  {
    static const struct
    {
      const char* name;
      stringQueue::ElementPriority value;
    } queuePriorities[] = {
        {"low", stringQueue::ElementPriority::LOW},
        {"normal", stringQueue::ElementPriority::NORMAL},
        {"high", stringQueue::ElementPriority::HIGH},
    };

    std::istream::sentry sentry(input);
    if (sentry) {
      std::string priorityName;
      input >> priorityName;

      auto result = std::find_if(std::begin(queuePriorities), std::end(queuePriorities),
          [&](auto& element)
          {
            return (priorityName == element.name);
          }
      );

      if (result == std::end(queuePriorities)) {
        input.setstate(std::ios_base::failbit);
        return input;
      }

      priority = result->value;
    }

    return input;
  }

  std::istream& operator >>(std::istream& input, commandPtr& command)
  {
    static const struct
    {
      const char* name;
      handlerType value;
    } queueHandlers[] = {
        {"add", &handleAdd},
        {"get", &handleGet},
        {"accelerate", &handleAccelerate}
    };

    std::istream::sentry sentry(input);
    if (sentry) {
      std::string commandName;
      input >> commandName;

      auto result = std::find_if(queueHandlers, std::end(queueHandlers),
          [&](auto& element)
          {
            return (element.name == commandName);
          }
      );

      if (result == std::end(queueHandlers)) {
        command = std::make_unique<CommandError>(CommandError(MSG_INVALID));
        return input;
      }

      command = result->value(input);
    }

    return input;
  }

  std::istream& operator >>(std::istream& input, data_t&& data)
  {
    std::istream::sentry sentry(input);
    if (sentry) {
      getline(input, data.data);
    }

    if (data.data.empty()) {
      input.setstate(std::ios_base::failbit);
    }

    return input;
  }

  static commandPtr handleAdd(std::istream& input)
  {
    stringQueue::ElementPriority priority;
    std::string data;
    input >> std::ws >> priority >> std::ws >> data_t(data);

    if (input.fail()) {
      return std::make_unique<CommandError>(CommandError(MSG_INVALID));
    }

    return std::make_unique<CommandAdd>(CommandAdd(priority, data));
  }

  static commandPtr handleGet(std::istream& input)
  {
    std::string arguments;
    std::getline(input >> std::ws, arguments);

    if (!arguments.empty()) {
      return std::make_unique<CommandError>(CommandError(MSG_INVALID));
    }

    return std::make_unique<CommandGet>(CommandGet());
  }

  static commandPtr handleAccelerate(std::istream& input)
  {
    std::string arguments;
    std::getline(input >> std::ws, arguments);

    if (!arguments.empty()) {
      return std::make_unique<CommandError>(CommandError(MSG_INVALID));
    }

    return std::make_unique<CommandAccelerate>(CommandAccelerate());
  }
}
