#include <iostream>
#include <iterator>
#include <list>
#include <stdexcept>
#include <sstream>

const int MAX_SIZE = 20;
const int MIN_VALUE = 1;
const int MAX_VALUE = 20;

void task2()
{
  std::list<int> list;
  int count = 0;

  int val = -1;
  while (std::cin >> val) {
    if (!((val >= MIN_VALUE) && (val <= MAX_VALUE))) {
      std::stringstream errStream;
      errStream << "Number must be in bounds [" << MIN_VALUE << "; " << MAX_VALUE << "].";
      throw std::invalid_argument(errStream.str());
    }
    count++;
    list.push_back(val);
    if (count > MAX_SIZE) {
      std::stringstream errStream;
      errStream << "Maximum count of numbers has reached (" << MAX_SIZE << ").";
      throw std::invalid_argument(errStream.str());
    }
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Reading from std::cin failed.");
  }

  if (list.empty()) {
    return;
  }

  auto front = list.begin();
  auto back = std::prev(list.end());
  for (int i = 0; i < count / 2; i++) {
    std::cout << *front++ << " " << *back-- << " ";
  }

  if (front == back) {
    std::cout << *front;
  }
  std::cout << '\n';
}
