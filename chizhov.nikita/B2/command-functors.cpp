#include "command-functors.hpp"

const std::string MSG_EMPTY = "<EMPTY>\n";

commands::CommandAdd::CommandAdd(const stringQueue::ElementPriority& priority, const std::string& data):
    priority_(priority),
    data_(data)
{ }

std::string commands::CommandAdd::operator ()(commands::stringQueue& queue) const
{
  queue.push(data_, priority_);
  return std::string();
}

std::string commands::CommandGet::operator ()(commands::stringQueue& queue) const
{
  if (queue.empty()) {
    return MSG_EMPTY;
  }

  std::string value;
  queue.handle_front([&value](std::string result){
    std::swap(value, result.append("\n"));
  });
  return value;
}

std::string commands::CommandAccelerate::operator ()(commands::stringQueue& queue) const
{
  if (queue.empty()) {
    return MSG_EMPTY;
  }

  queue.accelerate();
  return std::string();
}

commands::CommandError::CommandError(const std::string& error):
    error_(error)
{ }

std::string commands::CommandError::operator ()(commands::stringQueue&) const
{
  return error_;
}
