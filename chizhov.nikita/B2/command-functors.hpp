#ifndef CN_B2_CMDFNC
#define CN_B2_CMDFNC
#include <sstream>
#include "commands.hpp"
#include "priorityQueue.hpp"

namespace commands {
  class CommandAdd : public Command {
  public:
    CommandAdd(const stringQueue::ElementPriority&, const std::string&);

    std::string operator ()(stringQueue&) const override;

  private:
    stringQueue::ElementPriority priority_;
    std::string data_;
  };

  class CommandGet : public Command {
  public:
    CommandGet() = default;

    std::string operator ()(stringQueue&) const override;
  };

  class CommandAccelerate : public Command {
  public:
    CommandAccelerate() = default;

    std::string operator ()(stringQueue&) const override;
  };

  class CommandError : public Command {
  public:
    CommandError(const std::string&);

    std::string operator ()(stringQueue&) const override;

  private:
    std::string error_;
  };
}
#endif
