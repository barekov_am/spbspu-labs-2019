#ifndef CN_B2_QWP
#define CN_B2_QWP

#include <stdexcept>
#include <list>

template <typename T>
class QueueWithPriority
{
public:
  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  QueueWithPriority();

  bool operator ==(const QueueWithPriority&) const;
  bool operator !=(const QueueWithPriority&) const;

  void push(const T&, ElementPriority);
  template <typename handler_type>
  void handle_front(handler_type);
  void accelerate();
  bool empty() const
  {
    return queue_.empty();
  }

  bool size() const
  {
    return queue_.size();
  }

private:
  std::list<T> queue_;
  typename std::list<T>::iterator normal_;
  typename std::list<T>::iterator low_;
};

template <typename T>
QueueWithPriority<T>::QueueWithPriority() :
    queue_(),
    normal_(queue_.begin()),
    low_(queue_.begin())
{ }

template <typename T>
bool QueueWithPriority<T>::operator ==(const QueueWithPriority& rhs) const
{
  return ((queue_ == rhs.queue_) && (normal_ == rhs.normal_) && (low_ == rhs.low_));
}

template <typename T>
bool QueueWithPriority<T>::operator !=(const QueueWithPriority& rhs) const
{
  return !(*this == rhs);
}

template <typename T>
void QueueWithPriority<T>::push(const T& element, QueueWithPriority<T>::ElementPriority priority)
{
  switch (priority) {
    case ElementPriority::LOW: {
      queue_.insert(queue_.end(), element);
      if (low_ == queue_.end()) {
        low_ = std::prev(queue_.end());

        if (normal_ == queue_.end()) {
          normal_ = std::prev(queue_.end());
        }
      }
      break;
    }

    case ElementPriority::NORMAL: {
      queue_.insert(low_, element);
      if (normal_ == low_) {
        normal_ = std::prev(low_);
      }
      break;
    }

    case ElementPriority::HIGH: {
      queue_.insert(normal_, element);
    }
  }
}

template <typename T>
template <typename handler_type>
void QueueWithPriority<T>::handle_front(handler_type handler)
{
  if (!empty())
  {
    handler(queue_.front());

    if (normal_ == queue_.begin()) {
      normal_ = std::next(normal_);
    }

    if (low_ == queue_.begin()) {
      low_ = queue_.end();
      normal_ = queue_.end();
    }

    queue_.pop_front();
  }
  else
  {
    throw std::out_of_range("Queue is empty.");
  }
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  queue_.splice(normal_, queue_, low_, queue_.end());
  low_ = queue_.end();
}

#endif
