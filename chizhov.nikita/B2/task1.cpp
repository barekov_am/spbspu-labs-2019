#include <algorithm>
#include <iostream>
#include <sstream>

#include "commands.hpp"
#include "priorityQueue.hpp"

void task1()
{
  commands::stringQueue queue;
  std::string tmp;
  while (getline(std::cin, tmp)) {
    std::stringstream buffer(tmp);
    commands::commandPtr command;
    buffer >> command;
    std::cout << (*command)(queue);
  }

  if (!std::cin.eof() && std::cin.fail()) {
    throw std::ios_base::failure("Reading from std::cin failed.");
  }
}
