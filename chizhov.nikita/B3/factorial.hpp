#ifndef CN_B3_FACTORIAL
#define CN_B3_FACTORIAL

#include <iterator>

const int FACTORIAL_BOUND = 11;

class FactorialContainer
{
public:
  class iterator : public std::iterator<
      std::bidirectional_iterator_tag,
      unsigned long long,
      std::ptrdiff_t,
      unsigned long long,
      unsigned long long
  >
  {
  public:
    iterator(size_t, unsigned long long);
    unsigned long long operator *() const
    {
      return value_;
    }

    iterator operator ++(int);
    iterator& operator ++();
    iterator operator --(int);
    iterator& operator --();

    bool operator ==(const iterator& rhs) const
    {
      return ((value_ == rhs.value_) && (index_ == rhs.index_));
    }
    bool operator !=(const iterator& rhs) const
    {
      return !(*this == rhs);
    }

  private:
    unsigned long long value_;
    size_t index_;
    const size_t bound_ = FACTORIAL_BOUND;

    friend class FactorialContainer;
  };

  using const_iterator = iterator;
  using const_reverse_iterator = std::reverse_iterator<iterator>;

  using value_type = unsigned long long;
  using difference_type = std::ptrdiff_t;
  using size_type = size_t;

  FactorialContainer(size_t);

  iterator begin() const
  {
    return iterator(1, 1);
  }
  iterator end() const
  {
    return iterator(size_+1, endValue_);
  }
  const_iterator cbegin() const
  {
    return begin();
  }
  const_iterator cend() const
  {
    return end();
  }
  const_reverse_iterator rbegin() const
  {
    return const_reverse_iterator(begin());
  }
  const_reverse_iterator rend() const
  {
    return const_reverse_iterator(end());
  }

  size_t size() const
  {
    return size_;
  }
  size_t max_size() const
  {
    return FACTORIAL_BOUND - 1;
  }
  bool empty() const
  {
    return false;
  }

private:
  unsigned long long factorial(size_t) const;
  unsigned long long endValue_;
  size_type size_;
};

#endif
