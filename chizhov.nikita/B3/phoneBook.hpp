#ifndef CN_B3_PB
#define CN_B3_PB

#include <list>
#include <string>

class Phonebook
{
public:
  using list_type = std::list<std::pair<std::string, std::string>>;
  using value_type = list_type::value_type;
  using size_type = list_type::size_type;
  using difference_type = list_type::difference_type;

  using iterator = list_type::iterator;
  using reverse_iterator = list_type::reverse_iterator;
  using pointer = list_type::pointer;
  using reference = list_type::reference;

  using const_iterator = list_type::const_iterator;
  using const_reverse_iterator = list_type::const_reverse_iterator;
  using const_pointer = list_type::const_pointer;
  using const_reference = list_type::const_reference;

  iterator begin()
  {
    return list_.begin();
  }
  const_iterator cbegin() const
  {
    return list_.cbegin();
  }
  reverse_iterator rbegin()
  {
    return list_.rbegin();
  }
  const_reverse_iterator crbegin() const
  {
    return list_.crbegin();
  }

  iterator end()
  {
    return list_.end();
  }
  const_iterator cend() const
  {
    return list_.cend();
  }
  reverse_iterator rend()
  {
    return list_.rend();
  }
  const_reverse_iterator crend() const
  {
    return list_.crend();
  }

  bool empty() const
  {
    return list_.empty();
  }
  size_type size() const
  {
    return list_.size();
  }
  size_type max_size() const
  {
    return list_.max_size();
  }

  iterator insert(const_iterator pos, const value_type& value)
  {
    return list_.insert(pos, value);
  }
  void push_back(const value_type& value)
  {
    list_.push_back(value);
  }
  iterator erase(const_iterator pos)
  {
    return list_.erase(pos);
  }


private:
  list_type list_;
};

#endif
