#include "factorial.hpp"

#include <stdexcept>

FactorialContainer::iterator::iterator(size_t index, unsigned long long value) :
    value_(value),
    index_(index)
{ }

FactorialContainer::iterator& FactorialContainer::iterator::operator ++()
{
  if (index_ == bound_) {
    throw std::out_of_range("Index must not be greater than 11.");
  }
  value_ *= ++index_;
  return *this;
}

FactorialContainer::iterator& FactorialContainer::iterator::operator --()
{
  if (index_ == 0) {
    throw std::out_of_range("Index must not be less than 1.");
  }
  value_ /= index_--;
  return *this;
}

FactorialContainer::iterator FactorialContainer::iterator::operator++(int)
{
  iterator temp(*this);
  (*this)++;
  return --temp;
}
FactorialContainer::iterator FactorialContainer::iterator::operator--(int)
{
  iterator temp(*this);
  (*this)--;
  return temp;
}

FactorialContainer::FactorialContainer(size_t index) :
    endValue_(factorial(index+1)),
    size_(index)
{
  if ((index > FACTORIAL_BOUND) || (index == 0)) {
    throw std::out_of_range("Index must be between [1;11].");
  }
}

unsigned long long FactorialContainer::factorial(size_t index) const
{
  unsigned long long value = 1;
  for (size_t i = 2; i <= index; i++) {
    value *= i;
  }

  return value;
}
