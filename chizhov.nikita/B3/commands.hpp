#ifndef CN_B3_CMDS
#define CN_B3_CMDS

#include <algorithm>
#include <functional>
#include <map>
#include <memory>
#include <sstream>

#include "phoneBook.hpp"

namespace commands
{
  class Command;
  using bookmarks_type = std::map<std::string, Phonebook::iterator>;
  using commandPtr = std::unique_ptr<Command>;

  class Command : std::unary_function<Phonebook&, std::string> {
  public:
    virtual ~Command() = default;
    virtual std::string operator ()(Phonebook&, bookmarks_type&) const = 0;
  };

  std::istream& operator >>(std::istream&, commandPtr&);
}

#endif
