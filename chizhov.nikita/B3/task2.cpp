#include <algorithm>
#include <iostream>

#include "factorial.hpp"

void task2()
{
  FactorialContainer fc(10);

  std::copy(fc.begin(), fc.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << "\n";

  std::reverse_copy(fc.begin(), fc.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << "\n";
}
