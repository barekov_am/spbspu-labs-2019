#include <algorithm>
#include <iostream>
#include <sstream>

#include "commands.hpp"
#include "command-functors.hpp"

void task1()
{
  Phonebook pb;
  commands::bookmarks_type bms;
  bms.insert(std::make_pair("current", pb.end()));
  std::string tmp;
  while (getline(std::cin, tmp)) {
    std::stringstream buffer(tmp);
    commands::commandPtr command;
    buffer >> command;
    std::cout << (*command)(pb, bms);

  }

  if (!std::cin.eof() && std::cin.fail()) {
    throw std::ios_base::failure("Reading from std::cin failed.");
  }
}

