#ifndef CN_B3_CMDFNC
#define CN_B3_CMDFNC

#include <map>
#include <sstream>
#include "commands.hpp"
#include "phoneBook.hpp"

namespace commands {
  using bookmarks_type = std::map<std::string, Phonebook::iterator>;

  class CommandAdd : public Command
  {
  public:
    CommandAdd(const std::string&, const std::string&);
    std::string operator ()(Phonebook&, bookmarks_type&) const override;

  private:
    std::string name_;
    std::string number_;
  };
  class CommandStore : public Command
  {
  public:
    CommandStore(const std::string&, const std::string&);
    std::string operator ()(Phonebook&, bookmarks_type&) const override;

  private:
    std::string markName_;
    std::string newMarkName_;
  };
  class CommandInsert : public Command
  {
  public:
    CommandInsert(const std::string&, const std::string&, const std::string&, bool);
    std::string operator ()(Phonebook&, bookmarks_type&) const override;

  private:
    std::string markName_;
    std::string name_;
    std::string number_;
    bool before_;
  };
  class CommandDelete : public Command
  {
  public:
    CommandDelete(const std::string&);
    std::string operator ()(Phonebook&, bookmarks_type&) const override;

  private:
    std::string markName_;
  };
  class CommandShow : public Command
  {
  public:
    CommandShow(const std::string&);
    std::string operator ()(Phonebook&, bookmarks_type&) const override;

  private:
    std::string markName_;
  };
  class CommandMove : public Command
  {
  public:
    CommandMove(const std::string&, const std::string&);
    std::string operator ()(Phonebook&, bookmarks_type&) const override;

  private:
    std::string markName_;
    std::string steps_;
  };
  class CommandError : public Command {
  public:
    CommandError(const std::string&);

    std::string operator ()(Phonebook&, bookmarks_type&) const override;

  private:
    std::string error_;
  };
}

#endif
