#include "commands.hpp"

#include <memory>

#include "command-functors.hpp"

#include "manipulators.hpp"

const std::string MSG_INVALID = "<INVALID COMMAND>\n";

namespace commands
{
  using handler_type = std::function<commandPtr(std::istream&)>;
  static commandPtr handleAdd(std::istream&);
  static commandPtr handleStore(std::istream&);
  static commandPtr handleInsert(std::istream&);
  static commandPtr handleDelete(std::istream&);
  static commandPtr handleShow(std::istream&);
  static commandPtr handleMove(std::istream&);

  struct quoted_t
  {
    std::string& text;
    quoted_t(std::string& string) :
        text(string)
    { }
  };
  struct name_t
  {
    std::string& name;
    name_t(std::string& string) :
        name(string)
    { }
  };

  struct position_t
  {
    bool& position;
    position_t(bool& boolean):
        position(boolean)
    {  }
  };

  std::istream& operator >>(std::istream&, handler_type&);
  std::istream& operator >>(std::istream&, quoted_t&&);
  std::istream& operator >>(std::istream&, name_t&&);
  std::istream& operator >>(std::istream&, position_t&&);

  std::istream& operator >>(std::istream& input, commandPtr& command)
  {
    static const struct
    {
      const char* name;
      handler_type value;
    } queueHandlers[] = {
        {"add", &handleAdd},
        {"store", &handleStore},
        {"insert", &handleInsert},
        {"delete", &handleDelete},
        {"show", &handleShow},
        {"move", &handleMove}
    };

    std::istream::sentry sentry(input);
    if (sentry) {
      std::string commandName;
      input >> commandName;

      auto result = std::find_if(queueHandlers, std::end(queueHandlers),
          [&](auto& element)
          {
            return (element.name == commandName);
          }
      );

      if (result == std::end(queueHandlers)) {
        command = std::make_unique<CommandError>(CommandError(MSG_INVALID));
        return input;
      }

      command = result->value(input);
    }

    return input;
  }

  // СЛЕДУЮЩИЕ ДВА ОПЕРАТОРА Я ЧЕСТНО ВЗЯЛ У БАРЕКОВА.
  // У меня заболела голова от попыток починить все и я решил просто попробовать вставить их сюда.
  // Я изучил данный код и осознал. В случае необходимости могу переписать проще со своей точки зрения.
  std::istream& operator >>(std::istream& input, quoted_t&& string)
  {
    using traits = std::char_traits<char>;

    std::istream::sentry sentry(input);

    if (sentry) {
      auto eof = traits::eof();
      auto streambuf = input.rdbuf();
      auto c = streambuf->sgetc();

      std::string str{};

      if (!traits::eq(traits::to_char_type(c), '\"')) {
        input.setstate(std::ios_base::failbit);

        return input;
      }

      c = streambuf->snextc();

      while (!traits::eq_int_type(c, eof)
             && !traits::eq(traits::to_char_type(c), '\n')
             && !traits::eq(traits::to_char_type(c), '\"')) {
        if (traits::eq(traits::to_char_type(c), '\\')) {
          c = streambuf->snextc();
        }

        str += c;
        c = streambuf->snextc();
      }

      if (traits::eq_int_type(c, eof) || traits::eq(traits::to_char_type(c), '\n')) {
        input.setstate(std::ios_base::failbit);

        return input;
      }

      c = streambuf->snextc();

      string.text = str;
    }
    return input;
  }
  std::istream& operator >>(std::istream& input, name_t&& string)
  {
    std::istream::sentry sentry(input);

    if (sentry) {
      input >> string.name;

      auto it = std::find_if(string.name.begin(), string.name.end(),
          [&](auto character)
          {
            return (!std::isalnum(character) && character != '-');
          }
      );

      if (it != string.name.end()) {
        string.name.clear();
        input.setstate(std::ios_base::failbit);

        return input;
      }
    }

    return input;
  }
  std::istream& operator >>(std::istream& input, position_t&& position){
    std::istream::sentry sentry(input);
    std::string posName;
    if (sentry) {
      input >> posName;
    }

    if (posName.empty()) {
      input.setstate(std::ios_base::failbit);
    }

    if (posName == "before") {
      position.position = true;
      return input;
    }
    if (posName == "after") {
      position.position = false;
      return input;
    }
    input.setstate(std::ios_base::failbit);
    position.position = true;
    return input;
  }

  static commandPtr handleAdd(std::istream& input)
  {
    std::string number;
    std::string name;
    std::string junk;
    input >> blank >> number
          >> blank >> quoted_t(name)
          >> blank >> junk;

    if (name.empty() || ((input.fail() && !input.eof()) || !junk.empty())) {
      input.setstate(std::ios_base::failbit);
      return std::make_unique<CommandError>(CommandError(MSG_INVALID));
    }

    return std::make_unique<CommandAdd>(CommandAdd(number, name));
  }
  static commandPtr handleStore(std::istream& input)
  {
    std::string mark;
    std::string newMark;
    std::string junk;
    input >> blank >> name_t(mark)
          >> blank >> name_t(newMark)
          >> blank >> junk;

    if ((input.fail() && !input.eof()) || !junk.empty()) {
      input.setstate(std::ios_base::failbit);
      return std::make_unique<CommandError>(CommandError(MSG_INVALID));
    }

    return std::make_unique<CommandStore>(CommandStore(mark, newMark));
  }
  static commandPtr handleInsert(std::istream& input)
  {
    bool before;
    std::string mark;
    std::string number;
    std::string name;
    std::string junk;
    input >> blank >> position_t(before)
          >> blank >> name_t(mark)
          >> blank >> number
          >> blank >> quoted_t(name)
          >> blank >> junk;

    if (name.empty() || ((input.fail() && !input.eof()) || !junk.empty())) {
      return std::make_unique<CommandError>(CommandError(MSG_INVALID));
    }

    return std::make_unique<CommandInsert>(CommandInsert(mark, name, number, before));
  }
  static commandPtr handleDelete(std::istream& input)
  {
    std::string mark;
    std::string junk;
    input >> blank >> name_t(mark)
          >> blank >> junk;

    if ((input.fail() && !input.eof()) || !junk.empty()) {
      return std::make_unique<CommandError>(CommandError(MSG_INVALID));
    }

    return std::make_unique<CommandDelete>(CommandDelete(mark));
  }
  static commandPtr handleShow(std::istream& input)
  {
    std::string mark;
    std::string junk;
    input >> blank >> name_t(mark)
          >> blank >> junk;

    if ((input.fail() && !input.eof()) || !junk.empty()) {
      return std::make_unique<CommandError>(CommandError(MSG_INVALID));
    }

    return std::make_unique<CommandShow>(CommandShow(mark));
  }
  static commandPtr handleMove(std::istream& input)
  {
    std::string mark;
    std::string steps;
    std::string junk;
    input >> blank >> name_t(mark)
          >> blank >> steps
          >> blank >> junk;

    if ((input.fail() && !input.eof()) || !junk.empty()) {
      return std::make_unique<CommandError>(CommandError(MSG_INVALID));
    }

    return std::make_unique<CommandMove>(CommandMove(mark, steps));
  }
}
