#include "command-functors.hpp"

#include <iostream>
#include <algorithm>

const std::string MSG_EMPTY = "<EMPTY>\n";
const std::string MSG_INVALID_STEP = "<INVALID STEP>\n";
const std::string MSG_INVALID_BKMK = "<INVALID BOOKMARK>\n";


commands::CommandAdd::CommandAdd(const std::string& number, const std::string& name) :
    name_(name),
    number_(number)
{ }
commands::CommandStore::CommandStore(const std::string& markName, const std::string& newMarkName) :
    markName_(markName),
    newMarkName_(newMarkName)
{ }
commands::CommandInsert::CommandInsert(const std::string& markName, const std::string& name, const std::string& number, bool before) :
    markName_(markName),
    name_(name),
    number_(number),
    before_(before)
{ }
commands::CommandDelete::CommandDelete(const std::string& markName) :
    markName_(markName)
{ }
commands::CommandShow::CommandShow(const std::string& markName) :
    markName_(markName)
{ }
commands::CommandMove::CommandMove(const std::string& markName, const std::string& steps) :
    markName_(markName),
    steps_(steps)
{ }
commands::CommandError::CommandError(const std::string& error) :
    error_(error)
{ }

std::string commands::CommandAdd::operator ()(Phonebook& pb, bookmarks_type& bms) const
{
  pb.push_back(std::make_pair(name_, number_));
  if (std::next(pb.begin()) == pb.end()) {
    bms["current"] = pb.begin();
  }

  return std::string();
}
std::string commands::CommandStore::operator ()(Phonebook&, bookmarks_type& bms) const
{
  auto bm = bms.find(markName_);
  if (bm == bms.end()) {
    return MSG_INVALID_BKMK;
  }

  bms.emplace(std::make_pair(newMarkName_, bm->second));
  return std::string();
}
std::string commands::CommandInsert::operator ()(Phonebook& pb, bookmarks_type& bms) const
{
  auto bm = bms.find(markName_);
  if (bm == bms.end()) {
    return MSG_INVALID_BKMK;
  }

  if (bm->second == pb.end()) {
    pb.push_back(std::make_pair(name_, number_));
    if (std::next(pb.begin()) == pb.end()) {
      bms["current"] = pb.begin();
    }

    return std::string();
  }

  auto position = before_ ? bm->second : std::next(bm->second);

  pb.insert(position, std::make_pair(name_, number_));
  return std::string();
}
std::string commands::CommandDelete::operator ()(Phonebook& pb, bookmarks_type& bms) const
{
  auto bm = bms.find(markName_);
  if (bm == bms.end()) {
    return MSG_INVALID_BKMK;
  }

  auto toDelete = bm->second;
  for (auto i = bms.begin(); i != bms.end(); i++)
  {
    if ((i != bm) && (i->second == toDelete)) {
      i->second = std::next(toDelete);
      if ((i->second == pb.end() && (pb.begin() != pb.end())))
      {
        i->second = std::prev(i->second);
      }
    }
  }
  bm->second = std::next(bm->second);
  pb.erase(toDelete);
  if (bm->second == pb.end())
  {
    bm->second = std::prev(pb.end());
  }


  return std::string();
}
std::string commands::CommandShow::operator ()(Phonebook& pb, bookmarks_type& bms) const
{
  auto bm = bms.find(markName_);
  if (bm == bms.end()) {
    return MSG_INVALID_BKMK;
  }
  if (bm->second == pb.end()) {
    return MSG_EMPTY;
  }
  std::stringstream res;
  res << bm->second->second << " " << bm->second->first << '\n';
  return res.str();
}
std::string commands::CommandMove::operator ()(Phonebook& pb, bookmarks_type& bms) const
{
  auto bm = bms.find(markName_);
  if (bm == bms.end()) {
    return MSG_INVALID_BKMK;
  }

  if (steps_ == "first") {
    bm->second = pb.begin();
    return std::string();
  }
  if (steps_ == "last") {
    bm->second = std::prev(pb.end());
    return std::string();
  }

  char* ptr = nullptr;
  int steps = std::strtol(steps_.c_str(), &ptr, 10);
  if (*ptr) {
    return MSG_INVALID_STEP;
  }
  if (abs(steps) > (steps > 0 ? std::distance(bm->second, pb.end()) + 1 : std::distance(pb.begin(), bm->second) + 1)) {
    return MSG_INVALID_STEP;
  }
  std::advance(bm->second, steps);
  return std::string();
}
std::string commands::CommandError::operator ()(Phonebook&, bookmarks_type&) const
{
  return error_;
}
