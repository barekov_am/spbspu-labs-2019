#include <iostream>
#include <cstring>

const size_t BASE_LINE_WIDTH = 40;
const size_t MAX_WORD_LENGTH = 20;
const size_t DASH_LENGTH = 3;
const size_t SPACE_LENGTH = 1;
const size_t COMMA_LENGTH = 1;
const size_t MINIMUM_LINE_WIDTH = MAX_WORD_LENGTH + DASH_LENGTH + SPACE_LENGTH + COMMA_LENGTH;

void task(size_t lineWidth, std::istream &istream = std::cin, std::ostream &ostream = std::cout);

int main(int argc, char *argv[])
{
  try
  {
    size_t lineWidth = 0;
    if (argc == 1)
    {
      lineWidth = BASE_LINE_WIDTH;
    }
    else if (argc == 3)
    {
      if (std::strcmp(argv[1], "--line-width") != 0)
      {
        std::cerr << "Invalid parameters.\n";
        return 1;
      }
      if (static_cast<size_t>(std::stoi(argv[2])) < MINIMUM_LINE_WIDTH)
      {
        std::cerr << "Invalid line width.\n";
        return 1;
      }
      lineWidth = static_cast<size_t>(std::stoi(argv[2]));
    }
    else
    {
      std::cerr << "Invalid number of parameters.\n";
      return 1;
    }

    task(lineWidth);
  }
  catch (std::invalid_argument &exception)
  {
    std::cerr << exception.what() << "\n";
    return 1;
  }
  catch (std::exception &exception)
  {
    std::cerr << exception.what() << "\n";
    return 2;
  }

  return 0;
}
