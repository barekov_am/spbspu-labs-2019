#include "output-iterator.hpp"

OutputIterator::OutputIterator(std::ostream &output, size_t lineWidth) :
  state_(std::make_shared<state_t>(output, lineWidth))
{}

OutputIterator& OutputIterator::operator++()
{
  return *this;
}

OutputIterator& OutputIterator::operator++(int)
{
  return *this;
}

OutputIterator& OutputIterator::operator*()
{
  return *this;
}

OutputIterator& OutputIterator::operator=(const token_t &token)
{
  writeTokenToStream(token);
  return *this;
}

bool OutputIterator::operator==(const OutputIterator &other)
{
  return state_ == other.state_;
}

bool OutputIterator::operator!=(const OutputIterator &other)
{
  return state_ != other.state_;
}

void OutputIterator::writeTokenToStream(const token_t &token)
{
  size_t newLength = state_->data.size() + token.tokenData.size();

  if ((token.tokenType == token_t::TokenType::WORD)
      || (token.tokenType == token_t::TokenType::NUMBER))
  {
    if (!state_->data.empty() && !state_->ellipsisAfterPunct)
    {
      newLength++;
    }

    if (newLength > state_->lineWidth)
    {
      state_->ostream << state_->data << "\n";
      state_->data.clear();
    }

    if (!state_->data.empty() && !state_->ellipsisAfterPunct)
    {
      state_->data.append(" ");
    }

    state_->data.append(token.tokenData);
    state_->lastTokens[0] = state_->lastTokens[1];
    state_->lastTokens[1] = token;
  }
  else if (token.tokenType == token_t::TokenType::DASH)
  {
    if (!state_->data.empty())
    {
      newLength++;
    }

    if (newLength > state_->lineWidth)
    {
      transferLastToken(token.tokenType);
    }

    state_->data.append(" ");
    state_->data.append(token.tokenData);
    state_->lastTokens[0] = state_->lastTokens[1];
    state_->lastTokens[1] = token;
  }
  else if (token.tokenType == token_t::TokenType::MARK)
  {
    if (newLength > state_->lineWidth)
    {
      transferLastToken(token.tokenType);
    }

    state_->data.append(token.tokenData);
    state_->lastTokens[0] = state_->lastTokens[1];
    state_->lastTokens[1] = token;
  }
  else if (token.tokenType == token_t::TokenType::ELLIPSIS)
  {
    if ((state_->lastTokens[1].tokenType == token_t::TokenType::MARK))
    {
      if (state_->lastTokens[1].tokenData == ",")
      {
        state_->data.erase(state_->data.size() - 1, 1);
      }
      else
      {
        state_->data.append(" ");
        newLength++;
        state_->ellipsisAfterPunct = true;
      }
    }
    else if (state_->data.empty())
    {
      state_->ellipsisAfterPunct = true;
    }
    else
    {
      state_->ellipsisAfterPunct = false;
    }

    if (newLength > state_->lineWidth)
    {
      transferLastToken(token.tokenType);
    }

    state_->data.append(token.tokenData);
    state_->lastTokens[0] = state_->lastTokens[1];
    state_->lastTokens[1] = token;
  }
  else
  {
    state_->ostream.setstate(std::ios_base::failbit);
  }
}

OutputIterator::state_t::state_t(std::ostream &ostream, size_t lineWidth) :
  data(),
  ostream(ostream),
  lineWidth(lineWidth),
  lastTokens(),
  ellipsisAfterPunct(false)
{}

OutputIterator::state_t::~state_t()
{
  ostream << data << "\n";
}

void OutputIterator::transferLastToken(token_t::TokenType tokenType)
{
  size_t begin = 0;
  size_t length = 0;

  if (tokenType == token_t::TokenType::DASH)
  {
    bool lastIsMark = state_->lastTokens[1].tokenType == token_t::TokenType::MARK;
    int index = static_cast<int>(!lastIsMark);

    begin = state_->data.size() - state_->lastTokens[index].tokenData.size() - static_cast<int>(lastIsMark) - 1;
    length = state_->lastTokens[index].tokenData.size() + static_cast<int>(lastIsMark) + 1;
  }
  else
  {
    begin = state_->data.size() - state_->lastTokens[1].tokenData.size() - 1;
    length = state_->lastTokens[1].tokenData.size() + 1;
  }

  std::string last = state_->data.substr(begin, length).erase(0, 1);
  state_->data.erase(begin, length);
  state_->ostream << state_->data << "\n";
  state_->data = last;
}
