#ifndef B8_VALIDATOR_HPP
#define B8_VALIDATOR_HPP

#include "token.hpp"

class TokenValidator
{
public:
  static const size_t TOKEN_MAX_LENGTH = 20;
  static const size_t DASH_AND_ELLIPSIS_LENGTH = 3;

  TokenValidator() = default;

  token_t operator()(const token_t &token);

private:
  token_t lastToken_;
};

#endif
