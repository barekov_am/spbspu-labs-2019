#include <iostream>
#include <stdexcept>
#include <algorithm>

#include "token-iterator.hpp"
#include "output-iterator.hpp"
#include "token-validator.hpp"

void task(size_t lineWidth, std::istream &istream = std::cin, std::ostream &ostream = std::cout)
{
  std::transform(TokenIterator(istream), TokenIterator(), OutputIterator(ostream, lineWidth), TokenValidator());

  if ((!istream.good() && !istream.eof()) || !ostream.good())
  {
    throw std::runtime_error("Read failed");
  }
}
