#include "token-iterator.hpp"
#include <locale>

TokenIterator::TokenIterator() :
  state_(nullptr)
{}

TokenIterator::TokenIterator(std::istream &istream) :
  state_(std::make_shared<state_t>(istream))
{
  ++(*this);
}

TokenIterator& TokenIterator::operator++()
{
  if (!state_->istream)
  {
    state_ = nullptr;
    return *this;
  }

  state_->token.tokenData.clear();
  readTokenFromStream();
  if (state_->token.tokenType == token_t::TokenType::INVALID)
  {
    state_->istream.setstate(std::ios_base::failbit);
  }
  if (state_->token.tokenData.empty())
  {
    state_ = nullptr;
  }
  return *this;
}

TokenIterator TokenIterator::operator++(int)
{
  TokenIterator tempTokenIterator(*this);
  ++(*this);
  return tempTokenIterator;
}

bool TokenIterator::operator==(const TokenIterator &other)
{
  return state_ == other.state_;
}

bool TokenIterator::operator!=(const TokenIterator &other)
{
  return state_ != other.state_;
}

const token_t* TokenIterator::operator->() const
{
  return &(state_->token);
}

const token_t& TokenIterator::operator*() const
{
  return state_->token;
}

void TokenIterator::readTokenFromStream()
{
  std::istream::sentry sentry(state_->istream);
  if (!sentry)
  {
    return;
  }

  int c = state_->istream.get();
  std::string tokenData;
  bool isRead = true;

  char decimalPoint = std::use_facet<std::numpunct<char>>(state_->istream.getloc()).decimal_point();

  while(std::char_traits<char>::not_eof(c) && isRead)
  {
    if (std::isspace(c))
    {
      state_->token.tokenData = tokenData;
      return;
    }
    else if (std::isalpha(c))
    {
      if (tokenData.empty())
      {
        tokenData.push_back(static_cast<char>(c));
        state_->token.tokenType = token_t::TokenType::WORD;
      }
      else
      {
        switch (state_->token.tokenType)
        {
        case token_t::TokenType::MARK:
        case token_t::TokenType::DASH:
        case token_t::TokenType::ELLIPSIS:
          state_->istream.unget();
          isRead = false;
          break;

        case token_t::TokenType::WORD:
          tokenData.push_back(static_cast<char>(c));
          break;

        default:
          state_->token.tokenType = token_t::TokenType::INVALID;
        }
      }
    }
    else if (std::isdigit(c))
    {
      if (tokenData.empty())
      {
        tokenData.push_back(static_cast<char>(c));
        state_->token.tokenType = token_t::TokenType::NUMBER;
      }
      else
      {
        switch (state_->token.tokenType)
        {
        case token_t::TokenType::MARK:
        case token_t::TokenType::DASH:
        case token_t::TokenType::ELLIPSIS:
          state_->istream.unget();
          isRead = false;
          break;

        case token_t::TokenType::NUMBER:
          tokenData.push_back(static_cast<char>(c));
          break;

        default:
          state_->token.tokenType = token_t::TokenType::INVALID;
        }
      }
    }
    else if (c == '+')
    {
      if (tokenData.empty())
      {
        tokenData.push_back(static_cast<char>(c));
        c = state_->istream.get();
        if (std::isdigit(c))
        {
          tokenData.push_back(static_cast<char>(c));
          state_->token.tokenType = token_t::TokenType::NUMBER;
        }
        else
        {
          state_->istream.unget();
          state_->token.tokenType = token_t::TokenType::MARK;
        }
      }
      else
      {
        state_->istream.unget();
        isRead = false;
      }
    }
    else if (c == '-')
    {
      if (tokenData.empty())
      {
        tokenData.push_back(static_cast<char>(c));
        c = state_->istream.get();
        if (c == '-')
        {
          tokenData.push_back(static_cast<char>(c));
          state_->token.tokenType = token_t::TokenType::DASH;
        }
        else if (std::isdigit(c))
        {
          tokenData.push_back(static_cast<char>(c));
          state_->token.tokenType = token_t::TokenType::NUMBER;
        }
        else
        {
          if (!state_->istream.eof())
          {
            state_->istream.unget();
          }

          state_->token.tokenType = token_t::TokenType::MARK;
        }
      }
      else
      {
        switch (state_->token.tokenType)
        {
        case token_t::TokenType::WORD:
        {
          char tmp = c;
          c = state_->istream.get();
          if (c == '-')
          {
            state_->istream.unget();
            state_->istream.putback(tmp);
            isRead = false;
          }
          else
          {
            tokenData.push_back(tmp);
            if (std::isalpha(c))
            {
              tokenData.push_back(c);
            }
            else if (!state_->istream.eof())
            {
              state_->istream.unget();
            }
          }
          break;
        }

        case token_t::TokenType::DASH:
          tokenData.push_back(c);
          break;

        default:
          state_->istream.unget();
          isRead = false;
        }
      }
    }
    else if (c == '.')
    {
      if (tokenData.empty())
      {
        tokenData.push_back(static_cast<char>(c));
        c = state_->istream.get();
        if (c == '.')
        {
          tokenData.push_back(static_cast<char>(c));
          state_->token.tokenType = token_t::TokenType::ELLIPSIS;
        }
        else
        {
          if (!state_->istream.eof())
          {
            state_->istream.unget();
          }

          state_->token.tokenType = token_t::TokenType::MARK;
        }
      }
      else
      {
        switch (state_->token.tokenType)
        {
        case token_t::TokenType::NUMBER:
          if (c == decimalPoint)
          {
            char tmp = c;
            c = state_->istream.get();
            if (!std::isdigit(c))
            {
              state_->istream.unget();
              state_->istream.putback(tmp);
              isRead = false;
            }
            else
            {
              tokenData.push_back(tmp);
              tokenData.push_back(c);
            }
          }
          else
          {
            state_->istream.unget();
            isRead = false;
          }
          break;

        case token_t::TokenType::ELLIPSIS:
          tokenData.push_back(c);
          break;

        default:
          state_->istream.unget();
          isRead = false;
        }
      }
    }
    else if (c == decimalPoint)
    {
      if (tokenData.empty())
      {
        tokenData.push_back(static_cast<char>(c));
        state_->token.tokenType = token_t::TokenType::MARK;
        isRead = false;
      }
      else if (state_->token.tokenType == token_t::TokenType::NUMBER)
      {
        char tmp = c;
        c = state_->istream.get();
        if (!std::isdigit(c))
        {
          state_->istream.unget();
          state_->istream.putback(tmp);
          isRead = false;
        }
        else
        {
          tokenData.push_back(tmp);
          tokenData.push_back(c);
        }
      }
      else
      {
        state_->istream.unget();
        isRead = false;
      }
    }
    else if (std::ispunct(c))
    {
      if (tokenData.empty())
      {
        tokenData.push_back(static_cast<char>(c));
        state_->token.tokenType = token_t::TokenType::MARK;
      }
      else
      {
        state_->istream.unget();
      }
      isRead = false;
    }

    if (isRead)
    {
      c = state_->istream.get();
    }
  }

  state_->token.tokenData = tokenData;
}

TokenIterator::state_t::state_t(std::istream &istream) :
  istream(istream),
  token()
{}
