#include "token-validator.hpp"

#include <stdexcept>

token_t TokenValidator::operator()(const token_t &token)
{
  if ((token.tokenData.size() > TOKEN_MAX_LENGTH)
      || (((token.tokenType == token_t::TokenType::DASH)
        || (token.tokenType == token_t::TokenType::ELLIPSIS))
          && (token.tokenData.size() != DASH_AND_ELLIPSIS_LENGTH)))
  {
    throw std::runtime_error("Too long token.");
  }

  if (lastToken_.tokenData.empty()
      && ((token.tokenType == token_t::TokenType::MARK)
        || (token.tokenType == token_t::TokenType::DASH)))
  {
    throw std::runtime_error("Punctuation or dash at the beginning of the text.");
  }

  if ((lastToken_.tokenType == token_t::TokenType::MARK)
      || (lastToken_.tokenType == token_t::TokenType::DASH)
        || (lastToken_.tokenType == token_t::TokenType::ELLIPSIS))
  {
    if ((token.tokenType == token_t::TokenType::MARK)
        || ((token.tokenType == token_t::TokenType::DASH) && (lastToken_.tokenData != ",")))
    {
      throw std::runtime_error("Two punctuations in a row.");
    }
  }
  
  lastToken_ = token;
  return token;
}
