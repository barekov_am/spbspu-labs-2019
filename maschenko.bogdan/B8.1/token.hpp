#ifndef B8_TOKEN_HPP
#define B8_TOKEN_HPP

#include <string>

struct token_t
{
  enum class TokenType
  {
    WORD,
    NUMBER,
    MARK,
    DASH,
    ELLIPSIS,
    INVALID
  };

  TokenType tokenType;
  std::string tokenData;
};

#endif
