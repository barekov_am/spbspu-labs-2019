#ifndef B8_TOKENS_HPP
#define B8_TOKENS_HPP

#include <iosfwd>
#include <memory>
#include <iterator>

#include "token.hpp"

class TokenIterator : public std::iterator<std::input_iterator_tag, token_t>
{
public:
  TokenIterator();
  TokenIterator(std::istream &istream);

  TokenIterator &operator++();
  TokenIterator operator++(int);
  bool operator==(const TokenIterator &other);
  bool operator!=(const TokenIterator &other);
  const token_t *operator->() const;
  const token_t &operator*() const;

  void readTokenFromStream();

private:
  struct state_t
  {
    state_t(std::istream &istream);

    std::istream &istream;
    token_t token;
  };

  std::shared_ptr<state_t> state_;
};

#endif
