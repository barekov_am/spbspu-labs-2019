#ifndef B8_OUTPUT_HPP
#define B8_OUTPUT_HPP

#include <iosfwd>
#include <memory>
#include <iterator>

#include "token.hpp"

class OutputIterator : public std::iterator<std::output_iterator_tag, void, void, void, void>
{
public:
  OutputIterator(std::ostream &out, size_t lineWidth);
  OutputIterator &operator++();
  OutputIterator &operator++(int);
  OutputIterator &operator*();
  OutputIterator &operator=(const token_t &token);
  bool operator==(const OutputIterator &other);
  bool operator!=(const OutputIterator &other);

  void writeTokenToStream(const token_t &token);

private:
  struct state_t
  {
    state_t(std::ostream &ostream, size_t lineWidth);
    ~state_t();

    std::string data;
    std::ostream &ostream;
    size_t lineWidth;
    std::array<token_t, 2> lastTokens;
    bool ellipsisAfterPunct;
  };

  std::shared_ptr<state_t> state_;

  void transferLastToken(token_t::TokenType);
};

#endif
