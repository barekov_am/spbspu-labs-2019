#ifndef STATISTICS_HPP
#define STATISTICS_HPP

#include <iostream>

struct GetStatistics
{
public:
  GetStatistics();

  void operator()(int number);
  void print() const;

private:
  int max_;
  int min_;
  double mean_;
  unsigned int positive_;
  unsigned int negative_;
  long long int oddsum_;
  long long int evensum_;
  bool firstEqualLast_;
  int first_;
  int last_;
  unsigned int counter_;
};

#endif // STATISTICS_HPP
