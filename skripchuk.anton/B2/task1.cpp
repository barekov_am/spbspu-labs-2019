#include <algorithm>
#include <sstream>
#include <vector>
#include <iostream>
#include "QueueWithPriority.hpp"

void task1()
{
  QueueWithPriority<std::string> queue;
  std::vector<std::string> commands = { "add", "get", "accelerate" };
  std::vector<std::string> prioritets = { "low", "normal", "high" };
  std::string line;

  while (std::getline(std::cin, line))
  {
    std::stringstream buffer(line);
    buffer >> line;

    switch (std::distance(commands.begin(), std::find(commands.begin(), commands.end(), line)))
    {
    case 0:
    {
      std::string priority;
      buffer >> priority;
      auto findResult = std::find(prioritets.begin(), prioritets.end(), priority);
      if (findResult != prioritets.end())
      {
        std::string data;
        std::getline(buffer >> std::ws, data);
        if (data.empty())
        {
          std::cout << "<INVALID COMMAND>" << "\n";
        }

        queue.putElementToQueue(data, static_cast<ElementPriority>(std::distance(prioritets.begin(), findResult)));
      }
      else
      {
        std::cout << "<INVALID COMMAND>" << "\n";
      }
      break;
    }
    case 1:
      queue.empty() ? std::cout << "<EMPTY>" << "\n" : std::cout << queue.getElementFromQueue() << "\n";
      break;
    case 2:
      queue.accelerate();
      break;
    default:
      std::cout << "<INVALID COMMAND>" << "\n";
    };
  }
}
