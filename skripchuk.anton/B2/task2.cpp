#include <list>
#include <iostream>

void task2()
{
  std::list<int> list;
  unsigned int elem = 0;
  const unsigned int minValue = 0;
  unsigned int maxValue = 20;
  unsigned int maxSize = 20;
  
  while (std::cin >> elem)
  {
    if (elem > minValue && elem <= maxValue)
    {
      list.push_back(elem);
    }
    else
    {
      throw std::invalid_argument("Input value is not in the range [1-20]");
    }
  }
  if (list.size() > maxSize)
  {
    throw std::invalid_argument("List size is over 20");
  }
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Input failed");
  }
  auto i = list.begin();
  auto j = list.end();
  while (i != j)
  {
    std::cout << *(i++) << " ";
    if (i == j)
    {
      break;
    }
    std::cout << *(--j) << " ";
  }
  std::cout << '\n';
}
