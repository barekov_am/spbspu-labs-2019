#ifndef B2_QUEUE_INTERFACE_HPP
#define B2_QUEUE_INTERFACE_HPP

#include <list>
#include <deque>

enum ElementPriority
{
  LOW,
  NORMAL,
  HIGH
};

template <typename T>
 class QueueWithPriority
 {
 public:

   bool operator ==(const QueueWithPriority &other) const;
   bool operator !=(const QueueWithPriority &other) const;

   void putElementToQueue(const T &element, ElementPriority priority);

   T getElementFromQueue();

   void accelerate();

   bool empty();

   void print();

 private:
   std::deque<T> q;
   unsigned int highCounter = 0;
   unsigned int lowCounter = 0;
   unsigned int getCounter = 0;
 };
#endif //B2_QUEUE_INTERFACE_HPP
