#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>
#include "task.hpp"

const int MIN_KEY = -5;
const int MAX_KEY = 5;
const int COMMAS_COUNT = 2;

bool checkRange(int num, int min, int max)
{
  return num >= min && num <= max;
}

DataStruct parseInput(const std::string& string)
{
  std::stringstream input(string);

  int commasNumber = 0;
  for (size_t i = 0; i < string.length(); i++)
  {
    if (string[i] == ',')
    {
      commasNumber++;
    }
  }

  if (commasNumber != COMMAS_COUNT)
  {
    throw std::invalid_argument("Invalid input");
  }

  int key1, key2;
  char buffer;
  input >> key1 >> std::ws >> buffer >> key2 >> std::ws >> buffer;
  
  std::string str;
  std::getline(input >> std::ws, str);

  if (input.fail())
  {
    throw std::ios::failure("Reading failed");
  }

  if (!checkRange(key1, MIN_KEY, MAX_KEY) || !checkRange(key2, MIN_KEY, MAX_KEY))
  {
    throw std::invalid_argument("Key value is out of range");
  }

  return DataStruct{key1, key2, str};
}

void readVector(std::vector<DataStruct>& vector)
{
  std::string line;
  while (std::getline(std::cin, line))
  {
    vector.push_back(parseInput(line));
  }
}

void printVector(std::vector<DataStruct>& vector)
{
  for (auto elem : vector)
  {
    std::cout << elem.key1 << ", " << elem.key2 << ", " << elem.str << '\n';
  }
}

bool comp(const DataStruct& first, const DataStruct& second)
{
  if (first.key1 == second.key1)
  {
    if (first.key2 == second.key2)
    {
      if (first.str.size() == second.str.size())
      {
        return first.str < second.str;
      }
      return first.str.size() < second.str.size();
    }
    return first.key2 < second.key2;
  }
  return first.key1 < second.key1;
}

void task()
{
  std::vector<DataStruct> vector;
  readVector(vector);
  std::sort(vector.begin(), vector.end(), comp);
  printVector(vector);
}
