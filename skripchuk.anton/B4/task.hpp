#ifndef TASK_HPP
#define TASK_HPP

#include <string>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

void task();

#endif
