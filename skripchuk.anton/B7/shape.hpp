#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <istream>
#include <memory>

struct Point
{
  long long x;
  long long y;
};

class Shape
{
public:
  using shape_ptr = std::shared_ptr<Shape>;
  Shape(Point center_);

  virtual ~Shape() = default;
  bool isMoreLeft(const Shape & shape) const;
  bool isUpper(const Shape & shape) const;
  virtual void draw() const = 0;

protected:
 Point center_;
};

#endif
