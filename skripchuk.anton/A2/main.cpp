#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void printArea(const skripchuk::Shape &actualShape)
{
  std::cout << "area = " << actualShape.getArea() << std::endl;
  std::cout << std::endl;
}

void printFrameRect(const skripchuk::Shape &actualShape)
{
  skripchuk::rectangle_t Rect_ = actualShape.getFrameRect();
  std::cout << "frame rectangle:" << std::endl;
  std::cout << "width = " << Rect_.width;
  std::cout << "  height = " << Rect_.height;
  skripchuk::point_t point = Rect_.pos;
  std::cout << "  x = " << point.x;
  std::cout << "  y = " << point.y;
  std::cout << std::endl << std::endl;
}

void shapeTest(skripchuk::Shape &actualShape1, skripchuk::Shape &actualShape2)
{
  actualShape1.printParameters();

  std::cout << "creating same shape (2nd constructor)" << std::endl;
  actualShape2.printParameters();

  printArea(actualShape2);
  printFrameRect(actualShape2);
  std::cout << "moving shape on dx = 1, dy = -2" << std::endl;
  actualShape2.move(1, -2);
  actualShape2.printParameters();

  std::cout << "moving shape to x = 4, y = 5" << std::endl;
  actualShape2.move({4, 5});
  actualShape2.printParameters();

  std::cout << "scaling shape *1.5" << std::endl;
  actualShape2.scale(1.5);
  actualShape2.printParameters();
}

void shapeTest(skripchuk::Shape &actualShape)
{
  actualShape.printParameters();

  printArea(actualShape);
  printFrameRect(actualShape);
  std::cout << "moving shape on dx = 1, dy = -2" << std::endl;
  actualShape.move(1, -2);
  actualShape.printParameters();

  std::cout << "moving shape to x = 4, y = 5" << std::endl;
  actualShape.move({4, 5});
  actualShape.printParameters();

  std::cout << "scaling shape *1.5" << std::endl;
  actualShape.scale(1.5);
  actualShape.printParameters();
}

int main()
{
  std::cout << "----------Rectangles----------" << std::endl;
  std::cout << "creating shape: x = -1, y = 3, w = 2, h = 4" << std::endl;
  skripchuk::Rectangle rectangle1({-1, 3}, 2, 4);
  skripchuk::Rectangle rectangle2(-1, 3, 2, 4);

  shapeTest(rectangle1, rectangle2);

  std::cout << "-----------Circles------------" << std::endl;
  std::cout << "creating shape: x = -1, y = 3, r = 4" << std::endl;
  skripchuk::Circle circle1({-1, 3}, 4);
  skripchuk::Circle circle2(-1, 3, 4);

  shapeTest(circle1, circle2);

  std::cout << "----------Triangles-----------" << std::endl;
  std::cout << "creating shape: (1; 3), (-4; 2), (-1; -6)" << std::endl;
  skripchuk::Triangle triangle1({1, 3}, {-4, 2}, {-1, -6});
  skripchuk::Triangle triangle2(1, 3, -4, 2, -1, -6);

  shapeTest(triangle1, triangle2);

  std::cout << "-----------Polygons-----------" << std::endl;
  std::cout << "creating shape: (1; 3), (-4; 2), (-1; -6)" << std::endl;
  skripchuk::point_t pointArray[] {{4, 5}, {6, 4}, {7, -1}, {5, -3}, {2, 1}};
  skripchuk::Polygon polygon1(5, pointArray);

  shapeTest(polygon1);

  return 0;
}
