#include <stdexcept>
#include <vector>
#include <random>
#include <ctime>
#include "sort.hpp"
#include "strategy.hpp"

void fillRandom(double* array, size_t size)
{
  std::mt19937 randgen(time(0));
  std::uniform_real_distribution<double> distribution(-1.0, 1.0);

  for (size_t i = 0; i < size; i++)
  {
    array[i] = distribution(randgen);
  }
}

void task4(size_t size, const char* direction)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Invalid size");
  }

  std::vector<double> vector;
  vector.resize(size);
  fillRandom(&vector[0], size);

  print(vector);
  sort<StrategyAt>(vector, direction);
  print(vector);
}

