#include <iostream>
#include <stdexcept>
#include <forward_list>
#include <vector>
#include "strategy.hpp"
#include "sort.hpp"

void task1(const char* direction)
{
  std::vector<int> vectorBraces;

  int tmp = 0;
  while (std::cin >> tmp)
  {
    vectorBraces.push_back(tmp);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Reading failure");
  }

  std::vector<int> vectorAt = vectorBraces;
  std::forward_list<int> list(vectorBraces.begin(), vectorBraces.end());

  sort<StrategyBraces>(vectorBraces, direction);
  sort<StrategyAt>(vectorAt, direction);
  sort<StrategyIterator>(list, direction);

  print(vectorBraces);
  print(vectorAt);
  print(list);
}

