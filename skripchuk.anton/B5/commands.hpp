#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <list>
#include <string>
#include "shape.hpp"

using ShapesContainer = std::list<Shape>;

const int TRIANGLE_VERTICES = 3;
const int RECTANGLE_VERTICES = 4;
const int PENTAGON_VERTICES = 5;

void readShapes(ShapesContainer& shapesContainer);

int getVerticesCount(const ShapesContainer& shapesContainer);
int getTriangleCount(const ShapesContainer& shapesContainer);
int getRectangleCount(const ShapesContainer& shapesContainer);
int getSquareCount(const ShapesContainer& shapesContainer);

int sqrDistance(const Point& rhs, const Point& lhs);
bool cmpX(const Point& rhs, const Point& lhs);
bool cmpY(const Point& rhs, const Point& lhs);

Shape sortRectangle(const Shape& shape);
bool isRectangle(const Shape& shape);
bool isSquare(const Shape& shape);

void deletePentagons(ShapesContainer& shapesContainer);
Shape getFirstPoints(const ShapesContainer& shapesContainer);
void sortShapesContainer(ShapesContainer& shapesContainer);

#endif
