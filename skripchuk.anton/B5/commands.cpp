#include <sstream>
#include <iostream>
#include <algorithm>
#include "commands.hpp"

void readShapes(ShapesContainer& shapesContainer)
{
  std::string line;
  std::string buffer;

  while (std::getline(std::cin >> std::ws, line))
  {
    if (line.empty())
    {
      continue;
    }

    std::stringstream input(line);
    int vertNum = 0;
    input >> vertNum;

    if (vertNum < TRIANGLE_VERTICES)
    {
      throw std::invalid_argument("Invalid number of vertices");
    }

    Shape shape;
    for (int i = 0; i < vertNum; i++)
    {
      Point tmp_point;

      std::getline(input, buffer, '(');
      input >> tmp_point.x;

      std::getline(input, buffer, ';');
      input >> tmp_point.y;

      std::getline(input, buffer, ')');
      shape.push_back(tmp_point);
    }

    if (input.fail())
    {
      throw std::invalid_argument("Invalid input");
    }

    shapesContainer.push_back(shape);
  }
}

int getVerticesCount(const ShapesContainer& shapesContainer)
{
  int counter = 0;
  for (auto shape : shapesContainer)
  {
    counter += shape.size();
  }

  return counter;
}

int sqrDistance(const Point& rhs, const Point& lhs)
{
  return ((rhs.x - lhs.x) * (rhs.x - lhs.x) + (rhs.y - lhs.y) * (rhs.y - lhs.y));
}

bool cmpX(const Point& rhs, const Point& lhs)
{
  return (rhs.x < lhs.x);
}

bool cmpY(const Point& rhs, const Point& lhs)
{
  return (rhs.y < lhs.y);
}

Shape sortRectangle(const Shape& shape)
{
  Shape sortedByX = shape;
  std::sort(sortedByX.begin(), sortedByX.end(), cmpX);

  Point A = std::min(sortedByX[0], sortedByX[1], cmpY);
  Point B = std::max(sortedByX[0], sortedByX[1], cmpY);
  Point C = std::max(sortedByX[2], sortedByX[3], cmpY);
  Point D = std::min(sortedByX[2], sortedByX[3], cmpY);
  
  Shape sortedRectangle;
  sortedRectangle.push_back(A);
  sortedRectangle.push_back(B);
  sortedRectangle.push_back(C);
  sortedRectangle.push_back(D);

  return sortedRectangle;
}

bool isRectangle(const Shape& shape)
{
  if (shape.size() != RECTANGLE_VERTICES)
  {
    return false;
  }
  
  Shape rect = sortRectangle(shape);
  int diagonal1 = sqrDistance(rect[0], rect[2]);
  int diagonal2 = sqrDistance(rect[1], rect[3]);

  return (diagonal1 == diagonal2);
}

bool isSquare(const Shape& shape)
{
  if (!isRectangle(shape))
  {
    return false;
  }

  Shape rect = sortRectangle(shape);
  
  int side1 = sqrDistance(rect[0], rect[1]);
  int side2 = sqrDistance(rect[1], rect[2]);
  int side3 = sqrDistance(rect[2], rect[3]);
  int side4 = sqrDistance(rect[3], rect[0]);

  return ((side1 == side2) && (side2 == side3) && (side3 == side4) && (side4 == side1));
}

int getTriangleCount(const ShapesContainer& shapesContainer)
{
  int counter = 0;
  for (auto shape : shapesContainer)
  {
    if (shape.size() == TRIANGLE_VERTICES)
    {
      counter++;
    }
  }
  return counter;
}

int getRectangleCount(const ShapesContainer& shapesContainer)
{
  int counter = 0;
  for (auto shape : shapesContainer)
  {
    if (isRectangle(shape))
    {
      counter++;
    }
  }
  return counter;
}

int getSquareCount(const ShapesContainer& shapesContainer)
{
  int counter = 0;
  for (auto shape : shapesContainer)
  {
    if (isSquare(shape))
    {
      counter++;
    }
  }
  return counter;
}

void deletePentagons(ShapesContainer& shapesContainer)
{
  bool found = false;
  ShapesContainer::iterator buffer;

  for (auto shape = shapesContainer.begin(); shape != shapesContainer.end(); shape++)
  {
    if (found)
    {
      shapesContainer.erase(buffer);
      found = false;
    }
    if (shape->size() == PENTAGON_VERTICES)
    {
      found = true;
      buffer = shape;
    }
  }

  if (found)
  {
    shapesContainer.erase(buffer);
  }
}

Shape getFirstPoints(const ShapesContainer& shapesContainer)
{
  Shape firstPoints;
  for (auto shape : shapesContainer)
  {
    firstPoints.push_back(shape[0]);
  }
  
  return firstPoints;
}

void sortShapesContainer(ShapesContainer& shapesContainer)
{
  bool found = false;
  ShapesContainer::iterator buffer;

  ShapesContainer triangles;
  ShapesContainer rectangles;
  ShapesContainer squares;

  for (auto shape = shapesContainer.begin(); shape != shapesContainer.end(); shape++)
  {
    if (found)
    {
      shapesContainer.erase(buffer);
      found = false;
    }
    if (shape->size() == TRIANGLE_VERTICES)
    {
      found = true;
      buffer = shape;
      triangles.push_back(*shape);
      continue;
    }
    if (isSquare(*shape))
    {
      found = true;
      buffer = shape;
      squares.push_back(*shape);
      continue;
    }
    if (isRectangle(*shape))
    {
      found = true;
      buffer = shape;
      rectangles.push_back(*shape);
    }
  }

  if (found)
  {
    shapesContainer.erase(buffer);
  }

  shapesContainer.splice(shapesContainer.begin(), rectangles);
  shapesContainer.splice(shapesContainer.begin(), squares);
  shapesContainer.splice(shapesContainer.begin(), triangles);
}
