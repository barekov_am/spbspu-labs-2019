#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <vector>

struct Point
{
  int x, y;
};

using Shape = std::vector<Point>;

#endif
