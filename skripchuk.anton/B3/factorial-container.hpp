#ifndef FACTORIAL_CONTAINER_HPP
#define FACTORIAL_CONTAINER_HPP

#include <iterator>

class factorial_container
{
public:
  class iterator;

  class reverse_iterator;

  factorial_container();

  iterator begin();
  iterator end();
  reverse_iterator rbegin();
  reverse_iterator rend();
};

class factorial_container::iterator : public std::iterator<std::bidirectional_iterator_tag, size_t>
{
public:
  iterator();
  iterator(size_t pos);

  bool operator ==(const iterator& rhs) const;
  bool operator !=(const iterator& rhs) const;

  size_t operator *() const;

  iterator& operator ++();
  iterator operator ++(int);
  iterator& operator --();
  iterator operator --(int);

private:
  size_t pos_;
};

class factorial_container::reverse_iterator : public std::iterator<std::bidirectional_iterator_tag, size_t>
{
public:
  reverse_iterator();
  reverse_iterator(size_t pos);

  size_t operator *() const;

  bool operator ==(const reverse_iterator& rhs) const;
  bool operator !=(const reverse_iterator& rhs) const;

  reverse_iterator& operator ++();
  reverse_iterator& operator --();

private:
  size_t pos_;
};

#endif
