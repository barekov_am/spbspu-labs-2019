#include <iostream>
#include <sstream>
#include <string>
#include <stdexcept>
#include "phonebook-interface.hpp"

void task1()
{
  PhonebookInterface phonebookInterface;

  std::string line;

  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Error while reading");
    }

    std::stringstream input(line);

    phonebookInterface.readCommand(input);
  }
}
