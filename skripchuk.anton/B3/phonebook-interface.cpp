#include "phonebook-interface.hpp"
#include "phonebook.hpp"
#include <string>
#include <stdexcept>
#include <iostream>
#include <cctype>

std::string PhonebookInterface::getName(std::string& name)
{
  if (name.empty() || (name.size() < 2) || (name.front() != '\"') || (name.back() != '\"'))
  {
    throw std::invalid_argument("Invalid name");
  }
  
  name.erase(0, 1);
  name.erase(name.size() - 1, 1);

  for (size_t i = 0; i < name.size(); ++i)
  {
    if (name[i] == '\\')
    {
      if ((name[i + 1] == '\\') || (name[i + 1] == '\"'))
      {
        name.erase(i, 1);
      }
      else
      {
        throw std::invalid_argument("Invalid name");
      }
    }
  }
  return name;
}

std::string PhonebookInterface::getNumber(const std::string& number)
{
  if (number.empty())
  {
    throw std::invalid_argument("Invalid number");
  }

  for (size_t i = 0; i < number.size(); ++i)
  {
    if (!isdigit(number[i]))
    {
      throw std::invalid_argument("Invalid number");
    }
  }
  return number;
}

std::string PhonebookInterface::getMarkName(std::string& markName)
{
  if (markName.empty())
  {
    throw std::invalid_argument("Invalid bookmark");
  }

  for (size_t i = 0;  i < markName.size(); ++i)
  {
    if (!(isalnum(markName[i]) || isdigit(markName[i]) ||(markName[i] == '-')))
    {
      throw std::invalid_argument("Invalid bookmark");
    }
  }
  return markName;
}

PhonebookInterface::bookmarks::iterator PhonebookInterface::getIterator(const std::string& markName)
{
  auto iter = bookmarks_.find(markName);
  if (iter == bookmarks_.end())
  {
    throw std::out_of_range("Invalid bookmark");
  }
  return iter;
}

PhonebookInterface::PhonebookInterface()
{
  bookmarks_["current"] = phonebook_.begin();
}

void PhonebookInterface::add(const Phonebook::record_t& record)
{
  phonebook_.push_back(record);
  if (std::next(phonebook_.begin()) == phonebook_.end())
  {
    bookmarks_["current"] = phonebook_.begin();
  }
}

void PhonebookInterface::insert(const std::string& place, const std::string& markName, const Phonebook::record_t& record)
{
  auto iter = getIterator(markName);
  if (place == "before")
  {
    if (phonebook_.empty())
    {
      add(record);
    }
    else
    {
      phonebook_.insert_before(iter->second, record);
    }
  }
  else if (place == "after")
  {
    if (phonebook_.empty())
    {
      add(record);
    }
    else
    {
      phonebook_.insert_after(iter->second, record);
    }
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
  }
}

void PhonebookInterface::erase(const std::string& markName)
{
  auto mark = getIterator(markName);
  auto oldIter = mark->second;
  for (auto iter = bookmarks_.begin(); iter != bookmarks_.end(); ++iter)
  {
    if (iter->second == oldIter)
    {
      if (std::next(oldIter) == phonebook_.end())
      {
        iter->second = std::prev(oldIter);
      }
      else
      {
        iter->second = std::next(oldIter);
      }
    }
  }
  mark->second = phonebook_.erase(oldIter);
}

void PhonebookInterface::move(const std::string& markName, int steps)
{
  auto iter = getIterator(markName);
  iter->second = phonebook_.move(iter->second, steps);
}

void PhonebookInterface::moveToFirst(const std::string& markName)
{
  auto iter = getIterator(markName);
  iter->second = phonebook_.begin();
}

void PhonebookInterface::moveToLast(const std::string& markName)
{
  auto iter = getIterator(markName);
  iter->second = std::prev(phonebook_.end());
}

void PhonebookInterface::readCommand(std::stringstream& input)
{
  std::string command;
  input >> command;

  if (command == "add")
  {
    try
    {
      std::string number;
      std::string name;
      input >> number >> name;

      if (!input.eof())
      {
        std::string afterSpace;
        std::getline(input, afterSpace);
        name += afterSpace;
      }

      number = getNumber(number);
      name = getName(name);
      add({ name, number });
    }
    catch (std::invalid_argument&)
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
  else if (command == "store")
  {
    try
    {
      std::string markName;
      std::string newMarkName;
      input >> markName >> newMarkName;

      newMarkName = getMarkName(newMarkName);
      auto iter = getIterator(markName);
      bookmarks_.emplace(newMarkName, iter->second);
    }
    catch (std::invalid_argument&)
    {
      std::cout << "<INVALID BOOKMARK>\n";
    }
    catch (std::out_of_range&)
    {
      std::cout << "<INVALID BOOKMARK>\n";
    }
  }
  else if (command == "insert")
  {
    try
    {
      std::string place;
      std::string markName;
      std::string number;
      std::string name;
      input >> place >> markName >> number >> name;

      if (!input.eof())
      {
        std::string afterSpace;
        std::getline(input, afterSpace);
        name += afterSpace;
      }
      
      number = getNumber(number);
      name = getName(name);

      insert(place, markName, { name, number });
    }
    catch (std::out_of_range&)
    {
      std::cout << "<INVALID BOOKMARK>\n";
    }
    catch (std::invalid_argument&)
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
  else if (command == "delete")
  {
    try
    {
      std::string markName;
      input >> markName;

      erase(markName);
    }
    catch (std::invalid_argument&)
    {
      std::cout << "<INVALID BOOKMARK>\n";
    }
  }
  else if (command == "show")
  {
    try
    {
      std::string markName;

      input >> markName;

      auto iter = getIterator(markName);

      if (phonebook_.empty())
      {
        std::cout << "<EMPTY>\n";
        return;
      }

      std::cout << iter->second->number << " " << iter->second->name << "\n";
    }
    catch (std::out_of_range&)
    {
      std::cout << "<INVALID BOOKMARK>\n";
    }
  }
  else if (command == "move")
  {
    try
    {
      std::string markName;
      std::string steps;
      input >> markName >> steps;

      if (steps == "first")
      {
        moveToFirst(markName);
      }
      else if (steps == "last")
      {
        moveToLast(markName);
      }
      else
      {
        if ((steps[0] == '+') || (steps[0] == '-'))
        {
          for (size_t i = 1; i < steps.size(); ++i)
          {
            if (!isdigit(steps[i]))
            {
              throw std::invalid_argument("Invalid step");
            }
          }
        }
        else
        {
          for (size_t i = 0; i < steps.size(); ++i)
          {
            if (!isdigit(steps[i]))
            {
              throw std::invalid_argument("Invalid step");
            }
          }
        }

        int stepNumber = stoi(steps);
        move(markName, stepNumber);
      }
    }
    catch (std::out_of_range&)
    {
      std::cout << "<INVALID BOOKMARK>\n";
    }
    catch (std::invalid_argument&)
    {
      std::cout << "<INVALID STEP>\n";
    }
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
  }
}
