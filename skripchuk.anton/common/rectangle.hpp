#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace skripchuk
{
  class Rectangle : public Shape
  {
  public:

    Rectangle(const point_t& centre, double width, double height);
    Rectangle(double x, double y, double width, double height);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printParameters() const override;
    void move(double dx, double dy) override;
    void move(const point_t& centre) override;
    void scale(double multiplier) override;
    void rotate(double degree) override;

  private:

    point_t centre_;
    double width_;
    double height_;
    double rotationDegree_;
  };
}
#endif //RECTANGLE_HPP
