#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteRectangle)

  const double PRECISION = 0.01;

  BOOST_AUTO_TEST_CASE(RectangleConstAfterMoving)
  {
    skripchuk::Rectangle rectangle_({1, -1}, 2, 3);
    const skripchuk::rectangle_t firstFrame = rectangle_.getFrameRect();
    const double firstArea = rectangle_.getArea();

    rectangle_.move({4, 5});
    skripchuk::rectangle_t secondFrame = rectangle_.getFrameRect();
    double secondArea = rectangle_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
    BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);

    rectangle_.move(-2, 3);
    secondFrame = rectangle_.getFrameRect();
    secondArea = rectangle_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
    BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);
  }

  BOOST_AUTO_TEST_CASE(RectangleScaling)
  {
    skripchuk::Rectangle rectangle_({1, -1}, 2, 3);
    const double firstArea = rectangle_.getArea();

    const double multiplier = 2.2;
    rectangle_.scale(multiplier);
    double secondArea = rectangle_.getArea();

    BOOST_CHECK_CLOSE(firstArea *  multiplier * multiplier, secondArea, PRECISION);
  }

  BOOST_AUTO_TEST_CASE(RectangleThrowingExceptions)
  {
    BOOST_CHECK_THROW(skripchuk::Rectangle({5, 2}, -2, -3), std::invalid_argument);

    skripchuk::Rectangle rectangle_({3, 5}, 6, 2);
    BOOST_CHECK_THROW(rectangle_.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(testRectangleRotation)
  {
    skripchuk::Rectangle testRectangle({ 7, 2 }, 7.1, 2);
    const double areaBefore = testRectangle.getArea();
    const skripchuk::rectangle_t frameRectBefore = testRectangle.getFrameRect();

    double angle = -90;
    testRectangle.rotate(angle);
    double areaAfter = testRectangle.getArea();
    skripchuk::rectangle_t frameRectAfter = testRectangle.getFrameRect();

    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);

    angle = 40;
    testRectangle.rotate(angle);
    angle = 140;
    testRectangle.rotate(angle);

    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);
  }

BOOST_AUTO_TEST_SUITE_END()
