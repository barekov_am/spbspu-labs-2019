#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <cstddef>
#include "shape.hpp"

namespace skripchuk
{
  class Polygon : public Shape
  {
  public:

    Polygon(size_t numVertices, const point_t *vertex);
    Polygon(const Polygon& other);
    Polygon(Polygon&& other);
    ~Polygon() override;

    Polygon &operator =(const Polygon& other);
    Polygon &operator =(Polygon&& other);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printParameters() const override;
    void move(double dx, double dy) override;
    void move(const point_t& new_point) override;
    void scale(double multiplier) override;
    void rotate(double angle) override;
    size_t getSize() const;

  private:
    size_t numVertices_;
    point_t* vertex_;
    point_t getCentre() const;
    bool checkConvexity(size_t numVertices, const point_t* vertex) const;
  };
}
#endif
