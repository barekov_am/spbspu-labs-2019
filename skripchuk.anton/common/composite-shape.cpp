#include "composite-shape.hpp"
#include <iostream>
#include <cmath>
#include <math.h>

skripchuk::CompositeShape::CompositeShape():
  numShapes_(0),
  shapes_(nullptr)
{ }

skripchuk::CompositeShape::CompositeShape(const Shape::pointer& shape):
  CompositeShape()
{
  add(shape);
}

skripchuk::CompositeShape::CompositeShape(const CompositeShape& newShape):
  numShapes_(newShape.numShapes_),
  shapes_(std::make_unique<skripchuk::Shape::pointer[]>(newShape.numShapes_))
{
  for (size_t i = 0; i < numShapes_; i++)
  {
    shapes_[i] = newShape.shapes_[i];
  }
}

skripchuk::CompositeShape::CompositeShape(CompositeShape&& newShape):
  numShapes_(newShape.numShapes_),
  shapes_(std::move(newShape.shapes_))
{
  newShape.numShapes_ = 0;
  newShape.shapes_ = nullptr;
}

skripchuk::CompositeShape& skripchuk::CompositeShape::operator =(const CompositeShape& newShape)
{
  if (this != &newShape)
  {
    shapes_ = std::make_unique<skripchuk::Shape::pointer[]>(newShape.numShapes_);
    numShapes_ = newShape.numShapes_;
    for (size_t i = 0; i < numShapes_; i++)
    {
      shapes_[i] = newShape.shapes_[i];
    }
  }
  return *this;
}

skripchuk::CompositeShape& skripchuk::CompositeShape::operator =(CompositeShape&& newShape)
{
  if (this != &newShape)
  {
    numShapes_ = newShape.numShapes_;
    shapes_ = std::move(newShape.shapes_);
    newShape.numShapes_ = 0;
    newShape.shapes_ = nullptr;
  }
  return *this;
}

skripchuk::Shape::pointer skripchuk::CompositeShape::operator [](size_t index) const
{
  if (index >= numShapes_)
  {
    throw std::out_of_range("index is out of range");
  }

  return shapes_[index];
}


void skripchuk::CompositeShape::printParameters() const
{
  if (numShapes_ == 0)
  {
    std::cout << "composite shape is empty";
  }
  else
  {
    rectangle_t frameRect = getFrameRect();
    std::cout << "  centre: (" << frameRect.pos.x << " , " << frameRect.pos.y << ")" << std::endl;
    std::cout << "  frame rectangle:" << std::endl;
    std::cout << "    width = " << frameRect.width << std::endl;
    std::cout << "    height = " << frameRect.height << std::endl;
    std::cout << "  elements: " << std::endl;
    for (size_t i = 0; i < numShapes_; i++) {
      shapes_[i]->printParameters();
      std::cout << std::endl;
    }
  }
}

double skripchuk::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < numShapes_; i++)
  {
    area += shapes_[i]->getArea();
  }

  return area;
}

skripchuk::rectangle_t skripchuk::CompositeShape::getFrameRect() const
{
  if (numShapes_ == 0)
  {
    return rectangle_t{point_t{0, 0}, 0, 0};
  }

  rectangle_t tempRect = shapes_[0]->getFrameRect();

  double minX = tempRect.pos.x - tempRect.width / 2;
  double maxX = tempRect.pos.x + tempRect.width / 2;
  double minY = tempRect.pos.y - tempRect.height / 2;
  double maxY = tempRect.pos.y + tempRect.height / 2;

  for (size_t i = 1; i < numShapes_; i++)
  {
    tempRect = shapes_[i]->getFrameRect();

    minX = std::min(minX, tempRect.pos.x - tempRect.width / 2);
    maxX = std::max(maxX, tempRect.pos.x + tempRect.width / 2);
    minY = std::min(minY, tempRect.pos.y - tempRect.height / 2);
    maxY = std::max(maxY, tempRect.pos.y + tempRect.height / 2);
  }

  return rectangle_t{point_t{(maxX + minX) / 2, (maxY + minY) / 2}, maxX - minX, maxY - minY};
}

void skripchuk::CompositeShape::move(double dx, double dy)
{
  if (numShapes_ != 0)
  {
    for (size_t i = 0; i < numShapes_; i++)
    {
      shapes_[i]->move(dx, dy);
    }
  }
}

void skripchuk::CompositeShape::move(const point_t& newCentre)
{
  rectangle_t frameRect = getFrameRect();
  double dx = newCentre.x - frameRect.pos.x;
  double dy = newCentre.y - frameRect.pos.y;
  move(dx, dy);
}

void skripchuk::CompositeShape::scale(double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("scale multiplier can't be <=0");
  }

  if (numShapes_ != 0)
  {
    rectangle_t frameRect = getFrameRect();

    for (size_t i = 0; i < numShapes_; i++)
    {
      rectangle_t currentFrameRect = shapes_[i]->getFrameRect();
      double dx = (currentFrameRect.pos.x - frameRect.pos.x) * (multiplier - 1);
      double dy = (currentFrameRect.pos.y - frameRect.pos.y) * (multiplier - 1);
      shapes_[i]->move(dx, dy);
      shapes_[i]->scale(multiplier);
    }
  }
}

void skripchuk::CompositeShape::rotate(double degree)
{
  const double cos = std::cos((2 * M_PI * degree) / 360);
  const double sin = std::sin((2 * M_PI * degree) / 360);

  const point_t compCentre = getFrameRect().pos;

  for (std::size_t i = 0; i < numShapes_; i++)
  {
    const point_t shapeCentre = shapes_[i]->getFrameRect().pos;
    const double dx = (shapeCentre.x - compCentre.x) * cos - (shapeCentre.y - compCentre.y) * sin;
    const double dy = (shapeCentre.x - compCentre.x) * sin + (shapeCentre.y - compCentre.y) * cos;

    shapes_[i]->move({ compCentre.x + dx, compCentre.y + dy });
    shapes_[i]->rotate(degree);
  }
}


size_t skripchuk::CompositeShape::getSize() const
{
  return(numShapes_);
}

void skripchuk::CompositeShape::add(const Shape::pointer& shape)
{
  Shape::array shapes = std::make_unique<Shape::pointer[]>(numShapes_ + 1);

  for (size_t i = 0; i < numShapes_; i++)
  {
    shapes[i] = shapes_[i];
  }

  shapes[numShapes_] = shape;
  numShapes_++;
  shapes_ = std::move(shapes);
}

void skripchuk::CompositeShape::remove(size_t index)
{
  if (index >= numShapes_)
  {
    throw std::logic_error("deleted shape number is > than quantity of shapes in composite shape");
  }
  for (size_t i = index; i < (numShapes_ - 1); i++)
  {
    shapes_[i] = shapes_[i+1];
  }
  shapes_[--numShapes_] = nullptr;
}
