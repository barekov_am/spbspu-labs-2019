#include "rectangle.hpp"
#include <iostream>
#include <cassert>
#include <math.h>

skripchuk::Rectangle::Rectangle(const point_t& centre, double width, double height):
  centre_(centre),
  width_(width),
  height_(height),
  rotationDegree_(0)
{
  if (width_ <= 0)
  {
    throw std::invalid_argument("width of rectangle can't be <= 0");
  }
  if (height_ <= 0)
  {
    throw std::invalid_argument("height of rectangle can't be <= 0");
  }
}

skripchuk::Rectangle::Rectangle(double x, double y, double width, double height):
  Rectangle({x, y}, width, height)
{ }

void skripchuk::Rectangle::printParameters() const
{
  std::cout << "rectangle: x = " << centre_.x << ", y = " << centre_.y << ", width = "
      << width_ << ", height = " << height_ << ", degree = " << rotationDegree_ <<std::endl << std::endl;
}

double skripchuk::Rectangle::getArea() const
{
  return (width_ * height_);
}

skripchuk::rectangle_t skripchuk::Rectangle::getFrameRect() const
{
  const double cos = std::cos((2 * M_PI * rotationDegree_) / 360);
  const double sin = std::sin((2 * M_PI * rotationDegree_) / 360);
  const double width = width_ * std::fabs(cos) + height_ * std::fabs(sin);
  const double height = height_ * std::fabs(cos) + width_ * std::fabs(sin);
  return {centre_, width, height};
}

void skripchuk::Rectangle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

void skripchuk::Rectangle::move(const point_t& centre)
{
  centre_ = centre;
}

void skripchuk::Rectangle::scale(double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("scale multiplier can't be <=0");
  }
  width_ *= multiplier;
  height_ *= multiplier;
}

void skripchuk::Rectangle::rotate(double degree) {
  rotationDegree_ += degree;

  while (std::abs(rotationDegree_) >= 360)
  {
    rotationDegree_ = (degree > 0) ? rotationDegree_ - 360 : rotationDegree_ + 360;
  }
}
