#ifndef A3_PARTITION_HPP
#define A3_PARTITION_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace skripchuk
{
  skripchuk::Matrix part(const skripchuk::CompositeShape& composition);
}

#endif //A3_PARTITION_HPP
