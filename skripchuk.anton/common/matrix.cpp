#include "matrix.hpp"

skripchuk::Matrix::Matrix():
    rows_(0),
    columns_(0)
{ }

skripchuk::Matrix::Matrix(const Matrix& newMatrix):
    data_(std::make_unique<Shape::pointer[]>(newMatrix.rows_ * newMatrix.columns_)),
    rows_(newMatrix.rows_),
    columns_(newMatrix.columns_)
{
  for (std::size_t i = 0; i < (rows_ * columns_); i++)
  {
    data_[i] = newMatrix.data_[i];
  }
}

skripchuk::Matrix::Matrix(Matrix&& newMatrix):
    data_(std::move(newMatrix.data_)),
    rows_(newMatrix.rows_),
    columns_(newMatrix.columns_)
{
  newMatrix.rows_ = 0;
  newMatrix.columns_ = 0;
}

skripchuk::Matrix& skripchuk::Matrix::operator =(const Matrix& newMatrix)
{
  if (&newMatrix == this)
  {
    return *this;
  }

  Shape::array tmpData = std::make_unique<Shape::pointer[]>(newMatrix.rows_ * newMatrix.columns_);
  for (std::size_t i = 0; i < (newMatrix.rows_ * newMatrix.columns_); i++)
  {
    tmpData[i] = newMatrix.data_[i];
  }

  data_.swap(tmpData);
  rows_ = newMatrix.rows_;
  columns_ = newMatrix.columns_;

  return *this;
}

skripchuk::Matrix& skripchuk::Matrix::operator =(Matrix&& newMatrix)
{
  if (&newMatrix == this)
  {
    return *this;
  }
  rows_ = newMatrix.rows_;
  columns_ = newMatrix.columns_;
  data_ = std::move(newMatrix.data_);
  newMatrix.rows_ = 0;
  newMatrix.columns_ = 0;

  return *this;
}

skripchuk::Shape::array skripchuk::Matrix::operator [](std::size_t index) const
{
  if (index >= rows_)
  {
    throw std::out_of_range("Index is out of range!");
  }

  Shape::array tmpData = std::make_unique<Shape::pointer[]>(getLevelSize(index));

  for (std::size_t i = 0; i < getLevelSize(index); i++)
  {
    tmpData[i] = data_[index * columns_ + i];
  }

  return tmpData;
}

bool skripchuk::Matrix::operator ==(const Matrix& newMatrix) const
{
  if ((rows_ != newMatrix.rows_) || (columns_ != newMatrix.columns_))
  {
    return false;
  }

  for (std::size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (data_[i] != newMatrix.data_[i]) {
      return false;
    }
  }

  return true;
}

bool skripchuk::Matrix::operator !=(const Matrix& newMatrix) const
{
  return !(*this == newMatrix);
}

std::size_t skripchuk::Matrix::getRows() const
{
  return rows_;
}

std::size_t skripchuk::Matrix::getColumns() const
{
  return columns_;
}

std::size_t skripchuk::Matrix::getLevelSize(size_t level) const
{
  if (level >= rows_)
  {
    return 0;
  }

  for (std::size_t i = 0; i < columns_; i++)
  {
    if (data_[level * columns_ + i] == nullptr)
    {
      return i;
    }
  }

  return columns_;
}

void skripchuk::Matrix::add(const Shape::pointer& shape, std::size_t index)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Pointer can't be null!");
  }
  std::size_t tmpRows = (index >= rows_) ? (rows_ + 1) : rows_;
  std::size_t tmpColumns = (getLevelSize(index) == columns_) ? (columns_ + 1) : columns_;
  const std::size_t column = (index < rows_) ? getLevelSize(index) : 0;

  Shape::array tmpData = std::make_unique<Shape::pointer[]>(tmpRows * tmpColumns);

  for (std::size_t i = 0; i < tmpRows; i++)
  {
    for (std::size_t j = 0; j < tmpColumns; j++)
    {
      if ((i == rows_) || (j == columns_))
      {
        tmpData[i * tmpColumns + j] = nullptr;
      }
      else
      {
        tmpData[i * tmpColumns + j] = data_[i * columns_ + j];
      }
    }
  }

  if (index < rows_)
  {
    tmpData[index * tmpColumns + column] = shape;
  }
  else
  {
    tmpData[rows_ * tmpColumns + column] = shape;
  }

  data_.swap(tmpData);
  rows_ = tmpRows;
  columns_ = tmpColumns;
}
