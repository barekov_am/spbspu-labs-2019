#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace skripchuk
{
  class Shape
  {
  public:
    using pointer = std::shared_ptr<Shape>;
    using array = std::unique_ptr<pointer[]>;

    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void printParameters() const = 0;
    virtual void move(double x, double y) = 0;
    virtual void move(const point_t&) = 0;
    virtual void scale(double multiplier) = 0;
    virtual void rotate(double degree) = 0;
  };
}

#endif //SHAPE_HPP
