#include <boost/test/auto_unit_test.hpp>

#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

const double PRECISION = 0.01;

BOOST_AUTO_TEST_SUITE(testPartition)

  BOOST_AUTO_TEST_CASE(intersectCorrectness)
  {
    skripchuk::Circle testCircle({ 2, 0 }, 3);
    skripchuk::Rectangle testRectangle({ 2, 4 }, 9, 5);
    skripchuk::Rectangle testSquare({ 12, 21 }, 1, 1);

    BOOST_CHECK(skripchuk::isIntersected(testCircle.getFrameRect(),testRectangle.getFrameRect()));
    BOOST_CHECK(!(skripchuk::isIntersected(testSquare.getFrameRect(),testRectangle.getFrameRect())));
    BOOST_CHECK(!(skripchuk::isIntersected(testCircle.getFrameRect(),testSquare.getFrameRect())));
  }

  BOOST_AUTO_TEST_CASE(partitionCorrectness)
  {
    skripchuk::Shape::pointer testCircle = std::make_shared<skripchuk::Circle>(skripchuk::point_t{ 1, 1 }, 2);
    skripchuk::Shape::pointer testRectangle1 = std::make_shared<skripchuk::Rectangle>(skripchuk::point_t{ 3, -1 }, 5, 4);
    skripchuk::Shape::pointer testSquare1 = std::make_shared<skripchuk::Rectangle>(skripchuk::point_t{ 6, 15 }, 4, 4);
    skripchuk::Shape::pointer testRectangle2 = std::make_shared<skripchuk::Rectangle>(skripchuk::point_t{ 10, 2 }, 2, 3);
    skripchuk::Shape::pointer testRectangle3 = std::make_shared<skripchuk::Rectangle>(skripchuk::point_t{ 8, 15 }, 6, 30);
    skripchuk::Shape::pointer testSquare2 = std::make_shared<skripchuk::Rectangle>(skripchuk::point_t{ -20, 15 }, 5, 5);

    skripchuk::CompositeShape testComposition;
    testComposition.add(testCircle);
    testComposition.add(testRectangle1);
    testComposition.add(testSquare1);
    testComposition.add(testRectangle2);
    testComposition.add(testRectangle3);
    testComposition.add(testSquare2);

    skripchuk::Matrix testMatrix = skripchuk::part(testComposition);

    const std::size_t correctRows = 3;
    const std::size_t correctColumns = 4;

    BOOST_CHECK_EQUAL(testMatrix.getRows(), correctRows);
    BOOST_CHECK_EQUAL(testMatrix.getColumns(), correctColumns);
    BOOST_CHECK(testMatrix[0][0] == testCircle);
    BOOST_CHECK(testMatrix[0][1] == testSquare1);
    BOOST_CHECK(testMatrix[0][2] == testRectangle2);
    BOOST_CHECK(testMatrix[0][3] == testSquare2);
    BOOST_CHECK(testMatrix[1][0] == testRectangle1);
    BOOST_CHECK(testMatrix[2][0] == testRectangle3);
  }

BOOST_AUTO_TEST_SUITE_END()
