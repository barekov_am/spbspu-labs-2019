#include "commands.hpp"

#include <algorithm>
#include <iomanip>
#include <cctype>
#include <sstream>

#include "manipulators.hpp"
#include "stream-operators.hpp"

const auto reportInvalid = [](PhonebookManager&, std::ostream& output){ output << "<INVALID COMMAND>\n"; };

commands::execute_command commands::parseAddArguments(std::istream& input)
{
  std::string number;
  std::string name;
  std::string extraData;

  input >> manipulators::blank >> number >> manipulators::blank >> makeName(name)
        >> manipulators::blank >> extraData;

  if (name.empty() || !extraData.empty())
  {
    input.setstate(std::ios_base::failbit);

    return reportInvalid;
  }

  Phonebook::record_t record = {name, number};

  return [=](PhonebookManager& manager, std::ostream&){ manager.add(record); };
}

commands::execute_command commands::parseStoreArguments(std::istream& input)
{
  std::string bookmark;
  std::string newBookmark;
  std::string extraData;

  input >> manipulators::blank >> makeBookmark(bookmark)
        >> manipulators::blank >> makeBookmark(newBookmark)
        >> manipulators::blank >> extraData;

  if (bookmark.empty() || newBookmark.empty() || !extraData.empty())
  {
    input.setstate(std::ios_base::failbit);

    return reportInvalid;
  }

  return [=](PhonebookManager& manager, std::ostream& output)
         {
           if (!manager.bookmarkExists(bookmark))
           {
             output << "<INVALID BOOKMARK>\n";

             return;
           }

           manager.store(bookmark, newBookmark);
         };
}

commands::execute_command commands::parseInsertArguments(std::istream& input)
{
  insert_position_t insertPosition;
  std::string bookmark;
  std::string number;
  std::string name;
  std::string extraData;

  input >> manipulators::blank >> insertPosition >>  manipulators::blank >> makeBookmark(bookmark) 
        >> manipulators::blank >> number >> manipulators::blank >> makeName(name)
        >> manipulators::blank >> extraData;

  if (name.empty() || !extraData.empty())
  {
    input.setstate(std::ios_base::failbit);

    return reportInvalid;
  }

  Phonebook::record_t record = {name, number};

  return [=](PhonebookManager& manager, std::ostream& output)
         {
           if (!manager.bookmarkExists(bookmark))
           {
             output << "<INVALID BOOKMARK>\n";

             return;
           }

           manager.insert(bookmark, record, insertPosition.position);
         };
}

commands::execute_command commands::parseDeleteArguments(std::istream& input)
{
  std::string bookmark;
  std::string extraData;

  input >> manipulators::blank >> makeBookmark(bookmark) >> manipulators::blank >> extraData;

  if (bookmark.empty() || !extraData.empty())
  {
    input.setstate(std::ios_base::failbit);

    return reportInvalid;
  }

  return [=](PhonebookManager& manager, std::ostream& output)
         {
           if (!manager.bookmarkExists(bookmark))
           {
             output << "<INVALID BOOKMARK>\n";

             return;
           }

           if (!manager.bookmarkValid(bookmark))
           {
             output << "<INVALID COMMAND>\n";

             return;
           }

           manager.remove(bookmark);
         };
}

commands::execute_command commands::parseShowArguments(std::istream& input)
{
  std::string bookmark;
  std::string extraData;

  input >> manipulators::blank >> makeBookmark(bookmark) >> manipulators::blank >> extraData;

  if (bookmark.empty() || !extraData.empty())
  {
    input.setstate(std::ios_base::failbit);

    return reportInvalid;
  }

  return [=](PhonebookManager& manager, std::ostream& output)
         {
           if (!manager.bookmarkExists(bookmark))
           {
             output << "<INVALID BOOKMARK>\n";

             return;
           }

           if (manager.recordsEmpty())
           {
             output << "<EMPTY>\n";

             return;
           }

           if (!manager.bookmarkValid(bookmark))
           {
             return;
           }

           manager.show(bookmark, output);
         };
}

commands::execute_command commands::parseMoveArguments(std::istream& input)
{
  //Move position uses different error message so these inputs cannot be combined into one.
  std::string bookmark;
  input >> manipulators::blank >> makeBookmark(bookmark);

  if (input.fail())
  {
    return reportInvalid;
  }

  move_position_t position;
  input >> manipulators::blank >> position;
  if (input.fail())
  {
    return [](PhonebookManager&, std::ostream& output){ output << "<INVALID STEP>\n"; };
  }

  std::string extraData;
  input >> manipulators::blank >> extraData;
  if (!extraData.empty())
  {
    input.setstate(std::ios_base::failbit);

    return reportInvalid;
  }

  return [=](PhonebookManager& manager, std::ostream& output)
         {
           if (!manager.bookmarkExists(bookmark))
           {
             output << "<INVALID BOOKMARK>\n";

             return;
           }

           if (auto var = boost::get<move_position>(&position.position))
           {
             manager.move(bookmark, *var);
           }

           if (auto var = boost::get<int>(&position.position))
           {
             manager.move(bookmark, *var);
           }
         };
}
