#include "commands.hpp"
#include "phonebook-manager.hpp"
#include "stream-operators.hpp"

void task1(std::istream& input, std::ostream& output)
{
  PhonebookManager manager;

  std::for_each(std::istream_iterator<commands::execute_command>(input >> std::noskipws), std::istream_iterator<commands::execute_command>(),
      [&](auto& command)
      {
        command(manager, output);
        input >> std::ws;
      });
}
