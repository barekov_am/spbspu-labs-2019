#include "phonebook-manager.hpp"

#include <algorithm>
#include <iostream>

const std::string Current = "current";

PhonebookManager::PhonebookManager()
{
  bookmarks_[Current] = records_.end();
}

void PhonebookManager::add(const Phonebook::record_t& record)
{
  records_.push_back(record);

  if (std::next(records_.begin()) == records_.end())
  {
    bookmarks_[Current] = records_.begin();
  }
}

void PhonebookManager::store(const std::string& bookmark, const std::string& newBookmark)
{
  auto it = getElementIterator(bookmark);

  bookmarks_.emplace(newBookmark, it->second);
}

void PhonebookManager::insert(const std::string& bookmark, const Phonebook::record_t& record, InsertPosition position)
{
  auto it = getElementIterator(bookmark);

  if (it->second == records_.end())
  {
    add(record);

    return;
  }

  auto pos = (position == InsertPosition::before) ? it->second : std::next(it->second);

  records_.insert(pos, record);
}

void PhonebookManager::remove(const std::string& bookmark)
{
  auto it = getElementIterator(bookmark);

  if (it->second == records_.end())
  {
    throw std::invalid_argument("The element to remove does not exist.");
  }

  auto current = it->second;

  //There is a bit of violation of coding guidelines down here, as i thought it's nicer from my point of view.
  //Please, share your opinion here on `if` design in lambdas like that.

  std::for_each(bookmarks_.begin(), bookmarks_.end(),
      [&](auto& bookmark){ if (bookmark.second == current) bookmark.second = std::next(current); });

  records_.erase(current);

  if (records_.empty())
  {
    return;
  }

  std::for_each(bookmarks_.begin(), bookmarks_.end(),
      [&](auto& bookmark){ if (bookmark.second == records_.end()) bookmark.second = std::prev(records_.end()); });
}

void PhonebookManager::show(const std::string& bookmark, std::ostream& output)
{
  auto it = getElementIterator(bookmark);

  if (it->second == records_.end())
  {
    throw std::invalid_argument("The records list is empty.");
  }

  output << it->second->number << " " << it->second->name << "\n";
}

void PhonebookManager::move(const std::string& bookmark, int steps)
{
  auto it = getElementIterator(bookmark);

  std::advance(it->second, steps);
}

void PhonebookManager::move(const std::string& bookmark, MovePosition position)
{
  auto it = getElementIterator(bookmark);

  auto newPos = (position == MovePosition::first) ? records_.begin() : std::prev(records_.end());

  it->second = newPos;
}

void PhonebookManager::moveNext(const std::string& bookmark)
{
  move(bookmark, 1);
}

void PhonebookManager::movePrev(const std::string& bookmark)
{
  move(bookmark, -1);
}

void PhonebookManager::replace(const std::string& bookmark, const Phonebook::record_t& record)
{
  auto it = getElementIterator(bookmark);

  *(it->second) = record;
}

bool PhonebookManager::bookmarkExists(const std::string& bookmark) const
{
  auto it = bookmarks_.find(bookmark);

  return it != bookmarks_.end();
}

bool PhonebookManager::bookmarkValid(const std::string& bookmark)
{
  auto it = getElementIterator(bookmark);

  return it->second != records_.end();
}

bool PhonebookManager::recordsEmpty() const
{
  return records_.empty();
}

PhonebookManager::bookmarks::iterator PhonebookManager::getElementIterator(const std::string& bookmark)
{
  auto it = bookmarks_.find(bookmark);

  if (it != bookmarks_.end())
  {
    return it;
  }

  throw std::invalid_argument("The bookmark does not exist.");
}
