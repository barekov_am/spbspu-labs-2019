#include <boost/test/unit_test.hpp>

#include "commands.hpp"
#include "phonebook-manager.hpp"
#include "stream-operators.hpp"

BOOST_AUTO_TEST_SUITE(commandExecutionTestSuite)

  BOOST_AUTO_TEST_CASE(addOneElementWithComplexName)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "add 1 \"a      \\\"  \t\toranger\\\"  \t\"",   //add element
      "show current",                                 //show it
      "store current bookmark",                       //store with another bookmark
      "show bookmark"                                 //show another bookmark
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    BOOST_CHECK_MESSAGE(out[1].str() == "1 a      \"  \t\toranger\"  \t\n", "Complex name Fail.");
    BOOST_CHECK_MESSAGE(out[3].str() == "1 a      \"  \t\toranger\"  \t\n", "Bookmark store Fail.");
  }

  BOOST_AUTO_TEST_CASE(addASequenceOfElements)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "add 1 \"a\"",
      "add 2 \"b\"",
      "add 3 \"c\"",
      "add 4 \"d\"",
      "add 5 \"e\"",
      "add 6 \"f\"",     //add some elements
      "store current first",
      "move current 1",
      "store current second",
      "move current last",
      "store current sixth",
      "move current -1",
      "store current fifth",
      "move current -2",
      "store current third",
      "move current 1",
      "store current fourth",
      "show first",
      "show second",
      "show third",
      "show fourth",
      "show fifth",
      "show sixth"      //show them
    };

    std::vector<std::string> checks =
    {
      "1 a\n",
      "2 b\n",
      "3 c\n",
      "4 d\n",
      "5 e\n",
      "6 f\n"
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    for (size_t i = 0; i < commands.size() - checks.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, std::cout);
    }

    std::vector<std::stringstream> out(checks.size());

    for (size_t i = checks.size(); i > 0; --i)
    {
      commands::execute_command command;
      in[commands.size() - i] >> command;
      command(manager, out[checks.size() - i]);
    }

    for (size_t i = 0; i < checks.size(); ++i)
    {
      BOOST_CHECK_MESSAGE(out[i].str() == checks[i], "no. " << i + 1 << " check fail!");
    }
  }

  BOOST_AUTO_TEST_CASE(insertBetweenElements)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "add 1 \"a\"",
      "add 2 \"b\"",                     //add some elements
      "insert after current 25 \"ab\"",  //insert between them
      "show current",
      "move current last",
      "store current lastbm",
      "move current first",
      "show lastbm",
      "delete current",
      "delete lastbm",
      "show current"                     //show them with different delete examples
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    BOOST_CHECK_MESSAGE(out[3].str() == "1 a\n", "InsertBetween check Fail.");
    BOOST_CHECK_MESSAGE(out[7].str() == "2 b\n", "InsertBetween check Fail.");
    BOOST_CHECK_MESSAGE(out[10].str() == "25 ab\n", "InsertBetween check Fail.");
  }

  BOOST_AUTO_TEST_CASE(addAfterInsert)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "add 1 \"a\"",
      "add 2 \"b\"",                     //add some elements
      "insert after current 25 \"ab\"",  //insert between them
      "add 3 \"c\"",                     //add after insert
      "store current first",
      "move current 1",
      "store current second",
      "move current 1",
      "store current third",
      "move current 1",
      "store current fourth",
      "show first",
      "show second",
      "show third",
      "show fourth"
    };

    std::vector<std::string> checks =
    {
      "1 a\n",
      "25 ab\n",
      "2 b\n",
      "3 c\n"
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    for (size_t i = 0; i < commands.size() - checks.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, std::cout);
    }

    std::vector<std::stringstream> out(checks.size());

    for (size_t i = checks.size(); i > 0; --i)
    {
      commands::execute_command command;
      in[commands.size() - i] >> command;
      command(manager, out[checks.size() - i]);
    }

    for (size_t i = 0; i < checks.size(); ++i)
    {
      BOOST_CHECK_MESSAGE(out[i].str() == checks[i], "no. " << i + 1 << " check fail!");
    }
  }

  BOOST_AUTO_TEST_CASE(bookmarkRemainsValidAfterDeletion)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "add 1 \"a\"",
      "add 2 \"b\"",                     //add some elements
      "move current last",               //move current to "2 b"
      "delete current",                  //delete "2 b", so current should be now "1 a"
      "show current"                     //show
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    BOOST_CHECK_MESSAGE(out[commands.size() - 1].str() == "1 a\n", "Bookmark validity after deletion check Fail.");
  }

BOOST_AUTO_TEST_SUITE_END()
