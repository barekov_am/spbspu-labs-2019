#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <boost/variant.hpp>
#include <functional>

#include "phonebook-manager.hpp"

namespace commands
{
  using insert_position = PhonebookManager::InsertPosition;
  using move_position = PhonebookManager::MovePosition;
  using execute_command = std::function<void(PhonebookManager&, std::ostream&)>;

  struct insert_position_t
  {
    const char* name;
    insert_position position;
  };

  struct move_position_t
  {
    const char* name;
    boost::variant<move_position, int> position;
  };

  execute_command parseAddArguments(std::istream& input);
  execute_command parseStoreArguments(std::istream& input);
  execute_command parseInsertArguments(std::istream& input);
  execute_command parseDeleteArguments(std::istream& input);
  execute_command parseShowArguments(std::istream& input);
  execute_command parseMoveArguments(std::istream& input);
}

#endif
