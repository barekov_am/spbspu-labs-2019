#include <boost/test/unit_test.hpp>

#include "commands.hpp"
#include "phonebook-manager.hpp"
#include "stream-operators.hpp"

BOOST_AUTO_TEST_SUITE(commandRecognitionTestSuite)

  BOOST_AUTO_TEST_CASE(invalidInitialCommand)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "hello",
      "Hello, world!",
      "All hail Odin!",
      "Lorem ipsum dolor sit amet..."
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    for (size_t i = 0; i < commands.size(); ++i)
    {
      BOOST_CHECK_MESSAGE((out[i].str() == "<INVALID COMMAND>\n"), "no. " << i + 1 << " command!");
    }
  }

  BOOST_AUTO_TEST_CASE(rightAddCommands)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "add 1 \"a\"",                            //regular data
      "add 123 \"b c d\"",                      //data with spaces
      "add 134 \"   c e  d\t\t\"",              //data with different WS
      "add 133 \"  \\\" c\\\" e\\\\  d\t\t\""   //data with quotes and backslashes in it
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    for (size_t i = 0; i < commands.size(); ++i)
    {
      BOOST_CHECK_MESSAGE(out[i].str().empty(), "no. " << i + 1 << " command!");
    }
  }

  BOOST_AUTO_TEST_CASE(wrongAddCommands)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "add",                              //empty command
      "add 134",                          //empty name
      "add 135 \"\"",                     //empty name but with quotemarks
      "add 136 \"",                       //just one quotemark
      "add 137 \"a",                      //no closing quotemarks
      "add 138 a\"",                      //no opening quotemarks
      "add 139 \"name\"extradata",        //extra data after quotemarks
      "add 139 \"name\"extra data",       //extra data with spaces after quotemarks
      "add 130 \"name\" extradata",       //extra data after quotemarks and a space
      "add 144 \"name\\\""                //escaped quotemark in the end
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    for (size_t i = 0; i < commands.size(); ++i)
    {
      BOOST_CHECK_MESSAGE((out[i].str() == "<INVALID COMMAND>\n"), "no. " << i + 1 << " command!");
    }
  }

  BOOST_AUTO_TEST_CASE(rightStoreCommands)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "add 1 \"a\"",          //add element to have current bm
      "store current bm1",    //regular check
      "store bm1 book-mark"   //bookmark with -
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    for (size_t i = 0; i < commands.size(); ++i)
    {
      BOOST_CHECK_MESSAGE(out[i].str().empty(), "no. " << i + 1 << " command!");
    }
  }

  BOOST_AUTO_TEST_CASE(wrongStoreCommands)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "add 1 \"y\"",                      //add element to have current
      "store",                            //empty command
      "store gone\% hihi",                //wrong first right second
      "store current",                    //empty second bookmark
      "store current mdmdmd\"fefef",      //wrong second bookmark
      "store current new-bm lalala",      //extra data after args
      "store current new-bm lalala dw",   //extra data with spaces after args
      "store not-a-bookmark new-bm"       //first bookmark is not present
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    for (size_t i = 1; i < commands.size() - 1; ++i)
    {
      BOOST_CHECK_MESSAGE((out[i].str() == "<INVALID COMMAND>\n"), "no. " << i + 1 << " command!");
    }

    BOOST_CHECK_MESSAGE((out[commands.size() - 1].str() == "<INVALID BOOKMARK>\n"), "no. " << commands.size() << " command!");
  }

  BOOST_AUTO_TEST_CASE(rightInsertCommands)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "add 1 \"a\"",                      //add element to have current bm
      "insert before current 2 \"b\"",    //insert before
      "insert after current 3 \"c\""      //insert after
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    for (size_t i = 0; i < commands.size(); ++i)
    {
      BOOST_CHECK_MESSAGE(out[i].str().empty(), "no. " << i + 1 << " command!");
    }
  }

  BOOST_AUTO_TEST_CASE(wrongInsertCommands)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "add 1 \"a\"",                     //add to have current
      "insert",                          //empty command
      "insert hello current 2 \"b\"",    //wrong insert position
      "insert before cur\% 2 \"b\"",     //wrong bookmark name
      "insert after current 1 x",        //wrong name
      "insert after current 1 \"b\"wdf", //extra data after command
      "insert after current 2 \"c\"w f", //extra data after command with spaces
      "insert after nab 3 \"c\""         //bookmark is not present
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    for (size_t i = 1; i < commands.size() - 1; ++i)
    {
      BOOST_CHECK_MESSAGE((out[i].str() == "<INVALID COMMAND>\n"), "no. " << i + 1 << " command!");
    }

    BOOST_CHECK_MESSAGE((out[commands.size() - 1].str() == "<INVALID BOOKMARK>\n"), "no. " << commands.size() << " command!");
  }

  BOOST_AUTO_TEST_CASE(rightDeleteCommands)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "add 1 \"a\"",        //add element to have current bm
      "delete current",     //regular check
      "add 2 \"b\"",        //add another element to have current bm
      "delete current"      //second check
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    for (size_t i = 0; i < commands.size(); ++i)
    {
      BOOST_CHECK_MESSAGE(out[i].str().empty(), "no. " << i + 1 << " command!");
    }
  }

  BOOST_AUTO_TEST_CASE(wrongDeleteCommands)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "delete current",             //deleting invalid bookmark
      "add 1 \"y\"",                //add element to have current
      "add 2 \"y\"",                //add second element
      "delete",                     //empty command
      "delete gone\%",              //wrong bookmark
      "delete current extradata",   //extra data
      "delete current extra data",  //extra data with spaces
      "delete not-a-bookmark"       //bookmark is not present
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    BOOST_CHECK_MESSAGE((out[0].str() == "<INVALID COMMAND>\n"), "no. 1 command!");

    for (size_t i = 3; i < commands.size() - 1; ++i)
    {
      BOOST_CHECK_MESSAGE((out[i].str() == "<INVALID COMMAND>\n"), "no. " << i + 1 << " command!");
    }

    BOOST_CHECK_MESSAGE((out[commands.size() - 1].str() == "<INVALID BOOKMARK>\n"), "no. " << commands.size() << " command!");
  }

  BOOST_AUTO_TEST_CASE(rightShowCommands)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "add 1 \"a\"",        //add element to have current bm
      "show current",       //regular check
      "store current bm-1", //add another element to have current bm
      "show bm-1"           //second check
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    BOOST_CHECK_MESSAGE(out[0].str().empty(), "no. 1 command!");
    BOOST_CHECK_MESSAGE(out[1].str() == "1 a\n", "no. 2 command!");
    BOOST_CHECK_MESSAGE(out[2].str().empty(), "no. 3 command!");
    BOOST_CHECK_MESSAGE(out[3].str() == "1 a\n", "no. 4 command!");
  }

  BOOST_AUTO_TEST_CASE(wrongShowCommands)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "show current",               //showing empty
      "add 1 \"y\"",                //add element to have current
      "show",                       //empty command
      "show gone\%",                //invalid bookmark name
      "show current extradata",     //extra data
      "show current extra data",    //extra data with spaces
      "show not-a-bookmark"         //bookmark is not present
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    BOOST_CHECK_MESSAGE((out[0].str() == "<EMPTY>\n"), "no. 1 command!");

    for (size_t i = 2; i < commands.size() - 1; ++i)
    {
      BOOST_CHECK_MESSAGE((out[i].str() == "<INVALID COMMAND>\n"), "no. " << i + 1 << " command!");
    }

    BOOST_CHECK_MESSAGE((out[commands.size() - 1].str() == "<INVALID BOOKMARK>\n"), "no. " << commands.size() << " command!");
  }

  BOOST_AUTO_TEST_CASE(rightMoveCommands)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "add 1 \"a\"",
      "add 2 \"b\"",
      "add 3 \"c\"",
      "add 4 \"d\"",
      "add 5 \"e\"",             //add some elements
      "move current +3",         //move in range forward
      "move current 10",         //move out of range forward
      "move current -3",         //move in range backward
      "move current -10",        //move out of range backward
      "move current last",       //move last
      "move current first"       //move first
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    for (size_t i = 0; i < commands.size(); ++i)
    {
      BOOST_CHECK_MESSAGE(out[i].str().empty(), "no. " << i + 1 << " command!");
    }
  }

  BOOST_AUTO_TEST_CASE(wrongMoveCommands)
  {
    PhonebookManager manager;

    std::vector<std::string> commands =
    {
      "add 1 \"a\"",
      "add 2 \"b\"",
      "add 3 \"c\"",
      "add 4 \"d\"",
      "add 5 \"e\"",                 //add some elements
      "move",                        //empty command
      "move gone\% 5",               //move by steps invalid bookmark name
      "move gone\% first",           //move by position invalid bookmark name
      "move current 5 da!",          //extra data
      "move current 5 n et!",        //extra data with spaces
      "move current first da!",      //extra data after move-to-pos
      "move current last n et!",     //extra data with spaces after move-to-pos
      "move current second",         //invalid step
      "move not-a-bookmark 5",       //bookmark not present by steps
      "move not-a-bookmark first"    //bookmark not present by position
    };

    std::vector<std::stringstream> in(commands.size());
    for (size_t i = 0; i < commands.size(); ++i)
    {
      in[i] << commands[i];
    }

    std::vector<std::stringstream> out(commands.size());

    for (size_t i = 0; i < commands.size(); ++i)
    {
      commands::execute_command command;
      in[i] >> command;
      command(manager, out[i]);
    }

    for (size_t i = 5; i < commands.size() - 3; ++i)
    {
      BOOST_CHECK_MESSAGE((out[i].str() == "<INVALID COMMAND>\n"), "no. " << i + 1 << " command!");
    }

    BOOST_CHECK_MESSAGE((out[commands.size() - 3].str() == "<INVALID STEP>\n"), "no. " << commands.size() - 2 << " command!");
    BOOST_CHECK_MESSAGE((out[commands.size() - 2].str() == "<INVALID BOOKMARK>\n"), "no. " << commands.size() - 1 << " command!");
    BOOST_CHECK_MESSAGE((out[commands.size() - 1].str() == "<INVALID BOOKMARK>\n"), "no. " << commands.size() << " command!");
  }

BOOST_AUTO_TEST_SUITE_END()
