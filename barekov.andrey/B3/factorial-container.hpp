#ifndef FACTORIAL_CONTAINER_HPP
#define FACTORIAL_CONTAINER_HPP

#include <iterator>

class FactorialContainer
{
public:
  class iterator : public std::iterator<std::bidirectional_iterator_tag, unsigned long long,
                                                 std::ptrdiff_t, unsigned long long, unsigned long long>
  {
  public:
    iterator();
    iterator(size_t index);

    unsigned long long operator *() const;

    iterator& operator ++();
    iterator& operator --();

    iterator operator ++(int);
    iterator operator --(int);

    bool operator ==(const iterator& rhs) const;
    bool operator !=(const iterator& rhs) const;

  private:
    unsigned long long value_;
    size_t index_;

    unsigned long long count(size_t index) const;
  };

  using const_iterator = iterator;
  using const_reverse_iterator = std::reverse_iterator<iterator>;

  using value_type = unsigned long long;
  using difference_type = std::ptrdiff_t;
  using size_type = size_t;

  bool operator ==(const FactorialContainer&) const noexcept;
  bool operator !=(const FactorialContainer&) const noexcept;

  iterator begin() const;
  iterator end() const;

  const_iterator cbegin() const;
  const_iterator cend() const;

  const_reverse_iterator rbegin() const;
  const_reverse_iterator rend() const;

  void swap(const FactorialContainer&) const noexcept;

  size_type size() const noexcept;
  size_type max_size() const noexcept;
  bool empty() const noexcept;

private:
  const size_type size_ = 10;
};

#endif
