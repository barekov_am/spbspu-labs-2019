#include "factorial-container.hpp"

//iterator methods:

FactorialContainer::iterator::iterator() :
  value_(1),
  index_(1)
{ }

FactorialContainer::iterator::iterator(size_t index) :
  value_(count(index)),
  index_(index)
{ }

unsigned long long FactorialContainer::iterator::operator *() const
{
  return value_;
}

FactorialContainer::iterator& FactorialContainer::iterator::operator ++()
{
  ++index_;
  value_ *= index_;
  
  return *this;
}

FactorialContainer::iterator& FactorialContainer::iterator::operator --()
{
  if (index_ > 1)
  {
    value_ /= index_;
    --index_;
  }

  return *this;
}

FactorialContainer::iterator FactorialContainer::iterator::operator ++(int)
{
  iterator temp = *this;
  ++(*this);

  return temp;
}

FactorialContainer::iterator FactorialContainer::iterator::operator --(int)
{
  iterator temp = *this;
  --(*this);

  return temp;
}

bool FactorialContainer::iterator::operator ==(const iterator& rhs) const
{
  return ((value_ == rhs.value_) && (index_ == rhs.index_));
}

bool FactorialContainer::iterator::operator !=(const iterator& rhs) const
{
  return !(*this == rhs);
}

unsigned long long FactorialContainer::iterator::count(size_t index) const
{
  if (index <= 1)
  {
    return 1;
  }
  else
  {
    return index * count(index - 1);
  }
}

//Container methods:

bool FactorialContainer::operator ==(const FactorialContainer&) const noexcept
{
  return true;
}

bool FactorialContainer::operator !=(const FactorialContainer&) const noexcept
{
  return false;
}

FactorialContainer::iterator FactorialContainer::begin() const
{
  return iterator(1);
}

FactorialContainer::iterator FactorialContainer::end() const
{
  return iterator(size_ + 1);
}

FactorialContainer::const_iterator FactorialContainer::cbegin() const
{
  return begin();
}

FactorialContainer::const_iterator FactorialContainer::cend() const
{
  return end();
}

FactorialContainer::const_reverse_iterator FactorialContainer::rbegin() const
{
  return const_reverse_iterator(begin());
}

FactorialContainer::const_reverse_iterator FactorialContainer::rend() const
{
  return const_reverse_iterator(end());
}

void FactorialContainer::swap(const FactorialContainer&) const noexcept
{ }

FactorialContainer::size_type FactorialContainer::size() const noexcept
{
  return size_;
}

FactorialContainer::size_type FactorialContainer::max_size() const noexcept
{
  return size_;
}

bool FactorialContainer::empty() const noexcept
{
  return false;
}
