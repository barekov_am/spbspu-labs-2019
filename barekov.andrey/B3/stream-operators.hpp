#ifndef STREAM_OPERATORS_HPP
#define STREAM_OPERATORS_HPP

#include <iosfwd>

#include "commands.hpp"

std::istream& operator >>(std::istream& input, commands::execute_command& command);

namespace commands
{
  struct command_t
  {
    const char* name;
    std::function<execute_command(std::istream&)> action;
  };

  struct bookmark_t
  {
    std::string& data;
  };

  struct name_t
  {
    std::string& data;
  };

  std::istream& operator >>(std::istream& input, insert_position_t& insertPosition);
  std::istream& operator >>(std::istream& input, move_position_t& movePosition);
  std::istream& operator >>(std::istream& input, bookmark_t&& bookmark);
  std::istream& operator >>(std::istream& input, name_t&& name);

  bookmark_t makeBookmark(std::string& str);
  name_t makeName(std::string& str);
}

#endif
