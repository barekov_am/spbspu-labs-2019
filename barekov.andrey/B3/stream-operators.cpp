#include "stream-operators.hpp"

#include <algorithm>
#include <istream>
#include <limits>

#include "manipulators.hpp"

const auto reportInvalid = [](PhonebookManager&, std::ostream& output){ output << "<INVALID COMMAND>\n"; };

std::istream& operator >>(std::istream& input, commands::execute_command& command)
{
  static const commands::command_t Commands[] =
  {
    {"add", &commands::parseAddArguments},
    {"store", &commands::parseStoreArguments},
    {"insert", &commands::parseInsertArguments},
    {"delete", &commands::parseDeleteArguments},
    {"show", &commands::parseShowArguments},
    {"move", &commands::parseMoveArguments}
  };

  std::istream::sentry sentry(input);

  if (input.peek() == std::char_traits<char>::eof())
  {
    command = [](PhonebookManager&, std::ostream&){};

    return input;
  }

  if (sentry)
  {
    std::string commandName;
    input >> manipulators::blank >> commandName;

    auto cmd = std::find_if(std::begin(Commands), std::end(Commands),
        [&](const commands::command_t& command) { return commandName == command.name; });

    if (cmd != std::end(Commands))
    {
      command = cmd->action(input);

      if (input.fail() && !input.eof())
      {
        input.clear();
        input.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      }

      if (input.bad() && !input.eof())
      {
        throw std::ios_base::failure("Data reading fatal error.");
      }
    }
    else
    {
      command = reportInvalid;
    }
  }
  else
  {
    command = reportInvalid;
  }

  return input;
}

std::istream& commands::operator >>(std::istream& input, commands::insert_position_t& pos)
{
  std::istream::sentry sentry(input);

  if (sentry)
  {
    static const insert_position_t InsertPositions[] =
    {
      {"before", insert_position::before},
      {"after", insert_position::after}
    };

    std::string position;
    input >> manipulators::blank >> position;

    auto insertPosition = std::find_if(std::begin(InsertPositions), std::end(InsertPositions),
        [&](const insert_position_t& insertPosition) { return position == insertPosition.name; });

    if (insertPosition == std::end(InsertPositions))
    {
      input.setstate(std::ios_base::failbit);

      return input;
    }

    pos = *insertPosition;
  }

  return input;
}

std::istream& commands::operator >>(std::istream& input, commands::move_position_t& pos)
{
  std::istream::sentry sentry(input);

  if (sentry)
  {
    static const move_position_t MovePositions[] =
    {
      {"first", move_position::first},
      {"last", move_position::last}
    };

    std::string position;
    input >> manipulators::blank >> position;

    auto movePosition = std::find_if(std::begin(MovePositions), std::end(MovePositions),
        [&](const move_position_t& movePosition) { return position == movePosition.name; });

    if (movePosition == std::end(MovePositions))
    {
      char* ptr = nullptr;
      int steps = std::strtol(position.c_str(), &ptr, 10);
      if (*ptr != 0x00)
      {
        input.setstate(std::ios_base::failbit);

        return input;
      }

      pos = {"", steps};
    }
    else
    {
      pos = *movePosition;
    }
  } 

  return input;
}

std::istream& commands::operator >>(std::istream& input, commands::bookmark_t&& bookmark)
{
  std::istream::sentry sentry(input);

  if (sentry)
  {
    input >> bookmark.data;

    auto it = std::find_if(bookmark.data.begin(), bookmark.data.end(),
        [&](auto character){ return (!std::isalnum(character) && !(character == '-')); });

    if (it != bookmark.data.end())
    {
      bookmark.data.clear();
      input.setstate(std::ios_base::failbit);

      return input;
    }
  }

  return input;
}

std::istream& commands::operator >>(std::istream& input, commands::name_t&& name)
{
  using traits = std::char_traits<char>;

  std::istream::sentry sentry(input);

  if (sentry)
  {
    auto eof = traits::eof();
    auto streambuf = input.rdbuf();
    auto c = streambuf->sgetc();

    std::string str{};

    if (!traits::eq(traits::to_char_type(c), '\"'))
    {
      input.setstate(std::ios_base::failbit);

      return input;
    }

    c = streambuf->snextc();

    while (!traits::eq_int_type(c, eof)
        && !traits::eq(traits::to_char_type(c), '\n')
        && !traits::eq(traits::to_char_type(c), '\"'))
    {
      if (traits::eq(traits::to_char_type(c), '\\'))
      {
        c = streambuf->snextc();
      }

      str += c;
      c = streambuf->snextc();
    }

    if (traits::eq_int_type(c, eof) || traits::eq(traits::to_char_type(c), '\n'))
    {
      input.setstate(std::ios_base::failbit);

      return input;
    }

    c = streambuf->snextc();

    name.data = str;
  }

  return input;
}

commands::bookmark_t commands::makeBookmark(std::string& str)
{
  return {str};
}

commands::name_t commands::makeName(std::string& str)
{
  return {str};
}
