#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

#include "collect-stats.hpp"

int main()
{
  CollectStats stats = std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), CollectStats());

  if (!std::cin.eof() && std::cin.fail())
  {
    std::cerr << "Fail while reading data.";

    return 1;
  }

  if (stats.empty)
  {
    std::cout << "No Data\n";
  }
  else
  {
    const char* firstEqualToLast = stats.firstEqualToLast ? "yes\n" : "no\n";
    std::cout << "Max: " << stats.max << "\nMin: " << stats.min
              << "\nMean: " << stats.mean
              << "\nPositive: " << stats.posCount << "\nNegative: " << stats.negCount
              << "\nOdd Sum: " << stats.oddSum << "\nEven Sum: " << stats.evenSum
              << "\nFirst/Last Equal: " << firstEqualToLast;
  }

  return 0;
}
