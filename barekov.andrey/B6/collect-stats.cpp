#include "collect-stats.hpp"

#include <algorithm>
#include <limits>

CollectStats::CollectStats() :
  empty(true),
  max(std::numeric_limits<int>::min()),
  min(std::numeric_limits<int>::max()),
  mean(0),
  posCount(0),
  negCount(0),
  oddSum(0),
  evenSum(0),
  firstEqualToLast(false),
  first(0),
  numCount(0)
{ }

void CollectStats::operator ()(int num)
{
  if (empty)
  {
    first = num;
    empty = false;
  }

  max = std::max(num, max);
  min = std::min(num, min);

  if (num != 0)
  {
    ++(num > 0 ? posCount : negCount);
  }

  (num % 2 == 0 ? evenSum : oddSum) += num;

  mean = static_cast<float>(oddSum + evenSum) / (++numCount);

  firstEqualToLast = (first == num);
}
