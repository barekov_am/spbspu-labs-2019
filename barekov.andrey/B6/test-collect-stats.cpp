#include <boost/test/unit_test.hpp>

#include "collect-stats.hpp"

#include <boost/test/floating_point_comparison.hpp>
#include <vector>

const double Epsilon = 0.01;

BOOST_AUTO_TEST_SUITE(CollectStatsTestSuite)

  BOOST_AUTO_TEST_CASE(emptyInput)
  {
    std::vector<int> vector{};

    CollectStats stats = std::for_each(vector.begin(), vector.end(), CollectStats());

    BOOST_CHECK_MESSAGE(stats.empty, "Empty input check fail!");
  }

  BOOST_AUTO_TEST_CASE(oneElementVector)
  {
    std::vector<int> vector{1};

    CollectStats stats = std::for_each(vector.begin(), vector.end(), CollectStats());

    BOOST_CHECK_MESSAGE(!stats.empty, "One element check fail!");
    BOOST_CHECK_MESSAGE(stats.max == 1, "One element maximum check fail!");
    BOOST_CHECK_MESSAGE(stats.min == 1, "One element minimum check fail!");
    BOOST_CHECK_MESSAGE(stats.mean == 1, "One element arithmetic mean check fail!");
    BOOST_CHECK_MESSAGE(stats.posCount == 1, "One element positive count check fail!");
    BOOST_CHECK_MESSAGE(stats.negCount == 0, "One element negative count check fail!");
    BOOST_CHECK_MESSAGE(stats.oddSum == 1, "One element odd sum check fail!");
    BOOST_CHECK_MESSAGE(stats.evenSum == 0, "One element even sum check fail!");
    BOOST_CHECK_MESSAGE(stats.firstEqualToLast, "One element FirstEqualToLast check fail!");
  }

  BOOST_AUTO_TEST_CASE(simpleVector)
  {
    std::vector<int> vector{1, 3, 5, 7, 9, -2, -4, -6, -8, 0};

    CollectStats stats = std::for_each(vector.begin(), vector.end(), CollectStats());

    BOOST_CHECK_MESSAGE(!stats.empty, "Simple vector check fail!");
    BOOST_CHECK_MESSAGE(stats.max == 9, "Simple vector maximum check fail!");
    BOOST_CHECK_MESSAGE(stats.min == -8, "Simple vector minimum check fail!");
    BOOST_CHECK_CLOSE(stats.mean, 0.5, Epsilon);
    BOOST_CHECK_MESSAGE(stats.posCount == 5, "Simple vector positive count check fail!");
    BOOST_CHECK_MESSAGE(stats.negCount == 4, "Simple vector negative count check fail!");
    BOOST_CHECK_MESSAGE(stats.oddSum == 25, "Simple vector odd sum check fail!");
    BOOST_CHECK_MESSAGE(stats.evenSum == -20, "Simple vector even sum check fail!");
    BOOST_CHECK_MESSAGE(!stats.firstEqualToLast, "Simple vector FirstEqualToLast check fail!");
  }

  BOOST_AUTO_TEST_CASE(complexVector)
  {
    std::vector<int> vector{13, -22, 0, 0, 0, 3, 78, -232, -1748, 202020, 13};

    CollectStats stats = std::for_each(vector.begin(), vector.end(), CollectStats());

    BOOST_CHECK_MESSAGE(!stats.empty, "Complex vector check fail!");
    BOOST_CHECK_MESSAGE(stats.max == 202020, "Complex vector maximum check fail!");
    BOOST_CHECK_MESSAGE(stats.min == -1748, "Complex vector minimum check fail!");
    BOOST_CHECK_CLOSE(stats.mean, 18193.18, Epsilon);
    BOOST_CHECK_MESSAGE(stats.posCount == 5, "Complex vector positive count check fail!");
    BOOST_CHECK_MESSAGE(stats.negCount == 3, "Complex vector negative count check fail!");
    BOOST_CHECK_MESSAGE(stats.oddSum == 29, "Complex vector odd sum check fail!");
    BOOST_CHECK_MESSAGE(stats.evenSum == 200096, "Complex vector even sum check fail!");
    BOOST_CHECK_MESSAGE(stats.firstEqualToLast, "Complex vector FirstEqualToLast check fail!");
  }

BOOST_AUTO_TEST_SUITE_END()
