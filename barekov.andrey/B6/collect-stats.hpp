#ifndef COLLECT_STATS_HPP
#define COLLECT_STATS_HPP

#include <cstddef>

struct CollectStats
{
  bool empty;

  int max;
  int min;
  double mean;
  size_t posCount;
  size_t negCount;
  long oddSum;
  long evenSum;
  bool firstEqualToLast;

  CollectStats();

  void operator ()(int num);
  
private:
  int first;
  size_t numCount;
};

#endif
