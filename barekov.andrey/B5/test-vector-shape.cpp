#include <boost/test/unit_test.hpp>

#include "vector-shape.hpp"

BOOST_AUTO_TEST_SUITE(VectorShapeTestSuite)

  BOOST_AUTO_TEST_CASE(correctSquares)
  {
    std::vector<VectorShape> shapes =
    {
      {shape{{1, 1}, {1, 3}, {3, 3}, {3, 1}}},
      {shape{{-10, -10}, {-10, 10}, {10, 10}, {10, -10}}}    //Just regular squares
    };

    for (const auto& i : shapes)
    {
      BOOST_CHECK_MESSAGE(i.isSquare(), "isSquare check fail!");
    }
  }

  BOOST_AUTO_TEST_CASE(incorrectSquares)
  {
    std::vector<VectorShape> shapes =
    {
      {shape{{1, 2}, {1, 3}, {3, 3}, {3, 1}}},               //Not a rectangle or square
      {shape{{-10, -10}, {-10, 20}, {10, 20}, {10, -10}}},   //Rectangle, but not a square
      {shape{{-1, 1}, {-2, 3}, {-3, 4}}}                     //Not even a tetragon
    };

    for (const auto& i : shapes)
    {
      BOOST_CHECK_MESSAGE(!i.isSquare(), "isSquare check fail!");
    }
  }

  BOOST_AUTO_TEST_CASE(correctRectangles)
  {
    std::vector<VectorShape> shapes =
    {
      {shape{{1, 1}, {1, 3}, {3, 3}, {3, 1}}},
      {shape{{-10, -10}, {-10, 20}, {10, 20}, {10, -10}}},
      {shape{{-10, -10}, {-10, 10}, {10, 10}, {10, -10}}}   //Just regular rectangles
    };

    for (const auto& i : shapes)
    {
      BOOST_CHECK_MESSAGE(i.isRectangle(), "isRectangle check fail!");
    }
  }

  BOOST_AUTO_TEST_CASE(incorrectRectangles)
  {
    std::vector<VectorShape> shapes =
    {
      {shape{{1, 2}, {1, 3}, {3, 3}, {3, 1}}},               //Not a rectangle
      {shape{{-10, -10}, {-10, 10}, {10, 10}}}               //Not even a tetragon
    };

    for (const auto& i : shapes)
    {
      BOOST_CHECK_MESSAGE(!i.isRectangle(), "isRectangle check fail!");
    }
  }

BOOST_AUTO_TEST_SUITE_END()
