#include <boost/test/unit_test.hpp>

#include "algorithms.hpp"
#include "ctype-facets.hpp"

const size_t TetragonSides = 4;

const char Delim = ';';
const std::pair<char, char> Brackets('(', ')');

const auto Compare = [](const VectorShape& lhs, const VectorShape& rhs)
                     {
                       if ((lhs.vertices.size() == TetragonSides) && (rhs.vertices.size() == TetragonSides))
                       {
                         if (lhs.isSquare())
                         {
                           return true;
                         }

                         if (rhs.isSquare())
                         {
                           return false;
                         }

                         return !rhs.isRectangle();
                       }

                       return lhs.vertices.size() < rhs.vertices.size();
                     };

BOOST_AUTO_TEST_SUITE(AlgorithmsTestSuite)

  //Empty input test.
  BOOST_AUTO_TEST_CASE(emptyInput)
  {
    std::list<VectorShape> shapes;

    std::stringstream in("");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::fill(shapes, in);

    std::stringstream out;
    boost::io::ios_locale_saver saver_out(out);
    out.imbue(std::locale(out.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::print(shapes, out);

    BOOST_CHECK_MESSAGE(out.str().empty(), "Empty input check fail!");
  }

  //Basic data parsing tests.
  BOOST_AUTO_TEST_CASE(oneShape)
  {
    std::list<VectorShape> shapes;

    std::stringstream in("3 (1; 3) (4; 5) (6; 7)");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::fill(shapes, in);

    std::stringstream out;
    boost::io::ios_locale_saver saver_out(out);
    out.imbue(std::locale(out.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::print(shapes, out);

    BOOST_CHECK_MESSAGE(out.str() == "3 (1;3) (4;5) (6;7) \n", "OneShape check fail!");
  }

  BOOST_AUTO_TEST_CASE(oneShapeWithVariousWhitespaces)
  {
    std::list<VectorShape> shapes;

    std::stringstream in("\t\t3 \t\t(1\t; 3\t \t)    \t  (4\t; 5)          (       6; \t\t7\t\t\t)");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::fill(shapes, in);

    std::stringstream out;
    boost::io::ios_locale_saver saver_out(out);
    out.imbue(std::locale(out.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::print(shapes, out);

    BOOST_CHECK_MESSAGE(out.str() == "3 (1;3) (4;5) (6;7) \n", "Various Whitespace check fail!");
  }

  BOOST_AUTO_TEST_CASE(ExtraNewlinesBetweenPolygons)
  {
    std::list<VectorShape> shapes;

    std::stringstream in("3 (1; 3) (4; 5) (6; 7)\n"
                         "\n"
                         "\n"
                         "3 (1; 1) (4; 1) (1; 2)");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::fill(shapes, in);

    std::stringstream out;
    boost::io::ios_locale_saver saver_out(out);
    out.imbue(std::locale(out.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::print(shapes, out);

    BOOST_CHECK_MESSAGE(out.str() == "3 (1;3) (4;5) (6;7) \n3 (1;1) (4;1) (1;2) \n", "ExtraNL check fail!");
  }

  //Exception tests.
  BOOST_AUTO_TEST_CASE(NumberOfPointsFails)
  {
    std::list<VectorShape> shapes;

    std::vector<std::string> inputs =
    {
      "2 (1;2) (3;4)",
      "5 (1;3) (4;5) (6;7)",
      "3 (1;3) (4;5) (6;7) (8;9)"
    };

    std::vector<std::stringstream> in(inputs.size());

    for (size_t i = 0; i < inputs.size(); ++i)
    {
      in[i].imbue(std::locale(in[i].getloc(), new ctype_facets::csv_point(Delim, Brackets)));
      in[i] << inputs[i];
    }

    for (auto& i : in)
    {
      BOOST_CHECK_THROW(algorithms::fill(shapes, i), std::exception);
    }
  }

  BOOST_AUTO_TEST_CASE(IncorrectDataFails)
  {
    std::list<VectorShape> shapes;

    std::vector<std::string> inputs =
    {
      "3 (2;2) (2;2) (  )",
      "3 (2;2) (2;2) (3)",
      "3 (fe;2) (2;2) (3;fe)",
      "5 (1f3} (4;5) {6;7)",
      "3 (1;3) (4;5) {{{{{{{}}}}}}}(6;7) (8;9)",
      "Lorem ipsum dolor",
      "3 3?#?#?#?#?#?#",
      "3 {1;2} {1;4) {2;3)"
    };

    std::vector<std::stringstream> in(inputs.size());

    for (size_t i = 0; i < inputs.size(); ++i)
    {
      in[i].imbue(std::locale(in[i].getloc(), new ctype_facets::csv_point(Delim, Brackets)));
      in[i] << inputs[i];
    }

    for (auto& i : in)
    {
      BOOST_CHECK_THROW(algorithms::fill(shapes, i), std::exception);
    }
  }

  BOOST_AUTO_TEST_CASE(IncorrectWhitespaceFails)
  {
    std::list<VectorShape> shapes;

    std::vector<std::string> inputs =
    {
      "3 (2;2) (2;2)\n (3;3)",
      "3 (2;2) (\n2;2) (3;3)",
      "5 (1;3) (4;5) (6\n;7)",
      "3 (1;3) (4;5) (6;7\n) (8;9)",
      "3\n (1;3) (4;5) (6;7) (8;9)"
    };

    std::vector<std::stringstream> in(inputs.size());

    for (size_t i = 0; i < inputs.size(); ++i)
    {
      in[i].imbue(std::locale(in[i].getloc(), new ctype_facets::csv_point(Delim, Brackets)));
      in[i] << inputs[i];
    }

    for (auto& i : in)
    {
      BOOST_CHECK_THROW(algorithms::fill(shapes, i), std::exception);
    }
  }

  //Counting algorithms tests
  BOOST_AUTO_TEST_CASE(CountingVertices)
  {
    std::list<VectorShape> shapes;

    std::stringstream in("3 (1; 3) (4; 5) (6; 7)\n"
                         "3 (1; 1) (4; 1) (1; 2)");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::fill(shapes, in);

    BOOST_CHECK_MESSAGE(algorithms::countVertices(shapes) == 6, "CountVertices check fail!");
  }

  BOOST_AUTO_TEST_CASE(CountingNGons)
  {
    std::list<VectorShape> shapes;

    std::stringstream in("3 (1; 3) (4; 5) (6; 7)\n"
                         "5 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122)\n"
                         "5 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122)\n"
                         "5 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122)\n"
                         "4 (1; 4) (4; 2) (6; 7) (1; 1)\n"
                         "6 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122) (0; 0)\n"
                         "6 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122) (0; 0)\n"
                         "6 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122) (0; 0)\n"
                         "6 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122) (0; 0)\n"
                         "4 (1; 1) (4; 1) (1; 2) (0; 0)");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::fill(shapes, in);

    for (size_t i = 3; i < 7; ++i)
    {
      BOOST_CHECK_MESSAGE(algorithms::countNGons(shapes, i) == i - 2, "CountNGons check fail!");
    }

    BOOST_CHECK_MESSAGE(algorithms::countNGons(shapes, 7) == 0, "CountNGons check fail!");
  }

  BOOST_AUTO_TEST_CASE(CountingRectanglesAndSquares)
  {
    std::list<VectorShape> shapes;

    std::stringstream in("4 (1; 1) (1; 2) (2; 2) (2; 1)\n"
                         "4 (1; 1) (1; 3) (2; 3) (2; 1)");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::fill(shapes, in);

    BOOST_CHECK_MESSAGE(algorithms::countSquares(shapes) == 1, "CountTetragons check fail!");
    BOOST_CHECK_MESSAGE(algorithms::countRectangles(shapes) == 2, "CountTetragons check fail!");
  }

  //Deleting NGons
  BOOST_AUTO_TEST_CASE(DeletingNGons)
  {
    std::list<VectorShape> shapes;

    std::stringstream in("3 (1; 3) (4; 5) (6; 7)\n"
                         "5 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122)\n"
                         "5 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122)\n"
                         "5 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122)\n"
                         "4 (1; 4) (4; 2) (6; 7) (1; 1)\n"
                         "6 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122) (0; 0)\n"
                         "6 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122) (0; 0)\n"
                         "6 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122) (0; 0)\n"
                         "6 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122) (0; 0)\n"
                         "4 (1; 1) (4; 1) (1; 2) (0; 0)");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::fill(shapes, in);

    algorithms::deleteNGons(shapes, 6);

    std::stringstream out;
    boost::io::ios_locale_saver saver_out(out);
    out.imbue(std::locale(out.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::print(shapes, out);

    BOOST_CHECK_MESSAGE(out.str() == "3 (1;3) (4;5) (6;7) \n"
                                     "5 (1;4) (4;2) (6;7) (1;1) (0;-122) \n"
                                     "5 (1;4) (4;2) (6;7) (1;1) (0;-122) \n"
                                     "5 (1;4) (4;2) (6;7) (1;1) (0;-122) \n"
                                     "4 (1;4) (4;2) (6;7) (1;1) \n"
                                     "4 (1;1) (4;1) (1;2) (0;0) \n", "Deleting Hexagons check fail!");

    algorithms::deleteNGons(shapes, 5);

    std::stringstream out2;
    boost::io::ios_locale_saver saver_out2(out2);
    out2.imbue(std::locale(out2.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::print(shapes, out2);

    BOOST_CHECK_MESSAGE(out2.str() == "3 (1;3) (4;5) (6;7) \n"
                                      "4 (1;4) (4;2) (6;7) (1;1) \n"
                                      "4 (1;1) (4;1) (1;2) (0;0) \n", "Deleting Pentagons check fail!");

  }

  //Sorting test. 
  BOOST_AUTO_TEST_CASE(Sort)
  {
    std::list<VectorShape> shapes;

    std::stringstream in("3 (1; 3) (4; 5) (6; 7)\n"
                         "5 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122)\n"
                         "4 (1; 1) (1; 2) (2; 2) (2; 1)\n"
                         "6 (1; 4) (4; 2) (6; 7) (1; 1) (0; -122) (0; 0)\n"
                         "4 (1; 1) (1; 3) (2; 3) (3; 1)");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::fill(shapes, in);

    shapes.sort(Compare);

    std::stringstream out;
    boost::io::ios_locale_saver saver_out(out);
    out.imbue(std::locale(out.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

    algorithms::print(shapes, out);

    BOOST_CHECK_MESSAGE(out.str() == "3 (1;3) (4;5) (6;7) \n"
                                     "4 (1;1) (1;2) (2;2) (2;1) \n"
                                     "4 (1;1) (1;3) (2;3) (3;1) \n"
                                     "5 (1;4) (4;2) (6;7) (1;1) (0;-122) \n"
                                     "6 (1;4) (4;2) (6;7) (1;1) (0;-122) (0;0) \n", "Sorting check fail!");
  }

  //Custom point input tests.
  BOOST_AUTO_TEST_CASE(customPointInput)
  {
    std::list<VectorShape> shapes;

    char delim = ',';
    std::pair<char, char> brackets('{', '}');

    std::stringstream in("3 {1, 3} {4, 5} {6, 7}");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_point(delim, brackets)));

    algorithms::fill(shapes, in);

    std::stringstream out;
    boost::io::ios_locale_saver saver_out(out);
    out.imbue(std::locale(out.getloc(), new ctype_facets::csv_point(delim, brackets)));

    algorithms::print(shapes, out);

    BOOST_CHECK_MESSAGE(out.str() == "3 {1,3} {4,5} {6,7} \n", "Custom point input check fail!");
  }

BOOST_AUTO_TEST_SUITE_END()
