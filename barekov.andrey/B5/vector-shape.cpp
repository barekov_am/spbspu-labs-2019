#include "vector-shape.hpp"

#include <cmath>
#include <sstream>
#include <utility>

const size_t MinNumberOfPoints = 3;
const size_t TetragonSides = 4;

bool VectorShape::isSquare() const
{
  if (!isRectangle())
  {
    return false;
  }

  const std::pair<int, int> diag1 = std::make_pair((vertices[0].x - vertices[2].x), (vertices[0].y - vertices[2].y));
  const std::pair<int, int> diag2 = std::make_pair((vertices[1].x - vertices[3].x), (vertices[1].y - vertices[3].y));

  return (diag1.first * diag2.first + diag1.second * diag2.second) == 0;
}

bool VectorShape::isRectangle() const
{
  if (vertices.size() != TetragonSides)
  {
    return false;
  }

  const std::pair<int, int> side1 = std::make_pair((vertices[0].x - vertices[1].x), (vertices[0].y - vertices[1].y));
  const std::pair<int, int> side2 = std::make_pair((vertices[0].x - vertices[3].x), (vertices[0].y - vertices[3].y));
  const std::pair<int, int> side3 = std::make_pair((vertices[1].x - vertices[2].x), (vertices[1].y - vertices[2].y));

  const bool adjacentSidesPerpendicular = (side1.first * side2.first + side1.second * side2.second) == 0;
  const bool oppositeSidesEqual = !std::islessgreater(std::hypot(side2.first, side2.second), std::hypot(side3.first, side3.second));

  return adjacentSidesPerpendicular && oppositeSidesEqual;
}

std::istream& operator >>(std::istream& input, VectorShape& polygon)
{
  size_t size = 0;
  input >> std::ws >> size;

  if (!input || (size < MinNumberOfPoints))
  {
    input.setstate(std::ios_base::failbit);

    return input;
  }

  shape tmp{std::istream_iterator<point_t>(input), std::istream_iterator<point_t>()};

  if (tmp.size() != size)
  {
    input.setstate(std::ios_base::failbit);
    input.clear(input.rdstate() & ~std::ios_base::eofbit);

    return input;
  }

  polygon.vertices.swap(tmp);

  input.clear(input.rdstate() & ~std::ios_base::failbit);

  return input;
}

std::ostream& operator <<(std::ostream& output, const VectorShape& polygon)
{
  output << polygon.vertices.size() << " ";

  std::copy(polygon.vertices.begin(), polygon.vertices.end(), std::ostream_iterator<point_t>(output, " "));

  return output;
}
