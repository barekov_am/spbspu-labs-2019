#ifndef VECTOR_SHAPE_HPP
#define VECTOR_SHAPE_HPP

#include <iostream>
#include <iterator>
#include <vector>

#include "point.hpp"

using shape = std::vector<point_t>;

struct VectorShape
{
  shape vertices;

  bool isSquare() const;
  bool isRectangle() const;
};

std::istream& operator >>(std::istream& input, VectorShape& polygon);
std::ostream& operator <<(std::ostream& output, const VectorShape& polygon);

#endif
