#ifndef ALGORITHMS_HPP
#define ALGORITHMS_HPP

#include <algorithm>
#include <boost/io/ios_state.hpp>
#include <functional>
#include <iostream>
#include <iterator>
#include <memory>
#include <numeric>
#include <utility>

#include "vector-shape.hpp"

namespace algorithms
{
  using shape_compare = std::function<bool(const VectorShape&, const VectorShape&)>;

  template <typename T>
  void fill(T& container, std::istream& input)
  {
    using value_type = typename T::value_type;

    boost::io::ios_flags_saver saver(input);

    std::copy(std::istream_iterator<value_type>(input >> std::noskipws),
              std::istream_iterator<value_type>(),
              std::back_inserter(container));

    if (!input.good() && !input.eof())
    {
      throw std::invalid_argument("Fail while reading data.");
    }
  }

  template <typename T>
  size_t countVertices(const T& container)
  {
    return std::accumulate(container.begin(), container.end(), 0,
        [](size_t lhs, const auto& rhs){ return lhs + rhs.vertices.size(); });
  }

  template <typename T>
  size_t countNGons(const T& container, size_t n)
  {
    return std::count_if(container.begin(), container.end(),
        [=](const auto& element){ return element.vertices.size() == n; });
  }

  template <typename T>
  size_t countSquares(const T& container)
  {
    return std::count_if(container.begin(), container.end(),
        [](const auto& element){ return element.isSquare(); });
  }

  template <typename T>
  size_t countRectangles(const T& container)
  {
    return std::count_if(container.begin(), container.end(),
        [](const auto& element){ return element.isRectangle(); });
  }

  template <typename T>
  void deleteNGons(T& container, size_t n)
  {
    container.erase(std::remove_if(container.begin(), container.end(),
        [&](const auto& element){ return element.vertices.size() == n; }), container.end());
  }

  template <typename T>
  std::shared_ptr<shape> createPointVector(const T& container)
  {
    std::shared_ptr<shape> pointVector = std::make_shared<shape>(container.size());

    std::transform(container.begin(), container.end(), pointVector->begin(),
        [](auto& element){ return element.vertices.front(); });

    return pointVector;
  }

  template <typename T>
  void print(const T& container, std::ostream& output)
  {
    using value_type = typename T::value_type;

    std::copy(container.begin(), container.end(), std::ostream_iterator<value_type>(output, "\n"));
  }
}

#endif
