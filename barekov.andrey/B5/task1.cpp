#include <algorithm>
#include <iostream>
#include <iterator>
#include <unordered_set>

void task1(std::istream& input, std::ostream& output)
{
  std::unordered_set<std::string> wordList{std::istream_iterator<std::string>(input), std::istream_iterator<std::string>()};

  if (!input.good() && !input.eof())
  {
    throw std::ios_base::failure("Fail while reading data.");
  }

  std::copy(wordList.begin(), wordList.end(), std::ostream_iterator<std::string>(output, "\n"));
}
