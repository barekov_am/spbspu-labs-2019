#include <list>

#include "algorithms.hpp"
#include "ctype-facets.hpp"

const size_t TetragonSides = 4;

const size_t NGonsToCount = 3;
const size_t NGonsToDelete = 5;

const char Delim = ';';
const std::pair<char, char> Brackets('(', ')');

const auto Compare = [](const VectorShape& lhs, const VectorShape& rhs)
                     {
                       if ((lhs.vertices.size() == TetragonSides) && (rhs.vertices.size() == TetragonSides))
                       {
                         if (lhs.isSquare())
                         {
                           return true;
                         }

                         if (rhs.isSquare())
                         {
                           return false;
                         }

                         return !rhs.isRectangle();
                       }

                       return lhs.vertices.size() < rhs.vertices.size();
                     };

void task2(std::istream& input, std::ostream& output)
{
  std::list<VectorShape> shapes;

  boost::io::ios_locale_saver saver_in(input);
  boost::io::ios_locale_saver saver_out(output);

  input.imbue(std::locale(input.getloc(), new ctype_facets::csv_point(Delim, Brackets)));
  output.imbue(std::locale(output.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

  algorithms::fill(shapes, input);

  output << "Vertices: " << algorithms::countVertices(shapes) << "\n"
         << "Triangles: " << algorithms::countNGons(shapes, NGonsToCount) << "\n"
         << "Squares: " << algorithms::countSquares(shapes) << "\n"
         << "Rectangles: " << algorithms::countRectangles(shapes) << "\n";

  algorithms::deleteNGons(shapes, NGonsToDelete);

  std::shared_ptr<shape> pointVector = algorithms::createPointVector(shapes);

  output << "Points: ";
  std::copy(pointVector->begin(), pointVector->end(), std::ostream_iterator<point_t>(output, " "));

  shapes.sort(Compare);

  output << "\nShapes:\n";
  algorithms::print(shapes, output);
}
