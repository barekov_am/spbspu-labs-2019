#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <iterator>
#include <list>

void task1(std::istream& input, std::ostream& output)
{
  std::list<double> list{std::istream_iterator<double>(input), std::istream_iterator<double>()};

  if (!input.eof() && input.fail())
  {
    throw std::invalid_argument("Fail while reading data.");
  }

  auto functor = std::bind(std::multiplies<double>(), M_PI, std::placeholders::_1);

  std::transform(list.begin(), list.end(),
                 std::ostream_iterator<double>(output, " "), functor);
  output << "\n";
}
