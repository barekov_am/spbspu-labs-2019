#include "circle.hpp"

#include <ostream>

shapes::Circle::Circle(const point_t& point) :
  Shape(point)
{ }

void shapes::Circle::draw(std::ostream& output) const
{
  output << name << " " << centre_;
}
