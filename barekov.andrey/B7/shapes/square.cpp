#include "square.hpp"

#include <ostream>

shapes::Square::Square(const point_t& point) :
  Shape(point)
{ }

void shapes::Square::draw(std::ostream& output) const
{
  output << name << " " << centre_;
}
