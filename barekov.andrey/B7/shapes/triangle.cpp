#include "triangle.hpp"

#include <ostream>

shapes::Triangle::Triangle(const point_t& point) :
  Shape(point)
{ }

void shapes::Triangle::draw(std::ostream& output) const
{
  output << name << " " << centre_;
}
