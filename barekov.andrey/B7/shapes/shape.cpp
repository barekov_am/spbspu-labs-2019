#include "shape.hpp"

#include <algorithm>
#include <functional>
#include <iostream>

#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"

static shapes::Shape::shape_ptr createCirclePtr(const point_t& point)
{
  return std::make_shared<shapes::Circle>(point);
}

static shapes::Shape::shape_ptr createSquarePtr(const point_t& point)
{
  return std::make_shared<shapes::Square>(point);
}

static shapes::Shape::shape_ptr createTrianglePtr(const point_t& point)
{
  return std::make_shared<shapes::Triangle>(point);
}

std::istream& shapes::operator >>(std::istream& input, Shape::shape_ptr& shape)
{
  struct make_shape_pointer_t
  {
    const char* name;
    Shape::shape_ptr (*action)(const point_t& point);
  };

  point_t point{};

  static constexpr make_shape_pointer_t Shapes[] =
  {
    {Circle::name, &createCirclePtr},
    {Square::name, &createSquarePtr},
    {Triangle::name, &createTrianglePtr}
  };

  input >> std::ws;
  std::string shapeName;

  while (std::isalpha(input.peek()))
  {
    char letter = 0;
    input >> letter;
    shapeName += letter;
  }

  auto makeShapePointer = std::find_if(std::begin(Shapes), std::end(Shapes),
      [&](const make_shape_pointer_t& makeShapePointer) { return shapeName == makeShapePointer.name; });

  if (makeShapePointer == std::end(Shapes))
  {
    input.setstate(std::ios_base::failbit);

    return input;
  }

  input >> point;
  shape = makeShapePointer->action(point);

  return input;
}

std::ostream& shapes::operator <<(std::ostream& output, const Shape::shape_ptr& shape)
{
  shape->draw(output);

  return output;
}
