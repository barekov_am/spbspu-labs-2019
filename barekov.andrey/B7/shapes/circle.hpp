#ifndef B7_CIRCLE_HPP
#define B7_CIRCLE_HPP

#include "shape.hpp"

namespace shapes
{
  class Circle : public Shape
  {
  public:
    static constexpr auto name = "CIRCLE"; 

    Circle(const point_t& point);

    void draw(std::ostream& output) const override;
  };
}

#endif
