#ifndef B7_SQUARE_HPP
#define B7_SQUARE_HPP

#include "shape.hpp"

namespace shapes
{
  class Square : public Shape
  {
  public:
    static constexpr auto name = "SQUARE";

    Square(const point_t& point);

    void draw(std::ostream& output) const override;
  };
}

#endif
