#ifndef B7_SHAPE_HPP
#define B7_SHAPE_HPP

#include <memory>

#include "point.hpp"

namespace shapes
{
  class Shape
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;

    Shape(const point_t& point) : centre_(point) { }

    virtual ~Shape() = default;

    virtual void draw(std::ostream&) const = 0;

    bool isMoreLeft(const Shape& rhs) const { return centre_.x < rhs.centre_.x; }
    bool isUpper(const Shape& rhs) const { return centre_.y > rhs.centre_.y; }

  protected:
    point_t centre_;
  };

  std::istream& operator >>(std::istream& input, Shape::shape_ptr& shape);
  std::ostream& operator <<(std::ostream& output, const Shape::shape_ptr& shape);
}

#endif
