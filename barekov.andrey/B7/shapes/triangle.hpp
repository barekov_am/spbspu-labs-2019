#ifndef B7_TRIANGLE_HPP
#define B7_TRIANGLE_HPP

#include "shape.hpp"

namespace shapes
{
  class Triangle : public Shape
  {
  public:
    static constexpr auto name = "TRIANGLE"; 

    Triangle(const point_t& point);

    void draw(std::ostream& output) const override;
  };
}

#endif
