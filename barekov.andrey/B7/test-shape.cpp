#include <boost/test/unit_test.hpp>

#include "shapes/shape.hpp"

#include "ctype-facets.hpp"
#include "shapes/circle.hpp"
#include "shapes/square.hpp"
#include "shapes/triangle.hpp"

BOOST_AUTO_TEST_SUITE(ShapeTestSuite)
 
  BOOST_AUTO_TEST_CASE(isMoreLeftCheck)
  {
    shapes::Circle circle({0, 0});
    shapes::Square square({5, 0});
    shapes::Triangle triangle({5, 0});

    BOOST_CHECK_MESSAGE(circle.isMoreLeft(square), "IsMoreLeft check fail!");
    BOOST_CHECK_MESSAGE(!triangle.isMoreLeft(circle), "IsMoreLeft check fail!");
    BOOST_CHECK_MESSAGE(!square.isMoreLeft(triangle), "IsMoreLeft check fail!");
  }

  BOOST_AUTO_TEST_CASE(isUpperCheck)
  {
    shapes::Circle circle({0, 5});
    shapes::Square square({0, 0});
    shapes::Triangle triangle({0, 0});

    BOOST_CHECK_MESSAGE(circle.isUpper(square), "IsUpper check fail!");
    BOOST_CHECK_MESSAGE(!triangle.isUpper(circle), "IsUpper check fail!");
    BOOST_CHECK_MESSAGE(!square.isUpper(triangle), "IsUpper check fail!");
  }

  BOOST_AUTO_TEST_CASE(streamOperators)
  {
    const char delim = ';';
    const std::pair<char, char> brackets('(', ')');

    std::stringstream circleIStream("CIRCLE (1;3)");
    std::stringstream squareIStream("SQUARE(1;3)");
    std::stringstream triangleIStream("\tTRIANGLE\t (   \t1 \t;\t\t3   )");

    circleIStream.imbue(std::locale(circleIStream.getloc(), new ctype_facets::csv_point(delim, brackets)));
    squareIStream.imbue(std::locale(squareIStream.getloc(), new ctype_facets::csv_point(delim, brackets)));
    triangleIStream.imbue(std::locale(triangleIStream.getloc(), new ctype_facets::csv_point(delim, brackets)));

    shapes::Shape::shape_ptr circle;
    shapes::Shape::shape_ptr square;
    shapes::Shape::shape_ptr triangle;

    circleIStream >> circle;
    squareIStream >> square;
    triangleIStream >> triangle;

    std::stringstream out;
    out.imbue(std::locale(out.getloc(), new ctype_facets::csv_point(delim, brackets)));

    out << circle << "\n" << square << "\n" << triangle << "\n";
    BOOST_CHECK_MESSAGE(out.str() == "CIRCLE (1;3)\nSQUARE (1;3)\nTRIANGLE (1;3)\n", "Operators check fail!");
  }

BOOST_AUTO_TEST_SUITE_END()
