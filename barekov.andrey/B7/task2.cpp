#include <algorithm>
#include <boost/io/ios_state.hpp>
#include <iostream>
#include <iterator>
#include <list>

#include "ctype-facets.hpp"
#include "shapes/circle.hpp"
#include "shapes/square.hpp"
#include "shapes/triangle.hpp"

const char Delim = ';';
const std::pair<char, char> Brackets('(', ')');

void print(const std::list<shapes::Shape::shape_ptr>& list, std::ostream& output, 
           const std::string& from, const std::string& to)
{
  output << from << "-" << to << ":\n";
  std::copy(list.begin(), list.end(), std::ostream_iterator<shapes::Shape::shape_ptr>(output, "\n"));
}

void task2(std::istream& input, std::ostream& output)
{
  using shape_istream_iterator = std::istream_iterator<shapes::Shape::shape_ptr>;

  boost::io::ios_locale_saver saver_in(input);
  boost::io::ios_locale_saver saver_out(output);

  input.imbue(std::locale(input.getloc(), new ctype_facets::csv_point(Delim, Brackets)));
  output.imbue(std::locale(output.getloc(), new ctype_facets::csv_point(Delim, Brackets)));

  boost::io::ios_flags_saver flagSaver(input);

  std::list<shapes::Shape::shape_ptr> list{shape_istream_iterator(input >> std::noskipws), shape_istream_iterator()};

  if (!input.eof() && input.fail())
  {
    throw std::invalid_argument("Fail while reading data.");
  }

  std::cout << "Original:\n";
  std::copy(list.begin(), list.end(), std::ostream_iterator<shapes::Shape::shape_ptr>(output, "\n"));

  list.sort([](const auto& lhs, const auto& rhs){ return lhs->isMoreLeft(*rhs); });
  print(list, output, "Left", "Right");

  list.sort([](const auto& lhs, const auto& rhs){ return rhs->isMoreLeft(*lhs); });
  print(list, output, "Right", "Left");

  list.sort([](const auto& lhs, const auto& rhs){ return lhs->isUpper(*rhs); });
  print(list, output, "Top", "Bottom");

  list.sort([](const auto& lhs, const auto& rhs){ return rhs->isUpper(*lhs); });
  print(list, output, "Bottom", "Top");
}
