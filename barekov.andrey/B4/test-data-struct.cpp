#include <boost/test/unit_test.hpp>

#include <algorithm>
#include <boost/io/ios_state.hpp>
#include <iterator>
#include <vector>

#include "data-struct.hpp"
#include "ctype-facets.hpp"

const char Delim = ',';

const auto Compare = [](const DataStruct& lhs, const DataStruct& rhs)
                     {
                       if (lhs.key1 != rhs.key1)
                       {
                         return lhs.key1 < rhs.key1;
                       }

                       if (lhs.key2 != rhs.key2)
                       {
                         return lhs.key2 < rhs.key2;
                       }

                       return lhs.str.size() < rhs.str.size();
                     };

BOOST_AUTO_TEST_SUITE(dataStructTestSuite)

  //Empty input test.
  BOOST_AUTO_TEST_CASE(emptyInput)
  {
    std::stringstream in("");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_delimiter(Delim)));
    
    std::vector<DataStruct> vector{std::istream_iterator<DataStruct>(in), std::istream_iterator<DataStruct>()};

    std::stringstream out;
    boost::io::ios_locale_saver saver_out(out);
    out.imbue(std::locale(out.getloc(), new ctype_facets::csv_delimiter(Delim)));

    std::copy(vector.begin(), vector.end(), std::ostream_iterator<DataStruct>(out, "\n"));

    BOOST_CHECK_MESSAGE(out.str().empty(), "Empty input check fail!");
  }

  //Basic data parsing tests.
  BOOST_AUTO_TEST_CASE(oneDataStruct)
  {
    std::stringstream in("1,2,data");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_delimiter(Delim)));

    std::vector<DataStruct> vector{std::istream_iterator<DataStruct>(in), std::istream_iterator<DataStruct>()};

    std::stringstream out;
    boost::io::ios_locale_saver saver_out(out);
    out.imbue(std::locale(out.getloc(), new ctype_facets::csv_delimiter(Delim)));

    std::copy(vector.begin(), vector.end(), std::ostream_iterator<DataStruct>(out, "\n"));

    BOOST_CHECK_MESSAGE(out.str() == "1,2,data\n", "One DataStruct check fail!");
  }

  BOOST_AUTO_TEST_CASE(oneDataStructWithVariousWhitespace)
  {
    std::stringstream in("    1\t,\t  2,  \t  data");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_delimiter(Delim)));

    std::vector<DataStruct> vector{std::istream_iterator<DataStruct>(in), std::istream_iterator<DataStruct>()};

    std::stringstream out;
    boost::io::ios_locale_saver saver_out(out);
    out.imbue(std::locale(out.getloc(), new ctype_facets::csv_delimiter(Delim)));

    std::copy(vector.begin(), vector.end(), std::ostream_iterator<DataStruct>(out, "\n"));

    BOOST_CHECK_MESSAGE(out.str() == "1,2,data\n", "Various Whitespace check fail!");
  }

  //Exception tests.
  BOOST_AUTO_TEST_CASE(key1OutOfRange)
  {
    std::stringstream in("6,2,data");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_delimiter(Delim)));

    std::vector<DataStruct> vector{};

    BOOST_CHECK_THROW(std::copy(std::istream_iterator<DataStruct>(in), std::istream_iterator<DataStruct>(),
                                vector.begin()), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(key2OutOfRange)
  {
    std::stringstream in("1,-8,data");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_delimiter(Delim)));

    std::vector<DataStruct> vector{};

    BOOST_CHECK_THROW(std::copy(std::istream_iterator<DataStruct>(in), std::istream_iterator<DataStruct>(),
                                vector.begin()), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(incorrectDataFormat)
  {
    std::stringstream in("6, data");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_delimiter(Delim)));

    std::vector<DataStruct> vector{std::istream_iterator<DataStruct>(in), std::istream_iterator<DataStruct>()};

    BOOST_CHECK_MESSAGE(in.fail() && !in.eof(), "Incorrect data format check fail!");
  }

  //Sorting tests.
  BOOST_AUTO_TEST_CASE(sortByKey1)
  {
    std::stringstream in("3, 1, data3\n"
                         "2, 1, data2\n"
                         "5, 1, data5\n"
                         "1, 1, data1\n"
                         "4, 1, data4");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_delimiter(Delim)));

    std::vector<DataStruct> vector{std::istream_iterator<DataStruct>(in), std::istream_iterator<DataStruct>()};
    std::sort(vector.begin(), vector.end(), Compare);

    std::stringstream out;
    boost::io::ios_locale_saver saver_out(out);
    out.imbue(std::locale(out.getloc(), new ctype_facets::csv_delimiter(Delim)));

    std::copy(vector.begin(), vector.end(), std::ostream_iterator<DataStruct>(out, "\n"));

    BOOST_CHECK_MESSAGE(out.str() == "1,1,data1\n2,1,data2\n3,1,data3\n4,1,data4\n5,1,data5\n", "Sort by key1 check fail!");
  }

  BOOST_AUTO_TEST_CASE(sortByKey2)
  {
    std::stringstream in("1, 3, data3\n"
                         "1, 2, data2\n"
                         "1, 5, data5\n"
                         "1, 1, data1\n"
                         "1, 4, data4");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_delimiter(Delim)));

    std::vector<DataStruct> vector{std::istream_iterator<DataStruct>(in), std::istream_iterator<DataStruct>()};
    std::sort(vector.begin(), vector.end(), Compare);

    std::stringstream out;
    boost::io::ios_locale_saver saver_out(out);
    out.imbue(std::locale(out.getloc(), new ctype_facets::csv_delimiter(Delim)));

    std::copy(vector.begin(), vector.end(), std::ostream_iterator<DataStruct>(out, "\n"));

    BOOST_CHECK_MESSAGE(out.str() == "1,1,data1\n1,2,data2\n1,3,data3\n1,4,data4\n1,5,data5\n", "Sort by key2 check fail!");
  }

  BOOST_AUTO_TEST_CASE(sortByStr)
  {
    std::stringstream in("1, 1, 100\n"
                         "1, 1, 10\n"
                         "1, 1, 10000\n"
                         "1, 1, 1\n"
                         "1, 1, 1000");
    boost::io::ios_locale_saver saver_in(in);
    in.imbue(std::locale(in.getloc(), new ctype_facets::csv_delimiter(Delim)));

    std::vector<DataStruct> vector{std::istream_iterator<DataStruct>(in), std::istream_iterator<DataStruct>()};
    std::sort(vector.begin(), vector.end(), Compare);

    std::stringstream out;
    boost::io::ios_locale_saver saver_out(out);
    out.imbue(std::locale(out.getloc(), new ctype_facets::csv_delimiter(Delim)));

    std::copy(vector.begin(), vector.end(), std::ostream_iterator<DataStruct>(out, "\n"));

    BOOST_CHECK_MESSAGE(out.str() == "1,1,1\n1,1,10\n1,1,100\n1,1,1000\n1,1,10000\n", "Sort by str check fail!");
  }

BOOST_AUTO_TEST_SUITE_END()
