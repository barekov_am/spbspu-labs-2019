#include "data-struct.hpp"
#include "ctype-facets.hpp"

#include <algorithm>
#include <boost/io/ios_state.hpp>
#include <iostream>
#include <iterator>
#include <vector>

const char Delim = ',';

const auto Compare = [](const DataStruct& lhs, const DataStruct& rhs)
                     {
                       if (lhs.key1 != rhs.key1)
                       {
                         return lhs.key1 < rhs.key1;
                       }

                       if (lhs.key2 != rhs.key2)
                       {
                         return lhs.key2 < rhs.key2;
                       }

                       return lhs.str.size() < rhs.str.size();
                     };

int main()
{
  try
  {
    boost::io::ios_locale_saver locSaverIn(std::cin);
    boost::io::ios_locale_saver locSaverOut(std::cout);
    boost::io::ios_flags_saver flagSaverIn(std::cin);

    std::cin.imbue(std::locale(std::cin.getloc(), new ctype_facets::csv_delimiter(Delim)));
    std::cout.imbue(std::locale(std::cout.getloc(), new ctype_facets::csv_delimiter(Delim)));

    std::vector<DataStruct> vector{std::istream_iterator<DataStruct>(std::cin >> std::noskipws), std::istream_iterator<DataStruct>()};

    if (!std::cin.good() && !std::cin.eof())
    {
      throw std::ios_base::failure("Fail while reading data.");
    }

    std::sort(vector.begin(), vector.end(), Compare);
    std::copy(vector.begin(), vector.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << "\n";

    return 1;
  }

  return 0;
}
