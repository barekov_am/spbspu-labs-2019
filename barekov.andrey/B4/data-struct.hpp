#ifndef DATA_STRUCT_HPP
#define DATA_STRUCT_HPP

#include <iosfwd>
#include <string>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;

  DataStruct() = default;
  DataStruct(int k1, int k2, const std::string& s);
};

std::istream& operator >>(std::istream& input, DataStruct& dataStruct);
std::ostream& operator <<(std::ostream& output, const DataStruct& dataStruct);

#endif
