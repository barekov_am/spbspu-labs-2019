#include "data-struct.hpp"

#include "ctype-facets.hpp"
#include "manipulators.hpp"

const std::pair<int, int> KeyRange(-5, 5);

DataStruct::DataStruct(int k1, int k2, const std::string& s) :
  key1(k1),
  key2(k2),
  str(s)
{
  if ((key1 < KeyRange.first) || (key1 > KeyRange.second))
  {
    throw std::invalid_argument("Key1 is out of range.");
  }

  if ((key2 < KeyRange.first) || (key2 > KeyRange.second))
  {
    throw std::invalid_argument("Key2 is out of range.");
  }
}

static std::istream& operator >>(std::istream& input, char)
{
  if (!ctype_facets::isdelim(static_cast<char>(input.peek()), input.getloc()))
  {
    input.setstate(std::ios_base::failbit);

    return input;
  }

  input.get();

  return input;
}

std::istream& operator >>(std::istream& input, DataStruct& dataStruct)
{
  int key1 = 0;
  int key2 = 0;
  char delim = 0;
  input >> manipulators::blank >> key1 >> manipulators::blank >> delim
        >> manipulators::blank >> key2 >> manipulators::blank >> delim;

  if (!input)
  {
    input.setstate(std::ios_base::failbit);

    return input;
  }

  std::string str{};
  std::getline(input >> manipulators::blank, str);

  dataStruct = DataStruct(key1, key2, str);

  return input;
}

std::ostream& operator <<(std::ostream& output, const DataStruct& dataStruct)
{
  char delimiter = ctype_facets::getDelim(output.getloc());

  output << dataStruct.key1 << delimiter << dataStruct.key2 << delimiter << dataStruct.str;

  return output;
}
