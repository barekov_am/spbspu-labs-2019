#ifndef CTYPE_FACETS_HPP
#define CTYPE_FACETS_HPP

#include <locale>
#include <utility>

namespace ctype_facets
{
  /*--------------------------------------------------------------------------------*/
  // After looking through a lot of information about bitsets, masks in ctype etc., //
  //              I now can't think of a less crutchy way to do this.               //
  //                                                                                //
  //   So, there is a flaw in this implementation that a collision can be caused    //
  // if someone wants to add another custom ctype mask and uses the same 1 << 4.    //
  /*--------------------------------------------------------------------------------*/

  constexpr unsigned short ISdelim = 1 << 4;
  constexpr unsigned short ISbropn = 1 << 5;
  constexpr unsigned short ISbrcls = 1 << 6;

  struct csv_delimiter : std::ctype<char>
  {
    static const mask delim = ISdelim;

    csv_delimiter(char delimiter);

    const mask* make_table(char delimiter);

    char delim_;
  };

  struct csv_point : std::ctype<char>
  {
    static const mask delim = ISdelim;
    static const mask bropn = ISbropn;
    static const mask brcls = ISbrcls;

    csv_point(char delimiter, std::pair<char, char> brackets);

    const mask* make_table(char delimiter, std::pair<char, char> brackets);

    char delim_;
    std::pair<char, char> brackets_;
  };

  template <typename CharT>
  bool isdelim(CharT c, const std::locale& loc)
  {
    return std::use_facet<std::ctype<CharT>>(loc).is(ISdelim, c);
  }

  template <typename CharT>
  bool isbropn(CharT c, const std::locale& loc)
  {
    return std::use_facet<std::ctype<CharT>>(loc).is(ISbropn, c);
  }

  template <typename CharT>
  bool isbrcls(CharT c, const std::locale& loc)
  {
    return std::use_facet<std::ctype<CharT>>(loc).is(ISbrcls, c);
  }

  char getDelim(const std::locale& loc);
  char getOpeningBr(const std::locale& loc);
  char getClosingBr(const std::locale& loc);
}

#endif
