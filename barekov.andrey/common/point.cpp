#include "point.hpp"

#include <iostream>

#include "manipulators.hpp"
#include "ctype-facets.hpp"

std::istream& operator >>(std::istream& input, point_t& point)
{
  char delim = 0;
  input >> manipulators::blank >> makeOpeningBr(delim)
        >> manipulators::blank >> point.x
        >> manipulators::blank >> makeDelim(delim)
        >> manipulators::blank >> point.y
        >> manipulators::blank >> makeClosingBr(delim);

  if (!input)
  {
    input.setstate(std::ios_base::failbit);

    return input;
  }

  return input;
}

std::ostream& operator <<(std::ostream& output, const point_t& point)
{
  auto loc = output.getloc();
  output << ctype_facets::getOpeningBr(loc) << point.x
         << ctype_facets::getDelim(loc) << point.y
         << ctype_facets::getClosingBr(loc);

  return output;
}

opening_bracket_t makeOpeningBr(char bracket)
{
  return {bracket};
}

delim_t makeDelim(char delim)
{
  return {delim};
}

closing_bracket_t makeClosingBr(char bracket)
{
  return {bracket};
}

std::istream& operator >>(std::istream& input, opening_bracket_t&&)
{
 if (!ctype_facets::isbropn(static_cast<char>(input.peek()), input.getloc()))
  {
    input.setstate(std::ios_base::failbit);

    return input;
  }

  input.get();

  return input;
}

std::istream& operator >>(std::istream& input, delim_t&&)
{
 if (!ctype_facets::isdelim(static_cast<char>(input.peek()), input.getloc()))
  {
    input.setstate(std::ios_base::failbit);

    return input;
  }

  input.get();

  return input;
}

std::istream& operator >>(std::istream& input, closing_bracket_t&&)
{
 if (!ctype_facets::isbrcls(static_cast<char>(input.peek()), input.getloc()))
  {
    input.setstate(std::ios_base::failbit);

    return input;
  }

  input.get();

  return input;
}
