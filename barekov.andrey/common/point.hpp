#ifndef POINT_HPP
#define POINT_HPP

#include <iosfwd>

struct point_t
{
  int x;
  int y;
};

std::istream& operator >>(std::istream& input, point_t& point);
std::ostream& operator <<(std::ostream& output, const point_t& point);

struct opening_bracket_t
{
  char bracket;
};

struct delim_t
{
  char delim;
};

struct closing_bracket_t
{
  char bracket;
};

opening_bracket_t makeOpeningBr(char bracket);
delim_t makeDelim(char delim);
closing_bracket_t makeClosingBr(char bracket);

std::istream& operator >>(std::istream& input, opening_bracket_t&&);
std::istream& operator >>(std::istream& input, delim_t&&);
std::istream& operator >>(std::istream& input, closing_bracket_t&&);

#endif
