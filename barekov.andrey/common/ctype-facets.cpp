#include "ctype-facets.hpp"

#include <vector>

const char space = ' ';

ctype_facets::csv_delimiter::csv_delimiter(char delimiter):
  ctype(make_table(delimiter)),
  delim_(delimiter)
{ }

const ctype_facets::csv_delimiter::mask* ctype_facets::csv_delimiter::make_table(char delimiter)
{
  static std::vector<mask> v(classic_table(), classic_table() + table_size);
  v[delimiter] |= delim;

  return &v[0];
}

ctype_facets::csv_point::csv_point(char delimiter, std::pair<char, char> brackets):
  ctype(make_table(delimiter, brackets)),
  delim_(delimiter),
  brackets_(brackets)
{ }

const ctype_facets::csv_point::mask* ctype_facets::csv_point::make_table(char delimiter, std::pair<char, char> brackets)
{
  static std::vector<mask> v(classic_table(), classic_table() + table_size);
  v[delimiter] |= delim;
  v[brackets.first] |= bropn;
  v[brackets.second] |= brcls;

  return &v[0];
}

char ctype_facets::getDelim(const std::locale& loc)
{
  if (std::has_facet<csv_delimiter>(loc))
  {
    return std::use_facet<csv_delimiter>(loc).delim_;
  }
  else if (std::has_facet<csv_point>(loc))
  {
    return std::use_facet<csv_point>(loc).delim_;
  }
  else
  {
    return space;
  }
}

char ctype_facets::getOpeningBr(const std::locale& loc)
{
  if (std::has_facet<csv_point>(loc))
  {
    return std::use_facet<csv_point>(loc).brackets_.first;
  }
  else
  {
    return space;
  }
}

char ctype_facets::getClosingBr(const std::locale& loc)
{
  if (std::has_facet<csv_point>(loc))
  {
    return std::use_facet<csv_point>(loc).brackets_.second;
  }
  else
  {
    return space;
  }
}
