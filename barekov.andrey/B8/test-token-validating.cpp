#include <boost/test/unit_test.hpp>

#include "formatting-iterator.hpp"
#include "token-iterator.hpp"
#include "validate-token.hpp"

constexpr size_t DefaultLineWidth = 40;

BOOST_AUTO_TEST_SUITE(TokenValidatingTestSuite)

  BOOST_AUTO_TEST_CASE(StartWithPunctuation)
  {
    std::stringstream in("! Attention!");
    std::stringstream out;

    BOOST_CHECK_THROW(std::transform(format::token_iterator(in), format::token_iterator(),
                      format::formatting_iterator(out, DefaultLineWidth), format::ValidateToken()),
                      std::ios_base::failure);
  }

  BOOST_AUTO_TEST_CASE(StartWithDash)
  {
    std::stringstream in("\t---Vasya?");
    std::stringstream out;

    BOOST_CHECK_THROW(std::transform(format::token_iterator(in), format::token_iterator(),
                      format::formatting_iterator(out, DefaultLineWidth), format::ValidateToken()),
                      std::ios_base::failure);
  }

  BOOST_AUTO_TEST_CASE(DoublePunctuation)
  {
    std::stringstream in("Placeholder?!");
    std::stringstream out;

    BOOST_CHECK_THROW(std::transform(format::token_iterator(in), format::token_iterator(),
                      format::formatting_iterator(out, DefaultLineWidth), format::ValidateToken()),
                      std::ios_base::failure);
  }

  BOOST_AUTO_TEST_CASE(DoubleDash)
  {
    std::stringstream in("Placeholder --- --- placeholder!");
    std::stringstream out;

    BOOST_CHECK_THROW(std::transform(format::token_iterator(in), format::token_iterator(),
                      format::formatting_iterator(out, DefaultLineWidth), format::ValidateToken()),
                      std::ios_base::failure);
  }

  BOOST_AUTO_TEST_CASE(CommaAfterDash)
  {
    std::stringstream in("Placeholder, --- placeholder --- , oops.");
    std::stringstream out;

    BOOST_CHECK_THROW(std::transform(format::token_iterator(in), format::token_iterator(),
                      format::formatting_iterator(out, DefaultLineWidth), format::ValidateToken()),
                      std::ios_base::failure);
  }

  BOOST_AUTO_TEST_CASE(InvalidToken)
  {
    std::stringstream in("UltraPwnerRuleZZZ2005");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    BOOST_CHECK_MESSAGE((in.fail() && !in.eof()) && (out.fail() && !out.eof()), "Invalid token check fail!");
  }

  BOOST_AUTO_TEST_CASE(ShortDash)
  {
    std::stringstream in("InsertName -- dolboyashher");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    BOOST_CHECK_MESSAGE((in.fail() && !in.eof()) && (out.fail() && !out.eof()), "Short dash check fail!");
  }

  BOOST_AUTO_TEST_CASE(LongDash)
  {
    std::stringstream in("InsertName ------ dolboyashher");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    BOOST_CHECK_MESSAGE((in.fail() && !in.eof()) && (out.fail() && !out.eof()), "Long dash check fail!");
  }

BOOST_AUTO_TEST_SUITE_END()
