#include "token.hpp"

#include <iostream>

format::token_t::TokenGuard::TokenGuard(token_t& token) :
  token_(token)
{ }

format::token_t::TokenGuard::~TokenGuard()
{
  if ((token_.type == token_t::TokenType::dash) && (token_.data.size() != token_t::DashSize))
  {
    token_.type = token_t::TokenType::invalid;
  }
}

bool format::token_t::isWord() const
{
  return type == TokenType::word;
}

bool format::token_t::isPunct() const
{
  return type == TokenType::punct;
}

bool format::token_t::isDash() const
{
  return type == TokenType::dash;
}

bool format::token_t::isNumber() const
{
  return type == TokenType::number;
}

bool format::token_t::isInvalid() const
{
  return type == TokenType::invalid;
}
