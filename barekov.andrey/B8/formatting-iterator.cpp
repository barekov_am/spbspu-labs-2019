#include "formatting-iterator.hpp"

#ifndef __has_cpp_attribute
  #define FALLTHROUGH
#else
  #if !__has_cpp_attribute(fallthrough)
    #define FALLTHROUGH
  #else
    #define FALLTHROUGH [[fallthrough]]
  #endif
#endif

constexpr size_t SpaceLength = 1;

format::formatting_iterator::formatting_iterator(std::ostream& output, size_t lineWidth) :
  impl_(std::make_shared<FormattingIteratorImpl>(output, lineWidth))
{ }

format::formatting_iterator& format::formatting_iterator::operator *()
{
  return *this;
}

format::formatting_iterator& format::formatting_iterator::operator ++()
{
  return *this;
}

format::formatting_iterator& format::formatting_iterator::operator ++(int)
{
  return *this;
}

format::formatting_iterator& format::formatting_iterator::operator =(const format::token_t& token)
{
  impl_->writeToken(token);

  return *this;
}

format::formatting_iterator::FormattingIteratorImpl::FormattingIteratorImpl(std::ostream& output, size_t lineWidth) :
  ostream_(output),
  lineWidth_(lineWidth),
  str_({}),
  prevTokens_()
{
  str_.reserve(lineWidth + 1);
}

format::formatting_iterator::FormattingIteratorImpl::~FormattingIteratorImpl()
{
  ostream_ << str_ << "\n";
}

void format::formatting_iterator::FormattingIteratorImpl::writeToken(const format::token_t& token)
{
  size_t expectedLength = str_.size() + token.data.size() + static_cast<int>(!str_.empty() && !token.isPunct());

  switch (token.type)
  {
  case token_t::TokenType::word:
  case token_t::TokenType::number:
    if (expectedLength > lineWidth_)
    {
      ostream_ << str_ << "\n";
      str_.clear();
      str_.reserve(lineWidth_ + 1);
    }

    if (!str_.empty())
    {
      str_ += " ";
    }

    break;

  case token_t::TokenType::dash:
    if (!str_.empty())
    {
      str_ += " ";
    }

    FALLTHROUGH;

  case token_t::TokenType::punct:
    if (expectedLength > lineWidth_)
    {
      wrapPunctuation(token);
    }

    break;

  case token_t::TokenType::invalid:
    ostream_.setstate(std::ios_base::failbit);
  }

  str_ += token.data;
  prevTokens_.second = prevTokens_.first;
  prevTokens_.first = token;
}

void format::formatting_iterator::FormattingIteratorImpl::wrapPunctuation(const token_t& token)
{
  bool prevPunct = prevTokens_.first.isPunct();

  auto wrapSize = (prevPunct ? prevTokens_.second : prevTokens_.first).data.size() + static_cast<int>(!token.isPunct());
  auto prevPos = str_.size() - wrapSize - SpaceLength - static_cast<int>(prevPunct);

  std::string prev = str_.substr(prevPos, wrapSize + SpaceLength + static_cast<int>(prevPunct));
  str_.erase(prevPos, wrapSize + SpaceLength + static_cast<int>(prevPunct));

  ostream_ << str_ << "\n";
  str_ = prev.erase(0, 1);
}
