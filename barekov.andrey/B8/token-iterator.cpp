#include "token-iterator.hpp"

#include <locale>

constexpr size_t TabSize = 8;

format::token_iterator::token_iterator() :
  impl_(nullptr)
{ }

format::token_iterator::token_iterator(std::istream& input) :
  impl_(std::make_shared<TokenIteratorImpl>(input))
{
  ++(*this);
}

const format::token_t& format::token_iterator::operator *() const
{
  return impl_->token_;
}

const format::token_t* format::token_iterator::operator ->() const
{
  return &(impl_->token_);
}

format::token_iterator& format::token_iterator::operator ++()
{
  if (!impl_->istream_)
  {
    impl_ = nullptr;
  }
  else
  {
    impl_->token_.data.clear();
    impl_->readToken();

    if (impl_->token_.isInvalid())
    {
      impl_->istream_.setstate(std::ios_base::failbit);
    }
    
    if (impl_->token_.data.empty())
    {
      impl_ = nullptr;
    }
  }

  return *this;
}

format::token_iterator format::token_iterator::operator ++(int)
{
  token_iterator tmp(*this);
  ++(*this);

  return tmp;
}

bool format::token_iterator::operator ==(const format::token_iterator& rhs)
{
  return impl_ == rhs.impl_;
}

bool format::token_iterator::operator !=(const format::token_iterator& rhs)
{
  return !(*this == rhs);
}

format::token_iterator::TokenIteratorImpl::TokenIteratorImpl(std::istream& input) :
  istream_(input),
  token_(),
  locale_(istream_.getloc()),
  decimalPoint_(std::use_facet<std::numpunct<char>>(locale_).decimal_point()),
  line_(1),
  column_(1)
{ }

void format::token_iterator::TokenIteratorImpl::readToken()
{
  using traits = std::char_traits<char>;

  std::istream::sentry sentry(istream_);

  if (sentry)
  {
    token_t::TokenGuard tokenGuard(token_);

    auto& ct = std::use_facet<std::ctype<char>>(locale_);
    auto streambuf = istream_.rdbuf();
    auto c = streambuf->sgetc();

    std::string str{};
    bool isReading = true;
    while (traits::not_eof(c) && isReading)
    {
      if (ct.is(std::ctype_base::space, c))
      {
        if (traits::eq(c, '\n'))
        {
          ++line_;
          column_ = 0;
        }
        else if (traits::eq(c, '\t'))
        {
          column_ = (column_ + TabSize) / TabSize * TabSize; //calculates the next tab stop, because it's not just 8 ahead
        }
        else if (traits::eq(c, '\v'))
        {
          ++line_;
        }

        ++column_;
        token_.data = str;
        return;
      }
      else if (ct.is(std::ctype_base::alpha, c))
      {
        if (str.empty())
        {
          str.push_back(c);
          token_.type = token_t::TokenType::word;
          token_.line = line_;
          token_.column = column_;
        }
        else
        {
          switch (token_.type)
          {
          case token_t::TokenType::word:
            str.push_back(c);
            break;

          case token_t::TokenType::punct:
          case token_t::TokenType::dash:
            streambuf->sputbackc(c);
            isReading = false;
            break;

          default:
            token_.type = token_t::TokenType::invalid;
          }
        }
      }
      else if (ct.is(std::ctype_base::digit, c))
      {
        if (str.empty())
        {
          str.push_back(c);
          token_.type = token_t::TokenType::number;
          token_.line = line_;
          token_.column = column_;
        }
        else
        {
          switch (token_.type)
          {
          case token_t::TokenType::number:
            str.push_back(c);
            break;

          case token_t::TokenType::punct:
          case token_t::TokenType::dash:
            streambuf->sputbackc(c);
            isReading = false;
            break;

          default:
            token_.type = token_t::TokenType::invalid;
          }
        }
      }
      else if (traits::eq(c, '+'))
      {
        if (str.empty())
        {
          str.push_back(c);
          c = streambuf->snextc();
          if (ct.is(std::ctype_base::digit, c))
          {
            str.push_back(c);
            token_.type = token_t::TokenType::number;
            token_.line = line_;
            token_.column = column_;
            ++column_;
          }
          else
          {
            streambuf->sputbackc(c);
            token_.type = token_t::TokenType::punct;
            token_.line = line_;
            token_.column = column_;
          }
        }
        else
        {
          streambuf->sputbackc(c);
          isReading = false;
        }
      }
      else if (traits::eq(c, '-'))
      {
        if (str.empty())
        {
          str.push_back(c);
          c = streambuf->snextc();
          if (traits::eq(c, '-'))
          {
            str.push_back(c);
            token_.type = token_t::TokenType::dash;
            token_.line = line_;
            token_.column = column_;
            ++column_;
          }
          else if (ct.is(std::ctype_base::digit, c))
          {
            str.push_back(c);
            token_.type = token_t::TokenType::number;
            token_.line = line_;
            token_.column = column_;
            ++column_;
          }
          else
          {
            streambuf->sputbackc(c);
            token_.type = token_t::TokenType::punct;
          }
        }
        else
        {
          switch (token_.type)
          {
          case token_t::TokenType::word:
          {
            char current = c;
            c = streambuf->snextc();
            if (traits::eq(c, '-'))
            {
              streambuf->sputbackc(c);
              streambuf->sputbackc(current);
              isReading = false;
            }
            else
            {
              str.push_back(current);
              if (ct.is(std::ctype_base::alpha, c))
              {
                str.push_back(c);
              }
              else
              {
                streambuf->sputbackc(c);
              }
            }

            break;
          }

          case token_t::TokenType::dash:
            str.push_back(c);
            break;

          default:
            streambuf->sputbackc(c);
            isReading = false;
          }
        }
      }
      else if (traits::eq(c, decimalPoint_))
      {
        if (str.empty())
        {
          str.push_back(c);
          token_.type = token_t::TokenType::punct;
          token_.line = line_;
          token_.column = column_;
          isReading = false;
        }
        else
        {
          switch(token_.type)
          {
          case token_t::TokenType::number:
          {
            char current = c;
            c = streambuf->snextc();
            if (!ct.is(std::ctype_base::digit, c))
            {
              streambuf->sungetc();
              streambuf->sputbackc(current);
              isReading = false;
            }
            else
            {
              str.push_back(current);
              str.push_back(c);
            }

            break;
          }

          default:
            streambuf->sputbackc(c);
            isReading = false;
          }
        }
      }
      else if (ct.is(std::ctype_base::punct, c))
      {
        if (str.empty())
        {
          str.push_back(c);
          token_.type = token_t::TokenType::punct;
          token_.line = line_;
          token_.column = column_;
          isReading = false;
        }
        else
        {
          streambuf->sputbackc(c);
          isReading = false;
        }
      }

      if (isReading)
      {
        ++column_;
      }

      c = streambuf->snextc();
    }

    token_.data = str;
  }
}
