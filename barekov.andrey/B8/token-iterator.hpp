#ifndef FORMAT_TOKEN_ITERATOR_HPP
#define FORMAT_TOKEN_ITERATOR_HPP

#include <iterator>
#include <memory>

#include "token.hpp"

namespace format
{
  class token_iterator : public std::iterator<std::input_iterator_tag, token_t>
  {
  public:
    token_iterator();
    token_iterator(std::istream& input);

    const token_t& operator *() const;
    const token_t* operator ->() const;

    token_iterator& operator ++();
    token_iterator operator ++(int);

    bool operator ==(const token_iterator& rhs);
    bool operator !=(const token_iterator& rhs);

  private:
    class TokenIteratorImpl
    {
    public:
      TokenIteratorImpl(std::istream& input);

      void readToken();

      friend class token_iterator;

    private:
      std::istream& istream_;
      token_t token_;
      std::locale locale_;
      char decimalPoint_;

      size_t line_;
      size_t column_;
    };

    std::shared_ptr<TokenIteratorImpl> impl_;
  };
}

#endif
