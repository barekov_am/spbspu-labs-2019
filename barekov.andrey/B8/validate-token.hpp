#ifndef VALIDATE_TOKEN_HPP
#define VALIDATE_TOKEN_HPP

#include "formatting-iterator.hpp"

namespace format
{
  class ValidateToken : std::binary_function<token_t, formatting_iterator, void>
  {
  public:
    static constexpr size_t MaxTokenSize = 20;

    ValidateToken();

    token_t operator ()(const token_t& token);
  private:
    token_t prev;
  };
}

#endif
