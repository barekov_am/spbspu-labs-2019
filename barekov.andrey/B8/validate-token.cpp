#include "validate-token.hpp"

#include <ios>

format::ValidateToken::ValidateToken() :
  prev()
{ }

format::token_t format::ValidateToken::operator ()(const token_t& token)
{
  if (prev.data.empty())
  {
    if (token.isPunct() || token.isDash())
    {
      throw std::ios_base::failure("Text cannot start with punctuation. Failed at ("
          + std::to_string(token.line) + "; " + std::to_string(token.column) + ")");
    }
  }

  if (prev.isPunct() || prev.isDash())
  {
    if (token.isPunct())
    {
      throw std::ios_base::failure("There can be no double punctuation except of ',---'. Failed at ("
          + std::to_string(token.line) + "; " + std::to_string(token.column) + ")");
    }

    if (token.isDash())
    {
      if (prev.data != ",")
      {
        throw std::ios_base::failure("There can be no double punctuation except of ',---'. Failed at ("
            + std::to_string(token.line) + "; " + std::to_string(token.column) + ")");
      }
    }
  }

  if ((token.isWord() || token.isNumber()) && (token.data.size() > MaxTokenSize))
  {
    throw std::ios_base::failure("Length of a word or a number cannot exceed 20 characters. Failed at ("
            + std::to_string(token.line) + "; " + std::to_string(token.column) + ")");
  }

  prev = token;

  return token;
}
