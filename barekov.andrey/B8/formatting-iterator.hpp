#ifndef FORMATTING_ITERATOR_HPP
#define FORMATTING_ITERATOR_HPP

#include <iterator>
#include <memory>
#include <utility>

#include "token.hpp"

namespace format
{
  class formatting_iterator : public std::iterator<std::output_iterator_tag, void, void, void, void>
  {
  public:
    formatting_iterator(std::ostream& output, size_t lineWidth);

    formatting_iterator& operator *();

    formatting_iterator& operator ++();
    formatting_iterator& operator ++(int);

    formatting_iterator& operator =(const token_t& token);

  private:
    class FormattingIteratorImpl
    {
    public:
      FormattingIteratorImpl(std::ostream& output, size_t lineWidth);
      ~FormattingIteratorImpl();

      void writeToken(const token_t& token);
      void wrapPunctuation(const token_t& token);

    private:
      std::ostream& ostream_;
      size_t lineWidth_;
      std::string str_;
      std::pair<token_t, token_t> prevTokens_;
    };

    std::shared_ptr<FormattingIteratorImpl> impl_;
  };
}

#endif
