#include <algorithm>
#include <boost/io/ios_state.hpp>
#include <iostream>
#include <locale>

#include "formatting-iterator.hpp"
#include "token-iterator.hpp"
#include "validate-token.hpp"

constexpr size_t DefaultLineWidth = 40;

void task(std::istream& input, std::ostream& output, size_t lineWidth = DefaultLineWidth)
{
  std::transform(format::token_iterator(input), format::token_iterator(),
                 format::formatting_iterator(output, lineWidth), format::ValidateToken());

  if (input.fail() && !input.eof())
  {
    throw std::ios_base::failure("Fail while reading data.");
  }

  if (output.fail())
  {
    throw std::ios_base::failure("Fail while writing data.");
  }
}
