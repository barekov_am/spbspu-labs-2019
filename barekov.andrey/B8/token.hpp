#ifndef FORMAT_TOKEN_HPP
#define FORMAT_TOKEN_HPP

#include <iosfwd>
#include <string>

namespace format
{
  struct token_t
  {
    enum class TokenType
    {
      word,
      punct,
      dash,
      number,
      invalid
    };

    class TokenGuard
    {
    public:
      TokenGuard(token_t& token);
      ~TokenGuard();

    private:
      token_t& token_;
    };

    static constexpr size_t DashSize = 3;

    std::string data;
    TokenType type;
    size_t line;
    size_t column;

    token_t() = default;

    bool isWord() const;
    bool isPunct() const;
    bool isDash() const;
    bool isNumber() const;
    bool isInvalid() const;
  };
}

#endif
