#include <cstring>
#include <iostream>

constexpr size_t DefaultLineWidth = 40;

constexpr size_t MaxWordLength = 20;
constexpr size_t DashLength = 3;
constexpr size_t SpaceLength = 1;
constexpr size_t CommaLength = 1;

constexpr size_t MinLineWidth = MaxWordLength + CommaLength + DashLength + SpaceLength;

void task(std::istream& input, std::ostream& output, size_t lineWidth = DefaultLineWidth);

int main(int argc, char* argv[])
{
  try
  {
    switch (argc)
    {
    case 1:
      task(std::cin, std::cout);
      break;

    case 3:
    {
      if (std::strcmp(argv[1], "--line-width") != 0)
      {
        std::cerr << "Incorrect argument.\n";
        return 1;
      }

      char* ptr = nullptr;
      size_t lineWidth = std::strtol(argv[2], &ptr, 10);
      if ((*ptr != 0x00) || (lineWidth < MinLineWidth))
      {
        std::cerr << "Incorrect argument\n";
        return 1;
      }

      task(std::cin, std::cout, lineWidth);
      break;
    }
    default:
      std::cerr << "Invalid number of arguments\n";
      return 1;
    }
  }
  catch (const std::invalid_argument &e)
  {
    std::cerr << e.what() << '\n';

    return 1;
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << '\n';

    return 2;
  }

  return 0;
}
