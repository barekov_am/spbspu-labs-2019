#include <boost/test/unit_test.hpp>

#include "formatting-iterator.hpp"
#include "token-iterator.hpp"

constexpr size_t DefaultLineWidth = 40;
constexpr size_t WrapTestLineWidth = 25;

BOOST_AUTO_TEST_SUITE(TextFormattingTestSuite)

  BOOST_AUTO_TEST_CASE(EmptyInput)
  {
    std::stringstream in("");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    const std::string correctOutput = "\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Empty input check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(SimpleWord)
  {
    std::stringstream in("placeholder");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    const std::string correctOutput = "placeholder\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Simple word check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(WordWithAHyphen)
  {
    std::stringstream in("place-holder");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    const std::string correctOutput = "place-holder\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Word with a hyphen check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(WordHyphenAtTheEnd)
  {
    std::stringstream in("placeholder-");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    const std::string correctOutput = "placeholder-\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Word with a hyphen at the end check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(SimpleNumber)
  {
    std::stringstream in("1234567");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    const std::string correctOutput = "1234567\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Simple number check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(PositiveNumber)
  {
    std::stringstream in("+1234567");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    const std::string correctOutput = "+1234567\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Positive number check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(NegativeNumber)
  {
    std::stringstream in("-1234567");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    const std::string correctOutput = "-1234567\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Negative number check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(RealNumber)
  {
    std::stringstream in("1234.567");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    const std::string correctOutput = "1234.567\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Real number check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(LocaledRealNumber)
  {
    std::stringstream in("1234,567");
    std::stringstream out;

    in.imbue(std::locale("ru_RU.UTF8"));
    out.imbue(std::locale("ru_RU.UTF8"));

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    const std::string correctOutput = "1234,567\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Real number with locale check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(WordAfterNumber)
  {
    std::stringstream in("1234.hello");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    const std::string correctOutput = "1234. hello\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Word after number check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(WordWithPunctuation)
  {
    std::stringstream in("Hello\t!");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    const std::string correctOutput = "Hello!\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Word with punctuation check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(NumberWithPunctuation)
  {
    std::stringstream in("1234.567 !");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    const std::string correctOutput = "1234.567!\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Number with punctuation check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(PunctuationBetweenTokens)
  {
    std::stringstream in("1234.567    ! Hello ?");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    const std::string correctOutput = "1234.567! Hello?\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Punctuation between tokens check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(DashBetweenTokens)
  {
    std::stringstream in(" Placeholder---      1234.567\t!");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    const std::string correctOutput = "Placeholder --- 1234.567!\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Dash between tokens check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(CommaAndDash)
  {
    std::stringstream in(" Placeholder  ,---1234.567\t!");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, DefaultLineWidth));

    const std::string correctOutput = "Placeholder, --- 1234.567!\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Comma and dash check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(LineWrap)
  {
    std::stringstream in(" Placeholder---      1234.56789\t");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, WrapTestLineWidth));

    const std::string correctOutput = "Placeholder ---\n1234.56789\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Line wrap check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(PunctuationWrap)
  {
    std::stringstream in(" Placeholder---      1234.5678\t!");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, WrapTestLineWidth));

    const std::string correctOutput = "Placeholder ---\n1234.5678!\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Punctuation wrap check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(DashWrap)
  {
    std::stringstream in(" Placeholder---      1234.567\t---placeholder?");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, WrapTestLineWidth));

    const std::string correctOutput = "Placeholder ---\n1234.567 --- placeholder?\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Dash wrap check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

  BOOST_AUTO_TEST_CASE(CommaAndDashWrap)
  {
    std::stringstream in(" Placeholder---      1234.5\t,---placeholder?");
    std::stringstream out;

    std::copy(format::token_iterator(in), format::token_iterator(),
              format::formatting_iterator(out, WrapTestLineWidth));

    const std::string correctOutput = "Placeholder ---\n1234.5, --- placeholder?\n";

    BOOST_CHECK_MESSAGE(out.str() == correctOutput, 
        "Comma and dash wrap check fail!\nExpected: " + correctOutput 
          + "Got: " + out.str());
  }

BOOST_AUTO_TEST_SUITE_END()
