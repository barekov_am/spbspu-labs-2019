#include <iostream>
#include <string>
#include "options.hpp"

int main(int args, char *argv[])
{

  if(args != 2)
  {
    std::cerr << "Invalid number of arguments";
    return 1;
  }

  try
  {
    int option = std::stoi(argv[1]);
    switch(option)
    {
    case 1:
      runOptionOne(std::cin, std::cout);
      break;
    case 2:
      runOptionTwo(std::cin, std::cout);
      break;
    default:
      std::cerr << "Error option";
      return 1;
    }
  }
  
  catch(const std::invalid_argument &exception)
  {
    std::cerr << exception.what();
    return 1;
  }

  return 0;
}
