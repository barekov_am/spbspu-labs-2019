#include "dataStruct.hpp"
#include <iostream>

DataStruct readData()
{
  std::string line;
  std::getline(std::cin, line);

  int keys[2] = {0};
  for (int i = 0; i < 2; i++)
  {
    size_t comma = line.find_first_of(',');
    if (comma == std::string::npos)
    {
      throw std::invalid_argument("Error data.");
    }

    keys[i] = std::stoi(line.substr(0, comma));
    if (std::abs(keys[i]) > 5)
    {
      throw std::invalid_argument("Error key.");
    }
    line = line.erase(0, comma + 1);
  }

  while (line.find_first_of(' ') == 0)
  {
    line.erase(0, 1);
  }

  if (line.empty())
  {
    throw std::invalid_argument("Error data.");
  }

  return { keys[0], keys[1], line };
}
