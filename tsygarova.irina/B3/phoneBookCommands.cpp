#include <memory>
#include <sstream>
#include <algorithm>
#include "phoneBookHandler.hpp"

std::istream &skipws(std::istream &in)
{
  while (std::isblank(in.peek()))
  {
    in.get();
  }
  return in;
}

std::istream &operator>>(std::istream &in, PhoneBook::valueType &record)
{
  std::string number;
  std::string name;
  in >> skipws >> number >> skipws;

  char *end = nullptr;
  std::strtol(number.c_str(), &end, 10);

  if (*end)
  {
    in.setstate(std::ios_base::failbit);
    return in;
  }

  std::string tmp;
  if (in.peek() != '"')
  {
    in.setstate(std::ios_base::failbit);
    return in;
  }

  in.get();
  while (getline(in, tmp, '"'))
  {
    if (in.eof())
    {
      in.setstate(std::ios_base::failbit);
      return in;
    }

    name += tmp;
    if (name.back() != '\\')
    {
      break;
    }

    name.pop_back();
    name.push_back('"');
  }

  in >> skipws;
  if (in.good())
  {
    record = std::make_pair(number, name);
  }
  return in;
}

std::unique_ptr<PhoneBookCommand> createAddRecord(std::istream &arg)
{
  PhoneBook::valueType newRecord;
  arg >> newRecord;

  if (arg.bad())
  {
    throw std::runtime_error("Error input.\n");
  }

  if (arg.fail())
  {
    arg.clear();
    arg.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return std::make_unique<CommandInvalid>();
  }

  if (arg.peek() != '\n')
  {
    arg.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return std::make_unique<CommandInvalid>();
  }

  return std::make_unique<CommandAddRecord>(newRecord);
}

std::unique_ptr<PhoneBookCommand> createStoreBookmark(std::istream &arg)
{
  std::string nameMark;
  std::string newNameMark;
  arg >> skipws >> nameMark >> skipws >> newNameMark >> skipws;

  if (arg.bad())
  {
    throw std::runtime_error("Error input.\n");
  }

  if (arg.fail())
  {
    arg.clear();
    arg.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return std::make_unique<CommandInvalid>();
  }

  if (arg.peek() != '\n')
  {
    arg.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return std::make_unique<CommandInvalid>();
  }

  return std::make_unique<CommandStoreBookmark>(nameMark, newNameMark);
}

std::unique_ptr<PhoneBookCommand> createInsert(std::istream &arg)
{
  PhoneBook::valueType newRecord;
  std::string placeOfInsert;
  std::string nameMark;

  arg >> skipws >> placeOfInsert >> skipws >> nameMark >> newRecord;

  if (arg.bad())
  {
    throw std::runtime_error("Error input.\n");
  }

  if (arg.fail())
  {
    arg.clear();
    arg.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return std::make_unique<CommandInvalid>();
  }

  if (arg.peek() != '\n')
  {
    arg.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return std::make_unique<CommandInvalid>();
  }

  if (placeOfInsert == "before")
  {
    return std::make_unique<CommandInsertBeforeBookmark>(nameMark, newRecord);
  }

  if (placeOfInsert == "after")
  {
    return std::make_unique<CommandInsertAfterBookmark>(nameMark, newRecord);
  }

  return std::make_unique<CommandInvalid>();
}

std::unique_ptr<PhoneBookCommand> createDeleteRecord(std::istream &arg)
{
  std::string nameMark;
  arg >> skipws >> nameMark >> skipws;
  if (arg.bad())
  {
    throw std::runtime_error("Error input.\n");
  }

  if (arg.fail())
  {
    arg.clear();
    arg.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return std::make_unique<CommandInvalid>();
  }

  if (arg.peek() != '\n')
  {
    arg.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return std::make_unique<CommandInvalid>();
  }

  return std::make_unique<CommandDeleteRecord>(nameMark);
}

std::unique_ptr<PhoneBookCommand> createShowRecord(std::istream &arg)
{
  std::string nameMark;
  arg >> skipws >> nameMark >> skipws;
  if (arg.bad())
  {
    throw std::runtime_error("Error input.\n");
  }

  if (arg.fail())
  {
    arg.clear();
    arg.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return std::make_unique<CommandInvalid>();
  }

  if (arg.peek() != '\n')
  {
    arg.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return std::make_unique<CommandInvalid>();
  }

  return std::make_unique<CommandShowRecord>(nameMark);
}

std::unique_ptr<PhoneBookCommand> createMoveBookmark(std::istream &arg)
{
  std::string nameMark;
  std::string steps;
  arg >> skipws >> nameMark >> skipws >> steps >> skipws;

  if (arg.bad())
  {
    throw std::runtime_error("Error input.\n");
  }

  if (arg.fail())
  {
    arg.clear();
    arg.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return std::make_unique<CommandInvalid>();
  }

  if (arg.peek() != '\n')
  {
    arg.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return std::make_unique<CommandInvalid>();
  }

  return std::make_unique<CommandMoveBookmark>(nameMark, steps);
}

std::unique_ptr<PhoneBookCommand> getCommand(std::istream &in)
{
  static const struct
  {
    using creatorType = std::unique_ptr<PhoneBookCommand> (*) (std::istream &arg);
    const char *name;
    creatorType createCommand;
  } accordance[] = { {"add", &createAddRecord},
                     {"store", &createStoreBookmark},
                     {"insert", &createInsert},
                     {"delete", &createDeleteRecord},
                     {"show", &createShowRecord},
                     {"move", &createMoveBookmark}
                   };

  std::string commandName;
  in >> commandName;

  auto accordanceElement = std::find_if(accordance, std::end(accordance),
      [&](const auto& accordanceElement){ return (accordanceElement.name == commandName); });

  if (accordanceElement != std::end(accordance))
  {
    return accordanceElement->createCommand(in);
  }

  return std::make_unique<CommandInvalid>();
}
