#ifndef PHONE_BOOK_HANDLER
#define PHONE_BOOK_HANDLER

#include <unordered_map>
#include <memory>
#include "phoneBook.hpp"

using bookmarksType = std::unordered_map<std::string, PhoneBook::iterator>;

class PhoneBookCommand
{
public:
  virtual ~PhoneBookCommand() = default;
  virtual void execute(PhoneBook &phoneBook, bookmarksType &bookmarks, std::ostream &out) = 0;
};

class CommandAddRecord: public PhoneBookCommand
{
public:
  CommandAddRecord(const PhoneBook::valueType &record);
  void execute(PhoneBook &phoneBook, bookmarksType &bookmarks, std::ostream &out) override;
private:
  PhoneBook::valueType record_;
};

class CommandStoreBookmark: public PhoneBookCommand
{
public:
  CommandStoreBookmark(const std::string &nameMark, const std::string &newNameMark);
  void execute(PhoneBook &phoneBook, bookmarksType &bookmarks, std::ostream &out) override;
private:
  std::string nameMark_;
  std::string newNameMark_;
};

class CommandInsertBeforeBookmark: public PhoneBookCommand
{
public:
  CommandInsertBeforeBookmark(const std::string &nameMark, const PhoneBook::valueType &record);
  void execute(PhoneBook &phoneBook, bookmarksType &bookmarks, std::ostream &out) override;
private:
  std::string nameMark_;
  PhoneBook::valueType record_;
};

class CommandInsertAfterBookmark: public PhoneBookCommand
{
public:
  CommandInsertAfterBookmark(const std::string &nameMark, const PhoneBook::valueType &record);
  void execute(PhoneBook &phoneBook, bookmarksType &bookmarks, std::ostream &out) override;
private:
  std::string nameMark_;
  PhoneBook::valueType record_;
};

class CommandDeleteRecord: public PhoneBookCommand
{
public:
  CommandDeleteRecord(const std::string &nameMark);
  void execute(PhoneBook &phoneBook, bookmarksType &bookmarks, std::ostream &out) override;
private:
  std::string nameMark_;
};

class CommandShowRecord: public PhoneBookCommand
{
public:
  CommandShowRecord(const std::string &nameMark);
  void execute(PhoneBook &phoneBook, bookmarksType &bookmarks, std::ostream &out) override;
private:
  std::string nameMark_;
};

class CommandMoveBookmark: public PhoneBookCommand
{
public:
  CommandMoveBookmark(const std::string &nameMark, std::string steps);
  void execute(PhoneBook &phoneBook, bookmarksType &bookmarks, std::ostream &out) override;
private:
  std::string nameMark_;
  std::string steps_;
};

class CommandInvalid: public PhoneBookCommand
{
public:
  void execute(PhoneBook &phoneBook, bookmarksType &bookmarks, std::ostream &out) override;
};

#endif
