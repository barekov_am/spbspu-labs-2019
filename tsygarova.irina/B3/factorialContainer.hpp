#ifndef CONTAINER
#define CONTAINER

#include <iterator>

class FactorialContainer
{
public:
  class IteratorFactorial: public std::iterator<std::bidirectional_iterator_tag, int,  std::ptrdiff_t, int*, int>
  {
  public:
    IteratorFactorial(int index);

    int& operator*();

    IteratorFactorial& operator++();
    IteratorFactorial& operator--();

    IteratorFactorial operator++(int);
    IteratorFactorial operator--(int);

    bool operator!=(const IteratorFactorial &other) const;
    bool operator==(const IteratorFactorial &other) const;

  private:
    int index_;
    int value_;
    const int upperBound = 11;
  };

  IteratorFactorial begin();
  IteratorFactorial end();
};

#endif
