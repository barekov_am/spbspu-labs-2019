#include <iostream>
#include "phoneBookHandler.hpp"

std::unique_ptr<PhoneBookCommand> getCommand(std::istream &in);

void iterator1()
{
  std::string line;
  PhoneBook phoneBook;
  bookmarksType bookmarks;
  bookmarks.insert(std::make_pair("current", phoneBook.end()));

  while (!std::cin.eof())
  {
    auto command = getCommand(std::cin);
    if(std::cin.eof())
    {
      return;
    }
    command->execute(phoneBook, bookmarks, std::cout);
  }
}
