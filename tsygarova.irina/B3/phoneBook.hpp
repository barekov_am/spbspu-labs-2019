#ifndef PHONE_BOOK
#define PHONE_BOOK

#include <utility>
#include <list>
#include <vector>
#include <string>

class PhoneBook
{
public:
  using valueType = std::pair<std::string, std::string>;
  using iterator = std::list<valueType>::iterator;

  iterator insert(iterator pos, const valueType &record);
  void pushBack(const valueType &record);

  void replace(iterator pos, valueType record);
  iterator erase(iterator pos);

  void show(iterator pos);

  iterator begin();
  iterator end();

  int size();
  bool empty();

private:
  std::list<valueType> records_;
};

#endif
