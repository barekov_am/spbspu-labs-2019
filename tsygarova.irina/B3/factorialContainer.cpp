#include "factorialContainer.hpp"

FactorialContainer::IteratorFactorial FactorialContainer::begin()
{
  return IteratorFactorial(1);
}

FactorialContainer::IteratorFactorial FactorialContainer::end()
{
  return IteratorFactorial(11);
}
