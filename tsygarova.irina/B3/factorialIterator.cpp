#include "factorialContainer.hpp"
#include <stdexcept>
#include <iostream>

FactorialContainer::IteratorFactorial::IteratorFactorial(int index):
  index_(index)
{
  if (index_ > upperBound)
  {
    throw std::out_of_range("Index is outside.\n");
  }

  if (index_ <= 0)
  {
    throw std::out_of_range("Invalid index.\n");
  }

  value_ = 1;
  for (int i = 2; i <= index_; ++i)
  {
    value_ = value_ * i;
  }
}

int &FactorialContainer::IteratorFactorial::operator*()
{
  return value_;
}

bool FactorialContainer::IteratorFactorial::operator==(const IteratorFactorial &other) const
{
  return (index_ == other.index_);
}

bool FactorialContainer::IteratorFactorial::operator!=(const IteratorFactorial &other) const
{
  return !(*this == other);
}

FactorialContainer::IteratorFactorial &FactorialContainer::IteratorFactorial::operator++()
{
  if (index_ == upperBound)
  {
    throw std::out_of_range("Index is outside.\n");
  }

  ++(index_);
  value_ *= index_;
  return (*this);
}

FactorialContainer::IteratorFactorial &FactorialContainer::IteratorFactorial::operator--()
{
  if (index_ == 0)
  {
    throw std::out_of_range("Invalid index.\n");
  }

  value_ /= index_;
  --(index_);
  return (*this);
}

FactorialContainer::IteratorFactorial FactorialContainer::IteratorFactorial::operator++(int)
{
  IteratorFactorial temp(*this);
  this->operator++();
  return --temp;
}

FactorialContainer::IteratorFactorial FactorialContainer::IteratorFactorial::operator--(int)
{
  IteratorFactorial temp(*this);
  this->operator--();
  return temp;
}
