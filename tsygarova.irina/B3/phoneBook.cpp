#include "phoneBook.hpp"
#include <iostream>

PhoneBook::iterator PhoneBook::insert(iterator pos, const valueType &record)
{
  return records_.insert(pos, record);
}

void PhoneBook::pushBack(const valueType &record)
{
  records_.push_back(record);
}

void PhoneBook::replace(iterator pos, valueType record)
{
  records_.insert(records_.erase(pos), record);
}

PhoneBook::iterator PhoneBook::erase(iterator pos)
{
  return records_.erase(pos);
}

void PhoneBook::show(iterator pos)
{
  std::cout << pos->first << " " << pos->second << "\n";
}

PhoneBook::iterator PhoneBook::begin()
{
  return records_.begin();
}

PhoneBook::iterator PhoneBook::end()
{
  return records_.end();
}

int PhoneBook::size()
{
  return records_.size();
}

bool PhoneBook::empty()
{
  return records_.empty();
}
