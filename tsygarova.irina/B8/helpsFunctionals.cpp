#include "helpsFunctionals.hpp"

#include <algorithm>
#include <sstream>
#include <iostream>

std::string readPunctuation(char symbol)
{
  std::string value;
  value.push_back(symbol);
  return value;
}


bool notPunctuation(std::vector<std::string> &vec)
{
  for (unsigned long i = 0; i < vec.size() - 1; i++)
  {
    if ((vec[i + 1][0] == '-') && (vec[i][0] == ','))
    {
      vec[i + 1].insert(0, " ");
    }
    else if (ispunct(vec[i + 1][0]) && (ispunct(vec[i][0])))
    {
      return true;
    }
  }
  return false;
}


std::string readNumber(char symbol)
{
  int count = 0;
  std::string value;
  value.push_back(symbol);

  while ((std::cin.peek() == '.') || std::isdigit(std::cin.peek()))
  {
    char dot = std::cin.get();
    if (dot == '.')
    {
      ++count;
      if (count > 1)
      {
        throw std::invalid_argument("Error dots in number\n");
      }
    }
    value.push_back(dot);
  }

  if (value.length() > 20)
  {
    throw std::invalid_argument("Error number length\n");
  }
  return value;
}


std::string readWord(char symbol)
{
  std::string value;
  value.push_back(symbol);

  while ((std::cin.peek() == '-') || std::isalpha(std::cin.peek()))
  {
    char hyphen = std::cin.get();
    if (hyphen == '-' && std::cin.peek() == '-')
    {
      throw std::invalid_argument("Error hephen\n");
    }
    value.push_back(hyphen);
  }

  if (value.length() > 20)
  {
    throw std::invalid_argument("Error word length\n");
  }
  return value;
}


std::string readDash(char symbol)
{
  std::string value;
  value.push_back(symbol);

  while (std::cin.peek() == '-')
  {
    char ch = std::cin.get();
    value.push_back(ch);
  }

  if (value.length() != 3)
  {
    throw std::invalid_argument("Error dash\n");
  }
  return value;
}
