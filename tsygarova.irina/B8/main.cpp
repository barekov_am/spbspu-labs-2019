#include "textWork.hpp"

#include <iostream>
#include <stdexcept>


int main(int argc, char *argv[])
{
  try
  {
    if ((argc > 3) || (argc == 2))
    {
      throw std::invalid_argument("Error number of arguments\n");
    }

    unsigned long length = 40;

    if (argc == 3)
    {
      length = std::atoi(argv[2]);

      if (length < 25)
      {
        throw std::invalid_argument("Error arguments\n");
      }
    }

    Texting file(length);
    file.outputText();
  }

  catch (const std::exception &exception)
  {
    std::cerr << exception.what() << '\n';
    return 1;
  }

  return 0;
}
