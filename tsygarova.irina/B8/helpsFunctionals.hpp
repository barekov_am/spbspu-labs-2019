#ifndef HELPSFUNCTIONALS
#define HELPSFUNCTIONALS
#include "textWork.hpp"

#include <vector>
#include <string>

bool notPunctuation(std::vector<std::string> &vec);
std::string readNumber(char symbol);
std::string readWord(char symbol);
std::string readPunctuation(char symbol);
std::string readDash(char symbol);

#endif
