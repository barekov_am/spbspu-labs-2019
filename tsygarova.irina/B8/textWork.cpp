#include "textWork.hpp"
#include "helpsFunctionals.hpp"

#include <algorithm>
#include <sstream>
#include <iostream>

Texting::Texting(unsigned long length):
  m_criticalLength(length),
  m_strings()
{
}


void Texting::inputText()
{
  std::string line;

  while (std::cin)
  {
    std::cin >> std::ws;

    char thisCh = std::cin.get();
    char nextCh = std::cin.peek();

    if ((std::isdigit(thisCh)) || ((thisCh == '+' || thisCh == '-') && std::isdigit(nextCh)))
    {
      m_strings.push_back(readNumber(thisCh));
    }
    else if (std::isalpha(thisCh))
    {
      m_strings.push_back(readWord(thisCh));
    }
    else if (std::ispunct(thisCh))
    {
      if (thisCh == '-')
      {
        m_strings.push_back(readDash(thisCh));
      }
      else
      {
        m_strings.push_back(readPunctuation(thisCh));
      }
    }
  }
}


void Texting::outputText()
{
  inputText();

  if (m_strings.empty())
  {
    return;
  }

  if (notPunctuation(m_strings))
  {
    throw std::invalid_argument("Error punctuation\n");
  }

  if (((m_strings[0][0] == '-') && (m_strings[0][1] == '-')) ||
    (ispunct(m_strings[0][0]) && (m_strings[0][0] != '+') && (m_strings[0][0] != '-')))
  {
    throw std::invalid_argument("Error test begin\n");
  }

  std::for_each(m_strings.begin(), m_strings.end(), [&](std::string &symbol)
  { if (isdigit(symbol[0]) || isalpha(symbol[0]) ||
      (symbol[0] == '-') || ((symbol[0] == '+'))) { symbol.insert(0, " "); } });

  bool lineStart = true;
  int intLength = 0;

  for (unsigned long i = 0; i < m_strings.size() - 1; ++i)
  {
    if (lineStart)
    {
      lineStart = false;
      m_strings[i].erase(0, 1);
    }

    if ((ispunct(m_strings[i + 1][0])) && (m_strings[i].size() + m_strings[i + 1].size()
         + intLength >  m_criticalLength))
    {
      std:: cout << "\n" << m_strings[i].erase(0, 1);
      intLength = m_strings[i].size();
      continue;
    }
    else if ((m_strings[i + 1][1] == '-') && (m_strings[i].size() + m_strings[i + 1].size()
        + intLength >  m_criticalLength))
    {
      std::cout << "\n" << m_strings[i].erase(0, 1);
      intLength = m_strings[i].size();
      continue;
    }

    if (((intLength + m_strings[i].size()) >  m_criticalLength))
    {
      lineStart = false;
      std::cout << "\n" << m_strings[i].erase(0, 1);
      intLength = m_strings[i].size();
    }
    else if ((intLength + m_strings[i].size()) <  m_criticalLength)
    {
      lineStart = false;
      std::cout << m_strings[i];
      intLength += m_strings[i].size();
    }
    else if (((intLength + m_strings[i].size()) ==  m_criticalLength))
    {
      lineStart = true;
      std::cout << m_strings[i]<< std::endl;
      intLength = 0;
    }
  }

  if (m_strings[m_strings.size() - 1].size() + intLength <=  m_criticalLength)
  {
    if (lineStart)
      std::cout << m_strings[m_strings.size() - 1].erase(0, 1);
    else
      std::cout << m_strings.back();
  }
  else if (m_strings[m_strings.size() - 1].size() + intLength >  m_criticalLength)
  {
    std::cout << "\n" << m_strings[m_strings.size() - 1].erase(0, 1);
  }
  std::cout << std::endl;
}
