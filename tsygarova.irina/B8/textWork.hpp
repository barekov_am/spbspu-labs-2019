#ifndef TEXTWORK
#define TEXTWORK

#include <vector>
#include <string>

class Texting
{
public:
    Texting(unsigned long length);
    void inputText();
    void outputText();
private:
    unsigned long m_criticalLength;
    std::vector<std::string> m_strings;
};

#endif
