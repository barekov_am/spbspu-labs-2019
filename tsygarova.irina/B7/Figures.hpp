#ifndef FIGURES
#define FIGURES

#include "Shape.hpp"
#include <iostream>

class Square: public Shape
{
public:
  Square(double x, double y);
  void draw() const override;
};

class Circle: public Shape
{
public:
  Circle(double x, double y);
  void draw() const override;
};

class Triangle: public Shape
{
public:
  Triangle(double x, double y);
  void draw() const override;
};

#endif
