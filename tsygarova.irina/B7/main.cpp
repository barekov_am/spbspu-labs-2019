#include "tasks.hpp"
#include <iostream>
#include <string>

int main(int args, char *argv[])
{
  if(args != 2)
  {
    std::cerr << "Invalid number of arguments\n";
    return 1;
  }

  try
  {
    int task = std::stoi(argv[1]);

    switch(task)
    {
    case 1:
      task1(std::cin, std::cout);
      break;
    case 2:
      task2();
      break;
    default:
      std::cerr << "Error option\n";
      return 1;
    }
  }
  catch(const std::invalid_argument &exseption)
  {
    std::cerr << exseption.what();
    return 1;
  }
  return 0;
}
