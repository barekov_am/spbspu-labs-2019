#include "tasks.hpp"
#include "Shape.hpp"
#include "Figures.hpp"

#include <string>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <memory>
#include <sstream>
#include <list>

typedef std::shared_ptr<Shape> ShapePtr;

void correctData(std::string &shapeName)
{
  std::stringstream bufer(shapeName);
  std::string temp_str, str1;
  while (!bufer.eof())
  {
    bufer >> temp_str;
    str1 += temp_str;
  }
  shapeName = str1;
}

void task2()
{
  ShapePtr shapePt;
  std::string str, shapePoint;
  std::list<ShapePtr> shapeList;
  std::stringstream buf;

  while (std::getline(std::cin,str))
  {
    buf.clear();
    correctData(str);

    if (str.empty())
    {
      continue;
    }

    buf.str(str);
    std::getline(buf, str, '(');
    buf.unget();
    buf >> shapePoint;

    if (str.empty() || shapePoint.empty() || !buf.eof())
    {
      throw std::invalid_argument("Error input\n");
    }

    buf.clear();
    buf.str(shapePoint);

    char symbol1[] = "---";
    char symbol2[] = "(;)";
    double x = 0, y = 0;
    buf >> symbol1[0] >> x >> symbol1[1] >> y >> symbol1[2];

    if (std::string(symbol1) != std::string(symbol2))
    {
      throw std::invalid_argument("Error input\n");
    }

    if (str == std::string("CIRCLE"))
    {
      shapePt = ShapePtr(new Circle(x, y));
    }
    else if (str == std::string("TRIANGLE"))
    {
      shapePt = ShapePtr(new Triangle(x, y));
    }
    else if (str == std::string("SQUARE"))
    {
      shapePt = ShapePtr(new Square(x, y));
    }
    else
    {
      throw std::invalid_argument("Error input\n");
    }
    shapeList.push_back(shapePt);
  }

  std::cout << "\nOriginal: " << std::endl;
  std::for_each(shapeList.begin(), shapeList.end(),
     [](const ShapePtr shape) { shape->draw(); });

  shapeList.sort([](ShapePtr first, ShapePtr second) { return first->isMoreLeft(*second); });
  std::cout << "\nLeft-Right: " << std::endl;
  std::for_each(shapeList.begin(), shapeList.end(),
     [](const ShapePtr shape) { shape->draw(); });

  shapeList.sort([](ShapePtr first, ShapePtr second) { return !(first->isMoreLeft(*second)); });
  std::cout << "\nRight-Left: " << std::endl;
  std::for_each(shapeList.begin(), shapeList.end(),
     [](const ShapePtr shape) { shape->draw(); });

  shapeList.sort([](ShapePtr first, ShapePtr second) { return first->isUpper(*second); });
  std::cout << "\nTop-Bottom: " << std::endl;
  std::for_each(shapeList.begin(), shapeList.end(),
     [](const ShapePtr shape) { shape->draw(); });

  shapeList.sort([](ShapePtr first, ShapePtr second) { return !(first->isUpper(*second)); });
  std::cout << "\nBottom-Top: " << std::endl;
  std::for_each(shapeList.begin(), shapeList.end(),
     [](const ShapePtr shape) { shape->draw(); });
}
