#ifndef SHAPE
#define SHAPE

#include <iostream>

class Shape
{
public:
  Shape(double x, double y);
  virtual ~Shape() = default;
  bool isMoreLeft(const Shape &tmp) const;
  bool isUpper(const Shape &tmp) const;
  virtual void draw() const = 0;
protected:
  double x_, y_;
};

#endif
