#include "Figures.hpp"

Square::Square(double x, double y):
  Shape(x, y)
{
}

void Square::draw() const
{
  std::cout << "SQUARE (" << x_ << ";" << y_ << ")" << std::endl;
}

Circle::Circle(double x, double y):
  Shape(x, y)
{
}

void Circle::draw() const
{
  std::cout << "CIRCLE (" << x_ << ";" << y_ << ")" << std::endl;
}

Triangle::Triangle(double x, double y):
  Shape(x, y)
{
}

void Triangle::draw() const
{
  std::cout << "TRIANGLE (" << x_ << ";" << y_ << ")" << std::endl;
}
