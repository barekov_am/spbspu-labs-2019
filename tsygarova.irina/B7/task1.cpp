#define _USE_MATH_DEFINES

#include "tasks.hpp"
#include <iostream>
#include <math.h>
#include <iterator>
#include <algorithm>
#include <list>

class Multiply
{
public:
  Multiply(double data):
    data_(data)
  {
  }

  void operator() (double &number)
  {
    number *= data_;
  }
private:
  double data_;
};

void task1(std::istream &inp, std::ostream &out)
{
  std::list<double> numbersList;
  std::copy(std::istream_iterator<double>(inp), std::istream_iterator<double>(),
    std::back_inserter(numbersList));

  if(!inp.eof())
  {
    throw std::invalid_argument("Error input\n");
  }

  std::for_each(numbersList.begin(), numbersList.end(), Multiply(M_PI));

  std::copy(numbersList.begin(), numbersList.end(), std::ostream_iterator<double>(out, " "));
}
