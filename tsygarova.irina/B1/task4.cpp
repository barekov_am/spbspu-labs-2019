#include <vector>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include "sort.hpp"
#include "output.hpp"

void fillRandom(double *array, int size)
{
  for (int i = 0; i < size; ++i)
  {
    array[i] = (-10 + rand() % 21) / 10.0;
  }
}

void task4(const char *order, long size)
{
  if (size < 0)
  {
    std::cerr << "Size must be a positive number.\n";
  }

  if (!size)
  {
    return;
  }

  auto collating = submissionForSorting<double>(order);

  std::vector<double> vector(size, 0);
  fillRandom(&vector[0], size);

  std::cout << vector;

  sorting(vector, collating);

  std::cout << vector;
}
