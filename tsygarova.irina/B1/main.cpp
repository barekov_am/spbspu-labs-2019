#include <exception>
#include <iostream>
#include <cstdlib>


void task1(const char* order);
void task2(const char* file);
void task3();
void task4(const char* order, long size);

int main(int argc, char* argv[])
{
  try
  {
    if (argc <= 1)
    {
      std::cerr << "Invalid number of arguments.\n";
      return 1;
    }

    char *tail = nullptr;
    long task = std::strtol(argv[1], &tail, 10);

    if (*tail)
    {
      std::cerr << "Invalid task.\n";
      return 1;
    }

    switch (task)
    {
      case 1:
      {
        if (argc != 3)
        {
          std::cerr << "Invalid number of arguments.\n";
          return 1;
        }
        if (argv[2] == nullptr)
        {
          std::cerr << "Invalid parameter\n";
          return 1;
        }
    
        task1(argv[2]);
        break;
      }
      case 2:
      {
        if (argc != 3)
        {
          std::cerr << "Invalid arguments number.\n";
          return 1;
        }

        task2(argv[2]);
        break;
      }
      case 3:
      {
        if (argc != 2)
        {
          std::cerr << "Invalid arguments number.\n";
          return 1;
        }

        task3();
        break;
      }
      case 4:
      {
        if (argc != 4)
        {
          std::cerr << "Invalid arguments number.\n";
          return 1;
        }
        if (argv[2] == nullptr)
        {
          std::cerr << "Invalid parametr\n";
          return 1;
        }
    
        char *tail2 = nullptr;
        long number = std::strtol(argv[3], &tail2, 10);

        if (*tail2)
        {
          std::cerr << "Invalid elements number.\n";
          return 1;
        }

        task4(argv[2], number);
        break;
      }
      default:
      {
        std::cerr << "Invalid task.\n";
        return 1;
      }
   }
}
  catch (const std::exception &exception)
  {
    std::cerr << exception.what();
    return 1;
  }
  return 0;
}
