#include <vector>
#include <forward_list>
#include "sort.hpp"
#include "output.hpp"

void task1(const char *order)
{
  auto collating = submissionForSorting<int>(order);

  std::vector<int> vectorBrackets;

  int cur = 0;

  while (std::cin >> cur, !std::cin.eof())
  {
    if (!std::cin.eof() && std::cin.fail())
    {
      throw std::runtime_error("Errors Input.\n");
    }
    vectorBrackets.push_back(cur);
  }

  if (vectorBrackets.empty())
  {
    return;
  }
 
  std::vector<int> vectorAt(vectorBrackets);
  std::forward_list<int> list(vectorBrackets.begin(), vectorBrackets.end());

  sorting<SortBrackets>(vectorBrackets, collating);
  sorting<SortAt>(vectorAt, collating);
  sorting<SortIterators>(list, collating);

  std::cout << vectorBrackets << vectorAt << list;
}
