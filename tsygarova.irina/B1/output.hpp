#ifndef OUTPUT
#define OUTPUT
#include <iostream>

template <typename T, typename = typename T::iterator>
std::ostream& operator<<(std::ostream &out, const T &container)
{
  for (auto i = container.begin(); i != container.end(); ++i)
  {
    out << *i << " ";
  }

  out << "\n";
  return out;
}

#endif 
