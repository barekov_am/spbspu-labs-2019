#ifndef ALGORITHM
#define ALGORITHM

template <typename Container>
struct SortBrackets
{
  using valueType = typename Container::value_type;

  static int begin(Container&)
  {
    return 0;
  }

  static int end(Container& container)
  {
    return container.size();
  }

  static valueType& getIndex(Container& container, int index)
  {
    return container[index];
  }
};

template <typename Container>
struct SortAt
{
  using valueType = typename Container::value_type;

  static int begin(Container&)
  {
    return 0;
  }

  static int end(Container& container)
  {
    return container.size();
  }

  static valueType& getIndex(Container& container, int index)
  {
    return container.at(index);
  }
};

template <typename Container>
struct SortIterators
{
  using valueType = typename Container::value_type;
  using index_type = typename Container::iterator;

  static index_type begin(Container& container)
  {
    return container.begin();
  }

  static index_type end(Container& container)
  {
    return container.end();
  }

  template <typename Iterator>
  static valueType& getIndex(Container&, Iterator it)
  {
    return (*it);
  }

};

#endif
