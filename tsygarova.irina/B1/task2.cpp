#include <fstream>
#include <vector>
#include <stdlib.h>
#include <memory>
#include "output.hpp"

void task2(const char *file)
{
  std::ifstream inp(file);

  if (!inp.is_open())
  {
    std::cerr << "Error open the file.\n";
  }

  int power = 10;

  std::unique_ptr<char[], decltype(&free)> array(static_cast<char*>(calloc(power, 1)), free);

  if (!array)
  {
    std::cerr << "Error allocate memory.\n";
  }

  int size = 0;

  while (inp.read(&array[size], 1), !inp.eof())
  {
    size++;

    if (size == power)
    {
       power *= 2;
       std::unique_ptr<char[], decltype(&free)> tmp(static_cast<char*>(realloc(array.get(), power)), free);

       if (!tmp)
       {
        inp.close();
        std::cerr << "Error allocate memory.\n";
       }

       array.release();
       std::swap(array, tmp);
     }

     if (!inp.eof() && inp.fail())
     {
       throw std::runtime_error("Error Input.\n");
     }
  }

  inp.close();

  if (inp.is_open())
  {
    std::cerr << "Error close the file.\n";
  }

  if (!size)
  {
    return;
  }

  std::vector<char> vector(&array[0], &array[size - 1]);

  for (auto element: vector)
  {
    std::cout << element;
  }

  std::cout << "\n";
}
