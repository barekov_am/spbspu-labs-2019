#ifndef SORTING
#define SORTING
#include <functional>
#include <iostream>
#include "algorithm.hpp"

template <typename Element>
std::function<bool (Element, Element)> submissionForSorting(const std::string &order)
{
  if (order == "ascending")
  {
    return [](Element a, Element b)
    {
      return a < b;
    };
  }

  if (order == "descending")
  {
    return [](Element a, Element b)
    {
      return a > b;
    };
  }
  throw std::invalid_argument("Expected ascending or descending.\n");
}

template <template <typename> typename Sort = SortIterators, typename Container>
void sorting(Container& collection, std::function<bool(typename Container::value_type, typename Container::value_type)> compare)
{
  auto outside = Sort<Container>::begin(collection);
  auto end = Sort<Container>::end(collection);

  while (outside != end)
  {
    auto inside = outside;
    while (inside != end)
    {
      typename Sort<Container>::valueType& prev = Sort<Container>::getIndex(collection, inside);
      typename Sort<Container>::valueType& cur = Sort<Container>::getIndex(collection, outside);

      if (compare(prev, cur))
      {
        std::swap(prev, cur);
      }

      inside++;
    }
    outside++;
  }
}
#endif
