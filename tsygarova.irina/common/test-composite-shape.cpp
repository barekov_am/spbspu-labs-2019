#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(testCompositeShape)

const double EPSILON = 0.01;

BOOST_AUTO_TEST_CASE(compositeShapeCopyConstructor)
{
  tsygarova::Rectangle testRectangle({2, 12, {4, 7}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::Circle testCircle(10, {5, 8});
  tsygarova::ptr circPtr = std::make_shared<tsygarova::Circle>(testCircle);
  tsygarova::CompositeShape compositeSh(rectPtr);
  compositeSh.add(circPtr);
  const tsygarova::rectangle_t frameRect = compositeSh.getFrameRect();

  tsygarova::CompositeShape copyCompSh(compositeSh);
  const tsygarova::rectangle_t copyFrameRect = copyCompSh.getFrameRect();
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(compositeSh.getArea(), copyCompSh.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(compositeSh.getCount(), copyCompSh.getCount());
}

BOOST_AUTO_TEST_CASE(compositeShapeCopyOperator)
{
  tsygarova::Rectangle testRectangle({3, 8, {4, 6}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::Circle testCircle(10, {7, 8});
  tsygarova::ptr circPtr = std::make_shared<tsygarova::Circle>(testCircle);
  tsygarova::CompositeShape compositeSh(rectPtr);
  compositeSh.add(circPtr);
  const tsygarova::rectangle_t frameRect = compositeSh.getFrameRect();

  tsygarova::CompositeShape copyCompSh(rectPtr);
  copyCompSh = compositeSh;
  const tsygarova::rectangle_t copyFrameRect = copyCompSh.getFrameRect();
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(compositeSh.getArea(), copyCompSh.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(compositeSh.getCount(), copyCompSh.getCount());
}

BOOST_AUTO_TEST_CASE(compositeShapeMoveConstructor)
{
  tsygarova::Rectangle testRectangle({3, 8, {4, 6}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::Circle testCircle(5, {1, 3});
  tsygarova::ptr circPtr = std::make_shared<tsygarova::Circle>(testCircle);
  tsygarova::CompositeShape compositeSh(rectPtr);
  compositeSh.add(circPtr);
  const tsygarova::rectangle_t frameRect = compositeSh.getFrameRect();
  const double compShArea = compositeSh.getArea();
  const int compShCount = compositeSh.getCount();

  tsygarova::CompositeShape moveCompSh(std::move(compositeSh));
  const tsygarova::rectangle_t moveFrameRect = moveCompSh.getFrameRect();

  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(compShArea, moveCompSh.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(compShCount, moveCompSh.getCount());
  BOOST_CHECK_CLOSE(compositeSh.getArea(), 0, EPSILON);
  BOOST_CHECK_EQUAL(compositeSh.getCount(), 0);
}

BOOST_AUTO_TEST_CASE(compositeShapeMoveOperator)
{
  tsygarova::Rectangle testRectangle({3, 8, {4, 6}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::Circle testCircle(5, {1, 3});
  tsygarova::ptr circPtr = std::make_shared<tsygarova::Circle>(testCircle);
  tsygarova::CompositeShape movedCompSh(rectPtr);
  movedCompSh.add(circPtr);
  const tsygarova::rectangle_t frameRect = movedCompSh.getFrameRect();
  const double compShArea = movedCompSh.getArea();
  const int compShCount = movedCompSh.getCount();

  tsygarova::CompositeShape compositeSh(rectPtr);
  compositeSh = std::move(movedCompSh);
  const tsygarova::rectangle_t moveFrameRect = compositeSh.getFrameRect();
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(compShArea, compositeSh.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(compShCount, compositeSh.getCount());
  BOOST_CHECK_EQUAL(movedCompSh.getCount(), 0);
  BOOST_CHECK_CLOSE(movedCompSh.getArea(), 0, EPSILON);
}

BOOST_AUTO_TEST_CASE(compositeShapeConstancyOfParameters)
{
  tsygarova::Rectangle testRectangle({3, 8, {4, 6}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::Circle testCircle(5, {1, 3});
  tsygarova::ptr circPtr = std::make_shared<tsygarova::Circle>(testCircle);
  tsygarova::CompositeShape compositeSh(rectPtr);
  compositeSh.add(circPtr);
  const double areaBeforeMoving = compositeSh.getArea();
  const tsygarova::rectangle_t frameRectBeforeMoving = compositeSh.getFrameRect();
  compositeSh.move(3, 5);
  tsygarova::rectangle_t frameRectAfterMoving = compositeSh.getFrameRect();
  double areaAfterMoving = compositeSh.getArea();

  BOOST_CHECK_CLOSE(areaAfterMoving, areaBeforeMoving, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.height, frameRectAfterMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.width, frameRectAfterMoving.width, EPSILON);

  compositeSh.move({3, 4});
  frameRectAfterMoving = compositeSh.getFrameRect();
  areaAfterMoving = compositeSh.getArea();

  BOOST_CHECK_CLOSE(areaAfterMoving, areaBeforeMoving, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.height, frameRectAfterMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.width, frameRectAfterMoving.width, EPSILON);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleCoefficientMoreThanOne)
{
  tsygarova::Rectangle testRectangle({3, 8, {4, 6}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::Circle testCircle(5, {1, 3});
  tsygarova::ptr circPtr = std::make_shared<tsygarova::Circle>(testCircle);
  tsygarova::CompositeShape compositeSh(rectPtr);
  compositeSh.add(circPtr);
  const tsygarova::rectangle_t frameBeforeScale = compositeSh.getFrameRect();
  const int coefMoreThanOne = 3;
  compositeSh.scale(coefMoreThanOne);
  tsygarova::rectangle_t frameAfterScale = compositeSh.getFrameRect();

  BOOST_CHECK_CLOSE(frameBeforeScale.height * coefMoreThanOne, frameAfterScale.height, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeScale.width * coefMoreThanOne, frameAfterScale.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.x, frameAfterScale.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.y, frameAfterScale.pos.y, EPSILON);
  BOOST_CHECK(frameBeforeScale.height < frameAfterScale.height);
  BOOST_CHECK(frameBeforeScale.width < frameAfterScale.width);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleCoefficientLessThanOne)
{
  tsygarova::Rectangle testRectangle({3, 8, {4, 6}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::Circle testCircle(5, {1, 3});
  tsygarova::ptr circPtr = std::make_shared<tsygarova::Circle>(testCircle);
  tsygarova::CompositeShape compositeSh(rectPtr);
  compositeSh.add(circPtr);
  const tsygarova::rectangle_t frameBeforeScale = compositeSh.getFrameRect();
  const double coefLessThanOne = 0.3;
  compositeSh.scale(coefLessThanOne);
  tsygarova::rectangle_t frameAfterScale = compositeSh.getFrameRect();

  BOOST_CHECK_CLOSE(frameBeforeScale.height * coefLessThanOne, frameAfterScale.height, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeScale.width * coefLessThanOne, frameAfterScale.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.x, frameAfterScale.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.y, frameAfterScale.pos.y, EPSILON);
  BOOST_CHECK(frameBeforeScale.width > frameAfterScale.width);
  BOOST_CHECK(frameBeforeScale.height > frameAfterScale.height);
}

BOOST_AUTO_TEST_CASE(compositeShapeIncorrectScaleParameter)
{
  tsygarova::Rectangle testRectangle({3, 8, {4, 6}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::Circle testCircle(5, {1, 3});
  tsygarova::ptr circPtr = std::make_shared<tsygarova::Circle>(testCircle);
  tsygarova::CompositeShape compositeSh(rectPtr);
  compositeSh.add(circPtr);

  BOOST_CHECK_THROW(compositeSh.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShapeAreaAfterAddAndDelete)
{
  tsygarova::Rectangle testRectangle({3, 8, {4, 6}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::Circle testCircle(5, {1, 3});
  tsygarova::ptr circPtr = std::make_shared<tsygarova::Circle>(testCircle);
  tsygarova::CompositeShape compositeSh(rectPtr);
  const double compShAreaBeforeAdd = compositeSh.getArea();
  const double rectArea = testRectangle.getArea();
  const double circArea = testCircle.getArea();

  compositeSh.add(circPtr);
  double compShAreaAfterAdd = compositeSh.getArea();
  BOOST_CHECK_CLOSE(compShAreaBeforeAdd + circArea, compShAreaAfterAdd, EPSILON);

  compositeSh.remove(0);
  BOOST_CHECK_CLOSE(compShAreaAfterAdd - rectArea, compositeSh.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(compositeShapeAfterRotating)
{
  tsygarova::Rectangle testRectangle({3, 8, {4, 6}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::Circle testCircle(5, {1, 3});
  tsygarova::ptr circPtr = std::make_shared<tsygarova::Circle>(testCircle);
  tsygarova::CompositeShape compositeSh(rectPtr);
  compositeSh.add(circPtr);

  const double compShAreaBeforeRotating = compositeSh.getArea();
  const tsygarova::rectangle_t frameBeforeRotating = compositeSh.getFrameRect();
  
  compositeSh.rotate(90);
  double compShAreaAfterRotating = compositeSh.getArea();
  tsygarova::rectangle_t frameAfterRotating = compositeSh.getFrameRect();

  BOOST_CHECK_CLOSE(compShAreaBeforeRotating, compShAreaAfterRotating, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.x, frameAfterRotating.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.y, frameAfterRotating.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.width, frameAfterRotating.height, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.height, frameAfterRotating.width, EPSILON);
 
  compositeSh.rotate(-90);
  compShAreaAfterRotating = compositeSh.getArea();
  frameAfterRotating = compositeSh.getFrameRect();

  BOOST_CHECK_CLOSE(compShAreaBeforeRotating, compShAreaAfterRotating, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.x, frameAfterRotating.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.y, frameAfterRotating.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.width, frameAfterRotating.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.height, frameAfterRotating.height, EPSILON);
}

BOOST_AUTO_TEST_CASE(compositeShapeThrowingExeptions)
{
  tsygarova::Rectangle testRectangle({3, 8, {4, 6}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::Circle testCircle(5, {1, 3});
  tsygarova::ptr circPtr = std::make_shared<tsygarova::Circle>(testCircle);
  tsygarova::CompositeShape compositeSh(rectPtr);
  compositeSh.add(circPtr);

  BOOST_CHECK_THROW(compositeSh.remove(10), std::invalid_argument);
  BOOST_CHECK_THROW(compositeSh.remove(-10), std::invalid_argument);

  compositeSh.remove(1);
  BOOST_CHECK_THROW(compositeSh[1], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeTestThrowExceptionAfterUsingOfOperator)
{
  tsygarova::Rectangle testRectangle({3, 8, {4, 6}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::Circle testCircle(5, {1, 3});
  tsygarova::ptr circPtr = std::make_shared<tsygarova::Circle>(testCircle);
  tsygarova::CompositeShape compositeSh(rectPtr);
  compositeSh.add(circPtr);

  BOOST_CHECK_THROW(compositeSh[2], std::out_of_range);
  BOOST_CHECK_THROW(compositeSh[-2], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(emptyCompositeShapeThrowingExeptions)
{
  tsygarova::CompositeShape emptyCompSh;

  BOOST_CHECK_THROW(emptyCompSh.move(3, 5), std::logic_error);
  BOOST_CHECK_THROW(emptyCompSh.move({1, 1}), std::logic_error);
  BOOST_CHECK_THROW(emptyCompSh.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(emptyCompSh.scale(2), std::logic_error);
}

BOOST_AUTO_TEST_SUITE_END()
