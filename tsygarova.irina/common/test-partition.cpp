#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(partitionMetodsTests)

BOOST_AUTO_TEST_CASE(partitionTest)
{
  const tsygarova::Rectangle testRectangle1({2, 7, {9, 3}});
  tsygarova::ptr rect1Ptr = std::make_shared<tsygarova::Rectangle>(testRectangle1);
  const tsygarova::Rectangle testRectangle2({1, 3, {5, 2}});
  tsygarova::ptr rect2Ptr = std::make_shared<tsygarova::Rectangle>(testRectangle2);
  const tsygarova::Circle testCircle(10, {5, 8});
  tsygarova::ptr circPtr = std::make_shared<tsygarova::Circle>(testCircle);

  tsygarova::CompositeShape compositeSh(rect1Ptr);
  tsygarova::Matrix matrix1 = tsygarova::part(compositeSh);

  compositeSh.add(circPtr);
  tsygarova::Matrix matrix2 = tsygarova::part(compositeSh);

  compositeSh.add(rect2Ptr);
  tsygarova::Matrix matrix3 = tsygarova::part(compositeSh);

  BOOST_CHECK_EQUAL(matrix1.getLines(), 1);
  BOOST_CHECK_EQUAL(matrix1.getColumns(), 1);

  BOOST_CHECK_EQUAL(matrix2.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix2.getColumns(), 1);

  BOOST_CHECK_EQUAL(matrix3.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix3.getColumns(), 2);
}

BOOST_AUTO_TEST_CASE(intersectionTest)
{
  const tsygarova::Rectangle testRectangle1({5, 4, {1, 2}});
  const tsygarova::Rectangle testRectangle2({3, 1, {7, 7}});
  const tsygarova::Circle testCircle1(2, {-1, 2});
  const tsygarova::Circle testCircle2(4, {0, 1});

  BOOST_CHECK(intersect(testRectangle1.getFrameRect(), testCircle1.getFrameRect()));
  BOOST_CHECK(intersect(testRectangle1.getFrameRect(), testCircle2.getFrameRect()));
  BOOST_CHECK(!intersect(testRectangle1.getFrameRect(), testRectangle2.getFrameRect()));
  BOOST_CHECK(!intersect(testCircle1.getFrameRect(), testRectangle2.getFrameRect()));
  BOOST_CHECK(!intersect(testCircle2.getFrameRect(), testRectangle2.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
