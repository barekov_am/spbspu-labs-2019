#include <iostream>
#include <stdexcept>
#include <cmath>
#include <memory>
#include <algorithm>
#include "composite-shape.hpp"

tsygarova::CompositeShape::CompositeShape():
  count_(0)
{}

tsygarova::CompositeShape::CompositeShape(const CompositeShape &copyComposition):
  count_(copyComposition.count_),
  forms_(std::make_unique<ptr[]>(copyComposition.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    forms_[i] = copyComposition.forms_[i];
  }
}

tsygarova::CompositeShape::CompositeShape(CompositeShape &&moveComposition):
  count_(moveComposition.count_),
  forms_(std::move(moveComposition.forms_))
{
  moveComposition.count_ = 0;
}

tsygarova::CompositeShape::CompositeShape(const ptr &shape):
  count_(1),
  forms_(std::make_unique<ptr[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer can't be null");
  }
  forms_[0] = shape;
}

tsygarova::CompositeShape &tsygarova::CompositeShape::operator =(CompositeShape &&moveComposition)
{
  if (this != &moveComposition)
  {
    count_ = moveComposition.count_;
    forms_ = std::move(moveComposition.forms_);
    moveComposition.count_ = 0;
  }
  return *this;
}

tsygarova::CompositeShape &tsygarova::CompositeShape::operator =(const CompositeShape &copyComposition)
{
  if (this != &copyComposition)
  {
    count_ = copyComposition.count_;
    array shapesArray(std::make_unique<ptr[]>(count_ ));
    for (size_t i = 0; i < count_; i++)
    {
      shapesArray[i] = copyComposition.forms_[i];
    }
  forms_.swap(shapesArray);
  }
  return *this;
}

tsygarova::ptr tsygarova::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Error. Index out of range");
  }
  return forms_[index];
}

double tsygarova::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    area += forms_[i] -> getArea();
  }
  return area;
}

tsygarova::rectangle_t tsygarova::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Error. Empty composition");
  }
    
  rectangle_t workShape = forms_[0] -> getFrameRect();
  double minX = workShape.pos.x - workShape.width / 2;
  double maxX = workShape.pos.x + workShape.width / 2;
  double minY = workShape.pos.y - workShape.height / 2;
  double maxY = workShape.pos.y + workShape.height / 2;
    
  for (size_t i = 1; i < count_; i++)
  {
    workShape = forms_[i] -> getFrameRect();
    double workValue = workShape.pos.x - workShape.width / 2;
    minX = std::min(workValue, minX);
    workValue = workShape.pos.x + workShape.width / 2;
    maxX = std::max(workValue, maxX);
    workValue = workShape.pos.y - workShape.height / 2;
    minY = std::min(workValue, minY);
    workValue = workShape.pos.y + workShape.height / 2;
    maxY = std::max(workValue, maxY);
  }
  return {(maxX - minX), (maxY - minY), {(maxX + minX) / 2, (maxY + minY) / 2}};
}

void tsygarova::CompositeShape::move(const point_t &point)
{
  point_t frameRectPos = getFrameRect().pos;
  double dx = point.x - frameRectPos.x;
  double dy = point.y - frameRectPos.y;

  move(dx, dy);
}

void tsygarova::CompositeShape::move(double dx, double dy)
{
  if (count_ == 0)
  {
    throw std::logic_error("Error. Empty composition");
  }
    
  for (size_t i = 0; i < count_; i++)
  {
    forms_[i] -> move(dx, dy);
  }
}

void tsygarova::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Error. Invalid coefficient of scale\n");
  }

  const tsygarova::point_t frameRectPos = getFrameRect().pos;
  for (size_t i = 0; i < count_; i++)
  {
    tsygarova::point_t shapeCenter = forms_[i] -> getFrameRect().pos;
    double dx = (shapeCenter.x - frameRectPos.x) * (coefficient - 1);
    double dy = (shapeCenter.y - frameRectPos.y) * (coefficient - 1);
    forms_[i] -> move(dx, dy);
    forms_[i] -> scale(coefficient);
  }
}

void tsygarova::CompositeShape::add(ptr &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer can't be null");
  }

  array shapesArray(std::make_unique<ptr[]>(count_ + 1));
  for(size_t i = 0; i < count_; i++)
  {
    shapesArray[i] = forms_[i];
  }

  shapesArray[count_ ] = shape;
  count_++;
  forms_.swap(shapesArray);
}

void tsygarova::CompositeShape::remove(std::size_t index)
{
  if (index >= count_)
  {
    throw std::invalid_argument("Error. Invalid index\n");
  }

  count_--;
  for (size_t i = index; i < count_; i++)
  {
    forms_[i] = forms_[i + 1];
  }
  forms_[count_] = nullptr;
}

tsygarova::array tsygarova::CompositeShape::getForms() const
{
  array tmpForms(std::make_unique<ptr[]>(count_));

  for (size_t i = 0; i < count_; i++)
  {
    tmpForms[i] = forms_[i];
  }
  return tmpForms;
}

void tsygarova::CompositeShape::rotate(double angle)
{
  const double cosA = std::abs(std::cos(angle * M_PI / 180));
  const double sinA = std::abs(std::sin(angle * M_PI / 180));
  const point_t center = getFrameRect().pos;

  for (size_t i = 0; i < count_; i++)
  {
    point_t tmpCenter = forms_[i] -> getFrameRect().pos;
    const double dx = (tmpCenter.x - center.x) * (cosA - 1) - (tmpCenter.y - center.y)  * sinA;
    const double dy = (tmpCenter.x - center.x) * sinA + (tmpCenter.y - center.y)  * (cosA - 1);

    forms_[i] -> move(dx, dy);
    forms_[i] -> rotate(angle);
  }
}

void tsygarova::CompositeShape::print() const
{
  tsygarova::rectangle_t frame = getFrameRect();
  std::cout << "Center point of frame rectangle: (" << frame.pos.x
            << ";" << frame.pos.y << ")\n"
            << "Width of frame rectangle: " << frame.width << std::endl
            << "Height of frame rectangle: " << frame.height << std::endl
            << "Area = " << getArea() << std::endl;
}

size_t tsygarova::CompositeShape::getCount() const
{
  return count_;
}
