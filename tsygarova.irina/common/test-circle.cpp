#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>

#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testCircle)

const double EPSILON = 0.01;

BOOST_AUTO_TEST_CASE(circleConstancyAfterMoving)
{
  tsygarova::Circle testCircle(6, {2, 3});
  const tsygarova::rectangle_t initFrame = testCircle.getFrameRect();
  const double initArea = testCircle.getArea();

  testCircle.move(5, 9);
  tsygarova::rectangle_t currentFrame = testCircle.getFrameRect();
  double currentArea = testCircle.getArea();
  BOOST_CHECK_CLOSE(initFrame.width, currentFrame.width, EPSILON);
  BOOST_CHECK_CLOSE(initFrame.height, currentFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(initArea, currentArea, EPSILON);

  testCircle.move({9, 3});
  currentFrame = testCircle.getFrameRect();
  currentArea = testCircle.getArea();
  BOOST_CHECK_CLOSE(initFrame.width, currentFrame.width, EPSILON);
  BOOST_CHECK_CLOSE(initFrame.height, currentFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(initArea, currentArea, EPSILON);
}

BOOST_AUTO_TEST_CASE(circleScaling)
{
  tsygarova::Circle testCircle(6, {2, 3});
  const double initArea = testCircle.getArea();
  const double scaleCoefficient = 2;

  testCircle.scale(scaleCoefficient);
  double currentArea = testCircle.getArea();
  BOOST_CHECK_CLOSE(initArea * scaleCoefficient * scaleCoefficient, currentArea, EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidParametersCircle)
{
  BOOST_CHECK_THROW(tsygarova::Circle(-7, {3, 4}), std::invalid_argument);

  tsygarova::Circle testCircle(7, {3, 4});
  BOOST_CHECK_THROW(testCircle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
