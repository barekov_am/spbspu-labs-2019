#include "partition.hpp"
#include <cmath>

tsygarova::Matrix tsygarova::part(const array &figures, size_t count)
{
  Matrix matrix;
  size_t lines = 0;
  size_t columns = 0;

  for (size_t i = 0; i < count; i++)
  {
    for (size_t j = 0; j < matrix.getLines(); j++)
    {
      for (size_t k = 0; k < matrix.getColumns(); k++)
      {
        if (matrix[j][k] == nullptr)
        {
          lines = j;
          columns = k;
          break;
        }

        if (intersect(matrix[j][k]->getFrameRect(), figures[i]->getFrameRect()))
        {
          lines = j + 1;
          columns = 0;
          break;
        }
        lines = j;
        columns = k + 1;
      }
      if (lines == j)
      {
        break;
      }
    }
    matrix.add(figures[i], lines, columns);
  }
  return matrix;
}

tsygarova::Matrix tsygarova::part(const tsygarova::CompositeShape &compositeShape)
{
  return part(compositeShape.getForms(), compositeShape.getCount());
}

bool tsygarova::intersect(const tsygarova::rectangle_t &firstShape, const tsygarova::rectangle_t &secondShape)
{
  const double distanceBetweenCentersX = fabs(firstShape.pos.x - secondShape.pos.x);
  const double distanceBetweenCentersY = fabs(firstShape.pos.y - secondShape.pos.y);

  const double lengthX = (firstShape.width + secondShape.width) / 2;
  const double lengthY = (firstShape.height + secondShape.height) / 2;

  const bool firstCondition = (distanceBetweenCentersX < lengthX);
  const bool secondCondition = (distanceBetweenCentersY < lengthY);

  return firstCondition && secondCondition;
}
