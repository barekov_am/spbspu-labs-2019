#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace tsygarova
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t &point) = 0;
    virtual void move(const double dx, const double dy) = 0;
    virtual void print() const = 0;
    virtual void scale(const double coefficient) = 0;
    virtual void rotate(double) = 0;
  };
  using ptr = std::shared_ptr<Shape>;
  using array = std::unique_ptr<ptr[]>;
}

#endif
