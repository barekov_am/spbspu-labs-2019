#ifndef composite_shape_hpp
#define composite_shape_hpp

//#include <boost/mpl/size_t.hpp>
#include <memory>
#include "base-types.hpp"
#include "shape.hpp"

namespace tsygarova
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &copyComposition);
    CompositeShape(CompositeShape &&moveComposition);
    CompositeShape(const ptr &shape);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &copyComposition);
    CompositeShape &operator =(CompositeShape &&moveComposition);
    ptr operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(double dx, double dy) override;
    void scale(double coefficient) override;
    void add(ptr &shape);
    void remove(size_t index);
    size_t getCount() const;
    array getForms() const;
    void rotate(double angle) override;
    void print() const override;
  private:
    size_t count_;
    array forms_;
  };
}

#endif
