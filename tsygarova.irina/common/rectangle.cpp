#include <iostream>
#include <stdexcept>
#include <cmath>

#include "rectangle.hpp"

tsygarova::Rectangle::Rectangle(const rectangle_t &rect):
  rect_(rect),
  angle_(0)
{
  if ((rect_.height < 0.0) || (rect_.width < 0.0))
  {
    throw std::invalid_argument("Error. Invalid height or width of rectangle.\n");
  }
}

double tsygarova::Rectangle::getArea() const
{
  return rect_.width * rect_.height;
}

tsygarova::rectangle_t tsygarova::Rectangle::getFrameRect() const
{
  const double cosA = std::abs(std::cos(angle_ * M_PI / 180));
  const double sinA = std::abs(std::sin(angle_ * M_PI / 180));

  double frameWidth = rect_.height * sinA + rect_.width * cosA;
  double frameHeight = rect_.height * cosA + rect_.width * sinA;

  return {frameWidth, frameHeight, rect_.pos};
}

void tsygarova::Rectangle::move(const point_t &point)
{
  rect_.pos = point;
}

void tsygarova::Rectangle::move(const double dx, const double dy)
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

void tsygarova::Rectangle::print() const
{
  std::cout << "Area of rectangle: " << getArea() << std::endl;
  std::cout << "Width of frame rectangle: " << rect_.width << std::endl;
  std::cout << "Height of frame rectangle: " << rect_.height << std::endl;
  std::cout << "Center point of frame rectangle: (" << rect_.pos.x
            << "; " << rect_.pos.y << ")" << std::endl;
}

void tsygarova::Rectangle::scale(const double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Error.Invalid coefficient of scale.\n");
  }
  rect_.height *= coefficient;
  rect_.width *= coefficient;
}

void tsygarova::Rectangle::rotate (const double angle)
{
  angle_ += angle;
}
