#include "matrix.hpp"
#include <stdexcept>
#include <cmath>

tsygarova::Matrix::Matrix():
  lines_(0),
  columns_(0)
{}

tsygarova::Matrix::Matrix(const Matrix &copiedMatrix):
  lines_(copiedMatrix.lines_),
  columns_(copiedMatrix.columns_),
  forms_(std::make_unique<ptr[]>(copiedMatrix.lines_ * copiedMatrix.columns_))
{
  for (size_t i = 0; i < (lines_  * columns_); i++)
  {
    forms_[i] = copiedMatrix.forms_[i];
  }
}

tsygarova::Matrix::Matrix(tsygarova::Matrix &&movedMatrix):
  lines_(movedMatrix.lines_),
  columns_(movedMatrix.columns_),
  forms_(std::move(movedMatrix.forms_))
{
  movedMatrix.lines_ = 0;
  movedMatrix.columns_ = 0;
}

tsygarova::Matrix &tsygarova::Matrix::operator =(const Matrix &copiedMatrix)
{
  if (this != &copiedMatrix)
  {
    array tmpForms(std::make_unique<ptr[]>(copiedMatrix.lines_ * copiedMatrix.columns_));
    lines_ = copiedMatrix.lines_;
    columns_ = copiedMatrix.columns_;

    for (size_t i = 0; i < (lines_ * columns_); i++)
    {
      tmpForms[i] = copiedMatrix.forms_[i];
    }

    forms_.swap(tmpForms);
    }
  return *this;
}

tsygarova::Matrix &tsygarova::Matrix::operator =(Matrix &&movedMatrix)
{
  if (this != &movedMatrix)
  {
    lines_ = movedMatrix.lines_;
    columns_ = movedMatrix.columns_;
    forms_ = std::move(movedMatrix.forms_);
    movedMatrix.lines_ = 0;
    movedMatrix.columns_ = 0;
  }
  return *this;
}

tsygarova::array tsygarova::Matrix::operator [](size_t index) const
{
  if (index >= lines_)
  {
    throw std::out_of_range("Index out of range");
  }
  array tmpForms(std::make_unique<ptr[]>(columns_));

  for (size_t i = 0; i < getLineSize(index); i++)
  {
    tmpForms[i] = forms_[index * columns_ + i];
  }

  return tmpForms;
}

bool tsygarova::Matrix::operator==(const Matrix &shapesMatrix) const
{
  if ((lines_ != shapesMatrix.lines_) || (columns_ != shapesMatrix.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    if (forms_[i] != shapesMatrix.forms_[i])
    {
      return false;
    }
  }
  return true;
}

bool tsygarova::Matrix::operator!=(const Matrix &shapesMatrix) const
{
  return !(*this == shapesMatrix);
}

void tsygarova::Matrix::add(ptr shape, size_t line, size_t column)
{
  size_t newLine = (line == lines_) ? (lines_ + 1) : (lines_);
  size_t newColumn = (column == columns_) ? (columns_ + 1) : (columns_);

  array tmpForms(std::make_unique<ptr[]> (newLine * newColumn));

  for (size_t i = 0; i < newLine; i++)
  {
    for (size_t j = 0; j < newColumn; ++j)
    {
      if ((lines_ == i) || (columns_ == j))
      {
        tmpForms[i * newColumn + j] = nullptr;
      }
      else
      {
        tmpForms[i * newColumn + j] = forms_[i * columns_ + j];
      }
    }
  }
    
  tmpForms[line * newColumn + column] = shape;
  forms_.swap(tmpForms);
  lines_ = newLine;
  columns_ = newColumn;
}

std::size_t tsygarova::Matrix::getLines() const
{
  return lines_;
}

std::size_t tsygarova::Matrix::getColumns() const
{
  return columns_;
}

std::size_t tsygarova::Matrix::getLineSize(size_t line) const
{
  if (line >= lines_)
  {
    return 0;
  }

  for (size_t i = 0; i < columns_; i++)
  {
    if (forms_[line * columns_ + i] == nullptr)
    {
      return i;
    }
  }
  return columns_;
}

