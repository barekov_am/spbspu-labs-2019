#ifndef UNTITLED_MATRIX_HPP
#define UNTITLED_MATRIX_HPP

#include "shape.hpp"
#include <memory>

namespace tsygarova
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &copiedMatrix);
    Matrix(Matrix &&movedMatrix);
    ~Matrix() = default;

    Matrix &operator =(const Matrix &copiedMatrix);
    Matrix &operator =(Matrix &&movedMatrix);
    array operator [](size_t index) const;
    bool operator ==(const Matrix &shapesMatrix) const;
    bool operator !=(const Matrix &shapesMatrix) const;

    void add(ptr shape, size_t line, size_t column);
    size_t getLines() const;
    size_t getColumns() const;
    size_t getLineSize(size_t line) const;
  private:
    size_t lines_;
    size_t columns_;
    array forms_;
  };
};

#endif
