#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(matrixTests)

BOOST_AUTO_TEST_CASE(matrixCopyConstructor)
{
  tsygarova::Rectangle testRectangle({2, 12, {4, 7}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::CompositeShape compositeSh(rectPtr);
  tsygarova::Matrix copiedMatrix = part(compositeSh);

  tsygarova::Matrix matrix(copiedMatrix);

  BOOST_CHECK_EQUAL(matrix.getLines(), copiedMatrix.getLines());
  BOOST_CHECK_EQUAL(matrix.getColumns(), copiedMatrix.getColumns());
  BOOST_CHECK(matrix == copiedMatrix);
}

BOOST_AUTO_TEST_CASE(matrixCopyOperator)
{
  tsygarova::Rectangle testRectangle({2, 12, {4, 7}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::CompositeShape compositeSh(rectPtr);
  tsygarova::Matrix copiedMatrix = part(compositeSh);

  tsygarova::Matrix matrix;
  matrix = copiedMatrix;

  BOOST_CHECK_EQUAL(matrix.getLines(), copiedMatrix.getLines());
  BOOST_CHECK_EQUAL(matrix.getColumns(), copiedMatrix.getColumns());
  BOOST_CHECK(matrix == copiedMatrix);
}

BOOST_AUTO_TEST_CASE(matrixMoveConstructor)
{
  tsygarova::Rectangle testRectangle({2, 12, {4, 7}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::CompositeShape compositeSh(rectPtr);
  tsygarova::Matrix matrix = part(compositeSh);

  tsygarova::Matrix copyOfMatrix(matrix);
  tsygarova::Matrix moveMatrix(std::move(matrix));

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyOfMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyOfMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyOfMatrix);
  BOOST_CHECK_EQUAL(matrix.getLines(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(matrixMoveOperator)
{
  tsygarova::Rectangle testRectangle({2, 12, {4, 7}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(testRectangle);
  tsygarova::CompositeShape compositeSh(rectPtr);
  tsygarova::Matrix matrix = part(compositeSh);

  tsygarova::Matrix copyOfMatrix(matrix);
  tsygarova::Matrix moveMatrix;
  moveMatrix = std::move(matrix);

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyOfMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyOfMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyOfMatrix);
  BOOST_CHECK_EQUAL(matrix.getLines(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(matrixThrowExceptionAfterUsingOfEqualOperator)
{
  const tsygarova::Rectangle testRectangle1({2, 7, {9, 3}});
  tsygarova::ptr rect1Ptr = std::make_shared<tsygarova::Rectangle>(testRectangle1);
  const tsygarova::Rectangle testRectangle2({1, 3, {5, 2}});
  tsygarova::ptr rect2Ptr = std::make_shared<tsygarova::Rectangle>(testRectangle2);
  const tsygarova::Circle testCircle1(10, {5, 8});
  tsygarova::ptr circ1Ptr = std::make_shared<tsygarova::Circle>(testCircle1);
  const tsygarova::Circle testCircle2(4, {1, 3});
  tsygarova::ptr circ2Ptr = std::make_shared<tsygarova::Circle>(testCircle2);

  tsygarova::CompositeShape compositeSh(rect1Ptr);
  compositeSh.add(circ1Ptr);
  compositeSh.add(rect2Ptr);
  
  tsygarova::Matrix matrix = part(compositeSh);
  tsygarova::Matrix equalMatrix(matrix);
  tsygarova::Matrix unequalMatrix(matrix);
  unequalMatrix.add(circ2Ptr, 1, 1);

  BOOST_CHECK(matrix == equalMatrix);
  BOOST_CHECK(matrix != unequalMatrix);
}

BOOST_AUTO_TEST_CASE(matrixThrowExceptionAfterUsingOfOperator)
{
  const tsygarova::Rectangle testRectangle1({2, 7, {4, 3}});
  tsygarova::ptr rect1Ptr = std::make_shared<tsygarova::Rectangle>(testRectangle1);
  const tsygarova::Rectangle testRectangle2({1, 3, {5, 7}});
  tsygarova::ptr rect2Ptr = std::make_shared<tsygarova::Rectangle>(testRectangle2);
  const tsygarova::Circle testCircle1(4, {-1, 3});
  tsygarova::ptr circ1Ptr = std::make_shared<tsygarova::Circle>(testCircle1);
  const tsygarova::Circle testCircle2(1, {-3, 5});
  tsygarova::ptr circ2Ptr = std::make_shared<tsygarova::Circle>(testCircle2);

  tsygarova::CompositeShape compositeSh(rect1Ptr);
  compositeSh.add(circ1Ptr);
  compositeSh.add(rect2Ptr);
  compositeSh.add(circ2Ptr);

  tsygarova::Matrix matrix = part(compositeSh);

  BOOST_CHECK_THROW(matrix[10][5], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
