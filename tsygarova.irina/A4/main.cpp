#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

int main()
{
  tsygarova::Rectangle rectangle({8, 3, {7, 9}});
  tsygarova::ptr rectanglePtr = std::make_shared<tsygarova::Rectangle>(rectangle);
  std::cout << "Rectangle's parameters:\n";
  rectangle.print();
  std::cout << "\nRectangle with a new centre at (4, 3):\n";
  rectangle.move({4, 3});
  rectangle.print();
  std::cout << "\nRectangle shifted with dx=5, dy=3:\n";
  rectangle.move(5, 3);
  rectangle.print();
  std::cout << "\nScaled rectangle with coefficient=3:\n";
  rectangle.scale(3);
  rectangle.print();
  std::cout << "\nRotated with angle=45 rectangle's parameters:\n";
  rectangle.rotate(45);
  rectangle.print();

  tsygarova::Circle circle(3, {7, 9});
  tsygarova::ptr circlePtr = std::make_shared<tsygarova::Circle>(circle);
  std::cout << "\nCreated circle's parameters:\n";
  circle.print();
  std::cout << "\nCircle with a new centre at (12, 11):\n";
  circle.move({12, 11});
  circle.print();
  std::cout << "\nCircle shifted with dx=2.3, dy=4:\n";
  circle.move(2.3, 4);
  circle.print();
  std::cout << "\nScaled circle with coefficient=0.5:\n";
  circle.scale(0.5);
  circle.print();
  std::cout << "\nRotated with angle=30 circle's parameters:\n";
  circle.rotate(30);
  circle.print();
    
  tsygarova::CompositeShape compositeSh(rectanglePtr);
  std::cout << "\nCreated composite shape's parameters after adding figures:\n";
  compositeSh.add(circlePtr);
  compositeSh.print();
  std::cout << "\nComposite shape with a new center at (2, -3):\n";
  compositeSh.move({2, -3});
  compositeSh.print();
  std::cout << "\nScaled composite shape with coefficient=3:\n";
  compositeSh.scale(3);
  compositeSh.print();
  std::cout << "\nDelete composite shape's 1st component:\n";
  compositeSh.remove(0);
  compositeSh.print();
  std::cout << "\nRotated with angle=60 composite shape's parameters:\n";
  compositeSh.rotate(60);
  compositeSh.print();
  compositeSh.add(rectanglePtr);
  std::cout << "\nGet partition of Composite Shape:\n";

  tsygarova::Matrix matrix = tsygarova::part(compositeSh);
  for (size_t i = 0; i < matrix.getLines(); i++)
  {
    std::cout << "Line: " <<  i + 1
              << "\nShapes in line: \n";
    for (size_t j = 0; j < matrix.getColumns(); j++)
    {
      if (matrix[i][j] != nullptr)
      {
        tsygarova::rectangle_t frame = matrix[i][j]->getFrameRect();
        std::cout << j + 1
                  << ": \nArea " << matrix[i][j]->getArea()
                  << "\nFigure's center (" << frame.pos.x
                  << "," << frame.pos.y << ")\n\n";
      }
    }
  }
    
    return 0;
}
