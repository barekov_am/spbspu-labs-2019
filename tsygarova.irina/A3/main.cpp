#include <iostream>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  tsygarova::Rectangle rectangle({4, 2, {1, 3}});
  tsygarova::ptr rectPtr = std::make_shared<tsygarova::Rectangle>(rectangle);
  std::cout << "FOR RECTANGLE\n";
  rectPtr->print();
  std::cout << "1) Moving along Оx " << 4
            << " and Oy " << 7 << std::endl;
  rectPtr->move(4, 7);
  rectPtr->print();
  std::cout << "2) Moving to the point (" << 8
            << "; " << 6 << ")\n";
  rectPtr->move({8, 6});
  rectPtr->print();
  std::cout << "Rectangle after scale(4)\n";
  rectPtr->scale(4);
  rectPtr->print();
  std::cout << std::endl;

  tsygarova::Circle circle(6, {2, 3});
  tsygarova::ptr circPtr = std::make_shared<tsygarova::Circle>(circle);
  std::cout << "\nFOR CIRCLE\n";
  circPtr->print();
  std::cout << "1) Moving along Оx " << 5
            << " and Oy " << 9 << std::endl;
  circPtr->move(5, 9);
  circPtr->print();
  std::cout << "2) Moving to the point (" << 9
            << "; " << 3 << ")\n";
  circPtr->move({9, 3});
  circPtr->print();
  std::cout << "Circle after scale(2)\n";
  circPtr->scale(2);
  circPtr->print();

  tsygarova::CompositeShape compositeSh(rectPtr);
  std::cout << "\nFOR COMPOSITION\n";
  std::cout << "Created new parameters:\n";
  compositeSh.print();
  std::cout << "Moving composition to the point (" << 9
            << ";" << -2 << ")\n";
  compositeSh.move({9, -2});
  compositeSh.print();
  std::cout << "Composition after scale(2)\n";
  compositeSh.scale(2);
  compositeSh.print();
  std::cout << "Delete composition\n";
  compositeSh.remove(0);
  compositeSh.print();

  return 0;
}
