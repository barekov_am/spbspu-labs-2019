#include <iostream>
#include <string>
#include <sstream>
#include <functional>
#include "queue-with-priority.hpp"

using queueCommand = std::function<void(QueueWithPriority<std::string>&, std::ostream&)>;

queueCommand parseArguments(std::istream &inp);

void task1()
{
  QueueWithPriority<std::string> queue;

  std::string tmp;
  while (getline(std::cin, tmp))
  {
    if (!std::cin.eof() && std::cin.fail())
    {
      throw std::runtime_error("Input failed.\n");
    }
    std::stringstream inp(tmp);
    parseArguments(inp)(queue, std::ref(std::cout));
  }
}

