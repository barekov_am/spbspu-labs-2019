#include <iostream>
#include <stdexcept>

void task1();
void task2();

int main(int argc, char* argv[])
{
  try
  {
    if (argc <= 1)
    {
      std::cerr << "Invalid number of arguments.\n";
      return 1;
    }

    char *tail = nullptr;
    int task = std::strtol(argv[1], &tail, 10);

    if (*tail)
    {
      std::cerr << "Invalid number of task.\n";
      return 1;
    }

    switch (task)
    {
      case 1:
      {
        if (argc != 2)
        {
          std::cerr << "Invalid number of arguments.\n";
          return 1;
        }
        task1();
        break;
      }
      case 2:
      {
        if (argc != 2)
        {
          std::cerr << "Invalid number of arguments.\n";
          return 1;
        }
        task2();
        break;
      }
      default:
      {
        std::cerr << "Invalid number of task.\n";
        return 1;
      }
    }
  }
  catch (const std::exception &exception)
  {
    std::cerr << exception.what();
    return 1;
  }
  return 0;
}

