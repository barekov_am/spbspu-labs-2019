#include <iostream>
#include <limits>
#include <unordered_map>
#include <algorithm>
#include <sstream>
#include "queue-with-priority.hpp"

void add(QueueWithPriority<std::string> &queue,
         const QueueWithPriority<std::string>::ElementPriority &priority,
         const std::string &value)
{
  queue.putElementToQueue(value, priority);
}

void get(QueueWithPriority<std::string> &queue, std::ostream &out)
{
  if (queue.empty())
  {
    out << "<EMPTY>\n";
    return;
  }
  queue.handlerElement([&](std::string element) { out << element << "\n"; });
}

void accelerate(QueueWithPriority<std::string> &queue)
{
  queue.accelerate();
}

void invalid(std::ostream &out)
{
  out << "<INVALID COMMAND>" << "\n";
}
