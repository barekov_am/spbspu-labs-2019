#include <iostream>
#include <list>

void task2()
{
  std::list<int> list;
  int size = 0;
  const int maxSize = 21;
  int count = 0;

  while (std::cin >> count, !std::cin.eof())
  {
    if (!std::cin.eof() && std::cin.fail())
    {
      throw std::runtime_error("Input failed.\n");
    }

    ++size;
    if (size == maxSize)
    {
      throw std::invalid_argument("A maximum of 20 numbers can be processed.\n");
    }

    if ((count < 1) || (count >= maxSize))
    {
      throw std::invalid_argument("Number is out of range.\n");
    }

    list.push_back(count);
  }
    
  if (list.empty())
  {
    return;
  }

  auto head = list.begin();
  auto tail = std::prev(list.end());

  for (int i = 0; i < size / 2; ++i)
  {
    std::cout << *head++ << " " << *tail-- << " ";
  }

  if (head == tail)
  {
    std::cout << *head;
  }

  std::cout << "\n";
}
