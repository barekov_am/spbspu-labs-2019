#include <iostream>
#include <string>
#include <sstream>
#include <functional>
#include <algorithm>
#include "queue-with-priority.hpp"

using namespace std::placeholders;

using queueCommand = std::function<void(QueueWithPriority<std::string>&,
                                 std::ostream&)>;

void add(QueueWithPriority<std::string>&,
         const QueueWithPriority<std::string>::ElementPriority&,
         const std::string&);
void get(QueueWithPriority<std::string>&, std::ostream&);
void accelerate(QueueWithPriority<std::string>&);
void invalid(std::ostream&);

queueCommand parseArgumentsAdd(std::istream &in)
{
  std::string priority;
  std::string value;

  in >> priority >> std::ws;

  getline(in, value);

  if (value.empty())
  {
    return std::bind(invalid, _2);
  }

  static const struct
  {
    const char* name;
    QueueWithPriority<std::string>::ElementPriority priority;
  }
  priorities[] = { {"low", QueueWithPriority<std::string>::ElementPriority::LOW},
                   {"normal", QueueWithPriority<std::string>::ElementPriority::NORMAL},
                   {"high", QueueWithPriority<std::string>::ElementPriority::HIGH} };

  auto priorityElement = std::find_if(priorities, std::end(priorities),
         [&](const decltype (priorities[0]) &element)
         {return (element.name == priority);});

  if (priorityElement == std::end(priorities))
  {
    return std::bind(invalid, _2);
  }

  return std::bind(add, _1, priorityElement->priority, value);
}

queueCommand parseArgumentsGet(std::istream &in)
{
  in >> std::ws;
  if (!in.eof())
  {
    return std::bind(invalid, std::ref(std::cout));
  }
  return std::bind(get, _1, _2);
}

queueCommand parseArgumentsAccelerate(std::istream& in)
{
  using namespace std::placeholders;

  in >> std::ws;
  if (!in.eof())
  {
    return std::bind(invalid, _2);
  }
  return std::bind(accelerate, _1);
}

queueCommand parseArguments(std::istream& in)
{
  static const struct
  {
    using parserType = queueCommand (*)(std::istream& in);
    const char* name;
    parserType parse;
  }
  parsers[] = { {"add", &parseArgumentsAdd},
                {"get", &parseArgumentsGet},
                {"accelerate", &parseArgumentsAccelerate} };
    
  std::string tmp;
  in >> tmp;

  auto parserElement = std::find_if(parsers, std::end(parsers),
        [&](const decltype(parsers[0])& element)
        { return (element.name == tmp); });
    
  if (parserElement == std::end(parsers))
  {
    return std::bind(invalid, _2);
  }
  else
  {
    return parserElement->parse(in);
  }
}
