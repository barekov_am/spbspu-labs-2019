#include <iostream>
#include <exception>
#include <string>
#include <algorithm>

#include "shape.hpp"
#include "geometry.hpp"

const int VERTICES_OF_TRIANGLE = 3;
const int VERTICES_OF_RECTANGLE = 4;
const int VERTICES_OF_PENTAGON = 5;

std::vector<Shape> readShapes(std::istream&);

void task2()
{
  std::vector<Shape> shapes = readShapes(std::cin);

  std::size_t vertices = 0;
  std::size_t triangles = 0;
  std::size_t squares = 0;
  std::size_t rectangles = 0;

  std::for_each(shapes.begin(), shapes.end(), [&](const Shape& shape)
  {
    vertices += shape.size();
    if (shape.size() == VERTICES_OF_TRIANGLE)
    {
      ++triangles;
    }
    else if (shape.size() == VERTICES_OF_RECTANGLE)
    {
      if (geometry::isRectangle(shape))
      {
        ++rectangles;
        if (geometry::isSquare(shape))
        {
          ++squares;
        }
      }
    }
  });

  shapes.erase(std::remove_if(shapes.begin(), shapes.end(),
      [&](const Shape& shape) { return shape.size() == VERTICES_OF_PENTAGON; }), shapes.end());

  Shape leadingPoints(shapes.size());
  std::transform(shapes.begin(), shapes.end(), leadingPoints.begin(), [&](const Shape& shape) { return shape[0]; });

  std::sort(shapes.begin(), shapes.end(), [&](const Shape& lhs, const Shape& rhs)
  {
    if (lhs.size() < rhs.size())
    {
      return true;
    }
    if ((lhs.size() == VERTICES_OF_RECTANGLE) && (rhs.size() == VERTICES_OF_RECTANGLE))
    {
      if (geometry::isSquare(lhs))
      {
        if (geometry::isSquare(rhs))
        {
          return false;
        }
        return true;
      }
    }
    return false;
  });

  std::cout << "Vertices: " << vertices << '\n';
  std::cout << "Triangles: " << triangles << '\n';
  std::cout << "Squares: " << squares << '\n';
  std::cout << "Rectangles: " << rectangles << '\n';

  std::cout << "Points: ";
  for (const auto& point : leadingPoints)
  {
    std::cout << '(' << point.x << ';' << point.y << ") ";
  }

  std::cout << "\nShapes: \n";
  for (const auto& shape : shapes)
  {
    std::cout << shape.size();
    for (const auto& vertice : shape)
    {
      std::cout << " (" << vertice.x << ';' << vertice.y << ") ";
    }
    std::cout << '\n';
  }
}
