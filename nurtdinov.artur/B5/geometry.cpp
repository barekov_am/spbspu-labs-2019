#include "geometry.hpp"

int getDistance(const Point_t& lhs, const Point_t& rhs)
{
  return (lhs.x - rhs.x) * (lhs.x - rhs.x) + (lhs.y - rhs.y) * (lhs.y - rhs.y);
}

bool geometry::isRectangle(const Shape& shape)
{
  int diagonal1 = getDistance(shape[0], shape[2]);
  int diagonal2 = getDistance(shape[1], shape[3]);

  return diagonal1 == diagonal2;
}

bool geometry::isSquare(const Shape& shape)
{
  if (geometry::isRectangle(shape))
  {
    int side1 = getDistance(shape[0], shape[1]);
    int side2 = getDistance(shape[1], shape[2]);
    int side3 = getDistance(shape[2], shape[3]);
    int side4 = getDistance(shape[3], shape[0]);

    if ((side1 == side2) && (side2 == side3) && (side3 == side4) && (side4 == side1))
    {
      return true;
    }
  }
  return false;
}
