#include <string>
#include <iostream>

#include "shape.hpp"

std::vector<Shape> readShapes(std::istream& istream)
{
  std::vector<Shape> shapes;
  std::string line;
  while (std::getline(istream >> std::ws, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading failed\n");
    }

    auto blank = line.find_first_not_of(" \t");
    if (blank != std::string::npos)
    {
      line.erase(0, blank);
    }

    if (line.empty())
    {
      continue;
    }

    auto openBracketPos = line.find_first_of('(');
    if (openBracketPos == std::string::npos)
    {
      throw std::invalid_argument("Invalid separator\n");
    }

    int numOfPoints = std::stoi(line.substr(0, openBracketPos));
    line.erase(0, openBracketPos);
    if (numOfPoints < 1)
    {
      throw std::invalid_argument("Invalid number of vertices\n");
    }

    Shape shape;
    std::size_t separatorPos;
    std::size_t closeBracketPos;

    if (line.empty())
    {
      throw std::invalid_argument("Point were not provided\n");
    }

    for (int i = 0; i < numOfPoints; i++)
    {
      openBracketPos = line.find_first_of('(');
      separatorPos = line.find_first_of(';');
      closeBracketPos = line.find_first_of(')');

      if ((openBracketPos == std::string::npos) || (separatorPos == std::string::npos)
          || (closeBracketPos == std::string::npos))
      {
        throw std::invalid_argument("Invalid separator\n");
      }

      Point_t point
      {
        std::stoi(line.substr(openBracketPos + 1, separatorPos - openBracketPos - 1)),
        std::stoi(line.substr(separatorPos + 1, closeBracketPos - separatorPos - 1))
      };

      line.erase(0, closeBracketPos + 1);

      shape.push_back(point);
    }

    line.erase(line.find_last_not_of(" \t") + 1);

    if (!line.empty())
    {
      throw std::invalid_argument("Extra args was not expected\n");
    }

    shapes.push_back(shape);
  }
  return shapes;
}
