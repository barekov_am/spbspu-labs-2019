#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP

#include "shape.hpp"

namespace geometry
{
  bool isSquare(const Shape& shape);
  bool isRectangle(const Shape& shape);
} // namespace geometry

#endif // GEOMETRY_HPP
