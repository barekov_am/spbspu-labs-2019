#include <iostream>

#include "parser.hpp"

int main(int argc, char* argv[])
{
  try
  {
    if ((argc > 3) || (argc == 2))
    {
      throw std::invalid_argument("Invalid number of arguments\n");
    }

    size_t width = 40;

    if (argc == 3)
    {
      if (std::string(argv[1]) != "--line-width")
      {
        throw std::invalid_argument("Invalid 1st argument\n");
      }
      width = std::atoi(argv[2]);

      if (width < 25)
      {
        throw std::invalid_argument("Invalid width\n");
      }
    }

    Parser parser(std::cin, std::cout, width);
    parser.parse();
    parser.printText();
  }
  catch (const std::exception& ex)
  {
    std::cerr << ex.what();
    return 1;
  }

  return 0;
}
