#ifndef PARSER_HPP
#define PARSER_HPP

#include <iostream>

class Parser
{
public:
  Parser(std::istream& istream, std::ostream& out, unsigned long bufferSize);
  void parse();

  void printText();

private:
  std::string buffer_;
  char current_;
  unsigned long maxSize_;
  std::istream& istream_;
  std::ostream& out_;
};

#endif // PARSER_HPP
