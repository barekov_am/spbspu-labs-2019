#include "tokens.hpp"

#include <iostream>

const int MAX_WORD_LENGHT = 20;

TokensHandler::TokensHandler(std::istream& istream) :
  istream_(istream)
{}

void TokensHandler::readNumber(std::string& string, char& currentChar)
{
  size_t counter = 1;
  char decimalPoint = std::use_facet<std::numpunct<char>>(istream_.getloc()).decimal_point();

  while (istream_.get(currentChar))
  {
    if (std::isdigit(currentChar, istream_.getloc()))
    {
      counter++;
      if (counter > MAX_WORD_LENGHT)
      {
        throw std::invalid_argument("Not allowed to handle more than 20 chars\n");
      }
      string.push_back(currentChar);
    }
    else
    {
      if (std::isspace(currentChar, istream_.getloc()))
      {
        trimSpaces(currentChar);
      }
      else if (currentChar == decimalPoint)
      {
        if (string.find(decimalPoint) == string.npos)
        {
          counter++;
          string.push_back(currentChar);
          continue;
        }
      }
      if ((string == "-") || (string == "+"))
      {
        throw std::invalid_argument("Bad input\n");
      }

      if (std::ispunct(currentChar, istream_.getloc()))
      {
        if (string.back() == decimalPoint)
        {
          throw std::invalid_argument("Bad input\n");
        }

        if (currentChar == '-')
        {
          if (istream_.peek() == '-')
          {
            string.append(readDash(string, currentChar));
            return;
          }
        }
        else if (currentChar == ',')
        {
          string.push_back(currentChar);
          istream_.get(currentChar);
          trimSpaces(currentChar);
        }
        else if (currentChar != '+')
        {
          string.push_back(currentChar);
          return;
        }
      }

      istream_.unget();
      return;
    }
  }
}

void TokensHandler::readWord(std::string& string, char& currentChar)
{
  size_t counter = 1;

  while (istream_.get(currentChar))
  {
    if (std::isalpha(currentChar, istream_.getloc()))
    {
      counter++;
      if ((counter > MAX_WORD_LENGHT) || ((istream_.peek() == '-') && (counter == MAX_WORD_LENGHT)))
      {
        throw std::invalid_argument("Not allowed to handle a word with more than 20 chars\n");
      }
      string.push_back(currentChar);
    }
    else
    {
      if (currentChar == '-')
      {
        if (string.back() == '-')
        {
          string.append(readDash(string, currentChar));
          return;
        }
        else
        {
          ++counter;
          string.push_back(currentChar);
          continue;
        }
      }

      if (checkForDash(string, currentChar))
      {
        return;
      }

      if (std::ispunct(currentChar, istream_.getloc()))
      {
        string.push_back(currentChar);
        if (currentChar == ',')
        {
          istream_.get(currentChar);
          if (checkForDash(string, currentChar))
          {
            return;
          }
        }
        else
        {
          return;
        }
      }

      istream_.unget();
      return;
    }
  }
}

std::string TokensHandler::readDash(std::string& string, char& currentChar)
{
  std::string dash(1, '-');

  if (string.back() == '-')
  {
    dash.push_back('-');
  }

  for (int i = 0; i < 2; ++i)
  {
    if (istream_)
    {
      istream_.get(currentChar);
      dash.push_back(currentChar);
    }
  }

  if (dash.back() != '-')
  {
    istream_.unget();
    dash.pop_back();
  }

  if (dash == "----")
  {
    return " ---";
  }
  else if (dash == "---")
  {
    if (string.back() == '-')
    {
      string.back() = ' ';
      return dash;
    }
    else
    {
      return " ---";
    }
  }
  throw std::invalid_argument("Invalid dash\n");
}

bool TokensHandler::checkForDash(std::string& string, char& currentChar)
{
  trimSpaces(currentChar);
  if (currentChar == '-')
  {
    string.append(readDash(string, currentChar));
    return true;
  }
  return false;
}

void TokensHandler::trimSpaces(char& currentChar)
{
  while (std::isspace(currentChar, istream_.getloc()) && !(istream_.eof()))
  {
    istream_.get(currentChar);
  }
}
