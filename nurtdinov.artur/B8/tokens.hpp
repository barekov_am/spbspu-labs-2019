#ifndef TOKENS_HPP
#define TOKENS_HPP

#include <string>

class TokensHandler
{
public:
  TokensHandler(std::istream&);
  void readNumber(std::string&, char&);
  void readWord(std::string&, char&);

private:
  std::istream& istream_;

  std::string readDash(std::string&, char&);
  void trimSpaces(char&);
  bool checkForDash(std::string&, char&);
};

#endif // TOKENS_HPP
