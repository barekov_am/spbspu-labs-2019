#include "parser.hpp"

#include <cctype>

#include "tokens.hpp"

Parser::Parser(std::istream& istream, std::ostream& out, unsigned long bufferSize) :
  buffer_(),
  current_(' '),
  maxSize_(bufferSize),
  istream_(istream),
  out_(out)
{
  buffer_.reserve(bufferSize);
}

void Parser::parse()
{
  std::string string;
  TokensHandler handler(std::cin);

  while (istream_.get(current_))
  {
    if (std::isspace(current_, istream_.getloc()))
    {
      continue;
    }
    else if (std::isdigit(current_, istream_.getloc()) || current_ == '-' || current_ == '+')
    {
      string.push_back(current_);
      handler.readNumber(string, current_);
    }
    else if (std::isalpha(current_, istream_.getloc()))
    {
      string.push_back(current_);
      handler.readWord(string, current_);
    }
    else if (std::ispunct(current_, istream_.getloc()))
    {
      throw std::invalid_argument("Unexpected punctuation symbol\n");
    }

    if (!buffer_.empty())
    {
      if (maxSize_ - buffer_.size() <= string.size())
      {
        printText();
      }
      else if (string.size() + buffer_.size() < maxSize_ + 1)
      {
        buffer_.push_back(' ');
      }
    }

    buffer_.append(string);
    string.clear();
  }
}

void Parser::printText()
{
  out_ << buffer_ << '\n';
  buffer_.clear();
}
