#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

void printInfoAboutShape(const nurtdinov::Shape &shape)
{
  const double area = shape.getArea();
  const nurtdinov::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "Shape area is " << area << '\n';
  std::cout << "Shape frame rectangle width and height:" << '\n';
  std::cout << "Width = " << frameRect.width << '\n';
  std::cout << "Height = " << frameRect.height << '\n';
  std::cout << "Position of rectangle: x = " << frameRect.pos.x << ", y = " << frameRect.pos.y << "\n\n";
}

int main()
{
  nurtdinov::Circle circle({15.5, 5.3}, 3.4);
  printInfoAboutShape(circle);
  circle.move({3.5, 14.4});
  printInfoAboutShape(circle);
  circle.move(5.2, 4.3);
  printInfoAboutShape(circle);
  const double scaleRatioForCircle = 2.5;
  circle.scale(scaleRatioForCircle);
  printInfoAboutShape(circle);

  nurtdinov::Rectangle rectangle({14.3, 15.2}, 5.1, 4.3);
  printInfoAboutShape(rectangle);
  rectangle.move({5.4, 3.3});
  printInfoAboutShape(rectangle);
  rectangle.move(3.6, 7.7);
  printInfoAboutShape(rectangle);
  const double scaleRatioForRectangle = 4;
  rectangle.scale(scaleRatioForRectangle);
  printInfoAboutShape(rectangle);

  //now work with CompositeShape shape
  nurtdinov::Rectangle rectangleForComp({4, 5}, 2, 5);
  nurtdinov::Circle circleForComp({0, 0}, 2);

  nurtdinov::CompositeShape firstComposite;
  firstComposite.add(std::make_shared<nurtdinov::Rectangle>(rectangleForComp));
  printInfoAboutShape(firstComposite);
  firstComposite.add(std::make_shared<nurtdinov::Circle>(circleForComp));
  printInfoAboutShape(firstComposite);
  const double scaleRatio = 3;
  firstComposite.scale(scaleRatio);
  printInfoAboutShape(firstComposite);

  std::cout << "Count of shapes in first composite shape = " << firstComposite.getSize() << '\n';

  firstComposite.remove(1); // removed circleForComp
  printInfoAboutShape(firstComposite);

  firstComposite.add(std::make_shared<nurtdinov::Circle>(circleForComp)); // returned circleForComp
  printInfoAboutShape(firstComposite);

  firstComposite.move({1.8, 4.5});
  printInfoAboutShape(firstComposite);

  //use constructor
  nurtdinov::CompositeShape secondComposite(firstComposite);
  printInfoAboutShape(secondComposite);

  //use operator
  nurtdinov::CompositeShape thirdComposite = firstComposite;
  printInfoAboutShape(thirdComposite);

  //use get
  nurtdinov::Rectangle rectangleInComp({4, 5}, 2, 3);
  nurtdinov::Circle circleInComp({0, 0}, 2);
  nurtdinov::CompositeShape compositeShape;
  compositeShape.add(std::make_shared<nurtdinov::Rectangle>(rectangleInComp));
  compositeShape.add(std::make_shared<nurtdinov::Circle>(circleInComp));
  printInfoAboutShape(*compositeShape[0]); //we get rectangleInComp
  return 0;
}
