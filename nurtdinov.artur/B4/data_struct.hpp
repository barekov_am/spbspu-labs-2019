#ifndef DATA_STRUCT_HPP
#define DATA_STRUCT_HPP

#include <string>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

#endif // DATA_STRUCT_HPP
