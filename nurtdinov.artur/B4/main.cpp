#include <iostream>
#include <vector>
#include <algorithm>

#include "data_struct.hpp"

std::vector<DataStruct> readData(std::istream& istream);

int main()
{
  try
  {
    std::vector<DataStruct> vector = readData(std::cin);

    if (!vector.empty())
    {
      std::sort(vector.begin(), vector.end(), [&](DataStruct rhs, DataStruct lhs)
      {
        if (rhs.key1 != lhs.key1)
        {
          return rhs.key1 < lhs.key1;
        }
        else if (rhs.key2 != lhs.key2)
        {
          return rhs.key2 < lhs.key2;
        }
        else
        {
          return rhs.str.length() < lhs.str.length();
        }
      });

      for (const auto& el : vector)
      {
        std::cout << el.key1 << "," << el.key2 << "," << el.str << '\n';
      }
    }
  }
  catch (const std::exception& ex)
  {
    std::cerr << ex.what();
    return 1;
  }
  return 0;
}
