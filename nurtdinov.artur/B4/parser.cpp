#include <vector>
#include <iostream>

#include "data_struct.hpp"

const int UPPER_BOUND = 5;
const int LOWER_BOUND = -5;

void trimSpaces(std::string& line)
{
  auto firstNotWS = line.find_first_not_of(" \t");
  line.erase(0, firstNotWS);
}

std::vector<DataStruct> readData(std::istream& istream)
{
  std::vector<DataStruct> vector;
  std::string input;
  while (getline(istream >> std::ws, input))
  {
    if (input.empty())
    {
      throw std::invalid_argument("Empty input\n");
    }

    auto pos = input.find_first_of(',');
    if (pos == std::string::npos)
    {
      throw std::invalid_argument("Invalid separator\n");
    }

    int key1 = std::stoi(input.substr(0, pos));

    input.erase(0, pos + 1);
    pos = input.find_first_of(',');
    if (pos == std::string::npos)
    {
      throw std::invalid_argument("Invalid separator\n");
    }

    int key2 = std::stoi(input.substr(0, pos));

    input.erase(0, pos + 1);
    trimSpaces(input);

    if ((key1 > UPPER_BOUND) || (key1 < LOWER_BOUND) || (key2 > UPPER_BOUND) || (key2 < LOWER_BOUND))
    {
      throw std::invalid_argument("Keys must be in range (-5, 5)\n");
    }
    if (input.empty())
    {
      throw std::invalid_argument("Empty string\n");
    }

    vector.push_back({key1, key2, input});
  }
  return vector;
}
