#include "functor.hpp"

#include <algorithm>

Functor::Functor()
{
  max_ = std::numeric_limits<int>::min();
  min_ = std::numeric_limits<int>::max();
  first_ = 0;
  positive_ = 0;
  negative_ = 0;
  total_ = 0;
  odd_sum_ = 0;
  even_sum_ = 0;
  equals_ = false;
}

void Functor::operator()(int number)
{
  if (number > max_)
  {
    max_ = number;
  }

  if (number < min_)
  {
    min_ = number;
  }

  if (number > 0)
  {
    positive_++;
  }

  if (number < 0)
  {
    negative_++;
  }

  if (number % 2 == 0)
  {
    even_sum_ += number;
  }
  else
  {
    odd_sum_ += number;
  }

  if (total_ == 0)
  {
    first_ = number;
  }

  if (first_ == number)
  {
    equals_ = true;
  }
  else
  {
    equals_ = false;
  }

  total_++;
}

long long int Functor::getMaxNumber() const noexcept
{
  return max_;
}

long long int Functor::getMinNumber() const noexcept
{
  return min_;
}

double Functor::getAverageNumber() const noexcept
{
  return static_cast<double>(odd_sum_ + even_sum_) / total_;
}

long Functor::getNumOfPositive() const noexcept
{
  return positive_;
}

long Functor::getNumOfNegative() const noexcept
{
  return negative_;
}

long long int Functor::getEvenSum() const noexcept
{
  return even_sum_;
}

long long int Functor::getOddSum() const noexcept
{
  return odd_sum_;
}

bool Functor::isFirstEqualLast() const noexcept
{
  return equals_;
}

bool Functor::isEmpty() const noexcept
{
  return total_ == 0;
}
