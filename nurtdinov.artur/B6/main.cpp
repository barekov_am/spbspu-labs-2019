#include <iostream>

void task(std::istream&);

int main()
{
  try
  {
    task(std::cin);
  }
  catch (const std::exception& ex)
  {
    std::cerr << ex.what();
    return 1;
  }

  return 0;
}
