#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <vector>

class Functor
{
public:
  Functor();
  void operator()(int number);
  long long int getMaxNumber() const noexcept;
  long long int getMinNumber() const noexcept;
  double getAverageNumber() const noexcept;
  long getNumOfPositive() const noexcept;
  long getNumOfNegative() const noexcept;
  long long int getEvenSum() const noexcept;
  long long int getOddSum() const noexcept;
  bool isFirstEqualLast() const noexcept;
  bool isEmpty() const noexcept;

private:
  long long int max_;
  long long int min_;
  long long int first_;
  long positive_;
  long negative_;
  long total_;
  long long int odd_sum_;
  long long int even_sum_;
  bool equals_;
};

#endif // FUNCTOR_HPP
