#include <algorithm>
#include <iostream>
#include <iterator>

#include "functor.hpp"

void task(std::istream& istream)
{
  Functor functor;
  functor = std::for_each(std::istream_iterator<int>(istream), std::istream_iterator<int>(), functor);

  if (!istream.eof())
  {
    throw std::ios::failure("Wrong input were not processed\n");
  }

  if (functor.isEmpty())
  {
    std::cout << "No Data\n";
  }
  else
  {
    std::cout << "Max: " << functor.getMaxNumber() << "\n";
    std::cout << "Min: " << functor.getMinNumber() << "\n";
    std::cout << "Mean: " << std::fixed << functor.getAverageNumber() << "\n";
    std::cout << "Positive: " << functor.getNumOfPositive() << "\n";
    std::cout << "Negative: " << functor.getNumOfNegative() << "\n";
    std::cout << "Odd Sum: " << functor.getOddSum() << "\n";
    std::cout << "Even Sum: " << functor.getEvenSum() << "\n";
    std::cout << "First/Last Equal: ";

    if (functor.isFirstEqualLast())
    {
      std::cout << "yes\n";
    }
    else
    {
      std::cout << "no\n";
    }
  }
}
