#include <iostream>
#include <string>

#include "queue-with-priority.hpp"
#include "commands.hpp"

void firstTask()
{
  QueueWithPriority<std::string> queue;
  commands::parseTask(std::cin, queue);
}
