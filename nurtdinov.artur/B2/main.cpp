#include <iostream>
#include <stdexcept>

void firstTask();
void secondTask();

int main(int argc, char* argv[])
{
  if (argc <= 1)
  {
    std::cerr << "Invalid number of arguments";
    return 1;
  }
  const int taskNumber = atoi(argv[1]);

  try
  {
    switch (taskNumber)
    {
    case 1:
    {
      if (argc != 2)
      {
        std::cerr << "Invalid number of arguments";
        return 1;
      }
      firstTask();
      break;
    }
    case 2:
    {
      if (argc != 2)
      {
        std::cerr << "Invalid number of arguments";
        return 1;
      }
      secondTask();
      break;
    }
    default:
    {
      std::cerr << "Incorrect task number";
      return 1;
    }
    }
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}
