#include <iostream>
#include <list>

const std::size_t MAX_SIZE = 21;
const int LOWER_BOUND = 1;
const int UPPER_BOUND = 20;

void process(std::list<int>::iterator begin, std::list<int>::iterator end)
{
  if (begin == end)
  {
    std::cout << '\n';
    return;
  }
  if (begin == std::prev(end))
  {
    std::cout << *begin << '\n';
    return;
  }

  std::cout << *begin << " " << *std::prev(end) << " ";
  process(++begin, --end);
}

void secondTask()
{
  std::list<int> list;
  std::size_t size = 0;
  int i = 0;
  while (std::cin >> i, !std::cin.eof())
  {
    if (!std::cin.eof() && std::cin.fail())
    {
      throw std::runtime_error("Input failed");
    }
    ++size;
    if (size == MAX_SIZE)
    {
      throw std::invalid_argument("It's not possible to use more than 20 items");
    }
    if ((i < LOWER_BOUND) || (i > UPPER_BOUND))
    {
      throw std::invalid_argument("Number must be in range from 1 to 20");
    }
    list.push_back(i);
  }
  if (list.empty())
  {
    return;
  }
  process(list.begin(), list.end());
}
