#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <functional>

template<typename T>
class QueueWithPriority;
namespace commands
{
  using command_function = std::function<void(QueueWithPriority<std::string>&)>;

  void parseTask(std::istream& istream, QueueWithPriority<std::string> queue);
  command_function parseAdd(std::istream& istream);
  command_function parseGet(std::istream& istream);
  command_function parseAccelerate(std::istream& istream);
} // namespace commands

#endif // COMMANDS_HPP
