#include "commands.hpp"

#include <iostream>
#include <string>
#include <algorithm>

#include "queue-with-priority.hpp"

using namespace std::placeholders;

const commands::command_function INVALID_COMMAND =
    [](QueueWithPriority<std::string>&){ std::cout << "<INVALID COMMAND>\n"; };

template<typename Char, typename CharTraits>
std::basic_istream<Char, CharTraits>& blank(std::basic_istream<Char, CharTraits>& istream)
{
  using istream_type = std::basic_istream<Char, CharTraits>;
  using streambuf_type = std::basic_streambuf<Char, CharTraits>;
  using int_type = typename istream_type::int_type;
  using ctype_type = std::ctype<Char>;

  const ctype_type& ct = std::use_facet<ctype_type>(istream.getloc());
  const int_type eof = CharTraits::eof();
  streambuf_type* sb = istream.rdbuf();
  int_type c = sb->sgetc();

  while (!CharTraits::eq_int_type(c, eof) && ct.is(std::ctype_base::blank, CharTraits::to_char_type(c)))
  {
    c = sb->snextc();
  }

  if (CharTraits::eq_int_type(c, eof))
  {
    istream.setstate(std::ios_base::eofbit);
  }
  return istream;
}

std::istream& operator>>(std::istream& istream, QueueWithPriority<std::string>::ElementPriority& priority)
{
  static const struct
  {
    const char* name;
    QueueWithPriority<std::string>::ElementPriority priority;
  } PRIORITIES[] = {
    {"high", QueueWithPriority<std::string>::ElementPriority::HIGH},
    {"normal", QueueWithPriority<std::string>::ElementPriority::NORMAL},
    {"low", QueueWithPriority<std::string>::ElementPriority::LOW}
  };

  std::string priorityWord;
  istream >> priorityWord;

  auto elementPriority = std::find_if(std::begin(PRIORITIES), std::end(PRIORITIES),
      [&](const auto& element) { return priorityWord == element.name; });

  if (elementPriority == std::end(PRIORITIES))
  {
    istream.setstate(std::ios::failbit);
    return istream;
  }

  priority = elementPriority->priority;
  return istream;
}

void commands::parseTask(std::istream& istream, QueueWithPriority<std::string> queue)
{
  static const struct
  {
    const char* name;
    command_function(*function)(std::istream&);
  } COMMANDS[] = {
      {"add", &parseAdd},
      {"get", &parseGet},
      {"accelerate", &parseAccelerate}
  };

  std::string commandWord;
  while(istream >> blank >> commandWord)
  {
    auto command = std::find_if(std::begin(COMMANDS), std::end(COMMANDS),
        [&](const auto& element) { return commandWord == element.name; });

    if (command != std::end(COMMANDS))
    {
      command->function(istream >> blank)(queue);
    }
    else
    {
      istream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      INVALID_COMMAND(queue);
    }
  }
}

commands::command_function commands::parseAdd(std::istream& istream)
{
  QueueWithPriority<std::string>::ElementPriority priority;
  if (!(istream >> priority))
  {
    istream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return INVALID_COMMAND;
  }

  std::string value;
  getline(istream >> blank, value);
  if (value.empty())
  {
    return INVALID_COMMAND;
  }

  return std::bind(&QueueWithPriority<std::string>::put, _1, value, priority);
}

commands::command_function commands::parseGet(std::istream& istream)
{
  if ((istream.get() != '\n') && (!istream.eof()))
  {
    istream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return INVALID_COMMAND;
  }

  return [&](QueueWithPriority<std::string>& queue)
  {
    if (queue.empty())
    {
      std::cout << "<EMPTY>\n";
      return;
    }
    queue.handleElement([&](std::string element) { std::cout << element << '\n'; });
  };
}

commands::command_function commands::parseAccelerate(std::istream& istream)
{
  if ((istream.get() != '\n') && (!istream.eof()))
  {
    istream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return INVALID_COMMAND;
  }

  return [&](QueueWithPriority<std::string>& queue)
  {
    if (queue.empty())
    {
      std::cout << "<EMPTY>\n";
      return;
    }
    queue.accelerate();
  };
}
