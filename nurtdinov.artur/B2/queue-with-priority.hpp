#ifndef QUEUEWITHPRIORITY_HPP
#define QUEUEWITHPRIORITY_HPP

#include <list>
#include <stdexcept>

template<typename T>
class QueueWithPriority
{
public:
  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  template<typename handlerType>
  void handleElement(handlerType handler);
  void put(const T& element, ElementPriority priority);
  void accelerate();
  bool empty() const noexcept;
  std::size_t size() const noexcept;
  void swap(QueueWithPriority<T>& other);

private:
  std::list<T> low_;
  std::list<T> normal_;
  std::list<T> high_;

  T& getFrontElement();
  void removeFrontElement();
};

template<typename T>
void QueueWithPriority<T>::put(const T& element, ElementPriority priority)
{
  switch (priority)
  {
  case ElementPriority::HIGH:
  {
    high_.push_back(element);
    break;
  }
  case ElementPriority::NORMAL:
  {
    normal_.push_back(element);
    break;
  }
  case ElementPriority::LOW:
  {
    low_.push_back(element);
    break;
  }
  }
}

template<typename T>
template<typename handlerType>
void QueueWithPriority<T>::handleElement(handlerType handler)
{
  handler(getFrontElement());
  removeFrontElement();
}

template<typename T>
void QueueWithPriority<T>::accelerate()
{
  high_.splice(high_.end(), low_);
}

template<typename T>
bool QueueWithPriority<T>::empty() const noexcept
{
  return (low_.empty() && normal_.empty() && high_.empty());
}

template<typename T>
std::size_t QueueWithPriority<T>::size() const noexcept
{
  return (low_.size() + normal_.size() + high_.size());
}

template<typename T>
void QueueWithPriority<T>::swap(QueueWithPriority<T>& other)
{
  low_.swap(other.low);
  normal_.swap(other.normal);
  high_.swap(other.high);
}

template<typename T>
T& QueueWithPriority<T>::getFrontElement()
{
  if (!high_.empty())
  {
    return high_.front();
  }
  else if (!normal_.empty())
  {
    return normal_.front();
  }
  else if (!low_.empty())
  {
    return low_.front();
  }
  throw std::invalid_argument("No elements in queue");
}

template<typename T>
void QueueWithPriority<T>::removeFrontElement()
{
  if (!high_.empty())
  {
    high_.pop_front();
    return;
  }
  else if (!normal_.empty())
  {
    normal_.pop_front();
    return;
  }
  else if (!low_.empty())
  {
    low_.pop_front();
    return;
  }
  throw std::invalid_argument("No elements in queue");
}
#endif // QUEUEWITHPRIORITY_HPP
