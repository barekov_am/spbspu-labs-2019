#ifndef TASKS_HPP
#define TASKS_HPP

void firstTask(const char* direction);
void secondTask(const char* filename);
void thirdTask();
void fourthTask(const char* direction, int size);

#endif // TASKS_HPP
