#include <vector>
#include "sort.hpp"
#include "tasks.hpp"

void thirdTask()
{
  std::vector<int> vector;
  int curr = -1;
  while ((std::cin) && !(std::cin >> curr).eof())
  {
    if (std::cin.fail())
    {
      throw std::invalid_argument("Invalid input data");
    }
    if (curr == 0)
    {
      break;
    }
    vector.push_back(curr);
  }

  if (vector.empty())
  {
    return;
  }

  if (std::cin.eof())
  {
    throw std::invalid_argument("Missing zero");
  }

  if (vector.back() == 1)
  {
    std::vector<int>::iterator iter = vector.begin();
    while (iter != vector.end())
    {
      iter = ((*iter) % 2 == 0) ? vector.erase(iter) : ++iter;
    }
  }

  if (vector.back() == 2)
  {
    std::vector<int>::iterator iter = vector.begin();
    while (iter != vector.end())
    {
      iter = ((*iter) % 3 == 0) ? (vector.insert(++iter, 3, 1) + 3) : ++iter;
    }
  }

  print(vector);
}
