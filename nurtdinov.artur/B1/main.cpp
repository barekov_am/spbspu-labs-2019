#include <string.h>
#include "sort.hpp"
#include "tasks.hpp"

int main(int argc, char* argv[])
{
  if (argc == 1)
  {
    std::cerr << "Incorrect number of arguments";
    return 1;
  }
  try
  {
    const int taskNumber = atoi(argv[1]);
    switch (taskNumber)
    {
    case 1:
    {
      if (argc < 3)
      {
        std::cerr << "Incorrect input data";
        return 1;
      }

      firstTask(argv[2]);

      break;
    }
    case 2:
    {
      if (argc < 3)
      {
        std::cerr << "Incorrect input data";
        return 1;
      }
      secondTask(argv[2]);
      break;
    }
    case 3:
    {
      thirdTask();
      break;
    }
    case 4:
    {
      if (argc < 4)
      {
        std::cerr << "Incorrect input data";
        return 1;
      }
      srand(time(0));
      fourthTask(argv[2], atoi(argv[3]));
      break;
    }
    default:
      std::cerr << "Incorrect task number";
      return 1;
    }
  }
  catch (const std::exception& ex)
  {
    std::cerr << ex.what();
    return 1;
  }
  return 0;
}
