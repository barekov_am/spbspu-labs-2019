#include <cmath>
#include <ctime>
#include <exception>
#include <vector>

#include "tasks.hpp"
#include "sort.hpp"

void fillRandom(double* array, int size)
{
  for (int i = 0; i < size; ++i)
  {
    array[i] = (rand() % 19 - 9) / 10.0;
  }
}

void fourthTask(const char* direction, int size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size cannot be < 0");
  }
  std::vector<double> vector(size);
  fillRandom(vector.data(), size);
  auto order = getOrder<double>(direction);
  print(vector);

  sort<access_at>(vector, order);
  print(vector);
}
