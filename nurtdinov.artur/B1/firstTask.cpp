#include <exception>
#include <forward_list>
#include <vector>

#include "sort.hpp"
#include "tasks.hpp"

void firstTask(const char* direction)
{
  std::vector<int> vectorBracket;
  int i = -1;
  while (std::cin && !(std::cin >> i).eof())
  {
    if (std::cin.fail())
    {
      throw std::invalid_argument("Incorrect input data");
    }
    vectorBracket.push_back(i);
  }
  std::vector<int> vectorAt(vectorBracket);
  std::forward_list<int> list(vectorBracket.begin(), vectorBracket.end());
  auto order = getOrder<int>(direction);
  sort<access_at>(vectorAt, order);
  print(vectorAt);

  sort<access_bracket>(vectorBracket, order);
  print(vectorBracket);

  sort<access_iter>(list, order);
  print(list);
}
