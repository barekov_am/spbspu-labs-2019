#ifndef ACCESSPOLICY_HPP
#define ACCESSPOLICY_HPP

#include <cstddef>
template<typename Container>
struct access_bracket
{

  static typename Container::reference getElement(Container& arr, std::size_t index)
  {
    return arr[index];
  }

  static std::size_t begin(const Container&)
  {
    return 0;
  }

  static std::size_t end(const Container& arr)
  {
    return arr.size();
  }

  static std::size_t next(const std::size_t index)
  {
    return index + 1;
  }
};

template<typename Container>
struct access_at
{
  static typename Container::reference getElement(Container& arr, std::size_t index)
  {
    return arr.at(index);
  }

  static std::size_t begin(const Container&)
  {
    return 0;
  }

  static std::size_t end(const Container& arr)
  {
    return arr.size();
  }

  static std::size_t next(const std::size_t index)
  {
    return index + 1;
  }
};

template<typename Container>
struct access_iter
{
  static typename Container::reference getElement(Container&, typename Container::iterator iter)
  {
    return *iter;
  }

  static typename Container::iterator begin(Container& arr)
  {
    return arr.begin();
  }

  static typename Container::iterator end(Container& arr)
  {
    return arr.end();
  }

  static typename Container::iterator next(typename Container::iterator iter)
  {
    return ++iter;
  }
};

#endif // ACCESSPOLICY_HPP
