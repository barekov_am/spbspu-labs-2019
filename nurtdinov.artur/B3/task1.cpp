#include <iostream>

#include "commands-parser.hpp"

void task1()
{
  Bookmarks bookmarks;
  CommandsParser parser(std::cin, std::cout, bookmarks);
  parser.parseTask();
}
