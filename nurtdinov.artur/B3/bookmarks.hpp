#ifndef BOOKMARKS_HPP
#define BOOKMARKS_HPP

#include <map>
#include "phonebook.hpp"

class Bookmarks
{
public:
  enum Position
  {
    FIRST,
    LAST
  };

  Bookmarks();

  void add(const PhoneBook::valueType& value);
  void store(const std::string& mark, const std::string& newMark);
  void remove(const std::string& mark);
  PhoneBook::valueType& get(const std::string& markName) const;
  void insertBefore(const std::string& mark, const PhoneBook::valueType& value);
  void insertAfter(const std::string& mark, const PhoneBook::valueType& value);
  void move(const std::string& mark, long steps);
  void move(const std::string& mark, Position position);
  bool empty() const;

private:
  using bookmarks_type = std::map<std::string, PhoneBook::iterator>;

  bookmarks_type bookmarks_;
  PhoneBook book_;

  bookmarks_type::iterator find(const std::string& mark);
  bookmarks_type::const_iterator find(const std::string& mark) const;
};
#endif // BOOKMARKS_HPP
