#include "factorial-iterator.hpp"

#include <stdexcept>

FactorialIterator::FactorialIterator() :
  index_(1),
  value_(1)
{
}

FactorialIterator::FactorialIterator(std::size_t index) :
  index_(index),
  value_(factorial(index))
{
}

unsigned long long& FactorialIterator::operator*()
{
  return value_;
}

bool FactorialIterator::operator==(const FactorialIterator& other) const
{
  return (index_ == other.index_);
}

bool FactorialIterator::operator!=(const FactorialIterator& other) const
{
  return !(*this == other);
}

FactorialIterator& FactorialIterator::operator++()
{
  ++index_;
  value_ *= index_;
  return *this;
}
FactorialIterator& FactorialIterator::operator--()
{
  if (index_ > 1)
  {
    value_ /= index_;
    --index_;
  }

  return *this;
}

FactorialIterator FactorialIterator::operator++(int)
{
  FactorialIterator temp(*this);
  ++(*this);
  return temp;
}
FactorialIterator FactorialIterator::operator--(int)
{
  FactorialIterator temp(*this);
  --(*this);
  return temp;
}

unsigned long long FactorialIterator::factorial(size_t index)
{
  if (index <= 1)
  {
    return 1;
  }
  else
  {
    return index * factorial(index - 1);
  }
}
