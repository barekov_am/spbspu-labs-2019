#ifndef COMMANDS_PARSER_HPP
#define COMMANDS_PARSER_HPP

#include <functional>

#include "bookmarks.hpp"

class CommandsParser
{
public:
  CommandsParser(std::istream& in, std::ostream& out, Bookmarks& bookmarks);

  void parseTask();

private:
  std::istream& istream_;
  std::ostream& ostream_;
  Bookmarks& bookmarks_;

  void parseCommandAdd();
  void parseCommandDelete();
  void parseCommandStore();
  void parseCommandInsert();
  void parseCommandShow();
  void parseCommandMove();

  std::string parseRecordNumber();
  std::string parseRecordName();
  std::string parseMarkName();

  void ignoreInvalid();
  void checkForRedutantArgs();
};

#endif // COMMANDS_PARSER_HPP
