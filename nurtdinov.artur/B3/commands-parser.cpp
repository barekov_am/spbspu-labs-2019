#include "commands-parser.hpp"
#include <iostream>
#include <algorithm>

template<typename Char, typename CharTraits>
std::basic_istream<Char, CharTraits>& blank(std::basic_istream<Char, CharTraits>& istream)
{
  using istream_type = std::basic_istream<Char, CharTraits>;
  using streambuf_type = std::basic_streambuf<Char, CharTraits>;
  using int_type = typename istream_type::int_type;
  using ctype_type = std::ctype<Char>;

  const ctype_type& ct = std::use_facet<ctype_type>(istream.getloc());
  const int_type eof = CharTraits::eof();
  streambuf_type* sb = istream.rdbuf();
  int_type c = sb->sgetc();

  while (!CharTraits::eq_int_type(c, eof) && ct.is(std::ctype_base::blank, CharTraits::to_char_type(c)))
  {
    c = sb->snextc();
  }

  if (CharTraits::eq_int_type(c, eof))
  {
    istream.setstate(std::ios_base::eofbit);
  }
  return istream;
}

CommandsParser::CommandsParser(std::istream& in, std::ostream& out, Bookmarks& bookmarks) :
  istream_(in),
  ostream_(out),
  bookmarks_(bookmarks)
{}

void CommandsParser::parseTask()
{
  const static struct
  {
    const char* name;
    void (CommandsParser::*function)();
  } COMMANDS[] = {
    {"add", &CommandsParser::parseCommandAdd},
    {"store", &CommandsParser::parseCommandStore},
    {"insert", &CommandsParser::parseCommandInsert},
    {"delete", &CommandsParser::parseCommandDelete},
    {"show", &CommandsParser::parseCommandShow},
    {"move", &CommandsParser::parseCommandMove}
  };

  std::string commandWord;
  while (istream_ >> blank >> commandWord)
  {
    auto command = std::find_if(std::begin(COMMANDS), std::end(COMMANDS),
        [&](const auto& el) { return commandWord == el.name; });

    if (command == std::end(COMMANDS))
    {
      ignoreInvalid();
      continue;
    }
    try
    {
      (this->*command->function)();
    }
    catch (const std::invalid_argument& ex)
    {
      ostream_ << ex.what();
    }
  }
  if (!istream_.eof() && istream_.fail())
  {
    throw std::ios_base::failure("Bad input\n");
  }
}

void CommandsParser::parseCommandAdd()
{
  std::string number = parseRecordNumber();
  std::string name = parseRecordName();
  checkForRedutantArgs();

  PhoneBook::valueType value{name, number};
  bookmarks_.add(value);
}

void CommandsParser::parseCommandStore()
{
  std::string mark = parseMarkName();
  std::string newMark = parseMarkName();
  checkForRedutantArgs();
  bookmarks_.store(mark, newMark);
}

void CommandsParser::parseCommandInsert()
{
  std::string position;
  if ((istream_ >> blank).peek() == '\n')
  {
    ignoreInvalid();
    return;
  }

  istream_ >> position;
  if (position != "before" && position != "after")
  {
    ignoreInvalid();
    return;
  }

  std::string mark = parseMarkName();
  std::string number = parseRecordNumber();
  std::string name = parseRecordName();

  checkForRedutantArgs();

  if (position == "after")
  {
    bookmarks_.insertAfter(mark, {name, number});
  }
  else
  {
    bookmarks_.insertBefore(mark, {name, number});
  }
}

void CommandsParser::parseCommandDelete()
{
  std::string mark = parseMarkName();
  checkForRedutantArgs();

  bookmarks_.remove(mark);
}

void CommandsParser::parseCommandShow()
{
  std::string mark = parseMarkName();
  checkForRedutantArgs();

  PhoneBook::valueType record = bookmarks_.get(mark);
  ostream_ << record.number << ' ' << record.name << '\n';
}

void CommandsParser::parseCommandMove()
{
  std::string mark = parseMarkName();
  if ((istream_ >> blank).peek() == '\n')
  {
    ignoreInvalid();
    return;
  }
  std::string stepWord;
  istream_ >> stepWord;

  checkForRedutantArgs();

  if (stepWord == "first")
  {
    bookmarks_.move(mark, Bookmarks::FIRST);
  }
  else if (stepWord == "last")
  {
    bookmarks_.move(mark, Bookmarks::LAST);
  }
  else
  {
    char* end;
    long step = std::strtol(stepWord.c_str(), &end, 10);
    if (stepWord.empty() || *end != '\0')
    {
      ostream_ << "<INVALID STEP>\n";
      return;
    }

    bookmarks_.move(mark, step);
  }
}

std::string CommandsParser::parseRecordNumber()
{
  istream_ >> blank;
  if ((istream_ >> blank).peek() == '\n')
  {
    istream_.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    throw std::invalid_argument("<INVALID COMMAND>\n");
  }

  std::string number;
  istream_ >> number;

  auto checkForDigits = std::find_if(std::begin(number), std::end(number), [](char c) { return !isdigit(c); });

  if (checkForDigits != std::end(number))
  {
    istream_.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    throw std::invalid_argument("<INVALID COMMAND>\n");
  }

  return number;
}

std::string CommandsParser::parseRecordName()
{
  if ((istream_ >> blank).peek() != '\"')
  {
    istream_.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    throw std::invalid_argument("<INVALID COMMAND>\n");
  }

  istream_.ignore();
  std::string name;
  while (istream_)
  {
    const char c = istream_.get();
    if (c == '\\')
    {
      const char next = istream_.get();
      if (next == '\"' || next == '\\')
      {
        name += next;
        continue;
      }
      else
      {
        istream_.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        throw std::invalid_argument("<INVALID COMMAND>\n");
      }
    }
    if (c == '\"')
    {
      return name;
    }

    name += c;
  }

  istream_.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  throw std::invalid_argument("<INVALID COMMAND>\n");
}

std::string CommandsParser::parseMarkName()
{
  if ((istream_ >> blank).peek() == '\n')
  {
    istream_.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    throw std::invalid_argument("<INVALID COMMAND>\n");
  }
  std::string mark;
  istream_ >> mark;

  auto checkForCorrectName = std::find_if(std::begin(mark), std::end(mark), [](char c) { return !isalnum(c) && c != '-'; });

  if (checkForCorrectName != std::end(mark))
  {
    istream_.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    throw std::invalid_argument("<INVALID COMMAND>\n");
  }

  return mark;
}

void CommandsParser::ignoreInvalid()
{
  ostream_ << "<INVALID COMMAND>\n";
  istream_.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

void CommandsParser::checkForRedutantArgs()
{
  if ((istream_ >> blank).get() != '\n' && !istream_.eof())
  {
    istream_.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    throw std::invalid_argument("<INVALID COMMAND>\n");
  }
}
