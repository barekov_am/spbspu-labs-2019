#include "factorial-container.hpp"

const int MAX_SIZE = 11;

FactorialIterator FactorialContainer::begin()
{
  return FactorialIterator(1);
}

FactorialIterator FactorialContainer::end()
{
  return FactorialIterator(MAX_SIZE);
}
