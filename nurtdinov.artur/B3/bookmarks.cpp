#include "bookmarks.hpp"

#include <iostream>
Bookmarks::Bookmarks() :
  bookmarks_(),
  book_()
{
  bookmarks_["current"] = book_.end();
}

void Bookmarks::add(const PhoneBook::valueType& value)
{
  book_.push_bask(value);
  if (book_.size() == 1)
  {
    bookmarks_["current"] = book_.begin();
  }
}

void Bookmarks::store(const std::string& mark, const std::string& newMark)
{
  bookmarks_[newMark] = find(mark)->second;
}

void Bookmarks::remove(const std::string& mark)
{
  auto removed = find(mark);
  if (removed->second == book_.end())
  {
    throw std::invalid_argument("<EMPTY>\n");
  }

  auto next = book_.erase(removed->second);
  if (next == book_.end())
  {
    next = book_.begin();
  }

  for (auto& el : bookmarks_)
  {
    if (el.second == removed->second)
    {
      el.second = next;
    }
  }
}

PhoneBook::valueType& Bookmarks::get(const std::string& mark) const
{
  auto iter = find(mark);
  if (iter->second != book_.cend())
  {
    return *iter->second;
  }
  throw std::invalid_argument("<EMPTY>\n");
}

void Bookmarks::insertBefore(const std::string& mark, const PhoneBook::valueType& value)
{
  auto iter = find(mark);
  if (empty())
  {
    add(value);
  }
  book_.insert(iter->second, value);
}

void Bookmarks::insertAfter(const std::string& mark, const PhoneBook::valueType& value)
{
  auto iter = find(mark);
  if (empty())
  {
    add(value);
  }
  book_.insert(std::next(iter->second), value);
}

void Bookmarks::move(const std::string& mark, long steps)
{
  auto iter = find(mark);
  std::advance(iter->second, steps);
}

void Bookmarks::move(const std::string& mark, Bookmarks::Position position)
{
  auto iter = find(mark);
  if (position == Bookmarks::FIRST)
  {
    iter->second = book_.begin();
  }
  else
  {
    iter->second = std::prev(book_.end());
  }
}

bool Bookmarks::empty() const
{
  return book_.empty();
}

Bookmarks::bookmarks_type::iterator Bookmarks::find(const std::string& mark)
{
  auto iter = bookmarks_.find(mark);
  if (iter != bookmarks_.end())
  {
    return iter;
  }
  throw std::invalid_argument("<INVALID BOOKMARK>\n");
}

Bookmarks::bookmarks_type::const_iterator Bookmarks::find(const std::string& mark) const
{
  auto iter = bookmarks_.find(mark);
  if (iter != bookmarks_.cend())
  {
    return iter;
  }
  throw std::invalid_argument("<INVALID BOOKMARK>\n");
}
