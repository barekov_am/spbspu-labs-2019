#include "phonebook.hpp"

PhoneBook::iterator PhoneBook::begin() noexcept
{
  std::list<int> list;
  return list_.begin();
}

PhoneBook::const_iterator PhoneBook::cbegin() const noexcept
{
  return list_.cbegin();
}

PhoneBook::iterator PhoneBook::end() noexcept
{
  return list_.end();
}

PhoneBook::const_iterator PhoneBook::cend() const noexcept
{
  return list_.cend();
}


void PhoneBook::push_bask(const PhoneBook::valueType &value)
{
  list_.push_back(value);
}

PhoneBook::iterator PhoneBook::insert(PhoneBook::iterator pos, const PhoneBook::valueType& value)
{
  return list_.insert(pos, value);
}

void PhoneBook::replace(PhoneBook::iterator pos, const PhoneBook::valueType& value)
{
  *pos = value;
}

PhoneBook::iterator PhoneBook::erase(PhoneBook::iterator pos)
{
  return list_.erase(pos);
}

bool PhoneBook::empty() const noexcept
{
  return list_.empty();
}

std::size_t PhoneBook::size() const noexcept
{
  return list_.size();
}
