#ifndef FACTORIALCONTAINER_HPP
#define FACTORIALCONTAINER_HPP

#include "factorial-iterator.hpp"

class FactorialContainer
{
public:
  FactorialIterator begin();
  FactorialIterator end();
};

#endif // FACTORIALCONTAINER_HPP
