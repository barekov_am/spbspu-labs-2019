#include <iostream>

void task1();
void task2();

int main(int argc, char* argv[])
{
  if (argc != 2)
  {
    std::cerr << "Invalid number of arguments";
    return 1;
  }
  const int task = atoi(argv[1]);

  try
  {
    switch (task)
    {
    case 1:
    {
      task1();
      break;
    }

    case 2:
    {
      task2();
      break;
    }

    default:
    {
      std::cerr << "Invalid task number";
      return 1;
    }
    }
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what();
    return 1;
  }

  return 0;
}
