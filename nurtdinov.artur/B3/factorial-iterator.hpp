#ifndef FACTORIALITERATOR_HPP
#define FACTORIALITERATOR_HPP

#include <memory>

class FactorialIterator : public std::iterator<std::bidirectional_iterator_tag, unsigned long long, std::ptrdiff_t,
                                               unsigned long long*, unsigned long long>
{
public:
  using valueType = unsigned long long;

  FactorialIterator();
  FactorialIterator(std::size_t index);

  valueType& operator*();

  FactorialIterator& operator++();
  FactorialIterator& operator--();

  FactorialIterator operator++(int);
  FactorialIterator operator--(int);

  bool operator!=(const FactorialIterator& other) const;
  bool operator==(const FactorialIterator& other) const;

private:
  std::size_t index_;
  valueType value_;

  unsigned long long factorial(size_t index);
};

#endif // FACTORIALITERATOR_HPP
