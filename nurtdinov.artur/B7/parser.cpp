#include <iostream>
#include <algorithm>
#include "shape.hpp"

std::string readName(std::string& line)
{
  line.erase(std::remove_if(line.begin(), line.end(), [](const char c) { return std::isspace(c); }), line.end());
  auto openBracketPos = line.find_first_of("(");
  std::string res = line.substr(0, openBracketPos);
  line.erase(0, openBracketPos);
  return res;
}

point_t readPoint(std::string& line)
{
  if (line.empty())
  {
    throw std::invalid_argument("Input is empty\n");
  }
  if ((line.at(0) != '(') || (line.at(line.size() - 1) != ')'))
  {
    throw std::invalid_argument("Failed while parsing coordinates\n");
  }
  line = line.substr(1, line.size() - 1);
  auto separator = line.find_first_of(";");

  char* end;
  int x = std::strtol(line.substr(0, separator).c_str(), &end, 10);
  if (*end != '\0')
  {
    throw std::invalid_argument("Failed while parsing coordinates on x\n");
  }

  int y = std::strtol(line.substr(separator + 1, line.size() - separator - 2).c_str(), &end, 10);
  if (*end != '\0')
  {
    std::cout << line.at(separator);
    std::cout << line.size() - separator - 1;
    throw std::invalid_argument("Failed while parsing coordinates on y\n");
  }
  line.clear();
  return {x, y};
}
