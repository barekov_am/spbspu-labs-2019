#ifndef EXACTSHAPES_HPP
#define EXACTSHAPES_HPP

#include <iostream>

#include "shape.hpp"

class Circle : public Shape
{
public:
  Circle(const point_t& center);
  void draw(std::ostream& ostream) override;
};

class Triangle : public Shape
{
public:
  Triangle(const point_t& center);
  void draw(std::ostream& ostream) override;
};

class Square : public Shape
{
public:
  Square(const point_t& center);
  void draw(std::ostream& ostream) override;
};

#endif // EXACTSHAPES_HPP
