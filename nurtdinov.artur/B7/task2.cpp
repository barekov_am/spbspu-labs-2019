#include <algorithm>
#include <memory>
#include <iostream>
#include <vector>

#include "exactShapes.hpp"

std::shared_ptr<Shape> create(const std::string& name, const point_t& point);
std::string readName(std::string& line);
point_t readPoint(std::string& line);
void printVector(const std::vector<std::shared_ptr<Shape>>& vector);

void task2()
{
  std::vector<std::shared_ptr<Shape>> shapes;
  std::string line;
  while (getline(std::cin >> std::ws, line))
  {
    std::string name = readName(line);
    point_t point = readPoint(line);
    shapes.push_back(create(name, point));
  }

  std::cout << "Original:\n";
  printVector(shapes);

  //sorting left
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape>& lhs, const std::shared_ptr<Shape>& rhs)
  {
    return lhs->isMoreLeft(rhs.get());
  });
  std::cout << "Left-Right:\n";
  printVector(shapes);

  //sorting right
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape>& lhs, const std::shared_ptr<Shape>& rhs)
  {
    return !lhs->isMoreLeft(rhs.get());
  });
  std::cout << "Right-Left:\n";
  printVector(shapes);

  //sorting top-bottom
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape>& lhs, const std::shared_ptr<Shape>& rhs)
  {
    return lhs->isUpper(rhs.get());
  });
  std::cout << "Top-Bottom:\n";
  printVector(shapes);

  //sorting bottom-top
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape>& lhs, const std::shared_ptr<Shape>& rhs)
  {
    return !lhs->isUpper(rhs.get());
  });
  std::cout << "Bottom-Top:\n";
  printVector(shapes);
}

void printVector(const std::vector<std::shared_ptr<Shape>>& vector)
{
  for (const auto& shape : vector)
  {
    shape->draw(std::cout);
  }
}
