#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <cmath>

void task1()
{
  std::vector<double> numbers((std::istream_iterator<double>(std::cin)), std::istream_iterator<double>());

  if (!std::cin.eof())
  {
    throw std::invalid_argument("Wrong input were not processed\n");
  }

  std::vector<double> multipliedNumbers(numbers.size());
  std::transform(numbers.begin(), numbers.end(), multipliedNumbers.begin(),
                 std::bind1st(std::multiplies<double>(), M_PI));

  for (double it : multipliedNumbers)
  {
    std::cout << it << " ";
  }
  std::cout << "\n";
}
