#include <memory>
#include "exactShapes.hpp"

std::shared_ptr<Shape> create(const std::string& name, const point_t& point)
{
  if (name == "CIRCLE")
  {
    return std::make_shared<Circle>(Circle(point));
  }
  if (name == "TRIANGLE")
  {
    return std::make_shared<Triangle>(Triangle(point));
  }
  if (name == "SQUARE")
  {
    return std::make_shared<Square>(Square(point));
  }
  throw std::invalid_argument("Unknown name\n");
}
