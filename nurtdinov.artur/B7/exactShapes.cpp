#include "exactShapes.hpp"

Circle::Circle(const point_t& center) :
  Shape(center)
{}

void Circle::draw(std::ostream& ostream)
{
  ostream << "CIRCLE (" << center_.x << ";" << center_.y << ")\n";
}

Triangle::Triangle(const point_t& center) :
  Shape(center)
{}

void Triangle::draw(std::ostream& ostream)
{
  ostream << "TRIANGLE (" << center_.x << ";" << center_.y << ")\n";
}

Square::Square(const point_t& center) :
  Shape(center)
{}

void Square::draw(std::ostream& ostream)
{
  ostream << "SQUARE (" << center_.x << ";" << center_.y << ")\n";
}
