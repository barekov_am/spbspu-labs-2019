#include "rectangle.hpp"

#include <stdexcept>
#include <cmath>

nurtdinov::Rectangle::Rectangle(const point_t &point, double width, double height) :
  position_(point),
  width_(width),
  height_(height),
  angle_(0)
{
  if (width_ <= 0.0)
  {
    throw std::invalid_argument("Width must be positive.");
  }
  if (height_ <= 0.0)
  {
    throw std::invalid_argument("Height must be positive.");
  }
}

nurtdinov::Rectangle::Rectangle(const point_t &point, double width, double height, double angle):
  Rectangle(point, width, height)
{
  rotate(angle);
}

double nurtdinov::Rectangle::getArea() const
{
  return width_ * height_;
}

nurtdinov::rectangle_t nurtdinov::Rectangle::getFrameRect() const
{
  const double sin = std::sin(angle_ * M_PI / 180);
  const double cos = std::cos(angle_ * M_PI / 180);
  const double width = std::abs(width_ * cos) + std::abs(height_ * sin);
  const double height = std::abs(width_ * sin) + std::abs(height_ * cos);
  return {width, height, position_};
}

void nurtdinov::Rectangle::move(const nurtdinov::point_t &point)
{
  position_ = point;
}

void nurtdinov::Rectangle::move(double dx, double dy)
{
  position_.x += dx;
  position_.y += dy;
}

void nurtdinov::Rectangle::scale(double ratio)
{
  if (ratio <= 0)
  {
    throw std::invalid_argument("Scale ratio must be positive");
  }
  width_ *= ratio;
  height_ *= ratio;
}

void nurtdinov::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
