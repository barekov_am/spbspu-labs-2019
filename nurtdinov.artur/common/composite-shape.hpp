#ifndef MAIN_COMPOSITE_SHAPE_HPP
#define MAIN_COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace nurtdinov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &compositeShape);
    CompositeShape(CompositeShape &&compositeShape);
    ~CompositeShape() = default;
    CompositeShape &operator=(const CompositeShape &rhs);
    CompositeShape &operator=(CompositeShape &&rhs);
    ptr operator[](size_t index) const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &point) override;
    void scale(double ratio) override;
    void add(const ptr shape);
    void remove(size_t index);
    size_t getSize() const;
    array_ptr getList() const;
    void rotate(double angle) override;

  private:
    size_t count_;
    array_ptr shapes_;
  };
}

#endif //MAIN_COMPOSITE_SHAPE_HPP
