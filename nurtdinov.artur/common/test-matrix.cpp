#include <stdexcept>
#include <algorithm>
#include <boost/test/auto_unit_test.hpp>
#include "matrix.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(copyConstructorTest)
{
  nurtdinov::Matrix<double> matrixFirst;
  for (int i = 0; i < 10; i++)
  {
    matrixFirst.add(i, std::make_shared<double>(i / 10));
  }
  nurtdinov::Matrix<double> matrixSecond(matrixFirst);
  BOOST_CHECK(matrixFirst == matrixSecond);
}

BOOST_AUTO_TEST_CASE(moveConstructorTest)
{
  nurtdinov::Matrix<double> matrixFirst;
  for (int i = 0; i < 10; i++)
  {
    matrixFirst.add(i, std::make_shared<double>(i / 10));
  }
  //we need to copy one matrix to compare with
  nurtdinov::Matrix<double> copiedMatrix(matrixFirst);

  nurtdinov::Matrix<double> matrixSecond(std::move(matrixFirst));
  BOOST_CHECK(copiedMatrix == matrixSecond);
  //we can just create new empty matrix and check it with matrixFirst
  //except of checking every parameter of moved object
  BOOST_CHECK(nurtdinov::Matrix<double>() == matrixFirst);
}

BOOST_AUTO_TEST_CASE(copyOperatorTest)
{
  nurtdinov::Matrix<double> matrixFirst;
  for (int i = 0; i < 10; i++)
  {
    matrixFirst.add(i, std::make_shared<double>(i / 10));
  }
  nurtdinov::Matrix<double> matrixSecond = matrixFirst;
  BOOST_CHECK(matrixFirst == matrixSecond);
}

BOOST_AUTO_TEST_CASE(moveOperatorTest)
{
  nurtdinov::Matrix<double> matrixFirst;
  for (int i = 0; i < 10; i++)
  {
    matrixFirst.add(i, std::make_shared<double>(i / 10));
  }
  //we need to copy one matrix to compare with
  nurtdinov::Matrix<double> copiedMatrix(matrixFirst);

  nurtdinov::Matrix<double> matrixSecond = std::move(matrixFirst);
  BOOST_CHECK(copiedMatrix == matrixSecond);
  //we can just create new empty matrix and check it with matrixFirst
  //except of checking every parameter of moved object
  BOOST_CHECK(nurtdinov::Matrix<double>() == matrixFirst);
}

BOOST_AUTO_TEST_CASE(addingSizeTest)
{
  const int row1 = 0;
  const int row2 = 1;

  nurtdinov::Matrix<double> matrix;
  matrix.add(row1, std::make_shared<double>(8.5));
  matrix.add(row2, std::make_shared<double>(9.1));

  BOOST_CHECK_EQUAL(matrix.getRows(), std::max(row1, row2) + 1); // + 1 because row1 & row2 are indexes
  BOOST_CHECK_EQUAL(matrix[0].getSize(), 1);
  BOOST_CHECK_EQUAL(matrix[1].getSize(), 1);
  std::shared_ptr<double> valuePtr = std::make_shared<double>(1.5);
  matrix.add(row1, valuePtr);
  BOOST_CHECK_EQUAL(matrix[0].getSize(), 2);
  BOOST_CHECK_EQUAL(matrix[0][1], valuePtr);
  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), 2);
}

BOOST_AUTO_TEST_CASE(matrixExceptionThrowTest)
{
  nurtdinov::Matrix<double> matrix;
  matrix.add(0, std::make_shared<double>(1.1));
  matrix.add(0, std::make_shared<double>(1.5));

  BOOST_CHECK_THROW(matrix[-3], std::out_of_range);
  BOOST_CHECK_THROW(matrix[7][0], std::out_of_range);
  BOOST_CHECK_THROW(matrix.add(-1, std::make_shared<double>(5.5)), std::out_of_range);
  BOOST_CHECK_THROW(matrix.add(0, nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
