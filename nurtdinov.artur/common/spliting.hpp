#ifndef MAIN_SPLITING_HPP
#define MAIN_SPLITING_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace nurtdinov
{
  Matrix<Shape> split(const CompositeShape &compositeShape);
  Matrix<Shape> split(const nurtdinov::array_ptr &shapes, size_t size);
  bool intersect(const nurtdinov::Shape &shape1, const nurtdinov::Shape &shape2);
}
#endif //MAIN_SPLITING_HPP
