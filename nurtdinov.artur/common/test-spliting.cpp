#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "spliting.hpp"
#include "matrix.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(layeringTestSuite)

BOOST_AUTO_TEST_CASE(emptyCompositeSplitingTest)
{
  const nurtdinov::CompositeShape emptyComposite;
  nurtdinov::Matrix<nurtdinov::Shape> testMatrix = nurtdinov::split(emptyComposite);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 0);
  BOOST_CHECK_THROW(testMatrix[0], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeSplitingTest)
{
  nurtdinov::Rectangle rectangle1({2, 2}, 6, 4);
  nurtdinov::Rectangle rectangle2({-10, -1}, 3, 3);
  nurtdinov::Rectangle rectangle3({0, -1}, 2, 2);
  nurtdinov::Circle circle1({0, 0}, 3);
  nurtdinov::CompositeShape compositeShape;
  compositeShape.add(std::make_shared<nurtdinov::Rectangle>(rectangle1));   //1
  compositeShape.add(std::make_shared<nurtdinov::Circle>(circle1));         //2
  compositeShape.add(std::make_shared<nurtdinov::Rectangle>(rectangle2));   //3
  compositeShape.add(std::make_shared<nurtdinov::Rectangle>(rectangle3));   //4
  // 1 intersects with 2 and with 4. So rows = 3
  // 3 doesn't intersect with any, so cols = 2. And 3 is in matrix[0][1]

  nurtdinov::Matrix<nurtdinov::Shape> matrix = nurtdinov::split(compositeShape);

  BOOST_CHECK_EQUAL(matrix.getRows(), 3);
  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), 2);
  BOOST_CHECK_EQUAL(matrix[0][0], compositeShape[0]);
  BOOST_CHECK_EQUAL(matrix[0][1], compositeShape[2]);
  BOOST_CHECK_EQUAL(matrix[1][0], compositeShape[1]);
  BOOST_CHECK_EQUAL(matrix[2][0], compositeShape[3]);
  BOOST_CHECK_EQUAL(matrix[0].getSize(), 2);
  BOOST_CHECK_EQUAL(matrix[1].getSize(), 1);
  BOOST_CHECK_EQUAL(matrix[2].getSize(), 1);

  const int sizeOfComp = compositeShape.getSize();
  nurtdinov::array_ptr shapes(std::make_unique<nurtdinov::ptr[]>(sizeOfComp));
  shapes = compositeShape.getList();
  nurtdinov::Matrix<nurtdinov::Shape> matrixSecond = nurtdinov::split(compositeShape);

  BOOST_CHECK_EQUAL(matrixSecond.getRows(), 3);
  BOOST_CHECK_EQUAL(matrixSecond.getMaxColSize(), 2);
  BOOST_CHECK_EQUAL(matrixSecond[0][0], compositeShape[0]);
  BOOST_CHECK_EQUAL(matrixSecond[0][1], compositeShape[2]);
  BOOST_CHECK_EQUAL(matrixSecond[1][0], compositeShape[1]);
  BOOST_CHECK_EQUAL(matrixSecond[2][0], compositeShape[3]);

  nurtdinov::ptr circlePtr = std::make_shared<nurtdinov::Circle>(nurtdinov::Circle({10, 10}, 2));
  size_t placeRow = 0;
  matrixSecond.add(placeRow, circlePtr);
  BOOST_CHECK_EQUAL(matrixSecond[placeRow][2], circlePtr);
  BOOST_CHECK_EQUAL(matrixSecond.getRows(), 3);
  BOOST_CHECK_EQUAL(matrixSecond.getMaxColSize(), 3);

  nurtdinov::ptr rectanglePtr = std::make_shared<nurtdinov::Rectangle>(nurtdinov::Rectangle({10, 10}, 2, 2));
  placeRow = 3;
  matrixSecond.add(placeRow, rectanglePtr);
  BOOST_CHECK_EQUAL(matrixSecond[placeRow][0], rectanglePtr);
  BOOST_CHECK_EQUAL(matrixSecond.getRows(), 4);
  BOOST_CHECK_EQUAL(matrixSecond.getMaxColSize(), 3);
}

BOOST_AUTO_TEST_CASE(intersectTest)
{
  nurtdinov::Rectangle rectangle({5, 5}, 6, 4);
  nurtdinov::Circle circle1({3, 2}, 2);
  nurtdinov::Circle circle2({-4, -3}, 2);

  BOOST_CHECK(nurtdinov::intersect(rectangle, circle1));
  BOOST_CHECK(!nurtdinov::intersect(rectangle, circle2));
}

BOOST_AUTO_TEST_SUITE_END()
