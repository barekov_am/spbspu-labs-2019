#include "composite-shape.hpp"

#include <algorithm>
#include <stdexcept>
#include <cmath>

nurtdinov::CompositeShape::CompositeShape() :
  count_(0)
{
}

nurtdinov::CompositeShape::CompositeShape(const nurtdinov::CompositeShape &compositeShape) :
  count_(compositeShape.count_),
  shapes_(std::make_unique<ptr[]>(compositeShape.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i] = compositeShape.shapes_[i];
  }
}

nurtdinov::CompositeShape::CompositeShape(nurtdinov::CompositeShape &&compositeShape) :
  count_(compositeShape.count_),
  shapes_(std::move(compositeShape.shapes_))
{
  compositeShape.count_ = 0;
}

nurtdinov::CompositeShape &nurtdinov::CompositeShape::operator=(const nurtdinov::CompositeShape &rhs)
{
  if (this != &rhs)
  {
    shapes_ = std::make_unique<ptr[]>(rhs.count_);
    count_ = rhs.count_;
    for (size_t i = 0; i < count_; i++)
    {
      shapes_[i] = rhs.shapes_[i];
    }
  }
  return *this;
}

nurtdinov::CompositeShape &nurtdinov::CompositeShape::operator=(nurtdinov::CompositeShape &&rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    rhs.count_ = 0;
    shapes_ = std::move(rhs.shapes_);
  }
  return *this;
}

nurtdinov::ptr nurtdinov::CompositeShape::operator[](size_t index) const
{
  if (count_ <= index)
  {
    throw std::out_of_range("Invalid index");
  }
  return shapes_[index];
}

double nurtdinov::CompositeShape::getArea() const
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("Composite shape is empty");
  }
  double resultArea = 0;
  for (size_t i = 0; i < count_; i++)
  {
    resultArea += shapes_[i]->getArea();
  }
  return resultArea;
}

nurtdinov::rectangle_t nurtdinov::CompositeShape::getFrameRect() const
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("Composite shape is empty");
  }
  rectangle_t frameRect = shapes_[0]->getFrameRect();
  double minY = frameRect.pos.y - frameRect.height / 2;
  double maxY = frameRect.pos.y + frameRect.height / 2;
  double minX = frameRect.pos.x - frameRect.width / 2;
  double maxX = frameRect.pos.x + frameRect.width / 2;

  for (size_t i = 1; i < count_; i++)
  {
    frameRect = shapes_[i]->getFrameRect();
    double compared = frameRect.pos.y - frameRect.height / 2;
    minY = std::min(compared, minY);
    compared = frameRect.pos.y + frameRect.height / 2;
    maxY = std::max(compared, maxY);
    compared = frameRect.pos.x - frameRect.width / 2;
    minX = std::min(compared, minX);
    compared = frameRect.pos.x + frameRect.width / 2;
    maxX = std::max(compared, maxX);
  }
  //now we can find frame rectangle
  //it's width = maxX - minX, height = maxY - minY
  //and it's center could be found as sum coordinates divided by 2
  return rectangle_t{maxX - minX, maxY - minY, {(maxX + minX) / 2, (maxY + minY) / 2}};
}

void nurtdinov::CompositeShape::move(double dx, double dy)
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("Composite shape is empty");
  }
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void nurtdinov::CompositeShape::move(const nurtdinov::point_t &point)
{
  const nurtdinov::point_t framePos = getFrameRect().pos;
  const double dx = point.x - framePos.x;
  const double dy = point.y - framePos.y;
  move(dx, dy);
}

void nurtdinov::CompositeShape::scale(double ratio)
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("Composite shape is empty");
  }
  if (ratio <= 0)
  {
    throw std::invalid_argument("Scale ratio must be positive");
  }
  rectangle_t frameRect = getFrameRect();
  for (size_t i = 0; i < count_; i++)
  {
    nurtdinov::point_t figureCenter = shapes_[i]->getFrameRect().pos;
    double dx = (figureCenter.x - frameRect.pos.x) * (ratio - 1);
    double dy = (figureCenter.y - frameRect.pos.y) * (ratio - 1);
    shapes_[i]->move(dx, dy);
    shapes_[i]->scale(ratio);
  }
}

void nurtdinov::CompositeShape::add(const ptr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Pointer on shape you try to add is nullptr");
  }
  for (size_t i = 0; i < count_; i++)
  {
    if (shapes_[i] == shape)
    {
      return;
    }
  }

  array_ptr shapesTmp = std::make_unique<ptr[]>(count_ + 1);
  count_++;
  for (size_t i = 0; i < count_ - 1; i++)
  {
    shapesTmp[i] = shapes_[i];
  }
  shapesTmp[count_ - 1] = shape;
  shapes_ = std::move(shapesTmp);
}

void nurtdinov::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Invalid index");
  }

  for (size_t i = index; i < count_ - 1; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  shapes_[--count_] = nullptr;
}

size_t nurtdinov::CompositeShape::getSize() const
{
  return count_;
}

void nurtdinov::CompositeShape::rotate(double angle)
{
  const nurtdinov::point_t framePos = getFrameRect().pos;
  const double sin = std::sin(angle * M_PI / 180);
  const double cos = std::cos(angle * M_PI / 180);
  for (size_t i = 0; i < count_; i++)
  {
    const nurtdinov::point_t shapePos = shapes_[i]->getFrameRect().pos;
    const double dx = shapePos.x - framePos.x;
    const double dy = shapePos.y - shapePos.y;
    shapes_[i]->move(dx * (cos - 1) - dy * sin, dx * sin + dy * (cos - 1));
    shapes_[i]->rotate(angle);
  }
}

nurtdinov::array_ptr nurtdinov::CompositeShape::getList() const
{
  array_ptr arrayTmp(std::make_unique<ptr[]>(count_));
  for (size_t i = 0; i < count_; i++)
  {
    arrayTmp[i] = shapes_[i];
  }
  return arrayTmp;
}
