#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

const double ERROR_VALUE = 0.001;

BOOST_AUTO_TEST_SUITE(CompositeTesting)

BOOST_AUTO_TEST_CASE(compositeParamsSavedAfterMovingToPoint)
{
  nurtdinov::Circle circle({15.4, 20.3}, 3);
  nurtdinov::Rectangle rectangle({3, 2}, 4.5, 8.1);
  nurtdinov::CompositeShape compositeShape;
  compositeShape.add(std::make_shared<nurtdinov::Circle>(circle));
  compositeShape.add(std::make_shared<nurtdinov::Rectangle>(rectangle));
  const nurtdinov::rectangle_t frameBefore = compositeShape.getFrameRect();
  const double areaBefore = compositeShape.getArea();

  compositeShape.move({3.3, 4.8});
  const nurtdinov::rectangle_t frameAfter = compositeShape.getFrameRect();
  const double areaAfter = compositeShape.getArea();
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(compositeParamsSavedAfterShiftMove)
{
  nurtdinov::Circle circle({1.1, 1.4}, 2.2);
  nurtdinov::Rectangle rectangle({5.4, 7.5}, 1.2, 2.3);
  nurtdinov::CompositeShape compositeShape;
  compositeShape.add(std::make_shared<nurtdinov::Circle>(circle));
  compositeShape.add(std::make_shared<nurtdinov::Rectangle>(rectangle));
  const nurtdinov::rectangle_t frameBefore = compositeShape.getFrameRect();
  const double areaBefore = compositeShape.getArea();

  compositeShape.move(18.2, 4.5);
  const nurtdinov::rectangle_t frameAfter = compositeShape.getFrameRect();
  const double areaAfter = compositeShape.getArea();
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(compositeScaleTestIncrease)
{
  nurtdinov::Circle circle({14.1, 8.5}, 2.1);
  nurtdinov::Rectangle rectangle({3.3, 1.3}, 4.4, 2.3);
  nurtdinov::CompositeShape compositeShape;
  compositeShape.add(std::make_shared<nurtdinov::Circle>(circle));
  compositeShape.add(std::make_shared<nurtdinov::Rectangle>(rectangle));
  const double areaBefore = compositeShape.getArea();
  double scaleRatio = 3.5;
  compositeShape.scale(scaleRatio);
  const double areaAfter = compositeShape.getArea();
  BOOST_CHECK_CLOSE(areaBefore * scaleRatio * scaleRatio, areaAfter, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(compositeScaleTestDecrease)
{
  nurtdinov::Circle circle({4.1, 3.2}, 1.5);
  nurtdinov::Rectangle rectangle({9.2, 1.6}, 2.8, 3.9);
  nurtdinov::CompositeShape compositeShape;
  compositeShape.add(std::make_shared<nurtdinov::Circle>(circle));
  compositeShape.add(std::make_shared<nurtdinov::Rectangle>(rectangle));
  const double areaBefore = compositeShape.getArea();
  const double scaleRatio = 0.3;
  compositeShape.scale(scaleRatio);
  const double areaAfter = compositeShape.getArea();
  BOOST_CHECK_CLOSE(areaBefore * scaleRatio * scaleRatio, areaAfter, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(paramsChangeAfterCompositeRotation)
{
  nurtdinov::CompositeShape compositeShape;
  compositeShape.add(std::make_shared<nurtdinov::Circle>(nurtdinov::point_t{2, 2}, 1));
  compositeShape.add(std::make_shared<nurtdinov::Rectangle>(nurtdinov::point_t{4, 2}, 2, 2));

  const nurtdinov::rectangle_t frameBefore = compositeShape.getFrameRect();
  const double areaBefore = compositeShape.getArea();
  compositeShape.rotate(90);
  const double areaAfter = compositeShape.getArea();
  const nurtdinov::rectangle_t frameAfter = compositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ERROR_VALUE);
  //as we did it on test-rectangle
  //we compare frame height & width, because we rotated on 90, so now width = height, and height = width
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.pos.x, frameAfter.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.pos.y, frameAfter.pos.y, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(getListTest)
{
  nurtdinov::Rectangle rectangle1({5, 4}, 3, 2);
  nurtdinov::Rectangle rectangle2({-10, -1}, 3, 3);
  nurtdinov::Rectangle rectangle3({0, -1}, 2, 2);
  nurtdinov::Circle circle1({0, 0}, 3);

  nurtdinov::CompositeShape compositeShape;
  compositeShape.add(std::make_shared<nurtdinov::Rectangle>(rectangle1));
  compositeShape.add(std::make_shared<nurtdinov::Circle>(circle1));
  compositeShape.add(std::make_shared<nurtdinov::Rectangle>(rectangle2));
  compositeShape.add(std::make_shared<nurtdinov::Rectangle>(rectangle3));

  const int sizeOfComp = compositeShape.getSize();
  nurtdinov::array_ptr shapes(std::make_unique<nurtdinov::ptr[]>(sizeOfComp));
  shapes = compositeShape.getList();

  nurtdinov::rectangle_t frameInArr = shapes[0].get()->getFrameRect();
  nurtdinov::rectangle_t frameShape = rectangle1.getFrameRect();
  double areaInArr = shapes[0].get()->getArea();
  double areaShape = rectangle1.getArea();
  BOOST_CHECK_CLOSE(frameInArr.width, frameShape.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameInArr.height, frameShape.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameInArr.pos.x, frameShape.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameInArr.pos.y, frameShape.pos.y, ERROR_VALUE);
  BOOST_CHECK_CLOSE(areaInArr, areaShape, ERROR_VALUE);

  frameInArr = shapes[1].get()->getFrameRect();
  frameShape = circle1.getFrameRect();
  areaInArr = shapes[1].get()->getArea();
  areaShape = circle1.getArea();
  BOOST_CHECK_CLOSE(frameInArr.width, frameShape.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameInArr.height, frameShape.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameInArr.pos.x, frameShape.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameInArr.pos.y, frameShape.pos.y, ERROR_VALUE);
  BOOST_CHECK_CLOSE(areaInArr, areaShape, ERROR_VALUE);

  frameInArr = shapes[2].get()->getFrameRect();
  frameShape = rectangle2.getFrameRect();
  areaInArr = shapes[2].get()->getArea();
  areaShape = rectangle2.getArea();
  BOOST_CHECK_CLOSE(frameInArr.width, frameShape.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameInArr.height, frameShape.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameInArr.pos.x, frameShape.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameInArr.pos.y, frameShape.pos.y, ERROR_VALUE);
  BOOST_CHECK_CLOSE(areaInArr, areaShape, ERROR_VALUE);

  frameInArr = shapes[3].get()->getFrameRect();
  frameShape = rectangle3.getFrameRect();
  areaInArr = shapes[3].get()->getArea();
  areaShape = rectangle3.getArea();
  BOOST_CHECK_CLOSE(frameInArr.width, frameShape.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameInArr.height, frameShape.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameInArr.pos.x, frameShape.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameInArr.pos.y, frameShape.pos.y, ERROR_VALUE);
  BOOST_CHECK_CLOSE(areaInArr, areaShape, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(comositeCopyConstructorTesting)
{
  nurtdinov::Circle circle({4.3, 2.1}, 2.0);
  nurtdinov::Rectangle rectangle({2.2, 3.5}, 4.0, 5.0);
  nurtdinov::CompositeShape firstComposite;
  firstComposite.add(std::make_shared<nurtdinov::Circle>(circle));
  firstComposite.add(std::make_shared<nurtdinov::Rectangle>(rectangle));
  const double areaFirst = firstComposite.getArea();
  const nurtdinov::rectangle_t frameFirst = firstComposite.getFrameRect();
  nurtdinov::CompositeShape secondComposite(firstComposite);
  const double areaSecond = secondComposite.getArea();
  const nurtdinov::rectangle_t frameSecond = secondComposite.getFrameRect();
  BOOST_CHECK_CLOSE(areaFirst, areaSecond, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.width, frameSecond.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.height, frameSecond.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.pos.x, frameSecond.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.pos.y, frameSecond.pos.y, ERROR_VALUE);
  BOOST_CHECK_EQUAL(firstComposite.getSize(), secondComposite.getSize());
}

BOOST_AUTO_TEST_CASE(compositeCopyOperatorTesting)
{
  nurtdinov::Circle circle({2.5, 6.1}, 2.0);
  nurtdinov::Rectangle rectangle({3.5, 7.7}, 4.0, 5.0);
  nurtdinov::CompositeShape firstComposite;
  firstComposite.add(std::make_shared<nurtdinov::Circle>(circle));
  firstComposite.add(std::make_shared<nurtdinov::Rectangle>(rectangle));
  const double areaFirst = firstComposite.getArea();
  const nurtdinov::rectangle_t frameFirst = firstComposite.getFrameRect();
  nurtdinov::CompositeShape secondComposite = firstComposite;
  const double areaSecond = secondComposite.getArea();
  const nurtdinov::rectangle_t frameSecond = secondComposite.getFrameRect();
  BOOST_CHECK_CLOSE(areaFirst, areaSecond, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.width, frameSecond.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.height, frameSecond.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.pos.x, frameSecond.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.pos.y, frameSecond.pos.y, ERROR_VALUE);
  BOOST_CHECK_EQUAL(firstComposite.getSize(), secondComposite.getSize());
}

BOOST_AUTO_TEST_CASE(frameRectChangeAfterScaleByMoreOne)
{
  nurtdinov::Circle circle({-4.5, 3.2}, 3.1);
  nurtdinov::Rectangle rectangle({8.5, -3.1}, 4.4, 3.3);
  nurtdinov::CompositeShape composite;
  composite.add(std::make_shared<nurtdinov::Circle>(circle));
  composite.add(std::make_shared<nurtdinov::Rectangle>(rectangle));
  const double scaleRatio = 3.5;
  const nurtdinov::rectangle_t frameBefore = composite.getFrameRect();
  composite.scale(scaleRatio);
  const nurtdinov::rectangle_t frameAfter = composite.getFrameRect();
  BOOST_CHECK_CLOSE(frameBefore.width * scaleRatio, frameAfter.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.height * scaleRatio, frameAfter.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.pos.x, frameAfter.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.pos.y, frameAfter.pos.y, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(frameRectChangeAfterScaleByLessOne)
{
  nurtdinov::Circle circle({4.2, 1.8}, 2.1);
  nurtdinov::Rectangle rectangle({-3.2, -3.1}, 8.5, 4.5);
  nurtdinov::CompositeShape composite;
  composite.add(std::make_shared<nurtdinov::Circle>(circle));
  composite.add(std::make_shared<nurtdinov::Rectangle>(rectangle));
  const double scaleRatio = 0.5;
  const nurtdinov::rectangle_t frameBefore = composite.getFrameRect();
  composite.scale(scaleRatio);
  const nurtdinov::rectangle_t frameAfter = composite.getFrameRect();
  BOOST_CHECK_CLOSE(frameBefore.width * scaleRatio, frameAfter.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.height * scaleRatio, frameAfter.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.pos.x, frameAfter.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.pos.y, frameAfter.pos.y, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterAddingShape)
{
  nurtdinov::Circle circle({0.5, 5.5}, 5.1);
  nurtdinov::CompositeShape composite;
  composite.add(std::make_shared<nurtdinov::Circle>(circle));
  const int countBefore = composite.getSize();
  const double areaBefore = composite.getArea();
  nurtdinov::Rectangle rectangle({9.2, 3.1}, 4.8, 3.9);
  composite.add(std::make_shared<nurtdinov::Rectangle>(rectangle));
  const double areaAfter = composite.getArea();
  BOOST_CHECK_CLOSE(areaBefore + rectangle.getArea(), areaAfter, ERROR_VALUE);
  BOOST_CHECK_EQUAL(countBefore + 1, composite.getSize());
}

BOOST_AUTO_TEST_CASE(areaChangeAfterDeletingShape)
{
  nurtdinov::Circle circle({-5.1, 5.1}, 3);
  nurtdinov::CompositeShape composite;
  composite.add(std::make_shared<nurtdinov::Circle>(circle));
  nurtdinov::Rectangle rectangle({5.3, 5.3}, 2, 1);
  composite.add(std::make_shared<nurtdinov::Rectangle>(rectangle));
  const int countBefore = composite.getSize();
  const double areaBefore = composite.getArea();
  composite.remove(0);
  const double areaAfter = composite.getArea();
  BOOST_CHECK_CLOSE(areaBefore - circle.getArea(), areaAfter, ERROR_VALUE);
  BOOST_CHECK_EQUAL(countBefore - 1, composite.getSize());
}

BOOST_AUTO_TEST_CASE(compositeExceptionThrow)
{
  nurtdinov::Circle circle({1.5, 2.1}, 2.2);
  nurtdinov::Rectangle rectangle({13.1, 12.4}, 4.5, 8.1);
  nurtdinov::CompositeShape compositeShape;
  compositeShape.add(std::make_shared<nurtdinov::Circle>(circle));
  compositeShape.add(std::make_shared<nurtdinov::Rectangle>(rectangle));
  BOOST_CHECK_THROW(compositeShape.scale(-1), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape[2], std::out_of_range); //we can get only 0 or 1
  BOOST_CHECK_THROW(compositeShape.remove(2), std::out_of_range); // here we can delete only 0 or 1
  BOOST_CHECK_THROW(compositeShape.remove(-1), std::out_of_range);
  BOOST_CHECK_THROW(compositeShape.add(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(emptyCompositeExceptionThrow)
{
  nurtdinov::CompositeShape emptyComposite;
  BOOST_CHECK_THROW(emptyComposite.remove(1), std::out_of_range);
  BOOST_CHECK_THROW(emptyComposite.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(emptyComposite.getArea(), std::logic_error);
  nurtdinov::Circle circle({5.3, 2.8}, 4.4);
  BOOST_CHECK_THROW(emptyComposite.move({4.4, 2.8}), std::logic_error);
  BOOST_CHECK_THROW(emptyComposite.move(2.7, 3.3), std::logic_error);
  BOOST_CHECK_THROW(emptyComposite.scale(2), std::logic_error);
}

BOOST_AUTO_TEST_CASE(testMoveConstructor)
{
  nurtdinov::Circle circle({15.4, 2.1}, 11.0);
  nurtdinov::Rectangle rectangle({3.2, 2.2}, 4.0, 5.0);
  nurtdinov::CompositeShape firstComposite;
  firstComposite.add(std::make_shared<nurtdinov::Rectangle>(rectangle));
  firstComposite.add(std::make_shared<nurtdinov::Circle>(circle));
  const nurtdinov::rectangle_t frameFirst = firstComposite.getFrameRect();
  const double areaFirst = firstComposite.getArea();
  const int countFirst = firstComposite.getSize();

  nurtdinov::CompositeShape moveComposite(std::move(firstComposite));
  const nurtdinov::rectangle_t moveFrameRect = moveComposite.getFrameRect();
  BOOST_CHECK_CLOSE(areaFirst, moveComposite.getArea(), ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.height, moveFrameRect.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.width, moveFrameRect.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.pos.x, moveFrameRect.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.pos.y, moveFrameRect.pos.y, ERROR_VALUE);
  BOOST_CHECK_EQUAL(countFirst, moveComposite.getSize());
  BOOST_CHECK_EQUAL(firstComposite.getSize(), 0);
  // we cannot check if area = 0 because we cannot get area of empty composite
  // so we can check if it throws exception when we use it
  BOOST_CHECK_THROW(firstComposite.getArea(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(testMoveOperator)
{
  nurtdinov::Circle circle1({5.4, 3.2}, 2.1);
  nurtdinov::Rectangle rectangle1({3.2, 4.1}, 2.0, 3.0);
  nurtdinov::CompositeShape firstComposite;
  firstComposite.add(std::make_shared<nurtdinov::Rectangle>(rectangle1));
  firstComposite.add(std::make_shared<nurtdinov::Circle>(circle1));
  const nurtdinov::rectangle_t frameFirst = firstComposite.getFrameRect();
  const double areaFirst = firstComposite.getArea();
  const int countFirst = firstComposite.getSize();

  nurtdinov::Circle circle2({1.3, 3.5}, 5.5);
  nurtdinov::CompositeShape moveComposite;
  moveComposite.add(std::make_shared<nurtdinov::Circle>(circle2));
  moveComposite = std::move(firstComposite);
  nurtdinov::rectangle_t moveFrameRect = moveComposite.getFrameRect();
  BOOST_CHECK_CLOSE(areaFirst, moveComposite.getArea(), ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.height, moveFrameRect.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.width, moveFrameRect.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.pos.x, moveFrameRect.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.pos.y, moveFrameRect.pos.y, ERROR_VALUE);
  BOOST_CHECK_EQUAL(countFirst, moveComposite.getSize());
  BOOST_CHECK_EQUAL(firstComposite.getSize(), 0);
  BOOST_CHECK_THROW(firstComposite.getArea(), std::logic_error);
  moveComposite = std::move(moveComposite);
  moveFrameRect = moveComposite.getFrameRect();
  BOOST_CHECK_CLOSE(areaFirst, moveComposite.getArea(), ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.height, moveFrameRect.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.width, moveFrameRect.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.pos.x, moveFrameRect.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameFirst.pos.y, moveFrameRect.pos.y, ERROR_VALUE);
  BOOST_CHECK_EQUAL(countFirst, moveComposite.getSize());
}

BOOST_AUTO_TEST_SUITE_END()
