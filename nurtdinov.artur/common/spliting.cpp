#include "spliting.hpp"
#include <cmath>

nurtdinov::Matrix<nurtdinov::Shape> nurtdinov::split(const nurtdinov::CompositeShape &compositeShape)
{
  nurtdinov::Matrix<nurtdinov::Shape> matrix;

  for (size_t i = 0; i < compositeShape.getSize(); i++)
  {
    size_t row = 0;
    size_t j = matrix.getRows();
    while (j-- > 0)
    {
      bool ifIntersect = false;
      for (size_t k = 0; k < matrix[j].getSize(); k++)
      {
        if (intersect(*compositeShape[i], *matrix[j][k]))
        {
          row = j + 1;
          ifIntersect = true;
          break;
        }
      }
      if (ifIntersect)
      {
        break;
      }
    }
    matrix.add(row, compositeShape[i]);
  }
  return matrix;
}

nurtdinov::Matrix<nurtdinov::Shape> nurtdinov::split(const nurtdinov::array_ptr &shapes, size_t size)
{
  nurtdinov::CompositeShape compositeShape;
  for (size_t i = 0; i < size; i++)
  {
    compositeShape.add(shapes[i]);
  }
  return split(compositeShape);
}

bool nurtdinov::intersect(const nurtdinov::Shape &shape1, const nurtdinov::Shape &shape2)
{
  const nurtdinov::rectangle_t frameFirst = shape1.getFrameRect();
  const nurtdinov::rectangle_t frameSecond = shape2.getFrameRect();

  //first we check if first figure below or upper second on more than sum of it's heights / 2
  if (std::abs(frameFirst.pos.y - frameSecond.pos.y) > (frameFirst.height + frameSecond.height) / 2)
  {
    return false;
  }
  //then we check same but for width
  return (std::abs(frameFirst.pos.x - frameSecond.pos.x) <= (frameFirst.width + frameSecond.width) / 2);
}
