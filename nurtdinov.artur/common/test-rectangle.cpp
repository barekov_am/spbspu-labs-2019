#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "rectangle.hpp"

const double ERROR_VALUE = 0.001;

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(rectangleParamsSavedAfterMovingToPoint)
{
  nurtdinov::Rectangle rectangle({3.3, 5.8}, 4.0, 5.0);
  const double areaBefore = rectangle.getArea();
  const nurtdinov::rectangle_t frameBefore = rectangle.getFrameRect();
  rectangle.move({5.5, -2.3});

  const double areaAfter = rectangle.getArea();
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ERROR_VALUE);
  const nurtdinov::rectangle_t frameAfter = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameAfter.height, frameBefore.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameAfter.width, frameBefore.width, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(rectangleParamsSavedAfterShiftMove)
{
  nurtdinov::Rectangle rectangle({4.8, 5.1}, 4.0, 5.0);
  const double areaBefore = rectangle.getArea();
  const nurtdinov::rectangle_t frameBefore = rectangle.getFrameRect();
  rectangle.move(2.2, 3.9);

  const double areaAfter = rectangle.getArea();
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ERROR_VALUE);
  const nurtdinov::rectangle_t frameAfter = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameAfter.height, frameBefore.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameAfter.width, frameBefore.width, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(rectangleScaleTest)
{
  nurtdinov::Rectangle rectangle({2.5, 4.1}, 3.0, 4.0);
  const double areaBefore = rectangle.getArea();
  const nurtdinov::rectangle_t frameBefore = rectangle.getFrameRect();
  const double ratio = 4.0;

  rectangle.scale(ratio);
  BOOST_CHECK_CLOSE(rectangle.getArea(), areaBefore * ratio * ratio, ERROR_VALUE);
  const nurtdinov::rectangle_t frameAfter = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameAfter.height, frameBefore.height * ratio, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameAfter.width, frameBefore.width * ratio, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(paramsChangeAfterRectangleRotation)
{
  nurtdinov::Rectangle rectangle({1, 1}, 2, 5);
  const nurtdinov::rectangle_t frameBefore = rectangle.getFrameRect();
  const double areaBefore = rectangle.getArea();
  rectangle.rotate(90);
  const double areaAfter = rectangle.getArea();
  const nurtdinov::rectangle_t frameAfter = rectangle.getFrameRect();

  //we rotate on 90, so it's width will be equal to initial height and so with it's height
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.pos.x, frameAfter.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.pos.y, frameAfter.pos.y, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(rectangleThrowInvalidScale)
{
  nurtdinov::Rectangle rectangle({5.1, 4.2}, 3.0, 4.0);
  BOOST_CHECK_THROW(rectangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleThrowInvalidWidth)
{
  BOOST_CHECK_THROW(nurtdinov::Rectangle({2.2, 4.4}, -3.0, 4.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleThrowInvalidHeight)
{
  BOOST_CHECK_THROW(nurtdinov::Rectangle({2.2, 4.4}, 3.0, -4.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
