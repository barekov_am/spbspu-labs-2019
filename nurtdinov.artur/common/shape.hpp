#ifndef A1_SHAPE_HPP
#define A1_SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace nurtdinov
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t &point) = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void scale(double ratio) = 0;
    virtual void rotate(double angle) = 0;
  };
  using ptr = std::shared_ptr<Shape>;
  using array_ptr = std::unique_ptr<ptr[]>;
}

#endif //A1_SHAPE_HPP
