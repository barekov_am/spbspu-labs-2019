#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "circle.hpp"

const double ERROR_VALUE = 0.001;

BOOST_AUTO_TEST_SUITE(circleTest)

BOOST_AUTO_TEST_CASE(circleParamsSavedAfterMovingToPoint)
{
  nurtdinov::Circle circle({4.4, 2.5}, 5.0);
  const double areaBefore = circle.getArea();
  const nurtdinov::rectangle_t frameBefore = circle.getFrameRect();
  circle.move({2.2, 3.1});
  const double areaAfter = circle.getArea();
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ERROR_VALUE);
  const nurtdinov::rectangle_t frameAfter = circle.getFrameRect();
  BOOST_CHECK_CLOSE(frameAfter.height, frameBefore.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameAfter.width, frameBefore.width, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(circleParamsSavedAfterShiftMove)
{
  nurtdinov::Circle circle({4.4, 2.5}, 5.0);
  const double areaBefore = circle.getArea();
  const nurtdinov::rectangle_t frameBefore = circle.getFrameRect();
  circle.move(4.6, 2.5);
  const double areaAfter = circle.getArea();
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ERROR_VALUE);
  const nurtdinov::rectangle_t frameAfter = circle.getFrameRect();
  BOOST_CHECK_CLOSE(frameAfter.height, frameBefore.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameAfter.width, frameBefore.width, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(circleScaleTest)
{
  nurtdinov::Circle circle({2.5, 4.1}, 3.0);
  const double areaBefore = circle.getArea();
  const double ratio = 2.0;
  circle.scale(ratio);
  BOOST_CHECK_CLOSE(circle.getArea(), areaBefore * ratio * ratio, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(circleParamsSavedAfterRotation)
{
  nurtdinov::Circle circle({1, 1}, 2);
  const nurtdinov::rectangle_t frameBefore = circle.getFrameRect();
  const double areaBefore = circle.getArea();
  circle.rotate(90);
  const double areaAfter = circle.getArea();
  const nurtdinov::rectangle_t frameAfter = circle.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.height, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.width, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.pos.x, frameAfter.pos.x, ERROR_VALUE);
  BOOST_CHECK_CLOSE(frameBefore.pos.y, frameAfter.pos.y, ERROR_VALUE);
}

BOOST_AUTO_TEST_CASE(circleThrowInvalidScale)
{
  nurtdinov::Circle circle({2.2, 4.8}, 5.0);
  BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circleTestThrowException)
{
  BOOST_CHECK_THROW(nurtdinov::Circle({2.2, 4.4}, -1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
