#include "functor.hpp"

Functor::Functor():
        max(0),
        min(0),
        mean(0.0),
        positive(0),
        negative(0),
        oddSum(0),
        evenSum(0),
        firstLastEqual(false),
        totalCount(0),
        firstElement(0)
{
}

void Functor::operator()(long long int element)
{
  totalCount++;
  if(totalCount == 1)
  {
    firstElement = element;
    max = element;
    min = element;
  }
  if(element > max)
  {
    max = element;
  }
  if(element < min)
  {
    min = element;
  }
  if(element > 0)
  {
    positive++;
  }
  if(element < 0)
  {
    negative++;
  }
  if((element % 2) != 0)
  {
    oddSum += element;
  }
  else
  {
    evenSum += element;
  }
  firstLastEqual = (element == firstElement);
}

void Functor::printData(std::ostream &string)
{
  if(totalCount == 0)
  {
    string << "No Data" << std::endl;
    return;
  }

  mean = static_cast<double>(evenSum + oddSum) / totalCount;

  string << "Max: " << max << std::endl;
  string << "Min: " << min << std::endl;
  string << "Mean: " << mean << std::endl;
  string << "Positive: " << positive << std::endl;
  string << "Negative: " << negative << std::endl;
  string << "Odd Sum: " << oddSum << std::endl;
  string << "Even Sum: " << evenSum << std::endl;

  string << "First/Last Equal: ";
  if(firstLastEqual)
  {
    string << "yes" << std::endl;
  }
  else
  {
    string << "no" << std::endl;
  }
}
