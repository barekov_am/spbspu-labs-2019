#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <iostream>


class Functor
{
public:
  Functor();
  void operator()(long long int element);
  void printData(std::ostream & stream);
private:
  long long int max;
  long long int min;
  double mean;
  int positive;
  int negative;
  long long int oddSum;
  long long int evenSum;
  bool firstLastEqual;
  long long int totalCount;
  long long int firstElement;
};

#endif
