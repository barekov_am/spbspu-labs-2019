#include <iostream>
#include <cassert>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"


void printFigureInfo(const protasov::Shape &figure)
{

  protasov::rectangle_t frameRect = figure.getFrameRect();
  std::cout << "Center of figure: (" << frameRect.pos.x << ", " << frameRect.pos.y << ")\n";
  std::cout << "Figure width: " << frameRect.width << "\n";
  std::cout << "Figure height: " << frameRect.height << "\n";
  std::cout << "Figure area: " << figure.getArea() << "\n";
}

int main()
{
  //Working with rectangle
  protasov::Rectangle rectangle({6, 8}, 5, 2);
  std::cout << "Rectangle information after initialization:" << "\n";
  printFigureInfo(rectangle);
  rectangle.move({1, 2});
  rectangle.move(3, 9);
  std::cout << "Rectangle information after moving:" << "\n";
  printFigureInfo(rectangle);
  rectangle.scale(2.5);
  std::cout << "Rectangle information after scaling:" << "\n";
  printFigureInfo(rectangle);

  //Working with circle
  protasov::Circle circle({7, 5}, 3);
  std::cout << "Circle information after initialization:" << "\n";
  printFigureInfo(circle);
  circle.move({5, 5});
  circle.move(4, 4);
  std::cout << "Circle information after moving:" << "\n";
  printFigureInfo(circle);
  circle.scale(1.5);
  std::cout << "Circle information after scaling:" << "\n";
  printFigureInfo(circle);

  //Working with triangle
  protasov::Triangle triangle({6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8});
  std::cout << "Triangle information after initialization:" << "\n";
  printFigureInfo(triangle);
  triangle.move({1, 2});
  triangle.move(3, 9);
  std::cout << "Triangle information after moving:" << "\n";
  printFigureInfo(triangle);
  triangle.scale(2.5);
  std::cout << "Triangle information after scaling:" << "\n";
  printFigureInfo(triangle);

  //Working with polygon
  protasov::point_t poly[] = {{6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8}, {3, 6}};
  size_t count = 4;
  protasov::Polygon polygon(count, poly);
  std::cout << "Polygon information after initialization:" << "\n";
  printFigureInfo(polygon);
  polygon.move({1, 2});
  polygon.rotate(40);
  polygon.move(3, 9);
  std::cout << "Polygon information after moving:" << "\n";
  printFigureInfo(polygon);
  polygon.scale(2.5);
  std::cout << "Polygon information after scaling:" << "\n";
  printFigureInfo(polygon);

  protasov::CompositeShape shapes1;
  shapes1.addShape(std::make_shared<protasov::Rectangle>(rectangle));
  shapes1.addShape(std::make_shared<protasov::Circle>(circle));
  std::cout << "Area of composite shape after adding new shape: " << shapes1.getArea() << '\n';
  std::cout << "Center of composite shape: \n";
  std::cout << "x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';
  shapes1.move(7.0,7.0);
  std::cout << "Center of composite shape after moving: \n";
  std::cout << "x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';
  shapes1.scale(3.0);
  std::cout << "Center of composite shape after scaling: \n";
  std::cout << "x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';
  shapes1.addShape(std::make_shared<protasov::Triangle>(triangle));
  std::cout << "Area of composite shape: " << shapes1.getArea() << '\n';
  shapes1.addShape(std::make_shared<protasov::Polygon>(polygon));
  shapes1.deleteShape(1);
  std::cout << "Area of composite shape after deleting second shape: " << shapes1.getArea() << '\n';
  std::cout << "Center of composite shape: \n";
  std::cout << "x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';
  shapes1.move({7.0,7.0});
  std::cout << "Center of composite shape after moving: \n";
  std::cout << "x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';
  shapes1.scale(3.0);
  std::cout << "Center of composite shape after scaling: \n";
  std::cout << "x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';

  return 0;
}
