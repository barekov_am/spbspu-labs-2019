#include <iostream>
#include <algorithm>

#include "shape.hpp"
#include "shapesFunctions.hpp"

void task2()
{
  std::vector<Shape> figures = readShapes();
  std::size_t vertices = 0;
  std::size_t triangles = 0;
  std::size_t squares = 0;
  std::size_t rectangles = 0;
  for (auto figure :  figures)
  {
    vertices += figure.size();
    if (figure.size() == 3)
    {
      ++triangles;
    }
    else if (figure.size() == 4)
    {
      if (isRectangle(figure))
      {
        ++rectangles;
      }
      if (isSquare(figure))
      {
        ++squares;
      }
    }
  }
  figures.erase(std::remove_if(figures.begin(), figures.end(),[](const Shape & shape) { return shape.size() == 5; }), figures.end());
  std::cout << "Vertices: " << vertices << '\n';
  std::cout << "Triangles: " << triangles << '\n';
  std::cout << "Squares: " << squares << '\n';
  std::cout << "Rectangles: " << rectangles << '\n';
  Shape points;
  std::for_each(figures.begin(), figures.end(), [&points](const Shape& shape)
  {
    points.push_back(shape[0]);
  });
  std::sort(figures.begin(), figures.end(), comparator);
  std::cout << "Points: ";
  for (auto point : points)
  {
    std::cout << "(" << point.x << ";" << point.y <<") ";
  }
  std::cout << "\n";
  std::cout << "Shapes: \n";
  for (auto figure : figures)
  {
    std::cout << figure.size();
    for (auto point : figure) {
      std::cout << " (" << point.x << ";" << point.y << ") ";
    }
    std::cout << '\n';
  }
}
