#include <sstream>

#include "shape.hpp"
#include "shapesFunctions.hpp"

std::vector<Shape> readShapes()
{
  std::vector<Shape> figures;
  std::string inputString;
  while (std::getline(std::cin, inputString))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading caused an error");
    }
    std::stringstream ss(inputString);
    std::string number;
    ss >> number;
    if (number == "")
    {
      continue;
    }
    int num = std::stoi(number);
    Shape shape;
    size_t lBracket, separator, rBracket;
    for (int i = 0; i < num; ++i)
    {
      lBracket = inputString.find_first_of('(');

      if (lBracket == std::string::npos)
      {
        throw std::invalid_argument("Invalid argument");
      }
      inputString.erase(0, lBracket + 1);
      separator = inputString.find_first_of(';');
      if (separator == std::string::npos)
      {
        throw std::invalid_argument("Invalid argument");
      }
      int x = std::stoi(inputString.substr(0, separator));
      inputString.erase(0, separator + 1);
      rBracket = inputString.find_first_of(')');
      if (rBracket == std::string::npos)
      {
        throw std::invalid_argument("Invalid argument");
      }
      int y = std::stoi(inputString.substr(0, rBracket));
      inputString.erase(0, rBracket + 1);
      shape.push_back({x, y});
    }
    figures.push_back(shape);
    inputString.erase(inputString.find_last_not_of(" \t") + 1);
    if (inputString != "")
    {
      throw std::invalid_argument("Invalid argument");
    }
  }
  return figures;
}

bool isRectangle(const Shape & shape)
{
  if (shape.size() == 4)
  {
    int diag1 = (shape[0].x - shape[2].x)*(shape[0].x - shape[2].x) + (shape[0].y - shape[2].y)*(shape[0].y - shape[2].y);
    int diag2 = (shape[1].x - shape[3].x)*(shape[1].x - shape[3].x) + (shape[1].y - shape[3].y)*(shape[1].y - shape[3].y);
    if (diag1 == diag2)
    {
      return true;
    }
  }
  return false;
}

bool isSquare(const Shape & shape)
{
  if (isRectangle(shape))
  {
    int side1 = (shape[0].x - shape[1].x)*(shape[0].x - shape[1].x) + (shape[0].y - shape[1].y)*(shape[0].y - shape[1].y);
    int side2 = (shape[1].x - shape[2].x)*(shape[1].x - shape[2].x) + (shape[1].y - shape[2].y)*(shape[1].y - shape[2].y);
    if (side1 == side2)
    {
      return true;
    }
  }
  return false;
}

bool comparator(const Shape & lhs, const Shape & rhs)
{
  if (lhs.size() < rhs.size())
  {
    return true;
  }
  if ((lhs.size() == 4) && (rhs.size() == 4))
  {
    if (isSquare(lhs))
    {
      if (isSquare(rhs))
      {
        return lhs[0].x < rhs[0].x;
      }
      return true;
    }
  }
  return false;
}
