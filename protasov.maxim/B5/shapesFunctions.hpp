#ifndef SHAPESFUNCTIONS_HPP
#define SHAPESFUNCTIONS_HPP

#include <iostream>
#include "shape.hpp"

std::vector<Shape> readShapes();
bool isRectangle(const Shape &shape);
bool isSquare(const Shape &shape);
bool comparator(const Shape & lhs, const Shape & rhs);

#endif
