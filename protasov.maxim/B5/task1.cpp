#include <iostream>
#include <set>
#include <sstream>
#include <iterator>
void task1()
{
  std::string inputString;
  std::set<std::string> words;
  while (std::getline(std::cin, inputString))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading caused an error");
    }
    std::stringstream ss(inputString);
    std::string word;
    while (ss >> word)
    {
      words.insert(word);
    }
  }
  for (auto word : words)
  {
    std::cout << word << "\n";
  }
}
