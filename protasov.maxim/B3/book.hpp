#ifndef BOOK_HPP
#define BOOK_HPP

#include <list>
#include <string>

class Book
{
  public:
    struct note
    {
      std::string phoneNumber;
      std::string name;
    };
    using iterator = std::list<note>::iterator;


    void printCurrent(iterator it);
    iterator next(iterator it);
    iterator prev(iterator it);
    void insertBefore(iterator it, const note& newNote);
    void insertAfter(iterator it, const note& newNote);
    iterator changeNote(iterator it, const note& newNote);
    void pushBack(const note& newNote);
    iterator moveForward(iterator it, int n);
    iterator moveBackward(iterator it, int n);
    iterator begin();
    iterator end();
    void removeNote(iterator it);
    bool isEmpty();
  //iterator moveTo(iterator it, int num);


  private:
    std::list<note> notes;

};

#endif
