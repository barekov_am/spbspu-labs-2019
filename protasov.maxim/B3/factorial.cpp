#include "factorial.hpp"

Factorial::FactorialIterator::FactorialIterator() :
    FactorialIterator(1)
{

}

Factorial::FactorialIterator::FactorialIterator(std::size_t number) :
    index(number)
{

}

bool Factorial::FactorialIterator::operator ==(const Factorial::FactorialIterator &rhs) const
{
  return (index == rhs.index);
}

bool Factorial::FactorialIterator::operator !=(const Factorial::FactorialIterator &rhs) const
{
  return (index != rhs.index);
}

std::size_t Factorial::FactorialIterator::calculate(std::size_t number) const
{
  std::size_t result = 1;
  for (std::size_t i = 2; i <= number; ++i)
  {
    result *= i;
  }
  return result;
}

std::size_t Factorial::FactorialIterator::operator *()
{
  return calculate(index);
}

Factorial::FactorialIterator &Factorial::FactorialIterator::operator ++()
{
  if(index < 11)
  {
    ++index;
  }
  return *this;
}

const Factorial::FactorialIterator Factorial::FactorialIterator::operator ++(int)
{
  Factorial::FactorialIterator tmp = *this;
  ++(*this);
  return tmp;
}

Factorial::FactorialIterator &Factorial::FactorialIterator::operator --()
{
  if(index > 1)
  {
    --index;
  }
  return *this;
}

const Factorial::FactorialIterator Factorial::FactorialIterator::operator --(int)
{
  Factorial::FactorialIterator tmp = *this;
  --(*this);
  return tmp;
}

Factorial::FactorialIterator Factorial::begin()
{
  return FactorialIterator(1);
}

Factorial::FactorialIterator Factorial::end()
{
  return FactorialIterator(11);
}
