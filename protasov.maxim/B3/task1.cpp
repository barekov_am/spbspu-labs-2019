#include <iostream>
#include <sstream>
#include <regex>

#include "tasks.hpp"
#include "bookInterface.hpp"
#include "book.hpp"

bool parseName(std::string &name) {
  if (name[0] == '\"' && name[name.length() - 1] == '\"')
  {
    name.erase(0, 1);
    size_t i = 0;
    while (i < name.length())
    {
      if (i + 1 < name.length() && name[i] == '\\' && name[i+1] == '\"')
      {
        name.erase(i, 1);
      }
      ++i;
    }
    name.erase(name.length() - 1, 1);
    return true;
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
    return false;
  }
}

bool checkMarkName(std::string name)
{
  std::regex reg("[-*0-9a-zA-z]+");
  if (std::regex_match(name, reg))
  {
    return true;
  }
  return false;
}

void task1()
{
  BookInterface myBookInterface;
  std::string inputString;
  while (getline(std::cin, inputString))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Data reading failed");
    }

    int pos = inputString.find(' ');
    std::string word = inputString.substr(0, pos);
    inputString.erase(0, pos + 1);

    if (word == "add")
    {
      pos = inputString.find(' ');
      word = inputString.substr(0, pos);
      inputString.erase(0, pos + 1);
      if(parseName(inputString))
      {
        myBookInterface.add({word, inputString});
      }
    }
    else if (word == "show")
    {
      if (checkMarkName(inputString))
      {
        myBookInterface.show(inputString);
      }
      else
      {
        std::cout << "<INVALID COMMAND>\n";
      }
    }
    else if (word == "store")
    {
      pos = inputString.find(' ');
      word = inputString.substr(0, pos);
      inputString.erase(0, pos + 1);
      if (checkMarkName(word) && checkMarkName(inputString))
      {
        myBookInterface.store(word, inputString);
      }
      else
      {
        std::cout << "<INVALID COMMAND>\n";
      }
    }
    else if (word == "insert")
    {
      pos = inputString.find(' ');
      word = inputString.substr(0, pos);
      inputString.erase(0, pos + 1);
      if (word == "before" || word == "after")
      {
        pos = inputString.find(' ');
        std::string markName = inputString.substr(0, pos);
        inputString.erase(0, pos + 1);
        if (checkMarkName(markName))
        {
          pos = inputString.find(' ');
          std::string number = inputString.substr(0, pos);
          inputString.erase(0, pos + 1);
          if (parseName(inputString))
          {
            myBookInterface.insert(word, markName, {number, inputString});
          }
        }
        else
        {
          std::cout << "<INVALID COMMAND>\n";
        }
      }
      else
      {
        std::cout << "<INVALID COMMAND>\n";
      }
    }
    else if (word == "delete")
    {
      if (checkMarkName(inputString))
      {
        myBookInterface.deleteMark(inputString);
      }
      else
      {
        std::cout << "<INVALID COMMAND>\n";
      }
    }
    else if (word == "move")
    {
      pos = inputString.find(' ');
      std::string markName = inputString.substr(0, pos);
      inputString.erase(0, pos + 1);
      if (std::regex_match(inputString, std::regex("[-+]?[0-9]+")))
      {
        myBookInterface.move(markName, std::stoi(inputString));
      }
      else
      {
        myBookInterface.move(markName, inputString);
      }
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
