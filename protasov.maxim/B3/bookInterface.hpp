#ifndef BOOKINTERFACE_HPP
#define BOOKINTERFACE_HPP

#include <map>
#include "book.hpp"

class BookInterface
{
  public:
    using bookMarks = std::map<std::string, Book::iterator>;
    BookInterface();
    void add(const Book::note& newNote);
    void store(const std::string &markName, const std::string &newMarkName);
    void insert(const std::string &direction, const std::string &markName, const Book::note& newNote);
    void deleteMark(const std::string &markName);
    void show(const std::string &markName);
    void move(const std::string &markName, int steps);
    void move(const std::string &markName, const std::string &position);

  private:
    bookMarks marks;
    Book book;
};

#endif
