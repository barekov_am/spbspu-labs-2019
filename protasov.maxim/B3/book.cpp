#include <iterator>
#include <iostream>
#include "book.hpp"

void Book::printCurrent(Book::iterator it)
{
  std::cout << it->phoneNumber << " " << it->name << "\n";
}

Book::iterator Book::next(Book::iterator it)
{
  if (std::next(it) == notes.end())
  {
    return it;
  }
  return std::next(it);
}

Book::iterator Book::prev(Book::iterator it)
{
  if (std::prev(it) == notes.begin())
  {
    return it;
  }
  return std::prev(it);
}

void Book::insertBefore(Book::iterator it, const Book::note &newNote)
{
  notes.insert(it, newNote);
}

void Book::insertAfter(Book::iterator it, const Book::note &newNote)
{
  if (std::next(it) == notes.end())
  {
    pushBack(newNote);
  }
  else
  {
    notes.insert(std::next(it), newNote);
  }
}

Book::iterator Book::changeNote(Book::iterator it, const Book::note &newNote)
{
  return notes.insert(notes.erase(it), newNote);
}

void Book::pushBack(const Book::note &newNote)
{
  notes.push_back(newNote);
}

Book::iterator Book::moveForward(iterator it, int n)
{
  while (std::next(it) != notes.end() && n > 0)
  {
    it = std::next(it);
    --n;
  }
  return it;
}

Book::iterator Book::moveBackward(iterator it, int n)
{
  while (it != notes.begin() && n > 0)
  {
    it= std::prev(it);
    --n;
  }
  return it;
}

Book::iterator Book::begin()
{
  return notes.begin();
}

Book::iterator Book::end()
{
  return notes.end();
}

void Book::removeNote(iterator it)
{
  notes.erase(it);
}

bool Book::isEmpty()
{
  return notes.empty();
}
