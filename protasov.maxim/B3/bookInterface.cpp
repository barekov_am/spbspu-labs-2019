#include <iostream>
#include "bookInterface.hpp"

BookInterface::BookInterface()
{
  marks["current"] = book.begin();
}

void BookInterface::add(const Book::note &newNote)
{
  if (book.isEmpty())
  {
    book.pushBack(newNote);
    marks["current"] = book.begin();
  }
  else
  {
    book.pushBack(newNote);
  }
}

void BookInterface::store(const std::string &markName, const std::string &newMarkName)
{
  if (marks.find(markName) != marks.end())
  {
    marks.insert({newMarkName, marks.find(markName)->second});
  }
  else
  {
    std::cout << "<INVALID BOOKMARK>\n";
  }
}

void BookInterface::insert(const std::string &direction, const std::string &markName, const Book::note &newNote)
{
  auto it = marks.find(markName);
  if (it != marks.end())
  {
    if (book.isEmpty())
    {
      add(newNote);
    }
    else if (direction == "before")
    {
      book.insertBefore(it->second, newNote);
    }
    else
    {
      book.insertAfter(it->second, newNote);
    }
  }
  else
  {
    std::cout << "<INVALID BOOKMARK>\n";
  }
}

void BookInterface::deleteMark(const std::string &markName)
{
  if (!book.isEmpty())
  {
    auto it = marks.find(markName);
    if (it != marks.end())
    {
      if (it->second != book.end())
      {
        auto delNote = marks[markName];
        auto repNote = (std::next(it->second) == book.end()) ? std::prev(it->second) : std::next(it->second);
        book.removeNote(it->second);
        for (auto & mark : marks)
        {
          if (mark.second == delNote)
          {
            mark.second = repNote;
          }
        }
      }
    }
    else
    {
      std::cout << "<INVALID BOOKMARK>\n";
    }
  }
}

void BookInterface::show(const std::string &markName)
{
  auto it = marks.find(markName);
  if (it != marks.end())
  {
    if (!book.isEmpty())
    {
      book.printCurrent(it->second);
    }
    else
    {
        std::cout << "<EMPTY>\n";
    }
  }
  else
  {
    std::cout << "<INVALID BOOKMARK>\n";
  }
}

void BookInterface::move(const std::string &markName, int steps)
{
  auto it = marks.find(markName);
  if (it != marks.end())
  {
    if (steps < 0)
    {
      marks[markName] = book.moveBackward(it->second, abs(steps));
    }
    else
    {
      marks[markName] = book.moveForward(it->second, steps);
    }
  }
  else
  {
    std::cout << "<INVALID BOOKMARK>\n";
  }
}

void BookInterface::move(const std::string &markName, const std::string &position)
{
  auto it = marks.find(markName);
  if (it != marks.end())
  {
    if (position == "first")
    {
      it->second = book.begin();
    }
    else if (position == "last")
    {
      it->second = std::prev(book.end());
    }
    else
    {
      std::cout << "<INVALID STEP>\n";
    }
  }
  else
  {
    std::cout << "<INVALID BOOKMARK>\n";
  }
}
