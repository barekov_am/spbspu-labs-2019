#ifndef FACTORIAL_HPP
#define FACTORIAL_HPP

#include <iterator>

class Factorial
{
public:
  class FactorialIterator : public std::iterator<std::bidirectional_iterator_tag, std::size_t >
  {
  public:
    FactorialIterator();
    FactorialIterator(size_t);
    bool operator ==(const FactorialIterator&) const;
    bool operator !=(const FactorialIterator&) const;
    size_t operator *();
    FactorialIterator& operator ++();
    const FactorialIterator operator ++(int);
    FactorialIterator& operator --();
    const FactorialIterator operator --(int);
    std::size_t calculate(std::size_t) const;

  private:
    std::size_t index;
  };
  Factorial() = default;
  FactorialIterator  begin();
  FactorialIterator  end();
};

#endif
