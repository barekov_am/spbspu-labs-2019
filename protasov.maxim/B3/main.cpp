#include <iostream>
#include "tasks.hpp"

int main(int argc, char* argv[])
{
  if (argc != 2)
  {
    std::cerr << "Invalid arguments. You should input only task number.\n";
    return 1;
  }
  try
  {
    switch (atoi(argv[1]))
    {
      case 1:
        task1();
        break;
      case 2:
        task2();
        break;
      default:
        throw std::invalid_argument("Invalid task number. You should input \"1\" or \"2\".\n");
    }
  }
  catch (std::exception& e)
  {
    std::cerr << e.what();
    return 2;
  }
  return 0;
}
