#include <iostream>
#include <cassert>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void printFigureInfo(const protasov::Shape *figure)
{
  assert(figure != nullptr);
  protasov::rectangle_t frameRect = figure->getFrameRect();
  std::cout << "Center of figure: (" << frameRect.pos.x << ", " << frameRect.pos.y << ")\n";
  std::cout << "Figure width: " << frameRect.width << "\n";
  std::cout << "Figure height: " << frameRect.height << "\n";
  std::cout << "Figure area: " << figure->getArea() << "\n";
}

int main()
{
  //Working with rectangle
  protasov::Rectangle rectangle({6, 8}, 5, 2);
  protasov::Shape *figure = &rectangle;
  std::cout << "Rectangle information after initialization:" << "\n";
  printFigureInfo(figure);
  rectangle.move({1, 2});
  rectangle.move(3, 9);
  std::cout << "Rectangle information after moving:" << "\n";
  printFigureInfo(figure);
  rectangle.scale(2.5);
  std::cout << "Rectangle information after scaling:" << "\n";
  printFigureInfo(figure);

  //Working with circle
  protasov::Circle circle({7, 5}, 3);
  figure = &circle;
  std::cout << "Circle information after initialization:" << "\n";
  printFigureInfo(figure);
  circle.move({5, 5});
  circle.move(4, 4);
  std::cout << "Circle information after moving:" << "\n";
  printFigureInfo(figure);
  circle.scale(1.5);
  std::cout << "Circle information after scaling:" << "\n";
  printFigureInfo(figure);

  //Working with triangle
  protasov::Triangle triangle({6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8});
  figure = &triangle;
  std::cout << "Triangle information after initialization:" << "\n";
  printFigureInfo(figure);
  triangle.move({1, 2});
  triangle.move(3, 9);
  std::cout << "Triangle information after moving:" << "\n";
  printFigureInfo(figure);
  triangle.scale(2.5);
  std::cout << "Triangle information after scaling:" << "\n";
  printFigureInfo(figure);

  //Working with polygon
  protasov::point_t poly[] = {{6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8}, {3, 6}};
  size_t count = 4;
  protasov::Polygon polygon(count, poly);
  figure = &polygon;
  std::cout << "Polygon information after initialization:" << "\n";
  printFigureInfo(figure);
  polygon.move({1, 2});
  polygon.move(3, 9);
  std::cout << "Polygon information after moving:" << "\n";
  printFigureInfo(figure);
  polygon.scale(2.5);
  std::cout << "Polygon information after scaling:" << "\n";
  printFigureInfo(figure);

  return 0;
}
