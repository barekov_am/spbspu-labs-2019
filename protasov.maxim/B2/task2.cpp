#include <iostream>
#include <list>
#include "tasks.hpp"

void task2()
{
  std::list<int> inputList;
  int inputValue;
  while (std::cin >> inputValue)
  {
    if (inputValue > 20 || inputValue < 1)
    {
      throw std::invalid_argument("Value must be in range from 1 to 20");
    }
    else
    {
      inputList.push_back(inputValue);
    }
    if (inputList.size() > 20)
    {
      throw std::invalid_argument("There are too many values inputted");
    }
  }
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Input error");
  }
  if (inputList.empty())
  {
    return;
  }
  std::list<int>::iterator iLeft = inputList.begin();
  std::list<int>::iterator iRight = inputList.end();
  while (iLeft != iRight)
  {
    std::cout << *iLeft;
    ++iLeft;
    if (iLeft != iRight)
    {
      --iRight;
      std::cout  << " " << *iRight;
      if (iLeft != iRight)
      {
        std::cout << " ";
      }
    }
  }
  std::cout <<"\n";
}
