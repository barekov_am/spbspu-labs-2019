#include <iostream>
#include <sstream>
#include <string>
#include "tasks.hpp"
#include "PriorityQueue.hpp"


void task1()
{
  QueueWithPriority<std::string> priorityQueue;
  std::string inputString;
  while (getline(std::cin, inputString))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Data reading failed");
    }
    std::stringstream ss(inputString);
    std::string word;
    ss>>word;
    if (word == "get")
    {
      if (priorityQueue.isQueueEmpty())
      {
        std::cout << "<EMPTY>\n";
      }
      else
      {
        std::cout << priorityQueue.GetElementFromQueue() << "\n";
      }
    }
    else if (word == "accelerate")
    {
      if (priorityQueue.isQueueEmpty())
      {
        std::cout << "<EMPTY>\n";
      }
      priorityQueue.Accelerate();
    }
    else if (word == "add")
    {
      word.clear();
      ss >> std::ws >> word >> std::ws;
      std::string value;
      getline(ss, value);
      if (!value.empty())
      {
        if (word == "high")
        {
          priorityQueue.putElementToQueue(value, priorityQueue.HIGH);
        }
        else if (word == "normal")
        {
          priorityQueue.putElementToQueue(value, priorityQueue.NORMAL);
        }
        else if (word == "low")
        {
          priorityQueue.putElementToQueue(value, priorityQueue.LOW);
        }
        else
        {
          std::cout << "<INVALID COMMAND>\n";
        }
      }
      else
      {
        std::cout << "<INVALID COMMAND>\n";
      }
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
