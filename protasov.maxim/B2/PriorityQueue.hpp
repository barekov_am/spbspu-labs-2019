#ifndef PRIORITYQUEUE_HPP
#define PRIORITYQUEUE_HPP

#include <list>

template <typename T>
class QueueWithPriority
{
public:
  typedef enum
  {
    LOW,
    NORMAL,
    HIGH
  } ElementPriority;

  void putElementToQueue(const T & element, ElementPriority priority);
  T GetElementFromQueue();
  void Accelerate();
  bool isQueueEmpty();

private:
  std::list<T> highPriorityElements;
  std::list<T> normalPriorityElements;
  std::list<T> lowPriorityElements;
};

template <typename T>
void QueueWithPriority<T>::putElementToQueue(const T &element, QueueWithPriority::ElementPriority priority)
{
  switch(priority)
  {
    case HIGH:
      highPriorityElements.push_back(element);
      break;
    case NORMAL:
      normalPriorityElements.push_back(element);
      break;
    case LOW:
      lowPriorityElements.push_back(element);
      break;
  }
}

template <typename T>
T QueueWithPriority<T>::GetElementFromQueue()
{
  if (!highPriorityElements.empty())
  {
    T element = highPriorityElements.front();
    highPriorityElements.pop_front();
    return element;
  }
  else if (!normalPriorityElements.empty())
  {
    T element = normalPriorityElements.front();
    normalPriorityElements.pop_front();
    return element;
  }
  else
  {
    T element = lowPriorityElements.front();
    lowPriorityElements.pop_front();
    return element;
  }
}

template <typename T>
void QueueWithPriority<T>::Accelerate()
{
  highPriorityElements.splice(highPriorityElements.end(), lowPriorityElements);
}

template <typename T>
bool QueueWithPriority<T>::isQueueEmpty()
{
  return highPriorityElements.empty() && normalPriorityElements.empty() && lowPriorityElements.empty();
}
#endif
