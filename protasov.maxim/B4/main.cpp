#include <algorithm>
#include <iostream>
#include "dataStruct.hpp"
#include "functions.hpp"

int main()
{
  try
  {
    std::vector<DataStruct> myVector = readVector();
    if (myVector.size() > 1)
    {
      std::sort(myVector.begin(), myVector.end(), compare);
    }
    if (!myVector.empty())
    {
      for (const auto &vector: myVector)
      {
        std::cout << vector.key1 << ',' << vector.key2 << ',' << vector.str << '\n';
      }
    }
  }
  catch (const std::exception &err)
  {
    std::cerr << err.what();
    return 1;
  }
  return 0;
}
