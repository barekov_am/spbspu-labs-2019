#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <vector>
#include "dataStruct.hpp"

std::vector<DataStruct> readVector();
bool compare(const DataStruct &, const DataStruct &);

#endif
