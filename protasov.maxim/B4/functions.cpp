#include <iostream>
#include "functions.hpp"

std::vector<DataStruct> readVector()
{
  std::vector<DataStruct> myVector;

  int key[2] = {0};
  std::string line;
  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading caused an error");
    }

    for (int i = 0; i < 2; ++i)
    {
      std::size_t pos = line.find(',');
      if (pos == std::string::npos)
      {
        throw std::invalid_argument("Invalid input data");
      }
      key[i] = std::stoi(line.substr(0, pos));
      if (std::abs(key[i]) > 5)
      {
        throw std::invalid_argument("Invalid input data");
      }
      line.erase(0, pos + 1);
    }
    if (line.empty())
    {
      throw std::invalid_argument("Invalid input data");
    }

    myVector.push_back({key[0], key[1], line});

  }
  return myVector;
}

bool compare(const DataStruct &left, const DataStruct &right)
{
  if (left.key1 < right.key1)
  {
    return true;
  }
  else if (left.key1 == right.key1)
  {
    if (left.key2 < right.key2)
    {
      return true;
    }
    else if (left.key2 == right.key2)
    {
      if (left.str.size() < right.str.size())
      {
        return true;
      }
    }
  }
  return false;
}
