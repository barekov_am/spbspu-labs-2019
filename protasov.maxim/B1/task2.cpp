#include <vector>
#include <fstream>
#include <memory>
#include <iostream>

const size_t SIZE = 256;
void task2(const char* file)
{
  std::ifstream inputFile(file);
  if (!inputFile)
  {
    throw std::invalid_argument("File with this name does not exist.");
  }

  std::unique_ptr<char[], decltype(&free)> charArray(static_cast<char*>(malloc(SIZE)), &free);
  if (!charArray)
  {
    throw std::runtime_error("Can't allocate memory.");
  }

  std::size_t index = 0;
  std::size_t size = SIZE;
  while (!inputFile.eof())
  {
    inputFile.read(&charArray[index], SIZE);
    index += inputFile.gcount();

    if (inputFile.gcount() == SIZE)
    {
      size += SIZE;
      std::unique_ptr<char[], decltype(&free)> tmpArray(static_cast<char*>(realloc(charArray.get(), size)), &free);
      if (!tmpArray)
      {
        throw std::runtime_error("Can't reallocate memory.");
      }
      charArray.release();
      std::swap(charArray, tmpArray);
    }

    if (!inputFile.eof() && inputFile.fail())
    {
      throw std::runtime_error("Data reading failed");
    }
  }
  
  std::vector<char> vector(&charArray[0], &charArray[index]);
  for (auto element : vector)
  {
    std::cout << element;
  }
}
