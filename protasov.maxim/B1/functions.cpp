#include <functional>

void fillRandom(double * array, int size)
{
  for (int i = 0; i < size; ++i)
  {
    array[i] = 2.0 * rand() / RAND_MAX - 1;
  }
}
