#ifndef ACCESS_HPP
#define ACCESS_HPP

#include <cstddef>
template <typename Container>
struct Brackets
{
  static typename Container::reference element(Container &object, std::size_t index)
  {
    return object[index];
  }

  static std::size_t begin(const Container &)
  {
    return 0;
  }

  static std::size_t end(const Container &object)
  {
    return object.size();
  }

  static std::size_t next(size_t index)
  {
    return ++index;
  }
};

template <typename Container>
struct At
{
  static typename Container::reference element(Container &object, std::size_t index)
  {
    return object.at(index);
  }

  static std::size_t begin(const Container &)
  {
    return 0;
  }

  static std::size_t end(const Container &object)
  {
    return object.size();
  }

  static std::size_t next(std::size_t index)
  {
    return ++index;
  }
};

template <typename Container>
struct Iterator
{
  static typename Container::reference element(const Container &, typename Container::iterator iterator)
  {
    return *iterator;
  }

  static typename Container::iterator begin(Container &object)
  {
    return object.begin();
  }

  static typename Container::iterator end(Container &object)
  {
    return object.end();
  }

  static typename Container::iterator next(typename Container::iterator iterator)
  {
    return ++iterator;
  }
};

#endif
