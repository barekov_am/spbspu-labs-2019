#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <iostream>

void fillRandom(double * array, int size);

template <typename Collection>
void print(const Collection& collection)
{
  for (auto elem : collection)
  {
    std::cout << elem << " ";
  }
  std::cout << '\n';
}

#endif
