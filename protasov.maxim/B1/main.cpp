#include <iostream>
#include "tasks.hpp"
#include <random>

int main(int argc, char *argv[])
{
  if ((argc < 2) || (argc > 4))
  {
    std::cerr << "Number of parameters is incorrect";
    return 1;
  }

  try
  {
    switch (atoi(argv[1]))
    {
      case 1:
        if (argc != 3)
        {
          std::cerr << "Number of parameters is incorrect for this task";
          return 1;
        }
        task1(argv[2]);
        break;

      case 2:
        if (argc != 3)
        {
          std::cerr << "Number of parameters is incorrect for this task";
          return 1;
        }
        task2(argv[2]);
        break;

      case 3:
        if (argc != 2)
        {
          std::cerr << "Number of parameters is incorrect for this task";
          return 1;
        }
        task3();
        break;

      case 4:
        if (argc != 4)
        {
          std::cerr << "Number of parameters is incorrect for this task";
          return 1;
        }
        task4(argv[2], std::atoi(argv[3]));
        break;

      default:
        std::cerr << "Task number is incorrect";
        return 1;
    }
  }
  catch(const std::exception &e)
  {
    std::cerr << e.what();
    return 1;
  }

  return 0;
}
