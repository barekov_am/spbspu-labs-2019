#include <forward_list>
#include <iostream>
#include <stdexcept>
#include <vector>
#include "sort.hpp"
#include "access.hpp"
#include "functions.hpp"

void task1(const char* dir)
{
  std::vector<int> vectorBrackets;
  int element = 0;

  while (std::cin >> element)
  {
    vectorBrackets.push_back(element);
  }
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Error while reading data.");
  }
  std::vector<int> vectorAt = vectorBrackets;
  std::forward_list<int> listIterator(vectorBrackets.begin(), vectorBrackets.end());
  sort<Brackets>(vectorBrackets, dir);
  print(vectorBrackets);
  sort<At>(vectorAt, dir);
  print(vectorAt);
  sort<Iterator>(listIterator, dir);
  print(listIterator);
}
