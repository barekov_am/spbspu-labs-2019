#include <vector>
#include "functions.hpp"
#include "access.hpp"
#include "sort.hpp"

void task4(const char* dir, int size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Wrong size");
  }
  std::vector<double> randomVector(size);
  fillRandom(&randomVector[0], size);
  print(randomVector);
  sort<Brackets>(randomVector, dir);
  print(randomVector);
}
