#ifndef SORT_HPP
#define SORT_HPP

#include <functional>
#include <cstring>

template <template <typename Container> class Access, typename Container>
void sort(Container& collection, const char* dir)
{
  using valT = typename Container::value_type;
  using access = Access<Container>;
  std::function<bool (valT, valT)> comparator;
  if (std::strcmp(dir, "ascending") == 0)
  {
    comparator = [](valT a, valT b) { return a < b; };
  }
  else if (std::strcmp(dir, "descending") == 0)
  {
    comparator = [](valT a, valT b) { return a > b; };
  }
  else
  {
    throw std::invalid_argument("Invalid sorting direction input");
  }

  for (auto i = access::begin(collection); i != access::end(collection); ++i)
  {
    for (auto j = access::next(i); j != access::end(collection); ++j)
    {
      if (comparator(access::element(collection, j), access::element(collection, i)))
      {
        std::swap(access::element(collection, j), access::element(collection, i));
      }
    }
  }
}

#endif
