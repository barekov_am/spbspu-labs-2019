#ifndef B1_TASKS
#define B1_TASKS

void task1(const char *);
void task2(const char *);
void task3();
void task4(const char *, int);

#endif
