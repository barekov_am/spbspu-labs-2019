#include <iostream>
#include <vector>
#include "functions.hpp"

void task3()
{
  std::vector<int> inputVector;
  int value = 0;
  bool finished = false;
  while (std::cin >> value)
  {
    if (value == 0)
    {
      finished = true;
      break;
    }
    inputVector.push_back(value);
  }
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Incorrect input");
  }
  if (inputVector.empty())
  {
    return;
  }
  if (!finished)
  {
    throw std::invalid_argument("There must be 0 in the end of sequence");
  }

  auto iterator = inputVector.begin();
  if (inputVector.back() == 1)
  {
    while (iterator != inputVector.end())
    {
      if (*iterator % 2 == 0)
      {
        iterator = inputVector.erase(iterator);
      }
      else
      {
        ++iterator;
      }
    }
  }
  if (inputVector.back() == 2)
  {
    while (iterator != inputVector.end())
    {
      if (*iterator % 3 == 0)
      {
        iterator = inputVector.insert(++iterator, 3, 1) + 2;
      }
      else
      {
        ++iterator;
      }
    }
  }

  print(inputVector);
}
