#include "polygon.hpp"
#include <stdexcept>
#include <cmath>
#include "algorithm"


protasov::Polygon::Polygon(const Polygon &other) :
  vCount_(other.vCount_),
  vertices_(new point_t[vCount_])
{
  for (size_t i = 0; i < vCount_; ++i)
  {
    vertices_[i] = other.vertices_[i];
  }
}

protasov::Polygon::Polygon(Polygon &&other) noexcept :
  vCount_(other.vCount_),
  vertices_(other.vertices_)
{
  other.vCount_ = 0;
  other.vertices_ = nullptr;
}

protasov::Polygon::Polygon(const size_t &count, protasov::point_t *vertices) :
  vCount_(count),
  vertices_(new point_t[count])
{
  if (vertices == nullptr)
  {
    throw std::invalid_argument("Received null pointer to vertices.");
  }
  if (vCount_ < 3)
  {
    throw std::invalid_argument("The number of vertices must be at least 3.");
  }
  for (size_t i = 0; i < vCount_; i++)
  {
    vertices_[i] = vertices[i];
  }
  if (getArea() <= 0)
  {
    delete[] vertices_;
    throw std::invalid_argument("Polygon area must be positive.");
  }
  if (!isConvex())
  {
    delete[] vertices_;
    throw std::invalid_argument("Polygon is concave.");
  }
}

protasov::Polygon::~Polygon()
{
  delete[] vertices_;
}

protasov::Polygon& protasov::Polygon::operator=(const protasov::Polygon &other)
{
  if (this != &other)
  {
    vCount_ = other.vCount_;
    delete[] vertices_;
    vertices_ = new point_t[vCount_];
    for (size_t i = 0; i < vCount_; ++i)
    {
      vertices_[i] = other.vertices_[i];
    }
  }
  return *this;
}

protasov::Polygon& protasov::Polygon::operator=(protasov::Polygon &&other) noexcept
{
  if (this != &other)
  {
    vCount_ = other.vCount_;
    other.vCount_ = 0;
    delete[] vertices_;
    vertices_ = other.vertices_;
    other.vertices_ = nullptr;
  }
  return *this;
}

double protasov::Polygon::getArea() const
{
  double area = ((vertices_[vCount_ - 1].x + vertices_[0].x) * (vertices_[vCount_ - 1].y - vertices_[0].y));
  for (size_t i = 0; i < vCount_ - 1; ++i)
  {
    area += ((vertices_[i].x + vertices_[i + 1].x) * (vertices_[i].y - vertices_[i + 1].y));
  }
  return fabs(area) / 2;
}

protasov::rectangle_t protasov::Polygon::getFrameRect() const
{
  double min_x = vertices_[0].x;
  double max_x = vertices_[0].x;
  double max_y = vertices_[0].y;
  double min_y = vertices_[0].y;
  for (size_t i = 1; i < vCount_; ++i)
  {
    min_x = std::min(min_x, vertices_[i].x);
    max_x = std::max(max_x, vertices_[i].x);
    max_y = std::max(max_y, vertices_[i].y);
    min_y = std::min(min_y, vertices_[i].y);
  }
  rectangle_t rectangle;
  rectangle.pos.x = (min_x + max_x) / 2;
  rectangle.pos.y = (min_y + max_y) / 2;
  rectangle.height = (max_y - min_y);
  rectangle.width = (max_x - min_x);
  return rectangle;
}

void protasov::Polygon::scale(double scale)
{
  if (scale <= 0)
  {
    throw std::invalid_argument("Invalid Polygon scale coefficient!");
  }
  else
  {
    point_t center = getCenter();
    for (size_t i = 0; i < vCount_; ++i)
    {
      vertices_[i].x = center.x + (vertices_[i].x - center.x)*scale;
      vertices_[i].y = center.y + (vertices_[i].y - center.y)*scale;
    }
  }
}

void protasov::Polygon::move(double move_x, double move_y)
{
  for (size_t i = 0; i < vCount_; ++i)
  {
    vertices_[i].x += move_x;
    vertices_[i].y += move_y;
  }
}

void protasov::Polygon::move(const protasov::point_t &position)
{
  point_t center = getCenter();
  move(position.x - center.x, position.y - center.y);
}

protasov::point_t protasov::Polygon::getCenter() const
{
  point_t center{0, 0};
  for (size_t i = 0; i < vCount_; ++i)
  {
    center.x += vertices_[i].x / vCount_;
    center.y += vertices_[i].y / vCount_;
  }
  return center;
}

bool protasov::Polygon::isConvex() const
{
  point_t sideA = {vertices_[0].x - vertices_[vCount_ - 1].x, vertices_[0].y - vertices_[vCount_ - 1].y};
  point_t sideB = {vertices_[1].x - vertices_[0].x, vertices_[1].y - vertices_[0].y};
  double sign = sideA.x * sideB.y - sideA.y * sideB.x;
  for (size_t i = 0; i < vCount_ - 2; ++i)
  {
    sideA.x = vertices_[i + 1].x - vertices_[i].x;
    sideA.y = vertices_[i + 1].y - vertices_[i].y;
    sideB.x = vertices_[i + 2].x - vertices_[i + 1].x;
    sideB.y = vertices_[i + 2].y - vertices_[i + 1].y;
    if ((sideA.x * sideB.y - sideA.y * sideB.x) * sign <= 0)
    {
      return false;
    }
  }
  return true;
}

void protasov::Polygon::rotate(double angle)
{
  angle = angle * M_PI / 180;
  double r = 0;
  double angle2 = 0;
  point_t center = getCenter();
  for (size_t i = 0; i < vCount_; i++)
  {
    r = std::sqrt((center.x - vertices_[i].x) * (center.x - vertices_[i].x) + (center.y - vertices_[i].y) * (center.y - vertices_[i].y));
    if (vertices_[i].y > center.y)
    {
      angle2 = acos((vertices_[i].x - center.x) / r);
    }
    else
    {
      angle2 = 2 * M_PI - acos((vertices_[i].x - center.x) / r);
    }
    vertices_[i].x += (cos(angle2 + angle) - cos(angle2)) * r;
    vertices_[i].y += (sin(angle2 + angle) - sin(angle2)) * r;
  }
}

