#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>

namespace protasov
{
  template<typename T>
  class Matrix
  {
  public:
    using ptr = std::shared_ptr<T>;
    using arr = std::unique_ptr<ptr[]>;

    class Layer
    {
    public:
      Layer(const Layer &);
      Layer(Layer &&) noexcept;
      Layer(ptr *, size_t);
      ~Layer() = default;

      Layer &operator =(const Layer &);
      Layer &operator =(Layer &&) noexcept;
      ptr operator [](size_t) const;
      size_t getSize() const;

    private:
      size_t size_;
      std::unique_ptr<ptr[]> array_;
    };

    Matrix();
    Matrix(const Matrix<T> &);
    Matrix(Matrix<T> &&) noexcept;
    ~Matrix() = default;

    Matrix<T> &operator =(const Matrix<T> &);
    Matrix<T> &operator =(Matrix<T> &&) noexcept;
    Layer operator [](size_t) const;
    bool operator ==(const Matrix<T> &) const;
    bool operator !=(const Matrix<T> &) const;

    size_t getRows() const;
    void add(ptr, size_t);
    void swap(Matrix<T> &) noexcept;

  private:
    size_t rows_;
    size_t count_;
    std::unique_ptr<size_t[]> columns_;
    arr data_;
  };

  template <typename T>
  Matrix<T>::Matrix() :
    rows_(0),
    count_(0)
  {
  }

  template <typename T>
  Matrix<T>::Matrix(const Matrix<T> &other) :
    rows_(other.rows_),
    count_(other.count_),
    columns_(std::make_unique<size_t []>(other.rows_)),
    data_(std::make_unique<ptr []>(other.count_))
  {
    for (size_t i = 0; i < rows_; ++i)
    {
      columns_[i] = other.columns_[i];
    }

    for (size_t i = 0; i < count_; ++i)
    {
      data_[i] = other.data_[i];
    }
  }

  template <typename T>
  Matrix<T>::Matrix(Matrix<T> &&other) noexcept :
    rows_(other.rows_),
    count_(other.count_),
    columns_(std::move(other.columns_)),
    data_(std::move(other.data_))
  {
    other.rows_ = 0;
    other.count_ = 0;
  }

  template <typename T>
  Matrix<T> &Matrix<T>::operator =(const Matrix<T> &other)
  {
    if (this != &other)
    {
      Matrix<T>(other).swap(*this);
    }
    return *this;
  }

  template <typename T>
  Matrix<T> &Matrix<T>::operator =(Matrix<T> &&other) noexcept
  {
    if (this != &other)
    {
      rows_ = other.rows_;
      other.rows_ = 0;
      count_ = other.count_;
      other.count_ = 0;
      columns_ = std::move(other.columns_);
      data_ = std::move(other.data_);
    }

    return *this;
  }

  template <typename T>
  typename Matrix<T>::Layer Matrix<T>::operator [](size_t row) const
  {
    if (row >= rows_)
    {
      throw std::out_of_range("Index out of range!");
    }

    size_t startRow = 0;

    for (size_t i = 0; i < row; ++i)
    {
      startRow += columns_[i];
    }

    return Matrix<T>::Layer(&data_[startRow], columns_[row]);
  }

  template <typename T>
  bool Matrix<T>::operator ==(const Matrix<T> &matrix) const
  {
    if ((rows_ != matrix.rows_) || (count_ != matrix.count_))
    {
      return false;
    }

    for (size_t i = 0; i < rows_; ++i)
    {
      if (columns_[i] != matrix.columns_[i])
      {
        return false;
      }
    }

    for (size_t i = 0; i < count_; ++i)
    {
      if (data_[i] != matrix.data_[i])
      {
        return false;
      }
    }

    return true;
  }

  template <typename T>
  bool Matrix<T>::operator !=(const Matrix<T> &matrix) const
  {
    return !(*this == matrix);
  }

  template <typename T>
  size_t Matrix<T>::getRows() const
  {
    return rows_;
  }

  template <typename T>
  void Matrix<T>::add(ptr obj, size_t row)
  {
    if (row > rows_)
    {
      throw std::out_of_range("Index out of range");
    }

    if (!obj)
    {
      throw std::invalid_argument("Pointer is nullptr");
    }

    std::unique_ptr<ptr []> newData = std::make_unique<ptr []>(count_ + 1);
    size_t newIndex = 0;
    size_t oldIndex = 0;

    for (size_t i = 0; i < rows_; ++i)
    {
      for (size_t j = 0; j < columns_[i]; ++j)
      {
        newData[newIndex++] = data_[oldIndex++];
      }
      if (row == i)
      {
        newData[newIndex++] = obj;
        ++columns_[row];
      }
    }

    if (row == rows_)
    {
      std::unique_ptr<size_t []> newColumns = std::make_unique<size_t []>(rows_ + 1);

      for (size_t i = 0; i < rows_; ++i)
      {
        newColumns[i] = columns_[i];
      }
      newColumns[row] = 1;
      newData[count_] = obj;
      ++rows_;
      columns_.swap(newColumns);
    }

    data_.swap(newData);
    ++count_;
  }

  template <typename T>
  void Matrix<T>::swap(Matrix<T> &other) noexcept
  {
    std::swap(rows_, other.rows_);
    std::swap(count_, other.count_);
    std::swap(columns_, other.columns_);
    std::swap(data_, other.data_);
  }

  template <typename T>
  Matrix<T>::Layer::Layer(const Matrix<T>::Layer &other):
    size_(other.size_),
    array_(std::make_unique<ptr []>(other.size_))
  {
    for (size_t i = 0; i < size_; ++i)
    {
      array_[i] = other.array_[i];
    }
  }

  template <typename T>
  Matrix<T>::Layer::Layer(Matrix<T>::Layer &&other) noexcept :
    size_(other.size_),
    array_(std::move(other.array_))
  {
    other.size_ = 0;
  }

  template <typename T>
  Matrix<T>::Layer::Layer(ptr *array, size_t size):
    size_(size),
    array_(std::make_unique<ptr []>(size))
  {
    for (size_t i = 0; i < size_; ++i)
    {
      array_[i] = array[i];
    }
  }

  template <typename T>
  typename Matrix<T>::Layer &Matrix<T>::Layer::operator =(const Matrix<T>::Layer &other)
  {
    if (this != &other)
    {
      array_ = std::make_unique<ptr[]>(other.size_);
      for (size_t i = 0; i < other.size_; ++i)
      {
        array_[i] = other.data_[i];
      }
      size_ = other.size_;
    }

    return *this;
  }

  template <typename T>
  typename Matrix<T>::Layer &Matrix<T>::Layer::operator =(Matrix<T>::Layer &&other) noexcept
  {
    if (this != &other)
    {
      size_ = other.size_;
      other.size_ = 0;
      array_ = std::move(other.array_);
    }
  }

  template <typename T>
  typename Matrix<T>::ptr Matrix<T>::Layer::operator [](std::size_t index) const
  {
    if (index >= size_)
    {
      throw std::out_of_range("Index out of range");
    }

    return array_[index];
  }

  template<typename T>
  size_t Matrix<T>::Layer::getSize() const
  {
    return size_;
  }

}

#endif
