#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <cstddef>
#include "shape.hpp"

namespace protasov
{
  class Polygon : public Shape
  {
  public:
    Polygon(const Polygon &);
    Polygon(Polygon &&) noexcept;
    Polygon(const std::size_t &, point_t *);
    ~Polygon() override;
    Polygon &operator = (const Polygon &);
    Polygon &operator = (Polygon &&) noexcept;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void scale(double) override;
    void move(const point_t &) override;
    void move(double, double) override ;
    point_t getCenter() const;
    bool isConvex() const;
    void rotate(double) override;

  private:
    std::size_t vCount_;
    point_t *vertices_;
  };
}

#endif
