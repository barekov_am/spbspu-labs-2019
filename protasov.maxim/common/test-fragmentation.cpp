#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "fragmentation.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(fragmentationTestSuite)

    BOOST_AUTO_TEST_CASE(testSplitting)
    {
      const protasov::Circle circle1({0.0, 0.0}, 3.0);
      const protasov::Circle circle2({5.0, 5.0}, 1.0);
      const protasov::Rectangle rect1({-2.0, 1.0}, 4.0, 4.0);
      const protasov::Rectangle rect2({-5.0, 4.0}, 3.0, 6.0);

      std::shared_ptr<protasov::Shape> circlePtr1 = std::make_shared<protasov::Circle>(circle1);
      std::shared_ptr<protasov::Shape> circlePtr2 = std::make_shared<protasov::Circle>(circle2);
      std::shared_ptr<protasov::Shape> rectPtr1 = std::make_shared<protasov::Rectangle>(rect1);
      std::shared_ptr<protasov::Shape> rectPtr2 = std::make_shared<protasov::Rectangle>(rect2);

      protasov::CompositeShape compositeShape;
      compositeShape.addShape(circlePtr1);
      compositeShape.addShape(rectPtr1);
      compositeShape.addShape(circlePtr2);
      compositeShape.addShape(rectPtr2);

      protasov::Matrix<protasov::Shape> matrix = protasov::split(compositeShape);

      BOOST_CHECK(matrix[0][0] == circlePtr1);
      BOOST_CHECK(matrix[0][1] == circlePtr2);
      BOOST_CHECK(matrix[1][0] == rectPtr1);
      BOOST_CHECK(matrix[2][0] == rectPtr2);

      BOOST_CHECK_EQUAL(matrix.getRows(), 3);
      BOOST_CHECK_EQUAL(matrix[0].getSize(), 2);
      BOOST_CHECK_EQUAL(matrix[1].getSize(), 1);
      BOOST_CHECK_EQUAL(matrix[2].getSize(), 1);

      const protasov::Rectangle rect3({6.0, 6.0}, 2.0, 2.0);
      std::shared_ptr<protasov::Shape> rectPointer3 = std::make_shared<protasov::Rectangle>(rect3);

      compositeShape.addShape(rectPointer3);
      matrix = protasov::split(compositeShape);

      BOOST_CHECK(matrix[1][1] == rectPointer3);
      BOOST_CHECK_EQUAL(matrix.getRows(), 3);
      BOOST_CHECK_EQUAL(matrix[0].getSize(), 2);
      BOOST_CHECK_EQUAL(matrix[1].getSize(), 2);
      BOOST_CHECK_EQUAL(matrix[2].getSize(), 1);
    }

    BOOST_AUTO_TEST_CASE(testIntersect)
    {
      const protasov::Rectangle rect({0.0, 0.0}, 6.0, 5.0);
      const protasov::Rectangle rect1({2.0, 2.0}, 4.0, 4.0);
      const protasov::Rectangle rect2({-2.0, 3.0}, 4.0, 2.0);
      const protasov::Rectangle rect3({-2.0, -3.0}, 2.0, 2.0);
      const protasov::Rectangle rect4({2.0, -3.0}, 2.0, 2.0);
      const protasov::Circle circle({10.0, -20.0}, 4.0);

      BOOST_CHECK(protasov::intersect(rect, rect1));
      BOOST_CHECK(protasov::intersect(rect, rect2));
      BOOST_CHECK(protasov::intersect(rect, rect3));
      BOOST_CHECK(protasov::intersect(rect, rect4));
      BOOST_CHECK(!protasov::intersect(rect, circle));
    }

BOOST_AUTO_TEST_SUITE_END()
