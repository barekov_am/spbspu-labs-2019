#include "circle.hpp"
#include <stdexcept>
#include <cmath>

protasov::Circle::Circle(const protasov::point_t &center, const double &radius) :
  center_(center),
  radius_(radius)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("Radius must be positive");
  }
}

double protasov::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

protasov::rectangle_t protasov::Circle::getFrameRect() const
{
  return {2 * radius_, 2 * radius_, center_};
}

void protasov::Circle::move(const protasov::point_t &position)
{
  center_ = position;
}

void protasov::Circle::move(double move_x, double move_y)
{
  center_.x += move_x;
  center_.y += move_y;
}

void protasov::Circle::scale(double scale)
{
  if (scale <= 0)
  {
    throw std::invalid_argument("Scale must be positive");
  }
  else
  {
    radius_ *= scale;
  }
}

void protasov::Circle::rotate(double)
{

}
