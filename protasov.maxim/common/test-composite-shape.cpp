#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

const double DIFF = 0.01;

BOOST_AUTO_TEST_SUITE(CompositeShapeTestSuite)

  BOOST_AUTO_TEST_CASE(testCompositeShapeSavedParamAfterShiftMovingByValues)
  {
    protasov::Rectangle rectangle({1.0, 1.0}, 2.0, 2.0);
    protasov::Circle circle({2.0, 2.0}, 2.0);
    protasov::CompositeShape testShapes;
    testShapes.addShape(std::make_shared<protasov::Rectangle>(rectangle));
    testShapes.addShape(std::make_shared<protasov::Circle>(circle));
    const protasov::rectangle_t rectangleBefore = testShapes.getFrameRect();
    const double areaBefore = testShapes.getArea();
    testShapes.move(10.0, 10.0);
    BOOST_CHECK_CLOSE(rectangleBefore.width, testShapes.getFrameRect().width, DIFF);
    BOOST_CHECK_CLOSE(rectangleBefore.height, testShapes.getFrameRect().height, DIFF);
    BOOST_CHECK_CLOSE(areaBefore, testShapes.getArea(), DIFF);
  }

  BOOST_AUTO_TEST_CASE(testCompositeShapeSavedParamAfterShiftMovingByPos)
  {
    protasov::Triangle triangle({6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8});
    protasov::point_t poly[] = {{6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8}, {3, 6}};
    size_t count = 4;
    protasov::Polygon polygon(count, poly);
    protasov::CompositeShape testShapes;
    testShapes.addShape(std::make_shared<protasov::Triangle>(triangle));
    testShapes.addShape(std::make_shared<protasov::Polygon>(polygon));
    const protasov::rectangle_t rectangleBefore = testShapes.getFrameRect();
    const double areaBefore = testShapes.getArea();
    testShapes.move({10.0, 10.0});
    BOOST_CHECK_CLOSE(rectangleBefore.width, testShapes.getFrameRect().width, DIFF);
    BOOST_CHECK_CLOSE(rectangleBefore.height, testShapes.getFrameRect().height, DIFF);
    BOOST_CHECK_CLOSE(areaBefore, testShapes.getArea(), DIFF);
  }

  BOOST_AUTO_TEST_CASE(scalingCompositeShape)
  {
    protasov::Rectangle rectangle({1.0, 1.0}, 2.0, 2.0);
    protasov::Circle circle({2.0, 2.0}, 2.0);
    protasov::Triangle triangle({6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8});
    protasov::point_t poly[] = {{6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8}, {3, 6}};
    size_t count = 4;
    protasov::Polygon polygon(count, poly);
    protasov::CompositeShape testShapes;
    testShapes.addShape(std::make_shared<protasov::Rectangle>(rectangle));
    testShapes.addShape(std::make_shared<protasov::Circle>(circle));
    testShapes.addShape(std::make_shared<protasov::Triangle>(triangle));
    testShapes.addShape(std::make_shared<protasov::Polygon>(polygon));

    const double areaBefore = testShapes.getArea();
    const double coefficient = 3.0;
    testShapes.scale(coefficient);
    BOOST_CHECK_CLOSE(testShapes.getArea(), coefficient * coefficient * areaBefore, DIFF);
  }

  BOOST_AUTO_TEST_CASE(invalidCompositeShapeValues)
  {
    protasov::CompositeShape testShapes1;
    BOOST_CHECK_THROW(testShapes1.scale(2.0), std::logic_error);
    BOOST_CHECK_THROW(testShapes1.move(1.0, 1.0), std::logic_error);
    BOOST_CHECK_THROW(testShapes1.move({1.0, 1.0}), std::logic_error);
    BOOST_CHECK_THROW(testShapes1.getFrameRect(), std::logic_error);
    BOOST_CHECK_THROW(testShapes1.getArea(), std::logic_error);

    protasov::Rectangle rectangle({1.0, 1.0}, 2.0, 2.0);
    protasov::Circle circle({2.0, 2.0}, 2.0);
    protasov::Triangle triangle({6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8});
    protasov::point_t poly[] = {{6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8}, {3, 6}};
    size_t count = 4;
    protasov::Polygon polygon(count, poly);
    protasov::CompositeShape testShapes2;
    BOOST_CHECK_THROW(testShapes2.addShape(nullptr), std::invalid_argument);
    testShapes2.addShape(std::make_shared<protasov::Rectangle>(rectangle));
    testShapes2.addShape(std::make_shared<protasov::Circle>(circle));
    testShapes2.addShape(std::make_shared<protasov::Triangle>(triangle));
    testShapes2.addShape(std::make_shared<protasov::Polygon>(polygon));
    BOOST_CHECK_THROW(testShapes2.scale(-1.0), std::invalid_argument);
    BOOST_CHECK_THROW(testShapes2.deleteShape(-1), std::out_of_range);
    BOOST_CHECK_THROW(testShapes2.deleteShape(200), std::out_of_range);
    BOOST_CHECK_THROW(testShapes2[-1], std::out_of_range);
    BOOST_CHECK_THROW(testShapes2[100], std::out_of_range);
  }

BOOST_AUTO_TEST_SUITE_END()
