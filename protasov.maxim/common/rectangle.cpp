#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

protasov::Rectangle::Rectangle(const protasov::point_t &center, double width, double height):
  center_(center),
  width_(width),
  height_(height),
  angle_(0)
{
  if (width_ <= 0)
  {
    throw std::invalid_argument("Width must be positive");
  }
  if (height_ <= 0)
  {
    throw std::invalid_argument("Height must be positive");
  }
}

protasov::Rectangle::Rectangle(const protasov::point_t &center, double width, double height, double angle) :
  Rectangle(center, width, height)
{
  rotate(angle);
}

double protasov::Rectangle::getArea() const
{
  return width_ * height_;
}

protasov::rectangle_t protasov::Rectangle::getFrameRect() const
{
  const double sin = std::sin(angle_ * M_PI / 180);
  const double cos = std::cos(angle_ * M_PI / 180);
  const double height = width_ * std::abs(sin) + height_ * std::abs(cos);
  const double width = width_ * std::abs(cos) + height_ * std::abs(sin);
  return {width, height, center_};
}

void protasov::Rectangle::move(const protasov::point_t &position)
{
  center_ = position;
}

void protasov::Rectangle::move(double move_x, double move_y)
{
  center_.x += move_x;
  center_.y += move_y;
}

void protasov::Rectangle::scale(double scale)
{
  if (scale <= 0)
  {
    throw std::invalid_argument("Scale must be positive");
  }
  else
  {
    width_ *= scale;
    height_ *= scale;
  }
}

void protasov::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
