#include "fragmentation.hpp"
#include <cmath>

protasov::Matrix<protasov::Shape> protasov::split(const CompositeShape &compShape)
{
  protasov::Matrix<protasov::Shape> matrix;

  for (size_t i = 0; i < compShape.getCount(); ++i)
  {
    size_t placeRow = 0;

    size_t j = matrix.getRows();
    while (j-- > 0)
    {
      for (size_t k = 0; k < matrix[j].getSize(); ++k)
      {
        if (intersect(*compShape[i], *matrix[j][k]))
        {
          placeRow = j + 1;
          break;
        }
      }
      if (placeRow)
      {
        break;
      }
    }
    matrix.add(compShape[i], placeRow);
  }

  return matrix;
}



bool protasov::intersect(const protasov::Shape& shape1, const protasov::Shape& shape2)
{
  const protasov::rectangle_t shape1Frame = shape1.getFrameRect();
  const protasov::rectangle_t shape2Frame = shape2.getFrameRect();

  if (std::abs(shape1Frame.pos.x - shape2Frame.pos.x) > (shape1Frame.width + shape2Frame.width) / 2)
  {
    return false;
  }

  if (std::abs(shape1Frame.pos.y - shape2Frame.pos.y) > (shape1Frame.height + shape2Frame.height) / 2)
  {
    return false;
  }

  return true;
}
