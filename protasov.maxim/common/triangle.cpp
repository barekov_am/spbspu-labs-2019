#include "triangle.hpp"
#include "base-types.hpp"
#include <stdexcept>
#include <cmath>


protasov::Triangle::Triangle(const point_t &p1, const point_t &p2, const point_t &p3) :
  a_(p1),
  b_(p2),
  c_(p3)
{
  if(getArea() <= 0)
  {
    throw std::invalid_argument("Triangle area must be positive.");
  }
}

double protasov::Triangle::getArea() const
{
  double ab = getDistance(a_, b_);
  double ac = getDistance(a_, c_);
  double bc = getDistance(b_, c_);
  double semi_perimetr = (ab + ac + bc) / 2;
  return std::sqrt(semi_perimetr * (semi_perimetr - ab) * (semi_perimetr - ac) * (semi_perimetr - bc));
}

protasov::rectangle_t protasov::Triangle::getFrameRect() const
{
  using namespace std;
  double min_x = min(min(a_.x, b_.x), c_.x);
  double min_y = min(min(a_.y, b_.y), c_.y);
  double h = fabs(max(max(a_.y, b_.y), c_.y) - min_y);
  double w = fabs(max(max(a_.x, b_.x), c_.x) - min_x);
  return rectangle_t{w, h, getCenter()};
}

void protasov::Triangle::move(const point_t &position)
{
  move(position.x - getCenter().x, position.y - getCenter().y);
}

void protasov::Triangle::move(double move_x, double move_y)
{
  a_.x += move_x;
  b_.x += move_x;
  c_.x += move_x;
  a_.y += move_y;
  b_.y += move_y;
  c_.y += move_y;
}

void protasov::Triangle::scale(double scale)
{
  if(scale <= 0)
  {
    throw std::invalid_argument("Invalid Triangle scale coefficient!");
  }
  point_t center = getCenter();
  a_.x = center.x + (a_.x - center.x) * scale;
  a_.y = center.y + (a_.y - center.y) * scale;
  b_.x = center.x + (b_.x - center.x) * scale;
  b_.y = center.y + (b_.y - center.y) * scale;
  c_.x = center.x + (c_.x - center.x) * scale;
  c_.y = center.y + (c_.y - center.y) * scale;
}

protasov::point_t protasov::Triangle::getCenter() const
{
  return point_t{ (a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3 };
}

double protasov::Triangle::getDistance(const point_t &p1, const point_t &p2)
{
  return std::sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
}

void protasov::Triangle::rotate(double angle)
{
  point_t center = getCenter();
  const double cos = std::abs(std::cos(angle * M_PI / 180));
  const double sin = std::abs(std::sin(angle * M_PI / 180));
  const double Ax = (a_.x - center.x) * cos - (a_.y - center.y) * sin;
  const double Ay = (a_.x - center.x) * sin + (a_.y - center.y) * cos;
  const double Bx = (b_.x - center.x) * cos - (b_.y - center.y) * sin;
  const double By = (b_.x - center.x) * sin + (b_.y - center.y) * cos;
  const double Cx = (c_.x - center.x) * cos - (c_.y - center.y) * sin;
  const double Cy = (c_.x - center.x) * sin + (c_.y - center.y) * cos;
  a_.x = Ax + center.x;
  a_.y = Ay + center.y;
  b_.x = Bx + center.x;
  b_.y = By + center.y;
  c_.x = Cx + center.x;
  c_.y = Cy + center.y;
}
