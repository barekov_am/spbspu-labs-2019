#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double DIFF = 0.001;

BOOST_AUTO_TEST_SUITE(testRectangleMethods)

  BOOST_AUTO_TEST_CASE(testRectangleSavedParamAfterShiftMovingByValues)
  {
    protasov::Rectangle testRectangle({6.1, 8.2}, 5.3, 2.4);
    const double rectangleAreaBeforeMoving = testRectangle.getArea();
    const protasov::rectangle_t rectangleFrameBeforeMoving = testRectangle.getFrameRect();
    testRectangle.move(-3.5, 2.5);
    BOOST_CHECK_CLOSE(rectangleAreaBeforeMoving, testRectangle.getArea(), DIFF);
    protasov::rectangle_t rectangleFrameAfterMoving = testRectangle.getFrameRect();
    BOOST_CHECK_CLOSE(rectangleFrameBeforeMoving.width, rectangleFrameAfterMoving.width, DIFF);
    BOOST_CHECK_CLOSE(rectangleFrameBeforeMoving.height, rectangleFrameAfterMoving.height, DIFF);
  }

  BOOST_AUTO_TEST_CASE(testRectangleSavedParamAfterShiftMovingByPoint)
  {
    protasov::Rectangle testRectangle({5.2, -7.2}, 5.5, 2.2);
    const double rectangleAreaBeforeMoving = testRectangle.getArea();
    const protasov::rectangle_t rectangleFrameBeforeMoving = testRectangle.getFrameRect();
    testRectangle.move({3.5, 2.5});
    BOOST_CHECK_CLOSE(rectangleAreaBeforeMoving, testRectangle.getArea(), DIFF);
    protasov::rectangle_t rectangleFrameAfterMoving = testRectangle.getFrameRect();
    BOOST_CHECK_CLOSE(rectangleFrameBeforeMoving.width, rectangleFrameAfterMoving.width, DIFF);
    BOOST_CHECK_CLOSE(rectangleFrameBeforeMoving.height, rectangleFrameAfterMoving.height, DIFF);
  }

  BOOST_AUTO_TEST_CASE(testRectangleFunctionScale)
  {
    protasov::Rectangle testRectangle({6.4, 8.4}, 5.3, 2.4);
    const double rectangleAreaBeforeScale = testRectangle.getArea();
    const protasov::rectangle_t rectangleFrameBeforeScale = testRectangle.getFrameRect();
    const double testScale = 14;
    testRectangle.scale(testScale);
    BOOST_CHECK_CLOSE(rectangleAreaBeforeScale * testScale * testScale, testRectangle.getArea(), DIFF);
    const protasov::rectangle_t rectangleFrameAfterScale = testRectangle.getFrameRect();
    BOOST_CHECK_CLOSE(rectangleFrameBeforeScale.width * testScale, rectangleFrameAfterScale.width, DIFF);
    BOOST_CHECK_CLOSE(rectangleFrameBeforeScale.height * testScale, rectangleFrameAfterScale.height, DIFF);
  }

  BOOST_AUTO_TEST_CASE(rectangleTestInvalidConstructorParam)
  {
    BOOST_CHECK_THROW(protasov::Rectangle({6.4, 8.4}, -5.3, 2.4), std::invalid_argument);
    BOOST_CHECK_THROW(protasov::Rectangle({5.2, -7.2}, 5.5, -2.2), std::invalid_argument);
    BOOST_CHECK_THROW(protasov::Rectangle({1.2, 8.1}, -2.2, -5.3), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(rectangleTestInvalidScale)
  {
    protasov::Rectangle testRectangle({3.3, 3.4}, 2.5, 14.0);
    BOOST_CHECK_THROW(testRectangle.scale(-3), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
