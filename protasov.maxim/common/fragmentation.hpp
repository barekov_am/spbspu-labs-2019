#ifndef A3_FRAGMENTATION_HPP
#define A3_FRAGMENTATION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace protasov
{
  Matrix<Shape> split(const CompositeShape &);
  bool intersect(const protasov::Shape &, const protasov::Shape &);
}

#endif
