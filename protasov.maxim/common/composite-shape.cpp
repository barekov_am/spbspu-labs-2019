#include "composite-shape.hpp"

#include <iostream>
#include <stdexcept>
#include <cmath>
#include "shape.hpp"

protasov::CompositeShape::CompositeShape():
  count_(0),
  shapeList_(nullptr)
{
}

protasov::CompositeShape::CompositeShape(const CompositeShape &copyShape):
  count_(copyShape.count_),
  shapeList_(std::make_unique<ptr[]>(copyShape.count_))
{
  for(size_t i = 0; i < count_; i++)
  {
    shapeList_[i] = copyShape.shapeList_[i];
  }
}

protasov::CompositeShape::CompositeShape(CompositeShape &&displaceShape) noexcept:
  count_(displaceShape.count_),
  shapeList_(std::move(displaceShape.shapeList_))
{
  displaceShape.count_ = 0;
  displaceShape.shapeList_ = nullptr;
}

protasov::CompositeShape& protasov::CompositeShape::operator =(const CompositeShape& composShape)
{
  if (this != &composShape)
  {
    shapeList_= std::make_unique<ptr[]>(composShape.count_);
    count_ = composShape.count_;
    for (size_t i = 0; i < count_; ++i)
    {
      shapeList_[i] = composShape.shapeList_[i];
    }
  }
  return *this;
}

protasov::CompositeShape& protasov::CompositeShape::operator =(protasov::CompositeShape&& composShape) noexcept
{
  if (this != &composShape)
  {
    count_ = composShape.count_;
    composShape.count_ = 0;
    shapeList_ = std::move(composShape.shapeList_);
  }
  return *this;
}

protasov::ptr protasov::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }
  return shapeList_[index];
}

double protasov::CompositeShape::getArea() const
{
  if (shapeList_ == nullptr)
  {
    throw std::logic_error("Composite shape is empty");
  }

  double area = 0.0;
  for (size_t i = 0; i < count_; i++)
  {
    area += shapeList_[i]->getArea();
  }
  return area;
}

protasov::rectangle_t protasov::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  rectangle_t rectangle = shapeList_[0]->getFrameRect();
  double min_x = rectangle.pos.x - rectangle.width / 2;
  double max_x = rectangle.pos.x + rectangle.width / 2;
  double min_y = rectangle.pos.y - rectangle.height / 2;
  double max_y = rectangle.pos.y + rectangle.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    rectangle = shapeList_[i]->getFrameRect();

    min_x = std::min(rectangle.pos.x - rectangle.width / 2, min_x);
    min_y = std::min(rectangle.pos.y - rectangle.height / 2, min_y);
    max_x = std::max(rectangle.pos.x + rectangle.width / 2, max_x);
    max_y = std::max(rectangle.pos.y + rectangle.height / 2, max_y);
  }
  rectangle.pos.x = (min_x + max_x) / 2;
  rectangle.pos.y = (min_y + max_y) / 2;
  rectangle.height = (max_y - min_y);
  rectangle.width = (max_x - min_x);
  return rectangle;
}

void protasov::CompositeShape::move(double move_x, double move_y)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  for (size_t i = 0; i < count_; ++i)
  {
    shapeList_[i]->move(move_x, move_y);
  }
}

void protasov::CompositeShape::move(const point_t &point)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  rectangle_t rectangle = getFrameRect();
  double dx = point.x - rectangle.pos.x;
  double dy = point.y - rectangle.pos.y;
  move(dx, dy);
}

void protasov::CompositeShape::scale(double coefficient)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  if (coefficient <= 0)
  {
    throw std::invalid_argument("Scale coefficient cannot be less than zero.");
  }

  point_t center = getFrameRect().pos;
  for (size_t i = 0; i < count_; ++i)
  {
    protasov::point_t shapeCenter = shapeList_[i]->getFrameRect().pos;
    shapeList_[i]->move((shapeCenter.x - center.x) * (coefficient - 1), (shapeCenter.y - center.y) * (coefficient - 1));
    shapeList_[i]->scale(coefficient);
  }
}

void protasov::CompositeShape::addShape(ptr newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Adding shape shouldn't be nullptr");
  }
  for (size_t i = 0; i < count_; ++i)
  {
    if (shapeList_[i] == newShape)
    {
      return;
    }
  }
  count_++;
  array tempShapes = std::make_unique<ptr[]>(count_);
  for (size_t i = 0; i < count_ - 1; ++i)
  {
    tempShapes[i] = shapeList_[i];
  }
  tempShapes[count_ - 1] = newShape;
  shapeList_ = std::move(tempShapes);
}

void protasov::CompositeShape::deleteShape(size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }
  for (size_t i = index; i < count_ - 1; ++i)
  {
    shapeList_[i] = shapeList_[i + 1];
  }
  shapeList_[count_ - 1] = nullptr;
  count_--;
}

size_t protasov::CompositeShape::getCount() const
{
  return count_;
}

void protasov::CompositeShape::rotate(double angle)
{
  const protasov::point_t center = getFrameRect().pos;
  const double cos = std::abs(std::cos(angle * M_PI / 180));
  const double sin = std::abs(std::sin(angle * M_PI / 180));

  for (std::size_t i = 0; i < count_; ++i)
  {
    const protasov::point_t currCenter = shapeList_[i]->getFrameRect().pos;
    const double projection_x = currCenter.x - center.x;
    const double projection_y = currCenter.y - center.y;
    const double shift_x = projection_x * (cos - 1) - projection_y * sin;
    const double shift_y = projection_x * sin + projection_y * (cos - 1);
    shapeList_[i]->move(shift_x, shift_y);
    shapeList_[i]->rotate(angle);
  }
}
