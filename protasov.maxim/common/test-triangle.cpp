#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "triangle.hpp"

const double DIFF = 0.001;

BOOST_AUTO_TEST_SUITE(testTriangleMethods)

  BOOST_AUTO_TEST_CASE(testTriangleSavedParamAfterShiftMovingByValues)
  {
    protasov::Triangle testTriangle({6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8});
    const double triangleAreaBeforeMoving = testTriangle.getArea();
    const protasov::rectangle_t triangleFrameBeforeMoving = testTriangle.getFrameRect();
    testTriangle.move(-3.5, 2.5);
    BOOST_CHECK_CLOSE(triangleAreaBeforeMoving, testTriangle.getArea(), DIFF);
    protasov::rectangle_t triangleFrameAfterMoving = testTriangle.getFrameRect();
    BOOST_CHECK_CLOSE(triangleFrameBeforeMoving.width, triangleFrameAfterMoving.width, DIFF);
    BOOST_CHECK_CLOSE(triangleFrameBeforeMoving.height, triangleFrameAfterMoving.height, DIFF);
  }
  
  BOOST_AUTO_TEST_CASE(testTriangleSavedParamAfterShiftMovingByPoint)
  {
    protasov::Triangle testTriangle({6.1, 5.5}, {7.3, 3.5}, {-3.4, -1.8});
    const double triangleAreaBeforeMoving = testTriangle.getArea();
    const protasov::rectangle_t triangleFrameBeforeMoving = testTriangle.getFrameRect();
    testTriangle.move(3.5, 2.5);
    BOOST_CHECK_CLOSE(triangleAreaBeforeMoving, testTriangle.getArea(), DIFF);
    protasov::rectangle_t triangleFrameAfterMoving = testTriangle.getFrameRect();
    BOOST_CHECK_CLOSE(triangleFrameBeforeMoving.width, triangleFrameAfterMoving.width, DIFF);
    BOOST_CHECK_CLOSE(triangleFrameBeforeMoving.height, triangleFrameAfterMoving.height, DIFF);
  }
  
  BOOST_AUTO_TEST_CASE(testTriangleFunctionScale)
  {
    protasov::Triangle testTriangle({6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8});
    const double triangleAreaBeforeScale = testTriangle.getArea();
    const protasov::rectangle_t triangleFrameBeforeScale = testTriangle.getFrameRect();
    const double testScale = 2;
    testTriangle.scale(testScale);
    BOOST_CHECK_CLOSE(triangleAreaBeforeScale * testScale * testScale, testTriangle.getArea(), DIFF);
    const protasov::rectangle_t triangleFrameAfterScale = testTriangle.getFrameRect();
    BOOST_CHECK_CLOSE(triangleFrameBeforeScale.width * testScale, triangleFrameAfterScale.width, DIFF);
    BOOST_CHECK_CLOSE(triangleFrameBeforeScale.height * testScale, triangleFrameAfterScale.height, DIFF);
  }
  
  BOOST_AUTO_TEST_CASE(triangleTestInvalidConstructorParam)
  {
    BOOST_CHECK_THROW(protasov::Triangle({0.0, 1.0}, {0.0, 2.0}, {0.0, 3.0}), std::invalid_argument);
  }
  
  BOOST_AUTO_TEST_CASE(triangleTestInvalidScale)
  {
    protasov::Triangle testTriangle({6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8});
    BOOST_CHECK_THROW(testTriangle.scale(-3), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
