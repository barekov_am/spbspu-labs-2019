#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace protasov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&) noexcept;
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape &);
    CompositeShape& operator =(CompositeShape &&) noexcept;
    ptr operator [](size_t) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double , double) override;
    void move(const point_t &) override;
    void scale(double) override;
    void addShape(ptr);
    void deleteShape(size_t);
    size_t getCount() const;
    void rotate(double) override;

  private:
    size_t count_;
    array shapeList_;
  };
}

#endif
