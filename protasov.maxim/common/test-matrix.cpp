#include <stdexcept>
#include <algorithm>
#include <boost/test/auto_unit_test.hpp>
#include "fragmentation.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(matrixTesting)

    BOOST_AUTO_TEST_CASE(copyConstructorTest)
    {
      protasov::Rectangle rec1({1, 1}, 4, 7);
      protasov::Circle cir1({2, 2}, 4);

      protasov::CompositeShape compShape;
      compShape.addShape(std::make_shared<protasov::Rectangle>(rec1));
      compShape.addShape(std::make_shared<protasov::Circle>(cir1));

      protasov::Matrix<protasov::Shape> matrix;
      matrix = protasov::split(compShape);
      protasov::Matrix<protasov::Shape> matrix1(matrix);
      BOOST_CHECK_EQUAL(matrix.getRows(), matrix1.getRows());
      BOOST_CHECK(matrix[0][0] == matrix1[0][0]);
      BOOST_CHECK(matrix[1][0] == matrix1[1][0]);
    }

    BOOST_AUTO_TEST_CASE(moveConstructorTest)
    {
      protasov::Rectangle rec1({1, 1}, 4, 7);
      protasov::Circle cir1({2, 2}, 4);


      protasov::CompositeShape compShape;
      compShape.addShape(std::make_shared<protasov::Rectangle>(rec1));
      compShape.addShape(std::make_shared<protasov::Circle>(cir1));

      protasov::Matrix<protasov::Shape> matrix;
      matrix = protasov::split(compShape);
      protasov::Matrix<protasov::Shape> matrixClone(matrix);

      protasov::Matrix<protasov::Shape> matrix1(std::move(matrix));
      BOOST_CHECK_EQUAL(matrixClone.getRows(), matrix1.getRows());
      BOOST_CHECK(matrixClone[0][0] == matrix1[0][0]);
      BOOST_CHECK(matrixClone[1][0] == matrix1[1][0]);
      BOOST_CHECK_EQUAL(matrix.getRows(), 0);
    }

    BOOST_AUTO_TEST_CASE(copyOperatorTest)
    {
      protasov::Rectangle rec1({1, 1}, 4, 7);
      protasov::Circle cir1({2, 2}, 4);
      protasov::Rectangle rec2 ({10, 14}, 4, 4);
      protasov::CompositeShape compShape;
      compShape.addShape(std::make_shared<protasov::Rectangle>(rec1));
      compShape.addShape(std::make_shared<protasov::Circle>(cir1));

      protasov::Matrix<protasov::Shape> matrix = protasov::split(compShape);
      compShape.addShape(std::make_shared<protasov::Rectangle>(rec2));
      protasov::Matrix<protasov::Shape> matrix1 = protasov::split(compShape);

      matrix1 = matrix;

      BOOST_CHECK_EQUAL(matrix.getRows(), matrix1.getRows());
      BOOST_CHECK(matrix[0][0] == matrix1[0][0]);
      BOOST_CHECK(matrix[1][0] == matrix1[1][0]);
    }

    BOOST_AUTO_TEST_CASE(moveOperatorTest)
    {
      protasov::Rectangle rec1({1, 1}, 4, 7);
      protasov::Circle cir1({2, 2}, 4);
      protasov::Rectangle rec2 ({10, 14}, 1, 1);

      protasov::CompositeShape compShape;
      compShape.addShape(std::make_shared<protasov::Rectangle>(rec1));
      compShape.addShape(std::make_shared<protasov::Circle>(cir1));

      protasov::Matrix<protasov::Shape> matrix;
      matrix = protasov::split(compShape);
      compShape.addShape(std::make_shared<protasov::Rectangle>(rec2));
      protasov::Matrix<protasov::Shape> matrixClone(matrix);
      protasov::Matrix<protasov::Shape> matrix1;
      matrix1 = protasov::split(compShape);
      matrix1 = std::move(matrix);

      BOOST_CHECK_EQUAL(matrixClone.getRows(), matrix1.getRows());
      BOOST_CHECK(matrixClone[0][0] == matrix1[0][0]);
      BOOST_CHECK(matrixClone[1][0] == matrix1[1][0]);
      BOOST_CHECK_EQUAL(matrix.getRows(), 0);
    }

    BOOST_AUTO_TEST_CASE(addingTesting)
    {
      protasov::Rectangle rectangle({1, 1}, 4, 7);
      protasov::Circle circle({2, 2}, 4);

      protasov::CompositeShape compShape;
      compShape.addShape(std::make_shared<protasov::Rectangle>(rectangle));
      compShape.addShape(std::make_shared<protasov::Circle>(circle));

      protasov::Matrix<protasov::Shape> matrix;
      matrix = protasov::split(compShape);
      size_t rowsBefore = matrix.getRows();

      protasov::Rectangle rec2({10, 10}, 4, 4);
      matrix.add(std::make_shared<protasov::Rectangle>(rec2), 0);

      BOOST_CHECK_EQUAL(rowsBefore, matrix.getRows());
    }

    BOOST_AUTO_TEST_CASE(errorsDetectionTesting)
    {
      protasov::Rectangle rectangle({1, 1}, 4, 7);
      protasov::Circle circle({2, 2}, 4);

      protasov::CompositeShape compShape;
      compShape.addShape(std::make_shared<protasov::Rectangle>(rectangle));

      protasov::Matrix<protasov::Shape> matrix;
      matrix = protasov::split(compShape);

      BOOST_CHECK_THROW(matrix[2], std::out_of_range);
      BOOST_CHECK_THROW(matrix[0][2], std::out_of_range);
      BOOST_CHECK_THROW(matrix.add(std::make_shared<protasov::Circle>(circle), 4), std::out_of_range);
    }

BOOST_AUTO_TEST_SUITE_END()
