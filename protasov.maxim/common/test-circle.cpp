#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double DIFF = 0.001;

BOOST_AUTO_TEST_SUITE(testCircleMethods)

  BOOST_AUTO_TEST_CASE(testCircleSavedParamAfterShiftMovingByValues)
  {
    protasov::Circle testCircle({6.1, 8.5}, 5.5);
    const double circleAreaBeforeMoving = testCircle.getArea();
    const protasov::rectangle_t circleFrameBeforeMoving = testCircle.getFrameRect();
    testCircle.move(-1.5, 1.0);
    BOOST_CHECK_CLOSE(circleAreaBeforeMoving, testCircle.getArea(), DIFF);
    protasov::rectangle_t circleFrameAfterMoving = testCircle.getFrameRect();
    BOOST_CHECK_CLOSE(circleFrameBeforeMoving.width, circleFrameAfterMoving.width, DIFF);
    BOOST_CHECK_CLOSE(circleFrameBeforeMoving.height, circleFrameAfterMoving.height, DIFF);
  }

  BOOST_AUTO_TEST_CASE(testCircleSavedParamAfterMovingByPoint)
  {
    protasov::Circle testCircle({3.2, 3.8}, 3.0);
    const double circleAreaBeforeMoving = testCircle.getArea();
    const protasov::rectangle_t circleFrameBeforeMoving = testCircle.getFrameRect();
    testCircle.move({12.4, -1.9});
    BOOST_CHECK_CLOSE(circleAreaBeforeMoving, testCircle.getArea(), DIFF);
    protasov::rectangle_t circleFrameAfterMoving = testCircle.getFrameRect();
    BOOST_CHECK_CLOSE(circleFrameBeforeMoving.width, circleFrameAfterMoving.width, DIFF);
    BOOST_CHECK_CLOSE(circleFrameBeforeMoving.height, circleFrameAfterMoving.height, DIFF);
  }

  BOOST_AUTO_TEST_CASE(testCircleFunctionScale)
  {
    protasov::Circle testCircle({7.2, 6.8}, 10.0);
    const double circleAreaBeforeScale = testCircle.getArea();
    const double testScale = 3;
    testCircle.scale(testScale);
    BOOST_CHECK_CLOSE(circleAreaBeforeScale * testScale * testScale, testCircle.getArea(), DIFF);
  }

  BOOST_AUTO_TEST_CASE(circleTestInvalidConstructorParam)
  {
    BOOST_CHECK_THROW(protasov::Circle({6.6, 6.7}, -4.2), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(circleTestInvalidScale)
  {
    protasov::Circle testCircle({6.6, 6.7}, 4.2);
    BOOST_CHECK_THROW(testCircle.scale(-3), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
