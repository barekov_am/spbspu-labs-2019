#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "polygon.hpp"

const double DIFF = 0.001;

BOOST_AUTO_TEST_SUITE(testPolygonMethods)

  BOOST_AUTO_TEST_CASE(testPolygonSavedParamAfterShiftMovingByValues)
  {
    protasov::point_t poly[] = {{6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8}, {3, 6}};
    size_t count = 4;
    protasov::Polygon testPolygon(count, poly);
    const double polygonAreaBeforeMoving = testPolygon.getArea();
    const protasov::rectangle_t polygonFrameBeforeMoving = testPolygon.getFrameRect();
    testPolygon.move(-3.5, 2.5);
    BOOST_CHECK_CLOSE(polygonAreaBeforeMoving, testPolygon.getArea(), DIFF);
    protasov::rectangle_t polygonFrameAfterMoving = testPolygon.getFrameRect();
    BOOST_CHECK_CLOSE(polygonFrameBeforeMoving.width, polygonFrameAfterMoving.width, DIFF);
    BOOST_CHECK_CLOSE(polygonFrameBeforeMoving.height, polygonFrameAfterMoving.height, DIFF);
  }

  BOOST_AUTO_TEST_CASE(testPolygonSavedParamAfterShiftMovingByPoint)
  {
    protasov::point_t poly[] = {{6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8}, {3, 6}};
    size_t count = 4;
    protasov::Polygon testPolygon(count, poly);
    const double polygonAreaBeforeMoving = testPolygon.getArea();
    const protasov::rectangle_t polygonFrameBeforeMoving = testPolygon.getFrameRect();
    testPolygon.move({-3.5, 2.5});
    BOOST_CHECK_CLOSE(polygonAreaBeforeMoving, testPolygon.getArea(), DIFF);
    protasov::rectangle_t polygonFrameAfterMoving = testPolygon.getFrameRect();
    BOOST_CHECK_CLOSE(polygonFrameBeforeMoving.width, polygonFrameAfterMoving.width, DIFF);
    BOOST_CHECK_CLOSE(polygonFrameBeforeMoving.height, polygonFrameAfterMoving.height, DIFF);
  }

  BOOST_AUTO_TEST_CASE(testPolygonFunctionScale)
  {
    protasov::point_t poly[] = {{6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8}, {3, 6}};
    size_t count = 4;
    protasov::Polygon testPolygon(count, poly);
    const double polygonAreaBeforeScale = testPolygon.getArea();
    const protasov::rectangle_t polygonFrameBeforeScale = testPolygon.getFrameRect();
    const double testScale = 2;
    testPolygon.scale(testScale);
    BOOST_CHECK_CLOSE(polygonAreaBeforeScale * testScale * testScale, testPolygon.getArea(), DIFF);
    const protasov::rectangle_t polygonFrameAfterScale = testPolygon.getFrameRect();
    BOOST_CHECK_CLOSE(polygonFrameBeforeScale.width * testScale, polygonFrameAfterScale.width, DIFF);
    BOOST_CHECK_CLOSE(polygonFrameBeforeScale.height * testScale, polygonFrameAfterScale.height, DIFF);
  }

  BOOST_AUTO_TEST_CASE(polygonTestInvalidConstructorParam)
  {
    protasov::point_t poly[] = {{10, 10}, {10, 10}, {-10, -10}, {15, 5}};
    size_t count = 4;
    BOOST_CHECK_THROW(protasov::Polygon(count, poly), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(polygonTestInvalidScale)
  {
    protasov::point_t poly[] = {{6.1, 5.5}, {5.3, 0.5}, {2.4, 2.8}, {3, 6}};
    size_t count = 4;
    protasov::Polygon testPolygon(count, poly);
    BOOST_CHECK_THROW(testPolygon.scale(-3), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
