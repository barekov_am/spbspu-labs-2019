#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

int main()
{
  std::cout << "__________Circles&Rectangles__________";
  blyshchik::Circle circle1{blyshchik::point_t{2, 3.2}, 4};
  blyshchik::Circle circle2{3, 6, 9};
  blyshchik::Rectangle rect1{blyshchik::point_t{7, 2}, 5, 4};
  blyshchik::Rectangle rect2{7, 7, 4, 10};

  blyshchik::Shape* figures[] = {&circle1, &circle2, &rect1, &rect2};

  for (blyshchik::Shape *element:figures) {
    element->print();
  }
  std::cout << "\tMoving all figures: x + 5, y + 4: \n";
  for (blyshchik::Shape *element:figures) {
    element->move(5, 4);
    element->print();
  }

  std::cout << "\tMoving all figures to the points with coordinates (1;1): \n";
  for (blyshchik::Shape *element:figures) {
    element->move(blyshchik::point_t{1, 1});
    element->print();
  }

  const double coefficient = 1.4;
  std::cout << "\tScaling all figures by coefficient " << coefficient << ": \n";
  for (blyshchik::Shape* element:figures) {
    element->scale(coefficient);
    element->print();
  }


  std::cout << "\n\n__________CompositeShape__________";
  shape_ptr compFigure1 = std::make_shared<blyshchik::Circle>(circle1);
  shape_ptr compFigure2 = std::make_shared<blyshchik::Circle>(circle2);
  shape_ptr compFigure3 = std::make_shared<blyshchik::Rectangle>(rect1);
  shape_ptr compFigure4 = std::make_shared<blyshchik::Rectangle>(rect2);

  blyshchik::CompositeShape compShape;
  compShape.add(compFigure1);
  compShape.add(compFigure2);
  compShape.add(compFigure3);
  compShape.add(compFigure4);

  compShape.print();
  std::cout << "\tMoving composite shape to (3, 4):\n";
  compShape.move(3, 4);
  compShape.print();
  std::cout << "\tScaling composite shape by coefficient " << coefficient << ": \n";
  compShape.print();

  std::cout << "\tDeleting shape with index = 2: \n";
  compShape.remove(2);
  compShape.print();


  std::cout << "\n\n__________Matrix and partition on layers__________";
  blyshchik::Matrix matrix = blyshchik::part(compShape);
  for (size_t i = 0; i < matrix.getLines(); i++) {
    for (size_t j = 0; j < matrix.getColumns(); j++) {
      if (matrix[i][j] != nullptr) {
        std::cout << "Layer #" << i << ":\n" << "Figure #" << j << ":\n";
        std::cout << "Position: (" << matrix[i][j]->getPosition().x << "; "
                  << matrix[i][j]->getPosition().y << ")\n";
      }
    }
  }

  return 0;
}
