#ifndef A4_MATRIX_HPP
#define A4_MATRIX_HPP

#include <memory>

#include "shape.hpp"

namespace blyshchik {
  class Matrix {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    using shapes = std::unique_ptr<shape_ptr[]>;

    Matrix();
    Matrix(const Matrix& /*l-value*/);
    Matrix(Matrix&& /*r-value*/);
    ~Matrix() = default;

    Matrix& operator =(const Matrix& /*rhs*/);
    Matrix& operator =(Matrix&& /*rhs*/);

    shapes operator [](size_t /*rhs*/) const;

    bool operator ==(const Matrix& /*rhs*/) const;
    bool operator !=(const Matrix& /*rhs*/) const;

    void add(shape_ptr /*shape*/, size_t /*line*/, size_t /*column*/);
    size_t getLines() const;
    size_t getColumns() const;


  private:
    size_t lines_;
    size_t columns_;
    shapes list_;
  };
}

#endif //A4_MATRIX_HPP
