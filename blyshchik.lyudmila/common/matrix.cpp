#include "matrix.hpp"

#include <stdexcept>
#include <utility>

blyshchik::Matrix::Matrix():
  lines_(0),
  columns_(0)
{
}

blyshchik::Matrix::Matrix(const Matrix &source):
  lines_(source.lines_),
  columns_(source.columns_),
  list_(std::make_unique<shape_ptr[]>(source.lines_ * source.columns_))
{
  for (size_t i = 0; i < (lines_ * columns_); i++) {
    list_[i] = source.list_[i];
  }
}

blyshchik::Matrix::Matrix(Matrix &&source):
  lines_(source.lines_),
  columns_(source.columns_),
  list_(std::move(source.list_))
{
}

blyshchik::Matrix& blyshchik::Matrix::operator =(const Matrix &rhs)
{
  if (this != &rhs) {
    lines_ = rhs.lines_;
    columns_ = rhs.columns_;
    shapes tmpList(std::make_unique<shape_ptr[]>(rhs.lines_ * rhs.columns_));
    for (size_t i = 0; i < (lines_ * columns_); i++) {
      tmpList[i] = rhs.list_[i];
    }
    list_.swap(tmpList);
  }

  return *this;
}

blyshchik::Matrix& blyshchik::Matrix::operator =(Matrix &&rhs)
{
  if (this != &rhs) {
    lines_ = rhs.lines_;
    columns_ = rhs.columns_;
    list_ = std::move(rhs.list_);
  }

  return *this;
}

blyshchik::Matrix::shapes blyshchik::Matrix::operator [](size_t rhs) const
{
  if (rhs >= lines_) {
    throw std::out_of_range("Index is out of range");
  }

  shapes tmpList(std::make_unique<shape_ptr[]>(columns_));
  for (size_t i = 0; i < columns_; i++) {
    tmpList[i] = list_[rhs * columns_ + i];
  }

  return tmpList;
}

bool blyshchik::Matrix::operator ==(const Matrix& rhs) const
{
  if ((lines_ != rhs.lines_) || (columns_ != rhs.columns_)) {
    return false;
  }

  for (size_t i = 0; i < (lines_ * columns_); i++) {
    if (list_[i] != rhs.list_[i]) {
      return false;
    }
  }

  return true;
}

bool blyshchik::Matrix::operator !=(const Matrix& rhs) const
{
  return !(*this == rhs);
}

void blyshchik::Matrix::add(shape_ptr shape, size_t line, size_t column)
{
  size_t tmpLines = (line == lines_) ? (lines_ + 1) : (lines_);
  size_t tmpColumns = (column == columns_) ? (columns_ + 1) : (columns_);

  shapes tmpList(std::make_unique<shape_ptr[]>(tmpLines * tmpColumns));

  for (size_t i = 0; i < tmpLines; i++) {
    for (size_t j = 0; j < tmpColumns; j++) {
      if ((i == lines_) || (j == columns_)) {
        tmpList[i * tmpColumns + j] = nullptr;
      }
      else {
        tmpList[i * tmpColumns + j] = list_[i * columns_ + j];
      }
    }
  }
  tmpList[line * tmpColumns + column] = shape;
  list_.swap(tmpList);
  lines_ = tmpLines;
  columns_ = tmpColumns;
}

size_t blyshchik::Matrix::getLines() const
{
  return lines_;
}

size_t blyshchik::Matrix::getColumns() const
{
  return columns_;
}
