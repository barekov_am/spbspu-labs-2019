#include "boost/test/auto_unit_test.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

using shape_ptr = std::shared_ptr<blyshchik::Shape>;
const double accuracy = 0.01;

BOOST_AUTO_TEST_SUITE(testCompositeShape)

BOOST_AUTO_TEST_CASE(testCompAfterMove)
{
  blyshchik::CompositeShape testComp;
  shape_ptr circ1 = std::make_shared<blyshchik::Circle>(10, 20, 3);
  shape_ptr circ2 = std::make_shared<blyshchik::Circle>(-3, -7, 3);
  shape_ptr rect1 = std::make_shared<blyshchik::Rectangle>(-4, 1, 3, 4);
  shape_ptr rect2 = std::make_shared<blyshchik::Rectangle>(0.3, 2, 0.8, 2);

  testComp.add(circ1);
  testComp.add(circ2);
  testComp.add(rect1);
  testComp.add(rect2);

  const blyshchik::point_t newPos{10, 5};
  const double testArea = testComp.getArea();
  const blyshchik::rectangle_t testFrameRect = testComp.getFrameRect();

  testComp.move(newPos);

  BOOST_CHECK_CLOSE(testComp.getFrameRect().width, testFrameRect.width, accuracy);
  BOOST_CHECK_CLOSE(testComp.getFrameRect().height, testFrameRect.height, accuracy);
  BOOST_CHECK_CLOSE(testComp.getArea(), testArea, accuracy);
}

BOOST_AUTO_TEST_CASE(testCompAfterScale)
{
  blyshchik::CompositeShape testComp;
  shape_ptr circ1 = std::make_shared<blyshchik::Circle>(10, 20, 3);
  shape_ptr circ2 = std::make_shared<blyshchik::Circle>(-3, -7, 3);
  shape_ptr rect1 = std::make_shared<blyshchik::Rectangle>(-4, 1, 3, 4);
  shape_ptr rect2 = std::make_shared<blyshchik::Rectangle>(0.3, 2, 0.8, 2);

  testComp.add(circ1);
  testComp.add(circ2);
  testComp.add(rect1);
  testComp.add(rect2);

  const double coefficient = 2;
  blyshchik::rectangle_t testFrameRect = testComp.getFrameRect();
  const double newWidth = testFrameRect.width * coefficient;
  const double newHeight = testFrameRect.height * coefficient;
  const double testArea = testComp.getArea();
  const double newArea = testArea * coefficient * coefficient;

  testComp.scale(coefficient);
  BOOST_CHECK_CLOSE(testComp.getFrameRect().width, newWidth, accuracy);
  BOOST_CHECK_CLOSE(testComp.getFrameRect().height, newHeight, accuracy);
  BOOST_CHECK_CLOSE(testComp.getArea(), newArea, accuracy);
}

BOOST_AUTO_TEST_CASE(testCompAfterRotate)
{
  blyshchik::CompositeShape testComp;
  shape_ptr circ1 = std::make_shared<blyshchik::Circle>(10, 20, 3);
  shape_ptr rect1 = std::make_shared<blyshchik::Rectangle>(-4, 1, 3, 4);

  testComp.add(circ1);
  testComp.add(rect1);

  double area = testComp.getArea();
  double angle = 46.4;
  testComp.rotate(angle);
  BOOST_CHECK_CLOSE(area, testComp.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(testCompAddAndRemove)
{
  blyshchik::CompositeShape testComp;
  shape_ptr circ1 = std::make_shared<blyshchik::Circle>(10, 20, 3);
  shape_ptr circ2 = std::make_shared<blyshchik::Circle>(-3, -7, 3);
  shape_ptr rect1 = std::make_shared<blyshchik::Rectangle>(-4, 1, 3, 4);
  shape_ptr rect2 = std::make_shared<blyshchik::Rectangle>(0.3, 2, 0.8, 2);

  testComp.add(circ1);
  testComp.add(circ2);
  testComp.add(rect1);

  double testCompAreaBeforeAdding = testComp.getArea();
  double testCirc1Area = circ1->getArea();
  double testRect2Area = rect2->getArea();

  testComp.add(rect2);
  double testCompAreaAfterAdding = testComp.getArea();
  BOOST_CHECK_CLOSE(testCompAreaBeforeAdding + testRect2Area, testCompAreaAfterAdding, accuracy);

  testComp.remove(0);
  BOOST_CHECK_CLOSE(testCompAreaAfterAdding - testCirc1Area, testComp.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(testCompParametres)
{
  blyshchik::CompositeShape testComp;
  BOOST_CHECK_THROW(testComp.remove(0), std::out_of_range);

  shape_ptr circ1 = std::make_shared<blyshchik::Circle>(10, 20, 3);
  shape_ptr circ2 = std::make_shared<blyshchik::Circle>(-3, -7, 3);
  shape_ptr rect1 = std::make_shared<blyshchik::Rectangle>(-4, 1, 3, 4);
  shape_ptr rect2 = std::make_shared<blyshchik::Rectangle>(0.3, 2, 0.8, 2);

  testComp.add(circ1);
  testComp.add(circ2);
  testComp.add(rect1);
  testComp.add(rect2);

  BOOST_CHECK_THROW(testComp.scale(-2), std::out_of_range);
  BOOST_CHECK_THROW(testComp.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(testComp[10], std::out_of_range);
  BOOST_CHECK_THROW(testComp.rotate(0.0), std::invalid_argument);
  BOOST_CHECK_THROW(testComp.rotate(543.2), std::out_of_range);
  BOOST_CHECK_THROW(testComp.rotate(-215.1), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(testCompCopyAndMoveConstructors)
{
  shape_ptr circ1 = std::make_shared<blyshchik::Circle>(10, 20, 3);
  shape_ptr circ2 = std::make_shared<blyshchik::Circle>(-3, -7, 3);
  shape_ptr rect1 = std::make_shared<blyshchik::Rectangle>(-4, 1, 3, 4);
  shape_ptr rect2 = std::make_shared<blyshchik::Rectangle>(0.3, 2, 0.8, 2);

  blyshchik::CompositeShape testComp1;
  testComp1.add(circ1);
  testComp1.add(rect1);
  blyshchik::CompositeShape testCompCopy(testComp1);

  BOOST_CHECK_CLOSE(testComp1.getArea(), testCompCopy.getArea(), accuracy);
  BOOST_CHECK_EQUAL(testComp1.getSize(), testCompCopy.getSize());


  blyshchik::CompositeShape testComp2;
  testComp2.add(circ2);
  testComp2.add(rect2);
  double areaTestComp2 = testComp2.getArea();
  unsigned int sizeTestComp2 = testComp2.getSize();
  blyshchik::CompositeShape testCompMove(std::move(testComp2));

  BOOST_CHECK_CLOSE(areaTestComp2, testCompMove.getArea(), accuracy);
  BOOST_CHECK_EQUAL(sizeTestComp2, testCompMove.getSize());
  BOOST_CHECK_EQUAL(testComp2.getSize(), 0);
}

BOOST_AUTO_TEST_CASE(testCompAssignmentOperators)
{
  shape_ptr circ1 = std::make_shared<blyshchik::Circle>(10, 20, 3);
  shape_ptr circ2 = std::make_shared<blyshchik::Circle>(-3, -7, 3);
  shape_ptr rect1 = std::make_shared<blyshchik::Rectangle>(-4, 1, 3, 4);
  shape_ptr rect2 = std::make_shared<blyshchik::Rectangle>(0.3, 2, 0.8, 2);

  blyshchik::CompositeShape testComp;
  testComp.add(circ1);
  testComp.add(rect1);
  blyshchik::CompositeShape copyTestComp;
  copyTestComp.add(circ2);
  copyTestComp = testComp;

  BOOST_CHECK_CLOSE(testComp.getArea(), copyTestComp.getArea(), accuracy);
  BOOST_CHECK_EQUAL(testComp.getSize(), copyTestComp.getSize());


  blyshchik::CompositeShape moveTestComp;
  moveTestComp.add(rect2);
  double areaTestComp = testComp.getArea();
  unsigned int sizeTestComp = testComp.getSize();
  moveTestComp = std::move(testComp);

  BOOST_CHECK_CLOSE(areaTestComp, moveTestComp.getArea(), accuracy);
  BOOST_CHECK_EQUAL(sizeTestComp, moveTestComp.getSize());
  BOOST_CHECK_EQUAL(testComp.getSize(), 0);

  moveTestComp = std::move(moveTestComp);
  BOOST_CHECK_CLOSE(areaTestComp, moveTestComp.getArea(), accuracy);
  BOOST_CHECK_EQUAL(sizeTestComp, moveTestComp.getSize());
}

BOOST_AUTO_TEST_SUITE_END()
