#include "boost/test/auto_unit_test.hpp"
#include "circle.hpp"

const double accuracy = 0.01;

BOOST_AUTO_TEST_SUITE(testCircle)

BOOST_AUTO_TEST_CASE(testCircMove)
{
  const blyshchik::point_t pos{1, 2};
  const double radius = 3;
  const double newX = 1;
  const double newY = 2;

  blyshchik::Circle testCirc{pos, radius};
  const double area = testCirc.getArea();

  testCirc.move(newX, newY);

  BOOST_CHECK_CLOSE(testCirc.getRadius(), radius, accuracy);
  BOOST_CHECK_CLOSE(testCirc.getArea(), area, accuracy);
}

BOOST_AUTO_TEST_CASE(testCircScale)
{
  blyshchik::Circle testCirc{{1, 2}, 3};
  const double coefficient = 1.3;
  const double newArea = testCirc.getArea() * coefficient * coefficient;

  testCirc.scale(coefficient);

  BOOST_CHECK_CLOSE(testCirc.getArea(), newArea, accuracy);
}

BOOST_AUTO_TEST_CASE(testCircRotate)
{
  blyshchik::Circle testCirc{{1, 2}, 3};
  double area = testCirc.getArea();
  double angle = 36.7;

  testCirc.rotate(angle);

  BOOST_CHECK_CLOSE(area, testCirc.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(testCircParameters)
{
  BOOST_CHECK_THROW(blyshchik::Circle testCirc({1, 2}, 0), std::invalid_argument);
  blyshchik::Circle testCircScale({1, 2}, 3);
  BOOST_CHECK_THROW(testCircScale.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
