#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

using shape_ptr = std::shared_ptr<blyshchik::Shape>;
using shapes = std::unique_ptr<shape_ptr[]>;

namespace blyshchik {
  class CompositeShape : public Shape {
  public:

    CompositeShape();
    CompositeShape(const CompositeShape& /*composite shape l-value*/); //copy constructor
    CompositeShape(CompositeShape&& /*composite shape r-value*/) noexcept; //move constructor

    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape& /*rhs*/); //copy assignment operator
    CompositeShape &operator =(CompositeShape&& /*rhs*/) noexcept; //move assignment operator

    shape_ptr operator [](size_t /*rhs*/) const;
    bool operator ==(const CompositeShape& /*rhs*/) const;
    bool operator !=(const CompositeShape& /*rhs*/) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    size_t getSize() const;
    void move(const point_t /*newCentre*/) override;
    void move(double /*newX*/, double /*newY*/) override;
    void print() const override;
    point_t getPosition() const override;
    void scale(double /*coefficient*/) override;
    void rotate(double /*angle*/) override;

    void add(shape_ptr /*newFigure*/);
    void remove(size_t /*index*/);
    shapes showShapes() const;

  private:
    size_t size_;
    shapes figures_;
  };
}

#endif //COMPOSITE_SHAPE_HPP
