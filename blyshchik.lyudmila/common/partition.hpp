#ifndef A4_PARTITION_HPP
#define A4_PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

using shape_ptr = std::shared_ptr<blyshchik::Shape>;
using shapes = std::unique_ptr<shape_ptr[]>;

namespace blyshchik {
  Matrix part(const shapes& /*arr*/, size_t /*size*/);
  Matrix part(const CompositeShape& /*comp*/);
}

#endif //A4_PARTITION_HPP
