#include "composite-shape.hpp"
#include <iostream>
#include <cassert>
#include <cmath>
#include <stdexcept>

blyshchik::CompositeShape::CompositeShape():
  size_(0)
{
}

blyshchik::CompositeShape::CompositeShape(const blyshchik::CompositeShape &rhs):
  size_(rhs.size_),
  figures_(std::make_unique<shape_ptr[]>(rhs.size_))
{
  for (size_t i = 0; i < size_; i++) {
    figures_[i] = rhs.figures_[i];
  }
}

blyshchik::CompositeShape::CompositeShape(blyshchik::CompositeShape &&rhs) noexcept:
  size_(rhs.size_),
  figures_(std::move(rhs.figures_))
{
  rhs.size_ = 0;
}

blyshchik::CompositeShape &blyshchik::CompositeShape::operator =(const CompositeShape &rhs)
{
  if (this != &rhs) {
    size_ = rhs.size_;
    shapes tmp(std::make_unique<shape_ptr[]>(rhs.size_));

    for (size_t i = 0; i < size_; i++) {
      tmp[i] = rhs.figures_[i];
    }
    figures_.swap(tmp);
  }
  return *this;
}

blyshchik::CompositeShape &blyshchik::CompositeShape::operator =(CompositeShape &&rhs) noexcept
{
  if (this != &rhs) {
    size_ = rhs.size_;
    rhs.size_ = 0;
    figures_ = std::move(rhs.figures_);
  }
  return *this;
}

shape_ptr blyshchik::CompositeShape::operator [](size_t rhs) const
{
  if (rhs >= size_) {
    throw std::out_of_range("Index must not be more than size. ");
  }
  return figures_[rhs];
}

bool blyshchik::CompositeShape::operator ==(const CompositeShape &rhs) const
{
  if (size_ != rhs.size_)
  {
    return false;
  }

  for (size_t i = 0; i < size_; i++)
  {
    if (figures_[i] != rhs.figures_[i])
    {
      return false;
    }
  }

  return true;
}

bool blyshchik::CompositeShape::operator !=(const CompositeShape &rhs) const
{
  return !(*this == rhs);
}

double blyshchik::CompositeShape::getArea() const
{
  double compositeArea = 0;
  for (size_t i = 0; i < size_; i++) {
    compositeArea += figures_[i]->getArea();
  }
  return compositeArea;
}

blyshchik::rectangle_t blyshchik::CompositeShape::getFrameRect() const
{
  if (size_ == 0) {
    return {{0, 0}, 0, 0};
  }
  rectangle_t tmpArr = figures_[0]->getFrameRect();
  double ltX = tmpArr.pos.x - tmpArr.width / 2; //bottom left frame corner
  double ltY = tmpArr.pos.y - tmpArr.height / 2;
  double rtX = tmpArr.pos.x + tmpArr.width / 2; //upper right frame corner
  double rtY = tmpArr.pos.y + tmpArr.height / 2;
  for (size_t i = 1; i < size_; i++) {
    rectangle_t tmp = figures_[i]->getFrameRect();
    ltX = std::fmin(tmp.pos.x - tmp.width / 2, ltX);
    ltY = std::fmin(tmp.pos.y - tmp.height / 2, ltY);
    rtX = std::fmax(tmp.pos.x + tmp.width / 2, rtX);
    rtY = std::fmax(tmp.pos.y + tmp.height / 2, rtY);
  }
  return {{(rtX - ltX) / 2, (rtY + ltY) / 2}, rtX - ltX, rtY - ltY};
}

size_t blyshchik::CompositeShape::getSize() const
{
  return size_;
}

void blyshchik::CompositeShape::move(const blyshchik::point_t newCentre)
{
  const double dx = newCentre.x - getFrameRect().pos.x;
  const double dy = newCentre.y - getFrameRect().pos.y;
  for (size_t i = 0; i < size_; i++) {
    figures_[i]->move(dx, dy);
  }
}

void blyshchik::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; i++) {
    figures_[i]->move(dx, dy);
  }
}

void blyshchik::CompositeShape::print() const
{
  std::cout << "\nHere is composite shape. Quantity of figures is " << getSize()
            << ", coordinates of centre (" << getPosition().x << ", "
            << getPosition().y << "), \nits area = " << getArea()
            << ", frame: height = " << getFrameRect().height
            << ", width = " << getFrameRect().width << ".\n";
}

blyshchik::point_t blyshchik::CompositeShape::getPosition() const
{
  return getFrameRect().pos;
}

void blyshchik::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0.0) {
    throw std::out_of_range("Coefficient must be positive number. ");
  }
  const point_t centre = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++) {
    const double dx = figures_[i]->getFrameRect().pos.x - centre.x;
    const double dy = figures_[i]->getFrameRect().pos.y - centre.y;
    figures_[i]->move({centre.x + dx * coefficient, centre.y + dy * coefficient});
    figures_[i]->scale(coefficient);
  }
}

void blyshchik::CompositeShape::rotate(double angle)
{
  const double semicircle = 180;
  double sinA = std::sin(angle * M_PI / semicircle);
  double cosA = std::cos(angle * M_PI / semicircle);
  point_t oldCentre = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++) {
    point_t newCentre = figures_[i]->getFrameRect().pos;
    figures_[i]->move((newCentre.x - oldCentre.x) * cosA - (newCentre.y - oldCentre.y) * sinA,
        (newCentre.x - oldCentre.x) * sinA + (newCentre.y - oldCentre.y) * cosA);
    figures_[i]->rotate(angle);
  }
}

void blyshchik::CompositeShape::add(shape_ptr newFigure)
{
  if (newFigure == nullptr) {
    throw std::invalid_argument("Point of figure must not be nul. ");
  }

  shapes tmpFigures(std::make_unique<shape_ptr[]>(size_ + 1));
  for (size_t i = 0; i < size_; i++) {
    tmpFigures[i] = figures_[i];
  }
  tmpFigures[size_] = newFigure;
  size_++;
  figures_.swap(tmpFigures);
}

void blyshchik::CompositeShape::remove(size_t index)
{
  if (size_ == 0) {
    throw std::out_of_range("There is nothing to delete. ");
  }
  for (size_t i = index; i < size_ - 1; i++) {
    figures_[i] = figures_[i + 1];
  }
  size_--;
}

shapes blyshchik::CompositeShape::showShapes() const
{
  shapes tmpArray(std::make_unique<shape_ptr[]>(size_));
  for (size_t i = 0; i < size_; i++)
  {
    tmpArray[i] = figures_[i];
  }

  return tmpArray;
}
