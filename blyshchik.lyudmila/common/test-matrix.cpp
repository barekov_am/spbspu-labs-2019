#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>
#include <utility>

#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include "rectangle.hpp"

using shape_ptr = std::shared_ptr<blyshchik::Shape>;

BOOST_AUTO_TEST_SUITE(testMatrix)

  BOOST_AUTO_TEST_CASE(copyAndMove)
  {
    shape_ptr testCircle = std::make_shared<blyshchik::Circle>(4, 2, 3);
    blyshchik::CompositeShape testComposite;
    testComposite.add(testCircle);

    blyshchik::Matrix testMatrix = blyshchik::part(testComposite);

    BOOST_CHECK_NO_THROW(blyshchik::Matrix testMatrix2(testMatrix));
    BOOST_CHECK_NO_THROW(blyshchik::Matrix testMatrix2(std::move(testMatrix)));

    blyshchik::Matrix testMatrix2 = blyshchik::part(testComposite);
    blyshchik::Matrix testMatrix3;

    BOOST_CHECK_NO_THROW(testMatrix3 = testMatrix2);
    BOOST_CHECK_NO_THROW(testMatrix3 = std::move(testMatrix2));

    blyshchik::Matrix testMatrix4 = blyshchik::part(testComposite);
    blyshchik::Matrix testMatrix5;

    testMatrix5 = testMatrix4;
    BOOST_CHECK(testMatrix5 == testMatrix4);
    testMatrix5 = std::move(testMatrix4);
    BOOST_CHECK(testMatrix5 == testMatrix3);

    blyshchik::Matrix testMatrix6(testMatrix3);
    BOOST_CHECK(testMatrix6 == testMatrix3);
    blyshchik::Matrix testMatrix7(std::move(testMatrix3));
    BOOST_CHECK(testMatrix7 == testMatrix6);
  }

  BOOST_AUTO_TEST_CASE(exceptions)
  {
    shape_ptr testCircle = std::make_shared<blyshchik::Circle>(-4, 5, 1.5);
    shape_ptr testRectangle = std::make_shared<blyshchik::Rectangle>(-2, 3.5, 1, 6);
    blyshchik::CompositeShape testComposite;
    testComposite.add(testCircle);
    testComposite.add(testRectangle);

    blyshchik::Matrix testMatrix = blyshchik::part(testComposite);

    BOOST_CHECK_THROW(testMatrix[3], std::out_of_range);
    BOOST_CHECK_THROW(testMatrix[-1], std::out_of_range);
  }

BOOST_AUTO_TEST_SUITE_END()
