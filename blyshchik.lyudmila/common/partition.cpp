#include "partition.hpp"

blyshchik::Matrix blyshchik::part(const shapes &arr, size_t size)
{
  Matrix matrix;

  for (size_t i = 0; i < size; i++) {
    size_t rightLine = 0;
    size_t rightColumn = 0;
    for (size_t j = 0; j < matrix.getLines(); j++) {
      for (size_t k = 0; k < matrix.getColumns(); k++) {
        if (matrix[j][k] == nullptr)
        {
          rightLine = j;
          rightColumn = k;
          break;
        }

        if (intersect(arr[i]->getFrameRect(), matrix[j][k]->getFrameRect())) {
          rightLine = j + 1;
          rightColumn = 0;
          break;
        }
        else {
          rightLine = j;
          rightColumn = k + 1;
        }
      }

      if (rightLine == j) {//right line founded
        break;
      }
    }

    matrix.add(arr[i], rightLine, rightColumn);
  }

  return matrix;
}

blyshchik::Matrix blyshchik::part(const CompositeShape &comp)
{
  return part(comp.showShapes(), comp.getSize());
}
