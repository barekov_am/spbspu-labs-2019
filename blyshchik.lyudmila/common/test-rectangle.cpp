#include "boost/test/auto_unit_test.hpp"
#include "rectangle.hpp"

const double accuracy = 0.01;

BOOST_AUTO_TEST_SUITE(testRectangle)

BOOST_AUTO_TEST_CASE(testRectMove)
{
  blyshchik::Rectangle testRect({{1, 2}, 3, 4});
  const blyshchik::rectangle_t rectFrameBeforeMove = testRect.getFrameRect();
  const double area = testRect.getArea();

  testRect.move(7, 3);

  BOOST_CHECK_CLOSE(testRect.getWidth(), rectFrameBeforeMove.width, accuracy);
  BOOST_CHECK_CLOSE(testRect.getHeight(), rectFrameBeforeMove.height, accuracy);
  BOOST_CHECK_CLOSE(testRect.getArea(), area, accuracy);
}

BOOST_AUTO_TEST_CASE(testRectScale)
{
  blyshchik::Rectangle testRect({{1, 2}, 3, 4});
  const double coefficient = 2;
  const double newArea = testRect.getArea() * coefficient * coefficient;

  testRect.scale(coefficient);

  BOOST_CHECK_CLOSE(testRect.getArea(), newArea, accuracy);
}

BOOST_AUTO_TEST_CASE(testRectRotate)
{
  blyshchik::Rectangle testRect1({{1, 2}, 3, 4});
  double angle = 24.6;
  double area = testRect1.getArea();
  testRect1.rotate(angle);
  BOOST_CHECK_CLOSE(testRect1.getArea(), area, accuracy);

  blyshchik::Rectangle testRect2({{1, 2}, 3, 4});
  angle = 0.0;
  BOOST_CHECK_THROW(testRect2.rotate(angle), std::invalid_argument);

  blyshchik::Rectangle testRect3({{1, 2}, 3, 4});
  angle = 394.6;
  BOOST_CHECK_THROW(testRect3.rotate(angle), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(testRectParameters)
{
  const blyshchik::point_t pos{1, 2};
  const double corWidth = 3;
  const double incorWidth = -3;
  const double corHeight = 4;
  const double incorHeight = 0;
  const double incorCoefficient = -2;

  blyshchik::Rectangle testRect{pos, corWidth, corHeight};

  BOOST_CHECK_THROW(blyshchik::Rectangle incorWidthRect(pos, incorWidth, corHeight), std::invalid_argument);
  BOOST_CHECK_THROW(blyshchik::Rectangle incorHeightRect(pos, corWidth, incorHeight), std::invalid_argument);
  BOOST_CHECK_THROW(blyshchik::Rectangle incorWidthAndHeightRect(pos, incorWidth, incorHeight), std::invalid_argument);
  BOOST_CHECK_THROW(testRect.scale(incorCoefficient), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
