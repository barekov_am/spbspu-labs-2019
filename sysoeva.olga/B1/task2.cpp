#include <fstream>
#include <iostream>
#include <memory>
#include <vector>

void task2(const char* file)
{
  std::ifstream input(file);

  if (!input)
  {
    throw std::ios_base::failure("File is not open");
  }

  size_t capacity = 8;
  size_t size = 0;

  std::unique_ptr<char[], decltype(&free)> array(static_cast<char*>(malloc(capacity)), &free);

  while (input)
  {
    input.read(&array[size], capacity - size);
    size += input.gcount();
    if (size == capacity)
    {
      capacity *= 2;
      std::unique_ptr<char[], decltype(&free)> temp(static_cast<char*>(realloc(array.get(), capacity)), &free);
      if (!temp)
      {
        throw std::ios_base::failure("Failed to reallocate memory");
      }
      else
      {
        array.release();
        std::swap(array, temp);
      }
    }
  }

  input.close();
  if (input)
  {
    throw std::ios_base::failure("Trouble with closing file");
  }

  std::vector<char> vector(&array[0], &array[size]);

  for (size_t i = 0; i != vector.size(); i++)
  {
    std::cout << vector.at(i);
  }
}
