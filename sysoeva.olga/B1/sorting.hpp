#ifndef B1_SORTING_HPP
#define B1_SORTING_HPP

#include "accessMethod.hpp"

#include <algorithm>
#include <cstring>
#include <iostream>

template <typename Element>
bool compare(Element& a, Element& b, const char* order)
{
  if (strcmp(order, "ascending") == 0)
  {
    return a > b;
  }
  else
  {
    return a < b;
  }
}

template <typename Input>
void checkOrder(Input order)
{
  if ((strcmp(order, "ascending") != 0) && (strcmp(order, "descending") != 0))
  {
    throw std::invalid_argument("Wrong argument");
  }
}

template <template <typename Container> typename AccessMethod, typename Container>
void sorting(AccessMethod<Container> method, Container &rhs, const char* order)
{
  for(auto i = method.begin(rhs); i != method.end(rhs); i++)
  {
    for(auto j = i; j != method.end(rhs); j++)
    {
      if (compare(method.get(rhs, i), method.get(rhs, j), order))
      {
        std::swap(method.get(rhs, i), method.get(rhs, j));
      }
    }
  }
}

#endif
