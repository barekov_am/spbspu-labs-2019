#ifndef B1_ACESSMETHOD_HPP
#define B1_ACESSMETHOD_HPP

#include <iostream>

template <typename Container>
struct accessBrackets
{
  size_t begin(Container &)
  {
    return 0;
  }

  size_t end(Container &rhs)
  {
    return rhs.size();
  }

  typename Container::value_type& get(Container &rhs, size_t index)
  {
    if (index > rhs.size())
    {
      throw std::invalid_argument("Out of bounds array");
    }
    return rhs[index];
  }
};

template <typename Container>
struct accessAt
{
  size_t begin(Container &)
  {
    return 0;
  }

  size_t end(Container &rhs)
  {
    return rhs.size();
  }

  typename Container::value_type& get(Container &rhs, size_t index)
  {
    return rhs.at(index);
  }
};

template <typename Container>
struct accessIterator
{
  typename Container::iterator begin(Container &rhs)
  {
    return rhs.begin();
  }

  typename Container::iterator end(Container &rhs)
  {
    return rhs.end();
  }

  typename Container::value_type& get(Container &, typename Container::iterator it)
  {
    return *it;
  }
};

#endif
