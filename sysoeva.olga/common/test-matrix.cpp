#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "composite-shape.hpp"
#include "circle.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

const double FAULT = 0.01;

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(correctnessMatrixParameters)
{
  sysoeva::Circle circle(2.5, {3.0, 4.0});
  sysoeva::Rectangle rectangle(2.0, 2.0, {20.0, 14.5});

  sysoeva::CompositeShape compositeShape(std::make_shared<sysoeva::Circle>(circle));
  compositeShape.add(std::make_shared<sysoeva::Rectangle>(rectangle));
  sysoeva::Matrix matrix = sysoeva::divide(compositeShape);

  BOOST_CHECK_EQUAL(matrix.getRows(), 1);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 2);

  sysoeva::Circle circle1(2.5, {3.0, 4.0});
  sysoeva::Rectangle rectangle1(2.0, 2.0, {1.0, 4.5});

  sysoeva::CompositeShape compositeShape1(std::make_shared<sysoeva::Circle>(circle1));
  compositeShape1.add(std::make_shared<sysoeva::Rectangle>(rectangle1));
  sysoeva::Matrix matrix1 = sysoeva::divide(compositeShape1);

  BOOST_CHECK_EQUAL(matrix1.getRows(), 2);
  BOOST_CHECK_EQUAL(matrix1.getColumns(), 1);
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  sysoeva::Circle circle(2.5, {3.2, 4.5});
  sysoeva::Triangle triangle({2.3, 6.7}, {7.8, 3.4}, {4.5, 5.6});

  sysoeva::CompositeShape compositeShape(std::make_shared<sysoeva::Circle>(circle));
  compositeShape.add(std::make_shared<sysoeva::Triangle>(triangle));
  sysoeva::Matrix matrix = sysoeva::divide(compositeShape);

  BOOST_CHECK_THROW(matrix[5][5], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(constructorCopyAndMove)
{
  sysoeva::Circle circle(2.5, {3.2, 4.5});
  sysoeva::CompositeShape compositeShape(std::make_shared<sysoeva::Circle>(circle));
  sysoeva::Matrix matrix = sysoeva::divide(compositeShape);

  BOOST_CHECK_NO_THROW(sysoeva::Matrix copiedMatrix(matrix));
  BOOST_CHECK_NO_THROW(sysoeva::Matrix movedMatrix(std::move(matrix)));

  sysoeva::Rectangle rectangle(2, 2, {1.5, 7.5});
  sysoeva::CompositeShape compositeShape1(std::make_shared<sysoeva::Rectangle>(rectangle));
  sysoeva::Matrix matrix1 = sysoeva::divide(compositeShape1);

  sysoeva::Matrix tempMatrix1;
  sysoeva::Matrix tempMatrix2;

  BOOST_CHECK_NO_THROW(tempMatrix1 = matrix1);
  BOOST_CHECK_NO_THROW(tempMatrix2 = std::move(matrix1));
}

BOOST_AUTO_TEST_SUITE_END()
