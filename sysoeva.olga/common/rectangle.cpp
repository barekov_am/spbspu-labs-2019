#define _USE_MATH_DEFINES
#include "rectangle.hpp"

#include <cmath>
#include <iostream>
#include <stdexcept>

sysoeva::Rectangle::Rectangle(double width, double height, const point_t &center):
  angle_(0),
  width_(width),
  height_(height),
  center_(center)
{
  if (width_ <= 0)
  {
    throw std::invalid_argument("Width has invalid value");
  }
  if (height_ <= 0)
  {
    throw std::invalid_argument("Height has invalid value");
  }
}

double sysoeva::Rectangle::getArea() const
{
  return width_ * height_;
}

sysoeva::rectangle_t sysoeva::Rectangle::getFrameRect() const
{
  double width = height_ * fabs(sin((angle_ * M_PI) / 180)) + width_ * fabs(cos((angle_ * M_PI) / 180));
  double height = height_ * fabs(cos((angle_ * M_PI) / 180)) + width_ * fabs(sin((angle_ * M_PI) / 180));
  return {width, height, center_};
}

void sysoeva::Rectangle::move(const point_t &point)
{
  center_ = point;
}

void sysoeva::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void sysoeva::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient has invalid value");
  }
  width_ *= coefficient;
  height_ *= coefficient;
}

void sysoeva::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
