#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace sysoeva
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(double, double, const point_t &);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(double, double) override;
    void scale(double) override;
    void rotate(double) override;
  private:
    double angle_;
    double width_;
    double height_;
    point_t center_;
  };
}

#endif
