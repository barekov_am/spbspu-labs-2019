#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace sysoeva
{
  class Circle : public Shape
  {
  public:
    Circle(double, const point_t &);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(double, double) override;
    void scale(double) override;
    void rotate(double) override;
  private:
    double radius_;
    point_t center_;
  };
}

#endif
