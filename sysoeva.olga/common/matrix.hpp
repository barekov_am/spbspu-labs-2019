#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

namespace sysoeva
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &);
    Matrix(Matrix &&);

    ~Matrix() = default;

    Matrix &operator =(const Matrix &);
    Matrix &operator =(Matrix &&);
    array_ptr operator [](size_t) const;
    bool operator ==(const Matrix &) const;
    bool operator !=(const Matrix &) const;

    void add(const shape_ptr &, size_t row, size_t col);

    size_t getRows() const;
    size_t getColumns() const;
    size_t getLayerSize(size_t) const;
    void printMatrix();
  private:
    size_t row_;
    size_t col_;
    array_ptr shapes_;
  };
}

#endif
