#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

const double FAULT = 0.01;

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(invarianceRectParametersAfterMovingTo)
{
  sysoeva::Rectangle tempRectangle(5.2, 6.5, {4.0, 3.0});
  const double initialArea = tempRectangle.getArea();
  const sysoeva::rectangle_t initialRectangle = tempRectangle.getFrameRect();

  tempRectangle.move({14.3, 5.2});

  BOOST_CHECK_CLOSE(initialRectangle.width, tempRectangle.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(initialRectangle.height, tempRectangle.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(initialArea, tempRectangle.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(invarianceRectParametersAfterMovingBy)
{
  sysoeva::Rectangle tempRectangle(5.2, 6.5, {4.0, 3.0});
  const double initialArea = tempRectangle.getArea();
  const sysoeva::rectangle_t initialRectangle = tempRectangle.getFrameRect();

  tempRectangle.move(3.4, 7.5);

  BOOST_CHECK_CLOSE(initialRectangle.width, tempRectangle.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(initialRectangle.height, tempRectangle.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(initialArea, tempRectangle.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterIncreasing)
{
  sysoeva::Rectangle tempRectangle(5.2, 6.5, {4.0, 3.0});
  const double initialArea = tempRectangle.getArea();
  double coefficient = 2.0;

  tempRectangle.scale(coefficient);

  BOOST_CHECK_CLOSE(initialArea * coefficient * coefficient, tempRectangle.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterDecreasing)
{
  sysoeva::Rectangle tempRectangle(5.2, 6.5, {4.0, 3.0});
  const double initialArea = tempRectangle.getArea();
  double coefficient = 0.7;

  tempRectangle.scale(coefficient);

  BOOST_CHECK_CLOSE(initialArea * coefficient * coefficient, tempRectangle.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterRotating)
{
  sysoeva::Rectangle tempRectangle(5.2, 6.5, {4.0, 3.0});
  const double initialArea = tempRectangle.getArea();
  double angle = 70;

  tempRectangle.rotate(angle);

  BOOST_CHECK_CLOSE(initialArea, tempRectangle.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  BOOST_CHECK_THROW(sysoeva::Rectangle rectangle(0, 6.5, {4.0, 3.0}), std::invalid_argument);
  BOOST_CHECK_THROW(sysoeva::Rectangle rectangle(5.2, 0, {4.0, 3.0}), std::invalid_argument);
  BOOST_CHECK_THROW(sysoeva::Rectangle rectangle(-5.2, 6.5, {4.0, 3.0}), std::invalid_argument);
  BOOST_CHECK_THROW(sysoeva::Rectangle rectangle(5.2, -6.5, {4.0, 3.0}), std::invalid_argument);

  sysoeva::Rectangle tempRectangle(10.0, 12.0, {12.5, 9.5});

  BOOST_CHECK_THROW(tempRectangle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(tempRectangle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
