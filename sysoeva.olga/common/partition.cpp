#define _USE_MATH_DEFINES
#include "partition.hpp"

#include <cmath>

bool sysoeva::intersect(sysoeva::rectangle_t shape_1, sysoeva::rectangle_t shape_2)
{
  const bool firstCondition = (fabs(shape_1.pos.x - shape_2.pos.x) < (shape_1.width + shape_2.width) / 2);
  const bool secondCondition = (fabs(shape_1.pos.y - shape_2.pos.y) < (shape_1.height + shape_2.height) / 2);

  return firstCondition && secondCondition;
}

sysoeva::Matrix sysoeva::divide(array_ptr &shapes, size_t size)
{
  Matrix temp_matrix;

  for (size_t i = 0; i < size; i++)
  {
    size_t row = 0;
    size_t col = 0;
    for (size_t j = 0; j < temp_matrix.getRows(); j++)
    {
      for (size_t k = 0; k < temp_matrix.getLayerSize(j); k++)
      {
        if (intersect(shapes[i]->getFrameRect(), temp_matrix[j][k]->getFrameRect()))
        {
          row = temp_matrix.getRows();
          break;
        }
        if (k == temp_matrix.getLayerSize(j) - 1)
        {
          row = j;
          col = temp_matrix.getLayerSize(j);
        }
      }
      if (row == j)
      {
        break;
      }
    }
    temp_matrix.add(shapes[i], row, col);
  }
  return temp_matrix;
}

sysoeva::Matrix sysoeva::divide(sysoeva::CompositeShape &composite_shape)
{
  array_ptr temp = std::make_unique<shape_ptr[]> (composite_shape.getSize());
  for (size_t i = 0; i < composite_shape.getSize(); i++)
  {
    temp[i] = composite_shape[i];
  }

  Matrix matrix = divide(temp, composite_shape.getSize());
  return matrix;
}
