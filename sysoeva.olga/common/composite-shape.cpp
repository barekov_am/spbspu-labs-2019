#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include "shape.hpp"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <stdexcept>

sysoeva::CompositeShape::CompositeShape():
  size_(0)
{
}

sysoeva::CompositeShape::CompositeShape(const CompositeShape &shape):
  size_(shape.size_),
  shapes_(std::make_unique<shape_ptr[]> (shape.size_))
{
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i] = shape.shapes_[i];
  }
}

sysoeva::CompositeShape::CompositeShape(CompositeShape &&shape):
  size_(shape.size_),
  shapes_(std::move(shape.shapes_))
{
  if (!shapes_)
  {
    throw std::invalid_argument("Pointer to shapes is invalid");
  }
}

sysoeva::CompositeShape::CompositeShape(const shape_ptr &shape):
  size_(1),
  shapes_(std::make_unique<shape_ptr[]> (size_))
{
  shapes_[0] = shape;
}

sysoeva::CompositeShape &sysoeva::CompositeShape::operator =(const CompositeShape &shape)
{
  if (this != &shape)
  {
    size_ = shape.size_;
    array_ptr temp = std::make_unique<shape_ptr[]> (size_);
    for (size_t i = 0; i < size_; i++)
    {
      temp[i] = shape.shapes_[i];
    }
    shapes_ = std::move(temp);
  }
  return *this;
}

sysoeva::CompositeShape &sysoeva::CompositeShape::operator =(CompositeShape &&shape)
{
  if (this != &shape)
  {
    size_ = shape.size_;
    shapes_ = std::move(shape.shapes_);
  }
  return *this;
}

sysoeva::shape_ptr sysoeva::CompositeShape::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("There is no value!");
  }
  return shapes_[index];
}

void sysoeva::CompositeShape::add(const shape_ptr &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape is a null pointer!");
  }

  array_ptr temp = std::make_unique<shape_ptr[]> (size_ + 1);
  for (size_t i = 0; i < size_; i++)
  {
    temp[i] = shapes_[i];
  }
  temp[size_] = shape;
  size_ ++;
  shapes_ = std::move(temp);
}

void sysoeva::CompositeShape::remove(size_t index)
{
  if (index >= size_)
  {
    throw std::out_of_range("There is no value!");
  }

  for (size_t i = index; i < size_ - 1; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  size_ --;
}

void sysoeva::CompositeShape::remove(const shape_ptr &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape is a null pointer!");
  }

  for (size_t i = 0; i < size_; i++)
  {
    if (shapes_[i] == shape)
    {
      remove(i);
      return;
    }
  }
  size_ --;
}

double sysoeva::CompositeShape::getArea() const
{
  double full_area = 0;
  for (size_t i = 0; i < size_; i++)
  {
    full_area += shapes_[i]->getArea();
  }
  return full_area;
}

sysoeva::rectangle_t sysoeva::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    return {0, 0, {0, 0}};
  }
  rectangle_t temp = shapes_[0]->getFrameRect();

  double min_x = temp.pos.x - (temp.width / 2);
  double min_y = temp.pos.y - (temp.height / 2);
  double max_x = temp.pos.x + (temp.width / 2);
  double max_y = temp.pos.y + (temp.height / 2);

  for (size_t i = 1; i < size_; i++)
  {
    rectangle_t temp = shapes_[i]->getFrameRect();
    min_x = std::min(min_x, temp.pos.x - (temp.width / 2));
    min_y = std::min(min_y, temp.pos.y - (temp.height / 2));
    max_x = std::max(max_x, temp.pos.x + (temp.width / 2));
    max_y = std::max(max_y, temp.pos.y + (temp.height / 2));
  }
  double width = (max_x - min_x);
  double height = (max_y - min_y);
  return {(max_x - min_x), (max_y - min_y), {min_x + (width / 2), min_y + (height / 2)}};
}

void sysoeva::CompositeShape::move(const point_t &center)
{
  move(center.x - getFrameRect().pos.x, center.y - getFrameRect().pos.y);
}

void sysoeva::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void sysoeva::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient has invalid value!");
  }

  for (size_t i = 0; i < size_; i++)
  {
    rectangle_t temp = shapes_[i]->getFrameRect();
    double dx = (temp.pos.x - getFrameRect().pos.x) * coefficient;
    double dy = (temp.pos.y - getFrameRect().pos.y) * coefficient;

    shapes_[i]->move(dx, dy);
    shapes_[i]->scale(coefficient);
  }
}

size_t sysoeva::CompositeShape::getSize() const
{
  return size_;
}

void sysoeva::CompositeShape::rotate(double angle)
{
  double angle_rad = angle * M_PI / 180;
  point_t center = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    rectangle_t temp = shapes_[i]->getFrameRect();
    double new_x = (temp.pos.x - center.x) * fabs(cos(angle_rad)) - (temp.pos.y - center.y) * fabs(sin(angle_rad)) + center.x;
    double new_y = (temp.pos.x - center.x) * fabs(sin(angle_rad)) + (temp.pos.y - center.y) * fabs(cos(angle_rad)) + center.y;

    shapes_[i]->move({new_x, new_y});
    shapes_[i]->rotate(angle);
  }
}
