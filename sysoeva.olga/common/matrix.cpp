#include "matrix.hpp"

#include <stdexcept>
#include <iostream>

sysoeva::Matrix::Matrix():
  row_(0),
  col_(0)
{
}

sysoeva::Matrix::Matrix(const Matrix &rhs):
  row_(rhs.row_),
  col_(rhs.col_),
  shapes_(std::make_unique<shape_ptr[]>(rhs.row_ * rhs.col_))
{
  for (size_t i = 0; i < rhs.row_ * rhs.col_; i++)
  {
    shapes_[i] = rhs.shapes_[i];
  }
}

sysoeva::Matrix::Matrix(Matrix &&rhs):
  row_(rhs.row_),
  col_(rhs.col_),
  shapes_(std::move(rhs.shapes_))
{
  if (!shapes_)
  {
    throw std::invalid_argument("Pointer to matrix is invalid");
  }
}

sysoeva::Matrix &sysoeva::Matrix::operator =(const Matrix &rhs)
{
  if (this != &rhs)
  {
    row_ = rhs.row_;
    col_ = rhs.col_;
    array_ptr temp = std::make_unique<shape_ptr[]> (row_ * col_);
    for (size_t i = 0; i < (row_ * col_); i++)
    {
      temp[i] = rhs.shapes_[i];
    }
    shapes_ = std::move(temp);
  }
  return *this;
}

sysoeva::Matrix &sysoeva::Matrix::operator =(Matrix &&rhs)
{
  if (this != &rhs)
  {
    row_ = rhs.row_;
    col_ = rhs.col_;
    shapes_ = std::move(rhs.shapes_);
  }
  return *this;
}

sysoeva::array_ptr sysoeva::Matrix::operator [](size_t row) const
{
  if (row >= row_)
  {
    throw std::out_of_range("There is no value!");
  }

  array_ptr temp = std::make_unique<shape_ptr[]>(col_);
  for (size_t i = 0; i < getLayerSize(row); i++)
  {
    temp[i] = shapes_[row * col_ + i];
  }
  return temp;
}

bool sysoeva::Matrix::operator ==(const Matrix &rhs) const
{
  if ((rhs.row_ != row_) || (rhs.col_ != col_))
  {
    return false;
  }

  for (size_t i = 0; i < (row_ * col_); i++)
  {
    if (rhs.shapes_[i] != shapes_[i])
    {
      return false;
    }
  }

  return true;
}

bool sysoeva::Matrix::operator !=(const Matrix &rhs) const
{
  return !(*this == rhs);
}

void sysoeva::Matrix::add(const shape_ptr &rhs, size_t row, size_t col)
{
  if (!rhs)
  {
    throw std::invalid_argument("Shape is a null pointer!");
  }
  size_t new_row = (row_ == row) ? (row_ + 1) : (row_);
  size_t new_col = (col_ == col) ? (col_ + 1) : (col_);

  array_ptr temp = std::make_unique<shape_ptr[]> (new_row * new_col);
  for (size_t i = 0; i < row_; i++)
  {
    for (size_t j = 0; j < col_; j++)
    {
      temp[i * new_col + j] = shapes_[i * col_ + j];
    }
  }
  temp[row * new_col + col] = rhs;
  shapes_ = std::move(temp);
  row_ = new_row;
  col_ = new_col;
}

size_t sysoeva::Matrix::getRows() const
{
  return row_;
}

size_t sysoeva::Matrix::getColumns() const
{
  return col_;
}

size_t sysoeva::Matrix::getLayerSize(size_t row) const
{
  for (size_t i = 0; i < col_; i++)
  {
    if (shapes_[row * col_ + i] == nullptr)
    {
      return i;
    }
  }
  return col_;
}

void sysoeva::Matrix::printMatrix()
{
  std::cout << "Matrix:" << '\n';
  for (size_t i = 0; i < row_; i++)
  {
    std::cout << i + 1 << "  ";
    for (size_t j = 0; j < getLayerSize(i); j++)
    {
      std::cout << shapes_[i * col_ + j]->getArea() << " ";
    }
    std::cout <<'\n';
  }
}
