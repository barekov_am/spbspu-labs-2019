#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "composite-shape.hpp"
#include "circle.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

const double FAULT = 0.01;

BOOST_AUTO_TEST_SUITE(partitionTest)

BOOST_AUTO_TEST_CASE(correctnessIntersection)
{
  sysoeva::Circle circle(2.5, {3.0, 4.0});
  sysoeva::Rectangle rectangle(2.0, 2.0, {20.0, 14.5});
  sysoeva::Triangle triangle({2.0, 2.5}, {3.0, 4.0}, {8.5, 6.0});

  BOOST_CHECK_EQUAL(sysoeva::intersect(circle.getFrameRect(), triangle.getFrameRect()), true);
  BOOST_CHECK_EQUAL(sysoeva::intersect(circle.getFrameRect(), rectangle.getFrameRect()), false);
}

BOOST_AUTO_TEST_CASE(correctnessDivision)
{
  sysoeva::Circle circle(2.5, {3.0, 4.0});
  sysoeva::Rectangle rectangle(2.0, 2.0, {20.0, 14.5});
  sysoeva::Triangle triangle({2.0, 2.5}, {3.0, 4.0}, {8.5, 6.0});

  sysoeva::CompositeShape compositeShape(std::make_shared<sysoeva::Circle>(circle));
  compositeShape.add(std::make_shared<sysoeva::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<sysoeva::Triangle>(triangle));
  sysoeva::Matrix matrix = sysoeva::divide(compositeShape);

  BOOST_CHECK_EQUAL(matrix.getRows(), 2);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 2);
}

BOOST_AUTO_TEST_SUITE_END()
