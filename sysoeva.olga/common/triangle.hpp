#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace sysoeva
{
  class Triangle : public Shape
  {
  public:
    Triangle(point_t, point_t, point_t);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(double, double) override;
    void scale(double) override;
    void rotate(double) override;
    void showCoord();
    void printCenter();
  private:
    point_t vertex_1;
    point_t vertex_2;
    point_t vertex_3;
    point_t center_;
    point_t getCenter() const;
  };
}

#endif
