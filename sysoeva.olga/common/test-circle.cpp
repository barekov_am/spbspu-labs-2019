#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

const double FAULT = 0.01;

BOOST_AUTO_TEST_SUITE(circleTest)

BOOST_AUTO_TEST_CASE(invarianceRectParametersAfterMovingTo)
{
  sysoeva::Circle tempCircle(2.5, {3.2, 4.5});
  const double initialArea = tempCircle.getArea();
  const sysoeva::rectangle_t initialRectangle = tempCircle.getFrameRect();

  tempCircle.move({4.2, 5.2});

  BOOST_CHECK_CLOSE(initialRectangle.width, tempCircle.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(initialRectangle.height, tempCircle.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(initialArea, tempCircle.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(invarianceRectParametersAfterMovingBy)
{
  sysoeva::Circle tempCircle(2.5, {3.2, 4.5});
  const double initialArea = tempCircle.getArea();
  const sysoeva::rectangle_t initialRectangle = tempCircle.getFrameRect();

  tempCircle.move(3.5, 17.7);

  BOOST_CHECK_CLOSE(initialRectangle.width, tempCircle.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(initialRectangle.height, tempCircle.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(initialArea, tempCircle.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterIncreasing)
{
  sysoeva::Circle tempCircle(2.5, {3.2, 4.5});
  const double initialArea = tempCircle.getArea();
  double coefficient = 2.5;

  tempCircle.scale(coefficient);

  BOOST_CHECK_CLOSE(initialArea * coefficient * coefficient, tempCircle.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterDecreasing)
{
  sysoeva::Circle tempCircle(2.5, {3.2, 4.5});
  const double initialArea = tempCircle.getArea();
  double coefficient = 0.5;

  tempCircle.scale(coefficient);

  BOOST_CHECK_CLOSE(initialArea * coefficient * coefficient, tempCircle.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  BOOST_CHECK_THROW(sysoeva::Circle circle(0, {3.2, 4.5}), std::invalid_argument);
  BOOST_CHECK_THROW(sysoeva::Circle circle(-2.5, {3.2, 4.5}), std::invalid_argument);

  sysoeva::Circle tempCircle(5.7, {6.2, 7.3});

  BOOST_CHECK_THROW(tempCircle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(tempCircle.scale(-5), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
