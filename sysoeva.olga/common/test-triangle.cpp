#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "triangle.hpp"

const double FAULT = 0.01;

BOOST_AUTO_TEST_SUITE(triangleTest)

BOOST_AUTO_TEST_CASE(invarianceRectParametersAfterMovingTo)
{
  sysoeva::Triangle tempTriangle({2.3, 6.7}, {7.8, 3.4}, {4.5, 5.6});
  const double initialArea = tempTriangle.getArea();
  const sysoeva::rectangle_t initialRectangle = tempTriangle.getFrameRect();

  tempTriangle.move({4.2, 5.4});

  BOOST_CHECK_CLOSE(initialRectangle.width, tempTriangle.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(initialRectangle.height, tempTriangle.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(initialArea, tempTriangle.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(invarianceRectParametersAfterMovingBy)
{
  sysoeva::Triangle tempTriangle({2.3, 6.7}, {7.8, 3.4}, {4.5, 5.6});
  const double initialArea = tempTriangle.getArea();
  const sysoeva::rectangle_t initialRectangle = tempTriangle.getFrameRect();

  tempTriangle.move(3.3, 17.5);

  BOOST_CHECK_CLOSE(initialRectangle.width, tempTriangle.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(initialRectangle.height, tempTriangle.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(initialArea, tempTriangle.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterIncreasing)
{
  sysoeva::Triangle tempTriangle({2.3, 6.7}, {7.8, 3.4}, {4.5, 5.6});
  const double initialArea = tempTriangle.getArea();
  double coefficient = 3.0;

  tempTriangle.scale(coefficient);

  BOOST_CHECK_CLOSE(initialArea * coefficient * coefficient, tempTriangle.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterDecreasing)
{
  sysoeva::Triangle tempTriangle({2.3, 6.7}, {7.8, 3.4}, {4.5, 5.6});
  const double initialArea = tempTriangle.getArea();
  double coefficient = 3.0;

  tempTriangle.scale(coefficient);

  BOOST_CHECK_CLOSE(initialArea * coefficient * coefficient, tempTriangle.getArea(), FAULT);
}

 BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterRotating)
{
  sysoeva::Triangle tempTriangle({2.3, 6.7}, {7.8, 3.4}, {4.5, 5.6});
  const double initialArea = tempTriangle.getArea();
  double angle = 60;

  tempTriangle.rotate(angle);

  BOOST_CHECK_CLOSE(initialArea, tempTriangle.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  BOOST_CHECK_THROW(sysoeva::Triangle triangle({2.3, 6.7}, {7.8, 6.7}, {4.5, 6.7}), std::invalid_argument);
  BOOST_CHECK_THROW(sysoeva::Triangle triangle({2.0, 2.0}, {7.0, 7.0}, {7.0, 7.0}), std::invalid_argument);
  BOOST_CHECK_THROW(sysoeva::Triangle triangle({7.8, 6.7}, {7.8, 3.4}, {7.8, 5.6}), std::invalid_argument);

  sysoeva::Triangle tempTriangle({3.4, 4.5}, {2.3, 7.8}, {14.5, 3.4});

  BOOST_CHECK_THROW(tempTriangle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(tempTriangle.scale(-1.5), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
