#ifndef DIVISION_HPP
#define DIVISION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"
#include <memory>

namespace sysoeva
{
  bool intersect(rectangle_t, rectangle_t);
  Matrix divide(array_ptr &, size_t);
  Matrix divide(CompositeShape &);
}

#endif
