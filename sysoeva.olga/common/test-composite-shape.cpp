#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

const double FAULT = 0.01;

BOOST_AUTO_TEST_SUITE(compositeShapeTest)

BOOST_AUTO_TEST_CASE(invarianceCompositeShapeParametersAfterMovingTo)
{
  sysoeva::Circle circle(2.5, {3.2, 4.5});
  sysoeva::CompositeShape composite_shape(std::make_shared<sysoeva::Circle>(circle));

  sysoeva::Rectangle rectangle(5.2, 6.5, {4.0, 3.0});
  sysoeva::Triangle triangle({2.3, 6.7}, {7.8, 3.4}, {4.5, 5.6});

  composite_shape.add(std::make_shared<sysoeva::Rectangle>(rectangle));
  composite_shape.add(std::make_shared<sysoeva::Triangle>(triangle));

  const double initialArea = composite_shape.getArea();
  const sysoeva::rectangle_t initialRectangle = composite_shape.getFrameRect();

  composite_shape.move({2.7, 15.9});

  BOOST_CHECK_CLOSE(initialRectangle.width, composite_shape.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(initialRectangle.height, composite_shape.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(initialArea, composite_shape.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(invarianceRectParametersAfterMovingBy)
{
  sysoeva::Circle circle(2.5, {3.2, 4.5});
  sysoeva::CompositeShape composite_shape(std::make_shared<sysoeva::Circle>(circle));

  sysoeva::Rectangle rectangle(5.2, 6.5, {4.0, 3.0});
  sysoeva::Triangle triangle({2.3, 6.7}, {7.8, 3.4}, {4.5, 5.6});

  composite_shape.add(std::make_shared<sysoeva::Rectangle>(rectangle));
  composite_shape.add(std::make_shared<sysoeva::Triangle>(triangle));

  const double initialArea = composite_shape.getArea();
  const sysoeva::rectangle_t initialRectangle = composite_shape.getFrameRect();

  composite_shape.move(4.4, 8.7);

  BOOST_CHECK_CLOSE(initialRectangle.width, composite_shape.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(initialRectangle.height, composite_shape.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(initialArea, composite_shape.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterIncreasing)
{
  sysoeva::Circle circle(2.5, {3.2, 4.5});
  sysoeva::CompositeShape composite_shape(std::make_shared<sysoeva::Circle>(circle));

  sysoeva::Rectangle rectangle(5.2, 6.5, {4.0, 3.0});
  sysoeva::Triangle triangle({2.3, 6.7}, {7.8, 3.4}, {4.5, 5.6});

  composite_shape.add(std::make_shared<sysoeva::Rectangle>(rectangle));
  composite_shape.add(std::make_shared<sysoeva::Triangle>(triangle));

  const double initialArea = composite_shape.getArea();
  double coefficient = 4.8;

  composite_shape.scale(coefficient);

  BOOST_CHECK_CLOSE(initialArea * coefficient * coefficient, composite_shape.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterDecreasing)
{
  sysoeva::Circle circle(2.5, {3.2, 4.5});
  sysoeva::CompositeShape composite_shape(std::make_shared<sysoeva::Circle>(circle));

  sysoeva::Rectangle rectangle(5.2, 6.5, {4.0, 3.0});
  sysoeva::Triangle triangle({2.3, 6.7}, {7.8, 3.4}, {4.5, 5.6});

  composite_shape.add(std::make_shared<sysoeva::Rectangle>(rectangle));
  composite_shape.add(std::make_shared<sysoeva::Triangle>(triangle));

  const double initialArea = composite_shape.getArea();
  double coefficient = 0.4;

  composite_shape.scale(coefficient);

  BOOST_CHECK_CLOSE(initialArea * coefficient * coefficient, composite_shape.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterRotating)
{
  sysoeva::Circle circle(2.5, {3.2, 4.5});
  sysoeva::Rectangle rectangle(5.2, 6.5, {4.0, 3.0});

  sysoeva::CompositeShape composite_shape(std::make_shared<sysoeva::Circle>(circle));
  composite_shape.add(std::make_shared<sysoeva::Rectangle>(rectangle));

  const double initialArea = composite_shape.getArea();
  double angle = 20;

  composite_shape.rotate(angle);

  BOOST_CHECK_CLOSE(initialArea, composite_shape.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  sysoeva::Circle circle(2.5, {3.2, 4.5});
  sysoeva::CompositeShape composite_shape(std::make_shared<sysoeva::Circle>(circle));

  sysoeva::Rectangle rectangle(5.2, 6.5, {4.0, 3.0});
  sysoeva::Triangle triangle({2.3, 6.7}, {7.8, 3.4}, {4.5, 5.6});

  composite_shape.add(std::make_shared<sysoeva::Rectangle>(rectangle));
  composite_shape.add(std::make_shared<sysoeva::Triangle>(triangle));

  BOOST_CHECK_THROW(composite_shape.scale(0.0), std::invalid_argument);
  BOOST_CHECK_THROW(composite_shape.scale(-2.2), std::invalid_argument);

  BOOST_CHECK_THROW(composite_shape.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(composite_shape.remove(13), std::out_of_range);
  BOOST_CHECK_THROW(composite_shape.remove(-1), std::out_of_range);
  BOOST_CHECK_THROW(composite_shape.remove(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(constructorCopyAndMove)
{
  sysoeva::Circle circle(2.5, {3.2, 4.5});
  sysoeva::CompositeShape compositeShape(std::make_shared<sysoeva::Circle>(circle));

  BOOST_CHECK_NO_THROW(sysoeva::CompositeShape copiedCompositeShape(compositeShape));
  BOOST_CHECK_NO_THROW(sysoeva::CompositeShape movedCompositeShape(std::move(compositeShape)));

  sysoeva::CompositeShape compositeShape1;
  sysoeva::CompositeShape tempCompositeShape1;
  sysoeva::CompositeShape tempCompositeShape2;

  BOOST_CHECK_NO_THROW(tempCompositeShape1 = compositeShape1);
  BOOST_CHECK_NO_THROW(tempCompositeShape2 = std::move(compositeShape1));
}

BOOST_AUTO_TEST_SUITE_END()
