#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace sysoeva
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&);
    CompositeShape(const shape_ptr &);

    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &);
    CompositeShape &operator =(CompositeShape &&);
    shape_ptr operator [](size_t) const;

    void add(const shape_ptr &);
    void remove(size_t);
    void remove(const shape_ptr &);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(double, double) override;
    void scale(double) override;
    size_t getSize() const;
    void rotate(double) override;
    private:
    size_t size_;
    array_ptr shapes_;
  };
};

#endif
