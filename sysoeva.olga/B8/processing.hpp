#ifndef TEST_TEXTEDITOR_HPP
#define TEST_TEXTEDITOR_HPP

#include "states.hpp"

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

const int MaxLengthWord = 20;

struct Item
{
  States token;
  std::string element;
};

class Processing
{
public:
  Processing();
  void read(std::vector<Item> &text);
  void print(std::vector<Item> &text, size_t maxLen);
};

#endif
