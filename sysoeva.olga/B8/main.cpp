#include <cstring>

#include "processing.hpp"

int main(int argc, char* argv[])
{
  try
  {
    if (argc > 3 || argc == 2) {
      std::cerr << "Invalid number of arguments\n";
      return 1;
    }

    size_t maxLen = 40;
    size_t minLen = MaxLengthWord + 3 + 1 + 1;
    if (argc == 3) {
      if (!strcmp(argv[1], "--line-width")) {
        maxLen = atoi(argv[2]);
        if (maxLen < minLen) {
          std::cerr << "Line length too little\n";
          return 1;
        }
      } else {
        std::cerr << "Invalid parameter name\n";
        return 1;
      }
    }

    Processing processing;
    std::vector<Item> sourceText;
    processing.read(sourceText);
    processing.print(sourceText, maxLen);
  }
  catch (const std::exception &e)
  {
    std::cerr << e.what();
    return 1;
  }

  return 0;
}
