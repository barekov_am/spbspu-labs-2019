#ifndef TEST_STATES_HPP
#define TEST_STATES_HPP

enum States
{
  WORD,
  PUNCT,
  DASH,
  NUMBER,
};

#endif //TEST_STATES_HPP
