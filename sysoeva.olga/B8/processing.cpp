#include "processing.hpp"

Processing::Processing()
{}

void Processing::read(std::vector<Item> &text)
{
  while (std::cin) {
    std::cin >> std::noskipws;
    char first = std::cin.get();
    if (std::isalpha(first)) {
      std::string word;
      word += first;
      while(std::cin)
      {
        if ((std::isalpha(std::cin.peek())) || (std::cin.peek() == '-'))
        {
          word += std::cin.get();
          if (word.back() == '-')
          {
            if (std::cin.peek() == '-')
            {
              throw std::invalid_argument("Wrong record of hyphen\n");
            }
          }
        }
        else
        {
          break;
        }
      }

      if (word.size() > MaxLengthWord)
      {
        throw std::invalid_argument("Word too long\n");
      }

      Item element = {States::WORD, word};
      text.push_back(element);


    } else if ((((first == '+') || (first == '-')) && (std::isdigit(std::cin.peek()))) || (std::isdigit(first))) {
      std::string number;
      number += first;
      size_t countDot = 0;
      while(std::cin)
      {
        if ((std::cin.peek() == '.') || (std::isdigit(std::cin.peek())))
        {
          number += std::cin.get();
          if (number.back() == '.')
          {
            countDot++;
          }
        }
        else
        {
          break;
        }
      }

      if (number.size() > MaxLengthWord)
      {
        throw std::invalid_argument("Too long number\n");
      }

      if (countDot > 1)
      {
        throw std::invalid_argument("Wrong record of number\n");
      }

      Item element = {States::NUMBER, number};
      text.push_back(element);

    }
    else if (std::ispunct(first))
    {
      if ((first == '-') && (std::cin.peek() == '-'))
      {
        std::string dash;
        size_t count = 1;
        dash += first;
        while (std::cin)
        {
          if (std::cin.peek() == '-')
          {
            dash += std::cin.get();
            count++;
          }
          else
          {
            break;
          }
        }

        if (count > 3)
        {
          throw std::invalid_argument("Wrong record of dash\n");
        }

        Item element = {States::DASH, dash};
        text.push_back(element);
      }
      else
      {
        if (std::ispunct(std::cin.peek()))
        {
          throw std::invalid_argument("Wrong record of punctuation\n");
        }
        std::string punct;
        punct += first;
        Item element = {States::PUNCT, punct};
        text.push_back(element);
      }
    }
  }
}

void Processing::print(std::vector<Item> &text, size_t maxLen)
{
  if (text.empty())
  {
    return;
  }

  if ((text.at(0).token == States::PUNCT) || (text.at(0).token == States::DASH))
  {
    throw std::invalid_argument("Wrong beginning of text\n");
  }

  for (size_t i = 0; i < text.size() - 1; i++)
  {
    if ((text.at(i).token == States::PUNCT) && (text.at(i + 1).token == States::PUNCT))
    {
      throw std::invalid_argument("Too much punctuation\n");
    }
  }

  std::vector<std::string> finishedText;
  std::string line;
  size_t i = 0;
  size_t countLines = 0;
  while(i < text.size())
  {
    if (line.empty())
    {
      if ((text.at(i).token == States::PUNCT) || (text.at(i).token == States::DASH))
      {
        if ((text.at(i).token == States::PUNCT) && (finishedText.at(countLines - 1).size() < maxLen))
        {
          finishedText.at(countLines - 1) += text.at(i).element;
          i++;
          continue;
        }
        line += text.at(i - 1).element;
        (text.at(i).token == States::DASH) ? line += " " + text.at(i).element : line += text.at(i).element;
        auto pos = finishedText.at(countLines - 1).find_last_of(' ');
        finishedText.at(countLines - 1).erase(pos);
        i++;
        continue;
      }
      line += text.at(i).element;
      i++;
      continue;
    }

    if (line.size() + text.at(i).element.size() + 1 <= maxLen)
    {
      if (text.at(i).token == States::PUNCT)
      {
        line += text.at(i).element;
        i++;
        continue;
      }
      line += " " + text.at(i).element;
      i++;
      continue;
    }
    finishedText.push_back(line);
    countLines++;
    line.clear();
  }
  finishedText.push_back(line);

  std::for_each(finishedText.begin(), finishedText.end(), [](std::string str)
  {
    std::cout << str << "\n";
  });
}
