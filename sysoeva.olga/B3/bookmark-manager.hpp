#ifndef B3_BOOKMARKS_MANAGER_HPP
#define B3_BOOKMARKS_MANAGER_HPP

#include "phone-book.hpp"
#include <map>

class BookmarkManager
{
public:
  BookmarkManager();
  void add(const PhoneBook::contact &contact_);
  void store(const std::string &mark, const std::string &newMark);
  void insertBefore(const std::string &mark, const PhoneBook::contact &contact_);
  void insertAfter(const std::string &mark, const PhoneBook::contact &contact_);
  void removeMark(const std::string &mark);
  void show(const std::string &mark);
  void move(const std::string &mark, int step);
  void move(const std::string &mark, const std::string &pos);

  bool empty();
private:
  std::map<std::string, PhoneBook::iterator> bookmarks;
  using bookmarkIter = std::map<std::string, PhoneBook::iterator>::iterator;

  PhoneBook phoneBook_;
  bookmarkIter getIter(const std::string &mark);
};

#endif
