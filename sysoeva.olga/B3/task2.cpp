#include "container-factorial.hpp"

#include <iostream>
#include <algorithm>

void task2()
{
  ContainerFactorial factorial;

  std::copy(factorial.begin(), factorial.end(), std::ostream_iterator<unsigned long int>(std::cout, " "));
  std::cout << "\n";

  std::reverse_copy(factorial.begin(), factorial.end(), std::ostream_iterator<unsigned long int>(std::cout, " "));
  std::cout << "\n";
}
