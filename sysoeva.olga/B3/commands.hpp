#ifndef B3_COMMANDS_HPP
#define B3_COMMANDS_HPP

#include "bookmark-manager.hpp"
#include <sstream>

void add(BookmarkManager &bookmarkManager, std::stringstream &stringStream);
void store(BookmarkManager &bookmarkManager, std::stringstream &stringStream);
void insert(BookmarkManager &bookmarkManager, std::stringstream &stringStream);
void removeMark(BookmarkManager &bookmarkManager, std::stringstream &stringStream);
void show(BookmarkManager &bookmarkManager, std::stringstream &stringStream);
void move(BookmarkManager &bookmarkManager, std::stringstream &stringStream);
#endif
