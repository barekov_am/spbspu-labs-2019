#include "bookmark-manager.hpp"

#include <iostream>

BookmarkManager::BookmarkManager()
{
  bookmarks["current"] = phoneBook_.begin();
}

void BookmarkManager::add(const PhoneBook::contact &contact_)
{
  phoneBook_.pushBack(contact_);
  if (phoneBook_.size() == 1)
  {
    bookmarks["current"] = phoneBook_.begin();
  }
}

void BookmarkManager::store(const std::string &mark, const std::string &newMark)
{
  auto it = getIter(mark);
  if (it == bookmarks.end())
  {
    return;
  }
  bookmarks.emplace(newMark, it->second);
}

void BookmarkManager::insertBefore(const std::string &mark, const PhoneBook::contact &contact_)
{
  auto it = getIter(mark);
  phoneBook_.insert(it->second, contact_);
}

void BookmarkManager::insertAfter(const std::string &mark, const PhoneBook::contact &contact_)
{
  auto it = getIter(mark);
  phoneBook_.insert(std::next(it->second), contact_);
}

void BookmarkManager::removeMark(const std::string &mark)
{
  auto it = getIter(mark);

  if (it == bookmarks.end())
  {
    return;
  }

  auto curIt = bookmarks.begin();
  auto removeIt = it->second;

  while (curIt != bookmarks.end())
  {
    if (curIt->second == removeIt)
    {
      if (std::next(curIt->second) == phoneBook_.end())
      {
          curIt->second = phoneBook_.prev(removeIt);
      }
      else
      {
        curIt->second = phoneBook_.next(removeIt);
      }
    }
    ++curIt;
  }
  phoneBook_.removeMark(removeIt);
}

void BookmarkManager::show(const std::string &mark)
{
  auto it = getIter(mark);

  if (it == bookmarks.end())
  {
    return;
  }

  if (phoneBook_.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  phoneBook_.show(it->second);
}

void BookmarkManager::move(const std::string &mark, int step)
{
  auto it = getIter(mark);
  it->second = phoneBook_.skip(it->second, step);
}

void BookmarkManager::move(const std::string &mark, const std::string &pos)
{
  auto it = getIter(mark);
  if (pos == "first")
  {
    it->second = phoneBook_.begin();
  }
  else
  {
    it->second = --phoneBook_.end();
  }
}

bool BookmarkManager::empty()
{
  return phoneBook_.empty();
}

BookmarkManager::bookmarkIter BookmarkManager::getIter(const std::string &mark)
{
  auto it = bookmarks.find(mark);
  if (it == bookmarks.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
  }
  return it;
}
