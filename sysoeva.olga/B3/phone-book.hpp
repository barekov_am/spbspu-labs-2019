#ifndef B3_PHONE_BOOK_HPP
#define B3_PHONE_BOOK_HPP

#include <list>
#include <map>
#include <string>

class PhoneBook
{
public:
  struct contact
  {
    std::string name;
    std::string number;
  };

  using iterator = std::list<contact>::iterator;

  iterator begin();
  iterator end();

  iterator next(iterator pos);
  iterator prev(iterator pos);

  iterator skip(iterator pos, int n);

  void insert(iterator pos, const contact &contact_);
  void removeMark(iterator pos);
  void show(iterator pos) const;
  void pushBack(const contact &contact_);

  bool empty() const;
  size_t size();

private:
  std::list<contact> records;
};

#endif
