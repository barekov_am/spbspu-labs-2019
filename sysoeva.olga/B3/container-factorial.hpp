#ifndef B3_CONTAINER_FACTORIAL_HPP
#define B3_CONTAINER_FACTORIAL_HPP

#include <iterator>

class ContainerFactorial
{
public:
  class Iterator : public std::iterator<std::bidirectional_iterator_tag, unsigned long int>
  {
  public:
    Iterator(int index);

    unsigned long int operator*();

    Iterator &operator++();
    Iterator operator++(int);

    Iterator &operator--();
    Iterator operator--(int);

    bool operator==(const Iterator &iter) const;
    bool operator!=(const Iterator &iter) const;
  private:
    unsigned long int value_;
    int index_;
    int max = 11;
    unsigned long int calculateFactorial(int index) const;
  };

  Iterator begin() const;
  Iterator end() const;
};

#endif
