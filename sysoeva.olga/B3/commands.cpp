#include "bookmark-manager.hpp"
#include "commands.hpp"

#include <iostream>
#include <string>

std::string getNumber(std::string &number)
{
  if (number.empty())
  {
    return "";
  }

  auto it = number.begin();
  while (it != number.end())
  {
    if (!isdigit(*it))
    {
      return "";
    }
    ++it;
  }

  return number;
}

std::string getName(std::string &name)
{
  if (name.empty())
  {
    return "";
  }

  if ((name.front() != '\"') || (name.back() != '\"'))
  {
    return "";
  }

  name.erase(name.begin());
  name.erase(name.end() - 1);

  auto it = name.begin();
  while (it != name.end())
  {
    if (*it == '\\')
    {
      if (((it + 1) != name.end()) && ((*(it + 1) == '\\') || (*(it + 1) == '"')))
      {
        it = name.erase(it) + 1;
      }
      else
      {
        return "";
      }
    }
    else if (*it == '"')
    {
      return "";
    }
    else
    {
      ++it;
    }
  }
  return name;
}

std::string getMark(std::string &mark)
{
  if (mark.empty())
  {
    return "";
  }

  auto it = mark.begin();
  while (it != mark.end())
  {
    if ((!isalnum(*it)) && ((*it) != '-'))
    {
      return "";
    }
    ++it;
  }
  return mark;
}

void add(BookmarkManager &bookmarkManager, std::stringstream &stringStream)
{
  std::string number;
  stringStream >> number >> std::ws;
  number = getNumber(number);

  std::string name;
  getline(stringStream, name);
  name = getName(name);

  if (number.empty() || name.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  bookmarkManager.add({name, number});
}

void store(BookmarkManager &bookmarkManager, std::stringstream &stringStream)
{
  std::string mark;
  stringStream >> mark >> std::ws;
  mark = getMark(mark);

  std::string newMark;
  getline(stringStream, newMark);
  newMark = getMark(newMark);

  if (mark.empty() || newMark.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  bookmarkManager.store(mark, newMark);
}

void insert(BookmarkManager &bookmarkManager, std::stringstream &stringStream)
{
  std::string position;
  std::string mark;
  std::string number;
  std::string name;
  stringStream >> std::ws >> position >> std::ws;
  stringStream >> mark >> std::ws;
  stringStream >> number >> std::ws;
  getline(stringStream, name);
  mark = getMark(mark);
  number = getNumber(number);
  name = getName(name);

  if (position.empty() || mark.empty() || number.empty() || name.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (bookmarkManager.empty())
  {
    bookmarkManager.add({name, number});
    return;
  }

  if (position == "before")
  {
    bookmarkManager.insertBefore(mark, {name, number});
  }
  else if (position == "after")
  {
    bookmarkManager.insertAfter(mark, {name, number});
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}

void removeMark(BookmarkManager &bookmarkManager, std::stringstream &stringStream)
{
  std::string mark;
  stringStream >> mark >> std::ws;
  mark = getMark(mark);

  if (mark.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  bookmarkManager.removeMark(mark);
}

void show(BookmarkManager &bookmarkManager, std::stringstream &stringStream)
{
  std::string mark;
  stringStream >> mark;
  mark = getMark(mark);

  if (mark.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  bookmarkManager.show(mark);
}

void move(BookmarkManager &bookmarkManager, std::stringstream &stringStream)
{
  std::string mark;
  stringStream >> mark >> std::ws;
  mark = getMark(mark);
  std::string step;
  getline(stringStream, step);

  if ((mark.empty()) || (step.empty()))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if ((step == "first") || (step == "last"))
  {
    bookmarkManager.move(mark, step);
  }
  else if (step.at(0) == '-' || step.at(0) == '+' || (isdigit(step.at(0))))
  {
    for (size_t i = 1; i < step.size(); i++)
    {
      if (!isdigit(step.at(i)))
      {
        std::cout << "<INVALID COMMAND>\n";
        return;
      }
    }
    int num = std::stoi(step);
    bookmarkManager.move(mark, num);
  }
  else
  {
    std::cout << "<INVALID STEP>\n";
    return;
  }
}
