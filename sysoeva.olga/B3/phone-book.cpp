#include "phone-book.hpp"

#include <iostream>

PhoneBook::iterator PhoneBook::begin()
{
  return records.begin();
}

PhoneBook::iterator PhoneBook::end()
{
  return records.end();
}

PhoneBook::iterator PhoneBook::next(iterator pos)
{
  return {pos == records.end() ? pos : ++pos};
}

PhoneBook::iterator PhoneBook::prev(iterator pos)
{
  return {pos == records.begin() ? pos : --pos};
}

PhoneBook::iterator PhoneBook::skip(iterator pos, int n)
{
  std::advance(pos, n);
  return pos;
}

void PhoneBook::insert(iterator pos, const contact &contact_)
{
  records.insert(pos, contact_);
}

void PhoneBook::removeMark(iterator pos)
{
  records.erase(pos);
}

void PhoneBook::show(iterator pos) const
{
  std::cout << pos->number << " " << pos->name << "\n";
}

void PhoneBook::pushBack(const contact &contact_)
{
  records.push_back(contact_);
}

bool PhoneBook::empty() const
{
  return records.empty();
}

size_t PhoneBook::size()
{
  return records.size();
}
