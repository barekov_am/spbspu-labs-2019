#include <iostream>
#include "circle.hpp"
#include "composite-shape.hpp"
#include "partition.hpp"
#include "matrix.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

void printRect(const sysoeva::Shape &shape)
{
  std::cout << "Frame rectangle: x = " << shape.getFrameRect().pos.x;
  std::cout << " y = " << shape.getFrameRect().pos.y;
  std::cout << " width = " << shape.getFrameRect().width;
  std::cout << " height = " << shape.getFrameRect().height << '\n';
}

void showProgramWork(sysoeva::Shape &shape)
{
  std::cout << "Area = " << shape.getArea() << '\n';
  printRect(shape);
  double x = 2;
  double y = 2;
  shape.move({x, y});
  std::cout << "Move to: x = " << x << " y = " << y << '\n';
  printRect(shape);
  double dx = 5;
  double dy = 10;
  shape.move(dx, dy);
  std::cout << "Move by: dx = " << dx << " dy = " << dy << '\n';
  printRect(shape);
  double coefficient = 2;
  shape.scale(coefficient);
  std::cout << "Scaled figure: ";
  printRect(shape);
  shape.scale(0.5);
  double angle = 90;
  shape.rotate(angle);
  std::cout << "Rotated figure: ";
  printRect(shape);
  angle = 90;
  shape.rotate(angle);
  std::cout << "Rotated figure: ";
  printRect(shape);
}

int main()
{
  std::cout << "The first object is Circle:" << '\n';
  sysoeva::Circle circle(2.6, {3, 7});
  showProgramWork(circle);
  std::cout << "The second object is Rectangle:" << '\n';
  sysoeva::Rectangle rectangle(5, 6, {2, 4});
  showProgramWork(rectangle);
  std::cout << "The third object is Triangle:" << '\n';
  sysoeva::Triangle triangle({3, 7}, {5, 2}, {7, 4});
  triangle.printCenter();
  showProgramWork(triangle);
  triangle.showCoord();

  std::cout << "The Composite Shape:" << '\n';
  sysoeva::CompositeShape composite_shape;

  composite_shape.add(std::make_shared<sysoeva::Circle> (circle));
  composite_shape.add(std::make_shared<sysoeva::Rectangle> (rectangle));
  composite_shape.add(std::make_shared<sysoeva::Triangle> (triangle));
  showProgramWork(composite_shape);

  composite_shape.remove(0);
  showProgramWork(composite_shape);

  sysoeva::array_ptr shape = std::make_unique<sysoeva::shape_ptr[]> (6);
  sysoeva::Circle circle_1(2.0, {10, 3});
  sysoeva::Rectangle rectangle_1(2, 2, {11, 11});
  sysoeva::Triangle triangle_1({5, 2}, {8, 7}, {6, 4});
  shape[0] = std::make_shared<sysoeva::Circle>(circle);
  shape[1] = std::make_shared<sysoeva::Rectangle>(rectangle);
  shape[2] = std::make_shared<sysoeva::Triangle>(triangle);
  shape[3] = std::make_shared<sysoeva::Circle>(circle_1);
  shape[4] = std::make_shared<sysoeva::Rectangle>(rectangle_1);
  shape[5] = std::make_shared<sysoeva::Triangle>(triangle_1);
  sysoeva::Matrix matrix = sysoeva::divide(shape, 6);
  matrix.printMatrix();

  return 0;
}
