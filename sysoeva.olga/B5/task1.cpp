#include <iostream>
#include <iterator>
#include <set>
#include <sstream>

void task1()
{
  std::string string;
  std::set<std::string> set;

  while (std::getline(std::cin, string))
  {
    if (std::cin.fail() && !std::cin.eof())
    {
      throw std::ios_base::failure("Reading failed\n");
    }

    std::stringstream stringStream(string);
    std::istream_iterator<std::string> beginOfStream = std::istream_iterator<std::string>(stringStream);
    std::istream_iterator<std::string> endOfStream = std::istream_iterator<std::string>();
    std::copy(beginOfStream, endOfStream, inserter(set, set.end()));
  }

  std::copy(set.begin(), set.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
}
