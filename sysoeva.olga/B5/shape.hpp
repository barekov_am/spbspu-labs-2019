#ifndef B5_SHAPE_HPP
#define B5_SHAPE_HPP

#include <vector>
#include <istream>

struct Point
{
  int x;
  int y;
};

using Shape = std::vector<Point>;

std::istream &operator>>(std::istream &in, Shape &shape);
std::ostream &operator<<(std::ostream &out, const Point &point);

bool isTriangle(const Shape &shape);
bool isRectangle(const Shape &shape);
bool isSquare(const Shape &shape);
bool isPentagon(const Shape &shape);

bool compare(const Shape &lhs, const Shape &rhs);

int sumOfVertices(int sum, const Shape &shape);

#endif //B5_SHAPE_HPP
