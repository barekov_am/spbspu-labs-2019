#include "shape.hpp"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <sstream>

void task2()
{
  std::vector<Shape> shapes;
  std::string string;

  while (std::getline(std::cin, string))
  {
    if (std::cin.fail() && !std::cin.eof())
    {
      throw std::ios_base::failure("Reading failed\n");
    }

    bool isWsSequence = true;
    for (auto it = string.begin(); it != string.end(); ++it)
    {
      if (!(*it == ' ' || *it == '\t'))
      {
        isWsSequence = false;
        break;
      }
    }

    if (isWsSequence)
    {
      continue;
    }

    Shape shape;
    std::stringstream stringStream(string);
    stringStream >> shape;

    shapes.push_back(shape);
  }

  int numberOfVertices = std::accumulate(shapes.begin(), shapes.end(), 0, sumOfVertices);

  int numberOfTriangles = std::count_if(shapes.begin(), shapes.end(), isTriangle);
  int numberOfSquares = std::count_if(shapes.begin(), shapes.end(), isSquare);
  int numberOfRectangle = std::count_if(shapes.begin(), shapes.end(), isRectangle);

  std::cout << "Vertices: " << numberOfVertices << '\n';
  std::cout << "Triangles: " << numberOfTriangles << '\n';
  std::cout << "Squares: " << numberOfSquares << '\n';
  std::cout << "Rectangles: " << numberOfRectangle << '\n';

  shapes.erase(std::remove_if(shapes.begin(), shapes.end(), isPentagon), shapes.end());

  std::cout << "Points: ";
  Shape pointsOfShapes;
  for (Shape shape : shapes)
  {
    pointsOfShapes.push_back(shape.at(0));
  }

  std::copy(pointsOfShapes.begin(), pointsOfShapes.end(), std::ostream_iterator<Point>(std::cout, " "));

  std::sort(shapes.begin(), shapes.end(), compare);
  std::cout << "\nShapes:" << '\n';
  for (Shape shape : shapes)
  {
    std::cout << shape.size() << " ";
    std::copy(shape.begin(), shape.end(), std::ostream_iterator<Point>(std::cout, " "));
    std::cout << '\n';
  }
}
