#include "shape.hpp"

#include <cmath>
#include <iostream>

std::istream &operator>>(std::istream &in, Shape &shape)
{
  std::size_t vertices = 0;
  in >> vertices;
  if (vertices < 3)
  {
    throw std::invalid_argument("Wrong number of vertices");
  }

  for (std::size_t i = 0; i < vertices; i++)
  {
    char openBracket = 0;
    int x;
    char separator = 0;
    int y;
    char closeBracket = 0;

    in >> openBracket >> x >> separator >> y >> closeBracket;

    if (openBracket != '(' || separator != ';' || closeBracket != ')' || in.eof())
    {
      throw std::invalid_argument("Wrong point");
    }

    shape.push_back({x, y});
  }
  return in;
}

std::ostream &operator<<(std::ostream &out, const Point &point)
{
  std::cout << "(" << point.x << ";" << point.y << ")";
  return out;
}

double getDistance(const Point &point_1, const Point &point_2)
{
  return sqrt(pow(point_2.x - point_1.x, 2) + pow(point_2.y - point_1.y, 2));
}

bool isTriangle(const Shape &shape)
{
  return (shape.size() == 3);
}

bool isRectangle(const Shape &shape)
{
  if (shape.size() != 4)
  {
    return false;
  }
  return (getDistance(shape.at(0), shape.at(2)) == getDistance(shape.at(1), shape.at(3)));
}

bool isSquare(const Shape &shape)
{
  if (!isRectangle(shape))
  {
    return false;
  }
  return (getDistance(shape.at(0), shape.at(1)) == getDistance(shape.at(1), shape.at(2)));
}

bool isPentagon(const Shape &shape)
{
  return (shape.size() == 5);
}

bool compare(const Shape &lhs, const Shape &rhs)
{
  if (lhs.size() != rhs.size())
  {
    return lhs.size() < rhs.size();
  }

  if (isRectangle(lhs) && isRectangle(rhs))
  {
    if (isSquare(lhs) && isSquare(rhs))
    {
      return false;
    }
    else if (isSquare(lhs) && isRectangle(rhs))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  return false;
}

int sumOfVertices(int sum, const Shape &shape)
{
  return sum += shape.size();
}
