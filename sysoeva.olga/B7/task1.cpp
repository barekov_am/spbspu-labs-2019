#include "functor.hpp"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <list>

void task1()
{
  std::list <double> list((std::istream_iterator<double>(std::cin)), std::istream_iterator<double>());

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::ios_base::failure("Reading failed\n");
  }

  for_each(list.begin(), list.end(), Multiply());
  for (double &value : list)
  {
    std::cout << value << ' ';
  }
  std::cout << '\n';
}
