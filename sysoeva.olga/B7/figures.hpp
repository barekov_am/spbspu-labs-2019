#ifndef B7_FIGURES_HPP
#define B7_FIGURES_HPP

#include "shape.hpp"

#include <list>

class Circle: public Shape
{
public:
  Circle(point_t &point);

  void draw() const override;
};

class Triangle: public Shape
{
public:
  Triangle(point_t &point);

  void draw() const override;
};

class Square: public Shape
{
public:
  Square(point_t &point);

  void draw() const override;
};

std::istream &operator>>(std::istream &in, std::list<Shape::pointer> &shapes);
std::ostream &operator<<(std::ostream &out, std::list<Shape::pointer> &shapes);

#endif //B7_FIGURES_HPP
