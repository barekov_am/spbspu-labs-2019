#ifndef B7_SHAPE_HPP
#define B7_SHAPE_HPP

#include <iostream>
#include <memory>

struct point_t
{
  int x;
  int y;
};

class Shape
{
public:
  using pointer = std::shared_ptr<Shape>;

  Shape(point_t &point);
  virtual ~Shape() = default;

  point_t getCenter() const;
  bool isMoreLeft(const Shape &shape);
  bool isUpper(const Shape &shape);
  virtual void draw() const = 0;

protected:
  point_t center;
};

#endif //B7_SHAPE_HPP
