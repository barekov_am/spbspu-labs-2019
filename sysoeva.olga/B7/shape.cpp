#include "shape.hpp"

Shape::Shape(point_t &point):
  center(point)
{}

point_t Shape::getCenter() const
{
  return center;
}

bool Shape::isMoreLeft(const Shape &shape)
{
  return (center.x < shape.getCenter().x);
}

bool Shape::isUpper(const Shape &shape)
{
  return (center.y > shape.getCenter().y);
}
