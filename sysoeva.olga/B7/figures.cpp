#include "figures.hpp"

#include <algorithm>

Circle::Circle(point_t &point):
  Shape(point)
{}

void Circle::draw() const
{
  std::cout << "CIRCLE " << "(" << center.x << ";" << center.y << ")\n";
}

Triangle::Triangle(point_t &point):
  Shape(point)
{}

void Triangle::draw() const
{
  std::cout << "TRIANGLE " << "(" << center.x << ";" << center.y << ")\n";
}

Square::Square(point_t &point):
  Shape(point)
{}

void Square::draw() const
{
  std::cout << "SQUARE " << "(" << center.x << ";" << center.y << ")\n";
}

std::istream &operator>>(std::istream &in, std::list<Shape::pointer> &shapes)
{
  std::string shape = "";
  char openBracket = 0;
  int x = 0;
  char separator = 0;
  int y = 0;
  char closeBracket = 0;

  in >> openBracket;
  while (openBracket != '(' && !in.eof())
  {
    shape += openBracket;
    in >> openBracket;
  }

  in  >> x >> separator >> y >> closeBracket;

  if (in.fail())
  {
    throw std::ios_base::failure("Reading failed\n");
  }

  if (openBracket != '(' || separator != ';' || closeBracket != ')')
  {
    throw std::invalid_argument("Wrong point");
  }

  point_t point {x, y};


  if (shape == "CIRCLE")
  {
    shapes.push_back(std::make_shared<Circle>(Circle(point)));
  }

  else if (shape == "TRIANGLE")
  {
    shapes.push_back(std::make_shared<Triangle>(Triangle(point)));
  }

  else if (shape == "SQUARE")
  {
    shapes.push_back(std::make_shared<Square>(Square(point)));
  }

  else
  {
    throw std::invalid_argument("Wrong name of shape");
  }

  return in;
}

std::ostream &operator<<(std::ostream &out, std::list<Shape::pointer> &shapes)
{
  for_each(shapes.begin(), shapes.end(), [](Shape::pointer &shape) {shape->draw();});
  return out;
}
