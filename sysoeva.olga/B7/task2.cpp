#include "figures.hpp"

#include <iterator>
#include <sstream>
#include <list>

void task2()
{
  std::list<Shape::pointer> shapes;
  std::string string;

  while (getline(std::cin, string))
  {
    if (std::cin.fail() && !std::cin.eof())
    {
      throw std::ios_base::failure("Reading failed\n");
    }

    bool isWsSequence = true;
    for (auto it = string.begin(); it != string.end(); ++it)
    {
      if (!(*it == ' ' || *it == '\t'))
      {
        isWsSequence = false;
        break;
      }
    }

    if (isWsSequence)
    {
      continue;
    }

    std::stringstream stringStream(string);
    stringStream >> shapes;
  }

  std::cout << "Original:\n";
  std::cout << shapes;

  shapes.sort([](Shape::pointer &lhs, Shape::pointer &rhs) {return lhs->isMoreLeft(*rhs);});
  std::cout << "Left-Right:\n";
  std::cout << shapes;

  shapes.sort([](Shape::pointer &lhs, Shape::pointer &rhs) {return rhs->isMoreLeft(*lhs);});
  std::cout << "Right-Left:\n";
  std::cout << shapes;

  shapes.sort([](Shape::pointer &lhs, Shape::pointer &rhs) {return lhs->isUpper(*rhs);});
  std::cout << "Top-Bottom:\n";
  std::cout << shapes;

  shapes.sort([](Shape::pointer &lhs, Shape::pointer &rhs) {return rhs->isUpper(*lhs);});
  std::cout << "Bottom-Top:\n";
  std::cout << shapes;
}
