#ifndef B7_FUNCTOR_HPP
#define B7_FUNCTOR_HPP
#define _USE_MATH_DEFINES

#include <math.h>

struct Multiply
{
  void operator() (double &value)
  {
    value *= M_PI;
  }
};

#endif //B7_FUNCTOR_HPP
