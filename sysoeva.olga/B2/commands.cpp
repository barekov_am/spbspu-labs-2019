#include "queue-with-priorities.hpp"
#include "commands.hpp"

#include <iostream>

void add(QueueWithPriority<std::string> &queue, std::stringstream &stringStream)
{
  std::string priority;
  stringStream >> priority >> std::ws;

  std::string data;
  getline(stringStream, data);

  if (priority.empty() || data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  ElementPriority elementPriority;

  if (priority == "high")
  {
    elementPriority = ElementPriority::HIGH;
  }
  else if (priority == "normal")
  {
    elementPriority = ElementPriority::NORMAL;
  }
  else if (priority == "low")
  {
    elementPriority = ElementPriority::LOW;
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.putElementToQueue(data, elementPriority);
}

void get(QueueWithPriority<std::string> &queue, std::stringstream &stringStream)
{
  if (queue.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  if (!stringStream.eof())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::cout << queue.getElementFromQueue() << '\n';
};

void accelerate(QueueWithPriority<std::string> &queue, std::stringstream &stringStream)
{
  if (!stringStream.eof())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.accelerate();
};
