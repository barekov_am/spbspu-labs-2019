#ifndef B2_QUEUE_WITH_PRIORITIES_HPP
#define B2_QUEUE_WITH_PRIORITIES_HPP

#include <iostream>
#include <list>

enum ElementPriority
{
  LOW,
  NORMAL,
  HIGH
};

template <typename T>
class QueueWithPriority
{
public:
  void putElementToQueue(const T &element, ElementPriority priority);
  T getElementFromQueue();
  void accelerate();
  bool empty();

private:
  std::list<T> low;
  std::list<T> normal;
  std::list<T> high;
};

template <typename T>
void QueueWithPriority<T>::putElementToQueue(const T &element, ElementPriority priority)
{
  switch (priority)
  {
    case ElementPriority::HIGH:
    {
      high.push_back(element);
      break;
    }
    case ElementPriority::NORMAL:
    {
      normal.push_back(element);
      break;
    }
    case ElementPriority::LOW:
    {
      low.push_back(element);
      break;
    }
    default:
    {
      throw std::invalid_argument("Wrong priority\n");
    }
  }
}

template <typename T>
T QueueWithPriority<T>::getElementFromQueue()
{
  if (!high.empty())
  {
    T element = high.front();
    high.pop_front();
    return element;
  }
  if (!normal.empty())
  {
    T element = normal.front();
    normal.pop_front();
    return element;
  }
  if (!low.empty())
  {
    T element = low.front();
    low.pop_front();
    return element;
  }
  throw std::out_of_range("Queue is empty\n");
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  high.splice(high.end(), low);
}

template <typename T>
bool QueueWithPriority<T>::empty()
{
  return (high.empty() && normal.empty() && low.empty());
}

#endif
