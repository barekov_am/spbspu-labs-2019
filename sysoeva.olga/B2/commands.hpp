#ifndef B2_COMMANDS_HPP
#define B2_COMMANDS_HPP

#include <sstream>

template <typename T>
class QueueWithPriority;

void add(QueueWithPriority<std::string> &queue, std::stringstream &stringStream);
void get(QueueWithPriority<std::string> &queue, std::stringstream &stringStream);
void accelerate(QueueWithPriority<std::string> &queue, std::stringstream &stringStream);

#endif
