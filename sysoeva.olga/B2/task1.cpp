#include "commands.hpp"
#include "queue-with-priorities.hpp"

#include <iostream>
#include <sstream>

void task1()
{
  QueueWithPriority<std::string> queue;
  std::string string;

  while (std::getline(std::cin, string))
  {
    std::stringstream stringStream(string);
    std::string command;
    stringStream >> command;

    if(std::cin.fail() && !std::cin.eof())
    {
      throw std::ios_base::failure("Reading failed\n");
    }

    if (command == "add")
    {
      add(queue, stringStream);
    }
    else if (command == "get")
    {
      get(queue, stringStream);
    }
    else if (command == "accelerate")
    {
      accelerate(queue, stringStream);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
