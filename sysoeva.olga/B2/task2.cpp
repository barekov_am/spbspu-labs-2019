#include <iostream>
#include <list>

void task2()
{
  std::list<int> list;
  int value;
  while (std::cin >> value)
  {
    if ((value >= 1) && (value <= 20))
    {
      list.push_back(value);
    }
    else
    {
      throw std::invalid_argument("Wrong value\n");
    }

    if (list.size() == 21)
    {
      throw std::invalid_argument("Wrong size of list\n");
    }
  }

  if(std::cin.fail() && !std::cin.eof())
  {
    throw std::ios_base::failure("Reading failed\n");
  }

  if (list.empty())
  {
    return;
  }

  auto center = list.begin();
  if (list.size() % 2 == 0)
  {
    std::advance(center, list.size() / 2);
  }
  else
  {
    std::advance(center, (list.size() / 2) + 1);
  }

  auto begin = list.begin();
  auto end = list.end();
  std::advance(end, -1);

  while (begin != center)
  {
    if (begin == end)
    {
      std::cout << *begin << " ";
      break;
    }
    std::cout << *begin << " " << *end << " ";
    ++begin;
    --end;
  }
  std::cout << '\n';
}
