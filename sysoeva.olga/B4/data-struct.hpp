#ifndef B4_DATA_STRUCT_HPP
#define B4_DATA_STRUCT_HPP

#include <string>
#include <vector>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

std::istream &operator>>(std::istream &in, DataStruct &dataStruct);
std::ostream &operator<<(std::ostream &out, const DataStruct &dataStruct);

bool compare(DataStruct &lhs, DataStruct &rhs);

#endif
