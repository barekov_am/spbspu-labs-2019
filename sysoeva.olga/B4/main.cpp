#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>

#include "data-struct.hpp"

int main()
{
  try
  {
    std::vector<DataStruct> vector;
    std::string string;
    DataStruct data;

    while (std::getline(std::cin, string))
    {
      if(std::cin.fail() && !std::cin.eof())
      {
        throw std::ios_base::failure("Reading failed\n");
      }

      std::stringstream stringStream(string);
      stringStream >> data;
      vector.push_back(data);
    }

  sort(vector.begin(), vector.end(), compare);
  std::copy(vector.begin(), vector.end(), std::ostream_iterator<DataStruct>(std::cout));
  }
  catch (const std::exception &e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}
