#include "statistics.hpp"

statistics::statistics():
  first(0),
  qty(0),
  max_(0),
  min_(0),
  average_(0),
  amountOfPositive_(0),
  amountOfNegative_(0),
  sumOfEven_(0),
  sumOfOdd_(0),
  isEqual_(false),
  isFirst(true)
{}

bool statistics::empty() const
{
  return (qty == 0);
}

 void statistics::operator()(int value)
{
  ++qty;

  if (isFirst)
  {
    first = value;
    max_ = value;
    min_ = max_;
    isFirst = false;
  }

  max_ = std::max(max_, value);

  min_ = std::min(min_, value);

  if (value > 0)
  {
    ++amountOfPositive_;
  }

  if (value < 0)
  {
    ++amountOfNegative_;
  }

  if (value % 2 == 0)
  {
    sumOfEven_ += value;
  }
  else
  {
    sumOfOdd_ += value;
  }

  isEqual_ = (first == value);

  average_ = static_cast<double>(sumOfOdd_ + sumOfEven_)/ qty;
}
