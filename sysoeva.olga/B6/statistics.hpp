#ifndef B6_STATISTICS_HPP
#define B6_STATISTICS_HPP

#include <algorithm>

class statistics
{
public:

  statistics();
  bool empty() const;
  void operator()(int value);

  int max() {return max_;}
  int min() {return min_;}
  double average() {return average_;}
  int amountOfPositive() {return amountOfPositive_;}
  int amountOfNegative() {return amountOfNegative_;}
  long long sumOfEven() {return sumOfEven_;}
  long long sumOfOdd() {return sumOfOdd_;}
  bool isEqual() {return isEqual_;}

private:
  int first;
  int qty;

  int max_;
  int min_;
  double average_;
  int amountOfPositive_;
  int amountOfNegative_;
  long long sumOfEven_;
  long long sumOfOdd_;
  bool isEqual_;
  bool isFirst;
};

#endif //B6_STATISTICS_HPP
