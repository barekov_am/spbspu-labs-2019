#include "statistics.hpp"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <list>

int main()
{
  try
  {
    std::list<int> list((std::istream_iterator<int>(std::cin)), std::istream_iterator<int>());

    if (std::cin.fail() && !std::cin.eof())
    {
      throw std::ios_base::failure("Reading failed\n");
    }

    statistics Statistics;
    Statistics = for_each(list.begin(), list.end(), statistics());

    if (Statistics.empty())
    {
      std::cout << "No Data\n";
      return 0;
    }

    std::cout << "Max: " << Statistics.max() << '\n'
              << "Min: " << Statistics.min() << '\n'
              << "Mean: " << Statistics.average() << '\n'
              << "Positive: " << Statistics.amountOfPositive() << '\n'
              << "Negative: " << Statistics.amountOfNegative() << '\n'
              << "Odd Sum: " << Statistics.sumOfOdd() << '\n'
              << "Even Sum: " << Statistics.sumOfEven() << '\n'
              << "First/Last Equal: ";
    Statistics.isEqual() ? std::cout << "yes" : std::cout << "no";
    std::cout << '\n';
  }
  catch (const std::exception &e)
  {
    std::cerr << e.what();
    return 1;
  }
}
