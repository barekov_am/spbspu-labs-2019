#define BOOST_TEST_MODULE A2
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(TestCircle)

BOOST_AUTO_TEST_CASE(CoordinateMoveTest)
{
  alekseev::Circle testCircle({2.2, 3.4}, 5.2);
  alekseev::rectangle_t frameBeforeMove = testCircle.getFrameRect();
  double areaBeforeMove = testCircle.getArea();
  
  testCircle.move(2.0, 5.0);
  alekseev::rectangle_t frameAterMove = testCircle.getFrameRect();
  double areaAfterMove = testCircle.getArea();

  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(MiddleMoveTest)
{
  alekseev::Circle testCircle({2.2, 3.4}, 5.2);
  alekseev::rectangle_t frameBeforeMove = testCircle.getFrameRect();
  double areaBeforeMove = testCircle.getArea();
  
  testCircle.move({2.0, 5.0});
  alekseev::rectangle_t frameAterMove = testCircle.getFrameRect();
  double areaAfterMove = testCircle.getArea();

  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(TestScale)
{
  alekseev::Circle testCircle({2.2, 3.4}, 5.2);
  double areaBeforeScale = testCircle.getArea();
  double k = 3;
  testCircle.scale(k);
  double areaAfterScale = testCircle.getArea();
  
  BOOST_CHECK_CLOSE(areaBeforeScale * k * k, areaAfterScale, EPSILON);
}

BOOST_AUTO_TEST_CASE(IncorrectRadiusTest)
{
  BOOST_CHECK_THROW(alekseev::Circle({2.2, 3.4}, -5.2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(IncorrectScaleTest)
{
  alekseev::Circle testCircle({2.2, 3.4}, 5.2);
  
  BOOST_CHECK_THROW(testCircle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestRectangle)

BOOST_AUTO_TEST_CASE(CoordinateMoveTest)
{
  alekseev::Rectangle testRectangle({3.2, 3.3}, 2.0, 4.0);
  alekseev::rectangle_t frameBeforeMove = testRectangle.getFrameRect();
  double areaBeforeMove = testRectangle.getArea();
  
  testRectangle.move(2.0, 2.0);
  alekseev::rectangle_t frameAfterMove = testRectangle.getFrameRect();
  double areaAfterMove = testRectangle.getArea();
  
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(MiddleMoveTest)
{
  alekseev::Rectangle testRectangle({3.2, 3.3}, 2.0, 4.0);
  alekseev::rectangle_t frameBeforeMove = testRectangle.getFrameRect();
  double areaBeforeMove = testRectangle.getArea();
  
  testRectangle.move({2.0, 2.0});
  alekseev::rectangle_t frameAfterMove = testRectangle.getFrameRect();
  double areaAfterMove = testRectangle.getArea();
  
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(TestScale)
{
  alekseev::Rectangle testRectangle({3.2, 3.3}, 2.0, 4.0);
  double areaBeforeScale = testRectangle.getArea();
  const double k = 3;
  testRectangle.scale(k);
  double areaAfterScale = testRectangle.getArea();
  
  BOOST_CHECK_CLOSE(areaBeforeScale * k * k, areaAfterScale, EPSILON);
  
}

BOOST_AUTO_TEST_CASE(IncorrectWidtgTest)
{
  BOOST_CHECK_THROW(alekseev::Rectangle({3.2, 3.3}, -2.0, 4.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(IncorrectHeightTest)
{
  BOOST_CHECK_THROW(alekseev::Rectangle({3.2, 3.3}, 2.0, -4.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(IncorrectScaleTest)
{
  alekseev::Rectangle testRectangle({3.2, 3.3}, 2.0, 4.0);

  BOOST_CHECK_THROW(testRectangle.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestTriangle)

BOOST_AUTO_TEST_CASE(CoordinateMoveTest)
{
  alekseev::Triangle testTriangle({4.3, 6.5} ,{2.2, 3.1}, {5.4, 6.8});
  alekseev::rectangle_t frameBeforeMove = testTriangle.getFrameRect();
  double areaBeforeMove = testTriangle.getArea();
  
  testTriangle.move(2.0, 2.0);
  alekseev::rectangle_t frameAfterMove = testTriangle.getFrameRect();
  double areaAfterMove = testTriangle.getArea();
  
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, EPSILON);
}
BOOST_AUTO_TEST_CASE(MiddleMoveTest)
{
  alekseev::Triangle testTriangle({4.3, 6.5} ,{2.2, 3.1}, {5.4, 6.8});
  alekseev::rectangle_t frameBeforeMove = testTriangle.getFrameRect();
  double areaBeforeMove = testTriangle.getArea();
  
  testTriangle.move({2.0, 2.0});
  alekseev::rectangle_t frameAfterMove = testTriangle.getFrameRect();
  double areaAfterMove = testTriangle.getArea();
  
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(TestScale)
{
  alekseev::Triangle testTriangle({4.3, 6.5} ,{2.2, 3.1}, {5.4, 6.8});
  double areaBeforeScale = testTriangle.getArea();
  const double k = 3;
  testTriangle.scale(k);
  double areaAfterScale = testTriangle.getArea();
  
  BOOST_CHECK_CLOSE(areaBeforeScale * k * k, areaAfterScale, EPSILON);
}

BOOST_AUTO_TEST_CASE(IncorrectYInitTest)
{
  BOOST_CHECK_THROW(alekseev::Triangle({2.0, 3.0}, {7.0, 3.0}, {4.0, 3.0}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(IncorrectXInitTest)
{
  BOOST_CHECK_THROW(alekseev::Triangle({8.0, 2.0}, {8.0, 3.0}, {8.0, 4.0}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(IncorrectScaleTest)
{
  alekseev::Triangle testTriangle({6.0, 6.0}, {1.0, 3.0}, {4.0, 5.0});
  
  BOOST_CHECK_THROW(testTriangle.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestPolygon)

BOOST_AUTO_TEST_CASE(CoordinateMoveTest)
{
  const int sides = 6;
  const alekseev::point_t points[sides] = {{2.0, 3}, {4.3, 3}, {5, 4.75}, {4, 7.4}, {1.9, 7}, {-2.25, 4}};
  alekseev::Polygon testPolygon(sides, points);
  alekseev::rectangle_t frameBeforeMove = testPolygon.getFrameRect();
  double areaBeforeMove = testPolygon.getArea();
  
  testPolygon.move(2.0, 2.0);
  alekseev::rectangle_t frameAfterMove = testPolygon.getFrameRect();
  double areaAfterMove = testPolygon.getArea();
  
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(MiddleMoveTest)
{
  const int sides = 6;
  const alekseev::point_t points[sides] = {{2.0, 3}, {4.3, 3}, {5, 4.75}, {4, 7.4}, {1.9, 7}, {-2.25, 4}};
  alekseev::Polygon testPolygon(sides, points);
  alekseev::rectangle_t frameBeforeMove = testPolygon.getFrameRect();
  double areaBeforeMove = testPolygon.getArea();
  
  testPolygon.move({2.0, 2.0});
  alekseev::rectangle_t frameAfterMove = testPolygon.getFrameRect();
  double areaAfterMove = testPolygon.getArea();
  
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(TestScale)
{
  const int sides = 6;
  const alekseev::point_t points[sides] = {{2.0, 3}, {4.3, 3}, {5, 4.75}, {4, 7.4}, {1.9, 7}, {-2.25, 4}};
  alekseev::Polygon testPolygon(sides, points);
  double areaBeforeScale = testPolygon.getArea();
  const double k = 3;
  testPolygon.scale(k);
  double areaAfterScale = testPolygon.getArea();
  
  BOOST_CHECK_CLOSE(areaBeforeScale * k * k, areaAfterScale, EPSILON);
}

BOOST_AUTO_TEST_CASE(IncorrectParametrsTest)
{
  const int sides = 2;
  const alekseev::point_t points[sides] = {{2.0, 3}, {4.3, 3}};
  
  BOOST_CHECK_THROW(alekseev::Polygon testPolygon(sides, points), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(IncorrectScaleTest)
{
  const int sides = 6;
  const alekseev::point_t points[sides] = {{2.0, 3}, {4.3, 3}, {5, 4.75}, {4, 7.4}, {1.9, 7}, {-2.25, 4}};
  alekseev::Polygon testPolygon(sides, points);

  BOOST_CHECK_THROW(testPolygon.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
