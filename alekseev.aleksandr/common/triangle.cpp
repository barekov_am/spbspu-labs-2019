#include "triangle.hpp"
#include <cassert>
#include <algorithm>
#include <cmath>
#include <stdexcept>

alekseev::Triangle::Triangle(const point_t & point0, const point_t & point1, const point_t & point2):
  point0_(point0),
  point1_(point1),
  point2_(point2),
  middle_({(point0_.x + point1_.x + point2_.x) / 3, (point0_.y + point1_.y + point2_.y) / 3})
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("Points lie on the same line");
  }
}

double findLengthSide(const alekseev::point_t & point0, const alekseev::point_t point1)
{
  alekseev::point_t side = {(point1.x - point0.x),(point1.y - point0.y)};
  return sqrt(side.x * side.x + side.y * side.y);
}

double alekseev::Triangle::getArea() const
{
  double side1 = findLengthSide(point0_, point1_);
  double side2 = findLengthSide(point0_, point2_);
  double side3 = findLengthSide(point1_, point2_);
  double halfPerimeter = (side1 + side2 + side3) / 2;
  return sqrt(halfPerimeter * (halfPerimeter - side1) * (halfPerimeter - side2) * (halfPerimeter - side3));
}

alekseev::rectangle_t alekseev::Triangle::getFrameRect() const
{
  double maxX = std::max({point0_.x, point1_.x, point2_.x});
  double maxY = std::max({point0_.y, point1_.y, point2_.y});
  double minX = std::min({point0_.x, point1_.x, point2_.x});
  double minY = std::min({point0_.y, point1_.y, point2_.y});
  const double height = maxY - minY;
  const double width = maxX - minX;
  const point_t center = {minX + width / 2, minY + height / 2};
  return {width, height, center};
};

void alekseev::Triangle::move(double dx, double dy)
{
  middle_.x += dx;
  middle_.y += dy;
  
  point0_.x += dx;
  point0_.y += dy;
  
  point1_.x += dx;
  point1_.y += dy;
  
  point2_.x += dx;
  point2_.y += dy;
}

void alekseev::Triangle::move(const point_t &newMid)
{
  move((newMid.x - middle_.x), (newMid.y - middle_.y));
}

void alekseev::Triangle::scale(double multiplier)
{
  if (multiplier <= 0.0)
  {
    throw std::invalid_argument("Scale multiplier <= 0");
  }
  else
  {
    point0_.x = middle_.x + (point0_.x - middle_.x) * multiplier;
    point0_.y = middle_.y + (point0_.y - middle_.y) * multiplier;
    
    point1_.x = middle_.x + (point1_.x - middle_.x) * multiplier;
    point1_.y = middle_.y + (point1_.y - middle_.y) * multiplier;
    
    point2_.x = middle_.x + (point2_.x - middle_.x) * multiplier;
    point2_.y = middle_.y + (point2_.y - middle_.y) * multiplier;
  }
}
