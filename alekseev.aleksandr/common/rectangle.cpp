#include "rectangle.hpp"
#include <stdexcept>
#include <iostream>

alekseev::Rectangle::Rectangle(const point_t &middle, const double width, const double height):
  width_(width),
  height_(height),
  middle_(middle)
{
  if ((width_ < 0.0) || (height_ < 0.0))
  {
    throw  std::invalid_argument ("Invalid Rectangle\n");
  }
}

double alekseev::Rectangle::getArea() const
{
  return width_ * height_;
}

alekseev::rectangle_t alekseev::Rectangle::getFrameRect() const
{
  return {width_, height_, middle_};
}

void alekseev::Rectangle::move(double dx, double dy)
{
  middle_.x += dx;
  middle_.y += dy;
}

void alekseev::Rectangle::move(const point_t &newMid)
{
  middle_ = newMid;
}

void alekseev::Rectangle::scale(double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("Scale multiplier <= 0");
  }
  else
  {
    width_ *=multiplier;
    height_ *= multiplier;
  }
}
