#include "circle.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

alekseev::Circle::Circle(const point_t &middle, const double radius):
  middle_(middle),
  radius_(radius)
{
  if (radius < 0.0)
  {
    throw std::invalid_argument ("Invalid radius\n");
  }
}

double alekseev::Circle::getArea() const
{
  return radius_ * radius_ * M_PI;
}

alekseev::rectangle_t alekseev::Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, middle_};
}

void alekseev::Circle::move(double dx, double dy)
{
  middle_.x += dx;
  middle_.y += dy;
}

void alekseev::Circle::move(const point_t &newMid)
{
  middle_ = newMid;
}

void alekseev::Circle::scale(double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("Scale multiplier <= 0");
  }
  else
  {
    radius_ *= multiplier;
  }
}
