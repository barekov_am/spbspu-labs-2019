#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"
namespace alekseev
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &point0, const point_t & point1, const point_t & point2);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & newMid) override;
    void move(double dx, double dy) override;
    void scale(double multiplier) override;
  private:
    point_t point0_;
    point_t point1_;
    point_t point2_;
    point_t middle_;
  };
}
#endif
