#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace alekseev
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t & middle, const double width, const double height);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & newMid) override;
    void move(double dx,  double dy) override;
    void scale(double multiplier) override;
  private:
    double width_;
    double height_;
    point_t middle_;
  };
}
#endif
