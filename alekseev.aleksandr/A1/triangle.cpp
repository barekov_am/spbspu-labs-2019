#include "triangle.hpp"
#include <cassert>
#include <algorithm>
#include <cmath>

Triangle::Triangle(const point_t & point0, const point_t & point1, const point_t & point2):
  point0_(point0),
  point1_(point1),
  point2_(point2),
  middle_({(point0_.x + point1_.x + point2_.x) / 3, (point0_.y + point1_.y + point2_.y) / 3})
{
  assert(getArea() > 0.0);
}

double findLengthSide(const point_t & point0, const point_t point1)
{
  point_t side = {(point1.x - point0.x),(point1.y - point0.y)};
  return sqrt(side.x * side.x + side.y * side.y);
}

double Triangle::getArea() const
{
  double side1 = findLengthSide(point0_, point1_);
  double side2 = findLengthSide(point0_, point2_);
  double side3 = findLengthSide(point1_, point2_);
  double halfPerimeter = (side1 + side2 + side3) / 2;
  return sqrt(halfPerimeter * (halfPerimeter - side1) * (halfPerimeter - side2) * (halfPerimeter - side3));
}

rectangle_t Triangle::getFrameRect() const
{
  double maxX = std::max({point0_.x, point1_.x, point2_.x});
  double maxY = std::max({point0_.y, point1_.y, point2_.y});
  double minX = std::min({point0_.x, point1_.x, point2_.x});
  double minY = std::min({point0_.y, point1_.y, point2_.y});
  const double height = maxY - minY;
  const double width = maxX - minX;
  const point_t center = {minX + width / 2, minY + height / 2};
  return {width, height, center};
};

void Triangle::move(double dx, double dy)
{
  middle_.x += dx;
  middle_.y += dy;
  
  point0_.x += dx;
  point0_.y += dy;
  
  point1_.x += dx;
  point1_.y += dy;
  
  point2_.x += dx;
  point2_.y += dy;
}

void Triangle::move(const point_t &newMid)
{
  move((newMid.x - middle_.x), (newMid.y - middle_.y));
}
