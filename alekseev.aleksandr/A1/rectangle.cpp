#include "rectangle.hpp"
#include <iostream>

Rectangle::Rectangle(const point_t &middle, const double width, const double height):
  width_(width),
  height_(height),
  middle_(middle)
{
  if ((width_ < 0.0) || (height_ < 0.0))
  {
    std::cerr << "Invalid Rectangle\n";
  }
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return {width_, height_, middle_};
}

void Rectangle::move(double dx, double dy)
{
  middle_.x += dx;
  middle_.y += dy;
}

void Rectangle::move(const point_t &newMid)
{
  middle_ = newMid;
}
