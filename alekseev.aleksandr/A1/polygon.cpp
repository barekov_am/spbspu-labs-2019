#include "polygon.hpp"
#include <iostream>
#include <ctime>
#include <cmath>

Polygon::Polygon(const Polygon &other):
  sides_(other.sides_),
  points_(new point_t[sides_])
{
  for (int i = 0; i < sides_; i++)
  {
    points_[i] = other.points_[i];
  }
}

Polygon::Polygon(Polygon &&other) noexcept:
  sides_(other.sides_),
  points_(other.points_)
{
  other.sides_ = 0;
  other.points_ = nullptr;
}

Polygon::Polygon(const int sides, const point_t *points):
  sides_(sides),
  points_(new point_t[sides])
{
  if (sides_ < 3)
  {
    delete [] points_;
    throw std::invalid_argument("The number of sides should be more than 2");
  }
  
  if (points == nullptr)
  {
    delete[] points_;
    throw std::invalid_argument("Pointer cannot be nullptr");
  }
  
  for (int i = 0; i < sides_; i++)
  {
    points_[i] = points[i];
  }
  if (!isConvex())
  {
    delete[] points_;
    throw std::invalid_argument("Polygon is concave");
  }
}

Polygon::~Polygon()
{
  delete[] points_;
}

Polygon &Polygon::operator=(const Polygon &other)
{
  if (this != &other)
  {
    sides_ = other.sides_;
    delete[] points_;
    points_ = new point_t[sides_];
    for (int i = 0; i < sides_; i++)
    {
      points_[i] = other.points_[i];
    }
  }
  return  *this;
}

Polygon &Polygon::operator=(Polygon &&other) noexcept
{
  if (this != &other)
  {
    sides_ = other.sides_;
    other.sides_ = 0;
    delete[] points_;
    points_ = other.points_;
    other.points_ = nullptr;
  }
  return *this;
}

point_t Polygon::getCenter() const
{
  point_t centerTemp = {0.0, 0.0};
  
  for (int i = 0; i < sides_; i++)
  {
    centerTemp.x += points_[i].x;
    centerTemp.y += points_[i].y;
  }
  return {centerTemp.x / sides_, centerTemp.y / sides_};
}

bool Polygon::isConvex() const
{
  int sign = 0;
  for (int i = 0; i < sides_; i++) {
    const int j = (i + 1) % sides_;
    const int k = (i + 2) % sides_;
    const double determinant = (points_[j].x - points_[i].x) * (points_[k].y - points_[j].y)
        - (points_[k].x - points_[j].x) * (points_[j].y - points_[i].y);
    if (determinant != 0) {
      if (sign == 0) {
        sign = determinant > 0 ? 1 : -1;
      } else if ((sign * determinant) < 0) {
        return false;
      }
    }
  }
  return sign != 0;
}

double Polygon::getArea() const
{
  double sum = 0;
  
  for (int i = 0; i < sides_ - 1; i++)
  {
    sum += points_[i].x * points_[i + 1].y;
    sum -= points_[i + 1].x * points_[i].y;
  }
  sum += points_[sides_ - 1].x * points_[0].y;
  sum -= points_[0].x * points_[sides_ - 1].y;
  
  return fabs(sum) / 2;
}

rectangle_t Polygon::getFrameRect() const
{
  double min_x = points_[0].x;
  double max_x = points_[0].x;
  double min_y = points_[0].y;
  double max_y = points_[0].y;
  
  for (int i = 1; i < sides_; i++)
  {
    min_x = std::min(min_x, points_[i].x);
    max_x = std::max(max_x, points_[i].x);
    min_y = std::min(min_y, points_[i].y);
    max_y = std::max(max_y, points_[i].y);
  }
  return {max_x - min_x, max_y - min_y, {min_x + (max_x - min_x) / 2, + (max_y - min_y) / 2}};
}

void Polygon::move(const point_t &newPoint)
{
  const point_t center_ = getCenter();
  move(newPoint.x - center_.x, newPoint.y - center_.y);
}

void Polygon::move(double move_x, double move_y)
{
  for (int i = 0; i < sides_; i++)
  {
    points_[i].x += move_x;
    points_[i].y += move_y;
  }
}
