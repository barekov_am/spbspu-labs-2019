#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

class Rectangle : public Shape
{
public:
  Rectangle(const point_t & middle, const double width, const double height);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t & newMid) override;
  void move(double dx,  double dy) override;
private:
  double width_;
  double height_;
  point_t middle_;
};

#endif
