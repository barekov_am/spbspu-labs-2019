#ifndef POLYGON_HPP
#define POLYGON_HPP
#include "shape.hpp"

class Polygon : public Shape
{
public:
  Polygon(const Polygon &other);
  Polygon(Polygon &&other) noexcept;
  
  Polygon(const int sides, const point_t *points);
  ~Polygon() override;
  
  Polygon &operator = (const Polygon &other);
  Polygon &operator = (Polygon &&other) noexcept;
  
  point_t getCenter () const;
  bool isConvex() const;
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &) override;
  void move(const double x, const double y) override;
  
private:
  int sides_;
  point_t *points_;
};

#endif
