#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void printInfo(const Shape & object)
{
  std::cout << "Area: " << object.getArea() << std::endl;
  const rectangle_t frame = object.getFrameRect();
  std::cout << "Frame for object:" << std::endl;
  std::cout << "\tMiddle of frame: " << frame.pos.x << ", " << frame.pos.y << std::endl;
  std::cout << "\tWidth of frame:" << frame.width << std::endl;
  std::cout << "\tHeight of frame" << frame.height << std::endl;
}

int main()
{
  Rectangle rectangle({22.04, 7.16}, 9.23, 3.42);
  std::cout << "Rectangle:" << std::endl;
  printInfo(rectangle);
  rectangle.move(-3.22, 17.13);
  printInfo(rectangle);
  rectangle.move({13.2, 10.1});
  printInfo(rectangle);
  
  Circle circle({22.04, 7.16}, 9.23);
  std::cout << "Circle:" << std::endl;
  printInfo(circle);
  circle.move(-3.22, 17.13);
  printInfo(circle);
  circle.move({13.2, 10.1});
  printInfo(circle);
  
  Triangle triangle({4.3, 6.5}, {2.2, 3.1}, {5.4, 6.8});
  std::cout << "Triangle:" << std::endl;
  printInfo(triangle);
  triangle.move(-3.22, 17.13);
  printInfo(triangle);
  triangle.move({13.2, 10.1});
  printInfo(triangle);
  
  std::cout << "Polygon:" << std::endl;
  const int sides = 6;
  const point_t points[sides] = {{2.0, 3}, {4.3, 3}, {5, 4.75}, {4, 7.4}, {1.9, 7}, {-2.25, 4}};
  Polygon polygon(sides, points);
  printInfo(polygon);
  polygon.move({13.2, 10.1});
  printInfo(polygon);
  
  Polygon polygon1(polygon);
  printInfo(polygon1);
  polygon1.move(2.1, 5.5);
  
  Polygon polygon2(std::move(polygon1));
  printInfo(polygon2);
  
  polygon1 = polygon2;
  printInfo(polygon1);
  
  polygon2 = (std::move(polygon1));
  printInfo(polygon2);
  
  return 0;
}
