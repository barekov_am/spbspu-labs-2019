#ifndef circle_hpp
#define circle_hpp
#include "shape.hpp"

class Circle : public Shape
{
public:
  Circle(const point_t & middle, const double radius);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t & newMid) override;
  void move(double dx, double dy) override;
private:
  point_t middle_;
  double radius_;
};

#endif
