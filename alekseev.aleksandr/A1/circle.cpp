#include "circle.hpp"
#include <iostream>
#include <cmath>

Circle::Circle(const point_t &middle, const double radius):
  middle_(middle),
  radius_(radius)
{
  if (radius < 0.0)
  {
    std::cerr << "Invalid radius\n";
  }
}

double Circle::getArea() const
{
  return radius_ * radius_ * M_PI;
}

rectangle_t Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, middle_};
}

void Circle::move(double dx, double dy)
{
  middle_.x += dx;
  middle_.y += dy;
}

void Circle::move(const point_t &newMid)
{
  middle_ = newMid;
}
