#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include <iterator>
#include <map>
#include <cstring>

#include "textunits.hpp"

using VariantPair = std::pair<bool, TextUnit::pointer>;
using VariantsVector = std::vector<VariantPair>;
using TextList = std::list<TextUnitInstance>;

const char * LINE_WIDTH = "--line-width";
const size_t DEFAULT_WIDTH = 40;
const size_t MIN_WIDTH = 25;

size_t hasTrueVariant(VariantsVector &vector)
{
  size_t result = 0;
  for (size_t i = 0; i < vector.size(); ++i)
  {
    if (vector[i].first)
    {
      result = i + 1;
      break;
    }
  }
  return result;
}

TextList::iterator getNext(TextList::iterator current, const TextList::iterator & end)
{
  if (current->type == space)
  {
    // skip the space
    ++current;
  }

  if (current != end)
  {
    // skip the word or number
    ++current;
  }

  if (current != end)
  {
    if (current->type == punctuation)
    {
      // skip the punctuation symbol and the space after
      return ++current;
    }
    else
    {
      auto next = current;
      // skip the space after word/number
      ++next;
      if (next != end && next->type == punctuation)
      {
        // if the next is dash then skip it and the following space
        return ++next;
      }
      // current++ != dash
      else
      {
        return current;
      }
    }
  }

  return current;
}

int main(int argc, char *argv[])
{

  if ((argc != 1 && argc != 3) || (argc == 3 && strcmp(LINE_WIDTH, argv[1]) != 0))
  {
    std::cerr << "Invalid arguments" << std::endl;
    return 1;
  }

  size_t maxLength = DEFAULT_WIDTH;
  if (argc == 3)
  {
    try
    {
      maxLength = std::stoi(argv[2]);
      if (maxLength < MIN_WIDTH)
      {
        throw std::invalid_argument("");
      }
    }
    catch (std::invalid_argument &)
    {
      std::cerr << "Invalid arguments" << std::endl;
      return 1;
    }
  }

  TextList textList;

  while (!std::cin.eof() || !std::cin.fail())
  {
    VariantsVector variants;
    variants.emplace_back(true, std::make_shared<WordTextUnit>());
    variants.emplace_back(true, std::make_shared<SpaceTextUnit>());
    variants.emplace_back(true, std::make_shared<PunctuationTextUnit>());
    variants.emplace_back(true, std::make_shared<NumberTextUnit>());

    size_t result = 1;
    size_t oldResult;
    char prevChar = 0;

    bool hasVariant = false;
    while (true) {
      oldResult = result;

      char next = std::cin.peek();
      std::for_each(variants.begin(), variants.end(), [&next](VariantPair &variantPair)
      {
        if (variantPair.first)
        {
          variantPair.first = variantPair.second->push(next);
        }
      });
      result = hasTrueVariant(variants);
      if (result)
      {
        hasVariant = true;
        prevChar = std::cin.get();
      }
      else
      {
        break;
      }
    }

    if (prevChar == HYPHEN && std::cin.peek() == HYPHEN)
    {
      std::cin.putback(prevChar);
    }

    if (!hasVariant)
    {
      if (std::cin.peek() == std::char_traits<char>::eof())
      {
        break;
      }
      std::cerr << "Unknown text unit: " << std::cin.peek() << std::endl;
      return 1;
    }

    auto textUnitInstance = variants[oldResult - 1].second->getData();

    if (textUnitInstance.type == punctuation && (textList.empty() ||
        (textList.size() == 1 && textList.front().type == space)))
    {
      std::cerr << "The text cannot be start with punctuation" << std::endl;
      return 1;
    }

    if (textList.empty())
    {
      textList.push_back(textUnitInstance);
    }
    else
    {
      auto prev = --textList.end();

      switch (textUnitInstance.type)
      {
        case number:
        case word:
          if (prev->type == word || prev->type == number)
          {
            std::cerr << "Invalid input data: words and numbers must have spaces between" << std::endl;
            return 1;
          }
          textList.push_back(textUnitInstance);
          break;
        case space:
          textList.push_back(textUnitInstance);
          break;

        case punctuation:
          if (prev->type == space && prev != textList.begin())
          {
            --prev;
          }

          const bool isPunctuationPrev = prev->type == punctuation;
          const bool isDashCurrent = textUnitInstance.data.length() == DASH_LENGTH;
          const bool invalidDash = textUnitInstance.data.length() == DASH_LENGTH - 1;
          const bool isCommaPrev = prev->data[0] == COMMA;

          if (isPunctuationPrev && !(isDashCurrent && isCommaPrev))
          {
            std::cerr << "Invalid input data: punctuation marks cannot follow each other" << std::endl;
            return 1;
          } else if (invalidDash)
          {
            std::cerr << "Invalid input data: wrong dash" << std::endl;
            return 1;
          }
          textList.push_back(textUnitInstance);
          break;
      }
    }
  }

  if (!textList.empty())
  {

    for (auto current = textList.begin(); current != --textList.end(); ++current)
    {
      auto next = current;
      next++;

      if (next->type == punctuation)
      {
        if (current->type != space && next->data.length() == DASH_LENGTH)
        {
          textList.insert(next, {" ", space});
        }
        else if (current->type == space && next->data.length() != DASH_LENGTH)
        {
          current = --textList.erase(current);
        }
      }
      else if (current->type == punctuation && next->type != space)
      {
        textList.insert(next, {" ", space});
      }
    }
  }

  if (!textList.empty())
  {
    size_t currentLength = 0;

    TextList::iterator current = textList.begin();
    TextList::iterator end = textList.end();
    TextList::iterator nextWord;
    do
    {
      nextWord = getNext(current, end);

      size_t len = 0;
      std::for_each(current, nextWord, [&len](TextUnitInstance & textUnit)
      {
        len += textUnit.data.length();
      });

      if (len > maxLength)
      {
        std::cerr << "It is impossible to place words with a given length of string" << std::endl;
        return 2;
      }


      if (currentLength + len > maxLength)
      {
        current->endLine = TextUnitInstance::before;
      }
      else if (nextWord != end && currentLength + len == maxLength)
      {
        nextWord->endLine = TextUnitInstance::before;
      }

      currentLength = (currentLength + len) > maxLength
          ? len
          : (currentLength + len) % maxLength;

      if (current->endLine == TextUnitInstance::before) {
        --currentLength;
      }

      current = nextWord;
    } while (nextWord != end);
  }

  if (textList.size() == 1 && textList.front().type == space) {
    return 0;
  }

  std::copy(textList.begin(), textList.end(), std::ostream_iterator<TextUnitInstance>(std::cout));
  std::cout << std::endl;

  return 0;
}
