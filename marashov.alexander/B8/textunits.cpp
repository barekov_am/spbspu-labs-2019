#include "textunits.hpp"

TextUnitInstance TextUnit::getData() const
{
  return { data_, type_ };
}

TextUnit::TextUnit():
  data_(""),
  type_()
{

}

bool WordTextUnit::push(const char symbol)
{
  const size_t length = data_.length();

  const bool isCorrectSymbol = std::isalpha(symbol) || (symbol == HYPHEN && length && data_[length - 1] != HYPHEN);
  const bool hasPlace = length < MAX_WORD_LENGTH;
  const bool result = isCorrectSymbol && hasPlace;

  if (result)
  {
    data_.push_back(symbol);
  } else if (symbol == HYPHEN && length && data_[length - 1] == HYPHEN)
  {
    data_.pop_back();
  }

  return result;
}

WordTextUnit::WordTextUnit():
  TextUnit()
{
  type_ = word;
}


bool NumberTextUnit::push(const char symbol)
{
  const size_t length = data_.length();

  const bool rightSign = !length && (symbol == PLUS || symbol == MINUS);
  const bool rightSeparator = (symbol == *localeconv()->decimal_point && !hasPoint && !data_.empty());
  const bool isDigit = std::isdigit(symbol);
  const bool hasPlace = length < MAX_NUMBER_LENGTH;
  const bool result = (isDigit && hasPlace) || rightSign || rightSeparator;

  if (result)
  {
    data_.push_back(symbol);
    hasPoint |= rightSeparator;
  }
  return result;
}

NumberTextUnit::NumberTextUnit():
  TextUnit(),
  hasPoint(false)
{
  type_ = number;
}

bool SpaceTextUnit::push(const char symbol)
{
  const bool result = std::isspace(symbol);

  if (result && data_.empty())
  {
    data_.push_back(' ');
  }
  return result;
}

SpaceTextUnit::SpaceTextUnit():
  TextUnit()
{
  type_ = space;
}

bool PunctuationTextUnit::push(char symbol)
{
  const size_t length = data_.length();

  const bool isCorrectSymbol = std::ispunct(symbol);
  const bool hasPlace = !length || (symbol == HYPHEN && length < DASH_LENGTH && data_.back() == HYPHEN);
  const bool result = isCorrectSymbol && hasPlace;

  if (result)
  {
    data_.push_back(symbol);
  }
  return result;
}

PunctuationTextUnit::PunctuationTextUnit():
  TextUnit()
{
  type_ = punctuation;
}

std::ostream & operator <<(std::ostream & out, const TextUnitInstance & textUnitInstance)
{
  if (textUnitInstance.endLine == TextUnitInstance::EndLine::before)
  {
    out << std::endl;
    if (textUnitInstance.data.back() == SPACE)
    {
      return out;
    }
  }

  out << textUnitInstance.data;

  if (textUnitInstance.endLine == TextUnitInstance::EndLine::after)
  {
    out << std::endl;
  }
  return out;
}
