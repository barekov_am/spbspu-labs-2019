#ifndef B8_TEXTUNITS_HPP
#define B8_TEXTUNITS_HPP


#include <string>
#include <memory>
#include <iomanip>

const size_t MAX_WORD_LENGTH = 20;
const size_t MAX_NUMBER_LENGTH = 20;
const size_t DASH_LENGTH = 3;

const char HYPHEN = '-';
const char PLUS = '+';
const char MINUS = '-';
const char COMMA = ',';
const char SPACE = ' ';

enum TextUnitType
{
  space = 0,
  number = 1,
  word = 2,
  punctuation = 3
};

struct TextUnitInstance
{
  enum EndLine
  {
    none = 0,
    before = 1,
    after = 2
  };
  std::string data;
  TextUnitType type;
  EndLine endLine = none;
};

std::ostream & operator <<(std::ostream & out, const TextUnitInstance & textUnitInstance);

class TextUnit
{
public:
  using pointer = std::shared_ptr<TextUnit>;

  TextUnit();
  virtual ~TextUnit() = default;

  virtual bool push(char symbol) = 0;
  TextUnitInstance getData() const;

protected:
  std::string data_;
  TextUnitType type_;
};

class WordTextUnit: public TextUnit
{
public:
  WordTextUnit();
  bool push(char symbol) override;
};

class NumberTextUnit: public TextUnit
{
public:
  NumberTextUnit();
  bool push(char symbol) override;
private:
  bool hasPoint;
};

class SpaceTextUnit: public TextUnit
{
public:
  SpaceTextUnit();
  bool push(char symbol) override;
};

class PunctuationTextUnit: public TextUnit
{
public:
  PunctuationTextUnit();
  bool push(char symbol) override;
};

#endif //B8_TEXTUNITS_HPP
