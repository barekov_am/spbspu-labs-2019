#include <iostream>

#include "../common/composite-shape.hpp"
#include "../common/rectangle.hpp"
#include "../common/circle.hpp"
#include "../common/matrix.hpp"
#include "../common/shape-splitter.hpp"

int main()
{
  try
  {
    marashov::CompositeShape compositeShape;

    marashov::ShapePointer rectangle1(std::make_shared<marashov::Rectangle>(0, 0, 5, 5));
    marashov::ShapePointer rectangle2(std::make_shared<marashov::Rectangle>(2, 2, 5, 5));
    marashov::ShapePointer rectangle3(std::make_shared<marashov::Rectangle>(13, 10, 0.1, 0.1));

    marashov::ShapePointer circle1(std::make_shared<marashov::Circle>(10, 10, 1));
    marashov::ShapePointer circle2(std::make_shared<marashov::Circle>(10, 10, 3));

    compositeShape.add(rectangle1);
    compositeShape.add(rectangle2);
    compositeShape.add(circle1);
    compositeShape.add(circle2);
    compositeShape.add(rectangle3);

    compositeShape.writeInfo();
    compositeShape.rotate(45);
    compositeShape.writeInfo();

    marashov::Matrix matrix = marashov::split(compositeShape);
    for (unsigned int i = 0; i < matrix.rows(); i++)
    {
      std::cout << "layer = " << i << ":" << std::endl;
      for (unsigned int j = 0; j < matrix.columns(i); j++)
      {
        matrix[i][j]->writeInfo();
      }
    }
  }
  catch (const std::invalid_argument & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 1;
  }
  catch (const std::out_of_range & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 2;
  }
  catch (const std::logic_error & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 2;
  }

  return 0;
}
