#include <iostream>
#include "statistics.hpp"

Statistics::Statistics():
    max_(0),
    min_(0),
    positiveCount_(0),
    negativeCount_(0),
    zeroCount_(0),
    evenSum_(0),
    oddSum_(0),
    firstNumber_(0),
    hasNumbers_(false),
    isFirstLastEquals_(true)
{

}

void Statistics::operator ()(const long long & number)
{
  if (hasNumbers_)
  {
    max_ = std::max(max_, number);
    min_ = std::min(min_, number);
    isFirstLastEquals_ = (number == firstNumber_);
  }
  else
  {
    firstNumber_ = number;
    max_ = number;
    min_ = number;
    hasNumbers_ = true;
  }

  if (number > 0)
  {
    ++positiveCount_;
  }
  else if (number < 0)
  {
    ++negativeCount_;
  }
  else
  {
    ++zeroCount_;
  }

  if (number % 2)
  {
    oddSum_ += number;
  }
  else
  {
    evenSum_ += number;
  }
}

std::ostream & operator <<(std::ostream & out, const Statistics & statistics)
{
  if (statistics.hasNumbers_)
  {
    out << "Max: " << statistics.max_ << std::endl
        << "Min: " << statistics.min_ << std::endl
        << "Mean: " << std::fixed << statistics.getAverage() << std::endl
        << "Positive: " << statistics.positiveCount_ << std::endl
        << "Negative: " << statistics.negativeCount_ << std::endl
        << "Odd Sum: " << statistics.oddSum_ << std::endl
        << "Even Sum: " << statistics.evenSum_ << std::endl
        << "First/Last Equal: " << statistics.isFirstEqualsLast() << std::endl;
  }
  else
  {
    out << "No Data" << std::endl;
  }
  return out;
}

double Statistics::getAverage() const
{
  return static_cast<double>(evenSum_ + oddSum_) / static_cast<double>(positiveCount_ + negativeCount_ + zeroCount_);
}

std::string Statistics::isFirstEqualsLast() const
{
  return isFirstLastEquals_ && hasNumbers_ ? "yes" : "no";
}
