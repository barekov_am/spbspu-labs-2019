#ifndef B6_STATISTICS_HPP
#define B6_STATISTICS_HPP

#include <algorithm>
#include <ostream>

class Statistics
{
public:
  Statistics();
  void operator ()(const long long & number);
  friend std::ostream & operator <<(std::ostream & out, const Statistics & statistics);

private:

  long long max_;
  long long min_;

  unsigned long long positiveCount_;
  unsigned long long negativeCount_;
  unsigned long long zeroCount_;

  long long evenSum_;
  long long oddSum_;

  long long firstNumber_;
  bool hasNumbers_;
  bool isFirstLastEquals_;

  double getAverage() const;
  std::string isFirstEqualsLast() const;
};

#endif //B6_STATISTICS_HPP
