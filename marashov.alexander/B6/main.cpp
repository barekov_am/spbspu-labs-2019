#include <iostream>
#include <forward_list>
#include <iterator>
#include <algorithm>

#include "statistics.hpp"

int main() {
  std::forward_list<long long> list;
  std::copy(std::istream_iterator<long>(std::cin), std::istream_iterator<long>(), std::front_inserter(list));
  if (!std::cin.eof() && std::cin.fail())
  {
    std::cerr << "Error reading data" << std::endl;
    return 2;
  }

  Statistics statistics;
  statistics = std::for_each(list.cbegin(), list.cend(), statistics);
  std::cout << statistics;

  return 0;
}
