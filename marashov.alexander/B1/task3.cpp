#include <iostream>
#include <vector>

#include "printing.hpp"

void task3()
{
  int number = -1;
  std::vector<int> numbers(0);
  while (std::cin >> number)
  {
    if (number == 0)
    {
      break;
    }
    numbers.push_back(number);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Error reading data");
  }

  if (numbers.empty())
  {
    return;
  }

  if (number != 0)
  {
    throw std::invalid_argument("Incorrect input. The number sequence must end with 0");
  }

  std::vector<int>::iterator iterator = numbers.begin();

  switch (*(numbers.end() - 1))
  {
    case 1:
    {
      while ((iterator != numbers.end()) && (!((*iterator) & 1)))
      {
        numbers.erase(iterator);
        iterator = numbers.begin();
      }

      if (iterator == numbers.end())
      {
        std::cout << std::endl;
        return;
      }

      while (iterator + 1 != numbers.end())
      {
        if (!((*(iterator + 1)) & 1))
        {
          numbers.erase(iterator + 1);
        }
        else
        {
          ++iterator;
        }
      }
      break;
    }

    case 2:
    {
      const int value = 1;
      const int counts = 3;
      while (iterator != numbers.end())
      {
        if ((*iterator) % 3 == 0)
        {
          iterator = numbers.insert(iterator + 1, counts, value);
          iterator += 3;
        }
        else
        {
          ++iterator;
        }
      }
    }
  }
  printCollection(numbers);
  return;
}

