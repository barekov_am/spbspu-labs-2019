#ifndef SORT_HPP
#define SORT_HPP

#include <functional>

template <class T>
using CompareFunction = std::function<bool (const T & a, const T & b)>;

template <class T>
CompareFunction<T> descendingComp = [](const T & a, const T & b) -> bool
{
  return a < b;
};

template <class T>
CompareFunction<T> ascendingComp = [](const T & a, const T & b) -> bool
{
  return a > b;
};

template <template <class T> class Admission, class T>
void sort(T & collection, CompareFunction<typename T::value_type> comparator)
{
  using Index = typename Admission<T>::Index;
  Index beginIndex = Admission<T>::begin(collection);
  Index endIndex = Admission<T>::end(collection);

  for (Index i = beginIndex; i != endIndex; ++i)
  {
    for (Index j = i; j != endIndex; ++j)
    {
      if ((comparator)(Admission<T>::get(collection, i), Admission<T>::get(collection, j)))
      {
        typename T::value_type tmp = Admission<T>::get(collection, i);
        Admission<T>::get(collection, i) = Admission<T>::get(collection, j);
        Admission<T>::get(collection, j) = tmp;
      }
    }
  }
}

#endif // SORT_HPP

