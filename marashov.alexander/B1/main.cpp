#include <iostream>
#include <cstring>

#include "sorting.hpp"

static const char * ASCENDING = "ascending";
static const char * DESCENDING = "descending";

void task1(CompareFunction<int> comparator);
void task2(const char * fileName);
void task3();
void task4(int size, CompareFunction<double> comparator);

int main(int argc, char * argv[])
{
  try
  {
    if (argc <= 1)
    {
      throw std::invalid_argument("The number of input parameters must be greater than 1");
    }

    int task;
    try
    {
      task = std::stoi(argv[1]);
    }
    catch(std::invalid_argument & e)
    {
      std::cerr << "Invalid task number" << std::endl;
      return 1;
    }

    switch (task)
    {
      case 1:
      {
        if (argc != 3)
        {
          throw std::invalid_argument("Input parameters must contain the sort type and no more");
        }

        if (!strcmp(argv[2], ASCENDING))
        {
          task1(ascendingComp<int>);
        }
        else if (!strcmp(argv[2], DESCENDING))
        {
          task1(descendingComp<int>);
        }
        else
        {
          throw std::invalid_argument("Unknown sort type");
        }

        break;
      }

      case 2:
      {
        if (argc != 3)
        {
          throw std::invalid_argument("Input parameters must contain the file name and no more");
        }
        const char * fileName = argv[2];
        task2(fileName);
        break;
      }

      case 3:
      {
        if (argc > 2)
        {
          throw std::invalid_argument("Too many parameters");
        }
        task3();
        break;
      }

      case 4:
      {
        if (argc != 4)
        {
          throw std::invalid_argument("There must be 4 arguments");
        }

        int size;
        
        try
        {
          size = std::stoi(argv[3]);
        }
        catch(std::invalid_argument & e)
        {
          std::cerr << "Invalid array size" << std::endl;
          return 1;
        }

        if (!strcmp(argv[2], ASCENDING))
        {
          task4(size, ascendingComp<double>);
        }
        else if (!strcmp(argv[2], DESCENDING))
        {
          task4(size, descendingComp<double>);
        }
        else
        {
          throw std::invalid_argument("Unknown sort type");
        }
        break;
      }
      
      default:
      {
        throw std::invalid_argument("Unknown command");
      }

    }
  }
  catch (std::invalid_argument & e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (std::runtime_error & e)
  {
    std::cerr << e.what() << std::endl;
    return 2;
  }
  return 0;
}

