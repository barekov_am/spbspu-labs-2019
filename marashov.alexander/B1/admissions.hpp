#ifndef VECTORS_HPP
#define VECTORS_HPP

#include <cstddef>

template <class T>
struct BracketsAdmission
{
  using Index = std::size_t;
  using ValueType = typename T::value_type;

  static ValueType & get(T & collection, const Index & index)
  {
    return collection[index];
  }

  static Index begin(const T &)
  {
    return 0;
  }

  static Index end(const T & collection)
  {
    return collection.size();
  }
};

template <class T>
struct AtAdmission
{
  using Index = std::size_t;
  using ValueType = typename T::value_type;

  static ValueType & get(T & collection, const Index & index)
  {
    return collection.at(index);
  }

  static Index begin(const T &)
  {
    return 0;
  }

  static Index end(const T & collection)
  {
    return collection.size();
  }
};

template <class T>
struct IteratorAdmission
{
  using Index = typename T::iterator;
  using ValueType = typename T::value_type;

  static ValueType & get(T &, const Index & index)
  {
    return *index;
  }

  static Index begin(T & collection)
  {
    return collection.begin();
  }

  static Index end(T & collection)
  {
    return collection.end();
  }
};

#endif // VECTORS_HPP

