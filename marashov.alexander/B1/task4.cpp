#include <iostream>
#include <cstring>
#include <string>
#include <exception>
#include <vector>

#include "printing.hpp"
#include "sorting.hpp"
#include "admissions.hpp"

void fillRandom(double * array, int size)
{
  for (int i = 0; i < size; ++i)
  {
    array[i] = (rand() / (double) RAND_MAX) * 2.0 - 1;
  }
}

void task4(int size, CompareFunction<double> comparator)
{
  std::vector<double> randomArray(size);
  fillRandom(&randomArray[0], size);

  printCollection(randomArray);
  sort<BracketsAdmission>(randomArray, comparator);
  printCollection(randomArray);
}

