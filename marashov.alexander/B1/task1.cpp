#include <iostream>
#include <vector>
#include <forward_list>

#include "sorting.hpp"
#include "printing.hpp"
#include "admissions.hpp"

void task1(CompareFunction<int> comparator)
{

  int number;
  std::vector<int> numbers(0);
  while (std::cin >> number)
  {
    numbers.push_back(number);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Error reading data");
  }

  std::vector<int> numbersCopy1 = numbers;
  std::forward_list<int> numbersCopy2(numbers.begin(), numbers.end());

  sort<BracketsAdmission>(numbers, comparator);
  sort<AtAdmission>(numbersCopy1, comparator);
  sort<IteratorAdmission>(numbersCopy2, comparator);

  printCollection(numbers);
  printCollection(numbersCopy1);
  printCollection(numbersCopy2);
}

