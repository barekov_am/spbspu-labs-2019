#ifndef PRINTING_HPP
#define PRINTING_HPP

#include <iostream>

template <class T>
void printCollection(const T & collection)
{
  typename T::const_iterator iterI = collection.begin();
  typename T::const_iterator iterEnd = collection.end();

  for ( ; iterI != iterEnd; ++iterI)
  {
    std::cout << *iterI << " ";
  }
  std::cout << std::endl;
}

#endif // PRINTING_HPP

