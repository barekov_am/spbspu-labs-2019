#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

struct DataStruct
{
  int key1{};
  int key2{};
  std::string str;
};

std::ostream & operator <<(std::ostream & out, const DataStruct & dataStruct)
{
  out << dataStruct.key1 << "," << dataStruct.key2 << "," << dataStruct.str;
  return out;
}

std::istream & operator >>(std::istream & in, DataStruct & dataStruct)
{
  int * pointer = &dataStruct.key1;
  for (int i = 0; i <= 1; ++i) {
    std::string tmp;
    while (true) {
      if (in.eof()) {
        in.setstate(std::ios::failbit);
        return in;
      }
      const char next = in.get();
      if (next == '\n') {
        in.setstate(std::ios::failbit);
        return in;
      } else if (next == ',') {
        break;
      } else {
        tmp.push_back(next);
      }
    }
    try {
      *pointer = std::stoi(tmp);
      if (*pointer > 5 || *pointer < -5) {
        throw std::invalid_argument("");
      }
    }
    catch (std::invalid_argument &) {
      in.setstate(std::ios::failbit);
      return in;
    }
    pointer = &dataStruct.key2;
  }
  getline(in, dataStruct.str);
  return in;
}

bool comparator(const DataStruct & a, const DataStruct & b)
{
  return a.key1 < b.key1 || (a.key1 == b.key1 && ((a.key2 < b.key2) || (a.key2 == b.key2 && a.str.length() < b.str.length())));
}

int main(int, char *[])
{
  std::vector<DataStruct> vector;
  DataStruct dataStruct;
  while (std::cin >> dataStruct)
  {
    vector.push_back(dataStruct);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    std::cerr << "Invalid data" << std::endl;
    return 2;
  }

  std::sort(vector.begin(), vector.end(), comparator);
  std::copy(vector.begin(), vector.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));
  return 0;
}
