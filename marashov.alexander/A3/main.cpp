#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <utility>

#include "../common/circle.hpp"
#include "../common/rectangle.hpp"
#include "../common/composite-shape.hpp"

int main()
{
  try
  {
    marashov::ShapePointer circ1(new marashov::Circle(1, 2, 3));
    marashov::ShapePointer circ2(new marashov::Circle(6, 5, 4));
    marashov::ShapePointer circ3(new marashov::Circle(7, 8, 9));
    marashov::ShapePointer rect1(new marashov::Rectangle(0, 0, 5, 5));
    marashov::ShapePointer rect2(new marashov::Rectangle(0, 0, 10, 10));

    marashov::CompositeShape compositeShape1 = marashov::CompositeShape();

    compositeShape1.writeInfo();

    compositeShape1.add(circ1);
    compositeShape1.add(circ2);
    compositeShape1.add(circ3);
    compositeShape1.add(rect1);
    compositeShape1.writeInfo();

    compositeShape1.add(rect2);
    compositeShape1.writeInfo();

    compositeShape1.scale(2);
    compositeShape1.writeInfo();

    compositeShape1.move(10, 10);

    marashov::CompositeShape compositeShape2(std::move(compositeShape1));
    compositeShape2.writeInfo();

    compositeShape2.remove(4);
    compositeShape2.writeInfo();
  }
  catch (const std::invalid_argument & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 1;
  }
  catch (const std::out_of_range & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 2;
  }
  catch (const std::logic_error & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 2;
  }
  return 0;
}
