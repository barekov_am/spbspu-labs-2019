#include <iostream>
#include <cstring>

#include "queueWithPriority.hpp"

void task1()
{
  QueueWithPriority<std::string> queue;

  std::string command;
  while (std::getline(std::cin, command))
  {
    const int spaceIndex = command.find(' ');
    if (spaceIndex == -1)
    {
      if (!strcmp(&command[0], "get"))
      {
        try
        {
          std::cout << *queue.getElementFromQueue().get() << std::endl;
        }
        catch (std::logic_error & exception)
        {
          std::cout << exception.what() << std::endl;
        }
      } else if (!strcmp(&command[0], "accelerate"))
      {
        queue.accelerate();
      } else
      {
        std::cout << "<INVALID COMMAND>" << std::endl;
        continue;
      }
    } else if (!strcmp(&command.substr(0, spaceIndex)[0], "add"))
    {
      const int secondSpaceIndex = command.find(' ', spaceIndex + 1);
      if (secondSpaceIndex == -1)
      {
        std::cout << "<INVALID COMMAND>" << std::endl;
        continue;
      }

      const char * priorityLiteral = &(command.substr(spaceIndex + 1, secondSpaceIndex - spaceIndex - 1)[0]);

      const bool isLowPriority = (!strcmp(priorityLiteral, "low"));
      const bool isNormalPriority = (!strcmp(priorityLiteral, "normal"));
      const bool isHighPriority = (!strcmp(priorityLiteral, "high"));

      const bool isCorrectPriority = isLowPriority || isNormalPriority || isHighPriority;
      if (!isCorrectPriority)
      {
        std::cout << "<INVALID COMMAND>" << std::endl;
        continue;
      }

      const ElementPriority priority = (isLowPriority)
          ? ElementPriority::LOW
          : (isNormalPriority)
              ? ElementPriority::NORMAL
              : ElementPriority::HIGH;
      queue.putElementToQueue(createQueueElement<std::string>(command.substr(secondSpaceIndex + 1)), priority);
    } else
    {
        std::cout << "<INVALID COMMAND>" << std::endl;
        continue;
    }
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Error reading commands");
  }

}

