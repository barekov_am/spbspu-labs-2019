#include <iostream>
#include <list>
#include <iterator>

void task2()
{
  int number;
  std::list<int> numbers;
  while (std::cin >> number)
  {
    if ((number > 20) || (number <= 0))
    {
      throw std::invalid_argument("Out of range input number");
    }
    numbers.push_back(number);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Error reading data");
  }

  if (numbers.empty())
  {
    return;
  }

  if (numbers.size() > 20)
  {
    throw std::invalid_argument("Too many numbers");
  }

  std::list<int>::const_iterator iter1 = numbers.begin();
  std::list<int>::const_iterator iter2 = numbers.end();
  --iter2;

  if (numbers.size() & 1)
  {
    while (iter1 != iter2)
    {
      std::cout << *iter1 << " " << *iter2 << " ";
      ++iter1;
      --iter2;
    }
  }
  else
  {
    std::cout << *iter1 << " ";
    ++iter1;
    while (iter1 != iter2)
    {
      std::cout << *iter2 << " " << *iter1 << " ";
      ++iter1;
      --iter2;
    }
  }
  std::cout << *iter1 << std::endl;
}

