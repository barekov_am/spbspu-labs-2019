#include <iostream>

#include "queueWithPriority.hpp"

void task1();
void task2();

int main(int argc, char * argv[]) 
{
  try
  {

    if (argc != 2)
    {
      throw std::invalid_argument("The number of input parameters must be equals 1");
    }

    int task;
    try
    {
      task = std::stoi(argv[1]);
    }
    catch(std::invalid_argument & e)
    {
      std::cerr << "Invalid task number" << std::endl;
      return 1;
    }

    switch (task)
    {
      case 1:
      {
        task1();
        break;
      }

      case 2:
      {
        task2();
        break;
      }

      default:
        throw std::invalid_argument("Unknown task number");
    }

  }
  catch(std::invalid_argument & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 1;
  }
  catch(std::runtime_error & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 1;
  }
  return 0;
}

