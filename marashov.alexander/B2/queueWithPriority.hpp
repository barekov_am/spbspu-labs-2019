#ifndef QUEUE_WITH_PRIORITY_HPP
#define QUEUE_WITH_PRIORITY_HPP

#include <memory>
#include <utility>
#include <list>
#include <functional>

template <class T>
std::shared_ptr<T> createQueueElement(T value)
{
  return std::make_shared<T>(value);
};

typedef enum
{
  LOW,
  NORMAL,
  HIGH
} ElementPriority;

template <class T>
class QueueWithPriority
{
public:
  using value_type = std::shared_ptr<T>;
  using reference = value_type &;
  using const_reference = const reference;

  QueueWithPriority():
    elementList()
  {
    lowIterator = elementList.end();
    normIterator = elementList.end();
    highIterator = elementList.end();
  }

  QueueWithPriority(const QueueWithPriority & queue) = default;
  QueueWithPriority(QueueWithPriority && queue) noexcept = default;
  ~QueueWithPriority() = default;
  QueueWithPriority & operator =(const QueueWithPriority<T> & queue) = default;
  QueueWithPriority & operator =(QueueWithPriority<T> && queue) noexcept = default;

  void putElementToQueue(value_type element, ElementPriority priority)
  {
    elementList.push_back(pair_type(element, priority));
    switch (priority)
    {
      case ElementPriority::LOW:
        if (lowIterator == elementList.end())
        {
          lowIterator--;
        }
        break;
      case ElementPriority::NORMAL:
        if (normIterator == elementList.end())
        {
          normIterator--;
        }
        break;
      case ElementPriority::HIGH:
        if (highIterator == elementList.end())
        {
          highIterator--;
        }
        break;
    }
  }

  value_type getElementFromQueue()
  {
    if (elementList.empty())
    {
      throw std::logic_error("<EMPTY>");
    }

    const typename container_type::iterator end = elementList.end();

    iterator_type iterator =
        (highIterator != end)
            ? highIterator
            : (normIterator != end)
                ? normIterator
                : lowIterator;

    const value_type result = (*iterator).first;
    const iterator_type popIter = iterator;

    ++iterator;
    while (iterator != end && (*iterator).second != (*popIter).second)
    {
      ++iterator;
    }

    switch ((*popIter).second)
    {
      case ElementPriority::LOW:
        lowIterator = iterator;
        break;
      case ElementPriority::NORMAL:
        normIterator = iterator;
        break;
      case ElementPriority::HIGH:
        highIterator = iterator;
        break;
    }

    elementList.erase(popIter);

    return result;
  }

  void accelerate()
  {
    const iterator_type end = elementList.end();
    while (lowIterator != end)
    {
      if ((*lowIterator).second == ElementPriority::LOW)
      {
        const iterator_type oldLowIterator = lowIterator++;
        putElementToQueue((*oldLowIterator).first, ElementPriority::HIGH);
        elementList.erase(oldLowIterator);
      } else
      {
        ++lowIterator;
      }
    }
  }

  bool empty() const
  {
    return elementList.empty();
  }

  size_t size() const
  {
    return elementList.size();
  }

private:
  using pair_type = std::pair<value_type, ElementPriority>;
  using container_type = std::list<pair_type>;
  using iterator_type = typename container_type::iterator;

  container_type elementList;

  iterator_type lowIterator;
  iterator_type normIterator;
  iterator_type highIterator;
};

#endif // QUEUE_WITH_PRIORITY_HPP

