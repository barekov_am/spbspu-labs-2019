#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

#define TOLERANCE 0.001

BOOST_AUTO_TEST_SUITE(circleTest)

// проверка перемещения круга
BOOST_AUTO_TEST_CASE(paramsConstancyAfterMoving)
{
  const double radius = 10;
  const marashov::point_t circleCenter = { 0, 0 };
  marashov::Circle circle(circleCenter, radius);

  const double circleArea = circle.getArea();

  // перемещение центра круга в другую точку
  const marashov::point_t newCircleCenter = { 100, 100 };
  circle.move(newCircleCenter);

  // проверка неизменности радиуса и площади круга
  // (допускается погрешность TOLERANCE)
  BOOST_CHECK_CLOSE(circle.getRadius(), radius, TOLERANCE);
  BOOST_CHECK_CLOSE(circle.getArea(), circleArea, TOLERANCE);
}

// проверка масштабирования круга
BOOST_AUTO_TEST_CASE(quadraticSquareIncreaseAfterScaling)
{
  const double radius = 5;
  const marashov::point_t circleCenter = { 0, 0 };
  marashov::Circle circle(circleCenter, radius);

  const double circleArea = circle.getArea();

  // масштабирование круга
  const double amount = 2;
  circle.scale(amount);

  // проверка квадратичного изменения площади
  // (допускается погрешность TOLERANCE)
  BOOST_CHECK_CLOSE(circle.getArea(), circleArea * amount * amount, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(constructorExceptions)
{
  // проверка на наличие исключения "некорректный аргумент" при попытке задать
  // отрицательный или нулевой радиус круга
  const double incorrectArg1 = -2;
  const double incorrectArg2 = 0;
  BOOST_CHECK_THROW(marashov::Circle({ 0, 0 }, incorrectArg1), std::invalid_argument);
  BOOST_CHECK_THROW(marashov::Circle({ 0, 0 }, incorrectArg2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(scaleExceptions)
{
  const marashov::point_t circleCenter = { 0, 0 };
  marashov::Circle circle = marashov::Circle(circleCenter, 10);
  // проверка на наличие исключения "некорректный аргумент" при попытке задать
  // отрицательное или нулевое значение масштабирования
  const double incorrectArg1 = -2;
  const double incorrectArg2 = 0;
  BOOST_CHECK_THROW(circle.scale(incorrectArg1), std::invalid_argument);
  BOOST_CHECK_THROW(circle.scale(incorrectArg2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
