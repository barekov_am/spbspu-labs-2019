#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "matrix.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

#define TOLERANCE 0.001

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(constructorsAndOperators)
{
  marashov::Matrix matrix;

  marashov::ShapePointer rectangle1(std::make_shared<marashov::Rectangle>(2, 2, 5, 5));
  marashov::ShapePointer rectangle2(std::make_shared<marashov::Rectangle>(13, 10, 0.1, 0.1));
  marashov::ShapePointer circle1(std::make_shared<marashov::Circle>(10, 10, 1));
  marashov::ShapePointer circle2(std::make_shared<marashov::Circle>(10, 10, 3));

  matrix.add(rectangle1, 0);
  matrix.add(rectangle2, 0);
  matrix.add(circle1, 1);
  matrix.add(circle2, 1);

  const marashov::Matrix constCopiedMatrix1(matrix);
  BOOST_CHECK_EQUAL(matrix == constCopiedMatrix1, true);

  const marashov::Matrix constCopiedMatrix2 = matrix;
  BOOST_CHECK_EQUAL(matrix == constCopiedMatrix2, true);

  marashov::Matrix movedMatrix1(std::move(matrix));
  BOOST_CHECK_EQUAL(constCopiedMatrix1 == constCopiedMatrix2, true);

  const marashov::Matrix movedMatrix2 = std::move(movedMatrix1);
  BOOST_CHECK_EQUAL(constCopiedMatrix1 == movedMatrix2, true);
}

BOOST_AUTO_TEST_CASE(indexOperator)
{
  marashov::Matrix matrix;
  marashov::ShapePointer circle(std::make_shared<marashov::Circle>(0, 0, 5));
  marashov::ShapePointer rectangle(std::make_shared<marashov::Rectangle>(0, 0, 5, 10));
  matrix.add(circle, 0);

  BOOST_CHECK_EQUAL(circle == matrix[0][0], true);

  matrix[0][0] = rectangle;
  BOOST_CHECK_EQUAL(rectangle == matrix[0][0], true);
  
  BOOST_CHECK_THROW(matrix[1], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
