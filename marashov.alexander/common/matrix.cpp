#include "matrix.hpp"

#include <stdexcept>

marashov::Matrix::Matrix():
    rows_(0),
    size_(0)
{

}

marashov::Matrix::Matrix(const marashov::Matrix & matrix):
  shapeArray_(std::make_unique<ShapePointer[]>(matrix.size_)),
  columns_(std::make_unique<unsigned int[]>(matrix.rows_)),
  rows_(matrix.rows_),
  size_(matrix.size_)
{
  for (unsigned int i = 0; i < size_; i++)
  {
      shapeArray_[i] = matrix.shapeArray_[i];
  }
  for (unsigned int i = 0; i < rows_; i++)
  {
    columns_[i] = matrix.columns_[i];
  }
}

marashov::Matrix::Matrix(marashov::Matrix && matrix) noexcept:
  shapeArray_(std::move(matrix.shapeArray_)),
  columns_(std::move(matrix.columns_)),
  rows_(matrix.rows_),
  size_(matrix.size_)
{

}

marashov::Matrix & marashov::Matrix::operator =(const marashov::Matrix & matrix)
{
  if (matrix != *this)
  {
    ShapeArray temporaryArray(std::make_unique<ShapePointer[]>(matrix.size_));

    for (unsigned int i = 0; i < matrix.size_; i++)
    {
      temporaryArray[i] = matrix.shapeArray_[i];
    }

    UIntArray temporaryColArray(std::make_unique<unsigned int[]>(matrix.rows_));

    for (unsigned int i = 0; i < matrix.rows_; i++)
    {
      temporaryColArray[i] = matrix.columns_[i];
    }

    shapeArray_.swap(temporaryArray);
    columns_.swap(temporaryColArray);
    rows_ = matrix.rows_;
    size_ = matrix.size_;
  }
  return *this;
}

marashov::Matrix & marashov::Matrix::operator =(marashov::Matrix && matrix) noexcept
{
  if (matrix != *this)
  {
    shapeArray_ = std::move(matrix.shapeArray_);
    columns_ = std::move(matrix.columns_);
    rows_ = matrix.rows_;
    size_ = matrix.size_;
  }
  return *this;
}

marashov::ShapePointer * marashov::Matrix::operator [](unsigned int row) const
{
  if (row >= rows_)
  {
    throw std::out_of_range("Out of range row index");
  }
  unsigned int index = 0;
  for (unsigned int r = 0; r < row; r++)
  {
    index += columns_[r];
  }
  return &shapeArray_[index];
}

bool marashov::Matrix::operator ==(const marashov::Matrix & matrix) const
{
  if (rows_ != matrix.rows_)
  {
    return false;
  }
  for (unsigned int i = 0; i < rows_; i++)
  {
    if (columns_[i] == matrix.columns_[i])
    {
      for (unsigned int j = 0; j < columns_[i]; j++)
      {
        if (shapeArray_[j] != matrix.shapeArray_[j])
        {
          return false;
        }
      }
    }
    else
    {
      return false;
    }
  }
  return true;
}

bool marashov::Matrix::operator !=(const marashov::Matrix & matrix) const
{
  return !(*this == matrix);
}

void marashov::Matrix::add(marashov::ShapePointer & shapePointer, unsigned int row)
{
  if (shapePointer == nullptr)
  {
    throw std::invalid_argument("Shape must be not null pointer");
  }

  UIntArray temporaryColArray = (row >= rows_)
      ? std::make_unique<unsigned int[]>(row + 1)
      : std::make_unique<unsigned int[]>(rows_);

  for (unsigned int i = 0; i < rows_; i++)
  {
    temporaryColArray[i] = columns_[i];
  }

  for (unsigned int i = rows_; i <= row; i++)
  {
    temporaryColArray[i] = 0;
  }

  ShapeArray temporaryShapeArray = std::make_unique<ShapePointer []>(size_ + 1);
  unsigned int index = 0;
  for (unsigned int r = 0; r <= row; r++)
  {
    for (unsigned int i = 0; i < temporaryColArray[r]; i++)
    {
      temporaryShapeArray[index] = shapeArray_[index];
      index++;
    }
  }
  temporaryColArray[row]++;
  temporaryShapeArray[index] = shapePointer;
  for (unsigned int i = index; i < size_; i++)
  {
    temporaryShapeArray[i + 1] = shapeArray_[i];
  }

  rows_ = std::max(rows_, row + 1);
  size_++;
  shapeArray_.swap(temporaryShapeArray);
  columns_.swap(temporaryColArray);
}

unsigned int marashov::Matrix::columns(unsigned int row) const
{
  if (row >= rows_)
  {
    throw std::out_of_range("Out of range row index");
  }
  return columns_[row];
}

unsigned int marashov::Matrix::rows() const
{
  return rows_;
}
