#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include "pointer-types.hpp"

namespace marashov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape & shapes);
    CompositeShape(CompositeShape && shapes) noexcept;
    ~CompositeShape() override = default;

    CompositeShape & operator =(const CompositeShape & shapes);
    CompositeShape & operator =(CompositeShape && shapes) noexcept;

    ShapePointer & operator [](unsigned int index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dltX, double dltY) override;
    void move(const point_t & pos) override;
    void scale(double scaleAmount) override;
    void writeInfo() const override;
    void rotate(double degrees) override;

    void add(ShapePointer shape);
    void remove(unsigned int index);

    double getAngle() const;
    unsigned int size() const;

  private:
    ShapeArray shapeArray_;
    double angle_;
    unsigned int shapeCount_;
    unsigned int arraySize_;
  };
}

#endif //COMPOSITE_SHAPE_HPP
