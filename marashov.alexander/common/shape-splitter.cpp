#include "shape-splitter.hpp"

#include <stdexcept>

marashov::Matrix marashov::split(const marashov::CompositeShape & compositeShape)
{
  const unsigned int size = compositeShape.size();
  if (size == 0)
  {
    throw std::invalid_argument("Composite shape must be not empty");
  }

  // записать все ограничивающие прямоугольники в массив,
  // чтобы не пересчитывать их каждый раз
  RectangleArray frameRects = std::make_unique<rectangle_t[]>(size);
  for (unsigned i = 0; i < size; i++)
  {
    frameRects[i] = compositeShape[i]->getFrameRect();
  }

  Matrix matrix;
  matrix.add(compositeShape[0], 0);

  // массив для хранения номера слоя для каждой фигуры
  UIntArray shapeLayers = std::make_unique<unsigned int[]>(size);
  shapeLayers[0] = 0; // для самой первой фигуры результат очевиден

  // вычисление слоя каждой фигуры
  for (unsigned int i = 1; i < size; i++)
  {
    // массив для записи занятых и свободных слоёв (на размер больше в случае создания нового слоя)
    BoolArray overlapsArray = std::make_unique<bool[]>(matrix.rows() + 1);
    for (unsigned int k = 0; k <= matrix.rows(); k++)
    {
      overlapsArray[k] = false;
    }

    // проверка на возможность занять тот или иной слой для фигуры
    for (unsigned int j = 0; j < i; j++)
    {
      overlapsArray[shapeLayers[j]] |= frameRects[i].isOverlaps(frameRects[j]);
    }

    // находим индекс свободного слоя (новый, то есть последний, всегда свободен)
    shapeLayers[i] = 0;
    while (overlapsArray[shapeLayers[i]])
    {
      shapeLayers[i]++;
    }

    matrix.add(compositeShape[i], shapeLayers[i]);
  }

  return matrix;
}
