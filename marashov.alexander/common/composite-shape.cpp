#include "composite-shape.hpp"

#include <stdexcept>
#include <iostream>
#include <cmath>

marashov::CompositeShape::CompositeShape():
  angle_(0),
  shapeCount_(0),
  arraySize_(0)
{

}

marashov::CompositeShape::CompositeShape(const CompositeShape & shapes):
  shapeArray_(std::make_unique<ShapePointer[]>(shapes.arraySize_)),
  angle_(shapes.angle_),
  shapeCount_(shapes.shapeCount_),
  arraySize_(shapes.arraySize_)
{
  for (unsigned int i = 0; i < arraySize_; i++)
  {
    shapeArray_[i] = shapes.shapeArray_[i];
  }
}

marashov::CompositeShape::CompositeShape(CompositeShape && shapes) noexcept:
  shapeArray_(std::move(shapes.shapeArray_)),
  angle_(shapes.angle_),
  shapeCount_(shapes.shapeCount_),
  arraySize_(shapes.arraySize_)
{

}

marashov::CompositeShape & marashov::CompositeShape::operator =(const CompositeShape & shapes)
{
  if (&shapes != this)
  {
    ShapeArray temporaryArray = std::make_unique<ShapePointer[]>(shapes.arraySize_);

    for (unsigned int i = 0; i < shapes.arraySize_; i++)
    {
      temporaryArray[i] = shapes.shapeArray_[i];
    }

    shapeArray_.swap(temporaryArray);
    angle_ = shapes.angle_;
    arraySize_ = shapes.arraySize_;
    shapeCount_ = shapes.shapeCount_;
  }
  return *this;
}

marashov::CompositeShape & marashov::CompositeShape::operator =(CompositeShape && shapes) noexcept
{
  if (&shapes != this)
  {
    shapeArray_ = std::move(shapes.shapeArray_);
    angle_ = shapes.angle_;
    shapeCount_ = shapes.shapeCount_;
    arraySize_ = shapes.arraySize_;
  }
  return *this;
}

marashov::ShapePointer & marashov::CompositeShape::operator [](unsigned int index) const
{
  if (index >= shapeCount_)
  {
    throw std::out_of_range("Out of range index");
  }
  return shapeArray_[index];
}

double marashov::CompositeShape::getArea() const
{
  double area = 0;
  for (unsigned int i = 0; i < shapeCount_; i++)
  {
    area += shapeArray_[i]->getArea();
  }
  return area;
}

marashov::rectangle_t marashov::CompositeShape::getFrameRect() const
{
  if (shapeCount_ == 0)
  {
    throw std::logic_error("Can not get frame rectangle: CompositeShape is empty");
  }

  const rectangle_t startRect = shapeArray_[0]->getFrameRect();
  point_t topRightPoint_ = { startRect.pos.x + (startRect.width / 2), startRect.pos.y + (startRect.height / 2) };
  point_t bottomLeftPoint_ = { startRect.pos.x - (startRect.width / 2), startRect.pos.y - (startRect.height / 2) };

  for (unsigned int i = 0; i < shapeCount_; i++)
  {
    rectangle_t rect = shapeArray_[i]->getFrameRect();

    topRightPoint_.x = std::max(topRightPoint_.x, rect.pos.x + (rect.width / 2));
    topRightPoint_.y = std::max(topRightPoint_.y, rect.pos.y + (rect.height / 2));
    bottomLeftPoint_.x = std::min(bottomLeftPoint_.x, rect.pos.x - (rect.width / 2));
    bottomLeftPoint_.y = std::min(bottomLeftPoint_.y, rect.pos.y - (rect.height / 2));
  }

  return { topRightPoint_.x - bottomLeftPoint_.x, topRightPoint_.y - bottomLeftPoint_.y,
      { (topRightPoint_.x + bottomLeftPoint_.x) / 2, (topRightPoint_.y + bottomLeftPoint_.y) / 2 } };
}

void marashov::CompositeShape::move(double dltX, double dltY)
{
  if (shapeCount_ == 0)
  {
    throw std::logic_error("Can not move empty CompositeShape");
  }

  for (unsigned int i = 0; i < shapeCount_; i++)
  {
    shapeArray_[i]->move(dltX, dltY);
  }
}

void marashov::CompositeShape::move(const marashov::point_t & pos)
{
  const rectangle_t frameRect = getFrameRect();
  marashov::CompositeShape::move(frameRect.pos.x - pos.x, frameRect.pos.y - pos.y);
}

void marashov::CompositeShape::scale(double scaleAmount)
{
  if (scaleAmount <= 0)
  {
    throw std::invalid_argument("Scale amount must be positive");
  }

  if (shapeCount_ == 0)
  {
    throw std::logic_error("Can not scale empty CompositeShape");
  }

  const point_t center = getFrameRect().pos;
  for (unsigned int i = 0; i < shapeCount_; i++)
  {
    const point_t shapeCenter = shapeArray_[i]->getFrameRect().pos;
    shapeArray_[i]->move((shapeCenter.x - center.x) * (scaleAmount - 1), (shapeCenter.y - center.y) * (scaleAmount - 1));
    shapeArray_[i]->scale(scaleAmount);
  }
}

void marashov::CompositeShape::writeInfo() const
{
  if (shapeCount_ == 0)
  {
    std::cout << "Empty composite shape" << std::endl << std::endl;
    return;
  }

  const rectangle_t frameRect = getFrameRect();

  std::cout << "Composite shape. Center at ("
      << frameRect.pos.x << "; " << frameRect.pos.y << ")"
      << std::endl << "It includes " << shapeCount_ << " figures"
      << std::endl << "Area = " << getArea()
      << std::endl << "frame rectangle width = " << frameRect.width
      << std::endl << "frame rectangle height = " << frameRect.height
      << std::endl << "angle = " << angle_
      << std::endl << std::endl;
}

void marashov::CompositeShape::add(ShapePointer shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape must be not null pointer");
  }
  if (shapeCount_ < arraySize_)
  {
    shapeArray_[shapeCount_++] = shape;
  }
  else
  {
    ShapeArray temporaryArray = std::make_unique<ShapePointer[]>(arraySize_ + 1);
    for (unsigned int i = 0; i < arraySize_; i++)
    {
      temporaryArray[i] = shapeArray_[i];
    }
    temporaryArray[shapeCount_] = shape;
    arraySize_++;
    shapeCount_++;
    shapeArray_.swap(temporaryArray);
  }
}

void marashov::CompositeShape::remove(unsigned int index)
{
  if (index >= shapeCount_)
  {
    throw std::out_of_range("Out of range index");
  }

  for (unsigned int i = index; i < shapeCount_ - 1; i++)
  {
    shapeArray_[i] = shapeArray_[i + 1];
  }
  shapeArray_[--shapeCount_] = nullptr;
}

unsigned int marashov::CompositeShape::size() const
{
  return shapeCount_;
}

void marashov::CompositeShape::rotate(double degrees)
{
  if (shapeCount_ == 0)
  {
    throw std::logic_error("Can not rotate empty CompositeShape");
  }
  const point_t center = getFrameRect().pos;
  for (unsigned int i = 0; i < shapeCount_; i++)
  {
    shapeArray_[i]->rotate(degrees);

    const point_t shapeCenter = shapeArray_[i]->getFrameRect().pos;

    const double distanceX = shapeCenter.x - center.x;
    const double distanceY = shapeCenter.y - center.y;
    const double distance = sqrt(distanceX * distanceX + distanceY * distanceY);

    if (distance < 0.000001)
    {
      continue;
    }

    const double oldAngle = (std::abs(distanceX) > 0.000001)
        ? (atan(distanceY / distanceX) + ((distanceX < 0) ? M_PI : 0))
        : (distanceY > 0) ? (M_PI / 2) : (-M_PI / 2);

    const double newSin = sin(oldAngle + degrees * M_PI / 180);
    const double newCos = cos(oldAngle + degrees * M_PI / 180);

    shapeArray_[i]->move({ center.x + distance * newCos, center.y + distance * newSin });
  }
  angle_ = fmod(angle_ + degrees, 360);
}

double marashov::CompositeShape::getAngle() const
{
  return angle_;
}
