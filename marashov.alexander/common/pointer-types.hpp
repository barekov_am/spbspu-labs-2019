#ifndef POINTER_TYPES_HPP
#define POINTER_TYPES_HPP

#include <memory>

#include "shape.hpp"

namespace marashov
{
  using ShapePointer = std::shared_ptr<Shape>;
  using ShapeArray = std::unique_ptr<ShapePointer[]>;
  using RectangleArray = std::unique_ptr<rectangle_t[]>;
  using UIntArray = std::unique_ptr<unsigned int[]>;
  using BoolArray = std::unique_ptr<bool[]>;
}

#endif //POINTER_TYPES_HPP
