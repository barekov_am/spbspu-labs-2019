#ifndef SHAPE_SPLITTER_HPP
#define SHAPE_SPLITTER_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace marashov
{
  Matrix split(const CompositeShape & compositeShape);
}

#endif //SHAPE_SPLITTER_HPP
