#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

#define TOLERANCE 0.001

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(paramsConstancyAfterMoving)
{
  const double width = 100;
  const double height = 50;
  const marashov::point_t rectangleCenter = { 0, 0 };
  marashov::Rectangle rectangle(rectangleCenter, width, height);
  const double rectArea = rectangle.getArea();

  const marashov::point_t newRectCenter = { 10, 10 };
  rectangle.move(newRectCenter);

  BOOST_CHECK_CLOSE(rectangle.getWidth(), width, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getHeight(), height, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getArea(), rectArea, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(quadraticSquareIncreaseAfterScaling)
{
  const double width = 100;
  const double height = 50;
  const marashov::point_t rectangleCenter = { 0, 0 };
  marashov::Rectangle rectangle(rectangleCenter, width, height);
  const double rectArea = rectangle.getArea();

  const double amount = 2;
  rectangle.scale(amount);

  BOOST_CHECK_CLOSE(rectangle.getArea(), rectArea * amount * amount, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(constructorExceptions)
{
  const double incorrectArg1 = -2;
  const double incorrectArg2 = 0;
  const double correctArg = 10;
  BOOST_CHECK_THROW(marashov::Rectangle({ 0, 0 }, incorrectArg1, correctArg), std::invalid_argument);
  BOOST_CHECK_THROW(marashov::Rectangle({ 0, 0 }, incorrectArg2, correctArg), std::invalid_argument);
  BOOST_CHECK_THROW(marashov::Rectangle({ 0, 0 }, correctArg, incorrectArg1), std::invalid_argument);
  BOOST_CHECK_THROW(marashov::Rectangle({ 0, 0 }, correctArg, incorrectArg2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(scaleExceptions)
{
  marashov::Rectangle rectangle = marashov::Rectangle({ 0, 0 }, 10, 5);
  const double incorrectArg1 = -2;
  const double incorrectArg2 = 0;
  BOOST_CHECK_THROW(rectangle.scale(incorrectArg1), std::invalid_argument);
  BOOST_CHECK_THROW(rectangle.scale(incorrectArg2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(centerConstancyAfterRotating)
{
  const marashov::point_t oldCenter = { 10, 5 };
  marashov::Rectangle rectangle = marashov::Rectangle(oldCenter, 10, 5);
  rectangle.rotate(45);
  const marashov::point_t newCenter = rectangle.getCenter();
  BOOST_CHECK_CLOSE(newCenter.x, oldCenter.x, TOLERANCE);
  BOOST_CHECK_CLOSE(newCenter.y, oldCenter.y, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(areaConstancyAfterRotating)
{
  marashov::Rectangle rectangle = marashov::Rectangle({ 10, 5 }, 10, 5);
  const double oldArea = rectangle.getArea();
  rectangle.rotate(45);
  BOOST_CHECK_CLOSE(oldArea, rectangle.getArea(), TOLERANCE);
}

BOOST_AUTO_TEST_SUITE_END()
