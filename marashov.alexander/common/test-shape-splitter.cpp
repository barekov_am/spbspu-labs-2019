#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <iostream>

#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "shape-splitter.hpp"

#define TOLERANCE 0.001

BOOST_AUTO_TEST_SUITE(shapeSplitterTest)

BOOST_AUTO_TEST_CASE(correctSlpit)
{
  marashov::CompositeShape compositeShape;

  marashov::ShapePointer rectangle1(std::make_shared<marashov::Rectangle>(0, 0, 5, 5));
  marashov::ShapePointer rectangle2(std::make_shared<marashov::Rectangle>(2, 2, 5, 5));
  marashov::ShapePointer rectangle3(std::make_shared<marashov::Rectangle>(13, 10, 0.1, 0.1));

  marashov::ShapePointer circle1(std::make_shared<marashov::Circle>(10, 10, 1));
  marashov::ShapePointer circle2(std::make_shared<marashov::Circle>(10, 10, 3));


  compositeShape.add(rectangle1);
  compositeShape.add(rectangle2);
  compositeShape.add(circle1);
  compositeShape.add(circle2);
  compositeShape.add(rectangle3);

  marashov::Matrix matrix = marashov::split(compositeShape);

  BOOST_CHECK_EQUAL(matrix[0][0] == rectangle1, true);
  BOOST_CHECK_EQUAL(matrix[0][1] == circle1, true);
  BOOST_CHECK_EQUAL(matrix[0][2] == rectangle3, true);
  BOOST_CHECK_EQUAL(matrix[1][0] == rectangle2, true);
  BOOST_CHECK_EQUAL(matrix[1][1] == circle2, true);
}

BOOST_AUTO_TEST_SUITE_END()
