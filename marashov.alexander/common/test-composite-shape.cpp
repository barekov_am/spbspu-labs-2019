#include <stdexcept>
#include <cmath>

#include <boost/test/auto_unit_test.hpp>

#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

#define TOLERANCE 0.001

BOOST_AUTO_TEST_SUITE(compositeShapeTest)

// несколько констант вынесены отдельно в силу незначимости самих значений
// в тесте важно лишь то, что эти параметры корректны с точки зрения задания фигур
constexpr marashov::point_t correctPosition = { 10, 10 };
const double correctWidth = 10;
const double correctHeight = 20;
const double correctRadius = 5;

BOOST_AUTO_TEST_CASE(copyConstructorTest)
{
  marashov::ShapePointer rectangle(std::make_shared<marashov::Rectangle>(marashov::Rectangle(correctPosition, correctWidth, correctHeight)));
  marashov::ShapePointer circle(std::make_shared<marashov::Circle>(marashov::Circle(correctPosition, correctRadius)));

  marashov::CompositeShape compositeShape1 = marashov::CompositeShape();
  compositeShape1.add(rectangle);
  compositeShape1.add(circle);

  const marashov::CompositeShape compositeShape2(compositeShape1);

  const marashov::rectangle_t frameRect1 = compositeShape1.getFrameRect();
  const marashov::rectangle_t frameRect2 = compositeShape2.getFrameRect();

  BOOST_CHECK_CLOSE(frameRect1.pos.x, frameRect2.pos.x, TOLERANCE);
  BOOST_CHECK_CLOSE(frameRect1.pos.y, frameRect2.pos.y, TOLERANCE);
  BOOST_CHECK_CLOSE(frameRect1.width, frameRect2.width, TOLERANCE);
  BOOST_CHECK_CLOSE(frameRect1.height, frameRect2.height, TOLERANCE);
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), compositeShape2.getArea(), TOLERANCE);
  BOOST_CHECK_EQUAL(compositeShape1.size(), compositeShape2.size());
}

BOOST_AUTO_TEST_CASE(paramsConstancyAfterMoving)
{
  marashov::ShapePointer rectangle(std::make_shared<marashov::Rectangle>
      (marashov::Rectangle(correctPosition, correctWidth, correctHeight)));
  marashov::ShapePointer circle(std::make_shared<marashov::Circle>(marashov::Circle(correctPosition, correctRadius)));
  marashov::CompositeShape compositeShape = marashov::CompositeShape();

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  const double oldArea = compositeShape.getArea();
  const marashov::rectangle_t oldFrameRect = compositeShape.getFrameRect();

  compositeShape.move({ 500, 500 });
  compositeShape.move(123, 456);

  const double newArea = compositeShape.getArea();
  const marashov::rectangle_t newFrameRect = compositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(oldFrameRect.width, newFrameRect.width, TOLERANCE);
  BOOST_CHECK_CLOSE(oldFrameRect.height, newFrameRect.height, TOLERANCE);
  BOOST_CHECK_CLOSE(oldArea, newArea, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(centerConstancyAfterScaling)
{
  marashov::ShapePointer rectangle(std::make_shared<marashov::Rectangle>
      (marashov::Rectangle(correctPosition, correctWidth, correctHeight)));
  marashov::ShapePointer circle(std::make_shared<marashov::Circle>(marashov::Circle(correctPosition, correctRadius)));
  marashov::CompositeShape compositeShape = marashov::CompositeShape();

  compositeShape.add(circle);
  compositeShape.add(rectangle);
  const marashov::point_t oldCenter = compositeShape.getFrameRect().pos;

  const double scaleAmount1 = 4;
  compositeShape.scale(scaleAmount1);
  const marashov::point_t center2 = compositeShape.getFrameRect().pos;
  BOOST_CHECK_CLOSE(oldCenter.x, center2.x, TOLERANCE);
  BOOST_CHECK_CLOSE(oldCenter.y, center2.y, TOLERANCE);

  const double scaleAmount2 = 0.5;
  compositeShape.scale(scaleAmount2);
  const marashov::point_t center3 = compositeShape.getFrameRect().pos;
  BOOST_CHECK_CLOSE(oldCenter.x, center3.x, TOLERANCE);
  BOOST_CHECK_CLOSE(oldCenter.y, center3.y, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(quadraticSquareChangesAfterScaling)
{
  marashov::ShapePointer rectangle(std::make_shared<marashov::Rectangle>
      (marashov::Rectangle(correctPosition, correctWidth, correctHeight)));
  marashov::ShapePointer circle(std::make_shared<marashov::Circle>(marashov::Circle(correctPosition, correctRadius)));
  marashov::CompositeShape compositeShape = marashov::CompositeShape();

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  const double oldArea1 = compositeShape.getArea();
  const double amount1 = 2;
  compositeShape.scale(amount1);
  const double newArea1 = compositeShape.getArea();
  BOOST_CHECK_CLOSE(oldArea1 * amount1 * amount1, newArea1, TOLERANCE);

  const double oldArea2 = compositeShape.getArea();
  const double amount2 = 0.5;
  compositeShape.scale(amount2);
  const double newArea2 = compositeShape.getArea();
  BOOST_CHECK_CLOSE(oldArea2 * amount2 * amount2, newArea2, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(scaleExceptions)
{
  marashov::ShapePointer rectangle(std::make_shared<marashov::Rectangle>
      (marashov::Rectangle(correctPosition, correctWidth, correctHeight)));
  marashov::ShapePointer circle(std::make_shared<marashov::Circle>(marashov::Circle(correctPosition, correctRadius)));
  marashov::CompositeShape compositeShape = marashov::CompositeShape();

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  const double incorrectArg1 = -2;
  const double incorrectArg2 = 0;
  BOOST_CHECK_THROW(compositeShape.scale(incorrectArg1), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.scale(incorrectArg2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(addShapeTest)
{
  marashov::ShapePointer rectangle(std::make_shared<marashov::Rectangle>(correctPosition, correctWidth, correctHeight));
  marashov::ShapePointer circle(std::make_shared<marashov::Circle>(correctPosition, correctRadius));
  marashov::CompositeShape compositeShape = marashov::CompositeShape();

  compositeShape.add(circle);

  BOOST_CHECK_CLOSE(compositeShape[0]->getArea(), circle->getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(compositeShape[0]->getFrameRect().width, circle->getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(compositeShape[0]->getFrameRect().height, circle->getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE(compositeShape[0]->getFrameRect().pos.x, circle->getFrameRect().pos.x, TOLERANCE);
  BOOST_CHECK_CLOSE(compositeShape[0]->getFrameRect().pos.y, circle->getFrameRect().pos.y, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(removeShapeTest)
{
  marashov::ShapePointer rectangle(std::make_shared<marashov::Rectangle>(correctPosition, correctWidth, correctHeight));
  marashov::ShapePointer circle(std::make_shared<marashov::Circle>(correctPosition, correctRadius));
  marashov::CompositeShape compositeShape = marashov::CompositeShape();

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  const unsigned int incorrectIndex = 2;
  BOOST_CHECK_THROW(compositeShape.remove(incorrectIndex), std::out_of_range);

  const unsigned int shapeCount1 = compositeShape.size();
  compositeShape.remove(1);
  const unsigned int shapeCount2 = compositeShape.size();
  BOOST_CHECK_EQUAL(shapeCount1, shapeCount2 + 1);

  const unsigned int deletedIndex = 1;
  BOOST_CHECK_THROW(compositeShape.remove(deletedIndex), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(shapeCountChanges)
{
  marashov::ShapePointer circle(std::make_shared<marashov::Circle>(correctPosition, correctRadius));
  marashov::CompositeShape compositeShape = marashov::CompositeShape();
  const unsigned int shapeCount1 = compositeShape.size();

  compositeShape.add(circle);
  const unsigned int shapeCount2 = compositeShape.size();
  BOOST_CHECK_EQUAL(shapeCount1, shapeCount2 - 1);

  compositeShape.remove(0);
  const unsigned int shapeCount3 = compositeShape.size();
  BOOST_CHECK_EQUAL(shapeCount2 - 1, shapeCount3);
}

BOOST_AUTO_TEST_CASE(rotateTest)
{

  marashov::ShapePointer rectangle1(std::make_shared<marashov::Rectangle>(30.5, 8, 1, 7));
  marashov::ShapePointer rectangle2(std::make_shared<marashov::Rectangle>(12.5, 8.5, 10, 4));

  marashov::ShapePointer circle1(std::make_shared<marashov::Circle>(20, 10, 5));
  marashov::ShapePointer circle2(std::make_shared<marashov::Circle>(30, 15, 7));

  marashov::CompositeShape compositeShape;

  compositeShape.add(rectangle1);
  compositeShape.add(rectangle2);
  compositeShape.add(circle1);
  compositeShape.add(circle2);

  const double oldArea = compositeShape.getArea();

  for (int angle = -720; angle <= 720; angle += 45)
  {
    compositeShape.rotate(angle);
    BOOST_CHECK_CLOSE(oldArea, compositeShape.getArea(), TOLERANCE);
  }
}

BOOST_AUTO_TEST_SUITE_END()
