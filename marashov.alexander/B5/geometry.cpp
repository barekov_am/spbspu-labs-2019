#include <algorithm>

#include "geometry.hpp"

Point Point::operator -() const
{
  return {-x, -y};
}

Point Point::operator +(const Point & rhs) const
{
  return {x + rhs.x, y + rhs.y};
}

Point Point::operator -(const Point & rhs) const
{
  return *this + (-rhs);
}

double scalarProduct(const Vector2d & vector1, const Vector2d & vector2)
{
  return vector1.x * vector2.x + vector1.y * vector2.y;
}

bool isRectangle(const Shape & shape)
{
  if (shape.size() != 4)
  {
    return false;
  }
  const Vector2d AB = shape[1] - shape[0];
  const Vector2d CB = shape[1] - shape[2];
  const Vector2d CD = shape[3] - shape[2];
  const Vector2d AD = shape[3] - shape[0];
  const bool isFirstAngle90 = scalarProduct(AB, AD) == 0;
  const bool isSecondAngle90 = scalarProduct(CB, CD) == 0;
  const bool isSides1Equally = scalarProduct(AB, AB) == scalarProduct(CD, CD);
  const bool isSides2Equally = scalarProduct(AD, AD) == scalarProduct(CB, CB);
  return isFirstAngle90 && isSecondAngle90 && isSides1Equally && isSides2Equally;
}

bool isSquare(const Shape & shape)
{
  if (!isRectangle(shape))
  {
    return false;
  }

  const Vector2d AD = shape[3] - shape[0];
  const Vector2d CD = shape[3] - shape[2];

  return scalarProduct(AD, AD) == scalarProduct(CD, CD);
}

std::ostream & operator <<(std::ostream & out, const Point & point)
{
  out << '(' << point.x << "; " << point.y << ')';
  return out;
}

std::ostream & operator <<(std::ostream & out, const Shape & shape)
{
  out << shape.size() << " ";
  std::copy(shape.begin(), shape.end(), std::ostream_iterator<Point>(out, " "));
  return out;
}

std::istream & operator >>(std::istream & in, Shape & shape)
{
  shape.clear();

  int n;
  in >> n;
  if (in.eof() || in.fail())
  {
    in.setstate(std::ios::failbit);
    return in;
  }

  std::string str;
  std::getline(in, str, '\n');
  str.erase(std::remove_if(str.begin(), str.end(), [](const char c) { return isspace(c); }), str.end());

  int pos = 0;
  for (int i = 0; i < n; ++i)
  {
    Point point{};
    const int delimiterIndex = str.find(';', pos);
    const int closeBracket = str.find(')', delimiterIndex);
    if (str[pos] != '(' || delimiterIndex == -1 || closeBracket == -1)
    {
      in.setstate(std::ios::failbit);
      return in;
    }
    try
    {
      point.x = std::stoi(str.substr(pos + 1, delimiterIndex));
      point.y = std::stoi(str.substr(delimiterIndex + 1, closeBracket - 1));
    }
    catch (std::invalid_argument &)
    {
      in.setstate(std::ios::failbit);
      return in;
    }

    pos = closeBracket + 1;
    shape.push_back(point);
  }

  if (str[pos] == '(')
  {
    in.setstate(std::ios::failbit);
  }

  return in;
}
