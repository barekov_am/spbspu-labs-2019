#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP

#include <vector>
#include <iostream>
#include <iterator>

struct Point
{
  int x;
  int y;

  Point operator -() const;
  Point operator +(const Point & rhs) const;
  Point operator -(const Point & rhs) const;
};

using Vector2d = Point;
using Shape = std::vector<Point>;

double scalarProduct(const Vector2d & vector1, const Vector2d & vector2);
bool isRectangle(const Shape & shape);
bool isSquare(const Shape & shape);

std::ostream & operator <<(std::ostream & out, const Point & point);
std::ostream & operator <<(std::ostream & out, const Shape & shape);

std::istream & operator >>(std::istream & in, Shape & shape);

#endif //GEOMETRY_HPP
