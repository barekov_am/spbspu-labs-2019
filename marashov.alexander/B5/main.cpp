#include <iostream>

void task1();
void task2();

int main(int argc, char * argv[])
{
  try
  {
    if (argc != 2)
    {
      throw std::invalid_argument("Only task number expected");
    }

    int taskNumber;
    try
    {
      taskNumber = std::stoi(argv[1]);
    }
    catch (std::invalid_argument &)
    {
      std::cerr << "Invalid task number" << std::endl;
      return 1;
    }

    switch (taskNumber)
    {
      case 1:
      {
        task1();
        break;
      }
      case 2:
      {
        task2();
        break;
      }
      default:
      {
        throw std::invalid_argument("Invalid task number");
      }
    }
  }
  catch (std::invalid_argument & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 1;
  }
  catch (std::out_of_range & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 2;
  }
  catch (std::logic_error & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 2;
  }
  catch (std::runtime_error & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 2;
  }
  return 0;
}
