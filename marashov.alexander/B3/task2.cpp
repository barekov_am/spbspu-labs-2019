#include <iostream>
#include <algorithm>
#include <iterator>

#include "container.hpp"

void task2()
{
  Container container;
  std::copy(container.begin(), container.end(), std::ostream_iterator<unsigned long long>(std::cout, " "));
  std::cout << std::endl;
  std::reverse_copy(container.begin(), container.end(), std::ostream_iterator<unsigned long long>(std::cout, " "));
  std::cout << std::endl;
}

