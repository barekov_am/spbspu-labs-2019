#include "argument.hpp"

#include <istream>

const char ENDL = '\n';
const char QUOTE = '\"';
const char SPACE = ' ';
const char SLASH = '\\';
const char COMMA = ',';

bool isLetter(const char ch)
{
  return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z');
}

bool isNumber(const char ch)
{
  return ch >= '0' && ch <= '9';
}

std::istream & operator >>(std::istream & in, ArgumentPointer & arg)
{
  arg->reset();

  while (true)
  {
    if (in.eof())
    {
      arg->isEndOfCommand = true;
      break;
    }

    char nextChar = in.get();
    const bool isEndl = (nextChar == ENDL);

    if (isEndl) {
      arg->isEndOfCommand = true;
      break;
    }

    const bool isCorrectChar = arg->pushIfCorrect(nextChar);

    if (!isCorrectChar)
    {
      arg->interrupted = nextChar != SPACE;
      break;
    }
  }
  return in;
}

bool Argument::isCorrect()
{
  return !value.empty() && !interrupted;
}

void Argument::reset() {
  isEndOfCommand = false;
  interrupted = false;
  value = "";
}

bool NumberArg::pushIfCorrect(const char ch)
{
  const bool result = isNumber(ch);
  if (result)
  {
    value.push_back(ch);
  }
  return result;
}

bool NameArg::pushIfCorrect(const char ch)
{
  if (prevChar_ != SLASH && ch == QUOTE)
  {
    prevChar_ = ch;
    ++count_;
    return count_ <= 2;
  }
  if (count_ == 0 || count_ == 2 || (prevChar_ == SLASH && ch != QUOTE))
  {
    return false;
  }
  const bool isCorrectChar = isLetter(ch) || (ch == SPACE) || (ch == COMMA) || (ch == QUOTE);
  const bool isSlash = (ch == SLASH);
  const bool result = isCorrectChar || isSlash;

  if (result && !isSlash)
  {
    value.push_back(ch);
  }
  prevChar_ = ch;
  return result;
}

void NameArg::reset()
{
  Argument::reset();
  prevChar_ = 'a';
  count_ = 0;
}

bool NameArg::isCorrect()
{
  return Argument::isCorrect() && (prevChar_ != SLASH) && (count_ == 2);
}

bool BookmarkNameArg::pushIfCorrect(const char ch)
{
  const bool result = isNumber(ch) || isLetter(ch) || (ch == '-');
  if (result)
  {
    value.push_back(ch);
  }
  return result;
}

bool OffsetArg::pushIfCorrect(const char ch)
{
  const bool result = isNumber(ch) || isLetter(ch) || (ch == '-') || (ch == '+');
  if (result)
  {
    value.push_back(ch);
  }
  return result;
}

bool RelativeReservedWordArg::pushIfCorrect(const char ch)
{
  const bool isAfter = (index_ <= after_.length() && prevCh_ == after_[index_ - 1] && ch == after_[index_]);
  const bool isBefore = (index_ <= after_.length() && prevCh_ == before_[index_ - 1] && ch == before_[index_]);
  const bool result = isAfter || isBefore;
  if (result)
  {
    value.push_back(ch);
    prevCh_ = ch;
    ++index_; 
  }
  return isAfter || isBefore;
}

void RelativeReservedWordArg::reset()
{
  Argument::reset();
  prevCh_ = '-';
  index_ = 1;
}

bool RelativeReservedWordArg::isCorrect()
{
  return Argument::isCorrect() && ((value[0] == after_[1] && index_ == after_.length()) || (value[0] == before_[1] && index_ == before_.length()));
}
