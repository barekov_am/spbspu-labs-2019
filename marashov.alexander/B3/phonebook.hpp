#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <list>
#include <iterator>
#include <map>
#include <memory>
#include <utility>

#include "pointer-types.hpp"

class Contact
{
public:
  Contact(std::string name, std::string phoneNumber):
    name(std::move(name)),
    phoneNumber(std::move(phoneNumber))
  {
    
  }
  std::string name;
  std::string phoneNumber;
};

class Phonebook
{
public:
  using value_type = ContactPointer;
  using container_type = typename std::list<value_type>;
  using const_iterator_type = typename container_type::const_iterator;

  Phonebook();

  ContactPointer getCurrent() const;

  void move(int n);
  void moveLast();
  void moveFirst();

  inline void next()
  {
    move(1);
  }

  inline void prev()
  {
    move(-1);
  }

  void insertAfter(const value_type & contact);
  void insertBefore(const value_type & contact);
  void insertBack(const value_type & contact);
  void deleteCurrent();

  void switchBookmark(const std::string & name);
  void makeBookmark(const std::string & name);

private:
  using iterator_type = typename container_type::iterator;
  using map_type = typename std::map<std::string, iterator_type>;
  using map_iterator_type = typename map_type::iterator;

  container_type contacts_;
//  iterator_type current_;
  map_type bookmarks_;
  std::string currentBookmark_;
};

#endif // PHONEBOOK_HPP
