#include <iostream>
#include <istream>

#include "command.hpp"
#include "argument.hpp"

std::istream & operator >>(std::istream & in, CommandPointer & command)
{
  command->isFullyRead = false;
  command->isCorrect = true;

  for (auto & argument : command->arguments)
  {
    in >> argument;
    command->isFullyRead = argument->isEndOfCommand;
    command->isCorrect = argument->isCorrect();
    if (!command->isCorrect || (command->isFullyRead && argument != command->arguments.back()))
    {
      break;
    }
  }
  return in;
}

CommandAdd::CommandAdd()
{
  arguments.push_back(std::make_shared<NumberArg>(NumberArg()));
  arguments.push_back(std::make_shared<NameArg>(NameArg()));
}
void CommandAdd::run(Phonebook & phonebook)
{
  const std::string number = arguments.front()->value;
  const std::string name = arguments.back()->value;
  phonebook.insertBack(std::make_shared<Contact>(number, name));
}

CommandStore::CommandStore()
{
  arguments.push_back(std::make_shared<BookmarkNameArg>(BookmarkNameArg()));
  arguments.push_back(std::make_shared<BookmarkNameArg>(BookmarkNameArg()));
}

void CommandStore::run(Phonebook & phonebook)
{
  const std::string name = arguments.front()->value;
  const std::string newName = arguments.back()->value;
  phonebook.switchBookmark(name);
  phonebook.makeBookmark(newName);
}

CommandInsert::CommandInsert()
{
  arguments.push_back(std::make_shared<RelativeReservedWordArg>(RelativeReservedWordArg()));
  arguments.push_back(std::make_shared<BookmarkNameArg>(BookmarkNameArg()));
  arguments.push_back(std::make_shared<NumberArg>(NumberArg()));
  arguments.push_back(std::make_shared<NameArg>(NameArg()));
}

void CommandInsert::run(Phonebook & phonebook)
{
  auto it = arguments.begin();
  const std::string position = (*it)->value;
  ++it;
  const std::string markName = (*it)->value;
  ++it;
  const std::string number = (*it)->value;
  ++it;
  const std::string name = (*it)->value;

  phonebook.switchBookmark(markName);
  if (position == "after")
  {
    phonebook.insertAfter(std::make_shared<Contact>(number, name));
  }
  else if (position == "before")
  {
    phonebook.insertBefore(std::make_shared<Contact>(number, name));
  }
}

CommandDelete::CommandDelete()
{
  arguments.push_back(std::make_shared<BookmarkNameArg>(BookmarkNameArg()));
}
void CommandDelete::run(Phonebook & phonebook)
{
  const std::string markName = arguments.front()->value;
  phonebook.switchBookmark(markName);
  phonebook.deleteCurrent();
}

CommandShow::CommandShow()
{
  arguments.push_back(std::make_shared<BookmarkNameArg>(BookmarkNameArg()));
}
void CommandShow::run(Phonebook & phonebook)
{
  const std::string markName = arguments.front()->value;
  phonebook.switchBookmark(markName);
  const ContactPointer contact = phonebook.getCurrent();
  std::cout << contact->name << " " << contact->phoneNumber << std::endl;
}

CommandMove::CommandMove()
{
  arguments.push_back(std::make_shared<BookmarkNameArg>(BookmarkNameArg()));
  arguments.push_back(std::make_shared<OffsetArg>(OffsetArg()));
}
void CommandMove::run(Phonebook & phonebook)
{
  const std::string markName = arguments.front()->value;
  const std::string offset = arguments.back()->value;

  phonebook.switchBookmark(markName);
  if (offset == "first")
  {
    phonebook.moveFirst();
  }
  else if (offset == "last")
  {
    phonebook.moveLast();
  }
  else
  {
    try
    {
      phonebook.move(std::stoi(offset));
    }
    catch (std::invalid_argument &)
    {
      std::cout << "<INVALID STEP>" << std::endl;
    }
  }
}

