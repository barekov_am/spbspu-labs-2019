#ifndef COMMAND_HPP
#define COMMAND_HPP

#include <list>
#include <cstring>
#include <istream>

#include "argument.hpp"
#include "phonebook.hpp"
#include "pointer-types.hpp"

struct Command
{
  Command() = default;
  Command(const Command &) = default;
  Command(Command &&) = default;
  virtual ~Command() = default;
  virtual void run(Phonebook &) = 0;
  std::list<ArgumentPointer> arguments;
  bool isFullyRead = false;
  bool isCorrect = false;
};

std::istream & operator >>(std::istream & in, CommandPointer & command);

struct CommandAdd: public Command
{
  CommandAdd();
  ~CommandAdd() override = default;
  void run(Phonebook & phonebook) override;
};

struct CommandStore: public Command
{
  CommandStore();
  ~CommandStore() override = default;
  void run(Phonebook & phonebook) override;
};

struct CommandInsert: public Command
{
  CommandInsert();
  ~CommandInsert() override = default;
  void run(Phonebook & phonebook) override;
};

struct CommandDelete: public Command
{
  CommandDelete();
  ~CommandDelete() override = default;
  void run(Phonebook & phonebook) override;
};

struct CommandShow: public Command
{
  CommandShow();
  ~CommandShow() override = default;
  void run(Phonebook & phonebook) override;
};

struct CommandMove: public Command
{
  CommandMove();
  ~CommandMove() override = default;
  void run(Phonebook & phonebook) override;
};

#endif // COMMAND_HPP

