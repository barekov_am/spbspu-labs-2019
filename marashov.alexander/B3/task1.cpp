#include <iostream>
#include <map>

#include "command.hpp"
#include "pointer-types.hpp"

void task1()
{
  std::map<std::string, CommandPointer> commandsMap;
  commandsMap["add"] = std::make_shared<CommandAdd>(CommandAdd());
  commandsMap["store"] = std::make_shared<CommandStore>(CommandStore());
  commandsMap["insert"] = std::make_shared<CommandInsert>(CommandInsert());
  commandsMap["delete"] = std::make_shared<CommandDelete>(CommandDelete());
  commandsMap["show"] = std::make_shared<CommandShow>(CommandShow());
  commandsMap["move"] = std::make_shared<CommandMove>(CommandMove());

  Phonebook phonebook;

  std::string commandStr;
  while (std::cin >> commandStr)
  {
    if (commandsMap.find(commandStr) != commandsMap.end())
    {
      std::cin.get();
      CommandPointer command = commandsMap[commandStr];
      std::cin >> command;

      if (!command->isCorrect || !command->isFullyRead)
      {
        std::cout << "<INVALID COMMAND>" << std::endl;
        if (!command->isFullyRead) {
          std::string tmp;
          std::getline(std::cin, tmp);
        }
        continue;
      }
      try
      {
        command->run(phonebook);
      }
      catch (std::invalid_argument & exception)
      {
        std::cout << exception.what() << std::endl;
      }
      catch (std::logic_error & exception)
      {
        std::cout << exception.what() << std::endl;
      }
    }
    else
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
      std::string tmp;
      std::getline(std::cin, tmp);
      continue;
    }
  }
}

