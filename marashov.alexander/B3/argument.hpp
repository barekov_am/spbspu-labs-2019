#ifndef ARGUMENT_HPP
#define ARGUMENT_HPP

#include <iostream>
#include <memory>

#include "pointer-types.hpp"

struct Argument
{
  Argument() = default;
  Argument(const Argument &) = default;
  Argument(Argument &&) = default;
  virtual ~Argument() = default;

  virtual bool pushIfCorrect(char) = 0;
  virtual void reset();

  virtual bool isCorrect();

  std::string value;
  bool isEndOfCommand = false;
  bool interrupted = false;
};

std::istream & operator >>(std::istream & in, ArgumentPointer & arg);

struct NumberArg: public Argument 
{
  ~NumberArg() override = default;
  bool pushIfCorrect(char ch) override;
};
struct NameArg: public Argument 
{
  ~NameArg() override = default;
  bool pushIfCorrect(char ch) override;
  void reset() override;
  bool isCorrect() override;

private:
  char prevChar_;
  short count_;
};

struct BookmarkNameArg: public Argument 
{
  ~BookmarkNameArg() override = default;
  bool pushIfCorrect(char ch) override;
};

struct OffsetArg: public Argument
{
  ~OffsetArg() override = default;
  bool pushIfCorrect(char ch) override;
};

struct RelativeReservedWordArg: public Argument 
{
  ~RelativeReservedWordArg() override = default;
  bool pushIfCorrect(char ch) override;
  void reset() override;
  bool isCorrect() override;

private:
  const std::string after_ = "-after";
  const std::string before_ = "-before";
  char prevCh_;
  unsigned short index_;
};

#endif // ARGUMENT_HPP

