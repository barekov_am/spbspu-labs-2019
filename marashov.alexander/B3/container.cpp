#include "container.hpp"

const size_t FACTORIAL_INDEX_BORDER = 10;

ContainerIterator::ContainerIterator():
  value_(1),
  index_(1)
{

}

ContainerIterator::ContainerIterator(const size_t index):
  value_(factorial(index)),
  index_(index)
{

}

unsigned long long & ContainerIterator::operator *()
{
  return value_;
}
unsigned long long * ContainerIterator::operator ->()
{
  return &value_;
}

ContainerIterator & ContainerIterator::operator ++()
{
  ++index_;
  value_ *= index_;
  return *this;
}
ContainerIterator & ContainerIterator::operator --()
{
  if (index_ > 1)
  {
    value_ /= index_;
    --index_;
  }
  return *this;
}
ContainerIterator ContainerIterator::operator ++(int)
{
  ContainerIterator tmp = *this;
  ++(*this);
  return tmp;
}
ContainerIterator ContainerIterator::operator --(int)
{
  ContainerIterator tmp = *this;
  --(*this);
  return tmp;
}

bool ContainerIterator::operator ==(const ContainerIterator & rhs) const
{
  return (value_ == rhs.value_) && (index_ == rhs.index_);
}
bool ContainerIterator::operator !=(const ContainerIterator & rhs) const
{
  return !(*this == rhs);
}

unsigned long long ContainerIterator::factorial(const unsigned long long n)
{
  return calculateFactorial(n, 1);
}

unsigned long long ContainerIterator::calculateFactorial(const unsigned long long n, const unsigned long long accumulator)
{
  return (n == 0) ? accumulator : calculateFactorial(n - 1, accumulator * n);
}

ContainerIterator Container::begin()
{
  return {1};
}
ContainerIterator Container::end()
{
  return {FACTORIAL_INDEX_BORDER + 1};
}

