#include "phonebook.hpp"

#include <utility>
#include <iostream>

Phonebook::Phonebook():
  contacts_(),
  bookmarks_()
{
  currentBookmark_ = "current";
  bookmarks_.insert(std::make_pair(currentBookmark_, contacts_.end()));
}

ContactPointer Phonebook::getCurrent() const
{
  if (contacts_.begin() == contacts_.end())
  {
    throw std::logic_error("<EMPTY>");
  }
  return *(bookmarks_.find(currentBookmark_)->second);
}

void Phonebook::move(const int n)
{
    std::advance((bookmarks_.find(currentBookmark_)->second), n);
}

void Phonebook::moveLast()
{
  auto & current = (bookmarks_.find(currentBookmark_)->second);
  current = contacts_.end();
  if (!contacts_.empty())
  {
    --current;
  }
}

void Phonebook::moveFirst()
{
  (bookmarks_.find(currentBookmark_)->second) = contacts_.begin();
}

void Phonebook::insertAfter(const value_type & contact)
{
  auto & current = (bookmarks_.find(currentBookmark_)->second);
  if (current == contacts_.end())
  {
    contacts_.insert(current, contact);
    current = contacts_.begin();
  }
  else
  {
    next();
    contacts_.insert(current, contact);
    move(-2);
  }
}

void Phonebook::insertBefore(const value_type & contact)
{
  auto & current = (bookmarks_.find(currentBookmark_)->second);
  current = contacts_.insert(current, contact);
}

void Phonebook::insertBack(const value_type & contact)
{
  contacts_.push_back(contact);
  if (contacts_.size() == 1)
  {
    bookmarks_["current"] = contacts_.begin();
  }
}

void Phonebook::deleteCurrent()
{
  auto & current = (bookmarks_.find(currentBookmark_)->second);
  if (current == contacts_.end())
  {
    throw std::logic_error("<EMPTY>");
  }
  auto tmp = current;
  for (auto & bookmark: bookmarks_)
  {
    if (bookmark.second == tmp)
    {
      ++bookmark.second;
    }
  }
  contacts_.erase(tmp);
  if (current == contacts_.end() && !contacts_.empty())
  {
    --current;
  }
}

void Phonebook::switchBookmark(const std::string & name)
{
  auto bookmark = bookmarks_.find(name);
  if (bookmark != bookmarks_.end())
  {
    currentBookmark_ = bookmark->first;
  }
  else
  {
    throw std::invalid_argument("<INVALID BOOKMARK>");
  }
}

void Phonebook::makeBookmark(const std::string & name)
{
  auto & current = (bookmarks_.find(currentBookmark_)->second);
  bookmarks_.insert(std::make_pair(name, current));
}
