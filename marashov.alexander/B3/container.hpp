#ifndef CONTAINER_HPP
#define CONTAINER_HPP

#include <iterator>

class ContainerIterator: public std::iterator<std::bidirectional_iterator_tag, unsigned long long>
{
public:
  ContainerIterator();
  ContainerIterator(const size_t index);

  unsigned long long & operator *();
  unsigned long long * operator ->();

  ContainerIterator & operator ++();
  ContainerIterator & operator --();
  ContainerIterator operator ++(int);
  ContainerIterator operator --(int);

  bool operator ==(const ContainerIterator & rhs) const;
  bool operator !=(const ContainerIterator & rhs) const;

private:
  unsigned long long value_;
  size_t index_;

  unsigned long long factorial(const unsigned long long n);
  unsigned long long calculateFactorial(const unsigned long long n, const unsigned long long accumulator);
};

class Container
{
public:
  Container() = default;
  Container(const Container &) = delete;
  Container(Container &&) = delete;
  ~Container() = default;

  Container & operator =(const Container &) = delete;
  Container & operator =(Container &&) = delete;

  ContainerIterator begin();
  ContainerIterator end();
};

#endif // CONTAINER_HPP

