#ifndef POINTER_TYPES_HPP
#define POINTER_TYPES_HPP

#include <memory>

struct Contact;
struct Argument;
struct Command;

using ContactPointer = typename std::shared_ptr<Contact>;
using ArgumentPointer = typename std::shared_ptr<Argument>;
using CommandPointer = typename std::shared_ptr<Command>;

#endif //POINTER_TYPES_HPP

