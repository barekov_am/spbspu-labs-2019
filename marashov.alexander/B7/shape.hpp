#ifndef B7_SHAPE_HPP
#define B7_SHAPE_HPP


#include <memory>

struct Point
{
  long long x;
  long long y;
};

class Shape
{
public:
  using ShapePointer = std::shared_ptr<Shape>;

  explicit Shape(Point center);
  virtual ~Shape() = default;

  bool isMoreLeft(const Shape & shape) const;
  bool isUpper(const Shape & shape) const;

  Point getCenter() const;

  virtual void draw(std::ostream & out) const = 0;
  virtual std::string getType() const = 0;

  friend std::ostream & operator <<(std::ostream & out, const Point & point);

protected:
  Point center_;
};

std::ostream & operator <<(std::ostream & out, const Shape::ShapePointer & shapePointer);

#endif //B7_SHAPE_HPP
