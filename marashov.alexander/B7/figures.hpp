#ifndef B7_FIGURES_HPP
#define B7_FIGURES_HPP


#include <istream>
#include "shape.hpp"

class Circle: public Shape
{
public:
  explicit Circle(Point center);
  ~Circle() override = default;
  void draw(std::ostream & out) const override;
  std::string getType() const override;
};

class Triangle: public Shape
{
public:
  explicit Triangle(Point center);
  ~Triangle() override = default;
  void draw(std::ostream & out) const override;
  std::string getType() const override;
};

class Square: public Shape
{
public:
  explicit Square(Point center);
  ~Square() override = default;
  void draw(std::ostream & out) const override;
  std::string getType() const override;
};

std::istream & operator >>(std::istream & in, Shape::ShapePointer & shapePointer);

#endif //B7_FIGURES_HPP
