#ifndef B7_PIFACTOR_HPP
#define B7_PIFACTOR_HPP

#include <cmath>

class PiFactor
{
public:

  double operator ()(double & number)
  {
    return number * M_PI;
  }

};


#endif //B7_PIFACTOR_HPP
