#include "figures.hpp"

#include <cstring>
#include <algorithm>
#include <iostream>


Circle::Circle(const Point center):
    Shape(center)
{

}

void Circle::draw(std::ostream & out) const
{
  out << this;
}

std::string Circle::getType() const
{
  return "CIRCLE";
}

Triangle::Triangle(const Point center):
    Shape(center)
{

}

void Triangle::draw(std::ostream & out) const
{
  out << this;
}

std::string Triangle::getType() const
{
  return "TRIANGLE";
}

Square::Square(const Point center):
    Shape(center)
{

}

void Square::draw(std::ostream & out) const
{
  out << this;
}

std::string Square::getType() const
{
  return "SQUARE";
}

std::istream & operator >>(std::istream & in, Shape::ShapePointer & shapePointer)
{
  in >> std::ws;
  std::string str;
  std::getline(in, str, '\n');
  str.erase(std::remove_if(str.begin(), str.end(), [](const char c) { return isspace(c); }), str.end());

  const int typeDelimiterIndex = str.find('(');
  if (typeDelimiterIndex == -1) {
    in.setstate(std::ios::failbit);
    return in;
  }

  std::string typeStr = str.substr(0, typeDelimiterIndex);
  std::string centerStr = str.substr(typeDelimiterIndex + 1);

  Point point{};
  const int delimiterIndex = centerStr.find(';', 0);
  const int closeBracket = centerStr.find(')', delimiterIndex);
  if (delimiterIndex == -1 || closeBracket == -1 || closeBracket < static_cast<int>(centerStr.size()) - 1)
  {
    in.setstate(std::ios::failbit);
    return in;
  }
  try
  {
    point.x = std::stoi(centerStr.substr(0, delimiterIndex));
    point.y = std::stoi(centerStr.substr(delimiterIndex + 1, closeBracket - 1));
  }
  catch (std::invalid_argument &)
  {
    in.setstate(std::ios::failbit);
    return in;
  }

  if ("CIRCLE" == typeStr)
  {
    shapePointer = std::make_shared<Circle>(point);
  }
  else if ("TRIANGLE" == typeStr)
  {
    shapePointer = std::make_shared<Triangle>(point);
  }
  else if ("SQUARE" == typeStr)
  {
    shapePointer = std::make_shared<Square>(point);
  }
  else
  {
    in.setstate(std::ios::failbit);
  }

  return in;
}
