#include <list>
#include <iterator>
#include <iostream>
#include <algorithm>

#include "pifactor.hpp"

void task1()
{
  std::list<double> list;
  std::copy(std::istream_iterator<double>(std::cin), std::istream_iterator<double>(), std::back_inserter(list));
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Error reading data");
  }

  PiFactor piFactor;
  std::transform(list.begin(), list.end(), list.begin(), piFactor);
  std::copy(list.begin(), list.end(), std::ostream_iterator<double>(std::cout, " "));
}
