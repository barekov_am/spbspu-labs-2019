#include <list>
#include <iostream>
#include <iterator>

#include "figures.hpp"

void task2()
{
  std::list<Shape::ShapePointer> list;
  std::copy(std::istream_iterator<Shape::ShapePointer>(std::cin), std::istream_iterator<Shape::ShapePointer>(), std::back_inserter(list));

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Error reading data");
  }

  std::cout << "Original:" << std::endl;
  std::copy(list.begin(), list.end(), std::ostream_iterator<Shape::ShapePointer>(std::cout));

  auto leftRightComp = [](const Shape::ShapePointer & lhs, const Shape::ShapePointer & rhs)
  {
    return lhs->isMoreLeft(*rhs);
  };
  list.sort(leftRightComp);
  std::cout << "Left-Right:" << std::endl;
  std::copy(list.begin(), list.end(), std::ostream_iterator<Shape::ShapePointer>(std::cout));

  auto rightLeftComp = [&](const Shape::ShapePointer & lhs, const Shape::ShapePointer & rhs)
  {
    return leftRightComp(rhs, lhs);
  };
  list.sort(rightLeftComp);
  std::cout << "Right-Left:" << std::endl;
  std::copy(list.begin(), list.end(), std::ostream_iterator<Shape::ShapePointer>(std::cout));

  auto topBottomComp = [](const Shape::ShapePointer & lhs, const Shape::ShapePointer & rhs) {
    return lhs->isUpper(*rhs);
  };
  list.sort(topBottomComp);
  std::cout << "Top-Bottom:" << std::endl;
  std::copy(list.begin(), list.end(), std::ostream_iterator<Shape::ShapePointer>(std::cout));

  auto bottomTopComp = [&](const Shape::ShapePointer & lhs, const Shape::ShapePointer & rhs)
  {
    return topBottomComp(rhs, lhs);
  };
  list.sort(bottomTopComp);
  std::cout << "Bottom-Top:" << std::endl;
  std::copy(list.begin(), list.end(), std::ostream_iterator<Shape::ShapePointer>(std::cout));
}
