#include "shape.hpp"

#include <iomanip>

Shape::Shape(const Point center):
  center_(center)
{

}

bool Shape::isMoreLeft(const Shape & shape) const
{
  return center_.x < shape.center_.x;
}

bool Shape::isUpper(const Shape & shape) const
{
  return center_.y > shape.center_.y;
}

Point Shape::getCenter() const {
  return center_;
}

std::ostream & operator <<(std::ostream & out, const Point & point) {
  out << '(' << point.x << ";" << point.y << ')';
  return out;
}

std::ostream & operator <<(std::ostream & out, const Shape::ShapePointer & shapePointer)
{
  out << shapePointer->getType() << " " << shapePointer->getCenter() << std::endl;
  return out;
}
