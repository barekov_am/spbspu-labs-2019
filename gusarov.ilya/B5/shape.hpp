#ifndef SHAPE_H
#define SHAPE_H

#include <vector>

struct Point
{
    int x, y;
};

using Shape = std::vector<Point>;

#endif
