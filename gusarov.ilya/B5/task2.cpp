#include <string>
#include <iostream>
#include <boost/algorithm/string/trim.hpp>

#include "shape.hpp"

Shape read(std::string in, std::size_t num)
{
    Shape shapes;
    std::size_t openBracket;
    std::size_t delimeter;
    std::size_t closeBracket;

    for (std::size_t i = 0; i < num; ++i)
    {
        if (in.empty()) {
            throw std::invalid_argument("Invalid number of vertices!\n");
        }
        openBracket = in.find_first_of('(');
        delimeter = in.find_first_of(';');
        closeBracket = in.find_first_of(')');

        if ((openBracket == std::string::npos) || (delimeter == std::string::npos) || (closeBracket == std::string::npos)) {
            throw std::invalid_argument("Invalid point!\n");
        }

        Point point {
                std::stoi(in.substr(openBracket + 1, delimeter - openBracket - 1)),
                std::stoi(in.substr(delimeter + 1, closeBracket - delimeter - 1))
        };

        in.erase(0, closeBracket + 1);
        shapes.push_back(point);
    }
    if (!in.empty()) {
        throw std::invalid_argument("Too many points!\n");
    }

    return shapes;
}

bool isRectangle(const Shape & shape)
{
    int diagonal1 = (shape[0].x - shape[2].x)*(shape[0].x - shape[2].x) + (shape[0].y - shape[2].y)*(shape[0].y - shape[2].y);
    int diagonal2 = (shape[1].x - shape[3].x)*(shape[1].x - shape[3].x) + (shape[1].y - shape[3].y)*(shape[1].y - shape[3].y);

    return diagonal1 == diagonal2;
}

bool isSquare(const Shape & shape)
{
    if (isRectangle(shape)) {
        int a = (shape[0].x - shape[1].x)*(shape[0].x - shape[1].x) + (shape[0].y - shape[1].y)*(shape[0].y - shape[1].y);
        int b = (shape[1].x - shape[2].x)*(shape[1].x - shape[2].x) + (shape[1].y - shape[2].y)*(shape[1].y - shape[2].y);
        int c = (shape[2].x - shape[3].x)*(shape[2].x - shape[3].x) + (shape[2].y - shape[3].y)*(shape[2].y - shape[3].y);
        int d = (shape[3].x - shape[0].x)*(shape[3].x - shape[0].x) + (shape[3].y - shape[0].y)*(shape[3].y - shape[0].y);

        if ((a == b) && (b == c) && (c == d)) {
            return true;
        }
    }
    return false;
}

bool comparator(const Shape & lhs, const Shape & rhs)
{
    if (lhs.size() < rhs.size()) {
        return true;
    }
    if ((lhs.size() == 4) && (rhs.size() == 4)) {
        if (isSquare(lhs)) {
            if (isSquare(rhs)) {
                return lhs[0].x < rhs[0].x;
            }
            return true;
        }
    }
    return false;
}

void task2()
{
    std::vector<Shape> vec;

    std::string line;
    while (std::getline(std::cin, line))
    {

        boost::trim(line);

        if (line.empty()) {
            continue;
        }

        std::size_t num = std::stoi(line.substr(0,1));

        if (num < 3) {
            throw std::invalid_argument("Incorrect number of vertices!\n");
        }

        vec.push_back(read(line, num));
    }

    std::size_t allVertices = 0;

    std::size_t triangles = 0;
    std::size_t squares = 0;
    std::size_t rectangles = 0;


    for (auto & i : vec)
    {
        allVertices += i.size();

        if (i.size() == 3)
            triangles++;
        else if (i.size() == 4)
        {
            if (isRectangle(i))
                rectangles++;
            if (isSquare(i))
                ++squares;
        }
    }

    vec.erase(std::remove_if(vec.begin(), vec.end(),[](const Shape & shape) { return shape.size() == 5; }), vec.end());


    Shape points(vec.size());


    for (auto i : vec)
        points.push_back(i[0]);

    points.erase(points.begin(), points.begin() + vec.size());


    std::sort(vec.begin(), vec.end(), comparator);

    std::cout << "Vertices: " << allVertices << '\n';
    std::cout << "Triangles: " << triangles << '\n';
    std::cout << "Squares: " << squares << '\n';
    std::cout << "Rectangles: " << rectangles << '\n';

    std::cout << "Points: ";
    for (auto i : points) {
        std::cout << '(' << i.x << ';' << i.y << ") ";
    }
    std::cout << '\n';

    std::cout << "Shapes: \n";
    for (auto shape : vec) {
        std::cout << shape.size();
        for (auto point : shape) {
            std::cout << " (" << point.x << ';' << point.y << ") ";
        }
        std::cout << '\n';
    }


}
