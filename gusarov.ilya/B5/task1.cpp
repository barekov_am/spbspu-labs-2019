#include <string>
#include <sstream>
#include <iostream>
#include <list>
#include <boost/algorithm/string.hpp>

void task1()
{
    std::string in;
    std::list<std::string> list;
    while (getline(std::cin, in))
    {
        std::istringstream buffer(in);
        std::string word;
        while (buffer >> word)
        {
            list.push_back(word);
        }
    }

    list.sort();
    list.unique();

    std::ostream_iterator<std::string> out_it (std::cout, "\n");
    std::copy(list.begin(), list.end(), out_it);

}
