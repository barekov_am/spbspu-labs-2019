#include <iostream>
#include <vector>
#include <algorithm>

enum Token
{
    Word,
    Punct,
    Dash,
    Number,
};

struct Element
{
    Token token;
    std::string element;
};


std::vector<Element> words;

void task(std::size_t width)
{
    //reading

    while (std::cin) {

        char first = std::cin.get();
        if ((((first == '+') || (first == '-')) && (std::isdigit(std::cin.peek()))) || (std::isdigit(first)))
        {
            std::string strNum;
            strNum += first;
            size_t countDot = 0;
            while(std::cin)
            {
                if ((std::cin.peek() == '.') || (std::isdigit(std::cin.peek())))
                {
                    strNum += std::cin.get();
                    if (strNum.back() == '.')
                    {
                        countDot++;
                    }
                }
                else
                {
                    break;
                }
            }

            if (strNum.size() > 20)
            {
                throw std::invalid_argument("Invalid width of the number\n");
            }

            if (countDot > 1)
            {
                throw std::invalid_argument("Too many dots in the number\n");
            }
            Element element = {Token::Number, strNum};
            words.push_back(element);
        }
        else if (std::isalpha(first))
        {
            std::string strWords;
            strWords += first;
            while(std::cin)
            {
                if ((std::isalpha(std::cin.peek())) || (std::cin.peek() == '-'))
                {
                    strWords += std::cin.get();
                    if (strWords.back() == '-' && std::cin.peek() == '-')
                    {
                        throw std::invalid_argument("Too many dash\n");
                    }
                }
                else
                {
                    break;
                }
            }

            if (strWords.size() > 20)
            {
                throw std::invalid_argument("Incorrect width of word\n");
            }
            Element element = {Token::Word, strWords};
            words.push_back(element);
        }
        else if (std::ispunct(first))
        {
            if ((first == '-') && (std::cin.peek() == '-'))
            {
                std::string strDash;
                strDash += first;
                std::size_t count = 1;
                while (std::cin)
                {
                    if (std::cin.peek() == '-')
                    {
                        strDash += std::cin.get();
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }

                if (count > 3)
                {
                    throw std::invalid_argument("Too many dashes for hyphen\n");
                }

                Element element = {Token::Dash, strDash};
                words.push_back(element);
            }
            else
            {
                if (std::ispunct(std::cin.peek()))
                {
                    throw std::invalid_argument("Incorrect Punctuation\n");
                }
                std::string singleChar;
                singleChar += first;
                Element element = {Token::Punct, singleChar};
                words.push_back(element);
            }


        }
    }

    //printing

    if (words.empty())
    {
        return;
    }
    if (words.at(0).token == Token::Punct || words.at(0).token == Token::Dash)
    {
        throw std::invalid_argument("Invalid input, first element is punctuation mark\n");
    }

    for (std::size_t i = 0; i < words.size() - 1; i++)
    {
        if (words.at(i).token == Token::Punct && words.at(i + 1).token == Token::Punct)
        {
            throw std::invalid_argument("Two punctuation mark in a row\n");
        }
    }

    std::vector<std::string> lines;
    std::string line;
    std::size_t i = 0;
    std::size_t countLines = 0;
    while(i < words.size())
    {
        if (line.empty())
        {
            if (words.at(i).token == Token::Punct || words.at(i).token == Token::Dash)
            {
                if (words.at(i).token == Token::Punct && lines.at(countLines - 1).size() < width)
                {
                    lines.at(countLines - 1) += words.at(i).element;
                    i++;
                    continue;
                }
                line += words.at(i - 1).element;
                (words.at(i).token == Token::Dash) ? line += " " + words.at(i).element : line += words.at(i).element;
                auto pos = lines.at(countLines - 1).find_last_of(' ');
                lines.at(countLines - 1).erase(pos);
                i++;
                continue;
            }
            line += words.at(i).element;
            i++;
            continue;
        }

        if (line.size() + words.at(i).element.size() + 1 <= width)
        {
            if (words.at(i).token == Token::Punct)
            {
                line += words.at(i).element;
                i++;
                continue;
            }
            line += " " + words.at(i).element;
            i++;
            continue;
        }
        lines.push_back(line);
        countLines++;
        line.clear();
    }
    lines.push_back(line);

    std::for_each(lines.begin(), lines.end(), [](const std::string& str)
    {
        std::cout << str << '\n';
    });

}

