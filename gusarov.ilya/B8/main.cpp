#include <iostream>
#include <cstring>

void task(std::size_t width);

int main(int argc, char* argv[])
{
    try
    {
        if (argc > 3 || argc == 2)
        {
            throw std::invalid_argument("Incorrect number of arguments\n");
        }
        std::size_t width = 40;
        if (argc == 3)
        {
            if (std::strcmp(argv[1], "--line-width") != 0)
            {
                throw std::invalid_argument("Incorrect argument\n");
            }
            width = std::stoi(argv[2]);
            if (width < 25)
            {
                throw std::invalid_argument("Incorrect width of line\n");
            }
        }
        task(width);
    }

    catch (const std::exception &exception)
    {
        std::cerr << exception.what();
        return 1;
    }

    return 0;
}

