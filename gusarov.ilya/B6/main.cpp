#include <iostream>
#include <stdexcept>

void task();

int main() {
    try
    {
        task();
    }
    catch (const std::exception& e)
    {
        std::cout << e.what() << '\n';

        return 1;
    }
}
