
#include <climits>
#include "functor.h"

Functor::Functor() :
    max(LLONG_MIN),
    min(LLONG_MAX),
    mean(0),
    posNum(0),
    negNum(0),
    sumOdd(0),
    sumEven(0),
    empty(true),
    equals(false),
    first(0)
{
}

void Functor::operator()(long long num) {
    if (empty)
    {
        first = num;
        empty = false;
    }

    if (num > max)
        max = num;

    if (num < min)
        min = num;

    if (num > 0)
        ++posNum;

    if (num < 0)
        ++negNum;

    if (!(num % 2))
        sumEven += num;

    if (num % 2)
        sumOdd += num;

    equals = first == num;
}

long long Functor::getMax() {
    return max;
}

long long Functor::getMin() {
    return min;
}

double Functor::getMean() {
    if (negNum + posNum == 0)
        return 0.0;
    return (static_cast<double>(sumOdd + sumEven) / (negNum + posNum));
}

long long Functor::getPosNum() {
    return posNum;
}

long long Functor::getNegNum() {
    return negNum;
}

long long Functor::getSumOdd() {
    return sumOdd;
}

long long Functor::getSumEven() {
    return sumEven;
}

bool Functor::getEmpty() {
    return empty;
}

bool Functor::getEquals() {
    return equals;
}

