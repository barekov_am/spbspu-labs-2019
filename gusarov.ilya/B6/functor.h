#ifndef FUNCTOR_H
#define FUNCTOR_H


class Functor {
public:
    Functor();

    void operator()(long long num);

    long long getMax();
    long long getMin();
    double getMean();
    long long getPosNum();
    long long getNegNum();
    long long getSumOdd();
    long long getSumEven();
    bool getEmpty();
    bool getEquals();

private:
    long long max;
    long long min;
    double mean;
    long long posNum;
    long long negNum;
    long long sumOdd;
    long long sumEven;
    bool empty;
    bool equals;
    long long first;
};


#endif
