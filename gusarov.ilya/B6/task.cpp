
#include <iostream>
#include <vector>
#include <algorithm>
#include "functor.h"

void task()
{
    long long num;
    std::vector<long long> vec;
    while (std::cin >> num)
    {
        vec.push_back(num);
    }
    if (!std::cin.eof()) {
        throw std::ios_base::failure("Failed reading from stream\n");
    }

    Functor functor;
    functor = std::for_each(vec.begin(), vec.end(), functor);
    if (functor.getEmpty()) {
        std::cout << "No Data\n";
    }
    else {
        std::cout << "Max: " << functor.getMax() << "\n";
        std::cout << "Min: " << functor.getMin() << "\n";
        std::cout << "Mean: " << std::fixed << functor.getMean() << "\n";
        std::cout << "Positive: " << functor.getPosNum() << "\n";
        std::cout << "Negative: " << functor.getNegNum() << "\n";
        std::cout << "Odd Sum: " << functor.getSumOdd() << "\n";
        std::cout << "Even Sum: " << functor.getSumEven() << "\n";
        std::cout << "First/Last Equal: " << (functor.getEquals() ? "yes" : "no") << "\n";
    }

}

