#ifndef TASKS_HPP
#define TASKS_HPP

#include <iostream>

void task1(const char *);
void task2(const char *);
void task3();
void task4(const char *, int);

#endif
