#include <forward_list>
#include <vector>

#include "functions.hpp"
#include "tasks.hpp"
#include "strategy.hpp"

void task1(const char* dir)
{
    auto direction = getDir<int>(dir);
    std::vector<int> vectorBrackets;

    int i = -1;
    while (std::cin >> i)
    {
        vectorBrackets.push_back(i);
    }

    if (!std::cin.eof() && std::cin.fail()) {
        throw std::invalid_argument("Incorect input");
    }

    if (vectorBrackets.empty()) {
        return;
    }

    std::vector<int> vectorAt(vectorBrackets);
    std::forward_list<int> list(vectorBrackets.begin(), vectorBrackets.end());

    sort<AccessBrackets>(vectorBrackets, direction);
    print(vectorBrackets);

    sort<AccessAt>(vectorAt, direction);
    print(vectorAt);

    sort<AccessIterator>(list, direction);
    print(list);

}


