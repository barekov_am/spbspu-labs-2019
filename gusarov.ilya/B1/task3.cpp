#include <iterator>
#include <iostream>
#include <vector>

#include "functions.hpp"
#include "tasks.hpp"

void task3()
{
    std::vector<int> vector;

    int number;
    while (std::cin >> number)
    {
        if (number == 0)
        {
            break;
        }
        vector.push_back(number);
    }
    if (!std::cin.eof() && std::cin.fail())
    {
        throw std::invalid_argument("Incorect input");
    }

    if (vector.empty())
    {
        return;
    }

    if (number)
    {
        throw std::invalid_argument("Last is not zero");
    }

    switch(vector.back())
    {
        case 1:
        {
            auto iter = vector.begin();
            while (iter != vector.end())
            {
                iter = (*iter % 2 == 0) ? vector.erase(iter) : ++iter;
            }

            break;
        }
        case 2:
        {
            auto iter = vector.begin();
            while (iter != vector.end())
            {
                iter = (*iter % 3 == 0) ? vector.insert(++iter, 3, 1) : ++iter;
            }

            break;
        }
    }

    print(vector);
}

