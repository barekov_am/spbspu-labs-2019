#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <functional>
#include <cstring>
#include <iostream>

template <typename T>
std::function<bool (T, T)> getDir(const char* dir)
{
    if (!std::strcmp(dir, "descending"))
    {
        return [](T a, T b) { return a > b; };
    }

    if (!std::strcmp(dir, "ascending"))
    {
        return [](T a, T b) { return a < b; };
    }

    throw std::invalid_argument("Incorrect direction");
}

template <template <class Collection> class Access, typename Collection>
void sort(Collection& collection, std::function<bool (typename Collection::value_type, typename Collection::value_type)> comparator)
{
    using Strategy = Access<Collection>;

    for (auto i = Strategy::begin(collection); i != Strategy::end(collection); ++i)
    {
        for (auto j = Strategy::next(i); j != Strategy::end(collection); ++j)
        {
            if (comparator(Strategy::getElem(collection, j), Strategy::getElem(collection, i)))
            {
                std::swap(Strategy::getElem(collection, j), Strategy::getElem(collection, i));
            }
        }
    }
}

template <typename Collection>
void print(const Collection& collection)
{
    for (auto elem : collection)
    {
        std::cout << elem << " ";
    }
    std::cout << '\n';
}

#endif
