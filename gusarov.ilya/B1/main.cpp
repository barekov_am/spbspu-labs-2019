#include <iostream>

#include "tasks.hpp"

int main(int argc, char* argv[]) {
    if (argc == 1 || argc > 4)
    {
        std::cerr << "Incorect number of arguments" << '\n';

        return 1;
    }
    try
    {
        switch (atoi(argv[1]))
        {
            case 1:
                if (argc < 3)
                {
                    std::cerr << "Incorect number of arguments" << '\n';

                    return 1;
                }

                task1(argv[2]);

                break;
            case 2:
                if (argc < 3)
                {
                    std::cerr << "Incorect number of arguments" << '\n';

                    return 1;
                }
                task2(argv[2]);

                break;
            case 3:
                task3();

                break;
            case 4:
                if (argc < 4)
                {
                    std::cerr << "Incorect number of arguments" << '\n';

                    return 1;
                }
                task4(argv[2], atoi(argv[3]));

                break;
            default:
                std::cerr << "Incorect task number" << '\n';

                return 1;
        }
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what();

        return 1;
    }


    return 0;
}
