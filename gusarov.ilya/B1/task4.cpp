#include <vector>
#include <ctime>

#include "functions.hpp"
#include "tasks.hpp"
#include "strategy.hpp"

void fillRandom(double* array, int size)
{
    const double min = -1.0;
    const double max = 1.0;

    for (int i = 0; i < size; ++i)
    {
        array[i] = min + static_cast<double>(rand()) / static_cast<double>(RAND_MAX / (max - min));
    }
}

void task4(const char* dir, int size)
{
    if (size <= 0)
    {
        throw std::invalid_argument("Wrong size");
    }

    srand(static_cast<unsigned>(time(nullptr)));

    auto direction = getDir<double>(dir);
    std::vector<double> vectorBrackets(static_cast<unsigned>(size));

    fillRandom(vectorBrackets.data(), size);

    print(vectorBrackets);

    sort<AccessBrackets>(vectorBrackets, direction);

    print(vectorBrackets);
}
