#ifndef STRATEGY_HPP
#define STRATEGY_HPP

#include <iterator>

template <typename Collection>
struct AccessBrackets
{
    static typename Collection::reference getElem(Collection& collection, std::size_t index)
    {
        if (index >= collection.size())
        {
            throw std::out_of_range("Index is out of range");
        }
        return collection[index];
    }
    static std::size_t begin(const Collection&)
    {
        return 0;
    }
    static std::size_t end(const Collection& collection)
    {
        return collection.size();
    }
    static std::size_t next(std::size_t index)
    {
        return index++;
    }
};

template <typename Collection>
struct AccessAt
{
    static typename Collection::reference getElem(Collection& collection, std::size_t index)
    {
        return collection.at(index);
    }
    static std::size_t begin(const Collection&)
    {
        return 0;
    }
    static std::size_t end(const Collection& collection)
    {
        return collection.size();
    }
    static std::size_t next(std::size_t index)
    {
        return index++;
    }
};

template <typename Collection>
struct AccessIterator
{
    static typename Collection::reference getElem(const Collection&, typename Collection::iterator iterator)
    {
        return *iterator;
    }
    static typename Collection::iterator begin(Collection& collection)
    {
        return collection.begin();
    }
    static typename Collection::iterator end(Collection& collection)
    {
        return collection.end();
    }
    static typename Collection::iterator next(typename Collection::iterator iterator)
    {
        return iterator++;
    }
};

#endif
