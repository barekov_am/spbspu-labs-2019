#include <memory>
#include <vector>
#include <fstream>

#include "tasks.hpp"

const int SIZE = 256;

void task2(const char* file)
{
    std::ifstream inputFile(file);

    if (!inputFile)
    {
        throw std::ios_base::failure("Failed opening the file");
    }

    using typeCharArr = std::unique_ptr<char[], decltype(&free)>;
    typeCharArr charArr(static_cast<char*>(malloc(SIZE)), &free);

    if (!charArr)
    {
        throw std::runtime_error("Can`t allocate memory");
    }

    int size = SIZE;
    std::size_t i = 0;

    while (!inputFile.eof())
    {
        inputFile.read(&charArr[i], SIZE);
        i += static_cast<std::size_t>(inputFile.gcount());

        if (inputFile.gcount() == SIZE)
        {
            size += SIZE;
            typeCharArr tmpCharArr(static_cast<char*>(realloc(charArr.get(), size)), &free);

            if (!tmpCharArr)
            {
                throw std::runtime_error("Can`t allocate memory");
            }

            charArr.release();
            std::swap(charArr, tmpCharArr);
        }
        if (!inputFile.eof() && inputFile.fail())
        {
            throw std::runtime_error("Data reading failed");
        }
    }

    std::vector<char> vector(&charArr[0], &charArr[i]);

    for (auto elem : vector)
    {
        std::cout << elem;
    }
}
