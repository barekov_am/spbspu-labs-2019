#ifndef DATASTRUCT_H
#define DATASTRUCT_H

#include <string>

struct DataStruct
{
    int key1;
    int key2;
    std::string str;
};

#endif
