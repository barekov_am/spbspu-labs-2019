#include <vector>
#include <iostream>
#include <boost/tokenizer.hpp>
#include <functional>
#include <boost/algorithm/string.hpp>

#include "DataStruct.hpp"

DataStruct parse(const std::string& input)
{
    DataStruct result;
    if (input.empty())
    {
        throw std::invalid_argument("Empty string\n");
    }

    boost::char_separator<char> sep(",");
    boost::tokenizer<boost::char_separator<char> > tokens(input, sep);

    auto token = tokens.begin();
    std::string str(*token);
    boost::trim(str);
    if (!str.empty())
    {
        if (str.length() <= 2 && std::stoi(str) <= 5 && std::stoi(str) >= -5)
            result.key1 = std::stoi(str);
        else
            throw std::out_of_range("Argument value is out of range\n");
    }
    else
        throw std::out_of_range("Argument value is incorrect\n");

    token = ++tokens.begin();
    str = *token;
    boost::trim(str);
    if (!str.empty())
    {
        if (str.length() <= 2 && std::stoi(str) <= 5 && std::stoi(str) >= -5)
            result.key2 = std::stoi(str);
        else
            throw std::out_of_range("Argument value is out of range\n");
    }
    else
        throw std::out_of_range("Argument value is incorrect\n");

    ++token;

    result.str = *token;

    return result;
}

bool comparator(const DataStruct& i1, const DataStruct& i2)
{
    if (i1.key1 < i2.key1)
        return true;


    if (i1.key1 == i2.key1)
    {
        if (i1.key2 < i2.key2)
            return true;


        if ((i1.key2 == i2.key2) && (i1.str.length() < i2.str.length()))
            return true;
    }

    return false;
}


void task()
{
    std::vector<DataStruct> vector;

    std::string line;

    while(std::getline(std::cin, line))
    {
        if (std::cin.fail())
            throw std::ios_base::failure("Reading from stream has failed!\n");

        vector.push_back(parse(line));
    }

    std::sort(vector.begin(), vector.end(), comparator);

    for (const DataStruct& elem : vector)
    {
        std::cout << elem.key1 << ',' << elem.key2 << ',' << elem.str << "\n";
    };
}

