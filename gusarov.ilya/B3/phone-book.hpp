#ifndef PHONE_BOOK_HPP
#define PHONE_BOOK_HPP

#include <list>
#include <string>

#include "direction.hpp"

class PhoneBook
{

public:

    struct Note
    {
        std::string telephone_number;
        std::string name;
    };

    using iterator = std::list< Note >::iterator;

    std::string getNote(std::list<Note>::iterator) const;
    std::list<Note>::iterator nextPrevNote(std::list<Note>::iterator, std::ptrdiff_t);
    void insertNote(std::list<Note>::iterator, const Note&, Direction);
    void insertNote(const Note&);

    std::list<Note>::iterator deleteNote(std::list<Note>::iterator);

    iterator begin();
    iterator last();

    bool isEmpty() const;
private:
    std::list<Note> list;
};

#endif
