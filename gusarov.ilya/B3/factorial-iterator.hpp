#ifndef FACTORIAL_ITERATOR_H
#define FACTORIAL_ITERATOR_H

#include <iterator>

class FactorialIterator: public
        std::iterator<std::bidirectional_iterator_tag, size_t>
{
public:
    explicit FactorialIterator(size_t);

    size_t &operator *();

    FactorialIterator &operator ++();
    FactorialIterator &operator --();

    bool operator ==(const FactorialIterator&) const;
    bool operator !=(const FactorialIterator&) const;

private:
    size_t _index;
    size_t _factorial;
    static size_t getFactorial(size_t);
};

#endif
