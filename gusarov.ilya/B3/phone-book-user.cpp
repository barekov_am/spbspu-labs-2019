#include "phone-book-user.hpp"

PhoneBookUser::PhoneBookUser()
{
    bookmarks.insert(std::pair<std::string, PhoneBook::iterator>("current", phonebook.begin()));
}

void PhoneBookUser::addNote(const PhoneBook::Note& note)
{
    phonebook.insertNote(note);
    if (!phonebook.isEmpty() && bookmarks["current"] == phonebook.last())
    {
        bookmarks["current"] = phonebook.begin();
    }

}

void PhoneBookUser::store(const std::string& bookmarkName, const std::string& newBookmarkName)
{
    bookmarks.insert(std::pair<std::string, PhoneBook::iterator>(newBookmarkName, bookmarks[bookmarkName]));
}

void PhoneBookUser::insert(const Direction dir, const PhoneBook::Note& note, const std::string& booksmarkName)
{
    phonebook.insertNote(bookmarks[booksmarkName], note, dir);
    if (!phonebook.isEmpty() && bookmarks["current"] == phonebook.last())
    {
        bookmarks["current"] = phonebook.begin();
    }

}

void PhoneBookUser::remove(const std::string& booksmarkName)
{
    if (phonebook.isEmpty())
        return;
    auto next_of_deleted = phonebook.deleteNote(bookmarks[booksmarkName]);
    auto deleted_value = bookmarks[booksmarkName];
    for (auto & bookmark : bookmarks)
    {
        if (bookmark.second == deleted_value)
            bookmark.second = next_of_deleted;
    }
}

std::string PhoneBookUser::show(const std::string& booksmarkName)
{
    if (phonebook.getNote(bookmarks[booksmarkName]).empty()) {
        return "";
    }

    return phonebook.getNote(bookmarks[booksmarkName]);
}

void PhoneBookUser::move(const std::string& booksmarkName, int n)
{
    bookmarks[booksmarkName] = phonebook.nextPrevNote(bookmarks[booksmarkName], static_cast<std::ptrdiff_t>(n));
}

void PhoneBookUser::move(const std::string& booksmarkName, steps step)
{
    if (step == steps::FIRST)
    {
        bookmarks[booksmarkName] = phonebook.begin();
    }
    if (step == steps::LAST)
    {
        bookmarks[booksmarkName] = --phonebook.last();
    }
}

int PhoneBookUser::isMarkExists(const std::string& bookmarkName)
{
    if (bookmarks.count(bookmarkName))
        return 1;
    return 0;
}

bool PhoneBookUser::isEmpty() const {
    return phonebook.isEmpty();
}
