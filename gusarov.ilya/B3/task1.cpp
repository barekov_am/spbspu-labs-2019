#include <iostream>
#include <string>
#include <regex>
#include <boost/algorithm/string/replace.hpp>

#include "phone-book-user.hpp"
#include "direction.hpp"

int preprocessName(std::string& name)
{
    if (name[0] == '"' && name[name.length()-1] == '"')
    {
        name.erase(0, 1);
        name.erase(name.size()-1, 1);
        boost::replace_all(name, "\\", "");
        boost::replace_all(name, "\\n", " ");
    }
    else
    {
        std::cout << "<INVALID COMMAND>\n";
        return 0;
    }
    return 1;
}

void task1()
{
    PhoneBookUser phoneBook;
    std::string in;

    while (getline(std::cin, in))
    {
        std::istringstream buffer(in);
        std::string comandType;
        buffer >> comandType;
        if (comandType == "show")
        {
            std::string bookmark;
            buffer >> bookmark;
            if (phoneBook.isMarkExists(bookmark))
            {
                if (std::regex_match(bookmark, std::regex("[-*0-9a-zA-Z]+")))
                {
                    if (phoneBook.isEmpty())
                        std::cout << "<EMPTY>\n";
                    else if (phoneBook.show(bookmark).empty())
                    {
                        phoneBook.move(bookmark, phoneBook.steps::FIRST);
                        std::cout << phoneBook.show(bookmark) << '\n';
                    }
                    else
                    {
                        std::cout << phoneBook.show(bookmark) << '\n';
                    }

                }
                else
                {
                    std::cout << "<INVALID COMMAND>\n";
                }
            }
            else
            {
                std::cout << "<INVALID BOOKMARK>\n";
            }
        }
        else
        {
            switch (comandType.at(0))
            {
                case 'a':
                {
                    std::string number;
                    std::string name;
                    buffer >> number;
                    buffer >> std::ws;
                    std::getline(buffer, name);


                    if (preprocessName(name))
                        phoneBook.addNote({number, name});

                    break;
                }
                case 's':
                {
                    std::string oldName;
                    std::string newName;
                    buffer >> oldName;
                    buffer >> newName;

                    if (std::regex_match(newName, std::regex("[-*0-9a-zA-Z]+")))
                    {
                        if (!phoneBook.isMarkExists(oldName))
                            std::cout << "<INVALID BOOKMARK>\n";

                        phoneBook.store(oldName, newName);
                    }
                    else
                    {
                        std::cout << "<INVALID COMMAND>\n";
                    }

                    break;
                }
                case 'i':
                {
                    std::string nextCom;
                    std::string markName;
                    std::string name;
                    std::string number;
                    buffer >> nextCom;
                    buffer >> markName;
                    buffer >> number;
                    buffer >> std::ws;
                    std::getline(buffer, name);
                    if (nextCom == "before")
                    {
                        if (std::regex_match(markName, std::regex("[-*0-9a-zA-Z]+")))
                        {
                            if (!phoneBook.isMarkExists(markName))
                                std::cout << "<INVALID BOOKMARK>\n";

                            if (preprocessName(name))
                                phoneBook.insert(Direction::PREV, {number, name}, markName);

                        }
                        else
                        {
                            std::cout << "<INVALID COMMAND>\n";
                        }
                    }
                    else if (nextCom == "after")
                    {
                        if (std::regex_match(markName, std::regex("[-*0-9a-zA-Z]+")))
                        {
                            if (!phoneBook.isMarkExists(markName))
                                std::cout << "<INVALID BOOKMARK>\n";

                            if (preprocessName(name))
                                phoneBook.insert(Direction::NEXT, {number, name}, markName);

                        }
                        else
                        {
                            std::cout << "<INVALID COMMAND>\n";
                        }
                    }
                    else
                    {
                        std::cout << "<INVALID COMMAND>\n";
                    }



                    break;
                }
                case 'd':
                {
                    std::string bookmark;
                    buffer >> bookmark;

                    if (std::regex_match(bookmark, std::regex("[-*0-9a-zA-Z]+")))
                    {
                        if (!phoneBook.isMarkExists(bookmark))
                            std::cout << "<INVALID BOOKMARK>\n";

                        phoneBook.remove(bookmark);
                    }
                    else
                    {
                        std::cout << "<INVALID COMMAND>\n";
                    }

                    break;
                }
                case 'm':
                {
                    std::string bookmark;
                    std::string steps;
                    buffer >> bookmark;
                    buffer >> steps;

                    if (std::regex_match(bookmark, std::regex("[-*0-9a-zA-Z]+")))
                    {
                        if (!phoneBook.isMarkExists(bookmark))
                            std::cout << "<INVALID BOOKMARK>\n";

                        if (steps == "first")
                        {
                            phoneBook.move(bookmark, phoneBook.steps::FIRST);
                        }
                        else if (steps == "last")
                        {
                            phoneBook.move(bookmark, phoneBook.steps::LAST);
                        }

                        else if(std::regex_match(steps, std::regex("[+-]?[0-9]+")))
                        {
                            phoneBook.move(bookmark, std::stoi(steps));
                        }
                        else
                        {
                            std::cout << "<INVALID STEP>\n";
                        }

                    }
                    else
                    {
                        std::cout << "<INVALID COMMAND>\n";
                    }

                    break;
                }
                default:
                    std::cout << "<INVALID COMMAND>\n";
            }
        }
    }
}
