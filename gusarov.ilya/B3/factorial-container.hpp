#ifndef FACTORIAL_CONTAINER_HPP
#define FACTORIAL_CONTAINER_HPP

#include "factorial-iterator.hpp"

class FactorialContainer
{
public:
    static const int MAX = 11;
    static const int MIN = 1;

    FactorialContainer() = default;

    FactorialIterator begin();
    FactorialIterator end();
};


#endif
