#include "phone-book.hpp"
#include <iostream>

std::string PhoneBook::getNote(const std::list<PhoneBook::Note>::iterator iter) const
{
    if (iter == list.end() && !list.empty()) {
        return "";
    }
    return iter->telephone_number + " " + iter->name;
}

std::list<PhoneBook::Note>::iterator PhoneBook::nextPrevNote(std::list<PhoneBook::Note>::iterator iter, const std::ptrdiff_t n)
{
    if (n > 0)
    {
        if (std::distance(iter, list.end()) >= n)
            return std::next(iter, static_cast<long>(n));
    }

    if (n < 0)
    {
        if (std::distance(list.begin(), iter) >= abs(static_cast<int>(n)))
            return std::prev(iter, abs(static_cast<int>(n)));
    }

    return iter;
}

void PhoneBook::insertNote(std::list<PhoneBook::Note>::iterator iter, const PhoneBook::Note& note, const Direction dir)
{
    if (dir == Direction::NEXT)
    {
        if (iter == list.end())
            list.push_back(note);
        else
            list.insert(++iter, note);
    }

    if (dir == Direction::PREV)
    {
        if (iter == list.begin())
            list.push_front(note);
        else
        {
            list.insert(iter, note);
        }
    }
}

void PhoneBook::insertNote(const PhoneBook::Note& note)
{
    list.push_back(note);

}

std::list<PhoneBook::Note>::iterator PhoneBook::deleteNote(std::list<Note>::iterator iter)
{
    if (iter != list.end())
    {
        return list.erase(iter);
    }

    return iter;
}

PhoneBook::iterator PhoneBook::begin()
{
    return list.begin();
}

PhoneBook::iterator PhoneBook::last()
{
    return list.end();
}

bool PhoneBook::isEmpty() const
{
    if (list.empty())
        return 1;
    return 0;
}
