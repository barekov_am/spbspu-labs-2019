#ifndef PHONE_BOOK_USER_HPP
#define PHONE_BOOK_USER_HPP

#include <iostream>
#include <map>
#include <list>

#include "phone-book.hpp"

class PhoneBookUser
{
private:
    using map = std::map<std::string, PhoneBook::iterator>;
    map bookmarks;
    PhoneBook phonebook;
public:

    enum steps {
        FIRST,
        LAST
    };

    PhoneBookUser();
    void addNote(const PhoneBook::Note&);
    void store(const std::string&, const std::string&);
    int isMarkExists(const std::string&);
    void insert(Direction, const PhoneBook::Note&, const std::string&);
    void remove(const std::string&);
    std::string show(const std::string&);
    void move(const std::string&, int);
    void move(const std::string&, steps);
    bool isEmpty() const;
};

#endif
