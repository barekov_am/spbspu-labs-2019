#include <iostream>

#include "phone-book.hpp"

void task1();
void task2();


int main(int argc, char *argv[])
{
    try
    {
        if (argc != 2)
        {
            std::cerr << "Invalid number of arguments" << '\n';

            return 2;
        }
        switch (atoi(argv[1]))
        {
            case 1:
                task1();
                break;
            case 2:
                task2();
                break;
            default:
                std::cerr << "Invalid argument" << '\n';

                return 2;
        }

    }
    catch (const std::exception& e)
    {
        std::cout << e.what() << '\n';

        return 2;
    }


    return 0;
}
