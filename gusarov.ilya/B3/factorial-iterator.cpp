#include "factorial-iterator.hpp"
#include "factorial-container.hpp"

FactorialIterator::FactorialIterator(size_t index):
    _index(index),
    _factorial(getFactorial(index))
{
}

size_t &FactorialIterator::operator*() {
    return _factorial;
}


FactorialIterator &FactorialIterator::operator++() {
    if (_index > FactorialContainer::MAX)
    {
        throw std::out_of_range("Index is out of range\n");
    }
    ++_index;
    _factorial *= _index;

    return *this;
}

FactorialIterator &FactorialIterator::operator--() {
    if (_index < FactorialContainer::MIN)
    {
        throw std::out_of_range("Index is out of range\n");
    }
    _factorial /= _index;
    --_index;

    return *this;
}

bool FactorialIterator::operator ==(const FactorialIterator& iterator) const
{
    return (_index == iterator._index);
}


bool FactorialIterator::operator !=(const FactorialIterator& iterator) const
{
    return !(*this == iterator);
}

size_t FactorialIterator::getFactorial(size_t index) {
    if ( (index < FactorialContainer::MIN - 1) || (index > FactorialContainer::MAX) ) {
        throw std::out_of_range("Index is out of range\n");
    }

    size_t factorial = 1;
    for (size_t i = 1; i < index + 1; ++i)
    {
        factorial *= i;
    }

    return factorial;

}
