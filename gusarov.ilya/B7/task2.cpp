#include <list>
#include <memory>
#include <iostream>
#include <boost/algorithm/string.hpp>

#include "Shape.h"
#include "square.h"
#include "circle.h"
#include "triangle.h"

void parse(std::string& in, std::string& shape, point& center)
{
    std::size_t openBracket = in.find_first_of('(');
    std::size_t delimeter = in.find_first_of(';');
    std::size_t closeBracket = in.find_first_of(')');

    if ((openBracket == std::string::npos) || (delimeter == std::string::npos) || (closeBracket == std::string::npos))
        throw std::invalid_argument("Invalid point\n");

    shape = in.substr(0, openBracket);

    center = {
            std::stoi(in.substr(openBracket + 1, delimeter - openBracket - 1)),
            std::stoi(in.substr(delimeter + 1, closeBracket - delimeter - 1))
    };
}

void task2()
{
    std::list<std::unique_ptr<Shape>> list;
    std::string in;

    while (std::getline(std::cin, in))
    {
        if (!std::cin.eof() && std::cin.fail())
            throw std::ios::failure("Failed reading input stream");

        boost::trim(in);

        if (in.empty())
            continue;

        std::string shape;
        point center{};

        parse(in, shape, center);

        boost::trim(shape);

        if (shape == "SQUARE")
            list.push_back(std::make_unique<Square>(center));
        else if (shape == "CIRCLE")
            list.push_back(std::make_unique<Circle>(center));
        else if (shape == "TRIANGLE")
            list.push_back(std::make_unique<Triangle>(center));

        else
            throw std::invalid_argument("Shape name is undefined\n");

    }

    std::cout << "Original:\n";
    std::for_each(list.begin(), list.end(), [](const auto& ptr) { ptr->draw(); });

    std::cout << "Left-Right:\n";
    list.sort([](const auto& i1, const auto& i2) { return i1->isMoreLeft(*i2); });
    std::for_each(list.begin(), list.end(), [](const auto& ptr) { ptr->draw(); });

    std::cout << "Right-Left:\n";
    list.sort([](const auto& i1, const auto& i2) { return i2->isMoreLeft(*i1); });
    std::for_each(list.begin(), list.end(), [](const auto& ptr) { ptr->draw(); });

    std::cout << "Top-Bottom:\n";
    list.sort([](const auto& i1, const auto& i2) { return i1->isMoreUpper(*i2); });
    std::for_each(list.begin(), list.end(), [](const auto& ptr) { ptr->draw(); });

    std::cout << "Bottom-Top:\n";
    list.sort([](const auto& i1, const auto& i2) { return i2->isMoreUpper(*i1); });
    std::for_each(list.begin(), list.end(), [](const auto& ptr) { ptr->draw(); });

}
