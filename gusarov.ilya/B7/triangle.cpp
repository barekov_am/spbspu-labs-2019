#include <iostream>
#include "triangle.h"

Triangle::Triangle(const point &center) :
    Shape(center)
{
}

void Triangle::draw() const
{
    std::cout << "TRIANGLE (" << center.x << ";" << center.y << ")\n";
}
