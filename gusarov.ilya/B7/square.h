#ifndef SQUARE_H
#define SQUARE_H

#include "Shape.h"

class Square: public Shape
{
public:
    explicit Square(const point &center);

    void draw() const override;
};


#endif
