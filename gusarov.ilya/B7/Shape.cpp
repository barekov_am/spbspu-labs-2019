#include "Shape.h"

bool Shape::isMoreLeft(const Shape &shape) const
{
    return (center.x < shape.center.x);
}

bool Shape::isMoreUpper(const Shape &shape) const
{
    return (center.y > shape.center.y);
}

Shape::Shape(const point &center):
    center(center)
{
}
