#include <iostream>
#include "square.h"

Square::Square(const point &center):
    Shape(center)
{
}

void Square::draw() const
{
    std::cout << "SQUARE (" << center.x << ";" << center.y << ")\n";
}
