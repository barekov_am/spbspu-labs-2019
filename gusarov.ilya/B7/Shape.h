#ifndef SHAPE_H
#define SHAPE_H

#include "point.h"

class Shape
{
public:
    Shape(const point &center);

    bool isMoreLeft(const Shape &shape) const;
    bool isMoreUpper(const Shape &shape) const;

    virtual void draw() const = 0;

protected:
    point center;
};


#endif
