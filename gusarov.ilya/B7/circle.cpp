#include <iostream>
#include "circle.h"

Circle::Circle(const point &center) :
    Shape(center)
{
}

void Circle::draw() const
{
    std::cout << "CIRCLE (" << center.x << ";" << center.y << ")\n";
}
