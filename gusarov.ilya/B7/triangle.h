#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Shape.h"

class Triangle : public Shape
{
public:
    explicit Triangle(const point &center);

    void draw() const override;
};


#endif
