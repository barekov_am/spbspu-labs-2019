#ifndef CIRCLE_H
#define CIRCLE_H

#include "Shape.h"

class Circle: public Shape
{
public:
    explicit Circle(const point &center);

    void draw() const override;
};


#endif
