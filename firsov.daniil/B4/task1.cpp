#include <iostream>
#include <sstream>
#include <algorithm>

#include "dataStruct.hpp"
#include "functions.hpp"

void task1()
{
  std::vector<DataStruct> vector;
  std::string line;
  while (std::getline(std::cin, line)) {
    if (!std::cin && !std::cin.eof()) {
      throw std::ios_base::failure("Failed input. \n");
    }

    std::istringstream stream(line);
    vector.push_back(readStruct(stream));
  }
  sort(vector);
  std::for_each(vector.begin(), vector.end(), print);
}
