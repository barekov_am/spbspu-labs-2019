#ifndef B4_FUNCTIONS_HPP
#define B4_FUNCTIONS_HPP

#include <vector>
#include "dataStruct.hpp"

struct compare {
  bool operator()(const DataStruct& data1, const DataStruct& data2) {
    if (data1.key1 < data2.key1) {
      return true;
    }

    if (data1.key1 == data2.key1) {
      if (data1.key2 < data2.key2) {
        return true;
      }

      if (data1.key2 == data2.key2) {
        return data1.str.length() < data2.str.length();
      }
    }

    return false;
  }
};

void print(const DataStruct& data);
void sort(std::vector<DataStruct>& vector);

#endif
