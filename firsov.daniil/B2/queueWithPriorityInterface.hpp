#ifndef B2_QUEUEWITHPRIORITYINTERFACE_HPP
#define B2_QUEUEWITHPRIORITYINTERFACE_HPP

#include <list>
#include <iostream>

template <typename T>
class QueueWithPriority
{
public:
  enum ElementPriority
  {
    HIGH,
    NORMAL,
    LOW
  };

  void putElementToQueue(ElementPriority priority, const T& element);
  T getElementFromQueue();
  void accelerate();
  bool empty() const;

private:
  std::list<T> highPriority_;
  std::list<T> normalPriority_;
  std::list<T> lowPriority_;
};

#endif

