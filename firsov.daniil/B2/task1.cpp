#include <functional>
#include <algorithm>
#include <sstream>
#include <map>
#include "commands.hpp"

void task1()
{
  using executeCommand = std::function<void(QueueWithPriority<std::string>&, std::stringstream&)>;

  std::map<const std::string, executeCommand> commands =
    {
      {"add", executeAdd},
      {"get", executeGet},
      {"accelerate", executeAccelerate}
    };

  QueueWithPriority<std::string> queue;

  std::string inputStr;

  while (std::getline(std::cin, inputStr))
  {
    std::stringstream lineStream(inputStr);
    std::string commandName;
    lineStream >> commandName;

    auto check = [&](const std::pair<const std::string, executeCommand>& pair) {
      return (pair.first == commandName);
    };

    auto command = std::find_if(std::begin(commands), std::end(commands), check);

    if (command != std::end(commands)) {
      command->second(queue, lineStream);
    }
    else {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }
  }
}

