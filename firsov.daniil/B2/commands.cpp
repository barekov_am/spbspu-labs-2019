#include <map>
#include <sstream>
#include <algorithm>
#include "commands.hpp"

void executeAdd(QueueWithPriority<std::string>& queue, std::stringstream& input)
{
  using priorityType = QueueWithPriority<std::string>::ElementPriority;

  std::map<const std::string, priorityType> priorities = {
    {"high", priorityType::HIGH},
    {"normal", priorityType::NORMAL},
    {"low", priorityType::LOW}
  };

  std::string priorityName;
  input >> std::ws >> priorityName;

  auto check = [&](const std::pair<const std::string, priorityType>& pair) {
    return (pair.first == priorityName);
  };

  auto argPriority = std::find_if(std::begin(priorities), std::end(priorities), check);

  if (argPriority == std::end(priorities)) {
    std::cout << "<INVALID COMMAND>" << std::endl;
    return;
  }

  std::string data;
  std::getline(input >> std::ws, data);

  if (data.empty()) {
    std::cout << "<INVALID COMMAND>" << std::endl;
    return;
  }

  queue.putElementToQueue(argPriority->second, data);
}

void executeGet(QueueWithPriority<std::string>& queue, std::stringstream& input) {
  std::string data;
  std::getline(input >> std::ws, data);

  if (!data.empty()) {
    std::cout << "<INVALID COMMAND>" << std::endl;
    return;
  }

  if (queue.empty()) {
    std::cout << "<EMPTY>" << std::endl;
    return;
  }

  std::string element = queue.getElementFromQueue();
  std::cout << element << "" << std::endl;
}

void executeAccelerate(QueueWithPriority<std::string>& queue, std::stringstream & input) {
  std::string data;
  std::getline(input >> std::ws, data);

  if (!data.empty()) {
    std::cout << "<INVALID COMMAND>" << std::endl;
    return;
  }

  queue.accelerate();
}

void printList(std::list<int>::iterator start, std::list<int>::iterator end) {
  if (start == end) {
    std::cout << std::endl;
    return;
  }
  if (start == std::prev(end)) {
    std::cout << *start << std::endl;
    return;
  }
  std::cout << *start << " " << *(std::prev(end)) << " ";

  start++;
  end--;

  printList(start, end);
}

