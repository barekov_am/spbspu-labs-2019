#include <stdexcept>
#include "commands.hpp"

const std::size_t maxSize = 20;
const int MAX = 20;
const int MIN = 1;

void task2() {
  std::list<int> list;
  std::size_t size = 0;
  int tmp = 0;

  while (std::cin && !(std::cin >> tmp).eof())
  {
    if (std::cin.fail()) {
      throw std::ios_base::failure("Couldn't read input file. \n");
    }

    if ((tmp > MAX) || (tmp < MIN)) {
      throw std::out_of_range("Number must be between 1 and 20. \n");
    }

    ++size;
    if (size > maxSize) {
      throw std::out_of_range("Count of numbers must be from 1 to 20. \n");
    }

    list.push_back(tmp);
  }

  printList(list.begin(), list.end());
};

