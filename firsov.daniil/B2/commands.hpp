#ifndef B2_COMMANDS_HPP
#define B2_COMMANDS_HPP

#include "queueWithPriority.hpp"

void executeAdd(QueueWithPriority<std::string> &queue, std::stringstream &input);
void executeGet(QueueWithPriority<std::string>& queue, std::stringstream& str);
void executeAccelerate(QueueWithPriority<std::string>& queue, std::stringstream& str);
void printList(std::list<int>::iterator start, std::list<int>::iterator end);

#endif

