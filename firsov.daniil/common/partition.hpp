#ifndef PARTITION_HPP
#define PARTITION_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace firsov
{
  firsov::Matrix part(const firsov::CompositeShape& compShape);
  bool isIntersected(const rectangle_t& shape1, const rectangle_t& shape2);
}

#endif
