#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(A2Test)

const double PRECISION = 0.001;

BOOST_AUTO_TEST_CASE(invariabilityAfterMovingRectangleByIncrements)
{
  firsov::Rectangle testRectangle(7, 6, { 3, 4 });
  const firsov::rectangle_t initialBefore = testRectangle.getFrameRect();
  const double initialArea = testRectangle.getArea();

  testRectangle.move(-4.5, 5.2);
  BOOST_CHECK_CLOSE(initialBefore.width, testRectangle.getFrameRect().width, PRECISION);
  BOOST_CHECK_CLOSE(initialBefore.height, testRectangle.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), PRECISION);
}

BOOST_AUTO_TEST_CASE(invariabilityAfterMovingRectangleToPoint)
{
  firsov::Rectangle testRectangle(7, 6, { 3, 4 });
  const firsov::rectangle_t initialBefore = testRectangle.getFrameRect();
  const double initialArea = testRectangle.getArea();

  testRectangle.move(-4.5, 5.2);
  BOOST_CHECK_CLOSE(initialBefore.width, testRectangle.getFrameRect().width, PRECISION);
  BOOST_CHECK_CLOSE(initialBefore.height, testRectangle.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), PRECISION);
}

BOOST_AUTO_TEST_CASE(testRectangleRotation)
{
  firsov::Rectangle testRectangle(2, 7.1, { 7, 2 });
  const double areaBefore = testRectangle.getArea();
  const firsov::rectangle_t frameRectBefore = testRectangle.getFrameRect();

  double angle = -90;
  testRectangle.rotate(angle);
  double areaAfter = testRectangle.getArea();
  firsov::rectangle_t frameRectAfter = testRectangle.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);

  angle = 45;
  testRectangle.rotate(angle);
  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);

  angle = 140;
  testRectangle.rotate(angle);
  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);
}

BOOST_AUTO_TEST_CASE(quadraticRectangleScaleChange)
{
  firsov::Rectangle testRectangle(3, 7, { 4, 5 });
  const double initialArea = testRectangle.getArea();
  const double multiplier = 0.5;

  testRectangle.scale(multiplier);
  BOOST_CHECK_CLOSE(testRectangle.getArea(), initialArea * multiplier * multiplier, PRECISION);
}

BOOST_AUTO_TEST_CASE(invalidRectangleParametersAndScaling)
{
  BOOST_CHECK_THROW(firsov::Rectangle(-4, 5, { 6, 7 }), std::invalid_argument);

  firsov::Rectangle testRectangle(4, 5, { 6, 7 });
  BOOST_CHECK_THROW(testRectangle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
