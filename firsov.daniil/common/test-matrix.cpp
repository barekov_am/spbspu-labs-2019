#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(testOfCopyConstructor)
{
  firsov::Shape::shape_ptr rectangle = std::make_shared<firsov::Rectangle>(6, 9, firsov::point_t{ 3, 4 });
  firsov::Shape::shape_ptr circle = std::make_shared<firsov::Circle>(4, firsov::point_t{5, 5});
  firsov::CompositeShape compShape(rectangle);
  compShape.add(circle);
  firsov::Matrix matrix = part(compShape);
  firsov::Matrix testMatrix(matrix);
  BOOST_CHECK(matrix == testMatrix);
  BOOST_CHECK_EQUAL(matrix.getRows(), testMatrix.getRows());
  BOOST_CHECK_EQUAL(matrix.getColumns(), testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(testOfMoveConstructor)
{
  firsov::Shape::shape_ptr rectangle = std::make_shared<firsov::Rectangle>(6, 9, firsov::point_t{ 3, 4 });
  firsov::Shape::shape_ptr circle = std::make_shared<firsov::Circle>(4, firsov::point_t{5, 5});
  firsov::CompositeShape compShape(rectangle);
  compShape.add(circle);
  firsov::Matrix matrix = part(compShape);
  firsov::Matrix matrix1(matrix);
  firsov::Matrix testMatrix(std::move(matrix));
  BOOST_CHECK(testMatrix == matrix1);
  BOOST_CHECK_EQUAL(testMatrix.getRows(), matrix1.getRows());
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), matrix1.getColumns());
}

BOOST_AUTO_TEST_CASE(testOfCopyOperator)
{
  firsov::Shape::shape_ptr rectangle = std::make_shared<firsov::Rectangle>(6, 9, firsov::point_t{ 3, 4 });
  firsov::Shape::shape_ptr circle = std::make_shared<firsov::Circle>(4, firsov::point_t{ 5, 5 });
  firsov::CompositeShape compShape(rectangle);
  compShape.add(circle);
  firsov::Matrix matrix = part(compShape);
  firsov::Matrix testMatrix = matrix;
  BOOST_CHECK(matrix == testMatrix);
  BOOST_CHECK_EQUAL(matrix.getRows(), testMatrix.getRows());
  BOOST_CHECK_EQUAL(matrix.getColumns(), testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(testOfMoveOperator)
{
  firsov::Shape::shape_ptr rectangle = std::make_shared<firsov::Rectangle>(6, 9, firsov::point_t{ 3, 4 });
  firsov::Shape::shape_ptr circle = std::make_shared<firsov::Circle>(4, firsov::point_t{ 5, 5 });
  firsov::CompositeShape compShape(rectangle);
  compShape.add(circle);
  firsov::Matrix matrix = part(compShape);
  firsov::Matrix matrix1(matrix);
  firsov::Matrix testMatrix = std::move(matrix);
  BOOST_CHECK(testMatrix == matrix1);
  BOOST_CHECK_EQUAL(testMatrix.getRows(), matrix1.getRows());
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), matrix1.getColumns());
}

BOOST_AUTO_TEST_CASE(testMatrixUsingOfEqualOperator)
{
  firsov::Shape::shape_ptr rect1Ptr = std::make_shared<firsov::Rectangle>( 5, 4, firsov::point_t { 1, 2 });
  firsov::Shape::shape_ptr rect2Ptr = std::make_shared<firsov::Rectangle>(3, 1, firsov::point_t { 7, 7} );
  firsov::Shape::shape_ptr circ1Ptr = std::make_shared<firsov::Circle>( 2, firsov::point_t { -1, 2 });
  firsov::Shape::shape_ptr circ2Ptr = std::make_shared<firsov::Circle>( 4, firsov::point_t { 0, 1 });

  firsov::CompositeShape compShape(rect1Ptr);
  compShape.add(circ1Ptr);
  compShape.add(rect2Ptr);

  firsov::Matrix matrix = part(compShape);
  firsov::Matrix equalMatrix(matrix);

  firsov::Matrix unequalMatrix(matrix);
  unequalMatrix.add(circ2Ptr, 1);

  BOOST_CHECK(matrix == equalMatrix);
  BOOST_CHECK(matrix != unequalMatrix);
}

BOOST_AUTO_TEST_CASE(testAddingToMatrix)
{
  firsov::Shape::shape_ptr testCircle = std::make_shared<firsov::Circle>(2, firsov::point_t{ 0, 0 });
  firsov::Shape::shape_ptr testRectangle1 = std::make_shared<firsov::Rectangle>(2, 1, firsov::point_t{ 14, 12 });
  firsov::Shape::shape_ptr testRectangle2 = std::make_shared<firsov::Rectangle>(24, 12, firsov::point_t{ 0, 0 });
  firsov::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle1);
  testComposite.add(testRectangle2);
  size_t rows = 2;
  size_t columns = 2;
  firsov::Matrix testMatrix = firsov::part(testComposite);

  BOOST_CHECK_EQUAL(rows, testMatrix.getRows());
  BOOST_CHECK_EQUAL(columns, testMatrix.getColumns());

  firsov::Shape::shape_ptr testRectangle3 = std::make_shared<firsov::Rectangle>(20, 12, firsov::point_t{ 1, 3 });

  testMatrix.add(testRectangle3, rows);
  rows++;

  BOOST_CHECK_EQUAL(rows, testMatrix.getRows());
  BOOST_CHECK_EQUAL(columns, testMatrix.getColumns());

  firsov::Shape::shape_ptr testCircle2 = std::make_shared<firsov::Circle>(6, firsov::point_t{ -100, -40 });

  testMatrix.add(testCircle2, 0);
  columns++;

  BOOST_CHECK_EQUAL(rows, testMatrix.getRows());
  BOOST_CHECK_EQUAL(columns, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(testMatrixCreation)
{
  firsov::Shape::shape_ptr testCircle = std::make_shared<firsov::Circle>(3, firsov::point_t{ 0, 0 });
  firsov::Shape::shape_ptr testRectangle = std::make_shared<firsov::Rectangle>(2, 1, firsov::point_t{ 10, 10 });
  firsov::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle);
  size_t rows = 1;
  const size_t columns = 2;

  firsov::Matrix testMatrix = firsov::part(testComposite);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), rows);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), columns);

  firsov::Shape::shape_ptr testRectangle2 = std::make_shared<firsov::Rectangle>(5, 4, firsov::point_t{ 9, 8 });
  rows++;
  testComposite.add(testRectangle2);
  testMatrix = firsov::part(testComposite);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), rows);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), columns);
}

BOOST_AUTO_TEST_CASE(testOfInvalidParametrs)
{
  firsov::Shape::shape_ptr rectangle = std::make_shared<firsov::Rectangle>(6, 9, firsov::point_t{ 3, 4 });
  firsov::Shape::shape_ptr circle = std::make_shared<firsov::Circle>(4, firsov::point_t{ 5, 5 });
  firsov::CompositeShape compShape(rectangle);
  compShape.add(circle);
  firsov::Matrix matrix = part(compShape);
  BOOST_CHECK_THROW(matrix[9][2], std::out_of_range);
  BOOST_CHECK_THROW(matrix[-4][1], std::out_of_range);
  BOOST_CHECK_NO_THROW(matrix[1][1]);
}

BOOST_AUTO_TEST_SUITE_END();
