#include <boost/test/auto_unit_test.hpp>
#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testPartition)

BOOST_AUTO_TEST_CASE(testIntersection)
{
  firsov::Circle testCircle(4, { 3, 1 });
  firsov::Rectangle testRectangle(4, 4, { 8, 6 });

  BOOST_CHECK(firsov::isIntersected(testCircle.getFrameRect(),testRectangle.getFrameRect()));
}

BOOST_AUTO_TEST_CASE(partitionCorrectness)
{
  firsov::Shape::shape_ptr circlePtr1 = std::make_shared<firsov::Circle>(4, firsov::point_t { 3, 1 });
  firsov::Shape::shape_ptr rectanglePtr1 = std::make_shared<firsov::Rectangle>(6, 4, firsov::point_t { 9, 4 });
  firsov::Shape::shape_ptr circlePtr2 = std::make_shared<firsov::Circle>(2, firsov::point_t { 7, -3 });
  firsov::Shape::shape_ptr rectanglePtr2 = std::make_shared<firsov::Rectangle>(6, 4, firsov::point_t { 4, -4 });

  firsov::CompositeShape compShape(circlePtr1);
  compShape.add(rectanglePtr1);
  compShape.add(circlePtr2);
  compShape.add(rectanglePtr2);

  const firsov::Matrix testMatrix = firsov::part(compShape);

  const size_t rows = 3;
  const size_t columns = 2;

  BOOST_CHECK_EQUAL(testMatrix.getColumns(), columns);
  BOOST_CHECK_EQUAL(testMatrix.getRows(), rows);
  BOOST_CHECK(testMatrix[0][0] == circlePtr1);
  BOOST_CHECK(testMatrix[1][0] == rectanglePtr1);
  BOOST_CHECK(testMatrix[1][1] == circlePtr2);
  BOOST_CHECK(testMatrix[2][0] == rectanglePtr2);
}

BOOST_AUTO_TEST_SUITE_END()

