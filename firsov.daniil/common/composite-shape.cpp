#include <iostream>
#include <stdexcept>
#include <cmath>
#include "composite-shape.hpp"

firsov::CompositeShape::CompositeShape():
  count_(0),
  arrayShapes_()
{
}

firsov::CompositeShape::CompositeShape(const CompositeShape& source):
  count_(source.count_),
  arrayShapes_(std::make_unique<shape_ptr[]>(source.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    arrayShapes_[i] = source.arrayShapes_[i];
  }
}

firsov::CompositeShape::CompositeShape(CompositeShape&& source):
  count_(source.count_),
  arrayShapes_(std::move(source.arrayShapes_))

{
  source.count_ = 0;
}

firsov::CompositeShape::CompositeShape(const shape_ptr& shape):
  count_(1),
  arrayShapes_(std::make_unique<shape_ptr []>(1))
{
  arrayShapes_[0] = shape;
}

firsov::CompositeShape& firsov::CompositeShape::operator =(const CompositeShape& source)
{
  if (this != &source)
  {
    shape_array shapes(std::make_unique<shape_ptr[]>(count_));
    count_ = source.count_;
    for (size_t i = 0; i < count_; i++)
    {
      shapes[i] = source.arrayShapes_[i];
    }
    arrayShapes_.swap(shapes);
  }
  return *this;
}

firsov::CompositeShape &firsov::CompositeShape::operator =(CompositeShape &&source)
{
  if (&source != this)
  {
    count_ = source.count_;
    arrayShapes_ = std::move(source.arrayShapes_);
    source.arrayShapes_ = nullptr;
    source.count_ = 0;
  }
  return *this;
}

firsov::Shape::shape_ptr firsov::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }
  return arrayShapes_[index];
}

double firsov::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    area += arrayShapes_[i]->getArea();
  }
  return area;
}

firsov::rectangle_t firsov::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Warning: composite shape is empty");
  }
  rectangle_t tmpRectangle = arrayShapes_[0]->getFrameRect();
  double minX = tmpRectangle.pos.x - tmpRectangle.width / 2;
  double minY = tmpRectangle.pos.y - tmpRectangle.height / 2;
  double maxX = tmpRectangle.pos.x + tmpRectangle.width / 2;
  double maxY = tmpRectangle.pos.y + tmpRectangle.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    tmpRectangle = arrayShapes_[i]->getFrameRect();
    double tmp = tmpRectangle.pos.x - tmpRectangle.width / 2;

    minX = std::min(tmp, minX);
    tmp = tmpRectangle.pos.y - tmpRectangle.height / 2;
    minY = std::min(tmp, minY);
    tmp = tmpRectangle.pos.x + tmpRectangle.width / 2;
    maxX = std::max(tmp, maxX);
    tmp = tmpRectangle.pos.y + tmpRectangle.height / 2;
    maxY = std::max(tmp, maxY);
  }
  rectangle_t rect{maxX - minX, maxY - minY, point_t{(maxX + minX) / 2, (maxY + minY) / 2}};
  return rect;
}

void firsov::CompositeShape::move(const point_t& center)
{
  rectangle_t rectFrame = getFrameRect();
  double dx = center.x - rectFrame.pos.x;
  double dy = center.y - rectFrame.pos.y;

  move(dx, dy);
}

void firsov::CompositeShape::move(double dx, double dy)
{
  if (count_ == 0)
  {
    throw std::logic_error("Warning: composite shape is empty");
  }

  for (size_t i = 0; i < count_; i++)
  {
    arrayShapes_[i]->move(dx, dy);
  }
}

void firsov::CompositeShape::scale(double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("Input multiplier more than 0 ");
  }
  if (count_ == 0)
  {
    throw std::logic_error("Warning: composite shape is empty");
  }

  firsov::point_t rectFrame = getFrameRect().pos;
  for (size_t i = 0; i < count_; i++)
  {
    point_t shapeCenter = arrayShapes_[i]->getFrameRect().pos;
    double dx = (shapeCenter.x - rectFrame.x) * (multiplier - 1);
    double dy = (shapeCenter.y - rectFrame.y) * (multiplier - 1);
    arrayShapes_[i]->move(dx, dy);
    arrayShapes_[i]->scale(multiplier);
  }
}

void firsov::CompositeShape::rotate(double angle)
{
  if (count_ == 0) {
    throw std::logic_error("Composite shape is empty");
  }

  const double sinA = sin(angle * M_PI / 180);
  const double cosA = cos(angle * M_PI / 180);
  const point_t compCenter = getFrameRect().pos;

  for (size_t i = 0; i < count_; i++) {
    arrayShapes_[i]->rotate(angle);
    const point_t shapeCenter = arrayShapes_[i]->getFrameRect().pos;

    const double oldX = shapeCenter.x - compCenter.x;
    const double oldY = shapeCenter.y - compCenter.y;

    const double newX = oldX * cosA - oldY * sinA;
    const double newY = oldX * sinA + oldY * cosA;

    arrayShapes_[i]->move({newX + compCenter.x, newY + compCenter.y});
  }
}

void firsov::CompositeShape::add(shape_ptr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  shape_array shapes(std::make_unique<shape_ptr[]>(count_ + 1));
  for (size_t i = 0; i < count_; i++)
  {
    shapes [i] = arrayShapes_[i];
  }
  shapes [count_] = shape;
  count_++;
  arrayShapes_.swap(shapes);
}

void firsov::CompositeShape::remove(size_t index) {
  if ((index >= count_))
  {
    throw std::invalid_argument("Invalid index");
  }
  count_--;
  for (size_t i = index; i < count_; i++) {
    arrayShapes_[i] = arrayShapes_[i + 1];
  }
}

size_t firsov::CompositeShape::getSize() const
{
  return count_;
}

firsov::Shape::shape_array firsov::CompositeShape::getShapes() const
{
  shape_array shapes(std::make_unique<shape_ptr[]>(count_));

  for (size_t i = 0; i < count_; i++)
  {
    shapes[i] = arrayShapes_[i];
  }
  return shapes;
}

void firsov::CompositeShape::printShapeInfo() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  std::cout << "Composite Shape: " << std::endl;
  std::cout << "Area: " << getArea() << std::endl;
  rectangle_t frame = getFrameRect();
  std::cout << "Center: (" << frame.pos.x << " , " << frame.pos.y << ")" << std::endl;
  std::cout << "Frame width: " << frame.width << std::endl;
  std::cout << "Frame height: " << frame.height << std::endl;
  std::cout << "Count figures:" << count_ << std::endl;
  std::cout << "Elements info: " << std::endl;
  for (size_t i = 0; i < count_; i++)
  {
    arrayShapes_[i]->printShapeInfo();
    std::cout << std::endl;
  }
}


