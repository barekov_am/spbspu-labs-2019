#include <boost/test/auto_unit_test.hpp>
#include <memory>
#include <cmath>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(A3Test)

const double PRECISION = 0.001;

BOOST_AUTO_TEST_CASE(compositeShapeCopyConstructor)
{
  firsov::Shape:: shape_ptr testCircle = std::make_shared<firsov::Circle>(3, firsov::point_t{ 9, -4 });
  firsov::Shape:: shape_ptr testRectangle = std::make_shared<firsov::Rectangle>(7, 6, firsov::point_t{ 3, 4 });
  firsov::CompositeShape beforeCompShape(testCircle);

  beforeCompShape.add(testRectangle);
  firsov::CompositeShape copyCompShape(beforeCompShape);
  const firsov::rectangle_t frameRect = beforeCompShape.getFrameRect();
  const firsov::rectangle_t copyFrameRect = copyCompShape.getFrameRect();

  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, PRECISION);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, PRECISION);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(beforeCompShape.getArea(), copyCompShape.getArea(), PRECISION);
  BOOST_CHECK_EQUAL(beforeCompShape.getSize(), copyCompShape.getSize());
}

BOOST_AUTO_TEST_CASE(compositeShapeMoveConstructor)
{
  firsov::Shape:: shape_ptr testCircle = std::make_shared<firsov::Circle>(3, firsov::point_t{ 9, -4 });
  firsov::Shape:: shape_ptr testRectangle = std::make_shared<firsov::Rectangle>(7, 6, firsov::point_t{ 3, 4 });
  firsov::CompositeShape beforeCompShape(testRectangle);

  beforeCompShape.add(testCircle);
  const firsov::rectangle_t frameRect = beforeCompShape.getFrameRect();
  const double areaCompShape = beforeCompShape.getArea();
  const size_t countCompositeShape = beforeCompShape.getSize();

  firsov::CompositeShape moveCompShape(std::move(beforeCompShape));
  const firsov::rectangle_t moveFrameRect = moveCompShape.getFrameRect();

  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, PRECISION);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, PRECISION);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(areaCompShape, moveCompShape.getArea(), PRECISION);
  BOOST_CHECK_EQUAL(countCompositeShape, moveCompShape.getSize());
  BOOST_CHECK_CLOSE(beforeCompShape.getArea(), 0, PRECISION);
  BOOST_CHECK_EQUAL(beforeCompShape.getSize(), 0);
}

BOOST_AUTO_TEST_CASE(testMoveOperator)
{
  firsov::Shape:: shape_ptr testCircle = std::make_shared<firsov::Circle>(3, firsov::point_t{ 9, -4 });
  firsov::Shape:: shape_ptr testRectangle = std::make_shared<firsov::Rectangle>(7, 6, firsov::point_t{ 3, 4 });
  firsov::Rectangle rect(7, 6, { 3, 4 });
  firsov::Circle circ(3, { 9, -4 });
  firsov::CompositeShape compShape;
  compShape.add(std::make_shared<firsov::Circle>(circ));
  compShape.add(std::make_shared<firsov::Rectangle>(rect));

  firsov::CompositeShape testCompShape;
  const double areaBefore = compShape.getArea();
  const firsov::rectangle_t testRect = compShape.getFrameRect();

  testCompShape = std::move(compShape);

  BOOST_CHECK_CLOSE(areaBefore, testCompShape.getArea(), PRECISION);
  BOOST_CHECK_CLOSE(testRect.pos.x, testCompShape.getFrameRect().pos.x, PRECISION);
  BOOST_CHECK_CLOSE(testRect.pos.y, testCompShape.getFrameRect().pos.y, PRECISION);
  BOOST_CHECK_CLOSE(testRect.height, testCompShape.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(testRect.width, testCompShape.getFrameRect().width, PRECISION);
}

BOOST_AUTO_TEST_CASE(invariabilityAfterMovingCompositeShapeByIncrement)
{
  firsov::Shape:: shape_ptr testCircle = std::make_shared<firsov::Circle>(3, firsov::point_t{ 9, -4 });
  firsov::Shape:: shape_ptr testRectangle = std::make_shared<firsov::Rectangle>(7, 6, firsov::point_t{ 3, 4 });
  firsov::CompositeShape testCompShape;

  testCompShape.add(testCircle);
  testCompShape.add(testRectangle);

  const firsov::rectangle_t frameBefore = testCompShape.getFrameRect();
  const double areaBefore = testCompShape.getArea();

  testCompShape.move(-2, 7);

  const firsov::rectangle_t frameAfter = testCompShape.getFrameRect();
  const double areaAfter = testCompShape.getArea();

  BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);
  BOOST_CHECK_CLOSE(frameAfter.width, frameBefore.width, PRECISION);
  BOOST_CHECK_CLOSE(frameAfter.height, frameBefore.height, PRECISION);
}

BOOST_AUTO_TEST_CASE(checkIncorrectParameters)
{
  firsov::Shape:: shape_ptr testCircle = std::make_shared<firsov::Circle>(3, firsov::point_t{ 2, -4 });
  firsov::Shape:: shape_ptr testRectangle = std::make_shared<firsov::Rectangle>(5, 10, firsov::point_t{ 6, 4 });
  firsov::CompositeShape compShape;

  compShape.add(testRectangle);
  compShape.add(testCircle);

  BOOST_CHECK_THROW(compShape.scale(-0.4), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.remove(2), std::logic_error);
  BOOST_CHECK_THROW(compShape.remove(-1), std::logic_error);

}

BOOST_AUTO_TEST_CASE(invariabilityAfterMovingCompositeShapeToPoint)
{

  firsov::Shape:: shape_ptr testCircle = std::make_shared<firsov::Circle>(3, firsov::point_t{ 13.3, 7 });
  firsov::Shape:: shape_ptr testRectangle = std::make_shared<firsov::Rectangle>(3, 7, firsov::point_t{ 5, 4 });
  firsov::CompositeShape testCompShape;

  testCompShape.add(testCircle);
  testCompShape.add(testRectangle);

  const firsov::rectangle_t frameRectBefore = testCompShape.getFrameRect();
  const double areaBefore = testCompShape.getArea();

  testCompShape.move({ 10, -3 });

  const firsov::rectangle_t frameAfter = testCompShape.getFrameRect();
  const double areaAfter = testCompShape.getArea();

  BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);
  BOOST_CHECK_CLOSE(frameAfter.width, frameRectBefore.width, PRECISION);
  BOOST_CHECK_CLOSE(frameAfter.height, frameRectBefore.height, PRECISION);
}

BOOST_AUTO_TEST_CASE(quadraticCompositeShapeScaleChange)
{
  firsov::Shape:: shape_ptr testCircle = std::make_shared<firsov::Circle>(3, firsov::point_t{ 9, -4 });
  firsov::Shape:: shape_ptr testRectangle = std::make_shared<firsov::Rectangle>(7, 6, firsov::point_t{ 3, 4 });
  firsov::CompositeShape testCompShape(testRectangle);
  testCompShape.add(testCircle);

  const double areaBeforeScale = testCompShape.getArea();
  const unsigned int multiplier  = 2;
  const firsov::point_t centre = testCompShape.getFrameRect().pos;
  testCompShape.scale(multiplier);
  const firsov::point_t compCentre = testCompShape.getFrameRect().pos;
  const firsov::point_t ShapeCentre1 = testCompShape[0]->getFrameRect().pos;
  const firsov::point_t ShapeCentre2 = testCompShape[1]->getFrameRect().pos;
  double areaAfterScale = testCompShape.getArea();

  BOOST_CHECK_CLOSE(areaBeforeScale * multiplier  * multiplier , areaAfterScale, PRECISION);
  BOOST_CHECK_CLOSE(centre.x, compCentre.x, PRECISION);
  BOOST_CHECK_CLOSE(centre.y, compCentre.y, PRECISION);
  BOOST_CHECK_CLOSE(ShapeCentre1.x, 3 + (3 - compCentre.x) * (multiplier - 1), PRECISION);
  BOOST_CHECK_CLOSE(ShapeCentre1.y, 4 + (4 - compCentre.y) * (multiplier - 1), PRECISION);
  BOOST_CHECK_CLOSE(ShapeCentre2.x, 9 + (9 - compCentre.x) * (multiplier - 1), PRECISION);
  BOOST_CHECK_CLOSE(ShapeCentre2.y, -4 + (-4 - compCentre.y) * (multiplier - 1), PRECISION);
}

BOOST_AUTO_TEST_CASE(testOfCorrectRotation)
{
  firsov::Shape::shape_ptr testCircle = std::make_shared<firsov::Circle>(3, firsov::point_t { 3, 9 });
  firsov::Shape::shape_ptr testRectangle = std::make_shared<firsov::Rectangle>(6, 6, firsov::point_t { 9, 3 });
  firsov::CompositeShape testCompositeShape;
  testCompositeShape.add(testCircle);
  testCompositeShape.add(testRectangle);

  const firsov::rectangle_t tmpFrameBefore = testCompositeShape.getFrameRect();
  const double tmpAreaBefore = testCompositeShape.getArea();

  testCompositeShape.rotate(90);
  firsov::rectangle_t tmpFrameAfter = testCompositeShape.getFrameRect();
  double tmpAreaAfter = testCompositeShape.getArea();

  BOOST_CHECK_CLOSE(tmpFrameBefore.width, tmpFrameAfter.height, PRECISION);
  BOOST_CHECK_CLOSE(tmpFrameBefore.height, tmpFrameAfter.width, PRECISION);
  BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, PRECISION);

  testCompositeShape.rotate(270);
  tmpFrameAfter = testCompositeShape.getFrameRect();
  tmpAreaAfter = testCompositeShape.getArea();

  BOOST_CHECK_CLOSE(tmpFrameBefore.height, tmpFrameAfter.height, PRECISION);
  BOOST_CHECK_CLOSE(tmpFrameBefore.width, tmpFrameAfter.width, PRECISION);
  BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, PRECISION);

  testCompositeShape.rotate(45);
  tmpFrameAfter = testCompositeShape.getFrameRect();
  tmpAreaAfter = testCompositeShape.getArea();
  const double tmpWidth = 9 * sqrt(2) + 3;
  const double tmpHeight = 6 * sqrt(2);

  BOOST_CHECK_CLOSE(tmpWidth, tmpFrameAfter.width, PRECISION);
  BOOST_CHECK_CLOSE(tmpHeight, tmpFrameAfter.height, PRECISION);
  BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, PRECISION);

  testCompositeShape.rotate(-45);

  const firsov::rectangle_t tmpRectBefore = testRectangle->getFrameRect();
  testRectangle->rotate(45);
  const firsov::rectangle_t tmpRectAfter = testRectangle->getFrameRect();
  const double dWidth = (tmpRectAfter.width - tmpRectBefore.width) / 2;
  const double dHeight = (tmpRectAfter.height - tmpRectBefore.height) / 2;
  tmpFrameAfter = testCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(tmpFrameBefore.width + dWidth, tmpFrameAfter.width, PRECISION);
  BOOST_CHECK_CLOSE(tmpFrameBefore.height + dHeight, tmpFrameAfter.height, PRECISION);
  BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, PRECISION);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleMultiplierMoreThanOne)
{
  firsov::Shape:: shape_ptr testCircle = std::make_shared<firsov::Circle>(3, firsov::point_t{ 9, -4 });
  firsov::Shape:: shape_ptr testRectangle = std::make_shared<firsov::Rectangle>(7, 6, firsov::point_t{ 3, 4 });
  firsov::CompositeShape compShape(testRectangle);
  compShape.add(testCircle);
  const firsov::rectangle_t frameBeforeScale = compShape.getFrameRect();
  const int multiplier = 2;

  compShape.scale(multiplier);
  firsov::rectangle_t frameAfterScale = compShape.getFrameRect();

  BOOST_CHECK_CLOSE(frameBeforeScale.height * multiplier, frameAfterScale.height, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeScale.width * multiplier, frameAfterScale.width, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.x, frameAfterScale.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.y, frameAfterScale.pos.y, PRECISION);
  BOOST_CHECK(frameBeforeScale.height < frameAfterScale.height);
  BOOST_CHECK(frameBeforeScale.width < frameAfterScale.width);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleMultiplierLessThanOne)
{
  firsov::Shape:: shape_ptr testCircle = std::make_shared<firsov::Circle>(3, firsov::point_t{ 9, -4 });
  firsov::Shape:: shape_ptr testRectangle = std::make_shared<firsov::Rectangle>(7, 6, firsov::point_t{ 3, 4 });
  firsov::CompositeShape compShape(testRectangle);
  compShape.add(testCircle);
  const firsov::rectangle_t frameBeforeScale = compShape.getFrameRect();
  const double multiplier = 0.5;

  compShape.scale(multiplier);
  firsov::rectangle_t frameAfterScale = compShape.getFrameRect();

  BOOST_CHECK_CLOSE(frameBeforeScale.height * multiplier, frameAfterScale.height, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeScale.width * multiplier, frameAfterScale.width, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.x, frameAfterScale.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.y, frameAfterScale.pos.y, PRECISION);
  BOOST_CHECK(frameBeforeScale.height > frameAfterScale.height);
  BOOST_CHECK(frameBeforeScale.width > frameAfterScale.width);
}
BOOST_AUTO_TEST_CASE(consistancyInRotation)
{
  firsov::Shape::shape_ptr circ = std::make_shared<firsov::Circle>(3, firsov::point_t { 1, 2 });
  firsov::Shape::shape_ptr rect = std::make_shared<firsov::Rectangle>(3, 4, firsov::point_t { 4, 5 });
  firsov::CompositeShape testComposition;

  testComposition.add(circ);
  testComposition.add(rect);

  const double areaBefore = testComposition.getArea();
  const firsov::rectangle_t frameRectBefore = testComposition.getFrameRect();

  double angle = 360;
  testComposition.rotate(angle);

  double areaAfter = testComposition.getArea();
  firsov::rectangle_t frameRectAfter = testComposition.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);

  angle = 90;
  testComposition.rotate(angle);

  areaAfter = testComposition.getArea();
  frameRectAfter = testComposition.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);

  angle = 0;
  testComposition.rotate(angle);

  areaAfter = testComposition.getArea();
  frameRectAfter = testComposition.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);

  angle = -450;
  testComposition.rotate(angle);

  areaAfter = testComposition.getArea();
  frameRectAfter = testComposition.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);

}

BOOST_AUTO_TEST_CASE(checkCompositeShapeAfterAdd)
{
  firsov::Shape:: shape_ptr testCircle = std::make_shared<firsov::Circle>(6, firsov::point_t{ 13.3, 7 });
  firsov::Shape:: shape_ptr testRectangle = std::make_shared<firsov::Rectangle>(7, 6, firsov::point_t{ 3, 4 });
  firsov::CompositeShape compShape;
  compShape.add(testCircle);
  compShape.add(testRectangle);
  const double areaBeforeAdd = compShape.getArea();
  const double circleArea = testCircle->getArea();

  compShape.add(testCircle);
  double areaAfterAdd = compShape.getArea();

  BOOST_CHECK_CLOSE(areaBeforeAdd + circleArea, areaAfterAdd, PRECISION);
}

BOOST_AUTO_TEST_CASE(checkCompositeShapeAfterDelete)
{
  firsov::Shape:: shape_ptr testCircle = std::make_shared<firsov::Circle>(3, firsov::point_t{ 9, -4 });
  firsov::Shape:: shape_ptr testRectangle = std::make_shared<firsov::Rectangle>(7, 6, firsov::point_t{ 3, 4 });
  firsov::CompositeShape compShape;
  compShape.add(testRectangle);
  compShape.add(testCircle);

  const double areaAfterAdd = compShape.getArea();
  const double circleArea = testCircle->getArea();

  compShape.remove(1);
  double areaAfterDelete = compShape.getArea();

  BOOST_CHECK_CLOSE(areaAfterAdd - circleArea, areaAfterDelete, PRECISION);
}

BOOST_AUTO_TEST_SUITE_END()
