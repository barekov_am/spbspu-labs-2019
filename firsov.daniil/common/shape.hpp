#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <memory>
#include "base-types.hpp"

namespace firsov
{
  class Shape {
  public:
    virtual ~Shape() = default;
    using shape_ptr = std::shared_ptr<Shape>;
    using shape_array = std::unique_ptr<shape_ptr[]>;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t& point) = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void printShapeInfo() const = 0;
    virtual void scale(double multiplier) = 0;
    virtual void rotate(double angle) = 0;
  };
}
#endif
