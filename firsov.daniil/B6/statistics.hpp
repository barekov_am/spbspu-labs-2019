#ifndef B6_STATISTICS_HPP
#define B6_STATISTICS_HPP

class Statistics
{
public:
  Statistics();

  void operator()(const int& number);
  void printStatistics();

private:
  int max_;
  int min_;
  double mean_;
  int positiveCount_;
  int negativeCount_;
  long long int sumEven_;
  long long int sumOdd_;
  bool equalFirstLast_;

  bool empty_;
  int first_;
  int total_;
};

#endif
