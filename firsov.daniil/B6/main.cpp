#include <iostream>

void task1();

int main()
{
  try
  {
    task1();
  }
  catch (const std::exception& ex)
  {
    std::cerr << ex.what();

    return 1;
  }

  return 0;
}
