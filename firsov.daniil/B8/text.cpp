#include <string>
#include <iostream>
#include <algorithm>
#include "text.hpp"

Text::Text(const unsigned int width) :
  width_(width) {
}

void Text::read(std::istream& input)
{
  std::string line;

  while (std::cin) {
    char ch = std::cin.get();

    if (std::isspace(ch)) {
      ignoreSpaces(input);
    } else if (std::isalpha(ch)) {
      text_.push_back(readWord(ch, input));
    } else if ((std::isdigit(ch)) ||
        ((ch == punctMark["PLUS"] || ch == punctMark["HYPHEN"]) && std::isdigit(std::cin.peek()))) {
      text_.push_back(readNumber(ch, input));
    } else if (std::ispunct(ch)) {
      if (text_.empty()) {
        throw std::invalid_argument("Text should not begin with punctuation. \n");
      }
      if (isPunct(*std::prev(text_.end())) &&
          ((ch == punctMark["HYPHEN"] && (*std::prev(text_.end()) != ",")) || ch != punctMark["HYPHEN"])) {
        throw std::invalid_argument("More than one punctuation. \n");
      }
      if (ch == punctMark["HYPHEN"]) {
        text_.push_back(readDash(ch, input));
      } else {
        text_.push_back(readPunctuation(ch));
      }
    }
  }
}

void Text::format(std::ostream& out)
{
  if (text_.empty()) {
    return;
  }
  if (text_.size() == 1) {
    std::for_each(text_.begin(), text_.end(), [&](auto item) { out << item << std::endl; });
    return;
  }

  std::string prev;
  std::string current;
  std::string line;
  unsigned int space;

  std::for_each(text_.begin(), text_.end(), [&](auto& item) {
    {
      current = item;
      space = calcSpace(current);

      if (line.size() + current.size() + space > width_) {
        if (isPunct(current)) {
          std::size_t prevSpace = calcSpace(prev);
          line.erase(line.size() - prev.size() - prevSpace, prev.size() + prevSpace);
          std::cout << line << std::endl;

          line.clear();
          line += prev;

        } else {
          std::cout << line << std::endl;
          line.clear();
        }
      }

      if (!line.empty() && space == 1) {
        line.push_back(' ');
      }

      line += current;
      prev = current;
      current.clear();
    }
  });
  std::cout << line << std::endl;
}


