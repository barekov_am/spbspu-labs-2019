#include <iostream>
#include <algorithm>
#include "functions.hpp"

void ignoreSpaces(std::istream& input)
{
  while (input.peek() == ' ') {
    input.get();
  }
}

std::string readWord(char symbol, std::istream& input)
{
  std::string line;
  line.push_back(symbol);

  while ((input.peek() == '-') || std::isalpha(input.peek())) {
    char ch = input.get();
    if (ch == '-' && input.peek() == punctMark["HYPHEN"]) {
      throw std::invalid_argument("More than one hyphen. \n");
    }
    line += ch;
  }

  if (line.size() > WORD_MAX_SIZE) {
    throw std::invalid_argument("String should be less or equal 20. \n");
  }
  return line;
}

std::string readNumber(char symbol, std::istream& input)
{
  std::string line;
  line += symbol;
  unsigned int dots = 0;

  while ((input.peek() == punctMark["DOT"]) || std::isdigit(input.peek())) {
    symbol = input.get();
    line += symbol;

    if (symbol == punctMark["DOT"]) {
      dots++;
      if (dots > 1) {
        throw std::invalid_argument("More than one dot. \n");
      }
    }
  }

  if (line.size() > WORD_MAX_SIZE) {
    throw std::invalid_argument("length of num should be less or equal 20. \n");
  }
  return line;
}

std::string readDash(char symbol, std::istream& input)
{
  std::string line;
  line += symbol;

  while (input.peek() == punctMark["HYPHEN"]) {
    symbol = input.get();
    line += symbol;
  }

  if (line.size() != 3) {
    throw std::invalid_argument("Dash consist with three hyphen. \n");
  }
  return line;
}


std::string readPunctuation(char symbol)
{
  std::string line;
  line += symbol;
  return line;
}

bool isPunct(const std::string& str)
{
  return std::all_of(str.begin(), str.end(), ispunct);
}

unsigned int calcSpace(const std::string& line)
{
  return !(isPunct(line) && line != "-" && line != "---") ? 1 : 0;
}
