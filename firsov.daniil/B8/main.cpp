#include <iostream>
#include <cstring>
#include "text.hpp"

int main(int argc, char *argv[])
{
  try {
    const unsigned int MINWIDTH = 25;
    unsigned int width = 40;
    switch (argc) {
    case 3:
      if (strcmp(argv[1], "--line-width") == 0) {
        width = std::stoi(argv[2]);
        if (width < MINWIDTH) {
          std::cerr << "Invalid width. \n";
          return 1;
        }
      }
        break;
    case 2:
      std::cerr << "Invalid number of arguments. \n";
        return 1;
    case 1:
      break;
    default:
      throw std::invalid_argument("Expected 0 arguments or 1 argument in \"--line-width\" \n");
    }

    Text text(width);
    text.read(std::cin);
    text.format(std::cout);
  }
  catch (const std::exception& ex) {
    std::cerr << ex.what();
    return 1;
  }
}

