#ifndef B8_FUNCTIONS_HPP
#define B8_FUNCTIONS_HPP

#include <istream>
#include <map>

const unsigned int WORD_MAX_SIZE = 20;

static std::map<std::string, char> punctMark = {
  {"HYPHEN", '-'},
  {"PLUS",   '+'},
  {"DOT",    '.'},
  {"COMMA",  ','}
};

void ignoreSpaces(std::istream& input);

std::string readWord(char symbol, std::istream& input);

std::string readNumber(char symbol, std::istream& input);

std::string readDash(char symbol, std::istream& input);

std::string readPunctuation(char symbol);

bool isPunct(const std::string& str);

unsigned int calcSpace(const std::string& line);


#endif
