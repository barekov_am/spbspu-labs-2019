#ifndef B8_TEXT_HPP
#define B8_TEXT_HPP

#include <vector>
#include "functions.hpp"

class Text {
public:
  Text(const unsigned int width_);

  void read(std::istream& input);

  void format(std::ostream& out);

  ~Text() = default;

private:
  const unsigned int width_;

  std::vector<std::string> text_;
};

#endif
