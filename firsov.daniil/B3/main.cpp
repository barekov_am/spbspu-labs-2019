#include <iostream>

void task1();
void task2();

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Invalid number of arguments";
      return 1;
    }

    char *ptr = nullptr;
    int task = std::strtol(argv[1], &ptr, 10);

    if (*ptr)
    {
      std::cerr << "Invalid argument of task";
      return 1;
    }

    switch (task)
    {
      case 1:
      {
        task1();
        break;
      }

      case 2:
      {
        task2();
        break;
      }

      default:
      {
        std::cerr << "Invalid number of task";
        return 1;
      }
    }
  }

  catch (const std::ios_base::failure& ex)
  {
    std::cerr << ex.what();
    return 2;
  }
  catch (const std::exception& ex)
  {
    std::cerr << ex.what();
    return 1;
  }

  return 0;
}

