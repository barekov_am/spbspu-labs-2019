#ifndef B3_PHONEBOOKMANAGER_HPP
#define B3_PHONEBOOKMANAGER_HPP


#include <map>
#include "phoneBook.hpp"

class PhoneBookManager {

public:
  enum InsertPosition {
    BEFORE,
    AFTER
  };

  enum MovePosition {
    FIRST,
    LAST
  };

  PhoneBookManager();

  void add(const PhoneBook::record_t &);
  void store(const std::string &, const std::string &);

  void insert(const std::string& bookmark, const PhoneBook::record_t& record,
              InsertPosition& position);

  void erase(const std::string &);

  void show(const std::string &);

  void move(const std::string &, int);
  void move(const std::string &, MovePosition);

private:

  PhoneBook records_;
  std::map<std::string, PhoneBook::iter> bookmarks_;

  std::map<std::string, PhoneBook::iter>::iterator getBookmarkIterator(const std::string &);

};

#endif

