#include "phoneBook.hpp"

PhoneBook::iter PhoneBook::begin() {
  return recordsList_.begin();
}

PhoneBook::iter PhoneBook::end() {
  return recordsList_.end();
}

PhoneBook::iter PhoneBook::next(iter pos) {
  if (std::next(pos) == recordsList_.end()) {
    return pos;
  } else {
    return ++pos;
  }
}

PhoneBook::iter PhoneBook::prev(iter pos) {
  if (pos == recordsList_.begin()) {
    return pos;
  } else {
    return --pos;
  }
}

PhoneBook::iter PhoneBook::move(iter pos, int steps) {
  if (steps >= 0) {
    while (std::next(pos) != recordsList_.end() && (steps > 0)) {
      pos = next(pos);
      --steps;
    }
  } else {
    while (pos != recordsList_.begin() && (steps < 0)) {
      pos = prev(pos);
      ++steps;
    }
  }

  return pos;
}

void PhoneBook::pushBack(const record_t & record) {
  recordsList_.push_back(record);
}

PhoneBook::iter PhoneBook::insert(iter pos, const record_t & record) {
  return recordsList_.insert(pos, record);
}

PhoneBook::iter PhoneBook::erase(iter pos) {
  return recordsList_.erase(pos);
}

bool PhoneBook::empty() const {
  return recordsList_.empty();
}

void PhoneBook::view(iter it) const {
  std::cout << it->number << " " << it->name << std::endl;
}
