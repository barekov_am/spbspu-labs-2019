#include <sstream>
#include <iostream>
#include <algorithm>
#include <iterator>
#include "commands.hpp"
#include "phoneBookManager.hpp"
#include "factorialContainer.hpp"

void task1()
{
  PhoneBookManager PhoneBook;
  std::string line;

  while (std::getline(std::cin, line))
  {
    if (!std::cin && !std::cin.eof()) {
      throw std::ios_base::failure("Failed input");
    }

    std::istringstream input(line);
    execute(PhoneBook, input);
  }
}

void task2()
{
  FactorialContainer containerFactorial;

  std::copy(containerFactorial.begin(), containerFactorial.end(),
            std::ostream_iterator<unsigned long long>(std::cout, " "));
  std::cout << '\n';

  std::reverse_copy(containerFactorial.begin(), containerFactorial.end(),
                    std::ostream_iterator<unsigned long long>(std::cout, " "));
  std::cout << "\n";
}


