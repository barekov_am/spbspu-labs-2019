#include <algorithm>
#include <iostream>
#include <cctype>
#include <functional>
#include "commands.hpp"

void ignoreSpaces(std::istream& input)
{
  while (input.peek() == ' ')
  {
    input.get();
  }
}

std::string readBookmark(std::istream& input)
{
  ignoreSpaces(input);
  std::string bookmark;
  input >> bookmark;

  auto iter = std::find_if(bookmark.begin(), bookmark.end(),
                           [&](auto symbol) { return (!std::isalnum(symbol) && symbol != '-'); });

  if (iter != bookmark.end()) {
    return "";
  }

  return bookmark;
}

std::string readNumber(std::istream& input)
{
  ignoreSpaces(input);
  std::string number;
  input >> number;

  auto iter = std::find_if(number.begin(), number.end(),
                           [&](auto digit) { return !isdigit(digit); });

  if (iter != number.end()) {
    return "";
  }

  return number;
}

std::string readName(std::istream& input)
{
  ignoreSpaces(input);

  if (input.peek() != '"') {
    return "";
  }

  input.get();
  std::string name;
  std::string tmp;

  while (std::getline(input, tmp, '"'))
  {
    name += tmp;

    if (name.back() != '\\') {
      break;
    }

    name.pop_back();
    name.push_back('"');
  }

  if (!input) {
    return "";
  }
  return name;
}

void execute(PhoneBookManager& phoneBook, std::istream& input)
{
  using executeComand = std::function<void(PhoneBookManager&, std::istream&)>;

  std::map<const std::string, executeComand> commands =
    {
      {"add",    &executeAdd},
      {"store",  &executeStore},
      {"insert", &executeInsert},
      {"delete", &executeDelete},
      {"show",   &executeShow},
      {"move",   &executeMove}
    };

  std::string commandName;
  input >> commandName;

  auto check = [&](const std::pair<const std::string, executeComand>& pair) { return pair.first == commandName; };

  auto command = std::find_if(std::begin(commands), std::end(commands), check);

  if (command != std::end(commands)) {
    command->second(phoneBook, input);
  } else {
    std::cout << "<INVALID COMMAND>\n";
  }
}

void executeAdd(PhoneBookManager& phoneBook, std::istream& input)
{
  std::string number = readNumber(input);
  std::string name = readName(input);

  std::string data;
  std::getline(input >> std::ws, data);

  if (name.empty() || number.empty() || !data.empty()) {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  phoneBook.add({number, name});
}

void executeStore(PhoneBookManager& phoneBook, std::istream& input)
{
  std::string bookmark = readBookmark(input);
  std::string newBookmark = readBookmark(input);

  std::string data;
  std::getline(input >> std::ws, data);

  if (bookmark.empty() || newBookmark.empty() || !data.empty()) {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  phoneBook.store(bookmark, newBookmark);
}

void executeInsert(PhoneBookManager& phoneBook, std::istream& input)
{
  std::map<const std::string, PhoneBookManager::InsertPosition> movePos =
    {
      {"after",  PhoneBookManager::InsertPosition::AFTER},
      {"before", PhoneBookManager::InsertPosition::BEFORE},
    };

  ignoreSpaces(input);
  std::string pos;
  input >> pos;
  std::string bookmark = readBookmark(input);
  std::string number = readNumber(input);
  std::string name = readName(input);

  std::string data;
  std::getline(input >> std::ws, data);

  if (pos.empty() || bookmark.empty() || number.empty() || name.empty() || !data.empty()) {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  auto check = [&](const std::pair<const std::string,
    PhoneBookManager::InsertPosition>& pair) { return pair.first == pos; };
  auto command = std::find_if(std::begin(movePos), std::end(movePos), check);

  if (command == std::end(movePos)) {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  phoneBook.insert(bookmark, {number, name}, command->second);
}

void executeDelete(PhoneBookManager& phoneBook, std::istream& input) 
{
  std::string bookmark = readBookmark(input);
  std::string data;
  std::getline(input >> std::ws, data);

  if (bookmark.empty() || !data.empty()) {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  phoneBook.erase(bookmark);
}

void executeShow(PhoneBookManager& phoneBook, std::istream& input) 
{
  std::string bookmark = readBookmark(input);
  std::string data;
  std::getline(input >> std::ws, data);

  if (bookmark.empty() || !data.empty()) {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  phoneBook.show(bookmark);
}

void executeMove(PhoneBookManager& phoneBook, std::istream& input) 
{
  std::map<const std::string, PhoneBookManager::MovePosition> movePos =
    {
      {"first", PhoneBookManager::MovePosition::FIRST},
      {"last",  PhoneBookManager::MovePosition::LAST},
    };

  std::string bookmark = readBookmark(input);
  ignoreSpaces(input);
  std::string pos;
  input >> pos;
  std::string data;
  std::getline(input >> std::ws, data);

  if (bookmark.empty() || pos.empty() || !data.empty()) {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  auto check = [&](const std::pair<const std::string, PhoneBookManager::MovePosition>& pair) {
    return pair.first == pos;
  };

  auto command = std::find_if(std::begin(movePos), std::end(movePos), check);

  if (command == std::end(movePos)) {
    int multiplier = 1;
    if (*pos.begin() == '-') {
      multiplier = -1;
      pos.erase(pos.begin());
    } else if (*pos.begin() == '+') {
      pos.erase(pos.begin());
    }

    char *ptr = nullptr;
    int step = std::strtol(pos.c_str(), &ptr, 10);

    if (*ptr) {
      std::cout << "<INVALID STEP>\n";
      return;
    }

    phoneBook.move(bookmark, step * multiplier);
    return;
  }

  phoneBook.move(bookmark, command->second);
}
