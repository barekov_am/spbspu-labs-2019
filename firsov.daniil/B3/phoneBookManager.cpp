#include <algorithm>
#include <iostream>

#include "phoneBookManager.hpp"

PhoneBookManager::PhoneBookManager() {
  bookmarks_["current"] = records_.begin();
}

void PhoneBookManager::add(const PhoneBook::record_t& rec) {

  records_.pushBack(rec);

  if (std::next(records_.begin()) == records_.end()) {
    bookmarks_["current"] = records_.begin();
  }

}

void PhoneBookManager::store(const std::string& bookmark, const std::string& newBookmark) {

  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end()) {
    bookmarks_.emplace(newBookmark, iter->second);
  }

}

void PhoneBookManager::insert(const std::string& bookmark, const PhoneBook::record_t& rec,
                              InsertPosition& position) {
  auto iter = bookmarks_.find(bookmark);

  if (iter == bookmarks_.end()) {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  if (iter->second == records_.end()) {
    add(rec);
    return;
  }

  if (position == InsertPosition::BEFORE) {
    iter->second = std::next(records_.insert(iter->second, rec));
  } else if (position == InsertPosition::AFTER) {
    if (std::next(iter->second) == records_.end()) {
      records_.pushBack(rec);
      return;
    }
    iter->second = std::prev(records_.insert(std::next(iter->second), rec));
  }
}


void PhoneBookManager::erase(const std::string& bookmark) {

  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end()) {
    auto eraseIter = iter->second;

    auto find = [&](auto& it) {
      if (it.second == eraseIter) {

        if (std::next(it.second) == records_.end()) {
          it.second = records_.prev(eraseIter);
        } else {
          it.second = records_.next(eraseIter);
        }

      }
    };

    std::for_each(bookmarks_.begin(), bookmarks_.end(), find);

    records_.erase(eraseIter);

  }

}

void PhoneBookManager::show(const std::string& bookmark) {

  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end()) {

    if (records_.empty()) {
      std::cout << "<EMPTY>" << std::endl;
      return;
    }

    records_.view(iter->second);

  }

}

void PhoneBookManager::move(const std::string& bookmark, int n) {

  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end()) {
    iter->second = records_.move(iter->second, n);
  }

}

void PhoneBookManager::move(const std::string& bookmark, MovePosition pos) {

  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end()) {

    if (pos == MovePosition::FIRST) {
      iter->second = records_.begin();
    }

    if (pos == MovePosition::LAST) {
      iter->second = records_.prev(records_.end());
    }

  }

}

std::map<std::string, PhoneBook::iter>::iterator PhoneBookManager::getBookmarkIterator(const std::string& bookmark) {

  auto iter = bookmarks_.find(bookmark);

  if (iter != bookmarks_.end()) {
    return iter;
  } else {
    std::cout << "<INVALID BOOKMARK>" << std::endl;
    return bookmarks_.end();
  }

}
