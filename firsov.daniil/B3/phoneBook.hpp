#ifndef B3_PHONEBOOK_HPP
#define B3_PHONEBOOK_HPP

#include <iostream>
#include <list>

class PhoneBook
{
public:

  struct record_t {
    std::string number;
    std::string name;
  };

  using iter = std::list<record_t>::iterator;

  iter begin();
  iter end();

  iter next(iter pos);
  iter prev(iter pos);

  iter move(iter pos, int steps);
  void pushBack(const record_t& record);

  iter insert(iter pos, const record_t& record);
  iter erase(iter pos);
  bool empty() const;

  void view(iter) const;

private:

  std::list<record_t> recordsList_;
};

#endif
