#include <iostream>
#include <algorithm>
#include <string>
#include "shape.hpp"
#include "functions.hpp"

const std::size_t TRIANGLE_VERTICES = 3;
const std::size_t RECTANGLE_VERTICES = 4;
const std::size_t PENTAGON_VERTICES = 5;

void task2()
{
  std::vector<Shape> containerOfShapes;
  std::vector<Point_t> points;

  std::string line;

  readShape(containerOfShapes, line);

  std::size_t countOfVertices = 0;
  std::size_t countOfTriangles = 0;
  std::size_t countOfSquares = 0;
  std::size_t countOfRectangle = 0;

  std::for_each(containerOfShapes.begin(), containerOfShapes.end(), [&](const Shape& shape) {
    countOfVertices += shape.size();
    if (shape.size() == TRIANGLE_VERTICES) {
      ++countOfTriangles;
    } else if (shape.size() == RECTANGLE_VERTICES) {
      if (isRectangle(shape)) {
        ++countOfRectangle;
        if (isSquare(shape)) {
          ++countOfSquares;
        }
      }
    }
  });

  auto tmp = remove_if(containerOfShapes.begin(), containerOfShapes.end(),
                       [](const Shape& shape) { return (shape.size() == PENTAGON_VERTICES); });

  containerOfShapes.erase(tmp, containerOfShapes.end());

  for (auto i = containerOfShapes.begin(); i != containerOfShapes.end(); i++) {
    points.push_back((*i).front());
  }

  std::sort(containerOfShapes.begin(), containerOfShapes.end(), isLess);

  std::cout << "Vertices: " << countOfVertices << std::endl;
  std::cout << "Triangles: " << countOfTriangles << std::endl;
  std::cout << "Squares: " << countOfSquares << std::endl;
  std::cout << "Rectangles: " << countOfRectangle << std::endl;

  std::cout << "Points: ";
  for (auto& i : points) {
    std::cout << '(' << i.x << ';' << i.y << ") ";
  }
  std::cout << std::endl;

  std::cout << "Shapes: " << std::endl;
  for (const auto& shape : containerOfShapes) {
    std::cout << shape.size();
    for (const auto point : shape) {
      std::cout << " (" << point.x << ';' << point.y << ") ";
    }
    std::cout << std::endl;
  }
}

