#ifndef B5_FUNCTIONS_HPP
#define B5_FUNCTIONS_HPP

#include <iostream>
#include <set>
#include "shape.hpp"

void readText(std::set<std::string>& container);
void printWords(const std::set<std::string>& container);

void readShape(std::vector<Shape>& vector, std::string& line);
Shape readPoints(std::string& line, std::size_t vertices);
bool isRectangle(const Shape& shape);
bool isSquare(const Shape& shape);
bool isLess(const Shape& shape1, const Shape& shape2);
int getLength(const Point_t&, const Point_t&);

#endif

