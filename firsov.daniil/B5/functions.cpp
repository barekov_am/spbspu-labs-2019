#include <iostream>
#include "shape.hpp"
#include "functions.hpp"

const std::size_t RECT_VERTICES = 4;

void readText(std::set<std::string>& container)
{
  std::string line;

  while (std::getline(std::cin, line)) {

    while (line.find_first_of(" \n\t") == 0) {
      line.erase(0, 1);
    }

    while (!line.empty()) {
      size_t position = line.find_first_of(" \n\t");

      std::string word;
      word = line.substr(0, position);
      container.emplace(word);

      line.erase(0, position);
      while (line.find_first_of(" \n\t") == 0) {
        line.erase(0, 1);
      }
    }
  }
}

void printWords(const std::set<std::string>& container)
{
  for (const auto& tmp : container) {
    std::cout << tmp << std::endl;
  }
}

void readShape(std::vector<Shape>& vector, std::string& line)
{
  while (std::getline(std::cin, line)) {
    if (std::cin.fail()) {
      throw std::ios_base::failure("Failed during reading. \n");
    }

    while (line.find_first_of(" \t") == 0) {
      line.erase(0, 1);
    }

    if (line.empty()) {
      continue;
    }

    std::size_t pos = line.find_first_of('(');
    if (pos == std::string::npos) {
      throw std::invalid_argument("Invalid shape. \n");
    }

    std::size_t numOfVertices = std::stoi(line.substr(0, pos));
    line.erase(0, pos);
    if (numOfVertices < 1) {
      throw std::invalid_argument("Invalid count of vertices .\n");
    }

    Shape shape = readPoints(line, numOfVertices);

    vector.push_back(shape);
  }
}

Shape readPoints(std::string& line, std::size_t vertices)
{
  Shape shape;
  std::size_t openBracket;
  std::size_t semicolon;
  std::size_t closeBracket;

  for (std::size_t i = 0; i < vertices; i++) {
    if (line.empty()) {
      throw std::invalid_argument("Invalid number of vertices. \n");
    }

    while (line.find_first_of(" \t") == 0) {
      line.erase(0, 1);
    }

    openBracket = line.find_first_of('(');
    semicolon = line.find_first_of(';');
    closeBracket = line.find_first_of(')');

    if ((openBracket == std::string::npos) || (semicolon == std::string::npos) || (closeBracket == std::string::npos)) {
      throw std::invalid_argument("Invalid point declaration. \n");
    }

    Point_t point
      {
        std::stoi(line.substr(openBracket + 1, semicolon - openBracket - 1)),
        std::stoi(line.substr(semicolon + 1, closeBracket - semicolon - 1))
      };

    line.erase(0, closeBracket + 1);

    shape.push_back(point);
  }

  while (line.find_first_of(" \t") == 0) {
    line.erase(0, 1);
  }

  if (!line.empty()) {
    throw std::invalid_argument("Too big amount of points. \n");
  }

  return shape;
}

bool isRectangle(const Shape& shape) {
  int diag1 = getLength(shape[0], shape[2]);
  int diag2 = getLength(shape[1], shape[3]);

  int v1 = getLength(shape[0], shape[1]);
  int v2 = getLength(shape[1], shape[2]);
  int v3 = getLength(shape[2], shape[3]);
  int v4 = getLength(shape[3], shape[0]);

  return v1 == v3 && v2 == v4 && diag1 == diag2;
}

bool isSquare(const Shape& shape)
{
  if (isRectangle(shape))
  {
    int side1 = getLength(shape[0], shape[1]);
    int side2 = getLength(shape[1], shape[2]);

    if (side1 == side2)
    {
      return true;
    }
  }

  return false;
}


bool isLess(const Shape& shape1, const Shape& shape2)
{
  if (shape1.size() < shape2.size()) {
    return true;
  }
  if ((shape1.size() == RECT_VERTICES) && (shape2.size() == RECT_VERTICES)) {
    if (isSquare(shape1)) {
      if (isSquare(shape2)) {
        return shape1[0].x < shape2[0].x;
      }
      return true;
    }
  }
  return false;
}

int getLength(const Point_t& point1, const Point_t& point2) {
  return (point1.x - point2.x) * (point1.x - point2.x) + (point1.y - point2.y) * (point1.y - point2.y);
}

