#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

void printCompositeShapeInfo(firsov::Shape& shape)
{
  shape.printShapeInfo();
  std::cout << "AREA = " << shape.getArea() << std::endl;
  shape.move({ 2, 1 });
  std::cout << "AFTER MOVING TO A POINT: " << std::endl;
  shape.printShapeInfo();
  shape.move(2, 8);
  std::cout << "AFTER ADD TO OX, OY: " << std::endl;
  shape.printShapeInfo();
  shape.scale(2);
  std::cout << "AFTER SCALING: " << std::endl;
  shape.printShapeInfo();
  shape.rotate(45);
  std::cout << "AFTER ROTATION: " << std::endl;
  shape.printShapeInfo();
}


int main()
{
  firsov::Circle circle(21.77, { 9, -4 });
  std::cout << "__Circle info__: " << "\n\n";
  circle.printShapeInfo();
  std::cout << "Add to OX " << 1 << " and to OY " << 4 << std::endl;
  circle.move(1, 4);
  circle.printShapeInfo();
  std::cout << "Move to point (" << 30 << ", " << 11 << ")\n";
  circle.move({ 30, 11 });
  circle.printShapeInfo();
  circle.scale(22.8);
  std::cout << "__New parameters__" << std::endl;
  circle.printShapeInfo();

  firsov::Rectangle rectangle(7, 7, { 3, 5 });
  std::cout << "__Rectangle info__ " << "\n\n";
  rectangle.printShapeInfo();
  std::cout << "Add to ОX " << 5 << " and  to OY " << -1.7 << std::endl;
  rectangle.move(5, -1.7);
  rectangle.printShapeInfo();
  std::cout << "Move to point (" << 16.5 << ", " << 9 << ")\n";
  rectangle.move({ 16.5, 9 });
  rectangle.printShapeInfo();
  std::cout << std::endl;
  rectangle.scale(13.37);
  std::cout << "__New parameters__" << std::endl;
  rectangle.printShapeInfo();

  std::cout << "__Composite shape__" << std::endl;
  firsov::Shape::shape_ptr circle2 = std::make_shared<firsov::Circle>(4, firsov::point_t { 5, 5 });
  firsov::Shape::shape_ptr rectangle2 = std::make_shared<firsov::Rectangle>(4, 4, firsov::point_t { 5, 5 });
  firsov::Shape::shape_ptr square = std::make_shared<firsov::Rectangle>(4, 4, firsov::point_t { 5, 5 });
  firsov::CompositeShape compShape(rectangle2);
  compShape.printShapeInfo();
  compShape.add(circle2);
  compShape.add(rectangle2);
  compShape.add(square);
  std::cout << "__Print info about Composite Shape__" << std::endl;
  printCompositeShapeInfo(compShape);
  return 0;
}
