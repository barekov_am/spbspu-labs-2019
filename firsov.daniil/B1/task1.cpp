#include <stdexcept>
#include <forward_list>
#include <vector>
#include <iostream>

#include "functions.hpp"

void task1(const char *direction)
{
  Direction dir = sortDirection(direction);
  std::vector<int> vectorOp;

  int current = 0;

  while (std::cin >> current)
  {
    vectorOp.push_back(current);
  }
  if (!std::cin.eof() && std::cin.fail()) {
    throw std::runtime_error("Failed reading.\n");
    }

  if (vectorOp.empty()) {
    return;
  }

  std::vector<int> vectorAt = vectorOp;
  std::forward_list<int> listIt(vectorOp.begin(), vectorOp.end());

  sort<SortByOperator>(vectorOp, dir);
  sort<SortByAt>(vectorAt, dir);
  sort<SortByIterator>(listIt, dir);

  printContainer(vectorOp);
  printContainer(vectorAt);
  printContainer(listIt);
}

