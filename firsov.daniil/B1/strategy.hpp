#ifndef B1_STRATEGY_HPP
#define B1_STRATEGY_HPP

#include <cstddef>
#include <forward_list>
#include <iterator>

template <typename Container>
struct SortByOperator
{
  using valueType = typename Container::value_type;

  static size_t begin(Container&) {
    return 0;
  }

  static size_t end(Container& container) {
    return container.size();
  }

  static valueType& getElement(Container& container, size_t index) {
    return container[index];
  }
};

template <typename Container>
struct SortByAt
{
  using valueType = typename Container::value_type;

  static size_t begin(Container&) {
    return 0;
  }

  static size_t end(Container& container) {
    return container.size();
  }

  static valueType& getElement(Container& container, size_t index) {
    return container.at(index);
  }
};

template <typename Container>
struct SortByIterator
{
  using index = typename Container::iterator;

  static index begin(Container& array) {
    return array.begin();
  }

  static index end(Container& array) {
    return array.end();
  }

  static typename Container::value_type& getElement(Container&, index& iter) {
    return *iter;
  }
};

#endif //B1_STRATEGY_HPP

