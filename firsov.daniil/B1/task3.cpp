#include <vector>

#include "functions.hpp"

void task3()
{
  std::vector<int> vector;
  int current = 0;

  while (std::cin >> current)
  {
    if (current == 0) {
      break;
    }
    vector.push_back(current);
  }

  if (!std::cin.eof() && std::cin.fail()) {
    throw std::ios_base::failure("Failed input.\n");
  }

  if (current != 0) {
    throw std::invalid_argument("Last number expected zero.\n"); 
  }

  if (vector.empty()) {
    return;
  }

  if (vector.back() == 1) {
   auto iter = vector.begin();

   while (iter != vector.end())
   {
     if (*iter % 2 == 0) {
       iter = vector.erase(iter);
     }
     else {
       ++iter;
     }
   }
 }
  else if (vector.back() == 2) {
    auto iter = vector.begin();

    while (iter != vector.end())
    {
      if (*iter % 3 == 0) {
        iter = (vector.insert(++iter, 3, 1) + 3);
      }
      else {
        ++iter;
      }
    }
  }

  printContainer(vector);
}

