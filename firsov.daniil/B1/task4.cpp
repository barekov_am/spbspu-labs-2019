#include <vector>

#include "functions.hpp"

void task4(const char *direction, const int size)
{
  if (size <= 0) {
    throw std::invalid_argument("Incorrect size.\n");
  }

  Direction dir = sortDirection(direction);

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);
  printContainer(vector);

  sort<SortByOperator>(vector, dir);
  printContainer(vector);
}

