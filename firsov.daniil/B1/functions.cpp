#include <cstring>

#include "functions.hpp"

Direction sortDirection(const char *direction)
{
  if (std::strcmp(direction, "ascending") == 0) {
    return Direction::ascending;
  }

  if (std::strcmp(direction, "descending") == 0) {
    return Direction::descending;
  }

  throw std::invalid_argument("Incorrect sorting direction.\n");
}

void fillRandom(double *array, int size)
{
  for (int i = 0; i < size; ++i)
  {
    array[i] = (rand() % 21 - 10) / 10.0;
  }
}

