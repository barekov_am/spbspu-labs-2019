#ifndef B1_FUNCTIONS_HPP
#define B1_FUNCTIONS_HPP

#include <stdexcept>
#include <functional>
#include <iostream>

#include "strategy.hpp"

enum class Direction
{
  ascending,
  descending
};

Direction sortDirection(const char *direction);
void fillRandom(double *array, int size);

template <typename T>
void printContainer(const T& container) {
  for (const auto& element : container) {
    std::cout << element << " ";
  }
  std::cout << std::endl;
}

template <template <class T> class Strategy, typename T>
void sort(T& container, Direction direction)
{
  const auto begin = Strategy<T>::begin(container);
  const auto end = Strategy<T>::end(container);

  for (auto i = begin; i != end; ++i)
  {
    for (auto j = i; j != end; ++j)
    {
      auto tmp1 = i;
      auto tmp2 = j;

      if ((direction == Direction::ascending)
          and (Strategy<T>::getElement(container, tmp1) > Strategy<T>::getElement(container, tmp2)))
      {
        std::swap(Strategy<T>::getElement(container, tmp1), Strategy<T>::getElement(container, tmp2));
      }

      if ((direction == Direction::descending)
          and (Strategy<T>::getElement(container, tmp1) < Strategy<T>::getElement(container, tmp2)))
      {
        std::swap(Strategy<T>::getElement(container, tmp1), Strategy<T>::getElement(container, tmp2));
      }

    }

  }
}

#endif //B1_FUNCTIONS_HPP

