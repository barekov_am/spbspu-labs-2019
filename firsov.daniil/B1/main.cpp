#include <ctime>
#include <iostream>
#include <string>

void task1(const char *direction);
void task2(const char *inp_file);
void task3();
void task4(const char *direction, const int size);

int main(int argc, char *argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4)) {
      std::cerr << "Incorrect number of arguments.\n";
      return 1;
    }

    char *endStr = nullptr;
    int task = std::strtol(argv[1], &endStr, 10);

    switch (task)
    {
      case 1: {
        if (argc != 3) {
          std::cerr << "Incorrect number of arguments.\n";
          return 1;
        }

        task1(argv[2]);
        break;
      }

      case 2:
        {
        if (argc != 3) {
          std::cerr << "Incorrect number of arguments.\n";
          return 1;
        }

        if (sizeof(argv[2]) == 0) {
          std::cerr << "File is not exist.\n";
          return 1;
        }

        task2(argv[2]);
        break;
      }

      case 3:
        {
        if (argc != 2) {
          std::cerr << "Incorrect number of arguments.\n";
          return 1;
        }

        task3();
        break;
      }

      case 4:
        {
        if (argc != 4) {
          std::cerr << "Incorrect number of arguments.\n";
          return 1;
        }

        srand(time(0));
        int size = std::strtol(argv[3], &endStr, 10);
        task4(argv[2], size);
        break;
      }

      default:
        {
        std::cerr << "Invalid task number.\n";
        return 1;
      }
    }
    return 0;
  }

  catch (const std::exception& ex) {
    std::cerr << ex.what();
    return 1;
  }
}

