#ifndef B7_TRIANGLE_HPP
#define B7_TRIANGLE_HPP

#include "shape.hpp"

class Triangle : public Shape {
public:
  Triangle(double x, double y);
  void draw(std::ostream& out) const override;
};

#endif
