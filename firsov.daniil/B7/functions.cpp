#include "functions.hpp"

bool Line::containsNonSpaceCharacters()
{
  return (std::find_if(begin(), end(),
                       [](char c) { return !std::isspace(c, std::locale()); }) != end());
}

std::istream& operator>>(std::istream& in, Line& line)
{
  while (std::getline(in, line)) {
    if (!line.empty() && line.containsNonSpaceCharacters()) {
      break;
    }
  }
  return in;
}

std::unique_ptr<Shape> getShapeFromLine(const Line& line)
{
  std::stringstream stream(line);

  std::string typeOfShape;
  std::getline(stream, typeOfShape, '(');

  auto firstChar = std::find_if(typeOfShape.begin(), typeOfShape.end(),
                                std::bind(std::isalpha<char>, std::placeholders::_1, std::locale()));

  typeOfShape.erase(typeOfShape.begin(), firstChar);
  typeOfShape.erase(std::find_if(firstChar, typeOfShape.end(),
                                std::bind(std::isspace<char>, std::placeholders::_1, std::locale())), typeOfShape.end());
  double x_, y_;

  stream >> x_;
  stream.ignore(std::numeric_limits<std::streamsize>::max(), ';');

  stream >> y_;
  stream.ignore(std::numeric_limits<std::streamsize>::max(), ')');

  if (stream.fail()) {
    throw std::invalid_argument("Incorrect values. \n");
  }

  Line remLine;
  std::getline(stream, remLine);
  if (remLine.containsNonSpaceCharacters()) {
    throw std::invalid_argument("Invalid input. \n");
  }

  if (typeOfShape == "CIRCLE") {
    return std::unique_ptr<Shape>(new Circle(x_, y_));
  } else if (typeOfShape == "TRIANGLE") {
    return std::unique_ptr<Shape>(new Triangle(x_, y_));
  } else if (typeOfShape == "SQUARE") {
    return std::unique_ptr<Shape>(new Square(x_, y_));
  } else {
    throw std::invalid_argument("Invalid shape type. \n");
  }
}

