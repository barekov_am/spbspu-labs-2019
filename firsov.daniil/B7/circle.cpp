#include "circle.hpp"

Circle::Circle(double x, double y):
  Shape(x, y)
{
}

void Circle::draw(std::ostream &out) const
{
  out << "CIRCLE (" << x_ << ";" << y_ << ")" << std::endl;
}
