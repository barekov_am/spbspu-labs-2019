#ifndef B7_CIRCLE_HPP
#define B7_CIRCLE_HPP

#include "shape.hpp"

class Circle : public Shape {
public:
  Circle(double x, double y);
  void draw(std::ostream& out) const override;
};

#endif
