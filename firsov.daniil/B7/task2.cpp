#include <iostream>
#include <functional>
#include <memory>
#include <sstream>
#include "functions.hpp"

void task2(std::istream& in, std::ostream& out) {

  std::list<std::unique_ptr<Shape>> listOfShapes;

  std::transform(std::istream_iterator<Line>(in), std::istream_iterator<Line>(),
                 std::back_inserter(listOfShapes), getShapeFromLine);

  out << "Original:" << std::endl;
  std::for_each(listOfShapes.begin(), listOfShapes.end(),
                std::bind(&Shape::draw, std::placeholders::_1, std::ref(out)));

  out << "Left-Right:" << std::endl;
  listOfShapes.sort(std::bind(&Shape::isMoreLeft,
                              std::bind(&std::unique_ptr<Shape>::get, std::placeholders::_1),
                              std::bind(&std::unique_ptr<Shape>::get, std::placeholders::_2)));

  std::for_each(listOfShapes.begin(), listOfShapes.end(),
                std::bind(&Shape::draw, std::placeholders::_1, std::ref(out)));

  out << "Right-Left:" << std::endl;
  std::for_each(listOfShapes.rbegin(), listOfShapes.rend(),
                std::bind(&Shape::draw, std::placeholders::_1, std::ref(out)));


  out << "Top-Bottom:" << std::endl;
  listOfShapes.sort(std::bind(&Shape::isUpper,
                              std::bind(&std::unique_ptr<Shape>::get, std::placeholders::_1),
                              std::bind(&std::unique_ptr<Shape>::get, std::placeholders::_2)));

  std::for_each(listOfShapes.begin(), listOfShapes.end(),
                std::bind(&Shape::draw, std::placeholders::_1, std::ref(out)));

  out << "Bottom-Top:" << std::endl;
  std::for_each(listOfShapes.rbegin(), listOfShapes.rend(),
                std::bind(&Shape::draw, std::placeholders::_1, std::ref(out)));

}
