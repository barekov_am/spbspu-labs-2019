#ifndef B7_FUNCTIONS_HPP
#define B7_FUNCTIONS_HPP

#include <algorithm>
#include <iterator>
#include <list>
#include <memory>
#include <functional>
#include <sstream>
#include "shape.hpp"
#include "square.hpp"
#include "triangle.hpp"
#include "circle.hpp"


class Line : public std::string {
public:
  bool containsNonSpaceCharacters();
  friend std::istream& operator>>(std::istream& in, Line& line);
};

std::unique_ptr<Shape> getShapeFromLine(const Line& line);

#endif
