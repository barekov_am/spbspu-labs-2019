#include <iostream>

void task1(std::istream &inp, std::ostream &out);
void task2(std::istream &inp, std::ostream &out);

int main(int argc, char *argv[]) {
  try {
    if (argc != 2) {
      std::cerr << "Incorrect number of arguments. \n";
      return 1;
    }

    const int task = std::stoi(argv[1]);

    switch (task) {
    case 1:
      {
        task1(std::cin, std::cout);
        break;
      }
    case 2:
      {
        task2(std::cin, std::cout);
        break;
      }
    default:
      {
        std::cerr << "Incorrect task number." << std::endl;
        return 1;
      }
    }
  } catch (const std::exception& ex) {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
}
