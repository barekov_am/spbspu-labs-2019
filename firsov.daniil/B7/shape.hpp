#ifndef B7_SHAPE_HPP
#define B7_SHAPE_HPP

#include <iostream>

class Shape {
public:
  Shape(double x, double y);
  virtual ~Shape() = default;
  bool isMoreLeft(const Shape *tmp) const;
  bool isUpper(const Shape *tmp) const;
  virtual void draw(std::ostream& out) const = 0;

protected:
  double x_, y_;
};

#endif
