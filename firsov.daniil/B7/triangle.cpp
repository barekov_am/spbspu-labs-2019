#include "triangle.hpp"

Triangle::Triangle(double x, double y) :
  Shape(x, y)
{
}

void Triangle::draw(std::ostream& out) const {
  out << "TRIANGLE (" << x_ << ";" << y_ << ")" << std::endl;
}
