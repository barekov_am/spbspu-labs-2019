#include <iostream>
#include <iterator>
#include <algorithm>
#include <list>
#include <cmath>

void task1(std::istream& inp, std::ostream& out) {
  std::list<double> numbers((std::istream_iterator<double>(inp)), std::istream_iterator<double>());

  if (!inp.eof() && inp.fail()) {
    throw std::runtime_error("Input failed. \n");
  }
  auto predicate = [](const double& elem) { return elem * M_PI; };
  std::transform(numbers.begin(), numbers.end(), numbers.begin(), predicate);

  std::copy(numbers.begin(), numbers.end(), std::ostream_iterator<double>(out, " "));
  std::cout << "\n";
}
