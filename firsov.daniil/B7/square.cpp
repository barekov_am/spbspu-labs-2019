#include "square.hpp"
#include "shape.hpp"

Square::Square(double x, double y) :
  Shape(x, y)
{
}

void Square::draw(std::ostream& out) const {
  out << "SQUARE (" << x_ << ";" << y_ << ")" << std::endl;
}

