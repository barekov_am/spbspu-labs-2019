#include <iostream>
#include <iterator>

#include "functor.hpp"

void task()
{
  Functor functor;
  std::for_each(std::istream_iterator< int >(std::cin),
      std::istream_iterator< int >(), std::ref(functor));

  if (!std::cin.eof())
  {
    throw std::invalid_argument("Invalid data!");
  }

  if (functor.isEmpty())
  {
    std::cout << "No Data\n";
    return;
  }

  std::cout.precision(1);

  std::cout << "Max: " << functor.getMaxNumber() << "\n";
  std::cout << "Min: " << functor.getMinNumber() << "\n";
  std::cout << "Mean: " << std::fixed << functor.getAverageNumber() << "\n";
  std::cout << "Positive: " << functor.getNumOfPositive() << "\n";
  std::cout << "Negative: " << functor.getNumOfNegative() << "\n";
  std::cout << "Odd Sum: " << functor.getOddSum() << "\n";
  std::cout << "Even Sum: " << functor.getEvenSum() << "\n";
  std::cout << "First/Last Equal: ";

  std::cout << (functor.isFirstEqLast() ? "yes\n" : "no\n");
}
