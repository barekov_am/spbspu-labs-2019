#include "functor.hpp"

void Functor::operator()(const int& num)
{
  numbers_.push_back(num);
}

int Functor::getMaxNumber() const
{
  return (*std::max_element(numbers_.begin(), numbers_.end()));
}

int Functor::getMinNumber() const
{
  return (*std::min_element(numbers_.begin(), numbers_.end()));
}

double Functor::getAverageNumber() const
{
  double averageNumber = 0;
  for (int num : numbers_)
  {
    averageNumber += num;
  }

  averageNumber = averageNumber/numbers_.size();

  return averageNumber;
}

int Functor::getNumOfPositive() const
{
  int count = 0;
  auto iter = numbers_.begin();
  while(iter != numbers_.end())
  {
    if (*iter > 0)
    {
      ++count;
    }
    ++iter;
  }

  return count;
}

int Functor::getNumOfNegative() const
{
  int count = 0;
  auto iter = numbers_.begin();
  while(iter != numbers_.end())
  {
    if (*iter < 0)
    {
      ++count;
    }
    ++iter;
  }

  return count;
}

long long int Functor::getEvenSum() const
{
  long long int evenSum = 0;
  for (int number : numbers_)
  {
    if (number % 2 == 0)
    {
      evenSum += number;
    }
  }
  return evenSum;
}

long long int Functor::getOddSum() const
{
  long long int oddSum = 0;
  for (int number : numbers_)
  {
    if (number % 2 != 0)
    {
      oddSum += number;
    }
  }
  return oddSum;
}

bool Functor::isFirstEqLast() const
{
  return numbers_.front() == numbers_.back();
}

bool Functor::isEmpty() const
{
  return numbers_.empty();
}
