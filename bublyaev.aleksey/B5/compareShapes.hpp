#ifndef COMPARESHAPES_HPP
#define COMPARESHAPES_HPP

#include <functional>
#include "shapeContainer.hpp"

class CompareShapes: public std::binary_function<const Shape &, const Shape &, bool>
{
public:
  bool operator ()(const Shape &lhs, const Shape &rhs);
};

#endif // COMPARESHAPES_HPP
