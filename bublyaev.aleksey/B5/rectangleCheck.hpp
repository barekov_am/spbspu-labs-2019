#ifndef RECTANGLECHECK_HPP
#define RECTANGLECHECK_HPP

#include "shapeContainer.hpp"

int getSquaredDistance(const point &lhs, const point &rhs);
bool isRectangle(const Shape &shape);
bool isSquare(const Shape &shape);

#endif // RECTANGLECHECK_HPP
