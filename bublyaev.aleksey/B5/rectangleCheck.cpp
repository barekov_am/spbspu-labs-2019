#include "rectangleCheck.hpp"
int getSquaredDistance(const point &lhs, const point &rhs)
{
  return (lhs.x - rhs.x) * (lhs.x - rhs.x) + (lhs.y - rhs.y) * (lhs.y - rhs.y);
}

bool isRectangle(const Shape &shape)
{
  int diag1 = getSquaredDistance(shape[0], shape[2]);
  int diag2 = getSquaredDistance(shape[1], shape[3]);
  if (diag1 == diag2)
  {
    return true;
  }
  return false;
}

bool isSquare(const Shape &shape)
{
  if (isRectangle(shape))
  {
    int side1 = getSquaredDistance(shape[0], shape[1]);
    int side2 = getSquaredDistance(shape[1], shape[2]);
    if (side1 == side2)
    {
      return true;
    }
  }
  return false;
}
