#ifndef SHAPECONTAINER_HPP
#define SHAPECONTAINER_HPP

#include <vector>

struct point
{
  int x, y;
};

using Shape = std::vector<point>;
using ShapeList = std::vector<Shape>;

class ShapeContainer
{
public:
  ShapeContainer();

  void readShapes();
  void analyseShapeList();
  void deletePentagons();
  void createPointsList();
  void changeShapeList();
  void printData() const;

private:
  std::size_t vertices_;
  std::size_t triangles_;
  std::size_t squares_;
  std::size_t rectangles_;
  ShapeList shapes_;
  Shape pointsList_;
};

#endif // SHAPECONTAINER_HPP
