#include "compareShapes.hpp"
#include "rectangleCheck.hpp"

bool CompareShapes::operator ()(const Shape &lhs, const Shape &rhs)
{
  if (lhs.size() < rhs.size())
  {
    return true;
  }
  size_t rectangleVertices = 4;
  if ((lhs.size() == rectangleVertices) && (rhs.size() == rectangleVertices))
  {
    if (isSquare(lhs))
    {
      if (isSquare(rhs))
      {
        return false;
      }
      return true;
    }
  }
  return false;
}
