#ifndef ANALYSESHAPE_HPP
#define ANALYSESHAPE_HPP

#include <functional>
#include "shapeContainer.hpp"

class analyseShape: public std::unary_function<const Shape &, void>
{
public:
  analyseShape();

  void operator ()(const Shape & shape);

  std::size_t getVerticesAmount() const;
  std::size_t getTrianglesAmount() const;
  std::size_t getSquaresAmount() const;
  std::size_t getRectanglesAmount() const;
private:
  std::size_t vertices_;
  std::size_t triangles_;
  std::size_t squares_;
  std::size_t rectangles_;
};

#endif // ANALYSESHAPE_HPP
