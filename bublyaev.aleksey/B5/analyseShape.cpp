#include "analyseShape.hpp"
#include "rectangleCheck.hpp"

analyseShape::analyseShape():
  vertices_(0),
  triangles_(0),
  squares_(0),
  rectangles_(0)
{
}

void analyseShape::operator ()(const Shape &shape)
{
  size_t num = shape.size();
  vertices_ += num;
  switch (num)
  {
  case 3:
  {
    triangles_++;
    break;
  }

  case 4:
  {
    if (isRectangle(shape))
    {
      rectangles_++;
      if (isSquare(shape))
      {
        squares_++;
      }
    }
    break;
  }
  }
}

std::size_t analyseShape::getVerticesAmount() const
{
  return vertices_;
}

std::size_t analyseShape::getTrianglesAmount() const
{
  return triangles_;
}

std::size_t analyseShape::getSquaresAmount() const
{
  return squares_;
}

std::size_t analyseShape::getRectanglesAmount() const
{
  return rectangles_;
}
