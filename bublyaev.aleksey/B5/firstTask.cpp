#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

void firstTask()
{
  std::vector<std::string> words;
  std::string line;
  std::string delim = " \t\n";
  size_t position = std::string::npos;

  while (std::getline(std::cin, line))
  {
    while (line.find_first_of(delim) == 0)
    {
      line.erase(0, 1);
    }

    while (!line.empty())
    {
      position = line.find_first_of(delim);
      words.push_back(line.substr(0, position));
      if (position == std::string::npos)
      {
        line.clear();
        break;
      }
      line.erase(0, position + 1);

      while (line.find_first_of(delim) == 0)
      {
        line.erase(0, 1);
      }
    }
  }

  std::sort(words.begin(), words.end());
  words.erase(std::unique(words.begin(), words.end()), words.end());

  for (auto i = words.begin(); i != words.end(); i++)
  {
    std::cout << *i << "\n";
  }
}
