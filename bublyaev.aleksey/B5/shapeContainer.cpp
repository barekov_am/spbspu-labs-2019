#include "shapeContainer.hpp"

#include <iostream>
#include <string>
#include <cmath>
#include <algorithm>
#include <iterator>

#include "rectangleCheck.hpp"
#include "compareShapes.hpp"
#include "analyseShape.hpp"

ShapeContainer::ShapeContainer():
  vertices_(0),
  triangles_(0),
  squares_(0),
  rectangles_(0)
{
}

void ShapeContainer::readShapes()
{
  std::string line;
  std::string delim = " \t";
  Shape shape;

  while (std::getline(std::cin, line))
  {
    while (line.find_first_of(delim) == 0)
    {
      line.erase(0, 1);
    }

    size_t pos = line.find_first_of(delim);
    if (pos == std::string::npos)
    {
      continue;
    }

    int num = std::stoi(line.substr(0, pos));
    if (num <= 0)
    {
      throw std::invalid_argument("Amount of vertices must be positive.");
    }

    line.erase(0, pos + 1);

    for (int i = 0; i < num; i++)
    {
      while (line.find_first_of(delim) == 0)
      {
        line.erase(0, 1);
      }
      if (line.empty())
      {
        throw std::invalid_argument("Wrong amount of vertices.");
      }
      size_t openingBracket = line.find_first_of('(');
      size_t semicolon = line.find_first_of(';');
      size_t closingBracket = line.find_first_of(')');

      if ((openingBracket == std::string::npos) || (semicolon == std::string::npos) || (closingBracket == std::string::npos)
          || (openingBracket != 0) || !((openingBracket < semicolon) && (closingBracket > semicolon)))
      {
        throw std::invalid_argument("Wrong point declaration.");
      }
      int x = std::stoi(line.substr(1, semicolon - 1));
      int y = std::stoi(line.substr(semicolon + 1, closingBracket - semicolon - 1));
      shape.push_back({ x, y });
      line.erase(0, closingBracket + 1);
    }

    while (line.find_first_of(delim) == 0)
    {
      line.erase(0, 1);
    }

    if (!line.empty())
    {
      throw std::invalid_argument("Too much vertices.");
    }
    shapes_.push_back(shape);
    shape.clear();
  }
}

void ShapeContainer::analyseShapeList()
{
  analyseShape analisys = std::for_each(shapes_.begin(), shapes_.end(), analyseShape());
  vertices_ = analisys.getVerticesAmount();
  triangles_ = analisys.getTrianglesAmount();
  squares_ = analisys.getSquaresAmount();
  rectangles_ = analisys.getRectanglesAmount();
}

void ShapeContainer::deletePentagons()
{
  auto predicate = [](const Shape &sh) { return sh.size() == 5; };
  shapes_.erase(std::remove_if(shapes_.begin(), shapes_.end(), predicate), shapes_.end());
}

void ShapeContainer::createPointsList()
{
  auto predicate = [](const Shape &sh) { return sh[0]; };
  std::transform(shapes_.begin(), shapes_.end(), back_inserter(pointsList_), predicate);
}

void ShapeContainer::changeShapeList()
{
  std::sort(shapes_.begin(), shapes_.end(), CompareShapes());
}

void ShapeContainer::printData() const
{
  std::cout << "Vertices: " << vertices_ << "\n";
  std::cout << "Triangles: " << triangles_ << "\n";
  std::cout << "Squares: " << squares_ << "\n";
  std::cout << "Rectangles: " << rectangles_ << "\n";
  std::cout << "Points:";
  for (auto i = pointsList_.begin(); i != pointsList_.end(); i++)
  {
    std::cout << " (" << i->x << ";" << i->y << ")";
  }
  std::cout << "\nShapes:\n";
  for (auto i = shapes_.begin(); i != shapes_.end(); i++)
  {
    std::cout << i->size();
    for (auto j = i->begin(); j != i->end(); j++)
    {
      std::cout << " (" << j->x << ";" << j->y << ")";
    }
    std::cout << "\n";
  }
}
