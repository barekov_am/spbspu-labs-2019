#include "shapeContainer.hpp"

void secondTask()
{
  ShapeContainer shapes;
  shapes.readShapes();
  shapes.analyseShapeList();
  shapes.deletePentagons();
  shapes.createPointsList();
  shapes.changeShapeList();
  shapes.printData();
}
