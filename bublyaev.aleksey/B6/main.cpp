#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

#include "analyseData.hpp"

int main()
{
  try
  {
    std::vector<int> vector;
    std::string value;
    while (std::cin >> value)
    {
      double number = std::stod(value);
      if ((number - static_cast<int>(number)) != 0)
      {
        std::cerr << "Float number.";
        return 1;
      }
      vector.push_back(std::stoi(value));
    }

    analyseData analysis = std::for_each(vector.begin(), vector.end(), analyseData());
    analysis.printStatistics();
  }
  catch (const std::exception &ex)
  {
    std::cerr << ex.what();
    return 1;
  }

  return 0;
}
