#include "analyseData.hpp"

#include <iostream>

analyseData::analyseData():
  max_(0),
  min_(0),
  mean_(0),
  positiveAmount_(0),
  negativeAmount_(0),
  zeroAmount_(0),
  oddSum_(0),
  evenSum_(0),
  firstLastEqual_(false),
  first_(0)
{
}

void analyseData::operator ()(const int &num)
{
  if (isEmpty())
  {
    max_ = num;
    min_ = num;
    first_ = num;
  }

  if (num > max_)
  {
    max_ = num;
  }
  if (num < min_)
  {
    min_ = num;
  }

  if (num == 0)
  {
    zeroAmount_++;
  }
  else if (num > 0)
  {
    positiveAmount_++;
  }
  else
  {
    negativeAmount_++;
  }

  if (num % 2 == 0)
  {
    evenSum_ += num;
  }
  else
  {
    oddSum_ += num;
  }

  mean_ = static_cast<double>(evenSum_ + oddSum_) / (zeroAmount_ + positiveAmount_ + negativeAmount_);

  if (first_ == num)
  {
    firstLastEqual_ = true;
  }
  else
  {
    firstLastEqual_ = false;
  }
}

void analyseData::printStatistics() const
{
  if (isEmpty())
  {
    std::cout << "No Data\n";
    return;
  }
  std::cout << "Max: " << max_;
  std::cout << "\nMin: " << min_;
  std::cout << "\nMean: " << mean_;
  std::cout << "\nPositive: " << positiveAmount_;
  std::cout << "\nNegative: " << negativeAmount_;
  std::cout << "\nOdd Sum: " << oddSum_;
  std::cout << "\nEven Sum: " << evenSum_;
  std::cout << "\nFirst/Last Equal: " << (firstLastEqual_ ? "yes\n" : "no\n");
}

bool analyseData::isEmpty() const
{
  return (positiveAmount_ == 0) && (negativeAmount_ == 0) && (zeroAmount_ == 0);
}
