#ifndef ANALYSEDATA_HPP
#define ANALYSEDATA_HPP

#include <functional>

class analyseData: public std::unary_function<const int &, void>
{
public:
  analyseData();

  void operator ()(const int &num);
  void printStatistics() const;

  bool isEmpty() const;
private:
  int max_;
  int min_;
  double mean_;
  int positiveAmount_;
  int negativeAmount_;
  int zeroAmount_;
  long long int oddSum_;
  long long int evenSum_;
  bool firstLastEqual_;
  int first_;
};

#endif // ANALYSEDATA_HPP
