#include "triangle.hpp"

Triangle::Triangle(const point_t &center):
  Shape(center)
{
}

void Triangle::draw(std::ostream &stream) const
{
  stream << "TRIANGLE (" << center_.x << "; " << center_.y << ")\n";
}
