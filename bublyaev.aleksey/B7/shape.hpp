#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

struct point_t
{
  int x;
  int y;
};

class Shape
{
public:
  Shape(const point_t &center);
  virtual ~Shape() = default;

  bool isMoreLeft(Shape * shape) const;
  bool isUpper(Shape * shape) const;

  virtual void draw(std::ostream &stream) const = 0;
protected:
  point_t center_;
};

#endif // SHAPE_H
