#include "circle.hpp"

Circle::Circle(const point_t &center):
  Shape(center)
{
}

void Circle::draw(std::ostream &stream) const
{
  stream << "CIRCLE (" << center_.x << "; " << center_.y << ")\n";
}
