#include <iostream>

void firstTask();
void secondTask();

int main(int argc, char * argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Incorrect number of arquments.";
      return 1;
    }

    const int task = std::stoi(argv[1]);

    switch (task)
    {
    case 1:
    {
      firstTask();
      break;
    }
    case 2:
    {
      secondTask();
      break;
    }
    default:
    {
      std::cerr << "Incorrect task number.";
      return 1;
    }
    }
  }
  catch (const std::exception &ex)
  {
    std::cerr << ex.what();
    return 1;
  }

  return 0;
}
