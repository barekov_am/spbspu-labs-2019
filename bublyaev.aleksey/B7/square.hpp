#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "shape.hpp"

class Square : public Shape
{
public:
  Square(const point_t &center);

  void draw(std::ostream &stream) const override;
};

#endif // SQUARE_HPP
