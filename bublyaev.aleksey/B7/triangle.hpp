#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

class Triangle : public Shape
{
public:
  Triangle(const point_t &center);

  void draw(std::ostream &stream) const override;
};

#endif // TRIANGLE_HPP
