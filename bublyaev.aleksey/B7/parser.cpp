#include <iostream>
#include <string>
#include <memory>

#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"

std::shared_ptr<Shape> readShape(std::string &line)
{
  size_t openingBracket = line.find_first_of('(');

  std::string name = line.substr(0, openingBracket);
  line.erase(0, openingBracket);

  if (line.empty())
  {
    throw std::invalid_argument("Wrong center point declaration.");
  }

  openingBracket = line.find_first_of('(');
  size_t semicolon = line.find_first_of(';');
  size_t closingBracket = line.find_first_of(')');

  if ((openingBracket != 0) || (semicolon == std::string::npos) || (closingBracket == std::string::npos)
      || !((openingBracket < semicolon) && (closingBracket > semicolon)))
  {
    throw std::invalid_argument("Wrong point declaration.");
  }

  int x = std::stoi(line.substr(1, semicolon - 1));
  int y = std::stoi(line.substr(semicolon + 1, closingBracket - semicolon - 1));

  point_t point = { x, y };

  if (name == "CIRCLE")
  {
    return std::make_shared<Circle>(Circle(point));
  }
  if (name == "TRIANGLE")
  {
    return std::make_shared<Triangle>(Triangle(point));
  }
  if (name == "SQUARE")
  {
    return std::make_shared<Square>(Square(point));
  }

  throw std::invalid_argument("Wrong shape name.");
}
