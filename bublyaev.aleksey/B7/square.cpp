#include "square.hpp"

Square::Square(const point_t &center):
  Shape(center)
{
}

void Square::draw(std::ostream &stream) const
{
  stream << "SQUARE (" << center_.x << "; " << center_.y << ")\n";
}
