#include "shape.hpp"

Shape::Shape(const point_t &center):
  center_(center)
{
}

bool Shape::isMoreLeft(Shape * shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Null pointer to shape.");
  }

  return center_.x < shape->center_.x;
}

bool Shape::isUpper(Shape * shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Null pointer to shape.");
  }

  return center_.y > shape->center_.y;
}
