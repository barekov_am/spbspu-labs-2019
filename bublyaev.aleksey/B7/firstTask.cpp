#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <iterator>
#include <math.h>

void firstTask()
{
  std::vector<double> vector((std::istream_iterator<double>(std::cin)), std::istream_iterator<double>());

  if (!std::cin.eof())
  {
    throw std::ios_base::failure("Input failed.");
  }

  auto predicate = [](const double &elem) { return elem * M_PI; };
  std::transform(vector.begin(), vector.end(), vector.begin(), predicate);

  std::copy(vector.begin(), vector.end(), std::ostream_iterator<double>(std::cout, " "));
  std::cout << "\n";
}
