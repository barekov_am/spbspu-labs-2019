#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

void printShape(const bublyaev::Shape * shapePtr)
{
  if (shapePtr == nullptr)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  else
  {
    shapePtr->printInfo();
    std::cout << "=====================================================" << std::endl;
  }
}

int main()
{
  bublyaev::Circle circle1({56, 10}, 8);
  std::cout << "Creating the first circle at {56, 10}, radius = 8" << std::endl;
  circle1.move({5, 5});
  bublyaev::Circle circle2({47, 12}, 5);
  std::cout << "Creating the second circle at {47, 12}, radius = 5" << std::endl;
  circle2.move(5, 2);
  circle2.scale(3);
  bublyaev::Rectangle rectangle1({0, 0}, 5, 4);
  std::cout << "Creating the first rectangle at {0, 0}, width = 5, height = 4" << std::endl;
  rectangle1.move({3, 12});
  bublyaev::Rectangle rectangle2({5, 12}, 2, 4);
  std::cout << "Creating the second rectangle at {5, 12}, width = 2, height = 4" << std::endl;
  rectangle2.move(-3, 31);
  rectangle2.scale(0.5);

  bublyaev::Shape::pointer circle1Ptr = std::make_shared<bublyaev::Circle>(circle1);
  bublyaev::Shape::pointer circle2Ptr = std::make_shared<bublyaev::Circle>(circle2);
  bublyaev::Shape::pointer rectangle1Ptr = std::make_shared<bublyaev::Rectangle>(rectangle1);
  bublyaev::Shape::pointer rectangle2Ptr = std::make_shared<bublyaev::Rectangle>(rectangle2);

  bublyaev::CompositeShape composite(circle1Ptr);
  composite.add(circle2Ptr);
  composite.add(rectangle1Ptr);
  composite.add(rectangle2Ptr);
  printShape(&composite);
  composite.move(4,7);
  printShape(&composite);
  composite.scale(3);
  printShape(&composite);
  composite.remove(2);
  printShape(&composite);
  composite.move({0,0});

  std::cout << std::endl;

  bublyaev::Shape * shapeArr[5] = {&circle1, &rectangle1, &circle2, &rectangle2,&composite};
  for (bublyaev::Shape * shape : shapeArr)
  {
    printShape(shape);
  }

  return 0;
}
