#include <iostream>
#include <vector>
#include <forward_list>

#include "utility.hpp"

void firstTask(const char * direction)
{
  auto order = getDirection<int>(direction);
  std::vector<int> vectorBrackets;

  int i = 0;
  while (std::cin && !(std::cin >> i).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input failed");
    }

    vectorBrackets.push_back(i);
  }

  if (vectorBrackets.empty())
  {
    return;
  }

  std::vector<int> vectorAt(vectorBrackets);
  std::forward_list<int> listIterator(vectorBrackets.begin(), vectorBrackets.end());

  sort<bracketsStrategy>(vectorBrackets, order);
  sort<atStrategy>(vectorAt, order);
  sort<iteratorStrategy>(listIterator, order);

  printCollection(vectorBrackets);
  printCollection(vectorAt);
  printCollection(listIterator);
}
