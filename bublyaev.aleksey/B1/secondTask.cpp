#include <fstream>
#include <iostream>
#include <memory>
#include <vector>

const size_t initCapacity = 256;

void secondTask(const char * fileName)
{
  std::ifstream inputFile(fileName);
  if (!inputFile)
  {
    throw std::runtime_error("File is not open.");
  }

  size_t size = initCapacity;

  std::unique_ptr<char[], decltype(&free)> array(static_cast<char *>(malloc(size)), &free);

  if (!array)
  {
    throw std::runtime_error("Could not allocate memory.");
  }

  size_t index = 0;
  while (inputFile)
  {
    inputFile.read(&array[index], initCapacity);
    index += inputFile.gcount();
    if (inputFile.gcount() == initCapacity)
    {
      size += initCapacity;
      std::unique_ptr<char[], decltype(&free)> newArray(static_cast<char *>(realloc(array.get(), size)), &free);

      if (!newArray)
      {
        throw std::runtime_error("Could not reallocate memory.");
      }

      array.release();
      std::swap(array, newArray);
    }
    if (!inputFile.eof() && inputFile.fail())
    {
      throw std::runtime_error("Reading failed");
    }
  }

  std::vector<char> vector(&array[0], &array[index]);
  for (auto element : vector)
  {
    std::cout << element;
  }
}

