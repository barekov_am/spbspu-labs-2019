#include <iostream>
#include <cstdlib>
#include <ctime>

void firstTask(const char * direction);
void secondTask(const char * fileName);
void thirdTask();
void fourthTask(const char * direction, int size);

int main(int argc, char * argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4))
    {
      std::cerr << "Incorrect number of arquments.";
      return 1;
    }

    const int task = std::stoi(argv[1]);

    switch (task)
    {
    case 1:
    {
      if (argc != 3)
      {
        std::cerr << "Incorrect number of arquments.";
        return 1;
      }
      firstTask(argv[2]);
      break;
    }

    case 2:
    {
      if (argc != 3)
      {
        std::cerr << "Incorrect number of arquments.";
        return 1;
      }
      if (sizeof(argv[2]) == 0)
      {
        std::cerr << "File is not exist";
        return 1;
      }

      secondTask(argv[2]);
      break;
    }

    case 3:
    {
      if (argc != 2)
      {
        std::cerr << "Incorrect number of arquments.";
        return 1;
      }
      thirdTask();
      break;
    }

    case 4:
    {
      if (argc != 4)
      {
        std::cerr << "Incorrect number of arquments.";
        return 1;
      }
      srand(time(nullptr));
      fourthTask(argv[2], std::stoi(argv[3]));
      break;
    }

    default:
    {
      std::cerr << "Incorrect task number.";
      return 1;
    }
    }
  }
  catch (const std::exception &ex)
  {
    std::cerr << ex.what();
    return 1;
  }
  return 0;
}
