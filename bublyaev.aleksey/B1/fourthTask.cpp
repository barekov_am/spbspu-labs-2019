#include <random>
#include <vector>

#include "utility.hpp"

void fillRandom(double * array, int size)
{
  for (int i = 0; i < size; i++)
  {
    array[i] = (rand() % 21 - 10) / 10.0;
  }
}

void fourthTask(const char * direction, int size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size must be a positive number.");
  }
  auto order = getDirection<double>(direction);

  std::vector<double> vector(static_cast<unsigned int>(size));
  fillRandom(&vector[0], size);
  printCollection(vector);

  sort<bracketsStrategy>(vector, order);

  printCollection(vector);
}
