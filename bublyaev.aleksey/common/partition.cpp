#include "partition.hpp"

bublyaev::Matrix bublyaev::part(const Shape::arrayPointer &array, size_t size)
{
  Matrix tmpMatrix;
  size_t tmpRow = 0;
  bool intersect = false;

  for (size_t i = 0; i < size; i++)
  {
    for (size_t j = 0; j < tmpMatrix.getRows(); j++)
    {
      intersect = false;
      for (size_t k = 0; k < tmpMatrix.getLayerSize(j); k++)
      {
        if (isIntersected(tmpMatrix[j][k]->getFrameRect(), array[i]->getFrameRect()))
        {
          tmpRow = j + 1;
          intersect = true;
          break;
        }

        tmpRow = j;
      }

      if (!intersect)
      {
        break;
      }
    }

    tmpMatrix.add(array[i], tmpRow);
  }

  return tmpMatrix;
}

bublyaev::Matrix bublyaev::part(const CompositeShape &composite)
{
  return part(composite.getShapes(), composite.getCount());
}
