#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

namespace bublyaev
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &newMatrix);
    Matrix(Matrix &&newMatrix);
    ~Matrix() = default;

    Matrix &operator =(const Matrix &newMatrix);
    Matrix &operator =(Matrix &&newMatrix);
    Shape::arrayPointer operator [](size_t index) const;
    bool operator ==(const Matrix &rhs) const;
    bool operator !=(const Matrix &rhs) const;

    size_t getRows() const;
    size_t getColumns() const;
    size_t getLayerSize(size_t layer) const;

    void add(const Shape::pointer &shape, size_t row);

  private:
    Shape::arrayPointer list_;
    size_t rows_;
    size_t columns_;
  };
}

#endif // MATRIX_HPP
