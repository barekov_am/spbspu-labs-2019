#ifndef BASETYPES_HPP
#define BASETYPES_HPP

namespace bublyaev
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    double width;
    double height;
    point_t pos;
  };

  bool isIntersected(const rectangle_t &lhs, const rectangle_t &rhs);
}

#endif // BASETYPES_HPP
