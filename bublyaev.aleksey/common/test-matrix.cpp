#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(matrixTestSuite)

BOOST_AUTO_TEST_CASE(testMatrixCopyConstructor)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{0, 1}, 5);
  bublyaev::CompositeShape testComposite(testCircle);
  bublyaev::Matrix testMatrix = bublyaev::part(testComposite);

  bublyaev::Matrix copyMatrix(testMatrix);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), copyMatrix.getRows());
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(testMatrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(testMatrixCopyOperator)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{6, 3}, 2);
  bublyaev::CompositeShape testComposite(testCircle);
  bublyaev::Matrix testMatrix = bublyaev::part(testComposite);

  bublyaev::Matrix copyMatrix;
  copyMatrix = testMatrix;

  BOOST_CHECK_EQUAL(testMatrix.getRows(), copyMatrix.getRows());
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(testMatrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(testMatrixMoveConstructor)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{10, 1}, 52);
  bublyaev::CompositeShape testComposite(testCircle);
  bublyaev::Matrix testMatrix = bublyaev::part(testComposite);

  bublyaev::Matrix copyMatrix(testMatrix);
  bublyaev::Matrix moveMatrix = std::move(testMatrix);

  BOOST_CHECK_EQUAL(copyMatrix.getRows(), moveMatrix.getRows());
  BOOST_CHECK_EQUAL(copyMatrix.getColumns(), moveMatrix.getColumns());
  BOOST_CHECK_EQUAL(testMatrix.getRows(), 0);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 0);
  BOOST_CHECK(moveMatrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(testMatrixMoveOperator)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{1, 1}, 3);
  bublyaev::CompositeShape testComposite(testCircle);
  bublyaev::Matrix testMatrix = bublyaev::part(testComposite);

  bublyaev::Matrix copyMatrix(testMatrix);
  bublyaev::Matrix moveMatrix;
  moveMatrix = std::move(testMatrix);

  BOOST_CHECK_EQUAL(copyMatrix.getRows(), moveMatrix.getRows());
  BOOST_CHECK_EQUAL(copyMatrix.getColumns(), moveMatrix.getColumns());
  BOOST_CHECK_EQUAL(testMatrix.getRows(), 0);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 0);
  BOOST_CHECK(moveMatrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(testMatrixCreation)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{0, 0}, 3);
  bublyaev::Shape::pointer testRectangle = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{10, 10}, 2, 1);
  bublyaev::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle);
  size_t rows = 1;
  const size_t columns = 2;

  bublyaev::Matrix testMatrix = bublyaev::part(testComposite);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), rows);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), columns);

  bublyaev::Shape::pointer testRectangle2 = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{9, 8}, 5, 4);
  rows++;
  testComposite.add(testRectangle2);
  testMatrix = bublyaev::part(testComposite);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), rows);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), columns);
}

BOOST_AUTO_TEST_CASE(testGetingLayerSizeCorrectness)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{0, 0}, 2);
  bublyaev::Shape::pointer testRectangle1 = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{14, 12}, 2, 1);
  bublyaev::Shape::pointer testRectangle2 = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{0, 0}, 24, 12);
  bublyaev::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle1);
  testComposite.add(testRectangle2);
  const size_t row0 = 2;
  const size_t row1 = 1;
  bublyaev::Matrix testMatrix = bublyaev::part(testComposite);

  BOOST_CHECK_EQUAL(row0, testMatrix.getLayerSize(0));
  BOOST_CHECK_EQUAL(row1, testMatrix.getLayerSize(1));
}

BOOST_AUTO_TEST_CASE(testAddingToMatrix)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{0, 0}, 2);
  bublyaev::Shape::pointer testRectangle1 = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{14, 12}, 2, 1);
  bublyaev::Shape::pointer testRectangle2 = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{0, 0}, 24, 12);
  bublyaev::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle1);
  testComposite.add(testRectangle2);
  size_t rows = 2;
  size_t columns = 2;
  bublyaev::Matrix testMatrix = bublyaev::part(testComposite);

  BOOST_CHECK_EQUAL(rows, testMatrix.getRows());
  BOOST_CHECK_EQUAL(columns, testMatrix.getColumns());

  bublyaev::Shape::pointer testRectangle3 = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{1, 3}, 20, 12);

  testMatrix.add(testRectangle3, rows);
  rows++;

  BOOST_CHECK_EQUAL(rows, testMatrix.getRows());
  BOOST_CHECK_EQUAL(columns, testMatrix.getColumns());

  bublyaev::Shape::pointer testCircle2 = std::make_shared<bublyaev::Circle>(bublyaev::point_t{-100, -40}, 6);

  testMatrix.add(testCircle2, 0);
  columns++;

  BOOST_CHECK_EQUAL(rows, testMatrix.getRows());
  BOOST_CHECK_EQUAL(columns, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(exeptionHandling)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{0, 4}, 3);
  bublyaev::Shape::pointer testRectangle = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{10, 10}, 2, 1);
  bublyaev::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle);

  bublyaev::Matrix testMatrix = bublyaev::part(testComposite);

  BOOST_CHECK_THROW(testMatrix.add(testCircle, 10), std::logic_error);
  BOOST_CHECK_THROW(testMatrix[4], std::logic_error);
  BOOST_CHECK_EQUAL(testMatrix.getLayerSize(12), 0);
}

BOOST_AUTO_TEST_SUITE_END();
