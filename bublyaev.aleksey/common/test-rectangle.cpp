#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>
#include <math.h>

#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(rectangleTestSuite)

const double ACCURACY = 0.00001;

BOOST_AUTO_TEST_CASE(rectangleInvarianceAfterAbsoluteMoving)
{
  const double initialWidth = 2;
  const double initialHeight = 12.5;
  bublyaev::Rectangle testRectangle({1, 2}, initialWidth, initialHeight);
  const double initialArea = testRectangle.getArea();

  testRectangle.move({14, 7.5});
  BOOST_CHECK_CLOSE(initialWidth, testRectangle.getWidth(), ACCURACY);
  BOOST_CHECK_CLOSE(initialHeight, testRectangle.getHeight(), ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(rectangleInvarianceAfterRelativeMoving)
{
  const double initialWidth = 3;
  const double initialHeight = 15.5;
  bublyaev::Rectangle testRectangle({4, 2}, initialWidth, initialHeight);
  const double initialArea = testRectangle.getArea();

  testRectangle.move(0, 14.32);
  BOOST_CHECK_CLOSE(initialWidth, testRectangle.getWidth(), ACCURACY);
  BOOST_CHECK_CLOSE(initialHeight, testRectangle.getHeight(), ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(rectangleInvarianceAfterScaling)
{
  const double multiplier = 0.5;
  bublyaev::Rectangle testRectangle({0, 0}, 5, 12);
  const double initialArea = testRectangle.getArea();
  const bublyaev::rectangle_t initialFrame = testRectangle.getFrameRect();

  testRectangle.scale(multiplier);
  const bublyaev::rectangle_t resultFrame = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(initialArea * multiplier * multiplier, testRectangle.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.width * multiplier, resultFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.height * multiplier, resultFrame.height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testRectangleRotation)
{
  bublyaev::Rectangle testRectangle({6, 4}, 2, 2);
  double initialArea = testRectangle.getArea();
  bublyaev::rectangle_t initialFrame = testRectangle.getFrameRect();
  testRectangle.rotate(45);
  bublyaev::rectangle_t resultFrame = testRectangle.getFrameRect();

  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(resultFrame.width, initialFrame.width * std::sqrt(2), ACCURACY);
  BOOST_CHECK_CLOSE(resultFrame.height, initialFrame.height * std::sqrt(2), ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.x, resultFrame.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.y, resultFrame.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(exceptionHandling)
{
  BOOST_CHECK_THROW(bublyaev::Rectangle({5, 4}, 0, -1), std::invalid_argument);

  bublyaev::Rectangle testRectangle({0, 0}, 4, 6);
  BOOST_CHECK_THROW(testRectangle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END();
