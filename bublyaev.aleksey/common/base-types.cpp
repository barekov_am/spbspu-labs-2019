#include "base-types.hpp"
#include <cmath>

bool bublyaev::isIntersected(const rectangle_t &lhs, const rectangle_t &rhs)
{
  bool widthIntersect = fabs(lhs.pos.x - rhs.pos.x) < ((lhs.width + rhs.width) / 2);
  bool heightIntersect = fabs(lhs.pos.y - rhs.pos.y) < ((lhs.height + rhs.height) / 2);

  return widthIntersect && heightIntersect;
}
