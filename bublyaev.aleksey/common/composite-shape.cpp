#include "composite-shape.hpp"
#include <iostream>
#include <cmath>

bublyaev::CompositeShape::CompositeShape():
  count_(0)
{
}

bublyaev::CompositeShape::CompositeShape(const CompositeShape &newShape):
  count_(newShape.count_),
  shapes_(std::make_unique<pointer[]>(newShape.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i] = newShape.shapes_[i];
  }
}

bublyaev::CompositeShape::CompositeShape(CompositeShape &&newShape):
  count_(newShape.count_),
  shapes_(std::move(newShape.shapes_))
{
  newShape.count_ = 0;
}

bublyaev::CompositeShape::CompositeShape(pointer &shape):
  count_(1),
  shapes_(std::make_unique<pointer[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Invalid shape pointer");
  }

  shapes_[0] = shape;
}

bublyaev::CompositeShape &bublyaev::CompositeShape::operator =(const CompositeShape &newShape)
{
  if (this != &newShape)
  {
    count_ = newShape.count_;
    Shape::arrayPointer tmpShapes(std::make_unique<pointer[]>(count_));
    for (size_t i = 0; i < count_; i++)
    {
      tmpShapes[i] = newShape.shapes_[i];
    }
    shapes_.swap(tmpShapes);
  }

  return * this;
}

bublyaev::CompositeShape &bublyaev::CompositeShape::operator =(CompositeShape &&newShape)
{
  if (this != &newShape)
  {
    count_ = newShape.count_;
    shapes_ = std::move(newShape.shapes_);
    newShape.count_ = 0;
  }
  return * this;
}

bublyaev::Shape::pointer bublyaev::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::logic_error("Index is out of range");
  }

  return shapes_[index];
}

double bublyaev::CompositeShape::getArea() const
{
  double sumArea = 0;
  for (size_t i = 0; i < count_; i++)
  {
    sumArea += shapes_[i]->getArea();
  }
  return sumArea;
}

bublyaev::rectangle_t bublyaev::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  rectangle_t tempRectangle = shapes_[0]->getFrameRect();
  double minX = tempRectangle.pos.x - tempRectangle.width / 2;
  double maxX = tempRectangle.pos.x + tempRectangle.width / 2;
  double minY = tempRectangle.pos.y - tempRectangle.height / 2;
  double maxY = tempRectangle.pos.y + tempRectangle.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    tempRectangle = shapes_[i]->getFrameRect();

    double temp = tempRectangle.pos.x - tempRectangle.width / 2;
    minX = std::min(minX,temp);
    temp = tempRectangle.pos.x + tempRectangle.width / 2;
    maxX = std::max(maxX,temp);
    temp = tempRectangle.pos.y - tempRectangle.height / 2;
    minY = std::min(minY,temp);
    temp = tempRectangle.pos.y + tempRectangle.height / 2;
    maxY = std::max(maxY,temp);
  }

  return rectangle_t{maxX - minX, maxY - minY, point_t{(maxX + minX) / 2, (maxY + minY) / 2}};
}

void bublyaev::CompositeShape::move(const point_t &newPos)
{
  rectangle_t frame = getFrameRect();
  double dx = newPos.x - frame.pos.x;
  double dy = newPos.y - frame.pos.y;

  move(dx, dy);
}

void bublyaev::CompositeShape::move(double dx, double dy)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->move(dx,dy);
  }
}

void bublyaev::CompositeShape::scale(double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("Invalid scale multiplier");
  }

  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  rectangle_t frame = getFrameRect();

  for (size_t i = 0; i < count_; i++)
  {
    rectangle_t currFrame = shapes_[i]->getFrameRect();
    double dx = (currFrame.pos.x - frame.pos.x) * (multiplier - 1);
    double dy = (currFrame.pos.y - frame.pos.y) * (multiplier - 1);
    shapes_[i]->move(dx,dy);
    shapes_[i]->scale(multiplier);
  }
}

void bublyaev::CompositeShape::printInfo() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  std::cout << "Composite Shape: " << std::endl;
  std::cout << "Area: " << getArea() << std::endl;
  rectangle_t frame = getFrameRect();
  std::cout << "Center: (" << frame.pos.x << " , " << frame.pos.y << ")" << std::endl;
  std::cout << "Frame width: " << frame.width << std::endl;
  std::cout << "Frame height: " << frame.height << std::endl;
  std::cout << "Elements' info: " << std::endl;
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->printInfo();
    std::cout << std::endl;
  }
}

void bublyaev::CompositeShape::add(const pointer &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Invalid shape pointer");
  }

  for (size_t i = 0; i < count_; i++)
  {
    if (shape == shapes_[i])
    {
      return;
    }
  }

  arrayPointer newShapes = std::make_unique<pointer[]>(count_ + 1);

  for (size_t i = 0; i < count_; i++)
  {
    newShapes[i] = shapes_[i];
  }

  newShapes[count_] = shape;
  count_++;
  shapes_ = std::move(newShapes);
}

void bublyaev::CompositeShape::remove(size_t num)
{
  if (num >= count_)
  {
    throw std::logic_error("Index is out of range");
  }
  for (size_t i = num; i < (count_ - 1); i++)
  {
    shapes_[i] = shapes_[i+1];
  }
  shapes_[--count_] = nullptr;
}

void bublyaev::CompositeShape::rotate(double angle)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  double radianAngle = angle * M_PI / 180;
  const point_t compCenter = getFrameRect().pos;

  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->rotate(angle);
    const point_t shapeCenter = shapes_[i]->getFrameRect().pos;
    double oldX = shapeCenter.x - compCenter.x;
    double oldY = shapeCenter.y - compCenter.y;
    double newX = oldX * std::cos(radianAngle) - oldY * std::sin(radianAngle);
    double newY = oldX * std::sin(radianAngle) + oldY * std::cos(radianAngle);
    shapes_[i]->move({newX + compCenter.x, newY + compCenter.y});
  }

}

size_t bublyaev::CompositeShape::getCount() const
{
  return count_;
}

size_t bublyaev::CompositeShape::getIndex(const pointer &shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Invalid shape pointer");
  }

  for (size_t i = 0; i < count_; i++)
  {
    if (shape == shapes_[i])
    {
      return i;
    }
  }
  return count_;
}

bublyaev::Shape::arrayPointer bublyaev::CompositeShape::getShapes() const
{
  arrayPointer tmpShapes(std::make_unique<pointer[]>(count_));

  for (size_t i = 0; i < count_; i++)
  {
    tmpShapes[i] = shapes_[i];
  }
  return tmpShapes;
}
