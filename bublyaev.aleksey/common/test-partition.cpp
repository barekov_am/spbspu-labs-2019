#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(partitionTestSuite)

BOOST_AUTO_TEST_CASE(testIntersectedCorrectness)
{
  bublyaev::Circle circle({0, 0}, 3);
  bublyaev::Rectangle rectangle1({10, 10}, 2, 1);
  bublyaev::Rectangle rectangle2({0, 1}, 5, 5);

  BOOST_CHECK(bublyaev::isIntersected(circle.getFrameRect(), rectangle2.getFrameRect()));
  BOOST_CHECK(!(bublyaev::isIntersected(circle.getFrameRect(), rectangle1.getFrameRect())));
}

BOOST_AUTO_TEST_CASE(testPartitionCorrectness)
{
  bublyaev::Shape::pointer testCircle1 = std::make_shared<bublyaev::Circle>(bublyaev::point_t{0, 0}, 3);
  bublyaev::Shape::pointer testRectangle1 = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{10, 10}, 2, 1);
  bublyaev::Shape::pointer testCircle2 = std::make_shared<bublyaev::Circle>(bublyaev::point_t{0, 1}, 2);
  bublyaev::Shape::pointer testRectangle2 = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{11, 10}, 2, 3);
  bublyaev::Shape::pointer testRectangle3 = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{-100, -10}, 5, 5);
  bublyaev::Shape::pointer testRectangle4 = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{3, 0}, 6, 8);
  bublyaev::CompositeShape testComposite(testCircle1);
  testComposite.add(testRectangle1);
  testComposite.add(testCircle2);
  testComposite.add(testRectangle2);
  testComposite.add(testRectangle3);
  testComposite.add(testRectangle4);

  bublyaev::Matrix testMatrix = bublyaev::part(testComposite);

  const size_t rows = 3;
  const size_t columns = 3;

  BOOST_CHECK_EQUAL(rows, testMatrix.getRows());
  BOOST_CHECK_EQUAL(columns, testMatrix.getColumns());

  BOOST_CHECK(testMatrix[0][0] == testCircle1);
  BOOST_CHECK(testMatrix[0][1] == testRectangle1);
  BOOST_CHECK(testMatrix[0][2] == testRectangle3);
  BOOST_CHECK(testMatrix[1][0] == testCircle2);
  BOOST_CHECK(testMatrix[1][1] == testRectangle2);
  BOOST_CHECK(testMatrix[2][0] == testRectangle4);
}

BOOST_AUTO_TEST_SUITE_END();
