#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>
#include <math.h>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(compositeTestSuite)

const double ACCURACY = 0.00001;

BOOST_AUTO_TEST_CASE(testCompositeShapeCopyConstructor)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{-2.5, 3}, 6);
  bublyaev::Shape::pointer testRectangle = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{5, 0}, 10, 4.5);
  bublyaev::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle);
  const double initialArea = testComposite.getArea();
  const bublyaev::rectangle_t initialFrame = testComposite.getFrameRect();

  bublyaev::CompositeShape testCopyComposite(testComposite);
  const bublyaev::rectangle_t resultFrame = testCopyComposite.getFrameRect();

  BOOST_CHECK_EQUAL(testComposite.getCount(), testCopyComposite.getCount());
  BOOST_CHECK_CLOSE(initialArea, testCopyComposite.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.width, resultFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.height, resultFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.x, resultFrame.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.y, resultFrame.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeCopyOperator)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{23.5, 3}, 8);
  bublyaev::Shape::pointer testRectangle = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{5, -6}, 13, 4.2);
  bublyaev::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle);

  const double initialArea = testComposite.getArea();
  const bublyaev::rectangle_t initialFrame = testComposite.getFrameRect();

  bublyaev::CompositeShape testCopyComposite;
  testCopyComposite = testComposite;
  const bublyaev::rectangle_t resultFrame = testCopyComposite.getFrameRect();

  BOOST_CHECK_EQUAL(testComposite.getCount(), testCopyComposite.getCount());
  BOOST_CHECK_CLOSE(initialArea, testCopyComposite.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.width, resultFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.height, resultFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.x, resultFrame.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.y, resultFrame.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeMoveConstructor)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{8, 3}, 13.6);
  bublyaev::Shape::pointer testRectangle = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{5, 6}, 42, 5);
  bublyaev::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle);
  const size_t initialSize = testComposite.getCount();
  const double initialArea = testComposite.getArea();
  const bublyaev::rectangle_t initialFrame = testComposite.getFrameRect();

  bublyaev::CompositeShape testMoveComposite = std::move(testComposite);
  const bublyaev::rectangle_t resultFrame = testMoveComposite.getFrameRect();

  BOOST_CHECK_EQUAL(initialSize, testMoveComposite.getCount());
  BOOST_CHECK_CLOSE(initialArea, testMoveComposite.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.width, resultFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.height, resultFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.x, resultFrame.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.y, resultFrame.pos.y, ACCURACY);
  BOOST_CHECK_EQUAL(testComposite.getCount(), 0);
  BOOST_CHECK_CLOSE(testComposite.getArea(), 0, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeMoveOperator)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{8, 7}, 1.6);
  bublyaev::Shape::pointer testRectangle = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{5, 3.6}, 2, 53);
  bublyaev::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle);
  const size_t initialSize = testComposite.getCount();
  const double initialArea = testComposite.getArea();
  const bublyaev::rectangle_t initialFrame = testComposite.getFrameRect();

  bublyaev::CompositeShape testMoveComposite;
  testMoveComposite = std::move(testComposite);
  const bublyaev::rectangle_t resultFrame = testMoveComposite.getFrameRect();

  BOOST_CHECK_EQUAL(initialSize, testMoveComposite.getCount());
  BOOST_CHECK_CLOSE(initialArea, testMoveComposite.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.width, resultFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.height, resultFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.x, resultFrame.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.y, resultFrame.pos.y, ACCURACY);
  BOOST_CHECK_EQUAL(testComposite.getCount(), 0);
  BOOST_CHECK_CLOSE(testComposite.getArea(), 0, ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeInvatianceAfterAbsoluteMoving)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{-1.5, 2}, 5);
  bublyaev::Shape::pointer testRectangle = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{5, 4}, 12, 3.5);
  bublyaev::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle);
  const double initialArea = testComposite.getArea();
  const bublyaev::rectangle_t initialFrame = testComposite.getFrameRect();
  const double newX = 1;
  const double newY = 6;
  testComposite.move({newX, newY});
  const bublyaev::rectangle_t resultFrame = testComposite.getFrameRect();

  BOOST_CHECK_CLOSE(initialFrame.width, resultFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.height, resultFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testComposite.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(resultFrame.pos.x, newX, ACCURACY);
  BOOST_CHECK_CLOSE(resultFrame.pos.y, newY, ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeInvatianceAfterRelativeMoving)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{-15, 2.6}, 12.5);
  bublyaev::Shape::pointer testRectangle = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{0, 4}, 10, 6.3);
  bublyaev::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle);
  const double initialArea = testComposite.getArea();
  const bublyaev::rectangle_t initialFrame = testComposite.getFrameRect();
  const double dx = -6;
  const double dy = 0;
  testComposite.move(dx, dy);
  const bublyaev::rectangle_t resultFrame = testComposite.getFrameRect();

  BOOST_CHECK_CLOSE(initialFrame.width, resultFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.height, resultFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testComposite.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(resultFrame.pos.x, initialFrame.pos.x + dx, ACCURACY);
  BOOST_CHECK_CLOSE(resultFrame.pos.y, initialFrame.pos.y + dy, ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeInvarianceAfterScaling)
{
  double multiplier = 4;
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{-15, 0}, 53);
  bublyaev::Shape::pointer testRectangle = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{6, 4}, 10, 5);
  bublyaev::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle);
  double initialArea = testComposite.getArea();
  bublyaev::rectangle_t initialFrame = testComposite.getFrameRect();
  testComposite.scale(multiplier);
  bublyaev::rectangle_t resultFrame = testComposite.getFrameRect();

  BOOST_CHECK_CLOSE(initialArea * multiplier * multiplier, testComposite.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.width * multiplier, resultFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.height * multiplier, resultFrame.height, ACCURACY);

  multiplier = 0.5;
  initialArea = testComposite.getArea();
  initialFrame = testComposite.getFrameRect();
  testComposite.scale(multiplier);
  resultFrame = testComposite.getFrameRect();

  BOOST_CHECK_CLOSE(initialArea * multiplier * multiplier, testComposite.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.width * multiplier, resultFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.height * multiplier, resultFrame.height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeRotation)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{5, 4}, 8);
  bublyaev::Shape::pointer testRectangle = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{10, 6}, 5, 2);
  bublyaev::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle);
  double initialArea = testComposite.getArea();
  bublyaev::rectangle_t initialFrame = testComposite.getFrameRect();
  double angle = 360;
  testComposite.rotate(angle);
  bublyaev::rectangle_t resultFrame = testComposite.getFrameRect();

  BOOST_CHECK_CLOSE(initialArea, testComposite.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.width, resultFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.height, resultFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.x, resultFrame.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.y, resultFrame.pos.y, ACCURACY);

  angle = -90;
  testComposite.rotate(angle);
  resultFrame = testComposite.getFrameRect();

  BOOST_CHECK_CLOSE(initialArea, testComposite.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.width, resultFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.height, resultFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.x, resultFrame.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.y, resultFrame.pos.y, ACCURACY);

  angle = 180;
  testComposite.rotate(angle);
  resultFrame = testComposite.getFrameRect();

  BOOST_CHECK_CLOSE(initialArea, testComposite.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.width, resultFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.height, resultFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.x, resultFrame.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.y, resultFrame.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(exceptionHandling)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{-15, 0}, 53);
  bublyaev::Shape::pointer testRectangle = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{6, 4}, 10, 5);
  bublyaev::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle);

  BOOST_CHECK_THROW(testComposite.scale(-3), std::invalid_argument);
  bublyaev::Shape::pointer nullShape;

  BOOST_CHECK_THROW(bublyaev::CompositeShape{nullShape}, std::invalid_argument);
  BOOST_CHECK_THROW(testComposite.add(nullShape), std::invalid_argument);
  BOOST_CHECK_THROW(testComposite.getIndex(nullShape), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(logicErrorsHandling)
{
  bublyaev::Shape::pointer testCircle = std::make_shared<bublyaev::Circle>(bublyaev::point_t{-10, 0}, 5);
  bublyaev::Shape::pointer testRectangle = std::make_shared<bublyaev::Rectangle>(bublyaev::point_t{6, 4}, 10, 5);
  bublyaev::CompositeShape testComposite;

  BOOST_CHECK_THROW(testComposite.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(testComposite.move(4, 7), std::logic_error);
  BOOST_CHECK_THROW(testComposite.move({4, 7}), std::logic_error);
  BOOST_CHECK_THROW(testComposite.scale(4), std::logic_error);
  BOOST_CHECK_THROW(testComposite.printInfo(), std::logic_error);
  BOOST_CHECK_THROW(testComposite.rotate(56), std::logic_error);

  testComposite.add(testRectangle);

  BOOST_CHECK_EQUAL(testComposite.getIndex(testCircle), testComposite.getCount());
  BOOST_CHECK_THROW(testComposite[7], std::logic_error);
  BOOST_CHECK_THROW(testComposite.remove(6), std::logic_error);
}

BOOST_AUTO_TEST_SUITE_END();
