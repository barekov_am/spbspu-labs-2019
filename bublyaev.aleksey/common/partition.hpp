#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace bublyaev
{
  Matrix part(const Shape::arrayPointer &array, size_t size);
  Matrix part(const CompositeShape &composite);
}

#endif // PARTITION_HPP
