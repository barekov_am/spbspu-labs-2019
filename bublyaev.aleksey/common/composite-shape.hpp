#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include <iostream>
#include "shape.hpp"

namespace bublyaev
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &newShape);
    CompositeShape(CompositeShape &&newShape);
    CompositeShape(pointer &shape);

    CompositeShape &operator =(const CompositeShape &newShape);
    CompositeShape &operator =(CompositeShape &&newShape);
    pointer operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &newPos) override;
    void move(double dx, double dy) override;
    void scale(double multiplier) override;
    void printInfo() const override;
    void add(const pointer &shape);
    void remove(size_t num);
    void rotate(double angle) override;
    size_t getCount() const;
    size_t getIndex(const pointer &shape) const;
    Shape::arrayPointer getShapes() const;
  private:
    size_t count_;
    arrayPointer shapes_;
  };
}

#endif // COMPOSITESHAPE_HPP
