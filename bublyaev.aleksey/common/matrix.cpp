#include "matrix.hpp"

bublyaev::Matrix::Matrix():
  rows_(0),
  columns_(0)
{
}

bublyaev::Matrix::Matrix(const Matrix &newMatrix):
  list_(std::make_unique<Shape::pointer[]>(newMatrix.rows_ * newMatrix.columns_)),
  rows_(newMatrix.rows_),
  columns_(newMatrix.columns_)
{
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    list_[i] = newMatrix.list_[i];
  }
}

bublyaev::Matrix::Matrix(Matrix &&newMatrix):
  list_(std::move(newMatrix.list_)),
  rows_(newMatrix.rows_),
  columns_(newMatrix.columns_)
{
  newMatrix.rows_ = 0;
  newMatrix.columns_ = 0;
}

bublyaev::Matrix &bublyaev::Matrix::operator =(const Matrix &newMatrix)
{
  if (this != &newMatrix)
  {
    Shape::arrayPointer tmpMatrix(std::make_unique<Shape::pointer[]>(newMatrix.rows_ * newMatrix.columns_));
    for (size_t i = 0; i < (newMatrix.rows_ * newMatrix.columns_); i++)
    {
      tmpMatrix[i] = newMatrix.list_[i];
    }

    list_.swap(tmpMatrix);
    rows_ = newMatrix.rows_;
    columns_ = newMatrix.columns_;
  }

  return * this;
}

bublyaev::Matrix &bublyaev::Matrix::operator =(Matrix &&newMatrix)
{
  if (this != &newMatrix)
  {
    rows_ = newMatrix.rows_;
    columns_ = newMatrix.columns_;
    list_ = std::move(newMatrix.list_);
    newMatrix.rows_ = 0;
    newMatrix.columns_ = 0;
  }

  return * this;
}

bublyaev::Shape::arrayPointer bublyaev::Matrix::operator [](size_t index) const
{
  if (index >= rows_)
  {
    throw std::logic_error("Invalid row index");
  }

  const size_t columns = getLayerSize(index);

  Shape::arrayPointer tmpMatrix(std::make_unique<Shape::pointer[]>(columns));
  for (size_t i = 0; i < columns; i++)
  {
    tmpMatrix[i] = list_[index * columns_ + i];
  }

  return tmpMatrix;
}

bool bublyaev::Matrix::operator ==(const Matrix &rhs) const
{
  if ((rows_ != rhs.rows_) || (columns_ != rhs.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (list_[i] != rhs.list_[i])
    {
      return false;
    }
  }

  return true;
}

bool bublyaev::Matrix::operator !=(const Matrix &rhs) const
{
  return !(* this == rhs);
}

size_t bublyaev::Matrix::getRows() const
{
  return rows_;
}

size_t bublyaev::Matrix::getColumns() const
{
  return columns_;
}

size_t bublyaev::Matrix::getLayerSize(size_t layer) const
{
  if (layer >= rows_)
  {
    return 0;
  }

  for (size_t i = 0; i < columns_; i++)
  {
    if (list_[layer * columns_ + i] == nullptr)
    {
      return i;
    }
  }

  return columns_;
}

void bublyaev::Matrix::add(const Shape::pointer &shape, size_t row)
{
  if (row > rows_)
  {
    throw std::logic_error("invalid row index");
  }

  size_t tmpRows = (row == rows_) ? (rows_ + 1) : rows_;
  size_t tmpColumns = (getLayerSize(row) == columns_) ? (columns_ + 1) : columns_;
  size_t column = (row < rows_) ? getLayerSize(row) : 0;

  if ((tmpRows == rows_) && (tmpColumns == columns_))
  {
    list_[row * columns_ + column] = shape;
  }
  else
  {
    Shape::arrayPointer tmpMatrix(std::make_unique<Shape::pointer[]>(tmpRows * tmpColumns));

    for (size_t i = 0; i < tmpRows; i++)
    {
      for (size_t j = 0; j < tmpColumns; j++)
      {
        if ((rows_ == i) || (columns_ == j))
        {
          tmpMatrix[i * tmpColumns + j] = nullptr;
        }
        else
        {
          tmpMatrix[i * tmpColumns + j] = list_[i * columns_ + j];
        }
      }
    }

    tmpMatrix[row * tmpColumns + column] = shape;
    list_.swap(tmpMatrix);
    rows_ = tmpRows;
    columns_ = tmpColumns;
  }
}
