#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(circleTestSuite)

const double ACCURACY = 0.00001;

BOOST_AUTO_TEST_CASE(circleInvatianceAfterAbsoluteMoving)
{
  const double initialRadius = 5.4;
  bublyaev::Circle testCircle({0, 0}, initialRadius);
  const double initialArea = testCircle.getArea();

  testCircle.move({1, 6});
  BOOST_CHECK_CLOSE(initialRadius, testCircle.getRadius(), ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testCircle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(circleInvatianceAfterRelativeMoving)
{
  const double initialRadius = 5.4;
  bublyaev::Circle testCircle({1, 5}, initialRadius);
  const double initialArea = testCircle.getArea();

  testCircle.move(4, 0.6);
  BOOST_CHECK_CLOSE(initialRadius, testCircle.getRadius(), ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testCircle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(circleInvarianceAfterScaling)
{
  const double multiplier = 4;
  bublyaev::Circle testCircle({1, 2}, 7);
  const double initialArea = testCircle.getArea();
  const bublyaev::rectangle_t initialFrame = testCircle.getFrameRect();

  testCircle.scale(multiplier);
  const bublyaev::rectangle_t resultFrame = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(initialArea * multiplier * multiplier, testCircle.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.width * multiplier, resultFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.height * multiplier, resultFrame.height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testCircleRotation)
{
  bublyaev::Circle testCircle({6, 4}, 10);
  double initialArea = testCircle.getArea();
  bublyaev::rectangle_t initialFrame = testCircle.getFrameRect();
  testCircle.rotate(45);
  bublyaev::rectangle_t resultFrame = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(initialArea, testCircle.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.width, resultFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.height, resultFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.x, resultFrame.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(initialFrame.pos.y, resultFrame.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(exceptionHandling)
{
  BOOST_CHECK_THROW(bublyaev::Circle({1, 5}, -4), std::invalid_argument);

  bublyaev::Circle testCircle({1, 7}, 4);
  BOOST_CHECK_THROW(testCircle.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END();
