#include <iostream>

#include "parser.hpp"

const size_t MIN_WIDTH = 25;

int main(int argc, char * argv[])
{
  try {
    if ((argc > 3) || (argc == 2))
    {
      std::cerr << "Incorrect number of arquments.";
      return 1;
    }

    size_t width = 40;

    if (argc == 3)
    {
      width = std::stoul(argv[2]);
      if (std::string(argv[1]) != "--line-width")
      {
        std::cerr << "Incorrect argument name.";
        return 1;
      }
      if (width < MIN_WIDTH)
      {
        std::cerr << "Incorrect width.";
        return 1;
      }
    }

    Parser parser(width);
    parser.parseText();
    parser.formatText();
    parser.printText();
  }
  catch (const std::exception &ex)
  {
    std::cerr << ex.what();
    return 1;
  }

  return 0;
}
