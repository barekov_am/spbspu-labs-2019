#include "parser.hpp"
#include "iterator"

const size_t MAX_ELEMENT_LENGTH = 20;

Parser::Parser(size_t size):
  maxLength_(size)
{
}

void Parser::parseText()
{
  std::istream &in = std::cin;
  while (!in.eof())
  {
    char current = static_cast<char>(in.peek());
    if (std::isspace(current))
    {
      skipSpaces(in);
    }
    else if (std::isalpha(current))
    {
      readWord(in);
    }
    else if (std::isdigit(current) || (current == '+') || (current == '-'))
    {
      readNumber(in);
    }
    else if (std::ispunct(current))
    {
      if (elements_.empty())
      {
        throw std::invalid_argument("Wrong punctuation declaration: punctuation symbol at the beginning of the text.");
      }
      readPunct(in);
    }
  }
}

void Parser::formatText()
{
  if (elements_.empty())
  {
    return;
  }
  std::string currentLine;
  currentLine.append(elements_.front().data);
  size_t lineLength = currentLine.size();

  for (auto i = std::next(elements_.begin()); i != elements_.end(); i++)
  {
    element_t currentElem = *i;
    element_t previousElem = *std::prev(i);
    size_t currentLength = currentElem.data.size();
    size_t movingLength = 0;
    size_t space = 0;

    if (currentElem.type == elementType_t::PUNCT)
    {
      space = 0;
    }
    else
    {
      space = 1;
    }

    if ((currentLength + lineLength + space) > maxLength_)
    {
      if (currentElem.type == elementType_t::PUNCT)
      {
        movingLength = previousElem.data.size();
        std::string tmpString = currentLine.substr(lineLength - movingLength);
        currentLine.erase(lineLength - movingLength - 1);
        formattedText_.push_back(currentLine);
        currentLine.clear();
        currentLine.append(tmpString + currentElem.data);
        lineLength = currentLine.size();
      }
      else if (currentElem.type == elementType_t::DASH)
      {
        if (previousElem.type == elementType_t::PUNCT)
        {
          movingLength = std::prev(std::prev(i))->data.size() + 1;
        }
        else
        {
          movingLength = previousElem.data.size();
        }
        std::string tmpString = currentLine.substr(lineLength - movingLength);
        currentLine.erase(lineLength - movingLength - 1);
        formattedText_.push_back(currentLine);
        currentLine.clear();
        currentLine.append(tmpString + " " + currentElem.data);
        lineLength = currentLine.size();
      }
      else
      {
        formattedText_.push_back(currentLine);
        currentLine.clear();
        currentLine.append(currentElem.data);
        lineLength = currentLine.size();
      }
    }
    else
    {
      if (currentElem.type == elementType_t::PUNCT)
      {
        currentLine.append(currentElem.data);
        lineLength = currentLine.size();
      }
      else
      {
        currentLine.append(" " + currentElem.data);
        lineLength = currentLine.size();
      }
    }
  }
  formattedText_.push_back(currentLine);
}

void Parser::printText() const
{
  std::copy(formattedText_.begin(), formattedText_.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
}

void Parser::readWord(std::istream &in)
{
  std::string tmpString;
  char current;
  while (std::isalpha(in.peek()) || (in.peek() == '-'))
  {
    in.get(current);
    if ((current == '-') && tmpString.empty())
    {
      throw std::invalid_argument("Wrong word declaration: hyphen at the beginning.");
    }
    if ((current == '-') && (in.peek() == '-'))
    {
      throw std::invalid_argument("Wrong word declaration: consecutive hyphens.");
    }
    tmpString.push_back(current);
  }

  if (tmpString.size() > MAX_ELEMENT_LENGTH)
  {
    throw std::invalid_argument("Wrong word declaration: word size is more than 20.");
  }
  elements_.push_back({ tmpString, elementType_t::WORD });
}

void Parser::readNumber(std::istream &in)
{
  std::string tmpString;
  char current;
  char point = std::use_facet<std::numpunct<char>>(in.getloc()).decimal_point();
  while (std::isdigit(in.peek()) || (in.peek() == point) || (in.peek() == '+') || (in.peek() == '-'))
  {
    in.get(current);
    if ((current == point) && tmpString.empty())
    {
      throw std::invalid_argument("Wrong number declaration: decimal point at the beginning.");
    }
    if ((current == '-') && (in.peek() == '-'))
    {
      in.unget();
      readDash(in);
      return;
    }
    if (!std::isdigit(current) && ((tmpString.back() == '-') || (tmpString.back() == '+') || (tmpString.back() == point)))
    {
      throw std::invalid_argument("Wrong number declaration: consecutive non-digit symbols.");
    }
    tmpString.push_back(current);
  }

  if (tmpString.size() > MAX_ELEMENT_LENGTH)
  {
    throw std::invalid_argument("Wrong number declaration: number size is more than 20.");
  }
  elements_.push_back({ tmpString, elementType_t::NUMBER });
}

void Parser::readPunct(std::istream &in)
{
  std::string tmpString;
  char current;
  if (in.peek() == '-')
  {
    readDash(in);
  }
  else
  {
    in.get(current);
    tmpString.push_back(current);
    elements_.push_back({ tmpString, elementType_t::PUNCT });
  }
  skipSpaces(in);
  if ((tmpString == ",") && (in.peek() == '-'))
  {
    readDash(in);
  }
  skipSpaces(in);
  if (std::ispunct(in.peek()))
  {
    throw std::invalid_argument("Wrong punctuation declaration: consequtive punctuation symbols.");
  }
}

void Parser::readDash(std::istream &in)
{
  if (elements_.empty())
  {
    throw std::invalid_argument("Wrong dash declaration: dash at the beginning of the text.");
  }
  std::string tmpString;
  char current;
  while (in.peek() == '-')
  {
    in.get(current);
    tmpString.push_back(current);
  }
  if (tmpString.size() != 3)
  {
    throw std::invalid_argument("Wrong dash declaration: dash must be 3 symbols long.");
  }
  elements_.push_back({ tmpString, elementType_t::DASH });
}

void Parser::skipSpaces(std::istream &in)
{
  while (std::isspace(in.peek()))
  {
    in.get();
  }
}
