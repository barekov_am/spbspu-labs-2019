#ifndef PARSER_HPP
#define PARSER_HPP

#include <iostream>
#include <string>
#include <vector>

enum elementType_t
{
  NUMBER,
  WORD,
  PUNCT,
  DASH
};

struct element_t
{
  std::string data;
  elementType_t type;
};

class Parser
{
public:
  Parser(std::size_t size = 40);

  void parseText();
  void formatText();
  void printText() const;
private:
  std::size_t maxLength_;
  std::vector<element_t> elements_;
  std::vector<std::string> formattedText_;

  void readWord(std::istream &in);
  void readNumber(std::istream &in);
  void readPunct(std::istream &in);
  void readDash(std::istream &in);
  void skipSpaces(std::istream &in);
};

#endif // PARSER_HPP
