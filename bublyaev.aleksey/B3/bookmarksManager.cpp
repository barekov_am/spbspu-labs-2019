#include "bookmarksManager.hpp"
#include <iostream>
#include <algorithm>
#include <functional>

BookmarksManager::BookmarksManager()
{
  bookmarks_["current"] = records_.begin();
}

void BookmarksManager::add(const PhoneBook::record_t &record)
{
  records_.pushBack(record);
  if (std::next(records_.begin()) == records_.end())
  {
    bookmarks_["current"] = records_.begin();
  }
}

void BookmarksManager::store(const std::string &bookmark, const std::string &name)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    bookmarks_.emplace(name, iter->second);
  }
}

void BookmarksManager::insert(const std::string &bookmark, const PhoneBook::record_t &record, insertPosition point)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {
    if (iter->second == records_.end())
    {
      add(record);
    }
    auto position = (point == insertPosition::after) ? std::next(iter->second) : iter->second;

    if (position == records_.end())
    {
      add(record);
    }
    records_.insert(position, record);
  }
}

void BookmarksManager::remove(const std::string &bookmark)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    auto deleteIter = iter->second;

    auto search = [&](auto & it)
    {
      if (it.second == deleteIter)
      {
        if (std::next(it.second) == records_.end())
        {
          it.second = records_.prev(deleteIter);
        }
        else
        {
          it.second = records_.next(deleteIter);
        }
      }
    };

    std::for_each(bookmarks_.begin(), bookmarks_.end(), search);

    records_.remove(deleteIter);
  }
}

void BookmarksManager::show(const std::string &bookmark)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    if (records_.empty())
    {
      std::cout << "<EMPTY>\n";
      return;
    }

    records_.showCurrentRecord(iter->second);
  }
}

void BookmarksManager::move(const std::string &bookmark, int n)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    iter->second = records_.move(iter->second, n);
  }
}

void BookmarksManager::move(const std::string &bookmark, movePosition position)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    switch (position)
    {
    case movePosition::first:
    {
      iter->second = records_.begin();
      break;
    }
    case movePosition::last:
    {
      iter->second = records_.prev(records_.end());
      break;
    }
    }
  }
}

void BookmarksManager::nextRecord(const std::string &bookmark)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    iter->second = records_.next(iter->second);
  }
}

void BookmarksManager::prevRecord(const std::string &bookmark)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    iter->second = records_.prev(iter->second);
  }
}

BookmarksManager::bookmarkIterator BookmarksManager::getBookmarkIterator(const std::string &bookmark)
{
  bookmarkIterator iter = bookmarks_.find(bookmark);
  if (iter != bookmarks_.end())
  {
    return iter;
  }

  std::cout << "<INVALID BOOKMARK>\n";
  return bookmarks_.end();
}
