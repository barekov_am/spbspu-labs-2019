#include "factorialIterator.hpp"

FactorialIterator::FactorialIterator():
  value_(1),
  index_(1)
{
}

FactorialIterator::FactorialIterator(int index):
  value_(getValue(index)),
  index_(index)
{
}

FactorialIterator::reference FactorialIterator::operator *()
{
  return value_;
}

FactorialIterator::pointer FactorialIterator::operator ->()
{
  return &value_;
}

FactorialIterator &FactorialIterator::operator ++()
{
  ++index_;
  value_ *= index_;
  return * this;
}

FactorialIterator FactorialIterator::operator ++(int)
{
  FactorialIterator tmp = *this;
  ++(*this);
  return tmp;
}

FactorialIterator &FactorialIterator::operator --()
{
  if (index_ > 1)
  {
    value_ /= index_;
    --index_;
  }
  return * this;
}

FactorialIterator FactorialIterator::operator --(int)
{
  FactorialIterator tmp = *this;
  --(*this);
  return tmp;
}

bool FactorialIterator::operator ==(FactorialIterator iter) const
{
  return (value_ == iter.value_) && (index_ == iter.index_);
}

bool FactorialIterator::operator !=(FactorialIterator iter) const
{
  return !(*this == iter);
}


long long FactorialIterator::getValue(int index) const
{
  return (index <= 1) ? 1 : (index * getValue(index - 1));
}

FactorialIterator FactorialContainer::begin()
{
  return FactorialIterator(1);
}

FactorialIterator FactorialContainer::end()
{
  return FactorialIterator(11);
}

