#include <iostream>
#include <sstream>
#include <cstring>
#include <functional>
#include <algorithm>

#include "commands.hpp"
#include "bookmarksManager.hpp"

struct command_t
{
  const char * name;
  std::function<void(BookmarksManager &, std::stringstream &)> execute;
};

void firstTask()
{
  BookmarksManager manager;
  std::string line;

  while (getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input failed");
    }

    std::stringstream ss(line);
    std::string command;
    ss >> command;

    command_t commands[] =
    {
      {"add", &executeAdd},
      {"store", &executeStore},
      {"insert", &executeInsert},
      {"delete", &executeDelete},
      {"show", &executeShow},
      {"move", &executeMove}
    };
    auto predicate = [&](const command_t &com) { return com.name == command; };
    auto action = std::find_if(std::begin(commands), std::end(commands), predicate);

    if (action != std::end(commands))
    {
      action->execute(manager, ss);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
      return;
    }
  }
}
