#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <string>

class BookmarksManager;

void executeAdd(BookmarksManager &manager, std::stringstream &ss);
void executeStore(BookmarksManager &manager, std::stringstream &ss);
void executeInsert(BookmarksManager &manager, std::stringstream &ss);
void executeDelete(BookmarksManager &manager, std::stringstream &ss);
void executeShow(BookmarksManager &manager, std::stringstream &ss);
void executeMove(BookmarksManager &manager, std::stringstream &ss);

#endif // COMMANDS_HPP
