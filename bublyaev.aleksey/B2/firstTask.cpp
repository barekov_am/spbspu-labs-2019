#include <iostream>
#include <sstream>
#include <functional>
#include <cstring>
#include "commands.hpp"
#include "priorityQueue.hpp"

void firstTask()
{
  QueueWithPriority<std::string> queue;
  std::string line;

  while (getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input failed");
    }

    std::stringstream ss(line);
    std::string command;
    ss >> command;

    if (strcmp(&command[0], "add") == 0)
    {
      executeAdd(queue, ss);
    }
    else if (strcmp(&command[0], "get") == 0)
    {
       executeGet(queue, ss);
    }
    else if (strcmp(&command[0], "accelerate") == 0)
    {
      executeAccelerate(queue, ss);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
