#include <iostream>
#include <list>

void secondTask()
{
  std::list<int> list;
  size_t size = 0;
  int value = 0;
  const int minValue = 1;
  const int maxValue = 20;
  const size_t maxSize = 20;
  while (std::cin && !(std::cin >> value).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input failed");
    }

    if (value < minValue || value > maxValue)
    {
      throw std::invalid_argument("Invalid Argument. Value should be from 1 to 20.");
    }

    size++;
    if (size > maxSize)
    {
      throw std::invalid_argument("You can add only 20 elements.");
    }

    list.push_back(value);
  }

  if (list.empty())
  {
    return;
  }

  auto frontIter = list.begin();
  auto backIter = std::prev(list.end());
  for (size_t i = 0; i < size / 2; i++)
  {
    std::cout << *frontIter++ << " " << *backIter-- << " ";
  }
  if (frontIter == backIter)
  {
    std::cout << *frontIter;
  }
  std::cout << "\n";
}
