#ifndef PRIORITYQUEUE_HPP
#define PRIORITYQUEUE_HPP

#include <list>
#include <functional>

enum class ElementPriority
{
  LOW,
  NORMAL,
  HIGH
};

template <typename T>
class QueueWithPriority
{
public:
  using handlerType = std::function<void(const T &)>;

  void putElement(const T &element, ElementPriority proirity);

  void pullFrontElement(handlerType handler);
  void accelerate();

  bool empty() const;
private:
  std::list<T> low_;
  std::list<T> normal_;
  std::list<T> high_;
};

template <typename T>
void QueueWithPriority<T>::putElement(const T &element, ElementPriority proirity)
{
  switch (proirity)
  {
  case ElementPriority::LOW:
  {
    low_.push_back(element);
    break;
  }

  case ElementPriority::NORMAL:
  {
    normal_.push_back(element);
    break;
  }

  case ElementPriority::HIGH:
  {
    high_.push_back(element);
    break;
  }
  }
}

template <typename T>
void QueueWithPriority<T>::pullFrontElement(handlerType handler)
{
  if (!high_.empty())
  {
    handler(high_.front());
    high_.pop_front();
    return;
  }
  if (!normal_.empty())
  {
    handler(normal_.front());
    normal_.pop_front();
    return;
  }
  if (!low_.empty())
  {
    handler(low_.front());
    low_.pop_front();
    return;
  }
  throw std::runtime_error("Queue is empty.");
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  high_.splice(high_.end(), low_);
}

template <typename T>
bool QueueWithPriority<T>::empty() const
{
  return high_.empty() && normal_.empty() && low_.empty();
}


#endif // PRIORITYQUEUE_HPP
