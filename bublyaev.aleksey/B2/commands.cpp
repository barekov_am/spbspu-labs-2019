#include "commands.hpp"
#include <iostream>
#include <sstream>
#include <cstring>

#include "priorityQueue.hpp"

ElementPriority getPriority(const char * priority)
{
  if (std::strcmp(priority, "high") == 0)
  {
    return ElementPriority::HIGH;
  }
  if (std::strcmp(priority, "normal") == 0)
  {
    return ElementPriority::NORMAL;
  }
  if (std::strcmp(priority, "low") == 0)
  {
    return ElementPriority::LOW;
  }
  throw std::invalid_argument("Wrong priority.");
}

void executeAdd(QueueWithPriority<std::string> &queue, std::stringstream &ss)
{
  try
  {
    std::string priorityData;
    std::string data;
    ss >> std::ws >> priorityData;
    std::getline(ss >> std::ws, data);

    ElementPriority priority = getPriority(priorityData.c_str());

    if (data.empty())
    {
      std::cout << "<INVALID COMMAND>\n";
      return;
    }
    queue.putElement(data, priority);
  }
  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}

void executeGet(QueueWithPriority<std::string> &queue, std::stringstream &ss)
{
  std::string line;
  ss >> std::ws >> line;

  if (!line.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (queue.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }
  auto printElement = [](const std::string &element) { std::cout << element << "\n"; };
  queue.pullFrontElement(printElement);
}

void executeAccelerate(QueueWithPriority<std::string> &queue, std::stringstream &ss)
{
  std::string line;
  ss >> std::ws >> line;

  if (!line.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.accelerate();
}
