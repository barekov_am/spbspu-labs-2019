#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <string>

template <typename T>
class QueueWithPriority;

void executeAdd(QueueWithPriority<std::string> &queue, std::stringstream &ss);
void executeGet(QueueWithPriority<std::string> &queue, std::stringstream &ss);
void executeAccelerate(QueueWithPriority<std::string> &queue, std::stringstream &ss);

#endif // COMMANDS_HPP
