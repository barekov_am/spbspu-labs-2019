#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(testSircle)

BOOST_AUTO_TEST_CASE(invariableOfParametersAfterMoving)
{
  baboshina::Circle circle({ 3, 4 }, 6);
  baboshina::rectangle_t frameRect = circle.getFrameRect();
  double area = circle.getArea();
  circle.move({ 2, -3 });
  BOOST_CHECK_CLOSE(frameRect.width, circle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, circle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area, circle.getArea(), EPSILON);
  circle.move(4, 2);
  BOOST_CHECK_CLOSE(frameRect.width, circle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, circle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area, circle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(increaseInAreaAfterScalingAndInvalidArguments)
{
  baboshina::Circle circle({ 3, 4 }, 6);
  double area = circle.getArea();
  double coef = 2.0;
  circle.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, circle.getArea(), EPSILON);
  BOOST_CHECK_THROW(circle.scale(-3), std::invalid_argument);

  coef = 0.5;
  area = circle.getArea();
  circle.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, circle.getArea(), EPSILON);

  BOOST_CHECK_THROW(baboshina::Circle circle({ 3, 4 }, -5), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END() 
