#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(testRectangle)

BOOST_AUTO_TEST_CASE(invariableOfParametersAfterMoving)
{
  baboshina::Rectangle rectangle(20, 5, { 3, 4 });
  baboshina::rectangle_t frameRect = rectangle.getFrameRect();
  double area = rectangle.getArea();
  rectangle.move({ 7, 2 });
  BOOST_CHECK_CLOSE(frameRect.width, rectangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, rectangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area, rectangle.getArea(), EPSILON);
  rectangle.move(4, -2);
  BOOST_CHECK_CLOSE(frameRect.width, rectangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, rectangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area, rectangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(increaseInAreaAfterScalingAndInvalidArguments)
{
  double coef = 2;
  baboshina::Rectangle rectangle(20, 5, { 3, 4 });
  double area = rectangle.getArea();
  rectangle.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, rectangle.getArea(), EPSILON);
  BOOST_CHECK_THROW(rectangle.scale(-3), std::invalid_argument);
  
  coef = 0.5;
  area = rectangle.getArea();
  rectangle.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, rectangle.getArea(), EPSILON);

  BOOST_CHECK_THROW(baboshina::Rectangle rectangle(-10, 5, { 3, 4 }), std::invalid_argument);
  BOOST_CHECK_THROW(baboshina::Rectangle rectangle(10, -6, { 3, 4 }), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END() 
