#include <iostream>
#include <cassert>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void test(baboshina::Shape * shape)
{
  assert(shape != nullptr);
  shape->printInfo();
  std::cout << "The area of shape is: " << shape->getArea() << std::endl;
  std::cout << "Width of frame rectangle is: " << shape->getFrameRect().width << std::endl;
  std::cout << "Height of frame rectangle is: " << shape->getFrameRect().height << std::endl;
  shape->move({ 1, 1 });
  shape->printInfo();
  shape->move(4, 5);
  shape->printInfo();
  shape->scale(2);
  shape->printInfo();
}

int main()
{
  baboshina::Circle circle({ 3, 4 }, 6);
  std::cout << "\n---Information about circle---" << std::endl;
  test(&circle);

  baboshina::Rectangle rectangle(20, 5, { 5, 5 });
  std::cout << "\n---Information about rectangle---" << std::endl;
  test(&rectangle);

  baboshina::Triangle triangle({ 1, 1 }, { 3, 4 }, { -4, 4 });
  std::cout << "\n---Information about triangle---" << std::endl;
  test(&triangle);

  const int size = 5;
  const baboshina::point_t points[size] = { { 2, 2 }, { 2, 6 }, { 7, 7 }, { 9, 4 }, { 6, 1 } };
  baboshina::Polygon polygon(size, points);
  std::cout << "\n---Information about polygon---" << std::endl;
  test(&polygon);

  return 0; 
} 
