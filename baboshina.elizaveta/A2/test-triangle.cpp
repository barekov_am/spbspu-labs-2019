#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(testTriangle)

BOOST_AUTO_TEST_CASE(invariableOfParametersAfterMoving)
{
  baboshina::Triangle triangle({ 2, 3 }, { 6, 9 }, { 4, 7 });
  baboshina::rectangle_t frameRect = triangle.getFrameRect();
  double area = triangle.getArea();
  triangle.move({ 2, 1 });
  BOOST_CHECK_CLOSE(frameRect.width, triangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, triangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area, triangle.getArea(), EPSILON);
  triangle.move(3, 5);
  BOOST_CHECK_CLOSE(frameRect.width, triangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, triangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area, triangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(increaseInAreaAfterScalingAndInvalidArguments)
{
  double coef = 2;
  baboshina::Triangle triangle({ 2, 3 }, { 6, 9 }, { 4, 7 });
  double area = triangle.getArea();
  triangle.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, triangle.getArea(), EPSILON);
  BOOST_CHECK_THROW(triangle.scale(-3), std::invalid_argument);

  area = triangle.getArea();
  triangle.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, triangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_SUITE_END() 
