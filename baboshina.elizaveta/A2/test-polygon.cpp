#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "polygon.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(testPolygon)

BOOST_AUTO_TEST_CASE(invariableOfParametersAfterMoving)
{
  const int size = 5;
  const baboshina::point_t points[size] = { { 2, 2 }, { 2, 6 }, { 7, 7 }, { 9, 4 }, { 6, 1 } };
  baboshina::Polygon polygon(size, points);
  baboshina::rectangle_t frameRect = polygon.getFrameRect();
  const double area = polygon.getArea();
  polygon.move({ 3, 5 });
  BOOST_CHECK_CLOSE(frameRect.width, polygon.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, polygon.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area, polygon.getArea(), EPSILON);
  polygon.move(1, 9);
  BOOST_CHECK_CLOSE(frameRect.width, polygon.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, polygon.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area, polygon.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(increaseInAreaAfterScalingAndInvalidArguments)
{
  double coef = 2;
  const int size = 5;
  const baboshina::point_t points[size] = { { 2, 2 }, { 2, 6 }, { 7, 7 }, { 9, 4 }, { 6, 1 } };
  baboshina::Polygon polygon(size, points);
  double area = polygon.getArea();
  polygon.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, polygon.getArea(), EPSILON);
  BOOST_CHECK_THROW(polygon.scale(-3), std::invalid_argument);
  
  coef = 0.5;
  area = polygon.getArea();
  polygon.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, polygon.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  const int size = 5;
  const baboshina::point_t points[size] = { { 2, 2 }, { 5, 4 }, { 4, 7 }, { 9, 7 }, { 6, 3 } };
  BOOST_CHECK_THROW(baboshina::Polygon polygon(size, points), std::invalid_argument);
  const int testsize = 2;
  const baboshina::point_t testPoints[testsize] = { { 2, 2 }, { 6, 8 } };
  BOOST_CHECK_THROW(baboshina::Polygon polygon(testsize, testPoints), std::invalid_argument);
  BOOST_CHECK_THROW(baboshina::Polygon polygon(size, nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END() 
