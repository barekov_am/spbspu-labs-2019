#define _USE_MATH_DEFINES

#include "composite-shape.hpp"
#include <iostream>
#include <algorithm>
#include <memory>
#include <cmath>

baboshina::CompositeShape::CompositeShape() :
size_(0),
shapeArray_(nullptr)
{}

baboshina::CompositeShape::CompositeShape(const CompositeShape &other) :
size_(other.size_),
shapeArray_(std::make_unique<baboshina::Shape::ptr_type[]>(other.size_))
{
  for (int index = 0; index < size_; index++)
  {
    shapeArray_[index] = other.shapeArray_[index];
  }
}

baboshina::CompositeShape::CompositeShape(CompositeShape &&other) :
size_(other.size_),
shapeArray_(std::move(other.shapeArray_))
{}

baboshina::CompositeShape::CompositeShape(Shape::ptr_type &shape) :
  size_(1),
  shapeArray_(std::make_unique<Shape::ptr_type[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape is not exist");
  }
  shapeArray_[0] = shape;
}

baboshina::CompositeShape & baboshina::CompositeShape::operator=(const CompositeShape &other)
{
  if (this != &other)
  {
    size_ = other.size_;
    array_type tempArray(std::make_unique<Shape::ptr_type[]>(other.size_));

    for (int index = 0; index < size_; index++)
    {
      tempArray[index] = other.shapeArray_[index];
    }
    shapeArray_.swap(tempArray);
  }
  return *this;
}

baboshina::CompositeShape & baboshina::CompositeShape::operator=(CompositeShape &&other)
{
  if (this != &other)
  {
    size_ = other.size_;
    shapeArray_.swap(other.shapeArray_);
    other.size_ = 0;
    other.shapeArray_.reset();
  }
  return *this;
}

baboshina::Shape::ptr_type baboshina::CompositeShape::operator[](const int index) const
{
  if ((index < 0) || (index >= size_))
  {
    throw std::invalid_argument("Index is out of range");
  }
  return shapeArray_[index];
}

double baboshina::CompositeShape::getArea() const
{
  if (size_ == 0)
  {
    throw std::invalid_argument("Composite shape is not exist");
  }
  double area = 0.0;
  for (int index = 0; index < size_; index++)
  {
    area += shapeArray_[index]->getArea();
  }
  return area;
}

baboshina::rectangle_t baboshina::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::invalid_argument("Composite shape is not exist");
  }
  rectangle_t tempRectangle = shapeArray_[0]->getFrameRect();
  double minX = tempRectangle.pos.x - tempRectangle.width / 2;
  double maxX = tempRectangle.pos.x + tempRectangle.width / 2;
  double minY = tempRectangle.pos.y - tempRectangle.height / 2;
  double maxY = tempRectangle.pos.y + tempRectangle.height / 2;

  for (int i = 1; i < size_; i++)
  {
    tempRectangle = shapeArray_[i]->getFrameRect();
    double frameRectangle = tempRectangle.pos.x - tempRectangle.width / 2;
    minX = std::min(frameRectangle, minX);
    frameRectangle = tempRectangle.pos.y - tempRectangle.height / 2;
    minY = std::min(frameRectangle, minY);
    frameRectangle = tempRectangle.pos.x + tempRectangle.width / 2;
    maxX = std::max(frameRectangle, maxX);
    frameRectangle = tempRectangle.pos.y + tempRectangle.height / 2;
    maxY = std::max(frameRectangle, maxY);
  }
  return { (maxX - minX), (maxY - minY), {(minX + maxX) / 2, (minY + maxY) / 2} };
}

void baboshina::CompositeShape::move(const point_t &point)
{
  if (size_ == 0)
  {
    throw std::invalid_argument("Composite shape is not exist");
  }
  baboshina::point_t center = getFrameRect().pos;
  move(point.x - center.x, point.y - center.y);
}

void baboshina::CompositeShape::move(const double &x, const double &y)
{
  if (size_ == 0)
  {
    throw std::invalid_argument("Composite shape is not exist");
  }
  for (int index = 0; index < size_; index++)
  {
    shapeArray_[index]->move(x, y);
  }
}

void baboshina::CompositeShape::printInfo() const
{
  if (size_ == 0)
  {
    throw std::invalid_argument("Composite shape is not exist");
  }
  for (int index = 0; index < size_; index++)
  {
    std::cout << "---Information about figure number " << index << "---\n";
    shapeArray_[index]->printInfo();
  }
}

void baboshina::CompositeShape::scale(const double coef)
{
  if (size_ == 0)
  {
    throw std::invalid_argument("Composite shape is not exist");
  }
  if (coef <= 0)
  {
    throw std::invalid_argument("Coefficient must be > 0");
  }
  baboshina::point_t center = getFrameRect().pos;
  for (int index = 0; index < size_; index++)
  {
    baboshina::point_t centerShape = shapeArray_[index]->getFrameRect().pos;
    double x = (centerShape.x - center.x) * (coef - 1);
    double y = (centerShape.y - center.y) * (coef - 1);
    shapeArray_[index]->move(x, y);
    shapeArray_[index]->scale(coef);
  }
}

void baboshina::CompositeShape::rotate(const double angle)
{
  if (size_ == 0)
  {
    throw std::invalid_argument("Composite shape is not exist");
  }
  baboshina::point_t center = getFrameRect().pos;
  double cos = fabs(std::cos(angle * M_PI / 180));
  double sin = fabs(std::sin(angle * M_PI / 180));

  for (int i = 0; i < size_; i++)
  {
    baboshina::point_t currCenter = shapeArray_[i]->getFrameRect().pos;
    double deltaX = (currCenter.x - center.x) * (cos - 1) - (currCenter.y - center.y) * sin;
    double deltaY = (currCenter.x - center.x) * sin + (currCenter.y - center.y) * (cos - 1);
    shapeArray_[i]->move(deltaX, deltaY);
    shapeArray_[i]->rotate(angle);
  }
}

void baboshina::CompositeShape::addShape(const Shape::ptr_type& shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape is not exist");
  }
  array_type tempArray(std::make_unique<Shape::ptr_type[]>(size_ + 1));
  for (int index = 0; index < size_; index++)
  {
    tempArray[index] = shapeArray_[index];
  }
  tempArray[size_] = shape;
  size_++;
  shapeArray_.swap(tempArray);
}

void baboshina::CompositeShape::deleteShape(const int number)
{
  if ((number < 0) || (number >= size_))
  {
    throw std::invalid_argument("Index is out of range");
  }
  array_type tempArray(std::make_unique<Shape::ptr_type[]>(size_ - 1));

  for (int index = 0; index < number; index++)
  {
    tempArray[index] = shapeArray_[index];
  }
  size_--;

  for (int index = number; index < size_; index++)
  {
    tempArray[index] = shapeArray_[index + 1];
  }

  shapeArray_.swap(tempArray);
}

int baboshina::CompositeShape::getSize() const
{
  return size_;
}
