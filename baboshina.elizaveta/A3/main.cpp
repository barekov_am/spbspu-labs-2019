#include <iostream>
#include <cassert>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

void test(baboshina::Shape::ptr_type &shape)
{
  assert(shape != nullptr);
  shape->printInfo();
  std::cout << "The area of shape is: " << shape->getArea() << std::endl;
  std::cout << "Width of frame rectangle is: " << shape->getFrameRect().width << std::endl;
  std::cout << "Height of frame rectangle is: " << shape->getFrameRect().height << std::endl;
  shape->move({ 1, 1 });
  shape->printInfo();
  shape->move(4, 5);
  shape->printInfo();
  shape->scale(2);
  shape->printInfo();
}

int main()
{
  baboshina::Shape::ptr_type circle = std::make_shared<baboshina::Circle>(baboshina::point_t{ 3, 4 }, 6);
  std::cout << "\n---Information about circle---" << std::endl;
  test(circle);

  baboshina::Shape::ptr_type rectangle = std::make_shared<baboshina::Rectangle>(20, 5, baboshina::point_t{ 3, 4 });
  std::cout << "\n---Information about rectangle---" << std::endl;
  test(rectangle);

  baboshina::Shape::ptr_type triangle = std::make_shared<baboshina::Triangle>(baboshina::point_t{ 1, 1 },
    baboshina::point_t{ 3, 4 }, baboshina::point_t{ -4, 4 });
  std::cout << "\n---Information about triangle---" << std::endl;
  test(triangle);

  const int size = 5;
  const baboshina::point_t points[size] = { { 2, 2 }, { 2, 6 }, { 7, 7 }, { 9, 4 }, { 6, 1 } };
  baboshina::Shape::ptr_type polygon = std::make_shared<baboshina::Polygon>(size, points);
  std::cout << "\n---Information about polygon---" << std::endl;
  test(polygon); 

  baboshina::CompositeShape compShape(circle);
  compShape.addShape(rectangle);
  compShape.addShape(polygon);
  baboshina::Shape::ptr_type testCompShape = std::make_shared<baboshina::CompositeShape>(compShape);
  std::cout << "\n---Information about composite shape---" << std::endl;
  test(testCompShape);

  return 0;
}
