#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "shape.hpp"
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "polygon.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(testCompositeShape)

BOOST_AUTO_TEST_CASE(invariableOfParametersAfterMoving)
{
  baboshina::Shape::ptr_type circle = std::make_shared<baboshina::Circle>(baboshina::point_t{ 3, 4 }, 6);
  baboshina::Shape::ptr_type rectangle = std::make_shared<baboshina::Rectangle>(20, 5, baboshina::point_t{ 3, 4 });
  const int size = 5;
  const baboshina::point_t points[size] = { { 2, 2 }, { 2, 6 }, { 7, 7 }, { 9, 4 }, { 6, 1 } };
  baboshina::Shape::ptr_type polygon = std::make_shared<baboshina::Polygon>(size, points);

  baboshina::CompositeShape compShape(circle);
  compShape.addShape(rectangle);
  compShape.addShape(polygon);

  baboshina::rectangle_t frameRect = compShape.getFrameRect();
  double area = compShape.getArea();
  compShape.move({ 2, 1 });
  BOOST_CHECK_CLOSE(frameRect.width, compShape.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, compShape.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area, compShape.getArea(), EPSILON);
  compShape.move(3, 5);
  BOOST_CHECK_CLOSE(frameRect.width, compShape.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, compShape.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area, compShape.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(increaseInAreaAfterScaling)
{
  baboshina::Shape::ptr_type circle = std::make_shared<baboshina::Circle>(baboshina::point_t{ 3, 4 }, 6);
  baboshina::Shape::ptr_type rectangle = std::make_shared<baboshina::Rectangle>(20, 5, baboshina::point_t{ 3, 4 });
  const int size = 5;
  const baboshina::point_t points[size] = { { 2, 2 }, { 2, 6 }, { 7, 7 }, { 9, 4 }, { 6, 1 } };
  baboshina::Shape::ptr_type polygon = std::make_shared<baboshina::Polygon>(size, points);

  baboshina::CompositeShape compShape(circle);
  compShape.addShape(rectangle);
  compShape.addShape(polygon);

  double coef = 2;
  double area = compShape.getArea();
  baboshina::point_t center = compShape.getFrameRect().pos;
  compShape.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, compShape.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(center.x, compShape.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(center.y, compShape.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_THROW(compShape.scale(-3), std::invalid_argument);

  coef = 0.5;
  area = compShape.getArea();
  compShape.scale(coef);
  BOOST_CHECK_CLOSE(area * coef * coef, compShape.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  baboshina::Shape::ptr_type circle = std::make_shared<baboshina::Circle>(baboshina::point_t{ 3, 4 }, 6);
  baboshina::CompositeShape compShape(circle);

  BOOST_CHECK_THROW(compShape.deleteShape(-1), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.deleteShape(10), std::invalid_argument);
  compShape.deleteShape(0);
  BOOST_CHECK_THROW(compShape.getArea(), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.getFrameRect(), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.move(3, 6), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.move({ 3, 6 }), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.scale(2), std::invalid_argument);
  BOOST_CHECK_THROW(compShape[2], std::invalid_argument);
  BOOST_CHECK_THROW(compShape[-1], std::invalid_argument);
  BOOST_CHECK_THROW(compShape.printInfo(), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invariableOfParametersAfterRotate)
{
  baboshina::Shape::ptr_type circle = std::make_shared<baboshina::Circle>(baboshina::point_t{ 3, 4 }, 6);
  baboshina::Shape::ptr_type rectangle = std::make_shared<baboshina::Rectangle>(20, 5, baboshina::point_t{ 3, 4 });
  const int size = 5;
  const baboshina::point_t points[size] = { { 2, 2 }, { 2, 6 }, { 7, 7 }, { 9, 4 }, { 6, 1 } };
  baboshina::Shape::ptr_type polygon = std::make_shared<baboshina::Polygon>(size, points);

  baboshina::CompositeShape compShape(circle);
  compShape.addShape(rectangle);
  compShape.addShape(polygon);

  double area = compShape.getArea();
  baboshina::rectangle_t frameRect = compShape.getFrameRect();

  compShape.rotate(360);
  BOOST_CHECK_CLOSE(frameRect.height, compShape.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, compShape.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.x, compShape.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.y, compShape.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(area, compShape.getArea(), EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()
