#include "matrix.hpp"
#include <iostream>
#include <stdexcept>
#include "shape.hpp"

baboshina::Matrix::Matrix() :
  rows_(0),
  columns_(0)
{
}

baboshina::Matrix::Matrix(const Matrix& other) :
  shapesArray_(std::make_unique<Shape::ptr_type[]>(other.rows_* other.columns_)),
  rows_(other.rows_),
  columns_(other.columns_)
{
  for (int index = 0; index < (rows_ * columns_); index++)
  {
    shapesArray_[index] = other.shapesArray_[index];
  }
}

baboshina::Matrix::Matrix(Matrix&& other) :
  shapesArray_(std::move(other.shapesArray_)),
  rows_(other.rows_),
  columns_(other.columns_)
{
  other.rows_ = 0;
  other.columns_ = 0;
}

baboshina::Matrix& baboshina::Matrix::operator=(const Matrix& other)
{
  if (&other != this)
  {
    Shape::array_type tempArray = std::make_unique<Shape::ptr_type[]>(other.rows_ * other.columns_);
    for (int i = 0; i < (other.rows_ * other.columns_); i++)
    {
      tempArray[i] = other.shapesArray_[i];
    }

    shapesArray_.swap(tempArray);
    rows_ = other.rows_;
    columns_ = other.columns_;
  }
  return *this;
}

baboshina::Matrix& baboshina::Matrix::operator=(Matrix &&other)
{
  if (&other != this)
  {
    rows_ = other.rows_;
    columns_ = other.columns_;
    shapesArray_ = std::move(other.shapesArray_);

    other.rows_ = 0;
    other.columns_ = 0;
  }
  return *this;
}

baboshina::Shape::array_type baboshina::Matrix::operator[](int layer) const
{
  if ((rows_ <= layer) || (layer < 0))
  {
    throw std::invalid_argument("Index is out of range");
  }

  Shape::array_type shapeArray = std::make_unique<Shape::ptr_type[]>(getLayerSize(layer));

  for (int index = 0; index < getLayerSize(layer); index++)
  {
    shapeArray[index] = shapesArray_[layer * columns_ + index];
  }

  return shapeArray;
}

bool baboshina::Matrix::operator==(const Matrix &other) const
{
  if ((rows_ != other.rows_) || (columns_ != other.columns_))
  {
    return false;
  }

  for (int index = 0; index < (rows_ * columns_); index++)
  {
    if (shapesArray_[index] != other.shapesArray_[index])
    {
      return false;
    }
  }

  return true;
}

bool baboshina::Matrix::operator!=(const Matrix &other) const
{
  return !(*this == other);
}

int baboshina::Matrix::getLayerSize(int layer) const
{
  if (layer >= rows_)
  {
    return 0;
  }

  for (int i = 0; i < columns_; i++)
  {
    if (shapesArray_[layer * columns_ + i] == nullptr)
    {
      return i;
    }
  }

  return columns_;
}

int baboshina::Matrix::getRows()
{
  return rows_;
}

void baboshina::Matrix::add(const Shape::ptr_type &shape, int layer)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape is not exist");
  }

  const int tempRows = (layer >= rows_) ? (rows_ + 1) : rows_;
  const int tempColumns = (getLayerSize(layer) == columns_) ? (columns_ + 1) : columns_;
  const int column = (layer < rows_) ? getLayerSize(layer) : 0;

  Shape::array_type tempArray = std::make_unique<Shape::ptr_type[]>(tempRows * tempColumns);

  for (int i = 0; i < tempRows; i++)
  {
    for (int j = 0; j < tempColumns; j++)
    {
      if ((i == rows_) || (j == columns_))
      {
        tempArray[i * tempColumns + j] = nullptr;
      }
      else {
        tempArray[i * tempColumns + j] = shapesArray_[i * columns_ + j];
      }
    }
  }

  if (layer < rows_)
  {
    tempArray[layer * tempColumns + column] = shape;
  }
  else {
    tempArray[rows_ * tempColumns + column] = shape;
  }

  shapesArray_.swap(tempArray);
  rows_ = tempRows;
  columns_ = tempColumns;
}

void baboshina::Matrix::printInfo() const
{
  for (int i = 0; i < rows_; i++)
  {
    for (int j = 0; j < getLayerSize(i); j++)
    {
      std::cout << "This is layer " << i << ": " << "figure " << j << std::endl;
      shapesArray_[i * j]->printInfo();
    }
  }
}
