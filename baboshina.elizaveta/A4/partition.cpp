#include "partition.hpp"

bool baboshina::intersect(const rectangle_t& first, const rectangle_t& second)
{
  const point_t leftBottom = { first.pos.x - first.width / 2, first.pos.y - first.height / 2 };
  const point_t leftTop = { first.pos.x + first.width / 2, first.pos.y + first.height / 2 };
  const point_t rightBottom = { second.pos.x - second.width / 2, second.pos.y - second.height / 2 };
  const point_t rightTop = { second.pos.x + second.width / 2, second.pos.y + second.height / 2 };

  return ((leftBottom.x < rightTop.x && leftBottom.y < rightTop.y) &&
    (rightBottom.x < leftTop.x && rightBottom.y < leftTop.y));
}

baboshina::Matrix baboshina::partition(const CompositeShape &compShape)
{
  Matrix tmpMatrix;
  const int size = compShape.getSize();

  for (int i = 0; i < size; i++)
  {
    int layer = 0;
    for (int j = 0; j < tmpMatrix.getRows(); j++)
    {
      bool intersected = false;
      for (int k = 0; k < tmpMatrix.getLayerSize(j); k++)
      {
        if (intersect(compShape[i]->getFrameRect(), tmpMatrix[j][k]->getFrameRect()))
        {
          layer++;
          intersected = true;
          break;
        }
      }
      if (!intersected)
      {
        break;
      }
    }
    tmpMatrix.add(compShape[i], layer);
  }

  return tmpMatrix;
}
