#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

namespace baboshina
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &other);
    Matrix(Matrix &&other);
    ~Matrix() = default;

    Matrix& operator =(const Matrix &other);
    Matrix& operator =(Matrix &&other);
    Shape::array_type operator [](int layer) const;
    bool operator ==(const Matrix &other) const;
    bool operator !=(const Matrix &other) const;

    int getLayerSize(int layer) const;
    int getRows();
    void add(const Shape::ptr_type &shape, int layer);
    void printInfo() const;

  private:
    Shape::array_type shapesArray_;
    int rows_;
    int columns_;
  };
}

#endif //!MATRIX_HPP
