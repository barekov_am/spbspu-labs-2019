#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include <memory>
#include "matrix.hpp"
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "polygon.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(invalidArgumentsAndCorrectness)
{
  baboshina::Shape::ptr_type circle = std::make_shared<baboshina::Circle>(baboshina::point_t{ 3, 4 }, 6);
  baboshina::Shape::ptr_type rectangle = std::make_shared<baboshina::Rectangle>(20, 5, baboshina::point_t{ 3, 4 });
  const int size = 5;
  const baboshina::point_t points[size] = { { 2, 2 }, { 2, 6 }, { 7, 7 }, { 9, 4 }, { 6, 1 } };
  baboshina::Shape::ptr_type polygon = std::make_shared<baboshina::Polygon>(size, points);

  baboshina::CompositeShape compShape;
  compShape.addShape(circle);
  compShape.addShape(rectangle);
  compShape.addShape(polygon);

  baboshina::Matrix matrix = baboshina::partition(compShape);

  const int rows = 3;
  const int columns = 1;

  BOOST_CHECK_EQUAL(matrix.getRows(), rows);
  BOOST_CHECK_EQUAL(matrix.getLayerSize(1), columns);
  BOOST_CHECK(matrix[0][0] == circle);
  BOOST_CHECK(matrix[1][0] == rectangle);
  BOOST_CHECK(matrix[2][0] == polygon);

  BOOST_CHECK_THROW(matrix[20], std::invalid_argument);
  BOOST_CHECK_THROW(matrix[-2], std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
