#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace baboshina
{
  bool intersect(const rectangle_t &first, const rectangle_t &second);
  Matrix partition(const baboshina::CompositeShape &compShape);
}

#endif // !PARTITION_HPP
