#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include "shape.hpp"

namespace baboshina
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape& other);
    CompositeShape(CompositeShape&& other);
    CompositeShape(Shape::ptr_type& other);
    ~CompositeShape() = default;
    CompositeShape& operator =(const CompositeShape& other);
    CompositeShape& operator =(CompositeShape&& other);
    Shape::ptr_type operator [](const int index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& point) override;
    void move(const double& x, const double& y) override;
    void printInfo() const override;
    void scale(const double coef) override;
    void rotate(const double angle) override;
    void addShape(const Shape::ptr_type& shape);
    void deleteShape(const int number);
    int getSize() const;

  private:
    int size_;
    Shape::array_type shapeArray_;
  };
}

#endif // !COMPOSITESHAPE_HPP
