#define _USE_MATH_DEFINES

#include "polygon.hpp"
#include <iostream>
#include <algorithm>
#include <stdexcept>
#include <cmath>
#include "triangle.hpp"

baboshina::Polygon::Polygon(const int size, const point_t * points) :
  size_(size),
  points_(new point_t[size])
{
  if (points != nullptr)
  {
    for (int index = 0; index < size_; index++)
    {
      points_[index] = points[index];
    }
    if (testCorrectness() != true)
    {
      delete[] points_;
      throw std::invalid_argument("Polygon is not correct");
    }
  }
  else
  {
    delete[] points_;
    throw std::invalid_argument("Polygon is not exist");
  }
}

baboshina::Polygon::Polygon(const Polygon &other) :
  size_(other.size_),
  points_(new point_t[size_])
{
  for (int index = 0; index < size_; index++)
  {
    points_[index] = other.points_[index];
  }
}

baboshina::Polygon::Polygon(Polygon &&other) :
  size_(other.size_),
  points_(other.points_)
{
  other.size_ = 0;
  other.points_ = nullptr;
}

baboshina::Polygon::~Polygon()
{
  delete[] points_;
}

baboshina::Polygon & baboshina::Polygon::operator =(const Polygon &other)
{
  if (this != &other)
  {
    if (points_ != nullptr)
    {
      delete[] points_;
    }
    size_ = other.size_;
    points_ = new point_t[size_];
    for (int index = 0; index < size_; index++)
    {
      points_[index] = other.points_[index];
    }
  }
  return *this;
}

baboshina::Polygon & baboshina::Polygon::operator =(Polygon &&other)
{
  if (this != &other)
  {
    size_ = other.size_;
    other.size_ = 0;
    points_ = other.points_;
    other.points_ = nullptr;
  }
  return *this;
}

double baboshina::Polygon::getArea() const
{
  double area = 0;
  for (int index = 1; index < size_ - 1; index++)
  {
    baboshina::Triangle triangle(points_[0], points_[index], points_[index + 1]);
    area += triangle.getArea();
  }
  return area;
}

baboshina::rectangle_t baboshina::Polygon::getFrameRect() const
{
  double maxX = points_[0].x;
  double maxY = points_[0].y;
  double minX = points_[0].x;
  double minY = points_[0].y;
  for (int index = 1; index < size_; index++)
  {
    maxX = std::max(maxX, points_[index].x);
    maxY = std::max(maxY, points_[index].y);
    minX = std::min(minX, points_[index].x);
    minY = std::min(minY, points_[index].y);
  }
  return { maxX - minX, maxY - minY, { minX + maxX / 2, minY + maxY / 2 } };
}
 
void baboshina::Polygon::move(const point_t &point)
{
  point_t center = getCenter();
  move(point.x - center.x, point.y - center.y);
}

void baboshina::Polygon::move(const double &x, const double &y)
{
  for (int index = 0; index < size_; index++)
  {
    points_[index].x += x;
    points_[index].y += y;
  }
}

void baboshina::Polygon::printInfo() const
{
  std::cout << "\n";
  for (int index = 0; index < size_; index++)
  {
    std::cout << "Point[" << index << "] is: x=" << points_[index].x << " y=" << points_[index].y << std::endl;
  }
}

void baboshina::Polygon::scale(const double coef)
{
  if (coef <= 0)
  {
    throw std::invalid_argument("Coefficient must be > 0");
  }
  point_t center = getCenter();
  for (int index = 0; index < size_; index++)
  {
    points_[index].x = points_[index].x * coef - center.x * (coef - 1);
    points_[index].y = points_[index].y * coef - center.y * (coef - 1);
  }
}

void baboshina::Polygon::rotate(const double angle)
{
  double cos = std::cos(angle * M_PI / 180);
  double sin = std::sin(angle * M_PI / 180);

  point_t center = getCenter();
  for (int index = 0; index < size_; index++)
  {
    points_[index] = { center.x + cos * (points_[index].x - center.x) - sin * (points_[index].y - center.y), 
      center.y + cos * (points_[index].y - center.y) + sin * (points_[index].x - center.x) };
  }
 
}

baboshina::point_t baboshina::Polygon::getCenter() const
{
  point_t center = { 0, 0 };
  for (int index = 0; index < size_; index++)
  {
    center.x += points_[index].x;
    center.y += points_[index].y;
  }
  center.x /= size_;
  center.y /= size_;
  return center;
}

bool baboshina::Polygon::testCorrectness() const
{
  if ((size_ < 4) || (getArea() <= 0))
  {
    delete[] points_;
    throw std::invalid_argument("Polygon is not correct");
  }
  bool result = true;
  double sign1 = (points_[0].x - points_[size_ - 1].x) * (points_[1].y - points_[0].y)
    - (points_[1].x - points_[0].x) * (points_[0].y - points_[size_ - 1].y);
  for (int index = 1; index < size_; index++)
  {
    double sign2 = (points_[index].x - points_[index - 1].x) * (points_[(index + 1) % size_].y - points_[index].y)
      - (points_[(index + 1) % size_].x - points_[index].x) * (points_[index].y - points_[index - 1].y);
    if ((sign1 * sign2) < 0)
    {
      result = false;
      break;
    }
  }
  return result;
}
