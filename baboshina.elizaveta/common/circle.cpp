#define _USE_MATH_DEFINES

#include "circle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

baboshina::Circle::Circle(const point_t &center, const double &radius) :
  center_(center),
  radius_(radius)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("Radius must be > 0");
  }
}

double baboshina::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

baboshina::rectangle_t baboshina::Circle::getFrameRect() const
{
  return { 2 * radius_, 2 * radius_, center_ };
}

void baboshina::Circle::move(const point_t &point)
{
  center_ = point;
}

void baboshina::Circle::move(const double &x, const double &y)
{
  center_.x += x;
  center_.y += y;
}

void baboshina::Circle::printInfo() const
{
  std::cout << "\nRadius of circle is: " << radius_ << std::endl;
  std::cout << "Center of circle is: x=" << center_.x << " y=" << center_.y << std::endl;
}

void baboshina::Circle::scale(const double coef)
{
  if (coef <= 0)
  {
    throw std::invalid_argument("Coefficient must be > 0");
  }
  radius_ *= coef;
}
