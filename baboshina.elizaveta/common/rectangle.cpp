#define _USE_MATH_DEFINES

#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

baboshina::Rectangle::Rectangle(const double width, const double height, const point_t &pos) :
  width_(width),
  height_(height),
  center_(pos),
  angle_(0)
{
  if ((width_ <= 0) || (height_ <= 0))
  {
    throw std::invalid_argument("Width and height must be > 0");
  }
}

double baboshina::Rectangle::getArea() const
{
  return height_ * width_;
}

baboshina::rectangle_t baboshina::Rectangle::getFrameRect() const
{
  double angle = angle_ * M_PI / 180;

  double width = height_ * fabs(std::sin(angle)) + width_ * fabs(std::cos(angle));
  double height = height_ * fabs(std::cos(angle)) + width_ * fabs(std::sin(angle));

  return { width, height, center_ };
}

void baboshina::Rectangle::move(const point_t &point)
{
  center_ = point;
}

void baboshina::Rectangle::move(const double &x, const double &y)
{
  center_.x += x;
  center_.y += y;
}

void baboshina::Rectangle::printInfo() const
{
  std::cout << "\nWidth of rectangle is: " << width_ << std::endl;
  std::cout << "Height of rectangle is: " << height_ << std::endl;
  std::cout << "Center of rectangle is: x=" << center_.x << " y=" << center_.y << std::endl;
}

void baboshina::Rectangle::scale(const double coef)
{
  if (coef <= 0)
  {
    throw std::invalid_argument("Coefficient must be > 0");
  }
  width_ *= coef;
  height_ *= coef;
}

void baboshina::Rectangle::rotate(const double angle)
{
  angle_ += angle;
}
