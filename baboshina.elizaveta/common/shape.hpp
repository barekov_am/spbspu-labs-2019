#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace baboshina
{
  class Shape
  {
  public:
    using ptr_type = std::shared_ptr<Shape>;
    using array_type = std::unique_ptr<ptr_type[]>;

    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t &point) = 0;
    virtual void move(const double &x, const double &y) = 0;
    virtual void printInfo() const = 0;
    virtual void scale(const double coef) = 0;
    virtual void rotate(const double angle) = 0;
  };
}

#endif // !SHAPE_HPP
