#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "shape.hpp"

namespace baboshina
{
  class Polygon : public Shape
  {
  public:
    Polygon(const int size, const point_t * points);
    Polygon(const Polygon &other);
    Polygon(Polygon &&other);
    ~Polygon();
    Polygon &operator =(const Polygon &other);
    Polygon &operator =(Polygon &&other);
    
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(const double &x, const double &y) override;
    void printInfo() const override;
    void scale(const double coef) override;
    void rotate(const double angle) override;
  private:
    int size_;
    point_t * points_;
    point_t getCenter() const;
    bool testCorrectness() const;
  };
}

#endif // !POLYGON_HPP
