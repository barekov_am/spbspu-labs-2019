#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace baboshina
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t &center, const double &radius);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(const double &x, const double &y) override;
    void printInfo() const override;
    void scale(const double coef) override;
    void rotate(const double) override {};

  private:
    point_t center_;
    double radius_;
  };
}

#endif // !CIRCLE_HPP
