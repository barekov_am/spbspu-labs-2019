#define _USE_MATH_DEFINES

#include "triangle.hpp"
#include <iostream>
#include <algorithm>
#include <stdexcept>
#include <cmath>

baboshina::Triangle::Triangle(const point_t &pointA, const point_t &pointB, const point_t &pointC) :
  pointA_(pointA),
  pointB_(pointB),
  pointC_(pointC)
{
}

double baboshina::Triangle::getArea() const
{
  return fabs(((pointB_.x - pointA_.x) * (pointC_.y - pointA_.y)
      - (pointC_.x - pointA_.x) * (pointB_.y - pointA_.y)) / 2);
}

baboshina::rectangle_t baboshina::Triangle::getFrameRect() const
{
  double maxX = std::max(std::max(pointA_.x, pointB_.x), pointC_.x);
  double minX = std::min(std::min(pointA_.x, pointB_.x), pointC_.x);
  double maxY = std::max(std::max(pointA_.y, pointB_.y), pointC_.y);
  double minY = std::min(std::min(pointA_.y, pointB_.y), pointC_.y);
  return { maxX - minX, maxY - minY, { minX + maxX / 2, minY + maxY / 2 } };
}

void baboshina::Triangle::move(const point_t &point)
{
  point_t center = { (pointA_.x + pointB_.x + pointC_.x) / 3, (pointA_.y + pointB_.y + pointC_.y) / 3 };
  move(point.x - center.x, point.y - center.y);
}

void baboshina::Triangle::move(const double &x, const double &y)
{
  pointA_.x += x;
  pointB_.x += x;
  pointC_.x += x;
  pointA_.y += y;
  pointB_.y += y;
  pointC_.y += y;
}

void baboshina::Triangle::printInfo() const
{
  std::cout << "\nPoint A is: x=" << pointA_.x << " y=" << pointA_.y << std::endl;
  std::cout << "Point B is: x=" << pointB_.x << " y=" << pointB_.y << std::endl;
  std::cout << "Point C is: x=" << pointC_.x << " y=" << pointC_.y << std::endl;
}


void baboshina::Triangle::scale(const double coef)
{
  if (coef <= 0)
  {
    throw std::invalid_argument("Coefficient must be > 0");
  }
  point_t center = { (pointA_.x + pointB_.x + pointC_.x) / 3, (pointA_.y + pointB_.y + pointC_.y) / 3 };
  pointA_.x = pointA_.x * coef - center.x * (coef - 1);
  pointB_.x = pointB_.x * coef - center.x * (coef - 1);
  pointC_.x = pointC_.x * coef - center.x * (coef - 1);
  pointA_.y = pointA_.y * coef - center.y * (coef - 1);
  pointB_.y = pointB_.y * coef - center.y * (coef - 1);
  pointC_.y = pointC_.y * coef - center.y * (coef - 1);
}

void baboshina::Triangle::rotate(const double angle)
{
  double cos = std::cos(angle * M_PI / 180);
  double sin = std::sin(angle * M_PI / 180);

  point_t center = { (pointA_.x + pointB_.x + pointC_.x) / 3, (pointA_.y + pointB_.y + pointC_.y) / 3 };
  pointA_ = { center.x + cos * (pointA_.x - center.x) - sin * (pointA_.y - center.y),
    center.y + cos * (pointA_.y - center.y) + sin * (pointA_.x - center.x) };
  pointB_ = { center.x + cos * (pointB_.x - center.x) - sin * (pointB_.y - center.y),
    center.y + cos * (pointB_.y - center.y) + sin * (pointB_.x - center.x) };
  pointC_ = { center.x + cos * (pointC_.x - center.x) - sin * (pointC_.y - center.y),
    center.y + cos * (pointC_.y - center.y) + sin * (pointC_.x - center.x) };
}
