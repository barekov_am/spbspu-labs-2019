#include <iostream>
#include <sstream>
#include <algorithm>

#include "command_handler.hpp"
#include "phBook_exceptions.hpp"

std::string readName(std::istringstream& stream)
{
  stream.ignore();
  std::string name;
  std::getline(stream, name);

  if (name.empty())
  {
    return name;
  }

  if ((name.front() != '\"') || (name.back() != '\"'))
  {
    throw kuxov::details::IncorrectCommandException();
  }

  name.erase(std::remove(name.begin(), name.end(), '\\'), name.end());
  name.erase(0, 1);
  name.pop_back();
  return name;
}

std::string readNumber(std::istringstream& stream)
{
  std::string number;
  stream >> number;
  for (auto i : number)
  {
    if (!isdigit(i))
    {
      throw kuxov::details::IncorrectCommandException();
    }
  }
  return number;
}

std::string readMark(std::istringstream& stream)
{
  std::string mark_name;
  stream >> mark_name;
  for (auto i : mark_name)
  {
    if (!isalnum(i))
    {
      throw kuxov::details::IncorrectCommandException();
    }
  }
  return mark_name;
}

kuxov::details::command_function kuxov::details::handleCommand(const std::string& input)
{
  std::istringstream sStream(input);
  std::string command_Str;
  sStream >> command_Str;

  if (command_Str == kuxov::constants::ADD)
  {
    std::string number = readNumber(sStream);
    std::string name = readName(sStream);

    return[number, name](PhoneBook& book)
    {
      book.insertEnd(number, name);
    };
  }
  else if (command_Str == kuxov::constants::STORE)
  {
    std::string mark_name_old = readMark(sStream);
    std::string mark_name_new = readMark(sStream);

    return[mark_name_old, mark_name_new](PhoneBook& book)
    {
      book.addNote(mark_name_old, mark_name_new);
    };
  }
  else if (command_Str == kuxov::constants::INSERT)
  {
    std::string position;
    sStream >> position;

    std::string mark_name = readMark(sStream);
    std::string number = readNumber(sStream);
    std::string name = readName(sStream);

    if (position == kuxov::constants::AFTER)
    {
      return[number, name, mark_name](PhoneBook& book)
      {
        book.insertAfter(number, name, mark_name);
      };
    }
    else if (position == kuxov::constants::BEFORE)
    {
      return[number, name, mark_name](PhoneBook& book)
      {
        book.insertBefore(number, name, mark_name);
      };
    }
    else
    {
      throw kuxov::details::IncorrectCommandException();
    }
  }
  else if (command_Str == kuxov::constants::DELETE)
  {
    std::string mark_name = readMark(sStream);

    return[mark_name](PhoneBook& book)
    {
      book.deleteNote(mark_name);
    };
  }
  else if (command_Str == kuxov::constants::SHOW)
  {
    std::string mark_name = readMark(sStream);

    return[mark_name](PhoneBook& book)
    {
      std::cout << book.getRecord(mark_name).number << " " << book.getRecord(mark_name).name << std::endl;
    };
  }
  else if (command_Str == kuxov::constants::MOVE)
  {
    std::string bookmark = readMark(sStream),
      steps;
    sStream >> steps;
    return [bookmark, steps](PhoneBook& book)
    {
      book.move(steps, bookmark);
    };
  }
  return [](PhoneBook&)
  {
    throw kuxov::details::IncorrectCommandException();
  };
}
