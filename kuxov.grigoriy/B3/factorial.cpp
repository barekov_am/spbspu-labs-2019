#include "factorial.hpp"

kuxov::details::Factorial::FactIterator::FactIterator() : FactIterator(1)
{
}

kuxov::details::Factorial::FactIterator::FactIterator(size_t pos) :
  position_(pos),
  value_(factor(pos))
{
}

const size_t* kuxov::details::Factorial::FactIterator::operator->() const
{
  return &value_;
}

const size_t& kuxov::details::Factorial::FactIterator::operator*() const
{
  return value_;
}

kuxov::details::Factorial::FactIterator& kuxov::details::Factorial::FactIterator::operator++()
{
  if (position_ < MAX_POSITION)
  {
    ++position_;
    value_ *= position_;
  }

  return *this;
}

kuxov::details::Factorial::FactIterator kuxov::details::Factorial::FactIterator::operator++(int)
{
  Factorial::FactIterator temp = *this;
  ++(*this);

  return temp;
}

kuxov::details::Factorial::FactIterator& kuxov::details::Factorial::FactIterator::operator--()
{
  if (position_ > MIN_POSITION)
  {
    value_ /= position_;
    --position_;
  }

  return *this;
}

kuxov::details::Factorial::FactIterator kuxov::details::Factorial::FactIterator::operator--(int)
{
  Factorial::FactIterator temp = *this;
  --(*this);

  return temp;
}

bool kuxov::details::Factorial::FactIterator::operator==(const FactIterator& rhs) const
{
  return (position_ == rhs.position_);
}

bool kuxov::details::Factorial::FactIterator::operator!=(const FactIterator& rhs) const
{
  return !(*this == rhs);
}

inline size_t kuxov::details::Factorial::FactIterator::factor(size_t number)
{
  size_t res = 1;
  for (size_t i = 1; i <= number; i++)
  {
    res *= i;
  }

  return res;
}

kuxov::details::Factorial::FactIterator kuxov::details::Factorial::begin()
{
  return FactIterator(FactIterator::MIN_POSITION);
}

kuxov::details::Factorial::FactIterator kuxov::details::Factorial::end()
{
  return FactIterator(FactIterator::MAX_POSITION);
}
