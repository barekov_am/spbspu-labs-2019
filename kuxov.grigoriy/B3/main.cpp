#include <iostream>
#include <cstring>

#include "tasks.hpp"

int main(int argc, char* argv[])
{
  try
  {
    if (argc < 2)
    {
      throw std::invalid_argument("Not enough arguments!");
    }
    if (strcmp(argv[1], "1") == 0)
    {
      kuxov::tasks::taskOne();
    }
    else if (strcmp(argv[1], "2") == 0)
    {
      kuxov::tasks::taskTwo();
    }
    else
    {
      throw std::invalid_argument("Invalid input!");
    }
  }
  catch (std::exception& exc)
  {
    std::cerr << exc.what() << std::endl;
    return 2;
  }

  return 0;
}
