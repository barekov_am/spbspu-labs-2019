#include <limits>

#include "functor.hpp"

kuxov::Functor::Functor() noexcept
{
  max_ = std::numeric_limits<int>::min();
  min_ = std::numeric_limits<int>::max();
  first_ = 0;
  positive_ = 0;
  negative_ = 0;
  total_ = 0;
  odd_sum_ = 0;
  even_sum_ = 0;
  equals_ = false;
}

void kuxov::Functor::operator()(const int& number) noexcept
{
  if (number > max_)
  {
    max_ = number;
  }

  if (number < min_)
  {
    min_ = number;
  }

  if (number > 0)
  {
    positive_++;
  }

  if (number < 0)
  {
    negative_++;
  }

  if (number % 2 == 0)
  {
    even_sum_ += number;
  }
  else
  {
    odd_sum_ += number;
  }

  if (total_ == 0)
  {
    first_ = number;
  }

  if (first_ == number)
  {
    equals_ = true;
  }
  else
  {
    equals_ = false;
  }

  total_++;
}

std::ostream& kuxov::operator<<(std::ostream& stream, const kuxov::Functor& functor)
{
  if (functor.total_ == 0)
  {
    stream << "No Data" << std::endl;
    return stream;
  }

  stream << "Max: " << functor.max_ << std::endl;
  stream << "Min: " << functor.min_ << std::endl;
  stream << "Mean: " << static_cast<long double>((functor.even_sum_ + functor.odd_sum_)) / functor.total_ << std::endl;
  stream << "Positive: " << functor.positive_ << std::endl;
  stream << "Negative: " << functor.negative_ << std::endl;
  stream << "Odd Sum: " << functor.odd_sum_ << std::endl;
  stream << "Even Sum: " << functor.even_sum_ << std::endl;
  stream << "First/Last Equal: ";
  stream << (functor.equals_ ? "yes" : "no") << std::endl;

  return stream;
}
