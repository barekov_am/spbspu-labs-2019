#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace kuxov
{
  class Triangle : public Shape
  {
  public:
    Triangle(int x, int y);
    virtual void draw(std::ostream& stream) const override;
  };
}

#endif
