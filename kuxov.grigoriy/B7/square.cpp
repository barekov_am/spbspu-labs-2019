#include "square.hpp"

kuxov::Square::Square(int x, int y) :
  Shape(x, y)
{
}

void kuxov::Square::draw(std::ostream& stream) const
{
  stream << "SQUARE (" << getX() << ";" << getY() << ")" << std::endl;
}
