#include <vector>
#include <sstream>
#include <algorithm>
#include <functional>

#include "tasks.hpp"
#include "triangle.hpp"
#include "square.hpp"
#include "circle.hpp"

std::shared_ptr<kuxov::Shape> readShape(const std::string& line)
{
  std::istringstream is(line);
  std::string shape_type;
  int x = 0, y = 0;

  is >> shape_type;
  is.ignore(line.length(), ' ');
  is >> x;
  is.ignore(line.length(), ' ');
  is >> y;
  is.ignore(line.length(), ' ');

  if (is.fail())
  {
    throw std::invalid_argument("Error! Incorrect input!");
  }

  if (shape_type == "SQUARE")
  {
    return std::make_shared<kuxov::Square>(kuxov::Square(x, y));
  }
  else if (shape_type == "CIRCLE")
  {
    return std::make_shared<kuxov::Circle>(kuxov::Circle(x, y));
  }
  else if (shape_type == "TRIANGLE")
  {
    return std::make_shared<kuxov::Triangle>(kuxov::Triangle(x, y));
  }
  else
  {
    throw std::invalid_argument("Error! Invalid type of shape!");
  }
}

void printShapes(const std::vector<std::shared_ptr<kuxov::Shape>>& shapes)
{
  std::for_each(shapes.begin(), shapes.end(),
    std::bind(&kuxov::Shape::draw, std::placeholders::_1, std::ref(std::cout)));
}

void tasks::taskTwo()
{
  using shape_t = kuxov::Shape;
  using shape_ptr = std::shared_ptr<shape_t>;
  std::vector<shape_ptr> shapes;

  std::string line;
  while (std::getline(std::cin, line))
  {
    line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
    std::replace(line.begin(), line.end(), '(', ' ');
    std::replace(line.begin(), line.end(), ';', ' ');
    std::replace(line.begin(), line.end(), ')', ' ');
    if (line.empty()) { continue; }
    shapes.push_back(readShape(line));
  }

  std::cout << "Original:" << std::endl;
  printShapes(shapes);

  std::cout << "Left-Right:" << std::endl;
  std::sort(shapes.begin(), shapes.end(),
    [](const std::shared_ptr<shape_t >& rhs, const std::shared_ptr<shape_t>& lhs)
    {
      return rhs->isMoreLeft(lhs);
    });
  printShapes(shapes);

  std::cout << "Right-Left:" << std::endl;
  std::sort(shapes.begin(), shapes.end(),
    [](const std::shared_ptr<shape_t>& rhs, const std::shared_ptr<shape_t>& lhs)
    {
      return !rhs->isMoreLeft(lhs);
    });
  printShapes(shapes);

  std::cout << "Top-Bottom:" << std::endl;
  std::sort(shapes.begin(), shapes.end(),
    [](const std::shared_ptr<shape_t>& rhs, const std::shared_ptr<shape_t>& lhs)
    {
      return rhs->isUpper(lhs);
    });
  printShapes(shapes);

  std::cout << "Bottom-Top:" << std::endl;
  std::sort(shapes.begin(), shapes.end(),
    [](const std::shared_ptr<shape_t>& rhs, const std::shared_ptr<shape_t>& lhs)
    {
      return !rhs->isUpper(lhs);
    });
  printShapes(shapes);
}
