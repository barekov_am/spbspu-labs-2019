#include "triangle.hpp"

kuxov::Triangle::Triangle(int x, int y) :
  Shape(x, y)
{
}

void kuxov::Triangle::draw(std::ostream& stream) const
{
  stream << "TRIANGLE (" << getX() << ";" << getY() << ")" << std::endl;
}
