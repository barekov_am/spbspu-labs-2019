#include <iostream>
#include "tasks.hpp"

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      throw std::invalid_argument("Error! Incorrect number of args!");
    }

    std::string task_num(argv[1]);
    if (task_num == "1")
    {
      tasks::taskOne();
    }
    else if (task_num == "2")
    {
      tasks::taskTwo();
    }
    else
    {
      throw std::invalid_argument("Error! Incorrect task number!");
    }
  }
  catch (const std::exception& exc)
  {
    std::cout << exc.what() << std::endl;
    return 1;
  }
  return 0;
}
