#include "circle.hpp"

kuxov::Circle::Circle(int x, int y) :
  Shape(x, y)
{
}

void kuxov::Circle::draw(std::ostream& stream) const
{
  stream << "CIRCLE (" << getX() << ";" << getY() << ")" << std::endl;
}
