#include "shape.hpp"

kuxov::Shape::Shape(int x, int y) noexcept :
  x_(x),
  y_(y)
{
}

bool kuxov::Shape::isMoreLeft(const std::shared_ptr<Shape>& other) const noexcept
{
  return this->x_ < other->x_;
}

bool kuxov::Shape::isUpper(const std::shared_ptr<Shape>& other) const noexcept
{
  return this->y_ > other->y_;
}

double kuxov::Shape::getX() const noexcept
{
  return x_;
}

double kuxov::Shape::getY() const noexcept
{
  return y_;
}
