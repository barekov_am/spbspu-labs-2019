#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "shape.hpp"

namespace kuxov
{
  class Square : public Shape
  {
  public:
    Square(int x, int y);
    virtual void draw(std::ostream& stream) const override;
  };
}

#endif
