#ifndef TASKS_HPP
#define TASKS_HPP

namespace tasks
{
  void taskOne();
  void taskTwo();
}

#endif
