#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

int main()
{
  kuxov::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  kuxov::Shape * shapePtr = &rectangle;
  std::cout << "FOR RECTANGLE\n";
  shapePtr->print();
  std::cout << "1) Moving along 0x " << 7 << " and 0y " << 7 << std::endl;
  shapePtr->move(7.0, 7.0);
  shapePtr->print();
  std::cout << "2) Moving to the point (" << 9 << "; " << 9 << ")\n";
  shapePtr->move({ 9.0, 9.0 });
  shapePtr->print();
  double k = 9.0;
  std::cout << "3) Scaling a shape " << k << " times\n";
  shapePtr->scale(k);
  shapePtr->print();
  std::cout << std::endl;

  kuxov::Circle circle(5.0, { 3.0, 3.0 });
  shapePtr = &circle;
  std::cout << "FOR CIRCLE\n";
  shapePtr->print();
  std::cout << "1) Moving along 0x " << 9 << " and 0y " << 9 << std::endl;
  shapePtr->move(9.0, 9.0);
  shapePtr->print();
  std::cout << "2) Moving to the point (" << 8 << "; " << 8 << ")\n";
  shapePtr->move({ 8.0, 8.0 });
  shapePtr->print();
  k = 4.0;
  std::cout << "3) Scaling a shape " << k << " times\n";
  shapePtr->scale(k);
  shapePtr->print();
  std::cout << std::endl;

  kuxov::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  shapePtr = &triangle;
  std::cout << "\nFOR TRIANGLE\n";
  shapePtr->print();
  std::cout << "\n1) Moving along 0x " << 4 << " and 0y " << 4 << std::endl;
  shapePtr->move(4.0, 4.0);
  shapePtr->print();
  std::cout << "\n2) Moving to the point (" << 5 << ";" << 5 << ")\n";
  const kuxov::point_t nPosTrian = { 5.0, 5.0 };
  shapePtr->move(nPosTrian);
  shapePtr->print();
  k = 9.0;
  std::cout << "\n3) Scaling a shape " << k << " times\n";
  shapePtr->scale(k);
  shapePtr->print();
  
  kuxov::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  const size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  kuxov::Polygon polygon(quantityOfVertexes, fig);
  shapePtr = &polygon;
  std::cout << "\nFOR POLYGON\n";
  shapePtr->print();
  std::cout << "\n1) Moving along 0x " << 10 << " and 0y " << 10 << std::endl;
  shapePtr->move(10.0, 10.0);
  shapePtr->print();
  std::cout << "\n2) Moving to the point (" << 12 << ";" << 12 << ")\n";
  const kuxov::point_t nPosPoly = { 12.0, 12.0 };
  shapePtr->move(nPosPoly);
  shapePtr->print();
  k = 4.0;
  std::cout << "\n3) Scaling a shape " << k << " times\n";
  shapePtr->scale(k);
  shapePtr->print();
  std::cout << "\n4)Copy constructor\n";
  kuxov::Polygon copyConstructor(polygon);
  shapePtr = &copyConstructor;
  shapePtr->print();
  std::cout << "\n5)Copy assignment\n";
  kuxov::Polygon copyAssignment;
  shapePtr = &copyAssignment;
  copyAssignment = polygon;
  shapePtr->print();
  std::cout << "\n6)Move constructor of polygon\n";
  kuxov::Polygon moveConstructor(std::move(copyAssignment));
  shapePtr = &moveConstructor;
  shapePtr->print();
  std::cout << "\n7)Move assignment of polygon\n";
  kuxov::Polygon moveAssignment;
  shapePtr = &moveAssignment;
  moveAssignment = (std::move(polygon));
  shapePtr->print();

  return 0;
}
