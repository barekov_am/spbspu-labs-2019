#include <exception>

#include "task.hpp"

int main()
{
  try
  {
    kuxov::task();
  }
  catch (std::exception& exc)
  {
    std::cerr << exc.what();
    return 1;
  }
  return 0;
}
