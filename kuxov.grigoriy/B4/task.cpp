#include "task.hpp"

bool checkInputString(const std::string& str)
{
  int commas = 0;
  for (size_t i = 0; i < str.length(); i++)
  {
    if (str[i] == ',')
    {
      commas++;
    }
  }
  return commas == kuxov::details::COMMAS_NUMBER;
}

bool checkInRange(int num, int min, int max)
{
  return num <= max && num >= min;
}

kuxov::DataStruct parseInputString(const std::string& str)
{
  std::istringstream is(str);

  if (!checkInputString(str))
  {
    throw std::invalid_argument("Error! Incorrect input!");
  }

  int key1 = 0, key2 = 0;
  char comma;
  std::string s;

  is >> key1 >> comma >> key2 >> comma;
  s = str.substr(str.find_last_of(',') + 1);

  if (is.fail())
  {
    throw std::ios::failure("Error! Reading from string failed!");
  }

  if (!checkInRange(key1, kuxov::details::MIN_VAL, kuxov::details::MAX_VAL)
    || !checkInRange(key2, kuxov::details::MIN_VAL, kuxov::details::MAX_VAL))
  {
    throw std::invalid_argument("Error! Key value must be in declared range!");
  }

  return kuxov::DataStruct{ key1, key2, s };
}

void readVector(std::vector<kuxov::DataStruct>& vec)
{
  std::string line;
  while (std::getline(std::cin, line))
  {
    vec.push_back(parseInputString(line));
  }
}

void printVector(std::vector<kuxov::DataStruct>& vec)
{
  for (auto v : vec)
  {
    std::cout << v.key1 << "," << v.key2 << "," << v.str << std::endl;
  }
}

bool comparator(const kuxov::DataStruct& first, const kuxov::DataStruct& second)
{
  if (first.key1 < second.key1)
  {
    return true;
  }

  if (first.key1 == second.key1)
  {
    if (first.key2 == second.key2)
    {
      if (first.str.size() == second.str.size())
      {
        return (first.str < second.str);
      }
      return (first.str.size() < second.str.size());
    }
    return (first.key2 < second.key2);
  }
  return false;
}

void kuxov::task()
{
  std::vector<kuxov::DataStruct> vec;
  readVector(vec);
  std::sort(vec.begin(), vec.end(), comparator);
  printVector(vec);
}
