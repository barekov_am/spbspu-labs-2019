#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separation.hpp"

BOOST_AUTO_TEST_SUITE(separationTest)

BOOST_AUTO_TEST_CASE(checkCorrectCrossing)
{
  kuxov::Rectangle testRectangle({ 4.0, 2.0, {5.0, 5.0} });
  kuxov::Rectangle crossingTestRectangle({ 6.0, 2.0, {8.0, 5.0} });
  kuxov::Circle testCircle(1.0, { 11.0, 5.0 });

  const bool falseResult = kuxov::cross(testRectangle.getFrameRect(), testCircle.getFrameRect());
  const bool trueResult1 = kuxov::cross(testRectangle.getFrameRect(), crossingTestRectangle.getFrameRect());
  const bool trueResult2 = kuxov::cross(testCircle.getFrameRect(), crossingTestRectangle.getFrameRect());

  BOOST_CHECK_EQUAL(falseResult, false);
  BOOST_CHECK_EQUAL(trueResult1, true);
  BOOST_CHECK_EQUAL(trueResult2, true);

}

BOOST_AUTO_TEST_CASE(checkCorrectSeparation)
{
  kuxov::Rectangle testRectangle({ 3.0, 3.0, {1.0, 1.0} });
  kuxov::Rectangle crossingTestRectangle({ 3.0, 3.0, {1.0, 1.0} });
  kuxov::Circle testCircle(5.0, { 13.0, 13.0 });
  kuxov::shap3 rectanglePtr = std::make_shared<kuxov::Rectangle>(testRectangle);
  kuxov::shap3 rectangleCrossingPtr = std::make_shared<kuxov::Rectangle>(crossingTestRectangle);
  kuxov::shap3 circlePtr = std::make_shared<kuxov::Circle>(testCircle);

  kuxov::CompositeShape testCompositeShape(rectanglePtr);
  testCompositeShape.add(rectangleCrossingPtr);
  testCompositeShape.add(circlePtr);

  kuxov::Matrix testMatrix = kuxov::part(testCompositeShape);

  BOOST_CHECK(testMatrix[0][0] == rectanglePtr);
  BOOST_CHECK(testMatrix[0][1] == circlePtr);
  BOOST_CHECK(testMatrix[1][0] == rectangleCrossingPtr);
}

BOOST_AUTO_TEST_SUITE_END()
