#define _USE_MATH_DEFINES
#include "circle.hpp"

#include <iostream>
#include <stdexcept>
#include <math.h>


kuxov::Circle::Circle(const double radius, const kuxov::point_t & center) :
  radius_(radius),
  center_(center)
{
  if (radius_ <= 0.0)
  {
    throw std::invalid_argument("invalid value for circle's radius");
  }
}

double kuxov::Circle::getArea() const
{
  return (M_PI * radius_ * radius_);
}

kuxov::rectangle_t kuxov::Circle::getFrameRect() const
{
  return { 2 * radius_, 2 * radius_, center_ };
}

void kuxov::Circle::move(const kuxov::point_t & point)
{
  center_ = point;
}

void kuxov::Circle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void kuxov::Circle::print() const
{
  std::cout << "Area of circle: " << getArea() << std::endl;
  std::cout << "Width of frame rectangle: " << getFrameRect().width << std::endl;
  std::cout << "Height of frame rectangle: " << getFrameRect().height << std::endl;
  std::cout << "Center point of frame rectangle: (" << getFrameRect().pos.x
      << "; " << getFrameRect().pos.y << ")" << std::endl;
}

void kuxov::Circle::scale(double k)
{
  if (k <= 0.0)
  {
    throw std::invalid_argument("invalid value for 'k'");
  }
  else
  {
    radius_ *= k;
  }
}

void kuxov::Circle::rotate(const double)
{
}
