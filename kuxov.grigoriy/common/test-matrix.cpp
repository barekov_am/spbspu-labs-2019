#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separation.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithCrossing)
{
  const size_t layersValue = 2;
  const size_t columnsValue = 1;

  kuxov::Rectangle testRectangle({ 3.0, 3.0, {1.0, 1.0} });
  kuxov::Circle testCircle(5.0, { 3.0, 3.0 });
  kuxov::shap3 rectanglePtr = std::make_shared<kuxov::Rectangle>(testRectangle);
  kuxov::shap3 circlePtr = std::make_shared<kuxov::Circle>(testCircle);

  kuxov::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  kuxov::Matrix testMatrix = kuxov::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithoutCrossing)
{
  const size_t layersValue = 1;
  const size_t columnsValue = 2;

  kuxov::Rectangle testRectangle({ 3.0, 3.0, {1.0, 1.0} });
  kuxov::Circle testCircle(5.0, { 13.0, 3.0 });
  kuxov::shap3 rectanglePtr = std::make_shared<kuxov::Rectangle>(testRectangle);
  kuxov::shap3 circlePtr = std::make_shared<kuxov::Circle>(testCircle);

  kuxov::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  kuxov::Matrix testMatrix = kuxov::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  kuxov::Rectangle testRectangle({ 3.0, 3.0, {1.0, 1.0} });
  kuxov::Circle testCircle(5.0, { 3.0, 3.0 });
  kuxov::shap3 rectanglePtr = std::make_shared<kuxov::Rectangle>(testRectangle);
  kuxov::shap3 circlePtr = std::make_shared<kuxov::Circle>(testCircle);

  kuxov::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  kuxov::Matrix testMatrix = kuxov::part(testCompositeShape);

  BOOST_CHECK_THROW(testMatrix[5][0], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-5][0], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkCopyAndMove)
{
  kuxov::Rectangle testRectangle({ 3.0, 3.0, {1.0, 1.0} });
  kuxov::Circle testCircle(5.0, { 3.0, 3.0 });
  kuxov::shap3 rectanglePtr = std::make_shared<kuxov::Rectangle>(testRectangle);
  kuxov::shap3 circlePtr = std::make_shared<kuxov::Circle>(testCircle);

  kuxov::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  kuxov::Matrix testMatrix = kuxov::part(testCompositeShape);
  kuxov::Matrix copyMatrix(testMatrix);
  kuxov::Matrix moveMatrix(std::move(testMatrix));

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyMatrix);
}

BOOST_AUTO_TEST_SUITE_END()
