#define _USE_MATH_DEFINES
#include "rectangle.hpp"

#include <iostream>
#include <math.h>
#include <cassert>

const double FullAngle = 360.0;

kuxov::Rectangle::Rectangle(const kuxov::rectangle_t & rect) :
  rect_(rect),
  angle_(0.0)
{
  if (rect_.width <= 0.0)
  {
    throw std::invalid_argument("invalid value for rectangle's width");
  }
  if (rect_.height <= 0.0)
  {
    throw std::invalid_argument("invalid value for rectangle's height");
  }
}

double kuxov::Rectangle::getArea() const
{
  return (rect_.width * rect_.height);
}

kuxov::rectangle_t kuxov::Rectangle::getFrameRect() const
{
  const double radAngle = angle_ * M_PI / 180;
  const double width = rect_.width * fabs(cos(radAngle)) + rect_.height * fabs(sin(radAngle));
  const double height = rect_.width * fabs(sin(radAngle)) + rect_.height * fabs(cos(radAngle));
  
  return {width, height, rect_.pos};
}

void kuxov::Rectangle::move(const kuxov::point_t & point)
{
  rect_.pos = point;
}

void kuxov::Rectangle::move(const double dx, const double dy)
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

void kuxov::Rectangle::print() const
{
  std::cout << "Area of rectangle: " << getArea() << std::endl;
  std::cout << "Width of frame rectangle: " << rect_.width << std::endl;
  std::cout << "Height of frame rectangle: " << rect_.height << std::endl;
  std::cout << "Center point of frame rectangle: (" << rect_.pos.x
      << "; " << rect_.pos.y << ")" << std::endl;
}

void kuxov::Rectangle::scale(double k)
{
  if (k <= 0.0)
  {
    throw std::invalid_argument("Coefficient of scale must be a positive number");
  }
  else
  {
    rect_.width *= k;
    rect_.height *= k;
  }    
}

void kuxov::Rectangle::rotate(const double angle)
{
  angle_ += angle;

  if (angle_ < 0.0)
  {
    angle_ = FullAngle + fmod(angle_, FullAngle);
  }
  else
  {
    angle_ = fmod(angle_, FullAngle);
  }
}

