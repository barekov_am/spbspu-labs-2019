#define _USE_MATH_DEFINES
#include "composite-shape.hpp"

#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <cmath>

const double FullAngle = 360.0;

kuxov::CompositeShape::CompositeShape() :
  size_(0),
  shapeArray_(nullptr),
  angle_(0.0)
{
}

kuxov::CompositeShape::CompositeShape(const CompositeShape & ptr) :
  size_(ptr.size_),
  shapeArray_(std::make_unique<shap3[]>(ptr.size_)),
  angle_(ptr.angle_)
{
  for (size_t i = 0; i < size_; ++i)
  {
    shapeArray_[i] = ptr.shapeArray_[i];
  }
}

kuxov::CompositeShape::CompositeShape(CompositeShape && ptr) :
  size_(ptr.size_),
  shapeArray_(std::move(ptr.shapeArray_)),
  angle_(ptr.angle_)
{
  ptr.size_ = 0;
}

kuxov::CompositeShape::CompositeShape(const shap3 & shape) :
  size_(1),
  shapeArray_(std::make_unique<shap3[]>(size_)),
  angle_(0.0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape can`t be a nullptr");
  }

  shapeArray_[0] = shape;
}

kuxov::CompositeShape & kuxov::CompositeShape::operator =(const CompositeShape & ptr)
{
  if (this != &ptr)
  {
    size_ = ptr.size_;
    angle_ = ptr.angle_;
    dynamicArr tmpArray = std::make_unique<shap3[]>(size_);

    for (size_t i = 0; i < size_; i++)
    {
      tmpArray[i] = ptr.shapeArray_[i];
    }
    shapeArray_.swap(tmpArray);
  }

  return *this;
}

kuxov::CompositeShape & kuxov::CompositeShape::operator =(CompositeShape && ptr)
{
  if (this != &ptr)
  {
    size_ = ptr.size_;
    angle_ = ptr.angle_;
    shapeArray_ = std::move(ptr.shapeArray_);
    ptr.size_ = 0;
  }

  return *this;
}

kuxov::shap3 kuxov::CompositeShape::operator [](size_t index) const
{
  if (size_ <= index)
  {
    throw std::out_of_range("Index is out of range");
  }

  return shapeArray_[index];
}

double kuxov::CompositeShape::getArea() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  double area = 0.0;

  for (size_t i = 0; i < size_; i++)
  {
    area += shapeArray_[i]->getArea();
  }

  return area;
}

kuxov::rectangle_t kuxov::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  rectangle_t tmpFrame = shapeArray_[0]->getFrameRect();
  double minX = tmpFrame.pos.x - tmpFrame.width / 2;
  double minY = tmpFrame.pos.y - tmpFrame.height / 2;
  double maxX = tmpFrame.pos.x + tmpFrame.width / 2;
  double maxY = tmpFrame.pos.y + tmpFrame.height / 2;

  for (size_t i = 0; i < size_; i++)
  {
    tmpFrame = shapeArray_[i]->getFrameRect();
    minX = std::min(tmpFrame.pos.x - tmpFrame.width / 2, minX);
    minY = std::min(tmpFrame.pos.y - tmpFrame.height / 2, minY);
    maxX = std::max(tmpFrame.pos.x + tmpFrame.width / 2, maxX);
    maxY = std::max(tmpFrame.pos.y + tmpFrame.height / 2, maxY);
  }

  return rectangle_t{ (maxX - minX), (maxY - minY), {(minX + maxX) / 2, (minY + maxY) / 2} };
}

void kuxov::CompositeShape::move(const point_t & point)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  point_t tmpFrame = getFrameRect().pos;
  double dx = point.x - tmpFrame.x;
  double dy = point.y - tmpFrame.y;

  move(dx, dy);
}

void kuxov::CompositeShape::move(const double dx, const double dy)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  for (size_t i = 0; i < size_; i++)
  {
    shapeArray_[i]->move(dx, dy);
  }
}

void kuxov::CompositeShape::print() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  std::cout << "Amount of array is: " << getSize() << "\n";
  for (size_t i = 0; i < size_; i++)
  {
    std::cout << "SHAPE # " << i + 1 << std::endl;
    std::cout << "Area : " << shapeArray_[i]->getArea() << std::endl;
    std::cout << "Width of frame rectangle: " << shapeArray_[i]->getFrameRect().width << std::endl;
    std::cout << "Height of frame rectangle: " << shapeArray_[i]->getFrameRect().height << std::endl;
    std::cout << "Center point : (" << shapeArray_[i]->getFrameRect().pos.x
      << "; " << shapeArray_[i]->getFrameRect().pos.y << ")" << std::endl;
  }
}

void kuxov::CompositeShape::scale(const double coefficient)
{
  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Coefficient of scale must be a positive number");
  }

  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  point_t tmpFrame = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++)
  {
    point_t centerShape = shapeArray_[i]->getFrameRect().pos;
    double dx = (centerShape.x - tmpFrame.x) * (coefficient - 1);
    double dy = (centerShape.y - tmpFrame.y) * (coefficient - 1);
    shapeArray_[i]->move(dx, dy);
    shapeArray_[i]->scale(coefficient);
  }
}

size_t kuxov::CompositeShape::getSize() const
{
  return size_;
}

void kuxov::CompositeShape::add(const shap3 & other)
{
  if (other == nullptr)
  {
    throw std::invalid_argument("New adding shape can`t be a nullptr");
  }

  dynamicArr tmpShape = std::make_unique<shap3[]>(size_ + 1);
  for (size_t i = 0; i < size_; i++)
  {
    tmpShape[i] = shapeArray_[i];
  }

  tmpShape[size_] = other;
  size_++;
  shapeArray_.swap(tmpShape);
}

void kuxov::CompositeShape::remove(size_t index)
{
  if (size_ <= index)
  {
    throw std::out_of_range("Index is out of range");
  }

  size_--;

  for (size_t i = index; i < size_; i++)
  {
    shapeArray_[i] = shapeArray_[i + 1];
  }
}

void kuxov::CompositeShape::rotate(const double angle)
{
  angle_ += angle;

  if (angle_ < 0.0)
  {
    angle_ = FullAngle + fmod(angle_, FullAngle);
  }
  else
  {
    angle_ = fmod(angle_, FullAngle);
  }

  const double radAngle = angle_ * M_PI / 180;
  const point_t centre = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++)
  {
    const double oldX = shapeArray_[i]->getFrameRect().pos.x - centre.x;
    const double oldY = shapeArray_[i]->getFrameRect().pos.y - centre.y;

    const double newX = oldX * fabs(cos(radAngle)) - oldY * fabs(sin(radAngle));
    const double newY = oldX * fabs(sin(radAngle)) + oldY * fabs(cos(radAngle));

    shapeArray_[i]->move({ centre.x + newX, centre.y + newY });
    shapeArray_[i]->rotate(angle);
  }
}

kuxov::dynamicArr kuxov::CompositeShape::getList() const
{
  dynamicArr tmpArray(std::make_unique<shap3[]>(size_));

  for (size_t i = 0; i < size_; i++)
  {
    tmpArray[i] = shapeArray_[i];
  }

  return tmpArray;
}
