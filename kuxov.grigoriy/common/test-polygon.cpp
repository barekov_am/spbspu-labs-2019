#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "polygon.hpp"

const double EPS = 0.001;

BOOST_AUTO_TEST_SUITE(polygonTest)

BOOST_AUTO_TEST_CASE(firstMove)
{
  kuxov::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t qV = sizeof(fig) / sizeof(fig[0]);
  kuxov::Polygon polygon(qV, fig);
  const double oldA = polygon.getArea();
  polygon.move({ 5.0, 5.0 });
  BOOST_CHECK_CLOSE(oldA, polygon.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(secondMove)
{
  kuxov::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  kuxov::Polygon polygon(quantityOfVertexes, fig);
  const double oldArea = polygon.getArea();
  polygon.move(5.0, 5.0);
  BOOST_CHECK_CLOSE(oldArea, polygon.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(scale)
{
  kuxov::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  kuxov::Polygon polygon(quantityOfVertexes, fig);
  double oldArea = polygon.getArea();
  double k = 9;
  polygon.scale(9);
  BOOST_CHECK_CLOSE(oldArea * k * k, polygon.getArea(), EPS);
  oldArea = polygon.getArea();
  k = 0.5;
  polygon.scale(0.5);
  BOOST_CHECK_CLOSE(oldArea * k * k, polygon.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(errCoeff)
{
  kuxov::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  kuxov::Polygon polygon(quantityOfVertexes, fig);
  BOOST_CHECK_THROW(polygon.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(polygon.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testConstructor)
{
  kuxov::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  size_t zero = 0;
  kuxov::Polygon polygon1(quantityOfVertexes, fig);
  kuxov::Polygon polygon2(polygon1);
  BOOST_CHECK_EQUAL(polygon1.getQ(), polygon2.getQ());

  const size_t oldQuantity = polygon2.getQ();
  kuxov::Polygon polygon3(std::move(polygon2));
  BOOST_CHECK_EQUAL(polygon2.getQ(), zero);
  BOOST_CHECK_EQUAL(polygon3.getQ(), oldQuantity);
}

BOOST_AUTO_TEST_CASE(testOperator)
{
  kuxov::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  size_t zero = 0;
  kuxov::Polygon polygon1(quantityOfVertexes, fig);
  kuxov::Polygon polygon2;
  polygon2 = polygon1;
  BOOST_CHECK_EQUAL(polygon1.getQ(), polygon2.getQ());

  const size_t oldQuantity = polygon2.getQ();
  kuxov::Polygon polygon3 = std::move(polygon2);
  BOOST_CHECK_EQUAL(polygon2.getQ(), zero);
  BOOST_CHECK_EQUAL(polygon3.getQ(), oldQuantity);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  kuxov::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  kuxov::Polygon polygon(quantityOfVertexes, fig);

  double oldArea = polygon.getArea();
  const double angle = 90;

  polygon.rotate(angle);

  BOOST_CHECK_CLOSE(oldArea, polygon.getArea(), EPS);
}

BOOST_AUTO_TEST_SUITE_END()
