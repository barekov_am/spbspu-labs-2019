#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "composite-shape.hpp"
#include "polygon.hpp"
#include "triangle.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double EPS = 0.001;

BOOST_AUTO_TEST_SUITE(testCompositeShape)

BOOST_AUTO_TEST_CASE(firstMove)
{
  kuxov::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  kuxov::Circle circle(6.0, { 3.0, 3.0 });
  kuxov::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  kuxov::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t qV = sizeof(fig) / sizeof(fig[0]);
  kuxov::Polygon polygon(qV, fig);
  kuxov::shap3 rectanglePtr = std::make_shared<kuxov::Rectangle>(rectangle);
  kuxov::shap3 circlePtr = std::make_shared<kuxov::Circle>(circle);
  kuxov::shap3 trianglePtr = std::make_shared<kuxov::Triangle>(triangle);
  kuxov::shap3 polygonPtr = std::make_shared<kuxov::Polygon>(polygon);
  kuxov::CompositeShape compositeShape(rectanglePtr);
  compositeShape.add(circlePtr);
  compositeShape.add(trianglePtr);
  compositeShape.add(polygonPtr);

  const kuxov::rectangle_t oldFR = compositeShape.getFrameRect();
  const double oldA = compositeShape.getArea();
  compositeShape.move({ 50.0, 50.0 });
  BOOST_CHECK_CLOSE(oldFR.width, compositeShape.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(oldFR.height, compositeShape.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(oldA, compositeShape.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(secondMove)
{
  kuxov::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  kuxov::Circle circle(6.0, { 3.0, 3.0 });
  kuxov::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  kuxov::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t qV = sizeof(fig) / sizeof(fig[0]);
  kuxov::Polygon polygon(qV, fig);
  kuxov::shap3 rectanglePtr = std::make_shared<kuxov::Rectangle>(rectangle);
  kuxov::shap3 circlePtr = std::make_shared<kuxov::Circle>(circle);
  kuxov::shap3 trianglePtr = std::make_shared<kuxov::Triangle>(triangle);
  kuxov::shap3 polygonPtr = std::make_shared<kuxov::Polygon>(polygon);
  kuxov::CompositeShape compositeShape(rectanglePtr);
  compositeShape.add(circlePtr);
  compositeShape.add(trianglePtr);
  compositeShape.add(polygonPtr);

  const kuxov::rectangle_t oldFR = compositeShape.getFrameRect();
  const double oldA = compositeShape.getArea();
  compositeShape.move(10.0, 10.0);
  BOOST_CHECK_CLOSE(oldFR.width, compositeShape.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(oldFR.height, compositeShape.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(oldA, compositeShape.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(errValues)
{
  kuxov::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  kuxov::Circle circle(6.0, { 3.0, 3.0 });
  kuxov::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  kuxov::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t qV = sizeof(fig) / sizeof(fig[0]);
  kuxov::Polygon polygon(qV, fig);
  kuxov::shap3 rectanglePtr = std::make_shared<kuxov::Rectangle>(rectangle);
  kuxov::shap3 circlePtr = std::make_shared<kuxov::Circle>(circle);
  kuxov::shap3 trianglePtr = std::make_shared<kuxov::Triangle>(triangle);
  kuxov::shap3 polygonPtr = std::make_shared<kuxov::Polygon>(polygon);
  kuxov::CompositeShape compositeShape(rectanglePtr);
  compositeShape.add(circlePtr);
  compositeShape.add(trianglePtr);
  compositeShape.add(polygonPtr);

  BOOST_CHECK_THROW(compositeShape.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.remove(5), std::out_of_range);
  BOOST_CHECK_THROW(compositeShape.remove(-2), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(scale)
{
  kuxov::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  kuxov::Circle circle(6.0, { 3.0, 3.0 });
  kuxov::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  kuxov::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t qV = sizeof(fig) / sizeof(fig[0]);
  kuxov::Polygon polygon(qV, fig);
  kuxov::shap3 rectanglePtr = std::make_shared<kuxov::Rectangle>(rectangle);
  kuxov::shap3 circlePtr = std::make_shared<kuxov::Circle>(circle);
  kuxov::shap3 trianglePtr = std::make_shared<kuxov::Triangle>(triangle);
  kuxov::shap3 polygonPtr = std::make_shared<kuxov::Polygon>(polygon);
  kuxov::CompositeShape compositeShape(rectanglePtr);
  compositeShape.add(circlePtr);
  compositeShape.add(trianglePtr);
  compositeShape.add(polygonPtr);

  const double oldA = compositeShape.getArea();
  const double k = 9;
  compositeShape.scale(9);
  BOOST_CHECK_CLOSE(oldA * k * k, compositeShape.getArea(), EPS);
  const double oldAA = compositeShape.getArea();
  const double kk = 0.5;
  compositeShape.scale(0.5);
  BOOST_CHECK_CLOSE(oldAA * kk * kk, compositeShape.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  kuxov::Rectangle testRectangle({ 2.0, 4.0, {5.0, 5.0} });
  kuxov::Circle testCircle(1.0, {5.0, 5.0});
  kuxov::shap3 rectanglePtr = std::make_shared<kuxov::Rectangle>(testRectangle);
  kuxov::shap3 circlePtr = std::make_shared<kuxov::Circle>(testCircle);

  kuxov::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.add(rectanglePtr);

  const double testArea = testCompositeShape.getArea();
  const kuxov::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double angle = 90;

  testCompositeShape.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testCompositeShape.getFrameRect().width / 2, EPS);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCompositeShape.getFrameRect().height * 2, EPS);
  BOOST_CHECK_CLOSE(testArea, testCompositeShape.getArea(), EPS);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testCompositeShape.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testCompositeShape.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
