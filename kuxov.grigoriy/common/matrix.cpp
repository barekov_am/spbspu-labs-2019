#include "matrix.hpp"

#include <stdexcept>
#include <iostream>

kuxov::Matrix::Matrix() :
  lines_(0),
  columns_(0),
  list_()
{
}

kuxov::Matrix::Matrix(const Matrix & ptr) :
  lines_(ptr.lines_),
  columns_(ptr.columns_),
  list_(std::make_unique<shap3[]>(ptr.lines_ * ptr.columns_))
{
  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    list_[i] = ptr.list_[i];
  }
}

kuxov::Matrix::Matrix(Matrix && ptr) :
  lines_(ptr.lines_),
  columns_(ptr.columns_),
  list_(std::move(ptr.list_))
{
  ptr.lines_ = 0;
  ptr.columns_ = 0;
}

kuxov::Matrix & kuxov::Matrix::operator =(const Matrix & ptr)
{
  if (this != &ptr)
  {
    lines_ = ptr.lines_;
    columns_ = ptr.columns_;
    dynamicArr ptrMatrix(std::make_unique<shap3[]>(ptr.lines_ * ptr.columns_));

    for (size_t i = 0; i < (lines_ * columns_); i++)
    {
      ptrMatrix[i] = ptr.list_[i];
    }
    list_.swap(ptrMatrix);
  }

  return *this;
}

kuxov::Matrix & kuxov::Matrix::operator =(Matrix && ptr)
{
  if (this != &ptr)
  {
    lines_ = ptr.lines_;
    columns_ = ptr.columns_;
    list_ = std::move(ptr.list_);
    ptr.lines_ = 0;
    ptr.columns_ = 0;
  }

  return *this;
}

kuxov::dynamicArr kuxov::Matrix::operator [](size_t index) const
{
  if (lines_ <= index)
  {
    throw std::out_of_range("Index is out of range");
  }

  dynamicArr ptrMatrix(std::make_unique<shap3[]>(columns_));

  for (size_t i = 0; i < columns_; i++)
  {
    ptrMatrix[i] = list_[index * columns_ + i];
  }

  return ptrMatrix;
}

bool kuxov::Matrix::operator ==(const Matrix & ptr) const
{
  if ((lines_ != ptr.lines_) || (columns_ != ptr.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    if (list_[i] != ptr.list_[i])
    {
      return false;
    }
  }

  return true;
}

bool kuxov::Matrix::operator !=(const Matrix & ptr) const
{
  return !(*this == ptr);
}

size_t kuxov::Matrix::getLines() const
{
  return lines_;
}

size_t kuxov::Matrix::getColumns() const
{
  return columns_;
}

void kuxov::Matrix::add(const shap3 & ptr, size_t line, size_t column)
{
  size_t ptrLines;
  size_t ptrColumns;

  if (line == lines_)
  {
    ptrLines = lines_ + 1;
  }
  else
  {
    ptrLines = lines_;
  }

  if (column == columns_)
  {
    ptrColumns = columns_ + 1;
  }
  else
  {
    ptrColumns = columns_;
  }

  dynamicArr ptrList(std::make_unique<shap3[]>(ptrLines * ptrColumns));

  for (size_t i = 0; i < ptrLines; i++)
  {
    for (size_t j = 0; j < ptrColumns; j++)
    {
      if ((i == lines_) || (j == columns_))
      {
        ptrList[i * ptrColumns + j] = nullptr;
      }
      else
      {
        ptrList[i * ptrColumns + j] = list_[i * columns_ + j];
      }
    }
  }

  ptrList[line * ptrColumns + column] = ptr;
  list_.swap(ptrList);
  lines_ = ptrLines;
  columns_ = ptrColumns;
}

void kuxov::Matrix::print() const
{
  std::cout << "Value of lines: " << lines_ << "\n";
  std::cout << "Value of columns: " << columns_ << "\n";

  for (size_t i = 0; i < lines_; i++)
  {
    for (size_t j = 0; j < columns_; j++)
    {
      if (list_[i * columns_ + j] != nullptr)
      {
        std::cout << "On layer number " << i + 1 << " and position " << j + 1 << " there is figure:\n";
        list_[i * columns_ + j]->print();
      }
    }
  }
}
