#define _USE_MATH_DEFINES
#include "triangle.hpp"

#include <iostream>
#include <stdexcept>
#include <math.h>
#include <algorithm>

kuxov::Triangle::Triangle(const kuxov::point_t & vA, const kuxov::point_t & vB, const kuxov::point_t & vC) :
  pos_({ (vA.x + vB.x + vC.x) / 3, (vA.y + vB.y + vC.y) / 3 }),
  vA_(vA),
  vB_(vB),
  vC_(vC)
{
  const double areaTriangle = getArea();
  if (areaTriangle <= 0.0)
  {
    throw std::invalid_argument("invalid value for triangle's points");
  }
}

double kuxov::Triangle::getArea() const
{
  return (std::fabs(vA_.x - vC_.x) * (vB_.y - vC_.y) 
      - std::fabs(vB_.x - vC_.x)
        * (vA_.y - vC_.y)) / 2;
}

kuxov::rectangle_t kuxov::Triangle::getFrameRect() const
{
  const double maxX = std::max(std::max(vA_.x, vB_.x), vC_.x);
  const double minX = std::min(std::min(vA_.x, vB_.x), vC_.x);
  const double maxY = std::max(std::max(vA_.y, vB_.y), vC_.y);
  const double minY = std::min(std::min(vA_.y, vB_.y), vC_.y);
  const double width = maxX - minX;
  const double height = maxY - minY;
  const point_t position = { minX + width / 2, minY + height / 2 };

  return { width, height, position };
}

void kuxov::Triangle::print() const
{
  std::cout << "Area of triangle: " << getArea();
  std::cout << "\nWidth of frame rectangle: " << getFrameRect().width;
  std::cout << "\nHeight of frame rectangle: " << getFrameRect().height;
  std::cout << "\nTriangle's A : (" << vA_.x << ';' << vA_.y << ')';
  std::cout << "\nTriangle's B : (" << vB_.x << ';' << vB_.y << ')';
  std::cout << "\nTriangle's C : (" << vC_.x << ';' << vC_.y << ')';
  std::cout << "\nTriangle's center : (" << pos_.x << ';' << pos_.y << ')';
}

void kuxov::Triangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;

  vA_.x += dx;
  vA_.y += dy;
  vB_.x += dx;
  vB_.y += dy;
  vC_.x += dx;
  vC_.y += dy;
}

void kuxov::Triangle::move(const kuxov::point_t & nPos)
{
  const point_t diff = { nPos.x - pos_.x, nPos.y - pos_.y };
  move(diff.x, diff.y);
}

void kuxov::Triangle::scale(double k)
{
  if (k <= 0.0)
  {
    throw std::invalid_argument("invalid value for 'k'");
  }
  else
  {
    vA_.x *= k;
    vA_.y *= k;
    vB_.x *= k;
    vB_.y *= k;
    vC_.x *= k;
    vC_.y *= k;
    pos_.x *= k;
    pos_.y *= k;
  }
}

void kuxov::Triangle::rotate(const double angle)
{
  vA_ = rotate(vA_, angle);
  vB_ = rotate(vB_, angle);
  vC_ = rotate(vC_, angle);
}

kuxov::point_t kuxov::Triangle::rotate(const point_t & point, double angle) const
{
  angle *= M_PI / 180;
  return point_t{ pos_.x + (point.x - pos_.x) * cos(angle) - (point.y - pos_.y) * sin(angle),
      pos_.y + (point.y - pos_.y) * cos(angle) + (point.x - pos_.x) * sin(angle) };

}
