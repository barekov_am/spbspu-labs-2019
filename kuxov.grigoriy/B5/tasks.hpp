#ifndef TASKS_HPP
#define TASKS_HPP
#include <iostream>

namespace kuxov
{
  void firstTask();
  void secondTask();
}

#endif
