#include <sstream>
#include <algorithm>
#include <numeric>

#include "tasks.hpp"
#include "details.hpp"

details::Shape readShape(const std::string& line)
{
  std::istringstream sStream(line);

  int vertNum = 0;
  sStream >> vertNum;

  if (vertNum < 3)
  {
    throw std::invalid_argument("Incorrect number of vertices!");
  }

  details::Shape shape;
  for (int i(0); i < vertNum; i++)
  {
    details::Point tmp_point = { 0,0 };

    sStream.ignore(line.length(), '(');
    sStream >> tmp_point.x;

    sStream.ignore(line.length(), ';');
    sStream >> tmp_point.y;

    sStream.ignore(line.length(), ')');
    shape.push_back(tmp_point);
  }

  if (sStream.fail())
  {
    throw std::invalid_argument("Incorrect input!");
  }

  return shape;
}

size_t getVerticesAmount(const details::ShapesContainer& shapesContainer) noexcept
{
  return static_cast<size_t>(std::accumulate(shapesContainer.begin(), shapesContainer.end(), 0,
    [](size_t total, const details::Shape& shape)
    {
      return total + shape.size();
    }));
}

bool comparePointsX(const details::Point& rhs, const details::Point& lhs) noexcept
{
  return rhs.x < lhs.x;
}

bool comparePointsY(const details::Point& rhs, const details::Point& lhs) noexcept
{
  return rhs.y < lhs.y;
}

int squareDistance(const details::Point& rhs, const details::Point& lhs) noexcept
{
  return (rhs.x - lhs.x) * (rhs.x - lhs.x) + (rhs.y - lhs.y) * (rhs.y - lhs.y);
}

bool isTriangleShape(const details::Shape& shape) noexcept
{
  return shape.size() == details::TRIANGLE_VERT_NUM;
}

inline bool isSquareShape(const details::Shape& shape)
{
  return isRectangleShape(shape) && squareDistance(shape[0], shape[1]) == squareDistance(shape[1], shape[2]);
}

inline bool isRectangleShape(const details::Shape& shape)
{
  return shape.size() == 4 && squareDistance(shape[0], shape[2]) == squareDistance(shape[1], shape[3])
    && squareDistance(shape[1], shape[2]) == squareDistance(shape[0], shape[3])
    && squareDistance(shape[0], shape[1]) == squareDistance(shape[2], shape[3]);
}

size_t getTrianglesAmount(const details::ShapesContainer& shapesContainer) noexcept
{
  return static_cast<size_t>(std::count_if(shapesContainer.begin(), shapesContainer.end(), isTriangleShape));
}

size_t getSquaresAmount(const details::ShapesContainer& shapesContainer) noexcept
{
  return static_cast<size_t>(std::count_if(shapesContainer.begin(), shapesContainer.end(), isSquareShape));
}

size_t getRectanglesAmount(const details::ShapesContainer& shapesContainer) noexcept
{
  return static_cast<size_t>(std::count_if(shapesContainer.begin(), shapesContainer.end(), isRectangleShape));
}

void deletePentagons(details::ShapesContainer& shapesContainer) noexcept
{
  shapesContainer.erase(
    std::remove_if(shapesContainer.begin(), shapesContainer.end(),
      [](const details::Shape& shape) { return shape.size() == details::PENTAGON_VERT_NUM; }),
    shapesContainer.end());
}

std::vector<details::Point> makePointVec(const details::ShapesContainer& shapesContainer) noexcept
{
  std::vector<details::Point> pointVec;

  for (const auto& shape : shapesContainer)
  {
    pointVec.push_back(shape[0]);
  }

  return pointVec;
}

void fillShapeContainer(details::ShapesContainer& shapesContainer)
{
  std::string line;

  while (std::getline(std::cin, line))
  {
    line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
    if (line.empty())
    {
      continue;
    }
    shapesContainer.push_back(readShape(line));
  }
}

void reorganizeShapeContainer(details::ShapesContainer& shapesContainer)
{
  auto endOfTriangles = std::partition(shapesContainer.begin(), shapesContainer.end(), isTriangleShape);
  auto endOfSquares = std::partition(endOfTriangles, shapesContainer.end(), isSquareShape);
  std::partition(endOfSquares, shapesContainer.end(), isRectangleShape);
}

void kuxov::secondTask()
{
  details::ShapesContainer shapeContainer;

  fillShapeContainer(shapeContainer);

  size_t vertNumber = getVerticesAmount(shapeContainer);

  size_t triangles = 0, squares = 0, rects = 0;
  triangles = getTrianglesAmount(shapeContainer);
  squares = getSquaresAmount(shapeContainer);
  rects = getRectanglesAmount(shapeContainer);

  deletePentagons(shapeContainer);

  std::vector<details::Point> pointsVec = makePointVec(shapeContainer);

  reorganizeShapeContainer(shapeContainer);

  std::cout << "Vertices: " << vertNumber << std::endl;
  std::cout << "Triangles: " << triangles << std::endl;
  std::cout << "Squares: " << squares << std::endl;
  std::cout << "Rectangles: " << rects << std::endl;

  std::cout << "Points:";
  for (const auto& tmp : pointsVec)
  {
    std::cout << " (" << tmp.x << "; " << tmp.y << ")";
  }
  std::cout << std::endl;

  std::cout << "Shapes:" << std::endl;
  for (const auto& shape : shapeContainer)
  {
    std::cout << shape.size() << " ";
    for (const auto& tmp : shape)
    {
      std::cout << "(" << tmp.x << "; " << tmp.y << ") ";
    }
    std::cout << std::endl;
  }
}
