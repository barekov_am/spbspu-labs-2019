#include <set>
#include <string>

#include "tasks.hpp"

void readText(std::set<std::string>& text)
{
  std::string buffer;

  while (std::cin >> buffer)
  {
    text.emplace(buffer);
  }
}

void writeWords(const std::set<std::string>& text)
{
  for (const auto& tmp : text)
  {
    std::cout << tmp << std::endl;
  }
}

void kuxov::firstTask()
{
  std::set<std::string> words;
  readText(words);
  writeWords(words);
}
