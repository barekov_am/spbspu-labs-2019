#include <iostream>
#include <stdexcept>
#include <string>

void firstTask(const char* direction);
void secondTask(const char* filename);
void thirdTask();
void fourthTask(const char* direction, size_t size);

int main(int argc, char* argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4))
    {
      std::cerr << "Incorrect number of arguments.\n";
      return 1;
    }

    char* ptr = nullptr;
    int taskNumber = std::strtol(argv[1], &ptr, 10);
    if (*ptr != 0x00)
    {
      std::cerr << "Incorrect argument.\n";
      return 1;
    }

    switch (taskNumber)
    {
    case 1:
      if (argc != 3)
      {
        std::cerr << "Incorrect number of arguments.\n";
        return 1;
      }

      firstTask(argv[2]);
      break;

    case 2:
      if (argc != 3)
      {
        std::cerr << "Incorrect number of arguments.\n";
        return 1;
      }

      secondTask(argv[2]);
      break;

    case 3:
      if (argc != 2)
      {
        std::cerr << "Incorrect number of arguments.\n";
        return 1;
      }

      thirdTask();
      break;

    case 4:
      if (argc != 4)
      {
        std::cerr << "Incorrect number of arguments.\n";
        return 1;
      }

      fourthTask(argv[2], std::stoi(argv[3]));
      break;

    default:
      std::cerr << "Incorrect argument.\n";
      return 1;
    }
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << '\n';

    return 1;
  }

  return 0;
}
