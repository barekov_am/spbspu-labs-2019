#include <random>
#include <vector>
#include <ctime>

#include "output.hpp"
#include "sort.hpp"

void fillRandom(double* array, size_t size)
{
  std::mt19937 rng(time(0));
  std::uniform_real_distribution<double> urd(-1.0, 1.0);
  for (size_t i = 0; i < size; ++i)
  {
    array[i] = urd(rng);
  }
}

void fourthTask(const char* direction, size_t size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size must be more than 0.");
  }

  Direction dir = getDir(direction);

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);

  print(vector);
  selectionSort<bracketAcc>(vector, dir);
  print(vector);
}
