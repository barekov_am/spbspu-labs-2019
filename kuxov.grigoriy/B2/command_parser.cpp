#include <sstream>
#include <iostream>

#include "command_parser.hpp"
#include "queue.hpp"

kuxov::details::command_function<std::string> kuxov::details::parseCommand(const std::string& input)
{
  std::stringstream is(input);
  std::string command_str;
  is >> command_str;

  if (command_str == constants::ADD)
  {
    std::string priority_str;
    is >> priority_str;

    ElementPriority priority;
    if (priority_str == constants::LOW)
    {
      priority = ElementPriority::LOW;
    }
    else if (priority_str == constants::NORM)
    {
      priority = ElementPriority::NORMAL;
    }
    else if (priority_str == constants::HIGH)
    {
      priority = ElementPriority::HIGH;
    }
    else
    {
      return [](QueueWithPriority<std::string>&)
      {
        std::cout << constants::INVALID_COM << std::endl;
      };
    }

    is.ignore();
    std::string elem_str;
    std::getline(is >> std::ws, elem_str);

    if (elem_str.empty())
    {
      return [](QueueWithPriority<std::string>&)
      {
        std::cout << constants::INVALID_COM << std::endl;
      };
    }

    return [priority, elem_str](QueueWithPriority<std::string>& queue)
    {
      queue.putElement(elem_str, priority);
    };
  }
  else if (command_str == constants::GET)
  {
    return [](QueueWithPriority<std::string>& queue)
    {
      std::cout << (queue.isEmpty() ? constants::EMPTY : queue.getElement()) << std::endl;
    };
  }
  else if (command_str == constants::ACCEL)
  {
    return [](QueueWithPriority<std::string>& queue)
    {
      queue.accelerate();
    };
  }
  else
  {
    return [](QueueWithPriority<std::string>&)
    {
      std::cout << constants::INVALID_COM << std::endl;
    };
  }
}
