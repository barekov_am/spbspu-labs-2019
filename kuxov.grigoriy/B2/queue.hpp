#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <array>
#include <deque>

namespace kuxov
{
  namespace details
  {
    enum class ElementPriority
    {
      LOW = 0,
      NORMAL = 1,
      HIGH = 2
    };

    template <typename T>
    class QueueWithPriority
    {
    public:

      QueueWithPriority();

      QueueWithPriority(const T& element, ElementPriority priority, size_t num = 1);

      ~QueueWithPriority() = default;

      void putElement(const T& element, ElementPriority priority);

      T getElement();

      bool isEmpty() const;

      void accelerate();

    private:
      static const size_t PRIORITY_NUMBER = 3;
      typename std::array<std::deque<T>, PRIORITY_NUMBER> queue_;
    };
  }
}
#endif
