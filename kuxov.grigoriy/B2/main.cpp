#include <iostream>
#include <string>

#include "tasks.hpp"

int main(int argc, char** argv)
{
  try
  {
    if (argc != 2)
    {
      throw std::invalid_argument("Error!! Invalid number of parameters.");
    }
    switch (std::stoi(argv[1]))
    {
    case 1:
      kuxov::tasks::task1();
      break;
    case 2:
      kuxov::tasks::task2();
      break;
    default:
      throw std::invalid_argument("Error! Invalid number of the task!");
    }
  }
  catch (const std::exception& e)
  {
    std::cout << e.what();
    return 1;
  }

  return 0;
}
