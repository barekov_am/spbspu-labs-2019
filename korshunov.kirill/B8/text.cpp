#include <algorithm>
#include "text.hpp"

const std::size_t MAX_SIZE = 20;
const char HYPHEN = '-';
const char PLUS = '+';
const char POINT = '.';
const char COMMA = ',';

std::string readWord(char s, std::istream& in)
{
  std::string tmpStr;
  tmpStr.push_back(s);

  while (isalpha(in.peek()) || (in.peek() == HYPHEN))
  {
    s = in.get();
    tmpStr.push_back(s);

    if ((s == '-') && (in.peek() == '-'))
    {
      throw std::invalid_argument("More than one hyphen");
    }
  }

  if (tmpStr.size() > MAX_SIZE)
  {
    throw std::invalid_argument("Too long a word");
  }

  return tmpStr;
}

std::string readNumber(char s, std::istream& in)
{
  std::string tmpStr;
  tmpStr.push_back(s);
  int points = 0;

  while (isdigit(in.peek()) || (in.peek() == POINT))
  {
    s = in.get();
    tmpStr.push_back(s);

    if (s == POINT)
    {
      ++points;

      if (points > 1)
      {
        throw std::invalid_argument("More than one point in number");
      }
    }

  }

  if (tmpStr.size() > MAX_SIZE)
  {
    throw std::invalid_argument("Too long a number");
  }

  return tmpStr;
}

std::string readDash(char s, std::istream& in)
{
  std::string tmpStr;
  tmpStr.push_back(s);

  while (in.peek() == HYPHEN)
  {
    s = in.get();
    tmpStr.push_back(s);
  }

  if (tmpStr.size() != 3)
  {
    throw std::invalid_argument("Dash must consist of three hyphens");
  }

  return tmpStr;
}

std::string readPunct(char s)
{
  std::string tmpStr;
  tmpStr.push_back(s);
  return tmpStr;
}

bool isPunct(const std::string& str)
{
  for (auto& i : str)
  {
    if (!std::ispunct(i))
    {
      return false;
    }
  }

  return true;
}

Text::Text(const std::size_t w):
width_(w)
{
}

void Text::readText(std::istream& input)
{
  while (!input.eof())
  {
    char s = input.get();

    if (isalpha(s))
    {
      elements_.push_back(readWord(s, input));
    }
    else if(isdigit(s) || ((s == PLUS || s == HYPHEN) && isdigit(input.peek())))
    {
      elements_.push_back(readNumber(s, input));
    }
    else if(ispunct(s))
    {
      if (elements_.empty())
      {
        throw std::invalid_argument("Text should not begin with punctuation");
      }

      if (isPunct(*std::prev(elements_.end())) && ((s == HYPHEN && (*std::prev(elements_.end()) != ",")) || s != HYPHEN))
      {
        throw std::invalid_argument("More than one punctuation");
      }

      if (s == HYPHEN)
      {
        elements_.push_back(readDash(s, input));
      }
      else
      {
        elements_.push_back(readPunct(s));
      }
    }
  }
}

void Text::printText(std::ostream& out)
{
  if (elements_.empty())
  {
    return;
  }

  if (elements_.size() == 1)
  {
    out << *elements_.begin() << "\n";
    return;
  }

  for (auto el = elements_.begin(); el != std::prev(elements_.end()); ++el)
  {
    if (isPunct((*(el+1))) && (*(el+1) != "---"))
    {
      *el = *el + *std::next(el);
    }
  }

  auto tmp = std::remove_if(elements_.begin(), elements_.end(), [](std::string& str)
  {
    return (ispunct(str[0]) && (str[0]!= '-'));
  });

  elements_.erase(tmp, elements_.end());

  std::string line;
  std::string currentEl;
  std::string nextEl;
  const std::size_t space = 1;

  for (auto el = elements_.begin(); el != elements_.end() - 1; ++el)
  {
    currentEl = *el;
    nextEl = *(el + 1);

    if (line.empty())
    {
      line += currentEl;
    }
    else
    {
      if (nextEl == "---")
      {
        if (line.size() + 2*space + currentEl.size() + nextEl.size() > width_)
        {
          out << line << "\n";
          line = currentEl;
        }
        else
        {
          if (line.size() + space + currentEl.size() > width_)
          {
            out << line << "\n";
            line = currentEl;
          }
          else
          {
            line = line + " " + currentEl;
          }
        }
      }
      else
      {
        if (line.size() + space + currentEl.size() > width_)
        {
          out << line << "\n";
          line = currentEl;
        }
        else
        {
          line = line + " " + currentEl;
        }
      }
    }
  }

  if (line.size() + space + nextEl.size() > width_)
  {
    out << line << "\n";
    line = nextEl;
    out << line << "\n";
  }
  else
  {
    line = line + " " + nextEl;
    out << line << "\n";
  }

}
