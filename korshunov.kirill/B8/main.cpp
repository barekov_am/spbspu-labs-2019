#include <iostream>
#include "text.hpp"

int main(int argc, char * argv[])
{
  try {
    if ((argc > 3) || (argc == 2))
    {
      std::cerr << "Invalid number of arguments";
      return 1;
    }

    std::size_t width = 40;

    if (argc == 3)
    {
      width = std::stoi(argv[2]);

      if (std::string(argv[1]) != "--line-width")
      {
        std::cerr << "Invalid argument";
        return 1;
      }

      if (width < 25)
      {
        std::cerr << "Invalid width";
        return 1;
      }
    }

    Text text(width);
    text.readText(std::cin);
    text.printText(std::cout);

  }
  catch (const std::exception& ex)
  {
    std::cerr << ex.what();

    return 1;
  }

  return 0;
}
