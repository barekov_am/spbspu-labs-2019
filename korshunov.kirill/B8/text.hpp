#ifndef B8_TEXT_HPP
#define B8_TEXT_HPP


#include <iostream>
#include <vector>

class Text
{
public:
  Text(const std::size_t);

  void readText(std::istream&);
  void printText(std::ostream&);

  ~Text() = default;
private:
  const std::size_t width_;
  std::vector<std::string> elements_;
};


#endif
