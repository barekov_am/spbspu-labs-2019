#include <iostream>
#include <fstream>
#include <memory>
#include <stdexcept>
#include <vector>

const size_t init_size = 512;

void lab2(const char * inp_file)
{
  std::ifstream input_file(inp_file);

  if (!input_file)
  {
    throw std::ios_base::failure("Failed opening the file.");
  }

  size_t size = init_size;

  using char_array = std::unique_ptr< char[], decltype(&free) >;
  char_array ch_arr(static_cast< char* >(malloc(size)), &free);

  if (!ch_arr)
  {
    throw std::runtime_error("Failed allocating memory.");
  }

  size_t count = 0;

  while (!input_file.eof())
  {

    input_file.read(&ch_arr[count], init_size);
    count += input_file.gcount();

    if (input_file.gcount() == init_size)
    {

      size += init_size;
      char_array tmp_array(static_cast< char * >(realloc(ch_arr.get(), size)), &free);

      if (!tmp_array)
      {
        throw std::runtime_error("Failed reallocating memory.");
      }

      ch_arr.release();
      std::swap(ch_arr, tmp_array);

    }

    if (!input_file.eof() && input_file.fail())
    {
      throw std::runtime_error("Failed reading the file.");
    }

  }

  std::vector< char >(&ch_arr[0], &ch_arr[count]);

  for(size_t i = 0; i < count; i++)
  {
    std::cout << ch_arr[i];
  }
}
