#include <cstring>

#include "functions.hpp"

Direction get_sort_direction(const char* direction)
{
  if (std::strcmp(direction, "ascending") == 0)
  {
    return Direction::ascending;
  }

  if (std::strcmp(direction, "descending") == 0)
  {
    return Direction::descending;
  }

  throw std::invalid_argument("Incorrect sorting direction.");
}
