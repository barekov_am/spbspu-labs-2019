#include <ctime>
#include <iostream>
#include <stdexcept>
#include <string>

void lab1(const char* direction);
void lab2(const char* inp_file);
void lab3();
void lab4(const char* direction, const int size);

int main(int argc, char* argv[])
{
  try
  {

    if ((argc < 2) || (argc > 4))
    {
      std::cerr << "Incorrect number of arguments.";

      return 1;
    }

    char *end_str = nullptr;
    int num = std::strtol(argv[1], &end_str, 10);

    switch (num) {

    case 1:
      {
        if (argc != 3)
        {
          std::cerr << "Incorrect number of arguments.";

          return 1;
        }

        lab1(argv[2]);
        break;
      }

    case 2:
      {
        if (argc != 3)
        {
          std::cerr << "Incorrect number of arguments.";

          return 1;
        }

        if (sizeof(argv[2]) == 0)
        {
          std::cerr << "File is not exist.";

          return 1;
        }

        lab2(argv[2]);
        break;
      }

    case 3:
      {
        if (argc != 2)
        {
          std::cerr << "Incorrect number of arguments.";

          return 1;
        }

        lab3();
        break;
      }

    case 4:
      {
        if (argc != 4)
        {
          std::cerr << "Incorrect number of arguments.";

          return 1;
        }

        srand(time(0));

        int size = std::strtol(argv[3], &end_str, 10);
        lab4(argv[2], size);
        break;
      }

    default:
      {
        std::cerr << "Invalid task number.";

        return 1;
      }
    }

    return 0;

  }

  catch (const std::exception &ex)
  {
    std::cerr << ex.what();

    return 1;
  }
}
