#include <cstdlib>
#include <vector>

#include "functions.hpp"

void fillRandom(double* array, int size)
{
  for (int i = 0; i < size; ++i)
  {
    array[i] = (rand() % 21 - 10) / 10.0;
  }
}

void lab4(const char* direction, const int size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size should be more than 0.");
  }

  Direction dir = get_sort_direction(direction);

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);
  print(vector);

  sort<OpStrategy>(vector, dir);
  print(vector);
}
