#include <forward_list>
#include <vector>

#include "functions.hpp"

void lab1(const char* direction)
{
  Direction dir = get_sort_direction(direction);

  std::vector<int> vectorOp;
  int num = 0;

  while (std::cin >> num)
  {
    vectorOp.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Failed input.");
  }

  if (vectorOp.empty())
  {
    return;
  }

  std::vector<int> vectorAt = vectorOp;
  std::forward_list<int> listIt(vectorOp.begin(), vectorOp.end());

  sort<OpStrategy>(vectorOp, dir);
  sort<AtStrategy>(vectorAt, dir);
  sort<ItStrategy>(listIt, dir);

  print(vectorOp);
  print(vectorAt);
  print(listIt);
}
