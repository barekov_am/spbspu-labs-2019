#ifndef B1_FUNCTIONS_HPP
#define B1_FUNCTIONS_HPP

#include <iostream>

#include "strategy.hpp"

enum class Direction
{
  ascending,
  descending
};

Direction get_sort_direction(const char* direction);

template <typename T>
void print(T& container)
{
  for (const auto& i: container)
  {
    std::cout << i << " ";
  }

  std::cout << '\n';
}

template <template <class T> class Strategy, typename T>
void sort(T& container, Direction direction)
{
  const auto begin = Strategy<T>::begin(container);
  const auto end = Strategy<T>::end(container);

  for (auto i = begin; i != end; ++i)
  {

    for (auto j = Strategy<T>::next(i); j != end; ++j)
    {
      auto tmp1 = i;
      auto tmp2 = j;

      if ((direction == Direction::ascending)
          and (Strategy<T>::get_element(container, tmp1) > Strategy<T>::get_element(container, tmp2)))
      {
        std::swap(Strategy<T>::get_element(container, tmp1), Strategy<T>::get_element(container, tmp2));
      }

      if ((direction == Direction::descending)
          and (Strategy<T>::get_element(container, tmp1) < Strategy<T>::get_element(container, tmp2)))
      {
        std::swap(Strategy<T>::get_element(container, tmp1), Strategy<T>::get_element(container, tmp2));
      }

    }

  }
}

#endif
