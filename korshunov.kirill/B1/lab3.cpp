#include <vector>

#include "functions.hpp"

void lab3()
{
  std::vector<int> vector;
  int num = 0;
  bool inp_end = false;

  while (std::cin >> num)
  {

    if (num == 0)
    {
      inp_end = true;
      break;
    }

    vector.push_back(num);

  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Failed input.");
  }

  if (vector.empty())
  {
    return;
  }

  if (!inp_end)
  {
    throw std::runtime_error("Zero is necessary.");
  }

  auto i = vector.begin();

  switch (vector.back())
  {
  case 1:
    {
      while (i != vector.end())
      {

        if ((*i % 2) == 0)
        {
          i = vector.erase(i);
        }
        else
        {
          ++i;
        }

      }
      break;
    }

  case 2:
    {
      while (i != vector.end())
      {

        if ((*i % 3) == 0)
        {
          i = (vector.insert(++i, 3, 1) + 3);
        }
        else
        {
          ++i;
        }

      }
      break;
    }
  }

  print(vector);
}
