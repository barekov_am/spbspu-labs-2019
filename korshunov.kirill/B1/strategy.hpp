#ifndef B1_STRATEGY_HPP
#define B1_STRATEGY_HPP

#include <iterator>

template <typename T>
struct OpStrategy
{
  static int begin(const T&)
  {
    return 0;
  }

  static int end(const T& container)
  {
    return container.size();
  }

  static int next(int i)
  {
    return i + 1;
  }

  static typename T::reference get_element(T& container, int i)
  {
    return container[i];
  }
};

template <typename T>
struct AtStrategy
{
  static int begin(const T&)
  {
    return 0;
  }

  static int end(const T& container)
  {
    return container.size();
  }

  static int next(int i)
  {
    return i + 1;
  }

  static typename T::reference get_element(T& container, int i)
  {
    return container.at(i);
  }
};

template <typename T>
struct ItStrategy
{
  typedef typename T::iterator it;

  static it begin(T& container)
  {
    return container.begin();
  }

  static it end(T& container)
  {
    return container.end();
  }

  static it next(it& iterator)
  {
    return std::next(iterator);
  }

  static typename T::reference get_element(T&, it& iterator)
  {
    return *iterator;
  }
};

#endif
