#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include <iostream>

int main()
{
  korshunov::Shape::pointer r1 = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 3, 1 }, 5, 5);

  korshunov::Shape::pointer r2 = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 0, 1 }, 5, 1);

  korshunov::Shape::pointer c1 = std::make_shared<korshunov::Circle>(korshunov::point_t { 2, -9 }, 5);

  korshunov::Shape::pointer t1 = std::make_shared<korshunov::Triangle>(korshunov::point_t { -4, 10 },
      korshunov::point_t { -8, 4 }, korshunov::point_t { -10, 8 });

  korshunov::CompositeShape cs(r1);
  cs.add(r2);
  cs.add(c1);
  cs.add(t1);

  cs.printInfo();

  cs.remove(3);

  cs.printInfo();

  cs.move(5, 5);
  cs.scale(3);
  cs.rotate(45);
  cs.printInfo();

  korshunov::Shape::pointer shapes[] = {r1, r2, c1, t1};

  for (const korshunov::Shape::pointer shape : shapes)
  {
    shape->printInfo();
  }

  korshunov::Matrix matrix = korshunov::part(cs);

  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    for (size_t j = 0; j < matrix.getLayerSize(i); j++)
    {
      std::cout << "[" << i <<";" << j << "]\n";
      matrix[i][j]->printInfo();
    }
  }

  return 0;
}
