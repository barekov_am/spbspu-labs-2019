#ifndef B3_BOOKMARKMANAGER_HPP
#define B3_BOOKMARKMANAGER_HPP

#include <map>

#include "phonebook.hpp"

class BookmarkManager
{

public:
  enum class InsertLocation
  {
    before,
    after
  };

  enum class MoveLocation
  {
    first,
    last
  };

  BookmarkManager();

  void add(const Phonebook::record_t&);
  void store(const std::string&, const std::string&);
  void insert(InsertLocation, const std::string&, const Phonebook::record_t&);
  void erase(const std::string&);
  void show(const std::string&);
  void move(const std::string&, int);
  void move(const std::string&, MoveLocation);

private:
  using bookmarks = std::map<std::string, Phonebook::iterator>;

  Phonebook records_;
  bookmarks bookmarks_;

  bookmarks::iterator getBookmarkIterator(const std::string&);

};


#endif
