#ifndef B4_STRUCTCOMP_HPP
#define B4_STRUCTCOMP_HPP

#include "dataStruct.hpp"

class structComp
{
public:
  bool operator()(const DataStruct& data1, const DataStruct& data2) const;
};

#endif
