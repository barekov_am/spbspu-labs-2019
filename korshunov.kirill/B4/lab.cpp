#include <iostream>
#include <vector>
#include <algorithm>

#include "dataStruct.hpp"
#include "structComp.hpp"

void lab()
{
  std::vector<DataStruct> vector;

  while (std::cin.peek() != EOF)
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input failed");
    }

    vector.push_back(inputData());
  }

  std::sort(vector.begin(), vector.end(), structComp());

  for (auto i = vector.begin(); i != vector.end(); i++)
  {
    std::cout << i->key1 << "," << i->key2 << "," << i->str << "\n";
  }

}
