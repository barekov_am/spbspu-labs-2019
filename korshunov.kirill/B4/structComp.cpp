#include <iostream>

#include "structComp.hpp"

bool structComp::operator()(const DataStruct& data1, const DataStruct& data2) const
{
  if (data1.key1 < data2.key1)
  {
    return true;
  }
  if (data1.key1 == data2.key1)
  {
    if (data1.key2 < data2.key2)
    {
      return true;
    }
    if (data1.key2 == data2.key2)
    {
      if (data1.str.size() < data2.str.size())
      {
        return true;
      }
    }
  }

  return false;
}
