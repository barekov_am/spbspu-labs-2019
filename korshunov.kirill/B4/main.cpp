#include <iostream>

void lab();

int main()
{
  try
  {
    lab();
  }
  catch (const std::exception &ex)
  {
    std::cerr << ex.what();

    return 1;
  }

  return 0;
}
