#include <iostream>

#include "dataStruct.hpp"

DataStruct inputData()
{
  std::string line;
  std::getline(std::cin, line);

  int key[2] = {0};
  for (int i = 0; i < 2; i++)
  {
    size_t ind = line.find_first_of(',');

    if (ind == std::string::npos)
    {
      throw std::invalid_argument("Invalid data");
    }

    key[i] = std::stoi(line.substr(0, ind));

    if ((key[i] > 5) || (key[i] < -5))
    {
      throw std::invalid_argument("Invalid key");
    }

    line = line.erase(0, ind + 1);
  }

  while (line.find_first_of(' ') == 0)
  {
    line.erase(0, 1);
  }

  if (line.empty())
  {
    throw std::invalid_argument("Invalid data");
  }

  return { key[0], key[1], line };
}
