#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <cmath>

#include "statistics.hpp"

void lab()
{
 std::vector<int> vector;
 std::string num;

 while (std::cin >> num)
 {
   double tmp = std::stod(num);

   if (ceil(std::abs(tmp)) - trunc(std::abs(tmp)) > 0.5)
   {
     throw std::invalid_argument("Invalid input");
   }

   vector.push_back(std::stoi(num));
 }

 Statistics stat = for_each(vector.begin(), vector.end(), Statistics());

 stat.printStatistics();
}
