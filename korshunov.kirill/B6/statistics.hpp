#ifndef B6_STATISTICS_HPP
#define B6_STATISTICS_HPP


class Statistics
{
public:
  Statistics();

  void operator()(const int&);
  void printStatistics();
private:
  int max_;
  int min_;
  double mean_;
  int posCount_;
  int negCount_;
  long long int sumEven_;
  long long int sumOdd_;
  bool equalFirstLast_;

  bool empty_;
  int first_;
  int genCount_;
};


#endif
