#include <iostream>

#include "statistics.hpp"

Statistics::Statistics():
  max_(0),
  min_(0),
  mean_(0),
  posCount_(0),
  negCount_(0),
  sumEven_(0),
  sumOdd_(0),
  equalFirstLast_(false),
  empty_(true),
  first_(0),
  genCount_(0)
{
}

void Statistics::operator()(const int& num)
{
  if (empty_)
  {
    max_ = num;
    min_ = num;
    first_ = num;
    empty_ = false;
  }

  if (num > max_)
  {
    max_ = num;
  }

  if (num < min_)
  {
    min_ = num;
  }

  if (num > 0)
  {
    posCount_++;
  }
  else if (num < 0)
  {
    negCount_++;
  }

  genCount_++;

  if (num % 2 == 0)
  {
    sumEven_ = sumEven_ + num;
  }
  else
  {
    sumOdd_ = sumOdd_ + num;
  }

  mean_ = (sumEven_ + sumOdd_)/genCount_;

 equalFirstLast_ = (first_ == num);
}

void Statistics::printStatistics()
{
  if (empty_)
  {
    std::cout << "No Data" << std::endl;

    return;
  }

  std::cout << "Max: " << max_ << std::endl;
  std::cout << "Min: " << min_ << std::endl;
  std::cout << "Mean: " << mean_ << std::endl;
  std::cout << "Positive: " << posCount_ << std::endl;
  std::cout << "Negative: " << negCount_ << std::endl;
  std::cout << "Odd Sum: " << sumOdd_ << std::endl;
  std::cout << "Even Sum: " << sumEven_ << std::endl;
  std::cout << "First/Last Equal: " << (equalFirstLast_ ? "yes" : "no") << std::endl;
}
