#include <string>
#include <vector>
#include <algorithm>
#include <iostream>

void lab1()
{
  std::vector<std::string> vector;
  std::string line;
  std::string sep = " \t\n";

  while (std::getline(std::cin, line))
  {
    while (line.find_first_of(sep) == 0)
    {
      line.erase(0, 1);
    }

    while (!line.empty())
    {
      size_t pos = line.find_first_of(sep);
      vector.push_back(line.substr(0, pos));

      if (pos == std::string::npos)
      {
        line.clear();
        break;
      }

      line.erase(0, pos + 1);

      while (line.find_first_of(sep) == 0)
      {
        line.erase(0, 1);
      }
    }
  }

  std::sort(vector.begin(), vector.end());
  vector.erase(std::unique(vector.begin(), vector.end()), vector.end());

  for (auto i = vector.begin(); i != vector.end(); i++)
  {
    std::cout << *i << std::endl;
  }
}
