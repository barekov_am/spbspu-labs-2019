#ifndef B5_FUNCTORANALYSIS_HPP
#define B5_FUNCTORANALYSIS_HPP

#include "shapeContainer.hpp"

struct functorAnalysis
{
public:
  void operator ()(const Shape&);

  int vertices_ = 0;
  int triangles_ = 0;
  int squares_ = 0;
  int rectangles_ = 0;
};

#endif
