#ifndef B5_SHAPECONTAINER_HPP
#define B5_SHAPECONTAINER_HPP

#include <vector>

struct Point
{
  int x,y;
};

using Shape = std::vector<Point>;
using ShapeVector = std::vector<Shape>;

class ShapeContainer
{
public:
  ShapeContainer();

  void readShapeVec();
  void analysisShapeVec();
  void deletePent();
  void createPointVec();
  void sortShapeVec();
  void print();
private:
  int vertices_;
  int triangles_;
  int squares_;
  int rectangles_;
  Shape points_;
  ShapeVector shapes_;
};

#endif
