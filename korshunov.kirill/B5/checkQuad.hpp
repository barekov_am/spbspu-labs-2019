#ifndef B5_CHECKQUAD_HPP
#define B5_CHECKQUAD_HPP

#include "shapeContainer.hpp"

int getLen(const Point&, const Point&);
bool isRectangle(const Shape&);
bool isSquare(const Shape&);

#endif
