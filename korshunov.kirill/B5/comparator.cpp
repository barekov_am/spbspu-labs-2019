#include "comparator.hpp"
#include "checkQuad.hpp"

bool Comparator::operator ()(const Shape &shape1, const Shape &shape2)
{
  if (shape1.size() < shape2.size())
  {
    return true;
  }
  if ((shape1.size() == 4) && (shape2.size() == 4))
  {
    if (isSquare(shape1))
    {
      if (isSquare(shape2))
      {
        return false;
      }

      return true;
    }
  }

  return false;
}
