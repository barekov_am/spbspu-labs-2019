#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

#include "shapeContainer.hpp"
#include "comparator.hpp"
#include "checkQuad.hpp"
#include "functorAnalysis.hpp"

ShapeContainer::ShapeContainer():
  vertices_(0),
  triangles_(0),
  squares_(0),
  rectangles_(0)
{
}

void ShapeContainer::readShapeVec()
{
  std::string line;
  std::string separator = " \t";

  Shape shape;

  while (std::getline(std::cin, line))
  {
    while (line.find_first_of(separator) == 0)
    {
      line.erase(0, 1);
    }

    size_t pos = line.find_first_of(separator);

    if (pos == std::string::npos)
    {
      continue;
    }

    int vertices = std::stoi(line.substr(0, pos));

    if (vertices < 1)
    {
      throw std::invalid_argument("Invalid number of vertices");
    }

    line.erase(0, pos + 1);

    for (int i = 0; i != vertices; i++)
    {
      while (line.find_first_of(separator) == 0)
      {
        line.erase(0, 1);
      }

      size_t openBr = line.find_first_of("(");
      size_t semicolon = line.find_first_of(";");
      size_t closeBr = line.find_first_of(")");

      if ((openBr == std::string::npos) || (semicolon == std::string::npos) || (closeBr == std::string::npos) ||
          (openBr != 0) || !(openBr < semicolon) || !(semicolon < closeBr))
      {
        throw std::invalid_argument("Invalid number of vertices");
      }

      int x = std::stoi(line.substr(1, semicolon - 1));
      int y = std::stoi(line.substr(semicolon + 1, closeBr - (semicolon + 1)));
      shape.push_back({ x, y });
      line.erase(0, closeBr + 1);
    }

    while (line.find_first_of(separator) == 0)
    {
      line.erase(0, 1);
    }

    if (!line.empty())
    {
      throw std::invalid_argument("Invalid input");
    }

    shapes_.push_back(shape);
    shape.clear();
  }
}

void ShapeContainer::analysisShapeVec()
{
  functorAnalysis analysis = std::for_each(shapes_.begin(), shapes_.end(), functorAnalysis());

  triangles_ = analysis.triangles_;
  rectangles_ = analysis.rectangles_;
  squares_ = analysis.squares_;
  vertices_ = analysis.vertices_;
}

void ShapeContainer::deletePent()
{
  auto tmpRem = std::remove_if(shapes_.begin(), shapes_.end(), [](const Shape& shape)
  {
    return (shape.size() == 5);
  });

  shapes_.erase(tmpRem, shapes_.end());
}

void ShapeContainer::createPointVec()
{
  for (auto i = shapes_.begin(); i != shapes_.end(); i++)
  {
    auto tmpShape = *i;
    points_.push_back(tmpShape[0]);
  }
}

void ShapeContainer::sortShapeVec()
{
  std::sort(shapes_.begin(), shapes_.end(), Comparator());
}

void ShapeContainer::print()
{
  std::cout << "Vertices: " << vertices_ << std::endl;
  std::cout << "Triangles: " << triangles_ << std::endl;
  std::cout << "Squares: " << squares_ << std::endl;
  std::cout << "Rectangles: " << rectangles_ << std::endl;
  std::cout << "Points:";

  for (auto i = points_.begin(); i != points_.end(); i++)
  {
    std::cout << " (" << i->x << ";" << i->y << ")";
  }

  std::cout << "\nShapes:\n";

  for (auto i = shapes_.begin(); i != shapes_.end(); i++)
  {
    std::cout << i->size();

    for (auto j = i->begin(); j != i->end(); j++)
    {
      std::cout << " (" << j->x << ";" << j->y << ")";
    }

    std::cout << std::endl;
  }
}
