#include "functorAnalysis.hpp"
#include "checkQuad.hpp"

void functorAnalysis::operator ()(const Shape& shape)
{
  vertices_ = vertices_ + shape.size();

  if (shape.size() == 3)
  {
    triangles_++;
  }

  if (shape.size() == 4)
  {
    if (isRectangle(shape))
    {
      rectangles_++;

      if (isSquare(shape))
      {
        squares_++;
      }
    }
  }
}
