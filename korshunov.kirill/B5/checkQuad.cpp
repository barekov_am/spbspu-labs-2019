#include "checkQuad.hpp"

int getLen(const Point& point1, const Point& point2)
{
  return (point1.x - point2.x) * (point1.x - point2.x) + (point1.y - point2.y) * (point1.y - point2.y);
}

bool isRectangle(const Shape& shape)
{
  const int diag1 = getLen(shape[0], shape[2]);
  const int diag2 = getLen(shape[1], shape[3]);

  if (diag1 == diag2)
  {
    return true;
  }

  return false;
}

bool isSquare(const Shape& shape)
{
  if (isRectangle(shape))
  {
    int side1 = getLen(shape[0], shape[1]);
    int side2 = getLen(shape[1], shape[2]);

    if (side1 == side2)
    {
      return true;
    }
  }

  return false;
}
