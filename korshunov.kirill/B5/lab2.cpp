#include "shapeContainer.hpp"

void lab2()
{
  ShapeContainer shapes;

  shapes.readShapeVec();
  shapes.analysisShapeVec();
  shapes.deletePent();
  shapes.createPointVec();
  shapes.sortShapeVec();
  shapes.print();
}
