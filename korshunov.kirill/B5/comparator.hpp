#ifndef B5_COMPARATOR_HPP
#define B5_COMPARATOR_HPP

#include "shapeContainer.hpp"

class Comparator
{
public:
  bool operator ()(const Shape&, const Shape&);
};


#endif
