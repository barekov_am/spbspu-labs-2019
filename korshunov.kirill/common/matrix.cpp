#include "matrix.hpp"

korshunov::Matrix::Matrix():
  rows_(0),
  columns_(0)
{
}

korshunov::Matrix::Matrix(const Matrix &source):
  data_(std::make_unique<Shape::pointer[]>(source.rows_ * source.columns_)),
  rows_(source.rows_),
  columns_(source.columns_)
{
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    data_[i] = source.data_[i];
  }
}

korshunov::Matrix::Matrix(Matrix &&source):
  data_(std::move(source.data_)),
  rows_(source.rows_),
  columns_(source.columns_)
{
  source.rows_ = 0;
  source.columns_ = 0;
}

korshunov::Matrix &korshunov::Matrix::operator =(const Matrix &source)
{
  if (&source != this)
  {
    Shape::array tmpData = std::make_unique<Shape::pointer[]>(source.rows_ * source.columns_);

    for (std::size_t i = 0; i < (source.rows_ * source.columns_); i++)
    {
      tmpData[i] = source.data_[i];
    }

    data_.swap(tmpData);
    rows_ = source.rows_;
    columns_ = source.columns_;
  }

  return *this;
}

korshunov::Matrix & korshunov::Matrix::operator =(Matrix &&source)
{
  if (&source != this)
  {
    rows_ = source.rows_;
    columns_ = source.columns_;
    swap(data_, source.data_);
    source.rows_ = 0;
    source.columns_ = 0;
    source.data_ = nullptr;
  }

  return *this;
}

korshunov::Shape::array korshunov::Matrix::operator [](size_t layer) const
{
  if (layer >= rows_)
  {
    throw std::out_of_range("Index is out of range!");
  }

  Shape::array tmpData = std::make_unique<Shape::pointer[]>(getLayerSize(layer));

  for (std::size_t i = 0; i < getLayerSize(layer); i++)
  {
    tmpData[i] = data_[layer * columns_ + i];
  }

  return tmpData;
}

bool korshunov::Matrix::operator ==(const Matrix &source) const
{
  if ((source.rows_ != rows_) || (source.columns_ != columns_))
  {
    return false;
  }

  for (std::size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (source.data_[i] != data_[i])
    {
      return false;
    }
  }

  return true;
}

bool korshunov::Matrix::operator !=(const Matrix &source) const
{
  return !(*this == source);
}

std::size_t korshunov::Matrix::getRows() const
{
  return rows_;
}

std::size_t korshunov::Matrix::getColumns() const
{
  return columns_;
}

std::size_t korshunov::Matrix::getLayerSize(size_t layer) const
{
  if (layer >= rows_)
  {
    return 0;
  }

  for (size_t i = 0; i < columns_; i++)
  {
    if (data_[layer * columns_ + i] == nullptr)
    {
      return i;
    }
  }

  return columns_;
}

void korshunov::Matrix::add(const Shape::pointer &shape, size_t layer)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Pointer can't be null!");
  }
  
  size_t tmpRows = (layer >= rows_) ? (rows_ + 1) : rows_;
  size_t tmpColumns = (getLayerSize(layer) == columns_) ? (columns_ + 1) : columns_;
  const size_t column = (layer < rows_) ? getLayerSize(layer) : 0;

  Shape::array tmpData = std::make_unique<Shape::pointer[]>(tmpRows * tmpColumns);

  for (size_t i = 0; i < tmpRows; i++)
  {
    for (size_t j = 0; j < tmpColumns; j++)
    {
      if ((i == rows_) || (j == columns_))
      {
        tmpData[i * tmpColumns + j] = nullptr;
      }
      else
      {
        tmpData[i * tmpColumns + j] = data_[i * columns_ + j];
      }
    }
  }

  if (layer < rows_)
  {
    tmpData[layer * tmpColumns + column] = shape;
  }
  else
  {
    tmpData[rows_ * tmpColumns + column] = shape;
  }

  data_.swap(tmpData);
  rows_ = tmpRows;
  columns_ = tmpColumns;
}
