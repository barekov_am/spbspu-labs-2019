#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"
#include <memory>
#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

BOOST_AUTO_TEST_SUITE(test_suite_composite)

  const double Inaccuracy = 0.01;

  BOOST_AUTO_TEST_CASE(test_matrix_copy_constructor)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 3, 1 }, 4);
    korshunov::Shape::pointer rectanglePtr = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 6, 2 }, 10, 4);

    korshunov::CompositeShape composition(circlePtr);
    composition.add(rectanglePtr);

    korshunov::Matrix testMatrix = korshunov::part(composition);
    korshunov::Matrix testMatrixCopy(testMatrix);

    BOOST_CHECK(testMatrix == testMatrixCopy);
  }

  BOOST_AUTO_TEST_CASE(test_matrix_copy_operator)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 3, 1 }, 4);
    korshunov::Shape::pointer rectanglePtr = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 6, 2 }, 10, 4);

    korshunov::CompositeShape composition(circlePtr);
    composition.add(rectanglePtr);

    korshunov::Matrix testMatrix = korshunov::part(composition);
    korshunov::Matrix testMatrixCopy;
    testMatrixCopy = testMatrix;

    BOOST_CHECK(testMatrix == testMatrixCopy);
  }

  BOOST_AUTO_TEST_CASE(test_matrix_move_constructor)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 3, 1 }, 4);
    korshunov::Shape::pointer rectanglePtr = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 6, 2 }, 10, 4);

    korshunov::CompositeShape composition(circlePtr);
    composition.add(rectanglePtr);

    korshunov::Matrix testMatrix = korshunov::part(composition);
    korshunov::Matrix testMatrixCopy(testMatrix);
    korshunov::Matrix testMatrixMove(std::move(testMatrix));

    BOOST_CHECK(testMatrixCopy == testMatrixMove);
  }

  BOOST_AUTO_TEST_CASE(test_matrix_move_operator)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 3, 1 }, 4);
    korshunov::Shape::pointer rectanglePtr = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 6, 2 }, 10, 4);

    korshunov::CompositeShape composition(circlePtr);
    composition.add(rectanglePtr);

    korshunov::Matrix testMatrix = korshunov::part(composition);
    korshunov::Matrix testMatrixCopy(testMatrix);
    korshunov::Matrix testMatrixMove;

    testMatrixMove = std::move(testMatrix);

    BOOST_CHECK(testMatrixMove == testMatrixCopy);
  }

  BOOST_AUTO_TEST_CASE(matrix_throwing_exceptions)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 7, 7 }, 7);
    korshunov::Shape::pointer rectanglePtr = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 8, 1 }, 5, 1);
    korshunov::CompositeShape composition(circlePtr);
    composition.add(rectanglePtr);

    korshunov::Matrix testMatrix = korshunov::part(composition);

    BOOST_CHECK_THROW(testMatrix[77], std::out_of_range);
  }

BOOST_AUTO_TEST_SUITE_END()
