#ifndef A4_COMPOSITE_SHAPE_HPP
#define A4_COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include <iostream>

namespace korshunov
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &source);
    CompositeShape(CompositeShape &&source);
    CompositeShape(const Shape::pointer &shape);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &source);
    CompositeShape &operator =(CompositeShape &&source);
    Shape::pointer operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &center) override;
    void move(double dx, double dy) override;
    void scale(double) override;
    void rotate(double) override;
    void printInfo() const override;

    size_t getSize() const;
    size_t getIndex(const Shape::pointer &shape) const;
    void add(const Shape::pointer &shape);
    void remove(size_t index);

  private:
    size_t size_;
    Shape::array shape_array_;
  };
}

#endif
