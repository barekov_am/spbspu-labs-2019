#include "circle.hpp"
#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

BOOST_AUTO_TEST_SUITE(test_suite_circle)

  const double Inaccuracy = 0.01;

  BOOST_AUTO_TEST_CASE(circle_rotate)
  {
    korshunov::Circle testCircle({ 7, 7 }, 7);
    const korshunov::rectangle_t tmpFrameBefore = testCircle.getFrameRect();
    const double tmpAreaBefore = testCircle.getArea();

    testCircle.rotate(90);
    korshunov::rectangle_t tmpFrameAfter = testCircle.getFrameRect();
    double tmpAreaAfter = testCircle.getArea();

    BOOST_CHECK_CLOSE(tmpFrameBefore.width, tmpFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.height, tmpFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.pos.x, tmpFrameAfter.pos.x, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.pos.y, tmpFrameAfter.pos.y, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, Inaccuracy);

    testCircle.rotate(90);
    tmpFrameAfter = testCircle.getFrameRect();
    tmpAreaAfter = testCircle.getArea();

    BOOST_CHECK_CLOSE(tmpFrameBefore.width, tmpFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.height, tmpFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.pos.x, tmpFrameAfter.pos.x, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.pos.y, tmpFrameAfter.pos.y, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, Inaccuracy);

    testCircle.rotate(-193);
    tmpFrameAfter = testCircle.getFrameRect();
    tmpAreaAfter = testCircle.getArea();

    BOOST_CHECK_CLOSE(tmpFrameBefore.width, tmpFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.height, tmpFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.pos.x, tmpFrameAfter.pos.x, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.pos.y, tmpFrameAfter.pos.y, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(circle_invar_after_absolute_move)
  {
    korshunov::Circle testCircle({ 7, 7 }, 7);
    const korshunov::rectangle_t testFrameBefore = testCircle.getFrameRect();
    const double testAreaBefore = testCircle.getArea();

    testCircle.move({ 23, 49 });
    korshunov::rectangle_t testFrameAfter = testCircle.getFrameRect();
    double testAreaAfter = testCircle.getArea();
    BOOST_CHECK_CLOSE(testFrameBefore.width, testFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(testFrameBefore.height, testFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(testAreaBefore, testAreaAfter, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(circle_invar_after_relative_move)
  {
    korshunov::Circle testCircle({ 7, 7 }, 7);
    const korshunov::rectangle_t testFrameBefore = testCircle.getFrameRect();
    const double testAreaBefore = testCircle.getArea();

    testCircle.move(23, 17);
    korshunov::rectangle_t testFrameAfter = testCircle.getFrameRect();
    double testAreaAfter = testCircle.getArea();
    BOOST_CHECK_CLOSE(testFrameBefore.width, testFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(testFrameBefore.height, testFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(testAreaBefore, testAreaAfter, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(quadr_change_area_circle)
  {
    korshunov::Circle testCircle({ 7, 7 }, 7);
    const double testAreaBefore = testCircle.getArea();
    const double coefScale = 7;

    testCircle.scale(coefScale);
    BOOST_CHECK_CLOSE(testCircle.getArea(), coefScale * coefScale * testAreaBefore, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(throwing_exceptions)
  {
    BOOST_CHECK_THROW(korshunov::Circle({ 3, 6 }, -4), std::invalid_argument);

    korshunov::Circle testCircle({ 5, 8 }, 2);
    BOOST_CHECK_THROW(testCircle.scale(-7), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
