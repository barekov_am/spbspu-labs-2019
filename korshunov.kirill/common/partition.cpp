#include "partition.hpp"

korshunov::Matrix korshunov::part(const korshunov::CompositeShape &composition)
{
  Matrix tmpMatrix;
  const size_t size = composition.getSize();

  for (size_t i = 0; i < size; i++)
  {
    size_t layer = 0;
    for (size_t j = 0; j < tmpMatrix.getRows(); j++)
    {
      bool intersect = false;
      for (size_t k = 0; k < tmpMatrix.getLayerSize(j); k++)
      {
        if (isIntersected(composition[i]->getFrameRect(), tmpMatrix[j][k]->getFrameRect()))
        {
          layer++;
          intersect = true;
          break;
        }
      }
      if (!intersect)
      {
        break;
      }
    }

    tmpMatrix.add(composition[i], layer);
  }

  return tmpMatrix;
}
