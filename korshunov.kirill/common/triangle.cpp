#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

korshunov::Triangle::Triangle(const point_t &pointA, const point_t &pointB, const point_t &pointC):
  pointA_(pointA),
  pointB_(pointB),
  pointC_(pointC),
  center_({ (pointA.x + pointB.x + pointC.x) / 3, (pointA.y + pointB.y + pointC.y) / 3 })
{
  if (getArea() <= 0)
  {
    throw std::invalid_argument("Invalid coordinates.");
  }
}

double korshunov::Triangle::getArea() const
{
  return std::fabs(((pointA_.x - pointC_.x) * (pointB_.y - pointC_.y) - (pointB_.x - pointC_.x)
      * (pointA_.y - pointC_.y)) / 2);
}

korshunov::rectangle_t korshunov::Triangle::getFrameRect() const
{
  const double minX = std::min(std::min(pointA_.x, pointB_.x), pointC_.x);
  const double maxX = std::max(std::max(pointA_.x, pointB_.x), pointC_.x);
  const double minY = std::min(std::min(pointA_.y, pointB_.y), pointC_.y);
  const double maxY = std::max(std::max(pointA_.y, pointB_.y), pointC_.y);
  return rectangle_t{ point_t{ (maxX + minX) / 2, (maxY + minY) / 2 } , maxX - minX, maxY - minY };
}

void korshunov::Triangle::move(const point_t &center)
{
  const double dx = center.x - center_.x;
  const double dy = center.y - center_.y;
  move(dx, dy);
}

void korshunov::Triangle::move(double dx, double dy)
{
  pointA_.x += dx;
  pointB_.x += dx;
  pointC_.x += dx;
  center_.x += dx;
  pointA_.y += dy;
  pointB_.y += dy;
  pointC_.y += dy;
  center_.y += dy;
}

void korshunov::Triangle::scale(double scale)
{
  if (scale <= 0)
  {
    throw std::invalid_argument("Invalid scale");
  }
  pointA_.x = (pointA_.x - center_.x) * scale + center_.x;
  pointB_.x = (pointB_.x - center_.x) * scale + center_.x;
  pointC_.x = (pointC_.x - center_.x) * scale + center_.x;
  pointA_.y = (pointA_.y - center_.y) * scale + center_.y;
  pointB_.y = (pointB_.y - center_.y) * scale + center_.y;
  pointC_.y = (pointC_.y - center_.y) * scale + center_.y;
}

void korshunov::Triangle::rotate(double angle)
{
  const double sine = sin(angle * M_PI / 180);
  const double cosine = cos(angle * M_PI / 180);
  const double Ax = pointA_.x - center_.x;
  const double Ay = pointA_.y - center_.y;
  const double Bx = pointB_.x - center_.x;
  const double By = pointB_.y - center_.y;
  const double Cx = pointC_.x - center_.x;
  const double Cy = pointC_.y - center_.y;
  pointA_.x = Ax * cosine - Ay * sine + center_.x;
  pointA_.y = Ax * sine + Ay * cosine + center_.y;
  pointB_.x = Bx * cosine - By * sine + center_.x;
  pointB_.y = Bx * sine + By * cosine + center_.y;
  pointC_.x = Cx * cosine - Cy * sine + center_.x;
  pointC_.y = Cx * sine + Cy * cosine + center_.y;
}

void korshunov::Triangle::printInfo() const
{
  std::cout << "Triangle" << "\nXY: (" << center_.x << ";" << center_.y << ")\nA ("
            << pointA_.x << ";" << pointA_.y << ")\nB (" << pointB_.x << ";" << pointB_.y << ")\nC ("
            << pointC_.x << ";" << pointC_.y << ")\nS = " << getArea() << "\n\n";
}
