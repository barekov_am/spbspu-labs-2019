#ifndef A4_SHAPE_HPP_
#define A4_SHAPE_HPP_

#include "base-types.hpp"
#include <memory>
namespace korshunov
{
  class Shape
  {
  public:
    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t &center) = 0;
    virtual void move (double dx, double dy) = 0;
    virtual void scale(double) = 0;
    virtual void rotate (double) = 0;
    virtual void printInfo() const = 0;

    using pointer = std::shared_ptr<Shape>;
    using array = std::unique_ptr<pointer[]>;
  };

}

#endif
