#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

korshunov::Rectangle::Rectangle(const point_t &center, double width, double height):
  rectangle_({center, width, height}),
  angle_(0.0)
{
  if ((width < 0.0) || (height < 0.0))
  {
    throw std::invalid_argument("Width and height should be more 0");
  }
}

double korshunov::Rectangle::getArea() const
{
  return rectangle_.width * rectangle_.height;
}

korshunov::rectangle_t korshunov::Rectangle::getFrameRect() const
{
  const double sine = sin(angle_ * M_PI / 180);
  const double cosine = cos(angle_ * M_PI / 180);
  const double width = rectangle_.height * fabs(sine) + rectangle_.width * fabs(cosine);
  const double height = rectangle_.height * fabs(cosine) + rectangle_.width * fabs(sine);

  return rectangle_t { rectangle_.pos, width, height };
}

void korshunov::Rectangle::move(const point_t &center)
{
  rectangle_.pos = center;
}

void korshunov::Rectangle::move(double dx, double dy)
{
  rectangle_.pos.x += dx;
  rectangle_.pos.y += dy;
}

void korshunov::Rectangle::scale(double scale)
{
  if (scale <= 0)
  {
    throw std::invalid_argument("Invalid scale");
  }
  rectangle_.width *= scale;
  rectangle_.height *= scale;
}

void korshunov::Rectangle::rotate(double angle)
{
  angle_ += angle;
  const double fullCircle = 360;
  angle_ = (angle_ < 0.0) ? (fullCircle + fmod(angle_, fullCircle)) : fmod(angle_, fullCircle);
}

void korshunov::Rectangle::printInfo() const
{
  std::cout << "Rectangle\nXY: (" << rectangle_.pos.x << ";" << rectangle_.pos.y << ")\nWidth = "
      << rectangle_.width << "\nHeight = " << rectangle_.height << "\nS = " << getArea() << "\n\n";
}
