#include "composite-shape.hpp"
#include <cmath>
#include <iostream>

korshunov::CompositeShape::CompositeShape():
  size_(0),
  shape_array_(nullptr)
{
}

korshunov::CompositeShape::CompositeShape(const CompositeShape &source):
  size_(source.size_),
  shape_array_(std::make_unique<Shape::pointer[]>(source.size_))
{
  for (size_t i = 0; i < size_; i++)
  {
    shape_array_[i] = source.shape_array_[i];
  }
}

korshunov::CompositeShape::CompositeShape(CompositeShape &&source):
  size_(source.size_),
  shape_array_(std::move(source.shape_array_))
{
  source.size_ = 0;
}

korshunov::CompositeShape::CompositeShape(const Shape::pointer &shape):
  size_(1),
  shape_array_(std::make_unique<Shape::pointer[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer can't be null");
  }

  shape_array_[0] = shape;
}

korshunov::CompositeShape &korshunov::CompositeShape::operator =(const CompositeShape &source)
{
  if (&source != this)
  {
    size_ = source.size_;
    shape_array_ = std::make_unique<Shape::pointer[]>(source.size_);

    for (size_t i = 0; i < size_; i++)
    {
      shape_array_[i] = source.shape_array_[i];
    }
  }

  return *this;
}

korshunov::CompositeShape &korshunov::CompositeShape::operator =(CompositeShape &&source)
{
  if (&source != this)
  {
    size_ = source.size_;
    shape_array_ = std::move(source.shape_array_);
    source.shape_array_ = nullptr;
    source.size_ = 0;
  }

  return *this;
}

korshunov::Shape::pointer korshunov::CompositeShape::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::logic_error("Invalid index");
  }

  return shape_array_[index];
}

double korshunov::CompositeShape::getArea() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  double area = 0;
  for (size_t i = 0; i < size_; i++)
  {
    area += shape_array_[i]->getArea();
  }
  return area;
}

korshunov::rectangle_t korshunov::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  rectangle_t  tempRectangle = shape_array_[0]->getFrameRect();

  double minX = tempRectangle.pos.x - tempRectangle.width / 2;
  double maxX = tempRectangle.pos.x + tempRectangle.width / 2;
  double minY = tempRectangle.pos.y - tempRectangle.height / 2;
  double maxY = tempRectangle.pos.y + tempRectangle.height / 2;

  for (size_t i = 0; i < size_; i++)
  {
    tempRectangle = shape_array_[i]->getFrameRect();

    double temp = tempRectangle.pos.x - tempRectangle.width / 2;
    minX = std::min(minX,temp);
    temp = tempRectangle.pos.x + tempRectangle.width / 2;
    maxX = std::max(maxX,temp);
    temp = tempRectangle.pos.y - tempRectangle.height / 2;
    minY = std::min(minY,temp);
    temp = tempRectangle.pos.y + tempRectangle.height / 2;
    maxY = std::max(maxY,temp);
  }

  return rectangle_t{ point_t{ (maxX + minX) / 2, (maxY + minY) / 2 } , maxX - minX, maxY - minY };
}

void korshunov::CompositeShape::move(double dx, double dy)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  for (size_t i = 0; i < size_; i++)
  {
    shape_array_[i]->move(dx,dy);
  }
}

void korshunov::CompositeShape::move(const korshunov::point_t &center)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  const rectangle_t rect = getFrameRect();
  const double dx = center.x - rect.pos.x;
  const double dy = center.y - rect.pos.y;

  move(dx, dy);
}

void korshunov::CompositeShape::scale(double scale)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  if (scale <= 0)
  {
    throw std::invalid_argument("Invalid scale");
  }

  const rectangle_t rect = getFrameRect();

  for (size_t i = 0; i < size_; i++)
  {
    const double dx = (shape_array_[i]->getFrameRect().pos.x - rect.pos.x) * (scale - 1);
    const double dy = (shape_array_[i]->getFrameRect().pos.y - rect.pos.y) * (scale - 1);

    shape_array_[i]->move(dx,dy);
    shape_array_[i]->scale(scale);
  }
}

void korshunov::CompositeShape::rotate(double angle)
{
  if (size_ == 0) {
    throw std::logic_error("Composite shape is empty");
  }

  const double sine = sin(angle * M_PI / 180);
  const double cosine = cos(angle * M_PI / 180);
  const point_t compCenter = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++) {
    shape_array_[i]->rotate(angle);
    const point_t shapeCenter = shape_array_[i]->getFrameRect().pos;

    const double oldX = shapeCenter.x - compCenter.x;
    const double oldY = shapeCenter.y - compCenter.y;

    const double newX = oldX * cosine - oldY * sine;
    const double newY = oldX * sine + oldY * cosine;

    shape_array_[i]->move({newX + compCenter.x, newY + compCenter.y});
  }
}

void korshunov::CompositeShape::printInfo() const
{
  const rectangle_t FrameRect = getFrameRect();
  std::cout << "Composite Shape\nXY: (" << FrameRect.pos.x << ";" <<
      FrameRect.pos.y << ")\nSize = " << size_ << "\nS = " <<
      getArea() << "\n\n";
}

size_t korshunov::CompositeShape::getSize() const
{
  return size_;
}

size_t korshunov::CompositeShape::getIndex(const Shape::pointer &shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer can't be null");
  }

  for (size_t i = 0; i < size_; i++)
  {
    if (shape == shape_array_[i])
    {
      return i;
    }
  }
  return size_;
}

void korshunov::CompositeShape::add(const Shape::pointer &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer can't be null");
  }

  for (size_t i = 0; i < size_; i++)
  {
    if (shape == shape_array_[i])
    {
      return;
    }
  }

  Shape::array newShape = std::make_unique<Shape::pointer[]>(size_ + 1);

  for (size_t i = 0; i < size_ ; i++)
  {
    newShape[i] = shape_array_[i];
  }

  newShape[size_] = shape;
  size_++;
  shape_array_ = std::move(newShape);
}

void korshunov::CompositeShape::remove(size_t index)
{
  if (index >= size_)
  {
    throw std::logic_error("Invalid index");
  }

  for (size_t i = index; i < (size_ - 1); i++) {
    shape_array_[i] = shape_array_[i + 1];
  }

  shape_array_[--size_] = nullptr;
}
