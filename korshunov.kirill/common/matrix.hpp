#ifndef A4_MATRIX_HPP
#define A4_MATRIX_HPP

#include "shape.hpp"
#include <memory>

namespace korshunov
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &source);
    Matrix(Matrix &&source);
    ~Matrix() = default;

    Matrix & operator =(const Matrix &source);
    Matrix & operator =(Matrix &&source);
    Shape::array operator [](size_t layer) const;

    bool operator ==(const Matrix &source) const;
    bool operator !=(const Matrix &source) const;

    size_t getRows() const;
    size_t getColumns() const;
    size_t getLayerSize(size_t layer) const;

    void add(const Shape::pointer &shape, size_t layer);

  private:
    Shape::array data_;
    size_t rows_;
    size_t columns_;
  };
}

#endif
