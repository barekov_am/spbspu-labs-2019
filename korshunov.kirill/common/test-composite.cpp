#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include <cmath>
#include <memory>
#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

BOOST_AUTO_TEST_SUITE(test_suite_composite)

  const double Inaccuracy = 0.01;

  BOOST_AUTO_TEST_CASE(comp_shape_rotate)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 3, 9 }, 3);
    korshunov::Shape::pointer rectanglePtr = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 9, 3 }, 6, 6);
    korshunov::CompositeShape testCompositeShape;
    testCompositeShape.add(circlePtr);
    testCompositeShape.add(rectanglePtr);

    const korshunov::rectangle_t tmpFrameBefore = testCompositeShape.getFrameRect();
    const double tmpAreaBefore = testCompositeShape.getArea();

    testCompositeShape.rotate(90);
    korshunov::rectangle_t tmpFrameAfter = testCompositeShape.getFrameRect();
    double tmpAreaAfter = testCompositeShape.getArea();

    BOOST_CHECK_CLOSE(tmpFrameBefore.width, tmpFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.height, tmpFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, Inaccuracy);

    testCompositeShape.rotate(270);
    tmpFrameAfter = testCompositeShape.getFrameRect();
    tmpAreaAfter = testCompositeShape.getArea();

    BOOST_CHECK_CLOSE(tmpFrameBefore.height, tmpFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.width, tmpFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, Inaccuracy);

    testCompositeShape.rotate(45);
    tmpFrameAfter = testCompositeShape.getFrameRect();
    tmpAreaAfter = testCompositeShape.getArea();
    const double tmpWidth = 9 * sqrt(2) + 3;
    const double tmpHeight = 6 * sqrt(2);

    BOOST_CHECK_CLOSE(tmpWidth, tmpFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpHeight, tmpFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, Inaccuracy);

    testCompositeShape.rotate(-45);

    const korshunov::rectangle_t tmpRectBefore = rectanglePtr->getFrameRect();
    rectanglePtr->rotate(45);
    const korshunov::rectangle_t tmpRectAfter = rectanglePtr->getFrameRect();
    const double dWidth = (tmpRectAfter.width - tmpRectBefore.width) / 2;
    const double dHeight = (tmpRectAfter.height - tmpRectBefore.height) / 2;
    tmpFrameAfter = testCompositeShape.getFrameRect();

    BOOST_CHECK_CLOSE(tmpFrameBefore.width + dWidth, tmpFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.height + dHeight, tmpFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, Inaccuracy);
  }


  BOOST_AUTO_TEST_CASE(test_comp_shape_copy_constructor)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 7, 3 }, 6);
    korshunov::Shape::pointer rectanglePtr = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 5, 0 }, 10, 4);
    korshunov::CompositeShape testCompositeShape;
    testCompositeShape.add(circlePtr);
    testCompositeShape.add(rectanglePtr);

    const double initialArea = testCompositeShape.getArea();
    const korshunov::rectangle_t initialFrame = testCompositeShape.getFrameRect();

    korshunov::CompositeShape testCopyComposite(testCompositeShape);
    const korshunov::rectangle_t resultFrame = testCopyComposite.getFrameRect();

    BOOST_CHECK_EQUAL(testCompositeShape.getSize(), testCopyComposite.getSize());
    BOOST_CHECK_CLOSE(initialArea, testCopyComposite.getArea(), Inaccuracy);
    BOOST_CHECK_CLOSE(initialFrame.width, resultFrame.width, Inaccuracy);
    BOOST_CHECK_CLOSE(initialFrame.height, resultFrame.height, Inaccuracy);
    BOOST_CHECK_CLOSE(initialFrame.pos.x, resultFrame.pos.x, Inaccuracy);
    BOOST_CHECK_CLOSE(initialFrame.pos.y, resultFrame.pos.y, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(test_comp_shape_copy_operator)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 7, 3 }, 6);
    korshunov::Shape::pointer rectanglePtr = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 5, 0 }, 10, 4);
    korshunov::CompositeShape testCompositeShape;
    testCompositeShape.add(circlePtr);
    testCompositeShape.add(rectanglePtr);

    const double initialArea = testCompositeShape.getArea();
    const korshunov::rectangle_t initialFrame = testCompositeShape.getFrameRect();

    korshunov::CompositeShape testCopyComposite;
    testCopyComposite = testCompositeShape;
    const korshunov::rectangle_t resultFrame = testCopyComposite.getFrameRect();

    BOOST_CHECK_EQUAL(testCompositeShape.getSize(), testCopyComposite.getSize());
    BOOST_CHECK_CLOSE(initialArea, testCopyComposite.getArea(), Inaccuracy);
    BOOST_CHECK_CLOSE(initialFrame.width, resultFrame.width, Inaccuracy);
    BOOST_CHECK_CLOSE(initialFrame.height, resultFrame.height, Inaccuracy);
    BOOST_CHECK_CLOSE(initialFrame.pos.x, resultFrame.pos.x, Inaccuracy);
    BOOST_CHECK_CLOSE(initialFrame.pos.y, resultFrame.pos.y, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(test_comp_shape_move_constructor)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 7, 7 }, 7);
    korshunov::CompositeShape testCompositeShape;
    testCompositeShape.add(circlePtr);
    size_t testCompositeShapeSize = testCompositeShape.getSize();
    double testCompositeShapeArea = testCompositeShape.getArea();

    korshunov::CompositeShape testCompositeShapeMove(std::move(testCompositeShape));
    BOOST_CHECK_EQUAL(testCompositeShapeMove.getSize(), testCompositeShapeSize);
    BOOST_CHECK_CLOSE(testCompositeShapeMove.getArea(), testCompositeShapeArea, Inaccuracy);
    BOOST_CHECK_EQUAL(testCompositeShape.getSize(), 0);
    BOOST_CHECK_THROW(testCompositeShape.getArea(), std::logic_error);
  }

  BOOST_AUTO_TEST_CASE(test_comp_shape_move_operator)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 7, 7 }, 7);
    korshunov::CompositeShape testCompositeShape;
    testCompositeShape.add(circlePtr);
    size_t testCompositeShapeSize = testCompositeShape.getSize();
    double testCompositeShapeArea = testCompositeShape.getArea();

    korshunov::CompositeShape testCompositeShapeMove;
    testCompositeShapeMove = std::move(testCompositeShape);
    BOOST_CHECK_EQUAL(testCompositeShapeMove.getSize(), testCompositeShapeSize);
    BOOST_CHECK_CLOSE(testCompositeShapeMove.getArea(), testCompositeShapeArea, Inaccuracy);
    BOOST_CHECK_EQUAL(testCompositeShape.getSize(), 0);
    BOOST_CHECK_THROW(testCompositeShape.getArea(), std::logic_error);
  }

  BOOST_AUTO_TEST_CASE(comp_shape_invar_after_absolute_move)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 7, 7 }, 7);
    korshunov::Shape::pointer rectanglePtr = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 8, 1 }, 5, 1);
    korshunov::CompositeShape testCompositeShape;
    testCompositeShape.add(circlePtr);
    testCompositeShape.add(rectanglePtr);
    const korshunov::rectangle_t testFrameBefore = testCompositeShape.getFrameRect();
    const double testAreaBefore = testCompositeShape.getArea();

    testCompositeShape.move({ 23, 49 });
    korshunov::rectangle_t testFrameAfter = testCompositeShape.getFrameRect();
    double testAreaAfter = testCompositeShape.getArea();
    BOOST_CHECK_CLOSE(testFrameBefore.width, testFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(testFrameBefore.height, testFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(testAreaBefore, testAreaAfter, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(comp_shape_invar_after_relative_move)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 7, 7 }, 7);
    korshunov::Shape::pointer rectanglePtr = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 8, 1 }, 5, 1);
    korshunov::CompositeShape testCompositeShape;
    testCompositeShape.add(circlePtr);
    testCompositeShape.add(rectanglePtr);
    const korshunov::rectangle_t testFrameBefore = testCompositeShape.getFrameRect();
    const double testAreaBefore = testCompositeShape.getArea();

    testCompositeShape.move(23, 49);
    korshunov::rectangle_t testFrameAfter = testCompositeShape.getFrameRect();
    double testAreaAfter = testCompositeShape.getArea();
    BOOST_CHECK_CLOSE(testFrameBefore.width, testFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(testFrameBefore.height, testFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(testAreaBefore, testAreaAfter, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(quadr_change_area_comp_shape_with_coef_more_than_one)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 7, 7 }, 7);
    korshunov::Shape::pointer rectanglePtr = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 8, 1 }, 5, 1);
    korshunov::CompositeShape testCompositeShape;
    testCompositeShape.add(circlePtr);
    testCompositeShape.add(rectanglePtr);
    const double testAreaBefore = testCompositeShape.getArea();
    const double coefScale = 7;

    testCompositeShape.scale(coefScale);
    BOOST_CHECK_CLOSE(testCompositeShape.getArea(), coefScale * coefScale * testAreaBefore, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(quadr_change_area_comp_shape_with_coef_less_than_one)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 7, 7 }, 7);
    korshunov::Shape::pointer rectanglePtr = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 8, 1 }, 5, 1);
    korshunov::CompositeShape testCompositeShape;
    testCompositeShape.add(circlePtr);
    testCompositeShape.add(rectanglePtr);
    const double testAreaBefore = testCompositeShape.getArea();
    const double coefScale = 0.5;

    testCompositeShape.scale(coefScale);
    BOOST_CHECK_CLOSE(testCompositeShape.getArea(), coefScale * coefScale * testAreaBefore, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(comp_shape_incorrect_scale_parameter)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 7, 7 }, 7);
    korshunov::Shape::pointer rectanglePtr = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 8, 1 }, 5, 1);
    korshunov::CompositeShape testCompositeShape;
    testCompositeShape.add(circlePtr);
    testCompositeShape.add(rectanglePtr);

    BOOST_CHECK_THROW(testCompositeShape.scale(-7), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(composite_shape_throwing_exceptions)
  {
    korshunov::Shape::pointer circlePtr = std::make_shared<korshunov::Circle>(korshunov::point_t { 7, 7 }, 7);
    korshunov::Shape::pointer rectanglePtr = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 8, 1 }, 5, 1);
    korshunov::CompositeShape testCompositeShape;

    BOOST_CHECK_THROW(testCompositeShape.getFrameRect(), std::logic_error);
    BOOST_CHECK_THROW(testCompositeShape.getArea(), std::logic_error);
    BOOST_CHECK_THROW(testCompositeShape.move(3,5), std::logic_error);
    BOOST_CHECK_THROW(testCompositeShape.move({ 3, 5 }), std::logic_error);
    BOOST_CHECK_THROW(testCompositeShape.scale(7), std::logic_error);

    testCompositeShape.add(circlePtr);

    BOOST_CHECK_EQUAL(testCompositeShape.getIndex(rectanglePtr), testCompositeShape.getSize());
    BOOST_CHECK_THROW(testCompositeShape.remove(2), std::logic_error);

    korshunov::Shape::pointer nullShape = nullptr;

    BOOST_CHECK_THROW(korshunov::CompositeShape{nullShape}, std::invalid_argument);
    BOOST_CHECK_THROW(testCompositeShape.add(nullShape), std::invalid_argument);
    BOOST_CHECK_THROW(testCompositeShape.getIndex(nullShape), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()


// rotate

// sin = 0

// 30 | 45| 60

//
