#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "partition.hpp"
#include <memory>
#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

BOOST_AUTO_TEST_SUITE(test_suite_composite)

  BOOST_AUTO_TEST_CASE(test_intersection)
  {
    korshunov::Circle testCircle({ 3, 1 }, 4);
    korshunov::Rectangle testRectangle({ 8, 6 }, 4, 4);
    korshunov::Triangle testTriangle({ -20, -3 }, { -23, 5 }, { -18, 0 });

    BOOST_CHECK(korshunov::isIntersected(testCircle.getFrameRect(),testRectangle.getFrameRect()));
    BOOST_CHECK(!(korshunov::isIntersected(testTriangle.getFrameRect(),testRectangle.getFrameRect())));
  }

  BOOST_AUTO_TEST_CASE(test_partition)
  {
    korshunov::Shape::pointer circlePtr1 = std::make_shared<korshunov::Circle>(korshunov::point_t { 3, 1 }, 4);
    korshunov::Shape::pointer rectanglePtr1 = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 9, 4 }, 6, 4);
    korshunov::Shape::pointer circlePtr2 = std::make_shared<korshunov::Circle>(korshunov::point_t { 7, -3 }, 2);
    korshunov::Shape::pointer rectanglePtr2 = std::make_shared<korshunov::Rectangle>(korshunov::point_t { 4, -4 }, 6, 4);
    korshunov::Shape::pointer trianglePtr = std::make_shared<korshunov::Triangle>(korshunov::point_t { -20, -3 },
        korshunov::point_t{ -23, 5 }, korshunov::point_t{ -18, 0 });

    korshunov::CompositeShape composition(circlePtr1);
    composition.add(rectanglePtr1);
    composition.add(circlePtr2);
    composition.add(trianglePtr);
    composition.add(rectanglePtr2);

    const korshunov::Matrix testMatrix = korshunov::part(composition);

    const size_t rows = 3;
    const size_t columns = 2;

    BOOST_CHECK_EQUAL(testMatrix.getColumns(), columns);
    BOOST_CHECK_EQUAL(testMatrix.getRows(), rows);
    BOOST_CHECK_EQUAL(testMatrix.getLayerSize(0), 2);
    BOOST_CHECK_EQUAL(testMatrix.getLayerSize(1), 2);
    BOOST_CHECK_EQUAL(testMatrix.getLayerSize(2), 1);
    BOOST_CHECK(testMatrix[0][0] == circlePtr1);
    BOOST_CHECK(testMatrix[0][1] == trianglePtr);
    BOOST_CHECK(testMatrix[1][0] == rectanglePtr1);
    BOOST_CHECK(testMatrix[1][1] == circlePtr2);
    BOOST_CHECK(testMatrix[2][0] == rectanglePtr2);
  }

BOOST_AUTO_TEST_SUITE_END()
