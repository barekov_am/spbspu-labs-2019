#ifndef A4_PARTITION_HPP
#define A4_PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace korshunov
{
  korshunov::Matrix part(const korshunov::CompositeShape &composition);
}

#endif
