#ifndef A4_BASE_TYPES_HPP_
#define A4_BASE_TYPES_HPP_

namespace korshunov
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    point_t pos;
    double width;
    double height;
  };

  bool isIntersected(const rectangle_t &lhs, const rectangle_t &rhs);
}

#endif
