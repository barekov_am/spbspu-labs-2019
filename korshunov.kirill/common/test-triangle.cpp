#include "triangle.hpp"
#include <cmath>
#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

BOOST_AUTO_TEST_SUITE(test_suite_triangle)

  const double Inaccuracy = 0.01;

  BOOST_AUTO_TEST_CASE(triangle_rotate)
  {
    korshunov::Triangle testTriangle({ 4, 4 }, { 7, 2 }, { -3, 0 });
    const korshunov::rectangle_t tmpFrameBefore = testTriangle.getFrameRect();
    const double tmpAreaBefore = testTriangle.getArea();

    testTriangle.rotate(90);
    korshunov::rectangle_t tmpFrameAfter = testTriangle.getFrameRect();
    double tmpAreaAfter = testTriangle.getArea();

    BOOST_CHECK_CLOSE(tmpFrameBefore.width, tmpFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.height, tmpFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, Inaccuracy);

    testTriangle.rotate(270);
    tmpFrameAfter = testTriangle.getFrameRect();
    tmpAreaAfter = testTriangle.getArea();

    BOOST_CHECK_CLOSE(tmpFrameBefore.width, tmpFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.height, tmpFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, Inaccuracy);

    testTriangle.rotate(45);
    tmpFrameAfter = testTriangle.getFrameRect();
    tmpAreaAfter = testTriangle.getArea();
    const double tmpWidthAfter = 4 * sqrt(2);
    const double tmpHeightAfter = 6 * sqrt(2);

    BOOST_CHECK_CLOSE(tmpWidthAfter, tmpFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpHeightAfter, tmpFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(triangle_invar_after_absolute_move)
  {
    korshunov::Triangle testTriangle({ 7, 6 }, { 1, 13 }, { -7, 3});
    const korshunov::rectangle_t testFrameBefore = testTriangle.getFrameRect();
    const double testAreaBefore = testTriangle.getArea();

    testTriangle.move({ 23, 49 });
    korshunov::rectangle_t testFrameAfter = testTriangle.getFrameRect();
    double testAreaAfter = testTriangle.getArea();
    BOOST_CHECK_CLOSE(testFrameBefore.width, testFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(testFrameBefore.height, testFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(testAreaBefore, testAreaAfter, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(triangle_invar_after_relative_move)
  {
    korshunov::Triangle testTriangle({ 7, 6 }, { 1, 13 }, { -7, 3});
    const korshunov::rectangle_t testFrameBefore = testTriangle.getFrameRect();
    const double testAreaBefore = testTriangle.getArea();

    testTriangle.move(23, 17);
    korshunov::rectangle_t testFrameAfter = testTriangle.getFrameRect();
    double testAreaAfter = testTriangle.getArea();
    BOOST_CHECK_CLOSE(testFrameBefore.width, testFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(testFrameBefore.height, testFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(testAreaBefore, testAreaAfter, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(quadr_change_area_triangle)
  {
    korshunov::Triangle testTriangle({ 7, 6 }, { 1, 13 }, { -7, 3});
    const double testAreaBefore = testTriangle.getArea();
    const double coefScale = 7;

    testTriangle.scale(coefScale);
    BOOST_CHECK_CLOSE(testTriangle.getArea(), coefScale * coefScale * testAreaBefore, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(throwing_exceptions)
  {
    BOOST_CHECK_THROW(korshunov::Triangle({ 0, 6 }, { 0, 13 }, { 0, 3}), std::invalid_argument);

    korshunov::Triangle testTriangle({ 7, 6 }, { 1, 13 }, { -7, 3});
    BOOST_CHECK_THROW(testTriangle.scale(-7), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
