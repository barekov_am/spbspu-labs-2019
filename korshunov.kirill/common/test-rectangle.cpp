#include "rectangle.hpp"
#include <cmath>
#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

BOOST_AUTO_TEST_SUITE(test_suite_rectangle)

  const double Inaccuracy = 0.01;

  BOOST_AUTO_TEST_CASE(rectangle_rotate)
  {
    korshunov::Rectangle testRectangle({ 4, 4 }, 4, 8);
    const korshunov::rectangle_t tmpFrameBefore = testRectangle.getFrameRect();
    const double tmpAreaBefore = testRectangle.getArea();

    testRectangle.rotate(90);
    korshunov::rectangle_t tmpFrameAfter = testRectangle.getFrameRect();
    double tmpAreaAfter = testRectangle.getArea();

    BOOST_CHECK_CLOSE(tmpFrameBefore.width, tmpFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.height, tmpFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.pos.x, tmpFrameAfter.pos.x, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.pos.y, tmpFrameAfter.pos.y, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, Inaccuracy);

    testRectangle.rotate(90);
    tmpFrameAfter = testRectangle.getFrameRect();
    tmpAreaAfter = testRectangle.getArea();

    BOOST_CHECK_CLOSE(tmpFrameBefore.height, tmpFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.width, tmpFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.pos.x, tmpFrameAfter.pos.x, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.pos.y, tmpFrameAfter.pos.y, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, Inaccuracy);

    testRectangle.rotate(45);
    tmpFrameAfter = testRectangle.getFrameRect();
    tmpAreaAfter = testRectangle.getArea();
    const double coef = 1 / sqrt(2);

    BOOST_CHECK_CLOSE((tmpFrameBefore.height + tmpFrameBefore.width) * coef, tmpFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE((tmpFrameBefore.height + tmpFrameBefore.width) * coef, tmpFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.pos.x, tmpFrameAfter.pos.x, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpFrameBefore.pos.y, tmpFrameAfter.pos.y, Inaccuracy);
    BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(rectangle_invar_after_absolute_move)
  {
    korshunov::Rectangle testRectangle({ 7, 7 }, 7, 17);
    const korshunov::rectangle_t testFrameBefore = testRectangle.getFrameRect();
    const double testAreaBefore = testRectangle.getArea();

    testRectangle.move({ 27, 37 });
    korshunov::rectangle_t testFrameAfter = testRectangle.getFrameRect();
    double testAreaAfter = testRectangle.getArea();
    BOOST_CHECK_CLOSE(testFrameBefore.width, testFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(testFrameBefore.height, testFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(testAreaBefore, testAreaAfter, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(rectangle_invar_after_relative_move)
  {
    korshunov::Rectangle testRectangle({ 7, 7 }, 7, 17);
    const korshunov::rectangle_t testFrameBefore = testRectangle.getFrameRect();
    const double testAreaBefore = testRectangle.getArea();

    testRectangle.move(17, 13);
    korshunov::rectangle_t testFrameAfter = testRectangle.getFrameRect();
    double testAreaAfter = testRectangle.getArea();
    BOOST_CHECK_CLOSE(testFrameBefore.width, testFrameAfter.width, Inaccuracy);
    BOOST_CHECK_CLOSE(testFrameBefore.height, testFrameAfter.height, Inaccuracy);
    BOOST_CHECK_CLOSE(testAreaBefore, testAreaAfter, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(quadr_change_area_rectangle)
  {
    korshunov::Rectangle testRectangle({ 7, 7 }, 7, 3);
    const double testAreaBefore = testRectangle.getArea();
    const double coefScale = 7;

    testRectangle.scale(coefScale);
    BOOST_CHECK_CLOSE(testRectangle.getArea(), coefScale * coefScale * testAreaBefore, Inaccuracy);
  }

  BOOST_AUTO_TEST_CASE(throwing_exceptions)
  {
    BOOST_CHECK_THROW(korshunov::Rectangle({ 8, 5 }, 3, -6), std::invalid_argument);

    korshunov::Rectangle testRectangle({ 7, 3 }, 5, 7);
    BOOST_CHECK_THROW(testRectangle.scale(-7), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
