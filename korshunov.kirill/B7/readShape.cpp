#include <algorithm>

#include "readShape.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "square.hpp"

std::shared_ptr<Shape> readShape(std::string& line)
{
  size_t openBr = line.find_first_of("(");
  size_t closeBr = line.find_first_of(")");
  size_t semicolon = line.find_first_of(";");

  if ((closeBr == std::string::npos) || (closeBr <= semicolon) || (semicolon < openBr) || (openBr == 0))
  {
    throw std::invalid_argument("Invalid input");
  }

  std::string nameShape = line.substr(0, openBr);
  int x = std::stoi(line.substr(openBr + 1, semicolon));
  int y = std::stoi(line.substr(semicolon + 1, closeBr));

  if (nameShape == "TRIANGLE")
  {
    return std::make_shared<Triangle>(Triangle({x, y}));
  }
  else if (nameShape == "CIRCLE")
  {
    return std::make_shared<Circle>(Circle({x, y}));
  }
  else if (nameShape == "SQUARE")
  {
    return std::make_shared<Square>(Square({x, y}));
  }

  throw std::invalid_argument("Invalid name of shape");
}
