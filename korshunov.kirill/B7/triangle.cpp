#include "triangle.hpp"

Triangle::Triangle(const point_t& center):
  Shape(center)
{
}

void Triangle::draw(std::ostream& ostream) const
{
  ostream << "TRIANGLE " << '(' << center_.x << "; " << center_.y << ")" << std::endl;
}
