#include "circle.hpp"

Circle::Circle(const point_t& center):
  Shape(center)
{
}

void Circle::draw(std::ostream& ostream) const
{
  ostream << "CIRCLE " << '(' << center_.x << "; " << center_.y << ")" << std::endl;
}
