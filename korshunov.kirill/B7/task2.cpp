#include <algorithm>

#include "readShape.hpp"

void task2()
{

  std::vector<std::shared_ptr<Shape>> vector;

  std::string line;

  while (std::getline(std::cin, line))
  {
    line.erase(std::remove_if(line.begin(), line.end(), [](const char sym){ return std::isspace(sym); }), line.end());

    if (line.empty())
    {
      continue;
    }

    vector.push_back(readShape(line));
  }

  std::cout << "Original:" << std::endl;
  std::for_each(vector.begin(), vector.end(), [](const std::shared_ptr<Shape>& shape){ shape->draw(std::cout); });

  std::stable_sort(vector.begin(), vector.end(), [](const std::shared_ptr<Shape>& shape1, const std::shared_ptr<Shape>& shape2)
  {
    return shape1->isMoreLeft(shape2.get());
  });

  std::cout << "Left-Right:" << std::endl;
  std::for_each(vector.begin(), vector.end(), [](const std::shared_ptr<Shape>& shape){ shape->draw(std::cout); });

  std::reverse(vector.begin(), vector.end());

  std::cout << "Right-Left:\n";
  std::for_each(vector.begin(), vector.end(), [](const std::shared_ptr<Shape>& shape){ shape->draw(std::cout); });

  std::reverse(vector.begin(), vector.end());

  std::stable_sort(vector.begin(), vector.end(), [](const std::shared_ptr<Shape>& shape1, const std::shared_ptr<Shape>& shape2)
  {
    return shape1->isUpper(shape2.get());
  });

  std::cout << "Top-Bottom:\n";
  std::for_each(vector.begin(), vector.end(), [](const std::shared_ptr<Shape>& shape){ shape->draw(std::cout); });

  std::reverse(vector.begin(), vector.end());

  std::cout << "Bottom-Top:\n";
  std::for_each(vector.begin(), vector.end(), [](const std::shared_ptr<Shape>& shape){ shape->draw(std::cout); });

}
