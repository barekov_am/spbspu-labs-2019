#ifndef B7_SHAPE_HPP
#define B7_SHAPE_HPP

#include <iostream>
#include <memory>

struct point_t
{
  int x,y;
};

class Shape
{
public:
  Shape(const point_t&);
  virtual ~Shape() = default;

  bool isMoreLeft(const Shape*) const;
  bool isUpper(const Shape*) const;

  virtual void draw(std::ostream&) const = 0;

protected:
  point_t center_;
};


#endif
