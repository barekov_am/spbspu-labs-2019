#include "square.hpp"

Square::Square(const point_t& center):
  Shape(center)
{
}

void Square::draw(std::ostream& ostream) const
{
  ostream << "SQUARE " << '(' << center_.x << "; " << center_.y << ")" << std::endl;
}
