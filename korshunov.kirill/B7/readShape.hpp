#ifndef B7_READSHAPE_HPP
#define B7_READSHAPE_HPP

#include <memory>
#include <vector>

#include "shape.hpp"

std::shared_ptr<Shape> readShape(std::string&);

#endif
