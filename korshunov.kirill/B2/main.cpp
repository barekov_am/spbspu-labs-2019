#include <iostream>

void lab1();
void lab2();

int main(int argc, char * argv[])
{
  try
  {
    if (argc <= 1)
    {
      std::cerr << "Incorrect number of arguments.";

      return 1;
    }

    char *end_str = nullptr;
    int num = std::strtol(argv[1], &end_str, 10);

    switch (num)
    {
      case 1:
      {
        if (argc != 2)
        {
          std::cerr << "Incorrect number of arquments.";

          return 1;
        }
        lab1();

        break;
      }

      case 2:
      {
        if (argc != 2)
        {
          std::cerr << "Incorrect number of arquments.";

          return 1;
        }
        lab2();
        break;
      }

      default:
      {
        std::cerr << "Incorrect task number.";

        return 1;
      }

    }
  }
  catch (const std::exception &ex)
  {
    std::cerr << ex.what();

    return 1;
  }

  return 0;
}
