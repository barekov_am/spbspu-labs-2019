#ifndef B2_COMMANDS_HPP
#define B2_COMMANDS_HPP

#include <sstream>

#include "queue.hpp"

void executeAdd(QueueWithPriority<std::string>& queue, std::stringstream& str);
void executeGet(QueueWithPriority<std::string>& queue, std::stringstream& str);
void executeAccelerate(QueueWithPriority<std::string>& queue, std::stringstream& str);

#endif
