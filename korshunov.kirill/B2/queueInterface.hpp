#ifndef B2_QUEUEINTERFACE_HPP
#define B2_QUEUEINTERFACE_HPP

#include <list>

enum class ElementPriority
{
  LOW,
  NORMAL,
  HIGH
};

template <typename T>
class QueueWithPriority
{
public:
  void putElementToQueue(const T& element, ElementPriority priority);
  T getFrontElementFromQueue();
  void accelerate();
  bool empty() const;

private:
  std::list<T> low_;
  std::list<T> normal_;
  std::list<T> high_;
};

/*template <typename T>
void QueueWithPriority<T>::putElementToQueue(const T& element, ElementPriority priority)
{
  switch(priority)
  {
    case ElementPriority::LOW:
    {
      low_.push_back(element);
      break;
    }

    case ElementPriority::NORMAL:
    {
      normal_.push_back(element);
      break;
    }

    case ElementPriority::HIGH:
    {
      high_.push_back(element);
      break;
    }
  }
}

template <typename T>
T QueueWithPriority<T>::getFrontElementFromQueue()
{
  if (!high_.empty())
  {
    T element = high_.front();
    high_.pop_front();
    return element;
  }
  else if (!normal_.empty())
  {
    T element = normal_.front();
    normal_.pop_front();
    return element;
  }
  else
  {
    T element = low_.front();
    low_.pop_front();
    return element;
  }
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  high_.splice(high_.end(),low_);
}

template <typename T>
bool QueueWithPriority<T>::empty() const
{
  return high_.empty() && normal_.empty() && low_.empty();
}
*/
#endif
