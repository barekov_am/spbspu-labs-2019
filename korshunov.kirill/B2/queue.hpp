#ifndef B2_QUEUE_HPP
#define B2_QUEUE_HPP

#include "queueInterface.hpp"

template <typename T>
void QueueWithPriority<T>::putElementToQueue(const T& element, ElementPriority priority)
{
  switch(priority)
  {
    case ElementPriority::LOW:
    {
      low_.push_back(element);
      break;
    }

    case ElementPriority::NORMAL:
    {
      normal_.push_back(element);
      break;
    }

    case ElementPriority::HIGH:
    {
      high_.push_back(element);
      break;
    }
  }
}

template <typename T>
T QueueWithPriority<T>::getFrontElementFromQueue()
{
  if (!high_.empty())
  {
    T element = high_.front();
    high_.pop_front();
    return element;
  }
  else if (!normal_.empty())
  {
    T element = normal_.front();
    normal_.pop_front();
    return element;
  }
  else
  {
    T element = low_.front();
    low_.pop_front();
    return element;
  }
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  high_.splice(high_.end(),low_);
}

template <typename T>
bool QueueWithPriority<T>::empty() const
{
  return high_.empty() && normal_.empty() && low_.empty();
}

#endif
