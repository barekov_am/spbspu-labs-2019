#include <iostream>
#include <list>

void lab2()
{
  std::list<int> list;

  const int minVal = 1;
  const int maxVal = 20;
  const size_t maxSize = 20;

  size_t size = 0;
  int value = 0;

  while (std::cin >> value)
  {
    if ((value < minVal) || (value > maxVal))
    {
      throw std::invalid_argument("Invalid value");
    }

    size++;

    if (size > maxSize)
    {
      throw std::runtime_error("The maximum number of elements has been exceeded");
    }

    list.push_back(value);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Failed input");
  }

  if (list.empty())
  {
    return;
  }

  for (size_t i = 0; i < (size / 2); i++)
  {
    std::cout << *list.begin() << " " << *std::prev(list.end()) << " ";
    list.pop_front();
    list.pop_back();
  }

  if (!list.empty())
  {
    std::cout << *list.begin();
    list.pop_front();
  }

  std::cout << "\n";
}
