#include <iostream>
#include <stdexcept>
#include <vector>
#include <random>
#include <cstring>
#include "addition.hpp"

void fillRandom(double * array, int size)
{
  std::mt19937 Rng(time(0));
  std::uniform_real_distribution<double> distrib (-1.0, 1.0);
  for (int i = 0; i < size; ++i)
  {
    array[i] = distrib(Rng);
  }
}

void task_4(const char* sort_direction, const char* array_size)
{
  int size = atoi(array_size);
  if (size == 0)
  {
    throw std::invalid_argument("Invalid array size");
  }

  std::vector<double> vector (size);
  fillRandom(&vector[0],size);

  auto directionType = addition::chooseDirection<double>(sort_direction);

  addition::printContainer<std::vector<double> >(vector);
  addition::sort<addition::by_brackets,std::vector<double> >(vector,directionType);
  addition::printContainer<std::vector<double> >(vector);
}
