#ifndef TASK_HEADERS_HPP
#define TASK_HEADERS_HPP

void task_1(const char* sort_direction);
void task_2(const char* filename);
void task_3();
void task_4(const char* sort_direction, const char* array_size);

#endif
