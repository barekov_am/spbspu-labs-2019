#include <iostream>
#include <stdexcept>
#include <vector>
#include <forward_list>
#include <stdlib.h>
#include <cstring>
#include <functional>
#include "addition.hpp"

void task_1 (const char* sort_direction)
{
  std::vector<int> vector1;

  int n = 0;
  while (std::cin >> n)
  {
    vector1.push_back(n);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Invalid data in standard input stream");
  }

  std::vector<int> vector2 = vector1;
  std::forward_list<int> list1 (vector1.begin(),vector1.end());

  auto directionType = addition::chooseDirection<int>(sort_direction);

  addition::sort <addition::by_brackets,std::vector<int> >(vector1,directionType);
  addition::printContainer<std::vector<int> >(vector1);

  addition::sort <addition::by_at,std::vector<int> >(vector2,directionType);
  addition::printContainer<std::vector<int> >(vector2);

  addition::sort <addition::by_iterator,std::forward_list<int> >(list1,directionType);
  addition::printContainer<std::forward_list<int> >(list1);
}
