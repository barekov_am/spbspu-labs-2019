#ifndef DETAILS_HPP
#define DETAILS_HPP
#include <iostream>
#include <functional>
#include <string.h>
#include <cstddef>
#include <vector>

namespace addition
{

  template <typename C>
  struct by_brackets
  {
    typedef typename C::size_type indextype;
    static indextype getBegin(const C& /**container**/)
    {
      return 0;
    };
    static indextype getEnd(const C& cont)
    {
      return cont.size();
    };
    static typename C::reference get(C& cont, indextype i)
    {
      return cont[i];
    };
  };

  template <typename C>
  struct by_at
  {
    typedef typename C::size_type indextype;
    static indextype getBegin(C& /**container**/)
    {
      return 0;
    };
    static indextype getEnd(const C& cont)
    {
      return cont.size();
    };
    static typename C::reference get(C& cont, indextype i)
    {
      return cont.at(i);
    };
  };

  template <typename C>
  struct by_iterator
  {
    typedef typename C::iterator indextype;
    static indextype getBegin(C& cont)
    {
      return cont.begin();
    };
    static indextype getEnd(C& cont)
    {
      return cont.end();
    };
    static typename C::reference get(C& /**container**/, indextype i)
    {
      return *i;
    };
  };
  template <typename C>
  std::function<bool(const C&, const C&)> chooseDirection(const char *sort_direction)
  {
    if (strcmp(sort_direction, "ascending") == 0)
    {
      return [](C a, C b) {return a > b; };
    }
    else if (strcmp(sort_direction, "descending") == 0)
    {
      return [](C a, C b) {return a < b; };
    }
    else
    {
    throw std::invalid_argument("Invalid direction parametr");
    }
  }
  template <template <typename C> class Traits, class C>
  void sort (C& cont, std::function<bool(typename C::value_type,typename C::value_type)> compare)
  {
    typedef typename Traits<C>::indextype index;
    for (index i = Traits<C>::getBegin(cont); i != Traits<C>::getEnd(cont); ++i)
    {
      for (index j=i; j != Traits<C>::getEnd(cont); ++j)
      {
       if(compare(Traits<C>::get(cont,i),Traits<C>::get(cont,j)))
        {
          std::swap (Traits<C>::get(cont,i),Traits<C>::get(cont,j));
        }
      }
    }
  }


  template <typename C>
  void printContainer (const C& cont)
  {
    for (typename C::const_iterator it = cont.begin(); it != cont.end(); it++)
    {
      std::cout << *it << " ";
    }
    std::cout << "\n";
  }
}

#endif
