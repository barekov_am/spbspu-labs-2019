#ifndef CHECKER_HPP
#define CHECKER_HPP

#include "token.hpp"

class Checker
{
public:
  Checker();
  bool operator ()(const token_t& token);

private:
  token_t last_;
};

#endif
