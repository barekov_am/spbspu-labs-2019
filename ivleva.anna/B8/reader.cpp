#include "reader.hpp"

#include <istream>
#include <locale>

Reader::Reader(std::istream& istream) :
  istream_(istream),
  token_(),
  decimalPoint_(std::use_facet<std::numpunct<char>>(istream_.getloc()).decimal_point())
{}

void Reader::read()
{
  token_.str.clear();
  std::istream::sentry sentry(istream_);

  if (sentry)
  {
    bool cont = true;
    char ch = istream_.get();

    while (cont && !istream_.eof())
    {
      if (std::isspace(ch))
      {
        return;
      }
      else if (std::isalpha(ch))
      {
        cont = checkLetter(ch);
      }
      else if (std::isdigit(ch))
      {
        cont = checkDigit(ch);
      }
      else if (ch == Plus)
      {
        cont = checkPlus();
      }
      else if (ch == Hyphen)
      {
        cont = checkHyphen();
      }
      else if (ch == decimalPoint_)
      {
        cont = checkDecimalPoint();
      }
      else if (std::ispunct(ch))
      {
        cont = checkPunct(ch);
      }

      if (cont)
      {
        ch = istream_.get();
      }
    }
  }
}

bool Reader::checkLetter(char letter)
{
  if (token_.str.empty())
  {
    token_.str += letter;
    token_.type = token_t::WORD;
  }
  else
  {
    if (token_.type == token_t::WORD)
    {
      token_.str += letter;
    }
    else if ((token_.type == token_t::PUNCT) || (token_.type == token_t::DASH))
    {
      istream_.putback(letter);

      return false;
    }
    else
    {
      token_.type = token_t::INVALID;
    }
  }
  
  return true;
}

bool Reader::checkDigit(char digit)
{
  if (token_.str.empty())
  {
    token_.str += digit;
    token_.type = token_t::NUMBER;
  }
  else
  {
    if (token_.type == token_t::NUMBER)
    {
      token_.str += digit;
    }
    else if ((token_.type == token_t::PUNCT) || (token_.type == token_t::DASH))
    {
      istream_.putback(digit);

      return false;
    }
    else
    {
      token_.type = token_t::INVALID;
    }
  }
  
  return true;
}

bool Reader::checkPunct(char punct)
{
  if (token_.str.empty())
  {
    token_.str += punct;
    token_.type = token_t::PUNCT;
  }
  else 
  {
    istream_.putback(punct);
  }

  return false;
}

bool Reader::checkPlus()
{
  if (token_.str.empty())
  {
    token_.str += Plus;
    char ch = istream_.get();

    if (std::isdigit(ch))
    {
      token_.str += ch;
      token_.type = token_t::NUMBER;
    }
    else
    {
      istream_.putback(ch);
      token_.type = token_t::PUNCT;
    }
  }
  else
  {
    istream_.putback(Plus);

    return false;
  }
  
  return true;
}

bool Reader::checkHyphen()
{
  if (token_.str.empty())
  {
    token_.str += Hyphen;
    char ch = istream_.get();

    if (std::isdigit(ch))
    {
      token_.str += ch;
      token_.type = token_t::NUMBER;
    }
    else if (ch == Hyphen)
    {
      token_.str += ch;
      token_.type = token_t::DASH;
    }
    else
    {
      istream_.putback(ch);
      token_.type = token_t::PUNCT;
    }
  }
  else
  {
    if (token_.type == token_t::WORD)
    {
      char ch = istream_.peek();

      if (ch != Hyphen)
      {
        token_.str += Hyphen;
      }
      else
      {
        istream_.putback(Hyphen);

        return false;
      }
    }
    else if (token_.type == token_t::DASH)
    {
      token_.str += Hyphen;
    }
    else
    {
      istream_.putback(Hyphen);

      return false;
    }
  }
  
  return true;
}

bool Reader::checkDecimalPoint()
{
  if (token_.str.empty())
  {
    token_.str += decimalPoint_;
    token_.type = token_t::PUNCT;

    return false;
  }
  else if (token_.type == token_t::NUMBER)
  {
    char ch = istream_.get();

    if (std::isdigit(ch))
    {
      token_.str += decimalPoint_;
      token_.str += ch;
    }
    else
    {
      istream_.putback(ch);
      istream_.putback(decimalPoint_);

      return false;
    }
  }
  else
  {
    istream_.putback(decimalPoint_);

    return false;
  }

  return true;
}

token_t Reader::getToken()
{
  return token_;
}
