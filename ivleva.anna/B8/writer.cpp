#include "writer.hpp"

#include <ostream>

Writer::Writer(std::ostream& ostream, size_t lineWidth) :
  ostream_(ostream),
  lineWidth_(lineWidth),
  str_(),
  last_()
{}

Writer::~Writer()
{
  printStr();
}

void Writer::write(const token_t& token)
{
  size_t length = str_.size() + token.str.size();

  if ((token.type == token_t::WORD) || (token.type == token_t::NUMBER))
  {
    if (!str_.empty())
    {
      ++length;
    }

    if (length > lineWidth_)
    {
      printStr();
    }

    if (!str_.empty())
    {
      addSpace();
    }
 
    str_ += token.str;
    last_ = token;
  }
  else if (token.type == token_t::PUNCT)
  {
   if (length > lineWidth_)
    {
      removeLast();
      printStr();
      
      str_ += last_.str;
    }

    str_ += token.str;
    last_ = token;
  }
  else if (token.type == token_t::DASH)
  {
    if (!str_.empty())
    {
      ++length;
    }

    if (length > lineWidth_)
    {
      removeLast();
      printStr();

      str_ += last_.str;
    }

    addSpace();

    str_ += token.str;
    last_ = token;
  }
  else
  {
    throw std::invalid_argument("Invalid token.");
  }
}

void Writer::printStr()
{
  ostream_ << str_ << "\n";
  str_.clear();
}

void Writer::addSpace()
{
  str_ += " ";
}

void Writer::removeLast()
{
  if (str_.empty())
  {
    throw std::runtime_error("This method is not expected to be called with an empty str_ field.");
  }

  size_t begin = str_.size() - last_.str.size() - 1;
  size_t end = last_.str.size() + 2;
  str_.erase(begin, end);
}
