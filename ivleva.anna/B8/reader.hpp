#ifndef READER_HPP
#define READER_HPP

#include <iosfwd>

#include "token.hpp"

class Reader
{
public:
  Reader(std::istream& istream);

  void read();

  token_t getToken();

private:
  const char Plus = '+';
  const char Hyphen = '-';

  std::istream& istream_;
  token_t token_;
  char decimalPoint_;

  bool checkLetter(char letter);
  bool checkDigit(char digit);
  bool checkPunct(char punct);
  bool checkPlus();
  bool checkHyphen();
  bool checkDecimalPoint();
};

#endif
