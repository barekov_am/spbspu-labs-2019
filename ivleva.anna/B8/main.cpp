#include <cstring>
#include <iostream>
#include <stdexcept>

#include "reader.hpp"
#include "writer.hpp"
#include "checker.hpp"

const size_t LineWidth = 40;
const size_t MaxTokenSize = 20;
const size_t DashSize = 3;

void task(size_t lineWidth)
{
  Reader reader(std::cin);
  Writer writer(std::cout, lineWidth);
  Checker checker; 

  while (!std::cin.eof())
  {
    reader.read();
    token_t token = reader.getToken();
    if (!checker(token))
    {
      throw std::runtime_error("Invalid text.");
    }
    writer.write(token);
  }
}

int main(int argc, char* argv[])
{
  try
  {
    if (argc == 1)
    {
      task(LineWidth);
    }
    else if (argc == 3)
    {
      if (std::strcmp(argv[1], "--line-width") != 0)
      {
        std::cerr << "Incorrect command line arg.\n";
        return 1;
      }

      char* ptr = nullptr;
      size_t lineWidth = std::strtol(argv[2], &ptr, 10);
      if ((*ptr != 0x00) || (lineWidth < MaxTokenSize + DashSize + 1 + 1))
      {
        std::cerr << "Incorrect line width.\n";
        return 1;
      }

      task(lineWidth);
    }
    else
    {
      std::cerr << "Invalid number of args.\n";
      return 1;
    }
  }
  catch (const std::invalid_argument& e)
  {
    std::cerr << e.what() << '\n';

    return 1;
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << '\n';

    return 2;
  }

  return 0;
}
