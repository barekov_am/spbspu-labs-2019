#include "checker.hpp"

const size_t MaxTokenSize = 20;
const size_t DashSize = 3;

Checker::Checker() :
  last_()
{}

bool Checker::operator ()(const token_t& token)
{
  bool isCorrect = true;

  if (token.str.size() > MaxTokenSize)
  {
    isCorrect = false;
  }

  if ((token.type == token_t::DASH) && (token.str.size() != DashSize))
  {
    isCorrect = false;
  }

  if (last_.str.empty() && ((token.type == token_t::PUNCT) || (token.type == token_t::DASH)))
  {
    isCorrect = false;
  }

  if ((last_.type == token_t::PUNCT) || (last_.type == token_t::DASH))
  {
    if (token.type == token_t::PUNCT)
    {
      isCorrect = false;
    }

    if (token.type == token_t::DASH)
    {
      isCorrect = (last_.str == ",");
    }
  }

  if (token.str.empty())
  {
    isCorrect = true;
  }

  last_ = token;

  return isCorrect;
}
