#ifndef TOKEN_HPP
#define TOKEN_HPP

#include <string>

struct token_t
{
  enum TokenType
  {
    WORD,
    NUMBER,
    PUNCT,
    DASH,
    INVALID
  };

  std::string str;
  TokenType type;
};

#endif
