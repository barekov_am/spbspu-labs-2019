#ifndef WRITER_HPP
#define WRITER_HPP

#include <iosfwd>

#include "token.hpp"

class Writer
{
public:
  Writer(std::ostream& ostream, size_t lineWidth);

  ~Writer();

  void write(const token_t& token);

private:
  std::ostream& ostream_;
  size_t lineWidth_;
  std::string str_;
  token_t last_;

  void printStr();
  void addSpace();
  void removeLast();
};

#endif
