#ifndef B2_PRIORITY_QUEUE_HPP
#define B2_PRIORITY_QUEUE_HPP

#include "queue-with-priority.hpp"
#include "iostream"


template <typename QueueElementType>
QueueElementType QueueWithPriority<QueueElementType>::get()
{
  
  if (!high_.empty())
  {
    QueueElementType tmp = high_.front();
    high_.pop_front();
    return tmp;
  }
  else if (!normal_.empty())
  {
    QueueElementType tmp = normal_.front();
    normal_.pop_front();
    return tmp;
  }
  else
  {
    QueueElementType tmp = low_.front();
    low_.pop_front();
    return tmp;
  }

}

template <typename QueueElementType>
void QueueWithPriority<QueueElementType>::add(const QueueElementType &element, ElementPriority priority)
{
  switch (priority)
  {
  case ElementPriority::LOW:
    low_.push_back(element);
    break;
  case ElementPriority::NORMAL:
    normal_.push_back(element);
    break;
  case ElementPriority::HIGH:
    high_.push_back(element);
    break;
  }
}

template <typename QueueElementType>
void QueueWithPriority<QueueElementType>::accelerate()
{
  high_.splice(high_.end(), low_);
}

template <typename QueueElementType>
bool QueueWithPriority<QueueElementType>::empty()
{
  return (low_.empty() && normal_.empty() && high_.empty());
}

#endif //B2_PRIORITY_QUEUE_HPP
