#include "commands.hpp"

#include "priority-queue.hpp"

void add(QueueWithPriority<std::string> &queue, std::stringstream &stream)
{
  static const struct
  {
    const std::string name;
    QueueWithPriority<std::string>::ElementPriority priority;
  } priorities[] =
    {
      {"low", QueueWithPriority<std::string>::LOW},
      {"normal", QueueWithPriority<std::string>::NORMAL},
      {"high", QueueWithPriority<std::string>::HIGH}
    };
  std::string priority;
  stream >> priority;
  auto findPriority = std::find_if(std::begin(priorities), std::end(priorities), [&](const auto &elem) {return elem.name == priority;});

  if (findPriority == std::end(priorities))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  stream >> std::ws;
  std::string data;
  getline(stream, data);

  if (data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.add(data, findPriority->priority);
}

void get(QueueWithPriority<std::string> &queue, std::stringstream &stream)
{
  if (!stream.eof())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (queue.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  std::cout << queue.get() << std::endl;
}

void accelerate(QueueWithPriority<std::string> &queue, std::stringstream &stream)
{
  if (!stream.eof())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.accelerate();
}

void printNumbers(std::list<int>::iterator left, std::list<int>::iterator right)
{
  if (left == right)
  {
    std::cout << "\n";
    return;
  }

  if (left == std::prev(right))
  {
    std::cout << *left << "\n";
    return;
  }

  std::cout << *left << " " << *std::prev(right) << " ";
  ++left;
  --right;
  printNumbers(left, right);
}
