#ifndef B2_QUEUE_WITH_PRIORITY_HPP
#define B2_QUEUE_WITH_PRIORITY_HPP

#include <list>

template <typename QueueElementType>
class QueueWithPriority
{
public:
  enum ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  
  QueueElementType get();
  void add(const QueueElementType &element, ElementPriority priority);
  void accelerate();
  bool empty();

private:
  std::list<QueueElementType> low_;
  std::list<QueueElementType> normal_;
  std::list<QueueElementType> high_;
};

#endif //B2_QUEUE_WITH_PRIORITY_HPP
