#include "statistic.hpp"
#include <limits>
#include <stdexcept>

Statistics::Statistics() :
  m_Min(std::numeric_limits<int>::max()),
  m_Max(std::numeric_limits<int>::min()),
  m_OddSum(0),
  m_EvenSum(0),
  m_First(0),
  m_Count(0),
  m_PositiveCount(0),
  m_NegativeCount(0),
  m_Equality(false)
{
}

void Statistics::operator()(long long int num)
{
  if (num > m_Max)
  {
    m_Max = num;
  }
  if (num < m_Min)
  {
    m_Min = num;
  }

  if (num > 0)
  {
    m_PositiveCount++;
  }
  else if (num < 0)
  {
    m_NegativeCount++;
  }

  if (num % 2 == 0)
  {
    m_EvenSum+=num;
  }
  else
  {
    m_OddSum+=num;
  }

  if (m_Count == 0)
  {
    m_First = num;
  }

  if (num == m_First)
  {
    m_Equality = true;
  }
  else
  {
    m_Equality = false;
  }

  m_Count++;
}

long long int Statistics::getMin() const noexcept
{
  return m_Min;
}

long long int Statistics::getMax() const noexcept
{
  return m_Max;
}

double Statistics::getMean() const noexcept
{
  long long int count = m_Count;

  return (m_OddSum + m_EvenSum) / count;
}

size_t Statistics::getPositiveCount() const noexcept
{
  return m_PositiveCount;
}

size_t Statistics::getNegativeCount() const noexcept
{
  return m_NegativeCount;
}

long long int Statistics::getOddSum() const noexcept
{
  return m_OddSum;
}

long long int Statistics::getEvenSum() const noexcept
{
  return m_EvenSum;
}

bool Statistics::isFLEqual() const noexcept
{
  return m_Equality;
}

bool Statistics::isEmpty () const noexcept
{
  return m_Count == 0;
}
