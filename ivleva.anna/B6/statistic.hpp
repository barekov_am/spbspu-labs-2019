#ifndef STATISTIC_HPP
#define STATISTIC_HPP

#include <cstdlib>

class Statistics
{
  public:
    Statistics();
    void operator()(long long int);
    long long int getMin()const noexcept;
    long long int getMax()const noexcept;
    double getMean()const noexcept;
    size_t getPositiveCount()const noexcept;
    size_t getNegativeCount()const noexcept;
    long long int getOddSum()const noexcept;
    long long int getEvenSum()const noexcept;
    bool isFLEqual()const noexcept;
    bool isEmpty()const noexcept;

  private:
    long long int m_Min, m_Max, m_OddSum, m_EvenSum, m_First;
    size_t m_Count, m_PositiveCount, m_NegativeCount;
    bool m_Equality;
};

#endif // STATISTIC_HPP
