#include "task-h.hpp"
#include <iostream>
#include <algorithm>

#include "factorialContainer.hpp"

void task_2()
{
  FactorialContainer fcontainer;

  std::copy(fcontainer.begin(), fcontainer.end(), std::ostream_iterator<long long>(std::cout, " "));
  std::cout << std::endl;

  std::reverse_copy(fcontainer.begin(), fcontainer.end(), std::ostream_iterator<long long>(std::cout, " "));
  std::cout << std::endl;
}
