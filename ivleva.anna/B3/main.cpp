#include <iostream>

#include "task-h.hpp"


int main(int argc, char * argv[])
{
  try
  {
    if (argc <= 1)
    {
      std::cerr << "Incorrect number of arguments.";

      return 1;
    }

    int num = std::stoi(argv[1]);

    switch (num)
    {
      case 1:
      {
        if (argc != 2)
        {
          std::cerr << "Incorrect number of arquments.";

          return 1;
        }
        task_1();

        break;
      }

      case 2:
      {
        if (argc != 2)
        {
          std::cerr << "Incorrect number of arquments.";

          return 1;
        }
        task_2();

        break;
      }

      default:
      {
        std::cerr << "Incorrect task number.";

        return 1;
      }

    }
  }
  catch (const std::exception &ex)
  {
    std::cerr << ex.what();

    return 2;
  }

  return 0;
}
