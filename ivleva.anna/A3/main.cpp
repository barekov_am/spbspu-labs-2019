#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  ivleva::Rectangle rectangle1({4, 2, {1, 3}});
  ivleva::CompositeShape::p_shape p_rectangle1 = std::make_shared<ivleva::Rectangle>(rectangle1);
  ivleva::Rectangle rectangle2({6, 1, {8, 4}});
  ivleva::CompositeShape::p_shape p_rectangle2 = std::make_shared<ivleva::Rectangle>(rectangle2);
  ivleva::Rectangle rectangle3({3, 2, {1, 4}});
  ivleva::CompositeShape::p_shape p_rectangle3 = std::make_shared<ivleva::Rectangle>(rectangle3);
  ivleva::Rectangle rectangle4({2, 2, {1, 5}});
  ivleva::CompositeShape::p_shape p_rectangle4 = std::make_shared<ivleva::Rectangle>(rectangle4);
  ivleva::Circle circle1(6, {2, 3});
  ivleva::CompositeShape::p_shape p_circle1 = std::make_shared<ivleva::Circle>(circle1);
  ivleva::Circle circle2(3, {6, 4});
  ivleva::CompositeShape::p_shape p_circle2 = std::make_shared<ivleva::Circle>(circle2);
  ivleva::Circle circle3(1, {2, 4});
  ivleva::CompositeShape::p_shape p_circle3 = std::make_shared<ivleva::Circle>(circle3);
  ivleva::Circle circle4(4, {3, 3});
  ivleva::CompositeShape::p_shape p_circle4 = std::make_shared<ivleva::Circle>(circle4);

  ivleva::CompositeShape compositeShape;
  compositeShape.add(p_circle1);
  compositeShape.add(p_circle2);
  compositeShape.add(p_circle3);
  compositeShape.add(p_circle4);
  compositeShape.add(p_rectangle1);
  compositeShape.add(p_rectangle2);
  compositeShape.add(p_rectangle3);
  compositeShape.add(p_rectangle4);
  compositeShape.print();
  
  std::cout << "Composite shape movement\n";
  compositeShape.print();
  std::cout << "Moving dx  " << 5 << " and dy " << 10 << std::endl;
  compositeShape.move(5, 10);
  compositeShape.print();
  std::cout << "Moving to the point (" << 5 << "; " << 10 << ")\n";
  compositeShape.move({5, 10});
  compositeShape.print();
  std::cout << "Scale composite shape" << std::endl;
  compositeShape.scale(2.0);
  compositeShape.print();
  std::cout << "Removing shape" << std::endl;
  compositeShape.remove(3);
  compositeShape.print();

  return 0;
}
