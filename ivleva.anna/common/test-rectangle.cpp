#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testingOfRectangle)

const double accuracy = 0.001;

BOOST_AUTO_TEST_CASE(constancyOfSettings)
{
  ivleva::Rectangle rectangle({4, 2, {1, 3}});
  const ivleva::rectangle_t rectangleFrameBefore = rectangle.getFrameRect();
  const double areaBefore = rectangle.getArea();
  rectangle.move(3, 7);
  ivleva::rectangle_t rectangleFrameAfter = rectangle.getFrameRect();
  double areaAfter = rectangle.getArea();
    
  BOOST_CHECK_CLOSE(rectangleFrameBefore.width, rectangleFrameAfter.width, accuracy);
  BOOST_CHECK_CLOSE(rectangleFrameBefore.height, rectangleFrameAfter.height, accuracy);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, accuracy);

  rectangle.move({4, 3});
  rectangleFrameAfter = rectangle.getFrameRect();
  areaAfter = rectangle.getArea();
    
  BOOST_CHECK_CLOSE(rectangleFrameBefore.width, rectangleFrameAfter.width, accuracy);
  BOOST_CHECK_CLOSE(rectangleFrameBefore.height, rectangleFrameAfter.height, accuracy);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, accuracy);
}

BOOST_AUTO_TEST_CASE(quadraticAreaIncreasing)
{
  ivleva::Rectangle rectangle({4, 2, {1, 3}});
  const double areaBefore = rectangle.getArea();
  const double coefficient = 2.0;
  rectangle.scale(coefficient);
  const double areaAfter = rectangle.getArea();
    
  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, areaAfter, accuracy);
}

BOOST_AUTO_TEST_CASE(quadraticAreaDecreasing)
{
  ivleva::Rectangle rectangle({4, 2, {1, 3}});
  const double areaBefore = rectangle.getArea();
  const double coefficient = 0.2;
  rectangle.scale(coefficient);
  const double areaAfter = rectangle.getArea();

  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, areaAfter, accuracy);
}

BOOST_AUTO_TEST_CASE(incorrectParametersSearching)
{
  ivleva::Rectangle rectangle({4, 2, {1, 3}});
    
  BOOST_CHECK_THROW(rectangle.scale(-2.2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END();
