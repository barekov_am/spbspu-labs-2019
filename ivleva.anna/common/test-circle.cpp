#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testingOfCircle)

const double accuracy = 0.001;

BOOST_AUTO_TEST_CASE(constancyOfSettings)
{
  ivleva::Circle circle(6, {2, 3});
  const ivleva::rectangle_t circleFrameBefore = circle.getFrameRect();
  const double areaBefore = circle.getArea();
  circle.move(3, 6);
  ivleva::rectangle_t circleFrameAfter = circle.getFrameRect();
  double areaAfter = circle.getArea();
    
  BOOST_CHECK_CLOSE(circleFrameBefore.width, circleFrameAfter.width, accuracy);
  BOOST_CHECK_CLOSE(circleFrameBefore.height, circleFrameAfter.height, accuracy);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, accuracy);

  circle.move({6, 3});
  circleFrameAfter = circle.getFrameRect();
  areaAfter = circle.getArea();
    
  BOOST_CHECK_CLOSE(circleFrameBefore.width, circleFrameAfter.width, accuracy);
  BOOST_CHECK_CLOSE(circleFrameBefore.height, circleFrameAfter.height, accuracy);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, accuracy);
}

BOOST_AUTO_TEST_CASE(quadraticAreaIncreasing)
{
  ivleva::Circle circle(6, {2, 3});
  const double areaBefore = circle.getArea();
  const double coefficient = 2.0;
  circle.scale(coefficient);
  const double areaAfter = circle.getArea();
    
  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, areaAfter, accuracy);
}

BOOST_AUTO_TEST_CASE(quadraticAreaDecreasing)
{
  ivleva::Circle circle(6, {2, 3});
  const double areaBefore = circle.getArea();
  const double coefficient = 0.2;
  circle.scale(coefficient);
  const double areaAfter = circle.getArea();
    
  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, areaAfter, accuracy);
}

BOOST_AUTO_TEST_CASE(incorrectParametersSearching)
{
  ivleva::Circle circle(6, {2, 3});
    
  BOOST_CHECK_THROW(circle.scale(-2.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END();
