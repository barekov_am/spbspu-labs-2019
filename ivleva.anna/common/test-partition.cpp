#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include "rectangle.hpp"

using p_shape = std::shared_ptr<ivleva::Shape>;

BOOST_AUTO_TEST_SUITE(testImplementationOfPartition)

BOOST_AUTO_TEST_CASE(correctWorkingIsOverlapping)
{
  ivleva::point_t center1 = {-100, -100};
  ivleva::point_t center2 = {100, 100};

  ivleva::Rectangle rectangle1(2, 4, center1);
  ivleva::Rectangle rectangle2(4, 5, center1);
  ivleva::Circle circle(5, center2);

  p_shape p_rect1 = std::make_shared<ivleva::Rectangle>(rectangle1);
  p_shape p_rect2 = std::make_shared<ivleva::Rectangle>(rectangle2);
  p_shape p_circle = std::make_shared<ivleva::Circle>(circle);

  BOOST_CHECK(!ivleva::isOverlapping(p_rect1, p_circle));
  BOOST_CHECK(ivleva::isOverlapping(p_rect1, p_rect2));
}

BOOST_AUTO_TEST_CASE(correctWorkingOfPart)
{
  ivleva::point_t center1 = {14, 56};
  ivleva::point_t center2 = {-53, 123};
  ivleva::point_t center3 = {59, -68};
  ivleva::point_t center4 = {39, -58};

  ivleva::Circle circle1(5, center1);
  ivleva::Circle circle2(9, center2);
  ivleva::Circle circle3(13, center3);
  ivleva::Rectangle rectangle1(2, 7, center1);
  ivleva::Rectangle rectangle2(8, 9, center4);
  ivleva::Rectangle rectangle3(5, 10, center3);
  const size_t lines = 2;
  const size_t columns = 4;

  p_shape p_rect1 = std::make_shared<ivleva::Rectangle>(rectangle1);
  p_shape p_rect2 = std::make_shared<ivleva::Rectangle>(rectangle2);
  p_shape p_rect3 = std::make_shared<ivleva::Rectangle>(rectangle3);
  p_shape p_circle1 = std::make_shared<ivleva::Circle>(circle1);
  p_shape p_circle2 = std::make_shared<ivleva::Circle>(circle2);
  p_shape p_circle3 = std::make_shared<ivleva::Circle>(circle3);

  ivleva::CompositeShape compositeShape;
  compositeShape.add(p_circle1);
  compositeShape.add(p_circle2);
  compositeShape.add(p_circle3);
  compositeShape.add(p_rect1);
  compositeShape.add(p_rect2);
  compositeShape.add(p_rect3);
  ivleva::Matrix matrix = ivleva::part(compositeShape);

  BOOST_CHECK_EQUAL(lines, matrix.getLines());
  BOOST_CHECK_EQUAL(columns, matrix.getColumns());

  BOOST_CHECK(matrix[0][0] == p_circle1);
  BOOST_CHECK(matrix[0][1] == p_circle2);
  BOOST_CHECK(matrix[0][2] == p_circle3);
  BOOST_CHECK(matrix[0][3] == p_rect2);
  BOOST_CHECK(matrix[1][0] == p_rect1);
  BOOST_CHECK(matrix[1][1] == p_rect3);
}

BOOST_AUTO_TEST_SUITE_END()
