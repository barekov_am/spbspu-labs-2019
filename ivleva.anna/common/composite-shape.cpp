#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

ivleva::CompositeShape::CompositeShape() :
  count_(0)
{
}

ivleva::CompositeShape::CompositeShape(const ivleva::CompositeShape &composite_shape) :
  count_(composite_shape.count_),
  array_(std::make_unique<p_shape[]>(composite_shape.count_))
{
  for (size_t i = 0; i < count_; ++i)
  {
    array_[i] = composite_shape.array_[i];
  }

}

ivleva::CompositeShape::CompositeShape(CompositeShape &&composite_shape) :
  count_(composite_shape.count_),
  array_(std::move(composite_shape.array_))
{
}

ivleva::CompositeShape &ivleva::CompositeShape::operator =(const CompositeShape &rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    std::unique_ptr<p_shape[]> tmpArray(std::make_unique<p_shape[]>(rhs.count_));
    for (size_t i = 0; i < count_; i++)
    {
      tmpArray[i] = rhs.array_[i];
    }
    array_.swap(tmpArray);
  }

  return *this;
}

ivleva::CompositeShape &ivleva::CompositeShape::operator =(CompositeShape &&rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    array_ = std::move(rhs.array_);
  }

  return *this;
}

ivleva::CompositeShape::p_shape ivleva::CompositeShape::operator[](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }
  return array_[index];
}

double ivleva::CompositeShape::getArea() const
{
  double fullArea = 0;
  for (size_t i = 0; i < count_; ++i)
  {
    fullArea += array_[i]->getArea();
  }
  return fullArea;
}

ivleva::rectangle_t ivleva::CompositeShape::getFrameRect() const
{
  rectangle_t tmpFrameRect = array_[0]->getFrameRect();
  double max_x = tmpFrameRect.pos.x;
  double max_y = tmpFrameRect.pos.y;
  double min_x = tmpFrameRect.pos.x;
  double min_y = tmpFrameRect.pos.y;

  for (size_t i = 1; i < count_; ++i)
  {
    rectangle_t tmpRectangle = array_[i]->getFrameRect();
    double rightSide = tmpRectangle.pos.x + tmpRectangle.width / 2;
    max_x = std::max(max_x, rightSide);
    double topSide = tmpRectangle.pos.y + tmpRectangle.height / 2;
    max_y = std::max(max_y, topSide);
    double leftSide = tmpRectangle.pos.x - tmpRectangle.width / 2;
    min_x = std::min(min_x, leftSide);
    double bottomSide = tmpRectangle.pos.y - tmpRectangle.height / 2;
    min_y = std::min(min_y, bottomSide);
  }
  return { {(max_x + min_x) / 2, (max_y + min_y) / 2}, max_x - min_x, max_y - min_y};

}

void ivleva::CompositeShape::move(const point_t &center)
{
  const double dx = center.x - getFrameRect().pos.x;
  const double dy = center.y - getFrameRect().pos.y;
  move(dx, dy);
}


void ivleva::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < count_; i++)
  {
    array_[i]->move(dx, dy);
  }
}

void ivleva::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("The coefficient should be positive");
  }
  const point_t pos = getFrameRect().pos;
  for (size_t i = 0; i < count_; i++)
  {
    const double dx = array_[i]->getFrameRect().pos.x - pos.x;
    const double dy = array_[i]->getFrameRect().pos.y - pos.y;
    array_[i]->move(pos.x + dx * coefficient, pos.y + dy * coefficient);
    array_[i]->scale(coefficient);
  }
}

void ivleva::CompositeShape::print() const
{
  ivleva::rectangle_t tmpRectangle = getFrameRect();
  std::cout << std::endl
            << "Quantity shape in CompositeShape = " << count_ << std::endl
            << "Area = " << getArea() << std::endl
            << "Frame rectangle width = " << tmpRectangle.width
            << ", height = " << tmpRectangle.height
            << ", and with center (" << tmpRectangle.pos.x << ";"
            << tmpRectangle.pos.y << ") " << std::endl << std::endl;
}

size_t ivleva::CompositeShape::getShapeCount() const
{
  return count_;
}

void ivleva::CompositeShape::add(p_shape shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape can not be null");
  }

  std::unique_ptr<p_shape[]> tmpArray(std::make_unique<p_shape[]>(count_ + 1));

  for (size_t i = 0; i < count_; ++i)
  {
    tmpArray[i] = array_[i];
  }
  tmpArray[count_] = shape;
  count_++;
  array_.swap(tmpArray);
}

void ivleva::CompositeShape::remove(size_t index)
{
  if (index > count_)
  {
    throw std::out_of_range("Index out of range");
  }

  std::unique_ptr<p_shape[]> tmpArray(std::make_unique<p_shape[]>(count_ - 1));

  for (size_t i = 0; i < index; ++i)
  {
    tmpArray[i] = array_[i];
  }

  count_--;

  for (size_t i = index; i < count_; ++i)
  {
    tmpArray[i] = array_[i + 1];
  }

  array_.swap(tmpArray);
}

void ivleva::CompositeShape::remove(p_shape shape)
{
  for (size_t i = 0; i < count_; ++i)
  {
    if (shape == array_[i])
    {
      remove(i);
      return;
    }
  }

  throw std::invalid_argument("This shape doesn't exist");
}

void ivleva::CompositeShape::rotate(double angle)
{
  const point_t center = getCenter();
  const double cos_ = cos(angle);
  const double sin_ = sin(angle);

  for (size_t i = 0; i < count_; ++i)
  {
    const point_t other_center = array_[i]->getCenter();
    double center_x = {center.x + (other_center.x - center.x) * cos_
        - (other_center.y - center.y) * sin_};
    double center_y = {center.y + (other_center.x - center.x) * sin_
        - (other_center.y - center.y) * cos_};

    array_[i]->move({center_x, center_y});
    array_[i]->rotate(angle);
  }
}

ivleva::point_t ivleva::CompositeShape::getCenter() const
{
  rectangle_t tmpRectangle = array_[0]->getFrameRect();
  double max_x = tmpRectangle.pos.x + tmpRectangle.width / 2;
  double min_x = tmpRectangle.pos.x - tmpRectangle.width / 2;
  double max_y = tmpRectangle.pos.y + tmpRectangle.height / 2;
  double min_y = tmpRectangle.pos.y - tmpRectangle.height / 2;

  for (size_t i = 1; i < count_; ++i)
  {
    tmpRectangle = array_[i]->getFrameRect();
    max_x = std::max(max_x, tmpRectangle.pos.x + tmpRectangle.width / 2);
    min_x = std::min(min_x, tmpRectangle.pos.x - tmpRectangle.width / 2);
    max_y = std::max(max_y, tmpRectangle.pos.y + tmpRectangle.height / 2);
    min_y = std::min(min_y, tmpRectangle.pos.y - tmpRectangle.height / 2);
  }
  return {(max_x + min_x) / 2, (max_y + min_y) / 2};
}
