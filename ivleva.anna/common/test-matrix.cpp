#include <stdexcept>
#include <iostream>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "partition.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "matrix.hpp"

using p_shape = std::shared_ptr<ivleva::Shape>;

BOOST_AUTO_TEST_SUITE(testingOfMatrix)

BOOST_AUTO_TEST_CASE(quantityOfLayers)
{
  ivleva::point_t center = {0, 0};
  ivleva::Circle circle1(5, center);
  ivleva::Circle circle2(7, center);
  ivleva::Rectangle rect1(5, 10, center);
  ivleva::Rectangle rect2(8, 12, center);
  p_shape p_circle1 = std::make_shared<ivleva::Circle>(circle1);
  p_shape p_circle2 = std::make_shared<ivleva::Circle>(circle2);
  p_shape p_rect1 = std::make_shared<ivleva::Rectangle>(rect1);
  p_shape p_rect2 = std::make_shared<ivleva::Rectangle>(rect2);
  const size_t quantity_layers = 4;

  ivleva::CompositeShape compositeShape;
  compositeShape.add(p_rect1);
  compositeShape.add(p_rect2);
  compositeShape.add(p_circle1);
  compositeShape.add(p_circle2);
  ivleva::Matrix matrix = ivleva::part(compositeShape);

  BOOST_CHECK_EQUAL(quantity_layers, matrix.getLines());
}

BOOST_AUTO_TEST_CASE(quantityOfColumns)
{
  ivleva::Circle circle1(1, {-100, -100});
  ivleva::Circle circle2(1, {1000, -1000});
  ivleva::Rectangle rect1(1, 1, {0, 0});
  ivleva::Rectangle rect2(1, 1, {100, 100});
  p_shape p_circle1 = std::make_shared<ivleva::Circle>(circle1);
  p_shape p_circle2 = std::make_shared<ivleva::Circle>(circle2);
  p_shape p_rect1 = std::make_shared<ivleva::Rectangle>(rect1);
  p_shape p_rect2 = std::make_shared<ivleva::Rectangle>(rect2);
  const size_t quantity_columns = 4;

  ivleva::CompositeShape compositeShape;
  compositeShape.add(p_rect1);
  compositeShape.add(p_rect2);
  compositeShape.add(p_circle1);
  compositeShape.add(p_circle2);
  ivleva::Matrix matrix = ivleva::part(compositeShape);

  BOOST_CHECK_EQUAL(quantity_columns, matrix.getColumns());
}

BOOST_AUTO_TEST_SUITE_END()
