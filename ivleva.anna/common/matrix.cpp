#include "matrix.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

ivleva::Matrix::Matrix() :
  lines_(0),
  columns_(0),
  shapeMatrix()
{
}

ivleva::Matrix::Matrix(const Matrix &matrix) :
  lines_(matrix.getLines()),
  columns_(matrix.getColumns()),
  shapeMatrix(std::make_unique<p_shape[]>(matrix.lines_ * matrix.columns_))
{
  for (size_t i = 0; i < (lines_ * columns_); ++i)
  {
    shapeMatrix[i] = matrix.shapeMatrix[i];
  }
}

ivleva::Matrix::Matrix(Matrix &&matrix) :
  lines_(matrix.lines_),
  columns_(matrix.columns_),
  shapeMatrix(std::move(matrix.shapeMatrix))
{
}

ivleva::Matrix& ivleva::Matrix::operator =(const Matrix &rhs)
{
  if (this != &rhs)
  {
    lines_ = rhs.lines_;
    columns_ = rhs.columns_;
    std::unique_ptr<p_shape[]> tmpShapeMatrix(std::make_unique<p_shape[]>(rhs.lines_ * rhs.columns_));

    for (size_t i = 0; i < (lines_ * columns_); ++i)
    {
      tmpShapeMatrix[i] = rhs.shapeMatrix[i];
    }
    shapeMatrix.swap(tmpShapeMatrix);
  }

  return *this;
}

ivleva::Matrix& ivleva::Matrix::operator =(Matrix &&rhs)
{
  if (this != &rhs)
  {
    lines_ = rhs.lines_;
    columns_ = rhs.columns_;
    shapeMatrix = std::move(rhs.shapeMatrix);
  }

  return *this;
}

std::unique_ptr<ivleva::Matrix::p_shape[]> ivleva::Matrix::operator[](size_t index) const
{
  if (index >= lines_)
  {
    throw std::out_of_range("Index out of range");
  }

  std::unique_ptr<p_shape[]> tmpShapeMatrix(std::make_unique<p_shape[]>(columns_));

  for (size_t i = 0; i < columns_; ++i)
  {
    tmpShapeMatrix[i] = shapeMatrix[index * columns_ + i];
  }

  return tmpShapeMatrix;
}

bool ivleva::Matrix::operator==(const Matrix &rhs) const
{
  if ((lines_ != rhs.lines_) || (columns_ != rhs.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (lines_ * columns_); ++i)
  {
    if (shapeMatrix[i] != rhs.shapeMatrix[i])
    {
      return false;
    }
  }

  return true;
}

bool ivleva::Matrix::operator!=(const Matrix &rhs) const
{
  if (this == &rhs)
  {
    return false;
  }

  return true;
}

void ivleva::Matrix::add(p_shape shape, const pair_t &pair)
{
  size_t lines = (lines_ == pair.line) ? (lines_ + 1) : (lines_);
  size_t columns = (columns_ == pair.column) ? (columns_ + 1) : (columns_);

  std::unique_ptr<p_shape[]> tmpShapeMatrix(std::make_unique<p_shape[]>(lines * columns));
  for (size_t i = 0; i < lines; ++i)
  {
    for (size_t j = 0; j < columns; ++j)
    {
      if ((lines_ == i) || (columns_ == j))
      {
        tmpShapeMatrix[i * columns + j] = nullptr;
      }
      else
      {
        tmpShapeMatrix[i * columns + j] = shapeMatrix[i * columns_ + j];
      }
    }
  }

  tmpShapeMatrix[pair.line * columns + pair.column] = shape;
  shapeMatrix.swap(tmpShapeMatrix);
  lines_ = lines;
  columns_ = columns;
}

void ivleva::Matrix::print() const
{
  std::cout << "In matrix exist " << lines_ << " lines and "
            << columns_ << " columns" << std::endl;
  for (size_t i = 0; i < lines_; ++i)
  {
    for (size_t j = 0; j < columns_; ++j)
    {
      if (shapeMatrix[i * columns_ + j] != nullptr)
      {
        std::cout << "On " << i + 1 << " layer, "
                  << j + 1 << " position is : " << std::endl;
        shapeMatrix[i * columns_ + j]->print();
        std::cout << std::endl << "----------------------------" << std::endl << std::endl;
      }
    }
  }
}

size_t ivleva::Matrix::getLines() const
{
  return lines_;
}

size_t ivleva::Matrix::getColumns() const
{
  return columns_;
}
