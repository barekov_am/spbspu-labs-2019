#ifndef COMPOSITE_SHAPE
#define COMPOSITE_SHAPE
#include <memory>
#include "shape.hpp"

namespace ivleva
{
  class CompositeShape : public Shape
  {
  public:
    using p_shape = std::shared_ptr<Shape>;
    CompositeShape();
    CompositeShape(const CompositeShape &composite_shape);
    CompositeShape(CompositeShape &&composite_shape);
    ~CompositeShape() = default;
    CompositeShape &operator =(const CompositeShape &rhs);
    CompositeShape &operator =(CompositeShape &&rhs);
    p_shape operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const ivleva::point_t &center) override;
    void move(double dx, double dy) override;
    void scale(double coefficient) override;
    void print() const override;
    size_t getShapeCount() const;
    void add(p_shape shape);
    void remove(size_t index);
    void remove(p_shape shape);
    void rotate(double angle) override;
    point_t getCenter() const override;

  private:
    size_t count_;
    std::unique_ptr<p_shape[]> array_;
  };
}

#endif
