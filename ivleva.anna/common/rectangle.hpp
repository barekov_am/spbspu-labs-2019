#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace ivleva
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(double width, double height, const point_t &center);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &center) override;
    void move(double dx, double dy) override;
    void scale(double coefficient) override;
    void print() const override;
    void rotate(double angle) override;
    point_t getCenter() const override;

  private:
    point_t top_[4];
  };
}

#endif
