#include <stdexcept>
#include <iostream>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "partition.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "matrix.hpp"

using p_shape = std::shared_ptr<ivleva::Shape>;

BOOST_AUTO_TEST_SUITE(testingOfCompositeShape)

BOOST_AUTO_TEST_CASE(testingOfCopyingAndMovingConstructor)
{
  ivleva::Circle circle(5, {0, 0});
  ivleva::Rectangle rect(20, 30, {10, 10});
  p_shape p_circle = std::make_shared<ivleva::Circle>(circle);
  p_shape p_rect = std::make_shared<ivleva::Rectangle>(rect);
  ivleva::CompositeShape composite_shape;
  composite_shape.add(p_rect);
  composite_shape.add(p_circle);
  ivleva::Matrix matrix = ivleva::part(composite_shape);

  ivleva::Matrix matrix_copying(matrix);
  BOOST_CHECK_EQUAL(matrix_copying.getLines(), matrix.getLines());
  BOOST_CHECK_EQUAL(matrix_copying.getColumns(), matrix.getColumns());
  BOOST_CHECK(matrix == matrix_copying);

  ivleva::Matrix matrix_moving(std::move(matrix));
  BOOST_CHECK_EQUAL(matrix_copying.getLines(), matrix_moving.getLines());
  BOOST_CHECK_EQUAL(matrix_copying.getColumns(), matrix_moving.getColumns());
  BOOST_CHECK(matrix_moving == matrix_copying);
}

BOOST_AUTO_TEST_CASE(correctWorkingOperators)
{
  ivleva::point_t center1 = {14, 56};
  ivleva::point_t center2 = {-53, 123};
  ivleva::point_t center3 = {39, -58};
  ivleva::point_t center4 = {59, -68};

  ivleva::Circle circle1(5, center1);
  ivleva::Circle circle2(9, center2);
  ivleva::Circle circle3(13, center4);
  ivleva::Circle circle4(10, center1);
  ivleva::Rectangle rect1(2, 7, center1);
  ivleva::Rectangle rect2(8, 10, center3);
  ivleva::Rectangle rect3(5, 10, center4);
  ivleva::Rectangle rect4(3, 5, center1);

  p_shape p_circle1 = std::make_shared<ivleva::Circle>(circle1);
  p_shape p_circle2 = std::make_shared<ivleva::Circle>(circle2);
  p_shape p_circle3 = std::make_shared<ivleva::Circle>(circle3);
  p_shape p_circle4 = std::make_shared<ivleva::Circle>(circle4);
  p_shape p_rect1 = std::make_shared<ivleva::Rectangle>(rect1);
  p_shape p_rect2 = std::make_shared<ivleva::Rectangle>(rect2);
  p_shape p_rect3 = std::make_shared<ivleva::Rectangle>(rect3);
  p_shape p_rect4 = std::make_shared<ivleva::Rectangle>(rect4);

  ivleva::CompositeShape composite_shape;
  composite_shape.add(p_circle1);
  composite_shape.add(p_rect1);
  composite_shape.add(p_circle2);
  composite_shape.add(p_rect2);
  composite_shape.add(p_circle3);
  composite_shape.add(p_rect3);
  ivleva::Matrix matrix = ivleva::part(composite_shape);

  BOOST_CHECK(matrix[0][0] == p_circle1);
  BOOST_CHECK(matrix[0][1] == p_circle2);
  BOOST_CHECK(matrix[0][2] == p_rect2);
  BOOST_CHECK(matrix[0][3] == p_circle3);
  BOOST_CHECK(matrix[1][0] == p_rect1);
  BOOST_CHECK(matrix[1][1] == p_rect3);
  BOOST_CHECK(matrix[0][0] != p_rect3);
  BOOST_CHECK(matrix[1][0] != p_rect4);
  BOOST_CHECK(matrix[0][3] != p_circle4);

  ivleva::CompositeShape composite_shape1;
  ivleva::CompositeShape composite_shape2;
  composite_shape.add(p_circle1);
  composite_shape.add(p_rect1);
  composite_shape.add(p_circle2);
  composite_shape.add(p_rect2);
  composite_shape.add(p_circle3);
  composite_shape.add(p_rect3);
  ivleva::Matrix matrix1 = ivleva::part(composite_shape1);
  ivleva::Matrix matrix2 = ivleva::part(composite_shape2);

  BOOST_CHECK(matrix1 != matrix2);
  BOOST_CHECK(matrix1 != matrix);
  BOOST_CHECK(matrix != matrix2);

  matrix1 = matrix;
  BOOST_CHECK(matrix1 == matrix);
}

BOOST_AUTO_TEST_SUITE_END()
