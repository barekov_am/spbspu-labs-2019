#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

ivleva::Rectangle::Rectangle(double width, double height, const point_t &center)
{
  if ((width <= 0) || (height <= 0))
  {
    throw std::invalid_argument("Invalid width of height of rectangle.");
  }
  top_[0] = {center.x - width / 2, center.y + height / 2};
  top_[1] = {center.x + width / 2, center.y + height / 2};
  top_[2] = {center.x + width / 2, center.y - height / 2};
  top_[3] = {center.x - width / 2, center.y - height / 2};
}

double ivleva::Rectangle::getArea() const
{
  double width_x = top_[1].x - top_[0].x;
  double width_y = top_[1].y - top_[0].y;
  double height_x = top_[1].x - top_[2].x;
  double height_y = top_[1].y - top_[2].y;

  return sqrt(width_x * width_x + width_y * width_y) * sqrt(height_x * height_x + height_y * height_y);
}

ivleva::rectangle_t ivleva::Rectangle::getFrameRect() const
{
  double leftSide = top_[0].x;
  double rightSide = top_[0].x;
  double topSide = top_[0].y;
  double bottomSide = top_[0].y;

  for (int i = 1; i < 4; ++i)
  {
    leftSide = std::min(leftSide, top_[i].x);
    rightSide = std::max(rightSide, top_[i].x);
    topSide = std::max(topSide, top_[i].y);
    bottomSide = std::min(bottomSide, top_[i].y);
  }
  return {{(rightSide + leftSide) / 2, (bottomSide + topSide) / 2},
    rightSide - leftSide, topSide - bottomSide};
}

void ivleva::Rectangle::move(const point_t &center)
{
  point_t this_center = getCenter();
  const double dx = center.x - this_center.x;
  const double dy = center.y - this_center.y;
  move(dx, dy);
}

void ivleva::Rectangle::move(double dx, double dy)
{
  for (int i = 0; i < 4; ++i)
  {
    top_[i].x += dx;
    top_[i].y += dy;
  }
}

void ivleva::Rectangle::scale(double coefficient)
{
  if (coefficient > 0)
  {
    point_t center = getCenter();
    double dx = coefficient * (top_[1].x - center.x);
    double dy = coefficient * (top_[1].y - center.y);

    top_[0] = {center.x - dx, center.y + dy};
    top_[1] = {center.x + dx, center.y + dy};
    top_[2] = {center.x + dx, center.y - dy};
    top_[3] = {center.x - dx, center.y - dy};
  }
  else
  {
    throw std::invalid_argument("Invalid coefficient for rectangle");
  }
}

void ivleva::Rectangle::print() const
{
  rectangle_t rectangle = getFrameRect();
  std::cout << std::endl << "Rectangle with center: (" << rectangle.pos.x
            << ";" << rectangle.pos.y << ")" << std::endl
            << "Frame rectangle width = " << rectangle.width
            << ", height = " << rectangle.height << std::endl
            << "Area = " << getArea() << std::endl << std::endl;
}

void ivleva::Rectangle::rotate(double angle)
{
  const double cos_ = cos(angle);
  const double sin_ = sin(angle);
  point_t center = getCenter();

  for (int i = 0; i < 4; ++i)
  {
    double point_x = center.x + (top_[i].x - center.x) * cos_
        - (top_[i].y - center.y) * sin_;
    double point_y = center.y + (top_[i].x - center.x) * sin_
        + (top_[i].y - center.y) * cos_;
    top_[i] = {point_x, point_y};
  }
}

ivleva::point_t ivleva::Rectangle::getCenter() const
{
  return {(top_[0].x + top_[2].x) / 2, (top_[0].y + top_[2].y) / 2};
}
