#define USE_MATH_DEFINES

#include "circle.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>

ivleva::Circle::Circle(double radius, const point_t &center) :
  radius_(radius),
  center_(center)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("Invalid radius of circle.");
  }
}

double ivleva::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

ivleva::rectangle_t ivleva::Circle::getFrameRect() const
{
  return rectangle_t {center_, 2 * radius_, 2 * radius_};
}

void ivleva::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void ivleva::Circle::move(const point_t &center)
{
  center_ = center;
}

void ivleva::Circle::scale(double coefficient)
{
  if (coefficient > 0)
  {
    radius_ *= coefficient;
  }
  else
  {
    throw std::invalid_argument("Invalid coefficient for circle");
  }
}

void ivleva::Circle::print() const
{
  rectangle_t rectangle = getFrameRect();
  std::cout << std::endl
            << "Circle with center: (" << center_.x
            << ";" << center_.y << ")" << std::endl
            << "radius = " << radius_ << std::endl
            << "Frame rectangle width = " << rectangle.width
            << ", height = " << rectangle.height << std::endl
            << "Area = " << getArea() << std::endl << std::endl;
}

ivleva::point_t ivleva::Circle::getCenter() const
{
  return center_;
}

double ivleva::Circle::getRadius() const
{
  return radius_;
}

void ivleva::Circle::rotate(double)
{}
