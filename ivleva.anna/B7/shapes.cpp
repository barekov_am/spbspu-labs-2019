#include "shapes.hpp"

#include <iostream>

#include "functions.hpp"

Shape::Shape(const Point& point) :
  center_(point)
{}

bool Shape::isMoreLeft(const Shape& other) const
{
  return center_.x < other.center_.x;
}

bool Shape::isUpper(const Shape& other) const
{
  return center_.y > other.center_.y;
}

Circle::Circle(const Point& point) :
  Shape(point)
{ }

void Circle::draw() const
{
  std::cout << "CIRCLE ";
  writePoint(center_);
}

Square::Square(const Point& point) :
  Shape(point)
{ }

void Square::draw() const
{
  std::cout << "SQUARE ";
  writePoint(center_);
}

Triangle::Triangle(const Point& point) :
  Shape(point)
{ }

void Triangle::draw() const
{
  std::cout << "TRIANGLE ";
  writePoint(center_);
}
