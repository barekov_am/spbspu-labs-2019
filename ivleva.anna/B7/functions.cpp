#include "functions.hpp"

#include <iostream>

std::istream& skipSpaces(std::istream& in)
{
  while(std::isblank(in.peek()))
  {
    in.get();
  }

  return in;
}

Shape::Point readPoint()
{
  std::cin >> std::noskipws;

  char leftBr = 0;
  char delim = 0;
  char rightBr = 0;

  Shape::Point point{};

  std::cin >> skipSpaces >> leftBr
           >> skipSpaces >> point.x
           >> skipSpaces >> delim
           >> skipSpaces >> point.y
           >> skipSpaces >> rightBr;

  if ((leftBr != '(') || (delim != ';') || (rightBr != ')'))
  {
    throw std::runtime_error("Invalid reading of point.");
  }

  return point;
}

Shape::shPtr readShape()
{
  std::cin >> std::ws;
  if (std::cin.eof())
  {
    return nullptr;
  }

  std::string nameOfShape;

  while(std::isalpha(std::cin.peek()))
  {
    char c = 0;
    std::cin >> c;
    nameOfShape += c;
  }

  Shape::shPtr ptr = nullptr;
  if (nameOfShape == "CIRCLE")
  {
    ptr = std::make_shared<Circle>(readPoint());
  }
  else if (nameOfShape == "SQUARE")
  {
    ptr = std::make_shared<Square>(readPoint());
  }
  else if (nameOfShape == "TRIANGLE")
  {
    ptr = std::make_shared<Triangle>(readPoint());
  }
  else
  {
    throw std::runtime_error("Invalid reading of shape.");
  }

  return ptr;
}

void writePoint(const Shape::Point& point)
{
  std::cout << "(" << point.x << ";" << point.y << ")\n";
}

void writeShape(const Shape::shPtr& shape)
{
  shape->draw();
}
