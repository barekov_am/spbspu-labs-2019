#include "task-h.hpp"

#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <iterator>

void task_1()
{
  using namespace std::placeholders;

  std::transform(std::istream_iterator<double>(std::cin),
                 std::istream_iterator<double>(),
                 std::ostream_iterator<double>(std::cout, " "),
                 std::bind(std::multiplies<double>(), M_PI, _1));

  if (!std::cin.eof() && !std::cin.good())
  {
    throw std::runtime_error("Reading failed.");
  }

  std::cout << "\n";
}
