#include <iostream>

#include "task-h.hpp"

int main(int argc, char *argv[])
{
  if (argc != 2)
  {
    std::cerr << "Invalid number of arguments!\n";
    return 1;
  }

  try
  {
    const int task = std::atoi(argv[1]);
    switch (task)
    {
    case (1):
    {
      task_1();
      break;
    }
    case (2):
    {
      task_2();
      break;
    }
    default:
    {
      std::cerr << "Invalid task number!\n";
      return 1;
    }
    }
  }
  catch (std::exception &e)
  {
    std::cerr << e.what() << "\n";
    return 1;
  }

  return 0;
}
