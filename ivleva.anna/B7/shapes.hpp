#ifndef SHAPES_HPP
#define SHAPES_HPP

#include <memory>

class Shape
{
public:
  using shPtr = std::shared_ptr<Shape>;

  struct Point
  {
    int x;
    int y;
  };

  Shape(const Point& point);

  virtual ~Shape() = default;

  virtual void draw() const = 0;

  bool isMoreLeft(const Shape& other) const;
  bool isUpper(const Shape& other) const;

protected:
  Point center_;
};

class Circle : public Shape
{
public:
  Circle(const Point& point);

  void draw() const override;
};

class Triangle : public Shape
{
public:
  Triangle(const Point& point);

  void draw() const override;
};

class Square : public Shape
{
public:
  Square(const Point& point);

  void draw() const override;
};

#endif
