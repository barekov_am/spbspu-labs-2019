#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include "shapes.hpp"

std::istream& skipSpaces(std::istream& in);

Shape::Point readPoint();
Shape::shPtr readShape();

void writePoint(const Shape::Point& point);
void writeShape(const Shape::shPtr& shape);

#endif
