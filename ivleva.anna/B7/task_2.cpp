#include "task-h.hpp"

#include <algorithm>
#include <iostream>
#include <list>

#include "functions.hpp"
#include "shapes.hpp"

bool isMoreLeft(const Shape::shPtr& sh1, const Shape::shPtr& sh2)
{
  return sh1->isMoreLeft(*sh2);
}

bool isUpper(const Shape::shPtr& sh1, const Shape::shPtr& sh2)
{
  return sh1->isUpper(*sh2);
}

void task_2()
{
  std::list<Shape::shPtr> list;

  while (!std::cin.eof())
  {
    Shape::shPtr ptr = readShape();

    if (ptr)
    {
      list.push_back(ptr);
    }
  }

  if (!std::cin.eof() && !std::cin.good())
  {
    throw std::runtime_error("Reading failed.");
  }

  std::cout << "Original:\n";
  std::for_each(list.begin(), list.end(), &writeShape);

  list.sort(&isMoreLeft);
  std::cout << "Left-Right:\n";
  std::for_each(list.begin(), list.end(), &writeShape);

  list.reverse();
  std::cout << "Right-Left:\n";
  std::for_each(list.begin(), list.end(), &writeShape);

  list.sort(&isUpper);
  std::cout << "Top-Bottom:\n";
  std::for_each(list.begin(), list.end(), &writeShape);

  list.reverse();
  std::cout << "Bottom-Top:\n";
  std::for_each(list.begin(), list.end(), &writeShape);
}
