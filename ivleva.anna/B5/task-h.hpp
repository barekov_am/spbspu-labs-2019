#ifndef TASK_H_HPP
#define TASK_H_HPP

#include <iostream>

void task_1(std::istream &in, std::ostream &out);
void task_2(std::istream &in, std::ostream &out);

#endif
