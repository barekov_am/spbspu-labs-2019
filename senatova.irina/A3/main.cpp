#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

void printCompositeShapeInfo(senatova::Shape &shape)
{
  shape.print();
  std::cout << "AREA = " << shape.getArea() << std::endl;
  shape.move({2, 1});
  std::cout << "AFTER MOVING TO A POINT: " << std::endl;
  shape.print();
  shape.move(2, 8);
  std::cout << "AFTER ADD TO OX, OY: " << std::endl;
  shape.print();
  shape.scale(2);
  std::cout << "AFTER SCALING: " << std::endl;
  shape.print();
  shape.rotate(45);
  std::cout << "AFTER ROTATION: " << std::endl;
  shape.print();
}


int main()
{
  senatova::Circle circle({6, -4}, 15.3);
  std::cout << "Circle info\n";
  circle.print();
  std::cout << "Add to OX " << 3 << " and to OY " << 2 << std::endl;
  circle.move(3, 2);
  circle.print();
  std::cout << "Move to (" << 29 << ", " << 13 << ")\n";
  circle.move({29, 13});
  circle.print();
  circle.scale(21.4);
  std::cout << "New parameters" << std::endl;
  circle.print();

  senatova::Rectangle rectangle({3, 6}, 5, 8);
  std::cout << "Rectangle info\n";
  rectangle.print();
  std::cout << "Add to OX " << 5 << " and to OY " << -3 << std::endl;
  rectangle.move(5, -3);
  rectangle.print();
  std::cout << "Move to (" << 15 << ", " << 10 << ")\n";
  rectangle.move({15, 10});
  rectangle.print();
  std::cout << std::endl;
  rectangle.scale(11.5);
  std::cout << "New parameters" << std::endl;
  rectangle.print();

  std::cout << "Composite shape\n";
  senatova::Shape::shape_ptr circle2 = std::make_shared<senatova::Circle>(senatova::point_t{5, 6}, 3);
  senatova::Shape::shape_ptr rectangle2 = std::make_shared<senatova::Rectangle>(senatova::point_t{4, 4}, 5, 6);
  senatova::Shape::shape_ptr square = std::make_shared<senatova::Rectangle>(senatova::point_t{5, 5}, 4, 4);
  senatova::CompositeShape compShape(rectangle2);
  compShape.add(circle2);
  compShape.add(rectangle2);
  compShape.add(square);
  std::cout << "Info about composite shape" << std::endl;
  printCompositeShapeInfo(compShape);

  return 0;
}
