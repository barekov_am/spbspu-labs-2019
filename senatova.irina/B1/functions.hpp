#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <iostream>
#include "strategy.hpp"

template <class T>
void print(T &collection)
{
  for (auto i = collection.begin(); i != collection.end(); ++i)
  {
    std::cout << *i << ' ';
  }
  std::cout << std::endl;
}

bool sortDirection(const char *);

void fillRandom(double *array, int size);

template <template <class T> class Strategy, class T>
void sort(T &collection, bool ascending)
{
  const auto begin = Strategy<T>::begin(collection);
  const auto end = Strategy<T>::end(collection);
  for (auto i = begin; i != end; ++i)
  {
    auto tmp = i;
    for (auto j = Strategy<T>::next(i); j != end; ++j)
    {
      if ((ascending)
          and (Strategy<T>::get_element(collection, tmp) > Strategy<T>::get_element(collection, j)))
      {
        tmp = j;
      }
      if ((!ascending)
          and (Strategy<T>::get_element(collection, tmp) < Strategy<T>::get_element(collection, j)))
      {
        tmp = j;
      }
    }
    if (tmp != i)
    {
      std::swap(Strategy<T>::get_element(collection, tmp), Strategy<T>::get_element(collection, i));
    }
  }
}

#endif //FUNCTIONS_HPP
