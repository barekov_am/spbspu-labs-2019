#include <vector>
#include "functions.hpp"

void task4(const char *direction, const int size)
{
  bool dir = sortDirection(direction);
  if (size <= 0)
  {
    throw std::invalid_argument("Size must be more than 0");
  }

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);

  print(vector);
  sort<Brackets>(vector, dir);
  print(vector);
}

