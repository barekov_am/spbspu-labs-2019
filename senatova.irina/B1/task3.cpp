#include <vector>
#include <iterator>
#include <iostream>
#include "functions.hpp"

void task3()
{
  std::vector<int> vector;
  int current = -1;

  while (std::cin >> current)
  {
    if (current == 0)
    {
      break;
    }
    vector.push_back(current);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Input failed");
  }

  if (vector.empty())
  {
    return;
  }

  if (current != 0)
  {
    throw std::invalid_argument("Last number must be zero");
  }

  std::vector<int>::iterator i = vector.begin();
  switch(vector.back())
  {
    case 1:
    {
      while (i != vector.end())
      {
        if ((*i % 2) == 0)
        {
          vector.erase(i);
        }
        else
        {
          ++i;
        }
      }
      break;
    }
    case 2:
    {
      while (i != vector.end())
      {
        if ((*i % 3) == 0)
        {
          i = vector.insert(++i, 3, 1) + 3;
        }
        else
        {
          ++i;
        }
      }
      break;
    }
  }

  print(vector);
}
