#include "functions.hpp"
#include <cstring>
#include <ctime>
#include <cstdlib>

bool sortDirection(const char *direction)
{
  if (std::strcmp(direction, "ascending") == 0)
  {
    return true;
  }
  if (std::strcmp(direction, "descending") != 0)
  {
    throw std::invalid_argument("Wrong direction");
  }
  return false;
}

void fillRandom(double *array, int size)
{
  srand(time(NULL));
  for (int i = 0; i < size; i++)
  {
    array[i] = (rand() % 21) / 10.0 - 1;
  }
}
