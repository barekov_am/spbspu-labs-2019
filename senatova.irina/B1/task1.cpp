#include <forward_list>
#include <vector>
#include <fstream>
#include "functions.hpp"

void task1(const char *direction)
{
  bool dir = sortDirection(direction);
  std::vector<int> vector1;

  int current = 0;

  while (std::cin >> current)
  {
    vector1.push_back(current);
  }
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Input failed");
  }

  if (vector1.empty())
  {
    return;
  }

  std::vector<int> vector2 = vector1;
  std::forward_list<int> vector3(vector1.begin(), vector1.end());

  sort<Brackets>(vector1, dir);
  sort<At>(vector2, dir);
  sort<Iterator>(vector3, dir);

  print(vector1);
  print(vector2);
  print(vector3);
}
