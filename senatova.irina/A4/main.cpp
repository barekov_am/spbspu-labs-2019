#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "partition.hpp"

void printCompositeShapeInfo(senatova::Shape &shape)
{
  shape.print();
  std::cout << "AREA = " << shape.getArea() << std::endl;
  shape.move({2, 1});
  std::cout << "AFTER MOVING TO A POINT: " << std::endl;
  shape.print();
  shape.move(2, 8);
  std::cout << "AFTER ADD TO OX, OY: " << std::endl;
  shape.print();
  shape.scale(2);
  std::cout << "AFTER SCALING: " << std::endl;
  shape.print();
  shape.rotate(45);
  std::cout << "AFTER ROTATION: " << std::endl;
  shape.print();
}

int main()
{
  senatova::Circle circle({9, -4}, 22);
  std::cout << "Circle info: " << "\n\n";
  circle.print();
  std::cout << "Add to OX " << 1 << " and to OY " << 4 << std::endl;
  circle.move(1, 4);
  circle.print();
  std::cout << "Move to point (" << 3 << ", " << 11 << ")\n";
  circle.move({3, 11});
  circle.print();
  circle.scale(13);
  std::cout << "New parameters" << std::endl;
  circle.print();

  senatova::Rectangle rectangle({3, 5}, 5, 6);
  std::cout << "Rectangle info" << "\n\n";
  rectangle.print();
  std::cout << "Add to ОX " << 5 << " and  to OY " << -1 << std::endl;
  rectangle.move(5, -1);
  rectangle.print();
  std::cout << "Move to point (" << 6 << ", " << 19 << ")\n";
  rectangle.move({6, 19});
  rectangle.print();
  std::cout << std::endl;
  rectangle.scale(13);
  std::cout << "New parameters" << std::endl;
  rectangle.print();

  std::cout << "Composite shape" << std::endl;
  senatova::Shape::shape_ptr circle2 = std::make_shared<senatova::Circle>(senatova::point_t {5, 5}, 3);
  senatova::Shape::shape_ptr rectangle2 = std::make_shared<senatova::Rectangle>(senatova::point_t {5, 5}, 4, 4);
  senatova::Shape::shape_ptr square = std::make_shared<senatova::Rectangle>(senatova::point_t {5, 5}, 4, 4);
  senatova::CompositeShape compShape(rectangle2);
  compShape.print();
  compShape.add(circle2);
  compShape.add(rectangle2);
  compShape.add(square);
  std::cout << "Info about Composite Shape" << std::endl;
  printCompositeShapeInfo(compShape);

  std::cout << "Matrix" << std:: endl;
  senatova::Matrix matrix = senatova::part(compShape);
  std::cout << "Layers: " << matrix.getRows() << std::endl;
  std::cout << "Columns: " << matrix.getColumns() << std::endl;
  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    for (size_t j = 0; j < matrix.getLayerSize(i); j++)
    {
      std::cout << "Layer " << i << std::endl;
      std::cout << "Centre is (";
      std::cout << matrix[i][j]->getFrameRect().pos.x << ", " << matrix[i][j]->getFrameRect().pos.y << ")" << std::endl;
    }
  }
  return 0;
}
