#include <iostream>
#include <list>

void task2()
{
  std::list<int> list;
  const int minVal = 1;
  const int maxVal = 20;
  const int maxSize = 20;
  int num = 0;

  while (std::cin >> num)
  {
    if ((num > maxVal) || (num < minVal))
    {
      throw std::invalid_argument("Number must be between 1 and 20");
    }

    if (list.size() + 1 > maxSize)
    {
      throw std::out_of_range("Max size of list is 20");
    }

    list.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Can't read file");
  }

  while (!list.empty())
  {
    std::cout << list.front() << " ";
    list.pop_front();
    if (list.empty())
    {
      break;
    }
    std::cout << list.back() << " ";
    list.pop_back();
  }

  std::cout << std::endl;
}
