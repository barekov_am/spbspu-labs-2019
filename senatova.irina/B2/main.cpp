#include <iostream>

void task1();
void task2();

int main(int argc, char *argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Invalid number of arguments";
      return 1;
    }
    char *end_str = nullptr;
    int num = std::strtol(argv[1], &end_str, 10);
    switch (num)
    {
      case 1:
      {
        task1();
        break;
      }

      case 2:
      {
        task2();
        break;
      }

      default:
      {
        std::cerr << "Invalid number of task";
        return 1;
      }
    }
  }

  catch (const std::exception &ex)
  {
    std::cerr << ex.what();
    return 1;
  }

  return 0;
}
