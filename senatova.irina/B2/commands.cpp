#include <cstring>
#include <iostream>
#include <sstream>

#include "queueWithPriority.hpp"

ElementPriority getPriority(const char *priority)
{
  if (std::strcmp(priority, "high") == 0)
  {
    return ElementPriority::HIGH;
  }

  if (std::strcmp(priority, "normal") == 0)
  {
    return ElementPriority::NORMAL;
  }

  if (std::strcmp(priority, "low") == 0)
  {
    return ElementPriority::LOW;
  }

  throw std::invalid_argument("Invalid priority");
}

void executeAdd(QueueWithPriority<std::string> &queue, std::stringstream &str)
{
  try
  {
    std::string prior;
    std::string data;
    str >> std::ws >> prior;
    std::getline(str >> std::ws, data);

    ElementPriority priority = getPriority(prior.c_str());

    if (data.empty())
    {
      std::cout << "<INVALID COMMAND>\n";
      return;
    }

    queue.putElementToQueue(data, priority);
  }
  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}

void executeGet(QueueWithPriority<std::string> &queue, std::stringstream &str)
{
  std::string line;
  str >> std::ws >> line;

  if (!line.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (queue.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  std::string element = queue.getElementFromQueue();
  std::cout << element << std::endl;
}

void executeAccelerate(QueueWithPriority<std::string> &queue, std::stringstream &str)
{
  std::string data;
  std::getline(str >> std::ws, data);

  if (!data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.accelerate();
}
