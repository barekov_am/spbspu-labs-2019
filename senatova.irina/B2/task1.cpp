#include <iostream>
#include <cstring>
#include <sstream>

#include "queueWithPriority.hpp"

void executeAdd(QueueWithPriority<std::string> &queue, std::stringstream &str);
void executeGet(QueueWithPriority<std::string> &queue, std::stringstream &str);
void executeAccelerate(QueueWithPriority<std::string> &queue, std::stringstream &str);

void task1()
{
  QueueWithPriority<std::string> queue;
  std::string line;

  while (getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Can't read file");
    }

    std::stringstream str(line);
    std::string command;

    str >> command;

    if (strcmp(&command[0], "add") == 0)
    {
      executeAdd(queue, str);
    }
    else if (strcmp(&command[0], "get") == 0)
    {
      executeGet(queue, str);
    }
    else if (strcmp(&command[0], "accelerate") == 0)
    {
      executeAccelerate(queue, str);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
