#ifndef SENATOVA_IRINA_PARTITION_HPP
#define SENATOVA_IRINA_PARTITION_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace senatova
{
  senatova::Matrix part(const senatova::CompositeShape& compShape);
  bool isIntersected(const rectangle_t &shape1, const rectangle_t &shape2);
}

#endif //PARTITION_HPP
