#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <iostream>
#include "shape.hpp"

namespace senatova
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &newShape);
    CompositeShape(CompositeShape &&newShape);
    CompositeShape(const shape_ptr &newShape);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &newShape);
    CompositeShape &operator =(CompositeShape &&newShape);
    shape_ptr operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &newPos) override;
    void move(double dx, double dy) override;
    void scale(double multiplier) override;
    void rotate(double angle) override;
    void add(shape_ptr shape);
    void remove(size_t index);
    size_t getCount() const;
    shape_array getShapes() const;
    void print() const override;
  private:
    shape_array arrayShapes_;
    size_t count_;
  };
}

#endif //COMPOSITE_SHAPE_HPP
