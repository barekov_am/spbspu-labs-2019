#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

senatova::CompositeShape::CompositeShape():
  arrayShapes_(),
  count_(0)
{
}

senatova::CompositeShape::CompositeShape(const CompositeShape &newShape):
  arrayShapes_(std::make_unique<shape_ptr[]>(newShape.count_)),
  count_(newShape.count_)
{
  for (size_t i = 0; i < count_; i++)
  {
    arrayShapes_[i] = newShape.arrayShapes_[i];
  }
}

senatova::CompositeShape::CompositeShape(CompositeShape &&newShape):
  arrayShapes_(std::move(newShape.arrayShapes_)),
  count_(newShape.count_)
{
  newShape.count_ = 0;
}

senatova::CompositeShape::CompositeShape(const shape_ptr &shape):
  arrayShapes_(std::make_unique<shape_ptr[]>(1)),
  count_(1)
{
  arrayShapes_[0] = shape;
}

senatova::CompositeShape &senatova::CompositeShape::operator =(const CompositeShape &newShape)
{
  if (this != &newShape)
  {
    shape_array shapes(std::make_unique<shape_ptr[]>(count_));
    count_ = newShape.count_;
    for (size_t i = 0; i < count_; i++)
    {
      shapes[i] = newShape.arrayShapes_[i];
    }
    arrayShapes_.swap(shapes);
  }
  return *this;
}

senatova::CompositeShape &senatova::CompositeShape::operator =(CompositeShape &&newShape)
{
  if (&newShape != this)
  {
    count_ = newShape.count_;
    arrayShapes_ = std::move(newShape.arrayShapes_);
    newShape.arrayShapes_ = nullptr;
    newShape.count_ = 0;
  }
  return *this;
}

senatova::Shape::shape_ptr senatova::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }
  return arrayShapes_[index];
}

double senatova::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    area += arrayShapes_[i]->getArea();
  }
  return area;
}

senatova::rectangle_t senatova::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  rectangle_t tmpRectangle = arrayShapes_[0]->getFrameRect();
  double minX = tmpRectangle.pos.x - tmpRectangle.width / 2;
  double minY = tmpRectangle.pos.y - tmpRectangle.height / 2;
  double maxX = tmpRectangle.pos.x + tmpRectangle.width / 2;
  double maxY = tmpRectangle.pos.y + tmpRectangle.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    tmpRectangle = arrayShapes_[i]->getFrameRect();
    double tmp = tmpRectangle.pos.x - tmpRectangle.width / 2;
    minX = std::min(tmp, minX);
    tmp = tmpRectangle.pos.y - tmpRectangle.height / 2;
    minY = std::min(tmp, minY);
    tmp = tmpRectangle.pos.x + tmpRectangle.width / 2;
    maxX = std::max(tmp, maxX);
    tmp = tmpRectangle.pos.y + tmpRectangle.height / 2;
    maxY = std::max(tmp, maxY);
  }
  return{(maxX - minX), (maxY - minY), point_t{(maxX + minX) / 2, (maxY + minY) / 2}};
}

void senatova::CompositeShape::move(const point_t &newPos)
{
  rectangle_t rectFrame = getFrameRect();
  double dx = newPos.x - rectFrame.pos.x;
  double dy = newPos.y - rectFrame.pos.y;
  move(dx, dy);
}

void senatova::CompositeShape::move(double dx, double dy)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  for (size_t i = 0; i < count_; i++)
  {
    arrayShapes_[i]->move(dx, dy);
  }
}

void senatova::CompositeShape::scale(double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("Multiplier must be more than 0");
  }
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  senatova::point_t rectFrame = getFrameRect().pos;
  for (size_t i = 0; i < count_; i++)
  {
    point_t shapeCenter = arrayShapes_[i]->getFrameRect().pos;
    double dx = (shapeCenter.x - rectFrame.x) * (multiplier - 1);
    double dy = (shapeCenter.y - rectFrame.y) * (multiplier - 1);
    arrayShapes_[i]->move(dx, dy);
    arrayShapes_[i]->scale(multiplier);
  }
}

void senatova::CompositeShape::add(shape_ptr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  shape_array shapes(std::make_unique<shape_ptr[]>(count_ + 1));
  for (size_t i = 0; i < count_; i++)
  {
    shapes[i] = arrayShapes_[i];
  }
  shapes[count_] = shape;
  count_++;
  arrayShapes_.swap(shapes);
}

void senatova::CompositeShape::rotate(double angle)
{
  if(count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  const double sinA = sin(angle * M_PI / 180);
  const double cosA = cos(angle * M_PI / 180);
  const point_t compCenter = getFrameRect().pos;

  for (size_t i = 0; i < count_; i++)
  {
    arrayShapes_[i]->rotate(angle);
    const point_t shapeCenter = arrayShapes_[i]->getFrameRect().pos;

    const double oldX = shapeCenter.x - compCenter.x;
    const double oldY = shapeCenter.y - compCenter.y;

    const double newX = oldX * cosA - oldY * sinA;
    const double newY = oldX * sinA + oldY * cosA;

    arrayShapes_[i]->move({newX + compCenter.x, newY + compCenter.y});
  }
}

void senatova::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::invalid_argument("Invalid index");
  }
  count_--;
  for (size_t i = index; i < count_; i++)
  {
    arrayShapes_[i] = arrayShapes_[i + 1];
  }
  arrayShapes_[count_] = nullptr;
}

size_t senatova::CompositeShape::getCount() const
{
  return count_;
}

senatova::Shape::shape_array senatova::CompositeShape::getShapes() const
{
  shape_array shapes(std::make_unique<shape_ptr[]>(count_));

  for (size_t i = 0; i < count_; i++)
  {
    shapes[i] = arrayShapes_[i];
  }
  return shapes;
}

void senatova::CompositeShape::print() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  std::cout << "Composite shape: " << std::endl;
  std::cout << "Area: " << getArea() << std::endl;
  rectangle_t frame = getFrameRect();
  std::cout << "Center: (" << frame.pos.x << ", " << frame.pos.y << ")" << std::endl;
  std::cout << "Frame height: " << frame.height << std::endl;
  std::cout << "Frame width: " << frame.width << std::endl;
  std::cout << "Count figures:" << count_ << std::endl;
  std::cout << "Elements info: " << std::endl;
  for (size_t i = 0; i< count_; i++)
  {
    arrayShapes_[i]->print();
    std::cout << std::endl;
  }
}
