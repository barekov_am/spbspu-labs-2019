#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(testCopyConstructor)
{
  senatova::Shape::shape_ptr rectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{3, 4}, 6, 7);
  senatova::Shape::shape_ptr circle = std::make_shared<senatova::Circle>(senatova::point_t{5, 5}, 4);
  senatova::CompositeShape compShape(rectangle);
  compShape.add(circle);

  senatova::Matrix matrix = part(compShape);
  senatova::Matrix testMatrix(matrix);

  BOOST_CHECK(matrix == testMatrix);
  BOOST_CHECK_EQUAL(matrix.getRows(), testMatrix.getRows());
  BOOST_CHECK_EQUAL(matrix.getColumns(), testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(testMoveConstructor)
{
  senatova::Shape::shape_ptr rectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{3, 4}, 6, 7);
  senatova::Shape::shape_ptr circle = std::make_shared<senatova::Circle>(senatova::point_t{5, 5}, 4);
  senatova::CompositeShape compShape(rectangle);
  compShape.add(circle);

  senatova::Matrix matrix = part(compShape);
  senatova::Matrix matrix1(matrix);
  senatova::Matrix testMatrix(std::move(matrix));

  BOOST_CHECK(testMatrix == matrix1);
  BOOST_CHECK_EQUAL(testMatrix.getRows(), matrix1.getRows());
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), matrix1.getColumns());
}

BOOST_AUTO_TEST_CASE(testCopyOperator)
{
  senatova::Shape::shape_ptr rectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{3, 4}, 6, 7);
  senatova::Shape::shape_ptr circle = std::make_shared<senatova::Circle>(senatova::point_t{5, 5}, 4);
  senatova::CompositeShape compShape(rectangle);
  compShape.add(circle);

  senatova::Matrix matrix = part(compShape);
  senatova::Matrix testMatrix = matrix;

  BOOST_CHECK(matrix == testMatrix);
  BOOST_CHECK_EQUAL(matrix.getRows(), testMatrix.getRows());
  BOOST_CHECK_EQUAL(matrix.getColumns(), testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(testOfMoveOperator)
{
  senatova::Shape::shape_ptr rectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{3, 4}, 6, 7);
  senatova::Shape::shape_ptr circle = std::make_shared<senatova::Circle>(senatova::point_t{5, 5}, 4);
  senatova::CompositeShape compShape(rectangle);
  compShape.add(circle);

  senatova::Matrix matrix = part(compShape);
  senatova::Matrix matrix1(matrix);
  senatova::Matrix testMatrix = std::move(matrix);

  BOOST_CHECK(testMatrix == matrix1);
  BOOST_CHECK_EQUAL(testMatrix.getRows(), matrix1.getRows());
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), matrix1.getColumns());
}

BOOST_AUTO_TEST_CASE(testMatrixUsingOfEqualOperator)
{
  senatova::Shape::shape_ptr rect1Ptr = std::make_shared<senatova::Rectangle>(senatova::point_t{3, 4}, 6, 7);
  senatova::Shape::shape_ptr rect2Ptr = std::make_shared<senatova::Rectangle>(senatova::point_t{5, 5}, 4, 5);
  senatova::Shape::shape_ptr circ1Ptr = std::make_shared<senatova::Circle>(senatova::point_t {-1, 2}, 2);
  senatova::Shape::shape_ptr circ2Ptr = std::make_shared<senatova::Circle>(senatova::point_t {0, 1}, 4);

  senatova::CompositeShape compShape(rect1Ptr);
  compShape.add(circ1Ptr);
  compShape.add(rect2Ptr);

  senatova::Matrix matrix = part(compShape);
  senatova::Matrix equalMatrix(matrix);

  senatova::Matrix unequalMatrix(matrix);
  unequalMatrix.add(circ2Ptr, 1);

  BOOST_CHECK(matrix == equalMatrix);
  BOOST_CHECK(matrix != unequalMatrix);
}

BOOST_AUTO_TEST_CASE(testAddingToMatrix)
{
  senatova::Shape::shape_ptr testCircle = std::make_shared<senatova::Circle>(senatova::point_t{0, 0}, 2);
  senatova::Shape::shape_ptr testRectangle1 = std::make_shared<senatova::Rectangle>(senatova::point_t{14, 12}, 2, 1);
  senatova::Shape::shape_ptr testRectangle2 = std::make_shared<senatova::Rectangle>(senatova::point_t{0, 0}, 24, 12);
  senatova::CompositeShape testComposite(testCircle);

  testComposite.add(testRectangle1);
  testComposite.add(testRectangle2);
  size_t rows = 2;
  size_t columns = 2;

  senatova::Matrix testMatrix = senatova::part(testComposite);

  BOOST_CHECK_EQUAL(rows, testMatrix.getRows());
  BOOST_CHECK_EQUAL(columns, testMatrix.getColumns());

  senatova::Shape::shape_ptr testRectangle3 = std::make_shared<senatova::Rectangle>(senatova::point_t{1, 3}, 20, 12);

  testMatrix.add(testRectangle3, rows);
  rows++;

  BOOST_CHECK_EQUAL(rows, testMatrix.getRows());
  BOOST_CHECK_EQUAL(columns, testMatrix.getColumns());

  senatova::Shape::shape_ptr testCircle2 = std::make_shared<senatova::Circle>(senatova::point_t{-100, -40}, 6);

  testMatrix.add(testCircle2, 0);
  columns++;

  BOOST_CHECK_EQUAL(rows, testMatrix.getRows());
  BOOST_CHECK_EQUAL(columns, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(testMatrixCreation)
{
  senatova::Shape::shape_ptr testCircle = std::make_shared<senatova::Circle>(senatova::point_t{0, 0}, 3);
  senatova::Shape::shape_ptr testRectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{10, 10}, 2, 1);
  senatova::CompositeShape testComposite(testCircle);
  testComposite.add(testRectangle);

  size_t rows = 1;
  const size_t columns = 2;

  senatova::Matrix testMatrix = senatova::part(testComposite);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), rows);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), columns);

  senatova::Shape::shape_ptr testRectangle2 = std::make_shared<senatova::Rectangle>(senatova::point_t{9, 8}, 5, 4);
  rows++;

  testComposite.add(testRectangle2);
  testMatrix = senatova::part(testComposite);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), rows);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), columns);
}

BOOST_AUTO_TEST_CASE(testOfInvalidParameters)
{
  senatova::Shape::shape_ptr rectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{3, 4}, 6, 7);
  senatova::Shape::shape_ptr circle = std::make_shared<senatova::Circle>(senatova::point_t{5, 5}, 4);
  senatova::CompositeShape compShape(rectangle);
  compShape.add(circle);

  senatova::Matrix matrix = part(compShape);
  BOOST_CHECK_THROW(matrix[9][2], std::out_of_range);
  BOOST_CHECK_THROW(matrix[-4][1], std::out_of_range);
  BOOST_CHECK_NO_THROW(matrix[1][1]);
}

BOOST_AUTO_TEST_SUITE_END();
