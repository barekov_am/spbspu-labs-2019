#include <boost/test/auto_unit_test.hpp>
#include <memory>
#include <cmath>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(A3Test)

const double PRECISION = 0.001;

BOOST_AUTO_TEST_CASE(testCompositeShapeCopyConstructor)
{
  senatova::Shape::shape_ptr testCircle = std::make_shared<senatova::Circle>(senatova::point_t{9, -5}, 3);
  senatova::Shape::shape_ptr testRectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{3, 6}, 2, 4);
  senatova::CompositeShape beforeCompositeShape(testCircle);

  beforeCompositeShape.add(testRectangle);
  senatova::CompositeShape copyCompositeShape(beforeCompositeShape);
  const senatova::rectangle_t frameRect = beforeCompositeShape.getFrameRect();
  const senatova::rectangle_t copyFrameRect = copyCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, PRECISION);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, PRECISION);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(beforeCompositeShape.getArea(), copyCompositeShape.getArea(), PRECISION);
  BOOST_CHECK_EQUAL(beforeCompositeShape.getCount(), copyCompositeShape.getCount());
}

BOOST_AUTO_TEST_CASE(testCompositeShapeMoveConstructor)
{
  senatova::Shape::shape_ptr testCircle = std::make_shared<senatova::Circle>(senatova::point_t{9, -5}, 3);
  senatova::Shape::shape_ptr testRectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{3, 6}, 2, 4);
  senatova::CompositeShape beforeCompositeShape(testRectangle);

  beforeCompositeShape.add(testCircle);
  const senatova::rectangle_t frameRect = beforeCompositeShape.getFrameRect();
  const double areaCompositeShape = beforeCompositeShape.getArea();
  const size_t countCompositeShape = beforeCompositeShape.getCount();

  senatova::CompositeShape moveCompositeShape(std::move(beforeCompositeShape));
  const senatova::rectangle_t moveFrameRect = moveCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, PRECISION);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, PRECISION);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(areaCompositeShape, moveCompositeShape.getArea(), PRECISION);
  BOOST_CHECK_EQUAL(countCompositeShape, moveCompositeShape.getCount());
  BOOST_CHECK_CLOSE(beforeCompositeShape.getArea(), 0, PRECISION);
  BOOST_CHECK_EQUAL(beforeCompositeShape.getCount(), 0);
}

BOOST_AUTO_TEST_CASE(testMoveOperator)
{
  senatova::Shape::shape_ptr testCircle = std::make_shared<senatova::Circle>(senatova::point_t{9, -5}, 3);
  senatova::Shape::shape_ptr testRectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{3, 6}, 2, 4);
  senatova::Rectangle rect({3, 6}, 2, 4);
  senatova::Circle circ({9, -5}, 3);
  senatova::CompositeShape compShape;

  compShape.add(std::make_shared<senatova::Circle>(circ));
  compShape.add(std::make_shared<senatova::Rectangle>(rect));

  senatova::CompositeShape testCompShape;
  const double areaBefore = compShape.getArea();
  const senatova::rectangle_t testRect = compShape.getFrameRect();
  testCompShape.add(testRectangle);

  testCompShape = std::move(compShape);

  BOOST_CHECK_CLOSE(areaBefore, testCompShape.getArea(), PRECISION);
  BOOST_CHECK_CLOSE(testRect.pos.x, testCompShape.getFrameRect().pos.x, PRECISION);
  BOOST_CHECK_CLOSE(testRect.pos.y, testCompShape.getFrameRect().pos.y, PRECISION);
  BOOST_CHECK_CLOSE(testRect.height, testCompShape.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(testRect.width, testCompShape.getFrameRect().width, PRECISION);
}

BOOST_AUTO_TEST_CASE(InvariabilityAfterMovingCompositeShapeByIncrement)
{
  senatova::Shape::shape_ptr testCircle = std::make_shared<senatova::Circle>(senatova::point_t{9, -5}, 3);
  senatova::Shape::shape_ptr testRectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{3, 6}, 2, 4);
  senatova::CompositeShape testCompositeShape;
  testCompositeShape.add(testCircle);
  testCompositeShape.add(testRectangle);

  const senatova::rectangle_t frameBeforeMoving = testCompositeShape.getFrameRect();
  const double areaBeforeMoving = testCompositeShape.getArea();

  testCompositeShape.move(9, -3);

  const senatova::rectangle_t frameAfterMoving = testCompositeShape.getFrameRect();
  const double areaAfterMoving = testCompositeShape.getArea();

  BOOST_CHECK_CLOSE(areaAfterMoving, areaBeforeMoving, PRECISION);
  BOOST_CHECK_CLOSE(frameAfterMoving.height, frameBeforeMoving.height, PRECISION);
  BOOST_CHECK_CLOSE(frameAfterMoving.width, frameBeforeMoving.width, PRECISION);
}

BOOST_AUTO_TEST_CASE(invariabilityAfterMovingCompositeShapeToPoint)
{
  senatova::Shape::shape_ptr testCircle = std::make_shared<senatova::Circle>(senatova::point_t{9, -5}, 3);
  senatova::Shape::shape_ptr testRectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{3, 6}, 2, 4);
  senatova::CompositeShape testCompositeShape;
  testCompositeShape.add(testCircle);
  testCompositeShape.add(testRectangle);

  const senatova::rectangle_t frameRectBeforeMoving = testCompositeShape.getFrameRect();
  const double areaBeforeMoving = testCompositeShape.getArea();

  testCompositeShape.move({10, -3});

  const senatova::rectangle_t frameAfterMoving = testCompositeShape.getFrameRect();
  const double areaAfterMoving = testCompositeShape.getArea();

  BOOST_CHECK_CLOSE(areaAfterMoving, areaBeforeMoving, PRECISION);
  BOOST_CHECK_CLOSE(frameAfterMoving.height, frameRectBeforeMoving.height, PRECISION);
  BOOST_CHECK_CLOSE(frameAfterMoving.width, frameRectBeforeMoving.width, PRECISION);
}

BOOST_AUTO_TEST_CASE(quadraticCompositeShapeScaleChange)
{
  senatova::Shape::shape_ptr testCircle = std::make_shared<senatova::Circle>(senatova::point_t{13, 4}, 7);
  senatova::Shape::shape_ptr testRectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{4, 5}, 7, 2);
  senatova::CompositeShape testCompositeShape;
  testCompositeShape.add(testRectangle);
  testCompositeShape.add(testCircle);

  const double areaBeforeScale = testCompositeShape.getArea();
  const size_t multiplier  = 2.0;
  const senatova::point_t centre = testCompositeShape.getFrameRect().pos;

  testCompositeShape.scale(multiplier);
  const senatova::point_t compCentre = testCompositeShape.getFrameRect().pos;
  const senatova::point_t ShapeCentre1 = testCompositeShape[0]->getFrameRect().pos;
  const senatova::point_t ShapeCentre2 = testCompositeShape[1]->getFrameRect().pos;


  double areaAfterScale = testCompositeShape.getArea();

  BOOST_CHECK_CLOSE(areaBeforeScale * multiplier * multiplier, areaAfterScale, PRECISION);
  BOOST_CHECK_CLOSE(centre.x, compCentre.x, PRECISION);
  BOOST_CHECK_CLOSE(centre.y, compCentre.y, PRECISION);
  BOOST_CHECK_CLOSE(ShapeCentre1.x, 4 + (4 - compCentre.x) * (multiplier - 1), PRECISION);
  BOOST_CHECK_CLOSE(ShapeCentre1.y, 5 + (5 - compCentre.y) * (multiplier - 1), PRECISION);
  BOOST_CHECK_CLOSE(ShapeCentre2.x, 13 + (13 - compCentre.x) * (multiplier - 1), PRECISION);
  BOOST_CHECK_CLOSE(ShapeCentre2.y, 4 + (4 - compCentre.y) * (multiplier - 1), PRECISION);

}

BOOST_AUTO_TEST_CASE(testOfCorrectRotation)
{
  senatova::Shape::shape_ptr testCircle = std::make_shared<senatova::Circle>(senatova::point_t{3, 9}, 3);
  senatova::Shape::shape_ptr testRectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{9, 3}, 6, 6);
  senatova::CompositeShape testCompositeShape;
  testCompositeShape.add(testCircle);
  testCompositeShape.add(testRectangle);

  const senatova::rectangle_t tmpFrameBefore = testCompositeShape.getFrameRect();
  const double tmpAreaBefore = testCompositeShape.getArea();

  testCompositeShape.rotate(90);
  senatova::rectangle_t tmpFrameAfter = testCompositeShape.getFrameRect();
  double tmpAreaAfter = testCompositeShape.getArea();

  BOOST_CHECK_CLOSE(tmpFrameBefore.width, tmpFrameAfter.height, PRECISION);
  BOOST_CHECK_CLOSE(tmpFrameBefore.height, tmpFrameAfter.width, PRECISION);
  BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, PRECISION);

  testCompositeShape.rotate(270);
  tmpFrameAfter = testCompositeShape.getFrameRect();
  tmpAreaAfter = testCompositeShape.getArea();

  BOOST_CHECK_CLOSE(tmpFrameBefore.height, tmpFrameAfter.height, PRECISION);
  BOOST_CHECK_CLOSE(tmpFrameBefore.width, tmpFrameAfter.width, PRECISION);
  BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, PRECISION);

  testCompositeShape.rotate(45);
  tmpFrameAfter = testCompositeShape.getFrameRect();
  tmpAreaAfter = testCompositeShape.getArea();
  const double tmpWidth = 9 * sqrt(2) + 3;
  const double tmpHeight = 6 * sqrt(2);

  BOOST_CHECK_CLOSE(tmpWidth, tmpFrameAfter.width, PRECISION);
  BOOST_CHECK_CLOSE(tmpHeight, tmpFrameAfter.height, PRECISION);
  BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, PRECISION);

  testCompositeShape.rotate(-45);

  const senatova::rectangle_t tmpRectBefore = testRectangle->getFrameRect();
  testRectangle->rotate(45);
  const senatova::rectangle_t tmpRectAfter = testRectangle->getFrameRect();
  const double dWidth = (tmpRectAfter.width - tmpRectBefore.width) / 2;
  const double dHeight = (tmpRectAfter.height - tmpRectBefore.height) / 2;
  tmpFrameAfter = testCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(tmpFrameBefore.width + dWidth, tmpFrameAfter.width, PRECISION);
  BOOST_CHECK_CLOSE(tmpFrameBefore.height + dHeight, tmpFrameAfter.height, PRECISION);
  BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, PRECISION);
}

BOOST_AUTO_TEST_CASE(invalidCompositeShapeParametersAndScaling)
{
  senatova::Shape::shape_ptr testCircle = std::make_shared<senatova::Circle>(senatova::point_t{20, 11}, 4.2);
  senatova::Shape::shape_ptr testRectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{6, 7}, 3, 2);
  senatova::CompositeShape testCompositeShape;
  testCompositeShape.add(testCircle);
  testCompositeShape.add(testRectangle);

  BOOST_CHECK_THROW(testCompositeShape.scale(-3.6), std::invalid_argument);
  BOOST_CHECK_THROW(testCompositeShape.remove(2), std::logic_error);
  BOOST_CHECK_THROW(testCompositeShape.remove(-1), std::logic_error);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleMultiplierMoreThanOne)
{
  senatova::Shape::shape_ptr testCircle = std::make_shared<senatova::Circle>(senatova::point_t{9, -5}, 3);
  senatova::Shape::shape_ptr testRectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{3, 6}, 2, 4);
  senatova::CompositeShape compShape(testRectangle);
  compShape.add(testCircle);

  const senatova::rectangle_t frameBeforeScale = compShape.getFrameRect();
  const int multiplier = 2;

  compShape.scale(multiplier);
  senatova::rectangle_t frameAfterScale = compShape.getFrameRect();

  BOOST_CHECK_CLOSE(frameBeforeScale.height * multiplier, frameAfterScale.height, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeScale.width * multiplier, frameAfterScale.width, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.x, frameAfterScale.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.y, frameAfterScale.pos.y, PRECISION);
  BOOST_CHECK(frameBeforeScale.height < frameAfterScale.height);
  BOOST_CHECK(frameBeforeScale.width < frameAfterScale.width);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleMultiplierLessThanOne)
{
  senatova::Shape::shape_ptr testCircle = std::make_shared<senatova::Circle>(senatova::point_t{9, -5}, 3);
  senatova::Shape::shape_ptr testRectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{3, 6}, 2, 4);
  senatova::CompositeShape compShape(testRectangle);
  compShape.add(testCircle);

  const senatova::rectangle_t frameBeforeScale = compShape.getFrameRect();
  const double multiplier = 0.3;

  compShape.scale(multiplier);
  senatova::rectangle_t frameAfterScale = compShape.getFrameRect();

  BOOST_CHECK_CLOSE(frameBeforeScale.height * multiplier, frameAfterScale.height, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeScale.width * multiplier, frameAfterScale.width, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.x, frameAfterScale.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.y, frameAfterScale.pos.y, PRECISION);
  BOOST_CHECK(frameBeforeScale.height > frameAfterScale.height);
  BOOST_CHECK(frameBeforeScale.width > frameAfterScale.width);
}

BOOST_AUTO_TEST_CASE(constancyOfRotation)
{
  senatova::Shape::shape_ptr circ = std::make_shared<senatova::Circle>(senatova::point_t{9, -5}, 3);
  senatova::Shape::shape_ptr rect = std::make_shared<senatova::Rectangle>(senatova::point_t{3, 6}, 2, 4);
  senatova::CompositeShape testComposition;

  testComposition.add(circ);
  testComposition.add(rect);

  const double areaBefore = testComposition.getArea();
  const senatova::rectangle_t frameRectBefore = testComposition.getFrameRect();

  double angle = 360;
  testComposition.rotate(angle);

  double areaAfter = testComposition.getArea();
  senatova::rectangle_t frameRectAfter = testComposition.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);

  angle = 90;
  testComposition.rotate(angle);

  areaAfter = testComposition.getArea();
  frameRectAfter = testComposition.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);

  angle = 0;
  testComposition.rotate(angle);

  areaAfter = testComposition.getArea();
  frameRectAfter = testComposition.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);

  angle = -450;
  testComposition.rotate(angle);

  areaAfter = testComposition.getArea();
  frameRectAfter = testComposition.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);

}


BOOST_AUTO_TEST_CASE(checkCompositeShapeAfterAdd)
{
  senatova::Shape::shape_ptr testCircle = std::make_shared<senatova::Circle>(senatova::point_t{13.2, 4}, 7);
  senatova::Shape::shape_ptr testRectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{4, 5}, 7, 2);
  senatova::CompositeShape testCompositeShape;
  testCompositeShape.add(testCircle);
  testCompositeShape.add(testRectangle);

  const double areaBeforeAdd = testCompositeShape.getArea();
  const double circleArea = testCircle->getArea();

  testCompositeShape.add(testCircle);

  double areaAfterAdd = testCompositeShape.getArea();

  BOOST_CHECK_CLOSE(areaBeforeAdd + circleArea, areaAfterAdd, PRECISION);
}

BOOST_AUTO_TEST_CASE(checkCompositeShapeAfterDelete)
{
  senatova::Shape::shape_ptr testCircle = std::make_shared<senatova::Circle>(senatova::point_t{13.2, 4}, 7);
  senatova::Shape::shape_ptr testRectangle = std::make_shared<senatova::Rectangle>(senatova::point_t{4, 5}, 7, 2);
  senatova::CompositeShape testCompositeShape;
  testCompositeShape.add(testRectangle);
  testCompositeShape.add(testCircle);

  const double areaAfterAdd = testCompositeShape.getArea();
  const double circleArea = testCircle->getArea();

  testCompositeShape.remove(1);
  double areaAfterDelete = testCompositeShape.getArea();

  BOOST_CHECK_CLOSE(areaAfterAdd - circleArea, areaAfterDelete, PRECISION);
}

BOOST_AUTO_TEST_SUITE_END()
