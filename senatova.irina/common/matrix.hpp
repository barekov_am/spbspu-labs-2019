#ifndef MATRIX_HPP
#define MATRIX_HPP
#include <memory>
#include "shape.hpp"

namespace senatova
{
  class Matrix {
  public:
    Matrix();
    Matrix(const Matrix &matrix);
    Matrix(Matrix &&matrix);
    ~Matrix() = default;

    Matrix& operator =(const Matrix& matrix);
    Matrix& operator =(Matrix&& matrix);
    Shape::shape_array operator [](size_t index) const;
    bool operator ==(const Matrix &matrix) const;
    bool operator !=(const Matrix &matrix) const;

    size_t getRows() const;
    size_t getColumns() const;
    size_t getLayerSize(size_t layer) const;
    void add(Shape::shape_ptr shape, size_t layer);

  private:
    Shape:: shape_array arrayShapes_;
    size_t rows_;
    size_t columns_;
};
}


#endif //SENATOVA_IRINA_MATRIX_HPP
