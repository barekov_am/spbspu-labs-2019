#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <cmath>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(A2Test)

const double PRECISION = 0.001;

BOOST_AUTO_TEST_CASE(invariabilityAfterMovingRectangleByIncrements)
{
  senatova::Rectangle testRectangle({3, 6}, 2, 4);
  const senatova::rectangle_t initialBefore = testRectangle.getFrameRect();
  const double initialArea = testRectangle.getArea();

  testRectangle.move(5.8, -4);
  BOOST_CHECK_CLOSE(initialBefore.width, testRectangle.getFrameRect().width, PRECISION);
  BOOST_CHECK_CLOSE(initialBefore.height, testRectangle.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), PRECISION);
}

BOOST_AUTO_TEST_CASE(invariabilityAfterMovingRectangleToPoint)
{
  senatova::Rectangle testRectangle({3, 6}, 2, 4);
  const senatova::rectangle_t initialBefore = testRectangle.getFrameRect();
  const double initialArea = testRectangle.getArea();

  testRectangle.move(5.8, -4);
  BOOST_CHECK_CLOSE(initialBefore.width, testRectangle.getFrameRect().width, PRECISION);
  BOOST_CHECK_CLOSE(initialBefore.height, testRectangle.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), PRECISION);
}

BOOST_AUTO_TEST_CASE(quadraticRectangleScaleChange)
{
  senatova::Rectangle testRectangle({4, 5}, 7, 2);
  const double initialArea = testRectangle.getArea();
  const double multiplier = 0.5;

  testRectangle.scale(multiplier);
  BOOST_CHECK_CLOSE(testRectangle.getArea(), initialArea * multiplier * multiplier, PRECISION);
}

BOOST_AUTO_TEST_CASE(rectangleAfterRotating)
{
  senatova::Rectangle rect({0, 0}, 4, 4);
  const double rectangleAreaBeforeRotating = rect.getArea();
  const senatova::rectangle_t frameBeforeRotating = rect.getFrameRect();

  rect.rotate(45);
  double rectangleAreaAfterRotating = rect.getArea();
  senatova::rectangle_t frameAfterRotating = rect.getFrameRect();
  const double sq = sqrt(2);

  BOOST_CHECK_CLOSE(rectangleAreaBeforeRotating, rectangleAreaAfterRotating, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.x, frameAfterRotating.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.y, frameAfterRotating.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(frameAfterRotating.width, 4 * sq, PRECISION);
  BOOST_CHECK_CLOSE(frameAfterRotating.height, 4 * sq, PRECISION);
}


BOOST_AUTO_TEST_CASE(invalidRectangleParametersAndScaling)
{
  BOOST_CHECK_THROW(senatova::Rectangle({6, 7}, -3, 2), std::invalid_argument);

  senatova::Rectangle testRectangle({6, 7}, 3, 2);
  BOOST_CHECK_THROW(testRectangle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
