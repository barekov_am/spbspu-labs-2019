#include <cmath>
#include "partition.hpp"

senatova::Matrix senatova::part(const senatova::CompositeShape &compShape)
{
  Matrix matrix;
  const std::size_t size = compShape.getCount();
  for (std::size_t i = 0; i < size; i++)
  {
    std::size_t layer = 0;
    for (std::size_t j = 0; j < matrix.getRows(); j++)
    {
      bool intersect = false;
      for (std::size_t k = 0; k < matrix.getLayerSize(j); k++)
      {
        if (isIntersected(compShape[i]->getFrameRect(), matrix[j][k]->getFrameRect()))
        {
          layer++;
          intersect = true;
          break;
        }
      }
      if (!intersect)
      {
        break;
      }
    }
    matrix.add(compShape[i], layer);
  }
  return matrix;
}

bool senatova::isIntersected(const senatova::rectangle_t &shape1, const senatova::rectangle_t &shape2)
{
  const double distanceX = fabs(shape1.pos.x - shape2.pos.x);
  const double distanceY = fabs(shape1.pos.y - shape2.pos.y);

  const double lengthX = (shape1.width + shape2.width) / 2;
  const double lengthY = (shape1.height + shape2.height) / 2;

  const bool firstCondition = (distanceX < lengthX);
  const bool secondCondition = (distanceY < lengthY);

  return firstCondition&& secondCondition;
}
