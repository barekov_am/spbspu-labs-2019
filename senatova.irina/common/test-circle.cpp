#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(A2Test)

const double PRECISION = 0.001;

BOOST_AUTO_TEST_CASE(invariabilityAfterMovingCircleByIncrement)
{
  senatova::Circle testCircle({9, -5}, 3);
  const senatova::rectangle_t frameBefore = testCircle.getFrameRect();
  const double initialArea = testCircle.getArea();

  testCircle.move(7, 7);
  BOOST_CHECK_CLOSE(frameBefore.width, testCircle.getFrameRect().width, PRECISION);
  BOOST_CHECK_CLOSE(frameBefore.height, testCircle.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(initialArea, testCircle.getArea(), PRECISION);
}

BOOST_AUTO_TEST_CASE(invariabilityAfterMovingCircleToPoint)
{
  senatova::Circle testCircle({9, -5}, 3);
  const senatova::rectangle_t frameBefore = testCircle.getFrameRect();
  const double initialArea = testCircle.getArea();

  testCircle.move(-5, -9);
  BOOST_CHECK_CLOSE(frameBefore.width, testCircle.getFrameRect().width, PRECISION);
  BOOST_CHECK_CLOSE(frameBefore.height, testCircle.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(initialArea, testCircle.getArea(), PRECISION);
}

BOOST_AUTO_TEST_CASE(quadraticCircleScaleChange)
{
  senatova::Circle testCircle({13.2, 4}, 7);
  const double initialArea = testCircle.getArea();
  const double multiplier = 2.0;

  testCircle.scale(multiplier);
  BOOST_CHECK_CLOSE(initialArea * multiplier * multiplier, testCircle.getArea(), PRECISION);
}

BOOST_AUTO_TEST_CASE(circleAfterRotating)
{
  senatova::Circle circ({2, 1}, 10);
  const double circleAreaBeforeRotating = circ.getArea();
  const senatova::rectangle_t frameBeforeRotating = circ.getFrameRect();

  circ.rotate(45);
  double circleAreaAfterRotating = circ.getArea();
  senatova::rectangle_t frameAfterRotating = circ.getFrameRect();

  BOOST_CHECK_CLOSE(circleAreaBeforeRotating, circleAreaAfterRotating, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.x, frameAfterRotating.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.y, frameAfterRotating.pos.y, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeRotating.width, frameAfterRotating.width, PRECISION);
  BOOST_CHECK_CLOSE(frameBeforeRotating.height, frameAfterRotating.height, PRECISION);
}


BOOST_AUTO_TEST_CASE(invalidCircleParametersAndScaling)
{
  BOOST_CHECK_THROW(senatova::Circle testCircle({20, 11}, -4.2), std::invalid_argument);

  senatova::Circle testCircle({20, 11}, 4.2);
  BOOST_CHECK_THROW(testCircle.scale(-3.6), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
