#include <boost/test/auto_unit_test.hpp>
#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testPartition)

BOOST_AUTO_TEST_CASE(testIntersection)
{
  senatova::Circle testCircle({3, 2}, 5);
  senatova::Rectangle testRectangle({8, 6}, 4, 3);

  BOOST_CHECK(senatova::isIntersected(testCircle.getFrameRect(),testRectangle.getFrameRect()));
}

BOOST_AUTO_TEST_CASE(testCorrectnessPartition)
{
  senatova::Shape::shape_ptr circlePtr1 = std::make_shared<senatova::Circle>(senatova::point_t {3, 1}, 4);
  senatova::Shape::shape_ptr rectanglePtr1 = std::make_shared<senatova::Rectangle>(senatova::point_t {9, 4}, 4, 5);
  senatova::Shape::shape_ptr circlePtr2 = std::make_shared<senatova::Circle>(senatova::point_t {7, -3}, 3);
  senatova::Shape::shape_ptr rectanglePtr2 = std::make_shared<senatova::Rectangle>(senatova::point_t {4, -4}, 6, 4);

  senatova::CompositeShape compShape(circlePtr1);
  compShape.add(rectanglePtr1);
  compShape.add(circlePtr2);
  compShape.add(rectanglePtr2);

  const senatova::Matrix testMatrix = senatova::part(compShape);

  const size_t rows = 3;
  const size_t columns = 2;

  BOOST_CHECK_EQUAL(testMatrix.getColumns(), columns);
  BOOST_CHECK_EQUAL(testMatrix.getRows(), rows);
  BOOST_CHECK(testMatrix[0][0] == circlePtr1);
  BOOST_CHECK(testMatrix[1][0] == rectanglePtr1);
  BOOST_CHECK(testMatrix[1][1] == circlePtr2);
  BOOST_CHECK(testMatrix[2][0] == rectanglePtr2);
}

BOOST_AUTO_TEST_SUITE_END()
