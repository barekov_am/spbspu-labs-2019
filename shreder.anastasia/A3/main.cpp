#include <iostream>
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

int main()
{
  shreder::Rectangle rect1({{7, 5}, 1, 3});
  shreder::Rectangle rect2({{9, -1}, 2, 5});
  shreder::Circle circl({{11, 8}, 20});

  std::cout << "Creating of composite shape...\n";
  shreder::CompositeShape compShape(std::make_shared<shreder::Rectangle> (rect1));
  compShape.add(std::make_shared<shreder::Rectangle> (rect2));
  compShape.add(std::make_shared<shreder::Circle> (circl));
  std::cout << "Description of composite shape: \n";
  compShape.printInf();
  std::cout << "\n";
  std::cout << "Moving to the point with coordinates (35, 7)\n";
  compShape.move({35, 7});
  compShape.printInf();
  std::cout << "\n";
  std::cout << "Moving along abscissa for 4 and along ordinate for 6\n";
  compShape.move(4, 6);
  compShape.printInf();
  std::cout << "\n";
  std::cout << "Zoom scale of composite shape in two times and get new parameters\n";
  compShape.scale(2);
  compShape.printInf();
  std::cout << "\n";
  std::cout << "Deleting shape under number 2 \n";
  compShape.remove(2);
  compShape.printInf();
  std::cout << "\n";
  std::cout << "Description of shape under number 1\n";
  std::shared_ptr<shreder::Shape> selectedShape = compShape[1];
  selectedShape->printInf();
  std::cout << "\n";
  std::cout << "Ready!\n";
  std::cout << "\n";

  return 0;
}
