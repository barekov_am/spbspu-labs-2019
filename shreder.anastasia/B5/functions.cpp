#include <stdexcept>
#include <algorithm>
#include <iostream>
#include <sstream>
#include "functions.hpp"


bool isSquare(const shreder::Shape &shape)
{
  unsigned int sideLength_1 = getLength(shape[0], shape[1]);
  unsigned int sideLength_2 = getLength(shape[1], shape[2]);

  return ((sideLength_1 == sideLength_2) && (isRectangle(shape)));
}

bool isRectangle(const shreder::Shape &shape)
{
  unsigned int diagonal_1 = getLength(shape[0], shape[2]);
  unsigned int diagonal_2 = getLength(shape[1], shape[3]);

  return (diagonal_1 == diagonal_2);
}

unsigned int getLength(const shreder::Point &point_1, const shreder::Point &point_2)
{
  return (point_1.x - point_2.x) * (point_1.x - point_2.x) + (point_1.y - point_2.y) * (point_1.y - point_2.y);
}

shreder::ShapeVector readVector(std::istream &input)
{
  std::string str;
  shreder::ShapeVector vectorOfShapes;
  while (std::getline(input >> std::ws, str))
  {
    if (input.fail())
    {
      throw std::ios_base::failure("There is an error during reading the file\n");
    }

    if (str.empty())
    {
      continue;
    }

    std::stringstream string(str);
    unsigned int countOfVertexes;
    string >> countOfVertexes;

    if ((countOfVertexes < VERTEXES_TRIANGLE) || (str.empty()))
    {
      throw std::invalid_argument("Incorrect number of vertexes\n");
    }

    shreder::Shape shape;
    size_t openBracket;
    size_t closeBracket;
    size_t semicolon;

    for (size_t i = 0; i < countOfVertexes; ++i)
    {
      openBracket = str.find_first_of('(');
      closeBracket = str.find_first_of(')');
      semicolon = str.find_first_of(';');


    size_t npos = std::string::npos;

    if ((openBracket == npos) || (closeBracket == npos) || (semicolon == npos))
    {
      throw std::invalid_argument("Incorrect format of input\n");
    }

    int x = std::stoi(str.substr(openBracket + 1, semicolon - openBracket - 1));
    int y = std::stoi(str.substr(semicolon + 1, closeBracket - semicolon - 1));

      shape.push_back({x, y});
      str.erase(0, closeBracket + 1);
    }
  vectorOfShapes.push_back(shape);
  }
  return vectorOfShapes;
}

void deletePentagon(std::vector<shreder::Shape> & shapes)
{
  shapes.erase(std::remove_if(shapes.begin(), shapes.end(), [&](const shreder::Shape &shape)
  {
      return shape.size() == VERTEXES_PENTAGON;
  }), shapes.end());
}

void print(const shreder::Shape &shape)
{
  for (const auto &point: shape)
  {
    std::cout << " (" << point.x << "; " << point.y << ") ";
  }
}
