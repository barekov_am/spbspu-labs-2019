#include <set>
#include <string>
#include "tasks.hpp"

void shreder::task1()
{
  std::set<std::string> setOfWords;
  std::string word;

  while (std::cin >> word)
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("There is an error during reading the file");
    }
    setOfWords.insert(word);
  }

  if (!setOfWords.empty())
  {
    for (auto iterSet = setOfWords.begin(); iterSet != setOfWords.end(); iterSet++)
    {
      std::cout << *iterSet << '\n';
    }
  }
}
