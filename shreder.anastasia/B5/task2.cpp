#include <vector>
#include <algorithm>
#include "tasks.hpp"
#include "functions.hpp"

void shreder::task2()
{
  Shape vertexes;
  int totalNumberOfVertexes = 0;
  int totalNumberOfTriangles = 0;
  int totalNumberOfRectangles = 0;
  int totalNumberOfSquares = 0;
  ShapeVector shapesVector = readVector(std::cin);

  std::for_each(shapesVector.begin(), shapesVector.end(), [&](const Shape &shape)
  {
      totalNumberOfVertexes += shape.size();
      if (shape.size() == VERTEXES_TRIANGLE)
      {
        ++totalNumberOfTriangles;
      }

      if (shape.size() == VERTEXES_RECTANGLE)
      {
        if (isRectangle(shape))
        {
          ++totalNumberOfRectangles;
          if (isSquare(shape))
          {
            ++totalNumberOfSquares;
          }
        }
      }
  });

  deletePentagon(shapesVector);

  std::cout << "Vertices: " << totalNumberOfVertexes << '\n';
  std::cout << "Triangles: " << totalNumberOfTriangles << '\n';
  std::cout << "Squares: " << totalNumberOfSquares << '\n';
  std::cout << "Rectangles: " << totalNumberOfRectangles << '\n';


  for (ShapeVector::size_type i = 0; i != shapesVector.size(); i++)
  {
    vertexes.push_back(shapesVector[i][2]);
  }

  std::cout << "Points: ";
  print(vertexes);
  std::cout << '\n';

  std::sort(shapesVector.begin(), shapesVector.end(), [&](const Shape &firstShape, const Shape &secondShape)
  {
      if (firstShape.size() < secondShape.size())
      {
        return true;
      }
      if ((firstShape.size() == VERTEXES_RECTANGLE) && (secondShape.size() == VERTEXES_RECTANGLE))
      {
        if (isSquare(firstShape))
        {
          return !isSquare(secondShape);
        }
      }

      return false;
  });

  std::cout << "Shapes:\n";

  for (auto shape = shapesVector.begin(); shape != shapesVector.end(); ++shape)
  {
    std::cout << shape->size() << ' ';
    print(*shape);
    std::cout << '\n';
  }
}
