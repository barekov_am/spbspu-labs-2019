#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <string>
#include "shape.hpp"

const int VERTEXES_TRIANGLE = 3;
const int VERTEXES_RECTANGLE = 4;
const int VERTEXES_PENTAGON = 5;

bool isRectangle(const shreder::Shape &shape);
bool isSquare(const shreder::Shape &shape);

unsigned int getLength(const shreder::Point &, const shreder::Point &);
shreder::ShapeVector readVector(std::istream &);
void deletePentagon(shreder::ShapeVector & shapes);
void print(const shreder::Shape &);

#endif
