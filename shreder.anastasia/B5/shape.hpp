#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <vector>

namespace shreder
{
struct Point
{
  int x, y;
};

using Shape = std::vector<Point>;
using ShapeVector = std::vector<Shape>;
}

#endif
