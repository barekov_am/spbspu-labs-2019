#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <iostream>

namespace shreder
{
  class Functor
  {
  public:
    Functor();
    void operator()(long long int element);
    void printData(std::ostream & stream);
  private:
    long long int max_;
    long long int min_;
    double mean_;
    int positive_;
    int negative_;
    long long int oddSum_;
    long long int evenSum_;
    bool firstLastEqual_;
    long long int totalCount_;
    long long int firstElement_;
  };
}

#endif
