#include "functor.hpp"

shreder::Functor::Functor():
  max_(0),
  min_(0),
  mean_(0.0),
  positive_(0),
  negative_(0),
  oddSum_(0),
  evenSum_(0),
  firstLastEqual_(false),
  totalCount_(0),
  firstElement_(0)
{
}

void shreder::Functor::operator()(long long int element)
{
  totalCount_++;

  if(totalCount_ == 1)
  {
    firstElement_ = element;
    max_ = element;
    min_ = element;
  }

  if(element > max_)
  {
    max_ = element;
  }

  if(element < min_)
  {
    min_ = element;
  }

  if(element > 0)
  {
    positive_++;
  }

  if(element < 0)
  {
    negative_++;
  }

  if((element % 2) != 0)
  {
    oddSum_ += element;
  }
  else
  {
    evenSum_ += element;
  }

  firstLastEqual_ = (element == firstElement_);

}

void shreder::Functor::printData(std::ostream &string)
{
  if(totalCount_ == 0)
  {
    string << "No Data" << std::endl;
    return;
  }

  mean_ = static_cast<double>(evenSum_ + oddSum_) / totalCount_;

  string << "Max: " << max_ << std::endl;
  string << "Min: " << min_ << std::endl;
  string << "Mean: " << mean_ << std::endl;
  string << "Positive: " << positive_ << std::endl;
  string << "Negative: " << negative_ << std::endl;
  string << "Odd Sum: " << oddSum_ << std::endl;
  string << "Even Sum: " << evenSum_ << std::endl;

  string << "First/Last Equal: ";
  if(firstLastEqual_)
  {
    string << "yes" << std::endl;
  }
  else
  {
    string << "no" << std::endl;
  }
}
