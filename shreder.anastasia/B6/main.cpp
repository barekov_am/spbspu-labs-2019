#include <vector>
#include <algorithm>

#include "functor.hpp"

int main()
{
  std::vector<int> vector;

  try
  {
    int element = 0;

    while (std::cin >> element)
    {
      vector.push_back(element);
    }

    if(!(std::cin.eof()))
    {
      throw(std::invalid_argument("There is an error during input"));
    }

    shreder::Functor info = std::for_each(vector.begin(), vector.end(), shreder::Functor());

    info.printData(std::cout);

  }

  catch(std::exception &e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }

  return 0;
}
