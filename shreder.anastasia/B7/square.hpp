#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "shape.hpp"

namespace shreder
{
  class Square: public shreder::Shape
  {
    public:
      Square(double x, double y);
      void draw(std::ostream &string) override;

    private:
      double x_;
      double y_;
  };
}

#endif
