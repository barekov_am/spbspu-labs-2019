#include <iostream>
#include <algorithm>
#include "tasks.hpp"
#include "functions.hpp"

void shreder::task2()
{
  shapes_ptr vect = readShapes(std::cin);

  std::cout << "Original: " << std::endl;
  drawShapes(vect);

  std::cout << "Left-Right: " << std::endl;
  std::sort(vect.begin(), vect.end(), [](const ptr &shape1, const ptr &shape2)
  {
      return (shape1->isMoreLeft(shape2));
  });
  drawShapes(vect);

  std::cout << "Right-Left: " << std::endl;
  std::sort(vect.begin(), vect.end(), [](const ptr &shape1, const ptr &shape2)
  {
      return !(shape1->isMoreLeft(shape2));
  });
  drawShapes(vect);

  std::cout << "Top-Bottom: " << std::endl;
  std::sort(vect.begin(), vect.end(), [](const ptr &shape1, const ptr &shape2)
  {
      return (shape1->isUpper(shape2));
  });
  drawShapes(vect);

  std::cout << "Bottom-Top: " << std::endl;
  std::sort(vect.begin(), vect.end(), [](const ptr &shape1, const ptr &shape2)
  {
      return !(shape1->isUpper(shape2));
  });
  drawShapes(vect);
}
