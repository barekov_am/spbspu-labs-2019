#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace shreder
{
  class Triangle: public shreder::Shape
  {
    public:
      Triangle(double x, double y);
      void draw(std::ostream &string) override;

    private:
      double x_;
      double y_;
  };
}

#endif
