#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <cmath>
#include <iterator>
#include <vector>

#include "tasks.hpp"

void shreder::task1()
{
  std::vector<double> numbers((std::istream_iterator<double>(std::cin)), std::istream_iterator<double>());

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("There is an error during input\n");
  }

  if (numbers.empty())
  {
    return;
  }

  std::transform(numbers.begin(), numbers.end(), numbers.begin(), std::bind1st(std::multiplies<double>(), M_PI));

  for (double elem: numbers)
  {
    std::cout << elem << ' ';
  }
  std::cout << '\n';
}
