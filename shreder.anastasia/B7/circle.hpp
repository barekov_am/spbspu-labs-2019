#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace shreder
{
  class Circle: public shreder::Shape
  {
    public:
      Circle(double x, double y);
      void draw(std::ostream &string) override;

    private:
      double x_;
      double y_;
  };
}

#endif
