#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>
#include <cstddef>
#include <memory>


namespace shreder
{
  class Shape
  {
    public:
      Shape(double x, double y);
      ~Shape() = default;

      double getX();
      double getY();

      bool isUpper(const std::shared_ptr<shreder::Shape> &shape);
      bool isMoreLeft(const std::shared_ptr<shreder::Shape> &shape);
      virtual void draw(std::ostream &string) = 0;

    private:
      double x_;
      double y_;
  };
}

#endif
