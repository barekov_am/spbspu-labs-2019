#include "shape.hpp"

shreder::Shape::Shape(double x, double y):
        x_(x),
        y_(y)
{
}

double shreder::Shape::getX()
{
  return x_;
}

double shreder::Shape::getY()
{
  return y_;
}

bool shreder::Shape::isUpper(const std::shared_ptr<shreder::Shape> &shape)
{
  return (y_ > shape->getY());
}

bool shreder::Shape::isMoreLeft(const std::shared_ptr<shreder::Shape> &shape)
{
  return (x_ < shape->getX());
}
