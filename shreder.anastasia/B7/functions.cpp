#include <sstream>
#include <algorithm>
#include "functions.hpp"
#include "triangle.hpp"
#include "circle.hpp"
#include "square.hpp"

shapes_ptr readShapes(std::istream &stream)
{
  shapes_ptr vector;
  std::string str;

  while (std::getline(stream >> std::ws, str))
  {

    if (std::cin.fail())
    {
      throw std::ios_base::failure("There is an error during input\n");
    }

    if (str.empty())
    {
      continue;
    }

    std::istringstream string(str);
    std::string typeOfShape;

    size_t lastLetter = str.find_first_of('E');
    size_t openBracket = str.find_first_of('(');
    size_t closeBracket = str.find_first_of(')');
    size_t semicolon = str.find_first_of(';');

    size_t npos = std::string::npos;

    if ((openBracket == npos) || (closeBracket == npos) || (semicolon == npos)||(lastLetter == npos))
    {
      throw std::invalid_argument("Incorrect format of input\n");
    }

    typeOfShape = str.substr(0, lastLetter + 1);
    double x = std::stoi(str.substr(openBracket + 1, semicolon - openBracket - 1));
    double y = std::stoi(str.substr(semicolon + 1, closeBracket - semicolon - 1));

    if (typeOfShape == "SQUARE")
    {
      vector.push_back(std::make_shared<shreder::Square>(x, y));
    }
    else if (typeOfShape == "TRIANGLE")
    {
      vector.push_back(std::make_shared<shreder::Triangle>(x, y));
    }
    else if (typeOfShape == "CIRCLE")
    {
      vector.push_back(std::make_shared<shreder::Circle>(x, y));
    }
    else
    {
      throw std::invalid_argument("Incorrect name of shape\n");
    }

  }

  return vector;
}

void drawShapes(const shapes_ptr &vector)
{
  std::for_each(vector.begin(), vector.end(), [](const ptr &shape)
  {
      return shape->draw(std::cout);
  });
}
