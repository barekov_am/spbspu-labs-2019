#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <vector>
#include "shape.hpp"

using shapes_ptr = std::vector<std::shared_ptr<shreder::Shape>>;
using ptr = std::shared_ptr<shreder::Shape>;

shapes_ptr readShapes(std::istream &stream);
void drawShapes(const shapes_ptr &);

#endif
