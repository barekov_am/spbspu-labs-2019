#include "square.hpp"

shreder::Square::Square(double x, double y):
  Shape(x, y),
  x_(x),
  y_(y)
{
}

void shreder::Square::draw(std::ostream &string)
{
  string << "SQUARE (" << getX() << ";" << getY() << ")" << std::endl;
}
