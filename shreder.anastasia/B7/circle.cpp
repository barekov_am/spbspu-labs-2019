#include "circle.hpp"

shreder::Circle::Circle(double x, double y):
        Shape(x, y),
        x_(x),
        y_(y)
{
}

void shreder::Circle::draw(std::ostream &string)
{
  string << "CIRCLE (" << getX() << ";" << getY() << ")" << std::endl;
}
