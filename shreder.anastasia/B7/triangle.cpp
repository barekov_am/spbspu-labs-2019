#include "triangle.hpp"

shreder::Triangle::Triangle(double x, double y):
  Shape(x, y),
  x_(x),
  y_(y)
{
}

void shreder::Triangle::draw(std::ostream &string)
{
  string << "TRIANGLE(" << getX() << ";" << getY() << ")" << std::endl;
}
