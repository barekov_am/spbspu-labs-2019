#ifndef INPUT_EDITOR_HPP
#define INPUT_EDITOR_HPP

#include <vector>
#include "symbol.hpp"

namespace shreder
{
  class Editor
  {
    public:
      Editor(std::size_t);
      void read();
      void format();
      void print();

    private:
      std::size_t lineLength_;
      std::vector<component_t> text_;
      std::vector<std::string> outputText_;

      void getWord();
      void getDigit();
      void getDash();
      void getPunct();
  };
}

#endif
