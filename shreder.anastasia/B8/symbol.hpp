#ifndef B8_TEXT_HANDLER_HPP
#define B8_TEXT_HANDLER_HPP

#include <string>

namespace shreder
{
  struct component_t
  {
    enum
    {
      WORD,
      NUMBER,
      DASH,
      PUNCT
    } type;
    std::string content_;
  };
}

#endif
