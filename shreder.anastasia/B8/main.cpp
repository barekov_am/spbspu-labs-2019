#include <iostream>
#include "input-editor.hpp"

const int MIN_LINE_LENGTH = 25;
const int DEFAULT_LINE_LENGTH = 40;

int main(int argc, char *argv[])
{
  try
  {
    if ((argc == 2) || (argc > 3))
    {
      std::cerr << "Incorrect number of parameters\n";
      return 1;
    }
    int lineLength = DEFAULT_LINE_LENGTH;
    if (argc == 3)
    {
      if (std::string(argv[1]) != "--line-width")
      {
        throw std::invalid_argument("Invalid arguments\n");
      }
      lineLength = std::stoi(argv[2]);
      if (lineLength < MIN_LINE_LENGTH)
      {
        throw std::invalid_argument("Invalid length of line\n");
      }
    }
    shreder::Editor text(lineLength);
    text.read();
    text.format();
    text.print();
  }

  catch (const std::exception &err)
  {
    std::cerr << err.what();
    return 1;
  }
  return 0;
}
