#include "input-editor.hpp"

#include <iostream>
#include <algorithm>

const int MAX_DIGIT_LENGTH = 20;
const int MAX_WORD_LENGTH = 20;
const int MAX_DASHES_NUMBER = 3;

shreder::Editor::Editor(size_t lineLength) :
  lineLength_(lineLength),
  text_(),
  outputText_()
{
}

void shreder::Editor::read()
{
  while (std::cin)
  {
    char ch = (std::cin >> std::ws).get();
    if (std::isalpha(ch))
    {
      std::cin.unget();
      getWord();
    }
    else if (((ch == '-' || ch == '+') && std::isdigit(std::cin.peek())) || std::isdigit(ch))
    {
      std::cin.unget();
      getDigit();
    }
    else if (std::ispunct(ch))
    {
      if (ch == '-')
      {
        std::cin.unget();
        getDash();
      }
      else
      {
        std::cin.unget();
        getPunct();
      }
    }
  }
}

void shreder::Editor::format()
{
  size_t countOfElement = 0;
  size_t numberOfLine = 0;
  std::string line;
  while (countOfElement < text_.size())
  {
    if (line.empty())
    {
      if (text_.at(countOfElement).type == component_t::PUNCT || text_.at(countOfElement).type == component_t::DASH)
      {
        if (text_.at(countOfElement).type == component_t::PUNCT && outputText_.at(numberOfLine - 1).size() < lineLength_)
        {
          outputText_.at(numberOfLine - 1) += text_.at(countOfElement).content_;
          ++countOfElement;
        }
        else
        {
          line += text_.at(countOfElement - 1).content_;

          if (text_.at(countOfElement).type == component_t::DASH)
          {
            line += ' ';
          }

          line += text_.at(countOfElement).content_;
          std::size_t position = outputText_.at(numberOfLine - 1).find_last_of(' ');
          outputText_.at(numberOfLine - 1).erase(position);
          ++countOfElement;
        }
      }
      else
      {
        line += text_.at(countOfElement).content_;
        ++countOfElement;
      }

      continue;
    }

    if (line.size() + text_.at(countOfElement).content_.size() + 1 <= lineLength_)
    {
      if (text_.at(countOfElement).type == component_t::PUNCT)
      {
        line += text_.at(countOfElement).content_;
        ++countOfElement;
      }
      else
      {
        line += ' ' + text_.at(countOfElement).content_;
        ++countOfElement;
      }

      continue;
    }

    outputText_.push_back(line);
    line.clear();
    ++numberOfLine;
  }
  outputText_.push_back(line);
}

void shreder::Editor::print()
{
  for (const auto &string: outputText_)
  {
    std::cout << string << '\n';
  }
}

void shreder::Editor::getWord()
{
  std::string word;
  while (std::cin)
  {
    if ((std::cin.peek() == '-') || (std::isalpha(std::cin.peek())))
    {
      word += std::cin.get();
      if ((word.back() == '-') && (std::cin.peek() == '-'))
      {
        throw std::invalid_argument("Incorrect word\n");
      }
    }
    else
    {
      break;
    }
  }
  if (word.size() > MAX_WORD_LENGTH)
  {
    throw std::invalid_argument("Invalid length of the word\n");
  }
  text_.push_back({component_t::WORD, word});
}

void shreder::Editor::getDigit()
{
  std::string digit;
  bool isDot = false;
  while (std::cin)
  {
    if ((std::cin.peek() == '-') || std::cin.peek() == '+')
    {
      digit += std::cin.get();
    }
    else if (std::cin.peek() == '.' || std::isdigit(std::cin.peek()))
    {
      digit += std::cin.get();
      if (digit.back() == '.')
      {
        if (isDot)
        {
          throw std::invalid_argument("Incorrect input: too many dots\n");
        }
        isDot = true;
      }
    }
    else
    {
      break;
    }
 }
  if (digit.size() > MAX_DIGIT_LENGTH)
  {
    throw std::invalid_argument("Invalid length of the number\n");
  }
  text_.push_back({component_t::NUMBER, digit});
}

void shreder::Editor::getDash()
{
  if (text_.empty())
  {
    throw std::invalid_argument("Incorrect input: text is empty\n");
  }
  std::string dash;
  int countOfDashes = 0;
  while (std::cin)
  {
    if (std::cin.peek() == '-')
    {
      dash += std::cin.get();
      ++countOfDashes;
      if (countOfDashes > MAX_DASHES_NUMBER)
      {
        throw std::invalid_argument("Incorrect count of dashes\n");
      }
      if ((countOfDashes == 1) && (text_.back().type == component_t::PUNCT) && (text_.back().content_ != ","))
      {
        throw std::invalid_argument("Incorrect input of dash\n");
      }
    }
    else
    {
      break;
    }
  }
  text_.push_back({component_t::DASH, dash});
}

void shreder::Editor::getPunct()
{
  if (text_.empty())
  {
    throw std::invalid_argument("Incorrect input: text is empty\n");
  }
  if (text_.back().type == component_t::DASH)
  {
    throw std::invalid_argument("Invalid position of punctuation after dash\n");
  }
  if (text_.back().type == component_t::PUNCT)
  {
    throw std::invalid_argument("Invalid position of punctuation after punctuation\n");
  }
  std::string punct;
  punct += std::cin.get();
  text_.push_back({component_t::PUNCT, punct});
}
