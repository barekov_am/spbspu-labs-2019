#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "polygon.hpp"

const double accuracy = 0.01;

BOOST_AUTO_TEST_SUITE(testPolygon)

BOOST_AUTO_TEST_CASE(polygonConstAfterMoving)
{
  bondarev::point_t points_[]{ {4, 5}, {6, 4}, {7, -1}, {5, -3}, {2, 1} };

  bondarev::Polygon myPolygon(5, points_);
  const bondarev::rectangle_t firstFrame = myPolygon.getFrameRect();
  const double firstArea = myPolygon.getArea();

  myPolygon.move({ 4, 5 });
  bondarev::rectangle_t secondFrame = myPolygon.getFrameRect();
  double secondArea = myPolygon.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, accuracy);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, accuracy);
  BOOST_CHECK_CLOSE(firstArea, secondArea, accuracy);

  myPolygon.move(-2, 3);
  secondFrame = myPolygon.getFrameRect();
  secondArea = myPolygon.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, accuracy);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, accuracy);
  BOOST_CHECK_CLOSE(firstArea, secondArea, accuracy);
}

BOOST_AUTO_TEST_CASE(polygonScaling)
{
  bondarev::point_t points_[]{ {4, 5}, {6, 4}, {7, 1}, {5, 3}, {2, 1} };
  bondarev::Polygon polygon(5, points_);
  const double firstArea = polygon.getArea();

  const double coef = 2;
  polygon.scale(coef);
  double secondArea = polygon.getArea();

  BOOST_CHECK_CLOSE(firstArea * pow(coef, 2), secondArea, accuracy);
}

BOOST_AUTO_TEST_CASE(throwingExceptions)
{
  bondarev::point_t pointArray1[]{ {0, 0}, {-1, 0} };
  BOOST_CHECK_THROW(bondarev::Polygon(-2, pointArray1), std::invalid_argument);
  bondarev::point_t pointArray2[]{ { }, { } };
  BOOST_CHECK_THROW(bondarev::Polygon(2, pointArray2), std::invalid_argument);

  bondarev::point_t pointArray[]{ {4, 5}, {6, 4}, {7, -1}, {5, -3}, {2, 1} };
  bondarev::Polygon polygon_(5, pointArray);
  BOOST_CHECK_THROW(polygon_.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  bondarev::point_t points_[]{ {4, 5}, {6, 4}, {7, 1}, {5, 3}, {2, 1} };
  bondarev::Polygon myPolygon(5, points_);
  const double testArea = myPolygon.getArea();
  const bondarev::rectangle_t testFrameRect = myPolygon.getFrameRect();
  const double angle = 90;
  const double testArea2 = myPolygon.getArea();
  const bondarev::rectangle_t testFrameRect2 = myPolygon.getFrameRect();

  myPolygon.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testFrameRect2.width, accuracy);
  BOOST_CHECK_CLOSE(testFrameRect.height, testFrameRect2.height, accuracy);
  BOOST_CHECK_CLOSE(testArea, testArea2, accuracy);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testFrameRect2.pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testFrameRect2.pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
