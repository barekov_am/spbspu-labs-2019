#ifndef MATRIX_HPP
#include "shape.hpp"
#include <memory>

namespace bondarev
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &);
    Matrix(Matrix &&);
    ~Matrix() = default;

    Matrix &operator =(const Matrix &);
    Matrix &operator =(Matrix &&);
    shapes_array operator [](size_t) const;
    bool operator ==(const Matrix &) const;
    bool operator !=(const Matrix &) const;
    void add(shape_ptr, size_t, size_t);
    size_t getLines() const;
    size_t getColumns() const;

  private:
    size_t lines_;
    size_t columns_;
    shapes_array figures_;
  };
};
#endif // !MATRIX_HPP
