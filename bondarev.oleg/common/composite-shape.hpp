#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include <boost/mpl/size_t.hpp>
#include <memory>
#include "base-types.hpp"
#include "shape.hpp"

namespace bondarev
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &copiedCompositeShape);
    CompositeShape(CompositeShape &&movedCompositeShape);
    CompositeShape(const shape_ptr &shape);
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape &);
    CompositeShape& operator =(CompositeShape &&);
    shape_ptr operator [](size_t) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &newPos) override;
    void move(double dx, double dy) override;
    void scale(double coef) override;
    int getSize() const;
    void addShape(shape_ptr &);
    void deleteShape(size_t);
    shapes_array getFigures() const;
    void rotate(double) override;

  private:
    size_t size_;
    shapes_array shapes_;
  };
}

#endif // !COMPOSITE_SHAPE_HPP
