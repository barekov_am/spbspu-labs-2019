#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

const double accuracy = 0.01;

BOOST_AUTO_TEST_SUITE(compositeTests)

BOOST_AUTO_TEST_CASE(constAfterMovingToPoint)
{
  bondarev::shape_ptr rectPtr = std::make_shared<bondarev::Rectangle>(bondarev::point_t{ 1, 2 }, 3, 4);
  bondarev::shape_ptr circPtr = std::make_shared<bondarev::Circle>(bondarev::point_t{ 5, 6 }, 7);
  bondarev::CompositeShape composite(rectPtr);
  composite.addShape(circPtr);

  const double areaBefore = composite.getArea();
  const bondarev::rectangle_t frameRectBefore = composite.getFrameRect();
  const double newPointX = 8;
  const double newPointY = 9;

  composite.move({ newPointX, newPointY });
  const double areaAfter = composite.getArea();
  const bondarev::rectangle_t frameRectAfter = composite.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, accuracy);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, accuracy);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, accuracy);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, newPointX, accuracy);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, newPointY, accuracy);
}

BOOST_AUTO_TEST_CASE(constAfterMovingDxDy)
{
  bondarev::shape_ptr rectPtr = std::make_shared<bondarev::Rectangle>(bondarev::point_t{ 1, 2 }, 3, 4);
  bondarev::shape_ptr circPtr = std::make_shared<bondarev::Circle>(bondarev::point_t{ 5, 6 }, 7);
  bondarev::CompositeShape composite(rectPtr);
  composite.addShape(circPtr);

  const double areaBefore = composite.getArea();
  const bondarev::rectangle_t frameRectBefore = composite.getFrameRect();
  const double dx = 8;
  const double dy = 9;

  composite.move(dx, dy);
  const double areaAfter = composite.getArea();
  const bondarev::rectangle_t frameRectAfter = composite.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, accuracy);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, accuracy);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, accuracy);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x + dx, frameRectAfter.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y + dy, frameRectAfter.pos.y, accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShapeConstParameters)
{
  bondarev::shape_ptr rectPtr = std::make_shared<bondarev::Rectangle>(bondarev::point_t{ 1, 2 }, 3, 4);
  bondarev::shape_ptr circPtr = std::make_shared<bondarev::Circle>(bondarev::point_t{ 5, 6 }, 7);
  bondarev::CompositeShape composite(rectPtr);
  composite.addShape(circPtr);
  const double areaBeforeMoving = composite.getArea();
  const bondarev::rectangle_t frameRectBeforeMoving = composite.getFrameRect();
  composite.move(3, 5);
  bondarev::rectangle_t frameRectAfterMoving = composite.getFrameRect();
  double areaAfterMoving = composite.getArea();

  BOOST_CHECK_CLOSE(areaAfterMoving, areaBeforeMoving, accuracy);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.height, frameRectAfterMoving.height, accuracy);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.width, frameRectAfterMoving.width, accuracy);

  composite.move({ 3, 4 });
  frameRectAfterMoving = composite.getFrameRect();
  areaAfterMoving = composite.getArea();

  BOOST_CHECK_CLOSE(areaAfterMoving, areaBeforeMoving, accuracy);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.height, frameRectAfterMoving.height, accuracy);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.width, frameRectAfterMoving.width, accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleCoefficientMoreThanOne)
{
  bondarev::shape_ptr rectPtr = std::make_shared<bondarev::Rectangle>(bondarev::point_t{ 1, 2 }, 3, 4);
  bondarev::shape_ptr circPtr = std::make_shared<bondarev::Circle>(bondarev::point_t{ 5, 6 }, 7);
  bondarev::CompositeShape composite(rectPtr);
  composite.addShape(circPtr);
  const bondarev::rectangle_t frameBeforeScale = composite.getFrameRect();
  const int coefMoreThanOne = 3;
  composite.scale(coefMoreThanOne);
  bondarev::rectangle_t frameAfterScale = composite.getFrameRect();

  BOOST_CHECK_CLOSE(frameBeforeScale.height * coefMoreThanOne, frameAfterScale.height, accuracy);
  BOOST_CHECK_CLOSE(frameBeforeScale.width * coefMoreThanOne, frameAfterScale.width, accuracy);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.x, frameAfterScale.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.y, frameAfterScale.pos.y, accuracy);
  BOOST_CHECK(frameBeforeScale.height < frameAfterScale.height);
  BOOST_CHECK(frameBeforeScale.width < frameAfterScale.width);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleCoefficientLessThanOne)
{
  bondarev::shape_ptr rectPtr = std::make_shared<bondarev::Rectangle>(bondarev::point_t{ 1, 2 }, 3, 4);
  bondarev::shape_ptr circPtr = std::make_shared<bondarev::Circle>(bondarev::point_t{ 5, 6 }, 7);
  bondarev::CompositeShape composite(rectPtr);
  composite.addShape(circPtr);
  const bondarev::rectangle_t frameBeforeScale = composite.getFrameRect();
  const double coefLessThanOne = 0.3;
  composite.scale(coefLessThanOne);
  bondarev::rectangle_t frameAfterScale = composite.getFrameRect();

  BOOST_CHECK_CLOSE(frameBeforeScale.height * coefLessThanOne, frameAfterScale.height, accuracy);
  BOOST_CHECK_CLOSE(frameBeforeScale.width * coefLessThanOne, frameAfterScale.width, accuracy);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.x, frameAfterScale.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.y, frameAfterScale.pos.y, accuracy);
  BOOST_CHECK(frameBeforeScale.width > frameAfterScale.width);
  BOOST_CHECK(frameBeforeScale.height > frameAfterScale.height);
}

BOOST_AUTO_TEST_CASE(compositeShapeIncorrectScaleParameter)
{
  bondarev::shape_ptr rectPtr = std::make_shared<bondarev::Rectangle>(bondarev::point_t{ 1, 2 }, 3, 4);
  bondarev::shape_ptr circPtr = std::make_shared<bondarev::Circle>(bondarev::point_t{ 5, 6 }, 7);
  bondarev::CompositeShape composite(rectPtr);
  composite.addShape(circPtr);

  BOOST_CHECK_THROW(composite.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShapeAreaAfterAddAndDelete)
{
  bondarev::Rectangle rect({ 1, 1 }, 3, 4);
  bondarev::shape_ptr rectPtr = std::make_shared<bondarev::Rectangle>(rect);
  bondarev::Circle circ({ 5, 6 }, 7);
  bondarev::shape_ptr circPtr = std::make_shared<bondarev::Circle>(circ);
  bondarev::CompositeShape composite(rectPtr);
  const double compositeAreaBeforeAdd = composite.getArea();
  const double rectArea = rect.getArea();
  const double circArea = circ.getArea();

  composite.addShape(circPtr);
  double compositeAreaAfterAdd = composite.getArea();
  BOOST_CHECK_CLOSE(compositeAreaBeforeAdd + circArea, compositeAreaAfterAdd, accuracy);

  composite.deleteShape(0);
  BOOST_CHECK_CLOSE(compositeAreaAfterAdd - rectArea, composite.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShapeCopyConstructor)
{
  bondarev::shape_ptr rectPtr = std::make_shared<bondarev::Rectangle>(bondarev::point_t{ 1, 2 }, 3, 4);
  bondarev::shape_ptr circPtr = std::make_shared<bondarev::Circle>(bondarev::point_t{ 5, 6 }, 7);
  bondarev::CompositeShape composite(rectPtr);
  composite.addShape(circPtr);
  const bondarev::rectangle_t frameRect = composite.getFrameRect();

  bondarev::CompositeShape copyComposite(composite);
  const bondarev::rectangle_t copyFrameRect = copyComposite.getFrameRect();
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, accuracy);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, accuracy);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, accuracy);
  BOOST_CHECK_CLOSE(composite.getArea(), copyComposite.getArea(), accuracy);
  BOOST_CHECK_EQUAL(composite.getSize(), copyComposite.getSize());
}

BOOST_AUTO_TEST_CASE(compositeShapeCopyOperator)
{
  bondarev::shape_ptr rectPtr = std::make_shared<bondarev::Rectangle>(bondarev::point_t{ 1, 2 }, 3, 4);
  bondarev::shape_ptr circPtr = std::make_shared<bondarev::Circle>(bondarev::point_t{ 5, 6 }, 7);
  bondarev::CompositeShape composite(rectPtr);
  composite.addShape(circPtr);
  const bondarev::rectangle_t frameRect = composite.getFrameRect();

  bondarev::CompositeShape copyComposite(rectPtr);
  copyComposite = composite;
  const bondarev::rectangle_t copyFrameRect = copyComposite.getFrameRect();
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, accuracy);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, accuracy);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, accuracy);
  BOOST_CHECK_CLOSE(composite.getArea(), copyComposite.getArea(), accuracy);
  BOOST_CHECK_EQUAL(composite.getSize(), copyComposite.getSize());
}

BOOST_AUTO_TEST_CASE(compositeShapeMoveConstructor)
{
  bondarev::shape_ptr rectPtr = std::make_shared<bondarev::Rectangle>(bondarev::point_t{ 1, 2 }, 3, 4);
  bondarev::shape_ptr circPtr = std::make_shared<bondarev::Circle>(bondarev::point_t{ 5, 6 }, 7);
  bondarev::CompositeShape composite(rectPtr);
  composite.addShape(circPtr);
  const bondarev::rectangle_t frameRect = composite.getFrameRect();
  const double compositeArea = composite.getArea();
  const int compositeCount = composite.getSize();

  bondarev::CompositeShape movecomposite(std::move(composite));
  const bondarev::rectangle_t moveFrameRect = movecomposite.getFrameRect();

  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, accuracy);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, accuracy);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, accuracy);
  BOOST_CHECK_CLOSE(compositeArea, movecomposite.getArea(), accuracy);
  BOOST_CHECK_EQUAL(compositeCount, movecomposite.getSize());
  BOOST_CHECK_CLOSE(composite.getArea(), 0, accuracy);
  BOOST_CHECK_EQUAL(composite.getSize(), 0);
}

BOOST_AUTO_TEST_CASE(compositeShapeMoveOperator)
{
  bondarev::shape_ptr rectPtr = std::make_shared<bondarev::Rectangle>(bondarev::point_t{ 1, 2 }, 3, 4);
  bondarev::shape_ptr circPtr = std::make_shared<bondarev::Circle>(bondarev::point_t{ 5, 6 }, 7);
  bondarev::CompositeShape movedcomposite(rectPtr);
  movedcomposite.addShape(circPtr);
  const bondarev::rectangle_t frameRect = movedcomposite.getFrameRect();
  const double compositeArea = movedcomposite.getArea();
  const int compositeCount = movedcomposite.getSize();

  bondarev::CompositeShape composite(rectPtr);
  composite = std::move(movedcomposite);
  const bondarev::rectangle_t moveFrameRect = composite.getFrameRect();
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, accuracy);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, accuracy);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, accuracy);
  BOOST_CHECK_CLOSE(compositeArea, composite.getArea(), accuracy);
  BOOST_CHECK_EQUAL(compositeCount, composite.getSize());
  BOOST_CHECK_EQUAL(movedcomposite.getSize(), 0);
  BOOST_CHECK_CLOSE(movedcomposite.getArea(), 0, accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShapeAfterRotating)
{
  bondarev::shape_ptr rectPtr = std::make_shared<bondarev::Rectangle>(bondarev::point_t{ -1, 0 }, 2, 2);
  bondarev::shape_ptr circPtr = std::make_shared<bondarev::Circle>(bondarev::point_t{ 1, 0 }, 1);
  bondarev::CompositeShape composite(rectPtr);
  composite.addShape(circPtr);

  const double compositeAreaBeforeRotating = composite.getArea();
  const bondarev::rectangle_t frameBeforeRotating = composite.getFrameRect();

  composite.rotate(90);
  double compositeAreaAfterRotating = composite.getArea();
  bondarev::rectangle_t frameAfterRotating = composite.getFrameRect();

  BOOST_CHECK_CLOSE(compositeAreaBeforeRotating, compositeAreaAfterRotating, accuracy);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.x, frameAfterRotating.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.y, frameAfterRotating.pos.y, accuracy);
  BOOST_CHECK_CLOSE(frameBeforeRotating.width, frameAfterRotating.height, accuracy);
  BOOST_CHECK_CLOSE(frameBeforeRotating.height, frameAfterRotating.width, accuracy);

  composite.rotate(-90);
  compositeAreaAfterRotating = composite.getArea();
  frameAfterRotating = composite.getFrameRect();

  BOOST_CHECK_CLOSE(compositeAreaBeforeRotating, compositeAreaAfterRotating, accuracy);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.x, frameAfterRotating.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.y, frameAfterRotating.pos.y, accuracy);
  BOOST_CHECK_CLOSE(frameBeforeRotating.width, frameAfterRotating.width, accuracy);
  BOOST_CHECK_CLOSE(frameBeforeRotating.height, frameAfterRotating.height, accuracy);

  composite.rotate(45);
  compositeAreaAfterRotating = composite.getArea();

  BOOST_CHECK_CLOSE(compositeAreaBeforeRotating, compositeAreaAfterRotating, accuracy);
  BOOST_CHECK_CLOSE(composite.getFigures()[0]->getFrameRect().pos.x, composite.getFigures()[0]->getFrameRect().pos.y, accuracy);
  BOOST_CHECK_CLOSE(composite.getFigures()[1]->getFrameRect().pos.x, composite.getFigures()[1]->getFrameRect().pos.y, accuracy);
  BOOST_CHECK_CLOSE(composite.getFigures()[0]->getFrameRect().pos.x, -composite.getFigures()[1]->getFrameRect().pos.x, accuracy);
  BOOST_CHECK_CLOSE(composite.getFigures()[0]->getFrameRect().pos.y, -composite.getFigures()[1]->getFrameRect().pos.y, accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShapeThrowingExeptions)
{
  bondarev::shape_ptr rectPtr = std::make_shared<bondarev::Rectangle>(bondarev::point_t{ 1, 1 }, 3, 4);
  bondarev::shape_ptr circPtr = std::make_shared<bondarev::Circle>(bondarev::point_t{ 5, 6 }, 7);
  bondarev::CompositeShape composite(rectPtr);
  composite.addShape(circPtr);

  BOOST_CHECK_THROW(composite.deleteShape(10), std::invalid_argument);
  BOOST_CHECK_THROW(composite.deleteShape(-10), std::invalid_argument);

  composite.deleteShape(1);
  BOOST_CHECK_THROW(composite[1], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeTestThrowExceptionAfterUsingOfOperator)
{
  bondarev::shape_ptr rectPtr = std::make_shared<bondarev::Rectangle>(bondarev::point_t{ 1, 1 }, 3, 4);
  bondarev::shape_ptr circPtr = std::make_shared<bondarev::Circle>(bondarev::point_t{ 5, 6 }, 7);
  bondarev::CompositeShape composite(rectPtr);
  composite.addShape(circPtr);

  BOOST_CHECK_THROW(composite[2], std::out_of_range);
  BOOST_CHECK_THROW(composite[-2], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(emptyCompositeShapeThrowingExeptions)
{
  bondarev::CompositeShape emptycomposite;

  BOOST_CHECK_THROW(emptycomposite.move(3, 5), std::logic_error);
  BOOST_CHECK_THROW(emptycomposite.move({ 1, 1 }), std::logic_error);
  BOOST_CHECK_THROW(emptycomposite.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(emptycomposite.scale(2), std::logic_error);
}

BOOST_AUTO_TEST_SUITE_END()
