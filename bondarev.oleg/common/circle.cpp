#include "circle.hpp"
#include <stdexcept>
#define _USE_MATH_DEFINES
#include <math.h>

bondarev::Circle::Circle(const point_t &pos, double radius) :
  center_(pos),
  radius_(radius)
{
  if (radius_ <= 0.0)
  {
    throw std::invalid_argument("Invalid radius");
  }
}

double bondarev::Circle::getArea() const
{
  return (M_PI * radius_ * radius_);
}

bondarev::rectangle_t bondarev::Circle::getFrameRect() const
{
  return { center_, radius_ * 2, radius_ * 2 };
}

double bondarev::Circle::getRadius() const
{
  return radius_;
}

void bondarev::Circle::move(const point_t &newPos)
{
  center_ = newPos;
}

void bondarev::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void bondarev::Circle::scale(double coef)
{
  if (coef <= 0.0)
  {
    throw std::invalid_argument("Invalid  coefficient");
  }
  else
  {
    radius_ *= coef;
  }
}

void bondarev::Circle::rotate(double)
{
}
