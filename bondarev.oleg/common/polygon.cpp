#include "polygon.hpp"
#include <stdexcept>
#include <algorithm>
#define _USE_MATH_DEFINES
#include <math.h>

bondarev::Polygon::Polygon(const int dimension, const point_t * points)
{
  if (dimension <= 2)
  {
    throw std::invalid_argument("number of polygon's vertices can't be <=2");
  }
  if (points == nullptr)
  {
    throw std::invalid_argument("pointer to vertex is null");
  }
  dim_ = dimension;
  points_ = new point_t[dimension];
  double sumx = 0, sumy = 0;
  for (int i = 0; i < dimension; i++)
  {
    points_[i] = points[i];
    sumx += points_[i].x;
    sumy += points_[i].y;
  }
  center_ = { sumx / dimension, sumy / dimension };
}

bondarev::Polygon::Polygon(const Polygon& anotherOne) :
  dim_(anotherOne.dim_),
  points_(new point_t[dim_])
{
  for (int i = 0; i < dim_; i++)
  {
    points_[i] = anotherOne.points_[i];
  }
}

bondarev::Polygon::Polygon(Polygon&& anotherOne) noexcept :
  dim_(0),
  points_(nullptr)
{
  swap(anotherOne);
}

bondarev::Polygon::~Polygon()
{
  delete[] points_;
}

bondarev::Polygon & bondarev::Polygon::operator= (const bondarev::Polygon & anotherOne)
{
  if (&anotherOne != this)
  {
    Polygon temp(anotherOne);
    swap(temp);
  }
  return *this;
}

bondarev::Polygon & bondarev::Polygon::operator= (bondarev::Polygon && anotherOne)
{
  if (&anotherOne != this)
  {
    swap(anotherOne);
    delete[] anotherOne.points_;
    anotherOne.dim_ = 0;
    anotherOne.points_ = nullptr;
  }
  return *this;
}

double bondarev::Polygon::getArea() const
{
  if ((points_ == nullptr) || (dim_ <= 2))
  {
    return 0;
  }
  double sum_pos = 0, sum_neg = 0;
  for (int i = 0; i < dim_ - 1; i++)
  {
    sum_pos += points_[i].x * points_[i + 1].y;
    sum_neg += points_[i + 1].x * points_[i].y;
  }
  double area = sum_pos + points_[dim_ - 1].x * points_[0].y
    - sum_neg - points_[0].x * points_[dim_ - 1].y;
  return (0.5 * std::fabs(area));
}

bondarev::rectangle_t bondarev::Polygon::getFrameRect() const
{
  double minX = points_[0].x;
  double maxX = points_[0].x;
  double minY = points_[0].y;
  double maxY = points_[0].y;

  for (int i = 0; i < dim_; i++)
  {
    maxX = std::max(maxX, points_[i].x);
    minX = std::min(minX, points_[i].x);
    maxY = std::max(maxY, points_[i].y);
    minY = std::min(minY, points_[i].y);
  }

  double width = abs(maxX - minX);
  double height = abs(maxY - minY);

  point_t center = { (maxX - minX) / 2 + minX, (maxY - minY) / 2 + minY };

  return { center, height, width};
}

void bondarev::Polygon::move(const point_t &point_n)
{
  double shiftX = point_n.x - center_.x;
  double shiftY = point_n.y - center_.y;

  for (int i = 0; i < dim_; i++)
  {
    points_[i].x += shiftX;
    points_[i].y += shiftY;
  }

  center_ = { point_n.x, point_n.y };
}

void bondarev::Polygon::move(const double shiftX, const double shiftY)
{
  center_.x += shiftX;
  center_.y += shiftY;

  for (int i = 0; i < dim_; i++)
  {
    points_[i].x += shiftX;
    points_[i].y += shiftY;
  }
}

void bondarev::Polygon::swap(Polygon& anotherOne) noexcept
{
  std::swap(dim_, anotherOne.dim_);
  std::swap(points_, anotherOne.points_);
}

void bondarev::Polygon::scale(const double coef)
{
  if (coef <= 0.0)
  {
    throw std::invalid_argument("Invalid scale");
  }

  for (int i = 0; i < dim_; i++)
  {
    points_[i].x *= coef;
    points_[i].y *= coef;
  }
}

void bondarev::Polygon::rotate(double angle)
{
  angle_ = angle;

  if (angle_ < 0.0)
  {
    angle_ = 360.0 + fmod(angle_, 360.0);
  }
  else
  {
    angle_ = fmod(angle_, 360.0);
  }


  const double radAngle = angle_ * M_PI / 180;
  for (int i = 0; i < dim_; i++)
  {
    rotatePoint(points_[i], radAngle);
  }
}

void bondarev::Polygon::rotatePoint(point_t &point, const double radAngle)
{
  const double oldX = point.x - center_.x;
  const double oldY = point.y - center_.y;

  const double newX = oldX * fabs(cos(radAngle)) - oldY * fabs(sin(radAngle));
  const double newY = oldX * fabs(sin(radAngle)) + oldY * fabs(cos(radAngle));

  point = { center_.x + newX, center_.y + newY };
}
