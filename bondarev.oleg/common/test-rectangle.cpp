#include <cmath>
#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double accuracy = 0.01;

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(constAfterMovingToPoint)
{
  bondarev::Rectangle rectangle({ 1, 2 }, 3, 4);
  const double widthBefore = rectangle.getFrameRect().width;
  const double heightBefore = rectangle.getFrameRect().height;
  const double areaBefore = rectangle.getArea();

  rectangle.move({ 10, 11 });

  BOOST_CHECK_CLOSE(widthBefore, rectangle.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(heightBefore, rectangle.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(constAfterMovingDxDy)
{
  bondarev::Rectangle rectangle({ 1, 2 }, 3, 4);
  const double widthBefore = rectangle.getFrameRect().width;
  const double heightBefore = rectangle.getFrameRect().height;
  const double areaBefore = rectangle.getArea();

  rectangle.move(10, 11);

  BOOST_CHECK_CLOSE(widthBefore, rectangle.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(heightBefore, rectangle.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(constAfterScaling)
{
  bondarev::Rectangle rectangle({ 1, 2 }, 3, 4);
  const double areaBefore = rectangle.getArea();
  const double heightBefore = rectangle.getFrameRect().height;
  const double widthBefore = rectangle.getFrameRect().width;
  const double coef = 3;

  rectangle.scale(coef);

  BOOST_CHECK_CLOSE(areaBefore * coef * coef, rectangle.getArea(), accuracy);
  BOOST_CHECK_CLOSE(heightBefore * coef, rectangle.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(widthBefore * coef, rectangle.getFrameRect().width, accuracy);
}

BOOST_AUTO_TEST_CASE(invalidWidth)
{
  BOOST_CHECK_THROW(bondarev::Rectangle rectangle({ 4, -3 }, -2, 1), std::invalid_argument);
  BOOST_CHECK_THROW(bondarev::Rectangle rectangle({ 4, -3 }, 2, -1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidHeight)
{
  BOOST_CHECK_THROW(bondarev::Rectangle rectangle({ 4, -3 }, -2, 1), std::invalid_argument);
  BOOST_CHECK_THROW(bondarev::Rectangle rectangle({ 4, -3 }, 2, -1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidCoefficientOfScaling)
{
  bondarev::Rectangle rectangle({ 1, 2 }, 3, 4);
  BOOST_CHECK_THROW(rectangle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(rectangle.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  bondarev::Rectangle rectangleTest({ 1, 2 }, 3, 4);
  const double testArea = rectangleTest.getArea();
  const bondarev::rectangle_t testFrameRect = rectangleTest.getFrameRect();
  const double angle = 90;

  rectangleTest.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, rectangleTest.getFrameRect().width - 1, accuracy);
  BOOST_CHECK_CLOSE(testFrameRect.height, rectangleTest.getFrameRect().height + 1, accuracy);
  BOOST_CHECK_CLOSE(testArea, rectangleTest.getArea(), accuracy);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, rectangleTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, rectangleTest.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
