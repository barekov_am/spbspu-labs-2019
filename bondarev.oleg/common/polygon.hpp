#ifndef POLYGON_HPP
#define POLYGON_HPP
#include "shape.hpp"

namespace bondarev
{
  class Polygon : public Shape
  {
  public:
    Polygon(const int dimension, const point_t *);

    Polygon(const Polygon &);
    Polygon(Polygon &&) noexcept;
    ~Polygon() override;

    Polygon & operator= (const Polygon &);
    Polygon & operator= (Polygon &&);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(const double x, const double y) override;
    void scale(const double coef) override;
    void rotate(const double) override;
    void rotatePoint(point_t &, const double);


  private:
    int dim_;
    point_t * points_;
    point_t center_;
    double angle_;

    void swap(Polygon &) noexcept;
  };
}

#endif // !POLYGON_HPP
