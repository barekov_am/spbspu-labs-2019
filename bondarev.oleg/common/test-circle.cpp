#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

const double accuracy = 0.01;

BOOST_AUTO_TEST_SUITE(circleTest)

BOOST_AUTO_TEST_CASE(constAfterMovingToPoint)
{
  bondarev::Circle circle({ 5, 6 }, 7);
  const double radiusBefore = circle.getRadius();
  const double areaBefore = circle.getArea();

  circle.move({ 8, 9 });

  BOOST_CHECK_CLOSE(radiusBefore, circle.getRadius(), accuracy);
  BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(constAfterMovingDxDy)
{
  bondarev::Circle circle({ 5, 6 }, 7);
  const double radiusBefore = circle.getRadius();
  const double areaBefore = circle.getArea();

  circle.move({ 8, 9 });

  BOOST_CHECK_CLOSE(radiusBefore, circle.getRadius(), accuracy);
  BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(constAfterScaling)
{
  bondarev::Circle circle({ 5, 6 }, 7);
  const double areaBefore = circle.getArea();
  const double radiusBefore = circle.getRadius();
  const double coef = 2;

  circle.scale(coef);

  BOOST_CHECK_CLOSE(areaBefore * coef * coef, circle.getArea(), accuracy);
  BOOST_CHECK_CLOSE(radiusBefore * coef, circle.getRadius(), accuracy);
}

BOOST_AUTO_TEST_CASE(invalidRadius)
{
  BOOST_CHECK_THROW(bondarev::Circle circle({ 5, 6 }, -7), std::invalid_argument);
  BOOST_CHECK_THROW(bondarev::Circle circle({ 5, 6 }, 0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidCoefficientOfScaling)
{
  bondarev::Circle circle({ 5, 6 }, 7);
  BOOST_CHECK_THROW(circle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(circle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  bondarev::Circle circleTest({ 6, 3 }, 4);
  const double testArea = circleTest.getArea();
  const bondarev::rectangle_t testFrameRect = circleTest.getFrameRect();
  const double angle = 90;

  circleTest.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, circleTest.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(testFrameRect.height, circleTest.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(testArea, circleTest.getArea(), accuracy);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, circleTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, circleTest.getFrameRect().pos.y);
}


BOOST_AUTO_TEST_SUITE_END()
