#include "triangle.hpp"
#include <stdexcept>
#include <algorithm>
#define _USE_MATH_DEFINES
#include <math.h>

bondarev::Triangle::Triangle(const point_t &point_a, const point_t &point_b, const point_t &point_c) :
  a_(point_a),
  b_(point_b),
  c_(point_c),
  center_({ (point_a.x + point_b.x + point_c.x) / 3 , (point_a.y + point_b.y + point_c.y) / 3 })
{
  if (this->getArea() == 0.0)
  {
    throw std::invalid_argument("vertices of triangle can't lie on the same straight line or match");
  }
}

double bondarev::Triangle::getArea() const
{
  double p, ab, ac, bc;

  ab = sqrt((a_.x - b_.x) * (a_.x - b_.x) + (a_.y - b_.y) * (a_.y - b_.y));
  ac = sqrt((a_.x - c_.x) * (a_.x - c_.x) + (a_.y - c_.y) * (a_.y - c_.y));
  bc = sqrt((b_.x - c_.x) * (b_.x - c_.x) + (b_.y - c_.y) * (b_.y - c_.y));
  p = (ab + ac + bc) / 2;

  return (sqrt(p * (p - ab) * (p - ac) * (p - bc)));
}

bondarev::rectangle_t bondarev::Triangle::getFrameRect() const
{
  double min_y = std::min({ a_.y, b_.y, c_.y });
  double max_y = std::max({ a_.y, b_.y, c_.y });

  double height_ = abs(max_y - min_y);

  double min_x = std::min({ a_.x, b_.x, c_.x });
  double max_x = std::max({ a_.x, b_.x, c_.x });

  double width_ = abs(max_x - min_x);

  point_t center = { (max_x - min_x) / 2 + min_x, (max_y - min_y) / 2 + min_y };

  return { center, height_, width_ };
}

void bondarev::Triangle::move(const point_t &point_n)
{
  double shift_x, shift_y;

  shift_x = point_n.x - center_.x;
  shift_y = point_n.y - center_.y;

  a_.x += shift_x;
  a_.y += shift_y;

  b_.x += shift_x;
  b_.y += shift_y;

  c_.x += shift_x;
  c_.y += shift_y;

  center_ = { point_n.x, point_n.y };
}

void bondarev::Triangle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;

  a_.x += dx;
  a_.y += dy;

  b_.x += dx;
  b_.y += dy;

  c_.x += dx;
  c_.y += dy;
}

void bondarev::Triangle::scale(const double coef)
{
  if (coef <= 0.0)
  {
    throw std::invalid_argument("Invalid scale");
  }

  a_.x *= coef;
  a_.y *= coef;
  b_.x *= coef;
  b_.y *= coef;
  c_.x *= coef;
  c_.y *= coef;
}

void bondarev::Triangle::rotate(double angle)
{
  const double Ax = a_.x - center_.x;
  const double Ay = a_.y - center_.y;
  const double Bx = b_.x - center_.x;
  const double By = b_.y - center_.y;
  const double Cx = c_.x - center_.x;
  const double Cy = c_.y - center_.y;
  const double sinA = sin(angle * M_PI / 180);
  const double cosA = cos(angle * M_PI / 180);

  a_.x = Ax * cosA - Ay * sinA + center_.x;
  a_.y = Ax * sinA + Ay * cosA + center_.y;
  b_.x = Bx * cosA - By * sinA + center_.x;
  b_.y = Bx * sinA + By * cosA + center_.y;
  c_.x = Cx * cosA - Cy * sinA + center_.x;
  c_.y = Cx * sinA + Cy * cosA + center_.y;
}
