#include "rectangle.hpp"
#include <stdexcept>
#define _USE_MATH_DEFINES
#include <math.h>

bondarev::Rectangle::Rectangle(const point_t &pos, double height, double width) :
  center_(pos),
  height_(height),
  width_(width),
  angle_(0)
{
  if ((height_ <= 0.0) || (width_ <= 0.0))
  {
    throw std::invalid_argument("Invalid radius");
  };
}

double bondarev::Rectangle::getArea() const
{
  return (height_ * width_);
}

bondarev::rectangle_t bondarev::Rectangle::getFrameRect() const
{
  double frameWidth = height_ * std::abs(std::sin(angle_ * M_PI / 180))
      + width_ * std::abs(std::cos(angle_ * M_PI / 180));
  double frameHeight = height_ * std::abs(std::cos(angle_ * M_PI / 180))
      + width_ * std::abs(std::sin(angle_ * M_PI / 180));

  return { center_, frameWidth, frameHeight };
}

void bondarev::Rectangle::move(const point_t &newPos)
{
  center_ = newPos;
}

void bondarev::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void bondarev::Rectangle::scale(double coef)
{
  if (coef <= 0.0)
  {
    throw std::invalid_argument("Invalid  coefficient");
  }
  height_ *= coef;
  width_ *= coef;

}

void bondarev::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
