#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

namespace bondarev
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &, const point_t &, const point_t &);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(const double dx, const double dy) override;
    void scale(const double coef) override;
    void rotate(double angle) override;

  private:
    point_t a_, b_, c_, center_;
  };
}

#endif // !TRIANGLE_HPP
