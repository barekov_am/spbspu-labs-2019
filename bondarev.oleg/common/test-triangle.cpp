#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "triangle.hpp"

const double accuracy = 0.01;

BOOST_AUTO_TEST_SUITE(testTriangle)

BOOST_AUTO_TEST_CASE(triangleConstAfterMoving)
{
  bondarev::Triangle triangle({ 4, 3 }, { 5, 4 }, { 2, 6 });
  const bondarev::rectangle_t firstFrame = triangle.getFrameRect();
  const double firstArea = triangle.getArea();

  triangle.move({ 10, 3 });
  bondarev::rectangle_t secondFrame = triangle.getFrameRect();
  double secondArea = triangle.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, accuracy);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, accuracy);
  BOOST_CHECK_CLOSE(firstArea, secondArea, accuracy);

  triangle.move(10, 3);
  secondFrame = triangle.getFrameRect();
  secondArea = triangle.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, accuracy);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, accuracy);
  BOOST_CHECK_CLOSE(firstArea, secondArea, accuracy);
}

BOOST_AUTO_TEST_CASE(triangleScaling)
{
  bondarev::Triangle rectangle({ 1, -1 }, { 2, 2 }, { 3, -2 });
  const double firstArea = rectangle.getArea();

  const double multiplier = 2.0;
  rectangle.scale(multiplier);
  double secondArea = rectangle.getArea();

  BOOST_CHECK_CLOSE(firstArea * pow(multiplier, 2), secondArea, accuracy);
}

BOOST_AUTO_TEST_CASE(throwingExceptions)
{
  BOOST_CHECK_THROW(bondarev::Triangle({ 0, 0 }, { 0, 0 }, { 0, 0 }), std::invalid_argument);

  bondarev::Triangle triangle_({ 3, 5 }, { 2, 2 }, { -1, -3 });
  BOOST_CHECK_THROW(triangle_.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(constantAreaAfterRotation)
{
  bondarev::Triangle triangleTest({ 4, 3 }, { 5, 4 }, { 2, 6 });
  const double testArea = triangleTest.getArea();
  const double angle = 90;

  triangleTest.rotate(angle);
  BOOST_CHECK_CLOSE(testArea, triangleTest.getArea(), accuracy);
}

BOOST_AUTO_TEST_SUITE_END()
