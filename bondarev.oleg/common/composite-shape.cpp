#include "composite-shape.hpp"
#include <stdexcept>
#include <algorithm>
#include <stdexcept>
#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <memory>


bondarev::CompositeShape::CompositeShape() :
  size_(0)
{
}

bondarev::CompositeShape::CompositeShape(const CompositeShape& copiedCompositeShape) :
  size_(copiedCompositeShape.size_),
  shapes_(std::make_unique<shape_ptr[]>(copiedCompositeShape.size_))
{
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i] = copiedCompositeShape.shapes_[i];
  }
}

bondarev::CompositeShape::CompositeShape(CompositeShape &&movedCompositeShape) :
  size_(movedCompositeShape.size_),
  shapes_(std::move(movedCompositeShape.shapes_))
{
  movedCompositeShape.size_ = 0;
}

bondarev::CompositeShape::CompositeShape(const shape_ptr &shape) :
  size_(1),
  shapes_(std::make_unique<shape_ptr[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer can't be null");
  }

  shapes_[0] = shape;
}

bondarev::CompositeShape &bondarev::CompositeShape::operator =(const CompositeShape &copiedCompositeShape)
{
  if (this != &copiedCompositeShape)
  {
    shapes_array shapesArray(std::make_unique<shape_ptr[]>(copiedCompositeShape.size_));
    size_ = copiedCompositeShape.size_;
    for (size_t i = 0; i < size_; i++)
    {
      shapesArray[i] = copiedCompositeShape.shapes_[i];
    }

    shapes_.swap(shapesArray);
  }

  return *this;
}

bondarev::CompositeShape &bondarev::CompositeShape::operator =(CompositeShape &&movedCompositeShape)
{
  if (this != &movedCompositeShape)
  {
    size_ = movedCompositeShape.size_;
    shapes_ = std::move(movedCompositeShape.shapes_);
    movedCompositeShape.size_ = 0;
  }

  return *this;
}

bondarev::shape_ptr bondarev::CompositeShape::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Index out of range");
  }

  return shapes_[index];
}

double bondarev::CompositeShape::getArea() const
{
  double area = 0.0;
  for (size_t i = 0; i < size_; i++)
  {
    area += shapes_[i]->getArea();
  }

  return area;
}

bondarev::rectangle_t bondarev::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape must be not empty");
  }

  rectangle_t frameRect = shapes_[0]->getFrameRect();

  double minX = frameRect.pos.x - frameRect.width / 2;
  double maxX = frameRect.pos.x + frameRect.width / 2;
  double minY = frameRect.pos.y - frameRect.height / 2;
  double maxY = frameRect.pos.y + frameRect.height / 2;

  for (size_t i = 0; i < size_; i++)
  {
    frameRect = shapes_[i]->getFrameRect();
    minX = std::min(frameRect.pos.x - frameRect.width / 2, minX);
    maxX = std::max(frameRect.pos.x + frameRect.width / 2, maxX);
    minY = std::min(frameRect.pos.y - frameRect.height / 2, minY);
    maxY = std::max(frameRect.pos.y + frameRect.height / 2, maxY);
  }

  return { { (maxX + minX) / 2, (maxY + minY) / 2 }, maxX - minX, maxY - minY };
}

int bondarev::CompositeShape::getSize() const
{
  return size_;
}

void bondarev::CompositeShape::move(double dx, double dy)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape must be not empty");
  }

  for (unsigned int i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void bondarev::CompositeShape::move(const point_t & newPoint)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape must be not empty");
  }

  rectangle_t frameRect = getFrameRect();

  double dx = newPoint.x - frameRect.pos.x;
  double dy = newPoint.y - frameRect.pos.y;
  for (unsigned int i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void bondarev::CompositeShape::scale(double coef)
{
  if (coef <= 0.0)
  {
    throw std::invalid_argument("Coefficient of scale must be positive");
  }

  if (size_ == 0)
  {
    throw std::logic_error("Composite shape must be not empty");
  }

  rectangle_t frameRect = getFrameRect();

  for (unsigned int i = 0; i < size_; i++)
  {
    double dx = (shapes_[i]->getFrameRect().pos.x - frameRect.pos.x) * coef;
    double dy = (shapes_[i]->getFrameRect().pos.y - frameRect.pos.y) * coef;
    shapes_[i]->move({ frameRect.pos.x + dx, frameRect.pos.y + dy });
    shapes_[i]->scale(coef);
  }
}

void bondarev::CompositeShape::addShape(shape_ptr &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Pointer must be not null!");
  }

  shapes_array shapesArray(std::make_unique<shape_ptr[]>(size_ + 1));
  for (size_t i = 0; i < size_; i++)
  {
    shapesArray[i] = shapes_[i];
  }

  shapesArray[size_] = shape;
  size_++;

  shapes_.swap(shapesArray);
}

void bondarev::CompositeShape::deleteShape(size_t index)
{
  if (index >= size_)
  {
    throw std::invalid_argument("Index must be less than count of shapes");
  }

  size_--;
  for (unsigned int i = index; i < size_; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }

  shapes_[size_] = nullptr;
}

bondarev::shapes_array bondarev::CompositeShape::getFigures() const
{
  shapes_array tmpFigures(std::make_unique<shape_ptr[]>(size_));

  for (size_t i = 0; i < size_; i++)
  {
    tmpFigures[i] = shapes_[i];
  }

  return tmpFigures;
}

void bondarev::CompositeShape::rotate(double angle)
{
  const point_t center = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++)
  {
    point_t tmpCenter = shapes_[i]->getFrameRect().pos;
    const double dx = (tmpCenter.x - center.x) * (std::abs(std::cos(angle * M_PI / 180)) - 1)
        - (tmpCenter.y - center.y)  * std::abs(std::sin(angle * M_PI / 180));
    const double dy = (tmpCenter.x - center.x) * std::abs(std::sin(angle * M_PI / 180))
        + (tmpCenter.y - center.y)  * (std::abs(std::cos(angle * M_PI / 180)) - 1);

    shapes_[i]->move(dx, dy);
    shapes_[i]->rotate(angle);
  }
}
