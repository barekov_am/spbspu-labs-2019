#ifndef PARTITION_HPP
#include "composite-shape.hpp"
#include "matrix.hpp"

namespace bondarev
{
  Matrix split(const bondarev::shapes_array &, size_t);
  Matrix split(const CompositeShape &);
  bool intersect(const rectangle_t &, const rectangle_t &);
}

#endif // !PARTITION_HPP
