#include <iostream>
#include "base-types.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

void printArea(const bondarev::Shape &shape)
{
  std::cout << "Area is " << shape.getArea() << "\n";
}

void printFrameRect(const bondarev::Shape &shape)
{
  bondarev::rectangle_t frameRect = shape.getFrameRect();
  std::cout <<  "Center is " << '(' 
      << frameRect.pos.x << ", " << frameRect.pos.y << ')' 
      << ", Height is " << frameRect.height 
      << ", Width is " << frameRect.width << "\n\n";
}

int main()
{
  std::cout << "In rectangle with center (1, 2), height = 3, width = 4\n";
  bondarev::Rectangle rectangle({ {1, 2}, 3, 4 });
  printArea(rectangle);
  printFrameRect(rectangle);

  std::cout << "In circle with center (5, 6), radius = 7\n";
  bondarev::Circle circle({ 5, 6 }, 7);
  printArea(circle);
  printFrameRect(circle);

  std::cout << "Move circle by (dx = 8 and dy = 9)\n";
  circle.move(8, 9);
  printArea(circle);
  printFrameRect(circle);

  std::cout << "Move rectangle to the point (10, 11)\n";
  rectangle.move({ 10, 11 });
  printArea(rectangle);
  printFrameRect(rectangle);

  std::cout << "Scaling circle by 2\n";
  circle.scale(2);
  printArea(circle);
  printFrameRect(circle);

  std::cout << "Scaling rectangle by 3\n";
  rectangle.scale(3);
  printArea(rectangle);
  printFrameRect(rectangle);

  std::cout << "In triangle with vertices {4, 3} {5, 4} {2, 6}\n";
  bondarev::Triangle triangle({ 4, 3 }, { 5, 4 }, { 2, 6 });
  printArea(triangle);
  printFrameRect(triangle);

  std::cout << "Move triangle to the point (12, 3)\n";
  triangle.move({ 10, 3 });
  printArea(triangle);
  printFrameRect(triangle);

  std::cout << "Scaling triangle by 5\n";
  triangle.scale(5);
  printArea(triangle);
  printFrameRect(triangle);

  bondarev::point_t polyShape[] = { {4, 5}, { 6, 4 }, { 7, -1 }, { 5, -3 }, { 2, 1 } };
  int pointQuantity = sizeof(polyShape) / sizeof(polyShape[0]);
  bondarev::Polygon polygon(pointQuantity, polyShape);

  std::cout << "in polygon with vertices {4, 5} {6, 4} {7, -1} {5, -3} {2, 1}\n";
  printArea(polygon);
  printFrameRect(polygon);

  std::cout << "scaling polygon by 2\n";
  polygon.scale(2);
  printArea(polygon);
  printFrameRect(polygon);

  bondarev::CompositeShape composite;
  bondarev::shape_ptr rectanglePtr = std::make_shared<bondarev::Rectangle>(rectangle);
  bondarev::shape_ptr circlePtr = std::make_shared<bondarev::Circle>(circle);
  bondarev::shape_ptr trianglePtr = std::make_shared<bondarev::Triangle>(triangle);

  std::cout << "\nComposite shape:\n\n";
  composite.addShape(circlePtr);
  composite.addShape(rectanglePtr);
  composite.addShape(trianglePtr);
  printArea(composite);
  printFrameRect(composite);

  std::cout << "scaled by 0.5\n";
  composite.scale(0.5);
  printArea(composite);
  printFrameRect(composite);

  std::cout << "moved by (2, 4)\n";
  composite.move(2, 4);
  printArea(composite);
  printFrameRect(composite);

  std::cout << "deleting circle and moving composite shape to point {-0.3, 3.5}\n";
  composite.deleteShape(1);
  composite.move({ -0.3, 3.5 });
  printArea(composite);
  printFrameRect(composite);

  getchar();
  return 0;
}
