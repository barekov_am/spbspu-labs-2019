#include <stdexcept>
#include "boost/test/auto_unit_test.hpp"
#include "rectangle.hpp"

const double accuracy = 0.1;

BOOST_AUTO_TEST_SUITE(testsSuiteRectangle)

BOOST_AUTO_TEST_CASE(immutabilityOfMeasurements)
{
  trofimov::Rectangle rectangle({ 5, 10 }, 10, 5);
  const trofimov::rectangle_t rectangleFrameBeforeMoving = rectangle.getFrameRect();
  const double areaBeforeMoving = rectangle.getArea();
  rectangle.move(5, 6);
  trofimov::rectangle_t rectangleFrameAfterMoving = rectangle.getFrameRect();
  double areaAfterMoving = rectangle.getArea();
  BOOST_CHECK_CLOSE(rectangleFrameBeforeMoving.width, rectangleFrameAfterMoving.width, accuracy);
  BOOST_CHECK_CLOSE(rectangleFrameBeforeMoving.height, rectangleFrameAfterMoving.height, accuracy);
  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, accuracy);

  rectangle.move({ 2, 5 });
  rectangleFrameAfterMoving = rectangle.getFrameRect();
  areaAfterMoving = rectangle.getArea();
  BOOST_CHECK_CLOSE(rectangleFrameBeforeMoving.width, rectangleFrameAfterMoving.width, accuracy);
  BOOST_CHECK_CLOSE(rectangleFrameBeforeMoving.height, rectangleFrameAfterMoving.height, accuracy);
  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, accuracy);
}

BOOST_AUTO_TEST_CASE(squareChangeOfArea)
{
  trofimov::Rectangle rectangle({ 5, 10 }, 2, 2);
  const double areaBeforeScaling = rectangle.getArea();
  rectangle.scale(2);
  const double areaAfterScaling = rectangle.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScaling * areaBeforeScaling, areaAfterScaling, accuracy);
}

BOOST_AUTO_TEST_CASE(availabilityAndProcessingOfIncorrectParameters)
{
  BOOST_CHECK_THROW(trofimov::Rectangle rectangle({ 5, 10 }, -3, 3), std::invalid_argument);
  trofimov::Rectangle rectangle({ 2, 3 }, 5, 7);
  BOOST_CHECK_THROW(rectangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
