#include "base-types.hpp"
#include <cmath>

bool trofimov::intersection(const rectangle_t & rectangle1, const rectangle_t & rectangle2)
{
  if ((fabs(rectangle1.pos.x - rectangle2.pos.x) < rectangle1.width / 2 + rectangle2.width / 2) &&
      (fabs(rectangle1.pos.y - rectangle2.pos.y) < rectangle1.height / 2  + rectangle2.height / 2)) {
    return true;
  } else {
    return false;
  }
}
