#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

#include <iostream>
#include <memory>

namespace trofimov
{
  class Matrix
  {
  public:
    class Layer
    {
    public:
      Layer(Shape **, int);
      Shape * operator [](int) const;
      int getSize();
    private:
      std::unique_ptr< Shape *[]> shapes_;
      int size_;
    };

    Matrix();
    Matrix(const Matrix &);
    Matrix(Matrix &&);

    ~Matrix() = default;

    Matrix & operator =(const Matrix &);
    Matrix & operator =(Matrix &&);
    Layer operator [](int) const;

    void add(Shape *, int);

    int getSize() const;
    int getCountOfLayers() const;

  private:
    std::unique_ptr< Shape *[] > shapesMatrix_;
    int countOfLayers_;
    std::unique_ptr< int [] > sizeOfLayers_;
  };
}

#endif
