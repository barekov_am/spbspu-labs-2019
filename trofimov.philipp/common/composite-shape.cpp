#include "composite-shape.hpp"
#include <stdexcept>
#include <iostream>
#include <assert.h>
#include <cmath>

trofimov::CompositeShape::CompositeShape():
  shapes_(),
  shapesCount_(0),
  shapesSize_(0)
{
}

trofimov::CompositeShape::CompositeShape(Shape *shape):
  shapes_(new Shape *[1]),
  shapesCount_(1),
  shapesSize_(1)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Pointer on shape you try to add is nullptr");
  }
  shapes_[0] = shape;
}

trofimov::CompositeShape::CompositeShape(const CompositeShape &otherShape):
  shapes_(new Shape *[otherShape.shapesCount_]),
  shapesCount_(otherShape.shapesCount_),
  shapesSize_(otherShape.shapesSize_)
{
  for (int i = 0; i < shapesCount_; i++)
  {
    shapes_[i] = otherShape.shapes_[i];
  }
}

trofimov::CompositeShape::CompositeShape(CompositeShape &&otherShape):
  shapes_(std::move(otherShape.shapes_)),
  shapesCount_(otherShape.shapesCount_),
  shapesSize_(otherShape.shapesSize_)
{
  otherShape.shapesCount_ = 0;
  otherShape.shapesSize_ = 0;
}


trofimov::CompositeShape &trofimov::CompositeShape::operator =(const CompositeShape &otherShape)
{
  if (&otherShape == this)
  {
    return *this;
  }
  shapesCount_ = otherShape.shapesCount_;
  shapesSize_ = otherShape.shapesSize_;
  shapes_.reset(new Shape *[shapesCount_]);
  for (int i = 0; i < shapesCount_; i++)
  {
    shapes_[i] = otherShape.shapes_[i];
  }
  return *this;
}

trofimov::CompositeShape &trofimov::CompositeShape::operator =(CompositeShape &&otherShape)
{
  if (&otherShape == this)
  {
    return *this;
  }
  shapesCount_ = otherShape.shapesCount_;
  shapesSize_ = otherShape.shapesSize_;
  shapes_ = std::move(otherShape.shapes_);
  otherShape.shapesCount_ = 0;
  otherShape.shapesSize_ = 0;
  return *this;
}

double trofimov::CompositeShape::getArea() const
{
  if (shapesCount_ == 0)
  {
    throw std::logic_error("Ivalid count of figures");
  }

  double area = 0;
  for (int i = 0; i < shapesCount_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

trofimov::rectangle_t trofimov::CompositeShape::getFrameRect() const
{
  if (shapesCount_ == 0)
  {
    throw std::logic_error("Ivalid count of figures");
  }

  rectangle_t tmpRectangle = shapes_[0]->getFrameRect();
  double maxX = tmpRectangle.pos.x + (0.5 * tmpRectangle.width);
  double maxY = tmpRectangle.pos.y + (0.5 * tmpRectangle.height);
  double minX = tmpRectangle.pos.x - (0.5 * tmpRectangle.width);
  double minY = tmpRectangle.pos.y - (0.5 * tmpRectangle.height);

  for (int i = 1; i < shapesCount_; i++)
  {
    tmpRectangle = shapes_[i]->getFrameRect();

    maxX = std::max(tmpRectangle.pos.x + 0.5 * tmpRectangle.width, maxX);
    maxY = std::max(tmpRectangle.pos.y + 0.5 * tmpRectangle.height, maxY);
    minX = std::min(tmpRectangle.pos.x - 0.5 * tmpRectangle.width, minX);
    minY = std::min(tmpRectangle.pos.y - 0.5 * tmpRectangle.height, minY);

  }
  tmpRectangle.width = maxX - minX;
  tmpRectangle.height = maxY - minY;
  tmpRectangle.pos.x = 0.5 * (maxX + minX);
  tmpRectangle.pos.y = 0.5 * (maxX + minY);
  return tmpRectangle;
}

void trofimov::CompositeShape::move(double dx, double dy)
{
  if (shapesCount_ == 0)
  {
    throw std::logic_error("Ivalid count of figures");
  }

  for (int i = 0; i < shapesCount_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void trofimov::CompositeShape::move(const point_t &point)
{
  move(point.x - getFrameRect().pos.x, point.y - getFrameRect().pos.y);
}

void trofimov::CompositeShape::getInfo() const
{
  std::cout << "Composite shape area = " << getArea() << std::endl;
  std::cout << "X center = " << getFrameRect().pos.x << std::endl;
  std::cout << "Y center = " << getFrameRect().pos.y << std::endl;
  std::cout << "Composite shape has " << shapesCount_ << " figures" << std::endl;
  std::cout << std::endl;

  for (int i = 0; i < shapesCount_; i++)
  {
    std::cout << "Info about " << i << " figure: "<< std::endl;
    std::cout << "Area = " << shapes_[i]->getArea() << std::endl;
    std::cout << "X center = " << shapes_[i]->getFrameRect().pos.x << std::endl;
    std::cout << "Y center = " << shapes_[i]->getFrameRect().pos.y << std::endl;
    std::cout << "_____________________________________" << std::endl;
  }
}

void trofimov::CompositeShape::scale(double cScaling)
{
  if (shapesCount_ == 0)
  {
    throw std::logic_error("Ivalid count of figures");
  }

  if (cScaling <= 0)
  {
    throw std::invalid_argument("Ivalid cScaling");
  }

  const point_t center = getFrameRect().pos;

  for (int i = 0; i < shapesCount_; i++)
  {
    double dx = shapes_[i]->getFrameRect().pos.x - center.x;
    double dy = shapes_[i]->getFrameRect().pos.y - center.y;
    dx *= cScaling;
    dy *= cScaling;
    shapes_[i]->move({center.x + dx, center.y + dy});
    shapes_[i]->scale(cScaling);
  }
}


void trofimov::CompositeShape::add(Shape *otherShape)
{
  if (otherShape == nullptr)
  {
    throw std::invalid_argument("Pointer on shape you try to add is nullptr");
  }

  for (int i = 0; i < shapesCount_; i++)
  {
    if (shapes_[i] == otherShape)
    {
      return;
    }
  }

  if (shapesSize_ > shapesCount_)
  {
    shapes_[shapesCount_] = otherShape;
    ++shapesCount_;
    return;
  }
  else
  {
    int newShapesSize = 0;
    if (shapesSize_ == 0)
    {
      newShapesSize = 2;
    }
    else
    {
      newShapesSize = ceil(shapesSize_ * cArrayIncrease_);
    }
    std::unique_ptr<Shape *[]> updatedShape(new Shape *[newShapesSize]);
    for (int i = 0; i < shapesCount_; i++)
    {
      updatedShape[i] = shapes_[i];
    }
    updatedShape[shapesCount_] = otherShape;
    ++shapesCount_;
    shapesSize_ = newShapesSize;
    shapes_ = std::move(updatedShape);
  }
}

void trofimov::CompositeShape::remove(int index)
{
  if ((index > shapesCount_ ) || (index < 0))
  {
    throw std::invalid_argument("Invalid index");
  }

  for (int i = index; i < shapesCount_ - 1; i++)
  {
    shapes_[i] = shapes_[i+1];
  }
  shapes_[shapesCount_ - 1] = nullptr;
  --shapesCount_;
}

void trofimov::CompositeShape::rotate(double angle)
{
  const trofimov::point_t center = getFrameRect().pos;
  const double cos = std::abs(std::cos(angle * M_PI / 180));
  const double sin = std::abs(std::sin(angle * M_PI / 180));

  for (int i = 0; i < shapesCount_; ++i)
  {
    trofimov::point_t currCenter = shapes_[i]->getFrameRect().pos;
    double projectionX = currCenter.x - center.x;
    double projectionY = currCenter.y - center.y;
    double shiftX = projectionX * (cos - 1) - projectionX * sin;
    double shiftY = projectionX * sin + projectionY * (cos - 1);
    shapes_[i]->move(shiftX, shiftY);
    shapes_[i]->rotate(angle);
  }
}

trofimov::Matrix trofimov::CompositeShape::split() const
{
  Matrix matrix;

  if (shapesCount_ == 0)
  {
    return matrix;
  }

  int shapesLayers[shapesCount_];
  for (int i = 0; i < shapesCount_; i++)
  {
    shapesLayers[i] = 0;
  }

  for (int i = 0; i < shapesCount_; i++)
  {
    matrix.add(shapes_[i], shapesLayers[i]);
    rectangle_t shapeFrameRect = shapes_[i]->getFrameRect();
    for (int j = i + 1; j < shapesCount_; j++)
    {
      if (intersection(shapeFrameRect, shapes_[j]->getFrameRect()))
      {
        shapesLayers[j] = std::max(shapesLayers[j], shapesLayers[i] + 1);
      }
    }
  }

  return matrix;
}
