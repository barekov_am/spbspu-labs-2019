#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "matrix.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteMatrix)

BOOST_AUTO_TEST_CASE(testInitialization)
{
  trofimov::Rectangle testRectangle({ 2.0, 1.0 }, 4.0, 2.0);
  trofimov::Circle testCircle({2.0, 8.0}, 3.0);

  trofimov::CompositeShape testCoShape(&testRectangle);
  testCoShape.add(&testCircle);

  BOOST_CHECK_NO_THROW(trofimov::Matrix testMatrix1);
  BOOST_CHECK_NO_THROW(trofimov::Matrix testMatrix2 = testCoShape.split());

  trofimov::Matrix testMatrix = testCoShape.split();
  BOOST_CHECK_EQUAL(testMatrix.getSize(), 2);
  BOOST_CHECK_EQUAL(testMatrix.getCountOfLayers(), 1);
}

BOOST_AUTO_TEST_CASE(testOperators)
{
  trofimov::Rectangle testRectangle({ 2.0, 1.0 }, 4.0, 2.0);
  trofimov::Circle testCircle({2.0, 8.0}, 3.0);

  trofimov::CompositeShape testCoShape;
  testCoShape.add(&testRectangle);
  testCoShape.add(&testCircle);

  trofimov::Matrix testMatrix1;
  trofimov::Matrix testMatrix2 = testCoShape.split();

  BOOST_CHECK_NO_THROW(testMatrix1 = testMatrix2);
  BOOST_CHECK_NO_THROW(testMatrix1 = std::move(testMatrix2));

  BOOST_CHECK_THROW(testMatrix2[100], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix2[0][100], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(testIntersection)
{
  trofimov::Rectangle testRectangle({ 2.0, 1.0 }, 4.0, 2.0);
  trofimov::Circle testCircle({2.0, 8.0}, 3.0);

  BOOST_CHECK_EQUAL(trofimov::intersection(testRectangle.getFrameRect(), testCircle.getFrameRect()), false);
  
  trofimov::Rectangle testRectangle2({ 2.0, 1.0 }, 6.0, 3.0);
  
  BOOST_CHECK_EQUAL(trofimov::intersection(testRectangle.getFrameRect(), testRectangle2.getFrameRect()), true);
}

BOOST_AUTO_TEST_SUITE_END();
