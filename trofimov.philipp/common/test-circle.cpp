#include <stdexcept>
#include "boost/test/auto_unit_test.hpp"
#include "circle.hpp"

const double accuracy = 0.1;

BOOST_AUTO_TEST_SUITE(testsSuiteCircle)

BOOST_AUTO_TEST_CASE(immutabilityOfMeasurements)
{
  trofimov::Circle circle({ 13, 15 }, 5);
  const trofimov::rectangle_t circleFrameBeforeMoving = circle.getFrameRect();
  const double areaBeforeMoving = circle.getArea();
  circle.move(2, 4);
  trofimov::rectangle_t circleFrameAfterMoving = circle.getFrameRect();
  double areaAfterMoving = circle.getArea();
  BOOST_CHECK_CLOSE(circleFrameBeforeMoving.height, circleFrameAfterMoving.height, accuracy);
  BOOST_CHECK_CLOSE(circleFrameBeforeMoving.width, circleFrameAfterMoving.width, accuracy);
  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, accuracy);

  circle.move({ 3, 5 });
  circleFrameAfterMoving = circle.getFrameRect();
  areaAfterMoving = circle.getArea();
  BOOST_CHECK_CLOSE(circleFrameBeforeMoving.height, circleFrameAfterMoving.height, accuracy);
  BOOST_CHECK_CLOSE(circleFrameBeforeMoving.width, circleFrameAfterMoving.width, accuracy);
  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, accuracy);
}

BOOST_AUTO_TEST_CASE(squareChangeOfArea)
{
  trofimov::Circle circle({ 13, 15 }, 1);
  const double areaBeforeScaling = circle.getArea();
  circle.scale(1.772);
  double areaAfterScaling = circle.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScaling * areaBeforeScaling, areaAfterScaling, accuracy);
}

BOOST_AUTO_TEST_CASE(availabilityAndProcessingOfIncorrectParameters)
{
  BOOST_CHECK_THROW(trofimov::Circle circle({ 11, 3 }, -3), std::invalid_argument);
  trofimov::Circle circle({ 2, 3 }, 7);
  BOOST_CHECK_THROW(circle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
