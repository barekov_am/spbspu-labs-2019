#include "rectangle.hpp"
#include <iostream>
#include <assert.h>
#include <cmath>

trofimov::Rectangle::Rectangle(const point_t &pos, double w, double h) :
  rectangle_({ pos, w, h }),
  angle_(0)
{
  if ((rectangle_.height <= 0) || (rectangle_.width <= 0))
  {
    throw std::invalid_argument("Not positive parametrs");
  }
}

double trofimov::Rectangle::getArea() const
{
  return rectangle_.height * rectangle_.width;
}

trofimov::rectangle_t trofimov::Rectangle::getFrameRect() const
{
  const double cos = std::abs(std::cos(angle_ * M_PI / 180));
  const double sin = std::abs(std::sin(angle_ * M_PI / 180));
  const double width = fabs(cos * rectangle_.width) + fabs(sin * rectangle_.height);
  const double height = fabs(cos * rectangle_.height) + fabs(sin * rectangle_.width);

  return {rectangle_.pos, width, height};
}

void trofimov::Rectangle::move(const point_t &point)
{
  rectangle_.pos = point;
}

void trofimov::Rectangle::move(double dx, double dy)
{
  rectangle_.pos.x += dx;
  rectangle_.pos.y += dy;
}

void trofimov::Rectangle::getInfo() const
{
  rectangle_t rectangle = getFrameRect();
  std::cout << "Rectangle height = " << rectangle.height << std::endl;
  std::cout << "Rectangle width = " << rectangle.width << std::endl;
  std::cout << "Center x = " << rectangle.pos.x << std::endl;
  std::cout << "Center y = " << rectangle.pos.y << std::endl;
}

void trofimov::Rectangle::scale(double cScaling)
{
  if (cScaling <= 0)
  {
    throw std::invalid_argument("Not positive parametrs");
  }
  rectangle_.height *= cScaling;
  rectangle_.width *= cScaling;
}

void trofimov::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
