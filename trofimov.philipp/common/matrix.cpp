#include "matrix.hpp"

trofimov::Matrix::Matrix():
  shapesMatrix_(),
  countOfLayers_(0),
  sizeOfLayers_()
{ }

trofimov::Matrix::Matrix(const Matrix & other):
  shapesMatrix_(new Shape *[other.getSize()]),
  countOfLayers_(other.countOfLayers_),
  sizeOfLayers_(new int [countOfLayers_])
{
  for (int i = 0; i < getSize(); i++)
  {
    shapesMatrix_[i] = other.shapesMatrix_[i];
  }
  for (int i = 0; i < countOfLayers_; i++)
  {
    sizeOfLayers_[i] = other.sizeOfLayers_[i];
  }
}

trofimov::Matrix::Matrix(Matrix && other):
  shapesMatrix_(std::move(other.shapesMatrix_)),
  countOfLayers_(other.countOfLayers_),
  sizeOfLayers_(std::move(other.sizeOfLayers_))
{
  other.countOfLayers_ = 0;
}

trofimov::Matrix & trofimov::Matrix::operator =(const Matrix & other)
{
  if (this != &other) {
    int countOfFigures = other.getSize();
    shapesMatrix_.reset(new Shape *[countOfFigures]);
    for (int i = 0; i < countOfFigures; i++) {
      shapesMatrix_[i] = other.shapesMatrix_[i];
    }

    countOfLayers_ = other.countOfLayers_;

    sizeOfLayers_.reset(new int [countOfLayers_]);
    for (int i = 0; i < countOfLayers_; i++) {
      sizeOfLayers_[i] = other.sizeOfLayers_[i];
    }
  }

  return *this;
}

trofimov::Matrix & trofimov::Matrix::operator =(Matrix && other)
{
  if (this != &other) {
    shapesMatrix_ = std::move(other.shapesMatrix_);
    countOfLayers_ = other.countOfLayers_;
    sizeOfLayers_ = std::move(other.sizeOfLayers_);

    other.countOfLayers_ = 0;
  }

  return *this;
}

trofimov::Matrix::Layer trofimov::Matrix::operator [](int index) const
{
  if (index >= countOfLayers_)
  {
    throw std::out_of_range("There are not so many layers in the matrix");
  }

  int indexFirstFigure = 0;
  for (int i = 0; i < index; i++)
  {
    indexFirstFigure += sizeOfLayers_[i];
  }
  Shape * shapesArray[sizeOfLayers_[index]];
  for (int i = 0; i < sizeOfLayers_[index]; i++)
  {
    shapesArray[i] = shapesMatrix_[indexFirstFigure + i];
  }

  return Layer(shapesArray, sizeOfLayers_[index]);
}

void trofimov::Matrix::add(Shape * shape, int layer)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("There is no any figure to add - shape pointer is null");
  }

  int index = 0;
  for (int i = 0; i < layer; i++)
  {
    index += sizeOfLayers_[i]; //where figure should be inserted
  }
  if (layer != countOfLayers_)
  {
    index += sizeOfLayers_[layer];
    sizeOfLayers_[layer]++;
  }
  /*create new layer*/
  else
  {
    std::unique_ptr< int [] > tmpArray(new int [countOfLayers_ + 1]); //create new layer
    for (int i = 0; i < countOfLayers_; i++)
    {
      tmpArray[i] = sizeOfLayers_[i];
    }
    tmpArray[layer] = 1;
    countOfLayers_++;
    sizeOfLayers_ = std::move(tmpArray);
  }
  int newSize = getSize();
  std::unique_ptr< Shape *[] > tmpMatrix(new Shape *[newSize]);
  for (int i = 0; i < index; i++)
  {
    tmpMatrix[i] = shapesMatrix_[i];
  }
  tmpMatrix[index] = shape;
  for (int i = index + 1; i < newSize; i++)
  {
    tmpMatrix[i] = shapesMatrix_[i - 1];
  }
  shapesMatrix_ = std::move(tmpMatrix);
}

int trofimov::Matrix::getSize() const
{
  int size = 0;
  for (int i = 0; i < countOfLayers_; i++) {
    size += sizeOfLayers_[i];
  }

  return size;
}

int trofimov::Matrix::getCountOfLayers() const
{
  return countOfLayers_;
}

trofimov::Matrix::Layer::Layer(Shape ** shapes, int size):
  shapes_(new Shape *[size]),
  size_(size)
{
  if (shapes == nullptr) {
    throw std::invalid_argument("Pointer on array with layer's figures is null");
  }

  for (int i = 0; i < size_; i++) {
    shapes_[i] = shapes[i];
  }
}

trofimov::Shape * trofimov::Matrix::Layer::operator [](int index) const
{
  if (index >= size_) {
    throw std::out_of_range("There are not so many figures on the layer");
  }

  return shapes_[index];
}

int trofimov::Matrix::Layer::getSize()
{
  return size_;
}
