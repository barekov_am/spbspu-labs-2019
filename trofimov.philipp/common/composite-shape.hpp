#ifndef COMPOSITESHAPE_H
#define COMPOSITESHAPE_H
#include "shape.hpp"
#include "matrix.hpp"
#include <iostream>
#include <memory>

namespace trofimov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();

    CompositeShape(const CompositeShape &otherShape);
    CompositeShape(CompositeShape &&otherShape);

    CompositeShape(Shape *otherShape);

    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &otherShape);
    CompositeShape &operator =(CompositeShape &&otherShape);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(double dx, double dy) override;
    void getInfo() const override;
    void scale(double cScaling) override;
    void add(Shape *otherShape);
    void remove(int index);
    void rotate(double angle) override;
    Matrix split() const;
  private:
    const double cArrayIncrease_ = 1.6;
    std::unique_ptr<Shape *[]> shapes_;
    int shapesCount_;
    int shapesSize_;
  };
}
#endif
