#include <stdexcept>
#include "boost/test/auto_unit_test.hpp"
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testsSuiteCompositeShape)

BOOST_AUTO_TEST_CASE(immutabilityOfMeasurements)
{
  trofimov::Rectangle r1({ 5, 4 }, 6, 4);
  trofimov::Circle c1({ 2, 5 }, 5);
  trofimov::CompositeShape shape(&r1);
  shape.add(&c1);
  const trofimov::rectangle_t frameBeforeMoving = shape.getFrameRect();
  const double areaBeforeMoving = shape.getArea();
  shape.move(2, 5);
  trofimov::rectangle_t frameAfterMoving = shape.getFrameRect();
  double areaAfterMoving = shape.getArea();
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, frameAfterMoving.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, frameAfterMoving.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, ACCURACY);

  shape.move({ 3, 6 });
  frameAfterMoving = shape.getFrameRect();
  areaAfterMoving = shape.getArea();
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, frameAfterMoving.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, frameAfterMoving.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, ACCURACY);
}

BOOST_AUTO_TEST_CASE(squareChangeOfArea)
{
  trofimov::Rectangle r1({ 5, 4 }, 6, 4);
  trofimov::Circle c1({ 2, 5 }, 5);
  trofimov::CompositeShape shape(&r1);
  shape.add(&c1);
  const trofimov::rectangle_t frameBeforeScaling = shape.getFrameRect();
  const double cScaling = 10;
  const double area = shape.getArea();
  const double newArea = area * cScaling * cScaling;
  shape.scale(cScaling);
  trofimov::rectangle_t frameAfterScaling = shape.getFrameRect();
  BOOST_CHECK_CLOSE(frameBeforeScaling.width * cScaling, frameAfterScaling.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeScaling.height * cScaling, frameAfterScaling.height, ACCURACY);
  BOOST_CHECK_CLOSE(shape.getArea(), newArea, ACCURACY);
}

BOOST_AUTO_TEST_CASE(availabilityAndProcessingOfIncorrectParameters)
{
  trofimov::Rectangle r1({ 5, 4 }, 6, 4);
  trofimov::Circle c1({ 2, 5 }, 5);
  trofimov::Circle r2({ 1, 6 }, 6);
  trofimov::CompositeShape shape(&r1);
  shape.add(&c1);
  BOOST_CHECK_THROW(shape.remove(6), std::invalid_argument);
  trofimov::CompositeShape shape2;
  BOOST_CHECK_THROW(shape2.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(shape2.getArea(), std::logic_error);
  BOOST_CHECK_THROW(shape.scale(-2), std::invalid_argument);

}

BOOST_AUTO_TEST_CASE(copyAndMoveConstructorTest)
{
  trofimov::Rectangle r1({ 5, 4 }, 6, 4);
  trofimov::Rectangle r2({ 6, 24 }, 9, 3);
  trofimov::CompositeShape shape1(&r1);
  const double sumArea = r1.getArea() + r2.getArea();
  trofimov::CompositeShape shape2(shape1);
  shape2.add(&r2);
  BOOST_CHECK_CLOSE(sumArea, shape2.getArea(), ACCURACY);
  trofimov::CompositeShape shape3(std::move(shape2));
  BOOST_CHECK_CLOSE(sumArea, shape3.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(copyAndMoveOverloadOperatorTest)
{
  trofimov::Rectangle r1({ 5, 4 }, 6, 4);
  trofimov::Rectangle r2({ 6, 24 }, 9, 3);
  trofimov::CompositeShape shape1(&r1);
  const double sumArea = r1.getArea() + r2.getArea();
  trofimov::CompositeShape shape2 = shape1;
  shape2.add(&r2);
  BOOST_CHECK_CLOSE(sumArea, shape2.getArea(), ACCURACY);
  trofimov::CompositeShape shape3 = std::move(shape2);
  BOOST_CHECK_CLOSE(sumArea, shape3.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
