#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  trofimov::Rectangle r1({ 5, 10 }, 6, 20);
/*Create composite shape using constructor*/
  trofimov::CompositeShape shape(&r1);
  trofimov::Circle c1({ 6, 11 }, 6);
  trofimov::Circle c2({ 6, 9 }, 5);
  trofimov::Rectangle r2({ 5, 4 }, 20, 20);
  trofimov::Rectangle r3({ 5, 3 }, 21, 21);
/*Add figures into composite shape*/
  shape.add(&c1);
  shape.add(&c2);
  shape.add(&r3);
  shape.add(&r2);
/*Get all info about composite shape*/
  shape.getInfo();
/*Delete figure with index 3*/
  shape.remove(0);
  shape.getInfo();
/*Move composite shape on dx = 5 and dy = 4*/
  shape.move(5, 4);
  shape.getInfo();
/*Move composite shape on point (2,2)*/
  shape.move(2, 2);
  shape.getInfo();
/*Scale composite shape by cScaling = 10*/
  shape.scale(10);
  shape.getInfo();

  return 0;
}
