#include <cstring>

#include "queue-with-priority.hpp"

bool parsePriority(std::string line, priority_t& priority)
{
  if (line == "high") {
    priority = priority_t::HIGH;
    return true;
  }

  if (line == "normal") {
    priority = priority_t::NORMAL;
    return true;
  }

  if (line == "low") {
    priority = priority_t::LOW;
    return true;
  }

  return false;
}
