#include <string>
#include <iostream>
#include "shape.hpp"
#include "functions.hpp"

void task2()
{
  std::vector< Shape > shapes;
  std::vector< Point > points;

  std::string str;
  while (std::getline(std::cin >> std::ws, str))
  {
    if (str.empty())
    {
      continue;
    }
    shapes.push_back(readShapes(str));
  }

  writeCountOfVertices(shapes);
  writeCountOfTriangles(shapes);
  writeCountOfSquares(shapes);
  writeCountOfRectangles(shapes);
  removePentagons(shapes);
  writePoints(shapes, points);
  writeShapes(shapes);
}
