#include <iostream>
#include <set>
#include <iterator>

void task1()
{
  std::set< std::string > str;
  std::string line;

  while(std::cin >> std::ws >> line)
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Invalid eading");
    }
    str.insert(line);
  }

  if (!str.empty())
  {
    for (const auto & word: str)
    {
      std::cout << word << std::endl;
    }
  }
}
