#ifndef B5_SHAPE
#define B5_SHAPE

#include <vector>

struct Point
{
  int x, y;
};

using Shape = std::vector< Point >;

#endif
