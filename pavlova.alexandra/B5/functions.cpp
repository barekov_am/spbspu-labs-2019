#include "functions.hpp"

#include <iostream>
#include <sstream>
#include <algorithm>

Shape readShapes(std::string & str)
{
  std::stringstream stream(str);

  int kolVertices;
  stream >> kolVertices;
  if (kolVertices <= 2)
  {
    throw std::invalid_argument("Invalid amount of vertices");
  }

  Shape shape;

  const int strLen = str.length();
  for (int i = 0; i < kolVertices; ++i)
  {
    Point point{0, 0};
    stream.ignore(strLen, '(');
    stream >> point.x;

    stream.ignore(strLen, ';');
    stream >> point.y;

    stream.ignore(strLen, ')');
    shape.push_back(point);
  }

  if(stream.fail())
  {
    throw std::invalid_argument("Invalid value of points");
  }
  std::string extra;
  getline(stream >> std::ws, extra);
  if(!extra.empty())
  {
    throw std::invalid_argument("Too many points");
  }

  return shape;
}

void writeCountOfVertices(std::vector< Shape > & shapes)
{
  size_t numberOfVertices = 0;
  auto iterVertices = shapes.begin();
  while (iterVertices != shapes.end()) {
    numberOfVertices += (*iterVertices).size();
    ++iterVertices;
  }
  std::cout << "Vertices: " << numberOfVertices << std::endl;
}

void writeCountOfTriangles(std::vector< Shape > & shapes)
{
  size_t countofTriangles = 0;
  auto iterTriangles = shapes.begin();
  while(iterTriangles != shapes.end())
  {
    if ((*iterTriangles).size() == 3)
    {
      ++countofTriangles;
    }
    ++iterTriangles;
  }
  std::cout << "Triangles: " << countofTriangles << std::endl;
}

bool compareX(const Point & point1, const Point & point2)
{
  return point1.x < point2.x;
}

bool compareY(const Point & point1, const Point & point2)
{
  return point1.y < point2.y;
}

bool isTriangle(const Shape& shape)
{
  return (shape.size() == 3);
}

bool isRectangle(const Shape& shape)
{
  return (shape.size() == 4);
}

bool isSquare(const Shape& shape)
{
  bool isSquare = false;
  if (shape.size() == 4)
  {
    const int maxX = (std::max_element(shape.begin(), shape.end(), compareX))->x;
    const int minX = (std::min_element(shape.begin(), shape.end(), compareX))->x;

    const int maxY = (std::max_element(shape.begin(), shape.end(), compareY))->y;
    const int minY = (std::min_element(shape.begin(), shape.end(), compareY))->y;

    if ((maxX - minX) == (maxY - minY))
    {
      isSquare = true;
    }
  }
  return isSquare;
}

void writeCountOfSquares(std::vector< Shape > & shapes)
{
  size_t countofSquares = 0;
  auto iter = shapes.begin();
  while(iter != shapes.end())
  {
    if (isSquare(*iter))
    {
      ++countofSquares;
    }
    ++iter;
  }
  std::cout << "Squares: " << countofSquares << std::endl;
}

void writeCountOfRectangles(std::vector< Shape > & shapes)
{
  size_t countofRectangles = 0;
  auto iterRectangles = shapes.begin();
  while(iterRectangles != shapes.end())
  {
    if ((*iterRectangles).size() == 4)
    {
      ++countofRectangles;
    }
    ++iterRectangles;
  }
  std::cout << "Rectangles: " << countofRectangles << std::endl;
}

void removePentagons(std::vector< Shape > & shapes)
{
  auto temp = remove_if(shapes.begin(), shapes.end(), [](const Shape & shape) { return (shape.size() == 5); });
  shapes.erase(temp, shapes.end());
}

void writePoints(std::vector< Shape > & shapes, std::vector< Point > & points)
{
  for (const auto & it : shapes)
  {
    points.push_back(it.at(0));
  }
  std::cout << "Points: ";
  for (auto & i : points)
  {
    std::cout << '(' << i.x << ';' << i.y << ") ";
  }

  auto triangleEnd = std::partition(shapes.begin(), shapes.end(), isTriangle);
  auto squareEnd = std::partition(triangleEnd, shapes.end(), isSquare);
  std::partition(squareEnd, shapes.end(), isRectangle);
  std::cout << std::endl;
}

void writeShapes(std::vector< Shape > & shapes)
{
  std::cout << "Shapes: " << std::endl;
  for (const auto & shape : shapes)
  {
    std::cout << shape.size();
    for (const auto point : shape)
    {
      std::cout << " (" << point.x << ';' << point.y << ") ";
    }
    std::cout << std::endl;
  }
}
