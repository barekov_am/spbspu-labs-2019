#ifndef B5_FUNCTIONS_HPP
#define B5_FUNCTIONS_HPP

#include <string>
#include "shape.hpp"

Shape readShapes(std::string & str);
void writeCountOfVertices(std::vector< Shape > & shapes);
void writeCountOfTriangles(std::vector< Shape > & shapes);
void writeCountOfSquares(std::vector< Shape > & shapes);
void writeCountOfRectangles(std::vector< Shape > & shapes);
void removePentagons(std::vector< Shape > & shapes);
void writePoints(std::vector< Shape > & shapes, std::vector< Point > & points);
void writeShapes(std::vector< Shape > & shapes);

#endif
