#ifndef B2_QUEUEWITHPRIORITY_HPP
#define B2_QUEUEWITHPRIORITY_HPP

#include <iostream>
#include <list>

typedef enum
{
  LOW,
  NORMAL,
  HIGH
} ElementPriority;

template <typename T>
class QueueWithPriority
{
public:
  void putElementToQueue(const T & element, ElementPriority priority);
  T getElementFromQueue();
  void accelerate();
  bool empty();

private:
  std::list<T> highPriority_;
  std::list<T> normalPriority_;
  std::list<T> lowPriority_;
};

template <typename T>
void QueueWithPriority<T>::putElementToQueue(const T & element, ElementPriority priority) {
  switch (priority) {
    case ElementPriority::HIGH:
      highPriority_.push_back(element);
      break;
    case ElementPriority::NORMAL:
      normalPriority_.push_back(element);
      break;
    case ElementPriority::LOW:
      lowPriority_.push_back(element);
      break;
    default:
      throw std::invalid_argument("Incorrect Priority");
  }
}

template <typename T>
T QueueWithPriority<T>::getElementFromQueue()
{
  if (!highPriority_.empty())
  {
    T element = highPriority_.front();
    highPriority_.pop_front();
    return element;
  }
  else if (!normalPriority_.empty())
  {
    T element = normalPriority_.front();
    normalPriority_.pop_front();
    return element;
  }
  else
  {
    T element = lowPriority_.front();
    lowPriority_.pop_front();
    return element;
  }
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  highPriority_.splice(highPriority_.end(), lowPriority_);
}

template <typename T>
bool QueueWithPriority<T>::empty()
{
  return (highPriority_.empty() && normalPriority_.empty() && lowPriority_.empty());
}

#endif
