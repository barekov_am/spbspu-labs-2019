#include <sstream>
#include "QueueWithPriority.hpp"

void add(QueueWithPriority<std::string> & queue, std::stringstream & str)
{
  std::string priority;
  str >> priority >> std::ws;
  std::string data;
  getline(str, data);

  if (priority.empty() || data.empty())
  {
    std::cout << "<INVALID COMMAND>" << std::endl;
    return;
  }
  if (priority == "high")
  {
    queue.putElementToQueue(data, ElementPriority::HIGH);
  }
  else if (priority == "normal")
  {
    queue.putElementToQueue(data, ElementPriority::NORMAL);
  }
  else if (priority == "low")
  {
    queue.putElementToQueue(data, ElementPriority::LOW);
  }
  else
  {
    std::cout << "<INVALID COMMAND>" << std::endl;
    return;
  }
}

void get(QueueWithPriority<std::string> & queue, std::stringstream & str)
{

  if (queue.empty())
  {
    std::cout << "<EMPTY>" << std::endl;
    return;
  }
  if (!str.eof())
  {
    std::cout << "<INVALID COMMAND>" << std::endl;
    return;
  }
  std::cout << queue.getElementFromQueue() << std::endl;
}

void accelerate(QueueWithPriority<std::string> & queue, std::stringstream & str)
{
  if (!str.eof())
  {
    std::cout << "<INVALID COMMAND>" << std::endl;
    return;
  }
  queue.accelerate();
}

void task1()
{
  QueueWithPriority<std::string> queue;
  std::string line;
  while(getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Incorrect input");
    }

    std::stringstream stringStr(line);
    stringStr >> line;
    if (line == "add")
    {
      add(queue, stringStr);
    }
    else if (line == "get")
    {
      get(queue, stringStr);
    }
    else if (line == "accelerate")
    {
      accelerate(queue, stringStr);
    }
    else
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }
  }
}
