#include <iostream>
#include <list>

const unsigned int maxNum = 20;
const unsigned int minNum = 1;
const size_t maxSize = 20;

void task2()
{
  std::list<int> list;
  unsigned int num;
  while (std::cin && !(std::cin >> num).eof())
  {
    if (num >= minNum && num <= maxNum)
    {
      list.push_back(num);
    }
    else throw std::invalid_argument("Incorrect value");

    if (list.size() > maxSize)
    {
      throw std::invalid_argument("Incorrect number of elements");
    }

    if (list.empty())
    {
      return;
    }

    if (std::cin.fail())
    {
      throw std::ios_base::failure("Incorrect input");
    }
  }

    auto it_first = list.begin();
    auto it_last = list.rbegin();

    for (size_t i = 0; i < list.size() / 2; ++i)
    {
      std::cout << *it_first << " " << *it_last << " ";
      it_first++;
      it_last++;
    }
    if (list.size() % 2 != 0)
    {
      std::cout << *it_first;
    }
    std::cout << std::endl;
}
