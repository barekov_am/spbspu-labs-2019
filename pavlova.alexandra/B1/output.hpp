#ifndef OUTPUT_HPP
#define OUTPUT_HPP

#include <iostream>

template <typename T>
void print(const T & container)
{
  if (container.empty())
  {
    return;
  }

  for (const auto & element : container)  //const double&
  {
    std::cout << element << " ";
  }
  std::cout << std::endl;
}

#endif
