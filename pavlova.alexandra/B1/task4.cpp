#include <vector>
#include <random>
#include "sorting.hpp"
#include "output.hpp"

double generation()
{
  double u, v, r, c;
  do
  {
    u = -1 + (static_cast<float>(rand()) / (RAND_MAX)) * 2;
    v = -1 + (static_cast<float>(rand()) / (RAND_MAX)) * 2;
    r = u * u + v * v;
  } while ((r > 1) || (r == 0));
  c = (sqrt(-2 * log(r) / r)) * v;
  return c;
}

void fillRandom(double * array, int size)
{
  for (int i = 0; i < size; ++i)
  {
    array[i] = generation();
  }
}

void task4(const char * direction, int size)
{
  if (size < 0)
  {
    throw std::invalid_argument("Size of array must be positive \n");
  }

  if (!size)
  {
    return;
  }

  auto dir = getDirection(direction); //Mydirection

  std::vector<double> vector(size, 0);
  fillRandom(& vector[0], size);

  print(vector);
  selectionSort<BrAccess>(vector, dir);
  print(vector);
}
