#include <iostream>
#include <random>
#include <ctime>

void task1(const char * direction);
void task2(const char * file);
void task3();
void task4(const char * direction, int size);

int main(int argc, char * argv[])
{
  srand(static_cast<unsigned int>(time(0)));

  try
  {
    if ((argc < 2) || (argc > 4))
    {
      std::cerr << "Invalid argc" << std::endl;
      return 1;
    }

    char * ptr = nullptr;
    const int taskNum = std::strtol(argv[1], &ptr, 10);

    if (* ptr)
    {
      std::cerr << "Invalid argument" << std::endl;
      return 1;
    }

    switch (taskNum)
    {
      case 1:
        if (argc != 3)
        {
          std::cerr << "Incorrect number of arguments" << std::endl;
          return 1;
        }
        task1(argv[2]);
        break;

      case 2:
        if (argc != 3)
        {
          std::cerr << "Incorrect number of arguments" << std::endl;
          return 1;
        }
        task2(argv[2]);
        break;

      case 3:
        if (argc != 2)
        {
          std::cerr << "Incorrect number of arguments" << std::endl;
          return 1;
        }
        task3();
        break;

      case 4:
      {
        if (argc != 4) {
          std::cerr << "Incorrect number of arguments" << std::endl;
          return 1;
        }
        int vectorSize = std::strtol(argv[3], & ptr, 10);
        if (* ptr)
        {
          std::cerr << "Invalid vector size.";
          return 1;
        }

        task4(argv[2], vectorSize);
        break;
      }

      default:
        std::cerr << "Incorrect number of task" << std::endl;
        return 1;
    }
  }
  catch (const std::exception & err)
  {
    std::cerr << err.what() << std::endl;
    return 1;
  }

  return 0;
}
