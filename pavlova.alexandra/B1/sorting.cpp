#include <cstring>
#include <stdexcept>
#include "sorting.hpp"

Mydirection getDirection(const char * direction)
{
  if (std::strcmp(direction, "ascending") == 0)
  {
    return Mydirection::ascending;
  }

  if (std::strcmp(direction, "descending") == 0)
  {
    return Mydirection::discending;
  }

  throw std::invalid_argument("Incorrect sorting direction.");
}
