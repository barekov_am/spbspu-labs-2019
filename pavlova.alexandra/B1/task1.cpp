#include <vector>
#include <forward_list>

#include "sorting.hpp"
#include "output.hpp"

void task1(const char * direction)
{
  auto dir = getDirection(direction); //Mydirection

  std::vector<int> vectorBracket;
  int num = 0;

  while (std::cin >> num)
  {
    vectorBracket.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Fail while reading data.");
  }

  if (vectorBracket.empty())
  {
    return;
  }

  std::vector<int> vectorAt = vectorBracket;
  std::forward_list<int> listIt(vectorBracket.begin(), vectorBracket.end());

  selectionSort<BrAccess>(vectorBracket, dir);
  selectionSort<AtAccess>(vectorAt, dir);
  selectionSort<ItAccess>(listIt, dir);

  print(vectorBracket);
  print(vectorAt);
  print(listIt);
}
