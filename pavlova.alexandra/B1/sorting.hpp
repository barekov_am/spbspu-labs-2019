#ifndef SORTING_HPP
#define SORTING_HPP

#include <functional>
#include "accesses.hpp"

enum class Mydirection
{
  ascending,
  discending
};

Mydirection getDirection(const char * direction);

template <template <class T> class Access, typename T>
void selectionSort(T & collection, Mydirection direction)
{
  using value_type = typename T::value_type;
  std::function<bool(value_type, value_type)> compare = (direction == Mydirection::ascending)
    ? std::function<bool(value_type, value_type)>(std::less<value_type>())
    : std::function<bool(value_type, value_type)>(std::greater<value_type>());

  const auto begin = Access<T>::begin(collection); //unsigned int
  const auto end = Access<T>::end(collection);     //unsigned int

  for (auto i = begin; i != end; i++)              //unsigned int
  {
    for (auto j = begin; j != end; j++)            //unsigned int
    {
      typename T::value_type& a = Access<T>::element(collection, i);
      typename T::value_type& b = Access<T>::element(collection, j);

      if (compare(a, b)) {
        std::swap(a, b);
      }
    }
  }
}

#endif
