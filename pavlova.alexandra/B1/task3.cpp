#include <vector>
#include "output.hpp"

void task3()
{
  int current = -1;
  std::vector<int> vector;

  while (std::cin >> current)
  {
    if (current == 0)
    {
      break;
    }
    vector.push_back(current);
  }
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Input failed \n");
  }
  if (vector.empty())
  {
    return;
  }
  if (current != 0)
  {
    throw std::runtime_error("No zero \n");
  }

  auto it = vector.end() - 1; //__gnu_cxx::__normal_iterator<int*, std::vector<int>>
  switch (* it)
  {
    case 1:
      for (it = vector.begin(); it != vector.end();)
      {
        it = (*it % 2 == 0) ? vector.erase(it--) : ++it;
      }
      break;

    case 2:
      for (it = vector.begin(); it != vector.end();)
      {
        it = (*it % 3 == 0) ? (vector.insert(++it, 3, 1) + 2) : ++it;
      }
      break;

    default:
      break;
  }
  print(vector);
}
