#include <fstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <vector>

const size_t InitialSize = 1024;

void task2(const char * filename)
{
  size_t size = InitialSize;
  std::ifstream inputFile(filename);

  if (!inputFile)
  {
    throw std::ios_base::failure("Failed to open file.");
  }

  using charArray = std::unique_ptr <char[], decltype(free)*>;
  charArray arr(static_cast <char*>(malloc(InitialSize)), &free);

  int i = 0;
  while (!inputFile.eof())
  {
    inputFile.read(&arr[i], InitialSize);
    i += inputFile.gcount();

    if (inputFile.gcount() == InitialSize)
    {
      size += InitialSize;

      charArray temp(static_cast <char*> (realloc(arr.get(), size)), &free);

      if (temp)
      {
        arr.release();
        std::swap(arr, temp);
      }
      else
      {
        throw std::runtime_error("Failed to reallocate.");
      }
    }

    if (inputFile.fail() && !inputFile.eof())
    {
      throw std::runtime_error("Failed to read");
    }

  }

  std::vector<char> vector(&arr[0], &arr[i]);

  for (const auto& arrayOut : vector) //char
  {
    std::cout << arrayOut;
  }
}
