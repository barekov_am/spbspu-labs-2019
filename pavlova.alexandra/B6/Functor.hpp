#ifndef B6_FUNCTOR_HPP
#define B6_FUNCTOR_HPP

class Functor
{
public:
  Functor();
  void operator()(int num);
  void print();

private:
  int max_, min_;
  double mean_;
  int count_, posit_, negat_;
  long int oddSum_, evenSum_;
  int first_, last_;
};

#endif
