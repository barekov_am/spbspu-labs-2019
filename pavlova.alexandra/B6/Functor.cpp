#include <iostream>
#include "Functor.hpp"

Functor::Functor():
  max_(0),
  min_(0),
  mean_(0),
  count_(0),
  posit_(0),
  negat_(0),
  oddSum_(0),
  evenSum_(0),
  first_(0),
  last_(0)
{}

void Functor::operator()(int num)
{
  if (count_ == 0)
  {
    max_ = num;
    min_ = num;
    first_ = num;
  }
  count_++;

  if (num > max_)
  {
    max_ = num;
  }
  else if (num < min_)
  {
    min_ = num;
  }

  if (num > 0)
  {
    posit_++;
  }
  else if (num < 0)
  {
    negat_++;
  }

  if (num % 2 != 0)
  {
    oddSum_ += num;
  }
  else
  {
    evenSum_ += num;
  }
  mean_ = static_cast< double >(oddSum_ + evenSum_) / (count_);
  last_ = num;
}

void Functor::print()
{
  if (count_ == 0)
  {
    std::cout << "No Data" << std::endl;
  }
  else
  {
    std::cout << "Max: " << max_ << std::endl;
    std::cout << "Min: " << min_ << std::endl;
    std::cout << "Mean: " << mean_ << std::endl;
    std::cout << "Positive: " << posit_ << std::endl;
    std::cout << "Negative: " << negat_ << std::endl;
    std::cout << "Odd Sum: " << oddSum_ << std::endl;
    std::cout << "Even Sum: " << evenSum_ << std::endl;
    if (first_ == last_)
    {
      std::cout << "First/Last Equal: yes" << std::endl;
    }
    else
    {
      std::cout << "First/Last Equal: no" << std::endl;
    }
  }
}
