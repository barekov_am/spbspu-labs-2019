#include <iostream>
#include <vector>
#include <algorithm>
#include "Functor.hpp"

int main()
{
  try
  {
    std::vector< int > vector;
    int value = 0;
    while (std::cin >> value, !std::cin.eof())
    {
      if (std::cin.fail())
      {
        throw std::runtime_error("Invalid read");
      }
      vector.push_back(value);
    }

    Functor functor = std::for_each(vector.begin(), vector.end(), Functor());

    if (!std::cin.eof())
    {
      throw std::invalid_argument("Invalid input");
    }

    functor.print();
  }
  catch (const std::invalid_argument & err)
  {
    std::cerr << err.what() << "\n";
    return 1;
  }
  catch (const std::exception & err)
  {
    std::cerr << err.what() << "\n";
    return 2;
  }

  return 0;
}
