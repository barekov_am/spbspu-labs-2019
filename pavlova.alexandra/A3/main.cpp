#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

void writeArea(const pavlova::Shape &shape)
{
  std::cout << "Area = " << shape.getArea() << "\n";
}

void writeCenter(const pavlova::Shape &shape)
{
  std::cout << "Center (" << shape.getFrameRect().pos.x
            << ", " << shape.getFrameRect().pos.y << ")" << "\n";
}

void writeFrameRect(const pavlova::Shape &shape)
{
  pavlova::rectangle_t frameR = shape.getFrameRect();
  std::cout << "Frame: " << frameR.height
            << "(height), " << frameR.width
            << "(width), (" << frameR.pos.x
            << ", " << frameR.pos.y
            << ")" << "\n";
}

int main()
{
  pavlova::Circle circle(3.5, {6.1, 6.1});
  std::cout << "Circle:" << "\n";
  writeFrameRect(circle);
  writeCenter(circle);
  writeArea(circle);

  std::cout << "Scaling" << "\n";
  circle.scale(2);
  std::cout << "New area: " << circle.getArea() << "\n";
  std::cout << "\n";

  std::cout << "Rotation on 45 degrees" << "\n";
  circle.rotate(45);
  writeFrameRect(circle);
  std::cout << "\n";

  pavlova::Rectangle rectangle(4.15, 4.15, {5, 5});
  std::cout << "Rectangle:" << "\n";
  writeFrameRect(rectangle);
  writeCenter(rectangle);
  writeArea(rectangle);

  std::cout << "Scaling" << "\n";
  rectangle.scale(2);
  std::cout << "New area: " << rectangle.getArea() << "\n";
  std::cout << "\n";

  std::cout << "Rotation on 45 degrees" << "\n";
  rectangle.rotate(45);
  writeFrameRect(rectangle);
  std::cout << "\n";

  pavlova::CompositeShape composition{};
  std::cout << "CompositeShape:" << "\n";
  composition.addShape(std::make_shared<pavlova::Circle>(circle));
  composition.addShape(std::make_shared<pavlova::Rectangle>(rectangle));
  writeFrameRect(composition);
  writeCenter(composition);
  writeArea(composition);

  std::cout << "Scaling" << "\n";
  composition.scale(2);
  writeFrameRect(composition);
  writeCenter(composition);
  writeArea(composition);
  std::cout << "\n";

  composition.deleteShape(1);
  writeFrameRect(composition);
  writeCenter(composition);
  writeArea(composition);

  return 0;
}
