#ifndef B4_TASK_HPP
#define B4_TASK_HPP

#include <string>
#include <vector>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

struct compare
{
  bool operator()(const DataStruct & data1, const DataStruct & data2)
  {
    if (data1.key1 < data2.key1)
    {
      return true;
    }
    if (data1.key1 == data2.key1)
    {
      if (data1.key2 < data2.key2)
      {
        return true;
      }

      if (data1.key2 == data2.key2)
      {
        return data1.str.length() < data2.str.length();
      }
    }
    return false;
  }
};

DataStruct read(std::istream & input);
void sortVector(std::vector<DataStruct> & vector);
void printVector(const std::vector<DataStruct> & vector);

void task();

#endif
