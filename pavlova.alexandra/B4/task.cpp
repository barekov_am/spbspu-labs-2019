#include "task.hpp"
#include <iostream>
#include <algorithm>
#include <sstream>
#include <iterator>

const int MAX = 5;
const int MIN = -5;

DataStruct read(std::istream & input)
{
  if (input.eof())
  {
    throw std::invalid_argument("Invalid input\n");
  }

  int k1, k2;
  input >> k1;
  if ((input.fail()) || (k1 > MAX) || (k1 < MIN) || (input.eof()) || (input.get() != ',') || (input.peek() == '\n'))
  {
    throw std::invalid_argument("Wrong input(key1)\n");
  }
  input >> k2;
  if ((input.fail()) || (k2 > MAX) || (k2 < MIN) || (input.eof()) || (input.get() != ',') || (input.peek() == '\n'))
  {
    throw std::invalid_argument("Wrong input(key1)\n");
  }

  std::string str;
  std::getline(input >> std::ws, str);
  if (input.fail())
  {
    throw std::invalid_argument("Wrong input(str)\n");
  }

  DataStruct datastruct;
  datastruct.key1 = k1;
  datastruct.key2 = k2;
  datastruct.str = str;
  return datastruct;
}

void sortVector(std::vector<DataStruct> & vector)
{
  std::sort(vector.begin(), vector.end(), compare());
}

void printVector(const std::vector<DataStruct> & vector)
{
  DataStruct datastruct;
  for (auto i = vector.begin(); i < vector.end(); i++)
  {
    datastruct = *i;
    std::cout << datastruct.key1 << "," << datastruct.key2 << "," << datastruct.str << "\n";
  }
}

void task()
{
  std::vector<DataStruct> vector;
  std::string line;

  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Invalid input\n");
    }
    std::stringstream stream(line);
    DataStruct dataStruct = read(stream);
    vector.push_back(dataStruct);
  }

  sortVector(vector);
  printVector(vector);
}
