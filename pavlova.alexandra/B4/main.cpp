#include <iostream>
#include "task.hpp"

int main()
{
  try
  {
    task();
  }
  catch (const std::invalid_argument & err)
  {
    std::cerr << err.what() << "\n";
    return 1;
  }
  catch (const std::exception & err)
  {
    std::cerr << err.what() << "\n";
    return 2;
  }

  return 0;
}
