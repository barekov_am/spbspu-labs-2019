#include <boost/test/auto_unit_test.hpp>

#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

const double EPSILON = 0.01;

BOOST_AUTO_TEST_SUITE(testPartition)

  BOOST_AUTO_TEST_CASE(intersectCorrectness)
  {
    pavlova::Circle testCircle(3, {2, 0});
    pavlova::Rectangle testRectangle(9, 5, {2, 4});
    pavlova::Rectangle testSquare(1, 1, {12, 21});

    BOOST_CHECK(pavlova::isIntersected(testCircle.getFrameRect(),testRectangle.getFrameRect()));
    BOOST_CHECK(!(pavlova::isIntersected(testSquare.getFrameRect(),testRectangle.getFrameRect())));
    BOOST_CHECK(!(pavlova::isIntersected(testCircle.getFrameRect(),testSquare.getFrameRect())));
  }

  BOOST_AUTO_TEST_CASE(partitionCorrectness)
  {
    pavlova::Shape::ptr testCircle = std::make_shared<pavlova::Circle>(2, pavlova::point_t {1, 1});
    pavlova::Shape::ptr testRectangle1 = std::make_shared<pavlova::Rectangle>(5, 4, pavlova::point_t {3, -1});
    pavlova::Shape::ptr testSquare1 = std::make_shared<pavlova::Rectangle>(4, 4, pavlova::point_t {6, 15});
    pavlova::Shape::ptr testRectangle2 = std::make_shared<pavlova::Rectangle>(2, 3, pavlova::point_t {10, 2});
    pavlova::Shape::ptr testRectangle3 = std::make_shared<pavlova::Rectangle>(6, 30, pavlova::point_t {8, 15});
    pavlova::Shape::ptr testSquare2 = std::make_shared<pavlova::Rectangle>(5, 5, pavlova::point_t {-20, 15});

    pavlova::CompositeShape testComposition;
    testComposition.addShape(testCircle);
    testComposition.addShape(testRectangle1);
    testComposition.addShape(testSquare1);
    testComposition.addShape(testRectangle2);
    testComposition.addShape(testRectangle3);
    testComposition.addShape(testSquare2);

    pavlova::Matrix testMatrix = pavlova::part(testComposition);

    const std::size_t correctRows = 3;
    const std::size_t correctColumns = 4;

    BOOST_CHECK_EQUAL(testMatrix.getRows(), correctRows);
    BOOST_CHECK_EQUAL(testMatrix.getColumns(), correctColumns);
    BOOST_CHECK(testMatrix[0][0] == testCircle);
    BOOST_CHECK(testMatrix[0][1] == testSquare1);
    BOOST_CHECK(testMatrix[0][2] == testRectangle2);
    BOOST_CHECK(testMatrix[0][3] == testSquare2);
    BOOST_CHECK(testMatrix[1][0] == testRectangle1);
    BOOST_CHECK(testMatrix[2][0] == testRectangle3);
  }

BOOST_AUTO_TEST_SUITE_END()

