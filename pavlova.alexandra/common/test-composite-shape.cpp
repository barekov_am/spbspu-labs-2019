#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(testCompositeShape)

  const double EPSILON = 0.01;

  BOOST_AUTO_TEST_CASE(constCompositeAfterMove)
  {
    pavlova::Shape::ptr circle_ = std::make_shared<pavlova::Circle>(3, pavlova::point_t {6, 6});
    pavlova::Rectangle rectangle(3, 5, {6, 6});
    pavlova::Shape::ptr rectangle_ = std::make_shared<pavlova::Rectangle>(3, 5, pavlova::point_t {6, 6});
    pavlova::CompositeShape composition_(circle_);
    composition_.addShape(rectangle_);

    pavlova::rectangle_t rectangleBefore = composition_.getFrameRect();
    double areaBefore = composition_.getArea();

    composition_.move(4, 9);
    BOOST_CHECK_CLOSE(rectangleBefore.height, composition_.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(rectangleBefore.width, composition_.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, composition_.getArea(), EPSILON);
    composition_.move({2, 2});
    BOOST_CHECK_CLOSE(rectangleBefore.height, composition_.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(rectangleBefore.width, composition_.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, composition_.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(changeCompositionAreaAfterScale)
  {
    pavlova::Shape::ptr circle_ = std::make_shared<pavlova::Circle>(3, pavlova::point_t {6, 6});
    pavlova::Rectangle rectangle(3, 5, {6, 6});
    pavlova::Shape::ptr rectangle_ = std::make_shared<pavlova::Rectangle>(3, 5, pavlova::point_t {6, 6});
    pavlova::CompositeShape composition_(circle_);
    composition_.addShape(rectangle_);

    double areaBefore = composition_.getArea();

    double coeff = 6;
    composition_.scale(coeff);
    BOOST_CHECK_CLOSE(coeff * coeff, composition_.getArea() / areaBefore, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(incorrectComposition)
  {
    pavlova::Shape::ptr circle_ = std::make_shared<pavlova::Circle>(3, pavlova::point_t {6, 6});
    pavlova::Rectangle rectangle(3, 5, {6, 6});
    pavlova::Shape::ptr rectangle_ = std::make_shared<pavlova::Rectangle>(3, 5, pavlova::point_t {6, 6});
    pavlova::CompositeShape composition_(circle_);

    BOOST_CHECK_THROW(composition_.scale(0), std::invalid_argument);
    BOOST_CHECK_THROW(composition_.addShape(nullptr), std::invalid_argument);
    BOOST_CHECK_THROW(composition_.deleteShape(5), std::invalid_argument);
    BOOST_CHECK_THROW(composition_[-5], std::out_of_range);
    BOOST_CHECK_THROW(composition_[5], std::out_of_range);

    double firstArea = composition_.getArea();
    composition_.addShape(rectangle_);

    BOOST_CHECK_CLOSE(composition_.getArea(), firstArea + rectangle.getArea(), EPSILON);

    double secondArea = composition_.getArea();
    composition_.deleteShape(1);

    BOOST_CHECK_CLOSE(composition_.getArea(), secondArea - rectangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(constructorCopyComposition)
  {
    pavlova::Shape::ptr circle_ = std::make_shared<pavlova::Circle>(3, pavlova::point_t {6, 6});
    pavlova::Rectangle rectangle(3, 5, {6, 6});
    pavlova::Shape::ptr rectangle_ = std::make_shared<pavlova::Rectangle>(3, 5, pavlova::point_t {6, 6});
    pavlova::CompositeShape composition_(circle_);
    composition_.addShape(rectangle_);

    pavlova::rectangle_t firstFrameRect = composition_.getFrameRect();
    double firstArea = composition_.getArea();

    pavlova::CompositeShape copyComposition(composition_);

    BOOST_CHECK_CLOSE(firstFrameRect.pos.x, copyComposition.getFrameRect().pos.x, EPSILON);
    BOOST_CHECK_CLOSE(firstFrameRect.pos.y, copyComposition.getFrameRect().pos.y, EPSILON);
    BOOST_CHECK_CLOSE(firstFrameRect.height, copyComposition.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(firstFrameRect.width, copyComposition.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(firstArea, copyComposition.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(constructorMoveComposition)
  {
    pavlova::Shape::ptr circle_ = std::make_shared<pavlova::Circle>(3, pavlova::point_t {6, 6});
    pavlova::Rectangle rectangle(3, 5, {6, 6});
    pavlova::Shape::ptr rectangle_ = std::make_shared<pavlova::Rectangle>(3, 5, pavlova::point_t {6, 6});
    pavlova::CompositeShape composition_(circle_);
    composition_.addShape(rectangle_);

    pavlova::rectangle_t firstFrameRect = composition_.getFrameRect();
    double firstArea = composition_.getArea();

    pavlova::CompositeShape moveComposition(std::move(composition_));

    BOOST_CHECK_CLOSE(firstFrameRect.pos.x, moveComposition.getFrameRect().pos.x, EPSILON);
    BOOST_CHECK_CLOSE(firstFrameRect.pos.y, moveComposition.getFrameRect().pos.y, EPSILON);
    BOOST_CHECK_CLOSE(firstFrameRect.height, moveComposition.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(firstFrameRect.width, moveComposition.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(firstArea, moveComposition.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(operatorCopyComposition)
  {
    pavlova::Shape::ptr circle_ = std::make_shared<pavlova::Circle>(3, pavlova::point_t {6, 6});
    pavlova::Rectangle rectangle(3, 5, {6, 6});
    pavlova::Shape::ptr rectangle_ = std::make_shared<pavlova::Rectangle>(3, 5, pavlova::point_t {6, 6});
    pavlova::CompositeShape composition_(circle_);
    composition_.addShape(rectangle_);

    pavlova::rectangle_t firstFrameRect = composition_.getFrameRect();
    double firstArea = composition_.getArea();

    pavlova::CompositeShape copyComposition;
    copyComposition = composition_;

    BOOST_CHECK_CLOSE(firstFrameRect.pos.x, copyComposition.getFrameRect().pos.x, EPSILON);
    BOOST_CHECK_CLOSE(firstFrameRect.pos.y, copyComposition.getFrameRect().pos.y, EPSILON);
    BOOST_CHECK_CLOSE(firstFrameRect.height, copyComposition.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(firstFrameRect.width, copyComposition.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(firstArea, copyComposition.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(operatorMoveComposition)
  {
    pavlova::Shape::ptr circle_ = std::make_shared<pavlova::Circle>(3, pavlova::point_t {6, 6});
    pavlova::Rectangle rectangle(3, 5, {6, 6});
    pavlova::Shape::ptr rectangle_ = std::make_shared<pavlova::Rectangle>(3, 5, pavlova::point_t {6, 6});
    pavlova::CompositeShape composition_(circle_);
    composition_.addShape(rectangle_);

    pavlova::rectangle_t firstFrameRect = composition_.getFrameRect();
    double firstArea = composition_.getArea();

    pavlova::CompositeShape moveComposition;
    moveComposition = composition_;

    BOOST_CHECK_CLOSE(firstFrameRect.pos.x, moveComposition.getFrameRect().pos.x, EPSILON);
    BOOST_CHECK_CLOSE(firstFrameRect.pos.y, moveComposition.getFrameRect().pos.y, EPSILON);
    BOOST_CHECK_CLOSE(firstFrameRect.height, moveComposition.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(firstFrameRect.width, moveComposition.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(firstArea, moveComposition.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(constCompositionAfterRotation)
  {
    pavlova::Shape::ptr circle_ = std::make_shared<pavlova::Circle>(3, pavlova::point_t {6, 6});
    pavlova::Rectangle rectangle(3, 5, {6, 6});
    pavlova::Shape::ptr rectangle_ = std::make_shared<pavlova::Rectangle>(3, 5, pavlova::point_t {6, 6});
    pavlova::CompositeShape composition_(circle_);
    composition_.addShape(rectangle_);

    pavlova::rectangle_t rectangleBefore = composition_.getFrameRect();
    double areaBefore = composition_.getArea();
    double angle = 360;
    composition_.rotate(angle);

    BOOST_CHECK_CLOSE(composition_.getFrameRect().height, rectangleBefore.height, EPSILON);
    BOOST_CHECK_CLOSE(composition_.getFrameRect().width, rectangleBefore.width, EPSILON);
    BOOST_CHECK_CLOSE(composition_.getFrameRect().pos.x, rectangleBefore.pos.x, EPSILON);
    BOOST_CHECK_CLOSE(composition_.getFrameRect().pos.y, rectangleBefore.pos.y, EPSILON);
    BOOST_CHECK_CLOSE(composition_.getArea(), areaBefore, EPSILON);
  }

BOOST_AUTO_TEST_SUITE_END()
