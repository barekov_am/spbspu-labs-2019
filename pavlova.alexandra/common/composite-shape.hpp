#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace pavlova
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&);
    CompositeShape(const Shape::ptr &shape);

    ~CompositeShape() override = default;

    CompositeShape &operator=(const CompositeShape &);
    CompositeShape &operator=(CompositeShape &&);
    Shape::ptr operator [](int) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(double dx, double dy) override;
    void scale(double coeff) override;
    void rotate(double degrees) override;
    size_t getSize() const;

    void addShape(const Shape::ptr &shape);
    void deleteShape(int index);
  private:
    int count_;
    Shape::array shapeArray_;
  };
}

#endif
