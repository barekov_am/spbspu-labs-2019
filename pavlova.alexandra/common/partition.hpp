#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace pavlova
{
  pavlova::Matrix part(const pavlova::CompositeShape &composition);
}

#endif //PARTITION_HPP
