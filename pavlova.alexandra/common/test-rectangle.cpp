#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testRectangle)

  const double EPSILON = 0.01;

  BOOST_AUTO_TEST_CASE(constRectangleAfterMove)
  {
    pavlova::Rectangle rectangle(3, 5, {6, 6});
    pavlova::rectangle_t rectangleBefore = rectangle.getFrameRect();
    double areaBefore = rectangle.getArea();

    rectangle.move(4, 9);
    BOOST_CHECK_CLOSE(rectangleBefore.height, rectangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(rectangleBefore.width, rectangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), EPSILON);
    rectangle.move({2, 2});
    BOOST_CHECK_CLOSE(rectangleBefore.height, rectangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(rectangleBefore.width, rectangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(changeRectangleAreaAfterScale)
  {
    pavlova::Rectangle rectangle(3, 5, {6, 6});
    double coeff = 6;
    double areaBefore = rectangle.getArea();
    rectangle.scale(coeff);
    BOOST_CHECK_CLOSE(coeff * coeff, rectangle.getArea() / areaBefore, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(incorrectRectangle)
  {
    BOOST_CHECK_THROW(pavlova::Rectangle(-3, 5, {6, 6}), std::invalid_argument);
    BOOST_CHECK_THROW(pavlova::Rectangle(3, -5, {6, 6}), std::invalid_argument);
    pavlova::Rectangle rectangle(3, 5, {6, 6});
    BOOST_CHECK_THROW(rectangle.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(constRectangleAfterRotation)
  {
    pavlova::Rectangle rectangle(3, 5, {6, 6});
    pavlova::rectangle_t rectangleBefore = rectangle.getFrameRect();
    double areaBefore = rectangle.getArea();
    double angle = 180;

    rectangle.rotate(angle);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, rectangleBefore.height, EPSILON);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, rectangleBefore.width, EPSILON);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.x, rectangleBefore.pos.x, EPSILON);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.y, rectangleBefore.pos.y, EPSILON);
    BOOST_CHECK_CLOSE(rectangle.getArea(), areaBefore, EPSILON);
  }

BOOST_AUTO_TEST_SUITE_END()
