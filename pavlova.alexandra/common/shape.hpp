#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace pavlova
{
  class Shape
  {
  public:
    using ptr = std::shared_ptr<Shape>;
    using array = std::unique_ptr<ptr[]>;

    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t &point) = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void scale(double coeff) = 0;
    virtual void rotate(double degrees) = 0;
  };
}

#endif
