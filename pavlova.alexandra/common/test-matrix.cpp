#include <boost/test/auto_unit_test.hpp>

#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "partition.hpp"

const double EPSILON = 0.01;

BOOST_AUTO_TEST_SUITE(testMatrix)

  BOOST_AUTO_TEST_CASE(testCopyConstructor)
  {
    pavlova::Shape::ptr testCircle = std::make_shared<pavlova::Circle>(3, pavlova::point_t {6, 6});
    pavlova::Shape::ptr testRectangle = std::make_shared<pavlova::Rectangle>(3, 5, pavlova::point_t {6, 6});
    pavlova::CompositeShape testComposition;

    testComposition.addShape(testCircle);
    testComposition.addShape(testRectangle);

    pavlova::Matrix testMatrix = pavlova::part(testComposition);
    pavlova::Matrix testMatrixCopy(testMatrix);

    BOOST_CHECK(testMatrix == testMatrixCopy);
    BOOST_CHECK_EQUAL(testMatrixCopy.getRows(), testMatrix.getRows());
    BOOST_CHECK_EQUAL(testMatrixCopy.getColumns(), testMatrix.getColumns());
  }

  BOOST_AUTO_TEST_CASE(testMoveConstructor)
  {
    pavlova::Shape::ptr testCircle = std::make_shared<pavlova::Circle>(3, pavlova::point_t {6, 6});
    pavlova::Shape::ptr testRectangle = std::make_shared<pavlova::Rectangle>(3, 5, pavlova::point_t {6, 6});
    pavlova::CompositeShape testComposition;

    testComposition.addShape(testCircle);
    testComposition.addShape(testRectangle);

    pavlova::Matrix testMatrix = pavlova::part(testComposition);
    pavlova::Matrix testMatrixCopy(testMatrix);
    pavlova::Matrix testMatrixMove = std::move(testMatrix);

    BOOST_CHECK(testMatrixMove == testMatrixCopy);
    BOOST_CHECK_EQUAL(testMatrixMove.getRows(), testMatrixCopy.getRows());
    BOOST_CHECK_EQUAL(testMatrixMove.getColumns(), testMatrixCopy.getColumns());;
  }

  BOOST_AUTO_TEST_CASE(testCopyOperator)
  {
    pavlova::Shape::ptr testCircle = std::make_shared<pavlova::Circle>(3, pavlova::point_t {6, 6});
    pavlova::Shape::ptr testRectangle = std::make_shared<pavlova::Rectangle>(3, 5, pavlova::point_t {6, 6});
    pavlova::CompositeShape testComposition;

    testComposition.addShape(testCircle);
    testComposition.addShape(testRectangle);

    pavlova::Matrix testMatrix = pavlova::part(testComposition);
    pavlova::Matrix testMatrixCopy;
    testMatrixCopy = testMatrix;

    BOOST_CHECK(testMatrixCopy == testMatrix);
    BOOST_CHECK_EQUAL(testMatrixCopy.getRows(), testMatrix.getRows());
    BOOST_CHECK_EQUAL(testMatrixCopy.getColumns(), testMatrix.getColumns());;
  }

  BOOST_AUTO_TEST_CASE(testMoveOperator)
  {
    pavlova::Shape::ptr testCircle = std::make_shared<pavlova::Circle>(3, pavlova::point_t {6, 6});
    pavlova::Shape::ptr testRectangle = std::make_shared<pavlova::Rectangle>(3, 5, pavlova::point_t {6, 6});
    pavlova::CompositeShape testComposition;

    testComposition.addShape(testCircle);
    testComposition.addShape(testRectangle);

    pavlova::Matrix testMatrix = pavlova::part(testComposition);
    pavlova::Matrix testMatrixCopy(testMatrix);
    pavlova::Matrix testMatrixMove;
    testMatrixMove = std::move(testMatrix);

    BOOST_CHECK(testMatrixMove == testMatrixCopy);
    BOOST_CHECK_EQUAL(testMatrixMove.getRows(), testMatrixCopy.getRows());
    BOOST_CHECK_EQUAL(testMatrixMove.getColumns(), testMatrixCopy.getColumns());;
  }

  BOOST_AUTO_TEST_CASE(exceptOutOfRange)
  {
    pavlova::Shape::ptr testCircle = std::make_shared<pavlova::Circle>(3, pavlova::point_t {6, 6});
    pavlova::Shape::ptr testRectangle = std::make_shared<pavlova::Rectangle>(3, 5, pavlova::point_t {6, 6});
    pavlova::CompositeShape testComposition;

    testComposition.addShape(testCircle);
    testComposition.addShape(testRectangle);

    pavlova::Matrix testMatrix = pavlova::part(testComposition);

    BOOST_CHECK_THROW(testMatrix[105], std::out_of_range);
    BOOST_CHECK_THROW(testMatrix[-2], std::out_of_range);
  }

BOOST_AUTO_TEST_SUITE_END()

