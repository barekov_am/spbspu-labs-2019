#include <algorithm>
#include <stdexcept>
#include <math.h>
#include "composite-shape.hpp"

pavlova::CompositeShape::CompositeShape():
  count_(0),
  shapeArray_(nullptr)
{}

pavlova::CompositeShape::CompositeShape(const pavlova::CompositeShape &rhs):
  count_(rhs.count_),
  shapeArray_(std::make_unique<pavlova::Shape::ptr[]>(rhs.count_))
{
  for (int i = 0; i < count_; i++)
  {
    shapeArray_[i] = rhs.shapeArray_[i];
  }
}

pavlova::CompositeShape::CompositeShape(pavlova::CompositeShape &&rhs):
  count_(rhs.count_),
  shapeArray_(std::move(rhs.shapeArray_))
{
  rhs.count_ = 0;
  rhs.shapeArray_ = nullptr;
}

pavlova::CompositeShape::CompositeShape(const Shape::ptr &shape):
  CompositeShape()
{
  addShape(shape);
}

pavlova::CompositeShape& pavlova::CompositeShape::operator=(const pavlova::CompositeShape &rhs)
{
  if (this != &rhs)
  {
    shapeArray_ = std::make_unique<pavlova::Shape::ptr[]>(rhs.count_);
    count_ = rhs.count_;
    for (int i = 0; i < count_; i++)
    {
      shapeArray_[i] = rhs.shapeArray_[i];
    }
  }
  return *this;
}

pavlova::CompositeShape& pavlova::CompositeShape::operator=(pavlova::CompositeShape &&rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    shapeArray_ = std::move(rhs.shapeArray_);
    rhs.count_ = 0;
    rhs.shapeArray_ = nullptr;
  }
  return *this;
}

pavlova::Shape::ptr pavlova::CompositeShape::operator [](int index) const
{
  if ((index < 0) || (index >= count_))
  {
    throw std::out_of_range("Index is negative, zero or out of range");
  }
  return shapeArray_[index];
}


double pavlova::CompositeShape::getArea() const
{
  if (count_ == 0)
  {
    throw std::logic_error("CompositeShape is null");
  }

  double compositeShapeArea = 0;
  for (int i = 0; i < count_; i++)
  {
    compositeShapeArea += shapeArray_[i]->getArea();
  }
  return compositeShapeArea;
}

pavlova::rectangle_t pavlova::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("CompositeShape is null");
  }

  pavlova::rectangle_t Rect = shapeArray_[0]->getFrameRect();
  double minX = Rect.pos.x - Rect.width / 2;
  double minY = Rect.pos.y - Rect.height / 2;
  double maxX = Rect.pos.x + Rect.width / 2;
  double maxY = Rect.pos.y + Rect.height / 2;
  for (int i = 1; i < count_; i++)
  {
    Rect = shapeArray_[i]->getFrameRect();
    double value = Rect.pos.x - Rect.width / 2;
    minX = std::min(value, minX);
    value = Rect.pos.y - Rect.height / 2;
    minY = std::min(value, minY);
    value = Rect.pos.x + Rect.width / 2;
    maxX = std::min(value, maxX);
    value = Rect.pos.y + Rect.height / 2;
    maxY = std::min(value, maxY);
  }
  return{(maxX - minX), (maxY - minY), {(maxX + minX) / 2, (maxY + minY) / 2}};
}

void pavlova::CompositeShape::move(const pavlova::point_t &point)
{
  if (count_ == 0)
  {
    throw std::logic_error("CompositeShape is null");
  }

  point_t frameRectPos = getFrameRect().pos;
  double dx = point.x - frameRectPos.x;
  double dy = point.y - frameRectPos.y;
  move(dx, dy);
}

void pavlova::CompositeShape::move(double dx, double dy)
{
  if (count_ == 0)
  {
    throw std::logic_error("CompositeShape is null");
  }

  for (int i = 0; i < count_; i++)
  {
    shapeArray_[i]->move(dx, dy);
  }
}

void pavlova::CompositeShape::scale(double coeff)
{
  if (coeff <= 0)
  {
    throw std::invalid_argument("Coeff is negative or zero");
  }

  pavlova::point_t frameRectPos = getFrameRect().pos;
  for (int i = 0; i < count_; i++)
  {
    pavlova::point_t shapeCenter = shapeArray_[i]->getFrameRect().pos;
    double dx = (shapeCenter.x - frameRectPos.x) * (coeff - 1);
    double dy = (shapeCenter.y - frameRectPos.y) * (coeff - 1);
    shapeArray_[i]->move(dx, dy);
    shapeArray_[i]->scale(coeff);
  }
}

void pavlova::CompositeShape::rotate(double degrees)
{
  const double cos = std::cos((2 * M_PI * degrees) / 360);
  const double sin = std::sin((2 * M_PI * degrees) / 360);

  const point_t compCentre = getFrameRect().pos;

  for (int i = 0; i < count_; i++)
  {
    const point_t shapeCentre = shapeArray_[i]->getFrameRect().pos;
    const double dx = (shapeCentre.x - compCentre.x) * cos - (shapeCentre.y - compCentre.y) * sin;
    const double dy = (shapeCentre.x - compCentre.x) * sin + (shapeCentre.y - compCentre.y) * cos;

    shapeArray_[i]->move({ compCentre.x + dx, compCentre.y + dy });
    shapeArray_[i]->rotate(degrees);
  }
}

size_t pavlova::CompositeShape::getSize() const
{
  return(count_);
}

void pavlova::CompositeShape::addShape(const Shape::ptr &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("New shape is empty");
  }

  Shape::array shapesArray = std::make_unique<Shape::ptr[]>(count_ + 1);
  for (int i = 0; i < count_; i++)
  {
    shapesArray[i] = shapeArray_[i];
  }
  shapesArray[count_] = shape;
  count_++;
  shapeArray_ = std::move(shapesArray);
}

void pavlova::CompositeShape::deleteShape(int index)
{
  if ((index < 0) || (index >= count_))
  {
    throw std::invalid_argument("Index is negative or zero");
  }

  count_--;
  for (int i = index; i < count_; i++)
  {
    shapeArray_[i] = shapeArray_[i + 1];
  }
  shapeArray_[count_] = nullptr;
}
