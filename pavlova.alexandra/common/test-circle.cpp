#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testCircle)

  const double EPSILON = 0.01;

  BOOST_AUTO_TEST_CASE(constCircleAfterMove)
  {
    pavlova::Circle circle(3, {6, 6});
    pavlova::rectangle_t circleBefore = circle.getFrameRect();
    double areaBefore = circle.getArea();

    circle.move(4, 9);
    BOOST_CHECK_CLOSE(circleBefore.height, circle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(circleBefore.width, circle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), EPSILON);
    circle.move({2, 2});
    BOOST_CHECK_CLOSE(circleBefore.height, circle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(circleBefore.width, circle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(changeCircleAreaAfterScale)
  {
    pavlova::Circle circle(3, {6, 6});
    double coeff = 6;
    double areaBefore = circle.getArea();
    circle.scale(coeff);
    BOOST_CHECK_CLOSE(coeff * coeff, circle.getArea() / areaBefore, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(incorrectCircle)
  {
    BOOST_CHECK_THROW(pavlova::Circle(-3, {6, 6}), std::invalid_argument);
    pavlova::Circle circle(3, {6, 6});
    BOOST_CHECK_THROW(circle.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(constCircleAfterRotation)
  {
    pavlova::Circle circle(3, {6, 6});
    pavlova::rectangle_t circleBefore = circle.getFrameRect();
    double areaBefore = circle.getArea();
    double angle = -100;

    circle.rotate(angle);
    BOOST_CHECK_CLOSE(circle.getFrameRect().height, circleBefore.height, EPSILON);
    BOOST_CHECK_CLOSE(circle.getFrameRect().width, circleBefore.width, EPSILON);
    BOOST_CHECK_CLOSE(circle.getFrameRect().pos.x, circleBefore.pos.x, EPSILON);
    BOOST_CHECK_CLOSE(circle.getFrameRect().pos.y, circleBefore.pos.y, EPSILON);
    BOOST_CHECK_CLOSE(circle.getArea(), areaBefore, EPSILON);
  }

BOOST_AUTO_TEST_SUITE_END()
