#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

namespace pavlova
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &newMatrix);
    Matrix(Matrix &&newMatrix);

    ~Matrix() = default;

    Matrix &operator =(const Matrix &newMatrix);
    Matrix &operator =(Matrix &&newMatrix);

    Shape::array operator [](std::size_t index) const;
    bool operator ==(const Matrix &newMatrix) const;
    bool operator !=(const Matrix &newMatrix) const;

    std::size_t getRows() const;
    std::size_t getColumns() const;
    std::size_t getLevelSize(size_t level) const;

    void add(const Shape::ptr &shape, std::size_t index);

  private:

    Shape::array data_;
    std::size_t rows_;
    std::size_t columns_;
  };
}

#endif //MATRIX_HPP
