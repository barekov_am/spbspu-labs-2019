#include <cassert>
#include <cmath>
#include <stdexcept>
#include "rectangle.hpp"

pavlova::Rectangle::Rectangle(double height, double  width, const point_t &center):
  height_(height),
  width_(width),
  rotationDegrees_(),
  center_(center)
{
  if ((height <= 0) || (width <= 0))
  {
    throw std::invalid_argument("Height or width is negative or zero");
  }
}

double pavlova::Rectangle::getArea() const
{
  return width_ * height_;
}

pavlova::rectangle_t pavlova::Rectangle::getFrameRect() const
{
  const double cos = std::cos((2 * M_PI * rotationDegrees_) / 360);
  const double sin = std::sin((2 * M_PI * rotationDegrees_) / 360);
  const double width = width_ * std::fabs(cos) + height_ * std::fabs(sin);
  const double height = height_ * std::fabs(cos) + width_ * std::fabs(sin);
  return {height, width, center_};
}

void pavlova::Rectangle::move(const point_t &point)
{
  center_ = point;
}

void pavlova::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void pavlova::Rectangle::scale(double coeff)
{
  if (coeff <= 0)
  {
    throw std::invalid_argument("Coeff is negative or zero");
  }
  else
  {
    height_ *= coeff;
    width_ *= coeff;
  }
}

void pavlova::Rectangle::rotate(double degrees)
{
  rotationDegrees_ += degrees;
  while (std::abs(rotationDegrees_) >= 360)
  {
    rotationDegrees_ = (degrees > 0) ? rotationDegrees_ - 360 : rotationDegrees_ + 360;
  }
}
