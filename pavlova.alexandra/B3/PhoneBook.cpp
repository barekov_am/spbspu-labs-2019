#include "PhoneBook.hpp"
#include <iostream>

bool PhoneBook::empty()
{
  return listPhoneBookEntry.empty();
}

PhoneBook::iterator PhoneBook::begin()
{
  return listPhoneBookEntry.begin();
}

PhoneBook::iterator PhoneBook::end()
{
  return listPhoneBookEntry.end();
}

PhoneBook::iterator PhoneBook::insert(iterator iter, PhoneBookEntry & entry)
{
  return listPhoneBookEntry.insert(iter, entry);
}

PhoneBook::iterator PhoneBook::move(iterator iter, int steps)
{
  int counter = 0;
  if (steps >= 0)
  {
    while ((std::next(iter) != listPhoneBookEntry.end()) && (counter != steps))
    {
      ++iter;
      ++counter;
    }
  }
  else
  {
    while ((iter != listPhoneBookEntry.begin()) && (counter != steps))
    {
      --iter;
      --counter;
    }
  }
  return iter;
}

PhoneBook::iterator PhoneBook::moveNext(iterator iter)
{
  return ++iter;
}

PhoneBook::iterator PhoneBook::movePrevious(iterator iter)
{
  return --iter;
}

PhoneBook::iterator PhoneBook::remove(iterator iter)
{
  return listPhoneBookEntry.erase(iter);
}

void PhoneBook::show(iterator iter)
{
  std::cout << (*iter).number << " " << (*iter).name << "\n";
}

PhoneBook::iterator PhoneBook::replace(iterator iter, PhoneBookEntry & entry)
{
  *iter = entry;
  return iter;
}

void PhoneBook::pushBack(PhoneBookEntry & entry)
{
  return listPhoneBookEntry.push_back(entry);
}
