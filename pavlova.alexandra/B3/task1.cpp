#include <string>
#include <iostream>
#include <sstream>
#include "PhonebookInterface.hpp"

std::string getName(std::string & name)
{
  if ((name.empty()) || (name.front() != '\"') || (name.back() != '\"'))
  {
    return "";
  }
  name.erase(name.begin());
  size_t i = 0;
  while ((i < name.size()) && (name[i] != '\"'))
  {
    if (name[i] == '\\')
    {
      if ((name[i + 1] == '\"') && (i + 2 < name.size()))
      {
        name.erase(i, 1);
      }
      else
      {
        return "";
      }
    }
    ++i;
  }

  if (i == name.size())
  {
    return "";
  }
  name.erase(i);

  if (name.empty())
  {
    return "";
  }

  return name;
}


std::string getNumber(std::string & number)
{
  if (number.empty())
  {
    return "";
  }

  for (size_t i = 0; i < number.size(); i++)
  {
    if (!std::isdigit(number[i]))
    {
      return "";
    }
  }

  return number;
}

std::string getMarkName(std::string & name)
{
  if (name.empty())
  {
    return "";
  }

  for (size_t i = 0; i < name.size(); i++)
  {
    if ((!isalnum(name[i])) && (name[i] != '-'))
    {
      return "";
    }
  }

  return name;
}

void add(PhonebookInterface & phonebookInter, std::stringstream & input)
{
  std::string number;
  std::string name;
  std::string line;

  input >> std::ws >> number;
  std::getline(input >> std::ws, name);

  number = getNumber(number);
  name = getName(name);

  if (name.empty() || number.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  PhoneBookEntry newrecord = {name, number};
  phonebookInter.add(newrecord);
}

void store(PhonebookInterface & phonebookInter, std::stringstream & input)
{
  std::string markName;
  std::string newMarkName;

  input >> std::ws >> markName;
  markName = getMarkName(markName);

  input >> std::ws >> newMarkName;
  newMarkName = getMarkName(newMarkName);

  if (markName.empty() || newMarkName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  phonebookInter.store(markName, newMarkName);
}


void insert(PhonebookInterface & phonebookInter, std::stringstream & input)
{
  std::string direction;
  std::string markName;
  std::string number;
  std::string name;

  input >> std::ws >> direction;
  input >> std::ws >> markName;

  markName = getMarkName(markName);

  input >> std::ws >> number;
  number = getNumber(number);

  std::getline(input >> std::ws, name);
  name = getName(name);

  PhonebookInterface::InsertPosition position;

  if (markName.empty() || number.empty() || name.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  PhoneBookEntry record = {name, number};

  if (direction == "before")
  {
    position = PhonebookInterface::InsertPosition::before;
    phonebookInter.insert(position, markName, record);
  }
  else if (direction == "after")
  {
    position = PhonebookInterface::InsertPosition::after;
    phonebookInter.insert(position, markName, record);
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
  }
}


void deleteRecord(PhonebookInterface & phonebookInter, std::stringstream & input)
{
  std::string markName;
  input >> std::ws >> markName;

  markName = getMarkName(markName);

  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  phonebookInter.deleteNote(markName);
}


void show(PhonebookInterface & phonebookInter, std::stringstream & input)
{
  std::string markName;
  input >> std::ws >> markName;

  markName = getMarkName(markName);

  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  phonebookInter.show(markName);
}


void move(PhonebookInterface & phonebookInter, std::stringstream & input)
{
  std::string markName;
  std::string num;

  input >> std::ws >> markName;
  markName = getMarkName(markName);

  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  input >> std::ws >> num;

  if (num == "first")
  {
    PhonebookInterface::MovePosition position = PhonebookInterface::MovePosition::first;
    phonebookInter.move(markName, position);
  }
  else if (num == "last")
  {
    PhonebookInterface::MovePosition position = PhonebookInterface::MovePosition::last;
    phonebookInter.move(markName, position);
  }
  else
  {
    int sign = 1;

    if (num[0] == '-')
    {
      sign = -1;
      num.erase(num.begin());
    }
    else if (num[0] == '+')
    {
      num.erase(num.begin());
    }

    num = getNumber(num);

    if (num.empty())
    {
      std::cout << "<INVALID STEP>\n";
      return;
    }

    phonebookInter.move(markName, (stoi(num) * sign));
  }
}

void task1()
{
  PhonebookInterface phonebookInter;
  std::string line;

  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Invalid reading");
    }
    std::stringstream input(line);
    std::string inputCommand;
    input >> inputCommand;

    if (inputCommand == "add")
    {
      add(phonebookInter, input);
    }
    else if (inputCommand == "store")
    {
      store(phonebookInter, input);
    }
    else if (inputCommand == "insert")
    {
      insert(phonebookInter, input);
    }
    else if (inputCommand == "delete")
    {
      deleteRecord(phonebookInter, input);
    }
    else if (inputCommand == "show")
    {
      show(phonebookInter, input);
    }
    else if (inputCommand == "move")
    {
      move(phonebookInter, input);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
