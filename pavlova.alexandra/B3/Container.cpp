#include "Container.hpp"

Container::FactorialIterator::FactorialIterator(size_t index):
  index_(index),
  value_(getValue(index))
{}

Container::FactorialIterator Container::begin()
{
  return FactorialIterator(1);
}

Container::FactorialIterator Container::end()
{
  return FactorialIterator(11);
}

bool Container::FactorialIterator::operator ==(const Container::FactorialIterator & other) const
{
  return ((value_ == other.value_) && (index_ == other.index_));
}

bool Container::FactorialIterator::operator !=(const Container::FactorialIterator & other) const
{
  return (!(* this == other));
}

unsigned long long & Container::FactorialIterator::operator *()
{
  return value_;
}

unsigned long long * Container::FactorialIterator::operator ->()
{
  return & value_;
}

Container::FactorialIterator & Container::FactorialIterator::operator ++()
{
  ++index_;
  value_ *= index_;
  return * this;
}

Container::FactorialIterator & Container::FactorialIterator::operator --()
{
  if (index_ > 1)
  {
    value_ /= index_;
    --index_;
  }
  return * this;
}

Container::FactorialIterator Container::FactorialIterator::operator ++(int)
{
  FactorialIterator temp = * this;
  ++(* this);

  return temp;
}

Container::FactorialIterator Container::FactorialIterator::operator --(int)
{
  FactorialIterator temp = * this;
  --(* this);

  return temp;
}

unsigned long long Container::FactorialIterator::getValue(size_t index) const
{
  if (index <= 1)
  {
    return 1;
  }
  else
  {
    return index * getValue(index - 1);
  }
}
