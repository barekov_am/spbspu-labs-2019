#include "PhonebookInterface.hpp"
#include <iostream>
#include <sstream>

PhonebookInterface::PhonebookInterface()
{
  bookmarks_["current"] = phoneBook_.begin();
}

void PhonebookInterface::add(PhoneBookEntry & record)
{
  phoneBook_.pushBack(record);

  if (std::next(phoneBook_.begin()) == phoneBook_.end())
  {
    bookmarks_["current"] = phoneBook_.begin();
  }
};


void PhonebookInterface::store(std::string &markName, std::string &newMarkName)
{
  auto iter = bookmarks_.find(markName);

  if (iter != bookmarks_.end()) {
    bookmarks_[newMarkName] = iter->second;
    return;;
  }

  std::cout << "<INVALID BOOKMARK>\n";
};

void PhonebookInterface::insert(InsertPosition & position, std::string & markName, PhoneBookEntry & record)
{
  auto iter = bookmarks_.find(markName);

  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  if (iter->second == phoneBook_.end())
  {
    add(record);
  }

  switch (position)
  {
    case (InsertPosition::before):
    {
      phoneBook_.insert(iter->second, record);
      break;
    }
    case (InsertPosition::after):
    {
      phoneBook_.insert(std::next(iter->second), record);
      break;
    }
  }
};

void PhonebookInterface::deleteNote(std::string & markName)
{
  auto iter = bookmarks_.find(markName);
  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }
  else
  {
    auto deleteIter = iter->second;
    for (auto i = bookmarks_.begin(); i != bookmarks_.end(); i++)
    {
      if (i->second == deleteIter)
      {
        if (std::next(i->second) == phoneBook_.end())
        {
          i->second = phoneBook_.movePrevious(deleteIter);
        }
        else
        {
          i->second = phoneBook_.moveNext(deleteIter);
        }
      }
    }
    phoneBook_.remove(deleteIter);
  }
};

void PhonebookInterface::show(std::string & markName)
{
  auto iter = bookmarks_.find(markName);
  if (iter != bookmarks_.end())
  {
    if (phoneBook_.empty())
    {
      std::cout << "<EMPTY>\n";
      return;
    }
    return phoneBook_.show(iter->second);
  }

  std::cout << "<INVALID BOOKMARK>\n";
};

void PhonebookInterface::move(std::string &markName, int steps)
{
  auto iter = bookmarks_.find(markName);
  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  iter->second = phoneBook_.move(iter->second, steps);
};


void PhonebookInterface::move(std::string &markName, MovePosition & movePosition)
{
  auto iter = bookmarks_.find(markName);

  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  switch (movePosition)
  {
  case MovePosition::first:
    {
      iter->second = phoneBook_.begin();
      break;
    }
  case MovePosition::last:
    {
      iter->second = phoneBook_.movePrevious(phoneBook_.end());
      break;
    }
  }
};
