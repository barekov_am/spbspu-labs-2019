#ifndef B3_PHONEBOOK_HPP
#define B3_PHONEBOOK_HPP

#include <string>
#include <list>

struct PhoneBookEntry
{
  std::string name, number;
};

class PhoneBook
{
public:
  using iterator = std::list<PhoneBookEntry>::iterator;
  bool empty();
  iterator begin();
  iterator end();

  iterator insert(iterator iter, PhoneBookEntry & entry);
  iterator move(iterator iter, int steps);
  iterator moveNext(iterator iter);
  iterator movePrevious(iterator iter);
  iterator remove(iterator iter);
  iterator replace(iterator iter, PhoneBookEntry & entry);

  void show(iterator iter);
  void pushBack(PhoneBookEntry & entry);

private:
  std::list<PhoneBookEntry> listPhoneBookEntry;
};

#endif
