#ifndef B3_PHONEBOOKINTERFACE_HPP
#define B3_PHONEBOOKINTERFACE_HPP

#include <string>
#include <map>
#include "PhoneBook.hpp"

class PhonebookInterface
{
public:
  enum class InsertPosition
  {
    before,
    after
  };

  enum class MovePosition
  {
    first,
    last
  };

  PhonebookInterface();
  void add(PhoneBookEntry & record);
  void store(std::string & bookmark, std::string & newBookmark);
  void insert(InsertPosition & position, std::string & bookmark, PhoneBookEntry & record);
  void deleteNote(std::string & bookmark);
  void show(std::string & bookmark);
  void move(std::string & bookmark, int steps);
  void move(std::string & bookmark, MovePosition & movePosition);

private:
  std::map<std::string, PhoneBook::iterator> bookmarks_;
  PhoneBook phoneBook_;
};

#endif
