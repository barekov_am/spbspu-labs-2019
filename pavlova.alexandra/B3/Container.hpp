#ifndef B3_CONTAINER_HPP
#define B3_CONTAINER_HPP

#include <iterator>

class Container
{
public:
  Container() = default;

  class FactorialIterator;
  FactorialIterator begin();
  FactorialIterator end();
};

class Container::FactorialIterator: public std::iterator<std::bidirectional_iterator_tag, unsigned long long>
{
public:
  FactorialIterator(size_t index);

  bool operator ==(const FactorialIterator & other) const;
  bool operator !=(const FactorialIterator & other) const;
  unsigned long long & operator *();
  unsigned long long * operator ->();
  FactorialIterator & operator ++();
  FactorialIterator & operator --();
  FactorialIterator operator ++(int);
  FactorialIterator operator --(int);

private:
  size_t index_;
  unsigned long long value_;
  unsigned long long getValue(size_t index) const;
};

#endif
