#include "BookHandler.hpp"
#include <algorithm>
#include <cctype>
#include <iostream>

bool isEmpty(std::istream &);
std::unique_ptr<Command> create_add(std::istream &);
std::unique_ptr<Command> create_store(std::istream &);
std::unique_ptr<Command> create_insert(std::istream &);
std::unique_ptr<Command> create_delete(std::istream &);
std::unique_ptr<Command> create_show(std::istream &);
std::unique_ptr<Command> create_move(std::istream &);
std::unique_ptr<Command> create_invalid(std::istream &);

struct Commands {
  std::string name;
  std::function<std::unique_ptr<Command>(std::istream &)> command;
};

std::istream & operator>>(std::istream & is, std::unique_ptr<Command> & cmd)
{
  static const Commands cmds[] = {{"add", &create_add}, {"store", &create_store}, {"insert", &create_insert}, {"delete", &create_delete}, {"show", &create_show}, {"move", &create_move}};

  std::string cmd_s;
  is >> blank >> cmd_s;

  auto get_command =
      std::find_if(std::begin(cmds), std::end(cmds), [&](const Commands & command) { return cmd_s == command.name; });

  if (get_command != std::end(cmds)) {
    cmd = get_command->command(is);
  } else {
    cmd = create_invalid(is);
  }

  if (!isEmpty(is)) {
    cmd = create_invalid(is);
  }

  return is;
}

std::ostream & operator<<(std::ostream & os, const PhoneBook::Note & note)
{
  os << note.m_number << " " << note.m_name << "\n";

  return os;
}

std::istream & operator>>(std::istream & is, Number & numb)
{
  if (isEmpty(is)) {
    is.setstate(std::ios_base::failbit);
    return is;
  }

  is >> blank >> numb.m_number;

  return is;
}

std::ostream & operator<<(std::ostream & os, const Number & numb)
{
  os << numb.get_numb();

  return os;
}

std::istream & operator>>(std::istream & is, Name & name)
{
  if (isEmpty(is)) {
    is.setstate(std::ios_base::failbit);
    return is;
  }

  char ch;
  is >> blank >> ch;

  if (ch != '"') {
    is.setstate(std::ios_base::failbit);
    return is;
  }

  ch = '\0';
  while ((is.peek() != '\n')) {
    is.get(ch);

    if (ch == '"') {
      break;
    }

    if (ch == '\\') {
      if ((is.peek() != '\\') && (is.peek() != '"')) {
        is.setstate(std::ios_base::failbit);
        return is;
      } else {
        is.get(ch);
      }
    }

    name.m_name += ch;
  }

  auto blank_str =
      std::find_if(std::begin(name.m_name), std::end(name.m_name), [&](const char & c) { return !isspace(c); });

  if ((name.m_name.empty()) || (blank_str == std::end(name.m_name)) || (ch != '"')) {
    is.setstate(std::ios_base::failbit);
  }

  return is;
}

std::ostream & operator<<(std::ostream & os, const Name & name)
{
  os << name.get_name();

  return os;
}

std::istream & operator>>(std::istream & is, Mark & mark)
{
  if (isEmpty(is)) {
    is.setstate(std::ios_base::failbit);
    return is;
  }

  char ch;
  while (!isspace(is.peek())) {
    is >> blank >> ch;
    if (isalnum(ch) || ch == '-') {
      mark.m_name += ch;
    } else {
      is.setstate(std::ios_base::failbit);
      return is;
    }
  }

  return is;
}

std::ostream & operator<<(std::ostream & os, const Mark & mark)
{
  os << mark.get_name();

  return os;
}

std::istream & operator>>(std::istream & is, Steps & step)
{
  if (isEmpty(is)) {
    is.setstate(std::ios_base::failbit);
    return is;
  }

  step.m_correctness = true;

  static const std::string steps[]{"first", "last"};

  std::string step_s;
  is >> blank >> step_s;

  if ((step_s.size() == 1) && !isdigit(step_s[0])) {
    step.m_correctness = false;
  }

  auto foundStep = std::find(std::begin(steps), std::end(steps), step_s);

  if (foundStep == std::end(steps)) {
    int size = static_cast<int>(step_s.size());

    int i = (step_s[0] == '+' || step_s[0] == '-') ? 1 : 0;

    while (i < size) {
      if (!isdigit(step_s[i])) {
        step.m_correctness = false;
        break;
      }
      ++i;
    }

    step.m_step.m_mode = Steps::step_t::stepMode::COUNT;
  } else {
    if (step_s == "first") {
      step.m_step.m_mode = Steps::step_t::stepMode::FIRST;
    } else {
      step.m_step.m_mode = Steps::step_t::stepMode::LAST;
    }
  }

  step.m_step.m_value = step_s;

  return is;
}
