#include "BookHandler.hpp"
#include "manipulator.hpp"
#include <algorithm>
#include <iostream>
#include <limits>
#include <string>

BookHandler::BookHandler(PhoneBook & book) :
  m_book{book}
{}

BookHandler::Book::Book(PhoneBook & book) :
  m_phoneBook{book}
{
  m_marks.emplace("current", m_phoneBook.begin());
}

void BookHandler::run(std::istream & is, std::ostream & os)
{
  std::unique_ptr<Command> command;
  while (is >> command) {
    command->execute(m_book, os);

    is.clear();
    is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }
}

bool BookHandler::Book::isExisting(const Book & book, const Mark & mark)
{
  const auto currentMark = book.m_marks.find(mark.get_name());

  return !(currentMark == book.m_marks.end());
}

class Add : public Command {
public:
  Add(Number, Name);

  void execute(Book &, std::ostream &);

private:
  Number m_number;
  Name m_name;
};

class Store : public Command {
public:
  Store(Mark, Mark);
  void execute(Book &, std::ostream &);

private:
  commandState m_state;
  Mark m_mark;
  Mark m_newMark;
};

class Insert : public Command {
public:
  enum position
  {
    BEFORE,
    AFTER
  };

  struct position_t {
    std::string posName;
    position pos;
  };

  Insert(position, Mark, Number, Name);
  void execute(Book &, std::ostream &);

private:
  commandState m_state;
  position m_postion;
  Mark m_mark;
  Number m_number;
  Name m_name;

  void insert_to(Book &);
};

class Delete : public Command {
public:
  Delete(Mark);
  void execute(Book &, std::ostream &);

private:
  commandState m_state;
  Mark m_mark;

  void remove_from(Book &);
};

class Show : public Command {
public:
  Show(Mark);
  void execute(Book &, std::ostream &);

private:
  commandState m_state;
  Mark m_mark;
};

class Move : public Command {
public:
  Move(Mark, Steps);
  void execute(Book &, std::ostream &);

private:
  commandState m_state;
  Mark m_mark;
  Steps m_steps;

  void move_in(Book &);
};

class Invalid : public Command {
public:
  void execute(Book &, std::ostream &);
};

Add::Add(Number numb, Name name) :
  m_number{numb},
  m_name{name}
{}

Store::Store(Mark mark, Mark newMark) :
  m_state{commandState::OK},
  m_mark{mark},
  m_newMark{newMark}
{}

Insert::Insert(position position, Mark mark, Number numb, Name name) :
  m_state{commandState::OK},
  m_postion{position},
  m_mark{mark},
  m_number{numb},
  m_name{name}
{}

Delete::Delete(Mark mark) :
  m_state{commandState::OK},
  m_mark{mark}
{}

Show::Show(Mark mark) :
  m_state{commandState::OK},
  m_mark{mark}
{}

Move::Move(Mark mark, Steps step) :
  m_state{commandState::OK},
  m_mark{mark},
  m_steps{step}
{}

void Add::execute(Book & book, std::ostream &)
{
  PhoneBook::Note note{m_number.get_numb(), m_name.get_name()};

  book.m_phoneBook.push_back(note);

  if (book.m_phoneBook.size() == 1) {
    book.m_marks.clear();
    book.m_marks.emplace("current", book.m_phoneBook.begin());
  }
}

void Store::execute(Book & book, std::ostream & os)
{
  if (!book.isExisting(book, m_mark)) {
    m_state = commandState::INVALID_MARK;
  }

  switch (m_state) {
    case commandState::OK:
      book.m_marks.emplace(m_newMark.get_name(), book.m_marks[m_mark.get_name()]);
      break;
    case commandState::INVALID_MARK:
      os << consts::INVALID_BOOKMARK << "\n";
      break;
    default:
      break;
  }
}

void Insert::execute(Book & book, std::ostream & os)
{
  if (!book.isExisting(book, m_mark)) {
    m_state = commandState::INVALID_MARK;
  }

  switch (m_state) {
    case commandState::OK:
      insert_to(book);
      break;
    case commandState::INVALID_MARK:
      os << consts::INVALID_BOOKMARK << "\n";
    default:
      break;
  }
}

void Insert::insert_to(Book & book)
{
  PhoneBook::Note note{m_number.get_numb(), m_name.get_name()};

  if (book.m_phoneBook.empty()) {
    book.m_marks.clear();
    book.m_phoneBook.push_back(note);
    book.m_marks.emplace("current", book.m_phoneBook.begin());
  } else {
    switch (m_postion) {
      case position::BEFORE:
        book.m_phoneBook.insert_before(book.m_marks[m_mark.get_name()], note);
        break;
      case position::AFTER:
        book.m_phoneBook.insert_after(book.m_marks[m_mark.get_name()], note);
        break;
    }
  }
}

void Delete::execute(Book & book, std::ostream & os)
{
  if (book.m_phoneBook.empty()) {
    os << "<EMPTY>"
       << "\n";
    return;
  }

  if (!book.isExisting(book, m_mark)) {
    m_state = commandState::INVALID_MARK;
  }

  switch (m_state) {
    case commandState::OK:
      remove_from(book);
      break;
    case commandState::INVALID_MARK:
      os << consts::INVALID_BOOKMARK << "\n";
    default:
      break;
  }
}

void Delete::remove_from(Book & book)
{
  std::for_each(book.m_marks.begin(), book.m_marks.end(), [&](auto & mark) {
    if ((mark.second == book.m_marks[m_mark.get_name()]) && (mark.first != m_mark.get_name())) {
      mark.second = (mark.second == --book.m_phoneBook.end()) ? --mark.second : ++mark.second;
    }
  });

  book.m_phoneBook.remove(book.m_marks[m_mark.get_name()]);
}

void Show::execute(Book & book, std::ostream & os)
{
  if (!book.isExisting(book, m_mark)) {
    m_state = commandState::INVALID_MARK;
  }

  switch (m_state) {
    case commandState::OK:
      if (book.m_phoneBook.empty()) {
        os << "<EMPTY>"
           << "\n";
      } else {
        os << *book.m_marks[m_mark.get_name()];
      }
      break;
    case commandState::INVALID_MARK:
      os << consts::INVALID_BOOKMARK << "\n";
      break;
    default:
      break;
  }
}

void Move::execute(Book & book, std::ostream & os)
{
  if (!book.isExisting(book, m_mark)) {
    m_state = commandState::INVALID_MARK;
  } else if (!m_steps.isCorrect()) {
    m_state = commandState::INVALID_STEP;
  }

  switch (m_state) {
    case commandState::OK:
      move_in(book);
      break;
    case commandState::INVALID_MARK:
      os << consts::INVALID_BOOKMARK << "\n";
      break;
    case commandState::INVALID_STEP:
      os << consts::INVALID_STEP << "\n";
      break;
    default:
      break;
  }
}

void Move::move_in(Book & book)
{
  using StepMode = Steps::step_t::stepMode;

  int position;
  switch (m_steps.get_mode()) {
    case StepMode::FIRST:
      position = -book.m_phoneBook.size();
      break;
    case StepMode::LAST:
      position = book.m_phoneBook.size();
      break;
    case StepMode::COUNT:
      position = std::stoi(m_steps.get_value());
      break;
  }

  book.m_phoneBook.move_on(book.m_marks[m_mark.get_name()], position);
}

void Invalid::execute(Book &, std::ostream & os)
{
  os << consts::INVALID_COMMAND << "\n";
}

std::unique_ptr<Command> create_add(std::istream & is)
{
  Number numb;
  Name name;

  is >> numb >> name;

  if (is.fail()) {
    is.clear(std::ios_base::goodbit);
    return std::unique_ptr<Command>(new Invalid());
  }

  return std::unique_ptr<Command>(new Add(numb, name));
}

std::unique_ptr<Command> create_store(std::istream & is)
{
  Mark markName;
  Mark newName;

  is >> markName >> newName;

  return std::unique_ptr<Command>(new Store(markName, newName));
}

std::unique_ptr<Command> create_insert(std::istream & is)
{
  using position_t = Insert::position_t;

  static const position_t positions[]{{"before", Insert::position::BEFORE}, {"after", Insert::position::AFTER}};

  std::string position;
  Mark markName;
  Number numb;
  Name name;

  is >> blank >> position >> markName >> numb >> name;

  auto foundPos = std::find_if(std::begin(positions), std::end(positions),
                               [&](const position_t pos) { return pos.posName == position; });

  if (foundPos == std::end(positions) || is.fail()) {
    is.clear(std::ios_base::goodbit);
    return std::unique_ptr<Command>(new Invalid());
  }

  return std::unique_ptr<Command>(new Insert(foundPos->pos, markName, numb, name));
}

std::unique_ptr<Command> create_delete(std::istream & is)
{
  Mark mark;

  is >> mark;

  return std::unique_ptr<Command>(new Delete(mark));
}

std::unique_ptr<Command> create_show(std::istream & is)
{
  Mark mark;

  is >> mark;

  return std::unique_ptr<Command>(new Show(mark));
}

std::unique_ptr<Command> create_move(std::istream & is)
{
  Mark mark;
  Steps step;

  is >> mark >> step;

  return std::unique_ptr<Command>(new Move(mark, step));
}

std::unique_ptr<Command> create_invalid(std::istream &)
{
  return std::unique_ptr<Command>(new Invalid());
}
