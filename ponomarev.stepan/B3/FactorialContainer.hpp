#ifndef FACTORIAL_CONTAINER_HPP
#define FACTORIAL_CONTAINER_HPP

#include <cstddef>
#include <iterator>

class FactorialContainer {
public:
  class iterator :
    public std::iterator<std::bidirectional_iterator_tag,
                         unsigned long long,
                         std::ptrdiff_t,
                         unsigned long long,
                         unsigned long long> {
  public:
    iterator();
    iterator(unsigned int);

    unsigned long long operator*() const;

    bool operator==(const iterator &) const;
    bool operator!=(const iterator &) const;

    iterator & operator++();
    iterator & operator--();

    iterator operator++(int);
    iterator operator--(int);

  private:
    unsigned long long value_m;
    unsigned int index_m;
    unsigned int indexLimit_m;

    void count();
  };
  using reverse_iterator = std::reverse_iterator<FactorialContainer::iterator>;
  FactorialContainer(unsigned int);

  iterator begin() const;
  iterator end() const;

private:
  unsigned int size_m;
};

#endif
