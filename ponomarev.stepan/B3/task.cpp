#include "BookHandler.hpp"
#include "FactorialContainer.hpp"

#include <algorithm>
#include <iostream>

void task1()
{
  PhoneBook book;

  BookHandler ibook(book);

  ibook.run(std::cin, std::cout);
}

void task2()
{
  FactorialContainer fc(10);

  std::copy(fc.begin(), fc.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << "\n";

  std::reverse_copy(fc.begin(), fc.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << "\n";
}
