#include "PhoneBook.hpp"

PhoneBook::Note::Note(std::string number, std::string name) :
  m_number{number},
  m_name{name}
{}

PhoneBook::Iterator PhoneBook::begin()
{
  return m_notes.begin();
}

PhoneBook::Iterator PhoneBook::end()
{
  return m_notes.end();
}

void PhoneBook::next(Iterator & iter)
{
  iter = (iter != --m_notes.end()) ? ++iter : iter;
}

void PhoneBook::prev(Iterator & iter)
{
  iter = (iter != m_notes.begin()) ? --iter : iter;
}

void PhoneBook::insert_before(const Iterator & iter, const Note note)
{
  if (empty()) {
    push_back(note);
  }

  if (iter == m_notes.begin()) {
    m_notes.push_front(note);
  } else {
    m_notes.insert(iter, note);
  }
}

void PhoneBook::insert_after(const Iterator & iter, const Note note)
{
  if (empty() || (iter == --m_notes.end())) {
    push_back(note);
  } else {
    Iterator tmp = iter;
    next(tmp);
    m_notes.insert(tmp, note);
  }
}

void PhoneBook::remove(Iterator & iter)
{
  Iterator tmp = iter;

  if (iter == --m_notes.end()) {
    prev(iter);
  } else {
    next(iter);
  }

  m_notes.erase(tmp);
}

void PhoneBook::push_back(const Note note)
{
  if (m_notes.empty()) {
    m_notes.push_back(note);
  } else {
    m_notes.push_back(note);
  }
}

void PhoneBook::move_on(Iterator & iter, const int & steps)
{
  if (steps > 0) {
    for (int i = 0; i < steps; ++i) {
      next(iter);
      if (i > size()) {
        return;
      }
    }
  } else if (steps < 0) {
    for (int i = 0; i > steps; --i) {
      prev(iter);
      if (i < -size()) {
        return;
      }
    }
  }
}

bool PhoneBook::empty() const
{
  return m_notes.empty();
}

int PhoneBook::size() const
{
  return m_notes.size();
}
