#include "FactorialContainer.hpp"

#include <stdexcept>

FactorialContainer::FactorialContainer(unsigned int size) :
  size_m{size}
{}

FactorialContainer::iterator::iterator() :
  value_m{1},
  index_m{1},
  indexLimit_m{11}
{}

FactorialContainer::iterator::iterator(unsigned int index) :
  value_m{1},
  index_m{index},
  indexLimit_m{11}
{
  if (index == 0) {
    index_m = 1;
  }

  if (index_m > indexLimit_m) {
    throw std::out_of_range("Inputed index is too big.\n");
  }

  count();
}

unsigned long long FactorialContainer::iterator::operator*() const
{
  return value_m;
}

bool FactorialContainer::iterator::operator==(const iterator & iter) const
{
  return iter.value_m == value_m;
}

bool FactorialContainer::iterator::operator!=(const iterator & iter) const
{
  return !(*this == iter);
}

FactorialContainer::iterator & FactorialContainer::iterator::operator++()
{
  if (index_m < indexLimit_m) {
    ++index_m;
    value_m *= index_m;
  } else {
    throw std::out_of_range("Index is maximum.\n");
  }

  return *this;
}

FactorialContainer::iterator & FactorialContainer::iterator::operator--()
{
  if (index_m > 1) {
    value_m /= index_m;
    --index_m;
  } else {
    throw std::out_of_range("Index is minimum.\n");
  }

  return *this;
}

FactorialContainer::iterator FactorialContainer::iterator::operator++(int)
{
  iterator tmp = *this;
  ++(*this);

  return tmp;
}

FactorialContainer::iterator FactorialContainer::iterator::operator--(int)
{
  iterator tmp = *this;
  --(*this);

  return tmp;
}

void FactorialContainer::iterator::count()
{
  unsigned long fact = 1;
  for (unsigned int i = 1; i <= index_m; ++i) {
    fact *= i;
  }

  value_m = fact;
}

FactorialContainer::iterator FactorialContainer::begin() const
{
  return FactorialContainer::iterator(1);
}

FactorialContainer::iterator FactorialContainer::end() const
{
  return FactorialContainer::iterator(size_m + 1);
}
