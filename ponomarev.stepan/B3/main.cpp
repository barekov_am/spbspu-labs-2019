#include <iostream>

void task1();
void task2();

int main(int argc, char * argv[])
{
  try {
    if (argc != 2) {
      throw std::invalid_argument("Invalid count of args.\n");
    }

    const int task = std::stoi(argv[1]);

    if (task < 1 || task > 2) {
      throw std::invalid_argument("Invalid arg value.\n");
    }

    switch (task) {
      case 1:
        task1();
        break;
      case 2:
        task2();
        break;
    }

  } catch (std::invalid_argument &) {
    return 1;
  } catch (std::exception &) {
    return 2;
  }

  return 0;
}
