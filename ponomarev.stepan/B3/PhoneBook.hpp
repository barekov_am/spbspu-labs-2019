#ifndef PHONE_BOOK_HPP
#define PHONE_BOOK_HPP

#include <list>
#include <string>

class PhoneBook {
public:
  class Note {
  public:
    Note();
    Note(std::string, std::string);

    friend std::ostream & operator<<(std::ostream &, const Note &);

  private:
    std::string m_number;
    std::string m_name;
  };

  using Iterator = std::list<PhoneBook::Note>::iterator;

  PhoneBook() = default;

  Iterator begin();
  Iterator end();

  void next(Iterator &);
  void prev(Iterator &);

  void insert_before(const Iterator &, const Note);
  void insert_after(const Iterator &, const Note);

  void remove(Iterator &);
  void push_back(const Note);

  void move_on(Iterator &, const int &);

  bool empty() const;
  int size() const;

private:
  std::list<PhoneBook::Note> m_notes;
};

#endif
