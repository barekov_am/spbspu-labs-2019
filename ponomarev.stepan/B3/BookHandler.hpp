#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include "PhoneBook.hpp"
#include "manipulator.hpp"
#include <iostream>
#include <map>
#include <memory>
#include <string>

namespace consts {
  static const std::string INVALID_COMMAND = "<INVALID COMMAND>";
  static const std::string INVALID_BOOKMARK = "<INVALID BOOKMARK>";
  static const std::string INVALID_STEP = "<INVALID STEP>";
};

class Name {
public:
  ~Name() = default;
  friend std::istream & operator>>(std::istream &, Name &);
  friend std::ostream & operator<<(std::ostream &, const Name &);

  std::string get_name() const
  {
    return m_name;
  }

private:
  std::string m_name;
};

class Number {
public:
  ~Number() = default;
  friend std::istream & operator>>(std::istream &, Number &);
  friend std::ostream & operator<<(std::ostream &, const Number &);

  std::string get_numb() const
  {
    return m_number;
  }

private:
  std::string m_number;
};

class Mark {
public:
  ~Mark() = default;
  friend std::istream & operator>>(std::istream &, Mark &);
  friend std::ostream & operator<<(std::ostream &, const Mark &);

  std::string get_name() const
  {
    return m_name;
  }

private:
  std::string m_name;
};

class Steps {
public:
  struct step_t {
    enum stepMode
    {
      FIRST,
      LAST,
      COUNT
    };

    stepMode m_mode;
    std::string m_value;
  };

  ~Steps() = default;

  friend std::istream & operator>>(std::istream &, Steps &);

  std::string get_value() const
  {
    return m_step.m_value;
  }

  step_t::stepMode get_mode()
  {
    return m_step.m_mode;
  }

  bool isCorrect()
  {
    return m_correctness;
  }

private:
  bool m_correctness;
  step_t m_step;
};

class BookHandler {
public:
  struct Book {
    using BookMarks = std::map<std::string, PhoneBook::Iterator>;
    Book(PhoneBook &);

    bool isExisting(const Book &, const Mark &);

    PhoneBook & m_phoneBook;
    BookMarks m_marks;
  };

  BookHandler(PhoneBook &);
  void run(std::istream &, std::ostream &);

private:
  Book m_book;
};

class Command {
public:
  using Book = BookHandler::Book;

  enum commandState
  {
    INVALID_STEP,
    INVALID_MARK,
    OK
  };

  Command() = default;
  virtual ~Command() = default;
  virtual void execute(Book &, std::ostream &) = 0;
};

std::istream & operator>>(std::istream &, std::unique_ptr<Command> &);

#endif
