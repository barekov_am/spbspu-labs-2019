#ifndef QUEUE_WITH_PRIORITY_HPP
#define QUEUE_WITH_PRIORITY_HPP

#include <string>
#include <cstdlib>
#include <list>
#include <iostream>

template <typename T>
class QueueWithPriority {
public:
  enum class ElementPriority {
    LOW,
    NORMAL,
    HIGH
  };

  QueueWithPriority();

  bool operator == (const QueueWithPriority & another) const;
  bool operator != (const QueueWithPriority & another) const;

  void put_element_to_queue(const T &, ElementPriority);

  T get_element_from_queue();

  void accelerate();

  inline int size() const noexcept;
  void clear() noexcept;
  inline bool empty() noexcept;

  void print() const noexcept;
private:
  std::list<T> list_m;
  typename std::list<T>::iterator normal_it_m;
  typename std::list<T>::iterator low_it_m;

  void inline check_after_low_adding() noexcept;
  void inline check_after_normal_adding() noexcept;
};

template <typename T>
QueueWithPriority<T>::QueueWithPriority():
  list_m(),
  normal_it_m(list_m.begin()),
  low_it_m(list_m.begin())
{}

template <typename T>
bool QueueWithPriority<T>::operator == (const QueueWithPriority & another) const
{
 return (list_m == another.list_m);
}

template <typename T>
bool QueueWithPriority<T>::operator != (const QueueWithPriority & another) const
{
  return !(*this == another);
}


template <typename T>
void QueueWithPriority<T>::put_element_to_queue(const T & element, ElementPriority priority)
{
  switch (priority) {
    case ElementPriority::LOW:
      list_m.insert(list_m.end(), element);
      check_after_low_adding();
      break;
    case ElementPriority::NORMAL:
      list_m.insert(low_it_m, element);
      check_after_normal_adding();
      break;
    case ElementPriority::HIGH:
      list_m.insert(normal_it_m, element);
      break;
    default:
      throw std::runtime_error("Invalid priority.");
      break;
  }
}

template <typename T>
void inline QueueWithPriority<T>::check_after_low_adding() noexcept
{
  if (low_it_m == list_m.end()) {
    low_it_m = std::prev(list_m.end());

    if (normal_it_m == list_m.end()) {
      normal_it_m = std::prev(list_m.end());
    }
  }
}

template <typename T>
void inline QueueWithPriority<T>::check_after_normal_adding() noexcept
{
  if (normal_it_m == low_it_m) {
   normal_it_m = std::prev(low_it_m);
  }
}

template <typename T>
T QueueWithPriority<T>::get_element_from_queue()
{
  if (list_m.empty()) {
     throw std::runtime_error("Queue is empty.");
  }

  T element = list_m.front();

  if (normal_it_m == list_m.begin()) {
    normal_it_m = low_it_m;
  }

  list_m.pop_front();

  if (list_m.empty()) {
    normal_it_m = list_m.begin();
    low_it_m = list_m.begin();
  }

  return element;
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  if (list_m.size() > 1) {
    list_m.splice(normal_it_m, list_m, low_it_m, list_m.end());
  }
}

template <typename T>
int QueueWithPriority<T>::size() const noexcept
{
  return list_m.size();
}

template <typename T>
void QueueWithPriority<T>::clear() noexcept
{
  list_m.clear();
  normal_it_m = list_m.begin();
  low_it_m = list_m.begin();
}

template <typename T>
bool QueueWithPriority<T>::empty() noexcept
{
  return list_m.empty();
}

template <typename T>
void QueueWithPriority<T>::print() const noexcept
{
  for (auto e : list_m) {
    std::cout << e << " ";
  }

  std::cout << std::endl;
}

#endif
