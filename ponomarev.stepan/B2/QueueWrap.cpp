#include "QueueWrap.hpp"

bool is_line_empty(const std::string & str);
void erase_spaces(std::string &);

QueueWrap::QueueWrap(QueueWithPriority<std::string> & queue):
queue_m{queue}
{
  commands_m[0] = "add";
  commands_m[1] = "get";
  commands_m[2] = "accelerate";

  priorities_m[0] = "low";
  priorities_m[1] = "normal";
  priorities_m[2] = "high";
}

void QueueWrap::run()
{
  std::string line;
  while (std::getline(std::cin, line)) {
    if (!check_line(line)) {
      std::cout << "<INVALID COMMAND>" << "\n";
    } else {
      parse_line(line);
    }
  }
}

bool QueueWrap::check_line(std::string & str)
{
  if (is_line_empty(str)) {
    return false;
  }

  erase_spaces(str);

  if ((str ==  commands_m[1]) || (str ==  commands_m[2])) {
    return true;
  }

  int size = static_cast<int>(str.size());

  int space_numb = 0;
  bool ignore = false;
  for (int i = 0; i < size; ++i) {
    if (isspace(str.at(i))) {
      if (!ignore) {
        space_numb++;
        ignore = true;
      }
    } else if (space_numb > 0) {
      ignore = false;
    }
  }

  return (space_numb >= 2);
}

void QueueWrap::parse_line(std::string & str)
{
  if (str == commands_m[1]) {
    queue_m.empty()
      ? std::cout << "<EMPTY>" << "\n"
      : std::cout << queue_m.get_element_from_queue() << "\n";
  } else if (str == commands_m[2]) {
    queue_m.accelerate();
  } else {
    do_add(str);
  }
}

void QueueWrap::do_add(std::string & str) noexcept
{
  std::string command;
  std::string priority;
  std::string data;

  get_part(str, command);
  get_part(str, priority);
  data = str;

  if (check_command(command) && check_priority(priority)) {
    do_request(data, priority);
  } else {
    std::cout << "<INVALID COMMAND>" << "\n";
  }
}

void QueueWrap::get_part(std::string & from, std::string & to) noexcept
{
  to.clear();
  int i = 0;
  while (!isspace(from.at(i))) {
      to += from.at(i);
      ++i;
    }

  from.erase(0, i);
  erase_spaces(from);
}

bool inline QueueWrap::check_command(const std::string & str) const noexcept
{
  return (str == commands_m[0]);
}

bool inline QueueWrap::check_priority(const std::string & str) const noexcept
{
  return ((str == priorities_m[0]) || (str == priorities_m[1]) || (str == priorities_m[2]));
}

void inline QueueWrap::do_request(const std::string & data, const std::string & priority)
{
  Request request = get_request(data, priority);

  queue_m.put_element_to_queue(request.data, request.priority);
}

QueueWrap::Request inline QueueWrap::get_request(const std::string & data, const std::string & priority) const noexcept
{
  Request request {
    data,
    get_priority(priority)
  };

  return request;
}

QueueWrap::Priority inline QueueWrap::get_priority(const std::string & str) const
{
  for (int i = 0; i < 3; ++i) {
    if (str == priorities_m[i]) {
      return Priority(i);
    }
  }

  throw std::runtime_error("Invalid priority.");
}

void inline QueueWrap::set_queue(QueueWithPriority<std::string> & queue)
{
  queue_m = queue;
}

QueueWithPriority<std::string> inline & QueueWrap::get_queue() const noexcept
{
  return queue_m;
}
