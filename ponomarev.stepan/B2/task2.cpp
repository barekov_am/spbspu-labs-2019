#include <list>
#include <iostream>

namespace consts {
  const int MAXIMUM_VALUE = 20;
  const int MINIMUM_VALUE = 1;
  const int MAX_SIZE = 20;
};

void fill_list(std::list<int> &);
void print_list(std::list<int> &);

void task2()
{
  std::list<int> list;

  fill_list(list);

  print_list(list);
}

void fill_list(std::list<int> & list)
{
  using namespace consts;

  int quantity = 0;
  for (int data; std::cin >> data; ++quantity) {
    if (!std::cin.eof()) {
      if ((data > MAXIMUM_VALUE) || (data < MINIMUM_VALUE) || (quantity == MAX_SIZE)) {
        throw std::runtime_error("Incorrect input argument.");
      }
    }

    list.push_back(data);
  }

  if (std::cin.fail() && !std::cin.eof()) {
    throw std::runtime_error("Error input.");
  }
}

void print_list(std::list<int>::iterator &, std::list<int>::iterator &);
void print_list(std::list<int> & list)
{
  if (list.empty()) {
    return;
  }

  auto start = list.begin();
  auto end = --list.end();

  print_list(start, end);
}

void print_list(std::list<int>::iterator & start, std::list<int>::iterator & end)
{
  if (start ==  end) {
    std::cout << *end << "\n";
  } else {
    std::cout << *start;
    ++start;

    if (start ==  end) {
      std::cout << " " << *end << "\n";
    } else {
      std::cout << " " << *end << " ";
      --end;
      print_list(start, end);
    }
  }
}
