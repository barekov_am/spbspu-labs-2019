#include "QueueWithPriority.hpp"
#include "QueueWrap.hpp"

#include <iostream>
#include <vector>

void task1()
{
  QueueWithPriority<std::string> queue;

  QueueWrap wrap(queue);

  wrap.run();
}
