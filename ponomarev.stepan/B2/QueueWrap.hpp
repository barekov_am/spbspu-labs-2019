#ifndef QUEUE_HELPER_HPP
#define QUEUE_HELPER_HPP

#include "QueueWithPriority.hpp"

#include <string>
#include <iostream>
#include <functional>

class QueueWrap {
public:
  QueueWrap(QueueWithPriority<std::string> &);

  void run();

  void inline set_queue(QueueWithPriority<std::string> &);
  QueueWithPriority<std::string> & get_queue() const noexcept;
private:
  using Priority = QueueWithPriority<std::string>::ElementPriority;

  struct Request {
    std::string data;
    Priority priority;
  };

  QueueWithPriority<std::string> & queue_m;
  std::string commands_m[3];
  std::string priorities_m[3];

  bool check_line(std::string & str);
  void parse_line(std::string & str);

  void do_add(std::string &) noexcept;

  void get_part(std::string & from, std::string & to) noexcept;

  bool inline check_command(const std::string &) const noexcept;
  bool inline check_priority(const std::string &) const noexcept;

  void inline do_request(const std::string &, const std::string &);
  Priority inline get_priority(const std::string &) const;
  Request inline get_request(const std::string & data, const std::string & priority) const noexcept;
};

#endif
