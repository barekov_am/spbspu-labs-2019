#include <string>

void erase_spaces(std::string & str)
{
  const int size = static_cast<int>(str.size());

  int i = size - 1;
  int count = 0;
  while (isspace(str.at(i))) {
    count++;
    i--;
  }

  str.erase(size - count, size - 1);

  i = 0;
  while (isspace(str.at(i))) {
    ++i;
  }
  str.erase(0, i);
}

bool is_line_empty(const std::string & str) noexcept
{
  int size = static_cast<int>(str.size());
  int no_space = 0;
  for (int i = 0; i < size; ++i) {
    if (!isspace(str.at(i))) {
      ++no_space;
    }
  }

  if (no_space == 0) {
    return true;
  }

  return false;
}
