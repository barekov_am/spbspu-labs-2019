#include <iostream>
#include <cstdlib>

void task1();
void task2();

int main(int argc, char * argv[]) {
  try {
    if (argc != 2) {
      std::cerr << "Invalid quantity of args." << std::endl;
      return 1;
    }

    char * ptr = nullptr;
    const int task = std::strtol(argv[1], & ptr, 10);
    if (!ptr) {
      std::cerr << "Invalid task." << std::endl;
      return 1;
    }

    switch (task) {
      case 1:
        task1();
        break;
      case 2:
        task2();
        break;
      default:
        std::cerr << "Invalid task." << std::endl;
        return 1;
    }
  } catch (const std::exception & e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }

  return 0;
}
