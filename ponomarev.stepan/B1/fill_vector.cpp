#include <iostream>
#include <vector>

bool fill_vector(std::vector<int> & vector)
{
  bool isZeroExist = false;

  for (int i; (std::cin >> i); vector.push_back(i)) {
    if (i == 0) {
      isZeroExist = true;
      break;
    }
  }

  if (!std::cin.eof() && std::cin.fail()) {
    throw std::runtime_error("Input error");
  }

  return isZeroExist;
}
