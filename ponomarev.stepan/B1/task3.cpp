#include "sort.hpp"

#include <iostream>
#include <vector>
#include <cstdlib>

bool fill_vector(std::vector<int> &);
void handle_vector(std::vector<int> &);

void task3()
{
  std::vector<int> vector;

  bool isZeroExist = fill_vector(vector);

  if (vector.empty()) {
    return;
  } else if (isZeroExist){
    handle_vector(vector);
  } else {
    throw std::runtime_error("Input must be ended by zero");
  }

  print(vector);

  return;
}

void remove_two(std::vector<int> &);
void add_one(std::vector<int> &);

void handle_vector(std::vector<int> & vector)
{
  switch (vector.back()) {
    case 1: {
      remove_two(vector);
      break;
    }
    case 2: {
      add_one(vector);
      break;
    }
    default:
      break;
  }
}

void remove_two(std::vector<int> & vector)
{
  auto iter = vector.begin();
  while (iter != vector.end()) {
    if (*iter % 2 == 0) {
      vector.erase(iter);
    } else {
      iter++;
    }
  }
}

void add_one(std::vector<int> & vector)
{
  auto iter = vector.begin();
  while (iter != vector.end()) {
    iter = ((*iter % 3) == 0) ? vector.insert(++iter, 3, 1) : ++iter;
  }
}
