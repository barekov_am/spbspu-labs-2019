#include "direction.hpp"
#include "sort.hpp"

#include <vector>
#include <random>

void fillRandom(double *,  int);

void task4(int size, Direction direction)
{
  if (size <= 0) {
    throw std::invalid_argument("Size must be bigger than 0");
  }

  std::vector<double> vector(size);

  fillRandom(&vector[0], size);

  print(vector);
  sort<strategy::Braces>(vector, direction);
  print(vector);
}

void fillRandom(double * array, int size)
{
  std::mt19937 gen(time(0));
  std::uniform_real_distribution<double> rand(-1.0, 1.0);

  for (int i = 0; i < size; ++i) {
    array[i] = rand(gen);
  }
}
