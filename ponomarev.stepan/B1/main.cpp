#include "direction.hpp"

#include <iostream>
#include <cstdlib>

void task1(Direction);
void task2(const char *);
void task3();
void task4(int, Direction);

int main(int argc, char * argv[])
{
  try {
    if ((argc < 2) || (argc > 4)) {
      std::cerr << "Invalid arguments!";
      return 1;
    }

    char * ptr = nullptr;
    int task = std::strtol(argv[1], &ptr, 10);
    if (*ptr) {
      std::cerr << "Invalid arguments";
      return 1;
    }

    switch (task) {
      case 1: {
        if (argc != 3) {
          return 1;
        }

        Direction direction = get_direction(argv[2]);
        task1(direction);
        break;
      }
      case 2: {
        if (argc != 3) {
          return 1;
        }

        const char * fileName = argv[2];
        task2(fileName);
        break;
      }
      case 3: {
        if (argc != 2) {
            return 1;
        }

        task3();
        break;
      }
      case 4: {
        if (argc != 4) {
            return 1;
        }

        char * ptr = nullptr;
        int size = std::strtol(argv[3], &ptr, 10);
        if (*ptr) {
          std::cerr << "Invalid arguments";
          return 1;
          break;
        }

        Direction direction = get_direction(argv[2]);
        task4(size, direction);
        break;
      }
      default:
        std::cerr << "Invalid task";
        return 1;
    }
  } catch(const std::exception &e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }

  return 0;
}
