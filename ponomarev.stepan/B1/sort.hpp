#ifndef SORT_HPP
#define SORT_HPP

#include "strategy.hpp"
#include "direction.hpp"

#include <string>
#include <functional>
#include <iostream>

template <template <typename> class Strategy, typename T>
void sort(T & container, const Direction direction)
{
  using value_type = typename T::value_type;
  using Compare = std::function<bool(value_type, value_type)>;

  Compare compare = (direction == Direction::ascending)
      ? Compare(std::greater<value_type>())
      : Compare(std::less<value_type>());

   const auto begin = Strategy<T>::begin(container);
   const auto end = Strategy<T>::end(container);

   for (auto i = begin; i != end; ++i) {
     value_type & current = Strategy<T>::get(container, i);
     for (auto j = i; j != end; ++j) {
       value_type & compared = Strategy<T>::get(container, j);

       if (compare(current, compared)) {
         std::swap(current, compared);
       }
     }
   }
}

template <typename T>
void print(const T & collection)
{
  if (collection.empty()) {
    return;
  }

  for (const auto & elem : collection) {
    std::cout << elem << " ";
  }
  std::cout << std::endl;
}

#endif
