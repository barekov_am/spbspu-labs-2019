#ifndef DIRECTION_HPP
#define DIRECTION_HPP

enum Direction {
  ascending,
  descending
};

Direction get_direction(const char*);

#endif
