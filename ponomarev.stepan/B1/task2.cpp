#include "sort.hpp"

#include <stdexcept>
#include <fstream>
#include <vector>
#include <iostream>
#include <sys/stat.h>
#include <memory>

using arrayC = std::unique_ptr<char[], decltype(&free)>;

const std::size_t SIZE_OF_ARRAY = 256;

void read_file(std::ifstream &, arrayC &, int size, int & index);

void task2(const char * filename)
{
  std::ifstream inputFile(filename);

  arrayC array(static_cast<char*>(malloc(SIZE_OF_ARRAY)), &free);

  int index = 0;
  read_file(inputFile, array, SIZE_OF_ARRAY, index);

  std::vector<char> vector(&array[0], &array[index]);

  for (auto i = vector.begin(); i < vector.end(); std::cout << *(i++));
}

void read_file(std::ifstream & file, arrayC & array, int primarySize, int & index)
{
  if (!file) {
    throw std::runtime_error("Failed openning file");
  }

  int size = primarySize;
  while (!file.eof()) {
    file.read(&array[index], primarySize);
    index += file.gcount();

    if (file.gcount() == primarySize) {
      size += primarySize;
      array = arrayC(static_cast<char*>(realloc(array.release(), size)), &free);

      if (!array) {
        throw std::runtime_error("Realloc failed");
      }
    }
  }
}
