#ifndef STRATEGY_HPP
#define STRATEGY_HPP

#include <iostream>

namespace strategy {
  template <typename Container>
  struct Braces {
      using value_type = typename Container::value_type;

      static unsigned int begin(const Container &) {
        return 0;
      }

      static unsigned int end(const Container & container) {
        return container.size();
      }

      static value_type & get(Container & container, unsigned int index) {
        return container[index];
      }
  };

  template <typename Container>
  struct At {
    using value_type = typename Container::value_type;

    static unsigned int begin(const Container &) {
      return 0;
    }

    static unsigned int end(const Container & container) {
      return container.size();
    }

    static value_type & get(Container & container, unsigned int index) {
      return container.at(index);
    }
  };

  template <typename Container>
  struct Iterator {
    using value_type = typename Container::value_type;

    static auto begin(Container & container) {
      return container.begin();
    }

    static auto end(Container & container) {
      return container.end();
    }

    template <typename I>
    static value_type & get(Container &, I iter) {
      return *iter;
    }
  };
}

#endif
