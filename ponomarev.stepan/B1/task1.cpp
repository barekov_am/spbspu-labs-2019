#include "sort.hpp"
#include "direction.hpp"

#include <iostream>
#include <vector>
#include <stdexcept>
#include <forward_list>

bool fill_vector(std::vector<int> &);

void task1(Direction direction)
{
  std::vector<int> vectorBr;

  fill_vector(vectorBr);

  std::vector<int> vectorAt = vectorBr;
  std::forward_list<int> list(vectorAt.begin(), vectorAt.end());

  sort<strategy::Braces>(vectorBr, direction);
  sort<strategy::At>(vectorAt, direction);
  sort<strategy::Iterator>(list, direction);

  print(vectorBr);
  print(vectorAt);
  print(list);
}
