#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "split.hpp"

int main()
{
  ponomarev::Matrix matrix{};


  ponomarev::shapePtr rectS1(new ponomarev::Rectangle({0, 1}, 5, 5));
  ponomarev::shapePtr circS1(new ponomarev::Circle({-111, -110}, 5));
  ponomarev::shapePtr rectS2(new ponomarev::Rectangle({99, 199}, 2, 2));
  ponomarev::shapePtr circS2(new ponomarev::Circle({1, 1}, 34));



  matrix.add(rectS1);
  matrix.getInfo();
  std::cout << std::endl;

  matrix.add(circS2);
  matrix.getInfo();
  std::cout << std::endl;

  matrix.add(rectS2);
  matrix.getInfo();
  std::cout << std::endl;

  matrix.add(circS1);
  matrix.getInfo();
  std::cout << std::endl;



  return 0;
}
