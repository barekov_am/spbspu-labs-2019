#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace ponomarev {
  class Rectangle : public Shape {
  public:
    Rectangle(const point_t &new_pos, const double width,
        const double heigth);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &new_pos) override;
    void move(const double dx, const double dy) override;
    void scale(const double lambda) override;
    void rotate(const double alpha) override;
    void getInfo() const override;

  private:
    void setCorners();
    rectangle_t rectangle_;
  };
}

#endif //RECTANGLE_HPP
