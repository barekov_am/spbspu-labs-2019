#include <iostream>
#include <string>
#include <cctype>

bool isEmpty(std::istream & is)
{
  while (std::isblank(is.peek())) {
    is.get();
  }

  if (is.peek() == '\n') {
    return true;
  }

  return false;
}
