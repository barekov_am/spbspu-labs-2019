#include "boost/test/auto_unit_test.hpp"
#include <memory>

#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

const double TOLERANCE = 0.01;

BOOST_AUTO_TEST_SUITE(test_composite)

BOOST_AUTO_TEST_CASE(test_shape_constructor)
{
  BOOST_CHECK_THROW(ponomarev::CompositeShape(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(test_area)
{
  ponomarev::shapePtr testRec(new ponomarev::Rectangle({0, 0}, 1, 1));
  ponomarev::CompositeShape testComposite(testRec);
  double testArea(testRec->getArea());
  BOOST_CHECK_CLOSE_FRACTION(testArea, testComposite.getArea(), TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_move_to_point)
{
  std::shared_ptr<ponomarev::Shape> testCir(new ponomarev::Circle({0, 0}, 1));
  ponomarev::CompositeShape testComposite(testCir);
  testComposite.move({12, 1});
  BOOST_CHECK_CLOSE_FRACTION(testComposite.getFrameRect().pos.x, 12, TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(testComposite.getFrameRect().pos.y, 1, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_move)
{
  ponomarev::shapePtr testCir(new ponomarev::Circle({0, 0}, 1));
  ponomarev::CompositeShape testComposite(testCir);
  ponomarev::rectangle_t testRectangle_t(testCir->getFrameRect());

  testComposite.move(9, 9);
  BOOST_CHECK_CLOSE_FRACTION(testRectangle_t.width, testComposite.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(testRectangle_t.height, testComposite.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(testComposite.getFrameRect().pos.x, 9, TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(testComposite.getFrameRect().pos.y, 9, TOLERANCE);

  testComposite.move({0, 0});
  BOOST_CHECK_CLOSE_FRACTION(testRectangle_t.width, testComposite.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(testRectangle_t.height, testComposite.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(testComposite.getFrameRect().pos.x, 0, TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(testComposite.getFrameRect().pos.y, 0, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_scale)
{
  ponomarev::shapePtr testRec(new ponomarev::Rectangle({0, 0}, 2, 2));
  ponomarev::CompositeShape testComposite(testRec);
  double testArea = testComposite.getArea();
  testComposite.scale(2);
  BOOST_CHECK_CLOSE_FRACTION(testComposite.getArea(), testArea * 4, TOLERANCE);
  BOOST_CHECK_THROW (testComposite.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(test_addShape)
{
  ponomarev::CompositeShape testComposite;
  ponomarev::shapePtr testCir(new ponomarev::Circle({0, 0}, 1));
  testComposite.add(testCir);
  BOOST_CHECK_CLOSE_FRACTION((double) testComposite.getSize(), 1, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_removeShape)
{
  ponomarev::shapePtr testCir(new ponomarev::Circle({0, 0}, 1));
  ponomarev::CompositeShape testComposite(testCir);
  testComposite.remove(0);
  BOOST_CHECK_CLOSE_FRACTION((double) testComposite.getSize(), 0, TOLERANCE);
  BOOST_CHECK_THROW (testComposite.remove(20), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END();
