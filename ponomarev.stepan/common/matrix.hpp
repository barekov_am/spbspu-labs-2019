#ifndef MATRIX_HPP
#define MATRIX_HPP
#include "shape.hpp"
#include "cover.hpp"

#include <cstdlib>

namespace ponomarev {
  using shapePtr = std::shared_ptr<Shape>;
  using shapeArray = std::unique_ptr<std::shared_ptr<Shape>[]>;

  class Matrix {
  public:
    Matrix();
    Matrix(const Matrix & other);
    Matrix(Matrix && other);
    ~Matrix() = default;

    Matrix &operator =(const Matrix & other);
    Matrix &operator =(Matrix && other);
    shapeArray operator [](const int index) const;

    bool operator ==(const Matrix & other) const noexcept;
    bool operator !=(const Matrix & other) const noexcept;

    void add(const shapePtr & shape);
    void getInfo() const;

  private:
    inline int getIndex(int r, int c, const int clmns) const noexcept
    {
      return (r * clmns  + c);
    }

    int rows_;
    int columns_;
    shapeArray shapeArray_;
  };
}

#endif // MATRIX_HPP
