#include "matrix.hpp"
#include "split.hpp"

#include <iostream>
#include <cmath>

ponomarev::Matrix::Matrix():
rows_(0),
columns_(0),
shapeArray_(nullptr)
{}

ponomarev::Matrix::Matrix(const ponomarev::Matrix & other):
  rows_(other.rows_),
  columns_(other.columns_),
  shapeArray_(new shapePtr[other.columns_])
{
  for (int i = 0; i < columns_; i++)
  {
    shapeArray_[i] = other.shapeArray_[i];
  }
}

ponomarev::Matrix::Matrix(ponomarev::Matrix && other):
  rows_(other.rows_),
  columns_(other.columns_),
  shapeArray_(std::move(other.shapeArray_))
{}

ponomarev::Matrix & ponomarev::Matrix::operator =(const ponomarev::Matrix & other)
{
  if (this != &other)
  {
    columns_ = other.columns_;
    rows_ = other.rows_;
    shapeArray newArray = std::make_unique<shapePtr[]>(other.columns_);
    for (int i = 0; i < columns_; i++)
    {
      newArray[i] = other.shapeArray_[i];
    }
    shapeArray_.swap(newArray);
  }

  return *this;
}

ponomarev::Matrix & ponomarev::Matrix::operator =(ponomarev::Matrix && other)
{
  if (this != &other)
  {
    columns_ = other.columns_;
    rows_ = other.rows_;
    shapeArray_ = std::move(other.shapeArray_);
  }

  return *this;
}

std::unique_ptr<std::shared_ptr<ponomarev::Shape>[]> ponomarev::Matrix::operator [](const int index) const
{
  if ((index >= rows_) || (index < 0)) {
    throw std::invalid_argument("Invalid index");
  }

  shapeArray cacheArray(new shapePtr[columns_]);
  for (int c = 0; c < columns_; ++c) {
    cacheArray[c] = shapeArray_[getIndex(index, c, columns_)];
  }

  return cacheArray;
}

bool ponomarev::Matrix::operator ==(const Matrix & other) const noexcept
{
  if ((columns_ != other.columns_) || (rows_ != other.rows_))
  {
    return false;
  }

  for (int i = 0; i < columns_; i++)
  {
    if (shapeArray_[i] != other.shapeArray_[i])
    {
      return false;
    }
  }

  return true;
}

bool ponomarev::Matrix::operator !=(const Matrix & other) const noexcept
{
  return (!(*this == other));
}

void ponomarev::Matrix::add(const shapePtr & shape)
{
  if (shape == nullptr) {
    throw std::invalid_argument("Null ptr ERROR");
  }

  if (columns_ == 0) {
    ++rows_;
    ++columns_;

    shapeArray cacheArray(new shapePtr[1]);
    cacheArray[0] = shape;

    shapeArray_.swap(cacheArray);

    return;
  }

  const int oldRows = rows_;
  const int oldColumns = columns_;

  int newLevel = (rows_);

  int * shapesOnLevel(new int[rows_]);
  for (int k = 0; k < rows_; ++k) {
    shapesOnLevel[k] = 0;
  }

  bool levelFinded = false;
  for (int r = (rows_ - 1); r >= 0; --r) {
    for (int c = 0; c < columns_; ++c) {

      if (shapeArray_[this->getIndex(r, c, oldColumns)] != nullptr) {
        shapesOnLevel[r] += 1;
        if (covering(shapeArray_[this->getIndex(r, c, oldColumns)], shape) && (!levelFinded)) {
          levelFinded = true;
          newLevel = (r + 1);
        }
      }

    }
    if (!levelFinded) {
      newLevel = r;
    }
  }

  (newLevel > (rows_ - 1)) ? rows_++ : rows_;

  if (newLevel <= (rows_ - 1) && (rows_ == oldRows)) {
    (shapesOnLevel[newLevel] == columns_) ? columns_++ : columns_;
  }

  int newSize = rows_ * columns_;
  shapeArray cacheArray(new shapePtr[newSize]);
  for (int k = 0; k < newSize; ++k) {
    cacheArray[k] = nullptr;
  }

  for (int r = 0; r < rows_; ++r) {
    for (int c = 0; c < columns_; ++c) {
      if ((r < oldRows) && (c < oldColumns)) {
        cacheArray[this->getIndex(r, c, columns_)] = shapeArray_[this->getIndex(r, c, oldColumns)];
      }
    }
  }

  if (rows_ != oldRows) {
    cacheArray[this->getIndex(newLevel, 0, columns_)] = shape;
  } else if (columns_ != oldColumns) {
    cacheArray[this->getIndex(newLevel, oldColumns, columns_)] = shape;
  } else {
    cacheArray[this->getIndex(newLevel, shapesOnLevel[newLevel], columns_)] = shape;
  }

  shapeArray_.swap(cacheArray);
  delete [] shapesOnLevel;
}

void ponomarev::Matrix::getInfo() const
{
  if (columns_ == 0)
  {
    throw std::invalid_argument("There are no shapes");
  }

  std::cout << "\t";
  for (int c = 0; c < columns_; c++) {
    std::cout << c + 1 << "\t";
  }

  for (int r = 0; r < rows_; r++) {
    std::cout << "\n" << r + 1 << "\t";
    for (int c = 0; c < columns_; c++) {
      if (shapeArray_[this->getIndex(r, c, columns_)] != nullptr) {
        std::cout << "*       ";
      } else {
        std::cout << "\t";
      }
    }
  }
  std::cout << "\n";
}
