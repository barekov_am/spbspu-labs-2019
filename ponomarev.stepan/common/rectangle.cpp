#include "rectangle.hpp"

#include <iostream>
#include <cmath>
#include <stdexcept>

ponomarev::Rectangle::Rectangle(const point_t &new_pos, const double width,
    const double height)
{
  if (width <= 0) {
    throw std::invalid_argument("invalid width");
  }
  if (height <= 0) {
    throw std::invalid_argument("invalid height");
  }

  rectangle_.pos = new_pos;
  rectangle_.width = width;
  rectangle_.height = height;

  setCorners();

}

void ponomarev::Rectangle::move(const double dx, const double dy)
{
  rectangle_.pos.x += dx;
  rectangle_.pos.y += dy;

  for (std::size_t k = 0; k < 4; ++k) {
    rectangle_.corns[k].x += dx;
    rectangle_.corns[k].y += dy;
  }
}

void ponomarev::Rectangle::move(const point_t &new_pos)
{
  point_t deltaCoors{(new_pos.x - rectangle_.pos.x), (new_pos.y - rectangle_.pos.y)};

  rectangle_.pos = new_pos;

  for (std::size_t k = 0; k < 4; ++k) {
    rectangle_.corns[k].x += deltaCoors.x;
    rectangle_.corns[k].y += deltaCoors.y;
  }
}

void ponomarev::Rectangle::scale(const double lambda)
{
  if (lambda <= 0) {
    throw std::invalid_argument("invalid coefficient");
  }
  rectangle_.width *= lambda;
  rectangle_.height *= lambda;

  rectangle_.corns[1] = {(rectangle_.corns[1].x * lambda), (rectangle_.corns[1].y * lambda)};

  rectangle_.corns[2] = {(rectangle_.corns[2].x * lambda), (rectangle_.corns[2].y * lambda)};

  rectangle_.corns[3] = {(rectangle_.corns[3].x * lambda), (rectangle_.corns[3].y * lambda)};
}

void ponomarev::Rectangle::rotate(const double alpha)
{
  point_t tmpCorns[4] = {rectangle_.corns[0], rectangle_.corns[1], rectangle_.corns[2], rectangle_.corns[3]};

  rectangle_.corns[0].x = rectangle_.pos.x + (tmpCorns[0].x - rectangle_.pos.x) * cos(M_PI * alpha) -
      (tmpCorns[0].y - rectangle_.pos.y) * sin(M_PI * alpha);
  rectangle_.corns[0].y = rectangle_.pos.y + (tmpCorns[0].x - rectangle_.pos.x) * cos(M_PI * alpha) +
      (tmpCorns[0].y - rectangle_.pos.y) * sin(M_PI * alpha);

  rectangle_.corns[1].x = rectangle_.pos.x + (tmpCorns[1].x - rectangle_.pos.x) * cos(M_PI * alpha) -
      (tmpCorns[1].y - rectangle_.pos.y) * sin(M_PI * alpha);
  rectangle_.corns[1].y = rectangle_.pos.y + (tmpCorns[1].x - rectangle_.pos.x) * cos(M_PI * alpha) +
      (tmpCorns[1].y - rectangle_.pos.y) * sin(M_PI * alpha);

  rectangle_.corns[2].x = rectangle_.pos.x + (tmpCorns[2].x - rectangle_.pos.x) * cos(M_PI * alpha) -
      (tmpCorns[2].y - rectangle_.pos.y) * sin(M_PI * alpha);
  rectangle_.corns[2].y = rectangle_.pos.y + (tmpCorns[2].x - rectangle_.pos.x) * cos(M_PI * alpha) +
      (tmpCorns[2].y - rectangle_.pos.y) * sin(M_PI * alpha);

  rectangle_.corns[3].x = rectangle_.pos.x + (tmpCorns[3].x - rectangle_.pos.x) * cos(M_PI * alpha) -
      (tmpCorns[3].y - rectangle_.pos.y) * sin(M_PI * alpha);
  rectangle_.corns[3].y = rectangle_.pos.y + (tmpCorns[3].x - rectangle_.pos.x) * cos(M_PI * alpha) +
      (tmpCorns[3].y - rectangle_.pos.y) * sin(M_PI * alpha);
}

double ponomarev::Rectangle::getArea() const
{
  return rectangle_.width * rectangle_.height;
}

ponomarev::rectangle_t ponomarev::Rectangle::getFrameRect() const
{
  double lowX{rectangle_.corns[0].x};
  double lowY{rectangle_.corns[0].y};

  for (std::size_t k = 1; k < 4; ++k) {

    if (rectangle_.corns[k].x < lowX) {
      lowX = rectangle_.corns[k].x;
    }

    if (rectangle_.corns[k].y < lowY) {
      lowY = rectangle_.corns[k].y;
    }
  }

  const double newWidth = (rectangle_.pos.x - lowX) * 2;
  const double newHeight= (rectangle_.pos.y - lowY) * 2;

  point_t tmpCorns[4] = {{0, 0}, {0, 0}, {0, 0}, {0, 0}};
  tmpCorns[0] = {rectangle_.pos.x - newWidth / 2, rectangle_.pos.y + newHeight / 2};
  tmpCorns[1] = {rectangle_.pos.x + newWidth / 2, rectangle_.pos.y + newHeight / 2};
  tmpCorns[2] = {rectangle_.pos.x - newWidth / 2, rectangle_.pos.y - newHeight / 2};
  tmpCorns[3] = {rectangle_.pos.x + newWidth / 2, rectangle_.pos.y - newHeight / 2};

  rectangle_t frameRect{rectangle_.pos, {tmpCorns[0], tmpCorns[1], tmpCorns[2], tmpCorns[3]}, newWidth, newHeight};

  return frameRect;
}

void ponomarev::Rectangle::getInfo() const
{
  rectangle_t rectangle = getFrameRect();
  std::cout << "--------------------------" << std::endl;
  std::cout << "Centre:\n" << "x = " << rectangle.pos.x << " " << "y = "
      << rectangle.pos.y << std::endl;
  std::cout << std::endl;

  std::cout << "Side:\n" << "Width = " << rectangle.width << "\n" << "Height = "
      << rectangle.height << std::endl;
  std::cout << std::endl;

  std::cout << "Corners:" << std::endl;
  for (std::size_t k = 0; k < 4; ++k) {
    std::cout << "X" << k + 1 << ": " << getFrameRect().corns[k].x << std::endl;
    std::cout << "Y" << k + 1<< ": " << getFrameRect().corns[k].y << std::endl;
    std::cout << std::endl;
  }
  std::cout << std::endl;

  std::cout << "Area : " << getArea() << std::endl;
  std::cout << "--------------------------" << std::endl;
}

void ponomarev::Rectangle::setCorners()
{
  rectangle_.corns[0] = {(rectangle_.pos.x - rectangle_.width / 2), (rectangle_.pos.y + rectangle_.height / 2)};

  rectangle_.corns[1] = {(rectangle_.pos.x + rectangle_.width / 2), (rectangle_.pos.y + rectangle_.height / 2)};

  rectangle_.corns[2] = {(rectangle_.pos.x - rectangle_.width / 2), (rectangle_.pos.y - rectangle_.height / 2)};

  rectangle_.corns[3] = {(rectangle_.pos.x + rectangle_.width / 2), (rectangle_.pos.y - rectangle_.height / 2)};
}
