#define _USE_MATH_DEFINES
#include "circle.hpp"

#include <iostream>
#include <stdexcept>
#include <cmath>

ponomarev::Circle::Circle(const point_t pos, const double radius):
  pos_(pos),
  radius_(radius)
{
  if (radius <= 0) {
    throw std::invalid_argument("invalid radius");
  }
}

void ponomarev::Circle::move(const point_t &new_pos)
{
  pos_ = new_pos;
}

void ponomarev::Circle::move(const double dx, const double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void ponomarev::Circle::scale(const double lambda)
{
  if (lambda <= 0) {
    throw std::invalid_argument("invalid coefficient");
  }
  radius_ *= lambda;
}

void ponomarev::Circle::rotate(const double) noexcept
{
  return;
}

double ponomarev::Circle::getArea() const
{
  return (M_PI * radius_ * radius_);
}

ponomarev::rectangle_t ponomarev::Circle::getFrameRect() const
{
  return rectangle_t {
    pos_,
    {{pos_.x - radius_ * 2, pos_.y + radius_ * 2},{pos_.x + radius_ * 2, pos_.y + radius_ * 2},
      {pos_.x - radius_ * 2, pos_.y - radius_ * 2},{pos_.x + radius_ * 2, pos_.y - radius_ * 2}},
    radius_ * 2,
    radius_ * 2,
  };
}

void ponomarev::Circle::getInfo() const
{
  std::cout << "--------------------------" << std::endl;
  std::cout << "Centre:\n" << "x = " << pos_.x << " y = " << pos_.y << std::endl;
  std::cout << std::endl;
  std::cout << "Radius = " << radius_ << std::endl;
  std::cout << std::endl;

  rectangle_t rectangle = getFrameRect();
  std::cout << "Out rectangle:\n" << "Width = " << rectangle.width << "\n" << "Height = " << rectangle.height << "\n"
      << "Centre: " << "x = " << rectangle.pos.x << " y = " << rectangle.pos.y << std::endl;
  std::cout << std::endl;

  std::cout << "Area : " << getArea() << std::endl;
  std::cout << "--------------------------" << std::endl;
}
