#ifndef MANIPULATOR_HPP
#define MANIPULATOR_HPP
#include <istream>

template <typename CharT, typename Traits>
std::basic_istream<CharT, Traits> & blank(std::basic_istream<CharT, Traits> & is)
{
  using iBuf = std::basic_streambuf<CharT>;
  using int_type = typename Traits::int_type;
  const int_type eof = Traits::eof();

  iBuf * buffer = is.rdbuf();
  int_type c = buffer->sgetc();

  while (std::isblank(c) && !Traits::eq_int_type(c, eof)) {
    c = buffer->snextc();
  }

  return is;
}

#endif
