#ifndef A4_SPLIT_HPP
#define A4_SPLIT_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace ponomarev {
  Matrix split(CompositeShape & composite);
  Matrix split(const shapeArray & shapeList, const int & size);
}

#endif //A4_SPLIT_HPP
