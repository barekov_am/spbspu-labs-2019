#ifndef COMPOSITE_SHAPE
#define COMPOSITE_SHAPE
#include "shape.hpp"

#include <cstdlib>
#include <memory>

namespace ponomarev {
  using shapePtr = std::shared_ptr<Shape>;
  using shapeArray = std::unique_ptr<std::shared_ptr<Shape>[]>;

  class CompositeShape : public Shape {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &other);
    CompositeShape(CompositeShape &&other);
    CompositeShape(const shapePtr &shape);
    ~CompositeShape() = default;

    CompositeShape &operator=(const CompositeShape &other) noexcept;
    CompositeShape &operator=(CompositeShape &&other) noexcept;
    shapePtr operator[](const int index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &new_pos) override;
    void move(const double dx, const double dy) override;
    void scale(const double lambda) override;
    void rotate(const double alpha) override;
    void add(const shapePtr &shape);
    void remove(const int index);
    void getInfo() const override;
    int getSize() const;

  private:
    int size_;
    shapeArray shapeContainer_;
  };
}

#endif //COMPOSITE_SHAPE
