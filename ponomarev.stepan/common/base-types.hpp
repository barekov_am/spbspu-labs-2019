#ifndef BASE_TYPES
#define BASE_TYPES

namespace ponomarev {
  struct point_t {
    double x;
    double y;
  };

  struct rectangle_t {
    point_t pos;
    point_t corns[4];
    double width;
    double height;
  };
}

#endif //BASE_TYPES
