#include <cstdlib>
#include <boost/test/auto_unit_test.hpp>

#include "shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "split.hpp"

using shapePtr = std::shared_ptr<ponomarev::Shape>;
using shapeArray = std::unique_ptr<std::shared_ptr<ponomarev::Shape>[]>;

BOOST_AUTO_TEST_SUITE(testMatrix)

double INACCURACY = 0.001;

BOOST_AUTO_TEST_CASE(testCopyConstructor)
{
  ponomarev::shapePtr rect(new ponomarev::Rectangle({0, 1}, 5, 5));
  ponomarev::shapePtr circle(new ponomarev::Circle({-111, -110}, 5));
  ponomarev::shapePtr rect1(new ponomarev::Rectangle({99, 199}, 2, 2));
  ponomarev::shapePtr circle1(new ponomarev::Circle({1, 1}, 34));
  ponomarev::CompositeShape composite{};
  composite.add(rect1);
  composite.add(circle1);
  ponomarev::Matrix matrix{};
  matrix.add(rect);
  matrix.add(circle);
  BOOST_CHECK_NO_THROW(ponomarev::Matrix matrix1(matrix));
  ponomarev::Matrix matrix2(matrix);
  BOOST_CHECK_EQUAL(true, matrix == matrix2);
}

BOOST_AUTO_TEST_CASE(testCopyOperator)
{
  ponomarev::shapePtr rect(new ponomarev::Rectangle({0, 1}, 5, 5));
  ponomarev::shapePtr circle(new ponomarev::Circle({-111, -110}, 5));
  ponomarev::shapePtr rect1(new ponomarev::Rectangle({99, 199}, 2, 2));
  ponomarev::shapePtr circle1(new ponomarev::Circle({1, 1}, 34));
  ponomarev::CompositeShape composite{};
  composite.add(rect1);
  composite.add(circle1);
  ponomarev::Matrix matrix{};
  matrix.add(rect);
  matrix.add(circle);
  ponomarev::Matrix matrix1{};
  BOOST_CHECK_NO_THROW(matrix1 = matrix);
  BOOST_CHECK_EQUAL(true, matrix == matrix1);
}

BOOST_AUTO_TEST_CASE(testMoveConstructorAndOperator)
{
  ponomarev::shapePtr rect(new ponomarev::Rectangle({0, 1}, 5, 5));
  ponomarev::shapePtr circle(new ponomarev::Circle({-111, -110}, 5));
  ponomarev::shapePtr rect1(new ponomarev::Rectangle({99, 199}, 2, 2));
  ponomarev::shapePtr circle1(new ponomarev::Circle({1, 1}, 34));

  ponomarev::Matrix matrix{};
  matrix.add(rect);
  matrix.add(circle);
  matrix.add(rect1);
  matrix.add(circle1);
  BOOST_CHECK_NO_THROW(ponomarev::Matrix matrix1(std::move(matrix)));
  ponomarev::Matrix matrix2{};
  BOOST_CHECK_NO_THROW(matrix2 = std::move(matrix));
}

BOOST_AUTO_TEST_CASE(testIndexOperator)
{
  ponomarev::shapePtr rect(new ponomarev::Rectangle({0, 1}, 5, 5));
  ponomarev::shapePtr circle(new ponomarev::Circle({-111, -110}, 5));
  ponomarev::shapePtr rect1(new ponomarev::Rectangle({99, 199}, 2, 2));

  ponomarev::Matrix matrix{};
  matrix.add(rect);
  matrix.add(circle);
  matrix.add(rect1);

  BOOST_CHECK_THROW(matrix[52], std::invalid_argument);
  BOOST_CHECK_THROW(matrix[-3], std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testEqualOperator)
{
  ponomarev::shapePtr rect(new ponomarev::Rectangle({0, 1}, 5, 5));
  ponomarev::shapePtr circle(new ponomarev::Circle({-111, -110}, 5));
  ponomarev::Matrix matrix{};
  matrix.add(rect);
  matrix.add(circle);
  ponomarev::Matrix matrix1(matrix);
  BOOST_CHECK_EQUAL(true, matrix == matrix1);
  ponomarev::Matrix matrix2{};
  ponomarev::Matrix matrix3{};
  BOOST_CHECK_EQUAL(true, matrix2 == matrix3);
}

BOOST_AUTO_TEST_CASE(testNotEqualOperator)
{
  ponomarev::shapePtr rect(new ponomarev::Rectangle({0, 1}, 5, 5));
  ponomarev::shapePtr circle(new ponomarev::Circle({-111, -110}, 5));
  shapePtr rect1(new ponomarev::Rectangle({99, 199}, 2, 2));
  ponomarev::Matrix matrix{};
  matrix.add(rect);
  ponomarev::Matrix matrix1(matrix);
  BOOST_CHECK_EQUAL(false, matrix != matrix1);
  matrix.add(circle);
  BOOST_CHECK_EQUAL(true, matrix != matrix1);
  matrix1.add(rect1);
  BOOST_CHECK_EQUAL(true, matrix != matrix1);
}

BOOST_AUTO_TEST_CASE(emptyMatrix)
{
  ponomarev::Matrix matrix{};
  BOOST_CHECK_THROW(matrix.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(matrix.getInfo(), std::invalid_argument);
  BOOST_CHECK_THROW(matrix[0], std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testSplit)
{
  shapePtr rect(new ponomarev::Rectangle({0, 1}, 5, 5));
  shapePtr circle(new ponomarev::Circle({0, 0}, 10));
  shapePtr rect1(new ponomarev::Rectangle({110, 99}, 2, 2));
  shapePtr circle1(new ponomarev::Circle({122, 233}, 2));

  shapePtr cs_rect1 = rect1;
  shapePtr cs_circle1 = circle1;

  ponomarev::CompositeShape composite;
  composite.add(cs_rect1);
  composite.add(cs_circle1);
  std::shared_ptr<ponomarev::CompositeShape> comp(new ponomarev::CompositeShape(composite));

  shapePtr figure0 = rect;
  shapePtr figure1 = circle;
  std::shared_ptr<ponomarev::CompositeShape> figure2 = comp;

  shapeArray testArray = std::make_unique<shapePtr[]>(3);
  testArray[0] = figure0;
  testArray[1] = figure1;
  testArray[2] = figure2;

  ponomarev::Matrix matrix;
  matrix = ponomarev::split(testArray, 3);

  BOOST_CHECK_EQUAL(true, matrix[0][0] == figure0);
  BOOST_CHECK_EQUAL(true, matrix[1][0] == figure1);
  BOOST_CHECK_EQUAL(true, matrix[0][1] == figure2);

  ponomarev::Matrix matrix1;
  matrix1 = ponomarev::split(composite);
  BOOST_CHECK_EQUAL(true, matrix1[0][0] == cs_rect1);
  BOOST_CHECK_EQUAL(true, matrix1[0][1] == cs_circle1);
}

BOOST_AUTO_TEST_SUITE_END();
