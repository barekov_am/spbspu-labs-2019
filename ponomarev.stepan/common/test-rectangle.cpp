#include "boost/test/auto_unit_test.hpp"

#include "rectangle.hpp"

const double TOLERANCE = 0.01;

BOOST_AUTO_TEST_SUITE(test_rectangle)

BOOST_AUTO_TEST_CASE(test_move_in_dot)
{
  ponomarev::Rectangle rect({0, 0}, 2, 2);
  ponomarev::rectangle_t test_rect = rect.getFrameRect();
  rect.move({-31, 12});
  BOOST_CHECK_CLOSE_FRACTION(test_rect.height * test_rect.width, rect.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(test_rect.height, rect.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(test_rect.width, rect.getFrameRect().width, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_move_on_delta)
{
  ponomarev::Rectangle rect({0, 0}, 2, 2);
  ponomarev::rectangle_t test_rect = rect.getFrameRect();
  rect.move(18, 42);
  BOOST_CHECK_CLOSE_FRACTION(test_rect.height * test_rect.width, rect.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(test_rect.height, rect.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(test_rect.width, rect.getFrameRect().width, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_area)
{
  ponomarev::Rectangle rect({0, 0}, 1, 1);
  BOOST_REQUIRE_CLOSE(rect.getArea(), 1, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_scaling_error)
{
  ponomarev::Rectangle rect({0, 0}, 2, 2);
  BOOST_CHECK_THROW(rect.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(test_area_after_scale)
{
  ponomarev::Rectangle rect({0, 0}, 2, 2);
  ponomarev::Rectangle testRect = rect;
  const double scaleLambda(3);
  rect.scale(scaleLambda);
  BOOST_CHECK_CLOSE_FRACTION(rect.getArea(), testRect.getArea() * scaleLambda * scaleLambda, TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(rect.getFrameRect().height, testRect.getFrameRect().height * scaleLambda, TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(rect.getFrameRect().width, testRect.getFrameRect().width * scaleLambda, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_constructor)
{
  BOOST_CHECK_THROW(ponomarev::Rectangle({0, 0}, -8, 10.0), std::invalid_argument);
  BOOST_CHECK_THROW(ponomarev::Rectangle({0, 0}, 8, -10.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END();
