#include "boost/test/auto_unit_test.hpp"

#include "circle.hpp"

const double TOLERANCE = 0.01;

BOOST_AUTO_TEST_SUITE(test_circle)

BOOST_AUTO_TEST_CASE(test_move_in_dot)
{
  ponomarev::Circle circle({0, 0}, 3);
  ponomarev::rectangle_t test_rect = circle.getFrameRect();
  double test_area = circle.getArea();
  circle.move({38, 11});
  BOOST_CHECK_CLOSE_FRACTION(test_area, circle.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(test_rect.height, circle.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(test_rect.width, circle.getFrameRect().width, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_move_on_delta)
{
  ponomarev::Circle circle({0, 0}, 3);
  ponomarev::rectangle_t test_rect = circle.getFrameRect();
  double test_area = circle.getArea();
  circle.move(12, 1);
  BOOST_CHECK_CLOSE_FRACTION(test_area, circle.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(test_rect.height, circle.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(test_rect.width, circle.getFrameRect().width, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_area)
{
  ponomarev::Circle circle({0, 0}, 3);
  double test_area = circle.getArea();
  BOOST_REQUIRE_CLOSE(circle.getArea(), test_area, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_scaling_error)
{
  ponomarev::Circle circle({0, 0}, 3);
  BOOST_CHECK_THROW(circle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(test_area_after_scale)
{
  ponomarev::Circle circle({0, 0}, 3);
  ponomarev::Circle testCircle = circle;
  const double scaleLambda(3);
  circle.scale(scaleLambda);
  BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), testCircle.getArea() * scaleLambda * scaleLambda, TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().height, testCircle.getFrameRect().height * scaleLambda, TOLERANCE);
  BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().width, testCircle.getFrameRect().width * scaleLambda, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_constructor)
{
  BOOST_CHECK_THROW(ponomarev::Circle({0, 0}, -3.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END();
