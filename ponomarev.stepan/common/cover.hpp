#ifndef A4_COVER_HPP
#define A4_COVER_HPP
#include "shape.hpp"
#include <memory>

namespace ponomarev {
  using shapePtr = std::shared_ptr<Shape>;

  bool covering(const shapePtr & shape1, const shapePtr & shape2);
}

#endif //A4_COVER_HPP
