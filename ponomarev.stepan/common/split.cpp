#include "split.hpp"

ponomarev::Matrix ponomarev::split(CompositeShape & composite)
{
  Matrix tmpMatrix;
  for (int k = 0; k < composite.getSize(); ++k) {
    tmpMatrix.add(composite[k]);
  }

  return tmpMatrix;
}

ponomarev::Matrix ponomarev::split(const shapeArray & shapeList, const int & size)
{
  Matrix tmpMatrix;
  for (int k = 0; k < size; ++k) {
    tmpMatrix.add(shapeList[k]);
  }

  return tmpMatrix;
}
