#include "composite-shape.hpp"

#include <stdexcept>
#include <iostream>
#include <cstdlib>
#include <memory>
#include <cmath>

ponomarev::CompositeShape::CompositeShape():
  size_(0),
  shapeContainer_(nullptr)
{}

ponomarev::CompositeShape::CompositeShape(const ponomarev::CompositeShape &other)
{
  size_ = other.size_;

  shapeArray cacheArray(new shapePtr[other.size_]);
  for (int i = 0; i < size_; ++i) {
    cacheArray[i] = other.shapeContainer_[i];
  }

  shapeContainer_.reset();
  shapeContainer_.swap(cacheArray);
}

ponomarev::CompositeShape::CompositeShape(const shapePtr &shape):
  size_(1),
  shapeContainer_(new shapePtr[1])
{
  if (shape == nullptr) {
    throw std::invalid_argument("Invalid ptr");
  }

  shapeContainer_[0] = shape;
}

ponomarev::CompositeShape::CompositeShape(ponomarev::CompositeShape &&other)
{
  shapeContainer_.reset();
  shapeContainer_ = nullptr;

  size_ = other.size_;

  other.size_ = 0;
  shapeContainer_.swap(other.shapeContainer_);
}

ponomarev::CompositeShape &ponomarev::CompositeShape::operator=(const ponomarev::CompositeShape &other) noexcept
{
  if (this == &other) {
    return *this;
  }

  if (shapeContainer_ != nullptr) {
    shapeContainer_.reset();
    shapeContainer_ = nullptr;
  }

  size_ = other.size_;
  shapeArray cacheArray(new shapePtr[other.size_]);
  for (int i = 0; i < size_; ++i) {
    cacheArray[i] = other.shapeContainer_[i];
  }
  shapeContainer_.swap(cacheArray);

  return *this;
}

ponomarev::CompositeShape &ponomarev::CompositeShape::operator=(ponomarev::CompositeShape &&other) noexcept
{
  if (this == &other) {
    return *this;
  }

  shapeContainer_.reset();
  shapeContainer_ = nullptr;

  size_ = other.size_;

  other.size_ = 0;
  shapeContainer_.swap(other.shapeContainer_);

  return *this;
}

std::shared_ptr<ponomarev::Shape> ponomarev::CompositeShape::operator[](const int index) const
{
  if (index >= size_) {
    throw std::invalid_argument("Invalid index");
  }

  return shapeContainer_[index];
}

double ponomarev::CompositeShape::getArea() const
{
  if (size_ == 0) {
    return 0;
  }

  double totalArea(0);
  for (int i = 0; i < size_; ++i) {
    totalArea += shapeContainer_[i]->getArea();
  }

  return totalArea;
}

ponomarev::rectangle_t ponomarev::CompositeShape::getFrameRect() const
{
  if (size_ == 0) {
    return {{0, 0}, {{0, 0}, {0, 0}, {0, 0}, {0, 0}}, 0, 0};
  }

  point_t center{0, 0};
  for (int i = 0; i < size_; ++i) {
    center.x += shapeContainer_[i]->getFrameRect().pos.x;
    center.y += shapeContainer_[i]->getFrameRect().pos.y;
  }
  center.x /= size_;
  center.y /= size_;

  double left, right, top, bot;
  left = right = top = bot = 0;
  rectangle_t totalFrameRect;
  for (int i = 0; i < size_; ++i) {
    totalFrameRect = shapeContainer_[i]->getFrameRect();
    if (left >= totalFrameRect.pos.x - totalFrameRect.width / 2) {
      left = totalFrameRect.pos.x - totalFrameRect.width / 2;
    }
    if (right <= totalFrameRect.pos.x + totalFrameRect.width / 2) {
      right = totalFrameRect.pos.x + totalFrameRect.width / 2;
    }
    if (bot >= totalFrameRect.pos.x - totalFrameRect.height / 2) {
      bot = totalFrameRect.pos.x - totalFrameRect.height / 2;
    }
    if (top <= totalFrameRect.pos.x + totalFrameRect.height / 2) {
      top = totalFrameRect.pos.x + totalFrameRect.height / 2;
    }
  }

  totalFrameRect = {center, {{left, top}, {right, top}, {left, bot}, {right, bot}},
      fabs((2 * (right - center.x))), fabs((2 * (top - center.y)))};
  return totalFrameRect;
}

void ponomarev::CompositeShape::move(const point_t &new_pos)
{
  if (size_ == 0) {
    std::cout << "No shapes for move" << std::endl;
    return;
  }

  for (int i = 0; i < size_; ++i) {
    shapeContainer_[i]->move(new_pos);
  }
}

void ponomarev::CompositeShape::move(const double dx, const double dy)
{
  if (size_ == 0) {
    std::cout << "No shapes to move" << std::endl;
    return;
  }

  for (int i = 0; i < size_; ++i) {
    shapeContainer_[i]->move(dx, dy);
  }
}

void ponomarev::CompositeShape::scale(const double lambda)
{
  if (size_ == 0) {
    std::cout << "No shapes to scale" << std::endl;
    return;
  }

  for (int i = 0; i < size_; ++i) {
    shapeContainer_[i]->scale(lambda);
  }
}

void ponomarev::CompositeShape::rotate(const double alpha)
{
  if (size_ == 0) {
    std::cout << "Nothing to rotate" << std::endl;
    return;
  }

  for (int k = 0; k < size_; ++k) {
    shapeContainer_[k]->rotate(alpha);
  }
}

void ponomarev::CompositeShape::add(const shapePtr &shape)
{
  if (shape == nullptr) {
    throw std::invalid_argument("Error: nullptr");
  }

  size_++;
  shapeArray cacheArray(new shapePtr[size_]);

  for (int i = 0; i < size_ - 1; ++i) {
    cacheArray[i] = shapeContainer_[i];
  }

  cacheArray[size_ - 1] = shape;
  shapeContainer_.reset();
  shapeContainer_ = nullptr;
  shapeContainer_.swap(cacheArray);
}

void ponomarev::CompositeShape::remove(const int index)
{
  if ((index >= size_) || (size_ == 0)) {
    throw std::invalid_argument("Index error");
  }

  --size_;

  shapeArray cacheArray(new shapePtr[size_]);
  int j = 0;
  for (int i = 0; i <= size_; ++i) {
    if (i != index) {
      cacheArray[j] = shapeContainer_[i];
      ++j;
    }
  }

  shapeContainer_.swap(cacheArray);
}

void ponomarev::CompositeShape::getInfo() const
{
  if (size_ == 0) {
    std::cout << "--------Containter--------" << std::endl;
    std::cout << "Size[" << (size_) << "]" << std::endl;
    std::cout << "--------------------------" << std::endl;
    return;
  }

  std::cout << "--------Containter--------" << std::endl;
  std::cout << "Size[" << (size_) << "]" << std::endl;
  std::cout << "--------------------------" << std::endl;
  for (int i = 0; i < size_; ++i) {
    std::cout << (i + 1) << std::endl;
    shapeContainer_[i]->getInfo();
    std::cout << std::endl;
  }

  std::cout << "--------Corners--------" << std::endl;
  for (int k = 0; k < 4; ++k) {
    std::cout << "X" << k << ": " << getFrameRect().corns[k].x << std::endl;
    std::cout << "Y" << k << ": " << getFrameRect().corns[k].y << std::endl;
    std::cout << std::endl;
  }
  std::cout << "--------------------------" << std::endl;
}

int ponomarev::CompositeShape::getSize() const
{
  return size_;
}
