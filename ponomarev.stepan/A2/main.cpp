#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"

int main()
{
  std::cout << "----Created new circle:----\n";
  ponomarev::Circle circle({-1.0, 1.0}, 2.0);
  ponomarev::Shape* shape(&circle);
  shape->getInfo();
  std::cout << "-Scale x 3\n";
  shape->scale(3);
  shape->getInfo();

  std::cout << std::endl;

  std::cout << "----Created new rectangle:----\n";
  ponomarev::Rectangle rectangle({121, 31}, 200, 1);
  shape = &rectangle;
  shape->getInfo();
  std::cout << "-Scale x 5\n";
  shape->scale(5);
  shape->getInfo();

  return 0;
}
