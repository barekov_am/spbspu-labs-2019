#include <iostream>
#include <memory>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  ponomarev::shapePtr rectangle1(new ponomarev::Rectangle({0, 0}, 1, 1));
  ponomarev::CompositeShape shapeContainer(rectangle1);
  shapeContainer.getInfo();

  ponomarev::shapePtr circle1(new ponomarev::Circle({0, 0}, 1));
  shapeContainer.add(circle1);
  shapeContainer.getInfo();

  shapeContainer.remove(0);
  shapeContainer.getInfo();

  shapeContainer.remove(0);
  shapeContainer.getInfo();

  return 0;
}
