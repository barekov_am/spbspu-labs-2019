#ifndef DATA_STRUCT_HPP
#define DATA_STRUCT_HPP

#include <iosfwd>
#include <string>

struct DataStruct {
  DataStruct();
  DataStruct(int, int, std::string);

  int key1;
  int key2;
  std::string str;
};

std::istream & operator>>(std::istream &, DataStruct &);
std::ostream & operator<<(std::ostream &, const DataStruct &);

#endif
