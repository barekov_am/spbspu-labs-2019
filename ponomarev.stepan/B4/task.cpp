#include <algorithm>
#include <functional>
#include <iostream>
#include <iterator>
#include <vector>

#include "data_struct.hpp"

void task(std::istream & is, std::ostream & os)
{
  static const auto compare = [](const DataStruct & lds, const DataStruct & rds) {
    if (lds.key1 != rds.key1) {
      return lds.key1 < rds.key1;
    } else if (lds.key2 != rds.key2) {
      return lds.key2 < rds.key2;
    } else {
      return lds.str.size() < rds.str.size();
    }
  };

  std::vector<DataStruct> data((std::istream_iterator<DataStruct>(is)), std::istream_iterator<DataStruct>());

  if (is.fail() && !is.eof()) {
    throw std::ios_base::failure("Invalid input.");
  }

  std::sort(data.begin(), data.end(), compare);

  std::copy(data.begin(), data.end(), std::ostream_iterator<DataStruct>(os, "\n"));
}
