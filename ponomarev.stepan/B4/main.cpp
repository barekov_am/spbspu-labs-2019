#include <iostream>

void task(std::istream &, std::ostream &);

int main()
{
  try {
    task(std::cin, std::cout);
  } catch (const std::exception & e) {
    std::cerr << e.what() << "\n";
    return 2;
  }

  return 0;
}
