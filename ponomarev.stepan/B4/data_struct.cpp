#include "data_struct.hpp"
#include "manipulator.hpp"

/* The default constructor is required by std::istream_iterator<DataStruct> in task.cpp */
DataStruct::DataStruct() :
  key1{0},
  key2{0}
{}

DataStruct::DataStruct(int key1, int key2, std::string str) :
  key1{key1},
  key2{key2},
  str{str}
{
  const std::pair<int, int> KEY_RANGE(-5, 5);

  if ((this->key1 < KEY_RANGE.first || this->key1 > KEY_RANGE.second) ||
      (this->key2 < KEY_RANGE.first || this->key2 > KEY_RANGE.second)) {
    throw std::runtime_error("Invalid key.");
  }
}

static std::istream & delim(std::istream & is)
{
  if (is.peek() != ',') {
    is.setstate(std::ios_base::failbit);
  } else {
    is.get();
  }

  if (is.peek() == '\n') {
    is.setstate(std::ios_base::failbit);
  }

  return is;
}

std::istream & operator>>(std::istream & is, DataStruct & data)
{
  int key1 = 0;
  int key2 = 0;

  is >> blank >> key1 >> delim >> key2 >> delim;

  if (!is) {
    is.setstate(std::ios_base::failbit);
    return is;
  }

  std::string str;
  std::getline(is >> blank, str);

  data = DataStruct(key1, key2, str);

  return is;
}

std::ostream & operator<<(std::ostream & os, const DataStruct & data)
{
  os << data.key1 << "," << data.key2 << "," << data.str;

  return os;
}
