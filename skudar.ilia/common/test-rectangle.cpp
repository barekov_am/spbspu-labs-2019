#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(rectangleTest)

const double Epsilon = 0.01;

BOOST_AUTO_TEST_CASE(widthPermanenceAfterMovingToPoint)
{
  skudar::Rectangle rectangle(4, 2, 3, 5);
  rectangle.move({0, 0});
  const double width = 3;
  BOOST_CHECK_CLOSE(width, rectangle.getFrameRect().width, Epsilon);
}

BOOST_AUTO_TEST_CASE(widthPermanenceAfterMovingToCoordinates)
{
  skudar::Rectangle rectangle(4, 2, 3, 5);
  rectangle.move(1, 3);
  const double width = 3;
  BOOST_CHECK_CLOSE(width, rectangle.getFrameRect().width, Epsilon);
}

BOOST_AUTO_TEST_CASE(heightPermanenceAfterMovingToPoint)
{
  skudar::Rectangle rectangle(4, 2, 3, 5);
  rectangle.move({0, 0});
  const double height = 5;
  BOOST_CHECK_CLOSE(height, rectangle.getFrameRect().height, Epsilon);
}

BOOST_AUTO_TEST_CASE(heightPermanenceAfterMovingToCoordinates)
{
  skudar::Rectangle rectangle(4, 2, 3, 5);
  rectangle.move(1, 3);
  const double height = 5;
  BOOST_CHECK_CLOSE(height, rectangle.getFrameRect().height, Epsilon);
}

BOOST_AUTO_TEST_CASE(areaPermanenceAfterMovingToPoint)
{
  skudar::Rectangle rectangle(4, 2, 3, 5);
  const double rectangleArea = rectangle.getArea();
  rectangle.move({0, 0});
  BOOST_CHECK_CLOSE(rectangle.getArea(), rectangleArea, Epsilon);
}

BOOST_AUTO_TEST_CASE(areaPermanenceAfterMovingToCoordinates)
{
  skudar::Rectangle rectangle(4, 2, 3, 5);
  const double rectangleArea = rectangle.getArea();
  rectangle.move(1, 3);
  BOOST_CHECK_CLOSE(rectangle.getArea(), rectangleArea, Epsilon);
}

BOOST_AUTO_TEST_CASE(areaAfterRotating)
{
  skudar::Rectangle testRectangle({4, 4}, 5, 8);
  double areaBefore = testRectangle.getArea();

  double angle = 33.2;
  testRectangle.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testRectangle.getArea(), Epsilon);
  areaBefore = testRectangle.getArea();

  angle = -66.4;
  testRectangle.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testRectangle.getArea(), Epsilon);
}

BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterScaling)
{
  skudar::Rectangle rectangle(4, 2, 3, 5);
  const double rectangleArea = rectangle.getArea();
  const double scaleFactor = 2.5;
  rectangle.scale(scaleFactor);
  BOOST_CHECK_CLOSE(scaleFactor * scaleFactor, rectangle.getArea() / rectangleArea, Epsilon);
}

BOOST_AUTO_TEST_CASE(incorrectWidth)
{
  BOOST_CHECK_THROW(skudar::Rectangle rectangle(4, 5, -10, 4), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(incorrectHeight)
{
  BOOST_CHECK_THROW(skudar::Rectangle rectangle(4, 5, 4, -5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(incorrectCoefficientOfScaling)
{
  skudar::Rectangle rectangle({0, 0}, 3, 6);
  BOOST_CHECK_THROW(rectangle.scale(-2.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
