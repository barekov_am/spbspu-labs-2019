#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include <memory>

namespace skudar
{
  class CompositeShape : public Shape
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    using shape_array = std::unique_ptr<shape_ptr[]>;
    CompositeShape();
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&);

    ~CompositeShape() = default;

    CompositeShape & operator =(const CompositeShape &);
    CompositeShape & operator =(CompositeShape &&);
    shape_ptr operator [](size_t) const;
    bool operator ==(const CompositeShape &) const;
    bool operator !=(const CompositeShape &) const;


    double getArea() const override;
    rectangle_t getFrameRect() const override;
    point_t getPos() const override;
    void getInfo() const override;
    void move(point_t) override;
    void move(double, double) override;
    void scale(double) override;
    void rotate(double) override;

    void add(shape_ptr);
    void remove(size_t);
    size_t size() const;
    shape_array list() const;

  private:
    size_t count_;
    double angle_;
    shape_array arrayOfShapes_;
  };
}

#endif
