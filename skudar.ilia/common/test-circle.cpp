#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(circleTest)

const double Epsilon = 0.01;

BOOST_AUTO_TEST_CASE(radiusPermanenceAfterMovingToPoint)
{
  skudar::Circle circle(2, 2, 9);
  circle.move({0, 0});
  const double width = 9;
  BOOST_CHECK_CLOSE(width, circle.getFrameRect().width / 2, Epsilon);
}

BOOST_AUTO_TEST_CASE(radiusPermanenceAfterMovingToCoordinates)
{
  skudar::Circle circle(2, 2, 9);
  circle.move(3, 6);
  const double width = 9;
  BOOST_CHECK_CLOSE(width, circle.getFrameRect().width / 2, Epsilon);
}

BOOST_AUTO_TEST_CASE(areaPermanenceAfterMovingToPoint)
{
  skudar::Circle circle(2, 2, 9);
  const double circleArea = circle.getArea();
  circle.move({0, 0});
  BOOST_CHECK_CLOSE(circle.getArea(), circleArea, Epsilon);
}

BOOST_AUTO_TEST_CASE(areaPermanenceAfterMovingToCoordinates)
{
  skudar::Circle circle(2, 2, 9);
  const double circleArea = circle.getArea();
  circle.move(3, 8);
  BOOST_CHECK_CLOSE(circle.getArea(), circleArea, Epsilon);
}

BOOST_AUTO_TEST_CASE(areaPermanenceAfterRotation)
{
  double angle = 44.4;
  skudar::Circle circle(2, 2, 9);
  const double circleArea = circle.getArea();
  circle.rotate(angle);
  BOOST_CHECK_CLOSE(circle.getArea(), circleArea, Epsilon);
}

BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterScaling)
{
  skudar::Circle circle(2, 2, 9);
  const double circleArea = circle.getArea();
  const double scaleFactor = 3.3;
  circle.scale(scaleFactor);
  BOOST_CHECK_CLOSE(scaleFactor * scaleFactor, circle.getArea() / circleArea, Epsilon);
}

BOOST_AUTO_TEST_CASE(incorrectRadius)
{
  BOOST_CHECK_THROW(skudar::Circle circle(2, 2, -6), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(incorrectCoefficientOfScaling)
{
  skudar::Circle circle(2, 2, 6);
  BOOST_CHECK_THROW(circle.scale(-3.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
