#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <utility>
#include <cmath>

const double FullCircle = 360.0;

skudar::CompositeShape::CompositeShape():
count_(0),
angle_(0.0)
{ }

skudar::CompositeShape::CompositeShape(const CompositeShape & rhs) :
  count_(rhs.count_),
  angle_(rhs.angle_),
  arrayOfShapes_(std::make_unique<shape_ptr[]>(rhs.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i] = rhs.arrayOfShapes_[i];
  }
}

skudar::CompositeShape::CompositeShape(CompositeShape && rhs) :
  count_(rhs.count_),
  angle_(rhs.angle_),
  arrayOfShapes_(std::move(rhs.arrayOfShapes_))
{ }

skudar::CompositeShape & skudar::CompositeShape::operator =(const CompositeShape & rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    angle_ = rhs.angle_;
    shape_array tmpArray(std::make_unique<shape_ptr[]>(rhs.count_));
    for (size_t i = 0; i < count_; i++)
    {
      tmpArray[i] = rhs.arrayOfShapes_[i];
    }
    arrayOfShapes_.swap(tmpArray);
  }
  return *this;
}

skudar::CompositeShape & skudar::CompositeShape::operator =(CompositeShape && rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    angle_ = rhs.angle_;
    arrayOfShapes_ = std::move(rhs.arrayOfShapes_);
  }
  return *this;
}

skudar::CompositeShape::shape_ptr skudar::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("There is no figure!");
  }
  return arrayOfShapes_[index];
}

bool skudar::CompositeShape::operator ==(const CompositeShape & rhs) const
{
  if (count_ != rhs.count_)
  {
    return false;
  }

  for (size_t i = 0; i < count_; i++)
  {
    if (arrayOfShapes_[i] != rhs.arrayOfShapes_[i])
    {
      return false;
    }
  }
  return true;
}

bool skudar::CompositeShape::operator !=(const CompositeShape & rhs) const
{
  return !(*this == rhs);
}

double skudar::CompositeShape::getArea() const
{
  double sumArea = 0.0;
  for (size_t i = 0; i < count_; i++)
  {
    sumArea += arrayOfShapes_[i]->getArea();
  }
  return sumArea;
}

skudar::rectangle_t skudar::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    return {{0, 0}, 0, 0};
  }
  rectangle_t tmpFrameRect = arrayOfShapes_[0]->getFrameRect();

  double leftX = tmpFrameRect.pos.x - tmpFrameRect.width / 2;
  double rightX = tmpFrameRect.pos.x + tmpFrameRect.width / 2;
  double bottomY = tmpFrameRect.pos.y - tmpFrameRect.height / 2;
  double topY = tmpFrameRect.pos.y + tmpFrameRect.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    tmpFrameRect = arrayOfShapes_[i]->getFrameRect();
    double tmpValue = tmpFrameRect.pos.x - tmpFrameRect.width / 2;
    leftX = std::min(tmpValue, leftX);
    tmpValue = tmpFrameRect.pos.x + tmpFrameRect.width / 2;
    rightX = std::max(tmpValue, rightX);
    tmpValue = tmpFrameRect.pos.y - tmpFrameRect.height / 2;
    bottomY = std::min(tmpValue, bottomY);
    tmpValue = tmpFrameRect.pos.y + tmpFrameRect.height / 2;
    topY = std::max(tmpValue, topY);
  }
  return {point_t{(leftX + rightX) / 2, (bottomY + topY) / 2}, rightX - leftX, topY - bottomY};
}

skudar::point_t skudar::CompositeShape::getPos() const
{
  return getFrameRect().pos;
}

void skudar::CompositeShape::getInfo() const
{
  std::cout << "Composite shape area = " << getArea() << '\n';
  std::cout << "X center = " << getFrameRect().pos.x << '\n';
  std::cout << "Y center = " << getFrameRect().pos.y << '\n';
  std::cout << "Composite shape has " << count_ << " figures" << '\n';
  std::cout << '\n';

  for(size_t i = 0; i < count_; i++)
  {
    std::cout << "Info about " << i << " figure: "<< std::endl;
    std::cout << "Area = " << arrayOfShapes_[i]->getArea() << std::endl;
    std::cout << "X center = " << arrayOfShapes_[i]->getFrameRect().pos.x << '\n';
    std::cout << "Y center = " << arrayOfShapes_[i]->getFrameRect().pos.y << '\n';
    std::cout << '\n';
  }
}

void skudar::CompositeShape::move(point_t point)
{
  const double dx = point.x - getFrameRect().pos.x;
  const double dy = point.y - getFrameRect().pos.y;
  move(dx, dy);
}

void skudar::CompositeShape::move(double dy, double dx)
{
  for (size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i]->move(dx, dy);
  }
}

void skudar::CompositeShape::scale(double factor)
{
  if (factor <= 0.0)
  {
    throw std::invalid_argument("Scale factor must be a non-negative number");
  }
  const point_t centreOfCompositeShape = getFrameRect().pos;
  for (size_t i = 0; i < count_; i++)
  {
    const double dx = arrayOfShapes_[i]->getFrameRect().pos.x - centreOfCompositeShape.x;
    const double dy = arrayOfShapes_[i]->getFrameRect().pos.y - centreOfCompositeShape.y;
    arrayOfShapes_[i]->move({centreOfCompositeShape.x + dx * factor, centreOfCompositeShape.y + dy * factor});
    arrayOfShapes_[i]->scale(factor);
  }
}

void skudar::CompositeShape::rotate(double angle)
{
  angle_ += angle;
  //to prevent unnecessary conditions, fmod is used not only when angle_ >= 360.
  angle_ = (angle_ < 0.0) ? (FullCircle + fmod(angle_, FullCircle)) : fmod(angle_, FullCircle);

  const point_t centre = getFrameRect().pos;
  const double sine = sin(angle * M_PI / 180);
  const double cosine = cos(angle * M_PI / 180);

  for (size_t i = 0; i < count_; i++)
  {
    const double oldX = arrayOfShapes_[i]->getFrameRect().pos.x - centre.x;
    const double oldY = arrayOfShapes_[i]->getFrameRect().pos.y - centre.y;

    const double newX = oldX * fabs(cosine) - oldY * fabs(sine);
    const double newY = oldX * fabs(sine) + oldY * fabs(cosine);

    arrayOfShapes_[i]->move({centre.x + newX, centre.y + newY});
    arrayOfShapes_[i]->rotate(angle);
  }
}

void skudar::CompositeShape::add(shape_ptr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer is null");
  }
  shape_array tmpArray(std::make_unique<shape_ptr[]>(count_ + 1));
  for (size_t i = 0; i < count_; i++)
  {
    tmpArray[i] = arrayOfShapes_[i];
  }
  tmpArray[count_] = shape;
  count_++;
  arrayOfShapes_.swap(tmpArray);
}

void skudar::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("There is no shape!");
  }
  for (size_t i = index; i < count_ - 1; i++)
  {
    arrayOfShapes_[i] = arrayOfShapes_[i + 1];
  }
  count_--;
}

size_t skudar::CompositeShape::size() const
{
  return count_;
}

skudar::CompositeShape::shape_array skudar::CompositeShape::list() const
{
  shape_array tmpArray(std::make_unique<shape_ptr[]>(count_));
  for (size_t i = 0; i < count_; i++)
  {
    tmpArray[i] = arrayOfShapes_[i];
  }

  return tmpArray;
}
