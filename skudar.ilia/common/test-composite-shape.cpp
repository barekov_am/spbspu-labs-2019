#include <stdexcept>
#include <utility>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"

using shape_ptr = std::shared_ptr<skudar::Shape>;

const double Epsilon = 0.01;

BOOST_AUTO_TEST_SUITE(compositeTestSuite)

BOOST_AUTO_TEST_CASE(invariabilityAfterMoving)
{
  shape_ptr testCircle = std::make_shared<skudar::Circle>(4, 4, 8.7);
  shape_ptr testRectangle = std::make_shared<skudar::Rectangle>(5, 6, 7.8, 8.9);
  skudar::CompositeShape testComposite;
  testComposite.add(testCircle);
  testComposite.add(testRectangle);
  const skudar::rectangle_t frameRectBefore = testComposite.getFrameRect();
  const double areaBefore = testComposite.getArea();

  testComposite.move({1.9, 1.9});
  BOOST_CHECK_CLOSE(frameRectBefore.width, testComposite.getFrameRect().width, Epsilon);
  BOOST_CHECK_CLOSE(frameRectBefore.height, testComposite.getFrameRect().height, Epsilon);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), Epsilon);

  testComposite.move(5, -7);
  BOOST_CHECK_CLOSE(frameRectBefore.width, testComposite.getFrameRect().width, Epsilon);
  BOOST_CHECK_CLOSE(frameRectBefore.height, testComposite.getFrameRect().height, Epsilon);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), Epsilon);
}

BOOST_AUTO_TEST_CASE(areaAfterScaling)
{
  shape_ptr testCircle = std::make_shared<skudar::Circle>(4, 4, 3);
  shape_ptr testRectangle = std::make_shared<skudar::Rectangle>(5, 6, 2.1, 1);
  skudar::CompositeShape testComposite;
  testComposite.add(testCircle);
  testComposite.add(testRectangle);
  double areaBefore = testComposite.getArea();

  double scaleFactor = 2.75;
  testComposite.scale(scaleFactor);
  BOOST_CHECK_CLOSE(testComposite.getArea(), scaleFactor * scaleFactor * areaBefore, Epsilon);
  areaBefore = testComposite.getArea();

  scaleFactor = 0.75;
  testComposite.scale(scaleFactor);
  BOOST_CHECK_CLOSE(testComposite.getArea(), scaleFactor * scaleFactor * areaBefore, Epsilon);
}

BOOST_AUTO_TEST_CASE(areaAfterRotating)
{
  shape_ptr testCircle = std::make_shared<skudar::Circle>(4, 7, 1);
  shape_ptr testRectangle = std::make_shared<skudar::Rectangle>(5, 6, 4.5, 1.25);
  skudar::CompositeShape testComposite;
  testComposite.add(testCircle);
  testComposite.add(testRectangle);
  double areaBefore = testComposite.getArea();

  double angle = 48.2;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), Epsilon);
  areaBefore = testComposite.getArea();

  angle = -30;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), Epsilon);
  areaBefore = testComposite.getArea();

  angle = 75.3;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), Epsilon);
  areaBefore = testComposite.getArea();

  angle = -87.6;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), Epsilon);
}

BOOST_AUTO_TEST_CASE(copyAndMove)
{
  shape_ptr testCircle = std::make_shared<skudar::Circle>(1, 2, 3);
  skudar::CompositeShape testComposite;
  testComposite.add(testCircle);

  BOOST_CHECK_NO_THROW(skudar::CompositeShape testComposite2(testComposite));
  BOOST_CHECK_NO_THROW(skudar::CompositeShape testComposite2(std::move(testComposite)));

  skudar::CompositeShape testComposite2;
  testComposite2.add(testCircle);

  skudar::CompositeShape testComposite3;

  BOOST_CHECK_NO_THROW(testComposite3 = testComposite2);
  BOOST_CHECK_NO_THROW(testComposite3 = std::move(testComposite2));

  skudar::CompositeShape testComposite4;
  testComposite4.add(testCircle);

  skudar::CompositeShape testComposite5;

  testComposite5 = testComposite4;
  BOOST_CHECK(testComposite5 == testComposite4);
  testComposite5 = std::move(testComposite4);
  //cannot compare testComposite5 to testComposite4 as we moved from the latter.
  BOOST_CHECK(testComposite5 == testComposite3);

  skudar::CompositeShape testComposite6(testComposite3);
  BOOST_CHECK(testComposite6 == testComposite3);
  skudar::CompositeShape testComposite7(std::move(testComposite3));
  //cannot compare testComposite7 to testComposite3 as we moved from the latter.
  BOOST_CHECK(testComposite7 == testComposite6);
}

BOOST_AUTO_TEST_CASE(exceptionThrow)
{
  shape_ptr testCircle = std::make_shared<skudar::Circle>(1, 2, 3);
  shape_ptr testRectangle = std::make_shared<skudar::Rectangle>(4, 5, 6, 7);
  skudar::CompositeShape testComposite;
  testComposite.add(testCircle);
  testComposite.add(testRectangle);
  BOOST_CHECK_THROW(testComposite.scale(-2), std::invalid_argument);

  BOOST_CHECK_THROW(testComposite.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(testComposite.remove(4), std::out_of_range);
  BOOST_CHECK_THROW(testComposite.remove(-2), std::out_of_range);

  BOOST_CHECK_THROW(testComposite[100], std::out_of_range);
  BOOST_CHECK_THROW(testComposite[-1], std::out_of_range);

  testComposite.remove(1);
  testComposite.remove(0);
  BOOST_CHECK_THROW(testComposite.remove(0), std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
