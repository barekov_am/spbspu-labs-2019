#ifndef MATRIX_HPP
#define MATRIX_HPP

#include<memory>
#include"shape.hpp"

namespace skudar
{
  class Matrix
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    using shape_array = std::unique_ptr<shape_ptr[]>;

    Matrix();
    Matrix(const Matrix &);
    Matrix(Matrix &&);

    ~Matrix() = default;

    Matrix & operator =(const Matrix &);
    Matrix & operator =(Matrix &&);

    shape_array operator [](size_t) const;
    bool operator ==(const Matrix &) const;
    bool operator !=(const Matrix &) const;

    void add(shape_ptr , size_t, size_t);
    size_t getRows() const;
    size_t getColumns() const;
    size_t getSizeLayer(size_t) const;

  private:
    size_t rows_;
    size_t columns_;
    shape_array list_;
  };
}

#endif
