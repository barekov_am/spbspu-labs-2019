#include "rectangle.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>

const double FullCircle = 360.0;

skudar::Rectangle::Rectangle(const point_t &position, double width, double height) :
  center_(position),
  width_(width),
  height_(height),
  angle_(0.0)
{
  if ((width_ <= 0.0) || (height_ <= 0.0))
  {
    throw std::invalid_argument("Width and height must be positive numbers");
  }
}

skudar::Rectangle::Rectangle(double x, double y, double width, double height) :
  Rectangle({x, y}, width, height)
{
  if ((width_ <= 0.0) || (height_ <= 0.0))
  {
    throw std::invalid_argument("Width and height must be positive numbers");
  }
}

void skudar::Rectangle::move(point_t point)
{
  center_ = point;
}

void skudar::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

double skudar::Rectangle::getArea() const
{
  return width_ * height_;
}

skudar::rectangle_t skudar::Rectangle::getFrameRect() const
{
  const double sine = sin(angle_ * M_PI / 180);
  const double cosine = cos(angle_ * M_PI / 180);
  const double width = height_ * fabs(sine) + width_ * fabs(cosine);
  const double height = height_ * fabs(cosine) + width_ * fabs(sine);
  return {center_, width, height};
}

skudar::point_t skudar::Rectangle::getPos() const
{
  return center_;
}

void skudar::Rectangle::getInfo() const
{
  std::cout << "\nThe size of the rectangle is " << width_ << "x" << height_ << "\n";
}

void skudar::Rectangle::scale(double factor)
{
  if (factor <= 0.0)
  {
    throw std::invalid_argument("Rectangle scale factor must be a non-negative number");
  }
  width_ *= factor;
  height_ *= factor;
}

void skudar::Rectangle::rotate(double angle)
{
  angle_ += angle;
  //to prevent unnecessary conditions, fmod is used not only when angle_ >= 360.
  angle_ = (angle_ < 0.0) ? (FullCircle + fmod(angle_, FullCircle)) : fmod(angle_, FullCircle);
}
