#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  skudar::CompositeShape::shape_ptr rect1 = std::make_shared<skudar::Rectangle>(4, 4, 3, 2);
  skudar::CompositeShape::shape_ptr rect2 = std::make_shared<skudar::Rectangle>(0, 0, 4, 6);
  skudar::CompositeShape::shape_ptr circ1 = std::make_shared<skudar::Circle>(2, 3, 4);
  skudar::CompositeShape::shape_ptr circ2 = std::make_shared<skudar::Circle>(6, 8, 10);

  skudar::CompositeShape composite;
  composite.add(rect1);
  composite.add(rect2);
  composite.add(circ1);
  composite.add(circ2);

  composite.getInfo();

  composite.move(2, 5);
  composite.getInfo();

  composite.remove(1);
  composite.getInfo();

  composite.remove(2);
  composite.getInfo();

  composite.scale(4);
  composite.getInfo();

  return 0;
}
