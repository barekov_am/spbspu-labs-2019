#include <iostream>
#include <vector>
#include <forward_list>
#include <stdexcept>
#include <string>
#include "detail.hpp"

sortType_t getSortType(const std::string &param)
{
  if (param == "ascending")
  {
    return sortType_t::ascending;
  }
  else if (param == "descending")
  {
    return sortType_t::descending;
  }
  else
  {
    throw std::invalid_argument("Incorrect sort type");
  }
}

void task1(const char *argv)
{
  sortType_t type = getSortType(argv);

  std::forward_list<int> list1;
  int nextNum;
  while (std::cin >> nextNum)
  {
    list1.push_front(nextNum);
  }
  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::invalid_argument("Incorrect input - expected integer type");
  }
  std::vector<int> vec1(list1.begin(), list1.end());
  std::vector<int> vec2 = vec1;

  sort<Bracket>(vec1, type);
  printContainer(vec1);

  sort<At>(vec2, type);
  printContainer(vec2);

  sort<Iterator>(list1, type);
  printContainer(list1);
}
