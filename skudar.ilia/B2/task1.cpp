#include <sstream>
#include <iostream>
#include "queue_implementation.hpp"

template<typename T>
void add(std::istream &in, QueueWithPriority<T> &queue)
{
  std::string priority_str;
  in >> priority_str >> std::ws;
  T element;
  getline(in, element);
  if (element.empty())
  {
    std::cout << "<INVALID COMMAND>" << std::endl;
    return;
  }
  if (!in.eof() && in.fail())
  {
    throw std::ios::failure("Error reading input stream");
  }
  QueueWithPriority<std::string>::ElementPriority priorityElement;
  if (priority_str == "low")
  {
    priorityElement = QueueWithPriority<std::string>::ElementPriority::LOW;
    queue.put(element, priorityElement);
  }
  else if (priority_str == "normal")
  {
    priorityElement = QueueWithPriority<std::string>::ElementPriority::NORMAL;
    queue.put(element, priorityElement);
  }
  else if (priority_str == "high")
  {
    priorityElement = QueueWithPriority<std::string>::ElementPriority::HIGH;
    queue.put(element, priorityElement);
  }
  else
  {
    std::cout << "<INVALID COMMAND>" << std::endl;
  }
}

void task1()
{
  QueueWithPriority<std::string> queue_str;
  std::string input;

  while (getline(std::cin, input))
  {
    std::istringstream buffer(input);
    std::string keyWord;
    buffer >> keyWord;
    if (!std::cin.eof() && std::cin.fail())
    {
      throw std::ios::failure("Error reading input stream");
    }
    if (keyWord == "add")
    {
      add(buffer, queue_str);
    }
    else if (keyWord == "get")
    {
      if (queue_str.empty())
      {
        std::cout << "<EMPTY>" << std::endl;
      }
      else
      {
        std::cout << queue_str.get() << std::endl;
      }
    }
    else if (keyWord == "accelerate")
    {
      queue_str.accelerate();
    }
    else
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }
  }
}
