#ifndef QUEUE_INTERFACE
#define QUEUE_INTERFACE

#include <list>
#include <algorithm>
#include <stdexcept>

template<typename T>
class QueueWithPriority
{
public:
  enum ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  void put(const T &element, ElementPriority priority);
  void accelerate();
  T get();
  bool empty();

private:
  std::list<T> list_[3];
};

#endif
