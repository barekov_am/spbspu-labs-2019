#include <list>
#include <iostream>

const int MIN = 1;
const int MAX = 20;
const size_t MAX_COUNT = 20;

void task2()
{
  std::list<int> list;
  int num;
  size_t countNum = 0;
  while(std::cin >> num)
  {
    countNum++;
    if((num > MAX) || (num < MIN) || (countNum > MAX_COUNT))
    {
      throw std::invalid_argument("Incorrect data or count of elements\n");
    }
    list.push_back(num);
  }

  if(std::cin.fail() && !std::cin.eof())
  {
    throw std::ios_base::failure("Trouble with reading std::cin\n");
  }

  if (list.empty())
  {
    return;
  }

  auto first = list.begin();
  auto last_ = list.end();
  size_t iter = 0;

  while (iter < list.size() / 2)
  {
    std::cout << *first << " " << *std::prev(last_) << " ";
    ++first;
    --last_;
    iter++;
  }
  if (list.size() % 2 != 0)
  {
    std::cout << *first;
  }
  std::cout << "\n";
}
