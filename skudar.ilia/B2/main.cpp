#include <iostream>
#include <cstring>

void task1();
void task2();

int main(int argc, char* argv[])
{
  if (argc == 1)
  {
    std::cerr << "Trouble with count of parameters\n";
    return 1;
  }
  try
  {
    char * tail = nullptr;
    size_t numTask = std::strtol(argv[1], &tail, 10);
    if (*tail)
    {
      std::cerr << "Invalid type of task-argument\n";
      return 1;
    }
    
    switch (numTask)
    {
      case 1:
      {
        task1();
      }
        break;
      case 2:
      {
        task2();
      }
        break;
      default:
      {
        std::cerr << "Trouble with number of part\n";
        return 1;
      }
    }
  }
  catch (std::invalid_argument & err)
  {
    std::cerr << err.what() << '\n';
    return 1;
  }
  catch (std::exception &err)
  {
    std::cerr << err.what() << "\n";
    return 2;
  }
  return 0;
}
