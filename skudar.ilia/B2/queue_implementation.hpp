#ifndef QUEUE_IMPLEMENTATION
#define QUEUE_IMPLEMENTATION

#include "queue_interface.hpp"

template<typename T>
void QueueWithPriority<T>::put(const T &element, ElementPriority priority)
{
  list_[priority].push_back(element);
}

template<typename T>
void QueueWithPriority<T>::accelerate()
{
  list_[ElementPriority::HIGH].splice(list_[ElementPriority::HIGH].end(), list_[ElementPriority::LOW]);
}

template<typename T>
T QueueWithPriority<T>::get()
{
  for(size_t i = ElementPriority::HIGH; i >= ElementPriority::LOW; --i)
  {
    if (!list_[i].empty())
    {
      T name = list_[i].front();
      list_[i].pop_front();
      return name;
    }
  }
  throw std::invalid_argument("Queue is empty\n");
}

template<typename T>
bool QueueWithPriority<T>::empty()
{
  return ((list_[LOW].empty()) && (list_[NORMAL].empty()) && (list_[HIGH].empty()));
}

#endif
