#include <iostream>
#include <algorithm>
#include "DataStruct.hpp"

int main()
{
  std::vector<DataStruct> data;
  try
  {
    readVector(data);
  }
  catch (std::invalid_argument &err)
  {
    std::cerr << err.what();
    return 1;
  }
  sortVector(data);
  std::for_each(data.begin(), data.end(), printVector);
  return 0;
}
