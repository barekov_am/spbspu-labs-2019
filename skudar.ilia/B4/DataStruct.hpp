#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP

#include <string>
#include <vector>
#include <stdexcept>
#include <iostream>
#include <algorithm>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

void readVector(std::vector<DataStruct> &vector);
void printVector(const DataStruct &data);
void sortVector(std::vector<DataStruct> &vector);
bool less(const DataStruct &dataLft, const DataStruct &dataRhs);
bool outOfRange(const DataStruct &data);

#endif
