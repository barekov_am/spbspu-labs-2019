#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>

class Shape
{
public:
  Shape();
  Shape(int x, int y);
  virtual ~Shape() = default;

  bool isMoreLeft(const std::shared_ptr<Shape> & rhs) const;
  bool isUpper(const std::shared_ptr<Shape> & rhs) const;
  virtual void draw() const = 0;

  int x_;
  int y_;
};

#endif
