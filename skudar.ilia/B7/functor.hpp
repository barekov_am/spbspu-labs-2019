#ifndef B7_FUNCTOR_HPP
#define B7_FUNCTOR_HPP

#include <vector>

class Functor
{
public:
  void operator()(double num);
  void showAll();

private:
  std::vector<double> vec;
};


#endif
