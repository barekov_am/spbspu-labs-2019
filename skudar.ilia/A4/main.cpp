#include<iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

int main()
{
  skudar::Rectangle rect(9, 4, 1.5, 1.5);

  rect.move({4, 8});
  rect.getInfo();
  rect.scale(3);
  rect.getInfo();
  rect.rotate(77);
  rect.getInfo();

  skudar::Circle circ(5, 3, 7);

  circ.move(-8, 5);
  circ.getInfo();
  circ.scale(0.25);
  circ.getInfo();
  rect.rotate(39);
  circ.getInfo();

  skudar::Rectangle rect2(11, 3, 6, 6);

  skudar::CompositeShape::shape_ptr part1 = std::make_shared<skudar::Rectangle>(rect);
  skudar::CompositeShape::shape_ptr part2 = std::make_shared<skudar::Rectangle>(rect2);
  skudar::CompositeShape::shape_ptr part3 = std::make_shared<skudar::Circle>(circ);

  skudar::CompositeShape composite;
  composite.add(part1);
  composite.add(part2);
  composite.add(part3);

  composite.getInfo();
  composite.scale(0.5);
  composite.getInfo();
  composite.rotate(30);
  composite.getInfo();
  composite.move({3, 4});
  composite.getInfo();

  skudar::Matrix matrix = skudar::part(composite);
  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    for (size_t j = 0; j < matrix.getColumns(); j++)
    {
      if (matrix[i][j] != nullptr)
      {
        std::cout << "Layer no. " << i << ":\n" << "Figure no. " << j << ":\n";
        std::cout << "Position: (" << matrix[i][j]->getPos().x << "; "
            << matrix[i][j]->getPos().y << ")\n";
      }
    }
  }

  return 0;
}
