#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace karpenko 
{
  class Rectangle: public Shape
  {
  public:
    Rectangle(rectangle_t rect, double angle);

    double getArea() const noexcept override;
    rectangle_t getFrameRect() const override;
    double getAngle() const noexcept;
    void move(double dx, double dy) noexcept override;
    void move(point_t newPos) noexcept override;
    void scale(double scaleRatio) override;
    void rotate(double angle) noexcept override;

  private:
    rectangle_t rect_;
    double angle_;
  };
}

#endif
