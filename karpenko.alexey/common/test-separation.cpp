#include "boost/test/auto_unit_test.hpp"

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "separation.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteSeparation)

BOOST_AUTO_TEST_CASE(correctSeparation)
{
  karpenko::CompositeShape composite;
  karpenko::Circle circ1{10.0, {0.0, 10.0}};
  karpenko::Circle circ2{20.0, {40.0, 10.0}};
  karpenko::Rectangle rect1{{12.0, 10.0, {2.0, 4.0}}, 0.0};
  karpenko::Rectangle rect2{{12.0, 40.0, {11.00, 22.0}}, 0.0};

  composite.add(std::make_shared<karpenko::Rectangle>(rect2));
  composite.add(std::make_shared<karpenko::Rectangle>(rect1));
  composite.add(std::make_shared<karpenko::Circle>(circ1));
  composite.add(std::make_shared<karpenko::Circle>(circ2));

  const karpenko::Matrix matrix = separation(composite);

  BOOST_CHECK_EQUAL(matrix[0][0]->getFrameRect().height, rect2.getFrameRect().height);
  BOOST_CHECK_EQUAL(matrix[0][1]->getFrameRect().height, circ2.getFrameRect().height);
  BOOST_CHECK_EQUAL(matrix[1][0]->getFrameRect().height, rect1.getFrameRect().height);
  BOOST_CHECK_EQUAL(matrix[2][0]->getFrameRect().height, circ1.getFrameRect().height);
}

BOOST_AUTO_TEST_CASE(correctCheckIntersection)
{
  karpenko::Circle circle{20.0, {0.0, 0.0}};
  karpenko::Rectangle rectangle{{10.0, 10.0, {0.0, 0.0}}, 0.0};

  BOOST_CHECK_EQUAL(karpenko::intersect(circle.getFrameRect(), rectangle.getFrameRect()), true);

  rectangle.move({100.0, 0.0});
  BOOST_CHECK_EQUAL(karpenko::intersect(circle.getFrameRect(), rectangle.getFrameRect()), false);
}

BOOST_AUTO_TEST_CASE(checkThrowExceptions)
{
  karpenko::CompositeShape composite;
  karpenko::Circle circ1{10.0, {0.0, 10.0}};
  karpenko::Rectangle rect1{{12.0, 10.0, {2.0, 4.0}}, 0.0};
  composite.add(std::make_shared<karpenko::Rectangle>(rect1));
  composite.add(std::make_shared<karpenko::Circle>(circ1));

  const karpenko::Matrix matrix = separation(composite);

  BOOST_CHECK_THROW(matrix[10][10], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()


