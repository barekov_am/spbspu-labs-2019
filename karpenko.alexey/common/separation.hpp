#ifndef SEPARATION_HPP
#define SEPARATION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace karpenko
{
  bool intersect(const rectangle_t & lhs, const rectangle_t & rhs);
  Matrix separation(const CompositeShape & composite);
  Matrix separation(const shapesArray & array, int size);
}

#endif
