#ifndef MANIPULATORS_HPP
#define MANIPULATORS_HPP
#include <cctype>
#include <istream>

namespace manipulator
{
  inline std::istream& skipBlank(std::istream& in)
  {
    while (std::isblank(in.peek()))
    {
      in.get();
    }
    return in;
  }
}
#endif
