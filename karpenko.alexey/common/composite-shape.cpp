#include "composite-shape.hpp"
#define _USE_MATH_DEFINES
#include <cmath>
#include <stdexcept>
#include <iostream>
#include <algorithm>

// In order not to mix the subject of comments I will use the comment
// specifier "E*" to mark the generation of security exceptions. Like:
//1.E* "First comment about exception"

//E* This constructor satisfies Strong exception guarantee
karpenko::CompositeShape::CompositeShape() :
  size_(0)
{
}


//E* If an exception will be thrown in the constructor, 
//E* then everything that has been full-constructed would call auto-destructor.
//E* This constructor satisfies Strong exception guarantee
karpenko::CompositeShape::CompositeShape(const CompositeShape & source) :
  size_(source.size_),
  shapesArray_(std::make_unique<shapePtr[]>(source.size_))
{
  for (int i = 0; i < size_; i++)
  {
    shapesArray_[i] = source.shapesArray_[i];
  }
}


//E* tagged with the specifier for the same reason as in matrix.cpp 
//E* This constructor satisfies Strong exception guarantee
karpenko::CompositeShape::CompositeShape(CompositeShape && source) noexcept :
  size_(source.size_),
  shapesArray_(std::move(source.shapesArray_))
{
  source.size_ = 0;
}


//1.E* Creating temporary variables, I do not change the actual data
//2.E* Using smart pointers helps avoid memory leak. Actual data is safe.
//3.E* If the exceptions were not thrown, I will change the actual data.
//4.E* .swap usually satisfy Strong exception guarantee because of noexcept specifier.
karpenko::CompositeShape & karpenko::CompositeShape::operator =(const CompositeShape & source)
{
  if (this != & source)
  {
    const int size = source.size_; //1.E*
    shapesArray tempArr = std::make_unique<shapePtr[]>(size); //2.E*

    for (int i = 0; i < size; i++)
    {
      tempArr[i] = source.shapesArray_[i];
    }
    //3.E*
    size_ = source.size_;
    shapesArray_.swap(tempArr); //4.E*
  }

  return *this;
}


//E* move-semantic usually satisfy Strong exception guarantee because of noexcept specifier.
karpenko::CompositeShape & karpenko::CompositeShape::operator =(CompositeShape && someComposite) noexcept
{
  if (this != & someComposite)
  {
    size_ = someComposite.size_;
    shapesArray_ = std::move(someComposite.shapesArray_);
    someComposite.size_ = 0;
  }

  return *this;
}


//E* if an exception is thrown, the actual data has not been changed yet
karpenko::shapePtr karpenko::CompositeShape::operator [](int index) const
{
  if ((index < 0) || (size_ <= index))
  {
    throw std::out_of_range("Index out of range!");
  }

  return shapesArray_[index];
}


//E* This method does not change the any data.
double karpenko::CompositeShape::getArea() const noexcept
{
  double multiArea = 0;
  for (int i = 0; i < size_; i++)
  {
    multiArea += shapesArray_[i]->getArea();
  }

  return multiArea;
}


//E* This method does not change the any data.
//E* Even if an exception is thrown, it will not affect data integrity
//E* All variables in this method will be correctly released in the process of unwinding the stack
karpenko::rectangle_t karpenko::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    std::cout << "CompositeShape is empty!\n\n";
    return {0, 0, {0, 0}};
  }

  rectangle_t tempFrame = shapesArray_[0]->getFrameRect();

  double minX = tempFrame.pos.x - tempFrame.width / 2;
  double minY = tempFrame.pos.y - tempFrame.height / 2;
  double maxX = minX + tempFrame.width;
  double maxY = minY + tempFrame.height;

  for (int i = 1; i < size_; i++) // Find min and max coords
  {
    tempFrame = shapesArray_[i]->getFrameRect();

    minX = std::min(minX, (tempFrame.pos.x - tempFrame.width / 2));
    minY = std::min(minY, (tempFrame.pos.y - tempFrame.height / 2));
    maxX = std::max(maxX, (tempFrame.pos.x + tempFrame.width / 2));
    maxY = std::max(maxY, (tempFrame.pos.y + tempFrame.height / 2));
  }

  return {maxX - minX, maxY - minY, {(maxX + minX) / 2, (maxY + minY) / 2}};
}


//E* I removed the meaningless exception because it compromised the integrity of the program
//E* when used in other methods. Moreover, the compiler would not even go into the loop below.
void karpenko::CompositeShape::move(double dx, double dy) noexcept
{
  //E*
  for (int i = 0; i < size_; i++)
  {
    shapesArray_[i] -> move(dx, dy);
  }
}


//E* exception-throw removed by the same reason as statement move(dx, dy).
void karpenko::CompositeShape::move(point_t newPos)
{
  const point_t center = getFrameRect().pos;

  const double dx = newPos.x - center.x;
  const double dy = newPos.y - center.y;

  move(dx, dy);
}

//1.E* This check is also not essential, but I will not remove it because the scaling can throw another exception
//E*   It will also help identify problems in the program, if they occur.
//2.E* Even if an exception is thrown, it will not affect data integrity
//3.E* move() is noexcept method
//4.E* the exception will not occur here because of the check with scaleRatio upper
void karpenko::CompositeShape::scale(const double scaleRatio)
{
  //1.E*
  if (size_ == 0)
  {
    throw std::logic_error("Cannot scale an empty composite shape");
  }

  if (scaleRatio <= 0.0)
  {
    //2.E*
    throw std::invalid_argument("scaleRatio should be positive value!");
  }

  const point_t center = getFrameRect().pos;

  for (int i = 0; i < size_; i++)
  {
    rectangle_t delta = shapesArray_[i]->getFrameRect();

    delta.pos.x = center.x + scaleRatio * (delta.pos.x - center.x); // getting new center of figure after scaling
    delta.pos.y = center.y + scaleRatio * (delta.pos.y - center.y);

    shapesArray_[i]->move(delta.pos); //3.E*
    shapesArray_[i]->scale(scaleRatio); //4.E*
  }
}


//1.E* if the exception is thrown above, it will not affect the actual program data
//2.E* move() is noexcept method
//3.E  rotate() is noexcept method
void karpenko::CompositeShape::rotate(double angle)
{
  const double sinus = fabs(sin(angle * M_PI / 180));
  const double cosine = fabs(cos(angle * M_PI / 180));

  for (int i = 0; i < size_; i++)
  {
    const karpenko:: point_t center = getFrameRect().pos;

    const double prevX = shapesArray_[i]->getFrameRect().pos.x - center.x;
    const double prevY = shapesArray_[i]->getFrameRect().pos.y - center.y;

    // Getting new coords of figure after upcoming rotation
    const double newX = prevX * cosine - prevY * sinus;
    const double newY = prevY * cosine + prevX * sinus;

    //1.E*

    shapesArray_[i]->move({newX + center.x, newY + center.y}); //2.E*
    shapesArray_[i]->rotate(angle); //3.E*
  }
}


//E* This method does not change the any data.
int karpenko::CompositeShape::getSize() const noexcept
{
  return size_;
}


//E* This method does not change the any data.
karpenko::shapesArray karpenko::CompositeShape::getShapesArray() const
{
  shapesArray tempArray(std::make_unique<shapePtr[]>(size_));
  for (int i = 0; i < size_; i++)
  {
    tempArray[i] = shapesArray_[i];
  }
  //By returning a tempArray, I avoid problems with calling the destructor.
  return tempArray;
}


//1.E* Even if an exception is thrown, it will not affect data integrity
//2.E* If the exceptions were not thrown, I change the actual data.
//E*   Strong exception guarantee is respected here.
void karpenko::CompositeShape::add(const shapePtr & someShape)
{
  if (someShape == nullptr)
  {
    throw std::invalid_argument("Adding shape cannot be nullptr"); //1.E*
  }

  int size = size_;
  shapesArray tempArr = std::make_unique<shapePtr[]>(++size);

  for (int i = 0; i < size - 1; i++)
  {
    tempArr[i] = shapesArray_[i];
  }
  tempArr[size - 1] = someShape;

  //2.E*
  size_++;
  shapesArray_.swap(tempArr);
  std::cout << "Shape added. New size of array: " << size_ << "\n\n";
}


//1.E* Even if an exception is thrown, it will not affect data integrity
//2.E* Creating a copy of variables, in the case of an exception, I save the actual data
//3.E* If the exceptions were not thrown, I will change the actual data.
//E*   Strong exception guarantee is respected here.
void karpenko::CompositeShape::remove(int delNumber)
{
  if ((delNumber < 0) || (delNumber > size_))
  {
    throw std::invalid_argument("deleting number out of range"); //1.E*
  }

  std::cout << "Shape number " << delNumber + 1 << " will be removed.\n";

  //2.E*
  int size = size_;
  shapesArray tempArr = std::make_unique<shapePtr[]>(size);

  for (int i = 0; i < size; i++)
  {
    tempArr[i] = shapesArray_[i];
  }

  while (delNumber + 1 < size) // Array shift
  {
    tempArr[delNumber] = tempArr[delNumber + 1];
    delNumber++;
  }
  tempArr[--size] = nullptr;

  //3.E*
  size_--;
  shapesArray_.swap(tempArr);
  std::cout << "New size of array: " << size_ << "\n\n";
}

//P.S if some part of the code regarding exceptions is not commented, then most likely I did it specifically to avoid repetitions.

