#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include <memory>
#include "shape.hpp"

namespace karpenko
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape & source);
    CompositeShape(CompositeShape && source) noexcept;
    ~CompositeShape() = default;

    CompositeShape & operator =(const CompositeShape & source);
    CompositeShape & operator =(CompositeShape && source) noexcept;
    shapePtr operator [](int index) const;

    double getArea() const noexcept override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) noexcept override;
    void move(point_t newPos) override;
    void scale(double scaleRatio) override;
    void rotate(double angle) override;

    int getSize() const noexcept;
    shapesArray getShapesArray() const;
    void add(const shapePtr & shape);
    void remove(int delNumber);

  private:
    int size_;
    shapesArray shapesArray_;
  };
}

#endif
