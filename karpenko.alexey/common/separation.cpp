#include "separation.hpp"
#include <stdexcept>

// In order not to mix the subject of comments I will use the comment
// specifier "E*" to mark the generation of security exceptions. Like:
//1.E* "First comment about exception"


//E* This method does not change the any data.
bool karpenko::intersect(const rectangle_t & lhs, const rectangle_t & rhs)
{
  const point_t lhsTopLt = {lhs.pos.x - lhs.width / 2, lhs.pos.y + lhs.height / 2};
  const point_t lhsBotRt = {lhs.pos.x + lhs.width / 2, lhs.pos.y - lhs.height / 2};
  const point_t rhsTopLt = {rhs.pos.x - rhs.width / 2, rhs.pos.y + rhs.height / 2};
  const point_t rhsBotRt = {rhs.pos.x + rhs.width / 2, rhs.pos.y - rhs.height / 2};

  //if the top left point rhs is above and left about bottom right point lhs
  const bool condition1 = (rhsTopLt.x < lhsBotRt.x) && (rhsTopLt.y > lhsBotRt.y);

  //and Bottom Right point rhs is below and right about Top Left point lhs
  const bool condition2 = (rhsBotRt.y < lhsTopLt.y) && (rhsBotRt.x > lhsTopLt.x);

  //therefore the figures intersect
  return condition1 && condition2;
}


//1.E* If an exception is thrown at this point, the actual program data has not been changed yet.
//2.E* tempMatrix doesn't change actual data.
//3.E* If when changing temp data an error occurs, the destructor of the smart pointer will free up 
//E*   memory and we will be able to avoid memory leak. 
//E*   In the process of unwinding the stack, for the other variables, destructors will also be called
//4.E* If the function is successfully completed with a temporary matrix,
//E*   we return the finished object. Strong exception guarantee is respected here.
karpenko::Matrix karpenko::separation(const CompositeShape & composite)
{
  if (composite.getSize() == 0)
  {
    throw std::logic_error("Input figure is empty!"); //1.E*
  }

  Matrix tempMatrix; //2.E*
  const shapesArray inputArr = composite.getShapesArray();

  for (int i = 0; i < composite.getSize(); i++)
  {
    size_t rightLay = 0;
    size_t rightCol = 0;
    size_t currentCols = 0;

    //3.E*
    for (size_t j = 0; j < tempMatrix.getLayers(); j++)
    {
      currentCols = tempMatrix.getLayerSize(j);
      for (size_t k = 0; k < currentCols; k++)
      {
        // going through layer... If the shapes are intersected -> next layer
        if(intersect(inputArr[i]->getFrameRect(), tempMatrix[j][k]->getFrameRect()))
        {
          rightLay++;
          break;
        }
        // if the layer is end and not intersected -> get place in this layer
        if (k == currentCols - 1)
        {
          rightLay = j;
          rightCol = currentCols;
        }
      }

      if (rightLay == j) //if the correct layer was found
      {
        break;
      }
    }
    tempMatrix.add(inputArr[i], rightLay, rightCol);
  }
  //4.E*
  return tempMatrix;
}


//
karpenko::Matrix karpenko::separation(const shapesArray & arr, int size)
{
  CompositeShape composite;
  for (int i = 0; i < size; i++)
  {
    composite.add(arr[i]);
  }

  return separation(composite);
}

//P.S if some part of the code regarding exceptions is not commented, then most likely I did it specifically to avoid repetitions.

