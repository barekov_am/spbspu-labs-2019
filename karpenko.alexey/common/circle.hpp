#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

namespace karpenko
{
  class Circle : public Shape
  {
  public:
    Circle(double radius, point_t center);

    double getArea() const noexcept override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) noexcept override;
    void move(point_t newPos) noexcept override;
    void scale(double scaleRatio) override;
    void rotate(double angle) override;

  private:
    point_t center_;
    double radius_;
  };
}

#endif
