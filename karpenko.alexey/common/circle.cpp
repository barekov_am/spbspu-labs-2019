#include "circle.hpp"
#define _USE_MATH_DEFINES
#include <cmath>
#include <stdexcept>

// In order not to mix the subject of comments I will use the comment
// specifier "E*" to mark the generation of security exceptions. Like:
//1.E* "First comment about exception"


//1.E* If an exception is thrown at this point, fully constructed
//     elements "center_", "radius_" will be correctly destroyed during the stack unwinding
karpenko::Circle::Circle(double radius, point_t center) :
  center_(center),
  radius_(radius)
{
  if (radius <= 0.0)
  {
    throw std::invalid_argument("Radius must be positive number");  //1.E*
  }
}

//E* This method does not change the any data.
double karpenko::Circle::getArea() const noexcept
{
  return M_PI * radius_ * radius_;
}

//E* This method does not change the any data.
karpenko::rectangle_t karpenko::Circle::getFrameRect() const
{
  return { radius_ * 2, radius_ * 2 ,center_ };
}

//E* This method probably will not throw exceptions.
void karpenko::Circle::move(double dx, double dy) noexcept
{
  center_.x += dx;
  center_.y += dy;
}

//E* This method probably will not throw exceptions.
void karpenko::Circle::move(point_t newPos) noexcept
{
  center_ = newPos;
}

//E* Even if an exception is thrown, it will not affect data integrity
void karpenko::Circle::scale(double scaleRatio)
{
  if (scaleRatio < 0.0)
  {
    throw std::invalid_argument("scaleRatio should be positive value!"); //E*
  }
  radius_ *= scaleRatio;
}

void karpenko::Circle::rotate(double)
{
}
