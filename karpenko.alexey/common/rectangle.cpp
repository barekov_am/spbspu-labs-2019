#include "rectangle.hpp"
#define _USE_MATH_DEFINES
#include <cmath>
#include <stdexcept>

// In order not to mix the subject of comments I will use the comment
// specifier "E*" to mark the generation of security exceptions. Like:
//1.E* "First comment about exception"


//E* This constructor satisfies Strong exception guarantee
//1.E* If an exception is thrown at this point, fully constructed
//     elements "rect_", "angle_" will be correctly destroyed during the stack unwinding
karpenko::Rectangle::Rectangle(rectangle_t rect, double angle) :
  rect_(rect),
  angle_(angle)
{
  if ((rect.width <= 0.0) || (rect.height <= 0.0))
  {
    throw std::invalid_argument("Width and height must be positive numbers"); //1.E*
  }
}


//E* This method does not change the any data.
double karpenko::Rectangle::getArea() const noexcept
{
  return rect_.width * rect_.height;
}


//E* This method does not change the any data.
karpenko::rectangle_t karpenko::Rectangle::getFrameRect() const
{
  const double sinus = fabs(sin(angle_ * M_PI / 180));
  const double cosine = fabs(cos(angle_ * M_PI / 180));

  const double newHeight = rect_.height * cosine + rect_.width * sinus;
  const double newWidth = rect_.height * sinus + rect_.width * cosine;

  return {newWidth, newHeight, rect_.pos};
}


//E* This method does not change the any data.
double karpenko::Rectangle::getAngle() const noexcept
{
  return angle_;
}

//E* This method probably will not throw exceptions.
void karpenko::Rectangle::move(double dx, double dy) noexcept
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

//E* This method probably will not throw exceptions.
void karpenko::Rectangle::move(point_t newPos) noexcept
{
  rect_.pos = newPos;
}

//E* Even if an exception is thrown, it will not affect data integrity
void karpenko::Rectangle::scale(double scaleRatio)
{
  if (scaleRatio < 0.0)
  {
    throw std::invalid_argument("scaleRatio should be positive value!"); //E*
  }
  rect_.height *= scaleRatio;
  rect_.width *= scaleRatio;
}

//E* This method probably will not throw exceptions.
void karpenko::Rectangle::rotate(double angle) noexcept
{
  angle_ += angle;
}
