#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteRectangle)

const double LAPSE = 0.01;

BOOST_AUTO_TEST_CASE(afterMoveTestingRectangle)
{
  karpenko::Rectangle rectangle({3.5, 6.3, {2.0, 7.0}}, 0.0);
  const karpenko::rectangle_t originalRect = rectangle.getFrameRect();
  const double originalSquare = rectangle.getArea();

  rectangle.move(5.0, 6.0);
  karpenko::rectangle_t afterMoveData = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(rectangle.getArea(), originalSquare, LAPSE);
  BOOST_CHECK_CLOSE(afterMoveData.height, originalRect.height, LAPSE);
  BOOST_CHECK_CLOSE(afterMoveData.width, originalRect.width, LAPSE);

  rectangle.move({3.0, 2.0});
  afterMoveData = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(rectangle.getArea(), originalSquare, LAPSE);
  BOOST_CHECK_CLOSE(afterMoveData.height, originalRect.height, LAPSE);
  BOOST_CHECK_CLOSE(afterMoveData.width, originalRect.width, LAPSE);
}

BOOST_AUTO_TEST_CASE(quadroMetamaorphoseRectangleTesting)
{
  karpenko::Rectangle rectangle({3.5, 6.3, {2.0, 7.0}}, 0.0);
  const double originalSquare = rectangle.getArea();

  const double scaleRatio = 3.3;
  rectangle.scale(scaleRatio);
  BOOST_CHECK_CLOSE(rectangle.getArea(), originalSquare * scaleRatio * scaleRatio, LAPSE);
}

BOOST_AUTO_TEST_CASE(argumentTestingRectangle)
{
  BOOST_CHECK_THROW(karpenko::Rectangle({-2.0, 7.0, {2.0, 2.0}}, 0.0), std::invalid_argument);
  BOOST_CHECK_THROW(karpenko::Rectangle({2.0, -7.0, {2.0, 2.0}}, 0.0), std::invalid_argument);

  karpenko::Rectangle rectangle({2.0, 3.0, {2.0, 2.0}}, 0.0);
  BOOST_CHECK_THROW(rectangle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(afterRotateTesting)
{
  karpenko::Rectangle rectangle({4.0, 2.0, {0.0, 0.0}}, 0.0);
  const karpenko::rectangle_t originalRect = rectangle.getFrameRect();
  const double originalArea = rectangle.getArea();

  double angle = -90;
  rectangle.rotate(angle);
  karpenko::rectangle_t resultFrame = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(originalRect.height, resultFrame.height / 2, LAPSE);
  BOOST_CHECK_CLOSE(originalRect.width, resultFrame.width * 2, LAPSE);
  BOOST_CHECK_EQUAL(originalRect.pos.x, resultFrame.pos.x);
  BOOST_CHECK_EQUAL(originalRect.pos.y, resultFrame.pos.y);

  rectangle.rotate(angle);
  rectangle.rotate(angle);
  rectangle.rotate(angle);
  resultFrame = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(originalRect.height, resultFrame.height, LAPSE);
  BOOST_CHECK_CLOSE(originalRect.width, resultFrame.width, LAPSE);
  BOOST_CHECK_EQUAL(originalRect.pos.x, resultFrame.pos.x);
  BOOST_CHECK_EQUAL(originalRect.pos.y, resultFrame.pos.y);

  angle = 28.3;
  rectangle.rotate(angle);
  resultFrame = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(originalArea, rectangle.getArea(), LAPSE);
  BOOST_CHECK_EQUAL(originalRect.pos.x, resultFrame.pos.x);
  BOOST_CHECK_EQUAL(originalRect.pos.y, resultFrame.pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
