#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <memory>
#include "base-types.hpp"

namespace karpenko
{
  class Shape
  {
  public:
    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(point_t newPos) = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void scale(double scaleRatio) = 0;
    virtual void rotate(double angle) = 0;
  };

  using shapePtr = std::shared_ptr<Shape>;
  using shapesArray = std::unique_ptr<shapePtr[]>;
}
#endif
