#include <stdexcept>
#include <algorithm>
#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteMatrix)

BOOST_AUTO_TEST_CASE(addingSizeTest)
{
  karpenko::Matrix matrix;
  karpenko::Circle figureOnLay1(6.5, {2.0, 7.0});
  karpenko::Rectangle figureOnLay2({3.5, 6.3, {2.0, 0.0}}, 0.0);

  const int row1 = 0;
  const int col1 = 0;
  const int row2 = 1;

  matrix.add(std::make_shared<karpenko::Circle>(figureOnLay1), row1, col1);
  matrix.add(std::make_shared<karpenko::Rectangle>(figureOnLay2), row2, col1);

  BOOST_CHECK_EQUAL(matrix.getLayers(), std::max(row1, row2) + 1);
  BOOST_CHECK_EQUAL(matrix.getCols(), col1 + 1);
}

BOOST_AUTO_TEST_CASE(argumentTestingMatrix)
{
  karpenko::Matrix matrix;
  karpenko::Circle circle(6.5, {2.0, 7.0});
  karpenko::Rectangle rectangle({3.5, 6.3, {2.0, 0.0}}, 0.0);

  matrix.add(std::make_shared<karpenko::Circle>(circle), 0, 0);
  matrix.add(std::make_shared<karpenko::Rectangle>(rectangle), 0, 1);

  BOOST_CHECK_THROW(matrix[-3][0], std::out_of_range);
  BOOST_CHECK_THROW(matrix[7][0], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(correctCopyingSemantics)
{
  karpenko::Matrix oldMatrix;
  karpenko::Circle circle(6.5, {2.0, 7.0});
  karpenko::Rectangle rectangle({3.5, 6.3, {2.0, 0.0}}, 0.0);
  oldMatrix.add(std::make_shared<karpenko::Circle>(circle), 0, 0);
  oldMatrix.add(std::make_shared<karpenko::Rectangle>(rectangle), 1, 0);

  karpenko::Matrix newMatrix = oldMatrix;

  BOOST_CHECK_EQUAL(newMatrix.getCols(), oldMatrix.getCols());
  BOOST_CHECK_EQUAL(newMatrix.getLayers(), oldMatrix.getLayers());
}

BOOST_AUTO_TEST_CASE(correctMoveSemantics)
{
  karpenko::Matrix oldMatrix;
  karpenko::Circle circle(6.5, {2.0, 7.0});
  karpenko::Rectangle rectangle({3.5, 6.3, {2.0, 0.0}}, 0.0);
  oldMatrix.add(std::make_shared<karpenko::Circle>(circle), 0, 0);
  oldMatrix.add(std::make_shared<karpenko::Rectangle>(rectangle), 1, 0);

  const size_t oldMatrixLayers = oldMatrix.getLayers();
  const size_t empty = 0;

  karpenko::Matrix newMatrix = std::move(oldMatrix);

  BOOST_CHECK_EQUAL(oldMatrixLayers, newMatrix.getLayers());
  BOOST_CHECK_EQUAL(empty, oldMatrix.getLayers());
}

BOOST_AUTO_TEST_SUITE_END()
