#include "matrix.hpp"
#include <stdexcept>
#include <algorithm>

// In order not to mix the subject of comments I will use the comment
// specifier "E*" to mark the generation of security exceptions. Like:
//1.E* "First comment about exception"


//E* If an exception is raised in the constructor, 
//E* then everything that has been full-constructed would call auto-destructor.
//E* (C) Lecture "Exceptions" from 13.05.2019
karpenko::Matrix::Matrix() :
  layers_(0),
  cols_(0)
{
}


//*E For the same reason this copy constructor satisfies strong exception guarantee.
karpenko::Matrix::Matrix(const Matrix & source) :
  layers_(source.layers_),
  cols_(source.cols_),
  store_(std::make_unique<shapePtr[]>(source.layers_ * source.cols_))
{
  for (size_t i = 0; i < (layers_ * cols_); i++)
  {
    store_[i] = source.store_[i];
  }
}


//E* http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2010/n3050.html
//E* 23.3.2.3 deque modifiers [deque.modifiers] 
//E* Change Paragraph 2 as follows:
//E*..If an exception is thrown by the move constructor, the effects are unspecified.
//E* Therefore I use "noexcept" specifier
karpenko::Matrix::Matrix(Matrix && source) noexcept :
  layers_(source.layers_),
  cols_(source.cols_),
  store_(std::move(source.store_))
{
  source.layers_ = 0;
  source.cols_ = 0;
}


//1.E* Creating temporary variables, I do not change the actual data
//2.E* Using smart pointers helps avoid memory leak. Actual data is safe.
//3.E* If everything went relatively well, and the exceptions were not thrown, I change the actual data.
//E*   Strong exception guarantee is respected here.
//4.E* swap-function usually satisfy Strong exception guarantee because of noexcept specifier.
karpenko::Matrix& karpenko::Matrix::operator =(const Matrix & rhs)
{
  if (this != & rhs)
  {
    //1.E*
    const size_t tempLayers = rhs.layers_;
    const size_t tempCols = rhs.cols_;
    shapesArray tempStore = std::make_unique<shapePtr[]>(rhs.layers_ * rhs.cols_);

    for(size_t i = 0; i < (rhs.layers_ * rhs.cols_); i++)
    {
      //2.E*
      tempStore[i] = rhs.store_[i];
    }

    //3.E*
    layers_ = tempLayers;
    cols_ = tempCols;
    store_.swap(tempStore); //4.E*
  }

  return *this;
}


//E* The noexcept is used for the same reason as in the move constructor.
karpenko::Matrix& karpenko::Matrix::operator =(Matrix && rhs) noexcept
{
  if (this != & rhs)
  {
    layers_ = rhs.layers_;
    rhs.layers_ = 0;

    cols_ = rhs.cols_;
    rhs.cols_ = 0;

    store_ = std::move(rhs.store_);
  }

  return *this;
}


//1.E* At the moment, the program did not have time to change the actual data
//2.E* Creating temporary variables, I do not change the actual data
//3.E* Using smart pointers helps avoid memory leak. Actual data is safe.
//E*   Strong exception guarantee is respected here.
karpenko::shapesArray karpenko::Matrix::operator [](size_t layer) const
{
  if (layers_ <= layer)
  {
    throw std::out_of_range("Invalid parameter layer"); //1.E*
  }

  shapesArray tempStore(std::make_unique<shapePtr[]>(cols_)); //2.E*
  for (size_t i = 0; i < cols_; i++)
  {
    //3.E*
    tempStore[i] = store_[layer * cols_ + i];
  }

  return tempStore;
}


//E* This method does not change the any data.
bool karpenko::Matrix::operator ==(const Matrix & rhs) const
{
  if ((layers_ != rhs.layers_) || (cols_ != rhs.cols_))
  {
    return false;
  }

  for (size_t i = 0; i < (layers_ * cols_); i++)
  {
    if (store_[i] != rhs.store_[i])
    {
      return false;
    }
  }
  return true;
}


//E* This method does not change the any data.
bool karpenko::Matrix::operator !=(const Matrix & rhs) const
{
  return !(*this == rhs);
}


//E* This method does not change the any data.
size_t karpenko::Matrix::getLayers() const
{
  return layers_;
}


//E* This method does not change the any data.
size_t karpenko::Matrix::getCols() const
{
  return cols_;
}


//E* This method does not change the any data.
size_t karpenko::Matrix::getLayerSize(size_t layer) const
{
  if (layer >= layers_)
  {
    return 0;
  }
  for (size_t i = 0; i < cols_; i++)
  {
    if (store_[layer * cols_ + i] == nullptr)
    {
      return i;
    }
  }
  return cols_;
}


//1.E* Creating temporary variables, I do not change the actual data.
//2.E* If when copying objects an error occurs, the destructor of the smart pointer will free up 
//E*   memory and we will be able to avoid memory leak. In fact, without changing the state 
//E*   of the object it remained in the same state as before the start of the operation. 
//3.E* If everything went relatively well, and the exceptions were not thrown, I change the actual data.
//E*   Strong exception guarantee is respected here.
//4.E* swap-function usually satisfy Strong exception guarantee because of noexcept specifier.
void karpenko::Matrix::add(shapePtr shape, size_t layer, size_t col)
{
  //1.E*
  const size_t tempLayers = std::max(layers_, layer + 1);
  const size_t tempCols = std::max(cols_, col + 1);

  shapesArray tempStore(std::make_unique<shapePtr[]>(tempLayers * tempCols));
 
  for (size_t i = 0; i < layers_; i++)
  {
    for (size_t j = 0; j < cols_; j++)
    {
      tempStore[i * tempCols + j] = store_[i * cols_ + j]; //2.E*
    }
  }
  tempStore[layer * tempCols + col] = shape;

  //3.E*
  layers_ = tempLayers;
  cols_ = tempCols;
  store_.swap(tempStore); //4.E*
}

//P.S if some part of the code regarding exceptions is not commented, then most likely I did it specifically to avoid repetitions.
