#ifndef MATRIX_HPP
#define MATRIX_HPP
#include <memory>
#include "shape.hpp"

namespace karpenko
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix & source);
    Matrix(Matrix && source) noexcept;
    ~Matrix() = default;

    Matrix & operator =(const Matrix & rhs);
    Matrix & operator =(Matrix && rhs) noexcept;

    shapesArray operator [](size_t layer) const;
    bool operator ==(const Matrix & rhs) const;
    bool operator !=(const Matrix & rhs) const;

    size_t getLayers() const;
    size_t getCols() const;
    size_t getLayerSize(size_t layer) const;

    void add(shapePtr shape, size_t layer, size_t col);

  private:
    size_t layers_;
    size_t cols_;
    shapesArray store_;
  };
}

#endif
