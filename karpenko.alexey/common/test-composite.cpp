#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteCompositeShape)

const double LAPSE = 0.01;

BOOST_AUTO_TEST_CASE(afterMoveTestingCompositeShape)
{
  karpenko::Circle circle(6.5, {2.0, 7.0});
  karpenko::Rectangle rectangle({3.5, 6.3, {2.0, 7.0}}, 0.0);
  karpenko::CompositeShape compositeShape;
  compositeShape.add(std::make_shared<karpenko::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<karpenko::Circle>(circle));
  const karpenko::rectangle_t originalFrame = compositeShape.getFrameRect();
  const double originalSquare = compositeShape.getArea();


  compositeShape.move(4.0, 2.4);
  karpenko::rectangle_t afterMoveData = compositeShape.getFrameRect();
  BOOST_CHECK_CLOSE(compositeShape.getArea(), originalSquare, LAPSE);
  BOOST_CHECK_CLOSE(afterMoveData.height, originalFrame.height, LAPSE);
  BOOST_CHECK_CLOSE(afterMoveData.width, originalFrame.width, LAPSE);

  compositeShape.move({3.0, 7.0});
  afterMoveData = compositeShape.getFrameRect();
  BOOST_CHECK_CLOSE(compositeShape.getArea(), originalSquare, LAPSE);
  BOOST_CHECK_CLOSE(afterMoveData.height, originalFrame.height, LAPSE);
  BOOST_CHECK_CLOSE(afterMoveData.width, originalFrame.width, LAPSE);
}

BOOST_AUTO_TEST_CASE(quadroMetamaorphoseCompositeShapeTesting)
{
  karpenko::Circle circle(6.5, {2.0, 7.0});
  karpenko::Rectangle rectangle({3.5, 6.3, {2.0, 7.0}}, 0.0);
  karpenko::CompositeShape compositeShape;
  compositeShape.add(std::make_shared<karpenko::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<karpenko::Circle>(circle));
  const double originalSquare = compositeShape.getArea();

  double scaleRatio = 2.0;
  compositeShape.scale(scaleRatio);
  BOOST_CHECK_CLOSE(compositeShape.getArea(), originalSquare * scaleRatio * scaleRatio, LAPSE);

  scaleRatio = 0.5;
  compositeShape.scale(scaleRatio);
  BOOST_CHECK_CLOSE(compositeShape.getArea(), originalSquare, LAPSE);
}


BOOST_AUTO_TEST_CASE(argumentTestingCompositeShape)
{
  karpenko::Circle circle(6.5, {2.0, 7.0});
  karpenko::Rectangle rectangle({3.5, 6.3, {2.0, 7.0}}, 0.0);
  karpenko::CompositeShape compositeShape;
  
  BOOST_CHECK_THROW(compositeShape.scale(2), std::logic_error);
  
  compositeShape.add(std::make_shared<karpenko::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<karpenko::Circle>(circle));
  
  BOOST_CHECK_THROW(compositeShape.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.scale(-1), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.remove(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(addingDeletingToCompositeShapeTesting)
{
  karpenko::Circle circle(6.5, { 2.0, 7.0});
  karpenko::Rectangle rectangle({3.5, 6.3, {2.0, 7.0}}, 0.0);
  karpenko::CompositeShape compositeShape;
  int testSize = 0;

  compositeShape.add(std::make_shared<karpenko::Circle>(circle));
  testSize++;
  compositeShape.add(std::make_shared<karpenko::Rectangle>(rectangle));
  testSize++;
  BOOST_CHECK_EQUAL(compositeShape.getSize(), testSize);

  compositeShape.remove(testSize);
  testSize--;
  BOOST_CHECK_EQUAL(compositeShape.getSize(), testSize);
  compositeShape.remove(testSize);
  testSize--;
  BOOST_CHECK_EQUAL(compositeShape.getSize(), testSize);
}

BOOST_AUTO_TEST_CASE(outOfRangeException)
{
  const karpenko::CompositeShape compositeShape;
  BOOST_CHECK_THROW(compositeShape[-1], std::out_of_range);
  BOOST_CHECK_THROW(compositeShape[100], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(getSizeTesting)
{
  karpenko::Circle circle(6.5, {2.0, 7.0});
  karpenko::Rectangle rectangle({3.5, 6.3, {2.0, 7.0}}, 0.0);
  karpenko::CompositeShape compositeShape;
  int count = 0;

  compositeShape.add(std::make_shared<karpenko::Circle>(circle));
  count++;
  compositeShape.add(std::make_shared<karpenko::Rectangle>(rectangle));
  count++;

  BOOST_CHECK_EQUAL(compositeShape.getSize(), count);
}

BOOST_AUTO_TEST_CASE(correctCopyingSemantics)
{
  karpenko::Circle circle(3.0, {2.0, 7.0});
  karpenko::Rectangle rectangle({3.0, 4.0, {1.0, 3.0}}, 0.0);
  const double comparingArea = circle.getArea() + rectangle.getArea();

  karpenko::CompositeShape oldComposite;
  oldComposite.add(std::make_shared<karpenko::Circle>(circle));

  const int beforeCopyingSize = oldComposite.getSize();

  karpenko::CompositeShape newComposite = oldComposite;
  newComposite.add(std::make_shared<karpenko::Rectangle>(rectangle));

  BOOST_CHECK_CLOSE(comparingArea, newComposite.getArea(), LAPSE);
  BOOST_CHECK_EQUAL(beforeCopyingSize, oldComposite.getSize());
}

BOOST_AUTO_TEST_CASE(correctMoveSemantics)
{
  karpenko::Circle circle(3.0, {2.0, 7.0});
  karpenko::Rectangle rectangle({3.0, 4.0, {1.0, 3.0}}, 0.0);

  karpenko::CompositeShape oldComposite;
  oldComposite.add(std::make_shared<karpenko::Rectangle>(rectangle));
  oldComposite.add(std::make_shared<karpenko::Circle>(circle));

  int comparingSize = oldComposite.getSize();

  karpenko::CompositeShape newComposite = std::move(oldComposite);

  BOOST_CHECK_EQUAL(comparingSize, newComposite.getSize());
  
  comparingSize = 0;
  BOOST_CHECK_EQUAL(comparingSize, oldComposite.getSize());
}

BOOST_AUTO_TEST_CASE(compositeRotate)
{
  karpenko::Circle circle(6.5, {2.0, 7.0});
  karpenko::Rectangle rectangle({3.5, 6.3, {2.0, 7.0}}, 0.0);
  karpenko::CompositeShape compositeShape;
  compositeShape.add(std::make_shared<karpenko::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<karpenko::Circle>(circle));
  
  const double areaBefore = compositeShape.getArea();
  const karpenko::rectangle_t frameBefore = compositeShape.getFrameRect();

  double angle = 45.0;
  compositeShape.rotate(angle);
  angle = 30.0;
  compositeShape.rotate(angle);

  const double areaAfter = compositeShape.getArea();
  const karpenko::rectangle_t frameAfter = compositeShape.getFrameRect();
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, LAPSE);
  BOOST_CHECK_CLOSE(frameBefore.pos.x, frameAfter.pos.x, LAPSE);
  BOOST_CHECK_CLOSE(frameBefore.pos.y, frameAfter.pos.y, LAPSE);
}

BOOST_AUTO_TEST_SUITE_END()
