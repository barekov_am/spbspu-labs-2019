#include "point.hpp"
#include <iostream>
#include "manipulator.hpp"

std::istream& operator>>(std::istream &in, Point &vertex)
{
  char firstBracket = ' ';
  char lastBracket = ' ';
  char semicolon = ' ';
  
  in >> manipulator::skipBlank >> firstBracket;
  if (firstBracket != '(')
  {
    in.setstate(std::ios_base::failbit);
    return in;
  }

  in >> manipulator::skipBlank >> vertex.x
    >> manipulator::skipBlank >> semicolon;
  if (semicolon != ';')
  {
    in.setstate(std::ios_base::failbit);
    return in;
  }

  in >> manipulator::skipBlank >> vertex.y
    >> manipulator::skipBlank >> lastBracket;
  if (lastBracket != ')')
  {
    in.setstate(std::ios_base::failbit);
  }

  return in;
}

std::ostream& operator<<(std::ostream &out, const Point &vertex)
{
  out << "(" << vertex.x << ";" << vertex.y << ")";
  return out;
}

