#ifndef B7_D_POINT_HPP
#define B7_D_POINT_HPP

#include <iosfwd>

struct Point
{
  int x;
  int y;
};


std::istream& operator>>(std::istream &in, Point &vertex);
std::ostream& operator<<(std::ostream &out, const Point &vertex);

#endif
