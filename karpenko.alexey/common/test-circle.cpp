#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteCircle)

const double LAPSE = 0.01;

BOOST_AUTO_TEST_CASE(afterMoveTestingCircle)
{
  karpenko::Circle circle(6.5, {2.0, 7.0});
  const karpenko::rectangle_t originalFrame = circle.getFrameRect();
  const double originalSquare = circle.getArea();

  circle.move(4.0, 2.4);
  karpenko::rectangle_t afterMoveData = circle.getFrameRect();
  BOOST_CHECK_CLOSE(circle.getArea(), originalSquare, LAPSE);
  BOOST_CHECK_CLOSE(afterMoveData.height, originalFrame.height, LAPSE);
  BOOST_CHECK_CLOSE(afterMoveData.width, originalFrame.width, LAPSE);

  circle.move({3.0, 7.0});
  afterMoveData = circle.getFrameRect();
  BOOST_CHECK_CLOSE(circle.getArea(), originalSquare, LAPSE);
  BOOST_CHECK_CLOSE(afterMoveData.height, originalFrame.height, LAPSE);
  BOOST_CHECK_CLOSE(afterMoveData.width, originalFrame.width, LAPSE);
}

BOOST_AUTO_TEST_CASE(quadroMetamaorphoseCircleTesting)
{
  karpenko::Circle circle(6.5, {2.0, 7.0});
  const double originalSquare = circle.getArea();

  const double scaleRatio = 3.3;
  circle.scale(scaleRatio);
  BOOST_CHECK_CLOSE(circle.getArea(), originalSquare * scaleRatio * scaleRatio, LAPSE);
}

BOOST_AUTO_TEST_CASE(argumentTestingCircle)
{
  BOOST_CHECK_THROW(karpenko::Circle(-5.0, {2.0, 6.0}), std::invalid_argument);

  karpenko::Circle circle(6.5, {2.0, 7.0});
  BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
