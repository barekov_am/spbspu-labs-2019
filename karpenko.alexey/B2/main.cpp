#include <string>
#include "queue.hpp"

void task1();
void task2();

int main(int argc, char* argv[])
{
  try
  {
    if (argc == 2)
    {
      const unsigned short int taskNumber = std::stoi(argv[1]);
      switch (taskNumber)
      {
      case 1:
        task1();
        break;
      case 2:
        task2();
        break;
      default:
        std::cerr << "Invalid number of task";
        return 1;
      }
    }
    else
    {
      std::cerr << "Invalid number input parameters, only 1 expected";
      return 1;
    }
  }
  catch (std::exception & exception)
  {
    std::cerr << exception.what();
    return 1;
  }

  return 0;
}
