#include <iostream>
#include <string>

#include "tasks.hpp"

int main(int args, char* argv[])
{

  if (args != 2)
  {
    std::cerr << "Invalid number of arguments";
    return 1;
  }

  try
  {
    std::locale loc;
    std::cin.imbue(loc);
    std::locale::global(loc);
    std::cout.imbue(loc);

    int option = std::stoi(argv[1]);
    switch (option)
    {
    case 1:
      task1(std::cin, std::cout);
      break;
    case 2:
      task2(std::cin, std::cout);
      break;
    default:
      std::cerr << "invalid task";
      return 1;
    }
  }
  catch (const std::invalid_argument & e)
  {
    std::cerr << e.what();
    return 1;
  }

  return 0;
}
