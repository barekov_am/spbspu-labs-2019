#include <vector>
#include <algorithm>
#include <functional>

#include "functor.hpp"

int main() {

  std::vector<int> inputVector;
  karpenko::Functor functor;

  try {
    int elem;

    while (std::cin >> elem) {
      inputVector.push_back(elem);
    }

    if (!(std::cin.eof())) {
      throw(std::invalid_argument("Wrong input!"));
    }

    std::for_each(inputVector.begin(), inputVector.end(), std::ref(functor));

    functor.output(std::cout);

  }
  catch (std::exception & e) {
    std::cerr << e.what();
    return 1;
  }

  return 0;

}
