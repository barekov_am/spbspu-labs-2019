#include <iostream>
#include <iomanip>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "separation.hpp"

const double scaleRate1 = 3.42;
const double scaleRate2 = 2.0;
const double angle = 90.0;

void printRectangle(const karpenko::Shape * shape)
{
  if (shape == nullptr)
  {
    std::cout << "There no figure to print.";
  }
  else
  {
    const karpenko::rectangle_t frameRect = shape->getFrameRect();
    std::cout << "width is " << frameRect.width;
    std::cout << "; height is " << frameRect.height << ";\n";
    std::cout << "center on [" << frameRect.pos.x << ";";
    std::cout << frameRect.pos.y << "];\n";
    std::cout << "area of rectangle is " << shape->getArea() << ";\n";
  }
}

void printCircle(const karpenko::Shape * shape)
{
  if (shape == nullptr)
  {
    std::cout << "There no figure to print.";
  }
  else
  {
    const karpenko::rectangle_t frameRect = shape->getFrameRect();
    std::cout << "width of frame circle rectangle is " << frameRect.width;
    std::cout << "; height of frame circle rectangle is " << frameRect.height << ";\n";
    std::cout << "center on [" << frameRect.pos.x << ";" << frameRect.pos.y << "];\n";
    std::cout << "area of circle is " << shape->getArea() << ";\n\n";
  }
}

void printCompositeShape(const karpenko::CompositeShape * compositeShape)
{
  if (compositeShape == nullptr)
  {
    std::cout << "There no composite shape to print.";
  }
  else
  {
    const karpenko::rectangle_t frameRect = compositeShape->getFrameRect();
    std::cout << "width of frame Composite Shape rectangle is " << frameRect.width;
    std::cout << "; height of frame Composite Shape rectangle is " << frameRect.height << ";\n";
    std::cout << "center on [" << frameRect.pos.x << ";" << frameRect.pos.y << "];\n";
    std::cout << "area of Composite Shape is " << compositeShape->getArea() << ";\n";
  }
}

int main()
{
  karpenko::Rectangle rectangle({4.0, 2.0, {0.0, 0.0}}, 0.0);

  std::cout << "Rectangle demonstration started.\n\n";
  printRectangle(&rectangle);
  std::cout << "Moving to coordinates...\n";
  rectangle.move(5.0, 4.0);
  printRectangle(&rectangle);
  std::cout << "Moving to point...\n";
  rectangle.move({3.0, 3.0});
  printRectangle(&rectangle);

  std::cout << "Applies scaling with coefficient = " << scaleRate1 << "\n";
  rectangle.scale(scaleRate1);
  printRectangle(&rectangle);

  std::cout << "Applies rotating with coefficient = " << angle << "\n";
  rectangle.rotate(angle);
  printRectangle(&rectangle);

  std::cout << "End program for Rectangle;\n\n";


  karpenko::Circle circle(5.0, {1.0, 2.0});

  std::cout << "Circle demonstration started.\n";
  printCircle(&circle);
  std::cout << "Moving to coordinates...\n";
  circle.move(5.0, 6.0);
  printCircle(&circle);
  std::cout << "Moving to point...\n";
  circle.move({3.0, 3.0});
  printCircle(&circle);

  std::cout << "Applies scaling with coefficient = " << scaleRate1 << "\n";
  circle.scale(scaleRate2);
  printCircle(&circle);
  std::cout << "End program for Circle;\n";



  std::cout << "Start program for Composite Shape...\n";

  karpenko::Rectangle someShape1({2.0, 2.0, {1.0, 1.0}}, 0.0);
  karpenko::Rectangle someShape2({2.0, 2.0, {3.0, 1.0}}, 0.0);
  karpenko::Circle someShape3(2.0, {2.0, 4.0});
  karpenko::Rectangle someShape4({10.0, 2.0, {3.0, 1.0}}, 0.0);

  karpenko::CompositeShape compositeShape;

  printCompositeShape(&compositeShape);

  compositeShape.add(std::make_shared<karpenko::Rectangle>(someShape1));
  compositeShape.add(std::make_shared<karpenko::Rectangle>(someShape2));
  compositeShape.add(std::make_shared<karpenko::Circle>(someShape3));
  printCompositeShape(&compositeShape);
  compositeShape.add(std::make_shared<karpenko::Rectangle>(someShape4));
  printCompositeShape(&compositeShape);

  std::cout << "Moving to coordinates...\n";
  compositeShape.move(2.0, 2.0);
  printCompositeShape(&compositeShape);

  std::cout << "Moving to point...\n";
  compositeShape.move({3.0, 3.0});
  printCompositeShape(&compositeShape);

  std::cout << "Applies scaling with coefficient = " << scaleRate2 << "\n";
  compositeShape.scale(scaleRate2);
  printCompositeShape(&compositeShape);

  std::cout << "Applies rotating with coefficient = " << angle << "\n";
  compositeShape.rotate(angle);
  printCompositeShape(&compositeShape);

  compositeShape.remove(3);
  printCompositeShape(&compositeShape);

  std::cout << "Applying Composite Shape with matrix.\nStart demonstration for Matrix..\n\n\n";

  karpenko::Matrix matrix = separation(compositeShape);

  for (size_t l = 0; l < matrix.getLayers(); l++)
  {
    for (size_t c = 0; c < matrix.getLayerSize(l); c++)
    {
      std::cout << "| Figure with area " << std::fixed << std::setprecision(2);
      std::cout << matrix[l][c]->getArea() << " |";
    }
    std::cout << "\n";
  }

  std::cout << "\n\nEnd program for Matrix;\n";

  return 0;
}
