#include "algorithms.hpp"

#include <algorithm>
#include <sstream>

const int KEYS_QUANTITY = 2;

void algorithms::fill(std::vector<DataStruct>& vector, const std::pair<int, int>& keyRange)
{
  if (keyRange.first > keyRange.second)
  {
    throw std::invalid_argument("Invalid key range");
  }

  std::string line;
  while (std::getline(std::cin, line))
  {
    DataStruct tmp;
    std::istringstream input(line);
    auto isFirstIteration = true;

    int tmpKey = 0;
    char delim = 0;
    for (auto i = 0; i < KEYS_QUANTITY; i++)
    {
      input >> tmpKey;
      input >> delim;

      if (delim != ',')
      {
        throw std::invalid_argument("Incorrect data input");
      }

      if (tmpKey < keyRange.first || tmpKey > keyRange.second)
      {
        throw std::invalid_argument("Input Key must be in range: [-5, 5]");
      }

      isFirstIteration ? tmp.key1 = tmpKey : tmp.key2 = tmpKey;
      isFirstIteration = false;
    }
    getline(input, tmp.str);

    if (tmp.str.empty())
    {
      throw std::invalid_argument("Incorrect data input");
    }

    while ((tmp.str.at(0) == ' ') || (tmp.str.at(0) == '\t'))
    {
      tmp.str.erase(0, 1);
    }

    vector.push_back(tmp);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Reading error occured\n");
  }
}

void algorithms::sort(std::vector<DataStruct>& vector)
{

  std::sort(vector.begin(), vector.end(), compareDataStructs);
}

bool algorithms::compareDataStructs(const DataStruct& lhs, const DataStruct& rhs)
{
  if (lhs.key1 < rhs.key1)
  {
    return true;
  }

  if (lhs.key1 == rhs.key1)
  {
    if (lhs.key2 < rhs.key2)
    {
      return true;
    }

    if (lhs.key2 == rhs.key2)
    {
      if (lhs.str.size() < rhs.str.size())
      {
        return true;
      }
    }
  }
  return false;
}
