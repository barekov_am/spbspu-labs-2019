#include "algorithms.hpp"
#include <algorithm>

const std::pair<int, int> KEY_RANGE(-5, 5);

int main()
{
  try
  {
    std::vector<algorithms::DataStruct> vector;

    fill(vector, KEY_RANGE);
    sort(vector);
    std::for_each(vector.begin(), vector.end(),
      [&](auto el) { std::cout << el.key1 << "," << el.key2 << "," << el.str << "\n"; });
  }
  catch (std::invalid_argument& e)
  {
    std::cerr << e.what() << "\n";
    return 1;
  }

  return 0;
}

