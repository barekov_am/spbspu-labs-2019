#ifndef ALGORITHMS_HPP
#define ALGORITHMS_HPP

#include <vector>
#include <iostream>
#include <string>
#include <utility>

namespace algorithms
{

  struct DataStruct
  {
    int key1;
    int key2;
    std::string str;
  };


  void fill(std::vector<DataStruct>& vector, const std::pair<int, int>& keyRange);
  void sort(std::vector<DataStruct>& vector);
  bool compareDataStructs(const DataStruct& lhs, const DataStruct& rhs);
}
#endif

