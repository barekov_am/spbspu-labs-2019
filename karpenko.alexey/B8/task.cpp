#include "textHandler.hpp"

void task(std::istream& in, std::ostream& out, size_t width)
{
  TextHandler handler(width, in, out);
  handler.handleText();
  handler.sendRes();

  if (in.fail() && !in.eof())
  {
    throw std::ios_base::failure("Reading error occured");
  }

  if (out.fail())
  {
    throw std::ios_base::failure("Reading error occured");
  }
}
