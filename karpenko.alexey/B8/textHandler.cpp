#include "textHandler.hpp"

TextHandler::TextHandler(size_t width, std::istream& in, std::ostream& out):
  width_(width),
  text_({}),
  in_(in),
  out_(out),
  locale_(in.getloc()),
  decimalPoint_(std::use_facet<std::numpunct<char>>(locale_).decimal_point())
{
}

void TextHandler::handleText()
{ 
  while (in_)
  {
    in_ >> std::noskipws;
    const char c = static_cast<char>(in_.get());

    if (std::isalpha(c))
    {
      text_.push_back(algorithms::handleWord(c, in_));
    }
    else if (std::isdigit(c) || ((c == '+' || c == '-') && std::isdigit(in_.peek())))
    {
      text_.push_back(algorithms::handleNumber(c, in_, decimalPoint_));
    }
    else if (std::ispunct(c))
    {
      if (in_.peek() == '-')
      {
        text_.push_back(algorithms::handleDash(c, in_));
      }
      else
      {
        Token tmp { std::string(1, c), Type::punctuation};
        text_.push_back(tmp);
      }
     }
  }
}

void TextHandler::sendRes() const
{
  if (text_.empty())
  {
    return;
  }

  FormattingHandler formatHandler(text_, out_, width_);
  formatHandler.validateText();
  formatHandler.formatAndSend();
}
