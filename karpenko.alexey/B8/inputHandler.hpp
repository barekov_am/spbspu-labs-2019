#ifndef INPUT_HANDLER_HPP
#define INPUT_HANDLER_HPP

#include  "token.hpp"

namespace algorithms
{
  Token handleWord(char c, std::istream& in);
  Token handleNumber(char c, std::istream& in, char friendlyDecimalPoint);
  Token handleDash(char c, std::istream& in);
}

#endif
