#ifndef TEXT_HANDLER
#define TEXT_HANDLER

#include <vector>
#include <iostream>
#include <algorithm>

#include "token.hpp"
#include "inputHandler.hpp"
#include "formattingHandler.hpp"


class TextHandler
{
public:
  TextHandler(size_t width, std::istream& in, std::ostream& out);
  void handleText();
  void sendRes() const;

private:
  size_t width_;
  std::vector<Token> text_;
  std::istream& in_;
  std::ostream& out_;
  std::locale locale_;
  char decimalPoint_;
};

#endif
