#include <iostream>
#include <string>
#include <cstring>

#include "textHandler.hpp"

const int MIN_WIDTH = 25;
const int DEFAULT_WIDTH = 40;

void task(std::istream& in, std::ostream& out, size_t lineWidth = DEFAULT_WIDTH);

int main(int argc, char *argv[])
{
  try
  {
    int width = DEFAULT_WIDTH;
    switch (argc)
    {
      case 1:
        break;
      case 3:
        if (strcmp("--line-width", argv[1]) == 0) 
        {
          width = std::stoi(argv[2]);
          if (width < MIN_WIDTH)
          {
            throw std::invalid_argument("Line width must be more than 25");
          }
        }
        break;
      default:
        throw std::invalid_argument("Arguments must be like: --line-width 70");
    }

    task(std::cin, std::cout, width);
  }
  catch (std::exception &e)
  {
    std::cerr << e.what() << "\n";
    return 1;
  }

  return 0;
}
