#ifndef TOKEN_HPP
#define TOKEN_HPP

#include <string>

const size_t MAX_WORD_SIZE = 20;
const size_t DASH_SIZE = 3;

enum class Type
{
  word,
  number,
  punctuation,
  dash
};

struct Token
{
  std::string data;
  Type type;
};

#endif
