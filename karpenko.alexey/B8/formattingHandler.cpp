#include "formattingHandler.hpp"
#include <utility>

constexpr size_t SpaceLength = 1;

FormattingHandler::FormattingHandler(std::vector<Token> oldTokenVector, std::ostream& output, size_t width) :
  tokenVector_(std::move(oldTokenVector)),
  out_(output),
  lineWidth_(width),
  line_({}),
  prevLength_(),
  prevPos_()
{
  line_.reserve(width + 1);
}

void FormattingHandler::validateText()
{
  if ((tokenVector_.front().type == Type::dash) || tokenVector_.front().type == Type::punctuation)
  {
    throw std::invalid_argument("Text can't start with punctuation");
  }

  for (size_t i = 0; i < tokenVector_.size() - 1; i++)
  {
    if (tokenVector_.at(i).type == Type::punctuation && tokenVector_.at(i + 1).type == Type::punctuation)
    {
      throw std::invalid_argument("More than one punctuation sign goes in a row");
    }
  }
}

// Secondary functions block start:
void FormattingHandler::putNewToken(const std::string& stringWeWant)
{
  line_ += stringWeWant;
  prevLength_ = stringWeWant.size() + 1;
}

void FormattingHandler::putSpace()
{
  !line_.empty() ? line_ += " " : line_;
}

void FormattingHandler::execute()
{
  out_ << line_ << "\n";
  line_.clear();
  line_.reserve(lineWidth_ + 1);
}

void FormattingHandler::wrapPunctuation(size_t i)
{
  const bool prevPunct = tokenVector_.at(i - 1).type == Type::punctuation;
  const auto wrapSize = (prevPunct ? tokenVector_.at(i - 2) : tokenVector_.at(i - 1)).data.size() + static_cast<int>(!(tokenVector_.at(i).type == Type::punctuation));

  prevLength_ = wrapSize + SpaceLength + static_cast<int>(prevPunct);
  prevPos_ = line_.size() - prevLength_;

  std::string prevLine = line_.substr(prevPos_, prevLength_); //get last string
  line_.erase( prevPos_, prevLength_);
 
  execute();

  prevLine.erase(0, 1); //throw space
  line_ += prevLine;
}
void FormattingHandler::checkLastLine()
{
  if (!line_.empty())
  {
    execute();
  }
}
//end block.

// Main procedure
void FormattingHandler::formatAndSend()
{
  for (size_t i = 0; i < tokenVector_.size(); i++)
  {
    const auto token = tokenVector_.at(i);
    const std::string tmpStr = token.data;
    const size_t expectedLength = line_.size() + tmpStr.length() + static_cast<int>(!line_.empty() && token.type != Type::punctuation);
    prevPos_ = line_.size() - prevLength_;

    switch (token.type)
    {
    case Type::word:
    case Type::number:
      if (expectedLength > lineWidth_) // if not enough place to add new word/number
      {
        execute();
      }
      putSpace();
      putNewToken(tmpStr);
      break;

    case Type::dash:
      putSpace();
    
    case Type::punctuation:
      //case when symbol goes to the next line like the first character
      if (expectedLength > lineWidth_)
      {
        wrapPunctuation(i);
      }
      putNewToken(tmpStr);
      break;

    default:
      throw std::invalid_argument("Unknown error");
    }
  }

  checkLastLine();
}

