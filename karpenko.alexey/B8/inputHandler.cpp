#include "inputHandler.hpp"
#include <iostream>

Token algorithms::handleWord(char c, std::istream& in)
{
  std::string wordStr;
  wordStr.push_back(c);

  while (std::isalpha(in.peek()) || in.peek() == '-')
  {
    const char nextCh = static_cast<char>(in.get());
    if (nextCh == '-' && in.peek() == '-')
    {
      throw std::invalid_argument("Used double '-' symbol");
    }
    wordStr.push_back(nextCh);
  }

  if (wordStr.length() > MAX_WORD_SIZE)
  {
    throw std::invalid_argument("Max word length exceeded");
  }

  return {Token{wordStr, Type::word}};
}

Token algorithms::handleNumber(char c, std::istream& in, char friendlyDecimalPoint)
{
  std::string numberStr;
  numberStr.push_back(c);
  size_t count = 0;

  while (std::isdigit(in.peek()) || in.peek() == friendlyDecimalPoint)
  {
    const char nextCh = static_cast<char>(in.get());
    if (nextCh == friendlyDecimalPoint)
    {
      count++;
      if (count >= 2)
      {
        throw std::invalid_argument("Too many delimiters in number");
      }
    }
    numberStr.push_back(nextCh);
  }

  if (numberStr.length() > MAX_WORD_SIZE)
  {
    throw std::invalid_argument("Max number length exceeded");
  }

  return {Token{numberStr, Type::number}};
}

Token algorithms::handleDash(char c, std::istream& in)
{
  std::string dashStr;
  dashStr.push_back(c);

  while (in.peek() == '-')
  {
    dashStr.push_back(static_cast<char>(in.get()));
  }

  if (dashStr.length() != DASH_SIZE)
  {
    throw std::invalid_argument("Dash must be like ---");
  }

  return {Token{dashStr, Type::dash}};
}
