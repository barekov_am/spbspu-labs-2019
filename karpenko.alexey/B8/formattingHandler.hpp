#ifndef FORMATTING_HPP
#define FORMATTING_HPP

#include <vector>
#include <stdexcept>
#include <ios>
#include <ostream>

#include "token.hpp"

class FormattingHandler
{
public:
  FormattingHandler(std::vector<Token>, std::ostream& output, size_t width);

  void validateText();
  void putNewToken(const std::string& stringWeWant);
  void putSpace();
  void execute();
  void wrapPunctuation(size_t i);
  void checkLastLine();
  void formatAndSend();

private:
  std::vector<Token> tokenVector_;
  std::ostream& out_;
  size_t lineWidth_;
  std::string line_;
  size_t prevLength_;
  size_t prevPos_;
};

#endif
