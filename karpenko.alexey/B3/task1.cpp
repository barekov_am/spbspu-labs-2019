#include <sstream>
#include <iostream>
#include "tasks.hpp"
#include "interface.hpp"
#include "phoneBook.hpp"

int karpenko::task1(){
  PhoneBook book;
  const UserInterface bookInter(book, std::cout);

  std::string line;
  while (std::getline(std::cin, line)){
    std::stringstream command(line);

    if (std::cin.bad()){
      return 2;
    }

    if (std::cin.eof()){
      break;
    }
    bookInter.chooseAction(command);
  }

  return 0;
}
