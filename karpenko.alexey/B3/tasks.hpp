#ifndef TASKS_HPP
#define TASKS_HPP

namespace karpenko {
  int task1();
  int task2();
}

#endif
