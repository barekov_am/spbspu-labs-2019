#include "shape.hpp"

Shape::Shape(const Point &point):
  center_(point)
{}

bool Shape::isUpper(constShapePtr &rhs) const
{
  return (center_.y > rhs->center_.y);
}

bool Shape::isMoreLeft(constShapePtr &rhs) const
{
  return (center_.x < rhs->center_.x);
}

void Shape::draw(std::ostream &out)
{
  out << center_;
}
