#include <functional>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <cmath>

void task1()
{
  auto functor = std::bind(std::multiplies<double>(), M_PI, std::placeholders::_1);

  std::transform(std::istream_iterator<double>(std::cin), std::istream_iterator<double>(),
    std::ostream_iterator<double>(std::cout, " "), functor);

  if (!std::cin.eof())
  {
    throw std::ios_base::failure("Trouble with std::cin\n");
  }
  std::cout << "\n";
}
