#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include <iosfwd>
#include "shape.hpp"

constexpr const char * TRIANGLE_TYPE = "TRIANGLE";

class Triangle: public Shape
{
public:
  static constexpr const char * name = TRIANGLE_TYPE;
  Triangle(const Point& point);

  void draw(std::ostream &out) override;
};

#endif
