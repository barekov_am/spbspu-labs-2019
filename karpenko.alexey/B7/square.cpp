#include "square.hpp"
#include <ostream>

Square::Square(const Point &point):
  Shape(point)
{}

void Square::draw(std::ostream &out)
{
  out << name;
  Shape::draw(out);
}
