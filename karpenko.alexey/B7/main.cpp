#include <iostream>
#include <string>

void task1();
void task2();

int main(int argc, char* argv[])
{
  if (argc == 1)
  {
    std::cerr << "Trouble with count of parameters\n";
    return 1;
  }

  try
  {
    std::locale loc;
    std::cin.imbue(loc);
    std::locale::global(loc);
    std::cout.imbue(loc);

    const size_t numTask = std::stoi(argv[1]);
    switch (numTask)
    {
    case 1:
      task1();
      break;

    case 2:
      task2();
      break;

    default:
      std::cerr << "Trouble with number of part\n";
      return 1;
    }
  }
  catch (std::exception & exception)
  {
    std::cerr << exception.what() << '\n';
    return 1;
  }

  return 0;
}
