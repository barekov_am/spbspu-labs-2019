#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include <iosfwd>
#include "shape.hpp"

constexpr const char * CIRCLE_TYPE = "CIRCLE";

class Circle: public Shape
{
public:
  static constexpr const char * name = CIRCLE_TYPE;
  Circle(const Point &point);

  void draw(std::ostream &out) override;
};

#endif
