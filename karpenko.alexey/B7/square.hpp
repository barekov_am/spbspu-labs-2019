#ifndef SQUARE_HPP
#define SQUARE_HPP

#include <iosfwd>
#include "shape.hpp"

constexpr const char * SQUARE_TYPE = "SQUARE";

class Square: public Shape
{
public:
  static constexpr const char * name = SQUARE_TYPE;
  Square(const Point &point);

  void draw(std::ostream &out) override;
};

#endif
