#include "triangle.hpp"
#include <ostream>

Triangle::Triangle(const Point & point):
  Shape(point)
{}

void Triangle::draw(std::ostream &out)
{
  out << name;
  Shape::draw(out);
}
