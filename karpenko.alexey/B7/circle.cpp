#include "circle.hpp"
#include <ostream>

Circle::Circle(const Point &point):
  Shape(point)
{}

void Circle::draw(std::ostream &out)
{
  out << name;
  Shape::draw(out);
}
