#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "point.hpp"

class Shape
{
public:
  using shapePtr = std::shared_ptr<Shape>;
  using constShapePtr = const std::shared_ptr<const Shape>;

  Shape(const Point &point);
  bool isUpper(constShapePtr &rhs) const;
  bool isMoreLeft(constShapePtr &rhs) const;

  virtual void draw(std::ostream &out) = 0 ;

protected:
  Point center_;
};

#endif
