#include <map>
#include <functional>
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <string>

#include "circle.hpp"
#include "triangle.hpp"
#include "square.hpp"
#include "manipulator.hpp"

std::istream& operator>>(std::istream &in, Shape::shapePtr &shapePtr)
{
  const std::istream::sentry sentry(in >> std::ws);

  if (!sentry)
  {
    return in;
  }

  in >> std::noskipws;
  std::string typeName;
  std::map<std::string, std::function<void(Shape::shapePtr &shape, Point &point)>> map;
  map.emplace(CIRCLE_TYPE, [](Shape::shapePtr &shape, Point &point){shape = std::make_shared<Circle>(point);});
  map.emplace(TRIANGLE_TYPE, [](Shape::shapePtr &shape, Point &point){shape = std::make_shared<Triangle>(point);});
  map.emplace(SQUARE_TYPE, [](Shape::shapePtr &shape, Point &point){shape = std::make_shared<Square>(point);});

  in >> std::ws;
  while (std::isalpha(in.peek()))
  {
    typeName += in.get();
  }

  const auto iter = map.find(typeName);
  if (iter == map.end())
  {
    in.setstate(std::ios_base::failbit);
    return in;
  }

  Point point;
  in >> point;

  iter->second(shapePtr, point);

  return in;
}

std::ostream& operator<<(std::ostream &out, const Shape::shapePtr &shapePtr)
{
  shapePtr->draw(out);
  return out;
}

void task2()
{
  std::vector<Shape::shapePtr> vec{std::istream_iterator<Shape::shapePtr>(std::cin),
      std::istream_iterator<Shape::shapePtr>()};

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Reading error occured");
  }

  if (vec.empty())
  {
    return;
  }

  std::function<bool(Shape::shapePtr & ptr1, Shape::shapePtr & ptr2)> comparator;
  comparator = [](Shape::shapePtr& ptr, Shape::shapePtr& ptr2)->bool {return ptr->isMoreLeft(ptr2);};

  std::cout << "Original:\n";
  std::copy(vec.begin(), vec.end(), std::ostream_iterator<Shape::shapePtr>(std::cout, "\n"));

  std::sort(vec.begin(), vec.end(), comparator);
  std::cout << "Left-Right:\n";
  std::copy(vec.begin(), vec.end(), std::ostream_iterator<Shape::shapePtr>(std::cout, "\n"));

  std::sort(vec.begin(), vec.end(), std::bind(comparator, std::placeholders::_2, std::placeholders::_1));
  std::cout << "Right-Left:\n";
  std::copy(vec.begin(), vec.end(), std::ostream_iterator<Shape::shapePtr>(std::cout, "\n"));

  comparator = [](Shape::shapePtr& ptr, Shape::shapePtr& ptr1)->bool {return ptr->isUpper(ptr1);};

  std::sort(vec.begin(), vec.end(), comparator);
  std::cout << "Top-Bottom:\n";
  std::copy(vec.begin(), vec.end(), std::ostream_iterator<Shape::shapePtr>(std::cout, "\n"));

  std::sort(vec.begin(), vec.end(), std::bind(comparator, std::placeholders::_2, std::placeholders::_1));
  std::cout << "Bottom-Top:\n";
  std::copy(vec.begin(), vec.end(), std::ostream_iterator<Shape::shapePtr>(std::cout, "\n"));
}
