#include <stdexcept>
#include <vector>
#include <random>
#include <ctime>

#include "sorting.hpp"

void fillRandom(double* array, int size)
{
  std::knuth_b seed(time(0));
  std::uniform_real_distribution<double> randomNumber(-1.0, 1.0);

  for (auto i = 0; i < size; ++i)
  {
    array[i] = randomNumber(seed);
  }
}

void task4(const char* direction, int size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Vector is empty");
  }

  std::vector<double> vector(size);
  fillRandom(&vector[0], size); // vector.data() can be used

  print(vector);
  sort<AtAccess>(vector, direction);
  print(vector);
}


