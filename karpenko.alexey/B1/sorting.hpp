#ifndef SORTING_HPP
#define SORTING_HPP

#include <iostream>
#include <functional>
#include <stdexcept>
#include <cstring>

#include "access-methods.hpp"

template <template <typename> class Access, typename T>
void sort(T& collection, const char* direction)
{
  using valueType = typename T::value_type;

  const auto begin = Access<T>::begin(collection);
  const auto end = Access<T>::end(collection);


  std::function <bool(valueType, valueType)> compare;
  if (strcmp(direction, "ascending") == 0)
  {
    compare = std::function<bool(valueType, valueType)>(std::greater<valueType>()); 
  }
  else if (strcmp(direction, "descending") == 0)
  {
    compare = std::function<bool(valueType, valueType)>(std::less<valueType>()); 
  }
  else
  {
    throw std::invalid_argument("Invalid direction parameter");
  }


  for(auto i = begin; i != end; ++i)
  {
    auto tmp = i;
    for (auto j = i; j != end; ++j)
    {
      if (compare(Access<T>::getElement(collection, tmp), Access<T>::getElement(collection, j)))
      {
        tmp = j;
      }
    }

    if (tmp != i)
    {
      std::swap(Access<T>::getElement(collection, i), Access<T>::getElement(collection, tmp));
    }
  }
}


template<class Collection>
void print(const Collection& border)
{
  for (const auto& element: border)
  {
    std::cout << element << " ";
  }
  std::cout << '\n';
}

#endif

