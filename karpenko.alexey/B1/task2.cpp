#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include <stdexcept>

#include "sorting.hpp"


const size_t InitialSize = 1024;

void task2(const char* filename)
{
  std::ifstream input(filename);
  if(!input)
  {
    throw std::ios_base::failure("Reading error occured");
  }

  size_t size = InitialSize;

  std::unique_ptr<char[], decltype(&free)> arr(static_cast<char*>(malloc(size)), &free);

  size_t i = 0;
  while(input)
  {
    input.read(&arr[i], InitialSize);
    i += input.gcount();

    if (input.gcount() == InitialSize)
    {
      size += InitialSize;
      std::unique_ptr<char[], decltype(&free)> tmp(static_cast<char*>(realloc(arr.get(), size)), &free);
    
      if (tmp)
      {
        arr.release();
        std::swap(arr, tmp);
      }
      else
      {
        throw std::runtime_error("Reallocate error occured");
      }
    }
  }
  input.close();

  std::vector<char> vector(&arr[0], &arr[i]);
  for (auto iter: vector)
  {
    std::cout << iter;
  }
}

