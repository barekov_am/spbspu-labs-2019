#include <stdexcept>
#include <iostream>
#include <vector>

#include "sorting.hpp"

void task3()
{
  std::vector<int> vector;
  int data = -1;

  while (std::cin >> data)
  {
    if (data == 0)
    {
      break;
    }
    vector.push_back(data);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Read error occured");
  }

  if(vector.empty())
  {
    return;
  }

  if (data != 0)
  {
    throw std::invalid_argument("Invalid input data");
  }

  switch (vector.back())
  {
    case  1:
      {
        auto iter = vector.begin();
        while(iter != vector.end())
        {
          iter = ((*iter) % 2 == 0) ? vector.erase(iter) : ++iter;
        }
        break;
      }
    case 2:
      {
        auto iter = vector.begin();
        while (iter != vector.end())
        {
          iter = ((*iter) % 3 == 0) ? (vector.insert(++iter, 3, 1) + 3): ++iter;
        }
        break;
      }
    default:
      {
        break;
      }
  }

  print(vector);
}

