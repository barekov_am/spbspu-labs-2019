#include <iostream>
#include <stdexcept>
#include <vector>
#include <forward_list>

#include "sorting.hpp"

void task1(const char* direction)
{
  std::vector<int> vectorBracket;
  
  int tmp = 0;
  while (std::cin >> tmp)
  {
    vectorBracket.push_back(tmp);
  }

 if (!std::cin.eof() && std::cin.fail())
 {
  throw std::runtime_error("Read error occured");
 }

 std::vector<int> vectorAt = vectorBracket;
 std::forward_list<int> list(vectorAt.begin(), vectorAt.end());


 sort<BracketAccess>(vectorBracket, direction);
 print(vectorBracket);

 sort<AtAccess>(vectorAt, direction);
 print(vectorAt);

 sort<IteratorAccess>(list, direction);
 print(list);
}


