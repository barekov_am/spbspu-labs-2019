#include <iostream>
#include <string>

void task1(const char* direction);
void task2(const char* filename);
void task3();
void task4(const char* direction, int size);

int main(int argc, char* argv[])
{
  if (argc == 1)
  {
    std::cerr << "Wrong number of arguments1";
    return 1;
  }
  try
  {
    const int taskNum = atoi(argv[1]);

    switch (taskNum)
    {
    case 1:
      if (argc != 3)
      {
        std::cerr << "Wrong number of arguments";
        return 1;
      }

      task1(argv[2]);
      break;

    case 2:
      if (argc != 3)
      {
        std::cerr << "Wrong number of arguments";
        return 1;
      }
      
      task2(argv[2]);
      break;

    case 3:

      task3();
      break;

    case 4:
      if (argc != 4)
      {
        std::cerr << "Wrong number of arguments";
        return 1;
      }

      task4(argv[2], std::stoi(argv[3]));
      break;

    default:
      std::cerr << "Incorrect argument.\n";
      return 1;
    }
  }

  catch (const std::exception& exception)
  {
    std::cerr << exception.what();
    return 1;
  }  
 
  return 0;
}
