#ifndef ACCESS_METHODS
#define ACCESS_METHODS

#include <iterator>

template <typename T>
struct BracketAccess
{
  typedef size_t index;

  static index begin(const T&)
  {
    return 0;
  }

  static index end(const T& collection)
  {
    return collection.size();
  }

  static typename T::reference getElement(T& collection, index i)
  {
    return collection[i];
  }

  static index next(index i)
  {
    return i + 1;
  }
};


template <typename T>
struct AtAccess
{
  typedef size_t index;

  static index begin(const T&)
  {
    return 0;
  }

  static index end(const T& collection)
  {
    return collection.size();
  }

  static typename T::reference getElement(T& collection, index i)
  {
    return collection.at(i);
  }

  static index next(index i)
  {
    return i + 1;
  }
};


template <typename T>
struct IteratorAccess
{
  typedef typename T::iterator iterator;

  static iterator begin(T& collection)
  {
    return collection.begin();
  }

  static iterator end(T& collection)
  {
    return collection.end();
  }

  static typename T::reference getElement(T&, iterator& iter)
  {
    return *iter;
  }

  static iterator next(iterator& iter)
  {
    return ++iter;
  }
};

#endif

