#include "partition.hpp"

uzhegov::Matrix<uzhegov::Shape> uzhegov::part(const CompositeShape & compositeShape)
{
  uzhegov::Matrix<uzhegov::Shape> matrix;

  std::size_t count = compositeShape.getCount();
  for (std::size_t i = 0; i < count; ++ i)
  {
    std::size_t layer = 0;
    std::size_t matrixRows = matrix.getRows();
    for (std::size_t j = matrixRows; j-- > 0;)
    {
      std::size_t matrixColumns = matrix[j].size();
      for (std::size_t k = 0; k < matrixColumns; ++k)
      {
        if (intersect(matrix[j][k]->getFrameRect(), compositeShape[i]->getFrameRect()))
        {
          layer = j + 1;
          break;
        }
      }

      if (layer > j)
      {
        break;
      }
    }

    matrix.add(compositeShape[i], layer);
  }

  return matrix;
}

bool uzhegov::intersect(const rectangle_t & lftRect, const rectangle_t & rhtRect)
{
  const uzhegov::point_t lftRectLftBottom = {lftRect.pos.x - lftRect.width / 2, lftRect.pos.y - lftRect.height / 2};
  const uzhegov::point_t lftRectRhtTop = {lftRect.pos.x + lftRect.width / 2, lftRect.pos.y + lftRect.height / 2};

  const uzhegov::point_t rhtRectLftBottom = {rhtRect.pos.x - rhtRect.width / 2, rhtRect.pos.y - rhtRect.height / 2};
  const uzhegov::point_t rhtRectRhtTop = {rhtRect.pos.x + rhtRect.width / 2, rhtRect.pos.y + rhtRect.height / 2};

  const bool firstCondition = (rhtRectLftBottom.y < lftRectRhtTop.y) && (rhtRectLftBottom.x < lftRectRhtTop.x);
  const bool secondCondition = (rhtRectRhtTop.y > lftRectLftBottom.y) && (rhtRectRhtTop.x > lftRectLftBottom.x);

  return firstCondition && secondCondition;
}
