#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace uzhegov
{
  class Shape
  {
  public:
    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void printParams() const = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void move(const point_t &point) = 0;
    virtual void scale(double coefficient) = 0;
    virtual void rotate(double) = 0;
  };

  using shape_ptr = std::shared_ptr<Shape>;
  using shapes_array = std::unique_ptr<shape_ptr []>;

}
#endif
