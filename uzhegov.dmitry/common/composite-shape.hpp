#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include "shape.hpp"
#include <memory>
#include <stddef.h>

namespace uzhegov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape(const CompositeShape &copyCompositeShape);
    CompositeShape(CompositeShape &&movedCompositeShape);
    CompositeShape(const shape_ptr &shapeArray_);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &copyCompositeShape);
    CompositeShape &operator =(CompositeShape &&movedCompositeShape);
    shape_ptr operator [](size_t) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printParams() const override;
    void move(const point_t &point) override;
    void move(double dx, double dy) override;
    void scale(double coefficient) override;
    void rotate(double angle) override;
    int getCount() const;
    void add(const shape_ptr &shape);
    void remove(size_t index);

  private:
    shapes_array shapeArray_;
    size_t count_;
  };
}

#endif
