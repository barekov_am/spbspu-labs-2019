#include "composite-shape.hpp"
#include <memory>
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <cmath>

uzhegov::CompositeShape::CompositeShape(const uzhegov::CompositeShape &copyCompositeShape) :
  shapeArray_(std::make_unique<shape_ptr []>(copyCompositeShape.count_)),
  count_(copyCompositeShape.count_)
{
  for (size_t i = 0; i < count_; ++i)
  {
    shapeArray_[i] = copyCompositeShape.shapeArray_[i];
  }
}

uzhegov::CompositeShape::CompositeShape(uzhegov::CompositeShape &&movedCompositeShape) :
  shapeArray_(std::move(movedCompositeShape.shapeArray_)),
  count_(movedCompositeShape.count_)
{
  movedCompositeShape.shapeArray_ = nullptr;
  movedCompositeShape.count_ = 0;
}

uzhegov::CompositeShape::CompositeShape(const shape_ptr &shape) :
  shapeArray_(std::make_unique<shape_ptr []>(1)),
  count_(1)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer shouldn't be null");
  }
  shapeArray_[0] = shape;
}

uzhegov::CompositeShape &uzhegov::CompositeShape::operator =(const uzhegov::CompositeShape &copyCompositeShape)
{
  if (this != &copyCompositeShape)
  {
    count_ = copyCompositeShape.count_;
    shapeArray_ = std::make_unique<shape_ptr []>(count_);
    for (size_t i = 0; i < count_; ++i)
    {
      shapeArray_[i] = copyCompositeShape.shapeArray_[i];
    }
  }
  
  return *this;
}

uzhegov::CompositeShape &uzhegov::CompositeShape::operator =(uzhegov::CompositeShape &&movedCompositeShape)
{
  if (this != &movedCompositeShape)
  {
    count_ = movedCompositeShape.count_;
    movedCompositeShape.count_ = 0;
    shapeArray_ = std::move(movedCompositeShape.shapeArray_);
  }

  return *this;
}

uzhegov::shape_ptr uzhegov::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }

  return shapeArray_[index];
}

double uzhegov::CompositeShape::getArea() const
{
  double fullArea_ = 0;
  for (size_t i = 0; i < count_; ++i)
  {
    fullArea_ += shapeArray_[i]->getArea();
  }

  return fullArea_;
}

uzhegov::rectangle_t uzhegov::CompositeShape::getFrameRect() const
{
  uzhegov::rectangle_t currRectangle = shapeArray_[0]->getFrameRect();
  double max_X = currRectangle.pos.x + currRectangle.width / 2.0;
  double min_X = currRectangle.pos.x - currRectangle.width / 2.0;
  double max_Y = currRectangle.pos.y + currRectangle.height / 2.0;
  double min_Y = currRectangle.pos.y - currRectangle.height / 2.0;

  for (size_t i = 1; i < count_; ++i)
  {
    currRectangle = shapeArray_[i]->getFrameRect();
    max_X = std::max(currRectangle.pos.x + currRectangle.width / 2.0, max_X);
    min_X = std::min(currRectangle.pos.x - currRectangle.width / 2.0, min_X);
    max_Y = std::max(currRectangle.pos.y + currRectangle.height / 2.0, max_Y);
    min_Y = std::min(currRectangle.pos.y - currRectangle.height / 2.0, min_Y);
  }

  return {{(max_X + min_X) / 2.0, (max_Y + min_Y) / 2.0}, (max_X - min_X), (max_Y - min_Y)};
}

int uzhegov::CompositeShape::getCount() const
{
  return count_;
}

void uzhegov::CompositeShape::printParams() const
{
  const uzhegov::rectangle_t frameRect = getFrameRect();
  std::cout << "-Coordination of the center: (" << frameRect.pos.x << "," << frameRect.pos.y << ")" << "\n";
  std::cout << "-Count of shapes: " << count_ << "\n";
  std::cout << "-Full area of composite shape: " << getArea() << "\n";
  std::cout << "-Parameters of FrameRect:\n";
  std::cout << "--width: " << frameRect.width << "\n";
  std::cout << "--height: " << frameRect.height << "\n";
}

void uzhegov::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < count_; ++i)
  {
    shapeArray_[i]->move(dx, dy);
  }
}

void uzhegov::CompositeShape::move(const uzhegov::point_t &point)
{
  const uzhegov::rectangle_t frameRect = getFrameRect();
  double dx = point.x - frameRect.pos.x;
  double dy = point.y - frameRect.pos.y;
  move(dx, dy);
}

void uzhegov::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient should be positive");
  }

  const uzhegov::point_t posFrameRect = getFrameRect().pos;
  for (size_t i = 0; i < count_; ++i)
  {
    uzhegov::point_t shapeCenter = shapeArray_[i]->getFrameRect().pos;
    shapeArray_[i]->scale(coefficient);
    double dx = (shapeCenter.x - posFrameRect.x) * (coefficient - 1);
    double dy = (shapeCenter.y - posFrameRect.y) * (coefficient - 1);
    shapeArray_[i]->move(dx, dy);
  }
}

void uzhegov::CompositeShape::rotate(double angle)
{
  const uzhegov::point_t center = getFrameRect().pos;
  const double cosine = std::abs(std::cos(angle * M_PI / 180));
  const double sinus = std::abs(std::sin(angle * M_PI / 180));

  for (std::size_t i = 0; i < count_; ++i)
  {
    const uzhegov::point_t currCenter = shapeArray_[i]->getFrameRect().pos;
    const double projection_x = currCenter.x - center.x;
    const double projection_y = currCenter.y - center.y;
    const double shift_x = projection_x * (cosine - 1) - projection_y * sinus;
    const double shift_y = projection_x * sinus + projection_y * (cosine - 1);
    shapeArray_[i]->move(shift_x, shift_y);
    shapeArray_[i]->rotate(angle);
  }
}

void uzhegov::CompositeShape::add(const std::shared_ptr<uzhegov::Shape> &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Adding shape shouldn't be nullptr");
  }

  shapes_array newShapeArray(std::make_unique<std::shared_ptr<uzhegov::Shape> []>(count_ + 1));
  for (size_t i = 0; i < count_; ++i)
  {
    newShapeArray[i] = shapeArray_[i];
  }

  newShapeArray[count_] = shape;
  count_++;
  shapeArray_.swap(newShapeArray);
}

void uzhegov::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }
  if (count_ == 1)
  {
    throw std::invalid_argument("You can't delete single shape");
  }

  count_--;
  for (size_t i = index; i < count_; ++i)
  {
    shapeArray_[i] = shapeArray_[i + 1];
  }
  shapeArray_[count_] = nullptr;
}
