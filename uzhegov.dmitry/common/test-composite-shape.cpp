#include <boost/test/auto_unit_test.hpp>

#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double DIFF = 0.0001;

BOOST_AUTO_TEST_SUITE(compositeShapeMethodsTesting)

BOOST_AUTO_TEST_CASE(permanenceParamsOfCompositeShapeAfterMoving)
{
  uzhegov::Circle testCircle({5.5, 5.1}, 1.3);
  uzhegov::Rectangle testRectangle({0.0, 0.0}, 1.0, 2.0, 0.0);
  uzhegov::CompositeShape testComposite(std::make_shared<uzhegov::Rectangle>(testRectangle));
  testComposite.add(std::make_shared<uzhegov::Circle>(testCircle));

  const double areaBeforeMoving = testComposite.getArea();
  const uzhegov::rectangle_t frameRectBeforeMoving = testComposite.getFrameRect();
  testComposite.move(5.0, 3.0);

  BOOST_CHECK_CLOSE(testComposite.getArea(), areaBeforeMoving, DIFF);
  uzhegov::rectangle_t frameRectAfterMoving = testComposite.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.height, frameRectAfterMoving.height, DIFF);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.width, frameRectAfterMoving.width, DIFF);
}

BOOST_AUTO_TEST_CASE(permanenceParamsOfCompositeShapeAfterMovingTo)
{
  uzhegov::Circle testCircle({4.5, 7.1}, 2.2);
  uzhegov::Rectangle testRectangle({1.1, 2.2}, 3.9, 6.2, 233.0);
  uzhegov::CompositeShape testComposite(std::make_shared<uzhegov::Rectangle>(testRectangle));
  testComposite.add(std::make_shared<uzhegov::Circle>(testCircle));

  const double areaBeforeMoving = testComposite.getArea();
  const uzhegov::rectangle_t frameRectBeforeMoving = testComposite.getFrameRect();
  testComposite.move({-0.3, 1.1});

  BOOST_CHECK_CLOSE(testComposite.getArea(), areaBeforeMoving, DIFF);
  uzhegov::rectangle_t frameRectAfterMoving = testComposite.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.height, frameRectAfterMoving.height, DIFF);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.width, frameRectAfterMoving.width, DIFF);
}

BOOST_AUTO_TEST_CASE(compositeTestScale)
{
  uzhegov::Circle testCircle({3.5, 5.1}, 4.2);
  uzhegov::Rectangle testRectangle({1.2, 2.0}, 1.5, 1.8, 231.23);
  uzhegov::CompositeShape testComposite(std::make_shared<uzhegov::Rectangle>(testRectangle));
  testComposite.add(std::make_shared<uzhegov::Circle>(testCircle));

  const double areaBeforeMoving = testComposite.getArea();
  const double testScale = 2.0;

  testComposite.scale(testScale);
  BOOST_CHECK_CLOSE(areaBeforeMoving * testScale * testScale, testComposite.getArea(), DIFF);
}

BOOST_AUTO_TEST_CASE(compositeTestChangingAreaAfterAddAndDelete)
{
  uzhegov::Circle testCircle({-5.1, 3.4}, 2.0);
  uzhegov::Rectangle testRectangle({2.2, 5.5}, 3.5, 7.8, 22.0);
  uzhegov::Rectangle testRectangle2({-8.1, -3.2}, 5.6, 7.8, 0.0);
  uzhegov::CompositeShape testComposite(std::make_shared<uzhegov::Rectangle>(testRectangle));
  const double compositeAreaBeforeAdd = testComposite.getArea();
  const int countOfShapesBeforeAdd = testComposite.getCount();
  const int testIndex = 2;
  const double circleArea = testCircle.getArea();
  const double rect2Area = testRectangle2.getArea();

  testComposite.add(std::make_shared<uzhegov::Circle>(testCircle));
  testComposite.add(std::make_shared<uzhegov::Rectangle>(testRectangle2));
  double compositeAreaAfterAdd = testComposite.getArea();
  const int countOfShapesAfterAdd = testComposite.getCount();  
  BOOST_CHECK_CLOSE(compositeAreaBeforeAdd + circleArea + rect2Area, compositeAreaAfterAdd, DIFF);
  BOOST_CHECK_EQUAL(countOfShapesBeforeAdd + 2, countOfShapesAfterAdd);

  const double areaOfDeletingShape = testComposite[testIndex]->getArea();
  testComposite.remove(testIndex);
  BOOST_CHECK_CLOSE(compositeAreaAfterAdd - areaOfDeletingShape, testComposite.getArea(), DIFF);
  BOOST_CHECK_EQUAL(countOfShapesAfterAdd - 1, testComposite.getCount());
}

BOOST_AUTO_TEST_CASE(catchingExceptionAfterScale)
{
  uzhegov::Rectangle testRectangle({-7.8, 3.4}, 5.4, 7.9, 23.3);
  uzhegov::CompositeShape testComposite(std::make_shared<uzhegov::Rectangle>(testRectangle));
  BOOST_CHECK_THROW(testComposite.scale(-6.1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(catchingExceptionAfterDelete)
{
  uzhegov::Circle testCircle({3.5, 6.1}, 3.2);
  uzhegov::Rectangle testRectangle({2.2, 5.5}, 3.5, 7.8, 23.03);
  uzhegov::CompositeShape testComposite(std::make_shared<uzhegov::Rectangle>(testRectangle));
  BOOST_CHECK_THROW(testComposite.remove(0), std::invalid_argument);

  testComposite.add(std::make_shared<uzhegov::Circle>(testCircle));
  BOOST_CHECK_THROW(testComposite.remove(2), std::out_of_range);
  BOOST_CHECK_THROW(testComposite.remove(-3), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(catchingExceptionAfterUsingOperator)
{
  uzhegov::Circle testCircle({-4.1, 2.0}, 3.1);
  uzhegov::Rectangle testRectangle({2.3, 2.4}, 2.1, 1.8, 22.0);
  uzhegov::CompositeShape testComposite(std::make_shared<uzhegov::Rectangle>(testRectangle));
  testComposite.add(std::make_shared<uzhegov::Circle>(testCircle));

  BOOST_CHECK_THROW(testComposite[2], std::out_of_range);
  BOOST_CHECK_THROW(testComposite[-1], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(catchingExceptionNullPtr)
{
  BOOST_CHECK_THROW(uzhegov::CompositeShape(nullptr), std::invalid_argument);

  uzhegov::Rectangle testRectangle({-8.2, 4.2}, 5.1, 2.8, 21.3);
  uzhegov::CompositeShape testComposite(std::make_shared<uzhegov::Rectangle>(testRectangle));

  BOOST_CHECK_THROW(testComposite.add(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeTestCopyConstructor)
{
  uzhegov::Circle testCircle({2.3, 5.7}, 3.0);
  uzhegov::Rectangle testRectangle({0.2, 8.1}, 2.3, 2.8, 21.2);
  uzhegov::CompositeShape testComposite(std::make_shared<uzhegov::Rectangle>(testRectangle));
  testComposite.add(std::make_shared<uzhegov::Circle>(testCircle));
  const uzhegov::rectangle_t frameRect = testComposite.getFrameRect();
  uzhegov::CompositeShape copyComposite(testComposite);
  const uzhegov::rectangle_t copyFrameRect = copyComposite.getFrameRect();

  BOOST_CHECK_CLOSE(testComposite.getArea(), copyComposite.getArea(), DIFF);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, DIFF);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, DIFF);
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, DIFF);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, DIFF);
  BOOST_CHECK_EQUAL(testComposite.getCount(), copyComposite.getCount());
}

BOOST_AUTO_TEST_CASE(compositeTestMoveConstructor)
{
  uzhegov::Circle testCircle({1.2, 4.6}, 2.0);
  uzhegov::Rectangle testRectangle({0.0, 7.1}, 1.4, 0.8, 21.3);
  uzhegov::CompositeShape testComposite(std::make_shared<uzhegov::Rectangle>(testRectangle));
  testComposite.add(std::make_shared<uzhegov::Circle>(testCircle));
  const uzhegov::rectangle_t frameRect = testComposite.getFrameRect();
  const double compositeArea = testComposite.getArea();
  const int compositeCount = testComposite.getCount();
  uzhegov::CompositeShape moveComposite(std::move(testComposite));
  const uzhegov::rectangle_t moveFrameRect = moveComposite.getFrameRect();

  BOOST_CHECK_CLOSE(compositeArea, moveComposite.getArea(), DIFF);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, DIFF);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, DIFF);
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, DIFF);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, DIFF);
  BOOST_CHECK_EQUAL(compositeCount, moveComposite.getCount());
  BOOST_CHECK_EQUAL(testComposite.getCount(), 0);
  BOOST_CHECK_CLOSE(testComposite.getArea(), 0, DIFF);
}

BOOST_AUTO_TEST_CASE(compositeTestCopyOperator)
{
  uzhegov::Circle testCircle({2.3, 5.7}, 3.0);
  uzhegov::Rectangle testRectangle({0.2, 8.1}, 2.3, 2.8, 21.3);
  uzhegov::CompositeShape testComposite(std::make_shared<uzhegov::Rectangle>(testRectangle));
  testComposite.add(std::make_shared<uzhegov::Circle>(testCircle));
  const uzhegov::rectangle_t frameRect = testComposite.getFrameRect();

  uzhegov::Circle testCircle2({2.1, 3.2}, 5.6);
  uzhegov::CompositeShape copyComposite(std::make_shared<uzhegov::Circle>(testCircle2));
  copyComposite = testComposite;
  const uzhegov::rectangle_t copyFrameRect = copyComposite.getFrameRect();

  BOOST_CHECK_CLOSE(testComposite.getArea(), copyComposite.getArea(), DIFF);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, DIFF);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, DIFF);
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, DIFF);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, DIFF);
  BOOST_CHECK_EQUAL(testComposite.getCount(), copyComposite.getCount());
}

BOOST_AUTO_TEST_CASE(compositeTestMoveOperator)
{
  uzhegov::Circle testCircle({0.0, 3.1}, 2.0);
  uzhegov::Rectangle testRectangle({-5.9, 8.1}, 1.2, 1.4, 11.2);
  uzhegov::CompositeShape testComposite(std::make_shared<uzhegov::Rectangle>(testRectangle));
  testComposite.add(std::make_shared<uzhegov::Circle>(testCircle));
  const uzhegov::rectangle_t frameRect = testComposite.getFrameRect();
  const double compositeArea = testComposite.getArea();
  const int compositeCount = testComposite.getCount();

  uzhegov::Circle testCircle2({3.2, 4.1}, 5.3);
  uzhegov::CompositeShape moveComposite(std::make_shared<uzhegov::Circle>(testCircle2));
  moveComposite = std::move(testComposite);
  const uzhegov::rectangle_t moveFrameRect = moveComposite.getFrameRect();

  BOOST_CHECK_CLOSE(compositeArea, moveComposite.getArea(), DIFF);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, DIFF);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, DIFF);
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, DIFF);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, DIFF);  
  BOOST_CHECK_EQUAL(compositeCount, moveComposite.getCount());
  BOOST_CHECK_EQUAL(testComposite.getCount(), 0);
  BOOST_CHECK_CLOSE(testComposite.getArea(), 0, DIFF);

  moveComposite = std::move(moveComposite);
  BOOST_CHECK_CLOSE(compositeArea, moveComposite.getArea(), DIFF);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, DIFF);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, DIFF);
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, DIFF);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, DIFF);
  BOOST_CHECK_EQUAL(compositeCount, moveComposite.getCount());
}

BOOST_AUTO_TEST_SUITE_END()
