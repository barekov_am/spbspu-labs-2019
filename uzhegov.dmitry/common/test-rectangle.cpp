#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double DIFF = 0.0001;

BOOST_AUTO_TEST_SUITE(rectangleMethodsTest)

BOOST_AUTO_TEST_CASE(permanenceParamsOfRectangleAfterOffsetMoving)
{
  uzhegov::Rectangle testRectangle({1.0, 2.0}, 3.0, 4.0, 22.2);
  const double rectangleAreaBeforeMoving = testRectangle.getArea();
  const uzhegov::rectangle_t frameRectBeforeMoving = testRectangle.getFrameRect();
  testRectangle.move(5.0, 4.0);
  uzhegov::rectangle_t frameRectAfterMoving = testRectangle.getFrameRect();

  BOOST_CHECK_CLOSE(testRectangle.getArea(), rectangleAreaBeforeMoving, DIFF);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, DIFF);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, DIFF);
}

BOOST_AUTO_TEST_CASE(permanenceParamsOfRectangleAfterMovingToPoint)
{
  uzhegov::Rectangle testRectangle({0.0, 0.0}, 3.0, 4.0, 211.2);
  const double rectangleAreaBeforeMoving = testRectangle.getArea();
  const uzhegov::rectangle_t frameRectBeforeMoving = testRectangle.getFrameRect();
  testRectangle.move({0.0, 0.0});
  uzhegov::rectangle_t frameRectAfterMoving = testRectangle.getFrameRect();

  BOOST_CHECK_CLOSE(testRectangle.getArea(), rectangleAreaBeforeMoving, DIFF);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.width, frameRectBeforeMoving.width, DIFF);
  BOOST_CHECK_CLOSE(frameRectAfterMoving.height, frameRectBeforeMoving.height, DIFF);
}

BOOST_AUTO_TEST_CASE(increaseRectangleAreaAfterScaling)
{
  uzhegov::Rectangle testRectangle({0.1, 3.0}, 6.0, 3.0, 21.2);
  const double areaBefore = testRectangle.getArea();
  const double coefficient = 2.0;
  testRectangle.scale(coefficient);

  BOOST_CHECK_CLOSE(testRectangle.getArea(), coefficient * coefficient * areaBefore, DIFF);
}

BOOST_AUTO_TEST_CASE(catchingExceptionInRectagleParams)
{
  BOOST_CHECK_THROW(uzhegov::Rectangle({3.0, 2.0}, -1, 4, 222.0), std::invalid_argument);
  BOOST_CHECK_THROW(uzhegov::Rectangle({2.0, 1.1}, 4, -4, 0.2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(catchingExceptionAfterScale)
{
  uzhegov::Rectangle testRectangle({0.1, 2.0}, 1, 2, 22.2);
  BOOST_CHECK_THROW(testRectangle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleTestRotate)
{
  uzhegov::Rectangle testRectangle({5.0, 5.0}, 2.0, 5.0, 0.0);
  const double rectangleAreaBefore = testRectangle.getArea();
  const uzhegov::rectangle_t frameRectBeforeRotate = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, 5.0, DIFF);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, 2.0, DIFF);
  double angle = 90.0;
  testRectangle.rotate(angle);
  double rectangleAreaAfter = testRectangle.getArea();
  uzhegov::rectangle_t frameRectAfterRotate = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.width, DIFF);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.height, DIFF);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, DIFF);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, DIFF);
  BOOST_CHECK_CLOSE(rectangleAreaBefore, rectangleAreaAfter, DIFF);
}

BOOST_AUTO_TEST_SUITE_END()
