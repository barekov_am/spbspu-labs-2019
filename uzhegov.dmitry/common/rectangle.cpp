#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

uzhegov::Rectangle::Rectangle(const point_t &center, double width, double height) :
  center_(center),
  width_(width),
  height_(height),
  angle_(0)
{
  if (width <= 0)
  {
    throw std::invalid_argument("Width should be positive");
  }
  if (height <= 0)
  {
    throw std::invalid_argument("Height should be positive");
  }
}

uzhegov::Rectangle::Rectangle(const uzhegov::point_t &center, double width, double height, double angle) :
  Rectangle(center, width, height)
{
  rotate(angle);
}


double uzhegov::Rectangle::getArea() const
{
  return width_ * height_;
}

uzhegov::rectangle_t uzhegov::Rectangle::getFrameRect() const
{
  const double sine = std::sin(angle_ * M_PI / 180);
  const double cosine = std::cos(angle_ * M_PI / 180);
  const double height = width_ * std::abs(sine) + height_ * std::abs(cosine);
  const double width = width_ * std::abs(cosine) + height_ * std::abs(sine);
  return {center_, width, height};
}

void uzhegov::Rectangle::printParams() const
{
  std::cout << "-Coordination of the center (" << center_.x << ", " << center_.y << ")\n";
  std::cout << "-Width = " << width_ << ", Height = " << height_ << '\n';
  std::cout << "-Area = " << getArea() << '\n';
  uzhegov::rectangle_t frameRect = getFrameRect();
  std::cout << "-Parameters of FrameRect:\n";
  std::cout << "--Coordination of the center (" << frameRect.pos.x << ", " << frameRect.pos.y << ")\n";
  std::cout << "--Width = " << frameRect.width << ", Height = " << frameRect.height << '\n';
}

void uzhegov::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void uzhegov::Rectangle::move(const point_t &point)
{
  center_ = point;
}

void uzhegov::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient should be positive");
  }
  width_ *= coefficient;
  height_ *= coefficient;
}

void uzhegov::Rectangle::rotate(double angle)
{
  while (angle >= 360.0)
  {
    angle -= 360.0;
  }
  angle_ += angle;
}

