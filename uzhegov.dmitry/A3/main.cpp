#include <iostream>
#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

int main()
{
  uzhegov::Rectangle rect1({-7.2, -4.9}, 5.0, 3.0);
  uzhegov::Rectangle rect2({7.2, 3.6}, 4.9, 6.8);
  uzhegov::Circle circle1({2.0, 1.0}, 2.0);
  uzhegov::Circle circle2({5.3, 6.2}, 1.0);

  uzhegov::CompositeShape compositeShape(std::make_shared<uzhegov::Rectangle>(rect1));
  std::cout << "PARAMETERS OF THE COMPOSITE-SHAPE:\n";
  compositeShape.printParams();

  compositeShape.add(std::make_shared<uzhegov::Rectangle>(rect2));
  compositeShape.add(std::make_shared<uzhegov::Circle>(circle1));
  compositeShape.add(std::make_shared<uzhegov::Circle>(circle2));
  std::cout << "PARAMETERS OF THE COMPOSITE-SHAPE AFTER ADDED NEW SHAPES:\n";
  compositeShape.printParams();

  compositeShape.move(1, 4);
  std::cout << "PARAMETERS OF THE COMPOSITE-SHAPE AFTER MOVE dx = 9, dy = 4:\n";
  compositeShape.printParams();

  compositeShape.move({0, 1});
  std::cout << "PARAMETERS OF THE COMPOSITE-SHAPE AFTER MOVE TO (0, 1):\n";
  compositeShape.printParams();

  compositeShape.scale(3);
  std::cout << "PARAMETERS OF THE COMPOSITE-SHAPE AFTER TWOFOLD INCREASE:\n";
  compositeShape.printParams();

  std::cout << "PARAMETERS OF THE COMPOSITE-SHAPE AFTER REMOVE SECOND SHAPE:\n";
  compositeShape.remove(1);
  compositeShape.printParams();

  std::shared_ptr<uzhegov::Shape> shapeFromComposite = compositeShape[1];
  std::cout << "PARAMETERS OF THE SECOND SHAPE:\n";
  shapeFromComposite->printParams();

  return 0;
}
