#include <iostream>
#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "partition.hpp"


int main()
{
  uzhegov::Circle circle1({1.2, 1.2}, 3.0);
  uzhegov::Circle circle2({-0.2, 5.3}, 2.0);
  uzhegov::Rectangle rect1({1.6, 1.6}, 5.0, 3.0);
  uzhegov::Rectangle rect2({-7.3, 2.6}, 8.2, 5.3);

  std::cout << "Create composite shape:\n";
  uzhegov::CompositeShape compositeShape(std::make_shared<uzhegov::Circle>(circle1));

  compositeShape.printParams();

  std::cout << "Add other shapes in composite:\n";
  compositeShape.add(std::make_shared<uzhegov::Rectangle>(rect1));
  compositeShape.add(std::make_shared<uzhegov::Rectangle>(rect2));
  compositeShape.add(std::make_shared<uzhegov::Circle>(circle2));
  compositeShape.printParams();

  std::cout << "Shift at (3,3)\n";
  compositeShape.move(3, 3);
  compositeShape.printParams();

  std::cout << "Remove shapes which have index 2 and 3\n";
  compositeShape.remove(3);
  compositeShape.remove(2);
  compositeShape.printParams();

  std::cout << "Move to {-4,7}\n";
  compositeShape.move({-4, 7});
  compositeShape.printParams();

  std::cout << "Get shape which has index 0 and print information about it:\n";
  std::shared_ptr<uzhegov::Shape> shapeFromComposite = compositeShape[0];
  shapeFromComposite->printParams();

  std::cout << "Scale composite shape in 0,8 times\n\n";
  compositeShape.scale(0.8);
  compositeShape.printParams();

  std::cout << "Add new shape\n";
  compositeShape.add(std::make_shared<uzhegov::Circle>(circle2));
  compositeShape.printParams();

  std::cout << "Rotate composite shape by 30 degrees\n";
  compositeShape.rotate(30.0);
  compositeShape.printParams();

  std::cout << "Get partition of composite shape\n";
  uzhegov::Matrix<uzhegov::Shape> matrix = uzhegov::part(compositeShape);
  std::size_t layer = matrix.getRows();
  for (std::size_t i = 0; i < layer; ++i)
  {
    std::cout << "Layer: " << i + 1 << "\n";
    std::cout << "Shapes in the layer:\n";
    std::size_t columns = matrix[i].size();
    for (std::size_t j = 0; j < columns; ++j)
    {
      if (matrix[i][j] != nullptr)
      {
        std::cout << j + 1 << ":\n";
        std::cout << "Area: " << matrix[i][j]->getArea() << "\n";
        const uzhegov::rectangle_t frameRect = matrix[i][j]->getFrameRect();
        std::cout << "Position: (" << frameRect.pos.x << "," << frameRect.pos.y << ")\n";
      }
    }
  }

  return 0;
}
