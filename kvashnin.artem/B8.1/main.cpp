#include <iostream>
#include <cstring>
#include <algorithm>

#include "item-input-iterator.hpp"
#include "item-output-iterator.hpp"
#include "check-item.hpp"


int main(const int argc, char* argv[])
{
  try
  {
    std::size_t lineWidth = DEFAULT_LINE_WIDTH;
    if (argc > 1)
    {
      if ((argc != 3) || (strcmp(argv[1],  "--line-width") != 0))
      {
        std::cerr << "Input error" << std::endl;
        return 1;
      }
      char* end = nullptr;
      lineWidth = std::strtol(argv[2], &end, 10);
      if (lineWidth < MIN_LINE_WIDTH)
      {
        std::cerr << "Enter the correct line width" << std::endl;
        return 1;
      }
      if (*end != 0x00)
      {
        std::cerr << "Enter the correct argument" << std::endl;
        return 1;
      }
    }
    std::transform(ItemInputIterator(std::cin), {}, ItemOutputIterator(std::cout, lineWidth), check_item());
  }
  catch (const std::exception& except)
  {
    std::cerr << except.what() << std::endl;
    return 1;
  }
  return 0;
}
