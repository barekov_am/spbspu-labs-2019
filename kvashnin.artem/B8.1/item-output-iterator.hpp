#ifndef B8_ITEM_OUTPUT_ITERATOR_HPP
#define B8_ITEM_OUTPUT_ITERATOR_HPP
#include <iosfwd>
#include <iterator>
#include <memory>
#include "item.hpp"

class ItemOutputIterator: public std::iterator<std::output_iterator_tag, item_t>
{
public:
  ItemOutputIterator(std::ostream& out, std::size_t lineWidth);

  ItemOutputIterator& operator*();

  void operator=(const item_t& token);

  void operator++();
  void operator++(int);

private:
  class Impl;
  std::shared_ptr<Impl> impl;
};

#endif
