#ifndef B8_CHECK_ITEM_HPP
#define B8_CHECK_ITEM_HPP

#include "item.hpp"

class check_item
{
public:
  check_item();
  item_t operator()(const item_t& item);
private:
  item_t prevItem_;
  bool flagFirstItem_;

  bool isCorrect(const item_t& item);
};


#endif
