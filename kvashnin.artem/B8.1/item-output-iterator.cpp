#include "item-output-iterator.hpp"
#include <vector>
#include <ostream>

class ItemOutputIterator::Impl
{
public:
  Impl(std::ostream& out, std::size_t lineLength);
  ~Impl();

  void operator=(const item_t& token);

private:
  item_t prevItem_ = {item_t::Type::NUMBER};
  bool flagFirstItem_ = false;
  bool flagSpace_ = false;
  bool flagComma_ = false;
  std::ostream& out_;
  std::size_t lineWidth_;
  std::size_t currentWidth_ = 0;
  std::vector<item_t> line_;

  void printLine(const std::vector<item_t>& line) const;
  long remakeLine(std::vector<item_t>& line) const;
};

ItemOutputIterator::ItemOutputIterator(std::ostream& out, std::size_t lineWidth):
  impl(std::make_shared<Impl>(out, lineWidth))
{}

ItemOutputIterator& ItemOutputIterator::operator*()
{
  return *this;
}

void ItemOutputIterator::operator=(const item_t& token)
{
  *impl = token;
}
void ItemOutputIterator::operator++()
{}

void ItemOutputIterator::operator++(int)
{}

ItemOutputIterator::Impl::Impl(std::ostream& out, std::size_t lineLength):
  out_(out),
  lineWidth_(lineLength)
{}
ItemOutputIterator::Impl::~Impl()
{
  if (!out_)
  {
    return;
  }
  if (!line_.empty())
  {
    printLine(line_);
  }
}

void ItemOutputIterator::Impl::operator=(const item_t& item)
{
  if (item.type == item_t::Type::SPACE)
  {
    return;
  }
  switch (item.type)
  {
    case item_t::Type::PUNCT:
      if (currentWidth_ + SPACE_WIDTH > lineWidth_)
      {
        currentWidth_ = remakeLine(line_);
      }
      line_.push_back(item);
      currentWidth_ += item.value.size();
      break;
    case item_t::Type::DASH:
      if (currentWidth_ + DASH_WIDTH + SPACE_WIDTH > lineWidth_)
      {
        currentWidth_ = remakeLine(line_);
      }
      line_.push_back(item_t{item_t::Type::SPACE, " "});
      line_.push_back(item);
      currentWidth_ += item.value.size() + 1;
      break;
    case item_t::Type::DOTS:
      if (currentWidth_ + DOTS_WIDTH + SPACE_WIDTH > lineWidth_)
      {
        currentWidth_ = remakeLine(line_);
      }
      if (flagFirstItem_)
      {
        if ((prevItem_.value == ".") || (prevItem_.value == "!") || (prevItem_.value == "?"))
        {
          flagSpace_ = true;
        }
        if (prevItem_.value == ",")
        {
          currentWidth_ -= 1;
          flagComma_ = true;
          line_.pop_back();
        }
      }
      else
      {
        flagSpace_ = true;
      }
      if ((flagFirstItem_) && (!flagComma_))
      {
        line_.push_back(item_t{item_t::Type::SPACE, " "});
      }
      line_.push_back(item);
      currentWidth_ += item.value.size() + 1;
      break;
    case item_t::Type::NUMBER:
    case item_t::Type::WORD:
      if (currentWidth_ + item.value.size() + 1 > lineWidth_)
      {
        printLine(line_);
        line_.clear();
        currentWidth_ = 0;
      }
      else if ((!line_.empty()) && (!flagSpace_))
      {
        line_.push_back(item_t{item_t::Type::SPACE, " "});
        ++currentWidth_;
      }
      else if (flagSpace_)
      {
        flagSpace_ = false;
      }
      line_.push_back(item);
      currentWidth_ += item.value.size();
      break;
    case item_t::Type::SPACE:
      break;
  }
  flagFirstItem_ = true;
  prevItem_ = item;
}

void ItemOutputIterator::Impl::printLine(const std::vector<item_t>& line) const
{
  for (std::vector<item_t>::const_iterator iterator = line.cbegin(); iterator != line.cend(); iterator++)
  {
    out_ << iterator->value;
  }
  out_ << std::endl;
}

long ItemOutputIterator::Impl::remakeLine(std::vector<item_t>& line) const
{
  std::size_t lineWidth = 0;
  std::vector<item_t> tempLine;
  while (!line.empty())
  {
    tempLine.insert(tempLine.begin(), line.back());
    lineWidth += line.back().value.size();
    line.pop_back();
    if (tempLine.front().type == item_t::Type::WORD || tempLine.front().type == item_t::Type::NUMBER)
    {
      break;
    }
  }
  printLine(line);
  line = tempLine;
  return lineWidth;
}
