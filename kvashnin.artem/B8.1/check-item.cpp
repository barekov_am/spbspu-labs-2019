#include "check-item.hpp"
#include <stdexcept>

check_item::check_item():
  prevItem_({item_t::Type::SPACE}),
  flagFirstItem_(false)
{}

bool check_item::isCorrect(const item_t& item)
{
  if (item.type == item_t::Type::SPACE)
  {
    return true;
  }

  if (!flagFirstItem_)
  {
    if ((item.type != item_t::Type::WORD) && (item.type != item_t::Type::DOTS)
        && (item.type != item_t::Type::NUMBER))
    {
      return false;
    }
  }

  switch (item.type)
  {
    case item_t::Type::WORD:
      if (item.value.size() > MAX_WORD_WIDTH)
      {
        return false;
      }
      break;
    case item_t::Type::NUMBER:
      if (item.value.size() > MAX_WORD_WIDTH)
      {
        return false;
      }
      break;
    case item_t::Type::DASH:
      if (item.value.size() != DASH_WIDTH)
      {
        return false;
      }
      if (flagFirstItem_)
      {
        if ((prevItem_.type == item_t::Type::DASH)
            || ((prevItem_.type == item_t::Type::PUNCT) && (prevItem_.value != ",")))
        {
          return false;
        }
      }
      break;
    case item_t::Type::DOTS:
      if (item.value.size() != DOTS_WIDTH)
      {
        return false;
      }
      break;
    case item_t::Type::PUNCT:
      if (flagFirstItem_)
      {
        if ((prevItem_.type == item_t::Type::DASH) || (prevItem_.type == item_t::Type::PUNCT)
            || ((prevItem_.type == item_t::Type::DOTS) && (prevItem_.value != ",")))
        {
          return false;
        }
      }
      break;
    case item_t::Type::SPACE:
      break;
  }
  prevItem_ = item;
  flagFirstItem_ = true;
  return true;
}

item_t check_item::operator()(const item_t &item)
{
  if (!isCorrect(item))
  {
    throw std::runtime_error("Input error");
  }
  return item;
}
