#ifndef ITEM_HPP
#define ITEM_HPP

#include <string>

const std::size_t DEFAULT_LINE_WIDTH = 40u;
const std::size_t MAX_WORD_WIDTH = 20u;
const std::size_t DASH_WIDTH = 3u;
const std::size_t DOTS_WIDTH = 3u;
const std::size_t COMMA_WIDTH = 1u;
const std::size_t SPACE_WIDTH = 1u;
const std::size_t MIN_LINE_WIDTH = MAX_WORD_WIDTH + DASH_WIDTH + SPACE_WIDTH + COMMA_WIDTH;

struct item_t
{
  enum class Type
  {
    WORD,
    NUMBER,
    PUNCT,
    SPACE,
    DASH,
    DOTS
  };

  Type type;
  std::string value = "";
};
#endif
