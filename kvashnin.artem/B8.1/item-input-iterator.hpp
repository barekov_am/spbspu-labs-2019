#ifndef B8_ITEM_INPUT_ITERATOR_HPP
#define B8_ITEM_INPUT_ITERATOR_HPP

#include <iosfwd>
#include <locale>
#include <memory>
#include "item.hpp"

class ItemInputIterator: public std::iterator<std::input_iterator_tag, item_t>
{
public:
  ItemInputIterator();
  ItemInputIterator(std::istream& inp);

  const item_t& operator*() const;

  bool operator==(const ItemInputIterator& rhs) const noexcept;
  bool operator!=(const ItemInputIterator& rhs) const noexcept;

  ItemInputIterator& operator++();
  ItemInputIterator operator++(int);

private:
  class Impl;
  std::shared_ptr<Impl> impl;
};

#endif
