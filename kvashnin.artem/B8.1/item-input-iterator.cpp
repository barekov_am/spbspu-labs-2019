#include "item-input-iterator.hpp"
#include <istream>
#include <memory>

class ItemInputIterator::Impl
{
public:
  Impl();
  Impl(std::istream& inp);

  friend class ItemInputIterator;

private:
  std::istream* inp_;
  std::locale loc_;
  item_t curItem_;

  item_t readAsNumber(std::locale locale) const;
  item_t readAsWord(std::locale locale) const;
  item_t readAsDash() const;
  item_t readAsDots() const;
  item_t readAsPunct() const;
  item_t::Type getType(char ch) const;
};

ItemInputIterator::ItemInputIterator(std::istream& inp):
  impl(std::make_shared<Impl>(inp))
{
  (*this)++;
}

ItemInputIterator::ItemInputIterator():
  impl(std::make_shared<Impl>())
{}

ItemInputIterator::Impl::Impl(std::istream& inp):
  inp_(&inp),
  loc_(inp_->getloc())
{}

ItemInputIterator::Impl::Impl():
  inp_(nullptr)
{}

ItemInputIterator &ItemInputIterator::operator++()
{
  if (!impl->inp_)
  {
    impl->inp_ = nullptr;
  }
  if (impl->inp_->eof())
  {
    impl->inp_ = nullptr;
    return *this;
  }

  item_t::Type type = impl->getType(impl->inp_->get());
  switch (type)
  {
    case item_t::Type::WORD:
      impl->curItem_ = impl->readAsWord(impl->loc_);
      return *this;
    case item_t::Type::NUMBER:
      impl->curItem_ = impl->readAsNumber(impl->loc_);
      return *this;
    case item_t::Type::PUNCT:
      impl->curItem_ = impl->readAsPunct();
      return *this;
    case item_t::Type::DASH:
      impl->curItem_ = impl->readAsDash();
      return *this;
    case item_t::Type::DOTS:
      impl->curItem_ = impl->readAsDots();
      return *this;
    case item_t::Type::SPACE:
      break;
  }
  impl->curItem_ = {item_t::Type::SPACE};
  return *this;
}

item_t ItemInputIterator::Impl::readAsNumber(std::locale locale) const
{
  inp_->unget();
  item_t item{item_t::Type::NUMBER};
  bool flagPoint = false;
  char ch = inp_->get();
  if (ch == '+' || ch == '-')
  {
    item.value.push_back(ch);
  }
  else
  {
    inp_->unget();
  }

  while (std::use_facet<std::ctype<char>>(locale).is(std::ctype_base::digit, inp_->peek())
      || inp_->peek() == std::use_facet<std::numpunct<char>>(locale).decimal_point())
  {
    ch = inp_->get();
    if (ch == std::use_facet<std::numpunct<char>>(locale).decimal_point())
    {
      if (flagPoint)
      {
        inp_->unget();
        break;
      }
      flagPoint = true;
    }
    item.value.push_back(ch);
  }
  return item;
}

item_t ItemInputIterator::Impl::readAsWord(std::locale locale) const
{
  inp_->unget();
  item_t item{item_t::Type::WORD};

  while (std::use_facet<std::ctype<char>>(locale).is(std::ctype_base::alpha, inp_->peek())
      || inp_->peek() == '-')
  {
    const char ch = inp_->get();
    if (ch != '-')
    {
      item.value.push_back(ch);
      continue;
    }
    if (ch == '-' && inp_->peek() != '-')
    {
      item.value.push_back(ch);
    }
    else
    {
      inp_->unget();
      break;
    }
  }
  return item;
}

item_t ItemInputIterator::Impl::readAsDash() const
{
  inp_->unget();
  item_t item{item_t::Type::DASH};
  while (inp_->peek() == '-')
  {
    item.value.push_back(inp_->get());
  }
  return item;
}

item_t ItemInputIterator::Impl::readAsDots() const
{
  inp_->unget();
  item_t item{item_t::Type::DOTS};
  while (inp_->peek() == '.')
  {
    item.value.push_back(inp_->get());
  }
  return item;
}

item_t ItemInputIterator::Impl::readAsPunct() const
{
  inp_->unget();
  item_t item{item_t::Type::PUNCT};
  item.value.push_back(inp_->get());
  return item;
}

bool ItemInputIterator::operator==(const ItemInputIterator& rhs) const noexcept
{
  return (impl->inp_ == rhs.impl->inp_);
}

bool ItemInputIterator::operator!=(const ItemInputIterator& rhs) const noexcept
{
  return !(*this == rhs);
}

ItemInputIterator ItemInputIterator::operator++(int)
{
  ItemInputIterator tmp = *this;
  operator++();
  return tmp;
}

const item_t &ItemInputIterator::operator*() const
{
  if (!impl->inp_)
  {
    throw std::runtime_error("Istream incorrect");
  }
  return impl->curItem_;
}

item_t::Type ItemInputIterator::Impl::getType(char ch) const
{
  if (std::use_facet<std::ctype<char>>(loc_).is(std::ctype_base::alpha, ch))
  {
    return item_t::Type::WORD;
  }
  else if (ch == '-')
  {
    if (inp_->peek() == '-')
    {
      return item_t::Type::DASH;
    }
    else
    {
      return item_t::Type::NUMBER;
    }
  }
  else if (ch == '+')
  {
    return item_t::Type::NUMBER;
  }
  else if (ch == '.')
  {
    if (inp_->peek() == '.')
    {
      return item_t::Type::DOTS;
    }
    else
    {
      return item_t::Type::PUNCT;
    }
  }
  else if (std::use_facet<std::ctype<char>>(loc_).is(std::ctype_base::digit, ch))
  {
    return item_t::Type::NUMBER;
  }
  else if (std::use_facet<std::ctype<char>>(loc_).is(std::ctype_base::punct, ch))
  {
    return item_t::Type::PUNCT;
  }
  return item_t::Type::SPACE;
}
