#include "rectangle.hpp"
#include <iostream>
#include <cmath>

kvashnin::Rectangle::Rectangle(const point_t &position, const double width, const double height):
  rect_({position, width, height}),
  angle_(0)
{
  if ((width < 0) || (height < 0))
  {
    throw std::invalid_argument("width or height can not be < 0");
  }
}

double kvashnin::Rectangle::getArea() const
{
  return rect_.width * rect_.height;
}

kvashnin::rectangle_t kvashnin::Rectangle::getFrameRect() const
{
  const double absSin = fabs(sin(angle_ * M_PI / 180));
  const double absCos = fabs(cos(angle_ * M_PI / 180));

  return {rect_.pos, rect_.height * absSin + rect_.width * absCos, rect_.height * absCos + rect_.width * absSin};
}

void kvashnin::Rectangle::move(const point_t &pos)
{
  rect_.pos = pos;
}

void kvashnin::Rectangle::move(const double dx, const double dy)
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

void kvashnin::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("coefficient can not be <= 0");
  }
  rect_.height *= coefficient;
  rect_.width *= coefficient;
}

void kvashnin::Rectangle::printInfo() const
{
  rectangle_t FrameRectData = getFrameRect();
  std::cout << "Rectangle coordinates: X: " << rect_.pos.x
      << "; Y: " << rect_.pos.y << std::endl
      << "Rectangle area: " << getArea() << std::endl
      << "Rectangle frame width: " << FrameRectData.width
      << "; height: " << FrameRectData.height << std::endl
      << "Angle: " << angle_ << std::endl;
}

void kvashnin::Rectangle::rotate(double angle)
{
  angle_ = fmod(angle_ + angle, 360);
  if (angle < 0)
  {
    angle_ += 360;
  }
}
