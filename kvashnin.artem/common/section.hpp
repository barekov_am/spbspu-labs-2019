#ifndef SECTION_HPP
#define SECTION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace kvashnin
{
  bool overlay(const Shape *shp1, const Shape *shp2);
  Matrix part(const CompositeShape &shp);
}

#endif
