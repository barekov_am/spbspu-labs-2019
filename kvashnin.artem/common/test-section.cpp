#include <boost/test/auto_unit_test.hpp>
#include "matrix.hpp"
#include "section.hpp"
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testSection)

BOOST_AUTO_TEST_CASE(sectionTestOverlay)
{
  kvashnin::Rectangle rec1({0, 0}, 2, 4);
  kvashnin::Rectangle rec2({2, 4}, 8, 10);
  kvashnin::Rectangle rec3({100, 100}, 5, 10);

  BOOST_CHECK(!kvashnin::overlay(&rec1, &rec3));
  BOOST_CHECK(kvashnin::overlay(&rec1, &rec2));
}

/*other tests in test-matrix.cpp*/

BOOST_AUTO_TEST_SUITE_END()
