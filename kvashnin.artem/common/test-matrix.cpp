#include <stdexcept>
#include <iostream>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "section.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "matrix.hpp"

BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(matrixTestLayers)
{
  kvashnin::Rectangle rec1({0, 0}, 2, 4);
  kvashnin::Rectangle rec2({0, 0}, 8, 10);
  kvashnin::Circle cir1({0, 0}, 2);
  kvashnin::Circle cir2({0, 0}, 4);
  kvashnin::CompositeShape comShp;

  comShp.add(&rec1);
  comShp.add(&rec2);
  comShp.add(&cir1);
  comShp.add(&cir2);

  kvashnin::Matrix matrix = kvashnin::part(comShp);

  BOOST_CHECK_EQUAL(matrix.getRows(), 4);
}



BOOST_AUTO_TEST_CASE(matrixTestColumnsAndRows)
{
  kvashnin::point_t center1 = {10, 50};
  kvashnin::point_t center2 = {-100, 100};
  kvashnin::point_t center3 = {50, -50};
  kvashnin::point_t center4 = {10, -50};

  kvashnin::Circle cir1(center1, 5);
  kvashnin::Circle cir2(center2, 10);
  kvashnin::Circle cir3(center3, 15);
  kvashnin::Rectangle rec1(center1, 2, 8);
  kvashnin::Rectangle rec2(center4, 8, 10);
  kvashnin::Rectangle rec3(center3, 5, 15);
  kvashnin::CompositeShape comShp;

  comShp.add(&cir1);
  comShp.add(&cir2);
  comShp.add(&cir3);
  comShp.add(&rec1);
  comShp.add(&rec2);
  comShp.add(&rec3);

  kvashnin::Matrix matrix = kvashnin::part(comShp);

  BOOST_CHECK_EQUAL(matrix.getRows(), 2);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 4);
}

BOOST_AUTO_TEST_CASE(matrixTestOperators)
{
  kvashnin::Circle cir1({0, 0}, 5);
  kvashnin::Circle cir2({0, 0}, 10);
  kvashnin::Circle cir3({0, 0}, 15);
  kvashnin::Circle cir4({0, 0}, 20);
  kvashnin::Rectangle rec1({0, 0}, 2, 4);
  kvashnin::Rectangle rec2({0, 0}, 4, 2);
  kvashnin::Rectangle rec3({0, 0}, 10, 20);
  kvashnin::CompositeShape comShp;

  comShp.add(&cir1);
  comShp.add(&cir2);
  comShp.add(&cir3);
  comShp.add(&rec1);
  comShp.add(&rec2);
  comShp.add(&rec3);

  kvashnin::Matrix matrix = kvashnin::part(comShp);

  BOOST_CHECK(matrix[0][0] == &cir1);
  BOOST_CHECK(matrix[1][0] == &cir2);
  BOOST_CHECK(matrix[2][0] == &cir3);
  BOOST_CHECK(matrix[3][0] == &rec1);
  BOOST_CHECK(matrix[4][0] == &rec2);
  BOOST_CHECK(matrix[5][0] == &rec3);
  BOOST_CHECK(matrix[0][0] != &rec1);
  BOOST_CHECK(matrix[1][0] != &cir3);
  BOOST_CHECK(matrix[2][0] != &rec3);
}

BOOST_AUTO_TEST_CASE(matrixTestConstructors)
{
  kvashnin::Circle cir1({0, 0}, 5);
  kvashnin::Circle cir2({0, 0}, 10);
  kvashnin::Rectangle rec1({0, 0}, 2, 4);
  kvashnin::Rectangle rec2({0, 0}, 4, 2);
  kvashnin::CompositeShape comShp;

  comShp.add(&rec1);
  comShp.add(&rec2);
  comShp.add(&cir1);
  comShp.add(&cir2);

  kvashnin::Matrix matrix1 = kvashnin::part(comShp);

  kvashnin::Matrix matrix2(matrix1);
  BOOST_CHECK_EQUAL(matrix2.getRows(), matrix1.getRows());
  BOOST_CHECK_EQUAL(matrix2.getColumns(), matrix1.getColumns());
  BOOST_CHECK(matrix1 == matrix2);

  kvashnin::Matrix matrix3(std::move(matrix1));
  BOOST_CHECK_EQUAL(matrix2.getRows(), matrix3.getRows());
  BOOST_CHECK_EQUAL(matrix2.getColumns(), matrix3.getColumns());
  BOOST_CHECK(matrix3 == matrix2);
}

BOOST_AUTO_TEST_CASE(matrixTestThrowingExceptions)
{
  kvashnin::Circle cir1({0, 0}, 5);
  kvashnin::Circle cir2({0, 0}, 10);
  kvashnin::Rectangle rec1({0, 0}, 2, 4);
  kvashnin::Rectangle rec2({0, 0}, 4, 2);
  kvashnin::CompositeShape comShp;

  comShp.add(&rec1);
  comShp.add(&rec2);
  comShp.add(&cir1);
  comShp.add(&cir2);

  kvashnin::Matrix matrix = kvashnin::part(comShp);

  BOOST_CHECK_THROW(kvashnin::Matrix(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(matrix[matrix.getRows() + 1][matrix.getColumns() + 1], std::out_of_range);
  BOOST_CHECK_THROW(matrix.add(nullptr, 0, 0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
