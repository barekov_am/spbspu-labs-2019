#include "matrix.hpp"
#include <iostream>

kvashnin::Matrix::Matrix():
  row_(0),
  col_(0),
  array_()
{}

kvashnin::Matrix::Matrix(const Matrix& mtx):
  row_(mtx.row_),
  col_(mtx.col_),
  array_(new Shape*[mtx.row_ * mtx.col_])
{
  for (int i = 0; i < (row_ * col_); i++)
  {
    array_[i] = mtx.array_[i];
  }
}

kvashnin::Matrix::Matrix(Matrix&& mtx):
  row_(mtx.row_),
  col_(mtx.col_),
  array_(std::move(mtx.array_))
{}

kvashnin::Matrix::Matrix(Shape* shape):
  row_(1),
  col_(1),
  array_(new Shape*[1])
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("null ptr");
  }

  array_[0] = shape;
}

kvashnin::Matrix& kvashnin::Matrix::operator =(const Matrix& rh)
{
  if (this != &rh)
  {
    row_ = rh.row_;
    col_ = rh.col_;
    std::unique_ptr<Shape*[]> tmpList(new Shape*[rh.row_ * rh.col_]);
    for (int i = 0; i < (row_ * col_); i++)
    {
      tmpList[i] = rh.array_[i];
    }
    array_.swap(tmpList);
  }

  return *this;
}

kvashnin::Matrix& kvashnin::Matrix::operator =(Matrix&& rh)
{
  if (this != &rh)
  {
    row_ = rh.row_;
    col_ = rh.col_;
    array_ = std::move(rh.array_);
  }

  return *this;
}

bool kvashnin::Matrix::operator ==(const Matrix& rh) const
{
  if ((row_ != rh.row_) || (col_ != rh.col_))
  {
    return false;
  }

  for (int i = 0; i < (row_ * col_); i++)
  {
    if (array_[i] != rh.array_[i])
    {
      return false;
    }
  }

  return true;
}

bool kvashnin::Matrix::operator !=(const Matrix& rh) const
{
  return *this != rh;
}

std::unique_ptr<kvashnin::Shape *[]> kvashnin::Matrix::operator [](int index) const
{
  if ((index >= row_) || (index < 0))
  {
    throw std::out_of_range("Index is out of range");
  }

  std::unique_ptr<Shape*[]> tmp(new Shape*[col_]);
  for (int i = 0; i < col_; i++)
  {
    tmp[i] = array_[index * col_ + i];
  }
  return tmp;
}

void kvashnin::Matrix::add(Shape *shape, const int row, const int column)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("null ptr");
  }

  int tmpRow = (row_ == row) ? (row_ + 1) : (row_);
  int tmpCol = (col_ == column) ? (col_ + 1) : (col_);
  std::unique_ptr<Shape*[]> tmp(new Shape*[tmpRow * tmpCol]);

  for (int i = 0; i < tmpRow; ++i)
  {
    for (int j = 0; j < tmpCol; ++j)
    {
      if ((row_ == i) || (col_ == j))
      {
        tmp[i * tmpCol + j] = nullptr;
      }
      else
      {
        tmp[i * tmpCol + j] = array_[i * col_ + j];
      }
    }
  }

  tmp[row * tmpCol + column] = shape;
  array_.swap(tmp);

  row_ = tmpRow;
  col_ = tmpCol;
}

void kvashnin::Matrix::printInfo() const
{
  std::cout << "Matrix size: " << std::endl
      << "*rows: " << row_ << std::endl
      << "*columns: " << col_ << std::endl;
  for (int i = 0; i < row_; ++i)
  {
    for (int j = 0; j < col_; ++j)
    {
      if (array_[i * col_ + j] != nullptr)
      {
        std::cout << "layer: " << i + 1
            << ", column: " << j + 1 << std::endl
            << "Object information:" << std::endl;
        array_[i * col_ + j]->printInfo();
        std::cout << std::endl << "*next object*" << std::endl;
      }
    }
  }
}

int kvashnin::Matrix::getRows() const
{
  return row_;
}

int kvashnin::Matrix::getColumns() const
{
  return col_;
}
