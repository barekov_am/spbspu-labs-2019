#include "section.hpp"
#include <cmath>

bool kvashnin::overlay(const kvashnin::Shape *shp1, const kvashnin::Shape *shp2)
{
  if ((shp1 == nullptr) || (shp2 == nullptr))
  {
    throw std::invalid_argument("null ptr");
  }

  rectangle_t shp1FrameRect = shp1->getFrameRect();
  rectangle_t shp2FrameRect = shp2->getFrameRect();

  bool X = fabs(shp1FrameRect.pos.x - shp2FrameRect.pos.x)
      < ((shp1FrameRect.width / 2) + (shp2FrameRect.width / 2));
  bool Y = fabs(shp1FrameRect.pos.y - shp2FrameRect.pos.y)
      < ((shp1FrameRect.height / 2) + (shp2FrameRect.height / 2));

  return X && Y;
}


kvashnin::Matrix kvashnin::part(const kvashnin::CompositeShape &shp)
{
  Matrix mtx;

  for (int i = 0; i < shp.size(); i++)
  {
    int row = 0;
    int col = 0;

    for (int j = 0; j < mtx.getRows(); j++)
    {
      for (int k = 0; k < mtx.getColumns(); k++)
      {
        if (mtx[j][k] == nullptr)
        {
          col = k;
          row = j;
          break;
        }

        if (overlay(shp[i], mtx[j][k]))
        {
          col = 0;
          row = j + 1;
          break;
        }
        col = k + 1;
        row = j;
      }
      if (row == j)
      {
        break;
      }
    }
    mtx.add(shp[i], row, col);
  }
  return mtx;
}
