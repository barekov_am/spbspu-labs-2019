#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>

#include "shape.hpp"
#include "base-types.hpp"

namespace kvashnin
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix& mtx);
    Matrix(Matrix&& mtx);
    Matrix(Shape* shape);
    ~Matrix() = default;

    Matrix& operator =(const Matrix& rh);
    Matrix& operator =(Matrix&& rh);

    bool operator ==(const Matrix& rh) const;
    bool operator !=(const Matrix& rh) const;

    std::unique_ptr<Shape*[]> operator [](int index) const;

    void add(Shape *shape, const int row, const int column);
    void printInfo() const;
    int getRows() const;
    int getColumns() const;

  private:
    int row_;
    int col_;
    std::unique_ptr<Shape *[]> array_;
  };
}

#endif
