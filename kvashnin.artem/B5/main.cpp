#include <iostream>
#include <string>
#include "part.hpp"

int main(int args, char *argv[])
{

  if(args != 2)
  {
    std::cerr << "Invalid arguments";
    return 1;
  }

  try
  {
    int option = std::stoi(argv[1]);
    switch(option)
    {
      case 1:
        op1(std::cin, std::cout);
        break;
      case 2:
        op2(std::cin, std::cout);
        break;
      default:
        std::cerr << "bad argument";
        return 1;
    }
  }
  catch(const std::invalid_argument &e)
  {
    std::cerr << e.what();
    return 1;
  }

  return 0;
}
