#ifndef OPTIONS_HPP
#define OPTIONS_HPP

#include <iostream>

void op1(std::istream &in, std::ostream &out);
void op2(std::istream &in, std::ostream &out);

#endif
