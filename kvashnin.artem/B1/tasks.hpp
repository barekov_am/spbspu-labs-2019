#ifndef TASKS_HPP
#define TASKS_HPP

void task1(const char* direction);
void task2(const char* filename);
void task3();
void task4(const char* direction, int size);

#endif //TASKS_HPP
