#include <vector>
#include "details.hpp"
#include "tasks.hpp"


void task3()
{
  std::vector <int> intVector;

  int num = -1;
  while (std::cin >> num)
  {
    if (num == 0)
    {
      break;
    }
    else
    {
      intVector.push_back(num);
    }
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Reading from std::cin failed.");
  }

  if (intVector.empty())
  {
    return;
  }

  if (num != 0)
  {
    throw std::runtime_error("End of line character is invalid. Expected 0");
  }

  switch (intVector.back())
  {
    case 1:
      for (auto i = intVector.begin(); i != intVector.end();)
      {
        if (*i % 2 == 0)
        {
          i = intVector.erase(i);
        }
        else
        {
          i++;
        }
      }
      break;

    case 2:
      for (auto i = intVector.begin(); i != intVector.end();)
      {
        if (*i % 3 == 0)
        {
          i = intVector.insert(++i, 3, 1);
        }
        else
        {
          i++;
        }
      }
      break;

    default:
      break;
  }

  print(intVector);
}
