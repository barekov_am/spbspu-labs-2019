#include <fstream>
#include <vector>
#include <memory>
#include <iostream>
#include "tasks.hpp"

const size_t initSize = 256;

void task2(const char* filename)
{
  std::ifstream inputFile(filename);

  if (!inputFile)
  {
    throw std::ios_base::failure("Failed opening the file");
  }

  using charArray = std::unique_ptr <char[], decltype(free)*>;
  charArray chArr(static_cast <char*>(malloc(initSize)), &free);

  if (!chArr)
  {
    throw std::runtime_error("Failed allocating memory");
  }

  size_t size = initSize;
  size_t countData = 0;

  while (!inputFile.eof())
  {
    inputFile.read(&chArr[countData], initSize);
    countData += inputFile.gcount();

    if (inputFile.gcount() == initSize)
    {
      size += initSize;
      charArray tmpArray(static_cast <char*> (realloc(chArr.get(), size)), &free);

      if (!tmpArray)
      {
        throw std::runtime_error("Failed reallocating memory");
      }

      chArr.release();
      std::swap(chArr, tmpArray);
    }

    if (inputFile.fail() && !inputFile.eof())
    {
      throw std::runtime_error("Failed reading data");
    }
  }

  std::vector <char> (&chArr[0], &chArr[countData]);

  for (size_t i = 0; i < countData; i++)
  {
    std::cout << chArr[i];
  }
}
