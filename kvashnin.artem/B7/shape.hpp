#ifndef B7_SHAPE_HPP
#define B7_SHAPE_HPP

#include <iostream>
#include <memory>

class Shape
{
public:
  Shape(int x, int y);
  virtual ~Shape() = default;

  bool isMoreLeft(const std::shared_ptr<Shape> &shape) const;
  bool isUpper(const std::shared_ptr<Shape> &shape) const;

  virtual void draw() const = 0;

protected:
  int x_;
  int y_;
};

#endif //B7_SHAPE_HPP
