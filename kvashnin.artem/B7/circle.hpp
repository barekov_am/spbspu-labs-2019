#ifndef B7_CIRCLE_HPP
#define B7_CIRCLE_HPP

#include "shape.hpp"

class Circle : public Shape
{
public:
  Circle(int x, int y);
  void draw() const override;
};

#endif //B7_CIRCLE_HPP
