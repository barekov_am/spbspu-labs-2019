#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "section.hpp"

int main()
{
  kvashnin::Circle circle1({0, 0}, 5);
  kvashnin::Circle circle2({2, 2}, 2);
  kvashnin::Rectangle rectangle1({10, 20}, 2, 4);
  kvashnin::Rectangle rectangle2({100, 100}, 10, 20);

  kvashnin::CompositeShape comShp;
  comShp.add(&circle1);
  comShp.add(&circle2);
  comShp.add(&rectangle1);
  comShp.add(&rectangle2);


  kvashnin::Matrix mtx = kvashnin::part(comShp);
  mtx.printInfo();
  comShp.rotate(180);
  mtx.printInfo();

  std::cout << "overlay demo:" << std::endl
      << kvashnin::overlay(&circle1, &circle2) << std::endl
      << kvashnin::overlay(&circle1, &rectangle2) << std::endl;

  return 0;
}
