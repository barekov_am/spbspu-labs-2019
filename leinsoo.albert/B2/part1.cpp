#include <iostream>
#include <string>
#include <sstream>
#include "QueueWithPriority.hpp"
#include "Parts.hpp"

void part1()
{
  QueueWithPriority<std::string> arr;
  std::string string;
 // char line[255];

  while(std::getline(std::cin, string))
  {
    std::stringstream stream(string);
    std::string command;
    stream >> command;

    if(command == "add")
    {
      std::string priority;
      stream >> priority;

      std::string data;
      stream.ignore();
      std::getline(stream, data);

      while (data[0] == ' ' || data[0] == '\t')
      {
        data.erase(0,1);
      }

      if(data.empty())
      {
        std::cout << "<INVALID COMMAND>" << std::endl;
      }
      else if(priority == "high")
      {
        arr.PutElementToQueue(data, ElementPriority::HIGH);
      }
      else if(priority == "normal")
      {
        arr.PutElementToQueue(data, ElementPriority::NORMAL);
      }
      else if(priority == "low")
      {
        arr.PutElementToQueue(data, ElementPriority::LOW);
      }
      else
      {
        std::cout << "<INVALID COMMAND>" << std::endl;
      }
    }
    else if(command == "get")
    {
      if(arr.empty())
      {
        std::cout << "<EMPTY>" << std::endl;
      }
      else
      {
        std::cout << arr.GetElementFromQueue() << std::endl;
      }
    }
    else if(command == "accelerate")
    {
      arr.Accelerate();
    }
    else
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }

    if(std::cin.eof())
    {
      break;
    }
  }
}
