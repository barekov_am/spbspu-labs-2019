#ifndef QUEUEWITHPRIORITY_HPP
#define QUEUEWITHPRIORITY_HPP

#include <list>
#include <stdexcept>

typedef enum
{
  LOW = 0,
  NORMAL = 1,
  HIGH = 2
} ElementPriority;

template <typename T>
class QueueWithPriority
{
  public:

    QueueWithPriority();
    virtual ~QueueWithPriority() = default;

    T GetElementFromQueue ();
    void PutElementToQueue (const T &element, ElementPriority priority);
    void Accelerate();
    bool empty() const;

  private:
    std::list<T> arr_;
    typename std::list<T>::const_iterator highPartition, normalPartition;
};

template <typename T>
QueueWithPriority<T>::QueueWithPriority()
{
  T Element;
  highPartition = arr_.insert(arr_.end(), Element);
  normalPartition = arr_.insert(arr_.end(), Element);
}

template <typename T>
T QueueWithPriority<T>::GetElementFromQueue()
{
  if(empty())
  {
    throw std::invalid_argument("Empty!");
  }
  if(arr_.begin() != highPartition)
  {
    T frontElem = arr_.front();
    arr_.pop_front();
    return frontElem;
  }
  else if(std::next(highPartition) != normalPartition)
  {
    T frontElem = *std::next(highPartition);
    arr_.erase(std::next(highPartition));
    return frontElem;
  }
  else
  {
    T frontElem = *std::next(normalPartition);
    arr_.erase(std::next(normalPartition));
    return frontElem;
  }
}

template <typename T>
void QueueWithPriority<T>::PutElementToQueue(const T &element, ElementPriority priority)
{
  if (priority == 2)
  {
    arr_.insert(highPartition, element);
  }
  else if (priority == 1)
  {
    arr_.insert(normalPartition, element);
  }
  else if (priority == 0)
  {
    arr_.insert(arr_.end(), element);
  }
}

template <typename T>
void QueueWithPriority<T>::Accelerate()
{
  auto it = std::next(normalPartition);
  while(it != arr_.end())
  {
    arr_.insert(highPartition, *it);
    it++;
    arr_.erase(std::prev(it));
  }
}

template <typename T>
bool QueueWithPriority<T>::empty() const
{
  if((arr_.begin() == highPartition) && (std::next(highPartition) == normalPartition)
    && (std::next(normalPartition) == arr_.end()))
  {
    return true;
  }
  else
  {
    return false;
  }
}

#endif // QUEUEWITHPRIORITY_HPP
