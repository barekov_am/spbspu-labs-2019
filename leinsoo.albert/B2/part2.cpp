#include <iostream>
#include <list>
#include "Parts.hpp"
#include <stdexcept>

void printList(const std::list<int> &list)
{
  std::list<int>::const_iterator Forward = list.begin();
  std::list<int>::const_reverse_iterator Backward = list.rbegin();

  for (size_t i = 0; i < list.size()/2; ++i)
    {
      if (i > 0)
      {
        std::cout<<" ";
      }
       std::cout <<*Forward <<" " <<*Backward;
      ++Forward;
      ++Backward;
    }
    if (list.size()%2 == 1)
    {
      if (list.size() > 1)
      {
        std::cout<<" ";
      }
      std::cout <<*Forward;
    }
    std::cout <<std::endl;
}

void part2()
{
  std::list<int> list;

  int value;
  while(std::cin >> value)
  {
    if((value < 1) || (value > 20))
    {
      throw std::invalid_argument("Valid values: 1-20");
    }

    list.push_back(value);
  }
  if(std::cin.fail() && !std::cin.eof())
  {
    throw std::invalid_argument("Input error");
  }
  if(list.size() > 20)
  {
    throw std::invalid_argument("Items should not be more than 20!");
  }

  printList(list);
}


