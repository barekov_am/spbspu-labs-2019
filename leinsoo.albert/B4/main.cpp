#include <iostream>
#include <stdexcept>
#include <sstream>
#include "DataStruct.hpp"

bool compare(const DataStruct &data_st_1, const DataStruct &data_st_2)
{
  if (data_st_1.key1 < data_st_2.key1)
  {
    return true;
  }
  if (data_st_1.key1 == data_st_2.key1)
  {
    if (data_st_1.key2 == data_st_2.key2)
    {
      if (data_st_1.str.size() == data_st_2.str.size())
      {
        return (data_st_1.str < data_st_2.str);
      }
      return (data_st_1.str.size() < data_st_2.str.size());
    }
    return (data_st_1.key2 < data_st_2.key2);
  }
  return false;
}

DataStruct giveElement(const std::string &string)
{
  DataStruct temp;
  std::istringstream input(string);
  temp.key1 = 0;
  temp.key2 = 0;

  input >> temp.key1;
  input.ignore(string.length(), ',');
  input >> temp.key2;
  input.ignore(string.length(), ',');
  std::getline(input, temp.str);
  if (input.fail() || (temp.key1 > 5) || (temp.key1 < -5) || (temp.key2 > 5) || (temp.key2 < -5) || temp.str.empty())
  {
    throw std::invalid_argument("<INVALID INPUT>");
  }
  return temp;
}

void optionVector(std::vector<DataStruct>& vector)
{
  std::string line;
  while (std::getline(std::cin, line))
  {
    const auto temp = giveElement(line);
    vector.push_back(temp);
  }
  std::sort(vector.begin(), vector.end(), compare);
}

int main()
{
  std::vector<DataStruct> vector;
  try
  {
    optionVector(vector);

    for (auto ite :vector)
    {
      std::cout<< ite.key1 << "," << ite.key2 << "," << ite.str << std::endl;
    }
  }
  catch (std::exception &e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}
