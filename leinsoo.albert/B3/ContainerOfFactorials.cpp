#include "ContainerOfFactorials.hpp"

ContainerOfFactorials::Iterator::Iterator(size_t index, unsigned int value)
{
  index_ = index;
  value_ = value;
}

const unsigned int * ContainerOfFactorials::Iterator::operator->() const
{
  return &value_;
}

const unsigned int & ContainerOfFactorials::Iterator::operator*() const
{
  return value_;
}

ContainerOfFactorials::Iterator ContainerOfFactorials::begin()
{
  return ContainerOfFactorials::Iterator(1, 1);
}

ContainerOfFactorials::Iterator ContainerOfFactorials::end()
{
  return ContainerOfFactorials::Iterator(11, 39916800);
}

ContainerOfFactorials::Iterator & ContainerOfFactorials::Iterator::operator++()
{
  if (index_ < 11)
  {
    ++index_;
    value_ *= index_;
  }
  return *this;
}

ContainerOfFactorials::Iterator ContainerOfFactorials::Iterator::operator++(int)
{
  Iterator prev = *this;
  ++(*this);
  return prev;
}

ContainerOfFactorials::Iterator & ContainerOfFactorials::Iterator::operator--()
{
  if (index_ > 1)
  {
    value_ /= index_;
    --index_;
  }
  return *this;
}

bool ContainerOfFactorials::Iterator::operator==(const Iterator & other) const
{
  return (index_ == other.index_);
}

bool ContainerOfFactorials::Iterator::operator!=(const Iterator & other) const
{
  return (!(other == *this));
}

ContainerOfFactorials::Iterator ContainerOfFactorials::Iterator::operator--(int)
{
  Iterator prev = *this;
  --(*this);
  return prev;
}
