#include <vector>
#include <fstream>
#include <memory>
#include <iostream>
#include "labB1Templates.hpp"

void labB1task2 (char *arguments[])
{
  size_t inpFileSize = 4;

  std::ifstream inputFile (arguments[2]);
  if(!inputFile.is_open())
  {
    throw std::invalid_argument("Error task 2 - 1:Can not open file");
  }

  size_t curSize = inpFileSize;
  std::unique_ptr<char[], decltype (&free)> innerArrayC (static_cast<char*>(malloc(curSize)), &free);

  if (!innerArrayC)
  {
    throw std::runtime_error("Error task 2 - 2: Can not allocate");
  }

  size_t i = 0;
  while(inputFile)
  {
    inputFile.read(&innerArrayC[i], static_cast<long long>(inpFileSize));
    i += static_cast<unsigned long long>(inputFile.gcount());

    if (inputFile.gcount() == static_cast<long long>(inpFileSize))
    {
      curSize += inpFileSize;
      std::unique_ptr<char[], decltype (&free)> tmpArr(static_cast<char*>(realloc(innerArrayC.get(), curSize)), &free);

      if (tmpArr)
      {
        innerArrayC.release();
        std::swap(innerArrayC, tmpArr);
      }
      else
      {
        inputFile.close();
        throw std::invalid_argument("Error task 2 - 3: Can not reallocate");
      }
    }
  }

  inputFile.close();
  if(inputFile.is_open())
  {
    throw std::invalid_argument("Error task 2 - 4: Can not close file");
  }

  detail::print(&innerArrayC[0], &innerArrayC[i], detail::isNotDelimited, detail::isNotDelimited);
}
