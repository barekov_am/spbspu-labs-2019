#include <cstring>
#include <vector>
#include "labB1Templates.hpp"
#include <cstdlib>
#include <ctime>

void fillRandom(double * array, unsigned long long size)
{
  srand(static_cast<unsigned int>(time(nullptr)));
  while (size != 0)
  {
    array[--size] = (static_cast<double>(rand()) / (RAND_MAX / 2)) - 1.0;
//    size--;
  }
}

void labB1task4(char *sortParam, const unsigned long long vectorSize)
{
  sortDirection sortOrderParam = getSortDirection(sortParam);
  std::vector<double> random_vector(vectorSize);

  fillRandom(random_vector.data(), random_vector.size());

  detail::print(random_vector.begin(), random_vector.end(), detail::isDelimited, detail::isDelimited);
  detail::sort(random_vector, detail::IteratorAccess<std::vector<double>>(), sortOrderParam);
  detail::print(random_vector.begin(), random_vector.end(), detail::isDelimited, detail::isDelimited);
}
