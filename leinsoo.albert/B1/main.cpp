#include <cstdlib>
#include <stdexcept>
#include <iostream>

void labB1task1(char *arguments[]);
void labB1task2(char *arguments[]);
void labB1task3();
void labB1task4(char *sortParam, const unsigned long long vectorSize);

int main(int argc, char *argv[])
{
  try
  {
    if (argc < 2)
    {
      std::cerr << "Error - main 01: Need at least 2 parameters.";
      return 1;
    }
    switch(atoi(argv[1]))
    {
    case 1:
      if (argc < 3)
      {
        std::cerr << "Error - main 02: Missing parameters for task 1" << std::endl;
        return 1;
      }
      else
      {
        labB1task1(argv);
        break;
      }
    case 2:
      if (argc < 3)
      {
        std::cerr << "Error - main 03: Missing parameters for task 2" << std::endl;
        return 1;
      }
      else
      {
        labB1task2(argv);
        break;
      }
    case 3:
      labB1task3();
      break;

    case 4:
      if (argc < 4)
      {
        std::cerr << "Error - main 04: Missing parameters" << std::endl;
        return 1;
      }
      else
      {
        unsigned long long vectorSize = 0;
        try
        {
          vectorSize = static_cast<unsigned long long>(std::stoll(argv[3]));
        }
        catch (const std::invalid_argument& ia)
        {
          std::cerr << "Error task 4 - 2:Vector size must be a Number!: " << ia.what() << '\n';
          return 1;
        }

        labB1task4(argv[2], vectorSize);
        break;
      }
    default:
      std::cerr << "Error - main 05: Invalid task number" << std::endl;
      return 1;
    }
  }
  catch (const std::exception &exc)
  {
    std::cerr << exc.what() << std::endl;
    return 2; // В заданиях возникла ошибка
  }
  return 0;
}
