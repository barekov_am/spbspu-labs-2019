#ifndef LAB_B1_TEMPLATES
#define LAB_B1_TEMPLATES

#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <cstring>
#include <functional>

enum sortDirection {ascending=1, descending=0};

sortDirection getSortDirection(char* direction);

namespace detail
{
const bool isDelimited = true;
const bool isNotDelimited = false;

template<typename T>
class IndexAccess
{
public:
  typename T::reference operator()(T& cont, size_t i)
  {
    return cont[i];
  }
  size_t begin(const T&) const
  {
    return 0;
  }
  size_t end(const T& cont) const
  {
    return cont.size();
  }
};

template<typename T>
class AtAccess
{
public:
  typename T::reference operator()(T& cont, size_t i)
  {
    return cont.at(i);
  }
  size_t begin(const T&) const
  {
    return 0;
  }
  size_t end(const T& cont) const
  {
    return cont.size();
  }
};

template<typename T>
class IteratorAccess
{
public:
  typename T::reference operator()(T&, typename T::iterator it)
  {
    return *it;
  }
  typename T::iterator begin(T& cont)
  {
    return cont.begin();
  }
  typename T::iterator end(T& cont)
  {
    return cont.end();
  }
};

template<typename Container, typename Access, typename OrderParam>
void sort(Container& cont, Access access, OrderParam OrderMode)
{
  for(auto i = access.begin(cont); i != access.end(cont); ++i)
  {
    for(auto j = i; j != access.end(cont); ++j)
    {
      if ( (access(cont, i) > access(cont, j)) == OrderMode)
      {
        std::swap(access(cont, i), access(cont, j));
      }
    }
  }
}

template<typename ContainerIterator>
void print(ContainerIterator begin, ContainerIterator end, const bool spaceDelimiter, const bool isDelimitRow)
{
  for(ContainerIterator i = begin; i != end; ++i)
  {
    if (spaceDelimiter == true)
    {
      std::cout << *i << ' ';
    }
    else
    {
      std::cout << *i;
    }
  }
  if (isDelimitRow == true)
  {
    std::cout << '\n';
  }
}

} //details

#endif //LAB_B1_TEMPLATES
