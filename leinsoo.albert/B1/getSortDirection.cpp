#include "labB1Templates.hpp"

sortDirection getSortDirection(char *direction)
{
  if (std::strcmp("ascending", direction) == 0)
  {
    return sortDirection::ascending;
  }
  else if  (std::strcmp("descending", direction) == 0)
  {
    return sortDirection::descending;
  }
  else
  {
    throw std::invalid_argument("Error: Wrong sort direction parameter");
  }
}
