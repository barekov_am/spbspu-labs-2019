#include <iostream>
#include <vector>
#include <iterator>
#include "labB1Templates.hpp"
void labB1task3()
{
  std::vector<int> input_vector((std::istream_iterator<int>(std::cin)),
                                std::istream_iterator<int>());

  if (!std::cin.eof())
  {
    throw std::invalid_argument("Error task 3 - 1: Vector input error");
  }

  if(input_vector.empty())
  {
    return;
  }

  if(input_vector.back() != 0)
  {
    throw std::invalid_argument("Error task 3 - 3: Missing '0' at the end");
  }

  input_vector.pop_back();

  if(input_vector.empty())
  {
    throw std::invalid_argument("Error task 3 - 4:Missing data or vector is empty");
  }

  switch (input_vector.back())
  {
  case 1:
  {
    for(auto i = input_vector.begin(); i != input_vector.end();)
    {
      if(*i % 2 == 0)
      {
        i = input_vector.erase(i);
      }
      else
      {
        ++i;
      }
    }
    break;
  }

  case 2:
  {
    for(auto i = input_vector.begin(); i < input_vector.end();)
    {
      if (*i % 3 == 0)
      {
        ++i;
        i = input_vector.insert(i, 3, 1);
        i += 3;
      }
      else
      {
        ++i;
      }
    }
    break;
  }

  }

  detail::print(input_vector.begin(),input_vector.end(), detail::isDelimited, detail::isDelimited);
}
