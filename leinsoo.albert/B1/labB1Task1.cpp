#include "labB1Templates.hpp"
#include <vector>
#include <forward_list>
void labB1task1(char *arguments[])
{
  sortDirection sortOrderParam = getSortDirection(arguments[2]);

  std::vector <int> inputVector;
  int inputValue (0);

  while (std::cin >> inputValue)
  {
    if(!std::cin.good())
      {
        throw std::invalid_argument("Error task 1 - 2: Invalid input. Some values are not integer");
      }
    inputVector.push_back(inputValue);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Error task 1 - 3: Invalid input. Some values are not integer");
  }

  std::vector<int> copyInputVector (inputVector);
  std::forward_list<int> copyInputVectorAsDataList(inputVector.begin(), inputVector.end());

  detail::sort(inputVector, detail::IndexAccess<std::vector<int>>(), sortOrderParam);
  detail::print(inputVector.begin(), inputVector.end(), detail::isDelimited, detail::isDelimited);

  detail::sort(copyInputVector, detail::AtAccess<std::vector<int>>(), sortOrderParam);
  detail::print(inputVector.begin(), inputVector.end(), detail::isDelimited, detail::isDelimited);

  detail::sort(copyInputVectorAsDataList, detail::IteratorAccess<std::forward_list<int>>(), sortOrderParam);
  detail::print(inputVector.begin(), inputVector.end(), detail::isDelimited, detail::isDelimited);
}

