#include "rectangle.hpp"
#include <cassert>
#include <iostream>
#include <cmath>

const double MAXANGLE = 360.0;

lebedev::Rectangle::Rectangle(const double height, const double width, const point_t &center) :
  height_(height),
  width_(width),
  center_(center)
{
  if (height_ <= 0.0)
  {
    throw std::invalid_argument("width of rectangle can't be <= 0");
  }
  if (width_ <= 0.0)
  {
    throw std::invalid_argument("height of rectangle can't be <= 0");
  }
}

double lebedev::Rectangle::getArea() const
{
  return width_ * height_;
}

lebedev::rectangle_t lebedev::Rectangle::getFrameRect() const
{
  return { height_, width_, center_ };
}

void lebedev::Rectangle::move(const point_t &point_n)
{
  center_.x = point_n.x;
  center_.y = point_n.y;
}

void lebedev::Rectangle::move(const double x, const double y)
{
  center_.x += x;
  center_.y += y;
}

void lebedev::Rectangle::showInfo() const
{
  std::cout << "Area: " << getArea()
      << " height: " << getFrameRect().height
      << " width: " << getFrameRect().width
      << " pos.x " << getFrameRect().pos.x
      << " pos.y " << getFrameRect().pos.y << std::endl;
}

void lebedev::Rectangle::printInfo() const
{
  std::cout << "Area: " << getArea()
      << " height: " << getFrameRect().height
      << " width: " << getFrameRect().width
      << " pos.x " << getFrameRect().pos.x
      << " pos.y " << getFrameRect().pos.y << std::endl;
}

void lebedev::Rectangle::scale(const double multiply)
{
  if (multiply<=0.0)
  {
    throw std::invalid_argument("Invalid scale");
  }

  width_ *= multiply;
  height_ *= multiply;
}

void lebedev::Rectangle::rotate(const double angle)
{
  angle_ = angle;

  if (angle_ < 0.0)
  {
    angle_ = MAXANGLE + fmod(angle_, MAXANGLE);
  }
  else
  {
    angle_ = fmod(angle_, MAXANGLE);
  }
}
