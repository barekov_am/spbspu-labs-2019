#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <cmath>

const double MAXANGLE = 360.0;

lebedev::CompositeShape::CompositeShape() :
  size_(0),
  shapes_(nullptr),
  angle_(0.0)
{
}

lebedev::CompositeShape::CompositeShape(const CompositeShape &composite) :
  size_(composite.size_),
  shapes_(std::make_unique<shapePtr[]>(composite.size_)),
  angle_(composite.angle_)
{
  for (size_t i = 0; i < size_; ++i)
  {
    shapes_[i] = composite.shapes_[i];
  }
}

lebedev::CompositeShape::CompositeShape(CompositeShape &&composite) :
  size_(composite.size_),
  shapes_(std::move(composite.shapes_)),
  angle_(composite.angle_)
{
  composite.size_ = 0;
}

lebedev::CompositeShape::CompositeShape(const shapePtr &shape) :
  size_(1),
  shapes_(std::make_unique<shapePtr[]>(size_)),
  angle_(0.0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape can`t be a nullptr");
  }

  shapes_[0] = shape;
}

lebedev::CompositeShape &lebedev::CompositeShape::operator =(const CompositeShape &composite)
{
  if (this != &composite)
  {
    size_ = composite.size_;
    angle_ = composite.angle_;
    dynamicArray tmpArray = std::make_unique<shapePtr[]>(size_);

    for (size_t i = 0; i < size_; i++)
    {
      tmpArray[i] = composite.shapes_[i];
    }
    shapes_.swap(tmpArray);
  }

  return *this;
}

lebedev::CompositeShape &lebedev::CompositeShape::operator =(CompositeShape &&composite)
{
  if (this != &composite)
  {
    size_ = composite.size_;
    angle_ = composite.size_;
    shapes_ = std::move(composite.shapes_);
    composite.size_ = 0;
  }

  return *this;
}

lebedev::shapePtr lebedev::CompositeShape::operator [](size_t index) const
{
  if (size_ <= index)
  {
    throw std::out_of_range("Index is out of range");
  }

  return shapes_[index];
}

double lebedev::CompositeShape::getArea() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  double area = 0.0;

  for (size_t i = 0; i < size_; i++)
  {
    area += shapes_[i]->getArea();
  }

  return area;
}

void lebedev::CompositeShape::move(const point_t &point)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  point_t tmpFrame = getFrameRect().pos;
  double dx = point.x - tmpFrame.x;
  double dy = point.y - tmpFrame.y;

  move(dx, dy);
}

void lebedev::CompositeShape::move(const double dx, const double dy)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}


void lebedev::CompositeShape::scale(const double coefficient)
{
  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Coefficient of scale must be a positive number");
  }

  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  point_t tmpFrame = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++)
  {
    point_t centerShape = shapes_[i]->getFrameRect().pos;
    double dx = (centerShape.x - tmpFrame.x) * (coefficient - 1);
    double dy = (centerShape.y - tmpFrame.y) * (coefficient - 1);
    shapes_[i]->move(dx, dy);
    shapes_[i]->scale(coefficient);
  }
}

size_t lebedev::CompositeShape::getSize() const
{
  return size_;
}

void lebedev::CompositeShape::add(const shapePtr &composite)
{
  if (composite == nullptr)
  {
    throw std::invalid_argument("New adding shape can`t be a nullptr");
  }

  dynamicArray tmpShape = std::make_unique<shapePtr[]>(size_ + 1);
  for (size_t i = 0; i < size_; i++)
  {
    tmpShape[i] = shapes_[i];
  }

  tmpShape[size_] = composite;
  size_++;
  shapes_.swap(tmpShape);
}

void lebedev::CompositeShape::remove(size_t index)
{
  if (size_ <= index)
  {
    throw std::out_of_range("Index is out of range");
  }

  size_--;

  for (size_t i = index; i < size_; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
}

void lebedev::CompositeShape::inform() const
{
  const rectangle_t framingRect = getFrameRect();
  std::cout << std::endl << "CompositeShape frame width : " << framingRect.width;
  std::cout << std::endl << "CompositeShape frame height : " << framingRect.height;
  std::cout << std::endl << "Centre : ( x: " << framingRect.pos.x << ", y: " << framingRect.pos.y << " )";
  std::cout << std::endl << "Area is " << getArea() << std::endl;
}

void lebedev::CompositeShape::showInfo() const
{
  const rectangle_t framingRect = getFrameRect();
  std::cout << std::endl << "CompositeShape frame width : " << framingRect.width;
  std::cout << std::endl << "CompositeShape frame height : " << framingRect.height;
  std::cout << std::endl << "Centre : ( x: " << framingRect.pos.x << ", y: " << framingRect.pos.y << " )";
  std::cout << std::endl << "Area is " << getArea() << std::endl;
}

void lebedev::CompositeShape::printInfo() const
{
  const rectangle_t framingRect = getFrameRect();
  std::cout << std::endl << "CompositeShape frame width : " << framingRect.width;
  std::cout << std::endl << "CompositeShape frame height : " << framingRect.height;
  std::cout << std::endl << "Centre : ( x: " << framingRect.pos.x << ", y: " << framingRect.pos.y << " )";
  std::cout << std::endl << "Area is " << getArea() << std::endl;
}

lebedev::rectangle_t lebedev::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape must include at least 1 shape");
  }

  rectangle_t framingRect = shapes_[0]->getFrameRect();
  double minX = framingRect.pos.x - framingRect.width / 2;
  double minY = framingRect.pos.y - framingRect.height / 2;
  double maxX = framingRect.pos.x + framingRect.width / 2;
  double maxY = framingRect.pos.y + framingRect.height / 2;

  for (size_t i = 1; i < size_; i++)
  {
    framingRect = shapes_[i]->getFrameRect();
    minX = std::min(framingRect.pos.x - framingRect.width / 2, minX);
    minY = std::min(framingRect.pos.y - framingRect.height / 2, minY);
    maxX = std::max(framingRect.pos.x + framingRect.width / 2, maxX);
    maxY = std::max(framingRect.pos.y + framingRect.height / 2, maxY);
  }

  return rectangle_t{ (maxY - minY), (maxX - minX), { (minX + maxX) / 2, (minY + maxY) / 2 } };
}

void lebedev::CompositeShape::rotate(const double angle)
{
  angle_ += angle;

  if (angle_ < 0.0)
  {
    angle_ = MAXANGLE + fmod(angle_, MAXANGLE);
  }
  else
  {
    angle_ = fmod(angle_, MAXANGLE);
  }

  const double radAngle = angle_ * M_PI / 180;
  const point_t centre = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++)
  {
    const double oldX = shapes_[i]->getFrameRect().pos.x - centre.x;
    const double oldY = shapes_[i]->getFrameRect().pos.y - centre.y;

    const double newX = oldX * fabs(cos(radAngle)) - oldY * fabs(sin(radAngle));
    const double newY = oldX * fabs(sin(radAngle)) + oldY * fabs(cos(radAngle));

    shapes_[i]->move({ centre.x + newX, centre.y + newY });
    shapes_[i]->rotate(angle);
  }
}

lebedev::dynamicArray lebedev::CompositeShape::getList() const
{
  dynamicArray tmpArray(std::make_unique<shapePtr[]>(size_));

  for (size_t i = 0; i < size_; i++)
  {
    tmpArray[i] = shapes_[i];
  }

  return tmpArray;
}
