#ifndef SHAPE_HPP
#define SHAPE_HPP
#include "base-types.hpp"
#include <memory>

namespace lebedev
{
  class Shape 
  {
    public:
      virtual double getArea() const = 0;
      virtual rectangle_t getFrameRect() const = 0;
      virtual void move(const point_t &) = 0;
      virtual void move(const double x, const double y) = 0;
      virtual void showInfo() const = 0;
      virtual void printInfo() const = 0;
      virtual void scale(const double multiply) = 0;
      virtual void rotate(const double) = 0;
      virtual ~Shape() = default;
  };

  using shapePtr = std::shared_ptr<Shape>;
  using dynamicArray = std::unique_ptr<shapePtr[]>;
}

#endif

