#include <iostream>
#include <cmath>
#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double PRECISION = 0.01;

BOOST_AUTO_TEST_SUITE(myRectangle)


BOOST_AUTO_TEST_CASE(rectangleConstAfterMoving)
{
  lebedev::Rectangle myRectangle(2, 3, { 1, -1 });
  const lebedev::rectangle_t firstFrame = myRectangle.getFrameRect();
  const double firstArea = myRectangle.getArea();

  myRectangle.move({ 4, 5 });
  lebedev::rectangle_t secondFrame = myRectangle.getFrameRect();
  double secondArea = myRectangle.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);

  myRectangle.move(-2, 3);
  secondFrame = myRectangle.getFrameRect();
  secondArea = myRectangle.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(rectangleScaling)
{
  lebedev::Rectangle myRectangle(2, 3, { 1, -1 });
  const double firstArea = myRectangle.getArea();

  const double multiplier = 2.0;
  myRectangle.scale(multiplier);
  double secondArea = myRectangle.getArea();

  BOOST_CHECK_CLOSE(firstArea * pow(multiplier, 2), secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  lebedev::Rectangle myRectangle(2, 3, { 1, -1 });
  const double testArea = myRectangle.getArea();
  const lebedev::rectangle_t testFrameRect = myRectangle.getFrameRect();
  const double angle = 90;

  myRectangle.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, myRectangle.getFrameRect().width, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.height, myRectangle.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(testArea, myRectangle.getArea(), PRECISION);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, myRectangle.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, myRectangle.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_CASE(throwingExceptions)
{
  BOOST_CHECK_THROW(lebedev::Rectangle(-2, 3, { 5, 2 }), std::invalid_argument);
  lebedev::Rectangle rectangle_(6, 2, { 3, 5 });
  BOOST_CHECK_THROW(rectangle_.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
