#define _USE_MATH_DEFINES
#include "circle.hpp"
#include <cassert>
#include <cmath>
#include <iostream>

const double MAXANGLE = 360.0;

lebedev::Circle::Circle(const double rad, const point_t &center) :
  radius_(rad),
  center_(center)
{
  if (rad <= 0)
  {
    throw std::invalid_argument("radius of circle can't be <=0");
  }
}

double lebedev::Circle::getArea() const
{
  return (M_PI * radius_ * radius_);
}

lebedev::rectangle_t lebedev::Circle::getFrameRect() const
{
  return { 2 * radius_, 2 * radius_, center_ };
}

void lebedev::Circle::move(const point_t &point_n)
{
  center_.x = point_n.x;
  center_.y = point_n.y;
}

void lebedev::Circle::move(const double x, const double y)
{
  center_.x += x;
  center_.y += y;
}

void lebedev::Circle::showInfo() const
{
  std::cout << "Area: " << getArea()
      << " height: " << getFrameRect().height
      << " width: " << getFrameRect().width
      << " pos.x " << getFrameRect().pos.x
      << " pos.y " << getFrameRect().pos.y << std::endl;
}

void lebedev::Circle::printInfo() const
{
  std::cout << "Area: " << getArea()
      << " height: " << getFrameRect().height
      << " width: " << getFrameRect().width
      << " pos.x " << getFrameRect().pos.x
      << " pos.y " << getFrameRect().pos.y << std::endl;
}

void lebedev::Circle::scale(const double multiply)
{
  if (multiply<=0.0)
  {
    throw std::invalid_argument("Invalid scale");
  }

  radius_ *= multiply;
}

void lebedev::Circle::rotate(const double angle)
{
  angle_ = angle;

  if (angle_ < 0.0)
  {
    angle_ = MAXANGLE + fmod(angle_, MAXANGLE);
  }
  else
  {
    angle_ = fmod(angle_, MAXANGLE);
  }
}
