#include <iostream>
#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double PRECISION = 0.01;

BOOST_AUTO_TEST_SUITE(compositeShapeTest)

BOOST_AUTO_TEST_CASE(constantParametersAfterMovingToDistance)
{
  lebedev::Rectangle testRectangle(  2.1, 3.3, { 5.0, 3.9 } );
  lebedev::Circle testCircle( 2.5, { 5.9, 3.0 } );
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const double testArea = testCompositeShape.getArea();
  const lebedev::rectangle_t testFrameRect = testCompositeShape.getFrameRect();

  testCompositeShape.move(1.0, 8.3);

  BOOST_CHECK_CLOSE(testFrameRect.width, testCompositeShape.getFrameRect().width, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCompositeShape.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(testArea, testCompositeShape.getArea(), PRECISION);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterMovingToPoint)
{
  lebedev::Rectangle testRectangle( 2.1, 3.3, { 5.0, 3.9 } );
  lebedev::Circle testCircle( 2.5, { 5.9, 3.0 } );
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const double testArea = testCompositeShape.getArea();
  const lebedev::rectangle_t testFrameRect = testCompositeShape.getFrameRect();

  testCompositeShape.move({ 3.1, 5.0 });

  BOOST_CHECK_CLOSE(testFrameRect.width, testCompositeShape.getFrameRect().width, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCompositeShape.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(testArea, testCompositeShape.getArea(), PRECISION);
}

BOOST_AUTO_TEST_CASE(squareIncreaseAreaAfterScale)
{
  lebedev::Rectangle testRectangle(  2.1, 3.3, { 5.0, 3.9 } );
  lebedev::Circle testCircle( 2.5, { 5.9, 3.0 } );
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const double testArea = testCompositeShape.getArea();
  const double coefficient = 2.1;

  testCompositeShape.scale(coefficient);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testArea * coefficient * coefficient, PRECISION);
}

BOOST_AUTO_TEST_CASE(squareDecreaseAreaAfterScale)
{
  lebedev::Rectangle testRectangle(  2.1, 3.3, { 5.0, 3.9 } );
  lebedev::Circle testCircle( 2.5, { 5.9, 3.0 } );
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const double testArea = testCompositeShape.getArea();
  const double coefficient = 0.6;

  testCompositeShape.scale(coefficient);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testArea * coefficient * coefficient, PRECISION);
}

BOOST_AUTO_TEST_CASE(areaAfterRotation)
{
  lebedev::Rectangle testRectangle( 4.0, 2.0, { 5.0, 5.0 });
  lebedev::Circle testCircle(1.0, { 5.0, 5.0 });
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const double testArea = testCompositeShape.getArea();
  const double angle = 45;

  testCompositeShape.rotate(angle);

  BOOST_CHECK_CLOSE(testArea, testCompositeShape.getArea(), PRECISION);
}

BOOST_AUTO_TEST_CASE(parametersAfterAddingAndDeletion)
{
  lebedev::Rectangle testRectangle(  2.1, 3.3, { 5.0, 3.9 } );
  lebedev::Circle testCircle( 2.5, { 5.9, 3.0 } );
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);

  const double testCompositeArea = testCompositeShape.getArea();
  const double testCircleArea = testCircle.getArea();

  testCompositeShape.add(circlePtr);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea + testCircleArea, PRECISION);

  testCompositeShape.remove(1);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  lebedev::Rectangle testRectangle(  2.1, 3.3, { 5.0, 3.9 } );
  lebedev::Circle testCircle( 2.5, { 5.9, 3.0 } );
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  BOOST_CHECK_THROW(testCompositeShape.add(nullptr), std::invalid_argument);

  BOOST_CHECK_THROW(testCompositeShape.scale(-3.0), std::invalid_argument);
  BOOST_CHECK_THROW(testCompositeShape.scale(0.0), std::invalid_argument);

  BOOST_CHECK_THROW(testCompositeShape.remove(4), std::out_of_range);
  BOOST_CHECK_THROW(testCompositeShape.remove(-2), std::out_of_range);

  BOOST_CHECK_THROW(testCompositeShape[4], std::out_of_range);
  BOOST_CHECK_THROW(testCompositeShape[-2], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkCopyConstructor)
{
  lebedev::Rectangle testRectangle(  2.1, 3.3, { 5.0, 3.9 } );
  lebedev::Circle testCircle( 2.5, { 5.9, 3.0 } );
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const lebedev::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getSize();

  lebedev::CompositeShape copyCompositeShape(testCompositeShape);

  const lebedev::rectangle_t copyFrameRect = copyCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, copyCompositeShape.getArea(), PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.width, copyFrameRect.width, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.height, copyFrameRect.height, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, copyFrameRect.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, copyFrameRect.pos.y, PRECISION);
  BOOST_CHECK_EQUAL(testCompositeSize, copyCompositeShape.getSize());
}

BOOST_AUTO_TEST_CASE(checkMoveConstructor)
{
  lebedev::Rectangle testRectangle(  2.1, 3.3, { 5.0, 3.9 } );
  lebedev::Circle testCircle( 2.5, { 5.9, 3.0 } );
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const lebedev::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getSize();

  lebedev::CompositeShape moveCompositeShape(std::move(testCompositeShape));

  const lebedev::rectangle_t moveFrameRect = moveCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, moveCompositeShape.getArea(), PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.width, moveFrameRect.width, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.height, moveFrameRect.height, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, moveFrameRect.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, moveFrameRect.pos.y, PRECISION);
  BOOST_CHECK_EQUAL(testCompositeSize, moveCompositeShape.getSize());
}

BOOST_AUTO_TEST_CASE(checkCopyOperator)
{
  lebedev::Rectangle testRectangle(  2.1, 3.3, { 5.0, 3.9 } );
  lebedev::Circle testCircle( 2.5, { 5.9, 3.0 } );
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const lebedev::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getSize();

  lebedev::Circle testCircleNew( 3.1, { 3.1, 4.7 });
  lebedev::shapePtr circleNewPtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape copyCompositeShape(circleNewPtr);

  copyCompositeShape = testCompositeShape;

  const lebedev::rectangle_t copyFrameRect = copyCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, copyCompositeShape.getArea(), PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.width, copyFrameRect.width, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.height, copyFrameRect.height, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, copyFrameRect.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, copyFrameRect.pos.y, PRECISION);
  BOOST_CHECK_EQUAL(testCompositeSize, copyCompositeShape.getSize());
}

BOOST_AUTO_TEST_CASE(checkMoveOperator)
{
  lebedev::Rectangle testRectangle(  2.1, 3.3, { 5.0, 3.9 } );
  lebedev::Circle testCircle( 2.5, { 5.9, 3.0 } );
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  const lebedev::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getSize();

  lebedev::CompositeShape moveCompositeShape;

  moveCompositeShape = std::move(testCompositeShape);

  const lebedev::rectangle_t moveFrameRect = moveCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, moveCompositeShape.getArea(), PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.width, moveFrameRect.width, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.height, moveFrameRect.height, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, moveFrameRect.pos.x, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, moveFrameRect.pos.y, PRECISION);
  BOOST_CHECK_EQUAL(testCompositeSize, moveCompositeShape.getSize());
}

BOOST_AUTO_TEST_SUITE_END()

