#define _USE_MATH_DEFINES
#include "triangle.hpp"
#include <cmath>
#include <cassert>
#include <algorithm>
#include <iostream>

const double MAXANGLE = 360.0;

lebedev::Triangle::Triangle(const point_t &point_a, const point_t &point_b, const point_t &point_c) :
  a_(point_a),
  b_(point_b),
  c_(point_c),
  center_({ (point_a.x + point_b.x + point_c.x) / 3 , (point_a.y + point_b.y + point_c.y) / 3 }),
  angle_(0.0)
{
  if (this->getArea() == 0.0)
  {
    throw std::invalid_argument("vertexes of triangle can't lie on the same straight line or match");
  }
}

double lebedev::Triangle::getArea() const
{
  double p, ab, ac, bc;

  ab = sqrt((a_.x - b_.x) * (a_.x - b_.x) + (a_.y - b_.y) * (a_.y - b_.y));
  ac = sqrt((a_.x - c_.x) * (a_.x - c_.x) + (a_.y - c_.y) * (a_.y - c_.y));
  bc = sqrt((b_.x - c_.x) * (b_.x - c_.x) + (b_.y - c_.y) * (b_.y - c_.y));
  p = (ab + ac + bc) / 2;

  return sqrt(p * (p - ab) * (p - ac) * (p - bc));
}

lebedev::rectangle_t lebedev::Triangle::getFrameRect() const
{
  double min_x = std::min({ a_.x, b_.x, c_.x });
  double max_x = std::max({ a_.x, b_.x, c_.x });
  
  double width = abs(max_x - min_x);
  
  double min_y = std::min({ a_.y, b_.y, c_.y });
  double max_y = std::max({ a_.y, b_.y, c_.y });
  
  double height = abs(max_y - min_y);

  point_t center = { (max_x - min_x) / 2 + min_x, (max_y - min_y) / 2 + min_y };

  return { height, width, center };
}

void lebedev::Triangle::move(const point_t &point_n)
{
  double shift_x, shift_y;

  shift_x = point_n.x - center_.x;
  shift_y = point_n.y - center_.y;

  a_.x += shift_x;
  a_.y += shift_y;

  b_.x += shift_x;
  b_.y += shift_y;

  c_.x += shift_x;
  c_.y += shift_y;

  center_ = { point_n.x, point_n.y };
}

void lebedev::Triangle::move(const double x, const double y)
{
  center_.x += x;
  center_.y += y;

  a_.x += x;
  a_.y += y;

  b_.x += x;
  b_.y += y;

  c_.x += x;
  c_.y += y;
}

void lebedev::Triangle::rotate(const double angle)
{
  angle_ = angle;

  if (angle_ < 0.0)
  {
    angle_ = MAXANGLE + fmod(angle_, MAXANGLE);
  }
  else
  {
    angle_ = fmod(angle_, MAXANGLE);
  }

  const double radAngle = angle_ * M_PI / 180;

  rotatePoint(a_, radAngle);
  rotatePoint(b_, radAngle);
  rotatePoint(c_, radAngle);
}

void lebedev::Triangle::rotatePoint(point_t &point, const double radAngle)
{
  const double oldX = point.x - center_.x;
  const double oldY = point.y - center_.y;

  const double newX = oldX * fabs(cos(radAngle)) - oldY * fabs(sin(radAngle));
  const double newY = oldX * fabs(sin(radAngle)) + oldY * fabs(cos(radAngle));

  point = { center_.x + newX, center_.y + newY }; 
}


void lebedev::Triangle::showInfo() const
{
  std::cout << "Area: " << getArea()
      << " height: " << getFrameRect().height
      << " width: " << getFrameRect().width
      << " pos.x " << getFrameRect().pos.x
      << " pos.y " << getFrameRect().pos.y << std::endl;
}

void lebedev::Triangle::printInfo() const
{
  std::cout << "Area: " << getArea()
      << " height: " << getFrameRect().height
      << " width: " << getFrameRect().width
      << " pos.x " << getFrameRect().pos.x
      << " pos.y " << getFrameRect().pos.y << std::endl;
}

void lebedev::Triangle::scale(const double multiply)
{
  if (multiply<=0.0)
  {
    throw std::invalid_argument("Invalid scale");
  }
  a_.x *= multiply;
  a_.y *= multiply;
  b_.x *= multiply;
  b_.y *= multiply;
  c_.x *= multiply;
  c_.y *= multiply;
}
