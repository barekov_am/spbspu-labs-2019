#include <iostream>
#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "polygon.hpp"

const double PRECISION = 0.01;

BOOST_AUTO_TEST_SUITE(myPolygon)

BOOST_AUTO_TEST_CASE(polygonConstAfterMoving)
{
  lebedev::point_t points_[]{ {4, 5}, {6, 4}, {7, -1}, {5, -3}, {2, 1} };

  lebedev::Polygon myPolygon(5, points_);
  const lebedev::rectangle_t firstFrame = myPolygon.getFrameRect();
  const double firstArea = myPolygon.getArea();

  myPolygon.move({ 4, 5 });
  lebedev::rectangle_t secondFrame = myPolygon.getFrameRect();
  double secondArea = myPolygon.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);

  myPolygon.move(-2, 3);
  secondFrame = myPolygon.getFrameRect();
  secondArea = myPolygon.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(polygonScaling)
{
  lebedev::point_t points_[]{ {4, 5}, {6, 4}, {7, 1}, {5, 3}, {2, 1} };
  lebedev::Polygon myPolygon(5, points_);
  const double firstArea = myPolygon.getArea();

  const double multiplier = 6;
  myPolygon.scale(multiplier);
  double secondArea = myPolygon.getArea();

  BOOST_CHECK_CLOSE(firstArea * pow(multiplier, 2), secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  lebedev::point_t points_[]{ {4, 5}, {6, 4}, {7, 1}, {5, 3}, {2, 1} };
  lebedev::Polygon myPolygon(5, points_);
  const double testArea = myPolygon.getArea();
  const lebedev::rectangle_t testFrameRect = myPolygon.getFrameRect();
  const double angle = 90;
  const double testArea2 = myPolygon.getArea();
  const lebedev::rectangle_t testFrameRect2 = myPolygon.getFrameRect();

  myPolygon.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testFrameRect2.width, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.height, testFrameRect2.height, PRECISION);
  BOOST_CHECK_CLOSE(testArea, testArea2, PRECISION);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testFrameRect2.pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testFrameRect2.pos.y);
}

BOOST_AUTO_TEST_CASE(throwingExceptions)
{
  lebedev::point_t pointArray1[]{ {0, 0}, {-1, 0} };
  BOOST_CHECK_THROW(lebedev::Polygon(2, pointArray1), std::invalid_argument);
  lebedev::point_t pointArray2[]{ { }, { } };
  BOOST_CHECK_THROW(lebedev::Polygon(2, pointArray2), std::invalid_argument);

  lebedev::point_t pointArray[]{ {4, 5}, {6, 4}, {7, -1}, {5, -3}, {2, 1} };
  lebedev::Polygon polygon_(5, pointArray);
  BOOST_CHECK_THROW(polygon_.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
