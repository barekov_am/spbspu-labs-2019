#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separation.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithCrossing)
{
  const size_t layersValue = 2;
  const size_t columnsValue = 1;

  lebedev::Rectangle testRectangle( 4.0, 2.0, { 5.0, 5.0 } );
  lebedev::Circle testCircle( 2.0, { 5.0, 5.0 } );
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  lebedev::Matrix testMatrix = lebedev::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithoutCrossing)
{
  const size_t layersValue = 1;
  const size_t columnsValue = 2;

  lebedev::Rectangle testRectangle( 4.0, 2.0, { 5.0, 5.0 } );
  lebedev::Circle testCircle(2.0, { 10.0, 5.0 });
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  lebedev::Matrix testMatrix = lebedev::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  lebedev::Rectangle testRectangle( 4.0, 2.0, { 5.0, 5.0 } );
  lebedev::Circle testCircle( 2.0, { 5.0, 5.0 } );
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  lebedev::Matrix testMatrix = lebedev::part(testCompositeShape);

  BOOST_CHECK_THROW(testMatrix[5][0], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-5][0], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkCopyAndMove)
{
  lebedev::Rectangle testRectangle( 4.0, 2.0, { 5.0, 5.0 } );
  lebedev::Circle testCircle( 2.0, { 5.0, 5.0 } );
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  lebedev::Matrix testMatrix = lebedev::part(testCompositeShape);
  lebedev::Matrix copyMatrix(testMatrix);
  lebedev::Matrix moveMatrix(std::move(testMatrix));

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyMatrix);
}

BOOST_AUTO_TEST_SUITE_END()
