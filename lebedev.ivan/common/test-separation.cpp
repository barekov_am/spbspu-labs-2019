#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separation.hpp"

BOOST_AUTO_TEST_SUITE(separationTest)

BOOST_AUTO_TEST_CASE(checkCorrectCrossing)
{
  lebedev::Rectangle testRectangle( 4, 2, {5, 5} );
  lebedev::Rectangle crossingTestRectangle( 6, 2, {5, 5} );
  lebedev::Circle testCircle(50, { 11, 5 });

  const bool trueResult = lebedev::cross(testRectangle.getFrameRect(), testCircle.getFrameRect());
  const bool trueResult1 = lebedev::cross(testRectangle.getFrameRect(), crossingTestRectangle.getFrameRect());
  const bool trueResult2 = lebedev::cross(testCircle.getFrameRect(), crossingTestRectangle.getFrameRect());

  BOOST_CHECK_EQUAL(trueResult, true);
  BOOST_CHECK_EQUAL(trueResult1, true);
  BOOST_CHECK_EQUAL(trueResult2, true);
}

BOOST_AUTO_TEST_CASE(checkCorrectSeparation)
{
  lebedev::Rectangle testRectangle({ 4, 2, {5, 5} });
  lebedev::Rectangle crossingTestRectangle({ 6, 2, {8, 5} });
  lebedev::Circle testCircle(1, { 11, 5 });
  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(testRectangle);
  lebedev::shapePtr rectangleCrossingPtr = std::make_shared<lebedev::Rectangle>(crossingTestRectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(testCircle);

  lebedev::CompositeShape testCompositeShape(rectanglePtr);
  testCompositeShape.add(rectanglePtr);
  testCompositeShape.add(circlePtr);

  lebedev::Matrix testMatrix = lebedev::part(testCompositeShape);

  BOOST_CHECK(testMatrix[0][0] == rectanglePtr);
  BOOST_CHECK(testMatrix[0][1] == circlePtr);
}

BOOST_AUTO_TEST_SUITE_END()

