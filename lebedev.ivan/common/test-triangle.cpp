#include <iostream>
#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "triangle.hpp"

const double PRECISION = 0.01;

BOOST_AUTO_TEST_SUITE(myTriangle)

BOOST_AUTO_TEST_CASE(triangleConstAfterMoving)
{
  lebedev::Triangle myTriangle({ 0, 0 }, { 3, 0 }, { 0, 4 });
  const lebedev::rectangle_t firstFrame = myTriangle.getFrameRect();
  const double firstArea = myTriangle.getArea();

  myTriangle.move({ 4, 5 });
  lebedev::rectangle_t secondFrame = myTriangle.getFrameRect();
  double secondArea = myTriangle.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);

  myTriangle.move(-2, 3);
  secondFrame = myTriangle.getFrameRect();
  secondArea = myTriangle.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(triangleScaling)
{
  lebedev::Triangle myRectangle({ 1, -1 }, { 2, 2 }, { 3, -2 });
  const double firstArea = myRectangle.getArea();

  const double multiplier = 2.0;
  myRectangle.scale(multiplier);
  double secondArea = myRectangle.getArea();

  BOOST_CHECK_CLOSE(firstArea * pow(multiplier, 2), secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  lebedev::Triangle myTriangle({ 4, 2 }, { 5, 5 }, { 1, 1 });
  const double testArea = myTriangle.getArea();
  const lebedev::rectangle_t testFrameRect = myTriangle.getFrameRect();
  const double angle = 90;
  const lebedev::rectangle_t testFrameRect2 = myTriangle.getFrameRect();


  myTriangle.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, myTriangle.getFrameRect().width, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.height, myTriangle.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(testArea, myTriangle.getArea(), PRECISION);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testFrameRect2.pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testFrameRect2.pos.y);
}


BOOST_AUTO_TEST_CASE(throwingExceptions)
{
  BOOST_CHECK_THROW(lebedev::Triangle({ 0, 0 }, { 0, 0 }, { 0, 0 }), std::invalid_argument);

  lebedev::Triangle triangle_({ 3, 5 }, { 2, 2 }, { -1, -3 });
  BOOST_CHECK_THROW(triangle_.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
