#include "matrix.hpp"

#include <stdexcept>
#include <iostream>

lebedev::Matrix::Matrix() :
  rows_(0),
  columns_(0),
  list_()
{
}

lebedev::Matrix::Matrix(const Matrix &other) :
  rows_(other.rows_),
  columns_(other.columns_),
  list_(std::make_unique<shapePtr[]>(other.rows_ * other.columns_))
{
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    list_[i] = other.list_[i];
  }
}

lebedev::Matrix::Matrix(Matrix &&other) :
  rows_(other.rows_),
  columns_(other.columns_),
  list_(std::move(other.list_))
{
  other.rows_ = 0;
  other.columns_ = 0;
}

lebedev::Matrix &lebedev::Matrix::operator =(const Matrix &other)
{
  if (this != &other)
  {
    rows_ = other.rows_;
    columns_ = other.columns_;
    dynamicArray tmpMatrix(std::make_unique<shapePtr[]>(other.rows_ * other.columns_));

    for (size_t i = 0; i < (rows_ * columns_); i++)
    {
      tmpMatrix[i] = other.list_[i];
    }
    list_.swap(tmpMatrix);
  }

  return *this;
}

lebedev::Matrix& lebedev::Matrix::operator =(Matrix &&other)
{
  if (this != &other)
  {
    rows_ = other.rows_;
    columns_ = other.columns_;
    list_ = std::move(other.list_);
    other.rows_ = 0;
    other.columns_ = 0;
  }

  return *this;
}

lebedev::dynamicArray lebedev::Matrix::operator [](size_t index) const
{
  if (rows_ <= index)
  {
    throw std::out_of_range("Index is out of range");
  }

  dynamicArray tmpMatrix(std::make_unique<shapePtr[]>(columns_));

  for (size_t i = 0; i < columns_; i++)
  {
    tmpMatrix[i] = list_[index * columns_ + i];
  }

  return tmpMatrix;
}

bool lebedev::Matrix::operator ==(const Matrix &other) const
{
  if ((rows_ != other.rows_) || (columns_ != other.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (list_[i] != other.list_[i])
    {
      return false;
    }
  }

  return true;
}

bool lebedev::Matrix::operator !=(const Matrix &other) const
{
  return !(*this == other);
}

size_t lebedev::Matrix::getLines() const
{
  return rows_;
}

size_t lebedev::Matrix::getColumns() const
{
  return columns_;
}

void lebedev::Matrix::add(const shapePtr &other, size_t line, size_t column)
{
  size_t tmpLines;
  size_t tmpColumns;

  if (line == rows_)
  {
    tmpLines = rows_ + 1;
  }
  else
  {
    tmpLines = rows_;
  }

  if (column == columns_)
  {
    tmpColumns = columns_ + 1;
  }
  else
  {
    tmpColumns = columns_;
  }

  dynamicArray tmpList(std::make_unique<shapePtr[]>(tmpLines * tmpColumns));

  for (size_t i = 0; i < tmpLines; i++)
  {
    for (size_t j = 0; j < tmpColumns; j++)
    {
      if ((i == rows_) || (j == columns_))
      {
        tmpList[i * tmpColumns + j] = nullptr;
      }
      else
      {
        tmpList[i * tmpColumns + j] = list_[i * columns_ + j];
      }
    }
  }

  tmpList[line * tmpColumns + column] = other;
  list_.swap(tmpList);
  rows_ = tmpLines;
  columns_ = tmpColumns;
}

void lebedev::Matrix::showInfo() const
{
  std::cout << "--- MATRIX --- \n";
  std::cout << "Value of lines: " << rows_ << "\n";
  std::cout << "Value of columns: " << columns_ << "\n\n";

  for (size_t i = 0; i < rows_; i++)
  {
    for (size_t j = 0; j < columns_; j++)
    {
      if (list_[i * columns_ + j] != nullptr)
      {
        std::cout << "On layer number " << i + 1 << " and position " << j + 1 << " there is figure:\n";
        list_[i * columns_ + j]->showInfo();
      }
    }
  }
}

void lebedev::Matrix::printInfo() const
{
  std::cout << "--- MATRIX --- \n";
  std::cout << "Value of lines: " << rows_ << "\n";
  std::cout << "Value of columns: " << columns_ << "\n\n";

  for (size_t i = 0; i < rows_; i++)
  {
    for (size_t j = 0; j < columns_; j++)
    {
      if (list_[i * columns_ + j] != nullptr)
      {
        std::cout << "On layer number " << i + 1 << " and position " << j + 1 << " there is figure:\n";
        list_[i * columns_ + j]->showInfo();
      }
    }
  }
}
