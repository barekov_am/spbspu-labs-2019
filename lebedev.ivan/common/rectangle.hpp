#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"
namespace lebedev
{
  class Rectangle : public Shape
  {
    public:
      Rectangle(const double a, const double b, const point_t &);

      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const point_t &) override;
      void move(const double x, const double y) override;
      void showInfo() const override;
      void printInfo() const override;
      void scale(const double multiply) override;
      void rotate(const double) override;

    private:
      double height_, width_;
      point_t center_;
      double angle_;
  };
}
#endif
