#ifndef BASETYPES_HPP
#define BASETYPES_HPP

namespace lebedev
{
  struct point_t
  {
    double x, y;
  };

  struct rectangle_t
  {
    double height, width;
    point_t pos;
  };
}

#endif
