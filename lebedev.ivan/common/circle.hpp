#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

namespace lebedev
{
  class Circle : public Shape
  {
    public:
      Circle(const double r, const point_t &);

      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const point_t &) override;
      void move(const double x, const double y) override;
      void showInfo() const override;
      void printInfo() const override;
      void scale(const double multiply) override;
      void rotate(const double) override;

    private:
      double radius_;
      point_t center_;
      double angle_;
  };
}
#endif
