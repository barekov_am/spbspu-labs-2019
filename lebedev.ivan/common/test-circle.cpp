#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

const double PRECISION = 0.01;

BOOST_AUTO_TEST_SUITE(myCircle)

BOOST_AUTO_TEST_CASE(circleConstAfterMoving)
{
  lebedev::Circle myCircle(2, { 1, -1 });
  const lebedev::rectangle_t firstFrame = myCircle.getFrameRect();
  const double first = myCircle.getArea();

  myCircle.move({ 4, 5 });
  lebedev::rectangle_t secondFrame = myCircle.getFrameRect();
  double secondArea = myCircle.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(first, secondArea, PRECISION);

  myCircle.move(-2, 3);
  secondFrame = myCircle.getFrameRect();
  secondArea = myCircle.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(first, secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(circleScaling)
{
  lebedev::Circle myCircle(2, { 1, -1 });
  const double firstArea = myCircle.getArea();

  const double multiplier = 2.0;
  myCircle.scale(multiplier);
  double secondArea = myCircle.getArea();

  BOOST_CHECK_CLOSE(firstArea * multiplier * multiplier, secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  lebedev::Circle myCircle(2, { 1, -1 });
  const double testArea = myCircle.getArea();
  const lebedev::rectangle_t testFrameRect = myCircle.getFrameRect();
  const double angle = 90;

  myCircle.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, myCircle.getFrameRect().width, PRECISION);
  BOOST_CHECK_CLOSE(testFrameRect.height, myCircle.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(testArea, myCircle.getArea(), PRECISION);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, myCircle.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, myCircle.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_CASE(throwingExceptions)
{
  BOOST_CHECK_THROW(lebedev::Circle(-2, { 3, 4 }), std::invalid_argument);
  lebedev::Circle circle_(7, { 6, 5 });
  BOOST_CHECK_THROW(circle_.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
