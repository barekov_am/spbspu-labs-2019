#define _USE_MATH_DEFINES
#include "polygon.hpp"
#include <ctime>
#include <iostream>
#include <cassert>
#include <stdexcept>
#include <algorithm>
#include <cmath>

const double MAXANGLE = 360.0;

lebedev::Polygon::Polygon(const int dimension, const point_t * points)
{
  if (dimension <= 2) 
  {
  throw std::invalid_argument("number of polygon's vertices can't be <=2");
  }
  if (points == nullptr) 
  {
  throw std::invalid_argument("pointer to vertex is null");
  }
  dim_ = dimension;
  points_ = new point_t[dimension];
  double sumx = 0, sumy = 0;
  for (int i = 0; i < dimension; i++)
  {
  points_[i] = points[i];
  sumx += points_[i].x;
  sumy += points_[i].y;
  }
  center_ = { sumx / dimension, sumy / dimension };
}

lebedev::Polygon::Polygon(const Polygon& AnotherOne) :
  dim_(AnotherOne.dim_),
  points_(new point_t[dim_])
{
  for (int i = 0; i < dim_; i++)
  {
  points_[i] = AnotherOne.points_[i];
  }
}

lebedev::Polygon::Polygon(Polygon&& AnotherOne) noexcept :
  dim_(0),
  points_(nullptr)
{
  swap(AnotherOne);
}

lebedev::Polygon::~Polygon()
{
  delete[] points_;
}

lebedev::Polygon & lebedev::Polygon::operator= (const lebedev::Polygon & AnotherOne)
{
  if (&AnotherOne != this)
  {
  Polygon temp(AnotherOne);
  swap(temp);
  }
  return *this;
}

lebedev::Polygon & lebedev::Polygon::operator= (lebedev::Polygon && AnotherOne)
{
  if (&AnotherOne != this)
  {
  swap(AnotherOne);
  delete[] AnotherOne.points_;
  AnotherOne.dim_ = 0;
  AnotherOne.points_ = nullptr;
  }
  return *this;
}

double lebedev::Polygon::getArea() const
{
  if ((points_ == nullptr) || (dim_ <= 2))
  {
  return 0;
  }
  double sum_pos = 0, sum_neg = 0;
  for (int i = 0; i < dim_ - 1; i++)
  {
  sum_pos += points_[i].x * points_[i + 1].y;
  sum_neg += points_[i + 1].x * points_[i].y;
  }
  double area = sum_pos + points_[dim_ - 1].x * points_[0].y
    - sum_neg - points_[0].x * points_[dim_ - 1].y;
  return (0.5 * std::fabs(area));;
}

lebedev::rectangle_t lebedev::Polygon::getFrameRect() const
{
  double minX = points_[0].x;
  double maxX = points_[0].x;
  double minY = points_[0].y;
  double maxY = points_[0].y;

  for (int i = 0; i < dim_; i++)
  {
  maxX = std::max(maxX, points_[i].x);
  minX = std::min(minX, points_[i].x);
  maxY = std::max(maxY, points_[i].y);
  minY = std::min(minY, points_[i].y);
  }

  double width = abs(maxX - minX);
  double height = abs(maxY - minY);

  point_t center = { (maxX - minX) / 2 + minX, (maxY - minY) / 2 + minY };

  return { height, width, center };
}

void lebedev::Polygon::move(const point_t &point_n)
{
  double shiftX = point_n.x - center_.x;
  double shiftY = point_n.y - center_.y;

  for (int i = 0; i < dim_; i++)
  {
  points_[i].x += shiftX;
  points_[i].y += shiftY;
  }

  center_ = { point_n.x, point_n.y };
}

void lebedev::Polygon::move(const double shiftX, const double shiftY)
{
  center_.x += shiftX;
  center_.y += shiftY;

  for (int i = 0; i < dim_; i++)
  {
  points_[i].x += shiftX;
  points_[i].y += shiftY;
  }
}

void lebedev::Polygon::showInfo() const
{
  std::cout << "Area: " << getArea()
      << " height: " << getFrameRect().height
      << " width: " << getFrameRect().width
      << " pos.x " << getFrameRect().pos.x
      << " pos.y " << getFrameRect().pos.y << std::endl;
}

void lebedev::Polygon::printInfo() const
{
  std::cout << "Area: " << getArea()
      << " height: " << getFrameRect().height
      << " width: " << getFrameRect().width
      << " pos.x " << getFrameRect().pos.x
      << " pos.y " << getFrameRect().pos.y << std::endl;
}

void lebedev::Polygon::swap(Polygon& AnotherOne) noexcept
{
  std::swap(dim_, AnotherOne.dim_);
  std::swap(points_, AnotherOne.points_);
}

void lebedev::Polygon::scale(const double multiply)
{
  if (multiply<=0.0)
  {
  throw std::invalid_argument("Invalid scale");
  }

  for (int i = 0; i < dim_; i++)
  {
  points_[i].x *= multiply;
  points_[i].y *= multiply;
  }
}

void lebedev::Polygon::rotate(const double angle)
{
  angle_ = angle;

  if (angle_ < 0.0)
  {
    angle_ = MAXANGLE + fmod(angle_, MAXANGLE);
  }
  else
  {
    angle_ = fmod(angle_, MAXANGLE);
  }

  const double radAngle = angle_ * M_PI / 180;

  for (int i = 0; i < dim_; i++)
  {
    rotatePoint(points_[i], radAngle);
  }
}

void lebedev::Polygon::rotatePoint(point_t &point, const double radAngle)
{
  const double oldX = point.x - center_.x;
  const double oldY = point.y - center_.y;

  const double newX = oldX * fabs(cos(radAngle)) - oldY * fabs(sin(radAngle));
  const double newY = oldX * fabs(sin(radAngle)) + oldY * fabs(cos(radAngle));

  point = { center_.x + newX, center_.y + newY };
}
