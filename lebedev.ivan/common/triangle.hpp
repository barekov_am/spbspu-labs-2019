#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

namespace lebedev
{
  class Triangle : public Shape
  {
    public:
      Triangle(const point_t &, const point_t &, const point_t &);

      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const point_t &) override;
      void move(const double x, const double y) override;
      void showInfo() const override;
      void printInfo() const override;
      void scale(const double multiply) override;
      void rotate(const double) override;
      void rotatePoint(point_t &, const double);

    private:
      point_t a_, b_, c_, center_;
      double angle_;
  };
}
#endif
