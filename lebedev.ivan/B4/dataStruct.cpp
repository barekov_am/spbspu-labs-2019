#include "dataStruct.hpp"

int extractNumber(std::string&, std::istream&);
void skipSpaces(std::string&);

bool DataStruct::operator<(const DataStruct& rhs)
{
  if (key1 < rhs.key1)
  {
    return true;
  }

  if (key1 == rhs.key1)
  {
    if (key2 < rhs.key2)
    {
      return true;
    }

    if (key2 == rhs.key2)
    {
      if (str.length() < rhs.str.length())
      {
        return true;
      }
    }
  }

  return false;
}

bool DataStruct::operator>(const DataStruct& rhs)
{
  if (key1 > rhs.key1)
  {
    return true;
  }

  if (key1 == rhs.key1)
  {
    if (key2 > rhs.key2)
    {
      return true;
    }

    if (key2 == rhs.key2)
    {
      if (str.length() > rhs.str.length())
      {
        return true;
      }
    }
  }

  return false;
}

bool DataStruct::operator<=(const DataStruct& rhs)
{
  return !operator>(rhs);
}

bool DataStruct::operator>=(const DataStruct& rhs)
{
  return !operator<(rhs);
}

bool DataStruct::operator==(const DataStruct& rhs)
{
  if (key1 == rhs.key1)
  {
    return true;
  }

  if (key2 == rhs.key2)
  {
    return true;
  }

  if (str.length() == rhs.str.length())
  {
    return true;
  }

  return false;
}

bool DataStruct::operator!=(const DataStruct& rhs)
{
  return !operator==(rhs);
}

std::ostream& operator<<(std::ostream& cout, const DataStruct& data)
{
  cout << data.key1 << "," << data.key2 << "," << data.str;
  return cout;
}

std::istream& operator>>(std::istream& cin, DataStruct& data)
{
  if (cin.eof())
  {
    cin.setstate(std::ios_base::failbit);
    return cin;
  }
  std::string str;
  std::getline(cin, str);
  skipSpaces(str);
  if (str.empty())
  {
    cin.setstate(std::ios_base::failbit);
    return cin;
  }
  data.key1 = extractNumber(str, cin);
  if (cin.fail())
  {
    return cin;
  }
  skipSpaces(str);
  data.key2 = extractNumber(str, cin);
  if (cin.fail())
  {
    return cin;
  }

  skipSpaces(str);
  if (str.empty())
  {
    cin.clear(std::ios_base::failbit);
  }
  data.str = str;

  return cin;
}
