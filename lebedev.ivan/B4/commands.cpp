#include <string>
#include <iostream>

const char COMMA = ',';
const int MAXNUMBER = 5;
const int MINNUMBER = -5;

int extractNumber(std::string& str, std::istream& cin)
{
  size_t pos = str.find(COMMA);
  if (pos == std::string::npos)
  {
    cin.clear(std::ios_base::failbit);
    return 0;
  }

  std::string num = str.substr(0, pos);
  str.erase(0, pos + 1);
  char* ptr = nullptr;
  int number = std::strtol(num.c_str(), &ptr, 10);
  if (*ptr != 0x00)
  {
    cin.clear(std::ios_base::failbit);
    return 0;
  }

  if ((number < MINNUMBER) || (number > MAXNUMBER))
  {
    cin.clear(std::ios_base::failbit);
    return 0;
  }

  return number;
}

void skipSpaces(std::string& str)
{
  while (isspace(str[0]))
  {
    str.erase(0, 1);
  }
}
