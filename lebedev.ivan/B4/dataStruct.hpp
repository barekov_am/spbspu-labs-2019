#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP
#include <string>
#include <iostream>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;

  bool operator<(const DataStruct& rhs);
  bool operator<=(const DataStruct& rhs);
  bool operator>(const DataStruct& rhs);
  bool operator>=(const DataStruct& rhs);
  bool operator==(const DataStruct& rhs);
  bool operator!=(const DataStruct& rhs);
};

std::ostream& operator<<(std::ostream& cout, const DataStruct& data);
std::istream& operator>>(std::istream& cin, DataStruct& data);

#endif
