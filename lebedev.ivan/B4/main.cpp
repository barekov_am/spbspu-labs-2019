#include <vector>
#include <algorithm>
#include <iterator>
#include "dataStruct.hpp"

int main()
{
  try
  {
    std::vector<DataStruct> vector;
    DataStruct data;
    while (std::cin >> data)
    {
      vector.push_back(data);
    }

    if ((!std::cin.eof()) && (std::cin.fail()))
    {
      std::cerr << "Invalid reading data\n";
      return 1;
    }

    std::sort(vector.begin(), vector.end());
    std::copy(vector.begin(), vector.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));
  }

  catch (std::exception & exception)
  {
    exception.what();
    return 1;
  }

  return 0;
}
