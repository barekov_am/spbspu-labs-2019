#include <iostream>
#include "details.hpp"

int main(int argc, char* argv[])
{
  if (argc != 2)
  {
    std::cerr << "Invalid number of arguments!";
    return 1;
  }

  try
  {
    char* checkedPtr = nullptr;
    const int switcher = std::strtoll(argv[1], &checkedPtr, 10);
    if (*checkedPtr != '\x00')
    {
      std::cerr << "Invalid argument!\n";
      return 1;
    }

    switch (switcher)
    {
      case FIRST:
      {
        taskOne();
        break;
      }
      case SECOND:
      {
        taskTwo();
        break;
      }
      default:
      {
        std::cerr << "Invalid number of task!\n";
        return 1;
      }
    }
  }
  catch (const std::exception & e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}
