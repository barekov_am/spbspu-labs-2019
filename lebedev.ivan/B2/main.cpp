#include <string.h>
#include "queueWithProiority.hpp"

int main(int argc, char* argv[])
{
  if (argc != 2)
  {
    std::cerr << "Invalid number of arguments";
    return 1;
  }
  try
  {
    size_t numTask = std::stoi(argv[1]);
    switch (numTask)
    {
      case 1:
      {
        taskOne();
      }
      break;
      case 2:
      {
        taskTwo();
      }
      break;
      default:
      {
        std::cerr << "Invalid number of task";
        return 1;
      }
    }
  }
  catch (std::ios_base::failure & err)
  {
    std::cerr << err.what();
    return 1;
  }
  catch (std::invalid_argument & err)
  {
    std::cerr << err.what();
    return 1;
  }
  return 0;
}
