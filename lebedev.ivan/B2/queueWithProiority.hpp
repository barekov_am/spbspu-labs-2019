#include <iostream>
#include <cstring>
#include <algorithm>
#include <iterator>
#include "details.hpp"

template <typename Type>
  void QueueWithPriority<Type>::putElementToQueue(const Type& name, ElementPriority priority)
  {
    switch (priority)
    {
      case ElementPriority::HIGH:
      {
        dequeWithPriority[0].push_back(name);
      }
      break;

      case ElementPriority::NORMAL:
      {
        dequeWithPriority[1].push_back(name);
      }
      break;
      case ElementPriority::LOW:
      {
        dequeWithPriority[2].push_back(name);
      }
      break;
      default:
      {
        std::cerr << "Trouble with Priority in taskOne";
        return;
      }
    }
  }

template <typename Type>
Type QueueWithPriority<Type>::getElementFromQueue()
{
  for (int i = 0; i < 3; i++)
  {
    if (!dequeWithPriority[i].empty())
    {
      Type name = dequeWithPriority[i].front();
      dequeWithPriority[i].pop_front();
      return name;
    }
  }
  throw std::invalid_argument("Queue is empty");
}

template <typename Type>
void QueueWithPriority<Type>::accelerate()
{
  std::copy(dequeWithPriority[2].begin(), dequeWithPriority[2].end(), std::back_inserter(dequeWithPriority[0]));
  dequeWithPriority[2].clear();
}

template <typename Type>
bool QueueWithPriority<Type>::empty()
{
  return (dequeWithPriority[0].empty() && dequeWithPriority[1].empty() && dequeWithPriority[2].empty());
}
