#ifndef DETAILS_HPP_INCLUDED
#define DETAILS_HPP_INCLUDED
#include <deque>

void taskOne();
void taskTwo();

enum ElementPriority
{
  LOW,
  NORMAL,
  HIGH
};

template <typename Type>
class QueueWithPriority
{
  public:
    void putElementToQueue(const Type& name, ElementPriority priority);
    Type getElementFromQueue();
    void accelerate();
    bool empty();

  private:
    std::deque<Type> dequeWithPriority[3];
};

#endif
