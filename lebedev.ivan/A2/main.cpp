#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

int main()
{
  lebedev::Circle circle(8.0, { 1.0, 6.0 });

  lebedev::Rectangle rectangle(10, 2, { 2.0, 3.0 });

  lebedev::Triangle triangle(lebedev::point_t{ 4, 3 }, lebedev::point_t{ 5, 4 }, lebedev::point_t{ 2, 6 });

  //lebedev::point_t polyShape[] = { { -2.0, -2.0 }, { -2.0, 2.0 }, { 2.0, 2.0 }, { 2.0, -2.0 } };
  lebedev::point_t polyShape[] = { {4, 5}, { 6, 4 }, { 7, -1 }, { 5, -3 }, { 2, 1 } };
  //point_t polyShape[] = { {}, {} }; //Test for wrong input
  int pointQuantity = sizeof(polyShape) / sizeof(polyShape[0]);

  lebedev::Polygon polygon(pointQuantity, polyShape);

  lebedev::Shape * figures[] = { &circle, &rectangle, &triangle, &polygon };

  for (lebedev::Shape * shape : figures)
  {
    shape->showInfo();
    shape->move({ 4.0, 5.0 });
    shape->showInfo();
    shape->scale(10);
    shape->showInfo();
  }
  system("pause");
  return 0;
}
