#include <iostream>
#include <vector>
#include <algorithm>

#include "functor.hpp"

void taskOne()
{
  std::vector<int> vec;
  int num = 0;

  while (std::cin >> num || !std::cin.eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Bad Input\n");
    }
    vec.push_back(num);
  }

  Functor func;
  func = std::for_each(vec.begin(), vec.end(), Functor());
  func.showAll();
}
