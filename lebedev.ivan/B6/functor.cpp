#include "functor.hpp"
#include <iostream>

Functor::Functor() :
  max_(0),
  min_(0),
  mean_(0),
  countPositive_(0),
  countNegative_(0),
  sumOdd_(0),
  sumEven_(0),
  first_(0),
  coincides_(true),
  countNumbers_(0)
{ }

void Functor::operator()(int num)
{
  if ((countPositive_ == 0) && (countNegative_ == 0))
  {
    min_ = num;
    max_ = num;
    first_ = num;
  }
  countNumbers_++;
  if (max_ < num)
  {
    max_ = num;
  }
  else if (min_ > num)
  {
    min_ = num;
  }
  if (num > 0)
  {
    countPositive_++;
  }
  else if (num < 0)
  {
    countNegative_++;
  }
  if (num % 2 == 0)
  {
    sumEven_ += num;
  }
  else sumOdd_ += num;

  coincides_ = (first_ == num) ? true : false;
  mean_ = (static_cast<double>(sumEven_) + static_cast<double>(sumOdd_)) / static_cast<double>(countNumbers_);
}

void Functor::showAll() const
{
  if (countNumbers_ == 0)
  {
    std::cout << "No Data\n";
    return;
  }
  std::cout << "Max: " << max_ << "\n";
  std::cout << "Min: " << min_ << "\n";
  std::cout << "Mean: " << mean_ << "\n";
  std::cout << "Positive: " << countPositive_ << "\n";
  std::cout << "Negative: " << countNegative_ << "\n";
  std::cout << "Odd Sum: " << sumOdd_ << "\n";
  std::cout << "Even Sum: " << sumEven_ << "\n";
  std::string str = (coincides_) ? "yes" : "no";
  std::cout << "First/Last Equal: " << str << "\n";
}
