#include <iostream>

void taskOne();

int main()
{
  try {
    taskOne();
  }
  catch (const std::exception &err)
  {
    std::cerr << err.what();
    return 1;
  }
  return 0;
}
