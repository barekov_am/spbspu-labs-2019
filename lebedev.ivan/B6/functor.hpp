#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <cstdio>

class Functor
{
  public:
    Functor();
    void operator()(int num);
    void showAll() const;

  private:
    int max_;
    int min_;
    double mean_;
    size_t countPositive_;
    size_t countNegative_;
    long int sumOdd_;
    long int sumEven_;
    int first_;
    bool coincides_;
    size_t countNumbers_;
};

#endif
