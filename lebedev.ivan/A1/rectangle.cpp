#include "rectangle.hpp"
#include <cassert>
#include <iostream>


Rectangle::Rectangle(const double height, const double width, const point_t &center) :
  height_(height),
  width_(width),
  center_(center)
{
  assert((height_ > 0.0) && (width_ > 0.0));
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return { height_, width_, center_ };
}

void Rectangle::move(const point_t &point_n)
{
  center_.x = point_n.x;
  center_.y = point_n.y;
}

void Rectangle::move(const double x, const double y)
{
  center_.x += x;
  center_.y += y;
}

void Rectangle::showInfo() const
{
  std::cout << "Area: " << getArea()
      << " height: " << getFrameRect().height
      << " width: " << getFrameRect().width
      << " pos.x " << getFrameRect().pos.x
      << " pos.y " << getFrameRect().pos.y << std::endl;
}
