#ifndef IL_A1_BASETYPES
#define IL_A1_BASETYPES

struct point_t
{
  double x, y;
};

struct rectangle_t
{
  double height, width;
  point_t pos;
};

#endif
