#ifndef IL_A1_CIRCLE
#define IL_A1_CIRCLE
#include "shape.hpp"


class Circle : public Shape
{
  public:
    Circle(const double r, const point_t &);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(const double x, const double y) override;
    void showInfo() const override;

  private:
    double radius_;
    point_t center_;
};

#endif
