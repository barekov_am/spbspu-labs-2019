#ifndef IL_A1_RECTANGLE
#define IL_A1_RECTANGLE
#include "shape.hpp"

class Rectangle : public Shape
{
  public:
    Rectangle(const double a, const double b, const point_t &);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(const double x, const double y) override;
    void showInfo() const override;

  private:
    double height_, width_;
    point_t center_;
};

#endif
