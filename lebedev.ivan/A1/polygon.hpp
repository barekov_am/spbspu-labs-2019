#ifndef IL_A1_POLYGON
#define IL_A1_POLYGON
#include "shape.hpp"

class Polygon : public Shape
{
  public:
    Polygon(const int dimension, const point_t *);
   
    Polygon(const Polygon &);
    Polygon(Polygon &&) noexcept;
    ~Polygon() override;

    Polygon & operator= (const Polygon &);
    Polygon & operator= (Polygon &&);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(const double x, const double y) override;
    void showInfo() const override;

  private:
    int dim_;
    point_t * points_;
    point_t center_;

    void swap(Polygon &) noexcept;
};

#endif
