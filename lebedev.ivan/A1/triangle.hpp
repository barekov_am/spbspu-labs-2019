#ifndef IL_A1_TRIANGLE
#define IL_A1_TRIANGLE
#include "shape.hpp"

class Triangle : public Shape
{
  public:
    Triangle (const point_t &, const point_t &, const point_t &);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(const double x, const double y) override;
    void showInfo() const override;

  private:
    point_t a_, b_, c_, center_;
};

#endif
