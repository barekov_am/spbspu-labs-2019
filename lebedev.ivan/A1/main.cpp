#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

int main()
{
  Circle circle(8.0, { 1.0, 6.0 });

  Rectangle rectangle(10, 2, { 2.0, 3.0 });

  Triangle triangle(point_t{ 4, 3 }, point_t{ 5, 4 }, point_t{ 2, 6 });

  point_t polyShape[] = { { -2.0, -2.0 }, { -2.0, 2.0 }, { 2.0, 2.0 }, { 2.0, -2.0 } };
  //point_t polyShape[] = { {}, {} }; //Test for wrong input
  int pointQuantity = sizeof(polyShape) / sizeof(polyShape[0]);

  Polygon polygon(pointQuantity, polyShape);

  Shape * figures[] = { &circle, &rectangle, &triangle, &polygon };

  for (Shape * shape : figures)
  {
    shape->showInfo();
    shape->move({ 3.0, -2.0 });
    shape->showInfo();
  }
  
  return 0;
}
