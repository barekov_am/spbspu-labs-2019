#define _USE_MATH_DEFINES
#include "circle.hpp"
#include <cassert>
#include <cmath>
#include <iostream>


Circle::Circle(const double rad, const point_t &center) :
  radius_(rad),
  center_(center)
{
  assert(radius_ > 0.0);
}

double Circle::getArea() const
{
  return (M_PI * radius_ * radius_);
}

rectangle_t Circle::getFrameRect() const
{
  return { 2 * radius_, 2 * radius_, center_ };
}

void Circle::move(const point_t &point_n)
{
  center_.x = point_n.x;
  center_.y = point_n.y;
}

void Circle::move(const double x, const double y)
{
  center_.x += x;
  center_.y += y;
}

void Circle::showInfo() const
{
  std::cout << "Area: " << getArea()
      << " height: " << getFrameRect().height
      << " width: " << getFrameRect().width
      << " pos.x " << getFrameRect().pos.x
      << " pos.y " << getFrameRect().pos.y << std::endl;
}
