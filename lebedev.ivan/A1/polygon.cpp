#include "polygon.hpp"
#include <ctime>
#include <iostream>
#include <cassert>
#include <stdexcept>

Polygon::Polygon(const int dimension, const point_t * points)
{
  assert( ( dimension > 2 ) && ( points != nullptr ) );
  dim_ = dimension;
  points_ = new point_t[dimension];
  double sumx = 0, sumy = 0;
  for (int i = 0; i < dimension; i++)
  {
    points_[i] = points[i];
    sumx += points_[i].x;
    sumy += points_[i].y;
  }
  center_ = { sumx / dimension, sumy / dimension };
}

Polygon::Polygon(const Polygon& another) :
  dim_(another.dim_),
  points_(new point_t[dim_])
{
  for (int i = 0; i < dim_; i++)
  {
    points_[i] = another.points_[i];
  }
}

Polygon::Polygon(Polygon&& another) noexcept :
  dim_(0),
  points_(nullptr)
{
  swap(another);
}

Polygon::~Polygon()
{
  delete[] points_;
}

Polygon &Polygon::operator= (const Polygon & another)
{
  if (&another != this)
  {
    Polygon temp(another);
    swap(temp);
  }
  return *this;
}

Polygon &Polygon::operator=(Polygon && another)
{
  if (&another != this)
  {
    swap(another);
    delete[] another.points_;
    another.dim_ = 0;
    another.points_ = nullptr;
  }
  return *this;
}

double Polygon::getArea() const
{
  if ((points_ == nullptr) || (dim_ <= 2))
  {
    return 0;
  }
  double sum_pos = 0, sum_neg = 0;
  for (int i = 0; i < dim_ - 1; i++)
  {
    sum_pos += points_[i].x * points_[i + 1].y;
    sum_neg += points_[i + 1].x * points_[i].y;
  }
  double area = abs(sum_pos + points_[dim_ - 1].x * points_[0].y
      - sum_neg - points_[0].x * points_[dim_ - 1].y) / 2;
  return area;
}

rectangle_t Polygon::getFrameRect() const
{
  double min_x = points_[0].x;
  double max_x = points_[0].x;
  double min_y = points_[0].y;
  double max_y = points_[0].y;

  for (int i = 1; i < dim_; i++)
  {
    if (points_[i].x < min_x)
    {
      min_x = points_[i].x;
    }
    if (points_[i].x > max_x)
    {
      max_x = points_[i].x;
    }
    if (points_[i].y < min_y)
    {
      min_y = points_[i].x;
    }
    if (points_[i].y > max_y)
    {
      max_y = points_[i].y;
    }
  }

  double width = abs(max_x - min_x);
  double height = abs(max_y - min_y);

  point_t center = { (max_x - min_x) / 2 + min_x, (max_y - min_y) / 2 + min_y };

  return { height, width, center };
}

void Polygon::move(const point_t &point_n)
{
  double shift_x = point_n.x - center_.x;
  double shift_y = point_n.y - center_.y;

  for (int i = 0; i < dim_; i++)
  {
    points_[i].x += shift_x;
    points_[i].y += shift_y;
  }

  center_ = { point_n.x, point_n.y };
}

void Polygon::move(const double shift_x, const double shift_y)
{
  center_.x += shift_x;
  center_.y += shift_y;

  for (int i = 0; i < dim_; i++)
  {
    points_[i].x += shift_x;
    points_[i].y += shift_y;
  }
}

void Polygon::showInfo() const
{
  std::cout << "Area: " << getArea()
      << " height: " << getFrameRect().height
      << " width: " << getFrameRect().width
      << " pos.x " << getFrameRect().pos.x
      << " pos.y " << getFrameRect().pos.y << std::endl;
}

void Polygon::swap(Polygon& another) noexcept
{
  std::swap(dim_, another.dim_);
  std::swap(points_, another.points_);
}
