#ifndef IL_A1_SHAPE
#define IL_A1_SHAPE
#include "base-types.hpp"

class Shape
{
  public:
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t &) = 0;
    virtual void move(const double x, const double y) = 0;
    virtual void showInfo() const = 0;
    virtual ~Shape() = default;
};

#endif
