#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separation.hpp"


int main()
{
  lebedev::Circle circle(8.0, { 1.0, 6.0 });

  lebedev::Rectangle rectangle(10, 2, { 2.0, 3.0 });

  lebedev::Triangle triangle(lebedev::point_t{ 4, 3 }, lebedev::point_t{ 5, 4 }, lebedev::point_t{ 2, 6 });

  //lebedev::point_t polyShape[] = { { -2.0, -2.0 }, { -2.0, 2.0 }, { 2.0, 2.0 }, { 2.0, -2.0 } };
  lebedev::point_t polyShape[] = { {4, 5}, { 6, 4 }, { 7, -1 }, { 5, -3 }, { 2, 1 } };
  //point_t polyShape[] = { {}, {} }; //Test for wrong input
  int pointQuantity = sizeof(polyShape) / sizeof(polyShape[0]);

  lebedev::Polygon polygon(pointQuantity, polyShape);

  lebedev::shapePtr rectanglePtr = std::make_shared<lebedev::Rectangle>(rectangle);
  lebedev::shapePtr circlePtr = std::make_shared<lebedev::Circle>(circle);

  lebedev::CompositeShape compositeShape(rectanglePtr);
  compositeShape.showInfo();

  compositeShape.add(circlePtr);
  compositeShape.add(rectanglePtr);
  std::cout << "Figures have been added" << std::endl;
  compositeShape.showInfo();

  compositeShape.move(4.5, 1.5);
  std::cout << "Composite shape has been moved (x or y)" << std::endl;
  compositeShape.showInfo();

  compositeShape.move({ 12.0, 4 });
  std::cout << "Composite shape has been moved by coordinates" << std::endl;
  compositeShape.printInfo();

  compositeShape.scale(4);
  std::cout << "Composite shape has been scaled" << std::endl;
  compositeShape.showInfo();

  compositeShape.remove(0);
  std::cout << "First figure in composite shape has been deleted" << std::endl;
  compositeShape.showInfo();

  lebedev::shapePtr trianglePtr = std::make_shared<lebedev::Triangle>(triangle);
  lebedev::Circle newCircle(4, { 30, -13 });
  lebedev::shapePtr newCirclePtr = std::make_shared<lebedev::Circle>(newCircle);

  compositeShape.add(trianglePtr);
  compositeShape.add(newCirclePtr);

  lebedev::Matrix matrix = lebedev::part(compositeShape);
  std::cout << "matrix" << std::endl;
  std::cout << "rows " << matrix.getLines() << std::endl;
  std::cout << "columns " << matrix.getColumns() << std::endl;

  lebedev::Shape * figures[] = { &circle, &rectangle, &triangle, &polygon };

  for (lebedev::Shape * shape : figures)
  {
    shape->showInfo();
    shape->move({ 4.0, 5.0 });
    shape->showInfo();
    shape->scale(10);
    shape->showInfo();
  }

  return 0;
}
