#include <cstring>
#include <sstream>
#include <iostream>
#include "details.hpp"

BookmarkInterface::InsertLoc getInsertLocation(const char* loc)
{
  if (std::strcmp(loc, "after") == 0)
  {
    return BookmarkInterface::InsertLoc::after;
  }

  if (std::strcmp(loc, "before") == 0)
  {
    return BookmarkInterface::InsertLoc::before;
  }

  throw std::invalid_argument("Invalid location");
}

BookmarkInterface::MoveLoc getMoveLocation(const char* loc)
{
  if (std::strcmp(loc, "first") == 0)
  {
    return BookmarkInterface::MoveLoc::first;
  }

  if (std::strcmp(loc, "last") == 0)
  {
    return BookmarkInterface::MoveLoc::last;
  }

  throw std::invalid_argument("Invalid location");
}

std::string getName(std::string& name)
{
  if (name.empty())
  {
    throw std::invalid_argument("Invalid name");
  }

  if (name.front() != '\"')
  {
    throw std::invalid_argument("Invalid name");
  }

  name.erase(name.begin());

  size_t i = 0;
  while ((i < name.size()) && (name[i] != '\"'))
  {
    if (name[i] == '\\')
    {
      if ((name[i + 1] == '\"') && (i + 2 < name.size()))
      {
        name.erase(i, 1);
      }
      else
      {
        throw std::invalid_argument("Invalid name");
      }
    }
    ++i;
  }

  if (i == name.size())
  {
    throw std::invalid_argument("Invalid name");
  }

  name.erase(i);

  if (name.empty())
  {
    throw std::invalid_argument("Invalid name");
  }
  return name;
}

std::string getNumber(std::string& number)
{
  if (number.empty())
  {
    throw std::invalid_argument("Invalid number");
  }

  for (size_t i = 0; i < number.size(); i++)
  {
    if (!std::isdigit(number[i]))
    {
      throw std::invalid_argument("Invalid number");
    }
  }
  return number;
}

std::string getBookmarkName(std::string& bookmarkName)
{
  if (bookmarkName.empty())
  {
    throw std::invalid_argument("Invalid bookmark name");
  }

  for (size_t i = 0; i < bookmarkName.size(); i++)
  {
    if (!isalnum(bookmarkName[i]) && (bookmarkName[i] != '-'))
    {
      throw std::invalid_argument("Invalid bookmark name");
    }
  }
  return bookmarkName;
}


void executeAdd(BookmarkInterface& Interface, std::stringstream& str)
{
  try
  {
    std::string number;
    str >> std::ws >> number;
    number = getNumber(number);
    std::string name;
    std::getline(str >> std::ws, name);
    name = getName(name);
    Interface.add({ number, name });
  }

  catch (std::invalid_argument&)
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}

void executeStore(BookmarkInterface& Interface, std::stringstream& str)
{
  try
  {
    std::string bookmarkName;
    str >> std::ws >> bookmarkName;
    bookmarkName = getBookmarkName(bookmarkName);
    std::string newBookmarkName;
    str >> std::ws >> newBookmarkName;
    newBookmarkName = getBookmarkName(newBookmarkName);
    Interface.store(bookmarkName, newBookmarkName);
  }

  catch (std::invalid_argument&)
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}

void executeInsert(BookmarkInterface& Interface, std::stringstream& str)
{
  try
  {
    std::string locate;
    str >> std::ws >> locate;
    auto loc = getInsertLocation(locate.c_str());
    std::string bookmarkName;
    str >> std::ws >> bookmarkName;
    bookmarkName = getBookmarkName(bookmarkName);
    std::string number;
    str >> std::ws >> number;
    number = getNumber(number);
    std::string name;
    std::getline(str >> std::ws, name);
    name = getName(name);
    Interface.insert(loc, bookmarkName, { number, name });
  }

  catch (std::invalid_argument&)
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}

void executeDelete(BookmarkInterface& Interface, std::stringstream& str)
{
  try
  {
    std::string bookmarkName;
    str >> std::ws >> bookmarkName;
    bookmarkName = getBookmarkName(bookmarkName);
    Interface.erase(bookmarkName);
  }

  catch (std::invalid_argument&)
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}

void executeShow(BookmarkInterface& Interface, std::stringstream& str)
{
  try
  {
    std::string bookmarkName;
    str >> std::ws >> bookmarkName;
    bookmarkName = getBookmarkName(bookmarkName);
    Interface.show(bookmarkName);
  }

  catch (std::invalid_argument&)
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}

void executeMove(BookmarkInterface& Interface, std::stringstream& str)
{
  try
  {
    std::string bookmarkName;
    str >> std::ws >> bookmarkName;
    bookmarkName = getBookmarkName(bookmarkName);
    std::string steps;
    str >> std::ws >> steps;

    if ((steps.front() == '-') || (steps.front() == '+') || (std::isdigit(steps.front())))
    {
      int sign = 1;

      if (steps.front() == '-')
      {
        sign = -1;
        steps.erase(steps.begin());
      }
      else if (steps.front() == '+')
      {
        steps.erase(steps.begin());
      }
      steps = getNumber(steps);
      Interface.move(bookmarkName, std::stoi(steps) * sign);
    }
    else
    {
      auto loc = getMoveLocation(steps.c_str());
      Interface.move(bookmarkName, loc);
    }
  }

  catch (std::invalid_argument&)
  {
  std::cout << "<INVALID STEP>\n";
  return;
  }
}
