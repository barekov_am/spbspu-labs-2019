#include "details.hpp"

const int maxNum = 10;

FactorialIterator::FactorialIterator() :
  value_(1),
  index_(1)
{
}

FactorialIterator::FactorialIterator(int index) :
  value_(getValue(index)),
  index_(index)
{
}

FactorialIterator::reference FactorialIterator::operator *()
{
  return value_;
}

FactorialIterator::pointer FactorialIterator::operator ->()
{
  return &value_;
}

FactorialIterator& FactorialIterator::operator ++()
{
  index_++;
  value_ = value_ * index_;

  return *this;
}

FactorialIterator FactorialIterator::operator ++(int)
{
  FactorialIterator tmp = *this;
  ++(*this);

  return tmp;
}

FactorialIterator& FactorialIterator::operator --()
{
  if (index_ > 1)
  {
    value_ /= index_;
    --index_;
  }

  return *this;
}

FactorialIterator FactorialIterator::operator --(int)
{
  FactorialIterator tmp = *this;
  --(*this);
  return tmp;
}

bool FactorialIterator::operator ==(const FactorialIterator& rhs) const
{
  return ((value_ == rhs.value_) && (index_ == rhs.index_));
}

bool FactorialIterator::operator !=(const FactorialIterator& rhs) const
{
  return !(*this == rhs);
}

long long FactorialIterator::getValue(int index) const
{
  if (index <= 1)
  {
    return 1;
  }
  else
  {
    return (index * getValue(index - 1));
  }
}

FactorialIterator FactorialContainer::begin()
{
  return FactorialIterator(1);
}

FactorialIterator FactorialContainer::end()
{
  return FactorialIterator(maxNum + 1);
}
