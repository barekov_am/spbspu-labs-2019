#ifndef DETAILS_HPP
#define DETAILS_HPP
#include <list>
#include <string>
#include <map>
#include <iterator>

class Phonebook
{
  public:
    struct record_t
    {
      std::string name;
      std::string number;
    };
  using container = std::list<record_t>;
  using iterator = container::iterator;
  void view(iterator) const;
  void pushBack(const record_t&);
  bool empty() const;
  iterator begin();
  iterator end();
  iterator next(iterator);
  iterator prev(iterator);
  iterator insert(iterator, const record_t&);
  iterator erase(iterator);
  iterator move(iterator, int);
  private:
    container cont_;
};

class BookmarkInterface
{
  public:
    enum class MoveLoc
    {
      first,
      last
    };
  enum class InsertLoc
  {
    before,
    after
  };

  BookmarkInterface();

  void add(const Phonebook::record_t&);
  void store(const std::string&, const std::string&);
  void insert(InsertLoc, const std::string&, const Phonebook::record_t&);
  void erase(const std::string&);
  void show(const std::string&);
  void move(const std::string&, int);
  void move(const std::string&, MoveLoc);

  private:
    using bookmarks = std::map<std::string, Phonebook::iterator>;
    Phonebook records_;
    bookmarks bookmarks_;
    bookmarks::iterator getBookmarkIterator(const std::string&);
};

void executeAdd(BookmarkInterface&, std::stringstream&);
void executeDelete(BookmarkInterface&, std::stringstream&);
void executeStore(BookmarkInterface&, std::stringstream&);
void executeMove(BookmarkInterface&, std::stringstream&);
void executeInsert(BookmarkInterface&, std::stringstream&);
void executeShow(BookmarkInterface&, std::stringstream&);

class FactorialIterator : public std::iterator<std::bidirectional_iterator_tag, long long>
{
  public:
    FactorialIterator();
    FactorialIterator(int index);
    long long& operator *();
    long long* operator ->();
    FactorialIterator& operator ++();
    FactorialIterator operator ++(int);
    FactorialIterator& operator --();
    FactorialIterator operator --(int);
    bool operator ==(const FactorialIterator&) const;
    bool operator !=(const FactorialIterator&) const;
  private:
    long long value_;
    int index_;
    long long getValue(int) const;
};

class FactorialContainer
{
  public:
    FactorialContainer() = default;
    FactorialIterator begin();
    FactorialIterator end();
};

#endif
