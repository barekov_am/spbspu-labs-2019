#include <iostream>
#include <string>

void taskOne();
void taskTwo();

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Invalid number of arguments";

      return 1;
    }

    int num = std::stoi(argv[1]);

    switch (num)
    {
      case 1:
      {
        taskOne();
        break;
      }
      case 2:
      {
        taskTwo();
        break;
      }
      default:
      {
        std::cerr << "Invalid task number";
         return 1;
      }
    }
  }
  catch (const std::exception & ex)
  {
    std::cerr << ex.what();
    return 2;
  }
  return 0;
}
