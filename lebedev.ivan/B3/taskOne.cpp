#include <iostream>
#include <cstring>
#include <functional>
#include <algorithm>
#include <sstream>
#include "details.hpp"

struct command_t
{
  const char* name;
  std::function<void(BookmarkInterface&, std::stringstream&)> execute;
};

void taskOne()
{
  BookmarkInterface Interface;
  std::string line;

  while (getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Invalid Input");
    }

    std::stringstream str(line);
    std::string command;
    str >> command;

    if (strcmp("add", command.c_str()) == 0)
    {
      executeAdd(Interface, str);
    }
    else if (strcmp("delete", command.c_str()) == 0)
    {
      executeDelete(Interface, str);
    }
    else if (strcmp("store", command.c_str()) == 0)
    {
      executeStore(Interface, str);
    }
    else if (strcmp("move", command.c_str()) == 0)
    {
      executeMove(Interface, str);
    }
    else if (strcmp("insert", command.c_str()) == 0)
    {
      executeInsert(Interface, str);
    }
    else if (strcmp("show", command.c_str()) == 0)
    {
      executeShow(Interface, str);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
