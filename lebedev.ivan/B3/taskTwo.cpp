#include <iostream>
#include <algorithm>
#include "details.hpp"

void taskTwo()
{
  FactorialContainer factContainer;

  std::copy(factContainer.begin(), factContainer.end(), std::ostream_iterator<long long>(std::cout, " "));
  std::cout << std::endl;

  std::reverse_copy(factContainer.begin(), factContainer.end(), std::ostream_iterator<long long>(std::cout, " "));
  std::cout << std::endl;
}
