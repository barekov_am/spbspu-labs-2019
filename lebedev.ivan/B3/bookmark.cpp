#include <algorithm>
#include <iostream>
#include "details.hpp"

BookmarkInterface::BookmarkInterface()
{
  bookmarks_["current"] = records_.begin();
}

void BookmarkInterface::store(const std::string& bookmark, const std::string& newBookmark)
{
  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {
    bookmarks_.emplace(newBookmark, iter->second);
  }
}

void BookmarkInterface::insert(BookmarkInterface::InsertLoc loc, const std::string& bookmark,
  const Phonebook::record_t& rec)
{
  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {
    if (iter->second == records_.end())
    {
      add(rec);
    }
    if (loc == InsertLoc::after)
    {
      records_.insert(std::next(iter->second), rec);
    }
    else
    {
      records_.insert(iter->second, rec);
    }
  }
}

void BookmarkInterface::add(const Phonebook::record_t& rec)
{
  records_.pushBack(rec);

  if (std::next(records_.begin()) == records_.end())
  {
    bookmarks_["current"] = records_.begin();
  }
}

void BookmarkInterface::erase(const std::string& bookmark)
{
  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {
    auto eraseIter = iter->second;
    auto find = [&](auto& it)
    {
      if (it.second == eraseIter)
      {
        if (std::next(it.second) == records_.end())
        {
          it.second = records_.prev(eraseIter);
        }
        else
        {
          it.second = records_.next(eraseIter);
        }
      }
    };
    std::for_each(bookmarks_.begin(), bookmarks_.end(), find);
    records_.erase(eraseIter);
  }
}

void BookmarkInterface::show(const std::string& bookmark)
{
  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {
    if (records_.empty())
    {
      std::cout << "<EMPTY>\n";
      return;
    }
    records_.view(iter->second);
  }
}

void BookmarkInterface::move(const std::string& bookmark, int n)
{
  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {
    iter->second = records_.move(iter->second, n);
  }
}

void BookmarkInterface::move(const std::string& bookmark, MoveLoc loc)
{
  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {
    if (loc == MoveLoc::first)
    {
      iter->second = records_.begin();
    }
    if (loc == MoveLoc::last)
    {
      iter->second = records_.prev(records_.end());
    }
  }
}

BookmarkInterface::bookmarks::iterator BookmarkInterface::getBookmarkIterator(const std::string& bookmark)
{
  auto iter = bookmarks_.find(bookmark);

  if (iter != bookmarks_.end())
  {
    return iter;
  }
  else
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return bookmarks_.end();
  }
}
