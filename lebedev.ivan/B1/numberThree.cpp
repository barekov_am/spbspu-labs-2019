#include "details.hpp"
#include <iostream>
#include <stdexcept>
#include <fstream>
#include <vector>
#include <iterator>

void taskThree()
{
  std::vector<int> vector;
  int i = 0;

  while (std::cin >> i && i != 0)
  {
    vector.push_back(i);
  }

  if (std::cin.fail() && (!std::cin.eof() || i != 0))
  {
    throw std::ios_base::failure("Reading data fail");
  }
  if (vector.empty())
  {
    return;
  }
  std::vector<int>::iterator iter = vector.begin();

  if (vector.back() == 1)
  {
    while (iter != vector.end())
    {
      if (*iter % 2 == 0)
      {
        iter = vector.erase(iter);
      }
      else
      {
        iter++;
      }
    }
  }
  else if (vector.back() == 2)
  {
    while (iter != vector.end())
    {
      if (*iter % 3 == 0)
      {
        iter = vector.insert(++iter,3,1);
        std::advance(iter, 2);
      }
      iter++;
    }
  }

  detail::print(vector);
}
