#include "details.hpp"
#include <cstdlib>
#include <stdexcept>

int main(int argc, char * argv[])
{
  try
  {
    if ((argc == 1) || (argc >= 5))
    {
      throw std::invalid_argument("Invalid number of arguments");
    }
    switch (atoi(argv[1]))
    {
      case 1:
      {
        if (argc != 3)
        {
          throw std::invalid_argument("Invalid number of parameters for part 1");
        }
        taskOne(argv[2]);
        break;
      }
      case 2:
      {
        if (argc != 3)
        {
          throw std::invalid_argument("Invalid number of parameters for part 2");
        }
        taskTwo(argv[2]);
        break;
      }
      case 3:
      {
        if (argc != 2)
        {
          throw std::invalid_argument("Invalid number of parameters for part 3");
        }
        taskThree();
        break;
      }
      case 4:
      {
        if (argc != 4)
        {
          throw std::invalid_argument("Invalid number of parameters for part 4");
        }
        taskFour(argv[2], atoi(argv[3]));
        break;
      }
      default:
      {
        throw std::invalid_argument("Invalid number of part");
        break;
      }
    }
  }
  catch (std::ios_base::failure & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 1;
  }
  catch (std::invalid_argument & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cerr << "Undefined exception" << std::endl;
    return 1;
  }

  return 0;
}
