#include "details.hpp"
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <vector>
#include <memory>

void taskTwo(char *filename)
{
  std::ifstream file(filename);

  if (!file)
  {
    throw std::invalid_argument("File does not open");
  }

  size_t bufSize = 20;
  size_t count = 0;
  std::unique_ptr<char[]> buffer(new char[bufSize]);
  char ch = 0;
  file >> std::noskipws;
  while (file >> ch)
  {
    if (count < bufSize)
    {
      buffer[count] = ch;
    }
    else
    {
      std::unique_ptr<char[]> temp(new char[bufSize+100]);
      for (size_t i = 0; i < bufSize; i++)
      {
        temp[i] = buffer[i];
      }
      buffer.swap(temp);
      buffer[count] = ch;
      bufSize += 100;
    }
    count++;
  }
  if (file.bad())
  {
    throw std::ios_base::failure("Reading data fail");
  }

  std::vector<char> vector(buffer.get(), buffer.get() + count);

  for(auto it = vector.begin(); it != vector.end(); it++)
  {
    std::cout << *it;
  }
}
