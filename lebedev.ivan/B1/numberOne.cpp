#include "details.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <forward_list>

void taskOne(char *way)
{
  std::vector<int> vector;
  int i = 0;
  while (std::cin >> i)
  {
    vector.push_back(i);
  }
  if (!std::cin.eof())
  {
   throw std::ios_base::failure("Reading data fail");
  }

  std::vector<int> vector2(vector);
  std::forward_list<int> list(vector.begin(), vector.end());

  detail::sort<detail::withOper>(vector, way);
  detail::sort<detail::withAt>(vector2, way);
  detail::sort<detail::withIter>(list, way);
  detail::print(vector);
  detail::print(vector2);
  detail::print(list);
}
