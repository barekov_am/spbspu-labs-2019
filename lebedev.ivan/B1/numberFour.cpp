#include "details.hpp"
#include <vector>
#include <iostream>
#include <random>
#include <stdexcept>

void fillRandom(double *array, int size)
{
  std::random_device random;
  std::mt19937 gen(random());
  std::uniform_real_distribution<double> range(-1, 1);

  for (int i = 0; i < size; i++)
  {
    array[i] = range(gen);
  }
}

void taskFour(char *direction, size_t size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Invalid length");
  }
  std::vector<double> vector(size, 0);
  fillRandom(&vector[0], size);
  detail::print(vector);
  detail::sort<detail::withAt>(vector, direction);
  detail::print(vector);
}
