#ifndef DETAILS_HPP_INCLUDED
#define DETAILS_HPP_INCLUDED
#include <iostream>
#include <cstring>

void taskOne(char *direction);
void taskTwo(char *name);
void taskThree();
void taskFour(char *direction, size_t size);

namespace detail
{
  template<typename Type>
  void print(Type &container)
  {
    for (typename Type::iterator i = container.begin(); i != container.end(); i++)
    {
      std::cout << *i << " ";
    }
    std::cout << std::endl;
  }

  template<typename C>
  struct withOper
  {
    typedef typename C::size_type index_type;
    static index_type getBegin(C &);
    static index_type getEnd(C &container);
    static typename C::reference getElem(C &container, index_type i);
  };

  template<typename C>
  struct withAt
  {
    typedef typename C::size_type index_type;
    static index_type getBegin(C &);
    static index_type getEnd(C &container);
    static typename C::reference getElem(C &container, index_type i);

  };

  template<typename C>
  struct withIter
  {
    typedef typename C::iterator index_type;
    static index_type getBegin(C &container);
    static index_type getEnd(C &container);
    static typename C::reference getElem(C &, index_type i);
  };

  template<typename C>
  typename withOper<C>::index_type withOper<C>::getBegin(C &)
  {
      return 0;
  }

  template<typename C>
  typename withOper<C>::index_type withOper<C>::getEnd(C &container)
  {
      return container.size();
  }

  template<typename C>
  typename C::reference withOper<C>::getElem(C &container, withOper<C>::index_type i)
  {
    return container[i];
  }

  template<typename C>
  typename withAt<C>::index_type withAt<C>::getBegin(C &)
  {
    return 0;
  }

  template<typename C>
  typename withAt<C>::index_type withAt<C>::getEnd(C &container)
  {
    return container.size();
  }

  template<typename C>
  typename C::reference withAt<C>::getElem(C &container, withAt<C>::index_type i)
  {
    return container.at(i);
  }

  template<typename C>
  typename withIter<C>::index_type withIter<C>::getBegin(C &container)
  {
    return container.begin();
  }

  template<typename C>
  typename withIter<C>::index_type withIter<C>::getEnd(C &container)
  {
    return container.end();
  }

  template<typename C>
  typename C::reference withIter<C>::getElem(C &, withIter<C>::index_type i)
  {
    return *i;
  }

  template<typename Type>
  bool compareL(Type a, Type b)
  {
    return a < b;
  }

  template<typename Type>
  bool compareH(Type a, Type b)
  {
    return a > b;
  }

  template<template <typename Container> typename Access, typename Container>
  void sort(Container &C, char *direction)
  {
    bool (*comparator)(typename Container::value_type, typename Container::value_type) = nullptr;

    if (std::strcmp("ascending", direction) == 0)
    {
      comparator = compareH;
    }
    else if (std::strcmp("descending", direction) == 0)
    {
      comparator = compareL;
    }
    else
    {
      throw std::invalid_argument("Incorrect sorting direction");
    }

    for (typename Access<Container>::index_type i = Access<Container>::getBegin(C); i != Access<Container>::getEnd(C); i++)
    {
      for (typename Access<Container>::index_type j = i; j != Access<Container>::getEnd(C); j++)
      {
        if (comparator(Access<Container>::getElem(C,i),Access<Container>::getElem(C,j)))
        {
          std::swap(Access<Container>::getElem(C,i), Access<Container>::getElem(C,j));
        }
      }
    }
  }
}
#endif

