#include <iostream>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

void showRectangle_t(lebedev::rectangle_t data)
{
  std::cout << "Width = " << data.width << '\n';
  std::cout << "Height = " << data.height << '\n';
  std::cout << "(X = " << data.pos.x << ";";
  std::cout << "Y = " << data.pos.y << ")" << '\n';
}

void showAll(const lebedev::Shape *shape)
{
  if (shape != nullptr)
  {
    std::cout << "Area = " << shape->getArea() << '\n';
    showRectangle_t(shape->getFrameRect());
  }
  else
  {
    throw std::invalid_argument("Shape's pointer is null!!!");
  }
}

int main()
{
  lebedev::CompositeShape compShape;
  lebedev::point_t arrayVetex[4] = {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
  lebedev::point_t arrayVetex1[4] = {{10.0, 1.0}, {6.0, 1.0}, {7.0, 4.0}, {12.0, 4.0}};
  
  lebedev::CompositeShape::ptr_shape ptrRect = std::make_shared<lebedev::Rectangle>(arrayVetex);
  lebedev::CompositeShape::ptr_shape ptrRect1 = std::make_shared<lebedev::Rectangle>(arrayVetex1);
  lebedev::CompositeShape::ptr_shape ptrCirc = std::make_shared<lebedev::Circle>(2, lebedev::point_t {2.0, 5.0});
  lebedev::CompositeShape::ptr_shape ptrCirc1 = std::make_shared<lebedev::Circle>(3, lebedev::point_t {6.0, 7.0});
  compShape.add(ptrCirc);
  
  std::cout << "Composite Shape" << '\n';
  showAll(&compShape);
  const double dx = 1.0;
  const double dy = 2.0;
  compShape.move(dx, dy);
  std::cout << "Move to " << "(X = " << dx << "; Y = " << dy << ")" << '\n';
  showAll(&compShape);
  lebedev:: point_t newPos = {7.0, 6.0};
  compShape.move(newPos);
  std::cout << "Move to new pos = " << "(X = " << newPos.x << "; Y = " << newPos.y << ")" << '\n';
  showAll(&compShape);

  const double multiplier = 2.0;
  compShape.scale(multiplier);
  std::cout << "After scaling on multiplier = " << multiplier << '\n';
  showAll(&compShape);

  std::cout << "Before add quantity of shapes = " << compShape.getQuantity() << '\n';
  compShape.add(ptrCirc1);
  std::cout << "After add quantity of shapes = " << compShape.getQuantity() << '\n';
  std::cout << "qty ompshape after add" << '\n';
  showAll(&compShape);

  std::cout << "Before delete quantity of shapes = " << compShape.getQuantity() << '\n';
  compShape.disposal(0);
  std::cout << "After delete quantity of shapes = " << compShape.getQuantity() << '\n';
  std::cout << "Compshape after delete" << '\n';
  showAll(&compShape);
}
