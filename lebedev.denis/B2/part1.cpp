#include <iostream>
#include <sstream>
#include "queueWithPriority.hpp"

void part1()
{
  QueueWithPriority<std::string> queueWithPriority;
  std::string line;
  while (std::getline(std::cin, line))
  {
    std::istringstream input(line);
    std::string command;
    input >> command;
    if (command == "add")
    {
      Priority priority;
      std::string str;
      input >> str;
      if (str == "high")
      {
        priority = Priority::high;
      }
      else if (str == "normal")
      {
        priority = Priority::normal;
      }
      else if (str == "low")
      {
        priority = Priority::low;
      }
      else
      {
        std::cout << "<INVALID COMMAND>\n";
        continue;
      }
      str = "";
      getline(input >> std::ws, str);
      if (str == "")
      {
        std::cout << "<INVALID COMMAND>\n";
        continue;
      }
      queueWithPriority.addElem(priority, str);
    }
    else if (command == "get")
    {
      if (queueWithPriority.empty())
      {
        std::cout << "<EMPTY>\n";
        continue;
      }
      std::cout << queueWithPriority.getElem() << '\n';
    }
    else if (command == "accelerate")
    {
      queueWithPriority.accelerate();
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
