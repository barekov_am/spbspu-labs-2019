#include <iostream>
#include <stdexcept>


void part1();
void part2();

int main(int argc, char* argv[])
{
  if (argc == 1)
  {
    std::cerr << "Trouble with count of parameters\n";
    return 1;
  }
  try
  {
    size_t numTask = std::stoi(argv[1]);
    switch (numTask)
    {
      case 1:
      {
        part1();
      }
        break;
      case 2:
      {
        part2();
      }
        break;
      default:
      {
        std::cerr << "Trouble with number of part\n";
        return 1;
      }
    }
  }
  catch (std::exception &err)
  {
    std::cerr << err.what();
    return 1;
  }
  return 0;
}
