#include <iostream>
#include <list>
#include <stdexcept>
#include <iterator>
#include <algorithm>

const int MIN = 1;
const int MAX = 20;
const size_t MAX_COUNT = 20;

void part2()
{
  std::list<int> list_;
  std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), [&list_](int num)
      {
        if ((num < MIN) || (num > MAX))
        {
          throw std::invalid_argument("Incorrect Data\n");
        }
        list_.push_back(num);
      });

  if ((!std::cin.eof()) && (std::cin.fail()))
  {
    throw std::ios_base::failure("Incorrect reading\n");
  }

  if (list_.size() > MAX_COUNT)
  {
    throw std::invalid_argument("Incorrect size of Data\n");
  }

  if (list_.empty())
  {
    return;
  }

  size_t i = 0;
  size_t size = list_.size() / 2;
  auto iterHead = list_.begin();
  auto iterBack = std::prev(list_.end());
  while (i < size)
  {
    std::cout << *iterHead << " " << *iterBack << " ";
    ++iterHead;
    --iterBack;
    ++i;
  }
  if (iterHead == iterBack)
  {
    std::cout << *iterHead;
  }
  std::cout << "\n";
}
