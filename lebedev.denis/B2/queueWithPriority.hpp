#ifndef QUEUEWITHPRIORITY_HPP
#define QUEUEWITHPRIORITY_HPP

#include <string>
#include <list>
#include <stdexcept>

enum Priority
{
  high,
  normal,
  low
};

template <typename T>
class QueueWithPriority
{
public:
  QueueWithPriority();
  void addElem(Priority priority, T &elem);
  T getElem();
  void accelerate();
  bool empty();

private:
  std::list<T> list[3];
};

template <typename T>
QueueWithPriority<T>::QueueWithPriority():
  list()
{}

template <typename T>
void QueueWithPriority<T>::addElem(Priority priority, T &elem)
{
  switch (priority)
  {
    case high:
    {
      list[high].push_back(elem);
    }
      break;
    case normal:
    {
      list[normal].push_back(elem);
    }
      break;
    case low:
    {
      list[low].push_back(elem);
    }
      break;
    default:
      throw std::invalid_argument("Incorrect Priority\n");
  }
}

template <typename T>
T QueueWithPriority<T>::getElem()
{
  for (size_t i = 0; i < 3; i++)
  {
    if (!list[i].empty())
    {
      T elem = list[i].front();
      list[i].pop_front();
      return elem;
    }
  }
  throw std::invalid_argument("Empty list\n");
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  list[0].splice(list[0].end(), list[2]);
}

template <typename T>
bool QueueWithPriority<T>::empty()
{
  return (list[0].empty() && list[1].empty() && list[2].empty());
}

#endif
