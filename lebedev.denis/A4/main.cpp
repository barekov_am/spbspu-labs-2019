#include <iostream>
#include <stdexcept>
#include <cmath>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separator.hpp"


void showRectangle_t(lebedev::rectangle_t data)
{
  std::cout << "Width = " << data.width << '\n';
  std::cout << "Height = " << data.height << '\n';
  std::cout << "(X = " << data.pos.x << ";";
  std::cout << "Y = " << data.pos.y<<")" << '\n';
}

void showAll(const lebedev::Shape *shape)
{
  if (shape != nullptr)
  {
  
    std::cout << "Area = " << shape->getArea() << '\n';
    showRectangle_t(shape->getFrameRect());
  }
  else
  {
    throw std::invalid_argument("Shape's pointer is null!!!");
  }
}

void printMatrixInfo(const lebedev::Matrix &matrix)
{
  for (unsigned int i = 0; i < matrix.getLines(); i++)
  {
    std::cout << "Layer number - " << i + 1 << "\n";
    std::cout << "Amount of shapes - " << matrix.getColumns() << "\n";
    for (unsigned int j = 0; j < matrix.getColumns(); j++)
    {
      std::cout << "Shape number " << j + 1 << " with parameters : ";
    }
  }
}


int main()
{
  lebedev::point_t arrayVertex[4] = {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
  lebedev::Rectangle rectangle1(arrayVertex);
  std::cout << "rectangle. "<< '\n';
  showAll(&rectangle1);
  std::cout << "After scale" << '\n';
  rectangle1.scale(2.0);
  showAll(&rectangle1);
  std::cout << "After move(3.0, -1.0)" << '\n';
  rectangle1.move(3.00, -1.0);
  showAll(&rectangle1);
  std::cout << "After move(7.00, 3.0)" << '\n';
  lebedev::point_t newPos = {7.0, 3.0};
  rectangle1.move(newPos);
  showAll(&rectangle1);
  std::cout << "After rotate 90*" << '\n';
  rectangle1.rotate(0);
  showAll(&rectangle1);
  
  lebedev::CompositeShape compShape;
  lebedev::point_t arrayVeetex1[4] = {{10.0, 1.0}, {6.0, 1.0}, {7.0, 4.0}, {12.0, 4.0}};
  
  lebedev::CompositeShape::ptr_shape ptrRect = std::make_shared<lebedev::Rectangle>(arrayVertex);
  lebedev::CompositeShape::ptr_shape ptrRect1 = std::make_shared<lebedev::Rectangle>(arrayVeetex1);
  lebedev::CompositeShape::ptr_shape ptrCirc = std::make_shared<lebedev::Circle>(2, lebedev::point_t {2.0, 5.0});
  lebedev::CompositeShape::ptr_shape ptrCirc1 = std::make_shared<lebedev::Circle>(3, lebedev::point_t {6.0, 7.0});
  
  compShape.add(ptrCirc);
  compShape.add(ptrRect);
  compShape.add(ptrCirc1);
  compShape.add(ptrRect1);
  
  std::cout << "Area Composite Shape before rotate = " << compShape.getArea() << '\n';
  compShape.rotate(90);
  std::cout << "Area Composite Shape After rotate = " << compShape.getArea() << '\n';
  
  std::cout << "Area Composite Shape before rotate = " << compShape.getArea() << '\n';
  compShape.rotate(45);
  std::cout << "Area Composite Shape After rotate = " << compShape.getArea() << '\n';
  

  lebedev::Matrix matrix = lebedev::split(compShape);
    
  printMatrixInfo(matrix); 

  return 0;
  
}
