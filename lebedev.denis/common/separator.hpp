#ifndef SEPARATOR_HPP
#define SEPARATOR_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

using ptrShape = std::shared_ptr<lebedev::Shape>;

namespace lebedev
{
  bool isCrossing(ptrShape shape1, ptrShape shape2);
  Matrix split(CompositeShape &compShape);
}
#endif
