#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separator.hpp"

using ptrShape = std::shared_ptr<lebedev::Shape>;

BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(quantityOfLines)
{
  ptrShape p_circle = std::make_shared<lebedev::Circle>(6, lebedev::point_t{100, 1000});
  lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
  ptrShape p_rectangle = std::make_shared<lebedev::Rectangle>(shape);


  unsigned int quantity_lines = 1;

  lebedev::CompositeShape compShape;
  compShape.add(p_rectangle);
  compShape.add(p_circle);

  lebedev::Matrix matrix = lebedev::split(compShape);


  BOOST_CHECK_EQUAL(quantity_lines, matrix.getLines());
}

BOOST_AUTO_TEST_CASE(quantityOfColumns)
{
  ptrShape p_circle = std::make_shared<lebedev::Circle>(1, lebedev::point_t {10, 10});
  lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
  ptrShape p_rectangle1 = std::make_shared<lebedev::Rectangle>(shape);
  lebedev::point_t shape1[4]= {{2.0, 1.0}, {100.0, 1.0}, {60.0, 4.0}, {70.0, 4.0}};
  ptrShape p_rectangle2 = std::make_shared<lebedev::Rectangle>(shape1);

  const unsigned int quantity_columns = 3;

  lebedev::CompositeShape compShape;
  compShape.add(p_rectangle1);
  compShape.add(p_rectangle2);
  compShape.add(p_circle);

  lebedev::Matrix matrix = lebedev::split(compShape);

  BOOST_CHECK_EQUAL(quantity_columns, matrix.getColumns());
}

BOOST_AUTO_TEST_CASE(extendsColumnsAndLines)
{
  ptrShape p_circle1 = std::make_shared<lebedev::Circle>(4, lebedev::point_t {100, 100 });
  ptrShape p_circle2 = std::make_shared<lebedev::Circle>(7, lebedev::point_t {-1000, 100});
  lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
  ptrShape p_rectangle1 = std::make_shared<lebedev::Rectangle>(shape);
  lebedev::point_t shape1[4]= {{2.0, 1.0}, {100.0, 1.0}, {60.0, 4.0}, {70.0, 4.0}};
  ptrShape p_rectangle2 = std::make_shared<lebedev::Rectangle>(shape1);

  const unsigned int lines = 1;
  const unsigned int columns = 4;

  lebedev::CompositeShape compShape;
  compShape.add(p_circle2);
  compShape.add(p_rectangle1);
  compShape.add(p_circle1);
  compShape.add(p_rectangle2);

  lebedev::Matrix matrix = lebedev::split(compShape);

  BOOST_CHECK_EQUAL(lines, matrix.getLines());
  BOOST_CHECK_EQUAL(columns, matrix.getColumns());
}
BOOST_AUTO_TEST_CASE(throwExceptionForMatrix)
{
  ptrShape p_circle1 = std::make_shared<lebedev::Circle>(4, lebedev::point_t {100, 100 });
  ptrShape p_circle2 = std::make_shared<lebedev::Circle>(7, lebedev::point_t {-1000, 100});
  lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
  ptrShape p_rectangle1 = std::make_shared<lebedev::Rectangle>(shape);
  lebedev::point_t shape1[4]= {{2.0, 1.0}, {100.0, 1.0}, {60.0, 4.0}, {70.0, 4.0}};
  ptrShape p_rectangle2 = std::make_shared<lebedev::Rectangle>(shape1);

  lebedev::CompositeShape compShape;
  compShape.add(p_circle2);
  compShape.add(p_rectangle1);
  compShape.add(p_circle1);
  compShape.add(p_rectangle2);

  lebedev::Matrix matrix = lebedev::split(compShape);

  BOOST_CHECK_THROW(matrix[matrix.getLines() + 70][matrix.getColumns() + 50], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(copyingAndMovingConstructor)
{
  ptrShape p_circle1 = std::make_shared<lebedev::Circle>(4, lebedev::point_t {100, 100 });
  ptrShape p_circle2 = std::make_shared<lebedev::Circle>(7, lebedev::point_t {-1000, 100});
  lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
  ptrShape p_rectangle1 = std::make_shared<lebedev::Rectangle>(shape);
  lebedev::point_t shape1[4]= {{2.0, 1.0}, {100.0, 1.0}, {60.0, 4.0}, {70.0, 4.0}};
  ptrShape p_rectangle2 = std::make_shared<lebedev::Rectangle>(shape1);

  lebedev::CompositeShape compShape;
  compShape.add(p_circle2);
  compShape.add(p_rectangle1);
  compShape.add(p_circle1);
  compShape.add(p_rectangle2);

  lebedev::Matrix matrix = lebedev::split(compShape);

  lebedev::Matrix matrix_copying(matrix);

  BOOST_CHECK_EQUAL(matrix_copying.getLines(), matrix.getLines());
  BOOST_CHECK_EQUAL(matrix_copying.getColumns(), matrix.getColumns());
  BOOST_CHECK(matrix == matrix_copying);

  lebedev::Matrix matrix_moving(std::move(matrix));

  BOOST_CHECK_EQUAL(matrix_copying.getLines(), matrix_moving.getLines());
  BOOST_CHECK_EQUAL(matrix_copying.getColumns(), matrix_moving.getColumns());
  BOOST_CHECK(matrix_moving == matrix_copying);
}

BOOST_AUTO_TEST_CASE(copyingOperator)
{
  ptrShape p_circle1 = std::make_shared<lebedev::Circle>(4, lebedev::point_t {100, 100 });
  ptrShape p_circle2 = std::make_shared<lebedev::Circle>(7, lebedev::point_t {-1000, 100});
  lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
  ptrShape p_rectangle1 = std::make_shared<lebedev::Rectangle>(shape);
  lebedev::point_t shape1[4]= {{2.0, 1.0}, {100.0, 1.0}, {60.0, 4.0}, {70.0, 4.0}};
  ptrShape p_rectangle2 = std::make_shared<lebedev::Rectangle>(shape1);

  lebedev::CompositeShape compShape;
  compShape.add(p_circle2);
  compShape.add(p_rectangle1);
  compShape.add(p_circle1);
  compShape.add(p_rectangle2);

  lebedev::Matrix matrix = lebedev::split(compShape);

  BOOST_CHECK(matrix[0][0] == p_circle2);
  BOOST_CHECK(matrix[0][1] == p_rectangle1);
  BOOST_CHECK(matrix[0][2] == p_circle1);
  BOOST_CHECK(matrix[0][3] == p_rectangle2);
  BOOST_CHECK(matrix[0][0] != p_circle1);

  lebedev::CompositeShape compShape1;
  lebedev::CompositeShape compShape2;

  compShape1.add(p_circle2);
  compShape1.add(p_rectangle1);
  compShape1.add(p_circle1);
  compShape1.add(p_rectangle2);

  lebedev::Matrix matrix1 = lebedev::split(compShape1);
  lebedev::Matrix matrix2 = lebedev::split(compShape2);
  matrix1 = matrix2;
  BOOST_CHECK_EQUAL(matrix1.getColumns(), matrix2.getColumns());
  BOOST_CHECK_EQUAL(matrix1.getLines(), matrix2.getLines());

}

BOOST_AUTO_TEST_CASE(movingOperator)
{
  ptrShape p_circle1 = std::make_shared<lebedev::Circle>(4, lebedev::point_t {100, 100 });
  ptrShape p_circle2 = std::make_shared<lebedev::Circle>(7, lebedev::point_t {-1000, 100});
  lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
  ptrShape p_rectangle1 = std::make_shared<lebedev::Rectangle>(shape);
  lebedev::point_t shape1[4]= {{2.0, 1.0}, {100.0, 1.0}, {60.0, 4.0}, {70.0, 4.0}};
  ptrShape p_rectangle2 = std::make_shared<lebedev::Rectangle>(shape1);

  lebedev::CompositeShape compShape;
  compShape.add(p_circle2);
  compShape.add(p_rectangle1);
  compShape.add(p_circle1);
  compShape.add(p_rectangle2);

  lebedev::Matrix matrix1 = lebedev::split(compShape);
  lebedev::Matrix matrix2;

  matrix1 = std::move(matrix2);

  BOOST_CHECK_EQUAL(matrix1.getColumns(), 0);
  BOOST_CHECK_EQUAL(matrix1.getLines(), 0);
}

BOOST_AUTO_TEST_CASE(comparingOperator)
{
  ptrShape p_circle1 = std::make_shared<lebedev::Circle>(4, lebedev::point_t {100, 100 });
  ptrShape p_circle2 = std::make_shared<lebedev::Circle>(7, lebedev::point_t {-1000, 100});
  lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
  ptrShape p_rectangle1 = std::make_shared<lebedev::Rectangle>(shape);
  lebedev::point_t shape1[4]= {{2.0, 1.0}, {100.0, 1.0}, {60.0, 4.0}, {70.0, 4.0}};
  ptrShape p_rectangle2 = std::make_shared<lebedev::Rectangle>(shape1);

  lebedev::CompositeShape compShape;
  compShape.add(p_circle2);
  compShape.add(p_rectangle1);
  compShape.add(p_circle1);

  lebedev::Matrix matrix1 = lebedev::split(compShape);
  lebedev::Matrix matrix2 = lebedev::split(compShape);
  lebedev::CompositeShape compShape1;

  compShape.add(p_rectangle2);
  lebedev::Matrix matrix3 = lebedev::split(compShape1);
  BOOST_CHECK(matrix1 == matrix2);
  BOOST_CHECK(matrix1 != matrix3);
}
BOOST_AUTO_TEST_SUITE_END()
