#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "polygon.hpp"

const double FAULT = 0.001;
const double INCORRECT_ARG = -1.0;
using ptrShape = std::shared_ptr<lebedev::Shape>;

BOOST_AUTO_TEST_SUITE(A3TestCompositeShape)

  BOOST_AUTO_TEST_CASE(TestCompositeShapeConstructor)
  {
    ptrShape p_circle = std::make_shared<lebedev::Circle>(6.0, lebedev::point_t {0, 0 });
    lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
    ptrShape p_rectangle = std::make_shared<lebedev::Rectangle>(shape);
    lebedev::CompositeShape compShape;
    compShape.add(p_circle);
    compShape.add(p_rectangle);

    std::size_t zeroQty = 0;
    lebedev::CompositeShape copyCompShape(compShape);
    BOOST_CHECK_EQUAL(compShape.getQuantity(), copyCompShape.getQuantity());

    const std::size_t qtyBeforeMove = copyCompShape.getQuantity();
    lebedev::CompositeShape moveCompositeShape = (std::move(copyCompShape));
    BOOST_CHECK_EQUAL(zeroQty, copyCompShape.getQuantity());
    BOOST_CHECK_EQUAL(qtyBeforeMove, moveCompositeShape.getQuantity());
  }

  BOOST_AUTO_TEST_CASE(TestCompositeShapeAsigmentOperators)
  {
    
    ptrShape p_circle = std::make_shared<lebedev::Circle>(6.0, lebedev::point_t {0, 0 });
    lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
    ptrShape p_rectangle = std::make_shared<lebedev::Rectangle>(shape);
    lebedev::CompositeShape compShape;
    compShape.add(p_circle);
    compShape.add(p_rectangle);
    lebedev::CompositeShape moveCompositeShape;
    
    std::size_t zeroQty = 0;
    lebedev::CompositeShape copyCompShape;
    copyCompShape = compShape;
    BOOST_CHECK_EQUAL(compShape.getQuantity(), copyCompShape.getQuantity());

    const std::size_t qtyBeforeMove = copyCompShape.getQuantity();
    moveCompositeShape = std::move(copyCompShape);
    BOOST_CHECK_EQUAL(qtyBeforeMove, moveCompositeShape.getQuantity());
    BOOST_CHECK_EQUAL(zeroQty, copyCompShape.getQuantity());
  }

  BOOST_AUTO_TEST_CASE(TestCompositeShapeIndexOperator)
  {
    ptrShape p_circle = std::make_shared<lebedev::Circle>(6.0, lebedev::point_t {0, 0 });
    lebedev::CompositeShape compShape;
    compShape.add(p_circle);
    
    BOOST_CHECK_THROW(compShape[-1],std::out_of_range);
    BOOST_CHECK_THROW(compShape[100],std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(TestCompositeShapeAddAndDisposal)
  {
    ptrShape p_circle = std::make_shared<lebedev::Circle>(6.0, lebedev::point_t {0, 0 });
  
    lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
    ptrShape p_rectangle = std::make_shared<lebedev::Rectangle>(shape);
    lebedev::Rectangle rectangle(shape);
    lebedev::CompositeShape compShape;
    compShape.add(p_circle);
    
    const double AreaCircle = p_circle->getArea();
    const double areaRect = p_rectangle->getArea();
    
    const double areaBeforeAdd = compShape.getArea();
    const std::size_t qtyBeforeAdd = compShape.getQuantity();
    compShape.add(p_rectangle);
    const double areaAfterAdd = compShape.getArea();
    const std::size_t qtyAfterAdd = compShape.getQuantity();

    BOOST_CHECK_CLOSE(areaBeforeAdd + areaRect, areaAfterAdd, FAULT);
    BOOST_CHECK_EQUAL(qtyBeforeAdd + 1, qtyAfterAdd);

    compShape.disposal(0);
    const double areaAfterDisposal = compShape.getArea();
    const std::size_t qtyAfterDisposal = compShape.getQuantity();

    BOOST_CHECK_CLOSE(areaAfterAdd - AreaCircle, areaAfterDisposal, FAULT);
    BOOST_CHECK_EQUAL(qtyAfterAdd - 1, qtyAfterDisposal);

  }

  BOOST_AUTO_TEST_CASE(TestIncorrectArgumentDisposal)
  {
    lebedev::CompositeShape compShape;
    ptrShape p_circle = std::make_shared<lebedev::Circle>(6.0, lebedev::point_t {0, 0 });
    compShape.add(p_circle);
    BOOST_CHECK_THROW(compShape.disposal(-1), std::invalid_argument);
    BOOST_CHECK_THROW(compShape.disposal(100), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TestCompositeShapeAfterMove)
  {
    ptrShape p_circle = std::make_shared<lebedev::Circle>(6.0, lebedev::point_t {0, 0 });
    lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
    ptrShape p_rectangle = std::make_shared<lebedev::Rectangle>(shape);
    lebedev::CompositeShape compShape;
    compShape.add(p_circle);
    compShape.add(p_rectangle);

    const double areaBefore = compShape.getArea();
    const lebedev::rectangle_t frameBefore = compShape.getFrameRect();
    compShape.move(2.00, 5.4);
    const double areaAfterMove = compShape.getArea();
    const lebedev::rectangle_t frameAfterMove = compShape.getFrameRect();
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfterMove.height, FAULT);
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfterMove.width, FAULT);
    BOOST_CHECK_CLOSE(areaBefore, areaAfterMove, FAULT);
  }

  BOOST_AUTO_TEST_CASE(TestCompositeShapeAfterMovePos)
  {
    ptrShape p_circle = std::make_shared<lebedev::Circle>(6.0, lebedev::point_t {0, 0 });
    lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
    ptrShape p_rectangle = std::make_shared<lebedev::Rectangle>(shape);
    lebedev::CompositeShape compShape;
    compShape.add(p_circle);
    compShape.add(p_rectangle);

    const double areaBefore = compShape.getArea();
    const lebedev::rectangle_t frameBefore = compShape.getFrameRect();
    compShape.move({3.0, 4.0});
    const double areaAfterMove = compShape.getArea();
    const lebedev::rectangle_t frameAfterMove = compShape.getFrameRect();
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfterMove.height, FAULT);
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfterMove.width, FAULT);
    BOOST_CHECK_CLOSE(areaBefore, areaAfterMove, FAULT);
  }

  BOOST_AUTO_TEST_CASE(TestCompositeShapeScale)
  {
    ptrShape p_circle = std::make_shared<lebedev::Circle>(6.0, lebedev::point_t {0, 0 });
    lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
    ptrShape p_rectangle = std::make_shared<lebedev::Rectangle>(shape);
    lebedev::CompositeShape compShape;
    compShape.add(p_circle);
    compShape.add(p_rectangle);

    const double areaBeforeScale = compShape.getArea();
    const double multiplier = 2.0;
    compShape.scale(multiplier);
    const double areaAfterScale = compShape.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale * multiplier * multiplier, areaAfterScale, FAULT);
  }

  BOOST_AUTO_TEST_CASE(TestCompositeShapeInvalidCoefficient)
  {
    ptrShape p_circle = std::make_shared<lebedev::Circle>(6.0, lebedev::point_t {0, 0 });
    lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
    ptrShape p_rectangle = std::make_shared<lebedev::Rectangle>(shape);
    lebedev::CompositeShape compShape;
    compShape.add(p_circle);
    compShape.add(p_rectangle);

    BOOST_CHECK_THROW(compShape.scale(INCORRECT_ARG), std::invalid_argument);
  }
  
  BOOST_AUTO_TEST_CASE(TestCompositeShapeRotate)
  {
    ptrShape p_circle = std::make_shared<lebedev::Circle>(6.0, lebedev::point_t {0, 0 });
    lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
    ptrShape p_rectangle = std::make_shared<lebedev::Rectangle>(shape);
    lebedev::CompositeShape compShape;
    compShape.add(p_circle);
    compShape.add(p_rectangle);  
    
    const double areaBeforeRotate = compShape.getArea();
    compShape.rotate(90);
    const double areaAfterRotate = compShape.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotate, areaAfterRotate, FAULT);
    compShape.rotate(45);
    BOOST_CHECK_CLOSE(areaBeforeRotate, areaAfterRotate, FAULT);
  }

BOOST_AUTO_TEST_SUITE_END()
