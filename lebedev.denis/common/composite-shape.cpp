#include "composite-shape.hpp"
#include <cmath>
#include <algorithm>
#include <stdexcept>
#include <iostream>

lebedev::CompositeShape::CompositeShape():
  qtyShapes_(0)
{
}  

lebedev::CompositeShape::CompositeShape(const CompositeShape &other):
  qtyShapes_(other.qtyShapes_),
  arrayShapes_(std::make_unique<ptr_shape[]>(qtyShapes_))
{
  for (unsigned int index = 0; index < qtyShapes_; index++)
  {
    arrayShapes_[index] = other.arrayShapes_[index];
  }
}

lebedev::CompositeShape::CompositeShape(CompositeShape &&other):
  qtyShapes_(other.qtyShapes_),
  arrayShapes_(std::move(other.arrayShapes_))
{
  other.qtyShapes_ = 0;
}

lebedev::CompositeShape & lebedev::CompositeShape::operator =(const CompositeShape &other)
{
  if (this == &other)
  {
    return *this;
  }
  
  std::unique_ptr<ptr_shape[]>newArrayShapes_(std::make_unique<ptr_shape[]>(other.qtyShapes_));  
  qtyShapes_ = other.qtyShapes_;
  
  if (qtyShapes_ != 0)
  {  
    for (unsigned int index = 0; index < qtyShapes_; index++)
    {
      newArrayShapes_[index] = other.arrayShapes_[index];
    }
    arrayShapes_.swap(newArrayShapes_);
  }
  
  return *this;
}

lebedev::CompositeShape & lebedev::CompositeShape::operator =(CompositeShape &&other)
{
  if (this == &other)
  {
    return *this;
  }

  qtyShapes_ = other.qtyShapes_;
  other.qtyShapes_ = 0;
  arrayShapes_ = std::move(other.arrayShapes_);
  other.arrayShapes_ = nullptr;

  return *this;
}



lebedev::CompositeShape::ptr_shape lebedev::CompositeShape::operator [](unsigned int index) const
{
  if (index >= qtyShapes_)
  {
    throw std::out_of_range("Index parametrs must be less or equals Quantity of Shapes!");
  }

  return arrayShapes_[index];
}

void lebedev::CompositeShape::add(ptr_shape newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Shape pointer is null");
  }

  std::unique_ptr<ptr_shape[]>newArrayShapes_(std::make_unique<ptr_shape[]>(qtyShapes_ + 1));

  for (unsigned int index = 0; index < qtyShapes_; index++)
  {
    newArrayShapes_[index] = arrayShapes_[index];
  }

  newArrayShapes_[qtyShapes_] = newShape;
  qtyShapes_ += 1;
  arrayShapes_.swap(newArrayShapes_);
}

void lebedev::CompositeShape::disposal(unsigned int numberDeletedShape)
{
  if (qtyShapes_ == 0)
  {
    throw std::logic_error("Composite shape is empty!");
  }

  if (numberDeletedShape >= qtyShapes_)
  {
    throw std::invalid_argument("Number of deleted shape out of range!");
  }

  std::unique_ptr<ptr_shape[]> newArrayShapes_(std::make_unique<ptr_shape[]>(qtyShapes_ - 1));

  for (unsigned int index = 0; index < qtyShapes_; index++)
  {
    if (index == numberDeletedShape)
    {
      continue;
    }
      
    if (index  > numberDeletedShape)
    {
      newArrayShapes_[index - 1] = arrayShapes_[index];
    }
    else
    {
      newArrayShapes_[index] = arrayShapes_[index];
    }
  }
  
  qtyShapes_ -= 1;
  arrayShapes_.swap(newArrayShapes_);
}

unsigned int lebedev::CompositeShape::getQuantity() const
{
  return qtyShapes_;
}

double lebedev::CompositeShape::getArea() const
{
  if (qtyShapes_ == 0)
  {
    throw std::logic_error("Composite-Shape is empty! ");
  }

  double areaCompositeShape = 0.0;

  for (unsigned int index = 0; index < qtyShapes_; index++)
  {
   areaCompositeShape += arrayShapes_[index]->getArea();
  }

  return areaCompositeShape;
}

lebedev::rectangle_t lebedev::CompositeShape::getFrameRect() const
{
  if (qtyShapes_ == 0)
  {
    throw std::logic_error("Composite shape is empty!");
  }

  lebedev::rectangle_t frameRect = arrayShapes_[0]->getFrameRect();
  double maxX = (frameRect.pos.x + frameRect.width / 2);
  double minX = (frameRect.pos.x - frameRect.width / 2);
  double maxY = (frameRect.pos.y + frameRect.height / 2);
  double minY = (frameRect.pos.y - frameRect.height / 2);

  for (unsigned int index = 1; index < qtyShapes_; index++)
  {
    lebedev::rectangle_t frameRect = arrayShapes_[index]->getFrameRect();
    maxX = std::max(maxX, (frameRect.pos.x + frameRect.width / 2));
    minX = std::min(minX, (frameRect.pos.x - frameRect.width / 2));
    maxY = std::max(maxY, (frameRect.pos.y + frameRect.height / 2));
    minY = std::min(minY, (frameRect.pos.y - frameRect.height / 2));
  }

  const double height = (maxX - minX);
  const double width = (maxY - minY);
  const lebedev::point_t posFrameRect = {minX + (width / 2), minY + (height / 2)};

  return {height, width, posFrameRect};
}

void lebedev::CompositeShape::scale(const double multiplier)
{
  if (qtyShapes_ == 0)
  {
    throw std::logic_error ("Composite shape is empty!");
  }

  if (multiplier <= 0)
  {
    throw std::invalid_argument("Invalid multiplier for scaling of Composite shape!");
  }

  const lebedev::point_t pos = getFrameRect().pos;

  for (unsigned int index = 0; index < qtyShapes_; index++)
  {
    lebedev::point_t posShape = arrayShapes_[index]->getFrameRect().pos;
    double x = (posShape.x - pos.x) * (multiplier - 1);
    double y = (posShape.y - pos.y) * (multiplier - 1);
    arrayShapes_[index]->move(x, y);
    arrayShapes_[index]->scale(multiplier);
  }
}

void lebedev::CompositeShape::move(const double x, const double y)
{
  for (unsigned int index = 0; index < qtyShapes_; index++)
  {
    arrayShapes_[index]->move(x, y);
  }
}

void lebedev::CompositeShape::move(const point_t &newPoint)
{
  const double x = newPoint.x - getFrameRect().pos.x;
  const double y = newPoint.y - getFrameRect().pos.y;
  move(x, y);
}

void lebedev::CompositeShape::rotate(double angle)
{
  if (qtyShapes_ == 0)
  {
    throw std::invalid_argument("Composite Shape is empty");
  }
  
  angle *= M_PI / 180;
  
  for (unsigned int index = 0; index < qtyShapes_; index++)
  {
    point_t pos = arrayShapes_[index]->getFrameRect().pos;
    point_t csPos = getFrameRect().pos;
    double tempX = pos.x;
    pos.x = (pos.x - csPos.x) * cos(angle) - (pos.y - csPos.y) * sin(angle) + csPos.x;
    pos.y = (tempX - csPos.x) * sin(angle) + (pos.y - csPos.y) * cos(angle) + csPos.y;
    arrayShapes_[index]->move(pos);
    arrayShapes_[index]->rotate(angle);
  }
}
