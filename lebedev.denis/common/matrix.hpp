#ifndef MATRIX_HPP
#define MATRIX_HPP
#include <memory>
#include "shape.hpp"

namespace lebedev
{
  class Matrix
  {
  public:
    using ptrShape = std::shared_ptr<Shape>;
    
    Matrix();
    Matrix(const Matrix &other);
    Matrix(Matrix &&other);
    
    ~Matrix() = default;
    
    Matrix &operator =(const Matrix &other);
    Matrix &operator =(Matrix &&other);
    std::unique_ptr<ptrShape[]> operator [](unsigned int index) const;
    bool operator !=(const Matrix &other) const;
    bool operator ==(const Matrix &other) const;
    
    void add(ptrShape shape, unsigned int line, unsigned int column);
    unsigned int getLines() const;
    unsigned int getColumns() const;  
  
  private:    
    unsigned int lines_;
    unsigned int columns_;
    std::unique_ptr<ptrShape[]> list_;  
  };
}
#endif
