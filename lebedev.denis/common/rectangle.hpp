#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace lebedev
{
  class Rectangle: public Shape
  {
  public:
    Rectangle(lebedev::point_t arrayVetex[4]);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const double x, const double y) override;
    void move(const point_t &new_point) override;
    void scale(const double multiplier) override;
    point_t calcCentroid() const;
    void rotate(double angle) override;

  private:
    unsigned int qtyVertex_;
    point_t pos_;
    point_t arrayVertex_[4];
  };
}
#endif
