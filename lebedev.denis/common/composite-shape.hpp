#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include <memory>
#include "shape.hpp"

namespace lebedev
{
  class CompositeShape: public Shape
  {
  public:
    using ptr_shape = std::shared_ptr<Shape>;
    
    CompositeShape();
    CompositeShape(const CompositeShape &other);
    CompositeShape(CompositeShape &&other);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &other);
    CompositeShape &operator =(CompositeShape &&other);
    ptr_shape operator [](unsigned int index) const;

    void add(ptr_shape newShape);
    void disposal(unsigned int numberDeletedShape);
    unsigned int getQuantity() const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void scale(const double multiplier) override;
    void move(const double x, const double y) override;
    void move(const point_t &newPoint) override;
    void rotate(double angle) override;
  
  private:
    unsigned int qtyShapes_;
    std::unique_ptr<ptr_shape[]> arrayShapes_;
  };
}
#endif
