#include "separator.hpp"
#include <cmath>

bool lebedev::isCrossing(ptrShape shape1, ptrShape shape2)
{
  if ((shape1 == nullptr) || (shape2 == nullptr))
  {
    return false;
  }
  
  bool crossing_x = fabs(shape1->getFrameRect().pos.x - shape2->getFrameRect().pos.x)
                    < ((shape1->getFrameRect().width / 2) + (shape2->getFrameRect().width / 2));
  bool crossing_y = fabs(shape1->getFrameRect().pos.y - shape2->getFrameRect().pos.y)
                    < ((shape1->getFrameRect().height / 2) + (shape2->getFrameRect().height / 2));

  return crossing_x && crossing_y;
}

lebedev::Matrix lebedev::split(lebedev::CompositeShape &compShape)
{
  Matrix matrix;

  for (unsigned int i = 0; i < compShape.getQuantity(); i++)
  {
    unsigned int line = 0;
    unsigned int column = 0;

    for (unsigned int j = 0; j < matrix.getLines(); j++)
    {
      for (unsigned int k = 0; k < matrix.getColumns(); k++)
      {
        if (matrix[j][k] == nullptr)
        {
          column = k;
          line = j;
          break;
        }

        if (isCrossing(compShape[i], matrix[j][k]))
        {
          column = 0;
          line = j + 1;
          break;
        }

        column = k + 1;
        line = j;
      }

      if (line == j)
      {
        break;
      }
    }

    matrix.add(compShape[i], line, column);
  }
  return matrix;
}
