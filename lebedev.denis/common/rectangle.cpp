#include "rectangle.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

lebedev::Rectangle::Rectangle(lebedev::point_t arrayVetex[4]):
  qtyVertex_(4)
{
  if (qtyVertex_ < 4)
  {
    throw std::invalid_argument("Rectangle must have only 4 points");
  }
  
  if ((arrayVetex[0].y != arrayVetex[1].y) || (arrayVetex[2].y != arrayVetex[3].y))
  {
    throw std::invalid_argument("Invalid Rectangle");
  }
  
  for (unsigned int index = 0; index < qtyVertex_; index++)
  {
    arrayVertex_[index] = arrayVetex[index];
  }
  
  pos_ = calcCentroid();
}
  

double lebedev::Rectangle::getArea() const
{
  double summa = 0.0;

  for (std::size_t index = 0; index < qtyVertex_ - 1; index++)
  {
    summa += arrayVertex_[index].x * arrayVertex_[index + 1].y;
    summa -= arrayVertex_[index + 1].x * arrayVertex_[index].y;
  }

  summa += arrayVertex_[qtyVertex_-1].x * arrayVertex_[0].y;
  summa -= arrayVertex_[0].x * arrayVertex_[qtyVertex_-1].y;

  return (0.5 * std::fabs(summa));
}

lebedev::rectangle_t lebedev::Rectangle::getFrameRect() const
{
  double maxX = arrayVertex_[0].x;
  double minX = arrayVertex_[0].x;
  double maxY = arrayVertex_[0].y;
  double minY = arrayVertex_[0].y;

  for (unsigned int index = 1; index < qtyVertex_; index++)
  {
    maxX = std::max(maxX, arrayVertex_[index].x);
    minX = std::min(minX, arrayVertex_[index].x);
    maxY = std::max(maxY, arrayVertex_[index].y);
    minY = std::min(minY, arrayVertex_[index].y);
  }

  const double width = (maxY - minY);
  const double height = (maxX - minX);
  const point_t posFrameRect = {minX + (width / 2), minY + (height / 2)};

  return {width, height, posFrameRect};
}

void lebedev::Rectangle::move(const double x, const double y)
{
  for (unsigned int index = 0; index < qtyVertex_; index++)
  {
    arrayVertex_[index].x += x;
    arrayVertex_[index].y += y;
  }
  pos_ = getFrameRect().pos;
}

void lebedev::Rectangle::move(const point_t &new_point)
{
  const point_t differens = {new_point.x - pos_.x, new_point.y - pos_.y};
  move(differens.x, differens.y);
}

void lebedev::Rectangle::scale(const double multiplier)
{
  if (multiplier <= 0.0)
  {
    throw std::invalid_argument("Multiplier must be more then 0.0");
  }
  else
  {
    for (unsigned int index = 0; index < qtyVertex_; index ++)
    {
      arrayVertex_[index].x = pos_.x + (arrayVertex_[index].x - pos_.x) * multiplier;
      arrayVertex_[index].y = pos_.y + (arrayVertex_[index].y - pos_.y) * multiplier;
    }
  }
}


lebedev::point_t lebedev::Rectangle::calcCentroid() const
{
  lebedev::point_t temp_point = {0.0, 0.0};
  for (unsigned int index = 0; index < qtyVertex_; index++)
  {
    temp_point.x += arrayVertex_[index].x;
    temp_point.y += arrayVertex_[index].y;
  }
  return {temp_point.x / qtyVertex_, temp_point.y / qtyVertex_};
}

void lebedev::Rectangle::rotate(double angle)
{
  angle *= M_PI / 180;
  for (unsigned int index = 0; index < qtyVertex_; index++)
  {
    double tempX = arrayVertex_[index].x;
    arrayVertex_[index].x = pos_.x + (arrayVertex_[index].x - pos_.x) * cos(angle) 
                            - (arrayVertex_[index].y - pos_.y) * sin(angle);
    arrayVertex_[index].y = pos_.y + (tempX - pos_.x) * sin(angle) 
                            + (arrayVertex_[index].y - pos_.y) * cos(angle);
  }
}
