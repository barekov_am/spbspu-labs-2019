#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separator.hpp"

using p_shape = std::shared_ptr<lebedev::Shape>;

BOOST_AUTO_TEST_SUITE(testSeparator)

BOOST_AUTO_TEST_CASE(checkPartition)
{
  ptrShape p_circle1 = std::make_shared<lebedev::Circle>(4, lebedev::point_t {100, 100 });
  ptrShape p_circle2 = std::make_shared<lebedev::Circle>(7, lebedev::point_t {-1000, 100});
  lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
  ptrShape p_rectangle1 = std::make_shared<lebedev::Rectangle>(shape);
  lebedev::point_t shape1[4]= {{2.0, 1.0}, {100.0, 1.0}, {60.0, 4.0}, {70.0, 4.0}};
  ptrShape p_rectangle2 = std::make_shared<lebedev::Rectangle>(shape1);

  lebedev::CompositeShape compShape;
  compShape.add(p_circle2);
  compShape.add(p_rectangle1);
  compShape.add(p_circle1);
  compShape.add(p_rectangle2);

  lebedev::Matrix matrix = lebedev::split(compShape);


  BOOST_CHECK(matrix[0][0] == p_circle2);
  BOOST_CHECK(matrix[0][1] == p_rectangle1);
  BOOST_CHECK(matrix[0][2] == p_circle1);
  BOOST_CHECK(matrix[0][3] == p_rectangle2);
  BOOST_CHECK(matrix[0][0] != p_circle1);
}

BOOST_AUTO_TEST_SUITE_END()
