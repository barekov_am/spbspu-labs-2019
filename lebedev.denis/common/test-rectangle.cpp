#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

const double FAULT = 0.001;
const double INCORRECT_ARG = -47.0;

BOOST_AUTO_TEST_SUITE(A2TestRectangle)

  BOOST_AUTO_TEST_CASE(TestRectangleAfterMove)
  {
    lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
    lebedev::Rectangle testRectangle(shape);
    const double areaBefore = testRectangle.getArea();
    const lebedev::rectangle_t frameBefore = testRectangle.getFrameRect();
    testRectangle.move(2.00, 5.4);
    const double areaAfterMove = testRectangle.getArea();
    const lebedev::rectangle_t frameAfterMove = testRectangle.getFrameRect();
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfterMove.height, FAULT);
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfterMove.width, FAULT);
    BOOST_CHECK_CLOSE(areaBefore, areaAfterMove, FAULT);
  }

  BOOST_AUTO_TEST_CASE(TestRectangleAfterMovePos)
  {
    lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
    lebedev::Rectangle testRectangle(shape);
    const double areaBefore = testRectangle.getArea();
    const lebedev::rectangle_t frameBefore = testRectangle.getFrameRect();
    testRectangle.move({3.00, 4.00});
    const double areaAfterMove = testRectangle.getArea();
    const lebedev::rectangle_t frameAfterMove = testRectangle.getFrameRect();
    BOOST_CHECK_CLOSE(frameBefore.height, frameAfterMove.height, FAULT);
    BOOST_CHECK_CLOSE(frameBefore.width, frameAfterMove.width, FAULT);
    BOOST_CHECK_CLOSE(areaBefore, areaAfterMove, FAULT);
  }

  BOOST_AUTO_TEST_CASE(TestRectangleScale)
  {
    lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
    lebedev::Rectangle testRectangle(shape);
    const double areaBeforeScale = testRectangle.getArea();
    const double multiplier = 2.0;
    testRectangle.scale(multiplier);
    const double areaAfterScale = testRectangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale * multiplier * multiplier, areaAfterScale, FAULT);
  }

  BOOST_AUTO_TEST_CASE(TestRectangleInvalidParametrs)
  {
    lebedev::point_t shape[4]= {{2.0, INCORRECT_ARG}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
    BOOST_CHECK_THROW(lebedev::Rectangle rect(shape), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TestRectangleInvalidCoefficient)
  {
    lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
    lebedev::Rectangle testRectangle(shape);
    BOOST_CHECK_THROW(testRectangle.scale(INCORRECT_ARG), std::invalid_argument);
  }
  
  BOOST_AUTO_TEST_CASE(TestRectangleRotate)
  {
    lebedev::point_t shape[4]= {{2.0, 1.0}, {5.0, 1.0}, {5.0, 4.0}, {2.0, 4.0}};
    lebedev::Rectangle testRectangle(shape);
    const double areaBeforeRotate = testRectangle.getArea();
    testRectangle.rotate(90);
    const  double areaAfterRotate = testRectangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotate, areaAfterRotate, FAULT);
    testRectangle.rotate(45);
    BOOST_CHECK_CLOSE(areaBeforeRotate, areaAfterRotate, FAULT);
  }

BOOST_AUTO_TEST_SUITE_END();
