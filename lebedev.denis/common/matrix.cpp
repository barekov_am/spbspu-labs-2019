#include "matrix.hpp"
#include "stdexcept"

lebedev::Matrix::Matrix():
  lines_(0),
  columns_(0)
{  
}

lebedev::Matrix::Matrix(const Matrix &other):
  lines_(other.lines_),
  columns_(other.columns_),
  list_(std::make_unique<ptrShape[]>(other.lines_ * other.columns_))
{
  for (unsigned int i = 0; i < lines_ * columns_; i++)
  {
    list_[i] = other.list_[i];
  }
}

lebedev::Matrix::Matrix(Matrix &&other):
  lines_(other.lines_),
  columns_(other.columns_),
  list_(std::move(other.list_))
{
  other.lines_ = 0;
  other.columns_ = 0; 
}

lebedev::Matrix & lebedev::Matrix::operator =(const Matrix &other)
{
  if (*this == other)
  {
    return *this;
  }
  
  std::unique_ptr<ptrShape[]>newList(std::make_unique<ptrShape[]>(other.lines_ * other.columns_));
  
  for (unsigned int i = 0; i < other.lines_ * other.columns_; i++)
  {
    newList[i] = other.list_[i];
  }
  
  lines_ = other.lines_;
  columns_ = other.columns_;
  list_.swap(newList);
  
  return *this;
}

lebedev::Matrix & lebedev::Matrix::operator =(Matrix &&other)
{
  if (*this == other)
  {
    return *this;
  }
  
  lines_ = other.lines_;
  columns_ = other.columns_;
  list_ = std::move(other.list_);
  other.lines_ = 0;
  other.columns_ = 0;
  
  return *this;
}

std::unique_ptr<lebedev::Matrix::ptrShape[]> lebedev::Matrix::operator [](unsigned int index) const
{
  
  if (index >= lines_)
  {
    throw std::out_of_range("Index is out of range");
  }

  std::unique_ptr<ptrShape[]>tmpList(std::make_unique<ptrShape[]>(columns_));
  
  for (unsigned int i = 0; i < columns_; i++)
  {
    tmpList[i] = list_[index * columns_ + i];
  }

  return tmpList;
}

bool lebedev::Matrix::operator !=(const Matrix &other) const
{
  if ((lines_ != other.lines_) || (columns_ != other.columns_))
  {
    return true;
  }
  
  for (unsigned int i = 0; i < lines_ * columns_; i++)
  {
    if (list_[i] != other.list_[i])
    {
      return true;
    }
  }
  
  return false;
}

bool lebedev::Matrix::operator ==(const Matrix &other) const
{
  if (*this != other)
  {
    return false;
  }
  
  return true;
}

void lebedev::Matrix::add(ptrShape shape, unsigned int line, unsigned int column)
{
  unsigned int tmpLine = (line == lines_) ? (lines_ + 1) : (lines_ );
  unsigned int tmpColumn = (column == columns_) ? (columns_ + 1) : (columns_);
  
  std::unique_ptr<ptrShape[]>tmpList(std::make_unique<ptrShape[]>(tmpLine * tmpColumn));
  
  for (unsigned int i = 0; i < tmpLine; i++)
  {
    for (unsigned int j = 0; j < tmpColumn; j++)
    {
      if ((i == lines_) || (j == columns_))
      {
        tmpList[i * tmpColumn + j] = nullptr;
      } 
      else
      {
        tmpList[i * tmpColumn + j] = list_[i * columns_ + j];
      }
    }
  }
  
  tmpList[line * tmpColumn + column] = shape;
  lines_ = tmpLine;
  columns_ = tmpColumn;
  list_.swap(tmpList);
}

unsigned int lebedev::Matrix::getLines() const
{
  return lines_;
}

unsigned int lebedev::Matrix::getColumns() const
{
  return columns_;
}

