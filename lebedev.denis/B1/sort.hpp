#ifndef SORT_HPP
#define SORT_HPP

#include "details.hpp"
#include <string>
#include <algorithm>
#include <functional>

bool typeSort(const char* argv);

template <template <typename Container> class Access, typename Container>
void sort(Container &container, bool flag)
{
  std::function<bool(typename Container::value_type a, typename Container::value_type b)> compare;
  if (flag)
  {
    compare = [](typename Container::value_type a, typename Container::value_type b)->bool
    {
      return a > b;
    };
  }
  else
  {
    compare = [](typename Container::value_type a, typename Container::value_type b)->bool
    {
      return a < b;
    };
  }

  auto begin = Access<Container>::begin(container);
  auto end = Access<Container>::end(container);

  for (auto i = begin; i != end; ++i)
  {
    for (auto j = i; j != end; ++j)
    {
      auto a = Access<Container>::getElem(container, i);
      auto b = Access<Container>::getElem(container, j);
      if (compare(a, b))
      {
        std::swap(Access<Container>::getElem(container, i), Access<Container>::getElem(container, j));
      }
    }
  }
}

#endif
