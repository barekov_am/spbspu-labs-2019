#include "sort.hpp"
#include <cstring>
bool typeSort(const char* argv)
{
  if (!std::strcmp(argv,"ascending"))
  {
    return true;
  }
  else if (!std::strcmp(argv,"descending"))
  {
    return false;
  }
  throw std::invalid_argument("--");
}
