#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <vector>

template <typename T>
class Functor
{
public:
  void operator()(T elem)
  {
    vec.push_back(elem);
  }
  std::vector<T>& getVector()
  {
    return vec;
  }
  std::vector<T> vec;
};


#endif //B1_DENIS_FUNCTOR_HPP
