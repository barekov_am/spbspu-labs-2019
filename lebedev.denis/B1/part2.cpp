#include <iostream>
#include <memory>
#include <fstream>
#include <vector>
#include <algorithm>

void part2(const char *argv)
{
  std::ifstream input(argv);
  if (!input)
  {
    throw std::ios_base::failure("Trouble with file");
  }

  const int COUNTREAD = 256;
  size_t capacity = 256;
  size_t i = 0;
  std::unique_ptr<char[], decltype(&free)> array(static_cast<char *>(malloc(capacity)), &free);

  while (input)
  {
    input.read(&array[i], COUNTREAD);
    i += input.gcount();
    if (input.gcount() == COUNTREAD)
    {
      capacity += COUNTREAD;
      std::unique_ptr<char[], decltype(&free)> temp(static_cast<char *>(realloc(array.get(), capacity)), &free);
      if (!temp)
      {
        throw std::ios_base::failure(" Trouble with reallocate ");
      }
      else
      {
        array.release();
        std::swap(array, temp);
      }
    }
  }

  input.close();
  if (input.is_open())
  {
    throw std::ios_base::failure("Trouble with closing file");
  }

  std::vector<char> vector(&array[0], &array[i]);
  std::for_each(vector.begin(), vector.end(), [](char ch)
      {
        std::cout << ch;
      });
}
