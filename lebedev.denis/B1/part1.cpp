#include "sort.hpp"
#include "details.hpp"
#include "functor.hpp"
#include <iterator>
#include <algorithm>
#include <list>
#include <iostream>

void part1(const char* argv)
{
  Functor<int> vec;
  std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), std::ref(vec));
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Trouble with std::cin\n");
  }


  std::vector<int> vec1;
  std::copy(vec.getVector().begin(), vec.getVector().end(), std::back_inserter(vec1));
  std::vector<int> vec2 = vec1;
  std::list<int> list(vec1.begin(), vec1.end());

  bool flag = typeSort(argv);

  sort<AccessAt>(vec1, flag);
  sort<AccessBraccet>(vec2, flag);
  sort<AccessIter>(list, flag);

  print<AccessAt>(vec1);
  print<AccessBraccet>(vec2);
  print<AccessIter>(list);

}
