#ifndef DETAILS_HPP
#define DETAILS_HPP

#include <cstddef>
#include <iostream>

template <typename Container>
struct AccessAt
{
  using typeValue = typename Container::reference;
  using iterator = size_t;

  static iterator begin(Container &)
  {
    return 0;
  }

  static iterator end(Container &container)
  {
    return container.size();
  }

  static typename Container::value_type &getElem(Container &container, std::size_t i)
  {
    return container.at(i);
  }

};

template <typename Container>
struct AccessBraccet
{
  using typeValue = typename Container::reference;
  using iterator = size_t;

  static iterator begin(Container &)
  {
    return 0;
  }

  static iterator end(Container &container)
  {
    return container.size();
  }

  static typename Container::value_type &getElem(Container &container, std::size_t i)
  {
    return container[i];
  }
};

template <typename Container>
struct AccessIter
{
  using typeValue = typename Container::reference;
  using iterator = typename Container::iterator;

  static iterator begin(Container &container)
  {
    return container.begin();
  }

  static iterator end(Container &container)
  {
    return container.end();
  }

  static typename Container::value_type &getElem(Container &, iterator &iter)
  {
    return *iter;
  }
};

template<template<class Container> class Access, class Container>
void print(Container &container)
{
  auto begin = Access<Container>::begin(container);
  auto end = Access<Container>::end(container);
  for (auto i = begin; i != end; ++i)
  {
    std::cout << Access<Container>::getElem(container, i) << " ";
  }
  std::cout << "\n";
}

#endif
