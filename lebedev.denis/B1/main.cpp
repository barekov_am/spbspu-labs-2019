#include <iostream>
#include <string>

void part1(const char* argv);
void part2(const char* argv);
void part3();
void part4(const char* argv, size_t size);

int main(int argc, char* argv[])
{
  try
  {
    int taskNum = std::stoi(argv[1]);
    switch (taskNum)
    {
      case 1:
      {
        if (argc != 3)
        {
          std::cerr << "Trouble with parameters in Part 1\n";
          return 1;
        }
        part1(argv[2]);
      }
        break;

      case 2:
      {
        if (argc != 3)
        {
          std::cerr << "Wrong argc for Part 2\n";
          return 1;
        }
        part2(argv[2]);
      }
        break;

      case 3:
      {
        if (argc != 2)
        {
          std::cerr << "Wrong argc for Part 3\n";
          return 1;
        }
        part3();
      }
        break;

      case 4:
      {
        if (argc != 4)
        {
          std::cerr << "Trouble with parameters in Part 4\n";
          return 1;
        }
        size_t size = std::stoi(argv[3]);
        part4(argv[2], size);
      }
        break;

      default:
      {
        std::cerr << "Incorrect number of task\n";
        return 1;
      }
    }
  }
  catch (std::exception &exception)
  {
    std::cerr << exception.what() << '\n';
    return 1;
  }

  return 0;
}
