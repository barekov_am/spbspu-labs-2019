#include "functor.hpp"
#include <list>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <functional>

void part3()
{
  Functor<int> fun;
  std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), std::ref(fun));
  std::vector<int> vec = fun.vec;

  if (!std::cin.eof())
  {
    throw std::ios_base::failure("Incorrect input\n");
  }

  if (vec.empty())
  {
    return;
  }

  if (vec.back() != 0)
  {
    throw std::invalid_argument("Incorrect Data\n");
  }

  vec.pop_back();

  switch (vec.back())
  {
    case 2:
    {
      auto iter = vec.begin();
      while (iter != vec.end())
      {
        if (*iter % 3 == 0)
        {
          iter = vec.insert(iter + 1, 3, 1);
        }
        else
        {
          ++iter;
        }
      }
    }
      break;
    case 1:
    {
      auto iter = vec.begin();
      while (iter != vec.end())
      {
        if (*iter % 2 == 0)
        {
          iter = vec.erase(iter);
        }
        else
        {
          ++iter;
        }
      }
    }
      break;
    default:
      break;
  }

  std::for_each(vec.begin(), vec.end(), [](int num)
  {
    std::cout << num << " ";
  });
}
