#include <iostream>
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "circle.hpp"
#include "triangle.hpp"

void showParametrsShape(frolov::CompositeShape &compositeShape_)
{
  std::cout << "Area of Shape= " << compositeShape_.getArea() << "\n";
  std::cout << "Frame: " << "\n";
  frolov::rectangle_t frame = compositeShape_.getFrameRect();
  std::cout << "Height= " << frame.height << "\n";
  std::cout << "Width= " << frame.width << "\n";
  std::cout << "Point of Center in x-axis= " << frame.pos.x << "\n";
  std::cout << "Point of Center in y-axis= " << frame.pos.y << "\n";
  compositeShape_.showPoint();
  compositeShape_.move(4, 5);
  compositeShape_.showPoint();
  compositeShape_.move({4, 5});
  std::cout << "Cen= " << compositeShape_.getFrameRect().pos.x << " " << compositeShape_.getFrameRect().pos.y << "\n";
  compositeShape_.scale(2);
  std::cout << "Area after scale = " << compositeShape_.getArea() << "\n";
  std::cout << "Cen= " << compositeShape_.getFrameRect().pos.x << " " << compositeShape_.getFrameRect().pos.y << "\n";
  compositeShape_.remove(1);
  std::cout << "Area after delete one Shape= " << compositeShape_.getArea() << "\n\n";
}

int main()
{
  frolov::CompositeShape compositeShape5;
  frolov::Rectangle rectangle({4, 5}, 4, 4);
  compositeShape5.add(std::make_shared<frolov::Rectangle>(rectangle));
  std::cout << "Area of Composite Shape= " << compositeShape5.getArea() << "\n";
  frolov::Circle circle({6, 9}, 3);
  frolov::CompositeShape compositeShape(std::make_shared<frolov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<frolov::Circle>(circle));
  frolov::Triangle triangle({4, 10},{1, 1},{10, 1});
  std::cout << "Area of Triangle= " << triangle.getArea() << "\n";
  compositeShape.add(std::make_shared<frolov::Triangle>(triangle));
  std::cout << "Area of triangle in Composite= " << compositeShape[2]->getArea() << "\n";
  showParametrsShape(compositeShape);
  frolov::CompositeShape compositeShape2 = compositeShape;
  showParametrsShape(compositeShape2);
  frolov::CompositeShape compositeShape3(compositeShape);
  if (compositeShape3.getArea() != compositeShape2.getArea())
  {
    std::cout << "This is different objects!\n";
  }
  return 0;
}
