#include <sstream>
#include <iostream>
#include "queueImplementation.hpp"

void add(QueueWithPriority<std::string> &queueWithPriority ,std::string priority, std::string data)
{
  if (data.empty() || priority.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  ElementPriority elementPriority;

  if (priority == "low")
  {
    elementPriority = ElementPriority::LOW;
  }
  else if (priority == "normal")
  {
    elementPriority = ElementPriority::NORMAL;
  }
  else if (priority == "high")
  {
    elementPriority = ElementPriority::HIGH;
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queueWithPriority.putElementToQueue(data, elementPriority);
}

void get(QueueWithPriority<std::string> &priority)
{
  if (priority.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }
  std::cout << priority.getElementFromQueue() << "\n";
}

void part1()
{
  QueueWithPriority<std::string> queueWithPriority;
  std::string input;

  while(std::getline(std::cin, input))
  {
    std::istringstream line(input);
    std::string command;
    line >> command;
    if ((std::cin.fail()) && (!std::cin.eof()))
    {
      throw std::ios_base::failure("Trouble with cin in Part 1");
    }
    if (command == "add")
    {
      std::string priority;
      line >> priority >> std::ws;
      std::string data;
      std::getline(line, data);
      add(queueWithPriority, priority, data);
    }
    else if (command == "get")
    {
      get(queueWithPriority);
    }
    else if (command == "accelerate")
    {
      queueWithPriority.accelerate();
    }
    else
    {
      std::cout << "<INVALID COMMAND>";
    }
  }
}
