#ifndef QUEUE_IMPLEMENTATION_HPP
#define QUEUE_IMPLEMENTATION_HPP

#include "QueueWithPriority.hpp"

template <typename T>
void QueueWithPriority<T>::putElementToQueue(const T &name, ElementPriority priority)
{
  switch (priority)
  {
    case ElementPriority::HIGH:
    {
      dequeWithPriority[0].push_back(name);
    }
      break;

    case ElementPriority::NORMAL:
    {
      dequeWithPriority[1].push_back(name);
    }
      break;

    case ElementPriority::LOW:
    {
      dequeWithPriority[2].push_back(name);
    }
      break;
    default:
    {
      std::cerr << "Trouble with Priority in Part1";
      return;
    }
  }
}

template <typename T>
T QueueWithPriority<T>::getElementFromQueue()
{
  for(int i = 0; i < 3; i++)
  {
    if (!dequeWithPriority[i].empty())
    {
      T name = dequeWithPriority[i].front();
      dequeWithPriority[i].pop_front();
      return name;
    }
  }
  throw std::invalid_argument("Queue is empty\n");
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  std::copy(dequeWithPriority[2].begin(), dequeWithPriority[2].end(), std::back_inserter(dequeWithPriority[0]));
  dequeWithPriority[2].clear();
}

template <typename T>
bool QueueWithPriority<T>::empty()
{
  return (dequeWithPriority[0].empty() && dequeWithPriority[1].empty() && dequeWithPriority[2].empty());
}

#endif
