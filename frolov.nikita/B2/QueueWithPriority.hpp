#ifndef QUEUE_WITH_PRIORITY_HPP
#define QUEUE_WITH_PRIORITY_HPP

#include <deque>
#include <iostream>
#include <algorithm>
#include <iterator>

enum ElementPriority
{
  LOW,
  NORMAL,
  HIGH
};

template <typename T>
class QueueWithPriority
{
public:
  void putElementToQueue(const T &name, ElementPriority priority);
  T getElementFromQueue();
  void accelerate();
  bool empty();

private:
  std::deque<T> dequeWithPriority[3];
  //0-High priority
  //1-Normal priority
  //2-Low priority
};

#endif
