#ifndef CONTAINER_HPP
#define CONTAINER_HPP

#include "details.hpp"
#include <vector>

namespace geometry
{
  class Container
  {
  public:
    using shape_ = std::vector<geometry::Point>;

    Container();
    void add(shape_ &shape);
    void deleteAllPentagon();
    void printCountOfShape();
    void printVertexPoint();
    void printShapes();
    void changePlace();
    static double getDistance(geometry::Point point1, geometry::Point point2);
    static double scalarProduct(geometry::Point point1, geometry::Point point2);
    static bool deleterCondion(shape_ &vector);
    static bool changeCondion(shape_ &vector, shape_&vector1);
    static std::pair<bool, bool> typeQuadrangle(shape_ &shape);

  private:
    std::size_t countTriangle;
    std::size_t countRectangle;
    std::size_t countSquares;
    std::size_t countVertices;
    std::vector<shape_> vec;
  };
}

#endif
