#ifndef DETAILS_HPP
#define DETAILS_HPP

#include <string>

namespace geometry
{
  void clearSpace(std::string &word_);

  struct Point
  {
    int x;
    int y;
  };
}
#endif
