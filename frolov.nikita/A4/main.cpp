#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "partition.hpp"
#include "matrix.hpp"


int main()
{
  frolov::Rectangle rectangle({4, 5}, 6, 7);
  frolov::Circle circle({4, 5}, 6);
  frolov::Triangle triangle({4, 6}, {6, 7}, {1, 10});
  std::cout << rectangle.getArea() << std::endl;
  std::cout << circle.getArea() << std::endl;
  std::cout << triangle.getArea() << std::endl;
  frolov::CompositeShape compositeShape(std::make_shared<frolov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<frolov::Circle>(circle));
  compositeShape.add(std::make_shared<frolov::Triangle>(triangle));
  std::cout << compositeShape.getArea() << std::endl;
  std::unique_ptr<frolov::Shape::shapePtr []> shape = std::make_unique<frolov::Shape::shapePtr []>(5);
  frolov::Rectangle rectangle1({2, 4.5}, 1, 2);
  frolov::Rectangle rectangle2({3.5, 2}, 2, 5);
  frolov::Circle circle1({6, 4}, 2);
  frolov::Triangle triangle1({3, 2}, {2, 0}, {4, 0});
  frolov::Triangle triangle2({7, 4}, {9, 3}, {9, 5});
  shape[0] = std::make_shared<frolov::Rectangle>(rectangle1);
  shape[1] = std::make_shared<frolov::Rectangle>(rectangle2);
  shape[2] = std::make_shared<frolov::Circle>(circle1);
  shape[3] = std::make_shared<frolov::Triangle>(triangle1);
  shape[4] = std::make_shared<frolov::Triangle>(triangle2);
  frolov::Matrix matrix = frolov::division(shape, 5);
  matrix.showAll();
  return 0;
}
