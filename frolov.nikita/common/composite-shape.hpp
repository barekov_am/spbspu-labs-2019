#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include <memory>

namespace frolov
{
 class CompositeShape: public frolov::Shape
 {
 public:
   CompositeShape();
   CompositeShape(const CompositeShape &rhs);
   CompositeShape(CompositeShape &&rhs);
   CompositeShape(shapePtr shape);
   ~CompositeShape() = default;

   CompositeShape& operator =(const CompositeShape &rhs);
   CompositeShape& operator =(CompositeShape &&rhs);
   shapePtr operator [](size_t index) const;

   void add(shapePtr shape);
   void remove(size_t i);
   void remove(shapePtr shape);
   size_t getCount() const;
   void move(double dx, double dy) override;
   void move(const point_t &center) override;
   double getArea() const override;
   rectangle_t getFrameRect() const override;
   void showPoint() const override;
   void scale(double factor) override;
   void rotate(double angle) override;
 private:
   size_t countShapes_;
   std::unique_ptr<shapePtr []> shapes_;
 };
}

#endif
