#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(partitionMetodsTests)

BOOST_AUTO_TEST_CASE(partitionTest)
{
  frolov::Rectangle rectangle({2, 4.5}, 1, 2);
  frolov::Rectangle rectangle1({3.5, 2}, 2, 5);
  frolov::Circle circle({6, 4}, 2);

  frolov::CompositeShape compositeShape(std::make_shared<frolov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<frolov::Rectangle>(rectangle1));
  compositeShape.add(std::make_shared<frolov::Circle>(circle));

  frolov::Matrix matrix = frolov::division(compositeShape);

  BOOST_CHECK_EQUAL(matrix.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 2);
}

BOOST_AUTO_TEST_CASE(intersectionTest)
{
  frolov::Rectangle rectangle({2, 4.5}, 1, 2);
  frolov::Rectangle rectangle1({3.5, 2}, 2, 5);
  frolov::Circle circle({6, 4}, 2);

  BOOST_CHECK(!frolov::checkIntersect(rectangle.getFrameRect(), rectangle1.getFrameRect()));
  BOOST_CHECK(!frolov::checkIntersect(rectangle.getFrameRect(), circle.getFrameRect()));
  BOOST_CHECK(frolov::checkIntersect(rectangle1.getFrameRect(), circle.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
