#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"
#include <memory>

namespace frolov
{
  bool checkIntersect(frolov::rectangle_t shape1, frolov::rectangle_t shape2);
  Matrix division(std::unique_ptr<frolov::Shape::shapePtr []> &shapes, size_t size);
  Matrix division(CompositeShape &compositeShape);
}

#endif
