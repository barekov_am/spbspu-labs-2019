#include <boost/test/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

const double Inaccuracy = 0.01;

BOOST_AUTO_TEST_SUITE(testOfRectangle)

BOOST_AUTO_TEST_CASE(widthAndHightAfterMovingToPoint)
{
  frolov::Rectangle rectangle({3, 4}, 5, 5);
  rectangle.move({2, 2});
  BOOST_CHECK_CLOSE(5, rectangle.getFrameRect().width, Inaccuracy);
  BOOST_CHECK_CLOSE(5, rectangle.getFrameRect().height, Inaccuracy);
}

BOOST_AUTO_TEST_CASE(widthAndHightAfterMoving)
{
  frolov::Rectangle rectangle({3, 5}, 7, 7);
  rectangle.move(2, 2);
  BOOST_CHECK_CLOSE(7, rectangle.getFrameRect().width, Inaccuracy);
  BOOST_CHECK_CLOSE(7, rectangle.getFrameRect().height, Inaccuracy);
}

BOOST_AUTO_TEST_CASE(areaAfterMovingToPoint)
{
  frolov::Rectangle rectangle({3, 5}, 7, 7);
  double area = rectangle.getArea();
  rectangle.move({2, 2});
  BOOST_CHECK_CLOSE(area, rectangle.getArea(), Inaccuracy);
}

BOOST_AUTO_TEST_CASE(areaAfterMoving)
{
  frolov::Rectangle rectangle({3, 5}, 7, 7);
  double area = rectangle.getArea();
  rectangle.move(3, 3);
  BOOST_CHECK_CLOSE(area, rectangle.getArea(), Inaccuracy);
}

BOOST_AUTO_TEST_CASE(areaCheckAfterScalingOfRectangle)
{
  frolov::Rectangle rectangle({1, 5}, 4, 3);
  const double areaBeforeScale = rectangle.getArea();
  const double testScale = 5;
  rectangle.scale(testScale);
  BOOST_CHECK_CLOSE(areaBeforeScale * testScale * testScale, rectangle.getArea(), Inaccuracy);
}

BOOST_AUTO_TEST_CASE(invalidArgumentsInRectangle)
{
  BOOST_CHECK_THROW(frolov::Rectangle rectangle({1, 6}, 5, -9), std::invalid_argument);
  BOOST_CHECK_THROW(frolov::Rectangle rectangle({1, 6}, -9, 6), std::invalid_argument);
  frolov::Rectangle rectangle({3, 4}, 3, 4);
  BOOST_CHECK_THROW(rectangle.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(checkRotate)
{
  frolov::Rectangle rectangle({4, 5}, 6, 7);
  double area = rectangle.getArea();
  rectangle.rotate(45);
  BOOST_CHECK_CLOSE(area, rectangle.getArea(), Inaccuracy);

}

BOOST_AUTO_TEST_SUITE_END()
