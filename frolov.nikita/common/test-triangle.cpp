#include <boost/test/unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"

const double Inaccuracy = 0.01;

BOOST_AUTO_TEST_SUITE(testForTriangle)

BOOST_AUTO_TEST_CASE(widthAndHeightAfterMovingToPoint)
{
  frolov::Triangle triangle({3, 4}, {5, 2}, {1, 5});
  frolov::rectangle_t frame = triangle.getFrameRect();
  triangle.move({2, 2});
  BOOST_CHECK_CLOSE(frame.width, triangle.getFrameRect().width, Inaccuracy);
  BOOST_CHECK_CLOSE(frame.height, triangle.getFrameRect().height, Inaccuracy);
}

BOOST_AUTO_TEST_CASE(widthAndHeightAfterMoving)
{
  frolov::Triangle triangle({3, 4}, {5, 2}, {1, 5});
  frolov::rectangle_t frame = triangle.getFrameRect();
  triangle.move(2, 2);
  BOOST_CHECK_CLOSE(frame.width, triangle.getFrameRect().width, Inaccuracy);
  BOOST_CHECK_CLOSE(frame.height, triangle.getFrameRect().height, Inaccuracy);
}

BOOST_AUTO_TEST_CASE(areaAfterMovingToPoint)
{
  frolov::Triangle triangle({3, 4}, {5, 2}, {1, 5});
  double area = triangle.getArea();
  triangle.move({2, 2});
  BOOST_CHECK_CLOSE(area, triangle.getArea(), Inaccuracy);
}

BOOST_AUTO_TEST_CASE(areaAfterMoving)
{
  frolov::Triangle triangle({3, 4}, {5, 2}, {1, 5});
  double area = triangle.getArea();
  triangle.move(3, 3);
  BOOST_CHECK_CLOSE(area, triangle.getArea(), Inaccuracy);
}

BOOST_AUTO_TEST_CASE(areaCheckAfterScalingOfTriangle)
{
  frolov::Triangle triangle({3, 4}, {6, 7}, {1, 5});
  const double areaBeforScaling = triangle.getArea();
  const double testScale = 4;
  triangle.scale(testScale);
  BOOST_CHECK_CLOSE(areaBeforScaling * testScale * testScale, triangle.getArea(), Inaccuracy);
}

BOOST_AUTO_TEST_CASE(invalidArgumentsInTriangle)
{
  BOOST_CHECK_THROW(frolov::Triangle triangle({4, 5}, {4, 6}, {4, 7}), std::invalid_argument);
  BOOST_CHECK_THROW(frolov::Triangle triangle({5, 6}, {1, 6}, {4, 6}), std::invalid_argument);
  frolov::Triangle triangle({4, 5}, {1, 4}, {6, 7});
  BOOST_CHECK_THROW(triangle.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(checkRotate)
{
  frolov::Triangle triangle({4, 5}, {6, 7}, {9, 8});
  double area = triangle.getArea();
  triangle.rotate(45);
  BOOST_CHECK_CLOSE(area, triangle.getArea(), Inaccuracy);
 }

BOOST_AUTO_TEST_SUITE_END()
