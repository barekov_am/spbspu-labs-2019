#include <boost/test/unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

const double Inaccuracy = 0.01;

BOOST_AUTO_TEST_SUITE(testOfCompositeShape)

BOOST_AUTO_TEST_CASE(widthAndHeightAfterMoving)
{
  frolov::Circle circle({3, 4}, 5);
  frolov::Rectangle rectangle({5, 6}, 2, 6);
  frolov::Triangle triangle({4, 6}, {5, 3}, {4, 9});
  frolov::CompositeShape compositeShape(std::make_shared<frolov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<frolov::Circle>(circle));
  compositeShape.add(std::make_shared<frolov::Triangle>(triangle));
  frolov::rectangle_t frameBeforeMoving = compositeShape.getFrameRect();
  double area = compositeShape.getArea();
  compositeShape.move({4, 5});

  BOOST_CHECK_CLOSE(frameBeforeMoving.height, compositeShape.getFrameRect().height, Inaccuracy);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, compositeShape.getFrameRect().width, Inaccuracy);
  BOOST_CHECK_CLOSE(area, compositeShape.getArea(), Inaccuracy);

  compositeShape.move(4, 5);

  BOOST_CHECK_CLOSE(frameBeforeMoving.height, compositeShape.getFrameRect().height, Inaccuracy);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, compositeShape.getFrameRect().width, Inaccuracy);
  BOOST_CHECK_CLOSE(area, compositeShape.getArea(), Inaccuracy);
}

BOOST_AUTO_TEST_CASE(checkCorectAddAndRemove)
{
  frolov::Circle circle({3, 4}, 5);
  frolov::Rectangle rectangle({4, 5}, 6, 8);
  frolov::CompositeShape compositeShape(std::make_shared<frolov::Circle>(circle));
  double areaBeforeAdd = compositeShape.getArea();

  frolov::Shape::shapePtr shape = std::make_shared<frolov::Rectangle>(rectangle);

  compositeShape.add(shape);
  BOOST_CHECK_CLOSE(areaBeforeAdd + rectangle.getArea(), compositeShape.getArea(), Inaccuracy);

  compositeShape.remove(shape);
  BOOST_CHECK_CLOSE(areaBeforeAdd, compositeShape.getArea(), Inaccuracy);
}

BOOST_AUTO_TEST_CASE(outOfRangeException)
{
  frolov::Rectangle rectangle({5, 6}, 7, 7);
  frolov::CompositeShape compositeShape(std::make_shared<frolov::Rectangle>(rectangle));
  BOOST_CHECK_THROW(compositeShape[100], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(areaAfterScaling)
{
  frolov::Circle circle({3, 4}, 5);
  frolov::Rectangle rectangle({5, 6}, 2, 6);
  frolov::Triangle triangle({4, 6}, {5, 3}, {4, 9});
  frolov::CompositeShape compositeShape(std::make_shared<frolov::Circle>(circle));
  compositeShape.add(std::make_shared<frolov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<frolov::Triangle>(triangle));

  double area = compositeShape.getArea();
  compositeShape.scale(2);

  BOOST_CHECK_CLOSE(area * 4, compositeShape.getArea(), Inaccuracy);
}

BOOST_AUTO_TEST_CASE(checkCorrectGetFrameRect)
{
  frolov::Circle circle({6, 5}, 3);
  frolov::Rectangle rectangle({6, 5}, 6, 6);
  frolov::CompositeShape compositeShape(std::make_shared<frolov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<frolov::Circle>(circle));
  frolov::rectangle_t frame = rectangle.getFrameRect();
  //Будем проверять через квадрат, в который вписана окружность
  BOOST_CHECK_CLOSE(frame.height, compositeShape.getFrameRect().height, Inaccuracy);
  BOOST_CHECK_CLOSE(frame.width, compositeShape.getFrameRect().width, Inaccuracy);
  BOOST_CHECK_CLOSE(frame.pos.x, compositeShape.getFrameRect().pos.x, Inaccuracy);
  BOOST_CHECK_CLOSE(frame.pos.y, compositeShape.getFrameRect().pos.y, Inaccuracy);
}

BOOST_AUTO_TEST_CASE(exceptionThrow)
{
  BOOST_CHECK_THROW(frolov::CompositeShape(nullptr), std::invalid_argument);

  frolov::Circle circle({2, 2}, 9);
  frolov::Rectangle rectangle({4, 2}, 3, 5);
  frolov::Triangle triangle({4, 3}, {5, 6}, {3, 8});
  frolov::CompositeShape compositeShape(std::make_shared<frolov::Circle>(circle));
  compositeShape.add(std::make_shared<frolov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<frolov::Triangle>(triangle));
  frolov::CompositeShape compositeShape1;

  BOOST_CHECK_THROW(compositeShape1.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(compositeShape.scale(-1), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.remove(10), std::out_of_range);
  BOOST_CHECK_THROW(compositeShape.remove(nullptr), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(copyAndMove)
{
  frolov::Rectangle rect1({4, 1}, 8, 4);
  frolov::CompositeShape compositeShape1(std::make_shared<frolov::Rectangle>(rect1));
  frolov::CompositeShape compositeShape4;

  BOOST_CHECK_NO_THROW(frolov::CompositeShape compositeShape2(compositeShape1));

  frolov::CompositeShape compositeShape2;

  BOOST_CHECK_NO_THROW(compositeShape2 = compositeShape1);
  BOOST_CHECK_NO_THROW(compositeShape2 = frolov::CompositeShape(std::make_shared<frolov::Rectangle>(rect1)));

  BOOST_CHECK_NO_THROW(frolov::CompositeShape compositeShape3(std::move(compositeShape2)));
  BOOST_CHECK_NO_THROW(compositeShape4 = std::move(compositeShape1));
}

BOOST_AUTO_TEST_SUITE_END()
