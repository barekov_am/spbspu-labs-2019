#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

const double Inaccuracy = 0.01;

BOOST_AUTO_TEST_SUITE(testForMatrix)

BOOST_AUTO_TEST_CASE(copyAndMove)
{
  frolov::Rectangle rectangle({4, 6}, 6, 7);
  frolov::CompositeShape compositeShape(std::make_shared<frolov::Rectangle>(rectangle));
  frolov::Matrix matrix = frolov::division(compositeShape);

  frolov::Matrix matrix3 = matrix;

  BOOST_CHECK_EQUAL(matrix3.getLines(), 1);
  BOOST_CHECK_EQUAL(matrix3.getColumns(),1);
  BOOST_CHECK_NO_THROW(frolov::Matrix matrix1(matrix));
  BOOST_CHECK_NO_THROW(frolov::Matrix matrix2(std::move(matrix)));

  frolov::Matrix matrix4;
  frolov::Matrix matrix5;

  BOOST_CHECK_NO_THROW(matrix4 = matrix);
  BOOST_CHECK_NO_THROW(matrix5 = std::move(matrix));
}

BOOST_AUTO_TEST_CASE(testForThrow)
{
  frolov::Rectangle rectangle({2, 4.5}, 1, 2);
  frolov::Rectangle rectangle1({3.5, 2}, 2, 5);
  frolov::Circle circle({6, 4}, 2);
  frolov::Triangle triangle({3, 2}, {2, 0}, {4, 0});
  frolov::Triangle triangle1({7, 4}, {10, 3}, {10, 5});
  frolov::CompositeShape compositeShape(std::make_shared<frolov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<frolov::Rectangle>(rectangle1));
  compositeShape.add(std::make_shared<frolov::Circle>(circle));
  compositeShape.add(std::make_shared<frolov::Triangle>(triangle));
  compositeShape.add(std::make_shared<frolov::Triangle>(triangle1));

  frolov::Matrix matrix = frolov::division(compositeShape);
  BOOST_CHECK_THROW(matrix[10][10], std::out_of_range);
  BOOST_CHECK_NO_THROW(matrix[0][1]);

}

BOOST_AUTO_TEST_SUITE_END()
