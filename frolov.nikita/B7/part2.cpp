#include "shape.hpp"
#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"
#include <algorithm>
#include <iostream>
#include <vector>
#include <memory>
#include <string>


void clearSpace(std::string &str)
{
  while((!str.empty()) && ((str.at(0) == ' ') || str.at(0) == '\t'))
  {
    str.erase(0, 1);
  }
}

void clearBackSpace(std::string &str)
{
  while((!str.empty()) && ((str.back() == ' ') || str.back() == '\t'))
  {
    str.pop_back();
  }
}

void print(std::vector<std::shared_ptr<Shape>> vec)
{
  std::for_each(vec.begin(), vec.end(), [](std::shared_ptr<Shape> shape1){shape1->draw();});
}

void part2()
{
  std::vector<std::shared_ptr<Shape>> vec;
  std::string line;
  while(getline(std::cin >> std::ws, line))
  {
    if (line.empty())
    {
      continue;
    }
    clearSpace(line);
    auto posFirstBrake = line.find_first_of('(');
    std::string typeShape = line.substr(0, posFirstBrake);
    clearBackSpace(typeShape);
    line.erase(0, posFirstBrake);
    clearSpace(line);
    posFirstBrake = line.find_first_of('(');
    auto posSemicolon = line.find_first_of(';');
    auto posSecondBrake = line.find_first_of(')');

    if ((posSecondBrake == std::string::npos) || (posFirstBrake == std::string::npos) || (posSemicolon == std::string::npos))
    {
      throw std::invalid_argument("Wrong Data\n");
    }

    if (posFirstBrake > posSecondBrake)
    {
      throw std::invalid_argument("Wrong Data\n");
    }

    std::string str = line.substr(posFirstBrake, posSemicolon);

    int x = std::stoi(line.substr(posFirstBrake + 1, posSemicolon - 1));
    int y = std::stoi(line.substr(posSemicolon + 1, posSecondBrake - posSemicolon - 1));
    line.erase(0, posSecondBrake + 1);

    std::shared_ptr<Shape> ptr;
    if (typeShape == "TRIANGLE")
    {
      ptr = std::make_shared<Triangle>(x, y);
    }
    else if (typeShape == "SQUARE")
    {
      ptr = std::make_shared<Square>(x, y);
    }
    else if (typeShape == "CIRCLE")
    {
      ptr = std::make_shared<Circle>(x, y);
    }
    else
    {
      throw std::invalid_argument("Wrong type of Shape\n");
    }

    vec.push_back(ptr);
  }

  std::cout << "Original:\n";
  print(vec);
  std::sort(vec.begin(), vec.end(), [](std::shared_ptr<Shape> shape1, std::shared_ptr<Shape> shape2)->bool
      {
        return shape1->isMoreLeft(shape2);
      });
  std::cout << "Left-Right:\n";
  print(vec);
  std::reverse(vec.begin(), vec.end());
  std::cout << "Right-Left:\n";
  print(vec);
  std::sort(vec.begin(), vec.end(), [](std::shared_ptr<Shape> shape1, std::shared_ptr<Shape> shape2)->bool
      {
        return shape1->isUpper(shape2);
      });
  std::cout << "Top-Bottom:\n";
  print(vec);
  std::reverse(vec.begin(), vec.end());
  std::cout << "Bottom-Top:\n";
  print(vec);
}
