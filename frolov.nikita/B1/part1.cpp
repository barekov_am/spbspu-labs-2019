#include "sort.hpp"
#include "source.hpp"
#include <vector>
#include <forward_list>

void part1(const char* argv)
{
  checkTypeSort(argv);
  std::vector<int> vector;
  int intValue;
  while (std::cin >> intValue)
  {
    vector.push_back(intValue);
  }

  if(std::cin.fail() && !std::cin.eof())
  {
    throw std::ios_base::failure("Trouble with reading std::cin\n");
  }

  if(vector.empty())
  {
    return;
  }

  std::forward_list<int> list(vector.begin(), vector.end());
  std::vector<int> vector1 = vector;

  source::sort_<source::AccessForOperator>(vector, argv);
  source::sort_<source::AccessVecAt>(vector1, argv);
  source::sort_<source::AccessForList>(list, argv);

  source::printArray<source::AccessForOperator>(vector, " ");
  std::cout << "\n";
  source::printArray<source::AccessVecAt>(vector1, " ");
  std::cout << "\n";
  source::printArray<source::AccessForList>(list, " ");
  std::cout << "\n";
}
