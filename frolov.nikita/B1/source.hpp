#ifndef SOURCE_HPP
#define SOURCE_HPP

#include <cstddef>
#include <iostream>
#include <algorithm>
#include <cstring>

void part1(const char* argv);
void part2(const char* argv);
void part3();
void part4(const char* argv, size_t size);

bool checkTypeSort(const char* argv);

namespace source
{
  template<typename VecOp>
  struct AccessForOperator
  {
    using type_index = size_t;
    static typename VecOp::reference getElement(VecOp &vecOp, size_t i)
    {
      return vecOp[i];
    }

    static type_index begin(VecOp &)
    {
      return 0;
    }

    static type_index end(VecOp &vecOp)
    {
      return vecOp.size();
    }
    static void reverse(VecOp &vecOp)
    {
      std::reverse(vecOp.begin(), vecOp.end());
    }
  };

  template<typename VecAt>
  struct AccessVecAt
  {
    using type_index = size_t;
    static typename VecAt::reference getElement(VecAt &vecOp, size_t i)
    {
      return vecOp.at(i);
    }

    static type_index begin(const VecAt &)
    {
      return 0;
    }

    static type_index end(const VecAt &vecOp)
    {
      return vecOp.size();
    }

    static void reverse(VecAt &vecAt)
    {
      std::reverse(vecAt.begin(), vecAt.end());
    }
  };

  template<typename List>
  struct AccessForList
  {
    using type_index = typename List::iterator;
    static typename List::reference getElement(List& , type_index &i)
    {
      return *i;
    }

    static type_index begin(List &list)
    {
      return list.begin();
    }

    static type_index end(List &list)
    {
      return list.end();
    }

    static void reverse(List &list)
    {
      list.reverse();
    }
  };

  template<template<class Container> class Access, class Container>
  void printArray(Container &container, std::string seporator)
  {
    for (auto j = Access<Container>::begin(container); j != Access<Container>::end(container); ++j)
    {
      std::cout << Access<Container>::getElement(container,j) << seporator;
    }
  }
}
#endif
