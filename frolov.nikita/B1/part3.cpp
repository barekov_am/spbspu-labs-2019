#include "source.hpp"
#include <vector>
#include <fstream>

void part3()
{
  std::vector<int> vector;
  int value = 0;
  while (std::cin >> value)
  {
    vector.push_back(value);
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::ios_base::failure("Trouble with reading std::cin\n");
  }

  if (vector.empty())
  {
    return;
  }

  if (vector.back() != 0)
  {
    throw std::invalid_argument("You didn't insert null in end\n");
  }
  else
  {
    vector.pop_back();
  }

  switch (vector.back())
  {
    case 1:
    {
      auto iter = vector.begin();
      while (iter != vector.end())
      {
        iter = ((*iter) % 2 == 0) ? vector.erase(iter) : ++iter;
      }
    }
      break;

    case 2:
    {
      auto iter = vector.begin();
      while (iter != vector.end())
      {
        iter = ((*iter) % 3 == 0) ? vector.insert(iter + 1, 3, 1) : ++iter;
      }
    }
      break;
    default:
      break;
  }

  source::printArray<source::AccessForOperator>(vector, " ");
}
