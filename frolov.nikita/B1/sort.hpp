#ifndef SORT_HPP
#define SORT_HPP

#include "source.hpp"
#include <iostream>
#include <algorithm>

namespace source
{
  template<template<class Container> class Access, class Container>
  void sort_(Container &container, const char* argv)
  {
    auto begin = Access<Container>::begin(container);
    auto end = Access<Container>::end(container);

    for (auto i = begin; i != end; ++i)
    {
      for (auto j = i; j != end; ++j)
      {
        if (Access<Container>::getElement(container, i) > Access<Container>::getElement(container, j))
        {
          std::swap(Access<Container>::getElement(container, i), Access<Container>::getElement(container, j));
        }
      }
    }

    if (strcmp(argv, "descending") == 0)
    {
      Access<Container>::reverse(container);
    }
  }
}
#endif
