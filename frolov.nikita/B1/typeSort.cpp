#include <string.h>
#include <stdexcept>

void checkTypeSort(const char* argv)
{
  if ((strcmp(argv, "descending") != 0) && (strcmp(argv, "ascending") != 0))
  {
    throw std::invalid_argument("Problem with second parameter\n");
  }
}
