#include "source.hpp"
#include "sort.hpp"
#include <random>
#include <vector>

void fillRandom(double *array, size_t size)
{
  std::random_device randomDevice{};
  std::default_random_engine engine{randomDevice()};
  std::uniform_real_distribution<double> distribution{-1.0, 1.0};
  for (size_t i = 0; i < size; i++)
  {
    array[i] = distribution(engine);
    std::cout << array[i] << "  ";
  }
  std::cout << "\n";
}

void part4(const char *argv, size_t size)
{
  if (size == 0)
  {
    throw std::invalid_argument("You insert wrong size for array in Part4\n");
  }

  checkTypeSort(argv);
  
  double *array = new double[size];
  fillRandom(array, size);
  std::vector<double> vector;
  for (size_t i = 0; i < size; i++)
  {
    vector.push_back(array[i]);
  }
  delete[] array;
  source::sort_<source::AccessForOperator>(vector, argv);
  source::printArray<source::AccessForOperator>(vector, " ");
}

