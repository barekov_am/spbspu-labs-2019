#include "source.hpp"
#include <iostream>
#include <cstring>


int main(int argc, char *argv[])
{
  if ((argc < 2) || (argc > 4))
  {
    std::cerr << "The problem with the number of parameters\n";
    return 1;
  }

  try
  {
    int taskNum = std::stoi(argv[1]);
    switch (taskNum)
    {
      case 1:
      {
        if (argc != 3)
        {
          std::cerr << "Trouble with parameters in Part 1\n";
          return 1;
        }
        part1(argv[2]);
      }
        break;

      case 2:
      {
        if (argc != 3)
        {
          std::cerr << "Wrong argc for Part 2\n";
          return 1;
        }
        part2(argv[2]);
      }
        break;

      case 3:
      {
        if (argc != 2)
        {
          std::cerr << "Wrong argc for Part 3\n";
          return 1;
        }
        part3();
      }
        break;

      case 4:
      {
        if (argc != 4)
        {
          std::cerr << "Trouble with parameters in Part 4\n";
          return 1;
        }
        size_t size = std::stoi(argv[3]);
        part4(argv[2], size);
      }
        break;

      default:
      {
        std::cerr << "Incorrect number of task\n";
        return 1;
      }
    }
  }

  catch (std::ios_base::failure &err)
  {
    std::cerr << err.what() << "\n";
    return 1;
  }

  catch (std::invalid_argument &err)
  {
    std::cerr << err.what() << "\n";
    return 1;
  }

  return 0;
}
