#include "phoneBook.hpp"
#include <iostream>


const std::string currentKey = "current";

PhoneBook::PhoneBook()
{
  map_[currentKey] = list_.end();
}

void PhoneBook::goToFirst(const std::string &key)
{
  auto iter = getIter(key);
  iter->second = list_.begin();
}

void PhoneBook::goToLast(const std::string &key)
{
  auto iter = getIter(key);
  iter->second = std::prev(list_.end());
}

void PhoneBook::show(const std::string &key)
{
  auto iter = getIter(key);
  if (iter->second == list_.end())
  {
    std::cout << "<EMPTY>\n";
    return;
  }
  std::cout << iter->second->number << " " << iter->second->name << "\n";
}

void PhoneBook::add(const PhoneBook::Content &content)
{
  list_.push_back(content);
  if (list_.size() == 1)
  {
    map_[currentKey] = list_.begin();
  }
}

void PhoneBook::insertBefore(const std::string &key, const Content &content)
{
  auto iter = getIter(key);
  insert(iter, content);
}

void PhoneBook::insertAfter(const std::string &key, const Content &content)
{
  auto iter = getIter(key);
  insert(iter, content, false);
}

void PhoneBook::next(const std::string &key)
{
  moveTo(key, 1);
}

void PhoneBook::prev(const std::string &key)
{
  moveTo(key, -1);
}

void PhoneBook::moveTo(const std::string &key, int i)
{
  auto iter = getIter(key);
  std::advance(iter->second, i);
}

void PhoneBook::change(const std::string &key, const Content &content)
{
  auto iter = getIter(key);
  *(iter->second) = content;
}

void PhoneBook::remove(const std::string &key)
{
  auto iter = getIter(key);
  if (iter->second != list_.end())
  {
    auto removeElem = iter->second;
    auto iterMap = map_.begin();
    for (size_t i = 0; i < map_.size(); i++)
    {
      if (iterMap->second == removeElem)
      {
        if ((removeElem == std::prev(list_.end())) && (list_.size() > 1))
        {
          iterMap->second = std::prev(removeElem);
        }
        else
        {
          iterMap->second = std::next(removeElem);
        }
      }
      ++iterMap;
    }
    list_.erase(removeElem);
  }
}

void PhoneBook::newKey(const std::string &prevKey, const std::string &newKey)
{
  auto iter = getIter(prevKey);
  auto result = map_.emplace(newKey, iter->second);
  if (result.second == 0)
  {
    std::cout << "You inserted this key later\n";
  }
}

size_t PhoneBook::size()
{
  return list_.size();
}

bool PhoneBook::checkKey(const std::string &key)
{
  if (map_.find(key) == map_.end())
  {
    return false;
  }
  return true;
}

PhoneBook::mapIter PhoneBook::getIter(const std::string &key)
{
  auto iter = map_.find(key);
  if (iter != map_.end())
  {
    return iter;
  }
  throw std::invalid_argument("<INVALID BOOKMARK>");
}

void PhoneBook::insert(PhoneBook::mapIter &iter, const PhoneBook::Content &content, bool flag)
{
  if (list_.size() == 0)
  {
    add(content);
    return;
  }
  if (flag)
  {
    list_.insert(iter->second, content);
    return;
  }
  list_.insert(std::next(iter->second), content);
}
