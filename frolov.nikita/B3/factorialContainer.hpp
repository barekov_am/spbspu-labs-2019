#ifndef FACTORIALCONTAINER_HPP
#define FACTORIALCONTAINER_HPP

#include <iterator>

class FactorialContainer
{
public:
  class Iterator;

  FactorialContainer() = default;

  Iterator begin();
  Iterator end();
};

class FactorialContainer::Iterator : public std::iterator<std::bidirectional_iterator_tag, size_t>
{
public:
  Iterator(size_t id, unsigned int value);
  Iterator(const Iterator &other) = default;

  const unsigned int *operator->() const;
  const unsigned int &operator*() const;

  Iterator &operator++();
  Iterator operator++(int);
  Iterator &operator--();
  Iterator operator--(int);
  Iterator &operator=(const Iterator &object) = default;

  bool operator==(const Iterator &other) const;
  bool operator!=(const Iterator &other) const;

private:
  size_t index_;
  unsigned int value_;
};

#endif
