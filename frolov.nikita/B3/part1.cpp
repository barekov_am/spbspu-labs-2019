#include "phoneBookCommand.hpp"
#include "phoneBook.hpp"
#include <iostream>
#include <sstream>

std::string findKey(std::istream &in)
{
  std::string key;
  in >> key;
  for (auto i : key)
  {
    if ((!std::isalnum(i)) && (i != '-'))
    {
      std::cout << "<INVALID BOOKMARK>";
    }
  }
  return key;
}

void part1()
{
  PhoneBook phoneBook;
  std::string input;

  while (std::getline(std::cin, input))
  {
    PhoneBookCommand phoneBookCommand;
    std::istringstream in(input);
    std::string command;

    in >> command;
    if (command == "add")
    {
      phoneBookCommand.add(phoneBook, in);
    }
    else if (command == "insert")
    {
      std::string direction;
      in >> direction;
      std::string key = findKey(in);
      if (!phoneBook.checkKey(key))
      {
        std::cout << "<INVALID BOOKMARK>\n";
        continue;
      }
      if (direction == "before")
      {
        phoneBookCommand.insertBefore(key, phoneBook, in);
      } else if (direction == "after")
      {
        phoneBookCommand.insertAfter(key, phoneBook, in);
      } else
      {
        std::cout << "<INVALID COMMAND>\n";
      }
    }
    else if (command == "delete")
    {
      std::string key = findKey(in);
      if (!phoneBook.checkKey(key))
      {
        std::cout << "<INVALID BOOKMARK>\n";
        continue;
      }
      phoneBookCommand.remove(key, phoneBook);
    }
    else if (command == "store")
    {
      std::string oldKey = findKey(in);
      if (!phoneBook.checkKey(oldKey))
      {
        std::cout << "<INVALID BOOKMARK>\n";
        continue;
      }
      std::string newKey = findKey(in);
      phoneBookCommand.store(oldKey, newKey, phoneBook);
    }
    else if (command == "move")
    {
      std::string key = findKey(in);
      if (!phoneBook.checkKey(key))
      {
        std::cout << "<INVALID BOOKMARK>\n";
        continue;
      }
      phoneBookCommand.move(key, phoneBook, in);
    }
    else if (command == "show")
    {
      std::string key = findKey(in);
      if (!phoneBook.checkKey(key))
      {
        std::cout << "<INVALID BOOKMARK>\n";
        continue;
      }
      phoneBookCommand.show(key, phoneBook);
    }
    else
    {
     std::cout << "<INVALID COMMAND>\n";
    }
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios::failure("Trouble with cin");
  }
}
