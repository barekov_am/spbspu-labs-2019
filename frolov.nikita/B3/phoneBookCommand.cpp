#include <algorithm>
#include <istream>
#include <iostream>
#include "phoneBookCommand.hpp"

void PhoneBookCommand::add(PhoneBook &phoneBook, std::istream &in)
{
  std::string number = getNumber(in);
  if (number.empty())
  {
    return;
  }
  std::string name = getName(in);
  if (name.empty())
  {
    return;
  }
  phoneBook.add({name, number});
}

void PhoneBookCommand::show(const std::string &key, PhoneBook &phoneBook)
{
  phoneBook.show(key);
}

void PhoneBookCommand::insertBefore(const std::string &key, PhoneBook &phoneBook, std::istream &in)
{
  std::string number = getNumber(in);
  if (number.empty())
  {
    return;
  }
  std::string name = getName(in);
  if (name.empty())
  {
    return;
  }
  phoneBook.insertBefore(key, {name, number});
}

void PhoneBookCommand::insertAfter(const std::string &key, PhoneBook &phoneBook, std::istream &in)
{
  std::string number = getNumber(in);
  if (number.empty())
  {
    return;
  }
  std::string name = getName(in);
  if (name.empty())
  {
    return;
  }
  phoneBook.insertAfter(key, {name, number});
}

void PhoneBookCommand::remove(const std::string &key, PhoneBook &phoneBook)
{
  phoneBook.remove(key);
}

void PhoneBookCommand::move(const std::string &key, PhoneBook &phoneBook, std::istream &in)
{
  std::string step;
  in >> step >> std::ws;
  if (step == "first")
  {
    phoneBook.goToFirst(key);
  }
  else if (step == "last")
  {
    phoneBook.goToLast(key);
  }
  else
  {
    for (size_t i = 0; i < step.size(); i++)
    {
      if (!isdigit(step.at(i)) && (step.at(i) != ('+')) && (step.at(i) != ('-')))
      {
        std::cout << "<INVALID STEP>\n";
        return;
      }
    }
    int pos = std::stoi(step);

    phoneBook.moveTo(key, pos);
  }
}

void PhoneBookCommand::store(const std::string &oldKey, const std::string &newKey, PhoneBook &phoneBook)
{
  phoneBook.newKey(oldKey, newKey);
}

std::string PhoneBookCommand::getName(std::istream &in)
{
  std::string name;
  std::getline(in, name);

  if (name.empty())
  {
    return name;
  }

  if ((name.front() != '\"') || (name.back() != '\"'))
  {
    std::cout << "<INVALID COMMAND>\n";
    name.clear();
    return name;
  }

  auto iter = name.begin();
  while (iter != name.end()){
    if ((*iter == '\\') && ((*(iter+1) == '\\') || (*(iter+1) == '"'))){
      iter = name.erase(iter) + 1;
    }
    else if (*iter == '"'){
      iter = name.erase(iter);
    }
    else
    {
      ++iter;
    }
  }

  return name;
}

std::string PhoneBookCommand::getNumber(std::istream &in)
{
  std::string number;
  in >> number >> std::ws;
  for (size_t i = 0; i < number.size(); i++)
  {
    if (!isdigit(number.at(i)))
    {
      std::cout << "<INVALID COMMAND>\n";
      number.clear();
      break;
    }
  }
  return number;
}

