#include "factorialContainer.hpp"

FactorialContainer::Iterator::Iterator(size_t index, unsigned int value)
{
  index_ = index;
  value_ = value;
}

const unsigned int *FactorialContainer::Iterator::operator->() const
{
  return &value_;
}

const unsigned int &FactorialContainer::Iterator::operator*() const
{
  return value_;
}

FactorialContainer::Iterator FactorialContainer::begin()
{
  return FactorialContainer::Iterator(1, 1);
}

FactorialContainer::Iterator FactorialContainer::end()
{
  return FactorialContainer::Iterator(11, 39916800);
}

FactorialContainer::Iterator &FactorialContainer::Iterator::operator++()
{
  if (index_ < 11) {
    index_++;
    value_ *= index_;
  }
  return *this;
}

FactorialContainer::Iterator FactorialContainer::Iterator::operator++(int)
{
  Iterator prev = *this;
  ++(*this);
  return prev;
}

FactorialContainer::Iterator &FactorialContainer::Iterator::operator--()
{
  if (index_ > 1) {
    value_ /= index_;
    index_--;
  }
  return *this;
}

FactorialContainer::Iterator FactorialContainer::Iterator::operator--(int)
{
  Iterator prev = *this;
  --(*this);
  return prev;
}

bool FactorialContainer::Iterator::operator==(const Iterator &other) const
{
  return (index_ == other.index_);
}

bool FactorialContainer::Iterator::operator!=(const Iterator &other) const
{
  return (!(other == *this));
}
