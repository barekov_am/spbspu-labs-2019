#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <list>
#include <map>
#include <string>
#include <stdexcept>

class PhoneBook
{
public:

  struct Content
  {
    std::string name;
    std::string number;
  };

  PhoneBook();
  void goToFirst(const std::string &key);
  void goToLast(const std::string &key);
  void show(const std::string &key);
  void add(const Content &content);
  void insertBefore(const std::string &key, const Content &content);
  void insertAfter(const std::string &key, const Content &content);
  void next(const std::string &key);
  void prev(const std::string &key);
  void moveTo(const std::string &key, int i);
  void change(const std::string &key, const Content &content);
  void remove(const std::string &key);
  void newKey(const std::string &prevKey, const std::string &newKey);
  bool checkKey(const std::string &key);
  size_t size();

private:
  using mapIter = std::map<std::string, std::list<Content>::iterator>::iterator;
  std::list<Content> list_;
  std::map<std::string, std::list<Content>::iterator> map_;

  mapIter getIter(const std::string &key);
  void insert(mapIter &iter, const Content &content, bool flag = true);
};

#endif
