#ifndef PHONEBOOKCOMMAND_HPP
#define PHONEBOOKCOMMAND_HPP

#include "phoneBook.hpp"

class PhoneBookCommand
{
public:
  void add(PhoneBook &phoneBook, std::istream &in);
  void insertBefore(const std::string &key, PhoneBook &phoneBook, std::istream &in);
  void insertAfter(const std::string &key, PhoneBook &phoneBook, std::istream &in);
  void remove(const std::string &key, PhoneBook &phoneBook);
  void show(const std::string &key, PhoneBook &phoneBook);
  void move(const std::string &key, PhoneBook &phoneBook, std::istream &in);
  void store(const std::string &oldKey, const std::string &newKey, PhoneBook &phoneBook);
private:
  std::string getName(std::istream &in);
  std::string getNumber(std::istream &in);
};

#endif
