#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iosfwd>
#include <memory>
#include "point.hpp"

class Shape
{
public:
  typedef std::shared_ptr<Shape> ShapePtr;
  typedef std::shared_ptr<const Shape> ConstShapePtr;
  Shape(const Point& center);
  virtual ~Shape() = default;
  bool isMoreLeft(ConstShapePtr rhs) const;
  bool isUpper(ConstShapePtr rhs) const;
  virtual void draw(std::ostream& out) const = 0;
protected:
  Point center_;
};

#endif // SHAPE_HPP
