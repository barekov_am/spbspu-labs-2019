#include "shape.hpp"
#include <iostream>

Shape::Shape(const Point& center) :
  center_(center)
{
}

bool Shape::isMoreLeft(ConstShapePtr rhs) const
{
  return center_.x < (*rhs).center_.x;
}

bool Shape::isUpper(ConstShapePtr rhs) const
{
  return center_.y > (*rhs).center_.y;
}

void Shape::draw(std::ostream &out) const
{
  out << " " << center_;
}
