#include <algorithm>
#include <iterator>
#include <iostream>
#include <cmath>
#include <functional>

void task1()
{
  std::transform((std::istream_iterator<float>(std::cin)), std::istream_iterator<float>(),
    std::ostream_iterator<float>(std::cout, "\n"), std::bind1st(std::multiplies<float>(), M_PI));
  if (!std::cin.eof() && std::cin.fail()) {
    throw std::runtime_error("Input error");
  }
}
