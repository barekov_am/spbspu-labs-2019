#include <iostream>
#include <iterator>
#include "shape-io.hpp"

void task2()
{
  std::list<Shape::ShapePtr> shapes((std::istream_iterator<Shape::ShapePtr>(std::cin)), std::istream_iterator<Shape::ShapePtr>());
  if (std::cin.fail() && !std::cin.eof()) {
    throw std::runtime_error("Input error");
  }
  std::cout << "Original:\n";
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator<Shape::ShapePtr>(std::cout, "\n"));
  shapes.sort([](const Shape::ShapePtr& lhs, const Shape::ShapePtr& rhs) {return lhs->isMoreLeft(rhs);});
  std::cout << "Left-Right:\n";
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator<Shape::ShapePtr>(std::cout, "\n"));
  shapes.reverse();
  std::cout << "Right-Left:\n";
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator<Shape::ShapePtr>(std::cout, "\n"));
  shapes.sort([](const Shape::ShapePtr& lhs, const Shape::ShapePtr& rhs) {return lhs->isUpper(rhs);});
  std::cout << "Top-Bottom:\n";
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator<Shape::ShapePtr>(std::cout, "\n"));
  shapes.reverse();
  std::cout << "Bottom-Top:\n";
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator<Shape::ShapePtr>(std::cout, "\n"));
}
