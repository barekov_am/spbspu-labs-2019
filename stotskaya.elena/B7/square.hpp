#ifndef SQUARE_HPP
#define SQUARE_HPP

#include <iosfwd>
#include "shape.hpp"

class Square : public Shape
{
public:
  Square(const Point& center);
  void draw(std::ostream& out) const override;
};

#endif // SQUARE_HPP
