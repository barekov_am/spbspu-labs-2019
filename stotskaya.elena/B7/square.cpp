#include "square.hpp"
#include <iostream>

Square::Square(const Point& center):
  Shape(center)
{
}

void Square::draw(std::ostream& out) const
{
  out << "SQUARE";
  Shape::draw(out);
}
