#include "triangle.hpp"
#include <iostream>

Triangle::Triangle(const Point& center):
  Shape(center)
{
}

void Triangle::draw(std::ostream& out) const
{
  out << "TRIANGLE";
  Shape::draw(out);
}
