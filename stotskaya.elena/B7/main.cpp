#include <iostream>
#include <stdexcept>

void task1();
void task2();

int main(int argc, char* argv[])
{
  try {
    if (argc != 2) {
      throw std::invalid_argument("Incorrect number of parameters");
    }
    char* end = nullptr;
    long task = std::strtol(argv[1], &end, 10);
    if (end == nullptr || *end != '\0') {
      throw std::invalid_argument("Parameter is not an integer");
    }
    switch (task)
    {
      case 1:
        task1();
        break;
      case 2:
        task2();
        break;
      default:
        throw std::invalid_argument("Incorrect task number");
    }
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}
