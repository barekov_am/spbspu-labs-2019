#ifndef SHAPEIO_HPP
#define SHAPEIO_HPP

#include <list>
#include <string>
#include <iosfwd>
#include <map>
#include <functional>
#include "shape.hpp"

class ShapeCreator
{
public:
  ShapeCreator();
  Shape::ShapePtr operator()(Point center);
private:
  std::string shapeType;
  std::map<std::string, std::function<Shape::ShapePtr(const Point&)>> typeSelect;
  friend std::istream& operator>>(std::istream& in, ShapeCreator& creator);
};

std::istream& operator>>(std::istream& in, Shape::ShapePtr& shape);
std::ostream& operator<<(std::ostream& out, const Shape::ShapePtr& shape);

#endif // SHAPELIST_HPP
