#include "circle.hpp"
#include <iostream>

Circle::Circle(const Point& center):
  Shape(center)
{
}

void Circle::draw(std::ostream& out) const
{
  out << "CIRCLE";
  Shape::draw(out);
}
