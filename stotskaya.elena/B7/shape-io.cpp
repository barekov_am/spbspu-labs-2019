#include <iostream>
#include <stdexcept>
#include "shape-io.hpp"
#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"
#include "blank.hpp"

std::istream& operator>>(std::istream& in, ShapeCreator& creator)
{
  std::istream::sentry sentry(in);
  if (!sentry) {
    return in;
  }
  std::streambuf* inBuf = in.rdbuf();
  char current = inBuf->sgetc();
  while (std::isalpha(current)) {
    creator.shapeType.append(1, current);
    current = inBuf->snextc();
  }
  if (in.fail()) {
    in.clear(std::ios::failbit);
  }
  return in;
}

ShapeCreator::ShapeCreator()
{
  typeSelect = {
    {"CIRCLE", [](const Point& center) {return std::shared_ptr<Circle>(new Circle(center));}},
    {"SQUARE", [](const Point& center) {return std::shared_ptr<Square> (new Square(center));}},
    {"TRIANGLE", [](const Point& center) {return std::shared_ptr<Triangle> (new Triangle(center));}}
  };
}

Shape::ShapePtr ShapeCreator::operator()(Point center)
{
  auto typeIter = typeSelect.at(shapeType);
  return typeIter(center);
}

std::istream& operator>>(std::istream& in, Shape::ShapePtr& shape)
{
  ShapeCreator creator;
  Point center;
  if (in >> blank >> creator) {
    if (in >> center) {
      shape = creator(center);
    } else {
      in.clear(std::ios::failbit);
    }
  }
  return in;
}

std::ostream& operator<<(std::ostream& out, const Shape::ShapePtr& shape)
{
  if (shape) {
    shape->draw(out);
  }
  return out;
}
