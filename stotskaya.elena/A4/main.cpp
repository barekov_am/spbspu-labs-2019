#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "layers.hpp"

int main()
{
  stotskaya::Rectangle rect1({1.0, 2.0}, 20.0, 10.0, 0.0);
  rect1.move(3.5, -2.0);
  stotskaya::rectangle_t frame1 = rect1.getFrameRect();
  std::cout << "rect1 frame position: " << frame1.pos.x << ", " << frame1.pos.y << "\n";
  stotskaya::Shape* shapeP = &rect1;
  std::cout << "rect1 area: " << shapeP->getArea() << "\n";
  stotskaya::Circle circle1({1.0, 2.5}, 4.0);
  shapeP = &circle1;
  stotskaya::rectangle_t frame2 = shapeP->getFrameRect();
  std::cout << "circle1 frame size: " << frame2.width << "*" << frame2.height << "\n";
  shapeP->move({3.2, 3.4});
  frame2 = circle1.getFrameRect();
  std::cout << "circle1 frame position: " << frame2.pos.x << ", " << frame2.pos.y << "\n";
  std::cout << "circle1 area: " << circle1.getArea() << "\n";
  rect1.scale(3.0);
  std::cout << "rect1 area: " << rect1.getArea() << "\n";
  circle1.scale(0.4);
  std::cout << "circle1 area: " << circle1.getArea() << "\n";
  rect1.rotate(45.0);
  frame1 = rect1.getFrameRect();
  std::cout << "rect1 frame size after rotating: " << frame1.width << "*" << frame1.height << "\n";
  circle1.rotate(45.0);
  frame2 = circle1.getFrameRect();
  std::cout << "circle1 frame size after rotating: " << frame2.width << "*" << frame2.height << "\n\n";

  rect1.scale(0.5);
  rect1.move({10.0, 5.0});
  rect1.rotate(-45.0);
  circle1.scale(7);
  circle1.move({30.0, 10.0});
  stotskaya::CompositeShape composite1;
  composite1.addShape(std::make_shared<stotskaya::Rectangle>(rect1));
  frame1 = composite1.getFrameRect();
  std::cout << "composite1 frame position: " << frame1.pos.x << ", " << frame1.pos.y << "\n";
  std::cout << "composite1 frame size: " << frame1.width << "*" << frame1.height << "\n";
  std::cout << "composite1 area: " << composite1.getArea() << "\n\n";

  composite1.addShape(std::make_shared<stotskaya::Circle>(circle1));
  frame1 = composite1.getFrameRect();
  std::cout << "composite1 frame position after adding circle1: " << frame1.pos.x << ", " << frame1.pos.y << "\n";
  std::cout << "composite1 frame size after adding circle1: " << frame1.width << "*" << frame1.height << "\n";
  std::cout << "composite1 area after adding circle1: " << composite1.getArea() << "\n\n";

  composite1.scale(2.5);
  frame1 = composite1.getFrameRect();
  std::cout << "composite1 frame position after scaling: " << frame1.pos.x << ", " << frame1.pos.y << "\n";
  std::cout << "composite1 frame size after scaling: " << frame1.width << "*" << frame1.height << "\n";
  std::cout << "composite1 area after scaling: " << composite1.getArea() << "\n\n";

  composite1.move({3.0, -3.0});
  frame1 = composite1.getFrameRect();
  std::cout << "composite1 frame position after moving: " << frame1.pos.x << ", " << frame1.pos.y << "\n";
  std::cout << "composite1 frame size after moving: " << frame1.width << "*" << frame1.height << "\n";
  std::cout << "composite1 area after moving: " << composite1.getArea() << "\n\n";

  composite1.rotate(90);
  frame1 = composite1.getFrameRect();
  std::cout << "composite1 frame position after rotating: " << frame1.pos.x << ", " << frame1.pos.y << "\n";
  std::cout << "composite1 frame size after rotating: " << frame1.width << "*" << frame1.height << "\n";
  std::cout << "composite1 area after rotating: " << composite1.getArea() << "\n\n";

  stotskaya::Rectangle rect2({100.0, -100.0}, 5.0, 4.0, 0.0);
  composite1.addShape(std::make_shared<stotskaya::Rectangle>(rect2));
  stotskaya::Matrix<stotskaya::Shape> composite1Layers = stotskaya::splitLayers(composite1);
  for (unsigned int i = 0; i < composite1Layers.getRows(); i++) {
    std::cout << "Layer " << i << ":\n";
    for (unsigned int j = 0; j < composite1Layers[i].getCount(); j++) {
      stotskaya::rectangle_t elementFrame = composite1Layers[i][j]->getFrameRect();
      std::cout << "Shape " << j << ", center at (" << elementFrame.pos.x << ", " << elementFrame.pos.y
          << "), width: " << elementFrame.width << ", height: " << elementFrame.height << "\n";
    }
    std::cout << "\n";
  }

  composite1.deleteShape(0);
  frame1 = composite1.getFrameRect();
  std::cout << "composite1 frame position after deleting rect1: " << frame1.pos.x << ", " << frame1.pos.y << "\n";
  std::cout << "composite1 frame size after deleting rect1: " << frame1.width << "*" << frame1.height << "\n";
  std::cout << "composite1 area after deleting rect1: " << composite1.getArea();
  return 0;
}
