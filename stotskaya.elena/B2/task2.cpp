#include <iostream>
#include <list>

void printBeginEnd(std::list<int>::iterator begin, std::list<int>::iterator end)
{
  if (end != begin) {
    std::cout << *begin << ' ' << *end;
    if (++begin != end) {
      std::cout << ' ';
      printBeginEnd(begin, --end);
    } else {
      std::cout << "\n";
    }
  } else {
    std::cout << *begin << "\n";
  }
}

void task2()
{
  std::list<int> list;
  int num = 0;
  int count = 0;
  while (std::cin && !(std::cin >> num).eof()) {
    if (std::cin.fail()) {
      throw std::runtime_error("Input error");
    }
    if (num < 1 || num > 20) {
      throw std::invalid_argument("Incorrect list element");
    }
    list.push_back(num);
    ++count;
  }
  if (count > 20) {
    throw std::runtime_error("Too many elements");
  }
  if (!list.empty()) {
    printBeginEnd(list.begin(), --(list.end()));
  }
}
