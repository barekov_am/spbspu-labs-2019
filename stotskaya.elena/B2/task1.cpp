#include <iostream>
#include <string>
#include <map>
#include <functional>
#include "queue-with-priority.hpp"

void addElement(QueueWithPriority<std::string>& queue);
void getElement(QueueWithPriority<std::string>& queue);
void accelerate(QueueWithPriority<std::string>& queue);

void task1()
{
  QueueWithPriority<std::string> queue;
  std::map<std::string, std::function<void(QueueWithPriority<std::string>&)>> commandMap = {
    {"add", &addElement},
    {"get", &getElement},
    {"accelerate", &accelerate}};
  std::string command = "";
  while (std::cin >> command) {
    try
    {
      auto commandEl = commandMap.at(command);
      commandEl(queue);
    }
    catch(std::out_of_range)
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
