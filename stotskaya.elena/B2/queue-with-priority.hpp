#ifndef QUEUEWITHPRIORITY_HPP
#define QUEUEWITHPRIORITY_HPP

#include <list>
#include <stdexcept>
#include <functional>

template <typename QueueElement>
class QueueWithPriority
{
public:
  typedef enum
  {
    LOW,
    NORMAL,
    HIGH
  } ElementPriority;

  void putElementToQueue(const QueueElement& element, ElementPriority priority);
  void apply(std::function<void(QueueElement)> funcInterface);
  void accelerate();
  bool isEmpty() const;

private:
  std::list<QueueElement> highPriorityElements;
  std::list<QueueElement> normalPriorityElements;
  std::list<QueueElement> lowPriorityElements;
};

template <typename QueueElement>
void QueueWithPriority<QueueElement>::putElementToQueue(const QueueElement& element, ElementPriority priority)
{
  switch (priority)
  {
    case HIGH:
      highPriorityElements.push_back(element);
      break;
    case NORMAL:
      normalPriorityElements.push_back(element);
      break;
    case LOW:
      lowPriorityElements.push_back(element);
      break;
  }
}

template <typename QueueElement>
void QueueWithPriority<QueueElement>::apply(std::function<void(QueueElement)> funcInterface)
{
  if (!(highPriorityElements.empty())) {
    funcInterface(highPriorityElements.front());
    highPriorityElements.pop_front();
  } else if (!(normalPriorityElements.empty())) {
    funcInterface(normalPriorityElements.front());
    normalPriorityElements.pop_front();
  } else if (!(lowPriorityElements.empty())) {
    funcInterface(lowPriorityElements.front());
    lowPriorityElements.pop_front();
  } else {
    throw std::runtime_error("Reading from empty queue");
  }
}

template <typename QueueElement>
void QueueWithPriority<QueueElement>::accelerate()
{
  highPriorityElements.splice(highPriorityElements.end(), lowPriorityElements);
}

template <typename QueueElement>
bool QueueWithPriority<QueueElement>::isEmpty() const
{
  return (lowPriorityElements.empty() && normalPriorityElements.empty() && highPriorityElements.empty());
}

#endif // QUEUEWITHPRIORITY_HPP
