#include <iostream>
#include "queue-with-priority.hpp"

void addElement(QueueWithPriority<std::string>& queue)
{
  std::string priority = "";
  std::string content = "";
  std::cin >> std::ws >> priority;
  std::getline(std::cin >> std::ws, content);
  if (content == "") {
    std::cout << "<INVALID COMMAND>\n";
  } else {
    if (priority == "low") {
      queue.putElementToQueue(content, QueueWithPriority<std::string>::LOW);
    } else if (priority == "normal") {
      queue.putElementToQueue(content, QueueWithPriority<std::string>::NORMAL);
    } else if (priority == "high") {
      queue.putElementToQueue(content, QueueWithPriority<std::string>::HIGH);
    } else {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
  content = "";
}

void getElement(QueueWithPriority<std::string>& queue)
{
  if (queue.isEmpty()) {
    std::cout << "<EMPTY>\n";
  } else {
    queue.apply([](std::string element) {
      std::cout << element << "\n";
    });
  }
}

void accelerate(QueueWithPriority<std::string>& queue)
{
  queue.accelerate();
}
