#include <iostream>
#include <limits>
#include <iterator>
#include "shape.hpp"
#include "blank.hpp"

std::istream& operator>>(std::istream& in, Shape& shape)
{
  std::istream::sentry sentry(in);
  if (!sentry) {
    return in;
  }
  int vertexCount = 0;
  if (!(in >> vertexCount)) {
    in.clear(std::ios::failbit);
    return in;
  }
  if (vertexCount < 0) {
    in.clear(std::ios::failbit);
    return in;
  }
  if ((in >> blank).peek() == '\n' && vertexCount != 0) {
    in.clear(std::ios::failbit);
    return in;
  }
  Shape tempShape(vertexCount);
  for (int i = 0; i < vertexCount; ++i) {
    Point tempPoint;
    if (!(in >> tempPoint)) {
      in.clear(std::ios::failbit);
      return in;
    }
    tempShape[i] = tempPoint;
  }
  shape = tempShape;
  return in;
}

std::ostream& operator<<(std::ostream& out, const Shape& shape)
{
  out << shape.size() << ' ';
  std::copy(shape.begin(), shape.end(), std::ostream_iterator<Point>(out, " "));
  return out;
}

std::size_t addSize(std::size_t sum, Shape& shape)
{
  return sum + shape.size();
}

bool hasEqualPairsSides(Shape& shape)
{
  return (squareDistance(shape[0], shape[1]) == squareDistance(shape[2], shape[3]))
    && (squareDistance(shape[0], shape[3]) == squareDistance(shape[1], shape[2]));
}

bool hasEqualAdjacentSides(Shape& shape)
{
  return squareDistance(shape[0], shape[1]) == squareDistance(shape[0], shape[3]);
}

ShapeType getType(Shape& shape)
{
  if (shape.size() == 3) {
    return ShapeType::TRIANGLE;
  }
  if (shape.size() == 4 && hasEqualPairsSides(shape)) {
    if (hasEqualAdjacentSides(shape)) {
      return ShapeType::SQUARE;
    }
    return ShapeType::RECTANGLE;
  }
  if (shape.size() == 5) {
    return ShapeType::PENTAGON;
  }
  return ShapeType::OTHER;
}
