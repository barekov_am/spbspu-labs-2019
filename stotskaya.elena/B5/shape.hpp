#ifndef SHAPEDEF_HPP
#define SHAPEDEF_HPP

#include <vector>
#include <string>
#include <iosfwd>
#include "point.hpp"

using Shape = std::vector<Point>;

enum class ShapeType
{
  TRIANGLE,
  SQUARE,
  RECTANGLE,
  PENTAGON,
  OTHER
};

std::istream& operator>>(std::istream& in, Shape& shape);
std::ostream& operator<<(std::ostream& out, const Shape& shape);

std::size_t addSize(std::size_t sum, Shape& shape);

ShapeType getType(Shape& shape);

#endif // SHAPEDEF_HPP
