#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <list>
#include "shape.hpp"

void task2()
{
  std::list<Shape> shapeList((std::istream_iterator<Shape>(std::cin)), std::istream_iterator<Shape>());
  if (!std::cin.eof() && std::cin.fail()) {
    throw std::runtime_error("Input error");
  }
  int vertexSum = std::accumulate(shapeList.begin(), shapeList.end(), 0, &addSize);
  int triangleCount = 0;
  int squareCount = 0;
  int rectangleCount = 0;
  std::for_each(shapeList.begin(), shapeList.end(), [&](Shape& shape)
  {
    ShapeType currentType = getType(shape);
    if (currentType == ShapeType::TRIANGLE) {
      ++triangleCount;
    } else if (currentType == ShapeType::SQUARE) {
      ++squareCount;
      ++rectangleCount;
    } else if (currentType == ShapeType::RECTANGLE) {
      ++rectangleCount;
    }
  });
  shapeList.erase(std::remove_if(shapeList.begin(), shapeList.end(),
    [](Shape& shape){return getType(shape) == ShapeType::PENTAGON;}), shapeList.end());
  std::vector<Point> firstPoints(shapeList.size());
  std::transform(shapeList.begin(), shapeList.end(), firstPoints.begin(), [](Shape& shape){return  shape[0];});
  shapeList.sort([](Shape& lhs, Shape& rhs) {return getType(lhs) < getType(rhs);});
  std::cout << "Vertices: " << vertexSum << "\n";
  std::cout << "Triangles: " << triangleCount << "\n";
  std::cout << "Squares: " << squareCount << "\n";
  std::cout << "Rectangles: " << rectangleCount << "\n";
  std::cout << "Points: ";
  std::copy(firstPoints.begin(), firstPoints.end(), std::ostream_iterator<Point>(std::cout, " "));
  std::cout << "\n";
  std::cout << "Shapes:\n";
  std::copy(shapeList.begin(), shapeList.end(), std::ostream_iterator<Shape>(std::cout, "\n"));
}
