#include <iostream>
#include <set>
#include <iterator>
#include <stdexcept>

void task1()
{
  std::set<std::string> words((std::istream_iterator<std::string>(std::cin)), std::istream_iterator<std::string>());
  if (!std::cin.eof() && std::cin.fail()) {
    throw std::runtime_error("Input error");
  }
  std::copy(words.begin(), words.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
}
