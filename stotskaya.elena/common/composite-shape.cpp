#define _USE_MATH_DEFINES

#include "composite-shape.hpp"
#include <algorithm>
#include <cmath>
#include <stdexcept>

stotskaya::CompositeShape::CompositeShape():
  shapeCount_(0)
{
}

stotskaya::CompositeShape::CompositeShape(const CompositeShape& initialShape):
  shapeList_(std::make_unique<Shape::ptr[]>(initialShape.shapeCount_)),
  shapeCount_(initialShape.shapeCount_)
{
  for (size_t i = 0; i < shapeCount_; i++) {
    shapeList_[i] = initialShape.shapeList_[i];
  }
}

stotskaya::CompositeShape::CompositeShape(CompositeShape&& initialShape):
  shapeList_(std::move(initialShape.shapeList_)),
  shapeCount_(initialShape.shapeCount_)
{
  initialShape.shapeCount_ = 0;
}

stotskaya::CompositeShape& stotskaya::CompositeShape::operator=(const CompositeShape& initialShape)
{
  if (&initialShape != this) {
    shapeList_ = std::make_unique<Shape::ptr[]>(initialShape.shapeCount_);
    for (size_t i = 0; i < shapeCount_; i++) {
      shapeList_[i] = initialShape.shapeList_[i];
    }
    shapeCount_ = initialShape.shapeCount_;
  }
  return *this;
}

stotskaya::CompositeShape& stotskaya::CompositeShape::operator=(CompositeShape&& initialShape)
{
  if (&initialShape != this) {
    shapeList_ = std::move(initialShape.shapeList_);
    shapeCount_ = initialShape.shapeCount_;
    initialShape.shapeCount_ = 0;
  }
  return *this;
}

stotskaya::Shape::ptr stotskaya::CompositeShape::operator[](size_t shapeIndex) const
{
  if (shapeIndex >= shapeCount_) {
    throw std::out_of_range("Incorrect index");
  }
  return shapeList_[shapeIndex];
}

void stotskaya::CompositeShape::addShape(Shape::ptr newShape)
{
  if (newShape == nullptr) {
    throw std::invalid_argument("Cannot add nullptr to shapeList");
  }
  stotskaya::Shape::array tmpList = std::make_unique<Shape::ptr[]>(shapeCount_ + 1);
  for (size_t i = 0; i < shapeCount_; i++) {
    tmpList[i] = shapeList_[i];
  }
  shapeList_ = std::move(tmpList);
  shapeCount_++;
  shapeList_[shapeCount_ - 1] = newShape;
}

void stotskaya::CompositeShape::deleteShape(size_t index)
{
  if (index >= shapeCount_) {
    throw std::out_of_range("Incorrect index");
  }
  for(size_t i = index; i < shapeCount_ - 1; i++) {
    shapeList_[i] = shapeList_[i + 1];
  }
  shapeList_[shapeCount_ - 1] = nullptr;
  shapeCount_--;
}

double stotskaya::CompositeShape::getArea() const
{
  double area = 0.0;
  for (size_t i = 0; i < shapeCount_; i++) {
    area += shapeList_[i]->getArea();
  }
  return area;
}

stotskaya::rectangle_t stotskaya::CompositeShape::getFrameRect() const
{
  if (shapeCount_ == 0) {
    return {0, 0, {0, 0}};
  }
  rectangle_t tempFrame = shapeList_[0]->getFrameRect();
  double minX = tempFrame.pos.x - tempFrame.width / 2;
  double minY = tempFrame.pos.y - tempFrame.height / 2;
  double maxX = tempFrame.pos.x + tempFrame.width / 2;
  double maxY = tempFrame.pos.y + tempFrame.height / 2;
  for (size_t i = 1; i < shapeCount_; i++) {
    tempFrame = shapeList_[i]->getFrameRect();
    if ((tempFrame.width > 0) && (tempFrame.height > 0)) {
      double tempCoord = tempFrame.pos.x - tempFrame.width / 2;
      minX = std::min(minX, tempCoord);
      tempCoord = tempFrame.pos.y - tempFrame.height / 2;
      minY = std::min(minY, tempCoord);
      tempCoord = tempFrame.pos.x + tempFrame.width / 2;
      maxX = std::max(maxX, tempCoord);
      tempCoord = tempFrame.pos.y + tempFrame.height / 2;
      maxY = std::max(maxY, tempCoord);
    }
  }
  return {maxX - minX, maxY - minY, {(maxX + minX) / 2, (maxY + minY) / 2}};
}

size_t stotskaya::CompositeShape::getShapeCount() const
{
  return shapeCount_;
}

void stotskaya::CompositeShape::move(const point_t& point)
{
  rectangle_t frame = getFrameRect();
  double dx = point.x - frame.pos.x;
  double dy = point.y - frame.pos.y;
  for (size_t i = 0; i < shapeCount_; i++) {
    shapeList_[i]->move(dx, dy);
  }
}

void stotskaya::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < shapeCount_; i++) {
    shapeList_[i]->move(dx, dy);
  }
}

void stotskaya::CompositeShape::scale(double ratio)
{
  if (ratio <= 0.0) {
    throw std::invalid_argument("Incorrect negative scaling ratio");
  }
  rectangle_t frame = getFrameRect();
  double centerX = frame.pos.x;
  double centerY = frame.pos.y;
  for (size_t i = 0; i < shapeCount_; i++) {
    shapeList_[i]->move((shapeList_[i]->getFrameRect().pos.x - centerX) * (ratio - 1),
        (shapeList_[i]->getFrameRect().pos.y - centerY) * (ratio - 1));
    shapeList_[i]->scale(ratio);
  }
}

void stotskaya::CompositeShape::rotate(double angle)
{
  double sinAngle = std::sin(angle * M_PI / 180);
  double cosAngle = std::cos(angle * M_PI / 180);
  rectangle_t frame = getFrameRect();
  double centerX = frame.pos.x;
  double centerY = frame.pos.y;
  for (size_t i = 0; i < shapeCount_; i++)
  {
    double currentX = shapeList_[i]->getFrameRect().pos.x;
    double currentY = shapeList_[i]->getFrameRect().pos.y;
    shapeList_[i]->move((currentX - centerX) * (cosAngle - 1) - (currentY - centerY) * sinAngle,
        (currentX - centerX) * sinAngle + (currentY - centerY) * (cosAngle - 1));
    shapeList_[i]->rotate(angle);
  }
}
