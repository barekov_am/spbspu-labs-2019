#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include "shape.hpp"

namespace stotskaya
{
  class CompositeShape : public Shape {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape&);
    CompositeShape(CompositeShape&&);
    ~CompositeShape() = default;
    CompositeShape& operator=(const CompositeShape&);
    CompositeShape& operator=(CompositeShape&&);
    Shape::ptr operator[](size_t) const;
    void addShape(Shape::ptr);
    void deleteShape(size_t);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    size_t getShapeCount() const;
    void move(const point_t& point) override;
    void move(double dx, double dy) override;
    void scale(double ratio) override;
    void rotate(double angle) override;
  private:
    Shape::array shapeList_;
    size_t shapeCount_;
  };
}

#endif // COMPOSITESHAPE_HPP
