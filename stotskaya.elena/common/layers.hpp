#ifndef LAYERS_HPP
#define LAYERS_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace stotskaya
{
  bool overlay(const Shape&, const Shape&);
  Matrix<Shape> splitLayers(const CompositeShape&);
}

#endif // LAYERS_HPP
