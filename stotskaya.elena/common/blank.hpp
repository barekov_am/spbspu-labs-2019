#ifndef BLANK_HPP
#define BLANK_HPP

#include <iostream>

template<typename CharT, typename Traits>
std::basic_istream<CharT, Traits>& blank(std::basic_istream<CharT, Traits>& in)
{
  typedef std::basic_istream<CharT, Traits> istream_type;
  typedef std::basic_streambuf<CharT, Traits> streambuf_type;
  typedef typename istream_type::int_type int_type;
  typedef std::ctype<CharT> ctype_type;

  const ctype_type& ct = std::use_facet<ctype_type>(in.getloc());
  const int_type eof = Traits::eof();
  streambuf_type* sb = in.rdbuf();
  int_type c = sb->sgetc();

  while (!Traits::eq_int_type(c, eof) && ct.is(std::ctype_base::blank, Traits::to_char_type(c))) {
    c = sb->snextc();
  }

  if (Traits::eq_int_type(c, eof)) {
    in.setstate(std::ios_base::eofbit);
  }
  return in;
}

#endif // BLANK_HPP
