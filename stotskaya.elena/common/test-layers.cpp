#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "layers.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(layersTest)
  BOOST_AUTO_TEST_CASE(testOverlay)
  {
    stotskaya::Rectangle rect1({0.0, 0.0}, 6.0, 2.0, 0.0);
    stotskaya::Circle circle1({4.0, 2.0}, 2.0);
    stotskaya::Rectangle rect2({-3.0, 3.0}, 4.0, 5.0, 0.0);
    stotskaya::Circle circle2({6.5, 4.5}, 1.5);
    BOOST_CHECK(stotskaya::overlay(rect1, circle1));
    BOOST_CHECK(stotskaya::overlay(rect2, rect1));
    BOOST_CHECK(!stotskaya::overlay(rect2, circle2));
    BOOST_CHECK(stotskaya::overlay(circle1, circle2));
  }
  BOOST_AUTO_TEST_CASE(testSplitLayers)
  {
    stotskaya::CompositeShape testComposite;
    stotskaya::Rectangle rect1({0.0, 0.0}, 6.0, 2.0, 0.0);
    stotskaya::Circle circle1({4.0, 2.0}, 2.0);
    stotskaya::Rectangle rect2({-3.0, 3.0}, 4.0, 5.0, 0.0);
    stotskaya::Circle circle2({6.5, 4.5}, 1.5);
    testComposite.addShape(std::make_shared<stotskaya::Rectangle>(rect1));
    testComposite.addShape(std::make_shared<stotskaya::Circle>(circle1));
    testComposite.addShape(std::make_shared<stotskaya::Rectangle>(rect2));
    testComposite.addShape(std::make_shared<stotskaya::Circle>(circle2));
    stotskaya::Matrix<stotskaya::Shape> testLayers = stotskaya::splitLayers(testComposite);
    BOOST_CHECK_EQUAL(testLayers.getRows(), 2);
    BOOST_CHECK_EQUAL(testLayers[0][0], testComposite[0]);
    BOOST_CHECK_EQUAL(testLayers[0][1], testComposite[3]);
    BOOST_CHECK_EQUAL(testLayers[1][0], testComposite[1]);
    BOOST_CHECK_EQUAL(testLayers[1][1], testComposite[2]);
  }
BOOST_AUTO_TEST_SUITE_END()
