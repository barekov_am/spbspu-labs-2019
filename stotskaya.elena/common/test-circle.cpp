#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(circleTest)
  const double ERROR_MARGIN = 0.001;
  BOOST_AUTO_TEST_CASE(circleMove)
  {
    stotskaya::Circle testCircle({2.3, 3.0}, 1.5);
    const stotskaya::rectangle_t initialFrame = testCircle.getFrameRect();
    const double initialArea = testCircle.getArea();
    testCircle.move({3.0, 5.5});
    BOOST_CHECK_CLOSE(initialFrame.width, testCircle.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, testCircle.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea, testCircle.getArea(), ERROR_MARGIN);
    testCircle.move(-3.0, 4.0);
    BOOST_CHECK_CLOSE(initialFrame.width, testCircle.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, testCircle.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea, testCircle.getArea(), ERROR_MARGIN);
  }
  BOOST_AUTO_TEST_CASE(circleScale)
  {
    stotskaya::Circle testCircle({5.0, 6.5}, 6.0);
    const stotskaya::rectangle_t initialFrame = testCircle.getFrameRect();
    const double initialArea = testCircle.getArea();
    const double testRatio = 4.5;
    testCircle.scale(testRatio);
    BOOST_CHECK_CLOSE(initialFrame.width * testRatio, testCircle.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height * testRatio, testCircle.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea * testRatio * testRatio, testCircle.getArea(), ERROR_MARGIN);
  }
  BOOST_AUTO_TEST_CASE(circleRotate)
  {
    stotskaya::Circle testCircle({5.0, 6.5}, 6.0);
    const stotskaya::rectangle_t initialFrame = testCircle.getFrameRect();
    const double initialArea = testCircle.getArea();
    testCircle.rotate(90.0);
    BOOST_CHECK_CLOSE(initialFrame.width, testCircle.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, testCircle.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea, testCircle.getArea(), ERROR_MARGIN);
  }
  BOOST_AUTO_TEST_CASE(circleIncorrectValues)
  {
    BOOST_CHECK_THROW(stotskaya::Circle({1.0, 1.0}, 0.0), std::invalid_argument);
    BOOST_CHECK_THROW(stotskaya::Circle({1.0, 1.0}, -1.0), std::invalid_argument);
    stotskaya::Circle testCircle({1.0, 3.0}, 5.0);
    BOOST_CHECK_THROW(testCircle.scale(-1.0), std::invalid_argument);
  }
  BOOST_AUTO_TEST_SUITE_END()
