#ifndef POINT_HPP
#define POINT_HPP

#include <iosfwd>

struct Point
{
  int x;
  int y;
};

std::istream& operator>>(std::istream& in, Point& point);
std::ostream& operator<<(std::ostream& out, const Point& point);
int squareDistance(Point& point1, Point& point2);

#endif //POINT_HPP
