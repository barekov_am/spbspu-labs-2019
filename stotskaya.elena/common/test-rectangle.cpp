#define _USE_MATH_DEFINES

#include <cmath>
#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(rectangleTest)
  const double ERROR_MARGIN = 0.001;
  BOOST_AUTO_TEST_CASE(rectangleMove)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    const stotskaya::rectangle_t initialFrame = testRect.getFrameRect();
    const double initialArea = testRect.getArea();
    testRect.move({3.0, 5.5});
    BOOST_CHECK_CLOSE(initialFrame.width, testRect.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, testRect.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea, testRect.getArea(), ERROR_MARGIN);
    testRect.move(-3.0, 4.0);
    BOOST_CHECK_CLOSE(initialFrame.width, testRect.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, testRect.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea, testRect.getArea(), ERROR_MARGIN);
  }
  BOOST_AUTO_TEST_CASE(rectangleScale)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    const stotskaya::rectangle_t initialFrame = testRect.getFrameRect();
    const double initialArea = testRect.getArea();
    const double testRatio = 3.2;
    testRect.scale(testRatio);
    BOOST_CHECK_CLOSE(initialFrame.width * testRatio, testRect.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height * testRatio, testRect.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea * testRatio * testRatio, testRect.getArea(), ERROR_MARGIN);
  }
  BOOST_AUTO_TEST_CASE(rectangleRotate)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    const stotskaya::rectangle_t initialFrame = testRect.getFrameRect();
    const double initialArea = testRect.getArea();
    testRect.rotate(90.0);
    BOOST_CHECK_CLOSE(initialFrame.width, testRect.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, testRect.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea, testRect.getArea(), ERROR_MARGIN);
    testRect.rotate(-90.0);
    BOOST_CHECK_CLOSE(initialFrame.width, testRect.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, testRect.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea, testRect.getArea(), ERROR_MARGIN);
    testRect.rotate(30.0);
    const stotskaya::rectangle_t newFrame = testRect.getFrameRect();
    BOOST_CHECK_CLOSE(newFrame.width, initialFrame.width * std::cos(M_PI / 6)
        + initialFrame.height * std::sin(M_PI / 6), ERROR_MARGIN);
    BOOST_CHECK_CLOSE(newFrame.height, initialFrame.width * std::sin(M_PI / 6)
        + initialFrame.height * std::cos(M_PI / 6), ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea, testRect.getArea(), ERROR_MARGIN);
  }
  BOOST_AUTO_TEST_CASE(rectangleIncorrectValues)
  {
    BOOST_CHECK_THROW(stotskaya::Rectangle({1.0, 1.0}, 0.0, 1.0, 0.0), std::invalid_argument);
    BOOST_CHECK_THROW(stotskaya::Rectangle({1.0, 1.0}, 1.0, -1.0, 0.0), std::invalid_argument);
    stotskaya::Rectangle testRect({1.0, 3.0}, 5.0, 7.0, 0.0);
    BOOST_CHECK_THROW(testRect.scale(-1.0), std::invalid_argument);
  }
  BOOST_AUTO_TEST_SUITE_END()
