#include "layers.hpp"

bool stotskaya::overlay(const stotskaya::Shape& lhs, const stotskaya::Shape& rhs)
{
  const stotskaya::rectangle_t lhsFrame = lhs.getFrameRect();
  const stotskaya::rectangle_t rhsFrame = rhs.getFrameRect();
  if (abs(lhsFrame.pos.x - rhsFrame.pos.x) > (lhsFrame.width + rhsFrame.width) / 2) {
    return false;
  }
  if (abs(lhsFrame.pos.y - rhsFrame.pos.y) > (lhsFrame.height + rhsFrame.height) / 2) {
    return false;
  }
  return true;
}

stotskaya::Matrix<stotskaya::Shape> stotskaya::splitLayers(const CompositeShape& composite)
{
  Matrix<Shape> layers;
  for (size_t i = 0; i < composite.getShapeCount(); i++)
  {
    size_t layerCount = 0;
    bool setLayer = false;
    for (size_t j = 0; j < layers.getRows(); j++) {
      for (size_t k = 0; k < layers[j].getCount(); k++) {
        if (overlay(*composite[i], *layers[j][k])) {
          layerCount = j + 1;
          setLayer = false;
          break;
        } else {
          layerCount = j;
          setLayer = true;
        }
      }
      if (layerCount || setLayer) {
        break;
      }
    }
    layers.add(composite[i], layerCount);
  }
  return layers;
}
