#define _USE_MATH_DEFINES

#include "rectangle.hpp"
#include <cmath>
#include <stdexcept>

stotskaya::Rectangle::Rectangle(const point_t& center, double width, double height, double angle):
  center_(center),
  width_(width),
  height_(height),
  angle_(angle)
{
  if (width_ <= 0.0) {
    throw std::invalid_argument("Incorrect negative rectangle width");
  }
  if (height_ <= 0.0) {
    throw std::invalid_argument("Incorrect negative rectangle height");
  }
}

double stotskaya::Rectangle::getArea() const
{
  return (width_ * height_);
}

stotskaya::rectangle_t stotskaya::Rectangle::getFrameRect() const
{
  double sinAngle = std::sin(angle_ * M_PI / 180);
  double cosAngle = std::cos(angle_ * M_PI / 180);
  return {std::abs(width_ * cosAngle + height_ * sinAngle), std::abs(width_ * sinAngle + height_ * cosAngle), center_};
}

void stotskaya::Rectangle::move(const point_t &point)
{
  center_ = point;
}

void stotskaya::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void stotskaya::Rectangle::scale(double ratio)
{
  if (ratio <= 0.0) {
    throw std::invalid_argument("Incorrect negative scaling ratio");
  } else {
    width_ *= ratio;
    height_ *= ratio;
  }
}

void stotskaya::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
