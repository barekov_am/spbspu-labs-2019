#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "matrix.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)
  const double ERROR_MARGIN = 0.001;
  BOOST_AUTO_TEST_CASE(matrixCopyConstructor)
  {
    stotskaya::Matrix<int> testMatrix;
    int rows = 5;
    for (int i = 0; i < rows; i++) {
      testMatrix.add(std::make_shared<int>(i), i);
    }
    stotskaya::Matrix<int> newMatrix(testMatrix);
    BOOST_CHECK_EQUAL(testMatrix.getRows(), newMatrix.getRows());
    for (int i = 0; i < rows; i++) {
      BOOST_CHECK_EQUAL(*newMatrix[i][0], i);
    }
  }
  BOOST_AUTO_TEST_CASE(matrixMoveConstructor)
  {
    stotskaya::Matrix<int> testMatrix;
    int rows = 5;
    for (int i = 0; i < rows; i++) {
      testMatrix.add(std::make_shared<int>(i), i);
    }
    stotskaya::Matrix<int> newMatrix(std::move(testMatrix));
    BOOST_CHECK_EQUAL(newMatrix.getRows(), rows);
    for (int i = 0; i < rows; i++) {
      BOOST_CHECK_EQUAL(*newMatrix[i][0], i);
    }
  }
  BOOST_AUTO_TEST_CASE(matrixCopyAssignment)
  {
    stotskaya::Matrix<int> testMatrix;
    int rows = 5;
    for (int i = 0; i < rows; i++) {
      testMatrix.add(std::make_shared<int>(i), i);
    }
    stotskaya::Matrix<int> newMatrix = testMatrix;
    BOOST_CHECK_EQUAL(testMatrix.getRows(), newMatrix.getRows());
    for (int i = 0; i < rows; i++) {
      BOOST_CHECK_EQUAL(*newMatrix[i][0], i);
    }
  }
  BOOST_AUTO_TEST_CASE(matrixMoveAssignment)
  {
    stotskaya::Matrix<int> testMatrix;
    int rows = 5;
    for (int i = 0; i < rows; i++) {
      testMatrix.add(std::make_shared<int>(i), i);
    }
    stotskaya::Matrix<int> newMatrix = std::move(testMatrix);
    BOOST_CHECK_EQUAL(newMatrix.getRows(), rows);
    for (int i = 0; i < rows; i++) {
      BOOST_CHECK_EQUAL(*newMatrix[i][0], i);
    }
  }
  BOOST_AUTO_TEST_CASE(matrixEqualAndNotEqual)
  {
    stotskaya::Matrix<int> testMatrix1;
    int rows = 5;
    for (int i = 0; i < rows; i++) {
      testMatrix1.add(std::make_shared<int>(i), i);
    }
    stotskaya::Matrix<int> testMatrix2(testMatrix1);
    stotskaya::Matrix<int> testMatrix3;
    for (int i = 0; i < rows; i++) {
      testMatrix3.add(std::make_shared<int>(-1), i);
    }
    BOOST_CHECK_EQUAL(testMatrix2 == testMatrix1, true);
    BOOST_CHECK_EQUAL(testMatrix2 != testMatrix1, false);
    BOOST_CHECK_EQUAL(testMatrix3 == testMatrix1, false);
    BOOST_CHECK_EQUAL(testMatrix3 != testMatrix1, true);
  }
  BOOST_AUTO_TEST_CASE(matrixIncorrectIndex)
  {
    stotskaya::Matrix<int> testMatrix;
    int rows = 5;
    for (int i = 0; i < rows; i++) {
      testMatrix.add(std::make_shared<int>(i), i);
    }
    BOOST_CHECK_THROW(testMatrix[-1][0], std::out_of_range);
    BOOST_CHECK_THROW(testMatrix[7][0], std::out_of_range);
  }
BOOST_AUTO_TEST_SUITE_END()
