#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(compositeShapeTest)
  const double ERROR_MARGIN = 0.001;
  BOOST_AUTO_TEST_CASE(copyConstructor)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    stotskaya::Circle testCircle({2.3, 3.0}, 1.5);
    stotskaya::CompositeShape testComposite;
    testComposite.addShape(std::make_shared<stotskaya::Rectangle>(testRect));
    testComposite.addShape(std::make_shared<stotskaya::Circle>(testCircle));
    const stotskaya::rectangle_t initialFrame = testComposite.getFrameRect();
    stotskaya::CompositeShape newComposite(testComposite);
    const stotskaya::rectangle_t newFrame = newComposite.getFrameRect();
    BOOST_CHECK_CLOSE(initialFrame.width, newFrame.width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, newFrame.height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.pos.x, newFrame.pos.x, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.pos.y, newFrame.pos.y, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(testComposite.getArea(), newComposite.getArea(), ERROR_MARGIN);
    BOOST_CHECK_EQUAL(testComposite.getShapeCount(), newComposite.getShapeCount());
  }
  BOOST_AUTO_TEST_CASE(moveConstructor)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    stotskaya::Circle testCircle({2.3, 3.0}, 1.5);
    stotskaya::CompositeShape testComposite;
    testComposite.addShape(std::make_shared<stotskaya::Rectangle>(testRect));
    testComposite.addShape(std::make_shared<stotskaya::Circle>(testCircle));
    const stotskaya::rectangle_t initialFrame = testComposite.getFrameRect();
    const double initialArea = testComposite.getArea();
    const unsigned int initialCount = testComposite.getShapeCount();
    stotskaya::CompositeShape newComposite(std::move(testComposite));
    const stotskaya::rectangle_t newFrame = newComposite.getFrameRect();
    const double newArea = newComposite.getArea();
    const unsigned int newCount = newComposite.getShapeCount();
    BOOST_CHECK_CLOSE(initialFrame.width, newFrame.width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, newFrame.height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.pos.x, newFrame.pos.x, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.pos.y, newFrame.pos.y, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea, newArea, ERROR_MARGIN);
    BOOST_CHECK_EQUAL(initialCount, newCount);
    BOOST_CHECK_CLOSE(testComposite.getFrameRect().pos.x, 0.0, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(testComposite.getFrameRect().pos.y, 0.0, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(testComposite.getArea(), 0.0, ERROR_MARGIN);
    BOOST_CHECK_EQUAL(testComposite.getShapeCount(), 0);
  }
  BOOST_AUTO_TEST_CASE(copyAssignment)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    stotskaya::Circle testCircle({2.3, 3.0}, 1.5);
    stotskaya::CompositeShape testComposite;
    testComposite.addShape(std::make_shared<stotskaya::Rectangle>(testRect));
    testComposite.addShape(std::make_shared<stotskaya::Circle>(testCircle));
    const stotskaya::rectangle_t initialFrame = testComposite.getFrameRect();
    stotskaya::CompositeShape newComposite = testComposite;
    const stotskaya::rectangle_t newFrame = newComposite.getFrameRect();
    BOOST_CHECK_CLOSE(initialFrame.width, newFrame.width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, newFrame.height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.pos.x, newFrame.pos.x, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.pos.y, newFrame.pos.y, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(testComposite.getArea(), newComposite.getArea(), ERROR_MARGIN);
    BOOST_CHECK_EQUAL(testComposite.getShapeCount(), newComposite.getShapeCount());
  }
  BOOST_AUTO_TEST_CASE(moveAssignment)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    stotskaya::Circle testCircle({2.3, 3.0}, 1.5);
    stotskaya::CompositeShape testComposite;
    testComposite.addShape(std::make_shared<stotskaya::Rectangle>(testRect));
    testComposite.addShape(std::make_shared<stotskaya::Circle>(testCircle));
    const stotskaya::rectangle_t initialFrame = testComposite.getFrameRect();
    const double initialArea = testComposite.getArea();
    const unsigned int initialCount = testComposite.getShapeCount();
    stotskaya::CompositeShape newComposite = std::move(testComposite);
    const stotskaya::rectangle_t newFrame = newComposite.getFrameRect();
    const double newArea = newComposite.getArea();
    const unsigned int newCount = newComposite.getShapeCount();
    BOOST_CHECK_CLOSE(initialFrame.width, newFrame.width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, newFrame.height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.pos.x, newFrame.pos.x, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.pos.y, newFrame.pos.y, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea, newArea, ERROR_MARGIN);
    BOOST_CHECK_EQUAL(initialCount, newCount);
    BOOST_CHECK_CLOSE(testComposite.getFrameRect().pos.x, 0.0, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(testComposite.getFrameRect().pos.y, 0.0, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(testComposite.getArea(), 0.0, ERROR_MARGIN);
    BOOST_CHECK_EQUAL(testComposite.getShapeCount(), 0);
  }
  BOOST_AUTO_TEST_CASE(incorrectIndex)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    stotskaya::Circle testCircle({2.3, 3.0}, 1.5);
    stotskaya::CompositeShape testComposite;
    testComposite.addShape(std::make_shared<stotskaya::Rectangle>(testRect));
    testComposite.addShape(std::make_shared<stotskaya::Circle>(testCircle));
    BOOST_CHECK_THROW(testComposite[-1], std::out_of_range);
    BOOST_CHECK_THROW(testComposite[2], std::out_of_range);
  }
  BOOST_AUTO_TEST_CASE(emptyComposite)
  {
    stotskaya::CompositeShape testComposite;
    const stotskaya::rectangle_t emptyFrame = testComposite.getFrameRect();
    const double emptyArea = testComposite.getArea();
    BOOST_CHECK_CLOSE(emptyFrame.width, 0.0, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(emptyFrame.height, 0.0, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(emptyFrame.pos.x, 0.0, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(emptyFrame.pos.y, 0.0, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(emptyArea, 0.0, ERROR_MARGIN);
  }
  BOOST_AUTO_TEST_CASE(addToComposite)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    const stotskaya::rectangle_t rectFrame = testRect.getFrameRect();
    const double rectArea = testRect.getArea();
    stotskaya::CompositeShape testComposite1;
    testComposite1.addShape(std::make_shared<stotskaya::Rectangle>(testRect));
    BOOST_CHECK_CLOSE(rectFrame.width, testComposite1.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(rectFrame.height, testComposite1.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(rectFrame.pos.x, testComposite1.getFrameRect().pos.x, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(rectFrame.pos.y, testComposite1.getFrameRect().pos.y, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(rectArea, testComposite1.getArea(), ERROR_MARGIN);
    stotskaya::Circle testCircle({2.3, 3.0}, 1.5);
    const stotskaya::rectangle_t circleFrame = testCircle.getFrameRect();
    const double circleArea = testCircle.getArea();
    stotskaya::CompositeShape testComposite2;
    testComposite2.addShape(std::make_shared<stotskaya::Circle>(testCircle));
    BOOST_CHECK_CLOSE(circleFrame.width, testComposite2.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(circleFrame.height, testComposite2.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(circleFrame.pos.x, testComposite2.getFrameRect().pos.x, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(circleFrame.pos.y, testComposite2.getFrameRect().pos.y, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(circleArea, testComposite2.getArea(), ERROR_MARGIN);
  }
  BOOST_AUTO_TEST_CASE(addMultiple)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    stotskaya::CompositeShape testComposite;
    testComposite.addShape(std::make_shared<stotskaya::Rectangle>(testRect));
    stotskaya::Circle testCircle({2.3, 3.0}, 1.5);
    testComposite.addShape(std::make_shared<stotskaya::Circle>(testCircle));
    BOOST_CHECK_CLOSE(testComposite.getArea(), testRect.getArea() + testCircle.getArea(), ERROR_MARGIN);
  }
  BOOST_AUTO_TEST_CASE(deleteFromComposite)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    stotskaya::CompositeShape testComposite;
    testComposite.addShape(std::make_shared<stotskaya::Rectangle>(testRect));
    const stotskaya::rectangle_t initialFrame = testComposite.getFrameRect();
    const double initialArea = testComposite.getArea();
    stotskaya::Circle testCircle({2.3, 3.0}, 1.5);
    testComposite.addShape(std::make_shared<stotskaya::Circle>(testCircle));
    testComposite.deleteShape(1);
    BOOST_CHECK_CLOSE(initialFrame.width, testComposite.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, testComposite.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea, testComposite.getArea(), ERROR_MARGIN);
    BOOST_CHECK_THROW(testComposite.deleteShape(-1), std::out_of_range);
    BOOST_CHECK_THROW(testComposite.deleteShape(1), std::out_of_range);
  }
  BOOST_AUTO_TEST_CASE(moveAbsolute)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    stotskaya::Circle testCircle({2.3, 3.0}, 1.5);
    stotskaya::CompositeShape testComposite;
    testComposite.addShape(std::make_shared<stotskaya::Rectangle>(testRect));
    testComposite.addShape(std::make_shared<stotskaya::Circle>(testCircle));
    const stotskaya::rectangle_t initialFrame = testComposite.getFrameRect();
    const double initialArea = testComposite.getArea();
    testComposite.move({1.0, 1.0});
    BOOST_CHECK_CLOSE(initialFrame.width, testComposite.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, testComposite.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea, testComposite.getArea(), ERROR_MARGIN);
  }
  BOOST_AUTO_TEST_CASE(moveRelative)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    stotskaya::Circle testCircle({2.3, 3.0}, 1.5);
    stotskaya::CompositeShape testComposite;
    testComposite.addShape(std::make_shared<stotskaya::Rectangle>(testRect));
    testComposite.addShape(std::make_shared<stotskaya::Circle>(testCircle));
    const stotskaya::rectangle_t initialFrame = testComposite.getFrameRect();
    const double initialArea = testComposite.getArea();
    testComposite.move(3.0, -3.0);
    BOOST_CHECK_CLOSE(initialFrame.width, testComposite.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, testComposite.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea, testComposite.getArea(), ERROR_MARGIN);
  }
  BOOST_AUTO_TEST_CASE(scaleComposite)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    stotskaya::Circle testCircle({2.3, 3.0}, 1.5);
    stotskaya::CompositeShape testComposite;
    testComposite.addShape(std::make_shared<stotskaya::Rectangle>(testRect));
    testComposite.addShape(std::make_shared<stotskaya::Circle>(testCircle));
    const stotskaya::rectangle_t initialFrame = testComposite.getFrameRect();
    const double initialArea = testComposite.getArea();
    const double testRatio1 = 3.0;
    testComposite.scale(testRatio1);
    BOOST_CHECK_CLOSE(initialFrame.width * testRatio1, testComposite.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height * testRatio1, testComposite.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialArea * testRatio1 * testRatio1, testComposite.getArea(), ERROR_MARGIN);
    const stotskaya::rectangle_t newFrame = testComposite.getFrameRect();
    const double newArea = testComposite.getArea();
    const double testRatio2 = 0.2;
    testComposite.scale(testRatio2);
    BOOST_CHECK_CLOSE(newFrame.width * testRatio2, testComposite.getFrameRect().width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(newFrame.height * testRatio2, testComposite.getFrameRect().height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(newArea * testRatio2 * testRatio2, testComposite.getArea(), ERROR_MARGIN);
  }
  BOOST_AUTO_TEST_CASE(incorrectScaleRatio)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    stotskaya::Circle testCircle({2.3, 3.0}, 1.5);
    stotskaya::CompositeShape testComposite;
    testComposite.addShape(std::make_shared<stotskaya::Rectangle>(testRect));
    testComposite.addShape(std::make_shared<stotskaya::Circle>(testCircle));
    BOOST_CHECK_THROW(testComposite.scale(-1.0), std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(rotateComposite)
  {
    stotskaya::Rectangle testRect({5.0, 6.5}, 20.0, 10.0, 0.0);
    stotskaya::Circle testCircle({2.3, 3.0}, 1.5);
    stotskaya::CompositeShape testComposite;
    testComposite.addShape(std::make_shared<stotskaya::Rectangle>(testRect));
    testComposite.addShape(std::make_shared<stotskaya::Circle>(testCircle));
    const stotskaya::rectangle_t initialFrame = testComposite.getFrameRect();
    const double initialArea = testComposite.getArea();
    testComposite.rotate(90.0);
    stotskaya::rectangle_t newFrame = testComposite.getFrameRect();
    double newArea = testComposite.getArea();
    BOOST_CHECK_CLOSE(initialArea, newArea, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.width, newFrame.height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, newFrame.width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.pos.x, newFrame.pos.x, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.pos.y, newFrame.pos.y, ERROR_MARGIN);
    testComposite.rotate(-90.0);
    newFrame = testComposite.getFrameRect();
    newArea = testComposite.getArea();
    BOOST_CHECK_CLOSE(initialArea, newArea, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.width, newFrame.width, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.height, newFrame.height, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.pos.x, newFrame.pos.x, ERROR_MARGIN);
    BOOST_CHECK_CLOSE(initialFrame.pos.y, newFrame.pos.y, ERROR_MARGIN);
  }
  BOOST_AUTO_TEST_SUITE_END()
