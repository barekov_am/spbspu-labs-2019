#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <algorithm>
#include <memory>
#include <stdexcept>

namespace stotskaya
{
  template <typename T>
  class Matrix
  {
  public:
    using type_ptr = std::shared_ptr<T>;
    using type_array = std::unique_ptr<type_ptr[]>;
    class Row
    {
    public:
      Row(const Row&);
      Row(Row&&);
      Row(type_ptr*, size_t);
      ~Row() = default;
      Row& operator=(const Row&);
      Row& operator=(Row&&);
      type_ptr operator[](size_t) const;
      size_t getCount() const;
    private:
      type_array elements_;
      size_t count_;
    };
    Matrix();
    Matrix(const Matrix&);
    Matrix(Matrix&&);
    ~Matrix() = default;
    Matrix& operator=(const Matrix&);
    Matrix& operator=(Matrix&&);
    Row operator[](size_t) const;
    bool operator==(const Matrix&) const;
    bool operator!=(const Matrix&) const;
    void add(type_ptr, size_t);
    size_t getRows() const;
  private:
    type_array elements_;
    size_t rows_;
    size_t count_;
    std::unique_ptr<size_t[]> rowSize_;
    void swap(Matrix &rhs);
  };

  template<typename T>
  void Matrix<T>::swap(Matrix &rhs)
  {
    std::swap(rowSize_, rhs.rowSize_);
    std::swap(elements_, rhs.elements_);
    std::swap(count_, rhs.count_);
    std::swap(rows_, rhs.rows_);
  }

  template<typename T>
  Matrix<T>::Matrix():
    elements_(nullptr),
    rows_(0),
    count_(0)
  {
  }

  template <typename T>
  Matrix<T>::Matrix(const Matrix& rhs) :
    elements_(std::make_unique<type_ptr[]>(rhs.count_)),
    rows_(rhs.rows_),
    count_(rhs.count_),
    rowSize_(std::make_unique<size_t[]>(rhs.rows_))
  {
    for (size_t i = 0; i < rows_; i++)
    {
      rowSize_[i] = rhs.rowSize_[i];
    }

    for (size_t i = 0; i < count_; i++)
    {
      elements_[i] = rhs.elements_[i];
    }

  }

  template<typename T>
  Matrix<T>::Matrix(Matrix&& rhs) :
    elements_(std::move(rhs.elements_)),
    rows_(rhs.rows_),
    count_(rhs.count_),
    rowSize_(std::move(rhs.rowSize_))
  {
    rhs.rows_ = 0;
    rhs.count_ = 0;
  }

  template<typename T>
  Matrix<T>& Matrix<T>::operator=(const Matrix& rhs)
  {
    if (&rhs != this) {
      swap(Matrix<T>(rhs));
    }
    return *this;
  }

  template<typename T>
  Matrix<T>& Matrix<T>::operator=(Matrix&& rhs)
  {
    if (&rhs != this) {
      elements_ = std::move(rhs.elements_);
      rows_ = rhs.rows_;
      rhs.rows_ = 0;
      count_ = rhs.count_;
      rhs.count_ = 0;
      rowSize_ = std::move(rhs.rowSize_);
    }
    return *this;
  }

  template<typename T>
  typename Matrix<T>::Row Matrix<T>::operator[](size_t rowIndex) const
  {
    if (rowIndex >= rows_) {
      throw std::out_of_range("Row index out of range");
    }
    size_t elemIndex = 0;
    for (size_t i = 0; i < rowIndex; ++i)
    {
      elemIndex += rowSize_[i];
    }
    return Row(&elements_[elemIndex], rowSize_[rowIndex]);

  }

  template<typename T>
  bool Matrix<T>::operator==(const Matrix& rhs) const
  {
    if ((rows_ != rhs.rows_) || (count_ != rhs.count_)) {
      return false;
    }
    for (size_t i = 0; i < rows_; i++) {
      if (rowSize_[i] != rhs.rowSize_[i]) {
          return false;
      }
    }
    for (size_t i = 0; i < count_; i++) {
      if (elements_[i] != rhs.elements_[i]) {
          return false;
      }
    }
    return true;
  }

  template<typename T>
  bool Matrix<T>::operator!=(const Matrix& rhs) const
  {
    return !(*this == rhs);
  }

  template<typename T>
  void Matrix<T>::add(type_ptr newElement, size_t row)
  {
    if (newElement == nullptr) {
      throw std::invalid_argument("New element is empty");
    }
    if (row > rows_) {
      throw std::out_of_range("Row index out of range");
    }
    if (row == rows_) {
      std::unique_ptr<size_t[]> tmpRowSize = std::make_unique<size_t[]>(rows_ + 1);
      type_array tmpElements = std::make_unique<type_ptr[]>(count_ + 1);
      for (size_t i = 0; i < rows_; ++i)
      {
        tmpRowSize[i] = rowSize_[i];
      }
      tmpRowSize[rows_++] = 1;
      rowSize_ = std::move(tmpRowSize);

      for (size_t i = 0; i < count_; ++i)
      {
        tmpElements[i] = elements_[i];
      }
      tmpElements[count_++] = newElement;
      elements_ = std::move(tmpElements);
    } else {
      type_array tmpElements = std::make_unique<type_ptr[]>(count_ + 1);
      size_t ind = 0;
      size_t tmpInd = 0;
      for (size_t i = 0; i < rows_; i++) {
        for (size_t j = 0; j < rowSize_[i]; j++) {
          tmpElements[tmpInd++] = elements_[ind++];
        }
        if (i == row) {
          tmpElements[tmpInd++] = newElement;
        }
      }
      elements_ = std::move(tmpElements);
      rowSize_[row]++;
      count_++;
    }
  }

  template<typename T>
  size_t Matrix<T>::getRows() const
  {
    return rows_;
  }

  template<typename T>
  Matrix<T>::Row::Row(const Matrix<T>::Row& rhs) :
    elements_(std::make_unique<type_ptr[]>(rhs.elements_)),
    count_(rhs.count_)
  {
    for (size_t i = 0; i < count_; i++) {
      elements_[i] = rhs.elements_[i];
    }
  }

  template<typename T>
  Matrix<T>::Row::Row(Matrix<T>::Row&& rhs) :
    elements_(std::move(rhs.elements_)),
    count_(rhs.count_)
  {
    rhs.count_ = 0;
  }

  template<typename T>
  Matrix<T>::Row::Row(Matrix<T>::type_ptr* array, size_t count) :
    elements_(std::make_unique<type_ptr[]>(count)),
    count_(count)
  {
    for (size_t i = 0; i < count_; i++) {
      elements_[i] = array[i];
    }
  }

  template<typename T>
  typename Matrix<T>::Row& Matrix<T>::Row::operator=(const Matrix<T>::Row& rhs)
  {
    if (this != &rhs) {
      elements_ = std::make_unique<type_ptr[]>(rhs.count_);
      for (size_t i = 0; i < rhs.size_; i++) {
        elements_[i] = rhs.elements_[i];
      }
      count_ = rhs.count_;
    }
    return *this;
  }

  template<typename T>
  typename Matrix<T>::Row& Matrix<T>::Row::operator=(Matrix<T>::Row&& rhs)
  {
    if (this != &rhs) {
      count_ = rhs.count_;
      rhs.count_ = 0;
      elements_ = std::move(rhs.elements_);
    }
    return *this;
  }

  template<typename T>
  typename Matrix<T>::type_ptr Matrix<T>::Row::operator[](size_t ind) const
  {
    if (ind >= count_) {
      throw std::out_of_range("Column index out of range");
    }
    return elements_[ind];
  }

  template<typename T>
  size_t Matrix<T>::Row::getCount() const
  {
    return count_;
  }

}

#endif // MATRIX_HPP
