#include "point.hpp"
#include <iostream>
#include <limits>
#include "blank.hpp"

std::istream& operator>>(std::istream& in, Point& point)
{
  std::istream::sentry sentry(in);
  if (!sentry) {
    return in;
  }
  int tempX = 0;
  int tempY = 0;
  if ((in >> blank).peek() != '(') {
    in.clear(std::ios::failbit);
    return in;
  }
  in.ignore(std::numeric_limits<std::streamsize>::max(), '(');
  if ((in >> blank).peek() == '\n') {
    in.clear(std::ios::failbit);
    return in;
  }
  in >> tempX;
  if ((in >> blank).peek() != ';') {
    in.clear(std::ios::failbit);
    return in;
  }
  in.ignore(std::numeric_limits<std::streamsize>::max(), ';');
  if ((in >> blank).peek() == '\n') {
    in.clear(std::ios::failbit);
    return in;
  }
  in >> tempY;
  if ((in >> blank).peek() != ')') {
    in.clear(std::ios::failbit);
    return in;
  }
  in.ignore(std::numeric_limits<std::streamsize>::max(), ')');
  point = {tempX, tempY};
  return in;
}

std::ostream& operator<<(std::ostream& out, const Point& point)
{
  out << '(' << point.x << "; " << point.y << ')';
  return out;
}

int squareDistance(Point& point1, Point& point2)
{
  return (point1.x - point2.x) * (point1.x - point2.x) + (point1.y - point2.y) * (point1.y - point2.y);
}
