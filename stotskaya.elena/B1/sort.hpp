#ifndef SORT_HPP
#define SORT_HPP

#include <functional>
#include <cstring>
#include "access.hpp"

template <typename Element>
std::function<bool(Element, Element)> getComparator(const char* order)
{
  if (!strcmp(order, "ascending"))
  {
    return [](Element lhs, Element rhs) { return lhs < rhs; };
  }
  else if (!strcmp(order, "descending"))
  {
    return [](Element lhs, Element rhs) { return lhs > rhs; };
  }
  else
  {
    throw std::invalid_argument("Incorrect order parameter");
  }
}

template <template <class Container> class Access, typename Container, typename Element>
void sort(Container& container, std::function<bool(Element, Element)> compare)
{
  typedef class Access<Container> access;
  for (auto i = access::begin(container); i != access::end(container); ++i) {
    for (auto j = i; j != access::end(container); ++j) {
      if(compare(access::getElement(container, j), access::getElement(container, i))) {
        std::swap(access::getElement(container, j), access::getElement(container, i));
      }
    }
  }
}

#endif // SORT_HPP
