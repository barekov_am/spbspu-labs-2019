#include <vector>
#include "tasks.hpp"
#include "sort.hpp"
#include "print.hpp"

void fillRandom(double* array, int size)
{
  for (int i = 0; i < size; ++i) {
    array[i] = -1.0 + (rand() / (RAND_MAX / 2.0));
  }
}

void task4(const char* order, int vectorSize)
{
  if (vectorSize < 0) {
    throw std::invalid_argument("Inacceptable vector size");
  }
  if (!vectorSize) {
    return;
  }
  auto comparator = getComparator<double>(order);
  std::vector<double> arr(vectorSize);
  fillRandom(&arr[0], vectorSize);
  std::cout << arr << '\n';
  sort<BracketAccess>(arr, comparator);
  std::cout << arr << '\n';
}
