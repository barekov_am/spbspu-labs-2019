#include <fstream>
#include <memory>
#include <vector>
#include "tasks.hpp"

const std::size_t ARRAY_SIZE = 1024;
void task2(const char* fileName)
{
  std::ifstream file(fileName);
  if (!file) {
    throw std::invalid_argument("Incorrect file name");
  }
  std::size_t size = ARRAY_SIZE;
  typedef std::unique_ptr<char[], decltype(&free)> blockArray;
  blockArray array(static_cast<char*>(malloc(size)), &free);
  if (!array)
  {
    throw std::runtime_error("Could not allocate memory");
  }
  std::size_t ind = 0;
  while (file)
  {
    file.read(&array[ind], ARRAY_SIZE);
    ind += file.gcount();
    if (file.gcount() == ARRAY_SIZE)
    {
      size += ARRAY_SIZE;
       blockArray tmpArray(static_cast<char*>(realloc(array.get(), size)), &free);
      if (!tmpArray)
      {
        throw std::runtime_error("Could not allocate new array");
      }
      array.release();
      std::swap(array, tmpArray);
    }
    if (!file.eof() && file.fail())
    {
      throw std::runtime_error("Input error");
    }
  }
  std::vector<char> vector(&array[0], &array[ind]);
  for (const char& el : vector) {
      std::cout << el;
  }
}
