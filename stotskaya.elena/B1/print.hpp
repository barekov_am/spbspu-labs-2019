#ifndef PRINT_HPP
#define PRINT_HPP

#include <iostream>
#include <vector>
#include <forward_list>

template <typename Container, typename = typename Container::iterator>
std::ostream& operator<<(std::ostream& out, const Container& container)
{
  for (auto el : container) {
    out << el << ' ';
  }
  return out;
}

#endif // PRINT_HPP
