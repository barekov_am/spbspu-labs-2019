#ifndef ACCESS_HPP
#define ACCESS_HPP

#include <cstddef>

template <class Container>
struct BracketAccess
{
  using valueType = typename Container::value_type;

  static std::size_t begin(const Container&)
  {
    return 0;
  }

  static std::size_t end(const Container& container)
  {
    return container.size();
  }

  static valueType& getElement(Container& container, std::size_t ind)
  {
    return container[ind];
  }
};

template <typename Container>
struct AtAccess
{
  using valueType = typename Container::value_type;

  static std::size_t begin(const Container&)
  {
    return 0;
  }

  static std::size_t end(const Container& container)
  {
    return container.size();
  }

  static valueType& getElement(Container& container, std::size_t ind)
  {
    return container.at(ind);
  }
};

template <typename Container>
struct ListAccess
{
  using valueType = typename Container::value_type;

  static typename Container::iterator begin(Container& container)
  {
    return container.begin();
  }

  static typename Container::iterator end(Container& container)
  {
    return container.end();
  }

  static valueType& getElement(Container&, typename Container::iterator ind)
  {
    return *ind;
  }
};

#endif // ACCESS_HPP
