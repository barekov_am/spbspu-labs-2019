#include <iostream>
#include <vector>
#include "tasks.hpp"
#include "print.hpp"

void modify(std::vector<int>& vec)
{
  auto iter = vec.begin();
  if (*(vec.end() - 1) == 1)
  {
    while (iter != vec.end()) {
      if (*iter % 2 == 0) {
        iter = vec.erase(iter);
      } else {
        ++iter;
      }
    }
  } else if (*(vec.end() - 1) == 2) {
    while (iter != vec.end()) {
      if (*iter % 3 == 0) {
        iter = vec.insert(iter + 1, 3, 1);
        iter += 2;
      }
      ++iter;
    }
  }
}

void task3()
{
  std::vector<int> vec;
  int num = -1;
  while (std::cin && !(std::cin >> num).eof()) {
    if (std::cin.fail()) {
      throw std::runtime_error("Input error");
    }
    if (num == 0) {
      break;
    }
    vec.push_back(num);
  }
  if (vec.empty()) {
    return;
  }
  if (num != 0) {
    throw std::runtime_error("No zero at the end of vector");
  }
  modify(vec);
  std::cout << vec << '\n';
}
