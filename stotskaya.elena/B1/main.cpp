#include <stdexcept>
#include <ctime>
#include "tasks.hpp"

int main(int argc, char* argv[])
{
  try {
    if ((argc < 2) || (argc > 4)) {
      throw std::invalid_argument("Incorrect number of parameters");
    }
    char* end = nullptr;
    long task = std::strtol(argv[1], &end, 10);
    if(end == nullptr || *end != '\0') {
      throw std::invalid_argument("Parameter is not an integer");
    }
    switch (task)
    {
      case 1:
        if (argc != 3) {
          throw std::invalid_argument("Incorrect number of parameters");
        }
        task1(argv[2]);
        break;
      case 2:
        if (argc != 3) {
          throw std::invalid_argument("Incorrect number of parameters");
        }
        task2(argv[2]);
        break;
      case 3:
        task3();
        break;
      case 4:
      {
        if (argc != 4) {
          throw std::invalid_argument("Incorrect number of parameters");
        }
        int size = std::strtol(argv[3], &end, 10);
        if(end == nullptr || *end != '\0') {
          throw std::invalid_argument("Size is not an integer");
        }
        srand(time(0));
        task4(argv[2], size);
        break;
      }
      default:
        throw std::invalid_argument("Incorrect task number");
    }
  } catch (const std::exception& e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}
