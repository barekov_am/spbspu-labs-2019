#ifndef TASKS_HPP
#define TASKS_HPP

#include <iostream>

void task1(const char* order);
void task2(const char* fileName);
void task3();
void task4(const char* order, int vectorSize);

#endif // TASKS_HPP
