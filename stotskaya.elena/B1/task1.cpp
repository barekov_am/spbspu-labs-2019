#include <vector>
#include <forward_list>
#include "sort.hpp"
#include "tasks.hpp"
#include "print.hpp"

void task1(const char* order)
{
  auto comparator = getComparator<int>(order);
  std::vector<int> vectorBrackets;
  int num = 0;
  while (std::cin && !(std::cin >> num).eof()) {
    if (std::cin.fail()) {
      throw std::runtime_error("Input error");
    }
    vectorBrackets.push_back(num);
  }
  std::vector<int> vectorAt(vectorBrackets);
  std::forward_list<int> list(vectorBrackets.begin(), vectorBrackets.end());

  sort<AtAccess>(vectorAt, comparator);
  sort<BracketAccess>(vectorBrackets, comparator);
  sort<ListAccess>(list, comparator);

  std::cout << vectorBrackets << '\n';
  std::cout << vectorAt << '\n';
  std::cout << list << '\n';
}
