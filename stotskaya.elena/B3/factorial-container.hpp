#ifndef FACTORIALCONTAINER_HPP
#define FACTORIALCONTAINER_HPP

#include <iterator>

class FactorialContainer
{
public:
  class iterator : public std::iterator<std::bidirectional_iterator_tag, unsigned int, std::ptrdiff_t,
    const unsigned int*, unsigned int>
  {
  public:
    iterator();
    iterator(unsigned int index);
    bool operator ==(const iterator& rhs) const;
    bool operator !=(const iterator& rhs) const;
    unsigned int operator *() const;
    iterator& operator ++();
    iterator operator ++(int);
    iterator& operator --();
    iterator operator --(int);
  private:
    unsigned int factorial(unsigned int index) const;
    unsigned int index_;
    unsigned int factorial_;
  };
  typedef std::reverse_iterator<iterator> reverse_iterator;
  FactorialContainer() = default;
  iterator begin();
  iterator end();
  reverse_iterator rbegin();
  reverse_iterator rend();
};

#endif // FACTORIALCONTAINER_HPP
