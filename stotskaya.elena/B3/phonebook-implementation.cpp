#include "phonebook.hpp"
#include <stdexcept>

Phonebook::Phonebook()
{
  marks_ = {{"current", content_.begin()}};
}

bool Phonebook::isEmpty()
{
  return content_.empty();
}

entry Phonebook::getContent(std::string& markName)
{
  try {
    std::list<entry>::iterator iter = marks_.at(markName);
    if (iter == content_.end() && iter != content_.begin()) {
      throw invalid_bookmark();
    }
    if (isEmpty())
    {
      throw empty_book_error();
    }
    return *iter;
  } catch (std::out_of_range) {
    throw invalid_bookmark();
  }
}

void Phonebook::addFirst(std::string &number, std::string &name)
{
  content_.emplace_back(number, name);
  marks_.at("current") = content_.begin();
}

void Phonebook::addMark(std::string& markName, std::string& newName)
{
  if (isEmpty()) {
    throw empty_book_error();
  } else {
    try {
      std::list<entry>::iterator iter = marks_.at(markName);
      marks_.emplace(newName, iter);
    } catch(std::out_of_range) {
      throw invalid_bookmark();
    }
  }
}

void Phonebook::insertBefore(std::string& markName, std::string& number, std::string& name)
{
  try {
    std::list<entry>::iterator iter = marks_.at(markName);
    if (content_.empty()) {
      addFirst(number, name);
      return;
    }
    content_.emplace(iter, number, name);
  } catch(std::out_of_range) {
    throw invalid_bookmark();
  }
}

void Phonebook::insertAfter(std::string& markName, std::string& number, std::string& name)
{
  try {
    std::list<entry>::iterator iter = marks_.at(markName);
    if (content_.empty()) {
      addFirst(number, name);
      return;
    }
    content_.emplace(++iter, number, name);
  } catch(std::out_of_range) {
    throw invalid_bookmark();
  }
}

void Phonebook::addBack(std::string& number, std::string& name)
{
  if (content_.empty()) {
    addFirst(number, name);
    return;
  }
  content_.emplace_back(number, name);
}

void Phonebook::deleteEntry(std::string& markName)
{
  if (isEmpty()) {
    throw empty_book_error();
  }
  try {
    std::list<entry>::iterator iter = marks_.at(markName);
    std::list<entry>::iterator newIter = std::next(iter);
    if (newIter == content_.end()) {
      newIter = content_.begin();
    }
    for (std::map<std::string, std::list<entry>::iterator>::iterator i = marks_.begin(); i != marks_.end(); ++i) {
      if (i->second == iter) {
        i->second = newIter;
      }
    }
    content_.erase(iter);
  } catch(std::out_of_range) {
    throw invalid_bookmark();
  }
}

void Phonebook::move(std::string& markName, int steps)
{
  if (!steps) {
    return;
  }
  if (isEmpty()) {
    throw empty_book_error();
  }
  std::list<entry>::iterator iter;
  try {
    iter = marks_.at(markName);
  } catch(std::out_of_range) {
    throw invalid_bookmark();
  }
  if (steps > 0) {
    for (int i = 0; i < steps; ++i) {
      if (iter == content_.end()) {
        throw invalid_step();
      }
      ++iter;
    }
  } else {
    for (int i = 0; i > steps; --i) {
      if (iter == content_.begin()) {
        throw invalid_step();
      }
      --iter;
    }
  }
  marks_.at(markName) = iter;
}

void Phonebook::move(std::string& markName, std::string& order)
{
  if (isEmpty()) {
    throw empty_book_error();
  } else {
    std::map<std::string, std::list<entry>::iterator>::iterator mark = marks_.find(markName);
    if (mark == marks_.end()) {
      throw invalid_bookmark();
    }
    if (order == "first") {
      mark->second = content_.begin();
    } else {
      mark->second = --(content_.end());
    }
  }
}
