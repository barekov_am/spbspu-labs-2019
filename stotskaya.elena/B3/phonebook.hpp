#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <string>
#include <list>
#include <map>
#include "exceptions.hpp"

typedef std::pair<std::string, std::string> entry;

class Phonebook {
public:
  Phonebook();
  bool isEmpty();
  entry getContent(std::string& markName);
  void addFirst(std::string& number, std::string& name);
  void addMark(std::string& markName, std::string& newName);
  void insertBefore(std::string& markName, std::string& number, std::string& name);
  void insertAfter(std::string& markName, std::string& number, std::string& name);
  void addBack(std::string& number, std::string& name);
  void deleteEntry(std::string& markName);
  void move(std::string& markName, int steps);
  void move(std::string& markName, std::string& order);
private:
  std::list<entry> content_;
  std::map<std::string, std::list<entry>::iterator> marks_;
};

#endif // PHONEBOOK_HPP
