#include "factorial-container.hpp"

const unsigned int MIN_INDEX = 1;
const unsigned int MAX_INDEX = 11;

FactorialContainer::iterator::iterator() :
  index_(1),
  factorial_(1)
{
}

FactorialContainer::iterator::iterator(unsigned int index) :
  index_(index),
  factorial_(factorial(index))
{
}

bool FactorialContainer::iterator::operator ==(const iterator &rhs) const
{
  return index_ == rhs.index_;
}

bool FactorialContainer::iterator::operator !=(const iterator &rhs) const
{
  return !(*this == rhs);
}

unsigned int FactorialContainer::iterator::operator *() const
{
  return factorial_;
}

FactorialContainer::iterator& FactorialContainer::iterator::operator ++()
{
  if (index_ == MAX_INDEX) {
    throw std::out_of_range("Exceeded maximum value");
  }
  ++index_;
  factorial_ *= index_;
  return *this;
}

FactorialContainer::iterator FactorialContainer::iterator::operator ++(int)
{
  iterator old(*this);
  ++(*this);
  return old;
}

FactorialContainer::iterator& FactorialContainer::iterator::operator --()
{
  if (index_ == MIN_INDEX) {
    throw std::out_of_range("Exceeded minimum value");
  }
  factorial_ /= index_;
  --index_;
  return *this;
}

FactorialContainer::iterator FactorialContainer::iterator::operator --(int)
{
  iterator old(*this);
  --(*this);
  return old;
}

unsigned int FactorialContainer::iterator::factorial(unsigned int index) const
{
    unsigned int value = 1;
    for (unsigned int i = 2; i <= index; ++i) {
      value *= i;
    }
    return value;
}

FactorialContainer::iterator FactorialContainer::begin()
{
  return iterator();
}

FactorialContainer::iterator FactorialContainer::end()
{
  return iterator(MAX_INDEX);
}

FactorialContainer::reverse_iterator FactorialContainer::rbegin()
{
  return std::make_reverse_iterator(end());
}

FactorialContainer::reverse_iterator FactorialContainer::rend()
{
  return std::make_reverse_iterator(begin());
}
