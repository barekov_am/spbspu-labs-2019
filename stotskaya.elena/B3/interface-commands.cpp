#include "interface-commands.hpp"
#include <iostream>
#include <limits>
#include "blank.hpp"

bool checkQuotes(std::string& line)
{
  return((line[0] == '"') && (line[line.size() - 1] == '"') && (line != "\""));
}

void removeExtraSymbols(std::string& line)
{
  line = line.substr(1, line.size() - 2);
  for (std::size_t i = 0; i < line.size(); ++i) {
    if (line[i] == '\\' && line[i + 1] == '"') {
      line.erase(i, 1);
    }
  }
}

std::string readWithoutBlanks()
{
  std::string str;
  if ((std::cin >> blank).peek() == '\n') {
    throw invalid_command();
  }
  std::cin >> blank >> str;
  return str;
}

std::string readName()
{
  std::string name;
  if ((std::cin >> blank).peek() == '\n') {
    throw invalid_command();
  }
  std::getline(std::cin >> std::ws, name);
  if (!checkQuotes(name)) {
    throw invalid_command();
  }
  removeExtraSymbols(name);
  return name;
}

void addEntry(Phonebook& book)
{
  try {
    std::string number = readWithoutBlanks();
    std::string name = readName();
    book.addBack(number, name);
  } catch (invalid_command) {
    std::cout << "<INVALID COMMAND>\n";
  }
}

void storeNewMark(Phonebook& book)
{
  try {
    if (book.isEmpty()) {
      throw invalid_command();
    }
    std::string markName = readWithoutBlanks();
    std::string newName = readWithoutBlanks();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    book.addMark(markName, newName);
  } catch (invalid_bookmark) {
    std::cout << "<INVALID BOOKMARK>\n";
  } catch (empty_book_error) {
    std::cout << "<EMPTY>\n";
  } catch (invalid_command) {
    std::cout << "<INVALID COMMAND>\n";
  }
}

void insert(Phonebook& book)
{
  try {
    std::string order = readWithoutBlanks();
    if (order != "before" && order != "after") {
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      throw invalid_command();
    }
    std::string markName = readWithoutBlanks();
    std::string number = readWithoutBlanks();
    std::string name = readName();
    if (order == "before") {
      book.insertBefore(markName, number, name);
    } else {
      book.insertAfter(markName, number, name);
    }
  } catch (invalid_bookmark) {
    std::cout << "<INVALID BOOKMARK>\n";
  } catch (invalid_command) {
    std::cout << "<INVALID COMMAND>\n";
  }
}

void deleteEntry(Phonebook& book)
{
  try {
    std::string markName = readWithoutBlanks();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    book.deleteEntry(markName);
  } catch (invalid_bookmark) {
    std::cout << "<INVALID BOOKMARK>\n";
  } catch (empty_book_error) {
    std::cout << "<EMPTY>\n";
  } catch (invalid_command) {
    std::cout << "<INVALID COMMAND>\n";
  }
}

void show(Phonebook& book)
{
  try {
    std::string markName = readWithoutBlanks();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    entry outEntry = book.getContent(markName);
    std::cout << outEntry.first << ' ' << outEntry.second << '\n';
  } catch (invalid_bookmark) {
    std::cout << "<INVALID BOOKMARK>\n";
  } catch (empty_book_error) {
    std::cout << "<EMPTY>\n";
  } catch (invalid_command) {
    std::cout << "<INVALID COMMAND>\n";
  }
}

void moveMark(Phonebook& book)
{
  try {
    std::string markName = readWithoutBlanks();
    std::string stepsStr = readWithoutBlanks();
    if (stepsStr == "first" || stepsStr == "last") {
      book.move(markName, stepsStr);
    } else {
      int steps = 0;
      steps = std::stoi(stepsStr);
      book.move(markName, steps);
    }
  } catch (invalid_bookmark) {
    std::cout << "<INVALID BOOKMARK>\n";
  } catch (empty_book_error) {
    std::cout << "<EMPTY>\n";
  } catch (invalid_step) {
    std::cout << "<INVALID STEP>\n";
  } catch (std::invalid_argument) {
    std::cout << "<INVALID STEP>\n";
  }
}
