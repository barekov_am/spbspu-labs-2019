#ifndef INTERFACECOMMANDS_HPP
#define INTERFACECOMMANDS_HPP

#include "phonebook.hpp"

bool checkQuotes(std::string& line);
void removeExtraSymbols(std::string& line);
std::string readWithoutBlanks();
std::string readName();
void addEntry(Phonebook& book);
void storeNewMark(Phonebook& book);
void insert(Phonebook& book);
void deleteEntry(Phonebook& book);
void show(Phonebook& book);
void moveMark(Phonebook& book);

#endif // INTERFACECOMMANDS_HPP
