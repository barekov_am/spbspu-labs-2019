#ifndef EXCEPTIONS_HPP
#define EXCEPTIONS_HPP

#include <stdexcept>

class invalid_command : public std::exception
{
public:
  const char* what() const noexcept override
  {
    return "Invalid command";
  }
};

class invalid_bookmark : public std::exception
{
public:
  const char* what() const noexcept override
  {
    return "Invalid bookmark";
  }
};

class invalid_step : public std::exception
{
public:
  const char* what() const noexcept override
  {
    return "Invalid step";
  }
};

class empty_book_error : public std::exception
{
public:
  const char* what() const noexcept override
  {
    return "Phonebook is empty";
  }
};

#endif // EXCEPTIONS_HPP
