#include <iostream>
#include "factorial-container.hpp"

void task2()
{
  FactorialContainer factorial;
  std::copy(factorial.begin(), factorial.end(), std::ostream_iterator<unsigned int>(std::cout, " "));
  std::cout << "\n";
  std::copy(factorial.rbegin(), factorial.rend(), std::ostream_iterator<unsigned int>(std::cout, " "));
  std::cout << "\n";
}
