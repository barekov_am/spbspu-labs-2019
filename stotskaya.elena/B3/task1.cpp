#include <iostream>
#include <map>
#include <functional>
#include <limits>
#include "interface-commands.hpp"
#include "blank.hpp"

void task1()
{
  Phonebook book;
  std::map<std::string, std::function<void(Phonebook&)>> commandMap = {
    {"add", &addEntry},
    {"store", &storeNewMark},
    {"insert", &insert},
    {"delete", &deleteEntry},
    {"show", &show},
    {"move", &moveMark}
  };
  std::string command = "";
  while (!(std::cin >> blank >> command).eof()) {
    if (std::cin.fail()) {
      throw std::runtime_error("Input error");
    }
    auto commandEl = commandMap.find(command);
    if (commandEl == commandMap.end()) {
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      std::cout << "<INVALID COMMAND>\n";
    } else {
      (commandEl->second)(book);
    }
  }
}
