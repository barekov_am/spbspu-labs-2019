#ifndef STRUCTFUNCTIONS_HPP
#define STRUCTFUNCTIONS_HPP

#include <iosfwd>
#include <vector>
#include "data-struct.hpp"

int readKey(std::istream& in);
std::istream& operator>>(std::istream& in, DataStruct& data);
std::ostream& operator<<(std::ostream& out, const DataStruct& data);
void print(std::vector<DataStruct>& dataVector);

#endif // STRUCTFUNCTIONS_HPP
