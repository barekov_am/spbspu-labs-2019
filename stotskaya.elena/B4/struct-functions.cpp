#include "struct-functions.hpp"
#include <iostream>
#include <iterator>
#include "blank.hpp"

int readKey(std::istream& in)
{
  int key = 0;
  if ((in >> blank).peek() == '\n') {
    throw std::invalid_argument("Missing key");
  }
  in >> key;
  if (in.get() != ',') {
    throw std::invalid_argument("Incorrect key");
  }
  if (key < -5 || key > 5) {
    throw std::invalid_argument("Incorrect key");
  }
  return key;
}

std::istream& operator>>(std::istream& in, DataStruct& data)
{
  std::istream::sentry sentry(in);
  if (!sentry) {
    return in;
  }
  int key1 = readKey(in);
  int key2 = readKey(in);
  std::string str = "";
  std::getline(in >> blank, str);
  if (str == "") {
    throw std::invalid_argument("Missing string");
  }
  data = {key1, key2, str};
  return in;
}

std::ostream& operator<<(std::ostream& out, const DataStruct& data)
{
  out << data.key1 << "," << data.key2 << "," << data.str;
  return out;
}

void print(std::vector<DataStruct>& dataVector)
{
  std::copy(dataVector.begin(), dataVector.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));
}
