#include <iostream>
#include <stdexcept>
#include <vector>
#include <iterator>
#include <algorithm>
#include "struct-functions.hpp"

int main()
{
  try
  {
    std::vector<DataStruct> dataVector((std::istream_iterator<DataStruct>(std::cin)),
      std::istream_iterator<DataStruct>());
    std::sort(dataVector.begin(), dataVector.end(), [](DataStruct& struct1, DataStruct& struct2) {
      if (struct1.key1 != struct2.key1) {
        return (struct1.key1 < struct2.key1);
      } else if (struct1.key2 != struct2.key2) {
        return (struct1.key2 < struct2.key2);
      } else {
        return (struct1.str.size() < struct2.str.size());
      }
    });
    print(dataVector);
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}
