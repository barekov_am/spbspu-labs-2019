#include "statistics.hpp"
#include <limits>
#include <iostream>
#include <algorithm>

Statistics::Statistics() :
  max_(std::numeric_limits<long>::min()),
  min_(std::numeric_limits<long>::max()),
  totalCount_(0),
  posCount_(0),
  negCount_(0),
  oddSum_(0),
  evenSum_(0),
  first_(0),
  firstLastEqual_(false)
{
}

void Statistics::operator()(long el)
{
  if (!totalCount_) {
    first_ = el;
  }
  ++totalCount_;
  min_ = std::min(el, min_);
  max_ = std::max(el, max_);
  if (el > 0) {
    ++posCount_;
  } else if (el < 0) {
    ++negCount_;
  }
  if (el % 2) {
    oddSum_ += el;
  } else {
    evenSum_ += el;
  }
  firstLastEqual_ = (el == first_);
}

std::ostream& operator<<(std::ostream& out, const Statistics& stats)
{
  if (stats.totalCount_ == 0) {
    out << "No Data\n";
  } else {
    out << "Max: " << stats.max_ << "\n";
    out << "Min: " << stats.min_ << "\n";
    out << "Mean: " << (stats.oddSum_ + stats.evenSum_) / stats.totalCount_ << "\n";
    out << "Positive: " << stats.posCount_ << "\n";
    out << "Negative: " << stats.negCount_ << "\n";
    out << "Odd Sum: " << stats.oddSum_ << "\n";
    out << "Even Sum: " << stats.evenSum_ << "\n";
    out << "First/Last Equal: " << (stats.firstLastEqual_ ? "yes\n" : "no\n");
  }
  return out;
}
