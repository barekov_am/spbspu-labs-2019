#ifndef STATISTICS_HPP
#define STATISTICS_HPP

#include <iosfwd>

class Statistics
{
public:
  Statistics();
  void operator()(long el);
private:
  long max_;
  long min_;
  long totalCount_;
  long posCount_;
  long negCount_;
  long oddSum_;
  long evenSum_;
  long first_;
  bool firstLastEqual_;
  friend std::ostream& operator<<(std::ostream& out, const Statistics& stats);
};

#endif // STATISTICS_HPP
