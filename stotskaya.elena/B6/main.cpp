#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <stdexcept>
#include "statistics.hpp"

int main()
{
  try {
    Statistics stats;
    stats = std::for_each((std::istream_iterator<long>(std::cin)), std::istream_iterator<long>(), stats);
    if (std::cin.fail() && !std::cin.eof()) {
      throw std::runtime_error("Input error");
    }
    std::cout << stats;
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}
