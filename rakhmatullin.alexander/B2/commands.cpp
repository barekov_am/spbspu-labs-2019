#include <iostream>
#include <sstream>
#include <algorithm>
#include <locale>
#include "QueueWithPriority.hpp"

template <typename Char, typename CharTraits>
std::basic_istream<Char, CharTraits> & blank(std::basic_istream<Char, CharTraits> & is)
{
  while (std::isblank(static_cast<char>(is.peek()), std::locale("")))
  {
    is.ignore();
  }
  return is;
}

void addToQueue(QueueWithPriority<std::string> & queue, std::istream & args)
{
  static const struct
  {
    const char * name;
    QueueWithPriority<std::string>::ElementPriority priority;
  } priorities[] = {
      {"low", QueueWithPriority<std::string>::LOW},
      {"normal", QueueWithPriority<std::string>::NORMAL},
      {"high", QueueWithPriority<std::string>::HIGH}
  };

  std::string priority;
  args >> priority;

  auto findPriority = std::find_if(std::begin(priorities), std::end(priorities),
      [&](const auto & elem) { return priority == elem.name; });

  if (findPriority == std::end(priorities))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::string data;
  getline(args >> blank, data);

  if (data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.putElementToQueue(data, findPriority->priority);
}

void getFromQueue(QueueWithPriority<std::string> & queue, std::istream & args)
{
  if (!args.eof())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (queue.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  queue.getElementFromQueue([](const auto & elem) { std::cout << elem << '\n'; });
}

void accelerateQueue(QueueWithPriority<std::string> & queue, std::istream & args)
{
  if (!args.eof())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.accelerate();
}
