#include "tasks.hpp"
#include <sstream>
#include <iostream>
#include <functional>
#include <algorithm>
#include "QueueWithPriority.hpp"

void addToQueue(QueueWithPriority<std::string> & queue, std::istream & args);
void getFromQueue(QueueWithPriority<std::string> & queue, std::istream & args);
void accelerateQueue(QueueWithPriority<std::string> & queue, std::istream & args);

template <typename Char, typename CharTraits>
std::basic_istream<Char, CharTraits> & blank(std::basic_istream<Char, CharTraits> & is);

void task1()
{
  QueueWithPriority<std::string> queue;

  static const struct
  {
    const char * name;
    std::function<void(QueueWithPriority<std::string> & queue, std::istream & args)> function;
  } commands[] = {
      {"add", & addToQueue},
      {"get", & getFromQueue},
      {"accelerate", & accelerateQueue}
  };

  std::string line;

  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading failed");
    }

    std::stringstream input(line);
    input >> std::noskipws;

    std::string cmd;
    input >> blank >> cmd;

    auto findCmd = std::find_if(std::begin(commands), std::end(commands),
        [&](const auto & elem) { return cmd == elem.name; });

    if (findCmd != std::end(commands))
    {
      findCmd->function(queue, input >> blank);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
