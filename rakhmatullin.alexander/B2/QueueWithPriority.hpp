#ifndef QUEUEWITHPRIORITY_HPP
#define QUEUEWITHPRIORITY_HPP

#include <list>
#include <stdexcept>
#include <iostream>

template <typename QueueElementType>
class QueueWithPriority
{
public:
  enum ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  QueueWithPriority();
  ~QueueWithPriority() = default;

  void putElementToQueue(const QueueElementType & element, ElementPriority priority);

  template <typename Handler>
  void getElementFromQueue(Handler handle);

  void accelerate();

  bool empty() const noexcept;

private:
  std::list<QueueElementType> queue_;
  typename std::list<QueueElementType>::iterator highSeparator_;
  typename std::list<QueueElementType>::iterator normalSeparator_;
};

template <typename QueueElementType>
QueueWithPriority<QueueElementType>::QueueWithPriority() :
  queue_(),
  highSeparator_(queue_.begin()),
  normalSeparator_(queue_.begin())
{}

template <typename QueueElementType>
void QueueWithPriority<QueueElementType>::putElementToQueue(const QueueElementType & element, ElementPriority priority)
{
  switch (priority)
  {
  case ElementPriority::LOW:
    queue_.insert(queue_.end(), element);
    if (normalSeparator_ == queue_.end())
    {
      normalSeparator_ = std::prev(queue_.end());
      if (highSeparator_ == queue_.end())
      {
        highSeparator_ = std::prev(queue_.end());
      }
    }
    break;
  case ElementPriority::NORMAL:
    queue_.insert(normalSeparator_, element);
    if (highSeparator_ == normalSeparator_)
    {
      highSeparator_ = std::prev(normalSeparator_);
    }
    break;
  case ElementPriority::HIGH:
    queue_.insert(highSeparator_, element);
    break;
  default:
    std::cout << "Invalid priority value.\n";
  }
}

template <typename QueueElementType>
template <typename Handler>
void QueueWithPriority<QueueElementType>::getElementFromQueue(Handler handle)
{
  if (empty())
  {
    throw std::out_of_range("Queue is empty.");
  }

  handle(queue_.front());

  if (highSeparator_ == queue_.begin())
  {
    highSeparator_ = std::next(queue_.begin());
  }

  if (normalSeparator_ == queue_.begin())
  {
    highSeparator_ = std::next(queue_.begin());
    normalSeparator_ = std::next(queue_.begin());
  }

  queue_.pop_front();
}

template <typename QueueElementType>
void QueueWithPriority<QueueElementType>::accelerate()
{
  queue_.splice(highSeparator_, queue_, normalSeparator_, queue_.end());
}

template <typename QueueElementType>
bool QueueWithPriority<QueueElementType>::empty() const noexcept
{
  return queue_.empty();
}

#endif //QUEUEWITHPRIORITY_HPP
