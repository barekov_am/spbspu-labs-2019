#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

void printInfo(const rakhmatullin::Shape * shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape is empty");
  }
  rakhmatullin::rectangle_t tempRect = shape->getFrameRect();
  std::cout << "Width: " << tempRect.width << '\n';
  std::cout << "Height: " << tempRect.height << '\n';
  std::cout << "Center: (" << tempRect.pos.x << ',' << tempRect.pos.y << ")\n";
  std::cout << "Area: " << shape->getArea() << '\n';
}

int main()
{
  rakhmatullin::Rectangle rect({2.6, 4.4}, 15.0, 20.0); //Rectangle with center is (2.6,4.4) , width 15 , height 20
  rakhmatullin::Shape * shape = &rect;

  std::cout << "-----Rectangle-----\n";
  printInfo(shape);

  std::cout << "\nShift (1.7, -1.3):\n";
  shape->move(1.7, -1.3);
  printInfo(shape);

  std::cout << "\nMove to (2.3,5.8):\n";
  shape->move({2.3, 5.8});
  printInfo(shape);

  std::cout << "\nScale in 2.0\n";
  shape->scale(2.0);
  printInfo(shape);

  std::cout << "\nRotate by 90 degrees\n";
  shape->rotate(90);
  printInfo(shape);

  rakhmatullin::Circle cirle({7.2, 6.8}, 10.0); //Circle whit center is (7.2,6.8) , radius 10
  shape = &cirle;

  std::cout << "\n-----Circle-----\n";
  printInfo(shape);

  std::cout << "\nShift (1.0, 2.0):\n";
  shape->move(1.0, 2.0);
  printInfo(shape);

  std::cout << "\nMove to (5.3, 4.1):\n";
  shape->move({5.3, 4.1});
  printInfo(shape);

  std::cout << "\nScale in 3.0\n";
  shape->scale(3.0);
  printInfo(shape);

  std::cout << "\nRotate by 90 degrees\n";
  shape->rotate(90);
  printInfo(shape);

  std::cout << "\n-----Composite-Shape-----\n";
  rakhmatullin::CompositeShape compositeShape; //Composite shape (CS) is empty

  rakhmatullin::Circle circle1({5.0, 5.0}, 10.0); //Circle for CS whit center is (5.0, 5.0) , radius 10.0
  std::cout << "\nCircle 1: \n";
  shape = &circle1;
  printInfo(shape);

  std::cout << "\nAdd to composite shape!\n";
  compositeShape.addShape(std::make_shared<rakhmatullin::Circle>(circle1));
  compositeShape.printInfo();

  rakhmatullin::Rectangle rectangle1({9.0, 5.0}, 5.0, 25.0);//Rectangle for CS with center is (3.2,8.9) , width 5 , height 25
  std::cout << "\nRectangle 1: \n";
  shape = &rectangle1;
  printInfo(shape);

  std::cout << "\nAdd to composite shape!\n";
  compositeShape.addShape(std::make_shared<rakhmatullin::Rectangle>(rectangle1));
  compositeShape.printInfo();

  rakhmatullin::Circle circle2({-10.0, -5.0}, 10.0); //Circle for CS whit center is (-10.0, -5.0) , radius 10.0
  std::cout << "\nCircle 2: \n";
  shape = &circle2;
  printInfo(shape);

  std::cout << "\nAdd to composite shape!\n";
  compositeShape.addShape(std::make_shared<rakhmatullin::Circle>(circle2));
  compositeShape.printInfo();

  std::cout << "\nMove composite shape to (10, 10)\n";
  compositeShape.move({10.0, 10.0});
  compositeShape.printInfo();

  std::cout << "\nShift composite shape on (-3, -4)\n";
  compositeShape.move(-3.0, -4.0);
  compositeShape.printInfo();

  std::cout << "\nScale composite shape in 2.0\n";
  compositeShape.scale(2.0);
  compositeShape.printInfo();

  std::cout << "\nDelete Rectangle 1 from the composite shape\n";
  compositeShape.deleteShape(2);
  compositeShape.printInfo();

  std::cout << "\n-------Matrix-------\n";
  rakhmatullin::CompositeShape composite;
  composite.addShape(std::make_shared<rakhmatullin::Circle>(rakhmatullin::point_t{10.0, 10.0}, 5.0));
  composite.addShape(std::make_shared<rakhmatullin::Circle>(rakhmatullin::point_t{5.0, 1.0}, 3.0));
  composite.addShape(std::make_shared<rakhmatullin::Rectangle>(rakhmatullin::point_t{5.0, 6.0}, 5.0, 6.0));
  composite.addShape(std::make_shared<rakhmatullin::Rectangle>(rakhmatullin::point_t{5.0, 7.0}, 5.0, 5.0));

  rakhmatullin::Matrix<rakhmatullin::Shape> matrix = rakhmatullin::part(composite);

  for (std::size_t i = 0; i < matrix.getRows(); i++)
  {
    std::cout << "\n*Layer: " << i + 1 << "*\n";
    for (std::size_t j = 0; j < matrix[i].size(); j++)
    {
      std::cout << "Shape " << j + 1 << ":\n";
      const rakhmatullin::rectangle_t frameRect = matrix[i][j]->getFrameRect();
      std::cout << "Area: " << matrix[i][j]->getArea() << '\n';
      std::cout << "Position: (" << frameRect.pos.x << ", " << frameRect.pos.y << ")\n";

    }
  }

  return 0;
}
