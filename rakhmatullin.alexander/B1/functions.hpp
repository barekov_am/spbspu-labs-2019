#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <iostream>
#include "access.hpp"

bool isDescending(const char * direction);

template <typename ContainerType, typename AccessType>
void sort(ContainerType & container, const bool descending)
{
   if (AccessType::begin(container) == AccessType::end(container))
   {
     return;
   }

  const auto begin = AccessType::begin(container);
  const auto end = AccessType::end(container);

  for (auto i = begin; i != end; i++)
  {
    auto tmp = i;
    for (auto j = ++tmp; j != end ; j++)
    {
      if (descending ? AccessType::getElement(container, i) < AccessType::getElement(container, j)
          : AccessType::getElement(container, i) > AccessType::getElement(container, j))
      {
        std::swap(AccessType::getElement(container, i), AccessType::getElement(container, j));
      }
    }
  }
}

template <typename ContainerType, typename AccessType>
void print(ContainerType & container)
{
  for (auto i = AccessType::begin(container); i != AccessType::end(container); i++)
  {
    std::cout << AccessType::getElement(container, i) << ' ';
  }
  std::cout << '\n';
}

#endif //FUNCTIONS_HPP
