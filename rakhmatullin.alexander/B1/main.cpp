#include <iostream>
#include <stdexcept>
#include <ctime>
#include "tasks.hpp"


int main(int argc, char * argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4))
    {
      std::cerr << "Incorrect input data.\n";
      return 1;
    }

    const int taskNumber = atoi(argv[1]);

    switch (taskNumber)
    {
    case 1:
      if (argc == 3)
      {
        task1(argv[2]);
      }
      else
      {
        std::cerr << "Invalid first input parameter.\n";
        return 1;
      }
      break;

    case 2:
      if (argc == 3)
      {
        task2(argv[2]);
      }
      else
      {
        std::cerr << "Invalid first input parameter.\n";
        return 1;
      }
        break;

    case 3:
      task3();
      break;

    case 4:
      if (argc == 4)
      {
        srand(time(0));
        task4(argv[2], argv[3]);
      }
      else
      {
        std::cerr << "Invalid first input parameter.\n";
        return 1;
      }
      break;

    default:
      std::cerr << "Invalid first input parameter.\n";
      return 1;
    }

  }
  catch (std::exception & err)
  {
    std::cerr << err.what() << '\n';
    return 1;
  }

  return 0;
}
