#include <vector>
#include <stdexcept>
#include "functions.hpp"
#include "tasks.hpp"

void task3()
{
  std::vector<int> vector;

  int tmp = 0;
  while ((std::cin) && (!(std::cin >> tmp).eof()))
  {
    if (std::cin.fail() || std::cin.bad())
    {
      throw std::ios_base::failure("Error while reading data.");
    }

    if (tmp == 0)
    {
      break;
    }

    vector.push_back(tmp);
  }

  if (vector.empty())
  {
    return;
  }

  if (tmp != 0)
  {
    throw std::invalid_argument("No zero.");
  }

  switch (vector.back())
  {
  case 1:
    for (auto i = vector.begin(); i != vector.end(); i++)
    {
      if (*i % 2 == 0)
      {
        vector.erase(i);
        i--;
      }
    }
    break;

  case 2:
    for (auto i = vector.begin(); i != vector.end(); i++)
    {
      if ((*i % 3) == 0)
      {
        i = vector.insert(i + 1, 3, 1);
        std::advance(i, 2);
      }
    }
    break;

  default:
    break;
  }

  print<std::vector<int>, access<std::vector<int>>::at>(vector);
}
