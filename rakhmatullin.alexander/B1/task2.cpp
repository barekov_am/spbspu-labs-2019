#include <fstream>
#include <stdexcept>
#include <vector>
#include <memory>
#include <iostream>
#include "tasks.hpp"

const std::size_t initialSize = 1024;

void task2(const char * filename)
{
  std::ifstream inFile(filename);

  if (!inFile)
  {
    throw std::ios_base::failure("Unsuccessful opening of the file.");
  }

  std::size_t size = initialSize;

  std::unique_ptr<char[], decltype(& free)> array(static_cast<char *>(malloc(size)), & free);

  if (!array)
  {
    throw std::runtime_error("Couldn't allocate memory.");
  }

  std::size_t i = 0;

  while (inFile)
  {
    inFile.read(& array[i], initialSize);
    i += inFile.gcount();

    if (inFile.gcount() == initialSize)
    {
      size += initialSize;
      std::unique_ptr<char[], decltype(& free)> tmpArray(static_cast<char *>(realloc(array.get(), size)), & free);

      if (!tmpArray)
      {
        throw std::runtime_error("Couldn't allocate memory for new chars");
      }
      array.release();
      std::swap(array, tmpArray);
    }
    if (!inFile.eof() && inFile.fail())
    {
      throw std::ios_base::failure("Chars reading failed");
    }
  }

  std::vector<char> vector(& array[0], & array[i]);

  for (const auto & symbol : vector)
  {
    std::cout << symbol;
  }
}
