#include <vector>
#include <forward_list>
#include "tasks.hpp"
#include "functions.hpp"
#include "access.hpp"

void task1(const char * sortDirection)
{
  typedef std::vector<int> Vector;
  typedef std::forward_list<int> List;

  const bool descending = isDescending(sortDirection);

  Vector vector1;
  int n = 0;

  while (std::cin && !(std::cin >> n).eof())
  {
    if (std::cin.fail() || std::cin.bad())
    {
      throw std::ios_base::failure("Error while reading the file.");
    }
    vector1.push_back(n);
  }

  if (vector1.empty())
  {
    return;
  }

  Vector vector2(vector1.begin(), vector1.end());
  List list(vector1.begin(), vector1.end());

  sort<Vector, access<Vector>::bracket>(vector1, descending);
  print<Vector, access<Vector>::bracket>(vector1);

  sort<Vector, access<Vector>::at>(vector2, descending);
  print<Vector, access<Vector>::at>(vector2);

  sort<List, access<List>::iter>(list, descending);
  print<List, access<List>::iter>(list);
}
