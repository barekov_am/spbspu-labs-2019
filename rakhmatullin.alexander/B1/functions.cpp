#include "functions.hpp"
#include <stdexcept>
#include <cstring>

bool isDescending(const char * direction)
{
  bool descending = false;

  if (std::strcmp(direction, "descending") == 0)
  {
    descending = true;
  }
  else
  {
    if (std::strcmp(direction, "ascending") != 0)
    {
      throw std::invalid_argument("Invalid second input parameter(direction).");
    }
  }

  return descending;
}
