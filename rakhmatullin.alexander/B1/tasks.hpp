#ifndef TASKS_HPP
#define TASKS_HPP

void task1(const char * sortDirection);
void task2(const char* filename);
void task3();
void task4(const char * sortDirection, const char* countElem);

#endif //TASKS_HPP
