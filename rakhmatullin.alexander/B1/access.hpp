#ifndef ACCESS_HPP
#define ACCESS_HPP

#include <iterator>

template <typename ContainerType>
struct access
{
  struct bracket
  {
    static typename ContainerType::reference getElement(ContainerType & container, std::size_t index)
    {
      return container[index];
    }

    static std::size_t begin(const ContainerType &)
    {
      return 0;
    }

    static std::size_t end(const ContainerType & container)
    {
      return container.size();
    }
  };

  struct at
  {
    static typename ContainerType::reference getElement(ContainerType & container, std::size_t index)
    {
      return container.at(index);
    }

    static std::size_t begin(const ContainerType &)
    {
      return 0;
    }

    static std::size_t end(const ContainerType & container)
    {
      return container.size();
    }
  };

  struct iter
  {
    static typename ContainerType::reference getElement(ContainerType &, typename ContainerType::iterator iter)
    {
      return * iter;
    }

    static typename ContainerType::iterator begin(ContainerType & container)
    {
      return container.begin();
    }

    static typename ContainerType::iterator end(ContainerType & container)
    {
      return  container.end();
    }
  };
};

#endif //ACCESS_HPP
