#include <stdexcept>
#include <vector>
#include "functions.hpp"
#include "tasks.hpp"


void fillRandom(double * array, int size)
{
  for (int i = 0; i < size; i++)
  {
    array[i] = (rand() % 21 - 10) * 1.0 / 10;
  }
}

void task4(const char * sortDirection, const char * countElem)
{
  const bool descending = isDescending(sortDirection);
  int size = atoi(countElem);

  if (size <= 0)
  {
    throw std::invalid_argument("Invalid third input parameter(size).");
  }

  std::vector<double> vector(size);

  fillRandom(vector.data(), size);
  print<std::vector<double >, access<std::vector<double >>::at>(vector);

  sort<std::vector<double>, access<std::vector<double>>::at>(vector, descending);
  print<std::vector<double >, access<std::vector<double >>::at>(vector);
}
