#include <iostream>
#include <exception>
#include <string>
#include <algorithm>

#include "shape.hpp"

const int VERTICES_OF_TRIANGLE = 3;
const int VERTICES_OF_RECTANGLE = 4;
const int VERTICES_OF_PENTAGON = 5;

Shape readPoints(std::string line, std::size_t vertices);
bool isSquare(const Shape & shape);
bool isRectangle(const Shape & shape);

void task2()
{
  std::vector<Shape> containerOfShapes;

  std::string line;
  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading failed");
    }

    while (line.find_first_of(" \t") == 0)
    {
      line.erase(0, 1);
    }

    if (line.empty())
    {
      continue;
    }

    std::size_t pos = line.find_first_of('(');
    if (pos == std::string::npos)
    {
      throw std::invalid_argument("Invalid shape");
    }

    std::size_t numOfVertices = std::stoi(line.substr(0, pos));
    line.erase(0, pos);
    if (numOfVertices < 1)
    {
      throw std::invalid_argument("Invalid number of vertices");
    }

    Shape shape = readPoints(line, numOfVertices);

    containerOfShapes.push_back(shape);
  }

  std::size_t countOfVertices = 0;
  std::size_t countOfTriangles = 0;
  std::size_t countOfSquares = 0;
  std::size_t countOfRectangle = 0;

  std::for_each(containerOfShapes.begin(), containerOfShapes.end(), [&](const Shape & shape) {
      countOfVertices += shape.size();
      if (shape.size() == VERTICES_OF_TRIANGLE)
      {
        ++countOfTriangles;
      }
      else if (shape.size() == VERTICES_OF_RECTANGLE)
      {
        if (isRectangle(shape))
        {
          ++countOfRectangle;
          if (isSquare(shape))
          {
            ++countOfSquares;
          }
        }
      }
  });

  containerOfShapes.erase(std::remove_if(containerOfShapes.begin(), containerOfShapes.end(),
    [](const Shape & shape) { return shape.size() == VERTICES_OF_PENTAGON; } ), containerOfShapes.end());

  Shape points(containerOfShapes.size());

  std::transform(containerOfShapes.begin(), containerOfShapes.end(), points.begin(),
    [](const Shape & shape) { return shape[0]; });

  std::sort(containerOfShapes.begin(), containerOfShapes.end(),[](const Shape & lhs, const Shape & rhs) {
      if (lhs.size() < rhs.size())
      {
        return true;
      }
      if ((lhs.size() == VERTICES_OF_RECTANGLE) && (rhs.size() == VERTICES_OF_RECTANGLE))
      {
        if (isSquare(lhs))
        {
          if (isSquare(rhs))
          {
            return lhs[0].x < rhs[0].x;
          }
          return true;
        }
      }
      return false;
  });

  std::cout << "Vertices: " << countOfVertices << '\n';
  std::cout << "Triangles: " << countOfTriangles << '\n';
  std::cout << "Squares: " << countOfSquares << '\n';
  std::cout << "Rectangles: " << countOfRectangle << '\n';

  std::cout << "Points: ";
  for (auto & i : points)
  {
    std::cout << '(' << i.x << ';' << i.y << ") ";
  }
  std::cout << '\n';

  std::cout << "Shapes: \n";
  for (const auto & shape : containerOfShapes)
  {
    std::cout << shape.size();
    for (const auto point : shape)
    {
      std::cout << " (" << point.x << ';' << point.y << ") ";
    }
    std::cout << '\n';
  }
}
