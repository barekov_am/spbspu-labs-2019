#include <string>
#include <iostream>
#include <exception>
#include <cmath>

#include "shape.hpp"

Shape readPoints(std::string line, std::size_t vertices)
{
  Shape shape;
  std::size_t posOfOpenBracket;
  std::size_t posOfSemicolon;
  std::size_t posOfCloseBracket;

  for (std::size_t i = 0; i < vertices; i++)
  {
    if (line.empty())
    {
      throw std::invalid_argument("Invalid number of vertices");
    }

    while (line.find_first_of(" \t") == 0)
    {
      line.erase(0, 1);
    }

    posOfOpenBracket = line.find_first_of('(');
    posOfSemicolon = line.find_first_of(';');
    posOfCloseBracket = line.find_first_of(')');

    if ((posOfOpenBracket == std::string::npos) || (posOfSemicolon == std::string::npos) || (posOfCloseBracket == std::string::npos))
    {
      throw std::invalid_argument("Invalid point declaration");
    }

    Point_t point
        {
            std::stoi(line.substr(posOfOpenBracket + 1, posOfSemicolon - posOfOpenBracket - 1)),
            std::stoi(line.substr(posOfSemicolon + 1, posOfCloseBracket - posOfSemicolon -1))
        };

    line.erase(0, posOfCloseBracket + 1);

    shape.push_back(point);
  }

  while (line.find_first_of(" \t") == 0)
  {
    line.erase(0, 1);
  }

  if (!line.empty())
  {
    throw std::invalid_argument("Too much points");
  }

  return shape;
}

int getSquareDistance(const Point_t & lhs, const Point_t & rhs)
{
  return (lhs.x - rhs.x)*(lhs.x - rhs.x) + (lhs.y - rhs.y)*(lhs.y - rhs.y);
}

bool isRectangle(const Shape & shape)
{
  int diagonal1 = getSquareDistance(shape[0], shape[2]);
  int diagonal2 = getSquareDistance(shape[1], shape[3]);

  return diagonal1 == diagonal2;
}

bool isSquare(const Shape & shape)
{
  if (isRectangle(shape))
  {
    int diff1 = getSquareDistance(shape[0], shape[1]);
    int diff2 = getSquareDistance(shape[1], shape[2]);
    int diff3 = getSquareDistance(shape[2], shape[3]);
    int diff4 = getSquareDistance(shape[3], shape[0]);

    if ((diff1 == diff2) && (diff2 == diff3) && (diff3 == diff4) && (diff4 == diff1))
    {
      return true;
    }
  }
  return false;
}
