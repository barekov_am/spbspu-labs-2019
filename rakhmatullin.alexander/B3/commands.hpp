#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <string>
#include "bookmarksManager.hpp"

void addBookmark(BookmarksManager & manager, std::string & args);
void storeBookmark(BookmarksManager & manager, std::string & args);
void insertBookmark(BookmarksManager & manager, std::string & args);
void deleteBookmark(BookmarksManager & manager, std::string & args);
void showBookmark(BookmarksManager & manager, std::string & args);
void moveBookmark(BookmarksManager & manager, std::string & args);

std::string getWord(std::string & line);

#endif //COMMANDS_HPP
