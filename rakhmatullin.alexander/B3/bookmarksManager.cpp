#include "bookmarksManager.hpp"

#include <algorithm>
#include <iostream>

BookmarksManager::BookmarksManager()
{
  bookmarks_["current"] = nodes_.begin();
}

void BookmarksManager::add(const Phonebook::node_t & node)
{
  nodes_.pushBack(node);
  if (std::next(nodes_.begin()) == nodes_.end())
  {
    bookmarks_["current"] = nodes_.begin();
  }
}

void BookmarksManager::store(const std::string & bookmark, const std::string & name)
{
  BookmarksManager::bookmarkType::iterator iter = getBookmarkIterator(bookmark);
  if (iter == bookmarks_.end())
  {
    return;
  }

  bookmarks_.emplace(name, iter->second);
}

void BookmarksManager::insert(BookmarksManager::InsertPosition position, const std::string & bookmark,const Phonebook::node_t & node)
{
  BookmarksManager::bookmarkType::iterator iter = getBookmarkIterator(bookmark);
  if (iter == bookmarks_.end())
  {
    return;
  }

  if (iter->second == nodes_.end())
  {
    add(node);
    return;
  }

  auto point = (position == InsertPosition::after) ? std::next(iter->second) : iter->second;

  nodes_.insert(point, node);
}

void BookmarksManager::remove(const std::string & bookmark)
{
  BookmarksManager::bookmarkType::iterator iter = getBookmarkIterator(bookmark);
  if (iter == bookmarks_.end())
  {
    return;
  }

  auto tmp = iter->second;

  std::for_each(bookmarks_.begin(), bookmarks_.end(), [&](auto & it)
  {
      if (it.second == tmp)
      {
        if (std::next(it.second) == nodes_.end())
        {
          it.second = nodes_.prev(tmp);
        }
        else
        {
          it.second = nodes_.next(tmp);
        }
      }
  });

  nodes_.remove(tmp);
}

void BookmarksManager::show(const std::string & bookmark)
{
  BookmarksManager::bookmarkType::iterator iter = getBookmarkIterator(bookmark);
  if (iter == bookmarks_.end())
  {
    return;
  }

  if (nodes_.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  nodes_.show(iter->second);
}

void BookmarksManager::move(const std::string & bookmark, int step)
{
  BookmarksManager::bookmarkType::iterator iter = getBookmarkIterator(bookmark);
  if (iter == bookmarks_.end())
  {
    return;
  }

  iter->second = nodes_.moveOnSteps(iter->second, step);

}

void BookmarksManager::move(const std::string & bookmark, BookmarksManager::MovePosition position)
{
  BookmarksManager::bookmarkType::iterator iter = getBookmarkIterator(bookmark);
  if (iter == bookmarks_.end())
  {
    return;
  }

  iter->second = (position == BookmarksManager::MovePosition::first) ?
      nodes_.begin() : std::prev(nodes_.end());
}

BookmarksManager::bookmarkType::iterator BookmarksManager::getBookmarkIterator(const std::string & bookmark)
{
  BookmarksManager::bookmarkType::iterator iter = bookmarks_.find(bookmark);

  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
  }

  return iter;
}
