#ifndef FACTORIALITERATOR_HPP
#define FACTORIALITERATOR_HPP

#include <iterator>

class FactorialIterator: public std::iterator<std::bidirectional_iterator_tag, unsigned long long>
{
public:
  FactorialIterator();
  FactorialIterator(std::size_t index);

  reference operator *();
  pointer operator ->();

  FactorialIterator & operator ++();
  FactorialIterator operator ++(int);
  FactorialIterator & operator --();
  FactorialIterator operator --(int);

  bool operator ==(const FactorialIterator & iter) const;
  bool operator !=(const FactorialIterator & iter) const;
private:
  std::size_t index_;
  unsigned long long value_;

  unsigned long long count(std::size_t index) const;
};

class FactorialContainer
{
public:
  FactorialContainer() = default;

  FactorialIterator begin();
  FactorialIterator end();
};

#endif //FACTORIALITERATOR_HPP
