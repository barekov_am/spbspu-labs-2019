#ifndef BOOKMARKSMANAGER_HPP
#define BOOKMARKSMANAGER_HPP

#include <map>
#include "phonebook.hpp"

class BookmarksManager
{
public:
  enum class InsertPosition
  {
    before,
    after
  };

  enum class MovePosition
  {
    first,
    last
  };

  BookmarksManager();

  void add(const Phonebook::node_t & node);
  void store(const std::string & bookmark, const std::string & name);
  void insert(InsertPosition position, const std::string & bookmark, const Phonebook::node_t & node);
  void remove(const std::string & bookmark);
  void show(const std::string & bookmark);
  void move(const std::string & bookmark, int step);
  void move(const std::string & bookmark, MovePosition position);

private:
  typedef std::map<std::string, Phonebook::iterator> bookmarkType;

  Phonebook nodes_;
  bookmarkType bookmarks_;

  bookmarkType::iterator getBookmarkIterator(const std::string & bookmark);
};

#endif //BOOKMARKSMANAGER_HPP
