#include "commands.hpp"

#include <sstream>
#include <iostream>
#include <algorithm>

std::string getWord(std::string & line)
{
  if (line.empty())
  {
    return "";
  }

  std::size_t amountBlanks = 0;
  while ((amountBlanks != line.length()) && (isblank(line[amountBlanks])))
  {
    ++amountBlanks;
  }
  line.erase(0, amountBlanks);

  std::size_t wordLength = 0;
  while ((wordLength != line.length()) && (!isblank(line[wordLength])))
  {
    ++wordLength;
  }

  std::string word= line.substr(0, wordLength);
  line.erase(0, wordLength);

  return word;
}

std::string getCorrectNumber(std::string & num)
{
  std::string number = getWord(num);

  if (std::find_if(number.begin(), number.end(), [](char c) { return !isdigit(c); }) != number.end())
  {
    return "";
  }

  return number;
}

std::string getCorrectName(std::string & cmd)
{
  std::size_t blanks = 0;
  while ((blanks != cmd.length()) && (isblank(cmd[blanks])))
  {
    ++blanks;
  }
  cmd.erase(0, blanks);

  if (cmd.front() != '\"')
  {
    return "";
  }

  cmd.erase(cmd.begin());

  std::size_t i = 0;
  while ((i < cmd.length()) && (cmd[i] != '\"'))
  {
    if (cmd[i] == '\\')
    {
      if ((cmd[i + 1] == '\"') && (i + 2 < cmd.length()))
      {
        cmd.erase(i, 1);
      }
      else
      {
        return "";
      }
    }
    ++i;
  }

  if (i == cmd.length())
  {
    return "";
  }

  cmd.erase(i);

  std::string name = cmd.substr(0, i);
  cmd.erase(0, i);

  return name;
}

std::string getCorrectMarkName(std::string & cmd)
{
  std::string markName = getWord(cmd);

  if (std::find_if(markName.begin(), markName.end(), [](char c) { return !std::isalnum(c) && (c != '-'); }) != markName.end())
  {
    return "";
  }

  return markName;
}

void addBookmark(BookmarksManager & manager, std::string & args)
{
  std::string number = getCorrectNumber(args);

  if (number.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::string name = getCorrectName(args);

  if (name.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  manager.add({number, name});
}

void storeBookmark(BookmarksManager & manager, std::string & args)
{
  std::string mark = getCorrectMarkName(args);

  if (mark.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::string newMark = getCorrectMarkName(args);

  if (newMark.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  manager.store(mark, newMark);
}

void insertBookmark(BookmarksManager & manager, std::string & args)
{
  static const struct
  {
    const char * name;
    BookmarksManager::InsertPosition position;
  } positions[] = {
      {"before", BookmarksManager::InsertPosition::before},
      {"after", BookmarksManager::InsertPosition::after}
  };

  std::string positionName = getWord(args);

  auto findPosition = std::find_if(std::begin(positions), std::end(positions),
      [&](auto & elem){ return positionName == elem.name; });

  if (findPosition == std::end(positions))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::string mark = getCorrectMarkName(args);

  if (mark.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::string number = getCorrectNumber(args);

  if (number.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::string name = getCorrectName(args);

  if (name.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  manager.insert(findPosition->position, mark, {number, name});
}

void deleteBookmark(BookmarksManager & manager, std::string & args)
{
  std::string mark = getCorrectMarkName(args);

  if (mark.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  manager.remove(mark);
}

void showBookmark(BookmarksManager & manager, std::string & args)
{
  std::string mark = getCorrectMarkName(args);

  if (mark.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  manager.show(mark);
}

void moveBookmark(BookmarksManager & manager, std::string & args)
{
  static const struct
  {
    const char * name;
    BookmarksManager::MovePosition position;
  } positions[] = {
      {"first", BookmarksManager::MovePosition::first},
      {"last", BookmarksManager::MovePosition::last}
  };

  std::string mark = getCorrectMarkName(args);

  if (mark.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::string step = getWord(args);

  auto findPosition = std::find_if(std::begin(positions), std::end(positions),
      [&](auto & elem){ return step == elem.name; });

  if (findPosition != std::end(positions))
  {
    manager.move(mark, findPosition->position);
  }
  else
  {
    int sign = 1;
    if(step.front() == '-')
    {
      sign = -1;
      step.erase(step.begin());
    }
    else if (step.front() == '+')
    {
      step.erase(step.begin());
    }

    step = getCorrectNumber(step);
    if (step.empty())
    {
      std::cout << "<INVALID STEP>\n";
      return;
    }
    manager.move(mark, std::stoi(step) * sign);
  }
}
