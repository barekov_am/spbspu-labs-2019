#include "phonebook.hpp"
#include <iostream>

Phonebook::iterator Phonebook::begin() noexcept
{
  return list_.begin();
}

Phonebook::iterator Phonebook::end() noexcept
{
  return list_.end();
}

bool Phonebook::empty() const noexcept
{
  return list_.empty();
}

void Phonebook::show(Phonebook::iterator iter) const
{
  std::cout << iter->name << ' ' << iter->number << '\n';
}

Phonebook::iterator Phonebook::moveOnSteps(Phonebook::iterator iter, int step)
{
  if (step >= 0)
  {
    auto iterEnd = list_.end();
    while ((std::next(iter) != iterEnd) && (step > 0))
    {
      iter = std::next(iter);
      --step;
    }
  }
  else
  {
    auto iterBegin = list_.begin();
    while ((iter != iterBegin) && (step < 0))
    {
      iter = std::prev(iter);
      ++step;
    }
  }
  return iter;
}

Phonebook::iterator Phonebook::next(Phonebook::iterator iter)
{
  return moveOnSteps(iter, 1);
}

Phonebook::iterator Phonebook::prev(Phonebook::iterator iter)
{
  return moveOnSteps(iter, -1);
}

Phonebook::iterator Phonebook::insert(Phonebook::iterator iter, const Phonebook::node_t & node)
{
  return list_.insert(iter, node);
}

Phonebook::iterator Phonebook::replaceCurrentNode(Phonebook::iterator iter, const Phonebook::node_t & node)
{
  return list_.insert(list_.erase(iter), node);
}

void Phonebook::pushBack(const Phonebook::node_t & node)
{
  return list_.push_back(node);
}

Phonebook::iterator Phonebook::remove(Phonebook::iterator iter)
{
  return list_.erase(iter);
}
