#include "tasks.hpp"
#include <iostream>
#include <sstream>
#include <functional>
#include <algorithm>

#include "commands.hpp"
#include "bookmarksManager.hpp"

void task1()
{
  BookmarksManager manager;

  static const struct
  {
    const char * name;
    std::function<void(BookmarksManager &, std::string &)> function;
  } commands[] = {
      {"add", & addBookmark},
      {"store", & storeBookmark},
      {"insert", & insertBookmark},
      {"delete", & deleteBookmark},
      {"show", & showBookmark},
      {"move", & moveBookmark}
  };

  std::string line;

  while(getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading failed");
    }

    std::string cmd = getWord(line);


    auto findCmd = std::find_if(std::begin(commands), std::end(commands),
        [&](const auto & elem) { return cmd == elem.name; });

    if (findCmd != std::end(commands))
    {
      findCmd->function(manager, line);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
