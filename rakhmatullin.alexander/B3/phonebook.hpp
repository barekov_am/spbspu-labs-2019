#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <list>
#include <string>

class Phonebook
{
public:
  struct node_t
  {
    std::string name;
    std::string number;
  };

  typedef std::list<node_t> container;
  typedef container::iterator iterator;

  iterator begin() noexcept;
  iterator end() noexcept;
  bool empty() const noexcept;

  void show(iterator iter) const;
  iterator moveOnSteps(iterator iter, int step);
  iterator next(iterator iter);
  iterator prev(iterator iter);

  iterator insert(iterator iter, const node_t & node);
  iterator replaceCurrentNode(iterator iter, const node_t & node);
  void pushBack(const node_t & node);
  iterator remove(iterator iter);
private:
  container list_;
};

#endif //PHONEBOOK_HPP
