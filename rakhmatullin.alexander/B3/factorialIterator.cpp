#include "factorialIterator.hpp"

FactorialIterator::FactorialIterator():
  index_(1),
  value_(1)
{ }

FactorialIterator::FactorialIterator(std::size_t index):
  index_(index),
  value_(count(index))
{ }

FactorialIterator::reference FactorialIterator::operator *()
{
  return value_;
}

FactorialIterator::pointer FactorialIterator::operator ->()
{
  return & value_;
}

FactorialIterator & FactorialIterator::operator ++()
{
  if (index_ > 10)
  {
    throw std::out_of_range("Index is out of range");
  }
  ++index_;
  value_ *= index_;
  return * this;
}

FactorialIterator FactorialIterator::operator ++(int)
{
  FactorialIterator tmp = * this;
  this->operator++();
  return tmp;
}

FactorialIterator & FactorialIterator::operator --()
{
  if (index_ <= 1)
  {
    throw std::out_of_range("Index is out of range");
  }

  value_ /= index_;
  --index_;

  return * this;
}

FactorialIterator FactorialIterator::operator --(int)
{
  FactorialIterator tmp = * this;
  this->operator--();
  return tmp;
}

bool FactorialIterator::operator ==(const FactorialIterator & iter) const
{
  return ((value_ == iter.value_) && (index_ == iter.index_));
}

bool FactorialIterator::operator!=(const FactorialIterator & iter) const
{
  return (!(* this == iter));
}

unsigned long long FactorialIterator::count(std::size_t index) const
{
  return (index <= 1) ? 1 : (index * count(index -1));
}

FactorialIterator FactorialContainer::begin()
{
  return FactorialIterator(1);
}

FactorialIterator FactorialContainer::end()
{
  return FactorialIterator(11);
}
