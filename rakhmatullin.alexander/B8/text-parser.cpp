#include "text-parser.hpp"

const std::size_t MAX_LENGTH_WORD = 20;
const std::size_t LENGTH_HYPHEN = 3;

TextParser::TextParser(std::istream & in, std::ostream & out, std::size_t width) :
  in_(in),
  out_(out),
  width_(width),
  text_(),
  lastReadElement_()
{ }

void TextParser::readText()
{
  std::locale locale(in_.getloc());
  while (in_)
  {
    in_ >> std::ws;
    char firstChar = static_cast<char>(in_.get());
    char secondChar = static_cast<char>(in_.peek());
    in_.unget();

    if (std::isalpha(firstChar, locale))
    {
      readWord();
    }
    else if ((std::isdigit(firstChar, locale)) || ((firstChar == '+' || firstChar == '-') && std::isdigit(secondChar, locale)))
    {
      readNumber();
    }
    else if (std::ispunct(firstChar, locale))
    {
      if (firstChar == '-')
      {
        readHyphen();
      }
      else
      {
        readPunctuation();
      }
    }
  }
}

void TextParser::readWord()
{
  TextElement word {
    TextElement::WORD,
    ""
  };

  std::locale locale(in_.getloc());

  while (std::isalpha<char>(in_.peek(), locale) || (in_.peek() == '-'))
  {
    char ch = static_cast<char>(in_.get());
    if (ch == '-' && in_.peek() == '-')
    {
      in_.unget();
      break;
    }
    word.content.push_back(ch);
  }

  if (word.content.length() > MAX_LENGTH_WORD)
  {
    throw std::invalid_argument("Length of \"" + word.content + "\" more than 20 symbols");
  }

  lastReadElement_ = word;
  text_.push_back(word);
}

void TextParser::readPunctuation()
{
  if (text_.empty())
  {
    throw std::invalid_argument("Text can't start whit punctuation");
  }

  TextElement punc {
    TextElement::PUNCTUATION,
    ""
  };

  punc.content.push_back(static_cast<char>(in_.get()));

  if (lastReadElement_.type == TextElement::HYPHEN)
  {
    throw std::invalid_argument("Symbol of punctuation after hyphen");
  }
  if (lastReadElement_.type == TextElement::PUNCTUATION)
  {
    throw std::invalid_argument("Symbol of punctuation after symbol of punctuation");
  }

  lastReadElement_ = punc;
  text_.push_back(punc);
}

void TextParser::readNumber()
{
  TextElement number {
    TextElement::NUMBER,
    ""
  };

  std::locale locale(in_.getloc());

  const char decimalDot = std::use_facet<std::numpunct<char>>(locale).decimal_point();
  bool wasDot = false;
  number.content.push_back(in_.get());

  while (std::isdigit<char>(in_.peek(), locale) || (in_.peek() == decimalDot))
  {
    char ch = static_cast<char>(in_.get());
    if (ch == decimalDot)
    {
      if (wasDot)
      {
        in_.unget();
        break;
      }
      wasDot = true;
    }
    number.content.push_back(ch);
  }

  if (number.content.length() > MAX_LENGTH_WORD)
  {
    throw std::invalid_argument("Length of \"" + number.content + "\" more than 20 symbols");
  }

  lastReadElement_ = number;
  text_.push_back(number);
}

void TextParser::readHyphen()
{
  if (text_.empty())
  {
    throw std::invalid_argument("Text can't start whit hyphen");
  }

  TextElement hyphen {
    TextElement::HYPHEN,
    ""
  };

  while (in_.peek() == '-')
  {
    hyphen.content.push_back(static_cast<char>(in_.get()));
  }

  if (hyphen.content.length() != LENGTH_HYPHEN)
  {
    throw std::invalid_argument("Invalid symbol: " + hyphen.content);
  }
  if (lastReadElement_.type == TextElement::HYPHEN)
  {
    throw std::invalid_argument("Hyphen after hyphen");
  }
  if (lastReadElement_.type == TextElement::PUNCTUATION && text_.back().content != ",")
  {
    throw std::invalid_argument("Hyphen after punctuation");
  }

  lastReadElement_ = hyphen;
  text_.push_back(hyphen);
}

void TextParser::printText()
{
  std::size_t lengthLine = 0;
  std::list<TextElement> line;

  for (auto & iter : text_)
  {
    switch (iter.type)
    {
    case TextElement::NUMBER :
    case TextElement::WORD :
    {
      if ((lengthLine + iter.content.length() + 1) > width_)
      {
        printLine(line);
        line.clear();
        lengthLine = 0;
      }
      else if (!line.empty())
      {
        line.push_back(TextElement{TextElement::SPACE, " "});
        lengthLine++;
      }
      line.push_back(iter);
      lengthLine += iter.content.length();
      break;
    }
    case TextElement::PUNCTUATION :
    {
      if (lengthLine + 1 > width_)
      {
        printReformatLine(line, lengthLine);
      }
      line.push_back(iter);
      lengthLine += iter.content.length();
      break;
    }
    case TextElement::HYPHEN :
    {
      if (lengthLine + LENGTH_HYPHEN + 1 > width_)
      {
        printReformatLine(line, lengthLine);
      }
      line.push_back(TextElement{TextElement::SPACE, " "});
      line.push_back(iter);
      lengthLine += iter.content.length() + 1;
      break;
    }
    case TextElement::SPACE :
    {
      break;
    }
    }
  }
  if (!line.empty())
  {
    printLine(line);
  }
}

void TextParser::printLine(const std::list<TextElement> & line)
{
  for (const auto & iter : line)
  {
    out_ << iter.content;
  }
  out_ << '\n';
}

void TextParser::printReformatLine(std::list< TextElement > & line, std::size_t & lengthLine)
{
  std::size_t tmpLength = 0;
  std::list<TextElement> tmpLine;

  while (!line.empty())
  {
    tmpLine.push_front(line.back());
    tmpLength += line.back().content.length();
    line.pop_back();

    if (tmpLine.front().type == TextElement::WORD || tmpLine.front().type == TextElement::NUMBER)
    {
      break;
    }
  }
  printLine(line);
  line = tmpLine;
  lengthLine = tmpLength;
}


