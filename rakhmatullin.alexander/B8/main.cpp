#include <iostream>
#include <exception>
#include <string>
#include "text-parser.hpp"

const int MIN_WIDTH = 25;

int main(int argc, char * argv[])
{
  try
  {
    if ((argc != 3) && (argc != 1))
    {
      std::cerr << "Invalid number of arguments.\n";
      return 1;
    }

    int width = 40;

    if (argc == 3)
    {
      if (std::string(argv[1]) != "--line-width")
      {
        std::cerr << "Invalid first argument.\n";
        return 1;
      }

      char * end;
      width = std::strtol(std::string(argv[2]).c_str(), & end, 10);
      if ((*end != '\0') || (width < MIN_WIDTH))
      {
        std::cerr << "Invalid width argument.\n";
        return 1;
      }

    }

    TextParser parser(std::cin, std::cout, width);
    parser.readText();
    parser.printText();
  }
  catch (std::exception & err)
  {
    std::cerr << err.what() << '\n';
    return 1;
  }
  return 0;
}
