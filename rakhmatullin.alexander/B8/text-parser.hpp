#ifndef TEXT_PARSER_HPP
#define TEXT_PARSER_HPP

#include <iostream>
#include <list>

struct TextElement
{
  enum {
    WORD,
    PUNCTUATION,
    NUMBER,
    HYPHEN,
    SPACE
  } type;
  std::string content;
};

class TextParser
{
public:
  TextParser(std::istream & in, std::ostream & out, std::size_t width);
  void readText();
  void printText();

private:
  std::istream & in_;
  std::ostream & out_;
  std::size_t width_;
  std::list<TextElement> text_;
  TextElement lastReadElement_;

  void readWord();
  void readPunctuation();
  void readNumber();
  void readHyphen();
  void printLine(const std::list<TextElement> & line);
  void printReformatLine(std::list<TextElement> & line, std::size_t & lengthLine);
};

#endif //TEXT_PARSER_HPP
