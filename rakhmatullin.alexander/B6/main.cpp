#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

#include "container-statistics.hpp"

int main()
{
  try
  {
    ContainerStatistics statistics;
    std::vector<int> container;
    std::string line;

    while (std::cin >> line)
    {
      if (std::cin.fail())
      {
        std::cerr << "Reading failed\n";
        return 1;
      }

      char * end;
      int value = std::strtol(line.c_str(), & end, 10);
      if (* end != '\0')
      {
        std::cerr << "Invalid input\n";
        return 1;
      }

      container.push_back(value);
    }

    statistics = std::for_each(container.begin(), container.end(), statistics);

    statistics.printStatistics();

  }
  catch (std::exception & err)
  {
    std::cerr << err.what() << '\n';
  }

}
