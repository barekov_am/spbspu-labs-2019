#ifndef CONTAINER_STATISTICS_HPP
#define CONTAINER_STATISTICS_HPP

#include <cstddef>

class ContainerStatistics
{
public:
  ContainerStatistics() noexcept;

  void operator()(int value);
  void printStatistics();

private:
  int max_;
  int min_;
  double mean_;
  std::size_t positive_;
  std::size_t negative_;
  long long int oddSum_;
  long long int evenSum_;
  int first_;
  int last_;
  std::size_t countOfValue_;
};

#endif //CONTAINER_STATISTICS_HPP
