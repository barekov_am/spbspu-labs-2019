#include "container-statistics.hpp"
#include <limits>
#include <iostream>

ContainerStatistics::ContainerStatistics() noexcept :
  max_(std::numeric_limits<int>::min()),
  min_(std::numeric_limits<int>::max()),
  mean_(0.0),
  positive_(0),
  negative_(0),
  oddSum_(0),
  evenSum_(0),
  first_(0),
  last_(0),
  countOfValue_(0)
{ }

void ContainerStatistics::operator()(int value)
{
  if (countOfValue_ == 0)
  {
    first_ = value;
  }

  ++countOfValue_;
  last_ = value;

  max_ = std::max(max_, value);
  min_ = std::min(min_, value);

  if (value > 0)
  {
    ++positive_;
  }
  else if (value < 0)
  {
    ++negative_;
  }

  if ((value % 2) == 0)
  {
    evenSum_ += value;
  }
  else
  {
    oddSum_ += value;
  }
}

void ContainerStatistics::printStatistics()
{
  if (countOfValue_ < 1)
  {
    std::cout << "No Data\n";
    return;
  }

  std::cout << "Max: " << max_ << '\n';
  std::cout << "Min: " << min_ << '\n';

  mean_ = static_cast<double>(oddSum_ + evenSum_) / static_cast<double>(countOfValue_);

  std::cout << "Mean: " << mean_ << '\n';
  std::cout << "Positive: " << positive_ << '\n';
  std::cout << "Negative: " << negative_ << '\n';
  std::cout << "Odd Sum: " << oddSum_ << '\n';
  std::cout << "Even Sum: " << evenSum_ << '\n';
  std::cout << "First/Last Equal: " << ((first_ == last_) ? "yes" : "no") << '\n';
}
