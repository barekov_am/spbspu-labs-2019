#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "memory"

namespace rakhmatullin
{
  template <typename T>
  class Matrix
  {
  public:
    using type_ptr = std::shared_ptr<T>;

    class Array
    {
    public:
      Array(const Array &);
      Array(Array &&) noexcept;
      Array(type_ptr *, std::size_t);
      ~Array() = default;

      Array & operator =(const Array &);
      Array & operator =(Array &&) noexcept;
      type_ptr operator [](std::size_t) const;

      std::size_t size() const;
      void swap(Array &) noexcept;
    private:
      std::size_t size_;
      std::unique_ptr<type_ptr []> array_;
    };

    Matrix();
    Matrix(const Matrix &);
    Matrix(Matrix &&) noexcept;
    ~Matrix() = default;

    Matrix & operator =(const Matrix &);
    Matrix & operator =(Matrix &&) noexcept;
    Array operator [](std::size_t) const;
    bool operator ==(const Matrix &);
    bool operator !=(const Matrix &);

    void add(std::size_t, type_ptr);
    std::size_t getRows() const;
    void swap(Matrix &) noexcept;
  private:
    std::size_t rows_;
    std::size_t count_;
    std::unique_ptr<std::size_t []> columns_;
    std::unique_ptr<type_ptr []> data_;
  };
} //namespace rakhmatullin

template <typename T>
rakhmatullin::Matrix<T>::Matrix() :
  rows_(0),
  count_(0)
{
}

template <typename T>
rakhmatullin::Matrix<T>::Matrix(const rakhmatullin::Matrix<T> & other) :
  rows_(other.rows_),
  count_(other.count_),
  columns_(std::make_unique<std::size_t []>(other.rows_)),
  data_(std::make_unique<Matrix::type_ptr []>(other.count_))
{
  for (std::size_t i = 0; i < rows_; i++)
  {
    columns_[i] = other.columns_[i];
  }
  for (std::size_t i = 0; i < count_; i++)
  {
    data_[i] = other.data_[i];
  }
}

template <typename T>
rakhmatullin::Matrix<T>::Matrix(rakhmatullin::Matrix<T> && other) noexcept :
  rows_(other.rows_),
  count_(other.count_),
  columns_(std::move(other.columns_)),
  data_(std::move(other.data_))
{
  other.rows_ = 0;
  other.count_ = 0;
}

template <typename T>
rakhmatullin::Matrix<T> & rakhmatullin::Matrix<T>::operator =(const rakhmatullin::Matrix<T> & other)
{
  if (this != & other)
  {
    Matrix<T>(other).swap(* this);
  }
  return * this;
}

template <typename T>
rakhmatullin::Matrix<T> & rakhmatullin::Matrix<T>::operator =(rakhmatullin::Matrix<T> && other) noexcept
{
  if (this != & other)
  {
    rows_ = other.rows_;
    other.rows_ = 0;

    count_ = other.count_;
    other.count_ = 0;

    columns_ = std::move(other.columns_);

    data_ = std::move(other.data_);
  }
  return * this;
}

template <typename T>
typename rakhmatullin::Matrix<T>::Array rakhmatullin::Matrix<T>::operator [](std::size_t indexRow) const
{
  if (indexRow >= rows_)
  {
    throw std::out_of_range("Row index out of range");
  }

  std::size_t startIndex = 0;
  for (std::size_t i = 0; i < indexRow; i++)
  {
    startIndex += columns_[i];
  }

  return Matrix<T>::Array(& data_[startIndex], columns_[indexRow]);
}

template <typename T>
bool rakhmatullin::Matrix<T>::operator ==(const rakhmatullin::Matrix<T> & other)
{
  if ((rows_ != other.rows_) || (count_ != other.count_))
  {
    return false;
  }

  for (std::size_t i = 0; i < rows_; i++)
  {
    if (columns_[i] != other.columns_[i])
    {
      return false;
    }
  }

  for (std::size_t i = 0; i < count_; i++)
  {
    if (data_[i] != other.data_[i])
    {
      return false;
    }
  }

  return true;
}

template <typename T>
bool rakhmatullin::Matrix<T>::operator !=(const rakhmatullin::Matrix<T> & other)
{
  return !(*this == other);
}

template <typename T>
void rakhmatullin::Matrix<T>::add(std::size_t row, std::shared_ptr<T> typeObject)
{
  if (row > rows_)
  {
    throw std::out_of_range("Index out of range");
  }
  if (!typeObject)
  {
    throw std::invalid_argument("Element to be added can't be nullptr");
  }

  std::unique_ptr<Matrix::type_ptr []> tempData = std::make_unique<Matrix::type_ptr []>(count_ + 1);
  std::size_t indexTempNew = 0;
  std::size_t indexTempOld = 0;

  for (std::size_t i = 0; i < rows_; i++)
  {
    for (std::size_t j = 0; j < columns_[i]; j++)
    {
      tempData[indexTempNew++] = data_[indexTempOld++];
    }
    if (i == row)
    {
      tempData[indexTempNew] = typeObject;
      columns_[row]++;
    }
  }

  if (row == rows_)
  {
    std::unique_ptr<std::size_t []> tempColumns = std::make_unique<std::size_t []>(rows_ + 1);
    for (std::size_t i = 0; i < rows_; i++)
    {
      tempColumns[i] = columns_[i];
    }
    tempColumns[row] = 1;
    tempData[count_] = typeObject;
    rows_++;
    columns_.swap(tempColumns);
  }

  data_.swap(tempData);
  count_++;
}

template <typename T>
std::size_t rakhmatullin::Matrix<T>::getRows() const
{
  return rows_;
}

template <typename T>
void rakhmatullin::Matrix<T>::swap(rakhmatullin::Matrix<T> & other) noexcept
{
  std::swap(rows_, other.rows_);
  std::swap(count_, other.count_);
  std::swap(columns_, other.columns_);
  std::swap(data_, other.data_);
}

template <typename T>
rakhmatullin::Matrix<T>::Array::Array(const rakhmatullin::Matrix<T>::Array & other) :
  size_(other.size_),
  array_(std::make_unique<type_ptr []>(other.size_))
{
  for (std::size_t i = 0; i < size_; i++)
  {
    array_[i] = other.array_[i];
  }
}

template <typename T>
rakhmatullin::Matrix<T>::Array::Array(rakhmatullin::Matrix<T>::Array && other) noexcept :
  size_(other.size_),
  array_(std::move(other.array_))
{
  other.size_ = 0;
}

template <typename T>
rakhmatullin::Matrix<T>::Array::Array(rakhmatullin::Matrix<T>::type_ptr * array, std::size_t size) :
  size_(size),
  array_(std::make_unique<type_ptr []>(size))
{
  for (std::size_t i = 0; i < size_; i++)
  {
    array_[i] = array[i];
  }
}

template <typename T>
typename rakhmatullin::Matrix<T>::Array & rakhmatullin::Matrix<T>::Array::operator =(const rakhmatullin::Matrix<T>::Array & other)
{
  if (this != & other)
  {
    array_ = std::make_unique<type_ptr []>(other.size_);
    for (std::size_t i = 0; i < other.size_; i++)
    {
      array_[i] = other.array[i];
    }
    size_ = other.size_;
  }
  return * this;
}

template <typename T>
typename rakhmatullin::Matrix<T>::Array & rakhmatullin::Matrix<T>::Array::operator =(rakhmatullin::Matrix<T>::Array && other) noexcept
{
  if (this != & other)
  {
    size_ = other.size_;
    other.size_ = 0;

    array_ = std::move(other.array_);
  }
  return * this;
}

template <typename T>
typename rakhmatullin::Matrix<T>::type_ptr rakhmatullin::Matrix<T>::Array::operator [](std::size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Index out of range");
  }
  return array_[index];
}

template <typename T>
std::size_t rakhmatullin::Matrix<T>::Array::size() const
{
  return size_;
}

template <typename T>
void rakhmatullin::Matrix<T>::Array::swap(rakhmatullin::Matrix<T>::Array & other) noexcept
{
  std::swap(size_, other.size_);
  std::swap(array_, other.array_);
}

#endif //MATRIX_HPP
