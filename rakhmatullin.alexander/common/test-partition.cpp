#include <memory>

#include <boost/test/auto_unit_test.hpp>

#include "matrix.hpp"
#include "partition.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testPartitionMethods)

BOOST_AUTO_TEST_CASE(testPartition)
{
  rakhmatullin::Circle circle1({0.0, 0.0}, 3.0);
  rakhmatullin::Circle circle2({6.0, 5.0}, 2.0);
  rakhmatullin::Rectangle rectangle1({-5.0, 4.0}, 4.0, 6.0);

  std::shared_ptr<rakhmatullin::Shape> pointerCircle1 = std::make_shared<rakhmatullin::Circle>(circle1);
  std::shared_ptr<rakhmatullin::Shape> pointerCircle2 = std::make_shared<rakhmatullin::Circle>(circle2);
  std::shared_ptr<rakhmatullin::Shape> pointerRectangle1 = std::make_shared<rakhmatullin::Rectangle>(rectangle1);

  rakhmatullin::CompositeShape testComposite;
  testComposite.addShape(pointerCircle1);
  testComposite.addShape(pointerCircle2);
  testComposite.addShape(pointerRectangle1);

  rakhmatullin::Matrix<rakhmatullin::Shape> testMatrix = rakhmatullin::part(testComposite);

  BOOST_CHECK(testMatrix[0][0] == pointerCircle1);
  BOOST_CHECK(testMatrix[0][1] == pointerCircle2);
  BOOST_CHECK(testMatrix[1][0] == pointerRectangle1);
  BOOST_CHECK_EQUAL(testMatrix.getRows(), 2);
  BOOST_CHECK_EQUAL(testMatrix[0].size(), 2);
  BOOST_CHECK_EQUAL(testMatrix[1].size(), 1);
}

BOOST_AUTO_TEST_CASE(testIntersect)
{
  rakhmatullin::Rectangle testRect({0, 0}, 6, 6);
  rakhmatullin::Circle testCircle1({-5, 0}, 2);
  rakhmatullin::Circle testCircle2({0, 5}, 2);
  rakhmatullin::Circle testCircle3({0, -5}, 2);
  rakhmatullin::Circle testCircle4({10, 0}, 2);
  rakhmatullin::Circle testCircle5({10, 10}, 1);

  BOOST_CHECK(rakhmatullin::intersect(testRect.getFrameRect(), testCircle1.getFrameRect()));
  BOOST_CHECK(rakhmatullin::intersect(testRect.getFrameRect(), testCircle2.getFrameRect()));
  BOOST_CHECK(rakhmatullin::intersect(testRect.getFrameRect(), testCircle3.getFrameRect()));

  BOOST_CHECK(!rakhmatullin::intersect(testRect.getFrameRect(), testCircle4.getFrameRect()));
  BOOST_CHECK(!rakhmatullin::intersect(testRect.getFrameRect(), testCircle5.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
