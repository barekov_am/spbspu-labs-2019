#include "partition.hpp"
#include <cmath>

rakhmatullin::Matrix<rakhmatullin::Shape>
  rakhmatullin::part(const rakhmatullin::CompositeShape & composite)
{
  rakhmatullin::Matrix<rakhmatullin::Shape> matrix;

  for (std::size_t i = 0; i < composite.getCountOfShape(); i++)
  {
    std::size_t tempRow = 0;

    for (std::size_t j = matrix.getRows(); j-- > 0;)
    {
      bool inter = false;

      for (std::size_t k = 0; k < matrix[j].size(); k++)
      {
        if (intersect(composite[i]->getFrameRect(), matrix[j][k]->getFrameRect()))
        {
          tempRow = j + 1;
          inter = true;
          break;
        }
      }
      if (inter)
      {
        break;
      }
    }
    matrix.add(tempRow, composite[i]);
  }
  return matrix;
}

bool rakhmatullin::intersect(const rakhmatullin::rectangle_t & frame1, const rakhmatullin::rectangle_t & frame2)
{
  if (std::abs(frame1.pos.x - frame2.pos.x) > (std::abs(frame1.width + frame2.width) / 2.0))
  {
    return false;
  }
  return std::abs(frame1.pos.y - frame2.pos.y) <= std::abs(frame1.height + frame2.height) / 2.0;
}
