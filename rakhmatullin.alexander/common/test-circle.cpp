#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

const double DIFF = 0.0001;

BOOST_AUTO_TEST_SUITE(testCircleMethods)

BOOST_AUTO_TEST_CASE(testCircleSavedParamAfterShiftMoving)
{
  //Initialization of begins values
  rakhmatullin::Circle testCircle({7.2, 6.8}, 10.0);
  const double circleParamAreaBeforeMoving = testCircle.getArea();
  const rakhmatullin::rectangle_t circleParamFrameBeforeMoving = testCircle.getFrameRect();

  //shift circle on (-2.3, 1.0)
  testCircle.move(-2.3, 1.0);

  //circle parameter check
  BOOST_CHECK_CLOSE(circleParamAreaBeforeMoving, testCircle.getArea(), DIFF);
  rakhmatullin::rectangle_t circleParamFrameAfterMoving = testCircle.getFrameRect();
  BOOST_CHECK_EQUAL(circleParamFrameBeforeMoving.width, circleParamFrameAfterMoving.width);
  BOOST_CHECK_EQUAL(circleParamFrameBeforeMoving.height, circleParamFrameAfterMoving.height);
}

BOOST_AUTO_TEST_CASE(testCircleSavedParamAfterMovingTo)
{
  //Initialization of begins values
  rakhmatullin::Circle testCircle({7.2, 6.8}, 10.0);
  const double circleParamAreaBeforeMoving = testCircle.getArea();
  const rakhmatullin::rectangle_t circleParamFrameBeforeMoving = testCircle.getFrameRect();

  //Move circle to (5.2, -3.9)
  testCircle.move({5.2, -3.9});

  //circle parameter check
  BOOST_CHECK_CLOSE(circleParamAreaBeforeMoving, testCircle.getArea(), DIFF);
  rakhmatullin::rectangle_t circleParamFrameAfterMoving = testCircle.getFrameRect();
  BOOST_CHECK_EQUAL(circleParamFrameBeforeMoving.width, circleParamFrameAfterMoving.width);
  BOOST_CHECK_EQUAL(circleParamFrameBeforeMoving.height, circleParamFrameAfterMoving.height);
}

BOOST_AUTO_TEST_CASE(testCircleFunctionScale)
{
  //Initialization of begins values
  rakhmatullin::Circle testCircle({7.2, 6.8}, 10.0);
  const double circleParamAreaBeforeScale = testCircle.getArea();
  const double testScale = 3;

  //scale circle in 3 times
  testCircle.scale(testScale);

  //circle parameter check (The area should increase 9 times)
  BOOST_CHECK_CLOSE(circleParamAreaBeforeScale * testScale * testScale, testCircle.getArea(), DIFF);
}

BOOST_AUTO_TEST_CASE(circleTestInvalidConstructorParam)
{
  //invalid radius param
  BOOST_CHECK_THROW(rakhmatullin::Circle({7.2, 6.8}, -10.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circleTestInvalidScale)
{
  rakhmatullin::Circle testCircle({7.2, 6.8}, 10.0);
  BOOST_CHECK_THROW(testCircle.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circleTestRotate)
{
  //Initialization
  rakhmatullin::Circle testCircle({1.0, 1.0}, 5.0);
  const double circleAreaBeforeRotate = testCircle.getArea();
  const rakhmatullin::rectangle_t circleFrameRectBeforeRotate = testCircle.getFrameRect();
  double angle = 90.0;

  //Rotate Circle
  testCircle.rotate(angle);

  //Circle parameter check
  double circleAreaAfterRotate = testCircle.getArea();
  rakhmatullin::rectangle_t circleFrameRectAfterRotate = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(circleAreaBeforeRotate, circleAreaAfterRotate, DIFF);
  BOOST_CHECK_CLOSE(circleFrameRectBeforeRotate.height, circleFrameRectAfterRotate.height, DIFF);
  BOOST_CHECK_CLOSE(circleFrameRectBeforeRotate.width, circleFrameRectAfterRotate.width, DIFF);
  BOOST_CHECK_CLOSE(circleFrameRectBeforeRotate.pos.x, circleFrameRectAfterRotate.pos.x, DIFF);
  BOOST_CHECK_CLOSE(circleFrameRectBeforeRotate.pos.y, circleFrameRectAfterRotate.pos.y, DIFF);
}

BOOST_AUTO_TEST_SUITE_END()
