#include "composite-shape.hpp"

#include <stdexcept>
#include <iostream>
#include <memory>
#include <algorithm>
#include <cmath>

rakhmatullin::CompositeShape::CompositeShape() :
  countOfShape_(0),
  sizeOfArray_(0)
{
}

rakhmatullin::CompositeShape::CompositeShape(rakhmatullin::Shape::ptr shape) :
  countOfShape_(1),
  sizeOfArray_(1),
  shapeArray_ (std::make_unique<rakhmatullin::Shape::ptr []>(sizeOfArray_))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("shape is not exist");
  }
  shapeArray_[0] = shape;
}

rakhmatullin::CompositeShape::CompositeShape(const rakhmatullin::CompositeShape & other) :
  countOfShape_(other.countOfShape_),
  sizeOfArray_(other.sizeOfArray_),
  shapeArray_(std::make_unique<rakhmatullin::Shape::ptr []>(other.sizeOfArray_))
{
  for (std::size_t i = 0; i < sizeOfArray_; i++)
  {
    shapeArray_[i] = other.shapeArray_[i];
  }
}

rakhmatullin::CompositeShape::CompositeShape(rakhmatullin::CompositeShape && other) noexcept :
  countOfShape_(other.countOfShape_),
  sizeOfArray_(other.sizeOfArray_),
  shapeArray_(std::move(other.shapeArray_))
{
  other.countOfShape_ = 0;
  other.sizeOfArray_ = 0;
}

rakhmatullin::CompositeShape & rakhmatullin::CompositeShape::operator =(const rakhmatullin::CompositeShape & other)
{
  if (this != &other)
  {
    countOfShape_ = other.countOfShape_;
    sizeOfArray_ = other.sizeOfArray_;

    shapeArray_ = std::make_unique<rakhmatullin::Shape::ptr []>(other.sizeOfArray_);

    for (std::size_t i = 0; i < other.sizeOfArray_; i++)
    {
      shapeArray_[i] = other.shapeArray_[i];
    }
  }
  return *this;
}

rakhmatullin::CompositeShape & rakhmatullin::CompositeShape::operator =(rakhmatullin::CompositeShape && other) noexcept
{
  if (this != &other)
  {
    countOfShape_ = other.countOfShape_;
    other.countOfShape_ = 0;

    sizeOfArray_ = other.sizeOfArray_;
    other.sizeOfArray_ = 0;

    shapeArray_ = std::move(other.shapeArray_);
  }
  return *this;
}

std::shared_ptr<rakhmatullin::Shape> rakhmatullin::CompositeShape::operator [](std::size_t index) const
{
  if (countOfShape_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  if (index >= countOfShape_)
  {
    throw std::out_of_range("Index out of range");
  }
  return shapeArray_[index];
}

void rakhmatullin::CompositeShape::addShape(rakhmatullin::Shape::ptr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Added shape is not exist");
  }

  if (countOfShape_ == sizeOfArray_)
  {
    rakhmatullin::Shape::array tempArray = std::make_unique<rakhmatullin::Shape::ptr []>(sizeOfArray_ + 1);
    for (std::size_t i = 0; i < sizeOfArray_; i++)
    {
      tempArray[i] = shapeArray_[i];
    }

    sizeOfArray_++;
    shapeArray_ = std::move(tempArray);
  }
  countOfShape_++;
  shapeArray_[countOfShape_ - 1] = shape;
}

void rakhmatullin::CompositeShape::deleteShape(std::size_t index)
{
  if (countOfShape_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  if (index >= countOfShape_)
  {
    throw std::out_of_range("Index out of range");
  }

  for (std::size_t i = index; i < countOfShape_ - 1; i++)
  {
    shapeArray_[i] = shapeArray_[i + 1];
  }
  countOfShape_--;
  shapeArray_[countOfShape_] = nullptr;
}

std::size_t rakhmatullin::CompositeShape::getCountOfShape() const
{
  return countOfShape_;
}

void rakhmatullin::CompositeShape::printInfo() const
{
  const rectangle_t tempRect = getFrameRect();
  std::cout << "\nInfo about composite shape:\n";
  std::cout << "Count of shape: " << countOfShape_ << '\n';
  std::cout << "Width: " << tempRect.width << '\n';
  std::cout << "Height: " << tempRect.height << '\n';
  std::cout << "Center: (" << tempRect.pos.x << ',' << tempRect.pos.y << ")\n";
  std::cout << "Area: " << getArea() << '\n';
}

double rakhmatullin::CompositeShape::getArea() const
{
  if (countOfShape_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  double area = 0.0;
  for (std::size_t i = 0; i < countOfShape_; i++)
  {
    area += shapeArray_[i]->getArea();
  }

  return area;
}

rakhmatullin::rectangle_t rakhmatullin::CompositeShape::getFrameRect() const
{
  if (countOfShape_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  rectangle_t tempRect = shapeArray_[0]->getFrameRect();
  double maxX = tempRect.pos.x + tempRect.width / 2.0;
  double minX = tempRect.pos.x - tempRect.width / 2.0;
  double maxY = tempRect.pos.y + tempRect.height / 2.0;
  double minY = tempRect.pos.y - tempRect.height / 2.0;

  for (std::size_t i = 1; i < countOfShape_; i++)
  {
    tempRect = shapeArray_[i]->getFrameRect();
    maxX = std::max(maxX, tempRect.pos.x + tempRect.width / 2.0);
    minX = std::min(minX, tempRect.pos.x - tempRect.width / 2.0);
    maxY = std::max(maxY, tempRect.pos.y + tempRect.height / 2.0);
    minY = std::min(minY, tempRect.pos.y - tempRect.height / 2.0);
  }

  tempRect.width = maxX - minX;
  tempRect.height = maxY - minY;
  tempRect.pos = {(minX + maxX) / 2.0, (minY + maxY) / 2.0};
  return tempRect;
}

void rakhmatullin::CompositeShape::move(const rakhmatullin::point_t & position)
{
  rectangle_t tempRect = getFrameRect();
  double shiftX = position.x - tempRect.pos.x;
  double shiftY = position.y - tempRect.pos.y;

  move(shiftX, shiftY);
}

void rakhmatullin::CompositeShape::move(double shiftX, double shiftY)
{
  if (countOfShape_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  for (std::size_t i = 0; i < countOfShape_; i++)
  {
    shapeArray_[i]->move(shiftX, shiftY);
  }
}

void rakhmatullin::CompositeShape::scale(double factor)
{
  if (factor <= 0.0)
  {
    throw std::invalid_argument("Factor of scale should be positive");
  }
  if (countOfShape_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  rakhmatullin::rectangle_t tempRect = getFrameRect();
  for (std::size_t i = 0; i < countOfShape_; i++)
  {
    double shiftX = (shapeArray_[i]->getFrameRect().pos.x - tempRect.pos.x) * (factor - 1.0);
    double shiftY = (shapeArray_[i]->getFrameRect().pos.y - tempRect.pos.y) * (factor - 1.0);
    shapeArray_[i]->scale(factor);
    shapeArray_[i]->move(shiftX, shiftY);
  }
}

void rakhmatullin::CompositeShape::rotate(double angle)
{
  const double cosAngle = std::cos(angle * M_PI / 180);
  const double sinAngle = std::sin(angle * M_PI / 180);
  const rakhmatullin::point_t centerComp = getFrameRect().pos;

  for (std::size_t i = 0; i < countOfShape_; i++)
  {
    const rakhmatullin::point_t centerShape = shapeArray_[i]->getFrameRect().pos;
    const double oldX = centerShape.x - centerComp.x;
    const double oldY = centerShape.y - centerComp.y;
    const double shift_x = (oldX * (cosAngle - 1.0)) - (oldY * sinAngle);
    const double shift_y = (oldX * sinAngle) - (oldY * (cosAngle - 1.0));
    shapeArray_[i]->move(shift_x, shift_y);
    shapeArray_[i]->rotate(angle);
  }
}
