#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

rakhmatullin::Rectangle::Rectangle(const rakhmatullin::point_t &center, double width, double height) :
  center_(center),
  width_(width),
  height_(height),
  angle_(0.0)
{
  if ((width_ < 0.0) || (height_ < 0.0)) {
    throw std::invalid_argument("Rectangle width and height should be positive");
  }
}

rakhmatullin::Rectangle::Rectangle(const rakhmatullin::point_t &center, double width, double height, double angle) :
  Rectangle(center, width, height)
{
  rotate(angle);
}

double rakhmatullin::Rectangle::getArea() const
{
  return (width_ * height_);
}

rakhmatullin::rectangle_t rakhmatullin::Rectangle::getFrameRect() const
{
  const double cosAngle = std::cos(angle_ * M_PI / 180);
  const double sinAngle = std::sin(angle_ * M_PI / 180);
  const double width = std::abs(width_ * cosAngle) + std::abs(height_ * sinAngle);
  const double height = std::abs(width_ * sinAngle) + std::abs(height_ * cosAngle);
  return {width, height, center_};
}

void rakhmatullin::Rectangle::move(const rakhmatullin::point_t & position)
{
  center_ = position;
}

void rakhmatullin::Rectangle::move(double shift_x, double shift_y)
{
  center_.x += shift_x;
  center_.y += shift_y;
}

void rakhmatullin::Rectangle::scale(double factor)
{
  if (factor < 0.0) {
    throw std::invalid_argument("Factor of scale should be positive");
  }
  width_ *= factor;
  height_ *= factor;
}

void rakhmatullin::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
