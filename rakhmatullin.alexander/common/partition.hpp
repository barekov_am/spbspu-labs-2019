#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace rakhmatullin
{
  Matrix<Shape> part(const CompositeShape &);
  bool intersect(const rectangle_t &, const rectangle_t &);
}

#endif //PARTITION_HPP
