#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

const double DIFF = 0.0001;

BOOST_AUTO_TEST_SUITE(testCompositeShapeMethods)

BOOST_AUTO_TEST_CASE(testCompositeShapeDefaultConstructor)
{
  rakhmatullin::CompositeShape testCompShape;
  const int rightCountShape = 0;

  BOOST_CHECK_EQUAL(testCompShape.getCountOfShape(), rightCountShape);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeCopyConstructor)
{
  rakhmatullin::Circle testCircle({5.0, 5.0}, 10.0);
  rakhmatullin::Rectangle testRectangle({0.0, 4.0}, 3.0, 5.0);
  rakhmatullin::CompositeShape tempTestCompShape;

  tempTestCompShape.addShape(std::make_shared<rakhmatullin::Circle>(testCircle));
  tempTestCompShape.addShape(std::make_shared<rakhmatullin::Rectangle>(testRectangle));

  rakhmatullin::CompositeShape testCompShape(tempTestCompShape);
  BOOST_CHECK_EQUAL(testCompShape.getCountOfShape(), tempTestCompShape.getCountOfShape());
  BOOST_CHECK_EQUAL(testCompShape[0], tempTestCompShape[0]);
  BOOST_CHECK_EQUAL(testCompShape[1], tempTestCompShape[1]);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeAnotherConstructor)
{
  rakhmatullin::Circle testCircle({0.0, 0.0}, 5.0);
  rakhmatullin::CompositeShape testCompShape(std::make_shared<rakhmatullin::Circle>(testCircle));

  const std::size_t countOfShape = 1;
  BOOST_CHECK_EQUAL(testCompShape.getCountOfShape(), countOfShape);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeAnotherConstructorThrowExcept)
{
  BOOST_CHECK_THROW(rakhmatullin::CompositeShape testCompShape(nullptr),std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeAddShapeThrowExcept)
{
  rakhmatullin::CompositeShape testCompShape;

  BOOST_CHECK_THROW(testCompShape.addShape(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeAddShape)
{
  rakhmatullin::CompositeShape testCompShape;

  const std::size_t countOfShapeBeforeAdded = 0;
  BOOST_CHECK_EQUAL(testCompShape.getCountOfShape(), countOfShapeBeforeAdded);

  rakhmatullin::Circle testCircle({0.0, 0.0}, 5.0);
  testCompShape.addShape(std::make_shared<rakhmatullin::Circle>(testCircle));
  const std::size_t countOfShapeAfterAdded = 1;
  BOOST_CHECK_EQUAL(testCompShape.getCountOfShape(), countOfShapeAfterAdded);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeDeleteShapeThrowExcept)
{
  rakhmatullin::Circle testCircle({5.0, 5.0}, 10.0);
  rakhmatullin::Rectangle testRectangle({0.0, 4.0}, 3.0, 5.0);
  rakhmatullin::CompositeShape testCompShape;

  BOOST_CHECK_THROW(testCompShape.deleteShape(0), std::logic_error);

  testCompShape.addShape(std::make_shared<rakhmatullin::Circle>(testCircle));
  testCompShape.addShape(std::make_shared<rakhmatullin::Rectangle>(testRectangle));

  const int incorrectIndex = -1;
  BOOST_CHECK_THROW(testCompShape.deleteShape(incorrectIndex), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeDeleteShape)
{
  rakhmatullin::Circle testCircle({5.0, 5.0}, 10.0);
  rakhmatullin::Rectangle testRectangle({0.0, 4.0}, 3.0, 5.0);
  rakhmatullin::CompositeShape testCompShape;

  testCompShape.addShape(std::make_shared<rakhmatullin::Circle>(testCircle));
  testCompShape.addShape(std::make_shared<rakhmatullin::Rectangle>(testRectangle));

  const std::size_t countOfShapeBeforeDelete = 2;
  BOOST_CHECK_EQUAL(testCompShape.getCountOfShape(), countOfShapeBeforeDelete);
  testCompShape.deleteShape(1);
  const std::size_t countOfShapeAfterDelete = 1;
  BOOST_CHECK_EQUAL(testCompShape.getCountOfShape(), countOfShapeAfterDelete);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeTrueGetArea)
{
  rakhmatullin::Circle testCircle({0.0, 0.0}, 5.0);
  rakhmatullin::Rectangle testRectangle({0.0, 0.0}, 10.0, 5.0);
  rakhmatullin::CompositeShape testCompShape;

  testCompShape.addShape(std::make_shared<rakhmatullin::Circle>(testCircle));
  testCompShape.addShape(std::make_shared<rakhmatullin::Rectangle>(testRectangle));

  const double calculatedArea = testCircle.getArea() + testRectangle.getArea();
  BOOST_CHECK_CLOSE(testCompShape.getArea(), calculatedArea, DIFF);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeFrameRectThrowExcept)
{
  rakhmatullin::CompositeShape testCompShape;
  BOOST_CHECK_THROW(testCompShape.getFrameRect(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeMoveThrowExcept)
{
  rakhmatullin::CompositeShape testCompShape;

  const double shiftX = 3.0;
  const double shiftY = 3.0;

  BOOST_CHECK_THROW(testCompShape.move(shiftX, shiftY), std::logic_error);

  const double positionX = 3.0;
  const double positionY = 3.0;

  BOOST_CHECK_THROW(testCompShape.move({positionX, positionY}), std::logic_error);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeSavedParamAfterMoving)
{
  rakhmatullin::Circle testCircle({0.0, 5.0}, 10.0);
  rakhmatullin::Rectangle testRectangle({5.0, 5.0}, 5.0, 5.0);
  rakhmatullin::CompositeShape testCompShape;

  testCompShape.addShape(std::make_shared<rakhmatullin::Circle>(testCircle));
  testCompShape.addShape(std::make_shared<rakhmatullin::Rectangle>(testRectangle));

  const rakhmatullin::rectangle_t frameRectBeforeMoving = testCompShape.getFrameRect();
  const double AreaBeforeMoving = testCompShape.getArea();

  testCompShape.move({-10.0, 10.0});
  BOOST_CHECK_CLOSE(testCompShape.getFrameRect().height, frameRectBeforeMoving.height, DIFF);
  BOOST_CHECK_CLOSE(testCompShape.getFrameRect().width, frameRectBeforeMoving.width, DIFF);
  BOOST_CHECK_CLOSE(testCompShape.getArea(), AreaBeforeMoving, DIFF);

  testCompShape.move(5.0, 1.0);
  BOOST_CHECK_CLOSE(testCompShape.getFrameRect().height, frameRectBeforeMoving.height, DIFF);
  BOOST_CHECK_CLOSE(testCompShape.getFrameRect().width, frameRectBeforeMoving.width, DIFF);
  BOOST_CHECK_CLOSE(testCompShape.getArea(), AreaBeforeMoving, DIFF);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeScaleThrowExcept)
{
  rakhmatullin::Circle testCircle({0.0, 5.0}, 10.0);
  rakhmatullin::CompositeShape testCompShape;

  testCompShape.addShape(std::make_shared<rakhmatullin::Circle>(testCircle));

  const double incorrectScale = -3.0;
  BOOST_CHECK_THROW(testCompShape.scale(incorrectScale), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeScale)
{
  rakhmatullin::Circle testCircle({0.0, 5.0}, 10.0);
  rakhmatullin::CompositeShape testCompShape(std::make_shared<rakhmatullin::Circle>(testCircle));
  rakhmatullin::rectangle_t tempRect = testCompShape.getFrameRect();

  //test increase
  double scaleFactor = 2.0;
  double widthAfterTrueScale = tempRect.width * scaleFactor;
  double heightAfterTrueScale = tempRect.height * scaleFactor;

  testCompShape.scale(scaleFactor);

  tempRect = testCompShape.getFrameRect();
  BOOST_CHECK_CLOSE(tempRect.width, widthAfterTrueScale, DIFF);
  BOOST_CHECK_CLOSE(tempRect.height, heightAfterTrueScale, DIFF);

  //test decrease
  scaleFactor = 0.5;
  widthAfterTrueScale = tempRect.width * scaleFactor;
  heightAfterTrueScale = tempRect.height * scaleFactor;

  testCompShape.scale(scaleFactor);

  tempRect = testCompShape.getFrameRect();
  BOOST_CHECK_CLOSE(tempRect.width, widthAfterTrueScale, DIFF);
  BOOST_CHECK_CLOSE(tempRect.height, heightAfterTrueScale, DIFF);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeRotate)
{
  rakhmatullin::Circle testCircle({0.0, 5.0}, 10.0);
  rakhmatullin::Rectangle testRectangle({5.0, 5.0}, 5.0, 5.0);
  rakhmatullin::CompositeShape testCompShape(std::make_shared<rakhmatullin::Circle>(testCircle));
  testCompShape.addShape(std::make_shared<rakhmatullin::Rectangle>(testRectangle));

  const rakhmatullin::rectangle_t frameRectBeforeRotate = testCompShape.getFrameRect();
  const double areaBeforeRotate = testCompShape.getArea();
  const double angle = 90;

  testCompShape.rotate(angle);
  const rakhmatullin::rectangle_t frameRectAfterRotate = testCompShape.getFrameRect();
  const double areaAfterRotate = testCompShape.getArea();

  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, DIFF);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, DIFF);
  BOOST_CHECK_CLOSE(areaBeforeRotate, areaAfterRotate, DIFF);
}

BOOST_AUTO_TEST_SUITE_END()
