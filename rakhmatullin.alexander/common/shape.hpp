#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace rakhmatullin
{
  class Shape
  {
  public:
    using ptr = std::shared_ptr<Shape>;
    using array = std::unique_ptr<ptr []>;

    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t &) = 0;
    virtual void move(double, double) = 0;
    virtual void scale(double) = 0;
    virtual void rotate(double) = 0;
  };
}
#endif //SHAPE_HPP
