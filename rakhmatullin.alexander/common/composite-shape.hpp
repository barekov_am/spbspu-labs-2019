#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace rakhmatullin
{
  class CompositeShape :public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&) noexcept;
    CompositeShape(Shape::ptr);
    ~CompositeShape() = default;

    CompositeShape & operator =(const CompositeShape &);
    CompositeShape & operator =(CompositeShape &&) noexcept;
    Shape::ptr operator [](std::size_t) const;

    void addShape(Shape::ptr);
    void deleteShape(std::size_t);
    std::size_t getCountOfShape() const;
    void printInfo() const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(double, double) override;
    void scale(double) override;
    void rotate(double) override;
  private:
    std::size_t countOfShape_;
    std::size_t sizeOfArray_;
    Shape::array shapeArray_;
  };
} //namespace rakhmatullin

#endif //COMPOSITE_SHAPE_HPP
