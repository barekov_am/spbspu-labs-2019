#include <memory>
#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "matrix.hpp"

BOOST_AUTO_TEST_SUITE(testMatrixMethods)

BOOST_AUTO_TEST_CASE(testMatrixCopyConstructor)
{
  rakhmatullin::Matrix<int> testMatrix;
  for (std::size_t i = 0; i < 5; i++)
  {
    for (std::size_t j = 0; j < 5; j++)
    {
      testMatrix.add(i, std::make_shared<int>(i + j));
    }
  }
  rakhmatullin::Matrix<int> copyMatrix(testMatrix);
  BOOST_CHECK(testMatrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(testMatrixMoveConstructor)
{
  rakhmatullin::Matrix<int> testMatrix;
  for (std::size_t i = 0; i < 5; i++)
  {
    for (std::size_t j = 0; j < 5; j++)
    {
      testMatrix.add(i, std::make_shared<int>(i + j));
    }
  }
  rakhmatullin::Matrix<int> tempMatrix(testMatrix);
  rakhmatullin::Matrix<int> moveMatrix(std::move(testMatrix));

  BOOST_CHECK(rakhmatullin::Matrix<int>() == testMatrix);
  BOOST_CHECK(tempMatrix == moveMatrix);
}

BOOST_AUTO_TEST_CASE(testMatrixCopyOperator)
{
  rakhmatullin::Matrix<int> testMatrix;
  for (std::size_t i = 0; i < 5; i++)
  {
    for (std::size_t j = 0; j < 5; j++)
    {
      testMatrix.add(i, std::make_shared<int>(i + j));
    }
  }
  rakhmatullin::Matrix<int> copyMatrix = testMatrix;
  BOOST_CHECK(testMatrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(testMatrixMoveOperator)
{
  rakhmatullin::Matrix<int> testMatrix;
  for (std::size_t i = 0; i < 5; i++)
  {
    for (std::size_t j = 0; j < 5; j++)
    {
      testMatrix.add(i, std::make_shared<int>(i + j));
    }
  }
  rakhmatullin::Matrix<int> tempMatrix(testMatrix);
  rakhmatullin::Matrix<int> moveMatrix = std::move(testMatrix);

  BOOST_CHECK(rakhmatullin::Matrix<int>() == testMatrix);
  BOOST_CHECK(tempMatrix == moveMatrix);
}

BOOST_AUTO_TEST_CASE(testMatrixEqualOperator)
{
  rakhmatullin::Matrix<int> testMatrix;
  for (std::size_t i = 0; i < 5; i++)
  {
    for (std::size_t j = 0; j < 5; j++)
    {
      testMatrix.add(i, std::make_shared<int>(i + j));
    }
  }
  rakhmatullin::Matrix<int> equalMatrix(testMatrix);
  rakhmatullin::Matrix<int> unequalMatrix;
  unequalMatrix.add(0, std::make_shared<int>(-1));

  BOOST_CHECK_EQUAL(equalMatrix == testMatrix, true);
  BOOST_CHECK_EQUAL(unequalMatrix == testMatrix, false);
}

BOOST_AUTO_TEST_CASE(testMatrixUnequalOperator)
{
  rakhmatullin::Matrix<int> testMatrix;
  for (std::size_t i = 0; i < 5; i++)
  {
    for (std::size_t j = 0; j < 5; j++)
    {
      testMatrix.add(i, std::make_shared<int>(i + j));
    }
  }
  rakhmatullin::Matrix<int> equalMatrix(testMatrix);
  rakhmatullin::Matrix<int> unequalMatrix;
  unequalMatrix.add(0, std::make_shared<int>(-1));

  BOOST_CHECK_EQUAL(equalMatrix != testMatrix, false);
  BOOST_CHECK_EQUAL(unequalMatrix != testMatrix, true);
}

BOOST_AUTO_TEST_CASE(testMatrixExeption)
{
  rakhmatullin::Matrix<int> testMatrix;
  for (std::size_t i = 0; i < 5; i++)
  {
    for (std::size_t j = 0; j < 5; j++)
    {
      testMatrix.add(i, std::make_shared<int>(i + j));
    }
  }
  BOOST_CHECK_THROW(testMatrix[10][10], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
