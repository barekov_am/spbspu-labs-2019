
#include <exception>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "data_struct.hpp"

DataStruct getDataStructFromLine(std::string & line);
bool compareDataStruct(const DataStruct & lhs, const DataStruct & rhs);

int main(int , char *  [])
{
  try
  {
    std::vector< DataStruct > vector;
    std::string line;

    while (getline(std::cin, line))
    {
      if (std::cin.fail())
      {
        std::cerr << "Reading failed" << '\n';
        return 1;
      }

      DataStruct dataStruct = getDataStructFromLine(line);
      vector.push_back(dataStruct);
    }

    if (!vector.empty())
    {
      std::sort(vector.begin(), vector.end(), compareDataStruct);
    }

    for (auto & i : vector)
    {
      std::cout << i.key1 << ", " << i.key2 << ", " << i.str << '\n';
    }
  }
  catch (std::exception & err)
  {
    std::cerr << err.what() << '\n';
    return 1;
  }
  return 0;
}
