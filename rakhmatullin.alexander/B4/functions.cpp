
#include <string>
#include <stdexcept>
#include "data_struct.hpp"

const int MIN_RANGE_VALUE = -5;
const int MAX_RANGE_VALUE = 5;

DataStruct getDataStructFromLine(std::string & line)
{
  if (line.empty())
  {
    throw std::invalid_argument("Empty input.");
  }
  int key1;
  int key2;

  std::size_t pos = line.find_first_of(',');
  key1 = std::stoi(line.substr(0, pos));

  line.erase(0, pos + 1);
  while (line.find_first_of(" \t") == 0)
  {
    line.erase(0, 1);
  }

  pos = line.find_first_of(',');
  if (pos == std::string::npos)
  {
    throw std::invalid_argument("Invalid input");
  }
  key2 = std::stoi(line.substr(0, pos));

  line.erase(0, pos + 1);
  while (line.find_first_of(" \t") == 0)
  {
    line.erase(0, 1);
  }

  if ((key1 < MIN_RANGE_VALUE) || (key1 > MAX_RANGE_VALUE) || (key2 < MIN_RANGE_VALUE) || (key2 > MAX_RANGE_VALUE) || line.empty())
  {
    throw std::invalid_argument("Invalid input");
  }

  return {key1, key2, line};
}

bool compareDataStruct(const DataStruct & lhs, const DataStruct & rhs)
{
  if (lhs.key1 < rhs.key1)
  {
    return true;
  }

  if (lhs.key1 == rhs.key1)
  {
    if (lhs.key2 < rhs.key2)
    {
      return true;
    }

    if (lhs.key2 == rhs.key2)
    {
      if (lhs.str.length() < rhs.str.length())
      {
        return true;
      }
    }
  }

  return false;
}
