#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <memory>
#include <string>

#include "shape.hpp"

int checkNumber(const std::string & numberString);
std::shared_ptr<Shape> parse(std::string & line);

#endif //FUNCTIONS_HPP
