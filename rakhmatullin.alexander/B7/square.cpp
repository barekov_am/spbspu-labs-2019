#include "square.hpp"


Square::Square(const Point & center) :
  Shape(center)
{ }

void Square::draw(std::ostream & out) const
{
  out << "SQUARE (" << getCenter().x << ';' << getCenter().y << ")\n";
}
