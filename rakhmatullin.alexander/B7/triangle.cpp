#include "triangle.hpp"

Triangle::Triangle(const Point & center) :
  Shape(center)
{ }

void Triangle::draw(std::ostream & out) const
{
  out << "TRIANGLE (" << getCenter().x << ';' << getCenter().y << ")\n";
}
