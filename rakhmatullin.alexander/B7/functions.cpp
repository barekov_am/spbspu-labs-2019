#include "functions.hpp"

#include "circle.hpp"
#include "triangle.hpp"
#include "square.hpp"

int checkNumber(const std::string & numberString)
{
  if (numberString.empty())
  {
    throw std::invalid_argument("Invalid number input");
  }

  char * end = nullptr;
  const int number = std::strtoll(numberString.c_str(), & end, 10);

  if (*end != '\0')
  {
    throw std::invalid_argument("Invalid number input");
  }

  return number;
}

std::shared_ptr< Shape > parse(std::string & line)
{
  std::size_t pos = line.find_first_of('(');
  if (pos == std::string::npos)
  {
    throw std::invalid_argument("Invalid point input");
  }

  std::string nameShape = line.substr(0, pos);
  line.erase(0, pos + 1);

  pos = line.find_first_of(';');
  if (pos == std::string::npos)
  {
    throw std::invalid_argument("Invalid point input");
  }

  int x = checkNumber(line.substr(0, pos));

  line.erase(0, pos + 1);

  pos = line.find_first_of(')');
  if (pos == std::string::npos)
  {
    throw std::invalid_argument("Invalid point input");
  }

  int y = checkNumber(line.substr(0, pos));

  Point point = {x, y};

  if (nameShape == "CIRCLE")
  {
    return std::make_shared<Circle>(Circle(point));
  }
  else if (nameShape == "TRIANGLE")
  {
    return std::make_shared<Triangle>(Triangle(point));
  }
  else if (nameShape == "SQUARE")
  {
    return std::make_shared<Square>(Square(point));
  }
  else
  {
    throw std::invalid_argument("Invalid name of shape");
  }
}
