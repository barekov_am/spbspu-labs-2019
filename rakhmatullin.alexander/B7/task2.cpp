#include <vector>
#include <string>
#include <memory>
#include <iostream>
#include <algorithm>

#include "shape.hpp"
#include "functions.hpp"

using shape_ptr = std::shared_ptr<Shape>;

void task2()
{
  std::string line;
  std::vector<shape_ptr > shapes;

  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading failed");
    }

    line.erase(std::remove_if(line.begin(), line.end(), [](char ch) { return std::isspace(ch); }), line.end());

    if (line.empty())
    {
      continue;
    }

    shapes.push_back(parse(line));
  }

  std::cout << "Original:\n";
  std::for_each(shapes.begin(), shapes.end(), [](const shape_ptr & shape) { shape -> draw(std::cout); });

  std::cout << "Left-Right:\n";
  std::sort(shapes.begin(), shapes.end(), [](const shape_ptr & lhs, const shape_ptr & rhs)
  {
      return lhs -> isMoreLeft(*rhs);
  });
  std::for_each(shapes.begin(), shapes.end(), [](const shape_ptr & shape) { shape -> draw(std::cout); });

  std::cout << "Right-Left:\n";
  std::sort(shapes.begin(), shapes.end(), [](const shape_ptr & lhs, const shape_ptr & rhs)
  {
      return !(lhs -> isMoreLeft(*rhs));
  });
  std::for_each(shapes.begin(), shapes.end(), [](const shape_ptr & shape) { shape -> draw(std::cout); });

  std::cout << "Top-Bottom:\n";
  std::sort(shapes.begin(), shapes.end(), [](const shape_ptr & lhs, const shape_ptr & rhs)
  {
      return lhs -> isUpper(*rhs);
  });
  std::for_each(shapes.begin(), shapes.end(), [](const shape_ptr & shape) { shape -> draw(std::cout); });


  std::cout << "Bottom-Top:\n";
  std::sort(shapes.begin(), shapes.end(), [](const shape_ptr & lhs, const shape_ptr & rhs)
  {
      return !(lhs -> isUpper(*rhs));
  });
  std::for_each(shapes.begin(), shapes.end(), [](const shape_ptr & shape) { shape -> draw(std::cout); });
}

