#include "shape.hpp"

Shape::Shape(const Point & center) :
  center_(center)
{ }

Point Shape::getCenter() const
{
  return center_;
}

bool Shape::isMoreLeft(const Shape & otherShape) const
{
  return center_.x < otherShape.center_.x;
}

bool Shape::isUpper(const Shape & otherShape) const
{
  return center_.y > otherShape.center_.y;
}

