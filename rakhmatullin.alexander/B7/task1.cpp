#include <iostream>
#include <iterator>
#include <algorithm>
#include <functional>
#include <cmath>

void task1()
{
  std::transform(std::istream_iterator<double>(std::cin), std::istream_iterator<double>(), std::ostream_iterator<double>(std::cout, " "),
      std::bind(std::multiplies<double>(), std::placeholders::_1, M_PI));

  if (!std::cin.eof()) {
    throw std::ios_base::failure("Reading failed");
  }

  std::cout << '\n';
}
