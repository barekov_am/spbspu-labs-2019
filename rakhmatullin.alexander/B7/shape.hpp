#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>

struct Point
{
  int x, y;
};

class Shape
{
public:
    Shape(const Point & center);
    virtual ~Shape() = default;

    Point getCenter() const;
    bool isMoreLeft(const Shape & otherShape) const;
    bool isUpper(const Shape & otherShape) const;
    virtual void draw(std::ostream & out) const = 0;

private:
  Point center_;
};

#endif //SHAPE_HPP
