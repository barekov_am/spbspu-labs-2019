#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

zybkin::Rectangle::Rectangle(const zybkin::point_t &position, double width, double height) :
  height_(height),
  width_(width),
  angle_(0),
  center_(position)
{
  if (height_ <= 0)
  {
    throw std::invalid_argument("Height has to be positive");
  }
  if (width_ <= 0)
  {
    throw std::invalid_argument("Width has to be positive");
  }
}

zybkin::Rectangle::Rectangle(const zybkin::point_t &position, double width, double height, double angle) :
  Rectangle(position, width, height)
{
  rotate(angle);
}

double zybkin::Rectangle::getArea() const
{
  return (width_ * height_);
}

zybkin::rectangle_t zybkin::Rectangle::getFrameRect() const
{
  const double sine = std::sin(angle_ * M_PI / 180);
  const double cosine = std::cos(angle_ * M_PI / 180);
  const double height = width_ * std::abs(sine) + height_ * std::abs(cosine);
  const double width = width_ * std::abs(cosine) + height_ * std::abs(sine);
  return {width, height, center_};
}

void zybkin::Rectangle::printInfo() const
{
  std::cout << "Information about rectangle:\n";
  std::cout << "Width: " << width_ << ",\n";
  std::cout << "Height: " << height_ << ",\n";
  std::cout << "Area of rectangle: " << getArea() << ",\n";
  std::cout << "Center: (" << center_.x << "," << center_.y << ").\n";
  zybkin::rectangle_t frameRectangle = getFrameRect();
  std::cout << "width: " << frameRectangle.width << "\n";
  std::cout << "height: " << frameRectangle.height << "\n";
  std::cout << "Center: (" << frameRectangle.pos.x << "," << frameRectangle.pos.y << ").\n\n";
}

void zybkin::Rectangle::move(const zybkin::point_t &position)
{
  center_ = position;
}

void zybkin::Rectangle::move(double shift_x, double shift_y)
{
  center_.x += shift_x;
  center_.y += shift_y;
}

void zybkin::Rectangle::scale(double coef)
{
  if (coef <= 0)
  {
    throw std::invalid_argument("Coef has to be positive");
  }
  width_ *= coef;
  height_ *= coef;
}

void zybkin::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
