#include <memory>
#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "matrix.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

const double FAULT = 0.0001;

BOOST_AUTO_TEST_SUITE(matrixMethodsTesting)

BOOST_AUTO_TEST_CASE(testMatrixCopyConstructor)
{
  zybkin::Matrix<double> testMatrix;
  const std::size_t rows = 3;
  const std::size_t columns = 5;

  for (std::size_t i = 0; i < rows; ++i)
  {
    for (std::size_t j = 0; j < columns; ++j)
    {
      testMatrix.add(std::make_shared<double>((i + j) / 3.0), i);
    }
  }

  //check work of copy constructor
  zybkin::Matrix<double> copyMatrix(testMatrix);
  for (std::size_t i = 0; i < rows; ++i)
  {
    for (std::size_t j = 0; j < columns; ++j)
    {
      BOOST_CHECK_CLOSE(*copyMatrix[i][j], (i + j) / 3.0, FAULT);
    }
  }
}

BOOST_AUTO_TEST_CASE(testMatrixCopyOperator)
{
  zybkin::Matrix<double> testMatrix;
  const std::size_t rows = 3;
  const std::size_t columns = 5;

  for (std::size_t i = 0; i < rows; ++i)
  {
    for (std::size_t j = 0; j < columns; ++j)
    {
      testMatrix.add(std::make_shared<double>((i * j) / 5.0), i);
    }
  }

  zybkin::Matrix<double> copyMatrix;
  //check work of copy operator
  copyMatrix = testMatrix;

  for (std::size_t i = 0; i < rows; ++i)
  {
    for (std::size_t j = 0; j < columns; ++j)
    {
      BOOST_CHECK_CLOSE(*copyMatrix[i][j], (i * j) / 5.0, FAULT);
    }
  }
}

BOOST_AUTO_TEST_CASE(testMatrixMoveConstructor)
{
  zybkin::Matrix<double> testMatrix;
  const std::size_t rows = 3;
  const std::size_t columns = 5;

  for (std::size_t i = 0; i < rows; ++i)
  {
    for (std::size_t j = 0; j < columns; ++j)
    {
      testMatrix.add(std::make_shared<double>((i + j) / 3.0), i);
    }
  }

  //check work of move constructor
  zybkin::Matrix<double> moveMatrix(std::move(testMatrix));
  for (std::size_t i = 0; i < rows; ++i)
  {
    for (std::size_t j = 0; j < columns; ++j)
    {
      BOOST_CHECK_CLOSE(*moveMatrix[i][j], (i + j) / 3.0, FAULT);
    }
  }
}

BOOST_AUTO_TEST_CASE(testMatrixMoveOperator)
{
  zybkin::Matrix<double> testMatrix;
  const std::size_t rows = 3;
  const std::size_t columns = 5;

  for (std::size_t i = 0; i < rows; ++i)
  {
    for (std::size_t j = 0; j < columns; ++j)
    {
      testMatrix.add(std::make_shared<double>((i * j) / 5.0), i);
    }
  }

  zybkin::Matrix<double> moveMatrix;
  //check work of move operator
  moveMatrix = std::move(testMatrix);

  for (std::size_t i = 0; i < rows; ++i)
  {
    for (std::size_t j = 0; j < columns; ++j)
    {
      BOOST_CHECK_CLOSE(*moveMatrix[i][j], (i * j) / 5.0, FAULT);
    }
  }
}

BOOST_AUTO_TEST_CASE(testMatrixEqualOperator)
{
  zybkin::Matrix<double> testMatrix;
  const std::size_t rows = 3;
  const std::size_t columns = 5;

  for (std::size_t i = 0; i < rows; ++i)
  {
    for (std::size_t j = 0; j < columns; ++j)
    {
      testMatrix.add(std::make_shared<double>((i + j) / 3.0), i);
    }
  }

  //create the same matrix to check correct work of equal operator
  zybkin::Matrix<double> testEqualMatrix(testMatrix);
  //create matrix which have another data to check correct work of equal operator as well
  zybkin::Matrix<double> testUnequalMatrix;
  testUnequalMatrix.add(std::make_shared<double>(2.1), 0);

  BOOST_CHECK_EQUAL(testEqualMatrix == testMatrix, true);
  BOOST_CHECK_EQUAL(testUnequalMatrix == testMatrix, false);
}

BOOST_AUTO_TEST_CASE(testMatrixUnequalOperator)
{
  zybkin::Matrix<double> testMatrix;
  const std::size_t rows = 3;
  const std::size_t columns = 5;

  for (std::size_t i = 0; i < rows; ++i)
  {
    for (std::size_t j = 0; j < columns; ++j)
    {
      testMatrix.add(std::make_shared<double>((i + j) / 3.0), i);
    }
  }

  //create the same matrix to check correct work of unequal operator
  zybkin::Matrix<double> testEqualMatrix(testMatrix);
  //create matrix which have another data to check correct work of unequal operator as well
  zybkin::Matrix<double> testUnequalMatrix;
  testUnequalMatrix.add(std::make_shared<double>(2.1), 0);

  BOOST_CHECK_EQUAL(testEqualMatrix != testMatrix, false);
  BOOST_CHECK_EQUAL(testUnequalMatrix != testMatrix, true);
}

BOOST_AUTO_TEST_CASE(testMatrixGetMethodException)
{
  zybkin::Matrix<double> testMatrix;
  const std::size_t rows = 3;
  const std::size_t columns = 5;

  for (std::size_t i = 0; i < rows; ++i)
  {
    for (std::size_t j = 0; j < columns; ++j)
    {
      testMatrix.add(std::make_shared<double>((i + j) / 3.0), i);
    }
  }

  //check that operator [] throw exception in class Matrix
  BOOST_CHECK_THROW(testMatrix[7][3], std::out_of_range);
  //check that operator [] throw exception in subclass of Matrix - Array
  BOOST_CHECK_THROW(testMatrix[3][8], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
