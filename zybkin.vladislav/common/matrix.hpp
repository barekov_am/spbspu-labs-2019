#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>

namespace zybkin
{
  template <typename Type>
  class Matrix
  {
  public:
    using ptr = std::shared_ptr<Type>;

    class Array
    {
    public:
      Array(const Array &);
      Array(Array &&) noexcept;
      Array(ptr *, std::size_t);
      ~Array() = default;

      Array &operator =(const Array &);
      Array &operator =(Array &&) noexcept;
      ptr operator [](std::size_t) const;

      std::size_t size() const;
      void swap(Array &) noexcept;
    private:
      std::size_t size_;
      std::unique_ptr<ptr []> array_;
    };

    Matrix();
    Matrix(const Matrix<Type> &);
    Matrix(Matrix<Type> &&) noexcept;
    ~Matrix() = default;

    Matrix<Type> &operator =(const Matrix<Type> &);
    Matrix<Type> &operator =(Matrix<Type> &&) noexcept;

    Array operator [](std::size_t) const;
    bool operator ==(const Matrix<Type> &) const;
    bool operator !=(const Matrix<Type> &) const;

    std::size_t getRows() const;

    void add(ptr, std::size_t);
    void swap(Matrix<Type> &) noexcept;

  private:
    std::size_t rows_;
    std::size_t count_;
    std::unique_ptr<std::size_t []> columns_;
    std::unique_ptr<ptr []> data_;
  };

  template <typename Type>
  Matrix<Type>::Matrix() :
    rows_(0),
    count_(0)
  {
  }

  template <typename Type>
  Matrix<Type>::Matrix(const Matrix<Type> &copyMatrix) :
    rows_(copyMatrix.rows_),
    count_(copyMatrix.count_),
    columns_(std::make_unique<std::size_t []>(copyMatrix.rows_)),
    data_(std::make_unique<ptr []>(copyMatrix.count_))
  {
    for (std::size_t i = 0; i < rows_; ++i)
    {
      columns_[i] = copyMatrix.columns_[i];
    }

    for (std::size_t i = 0; i < count_; ++i)
    {
      data_[i] = copyMatrix.data_[i];
    }
  }

  template <typename Type>
  Matrix<Type>::Matrix(Matrix<Type> &&movedMatrix) noexcept :
    rows_(movedMatrix.rows_),
    count_(movedMatrix.count_),
    columns_(std::move(movedMatrix.columns_)),
    data_(std::move(movedMatrix.data_))
  {
    movedMatrix.rows_ = 0;
    movedMatrix.count_ = 0;
  }

  template <typename Type>
  Matrix<Type> &Matrix<Type>::operator =(const Matrix<Type> &copyMatrix)
  {
    if (this != &copyMatrix)
    {
      Matrix<Type>(copyMatrix).swap(*this);
    }

    return *this;
  }

  template <typename Type>
  Matrix<Type> &Matrix<Type>::operator =(Matrix<Type> &&movedMatrix) noexcept
  {
    if (this != &movedMatrix)
    {
      rows_ = movedMatrix.rows_;
      count_ = movedMatrix.count_;
      columns_ = std::move(movedMatrix.columns_);
      data_ = std::move(movedMatrix.data_);

      movedMatrix.rows_ = 0;
      movedMatrix.count_ = 0;
    }

    return *this;
  }

  template <typename Type>
  typename Matrix<Type>::Array Matrix<Type>::operator [](std::size_t row) const
  {
    if (row >= rows_)
    {
      throw std::out_of_range("Index out of range!");
    }

    std::size_t startIndex = 0;
    //calculate start index of row
    for (std::size_t i = 0; i < row; ++i)
    {
      startIndex += columns_[i];
    }
    return Matrix<Type>::Array(&data_[startIndex], columns_[row]);
  }

  template <typename Type>
  bool Matrix<Type>::operator ==(const Matrix<Type> &matrix) const
  {
    //check that matrixes have equal dimencions
    if ((rows_ != matrix.rows_) || (count_ != matrix.count_))
    {
      return false;
    }

    //check that matrixes have equal size of each rows
    for (std::size_t i = 0; i < rows_; i++)
    {
      if (columns_[i] != matrix.columns_[i])
      {
        return false;
      }
    }

    //check that matrixes have the same data
    for (std::size_t i = 0; i < count_; ++i)
    {
      if (data_[i] != matrix.data_[i])
      {
        return false;
      }
    }

    return true;
  }

  template <typename Type>
  bool Matrix<Type>::operator !=(const Matrix<Type> &matrix) const
  {
    return !(*this == matrix);
  }

  template <typename Type>
  std::size_t Matrix<Type>::getRows() const
  {
    return rows_;
  }

  template <typename Type>
  void Matrix<Type>::add(ptr type, std::size_t row)
  {
    if (row > rows_)
    {
      throw std::out_of_range("Index out of range");
    }

    if (!type)
    {
      throw std::invalid_argument("Pointer have to be not equal nullptr");
    }

    std::unique_ptr<ptr []> newData = std::make_unique<ptr []>(count_ + 1);
    //index of array which will have new element
    std::size_t newIndex = 0;
    //index of array which we will copy into new array
    std::size_t oldIndex = 0;
    for (std::size_t i = 0; i < rows_; ++i)
    {
      for (std::size_t j = 0; j < columns_[i]; ++j)
      {
        newData[newIndex++] = data_[oldIndex++];
      }
      if (row == i)
      {
        newData[newIndex++] = type;
        ++columns_[row];
      }
    }

    //if we need to create new row
    if (row == rows_)
    {
      std::unique_ptr<std::size_t []> newColumns = std::make_unique<std::size_t []>(rows_ + 1);
      for (std::size_t i = 0; i < rows_; ++i)
      {
        newColumns[i] = columns_[i];
      }
      newColumns[row] = 1;
      newData[count_] = type;
      ++rows_;
      columns_.swap(newColumns);
    }

    data_.swap(newData);
    ++count_;
  }

  template <typename Type>
  void Matrix<Type>::swap(Matrix<Type> &swappingMatrix) noexcept
  {
    std::swap(rows_, swappingMatrix.rows_);
    std::swap(count_, swappingMatrix.count_);
    std::swap(columns_, swappingMatrix.columns_);
    std::swap(data_, swappingMatrix.data_);
  }

  template <typename Type>
  Matrix<Type>::Array::Array(const Matrix<Type>::Array &copyArray):
    size_(copyArray.size_),
    array_(std::make_unique<ptr []>(copyArray.size_))
  {
    for (std::size_t i = 0; i < size_; ++i)
    {
      array_[i] = copyArray.array_[i];
    }
  }

  template <typename Type>
  Matrix<Type>::Array::Array(Matrix<Type>::Array &&movedArray) noexcept :
    size_(movedArray.size_),
    array_(std::move(movedArray.array_))
  {
    movedArray.size_ = 0;
  }

  template <typename Type>
  Matrix<Type>::Array::Array(ptr * array, std::size_t size):
    size_(size),
    array_(std::make_unique<ptr []>(size))
  {
    for (std::size_t i = 0; i < size_; ++i)
    {
      array_[i] = array[i];
    }
  }

  template <typename Type>
  typename Matrix<Type>::Array &Matrix<Type>::Array::operator =(const Matrix<Type>::Array &copyArray)
  {
    if (this != &copyArray)
    {
      Matrix<Type>::Array(copyArray).swap(*this);
    }

    return *this;
  }

  template <typename Type>
  typename Matrix<Type>::Array &Matrix<Type>::Array::operator =(Matrix<Type>::Array &&movedArray) noexcept
  {
    if (this != &movedArray)
    {
      size_ = movedArray.size_;
      movedArray.size_ = 0;
      array_ = std::move(movedArray.array_);
    }
  }

  template <typename Type>
  typename Matrix<Type>::ptr Matrix<Type>::Array::operator [](std::size_t index) const
  {
    if (index >= size_)
    {
      throw std::out_of_range("Index have to be less than size");
    }

    return array_[index];
  }

  template <typename Type>
  std::size_t Matrix<Type>::Array::size() const
  {
    return size_;
  }

  template <typename Type>
  void Matrix<Type>::Array::swap(Array &swappintArray) noexcept
  {
    std::swap(size_, swappintArray.size_);
    std::swap(array_, swappintArray.array_);
  }
}

#endif // MATRIX_HPP
