#include <memory>

#include <boost/test/auto_unit_test.hpp>

#include "matrix.hpp"
#include "partition.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testPartitionMethods)

BOOST_AUTO_TEST_CASE(testPartition)
{
  const zybkin::Circle circle1({0.0, 0.0}, 3.0);
  const zybkin::Circle circle2({5.0, 5.0}, 1.0);
  const zybkin::Rectangle rect1({-2.0, 1.0}, 4.0, 4.0);
  const zybkin::Rectangle rect2({-5.0, 4.0}, 3.0, 6.0);

  std::shared_ptr<zybkin::Shape> circlePointer = std::make_shared<zybkin::Circle>(circle1);
  std::shared_ptr<zybkin::Shape> circlePointer2 = std::make_shared<zybkin::Circle>(circle2);
  std::shared_ptr<zybkin::Shape> rectPointer1 = std::make_shared<zybkin::Rectangle>(rect1);
  std::shared_ptr<zybkin::Shape> rectPointer2 = std::make_shared<zybkin::Rectangle>(rect2);

  zybkin::CompositeShape compositeShape(circlePointer);
  compositeShape.add(rectPointer1);
  compositeShape.add(circlePointer2);
  compositeShape.add(rectPointer2);

  zybkin::Matrix<zybkin::Shape> matrix = zybkin::part(compositeShape);

  BOOST_CHECK(matrix[0][0] == circlePointer);
  BOOST_CHECK(matrix[0][1] == circlePointer2);
  BOOST_CHECK(matrix[1][0] == rectPointer1);
  BOOST_CHECK(matrix[2][0] == rectPointer2);

  //check that we have different size of rows
  BOOST_CHECK_EQUAL(matrix.getRows(), 3);
  BOOST_CHECK_EQUAL(matrix[0].size(), 2);
  BOOST_CHECK_EQUAL(matrix[1].size(), 1);
  BOOST_CHECK_EQUAL(matrix[2].size(), 1);

  const zybkin::Rectangle rect3({6.0, 6.0}, 2.0, 2.0);
  std::shared_ptr<zybkin::Shape> rectPointer3 = std::make_shared<zybkin::Rectangle>(rect3);

  compositeShape.add(rectPointer3);
  matrix = zybkin::part(compositeShape);

  BOOST_CHECK(matrix[1][1] == rectPointer3);
  //check that we have different size of rows once again(after adding of new shape)
  BOOST_CHECK_EQUAL(matrix.getRows(), 3);
  BOOST_CHECK_EQUAL(matrix[0].size(), 2);
  BOOST_CHECK_EQUAL(matrix[1].size(), 2);
  BOOST_CHECK_EQUAL(matrix[2].size(), 1);
}

BOOST_AUTO_TEST_CASE(testIntersect)
{
  const zybkin::Rectangle rect({0.0, 0.0}, 6.0, 5.0);

  const zybkin::Rectangle rect1({3.0, 3.0}, 4.0, 4.0);
  const zybkin::Rectangle rect2({-3.0, 2.0}, 4.0, 2.0);
  const zybkin::Rectangle rect3({-3.0, -2.0}, 2.0, 2.0);
  const zybkin::Rectangle rect4({3.0, -2.0}, 2.0, 2.0);
  const zybkin::Circle circle({30.0, -10.0}, 4.0);

  //check that frame rectangles intersect from different sides
  BOOST_CHECK(zybkin::intersect(rect.getFrameRect(), rect1.getFrameRect()));
  BOOST_CHECK(zybkin::intersect(rect.getFrameRect(), rect2.getFrameRect()));
  BOOST_CHECK(zybkin::intersect(rect.getFrameRect(), rect3.getFrameRect()));
  BOOST_CHECK(zybkin::intersect(rect.getFrameRect(), rect4.getFrameRect()));

  //check that frame rectangles don't intersect
  BOOST_CHECK(!zybkin::intersect(rect.getFrameRect(), circle.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
