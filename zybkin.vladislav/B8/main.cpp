#include <cstring>
#include "functions.hpp"

const int DEFUALT_WIDTH_PARAMETRS_AMOUNT = 1;
const int WITH_USER_SET_PARAMETRS_AMOUNT = 3;
const int MIN_LINE_WIDTH = 25;
const int DEFAULT_WIDTH = 40;

int main(int argc, char *argv[])
{
  try
  {
    if ((argc != DEFUALT_WIDTH_PARAMETRS_AMOUNT) && (argc != WITH_USER_SET_PARAMETRS_AMOUNT))
    {
      throw std::invalid_argument("Invalid count of arguments sent to program!\n");
    }

    int lineWidth = DEFAULT_WIDTH;

    if (argc == WITH_USER_SET_PARAMETRS_AMOUNT)
    {
      if (strcmp(argv[1], "--line-width") != 0)
      {
        throw std::invalid_argument("The second argumant is incorrect! It have to be --line-width!\n");
      }

      char* checkPtr = nullptr;
      lineWidth = std::strtoll(argv[2], &checkPtr, 10);
      if (*checkPtr != '\x00')
      {
         throw std::invalid_argument("The width of the text's line have to be more!\n");
      }

      if (lineWidth < MIN_LINE_WIDTH)
      {
        throw std::invalid_argument("Invalid Line Width!\n");
      }
    }
    task(std::cin, lineWidth);
  }
  catch (std::exception &ex)
  {
    std::cout << ex.what();
    return 1;
  }

  return 0;
}
