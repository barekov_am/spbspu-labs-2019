#include "tokens.hpp"

Token::Token(const std::string &token, int type) :
  token_(token),
  type_(type)
{
}

std::size_t Token::getLength() const
{
  return token_.length();
}

std::string Token::getToken()
{
  return token_;
}

int Token::getType() const
{
  return type_;
}
