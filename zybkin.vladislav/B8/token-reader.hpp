#ifndef TOKEN_READER_HPP
#define TOKEN_READER_HPP
#include <iostream>
#include <list>
#include <locale>
#include "tokens.hpp"

class TokenReader
{
public:
  using TokenList = std::list<Token>;

  TokenReader(std::istream &inputStrem);

  void read();
  TokenList getTokens();
private:
  std::istream &inputStrem_;
  TokenList tokens_;
  int previousToken_;
  std::locale locale_;
  char delimiter_;

  void readWord();
  void readPunct();
  void readNumber();
  void readHyphen();
  void pushToken(const std::string &token, int type);

  inline bool isNum(char current, char next)
  {
    return (std::isdigit(current, locale_) || (isOption(current) && std::isdigit(next, locale_)));
  }

  inline bool isOption(char current)
  {
    return (('+' == current) || ('-' == current));
  }

  inline bool isHyphen(char current)
  {
    return ('-' == current);
  }

  inline bool isDelimiter(char current)
  {
    return (delimiter_ == current);
  }
};

#endif
