#ifndef TEXT_FORMATTER_HPP
#define TEXT_FORMATTER_HPP
#include <list>
#include "tokens.hpp"

class TextFormatter
{
public:
  using TokenList = std::list<Token>;
  using LineList = std::list<std::string>;

  TextFormatter(TokenList tokens, int width);

  void format();
  LineList getText();

private:
  TokenList tokens_;
  int width_;
  LineList currentLine_;
  LineList formattedText_;
  int previousTokenType_;

  void ifFullLine();
  void ifPunctLast();
  void ifHyphenLast();
  void append(const std::string &token);
  void appendPunct(const std::string &token);
  void appendHyphen(const std::string &token);
  int getCurrentLineLength();
};

#endif //TEXT_FORMATTER_HPP
