#include "functions.hpp"

void print(TextFormatter::LineList text_)
{
  for (std::string nextLine : text_)
  {
    std::cout << nextLine << "\n";
  }
}

void task(std::istream &inputStream, int lineWidth)
{
  TokenReader tokenReader(inputStream);
  tokenReader.read();
  TextFormatter textFormatter(tokenReader.getTokens(), lineWidth);
  textFormatter.format();
  print(textFormatter.getText());
}

