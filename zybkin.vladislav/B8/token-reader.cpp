#include "token-reader.hpp"
#include <iostream>

TokenReader::TokenReader(std::istream &inputStrem) :
  inputStrem_(inputStrem),
  previousToken_(-1),
  locale_(inputStrem.getloc()),
  delimiter_(*localeconv()->decimal_point)
{
}

void TokenReader::pushToken(const std::string &token, int type)
{
  previousToken_ = type;
  Token newToken(token, type);
  tokens_.push_back(newToken);
}

void TokenReader::read()
{
  char current;
  char next;
  while (inputStrem_)
  {
    inputStrem_ >> std::ws;
    current = static_cast<char>(inputStrem_.get());
    next = static_cast<char>(inputStrem_.peek());
    inputStrem_.unget();
    if (isNum(current, next))
    {
          readNumber();
    }
    else if (std::ispunct(current, locale_))
    {
      readPunct();
    }
    else if (std::isalpha(current, locale_))
    {
      readWord();
    }
  }
}

TokenReader::TokenList TokenReader::getTokens()
{
  return tokens_;
}

void TokenReader::readHyphen()
{
  if ((previousToken_ == PUNCTUATION) && tokens_.back().getToken() != ",")
  {
    throw std::invalid_argument("Invalid sequence of text: the hyphen after other type of punctuation except , !\n");
  }

  if (previousToken_ == HYPHEN)
  {
    throw std::invalid_argument("Invalid sequence of text: two 3*hyphen in a row!\n");
  }

  std::string hyphen = "";
  char current = static_cast<char>(inputStrem_.peek());
  while (isHyphen(current))
  {
    inputStrem_.get();
    hyphen += current;
    current = static_cast<char>(inputStrem_.peek());
  }

  if (hyphen.length() != HYPHEN_LENGTH)
  {
    throw std::invalid_argument("The wrong number number of hyphen in a row!\n");
  }

  pushToken(hyphen, HYPHEN);
}

void TokenReader::readPunct()
{
  if (tokens_.empty())
  {
    throw std::invalid_argument("The first token have to be not a punctuation!\n");
  }

  if (isHyphen(inputStrem_.peek()))
  {
    readHyphen();
  }
  else {

    if ((previousToken_ == PUNCTUATION) || (previousToken_ == HYPHEN))
    {
      throw std::invalid_argument("Invalid format: two punctuations in a row!\n");
    }
    std::string punct;
    punct += static_cast<char>(inputStrem_.peek());
    pushToken(punct, PUNCTUATION);
  }
  inputStrem_.get();
}

void TokenReader::readWord()
{
  std::string newWord = "";
  char current = static_cast<char>(inputStrem_.peek());
  while (('-' == current) || std::isalpha(current, locale_))
  {
    inputStrem_.get();
    if (isHyphen(current) && isHyphen(inputStrem_.peek()))
    {
      inputStrem_.unget();
      throw std::invalid_argument("Invalid hyphen in row in the read word!\n");
    }

    newWord += current;
    current = static_cast<char>(inputStrem_.peek());
  }

  if (newWord.length() > MAX_WORD_LENGTH)
  {
    throw std::invalid_argument("The length of the word you tried to separate to token is more than 20!\n");
  }

  pushToken(newWord, WORD);
}

void TokenReader::readNumber()
{
  bool isDelimiterDetected = false;
  std::string newNumber;
  newNumber += static_cast<char>(inputStrem_.get());

  char current = static_cast<char>(inputStrem_.peek());
  while (std::isdigit(current, locale_) || isDelimiter(current))
  {
    inputStrem_.get();
    if (isDelimiter(current))
    {
      if (isDelimiterDetected)
      {
        inputStrem_.unget();
        break;
      }
      isDelimiterDetected = true;
    }
    newNumber += current;
    current = static_cast<char>(inputStrem_.peek());
  }

  if (newNumber.length() > MAX_NUMBER_LENGTH)
  {
    throw std::invalid_argument("The length of the number you tried to separate to token is more than 20!\n");
  }

  pushToken(newNumber, NUMBER);
}



