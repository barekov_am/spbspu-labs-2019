#include "text-formatter.hpp"
#include <stdexcept>
#include <iostream>

const int SPACE_LENGTH = 1;

TextFormatter::TextFormatter(TokenList tokens, int width) :
  tokens_(tokens),
  width_(width),
  previousTokenType_(-1)
{
}

void TextFormatter::format()
{
  for (Token nextToken : tokens_)
  {
    switch (nextToken.getType()) {
    case WORD:
    case NUMBER:
    {
      append(nextToken.getToken());
      break;
    }
    case PUNCTUATION:
    {
      appendPunct(nextToken.getToken());
      break;
    }
    case HYPHEN:
    {
      appendHyphen(nextToken.getToken());
      break;
    }
    default:
    {
      throw std::invalid_argument("Invalid type of token sent to format method!\n");
    }
    }
  }
  if (!currentLine_.empty())
  {
    ifFullLine();
  }
}

TextFormatter::LineList TextFormatter::getText()
{
  return formattedText_;
}

void TextFormatter::ifFullLine()
{
  std::string fullLine = "";
  if (" " == currentLine_.back())
  {
    currentLine_.pop_back();
  }
  for (std::string token : currentLine_)
  {
    fullLine += token;
  }
  formattedText_.push_back(fullLine);
  currentLine_.clear();
}

void TextFormatter::ifPunctLast()
{
  std::string lastAddedToken = currentLine_.back();
  currentLine_.pop_back();
  if (" " == lastAddedToken)
  {
    lastAddedToken = currentLine_.back();
    currentLine_.pop_back();
  }
  ifFullLine();
  currentLine_.push_back(lastAddedToken);
}

void TextFormatter::ifHyphenLast()
{
  LineList newLine;
  std::string last = currentLine_.back();
  if (" " == last)
  {
    if (previousTokenType_ == PUNCTUATION)
    {
      currentLine_.pop_back();
      newLine.push_front(",");
      currentLine_.pop_back();
    }
    newLine.push_back(currentLine_.back());
    currentLine_.pop_back();
  }
  else
  {
    newLine.push_back(currentLine_.back());
    currentLine_.pop_back();
  }
  ifFullLine();
  currentLine_ = newLine;
}

void TextFormatter::append(const std::string &token)
{
  bool check = ((previousTokenType_ == WORD) && !currentLine_.empty());
  int optional = check ? 1 : 0;
  const int unitedLength = token.length() + getCurrentLineLength() + optional;
  if (unitedLength > width_)
  {
    ifFullLine();
  }
  if (!currentLine_.empty() && check)
  {
    currentLine_.push_back(" ");
  }
  currentLine_.push_back(token);
  previousTokenType_ = WORD;
}

void TextFormatter::appendPunct(const std::string &token)
{
  const int unitedLength = token.length() + getCurrentLineLength();
  if (unitedLength > width_)
  {
    ifPunctLast();
  }

  currentLine_.push_back(token);
  if (unitedLength != width_)
  {
    currentLine_.push_back(" ");
  }
  previousTokenType_ = PUNCTUATION;
}

void TextFormatter::appendHyphen(const std::string &token)
{
  const int unitedLength = token.length() + getCurrentLineLength() + SPACE_LENGTH;
  if (unitedLength > width_)
  {
    ifHyphenLast();
  }
  else if (previousTokenType_ == PUNCTUATION)
  {
    currentLine_.pop_back();
  }
  currentLine_.push_back(" ");
  currentLine_.push_back(token);
  if (unitedLength != width_)
  {
    currentLine_.push_back(" ");
  }
  previousTokenType_ = HYPHEN;
}

int TextFormatter::getCurrentLineLength()
{
  int length = 0;
  for (std::string token : currentLine_)
  {
    length += token.length();
  }
  return length;
}

