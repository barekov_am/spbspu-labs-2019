#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP
#include <iostream>
#include "text-formatter.hpp"
#include "token-reader.hpp"

void task(std::istream & inputStream, int lineWidth);
void print(TextFormatter::LineList text_);

#endif //FUNCTIONS_HPP
