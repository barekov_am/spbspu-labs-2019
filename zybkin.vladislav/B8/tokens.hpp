#ifndef TOKENS_HPP
#define TOKENS_HPP
#include <string>

const int MAX_WORD_LENGTH = 20;
const int MAX_NUMBER_LENGTH = 20;
const int HYPHEN_LENGTH = 3;

const int WORD = 0;
const int PUNCTUATION = 1;
const int HYPHEN = 2;
const int NUMBER = 3;

class Token
{
public:
  Token(const std::string &token, int type);
  std::size_t getLength() const;
  std::string getToken();
  int getType() const;
private:
  std::string token_;
  int type_;
};

#endif //TOKENS_HPP
