#include "functions.hpp"
#include <cctype>
#include <stdexcept>
#include <algorithm>

void checkMarkName(std::string &markName)
{
  if (std::end(markName) != std::find_if(std::begin(markName), std::end(markName), [&](char c)
      { return !std::isalnum(c) && (c != '-'); }))
  {
    throw std::invalid_argument("<INVALID BOOKMARK>\n");
  }
}

bool isAfter(std::string &insertPosition)
{
  if (insertPosition == "after")
  {
    return true;
  }
  else if (insertPosition == "before")
  {
    return false;
  }

  throw std::invalid_argument("<INVALID COMMAND>\n");
}

void checkNumber(std::string &number)
{
  if (std::end(number) != std::find_if(std::begin(number), std::end(number), [&](char c)
      {return !std::isdigit(c); }) || number.empty())
  {
    throw std::invalid_argument("<INVALID COMMAND>\n");
  }
}

std::string &adaptName(std::string &name)
{
  name = name.substr(1, name.length() - 2);
  int size = name.length();
  for (int i = 0; i < size; ++i) {
    if (name[i] == '\\')
    {
      name = name.erase(i, 1);
    }
  }
  return name;
}
