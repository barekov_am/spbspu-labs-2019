#include "factorial-container.hpp"

const int MAX_SIZE = 11;
const int MIN_SIZE = 1;

FactorialContainer::FactorialIterator::FactorialIterator() :
  index_(1),
  value_(1)
{
}

FactorialContainer::FactorialIterator::FactorialIterator(std::size_t index)
{
  if (index < MIN_SIZE || index > MAX_SIZE)
  {
    throw std::invalid_argument("You've just sent to constuctor the wrong index!\n");
  }
  index_ = index;
  value_ = calculateFactorial(index);
}

bool FactorialContainer::FactorialIterator::operator ==(const FactorialContainer::FactorialIterator &rhs) const
{
  return ((index_ == rhs.index_) && (value_ == rhs.value_));
}

bool FactorialContainer::FactorialIterator::operator !=(const FactorialContainer::FactorialIterator &rhs) const
{
  return !(*this == rhs);
}

FactorialContainer::FactorialIterator &FactorialContainer::FactorialIterator::operator ++()
{
  if (index_ > MAX_SIZE)
  {
    throw std::out_of_range("You've just try to get next factorial which can't be stored!\n");
  }
  ++index_;
  value_ *= index_;

  return *this;
}

FactorialContainer::FactorialIterator FactorialContainer::FactorialIterator::operator ++(int)
{
  FactorialIterator tmp(*this);
  operator ++();
  return tmp;
}

FactorialContainer::FactorialIterator &FactorialContainer::FactorialIterator::operator --()
{
  if (index_ < MIN_SIZE)
  {
    throw std::out_of_range("You've just try to get next factorial which can't be stored!\n");
  }
  value_ /= index_;
  --index_;

  return *this;
}

FactorialContainer::FactorialIterator FactorialContainer::FactorialIterator::operator --(int)
{
  FactorialIterator tmp(*this);
  operator--();
  return tmp;
}

FactorialContainer::FactorialIterator::value_type FactorialContainer::FactorialIterator::operator *()
{
  return value_;
}

FactorialContainer::FactorialIterator::pointer FactorialContainer::FactorialIterator::operator ->()
{
  return &value_;
}

FactorialContainer::FactorialIterator::value_type FactorialContainer::FactorialIterator::calculateFactorial(std::size_t index) const
{
  return (index <= 1) ? 1 : (index * calculateFactorial(index - 1));
}

FactorialContainer::FactorialContainer() :
   begin_(FactorialIterator(MIN_SIZE)),
   end_(FactorialIterator(MAX_SIZE)),
   rbegin_(std::reverse_iterator<iterator>(FactorialIterator(MIN_SIZE))),
   rend_(std::reverse_iterator<iterator>(FactorialIterator(MAX_SIZE)))
{
}

FactorialContainer::FactorialIterator FactorialContainer::begin() const
{
  return begin_;
}

FactorialContainer::FactorialIterator FactorialContainer::end() const
{
  return end_;
}

std::reverse_iterator<FactorialContainer::iterator> FactorialContainer::rbegin() const
{
  return rbegin_;
}

std::reverse_iterator<FactorialContainer::iterator> FactorialContainer::rend() const
{
  return rend_;
}
