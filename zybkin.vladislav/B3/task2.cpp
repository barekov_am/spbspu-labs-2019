#include "tasks.hpp"
#include <iostream>
#include <iterator>
#include "factorial-container.hpp"

void task2()
{
  FactorialContainer factorials;
  std::ostream_iterator<unsigned long long> outputIter(std::cout, " ");

  std::copy(factorials.begin(), factorials.end(), outputIter);
  std::cout << "\n";

  std::copy(factorials.rend(), factorials.rbegin(), outputIter);
  std::cout << "\n";
}
