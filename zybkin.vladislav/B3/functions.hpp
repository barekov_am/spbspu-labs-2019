#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP
#include <string>

void checkMarkName(std::string &markName);
bool isAfter(std::string &insertPosition);
void checkNumber(std::string &number);
std::string &adaptName(std::string &name);

#endif // FUNCTIONS_HPP
