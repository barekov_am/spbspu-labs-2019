#include "marksmap.hpp"
#include <iostream>
#include <algorithm>

MarksMap::MarksMap()
{
  marksMap_["current"] = phonebook_.begin();
}

void MarksMap::add(const PhoneBook::note_t &note)
{
  phonebook_.insert(phonebook_.end(), note);

  if (phonebook_.begin() == std::prev(phonebook_.end()))
  {
    marksMap_["current"] = phonebook_.begin();
  }
}

void MarksMap::store(const std::string &mark, const std::string &newMark)
{
  mapIter it = getIter(mark);
  if (it == marksMap_.end())
  {
    return;
  }
  marksMap_.emplace(newMark, it->second);
}

void MarksMap::insert(bool after, const std::string &mark, const PhoneBook::note_t &note)
{
  mapIter markIt = getIter(mark);
  if (markIt == marksMap_.end())
  {
    return;
  }
  PhoneBook::iterator it = markIt->second;
  if (it == phonebook_.end())
  {
    add(note);
    return;
  }
  if (after)
  {
    it = std::next(it);
  }
  phonebook_.insert(it, note);
}

void MarksMap::remove(const std::string &mark)
{
  mapIter mapIt = getIter(mark);
  if (mapIt == marksMap_.end())
  {
    return;
  }
  if (phonebook_.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }
  PhoneBook::iterator it = mapIt->second;
  if (phonebook_.end() == it)
  {
    throw std::logic_error("You've just tried to delete element which aren't at the list");
  }
  std::for_each(marksMap_.begin(), marksMap_.end(), [&](auto &iter)
  {
    if (iter.second == it)
    {
      iter.second = (std::next(iter.second) == phonebook_.end()) ? phonebook_.prev(it) : phonebook_.next(it);
    }
  });
  it = phonebook_.remove(it);
}

void MarksMap::show(std::ostream &out, const std::string &mark)
{
  mapIter mapIt = getIter(mark);
  if (mapIt == marksMap_.end())
  {
    return;
  }
  if (phonebook_.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }
  PhoneBook::iterator it = mapIt->second;
  phonebook_.show(it, out);
}

void MarksMap::move(const std::string &mark, int steps)
{
  mapIter it = getIter(mark);
  if (it == marksMap_.end())
  {
    return;
  }
  it->second = phonebook_.move(it->second, steps);
}

void MarksMap::move(const std::string &mark, const std::string &position)
{
  mapIter it = getIter(mark);
  if (it == marksMap_.end())
  {
    return;
  }
  it->second = position == "first" ? phonebook_.begin() : --phonebook_.end();
}

std::map<std::string, PhoneBook::iterator>::iterator MarksMap::getIter(const std::string &mark)
{
  mapIter it = marksMap_.find(mark);
  if (it == marksMap_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
  }
  return it;
}
