#ifndef FACTORIALCONTAINER_HPP
#define FACTORIALCONTAINER_HPP
#include <iterator>

class FactorialContainer
{
public:
  class FactorialIterator :
      public std::iterator<std::bidirectional_iterator_tag, unsigned long long, int, const unsigned long long *, unsigned long long>
  {
  public:
    FactorialIterator();
    FactorialIterator(std::size_t index);

    bool operator ==(const FactorialIterator &rhs) const;
    bool operator !=(const FactorialIterator &rhs) const;

    FactorialIterator &operator ++();
    FactorialIterator operator ++(int);

    FactorialIterator &operator --();
    FactorialIterator operator --(int);

    value_type operator *();
    pointer operator ->();
  private:
    std::size_t index_;
    value_type value_;

    value_type calculateFactorial(std::size_t index) const;
  };
  using iterator = FactorialIterator;
  using const_iterator = FactorialIterator;

  FactorialContainer();

  FactorialIterator begin() const;
  FactorialIterator end() const;
  std::reverse_iterator<iterator> rbegin() const;
  std::reverse_iterator<iterator> rend() const;
private:
  FactorialIterator begin_;
  FactorialIterator end_;
  std::reverse_iterator<iterator> rbegin_;
  std::reverse_iterator<iterator> rend_;
};

#endif // FACTORIALCONTAINER_HPP
