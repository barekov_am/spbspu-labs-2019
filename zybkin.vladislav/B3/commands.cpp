#include "commands.hpp"
#include <sstream>
#include <algorithm>
#include <functional>
#include "functions.hpp"

void zybkin::add(MarksMap &map, std::stringstream &stream)
{
  std::string number;
  std::string name;
  stream >> number >> std::ws;
  std::getline(stream, name);
  if ((name[0] != '\"') || (name[name.length() - 1] != '\"') || name.empty()) {
    throw std::invalid_argument("<INVALID COMMAND>\n");
  }
  checkNumber(number);
  name = adaptName(name);
  PhoneBook::note_t note = {name, number};
  map.add(note);
}

void zybkin::store(MarksMap &map, std::stringstream &stream)
{
  std::string markName;
  std::string newMarkName;
  stream >> markName >> newMarkName;
  checkMarkName(markName);
  checkMarkName(newMarkName);

  map.store(markName, newMarkName);
}

void zybkin::insert(MarksMap &map, std::stringstream &stream)
{
  std::string insertPosition;
  std::string markName;
  std::string number;
  std::string name;
  stream >> insertPosition >> markName >> number >> std::ws;
  std::getline(stream, name);
  bool isafter;
  if ((name[0] != '\"') || (name[name.length() - 1] != '\"') || name.empty()) {
    throw std::invalid_argument("<INVALID COMMAND>\n");
  }
  isafter = isAfter(insertPosition);
  checkMarkName(markName);
  checkNumber(number);
  name = adaptName(name);
  PhoneBook::note_t note = {name, number};
  map.insert(isafter, markName, note);
}

void zybkin::remove(MarksMap &map, std::stringstream &stream)
{
  std::string markName;
  stream >> markName;
  checkMarkName(markName);
  map.remove(markName);
}

void zybkin::show(MarksMap &map, std::stringstream &stream)
{
  std::string markName;
  stream >> markName;
  checkMarkName(markName);
  map.show(std::cout, markName);
}

void zybkin::move(MarksMap &map, std::stringstream &stream)
{
  std::string markName;
  std::string steps;
  stream >> markName >> steps;
  checkMarkName(markName);

  if (steps == "first" || steps == "last")
  {
    map.move(markName, steps);
  }
  else
  {
    try
    {
      std::string substr = steps;
      if (steps[0] == '-' || steps[0] == '+')
      {
        substr = steps.substr(1, steps.length() - 1);
      }
      checkNumber(substr);
      map.move(markName, std::atoi(steps.c_str()));
    }
    catch (std::invalid_argument &e) {
      std::cout << "<INVALID STEP>\n";
    }
  }
}
