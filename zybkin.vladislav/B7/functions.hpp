#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP
#include <memory>
#include <list>
#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"

std::shared_ptr<Shape> parse(std::string &line);
std::shared_ptr<Shape> createShape(const std::string &typeOfShape, const point_t &center);
int checkNumber(std::string &number);
point_t getPoint(std::string &coordinates);
void printShapes(const std::list<std::shared_ptr<Shape>> &shapes);

#endif //FUNCTIONS_HPP
