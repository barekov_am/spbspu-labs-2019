#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <iostream>
#include "point.hpp"


class Shape
{
public:
  Shape(const point_t &center);
  virtual ~Shape() = default;

  point_t getCenter() const;
  bool isMoreLeft(const Shape &shape) const;
  bool isUpper(const Shape &shape) const;
  virtual void draw(std::ostream &out) const = 0;
private:
  point_t center_;
};

#endif //SHAPE_HPP
