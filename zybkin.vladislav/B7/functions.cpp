#include "functions.hpp"
#include <algorithm>

std::shared_ptr<Shape> createShape(std::string &typeOfShape, const point_t &center)
{
  if (typeOfShape == "CIRCLE")
  {
    return std::make_shared<Circle>(Circle(center));
  }
  if (typeOfShape == "SQUARE")
  {
    return std::make_shared<Square>(Square(center));
  }
  if (typeOfShape == "TRIANGLE")
  {
    return std::make_shared<Triangle>(Triangle(center));
  }

  throw std::invalid_argument("The type of shape is incorrect!\n");
}

std::shared_ptr<Shape> parse(std::string &line)
{
  std::size_t ePosition = line.find_first_of('E');
  if (ePosition == std::string::npos)
  {
    throw std::invalid_argument("The wrong format of shape!\n");
  }
  std::string typeOfShape = line.substr(0, ePosition + 1);
  std::size_t openBracket = line.find_first_of('(');
  std::size_t closeBracket = line.find_first_of(')');
  if ((openBracket == std::string::npos) || (closeBracket == std::string::npos))
  {
    throw std::invalid_argument("The wrong format of point!\n");
  }
  std::string pointString = line.substr(openBracket, closeBracket - openBracket + 1);
  point_t center = getPoint(pointString);
  return createShape(typeOfShape, center);
}

int checkNumber(std::string &number)
{
  number.erase(std::remove_if(std::begin(number), std::end(number), [&](char c)
  { return ((c == ' ') || (c == '\t')); }), std::end(number));
  char* checkedPtr = nullptr;
  const int num = std::strtoll(number.c_str(), &checkedPtr, 10);
  if (*checkedPtr != '\x00')
  {
    throw std::invalid_argument("The wrong format of number!\n");
    return 1;
  }

  return num;
}

point_t getPoint(std::string &coordinates)
{
  int colonPosition = coordinates.find_first_of(";");
  std::string strX = coordinates.substr(1, colonPosition - 1);
  std::string strY = coordinates.substr(colonPosition + 1, coordinates.length() - colonPosition - 2);

  return {checkNumber(strX), checkNumber(strY)};
}

void printShapes(const std::list<std::shared_ptr<Shape>> &shapes)
{
  std::for_each(std::begin(shapes), std::end(shapes), [&](const std::shared_ptr<Shape> &shape) {
    shape->draw(std::cout);
  });
}
