#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"


class Circle : public Shape
{
public:
  Circle(const point_t &center);
  virtual void draw(std::ostream &out) const;
};

#endif //CIRCLE_HPP
