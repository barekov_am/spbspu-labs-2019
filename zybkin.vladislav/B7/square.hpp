#ifndef SQUARE_HPP
#define SQUARE_HPP
#include "shape.hpp"


class Square : public Shape
{
public:
  Square(const point_t &center);
  virtual void draw(std::ostream &out) const;
};

#endif //SQUARE_HPP
