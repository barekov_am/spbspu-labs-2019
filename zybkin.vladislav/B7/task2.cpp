#include "tasks.hpp"
#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"
#include "functions.hpp"

void task2()
{
  std::list<std::shared_ptr<Shape>> shapes;
  std::string nextLine;
  std::cin >> std::ws;
  while(std::getline(std::cin, nextLine))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error has occured while the line was written!\n");
    }
    if (nextLine == "")
    {
      continue;
    }
    shapes.push_back(parse(nextLine));
    std::cin >> std::ws;
  }

  std::cout << "Original:\n";
  printShapes(shapes);

  shapes.sort([](const std::shared_ptr<Shape> &shape1, const std::shared_ptr<Shape> shape2) {
    return shape1->isMoreLeft(*shape2);
  });
  std::cout << "Left-Right:\n";
  printShapes(shapes);

  shapes.sort([](const std::shared_ptr<Shape> &shape1, const std::shared_ptr<Shape> shape2) {
    return shape2->isMoreLeft(*shape1);
  });
  std::cout << "Right-Left:\n";
  printShapes(shapes);

  shapes.sort([](const std::shared_ptr<Shape> &shape1, const std::shared_ptr<Shape> shape2) {
    return shape1->isUpper(*shape2);
  });
  std::cout << "Top-Bottom:\n";
  printShapes(shapes);

  shapes.sort([](const std::shared_ptr<Shape> &shape1, const std::shared_ptr<Shape> shape2) {
    return shape2->isUpper(*shape1);
  });
  std::cout << "Bottom-Top:\n";
  printShapes(shapes);
}
