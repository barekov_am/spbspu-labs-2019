#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"


class Triangle : public Shape
{
public:
  Triangle(const point_t &center);
  virtual void draw(std::ostream &out) const;
};

#endif //TRIANGLE_HPP
