#ifndef FUNCTIONS
#define FUNCTIONS
#include <vector>
#include "data-struct.hpp"

const int ABSOLUTE_KEY_VALUE_LIMIT = 5;

DataStruct parseLine(std::string &str);
std::string eraseSpaces(std::string &str, int commaIndex);
int getNextKey(std::string &str);
void checkNumber(std::string &number);
bool compare(const DataStruct &lhs, const DataStruct &rhs);
void formVector(std::vector<DataStruct> &vector);
void print(const std::vector<DataStruct> &vector);


#endif //FUNCTIONS
