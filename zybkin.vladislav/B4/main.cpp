#include <iostream>
#include <vector>
#include <algorithm>
#include "functions.hpp"

int main()
{
  try {
    std::vector<DataStruct> vector;
    formVector(vector);

    std::sort(std::begin(vector), std::end(vector), compare);

    print(vector);
  } catch(std::exception &ex) {
    std::cerr << ex.what();
    return 1;
  }
  return 0;
}


