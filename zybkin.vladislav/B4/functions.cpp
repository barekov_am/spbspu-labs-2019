#include "functions.hpp"
#include <stdexcept>
#include <algorithm>
#include <iostream>
#include <string>

DataStruct parseLine(std::string &str)
{
  if (str.empty())
  {
    throw std::invalid_argument("You've just sent empty line to parse function!\n");
  }
  int key1 = getNextKey(str);
  int key2 = getNextKey(str);
  if (str.empty())
  {
    throw std::invalid_argument("The line doesn't contain the last paramert!\n");
  }
  return {key1, key2, str};
}

std::string eraseSpaces(std::string &str, int commaIndex)
{
  str.erase(0, commaIndex + 1);
  while (str.find_first_of("\t ") == 0)
  {
    str.erase(0, 1);
  }
  return str;
}

int getNextKey(std::string &str)
{
  std::size_t commaIndex = str.find_first_of(',');
  if (commaIndex == std::string::npos)
  {
    throw std::invalid_argument("You've just sent the line which doesn't contain comma at all!\n");
  }
  std::string stringKey = str.substr(0, commaIndex);
  checkNumber(stringKey);
  int key = std::atoi(stringKey.c_str());
  if (std::abs(key) > ABSOLUTE_KEY_VALUE_LIMIT)
  {
    throw std::out_of_range("Ivalid range of input number!\n");
  }
  str = eraseSpaces(str, commaIndex);
  return key;
}

void checkNumber(std::string &number)
{
  std::string::iterator iter = std::begin(number);
  if (number[0] == '-')
  {
    ++iter;
  }
  if (std::end(number) != std::find_if(iter, std::end(number), [&](char c)
      {return !std::isdigit(c); }) || number.empty())
  {
    throw std::invalid_argument("Wrong format of number!\n");
  }
}

bool compare(const DataStruct &lhs, const DataStruct &rhs)
{
  if (lhs.key1 < rhs.key1)
  {
    return true;
  }

  if (lhs.key1 == rhs.key1)
  {
    if (lhs.key2 < rhs.key2)
    {
      return true;
    }

    if ((lhs.key2 == rhs.key2) && (lhs.str.length() < rhs.str.length()))
    {
      return true;
    }
  }

  return false;
}

void formVector(std::vector<DataStruct> &vector)
{
  std::string nextLine;
  while(std::getline(std::cin, nextLine))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error has occured while the next line was written!\n");
    }

    DataStruct nextDataStruct = parseLine(nextLine);
    vector.push_back(nextDataStruct);
  }
}

void print(const std::vector<DataStruct> &vector)
{
  for (DataStruct dataStruct : vector)
  {
    std::cout << dataStruct.key1 << ',' << dataStruct.key2 << ',' << dataStruct.str << "\n";
  }
}

