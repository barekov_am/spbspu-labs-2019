#include "sequence-statistic.hpp"
#include <limits>

SequenceStatistic::SequenceStatistic() :
  max_(std::numeric_limits<long long int>::min()),
  min_(std::numeric_limits<long long int>::max()),
  sum_(0),
  amount_(0),
  positives_(0),
  negatives_(0),
  oddSum_(0),
  evenSum_(0),
  first_(0),
  equalBorderElements_(false)
{
}

void SequenceStatistic::operator ()(long long nextNumber)
{
  if (amount_ == 0)
  {
    first_ = nextNumber;
  }
  ++amount_;
  sum_ += nextNumber;

  if (nextNumber > max_)
  {
    max_ = nextNumber;
  }
  if (nextNumber < min_)
  {
    min_ = nextNumber;
  }

  if (nextNumber > 0)
  {
    ++positives_;
  }
  if (nextNumber < 0)
  {
    ++negatives_;
  }

  if (nextNumber % 2)
  {
    oddSum_ += nextNumber;
  }
  else
  {
    evenSum_ += nextNumber;
  }

  equalBorderElements_ = first_ == nextNumber;
}

long long SequenceStatistic::getMax()
{
  return max_;
}

long long SequenceStatistic::getMin()
{
  return min_;
}

double SequenceStatistic::getMean()
{
  return static_cast<double>(oddSum_ + evenSum_) / amount_;
}

long long SequenceStatistic::getAmountOfPositive()
{
  return positives_;
}

long long SequenceStatistic::getAmountOfNegative()
{
  return negatives_;
}

long long SequenceStatistic::getOddSum()
{
  return oddSum_;
}

long long SequenceStatistic::getEvenSum()
{
  return evenSum_;
}

bool SequenceStatistic::equalBorderElements()
{
  return equalBorderElements_;
}

bool SequenceStatistic::empty()
{
  return amount_ == 0;;
}
