#include <iostream>
#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

int main()
{
  std::cout << "----Rectangle----\n";
  zybkin::Rectangle rect({5.3, 5.2}, 15, 10);
  zybkin::Shape * shape = &rect;
  shape->printInfo();
  std::cout << "Shift to (2.3,2.5)!\n";
  shape->move(2.3, 2.5);
  shape->printInfo();
  std::cout << "Move to (10,24)!\n";
  shape->move({10, 24});
  shape->printInfo();
  std::cout << "Scale in twice!\n";
  shape->scale(2);
  shape->printInfo();

  std::cout << "----Circle----\n";
  zybkin::Circle circle({3.0, 3.5}, 5.7);
  shape = &circle;
  shape->printInfo();
  std::cout << "Shift to (7,1)!\n";
  shape->move(7, 1);
  shape->printInfo();
  std::cout << "Move to (-27,9)!\n";
  shape->move({-27, 9});
  shape->printInfo();
  std::cout << "Scale in 0.5 times!\n";
  shape->scale(0.5);
  shape->printInfo();

  //create some shapes
  zybkin::Circle circle1({1.2, 1.2}, 3.0);
  zybkin::Circle circle2({-0.2, 5.3}, 2.0);
  zybkin::Rectangle rect1({1.6, 1.6}, 5.0, 3.0);
  zybkin::Rectangle rect2({-7.3, 2.6}, 8.2, 5.3);

  std::cout << "Create composite shape:\n";
  zybkin::CompositeShape compositeShape(std::make_shared<zybkin::Circle>(circle1));

  compositeShape.printInfo();

  std::cout << "Add other shapes in composite:\n";
  compositeShape.add(std::make_shared<zybkin::Rectangle>(rect1));
  compositeShape.add(std::make_shared<zybkin::Rectangle>(rect2));
  compositeShape.add(std::make_shared<zybkin::Circle>(circle2));
  compositeShape.printInfo();

  std::cout << "Shift at (3,3)\n";
  compositeShape.move(3, 3);
  compositeShape.printInfo();

  std::cout << "Remove shapes which have index 2 and 3\n";
  compositeShape.remove(3);
  compositeShape.remove(2);
  compositeShape.printInfo();

  std::cout << "Move to {-4,7}\n";
  compositeShape.move({-4, 7});
  compositeShape.printInfo();

  std::cout << "Get shape which has index 0 and print information about it:\n";
  std::shared_ptr<zybkin::Shape> shapeFromComposite = compositeShape[0];
  shapeFromComposite->printInfo();

  std::cout << "Scale composite shape in 0,8 times\n";
  compositeShape.scale(0.8);
  compositeShape.printInfo();

  return 0;
}
