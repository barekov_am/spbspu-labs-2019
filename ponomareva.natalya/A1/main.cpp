#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

int main()
{
  Rectangle rect(6, 3, { 1, 2 });
  Shape *shape = &rect;
  shape->printInfo();
  shape->move(3, -1);
  shape->printInfo();
  shape->move({ 2, 0 });
  shape->printInfo();

  Circle circle(4, { 8, 6 });
  shape = &circle;
  shape->printInfo();
  shape->move(-2, 9);
  shape->printInfo();
  shape->move({ 1, 5 });
  shape->printInfo();

  Triangle triangle({ 1, 1 }, { 2, 4 }, { 5, 4 });
  shape = &triangle;
  shape->printInfo();
  shape->move(0, -6);
  shape->printInfo();
  shape->move({ -2, 3 });
  shape->printInfo();

  const unsigned int size = 6;
  const point_t points[size] = { { -1, -2 }, { -3, 0 }, { 1, 4 }, { 3, 5 }, { 7, -1 }, { 3, -3 } };
  Polygon polygon(points, size);
  shape = &polygon;
  shape->printInfo();
  shape->move(2, 2);
  shape->printInfo();
  shape->move({ -4, -9 });
  shape->printInfo();

  return 0;
}
