#define _USE_MATH_DEFINES

#include "circle.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>

Circle::Circle(double radius, const point_t &center) :
  radius_(radius),
  center_(center)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("Radius must be positive!");
  }
}

double Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

rectangle_t Circle::getFrameRect() const
{
  return { 2 * radius_, 2 * radius_, center_ };
}

void Circle::move(const point_t &center)
{
  center_ = center;
}

void Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Circle::printInfo() const
{
  std::cout << "This is a Circle.\n";
  std::cout << "Center: (" << center_.x << ", " << center_.y << ")\n" << "Radius: " << radius_ << "\n";
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "Frame Rectangle:\n center: (" << getFrameRect().pos.x << ", " << getFrameRect().pos.y << ")\n";
  std::cout << " width: " << getFrameRect().width << "\n height: " << getFrameRect().height << "\n\n";
}
