#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

Triangle::Triangle(const point_t &pointA, const point_t &pointB, const point_t &pointC) :
  pointA_(pointA),
  pointB_(pointB),
  pointC_(pointC)
{
  if (getArea() <= 0.0)
  {
    throw std::invalid_argument("Vertices aren't correct!");
  }
}

double Triangle::getArea() const
{
  double AB = findSide(pointA_, pointB_);
  double BC = findSide(pointB_, pointC_);
  double AC = findSide(pointA_, pointC_);
  double halfP = (AB + BC + AC) / 2;
  return sqrt(halfP * (halfP - AB) * (halfP - BC) * (halfP - AC));
}

rectangle_t Triangle::getFrameRect() const
{
  double xMin = std::min(std::min(pointA_.x, pointB_.x), pointC_.x);
  double xMax = std::max(std::max(pointA_.x, pointB_.x), pointC_.x);
  double yMin = std::min(std::min(pointA_.y, pointB_.y), pointC_.y);
  double yMax = std::max(std::max(pointA_.y, pointB_.y), pointC_.y);
  return { xMax - xMin, yMax - yMin, { xMin + (xMax - xMin) / 2, yMin + (yMax - yMin) / 2 } };
}

void Triangle::move(const point_t &center)
{
  move(center.x - findCenter().x, center.y - findCenter().y);
}

void Triangle::move(double dx, double dy)
{
  pointA_.x += dx;
  pointB_.x += dx;
  pointC_.x += dx;
  pointA_.y += dy;
  pointB_.y += dy;
  pointC_.y += dy;
}

double Triangle::findSide(const point_t &a, const point_t &b) const
{
  return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

point_t Triangle::findCenter() const
{
  return { (pointA_.x + pointB_.x + pointC_.x) / 3, (pointA_.y + pointB_.y + pointC_.y) / 3 };
}

void Triangle::printInfo() const
{
  std::cout << "This is a Triangle.\n" << "Center: (" << findCenter().x << ", " << findCenter().y;
  std::cout << ")\nFirst vertice: (" << pointA_.x << ", " << pointA_.y << ")\n";
  std::cout << "Second vertice: (" << pointB_.x << ", " << pointB_.y << ")\n";
  std::cout << "Third vertice: (" << pointC_.x << ", " << pointC_.y << ")\n";
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "Frame Rectangle:\n center: (" << getFrameRect().pos.x << ", " << getFrameRect().pos.y << ")\n";
  std::cout << " width: " << getFrameRect().width << "\n height: " << getFrameRect().height << "\n\n";
}
