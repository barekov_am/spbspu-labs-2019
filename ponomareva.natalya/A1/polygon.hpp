#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "shape.hpp"

class Polygon : public Shape
{
public:
  Polygon(const point_t *points, unsigned int amount);
  Polygon(const Polygon &other);
  Polygon(Polygon &&other);
  virtual ~Polygon();
  Polygon &operator =(const Polygon &other);
  Polygon &operator =(Polygon &&other);

  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &center) override;
  void move(double dx, double dy) override;
  void printInfo() const override;

private:
  point_t* points_;
  unsigned int amount_;
  point_t findCenter() const;
  bool checkConvexity() const;
};

#endif //POLYGON_HPP
