#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>

Rectangle::Rectangle(double width, double height, const point_t &center) :
  width_(width),
  height_(height),
  center_(center)
{
  if ((width <= 0.0) || (height <= 0.0))
  {
    throw std::invalid_argument("Width and height must be positive!");
  }
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return { width_, height_, center_ };
}

void Rectangle::move(const point_t &center)
{
  center_ = center;
}

void Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Rectangle::printInfo() const
{
  std::cout << "This is a Rectangle.\n";
  std::cout << "Center: (" << center_.x << ", " << center_.y << ")\n";
  std::cout << "Width: " << width_ << "\nHeight: " << height_ << "\n";
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "Frame Rectangle:\n center: (" << getFrameRect().pos.x << ", " << getFrameRect().pos.y << ")\n";
  std::cout << " width: " << getFrameRect().width << "\n height: " << getFrameRect().height << "\n\n";
}
