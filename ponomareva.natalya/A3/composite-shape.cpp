#define _USE_MATH_DEFINES

#include "composite-shape.hpp"
#include <iostream>
#include <algorithm>
#include <memory>
#include <cmath>

ponomareva::CompositeShape::CompositeShape() :
  amount_(0),
  shapeArray_(nullptr)
{}

ponomareva::CompositeShape::CompositeShape(const CompositeShape &other) :
  amount_(other.amount_),
  shapeArray_(std::make_unique<ponomareva::Shape::ptr_type[]>(other.amount_))
{
  for (int i = 0; i < amount_; i++)
  {
    shapeArray_[i] = other.shapeArray_[i];
  }
}

ponomareva::CompositeShape::CompositeShape(CompositeShape &&other) :
  amount_(other.amount_),
  shapeArray_(std::move(other.shapeArray_))
{}

ponomareva::CompositeShape::CompositeShape(Shape::ptr_type& shape) :
  amount_(1),
  shapeArray_(std::make_unique<ponomareva::Shape::ptr_type[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape is not exist");
  }
  shapeArray_[0] = shape;
}

ponomareva::CompositeShape &ponomareva::CompositeShape::operator =(const CompositeShape &other)
{
  if (this != &other)
  {
    amount_ = other.amount_;
    array_type tempArray(std::make_unique<ponomareva::Shape::ptr_type[]>(other.amount_));
    for (int i = 0; i < amount_; i++)
    {
      tempArray[i] = other.shapeArray_[i];
    }
    shapeArray_.swap(tempArray);
  }
  return *this;
}

ponomareva::CompositeShape &ponomareva::CompositeShape::operator =(CompositeShape &&other)
{
  if (this != &other)
  {
    amount_ = other.amount_;
    shapeArray_.swap(other.shapeArray_);
    other.amount_ = 0;
    other.shapeArray_.reset();
  }
  return *this;
}

ponomareva::Shape::ptr_type ponomareva::CompositeShape::operator[](const int i) const
{
  if ((i < 0) || (i >= amount_))
  {
    throw std::invalid_argument("Index is out of range!");
  }
  return shapeArray_[i];
}

double ponomareva::CompositeShape::getArea() const
{
  if (amount_ == 0)
  {
    throw std::invalid_argument("Composite shape is not exist!");
  }
  double area = 0.0;
  for (int i = 0; i < amount_; i++)
  {
    area += shapeArray_[i]->getArea();
  }
  return area;
}

ponomareva::rectangle_t ponomareva::CompositeShape::getFrameRect() const
{
  if (amount_ == 0)
  {
    throw std::invalid_argument("Composite shape is not exist!");
  }
  rectangle_t frameRectangle = shapeArray_[0]->getFrameRect();
  double minX = frameRectangle.pos.x - frameRectangle.width / 2;
  double maxX = frameRectangle.pos.x + frameRectangle.width / 2;
  double minY = frameRectangle.pos.y - frameRectangle.height / 2;
  double maxY = frameRectangle.pos.y + frameRectangle.height / 2;
  for (int i = 1; i < amount_; i++)
  {
    frameRectangle = shapeArray_[i]->getFrameRect();
    double tempRectangle = frameRectangle.pos.x - frameRectangle.width / 2;
    minX = std::min(tempRectangle, minX);
    tempRectangle = frameRectangle.pos.y - frameRectangle.height / 2;
    minY = std::min(tempRectangle, minY);
    tempRectangle = frameRectangle.pos.x + frameRectangle.width / 2;
    maxX = std::max(tempRectangle, maxX);
    tempRectangle = frameRectangle.pos.y + frameRectangle.height / 2;
    maxY = std::max(tempRectangle, maxY);
  }
  return { (maxX - minX), (maxY - minY), {(minX + maxX) / 2, (minY + maxY) / 2} };
}

void ponomareva::CompositeShape::move(const point_t &point)
{
  if (amount_ == 0)
  {
    throw std::invalid_argument("Composite shape is not exist!");
  }
  ponomareva::point_t center = getFrameRect().pos;
  move(point.x - center.x, point.y - center.y);
}

void ponomareva::CompositeShape::move(double dx, double dy)
{
  if (amount_ == 0)
  {
    throw std::invalid_argument("Composite shape is not exist!");
  }
  for (int i = 0; i < amount_; i++)
  {
    shapeArray_[i]->move(dx, dy);
  }
}

void ponomareva::CompositeShape::scale(double coef)
{
  if (amount_ == 0)
  {
    throw std::invalid_argument("Composite shape is not exist!");
  }
  if (coef > 0.0)
  {
    ponomareva::point_t center = getFrameRect().pos;
    for (int i = 0; i < amount_; i++)
    {
      ponomareva::point_t centerShape = shapeArray_[i]->getFrameRect().pos;
      double dx = (centerShape.x - center.x) * (coef - 1);
      double dy = (centerShape.y - center.y) * (coef - 1);
      shapeArray_[i]->move(dx, dy);
      shapeArray_[i]->scale(coef);
    }
  }
  else
  {
    throw std::invalid_argument("Coefficient must be positive!");
  }
}

void ponomareva::CompositeShape::rotate(double angle)
{
  if (amount_ == 0)
  {
    throw std::invalid_argument("Composite shape is not exist!");
  }
  const point_t center = getFrameRect().pos;
  const double cos = fabs(std::cos(angle * M_PI / 180));
  const double sin = fabs(std::sin(angle * M_PI / 180));
  for (int i = 0; i < amount_; i++)
  {
    const point_t other_center = shapeArray_[i]->getFrameRect().pos;
    double dx = (other_center.x - center.x) * (cos - 1) - (other_center.y - center.y) * sin;
    double dy = (other_center.x - center.x) * sin + (other_center.y - center.y) * (cos - 1);
    shapeArray_[i]->move(dx, dy);
    shapeArray_[i]->rotate(angle);
  }
}

void ponomareva::CompositeShape::addShape(const Shape::ptr_type& shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape is not exist!");
  }
  array_type tempArray(std::make_unique<ponomareva::Shape::ptr_type[]>(amount_ + 1));
  for (int i = 0; i < amount_; i++)
  {
    tempArray[i] = shapeArray_[i];
  }
  tempArray[amount_] = shape;
  amount_++;
  shapeArray_.swap(tempArray);
}

void ponomareva::CompositeShape::deleteShape(const int n)
{
  if ((n < 0) || (n >= amount_))
  {
    throw std::invalid_argument("Index is out of range!");
  }
  array_type tempArray(std::make_unique<ponomareva::Shape::ptr_type[]>(amount_ - 1));
  for (int i = 0; i < n; i++)
  {
    tempArray[i] = shapeArray_[i];
  }
  amount_--;
  for (int i = n; i < amount_; i++)
  {
    tempArray[i] = shapeArray_[i + 1];
  }
  shapeArray_.swap(tempArray);
}

int ponomareva::CompositeShape::getAmount() const
{
  return amount_;
}

ponomareva::point_t ponomareva::CompositeShape::findCenter() const
{
  ponomareva::rectangle_t frameRect = shapeArray_[0]->getFrameRect();
  double maxX = frameRect.pos.x + frameRect.width / 2;
  double minX = frameRect.pos.x - frameRect.width / 2;
  double maxY = frameRect.pos.y + frameRect.height / 2;
  double minY = frameRect.pos.y - frameRect.height / 2;
  for (int i = 0; i < amount_; i++)
  {
    frameRect = shapeArray_[i]->getFrameRect();
    maxX = std::max(maxX, frameRect.pos.x + frameRect.width / 2);
    minX = std::min(minX, frameRect.pos.x - frameRect.width / 2);
    maxY = std::max(maxY, frameRect.pos.y + frameRect.height / 2);
    minY = std::min(minY, frameRect.pos.y - frameRect.height / 2);
  }
  return { (maxX + minX) / 2, (maxY + minY) / 2 };
}

void ponomareva::CompositeShape::printInfo() const
{
  if (amount_ == 0)
  {
    throw std::invalid_argument("Composite shape is not exist!");
  }
  for (int i = 0; i < amount_; i++)
  {
    std::cout << "Figure " << i << "\n";
    shapeArray_[i]->printInfo();
  }
}
