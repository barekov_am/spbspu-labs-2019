#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "polygon.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(testCompositeShape)

BOOST_AUTO_TEST_CASE(invariabilityOfParametersAfterMoving)
{
  ponomareva::Shape::ptr_type circle = std::make_shared<ponomareva::Circle>(4, ponomareva::point_t{ -4, 4 });
  ponomareva::Shape::ptr_type rect = std::make_shared<ponomareva::Rectangle>(4, 2, ponomareva::point_t{ 4, -3 });
  ponomareva::Shape::ptr_type triangle = std::make_shared<ponomareva::Triangle>(ponomareva::point_t{ -2, -1 },
    ponomareva::point_t{ -1, 3 }, ponomareva::point_t{ 3, 0 });
  const int amount = 5;
  const ponomareva::point_t points[amount] = { { 1, 3 }, { 2, 5 }, { 5, 5 }, { 5, 3 }, { 3, 1 } };
  ponomareva::Shape::ptr_type polygon = std::make_shared<ponomareva::Polygon>(points, amount);

  ponomareva::CompositeShape compShape(rect);
  compShape.addShape(circle);
  compShape.addShape(triangle);
  compShape.addShape(polygon);

  ponomareva::rectangle_t frameRect = compShape.getFrameRect();
  double area = compShape.getArea();
  compShape.move({ 5, -9 });
  BOOST_CHECK_CLOSE(frameRect.width, compShape.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRect.height, compShape.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(area, compShape.getArea(), ACCURACY);
  compShape.move(-1, 6);
  BOOST_CHECK_CLOSE(frameRect.width, compShape.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRect.height, compShape.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(area, compShape.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(squareChangingOfAreaAfterScaling)
{
  ponomareva::Shape::ptr_type circle = std::make_shared<ponomareva::Circle>(4, ponomareva::point_t{ -4, 4 });
  ponomareva::Shape::ptr_type rect = std::make_shared<ponomareva::Rectangle>(4, 2, ponomareva::point_t{ 4, -3 });
  ponomareva::Shape::ptr_type triangle = std::make_shared<ponomareva::Triangle>(ponomareva::point_t{ -2, -1 },
    ponomareva::point_t{ -1, 3 }, ponomareva::point_t{ 3, 0 });
  const int amount = 5;
  const ponomareva::point_t points[amount] = { { 1, 3 }, { 2, 5 }, { 5, 5 }, { 5, 3 }, { 3, 1 } };
  ponomareva::Shape::ptr_type polygon = std::make_shared<ponomareva::Polygon>(points, amount);

  ponomareva::CompositeShape compShape(rect);
  compShape.addShape(circle);
  compShape.addShape(triangle);
  compShape.addShape(polygon);

  double coef = 4;
  double area = compShape.getArea();
  ponomareva::point_t center = compShape.getFrameRect().pos;
  compShape.scale(coef);
  BOOST_CHECK_CLOSE(compShape.getArea(), area * coef * coef, ACCURACY);
  BOOST_CHECK_CLOSE(center.x, compShape.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(center.y, compShape.getFrameRect().pos.y, ACCURACY);
 
  coef = 0.4;
  area = compShape.getArea();
  compShape.scale(coef);
  BOOST_CHECK_CLOSE(compShape.getArea(), area * coef * coef, ACCURACY);
  BOOST_CHECK_CLOSE(center.x, compShape.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(center.y, compShape.getFrameRect().pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  ponomareva::Shape::ptr_type circle = std::make_shared<ponomareva::Circle>(6, ponomareva::point_t{ 3, 4 });
  ponomareva::CompositeShape compShape(circle);

  BOOST_CHECK_THROW(compShape.deleteShape(-1), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.deleteShape(5), std::invalid_argument);
  compShape.deleteShape(0);
  BOOST_CHECK_THROW(compShape.getArea(), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.getFrameRect(), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.move(6, 2), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.move({ -5, 1 }), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.scale(0.5), std::invalid_argument);
  BOOST_CHECK_THROW(compShape[3], std::invalid_argument);
  BOOST_CHECK_THROW(compShape[-4], std::invalid_argument);
  BOOST_CHECK_THROW(compShape.printInfo(), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invariabilityOfParametersAfterRotating)
{
  ponomareva::Shape::ptr_type circle = std::make_shared<ponomareva::Circle>(4, ponomareva::point_t{ -4, 4 });
  ponomareva::Shape::ptr_type rect = std::make_shared<ponomareva::Rectangle>(4, 2, ponomareva::point_t{ 4, -3 });
  ponomareva::Shape::ptr_type triangle = std::make_shared<ponomareva::Triangle>(ponomareva::point_t{ -2, -1 },
    ponomareva::point_t{ -1, 3 }, ponomareva::point_t{ 3, 0 });
  const int amount = 5;
  const ponomareva::point_t points[amount] = { { 1, 3 }, { 2, 5 }, { 5, 5 }, { 5, 3 }, { 3, 1 } };
  ponomareva::Shape::ptr_type polygon = std::make_shared<ponomareva::Polygon>(points, amount);

  ponomareva::CompositeShape compShape(rect);
  compShape.addShape(circle);
  compShape.addShape(triangle);
  compShape.addShape(polygon);
 
  double area = compShape.getArea();
  ponomareva::rectangle_t frameRect = compShape.getFrameRect();
  compShape.rotate(360);
  BOOST_CHECK_CLOSE(frameRect.height, compShape.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRect.width, compShape.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRect.pos.x, compShape.getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRect.pos.y, compShape.getFrameRect().pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(area, compShape.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
