#include "matrix.hpp"
#include <iostream>
#include <stdexcept>

ponomareva::Matrix::Matrix() :
  lines_(0),
  columns_(0),
  shape_matrix_()
{}

ponomareva::Matrix::Matrix(const Matrix &other) :
  lines_(other.lines_),
  columns_(other.columns_),
  shape_matrix_(std::make_unique<Shape::ptr_type[]>(other.lines_ * other.columns_))
{
  for (int i = 0; i < (lines_ * columns_); i++)
  {
    shape_matrix_[i] = other.shape_matrix_[i];
  }
}

ponomareva::Matrix::Matrix(Matrix &&other) :
  lines_(other.lines_),
  columns_(other.columns_),
  shape_matrix_(std::move(other.shape_matrix_))
{
  other.lines_ = 0;
  other.columns_ = 0;
}

ponomareva::Matrix& ponomareva::Matrix::operator =(const Matrix &other)
{
  if (this != &other)
  {
    lines_ = other.lines_;
    columns_ = other.columns_;
    Shape::array_type temp_shape_matrix = std::make_unique<Shape::ptr_type[]>(lines_ * columns_);
    for (int i = 0; i < (lines_ * columns_); i++)
    {
      temp_shape_matrix[i] = other.shape_matrix_[i];
    }
    shape_matrix_.swap(temp_shape_matrix);
  }
  return *this;
}

ponomareva::Matrix& ponomareva::Matrix::operator =(Matrix &&other)
{
  if (this != &other)
  {
    lines_ = other.lines_;
    columns_ = other.columns_;
    shape_matrix_ = std::move(other.shape_matrix_);
    other.lines_ = 0;
    other.columns_ = 0;
  }
  return *this;
}

ponomareva::Shape::array_type ponomareva::Matrix::operator[](int k) const
{
  if ((lines_ <= k) || (k < 0))
  {
    throw std::invalid_argument("Index is out of range!");
  }
  Shape::array_type temp_shape_matrix = std::make_unique<Shape::ptr_type[]>(columns_);
  for (int i = 0; i < columns_; i++)
  {
    temp_shape_matrix[i] = shape_matrix_[k * columns_ + i];
  }
  return temp_shape_matrix;
}

bool ponomareva::Matrix::operator==(const Matrix &other) const
{
  if ((lines_ != other.lines_) || (columns_ != other.columns_))
  {
    return false;
  }
  for (int i = 0; i < (lines_ * columns_); i++)
  {
    if (shape_matrix_[i] != other.shape_matrix_[i])
    {
      return false;
    }
  }
  return true;
}

bool ponomareva::Matrix::operator!=(const Matrix &other) const
{
  if (this == &other)
  {
    return false;
  }
  return true;
}

void ponomareva::Matrix::add(const Shape::ptr_type &shape, const pair_t &pair)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape doesn't exist!");
  }
  const int lines = (lines_ == pair.line) ? (lines_ + 1) : lines_;
  const int columns = (columns_ == pair.column) ? (columns_ + 1) : columns_;
  Shape::array_type temp_shape_matrix = std::make_unique<Shape::ptr_type[]>(lines * columns);
  for (int i = 0; i < lines; i++)
  {
    for (int j = 0; j < columns; j++)
    {
      if ((lines_ == i) || (columns_ == j))
      {
        temp_shape_matrix[i * columns + j] = nullptr;
      }
      else
      {
        temp_shape_matrix[i * columns + j] = shape_matrix_[i * columns_ + j];
      }
    }
  }
  temp_shape_matrix[pair.line * columns + pair.column] = shape;
  shape_matrix_.swap(temp_shape_matrix);
  lines_ = lines;
  columns_ = columns;
}

int ponomareva::Matrix::getLines() const
{
  return lines_;
}

int ponomareva::Matrix::getColumns() const
{
  return columns_;
}

void ponomareva::Matrix::printInfo() const
{
  std::cout << "Matrix: " << lines_ << " lines and " << columns_ << " columns (max value)" << std::endl;
  for (int i = 0; i < lines_; i++)
  {
    for (int j = 0; j < columns_; j++)
    {
      if (shape_matrix_[i * columns_ + j] != nullptr)
      {
        std::cout << "In " << i + 1 << " line, " << j + 1 << " column : " << std::endl;
        shape_matrix_[i * columns_ + j]->printInfo();
        std::cout << std::endl;
      }
    }
  }
}
