#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include <memory>
#include "circle.hpp"
#include "rectangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(matrixCopyConstructor)
{
  ponomareva::Rectangle rect({ 2, 12, {4, 7} });
  ponomareva::Shape::ptr_type rectPtr = std::make_shared<ponomareva::Rectangle>(rect);
  ponomareva::CompositeShape compShape(rectPtr);
  ponomareva::Matrix copiedMatrix = partition(compShape);

  ponomareva::Matrix matrix(copiedMatrix);

  BOOST_CHECK_EQUAL(matrix.getLines(), copiedMatrix.getLines());
  BOOST_CHECK_EQUAL(matrix.getColumns(), copiedMatrix.getColumns());
  BOOST_CHECK(matrix == copiedMatrix);
}

BOOST_AUTO_TEST_CASE(matrixCopyOperator)
{
  ponomareva::Rectangle rect({ 2, 12, {4, 7} });
  ponomareva::Shape::ptr_type rectPtr = std::make_shared<ponomareva::Rectangle>(rect);
  ponomareva::CompositeShape compShape(rectPtr);
  ponomareva::Matrix copiedMatrix = partition(compShape);

  ponomareva::Matrix matrix;
  matrix = copiedMatrix;

  BOOST_CHECK_EQUAL(matrix.getLines(), copiedMatrix.getLines());
  BOOST_CHECK_EQUAL(matrix.getColumns(), copiedMatrix.getColumns());
  BOOST_CHECK(matrix == copiedMatrix);
}

BOOST_AUTO_TEST_CASE(matrixMoveConstructor)
{
  ponomareva::Rectangle rect({ 2, 12, {4, 7} });
  ponomareva::Shape::ptr_type rectPtr = std::make_shared<ponomareva::Rectangle>(rect);
  ponomareva::CompositeShape compShape(rectPtr);
  ponomareva::Matrix matrix = partition(compShape);

  ponomareva::Matrix copiedMatrix(matrix);
  ponomareva::Matrix movedMatrix(std::move(matrix));
 
  BOOST_CHECK_EQUAL(movedMatrix.getLines(), copiedMatrix.getLines());
  BOOST_CHECK_EQUAL(movedMatrix.getColumns(), copiedMatrix.getColumns());
  BOOST_CHECK(movedMatrix == copiedMatrix);
  BOOST_CHECK_EQUAL(matrix.getLines(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(matrixMoveOperator)
{
  ponomareva::Rectangle rect({ 2, 12, {4, 7} });
  ponomareva::Shape::ptr_type rectPtr = std::make_shared<ponomareva::Rectangle>(rect);
  ponomareva::CompositeShape compShape(rectPtr);
  ponomareva::Matrix matrix = partition(compShape);

  ponomareva::Matrix copiedMatrix(matrix);
  ponomareva::Matrix movedMatrix;
  movedMatrix = std::move(matrix);
 
  BOOST_CHECK_EQUAL(movedMatrix.getLines(), copiedMatrix.getLines());
  BOOST_CHECK_EQUAL(movedMatrix.getColumns(), copiedMatrix.getColumns());
  BOOST_CHECK(movedMatrix == copiedMatrix);
  BOOST_CHECK_EQUAL(matrix.getLines(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(invalidArgumentsAndCorrectness)
{
  ponomareva::Shape::ptr_type circle = std::make_shared<ponomareva::Circle>(3, ponomareva::point_t{ 0, 8 });
  ponomareva::Shape::ptr_type rect = std::make_shared<ponomareva::Rectangle>(1, 5, ponomareva::point_t{ 20, 4 });
  const int amount = 5;
  const ponomareva::point_t points[amount] = { { -2, 2 }, { -3, 6 }, { -10, 3 }, { -8, -1 }, { -5, 0 } };
  ponomareva::Shape::ptr_type polygon = std::make_shared<ponomareva::Polygon>(points, amount);

  ponomareva::CompositeShape compShape;
  compShape.addShape(circle);
  compShape.addShape(rect);
  compShape.addShape(polygon);

  ponomareva::Matrix matrix = ponomareva::partition(compShape);

  const int lines = 2;
  const int columns = 2;

  BOOST_CHECK_EQUAL(matrix.getLines(), lines);
  BOOST_CHECK_EQUAL(matrix.getColumns(), columns);
  BOOST_CHECK(matrix[0][0] == circle);
  BOOST_CHECK(matrix[0][1] == rect);
  BOOST_CHECK(matrix[1][0] == polygon);

  BOOST_CHECK_THROW(matrix[10], std::invalid_argument);
  BOOST_CHECK_THROW(matrix[-1], std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
