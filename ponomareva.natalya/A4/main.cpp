#include <iostream>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

void test(ponomareva::Shape::ptr_type &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape doesn't exist!");
  }
  shape->printInfo();
  shape->move({ 5, -2 });
  shape->printInfo();
  shape->move(8, -20);
  shape->printInfo();
  shape->scale(4);
  shape->printInfo();
  shape->rotate(180);
  shape->printInfo();
}

int main()
{
  ponomareva::Shape::ptr_type rect = std::make_shared<ponomareva::Rectangle>(1, 5, ponomareva::point_t{ 20, 4 });
  test(rect);

  ponomareva::Shape::ptr_type circle = std::make_shared<ponomareva::Circle>(3, ponomareva::point_t{ 0, 8 });
  test(circle);

  ponomareva::Shape::ptr_type triangle = std::make_shared<ponomareva::Triangle>(ponomareva::point_t{ 1, 1 },
    ponomareva::point_t{ 3, 4 }, ponomareva::point_t{ -4, 4 });
  test(triangle);

  const int amount = 5;
  const ponomareva::point_t points[amount] = { { -2, 2 }, { -3, 6 }, { -10, 3 }, { -8, -1 }, { -5, 0 } };
  ponomareva::Shape::ptr_type polygon = std::make_shared<ponomareva::Polygon>(points, amount);
  test(polygon);

  ponomareva::CompositeShape compShape(rect);
  compShape.addShape(circle);
  compShape.addShape(polygon);

  ponomareva::Matrix matrix = ponomareva::partition(compShape);
  test(matrix[1][0]);
  return 0;
}
