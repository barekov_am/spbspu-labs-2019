#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace ponomareva
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &other);
    CompositeShape(CompositeShape &&other);
    CompositeShape(Shape::ptr_type &other);
    ~CompositeShape() = default;
    CompositeShape &operator =(const CompositeShape &other);
    CompositeShape &operator =(CompositeShape &&other);
    Shape::ptr_type operator [](const int i) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(double dx, double dy) override;
    void scale(double coef) override;
    void rotate(double angle) override;
    void addShape(const Shape::ptr_type &shape);
    void deleteShape(const int n);
    int getAmount() const;
    point_t findCenter() const;
    void printInfo() const override;

  private:
    int amount_;
    Shape::array_type shapeArray_;
  };
}

#endif //POLYGON_HPP
