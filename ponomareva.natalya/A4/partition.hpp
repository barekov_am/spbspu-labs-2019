#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace ponomareva
{
  bool isOverlapping(const rectangle_t &shape1, const rectangle_t &shape2);
  Matrix partition(const ponomareva::CompositeShape &compShape);
}

#endif // PARTITION_HPP
