#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

namespace ponomareva
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &matrix);
    Matrix(Matrix &&matrix);
    ~Matrix() = default;

    Matrix &operator =(const Matrix &other);
    Matrix &operator =(Matrix &&other);
    Shape::array_type operator [](int k) const;
    bool operator ==(const Matrix &other) const;
    bool operator !=(const Matrix &other) const;

    void add(const Shape::ptr_type &shape, const pair_t &pair);
    int getLines() const;
    int getColumns() const;
    void printInfo() const;

  private:
    int lines_;
    int columns_;
    Shape::array_type shape_matrix_;
  };
}

#endif // MATRIX_HPP
