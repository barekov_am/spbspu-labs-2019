#include "partition.hpp"

bool ponomareva::isOverlapping(const rectangle_t &shape1, const rectangle_t &shape2)
{
  const point_t leftBottom = { shape1.pos.x - shape1.width / 2, shape1.pos.y - shape1.height / 2 };
  const point_t leftTop = { shape1.pos.x + shape1.width / 2, shape1.pos.y + shape1.height / 2 };
  const point_t rightBottom = { shape2.pos.x - shape2.width / 2, shape2.pos.y - shape2.height / 2 };
  const point_t rightTop = { shape2.pos.x + shape2.width / 2, shape2.pos.y + shape2.height / 2 };

  return ((leftBottom.x < rightTop.x && leftBottom.y < rightTop.y) &&
    (rightBottom.x < leftTop.x && rightBottom.y < leftTop.y));
}

ponomareva::Matrix ponomareva::partition(const CompositeShape &compShape)
{
  Matrix matrix;
  for (int i = 0; i < compShape.getAmount(); i++)
  {
    int line = 0;
    int column = 0;

    for (int j = 0; j < matrix.getLines(); j++)
    {
      for (int k = 0; k < matrix.getColumns(); k++)
      {
        if (matrix[j][k] == nullptr)
        {
          column = k;
          line = j;
          break;
        }
        if (isOverlapping(compShape[i]->getFrameRect(), matrix[j][k]->getFrameRect()))
        {
          column = 0;
          line = j + 1;
          break;
        }
        column = k + 1;
        line = j;
      }
      if (line == j)
      {
        break;
      }
    }
    matrix.add(compShape[i], { line, column });
  }
  return matrix;
}
