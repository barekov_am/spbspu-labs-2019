#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(testCircle)

BOOST_AUTO_TEST_CASE(invariabilityOfParametersAfterMoving)
{
  ponomareva::Circle circle(2, { 3, 1 });
  ponomareva::rectangle_t frameRect = circle.getFrameRect();
  double area = circle.getArea();
  circle.move(5, 2);
  BOOST_CHECK_CLOSE(circle.getArea(), area, ACCURACY);
  BOOST_CHECK_CLOSE(circle.getFrameRect().width, frameRect.width, ACCURACY);
  BOOST_CHECK_CLOSE(circle.getFrameRect().height, frameRect.height, ACCURACY);
  circle.move({ 1, 6 });
  BOOST_CHECK_CLOSE(circle.getArea(), area, ACCURACY);
  BOOST_CHECK_CLOSE(circle.getFrameRect().width, frameRect.width, ACCURACY);
  BOOST_CHECK_CLOSE(circle.getFrameRect().height, frameRect.height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(squareChangingOfAreaAfterScaling)
{
  ponomareva::Circle circle(8, { -2, 5 });
  double area = circle.getArea();
  double coef = 3;
  circle.scale(coef);
  BOOST_CHECK_CLOSE(circle.getArea(), area * coef * coef, ACCURACY);
  coef = 0.3;
  area = circle.getArea();
  circle.scale(coef);
  BOOST_CHECK_CLOSE(circle.getArea(), area * coef * coef, ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  BOOST_CHECK_THROW(ponomareva::Circle circle(-5, { 8, 0 }), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
