#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(testRectangle)

BOOST_AUTO_TEST_CASE(invariabilityOfParametersAfterMoving)
{
  ponomareva::Rectangle rect(5, 5, { -2, 9 });
  ponomareva::rectangle_t frameRect = rect.getFrameRect();
  double area = rect.getArea();
  rect.move(9, 0);
  BOOST_CHECK_CLOSE(rect.getArea(), area, ACCURACY);
  BOOST_CHECK_CLOSE(rect.getFrameRect().width, frameRect.width, ACCURACY);
  BOOST_CHECK_CLOSE(rect.getFrameRect().height, frameRect.height, ACCURACY);
  rect.move({ 19, 5 });
  BOOST_CHECK_CLOSE(rect.getArea(), area, ACCURACY);
  BOOST_CHECK_CLOSE(rect.getFrameRect().width, frameRect.width, ACCURACY);
  BOOST_CHECK_CLOSE(rect.getFrameRect().height, frameRect.height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(squareChangingOfAreaAfterScaling)
{
  ponomareva::Rectangle rect(10, 9, { -20, 13 });
  double area = rect.getArea();
  double coef = 4;
  rect.scale(coef);
  BOOST_CHECK_CLOSE(rect.getArea(), area * coef * coef, ACCURACY);
  coef = 0.7;
  area = rect.getArea();
  rect.scale(coef);
  BOOST_CHECK_CLOSE(rect.getArea(), area * coef * coef, ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  BOOST_CHECK_THROW(ponomareva::Rectangle rect(-1, 2, { 2, -6 }), std::invalid_argument);
  BOOST_CHECK_THROW(ponomareva::Rectangle rect(3, 0, { 2, -6 }), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
