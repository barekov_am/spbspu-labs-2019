#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "polygon.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(testPolygon)

BOOST_AUTO_TEST_CASE(invariabilityOfParametersAfterMoving)
{
  const unsigned int size = 5;
  const ponomareva::point_t points[size] = { { -1, -2 }, { -3, 0 }, { 3, 5 }, { 7, -1 }, { 3, -3 } };
  ponomareva::Polygon polygon(points, size);
  ponomareva::rectangle_t frameRect = polygon.getFrameRect();
  double area = polygon.getArea();
  polygon.move(-8, 15);
  BOOST_CHECK_CLOSE(polygon.getArea(), area, ACCURACY);
  BOOST_CHECK_CLOSE(polygon.getFrameRect().width, frameRect.width, ACCURACY);
  BOOST_CHECK_CLOSE(polygon.getFrameRect().height, frameRect.height, ACCURACY);
  polygon.move({ -11, 7 });
  BOOST_CHECK_CLOSE(polygon.getArea(), area, ACCURACY);
  BOOST_CHECK_CLOSE(polygon.getFrameRect().width, frameRect.width, ACCURACY);
  BOOST_CHECK_CLOSE(polygon.getFrameRect().height, frameRect.height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(squareChangingOfAreaAfterScaling)
{
  const unsigned int size = 6;
  const ponomareva::point_t points[size] = { { -1, -2 }, { -3, 0 }, { 0, 6 }, { 3, 5 }, { 7, -1 }, { 3, -3 } };
  ponomareva::Polygon polygon(points, size);
  double area = polygon.getArea();
  double coef = 3;
  polygon.scale(coef);
  BOOST_CHECK_CLOSE(polygon.getArea(), area * coef * coef, ACCURACY);
  coef = 0.75;
  area = polygon.getArea();
  polygon.scale(coef);
  BOOST_CHECK_CLOSE(polygon.getArea(), area * coef * coef, ACCURACY);
}

BOOST_AUTO_TEST_CASE(unconvexPolygon)
{
  const unsigned int size = 4;
  ponomareva::point_t points[size] = { { 0, 1 }, { 3, 3 }, { 0, -3 }, { -3, 3 } };
  BOOST_CHECK_THROW(ponomareva::Polygon(points, size), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(polygonInvalidSize)
{
  const unsigned int size = 2;
  ponomareva::point_t points[size] = { { 0, 1 }, { 3, 3 } };
  BOOST_CHECK_THROW(ponomareva::Polygon(points, size), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(emptyPolygon)
{
  const unsigned int size = 5;
  BOOST_CHECK_THROW(ponomareva::Polygon(nullptr, size), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(polygonInvalidPoints)
{
  const unsigned int size = 5;
  ponomareva::point_t points[size] = { { -1, 0 }, { 0, 1 }, { 1, 2 }, { 2, 3 }, { 3, 4 } };
  BOOST_CHECK_THROW(ponomareva::Polygon(points, size), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()
