#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(testTriangle)

BOOST_AUTO_TEST_CASE(invariabilityOfParametersAfterMoving)
{
  ponomareva::Triangle triangle({ -4, -5 }, { 1, 3 }, { 9, -6 });
  ponomareva::rectangle_t frameRect = triangle.getFrameRect();
  double area = triangle.getArea();
  triangle.move(-8, 15);
  BOOST_CHECK_CLOSE(triangle.getArea(), area, ACCURACY);
  BOOST_CHECK_CLOSE(triangle.getFrameRect().width, frameRect.width, ACCURACY);
  BOOST_CHECK_CLOSE(triangle.getFrameRect().height, frameRect.height, ACCURACY);
  triangle.move({ -11, 7 });
  BOOST_CHECK_CLOSE(triangle.getArea(), area, ACCURACY);
  BOOST_CHECK_CLOSE(triangle.getFrameRect().width, frameRect.width, ACCURACY);
  BOOST_CHECK_CLOSE(triangle.getFrameRect().height, frameRect.height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(squareChangingOfAreaAfterScaling)
{
  ponomareva::Triangle triangle({ 1, 2 }, { 3, 9 }, { 7, 1 });
  double area = triangle.getArea();
  double coef = 2;
  triangle.scale(coef);
  BOOST_CHECK_CLOSE(triangle.getArea(), area * coef * coef, ACCURACY);
  coef = 0.5;
  area = triangle.getArea();
  triangle.scale(coef);
  BOOST_CHECK_CLOSE(triangle.getArea(), area * coef * coef, ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  BOOST_CHECK_THROW(ponomareva::Triangle({ -1, 0 }, { 1, 2 }, { 3, 4 }), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
