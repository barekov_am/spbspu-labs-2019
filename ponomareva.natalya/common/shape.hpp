#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace ponomareva
{
  class Shape
  {
  public:
    using ptr_type = std::shared_ptr<Shape>;
    using array_type = std::unique_ptr<ptr_type[]>;

    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void move(const point_t &pos) = 0;
    virtual void scale(double coef) = 0;
    virtual void rotate(double angle) = 0;
    virtual void printInfo() const = 0;
  };
}

#endif //SHAPE_HPP
