#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace ponomareva
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(double width, double height, const point_t &center);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &center) override;
    void move(double dx, double dy) override;
    void scale(double coef) override;
    void rotate(double angle) override;
    void printInfo() const override;

  private:
    double width_;
    double height_;
    point_t center_;
    double angle_;
  };
}

#endif //RECTANGLE_HPP
