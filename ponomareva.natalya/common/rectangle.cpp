#define _USE_MATH_DEFINES

#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

ponomareva::Rectangle::Rectangle(double width, double height, const point_t &center) :
  width_(width),
  height_(height),
  center_(center),
  angle_(0)
{
  if ((width <= 0.0) || (height <= 0.0))
  {
    throw std::invalid_argument("Width and height must be positive!");
  }
}

double ponomareva::Rectangle::getArea() const
{
  return width_ * height_;
}

ponomareva::rectangle_t ponomareva::Rectangle::getFrameRect() const
{
  const double ang = angle_ * M_PI / 180;
  const double cos = fabs(std::cos(ang));
  const double sin = fabs(std::sin(ang));
  double width = height_ * sin + width_ * cos;
  double height = height_ * cos + width_ * sin;
  return { width, height, center_ };
}

void ponomareva::Rectangle::move(const point_t &center)
{
  center_ = center;
}

void ponomareva::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void ponomareva::Rectangle::scale(double coef)
{
  if (coef > 0.0)
  {
    width_ *= coef;
    height_ *= coef;
  }
  else
  {
    throw std::invalid_argument("Coefficient must be positive");
  }
}

void ponomareva::Rectangle::rotate(double angle)
{
  angle_ += angle;
}

void ponomareva::Rectangle::printInfo() const
{
  std::cout << "This is a Rectangle.\n";
  std::cout << "Center: (" << center_.x << ", " << center_.y << ")\n";
  std::cout << "Width: " << width_ << "\nHeight: " << height_ << "\n";
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "Frame Rectangle:\n center: (" << getFrameRect().pos.x << ", " << getFrameRect().pos.y << ")\n";
  std::cout << " width: " << getFrameRect().width << "\n height: " << getFrameRect().height << "\n\n";
}
