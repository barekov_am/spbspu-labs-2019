#define _USE_MATH_DEFINES

#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

ponomareva::Triangle::Triangle(const point_t &pointA, const point_t &pointB, const point_t &pointC) :
  pointA_(pointA),
  pointB_(pointB),
  pointC_(pointC)
{
  if (getArea() <= 0.0)
  {
    throw std::invalid_argument("Vertices aren't correct!");
  }
}

double ponomareva::Triangle::getArea() const
{
  return fabs(((pointC_.x - pointA_.x) * (pointC_.y - pointB_.y)
    - (pointC_.y - pointA_.y) * (pointC_.x - pointB_.x)) / 2);
}

ponomareva::rectangle_t ponomareva::Triangle::getFrameRect() const
{
  double xMin = std::min(std::min(pointA_.x, pointB_.x), pointC_.x);
  double xMax = std::max(std::max(pointA_.x, pointB_.x), pointC_.x);
  double yMin = std::min(std::min(pointA_.y, pointB_.y), pointC_.y);
  double yMax = std::max(std::max(pointA_.y, pointB_.y), pointC_.y);
  return { xMax - xMin, yMax - yMin, { xMin + (xMax - xMin) / 2, yMin + (yMax - yMin) / 2 } };
}

void ponomareva::Triangle::move(const point_t& center)
{
  move(center.x - findCenter().x, center.y - findCenter().y);
}

void ponomareva::Triangle::move(double dx, double dy)
{
  pointA_.x += dx;
  pointB_.x += dx;
  pointC_.x += dx;
  pointA_.y += dy;
  pointB_.y += dy;
  pointC_.y += dy;
}

void ponomareva::Triangle::scale(double coef)
{
  if (coef > 0.0)
  {
    point_t center = findCenter();
    pointA_.x = pointA_.x * coef - center.x * (coef - 1);
    pointB_.x = pointB_.x * coef - center.x * (coef - 1);
    pointC_.x = pointC_.x * coef - center.x * (coef - 1);
    pointA_.y = pointA_.y * coef - center.y * (coef - 1);
    pointB_.y = pointB_.y * coef - center.y * (coef - 1);
    pointC_.y = pointC_.y * coef - center.y * (coef - 1);
  }
  else
  {
    throw std::invalid_argument("Coefficient must be positive");
  }
}

void ponomareva::Triangle::rotate(double angle)
{
  double ang = angle * M_PI / 180;
  double cos = std::cos(ang);
  double sin = std::sin(ang);

  point_t center = findCenter();
  pointA_ = { center.x + cos * (pointA_.x - center.x) - sin * (pointA_.y - center.y),
    center.y + cos * (pointA_.y - center.y) + sin * (pointA_.x - center.x) };
  pointB_ = { center.x + cos * (pointB_.x - center.x) - sin * (pointB_.y - center.y),
    center.y + cos * (pointB_.y - center.y) + sin * (pointB_.x - center.x) };
  pointC_ = { center.x + cos * (pointC_.x - center.x) - sin * (pointC_.y - center.y),
    center.y + cos * (pointC_.y - center.y) + sin * (pointC_.x - center.x) };
}

ponomareva::point_t ponomareva::Triangle::findCenter() const
{
  return { (pointA_.x + pointB_.x + pointC_.x) / 3, (pointA_.y + pointB_.y + pointC_.y) / 3 };
}

void ponomareva::Triangle::printInfo() const
{
  std::cout << "This is a Triangle.\n" << "Center: (" << findCenter().x << ", " << findCenter().y << ")\n";
  std::cout << "First vertice: (" << pointA_.x << ", " << pointA_.y << ")\n";
  std::cout << "Second vertice: (" << pointB_.x << ", " << pointB_.y << ")\n";
  std::cout << "Third vertice: (" << pointC_.x << ", " << pointC_.y << ")\n";
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "Frame Rectangle:\n center: (" << getFrameRect().pos.x << ", " << getFrameRect().pos.y << ")\n";
  std::cout << " width: " << getFrameRect().width << "\n height: " << getFrameRect().height << "\n\n";
}
