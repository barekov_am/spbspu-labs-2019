#define _USE_MATH_DEFINES

#include "polygon.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

ponomareva::Polygon::Polygon(const point_t *points, unsigned int amount) :
  points_(new point_t[amount]),
  amount_(amount)
{
  if (amount_ < 3)
  {
    delete[] points_;
    throw std::invalid_argument("There must be more than 2 vertices!");
  }
  if (points == nullptr)
  {
    delete[] points_;
    throw std::invalid_argument("Pointer to vertice mustn't be null!");
  }
  for (unsigned int i = 0; i < amount_; i++)
  {
    points_[i] = points[i];
  }
  if (getArea() <= 0)
  {
    delete[] points_;
    throw std::invalid_argument("Polygon's area must be positive!");
  }
  if (checkConvexity() == false)
  {
    delete[] points_;
    throw std::invalid_argument("Polygon is not convex!");
  }
}

ponomareva::Polygon::Polygon(const Polygon &other) :
  points_(new point_t[other.amount_]),
  amount_(other.amount_)
{
  for (unsigned int i = 0; i < amount_; i++)
  {
    points_[i] = other.points_[i];
  }
}

ponomareva::Polygon::Polygon(Polygon &&other) :
  points_(other.points_),
  amount_(other.amount_)
{
  other.points_ = nullptr;
  other.amount_ = 0;
}

ponomareva::Polygon::~Polygon()
{
  delete[] points_;
}

ponomareva::Polygon& ponomareva::Polygon::operator =(const Polygon& other)
{
  if (this == &other)
  {
    return *this;
  }
  amount_ = other.amount_;
  if (points_ != nullptr)
  {
    delete[] points_;
  }
  points_ = new point_t[amount_];
  for (unsigned int i = 0; i < amount_; i++)
  {
    points_[i] = other.points_[i];
  }
  return *this;
}

ponomareva::Polygon& ponomareva::Polygon::operator =(Polygon&& other)
{
  if (this == &other)
  {
    return *this;
  }
  amount_ = other.amount_;
  other.amount_ = 0;
  if (points_ != nullptr)
  {
    delete[] points_;
  }
  points_ = other.points_;
  other.points_ = nullptr;
  return *this;
}

double ponomareva::Polygon::getArea() const
{
  double sum = points_[amount_ - 1].x * points_[0].y - points_[0].x * points_[amount_ - 1].y;
  for (unsigned int i = 0; i < amount_ - 1; i++)
  {
    sum += points_[i].x * points_[i + 1].y - points_[i + 1].x * points_[i].y;
  }
  return 0.5 * fabs(sum);
}

ponomareva::rectangle_t ponomareva::Polygon::getFrameRect() const
{
  point_t min = points_[0];
  point_t max = points_[0];
  for (unsigned int i = 1; i < amount_; i++)
  {
    min.x = std::min(min.x, points_[i].x);
    max.x = std::max(max.x, points_[i].x);
    min.y = std::min(min.y, points_[i].y);
    max.y = std::max(max.y, points_[i].y);
  }
  return { max.x - min.x, max.y - min.y, { min.x + (max.x - min.x) / 2, min.y + (max.y - min.y) / 2 } };
}

void ponomareva::Polygon::move(const point_t& center)
{
  move(center.x - findCenter().x, center.y - findCenter().y);
}

void ponomareva::Polygon::move(double dx, double dy)
{
  for (unsigned int i = 0; i < amount_; i++)
  {
    points_[i].x += dx;
    points_[i].y += dy;
  }
}

void ponomareva::Polygon::scale(double coef)
{
  if (coef > 0.0)
  {
    point_t center = findCenter();
    for (unsigned int i = 0; i < amount_; i++)
    {
      points_[i].x = center.x + coef * (points_[i].x - center.x);
      points_[i].y = center.y + coef * (points_[i].y - center.y);
    }
  }
  else
  {
    throw std::invalid_argument("Coefficient must be positive");
  }
}

void ponomareva::Polygon::rotate(double angle)
{
  double cos = std::cos(angle * M_PI / 180);
  double sin = std::sin(angle * M_PI / 180);
  point_t center = findCenter();
  for (unsigned int i = 0; i < amount_; i++)
  {
    points_[i] = { center.x + cos * (points_[i].x - center.x) - sin * (points_[i].y - center.y),
    center.y + cos * (points_[i].y - center.y) + sin * (points_[i].x - center.x) };
  }
}

bool ponomareva::Polygon::checkConvexity() const
{
  point_t vector1 = { 0.0, 0.0 };
  point_t vector2 = { 0.0, 0.0 };
  unsigned int posQnty = 0;
  unsigned int negQnty = 0;
  for (unsigned int i = 0; i < amount_; i++)
  {
    int k = (i + 1) % amount_;
    vector1.x = points_[k].x - points_[i].x;
    vector1.y = points_[k].y - points_[i].y;
    vector2.x = points_[(i + 2) % amount_].x - points_[k].x;
    vector2.y = points_[(i + 2) % amount_].y - points_[k].y;
    if ((vector1.x * vector2.y - vector1.y * vector2.x) > 0.0)
    {
      posQnty++;
    }
    else
    {
      if ((vector1.x * vector2.y - vector1.y * vector2.x) < 0.0)
      {
        negQnty++;
      }
    }
  }
  if (((negQnty == 0) && (posQnty == amount_)) || ((posQnty == 0) && (negQnty == amount_)))
  {
    return true;
  }
  return false;
}

ponomareva::point_t ponomareva::Polygon::findCenter() const
{
  point_t center = { 0.0, 0.0 };
  for (unsigned int i = 0; i < amount_; i++)
  {
    center.x += points_[i].x;
    center.y += points_[i].y;
  }
  return { center.x / amount_, center.y / amount_ };
}

void ponomareva::Polygon::printInfo() const
{
  std::cout << "This is a Polygon.\n" << "Center: (" << findCenter().x << ", " << findCenter().y << ")\n";
  std::cout << "Vertices' amount: " << amount_ << "\n";
  for (unsigned int i = 0; i < amount_; i++)
  {
    std::cout << "Vertice " << i + 1 << ": (" << points_[i].x << ", " << points_[i].y << ")\n";
  }
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "Frame Rectangle:\n center : (" << getFrameRect().pos.x << ", " << getFrameRect().pos.y << ")\n";
  std::cout << " width: " << getFrameRect().width << "\n height: " << getFrameRect().height << "\n\n";
}
