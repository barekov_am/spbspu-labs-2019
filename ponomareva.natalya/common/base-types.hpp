#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace ponomareva
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    double width;
    double height;
    point_t pos;
  };

  struct pair_t
  {
    int line;
    int column;
  };
}

#endif //BASE_TYPES_HPP
