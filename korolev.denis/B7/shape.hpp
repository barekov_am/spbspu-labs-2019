#ifndef B7_SHAPE_HPP
#define B7_SHAPE_HPP

#include <stdexcept>
#include <memory>
#include <vector>

struct point_t {
  int x;
  int y;
};

class Shape {
public:
  Shape(const point_t& pos);

  bool isMoreLeft(const Shape* shape) const;
  bool isUpper(const Shape* shape) const;
  virtual void draw(std::ostream& output) const = 0;

protected:
  point_t pos_;
};

typedef std::shared_ptr<Shape> shapePtr;
void printShapes(std::ostream& stream, const std::vector<shapePtr>& shapes);

#endif //B7_SHAPE_HPP

