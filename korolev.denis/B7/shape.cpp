#include "shape.hpp"

#include <algorithm>

Shape::Shape(const point_t& pos) :
  pos_(pos)
{
}

bool Shape::isMoreLeft(const Shape* shape) const {
  return (pos_.x < shape->pos_.x);
}

bool Shape::isUpper(const Shape* shape) const {
  return (pos_.y > shape->pos_.y);
}

void printShapes(std::ostream& stream, const std::vector<shapePtr>& shapes) {
  std::for_each(shapes.begin(), shapes.end(), [&](const auto& shape){ shape->draw(stream); });
}


