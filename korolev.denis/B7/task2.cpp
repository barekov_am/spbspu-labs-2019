#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

#include "shape.hpp"
#include "parser.hpp"

void task2() {
  std::vector<shapePtr> shapes;
  std::string line;

  while (std::getline(std::cin, line)) {
    line.erase(std::remove_if(line.begin(), line.end(),
        [](const auto& elem){ return std::isspace(elem); }), line.end());

    if (line.empty()) {
      continue;
    }

    shapes.push_back(parser::readShape(line));
  }

  std::cout << "Original:" << "\n";
  printShapes(std::cout, shapes);

  std::cout << "Left-Right:" << "\n";
  std::sort(shapes.begin(), shapes.end(),
      [](const auto& lhs, const auto& rhs){ return lhs->isMoreLeft(rhs.get()); });
  printShapes(std::cout, shapes);

  std::cout << "Right-Left:" << "\n";
  std::sort(shapes.begin(), shapes.end(),
      [](const auto& lhs, const auto& rhs){ return !(lhs->isMoreLeft(rhs.get())); });
  printShapes(std::cout, shapes);

  std::cout << "Top-Bottom:" << "\n";
  std::sort(shapes.begin(), shapes.end(),
      [](const auto& lhs, const auto& rhs){ return lhs->isUpper(rhs.get()); });
  printShapes(std::cout, shapes);

  std::cout << "Bottom-Top:" << "\n";
  std::sort(shapes.begin(), shapes.end(),
      [](const auto& lhs, const auto& rhs){ return !(lhs->isUpper(rhs.get())); });
  printShapes(std::cout, shapes);
}


