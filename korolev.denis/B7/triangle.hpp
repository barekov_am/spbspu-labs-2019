#ifndef B7_TRIANGLE_HPP
#define B7_TRIANGLE_HPP

#include "shape.hpp"

class Triangle: public Shape {
public:
  Triangle(const point_t& pos);

  void draw(std::ostream& output) const override;
};

#endif //B7_TRIANGLE_HPP
