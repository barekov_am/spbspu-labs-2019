#include "circle.hpp"

#include <iostream>

Circle::Circle(const point_t& pos) :
  Shape(pos)
{
}

void Circle::draw(std::ostream& output) const {
  output << "CIRCLE" << " (" << pos_.x << ";" << pos_.y << ")" << "\n";
}

