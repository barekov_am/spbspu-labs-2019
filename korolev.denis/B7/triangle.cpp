#include "triangle.hpp"

#include <iostream>

Triangle::Triangle(const point_t& pos) :
  Shape(pos)
{
}

void Triangle::draw(std::ostream& output) const {
  output << "TRIANGLE" << " (" << pos_.x << ";" << pos_.y << ")" << "\n";
}


