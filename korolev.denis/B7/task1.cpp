#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <iterator>

void task1()
{
  std::vector<double> vector;
  transform((std::istream_iterator<double>(std::cin)), std::istream_iterator<double>(),
      std::back_inserter(vector), [](const double& elem){ return elem * M_PI; });

  if (!std::cin.eof()) {
    throw std::ios_base::failure("Invalid input data reading");
  }

  std::copy(vector.begin(), vector.end(), std::ostream_iterator<double>(std::cout, " "));
}


