#include "square.hpp"

#include <iostream>

Square::Square(const point_t& pos) :
  Shape(pos)
{
}

void Square::draw(std::ostream& output) const {
  output << "SQUARE" << " (" << pos_.x << ";" << pos_.y << ")" << "\n";
}


