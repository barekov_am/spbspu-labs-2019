#ifndef B7_CIRCLE_HPP
#define B7_CIRCLE_HPP

#include "shape.hpp"

class Circle: public Shape {
public:
  Circle(const point_t& pos);

  void draw(std::ostream& output) const override;
};

#endif //B7_CIRCLE_HPP


