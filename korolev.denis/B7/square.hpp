#ifndef B7_SQUARE_HPP
#define B7_SQUARE_HPP

#include "shape.hpp"

class Square: public Shape {
public:
  Square(const point_t& pos);

  void draw(std::ostream& output) const override;
};

#endif //B7_SQUARE_HPP
