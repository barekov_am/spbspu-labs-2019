#include "parser.hpp"

#include <string>

#include "square.hpp"
#include "circle.hpp"
#include "triangle.hpp"

shapePtr parser::readShape(std::string& line) {
  std::size_t openingBrake = line.find_first_of('(');
  std::string shapeStr = line.substr(0, openingBrake);

  std::size_t semicolon = line.find_first_of(';');
  std::size_t closingBrake = line.find_first_of(')');

  if (semicolon == std::string::npos || closingBrake == std::string::npos ||
      openingBrake == std::string::npos || semicolon <= openingBrake || semicolon >= closingBrake) {
    throw std::invalid_argument("Invalid input data reading");
  }

  int x = std::stoi(line.substr(openingBrake + 1, semicolon - (openingBrake + 1)));
  int y = std::stoi(line.substr(semicolon + 1, closingBrake - (semicolon + 1)));
  line.erase(0, closingBrake + 1);

  if (!line.empty()) {
    throw std::invalid_argument("Invalid input data reading");
  }

  if (shapeStr == "CIRCLE") {
    return std::make_shared<Circle>(Circle({ x, y }));
  } else if (shapeStr == "SQUARE") {
    return std::make_shared<Square>(Square({ x, y }));
  } else if (shapeStr == "TRIANGLE") {
    return std::make_shared<Triangle>(Triangle({ x, y }));
  } else {
    throw std::invalid_argument("Invalid input data reading");
  }
}


