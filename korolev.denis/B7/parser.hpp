#ifndef B7_PARSER_HPP
#define B7_PARSER_HPP

#include "shape.hpp"

namespace parser {
  shapePtr readShape(std::string& line);
};

#endif //B7_PARSER_HPP


