#include <iostream>

#include "circle.hpp"
#include "rectangle.hpp"
#include "base-types.hpp"
#include "composite-shape.hpp"

void printInf(const korolev::Shape & shape)
{
  korolev::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "Parameters of shape: " << std::endl;
  std::cout << "Center: (" << frameRect.pos.x << ";" << frameRect.pos.y << ")" << std::endl;
  std::cout << "Frame width: " << frameRect.width << std::endl;
  std::cout << "Frame height: " << frameRect.height << std::endl;
  std::cout << "Area: " << shape.getArea() << std::endl;
}

void movePoint(korolev::Shape & shape, double x, double y)
{
  printInf(shape);
  shape.move({ x, y });
  std::cout << "Parameters of shape after move: " << std::endl;
  printInf(shape);
}

void moveXY(korolev::Shape & shape, double dx, double dy)
{
  printInf(shape);
  shape.move(dx, dy);
  std::cout << "Parameters of shape after move: " << std::endl;
  printInf(shape);
}

void scale(korolev::Shape & shape, double coefficient)
{
  printInf(shape);
  shape.scale(coefficient);
  std::cout << "Parameters of shape after scale: " << std::endl;
  printInf(shape);
}

int main()
{
  korolev::Shape::pointer circle1Ptr = std::make_shared<korolev::Circle>(korolev::point_t{ -9.5, 12 }, 1.7);
  korolev::Shape::pointer circle2Ptr = std::make_shared<korolev::Circle>(korolev::point_t{ 3.5, -5.2 }, 6.3);
  korolev::Shape::pointer rectangle1Ptr = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.9, -3.4 }, 3 , 7.8);
  korolev::Shape::pointer rectangle2Ptr = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -14.2 }, 2.3 , 5.6);

  korolev::CompositeShape composite;

  composite.add(circle1Ptr);
  printInf(composite);

  composite.add(circle2Ptr);
  movePoint(composite, 12, 2.4);

  composite.add(rectangle1Ptr);
  moveXY(composite, 2.6, 4.3);

  composite.add(rectangle2Ptr);
  scale(composite, 2.3);

  composite.remove(1);
  movePoint(composite, -0.3, 3.5);

  return 0;
}

