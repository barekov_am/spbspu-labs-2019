#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

void task1()
{
  std::vector<std::string> words;
  std::string line;

  while (std::getline(std::cin, line)) {

    while (line.find_first_of(" \n\t") == 0) {
      line.erase(0, 1);
    }

    while (!line.empty()) {
      size_t position = line.find_first_of(" \n\t");

      std::string word;
      word = line.substr(0, position);
      words.push_back(word);

      line.erase(0, position);
      while (line.find_first_of(" \n\t") == 0) {
        line.erase(0, 1);
      }
    }
  }

  std::sort(words.begin(), words.end());
  words.erase(std::unique(words.begin(), words.end()), words.end());

  for (auto i = words.begin(); i!= words.end(); ++i)
  {
    std::cout << *i << "\n";
  }
}
