#include "shape-container.hpp"

void task2() {
  Shapes shapes;

  shapes.readShapeContainer();
  shapes.counting();
  shapes.printCountOfVertices();
  shapes.printCountOfTriangle();
  shapes.printCountOfSquares();
  shapes.printCountOfRectangles();
  shapes.erasePentagons();
  shapes.createVectorPoint();
  shapes.changeShapeContainer();
  shapes.printShapeContainer();
}

