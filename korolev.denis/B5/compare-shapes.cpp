#include "compare-shapes.hpp"

bool CompareShapes::operator()(const point::shape& lhs, const point::shape& rhs) {
  if (lhs.size() < rhs.size()) {
    return true;
  }
  std::size_t vertices = 4;
  if ((lhs.size() == vertices) && (rhs.size() == vertices))
  {
    if (isSquare(lhs))
    {
      return !isSquare(rhs);
    }
  }
  return false;
}
