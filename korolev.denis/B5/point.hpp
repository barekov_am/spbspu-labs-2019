#ifndef B5_POINT_H
#define B5_POINT_H

#include <vector>
#include <stdexcept>

namespace point
{
  struct point_t {
    int x, y;
  };

  using shape = std::vector<point_t>;

  void printShape(const shape& shape);
  int getDistance(const point_t& lhs, const point::point_t& rhs);
  bool isSquare(const shape& shape);
  bool isRectangle(const shape &shape);
  shape readPoints(std::string& points);
}

#endif //B5_POINT_H
