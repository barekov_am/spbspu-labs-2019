#ifndef B5_ANALYSE_SHAPE_HPP
#define B5_ANALYSE_SHAPE_HPP

#include <functional>
#include "shape-container.hpp"

struct analyseShape
{
  void operator ()(const point::shape &shape);

  std::size_t vertices_ = 0;
  std::size_t triangles_ = 0;
  std::size_t squares_ = 0;
  std::size_t rectangles_ = 0;
};

#endif //B5_ANALYSE_SHAPE_HPP

