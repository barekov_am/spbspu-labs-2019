#include <algorithm>
#include <iostream>
#include <string>

#include "point.hpp"

void point::printShape(const point::shape& shape) {
  std::for_each(shape.begin(), shape.end(),
      [&](const auto& point){ std::cout << " (" << point.x << ";" << point.y << ")"; });
}

int point::getDistance(const point::point_t& lhs, const point::point_t& rhs) {
  return (lhs.x - rhs.x)*(lhs.x - rhs.x) + (lhs.y - rhs.y)*(lhs.y - rhs.y);
}

bool point::isSquare(const point::shape& shape) {
  if ((getDistance(shape.at(0), shape.at(1))) != (getDistance(shape.at(1), shape.at(2)))) {
    return false;
  } else if ((getDistance(shape.at(1), shape.at(2))) != (getDistance(shape.at(2), shape.at(3)))) {
    return false;
  } else if ((getDistance(shape.at(2), shape.at(3))) != (getDistance(shape.at(3), shape.at(0)))) {
    return false;
  } else if ((getDistance(shape.at(0), shape.at(2))) != (getDistance(shape.at(1), shape.at(3)))) {
    return false;
  }
  return true;
}

bool point::isRectangle(const point::shape &shape) {
  return (getDistance(shape.at(0), shape.at(2))) == (getDistance(shape.at(1), shape.at(3)));
}

point::shape point::readPoints(std::string& points) {

  point::shape shape;

  while (!points.empty()) {
    while (points.find_first_of(" \t") == 0) {
      points.erase(0, 1);
    }
    std::size_t openBrakePosition = points.find_first_of('(');
    std::size_t semicolonPosition = points.find_first_of(';');
    std::size_t closeBrakePosition = points.find_first_of(')');

    if ((openBrakePosition == std::string::npos) || (semicolonPosition == std::string::npos)
        || (closeBrakePosition == std::string::npos) || (openBrakePosition != 0)
        || (openBrakePosition > semicolonPosition) || (closeBrakePosition < semicolonPosition)) {
      throw std::invalid_argument("Invalid point");
    }

    int x = std::stoi(points.substr(1, semicolonPosition - 1));
    int y = std::stoi(points.substr(semicolonPosition + 1,
        closeBrakePosition - semicolonPosition - 1));

    points.erase(0, closeBrakePosition + 1);

    while (points.find_first_of(" \t") == 0) {
      points.erase(0, 1);
    }

    shape.push_back({x, y});
  }
  return shape;
}


