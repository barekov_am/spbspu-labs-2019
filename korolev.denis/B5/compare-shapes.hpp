#ifndef B5_COMPARESHAPES_HPP
#define B5_COMPARESHAPES_HPP

#include "point.hpp"

struct CompareShapes
{
  bool operator()(const point::shape& lhs, const point::shape& rhs);
};

#endif //B5_COMPARESHAPES_HPP
