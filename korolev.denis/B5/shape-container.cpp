#include "shape-container.hpp"

#include <algorithm>
#include <stdexcept>
#include <iostream>

#include "compare-shapes.hpp"
#include "analyse-shape.hpp"

Shapes::Shapes():
  squares_(0),
  rectangles_(0),
  triangles_(0),
  vertices_(0)
{
}

void Shapes::readShapeContainer() {

  std::string line;
  point::shape shape;

  while (std::getline(std::cin, line)) {
    while (line.find_first_of(" \t") == 0)
    {
      line.erase(0, 1);
    }

    std::size_t position = line.find_first_of(" \t");
    if (position == std::string::npos)
    {
      continue;
    }

    int verticals = std::stoi(line.substr(0, position));
    line.erase(0, position + 1);

    shape = point::readPoints(line);
    if ((verticals > 1) && (static_cast<unsigned int>(verticals) != shape.size())) {
      throw std::invalid_argument("Invalid command");
    }
    shapeContainer.push_back(shape);
    shape.clear();
  }
}

void Shapes::counting() {
  analyseShape analyse = std::for_each(shapeContainer.begin(),
      shapeContainer.end(), analyseShape());
  triangles_ = analyse.triangles_;
  rectangles_ = analyse.rectangles_;
  squares_ = analyse.squares_;
  vertices_ = analyse.vertices_;
}

void Shapes::printCountOfRectangles() const {
  std::cout << "Rectangles: " << rectangles_ << "\n";
}

void Shapes::printCountOfTriangle() const {
  std::cout << "Triangles: " << triangles_ << "\n";
}

void Shapes::printCountOfSquares() const {
  std::cout << "Squares: " << squares_ << "\n";
}

void Shapes::printCountOfVertices() const {
  std::cout << "Vertices: " << vertices_ << "\n";
}

void Shapes::erasePentagons() {
  shapeContainer.erase(std::remove_if(shapeContainer.begin(), shapeContainer.end(),
      [](const point::shape &sh) { return sh.size() == 5; }), shapeContainer.end());
}

void Shapes::createVectorPoint() const {
  point::shape vectorPoint;
  std::for_each(shapeContainer.begin(), shapeContainer.end(),
      [&](const auto& shape){ vectorPoint.push_back(shape.at(0)); });

  std::cout << "Points:";
  std::for_each(vectorPoint.begin(), vectorPoint.end(),
      [&](const auto& point){ std::cout << " (" << point.x << ";" << point.y << ")"; });
  std::cout << "\n";
}

void Shapes::changeShapeContainer() {
  std::sort(shapeContainer.begin(), shapeContainer.end(), CompareShapes());
}

void Shapes::printShapeContainer() const {
  std::cout << "Shapes: " << "\n";
  for (auto iter = shapeContainer.begin(); iter != shapeContainer.end(); ++iter) {
    std::cout << iter->size();
    point::printShape(*iter);
    std::cout << "\n";
  }
}



