#ifndef B5_SHAPE_CONTAINER_H
#define B5_SHAPE_CONTAINER_H

#include <string>

#include "point.hpp"

class Shapes
{
public:

  Shapes();

  void readShapeContainer();
  void counting();

  void printCountOfVertices() const;
  void printCountOfSquares() const;
  void printCountOfRectangles() const;
  void printCountOfTriangle() const;

  void erasePentagons();
  void createVectorPoint() const;
  void changeShapeContainer();

  void printShapeContainer() const;

private:
  std::vector<point::shape> shapeContainer;

  std::size_t squares_;
  std::size_t rectangles_;
  std::size_t triangles_;
  std::size_t vertices_;
};

#endif //B5_SHAPE_CONTAINER_H

