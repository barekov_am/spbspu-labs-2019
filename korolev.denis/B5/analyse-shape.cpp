#include "analyse-shape.hpp"
#include "point.hpp"

void analyseShape::operator ()(const point::shape &shape)
{
  size_t num = shape.size();
  vertices_ += num;
  switch (num)
  {
    case 3:
    {
      triangles_++;
      break;
    }

    case 4:
    {
      if (isRectangle(shape))
      {
        rectangles_++;
        if (isSquare(shape))
        {
          squares_++;
        }
      }
      break;
    }
  }
}


