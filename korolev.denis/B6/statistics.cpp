#include <iostream>

#include "statistics.hpp"

Statistics::Statistics() :
  maxNumber_(0),
  minNumber_(0),
  meanNumber_(0),
  oddSum_(0),
  evenSum_(0),
  positive_(0),
  negative_(0),
  nullCount_(0),
  firstLastEqual_(false),
  first_(0),
  empty_(true)
{
}

void Statistics::operator()(const int& number) {
  if (empty_) {
    first_ = number;
    minNumber_ = number;
    maxNumber_ = number;
    empty_ = false;
  }

  if (number > maxNumber_) {
    maxNumber_ = number;
  }
  if (number < minNumber_) {
    minNumber_ = number;
  }

  (number % 2 == 0) ? (evenSum_ += number) : (oddSum_ += number);

  if (number > 0) {
    positive_ += 1;
  } else if (number < 0) {
    negative_ += 1;
  } else {
    nullCount_++;
  }

  meanNumber_ = static_cast<double>(oddSum_ + evenSum_) / (positive_ + negative_ + nullCount_);
  (first_ == number) ? (firstLastEqual_ = true) : (firstLastEqual_ = false);
}

void Statistics::print() {
  if (empty_) {
    std::cout << "No Data" << "\n";
  } else {
    std::cout << "Max: " << maxNumber_ << "\n";
    std::cout << "Min: " << minNumber_ << "\n";
    std::cout << "Mean: " << meanNumber_ << "\n";
    std::cout << "Positive: " << positive_ << "\n";
    std::cout << "Negative: " << negative_ << "\n";
    std::cout << "Odd Sum: " << oddSum_ << "\n";
    std::cout << "Even Sum: " << evenSum_ << "\n";
    std::cout << "First/Last Equal: ";
    
    if (firstLastEqual_) {
      std::cout << "yes" << "\n";
    } else {
      std::cout << "no" << "\n";
    }
  }
}


