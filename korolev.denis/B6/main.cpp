#include <iostream>
#include <stdexcept>

void task();

int main() {
  try {
    task();
  }
  catch (const std::exception& e) {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}

