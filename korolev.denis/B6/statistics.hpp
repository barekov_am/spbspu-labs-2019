#ifndef B6_STATISTICS_HPP
#define B6_STATISTICS_HPP

#include <stdexcept>

class Statistics
{
public:
  Statistics();
  void print();

  void operator()(const int& number);

private:
  int maxNumber_;
  int minNumber_;
  double meanNumber_;
  long long int oddSum_;
  long long int evenSum_;
  int positive_;
  int negative_;
  int nullCount_;
  bool firstLastEqual_;
  int first_;
  bool empty_;
};

#endif //B6_STATISTICS_HPP

