#include <stdexcept>
#include <iostream>
#include <vector>

#include "sort.hpp"

void task3()
{
  std::vector<int> vector;
  int num = -1;

  while(std::cin >> num)
  {
    if (num == 0)
    {
      break;
    }
    vector.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Reading failed");
  }

  if (vector.empty())
  {
    return;
  }

  if (num != 0)
  {
    throw std::runtime_error("Zero is not met");
  }

  switch(vector.back())
  {
    case 1:
    {
      auto i = vector.begin();

      while (i != vector.end())
      {
        if (*i % 2 == 0)
        {
          i = vector.erase(i);
          continue;
        }
        ++i;
      }
      break;
    }

    case 2:
    {
      auto i = vector.begin();

      while (i != vector.end())
      {
        if ((*i % 3 == 0))
        {
          i = vector.insert(++i, 3, 1) + 3;
          continue;
        }
        ++i;
      }
      break;
    }

    default:
    {
      break;
    }
  }

  print(vector);
}

