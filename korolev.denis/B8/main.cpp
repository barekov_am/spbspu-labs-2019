#include <iostream>
#include <vector>
#include <iterator>
#include <cstring>

#include "parser.hpp"

const int MIN_WIDTH = 25;

int main(int argc, char * argv[]) {
  try {
    if (argc > 3 || argc == 2) {
      throw std::invalid_argument("Invalid number of argument");
    }

    std::size_t width = 40;

    if (argc == 3) {
      if (std::strcmp(argv[1], "--line-width") != 0) {
        throw std::invalid_argument("Invalid argument");
      }
      char *ptr;

      width = std::strtoul(argv[2], &ptr, 10);

      if (width < MIN_WIDTH) {
        throw std::invalid_argument("Invalid argument");
      }

      if (ptr == nullptr) {
        throw std::invalid_argument("Invalid argument");
      }
    }

    Parser text(width);
    text.readText(std::cin);
    text.format();

  } catch (const std::exception &exception) {
    std::cerr << exception.what() << '\n';
    return 1;
  }

}


