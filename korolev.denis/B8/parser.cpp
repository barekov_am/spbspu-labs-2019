#include "parser.hpp"

#include <cctype>
#include <iostream>

const size_t MAXWORDSIZE = 20;
const size_t MAXNUMBERSIZE = 20;

Parser::Parser(std::size_t width) :
  width_(width)
{
}

void Parser::readText(std::istream& input) {

  while (!input.eof()) {
    char sign = input.get();

    if (std::isdigit(sign) || ((sign == '-' || sign == '+') && std::isdigit(input.peek()))) {
      elements_.push_back(readNumber(sign, input));
    } else if (std::isalpha(sign)) {
      elements_.push_back(readWord(sign, input));
    } else if (std::ispunct(sign)) {

      if (elements_.empty()) {
        throw std::invalid_argument("Begin with punctuation");
      }

      std::string prevPunct = readPunctuation(sign, input);

      if (!elements_.empty() && !(elements_.back() == "," && (prevPunct == "-" || prevPunct == "---")) &&
          std::ispunct(prevPunct[0]) && isPunct(elements_.back())) {
        throw std::invalid_argument("More than one punctuation nearly");
      }

      elements_.push_back(prevPunct);
    }

    while (std::isspace(input.peek())) {
      input.get();
    }
  }
}

std::string Parser::readWord(char firstElem, std::istream& input) {
  std::string word;
  word.push_back(firstElem);

  while (input.peek() == '-' || std::isalpha(input.peek())) {
    word.push_back(input.get());

    if (word.back() == '-' && input.peek() == '-') {
      throw std::invalid_argument("More than one '-' nearly");
    }
  }

  if (word.size() > MAXWORDSIZE) {
    throw std::invalid_argument("Size of word more than 20");
  }

  return word;
}

std::string Parser::readNumber(char firstElem, std::istream& input) {
  std::string number;
  number.push_back(firstElem);

  std::size_t countOfPoint = 0;

  while (std::isdigit(input.peek()) || input.peek() == '.') {

    if (input.peek() == '.') {
      countOfPoint++;
    }

    if (countOfPoint > 1) {
      throw std::invalid_argument("Count of point more 1");
    }

    if (input.peek() == '-') {
      throw std::invalid_argument("Count of point more 1");
    }

    number.push_back(input.get());
  }

  if (number.size() > MAXNUMBERSIZE) {
    throw std::invalid_argument("Size of number more than 20");
  }

  return number;
}

std::string Parser::readPunctuation(char firstElem, std::istream& input) {
  std::string punctuation;
  punctuation.push_back(firstElem);

  if (firstElem == '-') {
    if (std::isalpha(input.peek())) {
      throw std::invalid_argument("Word can not begin with hyphen");
    }

    while (input.peek() == '-') {
      punctuation.push_back(input.get());
    }
  }

  if (punctuation.size() != 1 && punctuation.size() != 3) {
    throw std::invalid_argument("Count of hyphen more 1");
  }

  return punctuation;
}

void Parser::format() {
  std::string prevElem;
  std::string currentElem;
  std::string line;
  std::size_t space;

  for (std::size_t i = 0; i < elements_.size(); i++) {
    currentElem = elements_.at(i);
    space = calcSpace(currentElem);

    if (line.size() + currentElem.size() + space > width_) {
      if (isPunct(currentElem)) {
        std::size_t prevSpace = calcSpace(prevElem);
        line.erase(line.size() - prevElem.size() - prevSpace, prevElem.size() + prevSpace);
        std::cout << line << "\n";

        line.clear();
        line += prevElem;

      } else {
        std::cout << line << "\n";
        line.clear();
      }
    }

    if (!line.empty() && space == 1) {
      line.push_back(' ');
    }

    line += currentElem;
    prevElem = currentElem;
    currentElem.clear();
  }

  std::cout << line << "\n";
}

bool isPunct(const std::string& punct) {
  for (auto& i : punct) {
    if (!std::ispunct(i)){
      return false;
    }
  }

  return true;
}

std::size_t calcSpace(const std::string& line) {
  return !(isPunct(line) && line != "-" && line != "---") ? 1 : 0;
}


