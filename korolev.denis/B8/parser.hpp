#ifndef B8_PARSER_HPP
#define B8_PARSER_HPP

#include <vector>
#include <string>
#include <istream>

class Parser {
public:
  Parser(std::size_t width);

  void readText(std::istream& input);
  void format();

private:

  std::string readWord(char firstElem, std::istream& input);
  std::string readNumber(char firstElem, std::istream& input);
  std::string readPunctuation(char firstElem, std::istream& input);

  std::size_t width_;
  std::vector<std::string> elements_;
};

bool isPunct(const std::string& punct);
std::size_t calcSpace(const std::string& punct);


#endif //B8_PARSER_HPP


