#include "phone-book.hpp"

PhoneBook::listIterator PhoneBook::begin()
{
  return recordsList_.begin();
}

PhoneBook::listIterator PhoneBook::end()
{
  return recordsList_.end();
}

PhoneBook::listIterator PhoneBook::next(listIterator position)
{
  if (std::next(position) == recordsList_.end())
  {
    return position;
  }
  else
  {
    return ++position;
  }
}

PhoneBook::listIterator PhoneBook::prev(listIterator position)
{
  if (position == recordsList_.begin()) {
    return position;
  }
  else
  {
    return --position;
  }
}

PhoneBook::listIterator PhoneBook::move(listIterator position, int steps)
{
  if (steps >= 0)
  {
    while (std::next(position) != recordsList_.end() && (steps > 0))
    {
      position = next(position);
      --steps;
    }
  }
  else
  {
    while (position != recordsList_.begin() && (steps < 0))
    {
      position = prev(position);
      ++steps;
    }
  }

  return position;
}

void PhoneBook::pushBack(const record_t& record)
{
  recordsList_.push_back(record);
}

PhoneBook::listIterator PhoneBook::insert(listIterator position, const record_t& record)
{
  return recordsList_.insert(position, record);
}

PhoneBook::listIterator PhoneBook::erase(listIterator position)
{
  return recordsList_.erase(position);
}

bool PhoneBook::empty() const
{
  return recordsList_.empty();
}


