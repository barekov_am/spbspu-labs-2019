#include "commands.hpp"

#include <algorithm>
#include <iostream>
#include <cctype>

void Commands::skipSpaces(std::istream& input)
{
  while (input.peek() == ' ')
  {
    input.get();
  }
}

std::string Commands::readBookmark(std::istream& input)
{
  skipSpaces(input);
  std::string bookmark;
  input >> bookmark;

  auto iter = std::find_if(bookmark.begin(), bookmark.end(),
      [&](auto character){ return (!std::isalnum(character) && character != '-'); });

  if (iter != bookmark.end())
  {
    return "";
  }

  return bookmark;
}

std::string Commands::readNumber(std::istream& input)
{
  skipSpaces(input);
  std::string number;
  input >> number;

  auto iter = std::find_if(number.begin(), number.end(),
      [&](auto digit) { return !isdigit(digit); });

  if (iter != number.end())
  {
    return "";
  }

  return number;
}

std::string Commands::readName(std::istream& input)
{
  skipSpaces(input);

  if (input.peek() != '"')
  {
    return "";
  }

  input.get();
  std::string name;
  std::string tmp;

  while (std::getline(input, tmp, '"'))
  {
    name += tmp;

    if (name.back() != '\\')
    {
      break;
    }

    name.pop_back();
    name.push_back('"');
  }

  if (!input)
  {
    return "";
  }
  return name;
}

void Commands::execute(PhoneBookManager& phoneBook, std::istream& input)
{
  using executeComand = std::function<void(PhoneBookManager&, std::istream&)>;

  std::map<const std::string, executeComand> commands =
  {
    {"add", &executeAdd},
    {"store", &executeStore},
    {"insert", &executeInsert},
    {"delete", &executeDelete},
    {"show", &executeShow},
    {"move", &executeMove}
  };

  std::string commandName;
  input >> commandName;

  auto check = [&](const std::pair<const std::string, executeComand>& pair)
      { return pair.first == commandName; };

  auto command = std::find_if(std::begin(commands), std::end(commands), check);

  if (command != std::end(commands))
  {
    command->second(phoneBook, input);
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
  }
}

void Commands::executeAdd(PhoneBookManager& phoneBook, std::istream& input)
{
  std::string number = readNumber(input);
  std::string name = readName(input);

  std::string data;
  std::getline(input >> std::ws, data);

  if (name.empty() || number.empty() || !data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  phoneBook.add({number, name});
}

void Commands::executeStore(PhoneBookManager& phoneBook, std::istream& input)
{
  std::string bookmark = readBookmark(input);
  std::string newBookmark = readBookmark(input);

  std::string data;
  std::getline(input >> std::ws, data);

  if (bookmark.empty() || newBookmark.empty() || !data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  phoneBook.store(bookmark, newBookmark);
}

void Commands::executeInsert(PhoneBookManager& phoneBook, std::istream& input)
{
  std::map<const std::string, PhoneBookManager::InsertPosition> positions =
  {
    {"after", PhoneBookManager::InsertPosition::after},
    {"before", PhoneBookManager::InsertPosition::before},
  };

  skipSpaces(input);
  std::string position;
  input >> position;
  std::string bookmark = readBookmark(input);
  std::string number = readNumber(input);
  std::string name = readName(input);

  std::string data;
  std::getline(input >> std::ws, data);

  if (position.empty() || bookmark.empty() || number.empty()
      || name.empty() || !data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  auto check = [&](const std::pair<const std::string,
      PhoneBookManager::InsertPosition>& pair){ return pair.first == position; };
  auto command = std::find_if(std::begin(positions), std::end(positions), check);

  if (command == std::end(positions))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  phoneBook.insert(bookmark, {number, name}, command->second);
}

void Commands::executeDelete(PhoneBookManager& phoneBook, std::istream& input)
{
  std::string bookmark = readBookmark(input);
  std::string data;
  std::getline(input >> std::ws, data);

  if (bookmark.empty() || !data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  phoneBook.remove(bookmark);
}

void Commands::executeShow(PhoneBookManager& phoneBook, std::istream& input)
{
  std::string bookmark = readBookmark(input);
  std::string data;
  std::getline(input >> std::ws, data);

  if (bookmark.empty() || !data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  phoneBook.show(bookmark);
}

void Commands::executeMove(PhoneBookManager& phoneBook, std::istream& input)
{
  std::map<const std::string, PhoneBookManager::MovePosition> positions =
    {
      {"first", PhoneBookManager::MovePosition::first},
      {"last", PhoneBookManager::MovePosition::last},
    };

  std::string bookmark = readBookmark(input);
  skipSpaces(input);
  std::string position;
  input >> position;
  std::string data;
  std::getline(input >> std::ws, data);

  if (bookmark.empty() || position.empty() || !data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  auto check = [&](const std::pair<const std::string, PhoneBookManager::MovePosition>& pair)
      { return pair.first == position; };

  auto command = std::find_if(std::begin(positions), std::end(positions), check);

  if (command == std::end(positions))
  {
    int multiplier = 1;
    if (*position.begin() == '-')
    {
      multiplier = -1;
      position.erase(position.begin());
    }
    else if (*position.begin() == '+')
    {
      position.erase(position.begin());
    }

    char *ptr = nullptr;
    int step = std::strtol(position.c_str(), &ptr, 10);

    if (*ptr)
    {
      std::cout << "<INVALID STEP>\n";
      return;
    }

    phoneBook.move(bookmark, step * multiplier);
    return;
  }

  phoneBook.move(bookmark, command->second);
}







