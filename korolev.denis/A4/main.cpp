#include <iostream>

#include "circle.hpp"
#include "rectangle.hpp"
#include "base-types.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include "shape.hpp"

void printInf(const korolev::Shape & shape)
{
  korolev::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "Parameters of shape: " << std::endl;
  std::cout << "Center: (" << frameRect.pos.x << ";" << frameRect.pos.y << ")" << std::endl;
  std::cout << "Frame width: " << frameRect.width << std::endl;
  std::cout << "Frame height: " << frameRect.height << std::endl;
  std::cout << "Area: " << shape.getArea() << std::endl;
}

void movePoint(korolev::Shape & shape, double x, double y)
{
  printInf(shape);
  shape.move({ x, y });
  std::cout << "Parameters of shape after move: " << std::endl;
  printInf(shape);
}

void moveXY(korolev::Shape & shape, double dx, double dy)
{
  printInf(shape);
  shape.move(dx, dy);
  std::cout << "Parameters of shape after move: " << std::endl;
  printInf(shape);
}

void scale(korolev::Shape & shape, double coefficient)
{
  printInf(shape);
  shape.scale(coefficient);
  std::cout << "Parameters of shape after scale: " << std::endl;
  printInf(shape);
}

void rotate(korolev::Shape & shape, double angle)
{
  printInf(shape);
  shape.rotate(angle);
  std::cout << "Parameters of shape after rotation: " << std::endl;
  printInf(shape);
}

int main()
{
  korolev::Circle circle({ 2, 3 }, 1);
  rotate(circle, 26);

  korolev::Rectangle rectangle({ 6, 7 }, 1.5, 3);
  rotate(rectangle, 95);

  korolev::Shape::pointer circle1 = std::make_shared<korolev::Circle>(circle);
  korolev::Shape::pointer rectangle1 = std::make_shared<korolev::Rectangle>(rectangle);
  korolev::Shape::pointer rectangle2 = std::make_shared<korolev::Rectangle>(korolev::point_t{ 4, 5 }, 2, 4);
  korolev::Shape::pointer circle2 = std::make_shared<korolev::Circle>(korolev::point_t{ 2, 3 }, 1);
  korolev::Shape::pointer circle3 = std::make_shared<korolev::Circle>(korolev::point_t{ 5, 1.3 }, 7);

  korolev::CompositeShape composite;
  composite.add(circle1);

  composite.add(rectangle1);
  movePoint(composite, 12, 2.4);

  composite.add(rectangle2);
  moveXY(composite, 2.6, 4.3);

  composite.add(circle2);
  scale(composite, 2.3);

  composite.add(circle3);
  rotate(composite, 34);

  composite.remove(1);
  rotate(composite, 394);

  korolev::Matrix matrix = korolev::partition(composite);

  for (size_t i = 0; i < matrix.getRows(); i++) {
    for (size_t j = 0; j < matrix.getColumns(); j++) {
      if (matrix[i][j] != nullptr) {

        std::cout << "Layer number: " << i << ";" << std::endl
            << "Figure number: " << j << ";" << std::endl;

        printInf(*matrix[i][j]);
      }
    }
  }

  return 0;
}


