#include <functional>
#include <algorithm>
#include <sstream>
#include <map>

#include "queue.hpp"

void executeAdd(QueueWithPriority<std::string>&, std::stringstream&);
void executeGet(QueueWithPriority<std::string>&, std::stringstream&);
void executeAccelerate(QueueWithPriority<std::string>&, std::stringstream&);

void task1()
{
  using executeCommand = std::function<void
      (QueueWithPriority<std::string>&, std::stringstream&)>;

  std::map<const std::string, executeCommand> commands =
  {
    {"add", executeAdd},
    {"get", executeGet},
    {"accelerate", executeAccelerate}
  };

  QueueWithPriority<std::string> queue;

  std::string line;

  while (std::getline(std::cin, line))
  {
    std::stringstream lineStream(line);
    std::string commandName;
    lineStream >> commandName;

    auto check = [&](const std::pair<const std::string, executeCommand>& pair)
        { return (pair.first == commandName); };

    auto command = std::find_if(std::begin(commands), std::end(commands), check);

    if (command != std::end(commands))
    {
      command->second(queue, lineStream);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}

