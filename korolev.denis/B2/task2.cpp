#include <iostream>
#include <list>

const std::size_t MAXSIZE = 20;
const int MAXNUM = 20;
const int MINNUM = 1;

void recursivePrint(std::list<int>::iterator begin, std::list<int>::iterator end)
{
  if (begin == end)
  {
    std::cout << "\n";
    return;
  }

  if (begin == std::prev(end))
  {
    std::cout << *begin << "\n";
    return;
  }

  std::cout << *begin << " " << *(std::prev(end)) << " ";

  ++begin;
  --end;

  recursivePrint(begin, end);
}

void task2()
{
  std::list<int> list;

  std::size_t size = 0;
  int tmp = 0;

  while (std::cin && !(std::cin >> tmp).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Fail reading");
    }

    if ((tmp > MAXNUM) || (tmp < MINNUM))
    {
      throw std::ios_base::failure("Invalid number");
    }

    ++size;
    if (size > MAXSIZE)
    {
      throw std::ios_base::failure("Invalid count of number");
    }

    list.push_back(tmp);
  }

  recursivePrint(list.begin(), list.end());
};

