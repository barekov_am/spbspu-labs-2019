#include <sstream>
#include <algorithm>
#include <iostream>
#include <vector>

#include "data-struct.hpp"

void task()
{
  std::vector<korolev::DataStruct> vector;
  std::string line;
  while(std::getline(std::cin, line))
  {
    if (!std::cin && !std::cin.eof())
    {
      throw std::ios_base::failure("Failed input");
    }

    std::istringstream stream(line);
    vector.push_back(korolev::readStruct(stream));
  }

  std::sort(vector.begin(), vector.end(),
      [](korolev::DataStruct& lhs, korolev::DataStruct& rhs)
  {
      if (lhs.key1 == rhs.key1 && lhs.key2 == rhs.key2)
      {
        return (lhs.str.length() < rhs.str.length());
      } 
      else if (lhs.key1 == rhs.key1) 
      {
        return (lhs.key2 < rhs.key2);
      }
      else
      {
        return (lhs.key1 < rhs.key1);
      }
  });

  std::for_each(vector.begin(), vector.end(), [](korolev::DataStruct& data)
      { korolev::print(data); });
}

