#ifndef B4_DATA_STRUCT_HPP
#define B4_DATA_STRUCT_HPP

#include <stdexcept>

namespace korolev
{
  struct DataStruct
  {
    int key1;
    int key2;
    std::string str;
  };

  void print(const DataStruct& data);
  DataStruct readStruct(std::istream& input);
};

#endif //B4_DATA_STRUCT_HPP

