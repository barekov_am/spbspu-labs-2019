#include "data-struct.hpp"

#include <algorithm>
#include <iostream>
#include <string>

void korolev::print(const korolev::DataStruct& data)
{
  std::cout << data.key1 << "," << data.key2 << "," << data.str << "\n";
}

korolev::DataStruct korolev::readStruct(std::istream& input)
{
  int keys[2] = {0};
  std::string str;
  for (int i = 0; i < 2; i++)
  {
    input >> keys[i];
    if (input.eof() || std::abs(keys[i]) > 5)
    {
      throw std::invalid_argument("Invalid key");
    }

    std::getline(input, str, ',');
    if (input.eof() || !str.empty())
    {
      throw std::invalid_argument("Invalid key");
    }
  }

  while (input.peek() == ' ')
  {
    input.get();
  }

  std::getline(input, str);
  if (!input.eof() || str.empty())
  {
    throw std::invalid_argument("Invalid str");
  }

  return { keys[0], keys[1], str };
}

