#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace korolev
{
  struct point_t
  {
    double x, y;
  };

  struct rectangle_t
  {
    point_t pos;
    double width, height;
  };

  bool isOverlapping(const rectangle_t & rectL, const rectangle_t & rectR);
}

#endif //BASE_TYPES_HPP


