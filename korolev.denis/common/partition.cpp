#include "partition.hpp"
#include <cmath>

korolev::Matrix korolev::partition(const Shape::pointerArray & array, size_t size)
{
  Matrix tmpMatrix;

  size_t tmpRow = 0;
  size_t tmpColumn = 0;

  for (size_t i = 0; i < size; i++) {
    for (size_t j = 0; j < tmpMatrix.getRows(); j++) {
      for (size_t k = 0; k < tmpMatrix.getColumns(); k++) {
        if (tmpMatrix[j][k] == nullptr) {

          tmpRow = j;
          tmpColumn = k;
          break;
        }

        if (korolev::isOverlapping(tmpMatrix[j][k]->getFrameRect(), array[i]->getFrameRect())) {
          tmpRow = j + 1;
          tmpColumn = 0;
          break;
        }
        tmpRow = j;
        tmpColumn = k + 1;
      }

      if (tmpRow == j) {
        break;
      }
    }

    tmpMatrix.addElement(array[i], tmpRow, tmpColumn);
  }

  return tmpMatrix;
}

korolev::Matrix korolev::partition(const CompositeShape & source)
{
  return partition(source.getShapes(), source.getSize());
}

