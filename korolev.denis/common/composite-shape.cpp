#define _USE_MATH_DEFINES
#include "composite-shape.hpp"

#include <cmath>
#include <stdexcept>

korolev::CompositeShape::CompositeShape() :
  shapes_(),
  count_(0)
{ }

korolev::CompositeShape::CompositeShape(const CompositeShape & source) :
  shapes_(std::make_unique<Shape::pointer[]>(source.count_)),
  count_(source.count_)
{
  for (size_t i = 0; i < count_; i++) {
    shapes_[i] = source.shapes_[i];
  }
}

korolev::CompositeShape::CompositeShape(CompositeShape && source) :
  shapes_(std::move(source.shapes_)),
  count_(source.count_)
{
  source.count_ = 0;
}

korolev::CompositeShape & korolev::CompositeShape::operator =(const CompositeShape & source)
{
  if (&source == this) {
    return *this;
  }

  count_ = source.count_;
  Shape::pointerArray arrayOfShapes(std::make_unique<Shape::pointer[]>(source.count_));

  for (std::size_t i = 0; i < count_; i++) {
    arrayOfShapes[i] = source.shapes_[i];
  }

  shapes_.swap(arrayOfShapes);
  return *this;
}

korolev::CompositeShape & korolev::CompositeShape::operator =(CompositeShape && source)
{
  if (&source == this) {
    return *this;
  }

  count_ = source.count_;
  shapes_ = std::move(source.shapes_);
  source.count_ = 0;

  return *this;
}

double korolev::CompositeShape::getArea() const
{
  double area = 0;

  for (size_t i = 0; i < count_; i++) {
      area += shapes_[i]->getArea();
  }

  return area;
}

korolev::Shape::pointer korolev::CompositeShape::operator [](size_t index) const
{
  if (index >= count_) {
    throw std::out_of_range("Index must be less than count of shapes");
  }

  return shapes_[index];
}

korolev::rectangle_t korolev::CompositeShape::getFrameRect() const
{
  if (count_ == 0) {
    throw std::logic_error("Composite shape must be not empty");
  }

  rectangle_t frameRect = shapes_[0]->getFrameRect();

  double minX = frameRect.pos.x - frameRect.width / 2;
  double maxX = frameRect.pos.x + frameRect.width / 2;
  double minY = frameRect.pos.y - frameRect.height / 2;
  double maxY = frameRect.pos.y + frameRect.height / 2;

  for (size_t i = 1; i < count_; i++) {

    frameRect = shapes_[i]->getFrameRect();

    minX = std::fmin(frameRect.pos.x - frameRect.width / 2, minX);
    maxX = std::fmax(frameRect.pos.x + frameRect.width / 2, maxX);
    minY = std::fmin(frameRect.pos.y - frameRect.height / 2, minY);
    maxY = std::fmax(frameRect.pos.y + frameRect.height / 2, maxY);
  }

  return rectangle_t{ { (maxX + minX) / 2, (maxY + minY) / 2 }, maxX - minX, maxY - minY };
}

size_t korolev::CompositeShape::getSize() const
{
  return count_;
}

void korolev::CompositeShape::move(double dx, double dy)
{
  if (count_ == 0) {
    throw std::logic_error("Composite shape must be not empty");
  }

  for (size_t i = 0; i < count_; i++) {
    shapes_[i]->move(dx, dy);
  }
}

void korolev::CompositeShape::move(const point_t & newPoint)
{
  if (count_ == 0) {
    throw std::logic_error("Composite shape must be not empty");
  }

  rectangle_t frameRect = getFrameRect();

  double dx = newPoint.x - frameRect.pos.x;
  double dy = newPoint.y - frameRect.pos.y;

  for (size_t i = 0; i < count_; i++) {
    shapes_[i]->move(dx, dy);
  }
}

void korolev::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0) {
    throw std::invalid_argument("Coefficient of scale must be positive");
  }

  if (count_ == 0) {
    throw std::logic_error("Composite shape must be not empty");
  }

  rectangle_t frameRect = getFrameRect();

  for (size_t i = 0; i < count_; i++) {
    double dx = (shapes_[i]->getFrameRect().pos.x - frameRect.pos.x) * coefficient;
    double dy = (shapes_[i]->getFrameRect().pos.y - frameRect.pos.y) * coefficient;

    shapes_[i]->move({ frameRect.pos.x + dx, frameRect.pos.y + dy });
    shapes_[i]->scale(coefficient);
  }
}

void korolev::CompositeShape::add(Shape::pointer & shape)
{
  if (shape == nullptr) {
    throw std::invalid_argument("Pointer must be not null");
  }

  for (size_t i = 0; i < count_; i++) {
    if (shapes_[i] == shape) {

      return;
    }
  }

  Shape::pointerArray arrayOfShapes(std::make_unique<Shape::pointer[]>(count_ + 1));

  for (size_t i = 0; i < count_; i++) {
    arrayOfShapes[i] = shapes_[i];
  }

  arrayOfShapes[count_] = shape;
  count_++;
  shapes_.swap(arrayOfShapes);
}

void korolev::CompositeShape::remove(size_t index)
{
  if (index >= count_) {
    throw std::invalid_argument("Index must be less than count of shapes");
  }

  for (size_t i = index; i < count_ - 1; i++) {
    shapes_[i] = shapes_[i + 1];
  }

  shapes_[count_ - 1] = nullptr;
  count_--;
}

void korolev::CompositeShape::rotate(double angle)
{
  if (count_ == 0) {
    throw std::logic_error("Composite shape must be not empty");
  }

  const double sine = sin(angle * M_PI / 180);
  const double cosine = cos(angle * M_PI / 180);

  point_t currCenter = getFrameRect().pos;

  for (std::size_t i = 0; i < count_; i++) {

    point_t tmpCenter = shapes_[i]->getFrameRect().pos;

    shapes_[i]->move({ currCenter.x + (tmpCenter.x - currCenter.x) * cosine - (tmpCenter.y - currCenter.y) * sine,
        currCenter.y + (tmpCenter.x - currCenter.x) * sine + (tmpCenter.y - currCenter.y) * cosine });
    shapes_[i]->rotate(angle);
  }
}

korolev::Shape::pointerArray korolev::CompositeShape::getShapes() const
{
  Shape::pointerArray tmpShapes(std::make_unique<Shape::pointer[]>(count_));

  for (size_t i = 0; i < count_; i++) {
    tmpShapes[i] = shapes_[i];
  }

  return tmpShapes;
}


