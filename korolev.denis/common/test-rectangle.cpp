#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>
#include <cmath>

#include "rectangle.hpp"

const double EPSILON = 0.01;

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(constAfterMovingToPoint)
{
  korolev::Rectangle rectangle({ -3, 2.5 }, 2, 5);
  const double widthBefore = rectangle.getFrameRect().width;
  const double heightBefore = rectangle.getFrameRect().height;
  const double areaBefore = rectangle.getArea();

  rectangle.move({2, 3});

  BOOST_CHECK_CLOSE(widthBefore, rectangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(heightBefore, rectangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(constAfterMovingToDxDy)
{
  korolev::Rectangle rectangle({ -3, 2.5 }, 2, 5);
  const double widthBefore = rectangle.getFrameRect().width;
  const double heightBefore = rectangle.getFrameRect().height;
  const double areaBefore = rectangle.getArea();

  rectangle.move(2, 3);

  BOOST_CHECK_CLOSE(widthBefore, rectangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(heightBefore, rectangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(constAfterScaling)
{
  korolev::Rectangle rectangle({ 5.2, -8.1 }, 5, 6);
  const double widthBefore = rectangle.getFrameRect().width;
  const double heightBefore = rectangle.getFrameRect().height;
  const double areaBefore = rectangle.getArea();
  const double coefficient = 2.7;

  rectangle.scale(coefficient);

  BOOST_CHECK_CLOSE(widthBefore * coefficient, rectangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(heightBefore * coefficient, rectangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, rectangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(testRotatingOn45)
{
  korolev::Rectangle rectangle({ 12, 8 }, 4, 4);

  const korolev::rectangle_t frameRectBefore = rectangle.getFrameRect();
  const double areaBefore = rectangle.getArea();

  const double angle = 45;

  rectangle.rotate(angle);

  const korolev::rectangle_t frameRectAfter = rectangle.getFrameRect();
  const double areaAfter = rectangle.getArea();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.width * std::sqrt(2), frameRectAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height * std::sqrt(2), frameRectAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(testRotatingOn90)
{
  korolev::Rectangle rectangle({ 12, 8 }, 4, 4);

  const korolev::rectangle_t frameRectBefore = rectangle.getFrameRect();
  const double areaBefore = rectangle.getArea();

  const double angle = 90;

  rectangle.rotate(angle);

  const korolev::rectangle_t frameRectAfter = rectangle.getFrameRect();
  const double areaAfter = rectangle.getArea();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(testRotatingOn180)
{
  korolev::Rectangle rectangle({ 12, 8 }, 4, 4);

  const korolev::rectangle_t frameRectBefore = rectangle.getFrameRect();
  const double areaBefore = rectangle.getArea();

  const double angle = 180;

  rectangle.rotate(angle);

  const korolev::rectangle_t frameRectAfter = rectangle.getFrameRect();
  const double areaAfter = rectangle.getArea();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidWidth)
{
  BOOST_CHECK_THROW(korolev::Rectangle rectangle({ 0.5, 2 }, -12.4, 4), std::invalid_argument);
  BOOST_CHECK_THROW(korolev::Rectangle rectangle({ 0.5, 2 }, 0, 4), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidHeight)
{
  BOOST_CHECK_THROW(korolev::Rectangle rectangle({ 0.5, 2 }, 18.4, -4.6), std::invalid_argument);
  BOOST_CHECK_THROW(korolev::Rectangle rectangle({ 0.5, 2 }, 18.4, 0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidCoefficientOfScaling)
{
  korolev::Rectangle rectangle({ 0.5, 2.3 }, 3, 6.3);
  BOOST_CHECK_THROW(rectangle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(rectangle.scale(-2.5), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

