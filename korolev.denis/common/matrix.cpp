#include "matrix.hpp"
#include "shape.hpp"

korolev::Matrix::Matrix() :
  rows_(0),
  columns_(0)
{ }

korolev::Matrix::Matrix(const Matrix & source) :
  shapes_(std::make_unique<Shape::pointer[]>(source.rows_ * source.columns_)),
  rows_(source.rows_),
  columns_(source.columns_)
{
  for (size_t i = 0; i < (rows_ * columns_); i++) {
    shapes_[i] = source.shapes_[i];
  }
}

korolev::Matrix::Matrix(Matrix && source) :
  shapes_(std::move(source.shapes_)),
  rows_(source.rows_),
  columns_(source.columns_)
{
  source.rows_ = 0;
  source.columns_ = 0;
}

korolev::Matrix & korolev::Matrix::operator =(const Matrix & source)
{
  if (this == &source) {

    return *this;
  }

  Shape::pointerArray tmpMatrix(std::make_unique<Shape::pointer []>(source.rows_ * source.columns_));

  for (size_t i = 0; i < (source.rows_ * source.columns_); i++) {
    tmpMatrix[i] = source.shapes_[i];
  }

  shapes_.swap(tmpMatrix);
  rows_ = source.rows_;
  columns_ = source.columns_;

  return *this;
}

korolev::Matrix & korolev::Matrix::operator =(Matrix && source)
{
  if (this == &source) {

    return *this;
  }

  rows_ = source.rows_;
  columns_ = source.columns_;
  shapes_ = std::move(source.shapes_);
  source.rows_ = 0;
  source.columns_ = 0;

  return *this;
}


korolev::Shape::pointerArray korolev::Matrix::operator [](size_t index) const
{
  if (index >= rows_) {
    throw std::out_of_range("Index must be less than count of rows");
  }

  Shape::pointerArray tmpShapes(std::make_unique<Shape::pointer[]>(getLayerSize(index)));

  for (size_t i = 0; i < getLayerSize(index); i++) {
    tmpShapes[i] = shapes_[index * columns_ + i];
  }

  return tmpShapes;
}

bool korolev::Matrix::operator ==(const Matrix & source) const
{
  if ((rows_ != source.rows_) || (columns_ != source.columns_)) {

    return false;
  }

  for (size_t i = 0; i < (rows_ * columns_); i++) {
    if (shapes_[i] != source.shapes_[i]) {

      return false;
    }
  }

  return true;
}

bool korolev::Matrix::operator !=(const Matrix & source) const
{
  return !(*this == source);
}

size_t korolev::Matrix::getLayerSize(size_t layer) const
{
  if (layer >= rows_) {
    throw std::logic_error("Index must be less than count of rows");
  }

  for (size_t i = 0; i < columns_; i++) {
    if (shapes_[layer * columns_ + i] == nullptr) {
      return i + 1;
    }
  }

  return columns_;
}

size_t korolev::Matrix::getRows() const
{
  return rows_;
}

size_t korolev::Matrix::getColumns() const
{
  return columns_;
}

void korolev::Matrix::addElement(Shape::pointer shape, size_t row, size_t column)
{
  size_t tmpRows = (row >= rows_) ? (rows_ + 1) : (rows_);
  size_t tmpColumns = (column >= columns_) ? (columns_ + 1) : (columns_);

  Shape::pointerArray tmpMatrix(std::make_unique<Shape::pointer[]>(tmpRows * tmpColumns));

  for (size_t i = 0; i < tmpRows; i++) {
    for (size_t j = 0; j < tmpColumns; j++) {
      if ((rows_ == i) || (columns_ == j)) {
        tmpMatrix[i * tmpColumns + j] = nullptr;
      }

      else {
        tmpMatrix[i * tmpColumns + j] = shapes_[i * columns_ + j];
      }
    }
  }

  tmpMatrix[row * tmpColumns + column] = shape;
  shapes_.swap(tmpMatrix);
  rows_ = tmpRows;
  columns_ = tmpColumns;
}



