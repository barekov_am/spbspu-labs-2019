#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"
#include "composite-shape.hpp"

namespace korolev 
{

  class Matrix
  {
  public:

    Matrix();
    Matrix(const Matrix & obj);
    Matrix(Matrix && obj);
    ~Matrix() = default;

    Matrix & operator=(const Matrix & source);
    Matrix & operator=(Matrix && source);

    Shape::pointerArray operator [](size_t index) const;
    bool operator ==(const Matrix & source) const;
    bool operator !=(const Matrix & source) const;

    void addElement(Shape::pointer shape, size_t row, size_t column);

    size_t getRows() const;
    size_t getColumns() const;
    size_t getLayerSize(size_t index) const;

  private:

    Shape::pointerArray shapes_;
    size_t rows_;
    size_t columns_;
  };
}

#endif // !MATRIX_HPP


