#include "base-types.hpp"

#include <stdexcept>
#include <cmath>

bool korolev::isOverlapping(const rectangle_t & rectL, const rectangle_t & rectR)
{
  return (std::abs(rectL.pos.x - rectR.pos.x) < (rectL.width + rectR.width) / 2) &&
      (std::abs(rectL.pos.y - rectR.pos.y) < (rectL.height + rectR.height) / 2);
}
