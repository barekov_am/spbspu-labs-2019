#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>
#include <cmath>

#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "shape.hpp"

const double EPSILON = 0.01;

BOOST_AUTO_TEST_SUITE(compositeShapeTests)

BOOST_AUTO_TEST_CASE(constAfterMovingToPoint)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);
  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const double areaBefore = composite.getArea();
  const korolev::rectangle_t frameRectBefore = composite.getFrameRect();
  const double newPointX = 5.4;
  const double newPointY = -0.4;

  composite.move({ newPointX, newPointY });
  const double areaAfter = composite.getArea();
  const korolev::rectangle_t frameRectAfter = composite.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, newPointX, EPSILON);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, newPointY, EPSILON);
}

BOOST_AUTO_TEST_CASE(constAfterMovingDxDy)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);
  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const double areaBefore = composite.getArea();
  const korolev::rectangle_t frameRectBefore = composite.getFrameRect();
  const double dx = 5.4;
  const double dy = -0.4;

  composite.move(dx, dy);
  const double areaAfter = composite.getArea();
  const korolev::rectangle_t frameRectAfter = composite.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x + dx, frameRectAfter.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y + dy, frameRectAfter.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(constAfterScalingWithCoefficientMoreThanOne)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);
  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const double areaBefore = composite.getArea();
  const double coefficient = 2.1;
  const korolev::rectangle_t frameRectBefore = composite.getFrameRect();

  composite.scale(coefficient);
  const double areaAfter = composite.getArea();
  const korolev::rectangle_t frameRectAfter = composite.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, areaAfter, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.width * coefficient, frameRectAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height * coefficient, frameRectAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(constAfterScalingWithCoefficientLessThanOneButMoreThanNull)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);
  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const double areaBefore = composite.getArea();
  const double coefficient = 0.6;
  const korolev::rectangle_t frameRectBefore = composite.getFrameRect();

  composite.scale(coefficient);
  const double areaAfter = composite.getArea();
  const korolev::rectangle_t frameRectAfter = composite.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, areaAfter, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.width * coefficient, frameRectAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height * coefficient, frameRectAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(changeAfterAdding)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);
  korolev::CompositeShape composite;
  double areaOfComposite = composite.getArea();
  int count = 0;
  BOOST_CHECK_CLOSE(areaOfComposite, 0, EPSILON);

  composite.add(circle);
  count++;
  areaOfComposite += circle->getArea();
  BOOST_CHECK_EQUAL(count, composite.getSize());
  BOOST_CHECK_CLOSE(areaOfComposite , composite.getArea(), EPSILON);
  composite.add(circle);
  BOOST_CHECK_EQUAL(count, composite.getSize());

  composite.add(rectangle);
  count++;
  areaOfComposite += rectangle->getArea();
  BOOST_CHECK_EQUAL(count, composite.getSize());
  BOOST_CHECK_CLOSE(areaOfComposite, composite.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(changeAfterDeleting)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 10.3, 13.2 }, 4, 6.4);
  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  double areaOfComposite = composite.getArea();
  int count = 2;

  composite.remove(0);
  count--;
  areaOfComposite -= circle->getArea();
  BOOST_CHECK_EQUAL(count, composite.getSize());
  BOOST_CHECK_CLOSE(areaOfComposite, composite.getArea(), EPSILON);

  areaOfComposite = composite.getArea();

  composite.remove(0);
  count--;
  areaOfComposite -= rectangle->getArea();
  BOOST_CHECK_EQUAL(count, composite.getSize());
  BOOST_CHECK_CLOSE(areaOfComposite, composite.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(useCopyConstructorForCopyInEmpty)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);
  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const korolev::rectangle_t frameRectOfComposite = composite.getFrameRect();

  korolev::CompositeShape copyComposite = composite;
  const double areaOfCopyComposite = copyComposite.getArea();
  const double sizeOfCopyComposite = copyComposite.getSize();
  const korolev::rectangle_t frameRectOfCopyComposite = copyComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfCopyComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfCopyComposite, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfCopyComposite.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfCopyComposite.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfCopyComposite.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfCopyComposite.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(useCopyOperatorForCopyInEmpty)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);
  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const korolev::rectangle_t frameRectOfComposite = composite.getFrameRect();

  korolev::CompositeShape copyComposite;
  copyComposite = composite;
  const double areaOfCopyComposite = copyComposite.getArea();
  const double sizeOfCopyComposite = copyComposite.getSize();
  const korolev::rectangle_t frameRectOfCopyComposite = copyComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfCopyComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfCopyComposite, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfCopyComposite.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfCopyComposite.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfCopyComposite.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfCopyComposite.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(useMoveConstructorForMoveInEmpty)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);
  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const korolev::rectangle_t frameRectOfComposite = composite.getFrameRect();

  korolev::CompositeShape moveComposite = std::move(composite);
  const double areaOfMoveComposite = moveComposite.getArea();
  const double sizeOfMoveComposite = moveComposite.getSize();
  const korolev::rectangle_t frameRectOfMoveComposite = moveComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfMoveComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfMoveComposite, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfMoveComposite.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfMoveComposite.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfMoveComposite.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfMoveComposite.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(useMoveOperatorForMoveInEmpty)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);
  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const korolev::rectangle_t frameRectOfComposite = composite.getFrameRect();

  korolev::CompositeShape moveComposite;
  moveComposite = std::move(composite);
  const double areaOfMoveComposite = moveComposite.getArea();
  const double sizeOfMoveComposite = moveComposite.getSize();
  const korolev::rectangle_t frameRectOfMoveComposite = moveComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfMoveComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfMoveComposite, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfMoveComposite.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfMoveComposite.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfMoveComposite.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfMoveComposite.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(useCopyOperatorForCopyInNotEmpty)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);
  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const korolev::rectangle_t frameRectOfComposite = composite.getFrameRect();

  korolev::Shape::pointer copyCircle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer copyRectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);
  korolev::CompositeShape copyComposite;
  copyComposite.add(copyCircle);
  copyComposite.add(copyRectangle);
  copyComposite = composite;
  const double areaOfCopyComposite = copyComposite.getArea();
  const double sizeOfCopyComposite = copyComposite.getSize();
  const korolev::rectangle_t frameRectOfCopyComposite = copyComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfCopyComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfCopyComposite, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfCopyComposite.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfCopyComposite.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfCopyComposite.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfCopyComposite.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(useMoveOperatorForMoveInNotEmpty)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);
  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const korolev::rectangle_t frameRectOfComposite = composite.getFrameRect();

  korolev::Shape::pointer moveCircle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer moveRectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);
  korolev::CompositeShape moveComposite;
  moveComposite.add(moveCircle);
  moveComposite.add(moveRectangle);
  moveComposite = std::move(composite);
  const double areaOfMoveComposite = moveComposite.getArea();
  const double sizeOfMoveComposite = moveComposite.getSize();
  const korolev::rectangle_t frameRectOfMoveComposite = moveComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfMoveComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfMoveComposite, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfMoveComposite.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfMoveComposite.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfMoveComposite.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfMoveComposite.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(testRotatingOn45)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 0, 0 }, 1);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 0, 0 }, 5, 5);
  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const korolev::rectangle_t frameRectBefore = composite.getFrameRect();
  const double areaBefore = composite.getArea();

  const double angle = 45;

  composite.rotate(angle);

  const korolev::rectangle_t frameRectAfter = composite.getFrameRect();
  const double areaAfter = composite.getArea();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.width * std::sqrt(2), frameRectAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height * std::sqrt(2), frameRectAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(testRotatingOn90)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 0, 0 }, 1);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 0, 0 }, 5, 5);
  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const korolev::rectangle_t frameRectBefore = composite.getFrameRect();
  const double areaBefore = composite.getArea();

  const double angle = 90;

  composite.rotate(angle);

  const korolev::rectangle_t frameRectAfter = composite.getFrameRect();
  const double areaAfter = composite.getArea();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(testRotatingOn180)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 0, 0 }, 1);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 0, 0 }, 5, 5);
  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const korolev::rectangle_t frameRectBefore = composite.getFrameRect();
  const double areaBefore = composite.getArea();

  const double angle = 180;

  composite.rotate(angle);

  const korolev::rectangle_t frameRectAfter = composite.getFrameRect();
  const double areaAfter = composite.getArea();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidCoefficientOfScaling)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);

  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  BOOST_CHECK_THROW(composite.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(composite.scale(-5.3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(emptyCompositeShape)
{
  korolev::CompositeShape composite;

  BOOST_CHECK_THROW(composite.scale(4), std::logic_error);
  BOOST_CHECK_THROW(composite.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(composite.move(5.4, -3.2), std::logic_error);
  BOOST_CHECK_THROW(composite.move({ 5.4, -3.2 }), std::logic_error);
}

BOOST_AUTO_TEST_SUITE_END()


