#ifndef A1_SHAPE_HPP
#define A1_SHAPE_HPP

#include "base-types.hpp"

#include <stdexcept>
#include <memory>

namespace korolev
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t & position) = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void scale(double coefficient) = 0;
    virtual void rotate(double angle) = 0;

    using pointer = std::shared_ptr<Shape>;
    using pointerArray = std::unique_ptr<pointer[]>;
  };
}

#endif //A1_SHAPE_HPP

