#ifndef A1_PARTITION_HPP
#define A1_PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"
#include "shape.hpp"

namespace korolev
{
  Matrix partition(const Shape::pointerArray & source, size_t size);
  Matrix partition(const CompositeShape & source);
}

#endif //A1_PARTITION_HPP

