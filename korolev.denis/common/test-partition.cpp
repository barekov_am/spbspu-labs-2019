#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>
#include <cmath>

#include "partition.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"

BOOST_AUTO_TEST_SUITE(testPartionSuite)

BOOST_AUTO_TEST_CASE(testOverlapping)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 2, 3 }, 1);
  korolev::Shape::pointer rectangle1 = std::make_shared<korolev::Rectangle>(korolev::point_t{ 6, 7 }, 1.5, 3);
  korolev::Shape::pointer rectangle2 = std::make_shared<korolev::Rectangle>(korolev::point_t{ 2, 3 }, 2, 2);

  BOOST_CHECK(!korolev::isOverlapping(circle->getFrameRect(), rectangle1->getFrameRect()));
  BOOST_CHECK(korolev::isOverlapping(circle->getFrameRect(), rectangle2->getFrameRect()));
  BOOST_CHECK(!korolev::isOverlapping(rectangle1->getFrameRect(), rectangle2->getFrameRect()));
}

BOOST_AUTO_TEST_CASE(testPartition)
{
  korolev::Shape::pointer circle1 = std::make_shared<korolev::Circle>(korolev::point_t{ 2, 3 }, 1);
  korolev::Shape::pointer rectangle1 = std::make_shared<korolev::Rectangle>(korolev::point_t{ 6, 7 }, 1.5, 3);
  korolev::Shape::pointer circle2 = std::make_shared<korolev::Circle>(korolev::point_t{ 8, 7 }, 3);
  korolev::Shape::pointer rectangle2 = std::make_shared<korolev::Rectangle>(korolev::point_t{ 2, 3 }, 2, 2);

  korolev::CompositeShape composite;
  composite.add(circle1);
  composite.add(rectangle1);
  composite.add(circle2);
  composite.add(rectangle2);

  korolev::Matrix matrix = korolev::partition(composite);

  BOOST_CHECK_EQUAL(matrix[0][0], circle1);
  BOOST_CHECK_EQUAL(matrix[0][1], rectangle1);
  BOOST_CHECK_EQUAL(matrix[1][0], circle2);
  BOOST_CHECK_EQUAL(matrix[1][1], rectangle2);
}

BOOST_AUTO_TEST_SUITE_END()


