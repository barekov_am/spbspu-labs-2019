#define _USE_MATH_DEFINES
#include "rectangle.hpp"

#include <stdexcept>
#include <cmath>

const double FULLCIRCLE = 360.0;

korolev::Rectangle::Rectangle(const point_t &center, double width, double height):
  center_(center),
  width_(width),
  height_(height),
  angle_(0)
{
  if ((width_ <= 0.0) || (height_ <= 0.0)) {
    throw std::invalid_argument("Wight and height must be positive");
  }
}

korolev::Rectangle::Rectangle(double x, double y, double width, double height):
  center_({x, y}),
  width_(width),
  height_(height),
  angle_(0)
{
  if ((width_ <= 0.0) || (height_ <= 0.0)) {
    throw std::invalid_argument("Wight and height must be positive");
  }
}

double korolev::Rectangle::getArea() const
{
  return width_ * height_;
}

korolev::rectangle_t korolev::Rectangle::getFrameRect() const
{
  const double sine = std::abs(sin(angle_ * M_PI / 180));
  const double cosine = std::abs(cos(angle_ * M_PI / 180));

  double width = width_ * cosine + height_ * sine;
  double height = height_ * cosine + width_ * sine;

  return rectangle_t{ center_, width, height };
}

void korolev::Rectangle::move(const point_t &position)
{
  center_ = position;
}

void korolev::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void korolev::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Coefficient must be positive");
  }
  height_ *= coefficient;
  width_ *= coefficient;
}

void korolev::Rectangle::rotate(double angle)
{
  angle_ += angle;
  angle_ = fmod(angle_, FULLCIRCLE);

  if (angle_ < 0.0) {
    angle_ += FULLCIRCLE;
  }
}

