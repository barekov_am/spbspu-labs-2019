#ifndef A1_COMPOSITE_SHAPE_HPP
#define A1_COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace korolev
{

  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape & source);
    CompositeShape(CompositeShape && source);

    CompositeShape& operator =(const CompositeShape & source);
    CompositeShape& operator =(CompositeShape && source);

    Shape::pointer operator [](size_t index) const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t & newPoint) override;
    void scale(double) override;
    size_t getSize() const;
    void add(Shape::pointer & shape);
    void remove(size_t index);
    void rotate(double angle) override;
    Shape::pointerArray getShapes() const;

  private:

    Shape::pointerArray shapes_;
    size_t count_;
  };
}

#endif //A1_COMPOSITE_SHAPE_HPP

