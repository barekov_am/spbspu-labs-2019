#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>
#include <cmath>

#include "matrix.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"
#include "shape.hpp"

const double EPSILON = 0.01;

BOOST_AUTO_TEST_SUITE(testMatrixSuite)

BOOST_AUTO_TEST_CASE(useCopyConstructor)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);

  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  korolev::Matrix matrix = korolev::partition(composite);

  korolev::Matrix copyMatrix(matrix);

  BOOST_CHECK(matrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(useCopyOperator)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);

  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  korolev::Matrix matrix = korolev::partition(composite);

  korolev::Matrix copyMatrix;
  copyMatrix = matrix;

  BOOST_CHECK(matrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(useMoveConstructor)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);

  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  korolev::Matrix matrix = korolev::partition(composite);

  korolev::Matrix copyMatrix(matrix);
  korolev::Matrix moveMatrix = std::move(matrix);

  BOOST_CHECK(copyMatrix == moveMatrix);
  BOOST_CHECK_EQUAL(matrix.getRows(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(useMoveOperator)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 1.3, -5.2 }, 4);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 1.3, -5.2 }, 4, 6.4);

  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  korolev::Matrix matrix = korolev::partition(composite);

  korolev::Matrix copyMatrix(matrix);
  korolev::Matrix moveMatrix;
  moveMatrix = std::move(matrix);

  BOOST_CHECK(copyMatrix == moveMatrix);
  BOOST_CHECK_EQUAL(matrix.getRows(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(testCreationAndAndAddingElements)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 2, 3 }, 1);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 6, 7 }, 1, 3);
  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  size_t rows = 1;
  const size_t columns = 2;

  korolev::Matrix matrix = korolev::partition(composite);
  BOOST_CHECK_EQUAL(matrix.getRows(), rows);
  BOOST_CHECK_EQUAL(matrix.getColumns(), columns);
  BOOST_CHECK_EQUAL(matrix[0][0], circle);
  BOOST_CHECK_EQUAL(matrix[0][1], rectangle);

  korolev::Shape::pointer rectangle1 = std::make_shared<korolev::Rectangle>(korolev::point_t{ 2, 3 }, 2, 2);
  composite.add(rectangle1);
  matrix = korolev::partition(composite);
  rows++;

  BOOST_CHECK_EQUAL(matrix.getRows(), rows);
  BOOST_CHECK_EQUAL(matrix.getColumns(), columns);
  BOOST_CHECK_EQUAL(matrix[0][0], circle);
  BOOST_CHECK_EQUAL(matrix[0][1], rectangle);
  BOOST_CHECK_EQUAL(matrix[1][0], rectangle1);
}

BOOST_AUTO_TEST_CASE(exeptionHandling)
{
  korolev::Shape::pointer circle = std::make_shared<korolev::Circle>(korolev::point_t{ 2, 3 }, 1);
  korolev::Shape::pointer rectangle = std::make_shared<korolev::Rectangle>(korolev::point_t{ 6, 7 }, 1.5, 3);

  korolev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  korolev::Matrix matrix = korolev::partition(composite);

  BOOST_CHECK_THROW(matrix[6], std::logic_error);
  BOOST_CHECK_THROW(matrix.getLayerSize(7), std::logic_error);
}

BOOST_AUTO_TEST_SUITE_END()


