#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

const double EPSILON = 0.01;

BOOST_AUTO_TEST_SUITE(circleTest)

BOOST_AUTO_TEST_CASE(constAfterMovingToPoint)
{
  korolev::Circle circle({ -3.2, 5 }, 3.6);
  const double radiusBefore = circle.getRadius();
  const double areaBefore = circle.getArea();

  circle.move({ 5.3, 7 });

  BOOST_CHECK_CLOSE(radiusBefore, circle.getRadius(), EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(constAfterMovingDxDy)
{
  korolev::Circle circle({ -3.2, 5 }, 3.6);
  const double radiusBefore = circle.getRadius();
  const double areaBefore = circle.getArea();

  circle.move(5.3, 7);

  BOOST_CHECK_CLOSE(radiusBefore, circle.getRadius(), EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(constAfterScaling)
{
  korolev::Circle circle({ 0, 0 }, 6);
  const double areaBefore = circle.getArea();
  const double radiusBefore = circle.getRadius();
  const double coefficient = 5.6;

  circle.scale(coefficient);

  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, circle.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(radiusBefore * coefficient, circle.getRadius(), EPSILON);
}

BOOST_AUTO_TEST_CASE(testRotating)
{
  korolev::Circle circle({ 3.2, 4.5 }, 5.2);

  const korolev::rectangle_t frameRectBefore = circle.getFrameRect();
  const double areaBefore = circle.getArea();

  const size_t angle = 45;

  circle.rotate(angle);

  const korolev::rectangle_t frameRectAfter = circle.getFrameRect();
  const double areaAfter = circle.getArea();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidRadius)
{
  BOOST_CHECK_THROW(korolev::Circle circle({ 0.5, 2 }, -6.3), std::invalid_argument);
  BOOST_CHECK_THROW(korolev::Circle circle({ 0.5, 2 }, 0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidCoefficientOfScaling)
{
  korolev::Circle circle({ 0.3, 1.3 }, 3);
  BOOST_CHECK_THROW(circle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(circle.scale(-4.5), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

