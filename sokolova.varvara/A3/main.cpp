#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
  sokolova::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  sokolova::Shape * shapePtr = &rectangle;
  std::cout << "FOR RECTANGLE\n";
  shapePtr->print();
  std::cout << "1) Moving along 0x " << 7 << " and 0y " << 7 << std::endl;
  shapePtr->move(7.0, 7.0);
  shapePtr->print();
  std::cout << "2) Moving to the point (" << 9 << "; " << 9 << ")\n";
  shapePtr->move({ 9.0, 9.0 });
  shapePtr->print();
  double k = 9.0;
  std::cout << "3) Scaling a shape " << k << " times\n";
  shapePtr->scale(k);
  shapePtr->print();
  std::cout << std::endl;

  sokolova::Circle circle(5.0, { 3.0, 3.0 });
  shapePtr = &circle;
  std::cout << "FOR CIRCLE\n";
  shapePtr->print();
  std::cout << "1) Moving along 0x " << 9 << " and 0y " << 9 << std::endl;
  shapePtr->move(9.0, 9.0);
  shapePtr->print();
  std::cout << "2) Moving to the point (" << 8 << "; " << 8 << ")\n";
  shapePtr->move({ 8.0, 8.0 });
  shapePtr->print();
  k = 4.0;
  std::cout << "3) Scaling a shape " << k << " times\n";
  shapePtr->scale(k);
  shapePtr->print();
  std::cout << std::endl;

  sokolova::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  shapePtr = &triangle;
  std::cout << "\nFOR TRIANGLE\n";
  shapePtr->print();
  std::cout << "\n1) Moving along 0x " << 4 << " and 0y " << 4 << std::endl;
  shapePtr->move(4.0, 4.0);
  shapePtr->print();
  std::cout << "\n2) Moving to the point (" << 5 << ";" << 5 << ")\n";
  const sokolova::point_t nPosTrian = { 5.0, 5.0 };
  shapePtr->move(nPosTrian);
  shapePtr->print();
  k = 9.0;
  std::cout << "\n3) Scaling a shape " << k << " times\n";
  shapePtr->scale(k);
  shapePtr->print();
  std::cout << "\n";


  std::cout << " \n\nFOR COMPOSITE SHAPE\n";
  sokolova::shap3 rectanglePtr = std::make_shared<sokolova::Rectangle>(rectangle);
  sokolova::shap3 circlePtr = std::make_shared<sokolova::Circle>(circle);
  sokolova::shap3 trianglePtr = std::make_shared<sokolova::Triangle>(triangle);
  sokolova::CompositeShape compositeShape(rectanglePtr);
  compositeShape.add(circlePtr);
  compositeShape.add(trianglePtr);
  shapePtr = &compositeShape;
  shapePtr->print();
  std::cout << "\n1) Moving along 0x " << 10 << " and 0y " << 10 << std::endl;
  shapePtr->move(10.0, 10.0);
  shapePtr->print();
  std::cout << "\n2) Moving to the point (" << 100 << ";" << 100 << ")\n";
  const sokolova::point_t nPosCompS = { 100.0, 100.0 };
  shapePtr->move(nPosCompS);
  shapePtr->print();
  k = 4.0;
  std::cout << "\n3) Scaling a shape " << k << " times\n";
  shapePtr->scale(k);
  shapePtr->print();
  std::cout << "\n4)Deleting a shape\n";
  compositeShape.remove(1);
  compositeShape.print();

  system("pause");

  return 0;
}
