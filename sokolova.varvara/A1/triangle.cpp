#include "triangle.hpp"

#include <iostream>
#include <math.h>
#include <cassert>
#include <algorithm>

Triangle::Triangle (const point_t & v_a, const point_t & v_b, const point_t & v_c):
  a_(v_a),
  b_(v_b),
  c_(v_c),
  centre_({(v_a.x + v_b.x + v_c.x) / 3, (v_a.y + v_b.y + v_c.y) / 3})
{
}

void Triangle::move(const point_t & dcentre)
{
  const point_t df = {dcentre.x - centre_.x, dcentre.y - centre_.y};
  move(df.x, df.y);
}

void Triangle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
  a_.x += dx;
  a_.y += dy;
  b_.x += dx;
  b_.y += dy;
  c_.x += dx;
  c_.y += dy;
}

double Triangle::getArea() const
{
  return (std::fabs(a_.x - c_.x) * (b_.y - c_.y) - std::fabs(b_.x - c_.x) * (a_.y - c_.y)) / 2;
}

rectangle_t Triangle::getFrameRect() const
{
  rectangle_t frect;
  frect.height = std::max(std::max(a_.y, b_.y), c_.y) - std::min(std::min(a_.y, b_.y), c_.y);
  frect.width = std::max(std::max(a_.x, b_.x), c_.x) - std::min(std::min(a_.x, b_.x), c_.x);
  frect.pos = centre_;
  return frect;
}

point_t Triangle::getCentre() const
{
  return centre_;
}
