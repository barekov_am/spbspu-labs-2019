#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

class Rectangle : public Shape
{
public:
  Rectangle(double width, double height, const point_t & centre_point);
  void move(const point_t & dcentre) override;
  void move(double dx, double dy) override;
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  double getWidth() const;
  double getHeight() const;
  point_t getCentre() const override;
  
private:
  double width_, height_;
  point_t centre_;
};

#endif 
