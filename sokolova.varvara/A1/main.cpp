#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

void print(const Shape & shape)
{
  std::cout << "area: " << shape.getArea() << std::endl;
  std::cout << "centre (x): " << shape.getCentre().x << std::endl;
  std::cout << "centre (y): " << shape.getCentre().y << std::endl;
  std::cout << "frame_rect height: " << shape.getFrameRect().height << std::endl;
  std::cout << "frame_rect width: " << shape.getFrameRect().width << std::endl;
  std::cout << "frame_rect pos(x): " << shape.getFrameRect().pos.x << std::endl;
  std::cout << "frame_rect pos(y): " << shape.getFrameRect().pos.y << std::endl;
  std::cout <<  std::endl;
}

int main()
{
  point_t rect_centre;
  rect_centre.x = 9.1;
  rect_centre.y = 1;
  Rectangle rect (1000, 5, rect_centre);
  point_t rect_move;
  rect_move.x = 99;
  rect_move.y = -1;
  rect.move({5, 7});
  std::cout << "Rectangle:" << std::endl;
  print(rect);
  rect.move(rect_move);
  print(rect);

  point_t circle_centre;
  circle_centre.x = 1;
  circle_centre.y = 12.3;
  Circle circle(10, circle_centre);
  point_t circle_move;
  circle_move.x = 99;
  circle_move.y = -1;
  circle.move({5, 7});
  std::cout << "Circle" << std::endl;
  print(circle);
  circle.move(circle_move);
  print(circle);
  
  Triangle triangle ({3.0, 3.0}, {8.0, 8.0}, {4.0, 4.0});
  point_t triangle_move;
  triangle_move.x = 33;
  triangle_move.y = -1;
  triangle.move({5, 7});
  std::cout << "Triangle:" << std::endl;
  print(triangle);
  triangle.move(triangle_move);
  print(triangle);

  return 0;
}
