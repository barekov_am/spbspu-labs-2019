#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

class Circle : public Shape
{
  public:
    Circle(double radius, const point_t & centre_point);
    void move(const point_t & dot_centre) override;
    void move(double dx, double dy) override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    double getRadius() const;
    point_t getCentre() const override;

  private:
    double radius_;
    point_t centre_;
};

#endif
