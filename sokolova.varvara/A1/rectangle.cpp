#include "rectangle.hpp"

#include <iostream>
#include <stdexcept>

Rectangle::Rectangle(double width, double height, const point_t & centre_point):
  width_(width), 
  height_(height),
  centre_(centre_point)
{
  if (width <= 0.0)
  {
    throw std::invalid_argument("Width <= 0 !");
  }

  if (height <= 0.0) 
  {
    throw std::invalid_argument("Height <= 0 !");
  }

}

void Rectangle::move(const point_t & dot_centre)
{
  centre_ = dot_centre;
}

void Rectangle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  rectangle_t frect;
  frect.height = height_;
  frect.width = width_;
  frect.pos = centre_;
  return frect;
}

double Rectangle::getWidth() const
{
  return width_;
}

double Rectangle::getHeight() const
{
  return height_;
}

point_t Rectangle::getCentre() const
{
  return centre_;
}
