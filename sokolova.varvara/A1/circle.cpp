#define _USE_MATH_DEFINES
#include "circle.hpp"

#include <cmath>
#include <stdexcept>
#include <iostream>


Circle::Circle(double radius, const point_t & centre_point) :
  radius_(radius),
  centre_(centre_point)
{
  if (radius <= 0.0) 
  {
    throw std::invalid_argument("Radius <= 0 !");
  }
}

void Circle::move(const point_t & dot_centre)
{
  centre_ = dot_centre;
}

void Circle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

double Circle::getArea() const
{
  return radius_ * radius_ * M_PI;
}

rectangle_t Circle::getFrameRect() const
{
  return rectangle_t {centre_, 2 * radius_, 2 * radius_};
}

double Circle::getRadius() const
{
  return radius_;
}

point_t Circle::getCentre() const
{
  return centre_;
}
