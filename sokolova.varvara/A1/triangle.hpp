#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

class Triangle: public Shape
{
    public:
      Triangle (const point_t & v_a, const point_t & v_b, const point_t & v_c);
      void move(const point_t & dot_centre) override;
      void move(double dx, double dy) override;
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      point_t getCentre() const override;

    private:
      point_t a_, b_, c_, centre_;
};

#endif
