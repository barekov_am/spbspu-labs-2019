#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separation.hpp"


int main()
{
  sokolova::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  sokolova::Shape * shapePtr = &rectangle;
  std::cout << "FOR RECTANGLE\n";
  shapePtr->print();
  std::cout << "1) Moving along 0x " << 7 << " and 0y " << 7 << std::endl;
  shapePtr->move(7.0, 7.0);
  shapePtr->print();
  std::cout << "2) Moving to the point (" << 9 << "; " << 9 << ")\n";
  shapePtr->move({ 9.0, 9.0 });
  shapePtr->print();
  double k = 9.0;
  std::cout << "3) Scaling a shape " << k << " times\n";
  shapePtr->scale(k);
  shapePtr->print();
  std::cout << "4)After 90 degree rotation: \n";
  shapePtr->rotate(90.0);
  shapePtr->print();
  std::cout << std::endl;

  sokolova::Circle circle(5.0, { 3.0, 3.0 });
  shapePtr = &circle;
  std::cout << "FOR CIRCLE\n";
  shapePtr->print();
  std::cout << "1) Moving along 0x " << 9 << " and 0y " << 9 << std::endl;
  shapePtr->move(9.0, 9.0);
  shapePtr->print();
  std::cout << "2) Moving to the point (" << 8 << "; " << 8 << ")\n";
  shapePtr->move({ 8.0, 8.0 });
  shapePtr->print();
  k = 4.0;
  std::cout << "3) Scaling a shape " << k << " times\n";
  shapePtr->scale(k);
  shapePtr->print();
  std::cout << "4)After 90 degree rotation: \n";
  shapePtr->rotate(90.0);
  shapePtr->print();
  std::cout << std::endl;

  sokolova::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  shapePtr = &triangle;
  std::cout << "\nFOR TRIANGLE\n";
  shapePtr->print();
  std::cout << "\n1) Moving along 0x " << 4 << " and 0y " << 4 << std::endl;
  shapePtr->move(4.0, 4.0);
  shapePtr->print();
  std::cout << "\n2) Moving to the point (" << 5 << ";" << 5 << ")\n";
  const sokolova::point_t nPosTrian = { 5.0, 5.0 };
  shapePtr->move(nPosTrian);
  shapePtr->print();
  k = 9.0;
  std::cout << "\n3) Scaling a shape " << k << " times\n";
  shapePtr->scale(k);
  shapePtr->print();
  std::cout << "\n4)After 90 degree rotation: \n";
  shapePtr->rotate(90.0);
  shapePtr->print();
  std::cout << "\n";

  sokolova::point_t fig[] = { {5.0, 2.0}, {7.0, 3.0}, {7.0, 5.0}, {5.0, 6.0}, {3.0, 4.0} };
  size_t qV = sizeof(fig) / sizeof(fig[0]);
  sokolova::Polygon polygon(qV, fig);
  shapePtr = &polygon;
  std::cout << "\nFOR POLYGON\n";
  shapePtr->print();
  std::cout << "\n1) Moving along 0x " << 10 << " and 0y " << 10 << std::endl;
  shapePtr->move(10.0, 10.0);
  shapePtr->print();
  std::cout << "\n2) Moving to the point (" << 12 << ";" << 12 << ")\n";
  const sokolova::point_t nPosPoly = { 12.0, 12.0 };
  shapePtr->move(nPosPoly);
  shapePtr->print();
  k = 4.0;
  std::cout << "\n3) Scaling a shape " << k << " times\n";
  shapePtr->scale(k);
  shapePtr->print();        
  std::cout << "\n4)Copy constructor\n";
  sokolova::Polygon copyConstructor(polygon);
  shapePtr = &copyConstructor;
  shapePtr->print();
  std::cout << "\n5)Copy assignment\n";
  sokolova::Polygon copyAssignment;
  shapePtr = &copyAssignment;
  copyAssignment = polygon;
  shapePtr->print();
  std::cout << "\n6)Move constructor of polygon\n";
  sokolova::Polygon moveConstructor(std::move(copyAssignment));
  shapePtr = &moveConstructor;
  shapePtr->print();
  std::cout << "\n7)Move assignment of polygon\n";
  sokolova::Polygon moveAssignment;
  shapePtr = &moveAssignment;
  moveAssignment = (std::move(polygon));
  shapePtr->print();
  std::cout << "\n8)After 90 degree rotation: \n";
  shapePtr->rotate(90.0);
  shapePtr->print();

  std::cout << " \n\nFOR COMPOSITE SHAPE\n";
  sokolova::shap3 rectanglePtr = std::make_shared<sokolova::Rectangle>(rectangle);
  sokolova::shap3 circlePtr = std::make_shared<sokolova::Circle>(circle);
  sokolova::shap3 trianglePtr = std::make_shared<sokolova::Triangle>(triangle);
  sokolova::shap3 polygonPtr = std::make_shared<sokolova::Polygon>(moveAssignment);
  sokolova::CompositeShape compositeShape(rectanglePtr);
  compositeShape.add(circlePtr);
  compositeShape.add(trianglePtr);
  compositeShape.add(polygonPtr);
  shapePtr = &compositeShape;
  shapePtr->print();
  std::cout << "\n1) Moving along 0x " << 10 << " and 0y " << 10 << std::endl;
  shapePtr->move(10.0, 10.0);
  shapePtr->print();
  std::cout << "\n2) Moving to the point (" << 100 << ";" << 100 << ")\n";
  const sokolova::point_t nPosCompS = { 100.0, 100.0 };
  shapePtr->move(nPosCompS);
  shapePtr->print();
  k = 4.0;
  std::cout << "\n3) Scaling a shape " << k << " times\n";
  shapePtr->scale(k);
  shapePtr->print();
  std::cout << "\n4)Deleting some shapes\n";
  compositeShape.remove(3);
  compositeShape.remove(2);
  compositeShape.print();
  std::cout << "5)After 90 degree rotation: \n";
  shapePtr->rotate(90.0);
  shapePtr->print();

  std::cout << " \n\nFOR MATRIX\n";
  sokolova::Matrix matrix;
  matrix = sokolova::part(compositeShape);
  matrix.print();
  
  system("pause");

  return 0;
}            
