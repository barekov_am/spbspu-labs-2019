#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "circle.hpp"

const double EPS = 0.001;

BOOST_AUTO_TEST_SUITE(circleTest)

BOOST_AUTO_TEST_CASE(firstMove)
{
  sokolova::Circle circle(5.0, { 3.0, 3.0 });
  const double oldRadius = 5.0;
  const double oldArea = circle.getArea();
  circle.move({ 5.0, 5.0 });
  BOOST_CHECK_CLOSE(oldRadius, 5.0, EPS);
  BOOST_CHECK_CLOSE(oldArea, circle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(secondMove)
{
  sokolova::Circle circle(6.0, { 3.0, 3.0 });
  const double oldRadius = 6.0;
  const double oldArea = circle.getArea();
  circle.move(5.0, 5.0);
  BOOST_CHECK_CLOSE(oldRadius, 6.0, EPS);
  BOOST_CHECK_CLOSE(oldArea, circle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(scale)
{
  sokolova::Circle circle(6.0, { 3.0, 3.0 });
  double oldRadius = 6.0;
  double oldArea = circle.getArea();
  double k = 9;
  circle.scale(9);
  BOOST_CHECK_CLOSE(oldRadius * k, 6.0 * k, EPS);
  BOOST_CHECK_CLOSE(oldArea * k * k, circle.getArea(), EPS);
  oldRadius = 54.0;
  oldArea = circle.getArea();
  k = 0.5;
  circle.scale(0.5);
  BOOST_CHECK_CLOSE(oldRadius * k, 54.0 * k, EPS);
  BOOST_CHECK_CLOSE(oldArea * k * k, circle.getArea(), EPS);

}

BOOST_AUTO_TEST_CASE(errRadius)
{
  BOOST_CHECK_THROW(sokolova::Circle circle(-6.0, { 3.0, 3.0 }), std::invalid_argument);
  BOOST_CHECK_THROW(sokolova::Circle circle(0.0, { 3.0, 3.0 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(errCoeff)
{
  sokolova::Circle circle(6.0, { 3.0, 3.0 });
  BOOST_CHECK_THROW(circle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  sokolova::Circle circle(6.0, { 3.0, 3.0 });
  const double oldArea = circle.getArea();
  const sokolova::rectangle_t testFR = circle.getFrameRect();
  const double angle = 90;

  circle.rotate(angle);

  BOOST_CHECK_CLOSE(testFR.width, circle.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(testFR.height, circle.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(oldArea, circle.getArea(), EPS);
  BOOST_CHECK_EQUAL(testFR.pos.x, circle.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFR.pos.y, circle.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
