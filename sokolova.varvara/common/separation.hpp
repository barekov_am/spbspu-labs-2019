#ifndef SEPARATION_HPP
#define SEPARATION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace sokolova
{
  bool cross(const rectangle_t &, const rectangle_t &);
  Matrix part(const dynamicArr &, size_t);
  Matrix part(const CompositeShape &);
}

#endif
