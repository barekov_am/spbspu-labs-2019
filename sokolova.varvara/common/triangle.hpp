#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace sokolova
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t & vA, const point_t & vB, const point_t & vC);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void print() const override;
    void move(double dx, double dy) override;
    void move(const point_t & nPos) override;
    void scale(double k) override;
    void rotate(const double) override;

    point_t rotate(const point_t &, double) const;

  private:
    point_t pos_, vA_, vB_, vC_;
  };
}

#endif
