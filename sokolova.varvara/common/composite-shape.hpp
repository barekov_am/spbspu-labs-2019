#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace sokolova
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&);
    CompositeShape(const shap3 &);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &);
    CompositeShape &operator =(CompositeShape &&);
    shap3 operator [](size_t) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(const double, const double) override;
    void print() const override;
    void scale(const double) override;
    void rotate(const double) override;
    
    size_t getSize() const;
    void add(const shap3 &);
    void remove(size_t);
    dynamicArr getList() const;

  private:
    size_t size_;
    dynamicArr shapeArray_;
    double angle_;
  };
}

#endif
