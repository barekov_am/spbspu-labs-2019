#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace sokolova
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const rectangle_t & frect);
    void move(const point_t & dcentre) override;
    void move(double dx, double dy) override;
    void scale(double k) override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void print() const override;
    void rotate(const double) override;

  private:
    rectangle_t rect_;
    double angle_;
  };
}

#endif 
