#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <cstddef>
#include "shape.hpp"

namespace sokolova
{
  class Polygon : public Shape
  {
  public:
    Polygon();
    Polygon(const Polygon & ptr);
    Polygon(Polygon && ptr);
    Polygon(std::size_t qV, point_t * v);
    ~Polygon();
    Polygon & operator =(const Polygon & ptr);
    Polygon & operator =(Polygon && ptr);

    point_t findCen() const;
    std::size_t getQ() const;
    void print() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void scale(double k) override;
    bool check() const;
    void move(const double dx, const double dy) override;
    void move(const point_t & point) override;
    void rotate(const double) override;

  private:
    std::size_t quantityOfVertexes_;
    point_t center_;
    point_t *vertex_;
  };
}

#endif
