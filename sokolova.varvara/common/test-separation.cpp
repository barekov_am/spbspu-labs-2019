#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separation.hpp"

BOOST_AUTO_TEST_SUITE(separationTest)

BOOST_AUTO_TEST_CASE(checkCorrectCrossing)
{
  sokolova::Rectangle testRectangle({ 4.0, 2.0, {5.0, 5.0} });
  sokolova::Rectangle crossingTestRectangle({ 6.0, 2.0, {8.0, 5.0} });
  sokolova::Circle testCircle(1.0, { 11.0, 5.0 });

  const bool falseResult = sokolova::cross(testRectangle.getFrameRect(), testCircle.getFrameRect());
  const bool trueResult1 = sokolova::cross(testRectangle.getFrameRect(), crossingTestRectangle.getFrameRect());
  const bool trueResult2 = sokolova::cross(testCircle.getFrameRect(), crossingTestRectangle.getFrameRect());

  BOOST_CHECK_EQUAL(falseResult, false);
  BOOST_CHECK_EQUAL(trueResult1, true);
  BOOST_CHECK_EQUAL(trueResult2, true);

}

BOOST_AUTO_TEST_CASE(checkCorrectSeparation)
{
  sokolova::Rectangle testRectangle({ 3.0, 3.0, {1.0, 1.0} });
  sokolova::Rectangle crossingTestRectangle({ 3.0, 3.0, {1.0, 1.0} });
  sokolova::Circle testCircle(5.0, { 13.0, 13.0 });
  sokolova::shap3 rectanglePtr = std::make_shared<sokolova::Rectangle>(testRectangle);
  sokolova::shap3 rectangleCrossingPtr = std::make_shared<sokolova::Rectangle>(crossingTestRectangle);
  sokolova::shap3 circlePtr = std::make_shared<sokolova::Circle>(testCircle);

  sokolova::CompositeShape testCompositeShape(rectanglePtr);
  testCompositeShape.add(rectangleCrossingPtr);
  testCompositeShape.add(circlePtr);

  sokolova::Matrix testMatrix = sokolova::part(testCompositeShape);

  BOOST_CHECK(testMatrix[0][0] == rectanglePtr);
  BOOST_CHECK(testMatrix[0][1] == circlePtr);
  BOOST_CHECK(testMatrix[1][0] == rectangleCrossingPtr);
}

BOOST_AUTO_TEST_SUITE_END()
