#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "polygon.hpp"

const double EPS = 0.001;

BOOST_AUTO_TEST_SUITE(polygonTest)

BOOST_AUTO_TEST_CASE(firstMove)
{
  sokolova::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t qV = sizeof(fig) / sizeof(fig[0]);
  sokolova::Polygon polygon(qV, fig);
  const double oldA = polygon.getArea();
  polygon.move({ 5.0, 5.0 });
  BOOST_CHECK_CLOSE(oldA, polygon.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(secondMove)
{
  sokolova::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  sokolova::Polygon polygon(quantityOfVertexes, fig);
  const double oldArea = polygon.getArea();
  polygon.move(5.0, 5.0);
  BOOST_CHECK_CLOSE(oldArea, polygon.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(scale)
{
  sokolova::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  sokolova::Polygon polygon(quantityOfVertexes, fig);
  double oldArea = polygon.getArea();
  double k = 9;
  polygon.scale(9);
  BOOST_CHECK_CLOSE(oldArea * k * k, polygon.getArea(), EPS);
  oldArea = polygon.getArea();
  k = 0.5;
  polygon.scale(0.5);
  BOOST_CHECK_CLOSE(oldArea * k * k, polygon.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(errCoeff)
{
  sokolova::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  sokolova::Polygon polygon(quantityOfVertexes, fig);
  BOOST_CHECK_THROW(polygon.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(polygon.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testConstructor)
{
  sokolova::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  size_t zero = 0;
  sokolova::Polygon polygon1(quantityOfVertexes, fig);
  sokolova::Polygon polygon2(polygon1);
  BOOST_CHECK_EQUAL(polygon1.getQ(), polygon2.getQ());

  const size_t oldQuantity = polygon2.getQ();
  sokolova::Polygon polygon3(std::move(polygon2));
  BOOST_CHECK_EQUAL(polygon2.getQ(), zero);
  BOOST_CHECK_EQUAL(polygon3.getQ(), oldQuantity);
}

BOOST_AUTO_TEST_CASE(testOperator)
{
  sokolova::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  size_t zero = 0;
  sokolova::Polygon polygon1(quantityOfVertexes, fig);
  sokolova::Polygon polygon2;
  polygon2 = polygon1;
  BOOST_CHECK_EQUAL(polygon1.getQ(), polygon2.getQ());

  const size_t oldQuantity = polygon2.getQ();
  sokolova::Polygon polygon3 = std::move(polygon2);
  BOOST_CHECK_EQUAL(polygon2.getQ(), zero);
  BOOST_CHECK_EQUAL(polygon3.getQ(), oldQuantity);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  sokolova::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  sokolova::Polygon polygon(quantityOfVertexes, fig);

  double oldArea = polygon.getArea();
  const double angle = 90;

  polygon.rotate(angle);

  BOOST_CHECK_CLOSE(oldArea, polygon.getArea(), EPS);
}

BOOST_AUTO_TEST_SUITE_END()
