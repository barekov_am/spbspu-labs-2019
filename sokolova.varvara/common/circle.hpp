#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace sokolova
{
  class Circle : public Shape
  {
  public:
    Circle(double radius, const point_t & center);
    void move(const point_t & point) override;
    void move(double dx, double dy) override;
    void scale(double k) override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void print() const override;
    void rotate(const double) override;

  private:
    double radius_;
    point_t center_;
  };
}

#endif
