#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

namespace sokolova
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &);
    Matrix(Matrix &&);
    ~Matrix() = default;

    Matrix &operator =(const Matrix &);
    Matrix &operator =(Matrix &&);
    dynamicArr operator [](size_t) const;
    bool operator ==(const Matrix &) const;
    bool operator !=(const Matrix &) const;

    size_t getLines() const;
    size_t getColumns() const;
    void add(const shap3 &, size_t, size_t);
    void print() const;

  private:
    size_t lines_;
    size_t columns_;
    dynamicArr list_;
  };
}

#endif
