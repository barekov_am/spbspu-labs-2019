#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separation.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithCrossing)
{
  const size_t layersValue = 2;
  const size_t columnsValue = 1;

  sokolova::Rectangle testRectangle({ 3.0, 3.0, {1.0, 1.0} });
  sokolova::Circle testCircle(5.0, { 3.0, 3.0 });
  sokolova::shap3 rectanglePtr = std::make_shared<sokolova::Rectangle>(testRectangle);
  sokolova::shap3 circlePtr = std::make_shared<sokolova::Circle>(testCircle);

  sokolova::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  sokolova::Matrix testMatrix = sokolova::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithoutCrossing)
{
  const size_t layersValue = 1;
  const size_t columnsValue = 2;

  sokolova::Rectangle testRectangle({ 3.0, 3.0, {1.0, 1.0} });
  sokolova::Circle testCircle(5.0, { 13.0, 3.0 });
  sokolova::shap3 rectanglePtr = std::make_shared<sokolova::Rectangle>(testRectangle);
  sokolova::shap3 circlePtr = std::make_shared<sokolova::Circle>(testCircle);

  sokolova::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  sokolova::Matrix testMatrix = sokolova::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  sokolova::Rectangle testRectangle({ 3.0, 3.0, {1.0, 1.0} });
  sokolova::Circle testCircle(5.0, { 3.0, 3.0 });
  sokolova::shap3 rectanglePtr = std::make_shared<sokolova::Rectangle>(testRectangle);
  sokolova::shap3 circlePtr = std::make_shared<sokolova::Circle>(testCircle);

  sokolova::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  sokolova::Matrix testMatrix = sokolova::part(testCompositeShape);

  BOOST_CHECK_THROW(testMatrix[5][0], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-5][0], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkCopyAndMove)
{
  sokolova::Rectangle testRectangle({ 3.0, 3.0, {1.0, 1.0} });
  sokolova::Circle testCircle(5.0, { 3.0, 3.0 });
  sokolova::shap3 rectanglePtr = std::make_shared<sokolova::Rectangle>(testRectangle);
  sokolova::shap3 circlePtr = std::make_shared<sokolova::Circle>(testCircle);

  sokolova::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  sokolova::Matrix testMatrix = sokolova::part(testCompositeShape);
  sokolova::Matrix copyMatrix(testMatrix);
  sokolova::Matrix moveMatrix(std::move(testMatrix));

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyMatrix);
}

BOOST_AUTO_TEST_SUITE_END()
