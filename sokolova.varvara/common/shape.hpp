#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>

#include "base-types.hpp"

namespace sokolova
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t & point) = 0;
    virtual void move(const double dx, const double dy) = 0;
    virtual void print() const = 0;
    virtual void scale(double k) = 0;
    virtual void rotate(const double) = 0;
  };
  using shap3 = std::shared_ptr<Shape>;
  using dynamicArr = std::unique_ptr<shap3[]>;
}

#endif
