#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "composite-shape.hpp"
#include "polygon.hpp"
#include "triangle.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double EPS = 0.001;

BOOST_AUTO_TEST_SUITE(testCompositeShape)

BOOST_AUTO_TEST_CASE(firstMove)
{
  sokolova::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  sokolova::Circle circle(6.0, { 3.0, 3.0 });
  sokolova::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  sokolova::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t qV = sizeof(fig) / sizeof(fig[0]);
  sokolova::Polygon polygon(qV, fig);
  sokolova::shap3 rectanglePtr = std::make_shared<sokolova::Rectangle>(rectangle);
  sokolova::shap3 circlePtr = std::make_shared<sokolova::Circle>(circle);
  sokolova::shap3 trianglePtr = std::make_shared<sokolova::Triangle>(triangle);
  sokolova::shap3 polygonPtr = std::make_shared<sokolova::Polygon>(polygon);
  sokolova::CompositeShape compositeShape(rectanglePtr);
  compositeShape.add(circlePtr);
  compositeShape.add(trianglePtr);
  compositeShape.add(polygonPtr);

  const sokolova::rectangle_t oldFR = compositeShape.getFrameRect();
  const double oldA = compositeShape.getArea();
  compositeShape.move({ 50.0, 50.0 });
  BOOST_CHECK_CLOSE(oldFR.width, compositeShape.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(oldFR.height, compositeShape.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(oldA, compositeShape.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(secondMove)
{
  sokolova::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  sokolova::Circle circle(6.0, { 3.0, 3.0 });
  sokolova::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  sokolova::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t qV = sizeof(fig) / sizeof(fig[0]);
  sokolova::Polygon polygon(qV, fig);
  sokolova::shap3 rectanglePtr = std::make_shared<sokolova::Rectangle>(rectangle);
  sokolova::shap3 circlePtr = std::make_shared<sokolova::Circle>(circle);
  sokolova::shap3 trianglePtr = std::make_shared<sokolova::Triangle>(triangle);
  sokolova::shap3 polygonPtr = std::make_shared<sokolova::Polygon>(polygon);
  sokolova::CompositeShape compositeShape(rectanglePtr);
  compositeShape.add(circlePtr);
  compositeShape.add(trianglePtr);
  compositeShape.add(polygonPtr);

  const sokolova::rectangle_t oldFR = compositeShape.getFrameRect();
  const double oldA = compositeShape.getArea();
  compositeShape.move(10.0, 10.0);
  BOOST_CHECK_CLOSE(oldFR.width, compositeShape.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(oldFR.height, compositeShape.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(oldA, compositeShape.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(errValues)
{
  sokolova::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  sokolova::Circle circle(6.0, { 3.0, 3.0 });
  sokolova::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  sokolova::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t qV = sizeof(fig) / sizeof(fig[0]);
  sokolova::Polygon polygon(qV, fig);
  sokolova::shap3 rectanglePtr = std::make_shared<sokolova::Rectangle>(rectangle);
  sokolova::shap3 circlePtr = std::make_shared<sokolova::Circle>(circle);
  sokolova::shap3 trianglePtr = std::make_shared<sokolova::Triangle>(triangle);
  sokolova::shap3 polygonPtr = std::make_shared<sokolova::Polygon>(polygon);
  sokolova::CompositeShape compositeShape(rectanglePtr);
  compositeShape.add(circlePtr);
  compositeShape.add(trianglePtr);
  compositeShape.add(polygonPtr);

  BOOST_CHECK_THROW(compositeShape.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.remove(5), std::out_of_range);
  BOOST_CHECK_THROW(compositeShape.remove(-2), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(scale)
{
  sokolova::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  sokolova::Circle circle(6.0, { 3.0, 3.0 });
  sokolova::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  sokolova::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t qV = sizeof(fig) / sizeof(fig[0]);
  sokolova::Polygon polygon(qV, fig);
  sokolova::shap3 rectanglePtr = std::make_shared<sokolova::Rectangle>(rectangle);
  sokolova::shap3 circlePtr = std::make_shared<sokolova::Circle>(circle);
  sokolova::shap3 trianglePtr = std::make_shared<sokolova::Triangle>(triangle);
  sokolova::shap3 polygonPtr = std::make_shared<sokolova::Polygon>(polygon);
  sokolova::CompositeShape compositeShape(rectanglePtr);
  compositeShape.add(circlePtr);
  compositeShape.add(trianglePtr);
  compositeShape.add(polygonPtr);

  const double oldA = compositeShape.getArea();
  const double k = 9;
  compositeShape.scale(9);
  BOOST_CHECK_CLOSE(oldA * k * k, compositeShape.getArea(), EPS);
  const double oldAA = compositeShape.getArea();
  const double kk = 0.5;
  compositeShape.scale(0.5);
  BOOST_CHECK_CLOSE(oldAA * kk * kk, compositeShape.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  sokolova::Rectangle testRectangle({ 2.0, 4.0, {5.0, 5.0} });
  sokolova::Circle testCircle(1.0, {5.0, 5.0});
  sokolova::shap3 rectanglePtr = std::make_shared<sokolova::Rectangle>(testRectangle);
  sokolova::shap3 circlePtr = std::make_shared<sokolova::Circle>(testCircle);

  sokolova::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.add(rectanglePtr);

  const double testArea = testCompositeShape.getArea();
  const sokolova::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double angle = 90;

  testCompositeShape.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testCompositeShape.getFrameRect().width / 2, EPS);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCompositeShape.getFrameRect().height * 2, EPS);
  BOOST_CHECK_CLOSE(testArea, testCompositeShape.getArea(), EPS);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testCompositeShape.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testCompositeShape.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
