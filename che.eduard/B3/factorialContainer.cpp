#include "factorialContainer.hpp"

const size_t FACTORIAL_BEGIN_INDEX = 1;
const size_t FACTORIAL_AFTER_END_INDEX = 11;

che::FactorialContainer::FactorialContainer(){
}

che::FactorialContainer::Iterator che::FactorialContainer::begin(){
  return {FACTORIAL_BEGIN_INDEX};
}

che::FactorialContainer::Iterator che::FactorialContainer::end(){
  return {FACTORIAL_AFTER_END_INDEX};
}

che::FactorialContainer::Iterator::Iterator():
  Iterator(FACTORIAL_BEGIN_INDEX)
{
}

che::FactorialContainer::Iterator::Iterator(size_t number):
  number_(number)
{
}

size_t che::FactorialContainer::Iterator::calculateFactorial(size_t num) const {
  size_t result = 1;
  for (size_t i = 1; i <= num; i++){
    result *= i;
  }
  return result;
}

size_t che::FactorialContainer::Iterator::operator*() const{
  return calculateFactorial(number_);
}

size_t che::FactorialContainer::Iterator::operator->() const{
  return calculateFactorial(number_);
}

che::FactorialContainer::Iterator& che::FactorialContainer::Iterator::operator++(){
  if (number_ < FACTORIAL_AFTER_END_INDEX){
    number_++;
  }
  return *this;
}

che::FactorialContainer::Iterator che::FactorialContainer::Iterator::operator++(int)
{
  const Iterator newIt = *this;

  if (number_ < FACTORIAL_AFTER_END_INDEX){
    number_++;
  }
  return newIt;
}


che::FactorialContainer::Iterator& che::FactorialContainer::Iterator::operator--(){
  if (number_ > FACTORIAL_BEGIN_INDEX){
    number_--;
  }
  return *this;
}

che::FactorialContainer::Iterator che::FactorialContainer::Iterator::operator--(int)
{
  const Iterator tmpIterator = *this;

  if (number_ > FACTORIAL_BEGIN_INDEX){
    number_--;
  }
  return tmpIterator;
}

bool che::FactorialContainer::Iterator::operator==(const Iterator& rhs) const{
  return number_ == rhs.number_;
}

bool che::FactorialContainer::Iterator::operator!=(const Iterator& rhs) const{
  return number_ != rhs.number_;
}

