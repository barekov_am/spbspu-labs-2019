#ifndef TASKS_HPP
#define TASKS_HPP

namespace che {
  int task1();
  int task2();
}

#endif
