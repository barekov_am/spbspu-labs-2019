#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

const double ACCURACY = 0.0001;

BOOST_AUTO_TEST_SUITE(Rectangle_Testing)

BOOST_AUTO_TEST_CASE(Rectangle_Immutability)
{
  che::Rectangle testRectangle({ 4.0, 9.0 }, 2.0, 4.0);
  const che::rectangle_t frameBeforeMoving = testRectangle.getFrameRect();
  const double  areaBeforeMoving = testRectangle.getArea();

  testRectangle.move(2.0, 3.0);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, testRectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, testRectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMoving, testRectangle.getArea(), ACCURACY);

  testRectangle.move({ 2.0, 4.0 });
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, testRectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, testRectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMoving, testRectangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(Scale_Testing)
{
  che::Rectangle testRectangle({ 4.0, 9.0 }, 2.0, 4.0);
  const double areaBeforeScaling = testRectangle.getArea();
  const double coef = 2.2;
  testRectangle.scale(coef);
  BOOST_CHECK_CLOSE(testRectangle.getArea(), areaBeforeScaling * coef * coef, ACCURACY);
}

BOOST_AUTO_TEST_CASE(Invalid_Values)
{
  BOOST_CHECK_THROW(che::Rectangle testRectangle({ 4.0, 9.0 }, -2.0, 4.0), std::invalid_argument);
  che::Rectangle testRectangle({ 4.0, 9.0 }, 2.0, 4.0);
  BOOST_CHECK_THROW(testRectangle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleTestRotate)
{
  che::Rectangle rect({ 0, 0 }, 4, 4);
  const double rectangleAreaBeforeRotating = rect.getArea();
  const che::rectangle_t frameBeforeRotating = rect.getFrameRect();

  rect.rotate(45);
  double rectangleAreaAfterRotating = rect.getArea();
  che::rectangle_t frameAfterRotating = rect.getFrameRect();
  double sq = 1.41421356;

  BOOST_CHECK_CLOSE(rectangleAreaBeforeRotating, rectangleAreaAfterRotating, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.x, frameAfterRotating.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.y, frameAfterRotating.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(frameAfterRotating.width, 4 * sq, ACCURACY);
  BOOST_CHECK_CLOSE(frameAfterRotating.height, 4 * sq, ACCURACY);

}

BOOST_AUTO_TEST_SUITE_END()
