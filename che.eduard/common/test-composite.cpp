#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double ACCURACY = 0.0001;

BOOST_AUTO_TEST_SUITE(Composite_Testing)

BOOST_AUTO_TEST_CASE(compositeShape_Immutability)
{
  che::Shape::ptr rectangle = std::make_shared<che::Rectangle>(che::point_t{ 4.0, 9.0 }, 2.0, 4.0);
  che::Shape::ptr circle = std::make_shared<che::Circle>(che::point_t{ 2.0, 3.5 }, 1.5);
  che::CompositeShape compShape(rectangle);
  compShape.add(circle);
  const double areaBeforeMoving = compShape.getArea();
  const che::rectangle_t frameBeforeMoving = compShape.getFrameRect();
  compShape.move(1.5, 3.0);
  che::rectangle_t frameAfterMoving = compShape.getFrameRect();
  double areaAfterMoving = compShape.getArea();

  BOOST_CHECK_CLOSE(areaAfterMoving, areaBeforeMoving, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, frameAfterMoving.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, frameAfterMoving.width, ACCURACY);

  compShape.move({ 3.5, 1.2 });
  frameAfterMoving = compShape.getFrameRect();
  areaAfterMoving = compShape.getArea();

  BOOST_CHECK_CLOSE(areaAfterMoving, areaBeforeMoving, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, frameAfterMoving.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, frameAfterMoving.width, ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShape_scale_testing1)
{
  che::Shape::ptr rectangle = std::make_shared<che::Rectangle>(che::point_t{ 4.0, 9.0 }, 2.0, 4.0);
  che::Shape::ptr circle = std::make_shared<che::Circle>(che::point_t{ 2.0, 3.5 }, 1.5);
  che::CompositeShape compShape(rectangle);
  compShape.add(circle);
  const che::rectangle_t frameBeforeScale = compShape.getFrameRect();
  const double coef = 2.5;
  compShape.scale(coef);
  che::rectangle_t frameAfterScale = compShape.getFrameRect();

  BOOST_CHECK_CLOSE(frameBeforeScale.height * coef, frameAfterScale.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeScale.width * coef, frameAfterScale.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.x, frameAfterScale.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.y, frameAfterScale.pos.y, ACCURACY);

}

BOOST_AUTO_TEST_CASE(compositeShape_scale_testing2)
{
  che::Shape::ptr rectangle = std::make_shared<che::Rectangle>(che::point_t{ 4.0, 9.0 }, 2.0, 4.0);
  che::Shape::ptr circle = std::make_shared<che::Circle>(che::point_t{ 2.0, 3.5 }, 1.5);
  che::CompositeShape compShape(rectangle);
  compShape.add(circle);
  const che::rectangle_t frameBeforeScale = compShape.getFrameRect();
  const double coef = 0.5;
  compShape.scale(coef);
  che::rectangle_t frameAfterScale = compShape.getFrameRect();

  BOOST_CHECK_CLOSE(frameBeforeScale.height * coef, frameAfterScale.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeScale.width * coef, frameAfterScale.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.x, frameAfterScale.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.y, frameAfterScale.pos.y, ACCURACY);

}

BOOST_AUTO_TEST_CASE(compositeShape_invalid_values)
{
  che::Shape::ptr rectangle = std::make_shared<che::Rectangle>(che::point_t{ 4.0, 9.0 }, 2.0, 4.0);
  che::Shape::ptr circle = std::make_shared<che::Circle>(che::point_t{ 2.0, 3.5 }, 1.5);
  che::CompositeShape compShape(rectangle);
  compShape.add(circle);

  BOOST_CHECK_THROW(compShape.scale(-2), std::invalid_argument);

  che::CompositeShape newcompShape;
  BOOST_CHECK_THROW(newcompShape.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(newcompShape.getArea(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(compositeShape_exeptions_testing)
{
  che::Shape::ptr rectangle = std::make_shared<che::Rectangle>(che::point_t{ 4.0, 9.0 }, 2.0, 4.0);
  che::Shape::ptr circle = std::make_shared<che::Circle>(che::point_t{ 2.0, 3.5 }, 1.5);
  che::CompositeShape compShape(rectangle);
  compShape.add(circle);

  BOOST_CHECK_THROW(compShape.remove(10), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.remove(-10), std::invalid_argument);

  compShape.remove(1);
  BOOST_CHECK_THROW(compShape[1], std::out_of_range);

}

BOOST_AUTO_TEST_CASE(copyConstructor_testing)
{
  che::Shape::ptr rectangle = std::make_shared<che::Rectangle>(che::point_t{ 4.0, 9.0 }, 2.0, 4.0);
  che::Shape::ptr circle = std::make_shared<che::Circle>(che::point_t{ 2.0, 3.5 }, 1.5);
  che::CompositeShape compShape(rectangle);
  compShape.add(circle);
  const che::rectangle_t frameRect = compShape.getFrameRect();

  che::CompositeShape copyCompShape(compShape);
  const che::rectangle_t copyFrameRect = copyCompShape.getFrameRect();

  BOOST_CHECK_CLOSE(compShape.getArea(), copyCompShape.getArea(), ACCURACY);
  BOOST_CHECK_EQUAL(compShape.getSize(), copyCompShape.getSize());
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, ACCURACY);
 
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(emptyCompositeShape_testing)
{
  che::CompositeShape emptycompShape;

  BOOST_CHECK_THROW(emptycompShape.move(2, 6), std::logic_error);
  BOOST_CHECK_THROW(emptycompShape.move({2, 3}), std::logic_error);
  BOOST_CHECK_THROW(emptycompShape.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(emptycompShape.scale(2.5), std::logic_error);
}

BOOST_AUTO_TEST_CASE(compShapeAfterRotating_testing)
{
  che::Shape::ptr rectanglePtr = std::make_shared<che::Rectangle>(che::point_t {-1, 0}, 2, 2);
  che::Shape::ptr circle = std::make_shared<che::Circle>(che::point_t {1, 0}, 1);
  che::CompositeShape compShape(rectanglePtr);
  compShape.add(circle);

  const double compShapeAreaBeforeRotating = compShape.getArea();
  const che::rectangle_t frameBeforeRotating = compShape.getFrameRect();

  compShape.rotate(90);
  double compShapeAreaAfterRotating = compShape.getArea();
  che::rectangle_t frameAfterRotating = compShape.getFrameRect();

  BOOST_CHECK_CLOSE(compShapeAreaBeforeRotating, compShapeAreaAfterRotating, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.x, frameAfterRotating.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.y, frameAfterRotating.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeRotating.width, frameAfterRotating.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeRotating.height, frameAfterRotating.width, ACCURACY);

  compShape.rotate(-90);
  compShapeAreaAfterRotating = compShape.getArea();
  frameAfterRotating = compShape.getFrameRect();

  BOOST_CHECK_CLOSE(compShapeAreaBeforeRotating, compShapeAreaAfterRotating, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.x, frameAfterRotating.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.y, frameAfterRotating.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeRotating.width, frameAfterRotating.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeRotating.height, frameAfterRotating.height, ACCURACY);
  
  compShape.rotate(45);
  compShapeAreaAfterRotating = compShape.getArea();
  
  BOOST_CHECK_CLOSE(compShapeAreaBeforeRotating, compShapeAreaAfterRotating, ACCURACY);
  BOOST_CHECK_CLOSE(compShape.getShapes()[0]->getFrameRect().pos.x, compShape.getShapes()[0]->getFrameRect().pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(compShape.getShapes()[1]->getFrameRect().pos.x, compShape.getShapes()[1]->getFrameRect().pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(compShape.getShapes()[0]->getFrameRect().pos.x, - compShape.getShapes()[1]->getFrameRect().pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(compShape.getShapes()[0]->getFrameRect().pos.y, - compShape.getShapes()[1]->getFrameRect().pos.y, ACCURACY);
}
BOOST_AUTO_TEST_CASE(ExceptionAfterOperator_testing)
{
  che::Shape::ptr rectanglePtr = std::make_shared<che::Rectangle>(che::point_t {4, 7}, 2, 12);
  che::Shape::ptr circle = std::make_shared<che::Circle>(che::point_t {2, 1}, 10);
  che::CompositeShape compShape(rectanglePtr);
  compShape.add(circle);

  BOOST_CHECK_THROW(compShape[2], std::out_of_range);
  BOOST_CHECK_THROW(compShape[-2], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
