#include "matrix.hpp"
#include <stdexcept>
#include <cmath>

che::Matrix::Matrix():
  lines_(0),
  columns_(0)
{}

che::Matrix::Matrix(const Matrix &copyMatrix):
  lines_(copyMatrix.lines_),
  columns_(copyMatrix.columns_),
  shapes_(std::make_unique<Shape::ptr[]>(copyMatrix.lines_ * copyMatrix.columns_))
{
  for (size_t i = 0; i < (lines_  * columns_); i++)
  {
    shapes_[i] = copyMatrix.shapes_[i];
  }
}

che::Matrix::Matrix(che::Matrix &&moveMatrix):
  lines_(moveMatrix.lines_),
  columns_(moveMatrix.columns_),
  shapes_(std::move(moveMatrix.shapes_))
{
  moveMatrix.lines_ = 0;
  moveMatrix.columns_ = 0;
}

che::Matrix &che::Matrix::operator =(const Matrix &copyMatrix)
{
  if (this != &copyMatrix)
  {
    Shape::array tmpShapes(std::make_unique<Shape::ptr[]>(copyMatrix.lines_ * copyMatrix.columns_));
    lines_ = copyMatrix.lines_;
    columns_ = copyMatrix.columns_;
    for (size_t i = 0; i < (lines_ * columns_); i++)
    {
      tmpShapes[i] = copyMatrix.shapes_[i];
    }

    shapes_.swap(tmpShapes);
  }

  return *this;
}

che::Matrix &che::Matrix::operator =(Matrix &&moveMatrix)
{
  if (this != &moveMatrix)
  {
    lines_ = moveMatrix.lines_;
    columns_ = moveMatrix.columns_;
    shapes_ = std::move(moveMatrix.shapes_);
    moveMatrix.lines_ = 0;
    moveMatrix.columns_ = 0;
  }

  return *this;
}

che::Shape::array che::Matrix::operator [](size_t index) const
{
  if (index >= lines_)
  {
    throw std::out_of_range("Index out of range");
  }

  Shape::array tmpShapes(std::make_unique<Shape::ptr[]>(columns_));
  for (size_t i = 0; i < columns_; i++)
  {
    tmpShapes[i] = shapes_[index * columns_ + i];
  }

  return tmpShapes;
}

bool che::Matrix::operator==(const Matrix &shapesMatrix) const
{
  if ((lines_ != shapesMatrix.lines_) || (columns_ != shapesMatrix.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    if (shapes_[i] != shapesMatrix.shapes_[i])
    {
      return false;
    }
  }

  return true;
}

bool che::Matrix::operator!=(const Matrix &shapesMatrix) const
{
  return !(*this == shapesMatrix);
}

void che::Matrix::add(Shape::ptr shape, size_t line, size_t column)
{
  if (line > lines_)
  {
    throw std::out_of_range("Line index out of range");
  }

  if (column > columns_)
  {
    throw std::out_of_range("Column index out of range");
  }

  size_t newLine = (line == lines_) ? (lines_ + 1) : (lines_);
  size_t newColumn = (column == columns_) ? (columns_ + 1) : (columns_);

  Shape::array tmpShapes(std::make_unique<Shape::ptr[]> (newLine * newColumn));

  for (size_t i = 0; i < newLine; i++)
  {
    for (size_t j = 0; j < newColumn; ++j)
    {
      if ((lines_ == i) || (columns_ == j))
      {
        tmpShapes[i * newColumn + j] = nullptr;
      }
      else
      {
        tmpShapes[i * newColumn + j] = shapes_[i * columns_ + j];
      }
    }
  }

  tmpShapes[line * newColumn + column] = shape;
  shapes_.swap(tmpShapes);
  lines_ = newLine;
  columns_ = newColumn;
}

std::size_t che::Matrix::getLines() const
{
  return lines_;
}

std::size_t che::Matrix::getColumns() const
{
  return columns_;
}

bool che::Matrix::containsObject(size_t line, size_t column) const
{
  if ((line >= lines_) || (column >= columns_))
  {
    return false;
  }

  if (shapes_[line * columns_ + column] == nullptr)
  {
    return false;
  }

  return true;
}

