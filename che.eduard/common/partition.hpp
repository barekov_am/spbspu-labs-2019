#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace che
{
  Matrix part(const che::Shape::array &shapes, size_t count);
  Matrix part(const CompositeShape &compositeShape);

  bool intersect(const rectangle_t &firstShape, const rectangle_t &secondShape);
}

#endif 

