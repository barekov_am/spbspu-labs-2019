#include "partition.hpp"

#include <cmath>

che::Matrix che::part(const che::Shape::array &shapes, size_t count)
{
  Matrix matrix;
  size_t lines = 0;
  size_t columns = 0;

  for (size_t i = 0; i < count; i++)
  {
    for (size_t j = 0; j < matrix.getLines(); j++)
    {
      for (size_t k = 0; k < matrix.getColumns(); k++)
      {
        if (matrix[j][k] == nullptr)
        {
          lines = j;
          columns = k;
          break;
        }

        if (intersect(matrix[j][k]->getFrameRect(), shapes[i]->getFrameRect()))
        {
          lines = j + 1;
          columns = 0;
          break;
        }

        lines = j;
        columns = k + 1;
      }

      if (lines == j)
      {
        break;
      }
    }

    matrix.add(shapes[i], lines, columns);
  }

  return matrix;
}

che::Matrix che::part(const che::CompositeShape &compositeShape)
{
  return part(compositeShape.getShapes(), compositeShape.getSize());
}

bool che::intersect(const che::rectangle_t &firstShape, const che::rectangle_t &secondShape)
{
  const double centerDistanceX = fabs(firstShape.pos.x - secondShape.pos.x);
  const double centerDistanceY = fabs(firstShape.pos.y - secondShape.pos.y);

  const double lengthX = (firstShape.width + secondShape.width) / 2;
  const double lengthY = (firstShape.height + secondShape.height) / 2;

  const bool firstCondition = (centerDistanceX < lengthX);
  const bool secondCondition = (centerDistanceY < lengthY);

  return firstCondition && secondCondition;
}
