#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include <memory>
#include "shape.hpp"

namespace che
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &copyShape);
    CompositeShape(CompositeShape &&movedShape);
    CompositeShape(const ptr &shape);
    ~CompositeShape() = default;
    
    CompositeShape &operator =(const CompositeShape &copyShape);
    CompositeShape &operator =(CompositeShape &&movedShape);
    ptr operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(double dx, double dy) override;
    void scale(double coef) override;

    void add(ptr &shape);
    void remove(size_t index);
    size_t getSize() const;
    array getShapes() const;
    void rotate(double angle) override;

  
  private:
    size_t count_;
    array shapes_;
  };
}
#endif
