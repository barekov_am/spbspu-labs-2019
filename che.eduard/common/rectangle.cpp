#include "rectangle.hpp"
#define _USE_MATH_DEFINES
#include <cmath>
#include <stdexcept>

che::Rectangle::Rectangle(point_t pos, double width, double height):
  height_(height),
  width_(width),
  angle_(0),
  pos_(pos)
{
  if (height_ <= 0)
  {
    throw std::invalid_argument("Height can't be negativ");
  }
  if (width_ <= 0)
  {
    throw std::invalid_argument("Width can't be negativ");
  }
}

double che::Rectangle::getArea() const
{
  return (height_ * width_);
}

che::rectangle_t che::Rectangle::getFrameRect() const
{
  const double cosA = std::abs(std::cos(angle_ * M_PI / 180));
  const double sinA = std::abs(std::sin(angle_ * M_PI / 180));

  double width = height_ * sinA + width_ * cosA;
  double height = height_ * cosA + width_ * sinA;

  return {width, height, pos_};
}

void che::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void che::Rectangle::move(const che::point_t &point)
{
  pos_ = point;
}

void che::Rectangle::scale(double coef)
{
  if (coef <= 0)
  {
    throw std::invalid_argument("invalid coef");
  }
  height_ *= coef;
  width_ *= coef;
}

void che::Rectangle::rotate(double angle)
{
  angle_ += angle;
}

