#include <memory>
#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "matrix.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(matrixTests)

BOOST_AUTO_TEST_CASE(CopyConstructor_testing)
{
  che::Shape::ptr rectPtr = std::make_shared<che::Rectangle>(che::point_t {2, 5}, 1, 10);
  che::CompositeShape compShape(rectPtr);
  che::Matrix copyMatrix = part(compShape);

  che::Matrix matrix(copyMatrix);

  BOOST_CHECK_EQUAL(matrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(matrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(matrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(CopyOperator_testing)
{
  che::Shape::ptr rectPtr = std::make_shared<che::Rectangle>(che::point_t {2, 5}, 1, 10);
  che::CompositeShape compShape(rectPtr);
  che::Matrix copyMatrix = part(compShape);

  che::Matrix matrix;
  matrix = copyMatrix;

  BOOST_CHECK_EQUAL(matrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(matrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(matrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(MoveConstructor_testing)
{
  che::Shape::ptr rectPtr = std::make_shared<che::Rectangle>(che::point_t {2, 5}, 1, 10);
  che::CompositeShape compShape(rectPtr);
  che::Matrix matrix = part(compShape);

  che::Matrix copyOfMatrix(matrix);
  che::Matrix moveMatrix(std::move(matrix));

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyOfMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyOfMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyOfMatrix);
  BOOST_CHECK_EQUAL(matrix.getLines(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(MoveOperator_testing)
{
  che::Shape::ptr rectPtr = std::make_shared<che::Rectangle>(che::point_t {2, 5}, 1, 10);
  che::CompositeShape compShape(rectPtr);
  che::Matrix matrix = part(compShape);

  che::Matrix copyOfMatrix(matrix);
  che::Matrix moveMatrix;
  moveMatrix = std::move(matrix);

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyOfMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyOfMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyOfMatrix);
  BOOST_CHECK_EQUAL(matrix.getLines(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(EqualOperator_testing)
{
  che::Shape::ptr rect1Ptr = std::make_shared<che::Rectangle>(che::point_t {1, 2}, 3, 4);
  che::Shape::ptr rect2Ptr = std::make_shared<che::Rectangle>(che::point_t {7, 5}, 3, 2);
  che::Shape::ptr circ1Ptr = std::make_shared<che::Circle>(che::point_t {3, 2}, 1);
  che::Shape::ptr circ2Ptr = std::make_shared<che::Circle>(che::point_t {1, 2}, 5);

  che::CompositeShape compShape(rect1Ptr);
  compShape.add(circ1Ptr);
  compShape.add(rect2Ptr);

  che::Matrix matrix = part(compShape);
  che::Matrix equalMatrix(matrix);

  che::Matrix unequalMatrix(matrix);
  unequalMatrix.add(circ2Ptr, 1, 1);

  BOOST_CHECK(matrix == equalMatrix);
  BOOST_CHECK(matrix != unequalMatrix);
}

BOOST_AUTO_TEST_CASE(ExceptionAfterOperator_testing)
{
  che::Shape::ptr rect1Ptr = std::make_shared<che::Rectangle>(che::point_t {1, 2}, 3, 4);
  che::Shape::ptr rect2Ptr = std::make_shared<che::Rectangle>(che::point_t {7, 5}, 3, 2);
  che::Shape::ptr circ1Ptr = std::make_shared<che::Circle>(che::point_t {1, 2}, 3);
  che::Shape::ptr circ2Ptr = std::make_shared<che::Circle>(che::point_t {1, 2}, 5);

  che::CompositeShape compShape(rect1Ptr);
  compShape.add(circ1Ptr);
  compShape.add(rect2Ptr);
  compShape.add(circ2Ptr);

  che::Matrix matrix = part(compShape);

  BOOST_CHECK_THROW(matrix[10][5], std::out_of_range);
  BOOST_CHECK_NO_THROW(matrix[2][1]);
}

BOOST_AUTO_TEST_SUITE_END()

