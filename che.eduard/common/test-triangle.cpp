#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"

const double ACCURACY = 0.0001;

BOOST_AUTO_TEST_SUITE(Triangle_Testing)

BOOST_AUTO_TEST_CASE(Triangle_Immutability)
{
  che::Triangle testTriangle({ 4.5, -1.2 }, { 1.3, 3.5 }, { 5.6, 3.2 });
  const che::rectangle_t frameBeforeMoving = testTriangle.getFrameRect();
  const double  areaBeforeMoving = testTriangle.getArea();

  testTriangle.move({ 3.4, 4.3 });
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, testTriangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, testTriangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMoving, testTriangle.getArea(), ACCURACY);

  testTriangle.move(6, -2.1);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, testTriangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, testTriangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMoving, testTriangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(Scale_Testing)
{
  che::Triangle testTriangle({ 2.5, -1.5 }, { 1.0, 4.5 }, { 2.6, 5.2 });
  const double areaBeforeScaling = testTriangle.getArea();
  const double coef = 6.7;

  testTriangle.scale(coef);
  BOOST_CHECK_CLOSE(areaBeforeScaling * coef * coef, testTriangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  BOOST_CHECK_THROW(che::Triangle({ 0, -2.2 }, { 0, 4.0 }, { 0, 8.0 }), std::invalid_argument);
  che::Triangle testTriangle({ 9.3, -3.2 }, { 10.0, 5.6 }, { 3.6, 2.9 });
  BOOST_CHECK_THROW(testTriangle.scale(-2.0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(triangleTestRotate)
{
  che::Triangle triangle({ 4, 2 }, { 3, 5 }, { 1, 8 });
  double area = triangle.getArea();
  triangle.rotate(45);
  BOOST_CHECK_CLOSE(area, triangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
