#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace che
{
 class Rectangle : public Shape
 {
 public:
   Rectangle(point_t pos, double width, double height);

   double getArea() const override;
   rectangle_t getFrameRect() const override;
   void move(const point_t & point) override;
   void move(double dx, double dy) override;
   void scale(double coef) override;
   void rotate(double angle) override;


 private:
   double height_;
   double width_;
   double angle_;
   point_t pos_;

 };
}
#endif 
