#include <memory>
#include <boost/test/auto_unit_test.hpp>
#include "matrix.hpp"
#include "partition.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(partitionMetodsTests)

BOOST_AUTO_TEST_CASE(partition_testing)
{
  const che::Rectangle rectangle1({4, 3}, 2, 1);
  che::Shape::ptr rectangle1Ptr = std::make_shared<che::Rectangle>(rectangle1);
  const che::Rectangle rectangle2({12, 10}, 5, 4);
  che::Shape::ptr rectangle2Ptr = std::make_shared<che::Rectangle>(rectangle2);
  const che::Circle circ({1, 2}, 3);
  che::Shape::ptr circlePtr = std::make_shared<che::Circle>(circ);

  che::CompositeShape compShape(rectangle1Ptr);
  che::Matrix matrix1 = che::part(compShape);

  compShape.add(circlePtr);
  che::Matrix matrix2 = che::part(compShape);

  compShape.add(rectangle2Ptr);
  che::Matrix matrix3 = che::part(compShape);

  BOOST_CHECK_EQUAL(matrix1.getLines(), 1);
  BOOST_CHECK_EQUAL(matrix1.getColumns(), 1);

  BOOST_CHECK_EQUAL(matrix2.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix2.getColumns(), 1);

  BOOST_CHECK_EQUAL(matrix3.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix3.getColumns(), 2);
}

BOOST_AUTO_TEST_CASE(intersection_testing)
{
  const che::Rectangle rectangle1({1, 2}, 3, 4);
  const che::Rectangle rectangle2({12, 5}, 10, 1);
  const che::Circle circle1({3, 2}, 1);
  const che::Circle circle2({2, 3}, 5);

  BOOST_CHECK(intersect(rectangle1.getFrameRect(), circle1.getFrameRect()));
  BOOST_CHECK(intersect(rectangle1.getFrameRect(), circle2.getFrameRect()));
  BOOST_CHECK(!intersect(rectangle1.getFrameRect(), rectangle2.getFrameRect()));
  BOOST_CHECK(!intersect(circle1.getFrameRect(), rectangle2.getFrameRect()));
  BOOST_CHECK(!intersect(circle2.getFrameRect(), rectangle2.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()

