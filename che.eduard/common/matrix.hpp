#ifndef MATRIX_HPP
#define MATRIX_HPP
#include "shape.hpp"
#include <memory>

namespace che
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &copyMatrix);
    Matrix(Matrix &&moveMatrix);
    ~Matrix() = default;

    Matrix &operator =(const Matrix &copyMatrix);
    Matrix &operator =(Matrix &&moveMatrix);
    Shape::array operator [](size_t index) const;
    bool operator ==(const Matrix &shapesMatrix) const;
    bool operator !=(const Matrix &shapesMatrix) const;

    void add(Shape::ptr shape, size_t line, size_t column);
    size_t getLines() const;
    size_t getColumns() const;
    bool containsObject(size_t line, size_t column) const;

  private:
    size_t lines_;
    size_t columns_;
    Shape::array shapes_;
  };
};

#endif

