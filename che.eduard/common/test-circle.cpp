#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double ACCURACY = 0.0001;

BOOST_AUTO_TEST_SUITE(Circle_Testing)

BOOST_AUTO_TEST_CASE(Circle_Immutability)
{
  che::Circle testCircle({ 2.0, 3.5 }, 1.5);
  const che::rectangle_t circleBeforeMoving = testCircle.getFrameRect();
  const double areaBeforeMoving = testCircle.getArea();
  testCircle.move(1.5, 3.0);
  BOOST_CHECK_CLOSE(circleBeforeMoving.width, testCircle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(circleBeforeMoving.height, testCircle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMoving, testCircle.getArea(), ACCURACY);

  testCircle.move({ 3.5, 1.2 });
  BOOST_CHECK_CLOSE(circleBeforeMoving.width, testCircle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(circleBeforeMoving.height, testCircle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMoving, testCircle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(Scale_Tetsting)
{
  che::Circle testCircle({ 2.0, 3.5 }, 1.5);
  const double areaBeforeScaling = testCircle.getArea();
  const double coef = 1.8;
  testCircle.scale(coef);
  BOOST_CHECK_CLOSE(testCircle.getArea(), areaBeforeScaling * coef * coef, ACCURACY);
}

BOOST_AUTO_TEST_CASE(Invalid_Values)
{
  BOOST_CHECK_THROW(che::Circle({ 2.0,3.0 }, -1.5), std::invalid_argument);
  che::Circle testCircle1({ 3.0, 3.0 }, 2.0);
  BOOST_CHECK_THROW(testCircle1.scale(-3.0), std::invalid_argument);
  che::Circle testCircle2({ 2.0, 2.0 }, 3.0);
  BOOST_CHECK_THROW(testCircle1.scale(-2.0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(circleTestRotate)
{
  che::Circle testCircle({2.0, 3.5}, 1.5);
  const double cirlceAreaBefore = testCircle.getArea();
  const che::rectangle_t frameRectBeforeRotate = testCircle.getFrameRect();
  double angle = 90.0;
  testCircle.rotate(angle);
  double circleAreaAfter = testCircle.getArea();
  che::rectangle_t frameRectAfterRotate = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(cirlceAreaBefore, circleAreaAfter, ACCURACY);

  angle = -45;
  testCircle.rotate(angle);
  circleAreaAfter = testCircle.getArea();
  frameRectAfterRotate = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(cirlceAreaBefore, circleAreaAfter, ACCURACY);

  angle = 180;
  testCircle.rotate(angle);
  circleAreaAfter = testCircle.getArea();
  frameRectAfterRotate = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.height, frameRectAfterRotate.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.width, frameRectAfterRotate.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.x, frameRectAfterRotate.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBeforeRotate.pos.y, frameRectAfterRotate.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(cirlceAreaBefore, circleAreaAfter, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
