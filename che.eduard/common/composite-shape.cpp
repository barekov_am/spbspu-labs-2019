#include "composite-shape.hpp"
#include <stdexcept>
#include <memory>
#include <algorithm>
#define _USE_MATH_DEFINES
#include <cmath>
che::CompositeShape::CompositeShape() :
  count_(0)
{
}

che::CompositeShape::CompositeShape(const CompositeShape& copyShape) :
  count_(copyShape.count_),
  shapes_(std::make_unique<ptr[]>(copyShape.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i] = copyShape.shapes_[i];
  }
}

che::CompositeShape::CompositeShape(CompositeShape &&movedShape) :
  count_(movedShape.count_),
  shapes_(std::move(movedShape.shapes_))
{
  movedShape.count_ = 0;
}

che::CompositeShape::CompositeShape(const ptr &shape) :
  count_(1),
  shapes_(std::make_unique<ptr[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape can't be empty");
  }
  shapes_[0] = shape;
}

che::CompositeShape &che::CompositeShape::operator =(const CompositeShape &copyShape)
{
  if (this != &copyShape)
  {
    count_ = copyShape.count_;
    array shapesArray(std::make_unique<ptr[]>(copyShape.count_));
    for (size_t i = 0; i < count_; i++)
    {
      shapesArray[i] = copyShape.shapes_[i];
    }

    shapes_.swap(shapesArray);
  }


  return *this;
}

che::CompositeShape &che::CompositeShape::operator =(CompositeShape &&movedShape)
{
  if (this != &movedShape)
  {
    count_ = movedShape.count_;
    shapes_ = std::move(movedShape.shapes_);
    movedShape.count_ = 0;
  }
  return *this;
}

che::Shape::ptr che::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }
  return shapes_[index];
}

double che::CompositeShape::getArea() const
{
  if (count_ <= 0)
  {
    throw std::logic_error("Invalid count");
  }
  double area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

che::rectangle_t che::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Invalid count");
  }
  che::rectangle_t coShape = shapes_[0]->getFrameRect();
  double minX = coShape.pos.x - coShape.width / 2;
  double minY = coShape.pos.y - coShape.height / 2;

  double maxX = coShape.pos.x + coShape.width / 2;
  double maxY = coShape.pos.y + coShape.height / 2;

  for (size_t i = 0; i < count_; i++)
  {
    coShape = shapes_[i]->getFrameRect();
    double coValue = coShape.pos.x - coShape.width / 2;
    minX = std::min(coValue, minX);
    coValue = coShape.pos.y - coShape.height / 2;
    minY = std::min(coValue, minY);
    coValue = coShape.pos.x + coShape.width / 2;
    maxX = std::max(coValue, maxX);
    coValue = coShape.pos.y + coShape.height / 2;
    maxY = std::max(coValue, maxY);
  }

  return{(maxX - minX), (maxY - minY), {(maxX + minX) / 2, (maxY + minY) / 2}};
}

void che::CompositeShape::move(const point_t &point)
{
  point_t frameRect = getFrameRect().pos;

  double dx = point.x - frameRect.x;
  double dy = point.y - frameRect.y;

  move(dx, dy);
}

void che::CompositeShape::move(double dx, double dy)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void che::CompositeShape::scale(double coef)
{
  if (coef <= 0)
  {
    throw std::invalid_argument("Invalid coef");
  }

  const che::point_t frameRect = getFrameRect().pos;

  for (size_t i = 0; i < count_; i++)
  {
    che::point_t center = shapes_[i]->getFrameRect().pos;
    const double dx = (center.x - frameRect.x) * (coef - 1);
    const double dy = (center.y - frameRect.y) * (coef - 1);
    shapes_[i]->move(dx, dy);
    shapes_[i]->scale(coef);
  }
}

void che::CompositeShape::add(ptr &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Pointer can't be null");
  }

  array shapesArray(std::make_unique<ptr[]>(count_ + 1));
  for(size_t i = 0; i < count_; i++)
  {
    shapesArray[i] = shapes_[i];
  }

  shapesArray[count_ ] = shape;
  count_++;
  shapes_.swap(shapesArray);

}

void che::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::invalid_argument("Invalid index");
  }
  count_--;
  for (size_t i = index; i < count_; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  shapes_[count_] = nullptr;
}

size_t che::CompositeShape::getSize() const
{
  return count_;
}

che::Shape::array che::CompositeShape::getShapes() const
{
  array tmpShapes(std::make_unique<ptr[]>(count_));

  for (size_t i = 0; i < count_; i++)
  {
    tmpShapes[i] = shapes_[i];
  }

  return tmpShapes;
}

void che::CompositeShape::rotate(double angle)
{
  const double cosA = std::abs(std::cos(angle * M_PI / 180));
  const double sinA = std::abs(std::sin(angle * M_PI / 180));
  const point_t center = getFrameRect().pos;

  for (size_t i = 0; i < count_; i++)
  {
    point_t tmpCenter = shapes_[i]->getFrameRect().pos;
    const double dx = (tmpCenter.x - center.x) * (cosA - 1) - (tmpCenter.y - center.y)  * sinA;
    const double dy = (tmpCenter.x - center.x) * sinA + (tmpCenter.y - center.y)  * (cosA - 1);

    shapes_[i]->move(dx, dy);
    shapes_[i]->rotate(angle);
  }
}

