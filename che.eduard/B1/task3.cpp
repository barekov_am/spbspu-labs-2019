#include <vector>
#include <iostream>
#include "detailing.hpp"


void task3()
{
  std::vector<int> vector;
  int i = -1;

  while (std::cin && !(std::cin >> i).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input failed");
    }

    if (i == 0)
    {
      break;
    }

    vector.push_back(i);
  }

  if (vector.empty())
  {
    return;
  }

  if (i != 0)
  {
    throw std::runtime_error("Waiting for 0!");
  }

  if (vector.back() == 1)
  {
    for (auto i = vector.begin(); i != vector.end();)
    {
      if (*i % 2 == 0)
      {
        i = vector.erase(i);
      }
      else
      {
        ++i;
      }
    }
  }
  else if (vector.back() == 2)
  {
    for (auto i = vector.begin(); i != vector.end();)
    {
      if (*i % 3 == 0)
      {
        i = vector.insert(++i, 3, 1) + 3;
      }
      else
      {
        ++i;
      }
    }
  }
  print(vector);
}
