#include <vector>
#include <memory>
#include <fstream>
#include <iostream>

const std::size_t INITCAPASITY = 64;

void task2(const char* fileName)
{
  std::ifstream inputFile(fileName);

  if (!inputFile)
  {
    throw std::runtime_error("Can not open file");
  }

  std::size_t size = INITCAPASITY;

  std::unique_ptr<char[], decltype(&free)> array(static_cast<char*>(malloc(size)), &free);

  if (!array)
  {
    throw std::runtime_error("Could not allocate memory.");
  }

  std::size_t index = 0;

  while (inputFile)
  {
    inputFile.read(&array[index], INITCAPASITY);
    index += inputFile.gcount();

    if (inputFile.gcount() == INITCAPASITY)
    {
      size += INITCAPASITY;
      std::unique_ptr<char[], decltype(&free)> tmp(static_cast<char*>(realloc(array.get(), size)), &free);

      if (!tmp)
      {
        throw std::runtime_error("Could not reallocate memory");
      }
      array.release();
      std::swap(array, tmp);
    }
    if (!inputFile.eof() && inputFile.fail())
    {
      throw std::runtime_error("Can not read file");
    }
  }

  inputFile.close();

  if (inputFile.is_open())
  {
    throw std::runtime_error("Can not close file");
  }

  std::vector<char> vector(&array[0], &array[index]);

  for (auto i : vector)
  {
    std::cout << i;
  }
}
