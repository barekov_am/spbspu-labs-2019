#include <vector>
#include <iostream>
#include <forward_list>
#include "detailing.hpp"


void task1(const char* direction)
{
  std::vector<int> collectionA;
  auto directionType = getDirection<int>(direction);
  int i = 0;
  
 while (std::cin >> i)
  {
    collectionA.push_back(i);
  }

  if ((!std::cin.eof()) && (std::cin.fail()))
  {
    throw std::runtime_error("Error while reading data.");
  }

  if (collectionA.empty())
  {
    return;
  }

  std::vector<int> collectionB = collectionA;
  std::forward_list<int> collectionC(collectionA.begin(), collectionA.end());

  sort<SortOperator>(collectionA, directionType);
  sort<SortAt>(collectionB, directionType);
  sort<SortIterator>(collectionC, directionType);

  print(collectionA);
  print(collectionB);
  print(collectionC);
}
