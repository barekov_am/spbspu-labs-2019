#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

void info(const che::Shape &shape)
{
  che::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "Shape area: " << shape.getArea() << "\n";
  std::cout << "Shape's center: " << frameRect.pos.x << ";" << frameRect.pos.y << "\n";
  std::cout << "Frame width: " << frameRect.width << "\n";
  std::cout << "Frame height: " << frameRect.height << "\n";
}

int main()
{
  std::cout << "Circle:\n";
  che::Circle circle({ 1, 1 }, 3);
  info(circle);
  std::cout << "Scaling by 2:\n";
  circle.scale(2);
  info(circle);
  std::cout << "Move circle to point (1, 5)\n";
  circle.move({ 1, 5 });
  info(circle);
   
  std::cout << "Rectangle:\n";
  che::Rectangle rectangle({2, 2}, 10, 10);
  info(rectangle);
  std::cout << "Scaling by 1.5:\n";
  rectangle.scale(1.5);
  info(rectangle);
  std::cout << "Move Rectangle to point (3, 1)\n";
  rectangle.move({ 3, 1 });
  info(rectangle);

  std::cout << "Triangle:\n";
  che::Triangle triangle{ {4, 8}, {6, 12}, {30, 5} };
  info(triangle);
  std::cout << "Scaling by 3:\n";
  triangle.scale(3);
  info(triangle);
  std::cout << "Move triangle to point (2, 4)\n";
  triangle.move({ 2, 4 });
  info(triangle);

  return 0;
}
