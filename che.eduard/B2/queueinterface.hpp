#ifndef QUEUEINTERFACE_HPP
#define QUEUEINTERFACE_HPP

#include "queue.hpp"
#include "iostream"

template <typename T>
bool QueueWithPriority<T>::operator ==(const QueueWithPriority& other) const
{
  return (q == other.q);
}

template <typename T>
bool QueueWithPriority<T>::operator !=(const QueueWithPriority& other) const
{
  return !(q == other.q);
}

template <typename T>
void QueueWithPriority<T>::putElementToQueue(const T& element, ElementPriority priority)
{
  switch (priority)
  {
  case ElementPriority::HIGH:
    q.insert(q.begin() + highCounter, element);
    ++highCounter;
    break;

  case ElementPriority::NORMAL:
    q.insert(q.end() - lowCounter, element);
    break;
  case ElementPriority::LOW:
    ++lowCounter;
    q.push_back(element);
    break;
  }
}

template <typename T>
void QueueWithPriority<T>::print()
{
  for (auto i : q)
  {
    std::cout << i << " ";
  }
  std::cout << '\n';
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  q.insert(q.begin() + highCounter, q.end() - lowCounter, q.end());
  q.erase(q.end() - lowCounter, q.end());
}

template <typename T>
T QueueWithPriority<T>::getElementFromQueue()
{
  return q.at(getCounter++);
}

template <typename T>
bool QueueWithPriority<T>::empty()
{
  return (q.empty() || getCounter == q.size());
}


#endif
