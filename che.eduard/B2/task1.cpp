#include <algorithm>
#include <sstream>
#include <vector>

#include "iostream"
#include "queueinterface.hpp"

void task1()
{
  QueueWithPriority<std::string> queue;
  std::string line;
  std::vector<std::string> commands = { "add", "get", "accelerate" };
  std::vector<std::string> priorities = { "low", "normal", "high" };

  while (std::getline(std::cin, line))
  {
    std::stringstream in(line);
    in >> line;

    switch (std::distance(commands.begin(), std::find(commands.begin(), commands.end(), line)))
    {
    case 0:
    {
      std::string priority;
      in >> priority;
      auto findResult = std::find(priorities.begin(), priorities.end(), priority);
      if (findResult != priorities.end())
      {
        std::string data;
        std::getline(in >> std::ws, data);
        if (data.empty())
        {
          std::cout << "<INVALID COMMAND>" << "\n";
        }

        queue.putElementToQueue(data, static_cast<ElementPriority>(std::distance(priorities.begin(), findResult)));
      }
      else
      {
        std::cout << "<INVALID COMMAND>" << "\n";
      }
      break;
    }
    case 1:
      queue.empty() ? std::cout << "<EMPTY>" << "\n" : std::cout << queue.getElementFromQueue() << "\n";
      break;
    case 2:
      queue.accelerate();
      break;
    default:
      std::cout << "<INVALID COMMAND>" << "\n";
    };
  }
}
