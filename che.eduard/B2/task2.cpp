#include <list>
#include <iostream>

const int MIN = 0;
const int MAX = 20;

void task2()
{
  std::list<int> list;
  int elem = -1;
  int count = 0;

  while (std::cin >> elem)
  {
    if (elem > MIN && elem <= MAX)
    {
      count++;
      if (count > MAX)
      {
        throw std::invalid_argument("Omitted due to size.");
      }
      list.push_back(elem);
    }
    else
    {
      throw std::invalid_argument("Values must be in range [0, 20]");
    }
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Reading error occured");
  }

  if (list.empty())
  {
    return;
  }

  auto i = list.begin();
  auto j = list.end();
  while (i != j)
  {
    std::cout << *(i++) << " ";
    if (i == j) break;
    std::cout << *(--j) << " ";
  }
  std::cout << '\n';
}

