#include <iostream>
#include <memory>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "triangle.hpp"

void info(const che::Shape::ptr &shape)
{
  che::rectangle_t frameRect = shape->getFrameRect();
  std::cout << "Shape area: " << shape->getArea() << "\n";
  std::cout << "Shape's center: " << frameRect.pos.x << ";" << frameRect.pos.y << "\n";
  std::cout << "Frame width: " << frameRect.width << "\n";
  std::cout << "Frame height: " << frameRect.height << "\n";
}

int main()
{
  std::cout << "Circle:\n";
  che::Shape::ptr circle = std::make_shared<che::Circle>(che::point_t {1, 1}, 3);
  info(circle);
  std::cout << "Scaling by 2:\n";
  circle->scale(2);
  info(circle);
  std::cout << "Move circle to point (1, 5)\n";
  circle->move({ 1, 5});
  info(circle);
   
  std::cout << "Rectangle:\n";
  che::Shape::ptr rectangle = std::make_shared<che::Rectangle>(che::point_t{ 2, 2 }, 10, 10);
  info(rectangle);
  std::cout << "Scaling by 1.5:\n";
  rectangle->scale(1.5);
  info(rectangle);
  std::cout << "Move Rectangle to point (3, 1)\n";
  rectangle->move({ 3, 1});
  info(rectangle);

  std::cout << "Composite shape:\n";
  che::CompositeShape compShape(rectangle);
  info(std::make_shared<che::CompositeShape>(compShape));
  std::cout << "New composite shape's parameters after adding:/n";
  compShape.add(circle);
  info(std::make_shared<che::CompositeShape>(compShape));
  std::cout <<"Move composite shape to point (3, 6)\n";
  compShape.move({ 3, 6 });
  info(std::make_shared<che::CompositeShape>(compShape));
  std::cout << "Scaling by 2.5:\n";
  compShape.scale(2.5);
  info(std::make_shared<che::CompositeShape>(compShape));
  std::cout << "Delete shape's first elem:\n";
  compShape.remove(0);
  info(std::make_shared<che::CompositeShape>(compShape));

  return 0;
}
