#include <iostream>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "partition.hpp"


void info(const che::Shape::ptr &shape)
{
  che::rectangle_t frame = shape->getFrameRect();
  std::cout << "Figure's center (" << frame.pos.x
            << "," << frame.pos.y << ")\n"
            <<"Rectangle frame width = \n" << frame.width
            <<"Rectangle frame height = \n" << frame.height
            << "Area = " << shape->getArea() << "\n";
}

int main()
{
  che::Shape::ptr rectPtr = std::make_shared<che::Rectangle>(che::point_t {10, -2}, 8, 3);
  std::cout << "Created rectangle's parameters:\n";
  info(rectPtr);
  std::cout << "\nRectangle with a new centre at (4, 3):\n";
  rectPtr->move({2, 4});
  info(rectPtr);
  std::cout << "\nRectangle shifted with dx=11, dy=0.9:\n";
  rectPtr->move(5, 2.5);
  info(rectPtr);
  std::cout << "\nScaled rectangle with coefficient=3:\n";
  rectPtr->scale(3);
  info(rectPtr);

  che::Shape::ptr circPtr = std::make_shared<che::Circle>(che::point_t {0.9, 2.3}, 3);
  std::cout << "\nCreated circle's parameters:\n";
  info(circPtr);
  std::cout << "\nCircle with a new centre at (12, 11):\n";
  circPtr->move({2, 1});
  info(circPtr);
  std::cout << "\nCircle shifted with dx=2.3, dy=4:\n";
  circPtr->move(1.5, 3);
  info(circPtr);
  std::cout << "\nScaled circle with coefficient=0.5:\n";
  circPtr->scale(1.5);
  info(circPtr);

  che::CompositeShape compShape(rectPtr);
  std::cout << "\nCreated composite shape's parameters after adding figures:\n";
  compShape.add(circPtr);
  info(std::make_shared<che::CompositeShape>(compShape));
  std::cout << "\nComposite shape with a new center at (2, -3):\n";
  compShape.move({2, 4});
  info(std::make_shared<che::CompositeShape>(compShape));
  std::cout << "\nScaled composite shape with coefficient=3:\n";
  compShape.scale(2);
  info(std::make_shared<che::CompositeShape>(compShape));
  std::cout << "\nDelete composite shape's 1st component:\n";
  compShape.remove(0);
  info(std::make_shared<che::CompositeShape>(compShape));

  compShape.add(rectPtr);
  std::cout << "\nGet partition of Composite Shape:\n";
  che::Matrix matrix = che::part(compShape);
  for (size_t i = 0; i < matrix.getLines(); i++)
  {
    std::cout << "Line: " <<  i + 1
              <<"Shapes in line: \n";
    for (size_t j = 0; j < matrix.getColumns(); j++)
    {
      if (matrix[i][j] != nullptr)
      {
        che::rectangle_t frame = matrix[i][j]->getFrameRect();
        std::cout << j + 1
                  <<"Area: \n" << matrix[i][j]->getArea()
                  << "Figure's center (" << frame.pos.x
                  << "," << frame.pos.y << ")\n";
      }
    }
  }

  return 0;
}
