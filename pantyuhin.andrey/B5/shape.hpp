#ifndef PANTYUHIN_ANDREY_SHAPE_HPP
#define PANTYUHIN_ANDREY_SHAPE_HPP

#include <vector>
#include <list>


namespace pantyuhin
{
  struct Point
  {
    int x, y;
  };

  using Shape = std::vector<Point>;
  using ShapeList = std::list<Shape>;

  const int TRIANGLE_VERTICES_COUNT = 3;
  const int RECTANGLE_VERTICES_COUNT = 4;
  const int PENTAGON_VERTICES_COUNT = 5;
}


#endif //PANTYUHIN_ANDREY_SHAPE_HPP
