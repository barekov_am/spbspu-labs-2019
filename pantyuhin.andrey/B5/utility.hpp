#ifndef PANTYUHIN_ANDREY_UTILITY_HPP
#define PANTYUHIN_ANDREY_UTILITY_HPP

#include <string>

#include "shape.hpp"


namespace pantyuhin
{
  Shape parseShape(const std::string &input);

  void fillShapeList(pantyuhin::ShapeList &shapeList);

  void sortShapeList(pantyuhin::ShapeList &shapes);

  std::vector<pantyuhin::Point> createPointsVector(const pantyuhin::ShapeList &shapeList);


  size_t countTriangles(const pantyuhin::ShapeList &shapeList);

  size_t countSquares(const pantyuhin::ShapeList &shapeList);

  size_t countRectangles(const pantyuhin::ShapeList &shapeList);

  void deletePentagons(pantyuhin::ShapeList &shapes);


  int distance(const pantyuhin::Point &left, const pantyuhin::Point &right);


  bool isRectangle(const pantyuhin::Shape &shape);

  bool isSquare(const pantyuhin::Shape &shape);
}

#endif //PANTYUHIN_ANDREY_UTILITY_HPP
