#include <vector>
#include <list>
#include <iostream>
#include <algorithm>
#include <sstream>

#include "utility.hpp"


pantyuhin::Shape pantyuhin::parseShape(const std::string &input)
{
  std::istringstream input_stream(input);
  int verticesCount = 0;
  input_stream >> verticesCount;

  if (verticesCount < TRIANGLE_VERTICES_COUNT)
  {
    throw std::invalid_argument("Invalid number of vertices");
  }

  pantyuhin::Shape shape;
  for (int i(0); i < verticesCount; i++)
  {
    pantyuhin::Point point = {0, 0};
    input_stream.ignore(input.length(), '(');
    input_stream >> point.x;
    input_stream.ignore(input.length(), ';');
    input_stream >> point.y;
    input_stream.ignore(input.length(), ')');
    shape.push_back(point);
  }

  if (input_stream.fail())
  {
    throw std::invalid_argument("Invalid input");
  }

  return shape;
}

void pantyuhin::fillShapeList(pantyuhin::ShapeList &shapeList)
{
  std::string line;

  while (std::getline(std::cin, line))
  {
    line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
    if (line.empty())
    {
      continue;
    }
    shapeList.push_back(pantyuhin::parseShape(line));
  }
}

void pantyuhin::sortShapeList(pantyuhin::ShapeList &shapes)
{
  auto endOfTriangles = std::partition(shapes.begin(), shapes.end(), [](const pantyuhin::Shape &shape)
  {
    return shape.size() == TRIANGLE_VERTICES_COUNT;
  });
  auto endOfSquares = std::partition(endOfTriangles, shapes.end(), isSquare);
  std::partition(endOfSquares, shapes.end(), isRectangle);
}

std::vector<pantyuhin::Point> pantyuhin::createPointsVector(const pantyuhin::ShapeList &shapeList)
{
  std::vector<pantyuhin::Point> points;

  for (const auto &shape : shapeList)
  {
    points.push_back(shape[0]);
  }

  return points;
}

size_t pantyuhin::countTriangles(const pantyuhin::ShapeList &shapeList)
{
  return static_cast<size_t>(std::count_if(shapeList.begin(), shapeList.end(),
      [](const pantyuhin::Shape &shape)
  {
    return shape.size() == TRIANGLE_VERTICES_COUNT;
  }));
}

size_t pantyuhin::countSquares(const pantyuhin::ShapeList &shapeList)
{

  return static_cast<size_t>(std::count_if(shapeList.begin(), shapeList.end(), isSquare));
}

size_t pantyuhin::countRectangles(const pantyuhin::ShapeList &shapeList)
{
  return static_cast<size_t>(std::count_if(shapeList.begin(), shapeList.end(), isRectangle));
}

void pantyuhin::deletePentagons(pantyuhin::ShapeList &shapes)
{
  shapes.erase(
    std::remove_if(shapes.begin(), shapes.end(),
        [](const pantyuhin::Shape &shape) { return shape.size() == PENTAGON_VERTICES_COUNT; }),
    shapes.end());
}

int pantyuhin::distance(const pantyuhin::Point &left, const pantyuhin::Point &right)
{
  return (left.x - right.x) * (left.x - right.x) + (left.y - right.y) * (left.y - right.y);
}

bool pantyuhin::isRectangle(const pantyuhin::Shape &shape)
{
  if (shape.size() == RECTANGLE_VERTICES_COUNT)
  {
    int diagonal1 = distance(shape[0], shape[2]);
    int diagonal2 = distance(shape[1], shape[3]);

    return diagonal1 == diagonal2;
  }
  else
  {
    return false;
  }
}

bool pantyuhin::isSquare(const pantyuhin::Shape &shape)
{
  if (isRectangle(shape))
  {
    int side1 = distance(shape[0], shape[1]);
    int side2 = distance(shape[1], shape[2]);
    int side3 = distance(shape[2], shape[3]);
    int side4 = distance(shape[3], shape[0]);

    if ((side1 == side2) && (side2 == side3) && (side3 == side4) && (side4 == side1))
    {
      return true;
    }
  }
  return false;
}
