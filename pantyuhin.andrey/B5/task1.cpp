#include <set>
#include <iostream>

#include "tasks.hpp"


void pantyuhin::task1()
{
  std::set<std::string> words;
  std::string word;

  while (std::cin >> word)
  {
    words.emplace(word);
  }

  for (const auto &i : words)
  {
    std::cout << i << std::endl;
  }
}
