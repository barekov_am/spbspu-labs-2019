#include <set>
#include <iostream>
#include <numeric>

#include "tasks.hpp"
#include "utility.hpp"


void pantyuhin::task2()
{
  pantyuhin::ShapeList shapes;

  pantyuhin::fillShapeList(shapes);

  size_t verticesQuantity = static_cast<size_t>(std::accumulate(shapes.begin(), shapes.end(), 0,
      [](size_t sum, const pantyuhin::Shape &shape)
  {
    return sum + shape.size();
  }
  ));

  size_t trianglesQuantity = 0, squaresQuantity = 0, rectanglesQuantity = 0;
  trianglesQuantity = pantyuhin::countTriangles(shapes);
  squaresQuantity = pantyuhin::countSquares(shapes);
  rectanglesQuantity = pantyuhin::countRectangles(shapes);

  pantyuhin::deletePentagons(shapes);

  std::vector<pantyuhin::Point> points = pantyuhin::createPointsVector(shapes);

  pantyuhin::sortShapeList(shapes);

  std::cout << "Vertices: " << verticesQuantity << std::endl;
  std::cout << "Triangles: " << trianglesQuantity << std::endl;
  std::cout << "Squares: " << squaresQuantity << std::endl;
  std::cout << "Rectangles: " << rectanglesQuantity << std::endl;

  std::cout << "Points:";
  for (const auto &i : points)
  {
    std::cout << " (" << i.x << "; " << i.y << ")";
  }
  std::cout << std::endl;

  std::cout << "Shapes:" << std::endl;
  for (const auto &shape : shapes)
  {
    std::cout << shape.size() << " ";
    for (const auto &i : shape)
    {
      std::cout << "(" << i.x << "; " << i.y << ") ";
    }
    std::cout << std::endl;
  }
}


