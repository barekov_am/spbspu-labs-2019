#include <iostream>

#include "tasks.hpp"


int main(int argc, char *argv[])
{
  try
  {
    if (argc == 2)
    {
      if (std::stoi(argv[1]) == 1)
      {
        pantyuhin::task1();
      }
      else if (std::stoi(argv[1]) == 2)
      {
        pantyuhin::task2();
      }
      else
      {
        std::cerr << "Incorrect task number";
        return 1;
      }
    }
    else
    {
      std::cerr << "Incorrect number of arguments";
      return 1;
    }
  }
  catch (std::exception &err)
  {
    std::cerr << err.what() << std::endl;
    return 2;
  }
  return 0;
}

