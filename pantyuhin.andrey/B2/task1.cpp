#include <algorithm>
#include <sstream>
#include <functional>

#include "commands.hpp"
#include "QueueWithPriority.hpp"

void task1()
{
  using runCommand = std::function<void(QueueWithPriority<std::string>&, std::stringstream&)>;
  struct command_t
  {
    const char *name;
    runCommand action;
  };

  static const command_t commands[] =
  {
    {"add",        commandAdd},
    {"get",        commandGet},
    {"accelerate", commandAccelerate},
  };

  QueueWithPriority<std::string> queue;

  std::string line;

  while (std::getline(std::cin, line))
  {
    std::stringstream input(line);
    std::string commandData;
    input >> commandData;
    if ((commandData == "out") || (commandData == "close"))
    {
      std::cout << "bye!";
      return;
    }
    auto command = std::find_if(std::begin(commands), std::end(commands),
        [&](const command_t &command) { return commandData == command.name; });

    if (command != std::end(commands))
    {
      command->action(queue, input);
    } else {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
