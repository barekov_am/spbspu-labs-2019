#include <sstream>
#include <algorithm>
#include <map>

#include "commands.hpp"

void commandAdd(QueueWithPriority<std::string>& queue, std::stringstream& input)
{
  using priorityType = ElementPriority;

  std::map<const std::string, priorityType> priorities =
    {
      {"low", priorityType::LOW},
      {"normal", priorityType::NORMAL},
      {"high", priorityType::HIGH}
    };

  std::string priority;
  input >> std::ws >> priority;

  auto check = [&](const std::pair<const std::string, priorityType>& pair)
  {
    return (pair.first == priority);
  };

  auto elementPriority = std::find_if(std::begin(priorities), std::end(priorities), check);

  if (elementPriority == std::end(priorities))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  std::string data;
  std::getline(input >> std::ws, data);

  if (data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  queue.put(data, elementPriority->second);
}

void commandGet(QueueWithPriority<std::string>& queue, std::stringstream& input)
{
  std::string data;
  std::getline(input >> std::ws, data);

  if (!data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (queue.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  std::string element = queue.get();
  std::cout << element << "\n";
}

void commandAccelerate(QueueWithPriority<std::string>& queue, std::stringstream& input)
{
  std::string data;
  std::getline(input >> std::ws, data);
  if (!data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  queue.accelerate();
}
