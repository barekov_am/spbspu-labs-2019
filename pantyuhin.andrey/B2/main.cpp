#include <iostream>
#include "tasks.hpp"

int main(int argc, char *argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Incorrect number of arguments\n";
      return 1;
    }

    char *ptrEnd = nullptr;
    int task = std::strtol(argv[1], &ptrEnd, 10);

    if (*ptrEnd != '\x00')
    {
      std::cerr << "Incorrect number of task\n";
      return 1;
    }

    switch (task)
    {
      case 1:
      {
        task1();
        break;
      }
      case 2:
      {
        task2();
        break;
      }
      default:
      {
        std::cerr << "Incorrect task number\n";
        return 1;
      }
    }
  } catch (const std::ios_base::failure &e)
  {
    std::cerr << "error: " << e.what() << "\n";
    return 1;
  } catch (const std::exception &e)
  {
    std::cerr << "error: " << e.what() << "\n";
    return 2;
  }
  return 0;
}


