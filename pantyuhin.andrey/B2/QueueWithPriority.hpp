#ifndef PANTYUHIN_ANDREY_QUEUEWITHPRIORITY_HPP
#define PANTYUHIN_ANDREY_QUEUEWITHPRIORITY_HPP

#include <list>
#include <iostream>
#include <memory>

typedef enum
{
  LOW,
  NORMAL,
  HIGH
} ElementPriority;

template<typename T>
class QueueWithPriority
{
public:

  void put(const T &elem, ElementPriority priority)
  {
    switch (priority)
    {
      case ElementPriority::LOW:
      {
        low_.push_back(elem);
        break;
      }
      case ElementPriority::NORMAL:
      {
        normal_.push_back(elem);
        break;
      }
      case ElementPriority::HIGH:
      {
        high_.push_back(elem);
        break;
      }
    }
  }

  T get()
  {
    if (!high_.empty())
    {
      T tmp = high_.front();
      high_.pop_front();
      return tmp;
    } else if (!normal_.empty())
    {
      T tmp = normal_.front();
      normal_.pop_front();
      return tmp;
    } else {
      T tmp = low_.front();
      low_.pop_front();
      return tmp;
    }
  }

  void accelerate()
  {
    high_.splice(high_.end(), low_);
  }

  bool empty() const
  {
    return (low_.empty() && normal_.empty() && high_.empty());
  }

private:

  std::list<T> low_;
  std::list<T> normal_;
  std::list<T> high_;

};
#endif //PANTYUHIN_ANDREY_QUEUEWITHPRIORITY_HPP
