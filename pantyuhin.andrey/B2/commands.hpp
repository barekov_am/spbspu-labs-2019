#ifndef PANTYUHIN_ANDREY_COMMANDS_HPP
#define PANTYUHIN_ANDREY_COMMANDS_HPP

#include "QueueWithPriority.hpp"

void commandAdd(QueueWithPriority<std::string>& queue, std::stringstream& input);
void commandGet(QueueWithPriority<std::string>& queue, std::stringstream& input);
void commandAccelerate(QueueWithPriority<std::string>& queue, std::stringstream& input);

#endif //PANTYUHIN_ANDREY_COMMANDS_HPP
