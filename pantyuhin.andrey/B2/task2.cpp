#include <iostream>
#include <list>

void task2()
{
  const std::size_t maxSize = 20;
  const int min = 1;
  const int max = 20;

  std::list<int> list;

  std::size_t size = 0;
  int tmp = 0;

  while (std::cin >> tmp)
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Fail while reading data\n");
    }

    if ((tmp > max) || (tmp < min))
    {
      throw std::ios_base::failure("Invalid number. Must be in [1, 20]\n");
    }

    if (size == maxSize)
    {
      throw std::ios_base::failure("Size of list exceeded (20)\n");
    }
    ++size;
    list.push_back(tmp);
  }
  if (!std::cin.eof())
  {
    throw std::ios_base::failure("Fail while reading data\n");
  }

  if (list.empty())
  {
    std::cout << "\n";
    return;
  }

  std::list<int>::iterator begin = list.begin();
  std::list<int>::iterator end = list.end();

  while (begin != end)
  {
    if (begin == std::prev(end))
    {
      std::cout << *begin << "\n";
      ++begin;
      break;
    }

    std::cout << *begin << " " << *(std::prev(end));
    ++begin;
    --end;

    if (begin == end)
    {
      std::cout << "\n";
    } else
    {
      std::cout << " ";
    }
  }
}

