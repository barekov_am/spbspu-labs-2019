#ifndef PANTYUHIN_ANDREY_OUTPUTPRINTER_HPP
#define PANTYUHIN_ANDREY_OUTPUTPRINTER_HPP

#include <iostream>
#include <list>
#include "symbols.hpp"

namespace pantyuhin
{

  class OutputPrinter
  {
  public:
    OutputPrinter(std::ostream &output_stream);

    void editAndPrint(size_t lineWidth, const std::list<token_t> &vector);

    bool isValid(const std::list<token_t> &vector);

  private:
    std::ostream &output_stream_;

    void printLine(const std::list<token_t> &line);

    size_t printValidPart(std::list<token_t> &line);

  };

}


#endif //PANTYUHIN_ANDREY_OUTPUTPRINTER_HPP
