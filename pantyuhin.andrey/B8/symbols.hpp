#ifndef PANTYUHIN_ANDREY_SYMBOLS_HPP
#define PANTYUHIN_ANDREY_SYMBOLS_HPP

#include <string>

namespace pantyuhin
{

  const size_t MIN_LINE_WIDTH = 25;
  const size_t DEFAULT_LINE_WIDTH = 40;
  const size_t MAX_TOKEN_LENGTH = 20;

  struct token_t
  {

    enum token_type
    {
      WORD,
      NUMBER,
      SYNTAX,
      DASH,
      SPACE
    };

    std::string value;
    token_type type;
  };

}


#endif //PANTYUHIN_ANDREY_SYMBOLS_HPP
