#include "inputConverter.hpp"

#include <stdexcept>

pantyuhin::InputConverter::InputConverter(std::istream &input_stream) :
  input_stream_(input_stream) {}

std::list<pantyuhin::token_t> pantyuhin::InputConverter::getListFromStream()
{
  while (input_stream_)
  {
    char ch = input_stream_.get();
    std::locale loc = std::locale();

    while (std::isspace(ch, loc))
    {
      ch = input_stream_.get();
    }

    if (std::isalpha(ch, loc))
    {
      input_stream_.unget();
      getWord();
    }
    else if (ch == '-')
    {
      if (input_stream_.peek() == '-')
      {
        input_stream_.unget();
        getDash();
      }
      else
      {
        input_stream_.unget();
        getNumber();
      }
    }
    else if ((ch == '+') || (std::isdigit(ch, loc)))
    {
      input_stream_.unget();
      getNumber();
    }
    else if (std::ispunct(ch, loc))
    {
      pantyuhin::token_t token{"", pantyuhin::token_t::SYNTAX};
      token.value.push_back(ch);
      text_.push_back(token);
    }
  }
  std::list<pantyuhin::token_t> vector(text_.begin(), text_.end());
  return vector;
}

void pantyuhin::InputConverter::getWord()
{
  pantyuhin::token_t symbol{"", pantyuhin::token_t::WORD};
  std::locale loc = std::locale();

  do
  {
    char ch = input_stream_.get();
    if ((ch == '-') && (input_stream_.peek() == '-'))
    {
      input_stream_.unget();
      break;
    }
    symbol.value.push_back(ch);

  } while ((std::isalpha<char>(input_stream_.peek(), loc)) || (input_stream_.peek() == '-'));

  text_.push_back(symbol);
}

void pantyuhin::InputConverter::getNumber()
{
  pantyuhin::token_t symbol{"", pantyuhin::token_t::NUMBER};
  std::locale loc = std::locale();

  char separator = std::use_facet<std::numpunct<char>>(std::locale()).decimal_point();
  bool gotSeparator = false;

  do
  {
    char ch = input_stream_.get();
    if (ch == separator)
    {
      if (gotSeparator)
      {
        input_stream_.unget();
        break;
      }
      gotSeparator = true;
    }
    symbol.value.push_back(ch);
  } while (std::isdigit<char>(input_stream_.peek(), loc) || (input_stream_.peek() == separator));

  text_.push_back(symbol);
}

void pantyuhin::InputConverter::getDash()
{
  pantyuhin::token_t symbol{"", pantyuhin::token_t::DASH};
  std::locale loc = std::locale();

  while (input_stream_.peek() == '-')
  {
    symbol.value.push_back(input_stream_.get());
  }

  if (symbol.value != "---")
  {
    throw (std::invalid_argument("Incorrect dash type or multiple-signed number"));
  }
  text_.push_back(symbol);
}
