#ifndef PANTYUHIN_ANDREY_INPUTCONVERTER_HPP
#define PANTYUHIN_ANDREY_INPUTCONVERTER_HPP

#include "symbols.hpp"
#include <iostream>
#include <list>


namespace pantyuhin
{

  class InputConverter
  {
  public:
    InputConverter(std::istream &input_stream);

    std::list<token_t> getListFromStream();

  private:
    std::istream &input_stream_;

    std::list<token_t> text_;

    void getWord();

    void getNumber();

    void getDash();
  };

}


#endif //PANTYUHIN_ANDREY_INPUTCONVERTER_HPP
