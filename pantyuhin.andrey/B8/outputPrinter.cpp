#include "outputPrinter.hpp"

#include <stdexcept>
#include <vector>

pantyuhin::OutputPrinter::OutputPrinter(std::ostream &output_stream) :
  output_stream_(output_stream) {}

void pantyuhin::OutputPrinter::printLine(const std::list<token_t> &line)
{
  for (auto iter = line.begin(); iter != line.end(); iter++)
  {
    output_stream_ << (*iter).value;
  }
  output_stream_ << std::endl;
}

size_t pantyuhin::OutputPrinter::printValidPart(std::list<token_t> &line)
{
  size_t width = 0;
  std::list<token_t> newLine;

  while (!line.empty())
  {
    width += line.back().value.size();
    newLine.push_front(line.back());
    line.pop_back();

    if ((newLine.front().type == pantyuhin::token_t::WORD) || (newLine.front().type == pantyuhin::token_t::NUMBER))
    {
      break;
    }
  }

  printLine(line);
  line = newLine;
  return width;
}

void pantyuhin::OutputPrinter::editAndPrint(size_t lineWidth, const std::list<token_t> &vector)
{
  size_t currWidth = 0;
  std::list<token_t> outputLine;

  for (auto iter = vector.begin(); iter != vector.end(); iter++)
  {
    switch ((*iter).type)
    {
      case pantyuhin::token_t::SYNTAX:

        if (currWidth >= lineWidth)
        {
          currWidth = printValidPart(outputLine);
        }

        outputLine.push_back(*iter);
        currWidth += (*iter).value.size();
        break;

      case pantyuhin::token_t::DASH:

        if (currWidth + 3 >= lineWidth)
        {
          currWidth = printValidPart(outputLine);
        }

        outputLine.push_back(token_t{" ", pantyuhin::token_t::SPACE});
        outputLine.push_back(*iter);
        currWidth += (*iter).value.size() + 1;
        break;

      case pantyuhin::token_t::WORD:

      case pantyuhin::token_t::NUMBER:

        if (currWidth + (*iter).value.size() >= lineWidth)
        {
          printLine(outputLine);
          outputLine.clear();
          currWidth = 0;
        }
        else if (!outputLine.empty())
        {
          outputLine.push_back(token_t{" ", pantyuhin::token_t::SPACE});
          currWidth++;
        }

        outputLine.push_back(*iter);
        currWidth += (*iter).value.size();
        break;

      case pantyuhin::token_t::SPACE:
        break;

      default:
        break;
    }
  }

  if (!outputLine.empty())
  {
    printLine(outputLine);
  }
}

bool pantyuhin::OutputPrinter::isValid(const std::list<token_t> &vector)
{
  if (!vector.empty() && (vector.front().type != pantyuhin::token_t::WORD) &&
      (vector.front().type != pantyuhin::token_t::NUMBER))
  {
    return false;
  }

  for (auto iter = vector.begin(); iter != vector.end(); iter++)
  {
    switch ((*iter).type)
    {
      case pantyuhin::token_t::WORD:
        if ((*iter).value.size() > pantyuhin::MAX_TOKEN_LENGTH)
        {
          throw (std::invalid_argument("Too long word"));
        }
        break;

      case pantyuhin::token_t::NUMBER:
        if ((*iter).value.size() > pantyuhin::MAX_TOKEN_LENGTH)
        {
          throw (std::invalid_argument("Too long number"));
        }
        break;

      case pantyuhin::token_t::DASH:
        if (iter != vector.begin())
        {
          const pantyuhin::token_t &prev = *std::prev(iter);
          if ((prev.type == pantyuhin::token_t::DASH) ||
              ((prev.type == pantyuhin::token_t::SYNTAX) && (prev.value != ",")))
          {
            throw (std::invalid_argument("Two dashes in a row or a dash after not-comma punctuation mark"));
          }
        }
        break;

      case pantyuhin::token_t::SYNTAX:
        if (iter != vector.begin())
        {
          const pantyuhin::token_t &prev = *std::prev(iter);
          if ((prev.type == pantyuhin::token_t::DASH) || (prev.type == pantyuhin::token_t::SYNTAX))
          {
            throw (std::invalid_argument("Two punctuation marks in a row or a punctuation mark after dash"));
          }
        }
        break;

      case pantyuhin::token_t::SPACE:
        break;

      default:
        break;
    }
  }

  return true;
}
