#include "inputConverter.hpp"
#include "outputPrinter.hpp"
#include "symbols.hpp"
#include <stdexcept>
#include <cstring>
#include <iostream>

int main(int argc, char **argv)
{

  try
  {
    if ((argc != 3) && (argc != 1))
    {
      throw std::invalid_argument("Invalid number of arguments");
    }

    size_t lineWidth = pantyuhin::DEFAULT_LINE_WIDTH;
    if (argc == 3)
    {
      if (strcmp(argv[1], "--line-width"))
      {
        throw (std::invalid_argument("Missed '--line-width'"));
      }
      else
      {
        lineWidth = atoi(argv[2]);
        if (lineWidth < pantyuhin::MIN_LINE_WIDTH)
        {
          throw (std::invalid_argument("Too small line width"));
        }
      }
    }

    pantyuhin::InputConverter editor(std::cin);

    pantyuhin::OutputPrinter printer(std::cout);

    std::list<pantyuhin::token_t> vector = editor.getListFromStream();

    if (!printer.isValid(vector))
    {
      throw (std::invalid_argument("Invalid input data"));
    }

    printer.editAndPrint(lineWidth, vector);
  }


  catch (std::exception &err)
  {
    std::cerr << err.what();
    return 1;
  }

  return 0;
}

