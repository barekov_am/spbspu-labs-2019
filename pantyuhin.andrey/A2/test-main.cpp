#define BOOST_TEST_MODULE A2

#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

BOOST_AUTO_TEST_SUITE(testSuite)

const double PRECISION = 0.01;

BOOST_AUTO_TEST_CASE(CircleConstAfterMoving)
{
  pantyuhin::Circle circle_({1, -1}, 2);
  const pantyuhin::rectangle_t firstFrame = circle_.getFrameRect();
  const double first = circle_.getArea();

  circle_.move({4, 5});
  pantyuhin::rectangle_t secondFrame = circle_.getFrameRect();
  double secondArea = circle_.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(first, secondArea, PRECISION);

  circle_.move(-2, 3);
  secondFrame = circle_.getFrameRect();
  secondArea = circle_.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(first, secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(RectangleConstAfterMoving)
{
  pantyuhin::Rectangle rectangle_({1, -1}, 2, 3);
  const pantyuhin::rectangle_t firstFrame = rectangle_.getFrameRect();
  const double firstArea = rectangle_.getArea();

  rectangle_.move({4, 5});
  pantyuhin::rectangle_t secondFrame = rectangle_.getFrameRect();
  double secondArea = rectangle_.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);

  rectangle_.move(-2, 3);
  secondFrame = rectangle_.getFrameRect();
  secondArea = rectangle_.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(TriangleConstAfterMoving)
{
  pantyuhin::Triangle triangle_({1, -1}, {2, 2}, {3, -2});
  const pantyuhin::rectangle_t firstFrame = triangle_.getFrameRect();
  const double firstArea = triangle_.getArea();

  triangle_.move({4, 5});
  pantyuhin::rectangle_t secondFrame = triangle_.getFrameRect();
  double secondArea = triangle_.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);

  triangle_.move(-2, 3);
  secondFrame = triangle_.getFrameRect();
  secondArea = triangle_.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(PolygonConstAfterMoving)
{
  pantyuhin::point_t pointArray[] {{4, 5}, {6, 4}, {7, -1}, {5, -3}, {2, 1}};

  pantyuhin::Polygon polygon_(5, pointArray);
  const pantyuhin::rectangle_t firstFrame = polygon_.getFrameRect();
  const double firstArea = polygon_.getArea();

  polygon_.move({4, 5});
  pantyuhin::rectangle_t secondFrame = polygon_.getFrameRect();
  double secondArea = polygon_.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);

  polygon_.move(-2, 3);
  secondFrame = polygon_.getFrameRect();
  secondArea = polygon_.getArea();

  BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
  BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
  BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(CircleScaling)
{
  pantyuhin::Circle circle_({1, -1}, 2);
  const double firstArea = circle_.getArea();

  const double multiplier = 2.2;
  circle_.scale(multiplier);
  double secondArea = circle_.getArea();

  BOOST_CHECK_CLOSE(firstArea * pow(multiplier, 2), secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(RectangleScaling)
{
  pantyuhin::Rectangle rectangle_({1, -1}, 2, 3);
  const double firstArea = rectangle_.getArea();

  const double multiplier = 2.2;
  rectangle_.scale(multiplier);
  double secondArea = rectangle_.getArea();

  BOOST_CHECK_CLOSE(firstArea * pow(multiplier, 2), secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(TriangleScaling)
{
  pantyuhin::Triangle triangle_({1, -1}, {2, 2}, {3, -2});
  const double firstArea = triangle_.getArea();

  const double multiplier = 4;
  triangle_.scale(multiplier);
  double secondArea = triangle_.getArea();

  BOOST_CHECK_CLOSE(firstArea * pow(multiplier, 2), secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(PolygonScaling)
{
  pantyuhin::point_t pointArray[] {{4, 5}, {6, 4}, {7, -1}, {5, -3}, {2, 1}};
  pantyuhin::Polygon polygon_(5, pointArray);
  const double firstArea = polygon_.getArea();

  const double multiplier = 6;
  polygon_.scale(multiplier);
  double secondArea = polygon_.getArea();

  BOOST_CHECK_CLOSE(firstArea * pow(multiplier, 2), secondArea, PRECISION);
}

BOOST_AUTO_TEST_CASE(throwingExceptions)
{
  BOOST_CHECK_THROW(pantyuhin::Circle({3, 4}, -2), std::invalid_argument);
  BOOST_CHECK_THROW(pantyuhin::Rectangle({5, 2}, -2, -3), std::invalid_argument);
  BOOST_CHECK_THROW(pantyuhin::Triangle({0, 0}, {-1, 0}, {1, 0}), std::invalid_argument);
  pantyuhin::point_t pointArray1[] {{0, 0}, {-1, 0}};
  BOOST_CHECK_THROW(pantyuhin::Polygon(2, pointArray1), std::invalid_argument);
  pantyuhin::point_t pointArray2[] {{-1, 1}, {0, -1}, {1, 1}, {2, -1}};
  BOOST_CHECK_THROW(pantyuhin::Polygon(4, pointArray2), std::invalid_argument);
  pantyuhin::point_t pointArray3[] {{-1, -1}, {1, 4}, {3, -1}, {0, 0}};
  BOOST_CHECK_THROW(pantyuhin::Polygon(4, pointArray3), std::invalid_argument);

  pantyuhin::Rectangle rectangle_({3, 5}, 6, 2);
  BOOST_CHECK_THROW(rectangle_.scale(0), std::invalid_argument);
  pantyuhin::Circle circle_({6, 5}, 7.5);
  BOOST_CHECK_THROW(circle_.scale(0), std::invalid_argument);
  pantyuhin::Triangle triangle_({3, 5}, {2, 2}, {-1, -3});
  BOOST_CHECK_THROW(triangle_.scale(0), std::invalid_argument);
  pantyuhin::point_t pointArray[] {{4, 5}, {6, 4}, {7, -1}, {5, -3}, {2, 1}};
  pantyuhin::Polygon polygon_(5, pointArray);
  BOOST_CHECK_THROW(triangle_.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
