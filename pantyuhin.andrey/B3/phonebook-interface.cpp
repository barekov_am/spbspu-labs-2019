#include "phonebook-interface.hpp"
#include "phonebook.hpp"

#include <iostream>
#include <algorithm>

pantyuhin::PhoneBookInterface::PhoneBookInterface()
{
  bookmarks_["current"] = phoneBook_.begin();
}

void pantyuhin::PhoneBookInterface::add(record_t &record)
{
  phoneBook_.add(record);
  if (phoneBook_.begin() == phoneBook_.end())
  {
    bookmarks_["current"] = phoneBook_.begin();
  }
};


bool pantyuhin::PhoneBookInterface::isEmpty() const
{
  return phoneBook_.isEmpty();
}


void pantyuhin::PhoneBookInterface::store(std::string &markName, std::string &newMarkName)
{
  auto iter = bookmarks_.find(markName);

  if (iter != bookmarks_.end())
  {
    bookmarks_[newMarkName] = iter->second;
    return;;
  }
  std::cout << "<INVALID BOOKMARK>\n";
};


void pantyuhin::PhoneBookInterface::insert(InsertPos &position, std::string &markName, record_t &record)
{
  auto iter = bookmarks_.find(markName);
  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  if (iter->second == phoneBook_.end())
  {
    add(record);
  }

  switch (position)
  {
    case (InsertPos::before):
    {
      phoneBook_.insertBefore(iter->second, record);
      break;
    }
    case (InsertPos::after):
    {
      phoneBook_.insertAfter(iter->second, record);
      break;
    }
  }
};


void pantyuhin::PhoneBookInterface::remove(std::string &markName)
{
  auto iter = bookmarks_.find(markName);
  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  if (iter != bookmarks_.end())
  {
    auto deleteIter = iter->second;
    for (auto i = bookmarks_.begin(); i != bookmarks_.end(); i++)
    {
      if (i->second == deleteIter)
      {
        if (i->second == phoneBook_.end())
        {
          i->second = phoneBook_.previous(deleteIter);
        }
        else
        {
          i->second = phoneBook_.next(deleteIter);
        }
      }
    }

    phoneBook_.remove(deleteIter);
  }
};

void pantyuhin::PhoneBookInterface::show(std::string &markName)
{
  auto iter = bookmarks_.find(markName);
  if (iter != bookmarks_.end())
  {
    if (phoneBook_.isEmpty())
    {
      std::cout << "<EMPTY>\n";
      return;
    }

    return phoneBook_.show(iter->second);
  }
  std::cout << "<INVALID BOOKMARK>\n";
};


void pantyuhin::PhoneBookInterface::move(std::string &markName, int steps)
{
  auto iter = bookmarks_.find(markName);
  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  iter->second = phoneBook_.move(iter->second, steps);
};


void pantyuhin::PhoneBookInterface::move(std::string &markName, MovePos &movePosition)
{
  auto iter = bookmarks_.find(markName);
  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  switch (movePosition)
  {
    case MovePos::first:
    {
      iter->second = phoneBook_.begin();
      break;
    }
    case MovePos::last:
    {
      iter->second = phoneBook_.end();
      break;
    }
  }
};
