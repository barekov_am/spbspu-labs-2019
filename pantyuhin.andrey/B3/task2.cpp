#include <forward_list>
#include <iostream>
#include <algorithm>
#include "factorial-container.hpp"
#include "tasks.hpp"

void pantyuhin::task2()
{
  pantyuhin::FactorialContainer container;

  std::copy(container.begin(), container.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;

  std::reverse_copy(container.begin(), container.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;
}

