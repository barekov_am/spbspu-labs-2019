#ifndef PANTYUHIN_ANDREY_FACTORIAL_ITERATOR_HPP
#define PANTYUHIN_ANDREY_FACTORIAL_ITERATOR_HPP

#include <iterator>

namespace pantyuhin
{
  class FactorialIterator : public std::iterator<std::bidirectional_iterator_tag, size_t>
  {
  public:
    FactorialIterator();

    FactorialIterator(size_t position);

    FactorialIterator &operator++();

    FactorialIterator operator++(int);

    FactorialIterator &operator--();

    FactorialIterator operator--(int);

    const size_t *operator->();

    const size_t &operator*();

    bool operator==(const FactorialIterator &it);

    bool operator!=(const FactorialIterator &it);

  private:
    size_t position, value;

    size_t getValue(size_t number);
  };
}


#endif //PANTYUHIN_ANDREY_FACTORIAL_ITERATOR_HPP
