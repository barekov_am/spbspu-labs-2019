#ifndef TELEPHONEBOOK_HPP
#define TELEPHONEBOOK_HPP

#include <list>
#include <string>
#include <memory>
#include <iterator>

namespace pantyuhin
{
  struct record_t
  {
    std::string name;
    std::string phoneNumber;
  };

  class PhoneBook
  {
  public:

    using container = std::list<record_t>;
    using iterator = container::iterator;

    bool isEmpty() const;

    void insertBefore(const iterator &iter, record_t record);

    void insertAfter(iterator iter, record_t record);

    void add(record_t record);

    void show(const iterator &iter);

    iterator begin();

    iterator end();

    iterator move(iterator iter, int offset) const;

    iterator next(iterator iter);

    iterator previous(iterator iter);

    iterator remove(iterator iter);

  private:

    container book_;
  };

}
#endif // TELEPHONEBOOK_HPP
