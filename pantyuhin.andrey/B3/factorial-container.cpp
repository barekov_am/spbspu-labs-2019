#include "factorial-container.hpp"

pantyuhin::FactorialIterator pantyuhin::FactorialContainer::begin()
{
  return pantyuhin::FactorialIterator(pantyuhin::FactorialContainer::BEGIN);
}

pantyuhin::FactorialIterator pantyuhin::FactorialContainer::end()
{
  return pantyuhin::FactorialIterator(pantyuhin::FactorialContainer::END);
}
