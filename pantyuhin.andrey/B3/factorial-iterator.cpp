#include "factorial-iterator.hpp"
#include "factorial-container.hpp"

pantyuhin::FactorialIterator::FactorialIterator() : FactorialIterator(1)
{
}

pantyuhin::FactorialIterator::FactorialIterator(size_t position) :
    position(position),
    value(getValue(position))
{
}

const size_t *pantyuhin::FactorialIterator::operator->()
{
  return &value;
};

const size_t &pantyuhin::FactorialIterator::operator*()
{
  return value;
}

pantyuhin::FactorialIterator &pantyuhin::FactorialIterator::operator++()
{
  if (position < pantyuhin::FactorialContainer::END)
  {
    ++position;
    value *= position;
  }

  return *this;
}

pantyuhin::FactorialIterator pantyuhin::FactorialIterator::operator++(int)
{
  pantyuhin::FactorialIterator it = *this;
  ++(it);

  return it;
}

pantyuhin::FactorialIterator &pantyuhin::FactorialIterator::operator--()
{
  if (position > pantyuhin::FactorialContainer::BEGIN)
  {
    value /= position;
    --position;
  }

  return *this;
}

pantyuhin::FactorialIterator pantyuhin::FactorialIterator::operator--(int)
{
  pantyuhin::FactorialIterator it = *this;
  --(it);

  return it;
}

bool pantyuhin::FactorialIterator::operator==(const FactorialIterator &it)
{
  return (position == it.position);
}

bool pantyuhin::FactorialIterator::operator!=(const FactorialIterator &it)
{
  return !(*this == it);
}

size_t pantyuhin::FactorialIterator::getValue(size_t number)
{
  size_t result = 1;

  for (size_t i = 1; i <= number; i++)
  {
    result *= i;
  }

  return result;
}
