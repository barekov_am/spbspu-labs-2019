#include "phonebook.hpp"

#include <iostream>

pantyuhin::PhoneBook::iterator pantyuhin::PhoneBook::begin()
{
  return book_.begin();
}

pantyuhin::PhoneBook::iterator pantyuhin::PhoneBook::end()
{
  if (!book_.empty())
  {
    return --book_.end();
  }
  else
  {
    return book_.begin();
  }
}

bool pantyuhin::PhoneBook::isEmpty() const
{
  return book_.empty();
}

void pantyuhin::PhoneBook::insertBefore(const pantyuhin::PhoneBook::iterator &iter, record_t record)
{
  if (book_.empty())
  {
    add(record);
  }
  else
  {
    book_.insert(iter, record);
  }
}

void pantyuhin::PhoneBook::insertAfter(pantyuhin::PhoneBook::iterator iter, record_t record)
{
  if (book_.empty())
  {
    add(record);
  }
  else
  {
    book_.insert(++iter, record);
  }
}

void pantyuhin::PhoneBook::add(record_t record)
{
  book_.push_back(record);
}

void pantyuhin::PhoneBook::show(const pantyuhin::PhoneBook::iterator &iter)
{
  std::cout << iter->name << " " << iter->phoneNumber << std::endl;
}

pantyuhin::PhoneBook::iterator pantyuhin::PhoneBook::move(pantyuhin::PhoneBook::iterator iter, int offset) const
{
  if (offset > 0)
  {
    for (int i = 0; i < offset; ++i)
    {
      if (++iter == book_.cend())
        //throw std::out_of_range("Out of range");
        --iter;
    }
  }
  else
  {
    for (int i = 0; offset < i; --i)
    {
      if (iter != book_.cbegin())
      {
        iter--;
      }
    }
  }
  return iter;
}

pantyuhin::PhoneBook::iterator pantyuhin::PhoneBook::next(iterator iter)
{
  if (std::next(iter) == book_.end())
  {
    return iter;
  }
  return ++iter;
}

pantyuhin::PhoneBook::iterator pantyuhin::PhoneBook::previous(iterator iter)
{
  if (iter == book_.begin())
  {
    return iter;
  }
  return --iter;
}

pantyuhin::PhoneBook::iterator pantyuhin::PhoneBook::remove(pantyuhin::PhoneBook::iterator iter)
{
  iter = book_.erase(iter);
  if (iter == book_.cend() && !book_.empty())
    iter--;
  return iter;
}




