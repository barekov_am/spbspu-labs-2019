#include "tasks.hpp"
#include "phonebook-interface.hpp"
#include "phonebook-commands.hpp"
#include <algorithm>
#include <iostream>
#include <sstream>

void pantyuhin::task1()
{
  pantyuhin::PhoneBookInterface book;
  std::string input_line;

  while (std::getline(std::cin, input_line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed reading");
    }
    try
    {
      std::istringstream input(input_line);
      classifyCommand(book, input);
    }
    catch (std::invalid_argument &ex)
    {
      std::cerr << ex.what() << std::endl;
    }
    catch (std::exception &ex)
    {
      std::cout << ex.what() << std::endl;
    }
  }
}



