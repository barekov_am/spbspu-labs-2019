#include <iostream>
#include "tasks.hpp"

int main(int argc, char **argv)
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Incorrect number of arguments";
      return 1;
    }
    if ((argv[1][1] != 0x00) || (argv[1][0] < '1') || (argv[1][0] > '2'))
    {
      std::cerr << "Incorrect task number";
      return 1;
    }
    switch (argv[1][0])
    {
      case '1':
        pantyuhin::task1();
        break;
      case '2':
        pantyuhin::task2();
    }
  }
  catch (std::exception & err)
  {
    std::cerr << err.what() << std::endl;
    return 2;
  }
  return 0;
}

