#ifndef PANTYUHIN_ANDREY_PHONEBOOK_INTERFACE_HPP
#define PANTYUHIN_ANDREY_PHONEBOOK_INTERFACE_HPP

#include <map>
#include <string>

#include "phonebook.hpp"

namespace pantyuhin
{
  class PhoneBookInterface
  {
  public:
    enum class InsertPos
    {
      before,
      after
    };

    enum class MovePos
    {
      first,
      last
    };

    PhoneBookInterface();

    bool isEmpty() const;

    void add(pantyuhin::record_t &record);

    void store(std::string &bookmark, std::string &newBookmark);

    void insert(InsertPos &position, std::string &bookmark, pantyuhin::record_t &record);

    void remove(std::string &bookmark);

    void show(std::string &bookmark);

    void move(std::string &bookmark, int steps);

    void move(std::string &bookmark, MovePos &movePosition);

  private:
    std::map<std::string, pantyuhin::PhoneBook::iterator> bookmarks_;
    pantyuhin::PhoneBook phoneBook_;

  };
}

#endif //PANTYUHIN_ANDREY_PHONEBOOK_INTERFACE_HPP
