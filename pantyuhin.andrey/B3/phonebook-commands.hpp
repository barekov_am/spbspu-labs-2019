#ifndef PANTYUHIN_ANDREY_COMMANDS_HPP
#define PANTYUHIN_ANDREY_COMMANDS_HPP

#include "phonebook.hpp"
#include "phonebook-interface.hpp"

void classifyCommand(pantyuhin::PhoneBookInterface &phoneBookInterface, std::istream &input);

void executeAdd(pantyuhin::PhoneBookInterface &phoneBookInterface, std::istream &input);

void executeStore(pantyuhin::PhoneBookInterface &phoneBookInterface, std::istream &input);

void executeInsert(pantyuhin::PhoneBookInterface &phoneBookInterface, std::istream &input);

void executeDelete(pantyuhin::PhoneBookInterface &phoneBookInterface, std::istream &input);

void executeShow(pantyuhin::PhoneBookInterface &phoneBookInterface, std::istream &input);

void executeMove(pantyuhin::PhoneBookInterface &phoneBookInterface, std::istream &input);


#endif //PANTYUHIN_ANDREY_COMMANDS_HPP
