#ifndef FACTORIALCONTEINER_HPP
#define FACTORIALCONTEINER_HPP

#include "factorial-iterator.hpp"

namespace pantyuhin
{
  class FactorialContainer
  {
  public:
    static const int END = 11;
    static const int BEGIN = 1;

    FactorialContainer() = default;

    FactorialIterator begin();

    FactorialIterator end();
  };
}


#endif // FACTORIALCONTEINER_HPP
