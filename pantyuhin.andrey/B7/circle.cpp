#include "circle.hpp"

pantyuhin::Circle::Circle(int x, int y) :
  Shape(x, y)
{ }

void pantyuhin::Circle::draw(std::ostream &stream) const
{
  stream << "CIRCLE (" << getX() << ";" << getY() << ")" << std::endl;
}
