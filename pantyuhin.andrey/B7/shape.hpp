#ifndef PANTYUHIN_ANDREY_SHAPE_HPP
#define PANTYUHIN_ANDREY_SHAPE_HPP

#include <memory>
#include <iostream>

namespace pantyuhin
{
  class Shape
  {
  public:
    Shape(int x, int y) noexcept;

    virtual ~Shape() = default;

    double getX() const noexcept;

    double getY() const noexcept;

    bool isMoreLeft(const Shape &shape) const noexcept;

    bool isUpper(const Shape &shape) const noexcept;

    virtual void draw(std::ostream &stream) const = 0;

  private:
    int x_;
    int y_;
  };
}

#endif //PANTYUHIN_ANDREY_SHAPE_HPP
