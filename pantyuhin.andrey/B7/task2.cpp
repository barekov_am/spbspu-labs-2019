#include <algorithm>
#include <iostream>
#include <vector>

#include "shape.hpp"
#include "tasks.hpp"
#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"

using shapePointer = std::shared_ptr<pantyuhin::Shape>;

std::vector<shapePointer> getShapes()
{
  std::vector<shapePointer> shapeVector;
  std::string input;

  while (std::getline(std::cin, input))
  {
    if (input.empty())
    {
      continue;
    }

    input.erase(remove(input.begin(), input.end(), ' '), input.end());
    input.erase(remove(input.begin(), input.end(), '\t'), input.end());

    if (input.empty() || input.size() == 1)
    {
      continue;
    }

    size_t open_bracket = input.find_first_of('(');
    size_t semicolon = input.find_first_of(';');
    size_t close_bracket = input.find_first_of(')');

    if (open_bracket == std::string::npos || semicolon == std::string::npos || close_bracket == std::string::npos)
    {
      throw std::runtime_error("Reading failed\n");
    }

    std::string shape = input.substr(0, open_bracket);
    int x = std::stoi(input.substr(open_bracket + 1, semicolon - open_bracket + 1));
    int y = std::stoi(input.substr(semicolon + 1, close_bracket - semicolon + 1));


    if (shape == "TRIANGLE")
    {
      shapeVector.emplace_back(std::make_shared<pantyuhin::Triangle>(pantyuhin::Triangle(x, y)));
    }
    else if (shape == "SQUARE")
    {
      shapeVector.emplace_back(std::make_shared<pantyuhin::Square>(pantyuhin::Square(x, y)));
    }
    else if (shape == "CIRCLE")
    {
      shapeVector.emplace_back(std::make_shared<pantyuhin::Circle>(pantyuhin::Circle(x, y)));
    }
    else
    {
      throw std::invalid_argument("Invalid input");
    }
  }
  return shapeVector;
}


void pantyuhin::task2()
{
  using shape_ptr = std::shared_ptr<pantyuhin::Shape>;

  std::vector<shape_ptr> vector = getShapes();

  std::cout << "Original:\n";
  std::for_each(vector.begin(), vector.end(), [](const shape_ptr shape) { shape->draw(std::cout); });

  std::cout << "Left-Right:\n";
  std::sort(vector.begin(), vector.end(),
            [](const shape_ptr &shape1, const shape_ptr &shape2) { return shape1->isMoreLeft(*shape2); });
  std::for_each(vector.begin(), vector.end(), [](const shape_ptr shape) { shape->draw(std::cout); });

  std::cout << "Right-Left:\n";
  std::for_each(vector.rbegin(), vector.rend(), [](const shape_ptr shape) { shape->draw(std::cout); });

  std::cout << "Top-Bottom:\n";
  std::sort(vector.begin(), vector.end(),
            [](const shape_ptr shape1, const shape_ptr shape2) { return (shape1->isUpper(*shape2)); });
  std::for_each(vector.begin(), vector.end(), [](const shape_ptr shape) { shape->draw(std::cout); });

  std::cout << "Bottom-Top:\n";
  std::for_each(vector.rbegin(), vector.rend(), [](const shape_ptr shape) { shape->draw(std::cout); });
}


