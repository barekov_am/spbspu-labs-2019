#include "shape.hpp"

pantyuhin::Shape::Shape(int x, int y) noexcept :
  x_(x),
  y_(y)
{ }

double pantyuhin::Shape::getX() const noexcept
{
  return x_;
}

double pantyuhin::Shape::getY() const noexcept
{
  return y_;
}

bool pantyuhin::Shape::isMoreLeft(const Shape &shape) const noexcept
{
  return (x_ < shape.x_);
}

bool pantyuhin::Shape::isUpper(const Shape &shape) const noexcept
{
  return (y_ > shape.y_);
}
