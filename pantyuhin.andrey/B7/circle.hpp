#ifndef PANTYUHIN_ANDREY_CIRCLE_HPP
#define PANTYUHIN_ANDREY_CIRCLE_HPP

#include "shape.hpp"

namespace pantyuhin
{
  class Circle : public Shape
  {
  public:
    Circle(int x, int y);

    void draw(std::ostream &stream) const override;
  };
}


#endif //PANTYUHIN_ANDREY_CIRCLE_HPP
