#include <iostream>
#include <algorithm>
#include <cmath>
#include <iterator>
#include <functional>

#include "tasks.hpp"

void pantyuhin::task1()
{
  std::transform(std::istream_iterator<double>(std::cin), std::istream_iterator<double>(),
                 std::ostream_iterator<double>(std::cout, " "),
                 std::bind(std::multiplies<double>(), std::placeholders::_1, M_PI));

  if (!(std::cin.eof()))
  {
    throw (std::invalid_argument("Invalid input"));
  }
}
