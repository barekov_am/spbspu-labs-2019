#include "square.hpp"

pantyuhin::Square::Square(int x, int y) :
  Shape(x, y)
{ }

void pantyuhin::Square::draw(std::ostream &stream) const
{
  stream << "SQUARE (" << getX() << ";" << getY() << ")" << std::endl;
}
