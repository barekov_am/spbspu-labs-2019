#include <iostream>
#include "tasks.hpp"

int main(int argc, char *argv[])
{
  try
  {
    if (argc == 2)
    {
      if (std::stoi(argv[1]) == 1)
      {
        pantyuhin::task1();
      }
      else if (std::stoi(argv[1]) == 2)
      {
        pantyuhin::task2();
      }
      else
      {
        throw std::invalid_argument("Invalid task number");
      }
    }
    else
    {
      throw std::invalid_argument("Invalid number of arguments");
    }
  }
  catch (std::exception &exception)
  {
    std::cerr << exception.what() << std::endl;
    return 2;
  }
  return 0;
}


