#ifndef PANTYUHIN_ANDREY_TRIANGLE_HPP
#define PANTYUHIN_ANDREY_TRIANGLE_HPP

#include "shape.hpp"

namespace pantyuhin
{
  class Triangle : public Shape
  {
  public:
    Triangle(int x, int y);

    void draw(std::ostream &stream) const override;
  };
}


#endif //PANTYUHIN_ANDREY_TRIANGLE_HPP
