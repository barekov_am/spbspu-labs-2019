#ifndef PANTYUHIN_ANDREY_SQUARE_HPP
#define PANTYUHIN_ANDREY_SQUARE_HPP

#include "shape.hpp"

namespace pantyuhin
{
  class Square : public Shape
  {
  public:
    Square(int x, int y);

    void draw(std::ostream &stream) const override;
  };
}


#endif //PANTYUHIN_ANDREY_SQUARE_HPP
