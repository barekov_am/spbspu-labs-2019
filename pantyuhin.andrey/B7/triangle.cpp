#include "triangle.hpp"

pantyuhin::Triangle::Triangle(int x, int y) :
  Shape(x, y)
{ }

void pantyuhin::Triangle::draw(std::ostream &stream) const
{
  stream << "TRIANGLE (" << getX() << ";" << getY() << ")" << std::endl;
}
