#include <random>
#include <vector>

#include "utility.hpp"
#include "strategy.hpp"

void fillRandom(double* array, size_t size)
{
  std::mt19937 rng(time(0));
  std::uniform_real_distribution<double> urd(-1.0, 1.0);
  for (size_t i = 0; i < size; ++i)
  {
    array[i] = urd(rng);
  }
}
void task4(const bool ascending, size_t size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size can't be <0");
  }
  std::vector<double> vector(size);
  fillRandom(&vector[0], size);

  print(vector);
  sort<brStrategy>(vector, ascending);
  print(vector);
}
