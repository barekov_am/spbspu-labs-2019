#ifndef PANTYUHIN_ANDREY_TASKS_HPP
#define PANTYUHIN_ANDREY_TASKS_HPP

#include <cstring>

void task1(bool ascending);
void task2(const char* file);
void task3();
void task4(bool ascending, size_t size);

#endif //PANTYUHIN_ANDREY_TASKS_HPP
