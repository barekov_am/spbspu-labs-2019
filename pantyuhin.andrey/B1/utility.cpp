#include "utility.hpp"

bool getDirection(const char *direction)
{
  if (std::strcmp(direction, "ascending") == 0)
  {
    return true;
  }

  if (std::strcmp(direction, "descending") == 0)
  {
    return false;
  }

  throw std::invalid_argument("Invalid direction of sorting");
}
