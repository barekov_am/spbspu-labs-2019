#include <iostream>
#include <stdexcept>
#include "tasks.hpp"
#include "utility.hpp"

int main(int argc, char* argv[])
{
  try {
    if (argc <= 1)
    {
      std::cerr << "Incorrect number of arguments\n";
      return 1;
    }

    char *ptrEnd = nullptr;
    int taskNumber = std::strtol(argv[1], &ptrEnd, 10);

    if (*ptrEnd != '\x00')
    {
      std::cerr << "Incorrect number of task\n";
      return 1;
    }

    switch (taskNumber)
    {
      case 1:
        {
        if (argc != 3)
        {
          std::cerr << "Incorrect number of arguments.\n";
          return 1;
        }
        task1(getDirection(argv[2]));
        break;
      }
      case 2:
        {
        if (argc != 3)
        {
          std::cerr << "Incorrect number of arguments.\n";
          return 1;
        }
        task2(argv[2]);
        break;
      }
      case 3:
        {
        if (argc != 2)
        {
          std::cerr << "Incorrect number of arguments.\n";
          return 1;
        }
        task3();
        break;
      }
      case 4:
        {
        if (argc != 4)
        {
          std::cerr << "Incorrect number of arguments.\n";
          return 1;
        }
        task4(getDirection(argv[2]), std::stoi(argv[3]));
        break;
      }
      default:
        std::cerr << "Incorrect number of task.\n";
        return 1;
    }
  }
  catch (const std::exception& exception)
  {
    std::cerr << exception.what() << '\n';
    return 1;
  }
  return 0;
}
