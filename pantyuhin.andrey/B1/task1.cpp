#include <forward_list>
#include <vector>
#include "strategy.hpp"
#include "utility.hpp"

void task1(const bool ascending)
{
  std::vector<int> vectorBr;
  int tmp = -1;

  while (std::cin && !(std::cin >> tmp).eof())
  {
    if (std::cin.fail() || std::cin.bad())
    {
      throw std::invalid_argument("Incorrect input data");
    }
    vectorBr.push_back(tmp);
  }

  std::vector<int> vectorAt(vectorBr);
  std::forward_list<int> list(vectorBr.begin(), vectorBr.end());

  sort<atStrategy>(vectorAt, ascending);
  sort<brStrategy>(vectorBr, ascending);
  sort<itStrategy>(list, ascending);

  print(vectorAt);
  print(vectorBr);
  print(list);
}
