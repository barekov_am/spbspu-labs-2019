#ifndef PANTYUHIN_ANDREY_STRATEGY_HPP
#define PANTYUHIN_ANDREY_STRATEGY_HPP

#include <cstddef>
#include <iterator>

template <typename Container>
struct brStrategy
{
  static typename Container::reference getElement(Container& array, std::size_t index)
  {
    return array[index];
  }
  static typename std::size_t begin(const Container&)
  {
    return 0;
  }
  static typename std::size_t end(const Container& array)
  {
    return array.size();
  }
  static typename std::size_t next(std::size_t index)
  {
    return ++index;
  }
  typedef size_t index;
};

template<typename Container>
struct atStrategy
{
  static typename Container::reference getElement(Container& array, std::size_t index)
  {
    return array.at(index);
  }
  static typename std::size_t begin(const Container&)
  {
    return 0;
  }
  static typename std::size_t end(const Container& array)
  {
    return array.size();
  }
  static typename std::size_t next(std::size_t index)
  {
    return ++index;
  }
  typedef size_t index;
};

template<typename Container>
struct itStrategy
{
  static typename Container::reference getElement(Container&, typename Container::iterator iterator)
  {
    return *iterator;
  }
  static typename Container::iterator begin(Container& array)
  {
    return array.begin();
  }
  static typename Container::iterator end(Container& array)
  {
    return array.end();
  }
  static typename Container::iterator next(typename Container::iterator iterator)
  {
    return ++iterator;
  }
  typedef typename Container::iterator index;
};

#endif //PANTYUHIN_ANDREY_ACCESSES_HPP
