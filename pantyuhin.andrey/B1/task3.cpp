#include <vector>

#include "utility.hpp"
#include "strategy.hpp"
#include "utility.hpp"

void task3()
{
  std::vector<int> vector;
  int tmp = -1;
  while ((std::cin) && !(std::cin >> tmp).eof())
  {
    if (std::cin.fail() || std::cin.bad())
    {
      throw std::invalid_argument("Incorrect input data");
    }
    if (tmp == 0)
    {
      break;
    }
    vector.push_back(tmp);
  }

  if (vector.empty())
  {
    return;
  }

  if (std::cin.eof())
  {
    throw std::invalid_argument("Missing zero");
  }

  if (vector.back() == 1)
  {
    std::vector<int>::iterator iterator = vector.begin();
    while (iterator != vector.end())
    {
      iterator = ((*iterator) % 2 == 0) ? vector.erase(iterator) : ++iterator;
    }
  }

  if (vector.back() == 2)
  {
    std::vector<int>::iterator iterator = vector.begin();
    while (iterator != vector.end())
    {
      iterator = ((*iterator) % 3 == 0) ? (vector.insert(++iterator, 3, 1) + 3) : ++iterator;
    }
  }

  print(vector);
}
