#ifndef PANTYUHIN_ANDREY_UTILITY_HPP
#define PANTYUHIN_ANDREY_UTILITY_HPP

#include <iostream>
#include <functional>
#include <cstring>
#include <algorithm>

bool getDirection(const char* direction);

template<class Container>
void print(const Container& array)
{
  for (auto element: array)
  {
    std::cout << element << " ";
  }
  std::cout << '\n';
}

template<template<class> class Strategy, class Container>
void sort(Container& array, bool ascending)
{
  typedef Strategy<Container> Access;
  typedef typename Access::index index;

  index first = Access::begin(array);
  index last = Access::end(array);

  for (index i = first; i != last; i++)
  {
    index tmp = i;
    for (index j = Access::next(i); j != last; j++)
    {
      bool less = (Access::getElement(array, j) < Access::getElement(array, tmp));
      if ((ascending && less) || (!ascending && !less))
      {
        tmp = j;
      }
    }

    if (tmp != i)
    {
      std::swap(Access::getElement(array, tmp), Access::getElement(array, i));
    }
  }

}

#endif //PANTYUHIN_ANDREY_UTILITY_HPP
