#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <memory>

void task2(const char* file)
{
  std::ifstream in(file);
  if (!in.is_open())
  {
    throw std::runtime_error("Couldn't open the file.\n");
  }
  int capacity = 10;

  std::unique_ptr<char[], decltype(&free)> array(static_cast<char*>(calloc(capacity, 1)), free);

  if (!array)
  {
    throw std::runtime_error("Couldn't allocate memory.\n");
  }

  int size = 0;

  while (in)
  {
    in.read(&array[size], capacity - size);
    size += in.gcount();
    if (size == capacity)
    {
      capacity *= 2;
      array.reset(static_cast<char*>(realloc(array.release(), capacity)));
      if (!array)
      {
        in.close();
        throw std::runtime_error("Couldn't allocate memory.\n");
      }
    }
    if (!in.eof() && in.fail())
    {
      throw std::runtime_error("Input failed.\n");
    }
  }

  in.close();

  if (in.is_open())
  {
    throw std::runtime_error("Couldn't close the file.\n");
  }
  if (!size)
  {
    return;
  }
  std::vector<char> vector(&array[0], &array[size]);

  for (auto symbol: vector)
  {
    std::cout << symbol;
  }
}
