#ifndef PANTYUHIN_ANDREY_TASK_HPP
#define PANTYUHIN_ANDREY_TASK_HPP


#include <sstream>
#include <algorithm>
#include <iostream>
#include <vector>

#include "data-struct.hpp"

void task()
{
  std::vector<pantyuhin::DataStruct> vector;
  std::string line;
  while (std::getline(std::cin, line))
  {
    if (!std::cin && !std::cin.eof())
    {
      throw std::ios_base::failure("Input failed");
    }

    std::istringstream stream(line);
    vector.push_back(pantyuhin::readDataStruct(stream));
  }

  std::sort(vector.begin(), vector.end(),
      [](pantyuhin::DataStruct &lhs, pantyuhin::DataStruct &rhs)
  {
    if (lhs.key1 == rhs.key1 && lhs.key2 == rhs.key2)
    {
      return (lhs.str.length() < rhs.str.length());
    }
    else if (lhs.key1 == rhs.key1)
    {
      return (lhs.key2 < rhs.key2);
    }
    else
    {
      return (lhs.key1 < rhs.key1);
    }
  });

  std::for_each(vector.begin(), vector.end(), [](pantyuhin::DataStruct &data) { pantyuhin::printDataStruct(data); });
}

#endif //PANTYUHIN_ANDREY_TASK_HPP
