#include "stats.hpp"

StatsCounter::StatsCounter() :
  max_(0),
  min_(0),
  mean_(0),
  positive_(0),
  negative_(0),
  odd_sum_(0),
  even_sum_(0),
  equal_(false),
  first_(0),
  last_(0),
  counter_(0)
{ }

void StatsCounter::operator()(const int num)
{
  if (counter_ == 0)
  {
    first_ = num;
    min_ = num;
    max_ = num;
  }

  last_ = num;
  ++counter_;

  if (num > max_)
  {
    max_ = num;
  }

  if (num < min_)
  {
    min_ = num;
  }

  if (num > 0)
  {
    ++positive_;
  }

  if (num < 0)
  {
    ++negative_;
  }

  if (num % 2 == 0)
  {
    even_sum_ += num;
  }
  else
  {
    odd_sum_ += num;
  }
}

double StatsCounter::getMean()
{
  mean_ = static_cast<double>((even_sum_ + odd_sum_)) / static_cast<double>(counter_);
  return mean_;
}

bool StatsCounter::isEqual()
{
  equal_ = first_ == last_;

  return equal_;
}

int StatsCounter::getMax() const
{
  return max_;
}

int StatsCounter::getMin() const
{
  return min_;
}

unsigned int StatsCounter::getPositive() const
{
  return positive_;
}

unsigned int StatsCounter::getNegative() const
{
  return negative_;
}

long long int StatsCounter::getEvenSum() const
{
  return even_sum_;
}

long long int StatsCounter::getOddSum() const
{
  return odd_sum_;
}

unsigned int StatsCounter::getCounter() const
{
  return counter_;
}
