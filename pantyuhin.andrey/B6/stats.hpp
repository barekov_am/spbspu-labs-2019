#ifndef PANTYUHIN_ANDREY_STATS_HPP
#define PANTYUHIN_ANDREY_STATS_HPP

class StatsCounter {

public:
  StatsCounter();

  void operator()(int num);

  int getMax() const;
  int getMin() const;
  double getMean();
  unsigned int getPositive() const;
  unsigned int getNegative() const;
  long long int getOddSum() const;
  long long int getEvenSum() const;
  bool isEqual();
  unsigned int getCounter() const;

private:
  int max_;
  int min_;
  double mean_;
  unsigned int positive_;
  unsigned int negative_;
  long long int odd_sum_;
  long long int even_sum_;
  bool equal_;
  int first_;
  int last_;
  unsigned int counter_;
};


#endif //PANTYUHIN_ANDREY_STATS_HPP
