#include "circle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>

pantyuhin::Circle::Circle(const point_t& centre, double radius):
  centre_(centre),
  radius_(radius)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("radius of circle can't be <=0");
  }
}

pantyuhin::Circle::Circle(double x, double y, double radius):
  Circle({x, y}, radius)
{ }

void pantyuhin::Circle::printParameters() const
{
  std::cout << "circle: x=" << centre_.x << ", y=" << centre_.y << ", rad="
      << radius_ << std::endl << std::endl;
}

double pantyuhin::Circle::getArea() const
{
  return (M_PI * pow(radius_, 2));
}

pantyuhin::rectangle_t pantyuhin::Circle::getFrameRect() const
{
  return {centre_, radius_ * 2, radius_ * 2};
}

void pantyuhin::Circle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

void pantyuhin::Circle::move(const point_t& centre)
{
  centre_ = centre;
}

void pantyuhin::Circle::scale(double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("scale multiplier can't be <= 0");
  }
  radius_ *= multiplier;
}

void pantyuhin::Circle::rotate(double)
{ }
