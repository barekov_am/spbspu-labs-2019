#include <boost/test/auto_unit_test.hpp>

#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

const double PRECISION = 0.01;

BOOST_AUTO_TEST_SUITE(testMatrix)

  BOOST_AUTO_TEST_CASE(testCopyConstructor)
  {
    pantyuhin::Shape::pointer testCircle = std::make_shared<pantyuhin::Circle>(pantyuhin::point_t { 3.5, 4.5 }, 3);
    pantyuhin::Shape::pointer testRectangle = std::make_shared<pantyuhin::Rectangle>(pantyuhin::point_t { 1, 0 }, 1, 4);
    pantyuhin::CompositeShape testComposition;

    testComposition.add(testCircle);
    testComposition.add(testRectangle);

    pantyuhin::Matrix testMatrix = pantyuhin::part(testComposition);
    pantyuhin::Matrix testMatrixCopy(testMatrix);

    BOOST_CHECK(testMatrix == testMatrixCopy);
    BOOST_CHECK_EQUAL(testMatrixCopy.getRows(), testMatrix.getRows());
    BOOST_CHECK_EQUAL(testMatrixCopy.getColumns(), testMatrix.getColumns());
  }

  BOOST_AUTO_TEST_CASE(testMoveConstructor)
  {
    pantyuhin::Shape::pointer testCircle = std::make_shared<pantyuhin::Circle>(pantyuhin::point_t { 1, 2 }, 3);
    pantyuhin::Shape::pointer testRectangle = std::make_shared<pantyuhin::Rectangle>(pantyuhin::point_t { 1, 2 }, 1, 6);
    pantyuhin::CompositeShape testComposition;

    testComposition.add(testCircle);
    testComposition.add(testRectangle);

    pantyuhin::Matrix testMatrix = pantyuhin::part(testComposition);
    pantyuhin::Matrix testMatrixCopy(testMatrix);
    pantyuhin::Matrix testMatrixMove = std::move(testMatrix);

    BOOST_CHECK(testMatrixMove == testMatrixCopy);
    BOOST_CHECK_EQUAL(testMatrixMove.getRows(), testMatrixCopy.getRows());
    BOOST_CHECK_EQUAL(testMatrixMove.getColumns(), testMatrixCopy.getColumns());;
  }

  BOOST_AUTO_TEST_CASE(testCopyOperator)
  {
    pantyuhin::Shape::pointer testCircle = std::make_shared<pantyuhin::Circle>(pantyuhin::point_t { 3, 7.2 }, 11);
    pantyuhin::Shape::pointer testRectangle = std::make_shared<pantyuhin::Rectangle>(pantyuhin::point_t { 2, 4 }, 9, 2);
    pantyuhin::CompositeShape testComposition;

    testComposition.add(testCircle);
    testComposition.add(testRectangle);

    pantyuhin::Matrix testMatrix = pantyuhin::part(testComposition);
    pantyuhin::Matrix testMatrixCopy;
    testMatrixCopy = testMatrix;

    BOOST_CHECK(testMatrixCopy == testMatrix);
    BOOST_CHECK_EQUAL(testMatrixCopy.getRows(), testMatrix.getRows());
    BOOST_CHECK_EQUAL(testMatrixCopy.getColumns(), testMatrix.getColumns());;
  }

  BOOST_AUTO_TEST_CASE(testMoveOperator)
  {
    pantyuhin::Shape::pointer testCircle = std::make_shared<pantyuhin::Circle>(pantyuhin::point_t { 2, 0 }, 3);
    pantyuhin::Shape::pointer testRectangle = std::make_shared<pantyuhin::Rectangle>(pantyuhin::point_t { 2, 4 }, 9, 5);
    pantyuhin::CompositeShape testComposition;

    testComposition.add(testCircle);
    testComposition.add(testRectangle);

    pantyuhin::Matrix testMatrix = pantyuhin::part(testComposition);
    pantyuhin::Matrix testMatrixCopy(testMatrix);
    pantyuhin::Matrix testMatrixMove;
    testMatrixMove = std::move(testMatrix);

    BOOST_CHECK(testMatrixMove == testMatrixCopy);
    BOOST_CHECK_EQUAL(testMatrixMove.getRows(), testMatrixCopy.getRows());
    BOOST_CHECK_EQUAL(testMatrixMove.getColumns(), testMatrixCopy.getColumns());;
  }

  BOOST_AUTO_TEST_CASE(exceptOutOfRange)
  {
    pantyuhin::Shape::pointer testCircle = std::make_shared<pantyuhin::Circle>(pantyuhin::point_t { 2, 0 }, 3);
    pantyuhin::Shape::pointer testRectangle = std::make_shared<pantyuhin::Rectangle>(pantyuhin::point_t { 2, 4 }, 9, 5);
    pantyuhin::CompositeShape testComposition;

    testComposition.add(testCircle);
    testComposition.add(testRectangle);

    pantyuhin::Matrix testMatrix = pantyuhin::part(testComposition);

    BOOST_CHECK_THROW(testMatrix[105], std::out_of_range);
    BOOST_CHECK_THROW(testMatrix[-2], std::out_of_range);
  }

BOOST_AUTO_TEST_SUITE_END()
