#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace pantyuhin
{
  class Circle : public Shape
  {
  public:

    Circle(const point_t& centre, double radius);
    Circle(double x, double y, double radius);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printParameters() const override;
    void move(double dx, double dy) override;
    void move(const point_t& centre) override;
    void scale(double multiplier) override;
    void rotate(double) override;

  private:

    point_t centre_;
    double radius_;
  };
}

#endif //CIRCLE_HPP
