#ifndef A3_PARTITION_HPP
#define A3_PARTITION_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace pantyuhin
{
  pantyuhin::Matrix part(const pantyuhin::CompositeShape& composition);
}

#endif //A3_PARTITION_HPP
