#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace pantyuhin
{
  class Triangle : public Shape
  {
  public:

    Triangle(const point_t& vertex1, const point_t& vertex2, const point_t& vertex3);
    Triangle(double x1, double y1, double x2, double y2, double x3, double y3);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printParameters() const override;
    void move(double dx, double dy) override;
    void move(const point_t& newCentre) override;
    void scale(double multiplier) override;
    void rotate(double angle) override;

  private:

    point_t vertex_[3];

    point_t getCentre() const;
  };
}

#endif //A_TRIANGLE_HPP
