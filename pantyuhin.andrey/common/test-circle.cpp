#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteCircle)

  const double PRECISION = 0.01;

  BOOST_AUTO_TEST_CASE(CircleConstAfterMoving)
  {
    pantyuhin::Circle circle_({1, -1}, 2);
    const pantyuhin::rectangle_t firstFrame = circle_.getFrameRect();
    const double first = circle_.getArea();

    circle_.move({4, 5});
    pantyuhin::rectangle_t secondFrame = circle_.getFrameRect();
    double secondArea = circle_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
    BOOST_CHECK_CLOSE(first, secondArea, PRECISION);

    circle_.move(-2, 3);
    secondFrame = circle_.getFrameRect();
    secondArea = circle_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
    BOOST_CHECK_CLOSE(first, secondArea, PRECISION);
  }

  BOOST_AUTO_TEST_CASE(CircleScaling)
  {
    pantyuhin::Circle circle_({1, -1}, 2);
    double firstArea = circle_.getArea();

    double multiplier = 2.2;
    circle_.scale(multiplier);
    double secondArea = circle_.getArea();

    BOOST_CHECK_CLOSE(firstArea * multiplier * multiplier, secondArea, PRECISION);

    firstArea = secondArea;
    multiplier = 0.3;
    circle_.scale(multiplier);
    secondArea = circle_.getArea();

    BOOST_CHECK_CLOSE(firstArea *  multiplier * multiplier, secondArea, PRECISION);
  }

  BOOST_AUTO_TEST_CASE(CircleThrowingExceptions)
  {
    BOOST_CHECK_THROW(pantyuhin::Circle({3, 4}, -2), std::invalid_argument);


    pantyuhin::Circle circle_({6, 5}, 7.5);
    BOOST_CHECK_THROW(circle_.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(testCircleRotation)
  {
    pantyuhin::Circle testCircle({5.0, 5.0}, 2.0);
    const double areaBefore = testCircle.getArea();
    const pantyuhin::rectangle_t frameRectBefore = testCircle.getFrameRect();

    double angle = 90;
    testCircle.rotate(angle);
    double areaAfter = testCircle.getArea();
    pantyuhin::rectangle_t frameRectAfter = testCircle.getFrameRect();

    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);

    angle = -150;
    testCircle.rotate(angle);
    areaAfter = testCircle.getArea();
    frameRectAfter = testCircle.getFrameRect();

    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);
  }

BOOST_AUTO_TEST_SUITE_END()
