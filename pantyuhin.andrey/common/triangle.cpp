#include "triangle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>

pantyuhin::Triangle::Triangle(const point_t& vertexA, const point_t& vertexB, const point_t& vertexC):
    vertex_{vertexA, vertexB, vertexC}
{
  double precision = 0.001;

  if (abs((vertexC.x - vertexA.x) * (vertexB.y - vertexA.y) - (vertexC.y - vertexA.y) * (vertexB.x - vertexA.x))
      < precision)
  {
    throw std::invalid_argument("vertexes of triangle can't lie on the same straight line or match");
  }
}

pantyuhin::Triangle::Triangle(double x1, double y1, double x2, double y2, double x3, double y3):
  Triangle({x1, y1}, {x2, y2}, {x3, y3})
{ }

pantyuhin::point_t pantyuhin::Triangle::getCentre() const
{
  return {(vertex_[0].x + vertex_[1].x + vertex_[2].x) / 3, ((vertex_[0].y + vertex_[1].y + vertex_[2].y) / 3)};
}

void pantyuhin::Triangle::printParameters() const
{
  for (int i = 0; i <= 2; i++)
  {
    std::cout << "(" << vertex_[i].x << ", " << vertex_[i].y << "); ";
  }
  std::cout << std::endl << std::endl;
}

double pantyuhin::Triangle::getArea() const
{
  return fabs((vertex_[0].x - vertex_[2].x) * (vertex_[1].y - vertex_[2].y)
      - (vertex_[1].x - vertex_[2].x) * (vertex_[0].y - vertex_[2].y)) / 2;
}

pantyuhin::rectangle_t pantyuhin::Triangle::getFrameRect() const
{
  double maxX = std::max(std::max(vertex_[0].x, vertex_[1].x), vertex_[2].x);
  double maxY = std::max(std::max(vertex_[0].y, vertex_[1].y), vertex_[2].y);
  double minX = std::min(std::min(vertex_[0].x, vertex_[1].x), vertex_[2].x);
  double minY = std::min(std::min(vertex_[0].y, vertex_[1].y), vertex_[2].y);

  return {{minX + (maxX - minX) / 2, minY + (maxY - minY) / 2}, maxX - minX, maxY - minY};
}

void pantyuhin::Triangle::move(double dx, double dy)
{
  for (int i = 0; i <= 2; i++)
  {
    vertex_[i].x += dx;
    vertex_[i].y += dy;
  }
}

void pantyuhin::Triangle::move(const point_t& newCentre)
{
  point_t oldCentre = getCentre();
  double dx = newCentre.x - oldCentre.x;
  double dy = newCentre.y - oldCentre.y;

  move(dx, dy);
}

void pantyuhin::Triangle::scale(double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("scale multiplier can't be <=0");
  }

  point_t centre = getCentre();
  for (int i = 0; i <= 2; i++)
  {
    vertex_[i].x = centre.x + multiplier * (vertex_[i].x - centre.x);
    vertex_[i].y = centre.y + multiplier * (vertex_[i].y - centre.y);
  }
}

void pantyuhin::Triangle::rotate(double degree)
{
  const double cos = std::cos((2 * M_PI * degree) / 360);
  const double sin = std::sin((2 * M_PI * degree) / 360);

  const point_t centre = getCentre();

  for (std::size_t i = 0; i < 2; i++)
  {
    vertex_[i].x = centre.x + (vertex_[i].x - centre.x) * cos - (vertex_[i].y - centre.y) * sin;
    vertex_[i].y = centre.y + (vertex_[i].x - centre.x) * sin + (vertex_[i].y - centre.y) * cos;
  }
}
