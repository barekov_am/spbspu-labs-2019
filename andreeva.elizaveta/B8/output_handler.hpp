#ifndef INPUT_HANDLER_HPP
#define INPUT_HANDLER_HPP

#include <string>

#include "token.hpp"

namespace andreeva
{
  class OutputHandler
  {
  public:

    OutputHandler(std::ostream & os, size_t width);
    void printLines(const std::vector<token_t> & token_array);

  private:
    std::ostream & os_;
    size_t width_;
    std::vector<std::string> line_vec_;

    void formatLines(const std::vector<token_t> & token_array);
    void connectTokens(std::vector<andreeva::token_t> & tokens, size_t & pos, token_type_t type);
    std::string createLine(const std::vector<andreeva::token_t> & tokens);
  };
}

#endif
