#include <cstring>

#include "token_gen.hpp"
#include "output_handler.hpp"

int main(int argc, char ** argv)
{
  try
  {
    size_t default_width = 40;
    const size_t min_width = 25;

    if (argc > 1)
    {
      if (argc != 3)
      {
        throw std::invalid_argument("Error!! Wrong number of arguments!");
      }
      else if (strcmp(argv[1], "--line-width") != 0)
      {
        throw std::invalid_argument("Error!! Wrong syntax of argumnets!");
      }
      else
      {
        default_width = static_cast<size_t>(std::stoi(argv[2]));
        if (default_width < min_width)
        {
          throw std::invalid_argument("Error!! Invalid length!");
        }
      }
    }

    andreeva::TokenGenerator in_handler(std::cin);
    std::vector<andreeva::token_t> token_vec = in_handler.parseText();

    andreeva::OutputHandler out_handler(std::cout, default_width);
    out_handler.printLines(token_vec);
  }
  catch (const std::invalid_argument & iex)
  {
    std::cerr << iex.what();
    return 1;
  }
  catch (const std::exception & exc)
  {
    std::cerr << exc.what();
    return 1;
  }
  return 0;
}
