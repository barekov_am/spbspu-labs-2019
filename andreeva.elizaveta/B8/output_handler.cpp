#include "output_handler.hpp"

andreeva::OutputHandler::OutputHandler(std::ostream & os, size_t width) :
  os_(os),
  width_(width)
{
}

void andreeva::OutputHandler::printLines(const std::vector<token_t> & token_array)
{
  formatLines(token_array);
  for (auto line : line_vec_)
  {
    os_ << line << std::endl;
  }
}

void andreeva::OutputHandler::formatLines(const std::vector<token_t> & token_array)
{
  std::vector<andreeva::token_t> temp;
  size_t current_width = 0;
  size_t i = 0;

  if (token_array.empty())
  {
    temp.push_back(token_t
    {
      " ", token_type_t::WHITESPACE
    });
  }

  for (; i < token_array.size(); i++)
  {
    if (current_width + token_array[i].value.size() > width_)
    {
      connectTokens(temp, i, token_array[i].type);
      i -= 1;
      current_width = 0;
    }
    else
    {
      temp.push_back(token_array[i]);
      current_width += token_array[i].value.size();
      if (current_width + 1 <= width_)
      {
        switch (token_array[i].type)
        {
        case token_type_t::NUMBER:
        case token_type_t::WORD:
          if (i + 1 < token_array.size())
          {
            if (token_array[i + 1].type != token_type_t::PUNCT)
            {
              temp.push_back(token_t
                {
                " ", token_type_t::WHITESPACE
                });
              current_width++;
            }
          }
          break;
        case token_type_t::PUNCT:
        case token_type_t::DASH:
          temp.push_back(token_t
            {
            " ", token_type_t::WHITESPACE
            });
          current_width++;
          break;
        case token_type_t::WHITESPACE:
          break;
        }
      }
    }
  }
  line_vec_.push_back(createLine(temp));
}

std::string andreeva::OutputHandler::createLine(const std::vector<andreeva::token_t> & tokens)
{
  std::string buffer = "";
  for (auto t : tokens)
  {
    buffer += t.value;
  }
  if (tokens.back().type == token_type_t::WHITESPACE)
  {
    buffer.pop_back();
  }
  return buffer;
}

void andreeva::OutputHandler::connectTokens(std::vector<andreeva::token_t> & tokens, size_t & pos, token_type_t type)
{
  switch (type)
  {
  case token_type_t::WORD:
  case token_type_t::NUMBER:
    line_vec_.push_back(createLine(tokens));
    break;
  case token_type_t::PUNCT:
  case token_type_t::DASH:
    while (tokens.back().type != token_type_t::NUMBER
      && tokens.back().type != token_type_t::WORD
      && !tokens.empty())
    {
      if (tokens.back().type != token_type_t::WHITESPACE && pos > 0)
      {
        pos--;
      }
      tokens.pop_back();
    }
    if (pos > 0)
    {
      pos--;
    }
    tokens.pop_back();
    line_vec_.push_back(createLine(tokens));
    break;
  case token_type_t::WHITESPACE:
    break;
  }
  tokens.clear();
}
