#include "token_gen.hpp"

andreeva::TokenGenerator::TokenGenerator(std::istream & is) :
  is_(is)
{
}

std::vector<andreeva::token_t> andreeva::TokenGenerator::parseText()
{
  if (!is_)
  {
    throw std::invalid_argument("Error!! Empty input stream!");
  }
  genTokens();
  checkInput();
  return token_vec_;
}

void andreeva::TokenGenerator::readWord()
{
  token_t el
  {
    "", token_type_t::WORD
  };

  do
  {
    auto c = static_cast<char>(is_.get());
    el.value.push_back(c);
    if (c == '-' && is_.peek() == '-')
    {
      el.value.pop_back();
      is_.unget();
      break;
    }
  } while ((std::isalpha(static_cast<char>(is_.peek()), std::locale())) || (is_.peek() == '-'));
  token_vec_.push_back(el);
}

void andreeva::TokenGenerator::readNum()
{
  token_t token
  {
    "", token_type_t::NUMBER
  };

  char decimalPoint = std::use_facet<std::numpunct<char>>(std::locale()).decimal_point();
  bool decimalPointRead = false;

  do
  {
    auto c = static_cast<char>(is_.get());
    if (c == decimalPoint)
    {
      if (decimalPointRead)
      {
        is_.unget();
        break;
      }
      decimalPointRead = true;
    }
    token.value.push_back(c);
  } while ((std::isdigit<char>(static_cast<char>(is_.peek()), std::locale()) || (is_.peek() == decimalPoint)));
  token_vec_.push_back(token);
}

void andreeva::TokenGenerator::readDash()
{
  token_t token
  {
    "", token_type_t::DASH
  };

  while (is_.peek() == '-')
  {
    auto c = static_cast<char>(is_.get());
    token.value.push_back(c);
  }
  token_vec_.push_back(token);
}

void andreeva::TokenGenerator::genTokens()
{
  while (is_)
  {
    auto c = static_cast<char>(is_.get());
    while (std::isspace(c, std::locale()))
    {
      c = static_cast<char>(is_.get());
    }

    if (isalpha(c, std::locale()))
    {
      is_.unget();
      readWord();
    }
    else if (c == '-')
    {
      if (is_.peek() == '-')
      {
        is_.unget();
        readDash();
      }
      else
      {
        is_.unget();
        readNum();
      }
    }
    else if ((c == '+') || (isdigit(c, std::locale())))
    {
      is_.unget();
      readNum();
    }
    else if (ispunct(c, std::locale()))
    {
      token_t token{
        "", token_type_t::PUNCT
      };
      token.value.push_back(c);
      token_vec_.push_back(token);
    }
  }
}

void andreeva::TokenGenerator::checkInput()
{
  if (!token_vec_.empty() &&
    (token_vec_.front().type != token_type_t::WORD)
    && (token_vec_.front().type != token_type_t::NUMBER))
  {
    throw std::invalid_argument("Error!! Input starts with punctuation or dash.");
  }

  for (auto token = token_vec_.begin(); token != token_vec_.end(); ++token)
  {
    switch (token->type)
    {
    case token_type_t::WORD:
    case token_type_t::NUMBER:
      if (token->value == "-" || token->value == "+")
      {
        throw std::invalid_argument("Error!! Incorrect dash sign.");
      }
      if (token->value.size() > 20)
      {
        throw std::invalid_argument("Error!! Length of char sequence is > 20 el.");
      }
      break;
    case token_type_t::DASH:
      if (token->value.size() != 3)
      {
        throw std::invalid_argument("Error!! Incorrect dash sign.");
      }
      if (token != token_vec_.begin())
      {
        const auto prev = std::prev(token);
        if ((prev->type == token_type_t::DASH) || ((prev->type == token_type_t::PUNCT) && (prev->value != ",")))
        {
          throw std::invalid_argument("Error!! Incorrect char sequence.");
        }
      }
      break;
    case token_type_t::PUNCT:
      if (token != token_vec_.begin())
      {
        const auto prev = std::prev(token);
        if ((prev->type == token_type_t::DASH) || (prev->type == token_type_t::PUNCT))
        {
          throw std::invalid_argument("Error!! Incorrect char sequence.");
        }
      }
      break;
    case token_type_t::WHITESPACE:
      break;
    }
  }
}
