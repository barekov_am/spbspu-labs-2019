#ifndef TOKEN_TYPE_HPP
#define TOKEN_TYPE_HPP

#include <iostream>
#include <vector>

namespace andreeva
{
  enum class token_type_t
  {
    WORD,
    PUNCT,
    DASH,
    NUMBER,
    WHITESPACE
  };

  struct token_t
  {
    std::string value;
    token_type_t type;
  };

}

#endif
