#ifndef TOKEN_GEN_HPP
#define TOKEN_GEN_HPP

#include <locale>

#include "token.hpp"

namespace andreeva
{
  class TokenGenerator
  {
  public:
    explicit TokenGenerator(std::istream & is);
    std::vector<token_t> parseText();

  private:
    std::istream & is_;
    std::vector<andreeva::token_t> token_vec_;

    void readWord();
    void readNum();
    void readDash();
    void genTokens();
    void checkInput();
  };
}

#endif
