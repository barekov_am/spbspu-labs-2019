#include <algorithm>
#include <iterator>
#include <functional>

#include "functor.hpp"

int main()
{
  try
  {
    andreeva::Functor functor;
    std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), std::ref(functor));

    if (!std::cin.eof())
    {
      std::cerr << "Incorrect input!";
      return 1;
    }

    std::cout << functor;
  }
  catch (std::exception& exc)
  {
    std::cout << exc.what();
  }
  return 0;
}
