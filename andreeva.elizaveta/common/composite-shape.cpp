#include <stdexcept>
#include <cmath>
#include <algorithm>
#include "composite-shape.hpp"

andreeva::CompositeShape::CompositeShape():
  count_(0),
  shapes_(nullptr)
{
}

andreeva::CompositeShape::CompositeShape(const CompositeShape &compositeShape):
  count_(compositeShape.count_),
  shapes_(std::make_unique<std::shared_ptr<andreeva::Shape>[]>(compositeShape.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i] = compositeShape.shapes_[i];
  }
}

andreeva::CompositeShape::CompositeShape(andreeva::CompositeShape &&compositeShape):
  count_(compositeShape.count_),
  shapes_(std::move(compositeShape.shapes_))
{
  compositeShape.count_ = 0;
}


andreeva::CompositeShape::CompositeShape(const andreeva::Shape::ptr_shape &shape):
  count_(1),
  shapes_(std::make_unique<std::shared_ptr<andreeva::Shape>[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("You can't create composite shape with nullptr");
  }
  shapes_[0] = shape;
}

andreeva::CompositeShape &andreeva::CompositeShape::operator =(const CompositeShape &compositeShape)
{
  if (&compositeShape != this)
  {
    count_ = compositeShape.count_;
    shapes_ = std::make_unique<andreeva::Shape::ptr_shape[]>(compositeShape.count_);
    for (size_t i = 0; i < count_; ++i)
    {
      shapes_[i] = compositeShape.shapes_[i];
    }
  }
  return *this;
}

andreeva::CompositeShape &andreeva::CompositeShape::operator =(andreeva::CompositeShape &&compositeShape)
{
  if (&compositeShape != this)
  {
    count_ = compositeShape.count_;
    shapes_ = std::move(compositeShape.shapes_);
  }
  return *this;
}


andreeva::Shape::ptr_shape andreeva::CompositeShape::operator[](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }
  return shapes_[index];
}

void andreeva::CompositeShape::addShape(const andreeva::Shape::ptr_shape &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("No shape to add");
  }

  andreeva::Shape::array_ptr newShapeList(std::make_unique<std::shared_ptr<andreeva::Shape>[]>(count_ + 1));
  for (size_t i = 0; i < count_; i++)
  {
    newShapeList[i] = shapes_[i];
  }
  newShapeList[count_] = shape;
  count_++;
  shapes_.swap(newShapeList);
}

void andreeva::CompositeShape::deleteShape(size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }

  for (size_t i = index; i < count_ - 1; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  shapes_[count_ - 1] = nullptr;
  count_--;
}

void andreeva::CompositeShape::deleteShape(std::shared_ptr<andreeva::Shape> shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("There is no shape to delete");
  }

  for (size_t i = 0; i < count_; i++)
  {
    if (shapes_[i] == shape)
    {
      deleteShape(i);
      break;
    }
  }
}

double andreeva::CompositeShape::getArea() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  double area = 0.0;
  for (size_t i = 0; i < count_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

andreeva::rectangle_t andreeva::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  rectangle_t tmpFrameRect = shapes_[0]->getFrameRect();
  double minX = tmpFrameRect.pos.x - tmpFrameRect.width / 2;
  double maxX = tmpFrameRect.pos.x + tmpFrameRect.width / 2;
  double minY = tmpFrameRect.pos.y - tmpFrameRect.height / 2;
  double maxY = tmpFrameRect.pos.y + tmpFrameRect.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    tmpFrameRect = shapes_[i]->getFrameRect();
    minX = std::min(tmpFrameRect.pos.x - tmpFrameRect.width / 2, minX);
    maxX = std::max(tmpFrameRect.pos.x + tmpFrameRect.width / 2, maxX);
    minY = std::min(tmpFrameRect.pos.y - tmpFrameRect.height / 2, minY);
    maxY = std::max(tmpFrameRect.pos.y + tmpFrameRect.height / 2, maxY);
  }
  return rectangle_t{maxX - minX, maxY - minY, point_t{(maxX + minX) / 2, (maxY + minY) / 2}};
}

void andreeva::CompositeShape::move(double dx, double dy)
{
  if (count_ == 0)
  {
    throw std::logic_error("There are no shapes");
  }

  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void andreeva::CompositeShape::move(const andreeva::point_t &newCenter)
{
  point_t frame = getFrameRect().pos;
  double dx = newCenter.x - frame.x;
  double dy = newCenter.y - frame.y;
  move(dx, dy);
}

void andreeva::CompositeShape::scale(double factor)
{
  if (count_ == 0)
  {
    throw std::logic_error("There are no shapes");
  }

  if (factor <= 0)
  {
    throw std::invalid_argument("Factor must be positive");
  }

  point_t majorFrameRect = getFrameRect().pos;
  for (size_t i = 0; i < count_; i++)
  {
    point_t frame = shapes_[i]->getFrameRect().pos;
    double dx = frame.x - majorFrameRect.x;
    double dy = frame.y - majorFrameRect.y;
    shapes_[i]->move({majorFrameRect.x + dx * factor, majorFrameRect.y + dy * factor});
    shapes_[i]->scale(factor);
  }
}

size_t andreeva::CompositeShape::size() const
{
  return count_;
}

void andreeva::CompositeShape::rotate(double angle)
{
  const andreeva::point_t center = getFrameRect().pos;
  const double sinus = std::sin(angle * M_PI / 180);
  const double cosine = std::cos(angle * M_PI / 180);
  for (size_t i = 0; i < count_; ++i)
  {
    const andreeva::point_t shapeCenter = shapes_[i]->getFrameRect().pos;
    const double dx = shapeCenter.x - center.x;
    const double dy = shapeCenter.y - center.y;
    shapes_[i]->move(dx * (cosine - 1) - dy * sinus, dx * sinus + dy * (cosine - 1));
    shapes_[i]->rotate(angle);
  }
}
