#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(A3TestingRectangle)

  const double accuracy = 0.01;

  BOOST_AUTO_TEST_CASE(movingRectangle)
  {
    andreeva::Rectangle testRectangle({8.0, 6.0}, 1.5, 1.5);
    const andreeva::rectangle_t rectangleBeforeMoving = testRectangle.getFrameRect();
    const double areaBeforeMoving = testRectangle.getArea();

    testRectangle.move({1.1, 1.2});
    BOOST_CHECK_CLOSE(rectangleBeforeMoving.width, testRectangle.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(rectangleBeforeMoving.height, testRectangle.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(areaBeforeMoving, testRectangle.getArea(), accuracy);

    testRectangle.move(-4.0, 7.0);
    BOOST_CHECK_CLOSE(rectangleBeforeMoving.width, testRectangle.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(rectangleBeforeMoving.height, testRectangle.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(areaBeforeMoving, testRectangle.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(scalingRectangle)
  {
    andreeva::Rectangle testRectangle({5.0, 5.0}, 2.0, 2.0);
    const double areaBeforeScaling = testRectangle.getArea();
    const double factor = 3.0;
    testRectangle.scale(factor);
    BOOST_CHECK_CLOSE(testRectangle.getArea(), factor * factor * areaBeforeScaling, accuracy);
  }

  BOOST_AUTO_TEST_CASE(invalidShapeValues)
  {
    BOOST_CHECK_THROW(andreeva::Rectangle({4.5, 3.2}, -2.0, 5.0), std::invalid_argument);
    BOOST_CHECK_THROW(andreeva::Rectangle({9.0, 4.7}, 5.0, -5.0), std::invalid_argument);

    andreeva::Rectangle testRectangle({4.0, 4.0}, 8.0, 8.0);
    BOOST_CHECK_THROW(testRectangle.scale(-3.6), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(rectangleRotation)
  {
    andreeva::Rectangle testRectangle({0, 0}, 10, 10);
    double areaBefore = testRectangle.getArea();

    double angle = -98.76;
    BOOST_CHECK_NO_THROW(testRectangle.rotate(angle));

    angle = 55.55;
    BOOST_CHECK_NO_THROW(testRectangle.rotate(angle));

    angle = -21.5;
    testRectangle.rotate(angle);
    BOOST_CHECK_CLOSE(testRectangle.getArea(), areaBefore, accuracy);

    angle = 12.34;
    testRectangle.rotate(angle);
    BOOST_CHECK_CLOSE(testRectangle.getArea(), areaBefore, accuracy);

    angle = 361.0;
    testRectangle.rotate(angle);
    BOOST_CHECK_CLOSE(testRectangle.getArea(), areaBefore, accuracy);

    angle = -1000.1;
    testRectangle.rotate(angle);
    BOOST_CHECK_CLOSE(testRectangle.getArea(),areaBefore, accuracy);
  }

BOOST_AUTO_TEST_SUITE_END()
