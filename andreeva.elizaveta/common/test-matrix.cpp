#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "composite-shape.hpp"
#include "separation.hpp"
#include "matrix.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(matrixTestSuite)

  BOOST_AUTO_TEST_CASE(copyAndMoveConstructor)
  {
    andreeva::Rectangle r1({-1.0, -1.0}, 4.0, 4.0);
    andreeva::Circle c1({3.0, 2.0}, 1.0);
    andreeva::CompositeShape testShapes(std::make_shared<andreeva::Rectangle>(r1));
    testShapes.addShape(std::make_shared<andreeva::Circle>(c1));

    andreeva::Matrix testMatrix = andreeva::layer(testShapes);

    BOOST_CHECK_NO_THROW(andreeva::Matrix testMatrix2(testMatrix));
    BOOST_CHECK_NO_THROW(andreeva::Matrix testMatrix3(std::move(testMatrix)));
  }

  BOOST_AUTO_TEST_CASE(operatorTests)
  {
    andreeva::Rectangle r1({-1.0, -1.0}, 4.0, 4.0);
    andreeva::Circle c1({3.0, 2.0}, 1.0);
    andreeva::CompositeShape testShapes1(std::make_shared<andreeva::Rectangle>(r1));
    testShapes1.addShape(std::make_shared<andreeva::Circle>(c1));

    andreeva::Matrix testMatrix1 = andreeva::layer(testShapes1);

    BOOST_CHECK_THROW(testMatrix1[100], std::out_of_range);
    BOOST_CHECK_THROW(testMatrix1[-1], std::out_of_range);

    BOOST_CHECK_NO_THROW(andreeva::Matrix testMatrix2 = testMatrix1);
    BOOST_CHECK_NO_THROW(andreeva::Matrix testMatrix3 = std::move(testMatrix1));

    andreeva::Rectangle r2({-1.0, -1.0}, 4.0, 4.0);
    andreeva::CompositeShape testShapes2(std::make_shared<andreeva::Rectangle>(r2));

    andreeva::Matrix testMatrix4 = andreeva::layer(testShapes2);
    andreeva::Matrix testMatrix5 = andreeva::layer(testShapes2);

    BOOST_CHECK(testMatrix4 == testMatrix5);

    BOOST_CHECK(testMatrix4 != testMatrix1);
    BOOST_CHECK(testMatrix5 != testMatrix1);
  }

BOOST_AUTO_TEST_SUITE_END()
