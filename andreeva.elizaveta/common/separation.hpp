#ifndef A4_SEPARATION_HPP
#define A4_SEPARATION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace andreeva
{
  bool cross(const Shape &firstShape, const Shape &secondShape);
  Matrix layer(const CompositeShape&);
}

#endif
