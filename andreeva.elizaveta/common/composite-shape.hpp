#ifndef A4_COMPOSITE_SHAPE_HPP
#define A4_COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include <memory>

namespace andreeva
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &compositeShape);
    CompositeShape(CompositeShape &&compositeShape);
    CompositeShape(const andreeva::Shape::ptr_shape &shape);

    ~CompositeShape() = default;


    CompositeShape &operator =(const CompositeShape &compositeShape);
    CompositeShape &operator =(CompositeShape &&compositeShape);

    andreeva::Shape::ptr_shape operator [](std::size_t index) const;

    void addShape(const andreeva::Shape::ptr_shape &shape);
    void deleteShape(std::shared_ptr<andreeva::Shape> shape);
    void deleteShape(std::size_t index);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &point) override;
    void scale(double factor) override;
    std::size_t size() const;
    void rotate(double angle);

  private:
    std::size_t count_;
    andreeva::Shape::array_ptr shapes_;

  };
}

#endif //A3_COMPOSITE_SHAPE_HPP
