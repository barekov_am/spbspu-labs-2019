#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(A3TestingCircle)

  const double accuracy = 0.01;

  BOOST_AUTO_TEST_CASE(movingCircle)
  {
    andreeva::Circle testCircle({2.5, 3.0}, 2.5);
    const andreeva::rectangle_t rectangleBeforeMoving = testCircle.getFrameRect();
    const double areaBeforeMoving = testCircle.getArea();

    testCircle.move(-1.0, 8.8);
    BOOST_CHECK_CLOSE(rectangleBeforeMoving.width, testCircle.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(rectangleBeforeMoving.height, testCircle.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(areaBeforeMoving, testCircle.getArea(), accuracy);

    testCircle.move({3.5, 3.6});
    BOOST_CHECK_CLOSE(rectangleBeforeMoving.width, testCircle.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(rectangleBeforeMoving.height, testCircle.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(areaBeforeMoving, testCircle.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(scalingCircle)
  {
    andreeva::Circle testCircle({1.0, 1.0}, 10.0);
    const double areaBeforeScaling = testCircle.getArea();
    const double factor = 4.2;
    testCircle.scale(factor);
    BOOST_CHECK_CLOSE(testCircle.getArea(), factor * factor * areaBeforeScaling, accuracy);
  }

  BOOST_AUTO_TEST_CASE(invalidShapeValues)
  {
    BOOST_CHECK_THROW(andreeva::Circle({3.0, 1.5}, -3.5), std::invalid_argument);
    BOOST_CHECK_THROW(andreeva::Circle({4.0, 4.0}, -8.0), std::invalid_argument);

    andreeva::Circle testCircle({4.0, 4.0}, 8.0);
    BOOST_CHECK_THROW(testCircle.scale(-3.6), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(circleRotation)
  {
    andreeva::Circle testCircle({0, 0}, 10);
    double areaBefore = testCircle.getArea();

    double angle = -98.71;
    BOOST_CHECK_NO_THROW(testCircle.rotate(angle));

    angle = 55.5;
    BOOST_CHECK_NO_THROW(testCircle.rotate(angle));

    angle = -21.5;
    testCircle.rotate(angle);
    BOOST_CHECK_CLOSE(testCircle.getArea(), areaBefore, accuracy);

    angle = 12.34;
    testCircle.rotate(angle);
    BOOST_CHECK_CLOSE(testCircle.getArea(), areaBefore, accuracy);

    angle = 361.0;
    testCircle.rotate(angle);
    BOOST_CHECK_CLOSE(testCircle.getArea(), areaBefore, accuracy);

    angle = -1100.1;
    testCircle.rotate(angle);
    BOOST_CHECK_CLOSE(testCircle.getArea(),areaBefore, accuracy);
  }

BOOST_AUTO_TEST_SUITE_END()
