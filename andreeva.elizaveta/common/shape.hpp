#ifndef A4_SHAPE_HPP
#define A4_SHAPE_HPP
#include "base-types.hpp"
#include <memory>

namespace andreeva
{
  class Shape
  {
  public:
    using ptr_shape = std::shared_ptr<Shape>;
    using array_ptr = std::unique_ptr<ptr_shape[]>;

    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void move(const point_t &point) = 0;
    virtual void scale(double factor) = 0;
    virtual void rotate(double) = 0;
  };
}
#endif
