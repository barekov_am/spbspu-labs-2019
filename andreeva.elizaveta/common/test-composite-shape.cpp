#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(A3TestingCompositeShape)

  const double accuracy = 0.01;

  BOOST_AUTO_TEST_CASE(movingShape)
  {
    andreeva::Rectangle testRectangle({0.0, 0.0}, 2.0, 5.0);
    andreeva::Circle testCircle({2.5, 3.0}, 1.0);
    andreeva::CompositeShape compositeShape(std::make_shared<andreeva::Rectangle>(testRectangle));
    compositeShape.addShape(std::make_shared<andreeva::Circle>(testCircle));

    const andreeva::rectangle_t compositeShapeBefore = compositeShape.getFrameRect();
    const double areaBeforeMoving = compositeShape.getArea();

    compositeShape.move(2.0, 2.0);
    BOOST_CHECK_CLOSE(compositeShapeBefore.width, compositeShape.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(compositeShapeBefore.height, compositeShape.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(areaBeforeMoving, compositeShape.getArea(), accuracy);

    compositeShape.move({4.4, 7.7});
    BOOST_CHECK_CLOSE(compositeShapeBefore.width, compositeShape.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(compositeShapeBefore.height, compositeShape.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(areaBeforeMoving, compositeShape.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(scalingShape)
  {
    andreeva::Rectangle testRectangle({0.0, 0.0}, 2.0, 5.0);
    andreeva::Circle testCircle({2.5, 3.0}, 1.0);
    andreeva::CompositeShape compositeShape(std::make_shared<andreeva::Rectangle>(testRectangle));
    compositeShape.addShape(std::make_shared<andreeva::Circle>(testCircle));

    const double areaBeforeScaling = compositeShape.getArea();
    const double factor = 2.0;

    compositeShape.scale(factor);
    BOOST_CHECK_CLOSE(areaBeforeScaling * factor * factor, compositeShape.getArea(), accuracy);

    const double areaBeforeScaling1 = compositeShape.getArea();
    const double factor1 = 0.5;

    compositeShape.scale(factor1);
    BOOST_CHECK_CLOSE(areaBeforeScaling1 * factor1 * factor1, compositeShape.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(invalidShapeValues)
  {
    andreeva::CompositeShape compositeShape;

    BOOST_CHECK_THROW(compositeShape.scale(4.5), std::logic_error);
    BOOST_CHECK_THROW(compositeShape.move(2.0, 2.0), std::logic_error);
    BOOST_CHECK_THROW(compositeShape.move({3.0, 2.0}), std::logic_error);
    BOOST_CHECK_THROW(compositeShape.getFrameRect(), std::logic_error);
    BOOST_CHECK_THROW(compositeShape.getArea(), std::logic_error);

    andreeva::Rectangle testRectangle({0.0, 0.0}, 2.0, 5.0);
    andreeva::Circle testCircle({2.5, 3.0}, 1.0);
    compositeShape.addShape(std::make_shared<andreeva::Rectangle>(testRectangle));
    compositeShape.addShape(std::make_shared<andreeva::Circle>(testCircle));

    BOOST_CHECK_THROW(compositeShape.scale(-9.9), std::invalid_argument);
    BOOST_CHECK_THROW(compositeShape.scale(0), std::invalid_argument);
    BOOST_CHECK_THROW(compositeShape.addShape(nullptr), std::invalid_argument);
    BOOST_CHECK_THROW(compositeShape.deleteShape(3), std::out_of_range);
    BOOST_CHECK_THROW(compositeShape.deleteShape(-1), std::out_of_range);
    BOOST_CHECK_THROW(compositeShape.deleteShape(nullptr), std::invalid_argument);
    BOOST_CHECK_THROW(compositeShape[-1], std::out_of_range);
    BOOST_CHECK_THROW(compositeShape[99], std::out_of_range);

    BOOST_CHECK_THROW(andreeva::CompositeShape compositeShape1(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(addingShape)
  {
    andreeva::Rectangle testRectangle({0.0, 0.0}, 2.0, 5.0);
    andreeva::CompositeShape compositeShape(std::make_shared<andreeva::Rectangle>(testRectangle));
    const double areaBefore = compositeShape.getArea();
    andreeva::Circle testCircle({2.0, 2.0}, 1.0);
    compositeShape.addShape(std::make_shared<andreeva::Circle>(testCircle));
    const double areaCircle = testCircle.getArea();
    BOOST_CHECK_CLOSE(areaBefore + areaCircle, compositeShape.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(deletingShape)
  {
    andreeva::Rectangle testRectangle({0.0, 0.0}, 2.0, 5.0);
    andreeva::Circle testCircle({2.0, 2.0}, 1.0);
    andreeva::CompositeShape compositeShape(std::make_shared<andreeva::Rectangle>(testRectangle));
    compositeShape.addShape(std::make_shared<andreeva::Circle>(testCircle));
    const double areaBefore = compositeShape.getArea();
    const double areaRectangle = testRectangle.getArea();
    compositeShape.deleteShape(0);
    BOOST_CHECK_CLOSE(compositeShape.getArea(), areaBefore - areaRectangle, accuracy);
  }

  BOOST_AUTO_TEST_CASE(copyConstructorTesting)
  {
    andreeva::Rectangle testRectangle({0.0, 0.0}, 2.0, 5.0);
    andreeva::Circle testCircle({2.0, 2.0}, 1.0);
    andreeva::CompositeShape compositeShape1(std::make_shared<andreeva::Rectangle>(testRectangle));
    compositeShape1.addShape(std::make_shared<andreeva::Circle>(testCircle));
    const double area1 = compositeShape1.getArea();
    const andreeva::rectangle_t frame1 = compositeShape1.getFrameRect();
    andreeva::CompositeShape compositeShape2(compositeShape1);
    const double area2 = compositeShape2.getArea();
    const andreeva::rectangle_t frame2 = compositeShape2.getFrameRect();

    BOOST_CHECK_CLOSE(area1, area2, accuracy);
    BOOST_CHECK_CLOSE(frame1.width, frame2.width, accuracy);
    BOOST_CHECK_CLOSE(frame1.height, frame2.height, accuracy);
    BOOST_CHECK_CLOSE(frame1.pos.x, frame2.pos.x, accuracy);
    BOOST_CHECK_CLOSE(frame1.pos.y, frame2.pos.y, accuracy);
    BOOST_CHECK_EQUAL(compositeShape1.size(), compositeShape2.size());
  }

  BOOST_AUTO_TEST_CASE(moveConstrucorTesting)
  {
    andreeva::Rectangle testRectangle({0.0, 0.0}, 2.0, 5.0);
    andreeva::Circle testCircle({2.0, 2.0}, 1.0);
    andreeva::CompositeShape compositeShape1(std::make_shared<andreeva::Rectangle>(testRectangle));
    compositeShape1.addShape(std::make_shared<andreeva::Circle>(testCircle));

    const andreeva::rectangle_t frame1 = compositeShape1.getFrameRect();
    const double area1 = compositeShape1.getArea();
    const int size1 = compositeShape1.size();

    andreeva::CompositeShape compositeShape2(std::move(compositeShape1));
    const andreeva::rectangle_t frame2 = compositeShape2.getFrameRect();

    BOOST_CHECK_CLOSE(frame1.width, frame2.width, accuracy);
    BOOST_CHECK_CLOSE(frame1.height, frame2.height, accuracy);
    BOOST_CHECK_CLOSE(frame1.pos.x, frame2.pos.x, accuracy);
    BOOST_CHECK_CLOSE(frame1.pos.y, frame2.pos.y, accuracy);
    BOOST_CHECK_CLOSE(area1, compositeShape2.getArea(), accuracy);
    BOOST_CHECK_EQUAL(size1, compositeShape2.size());

    BOOST_CHECK_THROW(compositeShape1.getFrameRect(), std::logic_error);
    BOOST_CHECK_EQUAL(compositeShape1.size(), 0);
  }

  BOOST_AUTO_TEST_CASE(copyOperatorTesting)
  {
    andreeva::Rectangle testRectangle({0.0, 0.0}, 2.0, 5.0);
    andreeva::Circle testCircle({2.0, 2.0}, 1.0);
    andreeva::CompositeShape compositeShape1(std::make_shared<andreeva::Rectangle>(testRectangle));
    compositeShape1.addShape(std::make_shared<andreeva::Circle>(testCircle));
    const double area1 = compositeShape1.getArea();
    const andreeva::rectangle_t frame1 = compositeShape1.getFrameRect();
    andreeva::CompositeShape compositeShape2 = compositeShape1;
    const double area2 = compositeShape2.getArea();
    const andreeva::rectangle_t frame2 = compositeShape2.getFrameRect();

    BOOST_CHECK_CLOSE(area1, area2, accuracy);
    BOOST_CHECK_CLOSE(frame1.width, frame2.width, accuracy);
    BOOST_CHECK_CLOSE(frame1.height, frame2.height, accuracy);
    BOOST_CHECK_CLOSE(frame1.pos.x, frame2.pos.x, accuracy);
    BOOST_CHECK_CLOSE(frame1.pos.y, frame2.pos.y, accuracy);
    BOOST_CHECK_EQUAL(compositeShape1.size(), compositeShape2.size());
  }

  BOOST_AUTO_TEST_CASE(moveOperatorTesting)
  {
    andreeva::Rectangle testRectangle({0.0, 0.0}, 2.0, 5.0);
    andreeva::Circle testCircle({2.0, 2.0}, 1.0);
    andreeva::CompositeShape compositeShape1(std::make_shared<andreeva::Rectangle>(testRectangle));
    compositeShape1.addShape(std::make_shared<andreeva::Circle>(testCircle));

    const andreeva::rectangle_t frame1 = compositeShape1.getFrameRect();
    const double area1 = compositeShape1.getArea();
    const int size1 = compositeShape1.size();

    andreeva::CompositeShape compositeShape2 = std::move(compositeShape1);
    const andreeva::rectangle_t frame2 = compositeShape2.getFrameRect();

    BOOST_CHECK_CLOSE(frame1.width, frame2.width, accuracy);
    BOOST_CHECK_CLOSE(frame1.height, frame2.height, accuracy);
    BOOST_CHECK_CLOSE(frame1.pos.x, frame2.pos.x, accuracy);
    BOOST_CHECK_CLOSE(frame1.pos.y, frame2.pos.y, accuracy);
    BOOST_CHECK_CLOSE(area1, compositeShape2.getArea(), accuracy);
    BOOST_CHECK_EQUAL(size1, compositeShape2.size());

    BOOST_CHECK_THROW(compositeShape1.getFrameRect(), std::logic_error);
    BOOST_CHECK_EQUAL(compositeShape1.size(), 0);
  }

  BOOST_AUTO_TEST_CASE(operatorTest)
  {
    andreeva::Rectangle testRectangle({2.0, 2.0}, 3.0, 3.0);
    andreeva::CompositeShape compositeShape(std::make_shared<andreeva::Rectangle>(testRectangle));
    const andreeva::rectangle_t testFrameRect = testRectangle.getFrameRect();
    const double areaOfShape = testRectangle.getArea();

    BOOST_CHECK_CLOSE(testFrameRect.width, compositeShape[0]->getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(testFrameRect.height, compositeShape[0]->getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(areaOfShape, compositeShape[0]->getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(compositeShapeRotation)
  {
    andreeva::Rectangle testRectangle({1.0, 1.0}, 2.0, 2.0);
    andreeva::Circle testCircle({2.0, 2.0}, 2.0);
    andreeva::CompositeShape testShapes(std::make_shared<andreeva::Rectangle>(testRectangle));
    testShapes.addShape(std::make_shared<andreeva::Circle>(testCircle));
    double areaBefore = testShapes.getArea();

    double angle = -99.8;
    BOOST_CHECK_NO_THROW(testShapes.rotate(angle));

    angle = 75.7;
    BOOST_CHECK_NO_THROW(testShapes.rotate(angle));

    angle = -30.5;
    testShapes.rotate(angle);
    BOOST_CHECK_CLOSE(testShapes.getArea(), areaBefore, accuracy);

    angle = 13.03;
    testShapes.rotate(angle);
    BOOST_CHECK_CLOSE(testShapes.getArea(), areaBefore, accuracy);

    angle = 363.0;
    testShapes.rotate(angle);
    BOOST_CHECK_CLOSE(testShapes.getArea(), areaBefore, accuracy);

    angle = -1101.1;
    testShapes.rotate(angle);
    BOOST_CHECK_CLOSE(testShapes.getArea(),areaBefore, accuracy);
  }

BOOST_AUTO_TEST_SUITE_END()
