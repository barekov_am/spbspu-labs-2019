#ifndef A4_MATRIX_HPP
#define A4_MATRIX_HPP

#include <memory>
#include "shape.hpp"

namespace andreeva
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &copyMatrix);
    Matrix(Matrix &&displaceMatrix);
    ~Matrix() = default;

    Matrix& operator =(const Matrix &someMatrix);
    Matrix& operator =(Matrix &&someMatrix);

    andreeva::Shape::array_ptr operator [](size_t index) const;
    bool operator ==(const Matrix& someMatrix) const;
    bool operator !=(const Matrix& someMatrix) const;

    void add(andreeva::Shape::ptr_shape shape, size_t row, size_t column);

    size_t getColumns() const;
    size_t getRows() const;

  private:
    size_t rows_;
    size_t columns_;
    andreeva::Shape::array_ptr matrixList_;
  };
}

#endif
