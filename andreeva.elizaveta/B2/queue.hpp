#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <array>
#include <deque>

namespace andreeva
{
  namespace details
  {
    enum class ElementPriority
    {
      LOW = 0,
      NORMAL = 1,
      HIGH = 2
    };

    template <typename T>
    class QueueWithPriority
    {
    public:

      QueueWithPriority();

      QueueWithPriority(const T& element, ElementPriority priority, size_t num = 1);

      ~QueueWithPriority() = default;

      void putElement(const T& element, ElementPriority priority);

      T getElement();

      bool isEmpty() const;

      void accelerate();

    private:
      static const size_t PRIORITY_NUMBER = 3;
      typename std::array<std::deque<T>, PRIORITY_NUMBER> queue_;
    };
  }

  template<typename T>
  details::QueueWithPriority<T>::QueueWithPriority()
  {
  }

  template<typename T>
  details::QueueWithPriority<T>::QueueWithPriority(const T & element, ElementPriority priority, size_t num)
  {
    for (int i = 0; i < num; i++)
    {
      queue_[static_cast<int>(priority)].push_back(element);
    }
  }

  template<typename T>
  void details::QueueWithPriority<T>::putElement(const T & element, ElementPriority priority)
  {
    queue_[static_cast<int>(priority)].push_back(element);
  }

  template<typename T>
  T details::QueueWithPriority<T>::getElement()
  {
    if (isEmpty())
    {
      throw std::out_of_range("Error! Trying to get element from empty queue.");
    }
    ElementPriority priority = ElementPriority::LOW;
    if (!queue_[static_cast<int>(ElementPriority::HIGH)].empty())
    {
      priority = ElementPriority::HIGH;
    }
    else if (!queue_[static_cast<int>(ElementPriority::NORMAL)].empty())
    {
      priority = ElementPriority::NORMAL;
    }
    else if (!queue_[static_cast<int>(ElementPriority::LOW)].empty())
    {
      priority = ElementPriority::LOW;
    }
    T elem = queue_[static_cast<int>(priority)].front();
    queue_[static_cast<int>(priority)].pop_front();
    return elem;
  }

  template<typename T>
  bool details::QueueWithPriority<T>::isEmpty() const
  {
    return queue_[static_cast<int>(ElementPriority::HIGH)].empty()
      && queue_[static_cast<int>(ElementPriority::NORMAL)].empty()
      && queue_[static_cast<int>(ElementPriority::LOW)].empty();
  }

  template<typename T>
  void details::QueueWithPriority<T>::accelerate()
  {
    while (!queue_[static_cast<int>(ElementPriority::LOW)].empty())
    {
      queue_[static_cast<int>(ElementPriority::HIGH)].push_back(
        queue_[static_cast<int>(ElementPriority::LOW)].front()
      );
      queue_[static_cast<int>(ElementPriority::LOW)].pop_front();
    }
  }
}

#endif
