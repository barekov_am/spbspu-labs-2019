#include <list>

#include "tasks.hpp"

void andreeva::tasks::task1()
{
  andreeva::details::QueueWithPriority<std::string> queue;
  std::string line;

  while (std::getline(std::cin >> std::ws, line))
  {
    details::command_function<std::string> com = details::parseCommand(line);
    com(queue);
  }
}

void andreeva::tasks::task2()
{
  std::list<int> list;

  int num = 0;
  while ((std::cin >> num) && list.size() <= constants::MAX_SIZE)
  {
    if (num < constants::MIN_VALUE || num > constants::MAX_VALUE)
    {
      throw std::invalid_argument("Error! Number must be in range [1; 20].");
    }
    list.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios::failure("Error! Input stream failed.");
  }

  if (list.size() > constants::MAX_SIZE)
  {
    throw std::invalid_argument("Error! Number of elements is bigger than 20.");
  }
  details::printIterators(list.begin(), list.end());
  std::cout << std::endl;
}
