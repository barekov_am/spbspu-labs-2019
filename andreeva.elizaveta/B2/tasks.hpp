#ifndef TASKS_HPP
#define TASKS_HPP

#include <iostream>

#include "command_parser.hpp"
#include "queue.hpp"

namespace andreeva
{
  namespace tasks
  {
    void task1();
    void task2();
  }

  namespace constants
  {
    const int MAX_SIZE = 20;
    const int MAX_VALUE = 20;
    const int MIN_VALUE = 1;
  }

  namespace details
  {
    template <typename Iterator>
    void printIterators(Iterator front, Iterator back)
    {
      if (front == back)
      {
        return;
      }
      back = std::prev(back);
      if (front == back)
      {
        std::cout << *front;
      }
      else
      {
        std::cout << *front << " " << *back << " ";
        if (std::next(front) != back)
        {
          printIterators(std::next(front), back);
        }
      }
    }
  }
}

#endif

