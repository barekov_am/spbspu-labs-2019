#ifndef COMMAND_PARSER_HPP
#define COMMAND_PARSER_HPP

#include <string>
#include <functional>

#include "queue.hpp"

namespace andreeva
{
  namespace constants
  {
    const std::string INVALID_COM = "<INVALID COMMAND>";
    const std::string EMPTY = "<EMPTY>";

    const std::string ADD = "add";
    const std::string GET = "get";
    const std::string ACCEL = "accelerate";

    const std::string HIGH = "high";
    const std::string NORM = "normal";
    const std::string LOW = "low";
  }

  namespace details
  {
    template <typename T>
    using command_function = std::function<void(QueueWithPriority<T>&)>;

    command_function<std::string> parseCommand(const std::string& input);
  }
}

#endif
