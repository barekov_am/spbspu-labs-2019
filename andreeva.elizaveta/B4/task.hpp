#ifndef TASK_HPP
#define TASK_HPP

#include <string>
#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>

namespace andreeva
{
  struct DataStruct
  {
    int key1;
    int key2;
    std::string str;
  };

  void task();

  namespace details
  {
    const int COMMAS_NUMBER = 2;
    const int MIN_VAL = -5;
    const int MAX_VAL = 5;
  }
}

#endif
