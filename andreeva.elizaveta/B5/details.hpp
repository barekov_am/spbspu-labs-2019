#ifndef DETAILS_HPP
#define DETAILS_HPP
#include <vector>
#include <list>

namespace details
{
  struct Point
  {
    int x, y;
  };

  using Shape = std::vector< Point >;
  using ShapesContainer = std::list<Shape>;

  const int PENTAGON_VERT_NUM = 5;
  const int TRIANGLE_VERT_NUM = 3;

}

#endif
