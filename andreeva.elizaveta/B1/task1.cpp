#include "tasks.hpp"

void andreeva::tasks::itemOne(const char* sortParameter, std::istream &istream)
{
  int spaceSwitch = 1;

  std::vector<int> vecInt, vecIntPrimary;

  int number;
  while (istream >> number)
  {
    vecInt.insert(vecInt.end(), number);
  }
  if (!istream.eof())
  {
    throw std::ios_base::failure("Wrong characters.");
  }

  vecIntPrimary = vecInt;

  if (!vecInt.empty())
  {
    if (sortParameter == andreeva::details::ASCENDING)
    {
      andreeva::details::sortVecT(vecInt, andreeva::details::getByBrackets, true);
    }
    else if (sortParameter == andreeva::details::DESCENDING)
    {
      andreeva::details::sortVecT(vecInt, andreeva::details::getByBrackets, false);
    }
    else
    {
      throw std::invalid_argument("Wrong sorting parameter.");
    }

    andreeva::details::VectorPrinter<int>(vecInt, spaceSwitch);
    std::cout << std::endl;

    vecInt = vecIntPrimary;

    if (sortParameter == andreeva::details::ASCENDING)
    {
      andreeva::details::sortVecT(vecInt, andreeva::details::getByAt, true);
    }
    else if (sortParameter == andreeva::details::DESCENDING)
    {
      andreeva::details::sortVecT(vecInt, andreeva::details::getByAt, false);
    }
    else
    {
      throw std::invalid_argument("Wrong sorting parameter.");
    }

    andreeva::details::VectorPrinter<int>(vecInt, spaceSwitch);
    std::cout << std::endl;

    vecInt = vecIntPrimary;

    std::vector<int>::iterator it1, it2;

    if (sortParameter == andreeva::details::ASCENDING)
    {
      andreeva::details::sortVecT(vecInt, andreeva::details::getByIterator, true);
    }
    else if (sortParameter == andreeva::details::DESCENDING)
    {
      andreeva::details::sortVecT(vecInt, andreeva::details::getByIterator, false);
    }
    else
    {
      throw std::invalid_argument("Wrong sorting parameter.");
    }

    andreeva::details::VectorPrinter<int>(vecInt, spaceSwitch);
  }
  else
  {
    if ((sortParameter == andreeva::details::ASCENDING) || (sortParameter == andreeva::details::DESCENDING))
    {
      return;
    }
    else
    {
      throw std::invalid_argument("Wrong sorting parameter.");
    }
  }
}
