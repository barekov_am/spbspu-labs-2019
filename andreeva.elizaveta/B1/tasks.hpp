#ifndef TASKS_HPP
#define TASKS_HPP

#include "utils.hpp"

namespace andreeva
{
  namespace tasks
  {
    void itemOne(const char* sortParameter, std::istream &istream);

    void itemTwo(char* fileName);

    void itemThree(std::istream &istream);

    void itemFour(const char* sortParameter, const char* sizeChar);
  }
}
#endif
