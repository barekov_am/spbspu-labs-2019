#include "tasks.hpp"
#include <string>

void andreeva::tasks::itemFour(const char* sortParameter, const char* sizeChar)
{
  int spaceSwitch = 1;

  std::vector<double> vecDouble;

  unsigned int vecSize(0);
  try
  {
    vecSize = std::stoi(sizeChar);
  }
  catch (...)
  {
    throw std::invalid_argument("Wrong length.");
  }

  if ((sortParameter != andreeva::details::ASCENDING) && (sortParameter != andreeva::details::DESCENDING))
  {
    throw std::invalid_argument("Wrong sorting parameter.");
  }

  andreeva::details::fillRandom_ForVector(vecDouble, vecSize);

  andreeva::details::VectorPrinter(vecDouble, spaceSwitch);
  std::cout << std::endl;

  if (sortParameter == andreeva::details::ASCENDING)
  {
    andreeva::details::sortVecT(vecDouble, andreeva::details::getByIterator, true);
  }
  else if (sortParameter == andreeva::details::DESCENDING)
  {
    andreeva::details::sortVecT(vecDouble, andreeva::details::getByIterator, false);
  }

  andreeva::details::VectorPrinter(vecDouble, spaceSwitch);
}
