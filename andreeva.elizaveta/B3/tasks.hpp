#ifndef TASKS_HPP
#define TASKS_HPP

#include "command_handler.hpp"
#include "factorial.hpp"

namespace andreeva
{
  namespace tasks
  {
    void taskOne();
    void taskTwo();
  }
}
#endif
