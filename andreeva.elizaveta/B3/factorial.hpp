#ifndef FACTORIAL_HPP
#define FACTORIAL_HPP

#include <iterator>

namespace andreeva
{
  namespace details
  {
    class Factorial
    {
    public:
      class FactIterator : public std::iterator<std::bidirectional_iterator_tag, size_t>
      {
      public:
        static const int MAX_POSITION = 11;
        static const int MIN_POSITION = 1;

        FactIterator();
        FactIterator(size_t pos);

        const size_t* operator->() const;
        const size_t& operator*() const;

        FactIterator& operator ++();
        FactIterator operator++(int);
        FactIterator& operator--();
        FactIterator operator--(int);

        bool operator==(const FactIterator& rhs) const;
        bool operator!=(const FactIterator& rhs) const;

      private:
        size_t position_, value_;

        inline size_t factor(size_t number);
      };

      Factorial() = default;
      FactIterator begin();
      FactIterator end();
    };
  }
}
#endif
