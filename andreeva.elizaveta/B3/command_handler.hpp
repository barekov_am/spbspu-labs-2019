#ifndef COMAND_HANDLER_HPP
#define COMAND_HANDLER_HPP

#include <string>
#include <functional>

#include "phone_book.hpp"

namespace andreeva
{
  namespace constants
  {
    const std::string ADD = "add";
    const std::string STORE = "store";
    const std::string INSERT = "insert";
    const std::string DELETE = "delete";
    const std::string SHOW = "show";
    const std::string MOVE = "move";

    const std::string BEFORE = "before";
    const std::string AFTER = "after";
  }
  namespace details
  {
    using command_function = std::function<void(PhoneBook &book)>;
    command_function handleCommand(const std::string &input);
  }
}
#endif

