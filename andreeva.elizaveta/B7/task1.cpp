#include <iterator>
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <cmath>

#include "tasks.hpp"

void tasks::taskOne()
{
  std::vector<double> inputData((std::istream_iterator<double>(std::cin)), std::istream_iterator<double>());

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios::failure("Error! Input stream failed!");
  }

  std::transform(inputData.begin(), inputData.end(), inputData.begin(),
    std::bind1st(std::multiplies<double>(), M_PI));
  std::copy(inputData.begin(), inputData.end(), std::ostream_iterator<double>(std::cout, " "));
}
