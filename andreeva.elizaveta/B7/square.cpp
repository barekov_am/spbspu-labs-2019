#include "square.hpp"

andreeva::Square::Square(int x, int y) :
  Shape(x, y)
{
}

void andreeva::Square::draw(std::ostream& stream) const
{
  stream << "SQUARE (" << getX() << ";" << getY() << ")" << std::endl;
}
