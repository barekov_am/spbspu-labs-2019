#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include <iostream>

namespace andreeva
{
  class Shape
  {
  public:

    Shape(int x, int y) noexcept;
    virtual ~Shape() = default;

    bool isMoreLeft(const std::shared_ptr<Shape>& other) const noexcept;
    bool isUpper(const std::shared_ptr<Shape>& other) const noexcept;

    virtual void draw(std::ostream& stream) const = 0;

    double getX() const noexcept;
    double getY() const noexcept;

  private:
    int x_;
    int y_;
  };
}

#endif
