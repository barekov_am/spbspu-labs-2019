#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace andreeva
{
  class Circle : public Shape
  {
  public:
    Circle(int x, int y);
    virtual void draw(std::ostream& stream) const override;
  };
}

#endif
