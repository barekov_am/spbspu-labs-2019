#include "shape.hpp"

andreeva::Shape::Shape(int x, int y) noexcept :
  x_(x),
  y_(y)
{
}

bool andreeva::Shape::isMoreLeft(const std::shared_ptr<Shape>& other) const noexcept
{
  return this->x_ < other->x_;
}

bool andreeva::Shape::isUpper(const std::shared_ptr<Shape>& other) const noexcept
{
  return this->y_ > other->y_;
}

double andreeva::Shape::getX() const noexcept
{
  return x_;
}

double andreeva::Shape::getY() const noexcept
{
  return y_;
}
