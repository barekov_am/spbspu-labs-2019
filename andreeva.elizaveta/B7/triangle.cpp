#include "triangle.hpp"

andreeva::Triangle::Triangle(int x, int y) :
  Shape(x, y)
{
}

void andreeva::Triangle::draw(std::ostream& stream) const
{
  stream << "TRIANGLE (" << getX() << ";" << getY() << ")" << std::endl;
}
