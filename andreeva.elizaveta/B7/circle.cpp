#include "circle.hpp"

andreeva::Circle::Circle(int x, int y) :
  Shape(x, y)
{
}

void andreeva::Circle::draw(std::ostream& stream) const
{
  stream << "CIRCLE (" << getX() << ";" << getY() << ")" << std::endl;
}
