#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "separation.hpp"
#include "matrix.hpp"

int main()
{
  andreeva::Circle circle({1.0, 1.0}, 5.0);
  andreeva::Shape *shapePtr = &circle;
  std::cout << "Circle area " << shapePtr->getArea() << '\n';
  shapePtr->scale(4.0);
  std::cout << "Circle area " << shapePtr->getArea() << '\n';
  andreeva::rectangle_t frameRect = shapePtr->getFrameRect();
  std::cout << "Frame Rectangle X " << frameRect.pos.x << '\n';
  std::cout << "Frame Rectangle Y " << frameRect.pos.y << '\n';
  std::cout << "Frame Rectangle Height " << frameRect.height << '\n';
  std::cout << "Frame Rectangle Width " << frameRect.width << '\n';
  shapePtr->move(1.0, 3.5);
  frameRect = shapePtr->getFrameRect();
  std::cout << "Frame Rectangle X after moving " << frameRect.pos.x << '\n';
  std::cout << "Frame Rectangle Y after moving " << frameRect.pos.y << '\n';
  shapePtr->rotate(256);
  frameRect = shapePtr->getFrameRect();
  std::cout << "Center after rotating: \n";
  std::cout << "X = " << frameRect.pos.x << " Y = " << frameRect.pos.y << '\n';

  andreeva::Rectangle rectangle({2.0, 2.0}, 5.0, 6.0);
  shapePtr = &rectangle;
  std::cout << "Rectangle area " << shapePtr->getArea() << '\n';
  shapePtr->scale(5.0);
  std::cout << "Rectangle area " << shapePtr->getArea() << '\n';
  frameRect = shapePtr->getFrameRect();
  std::cout << "Frame Rectangle X " << frameRect.pos.x << '\n';
  std::cout << "Frame Rectangle Y " << frameRect.pos.y << '\n';
  std::cout << "Frame Rectangle Width " << frameRect.width << '\n';
  std::cout << "Frame Rectangle Height " << frameRect.height << '\n';
  shapePtr->move({3.0, 4.0});
  frameRect = shapePtr->getFrameRect();
  std::cout << "Frame Rectangle X after moving " << frameRect.pos.x << '\n';
  std::cout << "Frame Rectangle Y after moving " << frameRect.pos.y << '\n';
  shapePtr->rotate(-1256.8);
  frameRect = shapePtr->getFrameRect();
  std::cout << "Center after rotating: \n";
  std::cout << "X = " << frameRect.pos.x << " Y = " << frameRect.pos.y << '\n';

  andreeva::CompositeShape compositeShape(std::make_shared<andreeva::Rectangle>(rectangle));
  compositeShape.addShape(std::make_shared<andreeva::Circle>(circle));
  shapePtr = &compositeShape;
  std::cout << "Composite Shape before changes:" << std::endl;
  std::cout << "Area " << shapePtr->getArea() << '\n';
  frameRect = shapePtr->getFrameRect();
  std::cout << "Frame Rectangle X " << frameRect.pos.x << '\n';
  std::cout << "Frame Rectangle Y " << frameRect.pos.y << '\n';
  std::cout << "Frame Rectangle Width " << frameRect.width << '\n';
  std::cout << "Frame Rectangle Height " << frameRect.height << '\n';
  shapePtr->move(2.0, 2.0);
  shapePtr->move({0.0, 0.0});
  shapePtr->scale(2.0);
  std::cout << "Composite Shape after changes:" << std::endl;
  std::cout << "Area " << shapePtr->getArea() << '\n';
  frameRect = shapePtr->getFrameRect();
  std::cout << "Frame Rectangle X " << frameRect.pos.x << '\n';
  std::cout << "Frame Rectangle Y " << frameRect.pos.y << '\n';
  std::cout << "Frame Rectangle Width " << frameRect.width << '\n';
  std::cout << "Frame Rectangle Height " << frameRect.height << '\n';
  shapePtr->rotate(555);
  frameRect = shapePtr->getFrameRect();
  std::cout << "Center after rotating: \n";
  std::cout << "X = " << frameRect.pos.x << " Y = " << frameRect.pos.y << '\n';
  std::cout << "Separation of the matrix into layers \n";

  andreeva::Matrix matrix = andreeva::layer(compositeShape);
  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    for (size_t j = 0; j < matrix.getColumns(); j++)
    {
      if (matrix[i][j] != nullptr)
      {
        std::cout << "Layer: " << i << ", " << "Figure: " << j << "\n";
      }
    }
  }

  compositeShape.deleteShape(1);
  std::cout << "Composite Shape after removing the circle:" << std::endl;
  std::cout << "Area " << compositeShape.getArea() << '\n';
  frameRect = compositeShape.getFrameRect();
  std::cout << "Frame Rectangle X " << frameRect.pos.x << '\n';
  std::cout << "Frame Rectangle Y " << frameRect.pos.y << '\n';
  std::cout << "Frame Rectangle Width " << frameRect.width << '\n';
  std::cout << "Frame Rectangle Height " << frameRect.height << '\n';

  return 0;
}
