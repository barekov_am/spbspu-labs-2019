#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
  std::cout << "___TESTING RECTANGLE___ \n";
  taran::Rectangle rectangle({0, 0}, 20, 10);
  rectangle.printFeatures();

  rectangle.move(10, 20);
  std::cout << "Rectangle has been moved by dx = 10 and dy = 20 \n";
  rectangle.printFeatures();

  rectangle.move({10, 20});
  std::cout << "Rectangle has been moved to a point with coordinates {10, 20} \n";
  rectangle.printFeatures();

  rectangle.scale(4);
  std::cout << "Rectangle has been scaled \n\n";
  rectangle.printFeatures();

  rectangle.rotate(60);
  std::cout << "Rectangle has been rotated by 60 degree\n";
  rectangle.printFeatures();

  std::cout << "___TESTING CIRCLE___ \n";
  taran::Circle circle({0, 0}, 10);
  circle.printFeatures();

  circle.move(5, 10);
  std::cout << "Circle has been moved by dx = 5 and dy = 10 \n";
  circle.printFeatures();

  circle.move({5, 10});
  std::cout << "Circle has been moved to a point with coordinates {5, 10} \n";
  circle.printFeatures();

  circle.scale(3);
  std::cout << "Circle has been scaled\n\n";
  circle.printFeatures();

  circle.rotate(60);
  std::cout << "Circle has been rotated by 60 degree\n";
  circle.printFeatures();

  std::cout << "____TESTING TRIANGLE___ \n";
  taran::Triangle triangle({0, 0}, {10, 10}, {20, -10});
  triangle.printFeatures();

  triangle.move(15, 30);
  std::cout << "Triangle has been moved by dx = 15 and dy = 30 \n";
  triangle.printFeatures();

  triangle.move({15, 30});
  std::cout << "Triangle has been moved to a point with coordinates {15, 30} \n";
  triangle.printFeatures();

  triangle.scale(2);
  std::cout << "Triangle has been scaled \n";
  triangle.printFeatures();

  triangle.rotate(60);
  std::cout << "Triangle has been rotated by 60 degree\n";
  triangle.printFeatures();

  std::cout << "___TESTING COMPOSITE SHAPE___ \n";
  taran::shape_ptr rectanglePtr = std::make_shared<taran::Rectangle>(rectangle);
  taran::shape_ptr circlePtr = std::make_shared<taran::Circle>(circle);

  taran::CompositeShape compositeShape(rectanglePtr);
  compositeShape.printFeatures();

  compositeShape.add(rectanglePtr);
  compositeShape.add(circlePtr);
  std::cout << "New figures have been added: \n";
  compositeShape.printFeatures();

  compositeShape.move(4.5, 1.5);
  std::cout << "Composite shape has been moved by dx = 4.5 and dy = 1.5 \n";
  compositeShape.printFeatures();

  compositeShape.move({12.0, 4});
  std::cout << "Composite shape has been moved to a point with coordinates {12.0, 4} \n";
  compositeShape.printFeatures();

  compositeShape.scale(4);
  std::cout << "Composite shape has been scaled with the coefficient 4 \n";
  compositeShape.printFeatures();

  compositeShape.remove(0);
  std::cout << "First figure in composite shape has been deleted: \n";
  compositeShape.printFeatures();

  compositeShape.rotate(60);
  std::cout << "Composite shape has been rotated by 60 degree\n";
  compositeShape.printFeatures();

  return 0;
}
