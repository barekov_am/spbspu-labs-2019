#include "circle.hpp"

#include <cmath>
#include <iostream>

taran::Circle::Circle(const point_t & pos, double radius) :
  pos_(pos),
  radius_(radius)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("Circle's radius should be positive");
  }
}

double taran::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

taran::rectangle_t taran::Circle::getFrameRect() const
{
  return rectangle_t{pos_, 2 * radius_, 2 * radius_};
}

void taran::Circle::printFeatures() const
{
  rectangle_t framingRectangle = getFrameRect();
  std::cout << "\n Circle frame width is " << framingRectangle.width;
  std::cout << "\n Circle frame height is " << framingRectangle.height;
  std::cout << "\n Circle position is ( " << "x: " << pos_.x << ", y: " << pos_.y << " )";
  std::cout << "\n Circle area is " << getArea() << "\n\n";
}

void taran::Circle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void taran::Circle::move(const point_t & pos)
{
  pos_ = pos;
}

void taran::Circle::scale(double times)
{
  if (times <= 0)
  {
    throw std::invalid_argument("Enter times correctly");
  }
  radius_ *= times;
}

void taran::Circle::rotate(double)
{
}
