#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

const double EPSILON = 0.01;

BOOST_AUTO_TEST_SUITE(compositeTests)

BOOST_AUTO_TEST_CASE(compositeShapeCopyConstructor)
{
  taran::shape_ptr rectPtr = std::make_shared<taran::Rectangle>(taran::point_t {4, 7}, 2, 12);
  taran::shape_ptr circPtr = std::make_shared<taran::Circle>(taran::point_t {2, 1}, 10);
  taran::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);
  const taran::rectangle_t frameRect = compSh.getFrameRect();

  taran::CompositeShape copyCompSh(compSh);
  const taran::rectangle_t copyFrameRect = copyCompSh.getFrameRect();
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(compSh.getArea(), copyCompSh.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(compSh.getSize(), copyCompSh.getSize());
}

BOOST_AUTO_TEST_CASE(compositeShapeCopyOperator)
{
  taran::shape_ptr rectPtr = std::make_shared<taran::Rectangle>(taran::point_t {4, 7}, 2, 12);
  taran::shape_ptr circPtr = std::make_shared<taran::Circle>(taran::point_t {2, 1}, 10);
  taran::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);
  const taran::rectangle_t frameRect = compSh.getFrameRect();

  taran::CompositeShape copyCompSh(rectPtr);
  copyCompSh = compSh;
  const taran::rectangle_t copyFrameRect = copyCompSh.getFrameRect();
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(compSh.getArea(), copyCompSh.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(compSh.getSize(), copyCompSh.getSize());
}

BOOST_AUTO_TEST_CASE(compositeShapeMoveConstructor)
{
  taran::shape_ptr rectPtr = std::make_shared<taran::Rectangle>(taran::point_t {4, 7}, 2, 12);
  taran::shape_ptr circPtr = std::make_shared<taran::Circle>(taran::point_t {2, 1}, 10);
  taran::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);
  const taran::rectangle_t frameRect = compSh.getFrameRect();
  const double compShArea = compSh.getArea();
  const int compShCount = compSh.getSize();

  taran::CompositeShape moveCompSh(std::move(compSh));
  const taran::rectangle_t moveFrameRect = moveCompSh.getFrameRect();

  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(compShArea, moveCompSh.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(compShCount, moveCompSh.getSize());
  BOOST_CHECK_CLOSE(compSh.getArea(), 0, EPSILON);
  BOOST_CHECK_EQUAL(compSh.getSize(), 0);
}

BOOST_AUTO_TEST_CASE(compositeShapeMoveOperator)
{
  taran::shape_ptr rectPtr = std::make_shared<taran::Rectangle>(taran::point_t {4, 7}, 2, 12);
  taran::shape_ptr circPtr = std::make_shared<taran::Circle>(taran::point_t {2, 1}, 10);
  taran::CompositeShape movedCompSh(rectPtr);
  movedCompSh.add(circPtr);
  const taran::rectangle_t frameRect = movedCompSh.getFrameRect();
  const double compShArea = movedCompSh.getArea();
  const int compShCount = movedCompSh.getSize();

  taran::CompositeShape compSh(rectPtr);
  compSh = std::move(movedCompSh);
  const taran::rectangle_t moveFrameRect = compSh.getFrameRect();
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(compShArea, compSh.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(compShCount, compSh.getSize());
  BOOST_CHECK_EQUAL(movedCompSh.getSize(), 0);
  BOOST_CHECK_CLOSE(movedCompSh.getArea(), 0, EPSILON);
}

BOOST_AUTO_TEST_CASE(compositeShapeConstancyOfParameters)
{
  taran::shape_ptr rectPtr = std::make_shared<taran::Rectangle>(taran::point_t {4, 7}, 2, 12);
  taran::shape_ptr circPtr = std::make_shared<taran::Circle>(taran::point_t {2, 1}, 10);
  taran::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);
  const double areaBeforeMoving = compSh.getArea();
  const taran::rectangle_t frameRectBeforeMoving = compSh.getFrameRect();
  compSh.move(3, 5);
  taran::rectangle_t frameRectAfterMoving = compSh.getFrameRect();
  double areaAfterMoving = compSh.getArea();

  BOOST_CHECK_CLOSE(areaAfterMoving, areaBeforeMoving, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.height, frameRectAfterMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.width, frameRectAfterMoving.width, EPSILON);

  compSh.move({3, 4});
  frameRectAfterMoving = compSh.getFrameRect();
  areaAfterMoving = compSh.getArea();

  BOOST_CHECK_CLOSE(areaAfterMoving, areaBeforeMoving, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.height, frameRectAfterMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.width, frameRectAfterMoving.width, EPSILON);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleCoefficientMoreThanOne)
{
  taran::shape_ptr rectPtr = std::make_shared<taran::Rectangle>(taran::point_t {4, 7}, 2, 12);
  taran::shape_ptr circPtr = std::make_shared<taran::Circle>(taran::point_t {2, 1}, 10);
  taran::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);
  const taran::rectangle_t frameBeforeScale = compSh.getFrameRect();
  const int coefMoreThanOne = 3;
  compSh.scale(coefMoreThanOne);
  taran::rectangle_t frameAfterScale = compSh.getFrameRect();

  BOOST_CHECK_CLOSE(frameBeforeScale.height * coefMoreThanOne, frameAfterScale.height, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeScale.width * coefMoreThanOne, frameAfterScale.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.x, frameAfterScale.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.y, frameAfterScale.pos.y, EPSILON);
  BOOST_CHECK(frameBeforeScale.height < frameAfterScale.height);
  BOOST_CHECK(frameBeforeScale.width < frameAfterScale.width);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleCoefficientLessThanOne)
{
  taran::shape_ptr rectPtr = std::make_shared<taran::Rectangle>(taran::point_t {4, 7}, 2, 12);
  taran::shape_ptr circPtr = std::make_shared<taran::Circle>(taran::point_t {2, 1}, 10);
  taran::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);
  const taran::rectangle_t frameBeforeScale = compSh.getFrameRect();
  const double coefLessThanOne = 0.3;
  compSh.scale(coefLessThanOne);
  taran::rectangle_t frameAfterScale = compSh.getFrameRect();

  BOOST_CHECK_CLOSE(frameBeforeScale.height * coefLessThanOne, frameAfterScale.height, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeScale.width * coefLessThanOne, frameAfterScale.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.x, frameAfterScale.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.y, frameAfterScale.pos.y, EPSILON);
  BOOST_CHECK(frameBeforeScale.width > frameAfterScale.width);
  BOOST_CHECK(frameBeforeScale.height > frameAfterScale.height);
}

BOOST_AUTO_TEST_CASE(compositeShapeIncorrectScaleParameter)
{
  taran::shape_ptr rectPtr = std::make_shared<taran::Rectangle>(taran::point_t {4, 7}, 2, 12);
  taran::shape_ptr circPtr = std::make_shared<taran::Circle>(taran::point_t {2, 1}, 10);
  taran::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);

  BOOST_CHECK_THROW(compSh.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShapeAreaAfterAddAndDelete)
{
  taran::Rectangle rect({4, 7}, 2, 12);
  taran::shape_ptr rectPtr = std::make_shared<taran::Rectangle>(rect);
  taran::Circle circ({2, 1}, 10);
  taran::shape_ptr circPtr = std::make_shared<taran::Circle>(circ);
  taran::CompositeShape compSh(rectPtr);
  const double compShAreaBeforeAdd = compSh.getArea();
  const double rectArea = rect.getArea();
  const double circArea = circ.getArea();

  compSh.add(circPtr);
  double compShAreaAfterAdd = compSh.getArea();
  BOOST_CHECK_CLOSE(compShAreaBeforeAdd + circArea, compShAreaAfterAdd, EPSILON);

  compSh.remove(0);
  BOOST_CHECK_CLOSE(compShAreaAfterAdd - rectArea, compSh.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(compositeShapeAfterRotating)
{
  taran::shape_ptr rectPtr = std::make_shared<taran::Rectangle>(taran::point_t {-1, 0}, 2, 2);
  taran::shape_ptr circPtr = std::make_shared<taran::Circle>(taran::point_t {1, 0}, 1);
  taran::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);

  const double compShAreaBeforeRotating = compSh.getArea();
  const taran::rectangle_t frameBeforeRotating = compSh.getFrameRect();

  compSh.rotate(90);
  double compShAreaAfterRotating = compSh.getArea();
  taran::rectangle_t frameAfterRotating = compSh.getFrameRect();

  BOOST_CHECK_CLOSE(compShAreaBeforeRotating, compShAreaAfterRotating, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.x, frameAfterRotating.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.y, frameAfterRotating.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.width, frameAfterRotating.height, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.height, frameAfterRotating.width, EPSILON);

  compSh.rotate(-90);
  compShAreaAfterRotating = compSh.getArea();
  frameAfterRotating = compSh.getFrameRect();

  BOOST_CHECK_CLOSE(compShAreaBeforeRotating, compShAreaAfterRotating, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.x, frameAfterRotating.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.y, frameAfterRotating.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.width, frameAfterRotating.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeRotating.height, frameAfterRotating.height, EPSILON);

  compSh.rotate(45);
  compShAreaAfterRotating = compSh.getArea();

  BOOST_CHECK_CLOSE(compShAreaBeforeRotating, compShAreaAfterRotating, EPSILON);
  BOOST_CHECK_CLOSE(compSh.getFigures()[0]->getFrameRect().pos.x, compSh.getFigures()[0]->getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(compSh.getFigures()[1]->getFrameRect().pos.x, compSh.getFigures()[1]->getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(compSh.getFigures()[0]->getFrameRect().pos.x, - compSh.getFigures()[1]->getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(compSh.getFigures()[0]->getFrameRect().pos.y, - compSh.getFigures()[1]->getFrameRect().pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(compositeShapeThrowingExeptions)
{
  taran::shape_ptr rectPtr = std::make_shared<taran::Rectangle>(taran::point_t {4, 7}, 2, 12);
  taran::shape_ptr circPtr = std::make_shared<taran::Circle>(taran::point_t {2, 1}, 10);
  taran::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);

  BOOST_CHECK_THROW(compSh.remove(10), std::invalid_argument);
  BOOST_CHECK_THROW(compSh.remove(-10), std::invalid_argument);

  compSh.remove(1);
  BOOST_CHECK_THROW(compSh[1], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeTestThrowExceptionAfterUsingOfOperator)
{
  taran::shape_ptr rectPtr = std::make_shared<taran::Rectangle>(taran::point_t {4, 7}, 2, 12);
  taran::shape_ptr circPtr = std::make_shared<taran::Circle>(taran::point_t {2, 1}, 10);
  taran::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);

  BOOST_CHECK_THROW(compSh[2], std::out_of_range);
  BOOST_CHECK_THROW(compSh[-2], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(emptyCompositeShapeThrowingExeptions)
{
  taran::CompositeShape emptyCompSh;

  BOOST_CHECK_THROW(emptyCompSh.move(3, 5), std::logic_error);
  BOOST_CHECK_THROW(emptyCompSh.move({1, 1}), std::logic_error);
  BOOST_CHECK_THROW(emptyCompSh.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(emptyCompSh.scale(2), std::logic_error);
}

BOOST_AUTO_TEST_SUITE_END()
