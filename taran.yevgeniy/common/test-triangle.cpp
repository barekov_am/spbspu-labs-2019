#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "triangle.hpp"

const double EPSILON = 0.01;

BOOST_AUTO_TEST_SUITE(TriangleTestSuite)

BOOST_AUTO_TEST_CASE(moveTriangleConstant)
{
  taran::Triangle triangleTest({0, 0}, {10, 10}, {20, -10});
  const taran::rectangle_t beforeFrame = triangleTest.getFrameRect();
  const double beforeArea = triangleTest.getArea();

  triangleTest.move(5, 5);
  BOOST_CHECK_CLOSE(beforeFrame.width, triangleTest.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(beforeFrame.height, triangleTest.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(beforeArea, triangleTest.getArea(), EPSILON);

  triangleTest.move({20, 20});
  BOOST_CHECK_CLOSE(beforeFrame.width, triangleTest.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(beforeFrame.height, triangleTest.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(beforeArea, triangleTest.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(scaleTriangleIncreaseArea)
{
  taran::Triangle triangleTest({0, 0}, {10, 10}, {20, -10});
  const double beforeArea = triangleTest.getArea();
  const double scaleFactor = 3;

  triangleTest.scale(scaleFactor);
  BOOST_CHECK_CLOSE(beforeArea * scaleFactor * scaleFactor, triangleTest.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(scaleTriangleDecreaseArea)
{
  taran::Triangle triangleTest({0, 0}, {10, 10}, {20, -10});
  const double beforeArea = triangleTest.getArea();
  const double scaleFactor = 0.5;

  triangleTest.scale(scaleFactor);
  BOOST_CHECK_CLOSE(beforeArea * scaleFactor * scaleFactor, triangleTest.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(constantAreaAfterRotation)
{
  taran::Triangle triangleTest({0, 0}, {10, 10}, {20, -10});
  const double testArea = triangleTest.getArea();
  const double angle = 90;

  triangleTest.rotate(angle);
  BOOST_CHECK_CLOSE(testArea, triangleTest.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidTriangleArguments)
{
  BOOST_CHECK_THROW(taran::Triangle triangleTest({0, 0}, {0, 0}, {10, 10}), std::invalid_argument);
  BOOST_CHECK_THROW(taran::Triangle triangleTest({0, 0}, {10, 10}, {10, 10}), std::invalid_argument);
  BOOST_CHECK_THROW(taran::Triangle triangleTest({0, 0}, {0, 0}, {0, 0}), std::invalid_argument);

  taran::Triangle triangleTest({0, 0}, {10, 10}, {20, -10});
  BOOST_CHECK_THROW(triangleTest.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
