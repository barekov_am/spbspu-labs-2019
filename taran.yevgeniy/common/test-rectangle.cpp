#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

const double EPSILON = 0.01;

BOOST_AUTO_TEST_SUITE(RectangleTestSuite)

BOOST_AUTO_TEST_CASE(moveRectangleConstant)
{
  taran::Rectangle rectangleTest({0, 0}, 20, 10);
  const taran::rectangle_t beforeFrame = rectangleTest.getFrameRect();
  const double beforeArea = rectangleTest.getArea();

  rectangleTest.move(5, 5);
  BOOST_CHECK_CLOSE(beforeFrame.width, rectangleTest.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(beforeFrame.height, rectangleTest.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(beforeArea, rectangleTest.getArea(), EPSILON);

  rectangleTest.move({20, 20});
  BOOST_CHECK_CLOSE(beforeFrame.width, rectangleTest.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(beforeFrame.height, rectangleTest.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(beforeArea, rectangleTest.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(scaleRectangleIncreaseArea)
{
  taran::Rectangle rectangleTest({0, 0}, 20, 10);
  const double beforeArea = rectangleTest.getArea();
  const double scaleFactor = 3;

  rectangleTest.scale(scaleFactor);
  BOOST_CHECK_CLOSE(beforeArea * scaleFactor * scaleFactor, rectangleTest.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(scaleRectangleDecreaseArea)
{
  taran::Rectangle rectangleTest({0, 0}, 20, 10);
  const double beforeArea = rectangleTest.getArea();
  const double scaleFactor = 0.5;

  rectangleTest.scale(scaleFactor);
  BOOST_CHECK_CLOSE(beforeArea * scaleFactor * scaleFactor, rectangleTest.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  taran::Rectangle rectangleTest({5, 5}, 4, 2);
  const double testArea = rectangleTest.getArea();
  const taran::rectangle_t testFrameRect = rectangleTest.getFrameRect();
  const double angle = 90;

  rectangleTest.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, rectangleTest.getFrameRect().width * 2, EPSILON);
  BOOST_CHECK_CLOSE(testFrameRect.height, rectangleTest.getFrameRect().height / 2, EPSILON);
  BOOST_CHECK_CLOSE(testArea, rectangleTest.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, rectangleTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, rectangleTest.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_CASE(invalidRectArguments)
{
  BOOST_CHECK_THROW(taran::Rectangle rectangleTest({0, 0}, -20, 10), std::invalid_argument);
  BOOST_CHECK_THROW(taran::Rectangle rectangleTest({0, 0}, -10, 20), std::invalid_argument);

  taran::Rectangle rectangleTest({0, 0}, 20, 10);
  BOOST_CHECK_THROW(rectangleTest.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
