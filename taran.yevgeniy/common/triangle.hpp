#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace taran
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &, const point_t &, const point_t &);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printFeatures() const override;
    void move(const point_t &) override;
    void move(double, double) override;
    void scale(double) override;
    void rotate(double) override;

  private:
    point_t pos_;
    point_t point1_;
    point_t point2_;
    point_t point3_;
  };
}
#endif
