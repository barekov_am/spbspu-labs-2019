#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>

#include "base-types.hpp"
#include "shape.hpp"

namespace taran
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape & copiedCompositeShape);
    CompositeShape(CompositeShape && movedCompositeShape);
    CompositeShape(const shape_ptr & shape);
    ~CompositeShape() = default;

    CompositeShape & operator =(const CompositeShape &);
    CompositeShape & operator =(CompositeShape &&);

    shape_ptr operator [](size_t) const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printFeatures() const override;
    void move(const point_t & point) override;
    void move(double, double) override;
    void scale(double override);
    void add(shape_ptr &);
    void remove(size_t);
    size_t getSize() const;
    shapes_array getFigures() const;
    void rotate(double) override;

  private:
    size_t size_;
    shapes_array shapes_;
  };
}
#endif
