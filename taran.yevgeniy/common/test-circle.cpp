#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

const double EPSILON = 0.01;

BOOST_AUTO_TEST_SUITE(CircleTestSuite)

BOOST_AUTO_TEST_CASE(moveCircleConstant)
{
  taran::Circle circleTest({0, 0}, 20);
  const taran::rectangle_t beforeFrame = circleTest.getFrameRect();
  const double beforeArea = circleTest.getArea();

  circleTest.move(5, 5);
  BOOST_CHECK_CLOSE(beforeFrame.width, circleTest.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(beforeFrame.height, circleTest.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(beforeArea, circleTest.getArea(), EPSILON);

  circleTest.move({20, 20});
  BOOST_CHECK_CLOSE(beforeFrame.width, circleTest.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(beforeFrame.height, circleTest.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(beforeArea, circleTest.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(scaleCircleIncreaseArea)
{
  taran::Circle circleTest({0, 0}, 20);
  const double beforeArea = circleTest.getArea();
  const double scaleFactor = 3;

  circleTest.scale(scaleFactor);
  BOOST_CHECK_CLOSE(beforeArea * scaleFactor * scaleFactor, circleTest.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(scaleCircleDecreaseArea)
{
  taran::Circle circleTest({0, 0}, 20);
  const double beforeArea = circleTest.getArea();
  const double scaleFactor = 3;

  circleTest.scale(scaleFactor);
  BOOST_CHECK_CLOSE(beforeArea * scaleFactor * scaleFactor, circleTest.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  taran::Circle circleTest({6, 3}, 4);
  const double testArea = circleTest.getArea();
  const taran::rectangle_t testFrameRect = circleTest.getFrameRect();
  const double angle = 90;

  circleTest.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, circleTest.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(testFrameRect.height, circleTest.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(testArea, circleTest.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, circleTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, circleTest.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_CASE(invalidCircleArguments)
{
  BOOST_CHECK_THROW(taran::Circle circleTest({0, 0}, -20), std::invalid_argument);

  taran::Circle circleTest({0, 0}, 20);
  BOOST_CHECK_THROW(circleTest.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
