#include "composite-shape.hpp"

#include <cmath>
#include <stdexcept>
#include <iostream>
#include <memory>
#include <algorithm>

taran::CompositeShape::CompositeShape() :
  size_(0)
{
}

taran::CompositeShape::CompositeShape(const CompositeShape & copiedCompositeShape) :
  size_(copiedCompositeShape.size_),
  shapes_(std::make_unique<shape_ptr[]>(copiedCompositeShape.size_))
{
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i] = copiedCompositeShape.shapes_[i];
  }
}

taran::CompositeShape::CompositeShape(CompositeShape && movedCompositeShape) :
  size_(movedCompositeShape.size_),
  shapes_(std::move(movedCompositeShape.shapes_))
{
  movedCompositeShape.size_ = 0;
}

taran::CompositeShape::CompositeShape(const shape_ptr & shape) :
  size_(1),
  shapes_(std::make_unique<shape_ptr[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer can't be null");
  }

  shapes_[0] = shape;
}

taran::CompositeShape & taran::CompositeShape::operator =(const CompositeShape & copiedCompositeShape)
{
  if (this != & copiedCompositeShape)
  {
    shapes_array shapesArray(std::make_unique<shape_ptr[]>(copiedCompositeShape.size_));
    size_ = copiedCompositeShape.size_;
    for (size_t i = 0; i < size_; i++)
    {
      shapesArray[i] = copiedCompositeShape.shapes_[i];
    }

    shapes_.swap(shapesArray);
  }

  return * this;
}

taran::CompositeShape & taran::CompositeShape::operator =(CompositeShape && movedCompositeShape)
{
  if (this != & movedCompositeShape)
  {
    size_ = movedCompositeShape.size_;
    shapes_ = std::move(movedCompositeShape.shapes_);
    movedCompositeShape.size_ = 0;
  }

  return * this;
}

taran::shape_ptr taran::CompositeShape::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Index out of range");
  }

  return shapes_[index];
}

double taran::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < size_; i++)
  {
    area += shapes_[i]->getArea();
  }

  return area;
}

taran::rectangle_t taran::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty!");
  }

  rectangle_t tmpShape = shapes_[0]->getFrameRect();
  double minX = tmpShape.pos.x - tmpShape.width / 2;
  double minY = tmpShape.pos.y - tmpShape.height / 2;
  double maxX = tmpShape.pos.x + tmpShape.width / 2;
  double maxY = tmpShape.pos.y + tmpShape.height / 2;

  for (size_t i = 0; i < size_; i++)
  {
    tmpShape = shapes_[i]->getFrameRect();
    double tmpValue = tmpShape.pos.x - tmpShape.width / 2;
    minX = std::min(tmpValue, minX);
    tmpValue = tmpShape.pos.y - tmpShape.height / 2;
    minY = std::min(tmpValue, minY);
    tmpValue = tmpShape.pos.x + tmpShape.width / 2;
    maxX = std::max(tmpValue, maxX);
    tmpValue = tmpShape.pos.y + tmpShape.height / 2;
    maxY = std::max(tmpValue, maxY);
  }

  return {{(maxX + minX) / 2, (maxY + minY) / 2}, (maxX - minX), (maxY - minY)};
}

void taran::CompositeShape::move(const point_t & point)
{
  point_t frameRectPos = getFrameRect().pos;
  double dx = point.x - frameRectPos.x;
  double dy = point.y - frameRectPos.y;

  move(dx, dy);
}

void taran::CompositeShape::move(double dx, double dy)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void taran::CompositeShape::printFeatures() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  std::cout << "SHAPE INFO \n";
  std::cout << "Area is: " << getArea() << "\n";
  std::cout << "Size of shapes_array is: " << getSize() << "\n";
  std::cout << "FRAME INFO\n";
  std::cout << "Width is: " << getFrameRect().width << "\n";
  std::cout << "Height is: " << getFrameRect().height << "\n";
  std::cout << "Center point on OX: " << getFrameRect().pos.x << "\n";
  std::cout << "Center point on OY: " << getFrameRect().pos.y << "\n\n";
}

void taran::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient can't be negative.");
  }

  const taran::point_t frameRectPos = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    taran::point_t shapeCenter = shapes_[i]->getFrameRect().pos;
    const double dx = (shapeCenter.x - frameRectPos.x) * (coefficient - 1);
    const double dy = (shapeCenter.y - frameRectPos.y) * (coefficient - 1);

    shapes_[i]->move(dx, dy);
    shapes_[i]->scale(coefficient);
  }
}

void taran::CompositeShape::add(shape_ptr & shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer can't be null");
  }

  shapes_array shapesArray(std::make_unique<shape_ptr[]>(size_ + 1));
  for(size_t i = 0; i < size_; i++)
  {
    shapesArray[i] = shapes_[i];
  }

  shapesArray[size_ ] = shape;
  size_++;
  shapes_.swap(shapesArray);
}

void taran::CompositeShape::remove(size_t index)
{
  if (index >= size_)
  {
    throw std::invalid_argument("Index out of range");
  }

  size_--;
  for (size_t i = index; i < size_ ; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }

  shapes_[size_] = nullptr;
}

size_t taran::CompositeShape::getSize() const
{
  return size_;
}

taran::shapes_array taran::CompositeShape::getFigures() const
{
  shapes_array tmpFigures(std::make_unique<shape_ptr[]>(size_));

  for (size_t i = 0; i < size_; i++)
  {
    tmpFigures[i] = shapes_[i];
  }

  return tmpFigures;
}

void taran::CompositeShape::rotate(double angle)
{
  const double cosA = std::abs(std::cos(angle * M_PI / 180));
  const double sinA = std::abs(std::sin(angle * M_PI / 180));
  const point_t center = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++)
  {
    point_t tmpCenter = shapes_[i]->getFrameRect().pos;
    const double dx = (tmpCenter.x - center.x) * (cosA - 1) - (tmpCenter.y - center.y)  * sinA;
    const double dy = (tmpCenter.x - center.x) * sinA + (tmpCenter.y - center.y)  * (cosA - 1);

    shapes_[i]->move(dx, dy);
    shapes_[i]->rotate(angle);
  }
}
