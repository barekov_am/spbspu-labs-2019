#ifndef CIRCLE_HPP_INCLUDED
#define CIRCLE_HPP_INCLUDED

#include "shape.hpp"

namespace taran
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t &, double);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printFeatures() const override;
    void move(const point_t &) override;
    void move(double, double) override;
    void scale(double) override;
    void rotate(double) override;

  private:
    point_t pos_;
    double radius_;
  };
}
#endif
