#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(partitionMetodsTests)

BOOST_AUTO_TEST_CASE(partitionTest)
{
  const taran::Rectangle rect1({0, 0}, 2, 1);
  taran::shape_ptr rectOnePtr = std::make_shared<taran::Rectangle>(rect1);
  const taran::Rectangle rect2({15, 10}, 2, 3);
  taran::shape_ptr rectTwoPtr = std::make_shared<taran::Rectangle>(rect2);
  const taran::Circle circ({1, -1}, 3);
  taran::shape_ptr circPtr = std::make_shared<taran::Circle>(circ);

  taran::CompositeShape compSh(rectOnePtr);
  taran::Matrix matrix1 = taran::split(compSh);

  compSh.add(circPtr);
  taran::Matrix matrix2 = taran::split(compSh);

  compSh.add(rectTwoPtr);
  taran::Matrix matrix3 = taran::split(compSh);

  BOOST_CHECK_EQUAL(matrix1.getLines(), 1);
  BOOST_CHECK_EQUAL(matrix1.getColumns(), 1);

  BOOST_CHECK_EQUAL(matrix2.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix2.getColumns(), 1);

  BOOST_CHECK_EQUAL(matrix3.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix3.getColumns(), 2);
}

BOOST_AUTO_TEST_CASE(intersectionTest)
{
  const taran::Rectangle rect1({1, 2}, 5, 4);
  const taran::Rectangle rect2({7, 7}, 3, 1);
  const taran::Circle circ1({-1, 2}, 2);
  const taran::Circle circ2({0, 1}, 4);

  BOOST_CHECK(intersect(rect1.getFrameRect(), circ1.getFrameRect()));
  BOOST_CHECK(intersect(rect1.getFrameRect(), circ2.getFrameRect()));
  BOOST_CHECK(!intersect(rect1.getFrameRect(), rect2.getFrameRect()));
  BOOST_CHECK(!intersect(circ1.getFrameRect(), rect2.getFrameRect()));
  BOOST_CHECK(!intersect(circ2.getFrameRect(), rect2.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
