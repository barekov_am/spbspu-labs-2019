#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "split.hpp"
#include "matrix.hpp"

int main()
{
  dang::Rectangle rectangle(7, 6, { 4, 5 });
  dang::Circle circle(6, { 4, 5 });
  dang::Triangle triangle({ 4, 6 }, { 6, 7 }, { 1, 10 });
  std::cout << rectangle.getArea() << std::endl;
  std::cout << circle.getArea() << std::endl;
  std::cout << triangle.getArea() << std::endl;
  dang::CompositeShape compositeShape(std::make_shared<dang::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<dang::Circle>(circle));
  compositeShape.add(std::make_shared<dang::Triangle>(triangle));
  std::cout << compositeShape.getArea() << std::endl;
  std::unique_ptr<dang::Shape::shapePtr[]> shape = std::make_unique<dang::Shape::shapePtr[]>(5);

  dang::Rectangle rectangle1(2, 1, { 2, 4.5 });
  dang::Rectangle rectangle2(5, 2, { 3.5, 2 });
  dang::Circle circle1(2, { 6, 4 });
  dang::Triangle triangle1({ 3, 2 }, { 2, 0 }, { 4, 0 });
  dang::Triangle triangle2({ 7, 4 }, { 9, 3 }, { 9, 5 });

  shape[0] = std::make_shared<dang::Rectangle>(rectangle1);
  shape[1] = std::make_shared<dang::Rectangle>(rectangle2);
  shape[2] = std::make_shared<dang::Circle>(circle1);
  shape[3] = std::make_shared<dang::Triangle>(triangle1);
  shape[4] = std::make_shared<dang::Triangle>(triangle2);
  dang::Matrix matrix = dang::division(shape, 5);
  matrix.showAll();
  return 0;
}
