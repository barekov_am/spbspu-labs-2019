#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <list>
#include <string>

typedef struct
{
  std::string name;
  std::string number;
} recordType;

using container = std::list<recordType>;

class PhoneBook
{
public:
  using iter = container::iterator;

  PhoneBook();

  void viewCurrentRecord(iter& iter);
  iter nextRecord();
  iter previousRecord();
  PhoneBook::iter insertAfter(const recordType& record, iter& position);
  PhoneBook::iter insertBefore(const recordType& record, iter& position);
  void replaceCurrentRecord(const recordType& record);
  void pushBack(const recordType& record);
  void deleteRecord(iter& element);
  bool isEmpty() const;
  iter getRecord();
  iter begin();
  iter end();

private:
  container phoneBook_;
  iter iterator_;
};

#endif

