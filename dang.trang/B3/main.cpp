#include <iostream>
#include <string>

void firstTask();
void secondTask();

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cout << "<INVALID COMMAND>\n";
      return 1;
    }

    const int taskId = std::stoi(argv[1]);

    switch (taskId)
    {
    case 1:
    {
      firstTask();
      return 0;
    }

    case 2:
    {
      secondTask();
      return 0;
    }

    default:
    {
      std::cout << "<INVALID COMMAND>\n";
      return 1;
    }
    }
  }

  catch (const std::exception & err)
  {
    std::cout << err.what() << std::endl;
    return 1;
  }

  return 0;
}
