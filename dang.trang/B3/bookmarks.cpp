#include "bookmarks.hpp"

#include <stdexcept>
#include <iostream>

Bookmarks::Bookmarks(PhoneBook* phoneBook) :
  phoneBookPointer_(phoneBook)
{
  markType mark;

  mark.markName = "current";
  mark.iter = phoneBookPointer_->end();

  bookmarks_.push_back(mark);
}

void Bookmarks::store(std::string& markName, std::string& newMarkName)
{
  std::list<markType>::iterator i;
  for (i = bookmarks_.begin(); i != bookmarks_.end(); ++i)
  {
    if (i->markName == markName)
    {
      break;
    }
  }

  markType mark;

  if (i->iter == phoneBookPointer_->end())
  {
    mark.markName = newMarkName;
    mark.iter = phoneBookPointer_->end();
  }
  else
  {
    mark.markName = newMarkName;
    mark.iter = i->iter;
  }

  bookmarks_.push_back(mark);
}

void Bookmarks::insertBefore(std::string& markName, std::string& number, std::string& name)
{
  std::list<markType>::iterator i;
  for (i = bookmarks_.begin(); i != bookmarks_.end(); ++i)
  {
    if (i->markName == markName)
    {
      break;
    }
  }

  recordType record;
  markType type;

  record.name = name;
  record.number = number;

  i->iter = phoneBookPointer_->insertBefore(record, i->iter);
}

void Bookmarks::insertAfter(std::string& markName, std::string& number, std::string& name)
{
  std::list<markType>::iterator i;
  for (i = bookmarks_.begin(); i != bookmarks_.end(); ++i)
  {
    if (i->markName == markName)
    {
      break;
    }
  }

  recordType record;

  record.name = name;
  record.number = number;

  i->iter = phoneBookPointer_->insertAfter(record, i->iter);
}

void Bookmarks::remove(std::string& markName)
{
  std::list<markType>::iterator i;
  for (i = bookmarks_.begin(); i != bookmarks_.end(); ++i)
  {
    if (i->markName == markName)
    {
      break;
    }
  }

  auto iter = i->iter;

  if (iter == phoneBookPointer_->end() || phoneBookPointer_->isEmpty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  if (std::next(i->iter) != phoneBookPointer_->end())
  {
    ++iter;
  }
  else if (i->iter != phoneBookPointer_->begin())
  {
    --iter;
  }
  else
  {
    iter = phoneBookPointer_->end();
  }

  for (auto j = bookmarks_.begin(); j != bookmarks_.end(); ++j)
  {
    if (i == j)
    {
      continue;
    }

    if (i->iter == j->iter)
    {
      j->iter = iter;
    }
  }

  phoneBookPointer_->deleteRecord(i->iter);

  i->iter = iter;
}

void Bookmarks::show(std::string& markName)
{
  std::list<markType>::iterator i;
  for (i = bookmarks_.begin(); i != bookmarks_.end(); ++i)
  {
    if (i->markName == markName)
    {
      break;
    }
  }

  phoneBookPointer_->viewCurrentRecord(i->iter);
}

markType Bookmarks::getCurrent()
{
  return *getCurrentIterator();
}

std::list<markType>::iterator Bookmarks::getCurrentIterator()
{
  for (auto i = bookmarks_.begin(); i != bookmarks_.end(); ++i)
  {
    if (i->markName == "current")
    {
      return i;
    }
  }

  throw std::runtime_error("Could not find current");
}

void Bookmarks::replaceCurrent(markType& newCurrent)
{
  bookmarks_.erase(getCurrentIterator());
  bookmarks_.push_back(newCurrent);
}

bool Bookmarks::findBookmark(std::string& markName)
{
  std::list<markType>::iterator i;
  for (i = bookmarks_.begin(); i != bookmarks_.end(); ++i)
  {
    if (i->markName == markName)
    {
      return true;
    }
  }

  return false;
}

void Bookmarks::move(std::string& markName, int& step)
{
  std::list<markType>::iterator i;
  for (i = bookmarks_.begin(); i != bookmarks_.end(); ++i)
  {
    if (i->markName == markName)
    {
      break;
    }
  }

  if (i->iter == phoneBookPointer_->end())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  if (step >= 0)
  {
    while (!(std::next(i->iter) == phoneBookPointer_->end()) && (step > 0))
    {
      i->iter = std::next(i->iter);
      --step;
    }
  }
  else if (step < 0)
  {
    while (!(i->iter == phoneBookPointer_->begin()) && (step < 0))
    {
      i->iter = std::prev(i->iter);
      --step;
    }
  }
  else
  {
    std::cout << "<INVALID STEP>\n";
  }
}

void Bookmarks::moveFirst(std::string& markName)
{
  std::list<markType>::iterator i;
  for (i = bookmarks_.begin(); i != bookmarks_.end(); ++i)
  {
    if (i->markName == markName)
    {
      break;
    }
  }

  i->iter = phoneBookPointer_->isEmpty()
    ? phoneBookPointer_->end()
    : phoneBookPointer_->begin();
}

void Bookmarks::moveLast(std::string& markName)
{
  std::list<markType>::iterator i;
  for (i = bookmarks_.begin(); i != bookmarks_.end(); ++i)
  {
    if (i->markName == markName)
    {
      break;
    }
  }

  i->iter = phoneBookPointer_->isEmpty() ? phoneBookPointer_->end() : --phoneBookPointer_->end();
}
