#include <iostream>
#include <sstream>
#include "phoneBook.hpp"
#include "bookmarks.hpp"

std::string getName(std::stringstream& string)
{
  std::string externalName = "";

  string.ignore();

  if (string.peek() != '\"')
    return "";

  while (string && string.peek() != -1)
  {
    const char c = string.get();

    if (c == '\"' || c == '\0')
    {
      continue;
    }
    else if (c == '\\')
    {
      const char nextC = string.get();

      if (nextC == '\"')
      {
        externalName += nextC;
      }
      else
      {
        externalName += c;
      }
    }
    else
    {
      externalName += c;
    }
  }

  return externalName;
}

void firstTask()
{
  std::string line;

  recordType recType;
  PhoneBook bookPhone;
  Bookmarks marks(&bookPhone);

  while (getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error reading data.");
    }

    std::stringstream string(line);

    std::string command;

    string >> command;

    if (command == "add")
    {
      std::string externalNumber;

      string >> externalNumber;

      std::string externalName = getName(string);

      if (externalName.empty())
      {
        std::cout << "<INVALID COMMAND>\n";
        continue;
      }

      recType.number = externalNumber;
      recType.name = externalName;

      if (bookPhone.isEmpty())
      {
        bookPhone.pushBack(recType);
        auto mark = marks.getCurrent();
        mark.iter = bookPhone.getRecord();
        marks.replaceCurrent(mark);
      }
      else
      {
        bookPhone.pushBack(recType);
      }
    }
    else if (command == "store")
    {
      std::string markName;
      std::string newMarkName;

      string >> markName >> newMarkName;

      if (marks.findBookmark(markName))
      {
        marks.store(markName, newMarkName);
      }
      else
      {
        std::cout << "<INVALID BOOKMARK>\n";
      }
    }
    else if (command == "insert")
    {
      std::string additionalCommand;
      std::string markName;
      std::string externalNumber;

      string >> additionalCommand;

      string >> markName >> externalNumber;

      std::string externalName = getName(string);

      if (externalName.empty())
      {
        std::cout << "<INVALID COMMAND>\n";
        continue;
      }

      if (marks.findBookmark(markName))
      {
        if (additionalCommand == "before")
        {
          marks.insertBefore(markName, externalNumber, externalName);
        }
        else if (additionalCommand == "after")
        {
          marks.insertAfter(markName, externalNumber, externalName);
        }
        else
        {
          std::cout << "<INVALID COMMAND>\n";
        }
      }
      else
      {
        std::cout << "<INVALID BOOKMARK>\n";
      }
    }
    else if (command == "delete")
    {
      std::string markName;

      string >> markName;

      if (marks.findBookmark(markName))
      {
        marks.remove(markName);
      }
      else
      {
        std::cout << "<INVALID BOOKMARK>\n";
      }
    }
    else if (command == "show")
    {
      std::string markName;

      string >> markName;

      if (marks.findBookmark(markName))
      {
        marks.show(markName);
      }
      else
      {
        std::cout << "<INVALID BOOKMARK>\n";
      }
    }
    else if (command == "move")
    {
      std::string markName;
      std::string externalStep;

      string >> markName >> externalStep;

      if (marks.findBookmark(markName))
      {
        if (externalStep == "first")
        {
          marks.moveFirst(markName);
        }
        else if (externalStep == "last")
        {
          marks.moveLast(markName);
        }
        else
        {
          try
          {
            int step = std::stoi(externalStep);
            marks.move(markName, step);
          }
          catch (std::invalid_argument)
          {
            std::cout << "<INVALID STEP>\n";
          }
        }
      }
      else
      {
        std::cout << "<INVALID BOOKMARK>\n";
      }
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
