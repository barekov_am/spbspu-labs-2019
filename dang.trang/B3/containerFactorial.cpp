#include "containerFactorial.hpp"
#include "containerFactorial.hpp"

ContainerFactorial::Iterator::Iterator() :
  value_(1),
  index_(1)
{
}

ContainerFactorial::Iterator::Iterator(int index) :
  value_(getValue(index)),
  index_(index)
{
}

ContainerFactorial::Iterator::reference ContainerFactorial::Iterator::operator*()
{
  return value_;
}

ContainerFactorial::Iterator::pointer ContainerFactorial::Iterator::operator->()
{
  return &value_;
}

ContainerFactorial::Iterator& ContainerFactorial::Iterator::operator++()
{
  index_++;
  value_ = value_ * index_;
  return *this;
}

ContainerFactorial::Iterator ContainerFactorial::Iterator::operator++(int)
{
  Iterator tmp = *this;
  ++(*this);
  return tmp;
}

ContainerFactorial::Iterator& ContainerFactorial::Iterator::operator--()
{
  if (index_ > 1)
  {
    value_ /= index_;
    --index_;
  }

  return *this;
}

ContainerFactorial::Iterator ContainerFactorial::Iterator::operator--(int)
{
  ContainerFactorial::Iterator tmp = *this;
  --(*this);
  return tmp;
}

bool ContainerFactorial::Iterator::operator==(const ContainerFactorial::Iterator& rhs) const
{
  return ((value_ == rhs.value_) && (index_ == rhs.index_));
}

bool ContainerFactorial::Iterator::operator!=(const ContainerFactorial::Iterator& rhs) const
{
  return !(*this == rhs);
}

long ContainerFactorial::Iterator::getValue(int index) const
{
  if (index <= 1)
  {
    return 1;
  }
  else
  {
    return (index * getValue(index - 1));
  }
}

ContainerFactorial::Iterator ContainerFactorial::begin()
{
  return Iterator(1);
}

ContainerFactorial::Iterator ContainerFactorial::end()
{
  return Iterator(11);
}
