#include <iostream>
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "circle.hpp"
#include "triangle.hpp"

void actions(dang::CompositeShape &usingcompositeShape_)
{
  usingcompositeShape_.printInfo();
  std::cout << "Move shape for 5 right, 6 up" << "\n";
  usingcompositeShape_.move(5, 6);
  usingcompositeShape_.printInfo();
  std::cout << "Move shape to (4, 7)" << "\n";
  usingcompositeShape_.move({ 4, 7 });
  std::cout << "Cen= " << usingcompositeShape_.getFrameRect().pos.x << " " << usingcompositeShape_.getFrameRect().pos.y << "\n";
  usingcompositeShape_.scale(2);
  std::cout << "Area after scale = " << usingcompositeShape_.getArea() << "\n";
  std::cout << "Cen= " << usingcompositeShape_.getFrameRect().pos.x << " " << usingcompositeShape_.getFrameRect().pos.y << "\n";
  usingcompositeShape_.remove(1);
  std::cout << "Area after delete Shape = " << usingcompositeShape_.getArea() << "\n";
}

int main()
{
  dang::CompositeShape compositeShape5;
  dang::Rectangle rectangle(4, 4, { 4, 5 });
  compositeShape5.add(std::make_shared<dang::Rectangle>(rectangle));
  std::cout << "Area of Composite Shape= " << compositeShape5.getArea() << "\n";
  dang::Circle circle(3, { 6, 9 });
  dang::CompositeShape compositeShape(std::make_shared<dang::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<dang::Circle>(circle));
  dang::Triangle triangle({ 4, 10 }, { 1, 1 }, { 10, 1 });
  std::cout << "Area of Triangle= " << triangle.getArea() << "\n";
  compositeShape.add(std::make_shared<dang::Triangle>(triangle));
  std::cout << "Area of triangle in Composite= " << compositeShape[2]->getArea() << "\n";
  actions(compositeShape);
  dang::CompositeShape compositeShape2 = compositeShape;
  actions(compositeShape2);
  dang::CompositeShape compositeShape3(compositeShape);
  if (compositeShape3.getArea() != compositeShape2.getArea())
  {
    std::cout << "This is different thing!\n";
  }
  return 0;
}
