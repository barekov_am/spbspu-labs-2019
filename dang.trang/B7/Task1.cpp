#include "Tasks.hpp"
#include <iostream>
#include <iterator>
#include <algorithm>
#include <list>
#define _USE_MATH_DEFINES
#include <math.h>

class MultiNumber
{
public:
  MultiNumber(double k) : k_(k) {}
  void operator() (double &num) { num *= k_; }
private:
  double k_;
};

void task1(std::istream &in, std::ostream &out)
{
  std::list<double> numbers;
  std::copy(std::istream_iterator<double>(in), std::istream_iterator<double>(),
    std::back_inserter(numbers));

  if(!in.eof())
  {
    throw std::invalid_argument("Wrong input!");
  }

  std::for_each(numbers.begin(), numbers.end(), MultiNumber(M_PI));

  std::copy(numbers.begin(), numbers.end(), std::ostream_iterator<double>(out, " "));
}
