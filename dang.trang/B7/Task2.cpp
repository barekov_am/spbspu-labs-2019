#include <iostream>
#include <functional>
#include <algorithm>
#include <iterator>
#include <memory>
#include <sstream>
#include <locale>
#include <list>
#include "Tasks.hpp"
#include "Shapes.hpp"
#include "Circles.hpp"
#include "Triangles.hpp"
#include "Square.hpp"

class Line: public std::string
{
public:
  bool checkSpace()
  {
    return (std::find_if(begin(), end(), [](char c) { return !std::isspace(c, std::locale()); }) != end());
  }
  friend std::istream &operator>>(std::istream &in, Line &line)
  {
    while(std::getline(in, line))
    {
      if(!line.empty() && line.checkSpace())
      {
        break;
      }
    }
    return in;
  }
};

std::unique_ptr<Shapes> makeShapeInTheLine(const Line &line)
{
  std::stringstream stream(line);
  std::string typeOfShapes;
  std::getline(stream, typeOfShapes, '(');
  auto firstChar = std::find_if(typeOfShapes.begin(), typeOfShapes.end(), std::bind(std::isalpha<char>, std::placeholders::_1, std::locale()));
  typeOfShapes.erase(typeOfShapes.begin(), firstChar);
  typeOfShapes.erase(std::find_if(firstChar, typeOfShapes.end(), std::bind(std::isspace<char>, std::placeholders::_1, std::locale())), typeOfShapes.end());

  double x_, y_;
  stream >> x_;
  stream.ignore(std::numeric_limits<std::streamsize>::max(), ';');
  stream >> y_;
  stream.ignore(std::numeric_limits<std::streamsize>::max(), ')');
  if(stream.fail())
  {
    throw std::invalid_argument("Syntax error for input!");
  }
  Line remLine;
  std::getline(stream, remLine);
  if(remLine.checkSpace())
  {
    throw std::invalid_argument("Wrong input!");
  }
  if(typeOfShapes == "TRIANGLE")
  {
    return std::unique_ptr<Shapes>(new Triangles(x_, y_));
  }
  else if(typeOfShapes == "CIRCLE")
  {
    return std::unique_ptr<Shapes>(new Circles(x_, y_));
  }
  else if(typeOfShapes == "SQUARE")
  {
    return std::unique_ptr<Shapes>(new Square(x_, y_));
  }
  else
  {
    throw std::invalid_argument("Wrong type input of shapes!");
  }
}

void task2(std::istream &inforInput, std::ostream &infoOutput)
{
  std::list<std::unique_ptr<Shapes>> listOfShape;
  std::transform(std::istream_iterator<Line>(inforInput), std::istream_iterator<Line>(), std::back_inserter(listOfShape), makeShapeInTheLine);

  infoOutput << "Original:" << std::endl;
  std::for_each(listOfShape.begin(), listOfShape.end(), std::bind(&Shapes::draw, std::placeholders::_1, std::ref(infoOutput)));

  infoOutput << "Left-Right:" << std::endl;
  listOfShape.sort(std::bind(&Shapes::isMoreLeft,std::bind(&std::unique_ptr<Shapes>::get, std::placeholders::_1),
    std::bind(&std::unique_ptr<Shapes>::get, std::placeholders::_2)));
  std::for_each(listOfShape.begin(), listOfShape.end(), std::bind(&Shapes::draw, std::placeholders::_1, std::ref(infoOutput)));

  infoOutput << "Right-Left:" << std::endl;
  std::for_each(listOfShape.rbegin(), listOfShape.rend(), std::bind(&Shapes::draw, std::placeholders::_1, std::ref(infoOutput)));

  infoOutput << "Top-Bottom:" << std::endl;
  listOfShape.sort(std::bind(&Shapes::isUpper, std::bind(&std::unique_ptr<Shapes>::get, std::placeholders::_1),
    std::bind(&std::unique_ptr<Shapes>::get, std::placeholders::_2)));
  std::for_each(listOfShape.begin(), listOfShape.end(), std::bind(&Shapes::draw, std::placeholders::_1, std::ref(infoOutput)));

  infoOutput << "Bottom-Top:" << std::endl;
  std::for_each(listOfShape.rbegin(), listOfShape.rend(), std::bind(&Shapes::draw, std::placeholders::_1, std::ref(infoOutput)));
}
