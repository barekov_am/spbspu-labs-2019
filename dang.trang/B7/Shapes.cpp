#include "Shapes.hpp"

Shapes::Shapes(double x, double y):
  x_(x),
  y_(y)
{
}
bool Shapes::isMoreLeft(const Shapes *other) const
{
  return (x_ < other->x_);
}
bool Shapes::isUpper(const Shapes *other) const
{
  return (y_ > other->y_);
}
