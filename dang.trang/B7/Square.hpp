#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "Shapes.hpp"

class Square: public Shapes
{
public:
  Square(double x, double y);
  void draw(std::ostream &out) const override;
};

#endif // SQUARE_HPP
