#ifndef CIRCLES_HPP
#define CIRCLES_HPP

#include "Shapes.hpp"

class Circles: public Shapes
{
public:
  Circles(double x, double y);
  void draw(std::ostream &out) const override;
};

#endif // CIRCLE_HPP
