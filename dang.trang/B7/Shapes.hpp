#ifndef SHAPES_HPP
#define SHAPES_HPP

#include <iostream>

class Shapes
{
public:
  Shapes(double x, double y);
  virtual ~Shapes() = default;
  bool isMoreLeft(const Shapes *other) const;
  bool isUpper(const Shapes *other) const;
  virtual void draw(std::ostream &out) const = 0;
protected:
  double x_, y_;
};

#endif // SHAPE_HPP
