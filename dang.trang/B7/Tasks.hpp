#ifndef OPTIONS_HPP_INCLUDED
#define OPTIONS_HPP_INCLUDED

#include <iostream>

void task1(std::istream &infoInput, std::ostream &infoOutput);
void task2(std::istream &infoInput, std::ostream &infoOutput);

#endif // OPTIONS_HPP_INCLUDED
