#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "Shapes.hpp"
#include <iostream>

class Triangles: public Shapes
{
public:
  Triangles(double x, double y);
  void draw(std::ostream &out) const override;
};

#endif // TRIANGLE_HPP
