#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

class GetStats
{
public:
  GetStats();
  void operator() (int val);
  void ShowData();
private:
  int max_, min_, positive_, negative_, first_, last_, numElements_;
  long long oddSum_, evenSum_;
  double mean;
};

#endif
