#include "functor.hpp"
#include <iostream>
#include <algorithm>
#include <functional>
#include <iomanip>

GetStats::GetStats() :
  max_(std::numeric_limits<int>::min()),
  min_(std::numeric_limits<int>::max()),
  positive_(0),
  negative_(0),
  numElements_(0),
  oddSum_(0),
  evenSum_(0)
{
}

void GetStats:: operator() (int val)
{
  max_ = std::max(max_, val);
  min_ = std::min(min_, val);

  if (val > 0)
  {
    positive_++;
  }
  else if (val < 0)
  {
    negative_++;
  }

  if (val % 2 == 0)
  {
    evenSum_ += val;
  }
  else
  {
    oddSum_ += val;
  }

  if (numElements_ == 0)
  {
    first_ = val;
  }

  last_ = val;
  numElements_++;
}

void GetStats::ShowData()
{
  if (numElements_ == 0)
  {
    std::cout << "No Data" << std::endl;
    return;
  }

  std::cout << "Max: " << max_ << std::endl;
  std::cout << "Min: " << min_ << std::endl;

  double mean = static_cast<double>(evenSum_ + oddSum_) / static_cast<double>(numElements_);

  std::cout << "Mean: " << std::setprecision(1) << std::fixed << mean << std::endl;
  std::cout << "Positive: " << positive_ << std::endl;
  std::cout << "Negative: " << negative_ << std::endl;
  std::cout << "Odd Sum: " << oddSum_ << std::endl;
  std::cout << "Even Sum: " << evenSum_ << std::endl;
  std::cout << "First/Last Equal: " << (first_ == last_ ? "yes" : "no") << std::endl;
}
