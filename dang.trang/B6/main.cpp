#include <iostream>
#include <algorithm>
#include <iterator>
#include <functional>
#include <iomanip>
#include "functor.hpp"

int main()
{
  GetStats getStats;
  std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), std::ref(getStats));

  if (!std::cin.eof())
  {
    std::cerr << "Wrong input!\n";
    return 1;
  }

  getStats.ShowData();

  return 0;
}
