#include <iostream>
#include <iterator>
#include <vector>
#include <string>
#include <locale>
#include <stdio.h>
#include "source.hpp"

std::locale current_locale;

int main(int argc, char *argv[])
{
  current_locale = std::cout.getloc();
  size_t lineWidthMax = 0;
  switch (argc)
  {
    case 1:
    {
      lineWidthMax = 40;
    }
    break;
    case 3:
    {
      if (std::string(argv[1]) != "--line-width")
      {
        std::cerr << "Need to: --line-width";
        return 1;
      }
      try
      {
        lineWidthMax = std::stoi(argv[2]);
      }
      catch(std::invalid_argument)
      {
        std::cerr << "Incorrect line width";
        return 1;
      }
    }
    break;
    default:
    {
      std::cerr << "Incorrect arguments";
      return 1;
    }
  }

  if (lineWidthMax<25)
  {
    std::cerr << "Bad line width";
    return 1;
  }

  char decimal_separator = std::use_facet< std::numpunct<char> >(current_locale).decimal_point();
  bool nextIsDigit = false;

  std::vector<char> words;
  std::vector<char> buffer;

  std::cin >> std::ws;

  size_t counter = 0;
  char ch_current = std::cin.get();

  if (ch_current == EOF)
  {
    return 0;
  }

  type prev_type = TypeSymbol(ch_current, current_locale);
  if (prev_type != other)
  {
    words.push_back(ch_current);
    ++counter;
  }
  else
  {
    char ch_next = std::cin.peek();
    if ((ch_current == '-') && (TypeSymbol(ch_next, current_locale) == isNumDigit) && (ch_next != '+'))
    {
      words.push_back(ch_current);
      ++counter;
      prev_type = isNumDigit;
    }
    else
    {
      std::cerr << "First - punctuation prohibited";
      return 1;
    }
  }

  size_t lineStart = 0;
  size_t possibleLineEnd = 0;
  std::vector<size_t> bordArray;

  while ((ch_current = std::cin.get()) != EOF)
  {
    type current_type{ TypeSymbol(ch_current, current_locale) };

    if ((current_type == isLetter) || (current_type == isNumDigit))
    {
      words.push_back(ch_current);
      if (prev_type == current_type)
      {
        ++counter;
      }
      else
      {
        if (prev_type != other)
        {
          std::cerr << "Bad input";
          return 1;
        }
        prev_type = current_type;
        counter = 1;
      }
    }
    else
    {
      if (((ch_current == decimal_separator) && (prev_type == isNumDigit) && (TypeSymbol(std::cin.peek(), current_locale) == isNumDigit)) || ((ch_current == '-') && (prev_type == isLetter) && (std::cin.peek() != '-')))
      {
        words.push_back(ch_current);
        ++counter;
        continue;
      }

      if (prev_type != other)
      {
        if (counter>20)
        {
          std::cerr << "Too long word/number";
          return 1;
        }
        else
        {
          counter = 0;
        }
        
        buffer.clear();
        buffer.push_back(ch_current);

        while (TypeSymbol(std::cin.peek(), current_locale) == other && (std::cin.peek() != EOF))
        {
          ch_current = std::cin.get();
          buffer.push_back(ch_current);
        }

        char chp = std::cin.peek();
        if ((ch_current == '-') && (buffer[buffer.size() - 2] != '-') && (TypeSymbol(chp, current_locale) == isNumDigit) && (chp != '+'))
        {
          nextIsDigit = true;
          buffer.pop_back();
        }

        format(buffer, current_locale);
        if (buffer.empty())
        {
          std::cerr << "Bad input";
          return 1;
        }

        words.insert(words.end(), buffer.begin(), buffer.end());

        size_t currentWidth = words.size() - lineStart;
        if (currentWidth < lineWidthMax)
        {
          possibleLineEnd = words.size() - 1;
        }
        else
        {
          size_t lineEnd = 0;
          if ((currentWidth == lineWidthMax) || (currentWidth == lineWidthMax + 1))
          {
            lineEnd = words.size() - 1;
          }
          else
          {
            lineEnd = possibleLineEnd;
          }
          bordArray.push_back(lineEnd);
          lineStart = lineEnd + 1;
          possibleLineEnd = words.size() - 1;
        }


        if (!nextIsDigit)
        {
          prev_type = other;
        }
        else
        {
          nextIsDigit = false;
          words.push_back('-');
          counter = 1;
          prev_type = isNumDigit;
        }
      }
    }
  }

  if (counter>20)
  {
    std::cerr << "Too long word/number";
    return 1;
  }

  if ((words.size() - lineStart) > lineWidthMax)
  {
    bordArray.push_back(possibleLineEnd);
  }

  std::vector<char*> strVec;
  auto it = words.begin();
  strVec.push_back(&(*it));

  for (size_t i = 0; i != bordArray.size(); i++)
  {
    words[bordArray[i]] = '\0';
    strVec.push_back(&(words[bordArray[i] + 1]));
  }

  if (*(words.end() - 1) == ' ')
  {
    *(words.end() - 1) = '\0';
  }
  else
  {
    words.push_back('\0');
  }

  for (auto it = strVec.begin(); it != strVec.end(); it++)
  {
    std::printf("%s", *it);
    std::cout << std::endl;
  }

  return 0;
}
