#include <locale>
#include "source.hpp"

type TypeSymbol(char symbol, std::locale current_locale)
{
  if (std::isalpha(symbol, current_locale))
  {
    return isLetter;
  }
  if (std::isdigit(symbol, current_locale) || symbol == '+')
  {
    return isNumDigit;
  }
  return other;
}

void format(std::vector<char>& bufer, std::locale current_locale)
{
  bool isComma = false, isDash = false, otherFlag = false;
  char punc = ' ';
  std::size_t i = 0, end = bufer.size();
  while (i != end)
  {
    if (std::isspace(bufer[i], current_locale))
    {
      i++;
      continue;
    }

    if (isDash || otherFlag)
    {
      bufer.clear();
      return;
    }

    switch (bufer[i])
    {
      case ',':
      {
        if (isComma)
        {
          bufer.clear();
          return;
        }
        isComma = true;
        punc = ',';
      }
      break;
      case '-':
      {
        if (i + 2<end && bufer[i + 2] == '-' && bufer[i + 1] == '-')
        {
          isDash = true;
          i += 3;
          continue;
        }
        else
        {
          bufer.clear();
          return;
        }
      }
      break;
      default:
      {
        if (isComma)
        {
          bufer.clear();
          return;
        }
        punc = bufer[i];
        otherFlag = true;
      }
      break;
    }
    i++;
  }

  bufer.clear();

  if (otherFlag || isComma)
  {
    bufer.push_back(punc);
  }

  if (isDash)
  {
    bufer.push_back(' ');
    bufer.insert(bufer.end(), 3, '-');
  }

  bufer.push_back(' ');

  return;
}
