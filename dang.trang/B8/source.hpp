#ifndef SOURCE_HPP
#define SOURCE_HPP

#include <vector>
#include <locale>

enum type { isLetter, isNumDigit, other };

type TypeSymbol(char symbol, std::locale current_locale);
void format(std::vector<char>& bufer, std::locale current_locale);

#endif // SOURCE_HPP
