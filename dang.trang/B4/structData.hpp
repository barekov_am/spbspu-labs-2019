#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP

#include <string>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

bool lessThan(const DataStruct &a, const DataStruct &b);
DataStruct getDataFromLine();
void outputData(DataStruct &data);
bool notInRange(const DataStruct &data);

#endif // DATASTRUCT_HPP
