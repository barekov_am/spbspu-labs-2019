#include "structData.hpp"
#include <sstream>
#include <iostream>
#include <algorithm>

bool lessThan(const DataStruct &a, const DataStruct &b)
{
  if (a.key1 == b.key1)
  {
    if (a.key2 == b.key2)
    {
      return (a.str.size() < b.str.size());
    }
    return (a.key2 < b.key2);
  }
  return (a.key1 < b.key1);
}

DataStruct getDataFromLine()
{
  std::string line;
  std::getline(std::cin, line);
  std::stringstream stream(line);

  DataStruct data;
  stream >> data.key1;

  stream.ignore(std::numeric_limits<std::streamsize>::max(), ',');
  stream >> data.key2;

  stream.ignore(std::numeric_limits<std::streamsize>::max(), ',');
  std::getline(stream, data.str);

  if (stream.fail())
  {
    throw std::invalid_argument("invalid input");
  }
  return data;
}

void outputData(DataStruct &data)
{
  std::cout << data.key1 << "," << data.key2 << "," << data.str << std::endl;
}

bool notInRange(const DataStruct &data)
{
  return !((data.key1 >= -5 && data.key1 <= 5) && (data.key2 >= -5 && data.key2 <= 5));
}
