#include <boost/test/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

const double INACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testForRectangle)

BOOST_AUTO_TEST_CASE(widthAndHightAfterMoveToPoint)
{
  dang::Rectangle rectangle(5, 5, { 3, 4 });
  rectangle.move({ 2, 2 });
  BOOST_CHECK_CLOSE(5, rectangle.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(5, rectangle.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(widthAndHightAfterMove)
{
  dang::Rectangle rectangle(7, 7, { 3, 5 });
  rectangle.move(2, 2);
  BOOST_CHECK_CLOSE(7, rectangle.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(7, rectangle.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterMoveToPoint)
{
  dang::Rectangle rectangle(7, 7, { 3, 5 });
  double area = rectangle.getArea();
  rectangle.move({ 2, 2 });
  BOOST_CHECK_CLOSE(area, rectangle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterMove)
{
  dang::Rectangle rectangle(7, 7, { 3, 5 });
  double area = rectangle.getArea();
  rectangle.move(3, 3);
  BOOST_CHECK_CLOSE(area, rectangle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterScaleOfRectangle)
{
  dang::Rectangle rectangle(4, 3, { 1, 5 });
  const double areaBeforeScale = rectangle.getArea();
  const double testScale = 5;
  rectangle.scale(testScale);
  BOOST_CHECK_CLOSE(areaBeforeScale * testScale * testScale, rectangle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidArgumentsInRectangle)
{
  BOOST_CHECK_THROW(dang::Rectangle rectangle(5, -9, { 1, 6 }), std::invalid_argument);
  BOOST_CHECK_THROW(dang::Rectangle rectangle(-9, 6, { 1, 6 }), std::invalid_argument);
  dang::Rectangle rectangle(3, 4, { 3, 4 });
  BOOST_CHECK_THROW(rectangle.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(checkingRotate)
{
  dang::Rectangle rectangle(7, 6, { 4, 5 });
  double area = rectangle.getArea();
  rectangle.rotate(40);
  BOOST_CHECK_CLOSE(area, rectangle.getArea(), INACCURACY);

}

BOOST_AUTO_TEST_SUITE_END()
