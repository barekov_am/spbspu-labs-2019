#include <boost/test/unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

const double INACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testForCompositeShape)

BOOST_AUTO_TEST_CASE(widthAndHeightAfterMove)
{
  dang::Circle circle(5, { 3, 4 });
  dang::Rectangle rectangle(6, 2, { 5, 6 });
  dang::Triangle triangle({ 4, 6 }, { 5, 3 }, { 4, 9 });
  dang::CompositeShape compositeShape(std::make_shared<dang::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<dang::Circle>(circle));
  compositeShape.add(std::make_shared<dang::Triangle>(triangle));

  dang::rectangle_t frameBeforeMove = compositeShape.getFrameRect();
  double area = compositeShape.getArea();
  compositeShape.move({ 4, 5 });

  BOOST_CHECK_CLOSE(frameBeforeMove.height, compositeShape.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMove.width, compositeShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(area, compositeShape.getArea(), INACCURACY);

  compositeShape.move(4, 5);

  BOOST_CHECK_CLOSE(frameBeforeMove.height, compositeShape.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMove.width, compositeShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(area, compositeShape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(correctOfAddAndRemove)
{
  dang::Circle circle(5, { 3, 4 });
  dang::Rectangle rectangle(8, 6, { 4, 5 });
  dang::CompositeShape compositeShape(std::make_shared<dang::Circle>(circle));
  double areaBeforeAdd = compositeShape.getArea();

  dang::Shape::shapePtr shape = std::make_shared<dang::Rectangle>(rectangle);

  compositeShape.add(shape);
  BOOST_CHECK_CLOSE(areaBeforeAdd + rectangle.getArea(), compositeShape.getArea(), INACCURACY);
 
  compositeShape.remove(shape);
  BOOST_CHECK_CLOSE(areaBeforeAdd, compositeShape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(outOfRangeException)
{
  dang::Rectangle rectangle(7, 7, { 5, 6 });
  dang::CompositeShape compositeShape(std::make_shared<dang::Rectangle>(rectangle));
  BOOST_CHECK_THROW(compositeShape[100], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(correctOfGetFrame)
{
  dang::Circle circle(3, { 6, 5 });
  dang::Rectangle rectangle(6, 6, { 6, 5 });
  dang::CompositeShape compositeShape(std::make_shared<dang::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<dang::Circle>(circle));
  dang::rectangle_t frame = rectangle.getFrameRect();

  BOOST_CHECK_CLOSE(frame.height, compositeShape.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(frame.width, compositeShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(frame.pos.x, compositeShape.getFrameRect().pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(frame.pos.y, compositeShape.getFrameRect().pos.y, INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterScale)
{
  dang::Circle circle(5, { 3, 4 });
  dang::Rectangle rectangle(6, 2, { 5, 6 });
  dang::Triangle triangle({ 4, 6 }, { 5, 3 }, { 4, 9 });
  dang::CompositeShape compositeShape(std::make_shared<dang::Circle>(circle));
  compositeShape.add(std::make_shared<dang::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<dang::Triangle>(triangle));

  double area = compositeShape.getArea();
  compositeShape.scale(2);

  BOOST_CHECK_CLOSE(area * 4, compositeShape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(exceptionThrow)
{
  BOOST_CHECK_THROW(dang::CompositeShape(nullptr), std::invalid_argument);

  dang::Circle circle(9, { 2, 2 });
  dang::Rectangle rectangle(5, 3, { 4, 2 });
  dang::Triangle triangle({ 4, 3 }, { 5, 6 }, { 3, 8 });
  dang::CompositeShape compositeShape(std::make_shared<dang::Circle>(circle));
  compositeShape.add(std::make_shared<dang::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<dang::Triangle>(triangle));
  dang::CompositeShape compositeShape1;

  BOOST_CHECK_THROW(compositeShape1.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(compositeShape.scale(-1), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.remove(10), std::out_of_range);
  BOOST_CHECK_THROW(compositeShape.remove(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(copyAndMove)
{
  dang::Rectangle rect1(4, 8, { 4, 1 });
  dang::CompositeShape compositeShape1(std::make_shared<dang::Rectangle>(rect1));
  dang::CompositeShape compositeShape4;
  BOOST_CHECK_NO_THROW(dang::CompositeShape compositeShape2(compositeShape1));
 
  dang::CompositeShape compositeShape2;

  BOOST_CHECK_NO_THROW(compositeShape2 = compositeShape1);
  BOOST_CHECK_NO_THROW(compositeShape2 = dang::CompositeShape(std::make_shared<dang::Rectangle>(rect1)));
  BOOST_CHECK_NO_THROW(dang::CompositeShape compositeShape3(std::move(compositeShape2)));
  BOOST_CHECK_NO_THROW(compositeShape4 = std::move(compositeShape1));
}

BOOST_AUTO_TEST_SUITE_END()
