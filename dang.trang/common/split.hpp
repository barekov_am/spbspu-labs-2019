#ifndef SPLIT_HPP
#define SPLIT_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"
#include <memory>

namespace dang
{
  bool checkCross(dang::rectangle_t shape1, dang::rectangle_t shape2);
  Matrix division(std::unique_ptr<dang::Shape::shapePtr []> &shapes, size_t size);
  Matrix division(CompositeShape &compositeShape);
}

#endif
