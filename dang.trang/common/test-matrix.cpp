#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "split.hpp"

const double INACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testForMatrix)

BOOST_AUTO_TEST_CASE(copyAndMove)
{
  dang::Rectangle rectangle(7, 6, { 4, 6 });
  dang::CompositeShape compositeShape(std::make_shared<dang::Rectangle>(rectangle));
  dang::Matrix matrix = dang::division(compositeShape);

  dang::Matrix matrix3 = matrix;

  BOOST_CHECK_EQUAL(matrix3.getLines(), 1);
  BOOST_CHECK_EQUAL(matrix3.getColumns(), 1);
  BOOST_CHECK_NO_THROW(dang::Matrix matrix1(matrix));
  BOOST_CHECK_NO_THROW(dang::Matrix matrix2(std::move(matrix)));

  dang::Matrix matrix4;
  dang::Matrix matrix5;

  BOOST_CHECK_NO_THROW(matrix4 = matrix);
  BOOST_CHECK_NO_THROW(matrix5 = std::move(matrix));
}

BOOST_AUTO_TEST_CASE(testForThrow)
{
  dang::Rectangle rectangle(2, 1, { 2, 4.5 });
  dang::Rectangle rectangle1(5, 2, { 3.5, 2 });
  dang::Circle circle(2, { 6, 4 });
  dang::Triangle triangle({ 3, 2 }, { 2, 0 }, { 4, 0 });
  dang::Triangle triangle1({ 7, 4 }, { 10, 3 }, { 10, 5 });
  dang::CompositeShape compositeShape(std::make_shared<dang::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<dang::Rectangle>(rectangle1));
  compositeShape.add(std::make_shared<dang::Circle>(circle));
  compositeShape.add(std::make_shared<dang::Triangle>(triangle));
  compositeShape.add(std::make_shared<dang::Triangle>(triangle1));

  dang::Matrix matrix = dang::division(compositeShape);
  BOOST_CHECK_THROW(matrix[10][10], std::out_of_range);
  BOOST_CHECK_NO_THROW(matrix[0][1]);
}

BOOST_AUTO_TEST_SUITE_END()
