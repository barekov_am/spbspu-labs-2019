#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

dang::Rectangle::Rectangle(double width, double height, const point_t &center) :
  width_(width),
  height_(height),
  pos_(center),
  angle_(0)
{
  if ((height_ <= 0) || (width_ <= 0))
  {
    throw std::invalid_argument("Width and height must be positive!");
  }
}

double dang::Rectangle::getArea() const
{
  return width_ * height_;
}

dang::rectangle_t dang::Rectangle::getFrameRect() const
{
  double width = width_ * std::fabs(cos((M_PI * angle_) / 180)) + height_ * std::fabs(sin((M_PI * angle_) / 180));
  double height = height_ * std::fabs(cos((M_PI * angle_) / 180)) + width_ * std::fabs(sin((M_PI * angle_) / 180));
  return { width, height, pos_ };
}

void dang::Rectangle::move(double mX, double mY)
{
  pos_.x += mX;
  pos_.y += mY;
}

void dang::Rectangle::move(const point_t& point)
{
  pos_ = point;
}

void dang::Rectangle::printInfo() const
{
  std::cout << "\n Width is:   " << width_ << std::endl;
  std::cout << "\n Height is:  " << height_ << std::endl;
  std::cout << "\n X center is:" << pos_.x << std::endl;
  std::cout << "\n Y center is:" << pos_.y << std::endl;
}

void dang::Rectangle::scale(double kooficent)
{
  if (kooficent <= 0)
  {
    throw std::invalid_argument("Width and height must be positive!");
  }
  height_ *= kooficent;
  width_ *= kooficent;
}

void dang::Rectangle::rotate(double angle)
{
  angle_ += angle;
  if (angle_ < 0)
  {
    angle_ = 360 + fmod(angle_, 360);
  }
}
