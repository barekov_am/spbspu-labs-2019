#include <boost/test/unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double INACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testForCircle)

BOOST_AUTO_TEST_CASE(radiusImmutabilityAfterMoveToPoint)
{
  dang::Circle circle(5, { 3, 3 });
  circle.move({ 0, 0 });
  BOOST_CHECK_CLOSE(5, circle.getFrameRect().width / 2, INACCURACY);
}

BOOST_AUTO_TEST_CASE(radiusImmutabilityAfterMove)
{
  dang::Circle circle(5, { 3, 3 });
  circle.move(2, 2);
  BOOST_CHECK_CLOSE(5, circle.getFrameRect().width / 2, INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterMoveToPoint)
{
  dang::Circle circle(7, { 3, 5 });
  double area = circle.getArea();
  circle.move({ 2, 2 });
  BOOST_CHECK_CLOSE(area, circle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterMove)
{
  dang::Circle circle(7, { 3, 5 });
  double area = circle.getArea();
  circle.move(4, 2);
  BOOST_CHECK_CLOSE(area, circle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterScaleOfCircle)
{
  dang::Circle circle(3, { 5, 4 });
  const double areaBeforeScale = circle.getArea();
  const double testScale = 5;
  circle.scale(testScale);
  BOOST_CHECK_CLOSE(areaBeforeScale * testScale * testScale, circle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidArgumentsInCircle)
{
  BOOST_CHECK_THROW(dang::Circle circle(-9, { 6, 5 }), std::invalid_argument);
  dang::Circle circle(4, { 4, 3 });
  BOOST_CHECK_THROW(circle.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
