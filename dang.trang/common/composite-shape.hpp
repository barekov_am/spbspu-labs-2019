#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include "shape.hpp"
#include <memory>

namespace dang
{
  class CompositeShape : public dang::Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &cs);
    CompositeShape(CompositeShape &&cs);
    CompositeShape(shapePtr shape);
    ~CompositeShape() = default;

    CompositeShape& operator= (const CompositeShape &rhs);
    CompositeShape& operator= (CompositeShape &&rhs);
    shapePtr operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double mX, double mY) override;
    void move(const point_t& point) override;
    void printInfo() const override;
    void scale(double kooficent) override;
    void add(shapePtr shape);
    void remove(size_t i);
    void remove(shapePtr shape);
    size_t getCount() const;
    void rotate(double angle) override;

  private:
    size_t countShapes;
    std::unique_ptr<shapePtr[]> shapes_;
  };
}

#endif
