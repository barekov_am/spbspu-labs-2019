#include "circle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>

dang::Circle::Circle(double rCircle, const point_t& cpoint) :
  rCircle_(rCircle),
  pos_(cpoint)
{
  if (rCircle_ <= 0)
  {
    throw std::invalid_argument("Radius must be positive");
  }
}

double dang::Circle::getArea() const
{
  return M_PI * rCircle_* rCircle_;
}

dang::rectangle_t dang::Circle::getFrameRect() const
{
  return { 2 * rCircle_, 2 * rCircle_, pos_ };
}

void dang::Circle::move(double mX, double mY)
{
  pos_.x += mX;
  pos_.y += mY;
}

void dang::Circle::move(const point_t& point)
{
  pos_ = point;
}

void dang::Circle::printInfo() const
{
  std::cout << "\n Area circle =" << getArea() << std::endl;
  std::cout << "\n Radius is :  " << rCircle_ << std::endl;
  std::cout << "\n Position_X_Y_" << pos_.x << " " << pos_.y << std::endl;
}

void dang::Circle::scale(double kooficent)
{
  if (kooficent <= 0)
  {
    throw std::invalid_argument("Radius must be positive!");
  }
  rCircle_ *= kooficent;
}

void dang::Circle::rotate(double /*angle*/)
{}
