#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

dang::Triangle::Triangle(const point_t& point0, const point_t& point1, const point_t& point2) :
  point0_(point0),
  point1_(point1),
  point2_(point2),
  center_({ (point0_.x + point1_.x + point2_.x) / 3, (point0_.y + point1_.y + point2_.y) / 3 })
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("Incorrect input");
  }
}

static double findDistance(const dang::point_t & point1, const dang::point_t & point2)
{
  const dang::point_t vector = { (point2.x - point1.x), (point2.y - point1.y) };
  return sqrt(vector.x * vector.x + vector.y * vector.y);
}

double dang::Triangle::getArea() const
{
  const double lengthVector1 = findDistance(point0_, point1_);
  const double lengthVector2 = findDistance(point0_, point2_);
  const double lengthVector3 = findDistance(point1_, point2_);
  const double halfPer = (lengthVector1 + lengthVector2 + lengthVector3) / 2;
  return sqrt(halfPer * (halfPer - lengthVector1) * (halfPer - lengthVector2) * (halfPer - lengthVector3));
}

dang::rectangle_t dang::Triangle::getFrameRect() const
{
  const double maxX = std::max({ point0_.x, point1_.x, point2_.x });
  const double maxY = std::max({ point0_.y, point1_.y, point2_.y });
  const double minX = std::min({ point0_.x, point1_.x, point2_.x });
  const double minY = std::min({ point0_.y, point1_.y, point2_.y });
  const double height = maxY - minY;
  const double width = maxX - minX;
  const point_t center = { minX + width / 2, minY + height / 2 };
  return { width, height, center };
}

void dang::Triangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;

  point0_.x += dx;
  point0_.y += dy;

  point1_.x += dx;
  point1_.y += dy;

  point2_.x += dx;
  point2_.y += dy;
}

void dang::Triangle::move(const point_t & center)
{
  move((center.x - center_.x), (center.y - center_.y));
}

void dang::Triangle::printInfo() const
{
  std::cout << "the point on the x: " << center_.x << "\n";
  std::cout << "the point on the y: " << center_.y << "\n";
}

double scaleOne(double point, double center, const double kooficent)
{
  return (center + point * kooficent - kooficent * center);
}

void dang::Triangle::scale(double kooficent)
{
  if (kooficent <= 0)
  {
    throw std::invalid_argument("Kooficent must be positive!");
  }
  point0_.x = scaleOne(point0_.x, center_.x, kooficent);
  point0_.y = scaleOne(point0_.y, center_.y, kooficent);
  point1_.x = scaleOne(point1_.x, center_.x, kooficent);
  point1_.y = scaleOne(point1_.y, center_.y, kooficent);
  point2_.x = scaleOne(point2_.x, center_.x, kooficent);
  point2_.y = scaleOne(point2_.y, center_.y, kooficent);
}

dang::point_t rotateOne(dang::point_t point, double angle)
{
  double x = point.x * cos(M_PI * angle / 180) - point.y * sin(M_PI * angle / 180);
  double y = point.y * cos(M_PI * angle / 180) + point.x * sin(M_PI * angle / 180);
  return { x, y };
}

void dang::Triangle::rotate(double angle)
{
  if (angle < 0)
  {
    angle = (360 + fmod(angle, 360));
  }
  point0_ = rotateOne(point0_, angle);
  point1_ = rotateOne(point1_, angle);
  point2_ = rotateOne(point2_, angle);
}
