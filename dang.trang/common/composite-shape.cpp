#include "composite-shape.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>

dang::CompositeShape::CompositeShape() :
  countShapes(0),
  shapes_(nullptr)
{}

dang::CompositeShape::CompositeShape(const dang::CompositeShape& cs) :
  countShapes(cs.countShapes),
  shapes_(std::make_unique<shapePtr []>(cs.countShapes))
{
  for (size_t i = 0; i < countShapes; i++)
  {
    shapes_[i] = cs.shapes_[i];
  }
}

dang::CompositeShape::CompositeShape(dang::CompositeShape&& cs) :
  countShapes(cs.countShapes),
  shapes_(std::move(cs.shapes_))
{
  cs.countShapes = 0;
}

dang::CompositeShape::CompositeShape(shapePtr shape) :
  countShapes(1),
  shapes_(std::make_unique<shapePtr []>(countShapes))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Invalid! Shape is nullptr!");
  }
  shapes_[0] = shape;
}

dang::CompositeShape& dang::CompositeShape::operator = (const dang::CompositeShape &rhs)
{
  if (this != &rhs)
  {
    countShapes = rhs.countShapes;
    std::unique_ptr<shapePtr []> temp(std::make_unique<shapePtr []>(rhs.countShapes));
    for (size_t i = 0; i < countShapes; i++)
    {
      temp[i] = rhs.shapes_[i];
    }
    shapes_.swap(temp);
  }
  return *this;
}

dang::CompositeShape& dang::CompositeShape::operator = (CompositeShape&& rhs)
{
  if (this != &rhs)
  {
    countShapes = rhs.countShapes;
    shapes_ = std::move(rhs.shapes_);
    rhs.countShapes = 0;
  }
  return *this;
}

dang::CompositeShape::shapePtr dang::CompositeShape::operator [](size_t index) const
{
  if (index >= countShapes)
  {
    throw std::out_of_range("Error! Do not draw figure!");
  }
  return shapes_[index];
}

void dang::CompositeShape::add(shapePtr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape is nullptr!!! it is error");
  }
  countShapes++;
  std::unique_ptr<shapePtr[]> temp(std::make_unique<shapePtr []>(countShapes));
  for (size_t i = 0; i < countShapes - 1; i++)
  {
    temp[i] = shapes_[i];
  }
  temp[countShapes - 1] = shape;
  shapes_ = std::move(temp);
}

void dang::CompositeShape::remove(shapePtr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("You give shape nullptr!");
  }
  size_t i = 0;
  while ((i < countShapes) && (shape != shapes_[i]))
  {
    i++;
  }
  if (i == countShapes)
  {
    throw std::invalid_argument("There is no such figure in the array");
  }
  remove(i);
}

void dang::CompositeShape::remove(size_t i)
{
  if (i > countShapes - 1)
  {
    throw std::out_of_range("The index is bigger than number of figures!");
  }

  {
    for (size_t j = i; j < countShapes - 1; j++)
    {
      shapes_[j] = shapes_[j + 1];
    }
    countShapes--;
  }
}

void dang::CompositeShape::move(double mX, double mY)
{
  for (size_t i = 0; i < countShapes; i++)
  {
    shapes_[i]->move(mX, mY);
  }
}

void dang::CompositeShape::move(const dang::point_t& point)
{
  point_t shift = { point.x - getFrameRect().pos.x, point.y - getFrameRect().pos.y };
  move(shift.x, shift.y);
}

size_t dang::CompositeShape::getCount() const
{
  return countShapes;
}

double dang::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < countShapes; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

void dang::CompositeShape::scale(double kooficent)
{
  point_t point = getFrameRect().pos;
  for (size_t i = 0; i < countShapes; i++)
  {
    point_t shift = { shapes_[i]->getFrameRect().pos.x - point.x, shapes_[i]->getFrameRect().pos.y - point.y };
    shapes_[i]->move(shift.x * (kooficent - 1), shift.y * (kooficent - 1));
    shapes_[i]->scale(kooficent);
  }
}

dang::point_t getBotAndTop(dang::rectangle_t frame, int kooficent)
{
  double x = frame.pos.x + (kooficent * (frame.width / 2));
  double y = frame.pos.y + (kooficent * (frame.height / 2));
  return { x, y };
}

dang::rectangle_t dang::CompositeShape::getFrameRect() const
{

  if (countShapes == 0)
  {
    throw std::logic_error("we do not have shape in array");
  }

  point_t maxXAndY = getBotAndTop(shapes_[0]->getFrameRect(), 1);
  point_t minXAndY = getBotAndTop(shapes_[0]->getFrameRect(), (-1));
  for (size_t i = 1; i < countShapes; i++)
  {
    maxXAndY.x = std::max(getBotAndTop(shapes_[i]->getFrameRect(), 1).x, maxXAndY.x);
    maxXAndY.y = std::max(getBotAndTop(shapes_[i]->getFrameRect(), 1).y, maxXAndY.y);
    minXAndY.x = std::min(getBotAndTop(shapes_[i]->getFrameRect(), (-1)).x, minXAndY.x);
    minXAndY.y = std::min(getBotAndTop(shapes_[i]->getFrameRect(), (-1)).y, minXAndY.y);
  }
  double height = maxXAndY.y - minXAndY.y;
  double width = maxXAndY.x - minXAndY.x;
  point_t center = { minXAndY.x + width / 2, minXAndY.y + height / 2 };
  return { width, height, center };
}

void dang::CompositeShape::printInfo() const
{
  for (size_t i = 0; i < countShapes; i++)
  {
    std::cout << "Center={" << shapes_[i]->getFrameRect().pos.x << ", " << shapes_[i]->getFrameRect().pos.y << "}\n";
  }
}
void dang::CompositeShape::rotate(double angle)
{
  const double cos = std::cos((2 * M_PI * angle) / 360);
  const double sin = std::sin((2 * M_PI * angle) / 360);

  const point_t compCentre = getFrameRect().pos;

  for (size_t i = 0; i < countShapes; i++)
  {
    const point_t center = shapes_[i]->getFrameRect().pos;
    const double dx = (center.x - compCentre.x) * cos - (center.y - compCentre.y) * sin;
    const double dy = (center.x - compCentre.x) * sin + (center.y - compCentre.y) * cos;
    shapes_[i]->move({ compCentre.x + dx, compCentre.y + dy });
    shapes_[i]->rotate(angle);
  }
}
