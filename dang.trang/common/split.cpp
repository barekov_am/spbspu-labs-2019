#include "split.hpp"
#include <cmath>

bool dang::checkCross(dang::rectangle_t shape1, dang::rectangle_t shape2)
{
  const double distanceBetweenCentersX = fabs(shape1.pos.x - shape2.pos.x);
  const double distanceBetweenCentersY = fabs(shape1.pos.y - shape2.pos.y);

  const double lengthX = (shape1.width + shape2.width) / 2;
  const double lengthY = (shape1.height + shape2.height) / 2;

  const bool firstCondition = (distanceBetweenCentersX < lengthX);
  const bool secondCondition = (distanceBetweenCentersY < lengthY);

  return firstCondition && secondCondition;

}

dang::Matrix dang::division(std::unique_ptr<dang::Shape::shapePtr []> &shapes, size_t size)
{
  Matrix matrix;
  size_t lines = 0;
  size_t columns = 0;

  for (size_t i = 0; i < size; i++)
  {
    for (size_t j = 0; j < matrix.getLines(); j++)
    {
      for (size_t k = 0; k < matrix.getColumns(); k++)
      {
        if (matrix[j][k] == nullptr)
        {
          lines = j;
          columns = k;
          break;
        }

        if (checkCross(matrix[j][k]->getFrameRect(), shapes[i]->getFrameRect()))
        {
          lines = j + 1;
          columns = 0;
          break;
        }

        lines = j;
        columns = k + 1;
      }

      if (lines == j)
      {
        break;
      }
    }

    matrix.add(shapes[i], lines, columns);
  }
  return matrix;
}

dang::Matrix dang::division(dang::CompositeShape &compositeShape)
{
  std::unique_ptr<Shape::shapePtr[]> temp(std::make_unique<Shape::shapePtr []>(compositeShape.getCount()));
  for (size_t i = 0; i < compositeShape.getCount(); i++)
  {
    temp[i] = compositeShape[i];
  }
  Matrix matrix = division(temp, compositeShape.getCount());
  return matrix;
}
