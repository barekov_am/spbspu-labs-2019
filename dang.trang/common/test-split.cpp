#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "split.hpp"

BOOST_AUTO_TEST_SUITE(splitMetodsTests)

BOOST_AUTO_TEST_CASE(splitTest)
{
  dang::Rectangle rectangle(2, 1, { 2, 4.5 });
  dang::Rectangle rectangle1(5, 2, { 3.5, 2 });
  dang::Circle circle(2, { 6, 4 });

  dang::CompositeShape compositeShape(std::make_shared<dang::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<dang::Rectangle>(rectangle1));
  compositeShape.add(std::make_shared<dang::Circle>(circle));

  dang::Matrix matrix = dang::division(compositeShape);

  BOOST_CHECK_EQUAL(matrix.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 2);
  BOOST_CHECK_EQUAL(matrix[0][0], compositeShape[0]);
  BOOST_CHECK_EQUAL(matrix[0][1], compositeShape[1]);
}

BOOST_AUTO_TEST_CASE(crossingTest)
{
  dang::Rectangle rectangle(2, 1, { 2, 4.5 });
  dang::Rectangle rectangle1(5, 2, { 3.5, 2 });
  dang::Circle circle(2, { 6, 4 });

  BOOST_CHECK(!dang::checkCross(rectangle.getFrameRect(), rectangle1.getFrameRect()));
  BOOST_CHECK(!dang::checkCross(rectangle.getFrameRect(), circle.getFrameRect()));
  BOOST_CHECK(dang::checkCross(rectangle1.getFrameRect(), circle.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
