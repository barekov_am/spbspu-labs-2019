#include "tasks.hpp"
#include <iostream>
#include <string>
#include <sstream>
#include "QueueWithPriority.hpp"

void executeTask1()
{
  QueueWithPriority<std::string> queue;
  std::string line;

  while (std::getline(std::cin, line))
  {
    std::stringstream stream(line);
    std::string command;
    stream >> command;

    if (command == "add")
    {
      std::string priority;
      stream >> priority;

      std::string data;
      stream.ignore();
      std::getline(stream, data);

      while (data[0] == ' ' || data[0] == '\t')
      {
        data.erase(0, 1);
      }

      if (data.empty())
      {
        std::cout << "<INVALID COMMAND>" << std::endl;
      }
      else if (priority == "high")
      {
        queue.put_Element(data, ElementPriority::HIGH);
      }
      else if (priority == "normal")
      {
        queue.put_Element(data, ElementPriority::NORMAL);
      }
      else if (priority == "low")
      {
        queue.put_Element(data, ElementPriority::LOW);
      }
      else
      {
        std::cout << "<INVALID COMMAND>" << std::endl;
      }
    }
    else if (command == "accelerate")
    {
      queue.Accelerate();
    }
    else if (command == "get")
    {
      if (queue.empty())
      {
        std::cout << "<EMPTY>" << std::endl;
      }
      else
      {
        std::cout << queue.get_Element() << std::endl;
      }
    }
    else
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }

    if (std::cin.eof())
    {
      break;
    }
  }
}
