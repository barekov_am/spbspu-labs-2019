#ifndef QUEUE_WITH_PRIO_HPP
#define QUEUE_WITH_PRIO_HPP

#include <list>
#include <stdexcept>

typedef enum
{
  LOW,
  NORMAL,
  HIGH
} ElementPriority;

template <typename T>
class QueueWithPriority
{
public:
  QueueWithPriority();
  ~QueueWithPriority() = default;

  void put_Element(const T &element, ElementPriority priority);
  T get_Element();
  void Accelerate();
  bool empty() const;

private:
  std::list<T> queue_;
  typename std::list<T>::iterator highSeperator, normalSeperator;
};

template <typename T>
QueueWithPriority<T>::QueueWithPriority()
{
  T dummyElement;
  highSeperator = queue_.insert(queue_.end(), dummyElement);
  normalSeperator = queue_.insert(queue_.end(), dummyElement);
}

template <typename T>
void QueueWithPriority<T>::put_Element(const T &element, ElementPriority priority)
{
  switch (priority)
  {
  case 2:
    queue_.insert(highSeperator, element);
    break;
  case 1:
    queue_.insert(normalSeperator, element);
    break;
  case 0:
    queue_.insert(queue_.end(), element);
    break;
  }
}

template <typename T>
T QueueWithPriority<T>::get_Element()
{
  if (empty())
  {
    throw std::invalid_argument("List is empty");
  }
  if (queue_.begin() != highSeperator)
  {
    T frontElem = queue_.front();
    queue_.pop_front();
    return frontElem;
  }
  else if (std::next(highSeperator) != normalSeperator)
  {
    T frontElem = *std::next(highSeperator);
    queue_.erase(std::next(highSeperator));
    return frontElem;
  }
  else
  {
    T frontElem = *std::next(normalSeperator);
    queue_.erase(std::next(normalSeperator));
    return frontElem;
  }
}

template <typename T>
void QueueWithPriority<T>::Accelerate()
{
  auto it = std::next(normalSeperator);
  while (it != queue_.end())
  {
    queue_.insert(highSeperator, *it);
    it++;
    queue_.erase(std::prev(it));
  }
}

template <typename T>
bool QueueWithPriority<T>::empty() const
{
  if ((queue_.begin() == highSeperator) && (std::next(highSeperator) == normalSeperator)
    && (std::next(normalSeperator) == queue_.end()))
  {
    return true;
  }
  else
  {
    return false;
  }
}

#endif
