#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

void actions(dang::Shape* usingShape)
{
  if (usingShape == nullptr)
  {
    throw std::invalid_argument("In actions(), usingShape now is nullptr");
  }
  usingShape->printInfo();
  std::cout << "Move shape for 5 right, 6 up" << "\n";
  usingShape->move(5, 6);
  usingShape->printInfo();
  std::cout << "Move shape to (4,7)" << "\n";
  usingShape->move({ 4, 7 });
  usingShape->printInfo();
  usingShape->scale(2);
  std::cout << "Area after scale= " << usingShape->getArea() << "\n";
}

int main()
{
  dang::Circle circle(5, { 3, 4 });
  actions(&circle);

  dang::Rectangle rectangle(9, 7, { 4, 5 });
  actions(&rectangle);

  dang::Triangle triangle({ 4, 5 }, { 3, 5 }, { 6, 7 });
  actions(&triangle);

  return 0;
}
