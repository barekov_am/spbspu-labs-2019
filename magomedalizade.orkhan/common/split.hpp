#ifndef SPLIT_HPP
#define SPLIT_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace magomedalizade
{
  Matrix split(const magomedalizade::shapes_array &, size_t);
  Matrix split(const CompositeShape &);
  bool intersect(const rectangle_t &, const rectangle_t &);
}

#endif
