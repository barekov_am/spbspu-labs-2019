#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace magomedalizade
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    point_t pos;
    double width;
    double height;
  };

  magomedalizade::point_t rotatePoint(const magomedalizade::point_t& centre, const magomedalizade::point_t& point, double angle);
}

#endif
