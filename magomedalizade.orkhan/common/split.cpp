#include "split.hpp"
#include <cmath>

magomedalizade::Matrix magomedalizade::split(const magomedalizade::shapes_array &shapes, size_t count)
{
  Matrix matrix;
  size_t lines = 0;
  size_t columns = 0;

  for (size_t i = 0; i < count; i++)
  {
    for (size_t j = 0; j < matrix.getLines(); j++)
    {
      for (size_t k = 0; k < matrix.getColumns(); k++)
      {
        if (matrix[j][k] == nullptr)
        {
          lines = j;
          columns = k;
          break;
        }

        if (intersect(matrix[j][k]->getFrameRect(), shapes[i]->getFrameRect()))
        {
          lines = j + 1;
          columns = 0;
          break;
        }

        lines = j;
        columns = k + 1;
      }

      if (lines == j)
      {
        break;
      }
    }

    matrix.add(shapes[i], lines, columns);
  }

  return matrix;
}

magomedalizade::Matrix magomedalizade::split(const magomedalizade::CompositeShape &compositeShape)
{
  return split(compositeShape.getFigures(), compositeShape.getSize());
}

bool magomedalizade::intersect(const magomedalizade::rectangle_t &firstShape, const magomedalizade::rectangle_t &secondShape)
{
  const double distanceX = std::abs(firstShape.pos.x - secondShape.pos.x);
  const double distanceY = std::abs(firstShape.pos.y - secondShape.pos.y);

  const double lengthX = (firstShape.width + secondShape.width) / 2;
  const double lengthY = (firstShape.height + secondShape.height) / 2;

  const bool firstCondition = (distanceX < lengthX);
  const bool secondCondition = (distanceY < lengthY);

  return firstCondition && secondCondition;
}
