#define _USE_MATH_DEFINES

#include "base-types.hpp"
#include <cmath>

magomedalizade::point_t magomedalizade::rotatePoint(const magomedalizade::point_t& center, const magomedalizade::point_t& point, double angle)
{
  double angle_ = angle * M_PI / 180;
  double x = center.x + (point.x - center.x) * cos(angle_) - (point.y - center.y) * sin(angle_);
  double y = center.y + (point.y - center.y) * cos(angle_) + (point.x - center.x) * sin(angle_);

  return {x, y};
}
