#include "triangle.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>
#include <algorithm>

magomedalizade::Triangle::Triangle(const point_t &vertexA, const point_t &vertexB, const point_t &vertexC):
  vertexA_(vertexA),
  vertexB_(vertexB),
  vertexC_(vertexC),
  center_ {(vertexA.x + vertexB.x + vertexC.x) / 3 , (vertexA.y + vertexB.y + vertexC.y) / 3}
{
  if (getArea() == 0)
  {
    throw std::invalid_argument("Vertexes can't match, lie on the same line");
  }
}

double magomedalizade::Triangle::getArea() const
{
  return (std::fabs((vertexA_.x - vertexC_.x) * (vertexB_.y - vertexC_.y) - (vertexB_.x - vertexC_.x) * (vertexA_.y - vertexC_.y)) / 2);
}

void magomedalizade::Triangle::printFeatures() const
{
  std::cout << "Center = " << center_.x << ";" << center_.y << " vertexA = " << vertexA_.x << ";" << vertexA_.y
    << " vertexB = " << vertexB_.x << ";" << vertexB_.y << " vertexC = " << vertexC_.x << ";" << vertexC_.y << "\n";
}

magomedalizade::rectangle_t magomedalizade::Triangle::getFrameRect() const
{
  const double maxX = std::max({vertexA_.x, vertexB_.x, vertexC_.x});
  const double minX = std::min({vertexA_.x, vertexB_.x, vertexC_.x});
  const double width = maxX - minX;
  const double maxY = std::max({vertexA_.y, vertexB_.y, vertexC_.y});
  const double minY = std::min({vertexA_.y, vertexB_.y, vertexC_.y});
  const double height = maxY - minY;
  const point_t pos = {minX + width / 2, minY + height / 2};

  return {pos, width, height};
}

void magomedalizade::Triangle::move(const point_t &newCenter)
{
  move((newCenter.x - center_.x), (newCenter.y - center_.y));
}

void magomedalizade::Triangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;

  vertexA_.x += dx;
  vertexA_.y += dy;

  vertexB_.x += dx;
  vertexB_.y += dy;

  vertexC_.x += dx;
  vertexC_.y += dy;
}

void magomedalizade::Triangle::scale(double ratio)
{
  if (ratio <= 0)
  {
    throw std::invalid_argument("Scale ratio <=0, please swap");
  }
  else
  {
    vertexA_.x = center_.x + (vertexA_.x - center_.x) * ratio;
    vertexA_.y = center_.y + (vertexA_.y - center_.y) * ratio;

    vertexB_.x = center_.x + (vertexB_.x - center_.x) * ratio;
    vertexB_.y = center_.y + (vertexB_.y - center_.y) * ratio;

    vertexC_.x = center_.x + (vertexC_.x - center_.x) * ratio;
    vertexC_.y = center_.y + (vertexC_.y - center_.y) * ratio;
  }
}

  void magomedalizade::Triangle::rotate(double angle)
  {
    vertexA_ = magomedalizade::rotatePoint(center_, vertexA_, angle);
    vertexB_ = magomedalizade::rotatePoint(center_, vertexB_, angle);
    vertexC_ = magomedalizade::rotatePoint(center_, vertexC_, angle);
  }
