#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "triangle.hpp"

const double EPS = 0.01;
BOOST_AUTO_TEST_SUITE(TriangleTestSuite)

BOOST_AUTO_TEST_CASE(moveTriangleConstant)
{
  magomedalizade::Triangle triangleTest({0, 0}, {10, 10}, {20, -10});
  const magomedalizade::rectangle_t beforeFrame = triangleTest.getFrameRect();
  const double beforeArea = triangleTest.getArea();

  triangleTest.move(5, 5);
  BOOST_CHECK_CLOSE(beforeFrame.width, triangleTest.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(beforeFrame.height, triangleTest.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(beforeArea, triangleTest.getArea(), EPS);

  triangleTest.move({20, 20});
  BOOST_CHECK_CLOSE(beforeFrame.width, triangleTest.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(beforeFrame.height, triangleTest.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(beforeArea, triangleTest.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(scaleTriangleIncreaseArea)
{
  magomedalizade::Triangle triangleTest({0, 0}, {10, 10}, {20, -10});
  const double beforeArea = triangleTest.getArea();
  const double scaleFactor = 3;

  triangleTest.scale(scaleFactor);
  BOOST_CHECK_CLOSE(beforeArea * scaleFactor * scaleFactor, triangleTest.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(scaleTriangleDecreaseArea)
{
  magomedalizade::Triangle triangleTest({0, 0}, {10, 10}, {20, -10});
  const double beforeArea = triangleTest.getArea();
  const double scaleFactor = 0.5;

  triangleTest.scale(scaleFactor);
  BOOST_CHECK_CLOSE(beforeArea * scaleFactor * scaleFactor, triangleTest.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(constantAreaAfterRotation)
{
  magomedalizade::Triangle triangleTest({0, 0}, {10, 10}, {20, -10});
  const double testArea = triangleTest.getArea();
  const double angle = 90;

  triangleTest.rotate(angle);
  BOOST_CHECK_CLOSE(testArea, triangleTest.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(invalidTriangleArguments)
{
  BOOST_CHECK_THROW(magomedalizade::Triangle triangleTest({0, 0}, {0, 0}, {10, 10}), std::invalid_argument);
  BOOST_CHECK_THROW(magomedalizade::Triangle triangleTest({0, 0}, {10, 10}, {10, 10}), std::invalid_argument);
  BOOST_CHECK_THROW(magomedalizade::Triangle triangleTest({0, 0}, {0, 0}, {0, 0}), std::invalid_argument);

  magomedalizade::Triangle triangleTest({0, 0}, {10, 10}, {20, -10});
  BOOST_CHECK_THROW(triangleTest.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
