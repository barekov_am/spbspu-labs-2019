#include <vector>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <boost/scoped_array.hpp>
#include "detail.hpp"

void executeTask2(int args, char *argv[]) {
  if (args != 3) {
    throw std::invalid_argument("Incorrect arguments for task 2");
  }

  std::ifstream inp(argv[2], std::ios_base::binary);

  if (!inp.good()) {
    throw std::invalid_argument("Incorrect filename for task 2");
  }

  size_t size = 0;
  size_t capacity = 1;
  boost::scoped_array <char> array(new char[capacity]);
  
  while (!inp.eof()) {
    if (size == capacity) {
      capacity *= 2;
      boost::scoped_array <char> newarray(new char[capacity]);

      if (!newarray) {
        throw std::invalid_argument("No more memory left");
      }

      for (size_t i = 0; i < size; i++){
        newarray[i] = array[i];
      }

      array.reset();
      array.swap(newarray);
    }
    
    inp >> std::noskipws >> array[size];
    size++;
  }

  if (size <= 1) {
    return;
  }

  std::vector<char> resultvector(&array[0], &array[size-1]);

  std::cout.write(&resultvector[0], resultvector.size());

}
