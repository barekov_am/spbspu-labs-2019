#include "detail.hpp"

detail::sortType_t getSortType(const std::string &param) {
  if (param == "ascending") {
    return detail::sortType_t::ascending;
  } else if (param == "descending") {
    return detail::sortType_t::descending;
  } else {
    throw std::invalid_argument("Incorrect sort type parameter");
  }
}
