#include <iostream>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

void printArea(const antufiev::Shape &shape)
{
  std::cout << "Area:" << shape.getArea() << std::endl;
}

void printFrameRect(const antufiev::Shape &shape)
{
  antufiev::rectangle_t frameRectangle = shape.getFrameRect();
  std::cout << "Width:" << frameRectangle.width << std::endl;
  std::cout << "Height:" << frameRectangle.height << std::endl;
  std::cout << "Center:" << frameRectangle.pos.x << " " << frameRectangle.pos.y << std::endl;
  std::cout << std::endl;
}

int main()
{
  const double x = 5.0;
  const double coef = 2;
  antufiev::point_t point1_center{x * 2, x * 2};
  antufiev::point_t point2_center{x * 4, x * 4};
  antufiev::Rectangle rectangle({point1_center}, x, x);
  antufiev::Circle circle(point1_center, x);
  const antufiev::point_t point1{1.0, 1.0};
  const antufiev::point_t point2{2.0, 1.0};
  const antufiev::point_t point3{1.0, 2.0};
  antufiev::Triangle triangle(point1, point2, point3);

  circle.printInfo();
  printArea(circle);
  circle.scale(coef);
  printArea(circle);
  printFrameRect(circle);
  circle.move(x * 3, x * 3);
  circle.printInfo();
  circle.move(point2_center);

  rectangle.printInfo();
  printArea(rectangle);
  rectangle.scale(coef);
  printArea(rectangle);
  printFrameRect(rectangle);
  rectangle.move(x, x);
  rectangle.printInfo();
  rectangle.move(point2_center);

  triangle.printInfo();
  printArea(triangle);
  printFrameRect(triangle);
  triangle.scale(coef);
  triangle.printInfo();
  printArea(triangle);
  printFrameRect(triangle);
  triangle.move(x * 3, x * 3);
  triangle.printInfo();
  triangle.move(point2_center);

  return 0;
}
