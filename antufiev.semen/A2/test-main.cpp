#define BOOST_TEST_MODULE A2

#include <iostream>
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(a2testSuite)

  const double ACCURACY = 0.00001;

  void checkMoveInFigure(antufiev::Shape &figure)
  {
    const antufiev::rectangle_t before_rectangle = figure.getFrameRect();
    const double before_area = figure.getArea();
    figure.move(3, 3);
    antufiev::rectangle_t after_rectangle = figure.getFrameRect();
    double after_area = figure.getArea();
    BOOST_CHECK_EQUAL(before_rectangle.height, after_rectangle.height);
    BOOST_CHECK_EQUAL(before_rectangle.width, after_rectangle.width);
    BOOST_CHECK_CLOSE(before_area, after_area, ACCURACY);
    figure.move({3, 3});
    after_rectangle = figure.getFrameRect();
    after_area = figure.getArea();
    BOOST_CHECK_EQUAL(before_rectangle.height, after_rectangle.height);
    BOOST_CHECK_EQUAL(before_rectangle.width, after_rectangle.width);
    BOOST_CHECK_CLOSE(before_area, after_area, ACCURACY);
  }

  void checkScaleInFigure(antufiev::Shape &figure, double coef)
  {
    const double before_area = figure.getArea();
    const antufiev::rectangle_t before_rectangle = figure.getFrameRect();
    figure.scale(coef);
    const antufiev::rectangle_t after_rectangle = figure.getFrameRect();
    BOOST_CHECK_CLOSE(before_rectangle.height * coef, after_rectangle.height, ACCURACY);
    BOOST_CHECK_CLOSE(before_rectangle.width * coef, after_rectangle.width, ACCURACY);
    BOOST_CHECK_CLOSE(before_area * coef * coef, figure.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(circleCheckParametersAfterMoving)
  {
    antufiev::Circle testCircle({5, 5}, 4);
    checkMoveInFigure(testCircle);
  }

  BOOST_AUTO_TEST_CASE(circleCheckAreaAfterScale)
  {
    antufiev::Circle circle({5, 5}, 2);
    checkScaleInFigure(circle, 2.0);
    checkScaleInFigure(circle, 0.2);
  }

  BOOST_AUTO_TEST_CASE(circleExceptionHandling)
  {
    BOOST_CHECK_THROW(antufiev::Circle({1, 3}, -5), std::invalid_argument);
    BOOST_CHECK_THROW(antufiev::Circle({1,3}, 0), std::invalid_argument);
    antufiev::Circle testCircle({2, 2}, 2);
    BOOST_CHECK_THROW(testCircle.scale(0), std::invalid_argument);
    BOOST_CHECK_THROW(testCircle.scale(-1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(rectangleCheckParametersAfterMoving)
  {
    antufiev::Rectangle testRectangle({3, 5}, 7, 2);
    checkMoveInFigure(testRectangle);
  }

  BOOST_AUTO_TEST_CASE(rectangleCheckAreaAfterScale)
  {
    antufiev::Rectangle rectangle({5, 5}, 2, 2);
    checkScaleInFigure(rectangle, 2.0);
    checkScaleInFigure(rectangle, 0.2);
  }

  BOOST_AUTO_TEST_CASE(rectangleExceptionHandling)
  {
    BOOST_CHECK_THROW(antufiev::Rectangle({5,4}, 0, -1), std::invalid_argument);
    BOOST_CHECK_THROW(antufiev::Rectangle({5,4}, -1, 0), std::invalid_argument);
    antufiev::Rectangle testRectangle({4, 5}, 2, 3);
    BOOST_CHECK_THROW(testRectangle.scale(-1), std::invalid_argument);
    BOOST_CHECK_THROW(testRectangle.scale(0),std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(triangleCheckParametersAfterMoving)
  {
    antufiev::Triangle testTriangle{{1, 5}, {4, 2}, {2, 8}};
    checkMoveInFigure(testTriangle);
  }

  BOOST_AUTO_TEST_CASE(triangleCheckAreaAfterScale)
  {
   antufiev::Triangle triangle({1, 2}, {3, 7}, {10, 5});
   checkScaleInFigure(triangle, 2);
   checkScaleInFigure(triangle, 0.2);
  }

  BOOST_AUTO_TEST_CASE(triangleExceptionHandling)
  {
    BOOST_CHECK_THROW(antufiev::Triangle testTriangle({1, 1}, {1 ,1}, {1, 1}), std::invalid_argument);
    antufiev::Triangle testTriangle({1, 1}, {5 ,9}, {6, 3});
    BOOST_CHECK_THROW(testTriangle.scale(-1), std::invalid_argument);
    BOOST_CHECK_THROW(testTriangle.scale(0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END();
