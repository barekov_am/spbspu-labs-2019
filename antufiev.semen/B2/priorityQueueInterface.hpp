#ifndef B2_PRIORITY_QUEUE_INTERFACE_HPP
#define B2_PRIORITY_QUEUE_INTERFACE_HPP

#include <list>

template <typename T>
class QueueWithPriority
{
public:

  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  QueueWithPriority();

  void putElementToQueue(const T& element, ElementPriority priority);
  T getElementFromQueue();

  void accelerate();
  bool empty() const;

private:
  std::list<T> low;
  std::list<T> normal;
  std::list<T> high;
};

#endif //B2_PRIORITY_QUEUE_INTERFACE_HPP
