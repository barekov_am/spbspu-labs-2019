#include "circle.hpp"

#include <iostream>
#include <cmath>
#include <cassert>

Circle::Circle(const point_t &center, double radius):
  center_(center),
  radius_(radius)
{
  assert(radius_ >= 0.0);
}

double Circle::getArea() const
{
  return radius_ * M_PI * radius_;
}

rectangle_t Circle::getFrameRect() const
{
  return {2 * radius_, 2 * radius_, center_};
}

void Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Circle::move(const point_t &center)
{
  center_ = center;
}

void Circle::printInfo() const
{
  std::cout << "Circle:" << std::endl;
  std::cout << "Center:" << center_.x << " " << center_.y << std::endl;
  std::cout << "Radius:" << radius_ << std::endl;
  std::cout << std::endl;
}
