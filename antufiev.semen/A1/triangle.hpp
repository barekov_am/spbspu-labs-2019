#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

class Triangle: public Shape
{
  public:
    Triangle(point_t point1, point_t point2_, point_t point3_);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(double dx, double dy) override;
    void printInfo() const override;
    point_t getCenter() const;
  private:
    point_t point1_, point2_, point3_;
};

#endif //TRIANGLE_HPP
