#ifndef B3_CONTAINER_FACTORIAL_HPP
#define B3_CONTAINER_FACTORIAL_HPP

#include "iterator-factorial.hpp"

class ContainerFactorial
{
public:
  ContainerFactorial() = default;
  ~ContainerFactorial() = default;
  
  IteratorFactorial begin();
  IteratorFactorial end();
};

#endif //B3_CONTAINER_FACTORIAL_HPP
