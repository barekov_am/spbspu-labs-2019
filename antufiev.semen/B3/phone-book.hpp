#ifndef B3_PHONE_BOOK_HPP
#define B3_PHONE_BOOK_HPP

#include <list>
#include <string>

class PhoneBook
{
public:

  struct record_t {
    std::string number;
    std::string name;
  };

  using listIterator = std::list<record_t>::iterator;

  listIterator begin();
  listIterator end();

  listIterator next(listIterator position);
  listIterator prev(listIterator position);

  listIterator move(listIterator position, int steps);
  void pushBack(const record_t& record);

  listIterator insert(listIterator position, const record_t& record);
  listIterator erase(listIterator position);
  bool empty() const;

private:

  std::list<record_t> recordsList_;
};

#endif //B3_PHONE_BOOK_HPP
