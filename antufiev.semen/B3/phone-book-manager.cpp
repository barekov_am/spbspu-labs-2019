#include "phone-book-manager.hpp"

#include <iostream>
#include <algorithm>

PhoneBookManager::PhoneBookManager()
{
  bookmarks_.insert({"current", records_.end()});
}

void PhoneBookManager::add(const PhoneBook::record_t& record)
{
  records_.pushBack(record);

  if (std::next(records_.begin()) == records_.end())
  {
    bookmarks_["current"] = records_.begin();
  }
}

void PhoneBookManager::store(const std::string& bookmark, const std::string& newBookmark)
{
  auto iter = bookmarks_.find(bookmark);

  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  bookmarks_.insert({newBookmark, iter->second});
}

void PhoneBookManager::move(const std::string& bookmark, MovePosition& position)
{
  auto iter = bookmarks_.find(bookmark);

  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  if (position == MovePosition::first)
  {
    iter->second = records_.begin();
  }
  else if (position == MovePosition::last)
  {
    iter->second = std::prev(records_.end());
  }
}

void PhoneBookManager::move(const std::string& bookmark, int n)
{
  auto iter = bookmarks_.find(bookmark);

  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  iter->second = records_.move(iter->second, n);
}

void PhoneBookManager::show(const std::string& bookmark)
{
  auto iter = bookmarks_.find(bookmark);

  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  if (records_.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  std::cout << iter->second->number << " " << iter->second->name << "\n";
}

void PhoneBookManager::remove(const std::string& bookmark)
{
  auto iter = bookmarks_.find(bookmark);

  if (iter == bookmarks_.end() || iter->second == records_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  auto current = iter->second;
  auto pos = (current == std::prev(records_.end())) ?
      std::prev(current) : std::next(current);

  std::for_each(bookmarks_.begin(), bookmarks_.end(),
      [&](auto& bookmark){ bookmark.second = pos; });

  records_.erase(current);
}

void PhoneBookManager::insert(const std::string& bookmark, const PhoneBook::record_t& record,
    InsertPosition& position)
{
  auto iter = bookmarks_.find(bookmark);

  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  if (iter->second == records_.end())
  {
    add(record);
    return;
  }

  if (position == InsertPosition::before)
  {
    iter->second = std::next(records_.insert(iter->second, record));
  }
  else if (position == InsertPosition::after)
  {
    if (std::next(iter->second) == records_.end())
    {
      records_.pushBack(record);
      return;
    }
    iter->second = std::prev(records_.insert(std::next(iter->second), record));
  }
}
