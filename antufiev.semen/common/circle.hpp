#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"
namespace antufiev
{
  class Circle : public Shape
  {
    public:
      Circle(const point_t &center, double radius);
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const point_t &centre) override;
      void move(double dx, double dy) override;
      void printInfo() const override;
      void scale(double multi) override;
      point_t getCenter() const override;
      void rotate(double /*angle*/) override;
    private:
      point_t center_;
      double radius_;
  };

}
#endif //CIRCLE_HPP
