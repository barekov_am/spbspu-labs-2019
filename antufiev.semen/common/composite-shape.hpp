#ifndef A2_COMPOSITE_SHAPE_HPP
#define A2_COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace antufiev
{
  class CompositeShape : public Shape
  {
    public:
      CompositeShape();
      CompositeShape(const CompositeShape &rhs);
      CompositeShape(CompositeShape &&rhs);
      CompositeShape(shapePtr shape);
      ~CompositeShape() = default;
      CompositeShape& operator =(const CompositeShape &rhs);
      CompositeShape& operator =(CompositeShape &&rhs);
      shapePtr operator [](size_t index) const;
      void add(shapePtr shape);
      void remove(size_t i);
      void remove(shapePtr shape);
      size_t size() const;
      void move(double dx, double dy) override;
      void move(const point_t &center) override;
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void printInfo() const override;
      point_t getCenter() const override;
      void scale(double multi) override;
      void rotate(double angle) override;

    private:
      size_t size_;
      std::unique_ptr<shapePtr []> shapes_;
  };
}
#endif //A2_COMPOSITE_SHAPE_HPP
