#include <boost/test/unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include "triangle.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

const double accuracy = 0.00001;

BOOST_AUTO_TEST_SUITE(testOfCompositeShape)

  BOOST_AUTO_TEST_CASE(widthAndHeightAfterMoving)
  {
    antufiev::Circle circle({3, 4}, 5);
    antufiev::Rectangle rectangle({5, 6}, 2, 6);
    antufiev::Triangle triangle({4, 6}, {5, 3}, {4, 9});
    antufiev::CompositeShape compositeShape(std::make_shared<antufiev::Rectangle>(rectangle));
    compositeShape.add(std::make_shared<antufiev::Circle>(circle));
    compositeShape.add(std::make_shared<antufiev::Triangle>(triangle));
    antufiev::rectangle_t frameBeforeMoving = compositeShape.getFrameRect();
    double area = compositeShape.getArea();
    compositeShape.move({4, 5});
    BOOST_CHECK_CLOSE(frameBeforeMoving.height, compositeShape.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(frameBeforeMoving.width, compositeShape.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(area, compositeShape.getArea(), accuracy);
    compositeShape.move(4, 5);
    BOOST_CHECK_CLOSE(frameBeforeMoving.height, compositeShape.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(frameBeforeMoving.width, compositeShape.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(area, compositeShape.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(checkCorrectAddAndRemove)
  {
    antufiev::Circle circle({3, 4}, 5);
    antufiev::Rectangle rectangle({4, 5}, 6, 8);
    antufiev::CompositeShape compositeShape(std::make_shared<antufiev::Circle>(circle));
    double areaBeforeAdd = compositeShape.getArea();
    antufiev::Shape::shapePtr shape = std::make_shared<antufiev::Rectangle>(rectangle);
    compositeShape.add(shape);
    BOOST_CHECK_CLOSE(areaBeforeAdd + rectangle.getArea(), compositeShape.getArea(), accuracy);
    compositeShape.remove(shape);
    BOOST_CHECK_CLOSE(areaBeforeAdd, compositeShape.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(outOfRangeException)
    {
      antufiev::Rectangle rectangle({5, 6}, 7, 7);
      antufiev::CompositeShape compositeShape(std::make_shared<antufiev::Rectangle>(rectangle));
      BOOST_CHECK_THROW(compositeShape[100], std::out_of_range);
    }

  BOOST_AUTO_TEST_CASE(areaAfterScaling)
  {
    antufiev::Circle circle({3, 4}, 5);
    antufiev::Rectangle rectangle({5, 6}, 2, 6);
    antufiev::Triangle triangle({4, 6}, {5, 3}, {4, 9});
    antufiev::CompositeShape compositeShape(std::make_shared<antufiev::Circle>(circle));
    compositeShape.add(std::make_shared<antufiev::Triangle>(triangle));
    compositeShape.add(std::make_shared<antufiev::Rectangle>(rectangle));
    double area = compositeShape.getArea();
    compositeShape.scale(2);
    BOOST_CHECK_CLOSE(area * 4, compositeShape.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(checkCorrectGetFrameRect)
  {
    antufiev::Circle circle({6, 5}, 3);
    antufiev::Rectangle rectangle({6, 5}, 6, 6);
    antufiev::CompositeShape compositeShape(std::make_shared<antufiev::Rectangle>(rectangle));
    compositeShape.add(std::make_shared<antufiev::Circle>(circle));
    antufiev::rectangle_t frame = rectangle.getFrameRect();
    BOOST_CHECK_CLOSE(frame.height, compositeShape.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(frame.width, compositeShape.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(frame.pos.x, compositeShape.getFrameRect().pos.x, accuracy);
    BOOST_CHECK_CLOSE(frame.pos.y, compositeShape.getFrameRect().pos.y, accuracy);
  }

  BOOST_AUTO_TEST_CASE(exceptionThrow)
  {
    BOOST_CHECK_THROW(antufiev::CompositeShape(nullptr), std::invalid_argument);
    antufiev::Circle circle({2, 2}, 9);
    antufiev::Rectangle rectangle({4, 2}, 3, 5);
    antufiev::Triangle triangle({4, 3}, {5, 6}, {3, 8});
    antufiev::CompositeShape compositeShape(std::make_shared<antufiev::Circle>(circle));
    compositeShape.add(std::make_shared<antufiev::Rectangle>(rectangle));
    compositeShape.add(std::make_shared<antufiev::Triangle>(triangle));
    antufiev::CompositeShape compositeShape1;
    BOOST_CHECK_THROW(compositeShape1.getFrameRect(), std::logic_error);
    BOOST_CHECK_THROW(compositeShape.scale(-1), std::invalid_argument);
    BOOST_CHECK_THROW(compositeShape.add(nullptr), std::invalid_argument);
    BOOST_CHECK_THROW(compositeShape.remove(10), std::out_of_range);
    BOOST_CHECK_THROW(compositeShape.remove(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(copyAndMove)
  {
    antufiev::Circle circle({2, 2}, 9);
    antufiev::Rectangle rectangle({4, 2}, 3, 5);
    antufiev::Triangle triangle({4, 3}, {5, 6}, {3, 8});
    antufiev::CompositeShape compositeShape1(std::make_shared<antufiev::Circle>(circle));
    compositeShape1.add(std::make_shared<antufiev::Rectangle>(rectangle));
    compositeShape1.add(std::make_shared<antufiev::Triangle>(triangle));
    const double areaB = compositeShape1.getArea();
    const antufiev::rectangle_t frameRectB = compositeShape1.getFrameRect();
    const double areaFrameRectB = frameRectB.width * frameRectB.height;
    antufiev::CompositeShape compositeShape2(compositeShape1);
    BOOST_CHECK_CLOSE(areaB, compositeShape2.getArea(), accuracy);
    BOOST_CHECK_CLOSE(areaFrameRectB, compositeShape2.getFrameRect().width * compositeShape2.getFrameRect().height, accuracy);
    antufiev::CompositeShape compositeShape3(std::move(compositeShape2));
    BOOST_CHECK_CLOSE(areaB, compositeShape3.getArea(), accuracy);
    BOOST_CHECK_CLOSE(areaFrameRectB, compositeShape3.getFrameRect().width * compositeShape3.getFrameRect().height, accuracy);
    antufiev::CompositeShape compositeShape4;
    compositeShape4 = compositeShape3;
    BOOST_CHECK_CLOSE(areaB, compositeShape4.getArea(), accuracy);
    BOOST_CHECK_CLOSE(areaFrameRectB, compositeShape4.getFrameRect().width *compositeShape4.getFrameRect().height, accuracy);
    antufiev::CompositeShape compositeShape5;
    compositeShape5 = std::move(compositeShape4);
    BOOST_CHECK_CLOSE(areaB, compositeShape5.getArea(), accuracy);
    BOOST_CHECK_CLOSE(areaFrameRectB, compositeShape5.getFrameRect().width * compositeShape5.getFrameRect().height, accuracy);
  }

  BOOST_AUTO_TEST_CASE(rotate)
  {
    antufiev::Circle circle({1, 1}, 1);
    antufiev::Rectangle rectangle({0, 0}, 5, 5);
    antufiev::CompositeShape composite(std::make_shared<antufiev::Circle>(circle));
    composite.add(std::make_shared<antufiev::Rectangle>(rectangle));
    const antufiev::rectangle_t frameRectBefore = composite.getFrameRect();
    const double areaBefore = composite.getArea();
    const double angle = 45;
    composite.rotate(angle);
    const antufiev::rectangle_t frameRectAfter = composite.getFrameRect();
    const double areaAfter = composite.getArea();
    BOOST_CHECK_CLOSE(areaBefore, areaAfter, accuracy);
    BOOST_CHECK_CLOSE(frameRectBefore.width * std::sqrt(2), frameRectAfter.width, accuracy);
    BOOST_CHECK_CLOSE(frameRectBefore.height * std::sqrt(2), frameRectAfter.height, accuracy);
    BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, accuracy);
    BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, accuracy);
  }

BOOST_AUTO_TEST_SUITE_END()
