#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace antufiev
{
  struct point_t
  {
    double x, y;
  };

  struct rectangle_t
  {
    double width, height;
    point_t pos;

    bool isOverlaps(const rectangle_t & rect) const;
    bool isInside(double x, double y) const;
  };
}

#endif //BASE_TYPES_HPP
