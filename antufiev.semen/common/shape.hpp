#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"
#include <memory>
namespace antufiev
{
  class Shape
  {
    public:
      using shapePtr = std::shared_ptr<Shape>;
      virtual ~Shape() = default;
      virtual double getArea() const = 0;
      virtual rectangle_t getFrameRect() const = 0;
      virtual void move(const point_t &centre) = 0;
      virtual void move(double dx, double dy) = 0;
      virtual void printInfo() const = 0;
      virtual void scale(double multi) = 0;
      virtual point_t getCenter() const = 0;
      virtual void rotate(double angle) = 0;
  };

}
#endif //SHAPE_HPP
