#include <boost/test/unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

  BOOST_AUTO_TEST_CASE(constructorsAndOperators)
  {
    antufiev::Matrix matrix;
    antufiev::Circle circle1({2 ,2}, 1);
    antufiev::Rectangle rectangle1({5, 5}, 1, 1);
    antufiev::Circle circle2({3 ,3}, 1);
    antufiev::Rectangle rectangle2({7, 6}, 2, 2);
    antufiev::ShapePointer ptrCircle1 = (std::make_shared<antufiev::Circle>(circle1));
    antufiev::ShapePointer ptrCircle2 = (std::make_shared<antufiev::Circle>(circle2));
    antufiev::ShapePointer ptrRectangle1 = (std::make_shared<antufiev::Rectangle>(rectangle1));
    antufiev::ShapePointer ptrRectangle2 = (std::make_shared<antufiev::Rectangle>(rectangle2));
    matrix.add(ptrRectangle1, 0);
    matrix.add(ptrRectangle2, 0);
    matrix.add(ptrCircle1, 1);
    matrix.add(ptrCircle2, 1);
    const antufiev::Matrix constCopiedMatrix1(matrix);
    BOOST_CHECK_EQUAL(matrix == constCopiedMatrix1, true);
    const antufiev::Matrix constCopiedMatrix2 = matrix;
    BOOST_CHECK_EQUAL(matrix == constCopiedMatrix2, true);
    antufiev::Matrix movedMatrix1(std::move(matrix));
    BOOST_CHECK_EQUAL(constCopiedMatrix1 == constCopiedMatrix2, true);
    const antufiev::Matrix movedMatrix2 = std::move(movedMatrix1);
    BOOST_CHECK_EQUAL(constCopiedMatrix1 == movedMatrix2, true);
  }
  
  BOOST_AUTO_TEST_CASE(indexOperator)
  {
    antufiev::Matrix matrix;
    antufiev::Circle circle({2 ,2}, 1);
    antufiev::ShapePointer ptrCircle = (std::make_shared<antufiev::Circle>(circle));
    matrix.add(ptrCircle, 0);
    BOOST_CHECK_EQUAL(ptrCircle == matrix[0][0], true);
    antufiev::Rectangle rectangle({1, 2}, 4, 5);
    antufiev::ShapePointer ptrRectangle = (std::make_shared<antufiev::Rectangle>(rectangle));
    matrix[0][0] = ptrRectangle;
    BOOST_CHECK_EQUAL(ptrRectangle == matrix[0][0], true);
    BOOST_CHECK_THROW(matrix[1], std::out_of_range);
  }

BOOST_AUTO_TEST_SUITE_END()
