#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "composite-shape.hpp"

const double accuracy = 0.00001;

BOOST_AUTO_TEST_SUITE(TestCircle)

  BOOST_AUTO_TEST_CASE(circleAreaConstantAfterMove)
  {
    antufiev::Circle circle({5.0, 5.0}, 2.0);
    const antufiev::rectangle_t frameRectPrev = circle.getFrameRect();
    const double areaPrev = circle.getArea();
    circle.move(1.0, 2.5);
    BOOST_CHECK_CLOSE(frameRectPrev.width, circle.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(frameRectPrev.height, circle.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(areaPrev, circle.getArea(), accuracy);
    circle.move({5.7, 2.4});
    BOOST_CHECK_CLOSE(frameRectPrev.width, circle.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(frameRectPrev.height, circle.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(areaPrev, circle.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(circleAreaAfterScale)
  {
    antufiev::Circle circle({5.0, 5.0}, 2.0);
    const double scaleCoef = 0.6;
    const double areaPrev = circle.getArea();
    circle.scale(scaleCoef);
    BOOST_CHECK_CLOSE(areaPrev * scaleCoef * scaleCoef, circle.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(shapeIllegalArgument)
  {
    antufiev::Circle circle({5.0, 5.0}, 2.0);
    BOOST_CHECK_THROW(antufiev::Circle({5.0, 5.0}, 0.0), std::invalid_argument);
    BOOST_CHECK_THROW(antufiev::Circle({5.0, 5.0}, -1.0), std::invalid_argument);
    BOOST_CHECK_THROW(antufiev::Circle({5.0, 5.0}, -5.1), std::invalid_argument);
    BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
