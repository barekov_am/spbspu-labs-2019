#include "composite-shape.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>

antufiev::CompositeShape::CompositeShape():
  size_(0),
  shapes_(nullptr)
{}

antufiev::CompositeShape::CompositeShape(const antufiev::CompositeShape &rhs):
  size_(rhs.size_),
  shapes_(std::make_unique<shapePtr []>(rhs.size_))
{
  for (size_t i = 0; i < size_; i++) {
    shapes_[i] = rhs.shapes_[i];
  }

}

antufiev::CompositeShape::CompositeShape(antufiev::CompositeShape &&rhs):
  size_(rhs.size_),
  shapes_(std::move(rhs.shapes_))
{
  rhs.size_ = 0;
}

antufiev::CompositeShape::CompositeShape(shapePtr shape):
  size_(1),
  shapes_(std::make_unique<shapePtr []>(size_))
{
  if (shape == nullptr) {
    throw std::invalid_argument("Your shape is nullptr!");
  }
  shapes_[0] = shape;
}

antufiev::CompositeShape& antufiev::CompositeShape::operator =(const antufiev::CompositeShape &rhs)
{
  if (&rhs == this) {
    return *this;
  }
  size_ = rhs.size_;
  std::unique_ptr<shapePtr []> arrayOfShapes(std::make_unique<shapePtr []>(rhs.size_));
  for (std::size_t i = 0; i < size_; i++) {
    arrayOfShapes[i] = rhs.shapes_[i];
  }
  shapes_.swap(arrayOfShapes);
  return *this;

}

antufiev::CompositeShape& antufiev::CompositeShape::operator =(antufiev::CompositeShape &&rhs)
{
  if (&rhs == this) {
    return *this;
  }
  size_ = rhs.size_;
  shapes_ = std::move(rhs.shapes_);
  rhs.size_ = 0;
  return *this;

}

antufiev::CompositeShape::shapePtr antufiev::CompositeShape::operator [](size_t index) const
{
  if (index >= size_) {
    throw std::out_of_range("There is no figure!");
  }
  return shapes_[index];
}

void antufiev::CompositeShape::add(shapePtr shape)
{
  if (shape == nullptr) {
    throw std::invalid_argument("You didn't add shape in Constructor or new object is nullptr!!!");
  }
  size_++;
  std::unique_ptr<shapePtr []> temp(std::make_unique<shapePtr []>(size_));
  for (size_t i = 0; i < size_ - 1; i++) {
    temp[i] = shapes_[i];
  }
  temp[size_ - 1] = shape;
  shapes_ = std::move(temp);
}

void antufiev::CompositeShape::remove(shapePtr shape)
{
  if (shape == nullptr) {
    throw std::invalid_argument("You gave nullptr!");
  }
  size_t i = 0;
  while ((i < size_) && (shape != shapes_[i])) {
    i++;
  }
  if (i == size_) {
    throw std::invalid_argument("There is no such figure in the array");
  }
  remove(i);
}

void antufiev::CompositeShape::remove(size_t i)
{
  if (i > size_ - 1) {
    throw std::out_of_range("The index is greater than number of figures!");
  }
  for (size_t j = i; j < size_ - 1; j++) {
      shapes_[j] = shapes_[j + 1];
  }
  size_--;
}

size_t antufiev::CompositeShape::size() const
{
  return size_;
}

void antufiev::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; i++) {
    shapes_[i]->move(dx, dy);
  }

}

void antufiev::CompositeShape::move(const antufiev::point_t &center)
{
  point_t shift = {center.x - getFrameRect().pos.x, center.y - getFrameRect().pos.y};
  move(shift.x, shift.y);
}

double antufiev::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < size_; i++) {
    area += shapes_[i]->getArea();
  }
  return area;
}

antufiev::point_t getBotAndTop(antufiev::rectangle_t frame, int factor)
{
  double x = frame.pos.x + (factor * (frame.width / 2));
  double y = frame.pos.y + (factor * (frame.height / 2));
  return {x, y};
}

antufiev::rectangle_t antufiev::CompositeShape::getFrameRect() const
{
  if (size_ == 0) {
    throw std::logic_error("You didn't add shape in array");
  }
  point_t maxXAndY = getBotAndTop(shapes_[0]->getFrameRect(), 1);
  point_t minXAndY = getBotAndTop(shapes_[0]->getFrameRect(), (-1));
  for (size_t i = 1; i < size_; i++) {
    maxXAndY.x = std::max(getBotAndTop(shapes_[i]->getFrameRect(), 1).x, maxXAndY.x);
    maxXAndY.y = std::max(getBotAndTop(shapes_[i]->getFrameRect(), 1).y, maxXAndY.y);
    minXAndY.x = std::min(getBotAndTop(shapes_[i]->getFrameRect(), (-1)).x, minXAndY.x);
    minXAndY.y = std::min(getBotAndTop(shapes_[i]->getFrameRect(), (-1)).y, minXAndY.y);
  }
  double height = maxXAndY.y - minXAndY.y;
  double width = maxXAndY.x - minXAndY.x;
  point_t center = {minXAndY.x + width / 2, minXAndY.y + height / 2};
  return {width, height, center};
}

antufiev::point_t antufiev::CompositeShape::getCenter() const
{
  return {getFrameRect().pos.x, getFrameRect().pos.y};
}

void antufiev::CompositeShape::scale(double multi)
{
  if (multi <= 0) {
    throw std::invalid_argument("Wrong multi");
  }
  point_t point = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++) {
    point_t shift = {shapes_[i]->getFrameRect().pos.x - point.x, shapes_[i]->getFrameRect().pos.y - point.y};
    shapes_[i]->move(shift.x * (multi - 1), shift.y * (multi - 1));
    shapes_[i]->scale(multi);
  }

}

void antufiev::CompositeShape::rotate(double angle)
{
  const double cos = std::cos((2 * M_PI * angle) / 360);
  const double sin = std::sin((2 * M_PI * angle) / 360);
  const point_t compCentre = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++) {
    const point_t center = shapes_[i]->getFrameRect().pos;
    const double dx = (center.x - compCentre.x) * cos - (center.y - compCentre.y) * sin;
    const double dy = (center.x - compCentre.x) * sin + (center.y - compCentre.y) * cos;
    shapes_[i]->move({compCentre.x + dx, compCentre.y + dy});
    shapes_[i]->rotate(angle);
  }

}

void antufiev::CompositeShape::printInfo() const
{
  for (size_t i = 0; i < size_; i++) {
    shapes_[i]->printInfo();
  }
  
}
