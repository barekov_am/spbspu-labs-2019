#include <boost/test/unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

const double accuracy = 0.00001;

BOOST_AUTO_TEST_SUITE(shapeSplitterTest)

  BOOST_AUTO_TEST_CASE(correctSlpit)
  {
    antufiev::CompositeShape compositeShape;
    antufiev::Circle circle1({10 ,10}, 1);
    antufiev::Rectangle rectangle1({0, 0}, 5, 5);
    antufiev::Circle circle2({10 ,10}, 3);
    antufiev::Rectangle rectangle2({2, 2}, 5, 5);
    antufiev::Rectangle rectangle3({13, 10}, 0.1, 0.1);
    antufiev::ShapePointer ptrCircle1 = (std::make_shared<antufiev::Circle>(circle1));
    antufiev::ShapePointer ptrCircle2 = (std::make_shared<antufiev::Circle>(circle2));
    antufiev::ShapePointer ptrRectangle1 = (std::make_shared<antufiev::Rectangle>(rectangle1));
    antufiev::ShapePointer ptrRectangle2 = (std::make_shared<antufiev::Rectangle>(rectangle2));
    antufiev::ShapePointer ptrRectangle3 = (std::make_shared<antufiev::Rectangle>(rectangle3));
    compositeShape.add(ptrRectangle1);
    compositeShape.add(ptrRectangle2);
    compositeShape.add(ptrCircle1);
    compositeShape.add(ptrCircle2);
    compositeShape.add(ptrRectangle3);
    antufiev::Matrix matrix = antufiev::split(compositeShape);
    BOOST_CHECK_EQUAL(matrix[0][0] == ptrRectangle1, true);
    BOOST_CHECK_EQUAL(matrix[0][1] == ptrCircle1, true);
    BOOST_CHECK_EQUAL(matrix[0][2] == ptrRectangle3, true);
    BOOST_CHECK_EQUAL(matrix[1][0] == ptrRectangle2, true);
    BOOST_CHECK_EQUAL(matrix[1][1] == ptrCircle2, true);
  }

BOOST_AUTO_TEST_SUITE_END()

