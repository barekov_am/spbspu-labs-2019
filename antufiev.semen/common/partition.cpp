#include "partition.hpp"

#include <stdexcept>

antufiev::Matrix antufiev::split(const antufiev::CompositeShape & compositeShape)
{
  const unsigned int size = compositeShape.size();
  if (size == 0) {
    throw std::invalid_argument("Composite shape must be not empty");
  }
  RectangleArray frameRects = std::make_unique<rectangle_t[]>(size);
  for (unsigned i = 0; i < size; i++) {
    frameRects[i] = compositeShape[i]->getFrameRect();
  }
  Matrix matrix;
  matrix.add(compositeShape[0], 0);
  UIntArray shapeLayers = std::make_unique<unsigned int[]>(size);
  shapeLayers[0] = 0;
  for (unsigned int i = 1; i < size; i++) {
    BoolArray overlapsArray = std::make_unique<bool[]>(matrix.rows() + 1);
    for (unsigned int k = 0; k <= matrix.rows(); k++) {
      overlapsArray[k] = false;
    }
    for (unsigned int j = 0; j < i; j++) {
      overlapsArray[shapeLayers[j]] |= frameRects[i].isOverlaps(frameRects[j]);
    }
    shapeLayers[i] = 0;
    while (overlapsArray[shapeLayers[i]])
    {
      shapeLayers[i]++;
    }

    matrix.add(compositeShape[i], shapeLayers[i]);
  }

  return matrix;
}
