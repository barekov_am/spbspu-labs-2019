#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "composite-shape.hpp"

const double accuracy = 0.00001;

BOOST_AUTO_TEST_SUITE(TestRectangle)

  BOOST_AUTO_TEST_CASE(frameShapesConstantAfterMove)
  {
    antufiev::Rectangle rectangle1({12, 8}, 4, 4);
    const antufiev::rectangle_t frameRectBefore1 = rectangle1.getFrameRect();
    const double areaBefore1 = rectangle1.getArea();
    const double angle1 = 45;
    rectangle1.rotate(angle1);
    const antufiev::rectangle_t frameRectAfter1 = rectangle1.getFrameRect();
    const double areaAfter1 = rectangle1.getArea();
    BOOST_CHECK_CLOSE(areaBefore1, areaAfter1, accuracy);
    BOOST_CHECK_CLOSE(frameRectBefore1.width * std::sqrt(2), frameRectAfter1.width, accuracy);
    BOOST_CHECK_CLOSE(frameRectBefore1.height * std::sqrt(2), frameRectAfter1.height, accuracy);
    BOOST_CHECK_CLOSE(frameRectBefore1.pos.x, frameRectAfter1.pos.x, accuracy);
    BOOST_CHECK_CLOSE(frameRectBefore1.pos.y, frameRectAfter1.pos.y, accuracy);
  }

  BOOST_AUTO_TEST_CASE(shapeAreaAfterScale)
  {
    antufiev::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);
    const double areaPrev = rectangle.getArea();
    const double scaleCoef = 2.3;
    rectangle.scale(scaleCoef);
    BOOST_CHECK_CLOSE(areaPrev * scaleCoef * scaleCoef, rectangle.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(shapeIllegalArgument)
  {
   antufiev::Rectangle rectangle({ 5.0, 5.0 }, 2.0, 1.0);
   BOOST_CHECK_THROW(antufiev::Rectangle({ 5.0, 5.0 }, -2.0, 1.0), std::invalid_argument);
   BOOST_CHECK_THROW(antufiev::Rectangle({ 5.0, 5.0 }, 2.0, -1.0), std::invalid_argument);
   BOOST_CHECK_THROW(antufiev::Rectangle({ 5.0, 5.0 }, -2.0, -1.0), std::invalid_argument);
   BOOST_CHECK_THROW(rectangle.scale(-1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(shapeFrameRectAfterRotate)
  {
    antufiev::Rectangle rectangle({5.0, 5.0},10, 10);
    double prevArea = rectangle.getArea();
    antufiev::rectangle_t frameRect = rectangle.getFrameRect();
    rectangle.rotate(45);
    BOOST_CHECK_CLOSE(prevArea, rectangle.getArea(), accuracy);
    BOOST_CHECK_CLOSE(prevArea, rectangle.getArea(), accuracy);
    BOOST_CHECK_CLOSE(frameRect.width * std::sqrt(2), rectangle.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(frameRect.height * std::sqrt(2), rectangle.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(frameRect.pos.x, rectangle.getFrameRect().pos.x, accuracy);
    BOOST_CHECK_CLOSE(frameRect.pos.y, rectangle.getFrameRect().pos.y, accuracy);
  }

BOOST_AUTO_TEST_SUITE_END()
