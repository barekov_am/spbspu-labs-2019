#include "triangle.hpp"

#include <iostream>
#include <algorithm>
#include <cassert>
#include <cmath>


static double getLength(const antufiev::point_t &point1, const antufiev::point_t &point2)
{
  return sqrt(pow(point1.x - point2.x, 2) + pow(point1.y - point2.y, 2));
}

static antufiev::point_t rotateOneCoordinate(antufiev::point_t point, double angle)
{
  double x = point.x * cos(M_PI * angle / 180) - point.y * sin(M_PI * angle / 180);
  double y = point.y * cos(M_PI * angle / 180) + point.x * sin(M_PI * angle / 180);
  return {x, y};
}

antufiev::Triangle::Triangle(point_t point1, point_t point2, point_t point3) :
  point1_(point1),
  point2_(point2),
  point3_(point3)
{
  if (getArea() == 0 ) {
    throw std::invalid_argument("Invalid points");
  }

}

double antufiev::Triangle::getArea() const
{
  double flank1 = getLength(point1_, point2_);
  double flank2 = getLength(point2_, point3_);
  double flank3 = getLength(point3_, point1_);
  double half_p = (flank1 + flank2 + flank3) / 2;
  return sqrt(half_p * (half_p - flank1) * (half_p - flank2) * (half_p - flank3));
}

antufiev::rectangle_t antufiev::Triangle::getFrameRect() const
{
  double max_x = std::max({point1_.x, point2_.x, point3_.x});
  double min_x = std::min({point1_.x, point2_.x, point3_.x});
  double max_y = std::max({point1_.y, point2_.y, point3_.y});
  double min_y = std::min({point1_.y, point2_.y, point3_.y});
  return {fabs(max_x - min_x), fabs(max_y - min_y),fabs(min_x + (max_x - min_x) / 2),fabs(min_y + (max_y - min_y) / 2)};
}

void antufiev::Triangle::move(double dx, double dy)
{
  point1_.x += dx;
  point2_.x += dx;
  point3_.x += dx;
  point1_.y += dy;
  point2_.y += dy;
  point3_.y += dy;
}

void antufiev::Triangle::move(const antufiev::point_t &point)
{
  double dx = point.x - point1_.x;
  double dy = point.y - point2_.y;
  move(dx, dy);
}

void antufiev::Triangle::printInfo() const
{
  const antufiev::point_t center = getCenter();
  std::cout << "Triangle:" << std::endl;
  std::cout << "Point1:" << point1_.x << " " << point1_.y << std::endl;
  std::cout << "Point2:" << point2_.x << " " << point2_.y << std::endl;
  std::cout << "Point3:" << point3_.x << " " << point3_.y << std::endl;
  std::cout << "Center:" << center.x << " " << center.y << std::endl;
  std::cout << std::endl;
}

antufiev::point_t antufiev::Triangle::getCenter() const
{
  return {(point1_.x + point2_.x + point3_.x) / 3, (point1_.y + point2_.y + point3_.y) / 3};
}

void antufiev::Triangle::scale(double multi)
{
  if (multi <= 0) {
    throw std::invalid_argument("Invalid multiplier");
  }
  const antufiev::point_t center = getCenter();
  point1_.x = center.x + (point1_.x - center.x) * multi;
  point1_.y = center.y + (point1_.y - center.y) * multi;
  point2_.x = center.x + (point2_.x - center.x) * multi;
  point2_.y = center.y + (point2_.y - center.y) * multi;
  point3_.x = center.x + (point3_.x - center.x) * multi;
  point3_.y = center.y + (point3_.y - center.y) * multi;
}

void antufiev::Triangle::rotate(double angle)
{
  if (angle < 0 )
  {
    angle = (360 + fmod(angle, 360));
  }
  point1_ = rotateOneCoordinate(point1_, angle);
  point2_ = rotateOneCoordinate(point2_, angle);
  point3_ = rotateOneCoordinate(point3_, angle);
}
