#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"
namespace antufiev
{
  class Rectangle: public Shape
  {
    public:
      Rectangle(const antufiev::point_t &center, double width, double height);
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(double dx, double dy) override;
      void move(const point_t &center) override;
      void printInfo() const override;
      void scale(double multi) override;
      point_t getCenter() const override;
      void rotate(double angle) override;

    private:
      point_t center_;
      double width_;
      double height_;
      double angle_;
  };

}
#endif //RECTANGLE_HPP
