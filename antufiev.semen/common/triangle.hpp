#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"
namespace antufiev
{
  class Triangle: public Shape
  {
    public:
      Triangle(point_t point1, point_t point2_, point_t point3_);
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const point_t &point) override;
      void move(double dx, double dy) override;
      void printInfo() const override;
      void scale(double multi) override;
      point_t getCenter() const override;
      void rotate(double angle) override;
    private:
      point_t point1_, point2_, point3_;
  };

}
#endif //TRIANGLE_HPP
