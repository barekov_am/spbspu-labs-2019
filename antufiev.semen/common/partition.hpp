#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace antufiev
{
  Matrix split(const CompositeShape & compositeShape);
}

#endif //PARTITION_HPP
