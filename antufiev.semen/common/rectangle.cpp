#include "rectangle.hpp"

#include <iostream>
#include <cassert>
#include <cmath>

antufiev::Rectangle::Rectangle(const antufiev::point_t &center, double width, double height):
  center_(center),
  width_(width),
  height_(height),
  angle_(0)
{
  if ((width_ <= 0) || (height_ <= 0)) {
    throw std::invalid_argument("Invalid width or height");
  }

}

double antufiev::Rectangle::getArea() const
{
  return width_ * height_;
}

antufiev::rectangle_t antufiev::Rectangle::getFrameRect() const
{
  double width = width_ * std::fabs(cos((M_PI * angle_) / 180)) + height_ * std::fabs(sin((M_PI * angle_) / 180));
  double height = height_ * std::fabs(cos((M_PI * angle_) / 180)) + width_ * std::fabs(sin((M_PI * angle_) / 180));
  return {width, height, center_};
}

void antufiev::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void antufiev::Rectangle::move(const point_t &center)
{
  center_ = center;
}

void antufiev::Rectangle::printInfo() const
{
  std::cout << "Rectangle:" << std::endl;
  std::cout << "Width:" << width_ << std::endl;
  std::cout << "Height:" << height_ << std::endl;
  std::cout << "Center:" << center_.x << " " << center_.y << std::endl;
  std::cout << std::endl;
}

void antufiev::Rectangle::scale(double multi)
{
  if (multi <= 0 ) {
    throw std::invalid_argument("Invalid multiplier");
  }
  width_ *= multi;
  height_ *= multi;
}

antufiev::point_t antufiev::Rectangle::getCenter() const
{
  return center_;
}

void antufiev::Rectangle::rotate(double angle)
{
  angle_ += angle;
  if (angle_ < 0) {
    angle_ = 360 + fmod(angle_, 360);
  }

}
