#include "circle.hpp"

#include <iostream>
#include <cmath>
#include <cassert>

antufiev::Circle::Circle(const point_t &center, double radius):
  center_(center),
  radius_(radius)
{
  if (radius_ <= 0.0) {
    throw std::invalid_argument("Invalid radius");
  }

}

double antufiev::Circle::getArea() const
{
  return radius_ * M_PI * radius_;
}

antufiev::rectangle_t antufiev::Circle::getFrameRect() const
{
  return {2 * radius_, 2 * radius_, center_};
}

void antufiev::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void antufiev::Circle::move(const point_t &center)
{
  center_ = center;
}

void antufiev::Circle::printInfo() const
{
  std::cout << "Circle:" << std::endl;
  std::cout << "Center:" << center_.x << " " << center_.y << std::endl;
  std::cout << "Radius:" << radius_ << std::endl;
  std::cout << std::endl;
}

void antufiev::Circle::scale(double multi)
{
  if (multi <= 0) {
    throw std::invalid_argument("Invalid multiplier");
  }
  radius_ *= multi;
}

antufiev::point_t antufiev::Circle::getCenter() const
{
  return center_;
}

void antufiev::Circle::rotate(double /*angle*/)
{

}
