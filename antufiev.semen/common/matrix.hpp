#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "pointer-types.hpp"

namespace antufiev
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix & matrix);
    Matrix(Matrix && matrix) noexcept;
    ~Matrix() = default;
    Matrix & operator =(const Matrix & matrix);
    Matrix & operator =(Matrix && matrix) noexcept;
    bool operator ==(const Matrix & matrix) const;
    bool operator !=(const Matrix & matrix) const;
    ShapePointer * operator [](unsigned int row) const;
    void add(const ShapePointer & shapePointer, unsigned int row);
    unsigned int rows() const;
    unsigned int columns(unsigned int row) const;
  private:
    ShapeArray shapeArray_;
    UIntArray columns_;
    unsigned int rows_;
    unsigned int size_;
  };
}

#endif //MATRIX_HPP
