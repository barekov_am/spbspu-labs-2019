#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "triangle.hpp"
#include "composite-shape.hpp"

const double accuracy = 0.00001;

BOOST_AUTO_TEST_SUITE(TestTriangle)

  BOOST_AUTO_TEST_CASE(frameShapesConstantAfterMove)
  {
    antufiev::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});
    const antufiev::rectangle_t frameRectPrev = triangle.getFrameRect();
    const double areaPrev = triangle.getArea();
    triangle.move(0.1, 2.5);
    BOOST_CHECK_CLOSE(frameRectPrev.width, triangle.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(frameRectPrev.height, triangle.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(areaPrev, triangle.getArea(), accuracy);
    triangle.move({2.1, 3.2});
    BOOST_CHECK_CLOSE(frameRectPrev.width, triangle.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(frameRectPrev.height, triangle.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(areaPrev, triangle.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(shapeAreaAfterScale)
  {
    antufiev::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});
    const double areaPrev = triangle.getArea();
    const double scaleCoef = 0.4;
    triangle.scale(scaleCoef);
    BOOST_CHECK_CLOSE(areaPrev * scaleCoef * scaleCoef, triangle.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(shapeIllegalArgument)
  {
    antufiev::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});
    BOOST_CHECK_THROW(antufiev::Triangle({5.0, 5.0}, {2.0, 5.0}, {2.0, 5.0}), std::invalid_argument);
    BOOST_CHECK_THROW(antufiev::Triangle({5.0, 5.0}, {5.0, 1.0}, {5.0, 1.0}), std::invalid_argument);
    BOOST_CHECK_THROW(antufiev::Triangle({0.0, 0.0}, {0.0, 0.0}, {0.0, 0.0}), std::invalid_argument);
    BOOST_CHECK_THROW(triangle.scale(-4.1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(shapeFrameRectAfterRotate)
  {
    antufiev::Triangle triangle2({4, 5}, {6, 7}, {9, 8});
    const antufiev::rectangle_t frameRectBefore2 = triangle2.getFrameRect();
    const double areaBefore2 = triangle2.getArea();
    const double angle2 = 90;
    triangle2.rotate(angle2);
    const antufiev::rectangle_t frameRectAfter2 = triangle2.getFrameRect();
    const double areaAfter2 = triangle2.getArea();
    BOOST_CHECK_CLOSE(areaBefore2, areaAfter2, accuracy);
    BOOST_CHECK_CLOSE(frameRectBefore2.width, frameRectAfter2.height, accuracy);
    BOOST_CHECK_CLOSE(frameRectBefore2.height, frameRectAfter2.width, accuracy);
    BOOST_CHECK_CLOSE(triangle2.getCenter().x, triangle2.getCenter().x, accuracy);
    BOOST_CHECK_CLOSE(triangle2.getCenter().y, triangle2.getCenter().y, accuracy);
  }

BOOST_AUTO_TEST_SUITE_END()
