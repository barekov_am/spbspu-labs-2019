#include <algorithm>
#include "functions.hpp"

void print(const DataStruct& data)
{
  std::cout << data.key1 << ',' << data.key2 << ',' << data.str << std::endl;
}

void sort(std::vector<DataStruct>& vector)
{
  std::sort(vector.begin(), vector.end(), compare());
}
