#include <algorithm>
#include <iostream>
#include <string>

#include "dataStruct.hpp"

DataStruct readStruct(std::istream& input)
{
  int keys[2] = {0};
  std::string str;
  for (int& key : keys) {
    input >> key;
    if (input.eof() || std::abs(key) > 5) {
      throw std::invalid_argument("Invalid key. \n");
    }

    std::getline(input, str, ',');
    if (input.eof() || !str.empty()) {
      throw std::invalid_argument("Invalid key. \n");
    }
  }

  while (input.peek() == ' ') {
    input.get();
  }

  std::getline(input, str);
  if (!input.eof() || str.empty()) {
    throw std::invalid_argument("Invalid string. \n");
  }

  return {keys[0], keys[1], str};
}
