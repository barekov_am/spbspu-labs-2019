#ifndef B4_DATASTRUCT_HPP
#define B4_DATASTRUCT_HPP

#include <iostream>

struct DataStruct {
  int key1;
  int key2;
  std::string str;
};

DataStruct readStruct(std::istream& input);

#endif
