#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "partition.hpp"
#include "matrix.hpp"


int main()
{
    antufiev::CompositeShape compositeShape;
    antufiev::Circle circle({3, 4}, 5);
    antufiev::Rectangle rectangle({5, 6}, 2, 6);
    antufiev::Triangle triangle({4, 6}, {5, 3}, {4, 9});
    compositeShape.add(std::make_shared<antufiev::Rectangle>(rectangle));
    compositeShape.add(std::make_shared<antufiev::Circle>(circle));
    compositeShape.add(std::make_shared<antufiev::Triangle>(triangle));
    compositeShape.printInfo();
    compositeShape.rotate(45);
    compositeShape.printInfo();
    antufiev::Matrix matrix = antufiev::split(compositeShape);
    for (unsigned int i = 0; i < matrix.rows(); i++) {
      std::cout << "layer = " << i << ":" << std::endl;
      for (unsigned int j = 0; j < matrix.columns(i); j++) {
        matrix[i][j]->printInfo();
      }
    }
  return 0;
}
