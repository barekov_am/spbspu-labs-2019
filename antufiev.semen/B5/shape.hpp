#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>
#include <vector>

struct Point_t
{
  int x, y;
};

using Shape = std::vector<Point_t>;

Shape readPoints(std::string &str, int tops);
void readShapes(std::vector<Shape> &vector, std::string &str);
int getSquaredLenght(const Point_t &point1, const Point_t &point2);
bool isRectangle(const Shape &shape);
bool isSquare(const Shape &shape);
bool isLess(const Shape &lhs, const Shape &rhs);

#endif //SHAPE_HPP
