#include <iostream>
#include <sstream>
#include "shape.hpp"

const int NUM_OF_RECTANGLE_TOPS = 4;

Shape readPoints(std::string &str, int tops)
{
  Shape shape;
  size_t openBracket;
  size_t closeBracket;
  size_t colon;

  for (int i = 0; i < tops; i++)
  {
    if (str.empty())
    {
      throw std::invalid_argument("Incorrect number of arguments");
    }

    openBracket = str.find_first_of('(');
    colon = str.find_first_of(';');
    closeBracket = str.find_first_of(')');
    size_t npos = std::string::npos;

    if ((openBracket == npos) || (colon == npos) || (closeBracket == npos))
    {
      throw std::invalid_argument("Invalid point");
    }

    int x = std::stoi(str.substr(openBracket + 1, colon - (openBracket + 1)));
    int y = std::stoi(str.substr(colon + 1, closeBracket - (colon + 1)));
    str.erase(0, closeBracket + 1);
    shape.push_back({x, y});
  }

  while (str.find_first_of(" \t") == 0)
  {
    str.erase(0, 1);
  }

  if (!str.empty())
  {
    throw std::invalid_argument("Incorrect number of arguments");
  }

  return shape;
}

void readShapes(std::vector<Shape> &vector, std::string &str)
{
  while (std::getline(std::cin, str))
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Invalid reading");
    }

    std::stringstream stream(str);
    std::string strTops;
    stream >> strTops;

    if (strTops == "")
    {
      continue;
    }

    if (stream.eof())
    {
      throw std::invalid_argument("Incorrect number of arguments");
    }

    int tops = std::stoi(strTops);

    if (tops < 3)
    {
      throw std::invalid_argument("Incorrect value of tops");
    }

    std::getline(stream, str);
    vector.push_back(readPoints(str, tops));
  }
}

int getSquaredLenght(const Point_t &point1, const Point_t &point2)
{
  int x1x2 = point1.x - point2.x;
  int y1y2 = point1.y - point2.y;

  return x1x2 * x1x2 + y1y2 * y1y2;
}

bool isRectangle(const Shape  &shape)
{
  bool compareDiags = getSquaredLenght(shape[0], shape[2]) == getSquaredLenght(shape[1], shape[3]);
  bool compSides1 = getSquaredLenght(shape[0], shape[1]) == getSquaredLenght(shape[2], shape[3]);
  bool compSides2 = getSquaredLenght(shape[1], shape[2]) == getSquaredLenght(shape[0], shape[3]);

  return compareDiags && compSides1 && compSides2;
}

bool isSquare(const Shape  &shape)
{
  bool compSides = getSquaredLenght(shape[0], shape[1]) == getSquaredLenght(shape[1], shape[2]);

  if (isRectangle(shape) && compSides)
  {
    return true;
  }

  return false;
}

bool isLess(const Shape &lhs, const Shape &rhs)
{
  if (lhs.size() < rhs.size())
  {
    return true;
  }

  if ((lhs.size() == NUM_OF_RECTANGLE_TOPS) && (rhs.size() == NUM_OF_RECTANGLE_TOPS))
  {
    if (isSquare(lhs))
    {
      if (isSquare(rhs))
      {
        return lhs[0].x < rhs[0].x;
      }

      return true;
    }
  }

  return false;
}
