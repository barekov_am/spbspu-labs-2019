#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

int main() {
  romanenko::Circle circle(1.0, {1.0, 1.0});
  romanenko::Rectangle rectangle({1.0, 1.0, {1.0, 1.0}});
  romanenko::Triangle triangle({0.0, 0.0}, {7.0, 3.0}, {- 2.0, 4.0});
  romanenko::point_t points[5] = {{2.0, 4.0}, {9.0, 4.0}, {4.0, 1.0}, {5.5, 7.0}, {7.0, 1.0}};
  romanenko::Polygon polygon(5, points);
  romanenko::CompositeShape cs(std::make_shared<romanenko::Rectangle>(rectangle));
  std::cout << "CompositeShape pos at the beginning: " << cs.getPos().x << "," << cs.getPos().y;
  std::cout << "\n CompositeShape area: " << cs.getArea();

  cs.addShape(std::make_shared<romanenko::Circle>(circle));
  cs.addShape(std::make_shared<romanenko::Triangle>(triangle));
  cs.addShape(std::make_shared<romanenko::Polygon>(polygon));
  std::cout << "\nCompositeShape new pos " << cs.getPos().x << "," << cs.getPos().y;
  std::cout << "\n CompositeShape new area: " << cs.getArea();

  cs.move({2.0, 5.0});
  std::cout << "\nCompositeShape pos after moving to (2,5) " << cs.getPos().x << "," << cs.getPos().y;

  cs.move(2.0, 4.0);
  std::cout << "\nCompositeShape pos after moving by dx=2 dy= 4 " << cs.getPos().x << "," << cs.getPos().y;

  cs.scale(2.0);
  std::cout << "\nCompositeShape area after scaling: " << cs.getArea() << "\n";

  return 0;
}
