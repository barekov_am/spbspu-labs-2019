#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "base-types.hpp"
#include "shape.hpp"

class Circle : public Shape
{
public:
  Circle(double radius, point_t center);

  double getArea() const override;
  void move(double dX, double dY) override;
  void move(point_t point) override;
  rectangle_t getFrameRect() const override;
  void printBaseInfo() const override;
  void print() const override;

private:
  const double pi = 3.14159265358979323846;
  double radius_;
  point_t center_;
};

#endif
