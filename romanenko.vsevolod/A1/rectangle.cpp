#include "rectangle.hpp"
#include <iostream>
#include <cassert>

Rectangle::Rectangle(double width, double height, point_t pnt)
{
  assert((width > 0.0) && (height > 0.0));
  rectangle_ = {width, height, pnt};
}

double Rectangle::getArea() const
{
  return (rectangle_.width * rectangle_.height);
}

void Rectangle::move(point_t pnt)
{
  rectangle_.pos = pnt;
}

void Rectangle::move(double dX, double dY)
{
  rectangle_.pos.x += dX;
  rectangle_.pos.y += dY;
}

rectangle_t Rectangle::getFrameRect() const
{
  return {rectangle_.width, rectangle_.height, rectangle_.pos};
}

void Rectangle::printBaseInfo() const
{
  std::cout << "Width: " << rectangle_.width <<"\n";
  std::cout << "Height: " << rectangle_.height << "\n";
  std::cout << "Center position: "<< "X: " << rectangle_.pos.x
  << " Y: " << rectangle_.pos.y << "\n";
  std::cout << "Rectangle area: " << getArea() << "\n";

}

void Rectangle::print() const
{
  std::cout << "Center position: "<< "X: " << rectangle_.pos.x
  << " Y: " << rectangle_.pos.y << "\n\n";
}

