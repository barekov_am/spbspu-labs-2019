#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

class Rectangle : public Shape
{
public:
  Rectangle(double width, double height, point_t pos);

  double getArea() const override;
  void move(double dX, double dY) override;
  void move(point_t point) override;
  rectangle_t getFrameRect() const override;
  void printBaseInfo() const override;
  void print() const override;

private:
  rectangle_t rectangle_;
};

#endif
