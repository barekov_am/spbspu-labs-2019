#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "base-types.hpp"
#include "shape.hpp"

class Triangle : public Shape
{
public:
  Triangle(const point_t &ang1, const point_t &ang2, const point_t &ang3);

  point_t getCenter() const;
  double getArea() const override;
  void move(double dX, double dY) override;
  void move(point_t cent_pnt_) override;
  rectangle_t getFrameRect() const override;
  void printBaseInfo() const override;
  void print() const override;

private:
  point_t ang_[3];
  point_t center_;
};
#endif
