#include "triangle.hpp"

#include <iostream>
#include <cassert>
#include <math.h>

Triangle::Triangle(const point_t &ang1, const point_t &ang2, const point_t &ang3)
{
  ang_[0] = ang1;
  ang_[1] = ang2;
  ang_[2] = ang3;
  assert(getArea() > 0.0);
}

point_t Triangle::getCenter() const
{
  return
  {((ang_[0].x + ang_[1].x + ang_[2].x) / 3.0),
  ((ang_[0].y + ang_[1].y + ang_[2].y) / 3.0)};
}

double Triangle::getArea() const
{
  return fabs(((ang_[1].x - ang_[0].x) * (ang_[2].y - ang_[0].y)
  - (ang_[2].x - ang_[0].x) * (ang_[1].y - ang_[0].y) / 2.0));
}

void Triangle::move(double dX, double dY)
{
  for (int i = 0; i <= 2; i++)
  {
    ang_[i].x += dX;
    ang_[i].y += dY;
  }
}

void Triangle::move(point_t cent_pnt_)
{
  point_t centPos = getCenter();
  move((cent_pnt_.x - centPos.x), (cent_pnt_.y - centPos.y));
}

rectangle_t Triangle::getFrameRect() const
{
  point_t left_top = ang_[0];
  point_t right_bottom = ang_[0];

  for (int i = 0; i <= 1; i++)
  {
    if (left_top.x > ang_[i+1].x)
    {
      left_top.x = ang_[i+1].x;
    }
    if (left_top.y < ang_[i+1].y)
    {
      left_top.y = ang_[i+1].y;
    }

    if (right_bottom.x < ang_[i+1].x)
    {
      right_bottom.x = ang_[i+1].x;
    }
    if (right_bottom.y > ang_[i+1].y)
    {
      right_bottom.y = ang_[i+1].y;
    }
  }
  return {left_top.x - right_bottom.x,
  left_top.y - right_bottom.y,
  {(left_top.x - right_bottom.x)/2.0,
  (left_top.y - right_bottom.y)/2.0}};
}

void Triangle::printBaseInfo() const
{
  print();
  std::cout << "Triangle area: " << getArea() << "\n\n";
}

void Triangle::print() const
{
  std::cout << "Angle positions: "
  << "\n X: " << ang_[0].x <<", " << ang_[1].x << ", " << ang_[2].x
  << "\n Y: " << ang_[0].y <<", " << ang_[1].y << ", " << ang_[2].y <<"\n";

  std::cout << "Center position: " << "\n X: " << getCenter().x
  << "\n Y: " << getCenter().y << "\n\n";
}
