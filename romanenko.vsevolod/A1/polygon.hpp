#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "base-types.hpp"
#include "shape.hpp"

class Polygon : public Shape
{
public:
  Polygon(int n);
  
  point_t getCenter() const;
  double getArea() const override;
  void move(double dX, double dY) override;
  void move(point_t pnt_) override;
  rectangle_t getFrameRect() const override;
  void printBaseInfo() const override;
  void print() const override;

  void memFree() const;
private:
  point_t *angArr_ptr_;
  int nAngle_;
  point_t center_;
};
#endif
