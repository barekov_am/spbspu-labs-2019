#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

int main() {

  Shape *pointer;
  Rectangle rectangle(10.0, 4.9, {-13.0, 9.5});
  Circle circle(6.0, {-7.0, 21.1});
  Triangle triangle({2.0, 3.4}, {-12.5, 1.0}, {22.5, -16.4});
  Polygon polygon(5);

//Rectangle
  pointer = &rectangle;
  std::cout << "Rectangle Characteristics\n";
  pointer->printBaseInfo();

  std::cout << "\nMoving rectangle center to position X: 4.3, Y: -5.5\n";
  pointer->move({4.0, -5.0});
  pointer->print();

  std::cout << "\nMoving rectangle by X: 6.7, Y: 5.5\n";
  pointer->move(6.0, 5.0);
  pointer->print();

  rectangle_t frameRec = pointer->getFrameRect();
  std::cout << "Frame rectangle\n";
  std::cout << "Width: " << frameRec.width << "\n";
  std::cout << "Height: " << frameRec.height << "\n";
  pointer->print();

//Circle
  pointer = &circle;
  std::cout << "Circle Characteristics\n";
  pointer->printBaseInfo();

  std::cout << "Moving Circle center to position: X: 34, Y: -11\n";
  pointer->move({34.0, -11.0});
  pointer->print();

  std::cout << "Moving Circle by X: 7, Y: 13\n";
  pointer->move(7.0, 13.0);
  pointer->print();

  frameRec = pointer->getFrameRect();
  std::cout << "Frame rectangle\n";
  std::cout << "Width: " << frameRec.width << "\n";
  std::cout << "Height: " << frameRec.height << "\n";
  pointer->print();

//Triangle
  pointer = &triangle;
  std::cout << "Triangle Characteristics\n";
  pointer->printBaseInfo();

  std::cout << "Moving Triangle center to position: X: -21, Y: -13\n";
  pointer->move({-21.0, -13.0});
  pointer->print();

  std::cout << "Moving Triangle by X: 31, Y: 14\n";
  pointer->move(31.0, 14.0);
  pointer->print();

  frameRec = pointer->getFrameRect();
  std::cout << "Frame rectangle\n";
  std::cout << "Width: " << frameRec.width <<"\n";
  std::cout << "Height: " << frameRec.height << "\n";
  std::cout << "Center position: " <<
  "\nX: " << frameRec.pos.x <<
  "\nY: " << frameRec.pos.y << "\n";

//Polygon
  pointer = &polygon;
  std::cout << "\nPolygon Characteristics\n\n";
  std::cout << "Attention! Angle positions are randomized!\n\n";
  pointer->printBaseInfo();

  std::cout << "Moving Polygon center to position: X: -21, Y: -13\n";
  pointer->move({-21.0, -13.0});
  pointer->print();

  std::cout << "Moving Polygon by X: 31, Y: 14\n";
  pointer->move(31.0, 14.0);
  pointer->print();

  frameRec = pointer->getFrameRect();
  std::cout << "Frame rectangle\n";
  std::cout << "Width: " << frameRec.width <<"\n";
  std::cout << "Height: " << frameRec.height << "\n";
  std::cout << "Center position: " <<
  "\nX: " << frameRec.pos.x <<
  "\nY: " << frameRec.pos.y << "\n";
  polygon.memFree();
  
  return(0);
}
