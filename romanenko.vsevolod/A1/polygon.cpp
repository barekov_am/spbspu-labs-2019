#include "polygon.hpp"

#include <iostream>
#include <cassert>
#include <math.h>

Polygon::Polygon(int n)
{
  nAngle_ = n;
  point_t *angArr_ = new
  point_t [nAngle_];
  for (int i = 0; i < nAngle_; i++)
  {
     angArr_[i].x = rand() % 100 - 50;
     angArr_[i].y = rand() % 100 - 50;
  }

angArr_ptr_ = angArr_;

  assert(getArea() > 0.0);
}

point_t Polygon::getCenter() const
{
  double xPos;
  double yPos;
  point_t cent;

  xPos = 0.0;
  yPos = 0.0;
  for (int i = 0; i < nAngle_; i++)
  {
    xPos = xPos + angArr_ptr_[i].x;
    yPos = yPos + angArr_ptr_[i].y;
  }
  cent.x = xPos/nAngle_;
  cent.y = yPos/nAngle_;
  return (cent);
}

double Polygon::getArea() const
{
  double area;
  area = 0.0;
  for (int i = 0; i < nAngle_; i++)
  {
    if ((i + 1) != nAngle_)
    {
      area = area + ((angArr_ptr_[i].x * angArr_ptr_[i + 1].y)-
      (angArr_ptr_[i].y * angArr_ptr_[i + 1].x));
    }
    if ((i + 1) == nAngle_)
    {
      area = area + ((angArr_ptr_[i].x * angArr_ptr_[0].y)-
      (angArr_ptr_[i].y * angArr_ptr_[0].x));
    }
  }
  return (area / 2.0);
}

void Polygon::move(double dX, double dY)
{
  for (int i = 0; i < nAngle_; i++)
  {
    angArr_ptr_[i].x += dX;
    angArr_ptr_[i].y += dY;
  }
}

void Polygon::move(point_t pnt_)
{
  point_t centPos = getCenter();
  move((pnt_.x - centPos.x), (pnt_.y - centPos.y));
}

rectangle_t Polygon::getFrameRect() const
{
  point_t left_top = angArr_ptr_[0];
  point_t right_bottom = angArr_ptr_[0];

  for (int i = 0; i < nAngle_ - 1; i++)
  {
    if (left_top.x > angArr_ptr_[i+1].x)
    {
      left_top.x = angArr_ptr_[i+1].x;
    }
    if (left_top.y < angArr_ptr_[i+1].y)
    {
      left_top.y = angArr_ptr_[i+1].y;
    }

    if (right_bottom.x < angArr_ptr_[i+1].x)
    {
      right_bottom.x = angArr_ptr_[i+1].x;
    }
    if (right_bottom.y > angArr_ptr_[i+1].y)
    {
      right_bottom.y = angArr_ptr_[i+1].y;
    }
  }
  return {left_top.x - right_bottom.x,
      left_top.y - right_bottom.y,
      {(left_top.x - right_bottom.x)/2.0,
      (left_top.y - right_bottom.y)/2.0}};
}

void Polygon::printBaseInfo() const
{
  print();
  std::cout << "Polygon area: " << getArea() << "\n";
  std::cout << "Amount of angles: " << nAngle_ << "\n\n";
}

void Polygon::print() const
{
  std::cout << "Angle positions: ";
  for (int i = 0; i < nAngle_; i++)
  {
    std::cout << "\nAngle " << i+1 << " X: " << angArr_ptr_[i].x;
    std::cout << "\nAngle " << i+1 << " Y: " << angArr_ptr_[i].y;
  }

  std::cout << "\nCenter position: " << "\n X: " << getCenter().x
  << "\n Y: " << getCenter().y << "\n\n";
}

void Polygon::memFree() const
{
  delete [] angArr_ptr_;
}


