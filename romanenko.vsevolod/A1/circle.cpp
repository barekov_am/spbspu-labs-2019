#include "circle.hpp"
#include <iostream>
#include <cassert>
#include <math.h>

Circle::Circle(double radius, point_t center)
{
  radius_ = radius;
  center_ = center;
  assert(radius_ > 0.0);
}

double Circle::getArea() const
{
  return (pi * radius_ * radius_);
}

void Circle::move(double dX, double dY)
{
  center_.x += dX;
  center_.y += dY;
}

void Circle::move(point_t point)
{
  center_ = point;
}

rectangle_t Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, center_};
}

void Circle::printBaseInfo() const
{
  print();
  std::cout << "Circle area: " << getArea() << "\n";
}

void Circle::print() const
{
  std::cout << "Center position: " << "\n X: " << center_.x
  << " Y: " << center_.y << "\n\n";
}

