#include "rectangle.hpp"
#include <iostream>
#include <cmath>

romanenko::Rectangle::Rectangle(const rectangle_t rect) :
  rect_(rect),
  angle_(0)
{
  if (rect.width <= 0.0 || rect.height <= 0.0)
  {
    throw std::invalid_argument("Dimensions must be positive");
  }
}

double romanenko::Rectangle::getArea() const
{
  return rect_.width * rect_.height;
}

romanenko::rectangle_t romanenko::Rectangle::getFrameRect() const
{
  const double sinus = sin(angle_);
  const double cosinus = cos(angle_);
  const double width = rect_.height * fabs(sinus) + rect_.width * fabs(cosinus);
  const double height = rect_.height * fabs(cosinus) + rect_.width * fabs(sinus);

  return {height, width, rect_.pos};

}

void romanenko::Rectangle::move(double dx, double dy)
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

void romanenko::Rectangle::move(const point_t & point)
{
  rect_.pos = point;
}

void romanenko::Rectangle::scale(double scale)
{
  if (scale <= 0.0)
  {
    throw std::invalid_argument("Scale must be positive");
  }
  rect_.width *= scale;
  rect_.height *= scale;
}

void romanenko::Rectangle::rotate(double angle)
{
  angle_ += angle;
}

romanenko::point_t romanenko::Rectangle::getPos() const
{
  return rect_.pos;
}

void romanenko::Rectangle::showInfo() const
{
  std::cout << "Center";
  showCenter();
  std::cout << "\nWidth: " << rect_.width;
  std::cout << "\nHeight: " << rect_.height;
  std::cout << "\nArea: " << getArea();
}

void romanenko::Rectangle::showCenter() const
{
  std::cout << "\n X: " << rect_.pos.x;
  std::cout << "\n Y: " << rect_.pos.y;
}

void romanenko::Rectangle::showFrameRect() const
{
  rectangle_t rect = getFrameRect();
  std::cout << "\nFrame rectangle";
  std::cout << "\nWidth: " << rect.width;
  std::cout << "\nHeight: " << rect.height;
  std::cout << "\nArea: " << getArea()<< "\n";
}
