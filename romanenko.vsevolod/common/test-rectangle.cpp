#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include <stdexcept>

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(Test_Rectangle)

BOOST_AUTO_TEST_CASE(constRectangleAfterMovingByIncrements)
{
  romanenko::Rectangle testRectangle({1.0, 1.0, {1.0, 1.0}});
  const romanenko::rectangle_t initialRect = testRectangle.getFrameRect();
  const double initialArea = testRectangle.getArea();

  testRectangle.move(5.0, 5.0);

  BOOST_CHECK_CLOSE(initialRect.width, testRectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialRect.height, testRectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(constRectangleAfterMovingToPoint)
{
  romanenko::Rectangle testRectangle({1.0, 1.0, {1.0, 1.0}});
  const romanenko::rectangle_t initialRect = testRectangle.getFrameRect();
  const double initialArea = testRectangle.getArea();

  testRectangle.move({5.0, 5.0});

  BOOST_CHECK_CLOSE(initialRect.width, testRectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialRect.height, testRectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(quadraticChangeRectangleAreaAfterScaling)
{
  romanenko::Rectangle testRectangle({1.0, 1.0, {1.0, 1.0}});
  const double initialArea = testRectangle.getArea();
  const double zoomFactor = 2.0;

  testRectangle.scale(zoomFactor);

  BOOST_CHECK_CLOSE(testRectangle.getArea(), initialArea * zoomFactor * zoomFactor, ACCURACY);
}

BOOST_AUTO_TEST_CASE(constRectangleAfterRotating)
{
  romanenko::Rectangle testRectangle({1.0, 1.0, {1.0, 1.0}});
  const double areaBefore = testRectangle.getArea();
  const romanenko::rectangle_t rectBefore = testRectangle.getFrameRect();

  const double angle = 90;
  testRectangle.rotate(angle);

  double areaAfter = testRectangle.getArea();
  romanenko::rectangle_t rectAfter = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(rectBefore.pos.x, rectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(rectBefore.pos.y, rectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidRectangleParameteres)
{
  BOOST_CHECK_THROW(romanenko::Rectangle({1.0, - 1.0, {1.0, 1.0}}), std::invalid_argument);
  BOOST_CHECK_THROW(romanenko::Rectangle({- 1.0, 1.0, {1.0, 1.0}}), std::invalid_argument);

  romanenko::Rectangle testRectangle({1.0, 1.0, {1.0, 1.0}});
  BOOST_CHECK_THROW(testRectangle.scale(- 2.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
