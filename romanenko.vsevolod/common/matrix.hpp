#ifndef A4_MATRIX_HPP
#define A4_MATRIX_HPP
#include "shape.hpp"
#include "composite-shape.hpp"
#include <memory>

namespace romanenko
{
  class Matrix {
  public:
    Matrix();
    Matrix(const Matrix & obj);
    Matrix(Matrix && obj);
    ~Matrix() = default;

    Matrix & operator =(const Matrix & rhs);
    Matrix & operator =(Matrix && rhs);
    CompositeShape::array_ptr operator [](const size_t index) const;
    bool operator ==(const Matrix & rhs) const;
    bool operator !=(const Matrix & rhs) const;

    void add(CompositeShape::ptr shape, size_t row, size_t column);
    size_t getRows() const;
    size_t getColumns() const;

  private:
    size_t rows_;
    size_t columns_;
    CompositeShape::array_ptr matrix_;
  };
}

#endif //A4_MATRIX_HPP
