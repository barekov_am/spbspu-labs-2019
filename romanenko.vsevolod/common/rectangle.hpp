#ifndef A4_RECTANGLE_HPP
#define A4_RECTANGLE_HPP

#include "shape.hpp"

namespace romanenko
{
  class Rectangle : public Shape {
  public:
    Rectangle(rectangle_t rect);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t & point) override;
    void scale(double scale) override;
    void rotate(double angle) override;
    point_t getPos() const override;
    void showInfo() const override;
    void showCenter() const override;
    void showFrameRect() const override;
  private:
    rectangle_t rect_;
    double angle_;
  };
}
#endif //A4_RECTANGLE_HPP
