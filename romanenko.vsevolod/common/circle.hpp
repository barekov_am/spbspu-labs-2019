#ifndef A4_CIRCLE_HPP
#define A4_CIRCLE_HPP

#include "shape.hpp"

namespace romanenko
{
  class Circle : public Shape {
  public:
    Circle(double radius, point_t point);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t & point) override;
    void scale(double scale) override;
    point_t getPos() const override;
    void showInfo() const override;
    void showCenter() const override;
    void showFrameRect() const override;
    void rotate(double angle) override;
  private:
    double radius_;
    point_t center_;
  };
}
#endif //A4_CIRCLE_HPP
