#ifndef A4_SHAPE_HPP
#define A4_SHAPE_HPP

#include "base-types.hpp"

namespace romanenko
{
  class Shape {
  public:
    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void move(const point_t & point) = 0;
    virtual void scale (double scale) = 0;
    virtual void rotate(double angle) = 0;
    virtual point_t getPos() const = 0;
    virtual void showInfo() const = 0;
    virtual void showCenter() const = 0;
    virtual void showFrameRect() const = 0;
  };
}
#endif //A4_SHAPE_HPP
