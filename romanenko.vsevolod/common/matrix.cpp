#include "matrix.hpp"
#include <stdexcept>

romanenko::Matrix::Matrix() :
  rows_(0),
  columns_(0),
  matrix_()
{ }

romanenko::Matrix::Matrix(const Matrix & obj) :
  rows_(obj.rows_),
  columns_(obj.columns_),
  matrix_(std::make_unique<CompositeShape::ptr[]>(obj.rows_ * obj.columns_))
{
  for(size_t i = 0; i < (rows_ * columns_); i++)
  {
    matrix_[i] = obj.matrix_[i];
  }
}

romanenko::Matrix::Matrix(Matrix && obj) :
  rows_(obj.rows_),
  columns_(obj.columns_),
  matrix_(std::move(obj.matrix_))
{ }

romanenko::Matrix & romanenko::Matrix::operator=(const Matrix & rhs)
{
  if (this != & rhs)
  {
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
    CompositeShape::array_ptr tempMatrix(std::make_unique<CompositeShape::ptr[]>(rhs.rows_ * rhs.columns_));
    for (size_t i = 0; i < (rows_ * columns_); i++)
    {
      tempMatrix[i] = rhs.matrix_[i];
    }
    matrix_.swap(tempMatrix);
  }
  return *this;
}

romanenko::Matrix & romanenko::Matrix::operator=(Matrix && rhs)
{
  if (this != & rhs)
  {
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
    matrix_ = std::move(rhs.matrix_);
  }
  return *this;
}

std::unique_ptr<std::shared_ptr<romanenko::Shape>[]> romanenko::Matrix::operator [](size_t index) const
{
  if (index >= rows_)
  {
    throw std::out_of_range("Index is out of range");
  }

  CompositeShape::array_ptr tempMatrix(std::make_unique<std::shared_ptr<Shape>[]>(columns_));
  for (size_t i = 0; i < columns_; i++)
  {
    tempMatrix[i] = matrix_[index * columns_ + i];
  }

  return tempMatrix;
}

bool romanenko::Matrix::operator ==(const Matrix &rhs) const
{
  if (rows_ != rhs.rows_ || columns_ != rhs.columns_)
  {
    return false;
  }

  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (matrix_[i] != rhs.matrix_[i])
    {
      return false;
    }
  }

  return true;
}

bool romanenko::Matrix::operator !=(const Matrix &rhs) const
{
  return !(*this == rhs);
}

void romanenko::Matrix::add(std::shared_ptr<Shape> shape, size_t row, size_t column)
{
  if (!shape)
  {
    throw std::invalid_argument("Can not add nullptr");
  }
  size_t newRows = (row == rows_) ? (rows_ + 1) : (rows_);
  size_t newColumns = (column == columns_) ? (columns_ + 1) : (columns_);

  CompositeShape::array_ptr tempMatrix(std::make_unique<CompositeShape::ptr[]>(newRows * newColumns));

  for (size_t i = 0; i < newRows; i++)
  {
    for (size_t j = 0; j < newColumns; j++)
    {
      if (i == rows_ || j == columns_)
      {
        tempMatrix[i * newColumns + j] = nullptr;
      }
      else
      {
        tempMatrix[i * newColumns + j] = matrix_[i * columns_ + j];
      }
    }
  }
  tempMatrix[row * newColumns + column] = shape;
  matrix_.swap(tempMatrix);
  rows_ = newRows;
  columns_ = newColumns;
}

size_t romanenko::Matrix::getColumns() const
{
  return columns_;
}

size_t romanenko::Matrix::getRows() const
{
  return rows_;
}
