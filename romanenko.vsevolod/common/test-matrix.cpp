#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "partition.hpp"
#include "matrix.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include <stdexcept>

BOOST_AUTO_TEST_SUITE(matrixTestSuite)

BOOST_AUTO_TEST_CASE(copyAndMoveConstructor)
{
  romanenko::Rectangle r1({4.0, 4.0, {-1.0, -1.0}});
  romanenko::Circle c1(1.0, {3.0, 2.0});
  romanenko::CompositeShape testShapes(std::make_shared<romanenko::Rectangle>(r1));
  testShapes.addShape(std::make_shared<romanenko::Circle>(c1));

  romanenko::Matrix testMatrix = romanenko::divide(testShapes);

  BOOST_CHECK_NO_THROW(romanenko::Matrix testMatrix2(testMatrix));
  BOOST_CHECK_NO_THROW(romanenko::Matrix testMatrix3(std::move(testMatrix)));
}

BOOST_AUTO_TEST_CASE(operatorTests)
{
  romanenko::Rectangle r1({4.0, 4.0, {-1.0, -1.0}});
  romanenko::Circle c1(1.0, {3.0, 2.0});
  romanenko::CompositeShape testShapes1(std::make_shared<romanenko::Rectangle>(r1));
  testShapes1.addShape(std::make_shared<romanenko::Circle>(c1));

  romanenko::Matrix testMatrix1 = romanenko::divide(testShapes1);

  BOOST_CHECK_THROW(testMatrix1[100], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix1[-1], std::out_of_range);

  BOOST_CHECK_NO_THROW(romanenko::Matrix testMatrix2 = testMatrix1);
  BOOST_CHECK_NO_THROW(romanenko::Matrix testMatrix3 = std::move(testMatrix1));

  romanenko::Rectangle r2({4.0, 4.0, {-1.0, -1.0}});
  romanenko::CompositeShape testShapes2(std::make_shared<romanenko::Rectangle>(r2));

  romanenko::Matrix testMatrix4 = romanenko::divide(testShapes2);
  romanenko::Matrix testMatrix5 = romanenko::divide(testShapes2);

  BOOST_CHECK(testMatrix4 == testMatrix5);

  BOOST_CHECK(testMatrix4 != testMatrix1);
  BOOST_CHECK(testMatrix5 != testMatrix1);
}

BOOST_AUTO_TEST_SUITE_END()
