#include "partition.hpp"
#include <cmath>

bool romanenko::overlap(const romanenko::Shape & firstShape, const romanenko::Shape & secondShape)
{
  const romanenko::rectangle_t firstFrame = firstShape.getFrameRect();
  const romanenko::rectangle_t secondFrame = secondShape.getFrameRect();
  if (std::abs(firstFrame.pos.x - secondFrame.pos.x) > (firstFrame.width + secondFrame.width) / 2)
  {
    return false;
  }
  if (std::abs(firstFrame.pos.y - secondFrame.pos.y) > (firstFrame.height + secondFrame.height) / 2)
  {
    return false;
  }
  return true;
}

romanenko::Matrix romanenko::divide(const romanenko::CompositeShape & compositeShape)
{
  romanenko::Matrix matrix;
  for (size_t i = 0; i < compositeShape.getSize(); ++i)
  {
    size_t rowsCount = 0;
    size_t columnsCount = 0;
    for (size_t j = matrix.getRows(); j-- > 0;)
    {
      bool between = false;
      for (size_t k = 0; k < matrix.getColumns(); ++k)
      {
        if (matrix[j][k] == nullptr)
        {
          columnsCount = k;
          break;
        }
        if (overlap(* compositeShape[i], * matrix[j][k]))
        {
          rowsCount = j + 1;
          between = true;
          break;
        }
        if (k == matrix.getColumns() - 1)
        {
          columnsCount = k + 1;
        }
      }
      if (between)
      {
        break;
      }
    }
    matrix.add(compositeShape[i], rowsCount, columnsCount);
  }
  return matrix;
}
