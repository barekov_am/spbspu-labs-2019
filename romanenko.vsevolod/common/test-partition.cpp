#include <boost/test/auto_unit_test.hpp>
#include "partition.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(separationTestSuite)

BOOST_AUTO_TEST_CASE(overlapingOfShapes)
{
  romanenko::Rectangle testRectangle1({4.0, 4.0, {0.0, 0.0}});
  romanenko::Circle testCircle1(1.0, {-5.0, 1.0});
  romanenko::Rectangle testRectangle2({4.0, 4.0, {-2.0, 1.0}});
  romanenko::Circle testCircle2(1.0, {-2.0, 0.0});

  BOOST_CHECK(romanenko::overlap(testRectangle1, testCircle2));
  BOOST_CHECK(romanenko::overlap(testRectangle1, testRectangle2));
  BOOST_CHECK(romanenko::overlap(testRectangle2, testCircle1));
  BOOST_CHECK(romanenko::overlap(testRectangle2, testCircle2));

  BOOST_CHECK(!romanenko::overlap(testRectangle1, testCircle1));
  BOOST_CHECK(!romanenko::overlap(testCircle1, testCircle2));
}

BOOST_AUTO_TEST_CASE(emptyCompositedivideing)
{
  romanenko::Matrix testMatrix = romanenko::divide(romanenko::CompositeShape());

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 0);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 0);
  }

BOOST_AUTO_TEST_CASE(correctCompositeSeparation)
{
  romanenko::Rectangle r1({4.0, 4.0, {-1.0, -1.0}});
  romanenko::Circle c1(1.0, {3.0, 2.0});
  romanenko::Rectangle r2({4.0, 4.0, {1.0, 2.0}});
  romanenko::Rectangle r3({4.0, 4.0, {1.0, -4.0}});
  romanenko::CompositeShape testShapes(std::make_shared<romanenko::Rectangle>(r1));
  testShapes.addShape(std::make_shared<romanenko::Circle>(c1));
  testShapes.addShape(std::make_shared<romanenko::Rectangle>(r2));
  testShapes.addShape(std::make_shared<romanenko::Rectangle>(r3));

  romanenko::Matrix testMatrix = romanenko::divide(testShapes);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 2);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 2);
  BOOST_CHECK_EQUAL(testMatrix[0][0], testShapes[0]);
  BOOST_CHECK_EQUAL(testMatrix[0][1], testShapes[1]);
  BOOST_CHECK_EQUAL(testMatrix[1][0], testShapes[2]);
  BOOST_CHECK_EQUAL(testMatrix[1][1], testShapes[3]);
}

BOOST_AUTO_TEST_SUITE_END()
