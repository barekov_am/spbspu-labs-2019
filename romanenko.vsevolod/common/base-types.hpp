#ifndef A4_BASE_TYPES_HPP
#define A4_BASE_TYPES_HPP

namespace romanenko
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    double width;
    double height;
    point_t pos;
  };
    double sinus (double angle);
    double cosinus (double angle);

}
#endif //A4_BASE_TYPES_HPP
