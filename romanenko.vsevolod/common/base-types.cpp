#include "base-types.hpp"
#include <cmath>

double romanenko::sinus (double angle)
{
    return std::sin(angle * M_PI / 180);
};

double romanenko::cosinus (double angle)
{
    return std::cos(angle * M_PI / 180);
};
