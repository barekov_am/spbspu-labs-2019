#include "circle.hpp"
#include <iostream>
#include <cmath>

romanenko::Circle::Circle(double radius, point_t point) :
  radius_(radius),
  center_(point)
{
  if (radius <= 0.0)
  {
    throw std::invalid_argument("Radius can not be less than 0");
  }
}

double romanenko::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

romanenko::rectangle_t romanenko::Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, center_};
}

romanenko::point_t romanenko::Circle::getPos() const
{
  return center_;
}

void romanenko::Circle::move(const point_t & point)
{
  center_ = point;
}

void romanenko::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void romanenko::Circle::scale(double scale)
{
  if (scale <= 0.0)
  {
    throw std::invalid_argument("Scale must be positive");
  }
  radius_ *= scale;
}

void romanenko::Circle::rotate(double )
{
}

void romanenko::Circle::showInfo() const
{
  std::cout << "\n Center";
  showCenter();
  std::cout << "\n Radius: " << radius_ <<"\n";
  std::cout << "Area: " << getArea();
}

void romanenko::Circle::showCenter() const
{
  std::cout << "\n X: " << center_.x;
  std::cout << "\n Y: " << center_.y;
}

void romanenko::Circle::showFrameRect() const
{
  std::cout << "\nFrame rectangle of circle";
  std::cout << "\nWidth: " << radius_ * 2;
  std::cout << "\nHeight: " << radius_ * 2;
  std::cout << "\nArea: " << getArea() << "\n";
}
