#include <list>
#include <iostream>

void printLeft(std::list<int>::iterator, std::list<int>::iterator);
void printRight(std::list<int>::iterator, std::list<int>::iterator);

void printLeft(std::list<int>::iterator left, std::list<int>::iterator right)
{
  if (left == right)
  {
    return;
  }

  std::cout << *left << ' ';
  ++left;
  printRight(left, right);
}

void printRight(std::list<int>::iterator left, std::list<int>::iterator right)
{
  if (left == right)
  {
    return;
  }

  std::cout << *std::prev(right) << ' ';
  --right;
  printLeft(left, right);
}

void task2()
{
  const int minValue = 1;
  const int maxValue = 20;
  const std::size_t maxLength = 20;

  std::list<int> list;

  int currNum = -1;
  while (std::cin && !(std::cin >> currNum).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error while reading data.");
    }

    if (currNum < minValue || currNum > maxValue)
    {
      throw std::invalid_argument("Number out of range.");
    }

    if (list.size() == maxLength)
    {
      throw std::invalid_argument("Too many numbers.");
    }

    list.push_back(currNum);
  }

  printLeft(list.begin(), list.end());
  std::cout << '\n';
}
