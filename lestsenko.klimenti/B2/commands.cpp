#include <iostream>
#include <sstream>
#include <algorithm>
#include <locale>
#include "priority-queue.hpp"

template<typename Char, typename CharTraits>
std::basic_istream<Char, CharTraits>& blank(std::basic_istream<Char, CharTraits>& is)
{
  while (std::isblank(static_cast<char>(is.peek()), std::locale("")))
  {
    is.ignore();
  }

  return is;
}

std::istream& operator>>(std::istream& is, PriorityQueue<std::string>::ElementPriority& priority)
{
  static const struct
  {
    const char* name;
    PriorityQueue<std::string>::ElementPriority priority;
  } priorities[] = {
      {"high", PriorityQueue<std::string>::HIGH},
      {"normal", PriorityQueue<std::string>::NORMAL},
      {"low", PriorityQueue<std::string>::LOW}};

  std::string priorityStr;
  is >> priorityStr;

  auto findPriority = std::find_if(
      std::begin(priorities), std::end(priorities), [&](const auto& el) { return priorityStr == el.name; });

  if (findPriority == std::end(priorities))
  {
    is.setstate(std::ios::failbit);
    return is;
  }

  priority = findPriority->priority;
  return is;
}

void addToQueue(PriorityQueue<std::string>& queue, std::istream& args)
{
  PriorityQueue<std::string>::ElementPriority priority;
  if (!(args >> priority))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::string data;
  getline(args >> blank, data);
  if (data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.add(data, priority);
}

void getFromQueue(PriorityQueue<std::string>& queue, std::istream& args)
{
  if (!args.eof())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (queue.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  queue.process([](const auto& el) { std::cout << el << '\n'; });
}

void accelerateQueue(PriorityQueue<std::string>& queue, std::istream& args)
{
  if (!args.eof())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.accelerate();
}
