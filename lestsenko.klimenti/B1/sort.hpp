#ifndef SORT_HPP
#define SORT_HPP

#include <iostream>
#include <functional>
#include <cstring>
#include "access.hpp"

template <template <class T> class Access, typename T>
void sort(T& collection, const char* direction)
{
  using value_type = typename T::value_type;

  std::function<bool(value_type, value_type)> compare = strcmp(direction, "ascending") == 0
      ? std::function<bool(value_type, value_type)>(std::less<value_type>())
      : strcmp(direction, "descending") == 0
      ? std::function<bool(value_type, value_type)>(std::greater<value_type>())
      : throw std::invalid_argument("Incorrect sorting direction.");

  const auto begin = Access<T>::begin(collection);
  const auto end = Access<T>::end(collection);

  for (auto i = begin; i != end; ++i)
  {
    auto tmp = i;
    for (auto j = Access<T>::next(i); j != end; ++j)
    {
      if (compare(Access<T>::getElement(collection, j), Access<T>::getElement(collection, tmp)))
      {
        tmp = j;
      }
    }

    if (tmp != i)
    {
      std::swap(Access<T>::getElement(collection, tmp), Access<T>::getElement(collection, i));
    }
  }
}

template<class T>
void print(const T& arr)
{
  for (const auto& elem: arr)
  {
    std::cout << elem << " ";
  }
  std::cout << '\n';
}

#endif
