#include <fstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <vector>

const size_t Capasity = 1024;

void task2(const char* filename)
{
  using charArray = std::unique_ptr<char[], decltype(&free)>;

  std::ifstream input(filename);
  if (!input)
  {
    throw std::ios_base::failure("Failed to open file.");
  }

  size_t size = Capasity;

  charArray arr(static_cast<char*>(malloc(size)), &free);

  size_t i = 0;
  while (input)
  {
    input.read(&arr[i], Capasity);
    i += input.gcount();

    if (input.gcount() == Capasity) 
    {
      size += Capasity;
      charArray temp(static_cast<char*>(realloc(arr.get(), size)), &free);

      if (temp)
      {
        arr.release();
        std::swap(arr, temp);
      }
      else
      {
        throw std::runtime_error("Failed to reallocate.");
      }
    }
  }
  input.close();

  if (input.is_open())
  {
    throw std::ios_base::failure("Failed to close file.");
  }
  
  std::vector<char> vector(&arr[0], &arr[i]);

  for (const auto& it: vector)
  {
    std::cout << it;
  } 
}
