#include <cstring>
#include <forward_list>
#include <vector>
#include "sort.hpp"

void task1(const char* direction)
{
  std::vector<int> vectorA;
  
  int num = 0;
  while (std::cin >> num)
  {
    vectorA.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Fail while reading data.");
  }

  std::vector<int> vectorB(vectorA);
  std::forward_list<int> list(vectorA.begin(), vectorA.end());
  
  sort<brackets_access>(vectorA, direction);
  sort<at_access>(vectorB, direction);
  sort<iterator_access>(list, direction);
  
  print(vectorA);
  print(vectorB);
  print(list);
}
