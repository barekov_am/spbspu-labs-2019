#ifndef ACCESS_HPP
#define ACCESS_HPP

#include <cstddef>

template<typename T>
struct brackets_access
{
  
  static typename T::reference getElement(T& collection, std::size_t index)
  {
    return collection[index];
  }

  static std::size_t begin(const T&)
  {
    return 0;
  }

  static std::size_t end(const T& collection)
  {
    return collection.size();
  }

  static std::size_t next(const std::size_t index)
  {
    return index + 1;
  }
};

template<typename T>
struct at_access
{
  static typename T::reference getElement(T& collection, std::size_t index)
  {
    return collection.at(index);
  }

  static std::size_t begin(const T&)
  {
    return 0;
  }

  static std::size_t end(const T& collection)
  {
    return collection.size();
  }

  static std::size_t next(const std::size_t index)
  {
    return index + 1;
  }
};

template<typename T>
struct iterator_access
{
  static typename T::reference getElement(T&, typename T::iterator iter)
  {
    return *iter;
  }

  static typename T::iterator begin(T& collection)
  {
    return collection.begin();
  }

  static typename T::iterator end(T& collection)
  {
    return collection.end();
  }

  static typename T::iterator next(typename T::iterator iterator)
  {
    return ++iterator;
  }
};

#endif
