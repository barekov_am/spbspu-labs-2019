#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
  lestsenko::Rectangle rectangle(1, 2, {0 , 0});
  lestsenko::Circle circle(2, {4 , 4});
  lestsenko::Triangle triangle({0, 0}, {-1, -2}, {-3 , -4});

  lestsenko::CompositeShape cs(std::make_shared<lestsenko::Rectangle>(rectangle));
  
  std::cout << "CompositeShape first pos: " << cs.getPos().x << "," << cs.getPos().y
      << "\nCompositeShape area: " << cs.getArea();
  
  cs.addShape(std::make_shared<lestsenko::Circle>(circle));
  cs.addShape(std::make_shared<lestsenko::Triangle>(triangle));

  std::cout << "\nCompositeShape new pos: " << cs.getPos().x << "," << cs.getPos().y
      << "\nCompositeShape new area: " << cs.getArea();
  
  cs.move({3, 4});
  std::cout << "\nCompositeShape second pos: " << cs.getPos().x << ", " << cs.getPos().y;

  cs.move(5, 6);
  std::cout << "\nCompositeShape third pos: " << cs.getPos().x << ", " << cs.getPos().y;

  cs.scale(0.5);
  std::cout << "\nCompositeShape area after scaling: " << cs.getArea() << "\n";

  return 0;
}
