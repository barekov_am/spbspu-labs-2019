#include <iostream>
#include "base-types.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

int main()
{
  lestsenko::Circle circle1(5, {1, 2});

  lestsenko::Shape *figure = &circle1;

  std::cout << "Circle1 first pos: " << figure->getPos().x << "," << figure->getPos().y
      << "\nCircle1 area: " << figure->getArea();

  lestsenko::rectangle_t circleRect = figure->getFrameRect();
  std::cout << "\nRectangle surrounding Circle1: h: " << circleRect.height
      << ", w: " << circleRect.width
      << ", x: " << circleRect.pos.x
      << ", y: " << circleRect.pos.y;

  figure->move({3, 4});
  std::cout << "\nCircle1 second pos: " << figure->getPos().x << ", " << figure->getPos().y;

  figure->move(5, 6);
  std::cout << "\nCircle1 third pos: " << figure->getPos().x << ", " << figure->getPos().y;

  figure->scale(0.5);
  std::cout << "\nCircle1 area after scaling: " << figure->getArea();

  lestsenko::Rectangle rectangle1(3, 4, {5, 6});

  figure = &rectangle1;

  std::cout << "\n\nRectangle1 first pos: " << figure->getPos().x << ", " << figure->getPos().y
      << "\nRectangle1 area: " << figure->getArea();

  figure->move({3, 4});
  std::cout << "\nRectangle1 second pos: " << figure->getPos().x << ", " << figure->getPos().y;

  figure->move(5, 6);
  std::cout << "\nRectangle1 third pos: " << figure->getPos().x << ", " << figure->getPos().y;

  figure->scale(2);
  std::cout << "\nRectanlge1 area after scaling: " << figure->getArea();
  
  lestsenko::Triangle triangle1({0, 1}, {1, 0}, {3, 3});

  figure = &triangle1;

  std::cout << "\n\nTriangle1 first pos: " << figure->getPos().x << "," << figure->getPos().y
      << "\nTriangle1 area: " << figure->getArea();

  lestsenko::rectangle_t triangleRect = figure->getFrameRect();
  std::cout << "\nRectangle surrounding Triangle1: h: " << triangleRect.height
      << ", w: " << triangleRect.width
      << ", x: " << triangleRect.pos.x
      << ", y: " << triangleRect.pos.y;

  figure->move({3, 4});
  std::cout << "\nTriangle1 second pos: " << figure->getPos().x << ", " << figure->getPos().y;

  figure->move(5, 6);
  std::cout << "\nTriangle1 third pos: " << figure->getPos().x << ", " << figure->getPos().y;

  figure->scale(2);
  std::cout << "\nTriangle1 area after scaling: " << figure->getArea() << "\n";

  return 0;
}
