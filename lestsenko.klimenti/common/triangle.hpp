#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

namespace lestsenko
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &ptA, const point_t &ptB, const point_t &ptC);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &newPos) override;
    void move(double dx, double dy) override;
    point_t getPos() const override;
    void scale(double ratio) override;
    void rotate(double angle) override;

  private:
    point_t ptA_;
    point_t ptB_;
    point_t ptC_;
    point_t center_;
  };
}

#endif // TRIANGLE_HPP
