#include "circle.hpp"
#include <stdexcept>
#include <cmath>

lestsenko::Circle::Circle(double radius, const point_t &center) :
  radius_(radius), 
  center_(center)
{
  if (radius_ < 0.0)
  {
    throw std::invalid_argument("radius can not be < 0.0");
  }
}

double lestsenko::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

lestsenko::rectangle_t lestsenko::Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, center_};
}

void lestsenko::Circle::move(const point_t &newPos)
{
  center_ = newPos;
}

void lestsenko::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

lestsenko::point_t lestsenko::Circle::getPos() const
{
  return center_;
}

double lestsenko::Circle::getRad() const
{
  return radius_;
}

void lestsenko::Circle::scale(double ratio)
{
  if (ratio <= 0.0)
  {
    throw std::invalid_argument("ratio can not be < 0.0");
  }
  radius_ *= ratio;
}

void lestsenko::Circle::rotate(double)
{ }
