#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "triangle.hpp"

const double CALC_ERR = 0.01;

BOOST_AUTO_TEST_SUITE(triangleTests)

BOOST_AUTO_TEST_CASE(triangleMovePoint)
{
  lestsenko::Triangle triangle({0, 1}, {1, 0}, {3, 3});
  const lestsenko::point_t testPoint = {15, 20};
  triangle.move(testPoint);
  BOOST_CHECK_CLOSE(triangle.getPos().x, testPoint.x, CALC_ERR);
  BOOST_CHECK_CLOSE(triangle.getPos().y, testPoint.y, CALC_ERR);
}

BOOST_AUTO_TEST_CASE(triangleAreaAfterMove)
{
  lestsenko::Triangle triangle({0, 1}, {1, 0}, {3, 3});
  const double areaBefore = triangle.getArea();
  triangle.move(1, 2);
  BOOST_CHECK_CLOSE(areaBefore, triangle.getArea(), CALC_ERR);
}

BOOST_AUTO_TEST_CASE(triangleScale)
{
  const lestsenko::point_t testPtA = {4, 5};
  const lestsenko::point_t testPtB = {-1, 9};
  const lestsenko::point_t testPtC = {0, 3};
  lestsenko::Triangle triangle(testPtA, testPtB, testPtC);
  const double areaBefore = triangle.getArea();
  const double testRatio = 2;
  triangle.scale(testRatio);
  BOOST_CHECK_CLOSE(triangle.getArea(), areaBefore * testRatio * testRatio, CALC_ERR);
}

BOOST_AUTO_TEST_CASE(triangleConstructorInvalidArgument)
{
  BOOST_CHECK_THROW(lestsenko::Triangle({1, 0}, {2, 0}, {3, 0}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(triangleScaleInvalidArgument)
{
  lestsenko::Triangle triangle({0, 1}, {1, 0}, {3, 3});
  BOOST_CHECK_THROW(triangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
