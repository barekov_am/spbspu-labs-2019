#include <stdexcept>
#include <memory>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double CALC_ERR = 0.01;

BOOST_AUTO_TEST_SUITE(compositeShapeTests)

BOOST_AUTO_TEST_CASE(compoositeShapeMove)
{
  lestsenko::Rectangle obj1(1, 2, {0 , 0});
  lestsenko::Circle obj2(2, {4 , 4});
  lestsenko::Triangle obj3({0, 0}, {-1, -2}, {-3 , -4});
  lestsenko::CompositeShape cs(std::make_shared<lestsenko::Rectangle>(obj1));
  cs.addShape(std::make_shared<lestsenko::Circle>(obj2));
  cs.addShape(std::make_shared<lestsenko::Triangle>(obj3));
  const lestsenko::point_t testPoint = {12, 12};
  const double testArea = cs.getArea();
  cs.move(testPoint);
  BOOST_CHECK_CLOSE(cs.getPos().x, testPoint.x, CALC_ERR);
  BOOST_CHECK_CLOSE(cs.getPos().y, testPoint.y, CALC_ERR);
  BOOST_CHECK_CLOSE(cs.getArea(), testArea, CALC_ERR);
}

BOOST_AUTO_TEST_CASE(compositeShapeCopy)
{
  lestsenko::Rectangle obj1(10, 20, {0 , 0});
  lestsenko::CompositeShape cs1(std::make_shared<lestsenko::Rectangle>(obj1));
  lestsenko::CompositeShape cs2(cs1);
  lestsenko::CompositeShape cs3 = cs1;
  BOOST_CHECK_CLOSE(cs1.getArea(), cs2.getArea(), CALC_ERR);
  BOOST_CHECK_CLOSE(cs1.getArea(), cs3.getArea(), CALC_ERR);
}

BOOST_AUTO_TEST_CASE(compositeShapeAssignment)
{
  lestsenko::Rectangle obj1(10, 20, {0 , 0});
  lestsenko::CompositeShape cs1(std::make_shared<lestsenko::Rectangle>(obj1));
  lestsenko::CompositeShape cs2(std::make_shared<lestsenko::Rectangle>(obj1));
  const double testArea1 = cs1.getArea();
  const double testArea2 = cs1.getArea();
  lestsenko::CompositeShape cs3(std::move(cs1));
  lestsenko::CompositeShape cs4;
  cs4 = std::move(cs2);
  BOOST_CHECK_CLOSE(testArea1, cs3.getArea(), CALC_ERR);
  BOOST_CHECK_CLOSE(testArea2, cs4.getArea(), CALC_ERR);
}

BOOST_AUTO_TEST_CASE(compositeShapeArea)
{
  lestsenko::Rectangle obj1(1, 2, {0 , 0});
  lestsenko::Circle obj2(2, {4 , 4});
  lestsenko::Triangle obj3({0, 0}, {-1, -2}, {-3 , -4});
  const double testArea = (obj1.getArea()) + (obj2.getArea()) + (obj3.getArea());
  lestsenko::CompositeShape cs(std::make_shared<lestsenko::Rectangle>(obj1));
  cs.addShape(std::make_shared<lestsenko::Circle>(obj2));
  cs.addShape(std::make_shared<lestsenko::Triangle>(obj3));
  BOOST_CHECK_CLOSE(cs.getArea(), testArea, CALC_ERR);
}

BOOST_AUTO_TEST_CASE(compositeShapeScale)
{
  lestsenko::Rectangle obj1(1, 2, {0 , 0});
  lestsenko::Circle obj2(2, {4 , 4});
  lestsenko::Triangle obj3({0, 0}, {-1, -2}, {-3 , -4});
  const double testArea = (obj1.getArea()) + (obj2.getArea()) + (obj3.getArea());
  lestsenko::CompositeShape cs(std::make_shared<lestsenko::Rectangle>(obj1));
  cs.addShape(std::make_shared<lestsenko::Circle>(obj2));
  cs.addShape(std::make_shared<lestsenko::Triangle>(obj3));
  const double scaleRatio = 2;
  cs.scale(scaleRatio);
  BOOST_CHECK_CLOSE(cs.getArea(), testArea * scaleRatio * scaleRatio, CALC_ERR);
}

BOOST_AUTO_TEST_CASE(compositeShapeAddShape)
{
  lestsenko::Rectangle obj1(10, 20, {0 , 0});
  lestsenko::Rectangle obj2(20, 30, {1 , 2});
  lestsenko::CompositeShape cs(std::make_shared<lestsenko::Rectangle>(obj1));
  BOOST_REQUIRE_EQUAL(cs.getLength(), 1);
  cs.addShape(std::make_shared<lestsenko::Rectangle>(obj2));
  BOOST_REQUIRE_EQUAL(cs.getLength(), 2);
}

BOOST_AUTO_TEST_CASE(compositeShapeRemoveShape)
{
  lestsenko::Rectangle obj1(10, 20, {0 , 0});
  lestsenko::Rectangle obj2(20, 30, {1 , 2});
  lestsenko::CompositeShape cs(std::make_shared<lestsenko::Rectangle>(obj1));
  cs.addShape(std::make_shared<lestsenko::Rectangle>(obj2));
  BOOST_REQUIRE_EQUAL(cs.getLength(), 2);
  cs.removeShape(1);
  BOOST_REQUIRE_EQUAL(cs.getLength(), 1);
}

BOOST_AUTO_TEST_CASE(compositeShapeClear)
{
  lestsenko::Rectangle obj1(10, 20, {0 , 0});
  lestsenko::Rectangle obj2(20, 30, {1 , 2});
  lestsenko::CompositeShape cs(std::make_shared<lestsenko::Rectangle>(obj1));
  cs.addShape(std::make_shared<lestsenko::Rectangle>(obj2));
  cs.clear();
  BOOST_REQUIRE_EQUAL(cs.getLength(), 0);
}

BOOST_AUTO_TEST_CASE(compositeShapeRemoveShapeInvalidArgument)
{
  lestsenko::Rectangle obj1(10, 20, {0 , 0});
  lestsenko::CompositeShape cs(std::make_shared<lestsenko::Rectangle>(obj1));
  BOOST_CHECK_THROW(cs.removeShape(2), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleInvalidArgument)
{
  lestsenko::Rectangle obj1(10, 20, {0 , 0});
  lestsenko::CompositeShape cs(std::make_shared<lestsenko::Rectangle>(obj1));
  BOOST_CHECK_THROW(cs.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
