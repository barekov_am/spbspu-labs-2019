#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

lestsenko::Rectangle::Rectangle(double height, double width, const point_t &center) :
  height_(height),
  width_(width),
  center_(center),
  angle_(0)
{
  if ((height_ < 0.0) || (width_ < 0.0))
  {
    throw std::invalid_argument("height / width can not be < 0");
  }
}

double lestsenko::Rectangle::getArea() const
{
  return height_ * width_;
}

lestsenko::rectangle_t lestsenko::Rectangle::getFrameRect() const
{
  const double sin = std::sin(angle_ * M_PI / 180);
  const double cos = std::cos(angle_ * M_PI / 180);
  const double width = height_ * fabs(sin) + width_ * fabs(cos);
  const double height = height_ * fabs(cos) + width_ * fabs(sin);

  return {height, width, center_};
}

void lestsenko::Rectangle::move(const point_t &newPos)
{
  center_ = newPos;
}

void lestsenko::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

lestsenko::point_t lestsenko::Rectangle::getPos() const
{
  return center_;
}

double lestsenko::Rectangle::getHeight() const
{
  return height_;
}

double lestsenko::Rectangle::getWidth() const
{
  return width_;
}

void lestsenko::Rectangle::scale(double ratio)
{
  if (ratio <= 0.0)
  {
    throw std::invalid_argument("ratio can not be <= 0");
  }
  height_ *= ratio;
  width_ *= ratio;
}

void lestsenko::Rectangle::rotate(double angle)
{
  angle_ += angle;
  while (angle_ >= 360.0)
  {
    angle_ -= 360.0;
  }
}
