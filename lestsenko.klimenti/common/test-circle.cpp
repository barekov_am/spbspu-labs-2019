#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "circle.hpp"

const double CALC_ERR = 0.01;

BOOST_AUTO_TEST_SUITE(circleTests)

BOOST_AUTO_TEST_CASE(circleMovePoint)
{
  lestsenko::Circle circle(5, {1, 2});
  const lestsenko::point_t testPoint = {15, 20};
  circle.move(testPoint);
  BOOST_CHECK_CLOSE(circle.getPos().x, testPoint.x, CALC_ERR);
  BOOST_CHECK_CLOSE(circle.getPos().y, testPoint.y, CALC_ERR);
}

BOOST_AUTO_TEST_CASE(circleAreaAfterMove)
{
  lestsenko::Circle circle(5, {1, 2});
  const double areaBefore = circle.getArea();
  circle.move({2, 4});
  BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), CALC_ERR);
}

BOOST_AUTO_TEST_CASE(circleScale)
{
  const double testRadius = 5;
  lestsenko::Circle circle(testRadius, {1, 2});
  const double testRatio = 2;
  circle.scale(testRatio);
  BOOST_CHECK_CLOSE(circle.getArea(), testRadius * testRadius * testRatio * testRatio * M_PI, CALC_ERR);
}

BOOST_AUTO_TEST_CASE(circleFrameRect)
{
  lestsenko::Circle circle(5, {1, 2});
  const lestsenko::rectangle_t testRect = circle.getFrameRect();
  BOOST_CHECK_CLOSE(testRect.height, circle.getRad() * 2, CALC_ERR);
  BOOST_CHECK_CLOSE(testRect.width, circle.getRad() * 2, CALC_ERR);
  BOOST_CHECK_CLOSE(testRect.pos.x, circle.getPos().x, CALC_ERR);
  BOOST_CHECK_CLOSE(testRect.pos.y, circle.getPos().y, CALC_ERR);
}

BOOST_AUTO_TEST_CASE(cirlceConstructorInvalidArgument)
{
  BOOST_CHECK_THROW(lestsenko::Circle(-1 , {1, 2}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circleScaleInvalidArgument)
{
  lestsenko::Circle circle(5, {1, 2});
  BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
