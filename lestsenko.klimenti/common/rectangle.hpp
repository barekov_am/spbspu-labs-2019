#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace lestsenko
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(double height, double width, const point_t &center);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &newPos) override;
    void move(double dx, double dy) override;
    point_t getPos() const override;
    double getHeight() const;
    double getWidth() const;
    void scale(double ratio) override;
    void rotate(double angle) override;

  private:
    double height_;
    double width_;
    point_t center_;
    double angle_;
  };
}
#endif // RECTANGLE_HPP
