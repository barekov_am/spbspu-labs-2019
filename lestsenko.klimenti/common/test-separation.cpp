#include <boost/test/auto_unit_test.hpp>
#include "separation.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(separationTestSuite)

  BOOST_AUTO_TEST_CASE(overlapingOfShapes)
  {
    lestsenko::Rectangle testRectangle1(4.0, 4.0, {0.0, 0.0});
    lestsenko::Circle testCircle1(1.0, {-5.0, 1.0});
    lestsenko::Rectangle testRectangle2(4.0, 4.0, {-2.0, 1.0});
    lestsenko::Circle testCircle2(1.0, {-2.0, 0.0});

    BOOST_CHECK(lestsenko::overlap(testRectangle1, testCircle2));
    BOOST_CHECK(lestsenko::overlap(testRectangle1, testRectangle2));
    BOOST_CHECK(lestsenko::overlap(testRectangle2, testCircle1));
    BOOST_CHECK(lestsenko::overlap(testRectangle2, testCircle2));

    BOOST_CHECK(!lestsenko::overlap(testRectangle1, testCircle1));
    BOOST_CHECK(!lestsenko::overlap(testCircle1, testCircle2));
  }

  BOOST_AUTO_TEST_CASE(emptyCompositedivideing)
  {
    lestsenko::Matrix testMatrix = lestsenko::divide(lestsenko::CompositeShape());

    BOOST_CHECK_EQUAL(testMatrix.getRows(), 0);
    BOOST_CHECK_EQUAL(testMatrix.getColumns(), 0);
  }

  BOOST_AUTO_TEST_CASE(correctCompositeSeparation)
  {
    lestsenko::Rectangle r1(4.0, 4.0, {-1.0, -1.0});
    lestsenko::Circle c1(1.0, {3.0, 2.0});
    lestsenko::Rectangle r2(4.0, 4.0, {1.0, 2.0});
    lestsenko::Rectangle r3(4.0, 4.0, {1.0, -4.0});
    lestsenko::CompositeShape testShapes(std::make_shared<lestsenko::Rectangle>(r1));
    testShapes.addShape(std::make_shared<lestsenko::Circle>(c1));
    testShapes.addShape(std::make_shared<lestsenko::Rectangle>(r2));
    testShapes.addShape(std::make_shared<lestsenko::Rectangle>(r3));

    lestsenko::Matrix testMatrix = lestsenko::divide(testShapes);

    BOOST_CHECK_EQUAL(testMatrix.getRows(), 2);
    BOOST_CHECK_EQUAL(testMatrix.getColumns(), 2);
    BOOST_CHECK_EQUAL(testMatrix[0][0], testShapes[0]);
    BOOST_CHECK_EQUAL(testMatrix[0][1], testShapes[1]);
    BOOST_CHECK_EQUAL(testMatrix[1][0], testShapes[2]);
    BOOST_CHECK_EQUAL(testMatrix[1][1], testShapes[3]);
  }

BOOST_AUTO_TEST_SUITE_END()
