#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include <memory>
#include "shape.hpp"

namespace lestsenko
{
  class CompositeShape : public Shape
  {
  public:
    using ptr = std::shared_ptr<Shape>;
    using array_ptr = std::unique_ptr<ptr[]>;
    
    CompositeShape();
    CompositeShape(const CompositeShape& obj);
    CompositeShape(CompositeShape&& obj);
    CompositeShape(const ptr &shape);
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape& rhs);
    CompositeShape& operator =(CompositeShape&& rhs);
    ptr operator [](size_t index) const;
    bool operator ==(const CompositeShape& rhs);
    bool operator !=(const CompositeShape& rhs);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& newPos) override;
    void move(double dx, double dy) override;
    point_t getPos() const override;
    void scale(double ratio) override;
    void rotate(double angle) override;

    size_t getLength() const;
    array_ptr getShapes() const;
    void addShape(const ptr& shape);
    void removeShape(size_t index);
    void clear();

  private:
    size_t length_;
    array_ptr shapes_;
  };
}

#endif // COMPOSITE_SHAPE_HPP
