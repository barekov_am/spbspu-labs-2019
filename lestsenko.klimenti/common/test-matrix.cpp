#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "separation.hpp"
#include "matrix.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(matrixTestSuite)

BOOST_AUTO_TEST_CASE(copyAndMoveConstructor)
{
  lestsenko::Rectangle r1(4.0, 4.0, {-1.0, -1.0});
  lestsenko::Circle c1(1.0, {3.0, 2.0});
  lestsenko::CompositeShape testShapes(std::make_shared<lestsenko::Rectangle>(r1));
  testShapes.addShape(std::make_shared<lestsenko::Circle>(c1));

  lestsenko::Matrix testMatrix = lestsenko::divide(testShapes);

  BOOST_CHECK_NO_THROW(lestsenko::Matrix testMatrix2(testMatrix));
  BOOST_CHECK_NO_THROW(lestsenko::Matrix testMatrix3(std::move(testMatrix)));
}

BOOST_AUTO_TEST_CASE(operatorTests)
{
  lestsenko::Rectangle r1(4.0, 4.0, {-1.0, -1.0});
  lestsenko::Circle c1(1.0, {3.0, 2.0});
  lestsenko::CompositeShape testShapes1(std::make_shared<lestsenko::Rectangle>(r1));
  testShapes1.addShape(std::make_shared<lestsenko::Circle>(c1));

  lestsenko::Matrix testMatrix1 = lestsenko::divide(testShapes1);

  BOOST_CHECK_THROW(testMatrix1[100], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix1[-1], std::out_of_range);

  BOOST_CHECK_NO_THROW(lestsenko::Matrix testMatrix2 = testMatrix1);
  BOOST_CHECK_NO_THROW(lestsenko::Matrix testMatrix3 = std::move(testMatrix1));
  
  lestsenko::Rectangle r2(4.0, 4.0, {-1.0, -1.0});
  lestsenko::CompositeShape testShapes2(std::make_shared<lestsenko::Rectangle>(r2));

  lestsenko::Matrix testMatrix4 = lestsenko::divide(testShapes2);
  lestsenko::Matrix testMatrix5 = lestsenko::divide(testShapes2);

  BOOST_CHECK(testMatrix4 == testMatrix5);

  BOOST_CHECK(testMatrix4 != testMatrix1);
  BOOST_CHECK(testMatrix5 != testMatrix1);
}

BOOST_AUTO_TEST_SUITE_END()
