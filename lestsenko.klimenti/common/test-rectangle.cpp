#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "rectangle.hpp"

const double CALC_ERR = 0.01;

BOOST_AUTO_TEST_SUITE(rectangleTests)

BOOST_AUTO_TEST_CASE(rectangleMovePoint)
{
  lestsenko::Rectangle rectangle(1, 2, {3, 4});
  const lestsenko::point_t testPoint = {15, 20};
  rectangle.move(testPoint);
  BOOST_CHECK_CLOSE(rectangle.getPos().x, testPoint.x, CALC_ERR);
  BOOST_CHECK_CLOSE(rectangle.getPos().y, testPoint.y, CALC_ERR);
}

BOOST_AUTO_TEST_CASE(rectanlgeAreaAfterMove)
{
  lestsenko::Rectangle rectangle(1, 2, {3, 4});
  const double areaBefore = rectangle.getArea();
  rectangle.move(1, 2);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), CALC_ERR);
}

BOOST_AUTO_TEST_CASE(rectangleScale)
{
  const double testHeight = 1;
  const double testWidth = 2;
  lestsenko::Rectangle rectangle(testHeight, testWidth, {3, 4});
  const double testRatio = 2;
  rectangle.scale(testRatio);
  BOOST_CHECK_CLOSE(rectangle.getArea(),testHeight * testWidth * testRatio * testRatio, CALC_ERR);
}

BOOST_AUTO_TEST_CASE(rectangleFrameRect)
{
  lestsenko::Rectangle rectangle(1, 2, {3, 4});
  const lestsenko::rectangle_t testRect = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(testRect.height, rectangle.getHeight(), CALC_ERR);
  BOOST_CHECK_CLOSE(testRect.width, rectangle.getWidth(), CALC_ERR);
  BOOST_CHECK_CLOSE(testRect.pos.x, rectangle.getPos().x, CALC_ERR);
  BOOST_CHECK_CLOSE(testRect.pos.y, rectangle.getPos().y, CALC_ERR);
}

BOOST_AUTO_TEST_CASE(rectangleConstructorInvalidArgument)
{
  BOOST_CHECK_THROW(lestsenko::Rectangle(-1, -1, {3, 4}), std::invalid_argument);
  BOOST_CHECK_THROW(lestsenko::Rectangle(1, -1, {3, 4}), std::invalid_argument);
  BOOST_CHECK_THROW(lestsenko::Rectangle(-1, 1, {3, 4}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleScaleInvalidArgument)
{
  lestsenko::Rectangle rectangle(1, 2, {3, 4});
  BOOST_CHECK_THROW(rectangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
