#include "triangle.hpp"
#include <stdexcept>
#include <algorithm>
#include <cmath>

lestsenko::Triangle::Triangle(const point_t &ptA, const point_t &ptB, const point_t &ptC) :
  ptA_(ptA),
  ptB_(ptB),
  ptC_(ptC),
  center_({(ptA.x + ptB.x + ptC.x) / 3, (ptA.y + ptB.y + ptC.y) / 3})
{
  if (getArea() <= 0.0)
  {
    throw std::invalid_argument("area can not be <= 0.0");
  }
}

double lestsenko::Triangle::getArea() const
{
  return abs(((ptB_.x - ptA_.x) * (ptC_.y - ptA_.y) - (ptC_.x - ptA_.x) * (ptB_.y - ptA_.y)) / 2);
}

lestsenko::rectangle_t lestsenko::Triangle::getFrameRect() const
{
  const double maxX = std::max({ptA_.x, ptB_.x, ptC_.x});
  const double minX = std::min({ptA_.x, ptB_.x, ptC_.x});
  const double maxY = std::max({ptA_.y, ptB_.y, ptC_.y});
  const double minY = std::min({ptA_.y, ptB_.y, ptC_.y});
  const double width = maxX - minX;
  const double height = maxY - minY;
  const point_t position = {minX + width / 2, minY + height / 2};
  return {width, height, position};
}

void lestsenko::Triangle::move(const point_t &newPos)
{
  const point_t diff = {newPos.x - center_.x, newPos.y - center_.y};
  move(diff.x, diff.y);
}

void lestsenko::Triangle::move(double dx, double dy)
{
  ptA_.x += dx;
  ptA_.y += dy;
  ptB_.x += dx;
  ptB_.y += dy;
  ptC_.x += dx;
  ptC_.y += dy;
  center_.x += dx;
  center_.y += dy;
}

lestsenko::point_t lestsenko::Triangle::getPos() const
{
  return center_;
}

void lestsenko::Triangle::scale(double ratio)
{
  if (ratio <= 0.0)
  {
    throw std::invalid_argument("ratio can not be <= 0");
  } 
  ptA_.x = center_.x + ratio * (ptA_.x - center_.x);
  ptA_.y = center_.y + ratio * (ptA_.y - center_.y);

  ptB_.x = center_.x + ratio * (ptB_.x - center_.x);
  ptB_.y = center_.y + ratio * (ptB_.y - center_.y);

  ptC_.x = center_.x + ratio * (ptC_.x - center_.x);
  ptC_.y = center_.y + ratio * (ptC_.y - center_.y);
}

void lestsenko::Triangle::rotate(double angle)
{
  point_t center = getPos();
  const double cosinus = std::abs(std::cos(angle * M_PI / 180));
  const double sinus = std::abs(std::sin(angle * M_PI / 180));
  const double Ax = (ptA_.x - center.x) * cosinus - (ptA_.y - center.y) * sinus;
  const double Ay = (ptA_.x - center.x) * sinus + (ptA_.y - center.y) * cosinus;
  const double Bx = (ptB_.x - center.x) * cosinus - (ptB_.y - center.y) * sinus;
  const double By = (ptB_.x - center.x) * sinus + (ptB_.y - center.y) * cosinus;
  const double Cx = (ptC_.x - center.x) * cosinus - (ptC_.y - center.y) * sinus;
  const double Cy = (ptC_.x - center.x) * sinus + (ptC_.y - center.y) * cosinus;
  ptA_.x = Ax + center.x;
  ptA_.y = Ay + center.y;
  ptB_.x = Bx + center.x;
  ptB_.y = By + center.y;
  ptC_.x = Cx + center.x;
  ptC_.y = Cy + center.y;
}
