#include "composite-shape.hpp"
#include <stdexcept>
#include <algorithm>
#include <cmath>

lestsenko::CompositeShape::CompositeShape() :
  length_(0),
  shapes_(nullptr)
{ }

lestsenko::CompositeShape::CompositeShape(const CompositeShape& obj) :
  length_(obj.length_),
  shapes_(std::make_unique<ptr[]>(obj.length_))
{ 
  for(size_t i = 0; i < length_; i++)
    {
      shapes_[i] = obj.shapes_[i];
    }
}

lestsenko::CompositeShape::CompositeShape(CompositeShape&& obj) :
  length_(obj.length_),
  shapes_(std::move(obj.shapes_))
{
  obj.length_ = 0;
}

lestsenko::CompositeShape::CompositeShape(const ptr &shape) :
  length_(1),
  shapes_(std::make_unique<ptr[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape does not exist");
  }
  shapes_[0] = shape;
}

lestsenko::CompositeShape& lestsenko::CompositeShape::operator =(const CompositeShape& rhs)
{
  if (this != &rhs)
  {
    length_ = rhs.length_;
    shapes_ = std::make_unique<CompositeShape::ptr[]>(rhs.length_);
    for (size_t i = 0; i < rhs.length_; ++i)
    {
      shapes_[i] = rhs.shapes_[i];
    }
  }
  return *this;
}

lestsenko::CompositeShape& lestsenko::CompositeShape::operator =(CompositeShape&& rhs)
{
  if (this != &rhs)
    {
      length_ = rhs.length_;
      shapes_ = std::move(rhs.shapes_);
      rhs.length_ = 0;
    }
    return *this;
}

lestsenko::CompositeShape::ptr lestsenko::CompositeShape::operator [](size_t index) const
{
  if (index >= length_)
    {
      throw std::out_of_range("Index out of range");
    }
    return shapes_[index];
}

bool lestsenko::CompositeShape::operator ==(const CompositeShape& rhs)
{
  if (length_ != rhs.length_)
  {
    return false;
  }

  for (size_t i = 0; i < length_; i++)
  {
    if (shapes_[i] != rhs.shapes_[i])
    {
      return false;
    }
  }
  return true;
}

bool lestsenko::CompositeShape::operator !=(const CompositeShape& rhs)
{
  return !(*this == rhs);
}

double lestsenko::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < length_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

lestsenko::rectangle_t lestsenko::CompositeShape::getFrameRect() const
{
  if (length_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  rectangle_t rectangle = shapes_[0]->getFrameRect();
  double minX = rectangle.pos.x - rectangle.width / 2;
  double maxX = rectangle.pos.x + rectangle.width / 2;
  double minY = rectangle.pos.y - rectangle.height / 2;
  double maxY = rectangle.pos.y + rectangle.height / 2;

  for (size_t i = 1; i < length_; i++)
  {
    rectangle = shapes_[i]->getFrameRect();

    minX = std::min(rectangle.pos.x - rectangle.width / 2, minX);
    minY = std::min(rectangle.pos.y - rectangle.height / 2, minY);
    maxX = std::max(rectangle.pos.x + rectangle.width / 2, maxX);
    maxY = std::max(rectangle.pos.y + rectangle.height / 2, maxY);
  }
  return {(maxX - minX), (maxY - minY), {(minX + maxX) / 2, (minY + maxY) / 2}};
}



void lestsenko::CompositeShape::move(const point_t& newPos)
{
  const point_t pos = getFrameRect().pos;
  move(newPos.x - pos.x, newPos.y - pos.y);
}

void lestsenko::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < length_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

lestsenko::point_t lestsenko::CompositeShape::getPos() const
{
  return getFrameRect().pos;
}

void lestsenko::CompositeShape::scale(double ratio)
{
  if (ratio <= 0.0)
  {
    throw std::invalid_argument("Ratio can not be <= 0");
  }
  const point_t pos = getFrameRect().pos;
  for (size_t i = 0; i < length_; i++)
  {
    const point_t shapePos = shapes_[i]->getFrameRect().pos;
    shapes_[i]->move({pos.x + ratio * (shapePos.x - pos.x), pos.y + ratio * (shapePos.y - pos.y)});
    shapes_[i]->scale(ratio);
  }
}

void lestsenko::CompositeShape::rotate(double angle)
{
  const point_t pos = getFrameRect().pos;
  const double sinus = std::sin(angle * M_PI / 180);
  const double cosinus = std::cos(angle * M_PI / 180);
  for (size_t i = 0; i < length_; i++)
  {
    const lestsenko::point_t shapeCenter = shapes_[i]->getFrameRect().pos;
    const double dx = shapeCenter.x - pos.x;
    const double dy = shapeCenter.y - pos.y;
    shapes_[i]->move(dx * (cosinus - 1) - dy * sinus, dx * sinus + dy * (cosinus - 1));
    shapes_[i]->rotate(angle);
  }
}

size_t lestsenko::CompositeShape::getLength() const
{
  return length_;
}

lestsenko::CompositeShape::array_ptr lestsenko::CompositeShape::getShapes() const
{
  array_ptr tempArray(std::make_unique<ptr[]>(length_));
  for (size_t i = 0; i < length_; i++)
  {
    tempArray[i] = shapes_[i];
  }
  return tempArray;
}

void lestsenko::CompositeShape::addShape(const ptr& newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Adding shape shouldn't be nullptr");
  }
  CompositeShape::array_ptr newShapeList(std::make_unique<CompositeShape::ptr[]>(length_ + 1));
  for (size_t i = 0; i < length_; i++)
  {
    newShapeList[i] = shapes_[i];
  }
  newShapeList[length_] = newShape;
  length_++;
  shapes_.swap(newShapeList);
}

void lestsenko::CompositeShape::removeShape(size_t index)
{
  if (index >= length_)
  {
    throw std::out_of_range("Index is out of range");
  }
  for (size_t i = index; i < length_ - 1; ++i)
  {
    shapes_[i] = shapes_[i + 1];
  }
  shapes_[length_ - 1] = nullptr;
  length_--;
}

void lestsenko::CompositeShape::clear()
{
  length_ = 0;
  shapes_.reset();
}
