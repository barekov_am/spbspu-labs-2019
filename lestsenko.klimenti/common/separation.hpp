#ifndef SEPARATION_HPP
#define SEPARATION_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace lestsenko
{
  bool overlap(const Shape &firstShape, const Shape &secondShape);
  Matrix divide(const CompositeShape&);
}

#endif // SEPARATION_HPP
