#include <random>
#include <ctime>
#include <vector>
#include "sort.hpp"
#include "strategy.hpp"


void fillRandom(double * array, size_t size)
{
  for (size_t i = 0; i < size; ++i)
  {
    array[i] = 0.1 * ((rand() % 21)) - 1;
  }
}

void task4(const char * direction, size_t size)
{
  auto dir = getDirection<double>(direction);

  if (size <= 0)
  {
    throw std::invalid_argument("Wrong size.");
  }

  std::vector<double> vector(size, 0);
  fillRandom(& vector[0], size);
  std::cout << vector << '\n';
  sort<ByAt>(vector, dir);
  std::cout << vector << '\n';
}


