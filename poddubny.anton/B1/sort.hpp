#ifndef B1_SORT_HPP
#define B1_SORT_HPP

#include <cstring>
#include <functional>
#include <iostream>

template <typename T>
std::function<bool (T, T)> getDirection(const char * order)
{
  if (std::strcmp(order, "ascending") == 0)
  {
    return [] (T lhs, T rhs) { return lhs < rhs; };
  }

  if (std::strcmp(order, "descending") == 0)
  {
    return [] (T lhs, T rhs) { return lhs > rhs; };
  }
  throw std::invalid_argument("Illegal order. Must be ""ascending"" or ""descending"".");
}

template <template <class T> class Strategy, typename T>
void sort(T & container, std::function<bool (typename T::value_type, typename T::value_type)> compare)
{
  const auto begin = Strategy<T>::begin(container);
  const auto end = Strategy<T>::end(container);
  for (auto i = begin; i != end; i++)
  {
    for (auto j = begin; j != end; j++)
    {
      typename T::value_type & a = Strategy<T>::getElement(container, i);
      typename T::value_type & b = Strategy<T>::getElement(container, j);
      if (compare(a, b))
      {
        std::swap(a, b);
      }
    }
  }
}

template <typename T, typename = typename T::iterator>
std::ostream & operator << (std::ostream & out, const T & container)
{
  for (const auto & i : container)
  {
    out << i << " ";
  }
  return out;
}

#endif //B1_SORT_HPP
