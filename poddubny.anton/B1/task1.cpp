#include <forward_list>
#include <vector>
#include "sort.hpp"
#include "strategy.hpp"

void task1(const char * direction)
{
  auto dir = getDirection<int>(direction);
  std::vector<int> vectorBracket;
  int num = 0;
  while (!(std::cin >> num).eof())
  {
    if (std::cin.fail())
    {
      throw std::invalid_argument("Reading failed.");
    }
    vectorBracket.push_back(num);
  }

  if (vectorBracket.empty())
  {
    return;
  }

  std::vector<int> vectorAt = vectorBracket;
  std::forward_list<int> list(vectorBracket.begin(), vectorBracket.end());

  sort<ByBrackets>(vectorBracket, dir);
  std::cout << vectorBracket << '\n';

  sort<ByAt>(vectorAt, dir);
  std::cout << vectorAt << '\n';

  sort<ByIterator>(list, dir);
  std::cout << list << '\n';
}
