#ifndef B1_STRATEGY_HPP
#define B1_STRATEGY_HPP

#include <iterator>

template <typename T>
struct ByBrackets
{
  static size_t begin(const T &)
  {
    return 0;
  }

  static size_t end(const T & vector)
  {
    return vector.size();
  }

  static typename T::value_type & getElement(T & vector, size_t i)
  {
    return vector[i];
  }
};

template <typename T>
struct ByAt
{
  static size_t begin(const T &)
  {
    return 0;
  }

  static size_t end(const T & vector)
  {
    return vector.size();
  }

  static typename T::reference & getElement(T & vector, size_t i)
  {
    return vector.at(i);
  }
};

template <typename T>
struct ByIterator
{
  static typename T::iterator begin(T & list)
  {
    return list.begin();
  }

  static typename T::iterator end(T & list)
  {
    return list.end();
  }

  static typename T::reference getElement(T &, typename T::iterator iterator)
  {
    return * iterator;
  }
};

#endif //B1_STRATEGY_HPP
