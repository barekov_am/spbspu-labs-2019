#include <fstream>
#include <memory>
#include <vector>
#include "sort.hpp"

void task2(const char * filename)
{
  std::ifstream file(filename);
  if (!file)
  {
    throw std::ios_base::failure("Cannot open file.");
  }
  size_t bufferSize = 1024;
  std::unique_ptr<char[], decltype(& free)> array(static_cast<char *>(malloc(bufferSize)), & free);
  if (!array)
  {
    throw std::runtime_error("Allocation of space failed.");
  }
  size_t readPos = 0;
  while (file)
  {
    file.read(& array[readPos], bufferSize - readPos);
    readPos += file.gcount();

    if (readPos == bufferSize)
    {
      bufferSize *= 2;
      std::unique_ptr<char[], decltype(& free)> tmp(static_cast<char *>(realloc(array.get(), bufferSize)), & free);
      if (!tmp)
      {
        file.close();
        throw std::runtime_error("Failed to reallocate space.");
      }
      array.release();
      std::swap(array, tmp);
    }
  }
  if (!file.eof() && !file.fail())
  {
    file.close();
    throw std::ios_base::failure("Reading from file failed.");
  }

  std::vector<char> vector(& array[0], & array[readPos]);
  for (auto symbol : vector)
  {
    std::cout << symbol;
  }
}
