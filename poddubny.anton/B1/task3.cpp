#include <vector>
#include "sort.hpp"

void task3()
{
  std::vector<int> vector;

  int num = -1;
  while (!(std::cin >> num).eof())
  {
    if (!std::cin.eof() && std::cin.fail())
    {
      throw std::ios_base::failure("Reading failed.");
    }
    if (num == 0)
    {
      break;
    }
    else
      {
      vector.push_back(num);
    }
  }
  if (vector.empty())
  {
    return;
  }

  if (num != 0)
  {
    throw std::runtime_error("Last number must be zero.");
  }

  switch (vector.back())
  {
    case 1:
      for (auto i = vector.begin(); i != vector.end();)
      {
        if (* i % 2 == 0)
        {
          i = vector.erase(i);
        }
        else
          {
          i++;
        }
      }
      break;

    case 2:
      for (auto i = vector.begin(); i != vector.end();)
      {
        if (* i % 3 == 0)
        {
          i = vector.insert(++i, 3, 1);
        }
        else
          {
          i++;
        }
      }
      break;

    default:
      break;
  }
  std::cout << vector << '\n';
}
