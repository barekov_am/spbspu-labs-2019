#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

class Triangle: public Shape
{
public:
  Triangle(const point_t & pointA, const point_t & pointB, const point_t & pointC);

  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t & newPoint) override;
  void move(double dx, double dy) override;
  void showFrameRect() const override;
  void showCenter() const override;
  void showInfo() const override;
private:
  point_t point_A;
  point_t point_B;
  point_t point_C;
  point_t center_;
};

#endif
