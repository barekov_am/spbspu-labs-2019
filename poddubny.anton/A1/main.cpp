#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

int main() 
{
  Circle circle(2.0, {2.0, 4.0});
  std::cout << "Circle:";
  Shape *figure = & circle;
  figure->showInfo();
  std::cout << "\n Moving by dx= -1 dy= 3.2";
  figure->move(-1.0, 3.2);
  figure->showCenter();
  std::cout << "\n Moving to {4,2}";
  figure->move({4.0, 2.0});
  figure->showCenter();
  figure->showFrameRect();

  Rectangle rectangle({5.0, 4.0, {2.0, 1.0}});
  std::cout << "\nRectangle:\n";
  figure = & rectangle;
  figure->showInfo();
  std::cout << "\n Moving by dx= -1 dy= 3.2 ";
  figure->move(-1.0, 3.2);
  figure->showCenter();
  std::cout << "\n Moving to {4,2} ";
  figure->move({4.0, 2.0});
  figure->showCenter();
  figure->showFrameRect();

  Triangle triangle({0.0, 0.0}, {7.0, 3.0}, {-2.0, 4.0});
  std::cout << "\n Triangle: \n";
  figure = & triangle;
  figure->showInfo();
  std::cout << "\n Moving by dx= -1 dy= 3.2 ";
  figure->move(-1.0, 3.2);
  figure->showCenter();
  std::cout << "\n Moving to {4,2} ";
  figure->move({4.0, 2.0});
  figure->showCenter();
  figure->showFrameRect();

  const point_t points[5] = {{2, 4}, {9, 4}, {4, 1}, {5.5, 7}, {7, 1}};
  Polygon polygon(5, points);

  std::cout << "\n Polygon: \n";
  figure = & polygon;
  figure->showInfo();
  std::cout << "\n Moving by dx= -1 dy= 3.2 ";
  figure->move(-1.0, 3.2);
  figure->showCenter();
  std::cout << "\n Moving to {4,2} ";
  figure->move({4.0, 2.0});
  figure->showCenter();
  figure->showFrameRect();
  return 0;
}
