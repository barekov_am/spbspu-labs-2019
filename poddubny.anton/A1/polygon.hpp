#ifndef POLYGON_HPP
#define POLYGON_HPP
#include "shape.hpp"

class Polygon : public Shape
{
public:
  Polygon(const Polygon & other);
  Polygon(Polygon && other);
  Polygon(int pointNum, const point_t * vertex);
  ~Polygon() override;

  Polygon & operator = (const Polygon & other);
  Polygon & operator = (Polygon && other);

  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t & point) override;
  void move(double dx, double dy) override;
  void showInfo() const override;
  void showFrameRect() const override;
  void showCenter() const override;
private:
  int pointNum_;
  point_t * vertex_;
  point_t getPos() const;
  bool isConvex() const;
};
#endif
