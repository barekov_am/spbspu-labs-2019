#ifndef A1_CIRCLE_HPP
#define A1_CIRCLE_HPP

#include "shape.hpp"

class Circle : public Shape {
public:
  Circle(const double & radius, const point_t & point);

  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(double dx, double dy) override;
  void move(const point_t & point) override;
  void showInfo() const override;
  void showCenter() const override;
  void showFrameRect() const override;
private:
  double radius_;
  point_t center_;
};

#endif //A1_CIRCLE_HPP
