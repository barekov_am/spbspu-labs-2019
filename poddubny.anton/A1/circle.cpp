#include "circle.hpp"
#include <iostream>
#include <cmath>
#include <cassert>

Circle::Circle(const double & radius, const point_t & point) :
  radius_(radius),
  center_(point)
{
  assert(radius > 0.0);
}

double Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

rectangle_t Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, center_};
}

void Circle::move(const point_t & point)
{
  center_ = point;
}

void Circle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Circle::showInfo() const
{
  std::cout << "\n Center";
  showCenter();
  std::cout << "\n Radius: " << radius_ <<"\n";
  std::cout << "Area: " << getArea();
}

void Circle::showCenter() const
{
  std::cout << "\n X: " << center_.x;
  std::cout << "\n Y: " << center_.y;
}

void Circle::showFrameRect() const
{
  std::cout << "\nFrame rectangle of circle";
  std::cout << "\nWidth: " << radius_ * 2;
  std::cout << "\nHeight: " << radius_ * 2;
  std::cout << "\nArea: " << getArea() << "\n";
}
