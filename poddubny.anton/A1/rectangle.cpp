#include "rectangle.hpp"
#include <iostream>
#include <cassert>

Rectangle::Rectangle(const rectangle_t & rect) :
  rect_(rect)
{
  assert(rect.width > 0.0 && rect.height > 0.0);
}

double Rectangle::getArea() const
{
  return rect_.width * rect_.height;
}

rectangle_t Rectangle::getFrameRect() const
{
  return rect_;
}

void Rectangle::move(double dx, double dy)
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

void Rectangle::move(const point_t & point)
{
  rect_.pos = point;
}

void Rectangle::showInfo() const
{
  std::cout << "Center";
  showCenter();
  std::cout << "\nWidth: " << rect_.width;
  std::cout << "\nHeight: " << rect_.height;
  std::cout << "\nArea: " << getArea();
}

void Rectangle::showCenter() const
{
  std::cout << "\n X: " << rect_.pos.x;
  std::cout << "\n Y: " << rect_.pos.y;
}

void Rectangle::showFrameRect() const
{
  rectangle_t rect = getFrameRect();
  std::cout << "\nFrame rectangle";
  std::cout << "\nWidth: " << rect.width;
  std::cout << "\nHeight: " << rect.height;
  std::cout << "\nArea: " << getArea()<< "\n";
}
