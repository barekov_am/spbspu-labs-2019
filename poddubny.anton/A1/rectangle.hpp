#ifndef A1_RECTANGLE_HPP
#define A1_RECTANGLE_HPP

#include "shape.hpp"

class Rectangle : public Shape {
public:
  Rectangle(const rectangle_t & rect);

  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(double dx, double dy) override;
  void move(const point_t & point) override;
  void showInfo() const override;
  void showCenter() const override;
  void showFrameRect() const override;
private:
  rectangle_t rect_;
};

#endif //A1_RECTANGLE_HPP
