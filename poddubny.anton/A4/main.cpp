#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

int main()
{
  poddubny::Rectangle rectangle({1.0, 2.0, {0.0, 0.0}});
  poddubny::Circle circle(2.0, {4.0, 4.0});
  poddubny::Triangle triangle({0.0, 0.0}, {-1.0, -2.0}, {-3.0, -4.0});
  poddubny::point_t points[5] = {{2.0, 4.0}, {9.0, 4.0}, {4.0, 1.0}, {5.5, 7.0}, {7.0, 1.0}};
  poddubny::Polygon polygon(5, points);

  poddubny::CompositeShape cs(std::make_shared<poddubny::Rectangle>(rectangle));

  std::cout << "CompositeShape first pos: " << cs.getPos().x << "," << cs.getPos().y
            << "\nCompositeShape area: " << cs.getArea();

  cs.addShape(std::make_shared<poddubny::Circle>(circle));
  cs.addShape(std::make_shared<poddubny::Triangle>(triangle));
  cs.addShape(std::make_shared<poddubny::Polygon>(polygon));
  std::cout << "\nCompositeShape new pos: " << cs.getPos().x << "," << cs.getPos().y
            << "\nCompositeShape new area: " << cs.getArea();

  cs.move({3.0, 4.0});
  std::cout << "\nCompositeShape second pos: " << cs.getPos().x << ", " << cs.getPos().y;

  cs.move(5.0, 6.0);
  std::cout << "\nCompositeShape third pos: " << cs.getPos().x << ", " << cs.getPos().y;

  cs.scale(2);
  std::cout << "\nCompositeShape area after scaling: " << cs.getArea();

  cs.rotate(90);
  std::cout << "\nCompositeShape area after rotating: " << cs.getArea();

  poddubny::Matrix matrix = poddubny::divide(cs);
  std::cout << "\nDividing CompositeShape:\n";

  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    for (size_t j = 0; j < matrix.getColumns(); j++)
    {
      if (matrix[i][j] != nullptr)
      {
        std::cout << "Layer: " << i << ", " << "Figure: " << j << "\n";
      }
    }
  }
  return 0;
}
