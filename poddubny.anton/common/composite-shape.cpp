#include "composite-shape.hpp"
#include <stdexcept>
#include <algorithm>
#include <cmath>
#include <iostream>

poddubny::CompositeShape::CompositeShape() :
  length_(0),
  shapes_(nullptr)
{
}

poddubny::CompositeShape::CompositeShape(const CompositeShape & other) :
  length_(other.length_),
  shapes_(std::make_unique<ptr[]>(other.length_))
{
  for (size_t i = 0; i < length_; i++)
    {
      shapes_[i] = other.shapes_[i];
    }
}

poddubny::CompositeShape::CompositeShape(CompositeShape && other) :
  length_(other.length_),
  shapes_(std::move(other.shapes_))
{
  other.length_ = 0;
}

poddubny::CompositeShape::CompositeShape(const ptr & shape) :
length_(1),
shapes_(std::make_unique<ptr[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape does not exist");
  }
  shapes_[0] = shape;
}

poddubny::CompositeShape& poddubny::CompositeShape::operator =(const CompositeShape & other)
{
  if (this != &other)
  {
    length_ = other.length_;
    shapes_ = std::make_unique<CompositeShape::ptr[]>(other.length_);
    for (size_t i = 0; i < other.length_; ++i)
    {
      shapes_[i] = other.shapes_[i];
    }
  }
  return *this;
}

poddubny::CompositeShape& poddubny::CompositeShape::operator =(CompositeShape && other)
{
  if (this != &other)
  {
    length_ = other.length_;
    shapes_ = std::move(other.shapes_);
    other.length_ = 0;
  }
  return *this;
}

poddubny::CompositeShape::ptr poddubny::CompositeShape::operator [](size_t index) const
{
  if (index >= length_)
  {
    throw std::out_of_range("Index is out of range");
  }
  return shapes_[index];
}

double poddubny::CompositeShape::getArea() const
{
  double area = 0.0;
  for (size_t i = 0; i < length_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

poddubny::rectangle_t poddubny::CompositeShape::getFrameRect() const
{
  if (length_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  rectangle_t rectangle = shapes_[0]->getFrameRect();
  double minX = rectangle.pos.x - rectangle.width / 2;
  double maxX = rectangle.pos.x + rectangle.width / 2;
  double minY = rectangle.pos.y - rectangle.height / 2;
  double maxY = rectangle.pos.y + rectangle.height / 2;

  for (size_t i = 1; i < length_; i++)
  {
    rectangle = shapes_[i]->getFrameRect();

    minX = std::min(rectangle.pos.x - rectangle.width / 2, minX);
    minY = std::min(rectangle.pos.y - rectangle.height / 2, minY);
    maxX = std::max(rectangle.pos.x + rectangle.width / 2, maxX);
    maxY = std::max(rectangle.pos.y + rectangle.height / 2, maxY);
  }
  return {(maxX - minX), (maxY - minY), {(minX + maxX) / 2, (minY + maxY) / 2}};
}

void poddubny::CompositeShape::move(const point_t & newPos)
{
  const point_t pos = getFrameRect().pos;
  move(newPos.x - pos.x, newPos.y - pos.y);
}

void poddubny::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < length_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

poddubny::point_t poddubny::CompositeShape::getPos() const
{
  return getFrameRect().pos;
}


void poddubny::CompositeShape::scale(double ratio)
{
  if (ratio <= 0.0)
  {
    throw std::invalid_argument("Ratio must be positive");
  }
  const point_t pos = getFrameRect().pos;
  for (size_t i = 0; i < length_; i++)
  {
    const point_t shapePos = shapes_[i]->getFrameRect().pos;
    shapes_[i]->move({pos.x + ratio * (shapePos.x - pos.x), pos.y + ratio * (shapePos.y - pos.y)});
    shapes_[i]->scale(ratio);
  }
}

void poddubny::CompositeShape::rotate(double angle)
{
  point_t center = getPos();
  for (size_t i = 0; i < length_; i++)
  {
    shapes_[i]->rotate(angle);
    poddubny::point_t shapeCenter = shapes_[i]->getFrameRect().pos;
    poddubny::rotationPoint(center, shapeCenter, angle);
  }
}


void poddubny::CompositeShape::addShape(const ptr & newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Adding shape shouldn't be nullptr");
  }
  CompositeShape::array_ptr newShapeList(std::make_unique<CompositeShape::ptr[]>(length_ + 1));
  for (size_t i = 0; i < length_; i++)
  {
    newShapeList[i] = shapes_[i];
  }
  newShapeList[length_] = newShape;
  length_++;
  shapes_.swap(newShapeList);
}

void poddubny::CompositeShape::removeShape(size_t index)
{
  if (index >= length_)
  {
    throw std::out_of_range("Index is out of range");
  }
  for (size_t i = index; i < length_ - 1; ++i)
  {
    shapes_[i] = shapes_[i + 1];
  }
  shapes_[length_ - 1] = nullptr;
  length_--;
}

void poddubny::CompositeShape::clear()
{
  length_ = 0;
  shapes_.reset();
}

size_t poddubny::CompositeShape::getSize() const
{
  return length_;
}

void poddubny::CompositeShape::showInfo() const
{
  std::cout << "\nCompositeShape size: " << getSize();
  std::cout << "\n Center";
  showCenter();
  showFrameRect();
}

void poddubny::CompositeShape::showCenter() const
{
  const point_t oldPos = getPos();
  std::cout << "\n X: " << oldPos.x;
  std::cout << "\n Y: " << oldPos.y;
}

void poddubny::CompositeShape::showFrameRect() const
{
  const rectangle_t rect = getFrameRect();
  std::cout << "\nFrame rectangle of CompositeShape\n";
  std::cout << "\nWidth: " << rect.width;
  std::cout << "\nHeight: " << rect.height;
  std::cout << "\nArea: " << getArea();
}
