#ifndef A4_COMPOSITE_SHAPE_HPP
#define A4_COMPOSITE_SHAPE_HPP
#include <memory>
#include "shape.hpp"

namespace poddubny {
  class CompositeShape : public Shape
  {
  public:
    using ptr = std::shared_ptr<Shape>;
    using array_ptr = std::unique_ptr<ptr[]>;

    CompositeShape();
    CompositeShape(const CompositeShape & other);
    CompositeShape(CompositeShape && other);
    CompositeShape(const ptr & shape);
    ~CompositeShape() = default;

    CompositeShape & operator =(const CompositeShape &otherShape);
    CompositeShape & operator =(CompositeShape &&otherShape);
    ptr operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & point) override;
    void move(double dx, double dy) override;
    point_t getPos() const override;
    void scale(double scale) override;
    void rotate (double angle) override;
    void showInfo() const override;
    void showFrameRect() const override;
    void showCenter() const override;
    void addShape(const ptr & shape);
    void removeShape(size_t index);
    size_t getSize() const;
    void clear();

  private:
    size_t length_;
    array_ptr shapes_;
  };
}

#endif //A4_COMPOSITE_SHAPE_HPP
