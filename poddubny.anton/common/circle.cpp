#include "circle.hpp"
#include <iostream>
#include <cmath>

poddubny::Circle::Circle(double radius, point_t point) :
  radius_(radius),
  center_(point)
{
  if (radius <= 0.0)
  {
    throw std::invalid_argument("Radius can not be less than 0");
  }
}

double poddubny::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

poddubny::rectangle_t poddubny::Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, center_};
}

poddubny::point_t poddubny::Circle::getPos() const
{
  return center_;
}

void poddubny::Circle::move(const point_t & point)
{
  center_ = point;
}

void poddubny::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void poddubny::Circle::scale(double scale)
{
  if (scale <= 0.0)
  {
    throw std::invalid_argument("Scale must be positive");
  }
  radius_ *= scale;
}

void poddubny::Circle::rotate(double )
{
}

void poddubny::Circle::showInfo() const
{
  std::cout << "\n Center";
  showCenter();
  std::cout << "\n Radius: " << radius_ <<"\n";
  std::cout << "Area: " << getArea();
}

void poddubny::Circle::showCenter() const
{
  std::cout << "\n X: " << center_.x;
  std::cout << "\n Y: " << center_.y;
}

void poddubny::Circle::showFrameRect() const
{
  std::cout << "\nFrame rectangle of circle";
  std::cout << "\nWidth: " << radius_ * 2;
  std::cout << "\nHeight: " << radius_ * 2;
  std::cout << "\nArea: " << getArea() << "\n";
}
