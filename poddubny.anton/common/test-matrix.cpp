#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "partition.hpp"
#include "matrix.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(matrixTestSuite)

BOOST_AUTO_TEST_CASE(copyAndMoveConstructor)
{
  poddubny::Rectangle r1({4.0, 4.0, {-1.0, -1.0}});
  poddubny::Circle c1(1.0, {3.0, 2.0});
  poddubny::CompositeShape testShapes(std::make_shared<poddubny::Rectangle>(r1));
  testShapes.addShape(std::make_shared<poddubny::Circle>(c1));

  poddubny::Matrix testMatrix = poddubny::divide(testShapes);

  BOOST_CHECK_NO_THROW(poddubny::Matrix testMatrix2(testMatrix));
  BOOST_CHECK_NO_THROW(poddubny::Matrix testMatrix3(std::move(testMatrix)));
}

BOOST_AUTO_TEST_CASE(operatorTests)
{
  poddubny::Rectangle r1({4.0, 4.0, {-1.0, -1.0}});
  poddubny::Circle c1(1.0, {3.0, 2.0});
  poddubny::CompositeShape testShapes1(std::make_shared<poddubny::Rectangle>(r1));
  testShapes1.addShape(std::make_shared<poddubny::Circle>(c1));

  poddubny::Matrix testMatrix1 = poddubny::divide(testShapes1);

  BOOST_CHECK_THROW(testMatrix1[100], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix1[-1], std::out_of_range);

  BOOST_CHECK_NO_THROW(poddubny::Matrix testMatrix2 = testMatrix1);
  BOOST_CHECK_NO_THROW(poddubny::Matrix testMatrix3 = std::move(testMatrix1));

  poddubny::Rectangle r2({4.0, 4.0, {-1.0, -1.0}});
  poddubny::CompositeShape testShapes2(std::make_shared<poddubny::Rectangle>(r2));

  poddubny::Matrix testMatrix4 = poddubny::divide(testShapes2);
  poddubny::Matrix testMatrix5 = poddubny::divide(testShapes2);

  BOOST_CHECK(testMatrix4 == testMatrix5);

  BOOST_CHECK(testMatrix4 != testMatrix1);
  BOOST_CHECK(testMatrix5 != testMatrix1);
}

BOOST_AUTO_TEST_SUITE_END()
