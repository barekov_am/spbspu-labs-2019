#include <boost/test/auto_unit_test.hpp>
#include "partition.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(separationTestSuite)

BOOST_AUTO_TEST_CASE(overlapingOfShapes)
{
  poddubny::Rectangle testRectangle1({4.0, 4.0, {0.0, 0.0}});
  poddubny::Circle testCircle1(1.0, {-5.0, 1.0});
  poddubny::Rectangle testRectangle2({4.0, 4.0, {-2.0, 1.0}});
  poddubny::Circle testCircle2(1.0, {-2.0, 0.0});

  BOOST_CHECK(poddubny::overlap(testRectangle1, testCircle2));
  BOOST_CHECK(poddubny::overlap(testRectangle1, testRectangle2));
  BOOST_CHECK(poddubny::overlap(testRectangle2, testCircle1));
  BOOST_CHECK(poddubny::overlap(testRectangle2, testCircle2));

  BOOST_CHECK(!poddubny::overlap(testRectangle1, testCircle1));
  BOOST_CHECK(!poddubny::overlap(testCircle1, testCircle2));
}

BOOST_AUTO_TEST_CASE(emptyCompositedivideing)
{
  poddubny::Matrix testMatrix = poddubny::divide(poddubny::CompositeShape());

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 0);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 0);
  }

BOOST_AUTO_TEST_CASE(correctCompositeSeparation)
{
  poddubny::Rectangle r1({4.0, 4.0, {-1.0, -1.0}});
  poddubny::Circle c1(1.0, {3.0, 2.0});
  poddubny::Rectangle r2({4.0, 4.0, {1.0, 2.0}});
  poddubny::Rectangle r3({4.0, 4.0, {1.0, -4.0}});
  poddubny::CompositeShape testShapes(std::make_shared<poddubny::Rectangle>(r1));
  testShapes.addShape(std::make_shared<poddubny::Circle>(c1));
  testShapes.addShape(std::make_shared<poddubny::Rectangle>(r2));
  testShapes.addShape(std::make_shared<poddubny::Rectangle>(r3));

  poddubny::Matrix testMatrix = poddubny::divide(testShapes);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 2);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 2);
  BOOST_CHECK_EQUAL(testMatrix[0][0], testShapes[0]);
  BOOST_CHECK_EQUAL(testMatrix[0][1], testShapes[1]);
  BOOST_CHECK_EQUAL(testMatrix[1][0], testShapes[2]);
  BOOST_CHECK_EQUAL(testMatrix[1][1], testShapes[3]);
}

BOOST_AUTO_TEST_SUITE_END()
