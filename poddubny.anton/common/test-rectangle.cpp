#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(Test_Rectangle)

BOOST_AUTO_TEST_CASE(constRectangleAfterMovingByIncrements)
{
  poddubny::Rectangle testRectangle({1.0, 1.0, {1.0, 1.0}});
  const poddubny::rectangle_t initialRect = testRectangle.getFrameRect();
  const double initialArea = testRectangle.getArea();

  testRectangle.move(5.0, 5.0);

  BOOST_CHECK_CLOSE(initialRect.width, testRectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialRect.height, testRectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(constRectangleAfterMovingToPoint)
{
  poddubny::Rectangle testRectangle({1.0, 1.0, {1.0, 1.0}});
  const poddubny::rectangle_t initialRect = testRectangle.getFrameRect();
  const double initialArea = testRectangle.getArea();

  testRectangle.move({5.0, 5.0});

  BOOST_CHECK_CLOSE(initialRect.width, testRectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialRect.height, testRectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(quadraticChangeRectangleAreaAfterScaling)
{
  poddubny::Rectangle testRectangle({1.0, 1.0, {1.0, 1.0}});
  const double initialArea = testRectangle.getArea();
  const double zoomFactor = 2.0;

  testRectangle.scale(zoomFactor);

  BOOST_CHECK_CLOSE(testRectangle.getArea(), initialArea * zoomFactor * zoomFactor, ACCURACY);
}

BOOST_AUTO_TEST_CASE(constRectangleAfterRotating)
{
  poddubny::Rectangle testRectangle({1.0, 1.0, {1.0, 1.0}});
  const double areaBefore = testRectangle.getArea();
  const poddubny::rectangle_t rectBefore = testRectangle.getFrameRect();

  const double angle = 90;
  testRectangle.rotate(angle);

  double areaAfter = testRectangle.getArea();
  poddubny::rectangle_t rectAfter = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(rectBefore.pos.x, rectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(rectBefore.pos.y, rectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidRectangleParameteres)
{
  BOOST_CHECK_THROW(poddubny::Rectangle({1.0, - 1.0, {1.0, 1.0}}), std::invalid_argument);
  BOOST_CHECK_THROW(poddubny::Rectangle({- 1.0, 1.0, {1.0, 1.0}}), std::invalid_argument);

  poddubny::Rectangle testRectangle({1.0, 1.0, {1.0, 1.0}});
  BOOST_CHECK_THROW(testRectangle.scale(- 2.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
