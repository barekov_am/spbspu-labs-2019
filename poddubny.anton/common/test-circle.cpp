#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(Test_Circle)

BOOST_AUTO_TEST_CASE(constCircleAfterMovingByIncrements)
{
  poddubny::Circle testCircle(1.0, {1.0, 1.0});
  const poddubny::rectangle_t initialRect = testCircle.getFrameRect();
  const double initialArea = testCircle.getArea();

  testCircle.move(5.0, 5.0);

  BOOST_CHECK_CLOSE(initialRect.width, testCircle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialRect.height, testCircle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testCircle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(constCircleAfterMovingToPoint)
{
  poddubny::Circle testCircle(1.0, {1.0, 1.0});
  const poddubny::rectangle_t initialCircle = testCircle.getFrameRect();
  const double initialArea = testCircle.getArea();

  testCircle.move({5.0, 5.0});

  BOOST_CHECK_CLOSE(initialCircle.width, testCircle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialCircle.height, testCircle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testCircle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(quadraticChangeCircleAreaAfterScaling)
{
  poddubny::Circle testCircle(1.0, {1.0, 1.0});
  const double initialArea = testCircle.getArea();
  const double zoomFactor = 2.0;

  testCircle.scale(zoomFactor);

  BOOST_CHECK_CLOSE(testCircle.getArea(), initialArea * zoomFactor * zoomFactor, ACCURACY);
}

BOOST_AUTO_TEST_CASE(constCircleAfterRotating)
{
  poddubny::Circle testCircle(1.0, {1.0, 1.0});
  const double areaBefore = testCircle.getArea();
  const poddubny::rectangle_t rectBefore = testCircle.getFrameRect();

  const double angle = 90;
  testCircle.rotate(angle);

  double areaAfter = testCircle.getArea();
  poddubny::rectangle_t rectAfter = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(rectBefore.pos.x, rectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(rectBefore.pos.y, rectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidCircleParameteres)
{
  BOOST_CHECK_THROW(poddubny::Circle(- 1.0, {1.0, 1.0}), std::invalid_argument);

  poddubny::Circle testCircle(1.0, {1.0, 1.0});
  BOOST_CHECK_THROW(testCircle.scale(-2.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
