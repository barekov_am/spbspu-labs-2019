#include "base-types.hpp"
#include <cmath>

poddubny::point_t poddubny::rotationPoint(poddubny::point_t &center, poddubny::point_t point, double angle)
{
  angle = angle * M_PI / 180;
  double x = (point.x - center.x) * cos(angle) - (point.y - center.y) * sin(angle) + center.x;
  double y = (point.y - center.y) * cos(angle) - (point.x - center.x) * sin(angle) + center.y;
  return { x, y };
}
