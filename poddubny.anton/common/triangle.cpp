#include "triangle.hpp"
#include <iostream>
#include <cmath>

poddubny::Triangle::Triangle(point_t pointA, point_t pointB, point_t pointC) :
  point_A(pointA),
  point_B(pointB),
  point_C(pointC),
  center_({(pointA.x + pointB.x + pointC.x) / 3, (pointA.y + pointB.y + pointC.y) / 3})
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("Area must be more than 0");
  }
}

double poddubny::Triangle::getArea() const
{
  return fabs((point_B.x - point_A.x) * (point_C.y - point_A.y) - (point_C.x - point_A.x) * (point_B.y - point_A.y)) / 2;
}

poddubny::rectangle_t poddubny::Triangle::getFrameRect() const
{
  const double maxX = std::fmax(std::fmax(point_A.x, point_B.x), point_C.x);
  const double minX = std::fmin(std::fmin(point_A.x, point_B.x), point_C.x);
  const double maxY = std::fmax(std::fmax(point_A.y, point_B.y), point_C.y);
  const double minY = std::fmin(std::fmin(point_A.y, point_B.y), point_C.y);
  return {maxX - minX, maxY - minY, {(maxX + minX) / 2, (maxY + minY) / 2}};
}

void poddubny::Triangle::move(const point_t & newPoint)
{
  const double dx = newPoint.x - center_.x;
  const double dy = newPoint.y - center_.y;

  move(dx, dy);
}

void poddubny::Triangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;

  point_A.x += dx;
  point_A.y += dy;

  point_B.x += dx;
  point_B.y += dy;

  point_C.x += dx;
  point_C.y += dy;
}

void poddubny::Triangle::scale(double scale)
{
  if (scale <= 0.0)
  {
    throw std::invalid_argument("Scale must be positive");
  }
  point_A.x = center_.x + scale * (point_A.x - center_.x);
  point_A.y = center_.x + scale * (point_A.y - center_.y);

  point_B.x = center_.x + scale * (point_B.x - center_.x);
  point_B.y = center_.x + scale * (point_B.y - center_.y);

  point_C.x = center_.x + scale * (point_C.x - center_.x);
  point_C.y = center_.x + scale * (point_C.y - center_.y);
}

poddubny::point_t poddubny::Triangle::getPos() const
{
  return center_;
}

void poddubny::Triangle::rotate(double angle)
{
  point_A = poddubny::rotationPoint(center_, point_A, angle);
  point_B = poddubny::rotationPoint(center_, point_B, angle);
  point_C = poddubny::rotationPoint(center_, point_C, angle);
}

void poddubny::Triangle::showInfo() const
{
  std::cout << "Points' coordinates :";
  showCenter();
  std::cout << "\n Point A: " << point_A.x << " " << point_A.y << "\n";
  std::cout << "Point B: " << point_B.x << " " << point_B.y << "\n";
  std::cout << "Point C: " << point_C.x << " " << point_C.y << "\n";
}

void poddubny::Triangle::showCenter() const
{
  std::cout << "\n X: " << center_.x;
  std::cout << "\n Y: " << center_.y;
}

void poddubny::Triangle::showFrameRect() const
{
  const rectangle_t rect = getFrameRect();
  std::cout << "\nFrame rectangle of triangle\n";
  std::cout << "\nWidth: " << rect.width;
  std::cout << "\nHeight: " << rect.height;
  std::cout << "\nArea: " << getArea();
}
