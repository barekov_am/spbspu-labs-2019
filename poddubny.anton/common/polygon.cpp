#include "polygon.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <algorithm>

poddubny::Polygon::Polygon(const Polygon & other) :
  pointNum_(other.pointNum_),
  vertex_(new point_t[other.pointNum_])
{
  for (int i = 0; i < pointNum_; i++)
  {
    vertex_[i] = other.vertex_[i];
  }
}

poddubny::Polygon::Polygon(Polygon && other) :
  pointNum_(other.pointNum_),
  vertex_(other.vertex_)
{
  other.vertex_ = nullptr;
  other.pointNum_ = 0;
}

poddubny::Polygon::Polygon(int pointNum, const point_t * vertex) :
  pointNum_(pointNum),
  vertex_(new point_t[pointNum])
{
  if (pointNum <= 2)
  {
    delete[] vertex_;
    throw std::invalid_argument("Number of points in polygon must be more than 2");
  }

  if (vertex == nullptr)
  {
    delete[] vertex_;
    throw std::invalid_argument("Pointer to vertex must not be null.");
  }

  for (int i = 0; i < pointNum_; i++)
  {
    vertex_[i] = vertex[i];
  }

  if (!isConvex())
  {
    delete[] vertex_;
    throw std::invalid_argument("Polygon must be convex");
  }

  if (getArea() == 0)
  {
    delete[] vertex_;
    throw std::invalid_argument("Polygon's area must be more than 0");
  }
}

poddubny::Polygon::~Polygon()
{
  delete[] vertex_;
}

poddubny::Polygon & poddubny::Polygon::operator = (const Polygon & other)
{
  if (this == &other)
  {
    return *this;
  }
  pointNum_ = other.pointNum_;
  delete[] vertex_;
  vertex_ = new point_t[pointNum_];
  for (int i = 0; i < pointNum_; i++)
  {
    vertex_[i] = other.vertex_[i];
  }
  return *this;
}

poddubny::Polygon & poddubny::Polygon::operator = (Polygon && other)
{
  if (this == &other)
  {
    return *this;
  }
  pointNum_ = other.pointNum_;
  other.pointNum_ = 0;
  delete[] vertex_;
  vertex_ = other.vertex_;
  other.vertex_ = nullptr;
  return *this;
}

double poddubny::Polygon::getArea() const
{
  double area = 0.0;
  for (int i = 0; i < pointNum_ - 1; i++)
  {
    area += vertex_[i].x * vertex_[i + 1].y;
    area -= vertex_[i + 1].x * vertex_[i].y;
  }
  area += vertex_[pointNum_ - 1].x * vertex_[0].y;
  area -= vertex_[0].x * vertex_[pointNum_ - 1].y;
  return std::fabs(area) / 2;
}

poddubny::point_t poddubny::Polygon::getPos() const
{
  point_t pos = {0.0, 0.0};
  for (int i = 0; i < pointNum_; i++)
  {
    pos.x += vertex_[i].x;
    pos.y += vertex_[i].y;
  }
  return {pos.x / pointNum_, pos.y / pointNum_};
}

bool poddubny::Polygon::isConvex() const
{
  point_t prevPoint = {0.0, 0.0};
  point_t nextPoint = {0.0, 0.0};
  for (int i = 0; i <= pointNum_ - 2; i++)
  {
    if (i != pointNum_ - 2)
    {
      prevPoint.x = vertex_[i + 1].x - vertex_[i].x;
      prevPoint.y = vertex_[i + 1].y - vertex_[i].y;
      nextPoint.x = vertex_[i + 2].x - vertex_[i + 1].x;
      nextPoint.y = vertex_[i + 2].y - vertex_[i + 1].y;
    }
    else
    {
      prevPoint.x = vertex_[i + 1].x - vertex_[i].x;
      prevPoint.y = vertex_[i + 1].y - vertex_[i].y;
      nextPoint.x = vertex_[0].x - vertex_[i + 1].x;
      nextPoint.y = vertex_[0].y - vertex_[i + 1].y;
    }

    if (((prevPoint.x * nextPoint.y) - (nextPoint.x * prevPoint.y)) >= 0)
    {
      return false;
    }
  }
  return true;
}

poddubny::rectangle_t poddubny::Polygon::getFrameRect() const
{
  double maxX = vertex_[0].x;
  double minX = vertex_[0].x;
  double maxY = vertex_[0].y;
  double minY = vertex_[0].y;
  for (int i = 1; i < pointNum_; i++)
  {
    maxX = std::max(maxX, vertex_[i].x);
    minX = std::min(minX, vertex_[i].x);
    maxY = std::max(maxY, vertex_[i].y);
    minY = std::min(minY, vertex_[i].y);
  }
  const double height = (maxY - minY);
  const double width = (maxX - minX);
  const point_t pos = {minX + (width / 2), minY + (height / 2)};
  return {width, height, pos};
}

void poddubny::Polygon::move(const point_t & newPos)
{
  const point_t oldPos = getPos();
  const double dx = newPos.x - oldPos.x;
  const double dy = newPos.y - oldPos.y;
  move(dx, dy);
}

void poddubny::Polygon::move(double dx, double dy)
{
  for (int i = 0; i < pointNum_; i++)
  {
    vertex_[i].x += dx;
    vertex_[i].y += dy;
  }
}

void poddubny::Polygon::scale(double scale)
{
  if (scale <= 0.0)
  {
    throw std::invalid_argument("Scale must be positive");
  }
  const point_t center = getPos();
  for (int i = 0; i < pointNum_; i++)
  {
    vertex_[i].x = center.x + scale * (vertex_[i].x - center.x);
    vertex_[i].y = center.y + scale * (vertex_[i].y - center.y);
  }
}

void poddubny::Polygon::rotate(double angle)
{
  point_t center = getFrameRect().pos;
  for(int i = 0; i < pointNum_; i++)
  {
    vertex_[i] = poddubny::rotationPoint(center, vertex_[i], angle);
  }
}

void poddubny::Polygon::showInfo() const
{
  rectangle_t framingRectangle = getFrameRect();
  for (int i = 0; i < pointNum_; i++)
  {
    std::cout << "\n Vertex index is:" << i + 1;
    std::cout << "\n Vertex point has coordinates (" << vertex_[i].x << ", " << vertex_[i].y << ")";
  }
  std::cout << "\n Polygon frame width is " << framingRectangle.width;
  std::cout << "\n Polygon frame height is " << framingRectangle.height;
  std::cout << "\n Polygon area is " << getArea() << "\n";
}

void poddubny::Polygon::showCenter() const
{
  const point_t oldPos = getPos();
  std::cout << "\n X: " << oldPos.x;
  std::cout << "\n Y: " << oldPos.y;
}

void poddubny::Polygon::showFrameRect() const
{
  const rectangle_t rect = getFrameRect();
  std::cout << "\nFrame rectangle of polygon\n";
  std::cout << "\nWidth: " << rect.width;
  std::cout << "\nHeight: " << rect.height;
  std::cout << "\nArea: " << getArea();
}
