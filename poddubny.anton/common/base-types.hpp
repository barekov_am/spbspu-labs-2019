#ifndef A4_BASE_TYPES_HPP
#define A4_BASE_TYPES_HPP

namespace poddubny
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    double width;
    double height;
    point_t pos;
  };

  point_t rotationPoint(poddubny::point_t & center, poddubny::point_t point, double angle);
}
#endif //A4_BASE_TYPES_HPP
