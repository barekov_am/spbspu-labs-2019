#ifndef A4_PARTITION_HPP
#define A4_PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace poddubny
{
  bool overlap(const Shape & firstShape, const Shape & secondShape);
  Matrix divide(const CompositeShape &);
}

#endif //A4_PARTITION_HPP
