#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(Test_Triangle)

BOOST_AUTO_TEST_CASE(constTriangleAfterMovingByIncrements)
{
  poddubny::Triangle testTriangle({0.0, 0.0}, {7.0, 3.0}, {-2.0, 4.0});
  const poddubny::rectangle_t initialTriangle = testTriangle.getFrameRect();
  const double initialArea = testTriangle.getArea();

  testTriangle.move(5.0, 5.0);

  BOOST_CHECK_CLOSE(initialTriangle.width, testTriangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialTriangle.height, testTriangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testTriangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(constTriangleAfterMovingToPoint)
{
  poddubny::Triangle testTriangle({0.0, 0.0}, {7.0, 3.0}, {-2.0, 4.0});
  const poddubny::rectangle_t initialTriangle = testTriangle.getFrameRect();
  const double initialArea = testTriangle.getArea();

  testTriangle.move({5.0, 5.0});

  BOOST_CHECK_CLOSE(initialTriangle.width, testTriangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialTriangle.height, testTriangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testTriangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(quadraticChangeTriangleAreaAfterScaling)
{
  poddubny::Triangle testTriangle({0.0, 0.0}, {7.0, 3.0}, {-2.0, 4.0});
  const double initialArea = testTriangle.getArea();
  const double zoomFactor = 2.0;

  testTriangle.scale(zoomFactor);

  BOOST_CHECK_CLOSE(testTriangle.getArea(), initialArea * zoomFactor * zoomFactor, ACCURACY);
}

BOOST_AUTO_TEST_CASE(constTriangleAfterRotating)
{
  poddubny::Triangle testTriangle({0.0, 0.0}, {7.0, 3.0}, {-2.0, 4.0});
  const double areaBefore = testTriangle.getArea();

  const double angle = 90;
  testTriangle.rotate(angle);

  double areaAfter = testTriangle.getArea();
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidTriangleParameteres)
{
  BOOST_CHECK_THROW(poddubny::Triangle({2.0, 2.0}, {2.0, 2.0}, {3.0, 3.0}), std::invalid_argument);

  poddubny::Triangle testTriangle({0.0, 0.0}, {7.0, 3.0}, {-2.0, 4.0});
  BOOST_CHECK_THROW(testTriangle.scale(-2.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
