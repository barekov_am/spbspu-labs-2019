#include "rectangle.hpp"
#include <iostream>
#include <cmath>

poddubny::Rectangle::Rectangle(const rectangle_t rect) :
  rect_(rect),
  angle_(0)
{
  if (rect.width <= 0.0 || rect.height <= 0.0)
  {
    throw std::invalid_argument("Dimensions must be positive");
  }
}

double poddubny::Rectangle::getArea() const
{
  return rect_.width * rect_.height;
}

poddubny::rectangle_t poddubny::Rectangle::getFrameRect() const
{
  const double sinus = std::sin(angle_ * M_PI / 180);
  const double cosinus = std::cos(angle_ * M_PI / 180);
  const double width =  fabs(rect_.height * sinus) + fabs(rect_.width * cosinus);
  const double height = fabs(rect_.height * cosinus) + fabs( rect_.width * sinus);

  return {height, width, rect_.pos};

}

void poddubny::Rectangle::move(double dx, double dy)
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

void poddubny::Rectangle::move(const point_t & point)
{
  rect_.pos = point;
}

void poddubny::Rectangle::scale(double scale)
{
  if (scale <= 0.0)
  {
    throw std::invalid_argument("Scale must be positive");
  }
  rect_.width *= scale;
  rect_.height *= scale;
}

void poddubny::Rectangle::rotate(double angle)
{
  angle_ += angle;
}

poddubny::point_t poddubny::Rectangle::getPos() const
{
  return rect_.pos;
}

void poddubny::Rectangle::showInfo() const
{
  std::cout << "Center";
  showCenter();
  std::cout << "\nWidth: " << rect_.width;
  std::cout << "\nHeight: " << rect_.height;
  std::cout << "\nArea: " << getArea();
}

void poddubny::Rectangle::showCenter() const
{
  std::cout << "\n X: " << rect_.pos.x;
  std::cout << "\n Y: " << rect_.pos.y;
}

void poddubny::Rectangle::showFrameRect() const
{
  rectangle_t rect = getFrameRect();
  std::cout << "\nFrame rectangle";
  std::cout << "\nWidth: " << rect.width;
  std::cout << "\nHeight: " << rect.height;
  std::cout << "\nArea: " << getArea()<< "\n";
}
