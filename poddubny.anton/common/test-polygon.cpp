#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "polygon.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(Test_Polygon)

BOOST_AUTO_TEST_CASE(constPolygonAfterMovingByIncrements)
{
  const poddubny::point_t points[] = {{2.0, 4.0}, {9.0, 4.0}, {4.0, 1.0}, {5.5, 7.0}, {7.0, 1.0}};
  poddubny::Polygon testPolygon(5, points);
  const poddubny::rectangle_t initialTriangle = testPolygon.getFrameRect();
  const double initialArea = testPolygon.getArea();

  testPolygon.move(5.0, 5.0);

  BOOST_CHECK_CLOSE(initialTriangle.width, testPolygon.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialTriangle.height, testPolygon.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testPolygon.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(constPolygonAfterMovingToPoint)
{
  const poddubny::point_t points[] = {{2.0, 4.0}, {9.0, 4.0}, {4.0, 1.0}, {5.5, 7.0}, {7.0, 1.0}};
  poddubny::Polygon testPolygon(5, points);
  const poddubny::rectangle_t initialTriangle = testPolygon.getFrameRect();
  const double initialArea = testPolygon.getArea();

  testPolygon.move({5.0, 5.0});

  BOOST_CHECK_CLOSE(initialTriangle.width, testPolygon.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(initialTriangle.height, testPolygon.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(initialArea, testPolygon.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(quadraticChangePolygonAreaAfterScaling)
{
  const poddubny::point_t points[] = {{2.0, 4.0}, {9.0, 4.0}, {4.0, 1.0}, {5.5, 7.0}, {7.0, 1.0}};
  poddubny::Polygon testPolygon(5, points);
  const double initialArea = testPolygon.getArea();
  const double zoomFactor = 2.0;

  testPolygon.scale(zoomFactor);

  BOOST_CHECK_CLOSE(testPolygon.getArea(), initialArea * zoomFactor * zoomFactor, ACCURACY);
}

BOOST_AUTO_TEST_CASE(constPolygonAfterRotating)
{
  const poddubny::point_t points[] = {{2.0, 4.0}, {9.0, 4.0}, {4.0, 1.0}, {5.5, 7.0}, {7.0, 1.0}};
  poddubny::Polygon testPolygon(5, points);
  const double areaBefore = testPolygon.getArea();
  const poddubny::rectangle_t rectBefore = testPolygon.getFrameRect();

  const double angle = 90;
  testPolygon.rotate(angle);

  double areaAfter = testPolygon.getArea();
  poddubny::rectangle_t rectAfter = testPolygon.getFrameRect();
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(rectBefore.pos.x, rectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(rectBefore.pos.y, rectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidPolygonParameteres)
{
  const poddubny::point_t points1[] = {{2.0, 4.0}, {9.0, 4.0}};
  BOOST_CHECK_THROW(poddubny::Polygon(2, points1), std::invalid_argument);

  const poddubny::point_t points2[] = {{2.0, 4.0}, {9.0, 4.0}, {4.0, 1.0}, {5.5, 7.0}, {7.0, 1.0}};
  poddubny::Polygon testPolygon(5, points2);
  BOOST_CHECK_THROW(testPolygon.scale(-2.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestPolygonCopyConstructor)
{
  const poddubny::point_t points[]{{-3.0, 0.0}, {-1.0, 3.0}, {1.0, 3.0}, {3.0, 0.0}, {0.0, -3.0}};
  poddubny::Polygon polygon1(5, points);
  poddubny::Polygon polygon2 = polygon1;

  const poddubny::rectangle_t rectangle1 = polygon1.getFrameRect();
  const poddubny::rectangle_t rectangle2 = polygon2.getFrameRect();

  BOOST_CHECK_CLOSE(rectangle1.width, rectangle2.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle1.height, rectangle2.height, ACCURACY);
  BOOST_CHECK_CLOSE(polygon1.getArea(), polygon2.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(TestPolygonCopyAssignmentOperator)
{
  const poddubny::point_t points1[] {{-3.0, 0.0}, {-1.0, 3.0}, {1.0, 3.0}, {3.0, 0.0}, {0.0, -3.0}};
  const poddubny::point_t points2[] {{-3.0, 0.0}, {-1.0, 3.0}, {1.0, 3.0}};
  poddubny::Polygon polygon1(5, points1);
  poddubny::Polygon polygon2(3, points2);
  polygon2 = polygon1;

  const poddubny::rectangle_t rectangle1 = polygon1.getFrameRect();
  const poddubny::rectangle_t rectangle2 = polygon2.getFrameRect();

  BOOST_CHECK_CLOSE(rectangle1.width, rectangle2.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle1.height, rectangle2.height, ACCURACY);
  BOOST_CHECK_CLOSE(polygon1.getArea(), polygon2.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(TestPolygonMoveConstructor)
{
  const poddubny::point_t points[] {{-3.0, 0.0}, {-1.0, 3.0}, {1.0, 3.0}, {3.0, 0.0}, {0.0, -3.0}};
  poddubny::Polygon polygon1(5, points);
  const double areaBeforeMove = polygon1.getArea();
  const poddubny::rectangle_t rectangle1 = polygon1.getFrameRect();
  poddubny::Polygon polygon2 = std::move(polygon1);

  const poddubny::rectangle_t rectangle2 = polygon2.getFrameRect();
  const double areaAfterMove = polygon2.getArea();

  BOOST_CHECK_CLOSE(rectangle1.width, rectangle2.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle1.height, rectangle2.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ACCURACY);
}

BOOST_AUTO_TEST_CASE(TestPolygonMoveAssignmentOperator)
{
  const poddubny::point_t points1[] {{-3.0, 0.0}, {-1.0, 3.0}, {1.0, 3.0}, {3.0, 0.0}, {0.0, -3.0}};
  const poddubny::point_t points2[] {{-3.0, 0.0}, {-1.0, 3.0}, {1.0, 3.0}};

  poddubny::Polygon polygon1(5, points1);
  poddubny::Polygon polygon2(3, points2);
  const double areaBeforeMove = polygon1.getArea();
  const poddubny::rectangle_t rectangle1 = polygon1.getFrameRect();

  polygon2 = std::move(polygon1);
  const poddubny::rectangle_t rectangle2 = polygon2.getFrameRect();
  const double areaAfterMove = polygon2.getArea();

  BOOST_CHECK_CLOSE(rectangle1.width, rectangle2.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle1.height, rectangle2.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
