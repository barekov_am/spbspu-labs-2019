#include <stdexcept>
#include <memory>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(compositeShapeTests)

BOOST_AUTO_TEST_CASE(compositeShapeMove)
{
  poddubny::Rectangle obj1({1.0, 2.0, {0.0, 0.0}});
  poddubny::Circle obj2(2.0, {4.0, 4.0});
  poddubny::Triangle obj3({0.0, 0.0}, {2.0, 0.0}, {2.0, 3.0});
  poddubny::point_t points[5] = {{2.0, 4.0}, {9.0, 4.0}, {4.0, 1.0}, {5.5, 7.0}, {7.0, 1.0}};
  poddubny::Polygon obj4(5, points);
  poddubny::CompositeShape cs(std::make_shared<poddubny::Rectangle>(obj1));
  cs.addShape(std::make_shared<poddubny::Circle>(obj2));
  cs.addShape(std::make_shared<poddubny::Triangle>(obj3));
  cs.addShape(std::make_shared<poddubny::Polygon>(obj4));
  const poddubny::point_t testPoint = {12.0, 12.0};
  const double testArea = cs.getArea();
  cs.move(testPoint);
  BOOST_CHECK_CLOSE(cs.getPos().x, testPoint.x, ACCURACY);
  BOOST_CHECK_CLOSE(cs.getPos().y, testPoint.y, ACCURACY);
  BOOST_CHECK_CLOSE(cs.getArea(), testArea, ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeCopy)
{
  poddubny::Rectangle obj1({10.0, 20.0, {0.0, 0.0}});
  poddubny::CompositeShape cs1(std::make_shared<poddubny::Rectangle>(obj1));
  poddubny::CompositeShape cs2(cs1);
  poddubny::CompositeShape cs3 = cs1;
  BOOST_CHECK_CLOSE(cs1.getArea(), cs2.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(cs1.getArea(), cs3.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeAssignment)
{
  poddubny::Rectangle obj1({10.0, 20.0, {0.0, 0.0}});
  poddubny::CompositeShape cs1(std::make_shared<poddubny::Rectangle>(obj1));
  poddubny::CompositeShape cs2(std::make_shared<poddubny::Rectangle>(obj1));
  const double testArea1 = cs1.getArea();
  const double testArea2 = cs1.getArea();
  poddubny::CompositeShape cs3(std::move(cs1));
  poddubny::CompositeShape cs4;
  cs4 = std::move(cs2);
  BOOST_CHECK_CLOSE(testArea1, cs3.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(testArea2, cs4.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeArea)
{
  poddubny::Rectangle obj1({1.0, 2.0, {0.0, 0.0}});
  poddubny::Circle obj2(2.0, {4.0, 4.0});
  poddubny::Triangle obj3({0.0, 0.0}, {-1.0, -2.0}, {-3.0, -4.0});
  poddubny::point_t points[5] = {{2.0, 4.0}, {9.0, 4.0}, {4.0, 1.0}, {5.5, 7.0}, {7.0, 1.0}};
  poddubny::Polygon obj4(5, points);
  const double testArea = (obj1.getArea()) + (obj2.getArea()) + (obj3.getArea()) + (obj4.getArea());
  poddubny::CompositeShape cs(std::make_shared<poddubny::Rectangle>(obj1));
  cs.addShape(std::make_shared<poddubny::Circle>(obj2));
  cs.addShape(std::make_shared<poddubny::Triangle>(obj3));
  cs.addShape(std::make_shared<poddubny::Polygon>(obj4));
  BOOST_CHECK_CLOSE(cs.getArea(), testArea, ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeScale)
{
  poddubny::Rectangle obj1({1.0, 2.0, {0.0, 0.0}});
  poddubny::Circle obj2(2.0, {4.0, 4.0});
  poddubny::Triangle obj3({0.0, 0.0}, {-1.0, -2.0}, {-3.0, -4.0});
  poddubny::point_t points[5] = {{2.0, 4.0}, {9.0, 4.0}, {4.0, 1.0}, {5.5, 7.0}, {7.0, 1.0}};
  poddubny::Polygon obj4(5, points);
  const double testArea = (obj1.getArea()) + (obj2.getArea()) + (obj3.getArea()) + (obj4.getArea());
  poddubny::CompositeShape cs(std::make_shared<poddubny::Rectangle>(obj1));
  cs.addShape(std::make_shared<poddubny::Circle>(obj2));
  cs.addShape(std::make_shared<poddubny::Triangle>(obj3));
  cs.addShape(std::make_shared<poddubny::Polygon>(obj4));
  const double scaleRatio = 2;
  cs.scale(scaleRatio);
  BOOST_CHECK_CLOSE(cs.getArea(), testArea * scaleRatio * scaleRatio, ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeRotate)
{
  poddubny::Rectangle obj1({1.0, 2.0, {0.0, 0.0}});
  poddubny::Circle obj2(2.0, {4.0, 4.0});
  poddubny::Triangle obj3({0.0, 0.0}, {2.0, 0.0}, {2.0, 3.0});
  poddubny::point_t points[5] = {{2.0, 4.0}, {9.0, 4.0}, {4.0, 1.0}, {5.5, 7.0}, {7.0, 1.0}};
  poddubny::Polygon obj4(5, points);
  poddubny::CompositeShape cs(std::make_shared<poddubny::Rectangle>(obj1));
  cs.addShape(std::make_shared<poddubny::Circle>(obj2));
  cs.addShape(std::make_shared<poddubny::Triangle>(obj3));
  cs.addShape(std::make_shared<poddubny::Polygon>(obj4));
  const double areaBefore = cs.getArea();
  const double angle = 180;
  cs.rotate(angle);
  double areaAfter = cs.getArea();
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeAddShape)
{
  poddubny::Rectangle obj1({10.0, 20.0, {0.0, 0.0}});
  poddubny::Rectangle obj2({20.0, 30.0, {1.0, 2.0}});
  poddubny::CompositeShape cs(std::make_shared<poddubny::Rectangle>(obj1));
  BOOST_REQUIRE_EQUAL(cs.getSize(), 1);
  cs.addShape(std::make_shared<poddubny::Rectangle>(obj2));
  BOOST_REQUIRE_EQUAL(cs.getSize(), 2);
}

BOOST_AUTO_TEST_CASE(compositeShapeRemoveShape)
{
  poddubny::Rectangle obj1({10.0, 20.0, {0.0, 0.0}});
  poddubny::Rectangle obj2({20.0, 30.0, {1.0, 2.0}});
  poddubny::CompositeShape cs(std::make_shared<poddubny::Rectangle>(obj1));
  cs.addShape(std::make_shared<poddubny::Rectangle>(obj2));
  BOOST_REQUIRE_EQUAL(cs.getSize(), 2);
  cs.removeShape(1);
  BOOST_REQUIRE_EQUAL(cs.getSize(), 1);
}

BOOST_AUTO_TEST_CASE(compositeShapeClear)
{
  poddubny::Rectangle obj1({10.0, 20.0, {0.0, 0.0}});
  poddubny::Rectangle obj2({20.0, 30.0, {1.0, 2.0}});
  poddubny::CompositeShape cs(std::make_shared<poddubny::Rectangle>(obj1));
  cs.addShape(std::make_shared<poddubny::Rectangle>(obj2));
  cs.clear();
  BOOST_REQUIRE_EQUAL(cs.getSize(), 0);
}

BOOST_AUTO_TEST_CASE(compositeShapeRemoveShapeInvalidArgument)
{
  poddubny::Rectangle obj1({10.0, 20.0, {0.0, 0.0}});
  poddubny::CompositeShape cs(std::make_shared<poddubny::Rectangle>(obj1));
  BOOST_CHECK_THROW(cs.removeShape(2), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleInvalidArgument)
{
  poddubny::Rectangle obj1({10.0, 20.0, {0.0, 0.0}});
  poddubny::CompositeShape cs(std::make_shared<poddubny::Rectangle>(obj1));
  BOOST_CHECK_THROW(cs.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
