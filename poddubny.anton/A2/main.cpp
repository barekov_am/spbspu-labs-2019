#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

int main() 
{
  poddubny::Circle testCircle(1.0, {1.0, 1.0});
  std::cout << "Circle:";
  poddubny::Shape *figure = & testCircle;
  figure->showInfo();
  std::cout << "\n Moving by dx= 5 dy= 5";
  figure->move(5, 5);
  figure->showCenter();
  std::cout << "\n Moving to {4,2}";
  figure->move({5, 5});
  figure->showCenter();
  std::cout << "\n Scaling X2 ";
  figure->scale(2.0);
  figure->showFrameRect();

  poddubny::Rectangle testRectangle({1.0, 1.0, {1.0, 1.0}});
  std::cout << "\nRectangle:\n";
  figure = & testRectangle;
  figure->showInfo();
  std::cout << "\n Moving by dx= 5 dy= 5 ";
  figure->move(5.0, 5.0);
  figure->showCenter();
  std::cout << "\n Moving to {5,5} ";
  figure->move({5.0, 5.0});
  figure->showCenter();
  std::cout << "\n Scaling X2 ";
  figure->scale(0.5);
  figure->showFrameRect();

  poddubny::Triangle triangle({0.0, 0.0}, {7.0, 3.0}, {-2.0, 4.0});
  std::cout << "\n Triangle: \n";
  figure = & triangle;
  figure->showInfo();
  std::cout << "\n Moving by dx= -1 dy= 3.2 ";
  figure->move(-1.0, 3.2);
  figure->showCenter();
  std::cout << "\n Moving to {4,2} ";
  figure->move({4.0, 2.0});
  figure->showCenter();
  std::cout << "\n Scaling X2 ";
  figure->scale(2.0);
  figure->showFrameRect();

  const poddubny::point_t points[5] = {{2.0, 4.0}, {9.0, 4.0}, {4.0, 1.0}, {5.5, 7.0}, {7.0, 1.0}};
  poddubny::Polygon polygon(5, points);

  std::cout << "\n Polygon: \n";
  figure = & polygon;
  figure->showInfo();
  std::cout << "\n Moving by dx= -1 dy= 3.2 ";
  figure->move(-1.0, 3.2);
  figure->showCenter();
  std::cout << "\n Moving to {4,2} ";
  figure->move({4.0, 2.0});
  figure->showCenter();
  std::cout << "\n Scaling X2 ";
  figure->scale(2.0);
  figure->showFrameRect();
  return 0;
}
