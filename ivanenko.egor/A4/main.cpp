#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "pointers.hpp"
#include "splitter.hpp"
int main()
{
    ivanenko::CompositeShape compositeShape;
    ivanenko::ShapePointer rectangle1(std::make_shared<ivanenko::Rectangle>(1, 1, 5, 5));
    ivanenko::ShapePointer rectangle2(std::make_shared<ivanenko::Rectangle>(2, 2, 5, 5));
    ivanenko::ShapePointer rectangle3(std::make_shared<ivanenko::Rectangle>(13, 10, 0.1, 0.1));
    ivanenko::ShapePointer circle1(std::make_shared<ivanenko::Circle>(10, 10, 1));
    ivanenko::ShapePointer circle2(std::make_shared<ivanenko::Circle>(10, 10, 3));

    compositeShape.add(rectangle1);
    compositeShape.add(rectangle2);
    compositeShape.add(circle1);
    compositeShape.add(circle2);
    compositeShape.add(rectangle3);

    compositeShape.printInfo();
    compositeShape.rotate(45);
    compositeShape.printInfo();

    ivanenko::Matrix matrix = ivanenko::split(compositeShape);
    for (unsigned int i = 0; i < matrix.rows(); i++)
    {
      std::cout << "layer = " << i << ":" << std::endl;
      for (unsigned int j = 0; j < matrix.columns(i); j++)
      {
        matrix[i][j]->printInfo();
      }
    }
  return 0;
}


