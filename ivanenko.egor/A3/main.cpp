#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include <iostream>

int main()
{
  ivanenko::ShapePointer circ1(new ivanenko::Circle(1, 2, 3));
  ivanenko::ShapePointer circ2(new ivanenko::Circle(6, 5, 4));
  ivanenko::ShapePointer circ3(new ivanenko::Circle(7, 8, 9));
  ivanenko::ShapePointer rect1(new ivanenko::Rectangle(0, 0, 5, 5));
  ivanenko::ShapePointer rect2(new ivanenko::Rectangle(0, 0, 10, 10));
  ivanenko::CompositeShape compositeShape1 = ivanenko::CompositeShape();
  compositeShape1.printInfo();
  compositeShape1.add(circ1);
  compositeShape1.add(circ2);
  compositeShape1.add(circ3);
  compositeShape1.add(rect1);
  compositeShape1.printInfo();
  compositeShape1.add(rect2);
  compositeShape1.printInfo();
  compositeShape1.scale(2);
  compositeShape1.printInfo();
  compositeShape1.move(10, 10);
  ivanenko::CompositeShape compositeShape2(std::move(compositeShape1));
  compositeShape2.printInfo();
  compositeShape2.remove(4);
  compositeShape2.printInfo();
}


