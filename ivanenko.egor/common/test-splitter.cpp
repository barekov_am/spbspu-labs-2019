#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <iostream>

#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "splitter.hpp"

BOOST_AUTO_TEST_SUITE(shapeSplitterTest)

BOOST_AUTO_TEST_CASE(correctSplit)
{
  ivanenko::CompositeShape compositeShape;

  ivanenko::ShapePointer rectangle1(std::make_shared<ivanenko::Rectangle>(1, 1, 5, 5));
  ivanenko::ShapePointer rectangle2(std::make_shared<ivanenko::Rectangle>(2, 2, 5, 5));

  ivanenko::ShapePointer circle1(std::make_shared<ivanenko::Circle>(10, 10, 1));
  ivanenko::ShapePointer circle2(std::make_shared<ivanenko::Circle>(10, 10, 3));

  compositeShape.add(rectangle1);
  compositeShape.add(rectangle2);
  compositeShape.add(circle1);
  compositeShape.add(circle2);

  ivanenko::Matrix matrix = ivanenko::split(compositeShape);

  BOOST_CHECK_EQUAL(matrix[0][0] == rectangle1, true);
  BOOST_CHECK_EQUAL(matrix[0][1] == circle1, true);
  BOOST_CHECK_EQUAL(matrix[1][0] == rectangle2, true);
  BOOST_CHECK_EQUAL(matrix[1][1] == circle2, true);
}

BOOST_AUTO_TEST_SUITE_END()

