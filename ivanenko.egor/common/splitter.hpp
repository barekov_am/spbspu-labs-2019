#ifndef SPLITTER_HPP
#define SPLITTER_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace ivanenko
{
  Matrix split(const CompositeShape & compositeShape);
}

#endif //SPLITTER_HPP

