#ifndef A2_COMPOSITE_SHAPE_HPP
#define A2_COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include "pointers.hpp"
#include <memory>

namespace ivanenko
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &newCompositeShape);
    CompositeShape(CompositeShape &&newCompositeShape) noexcept;
    ~CompositeShape() override = default;

    CompositeShape &operator =(const CompositeShape &newCompositeShape);
    CompositeShape &operator =(CompositeShape &&newCompositeShape);
    ShapePointer &operator [](unsigned int index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &newPos) override;
    void move(double dx, double dy) override;
    void scale(double scale) override;
    void add(ShapePointer shape);
    void remove(unsigned int index);
    unsigned int getSize() const;
    void printInfo() const override;
    void rotate(double angle) override;;
    double getAngle() const;

  private:
    double angle_;
    ShapeArray shapeArray_;
    unsigned int shapeCount_;
    unsigned int arraySize_;
  };
}

#endif //A2_COMPOSITE_SHAPE_HPP

