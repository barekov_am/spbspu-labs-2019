#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double CHECK = 0.01;

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(movementConstRect)
{
  ivanenko::Rectangle rectangle(5.0, 5.0, {5.0, 5.0});
  const ivanenko::rectangle_t beforeRectFrame = rectangle.getFrameRect();
  const double beforeRectArea = rectangle.getArea();

  rectangle.move(1,1);
  BOOST_CHECK_EQUAL(beforeRectFrame.width, rectangle.getFrameRect().width);
  BOOST_CHECK_EQUAL(beforeRectFrame.height, rectangle.getFrameRect().height);
  BOOST_CHECK_CLOSE(beforeRectArea, rectangle.getArea(), CHECK);

  rectangle.move({10,10});
  BOOST_CHECK_EQUAL(beforeRectFrame.width, rectangle.getFrameRect().width);
  BOOST_CHECK_EQUAL(beforeRectFrame.height, rectangle.getFrameRect().height);
  BOOST_CHECK_CLOSE(beforeRectArea, rectangle.getArea(), CHECK);
}

BOOST_AUTO_TEST_CASE(scalingRect)
{
  ivanenko::Rectangle rectangle(5.0, 5.0, {5.0, 5.0});
  const double beforeRectArea = rectangle.getArea();
  const double scaling = 1.5;

  rectangle.scale(scaling);
  BOOST_CHECK_CLOSE(beforeRectArea * scaling * scaling, rectangle.getArea(), CHECK);
}

BOOST_AUTO_TEST_CASE(wrongArgumentsRect)
{
  BOOST_CHECK_THROW(ivanenko::Rectangle rectangle(-5.0, 5.0, {5.0, 5.0}), std::invalid_argument);
  BOOST_CHECK_THROW(ivanenko::Rectangle rectangle(5.0, -5.0, {5.0, 5.0}), std::invalid_argument);

  ivanenko::Rectangle rectangle(5.0, 5.0, {5.0, 5.0});
  BOOST_CHECK_THROW(rectangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(centerConstancyAfterRotating)
{
  const ivanenko::point_t oldCenter = {10, 5};
  ivanenko::Rectangle rectangle = ivanenko::Rectangle(10, 5, oldCenter);
  rectangle.rotate(45);
  const ivanenko::point_t newCenter = rectangle.getFrameRect().pos;
  BOOST_CHECK_CLOSE(newCenter.x, oldCenter.x, CHECK);
  BOOST_CHECK_CLOSE(newCenter.y, oldCenter.y, CHECK);
}

BOOST_AUTO_TEST_CASE(areaConstancyAfterRotating)
{
  ivanenko::Rectangle rectangle = ivanenko::Rectangle(10, 5, {10, 5});
  const double oldArea = rectangle.getArea();
  rectangle.rotate(45);
  BOOST_CHECK_CLOSE(oldArea, rectangle.getArea(), CHECK);
}

BOOST_AUTO_TEST_SUITE_END()

