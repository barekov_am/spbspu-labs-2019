#ifndef POINTERS_HPP
#define POINTERS_HPP

#include <memory>

#include "shape.hpp"

namespace ivanenko
{
using ShapePointer = std::shared_ptr<Shape>;
using ShapeArray = std::unique_ptr<ShapePointer[]>;
using RectangleArray = std::unique_ptr<rectangle_t[]>;
using UIntArray = std::unique_ptr<unsigned int[]>;
using BoolArray = std::unique_ptr<bool[]>;
}

#endif //POINTERS_HPP
