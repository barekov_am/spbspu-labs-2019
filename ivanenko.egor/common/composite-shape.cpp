#include "composite-shape.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

ivanenko::CompositeShape::CompositeShape() :
  angle_(0),
  shapeCount_(0),
  arraySize_(0)
{
}

ivanenko::CompositeShape::CompositeShape(const ivanenko::CompositeShape &newCompositeShape) :
  angle_(newCompositeShape.angle_),
  shapeArray_(std::make_unique<ShapePointer[]>(newCompositeShape.arraySize_)),
  shapeCount_(newCompositeShape.shapeCount_),
  arraySize_(newCompositeShape.arraySize_)
{
  for (unsigned int i = 0; i < arraySize_; i++)
  {
    shapeArray_[i] = newCompositeShape.shapeArray_[i];
  }
}


ivanenko::CompositeShape::CompositeShape(CompositeShape &&newCompositeShape) noexcept:
  angle_(newCompositeShape.angle_),
  shapeArray_(std::move(newCompositeShape.shapeArray_)),
  shapeCount_(newCompositeShape.shapeCount_),
  arraySize_(newCompositeShape.arraySize_)
{
}

ivanenko::CompositeShape &ivanenko::CompositeShape::operator =(const CompositeShape &newCompositeShape)
{
  if (&newCompositeShape != this)
  {
    ShapeArray temporaryArray = std::make_unique<ShapePointer[]>(newCompositeShape.arraySize_);

    for (unsigned int i = 0; i < newCompositeShape.arraySize_; i++)
    {
      temporaryArray[i] = newCompositeShape.shapeArray_[i];
    }

    shapeArray_.swap(temporaryArray);
    angle_ = newCompositeShape.angle_;
    arraySize_ = newCompositeShape.arraySize_;
    shapeCount_ = newCompositeShape.shapeCount_;
  }
  return *this;
}


ivanenko::CompositeShape &ivanenko::CompositeShape::operator =(CompositeShape &&newCompositeShape)
{
  if (&newCompositeShape == this)
  {
    return * this;
  }
  shapeArray_ = std::move(newCompositeShape.shapeArray_);
  angle_ = newCompositeShape.angle_;
  shapeCount_ = newCompositeShape.shapeCount_;
  arraySize_ = newCompositeShape.arraySize_;

  return * this;
}

ivanenko::ShapePointer &ivanenko::CompositeShape::operator [](unsigned int index) const
{
  if (index >= shapeCount_)
  {
    throw std::out_of_range("Out of range index");
  }
  return shapeArray_[index];
}


double ivanenko::CompositeShape::getArea() const
{
  double area = 0;
  for (unsigned int i = 0; i < shapeCount_; i++)
  {
    area += shapeArray_[i]->getArea();
  }
  return area;
}

ivanenko::rectangle_t ivanenko::CompositeShape::getFrameRect() const
{
  if (shapeCount_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  rectangle_t frameRect = shapeArray_[0]->getFrameRect();
  double minX = frameRect.pos.x - frameRect.width / 2;
  double maxX = frameRect.pos.x + frameRect.width / 2;
  double minY = frameRect.pos.y - frameRect.height / 2;
  double maxY = frameRect.pos.y + frameRect.height / 2;

  for (unsigned int i = 0; i < shapeCount_; i++)
  {
    frameRect = shapeArray_[i]->getFrameRect();
    minX = std::min(minX, frameRect.pos.x - frameRect.width / 2);
    maxX = std::max(maxX, frameRect.pos.x + frameRect.width / 2);
    minY = std::min(minY, frameRect.pos.y - frameRect.height / 2);
    maxY = std::max(maxY, frameRect.pos.y + frameRect.height / 2);
  }
  return rectangle_t{maxX - minX, maxY - minY, {(minX + maxX) / 2, (minY + maxY) / 2}};
}

void ivanenko::CompositeShape::move(const ivanenko::point_t &newPos)
{
  if (shapeCount_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  rectangle_t frameRect = getFrameRect();
  double dx = newPos.x - frameRect.pos.x;
  double dy = newPos.y - frameRect.pos.y;

  move(dx,dy);
}

void ivanenko::CompositeShape::move(double dx, double dy)
{
  if (shapeCount_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  for (unsigned int i = 0; i < shapeCount_; i++)
  {
    shapeArray_[i]->move(dx,dy);
  }
}

void ivanenko::CompositeShape::scale(double scale)
{
  if (scale <= 0.0)
  {
    throw std::invalid_argument("Invalid scale");
  }

  if (shapeCount_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  rectangle_t frameRect = getFrameRect();
  for (unsigned int i = 0; i < shapeCount_; i++)
  {
    double dx = (shapeArray_[i]->getFrameRect().pos.x - frameRect.pos.x) * scale;
    double dy = (shapeArray_[i]->getFrameRect().pos.y - frameRect.pos.y) * scale;
    shapeArray_[i]->move({frameRect.pos.x + dx, frameRect.pos.y +dy});
    shapeArray_[i]->scale(scale);
  }
}

void ivanenko::CompositeShape::add(ShapePointer shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape must be not null pointer");
  }
  if (shapeCount_ < arraySize_)
  {
    shapeArray_[shapeCount_++] = shape;
  }
  else
  {
    ShapeArray temporaryArray = std::make_unique<ShapePointer[]>(arraySize_ + 1);
    for (unsigned int i = 0; i < arraySize_; i++)
    {
      temporaryArray[i] = shapeArray_[i];
    }
    temporaryArray[shapeCount_] = shape;
    arraySize_++;
    shapeCount_++;
    shapeArray_.swap(temporaryArray);
  }
}


void ivanenko::CompositeShape::remove(unsigned int index)
{
  if (index >= shapeCount_)
  {
    throw std::invalid_argument("Index must be less");
  }
  shapeCount_--;
  for (unsigned int i = 0; i < shapeCount_; i++)
  {
    shapeArray_[i] = shapeArray_[i + 1];
  }
  shapeArray_[shapeCount_] = nullptr;
}

unsigned int ivanenko::CompositeShape::getSize() const
{
  return shapeCount_;
}

void ivanenko::CompositeShape::rotate(double degrees)
{
  if (shapeCount_ == 0)
  {
    throw std::logic_error("Empty CompositeShape");
  }
  const point_t center = getFrameRect().pos;
  for (unsigned int i = 0; i < shapeCount_; i++)
  {
    shapeArray_[i]->rotate(degrees);

    const point_t shapeCenter = shapeArray_[i]->getFrameRect().pos;

    const double distanceX = shapeCenter.x - center.x;
    const double distanceY = shapeCenter.y - center.y;
    const double distance = sqrt(distanceX * distanceX + distanceY * distanceY);
    const double deviation = 0.000001;
    
    if (distance < deviation)
    {
      continue;
    }

    const double oldAngle = (std::abs(distanceX) > deviation)
        ? (atan(distanceY / distanceX) + ((distanceX < 0) ? M_PI : 0))
        : (distanceY > 0) ? (M_PI / 2) : (-M_PI / 2);

    const double newSin = sin(oldAngle + degrees * M_PI / 180);
    const double newCos = cos(oldAngle + degrees * M_PI / 180);

    shapeArray_[i]->move({center.x + distance * newCos, center.y + distance * newSin});
  }
  angle_ = fmod(angle_ + degrees, 360);
}

double ivanenko::CompositeShape::getAngle() const
{
  return angle_;
}

void ivanenko::CompositeShape::printInfo() const
{
  if (shapeCount_ == 0)
  {
    std::cout << "Empty composite shape" << std::endl << std::endl;
    return;
  }

  const rectangle_t frameRect = getFrameRect();

  std::cout << "Composite shape. Center at ("
            << frameRect.pos.x << "; " << frameRect.pos.y << ")"
            << std::endl << "It includes " << shapeCount_ << " figures"
            << std::endl << "Area = " << getArea()
            << std::endl << "frame rectangle width = " << frameRect.width
            << std::endl << "frame rectangle height = " << frameRect.height
            << std::endl << "angle = " << angle_
            << std::endl << std::endl;
}

