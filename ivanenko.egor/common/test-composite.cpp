#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>

#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double CHECK = 0.01;

BOOST_AUTO_TEST_SUITE(compositeShapeTest)

BOOST_AUTO_TEST_CASE(copyConstructorTest)
{
  ivanenko::ShapePointer rectangle(std::make_shared<ivanenko::Rectangle>(ivanenko::Rectangle(10, 20, {10, 10})));
  ivanenko::ShapePointer circle(std::make_shared<ivanenko::Circle>(ivanenko::Circle({10, 10}, 5)));

  ivanenko::CompositeShape compositeShape1 = ivanenko::CompositeShape();
  compositeShape1.add(rectangle);
  compositeShape1.add(circle);

  const ivanenko::CompositeShape compositeShape2(compositeShape1);

  const ivanenko::rectangle_t frameRect1 = compositeShape1.getFrameRect();
  const ivanenko::rectangle_t frameRect2 = compositeShape2.getFrameRect();

  BOOST_CHECK_CLOSE(frameRect1.pos.x, frameRect2.pos.x, CHECK);
  BOOST_CHECK_CLOSE(frameRect1.pos.y, frameRect2.pos.y, CHECK);
  BOOST_CHECK_CLOSE(frameRect1.width, frameRect2.width, CHECK);
  BOOST_CHECK_CLOSE(frameRect1.height, frameRect2.height, CHECK);
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), compositeShape2.getArea(), CHECK);
  BOOST_CHECK_EQUAL(compositeShape1.getSize(), compositeShape2.getSize());
}

BOOST_AUTO_TEST_CASE(paramsConstancyAfterMoving)
{
  ivanenko::ShapePointer rectangle(std::make_shared<ivanenko::Rectangle>
      (ivanenko::Rectangle(10, 20, {10, 10})));
  ivanenko::ShapePointer circle(std::make_shared<ivanenko::Circle>(ivanenko::Circle({10, 10}, 5)));
  ivanenko::CompositeShape compositeShape = ivanenko::CompositeShape();

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  const double oldArea = compositeShape.getArea();
  const ivanenko::rectangle_t oldFrameRect = compositeShape.getFrameRect();

  compositeShape.move({500, 500});
  compositeShape.move(123, 456);

  const double newArea = compositeShape.getArea();
  const ivanenko::rectangle_t newFrameRect = compositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(oldFrameRect.width, newFrameRect.width, CHECK);
  BOOST_CHECK_CLOSE(oldFrameRect.height, newFrameRect.height, CHECK);
  BOOST_CHECK_CLOSE(oldArea, newArea, CHECK);
}

BOOST_AUTO_TEST_CASE(centerConstancyAfterScaling)
{
  ivanenko::ShapePointer rectangle(std::make_shared<ivanenko::Rectangle>
      (ivanenko::Rectangle(10, 20, {10, 10})));
  ivanenko::ShapePointer circle(std::make_shared<ivanenko::Circle>(ivanenko::Circle({10, 10}, 5)));
  ivanenko::CompositeShape compositeShape = ivanenko::CompositeShape();

  compositeShape.add(circle);
  compositeShape.add(rectangle);
  const ivanenko::point_t oldCenter = compositeShape.getFrameRect().pos;

  const double scaleAmount1 = 4;
  compositeShape.scale(scaleAmount1);
  const ivanenko::point_t center2 = compositeShape.getFrameRect().pos;
  BOOST_CHECK_CLOSE(oldCenter.x, center2.x, CHECK);
  BOOST_CHECK_CLOSE(oldCenter.y, center2.y, CHECK);

  const double scaleAmount2 = 0.5;
  compositeShape.scale(scaleAmount2);
  const ivanenko::point_t center3 = compositeShape.getFrameRect().pos;
  BOOST_CHECK_CLOSE(oldCenter.x, center3.x, CHECK);
  BOOST_CHECK_CLOSE(oldCenter.y, center3.y, CHECK);
}

BOOST_AUTO_TEST_CASE(quadraticSquareChangesAfterScaling)
{
  ivanenko::ShapePointer rectangle(std::make_shared<ivanenko::Rectangle>
      (ivanenko::Rectangle(10, 20, {10, 10})));
  ivanenko::ShapePointer circle(std::make_shared<ivanenko::Circle>(ivanenko::Circle({10, 10}, 5)));
  ivanenko::CompositeShape compositeShape = ivanenko::CompositeShape();

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  const double oldArea1 = compositeShape.getArea();
  const double amount1 = 2;
  compositeShape.scale(amount1);
  const double newArea1 = compositeShape.getArea();
  BOOST_CHECK_CLOSE(oldArea1 * amount1 * amount1, newArea1, CHECK);

  const double oldArea2 = compositeShape.getArea();
  const double amount2 = 0.5;
  compositeShape.scale(amount2);
  const double newArea2 = compositeShape.getArea();
  BOOST_CHECK_CLOSE(oldArea2 * amount2 * amount2, newArea2, CHECK);
}

BOOST_AUTO_TEST_CASE(scaleExceptions)
{
  ivanenko::ShapePointer rectangle(std::make_shared<ivanenko::Rectangle>
      (ivanenko::Rectangle(10, 20, {10, 10})));
  ivanenko::ShapePointer circle(std::make_shared<ivanenko::Circle>(ivanenko::Circle({10, 10}, 5)));
  ivanenko::CompositeShape compositeShape = ivanenko::CompositeShape();

  compositeShape.add(circle);
  compositeShape.add(rectangle);

  const double incorrectArg1 = -2;
  const double incorrectArg2 = 0;
  BOOST_CHECK_THROW(compositeShape.scale(incorrectArg1), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.scale(incorrectArg2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(addShapeTest)
{
  ivanenko::ShapePointer rectangle(std::make_shared<ivanenko::Rectangle>(ivanenko::Rectangle(10, 20, {10, 10})));
  ivanenko::ShapePointer circle(std::make_shared<ivanenko::Circle>(ivanenko::Circle({10, 10}, 5)));
  ivanenko::CompositeShape compositeShape = ivanenko::CompositeShape();

  compositeShape.add(circle);

  BOOST_CHECK_CLOSE(compositeShape[0]->getArea(), circle->getArea(), CHECK);
  BOOST_CHECK_CLOSE(compositeShape[0]->getFrameRect().width, circle->getFrameRect().width, CHECK);
  BOOST_CHECK_CLOSE(compositeShape[0]->getFrameRect().height, circle->getFrameRect().height, CHECK);
  BOOST_CHECK_CLOSE(compositeShape[0]->getFrameRect().pos.x, circle->getFrameRect().pos.x, CHECK);
  BOOST_CHECK_CLOSE(compositeShape[0]->getFrameRect().pos.y, circle->getFrameRect().pos.y, CHECK);
}

BOOST_AUTO_TEST_CASE(shapeCountChanges)
{
  ivanenko::ShapePointer circle(std::make_shared<ivanenko::Circle>(ivanenko::Circle({10, 10}, 5)));
  ivanenko::CompositeShape compositeShape = ivanenko::CompositeShape();
  const unsigned int shapeCount1 = compositeShape.getSize();

  compositeShape.add(circle);
  const unsigned int shapeCount2 = compositeShape.getSize();
  BOOST_CHECK_EQUAL(shapeCount1, shapeCount2 - 1);

  compositeShape.remove(0);
  const unsigned int shapeCount3 = compositeShape.getSize();
  BOOST_CHECK_EQUAL(shapeCount2 - 1, shapeCount3);
}

BOOST_AUTO_TEST_CASE(rotateTest)
{
  ivanenko::ShapePointer rectangle1(std::make_shared<ivanenko::Rectangle>(30.5, 8, 1, 7));
  ivanenko::ShapePointer rectangle2(std::make_shared<ivanenko::Rectangle>(12.5, 8.5, 10, 4));

  ivanenko::ShapePointer circle1(std::make_shared<ivanenko::Circle>(ivanenko::Circle({20, 10}, 5)));
  ivanenko::ShapePointer circle2(std::make_shared<ivanenko::Circle>(ivanenko::Circle({30, 15}, 7)));

  ivanenko::CompositeShape compositeShape;

  compositeShape.add(rectangle1);
  compositeShape.add(rectangle2);
  compositeShape.add(circle1);
  compositeShape.add(circle2);

  const double oldArea = compositeShape.getArea();

  for (int angle = -720; angle <= 720; angle += 45)
  {
    compositeShape.rotate(angle);
    BOOST_CHECK_CLOSE(oldArea, compositeShape.getArea(), CHECK);
  }
}

BOOST_AUTO_TEST_CASE(moveConstructor)
{
  ivanenko::ShapePointer circle(std::make_shared<ivanenko::Circle>(ivanenko::Circle({10, 10}, 5)));
  ivanenko::ShapePointer rectangle(std::make_shared<ivanenko::Rectangle>(ivanenko::Rectangle(10, 20, {10, 10})));
  ivanenko::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const ivanenko::rectangle_t frameRectOfComposite = composite.getFrameRect();
  ivanenko::CompositeShape moveComposite = std::move(composite);
  const double areaOfMoveComposite = moveComposite.getArea();
  const double sizeOfMoveComposite = moveComposite.getSize();
  const ivanenko::rectangle_t frameRectOfMoveComposite = moveComposite.getFrameRect();
  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfMoveComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfMoveComposite, CHECK);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfMoveComposite.width, CHECK);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfMoveComposite.height, CHECK);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfMoveComposite.pos.x, CHECK);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfMoveComposite.pos.y, CHECK);
}

BOOST_AUTO_TEST_CASE(moveOperator)
{
  ivanenko::ShapePointer circleComposite(std::make_shared<ivanenko::Circle>(ivanenko::Circle({10, 10}, 5)));
  ivanenko::ShapePointer rectangleComposite(std::make_shared<ivanenko::Rectangle>(ivanenko::Rectangle(10, 20, {10, 10})));
  ivanenko::CompositeShape composite;
  composite.add(circleComposite);
  composite.add(rectangleComposite);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const ivanenko::rectangle_t frameRectOfComposite = composite.getFrameRect();
  ivanenko::ShapePointer circleOfMoveComposite(std::make_shared<ivanenko::Circle>(ivanenko::Circle({2, 5.2}, 2)));
  ivanenko::ShapePointer rectangleOfMoveComposite(std::make_shared<ivanenko::Rectangle>(ivanenko::Rectangle(1, 3.5, {4.2, 3.2})));
  ivanenko::CompositeShape moveComposite;
  moveComposite.add(circleOfMoveComposite);
  moveComposite.add(rectangleOfMoveComposite);
  moveComposite = std::move(composite);
  const double areaOfMoveComposite = moveComposite.getArea();
  const double sizeOfMoveComposite = moveComposite.getSize();
  const ivanenko::rectangle_t frameRectOfMoveComposite = moveComposite.getFrameRect();
  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfMoveComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfMoveComposite, CHECK);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfMoveComposite.width, CHECK);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfMoveComposite.height, CHECK);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfMoveComposite.pos.x, CHECK);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfMoveComposite.pos.y, CHECK);
}

BOOST_AUTO_TEST_SUITE_END()

