#include "base-types.hpp"

#include <cmath>

bool ivanenko::rectangle_t::isOverlaps(const ivanenko::rectangle_t & rect) const
{
  return isInside(rect.pos.x + rect.width / 2, rect.pos.y + rect.height / 2)
      || isInside(rect.pos.x - rect.width / 2, rect.pos.y - rect.height / 2)
      || isInside(rect.pos.x + rect.width / 2, rect.pos.y - rect.height / 2)
      || isInside(rect.pos.x - rect.width / 2, rect.pos.y + rect.height / 2)
      || rect.isInside(pos.x + width / 2, pos.y + height / 2)
      || rect.isInside(pos.x - width / 2, pos.y - height / 2)
      || rect.isInside(pos.x + width / 2, pos.y - height / 2)
      || rect.isInside(pos.x - width / 2, pos.y + height / 2);
}

bool ivanenko::rectangle_t::isInside(double x, double y) const
{
  return (std::abs(pos.x - x) <= width / 2)
      && (std::abs(pos.y - y) <= height / 2);
}
