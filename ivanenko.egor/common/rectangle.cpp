#include "rectangle.hpp"
#include "base-types.hpp"
#include <cassert>
#include <stdexcept>
#include <iostream>
#include <cmath>

ivanenko::Rectangle::Rectangle(double width, double height, const point_t &pos) :
  width_(width),
  height_(height),
  pos_(pos),
  angle_(0)
{
  if (width_ <= 0.0)
  {
    throw std::invalid_argument("Invalid width");
  }
  if (height_ <= 0.0)
  {
    throw std::invalid_argument("Invalid height");
  }
}

ivanenko::Rectangle::Rectangle(double width, double height, double X, double Y):
Rectangle(width, height, {X, Y})
{
}


double ivanenko::Rectangle::getArea() const
{
  return width_ * height_;
}

ivanenko::rectangle_t ivanenko::Rectangle::getFrameRect() const
{
  return {width_, height_, pos_};
}

void ivanenko::Rectangle::move(const point_t &pos)
{
  pos_ = pos;
}

void ivanenko::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void ivanenko::Rectangle::scale(const double scale)
{
  if (scale <= 0.0)
  {
    throw std::invalid_argument("Invalid scale");
  }
  width_ *= scale;
  height_ *= scale;
}

void ivanenko::Rectangle::printInfo() const
{
  std::cout << "Area of rectangle: " << getArea() << std::endl;
  std::cout << "Width of frame rectangle: " << width_ << std::endl;
  std::cout << "Height of frame rectangle: " << height_ << std::endl;
  std::cout << "Center point of frame rectangle: (" << pos_.x
            << "; " << pos_.y << ")" << std::endl;
}

void ivanenko::Rectangle::rotate (double degrees)
{
  angle_ += fmod(angle_ + degrees, 360);
}

