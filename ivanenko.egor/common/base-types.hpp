#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace ivanenko
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    double width;
    double height;
    point_t pos;

    bool isOverlaps(const rectangle_t & rect) const;
    bool isInside(double x, double y) const;
  };
}

#endif //BASE_TYPES_HPP

