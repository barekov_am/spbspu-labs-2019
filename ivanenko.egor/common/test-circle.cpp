#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

const double CHECK = 0.01;

BOOST_AUTO_TEST_SUITE(circleTest)

BOOST_AUTO_TEST_CASE(movementConstCircle)
{
  ivanenko::Circle circle({5.0, 5.0}, 5.0);
  const ivanenko::rectangle_t beforeCircleFrame = circle.getFrameRect();
  const double beforeCircleArea = circle.getArea();

  circle.move(1,1);
  BOOST_CHECK_EQUAL(beforeCircleFrame.width, circle.getFrameRect().width);
  BOOST_CHECK_EQUAL(beforeCircleFrame.height, circle.getFrameRect().height);
  BOOST_CHECK_CLOSE(beforeCircleArea, circle.getArea(), CHECK);

  circle.move({10,10});
  BOOST_CHECK_EQUAL(beforeCircleFrame.width, circle.getFrameRect().width);
  BOOST_CHECK_EQUAL(beforeCircleFrame.height, circle.getFrameRect().height);
  BOOST_CHECK_CLOSE(beforeCircleArea, circle.getArea(), CHECK);
}

BOOST_AUTO_TEST_CASE(scalingCircle)
{
  ivanenko::Circle circle({5.0, 5.0}, 5.0);
  const double beforeCircleArea = circle.getArea();
  const double scaling = 1.5;

  circle.scale(scaling);
  BOOST_CHECK_CLOSE(beforeCircleArea * scaling * scaling, circle.getArea(), CHECK);
}

BOOST_AUTO_TEST_CASE(wrongArgumentsCircle)
{
  BOOST_CHECK_THROW(ivanenko::Circle circle({5.0, 5.0}, -5.0), std::invalid_argument);

  ivanenko::Circle circle({5.0, 5.0}, 5.0);
  BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

