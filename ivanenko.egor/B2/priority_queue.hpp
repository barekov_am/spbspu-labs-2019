#ifndef B2_PRIORITY_QUEUE_HPP
#define B2_PRIORITY_QUEUE_HPP

#include <list>
#include <stdexcept>

template<typename T>
class PriorityQueue
{
public:
  enum class Priority
  {
    LOW,
    NORMAL,
    HIGH
  };

  void add(Priority, const T &);

  T get();

  void accelerate();

  bool isEmpty() const;

private:
  std::list<T> lowList_;
  std::list<T> normalList_;
  std::list<T> highList_;
};

#endif
