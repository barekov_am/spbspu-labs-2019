#include <iostream>
#include <list>

const int MAX = 20;

void task2()
{
  std::list<int> list;
  int data;

  while (std::cin >> data, !std::cin.eof())
  {
    if ((data < 1) || (data > MAX))
    {
      throw std::invalid_argument("Data must be higher than 1 and lower than 20");
    }

    if (list.size() >= MAX)
    {
      throw std::out_of_range("Size of list can`t be higher than 20");
    }

    list.push_back(data);
  }

  if ((!std::cin.eof()) && (std::cin.fail()))
  {
    throw std::runtime_error("Could not read from input stream");
  }

  auto start = list.begin();
  auto finish = list.end();

  while (start != finish)
  {
    std::cout << *start << " ";

    if (std::next(start) == finish)
    {
      break;
    }

    start++;
    finish--;
    std::cout << *finish << " ";
  }
  std::cout << "\n";
}

