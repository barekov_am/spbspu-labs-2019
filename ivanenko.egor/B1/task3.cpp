#include <vector>
#include <stdexcept>
#include <iostream>

#include "additions.hpp"

void task3()
{
  std::vector<int> vector;

  int i = -1;
  while (std::cin && !(std::cin >> i).eof())
  {
    if (std::cin.fail())
    {
      throw std::invalid_argument("Wrong input data");
    }
    if (i == 0)
    {
      break;
    }
    vector.push_back(i);
  }

  if (vector.empty())
  {
    return;
  }

  if (i != 0)
  {
    throw std::invalid_argument("No zero at the end");
  }


  auto it = vector.begin();

  if (vector.back() == 1)
  {
    while(it != vector.end())
    {
      if (*it % 2 == 0)
      {
        it = vector.erase(it);
      }
      else
      {
        ++it;
      }
    }
  }
  else if (vector.back() == 2)
  {
    while (it != vector.end())
    {
      if (*it % 3 == 0)
      {
        it = vector.insert(it + 1, 3, 1);
        it += 2;
      }
      ++it;
    }
  }

  print(vector);
}

