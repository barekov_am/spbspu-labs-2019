#include <vector>
#include <iostream>

#include "additions.hpp"

void fillRandom(double * array, const size_t size)
{
  for (size_t i = 0; i < size; ++i)
  {
    array[i] = -1.0 + static_cast<double>(rand()) / RAND_MAX * 2.0;
  }
}

void task4(const char * direction, const size_t size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Wrong size");
  }

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);
  print(vector);
  sort<AccessIterator>(vector, getDirection<double>(direction));
  std::cout << "\n";
  print(vector);
}

