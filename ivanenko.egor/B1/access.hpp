#ifndef B1_ACCESS_HPP
#define B1_ACCESS_HPP

#include <iostream>

template <typename Container>
struct AccessBrackets
{
  using valueType = typename Container::value_type;

  static unsigned int begin(const Container &)
  {
    return 0;
  }

  static unsigned int end(const Container & container)
  {
    return container.size();
  }

  static valueType & get(Container & container, unsigned int index)
  {
    if (index < 0 || index >= container.size())
    {
      throw std::invalid_argument("Index out of range");
    }
  
    return container[index];
  }
};

template <typename Container>
struct AccessAt
{
  using valueType = typename Container::value_type;

  static unsigned int begin(const Container &)
  {
    return 0;
  }

  static unsigned int end(const Container & container)
  {
    return container.size();
  }

  static valueType & get(Container & container, unsigned int index)
  {
    return container.at(index);
  }
};

template <typename Container>
struct AccessIterator
{
  using valueType = typename Container::value_type;

  static auto begin(Container & container)
  {
    return container.begin();
  }

  static auto end(Container & container)
  {
    return container.end();
  }

  static valueType & get(Container &, typename Container::iterator iter)
  {
    return *iter;
  }
};


#endif //B1_ACCESS_HPP

