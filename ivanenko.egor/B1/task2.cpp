#include <fstream>
#include <memory>
#include <vector>
#include <iostream>


const size_t initSize = 256;

void task2(const char * file)
{
  std::ifstream inputFile(file);
  if (!inputFile)
  {
    throw std::runtime_error("File read error");
  }

  size_t size = initSize;
  std::unique_ptr<char[], decltype(&free)> data(static_cast<char *>(malloc(size)), &free);
  size_t index = 0;

  while (inputFile)
  {
    inputFile.read(&data[index], initSize);
    index += inputFile.gcount();
    if (inputFile.gcount() == initSize)
    {
      size += initSize;
      std::unique_ptr<char[], decltype(&free)> newData(static_cast<char *>(realloc(data.get(), size)), &free);
      data.release();
      std::swap(data, newData);
    }
    if (!inputFile.eof() && inputFile.fail())
    {
      throw std::runtime_error("Reading failed");
    }
  }
  inputFile.close();
  std::vector<char> vector(&data[0], &data[index]);

  for (const char & elem : vector)
  {
    std::cout << elem;
  }
}

