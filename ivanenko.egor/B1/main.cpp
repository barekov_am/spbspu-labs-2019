#include <iostream>
#include "additions.hpp"

void task1(const char * direction);
void task2(const char * file);
void task3();
void task4(const char * direction, const size_t size);

int main(int argc, char* argv[])
{
  if ((argc < 2) || (argc > 4))
  {
    std::cerr << "Wrong number of arguments";
    return 1;
  }
  try
  {
    const int taskNumber = atoi(argv[1]);
    switch (taskNumber)
    {
      case 1:
      {
        if (argc != 3)
        {
          std::cerr << "Wrong number of arguments";
          return 1;
        }
        if (argv[2] == nullptr)
        {
          std::cerr << "Wrong direction";
          return 1;
        }
        task1(argv[2]);
        break;
      }
      case 2:
      {
        if (argc != 3)
        {
          std::cerr << "Wrong number of arguments";
          return 1;
        }
        if (argv[2] == nullptr)
        {
          std::cerr << "Wrong file name";
          return 1;
        }
        task2(argv[2]);
        break;
      }
      case 3:
      {
        if (argc != 2)
        {
          std::cerr << "Wrong number of arguments";
          return 1;
        }

        task3();
        break;
      }
      case 4:
      {
        if (argc != 4)
        {
          std::cerr << "Wrong number of arguments";
          return 1;
        }
        if ((argv[2] == nullptr) || (argv[3] == nullptr))
        {
          std::cerr << "Wrong task parameters";
          return 1;
        }
        task4(argv[2], atoi(argv[3]));
        break;
      }
      default:
      {
        std::cerr << "Wrong task number";
        return 1;
      }
    }
  }
  catch (const std::exception & ex)
  {
    std::cerr << ex.what();
    return 1;
  }
  return 0;
}

