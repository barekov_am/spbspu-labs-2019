#include <iostream>
#include <algorithm>
#include <iterator>
#include "sequence-statistic.hpp"

void task()
{
  SequenceStatistic sequenceStatistic;
  sequenceStatistic = std::for_each(std::istream_iterator<long long>(std::cin),
                                    std::istream_iterator<long long>(), sequenceStatistic);

  if (!std::cin.eof())
  {
    throw std::ios_base::failure("The error of input number into the sequence!\n");
  }

  if (sequenceStatistic.empty())
  {
    std::cout << "No Data\n";
  }
  else
  {
    std::cout << "Max: " << sequenceStatistic.getMax() << "\n";
    std::cout << "Min: " << sequenceStatistic.getMin() << "\n";
    std::cout << "Mean: " << sequenceStatistic.getMean() << "\n";
    std::cout << "Positive: " << sequenceStatistic.getAmountOfPositive() << "\n";
    std::cout << "Negative: " << sequenceStatistic.getAmountOfNegative() << "\n";
    std::cout << "Odd Sum: " << sequenceStatistic.getOddSum() << "\n";
    std::cout << "Even Sum: " << sequenceStatistic.getEvenSum() << "\n";
    std::cout << "First/Last Equal: " << (sequenceStatistic.equalBorderElements() ? "yes" : "no") << "\n";
  }
}

