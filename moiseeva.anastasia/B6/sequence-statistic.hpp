#ifndef SEQUENCE_STATISTIC_HPP
#define SEQUENCE_STATISTIC_HPP

class SequenceStatistic
{
public:
  SequenceStatistic();
  void operator()(long long nextNumber);

  long long getMax();
  long long getMin();

  double getMean();

  long long getAmountOfPositive();
  long long getAmountOfNegative();

  long long getOddSum();
  long long getEvenSum();

  bool equalBorderElements();
  bool empty();
private:
  long long max_;
  long long min_;
  long long sum_;
  long long amount_;
  long long positives_;
  long long negatives_;
  long long oddSum_;
  long long evenSum_;
  long long first_;
  bool equalBorderElements_;
};

#endif //SEQUENCE_STATISTIC_HPP
