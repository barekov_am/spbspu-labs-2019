#ifndef B2_PARSE_ARGUMENTS_HPP
#define B2_PARSE_ARGUMENTS_HPP

#include <string>
#include <functional>
#include "priorityqueue.cpp"

namespace moiseeva
{
  namespace values
  {
    const std::string INVALID = "<INVALID COMMAND>";
    const std::string EMPTY = "<EMPTY>";

    const std::string ADD = "add";
    const std::string GET = "get";
    const std::string ACCELERATE = "accelerate";

    const std::string HIGH = "high";
    const std::string NORM = "normal";
    const std::string LOW = "low";
  }

  namespace funcs
  {
    template <typename T>
    using command_function = std::function<void(funcs::queueWithPriority<T>&)>;
    command_function<std::string> parse(const std::string& input);
  }
}
#endif
