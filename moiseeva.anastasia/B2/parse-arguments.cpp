#include "parse-arguments.hpp"

#include <sstream>
#include <iostream>

moiseeva::funcs::command_function<std::string> moiseeva::funcs::parse(const std::string& input)
{
  std::stringstream inputStream(input);
  std::string commandString;
  inputStream >> commandString;

  if (commandString == values::ADD)
  {
    std::string priorityString;
    inputStream >> priorityString;

    ElementPriority priority;
    if (priorityString == values::LOW)
    {
      priority = ElementPriority::LOW;
    }
    else if (priorityString == values::NORM)
    {
      priority = ElementPriority::NORMAL;
    }
    else if (priorityString == values::HIGH)
    {
      priority = ElementPriority::HIGH;
    }
    else
    {
      return [](queueWithPriority<std::string>&)
      {
        std::cout << values::INVALID << std::endl;
      };
    }

    inputStream.ignore();
    std::string elementString;
    std::getline(inputStream >> std::ws, elementString);

    if (elementString.empty())
    {
      return [](queueWithPriority<std::string>&)
      {
        std::cout << values::INVALID << std::endl;
      };
    }

    return [priority, elementString](queueWithPriority<std::string>& queue)
    {
      queue.putElementToQueue(elementString, priority);
    };
  }
  else if (commandString == values::GET)
  {
    return [](queueWithPriority<std::string>& queue)
    {
      std::cout << (queue.isEmpty() ? values::EMPTY : queue.getElementFromQueue()) << std::endl;
    };
  }
  else if (commandString == values::ACCELERATE)
  {
    return [](queueWithPriority<std::string>& queue)
    {
      queue.accelerate();
    };
  }
  else
  {
    return [](queueWithPriority<std::string>&)
    {
      std::cout << values::INVALID << std::endl;
    };
  }
}
