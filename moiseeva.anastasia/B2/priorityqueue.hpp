#ifndef PRIORITYQUEUE_HPP
#define PRIORITYQUEUE_HPP

#include <array>
#include <deque>

namespace moiseeva
{
  namespace funcs
  {
    enum class ElementPriority
    {
      LOW = 0,
      NORMAL = 1,
      HIGH = 2
    };

    template<typename T>
    class queueWithPriority
    {
    public:

      queueWithPriority();

      queueWithPriority(const T &element, ElementPriority priority, size_t size = 1);

      ~queueWithPriority() = default;

      void putElementToQueue(const T &element, ElementPriority priority);

      T getElementFromQueue();

      bool isEmpty() const;

      void accelerate();

    private:
      static const size_t PRIORITY_NUMBER = 3;
      typename std::array<std::deque<T>, PRIORITY_NUMBER> queue_;
    };
  }
}

#endif
