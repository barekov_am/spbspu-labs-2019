#include "priorityqueue.hpp"

#include <array>
#include <deque>

template<typename T>
moiseeva::funcs::queueWithPriority<T>::queueWithPriority() = default;

template<typename T>
moiseeva::funcs::queueWithPriority<T>::queueWithPriority(const T &element, ElementPriority priority, size_t size)
{
  for (int i = 0; i < size; i++)
  {
    queue_[static_cast<int>(priority)].push_back(element);
  }
}

template<typename T>
void moiseeva::funcs::queueWithPriority<T>::putElementToQueue(const T &element, ElementPriority priority)
{
  queue_[static_cast<int>(priority)].push_back(element);
}

template<typename T>
T moiseeva::funcs::queueWithPriority<T>::getElementFromQueue()
{
  if (isEmpty())
  {
    throw std::out_of_range("Error:queue is empty");
  }
  ElementPriority priorityToReturn = ElementPriority::LOW;
  if (!queue_[static_cast<int>(ElementPriority::HIGH)].empty())
  {
    priorityToReturn = ElementPriority::HIGH;
  }
  else if (!queue_[static_cast<int>(ElementPriority::NORMAL)].empty())
  {
    priorityToReturn = ElementPriority::NORMAL;
  }
  T elem = queue_[static_cast<int>(priorityToReturn)].front();
  queue_[static_cast<int>(priorityToReturn)].pop_front();
  return elem;
}

template<typename T>
bool moiseeva::funcs::queueWithPriority<T>::isEmpty() const
{
  return queue_[static_cast<int>(ElementPriority::HIGH)].empty()
         && queue_[static_cast<int>(ElementPriority::NORMAL)].empty()
         && queue_[static_cast<int>(ElementPriority::LOW)].empty();
}

template<typename T>
void moiseeva::funcs::queueWithPriority<T>::accelerate()
{
  while (!queue_[static_cast<int>(ElementPriority::LOW)].empty())
  {
    queue_[static_cast<int>(ElementPriority::HIGH)].push_back(
      queue_[static_cast<int>(ElementPriority::LOW)].front()
    );
    queue_[static_cast<int>(ElementPriority::LOW)].pop_front();
  }
}
