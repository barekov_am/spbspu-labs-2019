#include "tasks.hpp"

#include <list>

void moiseeva::tasks::task1()
{
  moiseeva::funcs::queueWithPriority<std::string> queue;
  std::string line;

  while (std::getline(std::cin >> std::ws, line))
  {
    moiseeva::funcs::command_function<std::string> function = funcs::parse(line);
    function(queue);
  }
}

void moiseeva::tasks::task2()
{
  std::list<int> list;

  int number = 0;
  while ((std::cin >> number) && list.size() <= moiseeva::values::MAX_SIZE)
  {
    if (number >= moiseeva::values::MIN_VALUE && number <= moiseeva::values::MAX_VALUE)
    {
      list.push_back(number);
    }
    else
    {
      throw std::invalid_argument("List values must be from 0 to 20");
    }
  }

  if (list.size() > moiseeva::values::MAX_SIZE)
  {
    throw std::invalid_argument("Invalid list size");
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios::failure("Input stream failed");
  }

  funcs::printFrontBack(list.begin(), list.end());
  std::cout << std::endl;
}
