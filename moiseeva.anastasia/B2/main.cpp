#include <iostream>
#include <string>

#include "tasks.hpp"

int main(int argc, char **argv)
{
  try
  {
    if (argc != 2)
    {
      throw std::invalid_argument("Invalid number of parameters");
    }
    if (*argv[1] == '1')
    {
      moiseeva::tasks::task1();
    }
    else if (*argv[1] == '2')
    {
      moiseeva::tasks::task2();
    }
    else
    {
      throw std::invalid_argument("Invalid task number");
    }
  }
  catch (const std::exception &error)
  {
    std::cout << error.what();
    return 1;
  }
  return 0;
}
