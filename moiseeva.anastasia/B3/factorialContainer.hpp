#ifndef FACTORIALCONTAINER_HPP
#define FACTORIALCONTAINER_HPP

#include <cstdlib>
#include <iterator>

namespace moiseeva{

  class FactorialContainer{
  public:
    class Iterator;
    FactorialContainer();
    ~FactorialContainer() = default;
    Iterator begin();
    Iterator end();
  };

  class FactorialContainer::Iterator : public std::iterator<std::bidirectional_iterator_tag, size_t>{
  public:
    Iterator();
    Iterator(size_t number);
    size_t calculateFactorial(size_t) const;
    size_t operator*() const;
    size_t operator->() const;
    Iterator& operator++();
    const Iterator operator++(int);
    Iterator& operator--();
    const Iterator operator--(int);
    bool operator==(const Iterator& rhs) const;
    bool operator!=(const Iterator& rhs) const;
  private:
    size_t number_;
  };

}

#endif
