#include <iostream>
#include <string>
#include <stdexcept>
#include "tasks.hpp"

int main(int argc, char** argv)
{
  try
  {
    if (argc != 2){
      throw std::invalid_argument("Invalid parameters!");
    }

    switch (*argv[1]){
      case '1':
        moiseeva::doFirstPart();
        break;

      case '2':
        moiseeva::doSecondPart();
        break;

      default:
        throw std::invalid_argument("Invalid number of the task!");
    }
  }

  catch (const std::exception & error){
    std::cout << error.what();
    return 1;
  }

  return 0;
}
