#include "factorialContainer.hpp"

const size_t FACTORIAL_BEGIN_INDEX = 1;
const size_t FACTORIAL_AFTER_END_INDEX = 11;

moiseeva::FactorialContainer::FactorialContainer(){
}

moiseeva::FactorialContainer::Iterator moiseeva::FactorialContainer::begin(){
  return moiseeva::FactorialContainer::Iterator(FACTORIAL_BEGIN_INDEX);
}

moiseeva::FactorialContainer::Iterator moiseeva::FactorialContainer::end(){
  return moiseeva::FactorialContainer::Iterator(FACTORIAL_AFTER_END_INDEX);
}

moiseeva::FactorialContainer::Iterator::Iterator():
  Iterator(FACTORIAL_BEGIN_INDEX)
{
}

moiseeva::FactorialContainer::Iterator::Iterator(size_t number):
  number_(number)
{
}

size_t moiseeva::FactorialContainer::Iterator::calculateFactorial(size_t num) const {
  size_t result = 1;
  for (size_t i = 1; i <= num; i++){
    result *= i;
  }
  return result;
}

size_t moiseeva::FactorialContainer::Iterator::operator*() const{
  return calculateFactorial(number_);
}

size_t moiseeva::FactorialContainer::Iterator::operator->() const{
  return calculateFactorial(number_);
}

moiseeva::FactorialContainer::Iterator& moiseeva::FactorialContainer::Iterator::operator++(){
  if (number_ < FACTORIAL_AFTER_END_INDEX){
    number_++;
  }
  return *this;
}

const moiseeva::FactorialContainer::Iterator moiseeva::FactorialContainer::Iterator::operator++(int){
  FactorialContainer::Iterator newit = *this;

  if (number_ < FACTORIAL_AFTER_END_INDEX){
    number_++;
  }
  return newit;
}


moiseeva::FactorialContainer::Iterator& moiseeva::FactorialContainer::Iterator::operator--(){
  if (number_ > FACTORIAL_BEGIN_INDEX){
    number_--;
  }
  return *this;
}

const moiseeva::FactorialContainer::Iterator moiseeva::FactorialContainer::Iterator::operator--(int){
  FactorialContainer::Iterator tmp_iterator = *this;

  if (number_ > FACTORIAL_BEGIN_INDEX){
    number_--;
  }
  return tmp_iterator;
}

bool moiseeva::FactorialContainer::Iterator::operator==(const Iterator& rhs) const{
  return (number_ == rhs.number_);
}

bool moiseeva::FactorialContainer::Iterator::operator!=(const Iterator& rhs) const{
  return (number_ != rhs.number_);
}
