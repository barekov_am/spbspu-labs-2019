#include "phoneBook.hpp"

moiseeva::PhoneBook::PhoneBook(){
  bookmarks_.emplace_back(records_.end(), "current");
  curMark_ = bookmarks_.begin();
}

bool moiseeva::PhoneBook::addRecord(const std::string & name, const std::string & number){
  size_t size = records_.size();
  records_.emplace_back(std::make_pair(name,number));

  if (size < records_.size()){
    if (curMark_->first == records_.end()){
      curMark_->first--;
    }
    return true;
  }
  else
  {
    return false;
  }
}

bool moiseeva::PhoneBook::addMark(std::string& name){
  size_t size = records_.size();
  iter NewMarkIter = curMark_->first;
  bookmarks_.emplace_back(NewMarkIter, name);
  return(size < records_.size());
}

moiseeva::PhoneBook::record_t& moiseeva::PhoneBook::getCurRecord() const{
  return *(curMark_->first);
}

void moiseeva::PhoneBook::goPrev(size_t displ){
  size_t Distance = std::distance(records_.begin(), curMark_->first);
  if (Distance < displ){
    displ = Distance;
  }
  curMark_->first = std::prev(curMark_->first, displ);
}

void moiseeva::PhoneBook::goNext(size_t displ){
  size_t Distance = std::distance(curMark_->first, std::prev(records_.end()));
  if (Distance < displ){
    displ = Distance;
  }
  curMark_->first = std::next(curMark_->first, displ);
}

bool moiseeva::PhoneBook::insertAfter(const std::string & name, const std::string & number){
  size_t size = records_.size();

  if (curMark_->first == records_.end()){
    records_.emplace(curMark_->first, name, number);
    curMark_->first--;
    return true;
  }

  bookmarkIterator nextMark(curMark_);
  (nextMark->first)++;
  records_.emplace(nextMark->first, name, number);

  return(size < records_.size());
}

bool moiseeva::PhoneBook::insertBefore(const std::string & name, const std::string & number){
  size_t size = records_.size();

  if (curMark_->first == records_.end()){
    records_.emplace(curMark_->first, name, number);
    curMark_->first--;
    return true;
  }

  records_.emplace(curMark_->first, name, number);
  return (size < records_.size());
}

size_t moiseeva::PhoneBook::getSize() const{
  return records_.size();
}

bool moiseeva::PhoneBook::makeCurrent(const std::string& name){
  for (bookmarkIterator i = bookmarks_.begin(); i != bookmarks_.end();++i){
    if (i->second == name){
      curMark_ = i;
      return true;
    }
  }

  return false;
}

void moiseeva::PhoneBook::del(){
  if (curMark_->first == records_.end()){
    return;
  }

  iter deleted = curMark_->first;
  iter lastElem = records_.end();
  lastElem--;

  for (bookmarkIterator i = bookmarks_.begin(); i != bookmarks_.end();++i){
    if (i->first == deleted){
      if ((i->first == lastElem) && (records_.size() > 1)){
        --(i->first);
      }
      else
      {
        ++(i->first);
      }
    }
  }

  records_.erase(deleted);
}

