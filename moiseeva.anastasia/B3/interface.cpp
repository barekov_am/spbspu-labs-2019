#include <iostream>
#include <stdexcept>
#include "interface.hpp"

moiseeva::UserInterface::UserInterface(PhoneBook& book_, std::ostream & output):
  book_(book_),
  output_(output)
{}

void moiseeva::UserInterface::chooseAction(std::istream& command){
  std::string buf;

  command >> buf;
  if (buf.empty()){
    return;
  }
  if (buf == "add"){
    add(command);
  }
  else if (buf == "store"){
    store(command);
  }
  else if (buf == "insert"){
    insert(command);
  }
  else if (buf == "delete"){
    del(command);
  }
  else if (buf == "show"){
    show(command);
  }
  else if (buf == "move"){
    move(command);
  }
  else
  {
    output_ << "<INVALID COMMAND>" << std::endl;
  }
}

void moiseeva::UserInterface::add(std::istream& command){
  std::string number;
  std::string name;
  command >> number;
  command.ignore();
  std::getline(command, name);

  if (!HasQuotes(name)){
    return;
  }

  formatName(name);

  if ((name.empty()) || (number.empty())){
    output_ << "<INVALID COMMAND>" <<std::endl;
  }
  else
  {
    book_.addRecord(name, number);
  }
}

void moiseeva::UserInterface::store(std::istream& command){
  std::string markName;
  std::string newMarkName;

  command >> markName >> newMarkName;

  if ((markName.empty()) || (newMarkName.empty())){
    output_ << "<INVALID COMMAND>" <<std::endl;
  }
  else
  {
    bool IsValid = book_.makeCurrent(markName);
    if (!IsValid)
    {
      output_ << "<INVALID BOOKMARK>" <<std::endl;
    }
    else
    {
      book_.addMark(newMarkName);
    }
  }
}

void moiseeva::UserInterface::insert(std::istream& command){
  std::string pos;
  std::string markName;
  std::string name;
  std::string number;
  command >> pos >> markName >> number;
  command.ignore();
  std::getline(command, name);

  if (!HasQuotes(name)){
    return;
  }

  formatName(name);

  if (!book_.makeCurrent(markName)){
    output_ << "<INVALID BOOKMARK>" <<std::endl;
    return;
  }

  if (pos == "before"){
    book_.insertBefore(name, number);
  }
  else if (pos == "after"){
    book_.insertAfter(name, number);
  }
  else
  {
    output_ << "<INVALID COMMAND>";
  }
}

void moiseeva::UserInterface::show(std::istream& command){
  std::string markName;
  command >> markName;

  if (!book_.makeCurrent(markName)){
    output_ << "<INVALID BOOKMARK>" << std::endl;
  }
  else if (book_.getSize() == 0){
    output_ << "<EMPTY>" <<std::endl;
  }
  else
  {
    try{
      output_ << book_.getCurRecord().second << ' ' << book_.getCurRecord().first << std::endl;
    }
    catch (std::logic_error)
    {
      return;
    }
  }
}

void moiseeva::UserInterface::move(std::istream& command){
  std::string markName;
  std::string steps;
  command >> markName >> steps;

  bool isValid = book_.makeCurrent(markName);
  if (!isValid){
    output_ << "<INVALID BOOKMARK>" << std::endl;
    return;
  }

  if (steps == "last"){
    book_.goNext(book_.getSize());
  }
  else if (steps == "first"){
    book_.goPrev(book_.getSize());
  }
  else
  {
    size_t * error = new size_t(0);

    int numStep = 0;

    try{
      numStep = stoi(steps, error);
    }
    catch (std::invalid_argument){
      output_ << "<INVALID STEP>" << std::endl;
      delete error;
      return;
    }

    if (*error != steps.size()){
      output_ << "<INVALID STEP>" << std::endl;
      delete error;
      return;
    }
    delete error;

    if (numStep>0){
      book_.goNext(numStep);
    }
    else if (numStep<0){
      book_.goPrev(abs(numStep));
    }
  }
}

void moiseeva::UserInterface::del(std::istream& command){
  std::string markName;
  command >> markName;

  if (!book_.makeCurrent(markName)){
    output_ << "<INVALID BOOKMARK>" << std::endl;
    return;
  }

  book_.del();
}

bool moiseeva::UserInterface::HasQuotes(const std::string& name){
  if (name[0]!='"' || name[name.size()-1]!='"'){
    output_ << "<INVALID COMMAND>" << std::endl;
    return false;
  }

  return true;
}

void moiseeva::UserInterface::formatName(std::string& name){
  std::string::iterator I = name.begin();
  while (I != name.end()){
    if ((*I == '\\') && ((*(I+1) == '\\') || (*(I+1) == '"'))){
      I = name.erase(I) + 1;
    }
    else if (*I == '"'){
      I = name.erase(I);
    }
    else
    {
      ++I;
    }
  }
}
