#ifndef PARTS_HPP
#define PARTS_HPP

namespace moiseeva {
  int doFirstPart();
  int doSecondPart();
}

#endif


