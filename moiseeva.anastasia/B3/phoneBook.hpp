#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP
#include <string>
#include <utility>
#include <list>
#include <iterator>

namespace moiseeva {

  class PhoneBook{
  public:

    typedef std::pair<std::string, std::string> record_t;
    typedef std::list<record_t>::iterator iter;
    typedef std::list<std::pair<iter, std::string>>::iterator bookmarkIterator;

    PhoneBook();
    bool addRecord(const std::string & name, const std::string &number);
    record_t& getCurRecord() const;
    bool makeCurrent(const std::string&);
    void goPrev(size_t = 1);
    void goNext(size_t = 1);
    bool insertAfter(const std::string & name, const std::string & number);
    bool insertBefore(const std::string & name, const std::string & number);
    void del();
    size_t getSize() const;
    bool addMark(std::string& name);

  private:
    std::list<record_t> records_;
    std::list<std::pair<iter, std::string>> bookmarks_;
    bookmarkIterator curMark_;
  };

}

#endif
