#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(A3TestTriangle)

  const double ERROR = 0.01;

  BOOST_AUTO_TEST_CASE(immutabilityAfterMovingTriangle)
  {
    moiseeva::Triangle movableTriangle({4.0, 4.0}, {8.0, 8.0}, {7.0, 5.0});
    const moiseeva::rectangle_t rectangleBefore = movableTriangle.getFrameRect();
    const double areaBefore = movableTriangle.getArea();

    movableTriangle.move({1.5, 1.5});
    BOOST_CHECK_CLOSE(rectangleBefore.width, movableTriangle.getFrameRect().width, ERROR);
    BOOST_CHECK_CLOSE(rectangleBefore.height, movableTriangle.getFrameRect().height, ERROR);
    BOOST_CHECK_CLOSE(areaBefore, movableTriangle.getArea(), ERROR);

    movableTriangle.move(7.0, 1.0);
    BOOST_CHECK_CLOSE(rectangleBefore.width, movableTriangle.getFrameRect().width, ERROR);
    BOOST_CHECK_CLOSE(rectangleBefore.height, movableTriangle.getFrameRect().height,ERROR);
    BOOST_CHECK_CLOSE(areaBefore, movableTriangle.getArea(), ERROR);
  }

  BOOST_AUTO_TEST_CASE(scalingTriangle)
  {
    moiseeva::Triangle scalableTriangle({4.0, 4.0}, {8.0, 8.0}, {7.0, 5.0});
    const double areaBefore = scalableTriangle.getArea();
    const double coefficient = 3.0;
    scalableTriangle.scale(coefficient);
    BOOST_CHECK_CLOSE(scalableTriangle.getArea(), coefficient * coefficient * areaBefore, ERROR);
  }

  BOOST_AUTO_TEST_CASE(invalidTriangleValues)
  {
    BOOST_CHECK_THROW(moiseeva::Triangle triangleTest({0, 0}, {0, 0}, {10, 10}), std::invalid_argument);
    BOOST_CHECK_THROW(moiseeva::Triangle triangleTest({0, 0}, {10, 10}, {10, 10}), std::invalid_argument);
    BOOST_CHECK_THROW(moiseeva::Triangle triangleTest({0, 0}, {0, 0}, {0, 0}), std::invalid_argument);

    moiseeva::Triangle triangleTest({0, 0}, {10, 10}, {20, -10});
    BOOST_CHECK_THROW(triangleTest.scale(-4), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(triangleRotation)
  {
    moiseeva::Triangle triangle({0.0, 0.0}, {8.0, 4.0}, {2.0, 6.0});
    double triangleAreaBeforeRotating = triangle.getArea();

    double angle = -90.00;
    BOOST_CHECK_NO_THROW(triangle.rotate(angle));

    angle = 90.00;
    BOOST_CHECK_NO_THROW(triangle.rotate(angle));

    angle = -45.5;
    triangle.rotate(angle);
    BOOST_CHECK_CLOSE(triangle.getArea(), triangleAreaBeforeRotating, ERROR);

    angle = 15.35;
    triangle.rotate(angle);
    BOOST_CHECK_CLOSE(triangle.getArea(), triangleAreaBeforeRotating, ERROR);

    angle = 361.0;
    triangle.rotate(angle);
    BOOST_CHECK_CLOSE(triangle.getArea(), triangleAreaBeforeRotating, ERROR);

    angle = -1000.1;
    triangle.rotate(angle);
    BOOST_CHECK_CLOSE(triangle.getArea(), triangleAreaBeforeRotating, ERROR);
  }


BOOST_AUTO_TEST_SUITE_END()
