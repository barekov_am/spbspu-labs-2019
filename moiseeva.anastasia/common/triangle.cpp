#include "triangle.hpp"
#include <stdexcept>
#include <cmath>
#include <algorithm>
#include <iostream>

moiseeva::Triangle::Triangle(const point_t &A, const point_t &B, const point_t &C) :
  pos_({(A.x + B.x + C.x) / 3, (A.y + B.y + C.y) / 3}),
  point1_(A),
  point2_(B),
  point3_(C),
  angle_(0.0)
{
  if (getArea() == 0)
  {
    throw std::invalid_argument("You can't create triangle with this points");
  }
}

double moiseeva::Triangle::getArea() const
{
  return (std::fabs((point1_.x - point3_.x) * (point2_.y - point3_.y)
      - (point2_.x - point3_.x) * (point1_.y - point3_.y)) / 2);
}

moiseeva::rectangle_t moiseeva::Triangle::getFrameRect() const
{
  const double maxX = std::max(std::max(point1_.x, point2_.x), point3_.x);
  const double  minX = std::min(std::min(point1_.x, point2_.x), point3_.x);

  const double maxY = std::max(std::max(point1_.y, point2_.y), point3_.y);
  const double  minY = std::min(std::min(point1_.y, point2_.y), point3_.y);

  const double width = maxX - minX;
  const double height = maxY - minY;
  const point_t pos = {minX + width / 2, minY + height / 2};

  return {width, height, pos};
}

void moiseeva::Triangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;

  point1_.x += dx;
  point1_.y += dy;

  point2_.x += dx;
  point2_.y += dy;

  point3_.x += dx;
  point3_.y += dy;
}

void moiseeva::Triangle::move(const point_t &newPos)
{
  const double dx = newPos.x - pos_.x;
  const double dy = newPos.y - pos_.y;

  move(dx, dy);
}

void moiseeva::Triangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient should be positive");
  }
  point1_.x = pos_.x + (point1_.x - pos_.x) * coefficient;
  point1_.y = pos_.y + (point1_.y - pos_.y) * coefficient;

  point2_.x = pos_.x + (point2_.x - pos_.x) * coefficient;
  point2_.y = pos_.y + (point2_.y - pos_.y) * coefficient;

  point3_.x = pos_.x + (point3_.x - pos_.x) * coefficient;
  point3_.y = pos_.y + (point3_.y - pos_.y) * coefficient;
}

void moiseeva::Triangle::rotate(double angle) {

  while (angle >= 360.0) {
    angle -= 360.0;
  }
  angle_ += angle;

  point1_ = rotatePoint(point1_, pos_, angle_);
  point2_ = rotatePoint(point2_, pos_, angle_);
  point3_ = rotatePoint(point3_, pos_, angle_);
}

moiseeva::point_t moiseeva::Triangle::rotatePoint(point_t point, point_t pos_, double angle)
{
  const double sinus = sin(angle * M_PI / 180);
  const double cosinus = cos(angle * M_PI / 180);
  double x = point.x;
  double y = point.y;

  x = pos_.x + (x - pos_.x) * cosinus - (y - pos_.y) * sinus;
  point.y = pos_.y + (point.y - pos_.y) * cosinus + (point.x - pos_.x) * sinus;
  point.x = x;
  return (point);
}
