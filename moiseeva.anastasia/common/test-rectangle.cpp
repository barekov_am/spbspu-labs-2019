#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include <stdexcept>

#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(A4TestRectangle)

  const double ERROR = 0.01;

  BOOST_AUTO_TEST_CASE(immutabilityAfterMovingRectangle)
  {
    moiseeva::Rectangle movableRectangle({4.0, 4.0}, 1.7, 5.4);
    const moiseeva::rectangle_t rectangleBefore = movableRectangle.getFrameRect();
    const double areaBefore = movableRectangle.getArea();

    movableRectangle.move({1.5, 1.5});
    BOOST_CHECK_CLOSE(rectangleBefore.width, movableRectangle.getFrameRect().width, ERROR);
    BOOST_CHECK_CLOSE(rectangleBefore.height, movableRectangle.getFrameRect().height, ERROR);
    BOOST_CHECK_CLOSE(areaBefore, movableRectangle.getArea(), ERROR);

    movableRectangle.move(7.0, -1.0);
    BOOST_CHECK_CLOSE(rectangleBefore.width, movableRectangle.getFrameRect().width, ERROR);
    BOOST_CHECK_CLOSE(rectangleBefore.height, movableRectangle.getFrameRect().height, ERROR);
    BOOST_CHECK_CLOSE(areaBefore, movableRectangle.getArea(), ERROR);
  }

  BOOST_AUTO_TEST_CASE(scalingRectangle)
  {
    moiseeva::Rectangle scalableRectangle({5.0, 5.0}, 2.0, 2.0);
    const double areaBefore = scalableRectangle.getArea();
    const double coefficient = 3.0;
    scalableRectangle.scale(coefficient);
    BOOST_CHECK_CLOSE(scalableRectangle.getArea(), coefficient * coefficient * areaBefore, ERROR);
  }

  BOOST_AUTO_TEST_CASE(invalidRectangleValues)
  {
    BOOST_CHECK_THROW(moiseeva::Rectangle({4.5, 1.0}, -2.0, 5.0), std::invalid_argument);
    BOOST_CHECK_THROW(moiseeva::Rectangle({2.0, 4.1}, -1.0, -3.0), std::invalid_argument);

    moiseeva::Rectangle wrongRectangle1({2.0, 2.0}, 1.0, 1.0);
    BOOST_CHECK_THROW(wrongRectangle1.scale(-1.0), std::invalid_argument);

    moiseeva::Rectangle wrongRectangle2({1.0, 1.0}, 2.0, 2.0);
    BOOST_CHECK_THROW(wrongRectangle2.scale(-2.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(rectangleRotation)
  {
    moiseeva::Rectangle testRectangle({0, 0}, 10, 10);
    double areaBefore = testRectangle.getArea();

    double angle = -98.76;
    BOOST_CHECK_NO_THROW(testRectangle.rotate(angle));

    angle = 55.55;
    BOOST_CHECK_NO_THROW(testRectangle.rotate(angle));

    angle = -21.5;
    testRectangle.rotate(angle);
    BOOST_CHECK_CLOSE(testRectangle.getArea(), areaBefore, ERROR);

    angle = 12.34;
    testRectangle.rotate(angle);
    BOOST_CHECK_CLOSE(testRectangle.getArea(), areaBefore, ERROR);

    angle = 361.0;
    testRectangle.rotate(angle);
    BOOST_CHECK_CLOSE(testRectangle.getArea(), areaBefore, ERROR);

    angle = -1000.1;
    testRectangle.rotate(angle);
    BOOST_CHECK_CLOSE(testRectangle.getArea(),areaBefore, ERROR);
  }


BOOST_AUTO_TEST_SUITE_END()
