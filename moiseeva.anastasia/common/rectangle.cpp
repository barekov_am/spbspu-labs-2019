#include <stdexcept>
#include <cmath>

#include "rectangle.hpp"

moiseeva::Rectangle::Rectangle(const point_t &dot, double width, double height):
  center_(dot),
  width_(width),
  height_(height),
  angle_(0)
{
  if (width_ < 0.0)
  {
    throw std::invalid_argument("Rectangle width cannot be less than zero.");
  }
  if (height_ < 0.0)
  {
    throw std::invalid_argument("Rectangle height cannot be less than zero.");
  }
}

moiseeva::Rectangle::Rectangle(const point_t &dot, double width, double height, double angle):
  Rectangle(dot, width, height)
{
  rotate(angle);
}

double moiseeva::Rectangle::getArea() const
{
  return width_ * height_;
}

moiseeva::rectangle_t moiseeva::Rectangle::getFrameRect() const
{
  const double sinus = sin(angle_ * M_PI / 180);
  const double cosine = cos(angle_ * M_PI / 180);
  const double width = height_ * fabs(sinus) + width_ * fabs(cosine);
  const double height = height_ * fabs(cosine) + width_ * fabs(sinus);

  return {width, height, center_};
}

void moiseeva::Rectangle::move(const point_t &point)
{
  center_ = point;
}

void moiseeva::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void moiseeva::Rectangle::scale(double coefficient)
{
  if (coefficient < 0.0)
  {
    throw std::invalid_argument("Rectangle scale coefficient cannot be less than zero.");
  }
  else
  {
    width_ *= coefficient;
    height_ *= coefficient;
  }
}

void moiseeva::Rectangle::rotate(double angle)
{
  while (angle >= 360.0)
  {
    angle -= 360.0;
  }
  angle_ += angle;
}
