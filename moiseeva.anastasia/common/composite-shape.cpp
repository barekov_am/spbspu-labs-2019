#include <stdexcept>
#include <cmath>
#include <algorithm>

#include "composite-shape.hpp"



moiseeva::CompositeShape::CompositeShape() :
  count_(0),
  shapes_(nullptr)
{
}

moiseeva::CompositeShape::CompositeShape(const CompositeShape &compShape) :
  count_(compShape.count_),
  shapes_(std::make_unique<std::shared_ptr<moiseeva::Shape>[]>(compShape.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i] = compShape.shapes_[i];
  }
}

moiseeva::CompositeShape::CompositeShape(CompositeShape &&compShape) :
  count_(compShape.count_),
  shapes_(std::move(compShape.shapes_))
{
  compShape.count_ = 0;
}

moiseeva::CompositeShape::CompositeShape(const moiseeva::Shape::ptr_shape &firstShape):
  count_(1),
  shapes_(std::make_unique<std::shared_ptr<moiseeva::Shape>[]>(1))
{
  if (firstShape == nullptr)
  {
    throw std::invalid_argument("You can't create composite shape with nullptr");
  }
  shapes_[0] = firstShape;
}

moiseeva::CompositeShape &moiseeva::CompositeShape::operator =(const CompositeShape &compShape)
{
  if (this != &compShape)
  {
    count_ = compShape.count_;
    shapes_ = std::make_unique<moiseeva::Shape::ptr_shape[]>(compShape.count_);
    for (size_t i = 0; i < compShape.count_; i++)
    {
      shapes_[i] = compShape.shapes_[i];
    }
  }
  return *this;
}

moiseeva::CompositeShape &moiseeva::CompositeShape::operator =(moiseeva::CompositeShape &&compShape)
{
  if (this != &compShape)
  {
    count_ = compShape.count_;
    shapes_ = std::move(compShape.shapes_);
  }
  return *this;
}

moiseeva::Shape::ptr_shape moiseeva::CompositeShape::operator[](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }
  return shapes_[index];
}


double moiseeva::CompositeShape::getArea() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  double area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

moiseeva::rectangle_t moiseeva::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  rectangle_t currRectangle = shapes_[0]->getFrameRect();
  double minX = currRectangle.pos.x - currRectangle.width / 2;
  double maxX = currRectangle.pos.x + currRectangle.width / 2;
  double minY = currRectangle.pos.y - currRectangle.height / 2;
  double maxY = currRectangle.pos.y + currRectangle.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    currRectangle = shapes_[i]->getFrameRect();

    double frameRectangle = currRectangle.pos.x - currRectangle.width / 2;
    minX = std::min(frameRectangle, minX);
    frameRectangle = currRectangle.pos.y - currRectangle.height / 2;
    minY = std::min(frameRectangle, minY);
    frameRectangle = currRectangle.pos.x + currRectangle.width / 2;
    maxX = std::max(frameRectangle, maxX);
    frameRectangle = currRectangle.pos.y + currRectangle.height / 2;
    maxY = std::max(frameRectangle, maxY);
  }
  return {(maxX - minX), (maxY - minY), {(minX + maxX) / 2, (minY + maxY) / 2}};
}

void moiseeva::CompositeShape::move(const point_t& point)
{
  double dx = point.x - getFrameRect().pos.x;
  double dy = point.y - getFrameRect().pos.y;
  move(dx, dy);
}

void moiseeva::CompositeShape::move(double dx, double dy)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void moiseeva::CompositeShape::scale(double scale)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  if (scale <= 0)
  {
    throw std::invalid_argument("Scale must be > 0");
  }

  rectangle_t frameRect = getFrameRect();
  for (size_t i = 0; i < count_; i++)
  {
    double dx = shapes_[i]->getFrameRect().pos.x - frameRect.pos.x;
    double dy = shapes_[i]->getFrameRect().pos.y - frameRect.pos.y;
    shapes_[i]->move({frameRect.pos.x + dx * scale, frameRect.pos.y + dy * scale});
    shapes_[i]->scale(scale);
  }
}

void moiseeva::CompositeShape::add(const moiseeva::Shape::ptr_shape &newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Adding shape should not be nullptr");
  }

  moiseeva::Shape::array_ptr newShapeList(std::make_unique<std::shared_ptr<moiseeva::Shape>[]>(count_ + 1));
  for (size_t i = 0; i < count_; ++i)
  {
    newShapeList[i] = shapes_[i];
  }
  newShapeList[count_] = newShape;
  count_++;
  shapes_.swap(newShapeList);
}

void moiseeva::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }
  if (count_ == 0)
  {
    throw std::out_of_range("Composite shape is already empty");
  }

  for (size_t i = index; i < count_ - 1; ++i)
  {
    shapes_[i] = shapes_[i + 1];
  }
  shapes_[count_ - 1] = nullptr;
  count_--;
}

size_t moiseeva::CompositeShape::getSize() const
{
  return count_;
}

void moiseeva::CompositeShape::rotate(double angle)
{
  const moiseeva::point_t center = getFrameRect().pos;
  const double sinus = std::sin(angle * M_PI / 180);
  const double cosine = std::cos(angle * M_PI / 180);
  for (size_t i = 0; i < count_; ++i)
  {
    const moiseeva::point_t shapeCenter = shapes_[i]->getFrameRect().pos;
    const double dx = shapeCenter.x - center.x;
    const double dy = shapeCenter.y - center.y;
    shapes_[i]->move(dx * (cosine - 1) - dy * sinus, dx * sinus + dy * (cosine - 1));
    shapes_[i]->rotate(angle);
  }
}
