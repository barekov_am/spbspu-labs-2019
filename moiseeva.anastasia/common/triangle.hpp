#ifndef A2_TRIANGLE_HPP
#define A2_TRIANGLE_HPP

#include "shape.hpp"

namespace moiseeva
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &A, const point_t &B, const point_t &C);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &pos) override;
    void scale(double coefficient) override;
    void rotate(double angle) override;

  private:
    point_t rotatePoint(point_t point, point_t pos, double angle);
    point_t pos_;
    point_t point1_;
    point_t point2_;
    point_t point3_;
    double angle_;
  };
}
#endif //A2_TRIANGLE_HPP
