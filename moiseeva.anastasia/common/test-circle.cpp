#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include <stdexcept>

#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(A4TestCircle)

  const double ERROR = 0.01;

  BOOST_AUTO_TEST_CASE(immutabilityAfterMovingCircle)
  {
    moiseeva::Circle movableCircle({1.0, 2.0}, 5.0);
    const moiseeva::rectangle_t rectangleBefore = movableCircle.getFrameRect();
    const double areaBefore = movableCircle.getArea();

    movableCircle.move(-1.0, 9.7);
    BOOST_CHECK_CLOSE(rectangleBefore.width, movableCircle.getFrameRect().width, ERROR);
    BOOST_CHECK_CLOSE(rectangleBefore.height, movableCircle.getFrameRect().height, ERROR);
    BOOST_CHECK_CLOSE(areaBefore, movableCircle.getArea(), ERROR);

    movableCircle.move({2.5, 3.2});
    BOOST_CHECK_CLOSE(rectangleBefore.width, movableCircle.getFrameRect().width, ERROR);
    BOOST_CHECK_CLOSE(rectangleBefore.height, movableCircle.getFrameRect().height, ERROR);
    BOOST_CHECK_CLOSE(areaBefore, movableCircle.getArea(), ERROR);
  }

  BOOST_AUTO_TEST_CASE(scalingCircle)
  {
    moiseeva::Circle scalableCircle({1.0, 1.0}, 10.0);
    const double areaBefore = scalableCircle.getArea();
    const double coefficient = 5.2;
    scalableCircle.scale(coefficient);
    BOOST_CHECK_CLOSE(scalableCircle.getArea(), coefficient * coefficient * areaBefore, ERROR);
  }

  BOOST_AUTO_TEST_CASE(invalidCircleValues)
  {
    BOOST_CHECK_THROW(moiseeva::Circle({2.0, 3.5}, -2.0), std::invalid_argument);
    BOOST_CHECK_THROW(moiseeva::Circle({1.0, 2.5}, -1.0), std::invalid_argument);

    moiseeva::Circle wrongCircle1({2.0, 2.0}, 1.0);
    BOOST_CHECK_THROW(wrongCircle1.scale(-2.0), std::invalid_argument);

    moiseeva::Circle wrongCircle2({1.0, 1.0}, 2.0);
    BOOST_CHECK_THROW(wrongCircle1.scale(-1.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(circleRotation)
  {
    moiseeva::Circle testCircle({2, 2}, 5);
    double areaBefore = testCircle.getArea();

    double angle = -64.55;
    BOOST_CHECK_NO_THROW(testCircle.rotate(angle));

    angle = 77.75;
    BOOST_CHECK_NO_THROW(testCircle.rotate(angle));

    angle = -89.5;
    testCircle.rotate(angle);
    BOOST_CHECK_CLOSE(testCircle.getArea(), areaBefore, ERROR);

    angle = 64.34;
    testCircle.rotate(angle);
    BOOST_CHECK_CLOSE(testCircle.getArea(), areaBefore, ERROR);

    angle = 400.0;
    testCircle.rotate(angle);
    BOOST_CHECK_CLOSE(testCircle.getArea(), areaBefore, ERROR);

    angle = -1000.1;
    testCircle.rotate(angle);
    BOOST_CHECK_CLOSE(testCircle.getArea(),areaBefore, ERROR);
  }


BOOST_AUTO_TEST_SUITE_END()
