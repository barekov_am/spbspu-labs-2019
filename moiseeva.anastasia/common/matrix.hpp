#ifndef MATRIX_HPP_INCLUDED
#define MATRIX_HPP_INCLUDED

#include <memory>

#include "shape.hpp"

namespace moiseeva
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &rhs);
    Matrix(Matrix &&displaceMatrix);
    ~Matrix() = default;

    Matrix& operator =(const Matrix &someMatrix);
    Matrix& operator =(Matrix &&someMatrix);

    moiseeva::Shape::array_ptr operator [](size_t index) const;
    bool operator ==(const Matrix& someMatrix) const;
    bool operator !=(const Matrix& someMatrix) const;

    void add(moiseeva::Shape::ptr_shape shape, size_t row, size_t column);

    size_t getColumns() const;
    size_t getRows() const;

  private:
    size_t rows_;
    size_t columns_;
    moiseeva::Shape::array_ptr matrixList_;
  };
}

#endif
