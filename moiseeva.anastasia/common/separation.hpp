#ifndef SEPARATION_HPP_INCLUDED
#define SEPARATION_HPP_INCLUDED

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace moiseeva
{
  bool cross(const Shape &firstShape, const Shape &secondShape);
  Matrix layer(const CompositeShape&);
}

#endif
