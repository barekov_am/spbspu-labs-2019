#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace moiseeva
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t &dot, double width, double height);
    Rectangle(const point_t &dot, double width, double height, double angle);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &point) override;
    void scale(double coefficient) override;
    void rotate(double angle) override;

  private:
    point_t center_;
    double width_;
    double height_;
    double angle_;
  };
}

#endif
