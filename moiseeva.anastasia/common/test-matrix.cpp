#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>

#include "composite-shape.hpp"
#include "separation.hpp"
#include "matrix.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(matrixTestSuite)

  BOOST_AUTO_TEST_CASE(copyAndMoveConstructor)
  {
    moiseeva::Rectangle r1({-1.0, -1.0}, 4.0, 4.0);
    moiseeva::Circle c1({3.0, 2.0}, 1.0);
    moiseeva::CompositeShape testShapes(std::make_shared<moiseeva::Rectangle>(r1));
    testShapes.add(std::make_shared<moiseeva::Circle>(c1));

    moiseeva::Matrix testMatrix = moiseeva::layer(testShapes);

    BOOST_CHECK_NO_THROW(moiseeva::Matrix testMatrix2(testMatrix));
    BOOST_CHECK_NO_THROW(moiseeva::Matrix testMatrix3(std::move(testMatrix)));
  }

  BOOST_AUTO_TEST_CASE(operatorTests)
  {
    moiseeva::Rectangle r1({-1.0, -1.0}, 4.0, 4.0);
    moiseeva::Circle c1({3.0, 2.0}, 1.0);
    moiseeva::CompositeShape testShapes1(std::make_shared<moiseeva::Rectangle>(r1));
    testShapes1.add(std::make_shared<moiseeva::Circle>(c1));

    moiseeva::Matrix testMatrix1 = moiseeva::layer(testShapes1);

    BOOST_CHECK_THROW(testMatrix1[100], std::out_of_range);
    BOOST_CHECK_THROW(testMatrix1[-1], std::out_of_range);

    BOOST_CHECK_NO_THROW(moiseeva::Matrix testMatrix2 = testMatrix1);
    BOOST_CHECK_NO_THROW(moiseeva::Matrix testMatrix3 = std::move(testMatrix1));

    moiseeva::Rectangle r2({-1.0, -1.0}, 4.0, 4.0);
    moiseeva::CompositeShape testShapes2(std::make_shared<moiseeva::Rectangle>(r2));

    moiseeva::Matrix testMatrix4 = moiseeva::layer(testShapes2);
    moiseeva::Matrix testMatrix5 = moiseeva::layer(testShapes2);

    BOOST_CHECK(testMatrix4 == testMatrix5);

    BOOST_CHECK(testMatrix4 != testMatrix1);
    BOOST_CHECK(testMatrix5 != testMatrix1);
  }

BOOST_AUTO_TEST_SUITE_END()
