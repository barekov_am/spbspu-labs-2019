#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include <memory>

#include "shape.hpp"

namespace moiseeva
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &compShape);
    CompositeShape(CompositeShape &&compShape);
    CompositeShape(const moiseeva::Shape::ptr_shape &firstShape);
    ~CompositeShape() = default;


    CompositeShape& operator =(const CompositeShape &compShape);
    CompositeShape& operator =(CompositeShape &&compShape);

    moiseeva::Shape::ptr_shape operator [](std::size_t index) const;


    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& point) override;
    void move(double dx, double dy) override;
    void scale(double scale) override;

    void add(const moiseeva::Shape::ptr_shape &newShape);
    void remove(std::size_t index);
    std::size_t getSize() const;
    void rotate(double angle);

  private:
    std::size_t count_;
    moiseeva::Shape::array_ptr shapes_;

  };

}
#endif // COMPOSITESHAPE_HPP
