#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "triangle.hpp"

int main()
{
  moiseeva::Circle c1({3.0, 1.0}, 5.0);
  std::cout << "Circle area " << c1.getArea() << std::endl;
  c1.scale(2.0);
  std::cout << "New circle area " << c1.getArea() << std::endl;

  moiseeva::Circle c2({1.0, 2.0}, 4.0);
  c2.move({3.0, 5.0});

  moiseeva::Shape* figureP = &c2;
  moiseeva::rectangle_t tempRect = figureP->getFrameRect();
  std::cout << "X = " << tempRect.pos.x << " Y = " << tempRect.pos.y << std::endl;
  std::cout << "Width = " << tempRect.width << " Height = " << tempRect.height << std::endl;
  figureP->move(1.0, 3.5);
  tempRect = figureP->getFrameRect();
  std::cout << "X after moving " << tempRect.pos.x << " Y after moving " << tempRect.pos.y << std::endl;

  moiseeva::Rectangle r1({2.0, 2.0}, 6.0, 5.0);
  std::cout << "Rectangle area " << r1.getArea() << std::endl;
  r1.scale(2.0);
  std::cout << "New rectangle area " << r1.getArea() << std::endl;

  moiseeva::Rectangle r2({3.0, 3.0}, 7.0, 6.0);
  r2.move(1.0, 2.0);

  figureP = &r2;
  tempRect = figureP->getFrameRect();
  std::cout << "X = " << tempRect.pos.x << " Y = " << tempRect.pos.y << std::endl;
  std::cout << "Width = " << tempRect.width << " Height = " << tempRect.height << std::endl;
  figureP->move(1.0, 3.5);
  tempRect = figureP->getFrameRect();
  std::cout << "X after moving " << tempRect.pos.x << " Y after moving " << tempRect.pos.y << std::endl;

  moiseeva::Triangle t1({2.0, 2.0}, {3.0, 7.0}, {5.0, 1.0});
  std::cout << "Triangle area " << t1.getArea() << std::endl;
  t1.scale(2.0);
  std::cout << "New triangle area " << t1.getArea() << std::endl;

  figureP = &t1;
  tempRect = figureP->getFrameRect();
  std::cout << "X = " << tempRect.pos.x << " Y = " << tempRect.pos.y << std::endl;
  std::cout << "Width = " << tempRect.width << " Height = " << tempRect.height << std::endl;
  figureP->move(1.0, 3.5);
  tempRect = figureP->getFrameRect();
  std::cout << "X after moving " << tempRect.pos.x << " Y after moving " << tempRect.pos.y << std::endl;

  moiseeva::CompositeShape shapes1;
  shapes1.add(std::make_shared<moiseeva::Rectangle>(r1));
  std::cout << "Area of first composite shape: " << shapes1.getArea() << '\n';
  shapes1.add(std::make_shared<moiseeva::Circle>(c1));
  std::cout << "Area of first composite shape after adding new shape: " << shapes1.getArea() << '\n';
  std::cout << "Center of first composite shape: \n";
  std::cout << "x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';
  shapes1.move(7.0,7.0);
  std::cout << "Center of first composite shape after moving: \n";
  std::cout << "x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';
  shapes1.scale(3.0);
  std::cout << "Center of first composite shape after scaling: \n";
  std::cout << "x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';

  moiseeva::CompositeShape shapes2(std::make_shared<moiseeva::Rectangle>(r2));
  std::cout << "Area of second composite shape: " << shapes2.getArea() << '\n';
  shapes2.add(std::make_shared<moiseeva::Circle>(c2));
  std::cout << "Area of second composite shape after adding new shape: " << shapes2.getArea() << '\n';
  std::cout << "Count after adding new shape: " << shapes2.getSize() << '\n';
  shapes2.remove(1);
  std::cout << "Count after deleting second shape: " << shapes2.getSize() << '\n';
  std::cout << "Area of second composite shape after deleting second shape: " << shapes2.getArea() << '\n';
  std::cout << "Center of second composite shape: \n";
  std::cout << "x = " << shapes2.getFrameRect().pos.x << ", y = " << shapes2.getFrameRect().pos.y << '\n';
  shapes2.move({7.0,7.0});
  std::cout << "Center of second composite shape after moving: \n";
  std::cout << "x = " << shapes2.getFrameRect().pos.x << ", y = " << shapes2.getFrameRect().pos.y << '\n';
  shapes2.scale(3.0);
  std::cout << "Center of second composite shape after scaling: \n";
  std::cout << "x = " << shapes2.getFrameRect().pos.x << ", y = " << shapes2.getFrameRect().pos.y << '\n';

  return 0;
}

