#include "tasks.hpp"

void moiseeva::tasks::itemThree(std::istream &istream)
{
  int spaceSwitch = 1;
  std::vector<int> vecInt;

  int number = 0;
  bool foundZero = 0;
  while (!(istream >> number).eof())
  {
    if (istream.fail() || istream.bad())
    {
      throw std::ios_base::failure("Wrong characters.");
    }
    if (number != 0)
    {
      vecInt.push_back(number);
    }
    else
    {
      foundZero = 1;
      break;
    }
  }

  if (!vecInt.empty() && foundZero)
  {
    std::vector<int>::iterator it;
    it = vecInt.end() - 1;

    if (*it == 1)
    {
      for (it = vecInt.begin(); it != vecInt.end();)
      {
        if (!((*it) % 2))
        {
          it = vecInt.erase(it);
        }
        else
        {
          ++it;
        }
      }
    }
    else if (*it == 2)
    {
      for (it = vecInt.begin(); it != vecInt.end(); ++it)
      {
        if (!((*it) % 3))
        {
          for(int i=0; i<3; i++)
          {
            it = vecInt.emplace(it + 1, 1);
          }
        }
      }
    }

    moiseeva::details::VectorPrinter<int>(vecInt, spaceSwitch);
  }
  else if (!vecInt.empty() && !foundZero)
  {
    throw std::invalid_argument("Missing zero.");
  }
  else if (vecInt.empty())
  {
    return;
  }

}

