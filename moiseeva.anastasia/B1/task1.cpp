#include "tasks.hpp"

void moiseeva::tasks::itemOne(const char* sortParameter, std::istream &istream)
{
  int spaceSwitch = 1;

  std::vector<int> vecInt, vecIntPrimary;

  int number;
  while (istream >> number)
  {
    vecInt.insert(vecInt.end(), number);
  }
  if (!istream.eof())
  {
    throw std::ios_base::failure("Wrong characters.");
  }

  vecIntPrimary = vecInt;

  if (!vecInt.empty())
  {
    bool ascending;

    if (sortParameter == moiseeva::details::ASCENDING)
    {
      ascending = true;
    }
    else if (sortParameter == moiseeva::details::DESCENDING)
    {
      ascending = false;
    }
    else
    {
      throw std::invalid_argument("Wrong sorting parameter.");
    }

    moiseeva::details::sortVecT(vecInt, moiseeva::details::getByBrackets, ascending);
    moiseeva::details::VectorPrinter<int>(vecInt, spaceSwitch);
    std::cout << std::endl;

    vecInt = vecIntPrimary;

    moiseeva::details::sortVecT(vecInt, moiseeva::details::getByAt, ascending);
    moiseeva::details::VectorPrinter<int>(vecInt, spaceSwitch);
    std::cout << std::endl;

    vecInt = vecIntPrimary;

    std::vector<int>::iterator it1, it2;

    moiseeva::details::sortVecT(vecInt, moiseeva::details::getByIterator, ascending);
    moiseeva::details::VectorPrinter<int>(vecInt, spaceSwitch);
  }
  else
  {
    if ((sortParameter == moiseeva::details::ASCENDING) || (sortParameter == moiseeva::details::DESCENDING))
    {
      return;
    }
    else
    {
      throw std::invalid_argument("Wrong sorting parameter.");
    }
  }
}
