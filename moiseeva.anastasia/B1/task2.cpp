#include "tasks.hpp"
#include <memory>
#include <vector>

const std::size_t initialSize = 1024;

void moiseeva::tasks::itemTwo(char* fileName)
{
  int spaceSwitch = 0;

  std::ifstream fin(fileName);

  if (!fin)
  {
    throw std::invalid_argument("File does not exist.");
  }

  std::size_t size = initialSize;

  std::unique_ptr<char[], decltype(&free)> array(static_cast<char*>(malloc(size)), &free);

  if (!array)
  {
    throw std::runtime_error("Could not allocate memory");
  }

  std::size_t index = 0;
  while (fin)
  {
    fin.read(&array[index], initialSize);
    index += fin.gcount();
    if (fin.gcount() == initialSize)
    {
      size += initialSize;
      std::unique_ptr<char[], decltype(&free)> newArray(static_cast<char*>(realloc(array.get(), size)), &free);
      if (!newArray)
      {
        throw std::runtime_error("Could not allocate memory for new chars");
      }
      array.release();
      std::swap(array, newArray);
    }
    if (!fin.eof() && fin.fail())
    {
      throw std::runtime_error("Chars reading failed");
    }
  }

  std::vector<char> vecChar(&array[0], &array[index]);

  moiseeva::details::VectorPrinter<char>(vecChar, spaceSwitch);
}
