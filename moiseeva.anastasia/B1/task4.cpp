#include "tasks.hpp"
#include <string>

void moiseeva::tasks::itemFour(const char* sortParameter, const char* sizeChar)
{
  int spaceSwitch = 1;

  std::vector<double> vecDouble;

  unsigned int vecSize(0);
  try
  {
    vecSize = std::stoi(sizeChar);
  }
  catch (...)
  {
    throw std::invalid_argument("Wrong length.");
  }

  if ((sortParameter != moiseeva::details::ASCENDING) && (sortParameter != moiseeva::details::DESCENDING))
  {
    throw std::invalid_argument("Wrong sorting parameter.");
  }

  moiseeva::details::fillRandom_ForVector(vecDouble, vecSize);

  moiseeva::details::VectorPrinter(vecDouble, spaceSwitch);
  std::cout << std::endl;

  if (sortParameter == moiseeva::details::ASCENDING)
  {
    moiseeva::details::sortVecT(vecDouble, moiseeva::details::getByIterator, true);
  }
  else if (sortParameter == moiseeva::details::DESCENDING)
  {
    moiseeva::details::sortVecT(vecDouble, moiseeva::details::getByIterator, false);
  }

  moiseeva::details::VectorPrinter(vecDouble, spaceSwitch);
}
