#include <iostream>
#include <stdexcept>
#include <vector>
#include <algorithm>

#include "datastruct.hpp"
#include "otherfuncs.hpp"


int main(){

  std::vector<moiseeva::DataStruct> structs;
  std::string line;

  try{
    while(std::getline(std::cin, line)){
      std::stringstream buff(line);
      structs.push_back(moiseeva::structBuilder(buff));
      }
    }
  catch(std::exception & error){
    std::cerr << error.what();
    return 1;
  }

  if (structs.size() > 1){
    std::sort(structs.begin(), structs.end(), moiseeva::compare);
  }

  if(!structs.empty()){
    for (size_t i = 0; i < structs.size(); i++){
      std::cout << structs[i].key1 << "," << structs[i].key2 << "," << structs[i].str << std::endl;
    }
  }

  return 0;
}
