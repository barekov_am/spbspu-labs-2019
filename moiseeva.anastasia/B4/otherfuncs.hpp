#ifndef OTHERFUNCS_HPP
#define OTHERFUNCS_HPP

#include <sstream>
#include <vector>

#include "datastruct.hpp"

namespace moiseeva{

  moiseeva::DataStruct structBuilder(std::istream & line);

  bool compare(moiseeva::DataStruct struct1, moiseeva::DataStruct struct2);

}

#endif // OTHERFUNCS_HPP
