#include "tasks.hpp"

#include <set>
#include "shape.hpp"

void moiseeva::task1()
{
  std::set<std::string> words;
  std::string buffer;

  while (std::cin >> buffer)
  {
    words.emplace(buffer);
  }

  for (const auto &i : words)
  {
    std::cout << i << std::endl;
  }
}

void moiseeva::task2()
{
  moiseeva::Shapes shapes;

  moiseeva::fillShapeContainer(shapes);

  size_t vertices = static_cast<size_t>(std::accumulate(shapes.begin(), shapes.end(), 0,
                                                        [](size_t totalVerts, const moiseeva::Shape &shape)
                                                        {
                                                          return totalVerts + shape.size();
                                                        }));

  size_t triangles = 0, squares = 0, rectangles = 0;
  triangles = moiseeva::getTrianglesAmount(shapes);
  squares = moiseeva::getSquaresAmount(shapes);
  rectangles = moiseeva::getRectanglesAmount(shapes);

  moiseeva::deletePentagons(shapes);

  std::vector<moiseeva::Point> points = moiseeva::makePointsVector(shapes);

  moiseeva::sortContainer(shapes);

  std::cout << "Vertices: " << vertices << std::endl;
  std::cout << "Triangles: " << triangles << std::endl;
  std::cout << "Squares: " << squares << std::endl;
  std::cout << "Rectangles: " << rectangles << std::endl;

  std::cout << "Points:";
  for (const auto &i : points)
  {
    std::cout << " (" << i.x << "; " << i.y << ")";
  }
  std::cout << std::endl;

  std::cout << "Shapes:" << std::endl;
  for (const auto &shape : shapes)
  {
    std::cout << shape.size() << " ";
    for (const auto &i : shape)
    {
      std::cout << "(" << i.x << "; " << i.y << ") ";
    }
    std::cout << std::endl;
  }
}
