#include "shape.hpp"

moiseeva::Shape parseShape(const std::string &line)
{
  std::istringstream stream(line);
  int vertices = 0;
  stream >> vertices;

  if (vertices < 3)
  {
    throw std::invalid_argument("Invalid number of vertices");
  }

  moiseeva::Shape shape;
  for (int i(0); i < vertices; i++)
  {
    moiseeva::Point point = {0, 0};
    stream.ignore(line.length(), '(');
    stream >> point.x;
    stream.ignore(line.length(), ';');
    stream >> point.y;
    stream.ignore(line.length(), ')');
    shape.push_back(point);
  }

  if (stream.fail())
  {
    throw std::invalid_argument("Invalid input");
  }

  return shape;
}

void moiseeva::fillShapeContainer(moiseeva::Shapes &shapesContainer)
{
  std::string line;

  while (std::getline(std::cin, line))
  {
    line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
    if (line.empty())
    {
      continue;
    }
    shapesContainer.push_back(parseShape(line));
  }
}

bool comparePointsX(const moiseeva::Point &left, const moiseeva::Point &right) noexcept
{
  return left.x < right.x;
}

bool comparePointsY(const moiseeva::Point &left, const moiseeva::Point &right) noexcept
{
  return left.y < right.y;
}

int squareDistance(const moiseeva::Point &left, const moiseeva::Point &right) noexcept
{
  return (left.x - right.x) * (left.x - right.x) + (left.y - right.y) * (left.y - right.y);
}

std::vector<moiseeva::Point> sortPoints(const moiseeva::Shape &shape)
{
  if (shape.size() != 4)
  {
    return {};
  }
  std::vector<moiseeva::Point> result;

  moiseeva::Shape sortedShape = shape;
  std::sort(sortedShape.begin(), sortedShape.end(), comparePointsX);

  result.push_back(std::min(sortedShape[0], sortedShape[1], comparePointsY));
  result.push_back(std::max(sortedShape[0], sortedShape[1], comparePointsY));
  result.push_back(std::max(sortedShape[2], sortedShape[3], comparePointsY));
  result.push_back(std::min(sortedShape[2], sortedShape[3], comparePointsY));

  return result;
}

bool isSquareShape(const moiseeva::Shape &shape) noexcept
{
  std::vector<moiseeva::Point> points = sortPoints(shape);

  if (points.empty())
  {
    return false;
  }

  if (squareDistance(points.at(0), points.at(2)) != squareDistance(points.at(1), points.at(3)))
  {
    return false;
  }

  int side = squareDistance(points.at(0), points.at(1));
  return side == squareDistance(points.at(1), points.at(2)) &&
         side == squareDistance(points.at(2), points.at(3)) &&
         side == squareDistance(points.at(3), points.at(0));
}

bool isRectangleShape(const moiseeva::Shape &shape) noexcept
{
  std::vector<moiseeva::Point> points = sortPoints(shape);

  if (points.empty())
  {
    return false;
  }

  return squareDistance(points.at(0), points.at(2)) == squareDistance(points.at(1), points.at(3));
}

size_t moiseeva::getTrianglesAmount(const moiseeva::Shapes &shapesContainer) noexcept
{
  return static_cast<size_t>(std::count_if(shapesContainer.begin(), shapesContainer.end(),
                                           [](const moiseeva::Shape &shape)
                                           {
                                             return shape.size() == 3;
                                           }));
}

size_t moiseeva::getSquaresAmount(const moiseeva::Shapes &shapesContainer) noexcept
{

  return static_cast<size_t>(std::count_if(shapesContainer.begin(), shapesContainer.end(), isSquareShape));
}

size_t moiseeva::getRectanglesAmount(const moiseeva::Shapes &shapesContainer) noexcept
{
  return static_cast<size_t>(std::count_if(shapesContainer.begin(), shapesContainer.end(), isRectangleShape));
}

void moiseeva::deletePentagons(moiseeva::Shapes &shapes) noexcept
{
  shapes.erase(
    std::remove_if(shapes.begin(), shapes.end(), [](const moiseeva::Shape &shape)
    { return shape.size() == 5; }), shapes.end());
}

void moiseeva::sortContainer(moiseeva::Shapes &shapes)
{
  auto endOfTriangles = std::partition(shapes.begin(), shapes.end(), [](const moiseeva::Shape &shape)
  { return shape.size() == 3; });
  auto endOfSquares = std::partition(endOfTriangles, shapes.end(), isSquareShape);
  std::partition(endOfSquares, shapes.end(), isRectangleShape);
}

std::vector<moiseeva::Point> moiseeva::makePointsVector(const moiseeva::Shapes &shapes) noexcept
{
  std::vector<moiseeva::Point> points;

  for (const auto &shape : shapes)
  {
    points.push_back(shape[0]);
  }

  return points;
}
