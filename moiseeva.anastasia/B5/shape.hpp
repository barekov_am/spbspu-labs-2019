#ifndef B5_SHAPE_HPP
#define B5_SHAPE_HPP
#include <vector>
#include <list>
#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>
#include <numeric>

namespace moiseeva
{
  struct Point
  {
    int x, y;
  };

  using Shape = std::vector<Point>;
  using Shapes = std::list<Shape>;

  void fillShapeContainer(moiseeva::Shapes &shapesContainer);

  size_t getTrianglesAmount(const moiseeva::Shapes &shapesContainer) noexcept;

  size_t getSquaresAmount(const moiseeva::Shapes &shapesContainer) noexcept;

  size_t getRectanglesAmount(const moiseeva::Shapes &shapesContainer) noexcept;

  void deletePentagons(moiseeva::Shapes &shapes) noexcept;

  std::vector<moiseeva::Point> makePointsVector(const moiseeva::Shapes &shapes) noexcept;

  void sortContainer(moiseeva::Shapes &shapes);
}
#endif
