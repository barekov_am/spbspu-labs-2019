#ifndef INC_7_TRIANGLE_HPP
#define INC_7_TRIANGLE_HPP

#endif //INC_7_TRIANGLE_HPP
#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

class Triangle: public Shape
{
public:
  Triangle(const Point_t& center);
  virtual void draw(std::ostream &out) const;
};

#endif //TRIANGLE_HPP
