#include <iostream>

void task1();
void task2();

int main(int argc, char* argv[])
{
    try
    {
        if (argc != 2)
        {
            std::cerr << "Incorrect count of arguments which are sent from the console!";
            return 1;
        }
        char* checkedPtr = nullptr;
        const int task = std::strtoll(argv[1], &checkedPtr, 10);
        if (*checkedPtr != '\x00')
        {
            std::cerr << "The argument is incorrect!\n";
            return 1;
        }
        switch (task)
        {
            case 1:
                task1();
                break;
            case 2:
                task2();
                break;
            default:
                std::cerr << "Invalid task number!\n";
                return 1;
        }
    }
    catch (const std::exception& exception)
    {
        std::cerr << exception.what() << "\n";
        return 1;
    }

    return 0;
}
