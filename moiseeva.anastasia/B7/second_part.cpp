#include <list>
#include <memory>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <functional>

#include "shape.hpp"
#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"

using shapePtr = std::shared_ptr< Shape >;

shapePtr createShape(const std::string& shapeName, const Point_t& cur_Center)
{   
    if (shapeName == "CIRCLE")
    {
        return std::make_shared< Circle >(Circle(cur_Center));
    }
    else if (shapeName == "SQUARE")
    {
        return std::make_shared< Square >(Square(cur_Center));
    }
    else if (shapeName == "TRIANGLE")
    {
        return std::make_shared< Triangle >(Triangle(cur_Center));
    }
    else
    {
        throw std::invalid_argument("Invalid name of shape\n");
    }
}

std::string getShapeName(std::stringstream& strStream)
{
    std::string shapeName;
    std::getline(strStream, shapeName, '(');
    strStream.unget();//помещает символ назад
    shapeName.erase(std::remove_if(shapeName.begin(), shapeName.end(), isspace), shapeName.end());//удаляем все после последней буквы

    if ((shapeName != "CIRCLE") && (shapeName != "SQUARE") && (shapeName != "TRIANGLE"))
    {
        throw std::invalid_argument("Invalid shape name!");
    }

    return shapeName;
}

Point_t getCenter1(std::stringstream& strStream, const size_t& lineLength)
{
    strStream.ignore(lineLength, '(');//извлекает и отбрасывает символы, пока заданный символ не будет найден
    Point_t cur_Center = {0, 0};
    strStream >> cur_Center.x;

    strStream.ignore(lineLength, ';');
    strStream >> cur_Center.y;

    strStream.ignore(lineLength, ')');

    if (strStream.fail())
    {
        throw std::invalid_argument("Invalid point data!");
    }

    return cur_Center;
}

void print(const std::list< shapePtr >& shapeList)
{
std::for_each(std::begin(shapeList), std::end(shapeList), [&](const shapePtr& shape) {
shape->draw(std::cout);
});
}

void  task2()

{
    std::list< shapePtr > shapes;
    std::string nextLine;

    while (std::getline(std::cin >> std::ws, nextLine))
    {
        if (std::cin.fail())
        {
            throw std::ios_base::failure("An error occurred while writing a line!\n");
        }
        if (nextLine.empty())
        {
            continue;
        }

        std::stringstream stringStream(nextLine);

        const std::string shapeName = getShapeName(stringStream);
        const Point_t center = getCenter1(stringStream, nextLine.length());
        shapes.push_back(createShape(shapeName, center));

    }

    std::cout << "Original:\n";
    print(shapes);

    std::cout << "Left-Right:\n";
    shapes.sort([](const shapePtr& tmp, const shapePtr& next) { return tmp->isMoreLeft(*next); });
    print(shapes);

    std::cout << "Right-Left:\n";
    shapes.sort([](const shapePtr& tmp, const shapePtr& next) { return !tmp->isMoreLeft(*next); });
    print(shapes);

    std::cout << "Top-Bottom:\n";
    shapes.sort([](const shapePtr& tmp, const shapePtr& next) { return tmp->isUpper(*next); });
    print(shapes);

    std::cout << "Bottom-Top:\n";
    shapes.sort([](const shapePtr& tmp, const shapePtr& next) { return !tmp->isUpper(*next); });
    print(shapes);
}
