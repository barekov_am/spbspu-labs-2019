#ifndef SHAPE_B7
#define SHAPE_B7

#include <iostream>

struct Point_t
{
  int x;
  int y;
};

class Shape
{
public:
  Shape(const Point_t& center);
  ~Shape() = default;

  bool isMoreLeft(const Shape& shape) const;
  bool isUpper(const Shape& shape) const;
  virtual void draw(std::ostream &out) const = 0;

  Point_t getCenter() const;

private:
  Point_t center_;
};

#endif
