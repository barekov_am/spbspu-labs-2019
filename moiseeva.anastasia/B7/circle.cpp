#include "circle.hpp"

Circle::Circle(const Point_t& center):
        Shape(center)
{
}

void Circle::draw(std::ostream &out) const
{
    out << "CIRCLE (" << getCenter().x << ";" << getCenter().y << ")\n";
}
