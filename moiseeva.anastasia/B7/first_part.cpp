#include <iterator>
#include <iostream>
#include <functional>
#include <cmath>
#include <algorithm>

void task1()
{
    std::transform(std::istream_iterator<double>(std::cin), std::istream_iterator<double>(),
                   std::ostream_iterator<double>(std::cout, " "), std::bind(std::multiplies<double>(), std::placeholders::_1, M_PI));

    if (!std::cin.eof() && std::cin.fail())
    {
        throw std::ios_base::failure("Reading caused an error");
    }

    if (!std::cin.eof())
    {
        throw std::ios_base::failure("Reading failed!\n");
    }
}
