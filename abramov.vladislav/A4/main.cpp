#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

void printArea(const abramov::Shape& actualShape)
{
  std::cout << "area = " << actualShape.getArea() << std::endl;
  std::cout << std::endl;
}

void printFrameRect(const abramov::Shape& actualShape)
{
  abramov::rectangle_t Rect_ = actualShape.getFrameRect();
  std::cout << "frame rectangle:" << std::endl;
  std::cout << "width = " << Rect_.width;
  std::cout << "  height = " << Rect_.height;
  abramov::point_t point = Rect_.pos;
  std::cout << "  x = " << point.x;
  std::cout << "  y = " << point.y;
  std::cout << std::endl << std::endl;
}

void shapeTest(abramov::Shape& actualShape1, abramov::Shape& actualShape2)
{
  actualShape1.printParameters();

  std::cout << "creating same shape (2nd constructor)" << std::endl;
  actualShape2.printParameters();

  printArea(actualShape2);
  printFrameRect(actualShape2);
  std::cout << "moving shape on dx = 1, dy = -2" << std::endl;
  actualShape2.move(1.0, -2.0);
  actualShape2.printParameters();

  std::cout << "moving shape to x = 4, y = 5" << std::endl;
  actualShape2.move({4.0, 5.0});
  actualShape2.printParameters();

  std::cout << "scaling shape *1.5" << std::endl;
  actualShape2.scale(1.5);
  actualShape2.printParameters();

  std::cout << "rotating shape on 30 degrees" << std::endl;
  actualShape2.rotate(30);
  actualShape2.printParameters();
}

void shapeTest(abramov::Shape& actualShape)
{
  actualShape.printParameters();

  printArea(actualShape);
  printFrameRect(actualShape);
  std::cout << "moving shape on dx = 1, dy = -2" << std::endl;
  actualShape.move(1.0, -2.0);
  actualShape.printParameters();

  std::cout << "moving shape to x = 4, y = 5" << std::endl;
  actualShape.move({4.0, 5.0});
  actualShape.printParameters();

  std::cout << "scaling shape *1.5" << std::endl;
  actualShape.scale(1.5);
  actualShape.printParameters();

  std::cout << "rotating shape on 30 degrees" << std::endl;
  actualShape.rotate(30);
  actualShape.printParameters();
}

int main()
{
  std::cout << "RECTANGLES: " << std::endl;
  std::cout << "creating shape: x = -1, y = 3, w = 2, h = 4" << std::endl;
  abramov::Rectangle rectangle1({-1.0, 3.0}, 2.0, 4.0);
  abramov::Rectangle rectangle2(-1.0, 3.0, 2.0, 4.0);

  shapeTest(rectangle1, rectangle2);

  std::cout << "CIRCLES: " << std::endl;
  std::cout << "creating shape: x = -1, y = 3, r = 4" << std::endl;
  abramov::Circle circle1({-1.0, 3.0}, 4.0);
  abramov::Circle circle2(-1.0, 3.0, 4.0);

  shapeTest(circle1, circle2);

  std::cout << "TRIANGLES: " << std::endl;
  std::cout << "creating shape: (1; 3), (-4; 2), (-1; -6)" << std::endl;
  abramov::Triangle triangle1({1.0, 3.0}, {-4.0, 2.0}, {-1.0, -6.0});
  abramov::Triangle triangle2(1.0, 3.0, -4.0, 2.0, -1.0, -6.0);

  shapeTest(triangle1, triangle2);

  std::cout << "POLYGONS: " << std::endl;
  std::cout << "creating shape: (1; 3), (-4; 2), (-1; -6)" << std::endl;
  abramov::point_t pointArray[] {{4.0, 5.0}, {6.0, 4.0}, {7.0, -1.0}, {5.0, -3.0}, {2.0, 1.0}};
  abramov::Polygon polygon1(5, pointArray);

  shapeTest(polygon1);

  std::cout << "COMPOSITE SHAPE: " << std::endl;
  std::cout << "creating empty composite shape" << std::endl;
  abramov::CompositeShape composite1;
  std::cout << "adding a rectangle" << std::endl;
  abramov::Shape::pointer rectangle1_ = std::make_shared<abramov::Rectangle>(rectangle1);
  composite1.add(rectangle1_);
  shapeTest(composite1);
  std::cout << "adding a triangle" << std::endl;
  abramov::Shape::pointer triangle1_ = std::make_shared<abramov::Triangle>(triangle1);
  composite1.add(triangle1_);
  shapeTest(composite1);
  std::cout << "adding a circle" << std::endl;
  abramov::Shape::pointer circle1_ = std::make_shared<abramov::Circle>(circle1);
  composite1.add(circle1_);
  shapeTest(composite1);
  std::cout << "adding a polygon" << std::endl;
  abramov::Shape::pointer polygon1_ = std::make_shared<abramov::Polygon>(polygon1);
  composite1.add(polygon1_);
  std::cout << "removing a triangle" << std::endl;
  composite1.remove(2);
  shapeTest(composite1);

  std::cout << "PARTITION: " << std::endl;
  abramov::CompositeShape composite2;
  composite2.add(rectangle1_);
  composite2.add(triangle1_);
  composite2.add(circle1_);
  composite2.add(polygon1_);
  abramov::Matrix matrix = abramov::part(composite2);
  std::cout << "creating matrix from composite shape" << std::endl;
  std::cout << "  Rows: " << matrix.getRows() << std::endl;
  std::cout << "  Columns: " << matrix.getColumns() << std::endl;

  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    for (size_t j = 0; j < matrix.getLevelSize(i); j++)
    {
      std::cout << "  Level " << i << ";" << std::endl;
      std::cout << "  Shape " << j << ";"<< std::endl;
      std::cout << "  Centre (";
      std::cout << matrix[i][j]->getFrameRect().pos.x << ", " << matrix[i][j]->getFrameRect().pos.y << ");"<< std::endl;
    }
    std::cout << std::endl;
  }

  return 0;
}
