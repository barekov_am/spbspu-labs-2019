#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteTriangle)

  const double PRECISION = 0.01;

  BOOST_AUTO_TEST_CASE(TriangleConstAfterMoving)
  {
    abramov::Triangle triangle_({1.0, -1.0}, {2.0, 2.0}, {3.0, -2.0});
    const abramov::rectangle_t firstFrame = triangle_.getFrameRect();
    const double firstArea = triangle_.getArea();

    triangle_.move({4.0, 5.0});
    abramov::rectangle_t secondFrame = triangle_.getFrameRect();
    double secondArea = triangle_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
    BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);

    triangle_.move(-2.0, 3.0);
    secondFrame = triangle_.getFrameRect();
    secondArea = triangle_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
    BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);
  }

  BOOST_AUTO_TEST_CASE(TriangleScaling)
  {
    abramov::Triangle triangle_({1.0, -1.0}, {2.0, 2.0}, {3.0, -2.0});
    const double firstArea = triangle_.getArea();

    const double multiplier = 4.0;
    triangle_.scale(multiplier);
    double secondArea = triangle_.getArea();

    BOOST_CHECK_CLOSE(firstArea *  multiplier * multiplier, secondArea, PRECISION);
  }

  BOOST_AUTO_TEST_CASE(TriangleThrowingExceptions)
  {
    BOOST_CHECK_THROW(abramov::Triangle({0.0, 0.0}, {-1.0, 0.0}, {1.0, 0.0}), std::invalid_argument);

    abramov::Triangle triangle_({3.0, 5.0}, {2.0, 2.0}, {-1.0, -3.0});
    BOOST_CHECK_THROW(triangle_.scale(0.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
