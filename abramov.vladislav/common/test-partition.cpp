#include <boost/test/auto_unit_test.hpp>
#include <memory>

#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

const double PRECISION = 0.01;

BOOST_AUTO_TEST_SUITE(testPartition)

  BOOST_AUTO_TEST_CASE(intersectCorrectness)
  {
    abramov::Circle testCircle({2.0, 0.0}, 3.0);
    abramov::Rectangle testRectangle({2.0, 4.0}, 9.0, 5.0);
    abramov::Rectangle testSquare({12.0, 21.0}, 1.0, 1.0);

    BOOST_CHECK(abramov::isIntersected(testCircle.getFrameRect(),testRectangle.getFrameRect()));
    BOOST_CHECK(!(abramov::isIntersected(testSquare.getFrameRect(),testRectangle.getFrameRect())));
    BOOST_CHECK(!(abramov::isIntersected(testCircle.getFrameRect(),testSquare.getFrameRect())));
  }

  BOOST_AUTO_TEST_CASE(partitionCorrectness)
  {
    abramov::Shape::pointer testCircle = std::make_shared<abramov::Circle>(abramov::point_t{1.0, 1.0}, 2.0);
    abramov::Shape::pointer testRectangle1 = std::make_shared<abramov::Rectangle>(abramov::point_t{3.0, -1.0}, 5.0, 4.0);
    abramov::Shape::pointer testSquare1 = std::make_shared<abramov::Rectangle>(abramov::point_t{6.0, 15.0}, 4.0, 4.0);
    abramov::Shape::pointer testRectangle2 = std::make_shared<abramov::Rectangle>(abramov::point_t{10.0, 2.0}, 2.0, 3.0);
    abramov::Shape::pointer testRectangle3 = std::make_shared<abramov::Rectangle>(abramov::point_t{8.0, 15.0}, 6.0, 30.0);
    abramov::Shape::pointer testSquare2 = std::make_shared<abramov::Rectangle>(abramov::point_t{-20.0, 15.0}, 5.0, 5.0);

    abramov::CompositeShape testComposition;
    testComposition.add(testCircle);
    testComposition.add(testRectangle1);
    testComposition.add(testSquare1);
    testComposition.add(testRectangle2);
    testComposition.add(testRectangle3);
    testComposition.add(testSquare2);

    abramov::Matrix testMatrix = abramov::part(testComposition);

    const std::size_t correctRows = 3;
    const std::size_t correctColumns = 4;

    BOOST_CHECK_EQUAL(testMatrix.getRows(), correctRows);
    BOOST_CHECK_EQUAL(testMatrix.getColumns(), correctColumns);
    BOOST_CHECK(testMatrix[0][0] == testCircle);
    BOOST_CHECK(testMatrix[0][1] == testSquare1);
    BOOST_CHECK(testMatrix[0][2] == testRectangle2);
    BOOST_CHECK(testMatrix[0][3] == testSquare2);
    BOOST_CHECK(testMatrix[1][0] == testRectangle1);
    BOOST_CHECK(testMatrix[2][0] == testRectangle3);
  }

BOOST_AUTO_TEST_SUITE_END()
