#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace abramov
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    point_t pos;
    double width;
    double height;
  };

  bool isIntersected(const rectangle_t& lhs, const rectangle_t& rhs);

}

#endif //BASE_TYPES_HPP
