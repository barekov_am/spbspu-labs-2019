#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteCompositeShape)

  const double PRECISION = 0.01;

  BOOST_AUTO_TEST_CASE(CompositeShapeCopyConstructor)
  {
    abramov::Shape::pointer rectangle_ = std::make_shared<abramov::Rectangle>(abramov::point_t {2.0, 2.0}, 2.0, 3.0);
    abramov::Shape::pointer circle_ = std::make_shared<abramov::Circle>(abramov::point_t {1.0, 1.0}, 1.0);
    abramov::CompositeShape composite1_(rectangle_);
    composite1_.add(circle_);

    const abramov::rectangle_t frame1_ = composite1_.getFrameRect();
    const double area1_ = composite1_.getArea();

    abramov::CompositeShape composite2_(composite1_);

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, PRECISION);
    BOOST_CHECK_CLOSE(area1_, composite2_.getArea(), PRECISION);
    BOOST_CHECK_EQUAL(composite1_.getSize(), composite2_.getSize());
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeMoveConstructor)
  {
    abramov::Shape::pointer rectangle_ = std::make_shared<abramov::Rectangle>(abramov::point_t {2.0, 2.0}, 2.0, 3.0);
    abramov::Shape::pointer circle_ = std::make_shared<abramov::Circle>(abramov::point_t {1.0, 1.0}, 1.0);
    abramov::CompositeShape composite1_(rectangle_);
    composite1_.add(circle_);

    const abramov::rectangle_t frame1_ = composite1_.getFrameRect();
    const double area_ = composite1_.getArea();
    const size_t size_ = composite1_.getSize();

    abramov::CompositeShape composite2_(std::move(composite1_));

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, PRECISION);
    BOOST_CHECK_CLOSE(area_, composite2_.getArea(), PRECISION);
    BOOST_CHECK_EQUAL(size_, composite2_.getSize());
    BOOST_CHECK_CLOSE(composite1_.getArea(), 0, PRECISION);
    BOOST_CHECK_EQUAL(composite1_.getSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeCopyOperator)
  {
    abramov::Shape::pointer rectangle_ = std::make_shared<abramov::Rectangle>(abramov::point_t {2.0, 2.0}, 2.0, 3.0);
    abramov::Shape::pointer circle_ = std::make_shared<abramov::Circle>(abramov::point_t {1.0, 1.0}, 1.0);
    abramov::CompositeShape composite_(rectangle_);
    composite_.add(circle_);

    const abramov::rectangle_t frame1_ = composite_.getFrameRect();
    const double area_ = composite_.getArea();

    abramov::CompositeShape composite2_;
    composite2_ = composite_;

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, PRECISION);
    BOOST_CHECK_CLOSE(area_, composite2_.getArea(), PRECISION);
    BOOST_CHECK_EQUAL(composite_.getSize(), composite2_.getSize());
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeMoveOperator)
  {
    abramov::Shape::pointer rectangle_ = std::make_shared<abramov::Rectangle>(abramov::point_t {2.0, 2.0}, 2.0, 3.0);
    abramov::Shape::pointer circle_ = std::make_shared<abramov::Circle>(abramov::point_t {1.0, 1.0}, 1.0);
    abramov::CompositeShape composite1_(rectangle_);
    composite1_.add(circle_);

    const abramov::rectangle_t frame1_ = composite1_.getFrameRect();
    const double area_ = composite1_.getArea();
    const size_t size_ = composite1_.getSize();

    abramov::CompositeShape composite2_;
    composite2_ = std::move(composite1_);

    BOOST_CHECK_CLOSE(frame1_.pos.x, composite2_.getFrameRect().pos.x, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.pos.y, composite2_.getFrameRect().pos.y, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.width, composite2_.getFrameRect().width, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.height, composite2_.getFrameRect().height, PRECISION);
    BOOST_CHECK_CLOSE(area_, composite2_.getArea(), PRECISION);
    BOOST_CHECK_EQUAL(size_, composite2_.getSize());
    BOOST_CHECK_CLOSE(composite1_.getArea(), 0, PRECISION);
    BOOST_CHECK_EQUAL(composite1_.getSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeConstAfterMoving)
  {
    abramov::CompositeShape composite_;
    abramov::Shape::pointer circle_ = std::make_shared<abramov::Circle>(abramov::point_t {1.0, -1.0}, 2.0);
    abramov::Shape::pointer rectangle_ = std::make_shared<abramov::Rectangle>(abramov::point_t {1.0, -1.0}, 2.0, 3.0);
    abramov::Shape::pointer triangle_ = std::make_shared<abramov::Triangle>(abramov::Triangle {{1.0, -1.0}, {2.0, 2.0}, {3.0, -2.0}});
    abramov::point_t pointArray[] {{4.0, 5.0}, {6.0, 4.0}, {7.0, -1.0}, {5.0, -3.0}, {2.0, 1.0}};
    abramov::Shape::pointer polygon_ = std::make_shared<abramov::Polygon>(abramov::Polygon {5, pointArray});
    composite_.add(circle_);
    composite_.add(rectangle_);
    composite_.add(triangle_);
    composite_.add(polygon_);

    const abramov::rectangle_t firstFrame = composite_.getFrameRect();
    const double firstArea = composite_.getArea();

    composite_.move({4.0, 5.0});
    abramov::rectangle_t secondFrame = composite_.getFrameRect();
    double secondArea = composite_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
    BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);

    composite_.move(-2.0, 3.0);
    secondFrame = composite_.getFrameRect();
    secondArea = composite_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
    BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeScaling)
  {
    abramov::CompositeShape composite_;
    abramov::Shape::pointer circle_ = std::make_shared<abramov::Circle>(abramov::point_t {1.0, -1.0}, 2.0);
    abramov::Shape::pointer rectangle_ = std::make_shared<abramov::Rectangle>(abramov::point_t {1.0, -1.0}, 2.0, 3.0);
    abramov::Shape::pointer triangle_ = std::make_shared<abramov::Triangle>(abramov::Triangle {{1.0, -1.0}, {2.0, 2.0}, {3.0, -2.0}});
    abramov::point_t pointArray[] {{4.0, 5.0}, {6.0, 4.0}, {7.0, -1.0}, {5.0, -3.0}, {2.0, 1.0}};
    abramov::Shape::pointer polygon_ = std::make_shared<abramov::Polygon>(abramov::Polygon {5, pointArray});
    composite_.add(circle_);
    composite_.add(rectangle_);
    composite_.add(triangle_);
    composite_.add(polygon_);

    const double firstArea = composite_.getArea();
    const double multiplier = 6.0;
    composite_.scale(multiplier);
    double secondArea = composite_.getArea();

    BOOST_CHECK_CLOSE(firstArea *  multiplier * multiplier, secondArea, PRECISION);
  }

  BOOST_AUTO_TEST_CASE(testCompositeShapeRotation)
  {
    abramov::Shape::pointer testCircle = std::make_shared<abramov::Circle>(abramov::point_t { 3.0, 2.2 }, 3.0);
    abramov::Shape::pointer testRectangle = std::make_shared<abramov::Rectangle>(abramov::point_t { 4.0, 5.0 }, 9.0, 2.0);
    abramov::CompositeShape testComposition;

    testComposition.add(testCircle);
    testComposition.add(testRectangle);

    const double areaBefore = testComposition.getArea();
    const abramov::rectangle_t frameRectBefore = testComposition.getFrameRect();

    double angle = 360;
    testComposition.rotate(angle);

    double areaAfter = testComposition.getArea();
    abramov::rectangle_t frameRectAfter = testComposition.getFrameRect();

    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);

    angle = 90;
    testComposition.rotate(angle);

    areaAfter = testComposition.getArea();
    frameRectAfter = testComposition.getFrameRect();

    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);

    angle = -450;
    testComposition.rotate(angle);

    areaAfter = testComposition.getArea();
    frameRectAfter = testComposition.getFrameRect();

    BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, PRECISION);
    BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, PRECISION);
    BOOST_CHECK_CLOSE(areaAfter, areaBefore, PRECISION);
  }


  BOOST_AUTO_TEST_CASE(operatorTests)
  {
    abramov::Shape::pointer rectangle1_ = std::make_shared<abramov::Rectangle>(abramov::point_t {1.0, 1.0}, 2.0, 2.0);
    abramov::CompositeShape compShape1_(rectangle1_);

    const abramov::rectangle_t testFrameRect = rectangle1_->getFrameRect();
    const double area = rectangle1_->getArea();

    abramov::Shape::pointer circle1_ = std::make_shared<abramov::Circle>(abramov::point_t {2.0, 2.0}, 2.0);
    abramov::CompositeShape compShape2_(circle1_);

    BOOST_CHECK_NO_THROW(abramov::CompositeShape compShape2_(compShape1_));
    BOOST_CHECK_NO_THROW(compShape2_= compShape1_);
    BOOST_CHECK_NO_THROW(compShape2_= std::move(compShape1_));
    BOOST_CHECK_NO_THROW(abramov::CompositeShape compShape2_(std::move(compShape1_)));

    compShape1_.add(rectangle1_);
    BOOST_CHECK_CLOSE(testFrameRect.width, compShape1_.getFrameRect().width, PRECISION);
    BOOST_CHECK_CLOSE(testFrameRect.height, compShape1_.getFrameRect().height, PRECISION);
    BOOST_CHECK_CLOSE(area, compShape1_.getArea(), PRECISION);
  }

BOOST_AUTO_TEST_SUITE_END()
