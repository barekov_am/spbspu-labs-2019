#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace abramov
{
  abramov::Matrix part(const abramov::CompositeShape& composition);
}

#endif //A3_PARTITION_HPP
