#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include <iostream>

#include "shape.hpp"

namespace abramov
{
  class CompositeShape: public Shape
  {
  public:

    CompositeShape();
    CompositeShape(const CompositeShape &newShape);
    CompositeShape(CompositeShape &&newShape);
    CompositeShape(const Shape::pointer& shape);
    ~CompositeShape() override = default;

    CompositeShape& operator =(const CompositeShape& newShape);
    CompositeShape& operator =(CompositeShape&& newShape);
    Shape::pointer operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printParameters() const override;
    void move(const point_t& newCentre) override;
    void move(double dx, double dy) override;
    void scale(double multiplier) override;
    void rotate(double angle) override;
    size_t getSize() const;

    void add(const Shape::pointer& shape);
    void remove(size_t index);

  private:
    size_t numShapes_;
    Shape::array shapes_;
  };
}

#endif // COMPOSITESHAPE_HPP
