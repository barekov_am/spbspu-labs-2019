#include "circle.hpp"

#include <iostream>
#include <cmath>

abramov::Circle::Circle(const point_t& centre, double radius):
  centre_(centre),
  radius_(radius)
{
  if (radius_ <= 0.0)
  {
    throw std::invalid_argument("radius of circle can't be <=0");
  }
}

abramov::Circle::Circle(double x, double y, double radius):
  Circle({x, y}, radius)
{ }

void abramov::Circle::printParameters() const
{
  std::cout << "circle: x=" << centre_.x << ", y=" << centre_.y << ", rad="
      << radius_ << std::endl << std::endl;
}

double abramov::Circle::getArea() const
{
  return (M_PI * radius_ * radius_);
}

abramov::rectangle_t abramov::Circle::getFrameRect() const
{
  return {centre_, radius_ * 2, radius_ * 2};
}

void abramov::Circle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

void abramov::Circle::move(const point_t& centre)
{
  centre_ = centre;
}

void abramov::Circle::scale(double multiplier)
{
  if (multiplier <= 0.0)
  {
    throw std::invalid_argument("scale multiplier can't be <= 0");
  }
  radius_ *= multiplier;
}

void abramov::Circle::rotate(double)
{ }
