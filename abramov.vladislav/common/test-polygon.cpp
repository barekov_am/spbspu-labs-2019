#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "polygon.hpp"

BOOST_AUTO_TEST_SUITE(testSuitePolygon)

  const double PRECISION = 0.01;

  BOOST_AUTO_TEST_CASE(PolygonCopyConstructor)
  {
    abramov::point_t pointArray[] {{4.0, 5.0}, {6.0, 4.0}, {7.0, -1.0}, {5.0, -3.0}, {2.0, 1.0}};

    abramov::Polygon polygon1_(5, pointArray);

    const abramov::rectangle_t frame1_ = polygon1_.getFrameRect();
    const double area1_ = polygon1_.getArea();

    abramov::Polygon polygon2_(polygon1_);

    BOOST_CHECK_CLOSE(frame1_.pos.x, polygon2_.getFrameRect().pos.x, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.pos.y, polygon2_.getFrameRect().pos.y, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.width, polygon2_.getFrameRect().width, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.height, polygon2_.getFrameRect().height, PRECISION);
    BOOST_CHECK_CLOSE(area1_, polygon2_.getArea(), PRECISION);
    BOOST_CHECK_EQUAL(polygon1_.getSize(), polygon2_.getSize());
  }

  BOOST_AUTO_TEST_CASE(PolygonMoveConstructor)
  {
    abramov::point_t pointArray[] {{4.0, 5.0}, {6.0, 4.0}, {7.0, -1.0}, {5.0, -3.0}, {2.0, 1.0}};

    abramov::Polygon polygon1_(5, pointArray);

    const abramov::rectangle_t frame1_ = polygon1_.getFrameRect();
    const double area1_ = polygon1_.getArea();
    const size_t size1_ = polygon1_.getSize();

    abramov::Polygon polygon2_(std::move(polygon1_));

    BOOST_CHECK_CLOSE(frame1_.pos.x, polygon2_.getFrameRect().pos.x, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.pos.y, polygon2_.getFrameRect().pos.y, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.width, polygon2_.getFrameRect().width, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.height, polygon2_.getFrameRect().height, PRECISION);
    BOOST_CHECK_CLOSE(area1_, polygon2_.getArea(), PRECISION);
    BOOST_CHECK_EQUAL(size1_, polygon2_.getSize());
    BOOST_CHECK_CLOSE(polygon1_.getArea(), 0, PRECISION);
    BOOST_CHECK_EQUAL(polygon1_.getSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(PolygonCopyOperator)
  {
    abramov::point_t pointArray1[] {{4.0, 5.0}, {6.0, 4.0}, {7.0, -1.0}, {5.0, -3.0}, {2.0, 1.0}};

    abramov::Polygon polygon1_(5, pointArray1);

    const abramov::rectangle_t frame1_ = polygon1_.getFrameRect();
    const double area1_ = polygon1_.getArea();

    abramov::Polygon polygon2_(5, pointArray1);
    polygon2_ = polygon1_;

    BOOST_CHECK_CLOSE(frame1_.pos.x, polygon2_.getFrameRect().pos.x, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.pos.y, polygon2_.getFrameRect().pos.y, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.width, polygon2_.getFrameRect().width, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.height, polygon2_.getFrameRect().height, PRECISION);
    BOOST_CHECK_CLOSE(area1_, polygon2_.getArea(), PRECISION);
    BOOST_CHECK_EQUAL(polygon1_.getSize(), polygon2_.getSize());
  }

  BOOST_AUTO_TEST_CASE(PolygonMoveOperator)
  {
    abramov::point_t pointArray[] {{4.0, 5.0}, {6.0, 4.0}, {7.0, -1.0}, {5.0, -3.0}, {2.0, 1.0}};

    abramov::Polygon polygon1_(5, pointArray);

    const abramov::rectangle_t frame1_ = polygon1_.getFrameRect();
    const double area1_ = polygon1_.getArea();
    const size_t size1_ = polygon1_.getSize();

    abramov::Polygon polygon2_(5, pointArray);
    polygon2_ = std::move(polygon1_);

    BOOST_CHECK_CLOSE(frame1_.pos.x, polygon2_.getFrameRect().pos.x, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.pos.y, polygon2_.getFrameRect().pos.y, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.width, polygon2_.getFrameRect().width, PRECISION);
    BOOST_CHECK_CLOSE(frame1_.height, polygon2_.getFrameRect().height, PRECISION);
    BOOST_CHECK_CLOSE(area1_, polygon2_.getArea(), PRECISION);
    BOOST_CHECK_EQUAL(size1_, polygon2_.getSize());
    BOOST_CHECK_CLOSE(polygon1_.getArea(), 0, PRECISION);
    BOOST_CHECK_EQUAL(polygon1_.getSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(PolygonConstAfterMoving)
  {
    abramov::point_t pointArray[] {{4.0, 5.0}, {6.0, 4.0}, {7.0, -1.0}, {5.0, -3.0}, {2.0, 1.0}};

    abramov::Polygon polygon_(5, pointArray);
    const abramov::rectangle_t firstFrame = polygon_.getFrameRect();
    const double firstArea = polygon_.getArea();

    polygon_.move({4, 5});
    abramov::rectangle_t secondFrame = polygon_.getFrameRect();
    double secondArea = polygon_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
    BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);

    polygon_.move(-2, 3);
    secondFrame = polygon_.getFrameRect();
    secondArea = polygon_.getArea();

    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, PRECISION);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, PRECISION);
    BOOST_CHECK_CLOSE(firstArea, secondArea, PRECISION);
  }

  BOOST_AUTO_TEST_CASE(PolygonScaling)
  {
    abramov::point_t pointArray[]{{4.0, 5.0}, {6.0, 4.0}, {7.0, -1.0}, {5.0, -3.0}, {2.0, 1.0}};
    abramov::Polygon polygon_(5, pointArray);
    const double firstArea = polygon_.getArea();

    const double multiplier = 6;
    polygon_.scale(multiplier);
    double secondArea = polygon_.getArea();

    BOOST_CHECK_CLOSE(firstArea *  multiplier * multiplier, secondArea, PRECISION);
  }

  BOOST_AUTO_TEST_CASE(PolygonThrowingExceptions)
  {
    abramov::point_t pointArray1[] {{0.0, 0.0}, {-1.0, 0.0}};
    BOOST_CHECK_THROW(abramov::Polygon(2, pointArray1), std::invalid_argument);
    abramov::point_t pointArray2[] {{-1.0, 1.0}, {0.0, -1.0}, {1.0, 1.0}, {2.0, -1.0}};
    BOOST_CHECK_THROW(abramov::Polygon(4, pointArray2), std::invalid_argument);
    abramov::point_t pointArray3[] {{-1.0, -1.0}, {1.0, 4.0}, {3.0, -1.0}, {0.0, 0.0}};
    BOOST_CHECK_THROW(abramov::Polygon(4, pointArray3), std::invalid_argument);

    abramov::point_t pointArray[] {{4.0, 5.0}, {6.0, 4.0}, {7.0, -1.0}, {5.0, -3.0}, {2.0, 1.0}};
    abramov::Polygon polygon_(5, pointArray);
  }


BOOST_AUTO_TEST_SUITE_END()
