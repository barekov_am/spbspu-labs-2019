#include "triangle.hpp"
#include <cassert>
#include <cmath>

Triangle::Triangle(const point_t &top1, const point_t &top2, const point_t &top3):
  top1_(top1),
  top2_(top2),
  top3_(top3),
  center_({(top1_.x + top2_.x + top3_.x) / 3, (top1_.y + top2_.y + top3_.y) / 3})
{
  assert(getArea() != 0);
}

double Triangle::getArea() const
{
  return fabs(0.5 * (((top1_.x - top3_.x) * (top2_.y - top3_.y))
                     - ((top2_.x - top3_.x) * (top1_.y - top3_.y))));
}

rectangle_t Triangle::getFrameRect() const
{
  const double right = fmax(top1_.x, fmax(top2_.x, top3_.x));
  const double left = fmin(top1_.x, fmin(top2_.x, top3_.x));
  const double top = fmax(top1_.y, fmax(top2_.y, top3_.y));
  const double bottom = fmin(top1_.y, fmin(top2_.y, top3_.y));
  const double width = right - left;
  const double height = top - bottom;
  const double xOfCenter = (left + right) / 2;
  const double yOfCenter = (top + bottom) / 2;
  return {width, height, {xOfCenter, yOfCenter}};
}

void Triangle::move(const point_t &newCenter)
{
  move(newCenter.x - center_.x, newCenter.y - center_.y);
  center_ = newCenter;
}

void Triangle::move(const double dx, const double dy)
{
  top1_.x += dx;
  top1_.y += dy;
  top2_.x += dx;
  top2_.y += dy;
  top3_.x += dx;
  top3_.y += dy;
  center_.x += dx;
  center_.y += dy;
}

point_t Triangle::getCenter() const
{
  return center_;
}
