#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void printShapeInfo(const Shape &shape)
{
  std::cout << "_FRAME INFO_START_" << std::endl;
  std::cout << "The area is: " << shape.getArea() << std::endl;
  rectangle_t const frameRect = shape.getFrameRect();
  std::cout << "The width is: " << frameRect.width << ", ";
  std::cout << "The length is: " << frameRect.height << ", ";
  std::cout << "The center is: (" << frameRect.pos.x << ", " << frameRect.pos.y << ")" << std::endl;
  std::cout << "_FRAME INFO_END_" << std::endl;
}

int main()
{
  Rectangle rectangle(4, 6, {4.0, 2.0});
  std::cout << "_RECTANGLE INFO_" << std::endl;
  printShapeInfo(rectangle);
  rectangle.move({2.5, 3.0});
  std::cout << "After changing position to {2.5, 3.0}: " << std::endl;
  std::cout << "The center is: (" << rectangle.getFrameRect().pos.x;
  std::cout << ", " << rectangle.getFrameRect().pos.y << ")" << std::endl;
  printShapeInfo(rectangle);
  rectangle.move(2.3, 2.4);
  std::cout << "After moving dx for 2.3 and dy for 2.4: " << std::endl;
  std::cout << "The center is: (" << rectangle.getFrameRect().pos.x;
  std::cout << ", " << rectangle.getFrameRect().pos.y << ")" << std::endl;
  printShapeInfo(rectangle);
  std::cout << std::endl;

  Circle circle(5, {6.3, 3.1});
  std::cout << "_CIRCLE INFO_" << std::endl;
  printShapeInfo(circle);
  circle.move({2.1, 5.7});
  std::cout << "After changing position to {2.1, 5.7}: " << std::endl;
  std::cout << "The center is: (" << circle.getFrameRect().pos.x;
  std::cout << ", " << circle.getFrameRect().pos.y << ")" << std::endl;
  printShapeInfo(circle);
  circle.move(4.0, 2.3);
  std::cout << "After moving dx for 4.0 and dy for 2.3: " << std::endl;
  std::cout << "The center is: (" << circle.getFrameRect().pos.x;
  std::cout << ", " << circle.getFrameRect().pos.y << ")" << std::endl;
  printShapeInfo(circle);
  std::cout << std::endl;

  Triangle triangle({3, 1}, {1, 3}, {2, 0});
  std::cout << "_TRIANGLE INFO_" << std::endl;
  printShapeInfo(triangle);
  triangle.move({3.5, 4.3});
  std::cout << "After changing position to {3.5, 4.3}: " << std::endl;
  std::cout << "The center is: (" << triangle.getCenter().x << ", " << triangle.getCenter().y << ")" << std::endl;
  printShapeInfo(triangle);
  triangle.move(2.5, 6.0);
  std::cout << "After moving dx for 2.5 and dy for 6.0: " << std::endl;
  std::cout << "The center: (" << triangle.getCenter().x << ", " << triangle.getCenter().y << ")" << std::endl;
  printShapeInfo(triangle);
  std::cout << std::endl;

  point_t vertices[] = {{2, 1}, {3, 0}, {3, 3}, {2, 2}}; //the coordinates are set in order CCW
  Polygon polygon(4, vertices);
  std::cout << "_POLYGON INFO_" << std::endl;
  printShapeInfo(polygon);
  polygon.move({2.6, 6.3});
  std::cout << "After changing position to {2.6, 6.3}: " << std::endl;
  std::cout << "The center is: (" << polygon.getCenter().x << ", " << polygon.getCenter().y << ")" << std::endl;
  printShapeInfo(polygon);
  polygon.move(1.9, 3.2);
  std::cout << "After moving dx for 1.9 and dy for 3.2: " << std::endl;
  std::cout << "The center: (" << polygon.getCenter().x << ", " << polygon.getCenter().y << ")" << std::endl;
  printShapeInfo(polygon);
  return 0;
}
