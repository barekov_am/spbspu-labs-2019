#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

class Triangle : public Shape
{
public:
  Triangle(const point_t &top1, const point_t &top2, const point_t &top3);

  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &newCenter) override;
  void move(double dx, double dy) override;
  point_t getCenter() const;

private:
  point_t top1_, top2_, top3_, center_;
};

#endif
