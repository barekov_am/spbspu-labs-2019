#include "polygon.hpp"
#include <cassert>
#include <cmath>

Polygon::Polygon(const int tops, point_t *const vertices):
  tops_(tops),
  vertices_(vertices)
{
  assert(vertices_ != nullptr);
  assert(tops_ > 2);
  for (int i = 0; (i < tops_); ++i)
  {
    assert(((vertices_[(i + 1) % tops_].x - vertices_[i % tops_].x)
            * (vertices_[(i + 2) % tops_].y - vertices_[(i + 1) % tops_].y)
            - (vertices_[(i + 2) % tops_].x - vertices_[(i + 1) % tops_].x)
              * (vertices_[(i + 1) % tops_].y - vertices_[i % tops_].y)) > 0);
  }
  assert(getArea() > 0);
}

double Polygon::getArea() const
{
  double sum = (vertices_[tops_ - 1].x * vertices_[0].y) - (vertices_[0].x * vertices_[tops_ - 1].y);
  for (int i = 0; (i < (tops_ - 1)); ++i)
  {
    sum += (vertices_[i].x * vertices_[i + 1].y) - (vertices_[i + 1].x * vertices_[i].y);
  }
  return sum / 2;
}

rectangle_t Polygon::getFrameRect() const
{
  double right = vertices_[0].x;
  double left = vertices_[0].x;
  double top = vertices_[0].y;
  double bottom = vertices_[0].y;
  for (int i = 1; (i < tops_); ++i)
  {
    left = std::fmin(vertices_[i].x, left);
    right = std::fmax(vertices_[i].x, right);
    bottom = std::fmin(vertices_[i].y, bottom);
    top = std::fmax(vertices_[i].y, top);
  }
  const double width = right - left;
  const double height = top - bottom;
  const double xOfCenter = (left + right) / 2;
  const double yOfCenter = (top + bottom) / 2;
  return {width, height, {xOfCenter, yOfCenter}};
}

void Polygon::move(const point_t &newCenter)
{
  move(newCenter.x - getCenter().x, newCenter.y - getCenter().y);
}

void Polygon::move(const double dx, const double dy)
{
  for (int i = 0; (i < tops_); ++i)
  {
    vertices_[i].x += dx;
    vertices_[i].y += dy;
  }
}

point_t Polygon::getCenter() const
{
  double sumX = 0.0;
  double sumY = 0.0;
  for (int i = 0; (i < tops_); ++i)
  {
    sumX += vertices_[i].x;
    sumY += vertices_[i].y;
  }
  return {(sumX / tops_), (sumY / tops_)};
}
