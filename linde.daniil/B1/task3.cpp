#include <vector>
#include <iostream>
#include <iterator>

#include "print.hpp"

void task3()
{
  std::vector<int> data;
  int currentPosition = 0;
  while (std::cin >> currentPosition, !std::cin.eof())
  {
    if (std::cin.fail())
    {
      throw std::runtime_error{"Failed to read input"};
    }
    if (currentPosition == 0)
    {
      break;
    }
    data.push_back(currentPosition);
  }
  if (data.empty())
  {
    return;
  }

  if (currentPosition != 0)
  {
    throw std::runtime_error("Expected 0 at the end of the input");
  }

  auto curr = data.begin();
  switch (data.back())
  {
    case 1:
    {
      while (curr != data.end())
      {
        if (*curr % 2 == 0)
        {
          curr = data.erase(curr);
        }
        else
        {
          curr ++;
        }
      }
      break;
    }
    case 2:
    {
      while (curr != data.end())
      {
        if (*curr % 3 == 0)
        {
          curr = data.insert(curr + 1, 3, 1) + 3;
        }
        else
        {
          curr ++;
        }
      }
      break;
    }
  }

  print(data);

}
