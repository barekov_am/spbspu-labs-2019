#include <vector>
#include <string>
#include <iostream>
#include <forward_list>

#include "sort.hpp"
#include "print.hpp"

void task1(const std::string &SortDirect)
{
  auto comparator = getComparator<int>(SortDirect);
  std::vector<int> bracketVector;
  int i = 0;

  while (std::cin >> i, ! std::cin.eof())
  {
    if (! std::cin.eof() && std::cin.fail())
    {
      throw std::runtime_error{"Input error"};
    }
    bracketVector.push_back(i);
  }
  if (bracketVector.empty())
  {
    return;
  }

  std::vector<int> atVector{bracketVector};
  std::forward_list<int> iteratorList{bracketVector.begin(), bracketVector.end()};

  sort<BracketsAccess>(bracketVector, comparator);
  sort<AtAccess>(atVector, comparator);
  sort<IteratorAccess>(iteratorList, comparator);

  print(bracketVector);
  print(atVector);
  print(iteratorList);

}
