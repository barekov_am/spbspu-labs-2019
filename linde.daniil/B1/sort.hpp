#ifndef LABS_LABS_SORT_HPP
#define LABS_LABS_SORT_HPP

#include <iostream>
#include <functional>
#include <algorithm>
#include <stdexcept>

#include "accessWays.hpp"

template<typename T>
std::function<bool(T, T)> getComparator(const std::string &sortDirect)
{
  if (sortDirect == "ascending")
  {
    return std::less<T>();
  }
  if (sortDirect == "descending")
  {
    return std::greater<T>{};
  }
  throw std::invalid_argument{"Incorrect sorting direction."};

}

template<template<class> class accessWay = IteratorAccess, typename Container, class ComparatorType>
void sort(Container &container, ComparatorType comparator)
{
  using AccessWay = accessWay<Container>;
  auto begin = accessWay<Container>::begin(container);
  auto end = accessWay<Container>::end(container);
  for (auto i = begin; i != end; i ++)
  {
    for (auto j = i; j != end; j ++)
    {
      typename Container::value_type& a = AccessWay::get(container, i);
      typename Container::value_type& b = AccessWay::get(container, j);

      if (comparator(b, a))
      {
        std::swap(a, b);
      }
    }
  }
}

#endif
