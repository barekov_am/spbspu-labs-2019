#ifndef LABS_LABS_ACCESSWAYS_HPP
#define LABS_LABS_ACCESSWAYS_HPP

#include <iterator>

template<typename Container>
struct BracketsAccess
{
  typedef typename Container::size_type ContainerIteratorType;
  typedef typename Container::reference ContainerReferenceType;

  static ContainerIteratorType begin(const Container &)
  {
    return 0;
  }

  static ContainerIteratorType end(const Container &container)
  {
    return container.size();
  }

  static ContainerReferenceType get(Container &container, const ContainerIteratorType index)
  {
    return container[index];
  }

};

template<typename Container>
struct AtAccess
{
  typedef typename Container::size_type ContainerIteratorType;
  typedef typename Container::reference ContainerReferenceType;

  static ContainerIteratorType begin(const Container &)
  {
    return 0;
  }

  static ContainerIteratorType end(const Container &container)
  {
    return container.size();
  }

  static ContainerReferenceType get(Container &container, const ContainerIteratorType index)
  {
    return container.at(index);
  }
};

template<typename Container>
struct IteratorAccess
{
  typedef typename Container::iterator ContainerIteratorType;
  typedef typename Container::reference ContainerReferenceType;

  static ContainerIteratorType begin(Container &container)
  {
    return container.begin();
  }

  static ContainerIteratorType end(Container &container)
  {
    return container.end();
  }

  static ContainerReferenceType get( Container &, ContainerIteratorType iterator)
  {
    return *iterator;
  }
};

#endif
