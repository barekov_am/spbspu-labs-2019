#ifndef FACTORIALITERATOR_HPP
#define FACTORIALITERATOR_HPP

#include <iterator>
#include <cassert>

class FactorialIterator: public std::iterator< std::bidirectional_iterator_tag, size_t>{
public:
  FactorialIterator();
  explicit FactorialIterator(const size_t &current);

  FactorialIterator(const FactorialIterator &obj) = default;
  FactorialIterator(FactorialIterator &&obj) = default;

  FactorialIterator& operator=(const FactorialIterator& obj) = default;
  FactorialIterator& operator=(FactorialIterator&& obj) = default;

  bool operator ==(const FactorialIterator & rhs) const noexcept;
  bool operator !=(const FactorialIterator & rhs) const noexcept;

  size_t operator *() const noexcept;
  size_t operator ->() const noexcept;

  FactorialIterator &operator ++() noexcept;
  FactorialIterator operator ++(int) noexcept;

  FactorialIterator &operator --() noexcept;
  FactorialIterator operator --(int) noexcept;

private:
  size_t n_;

  size_t factorial() const;
};

#endif // FACTORIALITERATOR_HPP
