#ifndef TASK1_HPP
#define TASK1_HPP


#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
#include <stdexcept>

#include "bookmarkManager.hpp"

void task1();

void addCommand(BookmarkManager &bookmarkManager, std::istringstream &iss);
void storeCommand(BookmarkManager &bookmarkManager, std::istringstream &iss);
void insertCommand(BookmarkManager &bookmarkManager, std::istringstream &iss);
void deleteCommand(BookmarkManager &bookmarkManager, std::istringstream &iss);
void showCommand(BookmarkManager &bookmarkManager, std::istringstream &iss);
void moveCommand(BookmarkManager &bookmarkManager, std::istringstream &iss);


std::string readMark(std::istringstream &iss);
int getIntSteps(const std::string &steps);
std::string readNumber(std::istringstream &iss);
std::string readName(std::istringstream &iss);
std::string readPlace(std::istringstream &iss);


#endif // TASK1_HPP
