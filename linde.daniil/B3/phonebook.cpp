#include <iostream>

#include "phonebook.hpp"

Phonebook::Phonebook():
  records_({}),
  currentRecord_(records_.begin())
{}

void Phonebook::pushBack(const record_t &record)
{
  records_.push_back(record);
}

std::list<record_t>::iterator Phonebook::begin()
{
  return records_.begin();
}

std::list<record_t>::iterator Phonebook::end()
{
  return --records_.end();
}

void Phonebook::next()
{
  currentRecord_++;
}

void Phonebook::previous()
{
  currentRecord_--;
}

void Phonebook::view() const
{
  if (!records_.empty())
  {
    if (currentRecord_ == records_.end())
    {
      throw std::out_of_range("");
    }

    std::string formatName;

    for (char &sym : currentRecord_->name)
    {
      if (sym != '\\')
      {
        formatName += sym;
      }
    }

    std::cout << currentRecord_->number << " " << formatName << "\n";
  } else
  {
    throw std::out_of_range("<EMPTY>\n");
  }
}

void Phonebook::change(const std::string &number, const std::string &name)
{
  currentRecord_->number = number;
  currentRecord_->name = name;
}

void Phonebook::move(const int &steps)
{
  if (steps > 0){
    for (int i = 0; i < steps; i++)
    {
      Phonebook::next();
    }
  } else
  {
    for (int i = 0; i < -steps; i++)
    {
      Phonebook::previous();
    }
  }
}

void Phonebook::setCurrentRecord(const std::list<record_t>::iterator &iterator)
{
  currentRecord_ = iterator;
}

std::list<record_t>::iterator Phonebook::getCurrentRecord() const
{
  return currentRecord_;
}

void Phonebook::del()
{
  if (records_.empty())
  {
    throw std::logic_error("<EMPTY>\n");
  }

  if (currentRecord_ == Phonebook::end())
  {
    currentRecord_ = Phonebook::begin();
    records_.erase(Phonebook::end());
  } else
  {
    Phonebook::next();
    records_.erase(std::prev(currentRecord_));
  }
}

void Phonebook::insertBefore(const record_t &record)
{
  records_.insert(currentRecord_, record);
}

void Phonebook::insertAfter(const record_t &record)
{
  records_.insert(std::next(currentRecord_), record);
}

record_t Phonebook::getData() const
{
  return (*currentRecord_);
}

bool Phonebook::empty() const
{
  return records_.empty();
}
