#include <algorithm>
#include <iostream>

#include "bookmarkManager.hpp"

BookmarkManager::BookmarkManager():
  book_({}),
  marks_({})
{
  marks_.insert({"current", book_.begin()});
}

void BookmarkManager::add(const record_t &record)
{
  if (book_.empty())
  {
    book_.pushBack(record);
    marks_.find("current")->second = book_.begin();
  } else
  {
    book_.pushBack(record);
  }
}

void BookmarkManager::move(const std::string &markName, const int &steps)
{
  checkMarkName(markName);

  auto iterator = marks_.find(markName);
  if (iterator !=marks_.end())
  {
    book_.setCurrentRecord(iterator->second);
    book_.move(steps);

    iterator->second = book_.getCurrentRecord();
  }
}

void BookmarkManager::show(const std::string &markName)
{
  checkMarkName(markName);
  if (book_.empty())
  {
    std::cout << "<EMPTY>" << std::endl;
    return;
  }

  auto iterator = marks_.find(markName);
  book_.setCurrentRecord(iterator->second);
  book_.view();
}

void BookmarkManager::del(const std::string &markName)
{
  checkMarkName(markName);

  auto iterator = marks_.find(markName);
  auto recordIter = iterator->second;

  book_.setCurrentRecord(recordIter);
  book_.del();

  for (auto &mark : marks_)
  {
    if (mark.second == recordIter)
    {
      mark.second = book_.getCurrentRecord();
    }
  }
}

void BookmarkManager::store(const std::string &markName, const std::string &newMarkName)
{
  if (markName == newMarkName)
  {
    throw std::invalid_argument("<INVALID BOOKMARK>\n");
  }
  checkMarkName(markName);
  marks_.insert({newMarkName, marks_.find(markName)->second});
}

void BookmarkManager::checkMarkName(const std::string &markName) const
{
  if (marks_.count(markName) == 0)
  {
    throw std::invalid_argument("<INVALID BOOKMARK>\n");
  }
}

void BookmarkManager::insertBefore(const std::string &markName, const record_t &record)
{
  if (book_.empty())
  {
    book_.insertBefore(record);
    marks_.find("current")->second = book_.begin();
  } else
  {
    checkMarkName(markName);
    book_.insertBefore(record);
  }
}

void BookmarkManager::insertAfter(const std::string &markName, const record_t &record)
{
  if (book_.empty())
  {
    book_.insertAfter(record);
    marks_.find("current")->second = book_.begin();
  } else
  {
    checkMarkName(markName);
    book_.insertAfter(record);
  }
}

void BookmarkManager::moveToFirst(const std::string &markName)
{
  checkMarkName(markName);

  auto iterator = marks_.find(markName);
  book_.setCurrentRecord(book_.begin());

  iterator->second = book_.getCurrentRecord();
}

void BookmarkManager::moveToLast(const std::string &markName)
{
  checkMarkName(markName);

  auto iterator = marks_.find(markName);
  book_.setCurrentRecord(book_.end());

  iterator->second = book_.getCurrentRecord();
}
