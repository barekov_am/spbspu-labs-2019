#include "task1.hpp"

void task1()
{
  BookmarkManager bookmarkManager;
  std::string currentLine;

  while (std::getline(std::cin, currentLine))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Incorrect input");
    }

    if (currentLine.empty())
    {
      throw std::invalid_argument("Incorrect input");
    }
    std::istringstream iss(currentLine);

    std::string command;
    iss >> std::ws >> command;

    if (command == "add")
    {
      addCommand(bookmarkManager, iss);

    } else if (command == "store")
    {
      storeCommand(bookmarkManager, iss);

    } else if (command == "insert")
    {
      insertCommand(bookmarkManager, iss);

    } else if (command == "delete")
    {
      deleteCommand(bookmarkManager, iss);

    } else if (command == "show")
    {
      showCommand(bookmarkManager, iss);

    } else if (command == "move")
    {
      moveCommand(bookmarkManager, iss);

    } else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}

void addCommand(BookmarkManager &bookmarkManager, std::istringstream &iss)
{
  try
  {
    bookmarkManager.add({readNumber(iss), readName(iss)});
  }
  catch (const std::exception &e)
  {
    std::cout << "<INVALID COMMAND>" << std::endl;
    return;
  }
}

void showCommand(BookmarkManager &bookmarkManager, std::istringstream &iss)
{
  const std::string markName = readMark(iss);
  try
  {
    bookmarkManager.show(markName);
  }
  catch (const std::exception &e)
  {
    std::cout << "<INVALID BOOKMARK>" << std::endl;
    return;
  }
}

void insertCommand(BookmarkManager &bookmarkManager, std::istringstream &iss)
{
  try
  {
    const std::string place = readPlace(iss);

    const std::string mark = readMark(iss);
    const std::string number = readNumber(iss);
    const std::string name = readName(iss);

    if (place == "before")
    {
      bookmarkManager.insertBefore(mark, {number, name});

    } else if (place == "after")
    {
      bookmarkManager.insertAfter(mark, {number, name});

    }
  }
  catch (const std::exception &e)
  {
    std::cout << "<INVALID COMMAND>" << std::endl;
    return;
  }
}


void deleteCommand(BookmarkManager &bookmarkManager, std::istringstream &iss)
{
  const std::string markName = readMark(iss);
  try
  {
    bookmarkManager.del(markName);
  }
  catch (const std::exception &e)
  {
    std::cout << "<INVALID COMMAND>" << std::endl;
    return;
  }
}

void storeCommand(BookmarkManager &bookmarkManager, std::istringstream &iss)
{
  try
  {
    const std::string markName = readMark(iss);
    const std::string newMarkName = readMark(iss);

    bookmarkManager.store(markName, newMarkName);

  }
  catch (const std::exception &e)
  {
    std::cout << "<INVALID COMMAND>" << std::endl;
    return;
  }
}

void moveCommand(BookmarkManager &bookmarkManager, std::istringstream &iss)
{
  const std::string markName = readMark(iss);

  std::string steps;
  iss >> std::ws >>steps;

  try
  {
    if (steps == "first")
    {
      bookmarkManager.moveToFirst(markName);
    } else if (steps == "last")
    {
      bookmarkManager.moveToLast(markName);
    } else {
      const int numSteps = getIntSteps(steps);
      bookmarkManager.move(markName, numSteps);
    }
  }
  catch (const std::exception &e)
  {
    std::cout << "<INVALID STEP>" << std::endl;
    return;
  }
}

std::string readMark(std::istringstream &iss)
{
  std::string markName;
  iss >> std::ws >> markName;
  if (markName.empty())
  {
    throw std::invalid_argument("Invalid bookmark name");
  }
  for (size_t i = 0; i < markName.size(); i++)
  {

    if (!isalnum(markName[i]) && (markName[i] != '-'))
    {
      throw std::invalid_argument("Invalid bookmark name");
    }
  }

  return markName;
}

int getIntSteps(const std::string &steps)
{
  try
  {
    const int numSteps = std::stoi(steps);
    return numSteps;
  }
  catch (const std::invalid_argument &e)
  {
    throw std::invalid_argument("<INVALID STEP>\n");
  }
}

std::string readName(std::istringstream &iss)
{
  iss.ignore();
  std::string name;
  std::getline(iss >> std::ws, name);

  if (name.empty())
    {
      throw std::invalid_argument("Invalid name");
    }

  if (name.front() != '\"')
  {
    throw std::invalid_argument("Invalid name");
  }

  name.erase(name.begin());

  size_t i = 0;
  while ((i < name.size()) && (name[i] != '\"'))
  {
    if (name[i] == '\\')
    {

      if ((name[i + 1] == '\"') && (i + 2 < name.size()))
      {
        name.erase(i, 1);
      } else
      {
        throw std::invalid_argument("Invalid name");
      }
    }
    ++i;
  }

  if (i == name.size())
  {
    throw std::invalid_argument("Invalid name");
  }

  name.erase(i);

  if (name.empty())
  {
    throw std::invalid_argument("Invalid name");
  }

  return name;
}

std::string readNumber(std::istringstream &iss)
{
  std::string number;
  iss >> std::ws >> number;
  if (number.empty())
  {
    throw std::invalid_argument("Invalid number");
  }
  for (auto i : number)
  {
    if (!isdigit(i))
    {
      throw std::invalid_argument("<INVALID COMMAND>\n");
    }
  }
  return number;
}

std::string readPlace(std::istringstream &iss)
{
  std::string place;
  iss >> std::ws >> place;

  if ((place != "before") && (place != "after"))
  {
    throw std::invalid_argument("<INVALID COMMAND>\n");
  }
  return place;
}
