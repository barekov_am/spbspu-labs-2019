#include "factorialContainer.hpp"


FactorialIterator FactorialContainer::begin() const noexcept
{
  return FactorialIterator();
}

FactorialIterator FactorialContainer::end() const noexcept
{
  return FactorialIterator(11);
}
