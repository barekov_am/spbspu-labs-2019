#ifndef B3_BOOKMARKMANAGER_HPP
#define B3_BOOKMARKMANAGER_HPP

#include <map>

#include "phonebook.hpp"

class BookmarkManager
{
public:
  BookmarkManager();

  void add(const record_t &record);

  void insertBefore(const std::string &markName, const record_t &record);
  void insertAfter(const std::string &markName, const record_t &record);

  void store(const std::string &markName, const std::string &newMarkName);

  void del(const std::string &markName);

  void show(const std::string &markName);

  void move(const std::string &markName, const int &steps);
  void moveToFirst(const std::string &markName);
  void moveToLast(const std::string &markName);

private:
  Phonebook book_;
  std::map<std::string, std::list<record_t>::iterator> marks_;

  void checkMarkName(const std::string &markName) const;

};


#endif
