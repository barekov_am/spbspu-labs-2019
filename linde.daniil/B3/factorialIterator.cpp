#include "factorialIterator.hpp"


FactorialIterator::FactorialIterator():
  n_(1)
{}

FactorialIterator::FactorialIterator(const size_t &n):
  n_(n)
{}

bool FactorialIterator::operator==(const FactorialIterator &rhs) const noexcept
{
  return n_ == rhs.n_;
}

bool FactorialIterator::operator!=(const FactorialIterator &rhs) const noexcept
{
  return n_ != rhs.n_ ;
}

size_t FactorialIterator::operator*() const noexcept
{
  return factorial();
}

size_t FactorialIterator::operator->() const noexcept
{
  return factorial();
}

FactorialIterator &FactorialIterator::operator++() noexcept
{
  ++n_;
  return *this;
}

FactorialIterator FactorialIterator::operator++(int) noexcept
{
  FactorialIterator temp = *this;
  ++(*this);
  return temp;
}

FactorialIterator &FactorialIterator::operator--() noexcept
{
 if (n_>0)
 {
  --n_;
 }
  return *this;
}

FactorialIterator FactorialIterator::operator--(int) noexcept
{
  FactorialIterator temp = *this;
  --(*this);
  return temp;
}

size_t FactorialIterator::factorial() const
{
  size_t fact = 1;
  for (size_t i = 1; i <= n_; i++)
  {
    fact = fact * i;
  }
  return fact;
}
