#ifndef B3_PHONEBOOK_HPP
#define B3_PHONEBOOK_HPP

#include <list>
#include <string>

struct record_t
{
  std::string number;
  std::string name;
};

class Phonebook
{

public:
  Phonebook();

  void setCurrentRecord(const std::list<record_t>::iterator &iterator);
  std::list<record_t>::iterator getCurrentRecord() const;

  void next();
  void previous();

  void move(const int &steps);

  void pushBack(const record_t &record);
  void insertAfter(const record_t &record);
  void insertBefore(const record_t &record);

  void view() const;
  void change(const std::string &number, const std::string &name);
  void del();
  bool empty() const;

  record_t getData() const;

  std::list<record_t>::iterator begin();
  std::list<record_t>::iterator end();

private:
  std::list<record_t> records_;
  std::list<record_t>::iterator currentRecord_;
};


#endif
