#ifndef B3_FACTORIALCONTAINER_HPP
#define B3_FACTORIALCONTAINER_HPP

#include "factorialIterator.hpp"

class FactorialContainer{
public:
  FactorialContainer() = default;

  FactorialIterator begin() const noexcept ;
  FactorialIterator end() const noexcept ;
};


#endif
