#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>

Rectangle::Rectangle(double setWidth, double setHeight, const point_t &setCenter) :
  width_(setWidth),
  height_(setHeight),
  center_(setCenter)
{
  if ((width_ <= 0) && (height_ <= 0))
  {
    throw std::invalid_argument("Invalid parameters");
  }
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return rectangle_t{ width_, height_, center_ };
}

void Rectangle::move(const point_t & point)
{
  center_ = point;
}

void Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Rectangle::printInfo() const
{
  const rectangle_t framerect = getFrameRect();
  std::cout << "Rectangle:"<< std::endl;
  std::cout << "  Width: " << width_ << std::endl;
  std::cout << "  Height: " << height_ << std::endl;
  std::cout << "  Center coordinates for the rectangle: " << std::endl;
  std::cout << "    x: " << center_.x << std::endl;
  std::cout << "    y: " << center_.y << std::endl;
  std::cout << "  Frame rectangle for the rectangle: " << std::endl;
  std::cout << "    Width: " << framerect.width << std::endl;
  std::cout << "    Height: " << framerect.height << std::endl;
  std::cout << "  Area: " << getArea() << std::endl;
}
