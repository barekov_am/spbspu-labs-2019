#define _USE_MATH_DEFINES
#include "circle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>

Circle::Circle(double radius, const point_t & center) :
  radius_(radius),
  center_(center)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("radius must be > 0");
  }
}

double Circle::getArea() const
{
  return radius_ * radius_ * M_PI;
}

rectangle_t Circle::getFrameRect() const
{
  return rectangle_t{ radius_ * 2, radius_ * 2, center_ };
}

void Circle::move(const point_t & point)
{
  center_ = point;
}

void Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Circle::printInfo() const
{
  const rectangle_t framerect = getFrameRect();
  std::cout << "Circle:" << std::endl;
  std::cout << "  Radius for the circle: " << radius_ << std::endl;
  std::cout << "  Center coordinates for the circle: " << std::endl;
  std::cout << "    x: " << center_.x << std::endl;
  std::cout << "    y: " << center_.y << std::endl;
  std::cout << "  Frame rectangle for the circle: " << std::endl;
  std::cout << "    Width: " << framerect.width << std::endl;
  std::cout << "    Height: " << framerect.height << std::endl;
  std::cout << "  Area: " << getArea() << std::endl;
}
