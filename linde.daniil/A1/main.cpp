#include <iostream>
#include <cassert>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void demonstrateShape(Shape * shape_test_)
{
  assert(shape_test_ != nullptr);
  shape_test_->printInfo();
  std::cout << "Move the shape dx=3.0,dy=3.0;" << std::endl;
  shape_test_->move(3.0, 3.0);
  shape_test_->printInfo();
  std::cout << "Move the shape to point (5.0, 5.0)." << std::endl;
  shape_test_->move({ 5.0, 5.0 });
  shape_test_->printInfo();
  std::cout << std::endl;
}

int main()
{
  Rectangle rectangleTest = { 5.0, 5.0, { 1.0, 1.0 } };
  Shape * shapetest_ = & rectangleTest;
  std::cout << "First shape:" << std::endl;
  demonstrateShape(shapetest_);

  Circle circleTest = { 1.0, { 1.0, 1.0 } };
  shapetest_ = & circleTest;
  std::cout << "Second shape: " << std::endl;
  demonstrateShape(shapetest_);

  Triangle triangleTest = { { 13, 42 }, { 31, 28 }, { 25, 20 } };
  shapetest_ = & triangleTest;
  std::cout << "Third shape:" << std::endl;
  demonstrateShape(shapetest_);

  const point_t apexes[5] = { { 1.0, 2 }, { 3.2, 2 }, { 4, 3.75 }, { 3, 6.3 }, { 0.9, 6 } };
  Polygon polygoneTest = { 5, apexes };
  shapetest_ = & polygoneTest;
  std::cout << "Fourth shape:" << std::endl;
  demonstrateShape(shapetest_);

  return 0;
}
