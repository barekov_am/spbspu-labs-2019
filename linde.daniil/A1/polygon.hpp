#ifndef POLYGON_HPP
#define POLYGON_HPP
#include "shape.hpp"

class Polygon : public Shape
{
public: 
  Polygon(const Polygon & other);
  Polygon(Polygon && other);
  Polygon(const int number_of_points, const point_t * point);
  ~Polygon() override;
  Polygon & operator = (const Polygon & other);
  Polygon & operator = (Polygon && other);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t & point) override;
  void move(const double dx,const double dy) override;
  void printInfo() const override;  
private:
  point_t * points_;
  int number_point_;
  point_t getCenter() const;
};

#endif
