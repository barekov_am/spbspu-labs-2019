#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

Triangle::Triangle(const point_t & point_a, const point_t & point_b, const point_t & point_c) :
 points_{ point_a, point_b, point_c},
 center_({ (point_a.x + point_b.x + point_c.x) / 3 , (point_a.y + point_b.y + point_c.y) / 3 })
{
  if (getArea() <= 0)
  {
    throw std::invalid_argument("Points of the triangle must be different!");
  }
}

double Triangle::getArea() const
{
  return ((std::fabs(((points_[0].x - points_[2].x) * (points_[1].y - points_[2].y))
      - ((points_[0].y - points_[2].y) * (points_[1].x - points_[2].x))) / 2));
}

rectangle_t Triangle::getFrameRect() const
{
  const double min_x = std::min(std::min(points_[0].x, points_[1].x), points_[2].x);
  const double min_y = std::min(std::min(points_[0].y, points_[1].y), points_[2].y);
  const double h = std::max(std::max(points_[0].y, points_[1].y), points_[2].y) - min_y;
  const double w = std::max(std::max(points_[0].x, points_[1].x), points_[2].x) - min_x;
  return rectangle_t{ w, h, { (std::max(points_[0].x, points_[1].x) + min_x) / 2,
      (std::max(points_[0].y, points_[1].y) + min_y) / 2 } };
}

void Triangle::move(const point_t & point)
{
  const double dx = point.x - center_.x;
  const double dy = point.y - center_.y;
  move(dx, dy);
}

void Triangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
  for (int i = 0; i < 3; i++)
  {
    points_[i].x += dx;
    points_[i].y += dy;
  }
}

void Triangle::printInfo() const
{
  const rectangle_t framerect = getFrameRect();
  std::cout << "Triangle:"<< std::endl;
  std::cout << "  The apex coordinates of the triangle: " << std::endl;
  std::cout << "    A (" << points_[0].x << "," << points_[0].y << ")" << std::endl;
  std::cout << "    B (" << points_[1].x << "," << points_[1].y << ")" << std::endl;
  std::cout << "    C (" << points_[2].x << "," << points_[2].y << ")" << std::endl;
  std::cout << "  The Center coordinates of the triangle: " << std::endl;
  std::cout << "    x: " << center_.x << std::endl;
  std::cout << "    y: " << center_.y << std::endl;
  std::cout << "  Frame rectangle for the triangle: " << std::endl;
  std::cout << "    Width: " << framerect.width << std::endl;
  std::cout << "    Height: " << framerect.height << std::endl;
  std::cout << "  Area: " << getArea() << std::endl;
}
