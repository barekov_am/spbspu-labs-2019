#include "polygon.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>


Polygon::Polygon(const Polygon & other) :
  points_(new point_t[other.number_point_]),
  number_point_(other.number_point_)
{
  for (int i = 0; i < number_point_; i++)
  {
    points_[i] = other.points_[i];
  }
}

Polygon::Polygon(Polygon && other) :
  points_(other.points_),
  number_point_(other.number_point_)
{
  other.points_ = nullptr;
  other.number_point_ = 0;
}

Polygon::Polygon(const int number_of_points, const point_t * point):
  points_(new point_t[number_of_points]),
  number_point_(number_of_points)
{
  if (number_point_ < 3)
  {
    delete [] points_;
    throw std::invalid_argument("Count of points must be >= 3");
  }
  if (point == nullptr)
  {
    delete [] points_;
    throw std::invalid_argument("Pointer to points mustn't be null");
  }
  for (int i = 0; i < number_point_; i++)
  {
    if (((point[(i + 1) % number_point_].x - point[i % number_point_].x)
        * (point[(i + 2) % number_point_].y - point[(i + 1) % number_point_].y)
          - (point[(i + 2) % number_point_].x - point[(i + 1) % number_point_].x)
            * (point[(i + 1) % number_point_].y - point[i % number_point_].y)) <= 0)
    {
      delete [] points_;
      throw std::invalid_argument("Polygon must be convex");
    }
    points_[i] = point[i];
  }
  if (getArea() <= 0)
  {
    delete [] points_;
    throw std::invalid_argument("Points must be different");
  }
}

Polygon::~Polygon()
{
  delete[] points_;
}

Polygon & Polygon::operator =(const Polygon & other)
{
  if (this == & other)
  {
    return * this;
  }
  number_point_ = other.number_point_;
  delete[] points_;
  points_ = new point_t[number_point_];
  for (int i = 0; i < number_point_; i++)
  {
    points_[i] = other.points_[i];
  }
  return * this;
}

Polygon & Polygon::operator =(Polygon && other)
{
  if (this == & other)
  {
    return * this;
  }
  number_point_ = other.number_point_;
  other.number_point_ = 0;
  delete[] points_;
  points_ = other.points_;
  other.points_ = nullptr;
  return * this;
}

double Polygon::getArea() const
{
  double sum_pos = 0;
  double sum_neg = 0;
  for (int i = 0; i < number_point_ - 1; i++)
  {
    sum_pos += points_[i].x * points_[i + 1].y;
    sum_neg += points_[i + 1].x * points_[i].y;
  }
  return (std::fabs(sum_pos + points_[number_point_ - 1].x * points_[0].y - sum_neg
      - points_[0].x * points_[number_point_ - 1].y) / 2);
}

rectangle_t Polygon::getFrameRect() const
{
  double min_x = points_[0].x;
  double max_x = points_[0].x;
  double min_y = points_[0].y;
  double max_y = points_[0].y;
  for (int i = 1; i < number_point_; i++)
  {
    min_x = std::min(min_x, points_[i].x);
    max_x = std::max(max_x, points_[i].x);
    min_y = std::min(min_y, points_[i].y);
    max_y = std::max(max_y, points_[i].y);
  }
  double w = max_x - min_x;
  double h = max_y - min_y;
  point_t pos = { (max_x + min_x) / 2, (max_y + min_y) / 2 };
  return rectangle_t{ w, h, pos };
}

void Polygon::move(const point_t & point)
{
  const double dx = point.x - getCenter().x;
  const double dy = point.y - getCenter().y;
  move(dx, dy);
}

void Polygon::move(const double dx, const double dy)
{
  for (int i = 0; i < number_point_; i++)
  {
    points_[i].x += dx;
    points_[i].y += dy;
  }
}

void Polygon::printInfo() const
{
  const rectangle_t framerect = getFrameRect();
  const point_t center = getCenter();
  std::cout << "Polygon:"<< std::endl;
  std::cout << "  The apex coordinates of the Polygon: " << std::endl;
  for (int i = 0; i < number_point_; i++)
  {
    std::cout << "    Apex " << i << ": x = " << points_[i].x << " y = " << points_[i].y << std::endl;
  }
  std::cout << "  The Center coordinates of the Polygon: " << std::endl;
  std::cout << "    x: " << center.x << std::endl;
  std::cout << "    y: " << center.y << std::endl;
  std::cout << "  Frame rectangle for the Polygon: " << std::endl;
  std::cout << "    Width: " << framerect.width << std::endl;
  std::cout << "    Height: " << framerect.height << std::endl;
  std::cout << "  Area: " << getArea() << std::endl;
}

point_t Polygon::getCenter() const
{
  double sumx = 0;
  double sumy = 0;
  for (int i = 0; i < number_point_; i++)
  {
    sumx += points_[i].x;
    sumy += points_[i].y;
  }
  return point_t { sumx / number_point_, sumy / number_point_ };
}
