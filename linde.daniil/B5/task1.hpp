#ifndef TASK1_HPP
#define TASK1_HPP

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <set>

void task1();

std::set<std::string> readData();


#endif // TASK1_HPP
