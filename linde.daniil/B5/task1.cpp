#include "task1.hpp"

void task1()
{
  std::set<std::string> listofwords = readData();

  for (const auto &it : listofwords)
  {
    std::cout << it << "\n";
  }
};

std::set<std::string> readData()
{
  std::string currentLine;
  std::set<std::string> listofwords;

  while (std::getline(std::cin, currentLine))
  {
    std::istringstream iss(currentLine);
    std::string newword;
    while (iss >> std::ws >> newword)
    {
      listofwords.insert(newword);
    }
  }
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Error reading the data");
  }
  return listofwords;
}
