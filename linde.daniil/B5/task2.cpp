#include "task2.hpp"


void task2()
{
  std::vector < Shape > vectorofshapes;

  std::transform(std::istream_iterator<Line>(std::cin),
    std::istream_iterator<Line>(), std::back_inserter(vectorofshapes), readData);

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Error reading the data");
  }

  const int numvertices = numberofVertices(vectorofshapes);
  std::cout << "Vertices: " << numvertices << "\n";

  const size_t numtriangles = numberofTriangles(vectorofshapes);
  std::cout << "Triangles: " << numtriangles << "\n";

  const size_t numsquares = numberofSquares(vectorofshapes);
  std::cout << "Squares: " << numsquares << "\n";

  const size_t numrectangles = numberofRectangles(vectorofshapes);
  std::cout << "Rectangles: " << numrectangles << "\n";

  removePentagon(vectorofshapes);

  const std::vector< Point > pointsVector = createPointsVector(vectorofshapes);
  std::cout << "Points:";
  for (auto it : pointsVector)
  {
    std::cout << " (" << it.x << ";" << it.y << ")";
  }

  auto endoftriangle = std::partition(vectorofshapes.begin(), vectorofshapes.end(), checkTriangle);
  auto endofsquare = std::partition(endoftriangle, vectorofshapes.end(), checkSquare);
  std::partition(endofsquare, vectorofshapes.end(), checkRectangle);

  std::cout << "\nShapes:\n";
  for (auto &it : vectorofshapes)
  {
    std::cout << it.size() ;
    for (auto &sIt : it)
    {
      std::cout << " (" << sIt.x << ";" << sIt.y << ")";
    }
    std::cout << "\n";
  }

};

std::istream& operator>>(std::istream &in, Line &line)
{
  while(std::getline(in, line))
  {
    if(line.empty())
    {
      continue;
    }
    if(std::find_if(line.begin(), line.end(),[](char c) {
      return !std::isspace(c, std::locale());}) != line.end())
    {
      break;
    }
  }
  return in;
}

Shape readData(const Line &line)
{
  std::stringstream iss(line);

  int vertexnumber;
  iss >> std::ws >> vertexnumber;

  Shape newshape;
  const size_t lineLength = line.length();
  for(int i = 0; i < vertexnumber; i++)
  {
    Point newpoint{0, 0};
    iss.ignore(lineLength, '(');
    iss >> newpoint.x;

    iss.ignore(lineLength, ';');
    iss >> newpoint.y;

    iss.ignore(lineLength, ')');
    newshape.push_back(newpoint);
  }

  if(iss.fail())
  {
    throw std::invalid_argument("Invalid value of points;\n");
  }

  std::string rmLine;
  std::getline(iss >> std::ws, rmLine);
  if(std::find_if(rmLine.begin(), rmLine.end(), [](char c){return !std::isspace(c, std::locale()); }) != rmLine.end())
  {
    throw std::invalid_argument("Invalid input data;\n");
  }
  return newshape;
}

int numberofVertices(const std::vector<Shape> &vector)
{
  const int num = std::accumulate(vector.begin(), vector.end(), 0, []
    (int verticesNum, const Shape &shape) {return verticesNum + shape.size();});
  return num;
};

size_t numberofTriangles(const std::vector < Shape > &vector)
{
  return static_cast<size_t >(std::count_if(vector.begin(), vector.end(), checkTriangle));
};

size_t numberofSquares(const std::vector < Shape > &vector)
{
  return static_cast<size_t >(std::count_if(vector.begin(), vector.end(), checkSquare));
};

size_t numberofRectangles(const std::vector < Shape > &vector)
{
  return static_cast<size_t >(std::count_if(vector.begin(), vector.end(), checkRectangle));
};

bool checkSquare(const Shape &shape)
{
  //bool checkSquare = false;
  if (shape.size() == 4)
  {
    const int maxX = (std::max_element(shape.begin(), shape.end(), compareXcoordinate))->x;
    const int minX = (std::min_element(shape.begin(), shape.end(), compareXcoordinate))->x;

    const int maxY = (std::max_element(shape.begin(), shape.end(), compareYcoordinate))->y;
    const int minY = (std::min_element(shape.begin(), shape.end(), compareYcoordinate))->y;

    if ((maxX - minX) == (maxY - minY))
    {
      return true;
    } else
    {
       return false;
    }
  } else
  {
    return false;
  }
};

bool checkRectangle(const Shape &shape)
{
  if (shape.size() == 4)
  {
    return true;
  } else
  {
    return false;
  }
}

bool checkTriangle(const Shape &shape)
{
  if(shape.size() == 3)
  {
    return true;
  } else
  {
    return false;
  }
}

void removePentagon(std::vector < Shape > &vector)
{
  vector.erase(std::remove_if(vector.begin(), vector.end(), []
    (const Shape &sh){return sh.size() == 5;}), vector.end());
}

std::vector< Point > createPointsVector(const std::vector < Shape > &vectorofshapes)
{
  std::vector< Point > pointsVector;

  pointsVector.reserve(vectorofshapes.size());
  for (const auto &it : vectorofshapes)
  {
    pointsVector.push_back(it.at(0));
  }

  return pointsVector;
}

bool compareXcoordinate(const Point &tpoint1, const Point &tpoint2)
{
  return tpoint1.x < tpoint2.x;
}

bool compareYcoordinate(const Point &tpoint1, const Point &tpoint2)
{
  return tpoint1.y < tpoint2.y;
}
