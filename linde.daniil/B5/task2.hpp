#ifndef TASK2_HPP
#define TASK2_HPP


#include <vector>
#include <string>
#include <sstream>
#include <locale>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <iterator>



struct Point
{
  int x;
  int y;
};

class Line : public std::string
{
  friend std::istream& operator>>(std::istream &in, Line &line);
};

std::istream& operator>>(std::istream &in, Line &line);

using Shape = std::vector< Point >;

void task2();

Shape readData(const Line &line);

int numberofVertices(const std::vector < Shape > &vector);
size_t numberofTriangles(const std::vector < Shape > &vector);
size_t numberofSquares(const std::vector < Shape > &vector);
size_t numberofRectangles(const std::vector < Shape > &vector);
void removePentagon(std::vector < Shape > &vector);
std::vector< Point > createPointsVector(const std::vector < Shape > &vector);

bool  checkTriangle(const Shape &shape);
bool checkRectangle(const Shape &shape);
bool checkSquare(const Shape &shape);

bool compareXcoordinate(const Point &point1, const Point &point2);
bool compareYcoordinate(const Point &point1, const Point &point2);



#endif // TASK2_HPP
