#ifndef TASK2_HPP
#define TASK2_HPP

#include <list>
#include <iostream>

void task2();
void print(std::list<int>::iterator leftPos, std::list<int>::iterator rightPos, std::list<int>::iterator begin);

#endif // TASK2_HPP
