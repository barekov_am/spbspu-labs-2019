#ifndef B2_QUEUE_HPP
#define B2_QUEUE_HPP

#include <string>
#include <list>
#include <stdexcept>

typedef enum
{
  LOW,
  NORMAL,
  HIGH
} ElementPriority;


template <typename T>
class QueueWithPriority
{
public:
  QueueWithPriority() = default;
  virtual ~QueueWithPriority() = default;

  void putElementToQueue(const T & element, ElementPriority priority);
  T getElementFromQueue();

  void accelerate();

private:
  std::list<T> queue_[3];

  ElementPriority getPriority() const;
};

#endif //B2_QUEUE_HPP
