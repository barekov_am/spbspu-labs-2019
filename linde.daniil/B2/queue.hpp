#ifndef QUEUE_HPP
#define QUEUE_HPP
#include "queueWithPriority.hpp"

template <typename T>
void QueueWithPriority<T>::putElementToQueue(const T &element, ElementPriority priority)
{
  queue_[priority].push_back(element);
}

template <typename T>
ElementPriority QueueWithPriority<T>::getPriority() const
{
  if (queue_[HIGH].size() != 0)
  {
    return HIGH;
  } else if (queue_[NORMAL].size() != 0)
  {
    return NORMAL;
  } else if (queue_[LOW].size() != 0)
  {
    return LOW;
  } else
  {
    throw std::length_error("<EMPTY>\n");
  }
}

template <typename T>
T QueueWithPriority<T>::getElementFromQueue()
{
  ElementPriority priority = HIGH;
  try
  {
    priority = getPriority();
  }
  catch (const std::length_error & e)
  {
    throw e;
  }
  const T data = queue_[priority].front();
  queue_[priority].pop_front();

  return data;
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  queue_[HIGH].splice(queue_[HIGH].end(), queue_[LOW]);
}

#endif // QUEUE_HPP
