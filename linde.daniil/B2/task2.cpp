#include <iostream>
#include <list>
#include "task2.hpp"

void print(const std::list<int> & list)
{
  for (auto it1 = list.begin(), it2 = list.end(); it1 != it2--; it1++)
  {
    if (it1 == it2)
    {
      std::cout << *it1 << "\n";
      return;
    }
    std::cout << *it1 << " " << *it2 << " ";
  }
  std::cout << "\n";
}


void task2()
{
  std::list<int> list;

  int number = 0;
  while (std::cin >> number)
  {
    if ((number < 1) || (number > 20))
    {
      throw std::out_of_range("Numbers must be from 1 to 20");
    }
    list.push_back(number);
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::invalid_argument("Incorrect input data;");
  }

  if (list.size() > 20)
  {
    throw std::invalid_argument("Quantity of elements must be less than 20;");
  }

  print(list);
}
