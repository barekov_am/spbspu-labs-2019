#include "task1.hpp"

void task1()
{
  QueueWithPriority<std::string> queueWithPriority;

  std::string currentLine(" ");
  while (std::getline(std::cin, currentLine))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Incorrect input");
    }

    if (currentLine.empty())
    {
      throw std::invalid_argument("Incorrect input");
    }
    std::istringstream iss(currentLine);

    std::string command;
    iss >> std::ws >> command;
    if (command == "add")
    {
      addCommand(iss, queueWithPriority);
    } else if (command == "get")
    {
      getCommand(queueWithPriority);
    } else if (command == "accelerate")
    {
      accelerateCommand(queueWithPriority);
    } else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}

void addCommand(std::istringstream & iss, QueueWithPriority<std::string> & queueWithPriority)
{
  std::string priority;
  iss >> std::ws >> priority;

  ElementPriority elementPriority;
  if (priority == "low")
  {
    elementPriority = LOW;
  } else if (priority == "normal")
  {
    elementPriority = NORMAL;
  } else if (priority == "high")
  {
    elementPriority = HIGH;
  } else
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::string element;
  iss.get();
  std::getline(iss >> std::ws, element);

  if (element.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queueWithPriority.putElementToQueue(element, elementPriority);
}

void getCommand(QueueWithPriority<std::string> & queueWithPriority)
{
  try
  {
    std::cout << queueWithPriority.getElementFromQueue() << "\n";
  }
  catch (const std::length_error & e)
  {
    std::cout << e.what();
  }
}

void accelerateCommand(QueueWithPriority<std::string> & queueWithPriority)
{
  queueWithPriority.accelerate();
}
