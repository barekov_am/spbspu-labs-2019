#ifndef TASK1_HPP
#define TASK1_HPP

#include <iostream>
#include <sstream>
#include "queue.hpp"
#include <stdexcept>

void task1();

void addCommand(std::istringstream & iss, QueueWithPriority<std::string> & queueWithPriority);
void getCommand(QueueWithPriority<std::string> & queueWithPriority);
void accelerateCommand(QueueWithPriority<std::string> & queueWithPriority);

#endif // TASK1_HPP
