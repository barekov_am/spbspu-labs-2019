#include <iostream>
#include <iomanip>
#include <iterator>
#include <algorithm>
#include "functor.hpp"

int main()
{
  try
  {
    std::istream_iterator<long long int> end;
    std::istream_iterator<long long int> start(std::cin);

    Functor m_functor = for_each(start, end, Functor());

    if (!std::cin.eof() && (std::cin.fail() || std::cin.bad()))
    {
      std::cout << "Incorrect input";
      return 1;
    }

    if (m_functor.countAll() == 0)
    {
      std::cout << "No Data\n";
    }
    else
    {
      std::cout << "Max: " << m_functor.minNumber() << '\n';
      std::cout << "Min: " << m_functor.maxNumber() << '\n';
      std::cout.flags(std::ios::fixed);
      std::cout << "Mean: " << m_functor.averageNumber() << '\n';
      std::cout << "Positive: " << m_functor.countPositive()<< '\n';
      std::cout << "Negative: " << m_functor.countNegative() << '\n';
      std::cout << "Odd Sum: " << m_functor.oddSum() << '\n';
      std::cout << "Even Sum: " << m_functor.evenSum() << '\n';
      std::cout << "First/Last Equal: " << (m_functor.compareFirstandLast() ? "yes" : "no") << '\n';
    }
  }
  catch (const std::exception & e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}
