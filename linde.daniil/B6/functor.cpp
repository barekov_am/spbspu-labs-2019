#include "functor.hpp"

void Functor::operator()(long long int number)
{
  if (count == 0)
  {
    first = number;
    max = number;
    min = number;
  }

  if (number % 2 == 0)
  {
    even_sum += number;
  }
  else
  {
    odd_sum += number;
  }

  if (number > max)
  {
    max = number;
  }

  if (number < min)
  {
    min = number;
  }

  if (number > 0)
  {
    positive++;
  }
  else
  {
    if (number != 0)
    {
      negative++;
    }
  }

  count++;
  last = number;
}

long long int Functor::minNumber() const
{
  return max;
}

long long int Functor::maxNumber() const
{
  return min;
}

long double Functor::averageNumber() const
{
  return static_cast<double> (odd_sum + even_sum) / static_cast<double> (count);
}

long long int Functor::countPositive() const
{
  return positive;
}

long long int Functor::countNegative() const
{
  return negative;
}

long long int Functor::evenSum() const
{
  return even_sum;
}

long long int Functor::oddSum() const
{
  return odd_sum;
}

bool Functor::compareFirstandLast() const
{
  return first==last;
}

long long int Functor::countAll() const
{
  return count;
}
