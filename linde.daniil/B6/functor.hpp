#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP


class Functor
{
public:
  void operator()(long long int value);
  long long int minNumber() const;
  long long int maxNumber() const;
  long double averageNumber() const;
  long long int countPositive() const;
  long long int countNegative() const;
  long long int evenSum() const;
  long long int oddSum() const;
  long long int countAll() const;
  bool compareFirstandLast() const;

private:
  long long int max;
  long long int min;
  long long int count;
  long long int positive;
  long long int negative;
  long long int odd_sum;
  long long int even_sum;
  long long int first;
  long long int last;
};


#endif // FUNCTOR_HPP
