#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <iostream>
#include <memory>
#include <sstream>


class Shape
{
public:
  int x_0,y_0;
  bool correct;
  virtual ~Shape(){};
  Shape(int& x,int& y):x_0(x),y_0(y) {}
  Shape():x_0(0),y_0(0) {}
  bool isLeft(const Shape& sh) const;
  bool isHigher(const Shape& sh) const;
  virtual std::ostream& draw(std::ostream& os) = 0;
};

struct ForShape
{
  int x,y;
  std::string name;
};

bool compLeft(std::shared_ptr<Shape> one, std::shared_ptr<Shape> two);
bool compUp(std::shared_ptr<Shape> one, std::shared_ptr<Shape> two);

#endif // SHAPE_HPP
