#include "task.hpp"


int main(int argc,char** argv)
{
  try
  {
    if(argc!=2)
    {
      std::cerr<<"Invalid number of task!";
      return 1;
    }

    int numoftask=atoi(argv[1]);

    switch (numoftask)
    {
      case 1:
      {
        std::istream_iterator<std::string> end;
        std::istream_iterator<std::string> start(std::cin);
        std::for_each(start,end,multiplyByPi);
        break;
      }

      case 2:
      {
        task2();
        break;
      }

      default:
      {
        std::cerr<<"Incorrect task number";
        return 1;
      }
    }
  }
  catch (const std::exception& exception)
  {
    std::cerr << exception.what() << "\n";
    return 1;
  }

  return 0;
}
