#ifndef TASK_HPP
#define TASK_HPP
#include <string>
#include <algorithm>
#include <iterator>
#include <deque>
#include <cmath>
#include "figures.hpp"
#define _USE_MATH_DEFINES

void multiplyByPi(std::string value);
std :: ostream& operator << (std::ostream& os, Shape* s);
std::istream& deleteSpaces(std::istream& in);
std::shared_ptr<Shape> readShapeinfo();
void task2();

#endif // TASK_HPP
