#ifndef FIGURES_HPP
#define FIGURES_HPP

#include "shape.hpp"

const std::string circle="CIRCLE";
const std::string square="SQUARE";
const std::string triangle="TRIANGLE";

class Circle final:public Shape
{
public:
  Circle() : Shape() {}
  Circle(int& x, int& y) : Shape(x,y) {}
  std::ostream& draw(std::ostream& os)
  {
    os << "CIRCLE (" << x_0 << ";" << y_0 << ")";
    return os;
  }
};


class Square final:public Shape
{
public:
  Square() : Shape() {}
  Square(int& x, int& y) : Shape(x,y) {}
  std::ostream& draw(std::ostream& os)
  {
    os << "SQUARE (" << x_0 << ";" << y_0 << ")";
    return os;
  }
};


class Triangle final:public Shape
{
public:
  Triangle() : Shape() {}
  Triangle(int& x, int& y) : Shape(x,y) {}
  std::ostream& draw(std::ostream& os)
  {
    os << "TRIANGLE (" << x_0 << ";" << y_0 << ")";
    return os;
  }
};


#endif // FIGURES_HPP
