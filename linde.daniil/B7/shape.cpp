#include "shape.hpp"

bool Shape::isLeft(const Shape &sh) const
{
  return x_0 < sh.x_0;
}

bool Shape::isHigher(const Shape &sh) const
{
  return y_0 > sh.y_0;
}

bool compLeft(std::shared_ptr<Shape> one, std::shared_ptr<Shape> two)
{
  return one->isLeft(*two);
}

bool compUp(std::shared_ptr<Shape> one, std::shared_ptr<Shape> two)
{
  return one->isHigher(*two);
}
