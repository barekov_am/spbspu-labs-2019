#include "task.hpp"

void multiplyByPi(std::string value)
{
  char * error;
  double numval = strtod(value.c_str(), &error);

  if (*error)
  {
    std::cerr <<"Wrong number" <<std::endl;
    exit(1);
  }

  std::cout<< numval*M_PI <<std::endl;
}

std :: ostream& operator << (std::ostream& os, Shape* s)
{
  s->draw(os);
  return os;
}

std::istream& deleteSpaces(std::istream& in)
{
  while(std::isblank(in.peek()))
  {
    in.get();
  }

  return in;
}

std::shared_ptr<Shape> readShapeinfo()
{
  int x, y;
  char symb;
  ForShape new_;
  new_.name = "";

  std::cin >> std::ws >> symb;

  if(std::cin.eof())
  {
    return  nullptr;
  }

  while (std::isalpha(std::cin.peek()))
  {
    new_.name += symb;
    std::cin >> std::ws >> symb;
  }
  new_.name+=symb;
  if (new_.name.empty())
  {
    std::invalid_argument("No name");
  }
  std::cin >> std::noskipws;

  char leftBr = 0;
  char delim = 0;
  char rightBr = 0;

  std::cin >> deleteSpaces >> leftBr
           >> deleteSpaces >> x
           >> deleteSpaces >> delim
           >> deleteSpaces >> y
           >> deleteSpaces >> rightBr;

  if ((leftBr != '(') || (delim != ';') || (rightBr != ')'))
  {
    throw std::runtime_error("Invalid input");
  }
  new_.x = x;
  new_.y = y;

  std::shared_ptr<Shape> new_shape;
  if (new_.name == circle)
  {
    new_shape = std::make_shared<Circle>(new_.x, new_.y);
  }
  else if (new_.name == triangle)
  {
    new_shape = std::make_shared<Triangle>(new_.x, new_.y);
  }
  else if (new_.name == square)
  {
    new_shape = std::make_shared<Square>(new_.x, new_.y);
  }
  else if (new_.name != square && new_.name != triangle && new_.name != circle)
  {
    std::runtime_error("Invalid input");
  }
  return new_shape;
}


void task2()
{
  std::deque<std::shared_ptr<Shape>> my_shapes;

  while(!std::cin.eof())
  {
    std::shared_ptr<Shape> new_shape = readShapeinfo();
    if (new_shape)
    {
      my_shapes.push_back(new_shape);
    }
  }


  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Reading failed.");
  }

  std::cout << "Original:\n";
  std::copy(my_shapes.begin(),my_shapes.end(),std::ostream_iterator<std::shared_ptr<Shape>>(std :: cout, "\n"));
  std::cout<<"Left-Right:\n";
  std::sort(my_shapes.begin(),my_shapes.end(),compLeft);
  std::copy(my_shapes.begin(),my_shapes.end(),std::ostream_iterator<std::shared_ptr<Shape>>(std :: cout, "\n"));
  std::cout<<"Right-Left:\n";
  std::sort(my_shapes.rbegin(),my_shapes.rend(),compLeft);
  std::copy(my_shapes.begin(),my_shapes.end(),std::ostream_iterator<std::shared_ptr<Shape>>(std :: cout, "\n"));
  std::cout<<"Top-Bottom:\n";
  std::sort(my_shapes.begin(),my_shapes.end(),compUp);
  std::copy(my_shapes.begin(),my_shapes.end(),std::ostream_iterator<std::shared_ptr<Shape>>(std :: cout, "\n"));
  std::cout<<"Bottom-Top:\n";
  std::sort(my_shapes.rbegin(),my_shapes.rend(),compUp);
  std::copy(my_shapes.begin(),my_shapes.end(),std::ostream_iterator<std::shared_ptr<Shape>>(std :: cout, "\n"));
}
