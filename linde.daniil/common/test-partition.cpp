#include <boost/test/auto_unit_test.hpp>
#include "partition.hpp"
#include "matrix.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testPartition)

BOOST_AUTO_TEST_CASE(testResultOfPartition)
{
  linde::Rectangle testRect1({ 1.0, 2.0, {0.0, 0.0} });
  linde::Circle testCircle(1.0, { 1.0, 0.0 });
  linde::Rectangle testRect2({ 1.0, 1.0, {4.0, 5.0} });
  std::shared_ptr<linde::Shape> pointerRect1 = std::make_shared<linde::Rectangle>(testRect1);
  std::shared_ptr<linde::Shape> pointerCircle = std::make_shared<linde::Circle>(testCircle);
  std::shared_ptr<linde::Shape> pointerRect2 = std::make_shared<linde::Rectangle>(testRect2);

  linde::CompositeShape testCompShape;
  testCompShape.add(pointerRect1);
  testCompShape.add(pointerCircle);
  testCompShape.add(pointerRect2);

  linde::Matrix testMatrix = linde::partition(testCompShape);

  BOOST_CHECK_EQUAL(testMatrix.getSize(), 3);
  BOOST_CHECK_EQUAL(testMatrix.getRows(), 2);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(0), 2);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(1), 1);
  BOOST_CHECK_EQUAL(testMatrix.getCountElBehind(1), 2);

  BOOST_CHECK_EQUAL(testMatrix[0][0], pointerRect1);
  BOOST_CHECK_EQUAL(testMatrix[0][1], pointerRect2);
  BOOST_CHECK_EQUAL(testMatrix[1][0], pointerCircle);
}

BOOST_AUTO_TEST_SUITE_END();
