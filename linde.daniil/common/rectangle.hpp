#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace linde
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(double setWidth, double setHeight, const point_t& setCenter);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& point) override;
    void move(const double dx, const double dy) override;
    void printInfo() const override;
    void scale(const double coefficient) override;
    void rotate(double angle) override;
  private:
    double width_;
    double height_;
    double angle_;
    point_t center_;
  };
}
#endif
