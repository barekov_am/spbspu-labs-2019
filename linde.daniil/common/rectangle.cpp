#define _USE_MATH_DEFINES
#include "rectangle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>

linde::Rectangle::Rectangle(double setWidth, double setHeight, const point_t& setCenter) :
  width_(setWidth),
  height_(setHeight),
  angle_(0),
  center_(setCenter)
{
  if ((width_ <= 0) && (height_ <= 0))
  {
    throw std::invalid_argument("Invalid parameters");
  }
}

double linde::Rectangle::getArea() const
{
  return width_ * height_;
}

linde::rectangle_t linde::Rectangle::getFrameRect() const
{
  const double radAngle = angle_ * M_PI / 180;
  const double width = width_ * fabs(cos(radAngle)) + height_ * fabs(sin(radAngle));
  const double height = width_ * fabs(sin(radAngle)) + height_ * fabs(cos(radAngle));
  return { width, height, center_ };
}

void linde::Rectangle::move(const point_t& point)
{
  center_ = point;
}

void linde::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void linde::Rectangle::printInfo() const
{
  const rectangle_t framerect = getFrameRect();
  std::cout << "Rectangle:" << std::endl;
  std::cout << "  Width: " << width_ << std::endl;
  std::cout << "  Height: " << height_ << std::endl;
  std::cout << "  Center coordinates for the rectangle: " << std::endl;
  std::cout << "    x: " << center_.x << std::endl;
  std::cout << "    y: " << center_.y << std::endl;
  std::cout << "  Frame rectangle for the rectangle: " << std::endl;
  std::cout << "    Width: " << framerect.width << std::endl;
  std::cout << "    Height: " << framerect.height << std::endl;
  std::cout << "  Area: " << getArea() << std::endl;
}

void linde::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }
  width_ *= coefficient;
  height_ *= coefficient;
}

void linde::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
