#include "partition.hpp"
#include <math.h>
#include <iostream>
#include <stdexcept>

bool linde::intersect(const p_shape shape1, const p_shape shape2)
{
  const rectangle_t frameRect1 = shape1->getFrameRect();
  const rectangle_t frameRect2 = shape2->getFrameRect();

  const double xLengthBetweenCentres = fabs(frameRect1.pos.x - frameRect2.pos.x);
  const double yLengthBetweenCentres = fabs(frameRect2.pos.y - frameRect2.pos.y);

  const double xIntersectionBorder = (frameRect1.width + frameRect2.width) / 2;
  const double yIntersectionBorder = (frameRect1.height + frameRect2.height) / 2;

  return((xLengthBetweenCentres < xIntersectionBorder) && (yLengthBetweenCentres < yIntersectionBorder));
}

linde::Matrix linde::partition(const CompositeShape& compShape)
{
  const size_t size = compShape.getCount();
  Matrix matrix;

  for (size_t i = 0; i < size; i++)
  {
    bool addStatus = false;
    p_shape currentShape = compShape[i];

    for (size_t j = 0; j < matrix.getRows(); j++)
    {
      addStatus = true;
      size_t sizeOfRow = matrix.getColumns(j);

      for (size_t k = 0; k < sizeOfRow; k++)
      {
        if (intersect(currentShape, matrix[j][k]))
        {
          addStatus = false;
          break;
        }
      }

      if (addStatus)
      {
        matrix.add(currentShape, j);
        break;
      }
    }
    if (!addStatus)
    {
      matrix.add(currentShape, matrix.getRows());
    }
  }
  return matrix;
}

linde::Matrix linde::partition(const std::unique_ptr<p_shape[]> arrayOfShapes)
{
  if (!arrayOfShapes)
  {
    throw std::invalid_argument("Can't get partition for empty array");
  }

  CompositeShape compShape;
  compShape.add(arrayOfShapes[0]);
  size_t size = (sizeof(arrayOfShapes) / sizeof(arrayOfShapes[0]));

  for (size_t i = 1; i < size; i++)
  {
    compShape.add(arrayOfShapes[i]);
  }

  return partition(compShape);

}
