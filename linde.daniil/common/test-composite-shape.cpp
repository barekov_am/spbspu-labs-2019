#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

#define FAULT 0.0001
using p_shape = std::shared_ptr<linde::Shape>;

BOOST_AUTO_TEST_SUITE(CompositeShapeTests)

BOOST_AUTO_TEST_CASE(CompositeShapeMoveDxy)
{
  linde::Circle circle(8, { 10, 9 });
  linde::Rectangle rectangle(7, 8, { 5, 6 });
  p_shape p_circle = std::make_shared<linde::Circle>(circle);
  p_shape p_rect = std::make_shared<linde::Rectangle>(rectangle);


  linde::CompositeShape compositeTest;
  compositeTest.add(p_circle);
  compositeTest.add(p_rect);
  const double widthBefore = compositeTest.getFrameRect().width;
  const double heightBefore = compositeTest.getFrameRect().height;
  const double areaBefore = compositeTest.getArea();
  compositeTest.move(20, 20);
  BOOST_CHECK_CLOSE(widthBefore, compositeTest.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(heightBefore, compositeTest.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(areaBefore, compositeTest.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(CompositeShapeMovePoint)
{
  linde::Rectangle rectangle(11, 12, { 9, 10 });
  linde::Triangle triangle({ 29, 30 }, { 20,30 }, { 15,25 });
  linde::CompositeShape compositeTest;
  p_shape p_rect = std::make_shared<linde::Rectangle>(rectangle);
  p_shape p_triang = std::make_shared<linde::Triangle>(triangle);
  compositeTest.add(p_rect);
  compositeTest.add(p_triang);
  const double widthBefore = compositeTest.getFrameRect().width;
  const double heightBefore = compositeTest.getFrameRect().height;
  const double areaBefore = compositeTest.getArea();
  compositeTest.move({ 20, -25 });
  BOOST_CHECK_CLOSE(widthBefore, compositeTest.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(heightBefore, compositeTest.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(areaBefore, compositeTest.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(CompositeShapeScale)
{
  linde::Circle circleTest(5, { 6, 7 });
  linde::point_t points[] = { {20, 10}, {20, -10}, {-10, -10}, {-10, 10} };
  int size = sizeof(points) / sizeof(points [0]);
  linde::Polygon polygonTest(size, points);
  p_shape p_circle = std::make_shared<linde::Circle>(circleTest);
  p_shape p_polyg = std::make_shared<linde::Polygon>(polygonTest);

  linde::CompositeShape compositeTest;
  compositeTest.add(p_circle);
  compositeTest.add(p_polyg);
  const double areaBefore = compositeTest.getArea();
  const double coefficient = 2;
  const linde::point_t centerBefore = compositeTest.getFrameRect().pos;
  compositeTest.scale(coefficient);
  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, compositeTest.getArea(), FAULT);
  BOOST_CHECK_CLOSE(centerBefore.x, compositeTest.getFrameRect().pos.x, FAULT);
  BOOST_CHECK_CLOSE(centerBefore.y, compositeTest.getFrameRect().pos.y, FAULT);
}
BOOST_AUTO_TEST_CASE(CompositeInvalidParametres)
{
  linde::Circle circle(8, { 10, 9 });
  linde::Rectangle rectangle(7, 8, { 5, 6 });
  p_shape p_circle = std::make_shared<linde::Circle>(circle);
  p_shape p_rect = std::make_shared<linde::Rectangle>(rectangle);
  linde::CompositeShape compositeTest;
  compositeTest.add(p_circle);
  compositeTest.add(p_rect);
  BOOST_CHECK_THROW(compositeTest.scale(-2), std::invalid_argument);
  BOOST_CHECK_THROW(compositeTest.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compositeTest.remove(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compositeTest.remove(4), std::out_of_range);
  BOOST_CHECK_THROW(compositeTest.remove(-2), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(DeleteShapes)
{
  linde::Circle circle1(8, { 10, 9 });
  linde::Rectangle rectangle1(8, 8, { 10, 10 });
  linde::Rectangle rectangle2(8, 8, { 10, 10 });
  linde::CompositeShape compositeTest;
  p_shape p_circle1 = std::make_shared<linde::Circle>(circle1);
  p_shape p_rect1 = std::make_shared<linde::Rectangle>(rectangle1);
  p_shape p_rect2 = std::make_shared<linde::Rectangle>(rectangle2);
  compositeTest.add(p_circle1);
  compositeTest.add(p_rect1);
  const int size = compositeTest.getCount();
  compositeTest.remove(0);
  BOOST_CHECK_THROW(compositeTest.remove(p_circle1), std::invalid_argument);
  BOOST_CHECK_THROW(compositeTest.remove(p_rect2), std::invalid_argument);
  const int sizeAfterRemove = compositeTest.getCount();
  BOOST_CHECK_EQUAL(size - 1, sizeAfterRemove);
}

BOOST_AUTO_TEST_SUITE_END()
