#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

#define FAULT 0.0001

BOOST_AUTO_TEST_SUITE(RectangleTests)

BOOST_AUTO_TEST_CASE(RectangleMoveDxy)
{
  linde::Rectangle rectangleTest(7, 8, { 5, 6 });
  const double widthBefore = rectangleTest.getFrameRect().width;
  const double heightBefore = rectangleTest.getFrameRect().height;
  const double areaBefore = rectangleTest.getArea();
  rectangleTest.move(7, 8);
  BOOST_CHECK_CLOSE(widthBefore, rectangleTest.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(heightBefore, rectangleTest.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(areaBefore, rectangleTest.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(RectangleMovePoint)
{
  linde::Rectangle rectangleTest(11, 12, { 9, 10 });
  const double widthBefore = rectangleTest.getFrameRect().width;
  const double heightBefore = rectangleTest.getFrameRect().height;
  const double areaBefore = rectangleTest.getArea();
  rectangleTest.move({ 13, 14 });
  BOOST_CHECK_CLOSE(widthBefore, rectangleTest.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(heightBefore, rectangleTest.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(areaBefore, rectangleTest.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(RectangleScale)
{
  linde::Rectangle rectangleTest(17, 18, { 15, 16 });
  const double areaBefore = rectangleTest.getArea();
  const double coefficient = 2;
  rectangleTest.scale(coefficient);
  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, rectangleTest.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(RectangleInvalidParameters)
{
  BOOST_CHECK_THROW(linde::Rectangle(-1, 0, { 1, 1 }), std::invalid_argument);
  linde::Rectangle rectangleTest(10, 12, { 1, 5 });
  BOOST_CHECK_THROW(rectangleTest.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
