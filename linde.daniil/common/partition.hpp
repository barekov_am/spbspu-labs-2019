#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

using p_shape = std::shared_ptr<linde::Shape>;
namespace linde
{
  bool intersect(const std::shared_ptr<Shape> shape1, const std::shared_ptr<Shape> shape2);
  Matrix partition(const CompositeShape& compShape);
  Matrix partition(const std::unique_ptr<std::shared_ptr<Shape>[]> arrayOfShapes);
}

#endif 

