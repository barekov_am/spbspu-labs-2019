#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

namespace linde
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t& point_a, const point_t& point_b, const point_t& point_c);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& point) override;
    void move(const double dx, const double dy) override;
    void printInfo() const override;
    void scale(const double coefficient) override;
    void rotate(double) override;
  private:
    point_t points_[3];
    point_t getCenter() const;
  };
}
#endif
