#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

#define FAULT 0.0001

BOOST_AUTO_TEST_SUITE(CircleTests)

BOOST_AUTO_TEST_CASE(CircleMoveDxy)
{
  linde::Circle circleTest(8, { 10, 9 });
  const double widthBefore = circleTest.getFrameRect().width;
  const double heightBefore = circleTest.getFrameRect().height;
  const double areaBefore = circleTest.getArea();
  circleTest.move(7, 0);
  BOOST_CHECK_CLOSE(widthBefore, circleTest.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(heightBefore, circleTest.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(areaBefore, circleTest.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(CircleMovePoint)
{
  linde::Circle circleTest(4, { 6, 5 });
  const double widthBefore = circleTest.getFrameRect().width;
  const double heightBefore = circleTest.getFrameRect().height;
  const double areaBefore = circleTest.getArea();
  circleTest.move({ 3, 2 });
  BOOST_CHECK_CLOSE(widthBefore, circleTest.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(heightBefore, circleTest.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(areaBefore, circleTest.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(CircleScale)
{
  linde::Circle circleTest(5, { 7, 6 });
  const double areaBefore = circleTest.getArea();
  const double coefficient = 2;
  circleTest.scale(coefficient);
  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, circleTest.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(CircleInvalidParameters)
{
  BOOST_CHECK_THROW(linde::Circle(-10, { 1, 1 }), std::invalid_argument);
  linde::Circle circleTest(5, { 1, 5 });
  BOOST_CHECK_THROW(circleTest.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
