#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

namespace linde
{
  class Circle : public Shape
  {
  public:
    Circle(double radius, const point_t& Center);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& point) override;
    void move(double dx, double dy) override;
    void printInfo() const override;
    void scale(const double coefficient) override;
    void rotate(double angle) override;
  private:
   double radius_;
   point_t center_;
  };
}
#endif
