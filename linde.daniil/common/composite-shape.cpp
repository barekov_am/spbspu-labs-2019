#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>


linde::CompositeShape::CompositeShape() :
  count_(0)
{}

linde::CompositeShape::CompositeShape(const CompositeShape & other) :
  count_(other.count_),
  shapes_(std::make_unique<p_shape[]>(other.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i] = other.shapes_[i];
  }
}


linde::CompositeShape::CompositeShape(CompositeShape && other) :
  count_(other.count_),
  shapes_(std::move(other.shapes_))
{
  other.count_ = 0;
}

linde::CompositeShape& linde::CompositeShape::operator =(const CompositeShape & other)
{
  if (this != &other)
  {
    count_ = other.count_;
    std::unique_ptr<p_shape[]> newShapeArray(std::make_unique<p_shape[]>(other.count_));

    for (size_t i = 0; i < count_; i++)
    {
      shapes_[i] = other.shapes_[i];
    }
    shapes_.swap(newShapeArray);
  }
  return *this;
}

linde::CompositeShape& linde::CompositeShape::operator =(CompositeShape && other)
{
  if (this != &other)
  {
    count_ = other.count_;
    shapes_ = std::move(other.shapes_);
    other.count_ = 0;
  }
  return *this;
}

linde::CompositeShape::p_shape linde::CompositeShape::operator [](size_t index) const
{
  if ((index >= count_))
  {
    throw std::out_of_range("Index is out of range.");
  }
  return shapes_[index];
}


double linde::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

linde::rectangle_t linde::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    return { 0, 0, {0, 0} };
  }

  rectangle_t tmpFrame = shapes_[0]->getFrameRect();
  double min_x = tmpFrame.pos.x - tmpFrame.width / 2;
  double max_x = tmpFrame.pos.x + tmpFrame.width / 2;
  double min_y = tmpFrame.pos.y - tmpFrame.height / 2;
  double max_y = tmpFrame.pos.y + tmpFrame.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    tmpFrame = shapes_[i]->getFrameRect();
    min_x = std::fmin(min_x, tmpFrame.pos.x - tmpFrame.width / 2);
    max_x = std::fmax(max_x, tmpFrame.pos.x + tmpFrame.width / 2);
    min_y = std::fmin(min_y, tmpFrame.pos.y - tmpFrame.height / 2);
    max_y = std::fmax(max_y, tmpFrame.pos.y + tmpFrame.height / 2);
  }
  const double width = max_x - min_x;
  const double height = max_y - min_y;

  return { width, height, {(min_x + max_x) / 2, (max_y + min_y) / 2} };
}

void linde::CompositeShape::move(const point_t & point)
{
  rectangle_t frameRect = getFrameRect();
  double dx = point.x - frameRect.pos.x;
  double dy = point.y - frameRect.pos.y;

  move(dx, dy);
}

void linde::CompositeShape::move(const double dx, const double dy)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty. You can't move it.");
  }

  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void linde::CompositeShape::printInfo() const
{
  if (count_ == 0)
  {
    std::cout << "Composite shape is empty." << std::endl << std::endl;
    return;
  }
  const rectangle_t frameRect = getFrameRect();
  std::cout << "Composite shape. It's center is at: (" << frameRect.pos.x << "; " << frameRect.pos.y << ")" << std::endl;
  std::cout << "  Center coordinates for the rectangle: " << std::endl;
  std::cout << "    x: " << frameRect.pos.x << std::endl;
  std::cout << "    y: " << frameRect.pos.y << std::endl;
  std::cout << "It includes " << count_ << " figures" << std::endl;
  std::cout << "Frame rectangle has width = " << frameRect.width << std::endl;
  std::cout << "Frame rectangle has height = " << frameRect.height << std::endl;
  std::cout << "Area is: " << getArea() << std::endl << std::endl;
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->printInfo();
  }
}

void linde::CompositeShape::scale(const double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Scale amount must be positive.");
  }

  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty. You can't scale it.");
  }

  const rectangle_t frameRect = getFrameRect();

  for (size_t i = 0; i < count_; i++)
  {
    const double dx = shapes_[i]->getFrameRect().pos.x - frameRect.pos.x;
    const double dy = shapes_[i]->getFrameRect().pos.y - frameRect.pos.y;
    shapes_[i]->move(dx * (coefficient - 1), dy * (coefficient - 1));
    shapes_[i]->scale(coefficient);
  }
}

void linde::CompositeShape::rotate(double angle)
{
  const point_t frameRectCentre = getFrameRect().pos;
  const double radAngle = angle * M_PI / 180;
  const double cosAngle = cos(radAngle);
  const double sinAngle = sin(radAngle);
  for (size_t i = 0; i < count_; i++)
  {
    const double dx = shapes_[i]->getFrameRect().pos.x - frameRectCentre.x;
    const double dy = shapes_[i]->getFrameRect().pos.y - frameRectCentre.y;
    const double moveDX = dx * cosAngle - dy * sinAngle;
    const double moveDY = dx * sinAngle + dy * cosAngle;
    shapes_[i]->move({ frameRectCentre.x + moveDX, frameRectCentre.y + moveDY });
    shapes_[i]->rotate(angle);
  }
}



void linde::CompositeShape::add(p_shape shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape must not be a null pointer.");
  }
  std::unique_ptr<p_shape[]> newShapeArray(std::make_unique<p_shape[]>(count_ + 1));

  for (size_t i = 0; i < count_; i++)
  {
    newShapeArray[i] = shapes_[i];
  }
  newShapeArray[count_] = shape;
  count_++;
  shapes_.swap(newShapeArray);
}

void linde::CompositeShape::remove(const p_shape shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape must not be a null pointer.");
  }

  for (size_t i = 0; i < count_; i++)
  {
    if (shape == shapes_[i])
    {
      remove(i);
      return;
    }
  }
  throw std::invalid_argument("This shape does not exist in composite shape.");
}


void linde::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }
  std::unique_ptr<p_shape[]> newShapeArray(std::make_unique<p_shape[]>(count_ - 1));
  for (size_t i = 0; i < index; i++)
  {
    newShapeArray[i] = shapes_[i];
  }
  count_--;
  for (size_t i = index; i < count_; i++)
  {
    newShapeArray[i] = shapes_[i + 1];
  }
  shapes_.swap(newShapeArray);
}

size_t linde::CompositeShape::getCount() const
{
  return count_;
}
