#include <boost/test/auto_unit_test.hpp>
#include "matrix.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "partition.hpp"


BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(testMatrixGetters)
{
  linde::Circle testCircle1(2.0, { 3.0, -2.0 });
  linde::Circle testCircle2(3.0, { -1.0, 2.0 });
  linde::Rectangle testRect1({ 2.0, 3.0, {1.0, 2.0} });
  linde::Rectangle testRect2({ 3.0, 4.0, {-2.0, -3.0} });
  std::shared_ptr<linde::Shape> pointerCircle1 = std::make_shared<linde::Circle>(testCircle1);
  std::shared_ptr<linde::Shape> pointerCircle2 = std::make_shared<linde::Circle>(testCircle2);
  std::shared_ptr<linde::Shape> pointerRect1 = std::make_shared<linde::Rectangle>(testRect1);
  std::shared_ptr<linde::Shape> pointerRect2 = std::make_shared<linde::Rectangle>(testRect2);

  linde::Matrix testMatrix;
  testMatrix.add(pointerCircle1, 0);
  testMatrix.add(pointerRect1, 1);
  testMatrix.add(pointerRect2, 2);
  testMatrix.add(pointerCircle2, 2);

  BOOST_CHECK_EQUAL(testMatrix.getSize(), 4);
  BOOST_CHECK_EQUAL(testMatrix.getRows(), 3);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(0), 1);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(1), 1);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(2), 2);
  BOOST_CHECK_EQUAL(testMatrix.getCountElBehind(1), 1);
  BOOST_CHECK_EQUAL(testMatrix.getCountElBehind(2), 2);

  BOOST_CHECK_EQUAL(testMatrix[0][0], pointerCircle1);
  BOOST_CHECK_EQUAL(testMatrix[1][0], pointerRect1);
  BOOST_CHECK_EQUAL(testMatrix[2][0], pointerRect2);
  BOOST_CHECK_EQUAL(testMatrix[2][1], pointerCircle2);

}

BOOST_AUTO_TEST_CASE(testIncorrectArgumets)
{
  linde::Rectangle testRect({ 2.0, 3.0, {0.0, 0.0} });
  linde::Circle testCircle(3.0, { 7.0, 6.0 });
  std::shared_ptr<linde::Shape> pointerRect = std::make_shared<linde::Rectangle>(testRect);
  std::shared_ptr<linde::Shape> pointerCircle = std::make_shared<linde::Circle>(testCircle);


  linde::CompositeShape testCompShape;
  testCompShape.add(pointerRect);
  testCompShape.add(pointerCircle);

  linde::Matrix testMatrix = linde::partition(testCompShape);
  BOOST_CHECK_THROW(testMatrix[testMatrix.getRows()], std::invalid_argument);
  BOOST_CHECK_THROW(testMatrix.getColumns(testMatrix.getRows()), std::invalid_argument);
  BOOST_CHECK_THROW(testMatrix.getCountElBehind(testMatrix.getRows()), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testCopyConstructor)
{
  linde::Rectangle testRect1({ 3.2, 4.0, {0.0, 0.0} });
  linde::Rectangle testRect2({ 2.0, 3.0, {1.0, 2.0} });
  linde::Circle testCircle1(2.0, { 3.0, 7.0 });
  linde::Circle testCircle2(3.0, { -1.0, 2.0 });
  std::shared_ptr<linde::Shape> pointerRect1 = std::make_shared<linde::Rectangle>(testRect1);
  std::shared_ptr<linde::Shape> pointerRect2 = std::make_shared<linde::Rectangle>(testRect2);
  std::shared_ptr<linde::Shape> pointerCircle1 = std::make_shared<linde::Circle>(testCircle1);
  std::shared_ptr<linde::Shape> pointerCircle2 = std::make_shared<linde::Circle>(testCircle2);

  linde::CompositeShape testCompShape;
  testCompShape.add(pointerRect1);
  testCompShape.add(pointerCircle1);
  testCompShape.add(pointerCircle2);
  testCompShape.add(pointerRect2);

  linde::Matrix testMatrix = linde::partition(testCompShape);
  linde::Matrix copyMatrix(testMatrix);
  BOOST_CHECK(copyMatrix == testMatrix);
}

BOOST_AUTO_TEST_CASE(testMoveConstructor)
{
  linde::Rectangle testRect1({ 3.2, 4.0, {0.0, 0.0} });
  linde::Rectangle testRect2({ 2.0, 3.0, {1.0, 2.0} });
  linde::Circle testCircle1(2.0, { 3.0, 7.0 });
  linde::Circle testCircle2(3.0, { -1.0, 2.0 });
  std::shared_ptr<linde::Shape> pointerRect1 = std::make_shared<linde::Rectangle>(testRect1);
  std::shared_ptr<linde::Shape> pointerRect2 = std::make_shared<linde::Rectangle>(testRect2);
  std::shared_ptr<linde::Shape> pointerCircle1 = std::make_shared<linde::Circle>(testCircle1);
  std::shared_ptr<linde::Shape> pointerCircle2 = std::make_shared<linde::Circle>(testCircle2);

  linde::CompositeShape testCompShape;
  testCompShape.add(pointerRect1);
  testCompShape.add(pointerCircle1);
  testCompShape.add(pointerCircle2);
  testCompShape.add(pointerRect2);

  linde::Matrix testMatrix = linde::partition(testCompShape);
  linde::Matrix moveMatrix(linde::partition(testCompShape));
  BOOST_CHECK(moveMatrix == testMatrix);
}

BOOST_AUTO_TEST_CASE(testCopyAssignmentOperator)
{
  linde::Rectangle testRect1({ 3.2, 4.0, {0.0, 0.0} });
  linde::Rectangle testRect2({ 2.0, 3.0, {1.0, 2.0} });
  linde::Circle testCircle1(2.0, { 3.0, 7.0 });
  linde::Circle testCircle2(3.0, { -1.0, 2.0 });
  std::shared_ptr<linde::Shape> pointerRect1 = std::make_shared<linde::Rectangle>(testRect1);
  std::shared_ptr<linde::Shape> pointerRect2 = std::make_shared<linde::Rectangle>(testRect2);
  std::shared_ptr<linde::Shape> pointerCircle1 = std::make_shared<linde::Circle>(testCircle1);
  std::shared_ptr<linde::Shape> pointerCircle2 = std::make_shared<linde::Circle>(testCircle2);

  linde::CompositeShape testCompShape;
  testCompShape.add(pointerRect1);
  testCompShape.add(pointerCircle1);
  testCompShape.add(pointerCircle2);
  testCompShape.add(pointerRect2);

  linde::Matrix testMatrix = linde::partition(testCompShape);
  linde::Matrix copyMatrix;
  copyMatrix = testMatrix;
  BOOST_CHECK(copyMatrix == testMatrix);
}

BOOST_AUTO_TEST_CASE(testMoveAssignmentOperator)
{
  linde::Rectangle testRect1({ 3.2, 4.0, {0.0, 0.0} });
  linde::Rectangle testRect2({ 2.0, 3.0, {1.0, 2.0} });
  linde::Circle testCircle1(2.0, { 3.0, 7.0 });
  linde::Circle testCircle2(3.0, { -1.0, 2.0 });
  std::shared_ptr<linde::Shape> pointerRect1 = std::make_shared<linde::Rectangle>(testRect1);
  std::shared_ptr<linde::Shape> pointerRect2 = std::make_shared<linde::Rectangle>(testRect2);
  std::shared_ptr<linde::Shape> pointerCircle1 = std::make_shared<linde::Circle>(testCircle1);
  std::shared_ptr<linde::Shape> pointerCircle2 = std::make_shared<linde::Circle>(testCircle2);

  linde::CompositeShape testCompShape;
  testCompShape.add(pointerRect1);
  testCompShape.add(pointerCircle1);
  testCompShape.add(pointerCircle2);
  testCompShape.add(pointerRect2);

  linde::Matrix testMatrix = linde::partition(testCompShape);
  linde::Matrix moveMatrix;
  moveMatrix = linde::partition(testCompShape);
  BOOST_CHECK(moveMatrix == testMatrix);
}

BOOST_AUTO_TEST_SUITE_END();
