#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "polygon.hpp"

#define FAULT 0.0001

BOOST_AUTO_TEST_SUITE(PolygonTests)

BOOST_AUTO_TEST_CASE(PolygonMoveDxy)
{
  linde::point_t points[] = { {20, 10}, {20, -10}, {-10, -10}, {-10, 10} };
  int number_of_points = sizeof(points) / sizeof(points[0]);
  linde::Polygon polygonTest(number_of_points, points);
  const double widthBefore = polygonTest.getFrameRect().width;
  const double heightBefore = polygonTest.getFrameRect().height;
  const double areaBefore = polygonTest.getArea();
  polygonTest.move(10, 20);
  BOOST_CHECK_CLOSE(widthBefore, polygonTest.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(heightBefore, polygonTest.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(areaBefore, polygonTest.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(PolygonMovePoint)
{
  linde::point_t points[] = { {20, 10}, {20, -10}, {-10, -10}, {-10, 10} };
  int number_of_points = sizeof(points) / sizeof(points[0]);
  linde::Polygon polygonTest(number_of_points, points);
  const double widthBefore = polygonTest.getFrameRect().width;
  const double heightBefore = polygonTest.getFrameRect().height;
  const double areaBefore = polygonTest.getArea();
  polygonTest.move({ 30, 40 });
  BOOST_CHECK_CLOSE(widthBefore, polygonTest.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(heightBefore, polygonTest.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(areaBefore, polygonTest.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(PolygonScale)
{
  linde::point_t points[] = { {20, 10}, {20, -10}, {-10, -10}, {-10, 10} };
  int number_of_points = sizeof(points) / sizeof(points[0]);
  linde::Polygon polygonTest(number_of_points, points);
  const double areaBefore = polygonTest.getArea();
  const double coefficient = 2;
  polygonTest.scale(coefficient);
  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, polygonTest.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(PolygonInvalidScale)
{
  linde::point_t points[] = { {20, 10}, {20, -10}, {-10, -10}, {-10, 10} };
  int number_of_points = sizeof(points) / sizeof(points[0]);
  linde::Polygon polygonTest(number_of_points, points);
  BOOST_CHECK_THROW(polygonTest.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(PolygonInvalidParameters)
{
  linde::point_t points[] = { {20, 10}, {20, -10} };
  int number_of_points = sizeof(points) / sizeof(points[0]);
  BOOST_CHECK_THROW(linde::Polygon polygonTest(number_of_points, points), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
