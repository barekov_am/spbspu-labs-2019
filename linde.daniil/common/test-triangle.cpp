#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"

#define FAULT 0.0001

BOOST_AUTO_TEST_SUITE(TriangleTests)

BOOST_AUTO_TEST_CASE(TriangleMoveDxy)
{
  linde::Triangle triangleTest({ 29, 30 }, { 20,30 }, { 15,25 });
  const double widthBefore = triangleTest.getFrameRect().width;
  const double heightBefore = triangleTest.getFrameRect().height;
  const double areaBefore = triangleTest.getArea();
  triangleTest.move(5, 10);
  BOOST_CHECK_CLOSE(widthBefore, triangleTest.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(heightBefore, triangleTest.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(areaBefore, triangleTest.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(TriangleMovePoint)
{
  linde::Triangle triangleTest({ 40, 27 }, { 50, 21 }, { 60, 90 });
  const double widthBefore = triangleTest.getFrameRect().width;
  const double heightBefore = triangleTest.getFrameRect().height;
  const double areaBefore = triangleTest.getArea();
  triangleTest.move({ 20, -25 });
  BOOST_CHECK_CLOSE(widthBefore, triangleTest.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(heightBefore, triangleTest.getFrameRect().height, FAULT);
  BOOST_CHECK_CLOSE(areaBefore, triangleTest.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(TriangleScale)
{
  linde::Triangle triangleTest({ 13,20 }, { 15, 22 }, { 12, 12 });
  const double areaBefore = triangleTest.getArea();
  const double coefficient = 2;
  triangleTest.scale(coefficient);
  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, triangleTest.getArea(), FAULT);
}

BOOST_AUTO_TEST_CASE(TriangleInvalidParameters)
{
  BOOST_CHECK_THROW(linde::Triangle({ 1, 1 }, { 1, 1 }, { 5, 0 }), std::invalid_argument);
  linde::Triangle triangleTest({ 1, 5 }, { 5, 1 }, { 0, 0 });
  BOOST_CHECK_THROW(triangleTest.scale(-15), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
