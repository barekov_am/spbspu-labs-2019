#ifndef COMPOSITESHAPE_H
#define COMPOSITESHAPE_H

#include <memory>
#include "shape.hpp"

namespace linde
{
  class CompositeShape : public Shape
  {
  public:
    using p_shape = std::shared_ptr<Shape>;

    CompositeShape();
    CompositeShape(const CompositeShape & other);
    CompositeShape(CompositeShape && other);
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape & other);
    CompositeShape& operator =(CompositeShape && other);
    p_shape operator [](size_t index) const;

    bool operator ==(const CompositeShape& rhs) const;
    bool operator !=(const CompositeShape& rhs) const;


    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & point) override;
    void move(const double dx, const double dy) override;
    void printInfo() const override;
    void scale(const double coefficient) override;
    void rotate(double angle) override;

    void add(p_shape shape);
    void remove(p_shape shape);
    void remove(size_t index);
    size_t getCount() const;

  private:
    size_t count_;
    std::unique_ptr<p_shape[]> shapes_;

  };
}

#endif 

