#define _USE_MATH_DEFINES
#include "polygon.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

linde::Polygon::Polygon(const Polygon& other) :
  points_(new point_t[other.number_point_]),
  number_point_(other.number_point_)
{
  for (int i = 0; i < number_point_; i++)
  {
    points_[i] = other.points_[i];
  }
}

linde::Polygon::Polygon(Polygon && other) :
  points_(other.points_),
  number_point_(other.number_point_)
{
  other.points_ = nullptr;
  other.number_point_ = 0;
}

linde::Polygon::Polygon(const int number_of_points, const point_t* point) :
  points_(new point_t[number_of_points]),
  number_point_(number_of_points)
{
  if (number_point_ < 3)
  {
    delete[] points_;
    throw std::invalid_argument("Count of points must be >= 3");
  }
  if (point == nullptr)
  {
    delete[] points_;
    throw std::invalid_argument("Pointer to points mustn't be null");
  }
  for (int i = 0; i < number_point_; i++)
  {
    points_[i] = point[i];
  }
  if (!isConvex())
  {
    delete[] points_;
    throw std::invalid_argument("Polygon must be convex");
  }
  if (getArea() <= 0)
  {
    delete[] points_;
    throw std::invalid_argument("Points must be different");
  }
}

linde::Polygon::~Polygon()
{
  delete[] points_;
}

linde::Polygon& linde::Polygon::operator =(const Polygon& other)
{
  if (this == &other)
  {
    return *this;
  }
  number_point_ = other.number_point_;
  delete[] points_;
  points_ = new point_t[number_point_];
  for (int i = 0; i < number_point_; i++)
  {
    points_[i] = other.points_[i];
  }
  return *this;
}

linde::Polygon& linde::Polygon::operator =(Polygon&& other)
{
  if (this == &other)
  {
    return *this;
  }
  number_point_ = other.number_point_;
  other.number_point_ = 0;
  delete[] points_;
  points_ = other.points_;
  other.points_ = nullptr;
  return *this;
}

double linde::Polygon::getArea() const
{
  double sum_pos = 0;
  double sum_neg = 0;
  for (int i = 0; i < number_point_ - 1; i++)
  {
    sum_pos += points_[i].x * points_[i + 1].y;
    sum_neg += points_[i + 1].x * points_[i].y;
  }
  return (std::fabs(sum_pos + points_[number_point_ - 1].x * points_[0].y - sum_neg
    - points_[0].x * points_[number_point_ - 1].y) / 2);
}

linde::rectangle_t linde::Polygon::getFrameRect() const
{
  double min_x = points_[0].x;
  double max_x = points_[0].x;
  double min_y = points_[0].y;
  double max_y = points_[0].y;
  for (int i = 1; i < number_point_; i++)
  {
    min_x = std::fmin(min_x, points_[i].x);
    max_x = std::fmax(max_x, points_[i].x);
    min_y = std::fmin(min_y, points_[i].y);
    max_y = std::fmax(max_y, points_[i].y);
  }
  double w = max_x - min_x;
  double h = max_y - min_y;
  point_t pos = { (max_x + min_x) / 2, (max_y + min_y) / 2 };
  return rectangle_t{ w, h, pos };
}

void linde::Polygon::move(const point_t& point)
{
  const double dx = point.x - getCenter().x;
  const double dy = point.y - getCenter().y;
  move(dx, dy);
}

void linde::Polygon::move(const double dx, const double dy)
{
  for (int i = 0; i < number_point_; i++)
  {
    points_[i].x += dx;
    points_[i].y += dy;
  }
}

void linde::Polygon::printInfo() const
{
  const rectangle_t framerect = getFrameRect();
  const point_t center = getCenter();
  std::cout << "Polygon:" << std::endl;
  std::cout << "  The apex coordinates of the Polygon: " << std::endl;
  for (int i = 0; i < number_point_; i++)
  {
    std::cout << "    Apex " << i << ": x = " << points_[i].x << " y = " << points_[i].y << std::endl;
  }
  std::cout << "  The Center coordinates of the Polygon: " << std::endl;
  std::cout << "    x: " << center.x << std::endl;
  std::cout << "    y: " << center.y << std::endl;
  std::cout << "  Frame rectangle for the Polygon: " << std::endl;
  std::cout << "    Width: " << framerect.width << std::endl;
  std::cout << "    Height: " << framerect.height << std::endl;
  std::cout << "  Area: " << getArea() << std::endl;
}

void linde::Polygon::scale(const double coefficent)
{
  if (coefficent <= 0.0)
  {
    throw std::invalid_argument("Multiplier must be more then 0.0");
  }
  else
  {
    const point_t center = getCenter();
    for (int i = 0; i < number_point_; i++)
    {
      points_[i] = { points_[i].x - (center.x - points_[i].x) * (coefficent - 1),
          points_[i].y - (center.y - points_[i].y) * (coefficent - 1) };
    }
  }
}

void linde::Polygon::rotate(double angle)
{
  point_t center = getCenter();
  const double radAngle = angle * M_PI / 180;
  const double cosAngle = cos(radAngle);
  const double sinAngle = sin(radAngle);
  for (int i = 0; i < number_point_; i++)
  {
    points_[i] = { center.x + (points_[i].x - center.x) * cosAngle - (points_[i].y - center.y) * sinAngle,
      center.y + (points_[i].x - center.x) * sinAngle + (points_[i].y - center.y) * cosAngle };
  }
}


linde::point_t linde::Polygon::getCenter() const
{
  double sumx = 0;
  double sumy = 0;
  for (int i = 0; i < number_point_; i++)
  {
    sumx += points_[i].x;
    sumy += points_[i].y;
  }
  return point_t{ sumx / number_point_, sumy / number_point_ };
}

bool linde::Polygon::isConvex() const
{
  point_t sideA = { points_[0].x - points_[number_point_ - 1].x, points_[0].y - points_[number_point_ - 1].y };
  point_t sideB = { points_[1].x - points_[0].x, points_[1].y - points_[0].y };
  double sign = sideA.x * sideB.y - sideA.y * sideB.x;
  for (int i = 0; i < number_point_ - 2; ++i)
  {
    sideA.x = points_[i + 1].x - points_[i].x;
    sideA.y = points_[i + 1].y - points_[i].y;
    sideB.x = points_[i + 2].x - points_[i + 1].x;
    sideB.y = points_[i + 2].y - points_[i + 1].y;
    if ((sideA.x * sideB.y - sideA.y * sideB.x) * sign <= 0)
    {
      return false;
    }
  }
  return true;
}
