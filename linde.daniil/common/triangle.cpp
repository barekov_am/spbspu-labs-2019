#define _USE_MATH_DEFINES
#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

linde::Triangle::Triangle(const point_t& point_a, const point_t& point_b, const point_t& point_c) :
  points_{ point_a, point_b, point_c }
{
  if (getArea() <= 0)
  {
    throw std::invalid_argument("Points of the triangle must be different!");
  }
}

double linde::Triangle::getArea() const
{
  return ((std::fabs(((points_[0].x - points_[2].x) * (points_[1].y - points_[2].y))
      - ((points_[0].y - points_[2].y) * (points_[1].x - points_[2].x))) / 2));
}

linde::rectangle_t linde::Triangle::getFrameRect() const
{
  const double min_x = std::fmin(std::fmin(points_[0].x, points_[1].x), points_[2].x);
  const double min_y = std::fmin(std::fmin(points_[0].y, points_[1].y), points_[2].y);
  const double h = std::fmax(std::fmax(points_[0].y, points_[1].y), points_[2].y) - min_y;
  const double w = std::fmax(std::fmax(points_[0].x, points_[1].x), points_[2].x) - min_x;
  return rectangle_t{ w, h, { (std::fmax(points_[0].x, points_[1].x) + min_x) / 2,
      (std::fmax(points_[0].y, points_[1].y) + min_y) / 2 } };
}

void linde::Triangle::move(const point_t& point)
{
  const double dx = point.x - getCenter().x;
  const double dy = point.y - getCenter().y;
  move(dx, dy);
}

void linde::Triangle::move(double dx, double dy)
{
  for (int i = 0; i < 3; i++)
  {
    points_[i].x += dx;
    points_[i].y += dy;
  }
}

void linde::Triangle::printInfo() const
{
  const rectangle_t framerect = getFrameRect();
  const point_t center = getCenter();
  std::cout << "Triangle:" << std::endl;
  std::cout << "  The apex coordinates of the triangle: " << std::endl;
  std::cout << "    A (" << points_[0].x << "," << points_[0].y << ")" << std::endl;
  std::cout << "    B (" << points_[1].x << "," << points_[1].y << ")" << std::endl;
  std::cout << "    C (" << points_[2].x << "," << points_[2].y << ")" << std::endl;
  std::cout << "  The Center coordinates of the triangle: " << std::endl;
  std::cout << "    x: " << center.x << std::endl;
  std::cout << "    y: " << center.y << std::endl;
  std::cout << "  Frame rectangle for the triangle: " << std::endl;
  std::cout << "    Width: " << framerect.width << std::endl;
  std::cout << "    Height: " << framerect.height << std::endl;
  std::cout << "  Area: " << getArea() << std::endl;
}

void linde::Triangle::scale(const double coefficent)
{
  if (coefficent <= 0.0)
  {
    throw std::invalid_argument("Multiplier must be more then 0.0");
  }
  else
  {
  const point_t center = getCenter();
  for (int i = 0; i < 3; i++)
  {
    points_[i] = { points_[i].x - (center.x - points_[i].x) * (coefficent - 1),
        points_[i].y - (center.y - points_[i].y) * (coefficent - 1) };
  }
  }
}

void linde::Triangle::rotate(double angle)
{
  point_t center = getCenter();
  const double radAngle = angle * M_PI / 180;
  const double cosAngle = cos(radAngle);
  const double sinAngle = sin(radAngle);
  points_[0] = { center.x + (points_[0].x - center.x) * cosAngle - (points_[0].y - center.y) * sinAngle,
    center.y + (points_[0].x - center.x) * sinAngle + (points_[0].y - center.y) * cosAngle };
  points_[1] = { center.x + (points_[1].x - center.x) * cosAngle - (points_[1].y - center.y) * sinAngle,
    center.y + (points_[1].x - center.x) * sinAngle + (points_[1].y - center.y) * cosAngle };
  points_[2] = { center.x + (points_[2].x - center.x) * cosAngle - (points_[2].y - center.y) * sinAngle,
    center.y + (points_[2].x - center.x) * sinAngle + (points_[2].y - center.y) * cosAngle };
}

linde::point_t linde::Triangle::getCenter() const
{
  return point_t{ (points_[0].x + points_[1].x + points_[2].x) / 3, (points_[0].x + points_[1].x + points_[2].x) / 3 };
}
