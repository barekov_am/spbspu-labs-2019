#include <iostream>
#include <vector>
#include <stdexcept>
#include <cstring>
#include <iterator>
#include <algorithm>
#include "texteditor.hpp"

int main(int argc, char ** argv)
{
  if (argc > 3 || argc == 2)
  {
    std::cerr << "Invalid number of arguments" << std::endl;
    return 1;
  }

  try
  {
    size_t lineWidthMax = 40;
    if (argc == 3)
    {
      if (strcmp(argv[1], "--line-width") != 0)
      {
        std::cerr << "Invalid arguments" << std::endl;
        return 1;
      }
      std::string value = argv[2];
      for (auto symb:value)
      {
        if (!std::isdigit(symb))
        {
          std::cerr << "Invalid line-width" << std::endl;
          return 1;
        }
      }
      lineWidthMax = std::stoi(value);
      if (lineWidthMax < 25)
      {
        std::cerr << "Invalid line-width" << std::endl;
        return 1;
      }
    }

    TextEditor<std::vector<char>::iterator> handler(lineWidthMax);
    std::vector<char> vector;
    std::cin >> std::noskipws;
    std::copy(std::istream_iterator<char>(std::cin), std::istream_iterator<char>(), std::back_inserter(vector));
    if (!std::cin.eof())
    {
      std::cerr << "Reading is failed" << std::endl;
      return 1;
    }

    handler.textEnter(vector.begin(), vector.end());
  }
  catch (std::invalid_argument & ex)
  {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cerr << "Error" << std::endl;
    return 1;
  }

  return 0;
}
