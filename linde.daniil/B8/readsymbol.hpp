#ifndef READSYMBOL
#define READSYMBOL
#include <vector>
#include <locale>

class ReadSymbol
{
public:
  ReadSymbol();
  void word();
  void setSyntax();
  void setComma();
  void setDash();
  void setSpace();
  bool space();
  bool getNewWord();
private:
  bool wordFlag_, syntaxFlag_, spaceFlag_, commaFlag_;
};

#endif
