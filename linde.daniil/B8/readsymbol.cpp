#include <stdexcept>
#include <utility>
#include <locale>
#include <vector>
#include "readsymbol.hpp"

ReadSymbol::ReadSymbol() :
  wordFlag_(false),
  syntaxFlag_(false),
  spaceFlag_(true),
  commaFlag_(false)
{};

void ReadSymbol::word()
{
  wordFlag_ = true;
  spaceFlag_ = false;
  syntaxFlag_ = false;
  commaFlag_ = false;
}

void ReadSymbol::setSyntax()
{
  if (syntaxFlag_)
  {
    throw std::invalid_argument("Syntax befor syntax");
  }

  if (!wordFlag_)
  {
    throw std::invalid_argument("Unexpected syntax");
  }

  syntaxFlag_ = true;
  wordFlag_ = false;
  spaceFlag_ = true;
}

void ReadSymbol::setComma()
{
  setSyntax();
  commaFlag_ = true;
}

void ReadSymbol::setDash()
{
  if (!(commaFlag_ || wordFlag_))
  {
    throw std::invalid_argument("Unexpected syntex");
  }

  syntaxFlag_ = true;
  wordFlag_ = false;
  commaFlag_ = false;
  spaceFlag_ = true;
}

void ReadSymbol::setSpace()
{
  spaceFlag_ = true;
}

bool ReadSymbol::space()
{
  return spaceFlag_;
}

bool ReadSymbol::getNewWord()
{
  return syntaxFlag_ || spaceFlag_ || !wordFlag_;
}
