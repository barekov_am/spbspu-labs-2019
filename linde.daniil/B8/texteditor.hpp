#ifndef TEXTHANDLER
#define TEXTHANDLER

#include "readsymbol.hpp"
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <stdexcept>
#include <locale>

template<typename Iterator>
class TextEditor
{
public:
  TextEditor(size_t lineWidth);
  void textEnter(Iterator begin, Iterator end);

private:
  ReadSymbol symbol_;
  const size_t lineWidth_;
  std::vector<char> str_;
  std::vector<char> word_;
  Iterator iter_;
  Iterator end_;

  void newWord();
  bool readWord();
  bool readNumber();
  bool readSyntax();
  bool readSpace();
};

template<typename Iterator>
TextEditor<Iterator>::TextEditor(size_t lineWidthMax) :
  symbol_(),
  lineWidth_(lineWidthMax),
  str_(),
  word_(),
  iter_(),
  end_()
{
}

template<typename Iterator>
void TextEditor<Iterator>::textEnter(Iterator begin, Iterator end)
{
  if (begin == end)
  {
    return;
  }
  iter_ = begin;
  end_ = end;
  iter_--;

  while (iter_ != end_)
  {
    iter_++;
    if (!(readWord() || readNumber() || readSpace() || readSyntax()))
    {
      return;
    }
  }
  newWord();
  if (!str_.empty())
  {
    std::copy(str_.begin(), str_.end() - 1, std::ostream_iterator<char>(std::cout));
    std::cout << std::endl;
  }
}

template<typename Iterator>
void TextEditor<Iterator>::newWord()
{
  size_t sizeW = word_.size();
  size_t sizeStr = str_.size();

  if (sizeStr + sizeW < lineWidth_) {
    str_.insert(str_.end(), word_.begin(), word_.end());
    if (!str_.empty()) {
      str_.push_back(' ');
    }
  } else if (sizeStr + sizeW == lineWidth_) {
    std::copy(str_.begin(), str_.end(), std::ostream_iterator<char>(std::cout));
    std::copy(word_.begin(), word_.end(), std::ostream_iterator<char>(std::cout));
    std::cout << std::endl;
    str_.clear();
  } else {
    std::copy(str_.begin(), str_.end() - 1, std::ostream_iterator<char>(std::cout));
    std::cout << std::endl;
    str_.clear();
    std::vector<char>::iterator begin = word_.begin();
    str_.insert(str_.end(), begin, word_.end());
    str_.push_back(' ');
  }

  word_.clear();
  symbol_.word();
}

template<typename Iterator>
bool TextEditor<Iterator>::readWord()
{
  std::locale cloc("C");
  if (std::isalpha(*iter_, cloc)) {
    if (symbol_.getNewWord()) {
      newWord();
    }
  } else {
    return false;
  }

  while (iter_ != end_)
  {
    if (std::isalpha(*iter_, cloc)) {
      word_.push_back(*iter_);
    } else if (*iter_ == '-') {
      word_.push_back(*iter_);
      break;
    } else {
      iter_--;
      break;
    }

    iter_++;
  }

  if (word_.size() > 20)
  {
    throw std::invalid_argument("Too long word");
  }

  return true;
}

template<typename Iterator>
bool TextEditor<Iterator>::readNumber()
{
  std::locale cloc("C");
  if (isdigit(*iter_, cloc) || *iter_ == '+') {
    newWord();
  } else if (*iter_ == '-' && isdigit(*(iter_ + 1), cloc)) {
    ++iter_;
    newWord();
    word_.push_back('-');
  } else {
    return false;
  }

  char ch = std::use_facet< std::numpunct<char> >(std::cin.getloc()).decimal_point();

  while (iter_ != end_)
  {
    if (isdigit(*iter_, cloc) || *iter_ == ch || *iter_ == '+') {
      word_.push_back(*iter_);
    } else {
      iter_--;
      break;
    }
    iter_++;
  }

  if (word_.size() > 20)
  {
    throw std::invalid_argument("Too long number");
  }

  return true;
}

template<typename Iterator>
bool TextEditor<Iterator>::readSyntax()
{
  if (*iter_ == '-')
  {
    symbol_.setDash();
    if (*(++iter_) == '-' && *(++iter_) == '-') {
      word_.push_back(' ');
      word_.push_back('-');
      word_.push_back('-');
      word_.push_back('-');
    } else {
      throw std::invalid_argument("Unexpected symbol");
    }
  } else if (ispunct(*iter_)) {
    if (*iter_ == ',') {
      symbol_.setComma();
    } else {
      symbol_.setSyntax();
    }
    word_.push_back(*iter_);
  } else {
    return false;
  }

  return true;
}

template<typename Iterator>
bool TextEditor<Iterator>::readSpace()
{
  if (isspace(*iter_))
  {
    symbol_.setSpace();
    while (iter_ != end_)
    {
      if (!isspace(*iter_))
      {
        iter_--;
        break;
      }
      iter_++;
    }
  }
  else
  {
    return false;
  }

  return true;
}

#endif
