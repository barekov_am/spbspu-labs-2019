#include "triangle.hpp"

#include <iostream>
#include <algorithm>
#include <cassert>
#include <cmath>

double getLength(const point_t &point1, const point_t &point2)
{
  return sqrt(pow(point1.x - point2.x, 2) + pow(point1.y - point2.y, 2));
}

Triangle::Triangle(const point_t &point1, const point_t &point2, const point_t &point3) :
  point1_(point1),
  point2_(point2),
  point3_(point3)
{
  assert(getArea() >= 0);
}

double Triangle::getArea() const
{
  double flank1 = getLength(point1_, point2_);
  double flank2 = getLength(point2_, point3_);
  double flank3 = getLength(point3_, point1_);
  double half_p = (flank1 + flank2 + flank3) / 2;
  double area = sqrt(half_p * (half_p - flank1) * (half_p - flank2) * (half_p - flank3));
  return area;
}

rectangle_t Triangle::getFrameRect() const
{
  double max_x = std::max({point1_.x, point2_.x, point3_.x});
  double min_x = std::min({point1_.x, point2_.x, point3_.x});
  double max_y = std::max({point1_.y, point2_.y, point3_.y});
  double min_y = std::min({point1_.y, point2_.y, point3_.y});
  return {max_x - min_x, max_y - min_y, min_x + (max_x - min_x) / 2, min_y + (max_y - min_y) / 2};
}

void Triangle::move(double dx, double dy)
{
  point1_.x += dx;
  point2_.x += dx;
  point3_.x += dx;
  point1_.y += dy;
  point2_.y += dy;
  point3_.y += dy;
}

void Triangle::move(const point_t &point)
{
  double dx = point.x - point1_.x;
  double dy = point.y - point2_.y;
  move(dx, dy);
}

void Triangle::printInfo() const
{
  const point_t center = getCenter();
  std::cout << "Triangle:" << std::endl;
  std::cout << "Point1:" << point1_.x << " " << point1_.y << std::endl;
  std::cout << "Point2:" << point2_.x << " " << point2_.y << std::endl;
  std::cout << "Point3:" << point3_.x << " " << point3_.y << std::endl;
  std::cout << "Center:" << center.x << " " << center.y << std::endl;
  std::cout << std::endl;
}

point_t Triangle::getCenter() const
{
  return {(point1_.x + point2_.x + point3_.x) / 3, (point1_.y + point2_.y + point3_.y) / 3};
}

