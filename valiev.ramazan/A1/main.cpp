#include <iostream>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

void printArea(const Shape &shape)
{
  std::cout << "Area:" << shape.getArea() << std::endl;
}

void printFrameRect(const Shape &shape)
{
  rectangle_t frameRectangle = shape.getFrameRect();
  std::cout << "Width:" << frameRectangle.width << std::endl;
  std::cout << "Height:" << frameRectangle.height << std::endl;
  std::cout << "Center:" << frameRectangle.pos.x << " " << frameRectangle.pos.y << std::endl;
  std::cout << std::endl;
}

int main()
{
  const double x = 5.0;
  point_t point1_center{x * 2, x * 2};
  point_t point2_center{20.0, 20.0};
  Rectangle rectangle({point1_center}, x, x);
  Circle circle(point1_center, x);
  const point_t point1{1.0, 1.0};
  const point_t point2{2.0, 1.0};
  const point_t point3{1.0, 2.0};
  Triangle triangle(point1, point2, point3);

  circle.printInfo();
  printArea(circle);
  printFrameRect(circle);
  circle.move(x * 3, x * 3);
  circle.printInfo();
  circle.move(point2_center);

  rectangle.printInfo();
  printArea(rectangle);
  printFrameRect(rectangle);
  rectangle.move(x, x);
  rectangle.printInfo();
  rectangle.move(point2_center);

  triangle.printInfo();
  printArea(triangle);
  printFrameRect(triangle);
  triangle.move(x * 3, x * 3);
  triangle.printInfo();
  triangle.move(point2_center);

  return 0;
}

