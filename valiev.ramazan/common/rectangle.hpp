#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"
namespace valiev
{
  class Rectangle: public Shape
  {
    public:
      Rectangle(const valiev::point_t &center, double width, double height);
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(double dx, double dy) override;
      void move(const point_t &center) override;
      void printInfo() const override;
      void scale(double multi) override;
    private:
      point_t center_;
      double width_, height_;
  };

}
#endif //RECTANGLE_HPP
