#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"
namespace valiev
{
  class Triangle: public Shape
  {
    public:
      Triangle(const point_t &point1, const point_t &point2_, const point_t &point3_);
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const point_t &point) override;
      void move(double dx, double dy) override;
      void printInfo() const override;
      point_t getCenter() const;
      void scale(double multi) override;
    private:
      point_t point1_, point2_, point3_;
  };

}
#endif //TRIANGLE_HPP
