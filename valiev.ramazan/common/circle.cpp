#include "circle.hpp"

#include <iostream>
#include <cmath>
#include <cassert>

valiev::Circle::Circle(const point_t &center, double radius):
  center_(center),
  radius_(radius)
{
  if (radius_ <= 0.0) {
    throw std::invalid_argument("Invalid radius");
  }

}

double valiev::Circle::getArea() const
{
  return radius_ * M_PI * radius_;
}

valiev::rectangle_t valiev::Circle::getFrameRect() const
{
  return {2 * radius_, 2 * radius_, center_};
}

void valiev::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void valiev::Circle::move(const point_t &center)
{
  center_ = center;
}

void valiev::Circle::printInfo() const
{
  std::cout << "Circle:" << std::endl;
  std::cout << "Center:" << center_.x << " " << center_.y << std::endl;
  std::cout << "Radius:" << radius_ << std::endl;
  std::cout << std::endl;
}

void valiev::Circle::scale(double multi)
{
  if (multi <= 0) {
    throw std::invalid_argument("Invalid multiplier");
  }
  radius_ *= multi;
}
