#include "rectangle.hpp"

#include <iostream>
#include <cassert>

valiev::Rectangle::Rectangle(const valiev::point_t &center, double width, double height):
  center_(center),
  width_(width),
  height_(height)
{
  if ((width_ <= 0) || (height_ <= 0)) {
    throw std::invalid_argument("Invalid width or height");
  }

}

double valiev::Rectangle::getArea() const
{
  return width_ * height_;
}

valiev::rectangle_t valiev::Rectangle::getFrameRect() const
{
  return {width_, height_, center_};
}

void valiev::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void valiev::Rectangle::move(const point_t &center)
{
  center_ = center;
}

void valiev::Rectangle::printInfo() const
{
  std::cout << "Rectangle:" << std::endl;
  std::cout << "Width:" << width_ << std::endl;
  std::cout << "Height:" << height_ << std::endl;
  std::cout << "Center:" << center_.x << " " << center_.y << std::endl;
  std::cout << std::endl;
}

void valiev::Rectangle::scale(double multi)
{
  if (multi <= 0 ) {
    throw std::invalid_argument("Invalid multiplier");
  }
  width_ *= multi;
  height_ *= multi;
}
