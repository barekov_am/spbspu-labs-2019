#ifndef BOOKMARKSHANDLER_HPP
#define BOOKMARKSHANDLER_HPP

#include <map>
#include "phoneBook.hpp"

class BookmarksHandler
{
public:
  enum class insertPosition
  {
    before,
    after
  };

  enum class movePosition
  {
    first,
    last
  };

  BookmarksHandler();

  void add(const PhoneBook::record_t &record);
  void store(const std::string &bookmark, const std::string &name);
  void insert(const std::string &bookmark, const PhoneBook::record_t &record, insertPosition point);
  void remove(const std::string &bookmark);
  void show(const std::string &bookmark);
  void move(const std::string &bookmark, int n);
  void move(const std::string &bookmark, movePosition pos);
  void nextRecord(const std::string &bookmark);
  void prevRecord(const std::string &bookmark);
private:
  using bookmarkType = std::map<std::string, PhoneBook::iterator>;
  using bookmarkIterator = bookmarkType::iterator;

  PhoneBook records_;
  bookmarkType bookmarks_;

  bookmarkIterator getBookmarkIterator(const std::string &bookmark);
};

#endif // BOOKMARKSHANDLER_HPP
