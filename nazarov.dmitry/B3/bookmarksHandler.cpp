#include "bookmarksHandler.hpp"
#include <iostream>
#include <algorithm>
#include <functional>

BookmarksHandler::BookmarksHandler()
{
  bookmarks_["current"] = records_.begin();
}

void BookmarksHandler::add(const PhoneBook::record_t &record)
{
  records_.pushBack(record);
  if (std::next(records_.begin()) == records_.end())
  {
    bookmarks_["current"] = records_.begin();
  }
}

void BookmarksHandler::store(const std::string &bookmark, const std::string &name)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    bookmarks_.emplace(name, iter->second);
  }
}

void BookmarksHandler::insert(const std::string &bookmark, const PhoneBook::record_t &record, insertPosition point)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {
    if (iter->second == records_.end())
    {
      add(record);
    }
    auto pos = (point == insertPosition::after) ? std::next(iter->second) : iter->second;

    if (pos == records_.end())
    {
      add(record);
    }
    records_.insert(pos, record);
  }
}

void BookmarksHandler::remove(const std::string &bookmark)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    auto deleteIter = iter->second;

    auto search = [&](auto & it)
    {
      if (it.second == deleteIter)
      {
        if (std::next(it.second) == records_.end())
        {
          it.second = records_.prev(deleteIter);
        }
        else
        {
          it.second = records_.next(deleteIter);
        }
      }
    };

    std::for_each(bookmarks_.begin(), bookmarks_.end(), search);

    records_.remove(deleteIter);
  }
}

void BookmarksHandler::show(const std::string &bookmark)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    if (records_.empty())
    {
      std::cout << "<EMPTY>\n";
      return;
    }

    records_.showCurrentRecord(iter->second);
  }
}

void BookmarksHandler::move(const std::string &bookmark, int n)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    iter->second = records_.move(iter->second, n);
  }
}

void BookmarksHandler::move(const std::string &bookmark, movePosition pos)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    switch (pos)
    {
    case movePosition::first:
    {
      iter->second = records_.begin();
      break;
    }
    case movePosition::last:
    {
      iter->second = records_.prev(records_.end());
      break;
    }
    }
  }
}

void BookmarksHandler::nextRecord(const std::string &bookmark)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    iter->second = records_.next(iter->second);
  }
}

void BookmarksHandler::prevRecord(const std::string &bookmark)
{
  bookmarkIterator iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    iter->second = records_.prev(iter->second);
  }
}

BookmarksHandler::bookmarkIterator BookmarksHandler::getBookmarkIterator(const std::string &bookmark)
{
  bookmarkIterator iter = bookmarks_.find(bookmark);
  if (iter != bookmarks_.end())
  {
    return iter;
  }

  std::cout << "<INVALID BOOKMARK>\n";
  return bookmarks_.end();
}
