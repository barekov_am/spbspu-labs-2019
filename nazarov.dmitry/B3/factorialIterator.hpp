#ifndef FACTORIALITERATOR_H
#define FACTORIALITERATOR_H

#include <iterator>

class FactorialIterator: public std::iterator<std::bidirectional_iterator_tag, long long>
{
public:
  FactorialIterator();
  FactorialIterator(int index);

  reference operator *();
  pointer operator ->();

  FactorialIterator &operator ++();
  FactorialIterator operator ++(int);

  FactorialIterator &operator --();
  FactorialIterator operator --(int);

  bool operator ==(FactorialIterator iter) const;
  bool operator !=(FactorialIterator iter) const;
private:
  long long val_;
  int index_;

  long long getValue(int index) const;
};

class FactorialContainer
{
public:
  FactorialContainer() = default;

  FactorialIterator begin();
  FactorialIterator end();
};

#endif // FACTORIALITERATOR_H
