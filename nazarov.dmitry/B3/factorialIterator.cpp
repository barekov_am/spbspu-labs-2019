#include "factorialIterator.hpp"

FactorialIterator::FactorialIterator():
  val_(1),
  index_(1)
{
}

FactorialIterator::FactorialIterator(int index):
  val_(getValue(index)),
  index_(index)
{
}

FactorialIterator::reference FactorialIterator::operator *()
{
  return val_;
}

FactorialIterator::pointer FactorialIterator::operator ->()
{
  return &val_;
}

FactorialIterator &FactorialIterator::operator ++()
{
  ++index_;
  val_ *= index_;
  return * this;
}

FactorialIterator FactorialIterator::operator ++(int)
{
  FactorialIterator tmp = *this;
  ++(*this);
  return tmp;
}

FactorialIterator FactorialIterator::operator --(int)
{
  FactorialIterator tmp = *this;
  --(*this);
  return tmp;
}

FactorialIterator &FactorialIterator::operator --()
{
  if (index_ > 1)
  {
    val_ /= index_;
    --index_;
  }
  return * this;
}

bool FactorialIterator::operator ==(FactorialIterator iter) const
{
  return (val_ == iter.val_) && (index_ == iter.index_);
}

bool FactorialIterator::operator !=(FactorialIterator iter) const
{
  return !(*this == iter);
}


long long FactorialIterator::getValue(int index) const
{
  return (index <= 1) ? 1 : (index * getValue(index - 1));
}

FactorialIterator FactorialContainer::begin()
{
  return FactorialIterator(1);
}

FactorialIterator FactorialContainer::end()
{
  return FactorialIterator(11);
}

