#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <string>

class BookmarksHandler;

void executeAdd(BookmarksHandler &handler, std::stringstream &ss);
void executeDelete(BookmarksHandler &handler, std::stringstream &ss);
void executeStore(BookmarksHandler &handler, std::stringstream &ss);
void executeInsert(BookmarksHandler &handler, std::stringstream &ss);
void executeShow(BookmarksHandler &handler, std::stringstream &ss);
void executeMove(BookmarksHandler &handler, std::stringstream &ss);

#endif // COMMANDS_HPP
