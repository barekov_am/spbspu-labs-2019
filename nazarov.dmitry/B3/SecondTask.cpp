#include <iostream>
#include <algorithm>

#include "factorialIterator.hpp"

void secondTask()
{
  FactorialContainer container;

  std::copy(container.begin(), container.end(), std::ostream_iterator<long long>(std::cout, " "));
  std::cout << "\n";

  std::reverse_copy(container.begin(), container.end(), std::ostream_iterator<long long>(std::cout, " "));
  std::cout << "\n";
}
