#include "commands.hpp"

#include <iostream>
#include <sstream>
#include <cstring>
#include <cctype>
#include <algorithm>

#include "bookmarksHandler.hpp"

std::string getNumber(std::string &num)
{
  if (num.empty())
  {
    throw std::invalid_argument("Wrong number");
  }
  for (size_t i = 0; i < num.size(); i++)
  {
    if (!std::isdigit(num[i]))
    {
      throw std::invalid_argument("Wrong number");
    }
  }
  return num;
}

std::string getName(std::string &name)
{
  if (name.empty())
  {
    throw std::invalid_argument("Wrong name");
  }
  if (name.front() != '\"')
  {
    throw std::invalid_argument("Wrong name");
  }
  name.erase(name.begin());

  size_t i = 0;
  while (i < name.size() && name[i] != '\"')
  {
    if (name[i] == '\\')
    {
      if (name[i + 1] == '\"' && i + 2 < name.size())
      {
        name.erase(i, 1);
      }
      else
      {
        throw std::invalid_argument("Wrong name");
      }
    }
    ++i;
  }

  if (i == name.size())
  {
    throw std::invalid_argument("Wrong name");
  }

  name.erase(i);

  if (name.empty())
  {
    throw std::invalid_argument("Wrong name");
  }
  return name;
}

std::string getMarkName(std::string &name)
{
  if (name.empty())
  {
    throw std::invalid_argument("Wrong mark name");
  }
  for (size_t i = 0; i < name.size(); i++)
  {
    if (!isalnum(name[i]) && (name[i] != '-'))
    {
      throw std::invalid_argument("Wrong mark name");
    }
  }
  return name;
}

void executeAdd(BookmarksHandler &handler, std::stringstream &ss)
{
  try
  {
  std::string num;
  ss >> std::ws >> num;
  num = getNumber(num);
  std::string name;
  std::getline(ss >> std::ws, name);
  name = getName(name);
  handler.add({ num, name });
  }
  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}

void executeStore(BookmarksHandler &handler, std::stringstream &ss)
{
  try
  {
  std::string markName;
  ss >> std::ws >> markName;
  markName = getMarkName(markName);
  std::string newMarkName;
  ss >> std::ws >> newMarkName;
  newMarkName = getMarkName(newMarkName);
  handler.store(markName, newMarkName);
  }
  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}

void executeInsert(BookmarksHandler &handler, std::stringstream &ss)
{
  try
  {
  struct insertPosition_t
  {
    const char * name;
    BookmarksHandler::insertPosition pos;
  };

  std::string positionData;
  ss >> std::ws >> positionData;

  insertPosition_t positions[] =
  {
    { "before", BookmarksHandler::insertPosition::before },
    { "after", BookmarksHandler::insertPosition::after }
  };

  auto predicate = [&](const insertPosition_t &pos) { return pos.name == positionData; };
  auto pos = std::find_if(std::begin(positions), std::end(positions), predicate);

  std::string markName;
  ss >> std::ws >> markName;
  markName = getMarkName(markName);
  std::string num;
  ss >> std::ws >> num;
  num = getNumber(num);
  std::string name;
  std::getline(ss >> std::ws, name);
  name = getName(name);
  handler.insert(markName, { num, name }, pos->pos);
  }
  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}

void executeDelete(BookmarksHandler &handler, std::stringstream &ss)
{
  try {
  std::string markName;
  ss >> std::ws >> markName;
  markName = getMarkName(markName);
  handler.remove(markName);
  }
  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}

void executeShow(BookmarksHandler &handler, std::stringstream &ss)
{
  try
  {
  std::string markName;
  ss >> std::ws >> markName;
  markName = getMarkName(markName);
  handler.show(markName);
  }
  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}

void executeMove(BookmarksHandler &handler, std::stringstream &ss)
{
  try
  {
  struct movePosition_t
  {
    const char * name;
    BookmarksHandler::movePosition pos;
  };

  std::string markName;
  ss >> std::ws >> markName;
  markName = getMarkName(markName);
  std::string steps;
  ss >> std::ws >> steps;

  movePosition_t positions[] =
  {
    { "first", BookmarksHandler::movePosition::first },
    { "last", BookmarksHandler::movePosition::last }
  };

  auto predicate = [&](const movePosition_t &pos) { return pos.name == steps; };
  auto pos = std::find_if(std::begin(positions), std::end(positions), predicate);

  int sign = 1;
  if (pos == std::end(positions))
  {
    if (steps.front() == '-')
    {
      sign = -1;
      steps.erase(steps.begin());
    }
    else if (steps.front() == '+')
    {
      steps.erase(steps.begin());
    }

    steps = getNumber(steps);
    handler.move(markName, std::stoi(steps) * sign);
  }
  else
  {
    handler.move(markName, pos->pos);
  }
  }
  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID STEP>\n";
    return;
  }
}
