#include "phoneBook.hpp"

#include <iostream>

void PhoneBook::showCurrentRecord(iterator iter) const
{
  std::cout << iter->name << " " << iter->phone << "\n";
}

PhoneBook::iterator PhoneBook::insert(iterator iter, const record_t &record)
{
  return book_.insert(iter, record);
}

PhoneBook::iterator PhoneBook::remove(iterator iter)
{
  return book_.erase(iter);
}

PhoneBook::iterator PhoneBook::replaceCurrentRecord(iterator iter, const record_t &record)
{
  return book_.insert(book_.erase(iter), record);
}

void PhoneBook::pushBack(const record_t &record)
{
  return book_.push_back(record);
}

PhoneBook::iterator PhoneBook::next(iterator iter)
{
  if (std::next(iter) == book_.end())
  {
    return iter;
  }
  return ++iter;
}

PhoneBook::iterator PhoneBook::prev(iterator iter)
{
  if (iter == book_.begin())
  {
    return iter;
  }
  return --iter;
}

PhoneBook::iterator PhoneBook::move(iterator iter, int num)
{
  if (num >= 0)
  {
    while (std::next(iter) != book_.end() && num > 0)
    {
      iter = std::next(iter);
      --num;
    }
  }
  else
  {
    while (iter != book_.begin() && num < 0)
    {
      iter = std::prev(iter);
      ++num;
    }
  }
  return iter;
}

PhoneBook::iterator PhoneBook::begin()
{
  return book_.begin();
}

PhoneBook::iterator PhoneBook::end()
{
  return book_.end();
}

bool PhoneBook::empty() const
{
  return book_.empty();
}
