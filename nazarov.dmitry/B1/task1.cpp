#include <iostream>
#include <vector>
#include <forward_list>

#include "utils.hpp"

void firstTask(const char *dir)
{
  auto order = getDir<int>(dir);
  std::vector<int> vectorBrackets;

  int i = 0;
  while (std::cin && !(std::cin >> i).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input error");
    }

    vectorBrackets.push_back(i);
  }

  if (vectorBrackets.empty())
  {
    return;
  }

  std::vector<int> vectorAt(vectorBrackets);
  std::forward_list<int> listIterator(vectorBrackets.begin(), vectorBrackets.end());

  sort<bracketsStrategy>(vectorBrackets, order);
  sort<atStrategy>(vectorAt, order);
  sort<iteratorStrategy>(listIterator, order);

  printCollection(vectorBrackets);
  printCollection(vectorAt);
  printCollection(listIterator);
}
