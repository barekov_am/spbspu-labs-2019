#ifndef UTILS_HPP
#define UTILS_HPP

#include <iostream>
#include <functional>
#include <cstring>
#include "strategy.hpp"

template <typename E>
std::function<bool(E, E)> getDir(const char *dir)
{
  if (strcmp(dir, "ascending") == 0)
  {
    return [](E a, E b) { return a < b; };
  }
  if (strcmp(dir, "descending") == 0)
  {
    return [](E a, E b) { return a > b; };
  }
  throw std::invalid_argument("Sorting direction is wrong.");
}

template <template <class Container> class Access, typename Container>
void sort(Container &arr, std::function<bool(typename Container::value_type, typename Container::value_type)> order)
{
  const auto begin = Access<Container>::begin(arr);
  const auto end = Access<Container>::end(arr);

  for (auto i = begin; i != end; i++)
  {
    for (auto j = Access<Container>::next(i); j != end; j++)
    {
      typename Container::reference a = Access<Container>::get(arr, j);
      typename Container::reference b = Access<Container>::get(arr, i);
      if (order(a, b))
      {
        std::swap(a, b);
      }
    }
  }
}

template <typename Container>
void printCollection(const Container &arr)
{
  if (arr.empty())
  {
    return;
  }

  for (auto elem : arr)
  {
    std::cout << elem << " ";
  }
  std::cout << "\n";
}

#endif // UTILS_HPP
