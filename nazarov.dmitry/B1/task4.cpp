#include <random>
#include <vector>

#include "utils.hpp"

void fillRandom(double *array, int size)
{
  for (int i = 0; i < size; i++)
  {
    array[i] = (rand() % 21) / 10.0 - 1;
  }
}

void fourthTask(const char *dir, int size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size must be a positive number.");
  }
  auto order = getDir<double>(dir);

  std::vector<double> vector(static_cast<unsigned int>(size));
  fillRandom(&vector[0], size);
  printCollection(vector);

  sort<bracketsStrategy>(vector, order);

  printCollection(vector);
}
