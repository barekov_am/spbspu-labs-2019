#include <vector>
#include <iterator>

#include "utils.hpp"

void thirdTask()
{
  std::vector<int> vector;

  int i = -1;
  while (std::cin && !(std::cin >> i).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input failed");
    }

    if (i == 0)
    {
      break;
    }

    vector.push_back(i);
  }

  if (vector.empty())
  {
    return;
  }

  if (i != 0)
  {
    throw std::runtime_error("\"0\" at the end is expected.");
  }

  std::vector<int>::iterator iter = vector.begin();
  if (vector.back() == 1)
  {
    while (iter != vector.end())
    {
      iter = ((*iter) % 2 == 0) ? vector.erase(iter) : ++iter;
    }
  }

  if (vector.back() == 2)
  {
    while (iter != vector.end())
    {
      iter = ((*iter) % 3 == 0) ? (vector.insert(++iter, 3, 1) + 3) : ++iter;
    }
  }

  printCollection(vector);
}
