#ifndef PARTITION_HPP_INCLUDED
#define PARTITION_HPP_INCLUDED

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace nazarov
{
  Matrix split(const nazarov::shapes_array &, size_t);
  Matrix split(const CompositeShape &);
  bool intersect(const rectangle_t &, const rectangle_t &);
}

#endif
