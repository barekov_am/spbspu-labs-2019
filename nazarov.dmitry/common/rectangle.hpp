#ifndef RECTANGLE_HPP_INCLUDED
#define RECTANGLE_HPP_INCLUDED
#include "shape.hpp"

namespace nazarov
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t &, double, double);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printFeatures() const override;

    void move(const point_t &) override;
    void move(double, double) override;
    void scale(double) override;
    void rotate(double) override;

  private:
    point_t pos_;
    double width_;
    double height_;
    double angle_;
  };
}
#endif
