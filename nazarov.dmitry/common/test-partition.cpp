#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(partitionMetodsTests)

BOOST_AUTO_TEST_CASE(partitionTest)
{
  const nazarov::Rectangle rect1({0, 0}, 2, 1);
  nazarov::shape_ptr rectOnePtr = std::make_shared<nazarov::Rectangle>(rect1);
  const nazarov::Rectangle rect2({15, 10}, 2, 3);
  nazarov::shape_ptr rectTwoPtr = std::make_shared<nazarov::Rectangle>(rect2);
  const nazarov::Circle circ({1, -1}, 3);
  nazarov::shape_ptr circPtr = std::make_shared<nazarov::Circle>(circ);

  nazarov::CompositeShape compSh(rectOnePtr);
  nazarov::Matrix matrix1 = nazarov::split(compSh);

  compSh.add(circPtr);
  nazarov::Matrix matrix2 = nazarov::split(compSh);

  compSh.add(rectTwoPtr);
  nazarov::Matrix matrix3 = nazarov::split(compSh);

  BOOST_CHECK_EQUAL(matrix1.getLines(), 1);
  BOOST_CHECK_EQUAL(matrix1.getColumns(), 1);

  BOOST_CHECK_EQUAL(matrix2.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix2.getColumns(), 1);

  BOOST_CHECK_EQUAL(matrix3.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix3.getColumns(), 2);
}

BOOST_AUTO_TEST_CASE(intersectionTest)
{
  const nazarov::Rectangle rect1({1, 2}, 5, 4);
  const nazarov::Rectangle rect2({7, 7}, 3, 1);
  const nazarov::Circle circ1({-1, 2}, 2);
  const nazarov::Circle circ2({0, 1}, 4);

  BOOST_CHECK(intersect(rect1.getFrameRect(), circ1.getFrameRect()));
  BOOST_CHECK(intersect(rect1.getFrameRect(), circ2.getFrameRect()));
  BOOST_CHECK(!intersect(rect1.getFrameRect(), rect2.getFrameRect()));
  BOOST_CHECK(!intersect(circ1.getFrameRect(), rect2.getFrameRect()));
  BOOST_CHECK(!intersect(circ2.getFrameRect(), rect2.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
