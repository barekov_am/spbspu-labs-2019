#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

const double EPS = 0.01;

BOOST_AUTO_TEST_SUITE(CircleTestSuite)

BOOST_AUTO_TEST_CASE(moveCircleConstant)
{
  nazarov::Circle circleTest({0, 0}, 20);
  const nazarov::rectangle_t beforeFrame = circleTest.getFrameRect();
  const double beforeArea = circleTest.getArea();

  circleTest.move(5, 5);
  BOOST_CHECK_CLOSE(beforeFrame.width, circleTest.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(beforeFrame.height, circleTest.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(beforeArea, circleTest.getArea(), EPS);

  circleTest.move({20, 20});
  BOOST_CHECK_CLOSE(beforeFrame.width, circleTest.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(beforeFrame.height, circleTest.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(beforeArea, circleTest.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(scaleCircleIncreaseArea)
{
  nazarov::Circle circleTest({0, 0}, 20);
  const double beforeArea = circleTest.getArea();
  const double scaleFactor = 3;

  circleTest.scale(scaleFactor);
  BOOST_CHECK_CLOSE(beforeArea * scaleFactor * scaleFactor, circleTest.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(scaleCircleDecreaseArea)
{
  nazarov::Circle circleTest({0, 0}, 20);
  const double beforeArea = circleTest.getArea();
  const double scaleFactor = 3;

  circleTest.scale(scaleFactor);
  BOOST_CHECK_CLOSE(beforeArea * scaleFactor * scaleFactor, circleTest.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  nazarov::Circle circleTest({6, 3}, 4);
  const double testArea = circleTest.getArea();
  const nazarov::rectangle_t testFrameRect = circleTest.getFrameRect();
  const double angle = 90;

  circleTest.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, circleTest.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(testFrameRect.height, circleTest.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(testArea, circleTest.getArea(), EPS);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, circleTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, circleTest.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_CASE(invalidCircleArguments)
{
  BOOST_CHECK_THROW(nazarov::Circle circleTest({0, 0}, -20), std::invalid_argument);

  nazarov::Circle circleTest({0, 0}, 20);
  BOOST_CHECK_THROW(circleTest.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
