#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

const double EPS = 0.01;

BOOST_AUTO_TEST_SUITE(compositeTests)

BOOST_AUTO_TEST_CASE(compositeShapeCopyConstructor)
{
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {4, 7}, 2, 12);
  nazarov::shape_ptr circPtr = std::make_shared<nazarov::Circle>(nazarov::point_t {2, 1}, 10);
  nazarov::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);
  const nazarov::rectangle_t frameRect = compSh.getFrameRect();

  nazarov::CompositeShape copyCompSh(compSh);
  const nazarov::rectangle_t copyFrameRect = copyCompSh.getFrameRect();
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, EPS);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, EPS);
  BOOST_CHECK_CLOSE(compSh.getArea(), copyCompSh.getArea(), EPS);
  BOOST_CHECK_EQUAL(compSh.getSize(), copyCompSh.getSize());
}

BOOST_AUTO_TEST_CASE(compositeShapeCopyOperator)
{
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {4, 7}, 2, 12);
  nazarov::shape_ptr circPtr = std::make_shared<nazarov::Circle>(nazarov::point_t {2, 1}, 10);
  nazarov::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);
  const nazarov::rectangle_t frameRect = compSh.getFrameRect();

  nazarov::CompositeShape copyCompSh(rectPtr);
  copyCompSh = compSh;
  const nazarov::rectangle_t copyFrameRect = copyCompSh.getFrameRect();
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, EPS);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, EPS);
  BOOST_CHECK_CLOSE(compSh.getArea(), copyCompSh.getArea(), EPS);
  BOOST_CHECK_EQUAL(compSh.getSize(), copyCompSh.getSize());
}

BOOST_AUTO_TEST_CASE(compositeShapeMoveConstructor)
{
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {4, 7}, 2, 12);
  nazarov::shape_ptr circPtr = std::make_shared<nazarov::Circle>(nazarov::point_t {2, 1}, 10);
  nazarov::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);
  const nazarov::rectangle_t frameRect = compSh.getFrameRect();
  const double compShArea = compSh.getArea();
  const int compShCount = compSh.getSize();

  nazarov::CompositeShape moveCompSh(std::move(compSh));
  const nazarov::rectangle_t moveFrameRect = moveCompSh.getFrameRect();

  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, EPS);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, EPS);
  BOOST_CHECK_CLOSE(compShArea, moveCompSh.getArea(), EPS);
  BOOST_CHECK_EQUAL(compShCount, moveCompSh.getSize());
  BOOST_CHECK_CLOSE(compSh.getArea(), 0, EPS);
  BOOST_CHECK_EQUAL(compSh.getSize(), 0);
}

BOOST_AUTO_TEST_CASE(compositeShapeMoveOperator)
{
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {4, 7}, 2, 12);
  nazarov::shape_ptr circPtr = std::make_shared<nazarov::Circle>(nazarov::point_t {2, 1}, 10);
  nazarov::CompositeShape movedCompSh(rectPtr);
  movedCompSh.add(circPtr);
  const nazarov::rectangle_t frameRect = movedCompSh.getFrameRect();
  const double compShArea = movedCompSh.getArea();
  const int compShCount = movedCompSh.getSize();

  nazarov::CompositeShape compSh(rectPtr);
  compSh = std::move(movedCompSh);
  const nazarov::rectangle_t moveFrameRect = compSh.getFrameRect();
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, EPS);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, EPS);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, EPS);
  BOOST_CHECK_CLOSE(compShArea, compSh.getArea(), EPS);
  BOOST_CHECK_EQUAL(compShCount, compSh.getSize());
  BOOST_CHECK_EQUAL(movedCompSh.getSize(), 0);
  BOOST_CHECK_CLOSE(movedCompSh.getArea(), 0, EPS);
}

BOOST_AUTO_TEST_CASE(compositeShapeConstancyOfParameters)
{
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {4, 7}, 2, 12);
  nazarov::shape_ptr circPtr = std::make_shared<nazarov::Circle>(nazarov::point_t {2, 1}, 10);
  nazarov::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);
  const double areaBeforeMoving = compSh.getArea();
  const nazarov::rectangle_t frameRectBeforeMoving = compSh.getFrameRect();
  compSh.move(3, 5);
  nazarov::rectangle_t frameRectAfterMoving = compSh.getFrameRect();
  double areaAfterMoving = compSh.getArea();

  BOOST_CHECK_CLOSE(areaAfterMoving, areaBeforeMoving, EPS);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.height, frameRectAfterMoving.height, EPS);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.width, frameRectAfterMoving.width, EPS);

  compSh.move({3, 4});
  frameRectAfterMoving = compSh.getFrameRect();
  areaAfterMoving = compSh.getArea();

  BOOST_CHECK_CLOSE(areaAfterMoving, areaBeforeMoving, EPS);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.height, frameRectAfterMoving.height, EPS);
  BOOST_CHECK_CLOSE(frameRectBeforeMoving.width, frameRectAfterMoving.width, EPS);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleCoefficientMoreThanOne)
{
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {4, 7}, 2, 12);
  nazarov::shape_ptr circPtr = std::make_shared<nazarov::Circle>(nazarov::point_t {2, 1}, 10);
  nazarov::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);
  const nazarov::rectangle_t frameBeforeScale = compSh.getFrameRect();
  const int coefMoreThanOne = 3;
  compSh.scale(coefMoreThanOne);
  nazarov::rectangle_t frameAfterScale = compSh.getFrameRect();

  BOOST_CHECK_CLOSE(frameBeforeScale.height * coefMoreThanOne, frameAfterScale.height, EPS);
  BOOST_CHECK_CLOSE(frameBeforeScale.width * coefMoreThanOne, frameAfterScale.width, EPS);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.x, frameAfterScale.pos.x, EPS);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.y, frameAfterScale.pos.y, EPS);
  BOOST_CHECK(frameBeforeScale.height < frameAfterScale.height);
  BOOST_CHECK(frameBeforeScale.width < frameAfterScale.width);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleCoefficientLessThanOne)
{
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {4, 7}, 2, 12);
  nazarov::shape_ptr circPtr = std::make_shared<nazarov::Circle>(nazarov::point_t {2, 1}, 10);
  nazarov::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);
  const nazarov::rectangle_t frameBeforeScale = compSh.getFrameRect();
  const double coefLessThanOne = 0.3;
  compSh.scale(coefLessThanOne);
  nazarov::rectangle_t frameAfterScale = compSh.getFrameRect();

  BOOST_CHECK_CLOSE(frameBeforeScale.height * coefLessThanOne, frameAfterScale.height, EPS);
  BOOST_CHECK_CLOSE(frameBeforeScale.width * coefLessThanOne, frameAfterScale.width, EPS);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.x, frameAfterScale.pos.x, EPS);
  BOOST_CHECK_CLOSE(frameBeforeScale.pos.y, frameAfterScale.pos.y, EPS);
  BOOST_CHECK(frameBeforeScale.width > frameAfterScale.width);
  BOOST_CHECK(frameBeforeScale.height > frameAfterScale.height);
}

BOOST_AUTO_TEST_CASE(compositeShapeIncorrectScaleParameter)
{
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {4, 7}, 2, 12);
  nazarov::shape_ptr circPtr = std::make_shared<nazarov::Circle>(nazarov::point_t {2, 1}, 10);
  nazarov::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);

  BOOST_CHECK_THROW(compSh.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShapeAreaAfterAddAndDelete)
{
  nazarov::Rectangle rect({4, 7}, 2, 12);
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(rect);
  nazarov::Circle circ({2, 1}, 10);
  nazarov::shape_ptr circPtr = std::make_shared<nazarov::Circle>(circ);
  nazarov::CompositeShape compSh(rectPtr);
  const double compShAreaBeforeAdd = compSh.getArea();
  const double rectArea = rect.getArea();
  const double circArea = circ.getArea();

  compSh.add(circPtr);
  double compShAreaAfterAdd = compSh.getArea();
  BOOST_CHECK_CLOSE(compShAreaBeforeAdd + circArea, compShAreaAfterAdd, EPS);

  compSh.remove(0);
  BOOST_CHECK_CLOSE(compShAreaAfterAdd - rectArea, compSh.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(compositeShapeAfterRotating)
{
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {-1, 0}, 2, 2);
  nazarov::shape_ptr circPtr = std::make_shared<nazarov::Circle>(nazarov::point_t {1, 0}, 1);
  nazarov::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);

  const double compShAreaBeforeRotating = compSh.getArea();
  const nazarov::rectangle_t frameBeforeRotating = compSh.getFrameRect();

  compSh.rotate(90);
  double compShAreaAfterRotating = compSh.getArea();
  nazarov::rectangle_t frameAfterRotating = compSh.getFrameRect();

  BOOST_CHECK_CLOSE(compShAreaBeforeRotating, compShAreaAfterRotating, EPS);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.x, frameAfterRotating.pos.x, EPS);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.y, frameAfterRotating.pos.y, EPS);
  BOOST_CHECK_CLOSE(frameBeforeRotating.width, frameAfterRotating.height, EPS);
  BOOST_CHECK_CLOSE(frameBeforeRotating.height, frameAfterRotating.width, EPS);

  compSh.rotate(-90);
  compShAreaAfterRotating = compSh.getArea();
  frameAfterRotating = compSh.getFrameRect();

  BOOST_CHECK_CLOSE(compShAreaBeforeRotating, compShAreaAfterRotating, EPS);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.x, frameAfterRotating.pos.x, EPS);
  BOOST_CHECK_CLOSE(frameBeforeRotating.pos.y, frameAfterRotating.pos.y, EPS);
  BOOST_CHECK_CLOSE(frameBeforeRotating.width, frameAfterRotating.width, EPS);
  BOOST_CHECK_CLOSE(frameBeforeRotating.height, frameAfterRotating.height, EPS);
  
  compSh.rotate(45);
  compShAreaAfterRotating = compSh.getArea();
  
  BOOST_CHECK_CLOSE(compShAreaBeforeRotating, compShAreaAfterRotating, EPS);
  BOOST_CHECK_CLOSE(compSh.getFigures()[0]->getFrameRect().pos.x, compSh.getFigures()[0]->getFrameRect().pos.y, EPS);
  BOOST_CHECK_CLOSE(compSh.getFigures()[1]->getFrameRect().pos.x, compSh.getFigures()[1]->getFrameRect().pos.y, EPS);
  BOOST_CHECK_CLOSE(compSh.getFigures()[0]->getFrameRect().pos.x, - compSh.getFigures()[1]->getFrameRect().pos.x, EPS);
  BOOST_CHECK_CLOSE(compSh.getFigures()[0]->getFrameRect().pos.y, - compSh.getFigures()[1]->getFrameRect().pos.y, EPS);
}

BOOST_AUTO_TEST_CASE(compositeShapeThrowingExeptions)
{
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {4, 7}, 2, 12);
  nazarov::shape_ptr circPtr = std::make_shared<nazarov::Circle>(nazarov::point_t {2, 1}, 10);
  nazarov::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);

  BOOST_CHECK_THROW(compSh.remove(10), std::invalid_argument);
  BOOST_CHECK_THROW(compSh.remove(-10), std::invalid_argument);

  compSh.remove(1);
  BOOST_CHECK_THROW(compSh[1], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeTestThrowExceptionAfterUsingOfOperator)
{
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {4, 7}, 2, 12);
  nazarov::shape_ptr circPtr = std::make_shared<nazarov::Circle>(nazarov::point_t {2, 1}, 10);
  nazarov::CompositeShape compSh(rectPtr);
  compSh.add(circPtr);

  BOOST_CHECK_THROW(compSh[2], std::out_of_range);
  BOOST_CHECK_THROW(compSh[-2], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(emptyCompositeShapeThrowingExeptions)
{
  nazarov::CompositeShape emptyCompSh;

  BOOST_CHECK_THROW(emptyCompSh.move(3, 5), std::logic_error);
  BOOST_CHECK_THROW(emptyCompSh.move({1, 1}), std::logic_error);
  BOOST_CHECK_THROW(emptyCompSh.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(emptyCompSh.scale(2), std::logic_error);
}

BOOST_AUTO_TEST_SUITE_END()
