#include "triangle.hpp"
#include <stdexcept>
#include <cmath>
#include <algorithm>
#include <iostream>

nazarov::Triangle::Triangle(const point_t &A, const point_t &B, const point_t &C) :
  pos_({ (A.x + B.x + C.x) / 3, (A.y + B.y + C.y) / 3 }),
  point1_(A),
  point2_(B),
  point3_(C)
{
  if (getArea() <= 0)
  {
    throw std::invalid_argument("Triangle points must not have the same coordinates");
  }
}

double nazarov::Triangle::getArea() const
{
  return (std::fabs((point1_.x - point3_.x) * (point2_.y - point3_.y)
    - (point2_.x - point3_.x) * (point1_.y - point3_.y)) / 2);
}

nazarov::rectangle_t nazarov::Triangle::getFrameRect() const
{
  point_t min = { std::min(std::min(point1_.x, point2_.x), point3_.x), std::min(std::min(point1_.y, point2_.y), point3_.y) };
  point_t max = { std::max(std::max(point1_.x, point2_.x), point3_.x), std::max(std::max(point1_.y, point2_.y), point3_.y) };
  return rectangle_t{ {(max.x + min.x) / 2, (max.y + min.y) / 2}, max.x - min.x, max.y - min.y };
}

void nazarov::Triangle::move(const point_t &newPos)
{
  move(newPos.x - pos_.x, newPos.y - pos_.y);
}

void nazarov::Triangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;

  point1_.x += dx;
  point1_.y += dy;

  point2_.x += dx;
  point2_.y += dy;

  point3_.x += dx;
  point3_.y += dy;
}

void nazarov::Triangle::printFeatures() const
{
  rectangle_t framingRectangle = getFrameRect();
  std::cout << "Triangle point A is at position ( " << point1_.x << ';' << point1_.y << " )";
  std::cout << "\n point B is at position ( " << point2_.x << ';' << point2_.y << " )";
  std::cout << "\n point C is at position ( " << point3_.x << ';' << point3_.y << " )";
  std::cout << "\n Triangle centre is at position ( " << pos_.x << "," << pos_.y << " )";
  std::cout << "\n Triangle frame width is " << framingRectangle.width;
  std::cout << "\n Triangle frame height is " << framingRectangle.height;
  std::cout << "\n Triangle area is " << getArea() << "\n\n";
}

void nazarov::Triangle::scale(double times)
{
  if (times <= 0)
  {
    throw std::invalid_argument("'times' should be positive");
  }
  point1_.x = pos_.x + (point1_.x - pos_.x) * times;
  point1_.y = pos_.y + (point1_.y - pos_.y) * times;

  point2_.x = pos_.x + (point2_.x - pos_.x) * times;
  point2_.y = pos_.y + (point2_.y - pos_.y) * times;

  point3_.x = pos_.x + (point3_.x - pos_.x) * times;
  point3_.y = pos_.y + (point3_.y - pos_.y) * times;
}

void nazarov::Triangle::rotate(double angle)
{
  const double sin_a = sin(angle * M_PI / 180);
  const double cos_a = cos(angle * M_PI / 180);

  const double Ax = point1_.x - pos_.x;
  const double Ay = point1_.y - pos_.y;

  const double Bx = point2_.x - pos_.x;
  const double By = point2_.y - pos_.y;

  const double Cx = point3_.x - pos_.x;
  const double Cy = point3_.y - pos_.y;

  point1_.x = Ax * cos_a - Ay * sin_a + pos_.x;
  point1_.y = Ax * sin_a + Ay * cos_a + pos_.y;
  point2_.x = Bx * cos_a - By * sin_a + pos_.x;
  point2_.y = Bx * sin_a + By * cos_a + pos_.y;
  point3_.x = Cx * cos_a - Cy * sin_a + pos_.x;
  point3_.y = Cx * sin_a + Cy * cos_a + pos_.y;
}
