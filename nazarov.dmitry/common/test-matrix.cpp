#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(matrixTests)

BOOST_AUTO_TEST_CASE(matrixCopyConstructor)
{
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {4, 7}, 2, 12);
  nazarov::CompositeShape compSh(rectPtr);
  nazarov::Matrix copiedMatrix = split(compSh);

  nazarov::Matrix matrix(copiedMatrix);

  BOOST_CHECK_EQUAL(matrix.getLines(), copiedMatrix.getLines());
  BOOST_CHECK_EQUAL(matrix.getColumns(), copiedMatrix.getColumns());
  BOOST_CHECK(matrix == copiedMatrix);
}

BOOST_AUTO_TEST_CASE(matrixCopyOperator)
{
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {4, 7}, 2, 12);
  nazarov::CompositeShape compSh(rectPtr);
  nazarov::Matrix copiedMatrix = split(compSh);

  nazarov::Matrix matrix;
  matrix = copiedMatrix;

  BOOST_CHECK_EQUAL(matrix.getLines(), copiedMatrix.getLines());
  BOOST_CHECK_EQUAL(matrix.getColumns(), copiedMatrix.getColumns());
  BOOST_CHECK(matrix == copiedMatrix);
}

BOOST_AUTO_TEST_CASE(matrixMoveConstructor)
{
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {4, 7}, 2, 12);
  nazarov::CompositeShape compSh(rectPtr);
  nazarov::Matrix matrix = split(compSh);

  nazarov::Matrix copyOfMatrix(matrix);
  nazarov::Matrix moveMatrix(std::move(matrix));

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyOfMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyOfMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyOfMatrix);
  BOOST_CHECK_EQUAL(matrix.getLines(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(matrixMoveOperator)
{
  nazarov::shape_ptr rectPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {4, 7}, 2, 12);
  nazarov::CompositeShape compSh(rectPtr);
  nazarov::Matrix matrix = split(compSh);

  nazarov::Matrix copyOfMatrix(matrix);
  nazarov::Matrix moveMatrix;
  moveMatrix = std::move(matrix);

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyOfMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyOfMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyOfMatrix);
  BOOST_CHECK_EQUAL(matrix.getLines(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(testMatrixUsingOfEqualOperator)
{
  nazarov::shape_ptr rectOnePtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {1, 2}, 5, 4);
  nazarov::shape_ptr rectTwoPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {7, 7}, 3, 1);
  nazarov::shape_ptr circOnePtr = std::make_shared<nazarov::Circle>(nazarov::point_t {-1, 2}, 2);
  nazarov::shape_ptr circTwoPtr = std::make_shared<nazarov::Circle>(nazarov::point_t {0, 1}, 4);

  nazarov::CompositeShape compSh(rectOnePtr);
  compSh.add(circOnePtr);
  compSh.add(rectTwoPtr);

  nazarov::Matrix matrix = split(compSh);
  nazarov::Matrix equalMatrix(matrix);

  nazarov::Matrix unequalMatrix(matrix);
  unequalMatrix.add(circTwoPtr, 1, 1);

  BOOST_CHECK(matrix == equalMatrix);
  BOOST_CHECK(matrix != unequalMatrix);
}

BOOST_AUTO_TEST_CASE(matrixThrowExceptionAfterUsingOfOperator)
{
  nazarov::shape_ptr rectOnePtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {1, 2}, 5, 4);
  nazarov::shape_ptr rectTwoPtr = std::make_shared<nazarov::Rectangle>(nazarov::point_t {7, 7}, 3, 1);
  nazarov::shape_ptr circOnePtr = std::make_shared<nazarov::Circle>(nazarov::point_t {-1, 2}, 2);
  nazarov::shape_ptr circTwoPtr = std::make_shared<nazarov::Circle>(nazarov::point_t {0, 1}, 4);

  nazarov::CompositeShape compSh(rectOnePtr);
  compSh.add(circOnePtr);
  compSh.add(rectTwoPtr);
  compSh.add(circTwoPtr);

  nazarov::Matrix matrix = split(compSh);

  BOOST_CHECK_THROW(matrix[10][5], std::out_of_range);
  BOOST_CHECK_NO_THROW(matrix[2][1]);
}

BOOST_AUTO_TEST_SUITE_END()
