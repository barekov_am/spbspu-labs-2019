#include <iostream>

#include "circle.hpp"
#include "rectangle.hpp"

int main()
{
  Circle circle(3, {4, 4});
  Shape * circlePtr = &circle;
  point_t circleCenter = {0, 0};

  std::cout << "Moving Circle." << std::endl;
  std::cout << "Before moving:" << std::endl;
  std::cout << "Area: " << circlePtr->getArea() << std::endl;
  circleCenter = circlePtr->getFrameRect().pos;
  std::cout << "Coordinates: (" << circleCenter.x << ", " << circleCenter.y << ")" << std::endl;
  std::cout << "Move to (16,43)" << std::endl;
  circlePtr->move({16, 43});
  std::cout << "After moving:" << std::endl;
  std::cout << "Area: " << circlePtr->getArea() << std::endl;
  circleCenter = circlePtr->getFrameRect().pos;
  std::cout << "Coordinates: (" << circleCenter.x << ", " << circleCenter.y << ")" << std::endl;

  Rectangle rect(12, 3, {22, 1});
  Shape * rectPtr = &rect;
  point_t rectCenter = {0, 0};

  std::cout << "Moving Rectangle." << std::endl;
  std::cout << "Before moving:" << std::endl;
  std::cout << "Area: " << rectPtr->getArea() << std::endl;
  rectCenter = rectPtr->getFrameRect().pos;
  std::cout << "Coordinates: (" << rectCenter.x << ", " << rectCenter.y << ")" << std::endl;
  std::cout << "Move along oX 5 and oY 4" << std::endl;
  rectPtr->move(5, 4);
  std::cout << "After moving:" << std::endl;
  std::cout << "Area: " << rectPtr->getArea() << std::endl;
  rectCenter = rectPtr->getFrameRect().pos;
  std::cout << "Coordinates: (" << rectCenter.x << ", " << rectCenter.y << ")" << std::endl;
}
