#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"

class Shape
{
public:
  virtual ~Shape() = default;
  virtual double getArea() const = 0;
  virtual rectangle_t getFrameRect() const = 0;
  virtual void move(const double deltaX, const double deltaY) = 0;
  virtual void move(const point_t & targetPoint)  = 0;
protected:
  point_t m_center;
};

#endif
