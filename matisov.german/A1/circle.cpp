#include <iostream>
#include <cmath>
#include <cassert>

#include "circle.hpp"

Circle::Circle(const double radius, const point_t & center):    
  m_center(center),
  m_radius(radius)
{
  assert(radius > 0);
}

double Circle::getArea() const
{
  return (M_PI * pow(m_radius,2.0));
}

rectangle_t Circle::getFrameRect() const
{
  return { 2 * m_radius, 2 * m_radius, m_center };
}

void Circle::move(const double deltaX, const double deltaY) 
{
  m_center.x += deltaX;
  m_center.y += deltaY;
}

void Circle::move(const point_t & targetPoint)
{
  m_center = targetPoint;
}
