#include <iostream>
#include <stdexcept>
#include <cmath>
#include <cassert>

#include "rectangle.hpp"

Rectangle::Rectangle(const double width, const double height, const point_t &center): 
  m_center(center),
  m_width(width),
  m_height(height)
{
  assert((width > 0) && (height > 0));
}

double Rectangle::getArea() const
{
  return (m_height * m_width);
}

rectangle_t Rectangle::getFrameRect() const
{
  return {m_width, m_height, m_center};
}

void Rectangle::move(const double deltaX, const double deltaY)
{
  m_center.x += deltaX;
  m_center.y += deltaY;
}

void Rectangle::move(const point_t &targetPoint)
{
  m_center = targetPoint;
}
