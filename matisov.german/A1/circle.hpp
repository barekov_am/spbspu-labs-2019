#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

class Circle : public Shape
{
  public:
    Circle(const double radius, const point_t & center);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const double x, const double y) override;
    void move(const point_t & targetPoint) override;
  protected:
    point_t m_center;
  private:
    double m_radius;
};

#endif
