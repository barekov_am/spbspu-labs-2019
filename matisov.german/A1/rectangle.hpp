#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

class Rectangle: public Shape
{
public:
  Rectangle(const double width, const double height, const point_t & center);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t & point) override;
  void move(const double deltaX, const double deltaY) override;
protected:
  point_t m_center;
private:
  double m_width, m_height;
};

#endif
