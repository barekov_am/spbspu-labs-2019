#include <iostream>
#include <set>
#include <string>

void task1()
{
  std::string stream;
  std::set<std::string> set;

  while (std::getline(std::cin >> std::ws, stream))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed while reading data.\n");
    }

    while (stream.find_first_of(" \t\n") == 0)
    {
      stream.erase(0, 1);
    }

    while (!stream.empty())
    {
      auto pos = stream.find_first_of(" \t");
      set.insert(stream.substr(0, pos));
      stream.erase(0, pos);

      while (stream.find_first_of(" \t\n") == 0)
      {
        stream.erase(0, 1);
      }
    }
  }
  for (const auto & it : set)
  {
    std::cout << it << '\n';
  }
}
