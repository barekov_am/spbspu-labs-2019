#include <iostream>
#include <string>

void task1();
void task2();

int main(int argc, char *argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Incorrect count of arguments.\n";
      return 1;
    }

    int taskNumber = std::stoi(argv[1]);
    switch (taskNumber)
    {
      case 1:
        task1();
        break;

      case 2:
        task2();
        break;

      default:
        std::cerr << "Incorrect task number.\n";
        return 1;
    }
  }
  catch (std::exception& exception)
  {
    std::cerr << exception.what();
    return 1;
  }

  return 0;
}
