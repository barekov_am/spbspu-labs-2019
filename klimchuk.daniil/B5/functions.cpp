
#include <algorithm>
#include "functions.hpp"


int getLength(const Point& a, const Point& b)
{
  return ((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

bool isTriangle(const Shape& shape)
{
  return (shape.size() == triangleVertices);
}

bool isSquare(const Shape& shape)
{
  return (shape.size() == squareVertices) && (getLength(shape[0], shape[1]) == getLength(shape[1], shape[2]));
}

bool isRectangle(const Shape& shape)
{
  return (shape.size() == squareVertices) && (getLength(shape[0], shape[2]) == getLength(shape[1], shape[3]));
}

bool comparison(const Shape& a, const Shape& b)
{
  if (a.size() < b.size())
  {
    return true;
  }
  if ((a.size() == squareVertices) && (b.size() == squareVertices))
  {
    if (isSquare(a))
    {
      return !isSquare(b);
    }
  }
  return false;
}

void deletePentagons(std::vector<Shape>& shapes)
{
  shapes.erase(std::remove_if(shapes.begin(), shapes.end(),
      [&](const Shape& shape) { return shape.size() == pentagonVertices; }), shapes.end());
}
