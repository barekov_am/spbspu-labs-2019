#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include "functions.hpp"

std::vector<Shape> read()
{
  std::vector<Shape> shapes;
  std::string stream;

  while (std::getline(std::cin >> std::ws, stream))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed while reading data.\n");
    }

    auto openBracket = stream.find_first_of('(');
    if (openBracket == std::string::npos)
    {
      throw std::invalid_argument("Invalid parameters.\n");
    }
    int verticesCount = std::stoi(stream.substr(0, openBracket - 1));
    stream.erase(0, openBracket);
    if (verticesCount < 3)
    {
      throw std::invalid_argument("Incorrect number of vertices.\n");
    }

    size_t separator;
    size_t closeBracket;
    Shape shape;

    if (stream.empty())
    {
      throw std::invalid_argument("Incorrect number of arguments.\n");
    }

    for (int i = 0; i < verticesCount; ++i)
    {
      openBracket = stream.find_first_of('(');
      if (openBracket == std::string::npos)
      {
        throw std::invalid_argument("Invalid parameters.\n");
      }
      separator = stream.find_first_of(';');
      if (separator == std::string::npos)
      {
        throw std::invalid_argument("Invalid parameters.\n");
      }
      closeBracket = stream.find_first_of(')');
      if (closeBracket == std::string::npos)
      {
        throw std::invalid_argument("Invalid parameters.\n");
      }

      int x = std::stoi(stream.substr(openBracket + 1, separator - (openBracket + 1)));
      int y = std::stoi(stream.substr(separator + 1, closeBracket - (separator + 1)));
      stream.erase(0, closeBracket + 1);
      shape.push_back({x, y});
    }

    while (stream.find_first_of(" \t") == 0)
    {
      stream.erase(0, 1);
    }

    if (!stream.empty())
    {
      throw std::invalid_argument("Incorrect number of arguments.\n");
    }
    shapes.push_back(shape);
  }
  return shapes;
}

void task2()
{
  std::vector<Shape> shapes = read();

  size_t verticesCount = 0;
  size_t trianglesCount = 0;
  size_t squaresCount = 0;
  size_t rectanglesCount = 0;

  std::for_each(shapes.begin(), shapes.end(), [&](const Shape& shape)
  {
    verticesCount += shape.size();
    if (isTriangle(shape))
    {
      ++trianglesCount;
    }
    else if (isRectangle(shape))
    {
      ++rectanglesCount;
      if (isSquare(shape))
      {
        ++squaresCount;
      }
    }
  });
  std::cout << "Vertices: " << verticesCount << "\n";
  std::cout << "Triangles: " << trianglesCount << "\n";
  std::cout << "Squares: " << squaresCount << "\n";
  std::cout << "Rectangles: " << rectanglesCount << "\n";

  deletePentagons(shapes);

  Shape points(shapes.size());
  std::transform(shapes.begin(), shapes.end(), points.begin(), [&](const Shape& shape) {return shape[0];});
  std::sort(shapes.begin(), shapes.end(), comparison);
  std::cout << "Points: ";
  for (const auto & point : points)
  {
    std::cout << '(' << point.x << ';' << point.y << ") ";
  }

  std::cout << "\nShapes: \n";
  for (const auto & shape : shapes)
  {
    std::cout << shape.size() << " ";
    for (const auto & vertex : shape)
    {
      std::cout << '(' << vertex.x << ';' << vertex.y << ") ";
    }
    std::cout << '\n';
  }
}
