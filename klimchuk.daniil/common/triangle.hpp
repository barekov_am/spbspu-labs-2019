#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace klimchuk
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &pointA, const point_t &pointB, const point_t &pointC);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &pos) override;
    void move(double dx, double dy) override;
    void scale(double factor) override;
    void printInfo() const override;
    void rotate(double angle) override;

  private:
    point_t A_;
    point_t B_;
    point_t C_;
    double angle_;
    point_t getCenter() const;
    static double getDistance(const point_t &pointStart, const point_t &pointEnd);
  };
}

#endif //TRIANGLE_HPP
