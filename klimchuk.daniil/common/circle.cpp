#include "circle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>

klimchuk::Circle::Circle(double radius, const point_t &center) :
  radius_(radius),
  pos_(center)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("Invalid circle parameters!");
  }
}

double klimchuk::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

klimchuk::rectangle_t klimchuk::Circle::getFrameRect() const
{
  return {2.0 * radius_, 2.0 * radius_, pos_};
}

void klimchuk::Circle::move(const point_t &pos)
{
  pos_ = pos;
}

void klimchuk::Circle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void klimchuk::Circle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Invalid Circle scale factor!");
  }
  radius_ *= factor;
}

void klimchuk::Circle::rotate(double)
{}

void klimchuk::Circle::printInfo() const
{
  rectangle_t frame = getFrameRect();
  std::cout << "Circle:"
      << std::endl << "  Radius: " << radius_
      << std::endl << "  Center: "
      << std::endl << "    x: " << pos_.x
      << std::endl << "    y: " << pos_.y
      << std::endl << "  Frame rectangle for the circle: "
      << std::endl << "    Width: " << frame.width
      << std::endl << "    Height: " << frame.height
      << std::endl << "  Area: " << this->getArea()
      << std::endl << std::endl;
}
