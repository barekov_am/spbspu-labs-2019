#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

namespace klimchuk
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &other);
    Matrix(Matrix &&other) noexcept;
    ~Matrix() = default;

    Matrix& operator =(const Matrix &other);
    Matrix& operator =(Matrix &&other) noexcept;
    dynamicArray operator [](size_t) const;
    bool operator ==(const Matrix &other) const;
    bool operator !=(const Matrix &other) const;

    size_t getLines() const;
    size_t getColumns() const;
    void add(const shapePtr& other, size_t line, size_t column);
    void printInfo() const;

  private:
    size_t lines_;
    size_t columns_;
    dynamicArray list_;
  };
}

#endif //MATRIX_HPP
