#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

#define INACCURACY 0.0001

BOOST_AUTO_TEST_SUITE(CircleTests)

BOOST_AUTO_TEST_CASE(CircleMoveDxy)
{
  klimchuk::Circle circleTest(8, {10, 9});
  const double widthBefore = circleTest.getFrameRect().width;
  const double heightBefore = circleTest.getFrameRect().height;
  const double areaBefore = circleTest.getArea();
  circleTest.move(7, 0);
  BOOST_CHECK_CLOSE(widthBefore, circleTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, circleTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, circleTest.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(CircleMovePoint)
{
  klimchuk::Circle circleTest(4, {6, 5});
  const double widthBefore = circleTest.getFrameRect().width;
  const double heightBefore = circleTest.getFrameRect().height;
  const double areaBefore = circleTest.getArea();
  circleTest.move({3, 2});
  BOOST_CHECK_CLOSE(widthBefore, circleTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, circleTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, circleTest.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(CircleScale)
{
  klimchuk::Circle circleTest(5, {7, 6});
  const double areaBefore = circleTest.getArea();
  const double factor = 2;
  circleTest.scale(factor);
  BOOST_CHECK_CLOSE(areaBefore * factor * factor, circleTest.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(CircleInvalidParameters)
{
  BOOST_CHECK_THROW(klimchuk::Circle(-10, {1, 1}), std::invalid_argument);
  klimchuk::Circle circleTest(5, {1, 5});
  BOOST_CHECK_THROW(circleTest.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(CircleParametersAfterRotation)
{
  klimchuk::Circle circleTest(2.5, {5.9, 3.0});
  const double testArea = circleTest.getArea();
  const klimchuk::rectangle_t testFrameRect = circleTest.getFrameRect();
  const double angle = 90;

  circleTest.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, circleTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(testFrameRect.height, circleTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(testArea, circleTest.getArea(), INACCURACY);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, circleTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, circleTest.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
