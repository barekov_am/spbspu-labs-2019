#include "polygon.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>
#include <algorithm>

klimchuk::Polygon::Polygon() :
  size_(0),
  arr_(nullptr)
{}

klimchuk::Polygon::Polygon(const Polygon &other) :
  size_(other.size_),
  arr_(new point_t[size_])
{
  for (size_t i = 0; i < size_; i++)
  {
    arr_[i] = other.arr_[i];
  }
}

klimchuk::Polygon::Polygon(Polygon &&other) noexcept :
  size_(other.size_),
  arr_(other.arr_)
{
  other.size_ = 0;
  other.arr_ = nullptr;
}

klimchuk::Polygon::Polygon(size_t size, klimchuk::point_t* vertices) :
  size_(size),
  arr_(new point_t[size])
{
  if (size_ < 3)
  {
    delete [] arr_;
    throw std::invalid_argument("Polygon can't exist with 2 or less vertices");
  }

  if (vertices == nullptr)
  {
    delete [] arr_;
    throw std::invalid_argument("Pointer to vertex is null!");
  }

  for (size_t i = 0; i < size_; i++)
  {
    arr_[i] = vertices[i];
  }

  if (!isConvex())
  {
    delete [] arr_;
    throw std::invalid_argument("Polygon is non convex");
  }

  if (getArea() == 0)
  {
    delete [] arr_;
    throw std::invalid_argument("Invalid polygon's area");
  }
}

klimchuk::Polygon::~Polygon()
{
  delete [] arr_;
}

klimchuk::Polygon& klimchuk::Polygon::operator =(const Polygon &other)
{
  if (this != &other)
  {
    size_ = other.size_;
    delete [] arr_;
    arr_ = new point_t[size_];
    for (size_t i = 0; i < size_; i++)
    {
      arr_[i] = other.arr_[i];
    }
  }

  return *this;
}

klimchuk::Polygon& klimchuk::Polygon::operator =(Polygon &&other) noexcept
{
  if (this != &other)
  {
    size_ = other.size_;
    other.size_ = 0;
    delete [] arr_;
    arr_ = other.arr_;
    other.arr_ = nullptr;
  }

  return *this;
}

klimchuk::point_t klimchuk::Polygon::operator [](size_t number) const
{
  if (number >= size_)
  {
    throw std::invalid_argument("Polygon's index is out of range!");
  }

  return arr_[number];
}

klimchuk::point_t klimchuk::Polygon::getCenter() const
{
  point_t center{0.0, 0.0};
  for (size_t i = 0; i < size_; i++)
  {
    center.x += arr_[i].x;
    center.y += arr_[i].y;
  }
  return {center.x / size_, center.y / size_};
}

double klimchuk::Polygon::getArea() const
{
  double area = 0.0;
  for (size_t i = 0; i < size_ - 1; i++)
  {
    area += ((arr_[i].x + arr_[i + 1].x) * (arr_[i].y - arr_[i + 1].y));
  }
  area += ((arr_[size_ - 1].x + arr_[0].x) * (arr_[size_ - 1].y - arr_[0].y));

  return (fabs(area) / 2);
}

klimchuk::rectangle_t klimchuk::Polygon::getFrameRect() const
{
  double left = arr_[0].x;
  double right = arr_[0].x;
  double top = arr_[0].y;
  double bot = arr_[0].y;

  for (size_t i = 1; i < size_; i++)
  {
    left = std::min(left, arr_[i].x);
    right = std::max(right, arr_[i].x);
    top = std::max(top, arr_[i].y);
    bot = std::min(bot, arr_[i].y);
  }

  const double width = right - left;
  const double height = top - bot;

  return {width, height, {(left + right) / 2, (top + bot) / 2}};
}

void klimchuk::Polygon::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; i++)
  {
    arr_[i].x += dx;
    arr_[i].y += dy;
  }
}

void klimchuk::Polygon::move(const point_t &pos)
{
  point_t center = getCenter();
  move(pos.x - center.x, pos.y - center.y);
}

void klimchuk::Polygon::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Invalid Polygon scale factor!");
  }
  point_t center = getCenter();
  for (size_t i = 0; i < size_; i++)
  {
    arr_[i].x = center.x + (arr_[i].x - center.x) * factor;
    arr_[i].y = center.y + (arr_[i].y - center.y) * factor;
  }
}

void klimchuk::Polygon::rotate(double angle)
{
  point_t center = getFrameRect().pos;
  const double radAngle = angle * M_PI / 180;
  const double cosAngle = cos(radAngle);
  const double sinAngle = sin(radAngle);
  for (size_t i = 0; i < size_; i++)
  {
    arr_[i] = {center.x + (arr_[i].x - center.x) * cosAngle - (arr_[i].y - center.y) * sinAngle,
      center.y + (arr_[i].x - center.x) * sinAngle + (arr_[i].y - center.y) * cosAngle};
  }
}

void klimchuk::Polygon::printInfo() const
{
  point_t center_ = getCenter();
  rectangle_t frame = getFrameRect();
  std::cout << "Polygon:" << std::endl;
  std::cout << "  Polygon has " << size() << " vertices." << std::endl;
  for (size_t i = 0; i < size_; i++)
  {
    std::cout << "  Position of vertex number " << i + 1 << ": {" << arr_[i].x << ", ";
    std::cout << arr_[i].y << "}" << std::endl;
  }
  std::cout << "  Center: "
      << std::endl << "    x: " << center_.x
      << std::endl << "    y: " << center_.y
      << std::endl << "  Frame rectangle for the polygon: "
      << std::endl << "    Width: " << frame.width
      << std::endl << "    Height: " << frame.height
      << std::endl << "  Area: " << this->getArea()
      << std::endl << std::endl;
}

double klimchuk::Polygon::getLinePosition(const point_t &previous, const point_t &next) const
{
  return (previous.x * next.y) - (previous.y * next.x);
}

bool klimchuk::Polygon::isConvex() const
{
  bool negative = false;
  if ((getLinePosition({arr_[1].x - arr_[0].x, arr_[1].y - arr_[0].y},
    {arr_[2].x - arr_[1].x, arr_[2].y - arr_[1].y})) < 0)
  {
    negative = true;
  }

  for (size_t i = 2; i < size_; i++)
  {
    if (((getLinePosition({arr_[i - 1].x - arr_[i - 2].x, arr_[i - 1].y - arr_[i - 2].y},
      {arr_[i].x - arr_[i - 1].x, arr_[i].y - arr_[i - 1].y})) < 0) != negative)
    {
      return false;
    }
  }

  if (((getLinePosition({arr_[size_ - 1].x - arr_[size_ - 2].x, arr_[size_ - 1].y - arr_[size_ - 2].y},
    {arr_[0].x - arr_[size_ - 1].x, arr_[0].y - arr_[size_ - 1].y})) < 0) != negative)
  {
    return false;
  }

  if (((getLinePosition({arr_[0].x - arr_[size_ - 1].x, arr_[0].y - arr_[size_ - 1].y},
    {arr_[1].x - arr_[0].x, arr_[1].y - arr_[0].y})) < 0) != negative)
  {
    return false;
  }

  return true;
}

int klimchuk::Polygon::size() const
{
  return size_;
}
