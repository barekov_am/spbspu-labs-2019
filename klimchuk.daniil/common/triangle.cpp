#include "triangle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>
#include <algorithm>

klimchuk::Triangle::Triangle(const point_t &pointA, const point_t &pointB, const point_t &pointC) :
  A_(pointA),
  B_(pointB),
  C_(pointC),
  angle_(0)
{
  if (getArea() <= 0)
  {
    throw std::invalid_argument("Invalid triangle parameters!");
  }
}

double klimchuk::Triangle::getArea() const
{
  double AB = getDistance(A_, B_);
  double AC = getDistance(A_, C_);
  double BC = getDistance(B_, C_);
  double halfP = (AB + AC + BC) / 2;
  return sqrt(halfP * (halfP - AB) * (halfP - AC) * (halfP - BC));
}

klimchuk::rectangle_t klimchuk::Triangle::getFrameRect() const
{
  double left = std::min(std::min(A_.x, B_.x), C_.x);
  double bot = std::min(std::min(A_.y, B_.y), C_.y);
  double top = std::max(std::max(A_.y, B_.y), C_.y);
  double right = std::max(std::max(A_.x, B_.x), C_.x);
  double width = right - left;
  double height = top - bot;
  return {width, height, {(left + right) / 2, (top + bot) / 2}};
}

void klimchuk::Triangle::move(const point_t &pos)
{
  point_t center = getCenter();
  move(pos.x - center.x, pos.y - center.y);
}

void klimchuk::Triangle::move(double dx, double dy)
{
  A_.x += dx;
  B_.x += dx;
  C_.x += dx;
  A_.y += dy;
  B_.y += dy;
  C_.y += dy;
}

void klimchuk::Triangle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Invalid Triangle scale factor!");
  }
  point_t center = getCenter();
  A_.x = center.x + (A_.x - center.x) * factor;
  A_.y = center.y + (A_.y - center.y) * factor;
  B_.x = center.x + (B_.x - center.x) * factor;
  B_.y = center.y + (B_.y - center.y) * factor;
  C_.x = center.x + (C_.x - center.x) * factor;
  C_.y = center.y + (C_.y - center.y) * factor;
}

klimchuk::point_t klimchuk::Triangle::getCenter() const
{
  return point_t{(A_.x + B_.x + C_.x) / 3, (A_.y + B_.y + C_.y) / 3};
}

double klimchuk::Triangle::getDistance(const point_t &pointStart, const point_t &pointEnd)
{
  return sqrt((pointEnd.x - pointStart.x) * (pointEnd.x - pointStart.x)
      + (pointEnd.y - pointStart.y) * (pointEnd.y - pointStart.y));
}

void klimchuk::Triangle::rotate(double angle)
{
  point_t center = getFrameRect().pos;
  const double radAngle = angle * M_PI / 180;
  const double cosAngle = cos(radAngle);
  const double sinAngle = sin(radAngle);
  A_= {center.x + (A_.x - center.x) * cosAngle - (A_.y - center.y) * sinAngle,
    center.y + (A_.x - center.x) * sinAngle + (A_.y - center.y) * cosAngle};
  B_= {center.x + (B_.x - center.x) * cosAngle - (B_.y - center.y) * sinAngle,
    center.y + (B_.x - center.x) * sinAngle + (B_.y - center.y) * cosAngle};
  C_= {center.x + (C_.x - center.x) * cosAngle - (C_.y - center.y) * sinAngle,
    center.y + (C_.x - center.x) * sinAngle + (C_.y - center.y) * cosAngle};
}

void klimchuk::Triangle::printInfo() const
{
  point_t center_ = getCenter();
  rectangle_t frame = getFrameRect();
  std::cout << "Triangle:"
      << std::endl << "  The apex coordinates of the triangle: "
      << std::endl << "    A {" << A_.x << "," << A_.y << "}"
      << std::endl << "    B {" << B_.x << "," << B_.y << "}"
      << std::endl << "    C {" << C_.x << "," << C_.y << "}"
      << std::endl << "  Center: "
      << std::endl << "    x: " << center_.x
      << std::endl << "    y: " << center_.y
      << std::endl << "  Frame rectangle for the triangle: "
      << std::endl << "    Width: " << frame.width
      << std::endl << "    Height: " << frame.height
      << std::endl << "  Area: " << this->getArea()
      << std::endl << std::endl;
}
