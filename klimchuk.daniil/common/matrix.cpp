#include "matrix.hpp"

#include <stdexcept>
#include <iostream>

klimchuk::Matrix::Matrix() :
  lines_(0),
  columns_(0),
  list_()
{}

klimchuk::Matrix::Matrix(const Matrix &other) :
  lines_(other.lines_),
  columns_(other.columns_),
  list_(std::make_unique<shapePtr[]>(other.lines_ * other.columns_))
{
  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    list_[i] = other.list_[i];
  }
}

klimchuk::Matrix::Matrix(Matrix &&other) noexcept :
  lines_(other.lines_),
  columns_(other.columns_),
  list_(std::move(other.list_))
{
  other.lines_ = 0;
  other.columns_ = 0;
}

klimchuk::Matrix &klimchuk::Matrix::operator =(const Matrix &other)
{
  if (this != &other)
  {
    lines_ = other.lines_;
    columns_ = other.columns_;
    dynamicArray tmpMatrix(std::make_unique<shapePtr[]>(other.lines_ * other.columns_));

    for (size_t i = 0; i < (lines_ * columns_); i++)
    {
      tmpMatrix[i] = other.list_[i];
    }
    list_.swap(tmpMatrix);
  }

  return *this;
}

klimchuk::Matrix &klimchuk::Matrix::operator =(Matrix &&other) noexcept
{
  if (this != &other)
  {
    lines_ = other.lines_;
    columns_ = other.columns_;
    list_ = std::move(other.list_);
    other.lines_ = 0;
    other.columns_ = 0;
  }

  return *this;
}

klimchuk::dynamicArray klimchuk::Matrix::operator [](size_t index) const
{
  if (lines_ <= index)
  {
    throw std::out_of_range("Index is out of range");
  }

  dynamicArray tmpMatrix(std::make_unique<shapePtr[]>(columns_));

  for (size_t i = 0; i < columns_; i++)
  {
    tmpMatrix[i] = list_[index * columns_ + i];
  }

  return tmpMatrix;
}

bool klimchuk::Matrix::operator ==(const Matrix &other) const
{
  if ((lines_ != other.lines_) || (columns_ != other.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    if (list_[i] != other.list_[i])
    {
      return false;
    }
  }

  return true;
}

bool klimchuk::Matrix::operator !=(const Matrix &other) const
{
  return !(*this == other);
}

size_t klimchuk::Matrix::getLines() const
{
  return lines_;
}

size_t klimchuk::Matrix::getColumns() const
{
  return columns_;
}

void klimchuk::Matrix::add(const shapePtr &other, size_t line, size_t column)
{
  size_t tmpLines;
  size_t tmpColumns;

  if (line == lines_)
  {
    tmpLines = lines_ + 1;
  }
  else
  {
    tmpLines = lines_;
  }

  if (column == columns_)
  {
    tmpColumns = columns_ + 1;
  }
  else
  {
    tmpColumns = columns_;
  }

  dynamicArray tmpList(std::make_unique<shapePtr[]>(tmpLines * tmpColumns));

  for (size_t i = 0; i < tmpLines; i++)
  {
    for (size_t j = 0; j < tmpColumns; j++)
    {
      if ((i == lines_) || (j == columns_))
      {
        tmpList[i * tmpColumns + j] = nullptr;
      }
      else
      {
        tmpList[i * tmpColumns + j] = list_[i * columns_ + j];
      }
    }
  }

  tmpList[line * tmpColumns + column] = other;
  list_.swap(tmpList);
  lines_ = tmpLines;
  columns_ = tmpColumns;
}

void klimchuk::Matrix::printInfo() const
{
  std::cout << "--- MATRIX --- " << std::endl;
  std::cout << "Value of lines: " << lines_ << std::endl;
  std::cout << "Value of columns: " << columns_ << std::endl << std::endl;

  for (size_t i = 0; i < lines_; i++)
  {
    for (size_t j = 0; j < columns_; j++)
    {
      if (list_[i * columns_ + j] != nullptr)
      {
        std::cout << "On layer number " << i + 1 << " and position " << j + 1 << " there is figure:" << std::endl;
        list_[i * columns_ + j]->printInfo();
      }
    }
  }
}
