#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"

#define INACCURACY 0.0001

BOOST_AUTO_TEST_SUITE(TriangleTests)

BOOST_AUTO_TEST_CASE(TriangleMoveDxy)
{
  klimchuk::Triangle triangleTest({29, 30}, {20,30}, {15,25});
  const double widthBefore = triangleTest.getFrameRect().width;
  const double heightBefore = triangleTest.getFrameRect().height;
  const double areaBefore = triangleTest.getArea();
  triangleTest.move(5, 10);
  BOOST_CHECK_CLOSE(widthBefore, triangleTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, triangleTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, triangleTest.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(TriangleMovePoint)
{
  klimchuk::Triangle triangleTest({40, 27}, {50, 21}, {60, 90});
  const double widthBefore = triangleTest.getFrameRect().width;
  const double heightBefore = triangleTest.getFrameRect().height;
  const double areaBefore = triangleTest.getArea();
  triangleTest.move({20, -25});
  BOOST_CHECK_CLOSE(widthBefore, triangleTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, triangleTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, triangleTest.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(TriangleScale)
{
  klimchuk::Triangle triangleTest({13,20}, {15, 22}, {12, 12});
  const double areaBefore = triangleTest.getArea();
  const double factor = 2;
  triangleTest.scale(factor);
  BOOST_CHECK_CLOSE(areaBefore * factor * factor, triangleTest.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(TriangleInvalidParameters)
{
  BOOST_CHECK_THROW(klimchuk::Triangle({1, 1}, {1, 1}, {5, 0}), std::invalid_argument);
  klimchuk::Triangle triangleTest({1, 5}, {5, 1}, {0, 0});
  BOOST_CHECK_THROW(triangleTest.scale(-15), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TriangleParametersAfterRotation)
{
  klimchuk::Triangle triangleTest({13,20}, {15, 22}, {12, 12});
  const double areaBefore = triangleTest.getArea();
  const double widthBefore = triangleTest.getFrameRect().width;
  const double heightBefore = triangleTest.getFrameRect().height;
  const klimchuk::rectangle_t testFrameRect = triangleTest.getFrameRect();
  const double angle = 90;

  triangleTest.rotate(angle);

  BOOST_CHECK_CLOSE(widthBefore, triangleTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, triangleTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, triangleTest.getArea(), INACCURACY);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, triangleTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, triangleTest.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
