#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(separationTest)

BOOST_AUTO_TEST_CASE(checkCorrectCrossing)
{
  klimchuk::Rectangle testRectangle(4.0, 2.0, {5.0, 5.0});
  klimchuk::Rectangle crossingTestRectangle(6.0, 2.0, {8.0, 5.0});
  klimchuk::Circle testCircle(1.0, {11.0, 5.0});

  const bool falseResult = klimchuk::cross(testRectangle.getFrameRect(), testCircle.getFrameRect());
  const bool trueResult1 = klimchuk::cross(testRectangle.getFrameRect(), crossingTestRectangle.getFrameRect());
  const bool trueResult2 = klimchuk::cross(testCircle.getFrameRect(), crossingTestRectangle.getFrameRect());

  BOOST_CHECK_EQUAL(falseResult, false);
  BOOST_CHECK_EQUAL(trueResult1, true);
  BOOST_CHECK_EQUAL(trueResult2, true);
}

BOOST_AUTO_TEST_CASE(checkCorrectSeparation)
{
  klimchuk::Rectangle testRectangle(4.0, 2.0, {5.0, 5.0});
  klimchuk::Rectangle crossingTestRectangle(6.0, 2.0, {8.0, 5.0});
  klimchuk::Circle testCircle(1.0, {11.0, 5.0});
  klimchuk::shapePtr rectanglePtr = std::make_shared<klimchuk::Rectangle>(testRectangle);
  klimchuk::shapePtr rectangleCrossingPtr = std::make_shared<klimchuk::Rectangle>(crossingTestRectangle);
  klimchuk::shapePtr circlePtr = std::make_shared<klimchuk::Circle>(testCircle);

  klimchuk::CompositeShape compositeTest;
  compositeTest.add(rectanglePtr);
  compositeTest.add(rectangleCrossingPtr);
  compositeTest.add(circlePtr);

  klimchuk::Matrix testMatrix = klimchuk::part(compositeTest);

  BOOST_CHECK(testMatrix[0][0] == rectanglePtr);
  BOOST_CHECK(testMatrix[0][1] == circlePtr);
  BOOST_CHECK(testMatrix[1][0] == rectangleCrossingPtr);
}

BOOST_AUTO_TEST_SUITE_END()
