#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

#define INACCURACY 0.0001

BOOST_AUTO_TEST_SUITE(RectangleTests)

BOOST_AUTO_TEST_CASE(RectangleMoveDxy)
{
  klimchuk::Rectangle rectangleTest(7, 8, {5, 6});
  const double widthBefore = rectangleTest.getFrameRect().width;
  const double heightBefore = rectangleTest.getFrameRect().height;
  const double areaBefore = rectangleTest.getArea();
  rectangleTest.move(7, 8);
  BOOST_CHECK_CLOSE(widthBefore, rectangleTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, rectangleTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, rectangleTest.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(RectangleMovePoint)
{
  klimchuk::Rectangle rectangleTest(11, 12, {9, 10});
  const double widthBefore = rectangleTest.getFrameRect().width;
  const double heightBefore = rectangleTest.getFrameRect().height;
  const double areaBefore = rectangleTest.getArea();
  rectangleTest.move({13, 14});
  BOOST_CHECK_CLOSE(widthBefore, rectangleTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, rectangleTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, rectangleTest.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(RectangleScale)
{
  klimchuk::Rectangle rectangleTest(17, 18, {15, 16});
  const double areaBefore = rectangleTest.getArea();
  const double factor = 2;
  rectangleTest.scale(factor);
  BOOST_CHECK_CLOSE(areaBefore * factor * factor, rectangleTest.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(RectangleInvalidParameters)
{
  BOOST_CHECK_THROW(klimchuk::Rectangle(-1, 0, {1, 1}), std::invalid_argument);
  klimchuk::Rectangle rectangleTest(10, 12, {1, 5});
  BOOST_CHECK_THROW(rectangleTest.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  klimchuk::Rectangle rectangleTest(4.0, 2.0, {5.0, 5.0});
  const double testArea = rectangleTest.getArea();
  const klimchuk::rectangle_t testFrameRect = rectangleTest.getFrameRect();
  const double angle = 90;

  rectangleTest.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, rectangleTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(testFrameRect.height, rectangleTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(testArea, rectangleTest.getArea(), INACCURACY);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, rectangleTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, rectangleTest.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
