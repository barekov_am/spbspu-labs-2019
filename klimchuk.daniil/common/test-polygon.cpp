#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "polygon.hpp"

#define INACCURACY 0.0001

BOOST_AUTO_TEST_SUITE(PolygonTests)

BOOST_AUTO_TEST_CASE(PolygonMoveDxy)
{
  klimchuk::point_t vertices[] = {{20, 10}, {20, -10}, {-10, -10}, {-10, 10}};
  size_t size = sizeof(vertices) / sizeof(vertices[0]);
  klimchuk::Polygon polygonTest(size, vertices);
  const double widthBefore = polygonTest.getFrameRect().width;
  const double heightBefore = polygonTest.getFrameRect().height;
  const double areaBefore = polygonTest.getArea();
  polygonTest.move(10, 20);
  BOOST_CHECK_CLOSE(widthBefore, polygonTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, polygonTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, polygonTest.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(PolygonMovePoint)
{
  klimchuk::point_t vertices[] = {{20, 10}, {20, -10}, {-10, -10}, {-10, 10}};
  size_t size = sizeof(vertices) / sizeof(vertices[0]);
  klimchuk::Polygon polygonTest(size, vertices);
  const double widthBefore = polygonTest.getFrameRect().width;
  const double heightBefore = polygonTest.getFrameRect().height;
  const double areaBefore = polygonTest.getArea();
  polygonTest.move({30, 40});
  BOOST_CHECK_CLOSE(widthBefore, polygonTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, polygonTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, polygonTest.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(PolygonScale)
{
  klimchuk::point_t vertices[] = {{20, 10}, {20, -10}, {-10, -10}, {-10, 10}};
  size_t size = sizeof(vertices) / sizeof(vertices[0]);
  klimchuk::Polygon polygonTest(size, vertices);
  const double areaBefore = polygonTest.getArea();
  const double factor = 2;
  polygonTest.scale(factor);
  BOOST_CHECK_CLOSE(areaBefore * factor * factor, polygonTest.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(PolygonInvalidScale)
{
  klimchuk::point_t vertices[] = {{20, 10}, {20, -10}, {-10, -10}, {-10, 10}};
  size_t size = sizeof(vertices) / sizeof(vertices[0]);
  klimchuk::Polygon polygonTest(size, vertices);
  BOOST_CHECK_THROW(polygonTest.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(PolygonInvalidParameters)
{
  klimchuk::point_t vertices[] = {{20, 10}, {20, -10}};
  size_t size = sizeof(vertices) / sizeof(vertices[0]);
  BOOST_CHECK_THROW(klimchuk::Polygon polygonTest(size, vertices), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  klimchuk::point_t vertices[] = {{20, 10}, {20, -10}, {-10, -10}, {-10, 10}};
  size_t size = sizeof(vertices) / sizeof(vertices[0]);
  klimchuk::Polygon polygonTest(size, vertices);
  const double testArea = polygonTest.getArea();
  const double widthBefore = polygonTest.getFrameRect().width;
  const double heightBefore = polygonTest.getFrameRect().height;
  const klimchuk::rectangle_t testFrameRect = polygonTest.getFrameRect();
  const double angle = 90;

  polygonTest.rotate(angle);

  BOOST_CHECK_CLOSE(widthBefore, polygonTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, polygonTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(testArea, polygonTest.getArea(), INACCURACY);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, polygonTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, polygonTest.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
