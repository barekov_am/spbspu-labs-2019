#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <cstdlib>
#include "shape.hpp"

namespace klimchuk
{
  class Polygon : public Shape
  {
  public:
    Polygon();
    Polygon(const Polygon &other);
    Polygon(Polygon &&other) noexcept;
    Polygon(size_t size, point_t* vertices);
    virtual ~Polygon();

    Polygon &operator =(const Polygon &other);
    Polygon &operator =(Polygon &&other) noexcept;
    point_t operator [](size_t number) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &pos) override;
    void move(double dx, double dy) override;
    void scale(double factor) override;
    void rotate(double angle) override;
    void printInfo() const override;
    point_t getCenter() const;
    int size() const;

  private:
    size_t size_;
    point_t* arr_;
    bool isConvex() const;
    double getLinePosition(const point_t &previous, const point_t &next) const;
  };
}

#endif //POLYGON_HPP
