#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithCrossing)
{
  const size_t layersValue = 2;
  const size_t columnsValue = 1;

  klimchuk::Rectangle testRectangle(4.0, 2.0, {5.0, 5.0});
  klimchuk::Circle testCircle(2.0, {5.0, 5.0});
  klimchuk::shapePtr rectanglePtr = std::make_shared<klimchuk::Rectangle>(testRectangle);
  klimchuk::shapePtr circlePtr = std::make_shared<klimchuk::Circle>(testCircle);

  klimchuk::CompositeShape compositeTest;
  compositeTest.add(rectanglePtr);
  compositeTest.add(circlePtr);

  klimchuk::Matrix testMatrix = klimchuk::part(compositeTest);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithoutCrossing)
{
  const size_t layersValue = 1;
  const size_t columnsValue = 2;

  klimchuk::Rectangle testRectangle(4.0, 2.0, {5.0, 5.0});
  klimchuk::Circle testCircle(2.0, {10.0, 5.0});
  klimchuk::shapePtr rectanglePtr = std::make_shared<klimchuk::Rectangle>(testRectangle);
  klimchuk::shapePtr circlePtr = std::make_shared<klimchuk::Circle>(testCircle);

  klimchuk::CompositeShape compositeTest(rectanglePtr);
  compositeTest.add(circlePtr);

  klimchuk::Matrix testMatrix = klimchuk::part(compositeTest);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  klimchuk::Rectangle testRectangle(4.0, 2.0, {5.0, 5.0});
  klimchuk::Circle testCircle(2.0, {5.0, 5.0});
  klimchuk::shapePtr rectanglePtr = std::make_shared<klimchuk::Rectangle>(testRectangle);
  klimchuk::shapePtr circlePtr = std::make_shared<klimchuk::Circle>(testCircle);

  klimchuk::CompositeShape compositeTest(rectanglePtr);

  compositeTest.add(circlePtr);

  klimchuk::Matrix testMatrix = klimchuk::part(compositeTest);

  BOOST_CHECK_THROW(testMatrix[5][0], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-5][0], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
