#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

klimchuk::Rectangle::Rectangle(double width, double height, const point_t &center) :
  width_(width),
  height_(height),
  pos_(center),
  angle_(0)
{
  if ((width_ <= 0) || (height_ <= 0))
  {
    throw std::invalid_argument("Invalid Rectangle parameters!");
  }
}

double klimchuk::Rectangle::getArea() const
{
  return width_ * height_;
}

klimchuk::rectangle_t klimchuk::Rectangle::getFrameRect() const
{
  const double radAngle = angle_ * M_PI / 180;
  const double width = width_ * fabs(cos(radAngle)) + height_ * fabs(sin(radAngle));
  const double height = width_ * fabs(sin(radAngle)) + height_ * fabs(cos(radAngle));

  return {width, height, pos_};
}

void klimchuk::Rectangle::move(const point_t &pos)
{
  pos_ = pos;
}

void klimchuk::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void klimchuk::Rectangle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Invalid Rectangle scale factor!");
  }
  height_ *= factor;
  width_ *= factor;
}

void klimchuk::Rectangle::rotate(double angle)
{
  angle_ += angle;
}

void klimchuk::Rectangle::printInfo() const
{
  rectangle_t frame = getFrameRect();
  std::cout << "Rectangle:"
      << std::endl << "  Width: " << width_
      << std::endl << "  Height: " << height_
      << std::endl << "  Center: "
      << std::endl << "    x: " << pos_.x
      << std::endl << "    y: " << pos_.y
      << std::endl << "  Frame rectangle for the rectangle: "
      << std::endl << "    Width: " << frame.width
      << std::endl << "    Height: " << frame.height
      << std::endl << "  Area: " << this->getArea()
      << std::endl << std::endl;
}
