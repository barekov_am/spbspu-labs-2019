#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

klimchuk::CompositeShape::CompositeShape() :
  size_(0),
  array_(nullptr),
  angle_(0)
{}

klimchuk::CompositeShape::CompositeShape(const CompositeShape &other) :
  size_(other.size_),
  array_(std::make_unique<shapePtr[]>(other.size_)),
  angle_(other.angle_)
{
  for (size_t i = 0; i < size_; i++)
  {
    array_[i] = other.array_[i];
  }
}

klimchuk::CompositeShape::CompositeShape(CompositeShape &&other) noexcept :
  size_(other.size_),
  array_(std::move(other.array_)),
  angle_(other.angle_)
{}

klimchuk::CompositeShape::CompositeShape(const shapePtr &shape) :
  size_(1),
  array_(std::make_unique<shapePtr[]>(size_)),
  angle_(0)
{
  if (shape == nullptr)
  {
    size_ = 0;
    throw std::invalid_argument("Pointer to shape is null!");
  }
  array_[0] = shape;
}

klimchuk::CompositeShape& klimchuk::CompositeShape::operator =(const CompositeShape& other)
{
  if (this != &other)
  {
    size_ = other.size_;
    angle_ = other.angle_;
    dynamicArray tmpArray = std::make_unique<shapePtr[]>(size_);
    for (size_t  i = 0; i < size_; i++)
    {
      tmpArray[i] = other.array_[i];
    }
    array_.swap(tmpArray);
  }

  return *this;
}

klimchuk::CompositeShape& klimchuk::CompositeShape::operator =(CompositeShape&& other) noexcept
{
  if (this != &other)
  {
    size_ = other.size_;
    angle_ = other.angle_;
    array_ = std::move(other.array_);
    other.size_ = 0;
  }

  return *this;
}

klimchuk::shapePtr klimchuk::CompositeShape::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("CompositeShape's index is out of range!");
  }
  return array_[index];
}

double klimchuk::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < size_; i++)
  {
    area += array_[i]->getArea();
  }
  return area;
}

klimchuk::rectangle_t klimchuk::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    return {0, 0, {0, 0}};
  }

  rectangle_t tmpFrame = array_[0]->getFrameRect();
  double left = tmpFrame.pos.x - tmpFrame.width / 2;
  double right = tmpFrame.pos.x + tmpFrame.width / 2;
  double bot = tmpFrame.pos.y - tmpFrame.height / 2;
  double top = tmpFrame.pos.y + tmpFrame.height / 2;

  for (size_t i = 1; i < size_; i++)
  {
    tmpFrame = array_[i]->getFrameRect();
    left = std::min(left, tmpFrame.pos.x - tmpFrame.width / 2);
    right = std::max(right, tmpFrame.pos.x + tmpFrame.width / 2);
    bot = std::min(bot, tmpFrame.pos.y - tmpFrame.height / 2);
    top = std::max(top, tmpFrame.pos.y + tmpFrame.height / 2);
  }

  double width = right - left;
  double height = top - bot;

  return {width, height, {(left + right) / 2, (top + bot) / 2}};
}

void klimchuk::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; i++)
  {
    array_[i]->move(dx, dy);
  }
}

void klimchuk::CompositeShape::move(const point_t &pos)
{
  point_t center = getFrameRect().pos;
  move(pos.x - center.x, pos.y - center.y);
}

void klimchuk::CompositeShape::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Invalid CompositeShape scale coefficient!");
  }
  const point_t centerComp = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    point_t centerShape = array_[i]->getFrameRect().pos;
    array_[i]->move((centerShape.x - centerComp.x) * (factor - 1), (centerShape.y - centerComp.y) * (factor - 1));
    array_[i]->scale(factor);
  }
}

void klimchuk::CompositeShape::printInfo() const
{
  rectangle_t frame = getFrameRect();
  std::cout << "CompositeShape:"
      << std::endl << "The number of shape in the CompositeShape = " << size_
      << std::endl << "  Frame rectangle for the CompositeShape: "
      << std::endl << "    Width: " << frame.width
      << std::endl << "    Height: " << frame.height
      << std::endl << "  And with center: "
      << std::endl << "    x: " << frame.pos.x
      << std::endl << "    y: " << frame.pos.y
      << std::endl << "  Area: " << this->getArea()
      << std::endl << std::endl;
}

int klimchuk::CompositeShape::size() const
{
  return size_;
}

void klimchuk::CompositeShape::add(const shapePtr &other)
{
  if (other == nullptr)
  {
    throw std::invalid_argument("Added shape pointer must not be null");
  }

  dynamicArray newArray = std::make_unique<shapePtr[]>(size_ + 1);
  for (size_t i = 0; i < size_; i++)
  {
    newArray[i] = array_[i];
  }
  newArray[size_] = other;
  size_++;
  array_.swap(newArray);
}

void klimchuk::CompositeShape::remove(size_t index)
{
  if (index >= size_)
  {
    throw std::out_of_range("CompositeShape index out of range");
  }

  size_--;

  for (size_t i = index; i < size_; i++)
  {
    array_[i] = array_[i + 1];
  }
}

void klimchuk::CompositeShape::rotate(double angle)
{
  const point_t frameRectCentre = getFrameRect().pos;
  const double radAngle = angle * M_PI / 180;
  const double cosAngle = cos(radAngle);
  const double sinAngle = sin(radAngle);
  for (size_t i = 0; i < size_; i++)
  {
    const double dx = array_[i]->getFrameRect().pos.x - frameRectCentre.x;
    const double dy = array_[i]->getFrameRect().pos.y - frameRectCentre.y;
    const double moveDX = dx * cosAngle - dy * sinAngle;
    const double moveDY = dx * sinAngle + dy * cosAngle;
    array_[i]->move({frameRectCentre.x + moveDX, frameRectCentre.y + moveDY});
    array_[i]->rotate(angle);
  }
}

klimchuk::dynamicArray klimchuk::CompositeShape::getList() const
{
  dynamicArray tmpArray(std::make_unique<shapePtr[]>(size_));

  for (size_t i = 0; i < size_; i++)
  {
    tmpArray[i] = array_[i];
  }

  return tmpArray;
}
