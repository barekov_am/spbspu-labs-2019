#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

#define INACCURACY 0.0001

BOOST_AUTO_TEST_SUITE(CompositeShapeTests)

BOOST_AUTO_TEST_CASE(CompositeShapeMoveDxy)
{
  klimchuk::Circle circle(8, {10, 9});
  klimchuk::Rectangle rectangle(7, 8, {5, 6});
  klimchuk::shapePtr rectanglePtr = std::make_shared<klimchuk::Rectangle>(rectangle);
  klimchuk::shapePtr circlePtr = std::make_shared<klimchuk::Circle>(circle);
  klimchuk::CompositeShape compositeTest;

  compositeTest.add(circlePtr);
  compositeTest.add(rectanglePtr);
  const double widthBefore = compositeTest.getFrameRect().width;
  const double heightBefore = compositeTest.getFrameRect().height;
  const double areaBefore = compositeTest.getArea();
  compositeTest.move(20, 20);
  BOOST_CHECK_CLOSE(widthBefore, compositeTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, compositeTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, compositeTest.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(CompositeShapeMovePoint)
{
  klimchuk::Rectangle rectangle(11, 12, {9, 10});
  klimchuk::Triangle triangle({29, 30}, {20,30}, {15,25});
  klimchuk::shapePtr rectanglePtr = std::make_shared<klimchuk::Rectangle>(rectangle);
  klimchuk::shapePtr trianglePtr = std::make_shared<klimchuk::Triangle>(triangle);
  klimchuk::CompositeShape compositeTest;

  compositeTest.add(rectanglePtr);
  compositeTest.add(trianglePtr);
  const double widthBefore = compositeTest.getFrameRect().width;
  const double heightBefore = compositeTest.getFrameRect().height;
  const double areaBefore = compositeTest.getArea();
  compositeTest.move({20, -25});
  BOOST_CHECK_CLOSE(widthBefore, compositeTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, compositeTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, compositeTest.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(CompositeShapeScale)
{
  klimchuk::Circle circle(5, {6, 7});
  klimchuk::Triangle triangle({29, 30}, {20,30}, {15,25});
  klimchuk::shapePtr circlePtr = std::make_shared<klimchuk::Circle>(circle);
  klimchuk::shapePtr trianglePtr = std::make_shared<klimchuk::Triangle>(triangle);
  klimchuk::CompositeShape compositeTest;

  const double areaBefore = compositeTest.getArea();
  const klimchuk::rectangle_t testFrameRect = compositeTest.getFrameRect();

  const double factor = 2;
  compositeTest.scale(factor);
  BOOST_CHECK_CLOSE(areaBefore * factor * factor, compositeTest.getArea(), INACCURACY);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, compositeTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, compositeTest.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_CASE(CompositeThrowingExceptions)
{
  klimchuk::Circle circle(8, {10, 9});
  klimchuk::Rectangle rectangle(7, 8, {5, 6});
  klimchuk::shapePtr rectanglePtr = std::make_shared<klimchuk::Rectangle>(rectangle);
  klimchuk::shapePtr circlePtr = std::make_shared<klimchuk::Circle>(circle);
  klimchuk::CompositeShape compositeTest;

  compositeTest.add(circlePtr);
  compositeTest.add(rectanglePtr);

  BOOST_CHECK_THROW(compositeTest.scale(-2), std::invalid_argument);
  BOOST_CHECK_THROW(compositeTest.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compositeTest.remove(4), std::out_of_range);
  BOOST_CHECK_THROW(compositeTest.remove(-2), std::out_of_range);
  BOOST_CHECK_THROW(compositeTest[4], std::out_of_range);
  BOOST_CHECK_THROW(compositeTest[-2], std::out_of_range);

  const int size = compositeTest.size();
  compositeTest.remove(1);
  const int sizeAfterRemove = compositeTest.size();
  BOOST_CHECK_EQUAL(size - 1, sizeAfterRemove);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  klimchuk::Rectangle testRectangle(4, 2, {5, 5});
  klimchuk::Circle testCircle(1, {5, 5});
  klimchuk::shapePtr rectanglePtr = std::make_shared<klimchuk::Rectangle>(testRectangle);
  klimchuk::shapePtr circlePtr = std::make_shared<klimchuk::Circle>(testCircle);

  klimchuk::CompositeShape compositeTest(rectanglePtr);

  compositeTest.add(circlePtr);

  const double testArea = compositeTest.getArea();
  const klimchuk::rectangle_t testFrameRect = compositeTest.getFrameRect();
  const double angle = 90;

  compositeTest.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, compositeTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(testFrameRect.height, compositeTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(testArea, compositeTest.getArea(), INACCURACY);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, compositeTest.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, compositeTest.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
