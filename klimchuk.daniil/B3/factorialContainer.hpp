#ifndef FACTORIALCONTAINER_HPP
#define FACTORIALCONTAINER_HPP

#include <iterator>

class FactorialIterator: public std::iterator<std::bidirectional_iterator_tag,
  unsigned long long>
{
public:
  FactorialIterator();
  FactorialIterator(size_t index);
  unsigned long long& operator *();
  unsigned long long* operator ->();
  FactorialIterator& operator ++();
  FactorialIterator& operator --();
  FactorialIterator operator ++(int);
  FactorialIterator operator --(int);
  bool operator ==(FactorialIterator& other) const;
  bool operator !=(FactorialIterator& other) const;

private:
  unsigned long long value_;
  unsigned long long countData(size_t index) const;
  size_t argument_;
};

class FactorialContainer
{
public:
  FactorialContainer() = default;
  FactorialIterator begin();
  FactorialIterator end();
};

#endif //FACTORIALCONTAINER_HPP
