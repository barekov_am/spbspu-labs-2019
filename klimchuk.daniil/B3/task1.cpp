#include <iostream>
#include <string>
#include "commands.hpp"

void task1()
{
  std::string input;
  PhoneBookInterface bookmark;
  while (std::getline(std::cin, input))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input failed.");
    }

    std::stringstream inputStream(input);
    std::string command;
    inputStream >> command;
    parseCommand(command)(bookmark, inputStream);
  }
}
