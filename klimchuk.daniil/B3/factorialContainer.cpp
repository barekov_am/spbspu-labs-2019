#include "factorialContainer.hpp"

const int maxArgument = 11;

FactorialIterator::FactorialIterator() :
  value_(1),
  argument_(1)
{}

FactorialIterator::FactorialIterator(size_t index) :
  value_(countData(index)),
  argument_(index)
{}

unsigned long long& FactorialIterator::operator *()
{
  return value_;
}

unsigned long long* FactorialIterator::operator ->()
{
  return &value_;
}

FactorialIterator& FactorialIterator::operator ++()
{
  if (argument_ == maxArgument)
  {
    throw std::out_of_range("Cannot be bigger then 11.\n");
  }
  ++argument_;
  value_ *= argument_;

  return *this;
}

FactorialIterator& FactorialIterator::operator --()
{
  if (argument_ == 1)
  {
    throw std::overflow_error("Cannot be less then 1.\n");
  }

  value_ /= argument_;
  --argument_;

  return *this;
}

FactorialIterator FactorialIterator::operator ++(int)
{
  FactorialIterator tmp = *this;
  ++(*this);

  return tmp;
}

FactorialIterator FactorialIterator::operator --(int)
{
  FactorialIterator tmp = *this;
  --(*this);

  return tmp;
}

bool FactorialIterator::operator ==(FactorialIterator& other) const
{
  return ((value_ == other.value_) && (argument_ == other.argument_));
}

bool FactorialIterator::operator !=(FactorialIterator& other) const
{
  return ((value_ != other.value_) || (argument_ != other.argument_));
}

unsigned long long FactorialIterator::countData(size_t index) const
{
  if (index <= 1)
  {
    return 1;
  }
  else
  {
    return index * countData(index - 1);
  }
}

FactorialIterator FactorialContainer::begin()
{
  return FactorialIterator(1);
}

FactorialIterator FactorialContainer::end()
{
  return FactorialIterator(maxArgument);
}
