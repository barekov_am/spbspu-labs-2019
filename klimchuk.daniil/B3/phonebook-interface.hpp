#ifndef PHONEBOOK_INTERFACE_HPP
#define PHONEBOOK_INTERFACE_HPP

#include <map>
#include "phonebook.hpp"

class PhoneBookInterface
{
public:
  enum class InsertLocation
  {
    before,
    after
  };

  enum class MoveLocation
  {
    first,
    last
  };
  using bookmarks = std::map<std::string, PhoneBook::iterator>;
  using iterator = bookmarks::iterator;

  PhoneBookInterface();

  void add(const PhoneBook::recordType& data);
  void store(std::string& oldMarkName, std::string& newMarkName);
  void insert(InsertLocation point, std::string& markName, PhoneBook::recordType& data);
  void remove(std::string& markName);
  void view(std::string& markName);
  void move(std::string& markName, int steps);
  void move(std::string& markName, MoveLocation direction);

private:
  PhoneBook phoneBook_;
  bookmarks marks_;
};

#endif //PHONEBOOK_INTERFACE_HPP
