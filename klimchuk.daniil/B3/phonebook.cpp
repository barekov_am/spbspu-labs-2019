#include <iostream>
#include "phonebook.hpp"

PhoneBook::iterator PhoneBook::begin()
{
  return phoneBook_.begin();
}

PhoneBook::iterator PhoneBook::end()
{
  return phoneBook_.end();
}

void PhoneBook::viewCurrent(iterator iter)
{
  if (iter != phoneBook_.end())
  {
    std::cout << iter->number << " " << iter->name << "\n";
  }
  else
  {
    std::cout << "<EMPTY>\n";
  }
}

PhoneBook::iterator PhoneBook::next(iterator iter)
{
  if (std::next(iter) != phoneBook_.end())
  {
    return ++iter;
  }
  else
  {
    return iter;
  }
}

PhoneBook::iterator PhoneBook::prev(iterator iter)
{
  if (iter != phoneBook_.begin())
  {
    return --iter;
  }
  else
  {
    return iter;
  }
}

void PhoneBook::insertAfter(iterator iter, recordType& data)
{
  phoneBook_.insert(++iter, data);
}

void PhoneBook::insertBefore(iterator iter, recordType& data)
{
  phoneBook_.insert(iter, data);
}

PhoneBook::iterator PhoneBook::replace(iterator iter, recordType &data)
{
  *iter = data;
  return iter;
}

void PhoneBook::pushBack(const recordType &data)
{
  phoneBook_.push_back(data);
}

PhoneBook::iterator PhoneBook::remove(PhoneBook::iterator iter)
{
  return phoneBook_.erase(iter);
}

PhoneBook::iterator PhoneBook::move(iterator iter, int steps)
{
  if (steps >= 0)
  {
    while ((std::next(iter) != phoneBook_.end()) && (steps != 0))
    {
      ++iter;
      --steps;
    }
  }
  else
  {
    while ((iter != phoneBook_.begin()) && (steps != 0))
    {
      --iter;
      ++steps;
    }
  }

  return iter;
}

bool PhoneBook::isEmpty()
{
  return phoneBook_.empty();
}
