#include <algorithm>
#include <iostream>
#include "factorialContainer.hpp"

void task2()
{
  FactorialContainer factContainer;

  std::copy(factContainer.begin(), factContainer.end(), std::ostream_iterator<unsigned long long>(std::cout, " "));
  std::cout << "\n";

  std::reverse_copy(factContainer.begin(), factContainer.end(), std::ostream_iterator<unsigned long long>(std::cout, " "));
  std::cout << "\n";
}
