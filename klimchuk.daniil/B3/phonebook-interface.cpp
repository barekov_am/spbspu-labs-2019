#include <iostream>
#include <algorithm>
#include "phonebook-interface.hpp"

PhoneBookInterface::PhoneBookInterface()
{
  marks_["current"] = phoneBook_.end();
}

void PhoneBookInterface::add(const PhoneBook::recordType& data)
{
  phoneBook_.pushBack(data);
  if (++phoneBook_.begin() == phoneBook_.end())
  {
    marks_["current"] = phoneBook_.begin();
  }
}

void PhoneBookInterface::store(std::string& oldMarkName, std::string& newMarkName)
{
  auto iter = marks_.find(oldMarkName);
  if (iter == marks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  marks_.emplace(newMarkName, iter->second);
}

void PhoneBookInterface::insert(PhoneBookInterface::InsertLocation point, std::string& markName,
  PhoneBook::recordType& data)
{
  auto iter = marks_.find(markName);
  if (iter == marks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }
  if (iter->second == phoneBook_.end())
  {
    add(data);
    return;
  }
  if (point == InsertLocation::after)
  {
    phoneBook_.insertAfter(iter->second, data);
  }
  else
  {
    phoneBook_.insertBefore(iter->second, data);
  }
}

void PhoneBookInterface::remove(std::string& markName)
{
  auto iter = marks_.find(markName);
  if (iter == marks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  auto current = iter->second;

  std::for_each(marks_.begin(), marks_.end(), [&](auto& iter)
  {
    if (iter.second == current)
    {
      if (std::next(iter.second) == phoneBook_.end())
      {
        iter.second = phoneBook_.prev(current);
      }
      else
      {
        iter.second = phoneBook_.next(current);
      }
    }
  });
  phoneBook_.remove(current);
}

void PhoneBookInterface::view(std::string& markName)
{
  auto iter = marks_.find(markName);
  if (iter == marks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  if (phoneBook_.isEmpty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  if (iter->second == phoneBook_.end())
  {
    std::cout << "<EMPTY>\n";
    return;
  }
  phoneBook_.viewCurrent(iter->second);
}

void PhoneBookInterface::move(std::string& markName, int steps)
{
  auto iter = marks_.find(markName);
  if (iter == marks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }
  iter->second = phoneBook_.move(iter->second, steps);
}

void PhoneBookInterface::move(std::string& markName, PhoneBookInterface::MoveLocation direction)
{
  auto iter = marks_.find(markName);
  if (iter == marks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }
  if (direction == PhoneBookInterface::MoveLocation::first)
  {
    iter->second = phoneBook_.begin();
  }
  else
  {
    iter->second = phoneBook_.prev(phoneBook_.end());
  }
}
