#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <sstream>
#include <functional>
#include "phonebook-interface.hpp"

using command = std::function<void(PhoneBookInterface&, std::stringstream&)>;
command parseCommand(std::string&);

#endif //COMMANDS_HPP
