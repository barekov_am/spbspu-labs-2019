#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <string>
#include <list>

class PhoneBook
{
public:
  typedef struct
  {
    std::string name;
    std::string number;
  } recordType;

  using container = std::list<recordType>;
  using iterator = container::iterator;

  iterator begin();
  iterator end();
  iterator next(iterator iter);
  iterator prev(iterator iter);
  iterator replace(iterator iter, recordType& data);
  iterator remove(iterator iter);
  iterator move(iterator iter, int steps);

  void viewCurrent(iterator iter);
  void insertAfter(iterator iter, recordType& data);
  void insertBefore(iterator iter, recordType& data);
  void pushBack(const recordType& data);

  bool isEmpty();

private:
  container phoneBook_;
};

#endif //PHONEBOOK_HPP
