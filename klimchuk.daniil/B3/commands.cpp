#include "commands.hpp"
#include <cstring>
#include <iostream>
#include <cstdlib>

std::string getNumber(std::string& input)
{
  if (input.empty())
  {
    return "";
  }

  for (size_t i = 0; i < input.length(); ++i)
  {
    if (!std::isdigit(input[i]))
    {
      return "";
    }
  }
  return input;
}

std::string getMarkName(std::string& input)
{
  for  (size_t i = 0; i != input.length(); ++i)
  {
    if ((input[i] == '-') || (isalnum(input[i])))
    {
      return input;
    }
  }
  return "";
}

std::string getName(std::string& input)
{
  if ((input.empty()) || (input.front() != '\"'))
  {
    return "";
  }
  input.erase(input.begin());

  size_t i = 0;
  while ((i < input.size()) && (input[i] != '\"'))
  {
    if (input[i] == '\\')
    {
      if ((input[i+1] == '\"') && (i + 2 < input.size()))
      {
        input.erase(i, 1);
      }
      else
      {
        return "";
      }
    }
    ++i;
  }
  if (i == input.size())
  {
    return "";
  }
  input.erase(i);

  if (input.empty())
  {
    return "";
  }
  return input;
}

void add(PhoneBookInterface& bookmarks, std::stringstream& input)
{
  std::string number;
  input >> std::ws >> number;
  number = getNumber(number);

  std::string name;
  std::getline(input >> std::ws, name);
  name = getName(name);

  if ((number.empty()) || (name.empty()))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  bookmarks.add({ name, number });
}

void store(PhoneBookInterface& bookmarks, std::stringstream& input)
{
  std::string oldMarkName;
  input >> std::ws >> oldMarkName;
  oldMarkName = getMarkName(oldMarkName);

  std::string newMarkName;
  input >> std::ws >> newMarkName;
  newMarkName = getMarkName(newMarkName);

  if ((oldMarkName.empty()) || (newMarkName.empty()))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  bookmarks.store(oldMarkName, newMarkName);
}

void insert(PhoneBookInterface& bookmarks, std::stringstream& input)
{
  std::string location;
  input >> std::ws >> location;

  std::string markName;
  input >> std::ws >> markName;
  markName = getMarkName(markName);

  std::string number;
  input >> std::ws >> number;
  number = getNumber(number);

  std::string name;
  std::getline(input >> std::ws, name);
  name = getName(name);

  if ((markName.empty()) || (number.empty()) || (name.empty()))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  PhoneBook::recordType data = {name, number};
  if (std::strcmp(location.c_str(), "after") == 0)
  {
    bookmarks.insert(PhoneBookInterface::InsertLocation::after, markName, data);
  }
  else if (std::strcmp(location.c_str(), "before") == 0)
  {
    bookmarks.insert(PhoneBookInterface::InsertLocation::before, markName, data);
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
}

void erase(PhoneBookInterface& bookmark, std::stringstream& input)
{
  std::string markName;
  input >> std::ws >> markName;
  markName = getMarkName(markName);
  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  bookmark.remove(markName);
}

void view(PhoneBookInterface& bookmark, std::stringstream& input)
{
  std::string markName;
  input >> std::ws >> markName;
  markName = getMarkName(markName);
  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  bookmark.view(markName);
}

void move(PhoneBookInterface& bookmark, std::stringstream& input)
{
  std::string markName;
  input >> std::ws >> markName;
  markName = getMarkName(markName);
  if (markName.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::string steps;
  input >> std::ws >> steps;
  if (steps.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if ((steps.front() == '-') || (steps.front() == '+') || (std::isdigit(steps.front())))
  {
    int sign = 1;
    if (steps.front() == '-')
    {
      sign = -1;
      steps.erase(steps.begin());
    }
    else if (steps.front() == '+')
    {
      steps.erase(steps.begin());
    }

    steps = getNumber(steps);
    if (steps.empty())
    {
      std::cout << "<INVALID STEP>\n";
      return;
    }
    bookmark.move(markName, std::stoi(steps) * sign);
  }
  else
  {
    if (std::strcmp(steps.c_str(), "first") == 0)
    {
      bookmark.move(markName, PhoneBookInterface::MoveLocation::first);
    }
    else if (std::strcmp(steps.c_str(), "last") == 0)
    {
      bookmark.move(markName, PhoneBookInterface::MoveLocation::last);
    }
    else
    {
      std::cout << "<INVALID STEP>\n";
      return;
    }
  }
}

command parseCommand(std::string& command)
{
  if (strcmp("add", command.c_str()) == 0)
  {
    return add;
  }
  else if (strcmp("store", command.c_str()) == 0)
  {
    return store;
  }
  else if (strcmp("insert", command.c_str()) == 0)
  {
    return insert;
  }
  else if (strcmp("delete", command.c_str()) == 0)
  {
    return erase;
  }
  else if (strcmp("show", command.c_str()) == 0)
  {
    return view;
  }
  else if (strcmp("move", command.c_str()) == 0)
  {
    return move;
  }
  else
  {
    return [](PhoneBookInterface&, std::stringstream&)
    {
      std::cout << "<INVALID COMMAND>\n";
    };
  }
}
