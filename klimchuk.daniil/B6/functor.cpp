#include <iostream>

#include "functor.hpp"

Functor::Functor() :
  max_(0),
  min_(0),
  positive_(0),
  negative_(0),
  oddSum_(0),
  evenSum_(0),
  isEqual_(false),
  count_(0),
  firstNum_(0)
{ }

void Functor::operator()(const int& num)
{
  if (count_ == 0)
  {
    firstNum_ = num;
    min_ = num;
    max_ = num;
  }

  if (num < min_)
  {
    min_ = num;
  }

  if (num > max_)
  {
    max_ = num;
  }

  if (num > 0)
  {
    positive_++;
  }
  else if (num < 0)
  {
    negative_++;
  }

  if ((num % 2) == 0)
  {
    evenSum_ += num;
  }
  else
  {
    oddSum_ += num;
  }

  if (firstNum_ == num)
  {
    isEqual_ = true;
  }
  else
  {
    isEqual_ = false;
  }

  count_++;
}

void Functor::displayCalculation()
{
  if (count_ == 0)
  {
    std::cout << "No Data\n";
    return;
  }

  std::cout << "Max: " << max_ << '\n';
  std::cout << "Min: " << min_ << '\n';
  double mean = static_cast<double>(evenSum_ + oddSum_) / static_cast<double>(count_);
  std::cout << "Mean: " << mean << '\n';
  std::cout << "Positive: " << positive_ << '\n';
  std::cout << "Negative: " << negative_ << '\n';
  std::cout << "Odd Sum: " << oddSum_ << '\n';
  std::cout << "Even Sum: " << evenSum_ << '\n';
  if (isEqual_)
  {
    std::cout << "First/Last Equal: yes\n";
  }
  else
  {
    std::cout << "First/Last Equal: no\n";
  }
}
