#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <iostream>

class Functor
{
public:
  Functor();

  void operator()(const int& num);
  void displayCalculation();

private:
  int max_;
  int min_;
  int positive_;
  int negative_;
  long long int oddSum_;
  long long int evenSum_;
  bool isEqual_;
  int count_;
  int firstNum_;
};

#endif //FUNCTOR_HPP
