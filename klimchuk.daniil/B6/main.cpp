#include <iostream>
#include <algorithm>
#include <iterator>

#include "functor.hpp"
int main()
{
  try
  {
    Functor functor;
    functor = std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), functor);

    if (!std::cin.eof() && (std::cin.fail()))
    {
      throw std::ios_base::failure("Failed while reading data.\n");
    }

    functor.displayCalculation();
  }
  catch (const std::exception& exception)
  {
    std::cerr << exception.what();
    return 1;
  }

  return 0;
}
