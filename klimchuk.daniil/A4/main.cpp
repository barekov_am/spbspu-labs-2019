#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

int main()
{
  klimchuk::Rectangle rectangle(4, 5, {6, 7});
  klimchuk::Circle circle(5, {6, 7});

  klimchuk::shapePtr rectanglePtr = std::make_shared<klimchuk::Rectangle>(rectangle);
  klimchuk::shapePtr circlePtr = std::make_shared<klimchuk::Circle>(circle);

  klimchuk::CompositeShape compositeShape;
  compositeShape.add(rectanglePtr);
  compositeShape.printInfo();

  compositeShape.add(circlePtr);
  std::cout << "After adding new figure: " << std::endl;
  compositeShape.printInfo();

  compositeShape.move(3.7, 1.0);
  std::cout << "Let's move the shape 3.7 units right and 1 units up : " << std::endl;
  compositeShape.printInfo();

  compositeShape.move({10.0, 2.3});
  std::cout << "Let's move CompositeShape at the point (10, 2.3): " << std::endl;
  compositeShape.printInfo();

  compositeShape.scale(2.0);
  std::cout << "Let's scaling the shape by factor = 2: " << std::endl;
  compositeShape.printInfo();

  compositeShape.rotate(30.0);
  std::cout << "After 30 degree rotation: " << std::endl;
  compositeShape.printInfo();

  std::cout << "After deleting the first figure: " << std::endl;
  compositeShape.remove(0);
  compositeShape.printInfo();

  klimchuk::Rectangle rectangleNew(6.0, 2.0, {8.0, 6.0});
  klimchuk::shapePtr rectangleNewPtr = std::make_shared<klimchuk::Rectangle>(rectangleNew);
  klimchuk::Circle circleNew(1.0, {11.0, 5.0});
  klimchuk::shapePtr circleNewPtr = std::make_shared<klimchuk::Circle>(circleNew);

  compositeShape.add(rectangleNewPtr);
  compositeShape.add(circleNewPtr);

  klimchuk::Matrix matrix;
  matrix = klimchuk::part(compositeShape);
  matrix.printInfo();

return 0;
}
