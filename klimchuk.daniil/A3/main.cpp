#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  std::cout << "New rectangle with width 4, height 6 and center (10,11)." << std::endl << std::endl;
  klimchuk::Rectangle rectangle1(4, 6, {10, 11});
  std::cout << "New rectangle with width 10, height 4 and center (-7,17)." << std::endl << std::endl;
  klimchuk::Rectangle rectangle2(10, 4, {-7, 17});
  std::cout << "New circle with radius 5 and center (16,3)." << std::endl << std::endl;
  klimchuk::Circle circle(5, {16, 3});

  klimchuk::CompositeShape composite_shape;
  klimchuk::shapePtr rectanglePtr1 = std::make_shared<klimchuk::Rectangle>(rectangle1);
  klimchuk::shapePtr rectanglePtr2 = std::make_shared<klimchuk::Rectangle>(rectangle2);
  klimchuk::shapePtr circlePtr = std::make_shared<klimchuk::Rectangle>(rectangle2);
  composite_shape.add(rectanglePtr1);
  composite_shape.add(rectanglePtr2);
  composite_shape.add(circlePtr);

  std::cout << "That's what we have after adding the shapes:" << std::endl;
  composite_shape.printInfo();

  std::cout << "Let's move CompositeShape at the point (-8,12)." << std::endl;
  composite_shape.move({-8, 12});
  composite_shape.printInfo();

  std::cout << "Let's move the shape 2 units right and 6 units down." << std::endl;
  composite_shape.move(2, -6);
  composite_shape.printInfo();

  std::cout << "Let's scaling the shape by factor = 4." << std::endl;
  composite_shape.scale(4);
  composite_shape.printInfo();

  std::cout << "Let's try to remove second rectangle." << std::endl;
  composite_shape.remove(1);
  composite_shape.printInfo();

  return 0;
}
