#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

int main()
{
  klimchuk::Rectangle rectangleTest = {5.0, 3.5, {-1.0, -2.0}};
  klimchuk::Shape* shapeTest = &rectangleTest;
  std::cout << "That's what we know about shape:" << std::endl;
  shapeTest->printInfo();
  std::cout << "Let's move the shape 2 units left and 7.77 units up." << std::endl;
  shapeTest->move(-2.0, 7.77);
  shapeTest->printInfo();
  std::cout << "Now we will move the shape to a specific point (3, 4)." << std::endl;
  shapeTest->move({3.0, 4.0});
  shapeTest->printInfo();
  std::cout << "Let's scaling the shape by factor = 1.8." << std::endl;
  shapeTest->scale(1.8);
  shapeTest->printInfo();

  klimchuk::Circle circleTest = {2.5, {3.14, 2.71}};
  shapeTest = &circleTest;
  std::cout << "That's what we know about shape:" << std::endl;
  shapeTest->printInfo();
  std::cout << "Let's move the shape 2 units left and 7.77 units up." << std::endl;
  shapeTest->move(-2.0, 7.77);
  shapeTest->printInfo();
  std::cout << "Now we will move the shape to a specific point (3, 4)." << std::endl;
  shapeTest->move({3.0, 4.0});
  shapeTest->printInfo();
  std::cout << "Let's scaling the shape by factor = 0.72." << std::endl;
  shapeTest->scale(0.72);
  shapeTest->printInfo();

  klimchuk::Triangle triangleTest = {{13.0, 42.0}, {31.0, 28.0}, {25.0, 20.0}};
  shapeTest = &triangleTest;
  std::cout << "That's what we know about shape:" << std::endl;
  shapeTest->printInfo();
  std::cout << "Let's move the shape 2 units left and 7.77 units up." << std::endl;
  shapeTest->move(-2.0, 7.77);
  shapeTest->printInfo();
  std::cout << "Now we will move the shape to a specific point (3, 4)." << std::endl;
  shapeTest->move({3.0, 4.0});
  shapeTest->printInfo();
  std::cout << "Let's scaling the shape by factor = 2.6." << std::endl;
  shapeTest->scale(2.6);
  shapeTest->printInfo();

  klimchuk::point_t vertices[] = {{3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {3.0, 5.0}};
  size_t size = sizeof(vertices) / sizeof(vertices[0]);
  klimchuk::Polygon polygonTest(size, vertices);
  shapeTest = &polygonTest;
  std::cout << "That's what we know about shape:" << std::endl;
  shapeTest->printInfo();
  std::cout << "Let's move the shape 2 units left and 7.77 units up." << std::endl;
  shapeTest->move(-2.0, 7.77);
  shapeTest->printInfo();
  std::cout << "Now we will move the shape to a specific point (3, 4)." << std::endl;
  shapeTest->move({3.0, 4.0});
  shapeTest->printInfo();
  std::cout << "Let's scaling the shape by factor = 2.0." << std::endl;
  shapeTest->scale(2.0);
  shapeTest->printInfo();

  return 0;
}
