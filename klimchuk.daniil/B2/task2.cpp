#include <list>
#include <iostream>

void print(std::list<int>::iterator begin, std::list<int>::iterator end)
{
  if (begin == end)
  {
    std::cout << "\n";
    return;
  }

  if (begin == std::prev(end))
  {
    std::cout << *begin << "\n";
    return;
  }

  std::cout << *begin << " " << *std::prev(end) << " ";
  begin++;
  end--;
  print(begin, end);
}

void task2()
{
  const int min = 1;
  const int max = 20;
  const size_t capacity = 20;
  int num = 0;

  std::list<int> list;

  while (std::cin >> num)
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Failed while reading data");
    }

    if ((num < min) || (num > max))
    {
      throw std::invalid_argument("Every number must be in range 1-20");
    }
    list.push_back(num);

    if (list.size() > capacity)
    {
      throw std::invalid_argument("Count of numbers must be < 20");
    }
  }
  if (!std::cin.eof())
  {
    throw std::runtime_error("Failed while reading data");
  }
  if (list.empty())
  {
    return;
  }
  print(list.begin(), list.end());
}
