#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <list>
#include <iostream>

template <typename T>
class QueueWithPriority
{
public:
  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  void pushElement(const T& element, ElementPriority priority);
  T getElement();
  void Accelerate();
  bool isEmpty();

private:
  std::list<T> low_;
  std::list<T> normal_;
  std::list<T> high_;
};

template <typename T>
void QueueWithPriority<T>::pushElement(const T& element, ElementPriority priority)
{
  switch(priority)
  {
    case ElementPriority::LOW:
      low_.push_back(element);
      break;

    case ElementPriority::NORMAL:
      normal_.push_back(element);
      break;

    case ElementPriority::HIGH:
      high_.push_back(element);
      break;
  }
}

template <typename T>
T QueueWithPriority<T>::getElement()
{
  if (!high_.empty())
  {
    T tmp = *high_.begin();
    high_.pop_front();
    return tmp;
  }

  if (!normal_.empty())
  {
    T tmp = *normal_.begin();
    normal_.pop_front();
    return tmp;
  }

  if (!low_.empty())
  {
    T tmp = *low_.begin();
    low_.pop_front();
    return tmp;
  }

  throw std::runtime_error("Queue is empty!");
}

template <typename T>
bool QueueWithPriority<T>::isEmpty()
{
  return low_.empty() && normal_.empty() && high_.empty();
}

template <typename T>
void QueueWithPriority<T>::Accelerate()
{
  high_.splice(high_.end(), low_);
}

#endif //QUEUE_HPP
