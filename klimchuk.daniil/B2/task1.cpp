#include <cstring>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include "queue.hpp"

void add(QueueWithPriority<std::string>& queue, std::stringstream& stream);
void get(QueueWithPriority<std::string>& queue, std::stringstream& stream);
void accelerate(QueueWithPriority<std::string>& queue, std::stringstream& stream);

void task1()
{
  QueueWithPriority<std::string> queue;

  std::string data;
  while (getline(std::cin, data))
  {
    if (!std::cin.eof() && std::cin.fail())
    {
      throw std::runtime_error("Failed while reading data");
    }

    std::stringstream stream(data);
    std::string command;
    std::getline(stream >> std::ws, command, ' ');

    if (strcmp(command.data(), "add") == 0)
    {
      add(queue, stream);
    }
    else if (strcmp(command.data(), "get") == 0)
    {
      get(queue, stream);
    }
    else if (strcmp(command.data(), "accelerate") == 0)
    {
      accelerate(queue, stream);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
