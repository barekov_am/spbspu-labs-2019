#include <cstring>
#include <sstream>
#include "queue.hpp"

void add(QueueWithPriority<std::string>& queue, std::stringstream& stream)
{
  std::string priority;
  stream >> std::ws >> priority;
  std::string data;
  std::getline(stream >> std::ws, data);

  if (data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (strcmp(priority.data(), "low") == 0)
  {
    queue.pushElement(data, QueueWithPriority<std::string>::ElementPriority::LOW);
  }
  else if (strcmp(priority.data(), "normal") == 0)
  {
    queue.pushElement(data, QueueWithPriority<std::string>::ElementPriority::NORMAL);
  }
  else if (strcmp(priority.data(), "high") == 0)
  {
    queue.pushElement(data, QueueWithPriority<std::string>::ElementPriority::HIGH);
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
  }
}

void get(QueueWithPriority<std::string>& queue, std::stringstream& stream)
{
  std::string data;
  std::getline(stream >> std::ws, data);

  if (!data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (queue.isEmpty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  std::cout << queue.getElement() << "\n";
}

void accelerate(QueueWithPriority<std::string>& queue, std::stringstream& stream)
{
  std::string data;
  std::getline(stream >> std::ws, data);

  if (!data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.Accelerate();
}
