#include <iostream>
#include <string>
#include <cstring>
#include <iterator>
#include <vector>
#include "text.hpp"

int main(int argc, char* argv[])
{
  if (argc > 3 || argc == 2)
  {
    std::cerr << "Incorrect count of arguments.\n";
    return 1;
  }

  try
  {
    size_t lineWidth = 40;
    if (argc == 3)
    {
      if (strcmp(argv[1], "--line-width") != 0)
      {
        std::cerr << "Incorrect argument!\n";
        return 1;
      }
      lineWidth = std::stoi(argv[2]);
      if (lineWidth < 25)
      {
        std::cerr << "Line-width must be 25 or more!\n";
        return 1;
      }
    }
    Text file;
    file.read();
    file.formatting(lineWidth);
    file.write();
  }
  catch (std::exception& exception)
  {
    std::cerr << exception.what();
    return 1;
  }

  return 0;
}
