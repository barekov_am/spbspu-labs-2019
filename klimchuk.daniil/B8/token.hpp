#ifndef TOKEN_HPP
#define TOKEN_HPP

#include <string>

struct token_t
{
  enum token_type
  {
    WORD,
    NUMBER,
    PUNC,
    DASH,
    INVALID
  };
  token_type type;
  std::string value;
};

#endif //TOKEN_HPP
