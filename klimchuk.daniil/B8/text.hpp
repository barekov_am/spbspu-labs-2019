#ifndef TEXT_HPP
#define TEXT_HPP

#include <vector>
#include "token.hpp"

class Text
{
public:
  void read();
  void formatting(size_t size);
  void write() const;

private:
  void readWord();
  void readNumber();
  void readOther();
  bool isDefined(const char &p) const;
  char puncArray_[6] = { '.', ',', '!', '?', ':', ';' };
  std::vector<token_t> token_;
  std::vector<std::string> strings_;
};

#endif //TEXT_HPP
