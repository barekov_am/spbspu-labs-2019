#include <iostream>
#include <iterator>
#include <cctype>
#include <string>
#include "text.hpp"

void Text::read()
{
  char symbol;
  std::cin >> std::noskipws;
  while (std::cin >> symbol)
  {
    while (isspace(symbol) && (!std::cin.eof()))
    {
      std::cin >> symbol;
    }
    if (isalpha(symbol))
    {
      std::cin.unget();
      readWord();
    }
    else if (isdigit(symbol) || ((symbol == '+' || symbol == '-') && (isdigit(std::cin.peek()))))
    {
      std::cin.unget();
      readNumber();
    }
    else if ((symbol == '-' || isDefined(symbol)) && (!token_.empty()))
    {
      std::cin.unget();
      readOther();
    }
    else if (!isspace(symbol))
    {
      throw std::invalid_argument("Incorrect data type!\n");
    }
  }
}

void Text::readWord()
{
  token_t token{ token_t::WORD, "" };
  char symbol = std::cin.get();
  while ((isalpha(symbol) || (symbol == '-')) && (!std::cin.eof()))
  {
    if ((symbol == '-') && (!isalpha(std::cin.peek())))
    {
      if ((isDefined(std::cin.peek())) || (isspace(std::cin.peek())))
      {
        token.value.push_back(symbol);
        token_.push_back(token);
        return;
      }
      else if (isdigit(std::cin.peek()))
      {
        throw std::invalid_argument("Incorrect input!\n");
      }
      else if (std::cin.get() == '-')
      {
        symbol = std::cin.get();
        if (symbol != '-')
        {
          throw std::invalid_argument("Double hyphen!\n");
        }
        else if (symbol == '-' && std::cin.peek() == '-')
        {
          throw std::invalid_argument("Dash has only 3 hyphens!\n");
        }
        else if (isDefined(std::cin.peek()))
        {
          throw std::invalid_argument("Double punctuation!\n");
        }
        token_.push_back(token);
        token.type = token_t::DASH;
        token.value = "---";
        token_.push_back(token);
        return;
      }
    }
    token.value.push_back(symbol);
    std::cin >> symbol;
  }
  if (token.value.size() > 20)
  {
    throw std::invalid_argument("Too long word!\n");
  }
  token_.push_back(token);
  std::cin.unget();
}

void Text::readNumber()
{
  token_t token{ token_t::NUMBER, "" };
  char symbol = std::cin.get();
  while ((isdigit(symbol) || (symbol == '.') || (symbol == '+') || (symbol == '-')) && (!std::cin.eof()))
  {
    if ((symbol == '.') && (!isdigit(std::cin.peek())))
    {
      break;
    }
    token.value.push_back(symbol);
    std::cin >> symbol;
  }
  if (token.value.size() > 20)
  {
    throw std::invalid_argument("Too long number!\n");
  }
  token_.push_back(token);
  std::cin.unget();
}

void Text::readOther()
{
  token_t token{ token_t::PUNC, "" };
  char symbol = std::cin.get();
  if ((isDefined(symbol)) && (isDefined(std::cin.peek())))
  {
    throw std::invalid_argument("Double punctuation!\n");
  }
  if ((symbol == '-') && (isalpha(std::cin.peek())))
  {
    throw std::invalid_argument("Hyphen before word!\n");
  }
  if (isDefined(symbol))
  {
    token.value.push_back(symbol);
    token_.push_back(token);
    return;
  }
  if ((symbol == '-') && (std::cin.get() == '-'))
  {
    symbol = std::cin.get();
    if (symbol != '-')
    {
      throw std::invalid_argument("Double hyphen!");
    }
    if ((symbol == '-') && (std::cin.peek() == '-'))
    {
      throw std::invalid_argument("Dash has only 3 hyphens!\n");
    }
    if (isDefined(std::cin.peek()))
    {
      throw std::invalid_argument("Double punctuation!\n");
    }
    token.type = token_t::DASH;
    token.value = "---";
    token_.push_back(token);
  }
}

bool Text::isDefined(const char &p) const
{
  for (size_t i = 0; i < 6; i++)
  {
    if (p == puncArray_[i])
    {
      return true;
    }
  }
  return false;
}

void Text::formatting(size_t size)
{
  int strSize = size;
  std::string str;
  size_t last = 4;
  for (auto i = token_.begin(); i != token_.end(); i++)
  {
    token_t token = *i;
    token_t::token_type next_type = token_t::INVALID;
    if ((i + 1) != token_.end())
    {
      next_type = (i + 1)->type;
    }
    strSize -= token.value.size();
    if ((token.type == 2) && (last == 2))
    {
      throw std::invalid_argument("Double punctuation!\n");
    }
    if ((token.type == 3) && (last == 3))
    {
      throw std::invalid_argument("Double dash!\n");
    }
    if ((token.type == 2) && (token.value != ",") && (next_type == 3))
    {
      throw std::invalid_argument("Punctuation before dash!\n");
    }
    if ((token.type == 0) || (token.type == 1) || (token.type == 3))
    {
      if (last != 4)
      {
        strSize--;
      }
    }
    if ((strSize < 0) || ((strSize < 4) && (next_type == 3)) || ((strSize == 0) && (next_type == 2)))
    {
      strings_.push_back(str);
      str.clear();
      strSize = size - token.value.size();
      last = 4;
    }
    if ((token.type == 0) || (token.type == 1) || (token.type == 3))
    {
      if (last != 4)
      {
        str += " ";
      }
    }
    str += token.value;
    last = token.type;
    if (next_type == 4)
    {
      strings_.push_back(str);
    }
  }
}

void Text::write() const
{
  std::copy(strings_.begin(), strings_.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
}
