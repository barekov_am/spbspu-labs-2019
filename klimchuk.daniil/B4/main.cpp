#include <iostream>

#include "data-struct.hpp"
#include "functions.hpp"

int main()
{
  try
  {
    std::vector<DataStruct> vector = readStruct();
    sort(vector);
    print(vector);
  }

  catch (std::exception& ex)
  {
    std::cerr << ex.what();
    return 1;
  }

  return 0;
}
