#include <stdexcept>
#include <iostream>
#include <algorithm>
#include "functions.hpp"

std::vector<DataStruct> readStruct()
{
  const int maxValue = 5;
  const int minValue = -5;
  std::vector<DataStruct> vector;
  std::string stream;

  while (getline(std::cin >> std::ws, stream))
  {
    if (stream.empty())
    {
      throw std::runtime_error("Failed while reading.\n");
    }

    auto comma = stream.find_first_of(',');
    if (comma == std::string::npos)
    {
      throw std::invalid_argument("Invalid key.\n");
    }
    int key1 = std::stoi(stream.substr(0, comma));
    stream.erase(0, comma + 1);

    comma = stream.find_first_of(',');
    if (comma == std::string::npos)
    {
      throw std::invalid_argument("Invalid key.\n");
    }
    int key2 = std::stoi(stream.substr(0, comma));
    stream.erase(0, comma + 1);

    while ((stream.at(0) == ' ') || (stream.at(0) == '\t'))
    {
      stream.erase(0, 1);
    }

    if ((key1 < minValue) || (key1 > maxValue) || (key2 < minValue) || (key2 > maxValue))
    {
      throw std::out_of_range("Value is out of range.\n");
    }
    vector.push_back({key1, key2, stream});
  }
  return vector;
}

bool keyComparison(const DataStruct& first, const DataStruct& second)
{
  if (first.key1 < second.key1)
  {
    return true;
  }
  else if (first.key1 == second.key1)
  {
    if (first.key2 < second.key2)
    {
      return true;
    }
    else if (first.key2 == second.key2)
    {
      if (first.str.size() < second.str.size())
      {
        return true;
      }
    }
  }

  return false;
}

void sort(std::vector<DataStruct>& vector)
{
  std::sort(vector.begin(), vector.end(), keyComparison);
}

void print(const std::vector<DataStruct>& vector)
{
  for (size_t i = 0; i < vector.size(); ++i)
  {
    std::cout << vector[i].key1 << "," << vector[i].key2 << "," << vector[i].str << '\n';
  }
}
