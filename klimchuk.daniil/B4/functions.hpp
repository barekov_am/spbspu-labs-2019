#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <vector>
#include "data-struct.hpp"

std::vector<DataStruct> readStruct();
void print(const std::vector<DataStruct>& vector);
void sort(std::vector<DataStruct>& vector);
bool keyComparison(const DataStruct& key1, const DataStruct& key2);

#endif //FUNCTIONS_HPP
