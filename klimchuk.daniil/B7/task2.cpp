#include <iostream>
#include <memory>
#include <algorithm>
#include <vector>
#include <string>

#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"

std::vector<std::shared_ptr<Shape>> read()
{
  std::vector<std::shared_ptr<Shape>> shapes;
  std::string stream;

  while (std::getline(std::cin >> std::ws, stream))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed while reading data.\n");
    }

    stream.erase(std::remove_if(stream.begin(), stream.end(), [](const char sign) {return std::isspace(sign);}), stream.end());

    if (stream.empty())
    {
      continue;
    }
    auto openBracket = stream.find_first_of('(');
    if (openBracket == std::string::npos)
    {
      throw std::invalid_argument("Invalid parameters.\n");
    }
    std::string name = stream.substr(0, openBracket);
    stream.erase(0, openBracket);
    if ((name != "CIRCLE") && (name != "SQUARE") && (name != "TRIANGLE"))
    {
      throw std::invalid_argument("Incorrect figure name.\n");
    }

    openBracket = stream.find_first_of('(');
    auto separator = stream.find_first_of(';');
    auto closeBracket = stream.find_first_of(')');
    if ((separator == std::string::npos) || (closeBracket == std::string::npos))
    {
      throw std::invalid_argument("Incorrect point.\n");
    }

    int x = std::stoi(stream.substr(openBracket + 1, separator - (openBracket + 1)));
    int y = std::stoi(stream.substr(separator + 1, closeBracket - (separator + 1)));

    if (name == "CIRCLE")
    {
      shapes.push_back(std::make_shared<Circle>(Circle({x, y})));
    }
    if (name == "SQUARE")
    {
      shapes.push_back(std::make_shared<Square>(Square({x, y})));
    }
    if (name == "TRIANGLE")
    {
      shapes.push_back(std::make_shared<Triangle>(Triangle({x, y})));
    }
  }
  return shapes;
}

void printInfo(const std::vector<std::shared_ptr<Shape>>& vector)
{
  for (const auto & shape : vector)
  {
    shape->draw(std::cout);
  }
}

void task2()
{
  std::vector<std::shared_ptr<Shape>> shapes = read();

  std::cout << "Original:\n";
  printInfo(shapes);

  std::cout << "Left-Right:\n";
  auto leftRightComparison = [](const std::shared_ptr<Shape>& a, const std::shared_ptr<Shape>& b)
  {
    return a->isMoreLeft(b);
  };
  std::sort(shapes.begin(), shapes.end(), leftRightComparison);
  printInfo(shapes);

  std::cout << "Right-Left:\n";
  std::sort(shapes.rbegin(), shapes.rend(), leftRightComparison);
  printInfo(shapes);

  std::cout << "Top-Bottom:\n";
  auto topBottomComparison = [](const std::shared_ptr<Shape>& a, const std::shared_ptr<Shape>& b)
  {
    return a->isUpper(b);
  };
  std::sort(shapes.begin(), shapes.end(), topBottomComparison);
  printInfo(shapes);

  std::cout << "Bottom-Top:\n";
  std::sort(shapes.rbegin(), shapes.rend(), topBottomComparison);
  printInfo(shapes);
}
