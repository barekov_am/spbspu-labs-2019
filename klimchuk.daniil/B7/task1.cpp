#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <cmath>

void task1()
{
  std::vector<double> numbers((std::istream_iterator<double>(std::cin)), std::istream_iterator<double>());

  if (!std::cin.eof())
  {
    throw std::invalid_argument("Failed while reading data.\n");
  }

  std::for_each(numbers.begin(), numbers.end(), [](double& number) {number *= M_PI;});
  std::copy(numbers.begin(), numbers.end(), std::ostream_iterator<double>(std::cout, "\n"));
}
