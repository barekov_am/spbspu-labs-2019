#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>

class Shape
{
public:
  Shape(int x, int y);

  bool isMoreLeft(const std::shared_ptr<Shape>& shape) const;
  bool isUpper(const std::shared_ptr<Shape>& shape) const;
  virtual void draw(std::ostream& out) const = 0;

protected:
  int x_;
  int y_;
};

#endif //SHAPE_HPP
