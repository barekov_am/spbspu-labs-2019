#include <fstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <vector>

static const size_t  InitialSize = 100;

void task2(const char* FileName)
{
  std::ifstream inputFile(FileName);
  if (!inputFile)
  {
    throw std::ios_base::failure("Failed to open.");
  }

  size_t  size = InitialSize;

  std::unique_ptr<char[], decltype(&free)> array(static_cast<char*>(malloc(size)), &free);

  size_t i = 0;
  while (inputFile)
  {
    inputFile.read(&array[i], InitialSize);
    i += inputFile.gcount();

    if (inputFile.gcount() == InitialSize)
    {
      size += InitialSize;
      std::unique_ptr<char[], decltype(&free)> temp(static_cast<char*>(realloc(array.get(), size)), &free);

      if (temp)
      {
        array.release();
        std::swap(array, temp);
      }
      else
      {
        throw std::runtime_error("Failed to reallocate.");
      }
    }
  }
  inputFile.close();

  if (inputFile.is_open())
  {
    throw std::ios_base::failure("Failed to close.");
  }

  std::vector<char> vector(&array[0], &array[i]);

  for (const auto& info : vector)
  {
    std::cout << info;
  }
}
