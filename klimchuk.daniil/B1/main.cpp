#include <iostream>
#include <stdexcept>
#include <ctime>
#include <string.h>

void task1(const char* direction);
void task2(const char* FileName);
void task3();
void task4(const char* direction, size_t size);

int main(int argc, char* argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4))
    {
      std::cerr << "Incorrect number of arguments.";
      return 1;
    }

    int taskNumber = std::stoi(argv[1]);
    switch (taskNumber)
    {
      case 1:
        if (argc != 3)
        {
          std::cerr << "Incorrect number of arguments.";
          return 1;
        }

        task1(argv[2]);
        break;

      case 2:
        if (argc != 3)
        {
          std::cerr << "Incorrect number of arguments.";
          return 1;
        }

        task2(argv[2]);
        break;

      case 3:
        if (argc != 2)
        {
          std::cerr << "Incorrect number of arguments.";
          return 1;
        }

        task3();
        break;

      case 4:
        if (argc != 4)
        {
          std::cerr << "Incorrect number of arguments.";
          return 1;
        }

        srand(time(nullptr));
        task4(argv[2], std::stoi(argv[3]));
        break;

      default:
        std::cerr << "Incorrect task number.";
        return 1;
    }
  }

  catch (const std::exception& exception)
  {
    std::cerr << exception.what();
    return 1;
  }

  return 0;
}
