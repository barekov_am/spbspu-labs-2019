#include <exception>
#include <vector>
#include <forward_list>
#include "accesses.hpp"
#include "sorting.hpp"

void task1(const char* direction)
{
  auto order = getOrder<int>(direction);
  std::vector<int> vectorBr;
  int num = 0;
  while (std::cin >> num)
  {
    vectorBr.push_back(num);
  }

  if ((!std::cin.eof()) && (std::cin.fail()))
  {
    throw std::runtime_error("Error while reading data.");
  }

  if (vectorBr.empty())
  {
    return;
  }

  std::vector<int> vectorAt(vectorBr);
  std::forward_list<int> list(vectorBr.begin(), vectorBr.end());

  sort<AccessBracket>(vectorBr, order);
  print(vectorBr);

  sort<AccessAt>(vectorAt, order);
  print(vectorAt);

  sort<AccessIterator>(list, order);
  print(list);
}
