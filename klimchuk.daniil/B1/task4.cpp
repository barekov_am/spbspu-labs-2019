#include <vector>
#include <random>
#include <ctime>
#include <exception>
#include "sorting.hpp"

void fillRandom(double* array, size_t size)
{
  std::mt19937 rng(time(nullptr));
  std::uniform_real_distribution<double> dist(-1.0, 1.0);
  for (size_t i = 0; i < size; ++i)
  {
    array[i] = dist(rng);
  }
}

void task4(const char* direction, size_t size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size can't be less than zero.");
  }

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);
  print(vector);

  sort<AccessBracket>(vector, getOrder<double>(direction));
  print(vector);
}
