#ifndef ACCESSES_HPP
#define ACCESSES_HPP

#include <iostream>

template <typename T>
struct AccessBracket
{
  typedef size_t index;

  static index begin(const T&)
  {
    return 0;
  }

  static index end(const T& Container)
  {
    return  Container.size();
  }

  static typename  T::reference getElement(T& Container, index i)
  {
    return Container[i];
  }
};

template <typename T>
struct AccessAt
{
  typedef size_t index;

  static index begin(const T&)
  {
    return 0;
  }

  static index end(const T& Container)
  {
    return Container.size();
  }

  static typename T::reference getElement(T& Container, index i)
  {
    return Container.at(i);
  }
};

template <typename T>
struct AccessIterator
{
  typedef typename T::iterator index;

  static index begin(T& Container)
  {
    return Container.begin();
  }

  static index end(T& Container)
  {
    return Container.end();
  }

  static typename T::reference getElement(T&, index& iterator)
  {
    return *iterator;
  }
};

#endif //ACCESSES_HPP
