#ifndef SORTING_HPP
#define SORTING_HPP

#include <iostream>
#include <functional>
#include <string.h>
#include "accesses.hpp"

template <class T>
void print(const T& Container)
{
  for (const auto& i : Container)
  {
    std::cout << i << " ";
  }
  std::cout << std::endl;
}

template <typename T>
std::function<bool(T, T)> getOrder(const char* direction)
{
  if (strcmp(direction, "ascending") == 0)
  {
    return std::greater<T>();
  }
  else if (strcmp(direction, "descending") == 0)
  {
    return  std::less<T>();
  }
  throw std::invalid_argument("Incorrect direction.");
}

template <template<class> class Access, class T>
void sort(T& Container, std::function<bool(typename T::value_type, typename T::value_type)> compare)
{
  const auto begin = Access<T>::begin(Container);
  const auto end = Access<T>::end(Container);

  for (auto i = begin; i != end; ++i)
  {
    for (auto j = i; j != end; ++j)
    {
      if (compare(Access<T>::getElement(Container, i), Access<T>::getElement(Container, j)))
      {
        std::swap(Access<T>::getElement(Container, i), Access<T>::getElement(Container, j));
      }
    }

  }

}

#endif //SORTING_HPP
