#include <iostream>
#include "tasks.hpp"
#include <ctime>

int main(int argc, char *argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4))
    {
      std::cerr << "Number of arguments is incorrect" << argc << std::endl;
      return 1;
    }

    const int task = std::stoi(argv[1]);

    switch (task)
    {
      case 1:
      {
        if (argc != 3) {
          std::cerr << "Number of arguments is incorrect";
          return 1;
        }
        if (argv[2] == nullptr) {
          std::cerr << "Direction is incorrect";
          return 1;
        }
        firstTask(argv[2]);
        break;
      }

      case 2:
      {
        if (argc != 3) {
          std::cerr << "Number of arguments is incorrect";
          return 1;
        }

        secondTask(argv[2]);
        break;
      }

      case 3:
      {
        if (argc != 2) {
          std::cerr << "Number of arguments is incorrect";
          return 1;
        }
        thirdTask();
        break;
      }

      case 4:
      {
        if (argc != 4) {
          std::cerr << "Number of parameters is incorrect for this task";
          return 1;
        }

        fourthTask(argv[2], std::stoi(argv[3]));
        break;
      }

      default:
      {
        std::cerr << "Task number is incorrect";
        return 1;
      }
    }
  }

  catch(const std::exception &e)
  {
    std::cerr << e.what();
    return 1;
  }
}
