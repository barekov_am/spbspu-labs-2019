#include <vector>
#include <fstream>
#include <memory>
#include <iostream>

const size_t initSize = 256;
void secondTask(const char* file)
{
  std::ifstream inputFile(file);
  if (!inputFile)
  {
    throw std::invalid_argument("Name of input file is incorrect.");
  }

  std::unique_ptr<char[], decltype(&free)> firstArray(static_cast<char*>(malloc(initSize)), &free);
  if (!firstArray)
  {
    throw std::runtime_error("Memory could not be allocated.");
  }

  std::size_t index = 0;
  std::size_t size = initSize;
  while (!inputFile.eof())
  {
    inputFile.read(&firstArray[index], initSize);
    index += inputFile.gcount();

    if (inputFile.gcount() == initSize)
    {
      size += initSize;
      std::unique_ptr<char[], decltype(&free)> tmpArray(static_cast<char*>(realloc(firstArray.get(), size)), &free);
      if (!tmpArray)
      {
        throw std::runtime_error("Memory could not be reallocated.");
      }
      firstArray.release();
      std::swap(firstArray, tmpArray);
    }

    if (!inputFile.eof() && inputFile.fail())
    {
      throw std::runtime_error("Failed to read from input file.");
    }
  }

  std::vector<char> vector(&firstArray[0], &firstArray[index]);
  for (auto element : vector)
  {
    std::cout << element;
  }
}
