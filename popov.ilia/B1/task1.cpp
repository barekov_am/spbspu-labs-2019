#include <vector>
#include <forward_list>

#include "functions.hpp"
#include "tasks.hpp"

void firstTask(const char *dir)
{
  auto order = getDirection<int>(dir);
  std::vector<int> vector;

  int nextElement = -1;
  while (std::cin && !(std::cin >> nextElement).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error while reading input data");
    }

    vector.push_back(nextElement);
  }
  if (vector.empty())
  {
    return;
  }

  std::vector<int> vectAt(vector);
  std::forward_list<int> listIter(vector.begin(), vector.end());

  sort<bracketAccess>(vector, order);
  print(vector);

  sort<atAccess>(vectAt, order);
  print(vectAt);

  sort<iteratorAccess>(listIter, order);
  print(listIter);
}
