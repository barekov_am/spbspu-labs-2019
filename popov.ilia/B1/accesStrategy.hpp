#ifndef B11_ACCESSTRATEGY_HPP
#define B11_ACCESSTRATEGY_HPP

#include <cstddef>
#include <iterator>

template <typename Container>
struct bracketAccess
{
    static std::size_t begin(const Container &)
    {
      return 0;
    }

    static std::size_t end(const Container &cont)
    {
      return cont.size();
    }

    static std::size_t next(size_t index)
    {
      return ++index;
    }

    static typename Container::reference get(Container &cont, size_t index)
    {
      return cont[index];
    }
};

template <typename Container>
struct atAccess
{
    static std::size_t begin(const Container &)
    {
      return 0;
    }

    static std::size_t end(const Container &cont)
    {
      return cont.size();
    }

    static std::size_t next(size_t index)
    {
      return ++index;
    }

    static typename Container::reference get(Container &cont, size_t index)
    {
      return cont.at(index);
    }
};

template <typename Container>
struct iteratorAccess
{
    static typename Container::iterator begin(Container &cont)
    {
      return cont.begin();
    }

    static typename Container::iterator end(Container &cont)
    {
      return cont.end();
    }

    static typename Container::iterator next(typename Container::iterator iterator)
    {
      return ++iterator;
    }

    static typename Container::reference get(Container &, typename Container::iterator iterator)
    {
      return *iterator;
    }

};

#endif //B11_ACCESSTRATEGY_HPP
