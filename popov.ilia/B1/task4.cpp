#include <vector>
#include <random>

#include "functions.hpp"
#include "tasks.hpp"

void fillRandom(double *array, int size)
{
  for (int i = 0; i < size; i++)
  {
    array[i] = (rand() % 21 - 10) / 10.0;
  }
}

void fourthTask(const char *dir, int size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size of array must be more than '0'.");
  }
  auto order = getDirection<double>(dir);

  std::vector<double> vector(static_cast<unsigned int>(size));
  fillRandom(&vector[0], size);
  print(vector);

  sort<bracketAccess>(vector, order);

  print(vector);
}
