#ifndef B11_TASKS_HPP
#define B11_TASKS_HPP
#include <iostream>

void firstTask(const char *direction);
void secondTask(const char *filename);
void thirdTask();
void fourthTask(const char *direction, int size);

#endif //B11_TASKS_HPP
