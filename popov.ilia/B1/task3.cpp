#include <vector>
#include <iterator>

#include "functions.hpp"
#include "tasks.hpp"


void thirdTask()
{
  std::vector<int> inputVector;

  int value = 0;
  while (std::cin && !(std::cin >> value).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input is incorrect");
    }

    if (value == 0)
    {
      break;
    }
    inputVector.push_back(value);
  }

  if (inputVector.empty())
  {
    return;
  }

  if (std::cin.eof())
  {
    throw std::invalid_argument("Expected '0' at the end of file");
  }

  auto iter = inputVector.begin();
  if (inputVector.back() == 1)
  {
    while (iter != inputVector.end())
    {
      iter = ((*iter) % 2 == 0) ? inputVector.erase(iter) : ++iter;
    }
  }

  if (inputVector.back() == 2)
  {
    while (iter != inputVector.end())
    {
      iter = ((*iter) % 3 == 0) ? (inputVector.insert(++iter, 3, 1) + 3) : ++iter;
    }
  }

  print(inputVector);
}
