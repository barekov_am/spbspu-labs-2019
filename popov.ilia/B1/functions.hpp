#ifndef B11_FUNCTIONS_HPP
#define B11_FUNCTIONS_HPP

#include <iostream>
#include <functional>
#include <cstring>
#include "accesStrategy.hpp"

template <typename T>
std::function<bool(T, T)> getDirection(const char *dir)
{
  if (strcmp(dir, "ascending") == 0)
  {
    return [](T a, T b) { return a < b; };
  }
  if (strcmp(dir, "descending") == 0)
  {
    return [](T a, T b) { return a > b; };
  }
  throw std::invalid_argument("Sorting direction is incorrect.");
}

template <template <class Container> class Access, typename Container>
void sort(Container &array, std::function <bool(typename Container::value_type, typename Container::value_type)> comparator)
{
  using access = Access<Container>;
  for (auto i = access::begin(array); i != access::end(array); ++i)
  {
    for (auto j = access::next(i); j != access::end(array); ++j)
    {
      if (comparator(access::get(array, j), access::get(array, i)))
      {
        std::swap(access::get(array, j), access::get(array, i));
      }
    }
  }
}

template <typename Container>
void print(const Container &array)
{
  if (array.empty())
  {
    return;
  }

  for (const auto &elem: array)
  {
    std::cout << elem << " ";
  }
  std::cout << "\n";
}

#endif //B11_FUNCTIONS_HPP
