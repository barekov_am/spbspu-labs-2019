#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"


int main()
{
  popov::Rectangle rectangle({1.0, 2.0}, 3.0, 4.0);

  rectangle.move(1.5, 1.5);
  rectangle.printInformation();

  popov::Circle circle({1.0, 2.0}, 3.0);
  popov::Shape *shape = &circle;

  shape->move({2, -2});
  shape->scale(2);
  shape->printInformation();

  std::cout << "\n";
  popov::Triangle triangle({-5.0, 0.0}, {2.0, 7.0}, {2.0, -7.0});
  shape = &triangle;
  shape->scale(2);
  shape->printInformation();

  std::cout << "\n";
  const int sides = 5;
  const popov::point_t points[sides] = {{-3, 0}, {-1, 3}, {1, 3}, {3, 0}, {0, -3}};
  popov::Polygon polygon1(sides, points);
  shape = &polygon1;

  shape->printInformation();
  shape->move({2, 2});
  shape->printInformation();
  shape->scale(2);
  shape->printInformation();

//демонстрация конструкторов копирования и перемещения
  popov::Polygon polygon2(polygon1);
  polygon2.printInformation();
  polygon2.move(3.5, 4.5);

  popov::Polygon polygon3(std::move(polygon2));
  polygon3.printInformation();

//демонстрация операторов копирования и перемещения
  polygon2 = polygon3;
  polygon2.printInformation();

  polygon3 = (std::move(polygon2));
  polygon3.printInformation();

  return 0;
}
