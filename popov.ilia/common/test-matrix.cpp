#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "matrix.hpp"

BOOST_AUTO_TEST_SUITE(matricTestSuite)

  BOOST_AUTO_TEST_CASE(matrixCopyConstructor)
  {
    popov::Matrix<int> testMatrix;
    for (int i = 0; i < 10; ++i)
    {
      testMatrix.add(i, std::make_shared<int>(i));
    }

    popov::Matrix<int> newMatrix(testMatrix);

    BOOST_CHECK(testMatrix == newMatrix);
  }

  BOOST_AUTO_TEST_CASE(matrixMoveConstructor)
  {
    popov::Matrix<int> testMatrix;
    for (int i = 0; i < 10; ++i)
    {
      testMatrix.add(i, std::make_shared<int>(i));
    }
    popov::Matrix<int> initialCopy(testMatrix);

    popov::Matrix<int> newMatrix(std::move(testMatrix));

    BOOST_CHECK(initialCopy == newMatrix);
    BOOST_CHECK(popov::Matrix<int>() == testMatrix);
  }

  BOOST_AUTO_TEST_CASE(matrixCopyOperator)
  {
    popov::Matrix<int> testMatrix;
    for (int i = 0; i < 10; ++i)
    {
      testMatrix.add(i, std::make_shared<int>(i));
    }

    popov::Matrix<int> newMatrix = testMatrix;

    BOOST_CHECK(testMatrix == newMatrix);
  }

  BOOST_AUTO_TEST_CASE(matrixMoveOperator)
  {
    popov::Matrix<int> testMatrix;
    for (int i = 0; i < 10; ++i)
    {
      testMatrix.add(i, std::make_shared<int>(i));
    }
    popov::Matrix<int> initialCopy(testMatrix);

    popov::Matrix<int> newMatrix = std::move(testMatrix);

    BOOST_CHECK(initialCopy == newMatrix);
    BOOST_CHECK(popov::Matrix<int>() == testMatrix);
  }

  BOOST_AUTO_TEST_CASE(accesingOutOfRangeHandling)
  {
    popov::Matrix<int> testMatrix;
    testMatrix.add(0, std::make_shared<int>(5));
    testMatrix.add(0, std::make_shared<int>(6));

    BOOST_CHECK_THROW(testMatrix[-3][0], std::out_of_range);
    BOOST_CHECK_THROW(testMatrix[7][0], std::out_of_range);
    BOOST_CHECK_THROW(testMatrix[0][5], std::out_of_range);
    BOOST_CHECK_THROW(testMatrix[0][-2], std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(addingNullptrTest)
  {
    popov::Matrix<int> testMatrix;

    BOOST_CHECK_THROW(testMatrix.add(0, nullptr), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()


