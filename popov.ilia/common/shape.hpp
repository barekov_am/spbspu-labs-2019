#ifndef A3_SHAPE_HPP
#define A3_SHAPE_HPP
#include "base-types.hpp"
#include <memory>

namespace popov
{
  class Shape
  {
  public:
    using ptr = std::shared_ptr<Shape>;

    using array = std::unique_ptr<ptr[]>;

    virtual ~Shape() = default;

    virtual double getArea() const = 0;

    virtual rectangle_t getFrameRect() const = 0;

    virtual void move(double moveX, double moveY) = 0;

    virtual void move(const point_t &newPoint) = 0;

    virtual void printInformation() const = 0;

    virtual void scale(double coefficient) = 0;

    virtual void rotate(double) = 0;
  };
}

#endif
