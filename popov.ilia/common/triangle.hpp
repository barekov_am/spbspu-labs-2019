#ifndef A3_TRIANGLE_HPP
#define A3_TRIANGLE_HPP
#include "shape.hpp"

namespace popov
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &a, const point_t &b, const point_t &c);

    point_t getCenter() const;

    rectangle_t getFrameRect() const override;

    double getArea() const override;

    void move(double moveX, double moveY) override;

    void move(const point_t &newPoint) override;

    void scale(double coefficient) override;

    void printInformation() const override;

    void rotate(double angle) override;
  private:
    point_t a_;
    point_t b_;
    point_t c_;
  };
}

#endif
