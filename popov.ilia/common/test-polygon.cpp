#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>

#include "polygon.hpp"

BOOST_AUTO_TEST_SUITE(polygonTestSuite)

const double ACCURACY = 0.001;



BOOST_AUTO_TEST_CASE(changesAfterMovingPolygon)
{
  popov::point_t points[]{{-3, 0}, {-1, 3}, {1, 3}, {3, 0}, {0, -3}};

  popov::Polygon polygon(5, points);

  const double areaBeforeMove = polygon.getArea();
  const popov::rectangle_t rectangleBeforeMove = polygon.getFrameRect();

  polygon.move(1.0, 1.0);
  double areaAfterMove = polygon.getArea();
  popov::rectangle_t rectangleAfterMove = polygon.getFrameRect();

  BOOST_CHECK_CLOSE(rectangleBeforeMove.width, rectangleAfterMove.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeMove.height, rectangleAfterMove.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ACCURACY);

  polygon.move({1.0, 1.0});
  areaAfterMove = polygon.getArea();
  rectangleAfterMove = polygon.getFrameRect();

  BOOST_CHECK_CLOSE(rectangleBeforeMove.width, rectangleAfterMove.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeMove.height, rectangleAfterMove.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ACCURACY);
}

BOOST_AUTO_TEST_CASE(checkPolygonAfterScale)
{
  popov::point_t points[]{{-3, 0}, {-1, 3}, {1, 3}, {3, 0}, {0, -3}};
  popov::Polygon polygon(5, points);

  const double areaBeforeScale = polygon.getArea();
  const popov::rectangle_t rectangleBeforeScale = polygon.getFrameRect();

  double scale = 2.0;
  polygon.scale(scale);
  popov::rectangle_t rectangleAfterScale = polygon.getFrameRect();
  double areaAfterScale = polygon.getArea();

  BOOST_CHECK_CLOSE(rectangleBeforeScale.width * scale, rectangleAfterScale.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeScale.height * scale, rectangleAfterScale.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeScale * scale * scale, areaAfterScale, ACCURACY);

  scale = 0.5;
  polygon.scale(scale);
  rectangleAfterScale = polygon.getFrameRect();
  areaAfterScale = polygon.getArea();

  BOOST_CHECK_CLOSE(rectangleBeforeScale.width, rectangleAfterScale.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeScale.height, rectangleAfterScale.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeScale, areaAfterScale, ACCURACY);
}

BOOST_AUTO_TEST_CASE(incorrectParametrsPolygon)
{
  const popov::point_t points1[]{{-3, 0}, {-1, 3}};
  const int sides1 = 2;
  BOOST_CHECK_THROW(popov::Polygon({sides1, points1}), std::invalid_argument);

  const popov::point_t points2[]{{-3, 0}, {-1, 3}, {1, 3}, {3, 0}, {0, -3}};
  const int sides2 = 5;
  popov::Polygon polygon({sides2, points2});

  BOOST_CHECK_THROW(polygon.scale(0.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestPolygonCopyConstructor)
{
  const popov::point_t points[]{{-3, 0}, {-1, 3}, {1, 3}, {3, 0}, {0, -3}};
  popov::Polygon polygon1(5, points);
  popov::Polygon polygon2 = polygon1;

  const popov::rectangle_t rectangle1 = polygon1.getFrameRect();
  const popov::rectangle_t rectangle2 = polygon2.getFrameRect();

  BOOST_CHECK_CLOSE(rectangle1.width, rectangle2.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle1.height, rectangle2.height, ACCURACY);
  BOOST_CHECK_CLOSE(polygon1.getArea(), polygon2.getArea(), ACCURACY);

}

BOOST_AUTO_TEST_CASE(TestPolygonCopyAssignmentOperator)
{
  const popov::point_t points1[]{{-3, 0}, {-1, 3}, {1, 3}, {3, 0}, {0, -3}};
  const popov::point_t points2[]{{-3, 0}, {-1, 3}, {1, 3}};
  popov::Polygon polygon1(5, points1);
  popov::Polygon polygon2(3, points2);
  polygon2 = polygon1;

  const popov::rectangle_t rectangle1 = polygon1.getFrameRect();
  const popov::rectangle_t rectangle2 = polygon2.getFrameRect();

  BOOST_CHECK_CLOSE(rectangle1.width, rectangle2.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle1.height, rectangle2.height, ACCURACY);
  BOOST_CHECK_CLOSE(polygon1.getArea(), polygon2.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(TestPolygonMoveConstructor)
{
  const popov::point_t points[]{{-3, 0}, {-1, 3}, {1, 3}, {3, 0}, {0, -3}};
  popov::Polygon polygon1(5, points);
  const double areaBeforeMove = polygon1.getArea();
  const popov::rectangle_t rectangle1 = polygon1.getFrameRect();
  popov::Polygon polygon2 = std::move(polygon1);

  const popov::rectangle_t rectangle2 = polygon2.getFrameRect();
  const double areaAfterMove = polygon2.getArea();

  BOOST_CHECK_CLOSE(rectangle1.width, rectangle2.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle1.height, rectangle2.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ACCURACY);
}

BOOST_AUTO_TEST_CASE(TestPolygonMoveAssignmentOperator)
{
  const popov::point_t points1[]{{-3, 0}, {-1, 3}, {1, 3}, {3, 0}, {0, -3}};
  const popov::point_t points2[]{{-3, 0}, {-1, 3}, {1, 3}};

  popov::Polygon polygon1(5, points1);
  popov::Polygon polygon2(3, points2);
  const double areaBeforeMove = polygon1.getArea();
  const popov::rectangle_t rectangle1 = polygon1.getFrameRect();

  polygon2 = std::move(polygon1);
  const popov::rectangle_t rectangle2 = polygon2.getFrameRect();
  const double areaAfterMove = polygon2.getArea();

  BOOST_CHECK_CLOSE(rectangle1.width, rectangle2.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle1.height, rectangle2.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testPolygonRotation)
{
  const size_t quantity = 6;
  popov::point_t vertices[quantity] = { { -2, -2 }, { -1, 4 }, { 3, 8 }, { 6, 8 }, { 8, 7 }, { 3, 0 } };
  popov::Polygon testPolygon(quantity, vertices);
  const double areaBefore = testPolygon.getArea();
  const popov::rectangle_t frameRectBefore = testPolygon.getFrameRect();

  double angle = 90;
  testPolygon.rotate(angle);

  double areaAfter = testPolygon.getArea();

  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ACCURACY);

  angle = 270;
  testPolygon.rotate(angle);
  popov::rectangle_t frameRectAfter = testPolygon.getFrameRect();
  areaAfter = testPolygon.getArea();

  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ACCURACY);
}



BOOST_AUTO_TEST_SUITE_END()
