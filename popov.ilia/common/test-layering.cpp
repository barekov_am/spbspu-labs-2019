#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "layering.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(layeringTestSuite)

  BOOST_AUTO_TEST_CASE(emptyCompositeLayering)
  {
    popov::Matrix<popov::Shape> testMatrix = popov::layer(popov::CompositeShape());

    BOOST_CHECK_EQUAL(testMatrix.getRows(), 0);
  }

  BOOST_AUTO_TEST_CASE(correctCompositeLayering)
  {
    popov::CompositeShape testComposite;
    testComposite.add(std::make_shared<popov::Rectangle>(popov::point_t{0, 0}, 6, 4));
    testComposite.add(std::make_shared<popov::Circle>(popov::point_t{4, 3}, 3));
    testComposite.add(std::make_shared<popov::Rectangle>(popov::point_t{-5, 4}, 2, 1));

    popov::Matrix<popov::Shape> testMatrix = popov::layer(testComposite);

    BOOST_CHECK_EQUAL(testMatrix.getRows(), 2);
    BOOST_CHECK_EQUAL(testMatrix[0].size(), 2);
    BOOST_CHECK_EQUAL(testMatrix[1].size(), 1);
    BOOST_CHECK_EQUAL(testMatrix[0][0], testComposite[0]);
    BOOST_CHECK_EQUAL(testMatrix[0][1], testComposite[2]);
    BOOST_CHECK_EQUAL(testMatrix[1][0], testComposite[1]);
  }

  BOOST_AUTO_TEST_CASE(intercetionTseting)
  {
    popov::Rectangle rectangle({5, 5}, 6, 4);
    popov::Circle circle1({3, 2}, 2);
    popov::Circle circle2({-4, -3}, 2);

    BOOST_CHECK(popov::intersect(rectangle, circle1));
    BOOST_CHECK(!popov::intersect(rectangle, circle2));

  }


BOOST_AUTO_TEST_SUITE_END()
