#ifndef A3_COMPOSITE_SHAPE_HPP
#define A3_COMPOSITE_SHAPE_HPP
#include "shape.hpp"
#include <cstddef>

namespace popov
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &other);
    CompositeShape(CompositeShape &&other) noexcept;
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape &other);
    CompositeShape& operator =(CompositeShape &&other) noexcept;

    Shape::ptr operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &newPoint) override;
    void move(double moveX, double moveY) override;
    void scale(double coefficient) override;
    void printInformation() const override;
    void rotate(double angle) override;

    void add(Shape::ptr shape);
    void remove(size_t index);
    size_t getSize() const;

  private:
    size_t count_;
    Shape::array shapes_;
  };

}

#endif //A3_COMPOSITE_SHAPE_HPP
