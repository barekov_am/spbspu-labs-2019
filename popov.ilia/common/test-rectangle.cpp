#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>

#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(rectangleTestSuite)

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_CASE(changesAfterMovingRectangle)
{
  popov::Rectangle rectangle({2.0, 2.0}, 2.0, 2.0);

  const double areaBeforeMove = rectangle.getArea();
  const popov::rectangle_t rectangleBeforeMove = rectangle.getFrameRect();

  rectangle.move(1.0, 1.0);
  double areaAfterMove = rectangle.getArea();
  popov::rectangle_t rectangleAfterMove = rectangle.getFrameRect();

  BOOST_CHECK_CLOSE(rectangleBeforeMove.width, rectangleAfterMove.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeMove.height, rectangleAfterMove.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ACCURACY);

  rectangle.move({1.0, 1.0});
  areaAfterMove = rectangle.getArea();
  rectangleAfterMove = rectangle.getFrameRect();

  BOOST_CHECK_CLOSE(rectangleBeforeMove.width, rectangleAfterMove.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeMove.height, rectangleAfterMove.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ACCURACY);
}

BOOST_AUTO_TEST_CASE(checkRectangleAfterScale)
{
  popov::Rectangle rectangle({2.0, 2.0}, 2.0, 2.0);
  const double areaBeforeScale = rectangle.getArea();
  const popov::rectangle_t rectangleBeforeScale = rectangle.getFrameRect();

  double scale = 2.0;
  rectangle.scale(scale);
  double areaAfterScale = rectangle.getArea();
  popov::rectangle_t rectangleAfterScale = rectangle.getFrameRect();

  BOOST_CHECK_CLOSE(rectangleBeforeScale.width * scale, rectangleAfterScale.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeScale.height * scale, rectangleAfterScale.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeScale * scale * scale, areaAfterScale, ACCURACY);

  scale = 0.5;
  rectangle.scale(scale);
  rectangleAfterScale = rectangle.getFrameRect();
  areaAfterScale = rectangle.getArea();


  BOOST_CHECK_CLOSE(rectangleBeforeScale.width, rectangleAfterScale.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeScale.height, rectangleAfterScale.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeScale, areaAfterScale, ACCURACY);
}

BOOST_AUTO_TEST_CASE(incorrectParametrsRectangle)
{
  BOOST_CHECK_THROW(popov::Rectangle({2.0, 2.0}, -2.0, -2.0), std::invalid_argument);

  popov::Rectangle rectangle({2.0, 2.0}, 2.0, 2.0);
  BOOST_CHECK_THROW(rectangle.scale(0.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(immutabilityRectangleAfterRotation)
{
  popov::Rectangle testRectangle({5.0, 5.0}, 4.0, 4.0);
  const double areaBefore = testRectangle.getArea();
  const popov::rectangle_t frameRectBefore = testRectangle.getFrameRect();
  double angle = 90.0;
  testRectangle.rotate(angle);
  double areaAfter = testRectangle.getArea();
  popov::rectangle_t frameRectAfter = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);

  angle = 180;
  testRectangle.rotate(angle);
  areaAfter = testRectangle.getArea();
  frameRectAfter = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);

  angle = 360;
  testRectangle.rotate(angle);
  areaAfter = testRectangle.getArea();
  frameRectAfter = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
}



BOOST_AUTO_TEST_SUITE_END()
