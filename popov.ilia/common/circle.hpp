#ifndef A3_CIRCLE_HPP
#define A3_CIRCLE_HPP
#include "shape.hpp"

namespace popov
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t &center, double rad);

    rectangle_t getFrameRect() const override;

    double getArea() const override;

    void move(double moveX, double moveY) override;

    void move(const point_t &newPoint) override;

    void printInformation() const override;

    void scale(double coefficient) override;

    void rotate(double) override;

  private:
    point_t center_;
    double radius_;
  };
}

#endif
