#include "rectangle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>

popov::Rectangle::Rectangle(const point_t &center, double height, double width):
  center_(center),
  height_(height),
  width_(width),
  angle_(0)
{
  if ((height_ <= 0) || (width_ <= 0))
  {
    throw std::invalid_argument("Parameters must be more than 0");
  }
}

double popov::Rectangle::getArea() const
{
  return height_ * width_;
}

popov::rectangle_t popov::Rectangle::getFrameRect() const
{
  const double sinAngle = std::sin(angle_ * M_PI / 180);
  const double cosAngle = std::cos(angle_ * M_PI / 180);
  const double width = std::abs(width_ * cosAngle) + std::abs(height_ * sinAngle);
  const double height = std::abs(width_ * sinAngle) + std::abs(height_ * cosAngle);

  return {width, height, center_};

}

void popov::Rectangle::move(const double moveX, const double moveY)
{
  center_.x += moveX;
  center_.y += moveY;
}

void popov::Rectangle::move(const point_t &newPoint)
{
  center_ = newPoint;
}

void popov::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient <= 0");
  }

  height_ *= coefficient;
  width_ *= coefficient;
}

void popov::Rectangle::printInformation() const
{
  std::cout << "Information about rectangle: " << '\n';

  rectangle_t boundingRect = getFrameRect();
  std::cout << "Bounding rectangle: Height: " << boundingRect.height << " Width: " << boundingRect.width << '\n';
  std::cout << "Area: " << getArea();
  std::cout << " Center position: x: " << center_.x << " y: " << center_.y << '\n';
}

void popov::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
