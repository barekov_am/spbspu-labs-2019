#ifndef A3_MATRIX_HPP
#define A3_MATRIX_HPP

#include <memory>
#include <stdexcept>

namespace popov
{
  template<typename T>
  class Matrix
  {
  public:
    using ptr = std::shared_ptr<T>;
    using array = std::unique_ptr<ptr[]>;

    class Layer
    {
    public:
      Layer(const Layer&);
      Layer(Layer&&) noexcept;
      Layer(ptr*, size_t);
      ~Layer() = default;

      Layer& operator=(const Layer&);
      Layer& operator=(Layer&&) noexcept;
      ptr operator[](size_t) const;

      size_t size() const;

    private:
      size_t size_;
      array data_;
    };

    Matrix();
    Matrix(const Matrix&);
    Matrix(Matrix&&) noexcept;
    ~Matrix() = default;

    Matrix& operator=(const Matrix&);
    Matrix& operator=(Matrix&&) noexcept;
    Layer operator[](size_t) const;
    bool operator==(const Matrix&) const;
    bool operator!=(const Matrix&) const;

    void add(size_t, ptr);
    size_t getRows() const;
    void swap(Matrix&) noexcept;

  private:
    size_t rows_;
    size_t size_;
    std::unique_ptr<size_t[]> rowSize_;
    array data_;
  };

  template<typename T>
  Matrix<T>::Matrix() :
    rows_(0),
    size_(0)
  {
  }

  template<typename T>
  Matrix<T>::Matrix(const Matrix<T>& rhs) :
    rows_(rhs.rows_),
    size_(rhs.size_),
    rowSize_(std::make_unique<size_t[]>(rhs.rows_)),
    data_(std::make_unique<ptr[]>(rhs.size_))
  {
    for (size_t i = 0; i < rows_; ++i)
    {
      rowSize_[i] = rhs.rowSize_[i];
    }

    for (size_t i = 0; i < size_; ++i)
    {
      data_[i] = rhs.data_[i];
    }
  }

  template<typename T>
  Matrix<T>::Matrix(Matrix<T>&& rhs) noexcept :
    rows_(rhs.rows_),
    size_(rhs.size_),
    rowSize_(std::move(rhs.rowSize_)),
    data_(std::move(rhs.data_))
  {
    rhs.rows_ = 0;
    rhs.size_ = 0;
  }

  template<typename T>
  Matrix<T>& Matrix<T>::operator=(const Matrix<T>& rhs)
  {
    if (this != &rhs)
    {
      swap(Matrix<T>(rhs));
    }

    return *this;
  }

  template<typename T>
  Matrix<T>& Matrix<T>::operator=(Matrix<T>&& rhs) noexcept
  {
    if (this != &rhs)
    {
      rows_ = rhs.rows_;
      rhs.rows_ = 0;

      size_ = rhs.size_;
      rhs.size_ = 0;

      rowSize_ = std::move(rhs.rowSize_);

      data_ = std::move(rhs.data_);
    }

    return *this;
  }

  template<typename T>
  typename Matrix<T>::Layer Matrix<T>::operator[](size_t row) const
  {
    if (row >= rows_)
    {
      throw std::out_of_range("Row index out of range.");
    }

    size_t rowIndex = 0;
    for (size_t i = 0; i < row; ++i)
    {
      rowIndex += rowSize_[i];
    }

    return Layer(&data_[rowIndex], rowSize_[row]);
  }

  template<typename T>
  bool Matrix<T>::operator==(const Matrix<T>& rhs) const
  {
    if ((rows_ != rhs.rows_) || (size_ != rhs.size_))
    {
      return false;
    }

    for (size_t i = 0; i < rows_; ++i)
    {
      if (rowSize_[i] != rhs.rowSize_[i])
      {
        return false;
      }
    }

    for (size_t i = 0; i < size_; ++i)
    {
      if (data_[i] != rhs.data_[i])
      {
        return false;
      }
    }

    return true;
  }

  template<typename T>
  bool Matrix<T>::operator!=(const Matrix<T>& rhs) const
  {
    return !(*this == rhs);
  }

  template<typename T>
  void Matrix<T>::add(size_t row, Matrix<T>::ptr newObj)
  {
    if (newObj == nullptr)
    {
      throw std::invalid_argument("Can't add nullptr.");
    }

    if (row > rows_)
    {
      throw std::out_of_range("Row index out of range.");
    }

    // Adding new row and element to it
    if (row == rows_)
    {
      std::unique_ptr<size_t[]> tmpRowSize = std::make_unique<size_t[]>(rows_ + 1);
      array tmpData = std::make_unique<ptr[]>(size_ + 1);

      for (size_t i = 0; i < rows_; ++i)
      {
        tmpRowSize[i] = rowSize_[i];
      }
      tmpRowSize[rows_++] = 1;
      rowSize_ = std::move(tmpRowSize);

      for (size_t i = 0; i < size_; ++i)
      {
        tmpData[i] = data_[i];
      }
      tmpData[size_++] = newObj;
      data_ = std::move(tmpData);
    }
    else
    {
      array tmpData = std::make_unique<ptr[]>(size_ + 1);

      size_t currNewInd = 0;
      size_t currOldInd = 0;
      for (size_t i = 0; i < rows_; ++i)
      {
        for (size_t j = 0; j < rowSize_[i]; ++j)
        {
          tmpData[currNewInd++] = data_[currOldInd++];
        }
        if (i == row)
        {
          tmpData[currNewInd++] = newObj;
        }
      }

      data_ = std::move(tmpData);
      ++rowSize_[row];
      ++size_;
    }
  }

  template<typename T>
  size_t Matrix<T>::getRows() const
  {
    return rows_;
  }

  template<typename T>
  void Matrix<T>::swap(Matrix<T>& rhs) noexcept
  {
    std::swap(rows_, rhs.rows_);
    std::swap(size_, rhs.size_);
    std::swap(rowSize_, rhs.rowSize_);
    std::swap(data_, rhs.data_);
  }

  template<typename T>
  Matrix<T>::Layer::Layer(const Matrix<T>::Layer& rhs) :
    size_(rhs.size_),
    data_(std::make_unique<ptr[]>(rhs.size_))
  {
    for (size_t i = 0; i < size_; ++i)
    {
      data_[i] = rhs.data_[i];
    }
  }

  template<typename T>
  Matrix<T>::Layer::Layer(Matrix<T>::Layer&& rhs) noexcept :
    size_(rhs.size_),
    data_(std::move(rhs.data_))
  {
    rhs.size_ = 0;
  }

  template<typename T>
  Matrix<T>::Layer::Layer(Matrix<T>::ptr* array, size_t size) :
    size_(size),
    data_(std::make_unique<ptr[]>(size))
  {
    for (size_t i = 0; i < size_; ++i)
    {
      data_[i] = array[i];
    }
  }

  template<typename T>
  typename Matrix<T>::Layer& Matrix<T>::Layer::operator=(const Matrix<T>::Layer& rhs)
  {
    if (this != &rhs)
    {
      data_ = std::make_unique<ptr[]>(rhs.size_);
      for (size_t i = 0; i < rhs.size_; ++i)
      {
        data_[i] = rhs.data_[i];
      }

      size_ = rhs.size_;
    }

    return *this;
  }

  template<typename T>
  typename Matrix<T>::Layer& Matrix<T>::Layer::operator=(Matrix<T>::Layer&& rhs) noexcept
  {
    if (this != &rhs)
    {
      size_ = rhs.size_;
      rhs.size_ = 0;

      data_ = std::move(rhs.size_);
    }

    return *this;
  }

  template<typename T>
  typename Matrix<T>::ptr Matrix<T>::Layer::operator[](size_t ind) const
  {
    if (ind >= size_)
    {
      throw std::out_of_range("Column index out of range.");
    }

    return data_[ind];
  }

  template<typename T>
  size_t Matrix<T>::Layer::size() const
  {
    return size_;
  }
}

#endif //A3_MATRIX_HPP
