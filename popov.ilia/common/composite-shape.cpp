#include "composite-shape.hpp"

#include <iostream>
#include <stdexcept>
#include <cstddef>
#include <cmath>


popov::CompositeShape::CompositeShape():
  count_(0),
  shapes_(nullptr)
{
}

popov::CompositeShape::CompositeShape(const CompositeShape &other):
  count_(other.count_),
  shapes_(std::make_unique<popov::Shape::ptr[]>(other.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i] = other.shapes_[i];
  }
}

popov::CompositeShape::CompositeShape(CompositeShape &&other) noexcept:
  count_(other.count_),
  shapes_(std::move(other.shapes_))
{
  other.count_ = 0;
}

popov::CompositeShape& popov::CompositeShape::operator =(const CompositeShape &other)
{
  if (this != &other)
  {
    count_ = other.count_;
    shapes_ = std::make_unique<popov::Shape::ptr[]>(count_);
    for (size_t i = 0; i < count_; i++)
    {
      shapes_[i] = other.shapes_[i];
    }
  }
  return *this;
}

popov::CompositeShape& popov::CompositeShape::operator =(popov::CompositeShape &&other) noexcept
{
  if (this != &other)
  {
    count_ = other.count_;
    other.count_ = 0;
    shapes_ = std::move(other.shapes_);
  }
  return *this;
}

popov::Shape::ptr popov::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }
  return shapes_[index];
}


double popov::CompositeShape::getArea() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  double area = 0;

  for (size_t i = 0; i < count_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

popov::rectangle_t popov::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  rectangle_t tempRectangle = shapes_[0]->getFrameRect();
  double minX = tempRectangle.pos.x - tempRectangle.width / 2;
  double maxX = tempRectangle.pos.x + tempRectangle.width / 2;
  double minY = tempRectangle.pos.y - tempRectangle.height / 2;
  double maxY = tempRectangle.pos.y + tempRectangle.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    tempRectangle = shapes_[i]->getFrameRect();
    double frameRectangle = tempRectangle.pos.x - tempRectangle.width / 2;
    minX = std::min(frameRectangle, minX);
    frameRectangle = tempRectangle.pos.y - tempRectangle.height / 2;
    minY = std::min(frameRectangle, minY);
    frameRectangle = tempRectangle.pos.x + tempRectangle.width / 2;
    maxX = std::max(frameRectangle, maxX);
    frameRectangle = tempRectangle.pos.y + tempRectangle.height / 2;
    maxY = std::max(frameRectangle, maxY);
  }
  return {(maxX - minX), (maxY - minY), {(minX + maxX) / 2, (minY + maxY) / 2}};
}

void popov::CompositeShape::move(double moveX, double moveY)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->move(moveX, moveY);
  }
}

void popov::CompositeShape::move(const point_t &newPoint)
{
  double moveX = newPoint.x - getFrameRect().pos.x;
  double moveY = newPoint.y - getFrameRect().pos.y;
  move(moveX, moveY);
}

void popov::CompositeShape::scale(double coefficient)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  if (coefficient <= 0)
  {
    throw std::invalid_argument("coefficient must be > 0");
  }

  rectangle_t frameRect = getFrameRect();
  for (size_t i = 0; i < count_; i++)
  {
    double moveX = shapes_[i]->getFrameRect().pos.x - frameRect.pos.x;
    double moveY = shapes_[i]->getFrameRect().pos.y - frameRect.pos.y;
    shapes_[i]->move({frameRect.pos.x + moveX * coefficient, frameRect.pos.y + moveY * coefficient});
    shapes_[i]->scale(coefficient);
  }
}

void popov::CompositeShape::printInformation() const
{
  for (size_t i = 0; i < count_; i++)
  {
    std::cout << "Shape number: " << i+1 << "\n";
    shapes_[i]->printInformation();
  }
}

void popov::CompositeShape::rotate(double angle)
{

  const popov::point_t framePos = getFrameRect().pos;
  const double sinAngle = std::sin(angle * M_PI / 180);
  const double cosAngle = std::cos(angle * M_PI / 180);
  for (size_t i = 0; i < count_; i++)
  {
    const popov::point_t shapePos = shapes_[i]->getFrameRect().pos;
    const double moveX = shapePos.x - framePos.x;
    const double moveY = shapePos.y - shapePos.y;
    shapes_[i]->move(moveX * (cosAngle - 1) - moveY * sinAngle, moveX * sinAngle + moveY * (cosAngle - 1));
    shapes_[i]->rotate(angle);
  }
}


void popov::CompositeShape::add(popov::Shape::ptr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Can't add nullptr shape");
  }

  popov::Shape::array currShapes(std::make_unique<std::shared_ptr<popov::Shape>[]>(count_ + 1));
  for (size_t i = 0; i < count_; i++)
  {
    currShapes[i] = shapes_[i];
  }
  currShapes[count_] = shape;
  count_++;
  shapes_ = std::move(currShapes);
}

void popov::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }
  if (count_ == 0)
  {
    throw std::out_of_range("Composite shape is already empty");
  }

  for (size_t i = index; i < count_ - 1; ++i)
  {
    shapes_[i] = shapes_[i + 1];
  }

  shapes_[count_ - 1] = nullptr;
  count_--;
}

size_t popov::CompositeShape::getSize() const
{
  return count_;
}

