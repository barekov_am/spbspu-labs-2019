#ifndef A3_LAYERING_HPP
#define A3_LAYERING_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace popov
{
  bool intersect(const Shape&, const Shape&);
  Matrix<Shape> layer(const CompositeShape&);
}

#endif //A3_LAYERING_HPP
