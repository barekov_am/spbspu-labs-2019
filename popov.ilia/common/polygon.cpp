#include "polygon.hpp"
#include <iostream>
#include <cmath>


popov::Polygon::Polygon(const Polygon &other):
  sides_(other.sides_),
  points_(std::make_unique<point_t[]>(other.sides_))
{
  for (size_t i = 0; i < sides_; i++)
  {
    points_[i] = other.points_[i];
  }
}

popov::Polygon::Polygon(Polygon &&other) noexcept:
  sides_(other.sides_),
  points_(std::move(other.points_))
{
  other.sides_ = 0;
}

popov::Polygon::Polygon(size_t sides, const point_t *points):
  sides_(sides),
  points_(std::make_unique<point_t[]>(sides))
{
  if (sides_ < 3)
  {
    throw std::invalid_argument("Number of sides must be more than 2");
  }

  if (points == nullptr)
  {
    throw std::invalid_argument("Pointer can't be nullptr");
  }

  for (size_t i = 0; i < sides_; i++)
  {
    points_[i] = points[i];
  }

  if (!checkIfConvex())
  {
    throw std::invalid_argument("Polygon must be convex");
  }
}

popov::Polygon &popov::Polygon::operator=(const Polygon &other)
{
  if (this != &other)
  {
    sides_ = other.sides_;
    points_ = (std::make_unique<point_t[]>(other.sides_));
    for (size_t i = 0; i < sides_; i++)
    {
      points_[i] = other.points_[i];
    }
  }

  return  *this;
}

popov::Polygon &popov::Polygon::operator=(Polygon &&other) noexcept
{
  if (this != &other)
  {
    sides_ = other.sides_;
    other.sides_ = 0;
    points_ = std::move(other.points_);
  }

  return *this;
}

popov::point_t popov::Polygon::getCenter() const
{
  point_t centerTemp = {0.0, 0.0};

  for (size_t i = 0; i < sides_; i++)
  {
    centerTemp.x += points_[i].x;
    centerTemp.y += points_[i].y;
  }
  return {centerTemp.x / sides_, centerTemp.y / sides_};
}

double popov::Polygon::getArea() const
{
  double sum = 0.0;

  for (size_t i = 0; i < sides_ - 1; i++)
  {
    sum += points_[i].x * points_[i + 1].y;
    sum -= points_[i + 1].x * points_[i].y;
  }    

  sum += points_[sides_ - 1].x * points_[0].y;
  sum -= points_[0].x * points_[sides_ - 1].y;

  return fabs(sum) / 2;
}

popov::rectangle_t popov::Polygon::getFrameRect() const
{
  double maxX = points_[0].x;
  double minX = points_[0].x;
  double maxY = points_[0].y;
  double minY = points_[0].y;

  for (size_t i = 1; i < sides_; i++)
  {
    maxX = std::max(maxX, points_[i].x);
    minX = std::min(minX, points_[i].x);
    maxY = std::max(maxY, points_[i].y);
    minY = std::min(minY, points_[i].y);
  }

  return {maxX - minX, maxY - minY, {minX + (maxX - minX) / 2, minY + (maxY - minY) / 2}};
}

void popov::Polygon::move(double moveX, double moveY)
{
  for (size_t i = 0; i < sides_; i++)
  {
    points_[i].x += moveX;
    points_[i].y += moveY;
  }
}

void popov::Polygon::move(const point_t &newPoint)
{
  const point_t center_ = getCenter();
  move(newPoint.x - center_.x, newPoint.y - center_.y);
}

void popov::Polygon::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient <= 0");
  }

  const point_t center_ = getCenter();

  for (size_t i = 0; i < sides_; i++)
  {
    points_[i].x = center_.x + coefficient * (points_[i].x - center_.x);
    points_[i].y = center_.y + coefficient * (points_[i].y - center_.y);
  }
}

void popov::Polygon::printInformation() const
{
  std::cout << "Information about polygon: " << '\n';
  rectangle_t boundingRect = getFrameRect();
  std::cout << "Bounding rectangle: Height: " << boundingRect.height << " Width: " << boundingRect.width << '\n';
  std::cout << "Area: " << getArea();
  const point_t center_ = getCenter();
  std::cout << " Center position: x: " << center_.x << " y: " << center_.y << "\n\n";

  std::cout << "Points positions: " << '\n';
  for (size_t i = 0; i < sides_; i++)
  {
    std::cout << "(" << points_[i].x << ", " << points_[i].y << ") " << '\n';
  }

  std::cout << "\n\n";
}

bool popov::Polygon::checkIfConvex() const
{
  point_t sideA = {0.0, 0.0};
  point_t sideB = {0.0, 0.0};

  for (size_t i = 0; i <= sides_ - 2 ; i++)
  {
    sideA.x = points_[i + 1].x - points_[i].x;
    sideA.y = points_[i + 1].y - points_[i].y;

    if (i == sides_ - 2)
    {
      sideB.x = points_[0].x - points_[i + 1].x;
      sideB.y = points_[0].y - points_[i + 1].y;
    }
    else
    {
      sideB.x = points_[i + 2].x - points_[i + 1].x;
      sideB.y = points_[i + 2].y - points_[i + 1].y;
    }
    if ((sideA.x * sideB.y) - (sideA.y * sideB.x) < 0.0)
    {
      return true;
    }
  }
  return false;
}

void popov::Polygon::rotate(double angle)
{
  if (angle <= 0)
  {
    throw std::invalid_argument("Angle must be positive for rotation");
  }

  const double cos = std::cos((2 * M_PI * angle) / 360);
  const double sin = std::sin((2 * M_PI * angle) / 360);

  const point_t center = getFrameRect().pos;
  point_t tempPoint = center;

  for (size_t i = 0; i < sides_; i++)
  {
    tempPoint.x = center.x + (points_[i].x - center.x) * cos - (points_[i].y - center.y) * sin;
    tempPoint.y = center.y + (points_[i].x - center.x) * sin + (points_[i].y - center.y) * cos;

    points_[i].x = tempPoint.x;
    points_[i].x = tempPoint.y;
  }
}

