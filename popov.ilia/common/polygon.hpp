#ifndef A3_POLYGON_HPP
#define A3_POLYGON_HPP
#include "shape.hpp"

namespace popov
{
  class Polygon : public Shape
  {
  public:

    using point_array = std::unique_ptr<point_t[]>;

    Polygon(const Polygon &other);

    Polygon(Polygon &&other) noexcept;

    Polygon(size_t sides, const point_t *points);

    ~Polygon() = default;

    Polygon &operator=(const Polygon &other);

    Polygon &operator=(Polygon &&other) noexcept;

    point_t getCenter() const;

    double getArea() const override;

    rectangle_t getFrameRect() const override;

    void move(double moveX, double moveY) override;

    void move(const point_t &newPoint) override;

    void scale(double coefficient) override;

    void printInformation() const override;

    bool checkIfConvex() const;

    void rotate(double angle) override;

  private:
    size_t sides_;
    point_array points_;
  };
}

#endif
