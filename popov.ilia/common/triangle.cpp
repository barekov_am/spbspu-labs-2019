#include "triangle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>

popov::Triangle::Triangle(const point_t &a, const point_t &b, const point_t &c):
  a_(a),
  b_(b),
  c_(c)
{
  if (getArea() == 0)
  {
    throw std::invalid_argument("Area must be more than 0");
  }
}

popov::point_t popov::Triangle::getCenter() const
{
  return {(a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3};
}

double popov::Triangle::getArea() const
{
  return fabs(((a_.x - c_.x) * (b_.y - c_.y) - (a_.y - c_.y) * (b_.x - c_.x)) / 2);
}

popov::rectangle_t popov::Triangle::getFrameRect() const
{
  double maxX = std::max(std::max(a_.x, b_.x), c_.x);
  double minX = std::min(std::min(a_.x, b_.x), c_.x);
  double maxY = std::max(std::max(a_.y, b_.y), c_.y);
  double minY = std::min(std::min(a_.y, b_.y), c_.y);

  return {maxX - minX, maxY - minY, {minX + (maxX - minX) / 2, minY + (maxY - minY) / 2}};
}

void popov::Triangle::move(const double moveX, const double moveY)
{
  a_.x += moveX;
  a_.y += moveY;

  b_.x += moveX;
  b_.y += moveY;

  c_.x += moveX;
  c_.y += moveY;
}

void popov::Triangle::move(const point_t &newPoint)
{
  const point_t center_ = getCenter();
  move(newPoint.x - center_.x, newPoint.y - center_.y);
}

void popov::Triangle::scale(const double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient <= 0");
  }

  const point_t center_(getCenter());

  a_.x = center_.x + (a_.x - center_.x) * coefficient;
  a_.y = center_.y + (a_.y - center_.y) * coefficient;

  b_.x = center_.x + (b_.x - center_.x) * coefficient;
  b_.y = center_.y + (b_.y - center_.y) * coefficient;

  c_.x = center_.x + (c_.x - center_.x) * coefficient;
  c_.y = center_.y + (c_.y - center_.y) * coefficient;
}

void popov::Triangle::printInformation() const
{
  std::cout << "Information about triangle: " << '\n';
  rectangle_t boundingRect = getFrameRect();
  std::cout << "Bounding rectangle: Height: " << boundingRect.height << " Width: " << boundingRect.width << '\n';
  std::cout << "Area: " << getArea();
  std::cout << " Center position: x: " << getCenter().x << " y: " << getCenter().y << '\n';
}

void popov::Triangle::rotate(double angle)
{
  if (angle <= 0)
  {
    throw std::invalid_argument("Angle must be positive for rotation");
  }

  const double cos = std::cos((2 * M_PI * angle) / 360);
  const double sin = std::sin((2 * M_PI * angle) / 360);

  const point_t center = getFrameRect().pos;
  point_t tempPoint = a_;

  tempPoint.x = center.x + (a_.x - center.x) * cos - (a_.y - center.y) * sin;
  tempPoint.y = center.y + (a_.x - center.x) * sin + (a_.y - center.y) * cos;
  a_.x = tempPoint.x;
  a_.y = tempPoint.y;

  tempPoint.x = center.x + (b_.x - center.x) * cos - (b_.y - center.y) * sin;
  tempPoint.y = center.y + (b_.x - center.x) * sin + (b_.y - center.y) * cos;
  b_.x = tempPoint.x;
  b_.y = tempPoint.y;

  tempPoint.x = center.x + (c_.x - center.x) * cos - (c_.y - center.y) * sin;
  tempPoint.y = center.y + (c_.x - center.x) * sin + (c_.y - center.y) * cos;
  c_.x = tempPoint.x;
  c_.y = tempPoint.y;
}
