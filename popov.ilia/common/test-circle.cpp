#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>

#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(circleTestSuite)

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_CASE(changesAfterMovingCircle)
{
  popov::Circle circle({2.0, 2.0}, 2.0);

  const double areaBeforeMove = circle.getArea();
  const popov::rectangle_t rectangleBeforeMove = circle.getFrameRect();

  circle.move(1.0, 1.0);
  double areaAfterMove = circle.getArea();
  popov::rectangle_t rectangleAfterMove = circle.getFrameRect();

  BOOST_CHECK_CLOSE(rectangleBeforeMove.width, rectangleAfterMove.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeMove.height, rectangleAfterMove.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ACCURACY);

  circle.move({1.0, 1.0});
  areaAfterMove = circle.getArea();
  rectangleAfterMove = circle.getFrameRect();

  BOOST_CHECK_CLOSE(rectangleBeforeMove.width, rectangleAfterMove.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeMove.height, rectangleAfterMove.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ACCURACY);
}

BOOST_AUTO_TEST_CASE(checkAreaOfCircleAfterScale)
{
  popov::Circle circle({2.0, 2.0}, 2.0);
  const double areaBeforeScale = circle.getArea();
  const popov::rectangle_t rectangleBeforeScale = circle.getFrameRect();

  double scale = 2;
  circle.scale(scale);
  double areaAfterScale = circle.getArea();
  popov::rectangle_t rectangleAfterScale = circle.getFrameRect();

  BOOST_CHECK_CLOSE(rectangleBeforeScale.width * scale, rectangleAfterScale.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeScale.height * scale, rectangleAfterScale.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeScale * scale * scale, areaAfterScale, ACCURACY);

  scale = 0.5;
  circle.scale(scale);
  rectangleAfterScale = circle.getFrameRect();
  areaAfterScale = circle.getArea();

  BOOST_CHECK_CLOSE(rectangleBeforeScale.width, rectangleAfterScale.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeScale.height, rectangleAfterScale.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeScale, areaAfterScale, ACCURACY);
}

BOOST_AUTO_TEST_CASE(incorrectParametrsCircle)
{
  BOOST_CHECK_THROW(popov::Circle({2.0, 2.0}, -2.0), std::invalid_argument);

  popov::Circle circle({2.0, 2.0}, 2.0);
  BOOST_CHECK_THROW(circle.scale(0.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circleRotation)
{
  popov::Circle circle({1, 1}, 2);
  const popov::rectangle_t frameRectBefore = circle.getFrameRect();
  const double areaBefore = circle.getArea();
  circle.rotate(10);
  const double areaAfter = circle.getArea();
  const popov::rectangle_t frameRectAfter = circle.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
}


BOOST_AUTO_TEST_SUITE_END()
