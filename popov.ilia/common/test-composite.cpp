#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(compositeShapeTestSuite)

  BOOST_AUTO_TEST_CASE(testCopyConstructor)
  {
    popov::Rectangle testRectangle({6.0, 6.0}, 6.0, 6.0);
    popov::Circle testCircle({5.0, 5.0}, 5.0);
    popov::CompositeShape testCompShape;
    testCompShape.add(std::make_shared<popov::Circle>(testCircle));
    testCompShape.add(std::make_shared<popov::Rectangle>(testRectangle));
    popov::rectangle_t frameRect = testCompShape.getFrameRect();


    popov::CompositeShape copyCompShape(testCompShape);
    const popov::rectangle_t copyFrameRect = copyCompShape.getFrameRect();
    BOOST_CHECK_CLOSE(testCompShape.getArea(), copyCompShape.getArea(), ACCURACY);
    BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, ACCURACY);
    BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, ACCURACY);
    BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, ACCURACY);
    BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, ACCURACY);
   // BOOST_CHECK_EQUAL(testCompShape.getSize(), copyCompShape.getSize());
  }

  BOOST_AUTO_TEST_CASE(testMoveConstructor)
  {
    popov::Rectangle testRectangle({6.0, 6.0}, 6.0, 6.0);
    popov::Circle testCircle({5.0, 5.0}, 5.0);
    popov::CompositeShape testCompShape;
    testCompShape.add(std::make_shared<popov::Rectangle>(testRectangle));
    const popov::rectangle_t frameRect = testCompShape.getFrameRect();
    const double compositeArea = testCompShape.getArea();
    const int compositeCount = testCompShape.getSize();

    popov::CompositeShape moveCompShape(std::move(testCompShape));
    const popov::rectangle_t moveFrameRect = moveCompShape.getFrameRect();
    BOOST_CHECK_CLOSE(compositeArea, moveCompShape.getArea(), ACCURACY);
    BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, ACCURACY);
    BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, ACCURACY);
    BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, ACCURACY);
    BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, ACCURACY);
    BOOST_CHECK_EQUAL(compositeCount, moveCompShape.getSize());
    BOOST_CHECK_EQUAL(testCompShape.getSize(), 0);
  }


  BOOST_AUTO_TEST_CASE(testCopyOperator)
  {
    popov::Rectangle testRectangle({6.0, 6.0}, 6.0, 6.0);
    popov::Circle testCircle({5.0, 5.0}, 5.0);
    popov::CompositeShape testCompShape1;
    testCompShape1.add(std::make_shared<popov::Rectangle>(testRectangle));

    popov::CompositeShape testCompShape2 = testCompShape1;
    const double firstArea = testCompShape1.getArea();
    const double secondArea = testCompShape2.getArea();
    const popov::rectangle_t firstFrame = testCompShape1.getFrameRect();
    const popov::rectangle_t secondFrame = testCompShape2.getFrameRect();

    BOOST_CHECK_CLOSE(firstArea, secondArea, ACCURACY);
    BOOST_CHECK_CLOSE(firstFrame.pos.x, secondFrame.pos.x, ACCURACY);
    BOOST_CHECK_CLOSE(firstFrame.pos.y, secondFrame.pos.y, ACCURACY);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, ACCURACY);
    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, ACCURACY);
    BOOST_CHECK_EQUAL(testCompShape1.getSize(), testCompShape2.getSize());
  }

  BOOST_AUTO_TEST_CASE(testMoveOperator)
  {
    popov::Rectangle testRectangle({6.0, 6.0}, 6.0, 6.0);
    popov::Circle testCircle({5.0, 5.0}, 5.0);
    popov::CompositeShape testCompShape1;
    testCompShape1.add(std::make_shared<popov::Rectangle>(testRectangle));

    const double firstArea = testCompShape1.getArea();
    const popov::rectangle_t firstFrame = testCompShape1.getFrameRect();
    const int size1 = testCompShape1.getSize();

    popov::CompositeShape testCompShape2 = std::move(testCompShape1);
    const double secondArea = testCompShape2.getArea();
    popov::rectangle_t secondFrame = testCompShape2.getFrameRect();
    const int size2 = testCompShape2.getSize();

    BOOST_CHECK_CLOSE(firstArea, secondArea, ACCURACY);
    BOOST_CHECK_CLOSE(firstFrame.pos.x, secondFrame.pos.x, ACCURACY);
    BOOST_CHECK_CLOSE(firstFrame.pos.y, secondFrame.pos.y, ACCURACY);
    BOOST_CHECK_CLOSE(firstFrame.height, secondFrame.height, ACCURACY);
    BOOST_CHECK_CLOSE(firstFrame.width, secondFrame.width, ACCURACY);
    BOOST_CHECK_EQUAL(size1, size2);
    BOOST_CHECK_EQUAL(testCompShape1.getSize(), 0);

    testCompShape2 = std::move(testCompShape2);
    BOOST_CHECK_CLOSE(firstArea, testCompShape2.getArea(), ACCURACY);
    BOOST_CHECK_CLOSE(firstFrame.height, testCompShape2.getFrameRect().height, ACCURACY);
    BOOST_CHECK_CLOSE(firstFrame.width, testCompShape2.getFrameRect().width, ACCURACY);
    BOOST_CHECK_CLOSE(firstFrame.pos.x, testCompShape2.getFrameRect().pos.x, ACCURACY);
    BOOST_CHECK_CLOSE(firstFrame.pos.y, testCompShape2.getFrameRect().pos.y, ACCURACY);
    BOOST_CHECK_EQUAL(size1, testCompShape2.getSize());

  }


  BOOST_AUTO_TEST_CASE(immutabilityCompShapeAfterMoving)
  {
    popov::Circle testCircle({5.0, 5.0}, 5.0);
    popov::Rectangle testRectangle({6.0, 6.0}, 6.0, 6.0);

    popov::CompositeShape testCompShape;
    testCompShape.add(std::make_shared<popov::Rectangle>(testRectangle));
    const double initialArea = testCompShape.getArea();
    const popov::rectangle_t initialFrameRect = testCompShape.getFrameRect();

    testCompShape.move(1.0, 1.0);
    const popov::rectangle_t firstFrameRect = testCompShape.getFrameRect();
    BOOST_CHECK_CLOSE(firstFrameRect.width, initialFrameRect.width, ACCURACY);
    BOOST_CHECK_CLOSE(firstFrameRect.height, initialFrameRect.height, ACCURACY);
    BOOST_CHECK_CLOSE(testCompShape.getArea(), initialArea, ACCURACY);


    testCompShape.move({-1.0, -1.0});
    const popov::rectangle_t secondFrameRect = testCompShape.getFrameRect();
    BOOST_CHECK_CLOSE(secondFrameRect.width, initialFrameRect.width, ACCURACY);
    BOOST_CHECK_CLOSE(secondFrameRect.height, initialFrameRect.height, ACCURACY);
    BOOST_CHECK_CLOSE(testCompShape.getArea(), initialArea, ACCURACY);
  }


  BOOST_AUTO_TEST_CASE(checkAreaOfCompShapeAfterScaling)
  {
    popov::Circle testCircle({7.0, 7.0}, 7.0);
    popov::Rectangle testRectangle({8.0, 8.0}, 8.0, 8.0);

    popov::CompositeShape testCompShape;
    testCompShape.add(std::make_shared<popov::Rectangle>(testRectangle));

    const double scale = 2.0;
    const double initialArea = testCompShape.getArea();

    testCompShape.scale(scale);
    BOOST_CHECK_CLOSE(testCompShape.getArea(), initialArea * scale * scale, ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(checkFrameRectAfterScaling)
  {
    popov::Circle testCircle({7.0, 7.0}, 7.0);
    popov::Rectangle testRectangle({8.0, 8.0}, 8.0, 8.0);

    popov::CompositeShape testCompShape;
    testCompShape.add(std::make_shared<popov::Rectangle>(testRectangle));

    const double scale = 2.0;
    const popov::rectangle_t initialFrame = testCompShape.getFrameRect();

    testCompShape.scale(scale);

    const popov::rectangle_t finalFrameRect = testCompShape.getFrameRect();
    BOOST_CHECK_CLOSE(initialFrame.width * scale, finalFrameRect.width, ACCURACY);
    BOOST_CHECK_CLOSE(initialFrame.height * scale, finalFrameRect.height, ACCURACY);
    BOOST_CHECK_CLOSE(initialFrame.pos.x, finalFrameRect.pos.x, ACCURACY);
    BOOST_CHECK_CLOSE(initialFrame.pos.y, finalFrameRect.pos.y, ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(checkAreaOfCompShapeAfterScalingLessOne)
  {
    popov::Circle testCircle({7.0, 7.0}, 7.0);
    popov::Rectangle testRectangle({8.0, 8.0}, 8.0, 8.0);

    popov::CompositeShape testCompShape;
    testCompShape.add(std::make_shared<popov::Rectangle>(testRectangle));

    const double scale = 0.5;
    const double initialArea = testCompShape.getArea();

    testCompShape.scale(scale);
    BOOST_CHECK_CLOSE(testCompShape.getArea(), initialArea * scale * scale, ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(checkFrameRectAfterScalingLessOne)
  {
    popov::Circle testCircle({7.0, 7.0}, 7.0);
    popov::Rectangle testRectangle({8.0, 8.0}, 8.0, 8.0);

    popov::CompositeShape testCompShape;
    testCompShape.add(std::make_shared<popov::Rectangle>(testRectangle));

    const double scale = 0.5;
    const popov::rectangle_t initialFrame = testCompShape.getFrameRect();

    testCompShape.scale(scale);

    const popov::rectangle_t finalFrameRect = testCompShape.getFrameRect();
    BOOST_CHECK_CLOSE(initialFrame.width * scale, finalFrameRect.width, ACCURACY);
    BOOST_CHECK_CLOSE(initialFrame.height * scale, finalFrameRect.height, ACCURACY);
    BOOST_CHECK_CLOSE(initialFrame.pos.x, finalFrameRect.pos.x, ACCURACY);
    BOOST_CHECK_CLOSE(initialFrame.pos.y, finalFrameRect.pos.y, ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(checkAreaAfterAdding)
  {
    popov::Circle testCircle({9.0, 9.0}, 9.0);
    popov::CompositeShape testCompShape;
    testCompShape.add(std::make_shared<popov::Circle>(testCircle));
    const double initialArea = testCompShape.getArea();
    const int initialCount = testCompShape.getSize();

    popov::Rectangle testRectangle({10.0, 10.0}, 10.0, 10.0);
    testCompShape.add(std::make_shared<popov::Rectangle>(testRectangle));
    const double finalArea = testCompShape.getArea();

    BOOST_CHECK_EQUAL(initialCount + 1, testCompShape.getSize());
    BOOST_CHECK_CLOSE(initialArea + testRectangle.getArea(), finalArea, ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(checkAreaAfterDeletingByIndex)
  {
    popov::Circle testCircle({9.0, 9.0}, 9.0);
    popov::CompositeShape testCompShape;
    testCompShape.add(std::make_shared<popov::Circle>(testCircle));

    popov::Rectangle testRectangle({10.0, 10.0}, 10.0, 10.0);
    testCompShape.add(std::make_shared<popov::Rectangle>(testRectangle));
    const double initialArea = testCompShape.getArea();
    const int initialCount = testCompShape.getSize();

    testCompShape.remove(0);
    const double finalArea = testCompShape.getArea();

    BOOST_CHECK_EQUAL(initialCount - 1, testCompShape.getSize());
    BOOST_CHECK_CLOSE(initialArea - testCircle.getArea(), finalArea, ACCURACY);
  }


  BOOST_AUTO_TEST_CASE(actionsWithEmptyShape)
  {
    popov::CompositeShape testCompShape;

    BOOST_CHECK_THROW(testCompShape.move(5.0,5.0), std::logic_error);
    BOOST_CHECK_THROW(testCompShape.move({6.0,6.0}), std::logic_error);
    BOOST_CHECK_THROW(testCompShape.scale(2), std::logic_error);
    BOOST_CHECK_THROW(testCompShape.getFrameRect(), std::logic_error);
  }

  BOOST_AUTO_TEST_CASE(incorrrectParametrs)
  {
    popov::Circle testCircle({9.0, 9.0}, 9.0);
    popov::Rectangle testRectangle({10.0, 10.0}, 10.0, 10.0);
    popov::CompositeShape testCompShape;
    testCompShape.add(std::make_shared<popov::Rectangle>(testRectangle));

    BOOST_CHECK_THROW(testCompShape.scale(-3), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(indexOutOfRange)
  {
    popov::Rectangle testRectangle({10.0, 10.0}, 10.0, 10.0);
    popov::Circle testCircle({9.0, 9.0}, 9.0);
    popov::CompositeShape testCompShape;
    testCompShape.add(std::make_shared<popov::Rectangle>(testRectangle));

    BOOST_CHECK_THROW(testCompShape.remove(-1), std::out_of_range);
    BOOST_CHECK_THROW(testCompShape.remove(3), std::out_of_range);
    BOOST_CHECK_THROW(testCompShape[-1], std::out_of_range);
    BOOST_CHECK_THROW(testCompShape[3], std::out_of_range);
  }

BOOST_AUTO_TEST_SUITE_END()

