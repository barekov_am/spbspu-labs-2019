#include "layering.hpp"
#include <cmath>

popov::Matrix<popov::Shape> popov::layer(const popov::CompositeShape &composite)
{
  popov::Matrix<popov::Shape> matrix;

  for (size_t i = 0; i < composite.getSize(); ++i)
  {
    size_t placeRow = 0;

    size_t j = matrix.getRows();
    while (j-- > 0)
    {
      for (size_t k = 0; k < matrix[j].size(); ++k)
      {
        if (intersect(*composite[i], *matrix[j][k]))
        {
          placeRow = j + 1;
          break;
        }
      }
      if (placeRow)
      {
        break;
      }
    }
    matrix.add(placeRow, composite[i]);
  }

  return matrix;
}

bool popov::intersect(const popov::Shape &shape1, const popov::Shape &shape2)
{
  const popov::rectangle_t shape1Frame = shape1.getFrameRect();
  const popov::rectangle_t shape2Frame = shape2.getFrameRect();

  if (std::abs(shape1Frame.pos.x - shape2Frame.pos.x) > (shape1Frame.width + shape2Frame.width) / 2)
  {
    return false;
  }

  if (std::abs(shape1Frame.pos.y - shape2Frame.pos.y) > (shape1Frame.height + shape2Frame.height) / 2)
  {
    return false;
  }

  return true;
}

