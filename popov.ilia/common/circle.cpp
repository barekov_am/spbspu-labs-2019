#include "circle.hpp"
#include <iostream>
#include <cmath>
#include <cassert>

popov::Circle::Circle(const point_t &center, double rad):
  center_(center),
  radius_(rad)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("Radius must be more than 0");
  }
}

double popov::Circle::getArea() const
{
  return radius_ * radius_ * M_PI;
}

popov::rectangle_t popov::Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, center_};
}

void popov::Circle::move(const double moveX, const double moveY)
{
  center_.x += moveX;
  center_.y += moveY;
}

void popov::Circle::move(const point_t &newPoint)
{
  center_ = newPoint;
}

void popov::Circle::printInformation() const
{
  std::cout << "Information about circle: " << '\n';
  rectangle_t boundingRect = getFrameRect();
  std::cout << "Bounding rectangle: Height: " << boundingRect.height << " Width: " << boundingRect.width << '\n';
  std::cout << "Area: " << getArea();
  std::cout << " Center position: x: " << center_.x << " y: " << center_.y << '\n';
}

void popov::Circle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient <= 0");
  }

  radius_ *= coefficient;
}

void popov::Circle::rotate(double)
{
}
