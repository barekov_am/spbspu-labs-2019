#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>

#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(triangleTestSuite)

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_CASE(changesAfterMovingTriangle)
{
  popov::Triangle triangle({-2.0, 0.0}, {0.0, 2.0}, {2.0, 0.0});

  const double areaBeforeMove = triangle.getArea();
  const popov::rectangle_t rectangleBeforeMove = triangle.getFrameRect();

  triangle.move(1.0, 1.0);
  double areaAfterMove = triangle.getArea();
  popov::rectangle_t rectangleAfterMove = triangle.getFrameRect();

  BOOST_CHECK_CLOSE(rectangleBeforeMove.width, rectangleAfterMove.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeMove.height, rectangleAfterMove.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ACCURACY);

  triangle.move({1.0, 1.0});
  areaAfterMove = triangle.getArea();
  rectangleAfterMove = triangle.getFrameRect();

  BOOST_CHECK_CLOSE(rectangleBeforeMove.width, rectangleAfterMove.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeMove.height, rectangleAfterMove.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ACCURACY);
}

BOOST_AUTO_TEST_CASE(checkTriangleAfterScale)
{
  popov::Triangle triangle({-2.0, 0.0}, {0.0, 2.0}, {2.0, 0.0});
  const double areaBeforeScale = triangle.getArea();
  const popov::rectangle_t rectangleBeforeScale = triangle.getFrameRect();

  double scale = 2.0;
  triangle.scale(scale);
  double areaAfterScale = triangle.getArea();
  popov::rectangle_t rectangleAfterScale = triangle.getFrameRect();

  BOOST_CHECK_CLOSE(rectangleBeforeScale.width * scale, rectangleAfterScale.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeScale.height * scale, rectangleAfterScale.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeScale * scale * scale, areaAfterScale, ACCURACY);

  scale = 0.5;
  triangle.scale(scale);
  rectangleAfterScale = triangle.getFrameRect();
  areaAfterScale = triangle.getArea();

  BOOST_CHECK_CLOSE(rectangleBeforeScale.width, rectangleAfterScale.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleBeforeScale.height, rectangleAfterScale.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeScale, areaAfterScale, ACCURACY);
}

BOOST_AUTO_TEST_CASE(incorrectParametrsTriangle)
{
  BOOST_CHECK_THROW(popov::Triangle({0.0, 1.0}, {0.0, 2.0}, {0.0, 3.0}), std::invalid_argument);

  popov::Triangle triangle({-2.0, 0.0}, {0.0, 2.0}, {2.0, 0.0});
  BOOST_CHECK_THROW(triangle.scale(0.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testTriangleRotation)
{
  popov::Triangle testTriangle({ 0, 0 }, { 4, 0 }, { 0, 3 });
  const double areaBefore = testTriangle.getArea();
  const popov::rectangle_t frameRectBefore = testTriangle.getFrameRect();

  double angle = 90;
  testTriangle.rotate(angle);

  double areaAfter = testTriangle.getArea();
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ACCURACY);

  angle = 270;
  testTriangle.rotate(angle);

  popov::rectangle_t frameRectAfter = testTriangle.getFrameRect();
  areaAfter = testTriangle.getArea();

  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, ACCURACY);
}


BOOST_AUTO_TEST_SUITE_END()
