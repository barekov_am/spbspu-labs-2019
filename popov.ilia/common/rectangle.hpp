#ifndef A3_RECTANGLE_HPP
#define A3_RECTANGLE_HPP
#include "shape.hpp"

namespace popov
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t &center, double height, double width);

    rectangle_t getFrameRect() const override;

    double getArea() const override;

    void move(double moveX, double moveY) override;

    void move(const point_t &newPoint) override;

    void scale(double coefficient) override;

    void printInformation() const override;

    void rotate(double angle) override;

  private:
    point_t center_;
    double height_;
    double width_;
    double angle_;
  };
}

#endif
