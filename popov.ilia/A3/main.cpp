#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"


int main()
{
  popov::Rectangle rectangle({1.0, 2.0}, 3.0, 4.0);

  rectangle.move(1.5, 1.5);
  rectangle.printInformation();

  popov::Circle circle({1.0, 2.0}, 3.0);
  popov::Shape *shape = &circle;

  shape->move({2, -2});
  shape->scale(2);
  shape->printInformation();

  std::cout << "\n";
  popov::Triangle triangle({-5.0, 0.0}, {2.0, 7.0}, {2.0, -7.0});
  shape = &triangle;
  shape->scale(2);
  shape->printInformation();

  std::cout << "\n";
  const int sides = 5;
  const popov::point_t points[sides] = {{-3, 0}, {-1, 3}, {1, 3}, {3, 0}, {0, -3}};
  popov::Polygon polygon1(sides, points);
  shape = &polygon1;

  shape->printInformation();
  shape->move({2, 2});
  shape->printInformation();
  shape->scale(2);
  shape->printInformation();

  popov::CompositeShape composite1;
  composite1.add(std::make_shared<popov::Rectangle>(rectangle));
  std::cout << composite1.getArea() << '\n';

  composite1.add(std::make_shared<popov::Circle>(circle));
  std::cout << composite1.getArea() << '\n';

  composite1.printInformation();

  composite1.scale(2.0);
  std::cout << composite1.getArea() << '\n';
  composite1.remove(1);
  std::cout << composite1.getArea() << '\n';

  composite1.printInformation();

  return 0;
}
