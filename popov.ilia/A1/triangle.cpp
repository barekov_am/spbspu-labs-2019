#include "triangle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>

Triangle::Triangle(const point_t &a, const point_t &b, const point_t &c):
  a_(a),
  b_(b),
  c_(c)
{
  if (getArea() == 0)
  {
    throw std::invalid_argument("Area must be more than 0");
  }
}

point_t Triangle::getCenter() const
{
  return {(a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3};
}

double Triangle::getArea() const
{
  return fabs(((a_.x - c_.x) * (b_.y - c_.y) - (a_.y - c_.y) * (b_.x - c_.x)) / 2);
}

rectangle_t Triangle::getFrameRect() const
{
  double maxX = std::max(std::max(a_.x, b_.x), c_.x);
  double minX = std::min(std::min(a_.x, b_.x), c_.x);
  double maxY = std::max(std::max(a_.y, b_.y), c_.y);
  double minY = std::min(std::min(a_.y, b_.y), c_.y);

  return {maxX - minX, maxY - minY, {minX + (maxX - minX) / 2, minY + (maxY - minY) / 2}};
}

void Triangle::move(const double moveX, const double moveY)
{
  a_.x += moveX;
  a_.y += moveY;

  b_.x += moveX;
  b_.y += moveY;

  c_.x += moveX;
  c_.y += moveY;
}

void Triangle::move(const point_t &newPoint)
{
  const point_t center_ = getCenter();
  move(newPoint.x - center_.x, newPoint.y - center_.y);
}

void Triangle::printInformation() const
{
  std::cout << "Information about triangle: " << '\n';
  rectangle_t boundingRect = getFrameRect();
  std::cout << "Bounding rectangle: Height: " << boundingRect.height << " Width: " << boundingRect.width << '\n';
  std::cout << "Area: " << getArea();
  std::cout << " Center position: x: " << getCenter().x << " y: " << getCenter().y << '\n';
}
