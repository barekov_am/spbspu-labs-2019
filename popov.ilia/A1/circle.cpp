#include "circle.hpp"
#include <iostream>
#include <cmath>
#include <cassert>

Circle::Circle(const point_t &center, double rad):
  center_(center),
  radius_(rad)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("Radius must be more than 0");
  }
}

double Circle::getArea() const
{
  return radius_ * radius_ * M_PI;
}

rectangle_t Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, center_};
}

void Circle::move(const double moveX, const double moveY)
{
  center_.x += moveX;
  center_.y += moveY;
}

void Circle::move(const point_t &newPoint)
{
  center_ = newPoint;
}

void Circle::printInformation() const
{
  std::cout << "Information about circle: " << '\n';
  rectangle_t boundingRect = getFrameRect();
  std::cout << "Bounding rectangle: Height: " << boundingRect.height << " Width: " << boundingRect.width << '\n';
  std::cout << "Area: " << getArea();
  std::cout << " Center position: x: " << center_.x << " y: " << center_.y << '\n';
}
