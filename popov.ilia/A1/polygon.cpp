#include "polygon.hpp"
#include <iostream>
#include <cmath>


Polygon::Polygon(const Polygon &other):
  sides_(other.sides_),
  points_(new point_t[sides_])
{
  for (int i = 0; i < sides_; i++)
  {
    points_[i] = other.points_[i];
  }
}

Polygon::Polygon(Polygon &&other) noexcept:
  sides_(other.sides_),
  points_(other.points_)
{
  other.sides_ = 0;
  other.points_ = nullptr;
}

Polygon::Polygon(int sides, const point_t *points):
  sides_(sides),
  points_(new point_t[sides])
{
  if (sides_ < 3)
  {
    delete[] points_;
    throw std::invalid_argument("Number of sides must be more than 2");
  }

  if (points == nullptr)
  {
    delete[] points_;
    throw std::invalid_argument("Pointer can't be nullptr");
  }

  for (int i = 0; i < sides_; i++)
  {
    points_[i] = points[i];
  }

  if (!checkIfConvex())
  {
    delete[] points_;
    throw std::invalid_argument("Polygon must be convex");
  }
}

Polygon::~Polygon()
{
  delete[] points_;
}

Polygon &Polygon::operator=(const Polygon &other)
{
  if (this != &other)
  {
    sides_ = other.sides_;

    delete[] points_;

    points_ = new point_t[sides_];
    for (int i = 0; i < sides_; i++)
    {
      points_[i] = other.points_[i];
    }
  }

  return  *this;
}

Polygon &Polygon::operator=(Polygon &&other) noexcept
{
  if (this != &other)
  {
    sides_ = other.sides_;
    other.sides_ = 0;

    delete[] points_;

    points_ = other.points_;
    other.points_ = nullptr;
  }

  return *this;
}

point_t Polygon::getCenter() const
{
  point_t centerTemp = {0.0, 0.0};

  for (int i = 0; i < sides_; i++)
  {
    centerTemp.x += points_[i].x;
    centerTemp.y += points_[i].y;
  }
  return {centerTemp.x / sides_, centerTemp.y / sides_};
}

double Polygon::getArea() const
{
  double sum = 0.0;

  for (int i = 0; i < sides_ - 1; i++)
  {
    sum += points_[i].x * points_[i + 1].y;
    sum -= points_[i + 1].x * points_[i].y;
  }    

  sum += points_[sides_ - 1].x * points_[0].y;
  sum -= points_[0].x * points_[sides_ - 1].y;

  return fabs(sum) / 2;
}

rectangle_t Polygon::getFrameRect() const
{
  double maxX = points_[0].x;
  double minX = points_[0].x;
  double maxY = points_[0].y;
  double minY = points_[0].y;

  for (int i = 1; i < sides_; i++)
  {
    maxX = std::max(maxX, points_[i].x);
    minX = std::min(minX, points_[i].x);
    maxY = std::max(maxY, points_[i].y);
    minY = std::min(minY, points_[i].y);
  }

  return {maxX - minX, maxY - minY, {minX + (maxX - minX) / 2, minY + (maxY - minY) / 2}};
}

void Polygon::move(double moveX, double moveY)
{
  for (int i = 0; i < sides_; i++)
  {
    points_[i].x += moveX;
    points_[i].y += moveY;
  }
}

void Polygon::move(const point_t &newPoint)
{
  const point_t center_ = getCenter();
  move(newPoint.x - center_.x, newPoint.y - center_.y);
}

void Polygon::printInformation() const
{
  std::cout << "Information about polygon: " << '\n';
  rectangle_t boundingRect = getFrameRect();
  std::cout << "Bounding rectangle: Height: " << boundingRect.height << " Width: " << boundingRect.width << '\n';
  std::cout << "Area: " << getArea();
  const point_t center_ = getCenter();
  std::cout << " Center position: x: " << center_.x << " y: " << center_.y << "\n\n";
}

bool Polygon::checkIfConvex() const
{
  point_t sideA = {0.0, 0.0};
  point_t sideB = {0.0, 0.0};

  for (int i = 0; i <= sides_ - 2 ; i++)
  {
    sideA.x = points_[i + 1].x - points_[i].x;
    sideA.y = points_[i + 1].y - points_[i].y;

    if (i == sides_ - 2)
    {
      sideB.x = points_[0].x - points_[i + 1].x;
      sideB.y = points_[0].y - points_[i + 1].y;
    }
    else
    {
      sideB.x = points_[i + 2].x - points_[i + 1].x;
      sideB.y = points_[i + 2].y - points_[i + 1].y;
    }
    if ((sideA.x * sideB.y) - (sideA.y * sideB.x) < 0.0)
    {
      return true;
    }
  }
  return false;
}
