#ifndef A1_SHAPE_HPP
#define A1_SHAPE_HPP
#include "base-types.hpp"

class Shape
{
public:
  virtual ~Shape() = default;
  virtual double getArea() const = 0;
  virtual rectangle_t getFrameRect() const = 0;
  virtual void move(double moveX, double moveY) = 0;
  virtual void move(const point_t &newPoint) = 0;
  virtual void printInformation() const = 0;
};

#endif
