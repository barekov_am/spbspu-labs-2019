#ifndef A1_RECTANGLE_HPP
#define A1_RECTANGLE_HPP
#include "shape.hpp"

class Rectangle: public Shape
{
public:
  Rectangle(const point_t &center, double height, double width);
  rectangle_t getFrameRect() const override;
  double getArea() const override;
  void move(double moveX, double moveY) override;
  void move(const point_t &newPoint) override;
  void printInformation() const override;

private:
  point_t center_;
  double height_;
  double width_;
};

#endif
