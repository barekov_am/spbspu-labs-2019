#include "rectangle.hpp"
#include <iostream>
#include <cassert>

Rectangle::Rectangle(const point_t &center, double height, double width):
  center_(center),
  height_(height),
  width_(width)
{
  if ((height_ <= 0) || (width_ <= 0))
  {
    throw std::invalid_argument("Parameters must be more than 0");
  }
}

double Rectangle::getArea() const
{
  return height_ * width_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return {height_, width_, center_};
}

void Rectangle::move(const double moveX, const double moveY)
{
  center_.x += moveX;
  center_.y += moveY;
}

void Rectangle::move(const point_t &newPoint)
{
  center_ = newPoint;
}

void Rectangle::printInformation() const
{
  std::cout << "Information about rectangle: " << '\n';

  rectangle_t boundingRect = getFrameRect();
  std::cout << "Bounding rectangle: Height: " << boundingRect.height << " Width: " << boundingRect.width << '\n';
  std::cout << "Area: " << getArea();
  std::cout << " Center position: x: " << center_.x << " y: " << center_.y << '\n';
}
