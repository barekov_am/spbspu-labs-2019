#ifndef A1_POLYGON_HPP
#define A1_POLYGON_HPP
#include "shape.hpp"

class Polygon: public Shape
{
public:
  Polygon(const Polygon &other);
  Polygon(Polygon &&other) noexcept;

  Polygon(int sides, const point_t *points);
  ~Polygon() override;

  Polygon &operator = (const Polygon &other);
  Polygon &operator = (Polygon &&other) noexcept;

  point_t getCenter() const;
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(double moveX, double moveY) override;
  void move(const point_t &newPoint) override;
  void printInformation() const override;
  bool checkIfConvex() const;

private:
  int sides_;
  point_t *points_;
};


#endif //A1_POLYGON_HPP
