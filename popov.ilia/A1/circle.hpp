#ifndef A1_CIRCLE_HPP
#define A1_CIRCLE_HPP
#include "shape.hpp"

class Circle: public Shape
{
public:
  Circle(const point_t &center, double rad);
  rectangle_t getFrameRect() const override;
  double getArea() const override;
  void move(double moveX, double moveY) override;
  void move(const point_t &newPoint) override;
  void printInformation() const override;

private:
  point_t center_;
  double radius_;
};

#endif
