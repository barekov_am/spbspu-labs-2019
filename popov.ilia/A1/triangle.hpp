#ifndef A1_TRIANGLE_HPP
#define A1_TRIANGLE_HPP
#include "shape.hpp"

class Triangle: public Shape
{
public:
  Triangle(const point_t &a, const point_t &b, const point_t &c);
  point_t getCenter() const;
  rectangle_t getFrameRect() const override;
  double getArea() const override;
  void move(double moveX, double moveY) override;
  void move(const point_t &newPoint) override;
  void printInformation() const override;

private:
  point_t a_;
  point_t b_;
  point_t c_;
};

#endif
