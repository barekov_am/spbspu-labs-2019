#include <cstdio>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "util.hpp"

int main()
{
  altoukhov::Shape::shape_array shapes = std::make_unique<altoukhov::Shape::shape_ptr[]>(3);
  shapes[0] = std::make_shared<altoukhov::Rectangle>(altoukhov::Rectangle({ 0, 0 }, 2, 2));
  shapes[1] = std::make_shared<altoukhov::Circle>(altoukhov::Circle({ 2, 1 }, 1));
  shapes[2] = std::make_shared<altoukhov::Triangle>(altoukhov::Triangle({ 1, 1 }, { 2, 2 }, { 2, 0 }));

  altoukhov::Matrix matrix = altoukhov::part(shapes, 3);
  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    printf("Layer #%lu: | ", i);

    for (size_t j = 0; j < matrix.getCols(); j++)
    {
      if (matrix(i, j) != nullptr)
      {
        printf("{%.2lf,%.2lf} | ", matrix(i, j)->getCenter().x, matrix(i, j)->getCenter().y);
      }
      else
      {
        printf("%12c| ", ' ');
      }
    }
    
    printf("\n");
  }
}
