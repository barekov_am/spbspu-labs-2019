#ifndef UTILITY_FUNCTIONS_HPP
#define UTILITY_FUNCTIONS_HPP

#include <iostream>
#include <functional>
#include <string>
#include <stdexcept>

////////////////////////////////////////////////////////////////
//                   Function template sort                   //
////////////////////////////////////////////////////////////////

template <template <typename> typename AccessT,
          typename ContainerT,
          typename ComparatorT>
void sort(ContainerT& container, ComparatorT comparator)
{
  for (auto i = AccessT<ContainerT>::begin(container); i != AccessT<ContainerT>::end(container); i++)
  {
    for (auto j = AccessT<ContainerT>::begin(container); j != AccessT<ContainerT>::end(container); j++)
    {
      auto& ithItem = AccessT<ContainerT>::get(container, i);
      auto& jthItem = AccessT<ContainerT>::get(container, j);
      if (comparator(ithItem, jthItem))
      {
        std::swap(ithItem, jthItem);
      }
    }
  }
}

////////////////////////////////////////////////////////////////
//              Function template printContainer              //
////////////////////////////////////////////////////////////////

template <typename ContainerT>
void printContainer(const ContainerT& container,
                    std::ostream& ostr,
                    const char* delim = " ",  // const char* because string literal can be empty, char literal can't
                    const char* end   = "\n")
{
  for (auto iter = container.cbegin(); iter != container.cend(); ++iter)
  {
    ostr << *iter << delim;
  }
  ostr << end;
}

////////////////////////////////////////////////////////////////
//            Function template generateComparator            //
////////////////////////////////////////////////////////////////

template <typename T>
std::function<bool(const T&, const T&)> generateComparator(const std::string& ordering)
{
  if (ordering == "ascending")
  {
    return std::less<>();
  }

  if (ordering == "descending")
  {
    return std::greater<>();
  }

  throw std::invalid_argument("Error: invalid argument value for generateComparator().");
}

#endif
