#include "tasks.hpp"

#include <iostream>
#include <vector>
#include <forward_list>
#include <fstream>
#include <functional>
#include <memory>
#include <cstdlib>
#include <iterator>

#include "util-funcs.hpp"
#include "access-strategy.hpp"

////////////////////////////////////////////////////////////////
//                           Task 1                           //
////////////////////////////////////////////////////////////////

void executeTaskOne(const std::string& ordering)
{
  auto comparator = generateComparator<int>(ordering);
  std::vector<int> indexAccessContainer;

  int input = 0;
  while (std::cin >> input, !std::cin.eof())
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Error: failed to process input.");
    }
    indexAccessContainer.push_back(input);
  }

  std::vector<int>       atAccessContainer(indexAccessContainer);
  std::forward_list<int> iterAccessContainer(indexAccessContainer.begin(), indexAccessContainer.end());

  sort<IndexAccess>(indexAccessContainer, comparator);
  sort<AtAccess>(atAccessContainer, comparator);
  sort<IteratorAccess>(iterAccessContainer, comparator);

  printContainer(indexAccessContainer, std::cout);
  printContainer(atAccessContainer, std::cout);
  printContainer(iterAccessContainer, std::cout);
}

////////////////////////////////////////////////////////////////
//                           Task 2                           //
////////////////////////////////////////////////////////////////

void executeTaskTwo(const std::string& filename)
{
  using c_dealloc_buffer = std::unique_ptr<char[], decltype(&std::free)>;
  std::ifstream file(filename);

  if (!file.is_open())
  {
    throw std::runtime_error("Error: failed to open file.");
  }

  size_t size     = 0;
  size_t capacity = 1024;
  c_dealloc_buffer array(static_cast<char*>(std::malloc(capacity)), &std::free);
  if (!array)
  {
    throw std::runtime_error("Error: failed to allocate memory.");
  }

  while (!file.eof())
  {
    file.read(&array[size], (capacity - size));
    size += file.gcount();

    // If read() hits EOF before it extracts required amount of characters,
    // it sets both eofbit and failbit, which means doing this fail() check for
    // input errors will actually throw even if file finished reading successfully and hit EOF.
    // Operator bool simply returns fail(), so it's the same.
    // Only way to check actual input errors is to explicitly check for failbit without eofbit.
    if (!file.eof() && file.fail())
    {
      throw std::runtime_error("Error: failed to process input.");
    }

    if (size == capacity)
    {
      capacity *= 2;
      c_dealloc_buffer tmp(static_cast<char*>(std::realloc(array.get(), capacity)), std::free);

      if (!tmp)
      {
        throw std::bad_alloc();
      }

      array.release();
      std::swap(array, tmp);
    }
  }

  if (!size)
  {
    return;
  }

  std::vector<char> container(&array[0], &array[size]);
  printContainer(container, std::cout, "", "");
}

////////////////////////////////////////////////////////////////
//                           Task 3                           //
////////////////////////////////////////////////////////////////

void executeTaskThree()
{
  std::vector<int> container;

  int input = 0;
  while (std::cin >> input, !std::cin.eof())
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Error: failed to process input.");
    }
    if (input == 0)
    {
      break;
    }
    container.push_back(input);
  }

  if (input != 0)
  {
    throw std::runtime_error("Error: expected 0 at the end of file.");
  }

  if (container.empty())
  {
    return;
  }

  if (container.back() == 1)
  {
    std::vector<int>::iterator iter = container.begin();
    while (iter != container.end())
    {
      if (*iter % 2 == 0)
      {
        iter = container.erase(iter);
      }
      else
      {
        ++iter;
      }
    }
  }

  else if (container.back() == 2)
  {
    std::vector<int>::iterator iter = container.begin();
    while (iter != container.end())
    {
      if (*iter % 3 == 0)
      {
        iter = (container.insert(iter + 1, 3, 1) + 3);
      }
      else
      {
        ++iter;
      }
    }
  }

  printContainer(container, std::cout);
}

////////////////////////////////////////////////////////////////
//                           Task 4                           //
////////////////////////////////////////////////////////////////

void executeTaskFour(const std::string& ordering, size_t size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Error: size cannot be 0 or less.");
  }

  auto comparator = generateComparator<double>(ordering);
  std::vector<double> container(size);
  fillRandom(container.data(), container.size());

  printContainer(container, std::cout);
  sort<IndexAccess>(container, comparator);
  printContainer(container, std::cout);
}

void fillRandom(double* array, int size)
{
  for (int i = 0; i < size; i++)
  {
    array[i] = static_cast<double>(rand()) / RAND_MAX * 2 - 1;
  }
}
