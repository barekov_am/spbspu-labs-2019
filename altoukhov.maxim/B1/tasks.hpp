#ifndef TASKS_HPP
#define TASKS_HPP

#include <string>

void executeTaskOne(const std::string& ordering);
void executeTaskTwo(const std::string& filename);
void executeTaskThree();
void executeTaskFour(const std::string& ordering, size_t size);
void fillRandom(double* array, int size);

#endif
