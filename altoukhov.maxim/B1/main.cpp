#include <string>
#include <iostream>
#include <ctime>
#include "tasks.hpp"

#define SUCCESS       0
#define E_INVALID_ARG 1
#define E_INTERNAL    2

int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    std::cerr << "Error: too few command line arguments to run.\n";
    return E_INVALID_ARG;
  }

  srand(time(nullptr));
  try
  {
    switch (std::stoi(argv[1]))
    {
    case (1):
      if (argc < 3)
      {
        throw std::invalid_argument("Error: this task requires 1 more arg to run.");
      }
      executeTaskOne(argv[2]);
      return SUCCESS;
    
    case (2):
      if (argc < 3)
      {
        throw std::invalid_argument("Error: this task requires 1 more arg to run.");
      }
      executeTaskTwo(argv[2]);
      return SUCCESS;

    case (3):
      executeTaskThree();
      return SUCCESS;

    case (4):
      if (argc < 4)
      {
        throw std::invalid_argument("Error: this task requires 2 more arg to run.");
      }
      executeTaskFour(argv[2], std::stoull(argv[3]));
      return SUCCESS;

    default:
      throw std::invalid_argument("Error: must enter a valid task number.");
    }
  }

  catch (const std::invalid_argument& exc)
  {
    std::cerr << exc.what() << '\n';
    return E_INVALID_ARG;
  }

  catch (const std::exception& exc)
  {
    std::cerr << exc.what() << '\n';
    return E_INTERNAL;
  }
}
