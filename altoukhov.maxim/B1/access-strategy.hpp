#ifndef ACCESS_STRATEGY_HPP
#define ACCESS_STRATEGY_HPP

////////////////////////////////////////////////////////////////
//                       Access by index                      //
////////////////////////////////////////////////////////////////

template <typename ContainerT>
class IndexAccess
{
public:
  using container_type          = ContainerT;
  using container_iterator_type = typename ContainerT::size_type;
  using container_item_ref_type = typename ContainerT::reference;

  static container_item_ref_type get(container_type& container, container_iterator_type where)
  {
    return container[where];
  }

  static container_iterator_type begin(const container_type&)
  {
    return 0;
  }

  static container_iterator_type end(const container_type& container)
  {
    return container.size();
  }
};

////////////////////////////////////////////////////////////////
//                       Access by at()                       //
////////////////////////////////////////////////////////////////

template <typename ContainerT>
class AtAccess
{
public:
  using container_type          = ContainerT;
  using container_iterator_type = typename ContainerT::size_type;
  using container_item_ref_type = typename ContainerT::reference;

  static container_item_ref_type get(container_type& container, container_iterator_type where)
  {
    return container.at(where);
  }

  static container_iterator_type begin(const container_type&)
  {
    return 0;
  }

  static container_iterator_type end(const container_type& container)
  {
    return container.size();
  }
};

////////////////////////////////////////////////////////////////
//                     Access by iterator                     //
////////////////////////////////////////////////////////////////

template <typename ContainerT>
class IteratorAccess
{
public:
  using container_type          = ContainerT;
  using container_iterator_type = typename ContainerT::iterator;
  using container_item_ref_type = typename ContainerT::reference;

  static container_item_ref_type get(container_type&, container_iterator_type where)
  {
    return *where;
  }

  static container_iterator_type begin(container_type& container)
  {
    return container.begin();
  }

  static container_iterator_type end(container_type& container)
  {
    return container.end();
  }
};

#endif
