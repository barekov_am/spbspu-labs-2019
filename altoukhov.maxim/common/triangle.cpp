#include "triangle.hpp"
#include "util.hpp"
#include <stdexcept>
#include <algorithm>

altoukhov::Triangle::Triangle(const altoukhov::point_t& a, const altoukhov::point_t& b, const altoukhov::point_t& c):
  a_{a},
  b_{b},
  c_{c},
  angle_{0}
{
  if (getArea() <= 0)
  {
    throw std::invalid_argument("invalid position values");
  }
}

double altoukhov::Triangle::getArea() const
{
  return (std::fabs((a_.x - c_.x) * (b_.y - c_.y) - (b_.x - c_.x) * (a_.y - c_.y)) / 2);
}

altoukhov::rectangle_t altoukhov::Triangle::getFrameRect() const
{
  const double left = std::min({a_.x, b_.x, c_.x});
  const double right = std::max({a_.x, b_.x, c_.x});
  const double top = std::min({a_.y, b_.y, c_.y});
  const double bottom = std::max({a_.y, b_.y, c_.y});
  const double width = right - left;
  const double height = bottom - top;
  const altoukhov::point_t position = {((left + right) / 2), ((top + bottom) / 2)};
  return {position, width, height};
}

altoukhov::point_t altoukhov::Triangle::getCenter() const
{
  return {(a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3};
}

double altoukhov::Triangle::getAngle() const
{
  return angle_;
}

void altoukhov::Triangle::move(const altoukhov::point_t& point)
{
  point_t center = getCenter();
  move((point.x - center.x), (point.y - center.y));
}

void altoukhov::Triangle::move(double dx, double dy)
{
  a_.x += dx;
  b_.x += dx;
  c_.x += dx;
  a_.y += dy;
  b_.y += dy;
  c_.y += dy;
}

void altoukhov::Triangle::scale(double ratio)
{
  if (ratio <= 0)
  {
    throw std::invalid_argument("invalid scaling ratio value");
  }
  
  altoukhov::point_t center = getCenter();

  a_.x = center.x + ((a_.x - center.x) * ratio);
  a_.y = center.y + ((a_.y - center.y) * ratio);

  b_.x = center.x + ((b_.x - center.x) * ratio);
  b_.y = center.y + ((b_.y - center.y) * ratio);

  c_.x = center.x + ((c_.x - center.x) * ratio);
  c_.y = center.y + ((c_.y - center.y) * ratio);
}

void altoukhov::Triangle::rotate(double angle)
{
  a_ = getRotatedPoint(getCenter(), a_, angle);
  b_ = getRotatedPoint(getCenter(), b_, angle);
  c_ = getRotatedPoint(getCenter(), c_, angle);
}
