#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(CompositeTestSuite)

  const double INACCURACY = 0.001;
  const altoukhov::Rectangle EXAMPLE_RECTANGLE({0, 0}, 2, 2);
  const altoukhov::Circle EXAMPLE_CIRCLE({0, 0}, 2);
  const altoukhov::Shape::shape_ptr EXAMPLE_RECT_PTR = std::make_shared<altoukhov::Rectangle>(EXAMPLE_RECTANGLE);
  const altoukhov::Shape::shape_ptr EXAMPLE_CIR_PTR = std::make_shared<altoukhov::Circle>(EXAMPLE_CIRCLE);

  BOOST_AUTO_TEST_CASE(ImmutabilityTest)
  {
    altoukhov::CompositeShape composition;
    composition.add(EXAMPLE_RECT_PTR);
    composition.add(EXAMPLE_CIR_PTR);

    const double widthBeforeMoving = composition.getFrameRect().width;
    const double heightBeforeMoving = composition.getFrameRect().height;
    const double areaBeforeMoving = composition.getArea();

    composition.move(1, 1);
    BOOST_CHECK_CLOSE(widthBeforeMoving, composition.getFrameRect().width, INACCURACY);
    BOOST_CHECK_CLOSE(heightBeforeMoving, composition.getFrameRect().height, INACCURACY);
    BOOST_CHECK_CLOSE(areaBeforeMoving, composition.getArea(), INACCURACY);

    composition.move({ 0, 0 });
    BOOST_CHECK_CLOSE(widthBeforeMoving, composition.getFrameRect().width, INACCURACY);
    BOOST_CHECK_CLOSE(heightBeforeMoving, composition.getFrameRect().height, INACCURACY);
    BOOST_CHECK_CLOSE(areaBeforeMoving, composition.getArea(), INACCURACY);
  }

  BOOST_AUTO_TEST_CASE(ScalingTest)
  {
    altoukhov::CompositeShape composition;
    composition.add(EXAMPLE_RECT_PTR);
    composition.add(EXAMPLE_CIR_PTR);

    const double areaBeforeScaling = composition.getArea();
    const double scalingRatio = 2;
    composition.scale(scalingRatio);
    BOOST_CHECK_CLOSE((areaBeforeScaling * std::pow(scalingRatio, 2)), composition.getArea(), INACCURACY);
  }

  BOOST_AUTO_TEST_CASE(ExceptionTest)
  {
    altoukhov::CompositeShape composition;
    composition.add(EXAMPLE_RECT_PTR);
    composition.add(EXAMPLE_CIR_PTR);

    BOOST_CHECK_THROW(composition[2], std::out_of_range);
    BOOST_CHECK_THROW(composition.remove(2), std::out_of_range);
    BOOST_CHECK_THROW(composition.scale(-1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    altoukhov::CompositeShape compositionOriginal;
    compositionOriginal.add(EXAMPLE_RECT_PTR);
    compositionOriginal.add(EXAMPLE_CIR_PTR);

    altoukhov::CompositeShape compositionCopy(compositionOriginal);
    BOOST_CHECK_EQUAL(compositionOriginal.getArea(), compositionCopy.getArea());
  }

  BOOST_AUTO_TEST_CASE(CopyOperatorTest)
  {
    altoukhov::CompositeShape compositionOriginal;
    compositionOriginal.add(EXAMPLE_RECT_PTR);
    compositionOriginal.add(EXAMPLE_CIR_PTR);

    altoukhov::CompositeShape compositionCopy = compositionOriginal;
    BOOST_CHECK_EQUAL(compositionOriginal.getArea(), compositionCopy.getArea());
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    altoukhov::CompositeShape compositionOriginal;
    compositionOriginal.add(EXAMPLE_RECT_PTR);
    compositionOriginal.add(EXAMPLE_CIR_PTR);

    const double originalArea = compositionOriginal.getArea();
    altoukhov::CompositeShape compositionMove(std::move(compositionOriginal));
    BOOST_CHECK_EQUAL(compositionMove.getArea(), originalArea);
    BOOST_CHECK_THROW(compositionOriginal.getArea(), std::logic_error);
  }

  BOOST_AUTO_TEST_CASE(MoveOperatorTest)
  {
    altoukhov::CompositeShape compositionOriginal;
    compositionOriginal.add(EXAMPLE_RECT_PTR);
    compositionOriginal.add(EXAMPLE_CIR_PTR);

    const double originalArea = compositionOriginal.getArea();
    altoukhov::CompositeShape compositionMove = std::move(compositionOriginal);
    BOOST_CHECK_EQUAL(compositionMove.getArea(), originalArea);
    BOOST_CHECK_THROW(compositionOriginal.getArea(), std::logic_error);
  }

  BOOST_AUTO_TEST_CASE(RotationTest)
  {
    altoukhov::CompositeShape composition;
    composition.add(EXAMPLE_RECT_PTR);
    composition.add(EXAMPLE_CIR_PTR);
    const double originalArea = composition.getArea();
    const altoukhov::rectangle_t originalFrame = composition.getFrameRect();

    composition.rotate(90);
    BOOST_CHECK_CLOSE(originalArea, composition.getArea(), INACCURACY);
    BOOST_CHECK_CLOSE(originalFrame.width, composition.getFrameRect().width, INACCURACY);
    BOOST_CHECK_CLOSE(originalFrame.height, composition.getFrameRect().height, INACCURACY);
  }

BOOST_AUTO_TEST_SUITE_END()
