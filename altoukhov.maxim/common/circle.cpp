#include "circle.hpp"
#include <stdexcept>

altoukhov::Circle::Circle(const altoukhov::point_t& pos, double radius):
  pos_{pos},
  radius_{radius}
{
  if (radius <= 0)
  {
    throw std::invalid_argument("invalid radius value");
  }
}

double altoukhov::Circle::getArea() const
{
  return (std::pow(radius_, 2) * M_PI);
}

altoukhov::rectangle_t altoukhov::Circle::getFrameRect() const
{
  return {pos_, (2 * radius_), (2 * radius_)};
}

altoukhov::point_t altoukhov::Circle::getCenter() const
{
  return pos_;
}

double altoukhov::Circle::getAngle() const
{
  return 0;
}

void altoukhov::Circle::move(const altoukhov::point_t& point)
{
  pos_ = point;
}

void altoukhov::Circle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void altoukhov::Circle::scale(double ratio)
{
  if (ratio <= 0)
  {
    throw std::invalid_argument("invalid scaling ratio value");
  }
  radius_ *= ratio;
}

void altoukhov::Circle::rotate(double)
{
}
