#include "polygon.hpp"
#include "util.hpp"
#include <stdexcept>
#include <utility>
#include <algorithm>

altoukhov::Polygon::Polygon(altoukhov::point_t* vertices, int amount):
  vertices_{new altoukhov::point_t[amount]},
  count_{amount},
  angle_{0}
{
  if (amount < 3)
  {
    delete[] vertices_;
    throw std::invalid_argument("vertex number can't be less than 3");
  }
  
  if (vertices == nullptr)
  {
    delete[] vertices_;
    throw std::invalid_argument("pointer to vertices is null");
  }

  for (int i = 0; i < amount; i++)
  {
    vertices_[i] = vertices[i];
  }

  if (!isConvex())
  {
    delete[] vertices_;
    throw std::invalid_argument("polygon must be a convex one");
  }
}

altoukhov::Polygon::Polygon(const Polygon& other):
  vertices_{new altoukhov::point_t[other.count_]},
  count_{other.count_},
  angle_{other.angle_}
{
  for (int i = 0; i < count_; i++)
  {
    vertices_[i] = other.vertices_[i];
  }
}

altoukhov::Polygon::Polygon(Polygon&& other):
  vertices_{other.vertices_},
  count_{other.count_},
  angle_{other.angle_}
{
  other.vertices_ = nullptr;
  other.count_ = 0;
  other.angle_ = 0;
}

altoukhov::Polygon::~Polygon()
{
  delete[] vertices_;
}

altoukhov::Polygon& altoukhov::Polygon::operator = (const altoukhov::Polygon& other)
{
  if (this != &other)
  {
    altoukhov::point_t* tmp = new altoukhov::point_t[other.count_];
    delete[] vertices_;
    vertices_ = tmp;
    count_ = other.count_;
    angle_ = other.angle_;

    for (int i = 0; i < count_; i++)
    {
      vertices_[i] = other.vertices_[i];
    }
  }

  return *this;
}

altoukhov::Polygon& altoukhov::Polygon::operator = (altoukhov::Polygon&& other)
{
  delete[] vertices_;

  vertices_ = other.vertices_;
  count_ = other.count_;
  angle_ = other.angle_;

  other.vertices_ = nullptr;
  other.count_ = 0;
  other.angle_ = 0;

  return *this;
}

double altoukhov::Polygon::getArea() const
{
  if (!vertices_)
  {
    throw std::logic_error("object has been moved");
  }

  double area = 0;
  for (int i = 0; i < (count_ - 1); i++)
  {
    area += (vertices_[i].x * vertices_[i + 1].y);
    area -= (vertices_[i + 1].x * vertices_[i].y);
  }

  area += (vertices_[count_ - 1].x * vertices_[0].y);
  area -= (vertices_[0].x * vertices_[count_ - 1].y);

  return (std::abs(area) * 0.5);
}

altoukhov::rectangle_t altoukhov::Polygon::getFrameRect() const
{
  if (!vertices_)
  {
    throw std::logic_error("object has been moved");
  }

  double maxX = vertices_[0].x;
  double minX = vertices_[0].x;
  double maxY = vertices_[0].y;
  double minY = vertices_[0].y;

  for (int i = 0; i < count_; i++)
  {
    maxX = std::max(maxX, vertices_[i].x);
    minX = std::min(minX, vertices_[i].x);
    maxY = std::max(maxY, vertices_[i].y);
    minY = std::min(minY, vertices_[i].y);
  }

  const double width = (maxY - minY);
  const double height = (maxX - minX);
  const altoukhov::point_t position = {(minX + (width / 2)), (minY + (height / 2))};

  return {position, width, height};
}

altoukhov::point_t altoukhov::Polygon::getCenter() const
{
  if (!vertices_)
  {
    throw std::logic_error("object has been moved");
  }
  
  altoukhov::point_t center = {0, 0};
  for (int i = 0; i < count_; i++)
  {
    center.x += vertices_[i].x;
    center.y += vertices_[i].y;
  }
  return {(center.x / count_), (center.y / count_)};
}

double altoukhov::Polygon::getAngle() const
{
  return angle_;
}

void altoukhov::Polygon::move(const altoukhov::point_t& point)
{
  if (!vertices_)
  {
    throw std::logic_error("object has been moved");
  }

  const altoukhov::point_t center = getCenter();
  const double dx = (point.x - center.x);
  const double dy = (point.y - center.y);
  move(dx, dy);
}

void altoukhov::Polygon::move(double dx, double dy)
{
  if (!vertices_)
  {
    throw std::logic_error("object has been moved");
  }

  for (int i = 0; i < count_; i++)
  {
    vertices_[i].x += dx;
    vertices_[i].y += dy;
  }
}

void altoukhov::Polygon::scale(double ratio)
{
  if (!vertices_)
  {
    throw std::logic_error("object has been moved");
  }

  if (ratio <= 0)
  {
    throw std::invalid_argument("invalid scaling ratio value");
  }

  altoukhov::point_t center = getCenter();

  for (int i = 0; i < count_; i++)
  {
    vertices_[i].x = center.x + ((vertices_[i].x - center.x) * ratio);
    vertices_[i].y = center.y + ((vertices_[i].y - center.y) * ratio);
  }
}

void altoukhov::Polygon::rotate(double angle)
{
  if (!vertices_)
  {
    throw std::logic_error("object has been moved");
  }
  
  for (int i = 0; i < count_; i++)
  {
    vertices_[i] = getRotatedPoint(getCenter(), vertices_[i], angle);
  }
}

bool altoukhov::Polygon::isConvex() const
{
  altoukhov::point_t prev = {0, 0};
  altoukhov::point_t next = {0, 0};

  for (int i = 0; i <= (count_ - 2); i++)
  {
    prev.x = vertices_[i + 1].x - vertices_[i].x;
    prev.y = vertices_[i + 1].y - vertices_[i].y;
    
    if (i == (count_ - 2))
    {
      next.x = vertices_[0].x - vertices_[i + 1].x;
      next.x = vertices_[0].x - vertices_[i + 1].x;
    }
    else
    {
      next.x = vertices_[i + 2].x - vertices_[i + 1].x;
      next.x = vertices_[i + 2].x - vertices_[i + 1].x;
    }
    
    if (((prev.x * next.y) - (prev.y * next.x)) > 0)
    {
      return false;
    }
  }
  return true;
}
