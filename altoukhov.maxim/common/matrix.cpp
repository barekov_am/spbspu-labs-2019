#include "matrix.hpp"
#include <stdexcept>
#include <utility>

altoukhov::Matrix::Matrix() :
  rows_(0),
  cols_(0)
{
}

altoukhov::Matrix::Matrix(size_t rows, size_t cols):
  list_(std::make_unique<shape_ptr[]>(rows * cols)),
  rows_(rows),
  cols_(cols)
{
  for (size_t i = 0; i < (rows_ * cols_); i++)
  {
    list_[i] = nullptr;
  }
}

altoukhov::Matrix::Matrix(const altoukhov::Matrix& other) :
  list_(std::make_unique<shape_ptr[]>(other.rows_ * other.cols_)),
  rows_(other.rows_),
  cols_(other.cols_)
{
  for (size_t i = 0; i < (rows_ * cols_); i++)
  {
    list_[i] = other.list_[i];
  }
}

altoukhov::Matrix::Matrix(altoukhov::Matrix&& other) :
  list_(std::move(other.list_)),
  rows_(other.rows_),
  cols_(other.cols_)
{
  other.rows_ = 0;
  other.cols_ = 0;
}

altoukhov::Matrix& altoukhov::Matrix::operator = (const altoukhov::Matrix& other)
{
  if (this != &other)
  {
    shape_array tmp(std::make_unique<shape_ptr[]>(other.rows_ * other.cols_));
    rows_ = other.rows_;
    cols_ = other.cols_;

    for (size_t i = 0; i < (rows_ * cols_); i++)
    {
      tmp[i] = other.list_[i];
    }

    list_.swap(tmp);
  }

  return *this;
}

altoukhov::Matrix& altoukhov::Matrix::operator = (altoukhov::Matrix&& other)
{
  if (this != &other)
  {
    list_ = std::move(other.list_);
    rows_ = other.rows_;
    cols_ = other.cols_;

    other.rows_ = 0;
    other.cols_ = 0;
  }

  return *this;
}

altoukhov::Matrix::shape_ptr& altoukhov::Matrix::operator () (size_t row, size_t col) const
{
  if ((row >= rows_) || (col >= cols_))
  {
    throw std::out_of_range("index is out of range");
  }

  return list_[(row * cols_) + col];
}

bool altoukhov::Matrix::operator == (const altoukhov::Matrix& other) const
{
  if ((rows_ != other.rows_) || (cols_ != other.cols_))
  {
    return false;
  }

  for (size_t i = 0; i < (rows_ * cols_); i++)
  {
    if (list_[i] != other.list_[i])
    {
      return false;
    }
  }

  return true;
}

bool altoukhov::Matrix::operator != (const altoukhov::Matrix& other) const
{
  return !(*this == other);
}

void altoukhov::Matrix::add(shape_ptr shape, size_t row, size_t col)
{
  if ((row < rows_) && (col < cols_))
  {
    list_[(row * cols_) + col] = shape;
    return;
  }

  size_t tmpRows = ((row + 1) > rows_) ? (row + 1) : rows_;
  size_t tmpCols = ((col + 1) > cols_) ? (col + 1) : cols_;
  shape_array tmp(std::make_unique<shape_ptr[]>(tmpRows * tmpCols));

  for (size_t i = 0; i < tmpRows; i++)
  {
    for (size_t j = 0; j < tmpCols; j++)
    {
      if ((i >= rows_) || (j >= cols_))
      {
        tmp[(i * cols_) + j] = nullptr;
      }
      else
      {
        tmp[(i * cols_) + j] = list_[(i * cols_) + j];
      }
    }
  }

  tmp[(row * cols_) + col] = shape;
  list_.swap(tmp);
  rows_ = tmpRows;
  cols_ = tmpCols;
}

size_t altoukhov::Matrix::getRows() const
{
  return rows_;
}

size_t altoukhov::Matrix::getCols() const
{
  return cols_;
}
