#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(RectangleTestSuite)

  const double INACCURACY = 0.001;

  BOOST_AUTO_TEST_CASE(ImmutabilityTest)
  {
    altoukhov::Rectangle rectangle({0, 0}, 1, 1);
    const double widthBeforeMoving = rectangle.getFrameRect().width;
    const double heightBeforeMoving = rectangle.getFrameRect().height;
    const double areaBeforeMoving = rectangle.getArea();

    rectangle.move(1, 1);
    BOOST_CHECK_CLOSE(widthBeforeMoving, rectangle.getFrameRect().width, INACCURACY);
    BOOST_CHECK_CLOSE(heightBeforeMoving, rectangle.getFrameRect().height, INACCURACY);
    BOOST_CHECK_CLOSE(areaBeforeMoving, rectangle.getArea(), INACCURACY);

    rectangle.move({0, 0});
    BOOST_CHECK_CLOSE(widthBeforeMoving, rectangle.getFrameRect().width, INACCURACY);
    BOOST_CHECK_CLOSE(heightBeforeMoving, rectangle.getFrameRect().height, INACCURACY);
    BOOST_CHECK_CLOSE(areaBeforeMoving, rectangle.getArea(), INACCURACY);
  }

  BOOST_AUTO_TEST_CASE(ScalingTest)
  {
    altoukhov::Rectangle rectangle({0, 0}, 1, 1);
    const double areaBeforeScaling = rectangle.getArea();
    const double scalingRatio = 2;
    
    rectangle.scale(scalingRatio);
    BOOST_CHECK_CLOSE((areaBeforeScaling * std::pow(scalingRatio, 2)), rectangle.getArea(), INACCURACY);
  }

  BOOST_AUTO_TEST_CASE(ExceptionTest)
  {
    BOOST_CHECK_THROW(altoukhov::Rectangle({0, 0}, -1, 1), std::invalid_argument);
    BOOST_CHECK_THROW(altoukhov::Rectangle({0, 0}, 1, -1), std::invalid_argument);

    altoukhov::Rectangle rectangle({0, 0}, 1, 1);
    BOOST_CHECK_THROW(rectangle.scale(-1), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
