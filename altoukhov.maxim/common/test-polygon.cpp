#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "polygon.hpp"

BOOST_AUTO_TEST_SUITE(PolygonTestSuite)

  const double INACCURACY = 0.001;

  BOOST_AUTO_TEST_CASE(ImmutabilityTest)
  {
    altoukhov::point_t vertices[5]{{0, 2}, {2, 0}, {1, -2}, {-1, -2}, {0, -2}};
    altoukhov::Polygon polygon(vertices, 5);
    const double widthBeforeMoving = polygon.getFrameRect().width;
    const double heightBeforeMoving = polygon.getFrameRect().height;
    const double areaBeforeMoving = polygon.getArea();

    polygon.move(1, 1);
    BOOST_CHECK_CLOSE(widthBeforeMoving, polygon.getFrameRect().width, INACCURACY);
    BOOST_CHECK_CLOSE(heightBeforeMoving, polygon.getFrameRect().height, INACCURACY);
    BOOST_CHECK_CLOSE(areaBeforeMoving, polygon.getArea(), INACCURACY);

    polygon.move({0, 0});
    BOOST_CHECK_CLOSE(widthBeforeMoving, polygon.getFrameRect().width, INACCURACY);
    BOOST_CHECK_CLOSE(heightBeforeMoving, polygon.getFrameRect().height, INACCURACY);
    BOOST_CHECK_CLOSE(areaBeforeMoving, polygon.getArea(), INACCURACY);
  }

  BOOST_AUTO_TEST_CASE(ScalingTest)
  {
    altoukhov::point_t vertices[5]{{0, 2}, {2, 0}, {1, -2}, {-1, -2}, {0, -2}};
    altoukhov::Polygon polygon(vertices, 5);
    const double areaBeforeScaling = polygon.getArea();
    const double scalingRatio = 2;

    polygon.scale(scalingRatio);
    BOOST_CHECK_CLOSE((areaBeforeScaling * std::pow(scalingRatio, 2)), polygon.getArea(), INACCURACY);
  }

  BOOST_AUTO_TEST_CASE(ExceptionTest)
  {
    altoukhov::point_t convexPolygonVertices[5]{{0, 2}, {2, 0}, {1, -2}, {-1, -2}, {0, -2}};
    altoukhov::point_t nonconvexPolygonVertices[4]{{-2, 1}, {2, 0}, {0, -1}, {-1, 1}};

    BOOST_CHECK_THROW(altoukhov::Polygon(nullptr, 5), std::invalid_argument);
    BOOST_CHECK_THROW(altoukhov::Polygon(convexPolygonVertices, 2), std::invalid_argument);
    BOOST_CHECK_THROW(altoukhov::Polygon(nonconvexPolygonVertices, 5), std::invalid_argument);

    altoukhov::Polygon polygon(convexPolygonVertices, 5);
    BOOST_CHECK_THROW(polygon.scale(-1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    altoukhov::point_t vertices[5]{{0, 2}, {2, 0}, {1, -2}, {-1, -2}, {0, -2}};
    altoukhov::Polygon polygonOriginal(vertices, 5);
    altoukhov::Polygon polygonCopy(polygonOriginal);

    BOOST_CHECK_EQUAL(polygonOriginal.getArea(), polygonCopy.getArea());
  }

  BOOST_AUTO_TEST_CASE(CopyOperatorTest)
  {
    altoukhov::point_t vertices[5]{{0, 2}, {2, 0}, {1, -2}, {-1, -2}, {0, -2}};
    altoukhov::Polygon polygonOriginal(vertices, 5);
    altoukhov::Polygon polygonCopy = polygonOriginal;

    BOOST_CHECK_EQUAL(polygonOriginal.getArea(), polygonCopy.getArea());
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    altoukhov::point_t vertices[5]{{0, 2}, {2, 0}, {1, -2}, {-1, -2}, {0, -2}};
    altoukhov::Polygon polygonOriginal(vertices, 5);

    const double originalArea = polygonOriginal.getArea();
    altoukhov::Polygon polygonMove(std::move(polygonOriginal));

    BOOST_CHECK_EQUAL(polygonMove.getArea(), originalArea);
    BOOST_CHECK_THROW(polygonOriginal.getArea(), std::logic_error);
  }

  BOOST_AUTO_TEST_CASE(MoveOperatorTest)
  {
    altoukhov::point_t vertices[5]{{0, 2}, {2, 0}, {1, -2}, {-1, -2}, {0, -2}};
    altoukhov::Polygon polygonOriginal(vertices, 5);

    const double originalArea = polygonOriginal.getArea();
    altoukhov::Polygon polygonMove = std::move(polygonOriginal);

    BOOST_CHECK_EQUAL(polygonMove.getArea(), originalArea);
    BOOST_CHECK_THROW(polygonOriginal.getArea(), std::logic_error);
  }

BOOST_AUTO_TEST_SUITE_END()
