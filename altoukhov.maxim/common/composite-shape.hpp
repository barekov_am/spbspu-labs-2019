#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace altoukhov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape&);
    CompositeShape(CompositeShape&&);
    ~CompositeShape() = default;

    CompositeShape& operator = (const CompositeShape&);
    CompositeShape& operator = (CompositeShape&&);
    shape_ptr operator [] (size_t) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    point_t getCenter() const override;
    double getAngle() const override;
    void move(const point_t&) override;
    void move(double, double) override;
    void scale(double) override;
    void rotate(double) override;

    size_t getSize() const;
    shape_array getElements() const;
    void add(shape_ptr);
    void remove(size_t);

  private:
    shape_array shapes_;
    size_t count_;
    double angle_;
  };
}

#endif
