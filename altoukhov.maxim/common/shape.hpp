#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"
#define _USE_MATH_DEFINES
#include <cmath>
#include <memory>

namespace altoukhov
{
  class Shape
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    using shape_array = std::unique_ptr<shape_ptr[]>;

    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual point_t getCenter() const = 0;
    virtual double getAngle() const = 0;
    virtual void move(const point_t&) = 0;
    virtual void move(double, double) = 0;
    virtual void scale(double) = 0;
    virtual void rotate(double) = 0;
  };
}

#endif
