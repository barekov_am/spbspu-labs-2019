#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>

#include "matrix.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(MatrixTestSuite)

  BOOST_AUTO_TEST_CASE(ExceptionTest)
  {
    altoukhov::Matrix matrix(2, 2);

    BOOST_CHECK_THROW(matrix(3, 3), std::out_of_range);
    BOOST_CHECK_THROW(matrix(-1, -1), std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    altoukhov::Matrix matrixOriginal(2, 2);
    altoukhov::Matrix matrixCopy(matrixOriginal);

    BOOST_CHECK(matrixOriginal == matrixCopy);
  }

  BOOST_AUTO_TEST_CASE(CopyOperatorTest)
  {
    altoukhov::Matrix matrixOriginal(2, 2);
    altoukhov::Matrix matrixCopy = matrixOriginal;

    BOOST_CHECK(matrixOriginal == matrixCopy);
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    altoukhov::Matrix matrixOriginal(2, 2);
    size_t matrixOriginalRows = matrixOriginal.getRows();
    size_t matrixOriginalCols = matrixOriginal.getCols();
    altoukhov::Matrix matrixMove(std::move(matrixOriginal));

    BOOST_CHECK_EQUAL(matrixMove.getRows(), matrixOriginalRows);
    BOOST_CHECK_EQUAL(matrixMove.getCols(), matrixOriginalCols);
    BOOST_CHECK_EQUAL(matrixOriginal.getRows(), 0);
    BOOST_CHECK_EQUAL(matrixOriginal.getCols(), 0);
  }

  BOOST_AUTO_TEST_CASE(MoveOperatorTest)
  {
    altoukhov::Matrix matrixOriginal(2, 2);
    size_t matrixOriginalRows = matrixOriginal.getRows();
    size_t matrixOriginalCols = matrixOriginal.getCols();
    altoukhov::Matrix matrixMove = std::move(matrixOriginal);

    BOOST_CHECK_EQUAL(matrixMove.getRows(), matrixOriginalRows);
    BOOST_CHECK_EQUAL(matrixMove.getCols(), matrixOriginalCols);
    BOOST_CHECK_EQUAL(matrixOriginal.getRows(), 0);
    BOOST_CHECK_EQUAL(matrixOriginal.getCols(), 0);
  }

  BOOST_AUTO_TEST_CASE(AddMethodTest)
  {
    altoukhov::Matrix matrixLhs(2, 2);
    altoukhov::Matrix matrixRhs(2, 2);
    altoukhov::CompositeShape composition;
    altoukhov::Shape::shape_ptr comp_ptr(std::make_shared<altoukhov::CompositeShape>(composition));

    matrixLhs.add(comp_ptr, 0, 1);
    BOOST_CHECK_EQUAL(matrixLhs.getRows(), matrixRhs.getRows());
    BOOST_CHECK_EQUAL(matrixLhs.getCols(), matrixRhs.getCols());

    matrixLhs.add(comp_ptr, 3, 3);
    BOOST_CHECK(matrixLhs.getRows() != matrixRhs.getRows());
    BOOST_CHECK(matrixLhs.getCols() != matrixRhs.getCols());
  }

BOOST_AUTO_TEST_SUITE_END()
