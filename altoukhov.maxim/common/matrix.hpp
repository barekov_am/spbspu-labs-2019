#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

namespace altoukhov
{
  class Matrix
  {
  public:
    using shape_ptr = Shape::shape_ptr;
    using shape_array = Shape::shape_array;

    Matrix();
    Matrix(size_t, size_t);
    Matrix(const Matrix&);
    Matrix(Matrix&&);
    ~Matrix() = default;

    Matrix& operator = (const Matrix&);
    Matrix& operator = (Matrix&&);
    shape_ptr& operator () (size_t, size_t) const;
    bool operator == (const Matrix&) const;
    bool operator != (const Matrix&) const;

    void add(shape_ptr, size_t, size_t);
    size_t getRows() const;
    size_t getCols() const;

  private:
    shape_array list_;
    size_t rows_;
    size_t cols_;
  };
}

#endif
