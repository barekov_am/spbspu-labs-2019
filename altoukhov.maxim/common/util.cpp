#include "util.hpp"
#include <stdexcept>

altoukhov::Matrix altoukhov::part(const Shape::shape_array& shapes, size_t size)
{
  if (!size)
  {
    throw std::invalid_argument("size can't be zero");
  }

  Matrix matrix(1, 1);
  matrix(0, 0) = shapes[0];

  if (!shapes[0])
  {
    throw std::invalid_argument("shape pointer must not be null");
  }

  size_t layerCount = 1;

  for (size_t i = 1; i < size; i++)
  {
    if (!shapes[i])
    {
      throw std::invalid_argument("shape pointer must not be null");
    }

    if (isIntersecting(shapes[i]->getFrameRect(), shapes[i - 1]->getFrameRect()))
    {
      matrix.add(shapes[i], layerCount++, 0);
    }
    else
    {
      size_t layerElementsCount = 0;
      for (size_t j = 0; j < matrix.getCols(); j++)
      {
        if (matrix((layerCount - 1), j) != nullptr)
        {
          layerElementsCount++;
        }
        else
        {
          break;
        }
      }
      matrix.add(shapes[i], (layerCount - 1), layerElementsCount);
    }
  }

  return matrix;
}

bool altoukhov::isIntersecting(const rectangle_t& lhs, const rectangle_t& rhs)
{
  const point_t lhsTopLft = {lhs.pos.x - lhs.width / 2, lhs.pos.y + lhs.height / 2};
  const point_t lhsBtmRgt = {lhs.pos.x + lhs.width / 2, lhs.pos.y - lhs.height / 2};
  const point_t rhsTopLft = {rhs.pos.x - rhs.width / 2, rhs.pos.y + rhs.height / 2};
  const point_t rhsBtmRgt = {rhs.pos.x + rhs.width / 2, rhs.pos.y - rhs.height / 2};

  const bool topLftCondition = (lhsTopLft.x < rhsBtmRgt.x) && (rhsBtmRgt.y < lhsTopLft.y);
  const bool btmRgtCondition = (rhsTopLft.x < lhsBtmRgt.x) && (lhsBtmRgt.y < rhsTopLft.y);

  return (topLftCondition && btmRgtCondition);
}

altoukhov::point_t altoukhov::getRotatedPoint(const altoukhov::point_t& center, const altoukhov::point_t& point, double angle)
{
  const double cosA = std::fabs(std::cos(angle * M_PI / 180));
  const double sinA = std::fabs(std::sin(angle * M_PI / 180));
  const double dx = (point.x - center.x) * cosA - (point.y - center.y) * sinA;
  const double dy = (point.x - center.x) * sinA + (point.y - center.y) * cosA;

  return {(center.x + dx), (center.y + dy)};
}

