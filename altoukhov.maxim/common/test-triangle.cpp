#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(TriangleTestSuite)

  const double INACCURACY = 0.001;

  BOOST_AUTO_TEST_CASE(ImmutabilityTest)
  {
    altoukhov::Triangle triangle({0, 0}, {1, 1}, {2, 0});
    const double widthBeforeMoving = triangle.getFrameRect().width;
    const double heightBeforeMoving = triangle.getFrameRect().height;
    const double areaBeforeMoving = triangle.getArea();

    triangle.move(1, 1);
    BOOST_CHECK_CLOSE(widthBeforeMoving, triangle.getFrameRect().width, INACCURACY);
    BOOST_CHECK_CLOSE(heightBeforeMoving, triangle.getFrameRect().height, INACCURACY);
    BOOST_CHECK_CLOSE(areaBeforeMoving, triangle.getArea(), INACCURACY);

    triangle.move({0, 0});
    BOOST_CHECK_CLOSE(widthBeforeMoving, triangle.getFrameRect().width, INACCURACY);
    BOOST_CHECK_CLOSE(heightBeforeMoving, triangle.getFrameRect().height, INACCURACY);
    BOOST_CHECK_CLOSE(areaBeforeMoving, triangle.getArea(), INACCURACY);
  }

  BOOST_AUTO_TEST_CASE(ScalingTest)
  {
    altoukhov::Triangle triangle({0, 0}, {1, 1}, {2, 0});
    const double areaBeforeScaling = triangle.getArea();
    const double scalingRatio = 2;

    triangle.scale(scalingRatio);
    BOOST_CHECK_CLOSE((areaBeforeScaling * std::pow(scalingRatio, 2)), triangle.getArea(), INACCURACY);
  }

  BOOST_AUTO_TEST_CASE(ExceptionTest)
  {
    BOOST_CHECK_THROW(altoukhov::Triangle({0, 0}, {0, 0}, {1, 0}), std::invalid_argument);

    altoukhov::Triangle triangle({0, 0}, {1, 1}, {2, 0});
    BOOST_CHECK_THROW(triangle.scale(-1), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
