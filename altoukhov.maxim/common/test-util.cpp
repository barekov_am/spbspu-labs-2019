#include <stdexcept>
#include <cmath>
#include <memory>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "util.hpp"

BOOST_AUTO_TEST_SUITE(PartitionTestSuite)

  using shape_array = altoukhov::Shape::shape_array;
  using shape_ptr = altoukhov::Shape::shape_ptr;

  BOOST_AUTO_TEST_CASE(CorrectPartitionTest)
  {
    shape_array shapes = std::make_unique<shape_ptr[]>(3);

    shape_ptr rectPtr = std::make_shared<altoukhov::Rectangle>(altoukhov::Rectangle({0, 0}, 2, 2));
    shape_ptr cirPtr = std::make_shared<altoukhov::Circle>(altoukhov::Circle({2, 1}, 1));
    shape_ptr triPtr = std::make_shared<altoukhov::Triangle>(altoukhov::Triangle({1, 1}, {2, 2}, {2, 0}));

    shapes[0] = rectPtr;
    shapes[1] = cirPtr;
    shapes[2] = triPtr;

    // +---+---+
    // | 0 | 1 |
    // | 2 | x |
    // +---+---+

    altoukhov::Matrix matrix = altoukhov::part(shapes, 3);
    BOOST_CHECK_EQUAL(matrix.getRows(), 2);
    BOOST_CHECK_EQUAL(matrix.getCols(), 2);
    BOOST_CHECK(matrix(0, 0) == rectPtr);
    BOOST_CHECK(matrix(0, 1) == cirPtr);
    BOOST_CHECK(matrix(1, 0) == triPtr);
    BOOST_CHECK(matrix(1, 1) == nullptr);
  }

BOOST_AUTO_TEST_CASE(ExceptionTest)
{
  shape_array shapes = std::make_unique<shape_ptr[]>(2);
  BOOST_CHECK_THROW(altoukhov::part(shapes, 0), std::invalid_argument);
  BOOST_CHECK_THROW(altoukhov::part(shapes, 2), std::invalid_argument);

  shapes[0] = std::make_shared<altoukhov::Rectangle>(altoukhov::Rectangle({ 0, 0 }, 1, 1));
  BOOST_CHECK_THROW(altoukhov::part(shapes, 2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
