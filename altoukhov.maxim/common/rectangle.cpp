#include "rectangle.hpp"
#include <stdexcept>

altoukhov::Rectangle::Rectangle(const altoukhov::point_t& pos, double width, double height):
  pos_{pos},
  width_{width},
  height_{height},
  angle_{0}
{
  if ((width <= 0) || (height <= 0))
  {
    throw std::invalid_argument("invalid width or height value");
  }
}

double altoukhov::Rectangle::getArea() const
{
  return (width_ * height_);
}

altoukhov::rectangle_t altoukhov::Rectangle::getFrameRect() const
{
  const double sinA = std::fabs(std::sin(angle_ * M_PI / 180));
  const double cosA = std::fabs(std::cos(angle_ * M_PI / 180));
  const double width = height_ * sinA + width_ * cosA;
  const double height = height_ * cosA + width_ * sinA;

  return {pos_, width, height};
}

altoukhov::point_t altoukhov::Rectangle::getCenter() const
{
  return pos_;
}

double altoukhov::Rectangle::getAngle() const
{
  return angle_;
}

void altoukhov::Rectangle::move(const altoukhov::point_t& point)
{
  pos_ = point;
}

void altoukhov::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void altoukhov::Rectangle::scale(double ratio)
{
  if (ratio <= 0)
  {
    throw std::invalid_argument("invalid scaling ratio value");
  }
  width_ *= ratio;
  height_ *= ratio;
}

void altoukhov::Rectangle::rotate(double angle)
{
  angle_ += angle;
  angle_ = (angle_ > 0) ? (std::fmod(angle_, 2 * M_PI)) : (2 * M_PI + std::fmod(angle_, 2 * M_PI));
}
