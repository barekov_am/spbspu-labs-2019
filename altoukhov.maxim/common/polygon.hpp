#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "shape.hpp"

namespace altoukhov
{
  class Polygon : public Shape
  {
  public:
    Polygon(altoukhov::point_t*, int);
    Polygon(const altoukhov::Polygon&);
    Polygon(altoukhov::Polygon&&);
    ~Polygon();

    Polygon& operator = (const altoukhov::Polygon&);
    Polygon& operator = (altoukhov::Polygon&&);

    double getArea() const override;
    altoukhov::rectangle_t getFrameRect() const override;
    altoukhov::point_t getCenter() const override;
    double getAngle() const override;
    void move(const altoukhov::point_t&) override;
    void move(double, double) override;
    void scale(double) override;
    void rotate(double) override;

  private:
    altoukhov::point_t* vertices_;
    int count_;
    double angle_;

    bool isConvex() const;
  };
}

#endif
