#include "composite-shape.hpp"
#include "util.hpp"
#include <stdexcept>
#include <utility>
#include <algorithm>

altoukhov::CompositeShape::CompositeShape():
  count_{0},
  angle_{0}
{
}

altoukhov::CompositeShape::CompositeShape(const CompositeShape& other):
  shapes_{std::make_unique<shape_ptr[]>(other.count_)},
  count_{other.count_},
  angle_{other.angle_}
{
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i] = other.shapes_[i];
  }
}

altoukhov::CompositeShape::CompositeShape(CompositeShape&& other):
  shapes_{std::move(other.shapes_)},
  count_{other.count_},
  angle_{other.angle_}
{
  other.count_ = 0;
  other.angle_ = 0; 
}

altoukhov::CompositeShape& altoukhov::CompositeShape::operator = (const CompositeShape& other)
{
  if (this != &other)
  {
    shape_array tmp(std::make_unique<shape_ptr[]>(other.count_));
    count_ = other.count_;
    angle_ = other.angle_;

    for (size_t i = 0; i < count_; i++)
    {
      tmp[i] = other.shapes_[i];
    }

    shapes_.swap(tmp);
  }

  return *this;
}

altoukhov::CompositeShape& altoukhov::CompositeShape::operator = (CompositeShape&& other)
{
  if (this != &other)
  {
    shapes_ = std::move(other.shapes_);
    count_ = other.count_;
    angle_ = other.angle_;

    other.count_ = 0;
    other.angle_ = 0;
  }

  return *this;
}

altoukhov::CompositeShape::shape_ptr altoukhov::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("index is out of range");
  }

  return shapes_[index];
}

double altoukhov::CompositeShape::getArea() const
{
  if (!shapes_)
  {
    throw std::logic_error("object doesn't exist");
  }

  double area = 0;

  for (size_t i = 0; i < count_; i++)
  {
    area += shapes_[i]->getArea();
  }

  return area;
}

altoukhov::rectangle_t altoukhov::CompositeShape::getFrameRect() const
{
  if (!count_)
  {
    throw std::logic_error("object doesn't exist");
  }

  altoukhov::rectangle_t rect = shapes_[0]->getFrameRect();
  double maxY = rect.pos.y + rect.height / 2;
  double minY = rect.pos.y - rect.height / 2;
  double maxX = rect.pos.x + rect.width / 2;
  double minX = rect.pos.x - rect.width / 2;

  for (size_t i = 1; i < count_; i++)
  {
    rect = shapes_[i]->getFrameRect();
    maxY = std::max(maxY, rect.pos.y + rect.height / 2);
    minY = std::min(minY, rect.pos.y - rect.height / 2);
    maxX = std::max(maxX, rect.pos.x + rect.width / 2);
    minX = std::min(minX, rect.pos.x - rect.width / 2);
  }

  return {{(maxX + minX) / 2, (maxY + minY) / 2}, maxX - minX, maxY - minY};
}

altoukhov::point_t altoukhov::CompositeShape::getCenter() const
{
  return getFrameRect().pos;
}

double altoukhov::CompositeShape::getAngle() const
{
  return angle_;
}

void altoukhov::CompositeShape::move(const altoukhov::point_t& point)
{
  const altoukhov::point_t center = getCenter();
  const double dx = point.x - center.x;
  const double dy = point.y - center.y;

  move(dx, dy);
}

void altoukhov::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void altoukhov::CompositeShape::scale(double ratio)
{
  if (!count_)
  {
    throw std::logic_error("object doesn't exist");
  }

  if (ratio <= 0)
  {
    throw std::invalid_argument("invalid scaling ratio value");
  }

  altoukhov::point_t center = getCenter();
  
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->scale(ratio);
    altoukhov::point_t shapeCenter = shapes_[i]->getCenter();
    shapes_[i]->move((shapeCenter.x - center.x) * (ratio - 1), (shapeCenter.y - center.y) * (ratio - 1));
  }
}

void altoukhov::CompositeShape::rotate(double angle)
{
  angle_ += angle;
  angle_ = (angle_ > 0) ? (std::fmod(angle_, 2 * M_PI)) : (2 * M_PI + std::fmod(angle_, 2 * M_PI));

  const double sinA = std::fabs(std::sin(angle * M_PI / 180));
  const double cosA = std::fabs(std::cos(angle * M_PI / 180));
  const altoukhov::point_t center = getCenter();

  for (size_t i = 0; i < count_; i++)
  {
    const double oldX = shapes_[i]->getCenter().x - center.x;
    const double oldY = shapes_[i]->getCenter().y - center.y;

    const double newX = oldX * std::fabs(cosA) - oldY * std::fabs(sinA);
    const double newY = oldX * std::fabs(sinA) + oldY * std::fabs(cosA);

    shapes_[i]->move({center.x + newX, center.y + newY});
    shapes_[i]->rotate(angle);
  }
}

size_t altoukhov::CompositeShape::getSize() const
{
  return count_;
}

void altoukhov::CompositeShape::add(shape_ptr shape)
{
  if (!shape)
  {
    throw std::invalid_argument("shape pointer must not be null");
  }

  shape_array tmp(std::make_unique<shape_ptr[]>(count_ + 1));
  for (size_t i = 0; i < count_; i++)
  {
    tmp[i] = shapes_[i];
  }

  tmp[count_] = shape;
  count_++;
  shapes_.swap(tmp);
}

void altoukhov::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("index is out of range");
  }

  for (size_t i = index; i < count_ - 1; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  count_--;
}

altoukhov::CompositeShape::shape_array altoukhov::CompositeShape::getElements() const
{
  shape_array tmp(std::make_unique<shape_ptr[]>(count_));

  for (size_t i = 0; i < count_; i++)
  {
    tmp[i] = shapes_[i];
  }

  return tmp;
}

