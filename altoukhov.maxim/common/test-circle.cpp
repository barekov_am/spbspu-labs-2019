#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(CircleTestSuite)

  const double INACCURACY = 0.001;

  BOOST_AUTO_TEST_CASE(ImmutabilityTest)
  {
    altoukhov::Circle circle({0, 0}, 1);
    const double widthBeforeMoving = circle.getFrameRect().width;
    const double heightBeforeMoving = circle.getFrameRect().height;
    const double areaBeforeMoving = circle.getArea();

    circle.move(1, 1);
    BOOST_CHECK_CLOSE(widthBeforeMoving, circle.getFrameRect().width, INACCURACY);
    BOOST_CHECK_CLOSE(heightBeforeMoving, circle.getFrameRect().height, INACCURACY);
    BOOST_CHECK_CLOSE(areaBeforeMoving, circle.getArea(), INACCURACY);

    circle.move({0, 0});
    BOOST_CHECK_CLOSE(widthBeforeMoving, circle.getFrameRect().width, INACCURACY);
    BOOST_CHECK_CLOSE(heightBeforeMoving, circle.getFrameRect().height, INACCURACY);
    BOOST_CHECK_CLOSE(areaBeforeMoving, circle.getArea(), INACCURACY);
  }

  BOOST_AUTO_TEST_CASE(ScalingTest)
  {
    altoukhov::Circle circle({0, 0}, 1);
    const double areaBeforeScaling = circle.getArea();
    const double scalingRatio = 2;

    circle.scale(scalingRatio);
    BOOST_CHECK_CLOSE((areaBeforeScaling * std::pow(scalingRatio, 2)), circle.getArea(), INACCURACY);
  }

  BOOST_AUTO_TEST_CASE(ExceptionTest)
  {
    BOOST_CHECK_THROW(altoukhov::Circle({0, 0}, -1), std::invalid_argument);

    altoukhov::Circle circle({0, 0}, 1);
    BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
