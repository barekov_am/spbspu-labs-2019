#ifndef UTIL_HPP
#define UTIL_HPP

#include "shape.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

namespace altoukhov
{
  Matrix part(const Shape::shape_array& arr, size_t size);
  bool isIntersecting(const rectangle_t& lhs, const rectangle_t& rhs);
  point_t getRotatedPoint(const point_t& center, const point_t& point, double angle);
}

#endif
