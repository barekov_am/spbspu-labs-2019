#ifndef INTERPRETER_HPP
#define INTERPRETER_HPP

#include <string>
#include <functional>
#include <unordered_map>
#include <algorithm>

#include "PriorityQueue.hpp"

class Interpreter
{
public:
  using StringQueue = PriorityQueue<std::string>;
  using Executable  = std::function<void(StringQueue&)>;

  static Executable interpret(std::string str);

private:
  using HandlerPtr = Executable (*)(std::string&);

  static const std::unordered_map<std::string, HandlerPtr> handlers_;
  static const Executable defaultExec_;

  // Command handlers (arg parsing AND executable construction)
  static Executable handleAdd(std::string& str);
  static Executable handleGet(std::string& str);
  static Executable handleAccelerate(std::string& str);

  // Helper functions: queries
  inline static bool isBlank(char ch)                   { return isblank(static_cast<unsigned char>(ch));            }
  inline static bool isNotBlank(char ch)                { return !isBlank(ch);                                       }
  inline static bool isAllBlank(const std::string& str) { return std::all_of(str.cbegin(), str.cend(), isBlank);     }
  inline static bool isPriority(const std::string& arg) { return (arg == "low" || arg == "normal" || arg == "high"); }

  // Helper functions: modifiers
  static std::string extractNextToken(std::string& str);
  static void stripBeginBlank(std::string& str);

  // Helper functions: conversions
  static StringQueue::PriorityType toPriority(const std::string& arg);
};

#endif
