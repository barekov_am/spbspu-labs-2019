#ifndef PRIORITY_QUEUE_HPP
#define PRIORITY_QUEUE_HPP

#include <list>

/////////////////////////////////////////////////////////////
//                        Interface                        //
/////////////////////////////////////////////////////////////

template <typename T>
class PriorityQueue
{
public:
  enum class PriorityType
  {
    low,
    normal,
    high
  };

  bool push(const T& val, PriorityType prio);
  template <typename HandlerT>
  bool handleFront(HandlerT handler);
  void accelerate();
  bool isEmpty() const;

private:
  std::list<T> high_;
  std::list<T> normal_;
  std::list<T> low_;
};

/////////////////////////////////////////////////////////////
//                     Implementation                      //
/////////////////////////////////////////////////////////////

template <typename T>
bool PriorityQueue<T>::push(const T& val, PriorityType prio)
{
  switch (prio)
  {
  case (PriorityType::high):
    high_.push_back(val);
    return true;
  case (PriorityType::normal):
    normal_.push_back(val);
    return true;
  case (PriorityType::low):
    low_.push_back(val);
    return true;
  default:
    return false;
  }
}

template <typename T>
template <typename HandlerT>
bool PriorityQueue<T>::handleFront(HandlerT handler)
{
  if (!high_.empty())
  {
    handler(high_.front());
    high_.pop_front();
    return true;
  }
  if (!normal_.empty())
  {
    handler(normal_.front());
    normal_.pop_front();
    return true;
  }
  if (!low_.empty())
  {
    handler(low_.front());
    low_.pop_front();
    return true;
  }
  return false;
}

template <typename T>
void PriorityQueue<T>::accelerate()
{
  high_.splice(high_.end(), low_);
}

template <typename T>
bool PriorityQueue<T>::isEmpty() const
{
  return (high_.empty() && normal_.empty() && low_.empty());
}

#endif
