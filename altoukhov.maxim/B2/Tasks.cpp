#include <stdexcept>
#include <string>
#include <list>
#include <iostream>

#include "UtilityFunctions.hpp"
#include "PriorityQueue.hpp"
#include "Interpreter.hpp"

/////////////////////////////////////////////////////////////
//                         Task #1                         //
/////////////////////////////////////////////////////////////

void doTaskOne()
{
  PriorityQueue<std::string> queue;
  std::string line;

  while (std::getline(std::cin, line))
  {
    Interpreter::interpret(line)(queue);
  }
}

/////////////////////////////////////////////////////////////
//                         Task #2                         //
/////////////////////////////////////////////////////////////

void doTaskTwo()
{
  const int minVal      = 1;
  const int maxVal      = 20;
  const int maxListSize = 20;

  std::list<int> ls;
  int tmp = 0;

  while (std::cin >> tmp)
  {
    if ((tmp < minVal) || (tmp > maxVal))
    {
      throw std::invalid_argument("Error: invalid input");
    }

    ls.push_back(tmp);
    if (ls.size() > maxListSize)
    {
      throw std::runtime_error("Error: list size exceeds allowed size");
    }
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Error: invalid input");
  }

  printMirrored(ls.begin(), ls.end(), std::cout);
  std::cout << '\n';
}
