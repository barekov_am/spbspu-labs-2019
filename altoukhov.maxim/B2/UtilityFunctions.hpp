#ifndef UTILITY_FUNCTIONS_HPP
#define UTILITY_FUNCTIONS_HPP

#include <ostream>
#include <iterator>

template <typename BidirIterT>
void printMirrored(BidirIterT    begin,
                   BidirIterT    end,
                   std::ostream& out,
                   char          delim = ' ')
{
  // If range is even-sized, stop condition is begin == end; plus this checks for an empty range
  if (begin == end)
  {
    return;
  }

  // If range is odd-sized, stop condition is begin adjacent to end
  if (begin == std::prev(end))
  {
    out << *begin;
    return;
  }

  out << *begin++ << delim << *(--end) << delim;
  printMirrored(begin, end, out, delim);
}

#endif
