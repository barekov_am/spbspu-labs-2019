#include "Interpreter.hpp"

#include <iostream>
#include <algorithm>
#include <cctype>

Interpreter::Executable Interpreter::interpret(std::string str)
{
  if (str.empty())
  {
    return Executable();
  }

  std::string cmdName = std::move(extractNextToken(str));
  if (handlers_.find(cmdName) == handlers_.cend())
  {
    return defaultExec_;
  }

  return handlers_.at(cmdName)(str);
}

const std::unordered_map<std::string, Interpreter::HandlerPtr> Interpreter::handlers_
{
  { "add"        , &handleAdd        },
  { "get"        , &handleGet        },
  { "accelerate" , &handleAccelerate }
};

const Interpreter::Executable Interpreter::defaultExec_ = [](StringQueue&)
{
  std::cout << "<INVALID COMMAND>\n";
};

//////////////////////////////////////////////////////////
//                       Handlers                       //
//////////////////////////////////////////////////////////

Interpreter::Executable Interpreter::handleAdd(std::string& str)
{
  std::string argPrio = std::move(extractNextToken(str));
  // after extracting command name and prio, everything that is left in that string will be the argData
  stripBeginBlank(str);

  if (isPriority(argPrio) && !str.empty())
  {
    auto prio = toPriority(argPrio);
    return [prio, str](StringQueue& queue)
    {
      queue.push(str, prio);
    };
  }

  return defaultExec_;
}

Interpreter::Executable Interpreter::handleGet(std::string& str)
{
  if (!isAllBlank(str)) // "get" has to take no parameters to be valid
  {
    return defaultExec_;
  }

  return [](StringQueue& queue)
  {
    if (queue.isEmpty())
    {
      std::cout << "<EMPTY>\n";
    }
    else
    {
      queue.handleFront([](const std::string& data)
      {
        std::cout << data << '\n';
      });
    }
  };
}

Interpreter::Executable Interpreter::handleAccelerate(std::string& str)
{
  if (!isAllBlank(str)) // "accelerate" has to take no parameters to be valid
  {
    return defaultExec_;
  }

  return [](StringQueue& queue)
  {
    queue.accelerate();
  };
}

//////////////////////////////////////////////////////////
//              Helper functions: modifiers             //
//////////////////////////////////////////////////////////

std::string Interpreter::extractNextToken(std::string& str)
{
  if (isAllBlank(str) || str.empty())
  {
    return str;
  }

  // First non-blank char in [str.cbegin, str.cend) is a token beginning
  // First blank char in [tokenBegin, str.cend) is a token ending
  std::string::const_iterator tokenBegin = std::find_if(str.cbegin(), str.cend(), isNotBlank);
  std::string::const_iterator tokenEnd   = std::find_if(tokenBegin, str.cend(), isBlank);
  std::string token(tokenBegin, tokenEnd);

  // if we want to remove token only, we erase in [tokenBegin, tokenEnd)
  // if we want to remove token AND skipped blank chars, we erase in [str.cbegin, tokenEnd)
  str.erase(str.cbegin(), tokenEnd);
  return token;
}

void Interpreter::stripBeginBlank(std::string& str)
{
  std::string::const_iterator firstNonBlank = std::find_if(str.cbegin(), str.cend(), isNotBlank);
  str.erase(str.cbegin(), firstNonBlank);
}

//////////////////////////////////////////////////////////
//              Helper functions: conversions           //
//////////////////////////////////////////////////////////

Interpreter::StringQueue::PriorityType Interpreter::toPriority(const std::string& str)
{
  if (str == "low")
  {
    return StringQueue::PriorityType::low;
  }
  if (str == "normal")
  {
    return StringQueue::PriorityType::normal;
  }
  if (str == "high")
  {
    return StringQueue::PriorityType::high;
  }
  throw std::invalid_argument("Error: invalid PriorityType value passed to Interpreter::toPriority()");
}
