#ifndef PHONE_BOOK_HPP
#define PHONE_BOOK_HPP

#include <string>
#include <list>

class PhoneBook
{
public:
  struct record_t
  {
    std::string name;
    std::string number;
  };

  using iterator = std::list<record_t>::iterator;
  using const_iterator = std::list<record_t>::const_iterator;

  void insertAfter(iterator pos, const std::string& name, const std::string& number);
  void insertBefore(iterator pos, const std::string& name, const std::string& number);
  void append(const std::string& name, const std::string& number);
  void remove(iterator pos);

  iterator begin();
  const_iterator cbegin() const noexcept;
  iterator end();
  const_iterator cend() const noexcept;
  bool empty() const noexcept;
  size_t size() const noexcept;

private:
  std::list<record_t> records_;
};

#endif
