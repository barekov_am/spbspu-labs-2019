#include "Tasks.hpp"

#include <iostream>
#include <utility>

#include "FactorialContainer.hpp"
#include "PhoneBookManager.hpp"
#include "Interpreter.hpp"

void doTaskOne()
{
  PhoneBookManager pbm;
  std::string cmd;

  while (std::getline(std::cin, cmd))
  {
    std::cout << Interpreter::interpret(cmd)(pbm);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("input failed");
  }
}

void doTaskTwo()
{
  FactorialContainer container(10);

  std::copy(container.begin(), container.end(), std::ostream_iterator<FactorialContainer::value_type>(std::cout, " "));
  std::cout << '\n';

  std::copy(container.rbegin(), container.rend(), std::ostream_iterator<FactorialContainer::value_type>(std::cout, " "));
  std::cout << '\n';
}
