#include <iostream>
#include <string>
#include <stdexcept>

#include "Tasks.hpp"

#define SUCCESS           0
#define INVALID_ARG_ERROR 1
#define INTERNAL_ERROR    2
#define MIN_REQUIRED_ARGC 2

int main(int argc, const char* argv[])
{
  try
  {
    if (argc < MIN_REQUIRED_ARGC)
    {
      throw std::runtime_error("too few command line arguments to run");
    }

    switch (std::stoi(argv[1]))
    {
    case (1):
      doTaskOne();
      return SUCCESS;

    case (2):
      doTaskTwo();
      return SUCCESS;

    default:
      throw std::invalid_argument("invalid task number");
    }
  }

  catch (const std::invalid_argument& exc)
  {
    std::cerr << exc.what() << '\n';
    return INVALID_ARG_ERROR;
  }

  catch (const std::exception& exc)
  {
    std::cerr << exc.what() << '\n';
    return INTERNAL_ERROR;
  }
}
