#ifndef FACTORIAL_CONTAINER_HPP
#define FACTORIAL_CONTAINER_HPP

#include <cstdint>
#include <cstddef>
#include <iterator>

class FactorialContainer
{
public:
  class iterator;

  using value_type            = std::uint64_t;
  using size_type             = std::size_t;
  using const_iterator        = iterator;
  using reverse_iterator      = std::reverse_iterator<iterator>;
  using const_reverse_iterator = reverse_iterator;

  FactorialContainer(size_type endIndex);
  FactorialContainer(size_type beginIndex, size_type endIndex);

  iterator begin() const;
  iterator end() const;
  const_iterator cbegin() const;
  const_iterator cend() const;
  reverse_iterator rbegin() const;
  reverse_iterator rend() const;
  const_reverse_iterator crbegin() const;
  const_reverse_iterator crend() const;

private:
  size_type  beginIndex_;
  size_type  endIndex_;
  value_type beginVal_;
  value_type endVal_;

  static constexpr size_type getMaxIndex();
};

class FactorialContainer::iterator :
  public std::iterator<std::bidirectional_iterator_tag,
                       FactorialContainer::value_type,
                       std::ptrdiff_t,
                       FactorialContainer::value_type*,
                       FactorialContainer::value_type> // have to specify the ref-type for reverse_iterator to work correctly
{
public:
  iterator(FactorialContainer::size_type index, value_type val);

  value_type operator*() const;
  iterator& operator++();
  iterator operator++(int);
  iterator& operator--();
  iterator operator--(int);
  bool operator==(const iterator& other) const;
  bool operator!=(const iterator& other) const;

private:
  FactorialContainer::size_type index_;
  value_type val_;
};

#endif
