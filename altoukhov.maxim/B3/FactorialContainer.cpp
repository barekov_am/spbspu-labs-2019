#include "FactorialContainer.hpp"

#include <stdexcept>
#include <limits>

////////////////////////////////////////////////
//          getMaxIndex() definition          //
////////////////////////////////////////////////

constexpr FactorialContainer::size_type FactorialContainer::getMaxIndex()
{
  size_type  n    = 0;
  value_type fact = 1;

  while (fact < (std::numeric_limits<value_type>::max() / ++n))
  {
    fact *= n;
  }

  return (n - 2);
}

////////////////////////////////////////////////
//       FactorialContainer definition        //
////////////////////////////////////////////////

FactorialContainer::FactorialContainer(size_type endIndex):
  FactorialContainer(1, endIndex)
{
}

FactorialContainer::FactorialContainer(size_type beginIndex, size_type endIndex):
  beginIndex_(beginIndex),
  endIndex_(endIndex)
{
  if (beginIndex > endIndex)
  {
    throw std::invalid_argument("ending index must not be less than beginning index");
  }

  constexpr size_type maxIndex = getMaxIndex();
  if (endIndex > maxIndex)
  {
    throw std::overflow_error("ending index causes overflow");
  }

  size_type  n    = 0;
  value_type fact = 1;

  while (n != (endIndex + 1))
  {
    if (n == beginIndex)
    {
      beginVal_ = fact;
    }
    fact *= ++n;
  }

  endVal_ = fact;
}

FactorialContainer::iterator FactorialContainer::begin() const
{
  return iterator(beginIndex_, beginVal_);
}

FactorialContainer::iterator FactorialContainer::end() const
{
  return iterator((endIndex_ + 1), endVal_);
}

FactorialContainer::iterator FactorialContainer::cbegin() const
{
  return iterator(beginIndex_, beginVal_);
}

FactorialContainer::iterator FactorialContainer::cend() const
{
  return iterator((endIndex_ + 1), endVal_);
}

FactorialContainer::reverse_iterator FactorialContainer::rbegin() const
{
  return reverse_iterator(cend());
}

FactorialContainer::reverse_iterator FactorialContainer::rend() const
{
  return reverse_iterator(cbegin());
}

FactorialContainer::const_reverse_iterator FactorialContainer::crbegin() const
{
  return const_reverse_iterator(cend());
}

FactorialContainer::const_reverse_iterator FactorialContainer::crend() const
{
  return const_reverse_iterator(cbegin());
}

////////////////////////////////////////////////
//   FactorialContainer::Iterator definition  //
////////////////////////////////////////////////

FactorialContainer::iterator::iterator(FactorialContainer::size_type index, value_type val):
  index_(index),
  val_(val)
{
}

FactorialContainer::iterator::value_type FactorialContainer::iterator::operator*() const
{
  return val_;
}

FactorialContainer::iterator& FactorialContainer::iterator::operator++()
{
  val_ *= ++index_;
  return *this;
}

FactorialContainer::iterator FactorialContainer::iterator::operator++(int)
{
  FactorialContainer::iterator tmp(index_, val_);
  ++(*this);
  return tmp;
}

FactorialContainer::iterator& FactorialContainer::iterator::operator--()
{
  if (index_ == 0)
  {
    throw std::runtime_error("division by 0");
  }

  val_ /= index_--;
  return *this;
}

FactorialContainer::iterator FactorialContainer::iterator::operator--(int)
{
  FactorialContainer::iterator tmp(index_, val_);
  --(*this);
  return tmp;
}

bool FactorialContainer::iterator::operator==(const FactorialContainer::iterator& other) const
{
  return (index_ == other.index_);
}

bool FactorialContainer::iterator::operator!=(const FactorialContainer::iterator& other) const
{
  return !(*this == other);
}
