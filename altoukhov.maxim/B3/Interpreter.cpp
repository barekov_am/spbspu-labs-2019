#include "Interpreter.hpp"

#include <algorithm>
#include <stdexcept>

#include "PhoneBookManager.hpp"

const Interpreter::instruction_t Interpreter::invalidInstruction = [](PhoneBookManager&) {
  return "<INVALID COMMAND>\n";
};

const std::unordered_map<std::string, std::function<Interpreter::instruction_t(std::string&)>> Interpreter::parsers = {
  { "add"   , &Interpreter::parse_add_    },
  { "store" , &Interpreter::parse_store_  },
  { "insert", &Interpreter::parse_insert_ },
  { "delete", &Interpreter::parse_delete_ },
  { "show"  , &Interpreter::parse_show_   },
  { "move"  , &Interpreter::parse_move_   }
};

Interpreter::instruction_t Interpreter::interpret(const std::string& cmd)
{
  if (cmd.empty()) {
    return [](PhoneBookManager&) { return ""; };
  }
  try {
    return parse_(cmd);
  } catch (...) {
    return invalidInstruction;
  }
}

Interpreter::instruction_t Interpreter::parse_(std::string cmd)
{
  const std::string command = extract_lexeme_(cmd);
  const auto parser = parsers.find(command);
  if (parser == parsers.end()) {
    return invalidInstruction;
  }
  return parser->second(cmd);
}

Interpreter::instruction_t Interpreter::parse_add_(std::string& args)
{
  const auto number = extract_lexeme_(args);
  if (number.empty()) {
    return invalidInstruction;
  }
  char* end = nullptr;
  (void)strtoll(number.c_str(), &end, 10);
  if (*end) {
    return invalidInstruction;
  }
  if (args.empty() || args[0] != '\"') {
    return invalidInstruction;
  }
  const auto name = extract_str_(args);
  if (!args.empty()) {
    return invalidInstruction;
  }
  return [number, name](PhoneBookManager& p) {
    p.add(number, name);
    return "";
  };
}

Interpreter::instruction_t Interpreter::parse_insert_(std::string& args)
{
  const auto position = extract_lexeme_(args);
  const auto bookmark = extract_lexeme_(args);
  const auto number = extract_lexeme_(args);
  if (args.empty() || args[0] != '\"') {
    return invalidInstruction;
  }
  const auto name = extract_str_(args);
  if (position.empty() || bookmark.empty() || number.empty() || !args.empty()) {
    return invalidInstruction;
  }
  char* end = nullptr;
  (void)strtoll(number.c_str(), &end, 10);
  if (*end) {
    return invalidInstruction;
  }
  if (position == "before") {
    return [bookmark, number, name](PhoneBookManager& p) {
      return p.insertBefore(bookmark, number, name);
    };
  }
  if (position == "after") {
    return [bookmark, number, name](PhoneBookManager& p) {
      return p.insertAfter(bookmark, number, name);
    };
  }
  return invalidInstruction;
}

Interpreter::instruction_t Interpreter::parse_delete_(std::string& args)
{
  const auto bookmark = extract_lexeme_(args);
  if (bookmark.empty() || !args.empty()) {
    return invalidInstruction;
  }
  return [bookmark](PhoneBookManager& p) {
    return p.remove(bookmark);
  };
}

Interpreter::instruction_t Interpreter::parse_show_(std::string& args)
{
  const auto bookmark = extract_lexeme_(args);
  if (bookmark.empty() || !args.empty()) {
    return invalidInstruction;
  }
  return [bookmark](PhoneBookManager& p) {
    return p.show(bookmark);
  };
}

Interpreter::instruction_t Interpreter::parse_move_(std::string& args)
{
  const auto bookmark = extract_lexeme_(args);
  const auto step = extract_lexeme_(args);
  if (bookmark.empty() || step.empty() || !args.empty()) {
    return invalidInstruction;
  }
  if (step == "first") {
    return [bookmark](PhoneBookManager& p) {
      return p.moveToFirst(bookmark);
    };
  }
  if (step == "last") {
    return [bookmark](PhoneBookManager& p) {
      return p.moveToLast(bookmark);
    };
  }
  char* end = nullptr;
  auto s = strtoll(step.c_str(), &end, 10);
  if (*end) {
    return [](PhoneBookManager&) { return "<INVALID STEP>\n"; };
  }
  return [bookmark, s](PhoneBookManager& p) {
    return p.move(bookmark, s);
  };
}

Interpreter::instruction_t Interpreter::parse_store_(std::string& args)
{
  const auto oldBM = extract_lexeme_(args);
  const auto newMarkName = extract_lexeme_(args);
  if (oldBM.empty() || newMarkName.empty() || !args.empty()) {
    return invalidInstruction;
  }
  return [oldBM, newMarkName](PhoneBookManager& p) {
    return p.storeBm(oldBM, newMarkName);
  };
}

std::string Interpreter::extract_str_(std::string& sentence)
{
  size_t pos = 1;
  std::string str;
  while (pos < sentence.length() && sentence[pos] != '\"') {
    if (sentence[pos] == '\\' && pos + 1 < sentence.length() && sentence[pos + 1] == '\"') {
      pos += 2;
    } else {
      pos += 1;
    }
  }
  if (pos == sentence.length()) {
    throw std::runtime_error{ "unterminated string" };
  }
  str += sentence.substr(1, pos - 1);
  str.erase(std::remove(str.begin(), str.end(), '\\'), str.end());
  ++pos;
  while (pos < sentence.length() && isspace(sentence[pos])) {
    ++pos;
  }
  sentence.erase(0, pos);
  return str;
}

std::string Interpreter::extract_lexeme_(std::string& sentence)
{
  if (sentence.empty()) {
    return "";
  }
  const auto space = std::find_if(sentence.begin(), sentence.end(), [](const char& c) {return isspace(c); });
  if (space == sentence.end()) {
    auto word = sentence;
    sentence = "";
    return word;
  }
  const auto wordLength = std::distance(sentence.begin(), space);
  size_t trimLength = wordLength;
  while (isspace(sentence[trimLength])) {
    ++trimLength;
  }
  auto word = sentence.substr(0, wordLength);
  sentence.erase(0, trimLength);
  return word;

}
