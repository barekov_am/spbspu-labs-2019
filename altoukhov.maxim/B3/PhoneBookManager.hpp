#ifndef PHONE_BOOK_MANAGER_HPP
#define PHONE_BOOK_MANAGER_HPP

#include <unordered_map>
#include <string>

#include "PhoneBook.hpp"

class PhoneBookManager
{
public:
  PhoneBookManager();
  void add(const std::string&, const std::string&);
  std::string storeBm(const std::string&, const std::string&);
  std::string insertBefore(const std::string&, const std::string&, const std::string&);
  std::string insertAfter(const std::string&, const std::string&, const std::string&);
  std::string remove(const std::string&);
  std::string show(const std::string&);
  std::string move(const std::string&, long long);
  std::string moveToFirst(const std::string&);
  std::string moveToLast(const std::string&);

private:
  static const std::string invalidBM;
  static const std::string currentBM;

  PhoneBook phone_book_;
  std::unordered_map<std::string, PhoneBook::iterator> book_marks_;

  void revalidate();
};

#endif
