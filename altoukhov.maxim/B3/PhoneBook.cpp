#include "PhoneBook.hpp"

void PhoneBook::insertAfter(iterator pos, const std::string& name, const std::string& number)
{
  if (pos != records_.end()) {
    ++pos;
  }
  if (pos == records_.end()) {
    records_.push_back({ name, number });
  } else {
    records_.insert(pos, { name, number });
  }
}

void PhoneBook::insertBefore(iterator pos, const std::string& name, const std::string& number)
{
  records_.insert(pos, { name, number });
}

void PhoneBook::append(const std::string& name, const std::string& number)
{
  records_.push_back({ name, number });
}

void PhoneBook::remove(iterator pos)
{
  records_.erase(pos);
}

PhoneBook::iterator PhoneBook::begin()
{
  return records_.begin();
}

PhoneBook::const_iterator PhoneBook::cbegin() const noexcept
{
  return records_.cbegin();
}

PhoneBook::iterator PhoneBook::end()
{
  return records_.end();
}

PhoneBook::const_iterator PhoneBook::cend() const noexcept
{
  return records_.cend();
}

bool PhoneBook::empty() const noexcept
{
  return records_.empty();
}

size_t PhoneBook::size() const noexcept
{
  return records_.size();
}
