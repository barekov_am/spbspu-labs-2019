#ifndef INTERPRETER_HPP
#define INTERPRETER_HPP

#include <functional>
#include <string>
#include <unordered_map>

#include "PhoneBookManager.hpp"

class Interpreter
{
public:
  using instruction_t = std::function<std::string(PhoneBookManager&)>;
  static instruction_t interpret(const std::string& cmd);

private:

  static instruction_t parse_(std::string);

  static instruction_t parse_add_(std::string&);
  static instruction_t parse_store_(std::string&);
  static instruction_t parse_insert_(std::string&);
  static instruction_t parse_delete_(std::string&);
  static instruction_t parse_show_(std::string&);
  static instruction_t parse_move_(std::string&);

  static std::string extract_str_(std::string&);
  static std::string extract_lexeme_(std::string&);

  static const instruction_t invalidInstruction;

  static const std::unordered_map<std::string, std::function<instruction_t(std::string&)>> parsers;

};

#endif
