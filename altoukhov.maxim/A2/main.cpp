#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void printArea(const altoukhov::Shape& shape)
{
  std::cout << "Area: " << shape.getArea() << '\n';
}

void printFrameRect(const altoukhov::Shape& shape)
{
  altoukhov::rectangle_t rect = shape.getFrameRect();
  std::cout << "Frame rectangle width: " << rect.width << '\n';
  std::cout << "Frame rectangle height: " << rect.height << '\n';
  std::cout << "Frame rectangle center: {" << rect.pos.x << ", " << rect.pos.y << "}\n";
}

void printCenter(const altoukhov::Shape& shape)
{
  altoukhov::point_t center = shape.getCenter();
  std::cout << "Center: {" << center.x << ", " << center.y << "}\n";
}

void printChanges(altoukhov::Shape& shape)
{
  altoukhov::point_t point{1, 1};
  double ratio = 2;

  std::cout << "Moving shape to {" << point.x << ", " << point.y << "}.\n";
  shape.move(point);
  printCenter(shape);

  std::cout << "Moving shape by {" << point.x << ", " << point.y << "}.\n";
  shape.move(point.x, point.y);
  printCenter(shape);

  std::cout << "Scaling shape by " << ratio << ".\n";
  shape.scale(ratio);
  printArea(shape);

  std::cout << '\n';
}

int main()
{
  altoukhov::Rectangle rectangle({0, 0}, 2, 2);
  std::cout << "[RECTANGLE]\n";
  printArea(rectangle);
  printCenter(rectangle);
  printFrameRect(rectangle);
  std::cout << '\n';
  printChanges(rectangle);

  altoukhov::Circle circle({0, 0}, 2);
  std::cout << "[CIRCLE]\n";
  printArea(circle);
  printCenter(circle);
  printFrameRect(circle);
  std::cout << '\n';
  printChanges(circle);

  altoukhov::Triangle triangle({0, 1}, {1, 0}, {-1, 0});
  std::cout << "[TRIANGLE]\n";
  printArea(triangle);
  printCenter(triangle);
  printFrameRect(triangle);
  std::cout << '\n';
  printChanges(triangle);

  altoukhov::point_t vertices[5]{{0, 2}, {2, 0}, {1, -1}, {-1, -1}, {-2, 0}};
  altoukhov::Polygon polygon(vertices, 5);
  std::cout << "[POLYGON]\n";
  printArea(polygon);
  printCenter(polygon);
  printFrameRect(polygon);
  std::cout << '\n';
  printChanges(polygon);
}
