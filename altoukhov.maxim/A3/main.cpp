#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

void printArea(const altoukhov::Shape& shape)
{
  std::cout << "Area: " << shape.getArea() << '\n';
}

void printFrameRect(const altoukhov::Shape& shape)
{
  altoukhov::rectangle_t rect = shape.getFrameRect();
  std::cout << "Frame rectangle width: " << rect.width << '\n';
  std::cout << "Frame rectangle height: " << rect.height << '\n';
  std::cout << "Frame rectangle center: {" << rect.pos.x << ", " << rect.pos.y << "}\n";
}

void printCenter(const altoukhov::Shape& shape)
{
  altoukhov::point_t center = shape.getCenter();
  std::cout << "Center: {" << center.x << ", " << center.y << "}\n";
}

void printChanges(altoukhov::Shape& shape)
{
  altoukhov::point_t point{1, 1};
  double ratio = 2;

  std::cout << "Moving shape to {" << point.x << ", " << point.y << "}.\n";
  shape.move(point);
  printCenter(shape);

  std::cout << "Moving shape by {" << point.x << ", " << point.y << "}.\n";
  shape.move(point.x, point.y);
  printCenter(shape);

  std::cout << "Scaling shape by " << ratio << ".\n";
  shape.scale(ratio);
  printArea(shape);

  std::cout << '\n';
}

int main()
{
  using shape_ptr = std::shared_ptr<altoukhov::Shape>;

  altoukhov::Circle circle({1, 1}, 2);
  altoukhov::Rectangle rectangle({0, 0}, 1, 1);
  altoukhov::Triangle triangle({0, 2}, {2, 0}, {-2, 0});
  altoukhov::point_t vertices[5]{{0, 2}, {2, 0}, {1, -1}, {-1, -1}, {-2, 0}};
  altoukhov::Polygon polygon(vertices, 5);

  shape_ptr rect_ptr = std::make_shared<altoukhov::Rectangle>(rectangle);
  shape_ptr cir_ptr = std::make_shared<altoukhov::Circle>(circle);
  shape_ptr tri_ptr = std::make_shared<altoukhov::Triangle>(triangle);
  shape_ptr poly_ptr = std::make_shared<altoukhov::Polygon>(polygon);

  altoukhov::CompositeShape composition;
  std::cout << "[COMPOSITE SHAPE]\n";
  composition.add(rect_ptr);
  composition.add(cir_ptr);
  composition.add(tri_ptr);
  composition.add(poly_ptr);
  
  printArea(composition);
  printCenter(composition);
  printFrameRect(composition);
  std::cout << '\n';
  printChanges(composition);
}
