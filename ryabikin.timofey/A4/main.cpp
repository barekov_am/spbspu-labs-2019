#include <iostream>
#include <cassert>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "layering.hpp"


void showRectangle_t(ryabikin::rectangle_t data)
{
  std::cout << "width = " << data.width << ", ";
  std::cout << "height = " << data.height << "\n";
  std::cout << "{X = " << data.pos.x << "; ";
  std::cout << "Y = " << data.pos.y << "}" << "\n";
}

void showAll(const ryabikin::Shape * shape)
{
  assert(shape != nullptr);
  std::cout << "area = " << shape->getArea() << ", ";
  showRectangle_t(shape->getFrameRect());
}

void PrintComposite(const ryabikin::Shape *shape)
{
  assert(shape != nullptr);
  ryabikin::rectangle_t frameRect = shape->getFrameRect();
  std::cout << "{X = " << frameRect.pos.x << "; Y = ";
  std::cout << frameRect.pos.y << "} " << "\n";
  std::cout << "width = " << frameRect.width << ", ";
  std::cout << "height = " << frameRect.height << "\n";
}

void printRectFrames(const ryabikin::Shape &shape)
{
  ryabikin::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "{" << frameRect.pos.x << ","
    << frameRect.pos.y << "},"
    << frameRect.width << ","
    << frameRect.height << "\n";
}

void printMatrixInfo(const ryabikin::Matrix<ryabikin::Shape> &matrix)
{
  for (size_t i = 0; i < matrix.getRowsCount(); i++)
  {
    std::cout << "Layer number - " << i + 1 << "\n";
    std::cout << "Amount of shapes - " << matrix[i].getSize() << "\n";
    for (size_t j = 0; j < matrix[i].getSize(); j++)
    {
      std::cout << "Shape number " << j + 1 << " with parameters : ";
      printRectFrames(*matrix[i][j]);
    }
  }
}

int main()
{
  ryabikin::Rectangle rectnagle1(3.00, 4.00, { 5.00, 4.00 });
  std::cout << "Rectangle  " << "\n";
  showAll(&rectnagle1);
  rectnagle1.scale(3.0);
  std::cout << "After scale - " << "\n";
  showAll(&rectnagle1);
  rectnagle1.move({ .x = 7.00, .y = 8.00 });
  std::cout << "After move - " << "\n";
  showAll(&rectnagle1);

  ryabikin::Circle circle1(3.00, { 1.00, 3.00 });
  std::cout << "Circle  " << "\n";
  showAll(&circle1);
  circle1.scale(4.00);
  std::cout << "After scale - " << "\n";
  showAll(&circle1);
  circle1.move(3.00, -1.5);
  std::cout << "After move - " << "\n";
  showAll(&circle1);

  std::cout << "\n";
  std::cout << "-Cpmposite shapes-" << "\n";

  ryabikin::Rectangle rec4(4.00, 3.00, { 7.00, 9.00 });
  std::cout << "Area of rectangle is " << rec4.getArea() << "\n";
  ryabikin::Circle cir4(1.00, { 2.00, 2.00 });
  std::cout << "Area of circle is " << cir4.getArea() << "\n";

  ryabikin::CompositeShape compShape1;
  compShape1.add(std::make_shared<ryabikin::Rectangle>(rec4));
  PrintComposite(&compShape1);
  compShape1.add(std::make_shared<ryabikin::Circle>(cir4));
  PrintComposite(&compShape1);
  std::cout << "Area of 1 composite shape is " << compShape1.getArea() << "\n";

  compShape1.move(5, 5);
  PrintComposite(&compShape1);
  compShape1.move({ 6, 8 });
  PrintComposite(&compShape1);
  compShape1.scale(2);
  std::cout << "Area of 1 composite shape after multiplication is " << compShape1.getArea() << "\n";
  PrintComposite(&compShape1);
  compShape1.scale(0.3);
  std::cout << "Area of 1 composite shape after multiplication is " << compShape1.getArea() << "\n";
  PrintComposite(&compShape1);

  compShape1.remove(0);
  std::cout << "Area of 1 composite shape after removing rectangle is " << compShape1.getArea() << "\n";

  ryabikin::CompositeShape compShape2(compShape1);
  std::cout << "Area of 2 composite shape is " << compShape2.getArea() << "\n";
  ryabikin::Rectangle rec5(2.00, 4.00, { 15.00, 15.00 });
  std::cout << "Area of rectangle is " << rec5.getArea() << "\n";

  compShape2.add(std::make_shared<ryabikin::Rectangle>(rec5));
  std::cout << "Area of 2 composite shape after adding rectangle is " << compShape2.getArea() << "\n";
  std::cout << "The area of last shape is " << compShape2[1]->getArea() << "\n";

  std::cout << "\n";
  std::cout << "-Layering composite shape-" << "\n";

  ryabikin::CompositeShape compShape3;
  ryabikin::Rectangle rec6(4.00, 8.00, { 1.00, 1.00 });
  ryabikin::Circle cir6(1.00, { 2.00, 2.00 });
  ryabikin::Circle cir7(3.00, { 8.00, 8.00 });
  compShape3.add(std::make_shared<ryabikin::Rectangle>(rec6));
  compShape3.add(std::make_shared<ryabikin::Circle>(cir6));
  compShape3.add(std::make_shared<ryabikin::Circle>(cir7));

  ryabikin::Matrix<ryabikin::Shape> matrix;
  matrix = ryabikin::split(compShape3);
  printMatrixInfo(matrix);
  std::cout << "The biggest number of shapes in a layer is " << matrix.getMaxColSize() << " shapes \n";

  return 0;
}
