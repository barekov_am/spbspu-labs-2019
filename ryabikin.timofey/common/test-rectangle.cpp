#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(TestRectangle)

const double fault = 0.001;

BOOST_AUTO_TEST_CASE(TestRectangleMoved)
{
  ryabikin::Rectangle rectangle(5.00, 7.00, { 15.00, 15.00 });
  const double AreaBeforeMove = rectangle.getArea();
  const ryabikin::rectangle_t frameBeforeMove = rectangle.getFrameRect();
  rectangle.move(3.5, 5.00);
  const double AreaAfterMove = rectangle.getArea();
  const ryabikin::rectangle_t frameAfterMove = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, fault);
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, fault);
  BOOST_CHECK_CLOSE(AreaBeforeMove, AreaAfterMove, fault);
}

BOOST_AUTO_TEST_CASE(TestRectangleMovedPos)
{
  ryabikin::Rectangle rectangle(5.00, 7.00, { 15.00, 15.00 });
  const double AreaBeforeMove = rectangle.getArea();
  const ryabikin::rectangle_t frameBefore = rectangle.getFrameRect();
  rectangle.move({ 3.5, 5.00 });
  const double AreaAfterMove = rectangle.getArea();
  const ryabikin::rectangle_t frameAfterMove = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfterMove.height, fault);
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfterMove.width, fault);
  BOOST_CHECK_CLOSE(AreaBeforeMove, AreaAfterMove, fault);
}

BOOST_AUTO_TEST_CASE(TestRectangleScaled)
{
  ryabikin::Rectangle rectangle(5.00, 7.00, { 15.00, 15.00 });
  const double AreaBeforeScale = rectangle.getArea();
  const double coefficient = 2.0;
  const double multipler = coefficient * coefficient;
  rectangle.scale(coefficient);
  const double AreaAfterScale = rectangle.getArea();
  BOOST_CHECK_CLOSE(AreaBeforeScale * multipler, AreaAfterScale, fault);
}

BOOST_AUTO_TEST_CASE(TestRectangleParametrs)
{
  BOOST_CHECK_THROW(ryabikin::Rectangle(-1.00, 5.00, { 15.00, 15.00 }), std::invalid_argument);
  BOOST_CHECK_THROW(ryabikin::Rectangle(5.00, -1.00, { 15.00, 15.00 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestRectangleScaleCoefficient)
{
  ryabikin::Rectangle rectangle(5.00, 7.00, { 15.00, 15.00 });
  BOOST_CHECK_THROW(rectangle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestRectParametersAfterRotation)
{
  ryabikin::Rectangle rectangle(5.00, 7.00, { 15.00, 15.00 });
  const ryabikin::rectangle_t frameBefore = rectangle.getFrameRect();
  const double areaBefore = rectangle.getArea();
  rectangle.rotate(90);
  const double areaAfter = rectangle.getArea();
  const ryabikin::rectangle_t frameAfter = rectangle.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, fault);
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.height, fault);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.width, fault);
  BOOST_CHECK_CLOSE(frameBefore.pos.x, frameAfter.pos.x, fault);
  BOOST_CHECK_CLOSE(frameBefore.pos.y, frameAfter.pos.y, fault);
}

BOOST_AUTO_TEST_SUITE_END()
