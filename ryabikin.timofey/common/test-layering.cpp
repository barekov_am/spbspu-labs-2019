#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "layering.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(TestLayering)

BOOST_AUTO_TEST_CASE(TestMatrixParametersAfterLayering)
{
  ryabikin::Circle circle(2.00, { 1.00, 1.00 });
  ryabikin::Circle circle2(2.00, { 10.00, 10.00 });
  ryabikin::Rectangle rectangle(10.00, 4.00, { 5.00, 2.00 });

  std::shared_ptr<ryabikin::Shape> cirPointer1 = std::make_shared<ryabikin::Circle>(circle);
  std::shared_ptr<ryabikin::Shape> cirPointer2 = std::make_shared<ryabikin::Circle>(circle2);
  std::shared_ptr<ryabikin::Shape> recPointer1 = std::make_shared<ryabikin::Rectangle>(rectangle);

  ryabikin::CompositeShape compShape;
  compShape.add(cirPointer1);
  compShape.add(recPointer1);
  compShape.add(cirPointer2);

  ryabikin::Matrix<ryabikin::Shape> matrix;
  matrix = ryabikin::split(compShape);

  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), 2);
  BOOST_CHECK_EQUAL(matrix.getRowsCount(), 2);
  BOOST_CHECK_EQUAL(matrix[0].getSize(), 2);
  BOOST_CHECK_EQUAL(matrix[1].getSize(), 1);

  BOOST_CHECK(matrix[0][0] == cirPointer1);
  BOOST_CHECK(matrix[0][1] == cirPointer2);
  BOOST_CHECK(matrix[1][0] == recPointer1);
}

BOOST_AUTO_TEST_CASE(TestMatrixParametersAfterAdding)
{
  ryabikin::Rectangle rectangle(10.00, 4.00, { 5.00, 2.00 });
  ryabikin::Circle circle(2.00, { 1.00, 1.00 });

  ryabikin::Circle circle2(2.00, { 10.00, 10.00 });
  std::shared_ptr<ryabikin::Shape> cirPointer1 = std::make_shared<ryabikin::Circle>(circle);
  std::shared_ptr<ryabikin::Shape> cirPointer2 = std::make_shared<ryabikin::Circle>(circle2);
  std::shared_ptr<ryabikin::Shape> recPointer1 = std::make_shared<ryabikin::Rectangle>(rectangle);

  ryabikin::CompositeShape compShape;
  compShape.add(cirPointer1);
  compShape.add(recPointer1);
  compShape.add(cirPointer2);

  ryabikin::Rectangle rectangle2(1.00, 2.00, { 15.00, 15.00 });
  std::shared_ptr<ryabikin::Shape> recPointer2 = std::make_shared<ryabikin::Rectangle>(rectangle2);

  ryabikin::Matrix<ryabikin::Shape> matrix;
  matrix = ryabikin::split(compShape);

  size_t maxColBefore = matrix.getMaxColSize();
  size_t rowsCountBefore = matrix.getRowsCount();
  size_t layer1SizeBefore = matrix[0].getSize();
  size_t layer2SizeBefore = matrix[1].getSize();

  matrix.add(0, recPointer2);

  BOOST_CHECK_EQUAL(maxColBefore + 1, matrix.getMaxColSize());
  BOOST_CHECK_EQUAL(rowsCountBefore, matrix.getRowsCount());
  BOOST_CHECK_EQUAL(layer1SizeBefore + 1, matrix[0].getSize());
  BOOST_CHECK_EQUAL(layer2SizeBefore, matrix[1].getSize());

  BOOST_CHECK(matrix[0][0] == cirPointer1);
  BOOST_CHECK(matrix[0][1] == cirPointer2);
  BOOST_CHECK(matrix[0][2] == recPointer2);
  BOOST_CHECK(matrix[1][0] == recPointer1);
}

BOOST_AUTO_TEST_CASE(TestIntersect)
{
  ryabikin::Rectangle rectangle(1.00, 2.00, { 10.00, 5.00 });
  ryabikin::Circle circle(2.00, { 3.00, 2.00 });
  ryabikin::Circle circle2(2.00,{ 10.00, 6.00 });

  BOOST_CHECK(!ryabikin::intersect(rectangle, circle));
  BOOST_CHECK(ryabikin::intersect(rectangle, circle2));
}

BOOST_AUTO_TEST_SUITE_END()
