#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(TestCircle)

const double fault = 0.001;

BOOST_AUTO_TEST_CASE(TestCircleMoved)
{
  ryabikin::Circle circle(5.00, { 16.00, 16.00 });
  const double AreaBeforeMove = circle.getArea();
  const ryabikin::rectangle_t frameBeforeMove = circle.getFrameRect();
  circle.move(3.2, 5.0);
  const double AreaAfterMove = circle.getArea();
  const ryabikin::rectangle_t frameAfterMove = circle.getFrameRect();
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, fault);
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, fault);
  BOOST_CHECK_CLOSE(AreaBeforeMove, AreaAfterMove, fault);
}

BOOST_AUTO_TEST_CASE(TestCircleMovedPos)
{
  ryabikin::Circle circle(5.00, { 16.00, 16.00 });
  const double AreaBeforeMove = circle.getArea();
  const ryabikin::rectangle_t frameBeforeMove = circle.getFrameRect();
  circle.move({ 3.2, 5.00 });
  const double AreaAfterMove = circle.getArea();
  const ryabikin::rectangle_t frameAfterMove = circle.getFrameRect();
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, fault);
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, fault);
  BOOST_CHECK_CLOSE(AreaBeforeMove, AreaAfterMove, fault);
}

BOOST_AUTO_TEST_CASE(TestCircleScaled)
{
  ryabikin::Circle circle(5.00, { 16.00, 16.00 });
  const double AreaBeforeScale = circle.getArea();
  const double coefficient = 2.0;
  const double multipler = coefficient * coefficient;
  circle.scale(coefficient);
  const double AreaAfterScale = circle.getArea();
  BOOST_CHECK_CLOSE(AreaBeforeScale * multipler, AreaAfterScale, fault);
}

BOOST_AUTO_TEST_CASE(TestCircleParametrs)
{
  BOOST_CHECK_THROW(ryabikin::Circle(-1.00, { 16.00, 16.00 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestCircleScaleCoefficient)
{
  ryabikin::Circle circle(5.00, { 16.00, 16.00 });
  BOOST_CHECK_THROW(circle.scale(-1.00), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestCircleAfterRotation)
{
  ryabikin::Circle circle(5.00, { 16.00, 16.00 });
  const ryabikin::rectangle_t frameBefore = circle.getFrameRect();
  const double areaBefore = circle.getArea();
  circle.rotate(90);
  const double areaAfter = circle.getArea();
  const ryabikin::rectangle_t frameAfter = circle.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, fault);
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.height, fault);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.width, fault);
  BOOST_CHECK_CLOSE(frameBefore.pos.x, frameAfter.pos.x, fault);
  BOOST_CHECK_CLOSE(frameBefore.pos.y, frameAfter.pos.y, fault);
}


BOOST_AUTO_TEST_SUITE_END();
