#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"


const double fault = 0.001;

BOOST_AUTO_TEST_SUITE(compositeShapeA3)

BOOST_AUTO_TEST_CASE(testCopyConstructor)
{
  ryabikin::Rectangle rectangle(5.00, 5.00, { 7.00, 7.00 });
  ryabikin::Circle circle(5.00, { 6.00, 6.00 });
  ryabikin::CompositeShape compShape1;
  compShape1.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  compShape1.add(std::make_shared<ryabikin::Circle>(circle));


  ryabikin::CompositeShape compShape2(compShape1);
  const double area1 = compShape1.getArea();
  const double area2 = compShape2.getArea();
  const ryabikin::rectangle_t frame1 = compShape1.getFrameRect();
  const ryabikin::rectangle_t frame2 = compShape2.getFrameRect();

  BOOST_CHECK_CLOSE(area1, area2, fault);
  BOOST_CHECK_CLOSE(frame1.height, frame2.height, fault);
  BOOST_CHECK_CLOSE(frame1.width, frame2.width, fault);
  BOOST_CHECK_CLOSE(frame1.pos.x, frame2.pos.x, fault);
  BOOST_CHECK_CLOSE(frame1.pos.y, frame2.pos.y, fault);
}

BOOST_AUTO_TEST_CASE(immutabilityOfcompositeShapeAfterChangingCenter)
{
  ryabikin::Rectangle rectangle(5.00, 5.00, { 7.00, 7.00 });
  ryabikin::Circle circle(5.00, { 6.00, 6.00 });
  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  compShape.add(std::make_shared<ryabikin::Circle>(circle));

  const double areaBeforeChangingCenter = compShape.getArea();
  const ryabikin::rectangle_t frameBeforeChangingCenter = compShape.getFrameRect();
  compShape.move({ 2, 2 });
  const double areaAfterChangingCenter = compShape.getArea();
  const ryabikin::rectangle_t frameAfterChangingCenter = compShape.getFrameRect();
  BOOST_CHECK_CLOSE(areaBeforeChangingCenter, areaAfterChangingCenter, fault);
  BOOST_CHECK_CLOSE(frameBeforeChangingCenter.width, frameAfterChangingCenter.width, fault);
  BOOST_CHECK_CLOSE(frameBeforeChangingCenter.height, frameAfterChangingCenter.height, fault);
}

BOOST_AUTO_TEST_CASE(immutabilityOfcompositeShapeAfterMoving)
{
  ryabikin::Rectangle rectangle(5.00, 5.00, { 7.00, 7.00 });
  ryabikin::Circle circle(5.00, { 6.00, 6.00 });
  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  compShape.add(std::make_shared<ryabikin::Circle>(circle));

  const double areaBeforeDisplacement = compShape.getArea();
  const ryabikin::rectangle_t frameBeforeDisplacement = compShape.getFrameRect();
  compShape.move(7, 8);
  const double areaAfterDisplacement = compShape.getArea();
  const ryabikin::rectangle_t frameAfterDisplacement = compShape.getFrameRect();
  BOOST_CHECK_CLOSE(areaBeforeDisplacement, areaAfterDisplacement, fault);
  BOOST_CHECK_CLOSE(frameBeforeDisplacement.width, frameAfterDisplacement.width, fault);
  BOOST_CHECK_CLOSE(frameBeforeDisplacement.height, frameAfterDisplacement.height, fault);
}

BOOST_AUTO_TEST_CASE(changeCompositeShapeAreaAfterScaling)
{
  ryabikin::Rectangle rectangle(2.00, 6.00, { 6.00, 4.00 });
  ryabikin::Circle circle(2.00, { 1.00, 1.00 });
  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  compShape.add(std::make_shared<ryabikin::Circle>(circle));

  const double areaBeforeScaling = compShape.getArea();
  const double factor = 2;
  compShape.scale(factor);

  BOOST_CHECK_CLOSE(compShape.getArea(), factor * factor * areaBeforeScaling, fault);
}

BOOST_AUTO_TEST_CASE(compShapeAfterAdding)
{
  ryabikin::Rectangle rectangle(2.00, 6.00, { 6.00, 4.00 });
  ryabikin::Circle circle(2.00, { 1.00, 1.00 });
  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));

  const double areaBeforeAdding = compShape.getArea();

  compShape.add(std::make_shared<ryabikin::Circle>(circle));
  const double areaOfAddedShape = circle.getArea();

  const double areaAfterAdding = compShape.getArea();
  BOOST_CHECK_CLOSE(areaBeforeAdding + areaOfAddedShape, areaAfterAdding, fault);
}

BOOST_AUTO_TEST_CASE(compShapeAfterDeleting)
{
  ryabikin::Rectangle rectangle(2.00, 6.00, { 6.00, 4.00 });
  ryabikin::Circle circle(2.00, { 1.00, 1.00 });
  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  compShape.add(std::make_shared<ryabikin::Circle>(circle));


  const double areaBeforeDeleting = compShape.getArea();
  const double areaOfDeletedShape = circle.getArea();
  compShape.remove(1);
  const double areaAfterDeleting = compShape.getArea();
  BOOST_CHECK_CLOSE(areaBeforeDeleting - areaOfDeletedShape, areaAfterDeleting, fault);
}

BOOST_AUTO_TEST_CASE(invalidArgumentsErrorsDetection)
{
  ryabikin::Rectangle rectangle(2.00, 4.00, { 2.00, 2.00 });
  ryabikin::Circle circle(2.00, { 1.00, 1.00 });
  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  compShape.add(std::make_shared<ryabikin::Circle>(circle));


  BOOST_CHECK_THROW(compShape.scale(-2), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.add(nullptr), std::invalid_argument);

  ryabikin::CompositeShape compShape2;
  compShape2.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  compShape2.remove(0);
  BOOST_CHECK_THROW(compShape2.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(compShape2.getArea(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(outOfRangeErrorsDetection)
{
  ryabikin::Rectangle rectangle(2.00, 3.00, { 4.00, 4.00 });
  ryabikin::Circle circle(2.00, { 1.00, 1.00 });
  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  compShape.add(std::make_shared<ryabikin::Circle>(circle));

  BOOST_CHECK_THROW(compShape.remove(-1), std::out_of_range);
  BOOST_CHECK_THROW(compShape.remove(10), std::out_of_range);
  BOOST_CHECK_THROW(compShape[-1], std::out_of_range);
  BOOST_CHECK_THROW(compShape[10], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(testMoveConstructor)
{
  ryabikin::Rectangle rectangle(2.00, 3.00, { 4.00, 4.00 });
  ryabikin::Circle circle(2.00, { 1.00, 1.00 });
  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  compShape.add(std::make_shared<ryabikin::Circle>(circle));

  const ryabikin::rectangle_t frameRect = compShape.getFrameRect();
  const double compositeArea = compShape.getArea();

  ryabikin::CompositeShape compShape2(std::move(compShape));
  const ryabikin::rectangle_t frameRect2 = compShape2.getFrameRect();
  BOOST_CHECK_CLOSE(compositeArea, compShape2.getArea(), fault);
  BOOST_CHECK_CLOSE(frameRect.height, frameRect2.height, fault);
  BOOST_CHECK_CLOSE(frameRect.width, frameRect2.width, fault);
  BOOST_CHECK_CLOSE(frameRect.pos.x, frameRect2.pos.x, fault);
  BOOST_CHECK_CLOSE(frameRect.pos.y, frameRect2.pos.y, fault);
}

BOOST_AUTO_TEST_CASE(testMoveOperator)
{
  ryabikin::Rectangle rectangle(2.00, 3.00, { 4.00, 4.00 });
  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  const ryabikin::rectangle_t frameRect = compShape.getFrameRect();
  const double compositeArea = compShape.getArea();

  ryabikin::Circle circle(2.00, { 1.00, 1.00 });
  ryabikin::CompositeShape compShape2;
  compShape2.add(std::make_shared<ryabikin::Circle>(circle));
  compShape2 = std::move(compShape);
  const ryabikin::rectangle_t frameRect2 = compShape2.getFrameRect();
  const double compositeArea2 = compShape2.getArea();
  BOOST_CHECK_CLOSE(compositeArea, compositeArea2, fault);
  BOOST_CHECK_CLOSE(frameRect.height, frameRect2.height, fault);
  BOOST_CHECK_CLOSE(frameRect.width, frameRect2.width, fault);
  BOOST_CHECK_CLOSE(frameRect.pos.x, frameRect2.pos.x, fault);
  BOOST_CHECK_CLOSE(frameRect.pos.y, frameRect2.pos.y, fault);
}

BOOST_AUTO_TEST_CASE(testCopyOperator)
{
  ryabikin::Rectangle rectangle(2.00, 3.00, { 4.00, 4.00 });
  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));

  const ryabikin::rectangle_t frameRect = compShape.getFrameRect();
  const double compositeArea = compShape.getArea();

  ryabikin::Circle circle(2.00, { 1.00, 1.00 });
  ryabikin::CompositeShape compShape2;
  compShape2.add(std::make_shared<ryabikin::Circle>(circle));
  compShape2 = compShape;
  const ryabikin::rectangle_t frameRect2 = compShape2.getFrameRect();
  const double compositeArea2 = compShape2.getArea();
  BOOST_CHECK_CLOSE(compositeArea, compositeArea2, fault);
  BOOST_CHECK_CLOSE(frameRect.height, frameRect2.height, fault);
  BOOST_CHECK_CLOSE(frameRect.width, frameRect2.width, fault);
  BOOST_CHECK_CLOSE(frameRect.pos.x, frameRect2.pos.x, fault);
  BOOST_CHECK_CLOSE(frameRect.pos.y, frameRect2.pos.y, fault);
}

BOOST_AUTO_TEST_SUITE_END()
