#include <stdexcept>
#include <algorithm>
#include <boost/test/auto_unit_test.hpp>
#include "layering.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(TestMatrix)

BOOST_AUTO_TEST_CASE(TestCopyConstructor)
{
  ryabikin::Rectangle rectangle(4.00, 7.00, { 1.00, 1.00 });
  ryabikin::Circle circle(4.00, { 2.00, 2.00 });

  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  compShape.add(std::make_shared<ryabikin::Circle>(circle));

  ryabikin::Matrix<ryabikin::Shape> matrix;
  matrix = ryabikin::split(compShape);
  ryabikin::Matrix<ryabikin::Shape> matrix1(matrix);
  BOOST_CHECK_EQUAL(matrix.getRowsCount(), matrix1.getRowsCount());
  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), matrix1.getMaxColSize());
  BOOST_CHECK(matrix[0][0] == matrix1[0][0]);
  BOOST_CHECK(matrix[1][0] == matrix1[1][0]);
}

BOOST_AUTO_TEST_CASE(TestMoveConstructor)
{
  ryabikin::Rectangle rectangle(4.00, 7.00, { 1.00, 1.00 });
  ryabikin::Circle circle(4.00, { 2.00, 2.00 });

  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  compShape.add(std::make_shared<ryabikin::Circle>(circle));

  ryabikin::Matrix<ryabikin::Shape> matrix;
  matrix = ryabikin::split(compShape);
  ryabikin::Matrix<ryabikin::Shape> matrixClone(matrix);

  ryabikin::Matrix<ryabikin::Shape> matrix1(std::move(matrix));
  BOOST_CHECK_EQUAL(matrixClone.getRowsCount(), matrix1.getRowsCount());
  BOOST_CHECK_EQUAL(matrixClone.getMaxColSize(), matrix1.getMaxColSize());
  BOOST_CHECK(matrixClone[0][0] == matrix1[0][0]);
  BOOST_CHECK(matrixClone[1][0] == matrix1[1][0]);
  BOOST_CHECK_EQUAL(matrix.getRowsCount(), 0);
  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), 0);
}

BOOST_AUTO_TEST_CASE(TestCopyOperator)
{
  ryabikin::Rectangle rectangle(4.00, 7.00, { 1.00, 1.00 });
  ryabikin::Circle circle(4.00, { 2.00, 2.00 });
  ryabikin::Rectangle rectangle2(4.00, 4.00, { 10.00, 14.00 });
  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  compShape.add(std::make_shared<ryabikin::Circle>(circle));

  ryabikin::Matrix<ryabikin::Shape> matrix = ryabikin::split(compShape);
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle2));
  ryabikin::Matrix<ryabikin::Shape> matrix1 = ryabikin::split(compShape);

  matrix1 = matrix;

  BOOST_CHECK_EQUAL(matrix.getRowsCount(), matrix1.getRowsCount());
  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), matrix1.getMaxColSize());
  BOOST_CHECK(matrix[0][0] == matrix1[0][0]);
  BOOST_CHECK(matrix[1][0] == matrix1[1][0]);
}

BOOST_AUTO_TEST_CASE(TestMoveOperator)
{
  ryabikin::Rectangle rectangle(4.00, 7.00, { 1.00, 1.00 });
  ryabikin::Circle circle(4.00, { 2.00, 2.00 });
  ryabikin::Rectangle rectangle2(1.00, 1.00, { 10.00, 14.00 });

  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  compShape.add(std::make_shared<ryabikin::Circle>(circle));

  ryabikin::Matrix<ryabikin::Shape> matrix;
  matrix = ryabikin::split(compShape);
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle2));
  ryabikin::Matrix<ryabikin::Shape> matrixClone(matrix);
  ryabikin::Matrix<ryabikin::Shape> matrix1;
  matrix1 = ryabikin::split(compShape);
  matrix1 = std::move(matrix);

  BOOST_CHECK_EQUAL(matrixClone.getRowsCount(), matrix1.getRowsCount());
  BOOST_CHECK_EQUAL(matrixClone.getMaxColSize(), matrix1.getMaxColSize());
  BOOST_CHECK(matrixClone[0][0] == matrix1[0][0]);
  BOOST_CHECK(matrixClone[1][0] == matrix1[1][0]);
  BOOST_CHECK_EQUAL(matrix.getRowsCount(), 0);
  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), 0);
}

BOOST_AUTO_TEST_CASE(TestAdd)
{
  ryabikin::Rectangle rectangle(4.00, 7.00, { 1.00, 1.00 });
  ryabikin::Circle circle(4.00, { 2.00, 2.00 });
  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  compShape.add(std::make_shared<ryabikin::Circle>(circle));

  ryabikin::Matrix<ryabikin::Shape> matrix;
  matrix = ryabikin::split(compShape);
  size_t rowsBefore = matrix.getRowsCount();
  size_t maxBefore = matrix.getMaxColSize();

  ryabikin::Rectangle rectangle2(1.00, 1.00, { 10.00, 14.00 });
  matrix.add(0, std::make_shared<ryabikin::Rectangle>(rectangle2));

  BOOST_CHECK_EQUAL(rowsBefore, matrix.getRowsCount());
  BOOST_CHECK_EQUAL(maxBefore + 1, matrix.getMaxColSize());
}

BOOST_AUTO_TEST_CASE(TestErrorsDetection)
{
  ryabikin::Rectangle rectangle(4.00, 7.00, { 1.00, 1.00 });
  ryabikin::Circle circle(4.00, { 2.00, 2.00 });

  ryabikin::CompositeShape compShape;
  compShape.add(std::make_shared<ryabikin::Rectangle>(rectangle));

  ryabikin::Matrix<ryabikin::Shape> matrix;
  matrix = ryabikin::split(compShape);

  BOOST_CHECK_THROW(matrix[2], std::out_of_range);
  BOOST_CHECK_THROW(matrix[0][2], std::out_of_range);
  BOOST_CHECK_THROW(matrix.add(4, std::make_shared<ryabikin::Circle>(circle)), std::out_of_range);
  BOOST_CHECK_THROW(matrix.add(4, nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
