#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace ryabikin
{
  class Rectangle: public Shape
  {
    public:
      Rectangle(double height, double widht, const point_t &pos);
      Rectangle(double height, double widht, const point_t &pos, double m_angle);
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const double x, const double y) override;
      void move(const point_t &new_point) override;
      void scale(const double coefficient) override;
      void rotate(double angle) override;
    private:
      double m_height;
      double m_width;
      point_t m_pos;
      double m_angle;
  };
}
#endif
