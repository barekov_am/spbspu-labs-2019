#ifndef LAYERING_HPP
#define LAYERING_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace ryabikin
{
  Matrix<Shape> split(const CompositeShape &compShape);
  bool intersect(const ryabikin::Shape &shape1, const ryabikin::Shape &shape2);
}

#endif
