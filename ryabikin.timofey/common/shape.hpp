#ifndef SHAPE_HPP
#define SHAPE_HPP
#include "base-types.hpp"
#include <memory>

namespace ryabikin
{
  class Shape
  {
    public:
      virtual ~Shape() = default;
      virtual double getArea() const = 0;
      virtual rectangle_t getFrameRect() const = 0;
      virtual void move(const double x, const double y) = 0;
      virtual void move(const point_t &new_dot) = 0;
      virtual void scale(const double multiplier) = 0;
      virtual void rotate(double angle) = 0;
  };
  using shape_ptr = std::shared_ptr<Shape>;
  using shapes_array = std::unique_ptr<shape_ptr[]>;
}
#endif
