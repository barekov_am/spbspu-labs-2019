#include "rectangle.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

ryabikin::Rectangle::Rectangle(double height, double width, const point_t &pos):
  m_height(height),
  m_width(width),
  m_pos(pos),
  m_angle(0)
{
  if (height <= 0)
  {
    throw std::invalid_argument("Invalid heigt");
  }
  if (width <= 0)
  {
    throw std::invalid_argument("Invalid width");
  }
}


ryabikin::Rectangle::Rectangle(double height, double width, const point_t &pos, double angle):
Rectangle(height, width, pos)
{
  rotate (angle);
}

double ryabikin::Rectangle::getArea() const
{
  return (m_width * m_height);
}

ryabikin::rectangle_t ryabikin::Rectangle::getFrameRect() const
{
  const double sin = std::sin(m_angle * M_PI / 180);
  const double cos = std::cos(m_angle * M_PI / 180);
  const double height = m_width * std::abs(sin) + m_height * std::abs(cos);
  const double width = m_width * std::abs(cos) + m_height * std::abs(sin);

  return { width, height, m_pos };
}

void ryabikin::Rectangle::move(const double x, const double y)
{
  m_pos.x += x;
  m_pos.y += y;
}

void ryabikin::Rectangle::move(const point_t &new_point)
{
  m_pos = new_point;
}

void ryabikin::Rectangle::scale(const double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }
  m_height *= coefficient;
  m_width *= coefficient;
}

void ryabikin::Rectangle::rotate(double angle)
{
  m_angle += angle;
}
