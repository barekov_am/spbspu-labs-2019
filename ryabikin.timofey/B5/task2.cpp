#include "tasks.hpp"
#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>
#include <numeric>
#include <vector>
#include <string>
#include <locale>

struct Point
{
  int x, y;
};

using Shape = std::vector<Point>;

std::vector<Shape> readShapes(std::istream &stream)
{
  std::vector<Shape> vectorShapes;
  int n;
  while (stream >> n)
  {
    if (stream.fail() || stream.peek() != ' ')
    {
      throw std::invalid_argument("Invalid number of vertices input");
    }

    Shape shape;
    for (int i = 0; i < n; i++)
    {
      Point point;
      stream.ignore(std::numeric_limits<std::streamsize>::max(), '(');
      stream >> point.x;
      stream.ignore(std::numeric_limits<std::streamsize>::max(), ';');
      stream >> point.y;
      stream.ignore(std::numeric_limits<std::streamsize>::max(), ')');

      if (stream.fail())
      {
        throw std::invalid_argument("Invalid input of coordinates");
      }

      shape.push_back(point);
    }

    std::string leftLine;
    std::getline(stream, leftLine);
    if (std::find_if(leftLine.begin(), leftLine.end(), [](char c) {return !std::isspace(c); }) != leftLine.end())
    {
      throw std::invalid_argument("Number of coordinates is more than expected");
    }
    vectorShapes.push_back(shape);
  }
  return vectorShapes;
}

int sqDistance(const Point &a, const Point &b)
{
  return ((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
}

bool isTriangle(const Shape& shape)
{
  return (shape.size() == 3);
}

bool isRectangle(const Shape& shape)
{
  if (shape.size() == 4)
  {
    int squareDistanses[] = { sqDistance(shape[0], shape[1]), sqDistance(shape[0], shape[2]), sqDistance(shape[0], shape[3]) };
    std::swap(squareDistanses[2], *std::max_element(std::begin(squareDistanses), std::end(squareDistanses)));
    return (squareDistanses[0] + squareDistanses[1] == squareDistanses[2]);
  }
  return false;
}

bool isSquare(const Shape& shape)
{
  if (shape.size() == 4)
  {
    int squareDistanses[] = { sqDistance(shape[0], shape[1]), sqDistance(shape[0], shape[2]), sqDistance(shape[0], shape[3]) };
    std::swap(squareDistanses[2], *std::max_element(std::begin(squareDistanses), std::end(squareDistanses)));
    return (squareDistanses[0] == squareDistanses[1]) && (squareDistanses[0] * 2 == squareDistanses[2]);
  }
  return false;
}

std::vector<Point> firstPoints(std::vector<Shape> &vectorShapes)
{
  std::vector<Point> points;
  for (auto i : vectorShapes)
  {
    points.push_back(i[0]);
  }
  return points;
}

void printPoint(const Point &p, std::ostream &out)
{
  out << " (" << p.x << ";" << p.y << ")";
}

void printShape(const Shape &shape, std::ostream &out)
{
  out << shape.size();
  std::for_each(shape.begin(), shape.end(), [&out](const Point &p) {printPoint(p, out); });
  out << "\n";
}

void task2(std::istream &in, std::ostream &out)
{
  std::vector<Shape> shapes = readShapes(in);

  int totalVertices = std::accumulate(shapes.begin(), shapes.end(), 0,
    [](int sum, const Shape &sh) {return sum + sh.size(); });
  out << "Vertices: " << totalVertices << std::endl;

  int triangles = std::count_if(shapes.begin(), shapes.end(), isTriangle);
  out << "Triangles: " << triangles << std::endl;

  int squares = std::count_if(shapes.begin(), shapes.end(), isSquare);
  out << "Squares: " << squares << std::endl;

  int rectangles = std::count_if(shapes.begin(), shapes.end(), isRectangle);
  out << "Rectangles: " << rectangles << std::endl;

  shapes.erase(std::remove_if(shapes.begin(), shapes.end(),
    [](const Shape &sh) {return sh.size() == 5; }), shapes.end());

  std::vector<Point> points = firstPoints(shapes);

  out << "Points:";
  std::for_each(points.begin(), points.end(), [&out](const Point &p) {printPoint(p, out); });
  out << std::endl;

  auto triangleEnd = std::partition(shapes.begin(), shapes.end(), isTriangle);
  auto squareEnd = std::partition(triangleEnd, shapes.end(), isSquare);
  std::partition(squareEnd, shapes.end(), isRectangle);

  out << "Shapes:" << std::endl;
  std::for_each(shapes.begin(), shapes.end(), [&out](const Shape &sh) {printShape(sh, out); });

}
