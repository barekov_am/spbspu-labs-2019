#include "source.hpp"
#include <iostream>
#include <stdexcept>
#include <vector>
#include <random>
#include <ctime>
#include <cmath>
#include <cstring>

void Random(double * array, size_t size)
{
  std::mt19937 Rng(time(0));
  std::uniform_real_distribution<double> distrib (-1.0, 1.0);
  for (size_t i = 0; i < size; ++i)
  {
    array[i] = round(distrib(Rng)*10)/10;
  }
}

void punkt4(const char* direction, const char* arrsize)
{
  size_t size = atoi(arrsize);
  if (size == 0)
  {
    throw std::invalid_argument("Incorrect size.");
  }

  std::vector<double> vector (size);
  Random(&vector[0],size);

  bool (*comptr) (const double&,const double&) = nullptr;

  Container_output<std::vector<double> >(vector);
  sort <double,std::vector,access_by_vector<double,std::vector> >(vector,comptr, direction);
  Container_output<std::vector<double> >(vector);
}
