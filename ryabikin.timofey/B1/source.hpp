#ifndef SOURCE_HPP
#define SOURCE_HPP
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <vector>
#include <cstring>
#include <functional>


template <typename T,
          template <typename U,
          typename Allocator = std::allocator<U>> class Container>
struct access_by_scope
{
  typedef size_t IndexType;
  static IndexType getbegin(Container<T>&)
  {
    return 0;
  }
  static IndexType getend(Container<T>& A)
  {
    return A.size();
  }
  static T& getc(Container<T> &cont,IndexType num)
  {
   return cont[num];
  }
};

template <typename T,
          template <typename U,
          typename Allocator = std::allocator<U>> class Container>
struct access_by_vector
{
  typedef size_t IndexType;
  static IndexType getbegin(Container<T>&)
  {
    return 0;
  }
  static IndexType getend(Container<T>& A)
  {
    return A.size();
  }
  static T& getc(Container<T> &cont,IndexType num)
  {
   return cont.at(num);
  }
};

template <typename T,
          template <typename U,
          typename Allocator = std::allocator<U>> class Container>
struct access_by_iterator
{
  typedef typename Container<T>::iterator IndexType;
  static IndexType getbegin(Container<T>&A)
  {
    return A.begin();
  }
  static IndexType getend(Container<T>& A)
  {
    return A.end();
  }
  static T& getc(Container<T>&,IndexType num)
  {
   return *num;
  }
};

template <typename T>
bool more_comp  (const T a, const T b)
{
  return a < b;
}

template <typename T>
bool less_comp  (const T a, const T b)
{
  return a > b;
}

template <typename T,
          template <typename U,
          typename Allocator = std::allocator<U>> class Container,
          class Access>
void sort(Container<T>& contex, bool (*compare)(const T&,const T&), const char* direction)
{
  if (std::strcmp(direction,"ascending")==0){
    compare = more_comp;
  } else if (std::strcmp(direction,"descending")==0){
    compare = less_comp;
  } else {
    throw std::invalid_argument("Incorrect sorting direction.");
  }
  typedef typename Access::IndexType index;
  for (index i = Access::getbegin(contex); i != Access::getend(contex); ++i)
  {
    for (index j= Access::getbegin(contex); j != Access::getend(contex);  ++j)
    {
      if (compare(Access::getc(contex,i),Access::getc(contex,j)))
        std::swap(Access::getc(contex,i),Access::getc(contex,j));
    }
  }
}
template <typename D>
  void Container_output (const D& cont)
  {
    for (typename D::const_iterator k = cont.begin(); k != cont.end(); k++)
    {
      std::cout << *k << " ";
    }
    std::cout << "\n";
  }


#endif
