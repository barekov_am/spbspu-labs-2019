#include <iostream>
#include <cstring>
#include <vector>
#include <forward_list>
#include "source.hpp"
#include "punkts.hpp"

void punkt1(const char *direction)
{
  std::vector<int> vector1;
  int i = 0;
  bool (*comptr)(const int&,const int&) = nullptr;
  while (std::cin >> i)
    {
      vector1.push_back(i);
    }

    if (!std::cin.eof() && std::cin.fail())
    throw std::invalid_argument("incorrect data");
  std::vector<int> vector2 = vector1;
  std::forward_list<int> list(vector1.begin(),vector1.end());


  sort <int,std::vector,access_by_scope<int,std::vector> >(vector1,comptr, direction);
  Container_output <std::vector<int>> (vector1);

  sort <int,std::vector,access_by_vector<int,std::vector> >(vector2,comptr, direction);
  Container_output <std::vector<int>> (vector1);

  sort <int,std::forward_list,access_by_iterator<int,std::forward_list> >(list,comptr, direction);
  Container_output <std::forward_list<int>> (list);
}
