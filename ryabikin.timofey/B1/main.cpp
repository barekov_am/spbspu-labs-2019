#include "punkts.hpp"
#include <iostream>

int main (int argc, char** argv)
{
  try{
    if (argc < 2)
    {
      throw std::invalid_argument("Incorrect parameters");
    }
    switch (*argv[1])
    {
    case '1':
      if (argc == 3){
        if (argv[2] == nullptr)
          {
            std:: cerr << "Invalid";
            return 1;
          }
        punkt1(argv[2]);
      } else {
        throw std::invalid_argument("Incorrect parameters");
      }
      break;

    case '2':
      if (argc == 3){
        if (argv[2] == nullptr)
          {
            std:: cerr << "Invalid";
            return 1;
          }
        punkt2(argv[2]);
      } else {
        throw std::invalid_argument("Incorrect parameters");
      }
      break;

    case '3':
      if (argc == 2){
        punkt3();
      } else {
        throw std::invalid_argument("Incorrect parameters");
      }
      break;

    case '4':
      if (argc == 4){
        if (argv[2] == nullptr)
          {
            std:: cerr << "Invalid";
            return 1;
          }
        punkt4(argv[2],argv[3]);
      } else {
        throw std::invalid_argument("Incorrect number of input parameters.");
      }
      break;

    default:
      throw std::invalid_argument("Incorrect first input parameter.");
      break;
    }
  }
  catch (std::exception & e)
  {
    std::cerr << e.what() << "\n";
    return 1;
  }
  return 0;
}
