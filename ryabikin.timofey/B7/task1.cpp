#include <iostream>
#include <list>
#include <iterator>
#include <algorithm>
#include <cmath>

void task1()
{
  std::list<double> data({ std::istream_iterator<double>(std::cin), std::istream_iterator<double>() });
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Input failed");
  }
  std::for_each(data.begin(), data.end(), [](double &elem) {elem *= M_PI; });
  std::copy(data.begin(), data.end(), std::ostream_iterator<double>(std::cout, " "));
  std::cout << '\n';
}
