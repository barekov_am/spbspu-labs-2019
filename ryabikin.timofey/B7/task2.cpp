#include <iostream>
#include <string>
#include <list>
#include <algorithm>
#include <sstream>
#include <memory>
#include "figures.hpp"

using shapePair = std::pair<std::string, std::shared_ptr<Shape>>;

void printShapes(std::list<std::shared_ptr<Shape>> shapes)
{
  std::for_each(shapes.begin(), shapes.end(), [&](std::shared_ptr<Shape> shape) { shape->draw(std::cout); });
}

void task2()
{
  std::list<std::shared_ptr<Shape>> shapes;
  std::string shapeName;

  while (std::getline(std::cin >> std::ws, shapeName, '('))
  {
    shapeName.erase(std::remove_if(shapeName.begin(), shapeName.end(), [](char symbol) {return std::isblank(symbol); }), shapeName.end());
    std::string line;
    std::getline(std::cin, line);
    std::stringstream in(line);
    point_t center;
    in >> center.x;
    in.ignore(std::numeric_limits<std::streamsize>::max(), ';');
    in >> center.y;
    in.ignore(std::numeric_limits<std::streamsize>::max(), ')');
    if (!in)
    {
      throw std::invalid_argument("Invalid input");
    }

    std::list<shapePair> shapePairs = {
        std::make_pair("CIRCLE", std::make_shared<Circle>(center)),
        std::make_pair("TRIANGLE", std::make_shared<Triangle>(center)),
        std::make_pair("SQUARE", std::make_shared<Square>(center))
      };

    auto truePair = std::find_if(shapePairs.begin(), shapePairs.end(), [&](shapePair &pair) {return pair.first == shapeName; });
    if (truePair != shapePairs.end()) {
     shapes.push_back(truePair->second);
   }
    else {
      throw std::invalid_argument("Invalid shape name");
    }
  }

  std::cout << "Original:" << '\n';
  printShapes(shapes);

  std::cout << "Left-Right:" << '\n';
  shapes.sort([](std::shared_ptr<Shape> &a, std::shared_ptr<Shape> &b) { return a->isMoreLeft(*b); });
  printShapes(shapes);

  std::cout << "Right-Left:" << '\n';
  shapes.sort([](std::shared_ptr<Shape> &a, std::shared_ptr<Shape> &b) { return b->isMoreLeft(*a); });
  printShapes(shapes);

  std::cout << "Top-Bottom:" << '\n';
  shapes.sort([](std::shared_ptr<Shape> &a, std::shared_ptr<Shape> &b) { return a->isUpper(*b); });
  printShapes(shapes);

  std::cout << "Bottom-Top:" << '\n';
  shapes.sort([](std::shared_ptr<Shape> &a, std::shared_ptr<Shape> &b) { return b->isUpper(*a); });
  printShapes(shapes);
}
