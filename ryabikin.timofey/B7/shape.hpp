#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <ostream>

struct point_t
{
  double x;
  double y;
};

class Shape
{
public:
bool isUpper(const Shape &shape) const;
bool isMoreLeft(const Shape &shape) const;
Shape(const point_t &center);
virtual void draw(std::ostream &out) = 0;
protected:
point_t center_;
};

#endif
