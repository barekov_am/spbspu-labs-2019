#include "figures.hpp"

Square::Square(const point_t &center) :
  Shape(center)
{
}

void Square::draw(std::ostream &out)
{
  out << "SQUARE " << "(" << center_.x << ";" << center_.y << ")" << "\n";
}

Triangle::Triangle(const point_t &center) :
  Shape(center)
{
}

void Triangle::draw(std::ostream &out)
{
  out << "TRIANGLE " << "(" << center_.x << ";" << center_.y << ")" << "\n";
}

Circle::Circle(const point_t &center) :
  Shape(center)
{
}

void Circle::draw(std::ostream &out)
{
  out << "CIRCLE " << "(" << center_.x << ";" << center_.y << ")" << "\n";
}
