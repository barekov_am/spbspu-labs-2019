#ifndef FIGURES_HPP
#define FIGURES_HPP

#include "shape.hpp"

class Square: public Shape
{
public:
  Square(const point_t &center);
  void draw(std::ostream &out) override;
};

class Triangle: public Shape
{
public:
  Triangle(const point_t &center);
  void draw(std::ostream &out) override;
};

class Circle: public Shape
{
public:
  Circle(const point_t &center);
  void draw(std::ostream &out) override;
};

#endif
