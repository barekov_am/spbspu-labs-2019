#ifndef STRINGS_HPP
#define STRINGS_HPP

#include <iostream>
#include <vector>
#include <string>

struct element
{
  enum
  {  WORD, SPACE, PUNCT, NUMBER , DASH } type;
  std::string value;
};

class Strings
{
public:
  Strings(unsigned int width, std::istream &in);
  Strings(std::istream &in);

  void readNumber();
  void readWord();
  void readDash();
  void read();
  void print();
private:
  element char_;
  std::istream &istream_;
  std::vector<element> strings_;
  const unsigned int maxWidth = 40;
  const unsigned int maxLength = 20;
};

#endif
