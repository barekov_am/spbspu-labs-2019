#include <iostream>
#include "strings.hpp"

const unsigned int minLength = 25;

int main(int argc, char *argv[])
{
  try
  {
    if (argc == 3)
    {
      std::string paramName = argv[1];
      if (paramName != "--line-width")
      {
        throw std::invalid_argument("Invalid name parametr");
      }
      const unsigned int maxWidth = std::stoi(argv[2]);
      if (maxWidth < minLength)
      {
        throw std::invalid_argument("Invalid string");
      }
      Strings strings(maxWidth, std::cin);
      strings.read();
      strings.print();
    }
    else if (argc == 1)
    {
      Strings strings(std::cin);
      strings.read();
      strings.print();
    }
    else
    {
      std::cerr << "Inavlid input parametrs" << '\n';
      return 1;
    }
  }

  catch (std::exception &exception)
  {
    std::cerr << exception.what() << '\n';
    return 1;
  }
  return 0;
}
