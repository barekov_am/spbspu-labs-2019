#include "strings.hpp"

Strings::Strings(unsigned int width, std::istream &istream) :
istream_(istream),
maxWidth(width)
{
}

Strings::Strings(std::istream &istream) :
  istream_(istream)
{
}

void Strings::readNumber()
{
  int dotCounter = 0;
  char_ = { element::NUMBER, "" };

  while ((std::isdigit(istream_.peek()) || istream_.peek() == '.') && dotCounter < 2)
  {
    if (istream_.peek() == '.')
    {
      ++dotCounter;
    }
    char_.value += istream_.get();
  }

  if (char_.value.length() >= maxLength)
  {
    throw std::invalid_argument("Invalid number");
  }
}

void Strings::readWord()
{
  char_ = { element::WORD, "" };

  while (std::isalpha(istream_.peek()) || istream_.peek() == '-')
  {
    char_.value += istream_.get();
    if (char_.value.back() == '-')
    {
      if (!std::isalpha(istream_.peek()))
      {
        break;
      }
    }
  }

  if (char_.value.length() >= maxLength)
  {
    throw std::invalid_argument("Invalid word");
  }
}

void Strings::readDash()
{
  size_t counter = 1;
  char_ = { element::DASH, "" };

  while (istream_.peek() == '-')
  {
    char_.value += istream_.get();
    ++counter;
  }
  if (counter == 2 && strings_.back().value.back() == '-')
  {
    char_.value += strings_.back().value.back();
    strings_.back().value.pop_back();
    return;
  }

  if (counter != 3)
  {
    throw std::invalid_argument("It should be only three '-' symbols in the dash");
  }
}

void Strings::read()
{
  char begin = istream_.get();
  if (std::ispunct(begin) && !((begin == '+' || begin == '-') && std::isdigit(istream_.peek())))
  {
    throw std::invalid_argument("It can not start with punctuation symbol");
  }
  istream_.unget();
  while (istream_)
  {
    char first = std::cin.get();
    while (std::isblank(istream_.peek()))
    {
      istream_.get();
    }
    if (std::isalpha(first))
    {
      readWord();
      char_.value.insert(char_.value.begin(), first);
      strings_.push_back(char_);
    }
    else if ((first == '+' || first == '-' || std::isdigit(first)) &&
        (std::isdigit(std::cin.peek()) || std::cin.peek() == '.'))
    {
      readNumber();
      char_.value.insert(char_.value.begin(), first);
      strings_.push_back(char_);
    }
    else if (std::ispunct(first))
    {
      if (first == '-' && istream_.peek() == '-')
      {
        readDash();
        while (istream_.peek() == '\n' || std::isblank(istream_.peek()))
        {
          istream_.get();
        }
        if (std::ispunct(istream_.peek()))
        {
          throw std::invalid_argument("It can`t be two punctuation symbols in a raw");
        }
        char_.value.insert((char_.value.begin()), first);
        strings_.push_back(char_);
      }
      else
      {
        std::string punct;
        punct += first;
        while (istream_.peek() == '\n' || std::isblank(istream_.peek()))
        {
          istream_.get();
        }
        if ((std::ispunct(std::cin.peek()) && !(std::cin.peek() == '-' && first == ','))
            || ((strings_.back().value.back() == '-' && first == '-')))
        {
          throw std::invalid_argument("It can`t be two punctuation symbols in a raw");
        }
        char_ = { element::PUNCT, punct };
        strings_.push_back(char_);
      }
    }
  }
}

void Strings::print()
{
  if (!strings_.empty())
  {
    unsigned int lineLengthCounter = 0;
    int spaceLengthCounter = -1;
    unsigned int punctCounter = 0;
    for (auto i = strings_.begin(); i != strings_.end() - 1; i++)
    {
      punctCounter = 0;
      if (i->type == element::PUNCT)
      {
      }
      else
      {
        ++spaceLengthCounter;
      }
      lineLengthCounter += i->value.length();
      if ((i + 1)->type == element::PUNCT)
      {
        punctCounter += (i + 1)->value.length();
      }
      if ((i + 1)->type == element::DASH)
      {
        punctCounter += (i + 1)->value.length() + 1;
      }
      if (lineLengthCounter + spaceLengthCounter + punctCounter > maxWidth)
      {
        std::cout << '\n';
        lineLengthCounter = i->value.length();
        spaceLengthCounter = 0;
      }
      if (lineLengthCounter + spaceLengthCounter + (i + 1)->value.length() > maxWidth)
      {
        std::cout << i->value;
      }
      else
      {
        if ((i + 1)->type == element::PUNCT) {
          std::cout << i->value;
        }
        else
        {
         std::cout << i->value << ' ';
        }
      }
    }
    if (lineLengthCounter + strings_.back().value.length() + spaceLengthCounter > maxWidth)
       {
         std::cout << '\n' << strings_.back().value;
       }
       else
       {
         std::cout << strings_.back().value;
       }
    std::cout << '\n';
  }
}
