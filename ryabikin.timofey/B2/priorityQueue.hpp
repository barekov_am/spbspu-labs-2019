#ifndef B2_PRIORITY_QUEUE_HPP
#define B2_PRIORITY_QUEUE_HPP

#include "priorityQueueInterface.hpp"
#include "iostream"

 template <typename T>
 bool QueueWithPriority<T>::operator ==(const QueueWithPriority &other) const
 {
   return (q == other.q);
 }

 template <typename T>
 bool QueueWithPriority<T>::operator !=(const QueueWithPriority &other) const
 {
   return !(q == other.q);
 }

 template <typename T>
 void QueueWithPriority<T>::PutElementToQueue(const T &element, ElementPriority priority)
 {
   switch (priority)
   {
   case ElementPriority::HIGH:
     q.insert(q.begin() + highCounter, element);
     ++highCounter;
     break;
   case ElementPriority::NORMAL:
     q.insert(q.end() - lowCounter, element);
     break;
   case ElementPriority::LOW:
     ++lowCounter;
     q.push_back(element);
     break;
   }
 }

 template <typename T>
 void QueueWithPriority<T>::print()
 {
   for (auto i : q)
   {
     std::cout << i << " ";
   }
   std::cout << '\n';
 }

 template <typename T>
 void QueueWithPriority<T>::Accelerate()
 {
   q.insert(q.begin() + highCounter, q.end() - lowCounter, q.end());
   q.erase(q.end() - lowCounter, q.end());
 }

 template <typename T>
 T QueueWithPriority<T>::GetElementFromQueue()
 {
   return q.at(getCounter++);
 }

 template <typename T>
 bool QueueWithPriority<T>::empty()
 {
   return (q.empty() || getCounter == q.size());
 }


#endif //B2_PRIORITY_QUEUE_HPP
