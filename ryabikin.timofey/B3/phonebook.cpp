#include <iostream>
#include "phonebook.hpp"

void Phonebook::view(iterator iter) const
{
  std::cout << iter->name << " " << iter->number << std::endl;
}

void Phonebook::pushBack(const Phonebook::record_t& rec)
{
  return cont_.push_back(rec);
}

bool Phonebook::empty() const
{
  return cont_.empty();
}

Phonebook::iterator Phonebook::begin()
{
  return cont_.begin();
}

Phonebook::iterator Phonebook::end()
{
  return cont_.end();
}

Phonebook::iterator Phonebook::next(iterator iter)
{
  if (std::next(iter) != cont_.end())
  {
    return ++iter;
  }
  else
  {
    return iter;
  }
}

Phonebook::iterator Phonebook::prev(iterator iter)
{
  if (iter != cont_.begin())
  {
    return --iter;
  }
  else
  {
    return iter;
  }
}

Phonebook::iterator Phonebook::insert(iterator iter, const Phonebook::record_t& rec)
{
  return cont_.insert(iter, rec);
}

Phonebook::iterator Phonebook::erase(iterator iter)
{
  return cont_.erase(iter);
}

Phonebook::iterator Phonebook::move(iterator iter, int n)
{
  if (n >= 0)
  {
    while (std::next(iter) != cont_.end() && (n > 0))
    {
      iter = next(iter);
      --n;
    }
  }
  else
  {
    while (iter != cont_.begin() && (n < 0))
    {
      iter = prev(iter);
      ++n;
    }
  }
  return iter;
}
