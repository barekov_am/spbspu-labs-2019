#ifndef B3_FACTORIALCONTAINER_HPP
#define B3_FACTORIALCONTAINER_HPP
#include <iterator>

class FactorialIterator: public std::iterator<std::bidirectional_iterator_tag, long long>
{
public:
  FactorialIterator();
  FactorialIterator(int index);

  long long& operator *();
  long long* operator ->();

  FactorialIterator& operator ++();
  FactorialIterator operator ++(int);
  FactorialIterator& operator --();
  FactorialIterator operator --(int);

  bool operator ==(const FactorialIterator&) const;
  bool operator !=(const FactorialIterator&) const;

private:
  long long value_;
  int index_;

  long long getValue(int) const;
};

class FactorialContainer
{

public:
  FactorialContainer() = default;

  FactorialIterator begin();
  FactorialIterator end();

};

#endif
