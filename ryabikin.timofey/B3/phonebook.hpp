#ifndef B3_PHONEBOOK_HPP
#define B3_PHONEBOOK_HPP
#include <list>
#include <string>

class Phonebook
{

public:
  struct record_t
  {
    std::string name;
    std::string number;
  };

  using container = std::list<record_t>;
  using iterator = container::iterator;

  void view(iterator) const;
  void pushBack(const record_t&);

  bool empty() const;

  iterator begin();
  iterator end();
  iterator next(iterator);
  iterator prev(iterator);
  iterator insert(iterator, const record_t&);
  iterator erase(iterator);
  iterator move(iterator, int);

private:
  container cont_;

};


#endif
