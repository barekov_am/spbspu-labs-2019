#include <iostream>
#include <cstring>
#include <functional>
#include <algorithm>
#include <sstream>
#include "commands.hpp"

struct command_t
{
  const char * name;
  std::function<void(BookmarkManager &, std::stringstream &)> execute;
};

void task1()
{
  BookmarkManager manager;
  std::string line;

  while (getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input failed");
    }

    std::stringstream str(line);
    std::string command;
    str >> command;

    if (strcmp("add", command.c_str()) == 0)
    {
      executeAdd(manager, str);
    }
    else if (strcmp("store", command.c_str()) == 0)
    {
      executeStore(manager, str);
    }
    else if (strcmp("insert", command.c_str()) == 0)
    {
      executeInsert(manager, str);
    }
    else if (strcmp("delete", command.c_str()) == 0)
    {
      executeDelete(manager, str);
    }
    else if (strcmp("show", command.c_str()) == 0)
    {
      executeShow(manager, str);
    }
    else if (strcmp("move", command.c_str()) == 0)
    {
      executeMove(manager, str);
    }
    else
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }
  }
}
