#include <iostream>
#include <cassert>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"


void showRectangle_t(ryabikin::rectangle_t data)
{
  std::cout << "width = " << data.width << ", ";
  std::cout << "height = " << data.height << "\n";
  std::cout << "{X = " << data.pos.x << "; ";
  std::cout << "Y = " << data.pos.y << "}" << "\n";
}

void showAll(const ryabikin::Shape * shape)
{
  assert(shape != nullptr);
  std::cout << "area = " << shape->getArea() << ", ";
  showRectangle_t(shape->getFrameRect());
}

void PrintComposite(const ryabikin::Shape *shape)
{
  assert(shape != nullptr);
  ryabikin::rectangle_t frameRect = shape->getFrameRect();
  std::cout << "{X = " << frameRect.pos.x << "; Y = ";
  std::cout << frameRect.pos.y << "} " << "\n";
  std::cout << "width = " << frameRect.width << ", ";
  std::cout << "height = " << frameRect.height << "\n";
}


int main()
{
  ryabikin::Rectangle rectnagle1(3.00, 4.00, { 5.00, 4.00 });
  std::cout << "Rectangle  " << "\n";
  showAll(&rectnagle1);
  rectnagle1.scale(3.0);
  std::cout << "After scale - " << "\n";
  showAll(&rectnagle1);
  rectnagle1.move({ .x = 7.00, .y = 8.00 });
  std::cout << "After move - " << "\n";
  showAll(&rectnagle1);
  ryabikin::Circle circle1(3.00, { 1.00, 3.00 });
  std::cout << "Circle  " << "\n";
  showAll(&circle1);
  circle1.scale(4.00);
  std::cout << "After scale - " << "\n";
  showAll(&circle1);
  circle1.move(3.00, -1.5);
  std::cout << "After move - " << "\n";
  showAll(&circle1);

  std::cout << "\n";
  std::cout << "-Cpmposite shapes-" << "\n";
  ryabikin::Rectangle rectangle(4.00, 3.00, { 7.00, 9.00 });
  std::cout << "Area of rectangle is " << rectangle.getArea() << "\n";

  ryabikin::CompositeShape compShape1;
  compShape1.add(std::make_shared<ryabikin::Rectangle>(rectangle));
  PrintComposite(&compShape1);
  ryabikin::Circle circle(1.00, { 1.00, 1.00 });
  std::cout << "Area of circle is " << circle.getArea() << "\n";
  compShape1.add(std::make_shared<ryabikin::Circle>(circle));
  PrintComposite(&compShape1);

  std::cout << "Area of 1 composite shape = " << compShape1.getArea() << "\n";
  compShape1.move(5, 5);
  PrintComposite(&compShape1);
  compShape1.move({ 6, 8 });
  PrintComposite(&compShape1);
  compShape1.scale(2);
  std::cout << "Area of 1 composite shape after scaling = " << compShape1.getArea() << "\n";
  PrintComposite(&compShape1);
  compShape1.remove(0);
  std::cout << "Area of 1 composite shape after removing rectangle is " << compShape1.getArea() << "\n";
  ryabikin::CompositeShape compShape2(compShape1);
  std::cout << "Area of 2 composite shape is " << compShape2.getArea() << "\n";
  ryabikin::Rectangle rectangle2(2.00, 4.00, { 15.00, 15.00 });
  std::cout << "Area of rectangle is " << rectangle2.getArea() << "\n";
  compShape2.add(std::make_shared<ryabikin::Rectangle>(rectangle2));
  std::cout << "Area of 2 composite shape is " << compShape2.getArea() << "\n";

  return 0;
}
