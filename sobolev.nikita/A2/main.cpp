#include <iostream>

#include "base-types.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
#include "layering.hpp"

int main()
{
  std::cout << "________TEST RECTANGLE________\n";
  sobolev::Rectangle rectangle({2.2, -1}, 3, 4);
  sobolev::Shape *shapePtr = &rectangle;

  shapePtr->displayInfo();

  std::cout << "Move rectangle by dx and dy\n";
  shapePtr->move(-1, 1); 
  shapePtr->displayInfo();

  std::cout << "Scale rectangle x3\n";
  shapePtr->scale(3);
  shapePtr->displayInfo();
  
  std::cout << "Move rectangle by coordinate\n";
  shapePtr->move({1, 1}); 
  shapePtr->displayInfo();
  
  std::cout << "________TEST CIRCLE________\n";
  sobolev::Circle circle({2, -1.6}, 6);
  shapePtr = &circle;
  shapePtr->displayInfo();

  std::cout << "Move circle by dx and dy\n";
  shapePtr->move(-1, -1);
  shapePtr->displayInfo();

  std::cout << "Scale circle x3\n";
  shapePtr->scale(3);
  shapePtr->displayInfo();
 
  std::cout << "Move circle by coordinate\n";
  shapePtr->move({1, 1});
  shapePtr->displayInfo();
  
  std::cout << "________TEST TRIANGLE________\n";
  sobolev::Triangle triangle({1, 1}, {5, 1}, {1, 4});
  shapePtr = &triangle;
  shapePtr->displayInfo();
  
  std::cout << "Move triangle by dx and dy\n";
  shapePtr->move(-1, -1);
  shapePtr->displayInfo();

  std::cout << "Scale triangle x3\n";
  shapePtr->scale(3);
  shapePtr->displayInfo();

  std::cout << "Move triangle by coordinate\n";
  shapePtr->move({1, 1});
  shapePtr->displayInfo();
  
  std::cout << "________TEST POLYGON________\n";
  const sobolev::point_t points[4] = {{1, 1}, {1, 5}, {4, 5}, {4, 1}};
  sobolev::Polygon polygon(4, points);
  shapePtr = &polygon;
  shapePtr->displayInfo();

  std::cout << "Move polygon by dx and dy\n";
  shapePtr->move(-1, -1);
  shapePtr->displayInfo();

  std::cout << "Scale polygon x3\n";
  shapePtr->scale(3);
  shapePtr->displayInfo();

  std::cout << "Move polygon by coordinate\n";
  shapePtr->move({1, 1});
  shapePtr->displayInfo();
  
  std::cout << "Demonstration of the copy constructor:\n";
  sobolev::Polygon polygonCopy(polygon);
  polygonCopy.displayInfo();

  std::cout << "Demonstration of the copy assignment constructor:\n";
  sobolev::Polygon polygonAssigCopy = polygon;
  polygonAssigCopy.displayInfo();

  std::cout << "Demonstration of the move constructor:\n";
  sobolev::Polygon polygonMove(std::move(polygonCopy));
  polygonMove.displayInfo();

  std::cout << "Demonstration of the move assignment constructor:\n";
  sobolev::Polygon polygonAssigMove = std::move(polygonAssigCopy);
  polygonAssigMove.displayInfo();

  return 0;
}
