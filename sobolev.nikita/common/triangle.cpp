#include "triangle.hpp"

#include <iostream>
#include <algorithm>
#include <stdexcept>
#include <cmath>

sobolev::Triangle::Triangle(const point_t &pointA, const point_t &pointB, const point_t &pointC) :
  pos_({(pointA.x + pointB.x + pointC.x) / 3,
      (pointA.y + pointB.y + pointC.y) / 3}),
  pointA_(pointA),
  pointB_(pointB),
  pointC_(pointC),
  angle_(0)
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("Area mustn't be = 0");
  }

}

sobolev::Triangle::Triangle(const point_t & pointA, const point_t & pointB, const point_t & pointC, double angle) :
  Triangle(pointA, pointB, pointC)
{
  rotate(angle);
}

double sobolev::Triangle::getArea() const
{
  return std::fabs((pointA_.x - pointC_.x) * (pointB_.y - pointC_.y)
      - (pointB_.x - pointC_.x) * (pointA_.y - pointC_.y)) / 2;
}

sobolev::rectangle_t sobolev::Triangle::getFrameRect() const
{
  point_t min = {std::min(std::min(pointA_.x, pointB_.x), pointC_.x),
      std::min(std::min(pointA_.y, pointB_.y), pointC_.y)};
  point_t max = {std::max(std::max(pointA_.x, pointB_.x), pointC_.x),
      std::max(std::max(pointA_.y, pointB_.y), pointC_.y)};

  const double height = max.y - min.y;
  const double width = max.x - min.x;

  return rectangle_t{{(max.x + min.x) / 2, (max.y + min.y) / 2},
      width, height };
}

void sobolev::Triangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;

  pointA_.x += dx;
  pointA_.y += dy;

  pointB_.x += dx;
  pointB_.y += dy;

  pointC_.x += dx;
  pointC_.y += dy;
}

void sobolev::Triangle::move(const point_t &newPos)
{
  move(newPos.x - pos_.x, newPos.y - pos_.y);
}

void sobolev::Triangle::scale(double coef)
{
  if (coef <= 0.0)
  {
    throw std::invalid_argument("Invalid scaleCoef");
  }
  else
  {
    pointA_.x = pos_.x + (pointA_.x - pos_.x) * coef;
    pointA_.y = pos_.y + (pointA_.y - pos_.y) * coef;
    pointB_.x = pos_.x + (pointB_.x - pos_.x) * coef;
    pointB_.y = pos_.y + (pointB_.y - pos_.y) * coef;
    pointC_.x = pos_.x + (pointC_.x - pos_.x) * coef;
    pointC_.y = pos_.y + (pointC_.y - pos_.y) * coef;
  }
}

void sobolev::Triangle::rotate(double angle)
{
  angle_ += angle;
}

void sobolev::Triangle::displayInfo() const
{
  rectangle_t framingRectangle = getFrameRect();
  std::cout << "Triangle centre is at point ( "
      << pos_.x << "," << pos_.y << " )";
  std::cout << "\n Triangle point A has coordinates ( "
      << pointA_.x << ';' << pointA_.y << " )";
  std::cout << "\n Triangle point B has coordinates ( "
      << pointB_.x << ';' << pointB_.y << " )";
  std::cout << "\n Triangle point C has coordinates ( "
      << pointC_.x << ';' << pointC_.y << " )";
  std::cout << "\n Triangle frame width is "
      << framingRectangle.width;
  std::cout << "\n Triangle frame height is "
      << framingRectangle.height;
  std::cout << "\n Area is " << getArea() << "\n\n";
}

sobolev::point_t sobolev::Triangle::getCentre() const
{
  point_t min = { std::min(std::min(pointA_.x, pointB_.x), pointC_.x),
      std::min(std::min(pointA_.y, pointB_.y), pointC_.y) };
  point_t max = { std::max(std::max(pointA_.x, pointB_.x), pointC_.x),
      std::max(std::max(pointA_.y, pointB_.y), pointC_.y) };

  return point_t({ (max.x + min.x) / 2, (max.y + min.y) / 2 });
}
