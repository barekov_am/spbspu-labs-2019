#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "shape.hpp"

namespace sobolev
{
  class Polygon : public Shape
  {
  public:
    Polygon(const Polygon & other);
    Polygon(Polygon && other);
    Polygon(int vertNum, const point_t * vertex);
    Polygon(int vertNum, const point_t * vertex, double angle);
    ~Polygon() override;

    Polygon & operator =(const Polygon & other);
    Polygon & operator =(Polygon && other);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &newPos) override;
    void scale(double coef) override;
    void rotate(double angle) override;
    void displayInfo() const override;

  private:
    int vertNum_;
    point_t * vertex_;
    double angle_;
    bool isConvex() const;
    point_t getCentre() const override;
  };
}

#endif
