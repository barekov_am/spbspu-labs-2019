#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(rectangleTestSuite)

BOOST_AUTO_TEST_CASE(rectangleTestImmutability)
{
  sobolev::Rectangle testRect({2.2, -1}, 3, 4 );
  const double rectangleAreaBeforeMoving = testRect.getArea();
  const sobolev::rectangle_t rectangleFrameRectBeforeMoving = testRect.getFrameRect();

  testRect.move(-1, 1);

  const sobolev::rectangle_t rectangleFrameRectAfterMoving = testRect.getFrameRect();
  BOOST_CHECK_CLOSE(rectangleFrameRectAfterMoving.width, rectangleFrameRectBeforeMoving.width, EPSILON);
  BOOST_CHECK_CLOSE(rectangleFrameRectAfterMoving.height, rectangleFrameRectBeforeMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(testRect.getArea(), rectangleAreaBeforeMoving, EPSILON);

  testRect.move({1, 1});

  const sobolev::rectangle_t rectangleFrameRectAfterMovingTo = testRect.getFrameRect();
  BOOST_CHECK_CLOSE(rectangleFrameRectAfterMovingTo.width, rectangleFrameRectBeforeMoving.width, EPSILON);
  BOOST_CHECK_CLOSE(rectangleFrameRectAfterMovingTo.height, rectangleFrameRectBeforeMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(testRect.getArea(), rectangleAreaBeforeMoving, EPSILON);
}

BOOST_AUTO_TEST_CASE(rectangleTestScale)
{
  sobolev::Rectangle testRect({2.2, -1}, 3, 4);
  const double rectangleAreaBeforeScaling = testRect.getArea();

  testRect.scale(3);

  BOOST_CHECK_CLOSE(testRect.getArea(), rectangleAreaBeforeScaling * 3 * 3, EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidRectangleParametrs)
{
  BOOST_CHECK_THROW(sobolev::Rectangle({2.2, -1}, 3, -1), std::invalid_argument);
  BOOST_CHECK_THROW(sobolev::Rectangle({2.2, -1}, -1, 4), std::invalid_argument);
  BOOST_CHECK_THROW(sobolev::Rectangle({2.2, -1}, -1, -1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidRectangleScaleCoef)
{
  sobolev::Rectangle testRect({2.2, -1}, 3, 4);
  BOOST_CHECK_THROW(testRect.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
