#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"
#include <memory>

namespace sobolev
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
  
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void move(const point_t &newPos) = 0;
    virtual void scale(double coef) = 0;
    virtual void rotate(double angle) = 0;
    virtual point_t getCentre() const = 0;
    virtual void displayInfo() const = 0;
  };

  using shape_ptr = std::shared_ptr<Shape>;
  using shapes_array = std::unique_ptr<shape_ptr[]>;
}

#endif
