#define _USE_MATH_DEFINES

#include "circle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

sobolev::Circle::Circle(const point_t &pos, double radius) :
  pos_(pos),
  radius_(radius)
{
  if (radius <= 0.0)
  {
    throw std::invalid_argument("Invalid radius");
  }
}

double sobolev::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

sobolev::rectangle_t sobolev::Circle::getFrameRect() const
{
  return {pos_, 2 * radius_, 2 * radius_};
}

void sobolev::Circle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void sobolev::Circle::move(const point_t &newPos)
{
  pos_ = newPos;
}

void sobolev::Circle::scale(const double coef)
{
  if (coef <= 0.0)
  {
    throw std::invalid_argument("Invalid coef");
  }
  radius_ *= coef;
}

void sobolev::Circle::rotate(double)
{
}

void sobolev::Circle::displayInfo() const
{
  rectangle_t framingRectangle = getFrameRect();
  std::cout << "\n Circle frame width is " << framingRectangle.width;
  std::cout << "\n Circle frame height is " << framingRectangle.height;
  std::cout << "\n Circle is at point ( x: " << pos_.x << ", y: " << pos_.y << " )";
  std::cout << "\n Circle area is " << getArea() << "\n\n";
}

sobolev::point_t sobolev::Circle::getCentre() const
{
  return point_t(pos_);
}
