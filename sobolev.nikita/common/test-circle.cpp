#include <boost/test/auto_unit_test.hpp>
#include <cmath>
#include "circle.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(circleTestSuite)

BOOST_AUTO_TEST_CASE(circleTestImmutability)
{
  sobolev::Circle testCircle({2, -1.6}, 6);
  const double circleAreaBeforeMoving = testCircle.getArea();
  const sobolev::rectangle_t circleFrameRectBeforeMoving = testCircle.getFrameRect();

  testCircle.move(-1, 1);

  const sobolev::rectangle_t circleFrameRectAfterMoving = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(circleFrameRectAfterMoving.width, circleFrameRectBeforeMoving.width, EPSILON);
  BOOST_CHECK_CLOSE(circleFrameRectAfterMoving.height, circleFrameRectBeforeMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(testCircle.getArea(), circleAreaBeforeMoving, EPSILON);

  testCircle.move({1, 1});

  const sobolev::rectangle_t circleFrameRectAfterMovingTo = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(circleFrameRectAfterMovingTo.width, circleFrameRectBeforeMoving.width, EPSILON);
  BOOST_CHECK_CLOSE(circleFrameRectAfterMovingTo.height, circleFrameRectBeforeMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(testCircle.getArea(), circleAreaBeforeMoving, EPSILON);
}

BOOST_AUTO_TEST_CASE(circleTestScale)
{
  sobolev::Circle testCircle({2, -1.6}, 6);
  const double circleAreaBeforeScaling = testCircle.getArea();

  testCircle.scale(3);

  BOOST_CHECK_CLOSE(testCircle.getArea(), circleAreaBeforeScaling * 3 * 3, EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidCircleRadius)
{
  BOOST_CHECK_THROW(sobolev::Circle({2, -1.6}, -1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidCircleScaleCoef)
{
  sobolev::Circle testCircle({2, -1.6}, 6);
  BOOST_CHECK_THROW(testCircle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
