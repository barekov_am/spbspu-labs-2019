#include <stdexcept>
#include <algorithm>
#include <boost/test/auto_unit_test.hpp>
#include "layering.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(matrixTesting)

BOOST_AUTO_TEST_CASE(copyConstructor)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  sobolev::Matrix<sobolev::Shape> matrix;
  matrix = sobolev::split(compShape);
  sobolev::Matrix<sobolev::Shape> matrix1(matrix);
  BOOST_CHECK_EQUAL(matrix.getRowsCount(), matrix1.getRowsCount());
  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), matrix1.getMaxColSize());
  BOOST_CHECK(matrix[0][0] == matrix1[0][0]);
  BOOST_CHECK(matrix[1][0] == matrix1[1][0]);
}

BOOST_AUTO_TEST_CASE(moveConstructor)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  sobolev::Matrix<sobolev::Shape> matrix;
  matrix = sobolev::split(compShape);
  sobolev::Matrix<sobolev::Shape> matrixClone(matrix);

  sobolev::Matrix<sobolev::Shape> matrix1(std::move(matrix));
  BOOST_CHECK_EQUAL(matrixClone.getRowsCount(), matrix1.getRowsCount());
  BOOST_CHECK_EQUAL(matrixClone.getMaxColSize(), matrix1.getMaxColSize());
  BOOST_CHECK(matrixClone[0][0] == matrix1[0][0]);
  BOOST_CHECK(matrixClone[1][0] == matrix1[1][0]);
  BOOST_CHECK_EQUAL(matrix.getRowsCount(), 0);
  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), 0);
}

BOOST_AUTO_TEST_CASE(copyAssigOperator)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  sobolev::Matrix<sobolev::Shape> matrix = sobolev::split(compShape);
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  sobolev::Matrix<sobolev::Shape> matrix1 = sobolev::split(compShape);

  matrix1 = matrix;

  BOOST_CHECK_EQUAL(matrix.getRowsCount(), matrix1.getRowsCount());
  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), matrix1.getMaxColSize());
  BOOST_CHECK(matrix[0][0] == matrix1[0][0]);
  BOOST_CHECK(matrix[1][0] == matrix1[1][0]);
}

BOOST_AUTO_TEST_CASE(moveAssigOperator)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  sobolev::Matrix<sobolev::Shape> matrix;
  matrix = sobolev::split(compShape);
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  sobolev::Matrix<sobolev::Shape> matrixClone(matrix);
  sobolev::Matrix<sobolev::Shape> matrix1;
  matrix1 = sobolev::split(compShape);
  matrix1 = std::move(matrix);

  BOOST_CHECK_EQUAL(matrixClone.getRowsCount(), matrix1.getRowsCount());
  BOOST_CHECK_EQUAL(matrixClone.getMaxColSize(), matrix1.getMaxColSize());
  BOOST_CHECK(matrixClone[0][0] == matrix1[0][0]);
  BOOST_CHECK(matrixClone[1][0] == matrix1[1][0]);
  BOOST_CHECK_EQUAL(matrix.getRowsCount(), 0);
  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), 0);
}

BOOST_AUTO_TEST_CASE(addingTesting)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  sobolev::Matrix<sobolev::Shape> matrix;
  matrix = sobolev::split(compShape);
  size_t rowsBefore = matrix.getRowsCount();
  size_t maxBefore = matrix.getMaxColSize();

sobolev::Rectangle rec2({10, 10}, 4, 4);
matrix.add(0, std::make_shared<sobolev::Rectangle>(testRectangle));

BOOST_CHECK_EQUAL(rowsBefore, matrix.getRowsCount());
BOOST_CHECK_EQUAL(maxBefore + 1, matrix.getMaxColSize());
}

BOOST_AUTO_TEST_CASE(errorsDetectionTesting)
{
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  sobolev::Matrix<sobolev::Shape> matrix;
  matrix = sobolev::split(compShape);

  BOOST_CHECK_THROW(matrix[2], std::out_of_range);
  BOOST_CHECK_THROW(matrix[0][2], std::out_of_range);
  BOOST_CHECK_THROW(matrix.add(4, std::make_shared<sobolev::Circle>(testCircle)), std::out_of_range);
  BOOST_CHECK_THROW(matrix.add(4, nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
