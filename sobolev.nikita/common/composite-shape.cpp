#define _USE_MATH_DEFINES
#include "composite-shape.hpp"

#include <cstddef>
#include <stdexcept>
#include <cmath>
#include <algorithm>
#include <memory>

sobolev::CompositeShape::CompositeShape() :
  size_(0)
{
}

sobolev::CompositeShape::CompositeShape(const sobolev::CompositeShape &copy) :
  size_(copy.size_),
  shapes_(std::make_unique<shape_ptr[]>(copy.size_))
{
  for (std::size_t i = 0; i < size_; i++)
  {
    shapes_[i] = copy.shapes_[i];
  }
}

sobolev::CompositeShape::CompositeShape(sobolev::CompositeShape &&move) :
  size_(move.size_),
  shapes_(std::move(move.shapes_))
{
  move.size_ = 0;
}

sobolev::CompositeShape & sobolev::CompositeShape::operator=(const sobolev::CompositeShape &copy)
{
  if (this == &copy)
  {
    return *this;
  }

  shapes_ = std::make_unique<shape_ptr[]>(copy.size_);
  size_ = copy.size_;

  for (std::size_t i = 0; i < size_; i++)
  {
    shapes_[i] = copy.shapes_[i];
  }

  return *this;
}

sobolev::CompositeShape & sobolev::CompositeShape::operator=(sobolev::CompositeShape &&move)
{
  if (this == &move)
  {
    return *this;
  }
  size_ = move.size_;
  move.size_ = 0;
  shapes_ = std::move(move.shapes_);

  return *this;
}

sobolev::shape_ptr sobolev::CompositeShape::operator[](const std::size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Index is out of range");
  }

  return shapes_[index];
}

double sobolev::CompositeShape::getArea() const
{
  checkSize();

  double totalArea = 0.0;
  for (std::size_t i = 0; i < size_; i++)
  {
    totalArea += shapes_[i]->getArea();
  }

  return totalArea;
}

sobolev::rectangle_t sobolev::CompositeShape::getFrameRect() const
{
  checkSize();

  sobolev::rectangle_t frameRect = shapes_[0]->getFrameRect();
  double minX = frameRect.pos.x - frameRect.width / 2;
  double minY = frameRect.pos.y - frameRect.height / 2;
  double maxX = frameRect.pos.x + frameRect.width / 2;
  double maxY = frameRect.pos.y + frameRect.height / 2;

  for (std::size_t i = 1; i < size_; i++)
  {
    frameRect = shapes_[i]->getFrameRect();
    minX = std::min(frameRect.pos.x - frameRect.width / 2, minX);
    maxX = std::max(frameRect.pos.x + frameRect.width / 2, maxX);
    minY = std::min(frameRect.pos.y - frameRect.height / 2, minY);
    maxY = std::max(frameRect.pos.y + frameRect.height / 2, maxY);
  }

  return sobolev::rectangle_t{ {(minX + maxX) / 2, (minY + maxY) / 2}, (maxX - minX), (maxY - minY) };
}

void sobolev::CompositeShape::move(double dx, double dy)
{
  checkSize();

  for (std::size_t i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void sobolev::CompositeShape::move(const sobolev::point_t &newPos)
{
  checkSize();

  const sobolev::point_t oldPos = getFrameRect().pos;
  const double dx = newPos.x - oldPos.x;
  const double dy = newPos.y - oldPos.y;

  move(dx, dy);
}

void sobolev::CompositeShape::scale(double coef)
{
  checkSize();

  if (coef <= 0.0)
  {
    throw std::invalid_argument("Coefficient of scale must be a positive number");
  }

  sobolev::point_t FrameRect = getFrameRect().pos;
  for (std::size_t i = 0; i < size_; i++)
  {
    const sobolev::point_t centreShape = shapes_[i]->getCentre();
    const double dx = (centreShape.x - FrameRect.x) * (coef - 1);
    const double dy = (centreShape.y - FrameRect.y) * (coef - 1);

    shapes_[i]->move(dx, dy);
    shapes_[i]->scale(coef);
  }
}

std::size_t sobolev::CompositeShape::getSize() const
{
  return std::size_t(size_);
}

void sobolev::CompositeShape::addShape(shape_ptr newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Shape is nullptr");
  }

  size_++;
  shapes_array tempShapes = std::make_unique<shape_ptr[]>(size_);
  for (std::size_t i = 0; i < size_ - 1; i++)
  {
    tempShapes[i] = shapes_[i];
  }

  tempShapes[size_-1] = newShape;
  shapes_ = std::move(tempShapes);
}

void sobolev::CompositeShape::deleteShape(std::size_t deleteIndex)
{
  if (deleteIndex >= size_)
  {
    throw std::out_of_range("Index is out of range");
  }
  for (size_t i = deleteIndex; i < size_ - 1; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  shapes_[size_ - 1] = nullptr;
  size_--;
}

void sobolev::CompositeShape::rotate(double angle)
{
  const sobolev::point_t center = getFrameRect().pos;
  const double cosine = std::abs(std::cos(angle * M_PI / 180));
  const double sinus = std::abs(std::sin(angle * M_PI / 180));
  for (size_t i = 0; i < size_; ++i)
  {
    const sobolev::point_t currCenter = shapes_[i]->getFrameRect().pos;
    const double projection_x = currCenter.x - center.x;
    const double projection_y = currCenter.y - center.y;
    const double shift_x = projection_x * (cosine - 1) - projection_y * sinus;
    const double shift_y = projection_x * sinus + projection_y * (cosine - 1);
    shapes_[i]->move(shift_x, shift_y);
    shapes_[i]->rotate(angle);
  }
}

void sobolev::CompositeShape::displayInfo() const
{
  const sobolev::rectangle_t frameRect = getFrameRect();
  std::cout << "Framing rectangle width: " << frameRect.width << std::endl;
  std::cout << "Framing rectangle height: " << frameRect.height << std::endl;
  std::cout << "Framing rectangle center: (" << frameRect.pos.x
      << "; " << frameRect.pos.y << ")\n";
  std::cout << "Area = " << getArea() << std::endl << std::endl;
}

void sobolev::CompositeShape::checkSize() const
{
  if (size_ == 0)
  {
    throw std::invalid_argument("Composite shape is empty");
  }
}

sobolev::point_t sobolev::CompositeShape::getCentre() const
{
  return point_t(getFrameRect().pos);
}
