#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include <iostream>
#include <cstddef>

namespace sobolev
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &);
    CompositeShape &operator =(CompositeShape &&);
    shape_ptr operator [](const std::size_t) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double, double) override;
    void move(const point_t &) override;
    void scale(double) override;
    std::size_t getSize() const;
    void addShape(shape_ptr);
    void deleteShape(std::size_t);
    void rotate(double angle) override;
    void displayInfo() const override;

  private:
    std::size_t size_;
    shapes_array shapes_;
    void checkSize() const;
    point_t getCentre() const override;
  };
}

#endif
