#include <boost/test/auto_unit_test.hpp>
#include "triangle.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(triangleTestSuite)

BOOST_AUTO_TEST_CASE(triangleTestImmutabilityAfterMoving)
{
  sobolev::Triangle testTriangle({ 1, 1 }, { 5, 1 }, { 1, 4 });
  const double triangleAreaBeforeMoving = testTriangle.getArea();
  const sobolev::rectangle_t triangleFrameRectBeforeMoving = testTriangle.getFrameRect();

  testTriangle.move(-1, 1);

  const sobolev::rectangle_t triangleFrameRectAfterMoving = testTriangle.getFrameRect();
  BOOST_CHECK_CLOSE(triangleFrameRectAfterMoving.width, triangleFrameRectBeforeMoving.width, EPSILON);
  BOOST_CHECK_CLOSE(triangleFrameRectAfterMoving.height, triangleFrameRectBeforeMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(testTriangle.getArea(), triangleAreaBeforeMoving, EPSILON);

  testTriangle.move({1, 1});

  const sobolev::rectangle_t triangleFrameRectAfterMovingTo = testTriangle.getFrameRect();
  BOOST_CHECK_CLOSE(triangleFrameRectAfterMovingTo.width, triangleFrameRectBeforeMoving.width, EPSILON);
  BOOST_CHECK_CLOSE(triangleFrameRectAfterMovingTo.height, triangleFrameRectBeforeMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(testTriangle.getArea(), triangleAreaBeforeMoving, EPSILON);
}

BOOST_AUTO_TEST_CASE(triangleTestScale)
{
  sobolev::Triangle testTriangle({1, 1}, {5, 1}, {1, 4});
  const double triangleAreaBeforeScaling = testTriangle.getArea();

  testTriangle.scale(3);

  BOOST_CHECK_CLOSE(testTriangle.getArea(), triangleAreaBeforeScaling * 3 * 3, EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidTriangleParametrs)
{
  BOOST_CHECK_THROW(sobolev::Triangle({1, 1}, {1, 1}, {1, 4}), std::invalid_argument);
  BOOST_CHECK_THROW(sobolev::Triangle({1, 1}, {5, 1}, {4, 1}), std::invalid_argument);
  BOOST_CHECK_THROW(sobolev::Triangle({1, 1}, {1, 5}, {1, 4}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidTriangleScaleCoef)
{
  sobolev::Triangle testTriangle({1, 1}, {5, 1}, {1, 4});
  BOOST_CHECK_THROW(testTriangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

