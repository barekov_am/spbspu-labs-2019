
#ifndef LAYERING_HPP
#define LAYERING_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace sobolev
{
  Matrix<Shape> split(const CompositeShape &compShape);
  bool intersect(const sobolev::Shape &shape1, const sobolev::Shape &shape2);
}

#endif
