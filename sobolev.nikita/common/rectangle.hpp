#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace sobolev
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t &pos, double width, double height);
    Rectangle(const point_t &pos, double width, double height, double angle);
  
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &newPos) override;
    void scale(double coef) override;
    void rotate(double angle) override;
    void displayInfo() const override;
  
  private:
    point_t pos_;
    double width_;
    double height_;
    double angle_;
    virtual point_t getCentre() const override;
  };
}

#endif
