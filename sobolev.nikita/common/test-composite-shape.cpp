#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(compositeShape)

BOOST_AUTO_TEST_CASE(copyConstructor)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));
  
  sobolev::CompositeShape compyCompShape(compShape);
  const double area = compShape.getArea();
  const double copyArea = compyCompShape.getArea();
  const sobolev::rectangle_t frame = compShape.getFrameRect();
  const sobolev::rectangle_t copyFrame = compyCompShape.getFrameRect();

  BOOST_CHECK_CLOSE(area, copyArea, EPSILON);
  BOOST_CHECK_CLOSE(frame.height, copyFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(frame.width, copyFrame.width, EPSILON);
  BOOST_CHECK_CLOSE(frame.pos.x, copyFrame.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frame.pos.y, copyFrame.pos.y, EPSILON);
  BOOST_CHECK_EQUAL(compShape.getSize(), compyCompShape.getSize());
}

BOOST_AUTO_TEST_CASE(moveConstructor)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));
  
  const double area = compShape.getArea();
  const sobolev::rectangle_t frame = compShape.getFrameRect();
  const int amount = compShape.getSize();
  
  sobolev::CompositeShape moveCompShape(std::move(compShape));
  const double moveArea = moveCompShape.getArea();
  const sobolev::rectangle_t moveFrame = moveCompShape.getFrameRect();
  const int moveAmount = moveCompShape.getSize();

  BOOST_CHECK_CLOSE(area, moveArea, EPSILON);
  BOOST_CHECK_CLOSE(frame.height, moveFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(frame.width, moveFrame.width, EPSILON);
  BOOST_CHECK_CLOSE(frame.pos.x, moveFrame.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frame.pos.y, moveFrame.pos.y, EPSILON);
  BOOST_CHECK_EQUAL(amount, moveAmount);

  BOOST_CHECK_EQUAL(compShape.getSize(), 0);
  BOOST_CHECK_THROW(compShape.getArea(), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(copyAssigOperator)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  sobolev::CompositeShape copyAssigCompShape;
  sobolev::Rectangle copyAssigRect({7, 3}, 8, 10);
  compShape.addShape(std::make_shared<sobolev::Rectangle>(copyAssigRect));
  
  copyAssigCompShape = compShape;
  const double area = compShape.getArea();
  const double copyAssigArea = copyAssigCompShape.getArea();
  const sobolev::rectangle_t frame = compShape.getFrameRect();
  const sobolev::rectangle_t copyAssigCompFrame = copyAssigCompShape.getFrameRect();
  
  BOOST_CHECK_CLOSE(area, copyAssigArea, EPSILON);
  BOOST_CHECK_CLOSE(frame.height, copyAssigCompFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(frame.width, copyAssigCompFrame.width, EPSILON);
  BOOST_CHECK_CLOSE(frame.pos.x, copyAssigCompFrame.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frame.pos.y, copyAssigCompFrame.pos.y, EPSILON);
  BOOST_CHECK_EQUAL(compShape.getSize(), copyAssigCompShape.getSize());
}

BOOST_AUTO_TEST_CASE(moveAssigOperator)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  const double area = compShape.getArea();
  const sobolev::rectangle_t frame = compShape.getFrameRect();
  const size_t amount = compShape.getSize();
  
  sobolev::CompositeShape moveAssigCompShape = std::move(compShape);
  const double moveAssigArea = moveAssigCompShape.getArea();
  const sobolev::rectangle_t moveAssigFrame = moveAssigCompShape.getFrameRect();
  const size_t moveAssigAmount = moveAssigCompShape.getSize();
  
  BOOST_CHECK_CLOSE(area, moveAssigArea, EPSILON);
  BOOST_CHECK_CLOSE(frame.height, moveAssigFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(frame.width, moveAssigFrame.width, EPSILON);
  BOOST_CHECK_CLOSE(frame.pos.x, moveAssigFrame.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frame.pos.y, moveAssigFrame.pos.y, EPSILON);
  BOOST_CHECK_EQUAL(amount, moveAssigAmount);
  
  BOOST_CHECK_EQUAL(compShape.getSize(), 0);
  BOOST_CHECK_THROW(compShape.getArea(), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(comShapeInvariabilityAfterMovingInItself)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  const double area = compShape.getArea();
  const sobolev::rectangle_t frame = compShape.getFrameRect();
  const size_t amount = compShape.getSize();
  
  compShape = std::move(compShape);
  
  BOOST_CHECK_CLOSE(area, compShape.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(frame.height, compShape.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(frame.width, compShape.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frame.pos.x, compShape.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frame.pos.y, compShape.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_EQUAL(amount, compShape.getSize());
}

BOOST_AUTO_TEST_CASE(immutabilityOfcompositeShapeAfterChangingCenter)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  const double areaBeforeChangingCenter = compShape.getArea();
  const sobolev::rectangle_t frameBeforeChangingCenter = compShape.getFrameRect();
  compShape.move({2, 2});
  const double areaAfterChangingCenter = compShape.getArea();
  const sobolev::rectangle_t frameAfterChangingCenter = compShape.getFrameRect();
  
  BOOST_CHECK_CLOSE(areaBeforeChangingCenter, areaAfterChangingCenter, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeChangingCenter.width, frameAfterChangingCenter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeChangingCenter.height, frameAfterChangingCenter.height, EPSILON);
}

BOOST_AUTO_TEST_CASE(immutabilityOfcompositeShapeAfterMoving)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  const double areaBeforeDisplacement = compShape.getArea();
  const sobolev::rectangle_t frameBeforeDisplacement = compShape.getFrameRect();
  compShape.move(7, 8);
  const double areaAfterDisplacement = compShape.getArea();
  const sobolev::rectangle_t frameAfterDisplacement = compShape.getFrameRect();

  BOOST_CHECK_CLOSE(areaBeforeDisplacement, areaAfterDisplacement, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeDisplacement.width, frameAfterDisplacement.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeDisplacement.height, frameAfterDisplacement.height, EPSILON);
}

BOOST_AUTO_TEST_CASE(changeCompositeShapeAreaAfterScaling)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  const double areaBeforeScaling = compShape.getArea();
  const double factor1 = 2;
  compShape.scale(factor1);

  const double areaAfterScaling = compShape.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScaling * factor1 * factor1, areaAfterScaling, EPSILON);

  const double areaBeforeScaling1 = compShape.getArea();
  const double factor2 = 0.25;
  compShape.scale(factor2);
  
  const double areaAfterScaling1 = compShape.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScaling1 * factor2 * factor2, areaAfterScaling1, EPSILON);
}

BOOST_AUTO_TEST_CASE(compShapeFrameChangeAfterScaling)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  const sobolev::rectangle_t frameBeforeScaling = compShape.getFrameRect();
  double factor = 1.6;
  compShape.scale(factor);
  
  const sobolev::rectangle_t frameAfterScaling = compShape.getFrameRect();
  BOOST_CHECK_CLOSE(frameBeforeScaling.width * factor, frameAfterScaling.width, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeScaling.height * factor, frameAfterScaling.height, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeScaling.pos.x, frameAfterScaling.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameBeforeScaling.pos.y, frameAfterScaling.pos.y, EPSILON);
  
  factor = 0.6;
  compShape.scale(factor);
  
  const sobolev::rectangle_t frameAfterScaling1 = compShape.getFrameRect();
  BOOST_CHECK_CLOSE(frameAfterScaling.width * factor, frameAfterScaling1.width, EPSILON);
  BOOST_CHECK_CLOSE(frameAfterScaling.height * factor, frameAfterScaling1.height, EPSILON);
  BOOST_CHECK_CLOSE(frameAfterScaling.pos.x, frameAfterScaling1.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameAfterScaling.pos.y, frameAfterScaling1.pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(compShapeAfterAdding)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  const size_t amountWithoutCircle = compShape.getSize();
  const double areaBeforeAdding = compShape.getArea();
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));
  const size_t amountWithCircle = compShape.getSize();
  const double areaOfAddedShape = testCircle.getArea();
  const double areaAfterAdding = compShape.getArea();
  BOOST_CHECK_CLOSE(areaBeforeAdding + areaOfAddedShape, areaAfterAdding, EPSILON);
  BOOST_CHECK_EQUAL(amountWithoutCircle + 1, amountWithCircle);
}

BOOST_AUTO_TEST_CASE(compShapeAfterDeleting)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  const size_t amountWithCircle = compShape.getSize();
  const double areaBeforeDeleting = compShape.getArea();
  const double areaOfDeletedShape = testCircle.getArea();
  compShape.deleteShape(1);
  const size_t amountWithoutCircle = compShape.getSize();
  const double areaAfterDeleting = compShape.getArea();
  BOOST_CHECK_CLOSE(areaBeforeDeleting - areaOfDeletedShape, areaAfterDeleting, EPSILON);
  BOOST_CHECK_EQUAL(amountWithCircle - 1, amountWithoutCircle);
}

BOOST_AUTO_TEST_CASE(logicErrorsDetection)
{
sobolev::CompositeShape compShape;

BOOST_CHECK_THROW(compShape.getArea(), std::logic_error);
BOOST_CHECK_THROW(compShape.deleteShape(0), std::logic_error);
BOOST_CHECK_THROW(compShape.getFrameRect(), std::logic_error);
BOOST_CHECK_THROW(compShape.move(4, 4), std::logic_error);
BOOST_CHECK_THROW(compShape.move({4, 4}), std::logic_error);
BOOST_CHECK_THROW(compShape.scale(4), std::logic_error);
}

BOOST_AUTO_TEST_CASE(invalidArgumentsErrorsDetection)
{
sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);

sobolev::CompositeShape compShape;
compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
BOOST_CHECK_THROW(compShape.scale(-2), std::invalid_argument);
BOOST_CHECK_THROW(compShape.addShape(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(outOfRangeErrorsDetection)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  BOOST_CHECK_THROW(compShape[5], std::out_of_range);
  BOOST_CHECK_THROW(compShape.deleteShape(-1), std::out_of_range);
  BOOST_CHECK_THROW(compShape.deleteShape(10), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compShapeAfterRotation)
{
  sobolev::Rectangle testRectangle({ 2.2, -1 }, 3, 4);
  sobolev::Circle testCircle({ 2, -1.6 }, 6);

  sobolev::CompositeShape compShape;
  compShape.addShape(std::make_shared<sobolev::Rectangle>(testRectangle));
  compShape.addShape(std::make_shared<sobolev::Circle>(testCircle));

  const sobolev::rectangle_t frameBefore = compShape.getFrameRect();
  const double areaBefore = compShape.getArea();
  compShape.rotate(90);
  const double areaAfter = compShape.getArea();
  const sobolev::rectangle_t frameAfter = compShape.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, EPSILON);
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.height, EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()
