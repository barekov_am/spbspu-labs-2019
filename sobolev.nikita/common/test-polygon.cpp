#include <boost/test/auto_unit_test.hpp>
#include "polygon.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(polygonTestSuite)

BOOST_AUTO_TEST_CASE(polygonThrowNullTest)
{
  sobolev::point_t *points = nullptr;
  BOOST_CHECK_THROW(sobolev::Polygon polygon(2, points), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(polygonThrowSizeTest)
{ 
  sobolev::point_t points[] = {{1, 2}, {4, 3}};
  BOOST_CHECK_THROW(sobolev::Polygon polygon(2, points), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(polygonThrowConvexTest)
{
  sobolev::point_t points[] = {{2, 1}, {4, 6}, {7, 8}, {8, 6}, {5, 4}, {3, 7}};
  BOOST_CHECK_THROW(sobolev::Polygon polygon(6, points), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(polygonConstrucorOrderTest)
{
  sobolev::point_t points[] = {{2, 1}, {4, 6}, {7, 8}, {9, 5}, {4, 1}};
  BOOST_CHECK_NO_THROW(sobolev::Polygon polygon(5, points));
}

BOOST_AUTO_TEST_CASE(polygonConstrucorCopyTest)
{
  sobolev::point_t points[] = {{2, 1}, {4, 6}, {7, 8}, {9, 5}, {4, 1}};
  sobolev::Polygon polygon_c(5, points);
  BOOST_CHECK_NO_THROW(sobolev::Polygon polygon(polygon_c));
}

BOOST_AUTO_TEST_CASE(polygonConstrucorOperCopyTest)
{
  sobolev::point_t points_c[] = {{2, 1}, {4, 6}, {7, 8}, {9, 5}, {4, 1}};
  sobolev::Polygon polygon_c(5, points_c);

  sobolev::point_t points[] = {{1, 1}, {1, 2}, {2, 2}, {2, 1}};
  sobolev::Polygon polygon(4, points);
  BOOST_CHECK_NO_THROW(polygon = polygon_c);
}

BOOST_AUTO_TEST_CASE(invalidPolygonScaleCoef)
{
  sobolev::point_t points[] = {{2, 1}, {4, 6}, {7, 8}, {9, 5}, {4, 1}};
  sobolev::Polygon polygon(5, points);
  BOOST_CHECK_THROW(polygon.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(polygonTestScale)
{
  sobolev::point_t points[] = {{2, 1}, {4, 6}, {7, 8}, {9, 5}, {4, 1}};
  sobolev::Polygon polygon(5, points);
  const double polygonAreaBeforeScaling = polygon.getArea();

  polygon.scale(3);
  
  BOOST_CHECK_CLOSE(polygon.getArea(), polygonAreaBeforeScaling * 3 * 3, EPSILON);
}

BOOST_AUTO_TEST_CASE(polygon_move_abs)
{
  sobolev::point_t points[] = { {2, 1}, {4, 6}, {7, 8}, {9, 5}, {4, 1} };
  sobolev::Polygon testPolygon(5, points);
  const double polygonAreaBeforeMoving = testPolygon.getArea();
  const sobolev::rectangle_t polygonFrameRectBeforeMoving = testPolygon.getFrameRect();

  testPolygon.move({ 1, 1 });

  BOOST_CHECK_CLOSE(testPolygon.getFrameRect().height, polygonFrameRectBeforeMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(testPolygon.getFrameRect().width, polygonFrameRectBeforeMoving.width, EPSILON);
  BOOST_CHECK_CLOSE(testPolygon.getArea(), polygonAreaBeforeMoving, EPSILON);

  testPolygon.move(-1, -1);

  BOOST_CHECK_CLOSE(testPolygon.getFrameRect().height, polygonFrameRectBeforeMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(testPolygon.getFrameRect().width, polygonFrameRectBeforeMoving.width, EPSILON);
  BOOST_CHECK_CLOSE(testPolygon.getArea(), polygonAreaBeforeMoving, EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()
