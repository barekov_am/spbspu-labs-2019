#define _USE_MATH_DEFINES
#include "rectangle.hpp"

#include <iostream>
#include <stdexcept>
#include <cmath>

sobolev::Rectangle::Rectangle(const point_t &pos, double width, double height) :
  pos_(pos),
  width_(width),
  height_(height),
  angle_(0)
{
  if ((height_ <= 0.0) || (width_ <= 0.0))
  {
    throw std::invalid_argument("Invalid width or height");
  }
}

sobolev::Rectangle::Rectangle(const point_t &pos, double width, double height, double angle) :
  Rectangle(pos, width, height)
{
  rotate(angle);
}

double sobolev::Rectangle::getArea() const
{
  return width_ * height_;
}

sobolev::rectangle_t sobolev::Rectangle::getFrameRect() const
{
  const double sin = std::sin(angle_ * M_PI / 180);
  const double cos = std::cos(angle_ * M_PI / 180);
  const double height = width_ * std::abs(sin) + height_ * std::abs(cos);
  const double width = width_ * std::abs(cos) + height_ * std::abs(sin);
  return { pos_, width, height };
}

void sobolev::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void sobolev::Rectangle::move(const point_t &newPos)
{
  pos_ = newPos;
}

void sobolev::Rectangle::scale(const double coef)
{
  if (coef <= 0.0)
  {
    throw std::invalid_argument("Invalid coef");
  }
  width_ *= coef;
  height_ *= coef;
}

void sobolev::Rectangle::rotate(double angle)
{
  angle_ += angle;
}

void sobolev::Rectangle::displayInfo() const
{
  rectangle_t framingRect = getFrameRect();
  std::cout << "\n Framing rectangle width: " << framingRect.width;
  std::cout << "\n Framing rectangle height: " << framingRect.height;
  std::cout << "\n Rectangle is at point ( x: " << pos_.x << ", y: " << pos_.y << " )";
  std::cout << "\n Rectangle area is " << getArea() << "\n\n";
}

sobolev::point_t sobolev::Rectangle::getCentre() const
{
  return point_t(pos_);
}
