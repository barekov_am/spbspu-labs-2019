#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"


namespace sobolev
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t &pos, double radius);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &newPos) override;
    void scale(double coef) override;
    void rotate(double angle) override;
    void displayInfo() const override;

  private:
    point_t pos_;
    double radius_;
    point_t getCentre() const override;
  };
}

#endif
