#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace sobolev
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &pointA, const point_t &pointB, const point_t &pointC);
    Triangle(const point_t &pointA, const point_t &pointB, const point_t &pointC, double angle);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &newPos) override;
    void scale(double coef) override;
    void rotate(double angle) override;
    void displayInfo() const override;

  private:
    point_t pos_;
    point_t pointA_;
    point_t pointB_;
    point_t pointC_;
    double angle_;
    point_t getCentre() const override;
  };
}

#endif
