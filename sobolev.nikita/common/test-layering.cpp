#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "layering.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

BOOST_AUTO_TEST_SUITE(layeringTesting)

BOOST_AUTO_TEST_CASE(matrixParametersAfterLayering)
{
  sobolev::Circle testCircle1({ 1, 1 }, 2);
  sobolev::Circle testCircle2({ 10, 10 }, 2);
  sobolev::Rectangle testRectangle({ 5, 2 }, 10, 4);

  std::shared_ptr<sobolev::Shape> circlePointer1 = std::make_shared<sobolev::Circle>(testCircle1);
  std::shared_ptr<sobolev::Shape> circlePointer2 = std::make_shared<sobolev::Circle>(testCircle2);
  std::shared_ptr<sobolev::Shape> rectanglePointer = std::make_shared<sobolev::Rectangle>(testRectangle);

  sobolev::CompositeShape compShape;
  compShape.addShape(circlePointer1);
  compShape.addShape(circlePointer2);
  compShape.addShape(rectanglePointer);

  sobolev::Matrix<sobolev::Shape> matrix;
  matrix = sobolev::split(compShape);

  BOOST_CHECK_EQUAL(matrix.getMaxColSize(), 2);
  BOOST_CHECK_EQUAL(matrix.getRowsCount(), 2);
  BOOST_CHECK_EQUAL(matrix[0].getSize(), 2);
  BOOST_CHECK_EQUAL(matrix[1].getSize(), 1);

  BOOST_CHECK(matrix[0][0] == circlePointer1);
  BOOST_CHECK(matrix[0][1] == circlePointer2);
  BOOST_CHECK(matrix[1][0] == rectanglePointer);
}

BOOST_AUTO_TEST_CASE(matrixParametersAfterAdding)
{
sobolev::Rectangle testRectangle1({5, 2}, 10, 4);
sobolev::Circle testCircle1({1, 1}, 2);

sobolev::Circle testCircle2({10, 10}, 2);
std::shared_ptr<sobolev::Shape> circlePointer1 = std::make_shared<sobolev::Circle>(testCircle1);
std::shared_ptr<sobolev::Shape> circlePointer2 = std::make_shared<sobolev::Circle>(testCircle2);
std::shared_ptr<sobolev::Shape> rectanglePointer1 = std::make_shared<sobolev::Rectangle>(testRectangle1);

sobolev::CompositeShape compShape;
compShape.addShape(circlePointer1);
compShape.addShape(rectanglePointer1);
compShape.addShape(circlePointer2);

sobolev::Rectangle testRectangle2({15, 15}, 1, 2);
std::shared_ptr<sobolev::Shape> rectanglePointer2 = std::make_shared<sobolev::Rectangle>(testRectangle2);

sobolev::Matrix<sobolev::Shape> matrix;
matrix = sobolev::split(compShape);

size_t maxColBefore = matrix.getMaxColSize();
size_t rowsCountBefore = matrix.getRowsCount();
size_t layer1SizeBefore = matrix[0].getSize();
size_t layer2SizeBefore = matrix[1].getSize();

matrix.add(0, rectanglePointer2);

BOOST_CHECK_EQUAL(maxColBefore + 1, matrix.getMaxColSize());
BOOST_CHECK_EQUAL(rowsCountBefore, matrix.getRowsCount());
BOOST_CHECK_EQUAL(layer1SizeBefore + 1, matrix[0].getSize());
BOOST_CHECK_EQUAL(layer2SizeBefore, matrix[1].getSize());

BOOST_CHECK(matrix[0][0] == circlePointer1);
BOOST_CHECK(matrix[0][1] == circlePointer2);
BOOST_CHECK(matrix[0][2] == rectanglePointer2);
BOOST_CHECK(matrix[1][0] == rectanglePointer1);

}

BOOST_AUTO_TEST_CASE(intersectTest)
{
sobolev::Rectangle testRectangle({10, 5}, 1, 2);
sobolev::Circle testCircle1({3, 2}, 2);
sobolev::Circle testCircle2({10, 6}, 2);

BOOST_CHECK(!sobolev::intersect(testRectangle, testCircle1));
BOOST_CHECK(sobolev::intersect(testRectangle, testCircle2));
}

BOOST_AUTO_TEST_SUITE_END()
