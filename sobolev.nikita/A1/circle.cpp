#define _USE_MATH_DEFINES

#include "circle.hpp"
#include <math.h>
#include <cassert>
#include <iostream>

Circle::Circle(const point_t &pos, double radius) :
  pos_(pos),
  radius_(radius)
{
  assert(radius_ > 0.0);
}

double Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

rectangle_t Circle::getFrameRect() const
{
  return {pos_, 2 * radius_, 2 * radius_};
}

void Circle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void Circle::move(const point_t &newPos)
{
  pos_ = newPos;
}

void Circle::displayInfo() const
{
  rectangle_t framingRectangle = getFrameRect();
  std::cout << "\n Circle frame width is " << framingRectangle.width;
  std::cout << "\n Circle frame height is " << framingRectangle.height;
  std::cout << "\n Circle is at point ( x: " << pos_.x << ", y: " << pos_.y << " )";
  std::cout << "\n Circle area is " << getArea() << "\n\n";
}
