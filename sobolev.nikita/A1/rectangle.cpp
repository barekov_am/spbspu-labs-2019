#include "rectangle.hpp"

#include <iostream>
#include <cassert>

Rectangle::Rectangle(const point_t &pos, double width, double height) :
  pos_(pos),
  width_(width),
  height_(height)
{
  assert((width_ >= 0.0) && (height >= 0.0));
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return {pos_, width_, height_};
}

void Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void Rectangle::move(const point_t &newPos)
{
  pos_ = newPos;
}

void Rectangle::displayInfo() const
{
  rectangle_t framingRect = getFrameRect();
  std::cout << "\n Framing rectangle width: " << framingRect.width;
  std::cout << "\n Framing rectangle height: " << framingRect.height;
  std::cout << "\n Rectangle is at point ( x: " << pos_.x << ", y: " << pos_.y << " )";
  std::cout << "\n Rectangle area is " << getArea() << "\n\n";
}
