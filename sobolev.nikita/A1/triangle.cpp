#include "triangle.hpp"

#include <cmath>
#include <algorithm>
#include <cassert>
#include <iostream>

Triangle::Triangle(const point_t &pointA, const point_t &pointB, const point_t &pointC) :
  pos_({(pointA.x + pointB.x + pointC.x) / 3,
      (pointA.y + pointB.y + pointC.y) / 3}),
  pointA_(pointA),
  pointB_(pointB),
  pointC_(pointC)
{
  assert(getArea() > 0.0);
}

double Triangle::getArea() const
{
  return 0.5 * std::fabs((pointA_.x - pointC_.x) * (pointB_.y - pointC_.y)
      - (pointB_.x - pointC_.x) * (pointA_.y - pointC_.y));
}

rectangle_t Triangle::getFrameRect() const
{
  point_t min = {std::min(std::min(pointA_.x, pointB_.x), pointC_.x),
      std::min(std::min(pointA_.y, pointB_.y), pointC_.y)};
  point_t max = {std::max(std::max(pointA_.x, pointB_.x), pointC_.x),
      std::max(std::max(pointA_.y, pointB_.y), pointC_.y)};

  return rectangle_t{{(max.x + min.x) / 2, (max.y + min.y) / 2},
      max.x - min.x, max.y - min.y};
}

void Triangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;

  pointA_.x += dx;
  pointA_.y += dy;

  pointB_.x += dx;
  pointB_.y += dy;

  pointC_.x += dx;
  pointC_.y += dy;
}

void Triangle::move(const point_t &newPos)
{
  move(newPos.x - pos_.x, newPos.y - pos_.y);
}

void Triangle::displayInfo() const
{
  rectangle_t framingRectangle = getFrameRect();
  std::cout << "Triangle centre is at point ( "
      << pos_.x << "," << pos_.y << " )";
  std::cout << "\n Triangle point A has coordinates ( "
      << pointA_.x << ';' << pointA_.y << " )";
  std::cout << "\n Triangle point B has coordinates ( "
      << pointB_.x << ';' << pointB_.y << " )";
  std::cout << "\n Triangle point C has coordinates ( "
      << pointC_.x << ';' << pointC_.y << " )";
  std::cout << "\n Triangle frame width is "
      << framingRectangle.width;
  std::cout << "\n Triangle frame height is "
      << framingRectangle.height;
  std::cout << "\n Area is " << getArea() << "\n\n";
}
