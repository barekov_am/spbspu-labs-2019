#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "shape.hpp"

class Polygon : public Shape
{
public:
  Polygon(const Polygon & other);
  Polygon(Polygon && other);
  Polygon(int vertNum, const point_t * vertex);
  ~Polygon() override;

  Polygon & operator =(const Polygon & other);
  Polygon & operator =(Polygon && other);

  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(double dx, double dy) override;
  void move(const point_t &newPos) override;
  void displayInfo() const override;

private:
  int vertNum_;
  point_t * vertex_;

  point_t getCentre() const;
  bool isConvex() const;
};
#endif
