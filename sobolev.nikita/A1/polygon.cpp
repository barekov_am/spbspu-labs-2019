#include "polygon.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <algorithm>

Polygon::Polygon(const Polygon & other) :
  vertNum_(other.vertNum_),
  vertex_(new point_t[other.vertNum_])
{
  for (int i = 0; i < vertNum_; i++)
  {
    vertex_[i] = other.vertex_[i];
  }
}

Polygon::Polygon(Polygon && other) :
  vertNum_(other.vertNum_),
  vertex_(other.vertex_)
{
  other.vertNum_ = 0;
  other.vertex_ = nullptr;
}

Polygon::Polygon(int vertNum, const point_t * vertex) :
  vertNum_(vertNum),
  vertex_(new point_t[vertNum])
{
  if (vertNum <= 2)
  {
    delete[] vertex_;
    throw std::invalid_argument("Number of points in polygon must be more than 2");
  }

  if (vertex == nullptr)
  {
    delete[] vertex_;
    throw std::invalid_argument("Pointer to vertex must not be null.");
  }

  for (int i = 0; i < vertNum_; i++)
  {
    vertex_[i] = vertex[i];
  }

  if (!isConvex())
  {
    delete[] vertex_;
    throw std::invalid_argument("Polygon must be convex");
  }

  if (getArea() == 0.0)
  {
    delete[] vertex_;
    throw std::invalid_argument("Polygon's area must be more than 0");
  }
}

Polygon::~Polygon()
{
  delete[] vertex_;
}

Polygon & Polygon::operator =(const Polygon & other)
{
  if (this == &other)
  {
    return *this;
  }
  vertNum_ = other.vertNum_;

  delete[] vertex_;
  vertex_ = new point_t[vertNum_];

  for (int i = 0; i < vertNum_; i++)
  {
    vertex_[i] = other.vertex_[i];
  }
  return *this;
}

Polygon & Polygon::operator =(Polygon && other)
{
  if (this == &other)
  {
    return *this;
  }
  delete[] vertex_;

  vertNum_ = other.vertNum_;
  vertex_ = other.vertex_;
  other.vertNum_ = 0;
  other.vertex_ = nullptr;

  return *this;
}

double Polygon::getArea() const
{
  double area = 0;
  for (int i = 0; i < vertNum_ - 1; i++)
  {
    area += vertex_[i].x * vertex_[i + 1].y;
    area -= vertex_[i + 1].x * vertex_[i].y;
  }
  area += vertex_[vertNum_ - 1].x * vertex_[0].y;
  area -= vertex_[0].x * vertex_[vertNum_ - 1].y;

  return (std::fabs(area) / 2);
}

point_t Polygon::getCentre() const
{
  point_t pos = { 0, 0 };
  for (int i = 0; i < vertNum_; i++)
  {
    pos.x += vertex_[i].x;
    pos.y += vertex_[i].y;
  }
  return { pos.x / vertNum_, pos.y / vertNum_ };
}


bool Polygon::isConvex() const
{
  point_t prev = { 0, 0 };
  point_t nextPoint = { 0, 0 };
  for (int i = 0; i <= vertNum_ - 2; i++)
  {
    if (i != vertNum_ - 2)
    {
      prev.x = vertex_[i + 1].x - vertex_[i].x;
      prev.y = vertex_[i + 1].y - vertex_[i].y;
      nextPoint.x = vertex_[i + 2].x - vertex_[i + 1].x;
      nextPoint.y = vertex_[i + 2].y - vertex_[i + 1].y;
    }
    else
    {
      prev.x = vertex_[i + 1].x - vertex_[i].x;
      prev.y = vertex_[i + 1].y - vertex_[i].y;
      nextPoint.x = vertex_[0].x - vertex_[i + 1].x;
      nextPoint.y = vertex_[0].y - vertex_[i + 1].y;
    }

    if (((prev.x * nextPoint.y) - (nextPoint.x * prev.y)) >= 0)
    {
      return false;
    }
  }
  return true;
}

rectangle_t Polygon::getFrameRect() const
{
  double maxX = vertex_[0].x;
  double minX = vertex_[0].x;
  double maxY = vertex_[0].y;
  double minY = vertex_[0].y;
  for (int i = 1; i < vertNum_; i++)
  {
    maxX = std::max(maxX, vertex_[i].x);
    minX = std::min(minX, vertex_[i].x);
    maxY = std::max(maxY, vertex_[i].y);
    minY = std::min(minY, vertex_[i].y);
  }

  const double height = (maxY - minY);
  const double width = (maxX - minX);
  const point_t pos = {minX + (width / 2),
      minY + (height / 2)};
  return {pos, width, height};
}

void Polygon::move(const point_t &newPos)
{
  const point_t oldPos = getCentre();
  const double dx = newPos.x - oldPos.x;
  const double dy = newPos.y - oldPos.y;

  move(dx, dy);
}

void Polygon::move(double dx, double dy)
{
  for (int i = 0; i < vertNum_; i++)
  {
    vertex_[i].x += dx;
    vertex_[i].y += dy;
  }
}

void Polygon::displayInfo() const
{
  rectangle_t framingRectangle = getFrameRect();
  point_t center = getCentre();

  for (int i = 0; i < vertNum_; i++)
  {
    std::cout << "\n Vertex index is:" << i;
    std::cout << "\n Vertex point has coordinates ("
        << vertex_[i].x << " ; " << vertex_[i].y << ")";
  }
  std::cout << "\n Center = (" << center.x
      << " ; " << center.y << ")";
  std::cout << "\n Polygon frame width is "
      << framingRectangle.width;
  std::cout << "\n Polygon frame height is "
      << framingRectangle.height;
  std::cout << "\n Polygon area is "
      << getArea() << "\n\n";
}
