#include <iostream>

#include "base-types.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
#include "layering.hpp"

void displayMatrixInfo(const sobolev::Matrix<sobolev::Shape> &matrix)
{
  for (size_t i = 0; i < matrix.getRowsCount(); i++)
  {
    std::cout << "Layer number " << i + 1 << " : \n";
    std::cout << "Amount of shapes: " << matrix[i].getSize() << std::endl;
    for (size_t j = 0; j < matrix[i].getSize(); j++)
    {
      std::cout << "Shape number " << j + 1 << " with parameters:\n";
      sobolev::rectangle_t framingRect = matrix[i][j]->getFrameRect();
      std::cout << "Framing rectangle width: " << framingRect.width << std::endl;
      std::cout << "Framing rectangle height: " << framingRect.height << std::endl;
      std::cout << "Rectangle is at point: (" << framingRect.pos.x << ", " << framingRect.pos.y << ")\n\n";
    }
  }
}

int main()
{
  std::cout << "________TEST RECTANGLE________\n";
  sobolev::Rectangle rectangle({2.2, -1}, 3, 4);
  sobolev::Shape *shapePtr = &rectangle;

  shapePtr->displayInfo();

  std::cout << "Move rectangle by dx and dy\n";
  shapePtr->move(-1, 1); 
  shapePtr->displayInfo();

  std::cout << "Scale rectangle x3\n";
  shapePtr->scale(3);
  shapePtr->displayInfo();
  
  std::cout << "Move rectangle by coordinate\n";
  shapePtr->move({1, 1}); 
  shapePtr->displayInfo();
  
  std::cout << "________TEST CIRCLE________\n";
  sobolev::Circle circle({2, -1.6}, 6);
  shapePtr = &circle;
  shapePtr->displayInfo();

  std::cout << "Move circle by dx and dy\n";
  shapePtr->move(-1, -1);
  shapePtr->displayInfo();

  std::cout << "Scale circle x3\n";
  shapePtr->scale(3);
  shapePtr->displayInfo();
 
  std::cout << "Move circle by coordinate\n";
  shapePtr->move({1, 1});
  shapePtr->displayInfo();
  
  std::cout << "________TEST TRIANGLE________\n";
  sobolev::Triangle triangle({1, 1}, {5, 1}, {1, 4});
  shapePtr = &triangle;
  shapePtr->displayInfo();
  
  std::cout << "Move triangle by dx and dy\n";
  shapePtr->move(-1, -1);
  shapePtr->displayInfo();

  std::cout << "Scale triangle x3\n";
  shapePtr->scale(3);
  shapePtr->displayInfo();

  std::cout << "Move triangle by coordinate\n";
  shapePtr->move({1, 1});
  shapePtr->displayInfo();
  
  std::cout << "________TEST POLYGON________\n";
  const sobolev::point_t points[4] = {{1, 1}, {1, 5}, {4, 5}, {4, 1}};
  sobolev::Polygon polygon(4, points);
  shapePtr = &polygon;
  shapePtr->displayInfo();

  std::cout << "Move polygon by dx and dy\n";
  shapePtr->move(-1, -1);
  shapePtr->displayInfo();

  std::cout << "Scale polygon x3\n";
  shapePtr->scale(3);
  shapePtr->displayInfo();

  std::cout << "Move polygon by coordinate\n";
  shapePtr->move({1, 1});
  shapePtr->displayInfo();
  
  std::cout << "Demonstration of the copy constructor:\n";
  sobolev::Polygon polygonCopy(polygon);
  polygonCopy.displayInfo();

  std::cout << "Demonstration of the copy assignment constructor:\n";
  sobolev::Polygon polygonAssigCopy = polygon;
  polygonAssigCopy.displayInfo();

  std::cout << "Demonstration of the move constructor:\n";
  sobolev::Polygon polygonMove(std::move(polygonCopy));
  polygonMove.displayInfo();

  std::cout << "Demonstration of the move assignment constructor:\n";
  sobolev::Polygon polygonAssigMove = std::move(polygonAssigCopy);
  polygonAssigMove.displayInfo();
  
  std::cout << "________TEST COMPOSITE SHAPE________\n";
  sobolev::CompositeShape compShape;

  compShape.addShape(std::make_shared<sobolev::Circle>(circle));
  std::cout << "___FrameRect with circle:___\n";
  compShape.displayInfo();

  compShape.addShape(std::make_shared<sobolev::Rectangle>(rectangle));
  std::cout << "___FrameRect with circle and rectangle:___\n";
  compShape.displayInfo();
  
  compShape.addShape(std::make_shared<sobolev::Triangle>(triangle));
  std::cout << "___FrameRect with circle, rectangle and triangle:___\n";
  compShape.displayInfo();

  compShape.addShape(std::make_shared<sobolev::Polygon>(polygon));
  std::cout << "___FrameRect with circle, rectangle, triangle and polygon:___\n";
  compShape.displayInfo();

  std::cout << "Demonstration of move:\n";
  std::cout << "Move by -1 and -1\n";
  compShape.move(-1, -1);
  compShape.displayInfo();

  std::cout << "Move by point(1, 1)\n";
  compShape.move({ 1, 1 });
  compShape.displayInfo();

  std::cout << "Demonstration of scale:\n";
  compShape.scale(2);
  compShape.displayInfo();

  std::cout << "Demonstration of delete element of the array of the shapes:\n";
  std::cout << "Size of composite shape array: " << compShape.getSize() << std::endl;
  compShape.deleteShape(2);
  std::cout << "Size of composite shape array: " << compShape.getSize()
    << std::endl << std::endl;

  std::cout << "Demonstration of the copy constructor:\n";
  sobolev::CompositeShape copyCompShape(compShape);
  copyCompShape.displayInfo();

  std::cout << "Demonstration of the move constructor:\n";
  sobolev::CompositeShape moveCompShape(std::move(copyCompShape));
  moveCompShape.displayInfo();

  std::cout << "Demonstration of the copy assignment constructor:\n";
  sobolev::CompositeShape copyAssigCompShape = compShape;
  copyAssigCompShape.displayInfo();

  std::cout << "Demonstration of the move assignment constructor:\n";
  sobolev::CompositeShape moveAssigCompShape = std::move(copyAssigCompShape);
  moveAssigCompShape.displayInfo();

  compShape.rotate(90);
  std::cout << "___Rotate 90gr:___\n";
  compShape.displayInfo();

  std::cout << "________TEST LAYERING COMPOSITE SHAPE________\n";
  sobolev::Matrix<sobolev::Shape> matrix;
  matrix = sobolev::split(compShape);
  displayMatrixInfo(matrix);
  std::cout << "The biggest number of shapes in a layer is " << matrix.getMaxColSize() << " shapes \n";

  return 0;
}
