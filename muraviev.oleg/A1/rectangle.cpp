#include <iostream>
#include "rectangle.hpp"

Rectangle::Rectangle(double x, double y, double width, double height)
{
  r_.pos.x = x;
  r_.pos.y = y;
  r_.width = width;
  r_.height = height;
  
  if (width <= 0 || height <= 0)
    {
      throw std::invalid_argument ("ERROR! INVALID PARAMETERS!");
    }
}

double Rectangle::getArea() const
{
  return r_.height * r_.width;
}

rectangle_t Rectangle::getFrameRect() const
{
  return r_;
}

void Rectangle::move(const point_t &p)
{
  r_.pos = p;
}

void Rectangle::move(double dx, double dy)
{
  r_.pos.x += dx;
  r_.pos.y += dy;
}
