#ifndef POLYGON_HPP
#define POLYGON_HPP
#include "base-types.hpp"
#include "shape.hpp"

class Polygon : public Shape
{
  public:
    Polygon(int vertex, point_t *const points);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &p) override;
    
  private:
    int vertex_;
    point_t *points_;
};

#endif
