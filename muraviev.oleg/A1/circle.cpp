#include "circle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>
#define _USE_MATH_DEFINES

Circle::Circle(double x, double y, double radius)
{
  r_ = radius;
  center_.x = x;
  center_.y = y;
  
  if (r_ <= 0)
    {
      throw std::invalid_argument ("ERROR! INVALID PARAMETER!");
    }
}

double Circle::getArea() const
{
  return M_PI * r_ * r_;
}

rectangle_t Circle::getFrameRect() const
{
  return rectangle_t {r_ * 2, r_ * 2, center_};
}

void Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Circle::move(const point_t &p)
{
  center_ = p;
}
