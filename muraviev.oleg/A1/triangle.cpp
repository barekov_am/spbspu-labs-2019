#include <iostream>
#include <algorithm>
#include <cmath>
#include "base-types.hpp"
#include "triangle.hpp"

using namespace std;

Triangle::Triangle(const point_t &p1, const point_t &p2, const point_t &p3):
a_(p1), b_(p2), c_(p3)
{

}

double Triangle::getArea() const
{
  double bufCalc;
  bufCalc = ((b_.x - a_.x)*(c_.y - a_.y) - (c_.x - a_.x)*(b_.y - a_.y)) / 2;
  bufCalc = fabs(bufCalc);
  
  if (bufCalc <= 0)
  {
    throw std::invalid_argument("ERROR! INVALID PARAMETER!");
  }
  
  return bufCalc;
  
}

point_t Triangle::getCenter() const
{
  return point_t{(a_.x + b_.x + c_.x / 3), (a_.y + b_.y + c_.y / 3)}; 
}

rectangle_t Triangle::getFrameRect() const
{
  double bufM;
  bufM = min(a_.x, b_.x);
  double firstP = min(bufM, c_.x);
  bufM = min(a_.y, b_.y);
  double secondP = min(bufM, c_.y);
  bufM = min(a_.y, b_.y);
  double thirdP = max(bufM, c_.y) - secondP;
  bufM = max(a_.x, b_.x);
  double fourthP = max(bufM, c_.x) - firstP;

  return rectangle_t{fourthP, thirdP, getCenter()};
}

void Triangle::move(double dx, double dy)
{
a_.x = a_.x + dx;
b_.x = b_.x + dx;
c_.x = c_.x + dx;
a_.y = a_.y + dy;
b_.y = b_.y + dy;
c_.y = c_.y + dy;
}

void Triangle::move(const point_t &p)
{
  double pix;
  double piy;
  pix = p.x - getCenter().x;
  piy = p.y - getCenter().y;
  move(pix, piy);
}
