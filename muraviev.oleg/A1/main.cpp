#include <iostream>
#include <cstdlib>
#include "shape.hpp"
#include "base-types.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void printParams(const Shape &sh, const char *name)
{
  std::cout << name << " area=" << sh.getArea() << std::endl;
  std::cout << "height=" << sh.getFrameRect().height << std::endl;
  std::cout << "width=" << sh.getFrameRect().width << std::endl;
}

int main()
{
  Rectangle rect(20, 30, 10, 5);
  rect.move(15, 35);
  rect.move({12, 16});
  printParams(rect, "Rectangle");
  
  Circle circ(50, 60, 10);
  circ.move(10, 20);
  circ.move({5, 15});
  printParams(circ, "Circle");
  
  Triangle tria({0, 0}, {0, 30}, {40, 0});
  tria.move(4, 3);
  tria.move({7, 5});
  printParams(tria, "Triangle");
  
  point_t points[] = {{1, 1}, {2, 2}, {3, 0}};
  
  Polygon polyg(3, points);
  polyg.move(1, 2);
  polyg.move({1, 2});
  printParams(polyg, "Polygon");
  
  return 0;
}
