#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"
#include "base-types.hpp"

class Circle: public Shape
{
  public:
    Circle(double x, double y, double radius);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &p) override;
    void move(double dx, double dy) override;

  private:
    point_t center_;
    double r_;
};

#endif
