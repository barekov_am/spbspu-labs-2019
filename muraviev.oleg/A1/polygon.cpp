#define _USE_MATH_DEFINES
#include "polygon.hpp"
#include "base-types.hpp"
#include <cmath>
#include <stdexcept>

  Polygon::Polygon(const int vertex, point_t *const points) :
  vertex_(vertex),
  points_(points)
  {
    
    if (vertex < 3 || points_ == nullptr)
    {
      throw std::invalid_argument("ERROR! INVALID PARAMETER!");
    }

  }
  
  double Polygon::getArea() const
  {
    
    double area = (points_[vertex_- 1].x + points_[0].x)
      * (points_[vertex_ - 1].y - points_[0].y);
      
    for (int i = 0; i < vertex_ - 1; i++)
  {
      
     area = area + (points_[i].x + points_[i + 1].x)
       * (points_[i].y - points_[i + 1].y);
     
  }
    
    return (0.5 * fabs(area));
    
    if (area == 0)
    {
      throw std::invalid_argument("ERROR! INVALID PARAMETER!");
    }
    
  }
  
  rectangle_t Polygon::getFrameRect() const
  {
    double firstP;
    double secondP;
    double thirdP;
    double fourthP;
      
    point_t top_right_p;
    point_t bot_left_p;
    
    top_right_p = points_[0];
    bot_left_p = points_[0];
    
      for (int i = 1; i < vertex_; i++)
      {
  
        if(points_[i].x > top_right_p.x)
        {
          top_right_p.x = points_[i].x;
        }
      
        if(points_[i].x < bot_left_p.x)
        {
          bot_left_p.x = points_[i].x;
        }
        
        if(points_[i].y > top_right_p.y)
        {
          top_right_p.y = points_[i].y;
        }
        
        if(points_[i].y < bot_left_p.y)
        {
          bot_left_p.y = points_[i].y;
        }
    
      }
    
    firstP = top_right_p.x - bot_left_p.x;
    secondP = top_right_p.y - bot_left_p.y;
    thirdP = (top_right_p.x + bot_left_p.x) / 2;
    fourthP = (top_right_p.y + bot_left_p.y) / 2;
    
    return {firstP, secondP, {thirdP, fourthP}};
  }
  
  void Polygon::move(double dx, double dy)
  {
      
    for(int i = 0; i < vertex_; i++)
    {
      points_[i] = {points_[i].x + dx, points_[i].y + dy};
    }
    
  }

  void Polygon::move(const point_t &p)
  {
      
    point_t center_shape = Polygon::getFrameRect().pos;
    Polygon::move(p.x - center_shape.x,
    p.y - center_shape.y);
    
  }
