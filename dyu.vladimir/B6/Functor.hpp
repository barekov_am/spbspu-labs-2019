#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <stdio.h>

class Functor{
public:
  void operator()(int value);
  double getAverageValue();
  bool FirstEqualLast();
  void print();
  
private:
  int TotalElements_ = 0;
  long int sumElements_ = 0;
  long int maxValue_ = 0;
  long int minValue_ = 0;
  double averageValue_ = 0;
  int TotalPositive_ = 0;
  int TotalNegative_ = 0;
  long int oddSum_ = 0;
  long int evenSum_ = 0;
  long int firstValue_ = 0;
  long int lastValue_ = 0;
};



#endif
