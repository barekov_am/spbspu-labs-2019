#include <iostream>

#include "Functor.hpp"


void Functor::operator()(const int value){
  sumElements_ += value;
  lastValue_ = value;
  
  if (value > maxValue_){
    maxValue_ = value;
  }
  
  if (value < minValue_){
    minValue_ = value;
  }
  
  if (value > 0){
    TotalPositive_++;
  }
  else if (value < 0){
    TotalNegative_++;
  }
  
  if (value %2 != 0){
    oddSum_ += value;
  }
  else{
    evenSum_ += value;
  }
  
  if (TotalElements_ == 0){
    TotalElements_++;
    firstValue_ = value;
    maxValue_ = value;
    minValue_ = value;
  }
  else{
    TotalElements_++;
  }
}

double Functor::getAverageValue()
{
  averageValue_ = static_cast<double>(sumElements_) / static_cast<double>(TotalElements_);
  return averageValue_;
}

bool Functor::FirstEqualLast()
{
  return firstValue_ == lastValue_;
}

void Functor::print()
{
  std::cout << "Max: " << maxValue_ << '\n';
  std::cout << "Min: " << minValue_ << '\n';
  std::cout << "Mean: " << getAverageValue() << '\n';
  std::cout << "Positive: " << TotalPositive_ << '\n';
  std::cout << "Negative: " << TotalNegative_ << '\n';
  std::cout << "Odd Sum: " << oddSum_ << '\n';
  std::cout << "Even Sum: " << evenSum_ << '\n';
  std::cout << "First/Last Equal: ";
  if (FirstEqualLast())
  {
    std::cout << "yes" << '\n';
  }
  else
  {
    std::cout << "no" << '\n';
  }
}
