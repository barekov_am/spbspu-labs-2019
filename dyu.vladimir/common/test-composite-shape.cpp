#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include <utility>


using shape_ptr = std::shared_ptr<dyu::Shape>;
const double errorValue = 1e-10;

BOOST_AUTO_TEST_SUITE(test_A4_compositeShaes)

BOOST_AUTO_TEST_CASE(Test_composite_area)
{
  
  dyu::Circle Circle(3.0, {10.0, 10.0});
  shape_ptr testCircle = std::make_shared<dyu::Circle>(Circle);
  dyu::Rectangle Rect(3.0, 6.0, {10.0, 10.0});
  shape_ptr testRect = std::make_shared<dyu::Rectangle>(Rect);
  dyu::CompositeShape testCompShape;
  testCompShape.add(testRect);
  testCompShape.add(testCircle);
  
  const double areaBeforeMoving = testCompShape.getArea();
  const dyu::rectangle_t frameBeforeMoving = testCompShape.getFrameRect();
    
  testCompShape.move({0, 0});
  double areaAfterMoving = testCompShape.getArea();
  dyu::rectangle_t frameAfterMoving = testCompShape.getFrameRect();
  
  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, errorValue);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, frameAfterMoving.width, errorValue);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, frameAfterMoving.height, errorValue);
  
  testCompShape.move(4, 5);
  areaAfterMoving = testCompShape.getArea();
  frameAfterMoving = testCompShape.getFrameRect();
  
  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, errorValue);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, frameAfterMoving.width, errorValue);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, frameAfterMoving.height, errorValue);
}

BOOST_AUTO_TEST_CASE(Test_composite_area_2)
{
  dyu::Circle Circle(3.0, {10.0, 10.0});
  dyu::Rectangle Rect(3.0, 6.0, {10.0, 10.0});
  
  shape_ptr testCircle = std::make_shared<dyu::Circle>(Circle);
  shape_ptr testRect = std::make_shared<dyu::Rectangle>(Rect);
  
  const double allArea = testCircle->getArea() + testRect->getArea();
  
  dyu::CompositeShape testCompShape;
  testCompShape.add(testRect);
  testCompShape.add(testCircle);
  
  BOOST_CHECK_CLOSE(allArea, testCompShape.getArea(), errorValue);
}
  
BOOST_AUTO_TEST_CASE(Test_composite_scale)
{
  dyu::Circle Circle(3.0, {10.0, 10.0});
  dyu::Rectangle Rect(3.0, 6.0, {10.0, 10.0});
  
  shape_ptr testCircle = std::make_shared<dyu::Circle>(Circle);
  shape_ptr testRect = std::make_shared<dyu::Rectangle>(Rect);
  
  dyu::CompositeShape testCompShape;
  testCompShape.add(testRect);
  testCompShape.add(testCircle);
  const double areaBeforeScaling = testCompShape.getArea();
  
  const double coeficient = 2;
  testCompShape.scale(coeficient);
  
  const double areaAfterScaling = testCompShape.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScaling * coeficient * coeficient, areaAfterScaling, errorValue);
}

BOOST_AUTO_TEST_CASE(Test_composite_scale_negative)
{
  dyu::Circle Circle(3.0, {10.0, 10.0});
  shape_ptr testCircle = std::make_shared<dyu::Circle>(Circle);
  dyu::CompositeShape testCompShape;
  testCompShape.add(testCircle);
  BOOST_CHECK_THROW(testCompShape.scale(-10), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(Test_nullptr)
{
  dyu::CompositeShape testCompShape;
  BOOST_CHECK_THROW(dyu::CompositeShape(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(Test_outofrange)
{
  dyu::CompositeShape testCompShape;
  BOOST_CHECK_THROW(testCompShape[-1], std::out_of_range);
  BOOST_CHECK_THROW(testCompShape[100], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(Test_delete)
{
  dyu::Circle Circle(3.0, {10.0, 10.0});
  dyu::Rectangle Rect(3.0, 6.0, {10.0, 10.0});
  shape_ptr testCircle = std::make_shared<dyu::Circle>(Circle);
  shape_ptr testRect = std::make_shared<dyu::Rectangle>(Rect);
  
  const double areaTestRect = testRect->getArea();
  const dyu::rectangle_t frameTestRect = testRect->getFrameRect();
  
  dyu::CompositeShape testCompShape;
  testCompShape.add(testCircle);
  testCompShape.add(testRect);
  
  testCompShape.remove(0);
  
  dyu::rectangle_t frameAfterDelete = testCompShape.getFrameRect();
  double areaAfterDelete = testCompShape.getArea();
  
  BOOST_CHECK_CLOSE(frameTestRect.width, frameAfterDelete.width, errorValue);
  BOOST_CHECK_CLOSE(frameTestRect.height, frameAfterDelete.height, errorValue);
  BOOST_CHECK_CLOSE(areaTestRect, areaAfterDelete, errorValue);
}

BOOST_AUTO_TEST_CASE(Test_on_improver_delete)
{
  dyu::CompositeShape testCompShape;
  BOOST_CHECK_THROW(testCompShape.remove(100), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(areaAfterRotating)
{
  dyu::Circle Circle1(1, {4, 7});
  shape_ptr testCircle = std::make_shared<dyu::Circle>(Circle1);
  dyu::Rectangle Rectangle2(2.1, 1, { 5, 6 });
  shape_ptr testRectangle = std::make_shared<dyu::Rectangle>(Rectangle2);
  dyu::CompositeShape testComposite;
  testComposite.add(testCircle);
  testComposite.add(testRectangle);
  double areaBefore = testComposite.getArea();
  
  double angle = 48.2;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), errorValue);
  areaBefore = testComposite.getArea();
  
  angle = -30.88;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), errorValue);
  areaBefore = testComposite.getArea();
  
  angle = 765.2;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), errorValue);
  areaBefore = testComposite.getArea();
  
  angle = -987.3;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), errorValue);
}

BOOST_AUTO_TEST_SUITE_END()
