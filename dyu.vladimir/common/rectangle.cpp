#include "rectangle.hpp"
#include "cmath"
#include <iostream>
#include <cassert>


dyu::Rectangle::Rectangle(double width, double height, const point_t &pos):
  rect_(rectangle_t {width, height, pos}),
  angle_(0)
{
  if ((width < 0.0) || (height < 0.0))
  {
    throw std::invalid_argument("rectangle width or height is wrong");
  }
}

double dyu::Rectangle::getArea() const
{
  return rect_.width * rect_.height;
}

dyu::rectangle_t dyu::Rectangle::getFrameRect() const
{
  const double sinus = sin(angle_ * M_PI / 180);
  const double cosinus = cos(angle_ * M_PI / 180);
  const double width = rect_.height * abs(sinus) + rect_.width * abs(cosinus);
  const double height = rect_.height * abs(cosinus) + rect_.width * abs(sinus);

  return {height, width, rect_.pos};
}

void dyu::Rectangle::move(const point_t &pos)
{
  rect_.pos.x = pos.x;
  rect_.pos.y = pos.y;
}

void dyu::Rectangle::move(double x, double y)
{
  rect_.pos.x += x;
  rect_.pos.y += y;
}

void dyu::Rectangle::scale(double coeficient)
{
  if (coeficient < 0)
  {
    throw std::invalid_argument("coeficient is wrong");
  }
  rect_.width *= coeficient;
  rect_.height *= coeficient;
}

void dyu::Rectangle::rotate(double angle)
{
  angle_ += angle;
  angle_ = (angle_ < 360) ? angle_ : angle_ - 360;
  angle_ = (angle_ > 0) ? angle_ : angle_ + 360;
}

dyu::point_t dyu::Rectangle::getPos() const
{
  return rect_.pos;
}
