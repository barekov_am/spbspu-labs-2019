
#include <boost/test/auto_unit_test.hpp>

#include "patrition.hpp"
#include "matrix.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(SplitTestSuite)

BOOST_AUTO_TEST_CASE(CorrectWorkOfSplit)
{
  dyu::Rectangle rec1(4.0, 2.0, {7.0, 6.0});
  dyu::Rectangle rec2(4.0, 2.0, {10.0, 3.0});
  dyu::Circle cir1(2.0, {4.0, 4.0});
  dyu::Circle cir2(2.0, {10.0, 8.0});
  
  /*dyu::CompositeShape::shape_ptr part1 = std::make_shared<dyu::Rectangle>(rec1);
  dyu::CompositeShape::shape_ptr part2 = std::make_shared<dyu::Circle>(cir1);
  dyu::CompositeShape::shape_ptr part3 = std::make_shared<dyu::Rectangle>(rec1);
  dyu::CompositeShape::shape_ptr part4 = std::make_shared<dyu::Circle>(cir1);
  
  dyu::CompositeShape composite;
  composite.add(part1);
  composite.add(part3);
  composite.add(part2);
  composite.add(part4);*/
  
  dyu::CompositeShape composite;
  composite.add(std::make_shared<dyu::Circle>(cir1));
  composite.add(std::make_shared<dyu::Rectangle>(rec1));
  composite.add(std::make_shared<dyu::Rectangle>(rec2));
  composite.add(std::make_shared<dyu::Circle>(cir2));

  dyu::Matrix matrix = dyu::split(composite);
  
  BOOST_CHECK_EQUAL(matrix.getRows(), 2);
  BOOST_CHECK_EQUAL(matrix[0].getSize(), 3);
  BOOST_CHECK_EQUAL(matrix[1].getSize(), 1);
  BOOST_CHECK_EQUAL(matrix[0][0]->getArea(), cir1.getArea());
  BOOST_CHECK_EQUAL(matrix[1][0]->getArea(), rec1.getArea());
  BOOST_CHECK_EQUAL(matrix.getNumberOfFigures(), 4);
}

BOOST_AUTO_TEST_SUITE_END()
