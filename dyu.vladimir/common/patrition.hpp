#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace dyu
{
  Matrix split(const CompositeShape &listOfFigures);
}

#endif
