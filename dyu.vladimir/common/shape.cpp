#include "shape.hpp"
#include <cmath>

int dyu::Shape::intersect(const shape_ptr Shape) const
{
  rectangle_t firstFrame = getFrameRect();
  rectangle_t secondFrame = Shape->getFrameRect();
  
  if (fabs(firstFrame.pos.x - secondFrame.pos.x) > ((firstFrame.width + secondFrame.width) / 2))
  {
    return 0;
  }
  
  if (fabs(firstFrame.pos.y - secondFrame.pos.y) > ((firstFrame.height + secondFrame.height) / 2))
  {
    return 0;
  }
  return 1;
}

