#include "patrition.hpp"
#include "shape.hpp"

dyu::Matrix dyu::split(const CompositeShape &listOfFigures)
{
  dyu::Matrix matrix;
  matrix.add(listOfFigures[0], 0);
  
  for (int i = 1; i < listOfFigures.getShapeCount(); i++)
  {
    bool nextRow = true;
    
    for (int row = 0; row < matrix.getRows(); row++)
    {
      int counter = 0;
      for (int column = 0; column < matrix[row].getSize(); column++)
      {
        counter += matrix[row][column]->intersect(listOfFigures[i]);
      }
      
      if (counter == 0)
      {
        matrix.add(listOfFigures[i], row);
        nextRow = false;
        break;
      }
    }
    
    if (nextRow)
    {
      matrix.add(listOfFigures[i], matrix.getRows());
    }
  }
  
  return matrix;
}
