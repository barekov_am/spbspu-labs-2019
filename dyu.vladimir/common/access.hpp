#ifndef ACESS_HPP
#define ACESS_HPP

#include <cstddef>
#include <iterator>

template<typename Container>
struct access_bracket
{
  typedef std::size_t typeIndex;
   
  static typename Container::reference getElement(Container& arr, typeIndex index)
  {
    return arr[index];
  }

  static typeIndex begin(const Container&)
  {
    return 0;
  }

  static typeIndex end(const Container& arr)
  {
    return arr.size();
  }

  static typeIndex next(const std::size_t index)
  {
    return index + 1;
  }
};

template<typename Container>
struct access_at
{
  typedef std::size_t typeIndex;
  
  static typename Container::reference getElement(Container& arr, typeIndex index)
  {
    return arr.at(index);
  }

  static typeIndex begin(const Container&)
  {
    return 0;
  }

  static typeIndex end(const Container& arr)
  {
    return arr.size();
  }

  static typeIndex next(const typeIndex index)
  {
    return index + 1;
  }
};

template<typename Container>
struct access_iter
{
  static typename Container::reference getElement(Container&, typename Container::iterator iter)
  {
    return *iter;
  }

  static typename Container::iterator begin(Container& arr)
  {
    return arr.begin();
  }

  static typename Container::iterator end(Container& arr)
  {
    return arr.end();
  }

  static typename Container::iterator next(typename Container::iterator& iter)
  {
    return std::next(iter);
  }
};

#endif
