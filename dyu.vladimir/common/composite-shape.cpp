#include "composite-shape.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <algorithm>

dyu::CompositeShape::CompositeShape() :
  count_(0),
  angle_(0)
{}

dyu::CompositeShape::CompositeShape(const CompositeShape& tmp) :
  count_(tmp.count_),
  angle_(tmp.angle_),
  list_(std::make_unique<shape_ptr[]>(tmp.count_))
{
  for (int i = 0; i < count_; i++)
  {
    list_[i] = tmp.list_[i];
  }
}

dyu::CompositeShape::CompositeShape(CompositeShape&& tmp) :
  count_(tmp.count_),
  angle_(tmp.angle_),
  list_(std::move(tmp.list_))
{
  tmp.count_ = 0;
}

dyu::CompositeShape::CompositeShape(const shape_ptr& shape) :
  count_(1),
  list_(std::make_unique<shape_ptr[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape is nullptr");
  }
  list_[0] = shape;
}

dyu::CompositeShape& dyu::CompositeShape::operator = (const CompositeShape& tmp)
{
  if (this != &tmp)
  {
    count_ = tmp.count_;
    for (int i = 0; i < count_; i++)
    {
      list_[i] = tmp.list_[i];
    }
  }
  return *this;
}

dyu::CompositeShape& dyu::CompositeShape::operator = (dyu::CompositeShape&& tmp)
{
  if (this!= &tmp)
  {
    count_ = tmp.count_;
    list_ = std::move(tmp.list_);
    tmp.count_ = 0;
  }
  return *this;
}

dyu::CompositeShape::shape_ptr dyu::CompositeShape::operator [] (int i) const
{
  if ((i < 0 ) || (i >= count_))
  {
    throw std::out_of_range(" index is incorrect");
  }
  return list_[i];
}

double dyu::CompositeShape::getArea() const
{
  if (count_ <= 0)
  {
    throw std::logic_error("list is empty");
  }
  
  double area = 0.0;
  for (int i = 0; i < count_; i++)
  {
    area += list_[i]->getArea();
  }
  return area;
}

dyu::rectangle_t dyu::CompositeShape::getFrameRect() const
{
  if (count_ <= 0)
  {
    throw std::logic_error("list is empty");
  }
  
  dyu::rectangle_t firstFrameRect = list_[0]->getFrameRect();
  
  double left = firstFrameRect.pos.x - firstFrameRect.width / 2;
  double right = firstFrameRect.pos.x + firstFrameRect.width / 2;
  double top = firstFrameRect.pos.y + firstFrameRect.height / 2;
  double bottom = firstFrameRect.pos.y - firstFrameRect.height / 2;
  
  for (int i = 1; i < count_; i++)
  {
    rectangle_t curFrameRect = list_[i]->getFrameRect();
    
    double leftBorder = curFrameRect.pos.x - curFrameRect.width / 2;
    double rightBorder = curFrameRect.pos.x + curFrameRect.width / 2;
    double topBorder = curFrameRect.pos.y + curFrameRect.height / 2;
    double bottomBorder = curFrameRect.pos.y - curFrameRect.height / 2;
    
    left = std::min(left, leftBorder);
    right = std::max(right, rightBorder);
    top = std::max(top, topBorder);
    bottom = std::min(bottom, bottomBorder);
  }
  return rectangle_t{right - left, top - bottom, point_t{(left + right) / 2, (top + bottom) / 2}};
}

void dyu::CompositeShape::move(double x, double y)
{
  if (count_ == 0)
  {
    throw std::logic_error("list is empty");
  }
  for (int i = 0; i < count_; i++)
  {
    list_[i]->move(x, y);
  }
}

void dyu::CompositeShape::move(const point_t &pos)
{
  if (count_ <= 0)
  {
    throw std::logic_error("list is empty");
  }
  point_t center = getFrameRect().pos;
  for ( int i = 0; i < count_ ; i++)
  {
    list_[i]->move(pos.x - center.x, pos.y - center.y);
  }
}

void dyu::CompositeShape::scale(double coeficient)
{
  if (coeficient <= 0)
  {
    throw std::invalid_argument("coeficient is wrong");
  }
  if (count_ == 0)
  {
    throw std::logic_error("list is empty");
  }
  point_t tempFrame = getFrameRect().pos;
  
  for (int i = 0; i < count_; i++)
  {
    dyu::point_t currentFrame = list_[i]->getFrameRect().pos;
    list_[i]->move((currentFrame.x - tempFrame.x) * (coeficient - 1),
      (currentFrame.y - tempFrame.y) * (coeficient - 1));
    list_[i]->scale(coeficient);
  }
}

int dyu::CompositeShape::getSize() const
{
  return count_;
}

void dyu::CompositeShape::add(shape_ptr Shape)
{
  if (Shape == nullptr)
  {
    throw std::invalid_argument("Shape is empty");
  }
  ++count_;
  shapes_array tmpList(std::make_unique<shape_ptr[]>(count_));
  
  for (int i = 0; i < count_ - 1; i++)
  {
    tmpList[i] = list_[i];
  }
  tmpList[count_ - 1] = Shape;
  list_ .swap(tmpList);
}
  
void dyu::CompositeShape::remove(int number)
{
  if (number > count_)
  {
    throw std::out_of_range("number is wrong");
  }
  count_--;
  
  for (int i = number; i < count_; i++)
  {
    list_[i] = list_[i+1];
  }
  list_[count_] = nullptr;
}

void dyu::CompositeShape::remove(shape_ptr shape)
{
  for (int i = 0; i < count_; i++)
  {
    if (list_[i] == shape)
    {
      remove(i);
      return;
    }
  }
}

void dyu::CompositeShape::rotate(double angle)
{
  point_t tempRect = getFrameRect().pos;
  double radAngle = angle * M_PI / 180;
  
  for (int i = 0; i < count_; i++)
  {
    list_[i]->rotate(angle);
    double curX = list_[i]->getFrameRect().pos.x - tempRect.x;
    double curY = list_[i]->getFrameRect().pos.y - tempRect.y;
    double newX = curX * (std::cos(radAngle) - 1) - curY * std::sin(radAngle);
    double newY = curY * (std::cos(radAngle) - 1) - curX * std::sin(radAngle);
    list_[i]->move(newX, newY);
  }
}

void dyu::CompositeShape::writeParameters() const
{
  dyu::rectangle_t rectangle = getFrameRect();
  std::cout << "CompositeShape's centre is (" << getFrameRect().pos.x << ","
  << getFrameRect().pos.x << ")\n"
  << "Frame rectangle width = " << rectangle.width
  << ", height = " << rectangle.height << "\n"
  << "Area = " << getArea() << "\n";
}

dyu::point_t dyu::CompositeShape::getPos() const
{
  return getFrameRect().pos;
}

int dyu::CompositeShape::getShapeCount() const
{
  return count_;
}
