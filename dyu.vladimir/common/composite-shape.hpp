#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace dyu
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape&);
    CompositeShape(CompositeShape&&);
    CompositeShape(const shape_ptr&);
    CompositeShape& operator = (const CompositeShape&);
    CompositeShape& operator = (CompositeShape&&);
    shape_ptr operator [] (int i) const;
    
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &pos) override;
    void move(double x, double y) override;
    void scale(double coeficient) override;
    void add(shape_ptr shape);
    void remove(int number);
    void remove(shape_ptr);
    int getSize() const;
    void rotate(double angle) override;
    void writeParameters() const ;
    point_t getPos() const override;
    int getShapeCount() const;
    
  private:
    int count_;
    double angle_;
    shapes_array list_;
  };
}

#endif /* composite_shape_hpp */
