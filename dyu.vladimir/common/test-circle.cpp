#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

const double TOLERANCE = 0.01; // tolerance in percents

BOOST_AUTO_TEST_SUITE(test_A3_circle)

BOOST_AUTO_TEST_CASE(Test_circle_area)
{
  dyu::Circle testCircle(3.0, {10.0, 10.0});
  const double areaBeforeMoving = testCircle.getArea();
  const dyu::rectangle_t frameBeforeMoving = testCircle.getFrameRect();
  
  testCircle.move({0, 0});
  
  double areaAfterMoving = testCircle.getArea();
  dyu::rectangle_t frameAfterMoving = testCircle.getFrameRect();
  
  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, frameAfterMoving.width, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, frameAfterMoving.height, TOLERANCE);
  
  testCircle.move(4, 5);
  
  areaAfterMoving = testCircle.getArea();
  frameAfterMoving = testCircle.getFrameRect();
  
  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, frameAfterMoving.width, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, frameAfterMoving.height, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(Test_circle_scale)
{
  dyu::Circle testCircle(3.0, {10.0, 10.0});
  const double areaBeforeScaling = testCircle.getArea();
  
  const double coeficient = 2;
  testCircle.scale(coeficient);
  
  const double areaAfterScaling = testCircle.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScaling * coeficient * coeficient, areaAfterScaling, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(Test_circle_constructor_fail_throw)
{
  BOOST_CHECK_THROW(dyu::Circle circle(-3.0, {3.0, 2.0}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(Test_scale_circle_negativer_fail_throw)
{
  dyu::Circle testCircle(3.0, {10.0, 10.0});
  BOOST_CHECK_THROW(testCircle.scale(-1.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
