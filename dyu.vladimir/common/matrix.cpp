#include "matrix.hpp"

#include <iostream>

dyu::Matrix::Layer::Layer(dyu::Shape::shape_ptr *arr, int size) :
size_(size),
figures_(std::make_unique<Shape::shape_ptr[]>(size))
{
  for (int i = 0; i < size_; i++)
  {
    figures_[i] = arr[i];
  }
}

dyu::Shape::shape_ptr dyu::Matrix::Layer::operator [](int index) const
{
  if (index >= size_)
  {
    throw std::invalid_argument("Index is out of array");
  }
  
  return figures_[index];
}

int dyu::Matrix::Layer::getSize() const
{
  return size_;
}

dyu::Matrix::Matrix() :
rows_(0),
numberOfFigures(0)
{ }

dyu::Matrix::Matrix(const dyu::Matrix &other) :
rows_(other.rows_),
columns_(new int[other.rows_]),
figures_(std::make_unique<Shape::shape_ptr[]>(other.numberOfFigures)),
numberOfFigures(other.numberOfFigures)
{
  for (int i = 0; i < numberOfFigures; i++)
  {
    figures_[i] = other.figures_[i];
  }
  
  for (int i = 1; i < rows_; i++)
  {
    columns_[i] = other.columns_[i];
  }
}

dyu::Matrix::Matrix(dyu::Matrix &&other) :
rows_(other.rows_),
columns_(std::move(other.columns_)),
figures_(std::move(other.figures_)),
numberOfFigures(other.numberOfFigures)
{ }

dyu::Matrix &dyu::Matrix::operator =(const dyu::Matrix &other)
{
  if (this != &other)
  {
    rows_ = other.rows_;
    numberOfFigures = other.numberOfFigures;
    
    Shape::shapes_array tmpFigures = std::make_unique<Shape::shape_ptr[]>(other.numberOfFigures);
    for (int i = 0; i < numberOfFigures; i++)
    {
      tmpFigures[i] = other.figures_[i];
    }
    figures_.swap(tmpFigures);
    
    std::unique_ptr<int[]> tmpColumns = std::make_unique<int[]>(other.rows_);
    for (int i = 1; i < rows_; i++)
    {
      tmpColumns[i] = other.columns_[i];
    }
    columns_.swap(tmpColumns);
  }
  
  return *this;
}

dyu::Matrix &dyu::Matrix::operator =(dyu::Matrix &&other)
{
  if (this != &other)
  {
    rows_ = other.rows_;
    columns_ = std::move(other.columns_);
    figures_ = std::move(other.figures_);
    numberOfFigures = other.numberOfFigures;
  }
  
  return *this;
}

dyu::Matrix::Layer dyu::Matrix::operator [](int row) const
{
  if (row >= rows_)
  {
    throw std::invalid_argument("Index is out of array");
  }
  
  int index = 0;
  for (int i = 0; i < row; i++)
  {
    index += columns_[i];
  }
  
  return Layer(&figures_[index], columns_[row]);
}

void dyu::Matrix::add(Shape::shape_ptr shape, int row)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Can't be empty");
  }
  
  if (row > rows_)
  {
    throw std::invalid_argument("Argument must be less than or equal to the number of lines");
  }
  
  std::unique_ptr<Shape::shape_ptr[]> tmpFigures =
    std::make_unique<Shape::shape_ptr[]>(numberOfFigures + 1);
  
  int figureCounter = 0;
  int tmpCounter = 0;
  
  for (int tmpRow = 0; tmpRow < rows_; tmpRow++)
  {
    for (int column = 0; column < columns_[tmpRow]; column++)
    {
      tmpFigures[tmpCounter++] = figures_[figureCounter++];
    }
    
    if (row == tmpRow)
    {
      tmpFigures[tmpCounter++] = shape;
      columns_[tmpRow]++;
    }
  }
  
  if (row == rows_)
  {
    std::unique_ptr<int[]> tmpColumns = std::make_unique<int[]>(rows_ + 1);
    
    for (int tmpRow = 0; tmpRow < rows_; tmpRow++)
    {
      tmpColumns[tmpRow] = columns_[tmpRow];
    }
    
    tmpFigures[tmpCounter] = shape;
    tmpColumns[rows_] = 1;
    rows_++;
    columns_.swap(tmpColumns);
  }
  
  numberOfFigures++;
  figures_.swap(tmpFigures);
}

int dyu::Matrix::getRows() const
{
  return rows_;
}

int dyu::Matrix::getNumberOfFigures() const
{
  return numberOfFigures;
}

void dyu::Matrix::printInfo() const
{
  std::cout << "MATRIX" << std::endl;
  std::cout << "Number of rows: " << rows_ << std::endl;
  std::cout << "Number of figures: " << numberOfFigures << std::endl;
}
