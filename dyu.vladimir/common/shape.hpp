#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace dyu
{
  class Shape
  {
    public:
    using shape_ptr = std::shared_ptr<Shape>;
    using shapes_array = std::unique_ptr<shape_ptr[]>;
    
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t &pos) = 0;
    virtual void move(double x, double y) = 0;
    virtual void scale(double coeficient) = 0;
    virtual void rotate(double angle) = 0;
    int intersect(const shape_ptr Shape) const;
    virtual point_t getPos() const = 0;
  };
}

#endif // SHAPE_HPP
