#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "patrition.hpp"

void printShapeInfo(const dyu::Shape* Shape)
{
  double shapeArea = Shape->getArea();
  dyu::rectangle_t tempFrameRect = Shape->getFrameRect();
  std::cout << "Shape area is: " << shapeArea << std::endl;
  std::cout << "Shape frame rect params are:" << std::endl;
  std::cout << "Width: " << tempFrameRect.width << std::endl;
  std::cout << "Height: " << tempFrameRect.height << std::endl;
  std::cout << "Center: x: " << tempFrameRect.pos.x << ", y: " << tempFrameRect.pos.y << std::endl;
  std::cout << std::endl;
}


int main()
{
  dyu::Circle myCircle1(6.0, {4.0, 4.0});
  dyu::Rectangle myRectangle2(3.0, 1.0, {-7.0, 4.0});
  dyu::Rectangle myRectangle3(3.0, 3.0, {5.0, 4.0});

  dyu::CompositeShape::shape_ptr part1 = std::make_shared<dyu::Rectangle>(myRectangle2);
  dyu::CompositeShape::shape_ptr part2 = std::make_shared<dyu::Circle>(myCircle1);
  dyu::CompositeShape::shape_ptr part3 = std::make_shared<dyu::Rectangle>(myRectangle3);
  
  dyu::CompositeShape composite_shape;
  composite_shape.add(part1);
  composite_shape.add(part2);
  composite_shape.add(part3);

  composite_shape.move({0, 0});
  composite_shape.writeParameters();

  composite_shape.move(3, 3);
  composite_shape.writeParameters();

  composite_shape.rotate(45);
  composite_shape.writeParameters();

  composite_shape.scale(2);
  composite_shape.writeParameters();

  composite_shape.remove(1);
  composite_shape.writeParameters();

  dyu::Matrix matrix = dyu::split(composite_shape);
  for (int i = 0; i < matrix.getRows(); i++)
  {
    for (int j = 0; j < matrix.getNumberOfFigures(); j++)
    {
      if (matrix[i][j] != nullptr)
      {
        std::cout << "Layer no. " << i << ":\n" << "Figure no. " << j << ":\n";
        std::cout << "Position: (" << matrix[i][j]->getPos().x << "; "
        << matrix[i][j]->getPos().y << ")\n";
      }
    }
  }
  return 0;
}
