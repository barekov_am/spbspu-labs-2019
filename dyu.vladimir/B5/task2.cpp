#include <sstream>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <cmath>

#include "detail.hpp"

const int TRIANGLE_TOPS = 3;
const int RECTANGLE_TOPS = 4;
const int PENTAGON_TOPS = 5;
int countAllTops = 0;
int countTriangles = 0;
int countRectangles = 0;
int countSquares = 0;

Shape readPoints(const std::string &line, int tops){
  std::stringstream in(line);
  
  int pointNum = tops;
  
  if (pointNum < 3)
  {
    throw std::invalid_argument("Invalid nubver pointNumber");
  }
  
  Shape shape;
  for (int i = 0; i < pointNum; i++) {
    Point tmpPoint = {0, 0};
    
    in.ignore(line.length(), '(');
    in >> tmpPoint.x;
    in.ignore(line.length(), ';');
    in >> tmpPoint.y;
    in.ignore(line.length(), ')');
    shape.push_back(tmpPoint);
  }
  
  if (in.fail()){
    throw std::invalid_argument("Incorrect input!");
  }
  
  if (line.empty()){
    throw std::invalid_argument("Invalid number of arguments");
  }
  
  return shape;
}

void readShapes(std::vector<Shape> &shapesContainer, std::string &line){
  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Could not read");
    }

    std::stringstream in(line);
    std::string Tops;
    in >> Tops;

    if (Tops.empty())
    {
      continue;
    }

    if (in.eof())
    {
      throw std::invalid_argument("Invalid number of arguments");
    }
    
    int tops = std::stoi(Tops);
    if (tops < 3)
    {
      throw std::invalid_argument("Invalid value of tops");
    }
    std::getline(in, line);
    shapesContainer.push_back(readPoints(line, tops));
  }
}

int getLength(const Point &point1, const Point &point2){
  int x1x2 = point1.x - point2.x;
  int y1y2 = point1.y - point2.y;
  
  return (abs(x1x2) + abs(y1y2));
}

bool isRectangle(const Shape &shape){
  bool compareTopBot = getLength(shape[1], shape[2]) == getLength(shape[0], shape[3]);
  bool compareLeftRight = getLength(shape[0], shape[1]) == getLength(shape[2], shape[3]);
  bool comparediagonal = getLength(shape[0], shape[2]) == getLength(shape[1], shape[3]);
  
  return compareTopBot && compareLeftRight && comparediagonal;
}

bool isSquare(const Shape &shape)
{
  bool compareLeftTop  = getLength(shape[0], shape[1]) == getLength(shape[1], shape[2]);
  bool compareRightBot  = getLength(shape[2], shape[3]) == getLength(shape[1], shape[2]);
  
  return (isRectangle(shape) && compareLeftTop && compareRightBot);
}

bool isLess(const Shape &l, const Shape &r)
{
  if (l.size() < r.size())
  {
    return true;
  }
  
  if ((l.size() == RECTANGLE_TOPS) && (r.size() == RECTANGLE_TOPS))
  {
    if (isSquare(l))
    {
      if (isSquare(r))
      {
        return (l[0].x < r[0].x);
      }
      return true;
    }
  }
  return false;
}

void shapeCounter(const std::vector<Shape> &shapesContainer)
{
  for(const auto& shape : shapesContainer)
  {
    if (shape.size() == TRIANGLE_TOPS)
    {
      ++countTriangles;
    }
    
    if ((shape.size() == RECTANGLE_TOPS) && (isRectangle(shape)))
    {
      ++countRectangles;
      if (isSquare(shape))
      {
        ++countSquares;
      }
    }
    
    countAllTops += shape.size();
  }
}

void deletePentagon(std::vector<Shape> &shapes)
{
  shapes.erase(std::remove_if(shapes.begin(), shapes.end(), [](const Shape& shape)
    { return shape.size() == PENTAGON_TOPS; }), shapes.end());
}

std::vector<Point> makePointVec(const std::vector<Shape> &shapesContainer)
{

  std::vector<Point> pointVector;
 
  for (const auto& shape : shapesContainer)
  {
    pointVector.push_back(shape[0]);
  }
  
  return pointVector;
}



void task2(){
  
  std::vector<Shape> shapesContainer;
  std::string line;
  
  readShapes(shapesContainer, line);
  shapeCounter(shapesContainer);
  deletePentagon(shapesContainer);
  std::vector<Point> pointsVector = makePointVec(shapesContainer);
  std::sort(shapesContainer.begin(), shapesContainer.end(), isLess);
  std::cout << "Vertices: " << countAllTops<< std::endl;
  std::cout << "Triangles: " << countTriangles << std::endl;
  std::cout << "Squares: " << countSquares << std::endl;
  std::cout << "Rectangles: " << countRectangles << std::endl;
  std::cout << "Points: ";
  
  for (const auto& point : pointsVector)
  {
    std::cout << " (" << point.x << "; " << point.y << ")";
  }
  std::cout << std::endl;
  
  std::cout << "Shapes:" << std::endl;
  for (const auto& shape : shapesContainer)
  {
    std::cout << shape.size() << " ";
    for (const auto& point : shape)
    {
      std::cout << "(" << point.x << "; " << point.y << ") ";
    }
    std::cout << std::endl;
  }
}
