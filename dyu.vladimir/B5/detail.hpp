#ifndef DETAIL_HPP
#define DETAIL_HPP

#include <vector>
#include <list>

struct Point
{
  int x;
  int y;
};

using Shape = std::vector<Point>;

#endif
