#include <cmath>
#include <ctime>
#include <exception>
#include <vector>
#include <ctime>
#include <random>

#include "sort.hpp"
#include "print.hpp"

void fillRandom(double* array, int size)
{
  std::uniform_real_distribution<double> random(-1.0, 1.0);
  std::mt19937 rng(time(0));
  for (int i = 0; i < size; ++i)
  {
    array[i] = random(rng);
  }
}

void task4(const char* direction, int size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size cannot be < 0");
  }
  std::vector<double> vector(size);
  fillRandom(vector.data(), size);
  auto order = getOrder<double>(direction);
  print(vector);

  sort<access_at>(vector, order);
  print(vector);
}
