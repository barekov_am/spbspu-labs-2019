#include <exception>
#include <forward_list>
#include <vector>

#include "print.hpp"
#include "sort.hpp"

void task1(const char* direction)
{
  std::vector<int> vectorBracket;
  int i = -1;
  while (std::cin >> i)
  {
    vectorBracket.push_back(i);
  }
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Incorrect input data");
  }
  
  std::vector<int> vectorAt(vectorBracket);
  std::forward_list<int> list(vectorBracket.begin(), vectorBracket.end());
  auto order = getOrder<int>(direction);
  sort<access_at>(vectorAt, order);
  sort<access_bracket>(vectorBracket, order);
  sort<access_iter>(list, order);
  
  print(vectorBracket);
  print(vectorAt);
  print(list);
}


