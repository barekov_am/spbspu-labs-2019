#ifndef SORT_HPP
#define SORT_HPP

#include <iostream>
#include <functional>
#include <string.h>

#include "access.hpp"

template<typename Element>
std::function<bool(Element, Element)> getOrder(const char* direction)
{
  if (strcmp(direction, "ascending") == 0)
  {
    return [](Element a, Element b) { return a < b; };
  }
  if (strcmp(direction, "descending") == 0)
  {
    return [](Element a, Element b) { return a > b; };
  }
  throw std::invalid_argument("Incorrect sort order");
}

template<template<class> class AccessPolicy, class Container>
void sort(Container& arr, std::function<bool(typename Container::value_type, typename Container::value_type)> compare)
{
  typedef AccessPolicy<Container> Access;
  for (auto i = Access::begin(arr); i != Access::end(arr); ++i)
  {
    for (auto j = Access::next(i); j != Access::end(arr); ++j)
    {
      if (compare(Access::getElement(arr, j), Access::getElement(arr, i)))
      {
        std::swap(Access::getElement(arr, j), Access::getElement(arr, i));
      }
    }
  }
}

#endif
