#ifndef PRINT_HPP
#define PRINT_HPP

#include <iostream>

template<typename Container>
void print(const Container& arr)
{
  for (const auto& elem: arr)
  {
    std::cout << elem << " ";
  }
  std::cout << '\n';
}

#endif
