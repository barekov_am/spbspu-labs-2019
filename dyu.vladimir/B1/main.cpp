#include <string.h>
#include "sort.hpp"

void task1(const char* direction);
void task2(const char* filename);
void task3();
void task4(const char* direction, int size);

int main(int argc, char* argv[])
{
  if (argc == 1)
  {
    std::cerr << "Incorrect number of arguments";
    return 1;
  }
  try
  {
    const int taskNumber = atoi(argv[1]);
    switch (taskNumber)
    {
    case 1:
    {
      if (argc != 3)
      {
        std::cerr << "Incorrect input data";
        return 1;
      }
      if (argv[2] == nullptr)
      {
        std::cerr << "Incorrect parametr";
      }
      task1(argv[2]);
      break;
    }
    case 2:
    {
      if (argc < 3)
      {
        std::cerr << "Incorrect input data";
        return 1;
      }
      task2(argv[2]);
      break;
    }
    case 3:
    {
      task3();
      break;
    }
    case 4:
    {
      if (argc != 4)
      {
        std::cerr << "Incorrect input data";
        return 1;
      }
      if (argv[2] == nullptr)
      {
        std::cerr << "Incorrect parametr";
      }
      task4(argv[2], atoi(argv[3]));
      break;
    }
    default:
      std::cerr << "Incorrect task number";
      return 1;
    }
  }
  catch (const std::exception& ex)
  {
    std::cerr << ex.what();
    return 1;
  }
  return 0;
}
