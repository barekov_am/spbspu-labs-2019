#include "Shape.hpp"

Shape::Shape(int x, int y):
  point_({x, y})
{}


bool Shape::isMoreLeft(std::shared_ptr<Shape> figure){
  if (point_.x < figure->point_.x){
    return true;
  }
  else{
    return false;
  }
}

bool Shape::isUpper(std::shared_ptr<Shape> figure){
  if (point_.y > figure->point_.y){
    return true;
  }
  else{
    return false;
  }
}

Point Shape::getPoint() const{
  return point_;
}
