#include <vector>
#include <algorithm>
#include <string>
#include <iostream>

#include "Shape.hpp"
#include "Triangle.hpp"
#include "Square.hpp"
#include "Circle.hpp"

std::vector<std::shared_ptr<Shape>> filVector(){
    
   std::vector<std::shared_ptr<Shape>> ShapeVector;
   std::string line;
   while (std::getline(std::cin, line)){
     if (line.empty()){
       continue;
     }
     
     for (std::string::iterator it = line.begin() ; it<line.end(); it++ )
     {
       auto temp = it;
       if ((*it == ' ')||(*it == '\t')){
         while ((*temp == ' ')||(*temp == '\t')){
           ++temp;
         }
       line.erase(it, temp);
       }
     }
     
     if (!line.empty())
     {
       std::string shapeName = line.substr(0, (line.rfind("(")));
    
       if ((line.rfind(")") != std::string::npos) && (line.rfind(";") != std::string::npos) && (line.rfind("(") != std::string::npos)){
  
         auto rightBracket = line.rfind(")");
         auto leftBracket = line.rfind("(");
         auto semicolon = line.rfind(";");
         int x = std::stoi(line.substr(leftBracket + 1, semicolon - leftBracket - 1));
         int y = std::stoi(line.substr(semicolon + 1, rightBracket - semicolon - 1));
      
         std::shared_ptr<Shape> pointer;
         if (shapeName == "CIRCLE"){
           pointer = std::make_shared<Circle>(x,y);
         } else if (shapeName == "TRIANGLE"){
           pointer = std::make_shared<Triangle>(x,y);
         } else if (shapeName == "SQUARE"){
           pointer = std::make_shared<Square>(x,y);
         } else {
          throw std::invalid_argument("Invalid input shape name");
         }
         ShapeVector.push_back(pointer);
       } else throw std::invalid_argument("invalid input point");
     }
   }
   
   if (std::cin.fail() && !std::cin.eof())
   {
     throw std::invalid_argument("Incorrect input");
   }
  
  return ShapeVector;
}



void task2(){
  std::vector<std::shared_ptr<Shape>> shapeVector = filVector();
  
  std::cout << "Original:" << '\n';
  std::for_each(shapeVector.begin(), shapeVector.end(), [](const std::shared_ptr<Shape>& shape){ shape->draw(); });

  std::sort(shapeVector.begin(), shapeVector.end(), [](const std::shared_ptr<Shape>& shape1, const std::shared_ptr<Shape>& shape2)
    { return shape1->isMoreLeft(shape2);});

  std::cout << "Left-Right:" << '\n';
  std::for_each(shapeVector.begin(), shapeVector.end(), [](const std::shared_ptr<Shape>& shape){ shape->draw(); });

  std::reverse(shapeVector.begin(), shapeVector.end());

  std::cout << "Right-Left:" << '\n';
  std::for_each(shapeVector.begin(), shapeVector.end(), [](const std::shared_ptr<Shape>& shape){ shape->draw(); });

  std::sort(shapeVector.begin(), shapeVector.end(), [](const std::shared_ptr<Shape>& shape1, const std::shared_ptr<Shape>& shape2)
    { return shape1->isUpper(shape2);});

  std::cout << "Top-Bottom:" << '\n';
  std::for_each(shapeVector.begin(), shapeVector.end(), [](const std::shared_ptr<Shape>& shape){ shape->draw(); });

  std::reverse(shapeVector.begin(), shapeVector.end());

  std::cout << "Bottom-Top:" << '\n';
  std::for_each(shapeVector.begin(), shapeVector.end(), [](const std::shared_ptr<Shape>& shape){ shape->draw();});
}
