#ifndef DOUBLEFUNCTOR_HPP
#define DOUBLEFUNCTOR_HPP

#include <stdio.h>
#include <cmath>

class DoubleFunctor{
  public:
    void operator()(double value);
    void print();
  
  private:
    double pi_ = M_PI;
    double newValue;
};

#endif
