#include <iostream>

#include "Triangle.hpp"

Triangle::Triangle(int x, int y):
  Shape(x, y)
{}

  void Triangle::draw()const{
  std::cout << "TRIANGLE (" << getPoint().x << "; " << getPoint().y << ")" << '\n';
}
