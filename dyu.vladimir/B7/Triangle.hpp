#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include <stdio.h>

#include "Shape.hpp"

class Triangle:public Shape{
  public:
    Triangle(int x, int y);
    void draw()const override;
};

#endif
