#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>

struct Point {
  int x;
  int y;
};

class Shape{
  public:
    Shape(int x, int y);
    virtual ~Shape() = default;
    virtual void draw() const = 0;
  
    bool isMoreLeft(std::shared_ptr<Shape> figure);
    bool isUpper(std::shared_ptr<Shape> figure);
    Point getPoint() const;
  
  private:
    Point point_;
};

#endif
