#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>

#include "doubleFunctor.hpp"

void task1(){
  std::vector<double> doubleVector;
  double doubleValue;
    
  while (std::cin >> doubleValue) {
    doubleVector.push_back(doubleValue);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Invalid Input");
  }
  

  if (!doubleVector.empty()){
    std::for_each(doubleVector.begin(), doubleVector.end(), DoubleFunctor());
    }
};
