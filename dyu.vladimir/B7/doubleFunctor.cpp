#include <iostream>

#include "doubleFunctor.hpp"

void DoubleFunctor::operator()(double value){
  newValue = value * pi_;
  std::cout << newValue << '\n';
}
