#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  dyu::Rectangle rect(3.0, 6.0, {10.0, 10.0});
  dyu::Circle circle(3.0, {2.0, 3.0});
    
  dyu::CompositeShape::shape_ptr part1 = std::make_shared<dyu::Rectangle>(rect);
  dyu::CompositeShape::shape_ptr part2 = std::make_shared<dyu::Circle>(circle);
  
  std::cout << "list of arbitrary shapes" << std::endl;
  dyu::CompositeShape CompShape;
  CompShape.add(part1);
  CompShape.add(part2);
  CompShape.writeParameters();
  CompShape.move(2, 3);
  CompShape.scale(2);
  std::cout << "list after moving and scaling" << std::endl;
  CompShape.writeParameters();
  
  std::cout << "list after delete" << std::endl;
  CompShape.remove(1);
  CompShape.writeParameters();
  
  return 0;
}
