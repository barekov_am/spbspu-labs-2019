#include <iostream>
#include <cstring>
#include <sstream>
#include <vector>

#include "implementationQueue.hpp"

void task1()
{
  QueueWithPriority<std::string> queue;
  std::string line;
  
  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed during reading");
    }
    
    std::stringstream input(line);
    std::string command;
    
    input >> command;
    if (command == "add")
    {
      std::string priority;
      std::string data;
      input >> priority;
      std::getline(input >> std::ws, data);
    
      if (data.empty())
      {
        std::cout << "<INVALID COMMAND>\n";
        continue;
      }
      if (priority == "low")
      {
        queue.add(QueueWithPriority<std::string>::ElementPriority::low, data);
      }
      else if (priority == "normal")
      {
        queue.add(QueueWithPriority<std::string>::ElementPriority::normal, data);
      }
      else if (priority == "high")
      {
        queue.add(QueueWithPriority<std::string>::ElementPriority::high, data);
      }
      else
      {
        std::cout << "<INVALID COMMAND>\n";
      }
    }
    
    else if ((command == "get"))
    {
      try
      {
        std::cout << queue.Get() << '\n';
      }
      catch (std::invalid_argument)
      {
        std::cout << "<EMPTY>\n";
      }
    }
      
    else if (command == "accelerate")
    {
      queue.Accelerate();
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}


