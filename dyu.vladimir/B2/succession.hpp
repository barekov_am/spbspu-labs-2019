#ifndef SUCCESION_HPP
#define SUCCESION_HPP

#include <iostream>
#include <list>
#include <stdexcept>

template<typename T>
class QueueWithPriority
{
public:
  enum class ElementPriority
  {
    low,
    normal,
    high
  };

  void add(ElementPriority priority, const T& element);
  T Get();
  void Accelerate();
  bool empty();
  
private:
  struct ElementWithPriority
  {
    ElementPriority priority;
    T element;
  };
  
  std::list<ElementWithPriority> queue_;
};

#endif
