#include <deque>
#include <iostream>

void task2()
{
  std::deque<int> deque;
  int x  = -1;
  const int minValue = 1;
  const int maxValue = 20;
  
  while (std::cin >> x)
  {
    if ((x < minValue) || (x > maxValue))
    {
      throw std::invalid_argument("Incorrect input elem");
    }
    deque.push_back(x);
  }
  
  if  (deque.size() > maxValue)
  {
    throw std::invalid_argument("Omitted due to size.");
  }
  
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Incorrect input data");
  }
  
  if (deque.empty())
  {
    return;
  }
  
  std::deque<int>::iterator begin = deque.begin();
  std::deque<int>::iterator end = deque.end();
    
  while (begin != end)
  {
    std::cout << *begin;
    std::cout << " ";
    begin++;
    if (begin == end)
    {
      break;
    }
    --end;
    std::cout << *end;
    std::cout << " ";
  }
  deque.clear();
  std::cout << "\n";
}

