#include <stdexcept>
#include <iostream>
#include "task.hpp"

int main()
{
  try
  {
    task();
  }
  catch (const std::invalid_argument& e)
  {
    std::cerr << e.what() << '\n';
    return 1;
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << '\n';
    return 2;
  }

  return 0;
}
