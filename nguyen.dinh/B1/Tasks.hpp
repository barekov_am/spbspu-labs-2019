#ifndef TASK_HEADERS_HPP
#define TASK_HEADERS_HPP

void Task1(const char *sort_type);
void Task2(const char *filename);
void Task3();
void Task4(const char *sort_type, const char *array_size);

#endif
