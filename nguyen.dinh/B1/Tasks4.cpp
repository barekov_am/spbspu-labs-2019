#include <iostream>
#include <stdexcept>
#include <vector>
#include <random>
#include <cstring>

#include "declarations.hpp"

void fillRandom(double *array, int size)
{
  std::mt19937 Rng(time(0));
  std::uniform_real_distribution<double> distr(-1.0, 1.0);
  for (int i = 0; i < size; i++)
  {
    array[i] = distr(Rng);
  }
}

void Task4(const char *sort_type, const char *array_size)
{
  int size = atoi(array_size);
  if (size == 0)
  {
    throw std::invalid_argument("Incorrect array size.");
  }

  std::vector<double> vec(size);
  fillRandom(&vec[0], size);

  auto comp = detail::chooseDirection<double>(sort_type);

  detail::printCont<std::vector<double> >(vec);
  detail::sort<detail::bracketAccess>(vec, comp);
  detail::printCont<std::vector<double> >(vec);
}
