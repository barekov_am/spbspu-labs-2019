#ifndef DETAILS_HPP
#define DETAILS_HPP

#include <iostream>
#include <functional>
#include <string.h>
#include <cstddef>
#include <vector>


namespace detail
{
  template <typename C>
  struct bracketAccess
  {
    typedef typename C::size_type indexType;
    static typename C::reference getElement(C& cont, indexType i)
    {
      return cont[i];
    };
    static indexType getBegin(const C&)
    {
      return 0;
    };
    static indexType getEnd(const C& cont)
    {
      return cont.size();
    };
  };
  
  template <typename C>
  struct atAccess
  {
    typedef typename C::size_type indexType;
    static typename C::reference getElement(C& cont, indexType i)
    {
      return cont.at(i);
    };
    static indexType getBegin(C&)
    {
      return 0;
    };
    static indexType getEnd(const C& cont)
    {
      return cont.size();
    };
  };
  
  template <typename C>
  struct iteratorAccess
  {
    typedef typename C::iterator indexType;
    static typename C::reference getElement(C&, indexType i)
    {
      return *i;
    };
    static indexType getBegin(C& cont)
    {
      return cont.begin();
    };
    static indexType getEnd(C& cont)
    {
      return cont.end();
    };
  };
  
 template <typename C>
  std::function<bool(const C&, const C&)> chooseDirection(const char *sort_type)
  {
    if (strcmp(sort_type, "ascending") == 0)
    {
      return [](C a, C b) {return a > b; };
    }
    else if (strcmp(sort_type, "descending") == 0)
    {
      return [](C a, C b) {return a < b; };
    }
    else
    {
    throw std::invalid_argument("Invalid direction parametr");
    }
  }

  template <template <typename C> class Traits, class C>
  void sort (C& cont, std::function<bool(typename C::value_type,typename C::value_type)> compare)
  {
    typedef typename Traits<C>::indexType index;
    for (index i = Traits<C>::getBegin(cont); i != Traits<C>::getEnd(cont); ++i)
    {
      for (index j=i; j != Traits<C>::getEnd(cont); ++j)
      {
       if(compare(Traits<C>::getElement(cont,i),Traits<C>::getElement(cont,j)))
        {
          std::swap (Traits<C>::getElement(cont,i),Traits<C>::getElement(cont,j));
        }
      }
    }
  }
  
  template <typename C>
  void printCont(const C& cont)
  {
    for (typename C::const_iterator it = cont.begin(); it != cont.end(); it++)
    {
      std::cout << *it << " ";
    }
    std::cout << "\n";
  }
}
#endif
