#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

void actions(nguyen::CompositeShape &usingcompositeShape)
{
  usingcompositeShape.printInfo();
  std::cout << "Move shape for 5 right, 6 up" << std::endl;
  usingcompositeShape.move(5, 6);
  usingcompositeShape.printInfo();
  std::cout << "Move shape to (4,7)" << std::endl;
  usingcompositeShape.move({ 4, 7 });
  usingcompositeShape.printInfo();
  usingcompositeShape.scale(2);
  std::cout << "Area after scale = " << usingcompositeShape.getArea() << "\n";
  usingcompositeShape.remove(static_cast<std::size_t>(0));
  std::cout << "Area after delete Shape = " << usingcompositeShape.getArea() << "\n";
}

int main()
{
  nguyen::Rectangle rectangle(4, 4, { 4, 5 });
  nguyen::Circle circle(3, { 6, 9 });
  nguyen::CompositeShape compositeShape(std::make_shared<nguyen::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<nguyen::Circle>(circle));
  nguyen::Triangle triangle({ 4, 10 }, { 1, 1 }, { 10, 1 });
  compositeShape.add(std::make_shared<nguyen::Triangle>(triangle));
  actions(compositeShape);
  nguyen::CompositeShape compositeShape2 = compositeShape;
  actions(compositeShape2);
  nguyen::CompositeShape compositeShape3(compositeShape);
  if (compositeShape3.getArea() != compositeShape2.getArea())
  {
    std::cout << "Two composite shapes are different\n";
  }
  return 0;
}
