#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "partition.hpp"
#include "matrix.hpp"

int main()
{
  nguyen::Rectangle rectangle(4, 4, { 4, 5 });
  nguyen::Circle circle(4, { 6, 9 });
  nguyen::Triangle triangle({ 4, 6 }, { 6, 7 }, { 1, 10 });
  std::cout << rectangle.getArea() << std::endl;
  std::cout << circle.getArea() << std::endl;
  std::cout << triangle.getArea() << std::endl;
  nguyen::CompositeShape compositeShape(std::make_shared<nguyen::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<nguyen::Circle>(circle));
  compositeShape.add(std::make_shared<nguyen::Triangle>(triangle));
  std::cout << compositeShape.getArea() << std::endl;
  std::unique_ptr<nguyen::Shape::shapePtr []> shape = std::make_unique<nguyen::Shape::shapePtr []>(5);
  nguyen::Rectangle rectangle1(1, 2, { 2, 4.5 });
  nguyen::Rectangle rectangle2(2, 5, { 3.5, 2 });
  nguyen::Circle circle1(2, { 6, 4 });
  nguyen::Triangle triangle1({ 3, 2 }, { 2, 0 }, { 4, 0 });
  nguyen::Triangle triangle2({ 7, 4 }, { 9, 3 }, { 9, 5 });
  shape[0] = std::make_shared<nguyen::Rectangle>(rectangle1);
  shape[1] = std::make_shared<nguyen::Rectangle>(rectangle2);
  shape[2] = std::make_shared<nguyen::Circle>(circle1);
  shape[3] = std::make_shared<nguyen::Triangle>(triangle1);
  shape[4] = std::make_shared<nguyen::Triangle>(triangle2);
  nguyen::Matrix matrix = nguyen::division(shape, 5);
  matrix.showAll();
  return 0;
}

