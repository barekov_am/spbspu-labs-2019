#include <iostream>
#include <vector>
#include <algorithm>

#include "dataStruct.hpp"

int main()
{
  try
  {
    std::vector<DataStruct> vector;

    while (std::cin.peek() != EOF)
    {
      if (std::cin.fail())
      {
        throw std::ios_base::failure("Input failed");
      }
      vector.push_back(readData());
    }

    auto compare = [](const DataStruct &lhs, const DataStruct &rhs)
    {
      if (lhs.key1 < rhs.key1)
      {
        return true;
      }
      if (lhs.key1 == rhs.key1)
      {
        if (lhs.key2 < rhs.key2)
        {
          return true;
        }
        if (lhs.key2 == rhs.key2)
        {
          if (lhs.str.size() < rhs.str.size())
          {
            return true;
          }
        }
      }
      return false;
    };

    std::sort(vector.begin(), vector.end(), compare);

    for (auto i = vector.begin(); i != vector.end(); i++)
    {
      std::cout << i->key1 << "," << i->key2 << "," << i->str << "\n";
    }
  }
  catch (const std::exception &ex)
  {
    std::cerr << ex.what();
    return 1;
  }
  return 0;
}
