#include "phoneBook.hpp"

nguyen::PhoneBook::PhoneBook()
{
  bookmarks_.emplace_back(records_.end(), "current");
  curMark_ = bookmarks_.begin();
}

bool nguyen::PhoneBook::addRecord(const std::string & name, const std::string & number)
{
  const auto size = records_.size();
  records_.emplace_back(std::make_pair(name,number));

  if (size < records_.size()){
    if (curMark_->first == records_.end()){
      --curMark_->first;
    }
    return true;
  }
  return false;
}

bool nguyen::PhoneBook::addMark(std::string& name)
{
  const auto size = records_.size();
  auto NewMarkIter = curMark_->first;
  bookmarks_.emplace_back(NewMarkIter, name);
  return(size < records_.size());
}

nguyen::PhoneBook::record_t& nguyen::PhoneBook::getCurRecord() const
{
  return *(curMark_->first);
}

void nguyen::PhoneBook::goPrev(size_t display)
{
  const size_t distance = std::distance(records_.begin(), curMark_->first);
  if (distance < display)
  {
    display = distance;
  }
  curMark_->first = std::prev(curMark_->first, display);
}

void nguyen::PhoneBook::goNext(size_t display)
{
  const size_t distance = std::distance(curMark_->first, std::prev(records_.end()));
  if (distance < display)
  {
    display = distance;
  }
  curMark_->first = std::next(curMark_->first, display);
}

bool nguyen::PhoneBook::insertAfter(const std::string & name, const std::string & number)
{
  const auto size = records_.size();

  if (curMark_->first == records_.end())
  {
    records_.emplace(curMark_->first, name, number);
    --curMark_->first;
    return true;
  }

  const auto nextMark(curMark_);
  ++(nextMark->first);
  records_.emplace(nextMark->first, name, number);

  return(size < records_.size());
}

bool nguyen::PhoneBook::insertBefore(const std::string & name, const std::string & number)
{
  const auto size = records_.size();

  if (curMark_->first == records_.end())
  {
    records_.emplace(curMark_->first, name, number);
    --curMark_->first;
    return true;
  }

  records_.emplace(curMark_->first, name, number);
  return (size < records_.size());
}

size_t nguyen::PhoneBook::getSize() const
{
  return records_.size();
}

bool nguyen::PhoneBook::makeCurrent(const std::string& name)
{
  for (auto i = bookmarks_.begin(); i != bookmarks_.end();++i)
  {
    if (i->second == name){
      curMark_ = i;
      return true;
    }
  }

  return false;
}

void nguyen::PhoneBook::del()
{
  if (curMark_->first == records_.end())
  {
    return;
  }

  const auto deleted = curMark_->first;
  auto lastElem = records_.end();
  --lastElem;

  for (auto& bookmark : bookmarks_)
  {
    if (bookmark.first == deleted)
    {
      if ((bookmark.first == lastElem) && (records_.size() > 1))
      {
        --(bookmark.first);
      }
      else
      {
        ++bookmark.first;
      }
    }
  }

  records_.erase(deleted);
}
