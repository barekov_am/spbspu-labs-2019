#include <iostream>
#include <stdexcept>
#include "interface.hpp"

nguyen::UserInterface::UserInterface(PhoneBook& book, std::ostream & output):
  book_(book),
  output_(output)
{}

void nguyen::UserInterface::chooseAction(std::istream& command) const
{
  std::string buf;

  command >> buf;
  if (buf.empty())
  {
    return;
  }
  if (buf == "add")
  {
    add(command);
  }
  else if (buf == "store")
  {
    store(command);
  }
  else if (buf == "insert")
  {
    insert(command);
  }
  else if (buf == "delete")
  {
    del(command);
  }
  else if (buf == "show")
  {
    show(command);
  }
  else if (buf == "move")
  {
    move(command);
  }
  else
  {
    output_ << "<INVALID COMMAND>" << std::endl;
  }
}

void nguyen::UserInterface::add(std::istream& command) const
{
  std::string number;
  std::string name;
  command >> number;
  command.ignore();
  std::getline(command, name);

  if (!hasQuotes(name))
  {
    return;
  }

  formatName(name);

  if (name.empty() || number.empty())
  {
    output_ << "<INVALID COMMAND>" <<std::endl;
  }
  else
  {
    book_.addRecord(name, number);
  }
}

void nguyen::UserInterface::store(std::istream& command) const
{
  std::string markName;
  std::string newMarkName;

  command >> markName >> newMarkName;

  if ((markName.empty()) || (newMarkName.empty()))
  {
    output_ << "<INVALID COMMAND>" <<std::endl;
  }
  else
  {
    const bool isValid = book_.makeCurrent(markName);
    if (!isValid)
    {
      output_ << "<INVALID BOOKMARK>" <<std::endl;
    }
    else
    {
      book_.addMark(newMarkName);
    }
  }
}

void nguyen::UserInterface::insert(std::istream& command) const
{
  std::string pos;
  std::string markName;
  std::string name;
  std::string number;
  command >> pos >> markName >> number;
  command.ignore();
  std::getline(command, name);

  if (!hasQuotes(name))
  {
    return;
  }

  formatName(name);

  if (!book_.makeCurrent(markName))
  {
    output_ << "<INVALID BOOKMARK>" <<std::endl;
    return;
  }

  if (pos == "before"){
    book_.insertBefore(name, number);
  }
  else if (pos == "after"){
    book_.insertAfter(name, number);
  }
  else
  {
    output_ << "<INVALID COMMAND>";
  }
}

void nguyen::UserInterface::show(std::istream& command) const
{
  std::string markName;
  command >> markName;

  if (!book_.makeCurrent(markName))
  {
    output_ << "<INVALID BOOKMARK>" << std::endl;
  }
  else if (book_.getSize() == 0)
  {
    output_ << "<EMPTY>" <<std::endl;
  }
  else
  {
    try
    {
      output_ << book_.getCurRecord().second << ' ' << book_.getCurRecord().first << std::endl;
    }
    catch (std::logic_error)
    {
      return;
    }
  }
}

void nguyen::UserInterface::move(std::istream& command) const
{
  std::string markName;
  std::string steps;
  command >> markName >> steps;

  const bool isValid = book_.makeCurrent(markName);
  if (!isValid)
  {
    output_ << "<INVALID BOOKMARK>" << std::endl;
    return;
  }

  if (steps == "last")
  {
    book_.goNext(book_.getSize());
  }
  else if (steps == "first"){
    book_.goPrev(book_.getSize());
  }
  else
  {
    const auto error = new size_t(0);

    auto numStep = 0;

    try{
      numStep = stoi(steps, error);
    }
    catch (std::invalid_argument)
    {
      output_ << "<INVALID STEP>" << std::endl;
      delete error;
      return;
    }

    if (*error != steps.size()){
      output_ << "<INVALID STEP>" << std::endl;
      delete error;
      return;
    }
    delete error;

    if (numStep>0){
      book_.goNext(numStep);
    }
    else if (numStep<0){
      book_.goPrev(abs(numStep));
    }
  }
}

void nguyen::UserInterface::del(std::istream& command) const
{
  std::string markName;
  command >> markName;

  if (!book_.makeCurrent(markName))
  {
    output_ << "<INVALID BOOKMARK>" << std::endl;
    return;
  }

  book_.del();
}

bool nguyen::UserInterface::hasQuotes(const std::string& name) const
{
  if (name[0]!='"' || name[name.size()-1]!='"')
  {
    output_ << "<INVALID COMMAND>" << std::endl;
    return false;
  }

  return true;
}

void nguyen::UserInterface::formatName(std::string& name)
{
  auto iter = name.begin();
  while (iter != name.end()){
    if ((*iter == '\\') && ((*(iter+1) == '\\') || (*(iter+1) == '"')))
    {
      iter = name.erase(iter) + 1;
    }
    else if (*iter == '"')
    {
      iter = name.erase(iter);
    }
    else
    {
      ++iter;
    }
  }
}
