#ifndef INTERFACE_HPP
#define INTERFACE_HPP
#include "phoneBook.hpp"

namespace nguyen
{
  class UserInterface
  {
    public:
      UserInterface(PhoneBook& book, std::ostream& output);
      void chooseAction(std::istream& command) const;
      void add(std::istream& command) const;
      void store(std::istream& command) const;
      void insert(std::istream& command) const;
      void show(std::istream& command) const;
      void move(std::istream& command) const;
      void del(std::istream& command) const;

    private:
      PhoneBook& book_;
      std::ostream& output_;
      UserInterface() = delete;
      bool hasQuotes(const std::string& name) const;
      static void formatName(std::string& name);

  };

}
#endif
