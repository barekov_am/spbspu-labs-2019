#include <iterator>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <functional>


void task1()
{
  std::vector<double> list((std::istream_iterator<double>(std::cin)), std::istream_iterator<double>());
  
  if (!std::cin.eof())
  {
    throw std::invalid_argument("There is no input!!");
  }
  
  std::transform(list.begin(), list.end(), list.begin(),
                 std::bind(std::multiplies<double>(), std::placeholders::_1, M_PI));
  
  for (double numbers : list)
  {
    std::cout << numbers << " ";
  }
  
  std::cout << '\n';
}
