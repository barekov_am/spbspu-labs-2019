#ifndef FORMATTEXT_HPP
#define FORMATTEXT_HPP

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <stdexcept>
#include <locale>

template<typename Iterator>
class Reader
{
public:
  Reader(size_t line_width);
  void read_from_input(Iterator begin, Iterator end);
  
private:
  const size_t line_width_;
  std::vector<char> string_;
  std::vector<char> vector_read;
  Iterator iter_;
  Iterator end_;
  void check_division();
};

template<typename Iterator>
Reader<Iterator>::Reader(size_t line_width):
line_width_(line_width),
string_(),
vector_read(),
iter_(),
end_()
{
};

template<typename Iterator>
void Reader<Iterator>::read_from_input(Iterator begin, Iterator end)
{
  if (begin == end)
  {
    return;
  }
  char dec_point = std::use_facet< std::numpunct<char> >(std::cout.getloc()).decimal_point();
  iter_ = begin;
  end_ = end;
  if (ispunct(*iter_))
  {
    if ((*iter_ == '+' || *iter_ == '-') && isdigit(*(iter_+1)))
    {
    }
    else
    {
      std::cerr<<"Text can't begin with punctuation!\n";
      exit(1);
    }
  }
  while (iter_ != end_)
  {
    if ((*iter_ == '+' && isdigit(*(iter_+1))) || isdigit(*iter_))
    {
      int k=1;
      check_division();
      vector_read.push_back(*iter_);
      iter_++;
      while (iter_ != end_)
      {
        if (isdigit(*iter_)|| *iter_ == dec_point)
        {
          k++;
          vector_read.push_back(*iter_);
          iter_++;
        }
        else
        {
          break;
        }
      }
      if (k > 20)
      {
        std::cerr<<"Number is too big!";
        exit(1);
      }
    }
    else if (*iter_ == '-' && isdigit(*(iter_+1)))
    {
      int k=1;
      check_division();
      vector_read.push_back(*iter_);
      iter_++;
      while (iter_ != end_)
      {
        if (isdigit(*iter_)|| *iter_ == dec_point)
        {
          k++;
          vector_read.push_back(*iter_);
          iter_++;
        }
        else
        {
          break;
        }
      }
      if (k > 20)
      {
        std::cerr<<"Number is too big!";
        exit(1);
      }
    }
    else if (*iter_ == '-' && isalpha(*(iter_+1)))
    {
      std::cerr<<"Word can't begin with '-'!";
      exit(1);
    }
    else if (isalpha(*iter_))
    {
      check_division();
      int k=1;
      vector_read.push_back(*iter_);
      iter_++;
      while (iter_ != end_)
      {
        if (isalpha(*iter_))
        {
          k++;
          vector_read.push_back(*iter_);
          iter_++;
        }
        else if (*iter_ == '-')
        {
          if (isalpha(*(iter_+1)))
          {
            k++;
            vector_read.push_back(*iter_);
            iter_++;
          }
          else
          {
            k++;
            vector_read.push_back(*iter_);
            iter_++;
            break;
          }
        }
        else
        {
          break;
        }
      }
      if (k > 20)
      {
        std::cerr<<"Word is too big!";
        exit(1);
      }
    }
    else if (isspace(*iter_))
    {
      if (*iter_ != ' ')
      {
        iter_++;
      }
      else if (*(iter_+1) == ' ')
      {
        iter_++;
        while (*iter_ == ' ')
        {
          iter_++;
        }
      }
      else
      {
        iter_++;
      }
    }
    else if (ispunct(*iter_))
    {
      if (*iter_ == '+')
      {
        std::cerr<<"Wrong punctuation!";
        exit(1);
      }
      Iterator iter2 = vector_read.end();
      iter2--;
      if (*iter_ == ',')
      {
        if (ispunct(*iter2) || ispunct(*(iter_+1)))
        {
          if (*iter2 == '-')
          {
            iter2--;
            if (isalpha(*iter2))
            {
              iter2++;
            }
            else
            {
              std::cerr<<"Wrong punctuation!";
              exit(1);
            }
          }
          else
          {
            std::cerr<<"Wrong punctuation!";
            exit(1);
          }
        }
        if (*(iter_+1) == '-' && *(iter_+2) == '-' && *(iter_+3) == '-')
        {
          vector_read.push_back(*iter_);
          iter_++;
        }
        else
        {
          vector_read.push_back(*iter_);
          iter_++;
        }
      }
      else if (*iter_ == '-')
      {
        if (*(iter_+1) == '-' && *(iter_+2) == '-')
        {
          if ((ispunct(*iter2) && *iter2 != ',') || ispunct(*(iter_+3)))
          {
            if (*iter2 == '-')
            {
              iter2--;
              if (isalpha(*iter2))
              {
                iter2++;
              }
              else
              {
                std::cerr<<"Wrong punctuation!";
                exit(1);
              }
            }
            else
            {
              std::cerr<<"Wrong punctuation!";
              exit(1);
            }
          }
          iter_--;
          if (*(iter_) != ' ' || *(iter2) != ' ')
          {
            vector_read.push_back(' ');
          }
          iter_++;
          vector_read.push_back(*iter_);
          iter_++;
          vector_read.push_back(*iter_);
          iter_++;
          vector_read.push_back(*iter_);
          iter_++;
        }
        else
        {
          std::cerr<<"Wrong punctuation!";
          exit(1);
        }
      }
      else
      {
        if (ispunct(*iter2) || ispunct(*(iter_+1)))
        {
          if (*iter2 == '-')
          {
            iter2--;
            if (isalpha(*iter2))
            {
              iter2++;
            }
            else
            {
              std::cerr<<"Wrong punctuation!";
              exit(1);
            }
          }
          else
          {
            std::cerr<<"Wrong punctuation!";
            exit(1);
          }
        }
        vector_read.push_back(*iter_);
        iter_++;
      }
    }
    else
    {
      std::cerr<<"Incorrect input!";
      exit(1);
    }
  }
  check_division();
  if (!string_.empty())
  {
    std::copy(string_.begin(), string_.end() - 1, std::ostream_iterator<char>(std::cout));
    std::cout << std::endl;
  }
}

template<typename Iterator>
void Reader<Iterator>::check_division()
{
  size_t size_of_vector = vector_read.size();
  size_t size_of_string = string_.size();
  if (size_of_vector + size_of_string < line_width_)
  {
    string_.insert(string_.end(), vector_read.begin(), vector_read.end());
    if (!string_.empty())
    {
      string_.push_back(' ');
    }
  }
  else if (size_of_vector + size_of_string == line_width_)
  {
    std::copy(string_.begin(), string_.end(), std::ostream_iterator<char>(std::cout));
    std::copy(vector_read.begin(), vector_read.end(), std::ostream_iterator<char>(std::cout));
    std::cout << std::endl;
    string_.clear();
  }
  else
  {
    std::copy(string_.begin(), string_.end() - 1, std::ostream_iterator<char>(std::cout));
    std::cout << std::endl;
    string_.clear();
    std::vector<char>::iterator begin = vector_read.begin();
    string_.insert(string_.end(), begin, vector_read.end());
    string_.push_back(' ');
  }
  vector_read.clear();
}

#endif
