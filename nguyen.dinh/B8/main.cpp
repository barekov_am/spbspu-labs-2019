#include <iostream>
#include <vector>
#include <stdexcept>
#include <cstring>
#include <iterator>
#include <algorithm>
#include "FormatText.hpp"

int main(int argc, char *argv[])
{
  if (argc > 3 || argc == 2)
  {
    std::cerr << "Invalid number of arguments!\n";
    exit(1);
  }
  try
  {
    size_t line_width = 40;
    if (argc == 3)
    {
      if (strcmp(argv[1], "--line-width") != 0)
      {
        std::cerr << "Invalid arguments!\n";
        exit(1);
      }
      line_width = std::stoi(argv[2]);
      if (line_width < 25)
      {
        std::cerr << "Small argument --line-width!\n";
        exit(1);
      }
    }
    Reader<std::vector<char>::iterator>reader(line_width);
    std::vector<char> input_vector;
    std::cin >> std::noskipws;
    std::copy(std::istream_iterator<char>(std::cin), std::istream_iterator<char>(), std::back_inserter(input_vector));
    if (!std::cin.eof())
    {
      std::cerr << "Wrong input!\n";
      exit(1);
    }
    reader.read_from_input(input_vector.begin(), input_vector.end());
  }
  catch (std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    exit(1);
  }
  catch (...)
  {
    std::cerr << "Error!\n";
    exit(1);
  }
  return 0;
}
