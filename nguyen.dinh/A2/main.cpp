#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

void actions(nguyen::Shape *usingShape)
{
  if (usingShape == 0)
  {
    throw std::invalid_argument("Radius is surely invalid");
  }
  usingShape->printInfo();
  std::cout << "Move shape for 5 right, 6 up" << std::endl;
  usingShape->move(5, 6);
  usingShape->printInfo();
  std::cout << "Move shape to (4,7)" << std::endl;
  usingShape->move({ 4, 7 });
  usingShape->printInfo();
}

void actionsAfter(nguyen::Shape *usingShape)
{
  if (usingShape == 0)
  {
    throw std::invalid_argument("Radius is surely invalid");
  }
  std::cout << "After change: " << std::endl;
  usingShape->printInfo();
  std::cout << "\n";
}

int main()
{
  nguyen::Rectangle rectangle(10.0, 5.0, { 15.0, 15.0 });
  nguyen::Circle circle(2.5, { 3.14, 2.71 });
  nguyen::Triangle triangle({ 10, 10 }, { 10, 30 }, { 40, 30 });
  
  actions(&circle);
  circle.scale(8);
  actionsAfter(&circle);
  
  actions(&rectangle);
  rectangle.scale(4.6);
  actionsAfter(&rectangle);
  
  actions(&triangle);
  triangle.scale(5);
  actionsAfter(&triangle);
  
  return 0;
}
