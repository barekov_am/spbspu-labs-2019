#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <iostream>

namespace nguyen
  {

  class Functor
  {
  public:
    Functor();
    void operator()(int elem);
    void output(std::ostream& stream);

  private:
    long int min_;
    long int max_;
    double mean_;
    long int positive_;
    long int negative_;
    long int oddSum_;
    long int evenSum_;
    bool firstLastEqual_;
    long int totalAmount_;
    long int firstElem_;
  };
}

#endif
