#include "functor.hpp"

nguyen::Functor::Functor() :
  min_(0),
  max_(0),
  mean_(0.0),
  positive_(0),
  negative_(0),
  oddSum_(0),
  evenSum_(0),
  firstLastEqual_(false),
  totalAmount_(0),
  firstElem_(0)
{}

void nguyen::Functor::operator()(int elem)
{
  totalAmount_++;

  if (totalAmount_ == 1)
  {
    firstElem_ = elem;
    min_ = elem;
    max_ = elem;
  }

  if (elem > 0)
  {
    positive_++;
  }

  if (elem < 0)
  {
    negative_++;
  }

  if (elem < min_)
  {
    min_ = elem;
  }

  if (elem > max_)
  {
    max_ = elem;
  }

  if ((elem % 2) == 0)
  {
    evenSum_ += elem;
  }
  else
  {
    oddSum_ += elem;
  }

  firstLastEqual_ = (firstElem_ == elem);

}

void nguyen::Functor::output(std::ostream& stream)
{
  if (totalAmount_ == 0)
  {
    stream << "No Data" << std::endl;
    return;
  }

  mean_ = (oddSum_ + evenSum_) * 1.0 / totalAmount_;

  stream << "Max: " << max_ << std::endl;
  stream << "Min: " << min_ << std::endl;
  stream << "Mean: " << mean_ << std::endl;
  stream << "Positive: " << positive_ << std::endl;
  stream << "Negative: " << negative_ << std::endl;
  stream << "Odd Sum: " << oddSum_ << std::endl;
  stream << "Even Sum: " << evenSum_ << std::endl;
  stream << "First/Last Equal: ";
  if (firstLastEqual_)
  {
    stream << "yes" << std::endl;
  }
  else
  {
    stream << "no" << std::endl;
  }
}
