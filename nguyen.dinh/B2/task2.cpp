#include <iostream>
#include "tasks.hpp"
#include "queue-with-priority-interface.hpp"

const int MIN_VALUE = 1;
const int MAX_VALUE = 20;
const size_t MAX_LENGTH = 20;

void task2()
{
  std::list<int> list;

  int nextNum;
  while(std::cin >> nextNum)
  {
    if((nextNum < MIN_VALUE) || (nextNum > MAX_VALUE))
    {
      throw std::out_of_range("Value is out of range (1, 20)");
    }
    list.push_back(nextNum);
  }

  if(std::cin.fail() && !std::cin.eof())
  {
    throw std::invalid_argument("incorrect input");
  }

  if(list.size() > MAX_LENGTH)
  {
    throw std::invalid_argument("too many elements");
  }

  auto i = list.begin();
  auto j = std::prev(list.end());

  if(list.size() == MIN_VALUE)
  {
    std::cout << *i << std::endl;
  }
  else
  {
    while(i != j)
    {
      std::cout << *i << " " << *j << " ";
      i++;
      if(i == j)
      {
        break;
      }
      j--;
    }

    if(list.size() % 2 == 1)
    {
      std::cout << *i;
    }

    std::cout << std::endl;
  }
}
