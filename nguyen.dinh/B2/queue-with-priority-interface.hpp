#ifndef QUEUEWITHPRIORITYINTERFACE_HPP
#define QUEUEWITHPRIORITYINTERFACE_HPP

#include <stdexcept>
#include "queue-with-priority.hpp"

template <typename T>
detail::QueueWithPriority<T>::QueueWithPriority()
{
  T test_element;
  high_area = queue_.insert(queue_.end(), test_element);
  normal_area = queue_.insert(queue_.end(), test_element);
}

template <typename T>
void detail::QueueWithPriority<T>::put(const T &element, ElementPriority priority)
{
  switch(priority)
  {
    case ElementPriority::HIGH:
    {
      queue_.insert(high_area, element);
      break;
    }
    case ElementPriority::NORMAL:
    {
      queue_.insert(normal_area, element);
      break;
    }
    case ElementPriority::LOW:
    {
      queue_.insert(queue_.end(), element);
      break;
    }
  }
}

template <typename T>
T detail::QueueWithPriority<T>::get()
{
  if(empty())
  {
    throw std::invalid_argument("Queue is empty");
  }

  if(queue_.begin() != high_area)
  {
    T head_element = queue_.front();
    queue_.pop_front();
    return head_element;
  }

  else if(std::next(high_area) != normal_area)
  {
    T head_element = *std::next(high_area);
    queue_.erase(std::next(high_area));
    return head_element;
  }
  else
  {
    T head_element = *std::next(normal_area);
    queue_.erase(std::next(normal_area));
    return head_element;
  }
}

template <typename T>
void detail::QueueWithPriority<T>::accelerate()
{
  auto iter = std::next(normal_area);
  while(iter != queue_.end())
  {
    queue_.insert(high_area, *iter);
    ++iter;
    queue_.erase(std::prev(iter));
  }
}

template <typename T>
bool detail::QueueWithPriority<T>::empty() const
{
  if((queue_.begin() == high_area) && (std::next(high_area) == normal_area)
     && (std::next(normal_area) == queue_.end()))
  {
    return true;
  }
  else
  {
    return false;
  }
}

#endif
