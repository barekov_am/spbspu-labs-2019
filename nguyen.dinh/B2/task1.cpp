#include <iostream>
#include <string>
#include <sstream>
#include "tasks.hpp"
#include "queue-with-priority-interface.hpp"

void task1()
{
  detail::QueueWithPriority<std::string> queue;
  std::string line;
  
  while(std::getline(std::cin, line))
  {
    std::stringstream str_stream(line);
    std::string execute_command;
    str_stream >> execute_command;
    
    if(execute_command == "add")
    {
      std::string priority;
      str_stream >> priority;
      
      std::string data_string;
      str_stream.ignore();
      std::getline(str_stream >> std::ws, data_string);
      
      if(data_string.empty())
      {
        std::cout << "<INVALID COMMAND>" << std::endl;
      }
      else if(priority == "high")
      {
        queue.put(data_string, ElementPriority::HIGH);
      }
      else if(priority == "normal")
      {
        queue.put(data_string, ElementPriority::NORMAL);
      }
      else if(priority == "low")
      {
        queue.put(data_string, ElementPriority::LOW);
      }
      else
      {
        std::cout << "<INVALID COMMAND>" << std::endl;
      }
    }
    else if(execute_command == "accelerate" && str_stream.eof())
    {
      queue.accelerate();
    }
    else if(execute_command == "get")
    {
      if(queue.empty())
      {
        std::cout << "<EMPTY>" << std::endl;
      }
      else
      {
        std::cout << queue.get() << std::endl;
      }
    }
    else
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }
    
    if(std::cin.eof())
    {
      break;
    }
  }
}
