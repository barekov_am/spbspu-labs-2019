#include "base-types.hpp"

const double INACCURACY = 0.0000001;

bool nguyen::point_t::operator == (const point_t &right) const
{
  bool eq = true;
  if (!(x < right.x + INACCURACY && x > right.x - INACCURACY))
  {
    eq = false;
  }
  if (!(y < right.y + INACCURACY && y > right.y - INACCURACY))
  {
    eq = false;
  }
  return eq;
}

bool nguyen::rectangle_t::operator == (const rectangle_t &right) const
{
  bool eq = true;
  if (!(width < right.width + INACCURACY && width > right.width - INACCURACY))
  {
    eq = false;
  }
  if (!(height < right.height + INACCURACY && height > right.height - INACCURACY))
  {
    eq = false;
  }
  if (!(pos == right.pos) )
  {
    eq = false;
  }
  
  return eq;
}

double nguyen::distance(const nguyen::point_t &lf,  const nguyen::point_t &rt)
{
  return sqrt(pow(lf.x - rt.x, 2) + pow(lf.y - rt.y, 2));
}
