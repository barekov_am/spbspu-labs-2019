#include "triangle.hpp"
#include <iostream>
#include <cmath>
#include <algorithm>
#include <cassert>

nguyen::Triangle::Triangle(const point_t &corner_1, const point_t &corner_2, const point_t &corner_3):
  m_corner_1(corner_1),
  m_corner_2(corner_2),
  m_corner_3(corner_3),
  m_center({(m_corner_1.x + m_corner_2.x + m_corner_3.x) / 3, (m_corner_1.y + m_corner_2.y + m_corner_3.y) / 3})
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("Incorrect input");
  }
}

double  nguyen::Triangle::getArea() const
{
  return (std::abs((m_corner_2.x - m_corner_1.x) * (m_corner_3.y - m_corner_1.y)
          - (m_corner_3.x - m_corner_1.x) * (m_corner_2.y - m_corner_1.y))) / 2;
}

nguyen::rectangle_t nguyen::Triangle::getFrameRect() const
{
  const double maxX = std::max(std::max(m_corner_1.x, m_corner_2.x), m_corner_3.x);
  const double minX = std::min(std::min(m_corner_1.x, m_corner_2.x), m_corner_3.x);
  const double maxY = std::max(std::max(m_corner_1.y, m_corner_2.y), m_corner_3.y);
  const double minY = std::min(std::min(m_corner_1.y, m_corner_2.y), m_corner_3.y);
  const double width = maxX - minX;
  const double height = maxY - minY;
  const point_t position = {minX + width / 2, minY + height / 2};
  return {width, height, position};
}

void nguyen::Triangle::move(const point_t &point)
{
  const double dx = point.x - m_center.x;
  const double dy = point.y - m_center.y;
  move(dx, dy);
}

void nguyen::Triangle::move(const double dx, const double dy)
{
  m_corner_1.x += dx;
  m_corner_2.x += dx;
  m_corner_3.x += dx;
  m_corner_1.y += dy;
  m_corner_2.y += dy;
  m_corner_3.y += dy;
  m_center.x += dx;
  m_center.y += dy;
}

void nguyen::Triangle::printInfo() const
{
  std::cout << "Triangle" << std::endl
    << "Corner 1 (" << m_corner_1.x << "; " << m_corner_1.y << ")" << std::endl
    << "Corner 2 (" << m_corner_2.x << "; " << m_corner_2.y << ")" << std::endl
    << "Corner 3 (" << m_corner_3.x << "; " << m_corner_3.y << ")" << std::endl
    << "Center: (" << m_center.x << "; " << m_center.y << ")" << std::endl
    << "Area: " << getArea() << std::endl
    << "Rectangle Frame:" << std::endl
    << "Width = " << getFrameRect().width << " Height = " << getFrameRect().height << std::endl
    << "Center: (" << getFrameRect().pos.x << "; " << getFrameRect().pos.y << ")" << std::endl
    << std::endl;
}

void nguyen::Triangle::scale(double multiplyCoefficient)
{
  if (multiplyCoefficient <= 0)
  {
    throw std::invalid_argument("Multiply Coefficient is surely invalid");
  }
  m_corner_1.x *= multiplyCoefficient;
  m_corner_1.y *= multiplyCoefficient;
  m_corner_2.x *= multiplyCoefficient;
  m_corner_2.y *= multiplyCoefficient;
  m_corner_3.x *= multiplyCoefficient;
  m_corner_3.y *= multiplyCoefficient;
}

void nguyen::Triangle::rotate(double alpha)
{
  point_t center = getFrameRect().pos;
  double mod = sqrt(pow(m_corner_1.x - center.x,2) + pow(m_corner_1.y - center.y,2));
  double beta = alpha + atan((m_corner_1.y - center.y)/(m_corner_1.x - center.x));
  if ((m_corner_1.x - center.x) < 0)
  {
    beta = beta + M_PI;
  }
  m_corner_1.x = mod*cos(beta) + center.x;
  m_corner_1.y = mod*sin(beta) + center.y;
  mod = sqrt(pow(m_corner_2.x - center.x,2) + pow(m_corner_2.y - center.y,2));
  beta = alpha + atan((m_corner_2.y - center.y)/(m_corner_2.x - center.x));
  if ((m_corner_2.x - center.x) < 0)
  {
    beta = beta + M_PI;
  }
  m_corner_2.x = mod*cos(beta) + center.x;
  m_corner_2.y = mod*sin(beta) + center.y;
  mod = sqrt(pow(m_corner_3.x - center.x,2) + pow(m_corner_3.y - center.y,2));
  beta = alpha + atan((m_corner_3.y - center.y)/(m_corner_3.x - center.x));
  if ((m_corner_3.x - center.x) < 0)
  {
    beta = beta + M_PI;
  }
  m_corner_3.x = mod*cos(beta) + center.x;
  m_corner_3.y = mod*sin(beta) + center.y;
  
}

