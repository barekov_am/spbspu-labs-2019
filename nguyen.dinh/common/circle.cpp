#include "circle.hpp"
#include <iostream>
#include <cmath>

nguyen::Circle::Circle(const double radius, const point_t &center) :
  radius_(radius),
  pos_(center)
{
  if (radius <= 0)
  {
    throw std::invalid_argument("Radius is surely invalid");
  }
}

double nguyen::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

nguyen::rectangle_t nguyen::Circle::getFrameRect() const
{
  return {2.0 * radius_, 2.0 * radius_, pos_};
}

void nguyen::Circle::move(const point_t &point)
{
  pos_ = point;
}

void nguyen::Circle::move(const double dx, const double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void nguyen::Circle::printInfo() const
{
  std::cout << "Circle:" << std::endl
    << "\n  Radius: " << radius_ << std::endl
    << "\n  Position:" << std::endl
    << "\n    x: " << pos_.x << std::endl
    << "\n    y: " << pos_.y << std::endl
    << "\n  Area: " << this->getArea() << "\n\n" <<std::endl
    << std::endl;
}

void nguyen::Circle::scale(double multiplyCoefficient)
{
  if (multiplyCoefficient <= 0)
  {
    throw std::invalid_argument("Multiply Coefficient is surely invalid");
  }
  radius_ *= multiplyCoefficient;
}

void nguyen::Circle::rotate(double alpha)
{
  pos_.x = pos_.x * alpha / alpha;
}

