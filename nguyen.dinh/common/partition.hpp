#ifndef PARTITION_HPP
#define PARTITION_HPP

#include <memory>
#include "composite-shape.hpp"
#include "matrix.hpp"

namespace nguyen
{
  bool checkIntersect(nguyen::rectangle_t shape1, nguyen::rectangle_t shape2);
  Matrix division(std::unique_ptr<nguyen::Shape::shapePtr []> &shapes, size_t size);
  Matrix division(CompositeShape &compositeShape);
}

#endif
