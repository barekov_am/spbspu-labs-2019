#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP
#define _USE_MATH_DEFINES
#include <cmath>

namespace nguyen
{
  struct point_t
  {
    double x, y;
    
    bool operator == (const point_t &right) const;
  };
  
  struct rectangle_t
  {
    double width, height;
    point_t pos;
    
    bool operator == (const rectangle_t &right) const;
  };
    
  double distance(const point_t &lf, const point_t &rt);
}

#endif
