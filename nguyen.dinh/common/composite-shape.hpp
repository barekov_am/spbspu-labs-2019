#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP
#include <cstddef>
#include "shape.hpp"
#include <memory>
namespace nguyen
{
  class CompositeShape : public nguyen::Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &cs);
    CompositeShape(CompositeShape &&cs);
    CompositeShape(shapePtr shape);
    ~CompositeShape() = default;
    
    CompositeShape &operator = (const CompositeShape &cs);
    CompositeShape &operator = (CompositeShape &&cs);
    const shapePtr operator [](std::size_t index) const;
    
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(double dx, double dy) override;
    void printInfo() const override;
    size_t getCount() const;
    void scale(double multiplyCoefficient) override;
    void add(shapePtr shape);
    void remove(std::size_t i);
    void remove(shapePtr shape);
    void showAll() const;
    void rotate(double alpha) override;
    
  private:
    std::size_t count_;
    std::unique_ptr<shapePtr []> shapes_;
  };
}

#endif
