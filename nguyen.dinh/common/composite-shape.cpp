#include "composite-shape.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>
#include <stdexcept>

nguyen::CompositeShape::CompositeShape() :
  count_(0)
{
}

nguyen::CompositeShape::CompositeShape(const nguyen::CompositeShape &cs) :
  count_(cs.count_),
  shapes_(new shapePtr [cs.count_])
{
  for (std::size_t i = 0; i < count_; i++)
  {
    shapes_[i] = cs.shapes_[i];
  }
}

nguyen::CompositeShape::CompositeShape(nguyen::CompositeShape &&cs) :
  count_(cs.count_),
  shapes_(std::move(cs.shapes_))
{
  cs.count_ = 0;
  cs.shapes_ = nullptr;
}

nguyen::CompositeShape::CompositeShape(shapePtr shape) :
  count_(1),
  shapes_(new shapePtr [count_])
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Invalid! Shape is nullptr!");
  }
  shapes_[0] = shape;
}

nguyen::CompositeShape &nguyen::CompositeShape::operator = (const CompositeShape &cs)
{
  if (this != &cs)
  {
    this->count_ = cs.count_;
    std::unique_ptr<shapePtr []> temp(new shapePtr [cs.count_]);
    for (std::size_t i = 0; i < count_; i++)
    {
      temp[i] = cs.shapes_[i];
    }
    shapes_.swap(temp);
  }
  return *this;
}

nguyen::CompositeShape &nguyen::CompositeShape::operator = (CompositeShape &&cs)
{
  if (this != &cs)
  {
    count_ = cs.count_;
    shapes_ = std::move(cs.shapes_);
    cs.count_ = 0;
    cs.shapes_.reset();
  }
  return *this;
}

const nguyen::CompositeShape::shapePtr nguyen::CompositeShape::operator [](std::size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Error! Do not draw figure!");
  }
  return shapes_[index];
}

void nguyen::CompositeShape::add(shapePtr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("You didn't add shape in Constructor or new object is nullptr!!!");
  }
  count_++;
  std::unique_ptr<shapePtr []> temp(new shapePtr [count_]);
  for (std::size_t i = 0; i < count_ - 1; i++)
  {
    temp[i] = shapes_[i];
  }
  temp[count_ - 1] = shape;
  shapes_ = std::move(temp);
}

void nguyen::CompositeShape::remove(shapePtr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("You gave nullptr!");
  }
  std::size_t i = 0;
  while ((i < count_) && (shape != shapes_[i]))
  {
    i++;
  }
  if (i == count_)
  {
    throw std::invalid_argument("There is no such figure in the array");
  }
  remove(i);
}

void nguyen::CompositeShape::remove(std::size_t i)
{
  if (i > count_ - 1)
  {
    throw std::out_of_range("The index is greater than number of figures!");
  }
  for (std::size_t j = i; j < count_ - 1; j++)
  {
    shapes_[j] = shapes_[j + 1];
  }
  count_--;
}

void nguyen::CompositeShape::showAll() const
{
  std::cout << getArea() << "\n";
  std::cout << count_ << "\n";
}

size_t nguyen::CompositeShape::getCount() const
{
  return count_;
}

void nguyen::CompositeShape::move(double dx, double dy)
{
  for (std::size_t i = 0; i < count_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void nguyen::CompositeShape::move(const nguyen::point_t &point)
{
  nguyen::rectangle_t rect = getFrameRect();
  move(point.x - rect.pos.x, point.y - rect.pos.y);
}

double nguyen::CompositeShape::getArea() const
{
  double area = 0;
  for (std::size_t i = 0; i < count_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

void nguyen::CompositeShape::scale(double multiplyCoefficient)
{
  if (multiplyCoefficient <= 0)
  {
    throw std::invalid_argument("Invalid koeficent for CompositeShape");
  }
  
  point_t point = getFrameRect().pos;
  for (std::size_t i = 0; i < count_; i++)
  {
    nguyen::rectangle_t rect =  shapes_[i]->getFrameRect();
    point_t shift = { rect.pos.x - point.x, rect.pos.y - point.y };
    shapes_[i]->move(shift.x * (multiplyCoefficient - 1), shift.y * (multiplyCoefficient - 1));
    shapes_[i]->scale(multiplyCoefficient);
  }
}

nguyen::rectangle_t nguyen::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("For Composite Shape, no figures to calculate frame rect");
  }
  
  nguyen::rectangle_t rect = shapes_[0]->getFrameRect();
  double minX = rect.pos.x - rect.width / 2;
  double maxX = rect.pos.x + rect.width / 2;
  double minY = rect.pos.y - rect.height / 2;
  double maxY = rect.pos.y + rect.height / 2;
  
  for (std::size_t i = 1; i < count_; i++)
  {
    rect = shapes_[i]->getFrameRect();
    minX = std::min(minX, rect.pos.x - rect.width / 2);
    maxX = std::max(maxX, rect.pos.x + rect.width / 2);
    minY = std::min(minY, rect.pos.y - rect.height / 2);
    maxY = std::max(maxY, rect.pos.y + rect.height / 2);
  }
  
  const double width = maxX - minX;
  const double height = maxY - minY;
  
  return { width, height, { minX + width / 2, minY + height / 2 } };
}

void nguyen::CompositeShape::printInfo() const
{
  for (std::size_t i = 0; i < count_; i++)
  {
    std::cout << "Center={" << shapes_[i]->getFrameRect().pos.x << ", " << shapes_[i]->getFrameRect().pos.y << "}\n";
  }
}
    
void nguyen::CompositeShape::rotate(double alpha)
{
  point_t cShapeFrame_pos;
  cShapeFrame_pos.x = getFrameRect().pos.x;
  cShapeFrame_pos.y = getFrameRect().pos.y;
  for (size_t i = 0; i < count_; i++)
  {
    double range = 0;
    point_t movepoint{0, 0};
    rectangle_t sFrame = shapes_[i]->getFrameRect();
    range = distance(sFrame.pos, cShapeFrame_pos);
    double new_angle = asin((sFrame.pos.y - cShapeFrame_pos.y) / range) + alpha * M_PI/180;
    
    if ((sFrame.pos.x - cShapeFrame_pos.x) >= 0)
    {
      movepoint.x = cShapeFrame_pos.x + range * (cos(new_angle));
      movepoint.y = cShapeFrame_pos.y + range * (sin(new_angle));
    }
    if (((sFrame.pos.x - cShapeFrame_pos.x) < 0) && ((sFrame.pos.y - cShapeFrame_pos.y) > 0))
    {
      movepoint.x = cShapeFrame_pos.x + range * (cos(new_angle+M_PI / 2));
      movepoint.y = cShapeFrame_pos.y + range * (sin(new_angle+M_PI / 2));
    }
    if (((sFrame.pos.x - cShapeFrame_pos.x) < 0) && ((sFrame.pos.y - cShapeFrame_pos.y) <= 0))
    {
      movepoint.x = cShapeFrame_pos.x + range * (cos(new_angle-M_PI / 2));
      movepoint.y = cShapeFrame_pos.y + range * (sin(new_angle-M_PI / 2));
    }
    shapes_[i]->move(movepoint);
    shapes_[i]->rotate(alpha);
  }
}
