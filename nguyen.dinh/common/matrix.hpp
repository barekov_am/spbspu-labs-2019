#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"
#include <memory>

namespace nguyen
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &cs);
    Matrix(Matrix &&cs);
    ~Matrix() = default;
    
    Matrix &operator = (const Matrix &cs);
    Matrix &operator = (Matrix &&cs);
    std::unique_ptr<Shape::shapePtr []>  operator [](size_t index) const;
    bool operator == (const Matrix &shapesMatrix) const;
    bool operator != (const Matrix &shapesMatrix) const;
    
    void add(Shape::shapePtr shape, size_t line, size_t column);
    void showAll() const;
    size_t getLines() const;
    size_t getColumns() const;
    size_t getLineSize(size_t line) const;
    
  private:
    size_t lines_;
    size_t columns_;
    std::unique_ptr<Shape::shapePtr []> shapes_;
  };
};

#endif
