#include "rectangle.hpp"
#include <iostream>
#include <cassert>

nguyen::Rectangle::Rectangle(double width, double height, const point_t &center) :
  width_(width),
  height_(height),
  pos_(center),
  m_angle(0)
{
  if ((width <= 0) || (height <= 0))
  {
    throw std::invalid_argument("Arguments width and height are surely invalid");
  }
  
}

double nguyen::Rectangle::getArea() const
{
  return width_ * height_;
}

nguyen::rectangle_t nguyen::Rectangle::getFrameRect() const
{
  return {width_, height_, pos_};
}

void nguyen::Rectangle::move(const point_t &point)
{
  pos_ = point;
}

void nguyen::Rectangle::move(const double dx, const double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void nguyen::Rectangle::printInfo() const
{
  std::cout << "Rectangle:" << std::endl
    << "\n  Width: " << width_ << std::endl
    << "\n  Height: " << height_ << std::endl
    << "\n  Position:" << std::endl
    << "\n    x: " << pos_.x << std::endl
    << "\n    y: " << pos_.y << std::endl
    << "\n  Area: " << this->getArea() << "\n\n" << std::endl
    << std::endl;
}

void nguyen::Rectangle::scale(double multiplyCoefficient)
{
  if (multiplyCoefficient <= 0)
  {
    throw std::invalid_argument("Multiply Coefficient is surely invalid");
  }
  width_ *= multiplyCoefficient;
  height_ *= multiplyCoefficient;
}

void nguyen::Rectangle::rotate(double alpha)
{
  m_angle += alpha;
  const double fullCircle = 360;
  m_angle = (m_angle < 0.0) ? (fullCircle + fmod(m_angle, fullCircle)) : fmod(m_angle, fullCircle);
}

