#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

const double INACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testForRectangle)

BOOST_AUTO_TEST_CASE(widthAndHightAfterMoveToPoint)
{
  nguyen::Rectangle rectangle(5, 5, { 3, 4 });
  rectangle.move({ 2, 2 });
  BOOST_CHECK_CLOSE(5, rectangle.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(5, rectangle.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(widthAndHightAfterMove)
{
  nguyen::Rectangle rectangle(7, 7, { 3, 5 });
  rectangle.move(2, 2);
  BOOST_CHECK_CLOSE(7, rectangle.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(7, rectangle.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterMoveToPoint)
{
  nguyen::Rectangle rectangle(7, 7, { 3, 5 });
  double area = rectangle.getArea();
  rectangle.move({ 2, 2 });
  BOOST_CHECK_CLOSE(area, rectangle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterMove)
{
  nguyen::Rectangle rectangle(7, 7, { 3, 5 });
  double area = rectangle.getArea();
  rectangle.move(3, 3);
  BOOST_CHECK_CLOSE(area, rectangle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterScaleOfRectangle)
{
  nguyen::Rectangle rectangle(4, 3, { 1, 5 });
  const double areaBeforeScale = rectangle.getArea();
  const double testScale = 5;
  rectangle.scale(testScale);
  BOOST_CHECK_CLOSE(areaBeforeScale * testScale * testScale, rectangle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidArgumentsInRectangle)
{
  BOOST_CHECK_THROW(nguyen::Rectangle rectangle(5, -9, { 1, 6 }), std::invalid_argument);
  BOOST_CHECK_THROW(nguyen::Rectangle rectangle(-9, 6, { 1, 6 }), std::invalid_argument);
  nguyen::Rectangle rectangle(3, 4, { 3, 4 });
  BOOST_CHECK_THROW(rectangle.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(checkRotate)
{
  nguyen::Rectangle rectangle(6, 7, { 4, 5 });
  double area = rectangle.getArea();
  rectangle.rotate(45);
  BOOST_CHECK_CLOSE(area, rectangle.getArea(), INACCURACY);
}


BOOST_AUTO_TEST_SUITE_END()
