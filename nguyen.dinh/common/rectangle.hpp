#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace nguyen
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const double width, const double height, const point_t &center);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(const double dx, const double dy) override;
    void printInfo() const override;
    void scale(double multiplyCoefficient) override;
    void rotate(double alpha) override;
    
  private:
    double width_;
    double height_;
    point_t pos_;
    double m_angle;
  };
}

#endif
