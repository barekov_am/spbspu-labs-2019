#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testForMatrix)

BOOST_AUTO_TEST_CASE(copyAndMove)
{
    nguyen::Rectangle rectangle(5, 7, { 3, 6 });
    nguyen::CompositeShape compositeShape(std::make_shared<nguyen::Rectangle>(rectangle));
    nguyen::Matrix matrix = nguyen::division(compositeShape);
    
    nguyen::Matrix matrix3 = matrix;
    
    BOOST_CHECK_EQUAL(matrix3.getLines(), 1);
    BOOST_CHECK_EQUAL(matrix3.getColumns(),1);
    BOOST_CHECK_NO_THROW(nguyen::Matrix matrix1(matrix));
    BOOST_CHECK_NO_THROW(nguyen::Matrix matrix2(std::move(matrix)));
    
    nguyen::Matrix matrix4;
    nguyen::Matrix matrix5;
    
    BOOST_CHECK_NO_THROW(matrix4 = matrix);
    BOOST_CHECK_NO_THROW(matrix5 = std::move(matrix));
}

BOOST_AUTO_TEST_CASE(testForThrow)
{
    nguyen::Rectangle rectangle(3, 3, { 2, 5.5 });
    nguyen::Rectangle rectangle1(2, 5, { 4.5, 2 });
    nguyen::Circle circle(2, { 6, 4 });
    nguyen::Triangle triangle({3, 2}, {2, 0}, {4, 0});
    nguyen::Triangle triangle1({7, 4}, {10, 3}, {10, 5});
    nguyen::CompositeShape compositeShape(std::make_shared<nguyen::Rectangle>(rectangle));
    compositeShape.add(std::make_shared<nguyen::Rectangle>(rectangle1));
    compositeShape.add(std::make_shared<nguyen::Circle>(circle));
    compositeShape.add(std::make_shared<nguyen::Triangle>(triangle));
    compositeShape.add(std::make_shared<nguyen::Triangle>(triangle1));
    
    nguyen::Matrix matrix = nguyen::division(compositeShape);
    BOOST_CHECK_THROW(matrix[10][10], std::out_of_range);
    BOOST_CHECK_NO_THROW(matrix[0][1]);
}

BOOST_AUTO_TEST_SUITE_END()
