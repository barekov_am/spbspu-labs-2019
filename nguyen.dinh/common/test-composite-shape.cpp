#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

const double INACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testForCompositeShape)

BOOST_AUTO_TEST_CASE(widthAndHeightAfterMove)
{
  nguyen::Circle circle(5, { 3, 4 });
  nguyen::Rectangle rectangle(2, 6, { 5, 6 });
  nguyen::Triangle triangle({4, 6}, {5, 3}, {4, 9});
  nguyen::CompositeShape compositeShape(std::make_shared<nguyen::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<nguyen::Circle>(circle));
  compositeShape.add(std::make_shared<nguyen::Triangle>(triangle));
  nguyen::rectangle_t frameBeforeMoving = compositeShape.getFrameRect();
  double area = compositeShape.getArea();
  compositeShape.move({4, 5});
  
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, compositeShape.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, compositeShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(area, compositeShape.getArea(), INACCURACY);
  
  compositeShape.move(4, 5);
  
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, compositeShape.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, compositeShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(area, compositeShape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterScale)
{
  nguyen::Circle circle(5, { 3, 4 });
  nguyen::Rectangle rectangle(6, 8, { 4, 5 });
  
  nguyen::CompositeShape compositeShape(std::make_shared<nguyen::Circle>(circle));
  double areaBeforeAdd = compositeShape.getArea();

  nguyen::Shape::shapePtr shape = std::make_shared< nguyen::Rectangle>(rectangle);
  
  compositeShape.add(shape);
  BOOST_CHECK_CLOSE(areaBeforeAdd + rectangle.getArea(), compositeShape.getArea(), INACCURACY);
  
  compositeShape.remove(shape);
  BOOST_CHECK_CLOSE(areaBeforeAdd, compositeShape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(outOfRangeException)
{
  nguyen::Rectangle rectangle(7, 7, { 5, 6 });
  nguyen::CompositeShape compositeShape(std::make_shared<nguyen::Rectangle>(rectangle));
  BOOST_CHECK_THROW(compositeShape[100], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(areaAfterScaling)
{
  nguyen::Circle circle(5, { 3, 4 });
  nguyen::Rectangle rectangle(2, 6, { 5, 6 });
  nguyen::Triangle triangle({4, 6}, {5, 3}, {4, 9});
  nguyen::CompositeShape compositeShape(std::make_shared<nguyen::Circle>(circle));
  compositeShape.add(std::make_shared<nguyen::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<nguyen::Triangle>(triangle));
  
  double area = compositeShape.getArea();
  compositeShape.scale(2);
  
  BOOST_CHECK_CLOSE(area * 4, compositeShape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkCorrectGetFrameRect)
{
  nguyen::Circle circle(3, { 6, 5 });
  nguyen::Rectangle rectangle(6, 6, { 6, 5 });
  nguyen::CompositeShape compositeShape(std::make_shared<nguyen::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<nguyen::Circle>(circle));
  nguyen::rectangle_t frame = rectangle.getFrameRect();

  BOOST_CHECK_CLOSE(frame.height, compositeShape.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(frame.width, compositeShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(frame.pos.x, compositeShape.getFrameRect().pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(frame.pos.y, compositeShape.getFrameRect().pos.y, INACCURACY);
}

BOOST_AUTO_TEST_CASE(exceptionThrow)
{
  BOOST_CHECK_THROW(nguyen::CompositeShape(nullptr), std::invalid_argument);
  
  nguyen::Circle circle(9, { 2, 2 });
  nguyen::Rectangle rectangle(3, 5, { 4, 2 });
  nguyen::Triangle triangle({4, 3}, {5, 6}, {3, 8});
  nguyen::CompositeShape compositeShape(std::make_shared<nguyen::Circle>(circle));
  compositeShape.add(std::make_shared<nguyen::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<nguyen::Triangle>(triangle));
  nguyen::CompositeShape compositeShape1;
  
  BOOST_CHECK_THROW(compositeShape1.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(compositeShape.scale(-1), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.remove(10), std::out_of_range);
  BOOST_CHECK_THROW(compositeShape.remove(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.remove(-1), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(copyAndMove)
{
  nguyen::Rectangle rect1(8, 4, { 4, 1 });
  nguyen::CompositeShape compositeShape1(std::make_shared<nguyen::Rectangle>(rect1));
  nguyen::CompositeShape compositeShape4;
  
  BOOST_CHECK_NO_THROW(nguyen::CompositeShape compositeShape2(compositeShape1));
  
  nguyen::CompositeShape compositeShape2;
  
  BOOST_CHECK_NO_THROW(compositeShape2 = compositeShape1);
  BOOST_CHECK_NO_THROW(compositeShape2 = nguyen::CompositeShape(std::make_shared<nguyen::Rectangle>(rect1)));
  
  BOOST_CHECK_NO_THROW(nguyen::CompositeShape compositeShape3(std::move(compositeShape2)));
  BOOST_CHECK_NO_THROW(compositeShape4 = std::move(compositeShape1));
}

BOOST_AUTO_TEST_SUITE_END()
