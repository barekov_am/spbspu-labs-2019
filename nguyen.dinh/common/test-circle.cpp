#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double INACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testForCircle)

BOOST_AUTO_TEST_CASE(circleRotate)
{
  nguyen::Circle testCircle(7, { 7, 7 });
  const nguyen::rectangle_t tmpFrameBefore = testCircle.getFrameRect();
  const double tmpAreaBefore = testCircle.getArea();
  
  testCircle.rotate(80);
  nguyen::rectangle_t tmpFrameAfter = testCircle.getFrameRect();
  double tmpAreaAfter = testCircle.getArea();
  
  BOOST_CHECK_CLOSE(tmpFrameBefore.width, tmpFrameAfter.width, INACCURACY);
  BOOST_CHECK_CLOSE(tmpFrameBefore.height, tmpFrameAfter.height, INACCURACY);
  BOOST_CHECK_CLOSE(tmpFrameBefore.pos.x, tmpFrameAfter.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(tmpFrameBefore.pos.y, tmpFrameAfter.pos.y, INACCURACY);
  BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, INACCURACY);
  
  testCircle.rotate(-173);
  tmpFrameAfter = testCircle.getFrameRect();
  tmpAreaAfter = testCircle.getArea();
  
  BOOST_CHECK_CLOSE(tmpFrameBefore.width, tmpFrameAfter.width, INACCURACY);
  BOOST_CHECK_CLOSE(tmpFrameBefore.height, tmpFrameAfter.height, INACCURACY);
  BOOST_CHECK_CLOSE(tmpFrameBefore.pos.x, tmpFrameAfter.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(tmpFrameBefore.pos.y, tmpFrameAfter.pos.y, INACCURACY);
  BOOST_CHECK_CLOSE(tmpAreaBefore, tmpAreaAfter, INACCURACY);
}

BOOST_AUTO_TEST_CASE(radiusImmutabilityAfterMoveToPoint)
{
  nguyen::Circle circle(5, { 3, 3 });
  circle.move({ 0, 0 });
  BOOST_CHECK_CLOSE(5, circle.getFrameRect().width / 2, INACCURACY);
}

BOOST_AUTO_TEST_CASE(radiusImmutabilityAfterMove)
{
  nguyen::Circle circle(5, { 3, 3 });
  circle.move(2, 2);
  BOOST_CHECK_CLOSE(5, circle.getFrameRect().width / 2, INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterMoveToPoint)
{
  nguyen::Circle circle(7, { 3, 5 });
  double area = circle.getArea();
  circle.move({ 2, 2 });
  BOOST_CHECK_CLOSE(area, circle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterMove)
{
  nguyen::Circle circle(7, { 3, 5 });
  double area = circle.getArea();
  circle.move(4, 2);
  BOOST_CHECK_CLOSE(area, circle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterScaleOfCircle)
{
  nguyen::Circle circle(3, { 5, 4 });
  const double areaBeforeScale = circle.getArea();
  const double testScale = 5;
  circle.scale(testScale);
  BOOST_CHECK_CLOSE(areaBeforeScale * testScale * testScale, circle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidArgumentsInCircle)
{
  BOOST_CHECK_THROW(nguyen::Circle circle(-9, { 6, 5 }), std::invalid_argument);
  nguyen::Circle circle(4, { 4, 3 });
  BOOST_CHECK_THROW(circle.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
