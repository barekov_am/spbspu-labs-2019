#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(partitionMetodsTests)

BOOST_AUTO_TEST_CASE(partitionTest)
{
    nguyen::Rectangle rectangle(1, 2, { 2, 4.5 });
    nguyen::Rectangle rectangle1(2, 5, { 3.5, 2 });
    nguyen::Circle circle(2, { 6, 4 });
    
    nguyen::CompositeShape compositeShape(std::make_shared<nguyen::Rectangle>(rectangle));
    compositeShape.add(std::make_shared<nguyen::Rectangle>(rectangle1));
    compositeShape.add(std::make_shared<nguyen::Circle>(circle));
    
    nguyen::Matrix matrix = nguyen::division(compositeShape);
    
    BOOST_CHECK_EQUAL(matrix.getLines(), 2);
    BOOST_CHECK_EQUAL(matrix.getColumns(), 2);
}

BOOST_AUTO_TEST_CASE(intersectionTest)
{
    nguyen::Rectangle rectangle(1, 2, { 2, 4.5 });
    nguyen::Rectangle rectangle1(2, 5, { 3.5, 2 });
    nguyen::Circle circle(2, { 6, 4 });
    
    BOOST_CHECK(!nguyen::checkIntersect(rectangle.getFrameRect(), rectangle1.getFrameRect()));
    BOOST_CHECK(!nguyen::checkIntersect(rectangle.getFrameRect(), circle.getFrameRect()));
    BOOST_CHECK(nguyen::checkIntersect(rectangle1.getFrameRect(), circle.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
