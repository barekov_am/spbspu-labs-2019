#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"

const double INACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testForTriangle)

BOOST_AUTO_TEST_CASE(widthAndHeightAfterMoveToPoint)
{
  nguyen::Triangle triangle({ 4, 5 }, { 6, 3 }, { 2, 5 });
  nguyen::rectangle_t frame = triangle.getFrameRect();
  triangle.move({ 2, 2 });
  BOOST_CHECK_CLOSE(frame.width, triangle.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(frame.height, triangle.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(widthAndHeightAfterMove)
{
  nguyen::Triangle triangle({ 4, 5 }, { 6, 3 }, { 2, 5 });
  nguyen::rectangle_t frame = triangle.getFrameRect();
  triangle.move(2, 2);
  BOOST_CHECK_CLOSE(frame.width, triangle.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(frame.height, triangle.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterMoveToPoint)
{
  nguyen::Triangle triangle({ 3, 4 }, { 5, 2 }, { 1, 5 });
  double area = triangle.getArea();
  triangle.move({ 2, 2 });
  BOOST_CHECK_CLOSE(area, triangle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterMove)
{
  nguyen::Triangle triangle({ 3, 4 }, { 5, 2 }, { 1, 5 });
  double area = triangle.getArea();
  triangle.move(3, 3);
  BOOST_CHECK_CLOSE(area, triangle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(reaAfterScaleOfTriangle)
{
  nguyen::Triangle triangle({ 3, 4 }, { 6, 7 }, { 1, 5 });
  const double areaBeforScaling = triangle.getArea();
  const double testScale = 4;
  triangle.scale(testScale);
  BOOST_CHECK_CLOSE(areaBeforScaling * testScale * testScale, triangle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidArgumentsInTriangle)
{
  BOOST_CHECK_THROW(nguyen::Triangle triangle({ 4, 5 }, { 4, 6 }, { 4, 7 }), std::invalid_argument);
  BOOST_CHECK_THROW(nguyen::Triangle triangle({ 5, 6 }, { 1, 6 }, { 4, 6 }), std::invalid_argument);
  nguyen::Triangle triangle({ 4, 5 }, { 1, 4 }, { 6, 7 });
  BOOST_CHECK_THROW(triangle.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(checkRotate)
{
  nguyen::Triangle triangle({4, 5}, {6, 7}, {9, 8});
  double area = triangle.getArea();
  triangle.rotate(45);
  BOOST_CHECK_CLOSE(area, triangle.getArea(), INACCURACY);
}


BOOST_AUTO_TEST_SUITE_END()
