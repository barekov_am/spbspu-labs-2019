#include "otherfuncs.hpp"
#include <cstring>

void konev::fillRandom(double* array, int size)
{
  for (int i = 0; i < size; i++)
  {
    array[i] = std::rand() * 2.0 / RAND_MAX - 1.0;
  }
  return;
}

bool konev::checkCorrect(char *order)
{
  return ((std::strcmp(order, "ascending") == 0) || (std::strcmp(order, "descending") == 0));
}



