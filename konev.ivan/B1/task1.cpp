#include <forward_list>

#include "otherfuncs.hpp"
#include "sorting.hpp"
#include "tasks.hpp"

void konev::taskOne(char *sortDirection)
{
  int element;
  std::vector<int> primaryVect;
  while (std::cin >> element)
  {
    primaryVect.push_back(element);
  }

  if (!std::cin.eof())
  {
    throw(std::invalid_argument("Incorrect input!"));
  }

  if (!(konev::checkCorrect(sortDirection)))
  {
    throw std::invalid_argument("Incorrect sorting direction!");
  }

  if (primaryVect.empty())
  {
    return;
  }

  std::vector<int> sampleVect(primaryVect.begin(), primaryVect.end());
  konev::sort(sampleVect, AccessByOperator<std::vector<int>>(), sortDirection);

  konev::outputToStd(sampleVect, AccessByOperator<std::vector<int>>(), true);
  std::cout << std::endl;

  sampleVect = primaryVect;
  konev::sort(sampleVect, AccessByAt<std::vector<int>>(), sortDirection);

  konev::outputToStd(sampleVect, AccessByAt<std::vector<int>>(), true);
  std::cout << std::endl;

  std::forward_list<int> sampleList(primaryVect.begin(), primaryVect.end());
  konev::sort(sampleList, AccessByIterator<std::forward_list<int>>(), sortDirection);

  konev::outputToStd(sampleList, AccessByIterator<std::forward_list<int>>(), true);
  std::cout << std::endl;
}
