#include "otherfuncs.hpp"
#include "sorting.hpp"
#include "tasks.hpp"

void konev::taskFour(char *sortDirection, char *arraySize)
{
  int vectorSize = std::stoi(arraySize);

  std::vector<double> doubleVector(vectorSize);
  konev::fillRandom(&doubleVector[0], vectorSize);

  konev::outputToStd(doubleVector, AccessByIterator<std::vector<double>>(), true);
  std::cout << std::endl;

  konev::sort(doubleVector, AccessByOperator<std::vector<double>>(), sortDirection);

  konev::outputToStd(doubleVector, AccessByIterator<std::vector<double>>(), true);
}
