#ifndef TASKS_HPP
#define TASKS_HPP

namespace konev
{
  void taskOne(char * sortDirection);
  void taskTwo(char * fileName);
  void taskThree();
  void taskFour(char * sortDirection, char * arraySize);
}

#endif

