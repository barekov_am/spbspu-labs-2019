#include "tasks.hpp"
#include "otherfuncs.hpp"
#include "sorting.hpp"
#include <memory>
#include <vector>
#include <fstream>

const std::size_t initialSize = 1024;

void konev::taskTwo(char* fileName)
{
  std::ifstream fileIn(fileName);

  if (!fileIn)
  {
    throw std::invalid_argument("File does not exist.");
  }

  std::size_t size = initialSize;

  std::unique_ptr<char[], decltype(&free)> array(static_cast<char*>(malloc(size)), &free);

  if (!array)
  {
    throw std::runtime_error("Could not allocate memory");
  }

  std::size_t index = 0;
  while (fileIn)
  {
    fileIn.read(&array[index], initialSize);
    index += fileIn.gcount();
    if (fileIn.gcount() == initialSize)
    {
      size += initialSize;
      std::unique_ptr<char[], decltype(&free)> newArray(static_cast<char*>(realloc(array.get(), size)), &free);
      if (!newArray)
      {
        throw std::runtime_error("Could not allocate memory for new chars");
      }
      array.release();
      std::swap(array, newArray);
    }
    if (!fileIn.eof() && fileIn.fail())
    {
      throw std::runtime_error("Chars reading failed");
    }
  }

  std::vector<char> textVector(&array[0], &array[index]);

  konev::outputToStd(textVector, AccessByIterator<std::vector<char>>(), false);

  fileIn.close();
}
