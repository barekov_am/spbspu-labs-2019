#ifndef SORTING_HPP
#define SORTING_HPP

#include "otherfuncs.hpp"
#include <vector>
#include <iostream>
#include <functional>
#include <cstring>

namespace konev
{
  template<typename T>
  struct AccessByOperator
  {
    typename T::reference operator()(T& container, size_t i)
    {
      return container[i];
    }

    size_t getBegin(const T&) const
    {
      return 0;
    }

    size_t getEnd(const T& container) const
    {
      return container.size();
    }
  };

  template<typename T>
  struct AccessByAt
  {
    typename T::reference operator()(T& container, size_t i)
    {
      return container.at(i);
    }

    size_t getBegin(const T&) const
    {
      return 0;
    }

    size_t getEnd(const T& container) const
    {
      return container.size();
    }
  };

  template<typename T>
  struct AccessByIterator
  {
    typename T::reference operator()(T&, typename T::iterator iter)
    {
      return *iter;
    }

    static typename T::iterator getBegin(T& container)
    {
      return container.begin();
    }

    static typename T::iterator getEnd(T& container)
    {
      return container.end();
    }
  };

  template<typename Container, typename Access>
  void sort(Container& container, Access access, char* compare)
  {
    if (!(konev::checkCorrect(compare)))
    {
      throw std::invalid_argument("Incorrect sorting direction!");
    }

    for(auto i = access.getBegin(container); i != access.getEnd(container); i++)
    {
      for(auto j = i; j != access.getEnd(container); j++)
      {
        if (std::strcmp(compare, "ascending") == 0)
        {
          if (access(container, i) > access(container, j))
          {
            std::swap(access(container, i), access(container, j));
          }
        }
        else if (std::strcmp(compare, "descending") == 0)
        {
          if (access(container, i) < access(container, j))
          {
            std::swap(access(container, i), access(container, j));
          }
        }
      }
    }
  }

}


#endif

