#include "otherfuncs.hpp"
#include "sorting.hpp"
#include "tasks.hpp"

void konev::taskThree()
{
  std::vector<int> intVector;

  bool missingZero = true;
  int elem;
  while (std::cin && !(std::cin >> elem).eof())
  {
    if (std::cin.fail() || std::cin.bad())
    {
      throw(std::invalid_argument("Incorrect input!"));
    }
    if (elem == 0)
    {
      missingZero = false;
      break;
    }
    else
    {
    intVector.push_back(elem);
    }
  }

  if (missingZero)
  {
    if (intVector.empty())
    {
      return;
    }
    else
    {
    throw(std::invalid_argument("Missing zero!"));
    }
  }

  if (intVector.empty())
  {
    return;
  }

  if (intVector.back() == 1)
  {
    std::vector<int>::iterator i = intVector.begin();
    while (i != intVector.end())
    {
      if (*i % 2 == 0)
      {
        i = intVector.erase(i);
      }
      else
      {
        i++;
      }
    }
    intVector.shrink_to_fit();
  }

  if (intVector.back() == 2)
  {
    std::vector<int>::iterator i = intVector.begin();
    while (i != intVector.end())
    {
      if (*i % 3 == 0)
      {
        i = intVector.insert(i + 1, 3, 1) + 2;
      }
      else
      {
        i++;
      }
    }
  }

  konev::outputToStd(intVector, AccessByIterator<std::vector<int>>(), true);

}
