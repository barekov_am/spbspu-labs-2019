#include "tasks.hpp"
#include <iostream>

int main (int argc, char * argv[])
{
  try
  {
    if (argc < 2)
    {
      throw(std::invalid_argument("Too few arguments!"));
    }
    else
    {
      switch (*argv[1])
      {
      case '1':
        if (argc != 3)
        {
          throw(std::invalid_argument("Wrong number of arguments!"));
        }
        konev::taskOne(argv[2]);
        break;

      case '2':
        if (argc != 3)
        {
          throw(std::invalid_argument("Wrong number of parameters!"));
        }
        konev::taskTwo(argv[2]);
        break;

      case '3':
        if (argc != 2)
        {
          throw(std::invalid_argument("Wrong number of parameters!"));
        }
        konev::taskThree();
        break;

      case '4':
        if (argc != 4)
        {
          throw(std::invalid_argument("Wrong number of arguments!"));
        }
        konev::taskFour(argv[2], argv[3]);
        break;

      default:
        throw(std::invalid_argument("Unknown task number!"));
      }
    }
  }
  catch(std::exception& err)
  {
    std::cerr << err.what();
    return 1;
  }

  return 0;
}

