#ifndef OTHERFUNCS_HPP
#define OTHERFUNCS_HPP

#include <random>
#include <iostream>


namespace konev
{
  void fillRandom(double* array, int size);

  bool checkCorrect(char *order);

  template <typename Container, typename Access>
  void outputToStd(Container& container, Access access, bool addSpaces)
  {
    if (addSpaces)
    {
      for (auto i = access.getBegin(container); i != access.getEnd(container); i++)
      {
        std::cout << access(container, i) << " ";
      }
    }
    else
    {
      for (auto i = access.getBegin(container); i != access.getEnd(container); i++)
      {
        std::cout << access(container, i);
      }
    }
  }

}

#endif

