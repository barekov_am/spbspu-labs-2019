#include "tasks.hpp"
#include <list>
#include <iostream>
#include <stdexcept>

const int MAX_AMOUNT = 20;
const int MIN_AMOUNT = 1;

void konev::taskTwo(){
  std::list<int> list;
  int elem;

  while ((std::cin >> elem) && (!std::cin.eof())){
    if((elem < MIN_AMOUNT) || (elem > MAX_AMOUNT)){
      throw(std::invalid_argument("Value does not match allowed range!"));
    }
    list.push_back(elem);
  }

  if(!std::cin.eof()){
    throw(std::invalid_argument("Wrong input!"));
  }

  if(list.size() > MAX_AMOUNT){
    throw(std::invalid_argument("Too many numbers!"));
  }

  if(list.empty()){
    return;
  }
  else
  {
    std::list<int>::iterator b_iter = list.begin();
    std::list<int>::iterator e_iter = list.end();
    e_iter--;

    if(list.size() == MIN_AMOUNT){
      std::cout << *b_iter << std::endl;
    }
    else
    {
      do{
        std::cout << *b_iter << " " << *e_iter;
        b_iter++;
        if(b_iter == e_iter){
          break;
        }
        else
        {
          std::cout << " ";
        }
        e_iter--;
      }
      while(b_iter != e_iter);
      if(list.size() % 2 == 1){
        std::cout << *b_iter;
      }
      std::cout << std::endl;

    }

  }
}


