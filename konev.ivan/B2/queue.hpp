#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <list>
#include <stdexcept>

namespace konev {

  typedef enum{
    LOW,
    NORMAL,
    HIGH
  }
  ElementPriority;


  template<typename Element_t>
  class QueueWithPriority{
  public:
    QueueWithPriority() = default;
    ~QueueWithPriority() = default;

    void PutElementToQueue(const Element_t & element, konev::ElementPriority priority);

    Element_t GetElementFromQueue();

    void Accelerate();

    bool isEmpty();

  private:
    std::list<Element_t> data_low_;
    std::list<Element_t> data_high_;
    std::list<Element_t> data_norm_;
 };

}


template<typename Element_t>
void konev::QueueWithPriority<Element_t>::PutElementToQueue(const Element_t &element, konev::ElementPriority priority){
  switch (priority) {
  case konev::HIGH:{
    std::list<Element_t> & data_h = data_high_;
    data_h.push_back(element);
    break;
  }

  case konev::NORMAL:{
    std::list<Element_t> & data_n = data_norm_;
    data_n.push_back(element);
    break;
  }

  case konev::LOW:{
    std::list<Element_t> & data_l = data_low_;
    data_l.push_back(element);
    break;
  }

  default:{
    throw(std::invalid_argument("Unknown Priority type! Unable to add!"));
  }
  }
}


template<typename Element_t>
Element_t konev::QueueWithPriority<Element_t>::GetElementFromQueue(){
  if (!data_high_.empty()){
    Element_t front = data_high_.front();
    data_high_.pop_front();
    return front;
  }
  else if(!data_norm_.empty()){
    Element_t front = data_norm_.front();
    data_norm_.pop_front();
    return front;
  }
  else if(!data_low_.empty()){
    Element_t front = data_low_.front();
    data_low_.pop_front();
    return front;
  }
  else
  {
    return "<EMPTY>";
  }
}

template <typename Element_t>
void konev::QueueWithPriority<Element_t>::Accelerate(){
  std::move(data_low_.begin(), data_low_.end(), std::back_inserter(data_high_));
  data_low_.clear();
}

#endif // QUEUE_HPP

