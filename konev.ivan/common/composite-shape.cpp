#include <stdexcept>
#include <cmath>
#include <algorithm>

#include "composite-shape.hpp"

konev::CompositeShape::CompositeShape():
  count_(0),
  shapeList_(nullptr)
{
}

konev::CompositeShape::CompositeShape(const CompositeShape &copyShape):
  count_(copyShape.count_),
  shapeList_(std::make_unique<std::shared_ptr<konev::Shape>[]>(copyShape.count_))
{
  for(size_t i = 0; i < count_; i++)
  {
    shapeList_[i] = copyShape.shapeList_[i];
  }
}

konev::CompositeShape::CompositeShape(CompositeShape &&displaceShape):
  count_(displaceShape.count_),
  shapeList_(std::move(displaceShape.shapeList_))
{
  displaceShape.count_ = 0;
}

  konev::CompositeShape::CompositeShape(const konev::Shape::ptr_shape &firstShape):
  count_(1),
  shapeList_(std::make_unique<std::shared_ptr<konev::Shape>[]>(1))
{
  if (firstShape == nullptr)
  {
    throw std::invalid_argument("You can't create composite shape with nullptr");
  }
  shapeList_[0] = firstShape;
}

konev::CompositeShape &konev::CompositeShape::operator =(const CompositeShape &composShape)
{
  if (this != &composShape)
  {
    count_ = composShape.count_;
    shapeList_ = std::make_unique<konev::Shape::ptr_shape[]>(composShape.count_);
    for (size_t i = 0; i < composShape.count_; ++i)
    {
      shapeList_[i] = composShape.shapeList_[i];
    }
  }
  return *this;
}

konev::CompositeShape &konev::CompositeShape::operator =(konev::CompositeShape &&composShape)
{
  if (this != &composShape)
  {
    count_ = composShape.count_;
    shapeList_ = std::move(composShape.shapeList_);
  }
  return *this;
}

konev::Shape::ptr_shape konev::CompositeShape::operator[](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }
  return shapeList_[index];
}

double konev::CompositeShape::getArea() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  double area = 0.0;
  for (size_t i = 0; i < count_; i++)
  {
    area += shapeList_[i]->getArea();
  }
  return area;
}

konev::rectangle_t konev::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  rectangle_t rectangle = shapeList_[0]->getFrameRect();
  double minX = rectangle.pos.x - rectangle.width / 2;
  double maxX = rectangle.pos.x + rectangle.width / 2;
  double minY = rectangle.pos.y - rectangle.height / 2;
  double maxY = rectangle.pos.y + rectangle.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    rectangle = shapeList_[i]->getFrameRect();

    minX = std::min(rectangle.pos.x - rectangle.width / 2, minX);
    minY = std::min(rectangle.pos.y - rectangle.height / 2, minY);
    maxX = std::max(rectangle.pos.x + rectangle.width / 2, maxX);
    maxY = std::max(rectangle.pos.y + rectangle.height / 2, maxY);
  }
  return {(maxX - minX), (maxY - minY), {(minX + maxX) / 2, (minY + maxY) / 2}};
}

void konev::CompositeShape::move(double dx, double dy)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  for (size_t i = 0; i < count_; ++i)
  {
    shapeList_[i]->move(dx, dy);
  }
}

void konev::CompositeShape::move(const point_t &point)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  double dx = point.x - getFrameRect().pos.x;
  double dy = point.y - getFrameRect().pos.y;
  move(dx, dy);
}

void konev::CompositeShape::scale(double coefficient)
{
  if (count_ == 0)
    {
      throw std::logic_error("Composite shape is empty");
    }

  if (coefficient < 0)
  {
    throw std::invalid_argument("Scale coefficient cannot be less than zero.");
  }

  point_t center = getFrameRect().pos;
  for (size_t i = 0; i < count_; ++i)
  {
    konev::point_t shapeCenter = shapeList_[i]->getFrameRect().pos;
    shapeList_[i]->move((shapeCenter.x - center.x) * (coefficient - 1), (shapeCenter.y - center.y) * (coefficient - 1));
    shapeList_[i]->scale(coefficient);
  }
}

size_t konev::CompositeShape::getSize() const
{
  return count_;
}

void konev::CompositeShape::addShape(const konev::Shape::ptr_shape &newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Adding shape shouldn't be nullptr");
  }

  konev::Shape::array_ptr newShapeList(std::make_unique<std::shared_ptr<konev::Shape>[]>(count_ + 1));
  for (size_t i = 0; i < count_; i++)
  {
    newShapeList[i] = shapeList_[i];
  }
  newShapeList[count_] = newShape;
  count_++;
  shapeList_.swap(newShapeList);
}

void konev::CompositeShape::deleteShape(size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }
  for (size_t i = index; i < count_ - 1; ++i)
  {
    shapeList_[i] = shapeList_[i + 1];
  }
  shapeList_[count_ - 1] = nullptr;
  count_--;
}

void konev::CompositeShape::rotate(double angle)
{
  const konev::point_t center = getFrameRect().pos;
  const double sinus = std::sin(angle * M_PI / 180);
  const double cosine = std::cos(angle * M_PI / 180);
  for (size_t i = 0; i < count_; ++i)
  {
    const konev::point_t shapeCenter = shapeList_[i]->getFrameRect().pos;
    const double dx = shapeCenter.x - center.x;
    const double dy = shapeCenter.y - center.y;
    shapeList_[i]->move(dx * (cosine - 1) - dy * sinus, dx * sinus + dy * (cosine - 1));
    shapeList_[i]->rotate(angle);
  }
}
