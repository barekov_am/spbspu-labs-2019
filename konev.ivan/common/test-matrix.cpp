#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>

#include "composite-shape.hpp"
#include "separation.hpp"
#include "matrix.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(matrixTestSuite)

  BOOST_AUTO_TEST_CASE(copyAndMoveConstructor)
  {
    konev::Rectangle r1({-1.0, -1.0}, 4.0, 4.0);
    konev::Circle c1({3.0, 2.0}, 1.0);
    konev::CompositeShape testShapes(std::make_shared<konev::Rectangle>(r1));
    testShapes.addShape(std::make_shared<konev::Circle>(c1));

    konev::Matrix testMatrix = konev::layer(testShapes);

    BOOST_CHECK_NO_THROW(konev::Matrix testMatrix2(testMatrix));
    BOOST_CHECK_NO_THROW(konev::Matrix testMatrix3(std::move(testMatrix)));
  }

  BOOST_AUTO_TEST_CASE(operatorTests)
  {
    konev::Rectangle r1({-1.0, -1.0}, 4.0, 4.0);
    konev::Circle c1({3.0, 2.0}, 1.0);
    konev::CompositeShape testShapes1(std::make_shared<konev::Rectangle>(r1));
    testShapes1.addShape(std::make_shared<konev::Circle>(c1));

    konev::Matrix testMatrix1 = konev::layer(testShapes1);

    BOOST_CHECK_THROW(testMatrix1[100], std::out_of_range);
    BOOST_CHECK_THROW(testMatrix1[-1], std::out_of_range);

    BOOST_CHECK_NO_THROW(konev::Matrix testMatrix2 = testMatrix1);
    BOOST_CHECK_NO_THROW(konev::Matrix testMatrix3 = std::move(testMatrix1));

    konev::Rectangle r2({-1.0, -1.0}, 4.0, 4.0);
    konev::CompositeShape testShapes2(std::make_shared<konev::Rectangle>(r2));

    konev::Matrix testMatrix4 = konev::layer(testShapes2);
    konev::Matrix testMatrix5 = konev::layer(testShapes2);

    BOOST_CHECK(testMatrix4 == testMatrix5);

    BOOST_CHECK(testMatrix4 != testMatrix1);
    BOOST_CHECK(testMatrix5 != testMatrix1);
  }

BOOST_AUTO_TEST_SUITE_END()
