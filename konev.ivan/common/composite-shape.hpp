#ifndef COMPOSITE_SHAPE_HPP_INCLUDED
#define COMPOSITE_SHAPE_HPP_INCLUDED

#include <memory>

#include "shape.hpp"

namespace konev
{
  class CompositeShape : public Shape
  {
    public:
      CompositeShape();
      CompositeShape(const CompositeShape &copyShape);
      CompositeShape(CompositeShape &&displaceShape);
      CompositeShape(const konev::Shape::ptr_shape &firstShape);
      ~CompositeShape() = default;


      CompositeShape& operator =(const CompositeShape &composShape);
      CompositeShape& operator =(CompositeShape &&composShape);
      konev::Shape::ptr_shape operator [](std::size_t index) const;

      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(double dx, double dy) override;
      void move(const point_t &point) override;
      void scale(double coefficient) override;
      std::size_t getSize() const;
      void addShape(const konev::Shape::ptr_shape &newShape);
      void deleteShape(std::size_t index);
      void rotate(double angle);

    private:
      std::size_t count_;
      konev::Shape::array_ptr shapeList_;
  };
}

#endif // COMPOSITE-SHAPE_HPP_INCLUDED
