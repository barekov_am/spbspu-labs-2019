#include <stdexcept>

#include "matrix.hpp"

konev::Matrix::Matrix() :
  rows_(0),
  columns_(0)
{
}

konev::Matrix::Matrix(const Matrix &copyMatrix) :
  rows_(copyMatrix.rows_),
  columns_(copyMatrix.columns_),
  matrixList_(std::make_unique<konev::Shape::ptr_shape[]>(copyMatrix.rows_ * copyMatrix.columns_))
{
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    matrixList_[i] = copyMatrix.matrixList_[i];
  }
}

konev::Matrix::Matrix(Matrix &&displaceMatrix) :
  rows_(displaceMatrix.rows_),
  columns_(displaceMatrix.columns_),
  matrixList_(std::move(displaceMatrix.matrixList_))
{
}

konev::Matrix &konev::Matrix::operator =(const Matrix &rhs)
{
  if (this != &rhs)
  {
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
    konev::Shape::array_ptr newMatrixList(std::make_unique<konev::Shape::ptr_shape[]>(rhs.rows_ * rhs.columns_));
    for (size_t i = 0; i < (rows_ * columns_); i++)
    {
      newMatrixList[i] = rhs.matrixList_[i];
    }
    matrixList_.swap(newMatrixList);
  }

  return *this;
}

konev::Matrix &konev::Matrix::operator =(Matrix &&displaceMatrix)
{
  if (this != &displaceMatrix)
  {
    rows_ = displaceMatrix.rows_;
    columns_ = displaceMatrix.columns_;
    matrixList_ = std::move(displaceMatrix.matrixList_);
  }

  return *this;
}

std::unique_ptr<std::shared_ptr<konev::Shape>[]> konev::Matrix::operator [](size_t index) const
{
  if (index >= rows_)
  {
    throw std::out_of_range("Index is out of range");
  }

  konev::Shape::array_ptr newMatrixList(std::make_unique<std::shared_ptr<Shape>[]>(columns_));
  for (size_t i = 0; i < columns_; i++)
  {
    newMatrixList[i] = matrixList_[index * columns_ + i];
  }

  return newMatrixList;
}

bool konev::Matrix::operator ==(const Matrix &someMatrix) const
{
  if ((rows_ != someMatrix.rows_) || (columns_ != someMatrix.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (matrixList_[i] != someMatrix.matrixList_[i])
    {
      return false;
    }
  }

  return true;
}

bool konev::Matrix::operator !=(const Matrix &someMatrix) const
{
  return !(*this == someMatrix);
}

void konev::Matrix::add(std::shared_ptr<Shape> shape, size_t row, size_t column)
{
  size_t newRows = (row == rows_) ? (rows_ + 1) : (rows_);
  size_t newColumns = (column == columns_) ? (columns_ + 1) : (columns_);

  konev::Shape::array_ptr newMatrixList(std::make_unique<konev::Shape::ptr_shape[]>(newRows * newColumns));

  for (size_t i = 0; i < newRows; i++)
  {
    for (size_t j = 0; j < newColumns; j++)
    {
      if ((i == rows_) || (j == columns_))
      {
        newMatrixList[i * newColumns + j] = nullptr;
      }
      else
      {
        newMatrixList[i * newColumns + j] = matrixList_[i * columns_ + j];
      }
    }
  }
  newMatrixList[row * newColumns + column] = shape;
  matrixList_.swap(newMatrixList);
  rows_ = newRows;
  columns_ = newColumns;
}

size_t konev::Matrix::getColumns() const
{
  return columns_;
}

size_t konev::Matrix::getRows() const
{
  return rows_;
}
