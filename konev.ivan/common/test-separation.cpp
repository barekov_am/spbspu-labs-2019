#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>

#include "separation.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(separationTestSuite)

  BOOST_AUTO_TEST_CASE(crossingOfShapes)
  {
    konev::Rectangle testRectangle1({0.0, 0.0}, 4.0, 4.0);
    konev::Circle testCircle1({-5.0, 1.0}, 1.0);
    konev::Rectangle testRectangle2({-2.0, 1.0}, 4.0, 4.0);
    konev::Circle testCircle2({-2.0, 0.0}, 1.0);

    BOOST_CHECK(konev::cross(testRectangle1, testCircle2));
    BOOST_CHECK(konev::cross(testRectangle1, testRectangle2));
    BOOST_CHECK(konev::cross(testRectangle2, testCircle1));
    BOOST_CHECK(konev::cross(testRectangle2, testCircle2));

    BOOST_CHECK(!konev::cross(testRectangle1, testCircle1));
    BOOST_CHECK(!konev::cross(testCircle1, testCircle2));
  }

  BOOST_AUTO_TEST_CASE(emptyCompositeLayering)
  {
    konev::Matrix testMatrix = konev::layer(konev::CompositeShape());

    BOOST_CHECK_EQUAL(testMatrix.getRows(), 0);
    BOOST_CHECK_EQUAL(testMatrix.getColumns(), 0);
  }

  BOOST_AUTO_TEST_CASE(correctCompositeSeparation)
  {
    konev::Rectangle r1({-1.0, -1.0}, 4.0, 4.0);
    konev::Circle c1({3.0, 2.0}, 1.0);
    konev::Rectangle r2({1.0, 2.0}, 4.0, 4.0);
    konev::Rectangle r3({1.0, -4.0}, 4.0, 4.0);
    konev::CompositeShape testShapes(std::make_shared<konev::Rectangle>(r1));
    testShapes.addShape(std::make_shared<konev::Circle>(c1));
    testShapes.addShape(std::make_shared<konev::Rectangle>(r2));
    testShapes.addShape(std::make_shared<konev::Rectangle>(r3));

    konev::Matrix testMatrix = konev::layer(testShapes);

    BOOST_CHECK_EQUAL(testMatrix.getRows(), 2);
    BOOST_CHECK_EQUAL(testMatrix.getColumns(), 2);
    BOOST_CHECK_EQUAL(testMatrix[0][0], testShapes[0]);
    BOOST_CHECK_EQUAL(testMatrix[0][1], testShapes[1]);
    BOOST_CHECK_EQUAL(testMatrix[1][0], testShapes[2]);
    BOOST_CHECK_EQUAL(testMatrix[1][1], testShapes[3]);
  }

BOOST_AUTO_TEST_SUITE_END()
