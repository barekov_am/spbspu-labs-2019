#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include <stdexcept>

#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(CircleTestSuite)

  const double calculationError = 0.01;

  BOOST_AUTO_TEST_CASE(immutabilityAfterMovingCircle)
  {
    konev::Circle movableCircle({1.0, 2.0}, 5.0);
    const konev::rectangle_t rectangleBefore = movableCircle.getFrameRect();
    const double areaBefore = movableCircle.getArea();

    movableCircle.move(-1.0, 9.7);
    BOOST_CHECK_CLOSE(rectangleBefore.width, movableCircle.getFrameRect().width, calculationError);
    BOOST_CHECK_CLOSE(rectangleBefore.height, movableCircle.getFrameRect().height, calculationError);
    BOOST_CHECK_CLOSE(areaBefore, movableCircle.getArea(), calculationError);

    movableCircle.move({2.5, 3.2});
    BOOST_CHECK_CLOSE(rectangleBefore.width, movableCircle.getFrameRect().width, calculationError);
    BOOST_CHECK_CLOSE(rectangleBefore.height, movableCircle.getFrameRect().height, calculationError);
    BOOST_CHECK_CLOSE(areaBefore, movableCircle.getArea(), calculationError);
  }

  BOOST_AUTO_TEST_CASE(scalingCircle)
  {
    konev::Circle scalableCircle({1.0, 1.0}, 10.0);
    const double areaBefore = scalableCircle.getArea();
    const double coefficient = 5.2;
    scalableCircle.scale(coefficient);
    BOOST_CHECK_CLOSE(scalableCircle.getArea(), coefficient * coefficient * areaBefore, calculationError);
  }

  BOOST_AUTO_TEST_CASE(invalidCircleValues)
  {
    BOOST_CHECK_THROW(konev::Circle({2.0, 3.5}, -2.0), std::invalid_argument);
    BOOST_CHECK_THROW(konev::Circle({1.0, 2.5}, -1.0), std::invalid_argument);

    konev::Circle wrongCircle1({2.0, 2.0}, 1.0);
    BOOST_CHECK_THROW(wrongCircle1.scale(-2.0), std::invalid_argument);

    konev::Circle wrongCircle2({1.0, 1.0}, 2.0);
    BOOST_CHECK_THROW(wrongCircle1.scale(-1.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(circleRotation)
  {
    konev::Circle testCircle({0, 0}, 10);
    double areaBefore = testCircle.getArea();

    double angle = -98.76;
    BOOST_CHECK_NO_THROW(testCircle.rotate(angle));

    angle = 55.55;
    BOOST_CHECK_NO_THROW(testCircle.rotate(angle));

    angle = -21.5;
    testCircle.rotate(angle);
    BOOST_CHECK_CLOSE(testCircle.getArea(), areaBefore, calculationError);

    angle = 12.34;
    testCircle.rotate(angle);
    BOOST_CHECK_CLOSE(testCircle.getArea(), areaBefore, calculationError);

    angle = 361.0;
    testCircle.rotate(angle);
    BOOST_CHECK_CLOSE(testCircle.getArea(), areaBefore, calculationError);

    angle = -1000.1;
    testCircle.rotate(angle);
    BOOST_CHECK_CLOSE(testCircle.getArea(),areaBefore, calculationError);
  }

BOOST_AUTO_TEST_SUITE_END()
