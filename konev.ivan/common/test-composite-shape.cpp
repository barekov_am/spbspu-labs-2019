#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include <stdexcept>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(CompositeShapeTestSuite)

  const double calculationError = 0.01;

  BOOST_AUTO_TEST_CASE(immutabilityAfterMovingCompositeShape)
  {
    konev::Rectangle r1({1.0, 1.0}, 2.0, 2.0);
    konev::Circle c1({2.0, 2.0}, 2.0);
    konev::CompositeShape testShapes(std::make_shared<konev::Rectangle>(r1));
    testShapes.addShape(std::make_shared<konev::Circle>(c1));
    const konev::rectangle_t rectangleBefore = testShapes.getFrameRect();
    const double areaBefore = testShapes.getArea();

    testShapes.move(10.0, 10.0);
    BOOST_CHECK_CLOSE(rectangleBefore.width, testShapes.getFrameRect().width, calculationError);
    BOOST_CHECK_CLOSE(rectangleBefore.height, testShapes.getFrameRect().height, calculationError);
    BOOST_CHECK_CLOSE(areaBefore, testShapes.getArea(), calculationError);

    testShapes.move({10.0, 10.0});
    BOOST_CHECK_CLOSE(rectangleBefore.width, testShapes.getFrameRect().width, calculationError);
    BOOST_CHECK_CLOSE(rectangleBefore.height, testShapes.getFrameRect().height, calculationError);
    BOOST_CHECK_CLOSE(areaBefore, testShapes.getArea(), calculationError);
  }

  BOOST_AUTO_TEST_CASE(scalingCompositeShape)
  {
    konev::Rectangle r1({1.0, 1.0}, 2.0, 2.0);
    konev::Circle c1({2.0, 2.0}, 2.0);
    konev::CompositeShape testShapes(std::make_shared<konev::Rectangle>(r1));
    testShapes.addShape(std::make_shared<konev::Circle>(c1));

    const double areaBefore = testShapes.getArea();
    const double coefficient = 3.0;
    testShapes.scale(coefficient);
    BOOST_CHECK_CLOSE(testShapes.getArea(), coefficient * coefficient * areaBefore, calculationError);
  }

  BOOST_AUTO_TEST_CASE(invalidCompositeShapeValues)
  {
    konev::CompositeShape testShapes1;
    BOOST_CHECK_THROW(testShapes1.scale(2.0), std::logic_error);
    BOOST_CHECK_THROW(testShapes1.move(1.0, 1.0), std::logic_error);
    BOOST_CHECK_THROW(testShapes1.move({1.0, 1.0}), std::logic_error);
    BOOST_CHECK_THROW(testShapes1.getFrameRect(), std::logic_error);
    BOOST_CHECK_THROW(testShapes1.getArea(), std::logic_error);

    konev::Rectangle r1({1.0, 1.0}, 2.0, 2.0);
    konev::Circle c1({2.0, 2.0}, 2.0);
    konev::CompositeShape testShapes2(std::make_shared<konev::Rectangle>(r1));
    BOOST_CHECK_THROW(testShapes2.addShape(nullptr), std::invalid_argument);
    testShapes2.addShape(std::make_shared<konev::Circle>(c1));
    BOOST_CHECK_THROW(testShapes2.scale(-1.0), std::invalid_argument);

    BOOST_CHECK_THROW(testShapes2.deleteShape(-1), std::out_of_range);
    BOOST_CHECK_THROW(testShapes2.deleteShape(200), std::out_of_range);

    BOOST_CHECK_THROW(testShapes2[-1], std::out_of_range);
    BOOST_CHECK_THROW(testShapes2[100], std::out_of_range);

    BOOST_CHECK_THROW(konev::CompositeShape testShapes3(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(operatorTests)
  {
    konev::Rectangle r1({1.0, 1.0}, 2.0, 2.0);
    konev::CompositeShape testShapes1(std::make_shared<konev::Rectangle>(r1));

    BOOST_CHECK_NO_THROW(konev::CompositeShape testShapes2(testShapes1));
    BOOST_CHECK_NO_THROW(konev::CompositeShape testShapes2(std::move(testShapes1)));

    konev::Circle c1({2.0, 2.0}, 2.0);
    konev::CompositeShape testShapes2(std::make_shared<konev::Circle>(c1));

    BOOST_CHECK_NO_THROW(testShapes1 = testShapes2);
    BOOST_CHECK_NO_THROW(testShapes1 = std::move(testShapes2));

    konev::Rectangle r2({2.0, 2.0}, 3.0, 3.0);
    testShapes1.addShape(std::make_shared<konev::Rectangle>(r2));
    const konev::rectangle_t testFrameRect = r2.getFrameRect();
    const double areaOfShape = r2.getArea();

    BOOST_CHECK_CLOSE(testFrameRect.width, testShapes1[1]->getFrameRect().width, calculationError);
    BOOST_CHECK_CLOSE(testFrameRect.height, testShapes1[1]->getFrameRect().height, calculationError);
    BOOST_CHECK_CLOSE(areaOfShape, testShapes1[1]->getArea(), calculationError);
  }

  BOOST_AUTO_TEST_CASE(compositeShapeRotation)
  {
    konev::Rectangle r1({1.0, 1.0}, 2.0, 2.0);
    konev::Circle c1({2.0, 2.0}, 2.0);
    konev::CompositeShape testShapes(std::make_shared<konev::Rectangle>(r1));
    testShapes.addShape(std::make_shared<konev::Circle>(c1));
    double areaBefore = testShapes.getArea();

    double angle = -98.76;
    BOOST_CHECK_NO_THROW(testShapes.rotate(angle));

    angle = 55.55;
    BOOST_CHECK_NO_THROW(testShapes.rotate(angle));

    angle = -21.5;
    testShapes.rotate(angle);
    BOOST_CHECK_CLOSE(testShapes.getArea(), areaBefore, calculationError);

    angle = 12.34;
    testShapes.rotate(angle);
    BOOST_CHECK_CLOSE(testShapes.getArea(), areaBefore, calculationError);

    angle = 361.0;
    testShapes.rotate(angle);
    BOOST_CHECK_CLOSE(testShapes.getArea(), areaBefore, calculationError);

    angle = -1000.1;
    testShapes.rotate(angle);
    BOOST_CHECK_CLOSE(testShapes.getArea(),areaBefore, calculationError);
  }

BOOST_AUTO_TEST_SUITE_END()
