#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace konev
{
  class Shape
  {
    public:
      using ptr_shape = std::shared_ptr<Shape>;
      using array_ptr = std::unique_ptr<ptr_shape[]>;

      virtual ~Shape() = default;
      virtual double getArea() const = 0;
      virtual rectangle_t getFrameRect() const = 0;
      virtual void move(double dx, double dy) = 0;
      virtual void move(const point_t &dot) = 0;
      virtual void scale(double coefficient) = 0;
      virtual void rotate(double) = 0;
  };
}
#endif
