#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <cmath>
#include <iterator>

#include "tasks.hpp"

void konev::taskOne(){

  std::transform(std::istream_iterator<double>(std::cin), std::istream_iterator<double>(),
                 std::ostream_iterator<double>(std::cout, " "), std::bind2nd(std::multiplies<double>(), M_PI));

  if (!(std::cin.eof())){
    throw(std::invalid_argument("Wrong input!"));
  }

}

