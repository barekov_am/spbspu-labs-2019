#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace konev{

  class Circle: public konev::Shape{
  public:
    Circle(int x, int y);

    void draw() override;

  private:
    int x_;
    int y_;
  };

}

#endif // CIRCLE_HPP

