#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "shape.hpp"

namespace konev{

  class Square: public konev::Shape{
  public:
    Square(int x, int y);

    void draw() override;

  private:
    int x_;
    int y_;
  };

}

#endif // SQUARE_HPP

