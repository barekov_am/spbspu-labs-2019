#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace konev{

  class Triangle: public konev::Shape{
  public:
    Triangle(int x, int y);

    void draw() override;

  private:
    int x_;
    int y_;
  };

}

#endif // TRIANGLE_HPP

