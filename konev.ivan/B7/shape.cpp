#include "shape.hpp"

konev::Shape::Shape(int x, int y):
  x_(x),
  y_(y)
{}

int konev::Shape::getX(){
  return x_;
}

int konev::Shape::getY(){
  return y_;
}

bool konev::Shape::isUpper(konev::Shape &shape){
  return (y_ > shape.getY());
}

bool konev::Shape::isMoreLeft(konev::Shape &shape){
  return (x_ < shape.getX());
}

