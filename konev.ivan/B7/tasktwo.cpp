#include <iostream>
#include <vector>
#include <algorithm>

#include "triangle.hpp"
#include "square.hpp"
#include "circle.hpp"
#include "tasks.hpp"
#include "otherfuncs.hpp"

const bool ORIGINAL = false;
const bool REVERSE = true;

void konev::taskTwo(){
  std::vector<shape_ptr> vectorOfShapes;

  std::string str;

  while(std::getline(std::cin, str)){
    size_t pos = str.find('(');
    if(pos != std::string::npos){
      str.insert(pos, " ");
    }

    std::stringstream line(str);
    std::string word;
    int x, y;
    shape_ptr ptr;

    if(!(line >> word)){
      continue;
    }

    if(!(line >> konev::expect<'('> >> x >> konev::expect<';'> >> y >> konev::expect<')'> )){
      throw(std::invalid_argument("Wrong input syntax!"));
    }

    if(word == "SQUARE"){
      ptr = std::make_shared<konev::Square>(konev::Square(x, y));
    }
    else if(word == "TRIANGLE"){
      ptr = std::make_shared<konev::Triangle>(konev::Triangle(x, y));
    }
    else if(word == "CIRCLE"){
      ptr = std::make_shared<konev::Circle>(konev::Circle(x, y));
    }
    else
    {
      throw(std::invalid_argument("Wrong shape type!"));
    }

    vectorOfShapes.push_back(ptr);
  }

  if(!(std::cin.eof())){
    throw(std::invalid_argument("Wrong input!"));
  }

  if(vectorOfShapes.empty()){
    return;
  }

  std::cout << "Original:\n";
  konev::shapesPrinting(vectorOfShapes, ORIGINAL);

  std::cout << "Left-Right:\n";
  std::sort(vectorOfShapes.begin(), vectorOfShapes.end(), konev::leftRightSorting);
  konev::shapesPrinting(vectorOfShapes, ORIGINAL);

  std::cout << "Right-Left:\n";
  konev::shapesPrinting(vectorOfShapes, REVERSE);

  std::cout << "Top-Bottom:\n";
  std::sort(vectorOfShapes.begin(), vectorOfShapes.end(), konev::upDownSorting);
  konev::shapesPrinting(vectorOfShapes, ORIGINAL);

  std::cout << "Bottom-Top:\n";
  konev::shapesPrinting(vectorOfShapes, REVERSE);

}



