#ifndef OTHERFUNCS_HPP
#define OTHERFUNCS_HPP

#include <memory>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "shape.hpp"

using shape_ptr = std::shared_ptr<konev::Shape>;

namespace konev{

  void shapesPrinting(std::vector<shape_ptr> & vectorOfShapes, bool direction){
    if (direction){
      for (auto i = vectorOfShapes.rbegin(); i != vectorOfShapes.rend(); i++)
      {
        (*i)->draw();
      }
    }
    else{
      for (auto i = vectorOfShapes.begin(); i != vectorOfShapes.end(); i++)
      {
        (*i)->draw();
      }
    }
  }

  bool leftRightSorting(shape_ptr & sh1, shape_ptr & sh2){
    return (sh1->isMoreLeft(*sh2));
  }

  bool upDownSorting(shape_ptr & sh1, shape_ptr & sh2){
    return (sh1->isUpper(*sh2));
  }

  template<char C>
  std::istream &expect(std::istream &in){
    if ((in >> std::ws).peek() == C){
      in.ignore();
    }
    else
    {
      in.setstate(std::ios_base::failbit);
    }
    return in;
  }

}

#endif // OTHERFUNCS_HPP

