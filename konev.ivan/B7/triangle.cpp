#include <iostream>

#include "triangle.hpp"

konev::Triangle::Triangle(int x, int y):
  konev::Shape(x, y),
  x_(x),
  y_(y)
{}

void konev::Triangle::draw(){
  std::cout << "TRIANGLE (" << x_ << "; " << y_ << ")\n";
}


