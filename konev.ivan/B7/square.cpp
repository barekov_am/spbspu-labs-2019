#include <iostream>

#include "square.hpp"

konev::Square::Square(int x, int y):
  konev::Shape(x, y),
  x_(x),
  y_(y)
{}

void konev::Square::draw(){
  std::cout << "SQUARE (" << x_ << "; " << y_ << ")\n";
}

