#include <iostream>

#include "circle.hpp"

konev::Circle::Circle(int x, int y):
  konev::Shape::Shape(x, y),
  x_(x),
  y_(y)
{}

void konev::Circle::draw() {
  std::cout << "CIRCLE (" << x_ << "; " << y_ << ")\n";
}

