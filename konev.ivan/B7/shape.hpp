#ifndef SHAPE_HPP
#define SHAPE_HPP

namespace konev{

  class Shape{
  public:
    Shape(int x, int y);
    ~Shape() = default;

    int getX();
    int getY();

    bool isUpper(konev::Shape & shape);
    bool isMoreLeft(konev::Shape & shape);
    virtual void draw() = 0;

  private:

    int x_;
    int y_;

  };

}

#endif // SHAPE_HPP

