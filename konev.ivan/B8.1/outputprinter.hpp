#ifndef OUTPUTPRINTER_HPP
#define OUTPUTPRINTER_HPP

#include <iosfwd>
#include <list>
#include "symbol.hpp"

namespace konev
{
  class OutputPrinter
  {
    public:
      OutputPrinter(std::ostream & output_stream, std::list<konev::symbol_t> & text, size_t lineWidth);
      void print();
      bool checkForNextLine(std::list<konev::symbol_t>::iterator iterForCheck, size_t currentWidth = 0);

    private:
      std::ostream & output_stream_;
      std::list<konev::symbol_t> & text_;
      const size_t lineWidth_;
      void deletePrintedText(std::list<konev::symbol_t>::iterator iterForDelete);
  };
}

#endif
