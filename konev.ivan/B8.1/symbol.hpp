#ifndef SYMBOL_HPP
#define SYMBOL_HPP

#include <string>

namespace konev
{
  const size_t MIN_LINE_WIDTH = 25;
  const size_t DEFAULT_LINE_WIDTH = 40;
  const size_t MAX_TOKEN_LENGTH = 20;
  const size_t DASH_LENGTH = 3;
  const size_t THREE_DOTS_LENGTH = 3;
  const size_t SPACE_LENGTH = 1;
  const size_t SYNTAX_LENGTH = 1;

  struct symbol_t
  {
    enum sym_type
    {
      WORD,
      NUMBER,
      SYNTAX,
      DASH,
      SPACE,
      THREEDOTS
    };

    std::string value;
    sym_type type;
  };
}

#endif
