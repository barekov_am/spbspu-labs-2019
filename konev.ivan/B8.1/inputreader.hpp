#ifndef INPUTEDITOR_HPP
#define INPUTEDITOR_HPP

#include <iosfwd>
#include <list>
#include <locale>
#include "symbol.hpp"

namespace konev
{
  class InputReader
  {
    public:
      InputReader(std::istream & input_stream, std::list<konev::symbol_t> & text);
      bool checkTextFormat();
      void getToken();

    private:
      std::istream & input_stream_;
      const std::locale loc_;
      const std::ctype<char> & facet_;
      std::list<konev::symbol_t> & text_;
      void getWord();
      void getNumber();
      void getDash();
      void getThreeDots();
  };
}

#endif
