#include "inputreader.hpp"
#include <istream>
#include <stdexcept>

konev::InputReader::InputReader(std::istream & input_stream, std::list<konev::symbol_t> & text):
  input_stream_(input_stream),
  loc_(input_stream.getloc()),
  facet_(std::use_facet<std::ctype<char>>(loc_)),
  text_(text)
{}

bool konev::InputReader::checkTextFormat()
{
  if (!text_.empty() && (text_.front().type != konev::symbol_t::WORD)
    && (text_.front().type != konev::symbol_t::THREEDOTS) && (text_.front().type != konev::symbol_t::NUMBER))
  {
    return false;
  }

  for (auto iter = text_.begin(); iter != text_.end(); ++iter)
  {
    switch (iter->type)
    {
    case konev::symbol_t::WORD:
      if (iter->value.size() > konev::MAX_TOKEN_LENGTH)
      {
        throw std::invalid_argument("Too long word was met!");
      }
      break;

    case konev::symbol_t::NUMBER:
      if (iter->value.size() > konev::MAX_TOKEN_LENGTH)
      {
        throw std::invalid_argument("Too long number was met!");
      }
      break;

    case konev::symbol_t::DASH:
      if (iter != text_.begin())
      {
        const konev::symbol_t & prev = *std::prev(iter);
        if ((prev.type == konev::symbol_t::DASH) || ((prev.type == konev::symbol_t::SYNTAX) && (prev.value != ",")))
        {
          throw std::invalid_argument("Two dashes in a row or a dash after not-comma punctuation sign met!");
        }
      }
      break;

    case konev::symbol_t::SYNTAX:
      if (iter != text_.begin())
      {
        const konev::symbol_t & prev = *std::prev(iter);
        if ((prev.type == konev::symbol_t::DASH) || (prev.type == konev::symbol_t::SYNTAX)
          || ((prev.type == konev::symbol_t::THREEDOTS) && (iter->value != ",")))
        {
          throw std::invalid_argument("Incorrect punctuation");
        }
      }
      break;

    default:
      break;
    }
  }
  return true;
}

void konev::InputReader::getToken()
{
  input_stream_ >> std::ws;
  char ch = input_stream_.get();
  if (facet_.is(std::ctype_base::alpha, ch))
  {
    input_stream_.unget();
    getWord();
  }
  else if (ch == '-')
  {
    if (input_stream_.peek() == '-')
    {
      input_stream_.unget();
      getDash();
    }
    else
    {
      input_stream_.unget();
      getNumber();
    }
  }
  else if ((ch == '+') || facet_.is(std::ctype_base::digit, ch))
  {
    input_stream_.unget();
    getNumber();
  }
  else if (facet_.is(std::ctype_base::punct, ch))
  {
    if ((ch == '.') && (input_stream_.peek() == '.'))
    {
      input_stream_.unget();
      getThreeDots();
    }
    else
    {
      konev::symbol_t token{std::string(1, ch), konev::symbol_t::SYNTAX};
      text_.push_back(token);
    }
  }
}

void konev::InputReader::getWord()
{
  konev::symbol_t token{"", konev::symbol_t::WORD};

  do {
    char ch = input_stream_.get();
    token.value.push_back(ch);
    if ((ch == '-') && (input_stream_.peek() == '-'))
    {
      token.value.pop_back();
      input_stream_.unget();
      break;
    }
  } while (facet_.is(std::ctype_base::alpha, input_stream_.peek()) || (input_stream_.peek() == '-'));

  text_.push_back(token);
}

void konev::InputReader::getNumber()
{
  konev::symbol_t token{"", konev::symbol_t::NUMBER};
  static const char point = std::use_facet<std::numpunct<char>>(loc_).decimal_point();
  bool readPoint = false;

  do {
    char ch = input_stream_.get();
    if (ch == point)
    {
      if (readPoint)
      {
        input_stream_.unget();
        break;
      }
      readPoint = true;
    }
    token.value.push_back(ch);
  } while (facet_.is(std::ctype_base::digit, input_stream_.peek()) || (input_stream_.peek() == point));

  text_.push_back(token);
}

void konev::InputReader::getDash()
{
  konev::symbol_t token{"", konev::symbol_t::DASH};

  while (input_stream_.peek() == '-')
  {
    token.value.push_back(input_stream_.get());
  }

  if (token.value != "---")
  {
    throw std::invalid_argument("Incorrect dash type or multiple-signed number!");
  }
  text_.push_back(token);
}

void konev::InputReader::getThreeDots()
{
  konev::symbol_t token{"", konev::symbol_t::THREEDOTS};

  while (input_stream_.peek() == '.')
  {
    token.value.push_back(input_stream_.get());
  }

  if (token.value != "...")
  {
    throw std::invalid_argument("Incorrect three dots type!");
  }
  text_.push_back(token);
}
