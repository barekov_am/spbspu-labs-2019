#include "outputprinter.hpp"
#include <ostream>
#include <stdexcept>
#include <vector>

konev::OutputPrinter::OutputPrinter(std::ostream & output_stream, std::list<konev::symbol_t> & text, size_t lineWidth):
  output_stream_(output_stream),
  text_(text),
  lineWidth_(lineWidth)
{}

void konev::OutputPrinter::print()
{
  bool noSpace = false;
  bool noCommaSpace = false;
  size_t currentWidth = 0;

  for (auto iter = text_.begin(); iter != text_.end(); ++iter)
  {
    if (checkForNextLine(iter, currentWidth) && (currentWidth != 0))
    {
      deletePrintedText(iter);
      break;
    }

    switch (iter->type)
    {
    case konev::symbol_t::SYNTAX:
      if ((iter != prev(text_.end())) && (iter != text_.begin()))
      {
        if ((iter->value == ",") && ((next(iter)->type == konev::symbol_t::THREEDOTS)
          || (prev(iter)->type == konev::symbol_t::THREEDOTS)))
        {
          noCommaSpace = true;
          break;
        }
      }
      output_stream_ << iter->value;
      currentWidth += SYNTAX_LENGTH;
      break;

    case konev::symbol_t::THREEDOTS:
      if (iter != text_.begin())
      {
        const konev::symbol_t & prev = *std::prev(iter);
        if ((prev.value == ".") || (prev.value == "!") || (prev.value == "?"))
        {
          noSpace = true;
        }
      }
      else
      {
        noSpace = true;
      }
      if ((iter != text_.begin()) && !noCommaSpace)
      {
        output_stream_ << " ";
        currentWidth += SPACE_LENGTH;
      }
      output_stream_ << iter->value;
      currentWidth += THREE_DOTS_LENGTH;
      break;

    case konev::symbol_t::DASH:
      output_stream_ << " " << iter->value;
      currentWidth += SPACE_LENGTH + DASH_LENGTH;
      break;

    case konev::symbol_t::WORD:

    case konev::symbol_t::NUMBER:
      if ((currentWidth != 0) && !noSpace)
      {
        output_stream_ << " ";
        currentWidth += SPACE_LENGTH;
      }
      else if (noSpace)
      {
        noSpace = false;
      }
      output_stream_ << iter->value;
      currentWidth += iter->value.size();
      break;

    case konev::symbol_t::SPACE:
      break;
    }
  }
  output_stream_ << "\n";
}

bool konev::OutputPrinter::checkForNextLine(std::list<konev::symbol_t>::iterator iterForCheck, size_t currentWidth)
{
  if (currentWidth == 0)
  {
    for (auto iter = text_.begin(); iter != iterForCheck; ++iter)
    {
      currentWidth += iter->value.size();
    }
  }

  if (iterForCheck != text_.begin())
  {
    if ((iterForCheck->type == konev::symbol_t::WORD) || (iterForCheck->type == konev::symbol_t::NUMBER))
    {
      if (currentWidth + SPACE_LENGTH + iterForCheck->value.size() > lineWidth_)
      {
        return true;
      }
      else if (iterForCheck != prev(text_.end()))
      {
        if ((next(iterForCheck)->type == konev::symbol_t::SYNTAX)
          && (currentWidth + SPACE_LENGTH + iterForCheck->value.size() + SYNTAX_LENGTH > lineWidth_))
        {
          return true;
        }
        else if ((next(iterForCheck)->type == konev::symbol_t::DASH)
          && (currentWidth + SPACE_LENGTH + iterForCheck->value.size() + SPACE_LENGTH + DASH_LENGTH > lineWidth_))
        {
          return true;
        }
      }
    }
  }
  return false;
}

void konev::OutputPrinter::deletePrintedText(std::list<konev::symbol_t>::iterator iterForDelete)
{
  std::list<konev::symbol_t> newLineText;
  for (auto iter = iterForDelete; iter != text_.end(); ++iter)
  {
    newLineText.push_back(*iter);
  }
  text_.clear();
  text_ = newLineText;
}
