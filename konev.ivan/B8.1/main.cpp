#include <stdexcept>
#include <cstring>
#include <iostream>
#include "inputreader.hpp"
#include "outputprinter.hpp"
#include "symbol.hpp"

int main(int argc, char *argv[])
{
  try
  {
    if ((argc != 3) && (argc != 1))
    {
      throw std::invalid_argument("Wrong number of parameters!");
    }
    size_t lineWidth = konev::DEFAULT_LINE_WIDTH;
    if (argc == 3)
    {
      if (strcmp(argv[1], "--line-width"))
      {
        throw std::invalid_argument("Must be '--line-width'!");
      }
      else
      {
        lineWidth = atoi(argv[2]);
        if (lineWidth < konev::MIN_LINE_WIDTH)
        {
          throw std::invalid_argument("Too small line-width!");
        }
      }
    }
    std::list<konev::symbol_t> text;
    konev::InputReader reader(std::cin, text);
    konev::OutputPrinter printer(std::cout, text, lineWidth);
    while (std::cin)
    {
      reader.getToken();
      if (!reader.checkTextFormat())
      {
        throw std::invalid_argument("Wrong input data!");
      }
      while (printer.checkForNextLine(prev(text.end())))
      {
        printer.print();
      }
    }
    printer.print();
  }
  catch (const std::exception & err)
  {
    std::cerr << err.what() << "\n";
    return 1;
  }
  return 0;
}
