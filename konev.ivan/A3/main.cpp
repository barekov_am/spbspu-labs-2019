#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  konev::Rectangle r1({1.0, 1.0}, 3.0, 3.0);
  std::cout << r1.getArea() << std::endl;
  r1.scale(2.0);
  std::cout << r1.getArea() << std::endl;

  konev::Rectangle r2({2.0, 2.0}, 1.0, 4.0);
  r2.move(1.0, 2.0);

  konev::Circle c1({3.0, 1.0}, 3.0);
  std::cout << c1.getArea() << std::endl;
  c1.scale(2.0);
  std::cout << c1.getArea() << std::endl;

  konev::Circle c2({1.0, 2.0}, 4.0);
  c2.move({3.0, 5.0});

  konev::Shape *circP = &c2;
  konev::rectangle_t tempRect = circP->getFrameRect();
  std::cout << tempRect.pos.x << " " << tempRect.pos.y << " " << tempRect.width << " " << tempRect.height << std::endl;

  konev::Shape *rectP = &r2;
  tempRect = rectP->getFrameRect();
  std::cout << tempRect.pos.x << " " << tempRect.pos.y << " " << tempRect.width << " " << tempRect.height << std::endl;

  konev::CompositeShape shapes1;
  shapes1.addShape(std::make_shared<konev::Rectangle>(r1));
  std::cout << "Area of first composite shape: " << shapes1.getArea() << '\n';
  shapes1.addShape(std::make_shared<konev::Circle>(c1));
  std::cout << "Area of first composite shape after adding new shape: " << shapes1.getArea() << '\n';
  std::cout << "Center of first composite shape: \n";
  std::cout << "x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';
  shapes1.move(7.0,7.0);
  std::cout << "Center of first composite shape after moving: \n";
  std::cout << "x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';
  shapes1.scale(3.0);
  std::cout << "Center of first composite shape after scaling: \n";
  std::cout << "x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';

  konev::CompositeShape shapes2(std::make_shared<konev::Rectangle>(r2));
  std::cout << "Area of second composite shape: " << shapes2.getArea() << '\n';
  shapes2.addShape(std::make_shared<konev::Circle>(c2));
  std::cout << "Area of second composite shape after adding new shape: " << shapes2.getArea() << '\n';
  shapes2.deleteShape(1);
  std::cout << "Area of second composite shape after deleting second shape: " << shapes2.getArea() << '\n';
  std::cout << "Center of second composite shape: \n";
  std::cout << "x = " << shapes2.getFrameRect().pos.x << ", y = " << shapes2.getFrameRect().pos.y << '\n';
  shapes2.move({7.0,7.0});
  std::cout << "Center of second composite shape after moving: \n";
  std::cout << "x = " << shapes2.getFrameRect().pos.x << ", y = " << shapes2.getFrameRect().pos.y << '\n';
  shapes2.scale(3.0);
  std::cout << "Center of second composite shape after scaling: \n";
  std::cout << "x = " << shapes2.getFrameRect().pos.x << ", y = " << shapes2.getFrameRect().pos.y << '\n';

  return 0;
}
