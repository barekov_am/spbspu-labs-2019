#ifndef OTHERFUNCS_HPP
#define OTHERFUNCS_HPP

#include <sstream>
#include <vector>

#include "datastruct.hpp"

namespace konev{

  konev::DataStruct structBuilder(std::istream & line);

  bool compare(konev::DataStruct struct1, konev::DataStruct struct2);

}

#endif // OTHERFUNCS_HPP

