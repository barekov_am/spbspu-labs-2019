#include "otherfuncs.hpp"
#include <stdexcept>

const int MIN_RANGE_VALUE = -5;
const int MAX_RANGE_VALUE = 5;

konev::DataStruct konev::structBuilder(std::istream &line){

  int key1;
  int key2;

  std::string keyOne;
  std::string keyTwo;
  std::string str;
  char ch;

  while (!line.eof()){
    ch = line.get();
    if (std::isdigit(ch)){
      keyOne.push_back(ch);
    }
    else if ((ch == '-') || (ch == '+')){
      if (!keyOne.empty()){
        throw(std::invalid_argument("Key1 is not a number"));
      }
      keyOne.push_back(ch);
    }
    else if (ch == ','){
      break;
    }
    else if (!std::isspace(ch)){
      throw(std::invalid_argument("Key1 is not a number"));
    }
  }

  while (!line.eof()){

    ch = line.get();
    if (std::isdigit(ch)){
      keyTwo.push_back(ch);
    }
    else if ((ch == '-') || (ch == '+')){
      if (!keyTwo.empty()){
        throw(std::invalid_argument("Key2 is not a number"));
      }
      keyTwo.push_back(ch);
    }
    else if (ch == ','){
      break;
    }
    else if (!std::isspace(ch)){
      throw(std::invalid_argument("Key2 is not a number"));
    }
  }

    std::getline(line, str);

  if(keyOne.empty() || keyTwo.empty() || str.empty()){
    throw(std::invalid_argument("Сan't create an empty element"));
  }
  key1 = std::stoi(keyOne);
  key2 = std::stoi(keyTwo);
  if((key1 > MAX_RANGE_VALUE) || (key1 < MIN_RANGE_VALUE) || (key2 > MAX_RANGE_VALUE) || (key2 < MIN_RANGE_VALUE)){
    throw(std::invalid_argument("One of keys is out of range"));
  }

  konev::DataStruct data = {key1, key2, str};
  return(data);

}

bool konev::compare(konev::DataStruct struct1, konev::DataStruct struct2){
  if (struct1.key1 < struct2.key1)
  {
    return true;
  }

  if (struct1.key1 == struct2.key1)
  {
    if (struct1.key2 == struct2.key2)
    {
      if (struct1.str.size() == struct2.str.size())
      {
        return (struct1.str < struct2.str);
      }
      return (struct1.str.size() < struct2.str.size());
    }
    return (struct1.key2 < struct2.key2);
  }
  return false;

}



