#include <set>
#include <string>
#include <iostream>

#include "tasks.hpp"

void konev::taskOne(){

  std::set<std::string> words;
  std::string word;

  while (std::cin >> word){
    words.insert(word);
  }

  if(!words.empty()){
    for (auto i = words.begin(); i != words.end(); i++){
      std::cout << *i << std::endl;
    }
  }

}


