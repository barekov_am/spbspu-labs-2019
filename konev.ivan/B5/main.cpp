#include <stdexcept>
#include <iostream>

#include "tasks.hpp"

int main(int argc, char * argv[]){

  try{
    if(argc != 2){
      throw(std::invalid_argument("Wrong parameters number!"));
    }

    switch (*argv[1]) {
    case '1':
      konev::taskOne();
      break;

    case '2':
      konev::taskTwo();
      break;

    default:
      throw(std::invalid_argument("Wrong task number!"));
    }

  }
  catch(std::exception & err){
    std::cerr << err.what();
    return 1;
  }

  return 0;

}



