#include <string>
#include <iostream>
#include <algorithm>
#include <stdexcept>
#include <sstream>

#include "tasks.hpp"
#include "point.hpp"
#include "otherfuncs.hpp"

void konev::taskTwo(){

  std::string str;
  std::vector<konev::Shape> shapes;

  while (std::getline(std::cin, str)){

    std::istringstream line(str);

    size_t vertexNum;
    if(!(line >> vertexNum)){
      continue;
    }

    if(vertexNum < konev::TRIANGLE_VERTEX){
      throw(std::invalid_argument("Num of vertex less than minimum"));
    }

    konev::Shape currShape(vertexNum);

    for(size_t i = 0; i < vertexNum; i++){
      konev::Point p;
      if(!(line >> expect<'('> >> p.x >> expect<';'> >> p.y >> expect<')'>)){
        throw(std::invalid_argument("Wrong input data!"));
      }
      currShape[i] = p;
    }
    shapes.push_back(currShape);
  }

  size_t vertexes = 0;
  size_t squaresNum = 0;
  size_t triangleNum = 0;
  size_t rectangleNum = 0;
  size_t size;

  for(auto i = shapes.begin(); i != shapes.end(); i++){
    size = (*i).size();
    vertexes += size;

    if(size == konev::TRIANGLE_VERTEX){
      triangleNum++;
    }
    else if(size == konev::RECTANGLE_VERTEX){
      if(isRectangle(*i)){
        rectangleNum++;
        if(isSquare(*i)){
          squaresNum++;
        }
      }
    }
  }

  std::vector<konev::Point> points;
  for(auto i = shapes.begin(); i != shapes.end(); i++){
    if((*i).size() != konev::PENTAGON_VERTEX){
      points.push_back((*i)[0]);
    }
  }

  if(shapes.size() > 1){
    std::sort(shapes.begin(), shapes.end(), konev::compareShapes);
  }

  auto pentagonBegin = shapes.end();
  auto pentagonEnd = shapes.end();

  for(auto i = shapes.begin(); i != shapes.end(); i++){
    if((*i).size() == konev::PENTAGON_VERTEX){
      pentagonBegin = i;
      break;
    }
  }
  for(auto j = pentagonBegin; j !=shapes.end(); j++){
    if((*j).size() != konev::PENTAGON_VERTEX){
      pentagonEnd = (j);
      break;
    }
  }

  shapes.erase(pentagonBegin, pentagonEnd);
  shapes.shrink_to_fit();

  std::cout << "Vertices: " << vertexes << std::endl;
  std::cout << "Triangles: " << triangleNum << std::endl;
  std::cout << "Squares: " << squaresNum << std::endl;
  std::cout << "Rectangles: " << rectangleNum << std::endl;

  std::cout << "Points:";
  for(auto i = points.begin(); i != points.end(); i++){
    std::cout << " ( " << (*i).x << "; " << (*i).y << ")";
  }
  std::cout << std::endl;

  std::cout << "Shapes:";

  for(auto i = shapes.begin(); i != shapes.end(); i++){
    konev::shapeOutput(*i);
  }

}
