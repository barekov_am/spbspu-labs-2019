#ifndef TASKS_HPP
#define TASKS_HPP

namespace konev {

  void taskOne();
  void taskTwo();

}

#endif // TASKS_HPP

