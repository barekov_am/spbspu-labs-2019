#ifndef OTHERFUNCS_HPP
#define OTHERFUNCS_HPP

#include <string>
#include <iostream>

#include "point.hpp"

namespace konev {

  const int TRIANGLE_VERTEX = 3;
  const int RECTANGLE_VERTEX = 4;
  const int PENTAGON_VERTEX = 5;

  bool compareShapes(const konev::Shape & shape1, const konev::Shape & shape2);

  void shapeOutput(const konev::Shape& shape);

  bool isSquare(const konev::Shape & sh);

  bool isRectangle(const konev::Shape & sh);

  template<char C>
  std::istream &expect(std::istream &in){
    if ((in >> std::ws).peek() == C){
      in.ignore();
    }
    else
    {
      in.setstate(std::ios_base::failbit);
    }
    return in;
  }

}


#endif // OTHERFUNCS_HPP

