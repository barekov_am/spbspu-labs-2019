#include <sstream>
#include <stdexcept>
#include <iostream>
#include <algorithm>

#include "otherfuncs.hpp"

bool konev::compareShapes(const konev::Shape &shape1, const konev::Shape &shape2){
  if((shape1.size() < shape2.size()) && (shape1.size() <= konev::PENTAGON_VERTEX)){
    return true;
  }
  else if((shape1.size() == konev::RECTANGLE_VERTEX) && (shape2.size() == shape1.size())){
    if(konev::isRectangle(shape1)){
      if(!konev::isRectangle(shape2)){
        return true;
      }
      return (konev::isSquare(shape1) && !konev::isSquare(shape2));
    }
  }
  return false;
}

  std::istream &operator>>(std::istream & in, konev::Point & p){
    return in >> konev::expect<'('> >> p.x >> konev::expect<';'> >> p.y >> konev::expect<')'>;
  }

bool konev::isRectangle(const konev::Shape & sh){
  return((((sh[1].x - sh[0].x) * (sh[3].x - sh[0].x) + (sh[1].y - sh[0].y) * (sh[3].y - sh[0].y)) == 0) &&
      ((sh[3].x - sh[2].x) * (sh[1].x - sh[2].x) + (sh[3].y - sh[2].y) * (sh[1].y - sh[2].y)) == 0);
}

bool konev::isSquare(const konev::Shape &sh){
  return(((sh[2].x - sh[0].x) * (sh[3].x - sh[1].x) + (sh[2].y - sh[0].y) * (sh[3].y - sh[1].y)) == 0);
}

void konev::shapeOutput(const konev::Shape& shape){
  std::cout <<std::endl << shape.size();
  for (auto i = shape.begin(); i != shape.end(); i++){
    std::cout << " ( " << (*i).x << "; " << (*i).y << ")";
  }
}




