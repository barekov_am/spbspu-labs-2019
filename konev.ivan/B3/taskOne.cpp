#include <sstream>
#include <iostream>
#include "tasks.hpp"
#include "interface.hpp"
#include "phoneBook.hpp"

int konev::doFirstPart(){
  konev::PhoneBook book;
  konev::UserInterface bookInter(book, std::cout);

  std::string line;
  while (std::getline(std::cin, line)){
    std::stringstream command(line);

    if (std::cin.bad()){
      return 2;
    }
    else if (std::cin.eof()){
      break;
    }
    else
    {
      bookInter.chooseAction(command);
    }
  }

  return 0;
}
