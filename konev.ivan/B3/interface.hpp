#ifndef USERINTERFACE_HPP
#define USERINTERFACE_HPP
#include <iostream>
#include "phoneBook.hpp"

namespace konev {

  class UserInterface
  {
    public:
      UserInterface(PhoneBook& book, std::ostream& output);
      void chooseAction(std::istream& command);
      void add(std::istream& command);
      void store(std::istream& command);
      void insert(std::istream& command);
      void show(std::istream& command);
      void move(std::istream& command);
      void del(std::istream& command);

    private:
      PhoneBook& book_;
      std::ostream& output_;
      UserInterface() = delete;
      bool HasQuotes(const std::string& name);
      void formatName(std::string& name);

  };

}
#endif
