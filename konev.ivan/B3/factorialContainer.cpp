#include "factorialContainer.hpp"

const size_t FACTORIAL_BEGIN_INDEX = 1;
const size_t FACTORIAL_AFTER_END_INDEX = 11;

konev::FactorialContainer::FactorialContainer(){
}

konev::FactorialContainer::Iterator konev::FactorialContainer::begin(){
  return konev::FactorialContainer::Iterator(FACTORIAL_BEGIN_INDEX);
}

konev::FactorialContainer::Iterator konev::FactorialContainer::end(){
  return konev::FactorialContainer::Iterator(FACTORIAL_AFTER_END_INDEX);
}

konev::FactorialContainer::Iterator::Iterator():
  Iterator(FACTORIAL_BEGIN_INDEX)
{
}

konev::FactorialContainer::Iterator::Iterator(size_t number):
  number_(number)
{
}

size_t konev::FactorialContainer::Iterator::calculateFactorial(size_t num) const {
  size_t result = 1;
  for (size_t i = 1; i <= num; i++){
    result *= i;
  }
  return result;
}

size_t konev::FactorialContainer::Iterator::operator*() const{
  return calculateFactorial(number_);
}

size_t konev::FactorialContainer::Iterator::operator->() const{
  return calculateFactorial(number_);
}

konev::FactorialContainer::Iterator& konev::FactorialContainer::Iterator::operator++(){
  if (number_ < FACTORIAL_AFTER_END_INDEX){
    number_++;
  }
  return *this;
}

const konev::FactorialContainer::Iterator konev::FactorialContainer::Iterator::operator++(int){
  FactorialContainer::Iterator newit = *this;

  if (number_ < FACTORIAL_AFTER_END_INDEX){
    number_++;
  }
  return newit;
}


konev::FactorialContainer::Iterator& konev::FactorialContainer::Iterator::operator--(){
  if (number_ > FACTORIAL_BEGIN_INDEX){
    number_--;
  }
  return *this;
}

const konev::FactorialContainer::Iterator konev::FactorialContainer::Iterator::operator--(int){
  FactorialContainer::Iterator tmp_iterator = *this;

  if (number_ > FACTORIAL_BEGIN_INDEX){
    number_--;
  }
  return tmp_iterator;
}

bool konev::FactorialContainer::Iterator::operator==(const Iterator& rhs) const{
  return (number_ == rhs.number_);
}

bool konev::FactorialContainer::Iterator::operator!=(const Iterator& rhs) const{
  return (number_ != rhs.number_);
}

