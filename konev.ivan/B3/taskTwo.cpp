#include <algorithm>
#include <iterator>
#include <iostream>
#include "tasks.hpp"
#include "factorialContainer.hpp"

namespace konev {

  int doSecondPart(){
    FactorialContainer cont;

    std::copy(cont.begin(), cont.end(), std::ostream_iterator<size_t>(std::cout, " "));
    std::cout << std::endl;

    std::reverse_copy(cont.begin(), cont.end(), std::ostream_iterator<size_t>(std::cout, " "));
    std::cout << std::endl;

    return 0;
  }

}

