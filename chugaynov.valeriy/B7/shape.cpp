#include "shape.hpp"

#include <string>
#include <iostream>

#include "manipulators.hpp"
#include "figures.hpp"

Shape::Shape(const Point& point) :
  center_(point)
{}

bool Shape::isMoreLeft(const Shape& shape) const
{
  return this->center_.x < shape.center_.x;
}

bool Shape::isUpper(const Shape& shape) const
{
  return this->center_.y > shape.center_.y;
}


std::ostream& operator <<(std::ostream& out, const ShapePtr& shape)
{
  if (shape)
  {
    shape->draw(out);
  }
  return out;
}

std::istream& operator >>(std::istream& in, ShapePtr& shape)
{
  while ((in >> blank).peek() == '\n')
  {
    in.get();
  }
  if (in.eof())
  {
    in.setstate(std::ios_base::failbit);
    return in;
  }



  std::string figureName = "";
  while (isupper(in.peek()))
  {
    figureName += in.get();
  }

  if ((in >> blank).peek() != '\(')
  {
    in.clear(std::ios_base::failbit);
  }



  if (in)
  {
    Point point;
    in >> point >> blank;

    if (!in.eof() && in.get() != '\n')
    {
      in.clear(std::ios_base::failbit);
    }

    if (in)
    {
      if (figureName == "TRIANGLE")
      {
        shape = std::make_shared<Triangle>(point);
      }
      else if (figureName == "SQUARE")
      {
        shape = std::make_shared<Square>(point);
      }
      else if (figureName == "CIRCLE")
      {
        shape = std::make_shared<Circle>(point);
      }
      else
      {
        in.clear(std::ios_base::failbit);
      }
    }
  }

  return in;
}
