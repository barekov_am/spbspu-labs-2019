#include "figures.hpp"

void Circle::draw(std::ostream& out) const
{
  out << "CIRCLE " << center_;
}

void Triangle::draw(std::ostream& out) const
{
  out << "TRIANGLE " << center_;
}

void Square::draw(std::ostream& out) const
{
  out << "SQUARE " << center_;
}
