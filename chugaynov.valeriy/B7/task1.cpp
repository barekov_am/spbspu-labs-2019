#define _USE_MATH_DEFINES

#include <iterator>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <list>

struct MulByPi
{
  double operator ()(const double num)
  {
    return num * M_PI;
  }
};

void task1()
{
  std::list<double> list(std::istream_iterator<double>(std::cin), std::istream_iterator<double>{});

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Incorrect input");
  }

  std::transform(list.begin(), list.end(), list.begin(), MulByPi());
  std::copy(list.begin(), list.end(), std::ostream_iterator<double>(std::cout << std::fixed, "\n"));
}
