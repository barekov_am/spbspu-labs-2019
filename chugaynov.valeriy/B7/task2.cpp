#include "figures.hpp"
#include <iterator>

#include <list>
#include <algorithm>

void task2()
{
  std::list<ShapePtr> shapeArr(std::istream_iterator<ShapePtr>(std::cin), std::istream_iterator<ShapePtr>{});

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Incorrect input of shape");
  }

  std::cout << "Original:\n";
  std::copy(shapeArr.begin(), shapeArr.end(), std::ostream_iterator<ShapePtr>(std::cout, "\n"));


  shapeArr.sort([](const ShapePtr& first, const ShapePtr& second) {return first->isMoreLeft(*second); });
  std::cout << "Left-Right:\n";
  std::copy(shapeArr.begin(), shapeArr.end(), std::ostream_iterator<ShapePtr>(std::cout, "\n"));
  

  shapeArr.reverse();
  std::cout << "Right-Left:\n";
  std::copy(shapeArr.begin(), shapeArr.end(), std::ostream_iterator<ShapePtr>(std::cout, "\n"));


  shapeArr.sort([](const ShapePtr& first, const ShapePtr& second) {return first->isUpper(*second); });
  std::cout << "Top-Bottom:\n";
  std::copy(shapeArr.begin(), shapeArr.end(), std::ostream_iterator<ShapePtr>(std::cout, "\n"));
  

  shapeArr.reverse();
  std::cout << "Bottom-Top:\n";
  std::copy(shapeArr.begin(), shapeArr.end(), std::ostream_iterator<ShapePtr>(std::cout, "\n"));
}

