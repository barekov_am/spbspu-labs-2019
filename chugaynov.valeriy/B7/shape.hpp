#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>

#include "point.hpp"

class Shape
{
public:
  Shape(const Point& point);
  virtual ~Shape() = default;

  bool isMoreLeft(const Shape& shape) const;
  bool isUpper(const Shape& shape) const;
  virtual void draw(std::ostream& out) const = 0;

protected:
  Point center_;
};

using ShapePtr = std::shared_ptr<Shape>;

std::ostream& operator <<(std::ostream& out, const ShapePtr& shape);
std::istream& operator >>(std::istream& in, ShapePtr& shape);

#endif
