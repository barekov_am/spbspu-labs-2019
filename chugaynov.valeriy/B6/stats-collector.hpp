#ifndef STATS_COLLECTOR_HPP
#define STATS_COLLECTOR_HPP

struct StatsCollector
{
  int max;
  int min;
  double mean;
  int positive;
  int negative;
  long int evenSum;
  long int oddSum;
  bool firstEqLast;
  int size;

  StatsCollector();

  void operator ()(int num);

private:
  int first;
};

#endif
