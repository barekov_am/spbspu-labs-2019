#include "shape-algorithms.hpp"

#include <iostream>
#include <iterator>
#include <algorithm>

void task2()
{
  std::vector<Shape> shapes(std::istream_iterator<Shape>(std::cin), std::istream_iterator<Shape>());

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::runtime_error("Invalid entry of shapes");
  }

  int verticesAmount = 0;
  for (auto it : shapes)
  {
    verticesAmount += it.size();
  }

  int trianglesAmount = std::count_if(shapes.begin(), shapes.end(),
    [](const Shape& shape) {return getType(shape) == TRIANGLE; });

  int squaresAmount = std::count_if(shapes.begin(), shapes.end(),
    [](const Shape& shape) {return getType(shape) == SQUARE; });

  int rectanglesAmount = std::count_if(shapes.begin(), shapes.end(),
    [](const Shape& shape) {return getType(shape) == RECTANGLE; }) + squaresAmount;

  std::cout << "Vertices: " << verticesAmount << "\nTriangles: " << trianglesAmount
            << "\nSquares: " << squaresAmount << "\nRectangles: " << rectanglesAmount;


  shapes.erase(std::remove_if(shapes.begin(), shapes.end(),
    [](const Shape& shape) {return getType(shape) == PENTAGON; }), shapes.end());


  Shape points;
  std::transform(shapes.begin(), shapes.end(), std::back_inserter(points),
    [](const Shape& shape) {return shape.front(); });
  std::cout << "\nPoints: ";
  std::copy(points.begin(), points.end(), std::ostream_iterator<Point>(std::cout, " "));



  std::sort(shapes.begin(), shapes.end(),
    [](const Shape& shape1, const Shape& shape2) {return getType(shape2) > getType(shape1); });

  std::cout << "\nShapes:\n";
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator<Shape>(std::cout));
}

