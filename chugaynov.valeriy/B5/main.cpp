﻿#include <iostream>

void task1();
void task2();

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      throw std::invalid_argument("Invalid number of arguments");
    }
    char* end = nullptr;
    const int task = std::strtol(argv[1], &end, 10);
    if (*end || task > 2 || task < 1)
    {
      throw std::invalid_argument("Incorrect task number");
    }

    switch (task)
    {
    case 1:
      task1();
      break;
    case 2:
      task2();
      break;
    }
  }

  catch (std::exception& exc)
  {
    std::cerr << exc.what() << "\n";
    return 1;
  }
}

