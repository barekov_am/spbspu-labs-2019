﻿#ifndef SHAPE_ALGORITHMS_HPP
#define SHAPE_ALGORITHMS_HPP

#include <vector>
#include "point.hpp"

using Shape = std::vector<Point>;

std::istream& operator>>(std::istream& in, Shape& shape);
std::ostream& operator<<(std::ostream& out, const Shape& shape);

enum ShapeType
{
  TRIANGLE,
  SQUARE,
  RECTANGLE,
  PENTAGON,
  DEFAULT
};

ShapeType getType(const Shape& shape);

#endif
