#include "shape-algorithms.hpp"

#include <algorithm>
#include <sstream>
#include <string>
#include <iterator>

#include "point.hpp"
#include "manipulators.hpp"


const int TRIANGLE_TOPS = 3;
const int QUADRANGLE_TOPS = 4;
const int PENTAGON_TOPS = 5;


std::istream& operator>>(std::istream& in, Shape& shape)
{
  std::string pointInStr;
  std::getline(in, pointInStr);
  std::istringstream iss(pointInStr);

  while ((iss >> blank).eof())
  {
    if (in.eof())
    {
      in.setstate(std::ios_base::failbit);
      return in;
    }
    std::getline(in, pointInStr);
    iss = std::istringstream(pointInStr);
  }

  int numberOfVertices;
  iss >> numberOfVertices >> blank;
  if (!iss.eof() && !iss.fail() && numberOfVertices > 0)
  {
    Shape tempShape;
    std::copy_n(std::istream_iterator<Point>(iss), numberOfVertices, std::back_inserter(tempShape));
    if ((iss >> blank).eof() && !iss.fail())
    {
      shape = tempShape;
      return in;
    }
  }
  in.clear(std::ios_base::failbit);
  return in;
}

std::ostream& operator<<(std::ostream& out, const Shape& shape)
{
  out << shape.size() << " ";
  std::copy(shape.begin(), shape.end(), std::ostream_iterator<Point>(out, " "));
  return out << "\n";
}

ShapeType getType(const Shape& shape)
{
  const int size = shape.size();
  switch (size)
  {
  case TRIANGLE_TOPS:
    return TRIANGLE;
  case QUADRANGLE_TOPS:
  {
    const int s1 = squareDistance(shape.at(0), shape.at(1));
    const int s2 = squareDistance(shape.at(0), shape.at(2));
    const int s3 = squareDistance(shape.at(0), shape.at(3));
    const int s4 = squareDistance(shape.at(1), shape.at(2));
    const int s5 = squareDistance(shape.at(1), shape.at(3));
    const int s6 = squareDistance(shape.at(2), shape.at(3));
    if (s1 == s6 && s3 == s4 && s2 == s5)
    {
      if (2 * s1 == s2 || 2 * s2 == s1 || 2 * s1 == s3)
      {
        return SQUARE;
      }
      return RECTANGLE;
    }
    return DEFAULT;
  }
  case PENTAGON_TOPS:
    return PENTAGON;
  default:
    return DEFAULT;
  }
}

