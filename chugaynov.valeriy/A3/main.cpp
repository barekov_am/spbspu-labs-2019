#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

void printFrameRectData(const chugaynov::CompositeShape& compShape)
{
  std::cout << "\nFrame rectangle parameters:";
  std::cout << "\twidth = " << compShape.getFrameRect().width;
  std::cout << "\theight = " << compShape.getFrameRect().height;
  std::cout << "\tposition = (" << compShape.getFrameRect().pos.x;
  std::cout << ", " << compShape.getFrameRect().pos.y << ")" << std::endl;
}

int main()
{
  try
  {
    chugaynov::CompositeShape compShape;
    chugaynov::Circle circ1{ { 0, 2 }, 12 };
    chugaynov::Circle circ2{ { 48, 12 }, 36 };
    chugaynov::Rectangle rect1{ { 2, 4 }, 12, 50 };
    chugaynov::Rectangle rect2{ { 11, 22 }, 94, 4 };

    auto pCirc1(std::make_shared<chugaynov::Circle>(circ1));
    auto pCirc2(std::make_shared<chugaynov::Circle>(circ2));
    auto pRect1(std::make_shared<chugaynov::Rectangle>(rect1));
    auto pRect2(std::make_shared<chugaynov::Rectangle>(rect2));

    compShape.insert(pRect2, 0);
    compShape.pushBack(pCirc1);
    compShape.insert(pRect1, 1);
    compShape.pushBack(pCirc2);

    chugaynov::CompositeShape compShape2 = compShape;
    std::cout << "Total area compShape before transformations = " << compShape.getArea() << std::endl;
    printFrameRectData(compShape);

    compShape.move({ 100, 200 });
    printFrameRectData(compShape);

    compShape.move(10, 20);
    printFrameRectData(compShape);

    compShape.scale(5);
    std::cout << "\nCompShape parameters after scaling";
    printFrameRectData(compShape);
    std::cout << "Total area after scaling = " << compShape.getArea() << std::endl;

    compShape.popBack();
    compShape.erase(0);
    compShape.shrinkToFit();
    std::cout << "\nCompShape after erase(0) and popBack:";
    printFrameRectData(compShape);
    std::cout << "Total area this compShape = " << compShape.getArea() << std::endl;

    compShape.clear();

    std::cout << "\nCompShape2, equal original compShape after transformations and clearing compShape:";
    printFrameRectData(compShape2);
    std::cout << "Total area this compShape2 = " << compShape2.getArea() << std::endl;

    compShape = std::move(compShape2);
    std::cout << "\nTotal area compShape2 after moving = " << compShape2.getArea() << std::endl;
    std::cout << "Total area compShape after moving = " << compShape.getArea() << std::endl;
  }

  catch (const std::exception& ex)
  {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  return 0;
}
