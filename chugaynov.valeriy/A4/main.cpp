#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "distribution.hpp"

int main()
{
  try
  {
    std::cout << "Splitting demonstration:\n";
    chugaynov::CompositeShape compShape;
    chugaynov::Circle circ1{ { 0, 10 }, 10 };
    chugaynov::Circle circ2{ { 40, 10 }, 20 };
    chugaynov::Rectangle rect1{ { 2, 4 }, 12, 10 };
    chugaynov::Rectangle rect2{ { 11, 22 }, 12, 40 };

    auto pCirc1(std::make_shared<chugaynov::Circle>(circ1));
    auto pCirc2(std::make_shared<chugaynov::Circle>(circ2));
    auto pRect1(std::make_shared<chugaynov::Rectangle>(rect1));
    auto pRect2(std::make_shared<chugaynov::Rectangle>(rect2));
    
    compShape.pushBack(pRect2);
    compShape.pushBack(pRect1);
    compShape.pushBack(pCirc1);
    compShape.pushBack(pCirc2);

    chugaynov::CompositeShape compShape2(compShape);
    auto pCompShape2(std::make_shared<chugaynov::CompositeShape>(compShape2));

    compShape.insert(pCompShape2, 2);
    chugaynov::Matrix matrx(chugaynov::distribute(compShape));

    matrx.printMatrix();

    std::cout << "\n\nRotation demonstration:\n";

    compShape.printInfo();
    compShape.rotate(45);
    std::cout << "After 45 degree rotation:\n";
    compShape.printInfo();

    circ1.printInfo();
    circ1.rotate(60);
    std::cout << "After 60 degree rotation(no changes):\n";
    rect1.printInfo();

    rect1.printInfo();
    rect1.rotate(-120);
    std::cout << "After -120 degree rotation:\n";
    rect1.printInfo();
  }

  catch (const std::exception& ex)
  {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  return 0;
}
