#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>
#include <sstream>

#include "data-struct-algorithms.hpp"

int main()
{
  try
  {
    std::vector<DataStruct> vector;
    DataStruct dataStruct;
    std::string str;

    while (std::getline(std::cin, str))
    {
      std::stringstream newLine(str);
      newLine >> dataStruct;
      vector.push_back(dataStruct);
    }
    std::sort(vector.begin(), vector.end(), ascendingSort);
    std::copy(vector.begin(), vector.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));
  }

  catch (const std::exception& except)
  {
    std::cerr << except.what() << "\n";
    return 1;
  }
}

