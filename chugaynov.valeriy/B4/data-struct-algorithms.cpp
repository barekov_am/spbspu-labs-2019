#include "data-struct-algorithms.hpp"

#include <iostream>

const int UPPER_LIMIT = 5;
const int LOWER_LIMIT = -5;

std::ostream& operator<<(std::ostream& out, const DataStruct& dataStruct)
{
  return out << dataStruct.key1 << "," << dataStruct.key2 << "," << dataStruct.str;
}

std::istream& operator>>(std::istream& in, DataStruct& dataStruct)
{
  in >> dataStruct.key1 >> std::ws;
  if (in.get() != ',' || dataStruct.key1 > UPPER_LIMIT || dataStruct.key1 < LOWER_LIMIT)
  {
    throw std::runtime_error("Invalid first key\n");
  }

  in >> dataStruct.key2 >> std::ws;
  if (in.get() != ',' || dataStruct.key2 > UPPER_LIMIT || dataStruct.key2 < LOWER_LIMIT)
  {
    throw std::runtime_error("Invalid second key\n");
  }

  std::getline(in >> std::ws, dataStruct.str);

  return in;
}

bool ascendingSort(const DataStruct& left, const DataStruct& right)
{
  if (left.key1 != right.key1)
  {
    return left.key1 < right.key1;
  }
  if (left.key2 != right.key2)
  {
    return left.key2 < right.key2;
  }
  return left.str.size() < right.str.size();
}
