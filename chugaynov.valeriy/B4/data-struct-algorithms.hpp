#ifndef DATA_STRUCT_ALGORITHMS_HPP
#define DATA_STRUCT_ALGORITHMS_HPP

#include <string>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

bool ascendingSort(const DataStruct& left, const DataStruct& right);
std::ostream& operator<<(std::ostream& out, const DataStruct& dataStruct);
std::istream& operator>>(std::istream& in, DataStruct& dataStruct);

#endif
