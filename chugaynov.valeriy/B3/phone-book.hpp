#ifndef PHONE_BOOK_HPP
#define PHONE_BOOK_HPP

#include <string>
#include <list>
#include <unordered_map>

struct record_t
{
  std::string number;
  std::string name;
};

class PhoneBook
{
public:
  using iterator = std::list<record_t>::iterator;
  using markIter = std::unordered_map<std::string, iterator>::iterator;

  bool view(const std::string& bookMark, record_t& neededRecord);
  bool addMark(const std::string& bookMark, const std::string& newBookMark);

  bool advance(const std::string& bookMark, int steps);
  bool advanceToFirst(const std::string& bookMark);
  bool advanceToLast(const std::string& bookMark);
  bool next(const std::string& bookMark);
  bool prev(const std::string& bookMark);

  bool insertBefore(const record_t& record, const std::string& bookMark);
  bool insertAfter(const record_t& record, const std::string& bookMark);
  void pushBack(const record_t& record);
  void pushFront(const record_t& record);

  bool remove(const std::string& bookMark);
  bool replaceRecord(const record_t& record, const std::string& bookMark);
  bool empty() const;

private:
  std::list<record_t> records_;
  std::unordered_map<std::string, iterator> marks_;
};

#endif

