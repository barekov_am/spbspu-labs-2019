#include "phone-book.hpp"

bool PhoneBook::view(const std::string& bookMark, record_t& neededRecord)
{
  const markIter bookMarkIter = marks_.find(bookMark);
  if (bookMarkIter != marks_.end())
  {
    neededRecord = *(bookMarkIter->second);
    return true;
  }
  return false;
}

bool PhoneBook::addMark(const std::string& bookMark, const std::string& newBookMark)
{
  const markIter bookMarkIter = marks_.find(bookMark);
  if (bookMarkIter != marks_.end())
  {
    marks_.insert(std::make_pair(newBookMark, bookMarkIter->second));
    return true;
  }
  return false;
}

bool PhoneBook::advance(const std::string& bookMark, int steps)
{
  const markIter bookMarkIter = marks_.find(bookMark);
  if (bookMarkIter != marks_.end())
  {
    while (steps > 0 && bookMarkIter->second != std::prev(records_.end()))
    {
      --steps;
      ++(bookMarkIter->second);
    }
    while (steps < 0 && bookMarkIter->second != records_.begin())
    {
      ++steps;
      --(bookMarkIter->second);
    }
    return true;
  }
  return false;
}

bool PhoneBook::advanceToFirst(const std::string& bookMark)
{
  const markIter bookMarkIter = marks_.find(bookMark);
  if (bookMarkIter != marks_.end())
  {
    bookMarkIter->second = records_.begin();
    return true;
  }
  return false;
}

bool PhoneBook::advanceToLast(const std::string& bookMark)
{
  const markIter bookMarkIter = marks_.find(bookMark);
  if (bookMarkIter != marks_.end())
  {
    bookMarkIter->second = std::prev(records_.end());
    return true;
  }
  return false;
}

bool PhoneBook::next(const std::string& bookMark)
{
  const markIter bookMarkIter = marks_.find(bookMark);
  if (bookMarkIter != marks_.end())
  {
    const iterator recordIter = bookMarkIter->second;
    if (recordIter != std::prev(records_.end()))
    {
      bookMarkIter->second = std::next(recordIter);
    }
    return true;
  }
  return false;
}

bool PhoneBook::prev(const std::string& bookMark)
{
  const markIter bookMarkIter = marks_.find(bookMark);
  if (bookMarkIter != marks_.end())
  {
    const iterator recordIter = bookMarkIter->second;
    if (recordIter != records_.begin())
    {
      bookMarkIter->second = std::prev(recordIter);
    }
    return true;
  }
  return false;
}

bool PhoneBook::insertBefore(const record_t& record, const std::string& bookMark)
{
  if (records_.empty())
  {
    records_.push_back(record);
    marks_.insert(std::make_pair("current", records_.begin()));
  }
  else
  {
    const markIter bookMarkIter = marks_.find(bookMark);
    if (bookMarkIter == marks_.end())
    {
      return false;
    }
    records_.insert(bookMarkIter->second, record);
  }
  return true;
}

bool PhoneBook::insertAfter(const record_t& record, const std::string& bookMark)
{
  if (records_.empty())
  {
    records_.push_back(record);
    marks_.insert(std::make_pair("current", records_.begin()));
  }
  else
  {
    const markIter bookMarkIter = marks_.find(bookMark);
    if (bookMarkIter == marks_.end())
    {
      return false;
    }
    records_.insert(std::next(bookMarkIter->second), record);
  }
  return true;
}

void PhoneBook::pushBack(const record_t& record)
{
  records_.push_back(record);
  if (records_.size() == 1)
  {
    marks_.insert(std::make_pair("current", records_.begin()));
  }
}

void PhoneBook::pushFront(const record_t& record)
{
  records_.push_front(record);
  if (records_.size() == 1)
  {
    marks_.insert(std::make_pair("current", records_.begin()));
  }
}

bool PhoneBook::remove(const std::string& bookMark)
{
  const markIter bookMarkIter = marks_.find(bookMark);
  if (bookMarkIter != marks_.end())
  {
    const iterator recordIter = bookMarkIter->second;
    for (auto it = marks_.begin(); it != marks_.end(); ++it)
    {
      if (it->second == recordIter)
      {
        if (it->second != std::prev(records_.end()))
        {
          ++(it->second);
        }
        else if (records_.size() != 1)
        {
          --(it->second);
        }
        else
        {
          marks_.clear();
          break;
        }
      }
    }
    records_.erase(recordIter);
    return true;
  }
  return false;
}

bool PhoneBook::replaceRecord(const record_t& record, const std::string& bookMark)
{
  const markIter bookMarkIter = marks_.find(bookMark);
  if (bookMarkIter != marks_.end())
  {
    *(bookMarkIter->second) = record;
    return true;
  }
  return false;
}

bool PhoneBook::empty() const
{
  return records_.empty();
}

