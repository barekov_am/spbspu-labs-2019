#include "commands.hpp"

#include <functional>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <cctype>

const std::string INVALID_COMMAND = "<INVALID COMMAND>\n";
const std::string INVALID_BOOKMARK = "<INVALID BOOKMARK>\n";
const std::string EMPTY = "<EMPTY>\n";
const std::string INVALID_STEP = "<INVALID STEP>\n";

void Commands::execute(std::stringstream& command)
{
  std::string commandName;
  command >> commandName >> std::ws;
  if (commandName == "add")
  {
    comAdd(command);
  }
  else if (commandName == "store")
  {
    comStore(command);
  }
  else if (commandName == "insert")
  {
    comInsert(command);
  }
  else if (commandName == "delete")
  {
    comDelete(command);
  }
  else if (commandName == "show")
  {
    comShow(command);
  }
  else if (commandName == "move")
  {
    comMove(command);
  }
  else if (!commandName.empty())
  {
    std::cout << INVALID_COMMAND;
  }
}

std::string Commands::extractName(std::string& name) const
{
  if ((name.front() == '\"') && (name.back() == '\"'))
  {
    std::istringstream nameStream(name);
    nameStream >> std::quoted(name);
    return name;
  }
  return "";
}

bool Commands::checkNumber(const std::string& number) const
{
  auto it = number.cbegin();
  while (it != number.cend() && std::isdigit(*it))
  {
    ++it;
  }
  return !number.empty() && it == number.cend();
}

bool Commands::checkMarkName(const std::string& markName) const
{
  auto it = markName.cbegin();
  if ((*it == '-' || isalnum(*it)) && !markName.empty())
  {
    for (; it < markName.cend() && (isalnum(*it) || *it == '-'); ++it);
    if (it == markName.cend())
    {
      return true;
    }
  }
  return false;
}

void Commands::comAdd(std::stringstream& args)
{
  std::string number;
  std::string name;
  args >> number >> std::ws;
  std::getline(args, name);
  if (args.eof() && checkNumber(number) && !extractName(name).empty())
  {
    phB_.pushBack(record_t{ number,name });
  }
  else
  {
    std::cout << INVALID_COMMAND;
  }
}

void Commands::comStore(std::stringstream& args)
{
  std::string markName;
  std::string newMarkName;
  args >> markName >> newMarkName >> std::ws;
  if (args.eof() && checkMarkName(markName) && checkMarkName(newMarkName))
  {
    if (!phB_.addMark(markName, newMarkName))
    {
      std::cout << INVALID_BOOKMARK;
    }
  }
  else
  {
    std::cout << INVALID_COMMAND;
  }
}

void Commands::comInsert(std::stringstream& args)
{
  std::string insertPlace;
  args >> insertPlace;


  if (insertPlace == "before" || insertPlace == "after")
  {
    std::string markName;
    args >> markName;
    if (checkMarkName(markName) && !args.eof())
    {
      std::string number;
      std::string name;
      args >> number >> std::ws;
      std::getline(args, name);
      if (args.eof() && checkNumber(number) && !extractName(name).empty())
      {
        insertPlace == "before"
          ? phB_.insertBefore(record_t{ number,name }, markName)
          : phB_.insertAfter(record_t{ number,name }, markName);
      }


      else
      {
        std::cout << INVALID_COMMAND;
      }
    }
    else
    {
      std::cout << INVALID_BOOKMARK;
    }
  }
  else
  {
    std::cout << INVALID_COMMAND;
  }
}

void Commands::comDelete(std::stringstream& args)
{
  std::string markName;
  args >> markName;
  if (args.eof() && checkMarkName(markName))
  {
    if (!phB_.remove(markName))
    {
      std::cout << INVALID_BOOKMARK;
    }
  }
  else
  {
    std::cout << INVALID_COMMAND;
  }
}

void Commands::comShow(std::stringstream& args)
{
  std::string markName;
  args >> markName >> std::ws;

  if (args.eof() && checkMarkName(markName))
  {
    record_t record;
    if (phB_.view(markName, record))
    {
      std::cout << record.number << " " << record.name << "\n";
    }
    else if (markName == "current")
    {
      std::cout << EMPTY;
    }
    else
    {
      std::cout << INVALID_BOOKMARK;
    }
  }
  else
  {
    std::cout << INVALID_COMMAND;
  }
}

void Commands::comMove(std::stringstream& args)
{
  std::string markName;
  args >> markName >> std::ws;

  if (checkMarkName(markName) && !args.eof())
  {
    std::string steps;
    args >> steps >> std::ws;

    if (!args.eof())
    {
      std::cout << INVALID_COMMAND;
    }
    if (steps == "first")
    {
      if (!phB_.advanceToFirst(markName))
      {
        std::cout << INVALID_BOOKMARK;
      }
    }

    else if (steps == "last")
    {
      if (!phB_.advanceToLast(markName))
      {
        std::cout << INVALID_BOOKMARK;
      }
    }

    else
    {
      char* end;
      const int intSteps = strtol(steps.c_str(), &end, 10);
      if (*end != '\0')
      {
        std::cout << INVALID_STEP;
        return;
      }
      phB_.advance(markName, intSteps);
    }
  }
  else
  {
    std::cout << INVALID_COMMAND;
  }
}

