#include "factorial-container.hpp"

FactorialContainer::iterator FactorialContainer::begin() const
{
  return iterator{ 1 };
}

FactorialContainer::iterator FactorialContainer::end() const
{
  return iterator{ END_INDEX };
}

FactorialContainer::const_iterator FactorialContainer::cbegin() const
{
  return const_iterator{ 1 };
}

FactorialContainer::const_iterator FactorialContainer::cend() const
{
  return const_iterator{ END_INDEX };
}

