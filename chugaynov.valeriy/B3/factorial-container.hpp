#ifndef FACTORIAL_CONTAINER_HPP
#define FACTORIAL_CONTAINER_HPP

#include <iterator>

const int END_INDEX = 11;

class FactorialContainer
{
public:

  class iterator: public std::iterator<std::bidirectional_iterator_tag, int, std::ptrdiff_t, int, int>
  {
  public:
    friend class FactorialContainer;

    int operator*() const;
    iterator& operator++();
    iterator& operator--();
    iterator operator++(int);
    iterator operator--(int);
    bool operator==(const iterator& compared) const;
    bool operator!=(const iterator& compared) const;

  private:
    iterator(int index);
    int value_;
    int index_;
  };

  using const_iterator = iterator;

  iterator begin() const;
  iterator end() const;

  const_iterator cbegin() const;
  const_iterator cend() const;
};

#endif

