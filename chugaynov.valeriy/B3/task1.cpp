#include <iostream>
#include <sstream>

#include "commands.hpp"

void task1()
{
  Commands coms;
  while (!std::cin.eof())
  {
    std::string str;
    std::getline(std::cin, str);
    std::stringstream stream(str);
    coms.execute(stream);
  }
}

