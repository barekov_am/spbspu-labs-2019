#include "factorial-container.hpp"

int FactorialContainer::iterator::operator*() const
{
  return value_;
}

FactorialContainer::iterator& FactorialContainer::iterator::operator++()
{
  if (index_ == END_INDEX)
  {
    throw std::runtime_error("Index can't to be more than MAX_INDEX\n");
  }
  value_ *= ++index_;
  return *this;
}

FactorialContainer::iterator& FactorialContainer::iterator::operator--()
{
  if (index_ == 0)
  {
    throw std::runtime_error("Index can't to be negative\n");
  }
  value_ /= index_--;
  return *this;
}

FactorialContainer::iterator FactorialContainer::iterator::operator++(int)
{
  const iterator old(*this);
  this->operator++();
  return old;
}

FactorialContainer::iterator FactorialContainer::iterator::operator--(int)
{
  const iterator old(*this);
  this->operator--();
  return old;
}

bool FactorialContainer::iterator::operator==(const iterator& compared) const
{
  return this->index_ == compared.index_;
}

bool FactorialContainer::iterator::operator!=(const iterator& compared) const
{
  return !(this->index_ == compared.index_);
}

FactorialContainer::iterator::iterator(int index) :
  index_(index)
{
  if (index_ > END_INDEX)
  {
    throw std::runtime_error("Index can't to be more than MAX_INDEX\n");
  }
  if (index_ < 0)
  {
    throw std::runtime_error("Index can't to be negative\n");
  }
  value_ = 1;
  while (index > 1)
  {
    value_ *= index--;
  }
}

