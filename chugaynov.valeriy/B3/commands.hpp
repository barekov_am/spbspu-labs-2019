#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include "phone-book.hpp"

class Commands
{
public:
  void execute(std::stringstream& command);

private:
  PhoneBook phB_;

  std::string extractName(std::string& name) const;
  bool checkNumber(const std::string& number) const;
  bool checkMarkName(const std::string& markName) const;

  void comAdd(std::stringstream& args);
  void comStore(std::stringstream& args);
  void comInsert(std::stringstream& args);
  void comDelete(std::stringstream& args);
  void comShow(std::stringstream& args);
  void comMove(std::stringstream& args);
};

#endif

