#include "tasks.hpp"

#include <vector>

void task3()
{
  std::vector<int> vector;
  int inputValue = -1;
  bool endOfInput = 0;
  while (std::cin >> inputValue)
  {
    if (inputValue == 0)
    {
      endOfInput = 1;
      break;
    }
    vector.push_back(inputValue);
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::iostream::failure("Invalid characters passed");
  }

  if (vector.empty())
  {
    return;
  }

  if (!endOfInput)
  {
    throw std::invalid_argument("There isn't zero in the end of input");
  }

  if (vector.back() == 1)
  {
    auto i = vector.begin();
    while (i != vector.end())
    {
      i = ((*i) % 2 == 0) ? vector.erase(i) : ++i;
    }
  }
  else if (vector.back() == 2)
  {
    auto i = vector.begin();
    while (i != vector.end())
    {
      i = ((*i) % 3 == 0) ? (vector.insert(++i, 3, 1) + 3) : ++i;
    }
  }
  printCollection(vector);
}
