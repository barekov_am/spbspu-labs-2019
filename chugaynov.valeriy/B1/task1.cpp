#include "tasks.hpp"

#include <vector>
#include <forward_list>

void task1(const char* dir)
{
  const direction direct = sortDirect(dir);
  std::vector<int> vector1;
  int inputValue = -1;
  while (std::cin >> inputValue)
  {
    vector1.push_back(inputValue);
  }

  if (!std::cin.eof())
  {
    throw std::iostream::failure("Invalid characters passed");
  }
  if (vector1.empty())
  {
    return;
  }
  std::vector<int> vector2{ vector1.begin(),vector1.end() };
  std::forward_list<int> fList{ vector1.begin(),vector1.end() };
  sort<ByIndexOperator>(vector1, direct);
  sort<ByMethodAt>(vector2, direct);
  sort<ByIterator>(fList, direct);
  printCollection(vector1);
  printCollection(vector2);
  printCollection(fList);
}
