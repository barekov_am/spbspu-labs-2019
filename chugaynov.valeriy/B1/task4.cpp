#include "tasks.hpp"

#include <iostream>
#include <ctime>
#include <vector>

void fillRandom(double* array, const int size)
{
  for (int i = 0; i < size; i++)
  {
    array[i] = ((std::rand() % 21) - 10) / 10.0;
  }
}

void task4(const char* dir, const char* arrSize)
{
  char* end = nullptr;
  const int size = std::strtol(arrSize, &end, 10);
  if (*end != '\0')
  {
    throw std::invalid_argument("Size of array is incorrect");
  }
  const direction direct = sortDirect(dir);
  std::vector<double> rndVector(size, 0);
  fillRandom(&rndVector[0], size);
  printCollection(rndVector);
  sort<ByIterator>(rndVector, direct);
  printCollection(rndVector);
}
