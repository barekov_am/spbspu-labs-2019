#ifndef PARTS_HPP
#define PARTS_HPP

#include <iostream>

#include "sort.hpp"

void task1(const char* dir);
void task2(const char* fileName);
void task3();
void task4(const char* dir, const char* size);

template<typename Container>
void printCollection(const Container& container)
{
  for (auto i = container.cbegin(); i != container.cend(); ++i)
  {
    std::cout << *i << " ";
  }
  std::cout << std::endl;
}

#endif
