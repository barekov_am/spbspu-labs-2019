#include "sort.hpp"

#include <cstring>

direction sortDirect(const char * direction)
{
  if (!strcmp(direction, "ascending"))
  {
    return direction::ascending;
  }
  else if (!strcmp(direction, "descending"))
  {
    return direction::descending;
  }
  else
  {
    throw std::invalid_argument("Incorrect direction");
  }
}
