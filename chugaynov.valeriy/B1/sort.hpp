#ifndef SORT_HPP
#define SORT_HPP

#include <iostream>

enum direction
{
  ascending,
  descending
};

direction sortDirect(const char* direction);

template<typename Container>
struct ByIndexOperator
{
  using index = size_t;
  static index getBegin(const Container& /*container*/)
  {
    return 0;
  }
  static index getEnd(const Container& container)
  {
    return container.size();
  }
  static typename Container::reference getElement(Container& container, const index i)
  {
    return container[i];
  }
};

template<typename Container>
struct ByMethodAt
{
  using index = size_t;
  static index getBegin(const Container& /*container*/)
  {
    return 0;
  }
  static index getEnd(const Container& container)
  {
    return container.size();
  }
  static typename Container::reference getElement(Container& container, const index i)
  {
    return container.at(i);
  }
};

template<typename Container>
struct ByIterator
{
  using index = typename Container::iterator;
  static index getBegin(Container& container)
  {
    return container.begin();
  }
  static index getEnd(Container& container)
  {
    return container.end();
  }
  static typename Container::reference getElement(Container& /*container*/, const index i)
  {
    return *i;
  }
};

template<template<typename Container> typename SortType, typename Container>
void sort(Container& container, const direction dir)
{
  using SType = SortType<Container>;
  for (typename SType::index i = SType::getBegin(container); i != SType::getEnd(container); ++i)
  {
    typename SType::index tempIndex = i;
    for (typename SType::index j = i; j != SType::getEnd(container); ++j)
    {
      if ((SType::getElement(container, tempIndex) < SType::getElement(container, j)) == dir)
      {
        tempIndex = j;
      }
    }
    std::swap(SType::getElement(container, tempIndex), SType::getElement(container, i));
  }
}

#endif
