#include "tasks.hpp"

#include <fstream>
#include <vector>
#include <memory>
#include <iostream>

const int INIT_CAPACITY = 64;
const int CAPACITY_FACTOR = 2;

void task2(const char* fileName)
{
  std::ifstream inputFile(fileName);
  if (!inputFile)
  {
    throw std::iostream::failure("File wasn't opened\n");
  }

  using charArr = std::unique_ptr<char[], decltype(&free)>;
  
  int capacity = INIT_CAPACITY * sizeof(charArr::element_type);
  int size = 0;

  charArr arr(static_cast<char*>(malloc(capacity)), &free);
  if (!arr)
  {
    throw std::runtime_error("Memory allocation error\n");
  }

  while (inputFile.get(arr[size]))
  {
    if (++size == capacity)
    {
      capacity *= CAPACITY_FACTOR;
      charArr tmp(static_cast<char*>(realloc(arr.get(), capacity)), &free);
      if (!tmp)
      {
        throw std::runtime_error("Couldn't allocate memory.\n");
      }
      arr.release();
      std::swap(arr, tmp);
    }
  }

  inputFile.close();

  if (size == 0)
  {
    return;
  }

  const std::vector<char> vector(&arr[0], &arr[size]);
  for (auto i = vector.cbegin(); i != vector.cend(); ++i)
  {
    std::cout << *i;
  }
}
