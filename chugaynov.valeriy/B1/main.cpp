#include <iostream>
#include <string>
#include <ctime>

#include "tasks.hpp"

int main(int argc, char* argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4))
    {
      throw std::invalid_argument("Number of parameters is incorrect");
    }
    char* end = nullptr;
    const int task = std::strtol(argv[1], &end, 10);
    if ((*end != '\0') || (task > 4) || (task < 1))
    {
      throw std::invalid_argument("Number of task is incorrect");
    }

    srand(time(nullptr));
    switch (task)
    {
    case 1:
      if (argc != 3)
      {
        throw std::invalid_argument("Number of parameters is incorrect");
      }
      task1(argv[2]);
      break;
    case 2:
      if (argc != 3)
      {
        throw std::invalid_argument("Number of parameters is incorrect");
      }
      task2(argv[2]);
      break;
    case 3:
      if (argc != 2)
      {
        throw std::invalid_argument("Number of parameters is incorrect");
      }
      task3();
      break;
    case 4:
      if (argc != 4)
      {
        throw std::invalid_argument("Number of parameters is incorrect");
      }
      task4(argv[2], argv[3]);
      break;
    }
  }

  catch (const std::exception& except)
  {
    std::cerr << except.what() << std::endl;
    return 1;
  }
}
