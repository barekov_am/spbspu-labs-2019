#ifndef SHAPE_HPP
#define SHAPE_HPP
#include "base-types.hpp"
#include <memory>

namespace chugaynov
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual rectangle_t getFrameRect() const = 0;
    virtual double getArea() const = 0;
    virtual void move(const point_t& point) = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void scale(double coefficient) = 0;
    virtual void rotate(double angle) = 0;
    virtual void printInfo() const = 0;
  };
  using shapePtr = std::shared_ptr<Shape>;
  using arrayOfShapePtr = std::unique_ptr<shapePtr[]>;
}
#endif
