#include "matrix.hpp"
#include <iostream>
#include <memory>
#include <algorithm>

chugaynov::Matrix::Matrix():
  cols_(0),
  rows_(0)
{
}

chugaynov::Matrix::Matrix(const Matrix& mtrx):
  shapeMatrix_(std::make_unique<shapePtr[]>(mtrx.cols_ * mtrx.rows_)),
  cols_(mtrx.cols_),
  rows_(mtrx.rows_)
{
  for (size_t i = 0; i < cols_ * rows_; i++)
  {
    shapeMatrix_[i] = mtrx.shapeMatrix_[i];
  }
}

chugaynov::Matrix::Matrix(Matrix&& mtrx):
  shapeMatrix_(std::move(mtrx.shapeMatrix_)),
  cols_(mtrx.cols_),
  rows_(mtrx.rows_)
{
  mtrx.cols_ = mtrx.rows_ = 0;
}

chugaynov::Matrix& chugaynov::Matrix::operator =(const Matrix& mtrx)
{
  if (&mtrx != this)
  {
    arrayOfShapePtr tempShapeMatrix(std::make_unique<shapePtr[]>(cols_ * rows_));
    cols_ = mtrx.cols_;
    rows_ = mtrx.rows_;
    for (size_t i = 0; i < cols_ * rows_; i++)
    {
      tempShapeMatrix[i] = mtrx.shapeMatrix_[i];
    }
    shapeMatrix_ = std::move(tempShapeMatrix);
  }
  return *this;
}

chugaynov::Matrix& chugaynov::Matrix::operator =(Matrix&& mtrx)
{
  if (&mtrx != this)
  {
    shapeMatrix_ = std::move(mtrx.shapeMatrix_);
    cols_ = mtrx.cols_;
    rows_ = mtrx.rows_;
    mtrx.cols_ = mtrx.rows_ = 0;
  }
  return *this;
}

chugaynov::arrayOfShapePtr chugaynov::Matrix::operator [](size_t row) const
{
  if (row >= rows_)
  {
    throw std::out_of_range("\nRow must be less than rows");
  }
  size_t tempLayerSize = layerSize(row);
  arrayOfShapePtr layer(std::make_unique<shapePtr[]>(tempLayerSize));
  for (size_t i = 0; i < tempLayerSize; i++)
  {
    layer[i] = shapeMatrix_[row * cols_ + i];
  }
  return layer;
}

void chugaynov::Matrix::printMatrix() const
{
  for (size_t i = 0; i < rows_; i++)
  {
    std::cout << i << " Layer:\n";
    size_t tempLayerSize = layerSize(i);
    for (size_t j = 0; j < tempLayerSize; j++)
    {
      shapeMatrix_[i * cols_ + j]->printInfo();
    }
    std::cout << "\n";
  }
}

size_t chugaynov::Matrix::layerSize(size_t layer) const
{
  if (layer >= rows_)
  {
    return 0;
  }
  for (size_t i = 0; i < cols_; i++)
  {
    if (shapeMatrix_[layer * cols_ + i] == nullptr)
    {
      return i;
    }
  }
  return cols_;
}

size_t chugaynov::Matrix::getRows() const
{
  return rows_;
}

void chugaynov::Matrix::addShape(chugaynov::shapePtr shape, size_t row, size_t col)
{
  const size_t tempCols = std::max(cols_, col + 1);
  const size_t tempRows = std::max(rows_, row + 1);

  arrayOfShapePtr tempShapeMatrix(std::make_unique<shapePtr[]>(tempCols * tempRows));

  for (size_t j = 0; j < rows_; j++)
  {
    for (size_t k = 0; k < cols_; k++)
    {
      tempShapeMatrix[j * tempCols + k] = shapeMatrix_[j * cols_ + k];
    }
  }
  tempShapeMatrix[row * tempCols + col] = shape;

  shapeMatrix_ = std::move(tempShapeMatrix);
  cols_ = tempCols;
  rows_ = tempRows;
}
