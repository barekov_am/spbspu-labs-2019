#include "boost/test/auto_unit_test.hpp"

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "distribution.hpp"

BOOST_AUTO_TEST_SUITE(testForMatrix)

BOOST_AUTO_TEST_CASE(correctFunctionLayerSize)
{
  chugaynov::CompositeShape compShape;
  chugaynov::Circle circ1{ { 0, 10 }, 10 };
  chugaynov::Circle circ2{ { 40, 10 }, 20 };
  chugaynov::Circle circ3{ { -20, -5 }, 1 };

  chugaynov::Rectangle rect1{ { 2, 4 }, 12, 10 };
  chugaynov::Rectangle rect2{ { 11, 22 }, 12, 40 };
  chugaynov::Rectangle rect3{ { 0, -16 }, 30, 30 };

  chugaynov::shapePtr pCirc1(std::make_shared<chugaynov::Circle>(circ1));
  chugaynov::shapePtr pCirc2(std::make_shared<chugaynov::Circle>(circ2));
  chugaynov::shapePtr pCirc3(std::make_shared<chugaynov::Circle>(circ3));

  chugaynov::shapePtr pRect1(std::make_shared<chugaynov::Rectangle>(rect1));
  chugaynov::shapePtr pRect2(std::make_shared<chugaynov::Rectangle>(rect2));
  chugaynov::shapePtr pRect3(std::make_shared<chugaynov::Rectangle>(rect3));

  compShape.pushBack(pRect2);
  compShape.pushBack(pRect1);
  compShape.pushBack(pCirc1);
  compShape.pushBack(pCirc2);
  compShape.pushBack(pCirc3);
  compShape.pushBack(pRect3);

  chugaynov::Matrix matrix = chugaynov::distribute(compShape);
  BOOST_CHECK_EQUAL(matrix.layerSize(0), 4);
  BOOST_CHECK_EQUAL(matrix.layerSize(1), 1);
  BOOST_CHECK_EQUAL(matrix.layerSize(2), 1);
}

BOOST_AUTO_TEST_CASE(checkThrowExceptions)
{
  chugaynov::CompositeShape compShape;
  chugaynov::Circle circ{ { 0, 10 }, 10 };
  chugaynov::shapePtr pCirc(std::make_shared<chugaynov::Circle>(circ));

  compShape.pushBack(pCirc);
  chugaynov::Matrix matrix = chugaynov::distribute(compShape);

  BOOST_CHECK_THROW(matrix[10][10], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
