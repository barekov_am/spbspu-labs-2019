#define _USE_MATH_DEFINES
#include "boost/test/auto_unit_test.hpp"
#include <cmath>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(testForCompositeShape)

const double TOLERANCE = 0.0001;

BOOST_AUTO_TEST_CASE(frameRectangleAndDistancesInvariantAfterMoving)
{
  chugaynov::Rectangle testRect{ { 1.2, 4.5 }, 12.2, 102.3 };
  chugaynov::Circle testCirc{ { 12.4, 46.1 }, 12.2 };
  chugaynov::CompositeShape cs;

  chugaynov::shapePtr pCirc(std::make_shared<chugaynov::Circle>(testCirc));
  chugaynov::shapePtr pRect(std::make_shared<chugaynov::Rectangle>(testRect));

  cs.pushBack(pRect);
  cs.pushBack(pCirc);

  const double distanceX0 = cs[0]->getFrameRect().pos.x - cs[1]->getFrameRect().pos.x;
  const double distanceY0 = cs[0]->getFrameRect().pos.y - cs[1]->getFrameRect().pos.y;
  const chugaynov::rectangle_t rect0 = cs.getFrameRect();

  cs.move(1, 2);

  const double distanceX1 = cs[0]->getFrameRect().pos.x - cs[1]->getFrameRect().pos.x;
  const double distanceY1 = cs[0]->getFrameRect().pos.y - cs[1]->getFrameRect().pos.y;
  BOOST_CHECK_CLOSE(rect0.height, cs.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE(rect0.width, cs.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(distanceX0, distanceX1, TOLERANCE);
  BOOST_CHECK_CLOSE(distanceY0, distanceY1, TOLERANCE);

  cs.move({ 17.8, 1 });

  const double distanceX2 = cs[0]->getFrameRect().pos.x - cs[1]->getFrameRect().pos.x;
  const double distanceY2 = cs[0]->getFrameRect().pos.y - cs[1]->getFrameRect().pos.y;
  BOOST_CHECK_CLOSE(rect0.height, cs.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE(rect0.width, cs.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(distanceX0, distanceX2, TOLERANCE);
  BOOST_CHECK_CLOSE(distanceY0, distanceY2, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(areaAndDistancesChangeAfterScaling)
{
  chugaynov::Rectangle testRect{ { 1.2, 4.5 }, 12.2, 102.3 };
  chugaynov::Circle testCirc{ { 12.4, 46.1 }, 12.2 };
  chugaynov::CompositeShape cs;
  chugaynov::shapePtr pCirc(std::make_shared<chugaynov::Circle>(testCirc));
  chugaynov::shapePtr pRect(std::make_shared<chugaynov::Rectangle>(testRect));

  cs.pushBack(pRect);
  cs.pushBack(pCirc);
  const double coeff = 12.4;

  const double areaBeforeScaling = cs.getArea();
  const double distanceX0 = cs[0]->getFrameRect().pos.x - cs[1]->getFrameRect().pos.x;
  const double distanceY0 = cs[0]->getFrameRect().pos.y - cs[1]->getFrameRect().pos.y;

  cs.scale(coeff);

  const double distanceX1 = cs[0]->getFrameRect().pos.x - cs[1]->getFrameRect().pos.x;
  const double distanceY1 = cs[0]->getFrameRect().pos.y - cs[1]->getFrameRect().pos.y;
  BOOST_CHECK_CLOSE(areaBeforeScaling * coeff * coeff, cs.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(distanceX0 * coeff, distanceX1, TOLERANCE);
  BOOST_CHECK_CLOSE(distanceY0 * coeff, distanceY1, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(correctCopyConstructorAndOperator)
{
  chugaynov::Circle testCirc{ { 1, 2 }, 3 };
  chugaynov::Rectangle testRect{ { -12, -200 }, 42, 1 };
  chugaynov::CompositeShape cs;
  chugaynov::shapePtr pCirc(std::make_shared<chugaynov::Circle>(testCirc));
  chugaynov::shapePtr pRect(std::make_shared<chugaynov::Rectangle>(testRect));

  cs.pushBack(pRect);
  cs.pushBack(pCirc);

  const double originalArea = cs.getArea();
  const chugaynov::rectangle_t originalRect = cs.getFrameRect();

  chugaynov::CompositeShape cs2(cs);
  chugaynov::rectangle_t copyCompShapeRect = cs2.getFrameRect();
  BOOST_CHECK_CLOSE(originalArea, cs2.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.height, copyCompShapeRect.height, TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.width, copyCompShapeRect.width, TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.pos.x, copyCompShapeRect.pos.x, TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.pos.y, copyCompShapeRect.pos.y, TOLERANCE);

  chugaynov::CompositeShape cs3;
  cs3 = cs;
  copyCompShapeRect = cs3.getFrameRect();
  BOOST_CHECK_CLOSE(originalArea, cs3.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.height, copyCompShapeRect.height, TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.width, copyCompShapeRect.width, TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.pos.x, copyCompShapeRect.pos.x, TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.pos.y, copyCompShapeRect.pos.y, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(correctMoveConstructorAndOperator)
{
  chugaynov::Circle testCirc{ { 1, 2 }, 3 };
  chugaynov::Rectangle testRect{ { -12, -200 }, 42, 1 };
  chugaynov::CompositeShape cs;
  chugaynov::shapePtr pCirc(std::make_shared<chugaynov::Circle>(testCirc));
  chugaynov::shapePtr pRect(std::make_shared<chugaynov::Rectangle>(testRect));

  cs.pushBack(pRect);
  cs.pushBack(pCirc);

  const double originalArea = cs.getArea();
  const chugaynov::rectangle_t originalRect = cs.getFrameRect();

  chugaynov::CompositeShape cs2(std::move(cs));
  chugaynov::rectangle_t moveCompShapeRect = cs2.getFrameRect();
  BOOST_CHECK_CLOSE(originalArea, cs2.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.height, moveCompShapeRect.height, TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.width, moveCompShapeRect.width, TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.pos.x, moveCompShapeRect.pos.x, TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.pos.y, moveCompShapeRect.pos.y, TOLERANCE);

  chugaynov::CompositeShape cs3;
  cs3 = std::move(cs2);
  moveCompShapeRect = cs3.getFrameRect();
  BOOST_CHECK_CLOSE(originalArea, cs3.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.height, moveCompShapeRect.height, TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.width, moveCompShapeRect.width, TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.pos.x, moveCompShapeRect.pos.x, TOLERANCE);
  BOOST_CHECK_CLOSE(originalRect.pos.y, moveCompShapeRect.pos.y, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(checkingCorrectOfTheIndexOperator)
{
  chugaynov::Circle testCirc{ { 1, 2 }, 10 };
  chugaynov::CompositeShape cs;

  chugaynov::shapePtr pCirc(std::make_shared<chugaynov::Circle>(testCirc));

  cs.insert(pCirc, 0);
  BOOST_CHECK_CLOSE(cs[0]->getArea(), testCirc.getArea(), TOLERANCE);

  const chugaynov::rectangle_t tempRectForCircleInCS = cs[0]->getFrameRect();
  const chugaynov::rectangle_t tempRectForCircleOutCS = testCirc.getFrameRect();

  BOOST_CHECK_CLOSE(tempRectForCircleInCS.height, tempRectForCircleOutCS.height, TOLERANCE);
  BOOST_CHECK_CLOSE(tempRectForCircleInCS.width, tempRectForCircleOutCS.width, TOLERANCE);
  BOOST_CHECK_CLOSE(tempRectForCircleInCS.pos.x, tempRectForCircleOutCS.pos.x, TOLERANCE);
  BOOST_CHECK_CLOSE(tempRectForCircleInCS.pos.y, tempRectForCircleOutCS.pos.y, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(checkingRotation)
{
  const chugaynov::point_t pointRect1{ 10, 10 };
  const chugaynov::point_t pointCirc1{ -20, -10 };
  chugaynov::Rectangle testRect{ pointRect1, 20, 10 };
  chugaynov::Circle testCirc{ pointCirc1, 20 };

  chugaynov::shapePtr pCirc(std::make_shared<chugaynov::Circle>(testCirc));
  chugaynov::shapePtr pRect(std::make_shared<chugaynov::Rectangle>(testRect));

  chugaynov::CompositeShape cs;
  cs.pushBack(pRect);
  cs.pushBack(pCirc);

  const chugaynov::point_t center = cs.getFrameRect().pos;

  const double areaBefore = cs.getArea();

  const double alpha = 45;
  const double cosAngle = cos(M_PI * alpha / 180);
  const double sinAngle = sin(M_PI * alpha / 180);
  cs.rotate(alpha);

  const double pointRect2X = center.x + (pointRect1.x - center.x) * cosAngle
                                      - (pointRect1.y - center.y) * sinAngle;
  const double pointRect2Y = center.y + (pointRect1.x - center.x) * sinAngle
                                      + (pointRect1.y - center.y) * cosAngle;
  const double pointCirc2X = center.x + (pointCirc1.x - center.x) * cosAngle
                                      - (pointCirc1.y - center.y) * sinAngle;
  const double pointCirc2Y = center.y + (pointCirc1.x - center.x) * sinAngle
                                      + (pointCirc1.y - center.y) * cosAngle;

  BOOST_CHECK_CLOSE(pointRect2X, cs[0]->getFrameRect().pos.x, TOLERANCE);
  BOOST_CHECK_CLOSE(pointRect2Y, cs[0]->getFrameRect().pos.y, TOLERANCE);
  BOOST_CHECK_CLOSE(pointCirc2X, cs[1]->getFrameRect().pos.x, TOLERANCE);
  BOOST_CHECK_CLOSE(pointCirc2Y, cs[1]->getFrameRect().pos.y, TOLERANCE);

  BOOST_CHECK_CLOSE(areaBefore, cs.getArea(), TOLERANCE);
}

BOOST_AUTO_TEST_CASE(checkThrowExceptions)
{
  chugaynov::CompositeShape cs;
  chugaynov::Circle testCirc{ { 1, 2 }, 3 };
  chugaynov::shapePtr pCirc(std::make_shared<chugaynov::Circle>(testCirc));

  BOOST_CHECK_THROW(cs[10], std::out_of_range);
  BOOST_CHECK_THROW(cs.scale(-0.2), std::invalid_argument);
  BOOST_CHECK_THROW(cs.pushBack(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(cs.insert(nullptr, 0), std::invalid_argument);
  BOOST_CHECK_THROW(cs.insert(pCirc, -1), std::out_of_range);
  BOOST_CHECK_THROW(cs.insert(pCirc, 3), std::out_of_range);
  BOOST_CHECK_THROW(cs.erase(-1), std::out_of_range);
  BOOST_CHECK_THROW(cs.erase(4), std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
