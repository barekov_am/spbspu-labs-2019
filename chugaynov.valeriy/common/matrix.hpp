#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "composite-shape.hpp"

namespace chugaynov
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix& mtrx);
    Matrix(Matrix&& mtrx);
    ~Matrix() = default;

    Matrix& operator =(const Matrix& mtrx);
    Matrix& operator =(Matrix&& mtrx);
    arrayOfShapePtr operator [](size_t row) const;

    void printMatrix() const;
    size_t layerSize(size_t layer) const;
    size_t getRows() const;
  private:
    arrayOfShapePtr shapeMatrix_;
    size_t cols_;
    size_t rows_;
    void addShape(shapePtr shape, size_t row, size_t col);

    friend Matrix distribute(const CompositeShape& compShape);
  };
}
#endif
