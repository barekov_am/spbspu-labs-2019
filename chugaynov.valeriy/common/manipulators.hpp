#ifndef MANIPULATORS_HPP
#define MANIPULATORS_HPP

#include <iostream>

std::istream& blank(std::istream& in);

std::istream& space(std::istream& in);

#endif
