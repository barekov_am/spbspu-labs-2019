#ifndef DISTRIBUTION_HPP
#define DISTRIBUTION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace chugaynov
{
  bool intersect(const shapePtr shape1, const shapePtr shape2);
  Matrix distribute(const CompositeShape& compShape);
  Matrix distribute(const std::unique_ptr<shapePtr[]> shapeArray, size_t size);
}

#endif
