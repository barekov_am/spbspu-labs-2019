#include "boost/test/auto_unit_test.hpp"

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "distribution.hpp"

BOOST_AUTO_TEST_SUITE(testDistribution)

BOOST_AUTO_TEST_CASE(distributionValidation)
{
  chugaynov::CompositeShape compShape;
  chugaynov::Circle circ1{ { 0, 10 }, 10 };
  chugaynov::Circle circ2{ { 40, 10 }, 20 };
  chugaynov::Rectangle rect1{ { 2, 4 }, 12, 10 };
  chugaynov::Rectangle rect2{ { 11, 22 }, 12, 40 };

  chugaynov::shapePtr pCirc1(std::make_shared<chugaynov::Circle>(circ1));
  chugaynov::shapePtr pCirc2(std::make_shared<chugaynov::Circle>(circ2));
  chugaynov::shapePtr pRect1(std::make_shared<chugaynov::Rectangle>(rect1));
  chugaynov::shapePtr pRect2(std::make_shared<chugaynov::Rectangle>(rect2));

  compShape.pushBack(pRect2);
  compShape.pushBack(pRect1);
  compShape.pushBack(pCirc1);
  compShape.pushBack(pCirc2);

  chugaynov::Matrix matrix1 = chugaynov::distribute(compShape);
  BOOST_CHECK_EQUAL(matrix1[0][0], pRect2);
  BOOST_CHECK_EQUAL(matrix1[0][1], pCirc2);
  BOOST_CHECK_EQUAL(matrix1[1][0], pRect1);
  BOOST_CHECK_EQUAL(matrix1[2][0], pCirc1);


  size_t size = 4;
  chugaynov::arrayOfShapePtr shapeArray(std::make_unique<chugaynov::shapePtr[]>(size));
  shapeArray[0] = pRect2;
  shapeArray[1] = pRect1;
  shapeArray[2] = pCirc1;
  shapeArray[3] = pCirc2;

  chugaynov::Matrix matrix2 = chugaynov::distribute(std::move(shapeArray), size);
  BOOST_CHECK_EQUAL(matrix1[0][0], pRect2);
  BOOST_CHECK_EQUAL(matrix1[0][1], pCirc2);
  BOOST_CHECK_EQUAL(matrix1[1][0], pRect1);
  BOOST_CHECK_EQUAL(matrix1[2][0], pCirc1);
}

BOOST_AUTO_TEST_CASE(correctCheckIntersection)
{
  chugaynov::Circle circ{ { 0, 0 }, 20 };
  chugaynov::Rectangle rect{ { 0, 0 }, 10, 10 };
  chugaynov::shapePtr pCirc = std::make_shared<chugaynov::Circle>(circ);
  chugaynov::shapePtr pRect = std::make_shared<chugaynov::Rectangle>(rect);

  BOOST_CHECK_EQUAL(chugaynov::intersect(pRect, pCirc), true);
  pRect->move(100, 100);
  BOOST_CHECK_EQUAL(chugaynov::intersect(pRect, pCirc), false);
}

BOOST_AUTO_TEST_SUITE_END()
