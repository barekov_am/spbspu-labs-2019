#include "boost/test/auto_unit_test.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testForCircle)

const double TOLERANCE = 0.0001;

BOOST_AUTO_TEST_CASE(circleInvariantAfterMove)
{
  const double radius = 12.2;
  chugaynov::Circle testCirc({ 17.2, 1.39 }, radius);
  const double circleAreaBeforeMove = testCirc.getArea();
  testCirc.move(17.8, 1);
  BOOST_CHECK_CLOSE(circleAreaBeforeMove, testCirc.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(radius, testCirc.getRadius(), TOLERANCE);
  testCirc.move({ 17.8, 1 });
  BOOST_CHECK_CLOSE(circleAreaBeforeMove, testCirc.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(radius, testCirc.getRadius(), TOLERANCE);
}

BOOST_AUTO_TEST_CASE(circleSquareAreaChangeAfterMove)
{
  const double coeff = 10.4;
  chugaynov::Circle testCirc({ 17.2, 1.39 }, 12.2);
  const double circleAreaBeforeScale = testCirc.getArea();
  testCirc.scale(coeff);
  BOOST_CHECK_CLOSE(circleAreaBeforeScale * coeff * coeff, testCirc.getArea(), TOLERANCE);
}

BOOST_AUTO_TEST_CASE(checkRotation)
{
  const double alpha = 30;
  const chugaynov::point_t point{ 0, 0 };
  const double radius = 10;
  chugaynov::Circle testCirc{ point, radius };

  const double circleAreaBeforeRotate = testCirc.getArea();
  testCirc.rotate(alpha);

  BOOST_CHECK_CLOSE(circleAreaBeforeRotate, testCirc.getArea(), TOLERANCE);

  const chugaynov::rectangle_t frameRect = testCirc.getFrameRect();
  BOOST_CHECK_CLOSE(radius, frameRect.width / 2, TOLERANCE);
  BOOST_CHECK_CLOSE(point.x, frameRect.pos.x, TOLERANCE);
  BOOST_CHECK_CLOSE(point.y, frameRect.pos.y, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(checkThrowExceptions)
{
  BOOST_CHECK_THROW(chugaynov::Circle({ 13.2, 23 }, -1), std::invalid_argument);
  chugaynov::Circle testCirc({ 17.2, 1.39 }, 12.2);
  BOOST_CHECK_THROW(testCirc.scale(-0.2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
