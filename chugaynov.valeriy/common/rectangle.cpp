#define _USE_MATH_DEFINES
#include "rectangle.hpp"
#include <iostream>
#include <cmath>

chugaynov::Rectangle::Rectangle(const point_t& point, double width, double height) :
  pos_(point),
  width_(width),
  height_(height),
  angle_(0)
{
  if ((width <= 0) || (height <= 0))
  {
    throw std::invalid_argument("\nThe rectangle's sides must be positive\n");
  }
}

double chugaynov::Rectangle::getArea() const
{
  return width_ * height_;
}

chugaynov::rectangle_t chugaynov::Rectangle::getFrameRect() const
{
  const double cosAngle = cos(M_PI * angle_ / 180);
  const double sinAngle = sin(M_PI * angle_ / 180);
  const double width = fabs(cosAngle * width_) + fabs(sinAngle * height_);
  const double height = fabs(cosAngle * height_) + fabs(sinAngle * width_);
  return { pos_, width, height };
}

void chugaynov::Rectangle::move(const point_t& point)
{
  pos_ = point;
}

void chugaynov::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void chugaynov::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("\nScaling coefficient must be positive\n");
  }
  width_ *= coefficient;
  height_ *= coefficient;
}

void chugaynov::Rectangle::rotate(double angle)
{
  angle_ += angle;
}

void chugaynov::Rectangle::printInfo() const
{
  std::cout << "Rectangle: pos = (" << pos_.x << ", " << pos_.y << ")";
  std::cout << "\theight = " << height_ << "\twidth = " << width_ << std::endl;
}
