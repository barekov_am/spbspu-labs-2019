#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>
#include <memory>

chugaynov::CompositeShape::CompositeShape():
  size_(0),
  capacity_(0)
{
}

chugaynov::CompositeShape::CompositeShape(const CompositeShape& compShape):
  shapeArray_(std::make_unique<shapePtr[]>(compShape.capacity_)),
  size_(compShape.size_),
  capacity_(compShape.capacity_)
{
  for (size_t i = 0; i < size_; i++)
  {
    shapeArray_[i] = compShape.shapeArray_[i];
  }
}

chugaynov::CompositeShape::CompositeShape(CompositeShape&& compShape):
  shapeArray_(std::move(compShape.shapeArray_)),
  size_(compShape.size_),
  capacity_(compShape.capacity_)
{
  compShape.size_ = compShape.capacity_ = 0;
}

chugaynov::CompositeShape& chugaynov::CompositeShape::operator =(const CompositeShape& compShape)
{
  if (this != &compShape)
  {
    shapeArray_ = std::make_unique<shapePtr[]>(compShape.capacity_);
    size_ = compShape.size_;
    capacity_ = compShape.capacity_;
    for (size_t i = 0; i < size_; i++)
    {
      shapeArray_[i] = compShape.shapeArray_[i];
    }
  }
  return *this;
}

chugaynov::CompositeShape& chugaynov::CompositeShape::operator =(CompositeShape&& compShape)
{
  if (this != &compShape)
  {
    shapeArray_ = std::move(compShape.shapeArray_);
    size_ = compShape.size_;
    capacity_ = compShape.capacity_;
    compShape.size_ = compShape.capacity_ = 0;
  }
  return *this;
}

chugaynov::shapePtr chugaynov::CompositeShape::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("\nIndex must be less than size");
  }
  return shapeArray_[index];
}

chugaynov::rectangle_t chugaynov::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    return { { 0, 0 }, 0, 0 };
  }
  rectangle_t tempRect = shapeArray_[0]->getFrameRect();
  double top = tempRect.pos.y + tempRect.height / 2;
  double bottom = tempRect.pos.y - tempRect.height / 2;
  double left = tempRect.pos.x - tempRect.width / 2;
  double right = tempRect.pos.x + tempRect.width / 2;
  for (size_t i = 1; i < size_; i++)
  {
    tempRect = shapeArray_[i]->getFrameRect();
    top = std::max(top, tempRect.pos.y + tempRect.height / 2);
    bottom = std::min(bottom, tempRect.pos.y - tempRect.height / 2);
    left = std::min(left, tempRect.pos.x - tempRect.width / 2);
    right = std::max(right, tempRect.pos.x + tempRect.width / 2);
  }
  return { { (right + left) / 2, (top + bottom) / 2 }, right - left, top - bottom };
}

double chugaynov::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < size_; i++)
  {
    area += shapeArray_[i]->getArea();
  }
  return area;
}

void chugaynov::CompositeShape::move(const point_t& point)
{
  const rectangle_t tempRect = getFrameRect();
  const double dx = point.x - tempRect.pos.x;
  const double dy = point.y - tempRect.pos.y;
  move(dx, dy);
}

void chugaynov::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; i++)
  {
    shapeArray_[i]->move(dx, dy);
  }
}

void chugaynov::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("\nCoefficient must be positive");
  }
  const point_t generalCenter = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    const rectangle_t tempRect = shapeArray_[i]->getFrameRect();
    const double dx = tempRect.pos.x - generalCenter.x;
    const double dy = tempRect.pos.y - generalCenter.y;
    shapeArray_[i]->move({ generalCenter.x + dx * coefficient, generalCenter.y + dy * coefficient });
    shapeArray_[i]->scale(coefficient);
  }
}

void chugaynov::CompositeShape::rotate(double angle)
{
  const double cosAngle = cos(M_PI * angle / 180);
  const double sinAngle = sin(M_PI * angle / 180);
  const point_t center = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    const point_t shapeCenter = shapeArray_[i]->getFrameRect().pos;
    const double pointX = center.x + (shapeCenter.x - center.x) * cosAngle
                                   - (shapeCenter.y - center.y) * sinAngle;
    const double pointY = center.y + (shapeCenter.x - center.x) * sinAngle
                                   + (shapeCenter.y - center.y) * cosAngle;
    shapeArray_[i]->rotate(angle);
    shapeArray_[i]->move({ pointX, pointY });
  }
}

void chugaynov::CompositeShape::printInfo() const
{
  const rectangle_t rect = getFrameRect();
  std::cout << "CompShape: pos = (" << rect.pos.x << ", " << rect.pos.y << ")";
  std::cout << "\theight = " << rect.height << "\twidth = " << rect.width << std::endl;
}

void chugaynov::CompositeShape::updateCapacity()
{
  const double capacityFactor = 1.5;
  capacity_ = static_cast<size_t>(size_ * capacityFactor + 0.5);
  arrayOfShapePtr tempArray(std::make_unique<shapePtr[]>(capacity_));
  for (size_t i = 0; i < size_ - 1; i++)
  {
    tempArray[i] = shapeArray_[i];
  }
  shapeArray_ = std::move(tempArray);
}

void chugaynov::CompositeShape::pushBack(chugaynov::shapePtr newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("\nPointer on shape can't be nullptr");
  }

  size_++;
  if (size_ > capacity_)
  {
    updateCapacity();
  }
  shapeArray_[size_ - 1] = newShape;
}

void chugaynov::CompositeShape::popBack()
{
  if (size_ != 0)
  {
    size_--;
  }
}

void chugaynov::CompositeShape::insert(chugaynov::shapePtr newShape, size_t index)
{
  if (index > size_)
  {
    throw std::out_of_range("\nIndex must be <= size and can't be negative");
  }
  if (newShape == nullptr)
  {
    throw std::invalid_argument("\nPointer on shape can't be nullptr");
  }

  size_++;
  if (size_ > capacity_)
  {
    updateCapacity();
  }
  for (size_t i = size_ - 1; i > index; i--)
  {
    shapeArray_[i] = shapeArray_[i - 1];
  }
  shapeArray_[index] = newShape;
}

void chugaynov::CompositeShape::erase(size_t index)
{
  if (index >= size_)
  {
    throw std::out_of_range("\nIndex must be < size and can't be negative");
  }

  size_--;
  for (size_t i = index; i < size_; i++)
  {
    shapeArray_[i] = shapeArray_[i + 1];
  }
}

void chugaynov::CompositeShape::shrinkToFit()
{
  if (capacity_ != size_)
  {
    capacity_ = size_;
    arrayOfShapePtr tempArray(std::make_unique<shapePtr[]>(capacity_));
    for (size_t i = 0; i < size_; i++)
    {
      tempArray[i] = shapeArray_[i];
    }
    shapeArray_ = std::move(tempArray);
  }
}

void chugaynov::CompositeShape::clear()
{
  shapeArray_.reset();
  size_ = 0;
  capacity_ = 0;
}

size_t chugaynov::CompositeShape::size() const
{
  return size_;
}
