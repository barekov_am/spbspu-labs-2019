#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include "shape.hpp"
#include <memory>

namespace chugaynov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape& compShape);
    CompositeShape(CompositeShape&& compShape);
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape& compShape);
    CompositeShape& operator =(CompositeShape&& compShape);
    shapePtr operator [](size_t index) const;

    rectangle_t getFrameRect() const override;
    double getArea() const override;
    void move(const point_t& point) override;
    void move(double dx, double dy) override;
    void scale(double coefficient) override;
    void rotate(double angle) override;
    void printInfo() const override;
    void pushBack(shapePtr newShape);
    void popBack();
    void insert(shapePtr newShape, size_t index);
    void erase(size_t index);
    void shrinkToFit();
    void clear();
    size_t size() const;
  private:
    arrayOfShapePtr shapeArray_;
    size_t size_;
    size_t capacity_;
    void updateCapacity();
  };
}
#endif
