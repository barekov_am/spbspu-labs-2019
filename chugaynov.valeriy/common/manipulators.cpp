#include "manipulators.hpp"

std::istream& blank(std::istream& in)
{
  while (isblank(in.peek()))
  {
    in.ignore();
  }
  return in;
}

std::istream& space(std::istream& in)
{
  while (isspace(in.peek()))
  {
    in.ignore();
  }
  return in;
}
