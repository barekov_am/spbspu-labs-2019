#include "point.hpp"

#include <math.h>

#include "manipulators.hpp"

const char OPEN_BRACKET = '(';
const char CLOSE_BRACKET = ')';
const char SEMICOLON = ';';

std::istream& operator>>(std::istream& in, Point& point)
{
  in >> blank;
  if (!in.eof())
  {
    char symbol;
    in >> symbol >> blank;
    if (symbol == OPEN_BRACKET && in.peek() != '\n')
    {
      int x;
      if (in >> x >> blank && in.peek() != '\n')
      {
        in >> symbol >> blank;
        if (symbol == SEMICOLON && in.peek() != '\n')
        {
          int y;
          if (in >> y >> blank && in.peek() != '\n')
          {
            in >> symbol;
            if (symbol == CLOSE_BRACKET)
            {
              point = { x,y };
              return in;
            }
          }
        }
      }
    }
  }
  in.setstate(std::ios_base::failbit);
  return in;
}


std::ostream& operator<<(std::ostream& out, const Point& point)
{
  return out << OPEN_BRACKET << point.x << SEMICOLON << " " << point.y << CLOSE_BRACKET;
}

int squareDistance(const Point& point1, const Point& point2)
{
  return pow(point1.x - point2.x, 2) + pow(point1.y - point2.y, 2);
}

