#include "distribution.hpp"
#include <memory>
#include <cmath>

bool chugaynov::intersect(const chugaynov::shapePtr shape1, const chugaynov::shapePtr shape2)
{
  const rectangle_t rect1 = shape1->getFrameRect();
  const rectangle_t rect2 = shape2->getFrameRect();
  const double distanceX = fabs(rect1.pos.x - rect2.pos.x);
  const double distanceY = fabs(rect1.pos.y - rect2.pos.y);
  return ((distanceX <= (rect1.width + rect2.width) / 2) && (distanceY <= (rect1.height + rect2.height) / 2));
}

chugaynov::Matrix chugaynov::distribute(const CompositeShape& compShape)
{
  Matrix matrix;
  const size_t compSize = compShape.size();
  for (size_t i = 0; i < compSize; i++)
  {
    size_t newRow = 0;
    size_t newCol = 0;

    for (size_t j = 0; j < matrix.rows_; j++)
    {

      const size_t layerSize = matrix.layerSize(j);
      for (size_t k = 0; k < layerSize; k++)
      {
        if (intersect(compShape[i], matrix[j][k]))
        {
          newRow++;
          break;
        }
        if (k == layerSize - 1)
        {
          newCol = layerSize;
          newRow = j;
        }
      }

      if (newRow == j)
      {
        break;
      }

    }
    matrix.addShape(compShape[i], newRow, newCol);
  }
  return matrix;
}

chugaynov::Matrix chugaynov::distribute(const std::unique_ptr<chugaynov::shapePtr[]> shapeArray, size_t size)
{
  CompositeShape compShape;
  for (size_t i = 0; i < size; i++)
  {
    compShape.pushBack(shapeArray[i]);
  }
  return distribute(compShape);
}
