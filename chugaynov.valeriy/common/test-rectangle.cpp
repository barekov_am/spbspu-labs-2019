#define _USE_MATH_DEFINES

#include "boost/test/auto_unit_test.hpp"

#include <cmath>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testForRectangle)

const double TOLERANCE = 0.0001;

BOOST_AUTO_TEST_CASE(rectangleInvariantAfterMove)
{
  const double width = 18.89;
  const double height = 10.9;
  chugaynov::Rectangle testRect({ 12.26, 2.8 }, width, height);
  const double rectangleAreaBeforeMove = testRect.getArea();
  testRect.move(1.2, 1.9);
  BOOST_CHECK_CLOSE(rectangleAreaBeforeMove, testRect.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(width, testRect.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(height, testRect.getFrameRect().height, TOLERANCE);
  testRect.move({ 1.2, 1.9 });
  BOOST_CHECK_CLOSE(rectangleAreaBeforeMove, testRect.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(width, testRect.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(height, testRect.getFrameRect().height, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(rectangleSquareAreaChangeAfterMove)
{
  const double coeff = 12.4;
  chugaynov::Rectangle testRect({ 12.26, 2.8 }, 12.2, 102.3);
  const double rectangleAreaBeforeScale = testRect.getArea();
  testRect.scale(coeff);
  BOOST_CHECK_CLOSE(rectangleAreaBeforeScale * coeff * coeff, testRect.getArea(), TOLERANCE);
}

BOOST_AUTO_TEST_CASE(checkRotation)
{
  const double alpha = 30;
  const chugaynov::point_t point{ 0, 0 };
  const double height1 = 10, width1 = 5;
  chugaynov::Rectangle testRect{ point, width1, height1 };

  const double rectangleAreaBeforeRotate = testRect.getArea();
  testRect.rotate(alpha);

  BOOST_CHECK_CLOSE(rectangleAreaBeforeRotate, testRect.getArea(), TOLERANCE);

  const double cosAlpha = cos(M_PI * alpha / 180);
  const double sinAlpha = sin(M_PI * alpha / 180);

  const double width2 = fabs(cosAlpha * width1) + fabs(sinAlpha * height1);
  const double height2 = fabs(cosAlpha * height1) + fabs(sinAlpha * width1);
  BOOST_CHECK_CLOSE(width2, testRect.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(height2, testRect.getFrameRect().height, TOLERANCE);
  
  testRect.rotate(-alpha);
  const chugaynov::rectangle_t frameRect2 = testRect.getFrameRect();
  BOOST_CHECK_CLOSE(width1, frameRect2.width, TOLERANCE);
  BOOST_CHECK_CLOSE(height1, frameRect2.height, TOLERANCE);
  BOOST_CHECK_CLOSE(point.x, frameRect2.pos.x, TOLERANCE);
  BOOST_CHECK_CLOSE(point.y, frameRect2.pos.y, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(checkThrowExceptions)
{
  BOOST_CHECK_THROW(chugaynov::Rectangle({ 13.2, 23 }, -1, 4), std::invalid_argument);
  chugaynov::Rectangle testRect({ 12.26, 2.8 }, 12.2, 102.3);
  BOOST_CHECK_THROW(testRect.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
