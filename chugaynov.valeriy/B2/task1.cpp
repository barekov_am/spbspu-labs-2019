#include<string>
#include<iostream>
#include<vector>
#include<sstream>
#include"queue-with-priority.hpp"

void task1()
{
  QueueWithPriority<std::string> queue;
  std::string commandLine;

  while (std::getline(std::cin, commandLine))
  {
    if (!commandLine.empty())
    {
      std::stringstream streamLine(commandLine);
      std::string command;
      streamLine >> command >> std::ws;

      if ((command == "get") && (streamLine.eof()))
      {
        std::cout << (!queue.empty() ? queue.extractElementFromQueue() : "<EMPTY>") << "\n";
      }

      else if ((command == "accelerate") && (streamLine.eof()))
      {
        queue.accelerate();
      }

      else if (command == "add")
      {
        std::string priority;
        streamLine >> priority >> std::ws;
        std::string data;
        std::getline(streamLine, data);
        if ((priority == "low") && (!data.empty()))
        {
          queue.putElementToQueue(data, LOW);
        }
        else if ((priority == "normal") && (!data.empty()))
        {
          queue.putElementToQueue(data, NORMAL);
        }
        else if ((priority == "high") && (!data.empty()))
        {
          queue.putElementToQueue(data, HIGH);
        }
        else
        {
          std::cout << "<INVALID COMMAND>\n";
        }
      }

      else
      {
        std::cout << "<INVALID COMMAND>\n";
      }

    }
  }
}
