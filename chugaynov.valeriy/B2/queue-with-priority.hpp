#ifndef QUEUE_WITH_PRIORITY
#define QUEUE_WITH_PRIORITY

#include<list>
#include<iostream>

enum ElementPriority
{
  LOW,
  NORMAL,
  HIGH
};

template<typename QElem>
class QueueWithPriority
{
public:
  void putElementToQueue(const QElem& element, const ElementPriority priority);
  QElem extractElementFromQueue();
  QElem getElementFromQueue() const;
  bool empty() const;
  void accelerate();

private:
  std::list<QElem> highElements_;
  std::list<QElem> normalElements_;
  std::list<QElem> lowElements_;
};

template<typename QElem>
void QueueWithPriority<QElem>::putElementToQueue(const QElem& element, const ElementPriority priority)
{
  switch (priority)
  {
  case HIGH:
    highElements_.push_back(element);
    break;
  case NORMAL:
    normalElements_.push_back(element);
    break;
  case LOW:
    lowElements_.push_back(element);
    break;
  }
}

template<typename QElem>
QElem QueueWithPriority<QElem>::extractElementFromQueue()
{
  if (!highElements_.empty())
  {
    QElem neededElement = highElements_.front();
    highElements_.pop_front();
    return neededElement;
  }
  else if (!normalElements_.empty())
  {
    QElem neededElement = normalElements_.front();
    normalElements_.pop_front();
    return neededElement;
  }
  else if (!lowElements_.empty())
  {
    QElem neededElement = lowElements_.front();
    lowElements_.pop_front();
    return neededElement;
  }
    throw std::range_error("Queue is empty");
}

template<typename QElem>
QElem QueueWithPriority<QElem>::getElementFromQueue() const
{
  if (!highElements_.empty())
  {
    return highElements_.front();
  }
  else if (!normalElements_.empty())
  {
    return normalElements_.front();
  }
  else if (!lowElements_.empty())
  {
    return lowElements_.front();
  }
  throw std::range_error("Queue is empty");
}

template<typename QElem>
bool QueueWithPriority<QElem>::empty() const
{
  return lowElements_.empty() && normalElements_.empty() && highElements_.empty();
}

template<typename QElem>
void QueueWithPriority<QElem>::accelerate()
{
  highElements_.splice(highElements_.end(), lowElements_);
}

#endif
