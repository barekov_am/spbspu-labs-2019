#include "formatted-text.hpp"

#include <unordered_set>

#include "manipulators.hpp"

const char PLUS = '+';
const char HYPHEN = '-';
const char DECIMAL_SEPARATOR = '.';
const std::unordered_set<char> PUNCT_SET{ '.',',','!','?',':',';' };

constexpr int MAX_NUMBER_LENGTH = 20;
constexpr int MAX_WORD_LENGTH = 20;

constexpr size_t COMMA_SIZE = 1;
constexpr size_t SPACE_SIZE = 1;
constexpr size_t DASH_SIZE = 3;

const char COMMA[COMMA_SIZE + 1] = ",";
const char SPACE[SPACE_SIZE + 1] = " ";
const char DASH[DASH_SIZE + 1] = "---";

constexpr size_t MIN_LINE_SIZE
{
  MAX_NUMBER_LENGTH > MAX_WORD_LENGTH ? MAX_NUMBER_LENGTH : MAX_WORD_LENGTH + COMMA_SIZE + SPACE_SIZE + DASH_SIZE
};

using Token = FormattedText::Token;
using TokenType = FormattedText::TokenType;

std::istream& operator >>(std::istream& in, Token& token)
{
  const auto word = [](std::istream& in, char firstSymbol, char separator, int maxLength)
  {
    int count = 1;
    std::string data{ firstSymbol };
    char nextSymbol = in.peek();
    while (count < maxLength && (isalpha(nextSymbol) || (nextSymbol == separator && isalpha(data.back()))))
    {
      data += nextSymbol;
      count++;
      nextSymbol = in.ignore().peek();
    }
    if (isalpha(nextSymbol))
    {
      in.setstate(std::ios_base::failbit);
    }
    return data;
  };

  const auto number = [](std::istream& in, char firstSymbol, char separator, int maxLength)
  {
    int count = 1;
    std::string data{ firstSymbol };
    char nextSymbol = in.peek();
    bool decimalSeparatorHere = false;
    while (count < maxLength && (isdigit(nextSymbol) || (nextSymbol == separator && !decimalSeparatorHere)))
    {
      decimalSeparatorHere = nextSymbol == separator ? true : decimalSeparatorHere;
      data += nextSymbol;
      count++;
      nextSymbol = in.ignore().peek();
    }
    if (isdigit(nextSymbol) || ((firstSymbol == PLUS || firstSymbol == HYPHEN) && count == 1))
    {
      in.setstate(std::ios_base::failbit);
    }
    return data;
  };



  if ((in >> space).eof())
  {
    in.setstate(std::ios_base::failbit);
    return in;
  }

  const char firstSymbol = in.get();
  std::string tokenData;
  TokenType tokenType;

  if (firstSymbol == HYPHEN && in.peek() == HYPHEN)
  {
    if (in.ignore().get() == HYPHEN)
    {
      tokenData = DASH;
      tokenType = TokenType::DASH;
    }
    else
    {
      in.setstate(std::ios_base::failbit);
    }
  }
  else if (PUNCT_SET.find(firstSymbol) != PUNCT_SET.end())
  {
    tokenData = firstSymbol;
    tokenType = TokenType::PUNCT_MARK;
  }
  else if (isdigit(firstSymbol) || firstSymbol == PLUS || firstSymbol == HYPHEN)
  {
    tokenData = number(in, firstSymbol, DECIMAL_SEPARATOR, MAX_NUMBER_LENGTH);
    tokenType = TokenType::ALNUM;
  }
  else if (isalpha(firstSymbol))
  {
    tokenData = word(in, firstSymbol, HYPHEN, MAX_WORD_LENGTH);
    tokenType = TokenType::ALNUM;
  }
  else
  {
    in.setstate(std::ios_base::failbit);
  }


  if (in)
  {
    token.data = tokenData;
    token.type = tokenType;
  }
  return in;
}


std::ostream& operator <<(std::ostream& out, const Token& token)
{
  return out << token.data;
}




std::istream& operator >>(std::istream& in, FormattedText& fText)
{
  Token token;
  if (!(in >> token) || token.type == TokenType::PUNCT_MARK || token.type == TokenType::DASH)
  {
    in.setstate(std::ios_base::failbit);
    return in;
  }

  fText.vector_.push_back(token);
  Token prevToken = token;

  while (in >> token)
  {
    if ((prevToken.type == TokenType::PUNCT_MARK || prevToken.type == TokenType::DASH)
      && (token.type == TokenType::PUNCT_MARK || (token.type == TokenType::DASH && prevToken.data != COMMA)))
    {
      in.setstate(std::ios_base::failbit);
    }
    else
    {
      prevToken = token;
      fText.vector_.push_back(token);
    }
  }
  return in;
}



std::ostream& operator <<(std::ostream& out, const FormattedText& fText)
{
  if (fText.vector_.empty())
  {
    return out;
  }

  Token prevToken = fText.vector_.front();
  size_t lineSize = prevToken.data.size();

  for (auto curToken = fText.vector_.cbegin() + 1; curToken != fText.vector_.cend(); ++curToken)
  {
    const size_t curTokenSize = curToken->data.size();

    switch (curToken->type)
    {
    case TokenType::ALNUM:
      if (lineSize + curTokenSize >= fText.maxLineWidth_)
      {
        out << prevToken << '\n';
        lineSize = curTokenSize;
      }
      else
      {
        out << prevToken << SPACE;
        lineSize += (curTokenSize + SPACE_SIZE);
      }
      break;


    case TokenType::PUNCT_MARK:
      if (lineSize == fText.maxLineWidth_
        || (curToken->data == COMMA
          && (curToken + 1) != fText.vector_.cend()
          && (curToken + 1)->type == TokenType::DASH
          && (lineSize + SPACE_SIZE + DASH_SIZE) >= fText.maxLineWidth_))
      {
        out << '\n' << prevToken;
        lineSize = prevToken.data.size() + curTokenSize;
      }
      else
      {
        out << prevToken;
        lineSize += curTokenSize;
      }
      break;


    case TokenType::DASH:
      if (lineSize + curTokenSize >= fText.maxLineWidth_)
      {
        out << '\n' << prevToken << SPACE;
        lineSize = prevToken.data.size() + curTokenSize + SPACE_SIZE;
      }
      else
      {
        out << prevToken << SPACE;
        lineSize += (curTokenSize + SPACE_SIZE);
      }
      break;
    }

    prevToken = *curToken;
  }
  return out << prevToken;
}




FormattedText::FormattedText(const size_t maxLineWidth):
  maxLineWidth_(maxLineWidth)
{
  if (maxLineWidth < MIN_LINE_SIZE)
  {
    throw std::invalid_argument("Too small line-width argument");
  }
}

