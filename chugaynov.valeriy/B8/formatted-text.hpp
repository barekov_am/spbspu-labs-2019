#ifndef FORMATTED_TEXT_HPP
#define FORMATTED_TEXT_HPP

#include <vector>
#include <string>
#include <iostream>

class FormattedText
{
public:
  enum TokenType
  {
    ALNUM,
    PUNCT_MARK,
    DASH
  };

  struct Token
  {
    std::string data;
    TokenType type;
  };

  friend std::istream& operator >>(std::istream& in, FormattedText& fText);
  friend std::ostream& operator <<(std::ostream& in, const FormattedText& fText);

  FormattedText(const size_t maxLineWidth);

private:
  size_t maxLineWidth_;
  std::vector<Token> vector_;
};


std::istream& operator >>(std::istream& in, FormattedText::Token& token);
std::ostream& operator <<(std::ostream& out, const FormattedText::Token& token);

#endif
