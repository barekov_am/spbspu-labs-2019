#include <iostream>
#include <cstring>

#include "formatted-text.hpp"

const size_t DEFAULT_MAX_LINE_WIDTH = 40;

int main(int argc, char* argv[])
{
  try
  {
    auto getLineWidth = [](const char* argv2, size_t& maxLineWidth)->bool
    {
      char* end = nullptr;
      maxLineWidth = std::strtol(argv2, &end, 10);
      return !(*end);
    };

    size_t maxLineWidth = DEFAULT_MAX_LINE_WIDTH;
    if (argc == 1 || (argc == 3 && !strcmp(argv[1], "--line-width") && getLineWidth(argv[2], maxLineWidth)))
    {
      FormattedText fText(maxLineWidth);
      std::cin >> fText;
      if (std::cin.fail() && !std::cin.eof())
      {
        throw std::invalid_argument("Incorrect text");
      }
      std::cout << fText << "\n";
    }
    else
    {
      throw std::invalid_argument("Invalid arguments");
    }
  }
  catch (const std::exception& except)
  {
    std::cerr << except.what() << "\n";
    return 1;
  }
}
