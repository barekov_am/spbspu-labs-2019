#include <boost/test/auto_unit_test.hpp>
#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testPartition)

BOOST_AUTO_TEST_CASE(testIntersection)
{
  kovbun::Circle testCircle(4, { 3, 1 });
  kovbun::Rectangle testRectangle(4, 4, { 8, 6 });

  BOOST_CHECK(kovbun::isIntersected(testCircle.getFrameRect(),testRectangle.getFrameRect()));
}

BOOST_AUTO_TEST_CASE(partitionCorrectness)
{
  kovbun::Shape::shape_ptr circlePtr1 = std::make_shared<kovbun::Circle>(4, kovbun::point_t { 3, 1 });
  kovbun::Shape::shape_ptr rectanglePtr1 = std::make_shared<kovbun::Rectangle>(6, 4, kovbun::point_t { 9, 4 });
  kovbun::Shape::shape_ptr circlePtr2 = std::make_shared<kovbun::Circle>(2, kovbun::point_t { 7, -3 });
  kovbun::Shape::shape_ptr rectanglePtr2 = std::make_shared<kovbun::Rectangle>(6, 4, kovbun::point_t { 4, -4 });

  kovbun::CompositeShape compShape(circlePtr1);
  compShape.add(rectanglePtr1);
  compShape.add(circlePtr2);
  compShape.add(rectanglePtr2);

  const kovbun::Matrix testMatrix = kovbun::part(compShape);

  const size_t rows = 3;
  const size_t columns = 2;

  BOOST_CHECK_EQUAL(testMatrix.getColumns(), columns);
  BOOST_CHECK_EQUAL(testMatrix.getRows(), rows);
  BOOST_CHECK(testMatrix[0][0] == circlePtr1);
  BOOST_CHECK(testMatrix[1][0] == rectanglePtr1);
  BOOST_CHECK(testMatrix[1][1] == circlePtr2);
  BOOST_CHECK(testMatrix[2][0] == rectanglePtr2);
}

BOOST_AUTO_TEST_SUITE_END()


