#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

void printCompositeShapeInfo(kovbun::Shape& shape)
{
  shape.writeInfo();
  std::cout << "AREA = " << shape.getArea() << std::endl;
  shape.move({ 2, 1 });
  std::cout << "AFTER MOVING TO A POINT: " << std::endl;
  shape.writeInfo();
  shape.move(2, 8);
  std::cout << "AFTER ADD TO OX, OY: " << std::endl;
  shape.writeInfo();
  shape.scale(2);
  std::cout << "AFTER SCALING: " << std::endl;
  shape.writeInfo();
  shape.rotate(45);
  std::cout << "AFTER ROTATION: " << std::endl;
  shape.writeInfo();
}


int main()
{
  kovbun::Circle circle(21.77, { 9, -4 });
  std::cout << "__Circle info__: " << "\n\n";
  circle.writeInfo();
  std::cout << "Add to OX " << 1 << " and to OY " << 4 << std::endl;
  circle.move(1, 4);
  circle.writeInfo();
  std::cout << "Move to point (" << 30 << ", " << 11 << ")\n";
  circle.move({ 30, 11 });
  circle.writeInfo();
  circle.scale(22.8);
  std::cout << "__New parameters__" << std::endl;
  circle.writeInfo();

  kovbun::Rectangle rectangle(7, 7, { 3, 5 });
  std::cout << "__Rectangle info__ " << "\n\n";
  rectangle.writeInfo();
  std::cout << "Add to ОX " << 5 << " and  to OY " << -1.7 << std::endl;
  rectangle.move(5, -1.7);
  rectangle.writeInfo();
  std::cout << "Move to point (" << 16.5 << ", " << 9 << ")\n";
  rectangle.move({ 16.5, 9 });
  rectangle.writeInfo();
  std::cout << std::endl;
  rectangle.scale(13.37);
  std::cout << "__New parameters__" << std::endl;
  rectangle.writeInfo();

  std::cout << "__Composite shape__" << std::endl;
  kovbun::Shape::shape_ptr circle2 = std::make_shared<kovbun::Circle>(4, kovbun::point_t { 5, 5 });
  kovbun::Shape::shape_ptr rectangle2 = std::make_shared<kovbun::Rectangle>(4, 4, kovbun::point_t { 5, 5 });
  kovbun::Shape::shape_ptr square = std::make_shared<kovbun::Rectangle>(4, 4, kovbun::point_t { 5, 5 });
  kovbun::CompositeShape compShape(rectangle2);
  compShape.writeInfo();
  compShape.add(circle2);
  compShape.add(rectangle2);
  compShape.add(square);
  std::cout << "__Print info about Composite Shape__" << std::endl;
  printCompositeShapeInfo(compShape);
  return 0;
}

