#include <boost/test/unit_test.hpp>
#include <stdexcept>

#include "triangle.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(triangleTest)

BOOST_AUTO_TEST_CASE(testTriangleImmutabilityMoving)
{
  kovbun::Triangle triangle1({0, 0}, {10, 10}, {20, -10});
  const double beforeArea = triangle1.getArea();
  const kovbun::rectangle_t beforeFrame = triangle1.getFrameRect();

  triangle1.move(5, 5);
  BOOST_CHECK_CLOSE(beforeArea, triangle1.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(beforeFrame.width, triangle1.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(beforeFrame.height, triangle1.getFrameRect().height, EPSILON);
}

BOOST_AUTO_TEST_CASE(testTrinagleImmutabilityScaling)
{
  kovbun::Triangle triangle1({0, 0}, {10, 10}, {20, -10});
  const double scaleFactor = 3;
  const double beforeArea = triangle1.getArea();
  const kovbun::rectangle_t beforeFrame = triangle1.getFrameRect();

  triangle1.scale(scaleFactor);

  BOOST_CHECK_CLOSE(beforeArea * scaleFactor * scaleFactor, triangle1.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(beforeFrame.width * scaleFactor, triangle1.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(beforeFrame.height * scaleFactor, triangle1.getFrameRect().height, EPSILON);
}

BOOST_AUTO_TEST_CASE(testTriangleIncorrectParams)
{
  kovbun::Triangle triangle1({0, 0}, {10, 10}, {20, -10});
  BOOST_CHECK_THROW(triangle1.scale(-4), std::invalid_argument);
  BOOST_CHECK_THROW(kovbun::Triangle triangle1({0, 0}, {0, 0}, {0, 0}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  kovbun::Triangle myTriangle({ 4, 2 }, { 5, 5 }, { 1, 1 });
  const double testArea = myTriangle.getArea();
  const kovbun::rectangle_t testFrameRect = myTriangle.getFrameRect();
  const double angle = 90;
  const kovbun::rectangle_t testFrameRect2 = myTriangle.getFrameRect();


  myTriangle.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, myTriangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(testFrameRect.height, myTriangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(testArea, myTriangle.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testFrameRect2.pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testFrameRect2.pos.y);
}

BOOST_AUTO_TEST_SUITE_END()

