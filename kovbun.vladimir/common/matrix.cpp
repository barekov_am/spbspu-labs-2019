#include "matrix.hpp"

kovbun::Matrix::Matrix():
  rows_(0),
  columns_(0)
{
}

kovbun::Matrix::Matrix(const Matrix& matrix):
  arrayShapes_(std::make_unique<Shape::shape_ptr[]>(matrix.rows_ * matrix.columns_)),
  rows_(matrix.rows_),
  columns_(matrix.columns_)
{
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    arrayShapes_[i] = matrix.arrayShapes_[i];
  }
}

kovbun::Matrix::Matrix(Matrix&& matrix):
  arrayShapes_(std::move(matrix.arrayShapes_)),
  rows_(matrix.rows_),
  columns_(matrix.columns_)
{
  matrix.rows_ = 0;
  matrix.columns_ = 0;
}

kovbun::Matrix& kovbun::Matrix::operator =(const Matrix& matrix)
{
  if (this != &matrix)
  {
    Shape::shape_array shapes = std::make_unique<Shape::shape_ptr[]>(matrix.rows_ * matrix.columns_);
    for (size_t i = 0; i < (matrix.rows_ * matrix.columns_); i++)
    {
      shapes[i] = matrix.arrayShapes_[i];
    }
    arrayShapes_.swap(shapes);
    rows_ = matrix.rows_;
    columns_ = matrix.columns_;
  }
  return *this;
}

kovbun::Matrix& kovbun::Matrix::operator =(Matrix&& matrix)
{
  if (this != &matrix)
  {
    std::swap(rows_, matrix.rows_);
    std::swap(columns_, matrix.columns_);
    std::swap(arrayShapes_,matrix.arrayShapes_);
  }
  return *this;
}

kovbun::Shape::shape_array kovbun::Matrix::operator[](size_t index) const
{
  if (index >= rows_)
  {
    throw std::out_of_range("Index is out of range");
  }
  Shape::shape_array shapes = std::make_unique<Shape::shape_ptr[]>(columns_);
  for (size_t i = 0; i < columns_; i++)
  {
    shapes[i] = arrayShapes_[index * columns_ + i];
  }
  return shapes;
}

kovbun::Shape::shape_ptr kovbun::Matrix::at(const size_t layer, const size_t col)
{
  if (layer >= rows_ || col >= columns_) {
    throw std::out_of_range("indexes were out of range in Matrix::at");
  }
  return arrayShapes_[layer * columns_ + col];
}

bool kovbun::Matrix::operator ==(const Matrix& matrix) const
{
  if ((rows_ != matrix.rows_) || (columns_ != matrix.columns_))
  {
    return false;
  }
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (arrayShapes_[i] != matrix.arrayShapes_[i])
    {
      return false;
    }
  }
  return true;
}

bool kovbun::Matrix::operator!=(const Matrix& matrix) const
{
  return !(*this == matrix);
}

size_t kovbun::Matrix::getRows() const
{
  return rows_;
}

size_t kovbun::Matrix::getColumns() const
{
  return columns_;
}

size_t kovbun::Matrix::getLayerSize(size_t layer) const
{
  if (layer >= rows_)
  {
    return 0;
  }
  for (size_t i = 0; i < columns_; i++)
  {
    if (arrayShapes_[layer * columns_ + i] == nullptr)
    {
      return i;
    }
  }
  return columns_;
}

void kovbun::Matrix::add(kovbun::Shape::shape_ptr shape, size_t layer)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Pointer can't be null");
  }
  size_t tmpRows = (layer >= rows_) ? (rows_ + 1) : rows_;
  size_t tmpColumns = (getLayerSize(layer) == columns_) ? (columns_ + 1) : columns_;
  const size_t column = (layer < rows_) ? getLayerSize(layer) : 0;
  Shape::shape_array shapes = std::make_unique<Shape::shape_ptr[]>(tmpRows * tmpColumns);
  for (size_t i = 0; i < tmpRows; i++)
  {
    for (size_t j = 0; j < tmpColumns; j++)
    {
      if ((i != rows_) && (j != columns_))
      {
        shapes[i * tmpColumns + j] = arrayShapes_[i * columns_ + j];
      }
    }
  }
  if (layer < rows_)
  {
    shapes[layer * tmpColumns + column] = shape;
  }
  else
  {
    shapes[rows_ * tmpColumns + column] = shape;
  }
  arrayShapes_.swap(shapes);
  rows_ = tmpRows;
  columns_ = tmpColumns;
}

