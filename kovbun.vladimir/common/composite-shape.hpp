#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include <iostream>
#include "shape.hpp"

namespace kovbun
{
  class CompositeShape : public Shape {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape& source);
    CompositeShape(CompositeShape&& source);
    CompositeShape(const shape_ptr& source);
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape& source);
    CompositeShape& operator =(CompositeShape&& source);
    Shape::shape_ptr operator [](size_t) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& center) override;
    void move(double dx, double dy) override;
    void scale(double multiplier) override;
    void rotate(double angle) override;
    void add(shape_ptr shape);
    void remove(size_t index);
    size_t getSize() const;
    shape_array getShapes() const;
    void writeInfo() const override;

  private:
    size_t count_;
    shape_array arrayShapes_;
  };
}
#endif

