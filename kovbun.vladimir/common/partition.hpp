#ifndef PARTITION_HPP
#define PARTITION_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace kovbun
{
  kovbun::Matrix part(const kovbun::CompositeShape& compShape);
  bool isIntersected(const rectangle_t& shape1, const rectangle_t& shape2);
}

#endif

