#include "triangle.hpp"
#include "base-types.hpp"
#include <cmath>
#include <iostream>
#include <cassert>
#include <stdexcept>
#include <algorithm>


kovbun::Triangle::Triangle(const point_t &t1, const point_t &t2, const point_t &t3):
  point1_(t1),
  point2_(t2),
  point3_(t3)
{
  if (getArea() == 0.0)
   {
     throw std::invalid_argument("Area must be more than 0");
   }

}

kovbun::point_t kovbun::Triangle::getPos() const
{
  point_t point = {0.0, 0.0};
  point.x = (point1_.x + point2_.x + point3_.x) / 3;
  point.y = (point1_.y + point2_.y + point3_.y) / 3;
  return point;
}

void kovbun::Triangle::move(double dx, double dy)
{
  point1_.x += dx;
  point1_.y += dy;
  point2_.x += dx;
  point2_.y += dy;
  point3_.x += dx;
  point3_.y += dy;
}

void kovbun::Triangle::move(const point_t &center)
{
  point_t point = getPos();
  double dx1 = center.x - point.x;
  double dy1 = center.y - point.y;
  move(dx1, dy1);
}

double kovbun::Triangle::getArea() const
{
  return fabs(0.5 * (point1_.x * (point2_.y - point3_.y) + point2_.x * (point3_.y - point1_.y) + point3_.x * (point1_.y - point2_.y)));
}

kovbun::rectangle_t kovbun::Triangle::getFrameRect() const
{
  const double maxX = std::max({point1_.x, point2_.x, point3_.x});
  const double minX = std::min({point1_.x, point2_.x, point3_.x});
  const double maxY = std::max({point1_.y, point2_.y, point3_.y});
  const double minY = std::min({point1_.y, point2_.y, point3_.y});
  const double width = maxX - minX;
  const double height = maxY - minY;
  const point_t position = {minX + width / 2, minY + height / 2};
  return rectangle_t {position, width, height};
}

kovbun::point_t scaling(kovbun::point_t point_, kovbun::point_t centre, double scaleFactor)
{
  double x = centre.x + scaleFactor * (point_.x - centre.x);
  double y = centre.y + scaleFactor * (point_.y - centre.y);
  return {x,y};
}

void kovbun::Triangle::scale(double scaleFactor)
{
  if (scaleFactor <= 0)
    {
      throw std::invalid_argument("scale multiplier can't be <=0");
    }
    point_t centre = getPos();
    point1_ = scaling(point1_, centre, scaleFactor);
    point2_ = scaling(point2_, centre, scaleFactor);
    point3_ = scaling(point3_, centre, scaleFactor);

}

void kovbun::Triangle::writeInfo() const
{
  rectangle_t rect = getFrameRect();
  point_t point = getPos();
  std::cout << "Information about the Triangle: \n";
  std::cout << "Width and height of the rectangle: " << rect.width << "  " << rect.height << std::endl;
  std::cout << "Area: " << getArea() << std::endl;
  std::cout << "Coordinates of the center of the Triangle: " << point.x << "  " << point.y << "\n"
            << std::endl;
}

kovbun::point_t rotateOneCoordinate(kovbun::point_t point, double angle)
{
  double x = point.x * cos(M_PI * angle / 180) - point.y * sin(M_PI * angle / 180);
  double y = point.y * cos(M_PI * angle / 180) + point.x * sin(M_PI * angle / 180);
  return {x, y};
}


void kovbun::Triangle::rotate(double angle)
{
  if (angle < 0 )
  {
    angle = (360 + fmod(angle, 360));
  }
  point1_ = rotateOneCoordinate(point1_, angle);
  point2_ = rotateOneCoordinate(point2_, angle);
  point3_ = rotateOneCoordinate(point3_, angle);
}

