#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"
#include <memory>

namespace kovbun
{
  class Shape
  {
  public:
    virtual ~Shape() = default;

    virtual void move(const point_t &newCenter) = 0;
    virtual void move(double dx, double dy) = 0;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void writeInfo() const = 0;
    virtual void scale(double scaleFactor) = 0;
    virtual void rotate(double angle) = 0;

    using shape_ptr = std::shared_ptr<Shape>;
    using shape_array = std::unique_ptr<shape_ptr[]>;
  };
}
#endif

