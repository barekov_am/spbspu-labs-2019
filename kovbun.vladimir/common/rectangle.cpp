#include <iostream>
#include <stdexcept>
#include <cmath>
#include "rectangle.hpp"

kovbun::Rectangle::Rectangle(double width, double height, const point_t position):
  width_(width),
  height_(height),
  pos_(position),
  angle_(0)
{
  if (width_ <= 0.0)
  {
    throw std::invalid_argument("Wrong rectangle width");
  }
  if (height_ <= 0.0)
  {
    throw std::invalid_argument("Wrong rectangle height");
  }
}

double kovbun::Rectangle::getArea() const
{
  return (width_ * height_);
}

kovbun::rectangle_t kovbun::Rectangle::getFrameRect() const
{
  const double cosA = fabs(cos(angle_ * M_PI / 180));
  const double sinA = fabs(sin(angle_ * M_PI / 180));

  double width = height_* sinA + width_ * cosA;
  double height = height_ * cosA + width_ * sinA;

  return { pos_, width, height};
}

void kovbun::Rectangle::move(const point_t& point)
{
  pos_ = point;
}

void kovbun::Rectangle::move(const double dx, const double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}
void kovbun::Rectangle::scale(double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("Zoom factor must be positive");
  }
  else
  {
    width_ *= multiplier;
    height_ *= multiplier;
  }
}
void kovbun::Rectangle::rotate (const double angle)
{
  angle_ += angle;

}

void kovbun::Rectangle::writeInfo() const
{
  std::cout << "Width: " << width_ << std::endl;
  std::cout << "Height: " << height_ << std::endl;
  std::cout << "Center position: x = " << pos_.x << ", y = " << pos_.y << std::endl;
  std::cout << "Area: " << getArea() << std::endl;
  rectangle_t frame = getFrameRect();
  std::cout << "Rectangle frame: " << std::endl;
  std::cout << "Width: " << frame.width << std::endl;
  std::cout << "Height: " << frame.height << "\n\n";
}
