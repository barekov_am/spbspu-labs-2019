#include "circle.hpp"
#include <iostream>
#include <cassert>
#include <math.h>

kovbun::Circle::Circle(double radius, const point_t& position):
  radius_(radius),
  pos_(position)
{
  if (radius_ < 0)
  {
    throw std::invalid_argument("Invalid radius");
  }
}

double kovbun::Circle::getArea() const
{
  return (M_PI * radius_ * radius_);
}

kovbun::rectangle_t kovbun::Circle::getFrameRect() const
{
  return {pos_, 2 * radius_, 2 * radius_};
}

void kovbun::Circle::move(const point_t& point)
{
  pos_ = point;
}

void kovbun::Circle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void kovbun::Circle::scale(const double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("Invalid multiplier");
  }
  radius_ *= multiplier;
}

void kovbun::Circle::rotate(double)
{
  // rotation of circle can't change anything
}

void kovbun::Circle::writeInfo() const
{
  std::cout << "Radius: " << radius_ << std::endl;
  std::cout << "Area: " << getArea() << std::endl;
  rectangle_t frame = getFrameRect();
  std::cout << "Circle frame: " << std::endl;
  std::cout << "Width: " << frame.width << std::endl;
  std::cout << "Height: " << frame.height << "\n\n";
}

