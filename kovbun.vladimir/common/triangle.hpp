#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "base-types.hpp"
#include "shape.hpp"

namespace kovbun
{
  class Triangle: public Shape
  {
  public:
    Triangle(const point_t &t1,const point_t &t2,const point_t &t3);

    void move(const point_t &center) override;
    void move(double dx, double dy) override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    point_t getPos() const;
    void writeInfo() const override;
    void scale(double scaleFactor) override;
    void rotate(double angle) override;

  private:
    point_t point1_;
    point_t point2_;
    point_t point3_;
  };
}
#endif // TRIANGLE_HPP

