#include <cmath>
#include "partition.hpp"

kovbun::Matrix kovbun::part(const kovbun::CompositeShape& compShape)
{
  Matrix matrix;
  const std::size_t size = compShape.getSize();
  for (std::size_t i = 0; i < size; i++)
  {
    std::size_t layer = 0;
    for (std::size_t j = 0; j < matrix.getRows(); j++)
    {
      bool intersect = false;
      for (std::size_t k = 0; k < matrix.getLayerSize(j); k++)
      {
        if (isIntersected(compShape[i]->getFrameRect(), matrix[j][k]->getFrameRect()))
        {
          layer++;
          intersect = true;
          break;
        }
      }
      if (!intersect)
      {
        break;
      }
    }
    matrix.add(compShape[i], layer);
  }
  return matrix;
}

bool kovbun::isIntersected(const kovbun::rectangle_t& shape1, const kovbun::rectangle_t& shape2)
{
  const double distanceX = fabs(shape1.pos.x - shape2.pos.x);
  const double distanceY = fabs(shape1.pos.y - shape2.pos.y);

  const double lengthX = (shape1.width + shape2.width) / 2;
  const double lengthY = (shape1.height + shape2.height) / 2;

  const bool firstCondition = (distanceX < lengthX);
  const bool secondCondition = (distanceY < lengthY);

  return firstCondition&& secondCondition;
}

