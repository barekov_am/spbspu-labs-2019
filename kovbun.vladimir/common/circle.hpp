#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

namespace kovbun
{
  class Circle : public Shape {
  public:
    Circle(double radius, const point_t &center);

    double getArea() const override;

    rectangle_t getFrameRect() const override;
    void move(const point_t& point) override;
    void move(double dx, double dy) override;
    void scale(double multiplier) override;
    void rotate(double /*angle*/) override;
    void writeInfo() const override;

  private:
    double radius_;
    point_t pos_;
  };
}

#endif


