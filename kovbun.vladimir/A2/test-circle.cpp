#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(A3Test)

const double PRECISION = 0.001;

BOOST_AUTO_TEST_CASE(invariabilityAfterMovingCircleByIncrement)
{
  kovbun::Circle testCircle(3, { 9, -4 });
  const kovbun::rectangle_t frameBefore = testCircle.getFrameRect();
  const double initialArea = testCircle.getArea();

  testCircle.move(4, 4);
  BOOST_CHECK_CLOSE(frameBefore.width, testCircle.getFrameRect().width, PRECISION);
  BOOST_CHECK_CLOSE(frameBefore.height, testCircle.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(initialArea, testCircle.getArea(), PRECISION);
}

BOOST_AUTO_TEST_CASE(invariabilityAfterMovingCircleToPoint)
{
  kovbun::Circle testCircle(3, { 9, -4 });
  const kovbun::rectangle_t frameBefore = testCircle.getFrameRect();
  const double initialArea = testCircle.getArea();

  testCircle.move(-14, -7);
  BOOST_CHECK_CLOSE(frameBefore.width, testCircle.getFrameRect().width, PRECISION);
  BOOST_CHECK_CLOSE(frameBefore.height, testCircle.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(initialArea, testCircle.getArea(), PRECISION);
}

BOOST_AUTO_TEST_CASE(quadraticCircleScaleChange)
{
  kovbun::Circle testCircle(6, { 13.3, 7 });
  const double initialArea = testCircle.getArea();
  const double multiplier = 2.0;

  testCircle.scale(multiplier);
  BOOST_CHECK_CLOSE(initialArea * multiplier * multiplier, testCircle.getArea(), PRECISION);
}

BOOST_AUTO_TEST_CASE(invalidCircleParametersAndScaling)
{
  BOOST_CHECK_THROW(kovbun::Circle testCircle(-5.7, { 30, 6 }), std::invalid_argument);

  kovbun::Circle testCircle(5.7, { 30, 6 } );
  BOOST_CHECK_THROW(testCircle.scale(-3.4), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

