#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(A2Test)

const double PRECISION = 0.001;

BOOST_AUTO_TEST_CASE(invariabilityAfterMovingRectangleByIncrements)
{
  kovbun::Rectangle testRectangle(7, 6, { 3, 4 });
  const kovbun::rectangle_t initialBefore = testRectangle.getFrameRect();
  const double initialArea = testRectangle.getArea();

  testRectangle.move(-4.5, 5.2);
  BOOST_CHECK_CLOSE(initialBefore.width, testRectangle.getFrameRect().width, PRECISION);
  BOOST_CHECK_CLOSE(initialBefore.height, testRectangle.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), PRECISION);
}

BOOST_AUTO_TEST_CASE(invariabilityAfterMovingRectangleToPoint)
{
  kovbun::Rectangle testRectangle(7, 6, { 3, 4 });
  const kovbun::rectangle_t initialBefore = testRectangle.getFrameRect();
  const double initialArea = testRectangle.getArea();

  testRectangle.move(-4.5, 5.2);
  BOOST_CHECK_CLOSE(initialBefore.width, testRectangle.getFrameRect().width, PRECISION);
  BOOST_CHECK_CLOSE(initialBefore.height, testRectangle.getFrameRect().height, PRECISION);
  BOOST_CHECK_CLOSE(initialArea, testRectangle.getArea(), PRECISION);
}

BOOST_AUTO_TEST_CASE(quadraticRectangleScaleChange)
{
  kovbun::Rectangle testRectangle(3, 7, { 4, 5 });
  const double initialArea = testRectangle.getArea();
  const double multiplier = 0.5;

  testRectangle.scale(multiplier);
  BOOST_CHECK_CLOSE(testRectangle.getArea(), initialArea * multiplier * multiplier, PRECISION);
}

BOOST_AUTO_TEST_CASE(invalidRectangleParametersAndScaling)
{
  BOOST_CHECK_THROW(kovbun::Rectangle(-4, 5, { 6, 7 }), std::invalid_argument);

  kovbun::Rectangle testRectangle(4, 5, { 6, 7 });
  BOOST_CHECK_THROW(testRectangle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

