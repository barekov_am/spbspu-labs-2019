#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void printInfo(const Shape &shape)
{
  shape.writeInfo();
}

int main()
{
  point_t pointCenter = {1.0, 1.0};
  Circle circle(pointCenter, 10.0);
  Rectangle rectangle(pointCenter, 20.0, 30.0);
  point_t t1 = {6.1, 2.5};
  point_t t2 = {3.1, 1.5};
  point_t t3 = {5.1, 7.5};
  Triangle triangle(t1, t2, t3);
  const size_t qty = 5;
  const point_t vertices[qty] = {{1.0, 4.0}, {3.0, 2.0}, {5.0, 3.0}, {6.0, 5.0}, {3.0, 6.0}};
  Polygon polygon(vertices, qty);


  printInfo(circle);
  printInfo(rectangle);
  printInfo(triangle);
  printInfo(polygon);

  std::cout << "Circle: move (2.0, 2.0) " << std::endl;
  circle.move(2.0, 2.0);
  printInfo(circle);

  std::cout << "Rectangle: move (4.0, 4.0) " << std::endl;
  rectangle.move(4.0, 4.0);
  printInfo(rectangle);

  std::cout << "Triangle: move (3.0, 3.0) " << std::endl;
  triangle.move(3.0, 3.0);
  printInfo(triangle);

  std::cout<<"Polygon: move(3.0, -1.0)"<< std::endl;
  polygon.move(3.00, -1.0);
  printInfo(polygon);

  std::cout<<"Polygon: move(1.00, 2.0)"<< std::endl;
  polygon.move({1.00, 2.0});
  printInfo(polygon);

  return 0;
}
