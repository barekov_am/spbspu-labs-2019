#include "polygon.hpp"
#include <cstddef>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <algorithm>

Polygon::Polygon(const point_t* vertices, const size_t& qtyVertex):
  qtyVertex_(qtyVertex),
  vertices_(new point_t[qtyVertex])
{
  if (qtyVertex <= 2)
  {
    std::invalid_argument("Quantity of vertex must be more then 2.");
  }

  if (vertices == nullptr)
  {
    std::invalid_argument("Pointer to vertex is null.");
  }

  for (size_t index = 0; index < qtyVertex_; index++)
  {
    vertices_[index] = vertices[index];
  }

  if (checkBump())
  {
    std::invalid_argument("Polygon must be convex.");
  }

  if (getArea() == 0.0)
  {
    std::invalid_argument("Area must be more then 0.");
  }
}

Polygon::Polygon(const Polygon& other):
  qtyVertex_(other.qtyVertex_),
  vertices_(new point_t[qtyVertex_])
{
   for (size_t index = 0; index < qtyVertex_; index++)
   {
     vertices_[index] = other.vertices_[index];
   }
}

Polygon& Polygon::operator =(const Polygon& other)
{
  if (&other != this)
  {
    qtyVertex_ = other.qtyVertex_;
    for (size_t index = 0; index < qtyVertex_; index++)
    {
      vertices_[index] = other.vertices_[index];
    }
  }
  return *this;
}

Polygon::~Polygon()
{
  delete[] vertices_;
}

void Polygon::writeInfo() const noexcept
{
  if (qtyVertex_ != 0)
  {
    std::cout << "\n" << "Information about Polygon" << "\n";
    const point_t pos = getPos();
    std::cout << "Center: " << pos.x << ',' << pos.y << "\n";
    std::cout << "Vertices of polygon: ";

    for (size_t index = 0; index < qtyVertex_; index++)
    {
      std::cout << std::endl << index + 1 << ".\t(" << vertices_[index].x  << ';' << vertices_[index].y << ')';
    }

  }
  else
  {
    std::cout << "Polygon is empty.";
  }
}

double Polygon::getArea() const noexcept
{
  if (qtyVertex_ == 0)
  {
    return 0.0;
  }

  double sum = vertices_[qtyVertex_ - 1].x * vertices_[0].y - vertices_[0].x * vertices_[qtyVertex_ - 1].y;
  for (size_t index = 0; index < qtyVertex_ - 1; index++)
  {
    sum += vertices_[index].x * vertices_[index + 1].y - vertices_[index + 1].x * vertices_[index].y;
  }

  return 0.5 * fabs(sum);
}

rectangle_t Polygon::getFrameRect() const noexcept
{
  if (qtyVertex_ == 0)
  {
    return {{0.0, 0.0}, 0.0, 0.0};
  }

  point_t max = vertices_[0];
  point_t min = vertices_[0];

  for (size_t index = 1; index < qtyVertex_; index++)
  {
    max.x = std::max(max.x, vertices_[index].x);
    min.x = std::min(min.x, vertices_[index].x);
    max.y = std::max(max.y, vertices_[index].y);
    min.y = std::min(min.y, vertices_[index].y);
  }

  const double width = max.x - min.x;
  const double height = max.y - min.y;
  const point_t pos = {min.x + width / 2, min.y + height / 2};

  return {pos, width, height};
}

void Polygon::move(const point_t& newPosition) noexcept
{
  const point_t pos = getPos();
  move(newPosition.x - pos.x, newPosition.y - pos.y);
}

void Polygon::move(double dx, double dy) noexcept
{
  for (size_t index = 0; index < qtyVertex_; index++)
  {
    vertices_[index].x += dx;
    vertices_[index].y += dy;
  }
}

point_t Polygon::getPos() const noexcept
{
  point_t pos = {0.0, 0.0};

  for (size_t index = 0; index < qtyVertex_; index++)
  {
    pos.x += vertices_[index].x;
    pos.y += vertices_[index].y;
  }

  return pos;
}

bool Polygon::checkBump() const noexcept
{
  for (size_t index = 0; index <= qtyVertex_ - 2 ; index++)
  {
    point_t firstSide = {0.0, 0.0};
    point_t secondSide = {0.0, 0.0};
    firstSide.x = vertices_[index + 1].x - vertices_[index].x;
    firstSide.y = vertices_[index + 1].y - vertices_[index].y;
    secondSide.x = vertices_[(index + 2) % qtyVertex_].x - vertices_[index + 1].x;
    secondSide.y = vertices_[(index + 2) % qtyVertex_].y - vertices_[index + 1].y;

    if (((firstSide.x * secondSide.y) - (firstSide.y * secondSide.x)) < 0.0)
    {
      return true;
    }
  }
  return false;
}

void Polygon::swap(Polygon& other) noexcept
{
  std::swap(qtyVertex_, other.qtyVertex_);
  std::swap(vertices_, other.vertices_);
}
