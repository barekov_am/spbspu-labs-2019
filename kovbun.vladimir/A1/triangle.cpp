#include "triangle.hpp"
#include <cmath>
#include <iostream>
#include <cassert>

Triangle::Triangle(const point_t &t1, const point_t &t2, const point_t &t3):
  point1_(t1),
  point2_(t2),
  point3_(t3)
{
  assert(getArea() > 0);
}

point_t Triangle::getPos() const
{
  point_t point = {0.0, 0.0};
  point.x = (point1_.x + point2_.x + point3_.x) / 3;
  point.y = (point1_.y + point2_.y + point3_.y) / 3;
  return point;
}

void Triangle::move(const double dx, const double dy)
{
  point1_.x += dx;
  point1_.y += dy;
  point2_.x += dx;
  point2_.y += dy;
  point3_.x += dx;
  point3_.y += dy;
}

void Triangle::move(const point_t &center)
{
  point_t point = getPos();
  const double dx1 = center.x - point.x;
  const double dy1 = center.y - point.y;
  move(dx1, dy1);
}

double Triangle::getArea() const
{
  return fabs(0.5 * (point1_.x * (point2_.y - point3_.y) + point2_.x * (point3_.y - point1_.y) + point3_.x * (point1_.y - point2_.y)));
}

rectangle_t Triangle::getFrameRect() const
{
  const double width = fabs(sqrt(pow(point1_.x - point2_.x, 2) - pow(point1_.y - point2_.y, 2)));
  const double height = 2 * getArea() / width;
  return rectangle_t {point_t {getPos()}, width, height};
}

void Triangle::writeInfo() const
{
  rectangle_t rect = getFrameRect();
  point_t point = getPos();
  std::cout << "Information about the Triangle: \n";
  std::cout << "Width and height of the rectangle: " << rect.width << "  " << rect.height << std::endl;
  std::cout << "Area: " << getArea() << std::endl;
  std::cout << "Center: " << point.x << "  " << point.y << "\n"
            << std::endl;
}
