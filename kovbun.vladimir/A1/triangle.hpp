#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"


class Triangle: public Shape
{
public:
  Triangle(const point_t &t1, const point_t &t2, const point_t &t3);

  double getArea() const override;
  rectangle_t getFrameRect() const override;
  point_t getPos() const;
  void move(const point_t &center) override;
  void move(double dx, double dy) override;
  void writeInfo() const override;

private:
  point_t point1_;
  point_t point2_;
  point_t point3_;
};

#endif // TRIANGLE_HPP
