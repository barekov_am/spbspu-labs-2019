#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

void printData(const double Area, const novoseltseva::rectangle_t rect)
{
  std::cout << "Area = " << Area << "\n";
  std::cout << "pos-{ " << rect.pos.x << ", " << rect.pos.y << " }";
  std::cout << " frameRect: width-" << rect.width << "  height-" << rect.height << std::endl;
};

int main()
{
  novoseltseva::Circle newCircle({ 9, 9 }, 9);
  std::cout << "\nCreated new Circle\n";
  novoseltseva::Shape* shape = &newCircle;
  printData(newCircle.getArea(), newCircle.getFrameRect());
  std::cout << "Circle with a new centre (6, 6):\n";
  shape->move({ 6, 6 });
  printData(newCircle.getArea(), newCircle.getFrameRect());
  std::cout << "Circle moved = 3.5 dy= 2.8:\n";
  shape->move(3.5, 2.8);
  printData(newCircle.getArea(), newCircle.getFrameRect());
  std::cout << "Circle scale * 2\n";
  shape->scale(2.0);
  printData(newCircle.getArea(), newCircle.getFrameRect());

  novoseltseva::Rectangle newRectangle({ 4, 5 }, 7, 3);
  std::cout << "\nCreated new Rectangle\n";
  shape = &newRectangle;
  printData(newRectangle.getArea(), newRectangle.getFrameRect());
  std::cout << "Rectangle with a new centre  (-16, 2):\n";
  shape->move({ -16, 2 });
  printData(newRectangle.getArea(), newRectangle.getFrameRect());
  std::cout << "Rectangle moved  dx=12, dy=3:\n";
  shape->move(12, 3);
  printData(newRectangle.getArea(), newRectangle.getFrameRect());
  std::cout << "Rectangle scale * 0.5\n";
  shape->scale(0.5);
  printData(newRectangle.getArea(), newRectangle.getFrameRect());

  novoseltseva::Triangle triang({ 1, 1 }, { 2, 3 }, { 3, 1 });
  std::cout << "\nCreated Triangle\n";
  triang.printCenter();
  printData(triang.getArea(), triang.getFrameRect());
  std::cout << "Triangle with a new centre (7, 2):\n";
  triang.move({ 7, 2 });
  triang.printCenter();
  printData(triang.getArea(), triang.getFrameRect());
  std::cout << "Triangle moved = 10, dy= 15:\n";
  triang.move(10, 15);
  triang.printCenter();
  printData(triang.getArea(), triang.getFrameRect());
  std::cout << "Triangle scale * 3\n";
  triang.scale(3.0);
  triang.printCenter();
  printData(triang.getArea(), triang.getFrameRect());

  const int size = 5;
  const novoseltseva::point_t points[size] = { {9, 1}, {1, 1}, {2, 3}, {1, 6}, {4, 3} };
  novoseltseva::Polygon polygon(points, size);
  std::cout << "\nCreated Polygon\n";
  printData(polygon.getArea(), polygon.getFrameRect());
  polygon.printCenter();
  std::cout << "Polygon with a new centre (7, 2):\n";
  polygon.move({ 7, 2 });
  printData(polygon.getArea(), polygon.getFrameRect());
  polygon.printCenter();
  std::cout << "Polygon moved = 10, dy= 15:\n";
  polygon.move(10, 15);
  printData(polygon.getArea(), polygon.getFrameRect());
  polygon.printCenter();
  std::cout << "polygon scale * 2\n";
  polygon.scale(0.5);
  printData(polygon.getArea(), polygon.getFrameRect());
  polygon.printCenter();

  novoseltseva::CompositeShape compositeShape(std::make_shared<novoseltseva::Rectangle>(newRectangle));
  compositeShape.add(std::make_shared<novoseltseva::Circle>(newCircle));
  compositeShape.add(std::make_shared<novoseltseva::Triangle>(triang));
  compositeShape.add(std::make_shared<novoseltseva::Polygon>(polygon));
  std::cout << "\nArea = " << compositeShape.getArea() << std::endl;
  compositeShape.printInfo();
  compositeShape.scale(2.0);
  std::cout << "\nArea = " << compositeShape.getArea() << std::endl;
  compositeShape.printInfo();

  return 0;
}
