#define _USE_MATH_DEFINES
#include "circle.hpp"

#include <cassert>
#include <cmath>
#include <stdexcept>

novoseltseva::Circle::Circle(const point_t center, const double radius) :
  center_(center),
  radius_(radius)
{
  if (radius_ <= 0.0)
  {
    throw std::invalid_argument("Radius has invalid value");
  }
}

double novoseltseva::Circle::getArea() const
{
  return radius_ * radius_* M_PI;
}

novoseltseva::rectangle_t novoseltseva::Circle::getFrameRect() const
{
  return { center_, radius_ * 2, radius_ * 2 };
}

void novoseltseva::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void novoseltseva::Circle::move(const point_t& newPos)
{
  center_ = newPos;
}

void novoseltseva::Circle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("factor cannot be <= 0 ");
  }
  radius_ *= factor;
}

void novoseltseva::Circle::rotate(double)
{
}
