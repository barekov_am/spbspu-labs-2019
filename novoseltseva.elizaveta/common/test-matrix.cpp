//#include <stdexcept>
//#include <boost/test/auto_unit_test.hpp>
//
//#include "circle.hpp"
//#include "rectangle.hpp"
//#include "triangle.hpp"
//#include "composite-shape.hpp"
//#include "matrix.hpp"
//#include "partition.hpp"
//
//const double Inaccuracy = 0.01;
//
//BOOST_AUTO_TEST_SUITE(testForMatrix)
//
//BOOST_AUTO_TEST_CASE(copyAndMove)
//{
//  novoseltseva::Rectangle rectangle({ 4, 6 }, 6, 7);
//  novoseltseva::CompositeShape compositeShape(std::make_shared<novoseltseva::Rectangle>(rectangle));
//  novoseltseva::Matrix matrix = novoseltseva::division(compositeShape);
//
//  novoseltseva::Matrix matrix3 = matrix;
//
//  BOOST_CHECK_EQUAL(matrix3.getLines(), 1);
//  BOOST_CHECK_EQUAL(matrix3.getColumns(), 1);
//  BOOST_CHECK_NO_THROW(novoseltseva::Matrix matrix1(matrix));
//  BOOST_CHECK_NO_THROW(novoseltseva::Matrix matrix2(std::move(matrix)));
//
//  novoseltseva::Matrix matrix4;
//  novoseltseva::Matrix matrix5;
//
//  BOOST_CHECK_NO_THROW(matrix4 = matrix);
//  BOOST_CHECK_NO_THROW(matrix5 = std::move(matrix));
//}
//
//BOOST_AUTO_TEST_CASE(testForThrow)
//{
//  novoseltseva::Rectangle rectangle({ 2, 4.5 }, 1, 2);
//  novoseltseva::Rectangle rectangle1({ 3.5, 2 }, 2, 5);
//  novoseltseva::Circle circle({ 6, 4 }, 2);
//  novoseltseva::Triangle triangle({ 3, 2 }, { 2, 0 }, { 4, 0 });
//  novoseltseva::Triangle triangle1({ 7, 4 }, { 10, 3 }, { 10, 5 });
//  novoseltseva::CompositeShape compositeShape(std::make_shared<novoseltseva::Rectangle>(rectangle));
//  compositeShape.add(std::make_shared<novoseltseva::Rectangle>(rectangle1));
//  compositeShape.add(std::make_shared<novoseltseva::Circle>(circle));
//  compositeShape.add(std::make_shared<novoseltseva::Triangle>(triangle));
//  compositeShape.add(std::make_shared<novoseltseva::Triangle>(triangle1));
//
//  novoseltseva::Matrix matrix = novoseltseva::division(compositeShape);
//  BOOST_CHECK_THROW(matrix[10][10], std::out_of_range);
//  BOOST_CHECK_NO_THROW(matrix[0][1]);
//
//}
//
//BOOST_AUTO_TEST_SUITE_END()
