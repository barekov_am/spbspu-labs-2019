#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>

novoseltseva::CompositeShape::CompositeShape() :
  size_(0),
  shapeArray_(nullptr)
{}

novoseltseva::CompositeShape::CompositeShape(const novoseltseva::CompositeShape& rhs) :
  size_(rhs.size_),
  shapeArray_(std::make_unique<shapePtr[]>(rhs.size_))
{
  for (size_t i = 0; i < size_; i++)
  {
    shapeArray_[i] = rhs.shapeArray_[i];
  }
}

novoseltseva::CompositeShape::CompositeShape(novoseltseva::CompositeShape&& rhs) :
  size_(rhs.size_),
  shapeArray_(std::move(rhs.shapeArray_))
{
  rhs.size_ = 0;
}

novoseltseva::CompositeShape::CompositeShape(shapePtr shape) :
  size_(1),
  shapeArray_(std::make_unique<shapePtr[]>(size_))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Your shape is nullptr!");
  }
  shapeArray_[0] = shape;
}

novoseltseva::CompositeShape& novoseltseva::CompositeShape::operator =(const novoseltseva::CompositeShape& rhs)
{
  if (this != &rhs)
  {
    size_ = rhs.size_;
    std::unique_ptr<shapePtr[]> temp(std::make_unique<shapePtr[]>(rhs.size_));
    for (size_t i = 0; i < size_; i++)
    {
      temp[i] = rhs.shapeArray_[i];
    }
    shapeArray_.swap(temp);
  }
  return *this;
}

novoseltseva::CompositeShape& novoseltseva::CompositeShape::operator =(novoseltseva::CompositeShape&& rhs)
{
  if (this != &rhs)
  {
    size_ = rhs.size_;
    shapeArray_ = std::move(rhs.shapeArray_);
    rhs.size_ = 0;
  }
  return *this;
}

novoseltseva::CompositeShape::shapePtr novoseltseva::CompositeShape::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("There is no figure!");
  }
  return shapeArray_[index];
}

void novoseltseva::CompositeShape::add(shapePtr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("You didn't add shape in Constructor or new object is nullptr!!!");
  }
  size_++;
  std::unique_ptr<shapePtr[]> temp(std::make_unique<shapePtr[]>(size_));
  for (size_t i = 0; i < size_ - 1; i++)
  {
    temp[i] = shapeArray_[i];
  }
  temp[size_ - 1] = shape;
  shapeArray_ = std::move(temp);
}

void novoseltseva::CompositeShape::remove(shapePtr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("You gave nullptr!");
  }
  size_t i = 0;
  while ((i < size_) && (shape != shapeArray_[i]))
  {
    i++;
  }
  if (i == size_)
  {
    throw std::invalid_argument("There is no such figure in the array");
  }
  remove(i);
}

void novoseltseva::CompositeShape::remove(size_t i)
{
  if (i > size_ - 1)
  {
    throw std::out_of_range("The index is greater than number of figures!");
  }

  for (size_t j = i; j < size_ - 1; j++)
  {
    shapeArray_[j] = shapeArray_[j + 1];
  }
  size_--;
}

size_t novoseltseva::CompositeShape::getCount() const
{
  return size_;
}

void novoseltseva::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; i++)
  {
    shapeArray_[i]->move(dx, dy);
  }
}

void novoseltseva::CompositeShape::move(const novoseltseva::point_t& center)
{
  point_t shift = { center.x - getFrameRect().pos.x, center.y - getFrameRect().pos.y };
  move(shift.x, shift.y);
}

double novoseltseva::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < size_; i++)
  {
    area += shapeArray_[i]->getArea();
  }
  return area;
}

novoseltseva::point_t getBotAndTop(novoseltseva::rectangle_t frame, int factor)
{
  double x = frame.pos.x + (factor * (frame.width / 2));
  double y = frame.pos.y + (factor * (frame.height / 2));
  return { x, y };
}

novoseltseva::rectangle_t novoseltseva::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::logic_error("You didn't add shape in array");
  }

  point_t maxXAndY = getBotAndTop(shapeArray_[0]->getFrameRect(), 1);
  point_t minXAndY = getBotAndTop(shapeArray_[0]->getFrameRect(), (-1));
  for (size_t i = 1; i < size_; i++)
  {
    maxXAndY.x = std::max(getBotAndTop(shapeArray_[i]->getFrameRect(), 1).x, maxXAndY.x);
    maxXAndY.y = std::max(getBotAndTop(shapeArray_[i]->getFrameRect(), 1).y, maxXAndY.y);
    minXAndY.x = std::min(getBotAndTop(shapeArray_[i]->getFrameRect(), (-1)).x, minXAndY.x);
    minXAndY.y = std::min(getBotAndTop(shapeArray_[i]->getFrameRect(), (-1)).y, minXAndY.y);
  }
  double height = maxXAndY.y - minXAndY.y;
  double width = maxXAndY.x - minXAndY.x;
  point_t center = { minXAndY.x + width / 2, minXAndY.y + height / 2 };
  return { center, height, width };
}

void novoseltseva::CompositeShape::printInfo() const
{
  for (size_t i = 0; i < size_; i++)
  {
    std::cout << "Center= {" << shapeArray_[i]->getFrameRect().pos.x << ", " << shapeArray_[i]->getFrameRect().pos.y << "}\n";
  }
}

void novoseltseva::CompositeShape::scale(double factor)
{
  point_t point = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    point_t shift = { shapeArray_[i]->getFrameRect().pos.x - point.x, shapeArray_[i]->getFrameRect().pos.y - point.y };
    shapeArray_[i]->move(shift.x * (factor - 1), shift.y * (factor - 1));
    shapeArray_[i]->scale(factor);
  }
}

void novoseltseva::CompositeShape::rotate(double angle)
{
  const double cos = std::cos((2 * M_PI * angle) / 360);
  const double sin = std::sin((2 * M_PI * angle) / 360);

  const point_t compCentre = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++)
  {
    const point_t center = shapeArray_[i]->getFrameRect().pos;
    const double dx = (center.x - compCentre.x) * cos - (center.y - compCentre.y) * sin;
    const double dy = (center.x - compCentre.x) * sin + (center.y - compCentre.y) * cos;
    shapeArray_[i]->move({ compCentre.x + dx, compCentre.y + dy });
    shapeArray_[i]->rotate(angle);
  }
}
