#ifndef SEPARATION_HPP
#define SEPARATION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"
#include <memory>

namespace novoseltseva
{
  bool checkIntersect(novoseltseva::rectangle_t shape1, novoseltseva::rectangle_t shape2);
  Matrix division(std::unique_ptr<novoseltseva::Shape::shapePtr[]>& shapes, size_t size);
  Matrix division(CompositeShape& compositeShape);
}


#endif
