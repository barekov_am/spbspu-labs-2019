#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

namespace novoseltseva
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t first, const point_t second, const point_t thrid);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const double dx, const double dy) override;
    void move(const point_t& newPos) override;
    void printCenter() const;
    void scale(double factor) override;
    void rotate(double angle) override;
  private:
    point_t first_;
    point_t second_;
    point_t third_;
    point_t getCenter() const;
    point_t center_;
  };
}

#endif
