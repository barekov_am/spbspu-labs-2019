//#include <stdexcept>
//#include <boost/test/auto_unit_test.hpp>
//#include "rectangle.hpp"
//
//const double fault = 0.01;
//
//BOOST_AUTO_TEST_SUITE(rectangleTest)
//
//BOOST_AUTO_TEST_CASE(invarianceRectParametersAfterMovingTo)
//{
//  novoseltseva::Rectangle tempRectangle({ 4.0, 3.0 }, 5.2, 6.5);
//  const double initialArea = tempRectangle.getArea();
//  const novoseltseva::rectangle_t initialRectangle = tempRectangle.getFrameRect();
//
//  tempRectangle.move({ 14.3, 5.2 });
//
//  BOOST_CHECK_CLOSE(initialRectangle.width, tempRectangle.getFrameRect().width, fault);
//  BOOST_CHECK_CLOSE(initialRectangle.height, tempRectangle.getFrameRect().height, fault);
//  BOOST_CHECK_CLOSE(initialArea, tempRectangle.getArea(), fault);
//}
//
//BOOST_AUTO_TEST_CASE(invarianceRectParametersAfterMovingBy)
//{
//  novoseltseva::Rectangle tempRectangle({ 4.0, 3.0 }, 5.2, 6.5);
//  const double initialArea = tempRectangle.getArea();
//  const novoseltseva::rectangle_t initialRectangle = tempRectangle.getFrameRect();
//
//  tempRectangle.move(3.4, 7.5);
//
//  BOOST_CHECK_CLOSE(initialRectangle.width, tempRectangle.getFrameRect().width, fault);
//  BOOST_CHECK_CLOSE(initialRectangle.height, tempRectangle.getFrameRect().height, fault);
//  BOOST_CHECK_CLOSE(initialArea, tempRectangle.getArea(), fault);
//}
//
//BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterScale)
//{
//  novoseltseva::Rectangle tempRectangle({ 4.0, 3.0 }, 5.2, 6.5);
//  const double initialArea = tempRectangle.getArea();
//  double factor = 2.0;
//
//  tempRectangle.scale(factor);
//
//  BOOST_CHECK_CLOSE(initialArea * factor * factor, tempRectangle.getArea(), fault);
//}
//
//BOOST_AUTO_TEST_CASE(invalidValues)
//{
//  BOOST_CHECK_THROW(novoseltseva::Rectangle rectangle({ 4.0, 3.0 }, 0, 6.5), std::invalid_argument);
//  BOOST_CHECK_THROW(novoseltseva::Rectangle rectangle({ 4.0, 3.0 }, 5.2, 0), std::invalid_argument);
//  BOOST_CHECK_THROW(novoseltseva::Rectangle rectangle({ 4.0, 3.0 }, -5.2, 6.5), std::invalid_argument);
//  BOOST_CHECK_THROW(novoseltseva::Rectangle rectangle({ 4.0, 3.0 }, 5.2, -6.5), std::invalid_argument);
//
//  novoseltseva::Rectangle tempRectangle({ 12.5, 9.5 }, 10.0, 12.0);
//
//  BOOST_CHECK_THROW(tempRectangle.scale(0.0), std::invalid_argument);
//  BOOST_CHECK_THROW(tempRectangle.scale(-2.0), std::invalid_argument);
//}
//
//BOOST_AUTO_TEST_SUITE_END()
