//#include <stdexcept>
//#include <boost/test/auto_unit_test.hpp>
//#include "circle.hpp"
//
//const double fault = 0.01;
//
//BOOST_AUTO_TEST_SUITE(circleTest)
//
//BOOST_AUTO_TEST_CASE(invarianceRectParametersAfterMovingTo)
//{
//  novoseltseva::Circle tempCircle({ 3.2, 4.5 }, 2.5);
//  const double initialArea = tempCircle.getArea();
//  const novoseltseva::rectangle_t initialRectangle = tempCircle.getFrameRect();
//
//  tempCircle.move({ 4.2, 5.2 });
//
//  BOOST_CHECK_CLOSE(initialRectangle.width, tempCircle.getFrameRect().width, fault);
//  BOOST_CHECK_CLOSE(initialRectangle.height, tempCircle.getFrameRect().height, fault);
//  BOOST_CHECK_CLOSE(initialArea, tempCircle.getArea(), fault);
//}
//
//BOOST_AUTO_TEST_CASE(invarianceRectParametersAfterMovingBy)
//{
//  novoseltseva::Circle tempCircle({ 3.2, 4.5 }, 2.5);
//  const double initialArea = tempCircle.getArea();
//  const novoseltseva::rectangle_t initialRectangle = tempCircle.getFrameRect();
//
//  tempCircle.move(3.5, 17.7);
//
//  BOOST_CHECK_CLOSE(initialRectangle.width, tempCircle.getFrameRect().width, fault);
//  BOOST_CHECK_CLOSE(initialRectangle.height, tempCircle.getFrameRect().height, fault);
//  BOOST_CHECK_CLOSE(initialArea, tempCircle.getArea(), fault);
//}
//
//BOOST_AUTO_TEST_CASE(squareChangeOfAreaAfterScale)
//{
//  novoseltseva::Circle tempCircle({ 3.2, 4.5 }, 2.5);
//  const double initialArea = tempCircle.getArea();
//  double factor = 2.5;
//
//  tempCircle.scale(factor);
//
//  BOOST_CHECK_CLOSE(initialArea * factor * factor, tempCircle.getArea(), fault);
//}
//
//BOOST_AUTO_TEST_CASE(invalidValues)
//{
//  BOOST_CHECK_THROW(novoseltseva::Circle circle({ 3.2, 4.5 }, 0), std::invalid_argument);
//  BOOST_CHECK_THROW(novoseltseva::Circle circle({ 3.2, 4.5 }, -5), std::invalid_argument);
//
//  novoseltseva::Circle tempCircle({ 6.2, 7.3 }, 5.2);
//
//  BOOST_CHECK_THROW(tempCircle.scale(0.0), std::invalid_argument);
//  BOOST_CHECK_THROW(tempCircle.scale(-5.0), std::invalid_argument);
//}
//
//BOOST_AUTO_TEST_SUITE_END()
