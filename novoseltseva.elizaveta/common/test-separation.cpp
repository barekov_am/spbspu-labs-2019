//#include <boost/test/auto_unit_test.hpp>
//#include <stdexcept>
//#include "circle.hpp"
//#include "rectangle.hpp"
//#include "triangle.hpp"
//#include "composite-shape.hpp"
//#include "matrix.hpp"
//#include "separation.hpp"
//
//BOOST_AUTO_TEST_SUITE(partitionMetodsTests)
//
//BOOST_AUTO_TEST_CASE(partitionTest)
//{
//  novoseltseva::Rectangle rectangle({ 2, 4.5 }, 1, 2);
//  novoseltseva::Rectangle rectangle1({ 3.5, 2 }, 2, 5);
//  novoseltseva::Circle circle({ 6, 4 }, 2);
//
//  novoseltseva::CompositeShape compositeShape(std::make_shared<novoseltseva::Rectangle>(rectangle));
//  compositeShape.add(std::make_shared<novoseltseva::Rectangle>(rectangle1));
//  compositeShape.add(std::make_shared<novoseltseva::Circle>(circle));
//
//  novoseltseva::Matrix matrix = novoseltseva::division(compositeShape);
//
//  BOOST_CHECK_EQUAL(matrix.getLines(), 2);
//  BOOST_CHECK_EQUAL(matrix.getColumns(), 2);
//}
//
//BOOST_AUTO_TEST_CASE(intersectionTest)
//{
//  novoseltseva::Rectangle rectangle({ 2, 4.5 }, 1, 2);
//  novoseltseva::Rectangle rectangle1({ 3.5, 2 }, 2, 5);
//  novoseltseva::Circle circle({ 6, 4 }, 2);
//
//  BOOST_CHECK(!novoseltseva::checkIntersect(rectangle.getFrameRect(), rectangle1.getFrameRect()));
//  BOOST_CHECK(!novoseltseva::checkIntersect(rectangle.getFrameRect(), circle.getFrameRect()));
//  BOOST_CHECK(novoseltseva::checkIntersect(rectangle1.getFrameRect(), circle.getFrameRect()));
//}
//
//BOOST_AUTO_TEST_SUITE_END()
