#define _USE_MATH_DEFINES
#include "triangle.hpp"

#include <iostream>
#include <cassert>
#include <cmath>
#include <algorithm>

novoseltseva::Triangle::Triangle(const point_t first, const point_t second, const point_t thrid) :
  first_(first),
  second_(second),
  third_(thrid),
  center_(getCenter())
{
  if (getArea() == 0)
  {
    throw std::invalid_argument("Area mustn't be = 0");
  }
}

double getDistance(const novoseltseva::point_t first_, const novoseltseva::point_t second_)
{
  return sqrt((pow((second_.x - first_.x), 2)) + (pow((second_.y - first_.y), 2)));
}

double novoseltseva::Triangle::getArea() const
{
  double firstVector = getDistance(first_, second_);
  double secondVector = getDistance(second_, third_);
  double thirdVector = getDistance(third_, first_);
  double p = ((firstVector + secondVector + thirdVector) / 2);
  return (sqrt(p * (p - firstVector) * (p - secondVector) * (p - thirdVector)));
}

novoseltseva::rectangle_t novoseltseva::Triangle::getFrameRect() const
{
  double left = std::min({ first_.x, second_.x, third_.x });
  double bot = std::min({ first_.y, second_.y, third_.y });
  double height = std::max({ first_.x, second_.x, third_.x }) - left;
  double width = std::max({ first_.y, second_.y, third_.y }) - bot;
  point_t pos = { left + (height / 2), bot + (width / 2) };

  return { pos, width, height };
}

void novoseltseva::Triangle::move(const double dx, const double dy)
{
  first_.x += dx;
  first_.y += dy;
  second_.x += dx;
  second_.y += dy;
  third_.x += dx;
  third_.y += dy;
  center_.x += dx;
  center_.y += dy;
}

void novoseltseva::Triangle::move(const point_t & newPos)
{
  move((newPos.x - center_.x), (newPos.y - center_.y));
}

novoseltseva::point_t novoseltseva::Triangle::getCenter() const
{
  return { (first_.x + second_.x + third_.x) / 3, (first_.y + second_.y + third_.y) / 3 };
}

void novoseltseva::Triangle::printCenter() const
{
  std::cout << "triangle pos-{ " << center_.x << ", " << center_.y << " }" << std::endl;
}

void novoseltseva::Triangle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("factor cannot be <= 0 ");
  }
  first_.x = center_.x + (first_.x - center_.x) * factor;
  first_.y = center_.y + (first_.y - center_.y) * factor;
  second_.x = center_.x + (second_.x - center_.x) * factor;
  second_.y = center_.y + (second_.y - center_.y) * factor;
  third_.x = center_.x + (third_.x - center_.x) * factor;
  third_.y = center_.y + (third_.y - center_.y) * factor;
}

novoseltseva::point_t rotateOneCoordinate(novoseltseva::point_t point, double angle)
{
  double x = point.x * cos(M_PI * angle / 180) - point.y * sin(M_PI * angle / 180);
  double y = point.y * cos(M_PI * angle / 180) + point.x * sin(M_PI * angle / 180);
  return { x, y };
}

void novoseltseva::Triangle::rotate(double angle)
{
  if (angle < 0)
  {
    angle = (360 + fmod(angle, 360));
  }
  first_ = rotateOneCoordinate(first_, angle);
  second_ = rotateOneCoordinate(second_, angle);
  third_ = rotateOneCoordinate(third_, angle);
}
