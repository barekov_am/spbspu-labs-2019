#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <list>
#include <string>

class Phonebook
{

public:
    struct record_t
    {
        std::string name;
        std::string phone;
    };

    using list = std::list<record_t>;
    using iterator = list::iterator;

    void view(iterator) const;
    void pushBack(const record_t&);

    bool empty() const;

    iterator begin();
    iterator end();
    iterator next(iterator position);
    iterator prev(iterator position);
    iterator insert(iterator position, const record_t& rec);
    iterator remove(iterator position);
    iterator move(iterator position, int n);

private:
    list list_;

};

#endif //PHONEBOOK_HPP
