#include <iostream>
#include <functional>
#include <cstring>
#include <algorithm>
#include <sstream>

#include "commands.hpp"

void task1()
{
    bookmarkInterface manager;
    std::string line;

    while (getline(std::cin, line))
    {
        if (std::cin.fail())
        {
            throw std::ios_base::failure("Data read failure.");
        }

        std::stringstream stringStream(line);
        std::string command;
        stringStream >> command;

        if (strcmp("add", command.c_str()) == 0)
        {
            add(manager, stringStream);
        }
        else if (strcmp("store", command.c_str()) == 0)
        {
            store(manager, stringStream);
        }
        else if (strcmp("insert", command.c_str()) == 0)
        {
            insert(manager, stringStream);
        }
        else if (strcmp("delete", command.c_str()) == 0)
        {
            deleteBookmark(manager, stringStream);
        }
        else if (strcmp("show", command.c_str()) == 0)
        {
            show(manager, stringStream);
        }
        else if (strcmp("move", command.c_str()) == 0)
        {
            move(manager, stringStream);
        }
        else
        {
            std::cout << "<INVALID COMMAND>" << "\n";
        }
    }
}
