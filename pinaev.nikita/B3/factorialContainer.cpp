#include "factorialContainer.hpp"

const int maxNum = 10;

FactorialContainer::FactorialIterator::FactorialIterator():
        value_(1),
        index_(1)
{
}

FactorialContainer::FactorialIterator::FactorialIterator(int index):
        value_(getValue(index)),
        index_(index)
{
}

FactorialContainer::FactorialIterator::reference FactorialContainer::FactorialIterator::operator *()
{
    return value_;
}

FactorialContainer::FactorialIterator::pointer FactorialContainer::FactorialIterator::operator ->()
{
    return &value_;
}

FactorialContainer::FactorialIterator& FactorialContainer::FactorialIterator::operator ++()
{
    index_++;
    value_ = value_ * index_;

    return *this;
}

FactorialContainer::FactorialIterator FactorialContainer::FactorialIterator::operator ++(int)
{
    FactorialIterator tmp = *this;
    ++(*this);

    return tmp;
}

FactorialContainer::FactorialIterator& FactorialContainer::FactorialIterator::operator --()
{
    if (index_ > 1)
    {
        value_ /= index_;
        --index_;
    }

    return *this;
}

FactorialContainer::FactorialIterator FactorialContainer::FactorialIterator::operator --(int)
{
    FactorialIterator tmp = *this;
    --(*this);
    return tmp;
}

bool FactorialContainer::FactorialIterator::operator ==(const FactorialContainer::FactorialIterator& rhs) const
{
    return ((value_ == rhs.value_) && (index_ == rhs.index_));
}

bool FactorialContainer::FactorialIterator::operator !=(const FactorialContainer::FactorialIterator& rhs) const
{
    return !(*this == rhs);
}

unsigned FactorialContainer::FactorialIterator::getValue(int index) const
{
    if (index <= 1)
    {
        return 1;
    }
    else
    {
        return (index * getValue(index - 1));
    }
}

FactorialContainer::FactorialIterator FactorialContainer::begin()
{
    return {1};
}

FactorialContainer::FactorialIterator FactorialContainer::end()
{
    return {maxNum + 1};
}
