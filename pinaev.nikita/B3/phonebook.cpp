#include <iostream>

#include "phonebook.hpp"

void Phonebook::view(iterator iter) const
{
    std::cout << iter->name << " " << iter->phone <<"\n";
}

void Phonebook::pushBack(const Phonebook::record_t& rec)
{
    return list_.push_back(rec);
}

bool Phonebook::empty() const
{
    return list_.empty();
}

Phonebook::iterator Phonebook::begin()
{
    return list_.begin();
}

Phonebook::iterator Phonebook::end()
{
    return list_.end();
}

Phonebook::iterator Phonebook::next(iterator position)
{
    if (std::next(position) != list_.end())
    {
        return ++position;
    }
    else
    {
        return position;
    }
}

Phonebook::iterator Phonebook::prev(iterator position)
{
    if (position != list_.begin())
    {
        return --position;
    }
    else
    {
        return position;
    }
}

Phonebook::iterator Phonebook::insert(iterator position, const Phonebook::record_t& rec)
{
    return list_.insert(position, rec);
}

Phonebook::iterator Phonebook::remove(iterator position)
{
    return list_.erase(position);
}

Phonebook::iterator Phonebook::move(iterator position, int n)
{
    if (n >= 0)
    {
        while (std::next(position) != list_.end() && (n > 0))
        {
            position = next(position);
            --n;
        }
    }
    else
    {
        while (position != list_.begin() && (n < 0))
        {
            position = prev(position);
            ++n;
        }
    }
    return position;
}
