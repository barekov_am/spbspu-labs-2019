#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <string>

#include "bookmarkInterface.hpp"

void add(bookmarkInterface&, std::stringstream&);
void store(bookmarkInterface&, std::stringstream&);
void insert(bookmarkInterface&, std::stringstream&);
void deleteBookmark(bookmarkInterface&, std::stringstream&);
void show(bookmarkInterface&, std::stringstream&);
void move(bookmarkInterface&, std::stringstream&);


#endif //COMMANDS_HPP
