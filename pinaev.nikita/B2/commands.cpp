#include "commands.hpp"
#include <iostream>
#include <sstream>
#include "priority-queue.hpp"

void add(queueStr& queue, std::stringstream& stringStream)
{
    std::string priority;
    std::string data;

    stringStream >> priority;
    stringStream >> std::ws;
    std::getline(stringStream, data, '\0');

    if (data.empty())
    {
        std::cout << "<INVALID COMMAND>\n";
        return;
    }

    if (priority.compare("low") == 0)
    {
        return queue.PutElementToQueue(data, ElementPriority::LOW);
    }
    else if (priority.compare("normal") == 0)
    {
        queue.PutElementToQueue(data, ElementPriority::NORMAL);
    }
    else if (priority.compare("high") == 0)
    {
        queue.PutElementToQueue(data, ElementPriority::HIGH);
    }
    else
    {
        std::cout << "<INVALID COMMAND>\n";
    }
}

void get(queueStr& queue, std::stringstream& stringStream)
{
    stringStream >> std::ws;

    if (!stringStream.eof())
    {
        std::cout << "<INVALID COMMAND>\n";
        return;
    }

    if (queue.isEmpty())
    {
        std::cout << "<EMPTY>\n";
        return;
    }

    std::cout << queue.GetElementFromQueue() << "\n";
}

void accelerate(queueStr& queue, std::stringstream& stringStream)
{
    stringStream >> std::ws;
    if (!stringStream.eof())
    {
        std::cout << "<INVALID COMMAND>\n";
        return;
    }

    if (queue.isEmpty())
    {
        std::cout << "<EMPTY>\n";
        return;
    }

    queue.Accelerate();
}


executeCommand parseCommand(std::string& command)
{
    if (command.compare("add") == 0)
    {
        return add;
    }
    else if (command.compare("get") == 0)
    {
        return get;
    }
    else if (command.compare("accelerate") == 0)
    {
        return accelerate;
    }
    else
    {
        return [](queueStr&, std::stringstream&)
        {
            std::cout << "<INVALID COMMAND>\n";
        };
    }

}
