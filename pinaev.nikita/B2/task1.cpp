#include <sstream>
#include <iostream>
#include "commands.hpp"

void task1()
{
    std::string string;
    queueStr queue;
    while (std::getline(std::cin, string))
    {
        std::stringstream stringStream(string);
        std::string command;
        stringStream >> command;
        parseCommand(command)(queue, stringStream);

    }

}
