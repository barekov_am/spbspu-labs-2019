#ifndef QUEUE_HPP
#define QUEUE_HPP
#include <list>
#include <stdexcept>

typedef enum
{
    LOW,
    NORMAL,
    HIGH
} ElementPriority;

template<typename T>
class QueueWithPriority
{
public:
    T GetElementFromQueue();
    void PutElementToQueue(const T& element, ElementPriority priority);
    void Accelerate();
    bool isEmpty();

private:
    std::list<T> low;
    std::list<T> normal;
    std::list<T> high;

};


#endif // QUEUE_HPP
