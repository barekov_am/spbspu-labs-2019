#ifndef COMMANDS_HPP
#define COMMANDS_HPP
#include <functional>
#include "priority-queue.hpp"

using queueStr = QueueWithPriority<std::string>;
using executeCommand = std::function<void(queueStr&, std::stringstream&)>;
executeCommand parseCommand(std::string&);

#endif // COMMANDS_HPP
