#include <iostream>
#include <list>
#include <exception>

void printList(std::list<int>& listInt)
{
    std::list<int>::iterator begin = listInt.begin();
    std::list<int>::iterator end = --listInt.end();

    while (std::distance(begin, end) > 1)
    {
        std::cout << *begin << " " << *end << " ";
        ++begin;
        --end;
    }

    if (std::distance(begin, end) == 0)
    {
        std::cout << *begin << "\n";
    }
    else
    {
        std::cout << *begin << " " << *end << "\n";
    }
}

void task2()
{
    int num = 0;
    const size_t capacity = 20;
    size_t count = 0;
    const int min = 1;
    const int max = 20;
    std::list<int> list;
    while ((std::cin >> num) && (count < capacity))
    {
        if ((num > max) || (num < min))
        {
            throw std::invalid_argument("Wrong size of list.\n");
        }
        list.push_back(num);
        ++count;
    }

    if ((!std::cin.eof()) && (std::cin.fail()))
    {
        throw std::invalid_argument("Data read failure.\n");
    }

    if (!std::cin.eof())
    {
        throw std::invalid_argument("Out of range.\n");
    }

    if (list.empty())
    {
        return;
    }

    printList(list);
}
