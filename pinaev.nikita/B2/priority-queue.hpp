#ifndef PRIORITYQUEUE_HPP
#define PRIORITYQUEUE_HPP
#include "queue.hpp"

template<typename T>
T QueueWithPriority<T>::GetElementFromQueue()
{
    if (!high.empty())
    {
        T tmp = *high.begin();
        high.pop_front();
        return tmp;
    }

    if (!normal.empty())
    {
        T tmp = *normal.begin();
        normal.pop_front();
        return tmp;
    }

    if (!low.empty())
    {
        T tmp = *low.begin();
        low.pop_front();
        return tmp;
    }

    throw std::runtime_error("Out of time.\n");
}

template<typename T>
void QueueWithPriority<T>::PutElementToQueue(const T& element, ElementPriority priority)
{
    switch(priority)
    {
        case ElementPriority::LOW:
            low.push_back(element);
            break;
        case ElementPriority::NORMAL:
            normal.push_back(element);
            break;
        case ElementPriority::HIGH:
            high.push_back(element);
            break;
    }
}

template<typename T>
void QueueWithPriority<T>::Accelerate()
{
    high.splice(high.end(), low);
}

template<typename T>
bool QueueWithPriority<T>::isEmpty()
{
    return low.empty() && normal.empty() && high.empty();
}
#endif // PRIORITYQUEUE_HPP
