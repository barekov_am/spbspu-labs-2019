#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

const double EPSILON = 0.001;
const double INVALIDARG = -1;
const double SCALECOEF = 2;

BOOST_AUTO_TEST_SUITE(circleTestSuite)

BOOST_AUTO_TEST_CASE(circleTestImmutabilityAfterMoving)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  const double circleAreaBeforeMoving = testCircle.getArea();
  const pinaev::rectangle_t circleFrameRectBeforeMoving = testCircle.getFrameRect();

  testCircle.move(3, 4);

  const pinaev::rectangle_t circleFrameRectAfterMoving = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(circleFrameRectAfterMoving.width, circleFrameRectBeforeMoving.width, EPSILON);
  BOOST_CHECK_CLOSE(circleFrameRectAfterMoving.height, circleFrameRectBeforeMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(testCircle.getArea(), circleAreaBeforeMoving, EPSILON);

  testCircle.move({ -1, 0 });

  const pinaev::rectangle_t circleFrameRectAfterMovingTo = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(circleFrameRectAfterMovingTo.width, circleFrameRectBeforeMoving.width, EPSILON);
  BOOST_CHECK_CLOSE(circleFrameRectAfterMovingTo.height, circleFrameRectBeforeMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(testCircle.getArea(), circleAreaBeforeMoving, EPSILON);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  const double testArea = testCircle.getArea();
  const pinaev::rectangle_t testFrameRect = testCircle.getFrameRect();
  const double angle = 90;

  testCircle.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testCircle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCircle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(testArea, testCircle.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testCircle.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testCircle.getFrameRect().pos.y);
}


BOOST_AUTO_TEST_CASE(circleTestScale)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  const double circleAreaBeforeScaling = testCircle.getArea();

  testCircle.scale(SCALECOEF);

  BOOST_CHECK_CLOSE(testCircle.getArea(), circleAreaBeforeScaling * SCALECOEF * SCALECOEF, EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidCircleRadius)
{
  BOOST_CHECK_THROW(pinaev::Circle({ 1, 1 }, INVALIDARG), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidCircleScaleCoef)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  BOOST_CHECK_THROW(testCircle.scale(INVALIDARG), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
