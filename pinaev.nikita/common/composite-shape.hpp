#ifndef COMPOSITE_SHAPE
#define COMPOSITE_SHAPE
#include "shape.hpp"

namespace pinaev
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(shapePtr &shape);
    CompositeShape(const CompositeShape &otherShape);
    CompositeShape(CompositeShape &&otherShape);

    CompositeShape& operator =(const CompositeShape &otherShape);
    CompositeShape& operator =(CompositeShape &&otherShape);
    Shape &operator [](size_t) const;

    ~CompositeShape() = default;
    
    size_t getSize() const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const double, const double) override;
    void move(const point_t &) override;
    void showResults() const override;
    void scale(double scaleCoef) override;
    void addShape(const shapePtr &shape);
    void deleteShape(size_t numShape);
    void rotate(const double) override;
    dynamicArray getList() const;

  private:
    size_t size_;
    dynamicArray shapeArray_;
    double angle_;
  };
}

#endif //COMPOSITE_SHAPE
