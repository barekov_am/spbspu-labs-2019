#include <boost/test/auto_unit_test.hpp>
#include "triangle.hpp"

const double EPSILON = 0.001;
const double INVALIDARG = -1;
const double SCALECOEF = 2;

BOOST_AUTO_TEST_SUITE(triangleTestSuite)

BOOST_AUTO_TEST_CASE(triangleTestImmutabilityAfterMoving)
{
  pinaev::Triangle testTrian({ 1, 1 }, { -4, -3 }, { 5, -6 });
  const double triangleAreaBeforeMoving = testTrian.getArea();
  const pinaev::rectangle_t triangleFrameRectBeforeMoving = testTrian.getFrameRect();

  testTrian.move(4, 5);

  const pinaev::rectangle_t triangleFrameRectAfterMoving = testTrian.getFrameRect();
  BOOST_CHECK_CLOSE(triangleFrameRectAfterMoving.width, triangleFrameRectBeforeMoving.width, EPSILON);
  BOOST_CHECK_CLOSE(triangleFrameRectAfterMoving.height, triangleFrameRectBeforeMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(testTrian.getArea(), triangleAreaBeforeMoving, EPSILON);

  testTrian.move({ -2, 0 });

  const pinaev::rectangle_t triangleFrameRectAfterMovingTo = testTrian.getFrameRect();
  BOOST_CHECK_CLOSE(triangleFrameRectAfterMovingTo.width, triangleFrameRectBeforeMoving.width, EPSILON);
  BOOST_CHECK_CLOSE(triangleFrameRectAfterMovingTo.height, triangleFrameRectBeforeMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(testTrian.getArea(), triangleAreaBeforeMoving, EPSILON);
}

BOOST_AUTO_TEST_CASE(triangleTestScale)
{
  pinaev::Triangle testTrian({ 1, 1 }, { -4, -3 }, { 5, -6 });
  const double triangleAreaBeforeScaling = testTrian.getArea();

  testTrian.scale(SCALECOEF);

  BOOST_CHECK_CLOSE(testTrian.getArea(), triangleAreaBeforeScaling * SCALECOEF * SCALECOEF, EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidTriangleParametrs)
{
  BOOST_CHECK_THROW(pinaev::Triangle({ INVALIDARG, INVALIDARG }, { INVALIDARG, INVALIDARG }, { 5, -6 }),
    std::invalid_argument);
  BOOST_CHECK_THROW(pinaev::Triangle({ 1, INVALIDARG }, { -4, INVALIDARG }, { 5, INVALIDARG }),
    std::invalid_argument);
  BOOST_CHECK_THROW(pinaev::Triangle({ INVALIDARG, 1 }, { INVALIDARG, -3 }, { INVALIDARG, -6 }),
    std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidTriangleScaleCoef)
{
  pinaev::Triangle testTrian({ 1, 1 }, { -4, -3 }, { 5, -6 });
  BOOST_CHECK_THROW(testTrian.scale(INVALIDARG), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
