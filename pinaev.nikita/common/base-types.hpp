#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace pinaev
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    double width;
    double height;
    point_t pos;
  };

  pinaev::point_t rotatePoint(const pinaev::point_t& centr, const pinaev::point_t& point, double angle);
}

#endif //BASE_TYPES_HPP

