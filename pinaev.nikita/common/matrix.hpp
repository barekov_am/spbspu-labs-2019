#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

namespace pinaev
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix& otherMatrix);
    Matrix(Matrix&& otherMatrix);
    ~Matrix() = default;
    Matrix& operator =(const Matrix& otherMatrix);
    Matrix& operator =(Matrix&& otherMatrix);
    dynamicArray operator [](size_t index) const;
    bool operator ==(const Matrix& otherMatrix) const;
    bool operator !=(const Matrix& otherMatrix) const;
    size_t getLines() const;
    size_t getColumns() const;
    void add(const shapePtr& otherMatrix, size_t line, size_t column);
    void showResults() const;

  private:
    size_t lines_;
    size_t columns_;
    dynamicArray list_;
  };
}

#endif //MATRIX_HPP
