#ifndef SHAPE_HPP
#define SHAPE_HPP
#include "base-types.hpp"
#include <memory>

namespace pinaev
{
  class Shape
  {
  public:
    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const double, const double) = 0;
    virtual void move(const point_t &) = 0;
    virtual void showResults() const = 0;
    virtual void scale(double scaleCoef) = 0;
    virtual void rotate(double) = 0;
  };  
  using shapePtr = std::shared_ptr<Shape>;
  using dynamicArray = std::unique_ptr<shapePtr[]>;
}

#endif //SHAPE_HPP
