#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "split.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithCrossing)
{
  const size_t layersValue = 2;
  const size_t columnsValue = 1;

  pinaev::Circle testCircle({ 1, 1 }, 5);
  pinaev::Rectangle testRectangle({ 1, 1 }, 2, 3);
  pinaev::shapePtr rectanglePtr = std::make_shared<pinaev::Rectangle>(testRectangle);
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(testCircle);

  pinaev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.addShape(circlePtr);

  pinaev::Matrix testMatrix = pinaev::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithoutCrossing)
{
  const size_t layersValue = 1;
  const size_t columnsValue = 2;

  pinaev::Circle testCircle({ 10, 10 }, 5);
  pinaev::Rectangle testRectangle({ 1, 1 }, 2, 3);
  pinaev::shapePtr rectanglePtr = std::make_shared<pinaev::Rectangle>(testRectangle);
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(testCircle);

  pinaev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.addShape(circlePtr);

  pinaev::Matrix testMatrix = pinaev::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  pinaev::Rectangle testRectangle({ 1, 1 }, 2, 3);
  pinaev::shapePtr rectanglePtr = std::make_shared<pinaev::Rectangle>(testRectangle);
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(testCircle);

  pinaev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.addShape(circlePtr);

  pinaev::Matrix testMatrix = pinaev::part(testCompositeShape);

  BOOST_CHECK_THROW(testMatrix[10][0], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-10][0], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkCopyAndMove)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  pinaev::Rectangle testRectangle({ 1, 1 }, 2, 3);
  pinaev::shapePtr rectanglePtr = std::make_shared<pinaev::Rectangle>(testRectangle);
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(testCircle);

  pinaev::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.addShape(circlePtr);

  pinaev::Matrix testMatrix = pinaev::part(testCompositeShape);
  pinaev::Matrix copyMatrix(testMatrix);
  pinaev::Matrix moveMatrix(std::move(testMatrix));

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyMatrix);
}

BOOST_AUTO_TEST_SUITE_END()
