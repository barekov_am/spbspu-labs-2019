#define _USE_MATH_DEFINES
#include "circle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

pinaev::Circle::Circle(const point_t &centr, const double radius):
  radius_(radius),
  centr_(centr)
{
  if (radius <= 0.0)
  {
    throw std::invalid_argument("Invalid radius");
  } 
}
double pinaev::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}
pinaev::rectangle_t pinaev::Circle::getFrameRect() const
{
  return { radius_ * 2, radius_ * 2, centr_ };
}
void pinaev::Circle::move(const double dx, const double dy)
{
  centr_.x += dx;
  centr_.y += dy;
}
void pinaev::Circle::move(const point_t &newPoint)
{
  centr_ = newPoint;
}
void pinaev::Circle::showResults() const
{
  std::cout << std::endl << "Radius is " << radius_ << std::endl;
  std::cout << "Width of frame rectangle is " << getFrameRect().width << std::endl;
  std::cout << "Height  of frame rectangle is " << getFrameRect().height << std::endl;
  std::cout << "Center at: (" << getFrameRect().pos.x << "; " << getFrameRect().pos.y << ")" << std::endl;
  std::cout << "Area of circle is " << getArea() << std::endl;
}
void pinaev::Circle::scale(const double scaleCoef)
{
  if (scaleCoef <= 0.0)
  {
    throw std::invalid_argument("Invalid scaleCoef");
  }
  radius_ *= scaleCoef;
}
void pinaev::Circle::rotate(const double)
{
}
