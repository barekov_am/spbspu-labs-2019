#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double EPSILON = 0.001;
const double INVALIDARG = -1;
const double SCALECOEF = 2;

BOOST_AUTO_TEST_SUITE(rectangleTestSuite)

BOOST_AUTO_TEST_CASE(rectangleTestImmutabilityAfterMoving)
{
  pinaev::Rectangle testRect({ 1, 1 }, 2, 3);
  const double rectangleAreaBeforeMoving = testRect.getArea();
  const pinaev::rectangle_t rectangleFrameRectBeforeMoving = testRect.getFrameRect();

  testRect.move(2, 3);

  const pinaev::rectangle_t rectangleFrameRectAfterMoving = testRect.getFrameRect();
  BOOST_CHECK_CLOSE(rectangleFrameRectAfterMoving.width, rectangleFrameRectBeforeMoving.width, EPSILON);
  BOOST_CHECK_CLOSE(rectangleFrameRectAfterMoving.height, rectangleFrameRectBeforeMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(testRect.getArea(), rectangleAreaBeforeMoving, EPSILON);

  testRect.move({ 0, -1 });

  const pinaev::rectangle_t rectangleFrameRectAfterMovingTo = testRect.getFrameRect();
  BOOST_CHECK_CLOSE(rectangleFrameRectAfterMovingTo.width, rectangleFrameRectBeforeMoving.width, EPSILON);
  BOOST_CHECK_CLOSE(rectangleFrameRectAfterMovingTo.height, rectangleFrameRectBeforeMoving.height, EPSILON);
  BOOST_CHECK_CLOSE(testRect.getArea(), rectangleAreaBeforeMoving, EPSILON);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  pinaev::Rectangle testRectangle({ 1, 1 }, 2, 4);
  const double testArea = testRectangle.getArea();
  const pinaev::rectangle_t testFrameRect = testRectangle.getFrameRect();
  const double angle = 90;

  testRectangle.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testRectangle.getFrameRect().width / 2, EPSILON);
  BOOST_CHECK_CLOSE(testFrameRect.height, testRectangle.getFrameRect().height * 2, EPSILON);
  BOOST_CHECK_CLOSE(testArea, testRectangle.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testRectangle.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testRectangle.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_CASE(rectangleTestScale)
{
  pinaev::Rectangle testRect({ 1, 1 }, 2, 3);
  const double rectangleAreaBeforeScaling = testRect.getArea();

  testRect.scale(SCALECOEF);

  BOOST_CHECK_CLOSE(testRect.getArea(), rectangleAreaBeforeScaling * SCALECOEF * SCALECOEF, EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidRectangleParametrs)
{
  BOOST_CHECK_THROW(pinaev::Rectangle({ 1, 1 }, 2, INVALIDARG), std::invalid_argument);
  BOOST_CHECK_THROW(pinaev::Rectangle({ 1, 1 }, INVALIDARG, 3), std::invalid_argument);
  BOOST_CHECK_THROW(pinaev::Rectangle({ 1, 1 }, INVALIDARG, INVALIDARG), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidRectangleScaleCoef)
{
  pinaev::Rectangle testRect({ 1, 1 }, 2, 3);
  BOOST_CHECK_THROW(testRect.scale(INVALIDARG), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
