#define _USE_MATH_DEFINES
#include "base-types.hpp"
#include <cmath>

pinaev::point_t pinaev::rotatePoint(const pinaev::point_t& centr, const pinaev::point_t& point, double angle)
{
  double angle_ = angle * M_PI / 180;
  double x = centr.x + (point.x - centr.x) * cos(angle_) - (point.y - centr.y) * sin(angle_);
  double y = centr.y + (point.y - centr.y) * cos(angle_) + (point.x - centr.x) * sin(angle_);

  return { x, y };
}
