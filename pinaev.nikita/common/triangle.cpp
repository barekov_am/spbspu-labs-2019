#define _USE_MATH_DEFINES
#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

pinaev::Triangle::Triangle(const point_t &vertexA, const point_t &vertexB, const point_t &vertexC) :
  vertexA_(vertexA),
  vertexB_(vertexB),
  vertexC_(vertexC),
  centr_({ (vertexA.x + vertexB.x + vertexC.x) / 3, (vertexA.y + vertexB.y + vertexC.y) / 3 })
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("Invalid vertices");
  }
}

double pinaev::Triangle::getArea() const
{
  return (std::fabs((vertexA_.x - vertexC_.x) * (vertexB_.y - vertexC_.y)
    - (vertexB_.x - vertexC_.x) * (vertexA_.y - vertexC_.y)) / 2);
}

pinaev::rectangle_t pinaev::Triangle::getFrameRect() const
{
  const double maxX = std::max({ vertexA_.x, vertexB_.x, vertexC_.x });
  const double minX = std::min({ vertexA_.x, vertexB_.x, vertexC_.x });
  const double width = maxX - minX; 

  const double maxY = std::max({ vertexA_.y, vertexB_.y, vertexC_.y });
  const double minY = std::min({ vertexA_.y, vertexB_.y, vertexC_.y });
  const double height = maxY - minY;
  const point_t centrRect = { (minX + width) / 2, (minY + height) / 2 };

  return { width, height, centrRect };
}


void pinaev::Triangle::move(const point_t &newPoint)
{
  const double dx = newPoint.x - centr_.x;
  const double dy = newPoint.y - centr_.y;
  move(dx, dy);
}

void pinaev::Triangle::move(double dx, double dy)
{
  centr_.x += dx;
  vertexA_.x += dx;
  vertexB_.x += dx;
  vertexC_.x += dx;

  centr_.y += dy;
  vertexA_.y += dy;
  vertexB_.y += dy;
  vertexC_.y += dy;
}

static pinaev::point_t scaleVertexN(const pinaev::point_t &vertexN, const pinaev::point_t &centr, double scaleCoef)
{
  pinaev::point_t scaled = { (vertexN.x - centr.x) * scaleCoef, (vertexN.y - centr.y) * scaleCoef };
  return { centr.x + scaled.x, centr.y + scaled.y };
}

void pinaev::Triangle::scale(double scaleCoef)
{
  if (scaleCoef <= 0.0)
  {
    throw std::invalid_argument("Invalid scaleCoef");
  }
  else
  {
    vertexA_ = scaleVertexN(vertexA_, centr_, scaleCoef);
    vertexB_ = scaleVertexN(vertexB_, centr_, scaleCoef);
    vertexC_ = scaleVertexN(vertexC_, centr_, scaleCoef);
  }
}

void pinaev::Triangle::rotate(const double angle)
{
  vertexA_ = pinaev::rotatePoint(centr_, vertexA_, angle);
  vertexB_ = pinaev::rotatePoint(centr_, vertexB_, angle);
  vertexC_ = pinaev::rotatePoint(centr_, vertexC_, angle);
}

void pinaev::Triangle::showResults() const
{
  std::cout << "Center at (" << centr_.x << ", " << centr_.y << ")" << std::endl;
  std::cout << "VertexA at (" << vertexA_.x << ", " << vertexA_.y << ")" << std::endl;
  std::cout << "VertexB at (" << vertexB_.x << ", " << vertexB_.y << ")" << std::endl;
  std::cout << "VertexC at (" << vertexC_.x << ", " << vertexC_.y << ")" << std::endl;
  std::cout << "Center of frame rectangle at (" << getFrameRect().pos.x << ", " << getFrameRect().pos.y << ")" << std::endl;
  std::cout << "Width of frame rectangle is " << getFrameRect().width << std::endl;
  std::cout << "Height  of frame rectangle is " << getFrameRect().height << std::endl;
  std::cout << "Area of triangle is " << getArea() << std::endl;
  std::cout << std::endl;
}
