#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>    
#include <math.h>

pinaev::CompositeShape::CompositeShape() :
  size_(0),
  shapeArray_(nullptr),
  angle_(0.0)
{
}

pinaev::CompositeShape::CompositeShape(shapePtr &shape) :
  size_(1),
  shapeArray_(std::make_unique<shapePtr[]>(size_)),
  angle_(0.0)
{
  shapeArray_[0] = shape;
}

pinaev::CompositeShape::CompositeShape(const CompositeShape &otherShape) :
  size_(otherShape.size_),
  shapeArray_(std::make_unique<shapePtr[]>(otherShape.size_)),
  angle_(0.0)
{
  for (size_t i = 0; i < size_; ++i)
  {
    shapeArray_[i] = otherShape.shapeArray_[i];
  }
}

pinaev::CompositeShape::CompositeShape(CompositeShape &&otherShape) :
  size_(otherShape.size_),
  shapeArray_(std::move(otherShape.shapeArray_)),
  angle_(otherShape.angle_)
{
  otherShape.size_ = 0;
}

pinaev::CompositeShape &pinaev::CompositeShape::operator=(const CompositeShape &otherShape)
{
  if (this != &otherShape)
  {
    size_ = otherShape.size_;
    angle_ = otherShape.angle_;
    shapeArray_ = std::make_unique<shapePtr[]>(size_);

    for (size_t i = 0; i < size_; i++)
    {
      shapeArray_[i] = otherShape.shapeArray_[i];
    }
  }
  return *this;
}

pinaev::CompositeShape &pinaev::CompositeShape::operator=(CompositeShape &&otherShape)
{
  if (this != &otherShape)
  {
    size_ = otherShape.size_;
    angle_ = otherShape.angle_;
    shapeArray_ = std::move(otherShape.shapeArray_);
    otherShape.size_ = 0;
  }
  return *this;
}

pinaev::Shape &pinaev::CompositeShape::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Wrong index");
  }

  return *shapeArray_[index];
}

size_t pinaev::CompositeShape::getSize() const
{
  return size_;
}

double pinaev::CompositeShape::getArea() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Empty composite shape");
  }

  double allArea = 0.0;

  for (size_t i = 0; i < size_; ++i)
  {
    allArea += shapeArray_[i]->getArea();
  }
  return allArea;
}

pinaev::rectangle_t pinaev::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Empty composite shape");
  }

  rectangle_t tempFrame = shapeArray_[0]->getFrameRect();
  double minX = tempFrame.pos.x - tempFrame.width / 2;
  double minY = tempFrame.pos.y - tempFrame.height / 2;
  double maxX = tempFrame.pos.x + tempFrame.width / 2;
  double maxY = tempFrame.pos.y + tempFrame.height / 2;

  for (size_t i = 0; i < size_; i++)
  {
    tempFrame = shapeArray_[i]->getFrameRect();
    minX = std::min(tempFrame.pos.x - tempFrame.width / 2, minX);
    minY = std::min(tempFrame.pos.y - tempFrame.height / 2, minY);
    maxX = std::max(tempFrame.pos.x + tempFrame.width / 2, maxX);
    maxY = std::max(tempFrame.pos.y + tempFrame.height / 2, maxY);
  }
  return rectangle_t{ (maxY - minY), (maxX - minX), {(minX + maxX) / 2, (minY + maxY) / 2} };
}

void pinaev::CompositeShape::move(const point_t &point)
{
  if (size_ == 0)
  {
    throw std::logic_error("Empty composite shape");
  }

  point_t centrCompositeShape = getFrameRect().pos;
  double dx = point.x - centrCompositeShape.x;
  double dy = point.y - centrCompositeShape.y;

  for (size_t i = 0; i < size_; i++)
  {
    shapeArray_[i]->move(dx, dy);
  }
}

void pinaev::CompositeShape::move(const double dx, const double dy)
{
  if (size_ == 0)
  {
    throw std::logic_error("Empty composite shape");
  }

  for (size_t i = 0; i < size_; i++)
  {
    shapeArray_[i]->move(dx, dy);
  }
}

void pinaev::CompositeShape::showResults() const
{
  std::cout << "Amount of shapes: " << size_ << std::endl;
  std::cout << "Width of frame rectangle is " << getFrameRect().width << std::endl;
  std::cout << "Height  of frame rectangle is " << getFrameRect().height << std::endl;
  std::cout << "Center at: (" << getFrameRect().pos.x << "; " << getFrameRect().pos.y << ")" << std::endl;
  std::cout << "Area of composite shape is " << getArea() << std::endl;
}

void pinaev::CompositeShape::addShape(const shapePtr& shape)
{
  dynamicArray tempShape = std::make_unique<shapePtr[]>(size_ + 1);

  for (size_t i = 0; i < size_; i++)
  {
    tempShape[i] = shapeArray_[i];
  }

  tempShape[size_] = shape;
  size_++;
  shapeArray_.swap(tempShape);
}

void pinaev::CompositeShape::deleteShape(size_t numShape)
{
  if (numShape >= size_)
  {
    throw std::out_of_range("Out of range");
  }

  size_--;

  for (size_t i = numShape; i < size_; i++)
  {
    shapeArray_[i] = shapeArray_[i + 1];
  }
}

void pinaev::CompositeShape::scale(const double scaleCoef)
{
  if (size_ == 0)
  {
    throw std::logic_error("Empty composite shape");
  }

  if (scaleCoef <= 0.0)
  {
    throw std::invalid_argument("Invalid scaleCoef");
  }

  point_t centrCompositeShape = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++)
  {
    point_t centrShape = shapeArray_[i]->getFrameRect().pos;
    double x = centrCompositeShape.x + (centrCompositeShape.x - centrShape.x) * scaleCoef;
    double y = centrCompositeShape.y + (centrCompositeShape.y - centrShape.y) * scaleCoef;
    shapeArray_[i]->move({ x, y });
    shapeArray_[i]->scale(scaleCoef);
  }
}

void pinaev::CompositeShape::rotate(const double angle)
{
  const point_t compCentr = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++)
  {
    const point_t shapeCentr = shapeArray_[i]->getFrameRect().pos;
    shapeArray_[i]->move(pinaev::rotatePoint(compCentr, shapeCentr, angle));
    shapeArray_[i]->rotate(angle);
  }
} 

pinaev::dynamicArray pinaev::CompositeShape::getList() const
{
  dynamicArray tempArray(std::make_unique<shapePtr[]>(size_));

  for (size_t i = 0; i < size_; i++)
  {
    tempArray[i] = shapeArray_[i];
  }

  return tempArray;
}
