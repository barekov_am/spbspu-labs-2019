#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace pinaev
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t &, const double, const double);
    rectangle_t getFrameRect() const override;
    double getArea() const override;
    void move(const double, const double) override;
    void move(const point_t &) override;
    void showResults() const override;
    void scale(double scaleCoef) override;
    void rotate(const double angle) override;
  private:
    double angle_;
    double width_;
    double height_;
    point_t centr_;
  };
}

#endif //RECTANGLE_HPP
