#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

namespace pinaev
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &, const point_t &, const point_t &);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const double, const double) override;
    void move(const point_t &) override;
    void showResults() const override;
    void scale(double scaleCoef) override;
    void rotate(const double angle) override;

  private:
    point_t vertexA_;
    point_t vertexB_;
    point_t vertexC_;
    point_t centr_;
  };
}

#endif //TRIANGLE_HPP
