#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "split.hpp"

BOOST_AUTO_TEST_SUITE(splitTest)

BOOST_AUTO_TEST_CASE(checkCorrectCrossing)
{
  pinaev::Rectangle testRectangle({ 2, 0 }, 4, 2);
  pinaev::Rectangle crossingTestRectangle({ 5, 0 }, 6, 2);
  pinaev::Circle testCircle({ 8, 0 }, 1);

  const bool falseResult = pinaev::cross(testRectangle.getFrameRect(), testCircle.getFrameRect());
  const bool trueResult1 = pinaev::cross(testRectangle.getFrameRect(), crossingTestRectangle.getFrameRect());
  const bool trueResult2 = pinaev::cross(testCircle.getFrameRect(), crossingTestRectangle.getFrameRect());

  BOOST_CHECK_EQUAL(falseResult, false);
  BOOST_CHECK_EQUAL(trueResult1, true);
  BOOST_CHECK_EQUAL(trueResult2, true);
}

BOOST_AUTO_TEST_CASE(checkCorrectSeparation)
{
  pinaev::Rectangle testRectangle({ 2, 0 }, 4, 2);
  pinaev::Rectangle crossingTestRectangle({ 5, 0 }, 6, 2);
  pinaev::Circle testCircle({ 8, 0 }, 1);

  pinaev::shapePtr rectanglePtr = std::make_shared<pinaev::Rectangle>(testRectangle);
  pinaev::shapePtr rectangleCrossingPtr = std::make_shared<pinaev::Rectangle>(crossingTestRectangle);
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(testCircle);

  pinaev::CompositeShape testCompositeShape(rectanglePtr);
  testCompositeShape.addShape(rectangleCrossingPtr);
  testCompositeShape.addShape(circlePtr);

  pinaev::Matrix testMatrix = pinaev::part(testCompositeShape);

  BOOST_CHECK(testMatrix[0][0] == rectanglePtr);
  BOOST_CHECK(testMatrix[0][1] == circlePtr);
  BOOST_CHECK(testMatrix[1][0] == rectangleCrossingPtr);
}

BOOST_AUTO_TEST_SUITE_END()
