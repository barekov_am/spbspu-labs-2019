#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

namespace pinaev
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t &, const double);
    rectangle_t getFrameRect() const override;
    double getArea() const override;
    void move(const double, const double) override;
    void move(const point_t &) override;
    void showResults() const override;
    void scale(double scaleCoef) override;
    void rotate(const double angle) override;

  private:
    double radius_;
    point_t centr_;
  };
}

#endif //CIRCLE_HPP
