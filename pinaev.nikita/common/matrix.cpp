#include "matrix.hpp"

#include <stdexcept>
#include <iostream>

pinaev::Matrix::Matrix() :
  lines_(0),
  columns_(0),
  list_(nullptr)
{
}

pinaev::Matrix::Matrix(const Matrix& otherMatrix) :
  lines_(otherMatrix.lines_),
  columns_(otherMatrix.columns_),
  list_(std::make_unique<shapePtr[]>(otherMatrix.lines_* otherMatrix.columns_))
{
  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    list_[i] = otherMatrix.list_[i];
  }
}

pinaev::Matrix::Matrix(Matrix&& otherMatrix) :
  lines_(otherMatrix.lines_),
  columns_(otherMatrix.columns_),
  list_(std::move(otherMatrix.list_))
{
  otherMatrix.lines_ = 0;
  otherMatrix.columns_ = 0;
}

pinaev::Matrix& pinaev::Matrix::operator =(const Matrix& otherMatrix)
{
  if (this != &otherMatrix)
  {
    lines_ = otherMatrix.lines_;
    columns_ = otherMatrix.columns_;
    dynamicArray tempMatrix(std::make_unique<shapePtr[]>(otherMatrix.lines_ * otherMatrix.columns_));

    for (size_t i = 0; i < (lines_ * columns_); i++)
    {
      tempMatrix[i] = otherMatrix.list_[i];
    }
    list_.swap(tempMatrix);
  }
  return *this;
}

pinaev::Matrix& pinaev::Matrix::operator =(Matrix&& otherMatrix)
{
  if (this != &otherMatrix)
  {
    lines_ = otherMatrix.lines_;
    columns_ = otherMatrix.columns_;
    list_ = std::move(otherMatrix.list_);
    otherMatrix.lines_ = 0;
    otherMatrix.columns_ = 0;
  }
  return *this;
}

pinaev::dynamicArray pinaev::Matrix::operator [](size_t index) const
{
  if (lines_ <= index)
  {
    throw std::out_of_range("Index is out of range");
  }

  dynamicArray tempMatrix(std::make_unique<shapePtr[]>(columns_));

  for (size_t i = 0; i < columns_; i++)
  {
    tempMatrix[i] = list_[index * columns_ + i];
  }
  return tempMatrix;
}

bool pinaev::Matrix::operator ==(const Matrix& otherMatrix) const
{
  if ((lines_ != otherMatrix.lines_) || (columns_ != otherMatrix.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    if (list_[i] != otherMatrix.list_[i])
    {
      return false;
    }
  }
  return true;
}

bool pinaev::Matrix::operator !=(const Matrix& otherMatrix) const
{
  return !(*this == otherMatrix);
}

size_t pinaev::Matrix::getLines() const
{
  return lines_;
}

size_t pinaev::Matrix::getColumns() const
{
  return columns_;
}

void pinaev::Matrix::add(const shapePtr& otherMatrix, size_t line, size_t column)
{
  size_t tempLines;
  size_t tempColumns;

  if (line == lines_)
  {
    tempLines = lines_ + 1;
  }
  else
  {
    tempLines = lines_;
  }

  if (column == columns_)
  {
    tempColumns = columns_ + 1;
  }
  else
  {
    tempColumns = columns_;
  }

  dynamicArray tempList(std::make_unique<shapePtr[]>(tempLines * tempColumns));

  for (size_t i = 0; i < tempLines; i++)
  {
    for (size_t j = 0; j < tempColumns; j++)
    {
      if ((i == lines_) || (j == columns_))
      {
        tempList[i * tempColumns + j] = nullptr;
      }
      else
      {
        tempList[i * tempColumns + j] = list_[i * columns_ + j];
      }
    }
  }

  tempList[line * tempColumns + column] = otherMatrix;
  list_.swap(tempList);
  lines_ = tempLines;
  columns_ = tempColumns;
}

void pinaev::Matrix::showResults() const
{
  std::cout << "Value of lines: " << lines_ << "\n";
  std::cout << "Value of columns: " << columns_ << "\n\n";

  for (size_t i = 0; i < lines_; i++)
  {
    for (size_t j = 0; j < columns_; j++)
    {
      if (list_[i * columns_ + j] != nullptr)
      {
        std::cout << "On layer number " << i + 1 << " and position " << j + 1 << " there is figure:\n";
        list_[i * columns_ + j]->showResults();
      }
    }
  }
}
