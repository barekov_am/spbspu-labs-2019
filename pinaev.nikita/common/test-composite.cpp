#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

const double EPSILON = 0.001;
const double INVALIDARG = -1;
const double SCALECOEF = 2;

BOOST_AUTO_TEST_SUITE(compositeShapeTest)

BOOST_AUTO_TEST_CASE(compositeShapeTestImmutabilityAfterMoving)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  pinaev::Rectangle testRectangle({ 1, 1 }, 2, 3);
  pinaev::shapePtr rectanglePtr = std::make_shared<pinaev::Rectangle>(testRectangle);
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(testCircle);

  pinaev::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(rectanglePtr);

  const double compositeArea = testCompositeShape.getArea();
  const pinaev::rectangle_t frameRect = testCompositeShape.getFrameRect();

  testCompositeShape.move(4, 5);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), compositeArea, EPSILON);
  BOOST_CHECK_CLOSE(testCompositeShape.getFrameRect().width, frameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(testCompositeShape.getFrameRect().height, frameRect.height, EPSILON);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterMovingToPoint)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  pinaev::Rectangle testRectangle({ 1, 1 }, 2, 3);
  pinaev::shapePtr rectanglePtr = std::make_shared<pinaev::Rectangle>(testRectangle);
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(testCircle);

  pinaev::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(rectanglePtr);

  const double compositeArea = testCompositeShape.getArea();
  const pinaev::rectangle_t frameRect = testCompositeShape.getFrameRect();

  testCompositeShape.move({ -2, 0 });

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), compositeArea, EPSILON);
  BOOST_CHECK_CLOSE(testCompositeShape.getFrameRect().width, frameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(testCompositeShape.getFrameRect().height, frameRect.height, EPSILON);
}

BOOST_AUTO_TEST_CASE(compositeShapeTestScale)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  pinaev::Rectangle testRectangle({ 1, 1 }, 2, 3);
  pinaev::shapePtr rectanglePtr = std::make_shared<pinaev::Rectangle>(testRectangle);
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(testCircle);

  pinaev::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(rectanglePtr);

  const double testArea = testCompositeShape.getArea();

  testCompositeShape.scale(SCALECOEF);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testArea * SCALECOEF * SCALECOEF, EPSILON);
}

BOOST_AUTO_TEST_CASE(parametersAfterAddingAndDeletion)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  pinaev::Rectangle testRectangle({ 1, 1 }, 2, 3);
  pinaev::shapePtr rectanglePtr = std::make_shared<pinaev::Rectangle>(testRectangle);
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(testCircle);

  pinaev::CompositeShape testCompositeShape(circlePtr);

  const double testCompositeArea = testCompositeShape.getArea();

  testCompositeShape.addShape(rectanglePtr);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea + testRectangle.getArea(), EPSILON);

  testCompositeShape.deleteShape(1);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea, EPSILON);

}

BOOST_AUTO_TEST_CASE(invalidCompositeShapeParametrs)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  pinaev::Rectangle testRectangle({ 1, 1 }, 2, 3);
  pinaev::shapePtr rectanglePtr = std::make_shared<pinaev::Rectangle>(testRectangle);
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(testCircle);

  pinaev::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(rectanglePtr);

  BOOST_CHECK_THROW(testCompositeShape.scale(INVALIDARG), std::invalid_argument);

  BOOST_CHECK_THROW(testCompositeShape.deleteShape(4), std::out_of_range);
  BOOST_CHECK_THROW(testCompositeShape.deleteShape(-2), std::out_of_range);

  BOOST_CHECK_THROW(testCompositeShape[4], std::out_of_range);
  BOOST_CHECK_THROW(testCompositeShape[-2], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(testCopyConstructor)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  pinaev::Rectangle testRectangle({ 1, 1 }, 2, 3);
  pinaev::shapePtr rectanglePtr = std::make_shared<pinaev::Rectangle>(testRectangle);
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(testCircle);

  pinaev::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(rectanglePtr);

  const pinaev::rectangle_t frameRect = testCompositeShape.getFrameRect();
  const double compositeArea = testCompositeShape.getArea();
  const int compositeSize = testCompositeShape.getSize();

  pinaev::CompositeShape copyCompositeShape(testCompositeShape);

  const pinaev::rectangle_t copyFrameRect = copyCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(compositeArea, copyCompositeShape.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, EPSILON);
  BOOST_CHECK_EQUAL(compositeSize, copyCompositeShape.getSize());
}

BOOST_AUTO_TEST_CASE(testMoveConstructor)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  pinaev::Rectangle testRectangle({ 1, 1 }, 2, 3);
  pinaev::shapePtr rectanglePtr = std::make_shared<pinaev::Rectangle>(testRectangle);
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(testCircle);

  pinaev::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(rectanglePtr);

  const pinaev::rectangle_t frameRect = testCompositeShape.getFrameRect();
  const double compositeArea = testCompositeShape.getArea();
  const int compositeSize = testCompositeShape.getSize();

  pinaev::CompositeShape moveCompositeShape(std::move(testCompositeShape));

  const pinaev::rectangle_t moveFrameRect = moveCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(compositeArea, moveCompositeShape.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, EPSILON);
  BOOST_CHECK_EQUAL(compositeSize, moveCompositeShape.getSize());
}

BOOST_AUTO_TEST_CASE(testCopyOperator)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  pinaev::Triangle testTriangle({ 1, 1 }, { -4, -3 }, { 5, -6 });
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(testCircle);
  pinaev::shapePtr trianglePtr = std::make_shared<pinaev::Triangle>(testTriangle);

  pinaev::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const pinaev::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getSize();

  pinaev::Circle testCircleNew({ 3, 4 }, 5);
  pinaev::shapePtr circlePtrNew = std::make_shared<pinaev::Circle>(testCircleNew);

  pinaev::CompositeShape copyCompositeShape(circlePtrNew);

  copyCompositeShape = testCompositeShape;

  const pinaev::rectangle_t copyFrameRect = copyCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, copyCompositeShape.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(testFrameRect.width, copyFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(testFrameRect.height, copyFrameRect.height, EPSILON);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, copyFrameRect.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, copyFrameRect.pos.y, EPSILON);
  BOOST_CHECK_EQUAL(testCompositeSize, copyCompositeShape.getSize());

}

BOOST_AUTO_TEST_CASE(testMoveOperator)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  pinaev::Rectangle testRectangle({ 1, 1 }, 2, 3);
  pinaev::shapePtr rectanglePtr = std::make_shared<pinaev::Rectangle>(testRectangle);
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(testCircle);

  pinaev::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(rectanglePtr);

  const pinaev::rectangle_t frameRect = testCompositeShape.getFrameRect();
  const double compositeArea = testCompositeShape.getArea();
  const int compositeSize = testCompositeShape.getSize();

  pinaev::CompositeShape moveCompositeShape;

  moveCompositeShape = std::move(testCompositeShape);

  const pinaev::rectangle_t moveFrameRect = moveCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(compositeArea, moveCompositeShape.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, EPSILON);
  BOOST_CHECK_EQUAL(compositeSize, moveCompositeShape.getSize());
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  pinaev::Circle testCircle({ 1, 1 }, 5);
  pinaev::Rectangle testRectangle({ 1, 1 }, 2, 4);
  pinaev::shapePtr rectanglePtr = std::make_shared<pinaev::Rectangle>(testRectangle);
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(testCircle);

  pinaev::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(rectanglePtr);

  const double testArea = testCompositeShape.getArea();
  const pinaev::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double angle = 90;

  testCompositeShape.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testCompositeShape.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCompositeShape.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(testArea, testCompositeShape.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testCompositeShape.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testCompositeShape.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
