#define _USE_MATH_DEFINES
#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <math.h>


pinaev::Rectangle::Rectangle(const point_t &centr, const double width, const double height):
  angle_(0.0),
  width_(width),
  height_(height),
  centr_(centr)
{
  if ((height_ <= 0.0) || (width_ <= 0.0))
  {
    throw std::invalid_argument("Invalid width or height");
  }

}
double pinaev::Rectangle::getArea() const
{
  return width_ * height_;
}
pinaev::rectangle_t pinaev::Rectangle::getFrameRect() const
{
  const double cos_ = cos(angle_ * M_PI / 180);
  const double sin_ = sin(angle_ * M_PI / 180);
  const double width = height_ * std::fabs(sin_) + width_ * std::fabs(cos_);
  const double height = height_ * std::fabs(cos_) + width_ * std::fabs(sin_);
  return { width, height, centr_ };
}

void pinaev::Rectangle::move(const double dx, const double dy)
{
  centr_.x += dx;
  centr_.y += dy;
}
void pinaev::Rectangle::move(const point_t &newPoint)
{
  centr_ = newPoint;
}
void pinaev::Rectangle::showResults() const
{
  std::cout << std::endl << "Width of frame rectangle is " << getFrameRect().width << std::endl;
  std::cout << "Height  of frame rectangle is " << getFrameRect().height << std::endl;
  std::cout << "Center at: (" << getFrameRect().pos.x << "; " << getFrameRect().pos.y << ")" << std::endl;
  std::cout << "Area of rectangle is " << getArea() << std::endl;
}
void pinaev::Rectangle::scale(const double scaleCoef)
{
  if (scaleCoef <= 0.0)
  {
    throw std::invalid_argument("Invalid scaleCoef");
  }
  width_ *= scaleCoef;
  height_ *= scaleCoef;
}
void pinaev::Rectangle::rotate(const double angle)
{
  angle_ += angle;
}
