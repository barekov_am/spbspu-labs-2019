#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

int main()
{
    std::cout << "Rectangle" << std::endl;
    pinaev::Rectangle rect({ 1, 1 }, 2, 3);
    pinaev::Shape *shapePointer = &rect;
    shapePointer->showResults();
    std::cout << std::endl << "Moving for 2 by x and 3 by y: " << std::endl;
    shapePointer->move(2, 3);
    shapePointer->showResults();
    shapePointer->scale(2);
    std::cout << std::endl << "Rectangle after scaling x2: " << std::endl;
    shapePointer->showResults();
    std::cout << std::endl << "Moving to the (0; -1): " << std::endl;
    shapePointer->move({ 0, -1 });
    shapePointer->showResults();

    std::cout << std::endl  << "Circle" << std::endl;
    pinaev::Circle circ({ 1, 1 }, 5);
    shapePointer = &circ;
    shapePointer->showResults();
    std::cout << std::endl << "Moving for 3 by x and 4 by y: " << std::endl;
    shapePointer->move(3, 4);
    shapePointer->showResults();
    std::cout << std::endl << "Circle after scaling x2: " << std::endl;
    shapePointer->scale(2);
    shapePointer->showResults();
    std::cout << std::endl << "Moving to the (-1; 0): " << std::endl;
    shapePointer->move({ -1, 0 });
    shapePointer->showResults();

    std::cout << std::endl << "Triangle" << std::endl;
    pinaev::Triangle trian({ 1, 1 }, { -4, -3 }, { 5, -6 });
    shapePointer = &trian;
    shapePointer->showResults();
    std::cout << std::endl << "Moving for 4 by x and 5 by y: " << std::endl;
    shapePointer->move(4, 5);
    shapePointer->showResults();
    std::cout << std::endl << "Triangle after scaling x2: " << std::endl;
    shapePointer->scale(2);
    shapePointer->showResults();
    std::cout << std::endl << "Moving to the (-2; 0): " << std::endl;
    shapePointer->move({ -2, 0 });
    shapePointer->showResults();
      
    return 0;
}
