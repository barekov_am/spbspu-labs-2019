#include <random>
#include <vector>

#include "tools.hpp"

void fillRandom(double *array, int size)
{
    for (int i = 0; i < size; i++)
    {
        array[i] = (rand() % 21) / 10.0 - 1;
    }
}

void task4(const char *direction, int size)
{
    if (size <= 0)
    {
        throw std::invalid_argument("Vector size must be more than 0");
    }
    auto dir = getDirection<double>(direction);

    std::vector<double> vector(size);
    fillRandom(&vector[0], size);

    print(vector);
    sort<brackets>(vector, dir);
    print(vector);
}
