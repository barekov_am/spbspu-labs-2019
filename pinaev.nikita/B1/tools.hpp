#ifndef TOOLS_HPP
#define TOOLS_HPP

#include <iostream>
#include <functional>
#include <cstring>
#include "strategy.hpp"

template <typename T>
std::function<bool(T, T)> getDirection(const char *direction)
{
    if (strcmp(direction, "ascending") == 0)
    {
        return [](T a, T b) { return a < b; };
    }
    if (strcmp(direction, "descending") == 0)
    {
        return [](T a, T b) { return a > b; };
    }
    throw std::invalid_argument("Wrong sorting direction.");
}

template <template <class Container> class Access, typename Container>
void sort(Container &arr, std::function<bool(typename Container::value_type, typename Container::value_type)> direction)
{
    const auto begin = Access<Container>::begin(arr);
    const auto end = Access<Container>::end(arr);

    for (auto i = begin; i != end; i++)
    {
        for (auto j = Access<Container>::next(i); j != end; j++)
        {
            typename Container::reference a = Access<Container>::get(arr, j);
            typename Container::reference b = Access<Container>::get(arr, i);
            if (direction(a, b))
            {
                std::swap(a, b);
            }
        }
    }
}

template <typename Container>
void print(const Container &arr)
{
    if (arr.empty())
    {
        return;
    }

    for (auto elem : arr)
    {
        std::cout << elem << " ";
    }
    std::cout << "\n";
}

#endif // TOOLS_HPP
