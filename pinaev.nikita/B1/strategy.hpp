#ifndef STRATEGY_HPP
#define STRATEGY_HPP

#include <cstddef>
#include <iterator>

template <typename Container>
struct brackets
{
    static size_t begin(const Container &)
    {
        return 0;
    }

    static size_t end(const Container &container)
    {
        return container.size();
    }

    static typename Container::reference get(Container &container, size_t index)
    {
        return container[index];
    }

    static size_t next(size_t index)
    {
        return index + 1;
    }
};

template <typename Container>
struct at
{
    static size_t begin(const Container &)
    {
        return 0;
    }

    static size_t end(const Container &container)
    {
        return container.size();
    }

    static typename Container::reference get(Container &container, size_t index)
    {
        return container.at(index);
    }

    static size_t next(size_t index)
    {
        return index + 1;
    }
};

template <typename Container>
struct iterator
{
    static typename Container::iterator begin(Container &container)
    {
        return container.begin();
    }

    static typename Container::iterator end(Container &container)
    {
        return container.end();
    }

    static typename Container::reference get(Container &, typename Container::iterator iter)
    {
        return *iter;
    }

    static typename Container::iterator next(typename Container::iterator iter)
    {
        return ++iter;
    }
};

#endif // STRATEGY_HPP
