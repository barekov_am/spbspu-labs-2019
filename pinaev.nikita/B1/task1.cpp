#include <iostream>
#include <vector>
#include <forward_list>

#include "tools.hpp"

void task1(const char *direction)
{
    auto dir = getDirection<int>(direction);
    std::vector<int> vector;

    int i = 0;
    while (std::cin && !(std::cin >> i).eof())
    {
        if (std::cin.fail())
        {
            throw std::ios_base::failure("Input error");
        }

        vector.push_back(i);
    }

    if (vector.empty())
    {
        return;
    }

    std::vector<int> vectorAt(vector);
    std::forward_list<int> listIterator(vector.begin(), vector.end());

    sort<brackets>(vector, dir);
    sort<at>(vectorAt, dir);
    sort<iterator>(listIterator, dir);

    print(vector);
    print(vectorAt);
    print(listIterator);
}
