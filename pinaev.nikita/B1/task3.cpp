#include <vector>
#include <iterator>

#include "tools.hpp"

void task3()
{
    std::vector<int> vector;

    int num = -1;
    while (std::cin && !(std::cin >> num).eof())
    {
        if (std::cin.fail())
        {
            throw std::ios_base::failure("Input error");
        }

        if (num == 0)
        {
            break;
        }

        vector.push_back(num);
    }

    if (vector.empty())
    {
        return;
    }

    if (num != 0)
    {
        throw std::runtime_error("Last input number must be 0");
    }

    std::vector<int>::iterator i = vector.begin();
    if (vector.back() == 1)
    {
        while (i != vector.end())
        {
            i = ((*i) % 2 == 0) ? vector.erase(i) : ++i;
        }
    }

    if (vector.back() == 2)
    {
        while (i != vector.end())
        {
            i = ((*i) % 3 == 0) ? (vector.insert(++i, 3, 1) + 3) : ++i;
        }
    }

    print(vector);
}
