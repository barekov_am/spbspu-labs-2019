#include <fstream>
#include <iostream>
#include <memory>
#include <vector>

const size_t initSize = 1;

void task2(const char *file)
{
    std::ifstream inputFile(file);
    if (!inputFile)
    {
        throw std::runtime_error("Failed to open file.");
    }

    size_t size = initSize;

    std::unique_ptr<char[], decltype(&free)> array(static_cast<char *>(malloc(size)), &free);

    if (!array)
    {
        throw std::runtime_error("Failed to allocate memory.");
    }

    size_t index = 0;
    while (inputFile)
    {
        inputFile.read(&array[index], initSize);
        index += inputFile.gcount();
        if (inputFile.gcount() == initSize)
        {
            size += initSize;
            std::unique_ptr<char[], decltype(&free)> newArray(static_cast<char *>(realloc(array.get(), size)), &free);

            if (!newArray)
            {
                throw std::runtime_error("Failed to reallocate memory.");
            }

            array.release();
            std::swap(array, newArray);
        }
        if (!inputFile.eof() && inputFile.fail())
        {
            throw std::runtime_error("Failed to read file");
        }
    }

    std::vector<char> vector(&array[0], &array[index]);
    for(size_t i = 0; i < index; i++)
    {
        std::cout << array[i];
    }
}
