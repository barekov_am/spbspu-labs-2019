#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "split.hpp"

int main()
{
  std::cout << "Rectangle" << std::endl;
  pinaev::Rectangle rect({ 1, 1 }, 2, 3);
  pinaev::Shape *shapePointer = &rect;
  shapePointer->showResults();
  std::cout << std::endl << "Moving for 2 by x and 3 by y: " << std::endl;
  shapePointer->move(2, 3);
  shapePointer->showResults();
  shapePointer->scale(2);
  std::cout << std::endl << "Rectangle after scaling x2: " << std::endl;
  shapePointer->showResults();
  std::cout << std::endl << "Moving to the (0; -1): " << std::endl;
  shapePointer->move({ 0, -1 });
  shapePointer->showResults();

  std::cout << std::endl << "Circle" << std::endl;
  pinaev::Circle circ({ 1, 1 }, 5);
  shapePointer = &circ;
  shapePointer->showResults();
  std::cout << std::endl << "Moving for 3 by x and 4 by y: " << std::endl;
  shapePointer->move(3, 4);
  shapePointer->showResults();
  std::cout << std::endl << "Circle after scaling x2: " << std::endl;
  shapePointer->scale(2);
  shapePointer->showResults();
  std::cout << std::endl << "Moving to the (-1; 0): " << std::endl;
  shapePointer->move({ -1, 0 });
  shapePointer->showResults();

  std::cout << std::endl << "Triangle" << std::endl;
  pinaev::Triangle trian({ 1, 1 }, { -4, -3 }, { 5, -6 });
  shapePointer = &trian;
  shapePointer->showResults();
  std::cout << std::endl << "Moving for 4 by x and 5 by y: " << std::endl;
  shapePointer->move(4, 5);
  shapePointer->showResults();
  std::cout << std::endl << "Triangle after scaling x2: " << std::endl;
  shapePointer->scale(2);
  shapePointer->showResults();
  std::cout << std::endl << "Moving to the (-2; 0): " << std::endl;
  shapePointer->move({ -2, 0 });
  shapePointer->showResults();


  pinaev::shapePtr rectanglePtr = std::make_shared<pinaev::Rectangle>(rect);
  pinaev::shapePtr circlePtr = std::make_shared<pinaev::Circle>(circ);

  pinaev::CompositeShape compositeShape(circlePtr);
  std::cout << std::endl << "Composite shape:" << std::endl;
  compositeShape.showResults();
  std::cout << "After adding rectangle:" << std::endl;
  compositeShape.addShape(rectanglePtr);
  compositeShape.showResults();
  std::cout << "Moving to the point (2, 3):" << std::endl;
  compositeShape.move({ 2, 3 });
  compositeShape.showResults();
  std::cout << "Scaling with coefficient 2" << std::endl;
  compositeShape.scale(2);
  compositeShape.showResults();
  std::cout << std::endl << "Moving for 3 by x and 4 by y: " << std::endl;
  compositeShape.move(3, 4);
  compositeShape.showResults();
  std::cout << "30 degree Rotating" << std::endl;
  compositeShape.rotate(30);
  compositeShape.showResults();
  std::cout << "Deleting the first figure:" << std::endl;
  compositeShape.deleteShape(0);
  compositeShape.showResults();

  pinaev::shapePtr trianglePtr = std::make_shared<pinaev::Triangle>(trian);
  pinaev::Circle newCircle({ 5, 8 }, 10);
  pinaev::shapePtr newCirclePtr = std::make_shared<pinaev::Circle>(newCircle);

  compositeShape.addShape(trianglePtr);
  compositeShape.addShape(newCirclePtr);

  pinaev::Matrix matrix;
  matrix = pinaev::part(compositeShape);
  std::cout << "Matrix" << std::endl;;
  matrix.showResults();

  return 0;
}
