#include <iostream>
#include <cassert>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void printShapeInfo(const demchuk::Shape *ptrShape)
{
  assert(ptrShape != nullptr);

  std::cout << "Area = " << ptrShape->getArea() << std::endl;

  const demchuk::rectangle_t rec = ptrShape->getFrameRect();
  std::cout << "getFrameRect (x,y,H,W) = "
      << "(" << rec.pos.x << ", " << rec.pos.y << ", " << rec.height << ", " << rec.width << ")"
      << std::endl;
}

int main()
{
  demchuk::point_t pt = {0, 0};
  double radius = 1;

  demchuk::Circle circle(pt, radius);

  demchuk::Shape *ptrShape = &circle;
  demchuk::rectangle_t rec = ptrShape->getFrameRect();

  printShapeInfo(ptrShape);

  ptrShape->move(4, 7);
  std::cout << "move 1" << std::endl;
  printShapeInfo(ptrShape);

  pt = {-5, 3};

  ptrShape->move(pt);
  std::cout << "move 2" << std::endl;
  printShapeInfo(ptrShape);

  demchuk::Rectangle rect({5.0, 5.0}, 10.0, 10.0);

  ptrShape = &rect;

  rec = ptrShape->getFrameRect();

  printShapeInfo(ptrShape);

  ptrShape->move(4, 7);
  std::cout << "move 1" << std::endl;
  printShapeInfo(ptrShape);

  pt = {-5, 3};

  ptrShape->move(pt);
  std::cout << "move 2" << std::endl;
  printShapeInfo(ptrShape);

  demchuk::Triangle tring({0, 5}, {4, 3}, {1, 4});

  ptrShape = &tring;

  rec = ptrShape->getFrameRect();

  printShapeInfo(ptrShape);

  ptrShape->move(2, 1);
  std::cout << "move 1" << std::endl;
  printShapeInfo(ptrShape);

  pt = {-2, 1};

  ptrShape->move(pt);
  std::cout << "move 2" << std::endl;
  printShapeInfo(ptrShape);

  demchuk::point_t mas[] = {{2, 2}, {4, 1}, {6, 3}, {4, 4}};

  demchuk::Polygon polig(4, mas);

  ptrShape = &polig;

  rec = ptrShape->getFrameRect();

  printShapeInfo(ptrShape);

  ptrShape->move(3, 2);
  std::cout << "move 1" << std::endl;
  printShapeInfo(ptrShape);

  pt = {6, 2};

  ptrShape->move(pt);
  std::cout << "move 2" << std::endl;
  printShapeInfo(ptrShape);

  return 0;
}
