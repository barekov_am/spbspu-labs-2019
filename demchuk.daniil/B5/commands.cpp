#include <sstream>

#include "shape.hpp"
#include "commands.hpp"

const int QUADRANGLE_VERTICES = 4;

Shape readPoints(std::string& str, int tops)
{
  Shape shape;
  size_t open;
  size_t close;
  size_t colon;

  for (int i = 0; i < tops; i++)
  {
    if (str.empty())
    {
      throw std::invalid_argument("Invalid input");
    }

    open = str.find_first_of('(');
    colon = str.find_first_of(';');
    close = str.find_first_of(')');
    size_t npos = std::string::npos;

    if ((open == npos) || (colon == npos) || (close == npos))
    {
      throw std::invalid_argument("Invalid point");
    }

    int x = std::stoi(str.substr(open + 1, colon - (open + 1)));
    int y = std::stoi(str.substr(colon + 1, close - (colon + 1)));
    str.erase(0, close + 1);
    shape.push_back({ x, y });
  }

  while (str.find_first_of(" \t") == 0)
  {
    str.erase(0, 1);
  }

  if (!str.empty())
  {
    throw std::invalid_argument("Invalid number of parameter");
  }

  return shape;
}

void readShapes(std::vector<Shape>& vector, std::string& str)
{
  while (std::getline(std::cin, str))
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Invalid reading");
    }

    std::stringstream stream(str);
    std::string strTops;
    stream >> strTops;

    if (strTops.empty())
    {
      continue;
    }

    if (stream.eof())
    {
      throw std::invalid_argument("Invalid number of parameter");
    }

    int tops = std::stoi(strTops);

    if (tops < 3)
    {
      throw std::invalid_argument("Incorrect tops");
    }

    std::getline(stream, str);
    vector.push_back(readPoints(str, tops));
  }
}

int getLength(const Point_t& point1, const Point_t& point2)
{
  return ((point1.x - point2.x) * (point1.x - point2.x) + (point1.y - point2.y) * (point1.y - point2.y));
}

bool isRectangle(const Shape& shape)
{
  bool compareDiags = getLength(shape[0], shape[2]) == getLength(shape[1], shape[3]);
  bool compSides1 = getLength(shape[0], shape[1]) == getLength(shape[2], shape[3]);
  bool compSides2 = getLength(shape[1], shape[2]) == getLength(shape[0], shape[3]);

  return compareDiags && compSides1 && compSides2;
}

bool isSquare(const Shape& shape)
{
  return (getLength(shape[0], shape[1]) == getLength(shape[1], shape[2])) && isRectangle(shape);
}

bool isLess(const Shape& l, const Shape& r)
{
  if (l.size() < r.size())
  {
    return true;
  }

  if ((l.size() == QUADRANGLE_VERTICES) && (r.size() == QUADRANGLE_VERTICES))
  {
    if (isSquare(l))
    {
      if (isSquare(r))
      {
        return l[0].x < r[0].x;
      }

      return true;
    }
  }

  return false;
}
