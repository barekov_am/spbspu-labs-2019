#include <iostream>

#include "phonebook.hpp"

void Phonebook::view(iterator iter) const
{
  std::cout << iter->name << " " << iter->phone << std::endl;
}

void Phonebook::pushBack(const Phonebook::record_t& rec)
{
  return list_.push_back(rec);
}

bool Phonebook::empty() const
{
  return list_.empty();
}

Phonebook::iterator Phonebook::begin()
{
  return list_.begin();
}

Phonebook::iterator Phonebook::end()
{
  return list_.end();
}

Phonebook::iterator Phonebook::next(iterator iter)
{
  if (std::next(iter) != list_.end())
  {
    return ++iter;
  }
  else
  {
    return iter;
  }
}

Phonebook::iterator Phonebook::prev(iterator iter)
{
  if (iter != list_.begin())
  {
    return --iter;
  }
  else
  {
    return iter;
  }
}

Phonebook::iterator Phonebook::insert(iterator iter, const Phonebook::record_t& rec)
{
  return list_.insert(iter, rec);
}

Phonebook::iterator Phonebook::remove(iterator iter)
{
  return list_.erase(iter);
}

Phonebook::iterator Phonebook::move(iterator iter, int n)
{
  if (n >= 0)
  {
    while (std::next(iter) != list_.end() && (n > 0))
    {
      iter = next(iter);
      --n;
    }
  }
  else
  {
    while (iter != list_.begin() && (n < 0))
    {
      iter = prev(iter);
      ++n;
    }
  }
  return iter;
}
