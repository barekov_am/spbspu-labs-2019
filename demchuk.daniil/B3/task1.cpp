#include <iostream>
#include <cstring>
#include <functional>
#include <algorithm>
#include <sstream>

#include "commands.hpp"
#include "tasks.hpp"
#include "bookmarksmap.hpp"

void add(BookmarksMap&, std::stringstream&);
void store(BookmarksMap&, std::stringstream&);
void insert(BookmarksMap&, std::stringstream&);
void remove(BookmarksMap&, std::stringstream&);
void show(BookmarksMap&, std::stringstream&);
void move(BookmarksMap&, std::stringstream&);

void task1()
{
  BookmarksMap manager;
  std::string line;

  while (getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input failed");
    }

    std::stringstream str(line);
    std::string command;
    str >> command;

    if (strcmp("add", command.c_str()) == 0)
    {
      add(manager, str);
    }
    else if (strcmp("store", command.c_str()) == 0)
    {
      store(manager, str);
    }
    else if (strcmp("insert", command.c_str()) == 0)
    {
      insert(manager, str);
    }
    else if (strcmp("delete", command.c_str()) == 0)
    {
      remove(manager, str);
    }
    else if (strcmp("show", command.c_str()) == 0)
    {
      show(manager, str);
    }
    else if (strcmp("move", command.c_str()) == 0)
    {
      move(manager, str);
    }
    else
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }
  }
}
