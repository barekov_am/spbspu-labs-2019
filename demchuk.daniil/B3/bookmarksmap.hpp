#ifndef B3_BOOKMARKSMAP_H
#define B3_BOOKMARKSMAP_H

#include <map>

#include "phonebook.hpp"

class BookmarksMap
{

public:
  enum class movePosition
  {
    before,
    after
  };

  enum class insertPosition
  {
    first,
    last
  };

  BookmarksMap();

  void add(const Phonebook::record_t&);
  void store(const std::string&, const std::string&);
  void insert(movePosition, const std::string&, const Phonebook::record_t&);
  void remove(const std::string&);
  void show(const std::string&);
  void move(const std::string&, int);
  void move(const std::string&, insertPosition);

private:
  using bookmarks = std::map<std::string, Phonebook::iterator>;

  Phonebook records_;
  bookmarks bookmarks_;

  bookmarks::iterator getBookmarkIterator(const std::string&);

};

#endif //B3_BOOKMARKSMAP_H
