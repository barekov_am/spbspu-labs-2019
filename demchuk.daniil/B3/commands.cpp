#include <sstream>
#include <iostream>
#include <cctype>
#include <cstring>

#include "commands.hpp"
#include "bookmarksmap.hpp"

BookmarksMap::movePosition getInsertMark(const char* loc)
{
  if (std::strcmp(loc, "after") == 0)
  {
    return BookmarksMap::movePosition::after;
  }

  if (std::strcmp(loc, "before") == 0)
  {
    return BookmarksMap::movePosition::before;
  }

  throw std::invalid_argument("Invalid Mark");
}

BookmarksMap::insertPosition getMoveMark(const char* loc)
{
  if (std::strcmp(loc, "first") == 0)
  {
    return BookmarksMap::insertPosition::first;
  }

  if (std::strcmp(loc, "last") == 0)
  {
    return BookmarksMap::insertPosition::last;
  }

  throw std::invalid_argument("Invalid Mark");
}

std::string getName(std::string &name)
{
  if (name.empty())
  {
    throw std::invalid_argument("Invalid name");
  }

  if (name.front() != '\"')
  {
    throw std::invalid_argument("Invalid name");
  }

  name.erase(name.begin());

  size_t i = 0;
  while ((i < name.size()) && (name[i] != '\"'))
  {
    if (name[i] == '\\')
    {

      if ((name[i + 1] == '\"') && (i + 2 < name.size()))
      {
        name.erase(i, 1);
      }
      else
      {
        throw std::invalid_argument("Invalid name");
      }

    }

    ++i;
  }

  if (i == name.size())
  {
    throw std::invalid_argument("Invalid name");
  }

  name.erase(i);

  if (name.empty())
  {
    throw std::invalid_argument("Invalid name");
  }

  return name;
}

std::string getNumber(std::string& number)
{
  if (number.empty())
  {
    throw std::invalid_argument("Invalid number");
  }

  for (size_t i = 0; i < number.size(); i++)
  {

    if (!std::isdigit(number[i]))
    {
      throw std::invalid_argument("Invalid number");
    }

  }

  return number;
}

std::string getBookmarkName(std::string &bookmarkName)
{
  if (bookmarkName.empty())
  {
    throw std::invalid_argument("Invalid bookmark name");
  }

  for (size_t i = 0; i < bookmarkName.size(); i++)
  {

    if (!isalnum(bookmarkName[i]) && (bookmarkName[i] != '-'))
    {
      throw std::invalid_argument("Invalid bookmark name");
    }

  }

  return bookmarkName;
}


void add(BookmarksMap& manager, std::stringstream& str)
{
  try
  {
    std::string number;
    str >> std::ws >> number;
    number = getNumber(number);

    std::string name;
    std::getline(str >> std::ws, name);
    name = getName(name);

    manager.add({ number, name });
  }

  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID COMMAND>" << std::endl;

    return;
  }
}

void store(BookmarksMap& manager, std::stringstream& str)
{
  try
  {
    std::string bookmarkName;
    str >> std::ws >> bookmarkName;
    bookmarkName = getBookmarkName(bookmarkName);

    std::string newBookmarkName;
    str >> std::ws >> newBookmarkName;
    newBookmarkName = getBookmarkName(newBookmarkName);

    manager.store(bookmarkName, newBookmarkName);
  }

  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID COMMAND>" << std::endl;

    return;
  }
}

void insert(BookmarksMap& manager, std::stringstream& str)
{
  try
  {
    std::string locate;
    str >> std::ws >> locate;
    auto loc = getInsertMark(locate.c_str());

    std::string bookmarkName;
    str >> std::ws >> bookmarkName;
    bookmarkName = getBookmarkName(bookmarkName);

    std::string number;
    str >> std::ws >> number;
    number = getNumber(number);

    std::string name;
    std::getline(str >> std::ws, name);
    name = getName(name);

    manager.insert(loc, bookmarkName, { number, name });
  }

  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID COMMAND>" << std::endl;

    return;
  }
}

void remove(BookmarksMap& manager, std::stringstream& str)
{
  try
  {
    std::string bookmarkName;
    str >> std::ws >> bookmarkName;
    bookmarkName = getBookmarkName(bookmarkName);

    manager.remove(bookmarkName);
  }

  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID COMMAND>" << std::endl;

    return;
  }
}

void show(BookmarksMap& manager, std::stringstream& str)
{
  try
  {
    std::string bookmarkName;
    str >> std::ws >> bookmarkName;
    bookmarkName = getBookmarkName(bookmarkName);

    manager.show(bookmarkName);
  }

  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID COMMAND>" << std::endl;

    return;
  }
}

void move(BookmarksMap& manager, std::stringstream& str)
{
  try
  {
    std::string bookmarkName;
    str >> std::ws >> bookmarkName;
    bookmarkName = getBookmarkName(bookmarkName);

    std::string steps;
    str >> std::ws >> steps;

    if ((steps.front() == '-') || (steps.front() == '+') || (std::isdigit(steps.front())))
    {
      int sign = 1;

      if (steps.front() == '-')
      {
        sign = -1;
        steps.erase(steps.begin());
      }
      else if (steps.front() == '+')
      {
        steps.erase(steps.begin());
      }

      steps = getNumber(steps);
      manager.move(bookmarkName, std::stoi(steps) * sign);
    }
    else
    {
      auto loc = getMoveMark(steps.c_str());
      manager.move(bookmarkName, loc);
    }

  }

  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID STEP>" << std::endl;

    return;
  }
}
