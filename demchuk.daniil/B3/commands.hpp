#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include "bookmarksmap.hpp"

void add(BookmarksMap&, std::stringstream&);
void store(BookmarksMap&, std::stringstream&);
void insert(BookmarksMap&, std::stringstream&);
void remove(BookmarksMap&, std::stringstream&);
void show(BookmarksMap&, std::stringstream&);
void move(BookmarksMap&, std::stringstream&);



#endif // !COMMANDS_HPP
