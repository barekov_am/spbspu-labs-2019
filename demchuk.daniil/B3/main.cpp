#include <iostream>
#include <string>

#include "tasks.hpp"

int main(int argc, char * argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Invalid number of parameters";
      return 1;
    }

    int num = std::stoi(argv[1]);

    switch (num)
    {
    case 1:
    {
      task1();
      break;
    }

    case 2:
    {
      task2();
      break;
    }

    default:
    {
      std::cerr << "Invalid number of task";
      return 1;
    }

    }
  }
  catch (const std::exception &ex)
  {
    std::cerr << ex.what();
    return 1;
  }

  return 0;
}
