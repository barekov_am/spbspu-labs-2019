#include "model.hpp"
#include <iostream>
#include <vector>
#include <iterator>
#include <cstdlib>
#include <ctime>
#include <random>


void fillRandom(double * array, unsigned int size)
{
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> distribution(-1.0, 1.0);

  for (unsigned int i = 0; i < size; i++)
  {
    array[i] = distribution(gen);
  }
}

void part4(char * argv, unsigned int size)
{
  std::vector<double> vector(size, 0);
  fillRandom(&vector[0], size);

  bool(*compare)(double, double) = nullptr;

  if (std::strcmp("ascending", argv) == 0)
  {
    compare = detail::compareFMS;
  }
  else if (std::strcmp("descending", argv) == 0)
  {
    compare = detail::compareFLS;
  }
  else
  {
    throw std::invalid_argument("Sort not defined");
  }

  std::vector<double> vectorCopy = vector;
  detail::sort<detail::Access_index>(vectorCopy, compare);
  detail::printContainer(vector);
  detail::printContainer(vectorCopy);
}
