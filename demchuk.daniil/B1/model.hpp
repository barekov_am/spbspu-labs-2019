#ifndef MODEL_H
#define MODEL_H

#include <iostream>
#include <functional>
#include <cstring>
#include <cstdlib>

namespace detail
{
  template<typename C>
  class Access_iter
  {
  public:
    typedef typename C::iterator value_type;

    static value_type getBegin(C & container);
    static typename C::reference getValue(C & container, value_type n);
    static value_type getEnd(C & container);
  };

  template<typename C>
  class Access_index
  {
  public:
    typedef typename C::size_type value_type;

    static value_type getBegin(C & container);
    static typename C::reference getValue(C & container, value_type n);
    static value_type getEnd(C & container);
  };

  template<typename C>
  class Access_at
  {
  public:
    typedef typename C::size_type value_type;

    static value_type getBegin(C & container);
    static typename C::reference getValue(C & container, value_type n);
    static value_type getEnd(C & container);
  };

  template<typename C>
  typename Access_iter<C>::value_type Access_iter<C>::getBegin(C & container)
  {
    return container.begin();
  }

  template<typename C>
  typename C::reference Access_iter<C>::getValue(C & , value_type n)
  {
    return *n;
  }

  template<typename C>
  typename Access_iter<C>::value_type Access_iter<C>::getEnd(C & container)
  {
    return container.end();
  }

  template<typename C>
  typename Access_index<C>::value_type Access_index<C>::getBegin(C &)
  {
    return 0;
  }

  template<typename C>
  typename C::reference Access_index<C>::getValue(C & container, value_type n)
  {
    return container[n];
  }

  template<typename C>
  typename Access_index<C>::value_type Access_index<C>::getEnd(C & container)
  {
    return container.size();
  }

  template<typename C>
  typename Access_at<C>::value_type Access_at<C>::getBegin(C &)
  {
    return 0;
  }

  template<typename C>
  typename C::reference Access_at<C>::getValue(C & container, value_type n)
  {
    return container.at(n);
  }

  template<typename C>
  typename Access_at<C>::value_type Access_at<C>::getEnd(C & container)
  {
    return container.size();
  }


  template<template <typename C> typename Way_Of_Access, typename C>
  void sort(C & container, bool(*compare)(typename C::value_type, typename C::value_type))
  {
    for (typename Way_Of_Access<C>::value_type i = Way_Of_Access<C>::getBegin(container); i != Way_Of_Access<C>::getEnd(container); i++)
    {

      for (typename Way_Of_Access<C>::value_type j = i; j != Way_Of_Access<C>::getEnd(container); j++)
      {
        if (compare(Way_Of_Access<C>::getValue(container, i), Way_Of_Access<C>::getValue(container, j)))
        {
          std::swap(Way_Of_Access<C>::getValue(container, i), Way_Of_Access<C>::getValue(container, j));
        }
      }
    }
  }

  template<typename T>
  bool compareFMS(T value1, T value2)
  {
    return value1 > value2;
  }

  template<typename T>
  bool compareFLS(T value1, T value2)
  {
    return value1 < value2;
  }

  template<typename C>
  void printContainer(C & container)
  {
    if (container.begin() == container.end())
    {
      return;
    }

    for (typename C::iterator it = container.begin(); it != container.end(); it++)
    {
      std::cout << *it << " ";
    }
    std::cout << std::endl;
  }

}

void part1(char * argv);
void part2(char * argv);
void part3();
void part4(char * argv, unsigned int size);
#endif
