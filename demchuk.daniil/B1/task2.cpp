#include <fstream>
#include <vector>
#include <stdexcept>
#include <memory>
#include <iterator>
#include <cstdlib>
#include "model.hpp"


void part2(char * argv)
{
  std::ifstream source(argv);

  if (!source)
  {
    throw std::invalid_argument("Can't work with file");
  }

  char ch = 0;
  unsigned int arrSize = 5;
  std::unique_ptr<char[]> data(new char[arrSize]);
  unsigned int count = 0;

  source >> std::noskipws;

  while (source >> ch)
  {
    if (count < arrSize)
    {
      data[count] = ch;
    }
    else
    {
      std::unique_ptr<char[]> temp(new char[arrSize * 2]);

      for (unsigned int i = 0; i < arrSize; i++)
      {
        temp[i] = data[i];
      }

      data.swap(temp);
      data[count] = ch;
      arrSize *= 2;
    }
    count++;
  }

  if (source.bad())
  {
    throw std::ios_base::failure("Reading is faill");
  }

  std::vector<char> vector(data.get(), data.get() + count);

  for (unsigned int i = 0; i < vector.size(); i++)
  {
    std::cout << vector[i];
  }
}
