#include <iostream>
#include <iostream>
#include <stdexcept>
#include <stdlib.h>
#include <string>

#include "model.hpp"

int main(int argc, char * argv[])
{
  if (argc < 2)
  {
    std::cerr << "Input parameters are not correct." << std::endl;
    return 1;
  }

  try
  {
    switch (std::stoi(argv[1]))
    {
    case 1:
      if (argc != 3)
      {
        std::cerr << "Input parameters are not correct." << std::endl;
        return 1;
      }
      part1(argv[2]);
      break;
    case 2:
      if (argc != 3)
      {
        throw std::invalid_argument("Input parameters are not correct.");
      }
      part2(argv[2]);
      break;
    case 3:
      if (argc != 2)
      {
        throw std::invalid_argument("Input parameters are not correct.");
      }
      part3();
      break;
    case 4:
      if (argc != 4)
      {
        throw std::invalid_argument("Input parameters are not correct.");
      }
      part4(argv[2], std::stoi(argv[3]));
      break;
    default:
      throw std::invalid_argument("Invalid item number.");
    }
  }
  catch (std::ios_base::failure & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 1;
  }
  catch (std::invalid_argument & exception)
  {
    std::cerr << exception.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cerr << "An exception is thrown" << std::endl;
    return 1;
  }

  return 0;
}
