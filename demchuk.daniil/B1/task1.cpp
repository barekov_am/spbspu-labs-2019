#include <iostream>
#include <vector>
#include <forward_list>
#include <stdexcept>
#include <cstdlib>
#include <iterator>
#include "model.hpp"

void part1(char * argv)
{
  bool(*compare)(int, int) = nullptr;

  if (std::strcmp("ascending", argv) == 0)
  {
    compare = detail::compareFMS;
  }
  else if (std::strcmp("descending", argv) == 0)
  {
    compare = detail::compareFLS;
  }
  else
  {
    throw std::invalid_argument("Sort not defined");
  }

  std::vector<int> vector;
  int i = 0;
  while (std::cin >> i)
  {
    vector.push_back(i);
  }

  if (!std::cin.eof())
  {
    throw std::ios_base::failure("Reading is faill");
  }

  std::vector<int> vectorCopy = vector;
  std::forward_list<int> list(vector.begin(), vector.end());

  detail::sort<detail::Access_index>(vector, compare);
  detail::sort<detail::Access_at>(vectorCopy, compare);
  detail::sort<detail::Access_iter>(list, compare);

  detail::printContainer(vector);
  detail::printContainer(vectorCopy);
  detail::printContainer(list);
}
