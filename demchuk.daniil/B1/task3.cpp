#include <iostream>
#include <vector>
#include <stdexcept>
#include <fstream>
#include <cstdlib>
#include <iterator>
#include "model.hpp"

void part3()
{
  std::vector<int> vector;
  int i = 0;

  while (std::cin >> i && i != 0)
  {
    vector.push_back(i);
  }

  if (std::cin.fail() && (!std::cin.eof() || i != 0))
  {
    throw std::ios_base::failure("Reading is faill");
  }

  if (vector.empty())
  {
    return;
  }

  std::vector<int>::iterator it = vector.begin();

  if (vector.back() == 1)
  {
    while (it != vector.end())
    {
      if (*it % 2 == 0)
      {
        it = vector.erase(it);
      }
      else
      {
        it++;
      }
    }
  }
  else if (vector.back() == 2)
  {
    while (it != vector.end())
    {
      if (*it % 3 == 0)
      {
        it = vector.insert(++it, 3, 1);
        std::advance(it, 2);
      }
      it++;
    }
  }

  detail::printContainer(vector);
}
