#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <vector>
#include "DataStruct.hpp"

std::vector<DataStruct> fillVector();
bool compare(const DataStruct& data1, const DataStruct& data2);
void print(const std::vector<DataStruct>& vector);

#endif // !COMMANDS_HPP
