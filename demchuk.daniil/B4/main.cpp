#include <iostream>
#include <algorithm>
#include "DataStruct.hpp"
#include "commands.hpp"

int main()
{
  try
  {
    std::vector<DataStruct> vector = fillVector();
    std::sort(vector.begin(), vector.end(), compare);
    print(vector);

  }
  catch (std::exception& e)
  {
    std::cerr << e.what();
    return 1;
  }

  return 0;
}
