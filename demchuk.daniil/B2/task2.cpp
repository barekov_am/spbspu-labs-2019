#include <iostream>
#include <list>
#include "tasks.hpp"

void part2()
{
  std::list<int> list;
  const int maxValue = 20;
  const int minValue = 1;
  const int maxSize = 20;
  int number = 0;

  while (std::cin >> number)
  {
    if ((number > maxValue) || (number < minValue))
    {
      throw std::invalid_argument("Wrong number!");
    }

    if (list.size() + 1 > maxSize)
    {
      throw std::out_of_range("No more than 20 element");
    }

    list.push_back(number);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Incorrect input");
  }

  while (!list.empty())
  {
    std::cout << list.front() << " ";
    list.pop_front();
    if (list.empty())
    {
      break;
    }
    std::cout << list.back() << " ";
    list.pop_back();
  }

  std::cout << std::endl;
}
