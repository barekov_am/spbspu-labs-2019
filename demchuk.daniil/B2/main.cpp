#include <iostream>
#include "tasks.hpp"

int main(int argc, char *argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Wrong number!";
      return 1;
    }
    char *end_str = nullptr;
    int num = std::strtol(argv[1], &end_str, 10);
    switch (num)
    {
    case 1:
    {
      part1();
      break;
    }

    case 2:
    {
      part2();
      break;
    }

    default:
    {
      std::cerr << "Wrong task number";
      return 1;
    }
    }
  }
  catch (const std::exception &ex)
  {
    std::cerr << ex.what();
    return 1;
  }

  return 0;
}
