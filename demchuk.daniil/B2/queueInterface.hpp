#ifndef B2_QUEUEINTERFACE_H
#define B2_QUEUEINTERFACE_H

#include <list>

enum ElementPriority
{
  LOW,
  NORMAL,
  HIGH
};

template <typename T>
class QueueWithPriority
{
public:
  void putElementToQueue(const T& element, ElementPriority priority);
  T getElementFromQueue();
  void accelerate();
  bool empty() const;

private:
  std::list<T> low;
  std::list<T> normal;
  std::list<T> high;
};

#endif //B2_QUEUEINTERFACE_H

