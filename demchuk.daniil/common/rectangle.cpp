#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

demchuk::Rectangle::Rectangle(const demchuk::point_t &pos, double width, double height) :
  points_{{pos.x - width / 2, pos.y + height / 2},
    {pos.x + width / 2, pos.y + height / 2},
    {pos.x + width / 2, pos.y - height / 2},
    {pos.x - width / 2, pos.y - height / 2}},
  width_(width),
  height_(height)
{
  if (width <= 0)
  {
    throw std::invalid_argument("invalid width");
  }

  if (height <= 0)
  {
    throw std::invalid_argument("invalid height");
  }
}

double demchuk::Rectangle::getWidth() const 
{
  return width_;
}

double demchuk::Rectangle::getHeight() const 
{
  return height_;
}

double demchuk::Rectangle::getArea() const 
{
  return width_ * height_;
}

demchuk::rectangle_t demchuk::Rectangle::getFrameRect() const 
{ 
  demchuk::point_t center = {(points_[0].x + points_[2].x) / 2, (points_[0].y + points_[2].y) / 2};

  demchuk::point_t top_left = center;
  demchuk::point_t bottom_right = center;

  for (size_t i = 0; i < 4; ++i)
  {
    top_left.x = std::min(points_[i].x, top_left.x);
    top_left.y = std::max(points_[i].y, top_left.y);
    bottom_right.x = std::max(points_[i].x, bottom_right.x);
    bottom_right.y = std::min(points_[i].y, bottom_right.y);
  }

  return {center, bottom_right.x - top_left.x, top_left.y - bottom_right.y};
}

void demchuk::Rectangle::move(double dx, double dy) 
{
  for (size_t i = 0; i < 4; ++i)
  {
    points_[i].x += dx;
    points_[i].y += dy;
  }
}

void demchuk::Rectangle::move(const demchuk::point_t &pos) 
{
  demchuk::point_t center = getFrameRect().pos;
  move(pos.x - center.x, pos.y - center.y);
}

void demchuk::Rectangle::scale(double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("invalid scaling factor");
  }

  demchuk::point_t center = getFrameRect().pos;
  
  for (size_t i = 0; i < 4; ++i)
  {
    points_[i] = {center.x + k * (points_[i].x - center.x), center.y + k * (points_[i].y - center.y)};
  }

  width_ *= k;
  height_ *= k;
}

std::string demchuk::Rectangle::getName() const
{
  return "rectangle";
}

void demchuk::Rectangle::rotate(double angle)
{
  angle = angle * M_PI / 180;

  double angle_cos = cos(angle);
  double angle_sin = sin(angle);

  point_t center = getFrameRect().pos;

  for (size_t i = 0; i < 4; ++i)
  {
    points_[i] = {center.x + angle_cos * (points_[i].x - center.x) - angle_sin * (points_[i].y - center.y),
      center.y + angle_cos * (points_[i].y - center.y) + angle_sin * (points_[i].x - center.x)};
  }
}

bool demchuk::operator==(const demchuk::Rectangle &rectangle_1, const demchuk::Rectangle &rectangle_2)
{
  for (size_t i = 0; i < 4; ++i)
  {
    if (rectangle_1.points_[i].x != rectangle_2.points_[i].x ||
      rectangle_1.points_[i].y != rectangle_2.points_[i].y)
    {
      return false;
    }
  }

  if (rectangle_1.width_ == rectangle_2.width_ &&
    rectangle_1.height_ == rectangle_2.height_)
  {
    return true;
  }
  else
  {
    return false;
  }
}
