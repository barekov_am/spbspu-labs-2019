#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"
#include <string>

namespace demchuk
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t &pos, double width, double height);
    double getWidth() const;
    double getHeight() const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &pos) override;
    void scale(double k) override;
    std::string getName() const override;
    void rotate(double angle) override;
    friend bool operator==(const Rectangle &, const Rectangle &);

  private:
    point_t points_[4];
    double width_, height_;
  };

  bool operator==(const Rectangle &, const Rectangle &);
}

#endif
