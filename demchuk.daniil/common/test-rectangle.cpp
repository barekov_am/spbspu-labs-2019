#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double TOLERANCE = 0.0001;

BOOST_AUTO_TEST_SUITE(testRectangle)

BOOST_AUTO_TEST_CASE(move)
{
  demchuk::Rectangle rectangle({0, 0}, 4, 8);
  rectangle.move({10, 20});

  BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.x, 10, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.y, 20, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getWidth(), 4, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getHeight(), 8, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getArea(), 32, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(scale)
{
  demchuk::Rectangle rectangle({0, 0}, 2, 3);

  double k = 2;
  rectangle.scale(k);

  BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.x, 0, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.y, 0, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getWidth(), 4, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getHeight(), 6, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getArea(), 6 * k * k, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(invalid_width)
{
  BOOST_CHECK_THROW(demchuk::Rectangle rectangle({0, 0}, -10, 3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalid_height)
{
  BOOST_CHECK_THROW(demchuk::Rectangle rectangle({0, 0}, 5, 0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalid_scale)
{
  demchuk::Rectangle rectangle({0, 0}, 10, 10);
  BOOST_CHECK_THROW(rectangle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
