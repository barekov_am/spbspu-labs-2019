#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "polygon.hpp"

const double TOLERANCE = 0.001;

BOOST_AUTO_TEST_SUITE(TestPolygon)

BOOST_AUTO_TEST_CASE(TestPolygonAfterMove)
{
  demchuk::point_t shape[] = {{2, 2}, {4, 1}, {6, 3}, {4, 4}};
  int qtyVertex = sizeof(shape) / sizeof(shape[0]);
  demchuk::Polygon testPolygon(qtyVertex, shape);
  const double areaBefore = testPolygon.getArea();
  const demchuk::rectangle_t frameBefore = testPolygon.getFrameRect();
  testPolygon.move(2.00, 5.4);
  const double areaAfterMove = testPolygon.getArea();
  const demchuk::rectangle_t frameAfterMove = testPolygon.getFrameRect();
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfterMove.height, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfterMove.width, TOLERANCE);
  BOOST_CHECK_CLOSE(areaBefore, areaAfterMove, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(TestPolygonAfterMovePos)
{
  demchuk::point_t shape[] = {{2, 2}, {4, 1}, {6, 3}, {4, 4}};
  int qtyVertex = sizeof(shape) / sizeof(shape[0]);
  demchuk::Polygon testPolygon(qtyVertex, shape);
  const double areaBefore = testPolygon.getArea();
  const demchuk::rectangle_t frameBefore = testPolygon.getFrameRect();
  testPolygon.move({2, 1});
  const double areaAfterMove = testPolygon.getArea();
  const demchuk::rectangle_t frameAfterMove = testPolygon.getFrameRect();
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfterMove.height, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfterMove.width, TOLERANCE);
  BOOST_CHECK_CLOSE(areaBefore, areaAfterMove, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(TestPolygonScale)
{
  demchuk::point_t shape[] = {{2, 2}, {4, 1}, {6, 3}, {4, 4}};
  int qtyVertex = sizeof(shape) / sizeof(shape[0]);
  demchuk::Polygon testPolygon(qtyVertex, shape);
  const double areaBeforeScale = testPolygon.getArea();
  const double k = 2.0;
  testPolygon.scale(k);
  const double areaAfterScale = testPolygon.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScale * k * k, areaAfterScale, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(TestPolygonInvalidParametrs)
{
  demchuk::point_t shape[] = {{5, 1}, {9, 2}};
  int qtyVertex = sizeof(shape) / sizeof(shape[0]);
  BOOST_CHECK_THROW(demchuk::Polygon testPolygon(qtyVertex, shape), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestPolygonInvalidCoefficient)
{
  demchuk::point_t shape[] = {{2, 2}, {4, 1}, {6, 3}, {4, 4}};
  int qtyVertex = sizeof(shape) / sizeof(shape[0]);
  demchuk::Polygon testPolygon(qtyVertex, shape);
  BOOST_CHECK_THROW(testPolygon.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
