#include <boost/test/auto_unit_test.hpp>
#include <cmath>
#include "circle.hpp"

const double TOLERANCE = 0.0001;

BOOST_AUTO_TEST_SUITE(testCircle)

BOOST_AUTO_TEST_CASE(move)
{
  demchuk::Circle circle({5, -1}, 3);
  circle.move(30, 10);

  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.x, 35, TOLERANCE);
  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.y, 9, TOLERANCE);
  BOOST_CHECK_CLOSE(circle.getRadius(), 3, TOLERANCE);
  BOOST_CHECK_CLOSE(circle.getArea(), M_PI * 9, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(scale)
{
  demchuk::Circle circle({-10, 2}, 5);

  double k = 2;
  circle.scale(k);

  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.x, -10, TOLERANCE);
  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.y, 2, TOLERANCE);
  BOOST_CHECK_CLOSE(circle.getRadius(), 10, TOLERANCE);
  BOOST_CHECK_CLOSE(circle.getArea(), M_PI * 25 * k * k, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(invalid_radius)
{
  BOOST_CHECK_THROW(demchuk::Circle circle({0, 0}, -1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalid_scale)
{
  demchuk::Circle circle({0, 0}, 1);
  BOOST_CHECK_THROW(circle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
