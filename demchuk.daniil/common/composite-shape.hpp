#ifndef COMPOSITESHAPE_HPP_INCLUDED
#define COMPOSITESHAPE_HPP_INCLUDED
#include "shape.hpp"
#include <memory>
#include <string>

namespace demchuk
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&);
    CompositeShape();
    CompositeShape(const std::shared_ptr<Shape> &);
    ~CompositeShape() = default;
    CompositeShape & operator=(const CompositeShape &);
    CompositeShape & operator=(CompositeShape &&);
    void add(const std::shared_ptr<Shape> &);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const double dx, const double dy) override;
    void move(const point_t &pos) override;
    void scale(const double k) override;
    std::string getName() const override;
    void rotate(double angle) override;
    friend bool operator==(const CompositeShape &, const CompositeShape &);

  private:
    std::unique_ptr <std::shared_ptr<Shape>[]> shapes_;
    size_t size_;
  };

  bool operator==(const CompositeShape &, const CompositeShape &);
}

#endif
