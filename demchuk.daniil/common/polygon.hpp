#ifndef POLYGON_HPP
#define POLYGON_HPP
#include "shape.hpp"
#include <string>

namespace demchuk
{
  class Polygon : public Shape
  {
  public:
    Polygon(const Polygon &other);
    Polygon(Polygon &&other);
    Polygon(size_t size, const point_t *points);
    ~Polygon() override;
    Polygon &operator=(const Polygon &other);
    Polygon &operator=(Polygon &&other);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &newPos) override;
    void move(double dx, double dy) override;
    void scale(double k) override;
    int getSize() const;
    point_t getCenter() const;
    bool isConvex() const;
    std::string getName() const override;
    void rotate(double angle) override;
    friend bool operator==(const Polygon &, const Polygon &);

  private:
    size_t size_;
    point_t *points_;
  };

  bool operator==(const Polygon &, const Polygon &);
}

#endif
