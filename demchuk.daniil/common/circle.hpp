#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"
#include <string>

namespace demchuk
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t &pos, double radius);
    double getRadius() const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &pos) override;
    void scale(double k) override;
    std::string getName() const override;
    void rotate(double /*angle*/) override {};
    friend bool operator==(const Circle &, const Circle &);

  private:
    point_t pos_;
    double radius_;
  };

  bool operator==(const Circle &, const Circle &);
}

#endif
