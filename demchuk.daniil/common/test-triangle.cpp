#include <boost/test/auto_unit_test.hpp>
#include "triangle.hpp"
#include <cmath>

const double TOLERANCE = 0.0001;

BOOST_AUTO_TEST_SUITE(testTriangle)

BOOST_AUTO_TEST_CASE(move)
{
  demchuk::Triangle triangle({-3, 0}, {0, 9}, {3, 0});
  triangle.move(5, 6);

  BOOST_CHECK_CLOSE(triangle.getCenter().x, 5, TOLERANCE);
  BOOST_CHECK_CLOSE(triangle.getCenter().y, 9, TOLERANCE);
  BOOST_CHECK_CLOSE(triangle.getArea(), 27, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(scale)
{
  demchuk::point_t  pos_a = {1, 0};
  demchuk::point_t  pos_b = {2, 3};
  demchuk::point_t  pos_c = {3, 0};

  demchuk::Triangle triangle(pos_a, pos_b, pos_c);

  double k = 3;
  triangle.scale(k);

  BOOST_CHECK_CLOSE(triangle.getCenter().x, 2, TOLERANCE);
  BOOST_CHECK_CLOSE(triangle.getCenter().y, 1, TOLERANCE);
  BOOST_CHECK_CLOSE(triangle.getArea(), (std::fabs(((pos_a.x - pos_c.x) * (pos_b.y - pos_c.y)
      - (pos_b.x - pos_c.x) * (pos_a.y - pos_c.y))) / 2) * k * k, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(invalid_constructor)
{ 
  BOOST_CHECK_THROW(demchuk::Triangle triangle({1, 0}, {2, 0}, {3, 0}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalid_scale)
{
  demchuk::Triangle triangle({6, 2}, {0, 3}, {5, 2});
  BOOST_CHECK_THROW(triangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
