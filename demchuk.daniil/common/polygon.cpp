#include "polygon.hpp"
#include <algorithm>
#include <cmath>
#include <stdexcept>

demchuk::Polygon::Polygon(size_t size, const demchuk::point_t *points) :
  size_(size),
  points_(new demchuk::point_t[size_])
{
  if (size_ <= 2)
  {
    delete [] points_;
    throw std::invalid_argument("For polygon, error: at least 3 vertex must be given");
  }

  if (points == nullptr)
  {
    delete[] points_;
    throw std::invalid_argument("Pointer to vertex is null");
  }

  for (size_t i = 0; i < size_; i++)
  {
    points_[i] = points[i];
  }

  if (!isConvex())
  {
    delete[] points_;
    throw std::invalid_argument("Polygon must be convex");
  }

  if (getArea() == 0)
  {
    delete [] points_;
    throw std::invalid_argument("Invalid polygon's area");
  }
}

demchuk::Polygon::Polygon(const demchuk::Polygon &other) :
  size_(other.size_),
  points_(new demchuk::point_t[size_])
{
  for (size_t i = 0; i < size_; i++)
  {
    points_[i] = other.points_[i];
  }
}

demchuk::Polygon::Polygon(demchuk::Polygon &&other) :
  size_(other.size_),
  points_(other.points_)
{
  if (this != &other)
  {
    other.size_ = 0;
    other.points_ = nullptr;
  }
}

demchuk::Polygon &demchuk::Polygon::operator=(const demchuk::Polygon &other)
{
  if (this == &other)
  {
    return *this;
  }

  size_ = other.size_;

  delete [] points_;

  points_ = new demchuk::point_t[size_];
  for (size_t i = 0; i < size_; i++)
  {
    points_[i] = other.points_[i];
  }
  return *this;
}

demchuk::Polygon &demchuk::Polygon::operator=(demchuk::Polygon &&other)
{
  if (this == &other)
  {
    return *this;
  }
  size_ = other.size_;

  other.size_ = 0;

  delete [] points_;

  points_ = other.points_;
  other.points_ = nullptr;
  return *this;
}

demchuk::Polygon::~Polygon()
{
  delete [] points_;
}

double demchuk::Polygon::getArea() const 
{
  double sum = 0.0;

  for (size_t i = 0; i < size_ - 1; i++)
  {
    sum += points_[i].x * points_[i + 1].y;
    sum -= points_[i + 1].x * points_[i].y;
  }

  sum += points_[size_ - 1].x * points_[0].y;
  sum -= points_[0].x * points_[size_ - 1].y;

  return (0.5 * std::fabs(sum));
}

demchuk::rectangle_t demchuk::Polygon::getFrameRect() const 
{
  demchuk::point_t max = {points_[0].x, points_[0].y};
  demchuk::point_t min = {points_[0].x, points_[0].y};

  for (size_t i = 1; i < size_; i++)
  {
    max.x = std::max(max.x, points_[i].x);
    max.y = std::max(max.y, points_[i].y);
    min.x = std::min(min.x, points_[i].x);
    min.y = std::min(min.y, points_[i].y);
  }
  const double height = max.y - min.y;
  const double width = max.x - min.x;
  const demchuk::point_t cen = {min.x + width / 2, min.y + height / 2};

  return {cen, width, height};
}

void demchuk::Polygon::move(const demchuk::point_t &newPos) 
{
  const demchuk::point_t center = getCenter();
  const double dx = newPos.x - center.y;
  const double dy = newPos.y - center.y;

  move(dx, dy);
}

void demchuk::Polygon::move(double dx, double dy) 
{
  for (size_t i = 0; i < size_; ++i)
  {
    points_[i].x += dx;
    points_[i].y += dy;
  }
}

void demchuk::Polygon::scale(double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("invalid scaling factor");
  }

  demchuk::point_t center = getCenter();
  
  for (size_t i = 0; i < size_; ++i)
  {
    points_[i] = {center.x + k * (points_[i].x - center.x), center.y + k * (points_[i].y - center.y)};
  }
}

int demchuk::Polygon::getSize() const 
{
  return size_;
}

demchuk::point_t demchuk::Polygon::getCenter() const 
{
  demchuk::point_t temp_point = {0.0, 0.0};

  for (size_t i = 0; i < size_; i++)
  {
    temp_point.x += points_[i].x;
    temp_point.y += points_[i].y;
  }

  return { temp_point.x / size_, temp_point.y / size_ };
}

static double crossProductLength(double xA, double yA,
  double xB, double yB, double xC, double yC)
{
  const double xBA = xA - xB;
  const double yBA = yA - yB;
  const double xBC = xC - xB;
  const double yBC = yC - yB;

  return (xBA * yBC - yBA * xBC);
}

bool demchuk::Polygon::isConvex() const 
{
  bool gotNegative = false;
  bool gotPositive = false;

  for (size_t i = 0; i < size_; ++i)
  {
    const int b = (i + 1) % size_;
    const int c = (b + 1) % size_;

    double cross_product = crossProductLength(
      points_[i].x, points_[i].y,
      points_[b].x, points_[b].y,
      points_[c].x, points_[c].y);

    if (cross_product < 0)
    {
      gotNegative = true;
    }
    else if (cross_product > 0)
    {
      gotPositive = true;
    }

    if (gotNegative && gotPositive)
    {
      return false;
    }
  }

  return true;
}

std::string demchuk::Polygon::getName() const
{
  return "triangle";
}

void demchuk::Polygon::rotate(double angle)
{
  angle = angle * M_PI / 180;

  double angle_cos = cos(angle);
  double angle_sin = sin(angle);

  point_t center = getCenter();

  for (size_t i = 0; i < 3; ++i)
  {
    points_[i] = {center.x + angle_cos * (points_[i].x - center.x) - angle_sin * (points_[i].y - center.y),
      center.y + angle_cos * (points_[i].y - center.y) + angle_sin * (points_[i].x - center.x)};
  }
}

bool demchuk::operator==(const demchuk::Polygon &polygon_1, const demchuk::Polygon &polygon_2)
{
  if (polygon_1.size_ != polygon_2.size_)
  {
    return false;
  }

  for (size_t i = 0; i < polygon_1.size_; ++i)
  {
    if (polygon_1.points_[i].x != polygon_2.points_[i].x ||
      polygon_1.points_[i].y != polygon_2.points_[i].y)
    {
      return false;
    }
  }

  return true;
}
