#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

demchuk::Triangle::Triangle(const demchuk::point_t &pos_a, const demchuk::point_t &pos_b, const demchuk::point_t &pos_c) :
  points_{pos_a, pos_b, pos_c}
{
  if (getArea() == 0)
  {
    throw std::invalid_argument("degenerate triangle");
  }
}

demchuk::point_t demchuk::Triangle::getCenter() const 
{
  return {(points_[0].x + points_[1].x + points_[2].x) / 3,
      (points_[0].y + points_[1].y + points_[2].y) / 3};
}

double demchuk::Triangle::getArea() const 
{
  return fabs(((points_[0].x - points_[2].x) * (points_[1].y - points_[2].y)
      - (points_[1].x - points_[2].x) * (points_[0].y - points_[2].y))) / 2;
}

demchuk::rectangle_t demchuk::Triangle::getFrameRect() const 
{ 
  demchuk::point_t top_left = getCenter();
  demchuk::point_t bottom_right = top_left;

  for (size_t i = 0; i < 3; ++i)
  {
    top_left.x = std::min(points_[i].x, top_left.x);
    top_left.y = std::max(points_[i].y, top_left.y);
    bottom_right.x = std::max(points_[i].x, bottom_right.x);
    bottom_right.y = std::min(points_[i].y, bottom_right.y);
  }

  return {{(top_left.x + bottom_right.x) / 2, (top_left.y + bottom_right.y) / 2},
      bottom_right.x - top_left.x, top_left.y - bottom_right.y};
}

void demchuk::Triangle::move(double dx, double dy) 
{
  for (size_t i = 0; i < 3; ++i)
  {
    points_[i].x += dx;
    points_[i].y += dy;
  }
}

void demchuk::Triangle::move(const demchuk::point_t &pos) 
{
  demchuk::point_t center = getCenter();
  move(pos.x - center.x, pos.y - center.y);
}

void demchuk::Triangle::scale(double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("invalid scaling factor");
  }

  demchuk::point_t center = getCenter();
  
  for (size_t i = 0; i < 3; ++i)
  {
    points_[i] = {center.x + k * (points_[i].x - center.x), center.y + k * (points_[i].y - center.y)};
  }
}

std::string demchuk::Triangle::getName() const
{
  return "triangle";
}

void demchuk::Triangle::rotate(double angle)
{
  angle = angle * M_PI / 180;

  double angle_cos = cos(angle);
  double angle_sin = sin(angle);

  point_t center = getCenter();

  for (size_t i = 0; i < 3; ++i)
  {
    points_[i] = {center.x + angle_cos * (points_[i].x - center.x) - angle_sin * (points_[i].y - center.y),
      center.y + angle_cos * (points_[i].y - center.y) + angle_sin * (points_[i].x - center.x)};
  }
}

bool demchuk::operator==(const demchuk::Triangle &triangle_1, const demchuk::Triangle &triangle_2)
{
  for (size_t i = 0; i < 3; ++i)
  {
    if (triangle_1.points_[i].x != triangle_2.points_[i].x ||
      triangle_1.points_[i].y != triangle_2.points_[i].y)
    {
      return false;
    }
  }

  return true;
}
