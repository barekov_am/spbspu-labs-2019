#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix-shape.hpp"

const double TOLERANCE = 0.0001;

BOOST_AUTO_TEST_SUITE(testMatrixShape)

BOOST_AUTO_TEST_CASE(new_rows)
{
  auto rectangle = std::make_shared<demchuk::Rectangle>(demchuk::point_t{-3, -1}, 4, 4);
  auto circle = std::make_shared<demchuk::Circle>(demchuk::point_t{-1, 0}, 2);
  auto triangle = std::make_shared<demchuk::Triangle>(demchuk::point_t{4, 2}, demchuk::point_t{6, -2}, demchuk::point_t{2, -1});
  
  demchuk::MatrixShape matrix_shape(rectangle);
  matrix_shape.add(circle);
  matrix_shape.add(triangle);
  
  BOOST_CHECK(matrix_shape[1][0] == circle);
}

BOOST_AUTO_TEST_CASE(new_columns)
{
  auto rectangle_1 = std::make_shared<demchuk::Rectangle>(demchuk::point_t{-2, 2}, 2, 2);
  auto rectangle_2 = std::make_shared<demchuk::Rectangle>(demchuk::point_t{0, 0}, 2, 2);
  auto rectangle_3 = std::make_shared<demchuk::Rectangle>(demchuk::point_t{2, -2}, 2, 2);
  
  demchuk::MatrixShape matrix_shape(rectangle_1);
  matrix_shape.add(rectangle_2);
  matrix_shape.add(rectangle_3);
  
  BOOST_CHECK(matrix_shape[0][2] == rectangle_3);
}

BOOST_AUTO_TEST_CASE(add_one_shape)
{
  auto triangle = std::make_shared<demchuk::Triangle>(demchuk::point_t{0, 0}, demchuk::point_t{0, 5}, demchuk::point_t{8, 0});
  demchuk::MatrixShape matrix_shape(triangle);
  
  BOOST_CHECK(matrix_shape[0][0] == triangle);
}

BOOST_AUTO_TEST_CASE(add_composite_shape)
{
  auto rectangle_1 = std::make_shared<demchuk::Rectangle>(demchuk::point_t{0, 3}, 4, 2);
  auto rectangle_2 = std::make_shared<demchuk::Rectangle>(demchuk::point_t{-1, -1}, 2, 2);
  auto circle = std::make_shared<demchuk::Circle>(demchuk::point_t{0, 2}, 2);

  auto composite_shape_1 = std::make_shared<demchuk::CompositeShape>(rectangle_1);
  composite_shape_1->add(circle);

  auto composite_shape_2 = std::make_shared<demchuk::CompositeShape>(composite_shape_1);

  demchuk::MatrixShape matrix_shape(composite_shape_1);
  matrix_shape.add(composite_shape_2);

  BOOST_CHECK(matrix_shape[0][0] == composite_shape_1 && matrix_shape[1][0] == composite_shape_2);
}

BOOST_AUTO_TEST_CASE(invalid_constructor)
{ 
  BOOST_CHECK_THROW(demchuk::MatrixShape matrix_shape(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalid_add_shape)
{
  auto triangle = std::make_shared<demchuk::Triangle>(demchuk::point_t{-2, 3}, demchuk::point_t{0, 5}, demchuk::point_t{5, -2});
  demchuk::MatrixShape matrix_shape(triangle);
  
  BOOST_CHECK_THROW(matrix_shape.add(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
