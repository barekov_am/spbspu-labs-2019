#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"
#include <string>

namespace demchuk
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &pos_a, const point_t &pos_b, const point_t &pos_c);
    point_t getCenter() const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &pos) override;
    void scale(double k) override;
    std::string getName() const override;
    void rotate(double angle) override;
    friend bool operator==(const Triangle &, const Triangle &);

  private:
    point_t points_[3];
  };

  bool operator==(const Triangle &, const Triangle &);
}

#endif
