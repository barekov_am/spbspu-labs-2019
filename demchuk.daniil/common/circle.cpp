#include "circle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>

demchuk::Circle::Circle(const demchuk::point_t &pos, double radius) :
  pos_(pos),
  radius_(radius)
{
  if (radius <= 0)
  {
    throw std::invalid_argument("invalid_radius");
  }
}

double demchuk::Circle::getRadius() const 
{
  return radius_;
}

double demchuk::Circle::getArea() const 
{
  return M_PI * radius_ * radius_;
}

demchuk::rectangle_t demchuk::Circle::getFrameRect() const 
{
  return {pos_, 2 * radius_, 2 * radius_};
}

void demchuk::Circle::move(double dx, double dy) 
{
  pos_.x += dx;
  pos_.y += dy;
}

void demchuk::Circle::move(const demchuk::point_t &pos) 
{
  pos_ = pos;
}

void demchuk::Circle::scale(double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("invalid_scale");
  }
  
  radius_ *= k;
}

std::string demchuk::Circle::getName() const
{
  return "circle";
}

bool demchuk::operator==(const demchuk::Circle &circle_1, const demchuk::Circle &circle_2)
{
  if (circle_1.pos_.x == circle_2.pos_.x &&
    circle_1.pos_.y == circle_2.pos_.y &&
    circle_1.radius_ == circle_2.radius_)
  {
    return true;
  }
  else
  {
    return false;
  }
}
