#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

demchuk::CompositeShape::CompositeShape(const demchuk::CompositeShape &composite_shape):
  size_(composite_shape.size_)
{
  std::unique_ptr <std::shared_ptr<Shape>[]> shapes_copy(new std::shared_ptr<Shape>[size_]);

  for (size_t i = 0; i < size_; ++i)
  {
    shapes_copy[i] = composite_shape.shapes_[i];
  }

  shapes_.swap(shapes_copy);
}

demchuk::CompositeShape::CompositeShape(demchuk::CompositeShape &&composite_shape):
  size_(composite_shape.size_)
{
  shapes_.swap(composite_shape.shapes_);
}

demchuk::CompositeShape::CompositeShape() :
  shapes_(new std::shared_ptr<Shape>[0]),
  size_(0)
{
}

demchuk::CompositeShape::CompositeShape(const std::shared_ptr<Shape> &shape) :
  shapes_(new std::shared_ptr<Shape>[1]),
  size_(1)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("invalid pointer");
  }

  shapes_[0] = shape;
}

demchuk::CompositeShape & demchuk::CompositeShape::operator=(const demchuk::CompositeShape &composite_shape)
{
  if (this != &composite_shape)
  {
    size_ = composite_shape.size_;
    std::unique_ptr <std::shared_ptr<Shape>[]> shapes_copy(new std::shared_ptr<Shape>[size_]);

    for (size_t i = 0; i < size_; ++i)
    {
      shapes_copy[i] = composite_shape.shapes_[i];
    }

    shapes_.swap(shapes_copy);
  }

  return *this;
}

demchuk::CompositeShape & demchuk::CompositeShape::operator=(demchuk::CompositeShape &&composite_shape)
{
  if (this != &composite_shape)
  {
    size_ = composite_shape.size_;
    shapes_.swap(composite_shape.shapes_);
  }

  return *this;
}

void demchuk::CompositeShape::add(const std::shared_ptr<Shape> &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("invalid pointer");
  }

  std::unique_ptr <std::shared_ptr<Shape>[]> shapes_temp(new std::shared_ptr<Shape>[size_ + 1]);

  for (size_t i = 0; i < size_; ++i)
  {
    shapes_temp[i] = shapes_[i];
  }

  shapes_temp[size_] = shape;
  ++size_;
  shapes_.swap(shapes_temp);
}

double demchuk::CompositeShape::getArea() const
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("There are no shapes");
  }

  double area = 0;

  for (size_t i = 0; i < size_; ++i)
  {
    area += shapes_[i]->getArea();
  }

  return area;
}

demchuk::rectangle_t demchuk::CompositeShape::getFrameRect() const
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("There are no shapes");
  }

  demchuk::rectangle_t shape_frame_rect = shapes_[0]->getFrameRect();

  demchuk::point_t top_left = {shape_frame_rect.pos.x - shape_frame_rect.width / 2,
      shape_frame_rect.pos.y + shape_frame_rect.height / 2};

  demchuk::point_t bottom_right = {shape_frame_rect.pos.x + shape_frame_rect.width / 2,
      shape_frame_rect.pos.y - shape_frame_rect.height / 2};

  for (size_t i = 0; i < size_; ++i)
  {
    const demchuk::rectangle_t shape_frame_rect = shapes_[i]->getFrameRect();

    top_left.x = std::min(shape_frame_rect.pos.x - shape_frame_rect.width / 2, top_left.x);
    top_left.y = std::max(shape_frame_rect.pos.y + shape_frame_rect.height / 2, top_left.y);
    bottom_right.x = std::max(shape_frame_rect.pos.x + shape_frame_rect.width / 2, bottom_right.x);
    bottom_right.y = std::min(shape_frame_rect.pos.y - shape_frame_rect.height / 2, bottom_right.y);
  }

  return {{(top_left.x + bottom_right.x) / 2, (top_left.y + bottom_right.y) / 2},
      bottom_right.x - top_left.x, top_left.y - bottom_right.y};
}

void demchuk::CompositeShape::move(const double dx, const double dy)
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("There are no shapes");
  }

  for (size_t i = 0; i < size_; ++i)
  {
    shapes_[i]->move(dx, dy);
  }
}

void demchuk::CompositeShape::move(const demchuk::point_t &pos)
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("There are no shapes");
  }

  const demchuk::point_t center = getFrameRect().pos; 
  move(pos.x - center.x, pos.y - center.y);
}

void demchuk::CompositeShape::scale(const double k)
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("There are no shapes");
  }

  if (k <= 0)
  {
    throw std::invalid_argument("invalid scaling factor");
  }

  const demchuk::point_t center = getFrameRect().pos;

  for (size_t i = 0; i < size_; ++i)
  {
    const demchuk::point_t shape_center = shapes_[i]->getFrameRect().pos;

    shapes_[i]->move({center.x + k * (shape_center.x - center.x),
      center.y + k * (shape_center.y - center.y)});

    shapes_[i]->scale(k);
  }
}

std::string demchuk::CompositeShape::getName() const
{
  return "composite shape";
}

void demchuk::CompositeShape::rotate(double angle)
{
  double angle_cos = cos(angle * M_PI / 180);
  double angle_sin = sin(angle * M_PI / 180);

  point_t center = getFrameRect().pos;

  for (size_t i = 0; i < size_; ++i)
  {
    point_t shape_center = shapes_[i]->getFrameRect().pos;

    shapes_[i]->move({center.x + angle_cos * (shape_center.x - center.x) - angle_sin * (shape_center.y - center.y),
      center.y + angle_cos * (shape_center.y - center.y) + angle_sin * (shape_center.x - center.x)});

    shapes_[i]->rotate(angle);
  }
}

bool demchuk::operator==(const demchuk::CompositeShape &composite_shape_1, const demchuk::CompositeShape &composite_shape_2)
{
  if (composite_shape_1.size_ != composite_shape_2.size_)
  {
    return false;
  }

  size_t i = 0;

  while (i < composite_shape_1.size_)
  {
    if (composite_shape_1.shapes_[i] == composite_shape_2.shapes_[i])
    {
      ++i;
    }
    else
    {
      return false;
    }
  }

  return true;
}
