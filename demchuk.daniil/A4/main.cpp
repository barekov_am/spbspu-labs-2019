#include <iostream>
#include <cmath>
#include "rectangle.hpp"
#include "circle.hpp"
#include "matrix-shape.hpp"

void printShapeInfo(const std::shared_ptr<demchuk::Shape> &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("invalid pointer");
  }

  std::cout << "Area = " << shape->getArea() << std::endl;

  const demchuk::rectangle_t rec = shape->getFrameRect();
  std::cout << "getFrameRect (x,y,H,W) = "
      << "(" << rec.pos.x << ", " << rec.pos.y << ", " << rec.height << ", " << rec.width << ")"
      << std::endl;
}

int main()
{
  try
  {
    auto rectangle_1 = std::make_shared<demchuk::Rectangle>(demchuk::point_t{-1, -2.5}, 6, 3);
    std::cout << "rectangle_1 was created\n\n";

    printShapeInfo(rectangle_1);

    auto circle = std::make_shared<demchuk::Circle>(demchuk::point_t{3, 0}, 3);
    std::cout << "\ncircle was created\n\n";

    printShapeInfo(circle);

    auto rectangle_2 = std::make_shared<demchuk::Rectangle>(demchuk::point_t{-2.5, 1}, 3, 2);
    std::cout << "\nrectangle_2 was created\n\n";

    printShapeInfo(rectangle_2);

    demchuk::MatrixShape matrix_shape(rectangle_1);
    std::cout << "\nmatrix was created\n\n";

    matrix_shape.add(circle);
    matrix_shape.add(rectangle_2);

    matrix_shape.print();
  }
  catch (const std::invalid_argument &e)
  {
    std::cerr << e.what() << "\n";
    return 1;
  }

  return 0;
}
