#include <iostream>
#include "statistics.hpp"

Statistics::Statistics() :
  numberOfNum_(0),
  sumOfNum_(0),
  maxNum_(0),
  minNum_(0),
  averageNum_(0),
  numberOfPositive_(0),
  numberOfNegative_(0),
  oddSum_(0),
  evenSum_(0),
  firstNum_(0),
  lastNum_(0)
{
}

void Statistics::operator()(const int number)
{
  sumOfNum_ += number;
  lastNum_ = number;

  if (number > maxNum_)
  {
    maxNum_ = number;
  }

  if (number < minNum_)
  {
    minNum_ = number;
  }

  if (number > 0)
  {
    numberOfPositive_++;
  }
  else if (number < 0)
  {
    numberOfNegative_++;
  }

  if (number % 2 != 0)
  {
    oddSum_ += number;
  }
  else
  {
    evenSum_ += number;
  }

  if (numberOfNum_ == 0)
  {
    numberOfNum_++;
    firstNum_ = number;
    maxNum_ = number;
    minNum_ = number;
  }
  else
  {
    numberOfNum_++;
  }
}

long int Statistics::maxNum()
{
  return maxNum_;
}

long int Statistics::minNum()
{
  return minNum_;
}

double Statistics::meanNum()
{
  averageNum_ = static_cast<double>(sumOfNum_) / static_cast<double>(numberOfNum_);
  return averageNum_;
}

long int Statistics::getPositiveNum()
{
  return numberOfPositive_;
}

long int Statistics::getNegativeNum()
{
  return numberOfNegative_;
}

long int Statistics::getOddSum()
{
  return oddSum_;
}

long int Statistics::getEvenSum()
{
  return evenSum_;
}

bool Statistics::isFirstEqualLast()
{
  return firstNum_ == lastNum_;
}

void Statistics::print()
{
  std::cout << "Max: " << maxNum() << '\n';
  std::cout << "Min: " << minNum() << '\n';
  std::cout << "Mean: " << meanNum() << '\n';
  std::cout << "Positive: " << getPositiveNum() << '\n';
  std::cout << "Negative: " << getNegativeNum() << '\n';
  std::cout << "Odd Sum: " << getOddSum() << '\n';
  std::cout << "Even Sum: " << getEvenSum() << '\n';
  std::cout << "First/Last Equal: ";
  if (isFirstEqualLast())
  {
    std::cout << "yes" << '\n';
  }
  else
  {
    std::cout << "no" << '\n';
  }
}
