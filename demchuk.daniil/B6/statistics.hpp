#ifndef STATISTIC_HPP
#define STATISTIC_HPP

class Statistics
{
public:
  Statistics();
  void operator()(int number);
  long int maxNum();
  long int minNum();
  double meanNum();
  long int getPositiveNum();
  long int getNegativeNum();
  long int getOddSum();
  long int getEvenSum();
  bool isFirstEqualLast();

  void print();

private:
  int numberOfNum_;
  long int sumOfNum_;
  long int maxNum_;
  long int minNum_;
  double averageNum_;
  int numberOfPositive_;
  int numberOfNegative_;
  long int oddSum_;
  long int evenSum_;
  long int firstNum_;
  long int lastNum_;

};

#endif // !STATISTIC_HPP
