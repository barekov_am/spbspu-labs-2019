#include <iostream>
#include <vector>
#include <algorithm>
#include "statistics.hpp"

int main()
{
  try
  {
    std::vector<int> vector;
    int num;
    while (std::cin >> num)
    {
      vector.push_back(num);
    }
    if (std::cin.fail() && !std::cin.eof())
    {
      throw std::invalid_argument("Wrong input");
    }
    if (!vector.empty())
    {
      Statistics functor = std::for_each(vector.begin(), vector.end(), Statistics());

      functor.print();
    }
    else
    {
      std::cout << "No Data" << '\n';
    }
  }
  catch (std::exception& e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}
