#define _USE_MATH_DEFINES

#include "circle.hpp"
#include <cmath>
#include <stdexcept>

Circle::Circle(const point_t &point, double radius) :
  center_(point),
  radius_(radius)
{
  if (radius <= 0)
  {
    throw std::invalid_argument("Invalid radius, radius must be more then 0");
  }
}

double Circle::getArea() const
{
  return radius_ * radius_ * M_PI;
}

rectangle_t Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, center_};
}

void Circle::move(const point_t &point)
{
  center_ = point;
}

void Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}
