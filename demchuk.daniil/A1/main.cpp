#include <iostream>
#include <cassert>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void printShapeInfo(const Shape *ptrShape)
{
  assert(ptrShape != nullptr);

  std::cout << "Area = " << ptrShape->getArea() << std::endl;

  const rectangle_t rec = ptrShape->getFrameRect();
  std::cout << "getFrameRect (x,y,H,W) = "
      << "(" << rec.pos.x << ", " << rec.pos.y << ", " << rec.height << ", " << rec.width << ")"
      << std::endl;
}

int main()
{
  point_t pt = {0, 0};
  double radius = 1;

  Circle circle(pt, radius);

  Shape *ptrShape = &circle;
  
  rectangle_t rec = {0, 0, {0, 0}};
  
  rec = ptrShape->getFrameRect();

  printShapeInfo(ptrShape);

  ptrShape->move(4, 7);
  std::cout << "move 1" << std::endl;
  printShapeInfo(ptrShape);

  pt = {-5, 3};

  ptrShape->move(pt);
  std::cout << "move 2" << std::endl;
  printShapeInfo(ptrShape);

  rectangle_t rect_s{10.0, 10.0, {5.0, 5.0}};
  Rectangle rect(rect_s);

  ptrShape = &rect;

  rec = ptrShape->getFrameRect();

  printShapeInfo(ptrShape);

  ptrShape->move(4, 7);
  std::cout << "move 1" << std::endl;
  printShapeInfo(ptrShape);

  pt = {-5, 3};

  ptrShape->move(pt);
  std::cout << "move 2" << std::endl;
  printShapeInfo(ptrShape);

  Triangle tring({0, 5}, {4, 3}, {1, 4});

  ptrShape = &tring;

  rec = ptrShape->getFrameRect();

  printShapeInfo(ptrShape);

  ptrShape->move(2, 1);
  std::cout << "move 1" << std::endl;
  printShapeInfo(ptrShape);

  pt = {-2, 1};

  ptrShape->move(pt);
  std::cout << "move 2" << std::endl;
  printShapeInfo(ptrShape);

  point_t mas[] = {{2, 2}, {4, 1}, {6, 3}, {4, 4}};

  Polygon polig(4, mas);

  ptrShape = &polig;

  rec = ptrShape->getFrameRect();

  printShapeInfo(ptrShape);

  ptrShape->move(3, 2);
  std::cout << "move 1" << std::endl;
  printShapeInfo(ptrShape);

  pt = {6, 2};

  ptrShape->move(pt);
  std::cout << "move 2" << std::endl;
  printShapeInfo(ptrShape);

  return 0;
}
