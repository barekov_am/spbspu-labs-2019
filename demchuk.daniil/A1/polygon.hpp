#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <cstddef>
#include"shape.hpp"

class Polygon : public Shape
{
public:
  Polygon(const Polygon &other);
  Polygon(Polygon &&other);
  Polygon(int count, const point_t *vertices);
  ~Polygon() override;
  Polygon &operator=(const Polygon &other);
  Polygon &operator=(Polygon &&other);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &newPos) override;
  void move(double dx, double dy) override;
  point_t getCenter() const;
  bool isConvex() const;

private:
  std::size_t count_;
  point_t *vertices_;
};

#endif
