#include "rectangle.hpp"
#include <stdexcept>

Rectangle::Rectangle(rectangle_t rect) :
  rect_(rect)
{
  if (rect.height <= 0)
  {
    throw std::invalid_argument("Invalid parametr, height must be more then 0.0");
  }

  if (rect.width <= 0)
  {
    throw std::invalid_argument("Invalid parametr, width must be more then 0.0");
  }
}

double Rectangle::getArea() const
{
  return rect_.height * rect_.width;
}

rectangle_t Rectangle::getFrameRect() const
{
  return rect_;
}

void Rectangle::move(const point_t &point)
{
  rect_.pos = point;
}

void Rectangle::move(double dx, double dy)
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}
