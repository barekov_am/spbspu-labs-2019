#include "triangle.hpp"
#include <iostream>
#include <cmath>
#include <algorithm>

Triangle::Triangle(const point_t &a, const point_t &b, const point_t &c) :
  points_{a, b, c}
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("It is not a triangle");
  }
}

double Triangle::getArea() const
{
  const double a = sqrt(pow((points_[0].x - points_[1].x), 2.0) + pow((points_[0].y - points_[1].y), 2.0));
  const double b = sqrt(pow((points_[0].x - points_[2].x), 2.0) + pow((points_[0].y - points_[2].y), 2.0));
  const double c = sqrt(pow((points_[1].x - points_[2].x), 2.0) + pow((points_[1].y - points_[2].y), 2.0));
  const double p = (a + b + c) / 2;
  return sqrt(p * (p - a) * (p - b) * (p - c));
}

rectangle_t Triangle::getFrameRect() const
{
  const double minX = std::min(std::min(points_[0].x, points_[1].x), points_[2].x);
  const double minY = std::min(std::min(points_[0].y, points_[1].y), points_[2].y);
  const double maxX = std::max(std::max(points_[0].x, points_[1].x), points_[2].x);
  const double maxY = std::max(std::max(points_[0].y, points_[1].y), points_[2].y);

  const double height = (maxY - minY);
  const double width = (maxX - minX);
  const point_t cen = {(maxX + minX) / 2 , (maxY + minY) / 2};

  return {width, height, cen};
}

void Triangle::move(const point_t &point)
{
  double cen_x = (points_[0].x + points_[1].x + points_[2].x) / 3;
  double cen_y = (points_[0].y + points_[1].y + points_[2].y) / 3;
  move(point.x - cen_x, point.y - cen_y);
}

void Triangle::move(double dx, double dy)
{
  points_[0].x += dx;
  points_[1].x += dx;
  points_[2].x += dx;
  points_[0].y += dy;
  points_[1].y += dy;
  points_[2].y += dy;
}
