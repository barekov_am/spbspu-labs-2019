#include "polygon.hpp"
#include <algorithm>
#include <cmath>
#include <stdexcept>

Polygon::Polygon(int count, const point_t *vertices) :
  count_(count),
  vertices_(new point_t[count_])
{
  if (count_ <= 2)
  {
    delete [] vertices_;
    throw std::invalid_argument("For polygon, error: at least 3 vertex must be given");
  }

  if (vertices == nullptr)
  {
    delete[] vertices_;
    throw std::invalid_argument("Pointer to vertex is null");
  }

  for (std::size_t i = 0; i < count_; i++)
  {
    vertices_[i] = vertices[i];
  }

  if (!isConvex())
  {
    delete[] vertices_;
    throw std::invalid_argument("Polygon must be convex");
  }

  if (getArea() == 0)
  {
    delete [] vertices_;
    throw std::invalid_argument("Invalid polygon's area");
  }
}

Polygon::Polygon(const Polygon &other) :
  count_(other.count_),
  vertices_(new point_t[count_])
{
  for (std::size_t i = 0; i < count_; i++)
  {
    vertices_[i] = other.vertices_[i];
  }
}

Polygon::Polygon(Polygon &&other) :
  count_(other.count_),
  vertices_(other.vertices_)
{
  if (this != &other)
  {
    other.count_ = 0;
    other.vertices_ = nullptr;
  }
}

Polygon &Polygon::operator = (const Polygon &other)
{
  if (this == &other)
  {
    return *this;
  }

  count_ = other.count_;

  delete [] vertices_;

  vertices_ = new point_t[count_];
  for (std::size_t i = 0; i < count_; i++)
  {
    vertices_[i] = other.vertices_[i];
  }
  return *this;
}

Polygon &Polygon::operator = (Polygon &&other)
{
  if (this == &other)
  {
    return *this;
  }
  count_ = other.count_;

  other.count_ = 0;

  delete [] vertices_;

  vertices_ = other.vertices_;
  other.vertices_ = nullptr;
  return *this;
}

Polygon::~Polygon()
{
  delete [] vertices_;
}

double Polygon::getArea() const
{
  double sum = 0.0;

  for (std::size_t i = 0; i < count_ - 1; i++)
  {
    sum += vertices_[i].x * vertices_[i + 1].y;
    sum -= vertices_[i + 1].x * vertices_[i].y;
  }

  sum += vertices_[count_ - 1].x * vertices_[0].y;
  sum -= vertices_[0].x * vertices_[count_ - 1].y;

  return (0.5 * std::fabs(sum));
}

rectangle_t Polygon::getFrameRect() const
{
  point_t max = {vertices_[0].x, vertices_[0].y};
  point_t min = {vertices_[0].x, vertices_[0].y};

  for (std::size_t i = 1; i < count_; i++)
  {
    max.x = std::max(max.x, vertices_[i].x);
    max.y = std::max(max.y, vertices_[i].y);
    min.x = std::min(min.x, vertices_[i].x);
    min.y = std::min(min.y, vertices_[i].y);
  }
  const double height = max.y - min.y;
  const double width = max.x - min.x;
  const point_t cen = {min.x + width / 2, min.y + height / 2};

  return {width, height, cen};
}

void Polygon::move(const point_t &newPos)
{
  const point_t center = getCenter();
  const double dx = newPos.x - center.y;
  const double dy = newPos.y - center.y;

  move(dx, dy);
}

void Polygon::move(double dx, double dy)
{
  for (std::size_t i = 0; i < count_; ++i)
  {
    vertices_[i].x += dx;
    vertices_[i].y += dy;
  }
}

point_t Polygon::getCenter() const
{
  point_t temp_point = { 0.0, 0.0 };
  for (std::size_t i = 0; i < count_; i++)
  {
    temp_point.x += vertices_[i].x;
    temp_point.y += vertices_[i].y;
  }

  return { temp_point.x / count_, temp_point.y / count_ };
}

static double crossProductLength(double xA, double yA,
  double xB, double yB, double xC, double yC)
{
  const double xBA = xA - xB;
  const double yBA = yA - yB;
  const double xBC = xC - xB;
  const double yBC = yC - yB;

  return (xBA * yBC - yBA * xBC);
}

bool Polygon::isConvex() const
{
  bool gotNegative = false;
  bool gotPositive = false;
  for (std::size_t i = 0; i < count_; ++i)
  {
    const int b = (i + 1) % count_;
    const int c = (b + 1) % count_;

    double cross_product =
    crossProductLength(
    vertices_[i].x, vertices_[i].y,
    vertices_[b].x, vertices_[b].y,
    vertices_[c].x, vertices_[c].y);

    if (cross_product < 0)
    {
      gotNegative = true;
    }
    else if (cross_product > 0)
    {
      gotPositive = true;
    }
    if (gotNegative && gotPositive)
    {
      return false;
    }
  }

  return true;
}
