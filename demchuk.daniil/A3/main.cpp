#include <iostream>
#include <stdexcept> 
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

void printShapeInfo(const std::shared_ptr<demchuk::Shape> &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("invalid pointer");
  }

  std::cout << "Area = " << shape->getArea() << std::endl;

  const demchuk::rectangle_t rec = shape->getFrameRect();
  std::cout << "getFrameRect (x,y,H,W) = "
      << "(" << rec.pos.x << ", " << rec.pos.y << ", " << rec.height << ", " << rec.width << ")"
      << std::endl;
}

int main()
{
  try
  {
    auto rectangle = std::make_shared<demchuk::Rectangle>(demchuk::point_t{0, 0}, 20, 10);
    std::cout << "rectangle was created\n\n";

    printShapeInfo(rectangle);

    auto circle = std::make_shared<demchuk::Circle>(demchuk::point_t{10, 10}, 10);
    std::cout << "\ncircle was created\n\n";

    printShapeInfo(circle);

    auto triangle = std::make_shared<demchuk::Triangle>(demchuk::point_t{0, 0}, demchuk::point_t{0, 3}, demchuk::point_t{4, 0});
    std::cout << "\ntriangle was created\n\n";

    printShapeInfo(triangle);

    auto composite_shape = std::make_shared<demchuk::CompositeShape>();

    composite_shape->add(rectangle);
    std::cout << "\ncomposite shape was created\n\n"
      << "rectangle was added to composite shape\n";

    composite_shape->add(circle);
    std::cout << "circle was added to composite shape\n";

    composite_shape->add(triangle);
    std::cout << "triangle was added to composite shape\n\n";

    printShapeInfo(composite_shape);
  }
  catch (const std::invalid_argument &e)
  {
    std::cerr << e.what() << "\n";
    return 1;
  }
  catch (const std::logic_error &e)
  {
    std::cerr << "Logic error: " << e.what() << "\n";
    return 1;
  }

  return 0;
}
