#include <iostream>
#include <string>
#include "Text.hpp"

const size_t DEFAULT_LINE_WIDTH = 40;
const size_t MIN_LINE_WIDTH = 25;

int main(int argc, char* argv[])
{
  try
  {
    if (argc == 1 || argc == 3)
    {
      size_t width = DEFAULT_LINE_WIDTH;

      if (argc == 3)
      {
        if (std::string(argv[1]) == "--line-width")
        {
          width = std::stoi(argv[2]);
        }
      }

      if (width < MIN_LINE_WIDTH)
      {
        throw std::invalid_argument("Incorrect width");
      }

      Text text(std::cin);
      text.readText();
      text.printText(width);
    }
    else
    {
      throw std::invalid_argument("Invalid input");
    }
  }
  catch (std::exception& e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}
