#ifndef TEXT_HPP
#define TEXT_HPP

#include <vector>
#include <iostream>
#include "text-element.hpp"

class Text
{
public:
  Text(std::istream& in);
  void readText();
  void printText(const size_t& lineWidth);
  void checkText();
private:
  std::vector<TextElement> vector_;
  std::istream& in_;

  TextElement readWord();
  TextElement readNumber();
  TextElement readPunct();
};


#endif // TEXT_HPP
