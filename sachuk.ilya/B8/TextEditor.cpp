#include "TextEditor.hpp"
void TextEditor::reformatText(std::vector<TextElement>& vector, const size_t& lineWidth)
{
  std::string str;

  checkText(vector);
  for (std::vector<TextElement>::iterator iter = vector.begin(); iter != vector.end(); ++iter)
  {
    if (str.empty())
    {
      str += iter->value;
      continue;
    }

    if (iter->symbol == TextElement::Symbol::PUNCT && str.size() + iter->value.size() <= lineWidth)
    {
      str += iter->value;
      continue;
    }

    if ((iter + 1) != vector.end() && (iter + 1)->symbol == TextElement::Symbol::DASH &&
     str.size() + iter->value.size() + (iter + 1)->value.size() + 2 > lineWidth)
    {
      correctLines.push_back(str);
      str.clear();
      str += iter->value + " " + (iter + 1)->value;
      iter++;
      continue;
    }
    
    if ((iter + 1) != vector.end() && (iter + 1)->symbol == TextElement::Symbol::PUNCT &&
     str.size() + iter->value.size() + (iter + 1)->value.size() + 1 > lineWidth)
    {
      correctLines.push_back(str);
      str.clear();
      str += iter->value + (iter + 1)->value;
      iter++;
      continue;
    }

    if (str.size() + iter->value.size() + 1 <= lineWidth)
    {
      str += " " + iter->value;
      continue;
    }
    --iter;
    correctLines.push_back(str);
    str.clear();
  }
  correctLines.push_back(str);
}

void TextEditor::checkText(const std::vector<TextElement>& vector)
{
  if (!vector.empty())
  {
    if (vector[0].symbol == TextElement::Symbol::PUNCT || vector[0].symbol == TextElement::Symbol::DASH)
    {
      throw std::invalid_argument("Incorrect input");
    }
    for (size_t i = 0; i < vector.size() - 1; i++)
    {
      if ((vector[i].symbol == TextElement::Symbol::PUNCT && vector[i + 1].symbol == TextElement::Symbol::DASH) ||
        (vector[i].symbol == TextElement::Symbol::DASH && vector[i + 1].symbol == TextElement::Symbol::PUNCT))
      {
        if (vector[i].value == "," && vector[i + 1].value == "---")
        {
          continue;
        }
        else
        {
          throw std::invalid_argument("Invalid input");
        }
      }
      else if (vector[i].symbol == TextElement::Symbol::PUNCT && vector[i].value != "-")
      {
        if (vector[i + 1].symbol == TextElement::Symbol::PUNCT)
        {
          throw std::invalid_argument("Double punctuation");
        }
      }
      else if (vector[i].symbol == TextElement::Symbol::DASH && vector[i + 1].symbol == TextElement::Symbol::DASH)
      {
        throw std::invalid_argument("Double dash");
      }
    }
  }
}

std::vector<std::string> TextEditor::getText()
{
  return correctLines;
}
