#ifndef TEXTEDITOR_HPP
#define TEXTEDITOR_HPP

#include <vector>
#include <iostream>
#include "text-element.hpp"

class TextEditor
{
public:
  void reformatText(std::vector<TextElement>& vector, const size_t& lineWidth);
  void checkText(const std::vector<TextElement>& vector);
  std::vector<std::string> getText();
private:
  std::vector<std::string> correctLines;
};


#endif // TEXTEDITOR_HPP
