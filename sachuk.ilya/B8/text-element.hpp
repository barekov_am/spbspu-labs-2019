#ifndef TEXT_ELEMENT_HPP
#define TEXT_ELEMENT_HPP

#include <string>

const size_t MAX_LENGTH = 20;

struct TextElement
{
  enum  class Symbol
  {
    WORD,
    PUNCT,
    NUM,
    DASH,
  };
  Symbol symbol;
  std::string value;
};

#endif // !TEXT_ELEMENT_HPP
