#include <iostream>
#include <cctype>
#include "Text.hpp"
#include "TextEditor.hpp"
Text::Text(std::istream& in) :
  in_(in)
{
}

void Text::readText()
{
  char ch;
  while (in_.get(ch))
  {

    while (std::isspace(ch))
    {
      ch = in_.get();
    }

    if (((ch == '+' || ch == '-') && std::isdigit(in_.peek())) || std::isdigit(ch))
    {
      in_.unget();
      vector_.push_back(readNumber());
    }
    else if (std::ispunct(ch))
    {
      in_.unget();
      vector_.push_back(readPunct());
    }
    else if (std::isalpha(ch))
    {
      in_.unget();
      vector_.push_back(readWord());
    }
  }
}

void Text::printText(const size_t& lineWidth)
{
  TextEditor textEditor;
  textEditor.reformatText(vector_, lineWidth);
  std::vector<std::string> outputText = textEditor.getText();
  for (size_t i = 0; i < outputText.size(); ++i)
  {
    std::cout << outputText[i] << '\n';
  }
}

TextElement Text::readWord()
{
  std::string str;
  while (in_)
  {
    if (std::isalpha(in_.peek()) || in_.peek() == '-')
    {
      char ch = in_.get();
      if (ch == '-' && in_.peek() == '-')
      {
        in_.unget();
        break;
      }
      str += ch;
    }
    else
    {
      break;
    }
  }
  if (str.length() > MAX_LENGTH)
  {
    throw std::invalid_argument("Invalid length");
  }
  return { TextElement::Symbol::WORD, str };
}

TextElement Text::readNumber()
{
  std::string str;
  int dotCounter = 0;
  while (in_)
  {
    char ch = in_.get();
    if ((ch == '+' || ch == '-') && str.empty())
    {
      str += ch;
    }
    else if (std::isdigit(ch) || ch == '.')
    {
      if (ch == '.')
      {
        dotCounter++;
      }
      str += ch;
    }
    else
    {
      in_.unget();
      break;
    }
  }

  if (dotCounter > 1)
  {
    throw std::invalid_argument("Too many dots");
  }

  if (str.length() > MAX_LENGTH)
  {
    throw std::invalid_argument("Invalid length");
  }
  return { TextElement::Symbol::NUM, str };
}

TextElement Text::readPunct()
{
  char ch = in_.get();
  std::string str;
  str += ch;
  if (ch == '-' && in_.peek() == '-')
  {
    ch = in_.get();
    if (in_.peek() == '-')
    {
      str += ch;
      str += in_.get();
      return { TextElement::Symbol::DASH, str };
    }
    else
    {
      throw std::invalid_argument("Invalid input");
    }
  }
  return { TextElement::Symbol::PUNCT, str };
}
