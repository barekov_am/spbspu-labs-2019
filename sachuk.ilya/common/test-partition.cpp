#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(partitionModTest)

BOOST_AUTO_TEST_CASE(partitionTest)
{
  const sachuk::Rectangle testRect1({1, 2}, 2, 6);
  const sachuk::Rectangle testRect2({2, 2}, 4, 4);
  sachuk::shape_ptr rectPtr1 = std::make_shared<sachuk::Rectangle>(testRect1);
  sachuk::shape_ptr rectPtr2 = std::make_shared<sachuk::Rectangle>(testRect2);

  const sachuk::Circle testCircle1({3, 4}, 5);
  sachuk::shape_ptr circPtr1 = std::make_shared<sachuk::Circle>(testCircle1);

  sachuk::CompositeShape testCompShape(rectPtr1);
  sachuk::Matrix matrix1 = sachuk::part(testCompShape);

  testCompShape.add(circPtr1);
  sachuk::Matrix matrix2 = sachuk::part(testCompShape);

  testCompShape.add(rectPtr2);
  sachuk::Matrix matrix3 = sachuk::part(testCompShape);

  BOOST_CHECK_EQUAL(matrix1.getLines(), 1);
  BOOST_CHECK_EQUAL(matrix1.getColumns(), 1);

  BOOST_CHECK_EQUAL(matrix2.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix2.getColumns(), 1);

  BOOST_CHECK_EQUAL(matrix3.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix3.getColumns(), 2);
}

BOOST_AUTO_TEST_CASE(intersectTest)
{
  const sachuk::Rectangle testRect1({1, 2}, 2, 6);
  const sachuk::Rectangle testRect2({2, 2}, 4, 4);
  const sachuk::Circle testCirc1({1, 2}, 2);

  BOOST_CHECK(checkIntersect(testRect1.getFrameRect(), testCirc1.getFrameRect()));
  BOOST_CHECK(!checkIntersect(testRect1.getFrameRect(), testRect2.getFrameRect()));
  BOOST_CHECK(checkIntersect(testCirc1.getFrameRect(), testRect2.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
