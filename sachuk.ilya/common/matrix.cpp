#include "matrix.hpp"
#include <cmath>
#include <stdexcept>

sachuk::Matrix::Matrix():
  lines_(0),
  columns_(0)
{
}

sachuk::Matrix::Matrix(const Matrix & other) :
  lines_(other.lines_),
  columns_(other.columns_),
  forms_(std::make_unique<shape_ptr[]>(other.lines_ * other.columns_))
{
  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    forms_[i] = other.forms_[i];
  }
}

sachuk::Matrix::Matrix(Matrix && other) :
  lines_(other.lines_),
  columns_(other.columns_),
  forms_(std::move(other.forms_))
{
  other.lines_ = 0;
  other.columns_ = 0;
}

sachuk::Matrix & sachuk::Matrix::operator =(const Matrix & other)
{
  if (this != &other)
  {
    shapes_array tmpForms(std::make_unique <shape_ptr[]>(other.lines_ * other.columns_));
    lines_ = other.lines_;
    columns_ = other.columns_;

    for (size_t i = 0; i < (lines_ * columns_); i++)
    {
      tmpForms[i] = other.forms_[i];
    }

    forms_.swap(tmpForms);
  }
  return *this;
}

sachuk::Matrix & sachuk::Matrix::operator =(Matrix && other)
{
  if (this != &other)
  {
    lines_ = other.lines_;
    columns_ = other.columns_;
    forms_ = std::move(other.forms_);
    other.lines_ = 0;
    other.columns_ = 0;
  }
  return *this;
}

sachuk::shapes_array sachuk::Matrix::operator [](size_t index) const
{
  if (index >= lines_)
  {
    throw std::out_of_range("Invalid index");
  }
  shapes_array tmpForms(std::make_unique<shape_ptr[]>(columns_));

  for (size_t i = 0; i < getLineSize(index); i++)
  {
    tmpForms[i] = forms_[index * columns_ + i];
  }
  return tmpForms;
}

bool sachuk::Matrix::operator ==(const Matrix & matrixShapes) const
{
  if ((lines_ != matrixShapes.lines_) || (columns_ != matrixShapes.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    if (forms_[i] != matrixShapes.forms_[i])
    {
      return false;
    }
  }
  return true;
}

bool sachuk::Matrix::operator !=(const Matrix & matrixShapes) const
{
  return !(*this == matrixShapes);
}

void sachuk::Matrix::add(shape_ptr shape, size_t line, size_t column)
{
  size_t newLine = (line == lines_) ? (lines_ + 1) : (lines_);
  size_t newColumn = (column == columns_) ? (columns_ + 1) : (columns_);

  shapes_array tmpForms(std::make_unique <shape_ptr[]>(newLine * newColumn));

  for (size_t i = 0; i < newLine; i++)
  {
    for (size_t j = 0; j < newColumn; ++j)
    {
      if ((lines_ == i) || (columns_ == j))
      {
        tmpForms[i * newColumn + j] = nullptr;
      }
      else
      {
        tmpForms[i * newColumn + j] = forms_[i * columns_ + j];
      }
    }
  }

  tmpForms[line * newColumn + column] = shape;
  forms_.swap(tmpForms);
  lines_ = newLine;
  columns_ = newColumn;
}

size_t sachuk::Matrix::getLines() const
{
  return lines_;
}

size_t sachuk::Matrix::getColumns() const
{
  return columns_;
}

size_t sachuk::Matrix::getLineSize(size_t line) const
{
  if (line >= lines_)
  {
    return 0;
  }

  for (size_t i = 0; i < columns_; i++)
  {
    if (forms_[line * columns_ + i] == nullptr)
    {
      return i;
    }
  }
  return columns_;
}
