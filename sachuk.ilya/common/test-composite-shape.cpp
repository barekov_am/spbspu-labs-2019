#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>

#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(test_compositeShape)

const double FAULT = 0.01;

BOOST_AUTO_TEST_CASE(testCompositeShapeAfterMoveInDot)
{
  sachuk::Rectangle testRec({2, 4}, 3, 5);
  sachuk::Circle testCircle({4, 6}, 5);
  sachuk::shape_ptr rectPtr = std::make_shared<sachuk::Rectangle>(testRec);
  sachuk::shape_ptr circPtr = std::make_shared<sachuk::Circle>(testCircle);
  sachuk::CompositeShape testCompShape(rectPtr);
  testCompShape.add(circPtr);

  const double areaBeforeMove = testCompShape.getArea();
  const sachuk::rectangle_t frameBeforeMove = testCompShape.getFrameRect();

  testCompShape.move({40, 80});

  const double areaAfterMove = testCompShape.getArea();
  const sachuk::rectangle_t frameAfterMove = testCompShape.getFrameRect();

  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, FAULT);
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, FAULT);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.width, FAULT);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeAfterMoveOnDxDy)
{
  sachuk::Rectangle testRec({2, 4}, 3, 5);
  sachuk::Circle testCircle({4, 6}, 5);
  sachuk::shape_ptr rectPtr = std::make_shared<sachuk::Rectangle>(testRec);
  sachuk::shape_ptr circPtr = std::make_shared<sachuk::Circle>(testCircle);
  sachuk::CompositeShape testCompShape(rectPtr);
  testCompShape.add(circPtr);

  const double areaBeforeMove = testCompShape.getArea();
  const sachuk::rectangle_t frameBeforeMove = testCompShape.getFrameRect();

  testCompShape.move(2, 4);

  const double areaAfterMove = testCompShape.getArea();
  const sachuk::rectangle_t frameAfterMove = testCompShape.getFrameRect();

  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, FAULT);
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, FAULT);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.width, FAULT);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeScale)
{
  sachuk::Rectangle testRec({1, 2}, 2, 6);
  sachuk::Circle testCircle({3, 4}, 5);
  sachuk::shape_ptr rectPtr = std::make_shared<sachuk::Rectangle>(testRec);
  sachuk::shape_ptr circPtr = std::make_shared<sachuk::Circle>(testCircle);
  sachuk::CompositeShape testCompShape(rectPtr);
  testCompShape.add(circPtr);

  const double areaBeforeScale = testCompShape.getArea();
  const double multiplier = 3;

  testCompShape.scale(multiplier);
  const double areaAfterScale = testCompShape.getArea();

  BOOST_CHECK_CLOSE(areaBeforeScale * multiplier * multiplier, areaAfterScale, FAULT);
}

BOOST_AUTO_TEST_CASE(testScaleException)
{
  sachuk::Rectangle testRec({1, 2}, 2, 6);
  sachuk::Circle testCircle({3, 4}, 5);
  sachuk::shape_ptr rectPtr = std::make_shared<sachuk::Rectangle>(testRec);
  sachuk::shape_ptr circPtr = std::make_shared<sachuk::Circle>(testCircle);
  sachuk::CompositeShape testCompShape(rectPtr);
  testCompShape.add(circPtr);

  BOOST_CHECK_THROW(testCompShape.scale(-15), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testRemoveException)
{
  sachuk::Rectangle testRec({1, 2}, 2, 6);
  sachuk::Circle testCircle1({3, 4}, 5);
  sachuk::Circle testCircle2({5, 1}, 7);
  sachuk::shape_ptr rectPtr = std::make_shared<sachuk::Rectangle>(testRec);
  sachuk::shape_ptr circPtr1 = std::make_shared<sachuk::Circle>(testCircle1);
  sachuk::shape_ptr circPtr2 = std::make_shared<sachuk::Circle>(testCircle2);
  sachuk::CompositeShape testCompShape(rectPtr);
  testCompShape.add(circPtr1);
  testCompShape.add(circPtr2);

  BOOST_CHECK_THROW(testCompShape.remove(5), std::out_of_range);
  BOOST_CHECK_THROW(testCompShape.remove(-15), std::out_of_range);

}

BOOST_AUTO_TEST_CASE(testMoveInDotEmptyException)
{
  sachuk::CompositeShape testCompShape;

  BOOST_CHECK_THROW(testCompShape.move({2, 1}), std::logic_error);
}

BOOST_AUTO_TEST_CASE(testMoveOnDxDyEmptyException)
{
  sachuk::CompositeShape testCompShape;

  BOOST_CHECK_THROW(testCompShape.move(2, 2), std::logic_error);
}

BOOST_AUTO_TEST_CASE(testGetAreaEmptyException)
{
  sachuk::CompositeShape testCompShape;

  BOOST_CHECK_THROW(testCompShape.getArea(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(testScaleEmptyException)
{
  sachuk::CompositeShape testCompShape;

  BOOST_CHECK_THROW(testCompShape.scale(456), std::logic_error);
}

BOOST_AUTO_TEST_CASE(testGetFrameRectEmptyException)
{
  sachuk::CompositeShape testCompShape;

  BOOST_CHECK_THROW(testCompShape.getFrameRect(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(testInvalitNumberInArrException)
{
  sachuk::Circle testCircle({2, 2}, 3);
  sachuk::shape_ptr circPtr = std::make_shared<sachuk::Circle>(testCircle);
  sachuk::CompositeShape testCompShape(circPtr);

  BOOST_CHECK_THROW(testCompShape[1], std::out_of_range);
  BOOST_CHECK_THROW(testCompShape[-2], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(testCopyConstructor)
{
  sachuk::Rectangle testRec({1, 2}, 2, 6);
  sachuk::Circle testCircle({3, 4}, 5);
  sachuk::shape_ptr rectPtr = std::make_shared<sachuk::Rectangle>(testRec);
  sachuk::shape_ptr circPtr = std::make_shared<sachuk::Circle>(testCircle);
  sachuk::CompositeShape testCompShape1(rectPtr);
  testCompShape1.add(circPtr);

  const double areaCompShape1 = testCompShape1.getArea();
  const sachuk::rectangle_t frameRectCompShape1 = testCompShape1.getFrameRect();

  sachuk::CompositeShape testCompShape2(testCompShape1);

  BOOST_CHECK_CLOSE(areaCompShape1, testCompShape2.getArea(), FAULT);
  BOOST_CHECK_CLOSE(frameRectCompShape1.width, testCompShape2.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(frameRectCompShape1.height, testCompShape2.getFrameRect().height, FAULT);
}

BOOST_AUTO_TEST_CASE(testMoveConstructor)
{
  sachuk::Rectangle testRec({1, 2}, 2, 6);
  sachuk::Circle testCircle({3, 4}, 5);
  sachuk::shape_ptr rectPtr = std::make_shared<sachuk::Rectangle>(testRec);
  sachuk::shape_ptr circPtr = std::make_shared<sachuk::Circle>(testCircle);
  sachuk::CompositeShape testCompShape1(rectPtr);
  testCompShape1.add(circPtr);

  const double areaCompShape1 = testCompShape1.getArea();
  const sachuk::rectangle_t frameRectCompShape1 = testCompShape1.getFrameRect();

  sachuk::CompositeShape testCompShape2(std::move(testCompShape1));

  BOOST_CHECK_CLOSE(areaCompShape1, testCompShape2.getArea(), FAULT);
  BOOST_CHECK_CLOSE(frameRectCompShape1.width, testCompShape2.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(frameRectCompShape1.height, testCompShape2.getFrameRect().height, FAULT);
}

BOOST_AUTO_TEST_CASE(testCopyOperator)
{
  sachuk::Rectangle testRect1({1, 2}, 2, 6);
  sachuk::Rectangle testRect2({2, 2}, 4, 4);
  sachuk::shape_ptr rectPtr1 = std::make_shared<sachuk::Rectangle>(testRect1);
  sachuk::shape_ptr rectPtr2 = std::make_shared<sachuk::Rectangle>(testRect2);

  sachuk::Circle testCircle1({3, 4}, 5);
  sachuk::Circle testCircle2({5, 6}, 1);
  sachuk::shape_ptr circPtr1 = std::make_shared<sachuk::Circle>(testCircle1);
  sachuk::shape_ptr circPtr2 = std::make_shared<sachuk::Circle>(testCircle2);

  sachuk::CompositeShape testCompShape1(rectPtr1);
  testCompShape1.add(rectPtr2);

  sachuk::CompositeShape testCompShape2(circPtr1);
  testCompShape2.add(circPtr2);

  const double areaCompShape1 = testCompShape1.getArea();
  const sachuk::rectangle_t frameRectCompShape1 = testCompShape1.getFrameRect();

  testCompShape2 = testCompShape1;

  BOOST_CHECK_CLOSE(areaCompShape1, testCompShape2.getArea(), FAULT);
  BOOST_CHECK_CLOSE(frameRectCompShape1.width, testCompShape2.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(frameRectCompShape1.height, testCompShape2.getFrameRect().height, FAULT);
}

BOOST_AUTO_TEST_CASE(testMoveOperator)
{
  sachuk::Rectangle testRect1({1, 2}, 2, 6);
  sachuk::Rectangle testRect2({2, 2}, 4, 4);
  sachuk::shape_ptr rectPtr1 = std::make_shared<sachuk::Rectangle>(testRect1);
  sachuk::shape_ptr rectPtr2 = std::make_shared<sachuk::Rectangle>(testRect2);

  sachuk::Circle testCircle1({3, 4}, 5);
  sachuk::Circle testCircle2({5, 6}, 1);
  sachuk::shape_ptr circPtr1 = std::make_shared<sachuk::Circle>(testCircle1);
  sachuk::shape_ptr circPtr2 = std::make_shared<sachuk::Circle>(testCircle2);

  sachuk::CompositeShape testCompShape1(rectPtr1);
  testCompShape1.add(rectPtr2);

  sachuk::CompositeShape testCompShape2(circPtr1);
  testCompShape2.add(circPtr2);

  const double areaCompShape1 = testCompShape1.getArea();
  const sachuk::rectangle_t frameRectCompShape1 = testCompShape1.getFrameRect();

  testCompShape2 = std::move(testCompShape1);

  BOOST_CHECK_CLOSE(areaCompShape1, testCompShape2.getArea(), FAULT);
  BOOST_CHECK_CLOSE(frameRectCompShape1.width, testCompShape2.getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(frameRectCompShape1.height, testCompShape2.getFrameRect().height, FAULT);
}

BOOST_AUTO_TEST_CASE(testNumberInArrOperator)
{
  sachuk::Circle testCircle({2, 2}, 3);
  sachuk::shape_ptr circPtr = std::make_shared<sachuk::Circle>(testCircle);
  sachuk::CompositeShape testCompShape(circPtr);

  BOOST_CHECK_CLOSE(testCircle.getArea(), testCompShape[0]->getArea(), FAULT);
  BOOST_CHECK_CLOSE(testCircle.getFrameRect().width, testCompShape[0]->getFrameRect().width, FAULT);
  BOOST_CHECK_CLOSE(testCircle.getFrameRect().height, testCompShape[0]->getFrameRect().height, FAULT);
}

BOOST_AUTO_TEST_CASE(testRotation)
{
  sachuk::Rectangle testRec({1, 2}, 2, 6);
  sachuk::Circle testCircle({3, 4}, 5);
  sachuk::shape_ptr rectPtr = std::make_shared<sachuk::Rectangle>(testRec);
  sachuk::shape_ptr circPtr = std::make_shared<sachuk::Circle>(testCircle);
  sachuk::CompositeShape testCompShape(rectPtr);
  testCompShape.add(circPtr);

  const double areaBeforeRotate = testCompShape.getArea();

  testCompShape.rotate(44);
  BOOST_CHECK_CLOSE(areaBeforeRotate, testCompShape.getArea(), FAULT);
}

BOOST_AUTO_TEST_SUITE_END()
