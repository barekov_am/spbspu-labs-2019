#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace sachuk
{
  bool checkIntersect(const rectangle_t & shape1, const rectangle_t & shape2);

  Matrix part(const shapes_array & shapes, size_t number);
  Matrix part(const CompositeShape & compositeShape);
}

#endif //PARTITION_HPP
