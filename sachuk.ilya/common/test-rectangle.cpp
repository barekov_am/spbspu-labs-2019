#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>

#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(test_rectangle)

const double FAULT = 0.01;

BOOST_AUTO_TEST_CASE(testRectangleAfterMove)
{
  sachuk::Rectangle testRect({12, 3}, 13, 10);
  const double areaBeforeMove = testRect.getArea();
  const sachuk::rectangle_t frameBeforeMove = testRect.getFrameRect();

  testRect.move({4, 9});

  double areaAfterMove = testRect.getArea();
  sachuk::rectangle_t frameAfterMove = testRect.getFrameRect();
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, FAULT);
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, FAULT);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, FAULT);

  testRect.move(4, 5);

  areaAfterMove = testRect.getArea();
  frameAfterMove = testRect.getFrameRect();
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, FAULT);
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, FAULT);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, FAULT);
}

BOOST_AUTO_TEST_CASE(testRectangleScale)
{
  sachuk::Rectangle testRect({12, 3}, 13, 10);
  const double areaBeforeScale = testRect.getArea();

  const double multiplier = 5;
  testRect.scale(multiplier);

  const double areaAfterScale = testRect.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScale * multiplier * multiplier, areaAfterScale, FAULT);
}

BOOST_AUTO_TEST_CASE(testRectangleParameters)
{
  BOOST_CHECK_THROW(sachuk::Rectangle({12, -3}, -10, 20), std::invalid_argument);
  BOOST_CHECK_THROW(sachuk::Rectangle({12, -3}, 10, -4), std::invalid_argument);
  sachuk::Rectangle testRect({12, -3}, 4, 5);
  BOOST_CHECK_THROW(testRect.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testRotation)
{
  sachuk::Rectangle testRec({2, 3}, 4, 5);

  const double areaBeforeRotate = testRec.getArea();
  testRec.rotate(55);

  BOOST_CHECK_CLOSE(areaBeforeRotate, testRec.getArea(), FAULT);
}
BOOST_AUTO_TEST_SUITE_END()
