#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>

#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(test_circle)

const double FAULT = 0.01;

BOOST_AUTO_TEST_CASE(testCircleAfterMove)
{
  sachuk::Circle testCircle({2, 5}, 8);
  const double areaBeforeMove = testCircle.getArea();
  const sachuk::rectangle_t frameBeforeMove = testCircle.getFrameRect();

  testCircle.move({6, 8});

  double areaAfterMove = testCircle.getArea();
  sachuk::rectangle_t frameAfterMove = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, FAULT);
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, FAULT);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, FAULT);

  testCircle.move(6, 8);

  areaAfterMove = testCircle.getArea();
  frameAfterMove = testCircle.getFrameRect();
  BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, FAULT);
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, FAULT);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, FAULT);
}

BOOST_AUTO_TEST_CASE(testCircleScale)
{
  sachuk::Circle testCircle({2, 5}, 8);
  const double areaBeforeScale = testCircle.getArea();

  const double multiplier = 4;

  testCircle.scale(multiplier);
  const double areaAfterScale = testCircle.getArea();

  BOOST_CHECK_CLOSE(areaBeforeScale * multiplier * multiplier, areaAfterScale, FAULT);
}

BOOST_AUTO_TEST_CASE(testCircleParameters)
{
  BOOST_CHECK_THROW(sachuk::Circle({-3, 1}, -4), std::invalid_argument);
  sachuk::Circle testCircle({-3, 1}, 4);
  BOOST_CHECK_THROW(testCircle.scale(-4), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()
