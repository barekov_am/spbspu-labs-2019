#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <cmath>

sachuk::CompositeShape::CompositeShape():
  number_(0)
{
}

sachuk::CompositeShape::CompositeShape(const CompositeShape & obj):
  number_(obj.number_),
  shapes_(std::make_unique<shape_ptr[]>(obj.number_))
{
  for (size_t i = 0; i < number_; i++)
  {
    shapes_[i] = obj.shapes_[i];
  }
}

sachuk::CompositeShape::CompositeShape(CompositeShape && obj):
  number_(obj.number_),
  shapes_(std::move(obj.shapes_))
{
  obj.number_ = 0;
}

sachuk::CompositeShape::CompositeShape(const shape_ptr & shape):
  number_(1),
  shapes_(std::make_unique<shape_ptr[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Invalid argument");
  }
  shapes_[0] = shape;
}

sachuk::CompositeShape & sachuk::CompositeShape::operator =(const CompositeShape & obj)
{
  if (this != &obj)
  {
    number_ = obj.number_;
    shapes_ = std::make_unique<shape_ptr[]>(obj.number_);
    for (size_t i = 0; i < number_; i++)
    {
      shapes_[i] = obj.shapes_[i];
    }
  }
  return *this;
}

sachuk::CompositeShape & sachuk::CompositeShape::operator =(CompositeShape && obj)
{
  if (this != &obj)
  {
    number_ = obj.number_;
    shapes_ = std::move(obj.shapes_);
    obj.number_ = 0;
  }
  return *this;
}

sachuk::shape_ptr sachuk::CompositeShape::operator [](size_t numberInArr) const
{
  if (numberInArr >= number_)
  {
    throw std::out_of_range("Invalid argument");
  }
  return shapes_[numberInArr];
}

double sachuk::CompositeShape::getArea() const
{
  if (number_ == 0)
  {
    throw std::logic_error("Array list is empty");
  }
  double area = 0;

  for (size_t i = 0; i < number_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

sachuk::rectangle_t sachuk::CompositeShape::getFrameRect() const
{
  if (number_ == 0)
  {
    throw std::logic_error("Array list is empty");
  }

  sachuk::rectangle_t tRect = shapes_[0]->getFrameRect();
  sachuk::point_t lfTopPoint = {tRect.pos.x - tRect.width / 2, tRect.pos.y + tRect.height / 2};
  sachuk::point_t rtBotPoint = {tRect.pos.x + tRect.height / 2, tRect.pos.y - tRect.height / 2};

  for (size_t i = 1; i < number_; i++)
  {
    tRect = shapes_[i]->getFrameRect();

    lfTopPoint = {std::min(lfTopPoint.x, tRect.pos.x - tRect.width / 2), std::max(lfTopPoint.y, tRect.pos.y + tRect.height / 2)};
    rtBotPoint = {std::max(lfTopPoint.x, tRect.pos.x + tRect.width / 2), std::min(rtBotPoint.y, tRect.pos.y - tRect.height / 2)};
  }

  return {rtBotPoint.x - lfTopPoint.x, lfTopPoint.y - rtBotPoint.y, {(lfTopPoint.y + rtBotPoint.y) / 2, (rtBotPoint.x + lfTopPoint.x) / 2}};
}

void sachuk::CompositeShape::move(const point_t & point)
{
  if (number_ == 0)
  {
    throw std::logic_error("Array list is empty");
  }
  const double dx = point.x - getFrameRect().pos.x;
  const double dy = point.y - getFrameRect().pos.y;
  move(dx, dy);
}

void sachuk::CompositeShape::move(double dx, double dy)
{
  if (number_ == 0)
  {
    throw std::logic_error("Array list is empty");
  }
  for (size_t i = 0; i < number_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void sachuk::CompositeShape::scale(double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("Multiplier must be positive");
  }
  const sachuk::point_t frameRectCentr = getFrameRect().pos;

  for (size_t i = 0; i < number_; i++)
  {
    const double dx = (shapes_[i]->getFrameRect().pos.x - frameRectCentr.x) * (multiplier - 1);
    const double dy = (shapes_[i]->getFrameRect().pos.y - frameRectCentr.y) * (multiplier -1);
    shapes_[i]->move(dx, dy);
    shapes_[i]->scale(multiplier);
  }
}

void sachuk::CompositeShape::add(shape_ptr & shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape is nullptr");
  }
  shapes_array newArr(std::make_unique<shape_ptr[]>(number_ + 1));
  for (size_t i = 0; i < number_; i++)
  {
    newArr[i] = shapes_[i];
  }
  newArr[number_] = shape;
  number_++;
  shapes_.swap(newArr);
}

void sachuk::CompositeShape::remove(size_t numberInArr)
{
  if (numberInArr >= number_)
  {
    throw std::out_of_range("Invalid number");
  }

  for (size_t i = numberInArr; i < number_ - 1; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  shapes_[number_ - 1] = nullptr;
  number_--;
}

size_t sachuk::CompositeShape::getSize() const
{
  return number_;
}

void sachuk::CompositeShape::rotate(double angle)
{
  const double cosA = std::abs(std::cos(angle * M_PI / 180));
  const double sinA = std::abs(std::sin(angle * M_PI / 180));
  const point_t center = getFrameRect().pos;

  for (size_t i = 0; i < number_; i++)
  {
    point_t tmpArg = shapes_[i]->getFrameRect().pos;
    const double dx = (tmpArg.x - center.x) * (cosA - 1) - (tmpArg.y - center.y) * sinA;
    const double dy = (tmpArg.x - center.x) * sinA + (tmpArg.y - center.y) * (cosA - 1);

    shapes_[i]->move(dx, dy);
    shapes_[i]->rotate(angle);
  }

}

void sachuk::CompositeShape::printInfo() const
{
  std::cout << "CompositeShape:" << std::endl;
  std::cout << "Size:" << getSize() << std::endl;
  std::cout << "X:" << getFrameRect().pos.x << std::endl;
  std::cout << "Y:" << getFrameRect().pos.y << std::endl;
  std::cout << "Width:" << getFrameRect().width << std::endl;
  std::cout << "Height:" << getFrameRect().height << std::endl;
  std::cout << "Area:" << getArea() << std::endl;
}
