#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(testCopyConstructor)
{
  sachuk::Circle testCircle({2, 2}, 3);
  sachuk::shape_ptr circPtr = std::make_shared<sachuk::Circle>(testCircle);
  sachuk::CompositeShape testCompShape(circPtr);
  sachuk::Matrix copyMatrix = part(testCompShape);

  sachuk::Matrix matrix(copyMatrix);

  BOOST_CHECK_EQUAL(matrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(matrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(matrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(testMoveConstructor)
{
  sachuk::Circle testCircle({2, 2}, 3);
  sachuk::shape_ptr circPtr = std::make_shared<sachuk::Circle>(testCircle);
  sachuk::CompositeShape testCompShape(circPtr);
  sachuk::Matrix matrix = part(testCompShape);

  sachuk::Matrix copyMatrix(matrix);
  sachuk::Matrix newMatrix(std::move(matrix));

  BOOST_CHECK_EQUAL(newMatrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(newMatrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(newMatrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(testCopyOperator)
{
  sachuk::Circle testCircle({2, 2}, 3);
  sachuk::shape_ptr circPtr = std::make_shared<sachuk::Circle>(testCircle);
  sachuk::CompositeShape testCompShape(circPtr);
  sachuk::Matrix copyMatrix = part(testCompShape);

  sachuk::Matrix matrix = copyMatrix;

  BOOST_CHECK_EQUAL(matrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(matrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(matrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(testMoveOperator)
{
  sachuk::Circle testCircle({2, 2}, 3);
  sachuk::shape_ptr circPtr = std::make_shared<sachuk::Circle>(testCircle);
  sachuk::CompositeShape testCompShape(circPtr);
  sachuk::Matrix matrix = part(testCompShape);

  sachuk::Matrix copyMatrix(matrix);
  sachuk::Matrix newMatrix = std::move(matrix);

  BOOST_CHECK_EQUAL(newMatrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(newMatrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(newMatrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(equalException)
{
  const sachuk::Rectangle testRect1({3, 4}, 4, 4);
  const sachuk::Rectangle testRect2({2, 4}, 3, 4);
  sachuk::shape_ptr rectPtr1 = std::make_shared<sachuk::Rectangle>(testRect1);
  sachuk::shape_ptr rectPtr2 = std::make_shared<sachuk::Rectangle>(testRect2);

  const sachuk::Circle testCircle1({2, 2}, 3);
  const sachuk::Circle testCircle2({2, 3}, 4);
  sachuk::shape_ptr circPtr1 = std::make_shared<sachuk::Circle>(testCircle1);
  sachuk::shape_ptr circPtr2 = std::make_shared<sachuk::Circle>(testCircle2);

  sachuk::CompositeShape testCompShape(rectPtr1);
  testCompShape.add(circPtr1);
  testCompShape.add(circPtr2);

  sachuk::Matrix matrix = part(testCompShape);
  sachuk::Matrix equalMatrix(matrix);
  sachuk::Matrix unequalMatrix(matrix);
  unequalMatrix.add(circPtr2, 1, 1);

  BOOST_CHECK(matrix == equalMatrix);
  BOOST_CHECK(matrix != unequalMatrix);
}

BOOST_AUTO_TEST_CASE(opearatorException)
{
  const sachuk::Rectangle testRect1({3, 4}, 4, 4);
  const sachuk::Rectangle testRect2({2, 4}, 3, 4);
  sachuk::shape_ptr rectPtr1 = std::make_shared<sachuk::Rectangle>(testRect1);
  sachuk::shape_ptr rectPtr2 = std::make_shared<sachuk::Rectangle>(testRect2);

  const sachuk::Circle testCircle1({2, 2}, 3);
  const sachuk::Circle testCircle2({2, 3}, 4);
  sachuk::shape_ptr circPtr1 = std::make_shared<sachuk::Circle>(testCircle1);
  sachuk::shape_ptr circPtr2 = std::make_shared<sachuk::Circle>(testCircle2);

  sachuk::CompositeShape testCompShape(rectPtr1);
  testCompShape.add(circPtr1);
  testCompShape.add(rectPtr2);
  testCompShape.add(circPtr2);

  sachuk::Matrix matrix = part(testCompShape);
  BOOST_CHECK_THROW(matrix[10][5], std::out_of_range);
}
BOOST_AUTO_TEST_SUITE_END()
