#include "partition.hpp"
#include <cmath>

bool sachuk::checkIntersect(const rectangle_t & shape1, const rectangle_t & shape2)
{
  const double distanceBetweenCentersX = fabs(shape1.pos.x - shape2.pos.x);
  const double distanceBetweenCentersY = fabs(shape1.pos.y - shape2.pos.y);

  const double lengthX = (shape1.width + shape2.width) / 2;
  const double lengthY = (shape1.height + shape2.width) / 2;

  const bool conditionX = (distanceBetweenCentersX < lengthX);
  const bool conditionY = (distanceBetweenCentersY < lengthY);

  return conditionX && conditionY;
}

sachuk::Matrix sachuk::part(const shapes_array & shapes, size_t number)
{
  Matrix matrix;
  size_t lines = 0;
  size_t columns = 0;

  for (size_t i = 0; i < number; i++)
  {
    for (size_t j = 0; j < matrix.getLines(); j++)
    {
      for (size_t k = 0; k < matrix.getColumns(); k++)
      {
        if (matrix[j][k] == nullptr)
        {
          lines = j;
          columns = k;
          break;
        }

        if (checkIntersect(matrix[j][k]->getFrameRect(), shapes[i]->getFrameRect()))
        {
          lines = j + 1;
          columns = 0;
          break;
        }
        lines = j;
        columns = k + 1;
      }
      if (lines == j)
      {
        break;
      }
    }
    matrix.add(shapes[i], lines, columns);
  }
  return matrix;
}

sachuk::Matrix sachuk::part(const CompositeShape & compositeShape)
{
  shapes_array tmp(std::make_unique<shape_ptr[]>(compositeShape.getSize()));

  for (size_t i = 0; i < compositeShape.getSize(); i++)
  {
    tmp[i] = compositeShape[i];
  }
  Matrix matrix = part(tmp, compositeShape.getSize());
  return matrix;
}
