#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace sachuk
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape & obj);
    CompositeShape(CompositeShape && obj);
    CompositeShape(const shape_ptr & shape);
    ~CompositeShape() = default;

    CompositeShape & operator =(const CompositeShape & obj);
    CompositeShape & operator =(CompositeShape && obj);
    shape_ptr operator [](size_t numberInArr) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & point) override;
    void move(double dx, double dy) override;
    void scale(double multiplier) override;
    void add(shape_ptr & shape);
    void remove(size_t number);
    size_t getSize() const;
    void printInfo() const override;
    void rotate(double angle) override;

  private:
    size_t number_;
    shapes_array shapes_;
  };
}

#endif //COMPOSITE_SHAPE.HPP
