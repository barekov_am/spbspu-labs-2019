#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  sachuk::Circle circle1({2, 3}, 10);
  sachuk::Circle circle2({1, 1}, 4); //Создание круга
  sachuk::shape_ptr circPtr1 = std::make_shared<sachuk::Circle>(circle1);
  sachuk::shape_ptr circPtr2 = std::make_shared<sachuk::Circle>(circle2);
  circle1.printInfo();

  circle1.move({7, 8}); //Перемещение в точку
  circle1.printInfo();

  circle1.move(4, 5); //Смещение по осям
  circle1.printInfo();

  std::cout << "After scale" << std::endl;
  circle1.scale(6); //Масштабирование
  circle1.printInfo();

  sachuk::Rectangle rectangle1({5, 8}, 10, 14);
  sachuk::Rectangle rectangle2({4, 2}, 1, 1); //Создание прямоугольника
  sachuk::shape_ptr rectPtr1 = std::make_shared<sachuk::Rectangle>(rectangle1);
  sachuk::shape_ptr rectPtr2 = std::make_shared<sachuk::Rectangle>(rectangle2);

  rectangle1.printInfo();

  rectangle1.move({8, 3}); //Перемещение в точку
  rectangle1.printInfo();

  rectangle1.move(10, 23); //Перемещение по осям
  rectangle1.printInfo();

  std::cout << "After scale" << std::endl;
  rectangle1.scale(5); //Масштабирование
  rectangle1.printInfo();

  std::cout << "compoShape1" << std::endl;
  sachuk::CompositeShape compoShape1(circPtr1);

  compoShape1.printInfo();

  compoShape1.add(rectPtr1);
  compoShape1.printInfo();

  compoShape1.move({2, 8});
  compoShape1.printInfo();

  compoShape1.move(20, 12);
  compoShape1.printInfo();

  compoShape1.scale(6);
  compoShape1.printInfo();

  compoShape1.remove(1);
  compoShape1.printInfo();

  compoShape1.add(rectPtr2);
  compoShape1.printInfo();

  std::cout << "compoShape2" << std::endl;;
  sachuk::CompositeShape compoShape2(compoShape1);

  compoShape2.printInfo();

  compoShape2.add(circPtr2);
  compoShape2.printInfo();

  compoShape1 = compoShape2;
  compoShape1.printInfo();
  return 0;
}
