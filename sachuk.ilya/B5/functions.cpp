#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#include <math.h>
#include "functions.hpp"

const int TRIANGLE_VERTICES = 3;
const int QUADRANGLE_VERTICES = 4;
const int PENTAGON_VERTICES = 5;

std::list<Shape> fillContainer()
{
  std::list<Shape> shapes;
  std::string line;
  while (std::getline(std::cin, line))
  {
    Shape points;
    while (line.find_first_of(" \t") == 0)
    {
      line.erase(0, 1);
    }
    if (!line.empty())
    {
      int vertices = std::stoi(line.substr(0, 1));
      if (vertices < TRIANGLE_VERTICES)
      {
        throw std::invalid_argument("Invalid input");
      }
      for (int i = 0; i < vertices; i++)
      {
        while (line[0] != '(')
        {
          if (line.empty())
          {
            throw std::invalid_argument("Invalid input");
          }
          line.erase(0, 1);
        }
        size_t numSerapate = line.find_first_of(';');
        int x = std::stoi(line.substr(1, numSerapate - 1));

        size_t numSecondBracket = line.find_first_of(')');
        int y = std::stoi(line.substr(numSerapate + 1, numSecondBracket - 1 - numSerapate));
        points.push_back({ x, y });
        line.erase(0, numSecondBracket);
      }
    shapes.push_back(points);
    }
  }
  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::invalid_argument("Incorrect input");
  }

  return shapes;
}

int getLength(const Point& point1, const Point& point2)
{
  return ((point1.x - point2.x) * (point1.x - point2.x) + (point1.y - point2.y) * (point1.y - point2.y));
}

bool isRectangle(Shape& shape)
{
  if (!(shape.size() == QUADRANGLE_VERTICES))
  {
    return false;
  }
  return (getLength(shape[0], shape[1]) == getLength(shape[2], shape[3]) && getLength(shape[1], shape[2]) == getLength(shape[0], shape[3])
  && getLength(shape[0], shape[2]) == getLength(shape[1], shape[3]));
}

bool isSquare(Shape& shape)
{
  return (getLength(shape[0], shape[1]) == getLength(shape[1], shape[2])) && isRectangle(shape);
}

int countVerticates(std::list<Shape>& shapes)
{
  int counter = 0;

  for (std::list<Shape>::iterator iter = shapes.begin(); iter != shapes.end(); iter++)
  {
    counter += iter->size();
  }
  return counter;
}

int countTriangle(std::list<Shape>& shapes)
{
  int counter = std::count_if(shapes.begin(), shapes.end(), [](Shape& shape) { return shape.size() == TRIANGLE_VERTICES; });
  return counter;
}

int countSquare(std::list<Shape>& shapes)
{
  int counter = std::count_if(shapes.begin(), shapes.end(), [](Shape& shape) { return isSquare(shape); });
  return counter;
}

int countRectangle(std::list<Shape>& shapes)
{
  int counter = std::count_if(shapes.begin(), shapes.end(), [](Shape& shape) { return isRectangle(shape); });
  return counter;
}

void deletePentagons(std::list<Shape>& shapes)
{
  shapes.remove_if([](Shape& shape) { return shape.size() == PENTAGON_VERTICES; });
}

std::vector<Point> createVector(std::list<Shape>& shapes)
{
  std::vector<Point> vector;
  for (std::list<Shape>::iterator iter = shapes.begin(); iter != shapes.end(); iter++)
  {
    vector.push_back(iter->at(0));
  }
  return vector;
}

void printPoints(std::vector<Point> vector)
{
  std::cout << "Points:";
  for (size_t i = 0; i < vector.size(); i++)
  {
    std::cout << " (" << vector[i].x << "; " << vector[i].y << ")" << " ";
  }
  std::cout << '\n';
}

bool compare(Shape& shape1, Shape& shape2)
{
  if (shape1.size() < shape2.size())
  {
    return true;
  }
  else if (shape1.size() == QUADRANGLE_VERTICES && shape2.size() == QUADRANGLE_VERTICES && isSquare(shape1))
  {
    return true;
  }
  return false;
}

void sortContainer(std::list<Shape>& shapes)
{
  shapes.sort(compare);
}

void printContainer(std::list<Shape>& shapes)
{
  std::cout << "Shapes:" << '\n';
  for (std::list<Shape>::iterator iter = shapes.begin(); iter != shapes.end(); iter++)
  {
    std::cout << iter->size();
    for (Shape::size_type j = 0; j != iter->size(); j++)
    {
      std::cout << " (" << iter->at(j).x << "; " << iter->at(j).y << ")" << " ";
    }
    std::cout << '\n';
  }
}
