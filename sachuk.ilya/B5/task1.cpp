#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "tasks.hpp"

void task1()
{
  std::vector<std::string> vector_words;
  std::string str;
  while (std::getline(std::cin, str))
  {
    while (!str.empty())
    {
      if (str.empty())
      {
        break;
      }
      while (str.find_first_of(" \t\n") == 0)
      {
        str.erase(0, 1);
      }
      size_t firstChar = 0;
      size_t lastChar = str.find_first_of(" \t\n");
      if (str.empty())
      {
        break;
      }
      vector_words.push_back(str.substr(firstChar, lastChar));
      str.erase(firstChar, lastChar);
    }

  }
  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::invalid_argument("Incorrect input");
  }

  std::sort(vector_words.begin(), vector_words.end());

  vector_words.erase(std::unique(vector_words.begin(), vector_words.end()), vector_words.end());

  for (size_t i = 0; i < vector_words.size(); i++)
  {
    std::cout << vector_words[i] << '\n';
  }
}
