#ifndef POINT_HPP
#define POINT_HPP

#include <vector>

struct Point
{
  int x, y;
};

using Shape = std::vector<Point>;

#endif // POINT_HPP
