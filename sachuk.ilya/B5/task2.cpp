#include <iostream>
#include "tasks.hpp"
#include "functions.hpp"

void task2()
{
  std::list<Shape> shapeContainer;

  shapeContainer = fillContainer();

  int numberOfVertices = countVerticates(shapeContainer);
  int numberOfTriangle = countTriangle(shapeContainer);
  int numberOfSquare = countSquare(shapeContainer);
  int numberOfRectangle = countRectangle(shapeContainer);

  std::cout << "Vertices: " << numberOfVertices << '\n';
  std::cout << "Triangles: " << numberOfTriangle << '\n';
  std::cout << "Squares: " << numberOfSquare << '\n';
  std::cout << "Rectangles: " << numberOfRectangle << '\n';

  deletePentagons(shapeContainer);

  std::vector<Point> points = createVector(shapeContainer);
  printPoints(points);

  sortContainer(shapeContainer);


  printContainer(shapeContainer);
}
