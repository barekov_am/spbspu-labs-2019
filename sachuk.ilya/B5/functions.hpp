#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <list>
#include "point.hpp"

std::list<Shape> fillContainer();

int getLength(const Point& point1, const Point& point2);
bool isRectangle(Shape& shape);
bool isSquare(Shape& shape);

int countVerticates(std::list<Shape>& shapes);
int countTriangle(std::list<Shape>& shapes);
int countSquare(std::list<Shape>& shapes);
int countRectangle(std::list<Shape>& shapes);

void deletePentagons(std::list<Shape>& shapes);

std::vector<Point> createVector(std::list<Shape>& shapes);
void printPoints(std::vector<Point> vector);

bool compare(Shape& shape1, Shape& shape2);
void sortContainer(std::list<Shape>& shapes);

void printContainer(std::list<Shape>& shapes);


#endif // FUNCTIONS_HPP
