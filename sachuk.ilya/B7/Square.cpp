#include <iostream>
#include "Square.hpp"

Square::Square(int x, int y):
  Shape(x, y)
{

}

void Square::draw() const
{
  std::cout << "SQUARE (" << getPoint().x << "; " << getPoint().y << ")" << '\n';
}
