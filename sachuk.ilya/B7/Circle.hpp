#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "Shape.hpp"

class Circle : public Shape
{
public:
  Circle(int x, int y);
  void draw() const override;
};

#endif // CIRCLE_HPP
