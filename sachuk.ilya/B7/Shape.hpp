#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "Point.hpp"

class Shape
{
public:
  Shape(int x, int y);

  virtual ~Shape() = default;
  virtual void draw() const = 0;

  bool isMoreLeft(std::shared_ptr<Shape> shape);
  bool isMoreUpper(std::shared_ptr<Shape> shape);

  point getPoint() const;
private:
  point center_;
};

#endif // SHAPE_HPP
