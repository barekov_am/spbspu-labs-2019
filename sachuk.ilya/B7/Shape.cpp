#include "Shape.hpp"

Shape::Shape(int x, int y):
  center_({x, y})
{

}

bool Shape::isMoreLeft(std::shared_ptr<Shape> shape)
{
  return center_.x < shape->center_.x;
}

bool Shape::isMoreUpper(std::shared_ptr<Shape> shape)
{
  return center_.y > shape->center_.y;
}

point Shape::getPoint() const
{
  return center_;
}
