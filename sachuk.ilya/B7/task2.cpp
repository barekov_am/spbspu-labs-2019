#include <vector>
#include <algorithm>
#include <string>
#include <iostream>
#include "tasks.hpp"
#include "Shape.hpp"
#include "Triangle.hpp"
#include "Square.hpp"
#include "Circle.hpp"

std::vector<std::shared_ptr<Shape>> fillVector()
{
  std::vector<std::shared_ptr<Shape>> vect;
  std::string line;
  while (std::getline(std::cin, line))
  {
    while (line.find_first_of(" \t") == 0)
    {
      line.erase(0, 1);
    }
    if (!line.empty())
    {

      std::string shapeName = line.substr(0, line.find_first_of('('));
      size_t counter = 0;
      while (shapeName[counter] != ' ' && shapeName[counter] != '\t' && counter < shapeName.size())
      {
        counter++;
      }
      shapeName = shapeName.substr(0, counter);
      if ((line.find('(') != std::string::npos) && (line.find(';') != std::string::npos) && (line.find(')') != std::string::npos))
      {
        size_t firstBracket = line.find_first_of('(');
        size_t secondBracket = line.find_first_of(')');
        size_t separator = line.find_first_of(';');

        int x = std::stoi(line.substr(firstBracket + 1, separator - firstBracket + 1));
        int y = std::stoi(line.substr(separator + 1, secondBracket - separator + 1));

        std::shared_ptr<Shape> ptr;

        if (shapeName.compare("CIRCLE") == 0)
        {
          ptr = std::make_shared<Circle>(x, y);
        }
        else if (shapeName.compare("TRIANGLE") == 0)
        {
          ptr = std::make_shared<Triangle>(x, y);
        }
        else if (shapeName.compare("SQUARE") == 0)
        {
          ptr = std::make_shared<Square>(x, y);
        }
        else
        {
          throw std::invalid_argument("Invalid input");
        }

        vect.push_back(ptr);
      }
      else
      {
        throw std::invalid_argument("Invalid input");
      }
    }
  }
  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::invalid_argument("Incorrect input");
  }

  return vect;
}

void task2()
{
  std::vector<std::shared_ptr<Shape>> vector = fillVector();

  std::cout << "Original:" << '\n';
  std::for_each(vector.begin(), vector.end(), [](const std::shared_ptr<Shape>& shape) { shape->draw(); });

  std::sort(vector.begin(), vector.end(), [](const std::shared_ptr<Shape>& shape1, const std::shared_ptr<Shape>& shape2)
  { return shape1->isMoreLeft(shape2); });

  std::cout << "Left-Right:" << '\n';
  std::for_each(vector.begin(), vector.end(), [](const std::shared_ptr<Shape>& shape) { shape->draw(); });

  std::reverse(vector.begin(), vector.end());

  std::cout << "Right-Left:" << '\n';
  std::for_each(vector.begin(), vector.end(), [](const std::shared_ptr<Shape>& shape) { shape->draw(); });

  std::sort(vector.begin(), vector.end(), [](const std::shared_ptr<Shape>& shape1, const std::shared_ptr<Shape>& shape2)
  { return shape1->isMoreUpper(shape2); });

  std::cout << "Top-Bottom:" << '\n';
  std::for_each(vector.begin(), vector.end(), [](const std::shared_ptr<Shape>& shape) { shape->draw(); });

  std::reverse(vector.begin(), vector.end());

  std::cout << "Bottom-Top:" << '\n';
  std::for_each(vector.begin(), vector.end(), [](const std::shared_ptr<Shape>& shape) { shape->draw(); });
}
