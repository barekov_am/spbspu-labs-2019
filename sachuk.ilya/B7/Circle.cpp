#include <iostream>
#include "Circle.hpp"

Circle::Circle(int x, int y):
  Shape(x, y)
{

}

void Circle::draw() const
{
  std::cout << "CIRCLE (" << getPoint().x << "; " << getPoint().y << ")" << '\n';
}
