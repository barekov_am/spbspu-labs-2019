#include <vector>
#include <iterator>
#include <iostream>
#include <algorithm>
#include <cmath>
#include "tasks.hpp"

void task1()
{
  std::vector<double> vector;
  std::copy(std::istream_iterator<double>(std::cin), std::istream_iterator<double>(), std::inserter(vector, vector.end()));

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Invalid input");
  }

  std::transform(vector.begin(), vector.end(), vector.begin(), std::bind1st(std::multiplies<double>(), M_PI));
  std::copy(vector.begin(), vector.end(), std::ostream_iterator<double>(std::cout, "\n"));
}
