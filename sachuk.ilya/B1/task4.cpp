#include <random>
#include <ctime>
#include "tasks.hpp"
#include "addition.hpp"

void fillRandom(double* array, int size)
{
  std::mt19937 generator(time(0));
  std::uniform_real_distribution<> dis(-1.0, 1.0);

  for (int i = 0; i < size; i++)
  {
    array[i] = dis(generator);
  }

}

void task4(const char* way, int size)
{
  if (size == 0)
  {
    throw std::invalid_argument("Size can not be 0");
  }

  bool direction = isAscending(way);

  std::vector<double> vector(size);

  fillRandom(&vector[0], size);

  print(vector);

  sort<AccessByBrackets>(vector, direction);

  print(vector);
}
