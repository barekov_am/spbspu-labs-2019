#include <fstream>
#include <vector>
#include <memory>
#include "addition.hpp"

const std::size_t INITSIZE = 256;

void task2(const char* file)
{
  std::ifstream inFile(file);
  if (!inFile)
  {
    throw std::invalid_argument("Invalid name of file");
  }

  std::size_t size = INITSIZE;

  std::unique_ptr<char[], decltype(&free)> arr(static_cast<char*>(calloc(size, 1)), &free);

  if (!arr)
  {
    throw std::runtime_error("Memory can not be allocated");
  }

  std::size_t count = 0;

  while (inFile)
  {
    inFile.read(&arr[count], INITSIZE);
    count += inFile.gcount();

    if (inFile.gcount() == INITSIZE)
    {
      size += INITSIZE;
      std::unique_ptr<char[], decltype(&free)> newArr(static_cast<char*>(realloc(arr.get(), size)), &free);
      if (!newArr)
      {
        throw std::runtime_error("Memory can not be allocated");
      }
      arr.release();
      std::swap(arr, newArr);
    }
  }

  inFile.close();

  if (inFile.is_open())
  {
    throw std::runtime_error("File was not closed");
  }

  std::vector<char> vec1(&arr[0], &arr[count]);

  for (auto i: vec1)
  {
    std::cout << i;
  }
}
