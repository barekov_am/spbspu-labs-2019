#ifndef TASK_HPP
#define TASK_HPP

void task1(char* way);
void task2(const char* file);
void task3();
void task4(const char* way, int size);

#endif // TASK_HPP
