#ifndef ADDITION_HPP
#define ADDITION_HPP

#include <string>
#include <iostream>
#include "accession.hpp"
bool isAscending(const char* direction);

template<template<typename> typename Access, typename Container>
void sort(Container& container, bool direction)
{
  for (auto i = Access<Container>::begin(container); i != Access<Container>::end(container); i++)
  {
    for (auto j = i; j != Access<Container>::end(container); j++)
    {
      if (direction)
      {
        if (Access<Container>::getElement(container, i) > Access<Container>::getElement(container, j))
        {
          std::swap(Access<Container>::getElement(container, i), Access<Container>::getElement(container, j));
        }
      }
      else
      {
        if (Access<Container>::getElement(container, i) < Access<Container>::getElement(container, j))
        {
          std::swap(Access<Container>::getElement(container, i), Access<Container>::getElement(container, j));
        }
      }
    }
  }
}

template<typename Container>
void print(const Container& container)
{
  for (auto i = container.begin(); i != container.end(); i++)
  {
    std::cout << *i << " ";
  }
  std::cout << std::endl;
}

#endif // ADDITION_HPP
