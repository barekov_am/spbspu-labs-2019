#include <vector>
#include <iostream>
#include "tasks.hpp"
#include "addition.hpp"
void task3()
{
  std::vector<int> vector;
  int value = 0;

  while (std::cin >> value)
  {
    if (value == 0)
    {
      break;
    }
    vector.push_back(value);
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::invalid_argument("Incorrect input");
  }

  if (value != 0)
  {
    throw std::invalid_argument("Last argument must be 0");
  }

  if (vector.empty())
  {
    return;
  }

  int lastValue = vector.back();

  if (lastValue == 1)
  {
    auto iter = vector.begin();
    while (iter != vector.end())
    {
      if (*iter % 2 == 0)
      {
        iter = vector.erase(iter);
      }
      else
      {
        iter++;
      }
    }
  }
  else if (lastValue == 2)
  {
    auto iter = vector.begin();
    while (iter != vector.end())
    {
      if (*iter % 3 == 0)
      {
        iter = vector.insert(iter + 1, 3, 1) + 3;
      }
      else
      {
        iter++;
      }
    }
  }

  print(vector);
}
