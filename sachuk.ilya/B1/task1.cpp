#include <vector>
#include <forward_list>
#include "tasks.hpp"
#include "addition.hpp"
void task1(char* way)
{
  bool direction = isAscending(way);
  std::vector<int> vectorBr;
  int number = 0;
  while (std::cin && !(std::cin >> number).eof())
  {
    if (std::cin.fail() || std::cin.bad())
    {
      throw std::ios_base::failure("Incorrect argument");
    }
    vectorBr.push_back(number);
  }

  if (vectorBr.empty())
  {
    return;
  }
  std::vector<int> vectorAt(vectorBr);
  std::forward_list<int> list(vectorBr.begin(), vectorBr.end());
  sort<AccessByBrackets>(vectorBr, direction);
  sort<AccessByAt>(vectorAt, direction);
  sort<AccessByIterator>(list, direction);

  print(vectorBr);
  print(vectorAt);
  print(list);
}
