#include <cstring>
#include "addition.hpp"
bool isAscending(const char* direction)
{
  if (std::strcmp(direction, "ascending") != 0 && std::strcmp(direction, "descending") != 0)
  {
    throw std::invalid_argument("Invalid direction argument");
  }

  bool ascending = false;
  if (std::strcmp(direction, "ascending") == 0)
  {
    ascending = true;;
  }
  return ascending;
}
