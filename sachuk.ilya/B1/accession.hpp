#ifndef ACCESSION_HPP
#define ACCESSION_HPP

#include <cstddef>

template <typename Container>
struct AccessByBrackets
{
  typedef typename Container::size_type index;

  static index begin(const Container&)
  {
    return 0;
  }

  static index next(std::size_t i)
  {
    return i + 1;
  }

  static index end(const Container& container)
  {
    return container.size();
  }

  static typename Container::reference getElement(Container& container, std::size_t i)
  {
    return container[i];
  }
};

template <typename Container>
struct AccessByAt
{
  typedef typename Container::size_type index;

  static index begin(const Container&)
  {
    return 0;
  }

  static index next(std::size_t i)
  {
    return i + 1;
  }

  static index end(const Container& container)
  {
    return container.size();
  }

  static typename Container::reference getElement(Container& container, std::size_t i)
  {
    return container.at(i);
  }
};

template <typename Container>
struct AccessByIterator
{
  typedef typename Container::iterator index;

  static index begin(Container& container)
  {
    return container.begin();
  }

  static index end(Container& container)
  {
    return container.end();
  }

  static typename Container::reference getElement(Container&, index iter)
  {
    return *iter;
  }
};

#endif // ACCESSION_HPP
