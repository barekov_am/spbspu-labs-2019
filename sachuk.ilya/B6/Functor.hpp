#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

class Functor
{
public:
  Functor();

  void operator ()(int value);
  long int getMaxValue();
  long int getMinValue();
  double getMeanValue();
  int getNumberOfPositive();
  int getNumberOfNegative();
  long int getOddSum();
  long int getEvenSum();
  bool isFirstEqualLast();

  void printInfo();

private:
  int numberOfElements_;
  long int sumOfElements_;
  long int maxValue_;
  long int minValue_;
  double meanValue_;
  int numberOfPositive_;
  int numberOfNegative_;
  long int oddSum_;
  long int evenSum_;
  long int firstValue_;
  long int lastValue_;
};

#endif // !FUNCTOR_HPP
