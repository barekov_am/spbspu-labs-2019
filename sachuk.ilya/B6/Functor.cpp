#include <iostream>
#include "Functor.hpp"

Functor::Functor():
  numberOfElements_(0),
  sumOfElements_(0),
  maxValue_(0),
  minValue_(0),
  meanValue_(0),
  numberOfPositive_(0),
  numberOfNegative_(0),
  oddSum_(0),
  evenSum_(0),
  firstValue_(0),
  lastValue_(0)
{
}

void Functor::operator()(const int value)
{
  sumOfElements_ += value;
  lastValue_ = value;

  if (value > maxValue_)
  {
    maxValue_ = value;
  }

  if (value < minValue_)
  {
    minValue_ = value;
  }

  if (value > 0)
  {
    numberOfPositive_++;
  }
  else if (value < 0)
  {
    numberOfNegative_++;
  }

  if (value % 2 != 0)
  {
    oddSum_ += value;
  }
  else
  {
    evenSum_ += value;
  }

  if (numberOfElements_ == 0)
  {
    numberOfElements_++;
    firstValue_ = value;
    maxValue_ = value;
    minValue_ = value;
  }
  else
  {
    numberOfElements_++;
  }
}

long int Functor::getMaxValue()
{
  return maxValue_;
}

long int Functor::getMinValue()
{
  return minValue_;
}

double Functor::getMeanValue()
{
  meanValue_ = static_cast<double>(sumOfElements_) / static_cast<double>(numberOfElements_);
  return meanValue_;
}

int Functor::getNumberOfPositive()
{
  return numberOfPositive_;
}

int Functor::getNumberOfNegative()
{
  return numberOfNegative_;
}

long int Functor::getOddSum()
{
  return oddSum_;
}

long int Functor::getEvenSum()
{
  return evenSum_;
}

bool Functor::isFirstEqualLast()
{
  return firstValue_ == lastValue_;
}

void Functor::printInfo()
{
  std::cout << "Max: " << getMaxValue() << '\n';
  std::cout << "Min: " << getMinValue() << '\n';
  std::cout << "Mean: " << getMeanValue() << '\n';
  std::cout << "Positive: " << getNumberOfPositive() << '\n';
  std::cout << "Negative: " << getNumberOfNegative() << '\n';
  std::cout << "Odd Sum: " << getOddSum() << '\n';
  std::cout << "Even Sum: " << getEvenSum() << '\n';
  std::cout << "First/Last Equal: ";
  if (isFirstEqualLast())
  {
    std::cout << "yes" << '\n';
  }
  else
  {
    std::cout << "no" << '\n';
  }
}
