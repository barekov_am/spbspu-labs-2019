#include <iostream>
#include <string>
#include <sstream>
#include "QueueWithPriorityImpl.hpp"
#include "tasks.hpp"
void task1()
{
  QueueWithPriority<std::string> queue;
  std::string command;
  while (std::cin >> command)
  {
    if (command.compare("add") == 0)
    {
      std::string priority;
      std::string data;
      if (std::cin.peek() != '\n')
      {
        std::cin >> priority;

        std::getline(std::cin, data);

        std::istringstream str(data);

        std::getline(str >> std::ws, data);
      }
      if (data.empty())
      {
        std::cout << "<INVALID COMMAND>" << '\n';
      }
      if (priority.compare("high") == 0)
      {
        queue.putElementToQueue(data, QueueWithPriority<std::string>::ElementPriority::HIGH);
      }
      else if (priority.compare("normal") == 0)
      {
        queue.putElementToQueue(data, QueueWithPriority<std::string>::ElementPriority::NORMAL);
      }
      else if (priority.compare("low") == 0)
      {
        queue.putElementToQueue(data, QueueWithPriority<std::string>::ElementPriority::LOW);
      }
      else
      {
        std::cout << "<INVALID COMMAND>" << '\n';
      }
    }
    else if (command.compare("get") == 0)
    {
      if (queue.empty())
      {
        std::cout << "<EMPTY>" << '\n';
      }
      else
      {
        std::cout << queue.geElementFromQueue() << '\n';
      }
    }
    else if (command.compare("accelerate") == 0)
    {
      queue.accelerate();
    }
    else
    {
      std::cout << "<INVALID COMMAND>" << '\n';
    }
  }
}
