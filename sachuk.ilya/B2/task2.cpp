#include <list>
#include <iostream>
#include <iterator>
#include"tasks.hpp"
void task2()
{
  std::list<int> list;
  int size = 0;
  int number = 0;
  const int maxValue = 20;
  const int minValue = 1;
  const int maxSize = 20;
  while (std::cin >> number)
  {
    if (size < maxSize)
    {
    if (number < minValue || number > maxValue)
    {
      throw std::invalid_argument("List can contain numbers ranging from 1 to 20");
    }
    list.push_back(number);
    size++;
    }
    else
    {
      throw std::invalid_argument("List can not contain more than 20 el.");
    }
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::invalid_argument("Incorrect input");
  }

  if (list.empty())
  {
    return;
  }

  std::list<int>::iterator itBegin = list.begin();
  std::list<int>::iterator itEnd = list.end();
  itEnd--;
  int counter = 0;
  for (itBegin = list.begin(); itBegin != itEnd; itBegin++, itEnd--, counter++)
  {
    if (size % 2 == 0)
    {
      if (counter == size / 2)
      {
        break;
      }
    }
    std::cout << *itBegin << " " << *itEnd << " ";
  }
  if (size % 2 != 0)
  {
    std::cout << *itBegin;
  }
  std::cout << std::endl;
}
