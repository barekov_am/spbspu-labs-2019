#ifndef QUEUEWITHPRIORITYIMPL_HPP
#define QUEUEWITHPRIORITYIMPL_HPP

#include <exception>
#include <iostream>
#include "QueueWithPriority.hpp"

template <typename T>
void QueueWithPriority<T>::putElementToQueue(const T& element, ElementPriority priority)
{
  switch (priority)
  {
    case ElementPriority::HIGH:
      highPriority.push_back(element);
      break;
    case ElementPriority::NORMAL:
      normPriority.push_back(element);
      break;
    case ElementPriority::LOW:
      lowPriority.push_back(element);
      break;
  }
}

template <typename T>
T QueueWithPriority<T>::geElementFromQueue()
{
  if (empty())
  {
    throw std::range_error("Queue is empty");
  }

  if (!highPriority.empty())
  {
    T element = highPriority.front();
    highPriority.pop_front();
    return element;
  }
  else if (!normPriority.empty())
  {
    T element = normPriority.front();
    normPriority.pop_front();
    return element;
  }
  else
  {
    T element = lowPriority.front();
    lowPriority.pop_front();
    return element;
  }
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  typename std::list<T>::iterator it;
  for (it = lowPriority.begin(); it != lowPriority.end(); it++)
  {
    highPriority.push_back(*it);
  }
  lowPriority.clear();
}

template <typename T>
bool QueueWithPriority<T>::empty()
{
  return highPriority.empty() && normPriority.empty() && lowPriority.empty();
}

#endif //QUEUEWITHPRIORITYIMPL_HPP
