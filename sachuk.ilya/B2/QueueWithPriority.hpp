#ifndef QUEUEWITHPRIORITY_HPP
#define QUEUEWITHPRIORITY_HPP

#include <list>

template <typename T>
class QueueWithPriority
{
public:

  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  void putElementToQueue(const T& element, ElementPriority priority);

  T geElementFromQueue();

  void accelerate();

  bool empty();

private:
  std::list<T> highPriority;
  std::list<T> normPriority;
  std::list<T> lowPriority;
};

#endif // QUEUEWITTHPRIORITY_HPP
