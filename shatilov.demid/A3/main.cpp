#include <iostream>
#include <cassert>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

void printRectFrames(const shatilov::Shape *shape)
{
  assert(shape != nullptr);
  shatilov::rectangle_t frameRect = shape->getFrameRect();
  std::cout << "{" << frameRect.pos.x << ","
            << frameRect.pos.y << "},"
            << frameRect.width << ","
            << frameRect.height << "\n";
}

int main()
{
  shatilov::Rectangle rec1({3, 2}, 1, 8);
  rec1.move(5, 8);
  std::cout << rec1.getArea() << "\n";

  printRectFrames(&rec1);

  shatilov::Rectangle rec2({2, 1}, 4, 2);
  rec2.move({2, 1});

  printRectFrames(&rec2);

  shatilov::Rectangle rec3({1, 4}, 2, 2);
  rec3.scale(5);

  printRectFrames(&rec3);

  shatilov::Circle cir1({1, 7}, 5);
  cir1.move({3, 5});
  std::cout << cir1.getArea() << "\n";

  printRectFrames(&cir1);

  shatilov::Circle cir2({8, 4}, 4);
  cir2.move(4, 3);

  printRectFrames(&cir2);

  shatilov::Circle cir3({3, 5}, 4);
  cir3.scale(1.6);

  printRectFrames(&cir3);


  std::cout << "Composite shapes : " << "\n";

  shatilov::Rectangle rec4({7, 9}, 4, 3);
  std::cout << "Area of rectangle is " << rec4.getArea() << "\n";
  shatilov::Circle cir4({1, 1}, 1);
  std::cout << "Area of circle is " << cir4.getArea() << "\n";

  shatilov::CompositeShape compShape1;
  compShape1.add(std::make_shared<shatilov::Rectangle>(rec4));
  printRectFrames(&compShape1);
  compShape1.add(std::make_shared<shatilov::Circle>(cir4));
  printRectFrames(&compShape1);
  std::cout << "Area of 1st composite shape is " << compShape1.getArea() << "\n";

  shatilov::Rectangle rec5({15, 15}, 2, 4);
  std::cout << "Area of rectangle is " << rec5.getArea() << "\n";

  compShape1.add(std::make_shared<shatilov::Rectangle>(rec5));
  std::cout << "Area of 2nd composite shape after adding rectangle is " << compShape1.getArea() << "\n";

  std::cout << "There are " << compShape1.size() << " shape_s left" << "\n";

  std::cout << "The area of last shape is " << compShape1[1]->getArea() << "\n";

  return 0;
}
