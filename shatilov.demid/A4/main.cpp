#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "layering.hpp"

void printRectFrames(const shatilov::Shape &shape)
{
  shatilov::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "{" << frameRect.pos.x << ","
            << frameRect.pos.y << "},"
            << frameRect.width << ","
            << frameRect.height << "\n";
}

void printMatrixInfo(const shatilov::Matrix<shatilov::Shape> &matrix)
{
  for (size_t i = 0; i < matrix.getRowsCount(); i++)
  {
    std::cout << "Layer number " << i + 1 << " : \n";
    std::cout << "Amount of shapes : " << matrix[i].getSize() << "\n";
    for (size_t j = 0; j < matrix[i].getSize(); j++)
    {
      std::cout << "Shape number " << j + 1 << " with parameters : ";
      printRectFrames(*matrix[i][j]);
    }
  }
}

int main()
{
  shatilov::Rectangle rec1({ 3, 2 }, 1, 8);
  rec1.move(5, 8);
  std::cout << rec1.getArea() << "\n";
  printRectFrames(rec1);

  shatilov::Rectangle rec2({ 2, 1 }, 4, 2);
  rec2.move({ 2, 1 });
  printRectFrames(rec2);

  shatilov::Rectangle rec3({ 1, 4 }, 2, 2);
  rec3.scale(5);
  printRectFrames(rec3);

  shatilov::Circle cir1({ 1, 7 }, 5);
  cir1.move({ 3, 5 });
  std::cout << cir1.getArea() << "\n";
  printRectFrames(cir1);

  shatilov::Circle cir2({ 8, 4 }, 4);
  cir2.move(4, 3);
  printRectFrames(cir2);

  shatilov::Circle cir3({ 3, 5 }, 4);
  cir3.scale(1.6);
  printRectFrames(cir3);


  std::cout << "Composite shapes : " << "\n";

  shatilov::Rectangle rec4({ 7, 9 }, 4, 3);
  std::cout << "Area of rectangle is " << rec4.getArea() << "\n";
  shatilov::Circle cir4({ 1, 1 }, 1);
  std::cout << "Area of circle is " << cir4.getArea() << "\n";

  shatilov::CompositeShape compShape1;
  compShape1.add(std::make_shared<shatilov::Rectangle>(rec4));
  printRectFrames(compShape1);
  compShape1.add(std::make_shared<shatilov::Circle>(cir4));
  printRectFrames(compShape1);
  std::cout << "General area of 1 composite shape is " << compShape1.getArea() << "\n";

  compShape1.move(5, 5);
  printRectFrames(compShape1);
  compShape1.move({ 6, 8 });
  printRectFrames(compShape1);
  compShape1.scale(2);
  std::cout << "Area of 1 composite shape after multiplication by 2 is " << compShape1.getArea() << "\n";
  printRectFrames(compShape1);
  compShape1.scale(0.19);
  std::cout << "Area of 1 composite shape after multiplication by 0.19 is " << compShape1.getArea() << "\n";
  printRectFrames(compShape1);

  compShape1.remove(0);
  std::cout << "General area of 1 composite shape after removing rectangle is " << compShape1.getArea() << "\n";

  shatilov::CompositeShape compShape2(compShape1);
  std::cout << "Area of 2 composite shape is " << compShape2.getArea() << "\n";
  shatilov::Rectangle rec5({ 15, 15 }, 2, 4);
  std::cout << "Area of rectangle is " << rec5.getArea() << "\n";

  compShape2.add(std::make_shared<shatilov::Rectangle>(rec5));
  std::cout << "Area of 2 composite shape after adding rectangle is " << compShape2.getArea() << "\n";
  std::cout << "There are " << compShape2.size() << " shape_s left" << "\n";
  std::cout << "The area of last shape is " << compShape2[1]->getArea() << "\n";


  std::cout << "Layering composite shape :" << "\n";
  shatilov::CompositeShape compShape3;
  shatilov::Rectangle rec6({ 1, 1 }, 4, 8);
  shatilov::Circle cir6({ 2, 2 }, 1);
  shatilov::Circle cir7({ 8, 8 }, 3);
  compShape3.add(std::make_shared<shatilov::Rectangle>(rec6));
  compShape3.add(std::make_shared<shatilov::Circle>(cir6));
  compShape3.add(std::make_shared<shatilov::Circle>(cir7));

  shatilov::Matrix<shatilov::Shape> matrix;
  matrix = shatilov::split(compShape3);
  printMatrixInfo(matrix);
  std::cout << "The biggest number of shapes in a layer is " << matrix.getMaxColSize() << " shapes \n";

  return 0;
}
