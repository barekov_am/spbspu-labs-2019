#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "layering.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(layeringTesting)

BOOST_AUTO_TEST_CASE(matrixParametersAfterLayering)
{
shatilov::Circle cir1({1, 1}, 2);
shatilov::Circle cir2({10, 10}, 2);
shatilov::Rectangle rec1({5, 2}, 10, 4);

std::shared_ptr<shatilov::Shape> cirPointer1 = std::make_shared<shatilov::Circle>(cir1);
std::shared_ptr<shatilov::Shape> cirPointer2 = std::make_shared<shatilov::Circle>(cir2);
std::shared_ptr<shatilov::Shape> recPointer1 = std::make_shared<shatilov::Rectangle>(rec1);

shatilov::CompositeShape compShape;
compShape.add(cirPointer1);
compShape.add(recPointer1);
compShape.add(cirPointer2);

shatilov::Matrix<shatilov::Shape> matrix;
matrix = shatilov::split(compShape);

BOOST_CHECK_EQUAL(matrix.getMaxColSize(), 2);
BOOST_CHECK_EQUAL(matrix.getRowsCount(), 2);
BOOST_CHECK_EQUAL(matrix[0].getSize(), 2);
BOOST_CHECK_EQUAL(matrix[1].getSize(), 1);

BOOST_CHECK(matrix[0][0] == cirPointer1);
BOOST_CHECK(matrix[0][1] == cirPointer2);
BOOST_CHECK(matrix[1][0] == recPointer1);
}

BOOST_AUTO_TEST_CASE(matrixParametersAfterAdding)
{
shatilov::Rectangle rec1({5, 2}, 10, 4);
shatilov::Circle cir1({1, 1}, 2);

shatilov::Circle cir2({10, 10}, 2);
std::shared_ptr<shatilov::Shape> cirPointer1 = std::make_shared<shatilov::Circle>(cir1);
std::shared_ptr<shatilov::Shape> cirPointer2 = std::make_shared<shatilov::Circle>(cir2);
std::shared_ptr<shatilov::Shape> recPointer1 = std::make_shared<shatilov::Rectangle>(rec1);

shatilov::CompositeShape compShape;
compShape.add(cirPointer1);
compShape.add(recPointer1);
compShape.add(cirPointer2);

shatilov::Rectangle rec2({15, 15}, 1, 2);
std::shared_ptr<shatilov::Shape> recPointer2 = std::make_shared<shatilov::Rectangle>(rec2);

shatilov::Matrix<shatilov::Shape> matrix;
matrix = shatilov::split(compShape);

size_t maxColBefore = matrix.getMaxColSize();
size_t rowsCountBefore = matrix.getRowsCount();
size_t layer1SizeBefore = matrix[0].getSize();
size_t layer2SizeBefore = matrix[1].getSize();

matrix.add(0, recPointer2);

BOOST_CHECK_EQUAL(maxColBefore + 1, matrix.getMaxColSize());
BOOST_CHECK_EQUAL(rowsCountBefore, matrix.getRowsCount());
BOOST_CHECK_EQUAL(layer1SizeBefore + 1, matrix[0].getSize());
BOOST_CHECK_EQUAL(layer2SizeBefore, matrix[1].getSize());

BOOST_CHECK(matrix[0][0] == cirPointer1);
BOOST_CHECK(matrix[0][1] == cirPointer2);
BOOST_CHECK(matrix[0][2] == recPointer2);
BOOST_CHECK(matrix[1][0] == recPointer1);

}
BOOST_AUTO_TEST_CASE(intersectTest)
{
shatilov::Rectangle rectangle({10, 5}, 1, 2);
shatilov::Circle circle1({3, 2}, 2);
shatilov::Circle circle2({10, 6}, 2);

BOOST_CHECK(!shatilov::intersect(rectangle, circle1));
BOOST_CHECK(shatilov::intersect(rectangle, circle2));
}


BOOST_AUTO_TEST_SUITE_END()
