
#ifndef LAYERING_HPP
#define LAYERING_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace shatilov
{
  Matrix<Shape> split(const CompositeShape &compShape);
  bool intersect(const shatilov::Shape &shape1, const shatilov::Shape &shape2);
}

#endif
