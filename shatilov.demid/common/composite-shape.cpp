#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

shatilov::CompositeShape::CompositeShape() :
count_(0)
{
}

shatilov::CompositeShape::CompositeShape(const CompositeShape &compShape) :
count_(compShape.count_),
shapes_(std::make_unique<shape_ptr[]>(compShape.count_))
{
for (size_t i = 0; i < count_; i++)
{
shapes_[i] = compShape.shapes_[i];
}
}

shatilov::CompositeShape::CompositeShape(shatilov::CompositeShape &&compShape) :
count_(compShape.count_),
shapes_(std::move(compShape.shapes_))
{
compShape.count_ = 0;
}

shatilov::CompositeShape &shatilov::CompositeShape::operator =(const shatilov::CompositeShape &rhs)
{
  if (this != &rhs)
  {
    shapes_ = std::make_unique<shape_ptr[]>(rhs.count_);
    count_ = rhs.count_;
    for (size_t i = 0; i < count_; i++)
    {
      shapes_[i] = rhs.shapes_[i];
    }
  }
  return *this;
}

shatilov::CompositeShape &shatilov::CompositeShape::operator =(shatilov::CompositeShape &&rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    rhs.count_ = 0;
    shapes_ = std::move(rhs.shapes_);
  }
  return *this;
}

shatilov::shape_ptr shatilov::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }
  return shapes_[index];
}

double shatilov::CompositeShape::getArea() const
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("There are no shapes");
  }
  double area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

shatilov::rectangle_t shatilov::CompositeShape::getFrameRect() const
{
  if (shapes_ != nullptr)
  {
    rectangle_t frameRect = shapes_[0]->getFrameRect();
    double minX = frameRect.pos.x - frameRect.width / 2;
    double minY = frameRect.pos.y - frameRect.height / 2;
    double maxX = frameRect.pos.x + frameRect.width / 2;
    double maxY = frameRect.pos.y + frameRect.height / 2;
    for (size_t i = 1; i < count_; i++)
    {
      frameRect = shapes_[i]->getFrameRect();
      double comparable = frameRect.pos.x - frameRect.width / 2;
      minX = std::min(comparable, minX);
      comparable = frameRect.pos.y - frameRect.height / 2;
      minY = std::min(comparable, minY);
      comparable = frameRect.pos.x + frameRect.width / 2;
      maxX = std::max(comparable, maxX);
      comparable = frameRect.pos.y + frameRect.height / 2;
      maxY = std::max(comparable, maxY);
    }
    return rectangle_t{maxX - minX, maxY - minY, {(maxX + minX) / 2, (maxY + minY) / 2} };
  }
  throw std::logic_error("There are no shapes");
}

void shatilov::CompositeShape::move(double dx, double dy)
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("There are no shapes");
  }
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void shatilov::CompositeShape::move(const shatilov::point_t &newCenter)
{
  const shatilov::point_t frameCenter = getFrameRect().pos;
  double dx = newCenter.x - frameCenter.x;
  double dy = newCenter.y - frameCenter.y;
  move(dx, dy);
}

void shatilov::CompositeShape::scale(double factor)
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("There are no shapes");
  }
  if (factor <= 0)
  {
    throw std::invalid_argument("Factor must be positive");
  }
  const shatilov::point_t posFrameRect = getFrameRect().pos;
  for (size_t i = 0; i < count_; ++i)
  {
    shatilov::point_t shapeCenter = shapes_[i]->getFrameRect().pos;
    shapes_[i]->scale(factor);
    double dx = (shapeCenter.x - posFrameRect.x) * (factor - 1);
    double dy = (shapeCenter.y - posFrameRect.y) * (factor - 1);
    shapes_[i]->move(dx, dy);
  }
}

void shatilov::CompositeShape::add(const shape_ptr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("No shape to add");
  }
  for (size_t i = 0; i < count_; i++)
  {
    if (shapes_[i] == shape)
    {
      return;
    }
  }
  count_++;
  shapes_array tempShapes = std::make_unique<shape_ptr[]>(count_);
  for (size_t i = 0; i < (count_ - 1); i++)
  {
    tempShapes[i] = shapes_[i];
  }
  tempShapes[count_ - 1] = shape;
  shapes_ = std::move(tempShapes);
}

void shatilov::CompositeShape::remove(std::size_t index)
{
  if (index >= count_)
    {
      throw std::out_of_range("Index is out of range");
    }
  for (size_t i = index; i < count_ - 1; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  shapes_[count_ - 1] = nullptr;
  count_--;
}

size_t shatilov::CompositeShape::size() const
{
  return count_;
}

void shatilov::CompositeShape::rotate(double angle)
{
  const shatilov::point_t center = getFrameRect().pos;
  const double cosine = std::abs(std::cos(angle * M_PI / 180));
  const double sinus = std::abs(std::sin(angle * M_PI / 180));
  for (size_t i = 0; i < count_; ++i)
  {
    const shatilov::point_t currCenter = shapes_[i]->getFrameRect().pos;
    const double projection_x = currCenter.x - center.x;
    const double projection_y = currCenter.y - center.y;
    const double shift_x = projection_x * (cosine - 1) - projection_y * sinus;
    const double shift_y = projection_x * sinus + projection_y * (cosine - 1);
    shapes_[i]->move(shift_x, shift_y);
    shapes_[i]->rotate(angle);
  }
}
