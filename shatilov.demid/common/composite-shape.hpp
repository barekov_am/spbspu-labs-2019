#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace shatilov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &compShape);
    CompositeShape(CompositeShape &&compShape);
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape &rhs);
    CompositeShape& operator =(CompositeShape &&rhs);
    shape_ptr operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &newCenter) override;
    void scale(double factor) override;
    void add(const shape_ptr shape);
    void remove(size_t index);
    size_t size() const;
    void rotate(double angle) override;

  private:
    size_t count_;
    shapes_array shapes_;
  };
}


#endif
