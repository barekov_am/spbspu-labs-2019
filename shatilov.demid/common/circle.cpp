#define _USE_MATH_DEFINES
#include "circle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

shatilov::Circle::Circle(const shatilov::point_t &newCenter, double radius) :
center_(newCenter),
radius_(radius)
{
  if (radius <= 0)
  {
    throw std::invalid_argument("Problematic radius");
  }
}

void shatilov::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void shatilov::Circle::move(const point_t &newCenter)
{
  center_ = newCenter;
}

double shatilov::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

shatilov::rectangle_t shatilov::Circle::getFrameRect() const
{
  return { 2 * radius_, 2 * radius_, center_ };
}

void shatilov::Circle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Problematic factor");
  }
  radius_ *= factor;
}

void shatilov::Circle::rotate(double)
{
}
