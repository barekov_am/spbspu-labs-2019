#include <iostream>
#include "shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

void dataOutput(const borshchev::Shape* Shape)
{
  double shapeArea = Shape->getArea();
  borshchev::rectangle_t tempFrameRect = Shape->getFrameRect();
  std::cout << "Shape's AREA is: " << shapeArea << std::endl;
  std::cout << "Shape's FRAME RECTANGLE:" << std::endl;
  std::cout << "WIDTH: " << tempFrameRect.width << std::endl;
  std::cout << "HEIGHT: " << tempFrameRect.height << std::endl;
  std::cout << "CENTER: x: " << tempFrameRect.pos.x << ", y: " << tempFrameRect.pos.y << std::endl;
  std::cout << std::endl;
}

int main()
{
  borshchev::Rectangle rectangle(4, 6, { 5, 5 });
  borshchev::Shape *shape = &rectangle;
  dataOutput(shape);
  std::cout << "Rectangle's AREA: " << shape->getArea() << '\n';
  shape->move(8, 9);
  shape->move({ 2, 0 });
  dataOutput(shape);
  shape->scale(2.4);
  std::cout << "Rectangle's new AREA: " << shape->getArea() << '\n';

  borshchev::Circle circle(6, { 5, 5 });
  shape = &circle;
  dataOutput(shape);
  std::cout << "Circle's AREA: " << shape->getArea() << '\n';
  shape->move(4, 9);
  shape->move({ -3, 7 });
  dataOutput(shape);
  shape->scale(0.5);
  std::cout << "Circle's new AREA: " << shape->getArea() << '\n';

  return 0;
}
