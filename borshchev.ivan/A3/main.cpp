#include <iostream>
#include "shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  borshchev::Circle circle1(3, { 4, 4 });
  borshchev::Circle circle2(5, { 0, -2 });

  borshchev::CompositeShape::shape_ptr testCircle1 = std::make_shared<borshchev::Circle>(circle1);
  borshchev::CompositeShape::shape_ptr testCircle2 = std::make_shared<borshchev::Circle>(circle2);

  std::cout << "Creating COMPOSITE SHAPE" << '\n';
  borshchev::CompositeShape compositeShape;

  std::cout << "Adding other shapes to COMPOSITE SHAPE" << '\n';
  compositeShape.add(testCircle1);
  compositeShape.add(testCircle2);

  std::cout << "Moving COMPOSITE SHAPE by few units (2, 6)" << '\n';
  compositeShape.move(2, 6);

  std::cout << "Moving COMPOSITE SHAPE at point {0, -2}" << '\n';
  compositeShape.move({ 0, -2 });

  std::cout << "Scale COMPOSITE SHAPE by 0,5 coefficient" << '\n';
  compositeShape.scale(0.5);

  std::cout << "---Composite Shape after two MOVES and SCALING : " << '\n';
  compositeShape.displayParameters();

  std::cout << "Remove shapes out with INDEX [2]" << '\n';
  compositeShape.remove(2);

  std::cout << "---Composite Shape after two REMOVING two shapes : " << '\n';
  compositeShape.displayParameters();

  return 0;
}
