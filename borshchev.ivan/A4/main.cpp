#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "layering.hpp"

void dataOutput(const borshchev::Shape* Shape)
{
  double shapeArea = Shape->getArea();
  borshchev::rectangle_t tempFrameRect = Shape->getFrameRect();
  std::cout << "Shape's AREA is: " << shapeArea << std::endl;
  std::cout << "Shape's FRAME RECTANGLE:" << std::endl;
  std::cout << "WIDTH: " << tempFrameRect.width << std::endl;
  std::cout << "HEIGHT: " << tempFrameRect.height << std::endl;
  std::cout << "CENTER: x: " << tempFrameRect.pos.x << ", y: " << tempFrameRect.pos.y << std::endl;
  std::cout << std::endl;
}

int main()
{
  borshchev::Circle myCircle1(6.0, { 4.0, 4.0 });
  borshchev::Rectangle myRectangle2(3.0, 1.0, { -7.0, 4.0 });
  borshchev::Rectangle myRectangle3(3.0, 3.0, { 5.0, 4.0 });

  borshchev::CompositeShape::shape_ptr part1 = std::make_shared<borshchev::Rectangle>(myRectangle2);
  borshchev::CompositeShape::shape_ptr part2 = std::make_shared<borshchev::Circle>(myCircle1);
  borshchev::CompositeShape::shape_ptr part3 = std::make_shared<borshchev::Rectangle>(myRectangle3);

  borshchev::CompositeShape composite_shape;
  composite_shape.add(part1);
  composite_shape.add(part2);
  composite_shape.add(part3);

  composite_shape.move({ 0, 0 });
  composite_shape.displayParameters();

  composite_shape.move(3, 3);
  composite_shape.displayParameters();

  composite_shape.rotate(45);
  composite_shape.displayParameters();

  composite_shape.scale(2);
  composite_shape.displayParameters();

  composite_shape.remove(1);
  composite_shape.displayParameters();

  borshchev::Matrix matrix = borshchev::split(composite_shape);
  for (int i = 0; i < matrix.getRows(); i++)
  {
    for (int j = 0; j < matrix.getNumberOfFigures(); j++)
    {
      if (matrix[i][j] != nullptr)
      {
        std::cout << "LAYER : " << i << ":\n" << "SHAPES : " << j << ":\n";
        std::cout << "POSITION : (" << matrix[i][j]->getPos().x << "; "
          << matrix[i][j]->getPos().y << ")\n";
      }
    }
  }
  return 0;
}
