#include "matrix.hpp"
#include <iostream>

borshchev::Matrix::Layer::Layer(borshchev::Shape::shape_ptr *arr, int size) :
  size_(size),
  figures_(std::make_unique<Shape::shape_ptr[]>(size))
{
  for (int i = 0; i < size_; i++)
  {
    figures_[i] = arr[i];
  }
}

borshchev::Shape::shape_ptr borshchev::Matrix::Layer::operator [](int index) const
{
  if (index >= size_)
  {
    throw std::invalid_argument("Incorrect value, INDEX is out of bounds");
  }

  return figures_[index];
}

int borshchev::Matrix::Layer::getSize() const
{
  return size_;
}

borshchev::Matrix::Matrix() :
  rows_(0),
  numberOfFigures(0)
{
}

borshchev::Matrix::Matrix(const borshchev::Matrix &other) :
  rows_(other.rows_),
  columns_(new int[other.rows_]),
  figures_(std::make_unique<Shape::shape_ptr[]>(other.numberOfFigures)),
  numberOfFigures(other.numberOfFigures)
{
  for (int i = 0; i < numberOfFigures; i++)
  {
    figures_[i] = other.figures_[i];
  }

  for (int i = 1; i < rows_; i++)
  {
    columns_[i] = other.columns_[i];
  }
}

borshchev::Matrix::Matrix(borshchev::Matrix &&other) :
  rows_(other.rows_),
  columns_(std::move(other.columns_)),
  figures_(std::move(other.figures_)),
  numberOfFigures(other.numberOfFigures)
{
}

borshchev::Matrix &borshchev::Matrix::operator =(const borshchev::Matrix &other)
{
  if (this != &other)
  {
    rows_ = other.rows_;
    numberOfFigures = other.numberOfFigures;

    Shape::shapes_array tmpFigures = std::make_unique<Shape::shape_ptr[]>(other.numberOfFigures);
    for (int i = 0; i < numberOfFigures; i++)
    {
      tmpFigures[i] = other.figures_[i];
    }
    figures_.swap(tmpFigures);

    std::unique_ptr<int[]> tmpColumns = std::make_unique<int[]>(other.rows_);
    for (int i = 1; i < rows_; i++)
    {
      tmpColumns[i] = other.columns_[i];
    }
    columns_.swap(tmpColumns);
  }

  return *this;
}

borshchev::Matrix &borshchev::Matrix::operator =(borshchev::Matrix &&other)
{
  if (this != &other)
  {
    rows_ = other.rows_;
    columns_ = std::move(other.columns_);
    figures_ = std::move(other.figures_);
    numberOfFigures = other.numberOfFigures;
  }

  return *this;
}

borshchev::Matrix::Layer borshchev::Matrix::operator [](int row) const
{
  if (row >= rows_)
  {
    throw std::invalid_argument("Incorrect value, INDEX is out of bounds");
  }

  int index = 0;
  for (int i = 0; i < row; i++)
  {
    index += columns_[i];
  }

  return Layer(&figures_[index], columns_[row]);
}

void borshchev::Matrix::add(Shape::shape_ptr shape, int row)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Incorrect pointer, shape's POINTER should not be null");
  }

  if (row > rows_)
  {
    throw std::invalid_argument("Incorrect value, INDEX is out of bounds");
  }

  std::unique_ptr<Shape::shape_ptr[]> tmpFigures =
    std::make_unique<Shape::shape_ptr[]>(numberOfFigures + 1);

  int figureCounter = 0;
  int tmpCounter = 0;

  for (int tmpRow = 0; tmpRow < rows_; tmpRow++)
  {
    for (int column = 0; column < columns_[tmpRow]; column++)
    {
      tmpFigures[tmpCounter++] = figures_[figureCounter++];
    }

    if (row == tmpRow)
    {
      tmpFigures[tmpCounter++] = shape;
      columns_[tmpRow]++;
    }
  }

  if (row == rows_)
  {
    std::unique_ptr<int[]> tmpColumns = std::make_unique<int[]>(rows_ + 1);

    for (int tmpRow = 0; tmpRow < rows_; tmpRow++)
    {
      tmpColumns[tmpRow] = columns_[tmpRow];
    }

    tmpFigures[tmpCounter] = shape;
    tmpColumns[rows_] = 1;
    rows_++;
    columns_.swap(tmpColumns);
  }

  numberOfFigures++;
  figures_.swap(tmpFigures);
}

int borshchev::Matrix::getRows() const
{
  return rows_;
}

int borshchev::Matrix::getNumberOfFigures() const
{
  return numberOfFigures;
}

void borshchev::Matrix::printInfo() const
{
  std::cout << "THE MATRIX" << std::endl;
  std::cout << "Quantity of ROWS   : " << rows_ << std::endl;
  std::cout << "Quantity of SHAPES : " << numberOfFigures << std::endl;
}
