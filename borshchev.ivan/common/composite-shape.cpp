#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <algorithm>

borshchev::CompositeShape::CompositeShape() :
  count_(0),
  angle_(0)
{
}

borshchev::CompositeShape::CompositeShape(const CompositeShape& tmp) :
  count_(tmp.count_),
  angle_(tmp.angle_),
  list_(std::make_unique<shape_ptr[]>(tmp.count_))
{
  for (int i = 0; i < count_; i++)
  {
    list_[i] = tmp.list_[i];
  }
}

borshchev::CompositeShape::CompositeShape(CompositeShape&& tmp) :
  count_(tmp.count_),
  angle_(tmp.angle_),
  list_(std::move(tmp.list_))
{
  tmp.count_ = 0;
}

borshchev::CompositeShape::CompositeShape(const shape_ptr& shape) :
  count_(1),
  list_(std::make_unique<shape_ptr[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Incorrect value, shape's POINTER should be not null");
  }

  list_[0] = shape;
}

borshchev::CompositeShape& borshchev::CompositeShape::operator = (const CompositeShape& tmp)
{
  if (this != &tmp)
  {
    count_ = tmp.count_;
    for (int i = 0; i < count_; i++)
    {
      list_[i] = tmp.list_[i];
    }
  }
  return *this;
}

borshchev::CompositeShape& borshchev::CompositeShape::operator = (borshchev::CompositeShape&& tmp)
{
  if (this != &tmp)
  {
    count_ = tmp.count_;
    list_ = std::move(tmp.list_);
    tmp.count_ = 0;
  }
  return *this;
}

borshchev::CompositeShape::shape_ptr borshchev::CompositeShape::operator [] (int i) const
{
  if ((i < 0) || (i >= count_))
  {
    throw std::out_of_range("Incorrect value, INDEX is out of bounds");
  }

  return list_[i];
}

double borshchev::CompositeShape::getArea() const
{
  if (count_ <= 0)
  {
    throw std::logic_error("Error, COMPOSITE SHAPE is empty");
  }

  double area = 0.0;
  for (int i = 0; i < count_; i++)
  {
    area += list_[i]->getArea();
  }
  return area;
}

borshchev::rectangle_t borshchev::CompositeShape::getFrameRect() const
{
  if (count_ <= 0)
  {
    throw std::logic_error("Error, COMPOSITE SHAPE is empty");
  }

  borshchev::rectangle_t firstFrameRect = list_[0]->getFrameRect();

  double left = firstFrameRect.pos.x - firstFrameRect.width / 2;
  double right = firstFrameRect.pos.x + firstFrameRect.width / 2;
  double top = firstFrameRect.pos.y + firstFrameRect.height / 2;
  double bottom = firstFrameRect.pos.y - firstFrameRect.height / 2;

  for (int i = 1; i < count_; i++)
  {
    rectangle_t curFrameRect = list_[i]->getFrameRect();

    double leftBorder = curFrameRect.pos.x - curFrameRect.width / 2;
    double rightBorder = curFrameRect.pos.x + curFrameRect.width / 2;
    double topBorder = curFrameRect.pos.y + curFrameRect.height / 2;
    double bottomBorder = curFrameRect.pos.y - curFrameRect.height / 2;

    left = std::min(left, leftBorder);
    right = std::max(right, rightBorder);
    top = std::max(top, topBorder);
    bottom = std::min(bottom, bottomBorder);
  }
  return rectangle_t{ right - left, top - bottom, point_t{ (left + right) / 2, (top + bottom) / 2 } };
}

void borshchev::CompositeShape::move(double x, double y)
{
  if (count_ == 0)
  {
    throw std::logic_error("Error, COMPOSITE SHAPE is empty");
  }

  for (int i = 0; i < count_; i++)
  {
    list_[i]->move(x, y);
  }
}

void borshchev::CompositeShape::move(const point_t &pos)
{
  if (count_ <= 0)
  {
    throw std::logic_error("Error, COMPOSITE SHAPE is empty");
  }

  point_t center = getFrameRect().pos;
  for (int i = 0; i < count_; i++)
  {
    list_[i]->move(pos.x - center.x, pos.y - center.y);
  }
}

void borshchev::CompositeShape::scale(double coeficient)
{
  if (coeficient <= 0)
  {
    throw std::invalid_argument("Incorrect value, scaling COEFFICIENT should be positive");
  }
  if (count_ == 0)
  {
    throw std::logic_error("Error, COMPOSITE SHAPE is empty");
  }

  point_t tempFrame = getFrameRect().pos;

  for (int i = 0; i < count_; i++)
  {
    borshchev::point_t currentFrame = list_[i]->getFrameRect().pos;
    list_[i]->move((currentFrame.x - tempFrame.x) * (coeficient - 1),
      (currentFrame.y - tempFrame.y) * (coeficient - 1));
    list_[i]->scale(coeficient);
  }
}

int borshchev::CompositeShape::getSize() const
{
  return count_;
}

void borshchev::CompositeShape::add(shape_ptr Shape)
{
  if (Shape == nullptr)
  {
    throw std::invalid_argument("Incorrect pointer, shape's POINTER should not be null");
  }

  ++count_;
  shapes_array tmpList(std::make_unique<shape_ptr[]>(count_));

  for (int i = 0; i < count_ - 1; i++)
  {
    tmpList[i] = list_[i];
  }
  tmpList[count_ - 1] = Shape;
  list_.swap(tmpList);
}

void borshchev::CompositeShape::remove(int index)
{
  if (index > count_)
  {
    throw std::out_of_range("Incorrect value, INDEX is out of bounds");
  }

  count_--;

  for (int i = index; i < count_; i++)
  {
    list_[i] = list_[i + 1];
  }
  list_[count_] = nullptr;
}

void borshchev::CompositeShape::remove(shape_ptr other)
{
  for (int i = 0; i < count_; i++)
  {
    if (list_[i] == other)
    {
      remove(i);
      return;
    }
  }
}

void borshchev::CompositeShape::rotate(double angle)
{
  point_t tempRect = getFrameRect().pos;
  double radAngle = angle * M_PI / 180;

  for (int i = 0; i < count_; i++)
  {
    list_[i]->rotate(angle);
    double curX = list_[i]->getFrameRect().pos.x - tempRect.x;
    double curY = list_[i]->getFrameRect().pos.y - tempRect.y;
    double newX = curX * (std::cos(radAngle) - 1) - curY * std::sin(radAngle);
    double newY = curY * (std::cos(radAngle) - 1) - curX * std::sin(radAngle);
    list_[i]->move(newX, newY);
  }
}

void borshchev::CompositeShape::displayParameters() const
{
  borshchev::rectangle_t rectangle = getFrameRect();
  std::cout << "CompositeShape's CENTER is (" << getFrameRect().pos.x << ","
    << getFrameRect().pos.x << ")\n"
    << "Rectangle frame's WIDTH = " << rectangle.width
    << ", HEIGHT = " << rectangle.height << "\n"
    << "Total AREA = " << getArea() << "\n";
}

borshchev::point_t borshchev::CompositeShape::getPos() const
{
  return getFrameRect().pos;
}

int borshchev::CompositeShape::getShapeCount() const
{
  return count_;
}
