#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(rectangle_test_A4)

BOOST_AUTO_TEST_CASE(rectangle_area_test)
{
  borshchev::Rectangle testRect(3.0, 6.0, { 10.0, 10.0 });
  const double areaBeforeMoving = testRect.getArea();
  const borshchev::rectangle_t frameBeforeMove = testRect.getFrameRect();

  testRect.move({ 0, 0 });

  double areaAfterMoving = testRect.getArea();
  borshchev::rectangle_t frameAfterMoving = testRect.getFrameRect();

  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMoving.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMoving.height, ACCURACY);

  testRect.move(4, 5);

  areaAfterMoving = testRect.getArea();
  frameAfterMoving = testRect.getFrameRect();

  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMoving.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMoving.height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(rectangle_scaling_test)
{
  borshchev::Rectangle testRect(3.0, 6.0, { 10.0, 10.0 });
  const double areaBeforeScaling = testRect.getArea();

  const double coeficient = 2;
  testRect.scale(coeficient);

  const double areaAfterScaling = testRect.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScaling * coeficient * coeficient, areaAfterScaling, ACCURACY);
}

BOOST_AUTO_TEST_CASE(rectangle_construction_test)
{
  BOOST_CHECK_THROW(borshchev::Rectangle rect(-3.0, 6.0, { 10.0, 10.0 }), std::invalid_argument);
  BOOST_CHECK_THROW(borshchev::Rectangle rect(3.0, -6.0, { 10.0, 10.0 }), std::invalid_argument);
  BOOST_CHECK_THROW(borshchev::Rectangle rect(-3.0, -6.0, { 10.0, 10.0 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangle_scale_test)
{
  borshchev::Rectangle testRect(3.0, 6.0, { 10.0, 10.0 });
  BOOST_CHECK_THROW(testRect.scale(-1.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
