#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

namespace borshchev
{
  class Circle : public Shape
  {
  public:
    Circle(double radius, const point_t &pos);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double x, double y) override;
    void move(const point_t &pos) override;
    void scale(double scalingCoefficient) override;
    void rotate(double angle) override;
    point_t getPos() const override;

  private:
    double radius_;
    point_t pos_;
  };
}

#endif
