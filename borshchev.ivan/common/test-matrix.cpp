#include <boost/test/auto_unit_test.hpp>
#include "matrix.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(matrix_test_A4)

BOOST_AUTO_TEST_CASE(matrix_copying_construction_test)
{
  borshchev::Rectangle rec(4.0, 2.0, { 10.0, 3.0 });
  borshchev::Circle cir(2.0, { 4.0, 4.0 });

  borshchev::Matrix matrix;
  matrix.add(std::make_shared<borshchev::Rectangle>(rec), 0);
  matrix.add(std::make_shared<borshchev::Circle>(cir), 1);

  borshchev::Matrix testMatrix(matrix);

  BOOST_CHECK_EQUAL(matrix.getRows(), testMatrix.getRows());
  BOOST_CHECK_EQUAL(matrix.getNumberOfFigures(), testMatrix.getNumberOfFigures());
}

BOOST_AUTO_TEST_CASE(matrix_copying_operator_test)
{
  borshchev::Rectangle rec(4.0, 2.0, { 10.0, 3.0 });
  borshchev::Circle cir(2.0, { 4.0, 4.0 });

  borshchev::Matrix matrix;
  matrix.add(std::make_shared<borshchev::Rectangle>(rec), 0);
  matrix.add(std::make_shared<borshchev::Circle>(cir), 1);

  borshchev::Matrix testMatrix = matrix;

  BOOST_CHECK_EQUAL(matrix.getRows(), testMatrix.getRows());
  BOOST_CHECK_EQUAL(matrix.getNumberOfFigures(), testMatrix.getNumberOfFigures());
}

BOOST_AUTO_TEST_CASE(matrix_index_calling_test)
{
  borshchev::Rectangle rec(4.0, 2.0, { 10.0, 3.0 });
  borshchev::Circle cir(2.0, { 4.0, 4.0 });

  borshchev::Matrix matrix;
  matrix.add(std::make_shared<borshchev::Rectangle>(rec), 0);
  matrix.add(std::make_shared<borshchev::Circle>(cir), 1);

  BOOST_CHECK_EQUAL(matrix[0][0]->getArea(), rec.getArea());
  BOOST_CHECK_EQUAL(matrix[1][0]->getArea(), cir.getArea());
}

BOOST_AUTO_TEST_CASE(matrix_data_after_adding_test)
{
  borshchev::Rectangle rec(4.0, 2.0, { 10.0, 3.0 });
  borshchev::Circle cir(2.0, { 4.0, 4.0 });

  borshchev::Matrix matrix;
  matrix.add(std::make_shared<borshchev::Rectangle>(rec), 0);
  matrix.add(std::make_shared<borshchev::Circle>(cir), 1);
  const double rowsBefore = matrix.getRows();
  const double numberBefore = matrix.getNumberOfFigures();
  borshchev::CompositeShape::shape_ptr temp = std::make_shared<borshchev::Circle>(cir);

  borshchev::CompositeShape composite(std::make_shared<borshchev::Rectangle>(rec));
  composite.add(temp);
  matrix.add(std::make_shared<borshchev::CompositeShape>(composite), 2);

  BOOST_CHECK_EQUAL(rowsBefore, matrix.getRows() - 1);
  BOOST_CHECK_EQUAL(numberBefore, matrix.getNumberOfFigures() - 1);
}

BOOST_AUTO_TEST_CASE(matrix_incorrect_parameters_test)
{
  borshchev::Rectangle rec(4.0, 2.0, { 10.0, 3.0 });
  borshchev::Circle cir(2.0, { 4.0, 4.0 });

  borshchev::Matrix matrix;
  matrix.add(std::make_shared<borshchev::Rectangle>(rec), 0);
  matrix.add(std::make_shared<borshchev::Circle>(cir), 1);

  BOOST_CHECK_THROW(matrix[0][1], std::invalid_argument);
  BOOST_CHECK_THROW(matrix.add(nullptr, 0), std::invalid_argument);
  BOOST_CHECK_THROW(matrix.add(std::make_shared<borshchev::Rectangle>(rec), 3), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
