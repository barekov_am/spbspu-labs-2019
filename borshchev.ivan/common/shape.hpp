#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <memory>
#include "base-types.hpp"

namespace borshchev
{
  class Shape
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    using shapes_array = std::unique_ptr<shape_ptr[]>;

    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(double x, double y) = 0;
    virtual void move(const point_t &pos) = 0;
    virtual void scale(double scalingCoefficient) = 0;
    virtual void rotate(double angle) = 0;

    int intersectsWith(const shape_ptr other) const;
    virtual point_t getPos() const = 0;
  };
}

#endif
