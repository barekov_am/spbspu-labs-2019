#define _USE_MATH_DEFINES
#include "rectangle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>

borshchev::Rectangle::Rectangle(double width, double height, const point_t &pos) :
  rect_(rectangle_t{ width, height, pos }),
  angle_(0)
{
  if ((width < 0.0) || (height < 0.0))
  {
    throw std::invalid_argument("Incorrect value, WIDTH and HEIGHT should be positive");
  }
}

double borshchev::Rectangle::getArea() const
{
  return rect_.width * rect_.height;
}

borshchev::rectangle_t borshchev::Rectangle::getFrameRect() const
{
  const double sinus = sin(angle_ * M_PI / 180);
  const double cosinus = cos(angle_ * M_PI / 180);
  const double width = rect_.height * abs(sinus) + rect_.width * abs(cosinus);
  const double height = rect_.height * abs(cosinus) + rect_.width * abs(sinus);

  return { height, width, rect_.pos };
}

void borshchev::Rectangle::move(double x, double y)
{
  rect_.pos.x += x;
  rect_.pos.y += y;
}

void borshchev::Rectangle::move(const point_t &pos)
{
  rect_.pos.x = pos.x;
  rect_.pos.y = pos.y;
}

void borshchev::Rectangle::scale(double scalingCoefficient)
{
  if (scalingCoefficient < 0)
  {
    throw std::invalid_argument("Incorrect value, scaling COEFFICIENT should be positive");
  }

  rect_.width *= scalingCoefficient;
  rect_.height *= scalingCoefficient;
}

void borshchev::Rectangle::rotate(double angle)
{
  angle_ += angle;
  angle_ = (angle_ < 360) ? angle_ : angle_ - 360;
  angle_ = (angle_ > 0) ? angle_ : angle_ + 360;
}

borshchev::point_t borshchev::Rectangle::getPos() const
{
  return rect_.pos;
}
