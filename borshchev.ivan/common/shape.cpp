#include "shape.hpp"
#include <cmath>

int borshchev::Shape::intersectsWith(const shape_ptr other) const
{
  rectangle_t primaryFrame = getFrameRect();
  rectangle_t secondaryFrame = other->getFrameRect();

  if (fabs(primaryFrame.pos.x - secondaryFrame.pos.x) > ((primaryFrame.width + secondaryFrame.width) / 2))
  {
    return 0;
  }

  if (fabs(primaryFrame.pos.y - secondaryFrame.pos.y) > ((primaryFrame.height + secondaryFrame.height) / 2))
  {
    return 0;
  }
  return 1;
}
