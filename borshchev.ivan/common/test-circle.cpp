#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(circle_test_A4)

BOOST_AUTO_TEST_CASE(circle_area_test)
{
  borshchev::Circle testCircle(3.0, { 10.0, 10.0 });
  const double areaBeforeMoving = testCircle.getArea();
  const borshchev::rectangle_t frameBeforeMoving = testCircle.getFrameRect();

  testCircle.move({ 0, 0 });

  double areaAfterMoving = testCircle.getArea();
  borshchev::rectangle_t frameAfterMoving = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, frameAfterMoving.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, frameAfterMoving.height, ACCURACY);

  testCircle.move(4, 5);

  areaAfterMoving = testCircle.getArea();
  frameAfterMoving = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, frameAfterMoving.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, frameAfterMoving.height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(circle_scaling_test)
{
  borshchev::Circle testCircle(3.0, { 10.0, 10.0 });
  const double areaBeforeScaling = testCircle.getArea();

  const double coeficient = 2;
  testCircle.scale(coeficient);

  const double areaAfterScaling = testCircle.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScaling * coeficient * coeficient, areaAfterScaling, ACCURACY);
}

BOOST_AUTO_TEST_CASE(circle_construction_test)
{
  BOOST_CHECK_THROW(borshchev::Circle circle(-3.0, { 3.0, 2.0 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circle_scale_test)
{
  borshchev::Circle testCircle(3.0, { 10.0, 10.0 });
  BOOST_CHECK_THROW(testCircle.scale(-1.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
