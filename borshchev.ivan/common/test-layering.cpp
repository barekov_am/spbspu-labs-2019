#include <boost/test/auto_unit_test.hpp>
#include "layering.hpp"
#include "matrix.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(layering_test_A4)

BOOST_AUTO_TEST_CASE(layering_correct_splitting_test)
{
  borshchev::Rectangle rec1(4.0, 2.0, { 7.0, 6.0 });
  borshchev::Rectangle rec2(4.0, 2.0, { 10.0, 3.0 });
  borshchev::Circle cir1(2.0, { 4.0, 4.0 });
  borshchev::Circle cir2(2.0, { 10.0, 8.0 });

  borshchev::CompositeShape composite;
  composite.add(std::make_shared<borshchev::Circle>(cir1));
  composite.add(std::make_shared<borshchev::Rectangle>(rec1));
  composite.add(std::make_shared<borshchev::Rectangle>(rec2));
  composite.add(std::make_shared<borshchev::Circle>(cir2));

  borshchev::Matrix matrix = borshchev::split(composite);

  BOOST_CHECK_EQUAL(matrix.getRows(), 2);
  BOOST_CHECK_EQUAL(matrix[0].getSize(), 3);
  BOOST_CHECK_EQUAL(matrix[1].getSize(), 1);
  BOOST_CHECK_EQUAL(matrix[0][0]->getArea(), cir1.getArea());
  BOOST_CHECK_EQUAL(matrix[1][0]->getArea(), rec1.getArea());
  BOOST_CHECK_EQUAL(matrix.getNumberOfFigures(), 4);
}

BOOST_AUTO_TEST_SUITE_END()
