#include "layering.hpp"
#include "shape.hpp"

borshchev::Matrix borshchev::split(const CompositeShape &listOfShapes)
{
  borshchev::Matrix matrix;
  matrix.add(listOfShapes[0], 0);

  for (int i = 1; i < listOfShapes.getShapeCount(); i++)
  {
    bool nextRow = true;

    for (int row = 0; row < matrix.getRows(); row++)
    {
      int counter = 0;
      for (int column = 0; column < matrix[row].getSize(); column++)
      {
        counter += matrix[row][column]->intersectsWith(listOfShapes[i]);
      }

      if (counter == 0)
      {
        matrix.add(listOfShapes[i], row);
        nextRow = false;
        break;
      }
    }

    if (nextRow)
    {
      matrix.add(listOfShapes[i], matrix.getRows());
    }
  }

  return matrix;
}
