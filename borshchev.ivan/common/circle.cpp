#define _USE_MATH_DEFINES
#include "circle.hpp"
#include <cassert>
#include <iostream>
#include <cmath>

borshchev::Circle::Circle(double radius, const point_t &pos) :
  radius_(radius),
  pos_(pos)
{
  if (radius < 0.0)
  {
    throw std::invalid_argument("Incorrect value, RADIUS should be positive");
  }
}

double borshchev::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

borshchev::rectangle_t borshchev::Circle::getFrameRect() const
{
  return rectangle_t{ 2 * radius_, 2 * radius_, pos_ };
}

void borshchev::Circle::move(double x, double y)
{
  pos_.x += x;
  pos_.y += y;
}

void borshchev::Circle::move(const point_t &pos)
{
  pos_.x = pos.x;
  pos_.y = pos.y;
}

void borshchev::Circle::scale(double scalingCoefficient)
{
  if (scalingCoefficient < 0)
  {
    throw std::invalid_argument("Incorrect value, scaling COEFFICIENT should be positive");
  }

  radius_ *= scalingCoefficient;
}

void borshchev::Circle::rotate(double /*angle*/)
{
}

borshchev::point_t borshchev::Circle::getPos() const
{
  return pos_;
}
