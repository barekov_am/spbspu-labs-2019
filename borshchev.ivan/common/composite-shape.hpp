#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include "shape.hpp"

namespace borshchev
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();

    CompositeShape(const CompositeShape&);
    CompositeShape(CompositeShape&&);
    CompositeShape(const shape_ptr&);

    CompositeShape& operator = (const CompositeShape&);
    CompositeShape& operator = (CompositeShape&&);
    shape_ptr operator [] (int i) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double x, double y) override;
    void move(const point_t &pos) override;
    void scale(double scalingCoefficient) override;

    void add(shape_ptr Shape);
    void remove(int index);
    void remove(shape_ptr);

    int getSize() const;
    void rotate(double angle) override;
    void displayParameters() const;
    point_t getPos() const override;
    int getShapeCount() const;

  private:
    int count_;
    double angle_;
    shapes_array list_;
  };
}

#endif 
