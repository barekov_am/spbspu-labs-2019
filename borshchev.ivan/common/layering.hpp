#ifndef LAYERING_HPP
#define LAYERING_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace borshchev
{
  Matrix split(const CompositeShape &listOfShapes);
}

#endif
