#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include <utility>

using shape_ptr = std::shared_ptr<borshchev::Shape>;

const double errorValue = 1e-10;

BOOST_AUTO_TEST_SUITE(compositeShape_test_A4)

BOOST_AUTO_TEST_CASE(compositeShape_area_after_moving_test)
{

  borshchev::Circle Circle(3.0, { 10.0, 10.0 });
  shape_ptr testCircle = std::make_shared<borshchev::Circle>(Circle);
  borshchev::Rectangle Rect(3.0, 6.0, { 10.0, 10.0 });
  shape_ptr testRect = std::make_shared<borshchev::Rectangle>(Rect);
  borshchev::CompositeShape testCompShape;
  testCompShape.add(testRect);
  testCompShape.add(testCircle);

  const double areaBeforeMoving = testCompShape.getArea();
  const borshchev::rectangle_t frameBeforeMoving = testCompShape.getFrameRect();

  testCompShape.move({ 0, 0 });
  double areaAfterMoving = testCompShape.getArea();
  borshchev::rectangle_t frameAfterMoving = testCompShape.getFrameRect();

  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, errorValue);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, frameAfterMoving.width, errorValue);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, frameAfterMoving.height, errorValue);

  testCompShape.move(4, 5);
  areaAfterMoving = testCompShape.getArea();
  frameAfterMoving = testCompShape.getFrameRect();

  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, errorValue);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, frameAfterMoving.width, errorValue);
  BOOST_CHECK_CLOSE(frameBeforeMoving.height, frameAfterMoving.height, errorValue);
}

BOOST_AUTO_TEST_CASE(compositeShape_area_test)
{
  borshchev::Circle Circle(3.0, { 10.0, 10.0 });
  borshchev::Rectangle Rect(3.0, 6.0, { 10.0, 10.0 });

  shape_ptr testCircle = std::make_shared<borshchev::Circle>(Circle);
  shape_ptr testRect = std::make_shared<borshchev::Rectangle>(Rect);

  const double allArea = testCircle->getArea() + testRect->getArea();

  borshchev::CompositeShape testCompShape;
  testCompShape.add(testRect);
  testCompShape.add(testCircle);

  BOOST_CHECK_CLOSE(allArea, testCompShape.getArea(), errorValue);
}

BOOST_AUTO_TEST_CASE(compositeShape_scaling_test)
{
  borshchev::Circle Circle(3.0, { 10.0, 10.0 });
  borshchev::Rectangle Rect(3.0, 6.0, { 10.0, 10.0 });

  shape_ptr testCircle = std::make_shared<borshchev::Circle>(Circle);
  shape_ptr testRect = std::make_shared<borshchev::Rectangle>(Rect);

  borshchev::CompositeShape testCompShape;
  testCompShape.add(testRect);
  testCompShape.add(testCircle);
  const double areaBeforeScaling = testCompShape.getArea();

  const double coefficient = 2;
  testCompShape.scale(coefficient);

  const double areaAfterScaling = testCompShape.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScaling * coefficient * coefficient, areaAfterScaling, errorValue);
}

BOOST_AUTO_TEST_CASE(compositeShape_scale_test)
{
  borshchev::Circle Circle(3.0, { 10.0, 10.0 });
  shape_ptr testCircle = std::make_shared<borshchev::Circle>(Circle);
  borshchev::CompositeShape testCompShape;
  testCompShape.add(testCircle);
  BOOST_CHECK_THROW(testCompShape.scale(-10), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShape_nullptr_construction_test)
{
  borshchev::CompositeShape testCompShape;
  BOOST_CHECK_THROW(borshchev::CompositeShape(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShape_index_out_of_bounds_test)
{
  borshchev::CompositeShape testCompShape;
  BOOST_CHECK_THROW(testCompShape[-1], std::out_of_range);
  BOOST_CHECK_THROW(testCompShape[100], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeShape_deletion_test)
{
  borshchev::Circle Circle(3.0, { 10.0, 10.0 });
  borshchev::Rectangle Rect(3.0, 6.0, { 10.0, 10.0 });
  shape_ptr testCircle = std::make_shared<borshchev::Circle>(Circle);
  shape_ptr testRect = std::make_shared<borshchev::Rectangle>(Rect);

  const double areaTestRect = testRect->getArea();
  const borshchev::rectangle_t frameTestRect = testRect->getFrameRect();

  borshchev::CompositeShape testCompShape;
  testCompShape.add(testCircle);
  testCompShape.add(testRect);

  testCompShape.remove(0);

  borshchev::rectangle_t frameAfterDelete = testCompShape.getFrameRect();
  double areaAfterDelete = testCompShape.getArea();

  BOOST_CHECK_CLOSE(frameTestRect.width, frameAfterDelete.width, errorValue);
  BOOST_CHECK_CLOSE(frameTestRect.height, frameAfterDelete.height, errorValue);
  BOOST_CHECK_CLOSE(areaTestRect, areaAfterDelete, errorValue);
}

BOOST_AUTO_TEST_CASE(compositeShape_removing_test)
{
  borshchev::CompositeShape testCompShape;
  BOOST_CHECK_THROW(testCompShape.remove(100), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(compositeShape_area_after_rotating_test)
{
  borshchev::Circle Circle1(1, { 4, 7 });
  shape_ptr testCircle = std::make_shared<borshchev::Circle>(Circle1);
  borshchev::Rectangle Rectangle2(2.1, 1, { 5, 6 });
  shape_ptr testRectangle = std::make_shared<borshchev::Rectangle>(Rectangle2);
  borshchev::CompositeShape testComposite;
  testComposite.add(testCircle);
  testComposite.add(testRectangle);
  double areaBefore = testComposite.getArea();

  double angle = 48.2;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), errorValue);
  areaBefore = testComposite.getArea();

  angle = -30.88;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), errorValue);
  areaBefore = testComposite.getArea();

  angle = 765.2;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), errorValue);
  areaBefore = testComposite.getArea();

  angle = -987.3;
  testComposite.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, testComposite.getArea(), errorValue);
}

BOOST_AUTO_TEST_SUITE_END()
