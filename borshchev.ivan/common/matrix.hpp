#ifndef MATRIX_HPP
#define MATRIX_HPP
#include <memory>
#include "shape.hpp"

namespace borshchev
{
  class Matrix
  {
  public:
    class Layer
    {
    public:
      Layer(Shape::shape_ptr *arr, int size);
      Shape::shape_ptr operator [](int index) const;
      int getSize() const;

    private:
      int size_;
      Shape::shapes_array figures_;
    };

    Matrix();
    Matrix(const Matrix &other);
    Matrix(Matrix &&other);

    ~Matrix() = default;

    Matrix &operator =(const Matrix &other);
    Matrix &operator =(Matrix &&other);
    Layer operator [](int row) const;

    void add(Shape::shape_ptr shape, int row);
    int getRows() const;
    int getNumberOfFigures() const;
    void printInfo() const;

  private:
    int rows_;
    std::unique_ptr<int[]> columns_;
    Shape::shapes_array figures_;
    int numberOfFigures;
  };
}

#endif
