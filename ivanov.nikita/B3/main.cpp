#include <iostream>
#include <sstream>
#include <string>

void task1();
void task2();

int main(int argc, char *argv[])
{
  if (argc==1)
  {
    std::cerr << "Invalid parameters count\n";
    return 1;
  }

  try
  {
    switch (std::stoi(argv[1]))
    {
      case 1:
        task1();
        break;
      case 2:
        task2();
        break;
      default:
        std::cerr << "Invalid task number\n";
        return 1;
    }
  }
    catch (const std::exception &exception)
  {
    std::cerr << exception.what() << '\n';
    return 1;
  }
  return 0;
}
