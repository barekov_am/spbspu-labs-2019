#ifndef FACTORIALCONTAINER_HPP
#define FACTORIALCONTAINER_HPP

#include <iterator>

class FactorialContainer
{
public:
  class Iterator;
  FactorialContainer() = default;
  Iterator begin();
  Iterator end();
};

class FactorialContainer::Iterator : public std::iterator<std::bidirectional_iterator_tag, size_t>
{
public:
  Iterator (size_t index, unsigned long long int value);
  Iterator(size_t index);
  Iterator(const Iterator &other) = default;

  const unsigned long long int *operator->() const;
  const unsigned long long int &operator*() const;

  Iterator &operator=(const Iterator &object) = default;
  Iterator &operator++();
  Iterator operator++(int);
  Iterator &operator--();
  Iterator operator--(int);

  bool operator!=(const Iterator &other) const;
  bool operator==(const Iterator &other) const;

private:
  unsigned long long calculate(size_t index) const;

  size_t index_;
  unsigned long long value_;
};

#endif //FACTORIALCONTAINER_HPP
