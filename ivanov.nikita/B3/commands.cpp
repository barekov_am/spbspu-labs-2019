#include <iostream>
#include <istream>
#include <algorithm>
#include <regex>

#include "commands.hpp"


void Commands::add(PhoneBook &phoneBook, std::istream &inputStream)
{
  auto parse = parseRecord(inputStream);
  if (parse.first)
  {
    phoneBook.add(parse.second);
  }
}

void Commands::store(const std::string &bookmark, const std::string &newBookmark, PhoneBook &phoneBook)
{
  phoneBook.newBookMark(bookmark, newBookmark);
}

void Commands::insert(const std::string &bookmark, PhoneBook &phoneBook, bool after, std::istream &inputStream)
{
  auto parse = parseRecord(inputStream);
  if (parse.first)
  {
    phoneBook.insert(bookmark, parse.second, after);
  }
}

void Commands::show(const std::string &bookmark, PhoneBook &phoneBook)
{
  phoneBook.show(bookmark);
}

void Commands::remove(const std::string &bookmark, PhoneBook &phoneBook)
{
  phoneBook.remove(bookmark);
}

void Commands::move(const std::string &bookmark, PhoneBook &phoneBook, std::istream &inputStream)
{
  std::string step;
  inputStream >> step >> std::ws;
  if (step == "first")
  {
    phoneBook.toFirst(bookmark);
  } else if (step == "last")
  {
    phoneBook.toLast(bookmark);
  } else
  {
    try
    {
      int n = std::stoi(step);
      phoneBook.move(bookmark, n);
    }
    catch (const std::exception &exception)
    {
      std::cout << "<INVALID STEP>\n";
      return;
    }
  }
}

std::pair<bool, PhoneBook::phoneRecord_t> Commands::parseRecord(std::istream &inputStream)
{
  std::string number = parseNumber(inputStream);
  if (!number.empty())
  {
    std::string name = parseName(inputStream);
    return std::make_pair(!(number.empty() || name.empty()), PhoneBook::phoneRecord_t{name, number});
  } else
  {
    return std::make_pair(false, PhoneBook::phoneRecord_t{"", number});
  }
}

std::string Commands::parseName(std::istream &inputStream)
{
  std::string name;
  std::getline(inputStream, name);

  if (name.empty())
  {
    return name;
  }

  if ((name.front() != '\"') || (name.back() != '\"'))
  {
    std::cout << "<INVALID COMMAND>\n";
    name.clear();
    return name;
  }

  auto iter = name.begin();
  while (iter != name.end())
  {
    if ((*iter == '\\') && ((*(iter + 1) == '\\') || (*(iter + 1) == '"')))
    {
      iter = name.erase(iter) + 1;
    } else if (*iter == '"')
    {
      iter = name.erase(iter);
    } else
    {
      ++iter;
    }
  }

  return name;
}

std::string Commands::parseNumber(std::istream &inputStream)
{
  std::string number;
  inputStream >> number >> std::ws;

  if (!std::regex_match(number, std::regex("[0-9]*")))
  {
    std::cout << "<INVALID COMMAND>\n";
    number.clear();
  }
  return number;
}
