#include <istream>
#include <iostream>
#include <algorithm>

#include "phoneBook.hpp"


const std::string currentBookMark = "current";

PhoneBook::PhoneBook()
{
  map_[currentBookMark] = list_.end();
}

void PhoneBook::add(const PhoneBook::phoneRecord_t &record)
{
  {
    list_.push_back(record);
    if (list_.size() == 1)
    {
      map_[currentBookMark] = list_.begin();
    }
  }
}

bool PhoneBook::checkBookMark(const std::string &bookMark)
{
  return map_.find(bookMark) != map_.end();
}

void PhoneBook::newBookMark(const std::string &prevBookMark, const std::string &newBookMark)
{
  auto iter = getIterator(prevBookMark);
  auto result = map_.emplace(newBookMark, iter->second);
  if (!result.second)
  {
    std::cout << "Already exists\n";
  }
}

void PhoneBook::remove(const std::string &bookMark)
{
  auto iter = getIterator(bookMark);
  if (iter->second != list_.end())
  {
    auto removeElem = iter->second;
    auto iteratorMap = map_.begin();
    for (size_t i = 0; i < map_.size(); i++)
    {
      if (iteratorMap->second == removeElem)
      {
        if ((list_.size() > 1) && (removeElem == std::prev(list_.end())))
        {
          iteratorMap->second = std::prev(removeElem);
        } else
        {
          iteratorMap->second = std::next(removeElem);
        }
      }
      ++iteratorMap;
    }
    list_.erase(removeElem);
  }
}

PhoneBook::mapIterator PhoneBook::getIterator(const std::string &bookMark)
{
  auto iter = map_.find(bookMark);
  if (iter != map_.end())
  {
    return iter;
  }
  throw std::invalid_argument("<INVALID BOOKMARK>");
}

void PhoneBook::insert(const std::string &bookMark, const phoneRecord_t &record, bool after)
{
  auto iterator = getIterator(bookMark);
  insert(iterator, record, after);
}

void PhoneBook::insert(PhoneBook::mapIterator &iterator, const PhoneBook::phoneRecord_t &record, bool after)
{
  if (list_.empty())
  {
    add(record);
    return;
  }

  if (!after)
  {
    list_.insert(iterator->second, record);
  } else
  {
    list_.insert(std::next(iterator->second), record);
  }
}

void PhoneBook::toFirst(const std::string &bookMark)
{
  auto iter = getIterator(bookMark);
  iter->second = list_.begin();
}

void PhoneBook::toLast(const std::string &bookMark)
{
  auto iter = getIterator(bookMark);
  iter->second = std::prev(list_.end());
}

void PhoneBook::move(const std::string &bookMark, int n)
{
  std::advance(getIterator(bookMark)->second, n);
}

void PhoneBook::show(const std::string &bookMark)
{
  auto iter = getIterator(bookMark);
  if (iter->second == list_.end())
  {
    std::cout << "<EMPTY>\n";
    return;
  }
  std::cout << iter->second->number << " " << iter->second->name << "\n";
}

void PhoneBook::next(const std::string &bookMark)
{
  move(bookMark, 1);
}

void PhoneBook::prev(const std::string &bookMark)
{
  move(bookMark, -1);
}

void PhoneBook::update(const std::string &bookMark, const phoneRecord_t &record)
{
  *(getIterator(bookMark)->second) = record;
}

size_t PhoneBook::getSize()
{
  return list_.size();
}
