#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <algorithm>
#include <istream>
#include <iostream>
#include <regex>

#include "phoneBook.hpp"

class Commands
{
public:
  void add(PhoneBook &phoneBook, std::istream &input);
  void insert(const std::string &bookmark, PhoneBook &phoneBook, bool after, std::istream &input);
  void store(const std::string &bookmark, const std::string &newBookmark, PhoneBook &phoneBook);
  void remove(const std::string &bookmark, PhoneBook &phoneBook);
  void move(const std::string &bookmark, PhoneBook &phoneBook, std::istream &inputStream);
  void show(const std::string &bookmark, PhoneBook &phoneBook);

private:
  std::pair<bool, PhoneBook::phoneRecord_t> parseRecord(std::istream &inputStream);
  std::string parseName(std::istream &inputStream);
  std::string parseNumber(std::istream &input);
};

#endif //COMMANDS_HPP
