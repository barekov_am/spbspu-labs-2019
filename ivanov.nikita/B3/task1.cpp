#include <iostream>
#include <sstream>
#include "phoneBook.hpp"
#include "commands.hpp"

std::pair<bool, std::string> parseBookMark(std::istream &inputStream)
{
  std::string bookMark;
  inputStream >> bookMark;

  bool error = false;
  for (auto i : bookMark)
  {
    if (!(std::isalnum(i) || (i == '-')))
    {
      std::cout << "<INVALID BOOKMARK>";
      error = true;
    }
  }
  return std::make_pair(!error, bookMark);
}

std::pair<bool, std::string> analyzeBookMark(std::istream &inputStream, PhoneBook &phoneBook)
{
  auto afterParseBookMark = parseBookMark(inputStream);

  if(afterParseBookMark.first)
  {
    if (!phoneBook.checkBookMark(afterParseBookMark.second))
    {
      std::cout << "<INVALID BOOKMARK>\n";
      afterParseBookMark.first = false;
    }
  }
  return afterParseBookMark;
}

void task1()
{
  PhoneBook phoneBook;
  std::string input;

  while (std::getline(std::cin, input))
  {
    Commands commands;
    std::istringstream inputStream(input);
    std::string commandStr;
    inputStream >> commandStr;

    if (commandStr == "add")
    {
      commands.add(phoneBook, inputStream);
    } else if (commandStr == "insert")
    {
      std::string directionStr;
      inputStream >> directionStr;

      if (directionStr != "after" && directionStr != "before")
      {
        std::cout << "<INVALID COMMAND>\n";
        continue;
      }

      auto afterAnalyzeBookMark = analyzeBookMark(inputStream, phoneBook);
      if (!afterAnalyzeBookMark.first)
      {
        continue;
      }

      commands.insert(afterAnalyzeBookMark.second, phoneBook, directionStr == "after", inputStream);
    } else if (commandStr == "delete")
    {
      auto afterAnalyzeBookMark = analyzeBookMark(inputStream, phoneBook);
      if (!afterAnalyzeBookMark.first)
      {
        continue;
      }

      commands.remove(afterAnalyzeBookMark.second, phoneBook);
    } else if (commandStr == "store")
    {
      auto afterAnalyzeOldBookMark = analyzeBookMark(inputStream, phoneBook);
      if (!afterAnalyzeOldBookMark.first)
      {
        continue;
      }

      auto afterParseNewBookMark = parseBookMark(inputStream);
      if (!afterParseNewBookMark.first)
      {
        continue;
      }

      commands.store(afterAnalyzeOldBookMark.second, afterParseNewBookMark.second, phoneBook);
    } else if (commandStr == "move")
    {
      auto afterAnalyzeBookMark = analyzeBookMark(inputStream, phoneBook);
      if (!afterAnalyzeBookMark.first)
      {
        continue;
      }

      commands.move(afterAnalyzeBookMark.second, phoneBook, inputStream);
    } else if (commandStr == "show")
    {
      auto afterAnalyzeBookMark = analyzeBookMark(inputStream, phoneBook);
      if (!afterAnalyzeBookMark.first)
      {
        continue;
      }

      commands.show(afterAnalyzeBookMark.second, phoneBook);
    } else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios::failure("Fail reading");
  }
}
