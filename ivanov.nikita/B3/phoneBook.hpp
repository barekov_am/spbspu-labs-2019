#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <list>
#include <map>
#include <string>
#include <stdexcept>
#include <iostream>

class PhoneBook
{
public:
  struct phoneRecord_t
    {
      std::string name;
      std::string number;
    };
  PhoneBook();
  void add(const phoneRecord_t &record);
  bool checkBookMark(const std::string &bookMark);
  void newBookMark(const std::string &prevBookMark, const std::string &newBookMark);
  void remove(const std::string &bookMark);
  void insert(const std::string &bookMark, const phoneRecord_t &record, bool after);
  void toFirst(const std::string &bookMark);
  void toLast(const std::string &bookMark);
  void move(const std::string &bookMark, int n);
  void show(const std::string &bookMark);
  void next(const std::string &bookMark);
  void prev(const std::string &bookMark);
  void update(const std::string &bookMark, const phoneRecord_t &record);
  size_t getSize();

private:
  std::map<std::string, std::list<phoneRecord_t>::iterator> map_;
  std::list<phoneRecord_t> list_;

  using mapIterator = std::map<std::string, std::list<phoneRecord_t>::iterator>::iterator;
  mapIterator getIterator(const std::string &bookMark);
  void insert(mapIterator &iterator, const phoneRecord_t &record, bool after = true);
};

#endif //PHONEBOOK_HPP
