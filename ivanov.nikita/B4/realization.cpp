#include <sstream>
#include <algorithm>

#include "realization.hpp"

const int NUMBER_OF_KEYS = 2;

void realization::filling(std::vector<DataStruct> &vector, const std::pair<int, int> &keyRange)
{
  std::string inputLine;
  while (std::getline(std::cin, inputLine))
  {
    DataStruct dataStruct;
    auto isFirstIteration = true;
    std::istringstream inputStream(inputLine);

    int tmpKey = 0;
    char delimiter = 0;
    for (auto i = 0; i < NUMBER_OF_KEYS; i++)
    {
      inputStream >> tmpKey;

      inputStream >> delimiter;

      if (tmpKey > keyRange.second || tmpKey < keyRange.first)
      {
        throw std::invalid_argument("Key range must be in: [-5, 5]");
      }

      if (delimiter != ',')
      {
        throw std::invalid_argument("Invalid data input");
      }

      isFirstIteration ? dataStruct.key1 = tmpKey : dataStruct.key2 = tmpKey;

      isFirstIteration = false;
    }
    getline(inputStream, dataStruct.str);

    if (dataStruct.str.empty())
    {
      throw std::invalid_argument("Invalid data input");
    }

    while ((dataStruct.str.at(0) == ' ') || (dataStruct.str.at(0) == '\t'))
    {
      dataStruct.str.erase(0, 1);
    }
    vector.push_back(dataStruct);
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::invalid_argument("Reading error\n");
  }
}

void realization::sort(std::vector<DataStruct> &vector)
{
  std::sort(vector.begin(), vector.end(), needSorting);
}

void realization::print(std::vector<DataStruct> &vector)
{
  std::for_each(vector.begin(), vector.end(),
    [&](auto element) { std::cout << element.key1 << "," << element.key2 << "," << element.str << "\n"; });
}

bool realization::needSorting(const DataStruct &struct1, const DataStruct &struct2)
{
  if (struct1.key1 > struct2.key1)
  {
    return false;
  }

  if (struct1.key1 == struct2.key1)
  {
    if (struct1.key2 > struct2.key2)
    {
      return false;
    }

    if (struct1.key2 == struct2.key2)
    {
      if (struct1.str.size() > struct2.str.size())
      {
        return false;
      }
    }
  }
  return true;
}
