#include "realization.hpp"

const std::pair<int, int> RANGE_OF_KEY(-5, 5);

int main()
{
  try
  {
    std::vector<realization::DataStruct> vector;

    filling(vector, RANGE_OF_KEY);
    sort(vector);
    print(vector);
  }
  catch (std::invalid_argument &exeption)
  {
    std::cerr << exeption.what() << "\n";
    return 1;
  }
  return 0;
}
