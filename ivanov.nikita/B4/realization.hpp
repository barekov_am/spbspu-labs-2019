#ifndef ALGORITHMS_HPP
#define ALGORITHMS_HPP

#include <iostream>
#include <vector>
#include <utility>
#include <string>

namespace realization
{
    struct DataStruct
    {
        int key1;
        int key2;
        std::string str;
    };

    void filling(std::vector<DataStruct> &vector, const std::pair<int, int> &keyRange);

    void sort(std::vector<DataStruct> &vector);

    void print(std::vector<DataStruct> &vector);

    bool needSorting(const DataStruct &struct1, const DataStruct &struct2);
}
#endif
