#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
  ivanov::Rectangle rectangle({5, 7, {5, 4}});
  ivanov::Shape *figure = &rectangle;
  std::cout << "\nRECTANGLE:\n";
  figure->draw();
  std::cout << "Moving rectangle Ox -3 and Oy 5\n";
  figure->move(-3, 5);
  figure->draw();
  std::cout << "Moving rectangle to the point (9, 4)\n";
  figure->move({9, 4});
  figure->draw();
  std::cout << "Change the scale of the rectangle with coefficient 1.2\n";
  figure->scale(1.2);
  figure->draw();
  std::cout << "\n";

  ivanov::Circle circle(7, {1, 5});
  figure = &circle;
  std::cout << "\nCIRCLE:\n";
  figure->draw();
  std::cout << "Moving circle Ox +4 and Oy +3\n";
  figure->move(4, 3);
  figure->draw();
  std::cout << "Moving circle to the point (-10, 7)\n";
  figure->move({-10, 7});
  figure->draw();
  std::cout << "Change the scale of the circle with coefficient 2.2\n";
  figure->scale(2.2);
  figure->draw();
  std::cout << "\n";

  ivanov::Triangle triangle({3, 5}, {4, 10}, {6, 5});
  figure = &triangle;
  std::cout << "\nTRIANGLE:\n";
  figure->draw();
  std::cout << "Moving triangle Ox +4 and Oy +3\n";
  figure->move(4, 3);
  figure->draw();
  std::cout << "Moving triangle to the point (-5, 2)\n";
  figure->move({-5, 2});
  figure->draw();
  std::cout << "Change the scale of the triangle with coefficient 1.3\n";
  figure->scale(1.3);
  figure->draw();
  std::cout << "\n";

  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(rectangle);
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(circle);
  ivanov::shapePtr trianglePtr = std::make_shared<ivanov::Triangle>(triangle);
  ivanov::CompositeShape compositeShape(circlePtr);

  std::cout << "\nCOMPOSITE SHAPE\n";
  compositeShape.draw();
  std::cout << "After adding circle\n";
  compositeShape.addShape(rectanglePtr);
  compositeShape.draw();
  std::cout << "Moving to the point (1, 2)\n";
  compositeShape.move({1, 2});
  compositeShape.draw();
  std::cout << "Scaling with coefficient 3.2\n";
  compositeShape.scale(3.2);
  compositeShape.draw();
  std::cout << "Moving along Ox +7 and Oy -9\n";
  compositeShape.move(3, -2);
  compositeShape.draw();
  std::cout << "After adding triangle\n";
  compositeShape.addShape(trianglePtr);
  compositeShape.draw();
  std::cout << "Deleting the first figure:";
  compositeShape.remove(0);
  compositeShape.draw();
  std::cout << "\n";

  return 0;
}
