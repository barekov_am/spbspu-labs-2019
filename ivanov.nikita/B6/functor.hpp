#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <algorithm>
namespace lab6
{
  class Functor
  {
  public:
    Functor();
    bool empty() const;
    void operator()(int value);

    int max() { return max_; }
    int min() { return min_; }
    double average() { return average_; }
    size_t amountOfPositive() { return quantityPositive_; }
    size_t amountOfNegative() { return quantityNegative_; }
    long long sumOfEven() { return sumOfEven_; }
    long long sumOfOdd() { return sumOfOdd_; }
    bool isEqual() { return isEqual_; }
    void printInfo();

  private:
    int first;
    int quantity;

    int max_;
    int min_;
    double average_;
    size_t quantityPositive_;
    size_t quantityNegative_;
    long long sumOfEven_;
    long long sumOfOdd_;
    bool isEqual_;
    bool isFirst;
  };
}
#endif //FUNCTOR_HPP
