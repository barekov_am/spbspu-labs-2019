#include "functor.hpp"
#include<iostream>

lab6::Functor::Functor():
  first(0),
  quantity(0),
  max_(0),
  min_(0),
  average_(0),
  quantityPositive_(0),
  quantityNegative_(0),
  sumOfEven_(0),
  sumOfOdd_(0),
  isEqual_(false),
  isFirst(true)
{}

bool lab6::Functor::empty() const
{
  return (quantity == 0);
}

 void lab6::Functor::operator()(int value)
{
  ++quantity;

  if (isFirst)
  {
    first = value;
    max_ = value;
    min_ = max_;
    isFirst = false;
  }

  max_ = std::max(max_, value);

  min_ = std::min(min_, value);

  if (value > 0)
  {
    ++quantityPositive_;
  }

  if (value < 0)
  {
    ++quantityNegative_;
  }

  if (value % 2 == 0)
  {
    sumOfEven_ += value;
  }
  else
  {
    sumOfOdd_ += value;
  }

  isEqual_ = (first == value);

  average_ = static_cast<double>(sumOfOdd_ + sumOfEven_)/ quantity;
}

 void lab6::Functor::printInfo()
 {
   std::cout << "Max: " << max() << '\n'
             << "Min: " << min() << '\n'
             << "Mean: " << average() << '\n'
             << "Positive: " << amountOfPositive() << '\n'
             << "Negative: " << amountOfNegative() << '\n'
             << "Odd Sum: " << sumOfOdd() << '\n'
             << "Even Sum: " << sumOfEven() << '\n'
             << "First/Last Equal: ";
   isEqual() ? std::cout << "yes" : std::cout << "no";
   std::cout << '\n';
 }
