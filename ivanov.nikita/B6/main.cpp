#include "functor.hpp"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <list>

int main()
{
  try
  {
    std::list<int> list((std::istream_iterator<int>(std::cin)), std::istream_iterator<int>());
    
    lab6::Functor functor;
    functor = for_each(list.begin(), list.end(), lab6::Functor());

    if ((!std::cin.eof()) && (std::cin.fail()))
    {
      throw std::runtime_error("Can`t read data\n");
    }

    if (functor.empty())
    {
      std::cout << "No Data\n";
      return 0;
    }

    functor.printInfo();
  }
  catch (const std::exception &exception)
  {
    std::cerr << exception.what() << std::endl;
    return 2;
  }

  return 0;
}
