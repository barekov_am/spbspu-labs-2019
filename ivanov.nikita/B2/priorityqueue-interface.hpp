#ifndef PRIORITYQUEUE_INTERFACE_HPP
#define PRIORITYQUEUE_INTERFACE_HPP

#include "priorityqueue.hpp"

template<typename T>
void QueueWithPriority<T>::addToQueue(ElementPriority priority, const T &element)
{
  switch(priority)
  {
    case ElementPriority::HIGH:
      highPriority.push_back(element);
      break;
    case ElementPriority::LOW:
      lowPriority.push_back(element);
      break;
    case ElementPriority::NORMAL:
      normalPriority.push_back(element);
      break;
  }
}

template<typename T>
T QueueWithPriority<T>::getElementFromQueue()
{
  if (empty())
  {
    throw std::out_of_range("Empty queue");
  }

  if (!highPriority.empty())
  {
    T element = highPriority.front();
    highPriority.pop_front();
    return element;
  }

  else if (!normalPriority.empty())
  {
    T element = normalPriority.front();
    normalPriority.pop_front();
    return element;
  }

  else
  {
    T element = lowPriority.front();
    lowPriority.pop_front();
    return element;
  }
}

template<typename T>
void QueueWithPriority<T>::accelerate()
{
  highPriority.splice(highPriority.end(), lowPriority);
}

template<typename T>
bool QueueWithPriority<T>::empty()
{
  return highPriority.empty() && normalPriority.empty() && lowPriority.empty();
}

#endif // PRIORITYQUEUE_INTERFACE_HPP
