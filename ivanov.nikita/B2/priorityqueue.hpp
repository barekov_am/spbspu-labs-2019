#ifndef PRIORITYQUEUE_HPP
#define PRIORITYQUEUE_HPP

#include <list>
#include <iostream>

typedef enum
{
    HIGH,
    NORMAL,
    LOW
} ElementPriority;

template<typename T>
class QueueWithPriority
{
public:
  void addToQueue(ElementPriority prirority, const T &element);
  T getElementFromQueue();
  void accelerate();
  bool empty();

private:
  std::list<T> highPriority;
  std::list<T> normalPriority;
  std::list<T> lowPriority;
};

#endif // PRIORITYQUEUE_HPP
