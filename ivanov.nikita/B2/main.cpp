#include <iostream>
#include <string>
void task1();
void task2();

int main(int argc, char * argv[])
{
  try
  {
    if (argc != 2 )
    {
      std::cerr << "Incorrect number of arguments\n";
      return 1;
    }

    int task = std::stoi(argv[1]);

    switch (task)
    {
      case 1:
      {
        task1();
        break;
      }
      case 2:
      {    
        task2();
        break;
      }
      default:
      {
        std::cerr << "Incorrect task number\n";
        return 1;
      }
    }
  }
  catch (const std::exception& exception)
  {
    std::cerr << exception.what() << "\n";
    return 2;
  }
  return 0;
}
