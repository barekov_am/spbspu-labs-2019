#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>

ivanov::Triangle::Triangle(const point_t &point1, const point_t &point2, const point_t &point3) :
  point1_(point1),
  point2_(point2),
  point3_(point3),
  center_({(point1_.x + point2_.x + point3_.x) / 3, (point1_.y + point2_.y + point3_.y) / 3 })
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("Error. Wrong point(s)");
  }
}

static double findInterval(const ivanov::point_t &point1, const ivanov::point_t &point2)
{
  const ivanov::point_t vector = {(point2.x - point1.x), (point2.y - point1.y)};
  return sqrt(vector.x * vector.x + vector.y * vector.y);
}

double ivanov::Triangle::getArea() const
{
  const double lengthVector1 = findInterval(point1_, point2_);
  const double lengthVector2 = findInterval(point1_, point3_);
  const double lengthVector3 = findInterval(point2_, point3_);
  const double hfPerimeter = (lengthVector1 + lengthVector2 + lengthVector3) / 2;
  return sqrt(hfPerimeter * (hfPerimeter - lengthVector1) * (hfPerimeter - lengthVector2) * (hfPerimeter - lengthVector3));
}

ivanov::rectangle_t ivanov::Triangle::getFrameRect() const
{
  const double minX = std::min({point1_.x, point2_.x, point3_.x});
  const double minY = std::min({point1_.y, point2_.y, point3_.y});
  const double maxX = std::max({ point1_.x, point2_.x, point3_.x });
  const double maxY = std::max({ point1_.y, point2_.y, point3_.y });
  const double height = maxY - minY;
  const double width = maxX - minX;
  const point_t center = {minX + width / 2, minY + height / 2};
  return {height, width, center};
}

void ivanov::Triangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;

  point1_.x += dx;
  point1_.y += dy;

  point2_.x += dx;
  point2_.y += dy;

  point3_.x += dx;
  point3_.y += dy;
}

void ivanov::Triangle::move(const point_t &center)
{
  move((center.x - center_.x), (center.y - center_.y));
}

double scaleOneCoordinate(double point, double center, const double coefficient)
{
  return (center + point * coefficient - coefficient * center);
}

void ivanov::Triangle::draw() const
{
  std::cout << "Width of frame traingle: " << getFrameRect().width << "\n";
  std::cout << "Height of frame traingle: " << getFrameRect().height << "\n";
  std::cout << "Area of traingle: " << getArea() << "\n";
  std::cout << "Center of frame traingle: (" << getFrameRect().pos.x
            << "; " << getFrameRect().pos.y << ")" << "\n";
}

void ivanov::Triangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Ñoefficient must be positive");
  }
  point1_.x = scaleOneCoordinate(point1_.x, center_.x, coefficient);
  point1_.y = scaleOneCoordinate(point1_.y, center_.y, coefficient);
  point2_.x = scaleOneCoordinate(point2_.x, center_.x, coefficient);
  point2_.y = scaleOneCoordinate(point2_.y, center_.y, coefficient);
  point3_.x = scaleOneCoordinate(point3_.x, center_.x, coefficient);
  point3_.y = scaleOneCoordinate(point3_.y, center_.y, coefficient);
}

ivanov::point_t rotateOneCoordinate(ivanov::point_t point, double angle)
{
  double radAngle = M_PI * angle / 180;

  double x = point.x * cos(radAngle) - point.y * sin(radAngle);
  double y = point.y * cos(radAngle) + point.x * sin(radAngle);

  return {x, y};
}

void ivanov::Triangle::rotate(double angle)
{
  angle = (angle < 0) ? (360 + fmod(angle, 360)) : angle;

  point1_ = rotateOneCoordinate(point1_, angle);
  point2_ = rotateOneCoordinate(point2_, angle);
  point3_ = rotateOneCoordinate(point3_, angle);
}
