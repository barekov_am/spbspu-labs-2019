#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "split.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithCrossing)
{
  const unsigned int layersVal = 2;
  const unsigned int columnsVal = 1;

  ivanov::Circle testCircle(2.0, {5.0, 5.0});
  ivanov::Rectangle testRectangle({4.0, 2.0, {5.0, 5.0}});
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);

  ivanov::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.addShape(circlePtr);

  ivanov::Matrix testMatrix = ivanov::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersVal, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsVal, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithoutCrossing)
{
  const unsigned int layersVal = 1;
  const unsigned int columnsVal = 2;

  ivanov::Circle testCircle(2.0, {10.0, 5.0});
  ivanov::Rectangle testRectangle({4.0, 2.0, {5.0, 5.0}});
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);

  ivanov::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.addShape(circlePtr);

  ivanov::Matrix testMatrix = ivanov::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersVal, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsVal, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(checkCopyAndMove)
{
  ivanov::Rectangle testRectangle({ 4.0, 2.0, {5.0, 5.0} });
  ivanov::Circle testCircle(2.0, { 5.0, 5.0 });
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);

  ivanov::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.addShape(circlePtr);

  ivanov::Matrix testMatrix = ivanov::part(testCompositeShape);
  ivanov::Matrix copyMatrix(testMatrix);
  ivanov::Matrix moveMatrix(std::move(testMatrix));

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  ivanov::Rectangle testRectangle({4.0, 2.0, {5.0, 5.0}});
  ivanov::Circle testCircle(2.0, {5.0, 5.0});
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);

  ivanov::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.addShape(circlePtr);

  ivanov::Matrix testMatrix = ivanov::part(testCompositeShape);

  BOOST_CHECK_THROW(testMatrix[5][0], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-5][0], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
