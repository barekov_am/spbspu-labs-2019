#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>
#include "triangle.hpp"

const double tolerance = 0.001;

BOOST_AUTO_TEST_SUITE(triangleTest)

BOOST_AUTO_TEST_CASE(triangleConstantParametersAfterMovingToPoint)
{
  ivanov::Triangle testTriangle({2, 4}, {4, 5}, {6, 4});
  const double testArea = testTriangle.getArea();
  const ivanov::rectangle_t testFrameRect = testTriangle.getFrameRect();
  testTriangle.move({4, 5});
  BOOST_CHECK_CLOSE(testTriangle.getFrameRect().height, testFrameRect.height, tolerance);
  BOOST_CHECK_CLOSE(testTriangle.getFrameRect().width, testFrameRect.width,  tolerance);
  BOOST_CHECK_CLOSE(testTriangle.getArea(), testArea, tolerance);
}

BOOST_AUTO_TEST_CASE(triangleConstantParametersAfterMovingByDistance)
{
  ivanov::Triangle testTriangle({3, 4}, {4, 5}, {6, 4});
  const double testArea = testTriangle.getArea();
  const ivanov::rectangle_t testFrame = testTriangle.getFrameRect();
  testTriangle.move(4, 5);
  BOOST_CHECK_CLOSE(testTriangle.getFrameRect().height, testFrame.height, tolerance);
  BOOST_CHECK_CLOSE(testTriangle.getFrameRect().width, testFrame.width, tolerance);
  BOOST_CHECK_CLOSE(testTriangle.getArea(), testArea, tolerance);
}

BOOST_AUTO_TEST_CASE(boundingTriangleChangeAfterScaling)
{
  ivanov::Triangle triangle({3, 4}, {6, 7}, {1, 5});
  const double areaBeforScaling = triangle.getArea();
  const double testScale = 4;
  triangle.scale(testScale);
  BOOST_CHECK_CLOSE(areaBeforScaling * testScale * testScale, triangle.getArea(), tolerance);
}

BOOST_AUTO_TEST_CASE(triangleInvalidValues)
{
  BOOST_CHECK_THROW(ivanov::Triangle triangle({8, 5}, {8, 6}, {8, 7}), std::invalid_argument);
  BOOST_CHECK_THROW(ivanov::Triangle triangle({6, 2}, {2, 2}, {5, 2}), std::invalid_argument);
  ivanov::Triangle triangle({4, 5}, {1, 4}, {6, 7});
  BOOST_CHECK_THROW(triangle.scale(-5), std::invalid_argument);
  BOOST_CHECK_THROW(triangle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
