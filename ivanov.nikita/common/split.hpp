#ifndef SPLIT_HPP
#define SPLIT_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace ivanov
{
  bool cross(const rectangle_t&, const rectangle_t&);
  Matrix part(const CompositeShape&);
  Matrix part(const dynamicArray&, unsigned int);
}

#endif //SPLIT_HPP
