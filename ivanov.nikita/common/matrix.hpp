#ifndef MATRIX_HPP
#define MATRIX_HPP
#include "shape.hpp"

namespace ivanov
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &copyMatrix);
    Matrix(Matrix &&moveMatrix);
    ~Matrix() = default;
    Matrix& operator =(const Matrix &copyMatrix);
    Matrix& operator =(Matrix &&moveMatrix);
    dynamicArray operator [](unsigned int index) const;
    bool operator ==(const Matrix &copyMatrix) const;
    bool operator !=(const Matrix &copyMatrix) const;
    unsigned int getLines() const;
    unsigned int getColumns() const;
    void add(const shapePtr &copyMatrix, unsigned int line, unsigned int column);
    void printInfo() const;

  private:
    unsigned int lines_;
    unsigned int columns_;
    dynamicArray list_;
  };
}

#endif //MATRIX_HPP
