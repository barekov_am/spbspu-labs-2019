#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include "shape.hpp"

namespace ivanov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(shapePtr &shape);
    CompositeShape(const CompositeShape &copyCompositeShape);
    CompositeShape(CompositeShape &&moveCompositeShape);
    CompositeShape &operator =(const CompositeShape &copyCompositeShape);
    CompositeShape &operator =(CompositeShape &&);
    Shape &operator [](unsigned int) const;
    ~CompositeShape() = default;
    unsigned int getCount() const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(const double dx, const double dy) override;
    void draw() const override;
    void addShape(const shapePtr &shape);
    void remove(unsigned int index);
    void scale(double coefficient) override;
    void rotate(const double) override;
    dynamicArray getList() const;

  private:
    unsigned int count_;
    dynamicArray shapes_;
    double angle_;
  };
}
#endif //COMPOSITE_SHAPE_HPP
