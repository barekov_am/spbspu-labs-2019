#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

const double tolerance = 0.001;

BOOST_AUTO_TEST_SUITE(compositeShapeTest)

BOOST_AUTO_TEST_CASE(constantParametersAfterMovingToDistance)
{
  ivanov::Circle testCircle(4, {1, 2});
  ivanov::Rectangle testRectangle({3, 5, {1, 2}});
  ivanov::Triangle testTriangle({0, 2}, {7, 14}, {13, 5});
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr trianglePtr = std::make_shared<ivanov::Triangle>(testTriangle);
  
  ivanov::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const double testCompositeArea = testCompositeShape.getArea();
  const ivanov::rectangle_t testFrameRect = testCompositeShape.getFrameRect();

  testCompositeShape.move(1.0, 8.3);
  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea, tolerance);
  BOOST_CHECK_CLOSE(testCompositeShape.getFrameRect().width, testFrameRect.width, tolerance);
  BOOST_CHECK_CLOSE(testCompositeShape.getFrameRect().height, testFrameRect.height, tolerance);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterMovingToPoint)
{
  ivanov::Circle testCircle(4, {2, 3});
  ivanov::Rectangle testRectangle({4, 6, {2, 3}});
  ivanov::Triangle testTriangle({0, 2}, {7,15}, {12,5});
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr trianglePtr = std::make_shared<ivanov::Triangle>(testTriangle);
  
  ivanov::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const double testCompositeArea = testCompositeShape.getArea();
  const ivanov::rectangle_t testFrameRect = testCompositeShape.getFrameRect();

  testCompositeShape.move({7, 3});

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea, tolerance);
  BOOST_CHECK_CLOSE(testCompositeShape.getFrameRect().width, testFrameRect.width, tolerance);
  BOOST_CHECK_CLOSE(testCompositeShape.getFrameRect().height, testFrameRect.height, tolerance);
}

BOOST_AUTO_TEST_CASE(FrameRectChangeAreaAfterPositiveScale)
{
  ivanov::Circle testCircle(4, {2, 3});
  ivanov::Rectangle testRectangle({4, 6, {2, 3}});
  ivanov::Triangle testTriangle({0, 2}, {8, 16}, {13,5});
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr trianglePtr = std::make_shared<ivanov::Triangle>(testTriangle);
  
  ivanov::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const double testArea = testCompositeShape.getArea();
  const double coefficient = 3.5;

  testCompositeShape.scale(coefficient);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testArea * coefficient * coefficient, tolerance);
}

BOOST_AUTO_TEST_CASE(FrameRectChangeAreaAfterNegativeScale)
{
  ivanov::Circle testCircle(3, {2, 3});
  ivanov::Rectangle testRectangle({4, 6, {1, 3}});
  ivanov::Triangle testTriangle({0, 2}, {7, 17}, {12,5});
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);
  ivanov::shapePtr trianglePtr = std::make_shared<ivanov::Triangle>(testTriangle);
  
  ivanov::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const double testArea = testCompositeShape.getArea();
  const double coefficient = 0.5;

  testCompositeShape.scale(coefficient);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testArea * coefficient * coefficient, tolerance);
}

BOOST_AUTO_TEST_CASE(parametersAfterAddAndRemove)
{
  ivanov::Circle testCircle(4, {2, 3});
  ivanov::Rectangle testRectangle({4, 6, {2, 3}});
  ivanov::Triangle testTriangle({0, 2}, {7,15}, {12,5});
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);
  ivanov::shapePtr trianglePtr = std::make_shared<ivanov::Triangle>(testTriangle);
  
  ivanov::CompositeShape testCompositeShape(circlePtr);

  const double testCompositeArea = testCompositeShape.getArea();

  testCompositeShape.addShape(rectanglePtr);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea + testRectangle.getArea(), tolerance);

  testCompositeShape.remove(1);

  BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea, tolerance);
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  ivanov::Circle testCircle(4, {2, 3});
  ivanov::Rectangle testRectangle({4, 6, {2, 3}});
  ivanov::Triangle testTriangle({0, 2}, {7, 15}, {12, 5});
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);
  ivanov::shapePtr trianglePtr = std::make_shared<ivanov::Triangle>(testTriangle);

  ivanov::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  BOOST_CHECK_THROW(testCompositeShape.scale(-3.0), std::invalid_argument);
  BOOST_CHECK_THROW(testCompositeShape.scale(0.0), std::invalid_argument);

  BOOST_CHECK_THROW(testCompositeShape[4], std::out_of_range);
  BOOST_CHECK_THROW(testCompositeShape[-2], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkAfterCopyConstructor)
{
  ivanov::Circle testCircle(4, {2, 3});
  ivanov::Rectangle testRectangle({4, 6, {2, 3}});
  ivanov::Triangle testTriangle({0, 2}, {7,15}, {12,5});
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr trianglePtr = std::make_shared<ivanov::Triangle>(testTriangle);
  
  ivanov::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const ivanov::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getCount();

  ivanov::CompositeShape copyCompositeShape(testCompositeShape);

  const ivanov::rectangle_t copyFrameRect = copyCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, copyCompositeShape.getArea(), tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.width, copyFrameRect.width, tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.height, copyFrameRect.height, tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, copyFrameRect.pos.x, tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, copyFrameRect.pos.y, tolerance);
  BOOST_CHECK_EQUAL(testCompositeSize, copyCompositeShape.getCount());
}

BOOST_AUTO_TEST_CASE(checkAfterCopyOperator)
{
  ivanov::Circle testCircle(4, { 2, 3 });
  ivanov::Rectangle testRectangle({ 4, 6, {2, 3} });
  ivanov::Triangle testTriangle({ 0, 2 }, { 7,15 }, { 12,5 });
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);
  ivanov::shapePtr trianglePtr = std::make_shared<ivanov::Triangle>(testTriangle);

  ivanov::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const ivanov::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getCount();

  ivanov::Circle testCircleNew(3.1, { 3.1, 4.7 });
  ivanov::shapePtr circlePtrNew = std::make_shared<ivanov::Circle>(testCircleNew);

  ivanov::CompositeShape copyCompositeShape(circlePtrNew);

  copyCompositeShape = testCompositeShape;

  const ivanov::rectangle_t copyFrameRect = copyCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, copyCompositeShape.getArea(), tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.width, copyFrameRect.width, tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.height, copyFrameRect.height, tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, copyFrameRect.pos.x, tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, copyFrameRect.pos.y, tolerance);
  BOOST_CHECK_EQUAL(testCompositeSize, copyCompositeShape.getCount());
}


BOOST_AUTO_TEST_CASE(checkAfterMoveConstructor)
{
  ivanov::Circle testCircle(4, {2, 3});
  ivanov::Rectangle testRectangle({4, 6, {2, 3}});
  ivanov::Triangle testTriangle({0, 2}, {7,15}, {12,5});
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);
  ivanov::shapePtr trianglePtr = std::make_shared<ivanov::Triangle>(testTriangle);
  
  ivanov::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const ivanov::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getCount();

  ivanov::CompositeShape moveCompositeShape(std::move(testCompositeShape));

  const ivanov::rectangle_t moveFrameRect = moveCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, moveCompositeShape.getArea(), tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.width, moveFrameRect.width, tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.height, moveFrameRect.height, tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, moveFrameRect.pos.x, tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, moveFrameRect.pos.y, tolerance);
  BOOST_CHECK_EQUAL(testCompositeSize, moveCompositeShape.getCount());
}

BOOST_AUTO_TEST_CASE(checkAfterMoveOperator)
{
  ivanov::Circle testCircle(4, { 2, 3 });
  ivanov::Rectangle testRectangle({ 4, 6, {2, 3} });
  ivanov::Triangle testTriangle({ 0, 2 }, { 7,15 }, { 12,5 });
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);
  ivanov::shapePtr trianglePtr = std::make_shared<ivanov::Triangle>(testTriangle);
  
  ivanov::CompositeShape testCompositeShape(circlePtr);

  testCompositeShape.addShape(trianglePtr);

  const ivanov::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double testCompositeArea = testCompositeShape.getArea();
  const int testCompositeSize = testCompositeShape.getCount();

  ivanov::CompositeShape moveCompositeShape;

  moveCompositeShape = std::move(testCompositeShape);

  const ivanov::rectangle_t moveFrameRect = moveCompositeShape.getFrameRect();

  BOOST_CHECK_CLOSE(testCompositeArea, moveCompositeShape.getArea(), tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.width, moveFrameRect.width, tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.height, moveFrameRect.height, tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.pos.x, moveFrameRect.pos.x, tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, moveFrameRect.pos.y, tolerance);
  BOOST_CHECK_EQUAL(testCompositeSize, moveCompositeShape.getCount());
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  ivanov::Circle testCircle(1, { 5, 5 });
  ivanov::Rectangle testRectangle({ 4, 2, {5, 5} });
  ivanov::Triangle testTriangle({ 0, 2 }, { 7,15 }, { 12,5 });
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);
  ivanov::shapePtr trianglePtr = std::make_shared<ivanov::Triangle>(testTriangle);
  
  ivanov::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.addShape(circlePtr);

  const double testArea = testCompositeShape.getArea();
  const ivanov::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
  const double angle = 90;

  testCompositeShape.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testCompositeShape.getFrameRect().width / 2, tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCompositeShape.getFrameRect().height * 2, tolerance);
  BOOST_CHECK_CLOSE(testArea, testCompositeShape.getArea(), tolerance);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testCompositeShape.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testCompositeShape.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
