#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>
#include "circle.hpp"

const double tolerance = 0.001;

BOOST_AUTO_TEST_SUITE(circleTest)

BOOST_AUTO_TEST_CASE(circleConstantParametersAfterMovingToPoint)
{
  ivanov::Circle testCircle(4, {2, 3});
  const double testCircleArea = testCircle.getArea();
  const ivanov::rectangle_t testFrameRect = testCircle.getFrameRect();
  testCircle.move({1.5, 7});
  BOOST_CHECK_CLOSE(testCircle.getFrameRect().height, testFrameRect.height, tolerance);
  BOOST_CHECK_CLOSE(testCircle.getFrameRect().width, testFrameRect.width, tolerance);
  BOOST_CHECK_CLOSE(testCircle.getArea(), testCircleArea, tolerance);
}

BOOST_AUTO_TEST_CASE(circleConstantParametersAfterMovingByDistance)
{
  ivanov::Circle testCircle(8.2, {2.4, 3.6});
  const double testCircleArea = testCircle.getArea();
  const ivanov::rectangle_t testFrameRect = testCircle.getFrameRect();
  testCircle.move(11.4, 2.3);
  BOOST_CHECK_CLOSE(testCircle.getFrameRect().height, testFrameRect.height, tolerance);
  BOOST_CHECK_CLOSE(testCircle.getFrameRect().width, testFrameRect.width, tolerance);
  BOOST_CHECK_CLOSE(testCircle.getArea(), testCircleArea, tolerance);
}

BOOST_AUTO_TEST_CASE(boundingCircleChangeAfterScaling)
{
  ivanov::Circle testCircle(4, {2.1, 7.7});
  const double testArea = testCircle.getArea();
  const double coefficient = 3.5;
  testCircle.scale(coefficient);
  BOOST_CHECK_CLOSE(testCircle.getArea(), (testArea * coefficient * coefficient), tolerance);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  ivanov::Circle testCircle(1.8, {4.9, 2.0});
  const double testArea = testCircle.getArea();
  const ivanov::rectangle_t testFrameRect = testCircle.getFrameRect();
  const double angle = 90;

  testCircle.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testCircle.getFrameRect().width, tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.height, testCircle.getFrameRect().height, tolerance);
  BOOST_CHECK_CLOSE(testArea, testCircle.getArea(), tolerance);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testCircle.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testCircle.getFrameRect().pos.y);
}


BOOST_AUTO_TEST_CASE(circleInvalidValues)
{
  BOOST_CHECK_THROW(ivanov::Circle testCircle(0, {1.2, 4}), std::invalid_argument);
  BOOST_CHECK_THROW(ivanov::Circle testCircle(-4, {4.3, 4.1}), std::invalid_argument);
  ivanov::Circle testCircle(6, {1, 8.2});
  BOOST_CHECK_THROW(testCircle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(testCircle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
