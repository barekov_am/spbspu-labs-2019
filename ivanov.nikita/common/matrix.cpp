#include "matrix.hpp"
#include <stdexcept>
#include <iostream>

ivanov::Matrix::Matrix() :
  lines_(0),
  columns_(0),
  list_(nullptr)
{
}

ivanov::Matrix::Matrix(const Matrix &copyMatrix) :
  lines_(copyMatrix.lines_),
  columns_(copyMatrix.columns_),
  list_(std::make_unique<shapePtr[]>(copyMatrix.lines_* copyMatrix.columns_))
{
  for (unsigned int i = 0; i < (lines_ * columns_); i++)
  {
    list_[i] = copyMatrix.list_[i];
  }
}

ivanov::Matrix::Matrix(Matrix &&moveMatrix) :
  lines_(moveMatrix.lines_),
  columns_(moveMatrix.columns_),
  list_(std::move(moveMatrix.list_))
{
  moveMatrix.lines_ = 0;
  moveMatrix.columns_ = 0;
}

ivanov::Matrix& ivanov::Matrix::operator =(const Matrix &copyMatrix)
{
  if (this != &copyMatrix)
  {
    lines_ = copyMatrix.lines_;
    columns_ = copyMatrix.columns_;
    dynamicArray tempMatrix(std::make_unique<shapePtr[]>(copyMatrix.lines_ * copyMatrix.columns_));

    for (unsigned int i = 0; i < (lines_ * columns_); i++)
    {
      tempMatrix[i] = copyMatrix.list_[i];
    }
    list_.swap(tempMatrix);
  }

  return *this;
}

ivanov::Matrix& ivanov::Matrix::operator =(Matrix &&moveMatrix)
{
  if (this != &moveMatrix)
  {
    lines_ = moveMatrix.lines_;
    columns_ = moveMatrix.columns_;
    list_ = std::move(moveMatrix.list_);
    moveMatrix.lines_ = 0;
    moveMatrix.columns_ = 0;
  }

  return *this;
}

ivanov::dynamicArray ivanov::Matrix::operator [](unsigned int index) const
{
  if (lines_ <= index)
  {
    throw std::out_of_range("Index is out of range");
  }

  dynamicArray tempMatrix(std::make_unique<shapePtr[]>(columns_));

  for (unsigned int i = 0; i < columns_; i++)
  {
    tempMatrix[i] = list_[index * columns_ + i];
  }

  return tempMatrix;
}

bool ivanov::Matrix::operator ==(const Matrix &moveMatrix) const
{
  if ((lines_ != moveMatrix.lines_) || (columns_ != moveMatrix.columns_))
  {
    return false;
  }

  for (unsigned int i = 0; i < (lines_ * columns_); i++)
  {
    if (list_[i] != moveMatrix.list_[i])
    {
      return false;
    }
  }

  return true;
}

bool ivanov::Matrix::operator !=(const Matrix &moveMatrix) const
{
  return !(*this == moveMatrix);
}

unsigned int ivanov::Matrix::getLines() const
{
  return lines_;
}

unsigned int ivanov::Matrix::getColumns() const
{
  return columns_;
}

void ivanov::Matrix::add(const shapePtr &moveMatrix, unsigned int line, unsigned int column)
{
  unsigned int tempLines;
  unsigned int tempColumns;

  if (line == lines_)
  {
    tempLines = lines_ + 1;
  }
  else
  {
    tempLines = lines_;
  }

  if (column == columns_)
  {
    tempColumns = columns_ + 1;
  }
  else
  {
    tempColumns = columns_;
  }

  dynamicArray tempList(std::make_unique<shapePtr[]>(tempLines * tempColumns));

  for (unsigned int i = 0; i < tempLines; i++)
  {
    for (unsigned int j = 0; j < tempColumns; j++)
    {
      if ((i == lines_) || (j == columns_))
      {
        tempList[i * tempColumns + j] = nullptr;
      }
      else
      {
        tempList[i * tempColumns + j] = list_[i * columns_ + j];
      }
    }
  }

  tempList[line * tempColumns + column] = moveMatrix;
  list_.swap(tempList);
  lines_ = tempLines;
  columns_ = tempColumns;
}

void ivanov::Matrix::printInfo() const
{
  std::cout << "Value of columns: " << columns_ << "\n";
  std::cout << "Value of lines: " << lines_ << "\n";

  for (unsigned int i = 0; i < lines_; i++)
  {
    for (unsigned int j = 0; j < columns_; j++)
    {
      if (list_[i * columns_ + j] != nullptr)
      {
        std::cout << "On layer number " << i + 1 << ", position " << j + 1 << " - there is figure:\n";
        list_[i * columns_ + j]->draw();
      }
    }
  }
}
