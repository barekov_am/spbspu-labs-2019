#include "composite-shape.hpp"
#include <iostream>
#include <algorithm>
#define _USE_MATH_DEFINES
#include <math.h>

const double FullAngle = 360;

ivanov::CompositeShape::CompositeShape() :
  count_(0),
  shapes_(nullptr),
  angle_(0)
{
}

ivanov::CompositeShape::CompositeShape(shapePtr &shape) :
  count_(1),
  shapes_(std::make_unique<shapePtr[]>(count_)),
  angle_(0)
{
  shapes_[0] = shape;
}

ivanov::CompositeShape::CompositeShape(const CompositeShape &copyCompositeShape) :
  count_(copyCompositeShape.count_),
  shapes_(std::make_unique<shapePtr[]>(copyCompositeShape.count_)),
  angle_(0)
{
  for (unsigned int i = 0; i < count_; ++i)
  {
    shapes_[i] = copyCompositeShape.shapes_[i];
  }
}

ivanov::CompositeShape::CompositeShape(CompositeShape  &&moveCompositeShape) :
  count_(moveCompositeShape.count_),
  shapes_(std::move(moveCompositeShape.shapes_)),
  angle_(moveCompositeShape.angle_)
{
  moveCompositeShape.count_ = 0;
}

ivanov::CompositeShape& ivanov::CompositeShape::operator =(const CompositeShape &copyCompositeShape)
{
  if (this != &copyCompositeShape)
  {
    count_ = copyCompositeShape.count_;
    angle_ = copyCompositeShape.angle_;
    dynamicArray tempArray = std::make_unique<shapePtr[]>(count_);

    for (unsigned int i = 0; i < count_; i++)
    {
      tempArray[i] = copyCompositeShape.shapes_[i];
    }
    shapes_.swap(tempArray);
  }
  return *this;
}

ivanov::CompositeShape& ivanov::CompositeShape::operator =(ivanov::CompositeShape &&moveCompositeShape)
{
  if (this != &moveCompositeShape)
  {
    count_ = moveCompositeShape.count_;
    angle_ = moveCompositeShape.count_;
    shapes_ = std::move(moveCompositeShape.shapes_);
    moveCompositeShape.count_ = 0;
  }
  return *this;
}

ivanov::Shape& ivanov::CompositeShape::operator [](unsigned int index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }
  return *shapes_[index];
}

unsigned int ivanov::CompositeShape::getCount() const
{
  return count_;
}

double ivanov::CompositeShape::getArea() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  double area = 0;

  for (unsigned int i = 0; i < count_; ++i)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

ivanov::rectangle_t ivanov::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty.");
  }

  rectangle_t tempFrame = shapes_[0]->getFrameRect();
  double minX = tempFrame.pos.x - tempFrame.width / 2;
  double minY = tempFrame.pos.y - tempFrame.height / 2;
  double maxX = tempFrame.pos.x + tempFrame.width / 2;
  double maxY = tempFrame.pos.y + tempFrame.height / 2;

  for (size_t i = 0; i < count_; i++)
  {
    tempFrame = shapes_[i]->getFrameRect();
    minX = std::min(tempFrame.pos.x - tempFrame.width / 2, minX);
    minY = std::min(tempFrame.pos.y - tempFrame.height / 2, minY);
    maxX = std::max(tempFrame.pos.x + tempFrame.width / 2, maxX);
    maxY = std::max(tempFrame.pos.y + tempFrame.height / 2, maxY);
  }
  return rectangle_t{(maxY - minY), (maxX - minX), {(minX + maxX) / 2, (minY + maxY) / 2}};
}

void ivanov::CompositeShape::move(const point_t &point)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  point_t centerCompositeShape = getFrameRect().pos;
  double dx = point.x - centerCompositeShape.x;
  double dy = point.y - centerCompositeShape.y;

  for (unsigned int i = 0; i < count_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void ivanov::CompositeShape::move(const double dx, const double dy)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  for (unsigned int i = 0; i < count_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void ivanov::CompositeShape::draw() const
{
  std::cout << "Area of composite shape: " << getArea() << "\n";
  std::cout << "Count of figures in composite shape: " << count_ << "\n";
  std::cout << "Width of frame rectangle of composite shape: " << getFrameRect().width << "\n";
  std::cout << "Height of frame rectangle of composite shape: " << getFrameRect().height << "\n";
  std::cout << "Center point of frame rectangle of composite shape: (" << getFrameRect().pos.x
    << "; " << getFrameRect().pos.y << ")" << "\n";
}

void ivanov::CompositeShape::addShape(const shapePtr& shape)
{
  dynamicArray tempShape = std::make_unique<shapePtr[]>(count_ + 1);
  for (unsigned int i = 0; i < count_; i++)
  {
    tempShape[i] = shapes_[i];
  }
  tempShape[count_] = shape;
  count_++;
  shapes_.swap(tempShape);
}

void ivanov::CompositeShape::remove(unsigned int index)
{
  if (index >= count_)
  {
    throw std::invalid_argument("Index out of range");
  }
  count_--;

  for (unsigned int i = index; i < count_; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
}

void ivanov::CompositeShape::scale(const double coefficient)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient can't be negative");
  }

  ivanov::point_t frameRect = getFrameRect().pos;
  for (unsigned int i = 0; i < count_; i++)
  {
    ivanov::point_t shapeCenter = shapes_[i]->getFrameRect().pos;
    double dx = (shapeCenter.x - frameRect.x) * (coefficient - 1);
    double dy = (shapeCenter.y - frameRect.y) * (coefficient - 1);
    shapes_[i]->move(dx, dy);
    shapes_[i]->scale(coefficient);
  }
}

void ivanov::CompositeShape::rotate(const double angle)
{
  angle_ += angle;

  if (angle_ < 0.0)
  {
    angle_ = FullAngle + fmod(angle_, FullAngle);
  }
  else
  {
    angle_ = fmod(angle_, FullAngle);
  }

  const double radAngle = angle_ * M_PI / 180;
  const point_t center = getFrameRect().pos;

  for (unsigned int i = 0; i < count_; i++)
  {
    const double oldX = shapes_[i]->getFrameRect().pos.x - center.x;
    const double oldY = shapes_[i]->getFrameRect().pos.y - center.y;

    const double newX = oldX * fabs(cos(radAngle)) - oldY * fabs(sin(radAngle));
    const double newY = oldX * fabs(sin(radAngle)) + oldY * fabs(cos(radAngle));

    shapes_[i]->move({ center.x + newX, center.y + newY });
    shapes_[i]->rotate(angle);
  }
}
ivanov::dynamicArray ivanov::CompositeShape::getList() const
{
  dynamicArray tempArray(std::make_unique<shapePtr[]>(count_));

  for (unsigned int i = 0; i < count_; i++)
  {
    tempArray[i] = shapes_[i];
  }

  return tempArray;
}
