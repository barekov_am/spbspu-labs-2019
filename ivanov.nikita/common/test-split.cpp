#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "split.hpp"

BOOST_AUTO_TEST_SUITE(splitTest)

BOOST_AUTO_TEST_CASE(checkCrossing)
{
  ivanov::Rectangle testRectangle({4, 2, {5, 5}});
  ivanov::Rectangle crossingTestRectangle({6, 2, {8, 5}});
  ivanov::Circle testCircle(1, {11, 5});

  const bool falseResult = ivanov::cross(testRectangle.getFrameRect(), testCircle.getFrameRect());
  const bool trueResult1 = ivanov::cross(testRectangle.getFrameRect(), crossingTestRectangle.getFrameRect());
  const bool trueResult2 = ivanov::cross(testCircle.getFrameRect(), crossingTestRectangle.getFrameRect());

  BOOST_CHECK_EQUAL(falseResult, false);
  BOOST_CHECK_EQUAL(trueResult1, true);
  BOOST_CHECK_EQUAL(trueResult2, true);

}

BOOST_AUTO_TEST_CASE(checkCorrectSeparation)
{
  ivanov::Rectangle testRectangle({4, 2, {5, 5}});
  ivanov::Rectangle crossingTestRectangle({6, 2, {8, 5}});
  ivanov::Circle testCircle(1, {11, 5});
  ivanov::shapePtr rectanglePtr = std::make_shared<ivanov::Rectangle>(testRectangle);
  ivanov::shapePtr rectangleCrossingPtr = std::make_shared<ivanov::Rectangle>(crossingTestRectangle);
  ivanov::shapePtr circlePtr = std::make_shared<ivanov::Circle>(testCircle);

  ivanov::CompositeShape testCompositeShape(rectanglePtr);
  testCompositeShape.addShape(rectangleCrossingPtr);
  testCompositeShape.addShape(circlePtr);

  ivanov::Matrix testMatrix = ivanov::part(testCompositeShape);

  BOOST_CHECK(testMatrix[0][0] == rectanglePtr);
  BOOST_CHECK(testMatrix[0][1] == circlePtr);
  BOOST_CHECK(testMatrix[1][0] == rectangleCrossingPtr);
}

BOOST_AUTO_TEST_SUITE_END()
