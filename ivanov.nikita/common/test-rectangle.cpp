#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>
#include "rectangle.hpp"

const double tolerance = 0.001;

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(rectangleConstantParametersAfterMovingByDistance)
{
  ivanov::Rectangle testRectangle({3, 6.2, {1, 3}});
  const double testRectangleArea = testRectangle.getArea();
  const ivanov::rectangle_t testFrameRect = testRectangle.getFrameRect();
  testRectangle.move(4, 2);
  BOOST_CHECK_CLOSE(testRectangle.getFrameRect().height, testFrameRect.height, tolerance);
  BOOST_CHECK_CLOSE(testRectangle.getFrameRect().width, testFrameRect.width, tolerance);
  BOOST_CHECK_CLOSE(testRectangle.getArea(), testRectangleArea, tolerance);
}

BOOST_AUTO_TEST_CASE(rectangleConstantParametersAfterMovingToPoint)
{
  ivanov::Rectangle testRectangle({4.1, 2, {2, 3.6}});
  const double testRectangleArea = testRectangle.getArea();
  const ivanov::rectangle_t testFrameRect = testRectangle.getFrameRect();
  testRectangle.move({4.2, 7});
  BOOST_CHECK_CLOSE(testRectangle.getFrameRect().height, testFrameRect.height, tolerance);
  BOOST_CHECK_CLOSE(testRectangle.getFrameRect().width, testFrameRect.width, tolerance);
  BOOST_CHECK_CLOSE(testRectangle.getArea(), testRectangleArea, tolerance);
}

BOOST_AUTO_TEST_CASE(boundingRectangleChangeAfterScalingRectangle)
{
  ivanov::Rectangle testRectangle({1.9, 8.4, {2.7, 7}});
  const double testArea = testRectangle.getArea();
  const double coefficient = 3.5;
  testRectangle.scale(coefficient);
  BOOST_CHECK_CLOSE(testRectangle.getArea(), (testArea * coefficient * coefficient), tolerance);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  ivanov::Rectangle testRectangle({ 4, 2, {5, 5} });
  const double testArea = testRectangle.getArea();
  const ivanov::rectangle_t testFrameRect = testRectangle.getFrameRect();
  const double angle = 90;

  testRectangle.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testRectangle.getFrameRect().width * 2, tolerance);
  BOOST_CHECK_CLOSE(testFrameRect.height, testRectangle.getFrameRect().height / 2, tolerance);
  BOOST_CHECK_CLOSE(testArea, testRectangle.getArea(), tolerance);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testRectangle.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testRectangle.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_CASE(rectangleInvalidValues)
{
  BOOST_CHECK_THROW(ivanov::Rectangle testRectangle({0, 2.5, {3, 4.1}}), std::invalid_argument);
  BOOST_CHECK_THROW(ivanov::Rectangle testRectangle({3.1, 0, {4, 4}}), std::invalid_argument);
  BOOST_CHECK_THROW(ivanov::Rectangle testRectangle({-1.8, 7, {2, 4}}), std::invalid_argument);
  BOOST_CHECK_THROW(ivanov::Rectangle testRectangle({6, -1, {7, 4.9}}), std::invalid_argument);

  ivanov::Rectangle testRectangle({2, 1, {1, 8.2}});

  BOOST_CHECK_THROW(testRectangle.scale(-1), std::invalid_argument);
  BOOST_CHECK_THROW(testRectangle.scale(0), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()
