#include "split.hpp"
#include <cmath>

bool ivanov::cross(const rectangle_t& lhs, const rectangle_t& rhs)
{
  const bool firstCondition = (fabs(lhs.pos.x - rhs.pos.x) < (lhs.width + rhs.width) / 2);
  const bool secondCondition = (fabs(lhs.pos.y - rhs.pos.y) < (lhs.height + rhs.height) / 2);

  return firstCondition && secondCondition;
}

ivanov::Matrix ivanov::part(const CompositeShape& composite)
{
  return part(composite.getList(), composite.getCount());
}

ivanov::Matrix ivanov::part(const dynamicArray & array, unsigned int size)
{
  Matrix tempMatrix;

  for (unsigned int i = 0; i < size; i++)
  {
    unsigned int rightColumn = 0;
    unsigned int rightLine = 0;

    for (unsigned int j = 0; j < tempMatrix.getLines(); j++)
    {

      for (unsigned int k = 0; k < tempMatrix.getColumns(); k++)
      {

        if (tempMatrix[j][k] == nullptr)
        {
          rightLine = j;
          rightColumn = k;
          break;
        }

        if (cross(array[i]->getFrameRect(), tempMatrix[j][k]->getFrameRect()))
        {
          rightLine = j + 1;
          rightColumn = 0;
          break;
        }
        else
        {
          rightLine = j;
          rightColumn = k + 1;
        }
      }

      if (rightLine == j)
      {
        break;
      }
    }

    tempMatrix.add(array[i], rightLine, rightColumn);
  }

  return tempMatrix;
}
