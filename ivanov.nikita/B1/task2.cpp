#include <fstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <vector>

const size_t Capacity = 1024;

void task2(const char* filename)
{
  using charArray = std::unique_ptr<char[], decltype(&free)>;

  std::ifstream inputFile(filename);
  if (!inputFile)
  {
    throw std::ios_base::failure("Trouble with file.");
  }

  size_t size = Capacity;

  charArray arr(static_cast<char*>(malloc(size)), &free);

  size_t i = 0;
  while (inputFile)
  {
    inputFile.read(&arr[i], Capacity);
    i += inputFile.gcount();

    if (inputFile.gcount() == Capacity)
    {
      size += Capacity;
      charArray tempArr(static_cast<char*>(realloc(arr.get(), size)), &free);

      if (tempArr)
      {
        arr.release();
        std::swap(arr, tempArr);
      }
      else
      {
        throw std::runtime_error("Failed to reallocate.");
      }
    }
  }
  inputFile.close();

  if (inputFile.is_open())
  {
    throw std::ios_base::failure("Failed to close file.");
  }
  
  std::vector<char> vector(&arr[0], &arr[i]);

  for (const auto& iterator: vector)
  {
    std::cout << iterator;
  }
}
