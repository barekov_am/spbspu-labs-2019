#include <iostream>
#include <stdexcept>
#include <string>

void task1(const char* direction);
void task2(const char* filename);
void task3();
void task4(const char* direction, size_t size);

int main(int argc, char* argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4))
      throw std::invalid_argument("Incorrect number of arguments.");

    char* ptr = nullptr;
    int taskNumber = std::strtol(argv[1], &ptr, 10);
    if (*ptr != 0x00)
      throw std::invalid_argument("Incorrect argument.");

    switch (taskNumber)
    {
    case 1:
      if (argc != 3)
        throw std::invalid_argument("Incorrect number of arguments.");
      task1(argv[2]);
      break;

    case 2:
      if (argc != 3)
        throw std::invalid_argument("Incorrect number of arguments.");
      task2(argv[2]);
      break;

    case 3:
      if (argc != 2)
        throw std::invalid_argument("Incorrect number of arguments.");
      task3();
      break;

    case 4:
      if (argc != 4)
        throw std::invalid_argument("Incorrect number of arguments.");
      task4(argv[2], std::stoi(argv[3]));
      break;

    default:
      throw std::invalid_argument("Incorrect argument.");
    }
  }
  catch (const std::exception& exeption)
  {
    std::cerr << exeption.what() << '\n';

    return 1;
  }

  return 0;
}
