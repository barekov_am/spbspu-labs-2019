#ifndef SORT_HPP
#define SORT_HPP

#include <functional>

#include "accesses-structures.hpp"

enum class Direction
{
  ascending,
  descending
};

Direction getDirection(const char* direction);

template <template <class T> class Access, typename T>
void bubbleSort(T& collection, Direction direction)
{
  const auto begin = Access<T>::begin(collection);
  const auto end = Access<T>::end(collection);

  for (auto i = begin; i != end; ++i)
  {
    for (auto j = i; j != end; ++j)
    {
      if (Access<T>::getElement(collection, i) > Access<T>::getElement(collection, j))
      {
        std::swap(Access<T>::getElement(collection, i), Access<T>::getElement(collection, j));
      }
    }
  }

  if (direction == Direction::descending)
    Access<T>::reverse(collection);
}

#endif
