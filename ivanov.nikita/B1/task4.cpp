#include <random>
#include <vector>
#include <time.h>

#include "sort.hpp"
#include "print.hpp"


void fillRandom(double* array, size_t size)
{
  std::mt19937 rng(time(0));
  std::uniform_real_distribution<double> distribution(-1.0, 1.0);
  for (size_t i = 0; i < size; ++i)
  {
    array[i] = distribution(rng);
  }
}

void task4(const char* direction, size_t size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size must be >= 0.");
  }

  Direction dir = getDirection(direction);

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);

  print(vector);
  bubbleSort<BracketAccess>(vector, dir);
  print(vector);
}

