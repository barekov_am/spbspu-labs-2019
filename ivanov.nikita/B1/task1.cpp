#include <forward_list>
#include <vector>

#include "print.hpp"
#include "sort.hpp"

void task1(const char* direction)
{
  Direction dir = getDirection(direction);

  std::vector<int> vectorBracket;
  int num = 0;
  while (std::cin >> num)
    vectorBracket.push_back(num);

  if (!std::cin.eof())
    throw std::ios_base::failure("Fail while reading data.");

  if (vectorBracket.empty())
    return;

  std::vector<int> vectorAt = vectorBracket;
  std::forward_list<int> listIterator(vectorBracket.begin(), vectorBracket.end());

  bubbleSort<BracketAccess>(vectorBracket, dir);
  bubbleSort<AtAccess>(vectorAt, dir);
  bubbleSort<IteratorAccess>(listIterator, dir);

  print(vectorBracket);
  print(vectorAt);
  print(listIterator);
}
