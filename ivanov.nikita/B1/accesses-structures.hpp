#ifndef ACCESSES_STRUCTURES_HPP
#define ACCESSES_STRUCTURES_HPP

#include <iterator>
#include <algorithm>

template <typename T>
struct AtAccess
{
  typedef size_t index;

  static index begin(const T&)
  {
    return 0;
  }

  static index end(const T& collection)
  {
    return collection.size();
  }

  static typename T::reference getElement(T& collection, index i)
  {
    return collection.at(i);
  }

  static index next(index i)
  {
    return i + 1;
  }

  static void reverse(T& collection)
  {
    std::reverse(collection.begin(), collection.end());
  }

};

template <typename T>
struct BracketAccess
{
  typedef size_t index;

  static index begin(const T&)
  {
    return 0;
  }

  static index end(const T& collection)
  {
    return collection.size();
  }

  static typename T::reference getElement(T& collection, index i)
  {
    return collection[i];
  }

  static index next(index i)
  {
    return i + 1;
  }

  static void reverse(T& collection)
  {
    std::reverse(collection.begin(), collection.end());
  }

};

template <typename T>
struct IteratorAccess
{
  typedef typename T::iterator index;

  static index begin(T& collection)
  {
    return collection.begin();
  }

  static index end(T& collection)
  {
    return collection.end();
  }

  static typename T::reference getElement(T&, index& iterator)
  {
    return *iterator;
  }

  static index next(index& iterator)
  {
    return std::next(iterator);
  }

  static void reverse(T& collection)
  {
    collection.reverse();
  }

};

#endif
