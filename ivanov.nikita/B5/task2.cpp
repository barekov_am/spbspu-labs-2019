#include <sstream>
#include <algorithm>
#include <numeric>

#include "Shape.hpp"

Shape reading(const std::string& input)
{
  std::istringstream stream(input);

  int verticesNum = 0;
  stream >> verticesNum;

  if (verticesNum < 3)
  {
    throw std::invalid_argument("Invalid number of vertices");
  }

  Shape shape;
  for (int i = 0; i < verticesNum; i++)
  {
    Point point = {0, 0 };

    stream.ignore(input.length(), '(');
    stream >> point.x;
    stream.ignore(input.length(), ';');
    stream >> point.y;
    stream.ignore(input.length(), ')');
    shape.push_back(point);
  }

  if (stream.fail())
  {
    throw std::invalid_argument("Incorrect input!");
  }

  return shape;
}

size_t getVerticesAmount(const Shapes& shapesContainer) noexcept
{
  return static_cast<size_t>(std::accumulate(shapesContainer.begin(), shapesContainer.end(), 0,
    [](size_t total, const Shape& shape)
    {
      return total + shape.size();
    }));
}

int getDistance(const Point& point1, const Point& point2) noexcept
{
  return (point1.x - point2.x) * (point1.x - point2.x) + (point1.y - point2.y) * (point1.y - point2.y);
}

bool isTriangleShape(const Shape& shape) noexcept
{
  return shape.size() == 3;
}

size_t getTrianglesAmount(const Shapes& shapes) noexcept
{
  return static_cast<size_t>(std::count_if(shapes.begin(), shapes.end(), isTriangleShape));
}

inline bool isSquareShape(const Shape& shape)
{
  return shape.size() == 4 && getDistance(shape[0], shape[2]) == getDistance(shape[1], shape[3])
         && getDistance(shape[1], shape[2]) == getDistance(shape[0], shape[3])
         && getDistance(shape[0], shape[1]) == getDistance(shape[2], shape[3])
         && getDistance(shape[0], shape[1]) == getDistance(shape[1], shape[2]);
}

size_t getSquaresAmount(const Shapes& shapes) noexcept
{
  return static_cast<size_t>(std::count_if(shapes.begin(), shapes.end(), isSquareShape));
}

inline bool isRectangleShape(const Shape& shape)
{
  return shape.size() == 4 && getDistance(shape[0], shape[2]) == getDistance(shape[1], shape[3])
         && getDistance(shape[1], shape[2]) == getDistance(shape[0], shape[3])
         && getDistance(shape[0], shape[1]) == getDistance(shape[2], shape[3]);
}

size_t getRectanglesAmount(const Shapes& shapes) noexcept
{
  return static_cast<size_t>(std::count_if(shapes.begin(), shapes.end(), isRectangleShape));
}

void deletePentagons(Shapes& shapes) noexcept
{
  shapes.erase(
    std::remove_if(shapes.begin(), shapes.end(),
                   [](const Shape& shape) { return shape.size() == 5; }),
    shapes.end());
}

std::vector<Point> makeVector(const Shapes& shapes) noexcept
{
  std::vector<Point> pointVector;

  for (const auto& shape : shapes)
  {
    pointVector.push_back(shape[0]);
  }

  return pointVector;
}

void fillShapeContainer(Shapes& shapes)
{
  std::string line;
  while (std::getline(std::cin, line))
  {
    line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
    if (line.empty())
    {
      continue;
    }
    shapes.push_back(reading(line));
  }
}

void regularizeContainer(Shapes& shapes)
{
  auto endTriangles = std::partition(shapes.begin(), shapes.end(), isTriangleShape);
  auto endSquares = std::partition(endTriangles, shapes.end(), isSquareShape);
  std::partition(endSquares, shapes.end(), isRectangleShape);
}

void task2()
{
  Shapes shapes_;
  fillShapeContainer(shapes_);
  size_t verticesNumber = getVerticesAmount(shapes_);
  size_t triangles = 0;
  size_t squares = 0;
  size_t rectangles = 0;
  triangles = getTrianglesAmount(shapes_);
  squares = getSquaresAmount(shapes_);
  rectangles = getRectanglesAmount(shapes_);

  deletePentagons(shapes_);
  std::vector<Point> pointsVector = makeVector(shapes_);
  regularizeContainer(shapes_);

  std::cout << "Vertices: " << verticesNumber << std::endl;
  std::cout << "Triangles: " << triangles << std::endl;
  std::cout << "Squares: " << squares << std::endl;
  std::cout << "Rectangles: " << rectangles << std::endl;

  std::cout << "Points:";
  for (const auto& tmp : pointsVector)
  {
    std::cout << " (" << tmp.x << "; " << tmp.y << ")";
  }
  std::cout << std::endl;

  std::cout << "Shapes:" << std::endl;
  for (const auto& shape : shapes_)
  {
    std::cout << shape.size() << " ";
    for (const auto& point : shape)
    {
      std::cout << "(" << point.x << "; " << point.y << ") ";
    }
    std::cout << std::endl;
  }
}
