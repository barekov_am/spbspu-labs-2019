#include <iostream>
#include <string>
#include <set>

void task1()
{
  std::set<std::string> words;
  std::string word;
  while (std::cin >> word)
  {
    words.emplace(word);
  }

  for(const auto& el : words)
  {
    std::cout << el << std::endl;
  }
}
