#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>
#include <vector>
#include <list>

struct Point
{
  int x, y;
};

using Shape = std::vector<Point>;
using Shapes = std::list<Shape>;

void task1();
void task2();

inline bool isRectangleShape(const Shape &shape);
inline bool isSquareShape(const Shape &shape);

#endif
