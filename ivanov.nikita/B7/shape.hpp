#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>

namespace lab7
{
  class Shape
  {
  public:
    Shape();
    Shape(double x, double y);
    virtual ~Shape() = default;

    bool isMoreLeft(const Shape* shape) const;

    bool isUpper(const Shape* shape) const;

    virtual void draw(std::ostream& out) = 0;

  protected:
    double x;
    double y;
  };
}
#endif // SHAPE_HPP
