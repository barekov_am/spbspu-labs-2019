#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <iostream>
#include <vector>

namespace lab7
{
  class Functor
  {
  public:
    void operator()(double elem);
    void print();

  private:
    std::vector<double> numbers;
  };
}

#endif // FUNCTOR_HPP
