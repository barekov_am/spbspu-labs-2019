#include <iterator>
#include <algorithm>
#include <functional>
#include <iostream>

#include "functor.hpp"

void task1()
{
  lab7::Functor functor;
  std::for_each(std::istream_iterator<double>(std::cin), std::istream_iterator<double>(), std::ref(functor));

  if (!std::cin.eof())
  {
      throw std::ios_base::failure("Reading data error\n");
  }

  functor.print();
}
