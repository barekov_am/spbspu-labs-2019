#define _USE_MATH_DEFINES

#include <cmath>
#include <algorithm>
#include <iostream>

#include "functor.hpp"

void lab7::Functor::operator()(double element)
{
  numbers.push_back(element * M_PI);
}

void lab7::Functor::print()
{
  if (numbers.empty())
  {
    return;
  }

  std::for_each(numbers.begin(), numbers.end(),[](double elem){std::cout << elem << " ";});
}
