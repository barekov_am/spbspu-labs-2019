#ifndef SHAPES_HPP
#define SHAPES_HPP

#include "shape.hpp"

class Triangle : public lab7::Shape
{
public:
  Triangle(double x, double y);
  void draw(std::ostream & out) override;
};

class Square : public lab7::Shape
{
public:
  Square(double x, double y);
  void draw(std::ostream & out) override;
};

class Circle : public lab7::Shape
{
public:
  Circle(double x, double y);
  void draw(std::ostream & out) override;
};
#endif // SHAPES_HPP
