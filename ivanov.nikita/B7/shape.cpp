#include <stdexcept>

#include "shape.hpp"

lab7::Shape::Shape():
  x(0),
  y(0)
{}

lab7::Shape::Shape(double x, double y) :
  x(x),
  y(y)
{}

bool lab7::Shape::isMoreLeft(const Shape* shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Empty shape\n");
  }

  return x < shape->x;
}

bool lab7::Shape::isUpper(const Shape* shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Empty shape\n");
  }

  return y > shape->y;
}
