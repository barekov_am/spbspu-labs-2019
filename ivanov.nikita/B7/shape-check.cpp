#include <iostream>
#include <string>
#include <memory>
#include <algorithm>

#include "shapes.hpp"

std::shared_ptr<lab7::Shape> readShape(std::string& line)
{
  if (line.empty())
  {
    throw std::invalid_argument("No data\n");
  }

  line.erase(std::remove_if(line.begin(), line.end(), [](const char ch) { return std::isspace(ch); }), line.end());
  auto bracketFirst = line.find_first_of('(');
  std::string shapeType = line.substr(0, bracketFirst);
  line.erase(0, bracketFirst);

  auto delimiter = line.find_first_of(';');
  auto bracketSecond = line.find_first_of(')');

  if ((bracketSecond == std::string::npos) || (delimiter == std::string::npos) || (bracketFirst == std::string::npos))
  {
    throw std::invalid_argument("Ivalid center\n");
  }

  int x = std::stoi(line.substr(1, delimiter - 1));
  int y = std::stoi(line.substr(delimiter + 1, bracketSecond - delimiter - 1));

  if (shapeType == "CIRCLE")
  {
    return std::make_shared<Circle>(Circle(x, y));
  }
  else if (shapeType == "TRIANGLE")
  {
    return std::make_shared<Triangle>(Triangle(x, y));
  }
  else if (shapeType == "SQUARE")
  {
    return std::make_shared<Square>(Square(x, y));
  }
  else
  {
    throw std::invalid_argument("Invalid type of shape\n");
  }
}
