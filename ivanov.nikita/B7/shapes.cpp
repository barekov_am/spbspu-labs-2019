#include "shapes.hpp"

Triangle::Triangle(double x, double y) :
  Shape(x, y)
{}

void Triangle::draw(std::ostream& output)
{
  output << "TRIANGLE (" << x << ";" << y << ")" << std::endl;
}

Square::Square(double x, double y) :
  Shape(x, y)
{}

void Square::draw(std::ostream& output)
{
  output << "SQUARE (" << x << ";" << y << ")" << std::endl;
}

Circle::Circle(double x, double y) :
  Shape(x, y)
{}

void Circle::draw(std::ostream& output)
{
  output << "CIRCLE (" << x << ";" << y << ")" << std::endl;
}
