#include <memory>
#include <string>
#include <vector>
#include <algorithm>

#include "shape.hpp"

std::shared_ptr<lab7::Shape> readShape(std::string & line);

void print(const std::vector<std::shared_ptr<lab7::Shape>> & vector)
{
  for (const auto& shape : vector)
  {
    shape->draw(std::cout);
  }
}

void task2()
{
  std::vector<std::shared_ptr<lab7::Shape>> shapes;
  std::string str;

  while (getline(std::cin >> std::ws, str))
  {
    std::shared_ptr<lab7::Shape> shape = readShape(str);
    shapes.push_back(shape);
  }

  std::cout << "Original:" << std::endl;
  print(shapes);

  std::cout << "Left-Right:" << std::endl;
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<lab7::Shape>& left, const std::shared_ptr<lab7::Shape>& right)
  {
    return left->isMoreLeft(right.get());
  });
  print(shapes);

  std::cout << "Right-Left:" <<std::endl;
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<lab7::Shape>& left, const std::shared_ptr<lab7::Shape>& right)
  {
    return !left->isMoreLeft(right.get());
  });
  print(shapes);

  std::cout << "Top-Bottom:" << std::endl;
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<lab7::Shape>& left, const std::shared_ptr<lab7::Shape>& right)
  {
    return left->isUpper(right.get());
  });
  print(shapes);

  std::cout << "Bottom-Top:" << std::endl;
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<lab7::Shape>& left, const std::shared_ptr<lab7::Shape>& right)
  {
    return !left->isUpper(right.get());
  });
  print(shapes);
}
