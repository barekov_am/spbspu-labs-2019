#include <cstring>

#include "tokens.hpp"
#include "output.hpp"

int main(int argc, char ** argv)
{
  try
  {
    const size_t minWidth = 25;
    size_t width = 40;

    if (argc > 1)
    {
      if (argc != 3)
      {
        throw std::invalid_argument("Invalid number of arguments");
      }
      else if (strcmp(argv[1], "--line-width") != 0)
      {
        throw std::invalid_argument("Invalid arguments");
      }
      else
      {
        width = static_cast<size_t>(std::stoi(argv[2]));
        if (width < minWidth)
        {
          throw std::invalid_argument("Invalid width");
        }
      }
    }

    lyalin::TokenGenerator inputHandler(std::cin);
    std::vector<lyalin::token_t> tokensVector = inputHandler.parseText();

    lyalin::OutputHandler outputHandler(std::cout, width);
    outputHandler.printLines(tokensVector);
  }
  catch (const std::exception& ex)
  {
    std::cerr << ex.what();
    return 1;
  }
  return 0;
}
