#include "output.hpp"

lyalin::OutputHandler::OutputHandler(std::ostream& ostream, size_t width) :
   ostream(ostream),
   lineWidth(width)
{
}

void lyalin::OutputHandler::printLines(const std::vector<token_t> & tokens)
{
  formatLines(tokens);
  for (const auto &line : linesVector)
  {
    ostream << line << std::endl;
  }
}

void lyalin::OutputHandler::formatLines(const std::vector<token_t> & tokens)
{
  std::vector<lyalin::token_t> temp;
  size_t currentWidth = 0;

  if (tokens.empty())
  {
    temp.push_back(token_t{
       " ", token_type_t::WHITESPACE
    });
  }

  for (size_t i = 0; i < tokens.size(); i++)
  {
    if (currentWidth + tokens[i].value.size() > lineWidth)
    {
      connectTokens(temp, i, tokens[i].type);
      i -= 1;
      currentWidth = 0;
    }
    else
    {
      temp.push_back(tokens[i]);
      currentWidth += tokens[i].value.size();

      if (currentWidth + 1 <= lineWidth)
      {
        switch (tokens[i].type)
        {
          case token_type_t::NUMBER:
          case token_type_t::WORD:
            if (i + 1 < tokens.size())
            {
              if (tokens[i + 1].type != token_type_t::PUNCT)
              {
                temp.push_back(token_t{
                   " ", token_type_t::WHITESPACE
                });
                currentWidth++;
              }
            }
            break;
          case token_type_t::PUNCT:
          case token_type_t::DASH:
            temp.push_back(token_t{
               " ", token_type_t::WHITESPACE
            });
            currentWidth++;
            break;
          case token_type_t::WHITESPACE:
            break;
        }
      }
    }
  }
  linesVector.push_back(createLine(temp));
}

std::string lyalin::OutputHandler::createLine(const std::vector<lyalin::token_t> &tokens)
{
  std::string buffer = "";
  for (const auto &t : tokens)
  {
    buffer += t.value;
  }
  if (tokens.back().type == token_type_t::WHITESPACE)
  {
    buffer.pop_back();
  }
  return buffer;
}

void lyalin::OutputHandler::connectTokens(std::vector<lyalin::token_t>& tokens, size_t& pos, token_type_t type)
{
  switch (type)
  {
    case token_type_t::WORD:
    case token_type_t::NUMBER:
      linesVector.push_back(createLine(tokens));
      break;
    case token_type_t::PUNCT:
    case token_type_t::DASH:
      while (tokens.back().type != token_type_t::NUMBER
             && tokens.back().type != token_type_t::WORD
             && !tokens.empty())
      {
        if (tokens.back().type != token_type_t::WHITESPACE && pos > 0)
        {
          pos--;
        }
        tokens.pop_back();
      }
      if (pos > 0)
      {
        pos--;
      }
      tokens.pop_back();
      linesVector.push_back(createLine(tokens));
      break;
    case token_type_t::WHITESPACE:
      break;
  }
  tokens.clear();
}

