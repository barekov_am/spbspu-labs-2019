#ifndef B8_OUTPUT_HPP
#define B8_OUTPUT_HPP

#include <string>

#include "tokens.hpp"

namespace lyalin
{
  class OutputHandler
  {
    public:
    OutputHandler(std::ostream& ostream, size_t width);
    void printLines(const std::vector<token_t> & tokenArray);

    private:
    std::ostream & ostream;
    size_t lineWidth;
    std::vector<std::string> linesVector;
    void formatLines(const std::vector<token_t> & tokens);
    void connectTokens(std::vector<lyalin::token_t>& tokens, size_t& pos, token_type_t type);
    std::string createLine(const std::vector<lyalin::token_t> &tokens);
  };
}

#endif
