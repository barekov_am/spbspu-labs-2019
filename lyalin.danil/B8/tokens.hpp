#ifndef B8_TOKENS_HPP
#define B8_TOKENS_HPP

#include <iostream>
#include <vector>
#include <locale>

namespace lyalin
{
  enum class token_type_t
  {
    WORD,
    PUNCT,
    DASH,
    NUMBER,
    WHITESPACE
  };

  struct token_t
  {
    std::string value;
    token_type_t type;
  };

  class TokenGenerator
  {
    public:
    explicit TokenGenerator(std::istream &istream);
    std::vector<token_t> parseText();

    private:
    std::istream &istream;
    std::vector<lyalin::token_t> tokensVector;
    void readWord();
    void readNum();
    void readDash();
    void genTokens();
    void checkInput();
  };
}
#endif
