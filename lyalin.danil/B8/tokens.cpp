#include "tokens.hpp"

lyalin::TokenGenerator::TokenGenerator(std::istream & istream) :
   istream(istream)
{
}

std::vector<lyalin::token_t> lyalin::TokenGenerator::parseText()
{
  if (!istream)
  {
    throw std::invalid_argument("Invalid input stream");
  }
  genTokens();
  checkInput();
  return tokensVector;
}

void lyalin::TokenGenerator::readWord()
{
  token_t token{
     "", token_type_t::WORD
  };

  do
  {
    auto chr = static_cast<char>(istream.get());
    token.value.push_back(chr);
    if (chr == '-' && istream.peek() == '-')
    {
      token.value.pop_back();
      istream.unget();
      break;
    }
  } while ((std::isalpha(static_cast<char>(istream.peek()), std::locale())) || (istream.peek() == '-'));
  tokensVector.push_back(token);
}

void lyalin::TokenGenerator::readNum()
{
  token_t token{
     "", token_type_t::NUMBER
  };

  char decimalPoint = std::use_facet<std::numpunct<char>>(std::locale()).decimal_point();
  bool decimalPointRead = false;

  do
  {
    auto chr = static_cast<char>(istream.get());
    if (chr == decimalPoint)
    {
      if (decimalPointRead)
      {
        istream.unget();
        break;
      }
      decimalPointRead = true;
    }
    token.value.push_back(chr);
  } while ((std::isdigit<char>(static_cast<char>(istream.peek()), std::locale()) || (istream.peek() == decimalPoint)));
  tokensVector.push_back(token);
}

void lyalin::TokenGenerator::readDash()
{
  token_t token{
     "", token_type_t::DASH
  };

  while (istream.peek() == '-')
  {
    auto chr = static_cast<char>(istream.get());
    token.value.push_back(chr);
  }
  tokensVector.push_back(token);
}

void lyalin::TokenGenerator::genTokens()
{
  while (istream)
  {
    auto chr = static_cast<char>(istream.get());
    while (std::isspace(chr, std::locale()))
    {
      chr = static_cast<char>(istream.get());
    }

    if (isalpha(chr, std::locale()))
    {
      istream.unget();
      readWord();
    }
    else if (chr == '-')
    {
      if (istream.peek() == '-')
      {
        istream.unget();
        readDash();
      }
      else
      {
        istream.unget();
        readNum();
      }
    }
    else if ((chr == '+') || (isdigit(chr, std::locale())))
    {
      istream.unget();
      readNum();
    }
    else if (ispunct(chr, std::locale()))
    {
      token_t token{
         "", token_type_t::PUNCT
      };
      token.value.push_back(chr);
      tokensVector.push_back(token);
    }
  }
}

void lyalin::TokenGenerator::checkInput()
{
  if (!tokensVector.empty() &&
      (tokensVector.front().type != token_type_t::WORD)
      && (tokensVector.front().type != token_type_t::NUMBER))
  {
    throw std::invalid_argument("Input starts with punctuation or dash");
  }

  for (auto token = tokensVector.begin(); token != tokensVector.end(); ++token)
  {
    switch (token->type)
    {
      case token_type_t::WORD:
      case token_type_t::NUMBER:
        if (token->value == "-" || token->value == "+")
        {
          throw std::invalid_argument("Invalid dash sign");
        }
        if (token->value.size() > 20)
        {
          throw std::invalid_argument("Length of char sequence must be more than 20");
        }
        break;
      case token_type_t::DASH:
        if (token->value.size() != 3)
        {
          throw std::invalid_argument("Invalid dash sign");
        }
        if (token != tokensVector.begin())
        {
          const auto prev = std::prev(token);
          if ((prev->type == token_type_t::DASH) || ((prev->type == token_type_t::PUNCT) && (prev->value != ",")))
          {
            throw std::invalid_argument("Invalid char sequence");
          }
        }
        break;
      case token_type_t::PUNCT:
        if (token != tokensVector.begin()) {
          const auto prev = std::prev(token);
          if ((prev->type == token_type_t::DASH) || (prev->type == token_type_t::PUNCT))
          {
            throw std::invalid_argument("Invalid char sequence");
          }
        }
        break;
      case token_type_t::WHITESPACE:
        break;
    }
  }
}
