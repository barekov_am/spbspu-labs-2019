#include "tasks.hpp"

#include <set>
#include "shape.hpp"

void lyalin::task1()
{
  std::set<std::string> words;
  std::string buffer;

  while (std::cin >> buffer)
  {
    words.emplace(buffer);
  }

  for (const auto &i : words)
  {
    std::cout << i << std::endl;
  }
}

void lyalin::task2()
{
  lyalin::Shapes shapes;

  lyalin::fillShapeContainer(shapes);

  size_t vertices = static_cast<size_t>(std::accumulate(shapes.begin(), shapes.end(), 0,
                                                        [](size_t totalVerts, const lyalin::Shape &shape)
                                                        {
                                                          return totalVerts + shape.size();
                                                        }));

  size_t triangles = 0, squares = 0, rectangles = 0;
  triangles = lyalin::getTrianglesAmount(shapes);
  squares = lyalin::getSquaresAmount(shapes);
  rectangles = lyalin::getRectanglesAmount(shapes);

  lyalin::deletePentagons(shapes);

  std::vector<lyalin::Point> points = lyalin::makePointsVector(shapes);

  lyalin::sortContainer(shapes);

  std::cout << "Vertices: " << vertices << std::endl;
  std::cout << "Triangles: " << triangles << std::endl;
  std::cout << "Squares: " << squares << std::endl;
  std::cout << "Rectangles: " << rectangles << std::endl;

  std::cout << "Points:";
  for (const auto &i : points)
  {
    std::cout << " (" << i.x << "; " << i.y << ")";
  }
  std::cout << std::endl;

  std::cout << "Shapes:" << std::endl;
  for (const auto &shape : shapes)
  {
    std::cout << shape.size() << " ";
    for (const auto &i : shape)
    {
      std::cout << "(" << i.x << "; " << i.y << ") ";
    }
    std::cout << std::endl;
  }
}
