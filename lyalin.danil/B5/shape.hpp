#ifndef B5_SHAPE_HPP
#define B5_SHAPE_HPP
#include <vector>
#include <list>
#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>
#include <numeric>

namespace lyalin
{
  struct Point
  {
    int x, y;
  };

  using Shape = std::vector<Point>;
  using Shapes = std::list<Shape>;

  void fillShapeContainer(lyalin::Shapes &shapesContainer);

  size_t getTrianglesAmount(const lyalin::Shapes &shapesContainer) noexcept;

  size_t getSquaresAmount(const lyalin::Shapes &shapesContainer) noexcept;

  size_t getRectanglesAmount(const lyalin::Shapes &shapesContainer) noexcept;

  void deletePentagons(lyalin::Shapes &shapes) noexcept;

  std::vector<lyalin::Point> makePointsVector(const lyalin::Shapes &shapes) noexcept;

  void sortContainer(lyalin::Shapes &shapes);
}
#endif
