#ifndef UTILS_HPP
#define UTILS_HPP

#include <iostream>

namespace lyalin
{
  namespace values
  {
    const int MAX_SIZE = 20;
    const int MAX_VALUE = 20;
    const int MIN_VALUE = 1;
  }

  namespace funcs
  {
    template <typename Iterator>
    void printFrontBack(Iterator front, Iterator back)
    {
      if (front == back)
      {
        return;
      }
      back = std::prev(back);
      if (front == back)
      {
        std::cout << *front;
      }
      else
      {
        std::cout << *front << " " << *back << " ";
        if (std::next(front) != back)
        {
          printFrontBack(std::next(front), back);
        }
      }
    }
  }
}
#endif
