#include "tasks.hpp"

#include <list>

void lyalin::tasks::task1()
{
  funcs::queueWithPriority<std::string> queue;
  std::string line;

  while (std::getline(std::cin >> std::ws, line))
  {
    funcs::command_function<std::string> function = funcs::parse(line);
    function(queue);
  }
}

void lyalin::tasks::task2()
{
  std::list<int> list;

  int number = 0;
  while ((std::cin >> number) && list.size() <= lyalin::values::MAX_SIZE)
  {
    if (number >= lyalin::values::MIN_VALUE && number <= lyalin::values::MAX_VALUE)
    {
      list.push_back(number);
    }
    else
    {
      throw std::invalid_argument("List values must be from 0 to 20");
    }
  }

  if (list.size() > lyalin::values::MAX_SIZE)
  {
    throw std::invalid_argument("Invalid list size");
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios::failure("Input stream failed");
  }

  funcs::printFrontBack(list.begin(), list.end());
  std::cout << std::endl;
}
