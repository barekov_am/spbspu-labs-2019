#ifndef TASKS_HPP
#define TASKS_HPP

#include "parse-arguments.hpp"
#include "priorityqueue.hpp"
#include "utils.hpp"

namespace lyalin
{
  namespace tasks
  {
    void task1();
    void task2();
  }
}

#endif
