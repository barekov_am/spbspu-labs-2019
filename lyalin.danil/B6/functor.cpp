#include "functor.hpp"
#include <limits>

lyalin::Functor::Functor() noexcept
{
  max = std::numeric_limits<int>::min();
  min = std::numeric_limits<int>::max();
  first = 0;
  oddSum = 0;
  evenSum = 0;
  positive = 0;
  negative = 0;
  totalNums = 0;
  firstEqualLast = false;
}

void lyalin::Functor::operator()(const int &number) noexcept
{
  if (number > max)
  {
    max = number;
  }

  if (number < min)
  {
    min = number;
  }

  if (number > 0)
  {
    ++positive;
  }

  if (number < 0)
  {
    ++negative;
  }

  if (number & 1)
  {
    oddSum += number;
  }
  else
  {
    evenSum += number;
  }

  if (totalNums == 0)
  {
    first = number;
  }

  firstEqualLast = (first == number);

  ++totalNums;
}

std::ostream &lyalin::operator<<(std::ostream &stream, const lyalin::Functor &functor)
{
  if (functor.totalNums == 0)
  {
    stream << "No Data" << std::endl;
    return stream;
  }

  stream << "Max: " << functor.max << std::endl;
  stream << "Min: " << functor.min << std::endl;
  stream << "Mean: " << static_cast<long double>((functor.evenSum + functor.oddSum)) / functor.totalNums << std::endl;
  stream << "Positive: " << functor.positive << std::endl;
  stream << "Negative: " << functor.negative << std::endl;
  stream << "Odd Sum: " << functor.oddSum << std::endl;
  stream << "Even Sum: " << functor.evenSum << std::endl;
  stream << "First/Last Equal: ";
  if (functor.firstEqualLast)
  {
    stream << "yes" << std::endl;
  }
  else
  {
    stream << "no" << std::endl;
  }
  return stream;
}

