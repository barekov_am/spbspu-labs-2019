#ifndef B6_FUNCTOR_HPP
#define B6_FUNCTOR_HPP
#include <iostream>

namespace lyalin
{
  class Functor
  {
    public:
    Functor() noexcept;

    void operator()(const int &number) noexcept;
    friend std::ostream &operator<<(std::ostream &stream, const Functor &functor);

    private:
    long long int max;
    long long int min;
    long long int first;
    long long int oddSum;
    long long int evenSum;
    size_t positive;
    size_t negative;
    size_t totalNums;
    bool firstEqualLast;
  };

  std::ostream &operator<<(std::ostream &stream, const Functor &functor);
}
#endif
