#include <algorithm>
#include <iterator>
#include <functional>
#include "functor.hpp"

int main()
{
  try
  {
    lyalin::Functor functor;

    std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), std::ref(functor));

    if (!std::cin.eof())
    {
      std::cerr << "Invalid input";
      return 1;
    }

    std::cout << functor;
  }
  catch (std::exception &ex)
  {
    std::cout << ex.what();
  }
  return 0;
}
