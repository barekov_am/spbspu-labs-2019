#define BOOST_TEST_MODULE A2

#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(A2TestSuite)

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_CASE(rectangleUnalteredAfterMove)
{
  lyalin::Rectangle rectangle({0.0, 0.0}, 5.0, 15.0);
  const lyalin::rectangle_t rectFrameBeforeMoving = rectangle.getFrameRect();
  const double rectAreaBeforeMoving = rectangle.getArea();
  
  rectangle.move({5.0, 8.0});
  BOOST_CHECK_CLOSE(rectFrameBeforeMoving.width, rectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(rectFrameBeforeMoving.height, rectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(rectAreaBeforeMoving, rectangle.getArea(), ACCURACY);

  rectangle.move(10.0, -10.0);
  BOOST_CHECK_CLOSE(rectFrameBeforeMoving.width, rectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(rectFrameBeforeMoving.height, rectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(rectAreaBeforeMoving, rectangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(rectangleScaling)
{
  lyalin::Rectangle rectangle({1.3, 3.7}, 10.0, 20.0);
  const double rectAreaBeforeScaling = rectangle.getArea();
  const double scaleCoef = 2.0;
  rectangle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(rectangle.getArea(), scaleCoef * scaleCoef * rectAreaBeforeScaling, ACCURACY);
}

BOOST_AUTO_TEST_CASE(rectangleInvalidParameters)
{
  BOOST_CHECK_THROW(lyalin::Rectangle({3.0, 5.0}, 2.0, -5.0), std::invalid_argument);
  BOOST_CHECK_THROW(lyalin::Rectangle({2.0, 4.1}, -1.23, 3.21), std::invalid_argument);
  BOOST_CHECK_THROW(lyalin::Rectangle({5.0, 3.1}, -20.0, -21.0), std::invalid_argument);

  lyalin::Rectangle rectangle({3.0, 3.0}, 12.0, 12.0);
  BOOST_CHECK_THROW(rectangle.scale(-100.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circleUnalteredAfterMove)
{
  lyalin::Circle circle({1.0, 2.0}, 5.0);
  const lyalin::rectangle_t circFrameBeforeMove = circle.getFrameRect();
  const double circAreaBeforeMove = circle.getArea();

  circle.move(-1.0, 9.7);
  BOOST_CHECK_CLOSE(circFrameBeforeMove.width, circle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(circFrameBeforeMove.height, circle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(circAreaBeforeMove, circle.getArea(), ACCURACY);
  
  circle.move({2.5, 3.2});
  BOOST_CHECK_CLOSE(circFrameBeforeMove.width, circle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(circFrameBeforeMove.height, circle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(circAreaBeforeMove, circle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(circleScaling)
{
  lyalin::Circle circle({0.0, 0.0}, 10.0);
  const double circAreaBeforeScaling = circle.getArea();
  const double scaleCoef = 0.1;
  circle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(circle.getArea(), scaleCoef * scaleCoef * circAreaBeforeScaling, ACCURACY);
}

BOOST_AUTO_TEST_CASE(circleInvalidParameters)
{
  BOOST_CHECK_THROW(lyalin::Circle({0.0, 0.0}, -1.0), std::invalid_argument);
  lyalin::Circle circle({6.5, 4.3}, 2.1);
  BOOST_CHECK_THROW(circle.scale(-5.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
