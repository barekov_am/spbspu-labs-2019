#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"

int main()
{
  lyalin::Rectangle r1({1.3, 3.7}, 10.0, 20.0);
  std::cout << r1.getArea() << std::endl;
  r1.scale(2.0);
  std::cout << r1.getArea() << std::endl;

  lyalin::Rectangle r2({2.0, 1.5}, 2.0, 8.0);
  r2.move(1.5, 2.0);

  lyalin::Circle c1({0.0, 0.0}, 2.0);
  std::cout << c1.getArea() << std::endl;
  c1.scale(0.5);
  std::cout << c1.getArea() << std::endl;

  lyalin::Circle c2({6.0, 6.0}, 6.0);
  c2.move({-6.0, 6.0});

  lyalin::Shape *shapePtr = &c2;
  lyalin::rectangle_t tmpRect = shapePtr->getFrameRect();
  std::cout << tmpRect.pos.x << " " << tmpRect.pos.y << " ";
  std::cout << tmpRect.width << " " << tmpRect.height << std::endl;

  shapePtr = &r2;
  tmpRect = shapePtr->getFrameRect();
  std::cout << tmpRect.pos.x << " " << tmpRect.pos.y << " ";
  std::cout << tmpRect.width << " " << tmpRect.height << std::endl;

  return 0;
}
