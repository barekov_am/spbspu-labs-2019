#include "command-parser.hpp"

#include <iostream>
#include <sstream>
#include <algorithm>
#include "phoneBookComponent.hpp"

std::string readName(std::istringstream& stream)
{
  stream.ignore();
  std::string name;
  std::getline(stream, name);

  if (name.empty())
  {
    return name;
  }

  if ((name.front() != '\"') || (name.back() != '\"'))
  {
    throw lyalin::exceptions::invalidCommand();
  }

  name.erase(std::remove(name.begin(), name.end(), '\\'), name.end());
  name.erase(0, 1);
  name.pop_back();
  return name;
}

std::string readNumber(std::istringstream & stream)
{
  std::string number;
  stream >> number;
  for (auto i : number)
  {
    if (!isdigit(i))
    {
      throw lyalin::exceptions::invalidCommand();
    }
  }
  return number;
}

std::string readMark(std::istringstream & stream)
{
  std::string mark_name;
  stream >> mark_name;
  for (auto i : mark_name)
  {
    if ((!isalnum(i)) && (i != '-'))
    {
      throw lyalin::exceptions::invalidCommand();
    }
  }
  return mark_name;
}

lyalin::command_function lyalin::parse(const std::string & input)
{
  std::istringstream stream(input);
  std::string commandString;
  stream >> commandString;

  if (commandString == lyalin::values::ADD)
  {
    std::string number = readNumber(stream);
    std::string name = readName(stream);

    return[number, name](PhoneBook &book)
    {
      book.insertEnd(number, name);
    };
  }
  else if (commandString == lyalin::values::STORE)
  {
    std::string mark_name_old = readMark(stream);
    std::string mark_name_new = readMark(stream);

    return[mark_name_old, mark_name_new](PhoneBook &book)
    {
      book.add(mark_name_old, mark_name_new);
    };
  }
  else if (commandString == lyalin::values::INSERT)
  {
    std::string position;
    stream >> position;

    std::string mark_name = readMark(stream);
    std::string number = readNumber(stream);
    std::string name = readName(stream);

    if (position == lyalin::values::AFTER)
    {
      return[number, name, mark_name](PhoneBook &book)
      {
        book.insert(number, name,false , mark_name);
      };
    }
    else if (position == lyalin::values::BEFORE)
    {
      return[number, name, mark_name](PhoneBook &book)
      {
        book.insert(number, name,true , mark_name);
      };
    }
    else
    {
      throw lyalin::exceptions::invalidCommand();
    }
  }
  else if (commandString == lyalin::values::DELETE)
  {
    std::string mark_name = readMark(stream);

    return[mark_name](PhoneBook &book)
    {
      book.erase(mark_name);
    };
  }
  else if (commandString == lyalin::values::SHOW)
  {
    std::string mark_name = readMark(stream);

    return[mark_name](PhoneBook &book)
    {
      std::cout << book.get(mark_name).number << " " << book.get(mark_name).name << std::endl;
    };
  }
  else if (commandString == lyalin::values::MOVE)
  {
    std::string bookmark = readMark(stream),
       steps;
    stream >> steps;
    return [bookmark, steps](PhoneBook &book)
    {
      book.move(steps, bookmark);
    };
  }
  return [](PhoneBook &)
  {
    throw lyalin::exceptions::invalidCommand();
  };
}
