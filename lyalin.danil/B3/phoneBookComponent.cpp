#include "phoneBookComponent.hpp"

#include <sstream>

lyalin::PhoneBook::PhoneBook()
{
  bookmarks[lyalin::values::CURRENT_BOOKMARK] = book.end();
}

lyalin::PhoneBook::PhoneBook(const std::string &number, const std::string &name)
{
  bookmarks[lyalin::values::CURRENT_BOOKMARK] = book.end();
  insertEnd(number, name);
}

lyalin::PhoneBook::PhoneBook(const phonebookpage_t &page) : PhoneBook(page.number, page.name)
{

}

lyalin::PhoneBook::phonebookpage_t
lyalin::PhoneBook::get(const std::string &bookMark) const
{
  if (bookmarks.find(bookMark) == bookmarks.end())
  {
    throw lyalin::exceptions::invalidBookmark();
  }
  if (book.empty())
  {
    throw lyalin::exceptions::emptyBook();
  }
  return *bookmarks.at(bookMark);
}

void lyalin::PhoneBook::move(const std::string &steps, const std::string &bookMark)
{
  if (bookmarks.find(bookMark) == bookmarks.end())
  {
    throw lyalin::exceptions::invalidBookmark();
  }
  if (steps == "first")
  {
    bookmarks[bookMark] = book.begin();
    return;
  }
  if (steps == "last")
  {
    bookmarks[bookMark] = std::prev(book.end());
    return;
  }
  std::istringstream stream(steps);
  int distance = 0;
  stream >> distance;
  if (stream.fail())
  {
    throw lyalin::exceptions::invalidStep();
  }
  if (distance + static_cast<int>(std::distance(book.begin(), bookmarks[bookMark])) < 0
      || distance + +static_cast<int>(std::distance(book.begin(), bookmarks[bookMark])) >= static_cast<int>(book.size()))
  {
    return;
  }
  std::advance(bookmarks[bookMark], distance);
}

void lyalin::PhoneBook::insertEnd(const std::string &number, const std::string &name)
{
  lyalin::PhoneBook::phonebookpage_t page;
  page.number = number;
  page.name = name;
  book.push_back(page);
  if (book.size() == 1)
  {
    bookmarks[lyalin::values::CURRENT_BOOKMARK] = book.begin();
  }
}

void lyalin::PhoneBook::insert(const std::string &number, const std::string &name, const bool &before,
                               const std::string &bookMark)
{
  if (bookmarks.find(bookMark) == bookmarks.end())
  {
    throw lyalin::exceptions::invalidBookmark();
  }
  lyalin::PhoneBook::phonebookpage_t page;
  page.number = number;
  page.name = name;

  if (bookmarks[bookMark] == book.end())
  {
    book.push_back(page);
    bookmarks[bookMark] = std::prev(book.end());
  }
  else if (before)
  {
    book.insert(bookmarks[bookMark], page);
  }
  else
  {
    book.insert(std::next(bookmarks[bookMark]), page);
  }

}

void lyalin::PhoneBook::erase(const std::string &bookmark)
{
  if (bookmarks.find(bookmark) == bookmarks.end())
  {
    throw lyalin::exceptions::invalidBookmark();
  }
  auto current = getBookmark(bookmark);

  if (book.empty() || current == book.end())
  {
    throw std::invalid_argument("Phone book is empty");
  }

  for (auto &item : bookmarks)
  {
    if (item.second == current)
    {
      item.second = std::next(current);
    }
  }

  book.erase(std::prev(bookmarks[bookmark]));

  for (auto &item : bookmarks)
  {
    if (item.second == book.end())
    {
      item.second = std::prev(book.end());
    }
  }
}

void lyalin::PhoneBook::add(const std::string &bookMarkOld, const std::string &bookMarkNew)
{
  if (bookMarkNew == lyalin::values::CURRENT_BOOKMARK || bookmarks.find(bookMarkOld) == bookmarks.end())
  {
    throw lyalin::exceptions::invalidBookmark();
  }

  bookmarks[bookMarkNew] = bookmarks[bookMarkOld];
}

std::list<lyalin::PhoneBook::phonebookpage_t>::iterator lyalin::PhoneBook::getBookmark(const std::string &bookmark)
{
  if (bookmarks.find(bookmark) == bookmarks.end())
  {
    throw lyalin::exceptions::invalidBookmark();
  }

  if (book.empty())
  {
    throw lyalin::exceptions::emptyBook();
  }

  return bookmarks[bookmark];
}
