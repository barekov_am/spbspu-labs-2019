#include "factorial-iterator.hpp"
#include "factorial-container.hpp"

lyalin::FactorialIterator::FactorialIterator() : FactorialIterator(1)
{
}

lyalin::FactorialIterator::FactorialIterator(size_t position_) :
   position(position_),
   value(getValue(position))
{
}

const size_t *lyalin::FactorialIterator::operator->()
{
  return &value;
};

const size_t &lyalin::FactorialIterator::operator*()
{
  return value;
}

lyalin::FactorialIterator &lyalin::FactorialIterator::operator++()
{
  if (position < lyalin::FactorialContainer::MAX_ITERATOR_POSITION)
  {
    ++position;
    value *= position;
  }

  return *this;
}

lyalin::FactorialIterator lyalin::FactorialIterator::operator++(int)
{
  lyalin::FactorialIterator it = *this;
  ++(it);
  return it;
}

lyalin::FactorialIterator &lyalin::FactorialIterator::operator--()
{
  if (position > lyalin::FactorialContainer::MIN_ITERATOR_POSITION)
  {
    value /= position;
    --position;
  }

  return *this;
}

lyalin::FactorialIterator lyalin::FactorialIterator::operator--(int)
{
  lyalin::FactorialIterator it = *this;
  --(it);
  return it;
}

bool lyalin::FactorialIterator::operator==(const FactorialIterator &it)
{
  return (position == it.position);
}

bool lyalin::FactorialIterator::operator!=(const FactorialIterator &it)
{
  return !(*this == it);
}

size_t lyalin::FactorialIterator::getValue(size_t number)
{
  size_t result = 1;
  for (size_t i = 1; i <= number; i++)
  {
    result *= i;
  }
  return result;
}
