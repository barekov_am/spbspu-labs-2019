#include "tasks.hpp"

#include "factorial-container.hpp"
#include "command-parser.hpp"
#include "phoneBookComponent.hpp"
#include <algorithm>
#include <iostream>

void lyalin::tasks::task1()
{
  lyalin::PhoneBook book;
  std::string input_line;

  while (std::getline(std::cin, input_line))
  {
    try
    {
      lyalin::command_function command = lyalin::parse(input_line);
      command(book);
    }
    catch (std::invalid_argument& ex)
    {
      std::cerr << ex.what() << std::endl;
    }
    catch (std::exception& ex)
    {
      std::cout << ex.what() << std::endl;
    }
  }
}

void lyalin::tasks::task2()
{
  lyalin::FactorialContainer container;
  std::copy(container.begin(), container.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;
  std::reverse_copy(container.begin(), container.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;
}
