#ifndef B3_PHONEBOOKCOMPONENT_HPP
#define B3_PHONEBOOKCOMPONENT_HPP

#include <list>
#include <map>

namespace lyalin
{
  namespace values
  {
    const std::string ADD = "add";
    const std::string STORE = "store";
    const std::string INSERT = "insert";
    const std::string BEFORE = "before";
    const std::string AFTER = "after";
    const std::string DELETE = "delete";
    const std::string SHOW = "show";
    const std::string MOVE = "move";
    const std::string CURRENT_BOOKMARK = "current";
  }

  class PhoneBook
  {
    public:
    struct phonebookpage_t
      {
        std::string name, number;
      };

      PhoneBook();

      PhoneBook(const std::string &number, const std::string &name);

      PhoneBook(const phonebookpage_t &page);

      ~PhoneBook() = default;

      phonebookpage_t get(const std::string &bookMark = lyalin::values::CURRENT_BOOKMARK) const;

      void move(const std::string &steps, const std::string &bookMark = lyalin::values::CURRENT_BOOKMARK);

      void insert(const std::string &number, const std::string &name, const bool &before,
           const std::string &bookMark = lyalin::values::CURRENT_BOOKMARK);

      void insertEnd(const std::string &number, const std::string &name);

      void erase(const std::string &bookmark = lyalin::values::CURRENT_BOOKMARK);

      void add(const std::string &bookMarkOld, const std::string &bookMarkNew);
    private:
      std::list<phonebookpage_t> book;
      std::map<std::string, std::list<phonebookpage_t>::iterator> bookmarks;
      std::list<phonebookpage_t>::iterator getBookmark(const std::string &bookMark);
  };

  namespace exceptions
  {
    struct emptyBook : public std::exception
    {
      const char *what() const throw()
      {
        return "<EMPTY>";
      }
    };

    struct invalidStep : public std::exception
    {
      const char *what() const throw()
      {
        return "<INVALID STEP>";
      }
    };

    struct invalidBookmark : public std::exception
    {
      const char *what() const throw()
      {
        return "<INVALID BOOKMARK>";
      }
    };

    struct invalidCommand : public std::exception
    {
      const char *what() const throw()
      {
        return "<INVALID COMMAND>";
      }
    };
  }
}

#endif
