#include "factorial-container.hpp"

lyalin::FactorialIterator lyalin::FactorialContainer::begin()
{
  return lyalin::FactorialIterator(lyalin::FactorialContainer::MIN_ITERATOR_POSITION);
}

lyalin::FactorialIterator lyalin::FactorialContainer::end()
{
  return lyalin::FactorialIterator(lyalin::FactorialContainer::MAX_ITERATOR_POSITION);
}
