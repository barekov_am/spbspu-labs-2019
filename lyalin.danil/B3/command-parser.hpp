#ifndef B3_COMMAND_PARSER_HPP
#define B3_COMMAND_PARSER_HPP
#include <string>
#include <functional>
#include "phoneBookComponent.hpp"

namespace lyalin
{
  using command_function = std::function<void(lyalin::PhoneBook &book)>;
  command_function parse(const std::string &input);
}
#endif
