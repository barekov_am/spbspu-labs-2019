#include "task.hpp"

int main()
{
  try
  {
    lyalin::task();
  }
  catch(std::exception &exception)
  {
    std::cerr << exception.what();
    return 1;
  }
  return 0;
}
