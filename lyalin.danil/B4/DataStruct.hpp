#ifndef B4_DATASTRUCT_HPP
#define B4_DATASTRUCT_HPP

#include <iostream>
#include <vector>
#include <sstream>

namespace lyalin
{
  struct DataStruct
  {
    int key1;
    int key2;
    std::string str;
  };

  void readDataStructVector(std::vector<lyalin::DataStruct> &vector);

  void printDataStructVector(std::vector<lyalin::DataStruct> &vector);

  namespace values
  {
    const int KEY_MIN=-5;
    const int KEY_MAX=5;
    const int NUMBER_OF_ARGUMENTS=3;
  }
}
#endif
