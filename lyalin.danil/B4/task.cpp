#include "task.hpp"

bool cmpFunction(const lyalin::DataStruct &first, const lyalin::DataStruct &second)
{
  if (first.key1 < second.key1)
  {
    return true;
  }

  if (first.key1 == second.key1)
  {
    if (first.key2 < second.key2)
    {
      return true;
    }
    if (first.key2 == second.key2)
    {
      return first.str.length() < second.str.length();
    }
  }
  return false;
}

void lyalin::task()
{
  std::vector<lyalin::DataStruct> vector;
  lyalin::readDataStructVector(vector);
  std::sort(vector.begin(), vector.end(), cmpFunction);
  lyalin::printDataStructVector(vector);
}
