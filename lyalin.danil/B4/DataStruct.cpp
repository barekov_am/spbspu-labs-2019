#include "DataStruct.hpp"

lyalin::DataStruct stringToDataStruct(const std::string &line)
{
  std::istringstream is(line);

  int commas = 0;

  for (char i : line)
  {
    if (i == ',')
    {
      commas++;
    }
  }

  if (commas != (lyalin::values::NUMBER_OF_ARGUMENTS - 1))
  {
    throw std::invalid_argument("Invalid arguments");
  }

  int key1 = 0;
  int key2 = 0;

  char comma;
  is >> key1 >> comma >> key2 >> comma;

  std::string str = line.substr(line.find_last_of(',') + 1);

  if (is.fail())
  {
    throw std::ios::failure("Reading from string failed");
  }

  if (!(key1 >= lyalin::values::KEY_MIN && key1 <= lyalin::values::KEY_MAX)
      || !(key2 >= lyalin::values::KEY_MIN && key2 <= lyalin::values::KEY_MAX))
  {
    throw std::invalid_argument("Invalid key value");
  }

  return lyalin::DataStruct{key1, key2, str};
}

void lyalin::readDataStructVector(std::vector<lyalin::DataStruct> &vector)
{
  std::string line;
  while (std::getline(std::cin, line))
  {
    vector.push_back(stringToDataStruct(line));
  }
}

void lyalin::printDataStructVector(std::vector<lyalin::DataStruct> &vector)
{
  for (const auto &i : vector)
  {
    std::cout << i.key1 << "," << i.key2 << "," << i.str << std::endl;
  }
}

