#include "shape.hpp"

shapes::Shape::Shape(int x, int y) noexcept :
   x_(x),
   y_(y)
{
}


double shapes::Shape::getX() const noexcept
{
  return x_;
}

double shapes::Shape::getY() const noexcept
{
  return y_;
}

bool shapes::Shape::isMoreLeft(const std::shared_ptr<Shape> &shape) const noexcept
{
  return (this->x_ < shape->x_);
}

bool shapes::Shape::isUpper(const std::shared_ptr<Shape> &shape) const noexcept
{
  return (this->y_ > shape->y_);
}

