#ifndef B7_TRIANGLE_HPP
#define B7_TRIANGLE_HPP

#include "shape.hpp"

namespace shapes
{
  class Triangle : public Shape
  {
    public:
    Triangle(int x, int y);

    void draw(std::ostream &stream) const override;
  };
}

#endif
