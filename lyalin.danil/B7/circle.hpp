#ifndef B7_CIRCLE_HPP
#define B7_CIRCLE_HPP

#include "shape.hpp"

namespace shapes
{
  class Circle : public Shape
  {
    public:
    Circle(int x, int y);

    void draw(std::ostream &stream) const override;
  };
}


#endif
