#include "circle.hpp"

shapes::Circle::Circle(int x, int y) :
   Shape(x, y)
{
}

void shapes::Circle::draw(std::ostream &stream) const
{
  stream << "CIRCLE (" << getX() << ";" << getY() << ")" << std::endl;
}
