#ifndef B7_SQUARE_HPP
#define B7_SQUARE_HPP

#include "shape.hpp"

namespace shapes
{
  class Square : public Shape
  {
    public:
    Square(int x, int y);
    void draw(std::ostream &stream) const override;
  };
}

#endif
