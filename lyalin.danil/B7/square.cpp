#include "square.hpp"

shapes::Square::Square(int x, int y) :
   Shape(x, y)
{
}

void shapes::Square::draw(std::ostream &stream) const
{
  stream << "SQUARE (" << getX() << ";" << getY() << ")" << std::endl;
}
