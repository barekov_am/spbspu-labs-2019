#include <iostream>
#include <algorithm>
#include <cmath>
#include <iterator>
#include <functional>
#include <vector>
#include <sstream>
#include <fstream>

#include "triangle.hpp"
#include "square.hpp"
#include "circle.hpp"

#include "tasks.hpp"

void lyalin::task1()
{

  std::transform(std::istream_iterator<double>(std::cin), std::istream_iterator<double>(),
                 std::ostream_iterator<double>(std::cout, " "),
                 std::bind(std::multiplies<double>(), std::placeholders::_1, M_PI));

  if (!(std::cin.eof()))
  {
    throw (std::invalid_argument("Invalid input"));
  }
}

std::shared_ptr<shapes::Shape> readShape(const std::string &line)
{
  std::istringstream istream(line);
  std::string shapeType;
  int x = 0, y = 0;

  istream >> shapeType;
  istream.ignore(line.length(), ' ');
  istream >> x;
  istream.ignore(line.length(), ' ');
  istream >> y;
  istream.ignore(line.length(), ' ');

  if (istream.fail())
  {
    throw std::invalid_argument("Invalid input");
  }
  if (shapeType == "SQUARE")
  {
    return std::make_shared<shapes::Square>(shapes::Square(x, y));
  }
  else if (shapeType == "CIRCLE")
  {
    return std::make_shared<shapes::Circle>(shapes::Circle(x, y));
  }
  else if (shapeType == "TRIANGLE")
  {
    return std::make_shared<shapes::Triangle>(shapes::Triangle(x, y));
  }
  else
  {
    throw std::invalid_argument("Invalid shape type");
  }
}

void printShapeList(const std::vector<std::shared_ptr<shapes::Shape>> &shapes)
{
  std::for_each(shapes.begin(), shapes.end(),
                std::bind(&shapes::Shape::draw, std::placeholders::_1, std::ref(std::cout)));
}

void lyalin::task2()
{
  {
    std::vector<std::shared_ptr<shapes::Shape>> shapesPointers;

    std::string line;
    while (std::getline(std::cin, line))
    {
      line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());

      std::replace(line.begin(), line.end(), '(', ' ');
      std::replace(line.begin(), line.end(), ';', ' ');
      std::replace(line.begin(), line.end(), ')', ' ');
      if (line.empty())
      {
        continue;
      }
      shapesPointers.push_back(readShape(line));
    }

    std::cout << "Original:" << std::endl;
    printShapeList(shapesPointers);

    std::cout << "Left-Right:" << std::endl;
    std::sort(shapesPointers.begin(), shapesPointers.end(),
              [](const std::shared_ptr<shapes::Shape> &left, const std::shared_ptr<shapes::Shape> &right)
              {
                return left->isMoreLeft(right);
              });
    printShapeList(shapesPointers);

    std::cout << "Right-Left:" << std::endl;
    std::sort(shapesPointers.begin(), shapesPointers.end(),
              [](const std::shared_ptr<shapes::Shape> &left, const std::shared_ptr<shapes::Shape> &right)
              {
                return !left->isMoreLeft(right);
              });
    printShapeList(shapesPointers);

    std::cout << "Top-Bottom:" << std::endl;
    std::sort(shapesPointers.begin(), shapesPointers.end(),
              [](const std::shared_ptr<shapes::Shape> &left, const std::shared_ptr<shapes::Shape> &right)
              {
                return left->isUpper(right);
              });
    printShapeList(shapesPointers);

    std::cout << "Bottom-Top:" << std::endl;
    std::sort(shapesPointers.begin(), shapesPointers.end(),
              [](const std::shared_ptr<shapes::Shape> &left, const std::shared_ptr<shapes::Shape> &right)
              {
                return !left->isUpper(right);
              });
    printShapeList(shapesPointers);
  }
}
