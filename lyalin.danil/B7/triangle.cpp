#include "triangle.hpp"

shapes::Triangle::Triangle(int x, int y) :
   Shape(x, y)
{
}

void shapes::Triangle::draw(std::ostream &stream) const
{
  stream << "TRIANGLE (" << getX() << ";" << getY() << ")" << std::endl;
}
