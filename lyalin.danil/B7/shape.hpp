#ifndef B7_SHAPE_HPP
#define B7_SHAPE_HPP

#include <memory>
#include <iostream>

namespace shapes
{
  class Shape
  {
    public:
    Shape(int x, int y) noexcept;

    virtual ~Shape() = default;

    double getX() const noexcept;

    double getY() const noexcept;

    bool isMoreLeft(const std::shared_ptr<Shape> &shape) const noexcept;

    bool isUpper(const std::shared_ptr<Shape> &shape) const noexcept;

    virtual void draw(std::ostream &stream) const = 0;

    private:
    int x_;
    int y_;
  };
}

#endif
