#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
  lyalin::Rectangle r1({1.3, 3.7}, 10.0, 20.0);
  std::cout << "Rectangle area: " << r1.getArea() << '\n';
  r1.scale(2.0);
  std::cout << "Rectangle area after scaling: " << r1.getArea() << '\n';

  lyalin::Rectangle r2({2.0, 1.5}, 2.0, 8.0);
  r2.move(1.5, 2.0);

  lyalin::Circle c1({0.0, 0.0}, 2.0);
  std::cout << "Circle area: " << c1.getArea() << '\n';
  c1.scale(0.5);
  std::cout << "Circle area after scaling: " << c1.getArea() << '\n';

  lyalin::Circle c2({6.0, 6.0}, 6.0);
  c2.move({-6.0, 6.0});

  lyalin::Triangle t1({0.0, 0.0}, {3.0, 0.0}, {0.0, 4.0});
  std::cout << "Triangle area: " << t1.getArea() << '\n';
  t1.scale(20.0);
  std::cout << "Triangle area after scaling: " << t1.getArea() << '\n';

  lyalin::Shape *shapePtr = &c2;
  lyalin::rectangle_t tmpRect = shapePtr->getFrameRect();
  std::cout << "Abstract shape center: x = " << tmpRect.pos.x << ", y = " << tmpRect.pos.y << "\n";
  std::cout << "Abstract shape width is " << tmpRect.width << " and height is " << tmpRect.height << '\n';

  shapePtr = &r2;
  tmpRect = shapePtr->getFrameRect();
  std::cout << "New abstract shape center: x = " << tmpRect.pos.x << ", y = " << tmpRect.pos.y << "\n";
  std::cout << "New abstract shape width is " << tmpRect.width << " and height is " << tmpRect.height << '\n';

  std::cout << "Сomposite shape: " << '\n';
  lyalin::CompositeShape shapes1;
  shapes1.add(std::make_shared<lyalin::Rectangle>(r1));
  std::cout << "Area after adding rectangle: " << shapes1.getArea() << '\n';
  shapes1.add(std::make_shared<lyalin::Circle>(c1));
  std::cout << "Area after adding rectangle and circle: " << shapes1.getArea() << '\n';
  std::cout << "Center: x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';
  shapes1.move(1.3, 3.7);
  std::cout << "Center after moving: ";
  std::cout << "x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';
  shapes1.scale(10.0);
  std::cout << "Center after scaling: ";
  std::cout << "x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';
  shapes1.deleteShape(0);
  std::cout << "Area after deleting first shape: " << shapes1.getArea() << '\n';
  std::cout << "Center after deleting first shape: ";
  std::cout << "x = " << shapes1.getFrameRect().pos.x << ", y = " << shapes1.getFrameRect().pos.y << '\n';
  return 0;
}
