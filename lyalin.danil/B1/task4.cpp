#include "tasks.hpp"
#include <string>

void lyalin::tasks::taskFour(const char* parameter, const char* sizeChar)
{
  bool spaces = true;
  bool ascending;
  std::vector<double> vector;
  unsigned int vectorSize(0);
  try
  {
    vectorSize = std::stoi(sizeChar);
  }
  catch (...)
  {
    throw std::invalid_argument("Invalid size parameter");
  }

  if ((parameter != lyalin::funcs::ASCENDING) && (parameter != lyalin::funcs::DESCENDING))
  {
    throw std::invalid_argument("Invalid sorting parameter");
  }

  lyalin::funcs::fillRandomVector(vector, vectorSize);

  lyalin::funcs::printVector(vector, spaces);
  std::cout << std::endl;

  if (parameter == lyalin::funcs::ASCENDING)
  {
    ascending = true;
  }
  else
  {
    ascending = false;
  }
  lyalin::funcs::sortVector(vector, lyalin::funcs::getByIterator, ascending);
  lyalin::funcs::printVector(vector, spaces);
}
