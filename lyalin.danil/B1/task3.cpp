#include "tasks.hpp"

void lyalin::tasks::taskThree(std::istream &istream)
{
  bool spaces = true;
  std::vector<int> vector;

  int number = 0;
  bool foundZero = false;
  while (!(istream >> number).eof())
  {
    if (istream.fail() || istream.bad())
    {
      throw std::ios_base::failure("Invalid characters");
    }
    if (number != 0)
    {
      vector.push_back(number);
    }
    else
    {
      foundZero = true;
      break;
    }
  }
  if (!vector.empty() && foundZero)
  {
    std::vector<int>::iterator it;
    it = vector.end() - 1;

    if (*it == 1)
    {
      for (it = vector.begin(); it != vector.end();)
      {
        if (!((*it) % 2))
        {
          it = vector.erase(it);
        }
        else
        {
          ++it;
        }
      }
    }
    else if (*it == 2)
    {
      for (it = vector.begin(); it != vector.end(); ++it)
      {
        if (!((*it) % 3))
        {
          for(int i=0; i<3; i++)
          {
            it = vector.emplace(it + 1, 1);
          }
        }
      }
    }

    lyalin::funcs::printVector<int>(vector, spaces);
  }
  else if (!vector.empty() && !foundZero)
  {
    throw std::invalid_argument("Invalid number sequence: no zero found");
  }
  else if (vector.empty())
  {
    return;
  }

}
