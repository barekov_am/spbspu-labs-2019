#include <iostream>
#include "tasks.hpp"

int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    std::cerr << "Invalid number of arguments" << std::endl;
    return 1;
  }
  try {
    if (*argv[1] == '1' && argc == 3)
    {
      lyalin::tasks::taskOne(argv[2], std::cin);
    }
    else if (*argv[1] == '2' && argc == 3)
    {
      lyalin::tasks::taskTwo(argv[2]);
    }
    else if (*argv[1] == '3' && argc == 2)
    {
      lyalin::tasks::taskThree(std::cin);
    }
    else if (*argv[1] == '4' && argc == 4)
    {
      lyalin::tasks::taskFour(argv[2], argv[3]);
    }
    else
    {
      throw std::invalid_argument("Invalid arguments");
    }
  }

  catch (const std::exception &exc)
  {
    std::cerr << exc.what() << std::endl;
    return 1;
  }
  return 0;
}
