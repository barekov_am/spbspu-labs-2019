#ifndef FUNCS_HPP
#define FUNCS_HPP

#include <iostream>
#include <vector>
#include <iterator>
#include <fstream>
#include <stdexcept>

namespace lyalin
{
  namespace funcs
  {

    const std::string ASCENDING = "ascending";
    const std::string DESCENDING = "descending";

    template<typename T>
    void printVector(std::vector<T> vector, bool spaces)
    {
      if (spaces)
      {
        copy(vector.begin(),
             vector.end(),
             std::ostream_iterator<T>(std::cout, " ")
        );
      }
      else
      {
        copy(vector.begin(),
             vector.end(),
             std::ostream_iterator<T>(std::cout, "")
        );
      }
    }
    inline void fillRandomArray(double * array, int size)
    {
      for (int i = 0; i < size; i++)
      {
        array[i] = (rand() % 10 + 1);
        array[i] /= 10;
        if ((rand() % 2) == 1)
        {
          array[i] *= -1;
        }
      }
    }
    inline void fillRandomVector(std::vector<double> &vector, int size)
    {
      double *arrayToFill = new double[size];
      lyalin::funcs::fillRandomArray(arrayToFill, size);
      for (int i(0); i < size; i++)
      {
        vector.push_back(arrayToFill[i]);
      }
      delete[]arrayToFill;
    }

    template <typename T>
    typename T::value_type getByBrackets(const T& vector, const typename T::iterator& it)
    {
      size_t index = std::distance<typename T::const_iterator>(vector.begin(), it);

      if(index <= vector.size())
      {
        return vector[index];
      }
      else
      {
        throw std::out_of_range("Index out of range");
      }
    }

    template <typename T>
    typename T::value_type getByAt(const T& vecT, const typename T::iterator& it) noexcept
    {
      long index = std::distance<typename T::const_iterator>(vecT.begin(), it);
      return vecT.at(index);
    }

    template <typename T>
    typename T::value_type getByIterator(const T&, const typename T::iterator& it) noexcept
    {
      return *it;
    }

    template <typename T>
    void sortVector(T& vector,
                  typename T::value_type(&get_elem)(const T&, const typename T::iterator&),
                  bool ascending) noexcept
    {
      using iterator_type = typename T::iterator;
      for (iterator_type it1 = vector.begin(); it1 != vector.end(); ++it1)
      {
        for (iterator_type it2 = std::next(it1); it2 != vector.end(); ++it2)
        {
          if ((get_elem(vector, it1) > get_elem(vector, it2)) == ascending)
          {
            std::swap(*it1, *it2);
          }
        }
      }
    }
  }
}
#endif
