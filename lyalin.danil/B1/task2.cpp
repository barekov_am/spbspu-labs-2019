#include "tasks.hpp"
#include <memory>
#include <vector>

const std::size_t initialSize = 1024;

void lyalin::tasks::taskTwo(char* fileName)
{
  bool spaces = false;

  std::ifstream fin(fileName);

  if (!fin)
  {
    throw std::invalid_argument("Invalid file path");
  }

  std::size_t size = initialSize;

  std::unique_ptr<char[], decltype(&free)> array(static_cast<char*>(malloc(size)), &free);

  if (!array)
  {
    throw std::runtime_error("Error in memory allocation");
  }

  std::size_t index = 0;
  while (fin)
  {
    fin.read(&array[index], initialSize);
    index += fin.gcount();
    if (fin.gcount() == initialSize)
    {
      size += initialSize;
      std::unique_ptr<char[], decltype(&free)> newArray(static_cast<char*>(realloc(array.get(), size)), &free);
      if (!newArray)
      {
        throw std::runtime_error("Could not allocate memory for new characters");
      }
      array.release();
      std::swap(array, newArray);
    }
    if (!fin.eof() && fin.fail())
    {
      throw std::runtime_error("Character reading failed");
    }
  }
  std::vector<char> vector(&array[0], &array[index]);

  lyalin::funcs::printVector<char>(vector, spaces);
}
