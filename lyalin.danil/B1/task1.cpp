#include "tasks.hpp"

void lyalin::tasks::taskOne(const char *parameter, std::istream &istream){
  const bool spaces = true;

  std::vector<int> vector, vectorDefault;
  int number;
  while (istream >> number)
  {
    vector.insert(vector.end(), number);
  }
  if (!istream.eof())
  {
    throw std::ios_base::failure("Invalid characters");
  }

  vectorDefault = vector;

  if (!vector.empty())
  {
    bool ascending;

    if (parameter == lyalin::funcs::ASCENDING)
    {
      ascending = true;

    }
    else if (parameter == lyalin::funcs::DESCENDING)
    {
      ascending = false;
    }
    else
    {
      throw std::invalid_argument("Invalid sorting parameter");
    }

    lyalin::funcs::sortVector(vector, lyalin::funcs::getByBrackets, ascending);
    lyalin::funcs::printVector<int>(vector, spaces);
    std::cout << std::endl;

    vector = vectorDefault;

    lyalin::funcs::sortVector(vector, lyalin::funcs::getByAt, ascending);
    lyalin::funcs::printVector<int>(vector, spaces);
    std::cout << std::endl;

    vector = vectorDefault;

    lyalin::funcs::sortVector(vector, lyalin::funcs::getByIterator, ascending);
    lyalin::funcs::printVector<int>(vector, spaces);
  }
  else
  {
    if ((parameter == lyalin::funcs::ASCENDING) || (parameter == lyalin::funcs::DESCENDING))
    {
      return;
    }
    else
    {
      throw std::invalid_argument("Invalid sorting parameter");
    }
  }
}
