#ifndef TASKS_HPP
#define TASKS_HPP

#include "funcs.hpp"

namespace lyalin
{
  namespace tasks
  {
    void taskOne(const char* parameter, std::istream &istream);

    void taskTwo(char* fileName);

    void taskThree(std::istream &istream);

    void taskFour(const char* parameter, const char* sizeChar);
  }
}
#endif
