#ifndef COMPOSITE_SHAPE_HPP_INCLUDED
#define COMPOSITE_SHAPE_HPP_INCLUDED

#include <memory>
#include "shape.hpp"

namespace lyalin
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();

    CompositeShape(const CompositeShape &copyShape);

    CompositeShape(CompositeShape &&deleteShape);

    CompositeShape(const lyalin::Shape::shape_ptr &firstShape);

    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape &composShape);

    CompositeShape& operator =(CompositeShape &&composShape);

    lyalin::Shape::shape_ptr operator [](std::size_t index) const;

    double getArea() const override;

    rectangle_t getFrameRect() const override;

    void move(double dx, double dy) override;

    void move(const point_t &point) override;

    void scale(double scaleCoef) override;

    std::size_t size() const;

    void add(const lyalin::Shape::shape_ptr &newShape);

    void deleteShape(std::size_t index);

    void rotate(double angle) override;

  private:
    std::size_t size_;
    lyalin::Shape::array_ptr shapes_;
  };
}
#endif
