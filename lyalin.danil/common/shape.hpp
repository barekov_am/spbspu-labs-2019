#ifndef A2_SHAPE_HPP
#define A2_SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace lyalin
{
  class Shape
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;

    using array_ptr = std::unique_ptr<shape_ptr[]>;

    virtual ~Shape() = default;

    virtual double getArea() const = 0;

    virtual rectangle_t getFrameRect() const = 0;

    virtual void move(double dx, double dy) = 0;

    virtual void move(const point_t &position) = 0;

    virtual void scale(double scaleCoef) = 0;

    virtual void rotate(double) = 0;
  };
}
#endif
