#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(CompositeShapeTestSuite)

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_CASE(compositeShapeUnalteredAfterMove)
{
  lyalin::Rectangle rectangle({0.0, 0.0}, 5.0, 15.0);
  lyalin::Circle circle({1.0, 2.0}, 5.0);
  lyalin::CompositeShape shapes(std::make_shared<lyalin::Rectangle>(rectangle));
  shapes.add(std::make_shared<lyalin::Circle>(circle));
  const lyalin::rectangle_t shapesFrameBeforeMoving = shapes.getFrameRect();
  const double shapesAreaBeforeMoving = shapes.getArea();

  shapes.move({5.0, 8.0});
  BOOST_CHECK_CLOSE(shapesFrameBeforeMoving.width, shapes.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(shapesFrameBeforeMoving.height, shapes.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(shapesAreaBeforeMoving, shapes.getArea(), ACCURACY);

  shapes.move(10.0, -10.0);
  BOOST_CHECK_CLOSE(shapesFrameBeforeMoving.width, shapes.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(shapesFrameBeforeMoving.height, shapes.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(shapesAreaBeforeMoving, shapes.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaling)
{
  lyalin::Rectangle rectangle({0.0, 0.0}, 5.0, 15.0);
  lyalin::Circle circle({1.0, 2.0}, 5.0);
  lyalin::CompositeShape shapes(std::make_shared<lyalin::Rectangle>(rectangle));
  shapes.add(std::make_shared<lyalin::Circle>(circle));
  const double shapesAreaBeforeScaling = shapes.getArea();
  const double scaleCoef = 2.0;
  shapes.scale(scaleCoef);
  BOOST_CHECK_CLOSE(shapes.getArea(), scaleCoef * scaleCoef * shapesAreaBeforeScaling, ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeInvalidParameters)
{
  lyalin::CompositeShape shapes1;
  BOOST_CHECK_THROW(shapes1.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(shapes1.getArea(), std::logic_error);
  BOOST_CHECK_THROW(shapes1.scale(100.0), std::logic_error);
  BOOST_CHECK_THROW(shapes1.move(0.0, 0.0), std::logic_error);
  BOOST_CHECK_THROW(shapes1.move({0.0, 0.0}), std::logic_error);

  lyalin::Rectangle rectangle({0.0, 0.0}, 5.0, 15.0);
  lyalin::CompositeShape shapes2(std::make_shared<lyalin::Rectangle>(rectangle));
  BOOST_CHECK_THROW(shapes2.add(nullptr), std::invalid_argument);

  lyalin::Circle circle({1.0, 2.0}, 5.0);
  shapes2.add(std::make_shared<lyalin::Circle>(circle));
  BOOST_CHECK_THROW(shapes2.scale(-100.0), std::invalid_argument);
  BOOST_CHECK_THROW(shapes2.deleteShape(-100), std::out_of_range);
  BOOST_CHECK_THROW(shapes2.deleteShape(100), std::out_of_range);
  BOOST_CHECK_THROW(shapes2[-100], std::out_of_range);
  BOOST_CHECK_THROW(shapes2[100], std::out_of_range);

  BOOST_CHECK_THROW(lyalin::CompositeShape shapes3(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShapeOperatorTests)
{
  lyalin::Rectangle rectangle1({0.0, 0.0}, 5.0, 15.0);
  lyalin::CompositeShape shapes1(std::make_shared<lyalin::Rectangle>(rectangle1));

  BOOST_CHECK_NO_THROW(lyalin::CompositeShape shapes2(shapes1));
  BOOST_CHECK_NO_THROW(lyalin::CompositeShape shapes2(std::move(shapes1)));

  lyalin::Circle circle({1.0, 2.0}, 5.0);
  lyalin::CompositeShape shapes2(std::make_shared<lyalin::Circle>(circle));

  BOOST_CHECK_NO_THROW(shapes1 = shapes2);
  BOOST_CHECK_NO_THROW(shapes1 = std::move(shapes2));

  lyalin::Rectangle rectangle2({1.3, 3.7}, 10.0, 20.0);
  shapes1.add(std::make_shared<lyalin::Rectangle>(rectangle2));
  const lyalin::rectangle_t rectFrame = rectangle2.getFrameRect();
  const double rectArea = rectangle2.getArea();

  BOOST_CHECK_CLOSE(rectFrame.width, shapes1[1]->getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(rectFrame.height, shapes1[1]->getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(rectArea, shapes1[1]->getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeShapeRotation)
{
  lyalin::Rectangle rectangle({0.0, 0.0}, 5.0, 15.0);
  lyalin::Circle circle({1.0, 2.0}, 5.0);
  lyalin::CompositeShape testShapes(std::make_shared<lyalin::Rectangle>(rectangle));
  testShapes.add(std::make_shared<lyalin::Circle>(circle));
  double areaBefore = testShapes.getArea();

  double angle = -90.00;
  BOOST_CHECK_NO_THROW(testShapes.rotate(angle));

  angle = 90.00;
  BOOST_CHECK_NO_THROW(testShapes.rotate(angle));

  angle = -45.5;
  testShapes.rotate(angle);
  BOOST_CHECK_CLOSE(testShapes.getArea(), areaBefore, ACCURACY);

  angle = 15.35;
  testShapes.rotate(angle);
  BOOST_CHECK_CLOSE(testShapes.getArea(), areaBefore, ACCURACY);

  angle = 361.0;
  testShapes.rotate(angle);
  BOOST_CHECK_CLOSE(testShapes.getArea(), areaBefore, ACCURACY);

  angle = -1000.1;
  testShapes.rotate(angle);
  BOOST_CHECK_CLOSE(testShapes.getArea(),areaBefore, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
