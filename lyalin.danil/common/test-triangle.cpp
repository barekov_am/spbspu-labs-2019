#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(TriangleTestSuite)

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_CASE(triangleUnalteredAfterMove)
{
  lyalin::Triangle triangle({0.0, 0.0}, {8.0, 4.0}, {2.0, 6.0});
  const lyalin::rectangle_t triFrameBeforeMove = triangle.getFrameRect();
  const double triAreaBeforeMove = triangle.getArea();

  triangle.move(-1.0, 9.7);
  BOOST_CHECK_CLOSE(triFrameBeforeMove.width, triangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(triFrameBeforeMove.height, triangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(triAreaBeforeMove, triangle.getArea(), ACCURACY);

  triangle.move({2.5, 3.2});
  BOOST_CHECK_CLOSE(triFrameBeforeMove.width, triangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(triFrameBeforeMove.height, triangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(triAreaBeforeMove, triangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(triangleScaling)
{
  lyalin::Triangle triangle({0.0, 0.0}, {8.0, 4.0}, {2.0, 6.0});
  const double triangleAreaBeforeScaling = triangle.getArea();
  const double scaleCoef = 0.1;
  triangle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(triangle.getArea(), scaleCoef * scaleCoef * triangleAreaBeforeScaling, ACCURACY);
}

BOOST_AUTO_TEST_CASE(triangleInvalidParameters)
{
  BOOST_CHECK_THROW(lyalin::Triangle({2.0, 2.0}, {2.0, 2.0}, {20.0, 20.0}), std::invalid_argument);
  lyalin::Triangle circle({0.0, 0.0}, {8.0, 4.0}, {2.0, 6.0});
  BOOST_CHECK_THROW(circle.scale(-5.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(triangleRotation)
{
  lyalin::Triangle triangle({0.0, 0.0}, {8.0, 4.0}, {2.0, 6.0});
  double triangleAreaBeforeRotating = triangle.getArea();

  double angle = -90.00;
  BOOST_CHECK_NO_THROW(triangle.rotate(angle));

  angle = 90.00;
  BOOST_CHECK_NO_THROW(triangle.rotate(angle));

  angle = -45.5;
  triangle.rotate(angle);
  BOOST_CHECK_CLOSE(triangle.getArea(), triangleAreaBeforeRotating, ACCURACY);

  angle = 15.35;
  triangle.rotate(angle);
  BOOST_CHECK_CLOSE(triangle.getArea(), triangleAreaBeforeRotating, ACCURACY);

  angle = 361.0;
  triangle.rotate(angle);
  BOOST_CHECK_CLOSE(triangle.getArea(), triangleAreaBeforeRotating, ACCURACY);

  angle = -1000.1;
  triangle.rotate(angle);
  BOOST_CHECK_CLOSE(triangle.getArea(), triangleAreaBeforeRotating, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
