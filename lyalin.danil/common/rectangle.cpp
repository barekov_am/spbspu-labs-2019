#include "rectangle.hpp"

#include <stdexcept>
#include <cmath>

lyalin::Rectangle::Rectangle(const point_t &position, double width, double height) :
  center_(position),
  width_(width),
  height_(height),
  angle_(0)
{
  if ((width_ <= 0.0) || (height_ <= 0.0))
  {
    throw std::invalid_argument("Rectangle: some parameters are invalid.");
  }
}

lyalin::Rectangle::Rectangle(const point_t &position, double width, double height, double angle) :
  Rectangle(position,width,height)
{
  rotate(angle);
}

double lyalin::Rectangle::getArea() const
{
  return width_ * height_;
}

lyalin::rectangle_t lyalin::Rectangle::getFrameRect() const
{
  const double sinus = sin(angle_ * M_PI / 180);
  const double cosinus = cos(angle_ * M_PI / 180);
  const double width = height_ * fabs(sinus) + width_ * fabs(cosinus);
  const double height = height_ * fabs(cosinus) + width_ * fabs(sinus);

  return {width, height, center_};
}

void lyalin::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void lyalin::Rectangle::move(const point_t &position)
{
  center_ = position;
}

void lyalin::Rectangle::scale(double scaleCoef)
{
  if (scaleCoef <= 0.0)
  {
    throw std::invalid_argument("Rectangle: scaling parameter is invalid");
  }
  else
  {
    width_ *= scaleCoef;
    height_ *= scaleCoef;
  }
}
void lyalin::Rectangle::rotate(double angle)
{
  while (angle >= 360.0)
  {
    angle -= 360.0;
  }
  angle_ += angle;
}
