#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "separation.hpp"
#include "matrix.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(matrixTestSuite)

  BOOST_AUTO_TEST_CASE(copyAndMoveConstructor)
  {
    lyalin::Rectangle rectangle({0.0, 0.0}, 5.0, 15.0);
    lyalin::Circle circle({1.0, 2.0}, 5.0);
    lyalin::CompositeShape shapes(std::make_shared<lyalin::Rectangle>(rectangle));
    shapes.add(std::make_shared<lyalin::Circle>(circle));

    lyalin::Matrix<lyalin::Shape> matrix = lyalin::layer(shapes);

    BOOST_CHECK_NO_THROW(lyalin::Matrix<lyalin::Shape> matrix2(matrix));
    BOOST_CHECK_NO_THROW(lyalin::Matrix<lyalin::Shape> matrix3(std::move(matrix)));
  }

  BOOST_AUTO_TEST_CASE(operatorTests)
  {
    lyalin::Rectangle r1({0.0, 0.0}, 5.0, 15.0);
    lyalin::Circle c1({1.0, 2.0}, 5.0);
    lyalin::CompositeShape testShapes1(std::make_shared<lyalin::Rectangle>(r1));
    testShapes1.add(std::make_shared<lyalin::Circle>(c1));

    lyalin::Matrix<lyalin::Shape> matrix1 = lyalin::layer(testShapes1);

    BOOST_CHECK_THROW(matrix1[100], std::out_of_range);
    BOOST_CHECK_THROW(matrix1[-1], std::out_of_range);

    BOOST_CHECK_NO_THROW(lyalin::Matrix<lyalin::Shape> matrix2 = matrix1);
    BOOST_CHECK_NO_THROW(lyalin::Matrix<lyalin::Shape> matrix3 = std::move(matrix1));

    lyalin::Rectangle r2({-1.0, -1.0}, 4.0, 4.0);
    lyalin::CompositeShape testShapes2(std::make_shared<lyalin::Rectangle>(r2));

    lyalin::Matrix<lyalin::Shape> matrix4 = lyalin::layer(testShapes2);
    lyalin::Matrix<lyalin::Shape> matrix5 = lyalin::layer(testShapes2);

    BOOST_CHECK(matrix4 == matrix5);

    BOOST_CHECK(matrix4 != matrix1);
    BOOST_CHECK(matrix5 != matrix1);
  }

BOOST_AUTO_TEST_SUITE_END()
