#include "composite-shape.hpp"

#include <iostream>
#include <stdexcept>
#include <cmath>

lyalin::CompositeShape::CompositeShape() :
  size_(0),
  shapes_(nullptr)
{
}

lyalin::CompositeShape::CompositeShape(const lyalin::CompositeShape &copyShape) :
  size_(copyShape.size_),
  shapes_(std::make_unique<std::shared_ptr<lyalin::Shape>[]>(copyShape.size_))
{
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i] = copyShape.shapes_[i];
  }
}

lyalin::CompositeShape::CompositeShape(CompositeShape &&deleteShape) :
  size_(deleteShape.size_),
  shapes_(std::move(deleteShape.shapes_))
{
  deleteShape.size_ = 0;
}

lyalin::CompositeShape::CompositeShape(const lyalin::Shape::shape_ptr &firstShape) :
  size_(1),
  shapes_(std::make_unique<std::shared_ptr<lyalin::Shape>[]>(1))
{
  if (firstShape == nullptr)
  {
    throw std::invalid_argument("Composite Shape:Error in creation");
  }
  shapes_[0] = firstShape;
}

lyalin::CompositeShape &lyalin::CompositeShape::operator =(const CompositeShape &composShape)
{
  if (this != &composShape)
  {
    size_ = composShape.size_;
    shapes_ = std::make_unique<lyalin::Shape::shape_ptr []>(composShape.size_);
    for (size_t i = 0; i < size_; ++i)
    {
      shapes_[i] = composShape.shapes_[i];
    }
  }
  return *this;
}

lyalin::CompositeShape &lyalin::CompositeShape::operator =(lyalin::CompositeShape &&composShape)
{
  if (this != &composShape)
  {
    size_ = composShape.size_;
    shapes_ = std::move(composShape.shapes_);
  }
  return *this;
}

lyalin::Shape::shape_ptr lyalin::CompositeShape::operator[](std::size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Composite Shape:Index out of range");
  }
  return shapes_[index];
}

double lyalin::CompositeShape::getArea() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite Shape:Shape is empty");
  }

  double area = 0.0;
  for (size_t i = 0; i < size_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

lyalin::rectangle_t lyalin::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite Shape:Shape is empty");
  }
  rectangle_t rectangle = shapes_[0]->getFrameRect();
  double minX = rectangle.pos.x - rectangle.width / 2;
  double maxX = rectangle.pos.x + rectangle.width / 2;
  double minY = rectangle.pos.y - rectangle.height / 2;
  double maxY = rectangle.pos.y + rectangle.height / 2;

  for (size_t i = 1; i < size_; i++)
  {
    rectangle = shapes_[i]->getFrameRect();

    minX = std::min(rectangle.pos.x - rectangle.width / 2, minX);
    minY = std::min(rectangle.pos.y - rectangle.height / 2, minY);
    maxX = std::max(rectangle.pos.x + rectangle.width / 2, maxX);
    maxY = std::max(rectangle.pos.y + rectangle.height / 2, maxY);
  }
  return {(maxX - minX), (maxY - minY), {(minX + maxX) / 2, (minY + maxY) / 2}};
}

void lyalin::CompositeShape::move(double dx, double dy)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite Shape:Shape is empty");
  }
  for (size_t i = 0; i < size_; ++i)
  {
    shapes_[i]->move(dx, dy);
  }
}

void lyalin::CompositeShape::move(const point_t &point)
{
  rectangle_t frame = getFrameRect();
  double dx = point.x - frame.pos.x;
  double dy = point.y - frame.pos.y;
  move(dx, dy);
}

void lyalin::CompositeShape::scale(double scaleCoef)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite Shape:Shape is empty");
  }

  if (scaleCoef < 0)
  {
    throw std::invalid_argument("Composite Shape:invalid scaling parameter");
  }

  point_t center = getFrameRect().pos;
  for (size_t i = 0; i < size_; ++i)
  {
    lyalin::point_t shapeCenter = shapes_[i]->getFrameRect().pos;
    shapes_[i]->move((shapeCenter.x - center.x) * (scaleCoef - 1), (shapeCenter.y - center.y) * (scaleCoef - 1));
    shapes_[i]->scale(scaleCoef);
  }
}

size_t lyalin::CompositeShape::size() const
{
  return size_;
}

void lyalin::CompositeShape::add(const lyalin::Shape::shape_ptr &newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Composite Shape:Error in adding shape");
  }

  lyalin::Shape::array_ptr newShapeList(std::make_unique<std::shared_ptr<lyalin::Shape>[]>(size_ + 1));
  for (size_t i = 0; i < size_; ++i)
  {
    newShapeList[i] = shapes_[i];
  }

  newShapeList[size_] = newShape;
  size_++;
  shapes_.swap(newShapeList);
}

void lyalin::CompositeShape::deleteShape(size_t index)
{
  if (index >= size_)
  {
    throw std::out_of_range("Composite Shape:Index out of range");
  }
  for (size_t i = index; i < size_ - 1; ++i)
  {
    shapes_[i] = shapes_[i + 1];
  }
  shapes_[size_ - 1] = nullptr;
  size_--;
}

void lyalin::CompositeShape::rotate(double angle)
{
  const lyalin::point_t center = getFrameRect().pos;
  const double sinus = std::sin(angle * M_PI / 180);
  const double cosinus = std::cos(angle * M_PI / 180);

  for (size_t i = 0; i < size_; ++i)
  {
    const lyalin::point_t shapeCenter = shapes_[i]->getFrameRect().pos;
    const double dx = shapeCenter.x - center.x;
    const double dy = shapeCenter.y - center.y;

    shapes_[i]->move(dx * (cosinus - 1) - dy * sinus, dx * sinus + dy * (cosinus - 1));
    shapes_[i]->rotate(angle);
  }
}
