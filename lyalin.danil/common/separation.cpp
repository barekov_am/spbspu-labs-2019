#include "separation.hpp"
#include <cmath>

bool lyalin::cross(const lyalin::Shape &firstShape, const lyalin::Shape &secondShape)
{
  const lyalin::rectangle_t firstFrame = firstShape.getFrameRect();
  const lyalin::rectangle_t secondFrame = secondShape.getFrameRect();

  if (std::abs(firstFrame.pos.x - secondFrame.pos.x) > (firstFrame.width + secondFrame.width) / 2)
  {
    return false;
  }

  if (std::abs(firstFrame.pos.y - secondFrame.pos.y) > (firstFrame.height + secondFrame.height) / 2)
  {
    return false;
  }

  return true;
}

lyalin::Matrix<lyalin::Shape> lyalin::layer(const lyalin::CompositeShape &compositeShape)
{
  lyalin::Matrix<lyalin::Shape> matrix;

  for (size_t i = 0; i < compositeShape.size(); ++i)
  {
    size_t rowsCount = 0;
    size_t columnsCount = 0;

    for (size_t j = matrix.getRows(); j-- > 0;)
    {
      bool between = false;
      for (size_t k = 0; k < matrix.getColumns(); ++k)
      {
        if (matrix[j][k] == nullptr)
        {
          columnsCount = k;
          break;
        }

        if (cross(*compositeShape[i], *matrix[j][k]))
        {
          rowsCount = j + 1;
          between = true;
          break;
        }

        if (k == matrix.getColumns() - 1)
        {
          columnsCount = k + 1;
        }
      }

      if (between)
      {
        break;
      }
    }
    matrix.add(compositeShape[i], rowsCount, columnsCount);
  }
  return matrix;
}
