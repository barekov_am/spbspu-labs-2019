#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(CircleTestSuite)

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_CASE(circleUnalteredAfterMove)
{
  lyalin::Circle circle({1.0, 2.0}, 5.0);
  const lyalin::rectangle_t circFrameBeforeMove = circle.getFrameRect();
  const double circAreaBeforeMove = circle.getArea();

  circle.move(-1.0, 9.7);
  BOOST_CHECK_CLOSE(circFrameBeforeMove.width, circle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(circFrameBeforeMove.height, circle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(circAreaBeforeMove, circle.getArea(), ACCURACY);

  circle.move({2.5, 3.2});
  BOOST_CHECK_CLOSE(circFrameBeforeMove.width, circle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(circFrameBeforeMove.height, circle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(circAreaBeforeMove, circle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(circleScaling)
{
  lyalin::Circle circle({0.0, 0.0}, 10.0);
  const double circAreaBeforeScaling = circle.getArea();
  const double scaleCoef = 0.1;
  circle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(circle.getArea(), scaleCoef * scaleCoef * circAreaBeforeScaling, ACCURACY);
}

BOOST_AUTO_TEST_CASE(circleInvalidParameters)
{
  BOOST_CHECK_THROW(lyalin::Circle({0.0, 0.0}, -1.0), std::invalid_argument);
  lyalin::Circle circle({6.5, 4.3}, 2.1);
  BOOST_CHECK_THROW(circle.scale(-5.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circleRotation)
{
  lyalin::Circle circle({0, 0}, 10);
  double circAreaBeforeRotating = circle.getArea();

  double angle = -90.00;
  BOOST_CHECK_NO_THROW(circle.rotate(angle));

  angle = 90.00;
  BOOST_CHECK_NO_THROW(circle.rotate(angle));

  angle = -45.5;
  circle.rotate(angle);
  BOOST_CHECK_CLOSE(circle.getArea(), circAreaBeforeRotating, ACCURACY);

  angle = 15.35;
  circle.rotate(angle);
  BOOST_CHECK_CLOSE(circle.getArea(), circAreaBeforeRotating, ACCURACY);

  angle = 361.0;
  circle.rotate(angle);
  BOOST_CHECK_CLOSE(circle.getArea(), circAreaBeforeRotating, ACCURACY);

  angle = -1000.1;
  circle.rotate(angle);
  BOOST_CHECK_CLOSE(circle.getArea(), circAreaBeforeRotating, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
