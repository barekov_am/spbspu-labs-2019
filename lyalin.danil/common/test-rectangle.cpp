#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(RectangleTestSuite)

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_CASE(rectangleUnalteredAfterMove)
{
  lyalin::Rectangle rectangle({0.0, 0.0}, 5.0, 15.0);
  const lyalin::rectangle_t rectFrameBeforeMoving = rectangle.getFrameRect();
  const double rectAreaBeforeMoving = rectangle.getArea();

  rectangle.move({5.0, 8.0});
  BOOST_CHECK_CLOSE(rectFrameBeforeMoving.width, rectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(rectFrameBeforeMoving.height, rectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(rectAreaBeforeMoving, rectangle.getArea(), ACCURACY);

  rectangle.move(10.0, -10.0);
  BOOST_CHECK_CLOSE(rectFrameBeforeMoving.width, rectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(rectFrameBeforeMoving.height, rectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(rectAreaBeforeMoving, rectangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(rectangleScaling)
{
  lyalin::Rectangle rectangle({1.3, 3.7}, 10.0, 20.0);
  const double rectAreaBeforeScaling = rectangle.getArea();
  const double scaleCoef = 2.0;
  rectangle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(rectangle.getArea(), scaleCoef * scaleCoef * rectAreaBeforeScaling, ACCURACY);
}

BOOST_AUTO_TEST_CASE(rectangleInvalidParameters)
{
  BOOST_CHECK_THROW(lyalin::Rectangle({3.0, 5.0}, 2.0, -5.0), std::invalid_argument);
  BOOST_CHECK_THROW(lyalin::Rectangle({2.0, 4.1}, -1.23, 3.21), std::invalid_argument);
  BOOST_CHECK_THROW(lyalin::Rectangle({5.0, 3.1}, -20.0, -21.0), std::invalid_argument);

  lyalin::Rectangle rectangle({3.0, 3.0}, 12.0, 12.0);
  BOOST_CHECK_THROW(rectangle.scale(-100.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleRotation)
{
  lyalin::Rectangle rectangle({3.0, 5.0}, 2.0, 5.0);
  double areaBefore = rectangle.getArea();

  double angle = -90.00;
  BOOST_CHECK_NO_THROW(rectangle.rotate(angle));

  angle = 90.00;
  BOOST_CHECK_NO_THROW(rectangle.rotate(angle));

  angle = -45.5;
  rectangle.rotate(angle);
  BOOST_CHECK_CLOSE(rectangle.getArea(), areaBefore, ACCURACY);

  angle = 15.35;
  rectangle.rotate(angle);
  BOOST_CHECK_CLOSE(rectangle.getArea(), areaBefore, ACCURACY);

  angle = 361.0;
  rectangle.rotate(angle);
  BOOST_CHECK_CLOSE(rectangle.getArea(), areaBefore, ACCURACY);

  angle = -1000.1;
  rectangle.rotate(angle);
  BOOST_CHECK_CLOSE(rectangle.getArea(),areaBefore, ACCURACY);
}


BOOST_AUTO_TEST_SUITE_END()
