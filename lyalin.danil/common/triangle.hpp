#ifndef A3_TRIANGLE_HPP
#define A3_TRIANGLE_HPP

#include "shape.hpp"

namespace lyalin
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &, const point_t &, const point_t &);

    point_t getCenter() const;

    double getArea() const override;

    rectangle_t getFrameRect() const override;

    void move(const double x, const double y) override;

    void move(const point_t &) override;

    void scale(const double) override;

    void rotate(double angle) override;

  private:
    point_t pointA_;
    point_t pointB_;
    point_t pointC_;
  };
}
#endif
