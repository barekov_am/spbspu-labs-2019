#include "circle.hpp"
#include <stdexcept>
#include <cmath>

lyalin::Circle::Circle(const point_t &position, double radius) :
  center_(position),
  radius_(radius)
{
  if (radius_ <= 0.0)
  {
    throw std::invalid_argument("Circle: invalid radius");
  }
}

double lyalin::Circle::getArea() const
{
  return radius_ * radius_ * M_PI;
}

lyalin::rectangle_t lyalin::Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, center_};
}

void lyalin::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void lyalin::Circle::move(const point_t &position)
{
  center_ = position;
}

void lyalin::Circle::scale(double scaleCoef)
{
  if (scaleCoef <= 0.0)
  {
    throw std::invalid_argument("Circle: scaling parameter is invalid");
  }
  else
  {
    radius_ *= scaleCoef;
  }
}

void lyalin::Circle::rotate(double)
{
}
