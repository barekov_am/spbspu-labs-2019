#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

lyalin::Triangle::Triangle(const point_t &pointA, const point_t &pointB, const point_t &pointC) :
   pointA_(pointA),
   pointB_(pointB),
   pointC_(pointC)
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("Triangle:invalid parameters.");
  }
}

lyalin::point_t lyalin::Triangle::getCenter() const
{
  return {(pointA_.x + pointB_.x + pointC_.x) / 3, (pointA_.y + pointB_.y + pointC_.y) / 3};
}

double lyalin::Triangle::getArea() const
{
  point_t side1 = {pointB_.x - pointA_.x, pointB_.y - pointA_.y};
  point_t side2 = {pointC_.x - pointA_.x, pointC_.y - pointA_.y};

  return (fabs((side1.x * side2.y - side1.y * side2.x)) / 2);
}

lyalin::rectangle_t lyalin::Triangle::getFrameRect() const
{
  point_t max{std::max(pointA_.x, std::max(pointB_.x, pointC_.x)),
              std::max(pointA_.y, std::max(pointB_.y, pointC_.y))};
  point_t min{std::min(pointA_.x, std::min(pointB_.x, pointC_.x)),
              std::min(pointA_.y, std::min(pointB_.y, pointC_.y))};

  double width = max.x - min.x;
  double height = max.y - min.y;
  point_t pos = {width / 2 + min.x, height / 2 + min.y};

  return {width, height, pos};
}

void lyalin::Triangle::move(const double X, const double Y)
{
  pointA_.x += X;
  pointA_.y += Y;

  pointB_.x += X;
  pointB_.y += Y;

  pointC_.x += X;
  pointC_.y += Y;
}

void lyalin::Triangle::move(const point_t &point)
{
  point_t center = getCenter();
  move(point.x - center.x, point.y - center.y);
}

void lyalin::Triangle::scale(const double scaleCoef)
{
  if (scaleCoef <= 0)
  {
    throw std::invalid_argument("Triangle: scaling parameter is invalid");
  }
  point_t center = getCenter();
  pointA_.x = center.x * (1 - scaleCoef) + pointA_.x * scaleCoef;
  pointA_.y = center.y * (1 - scaleCoef) + pointA_.y * scaleCoef;

  pointB_.x = center.x * (1 - scaleCoef) + pointB_.x * scaleCoef;
  pointB_.y = center.y * (1 - scaleCoef) + pointB_.y * scaleCoef;

  pointC_.x = center.x * (1 - scaleCoef) + pointC_.x * scaleCoef;
  pointC_.y = center.y * (1 - scaleCoef) + pointC_.y * scaleCoef;
}
void lyalin::Triangle::rotate(double angle)
{
  point_t center = getCenter();
  const double cosinus = std::abs(std::cos(angle * M_PI / 180));
  const double sinus = std::abs(std::sin(angle * M_PI / 180));
  const double Ax = (pointA_.x - center.x) * cosinus - (pointA_.y - center.y) * sinus;
  const double Ay = (pointA_.x - center.x) * sinus + (pointA_.y - center.y) * cosinus;
  const double Bx = (pointB_.x - center.x) * cosinus - (pointB_.y - center.y) * sinus;
  const double By = (pointB_.x - center.x) * sinus + (pointB_.y - center.y) * cosinus;
  const double Cx = (pointC_.x - center.x) * cosinus - (pointC_.y - center.y) * sinus;
  const double Cy = (pointC_.x - center.x) * sinus + (pointC_.y - center.y) * cosinus;
  pointA_.x = Ax + center.x;
  pointA_.y = Ay + center.y;
  pointB_.x = Bx + center.x;
  pointB_.y = By + center.y;
  pointC_.x = Cx + center.x;
  pointC_.y = Cy + center.y;
}
