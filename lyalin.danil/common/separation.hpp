#ifndef SEPARATION_HPP_INCLUDED
#define SEPARATION_HPP_INCLUDED

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace lyalin
{
  bool cross(const Shape &firstShape, const Shape &secondShape);
  Matrix<Shape> layer(const CompositeShape&);
}

#endif
