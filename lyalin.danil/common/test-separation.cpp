#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "separation.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(separationTestSuite)

  BOOST_AUTO_TEST_CASE(crossingOfShapes)
  {
    lyalin::Rectangle testRectangle1({0.0, 0.0}, 4.0, 4.0);
    lyalin::Circle testCircle1({-5.0, 1.0}, 1.0);
    lyalin::Rectangle testRectangle2({-2.0, 1.0}, 4.0, 4.0);
    lyalin::Circle testCircle2({-2.0, 0.0}, 1.0);

    BOOST_CHECK(lyalin::cross(testRectangle1, testCircle2));
    BOOST_CHECK(lyalin::cross(testRectangle1, testRectangle2));
    BOOST_CHECK(lyalin::cross(testRectangle2, testCircle1));
    BOOST_CHECK(lyalin::cross(testRectangle2, testCircle2));

    BOOST_CHECK(!lyalin::cross(testRectangle1, testCircle1));
    BOOST_CHECK(!lyalin::cross(testCircle1, testCircle2));
  }

  BOOST_AUTO_TEST_CASE(emptyCompositeLayering)
  {
    lyalin::Matrix<lyalin::Shape> testMatrix = lyalin::layer(lyalin::CompositeShape());

    BOOST_CHECK_EQUAL(testMatrix.getRows(), 0);
    BOOST_CHECK_EQUAL(testMatrix.getColumns(), 0);
  }

  BOOST_AUTO_TEST_CASE(correctCompositeSeparation)
  {
    lyalin::Rectangle r1({-1.0, -1.0}, 4.0, 4.0);
    lyalin::Circle c1({3.0, 2.0}, 1.0);
    lyalin::Rectangle r2({1.0, 2.0}, 4.0, 4.0);
    lyalin::Rectangle r3({1.0, -4.0}, 4.0, 4.0);
    lyalin::CompositeShape shapes(std::make_shared<lyalin::Rectangle>(r1));
    shapes.add(std::make_shared<lyalin::Circle>(c1));
    shapes.add(std::make_shared<lyalin::Rectangle>(r2));
    shapes.add(std::make_shared<lyalin::Rectangle>(r3));

    lyalin::Matrix<lyalin::Shape> testMatrix = lyalin::layer(shapes);

    BOOST_CHECK_EQUAL(testMatrix.getRows(), 2);
    BOOST_CHECK_EQUAL(testMatrix.getColumns(), 2);
    BOOST_CHECK_EQUAL(testMatrix[0][0], shapes[0]);
    BOOST_CHECK_EQUAL(testMatrix[0][1], shapes[1]);
    BOOST_CHECK_EQUAL(testMatrix[1][0], shapes[2]);
    BOOST_CHECK_EQUAL(testMatrix[1][1], shapes[3]);
  }

BOOST_AUTO_TEST_SUITE_END()
