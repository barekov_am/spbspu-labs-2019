#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace lyalin
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t &position, double radius);

    double getArea() const override;

    rectangle_t getFrameRect() const override;

    void move(double dx, double dy) override;

    void move(const point_t &position) override;

    void scale(double scaleCoef) override;

    void rotate(double) override;

  private:
    point_t center_;
    double radius_;
  };
}
#endif
