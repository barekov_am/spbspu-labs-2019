#include <boost/test/auto_unit_test.hpp>

#include "shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(CompositeShapeTestSuite)

BOOST_AUTO_TEST_CASE(ImmutabilityofCompositeShapeParameters)
{
  sheve::Rectangle rec({ 0.0, 0.0 }, 2.0, 4.0);
  sheve::Circle cir({ 2.0, 3.0 }, 1.0);
  sheve::CompositeShape composite(std::make_shared<sheve::Rectangle>(rec));
  composite.add(std::make_shared<sheve::Circle>(cir));
  const sheve::rectangle_t compositeBefore = composite.getFrameRect();
  const double areaBefore = composite.getArea();
  const size_t countBefore = composite.getCount();

  composite.move(1.0, 1.0);
  BOOST_CHECK_CLOSE(compositeBefore.width, composite.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(compositeBefore.height, composite.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, composite.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(countBefore, composite.getCount());

  composite.move({ 4.0, 6.0 });
  BOOST_CHECK_CLOSE(compositeBefore.width, composite.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(compositeBefore.height, composite.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, composite.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(countBefore, composite.getCount());
}

BOOST_AUTO_TEST_CASE(CompositeShapeAreaAfterScale)
{
  sheve::Rectangle rec({ 0.0, 0.0 }, 2.0, 4.0);
  sheve::Circle cir({ 2.0, 3.0 }, 1.0);
  sheve::CompositeShape composite(std::make_shared<sheve::Rectangle>(rec));
  composite.add(std::make_shared<sheve::Circle>(cir));
  const double areaBefore1 = composite.getArea();
  const double scaleFactor1 = 2.0;

  composite.scale(scaleFactor1);
  BOOST_CHECK_CLOSE(areaBefore1 * scaleFactor1 * scaleFactor1, composite.getArea(), EPSILON);

  const double areaBefore2 = composite.getArea();
  const double scaleFactor2 = 0.5;

  composite.scale(scaleFactor2);
  BOOST_CHECK_CLOSE(areaBefore2 * scaleFactor2 * scaleFactor2, composite.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(CompositeShapeFrameRectAfterScale)
{
  sheve::Rectangle rec({ 0.0, 0.0 }, 2.0, 4.0);
  sheve::Circle cir({ 2.0, 3.0 }, 1.0);
  sheve::CompositeShape composite(std::make_shared<sheve::Rectangle>(rec));
  composite.add(std::make_shared<sheve::Circle>(cir));
  const sheve::rectangle_t frameRectBefore = composite.getFrameRect();
  const double scaleFactor = 3.0;

  composite.scale(scaleFactor);
  const sheve::rectangle_t frameRectAfter = composite.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBefore.width * scaleFactor, frameRectAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height * scaleFactor, frameRectAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, EPSILON);
}


BOOST_AUTO_TEST_CASE(CompositeShapeFrameRectAfterRotate)
{
  sheve::Rectangle rec({0.0, 0.0}, 2.0, 4.0);
  sheve::Circle cir({2.0, 3.0}, 1.0);
  sheve::CompositeShape composite(std::make_shared<sheve::Rectangle>(rec));
  composite.add(std::make_shared<sheve::Circle>(cir));
  const sheve::rectangle_t frameRectBefore = composite.getFrameRect();
  const double areaBefore = composite.getArea();

  composite.rotate(90);
  const sheve::rectangle_t frameRectAfter = composite.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.width, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, composite.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(CompositeShapeIncorrectParameters)
{
  sheve::Rectangle rec({ 0.0, 0.0 }, 2.0, 4.0);
  sheve::Circle cir({ 2.0, 3.0 }, 1.0);
  sheve::CompositeShape composite(std::make_shared<sheve::Rectangle>(rec));
  composite.add(std::make_shared<sheve::Circle>(cir));

  BOOST_CHECK_THROW(composite.scale(-1.0), std::invalid_argument);
  BOOST_CHECK_THROW(composite.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(composite.remove(3), std::invalid_argument);
  BOOST_CHECK_THROW(composite[2], std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(CompositeShapeAfterAddShape)
{
  sheve::Rectangle rec({ 0.0, 0.0 }, 2.0, 4.0);
  sheve::CompositeShape composite(std::make_shared<sheve::Rectangle>(rec));
  const double areaBefore = composite.getArea();
  const size_t countBefore = composite.getCount();
  sheve::Circle cir({ 2.0, 3.0 }, 1.0);
  composite.add(std::make_shared<sheve::Circle>(cir));
  const double areaCir = composite[1]->getArea();
  BOOST_CHECK_CLOSE(areaBefore + areaCir, composite.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(countBefore + 1, composite.getCount());
}

BOOST_AUTO_TEST_CASE(CompositeShapeAfterDeleteByIndexShape)
{
  sheve::Rectangle rec({ 0.0, 0.0 }, 2.0, 4.0);
  sheve::Circle cir({ 2.0, 3.0 }, 1.0);
  sheve::CompositeShape composite(std::make_shared<sheve::Rectangle>(rec));
  composite.add(std::make_shared<sheve::Circle>(cir));
  const double areaBefore = composite.getArea();
  const double areaSecondShape = composite[1]->getArea();
  const size_t countBefore = composite.getCount();

  composite.remove(1);
  BOOST_CHECK_CLOSE(composite.getArea(), areaBefore - areaSecondShape, EPSILON);
  BOOST_CHECK_EQUAL(countBefore - 1, composite.getCount());
}

BOOST_AUTO_TEST_CASE(CopyConstructor)
{
  sheve::Rectangle rec({ 0.0, 0.0 }, 2.0, 4.0);
  sheve::Circle cir({ 2.0, 3.0 }, 1.0);
  sheve::CompositeShape composite(std::make_shared<sheve::Rectangle>(rec));
  composite.add(std::make_shared<sheve::Circle>(cir));
  const double compositeArea = composite.getArea();
  const sheve::rectangle_t compositeFrameRect = composite.getFrameRect();
  const size_t compositeCount = composite.getCount();
  sheve::CompositeShape testComposite(composite);
  const double testCompositeArea = testComposite.getArea();
  const sheve::rectangle_t testCompositeFrameRect = testComposite.getFrameRect();
  const size_t testCompositeCount = testComposite.getCount();

  BOOST_CHECK_CLOSE(compositeArea, testCompositeArea, EPSILON);
  BOOST_CHECK_CLOSE(compositeFrameRect.width, testCompositeFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(compositeFrameRect.height, testCompositeFrameRect.height, EPSILON);
  BOOST_CHECK_CLOSE(compositeFrameRect.pos.x, testCompositeFrameRect.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(compositeFrameRect.pos.y, testCompositeFrameRect.pos.y, EPSILON);
  BOOST_CHECK_EQUAL(compositeCount, testCompositeCount);
}

BOOST_AUTO_TEST_CASE(CopyOperator)
{
  sheve::Rectangle rec({ 0.0, 0.0 }, 2.0, 4.0);
  sheve::Circle cir({ 2.0, 3.0 }, 1.0);
  sheve::CompositeShape composite(std::make_shared<sheve::Rectangle>(rec));
  composite.add(std::make_shared<sheve::Circle>(cir));
  const double compositeArea = composite.getArea();
  const sheve::rectangle_t compositeFrameRect = composite.getFrameRect();
  const size_t compositeCount = composite.getCount();
  sheve::CompositeShape testComposite = composite;
  const double testCompositeArea = testComposite.getArea();
  const sheve::rectangle_t testCompositeFrameRect = testComposite.getFrameRect();
  const size_t testCompositeCount = testComposite.getCount();

  BOOST_CHECK_CLOSE(compositeArea, testCompositeArea, EPSILON);
  BOOST_CHECK_CLOSE(compositeFrameRect.width, testCompositeFrameRect.width, EPSILON);
  BOOST_CHECK_CLOSE(compositeFrameRect.height, testCompositeFrameRect.height, EPSILON);
  BOOST_CHECK_CLOSE(compositeFrameRect.pos.x, testCompositeFrameRect.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(compositeFrameRect.pos.y, testCompositeFrameRect.pos.y, EPSILON);
  BOOST_CHECK_EQUAL(compositeCount, testCompositeCount);
}

BOOST_AUTO_TEST_SUITE_END()
