#include <stdexcept>
#include <iostream>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(circle)

BOOST_AUTO_TEST_CASE(invalid_Initialisation)
{
  BOOST_CHECK_THROW(sheve::Circle({ 23, -5 }, -7), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalid_Scale)
{
  sheve::Circle cir({ 1, 1 }, 5);
  BOOST_CHECK_THROW(cir.scale(-15), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(Move_dx_dy)
{
  sheve::Circle cir({ 20, 30 }, 13);
  const double framebeforemove_w = cir.getFrameRect().width;
  const double framebeforemove_h = cir.getFrameRect().height;
  const double areabeforemoving = cir.getArea();
  cir.move(-12, 9);
  BOOST_CHECK_CLOSE(framebeforemove_w, cir.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(framebeforemove_h, cir.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areabeforemoving, cir.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(move_point)
{
  sheve::Circle cir({ 20, 30 }, 13);
  const double framebeforemove_w = cir.getFrameRect().width;
  const double framebeforemove_h = cir.getFrameRect().height;
  const double areabeforemoving = cir.getArea();
  cir.move({ 32, -6 });
  BOOST_CHECK_CLOSE(framebeforemove_w, cir.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(framebeforemove_h, cir.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areabeforemoving, cir.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(Scaling)
{
  sheve::Circle cir({ 20, 30 }, 13);
  const double areabeforemoving = cir.getArea();
  const double scalefactor = 0.9;
  cir.scale(scalefactor);
  BOOST_CHECK_CLOSE(areabeforemoving * scalefactor * scalefactor, cir.getArea(), EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()
