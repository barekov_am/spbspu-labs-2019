#include "rectangle.hpp"

#include <iostream>
#include <stdexcept>
#include <cmath>

sheve::Rectangle::Rectangle(const point_t &p, double w, double h):
  m_pos(p),
  m_width(w),
  m_height(h),
  m_corners{ { p.x - w / 2, p.y + h / 2 },
             { p.x + w / 2, p.y + h / 2 },
             { p.x + w / 2, p.y - h / 2 },
             { p.x - w / 2, p.y - h / 2 } }
{
  if ((m_width <= 0.0) || (m_height <= 0.0))
  {
    throw std::invalid_argument("Rectangle parameters must be positive!");
  }
}

double sheve::Rectangle::getArea() const
{
  return (m_width * m_height);
}

sheve::rectangle_t sheve::Rectangle::getFrameRect() const
{
  point_t topLeft = m_corners[0];
  point_t bottomRight = m_corners[2];

  for (size_t i = 0; i < 4; i++)
  {
    topLeft.x = std::min(m_corners[i].x, topLeft.x);
    topLeft.y = std::max(m_corners[i].y, topLeft.y);
    bottomRight.x = std::max(m_corners[i].x, bottomRight.x);
    bottomRight.y = std::min(m_corners[i].y, bottomRight.y);
  }

  point_t center = { (topLeft.x + bottomRight.x) / 2, (topLeft.y + bottomRight.y) / 2 };

  return { center, bottomRight.x - topLeft.x, topLeft.y - bottomRight.y } ;
}

void sheve::Rectangle::move(const point_t &p)
{
  for (size_t i = 0; i < 4; i++)
  {
    m_corners[i] = { p.x + m_corners[i].x - m_pos.x, p.y + m_corners[i].y - m_pos.y };
  }

  m_pos = p;
}

void sheve::Rectangle::move(double dx, double dy)
{
  m_pos.x += dx;
  m_pos.y += dy;

  for (size_t i = 0; i < 4; i++)
  {
  m_corners[i].x += dx;
  m_corners[i].y += dy;
  }

}

void sheve::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Coefficient should be positive!");
  }
  m_width *= coefficient;
  m_height *= coefficient;

  for (size_t i = 0; i < 4; i++)
  {
    m_corners[i] = { m_pos.x + (m_corners[i].x - m_pos.x) * coefficient,
                     m_pos.y + (m_corners[i].y - m_pos.y) * coefficient };
  }
}

void sheve::Rectangle::rotate(double degrees)
{
  const double angleRadian = M_PI * degrees / 180;
  const double sin = std::sin(angleRadian);
  const double cos = std::cos(angleRadian);

  for (size_t i =0; i < 4; i++)
  {
    m_corners[i] = { m_pos.x + (m_corners[i].x - m_pos.x) * cos - (m_corners[i].y - m_pos.y) * sin,
                     m_pos.y + (m_corners[i].y - m_pos.y) * cos + (m_corners[i].x - m_pos.x) * sin };
  }
}

void sheve::Rectangle::printInfo() const
{
  std::cout << "Center: " << m_pos.x << ", " << m_pos.y << std::endl;
  std::cout << "Width: " << m_width << std::endl;
  std::cout << "Height: " << m_height << std::endl;
  std::cout << "Rectangle area: " << getArea() << std::endl;
  std::cout << "Frame rectangle:" << std::endl;
  std::cout << "-Center: " << getFrameRect().pos.x << ", " << getFrameRect().pos.y << std::endl;
  std::cout << "-Width: " << getFrameRect().width << std::endl;
  std::cout << "-Height: " << getFrameRect().height << std::endl;
  std::cout << "Coordinates of the corners of the rectangle" << std::endl;
  std::cout << "Top left: " << m_corners[0].x << ", " << m_corners[0].y << std::endl;
  std::cout << "Top right: " << m_corners[1].x << ", " << m_corners[1].y << std::endl;
  std::cout << "Bottom right: " << m_corners[2].x << ", " << m_corners[2].y << std::endl;
  std::cout << "Bottom left: " << m_corners[3].x << ", " << m_corners[3].y << std::endl;
  std::cout << std::endl;
}
