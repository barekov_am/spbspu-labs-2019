#include <stdexcept>
#include <iostream>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "triangle.hpp"


const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(triangle)

BOOST_AUTO_TEST_CASE(invalid_Initialisation)
{
  BOOST_CHECK_THROW(sheve::Triangle({ 1, 2 }, { -13, 2 }, { 1, 2 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalid_Scale)
{
  sheve::Triangle tri({ 12, -12 }, { 13, 5 }, { 1, 23 });
  BOOST_CHECK_THROW(tri.scale(-19), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(Move_dx_dy)
{
  sheve::Triangle tri({ 12, -12 }, { 13, 5 }, { 1, 23 });
  const double framebeforemove_w = tri.getFrameRect().width;
  const double framebeforemove_h = tri.getFrameRect().height;
  const double areabeforemoving = tri.getArea();
  tri.move(10, 20);
  BOOST_CHECK_CLOSE(framebeforemove_w, tri.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(framebeforemove_h, tri.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areabeforemoving, tri.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(move_point)
{
  sheve::Triangle tri({ 12, -12 }, { 13, 5 }, { 1, 23 });
  const double framebeforemove_w = tri.getFrameRect().width;
  const double framebeforemove_h = tri.getFrameRect().height;
  const double areabeforemoving = tri.getArea();
  tri.move({ -7, 4 });
  BOOST_CHECK_CLOSE(framebeforemove_w, tri.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(framebeforemove_h, tri.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areabeforemoving, tri.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(Scaling)
{
  sheve::Triangle tri({ 12, -12 }, { 13, 5 }, { 1, 23 });
  const double areabeforemoving = tri.getArea();
  const double scalefactor = 1.63;
  tri.scale(scalefactor);
  BOOST_CHECK_CLOSE(areabeforemoving * scalefactor * scalefactor, tri.getArea(), EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()
