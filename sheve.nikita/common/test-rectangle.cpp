#include <stdexcept>
#include <iostream>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "base-types.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(rectangle)

BOOST_AUTO_TEST_CASE(create_rectangle)
{
  BOOST_CHECK_THROW(sheve::Rectangle({ -23, -5 }, 2, -35), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(changing_Scale)
{
  sheve::Rectangle rec({ 11, 23 }, 5, 50);
  BOOST_CHECK_THROW(rec.scale(-12), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(Moving_by_dx_dy)
{
  sheve::Rectangle rec({ 20, 10 }, 15, 50);
  const double framebeforemove_w = rec.getFrameRect().width;
  const double framebeforemove_h = rec.getFrameRect().height;
  const double areabeforemoving = rec.getArea();
  rec.move(10, -30);
  BOOST_CHECK_CLOSE(framebeforemove_w, rec.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(framebeforemove_h, rec.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areabeforemoving, rec.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(Moving_by_point)
{
  sheve::Rectangle rec({ 20, 10 }, 15, 50);
  const double framebeforemove_w = rec.getFrameRect().width;
  const double framebeforemove_h = rec.getFrameRect().height;
  const double areabeforemoving = rec.getArea();
  rec.move({ -27, 52 });
  BOOST_CHECK_CLOSE(framebeforemove_w, rec.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(framebeforemove_h, rec.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areabeforemoving, rec.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(RectangleAreaAfterRotate)
{
  sheve::Rectangle rec({ 20, 10 }, 15, 50);
  const double areaBefore = rec.getArea();
  const double widthBefore = rec.getFrameRect().width;
  const double heightBefore = rec.getFrameRect().height;

  rec.rotate(90);
  BOOST_CHECK_CLOSE(areaBefore, rec.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(widthBefore, rec.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(heightBefore, rec.getFrameRect().width, EPSILON);
}


BOOST_AUTO_TEST_CASE(Scaling)
{
  sheve::Rectangle rec({ 20, 10 }, 15, 50);
  const double areabeforemoving = rec.getArea();
  const double scalefactor = 1.2;
  rec.scale(scalefactor);
  BOOST_CHECK_CLOSE(areabeforemoving * scalefactor * scalefactor, rec.getArea(), EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()
