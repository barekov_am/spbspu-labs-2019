#ifndef SPLIT_A4
#define SPLIT_A4

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace sheve
{
  Matrix split(const CompositeShape &listOfFigures);
}

#endif

