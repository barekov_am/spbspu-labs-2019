#ifndef SHAPE
#define SHAPE

#include <iostream>

class Shape
{
public:
  Shape(int x, int y);

  virtual ~Shape() = default;

  bool isMoreLeft(const Shape & shape) const;

  bool isUpper(const Shape & shape) const;

  virtual void draw() const = 0;

protected:
  int x_;
  int y_;
};

class Circle: public Shape
{
public:

  Circle(int x, int y);

  void draw() const override;

};

class Square: public Shape
{
public:

  Square(int x, int y);

  void draw() const override;

};

class Triangle: public Shape
{
public:

  Triangle(int x, int y);

  void draw() const override;

};

#endif
