#include "shape.hpp"

Shape::Shape(int x, int y): x_(x), y_(y)
{}

bool Shape::isMoreLeft(const Shape & shape) const
{
  return (x_ < shape.x_);
}

bool Shape::isUpper(const Shape & shape) const
{
  return (y_ > shape.y_);
}

Circle::Circle(int x, int y): Shape(x, y)
{}

void Circle::draw() const
{
  std::cout << "CIRCLE" << " " << " (" << x_ << ";" << y_ << ")" << std::endl;
}

Triangle::Triangle(int x, int y): Shape(x, y)
{}

void Triangle::draw() const
{
  std::cout << "TRIANGLE" << " " << "(" << x_ << ";" << y_ << ")" << std::endl;
}

Square::Square(int x, int y): Shape(x, y)
{}

void Square::draw() const
{
  std::cout << "SQUARE" << " " << "(" << x_ << ";" << y_ << ")" << std::endl;
}
