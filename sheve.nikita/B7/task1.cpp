#include <vector>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <iterator>
#include <list>

void task1()
{
  std::list<double> line;

  std::copy(std::istream_iterator<double>(std::cin), std::istream_iterator<double>(), std::inserter(line, line.end()));

  if (std::cin.fail() && !std::cin.eof()){
    throw std::invalid_argument("Invalid input");
  }
  
  std::for_each(line.begin(), line.end(), [](double &value) {value *= M_PI; });

  std::copy(line.begin(), line.end(), std::ostream_iterator<double>(std::cout, "\n"));
}
