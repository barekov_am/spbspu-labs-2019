#include <iostream>
#include <list>
#include <algorithm>
#include <iterator>
#include <exception>
#include <memory>
#include <sstream>

#include "shape.hpp"



void print(std::list<std::shared_ptr<Shape>> list)
{
  std::for_each(list.begin(), list.end(), [](std::shared_ptr<Shape> sh1){sh1->draw();});
}

bool isMoreLeft(const std::shared_ptr<Shape>& lhs, const std::shared_ptr<Shape>& rhs)
{
  return rhs->isMoreLeft(*lhs);
}

bool isUpper(const std::shared_ptr<Shape>& lhs, const std::shared_ptr<Shape>& rhs)
{
  return rhs->isUpper(*lhs);
}


void task2()
{
  std::list<std::shared_ptr<Shape>> list;

  std::string line;

  int x, y;

  while (std::getline(std::cin >> std::ws, line))
  {
    if (line.empty())
    {
      continue;
    }
    
    std::stringstream in(line);

    std::string shapes;

    std::string trash;
    
   std::getline(in >> std::ws, shapes, '(');
 
    if (shapes.find(' ') != std::string::npos)
    {
      shapes.erase(shapes.find(' '), std::string::npos);
    }

    in >> x;
    std::getline(in, trash, ';');

    in >> y;
    std::getline(in, trash, ')');

    if (in.fail())
    {
      throw std::invalid_argument("Incorrect in");
    }

    std::shared_ptr<Shape> ptr;

    if (shapes == "CIRCLE")
    {
      ptr = std::make_shared<Circle>(x, y);
    }

    else if (shapes == "SQUARE")
    {
      ptr = std::make_shared<Square>(x, y);
    }

    else if (shapes == "TRIANGLE")
    {
      ptr = std::make_shared<Triangle>(x, y);
    }

    else
    {
      throw std::invalid_argument("Wrong type of shape\n");
    }

    list.push_back(ptr);
  }
  std::cout << "Original:\n";
  print(list);
  list.sort(&isMoreLeft);

  std::cout << "Left-Right:\n";
  list.reverse();
  print(list);

  std::cout << "Right-Left:\n";
  list.reverse();
  print(list);

  std::cout << "Top-Bottom:\n";
  list.sort(&isUpper);
  list.reverse();
  print(list);

  std::cout << "Bottom-Top:\n";
  list.reverse();
  print(list);
}
