#include <iostream>

void task1();
void task2();

int main(int argc, const char * argv[]) 
{
  if (argc != 2)
  {
    std::cerr << "Incorrect number of parameters number!" << '\n';
    return 1;
  }

  std::string task(argv[1]);

  try
  {
    if (task == "1")
    {
      task1();
    }

    else if (task == "2")
    {
      task2();
    }

    else
    {
      std::cerr << "Incorrect task number!" << '\n';
      return 1;
    }
  }

  catch (std::exception& e)
  {
    std::cerr << e.what();
    return 1;
  }

  return 0;
}
