#include "iostream"
#include "functor.hpp"

MyFunctor::MyFunctor():
  element_(0),
  summElement_(0),
  max_(0),
  min_(0),
  positive_(0),
  negative_(0),
  oddSumm_(0),
  evenSumm_(0),
  first_(0),
  last_(0)
{
}

void MyFunctor::operator()(int value)
{
  summElement_ += value;
  last_ = value;
 
  if (value > 0)
  {
    positive_++;
  }
  else if (value < 0)
  {
    negative_++;
  }
  
  if (value < min_)
  {
    min_ = value;
  }
  else if (value > max_)
  {
    max_ = value;
  }
  
  if (value % 2 == 0)
  {
    evenSumm_ += value;
  }
  else
  {
    oddSumm_ += value;
  }

  if ( element_ != 0 )
  { 
    element_++;
  }
  else
  {
    element_++;
    first_ = value;
    max_ = value;
    min_ = value;
  }
}

long int MyFunctor::Max()
{
  return max_;
}

long int MyFunctor::Min()
{
  return min_;
}

int MyFunctor::Positive()
{
  return positive_;
}

int MyFunctor::Negative()
{
  return negative_;
}

long int MyFunctor::EvenSumm()
{
  return evenSumm_;
}

long int MyFunctor::OddSumm()
{
  return oddSumm_;
}

double MyFunctor::Mean()
{
  return static_cast<double>(summElement_) / static_cast<double>(element_);
}

bool MyFunctor::FirstLastEqual()
{
  return last_ == first_;
}

void MyFunctor::print()
{
  std::cout << "Max: " << Max() << '\n';
  std::cout << "Min: " << Min() << '\n';
  std::cout << "Mean: " << Mean() << '\n';
  std::cout << "Positive: " << Positive() << '\n';
  std::cout << "Negative: " << Negative() << '\n';
  std::cout << "Odd Sum: " << OddSumm() << '\n';
  std::cout << "Even Sum: " << EvenSumm() << '\n';
  std::cout << "First/Last Equal: ";
  if (FirstLastEqual())
  {
    std::cout << "yes" << '\n';
  }
  else
  {
    std::cout << "no" << '\n';
  }
}















