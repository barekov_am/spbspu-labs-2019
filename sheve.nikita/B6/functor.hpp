#ifndef MYFUNCTOR_HPP
#define MYFUNCTOR_HPP 

class MyFunctor
{
public :
  MyFunctor();

  void print();

  void operator()(int value);
  long int Max();
  long int Min();
  double Mean();
  int Positive();
  int Negative();
  long int OddSumm();
  long int EvenSumm();
  bool FirstLastEqual();

private :

  int element_;
  long int summElement_;
  long int max_;
  long int min_;
  int positive_;
  int negative_;
  long int oddSumm_;
  long int evenSumm_;
  long int first_;
  long int last_;
};

#endif
