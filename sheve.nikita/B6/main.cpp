#include <iostream>
#include <vector>
#include <algorithm>
#include "functor.hpp"
int main()
{
  try
  {
    std::vector<int> vector;
    int value;
    while (std::cin >> value)
    {
      vector.push_back(value);
    }
    if (std::cin.fail() && !std::cin.eof())
    {
      throw std::invalid_argument("Incorrect input");
    }
    if (!vector.empty())
    {
      MyFunctor functor = std::for_each(vector.begin(), vector.end(), MyFunctor());

      functor.print();
    }
    else
    {
      std::cout << "No Data" << '\n';
    }
  }
  catch (std::exception& e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}

