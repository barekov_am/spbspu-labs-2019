#ifndef TEMPLATES_HPP
#define TEMPLATES_HPP

#include <functional>
#include <iostream>
#include <string.h>

#include "algorithm.hpp"

template <typename T>
std::function<bool(T, T)> getDirection(const char* direction)
{
  if (strcmp(direction, "ascending") == 0)
  {
    return [](T a, T b) {return a > b; };
  }
  else if (strcmp(direction, "descending") == 0)
  {
    return [](T a, T b) {return a < b; };
  }
  else
  {
    throw std::invalid_argument("Write the right direction");
  }
}

template <template <class T> class Access, class T>
void sort(T& array, std::function<bool(typename T::value_type, typename T::value_type)> compare)
{
  using index = typename Access<T>::index;
  const index begin = Access<T>::begin(array);
  const index end = Access<T>::end(array);
  for (index i = begin; i != end; ++i)
  {
    for (index j = i; j != end; ++j)
    {
      if (compare(Access<T>::getIndex(array, i), Access<T>::getIndex(array, j)))
      {
        std::swap(Access<T>::getIndex(array, i), Access<T>::getIndex(array, j));
      }
    }
  }
}

template <typename T>
void print(const T& array)
{
  if (array.empty())
  {
    return;
  }

  for (auto elem : array)
  {
    std::cout << elem << " ";
  }
  std::cout << "\n";
}

#endif
