#include <iostream>
#include <cstdlib>
#include <ctime>

void task1(const char* direction);
void task2(const char* fileName);
void task3();
void task4(const char* direction, int size);

int main(int argc, char* argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4))
    {
      std::cerr << "Invalid number of arguments";
      return 1;
    }
    
        char* ptr = nullptr;
    int task = std::strtol(argv[1], &ptr, 10);

    if (*ptr)
    {
      std::cerr << "Invalid argument of task`s number";
      return 1;
    }



    switch (task)
    {
    case 1:
    {
      if (argc != 3)
      {
        std::cerr << "Incorrect number of arguments";
        return 1;
      }

      task1(argv[2]);
      break;
    }

    case 2:
    {
      if (argc != 3)
      {
        std::cerr << "Incorrect number of arguments";
        return 1;
      }
      
      if (sizeof(argv[2]) == 0)
      {
        std::cerr << "File is not exist";
        return 1;
      }

      task2(argv[2]);
      break;
    }

    case 3:
    {
      if (argc != 2)
      {
        std::cerr << "Incorrect number of arguments";
        return 1;
      }
      
      task3();
      break;
    }

    case 4:
    {
      if (argc != 4)
      {
        std::cerr << "Incorrect number of arguments";
        return 1;
      }
      
      if (argv[2] == nullptr)
      {
         std::cerr << "Invalid parametr\n";
         return 1;
      }

      char* sizePtr = nullptr;
      int size = std::strtol(argv[3], &sizePtr, 10);
      if (*sizePtr)
      {
        std::cerr << "Invalid argument!";
        return 1;
      }

      task4(argv[2], size);
      break;
    }

    default:
    {
      std::cerr << "Incorrect task number";
      return 1;
    }
    }
  }
  catch (const std::exception & exception)
  {
    std::cerr << exception.what();
    return 1;
  }

  return 0;
}

