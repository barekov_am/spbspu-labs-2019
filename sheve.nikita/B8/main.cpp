#include "texting.hpp"

#include <iostream>
#include <cstring>

const int MINWIDTH = 25;

int main(int argc, char * argv[])
{
  try
  {
    if ((argc == 2) || (argc > 3))
    {
      std::cerr << "Number of parameters is incorrect!" << std::endl;
      return 1;
    }
    size_t len = 40;

    if (argc == 3)
    {
      if (strcmp(argv[1], "--line-width") != 0)
      {
        throw std::invalid_argument("Invalid input!");
      }

      len = std::stoi(argv[2]);

      if (len < MINWIDTH)
      {
        throw std::invalid_argument("Invalid line width! It must be greater than 25!");
      }
    }

    Texting text(len);
    text.readText();
    text.printText();
  }

  catch (std::exception& exc)
  {
    std::cerr << exc.what() << std::endl;
    return 2;
  }

  return 0;
}

