#ifndef texting_B8
#define texting_B8

#include <string>
#include <vector>

enum Token
{
  WORD,
  PUNCT,
  DASH,
  NUMBER,
};

struct m_token
{
  Token type;
  std::string string;
};

class Texting
{

public:

  Texting(size_t len = 40);
  void readText();
  void printText();
  void checkWords();

private:

  size_t m_len;
  std::vector<m_token> list;
};

#endif
