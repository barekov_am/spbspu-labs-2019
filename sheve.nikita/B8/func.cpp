#include "func.hpp"

#include <algorithm>
#include <sstream>
#include <iostream>

const int MaxWord = 20;
const int MaxDash = 3;
const int MaxNumber = 1;

void readWord(std::string &line)
{

  while(std::cin)
  {

    if ((std::isalpha(std::cin.peek())) || (std::cin.peek() == '-'))
    {

      line += std::cin.get();

      if ((line.back() == '-') && (std::cin.peek() == '-'))
      {
         throw std::invalid_argument("Incorrect data!");
      }
    }

    else
    {
      break;
    }
  }

  if (line.size() > MaxWord)
  {
    throw std::invalid_argument("Width must be less than 20!");
  }
}

void readNumber(std::string &line)
{
  int countNum = 0;

  while(std::cin)
  {

    if ((std::cin.peek() == '.') || (std::isdigit(std::cin.peek())))
    {

      line += std::cin.get();

      if (line.back() == '.')
      {
        countNum++;
      }
    }

    else
    {
      break;
    }
  }

  if (line.size() > MaxWord)
  {
    throw std::invalid_argument("Error");
  }
  
  if (countNum > MaxNumber)
  {
    throw std::invalid_argument("Error");
  }
}

void readDash(std::string &line)
{
  int countNum = 1;

  while (std::cin)
  {

    if (std::cin.peek() == '-')
    {

      line += std::cin.get();
      countNum++;
    }

    else
    {
      break;
    }
  }

  if (countNum > MaxDash)
  {
    throw std::invalid_argument("Too many dashes for a hyphen\n");
  }

}




