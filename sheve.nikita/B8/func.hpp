#ifndef func_B8
#define func_B8

#include "texting.hpp"

#include <string>

void readWord(std::string &line);
void readDash(std::string &line);
void readNumber(std::string &line);

#endif


