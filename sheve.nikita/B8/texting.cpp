#include "texting.hpp"
#include "func.hpp"

#include <iostream>
#include <algorithm>

void Texting::checkWords()
{
  if ((list.at(0).type == Token::PUNCT) || (list.at(0).type == Token::DASH))
  {
    throw std::invalid_argument("Error\n");
  }

  for (size_t j = 0; j < list.size() - 1; j++)
  {
    if ((list.at(j).type == Token::PUNCT) && (list.at(j + 1).type == Token::PUNCT))
    {
      throw std::invalid_argument("Error\n");
    }
  }
}

Texting::Texting(size_t len) : m_len(len), list()
{ }

void Texting::readText()
{
  while(std::cin)
  {
    std::cin >> std::ws;
    char fr = std::cin.get();

    if (std::isalpha(fr))
    {

      std::string line;
      readWord(line += fr);
      m_token token = {Token::WORD, line};
      list.push_back(token);

    }

    else if ((((fr == '+') || (fr == '-')) && (std::isdigit(std::cin.peek()))) || (std::isdigit(fr)))
    {

      std::string line;
      readNumber(line += fr);
      m_token token = {Token::NUMBER, line};
      list.push_back(token);

    }

    else if (std::ispunct(fr))
    {
      if ((fr == '-') && (std::cin.peek() == '-'))
      {

         std::string line;
         readDash(line += fr);
         m_token token = {Token::DASH, line};
         list.push_back(token);

      }
      else if (!std::ispunct(std::cin.peek()))
      {

        std::string line;
        line += fr;
        m_token token = {Token::PUNCT, line};
        list.push_back(token);

      }

      else
      {
        throw std::invalid_argument("Incorrect data!");
      }
    }
  }
}

void Texting::printText()
{
  std::vector<std::string> m_list;
  std::string m_line;

  size_t counter = 0;
  size_t counterNumber = 0;

  if (list.empty())
  {
    return;
  }

  checkWords();

  while (counter < list.size())
  {

    if (m_line.empty())
    {

      if ((list.at(counter).type == Token::PUNCT) || (list.at(counter).type == Token::DASH))
      {

        if ((list.at(counter).type == Token::PUNCT) && (m_list.at(counterNumber - 1).size() < m_len))
        {

          m_list.at(counterNumber - 1) += list.at(counter).string;
          counter++;
          continue;
        }

        m_line += list.at(counter - 1).string;

        (list.at(counter).type == Token::DASH) ? m_line += " " + list.at(counter).string : m_line += list.at(counter).string;

        auto pos = m_list.at(counterNumber - 1).find_last_of(' ');

        m_list.at(counterNumber - 1).erase(pos);

        counter++;
        continue;
      }

      m_line += list.at(counter).string;
      counter++;
      continue;
    }

    if (m_line.size() + list.at(counter).string.size() + 1 <= m_len)
    {

      if (list.at(counter).type == Token::PUNCT)
      {

        m_line += list.at(counter).string;
        counter++;
        continue;
      }

      m_line += " " + list.at(counter).string;
      counter++;
      continue;
    }

    m_list.push_back(m_line);
    counterNumber++;
    m_line.clear();
  }

  m_list.push_back(m_line);

  std::for_each(m_list.begin(), m_list.end(), [](std::string elem)
  {
    std::cout << elem << "\n"; });
}

