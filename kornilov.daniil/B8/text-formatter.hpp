#ifndef TEXT_FORMATTER_HPP
#define TEXT_FORMATTER_HPP

#include <vector>
#include <string>

class TextFormatter {
public:
  void readText();
  void reformatText(std::size_t line_width);

private:
  struct element_t {
    enum class Type {
      WORD,
      NUMBER,
      PUNCT,
      SPACE,
      DASH
    };

    Type type;
    std::string value;
  };

  std::vector<element_t> vector_;

  void readWord();
  void readNumber();
  void readDash();
  void readPunct(char ch);
  bool checkCorrectness();
  static void printLine(const std::vector<element_t>& line);
  static std::size_t remakeLine(std::vector<element_t>& line);
};

#endif
