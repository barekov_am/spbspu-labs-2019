#include <iostream>
#include "text-formatter.hpp"

int main(const int args, char* argv[])
{
  try {
    auto line_width = 40;
    if (args > 1) {
      if (args != 3) {
        std::cerr << "Invalid number of arguments!\n";
        return 1;
      }
      if (std::string(argv[1]) != "--line-width") {
        std::cerr << "Invalid arguments!\n";
        return 1;
      }
      char* end = nullptr;
      line_width = std::strtol(argv[2], &end, 10);
      if (*end != '\0') {
        std::cerr << "Invalid task number!\n";
        return 1;
      }
      if (line_width < 25) {
        std::cerr << "Invalid line width!\n";
        return 1;
      }
    }
    TextFormatter text_formatter;
    text_formatter.readText();
    text_formatter.reformatText(line_width);
  }
  catch (const std::exception& exception) {
    std::cerr << exception.what() << "\n";
    return 1;
  }
  return 0;
}
