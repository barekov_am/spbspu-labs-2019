#include "text-formatter.hpp"
#include <iostream>
#include <locale>

void TextFormatter::readText()
{
  while (std::cin) {
    char ch = std::cin.get();
    while (std::isspace(ch, std::locale())) {
      ch = std::cin.get();
    }
    if (std::isalpha(ch, std::locale())) {
      std::cin.unget();
      readWord();
    } else if (ch == '-') {
      if (std::cin.peek() == '-') {
        std::cin.unget();
        readDash();
      } else {
        std::cin.unget();
        readNumber();
      }
    } else if (ch == '+' || isdigit(ch, std::locale())) {
      std::cin.unget();
      readNumber();
    } else if (ispunct(ch, std::locale())) {
      readPunct(ch);
    }
  }
  if (!checkCorrectness()) {
    throw std::runtime_error("Incorrect input!\n");
  }
}

void TextFormatter::reformatText(const std::size_t line_width)
{
  std::size_t current_width = 0;
  std::vector<element_t> line;
  for (auto iterator = vector_.begin(); iterator != vector_.end(); ++iterator) {
    if (iterator->type == element_t::Type::PUNCT) {
      if (current_width + 1 > line_width) {
        current_width = remakeLine(line);
      }
      line.push_back(*iterator);
      current_width += iterator->value.size();
    }
    if (iterator->type == element_t::Type::DASH) {
      if (current_width + 4 > line_width) {
        current_width = remakeLine(line);
      }
      line.push_back(element_t{element_t::Type::SPACE, " "});
      line.push_back(*iterator);
      current_width += iterator->value.size() + 1;
    }
    if (iterator->type == element_t::Type::NUMBER || iterator->type == element_t::Type::WORD) {
      if (current_width + iterator->value.size() + 1 > line_width) {
        printLine(line);
        line.clear();
        current_width = 0;
      } else if (!line.empty()) {
        line.push_back(element_t{element_t::Type::SPACE, " "});
        ++current_width;
      }
      line.push_back(*iterator);
      current_width += iterator->value.size();
    }
  }
  if (!line.empty()) {
    printLine(line);
  }
}

void TextFormatter::readWord()
{
  element_t element{element_t::Type::WORD, ""};
  while (std::isalpha<char>(std::cin.peek(), std::locale()) || std::cin.peek() == '-') {
    const char ch = std::cin.get();
    if (ch != '-') {
      element.value.push_back(ch);
      continue;
    }
    if (ch == '-' && std::cin.peek() != '-') {
      element.value.push_back(ch);
    } else {
      std::cin.unget();
      break;
    }
  }
  vector_.push_back(element);
}

void TextFormatter::readNumber()
{
  element_t element{element_t::Type::NUMBER, ""};
  char ch = std::cin.get();
  if (ch == '+' || ch == '-') {
    element.value.push_back(ch);
  } else {
    std::cin.unget();
  }
  auto was_decimal_point = false;
  while (std::isdigit<char>(std::cin.peek(), std::locale()) ||
    std::cin.peek() == std::use_facet<std::numpunct<char>>(std::locale()).decimal_point()) {
    ch = std::cin.get();
    if (ch == std::use_facet<std::numpunct<char>>(std::locale()).decimal_point()) {
      if (was_decimal_point) {
        std::cin.unget();
        break;
      }
      was_decimal_point = true;
    }
    element.value.push_back(ch);
  }
  vector_.push_back(element);
}

void TextFormatter::readDash()
{
  element_t element{element_t::Type::DASH, ""};
  while (std::cin.peek() == '-') {
    const char ch = std::cin.get();
    element.value.push_back(ch);
  }
  vector_.push_back(element);
}

void TextFormatter::readPunct(const char ch)
{
  element_t element{element_t::Type::PUNCT, ""};
  element.value.push_back(ch);
  vector_.push_back(element);
}

bool TextFormatter::checkCorrectness()
{
  if (!vector_.empty() && (vector_.front().type != element_t::Type::WORD) && (vector_.front().type != element_t::Type::
    NUMBER)) {
    return false;
  }
  for (auto iterator = vector_.begin(); iterator != vector_.end(); ++iterator) {
    if (iterator->type == element_t::Type::WORD) {
      if (iterator->value.size() > 20) {
        return false;
      }
    }
    if (iterator->type == element_t::Type::NUMBER) {
      if (iterator->value.size() > 20) {
        return false;
      }
    }
    if (iterator->type == element_t::Type::DASH) {
      if (iterator->value.size() != 3) {
        return false;
      }
      if (iterator != vector_.begin()) {
        const element_t& prev = *std::prev(iterator);
        if ((prev.type == element_t::Type::DASH) || ((prev.type == element_t::Type::PUNCT) && (prev.value != ","))) {
          return false;
        }
      }
    }
    if (iterator->type == element_t::Type::PUNCT) {
      if (iterator != vector_.begin()) {
        const element_t& prev = *std::prev(iterator);
        if (prev.type == element_t::Type::DASH || prev.type == element_t::Type::PUNCT) {
          return false;
        }
      }
    }
  }
  return true;
}

void TextFormatter::printLine(const std::vector<element_t>& line)
{
  for (auto iterator = line.begin(); iterator != line.end(); ++iterator) {
    std::cout << iterator->value;
  }
  std::cout << '\n';
}

std::size_t TextFormatter::remakeLine(std::vector<element_t>& line)
{
  std::size_t line_width = 0;
  std::vector<element_t> new_line;
  while (!line.empty()) {
    new_line.insert(new_line.begin(), line.back());
    line_width += line.back().value.size();
    line.pop_back();
    if (new_line.front().type == element_t::Type::WORD || new_line.front().type == element_t::Type::NUMBER) {
      break;
    }
  }
  printLine(line);
  line = new_line;
  return line_width;
}
