#include "statistics.hpp"

StatisticsCounter::StatisticsCounter() :
  max_(0),
  min_(0),
  mean_(0),
  positive_(0),
  negative_(0),
  odd_sum_(0),
  even_sum_(0),
  equal_(false),
  first_(0),
  last_(0),
  counter_(0)
{
}

void StatisticsCounter::operator()(const int number)
{
  if (counter_ == 0) {
    first_ = number;
    min_ = number;
    max_ = number;
  }
  last_ = number;
  ++counter_;
  if (number > max_) {
    max_ = number;
  }
  if (number < min_) {
    min_ = number;
  }
  if (number > 0) {
    ++positive_;
  }
  if (number < 0) {
    ++negative_;
  }
  number % 2 == 0 ? even_sum_ += number : odd_sum_ += number;
}

int StatisticsCounter::getMax() const
{
  return max_;
}

int StatisticsCounter::getMin() const
{
  return min_;
}

double StatisticsCounter::getMean()
{
  mean_ = static_cast<double>((even_sum_ + odd_sum_)) / static_cast<double>(counter_);
  return mean_;
}

unsigned int StatisticsCounter::getPositive() const
{
  return positive_;
}

unsigned int StatisticsCounter::getNegative() const
{
  return negative_;
}

long long int StatisticsCounter::getEvenSum() const
{
  return even_sum_;
}

long long int StatisticsCounter::getOddSum() const
{
  return odd_sum_;
}

bool StatisticsCounter::getEqual()
{
  first_ == last_ ? equal_ = true : equal_ = false;
  return equal_;
}

unsigned int StatisticsCounter::getCounter() const
{
  return counter_;
}
