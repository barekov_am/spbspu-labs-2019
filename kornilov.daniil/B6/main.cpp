#include <iostream>
#include <algorithm>
#include <iterator>
#include "statistics.hpp"

int main()
{
  try {
    auto statistics_counter = std::for_each(std::istream_iterator<int>(std::cin),
                                            std::istream_iterator<int>(), StatisticsCounter());
    if (std::cin.fail() && !std::cin.eof()) {
      throw std::runtime_error("Reading error!\n");
    }
    if (statistics_counter.getCounter() == 0) {
      std::cout << "No Data\n";
      return 0;
    }
    std::cout << "Max: " << statistics_counter.getMax()
      << "\nMin: " << statistics_counter.getMin()
      << "\nMean: " << statistics_counter.getMean()
      << "\nPositive: " << statistics_counter.getPositive()
      << "\nNegative: " << statistics_counter.getNegative()
      << "\nOdd Sum: " << statistics_counter.getOddSum()
      << "\nEven Sum: " << statistics_counter.getEvenSum()
      << "\nFirst/Last Equal: ";
    statistics_counter.getEqual() ? std::cout << "yes" : std::cout << "no";
  }
  catch (const std::exception& exception) {
    std::cerr << exception.what() << '\n';
    return 1;
  }
  return 0;
}
