#ifndef STATISTICS_HPP
#define STATISTICS_HPP

class StatisticsCounter {

public:
  StatisticsCounter();

  void operator()(int number);

  int getMax() const;
  int getMin() const;
  double getMean();
  unsigned int getPositive() const;
  unsigned int getNegative() const;
  long long int getOddSum() const;
  long long int getEvenSum() const;
  bool getEqual();
  unsigned int getCounter() const;

private:
  int max_;
  int min_;
  double mean_;
  unsigned int positive_;
  unsigned int negative_;
  long long int odd_sum_;
  long long int even_sum_;
  bool equal_;
  int first_;
  int last_;
  unsigned int counter_;
};

#endif
