#include "access-strategies.hpp"

void doTask3()
{
  bool input_zero = false;
  std::vector<int> vector;
  int value = 0;
  while (std::cin >> value)
  {
    if (value == 0)
    {
      input_zero = true;
      break;
    }
    vector.push_back(value);
  }
  if ((std::cin.fail()) && (!std::cin.eof()))
  {
    throw std::runtime_error("Failed to do task 3! Reading error!\n");
  }
  if (vector.empty())
  {
    return;
  }
  if (!input_zero)
  {
    throw std::runtime_error("Failed to do task 3! No zero in the end of input!\n");
  }
  auto i = vector.begin();
  switch (vector.back())
  {
  case 1:
    while (i < vector.end())
    {
      if (*i % 2 == 0)
      {
        i = vector.erase(i);
      }
      else
      {
        ++i;
      }
    }
    break;
  case 2:
    while (i < vector.end())
    {
      if (*i % 3 == 0)
      {
        i = vector.insert(++i, 3, 1);
      }
      ++i;
    }
    break;
  default:
    break;
  }
  printElements(vector);
}
