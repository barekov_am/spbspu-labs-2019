#include <iostream>

void doTask1(char* argv[]);
void doTask2(char* argv[]);
void doTask3();
void doTask4(char* argv[]);

int main(const int args, char* argv[])
{
  try
  {
    if ((args < 2) || (args > 4))
    {
      std::cerr << "Incorrect number of arguments!\n";
      exit(1);
    }
    const int task = std::atoi(argv[1]);
    switch (task)
    {
    case 1:
      if (args != 3)
      {
        std::cerr << "Failed to do Task1! Incorrect arguments!\n";
        return 1;
      }
      doTask1(argv);
      break;
    case 2:
      if (args != 3)
      {
        std::cerr << "Failed to do Task2! Incorrect arguments!\n";
        return 1;
      }
      doTask2(argv);
      break;
    case 3:
      if (args != 2)
      {
        std::cerr << "Failed to do Task3! Incorrect arguments!\n";
        return 1;
      }
      doTask3();
      break;
    case 4:
      if (args != 4)
      {
        std::cerr << "Failed to do Task4! Incorrect arguments!\n";
        return 1;
      }
      doTask4(argv);
      break;
    default:
      std::cerr << "Incorrect task number!\n";
      return 1;
    }
  }
  catch (const std::exception& ex)
  {
    std::cerr << ex.what();
    return 1;
  }
  return 0;
}
