#ifndef ACCESS_STRATEGIES_HPP
#define ACCESS_STRATEGIES_HPP

#include <iostream>
#include <vector>
#include <forward_list>
#include <functional>

template <typename C>
struct BracketsImplementation
{
  using index_t = size_t;

  static typename C::reference getElement(C& container, const size_t index)
  {
    return container[index];
  }

  static size_t getBegin(const C&)
  {
    return 0;
  }

  static size_t getEnd(const C& container)
  {
    return container.size();
  }

  static size_t getNext(const size_t index)
  {
    return index + 1;
  }
};

template <typename C>
struct AtImplementation
{
  using index_t = size_t;

  static typename C::reference getElement(C& container, const size_t index)
  {
    return container.at(index);
  }

  static size_t getBegin(const C&)
  {
    return 0;
  }

  static size_t getEnd(const C& container)
  {
    return container.size();
  }

  static size_t getNext(const size_t index)
  {
    return index + 1;
  };
};

template <typename C>
struct IteratorImplementation
{
  using index_t = typename C::iterator;

  static typename C::reference getElement(const C&, typename C::iterator& iterator)
  {
    return *iterator;
  }

  static typename C::iterator getBegin(C& container)
  {
    return container.begin();
  }

  static typename C::iterator getEnd(C& container)
  {
    return container.end();
  }

  static typename C::iterator getNext(typename C::iterator& iterator)
  {
    return std::next(iterator);
  }
};

template <typename C>
void printElements(const C& container)
{
  for (auto i = container.cbegin(); i != container.cend(); ++i)
  {
    std::cout << *i << ' ';
  }
  std::cout << "\n";
}

template <typename C>
std::function<bool(C, C)> getCmp(const std::string& sort_method)
{
  if (sort_method == "ascending")
  {
    return std::greater<C>();
  }
  if (sort_method == "descending")
  {
    return std::less<C>();
  }
  throw std::invalid_argument("Incorrect sort method!\n");
}

template <template <class C> class Strategy, typename C>
void sort(C& container, std::function<bool(typename C::value_type, typename C::value_type)> cmp)
{
  using index_t = typename Strategy<C>::index_t;
  const index_t container_end = Strategy<C>::getEnd(container);
  for (index_t i = Strategy<C>::getBegin(container); i != container_end; ++i)
  {
    for (index_t j = Strategy<C>::getNext(i); j != container_end; ++j)
    {
      typename C::reference a = Strategy<C>::getElement(container, i);
      typename C::reference b = Strategy<C>::getElement(container, j);
      if (cmp(a, b))
      {
        std::swap(a, b);
      }
    }
  }
}

#endif
