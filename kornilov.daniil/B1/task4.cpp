#include <random>
#include "access-strategies.hpp"

void fillRandom(double* array, const size_t size)
{
  std::random_device random_device;
  std::mt19937 generator(random_device());
  std::uniform_real_distribution<double> distribution(-1.0, 1.0);
  for (size_t i = 0; i < size; i++)
  {
    array[i] = distribution(generator);
  }
}

void doTask4(char* argv[])
{
  const size_t length = std::atoi(argv[3]);
  if (!length)
  {
    throw std::invalid_argument("Failed to do task 4! Incorrect length!\n");
  }
  std::vector<double> vector(length);
  if (!vector.empty())
  {
    fillRandom(&vector[0], vector.size());
    printElements(vector);
    const auto cmp = getCmp<double>(argv[2]);
    sort<BracketsImplementation>(vector, cmp);
    printElements(vector);
  }
}
