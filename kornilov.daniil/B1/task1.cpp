#include "access-strategies.hpp"

void doTask1(char* argv[])
{
  std::forward_list<int> list;
  int value = 0;
  while (std::cin >> value)
  {
    list.push_front(value);
  }
  if ((std::cin.fail()) && (!std::cin.eof()))
  {
    throw std::runtime_error("Failed to do task 1! Reading error!\n");
  }
  std::vector<int> vector1(list.begin(), list.end());
  std::vector<int> vector2(list.begin(), list.end());
  const auto cmp = getCmp<int>(argv[2]);
  sort<BracketsImplementation>(vector1, cmp);
  printElements(vector1);
  sort<AtImplementation>(vector2, cmp);
  printElements(vector2);
  sort<IteratorImplementation>(list, cmp);
  printElements(list);
}
