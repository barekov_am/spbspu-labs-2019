#include <fstream>
#include <memory>
#include <vector>
#include <iostream>

void doTask2(char* argv[])
{
  std::ifstream in_file(argv[2]);
  if (!in_file.is_open())
  {
    throw std::ios_base::failure("Failed to do task 2! File is not open!\n");
  }
  size_t cur_size = 0;
  size_t capacity = 128;
  using char_arr = std::unique_ptr<char[], decltype(&free)>;
  char_arr arr(static_cast<char*>(malloc(capacity)), &free);
  if (!arr)
  {
    throw std::runtime_error("Memory allocation failed!\n");
  }
  while (in_file)
  {
    in_file.read(&arr[cur_size], capacity - cur_size);
    cur_size += in_file.gcount();
    if (capacity == cur_size)
    {
      capacity *= 2;
      arr.reset(static_cast<char*>(realloc(arr.release(), capacity)));
      if (!arr)
      {
        throw std::runtime_error("Memory reallocation failed!\n");
      }
    }
  }
  in_file.close();
  std::vector<char> vector(&arr[0], &arr[cur_size]);
  for (auto i = vector.begin(); i != vector.end(); ++i)
  {
    std::cout << *i;
  }
}
