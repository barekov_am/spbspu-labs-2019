#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

void writeFrameRectInfo(const kornilov::Shape& shape)
{
  kornilov::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "Frame Rectangle: " << "Centre: " << '(' << frameRect.pos.x << ", " << frameRect.pos.y << ')'
            << " Width: " << frameRect.width << " Height: " << frameRect.height << std::endl;
}

void writeArea(const kornilov::Shape& shape)
{
  std::cout << "Area is " << shape.getArea() << std::endl;
}

int main()
{
  try
  {
    std::cout << "Rectangle with centre (0, 0), width = 10 and height = 10" << std::endl;
    kornilov::Rectangle rectangle({{0, 0}, 10, 10});

    std::cout << "Circle with centre (0, 0) and radius = 10" << std::endl;
    kornilov::Circle circle({0, 0}, 10);

    std::cout << "Creating composite shape, consisting of rectangle and circle" << std::endl;
    kornilov::CompositeShape compositeShape;
    
    auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
    auto p_circle = std::make_shared<kornilov::Circle>(circle);
    
    compositeShape.add(p_circle);
    compositeShape.add(p_rectangle);
    writeArea(compositeShape);
    writeFrameRectInfo(compositeShape);

    std::cout << "Moving composite shape to point (10, 10)" << std::endl;
    compositeShape.move({10, 10});
    writeArea(compositeShape);
    writeFrameRectInfo(compositeShape);

    std::cout << "Scaling composite shape with coefficient = 2" << std::endl;
    compositeShape.scale(2);
    writeArea(compositeShape);
    writeFrameRectInfo(compositeShape);

    std::cout << "Write circle's info" << std::endl;
    writeArea(*compositeShape[compositeShape.getIndex(p_circle)]);
    writeFrameRectInfo(*compositeShape[compositeShape.getIndex(p_circle)]);

    std::cout << "Remove circle" << std::endl;
    compositeShape.remove((compositeShape.getIndex(p_circle)));
    writeArea(compositeShape);
    writeFrameRectInfo(compositeShape);
  }

  catch (const std::exception& ex)
  {
    std::cerr << ex.what() << std::endl;
    return 1;
  }

  return 0;
}
