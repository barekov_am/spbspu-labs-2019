#ifndef FACTORIAL_HPP
#define FACTORIAL_HPP

#include <iterator>

class FactorialContainer {
public:
  class Iterator : public std::iterator<std::bidirectional_iterator_tag, size_t> {
  public:
    Iterator(unsigned int value, unsigned int index);

    Iterator& operator++();
    Iterator operator++(int);
    Iterator& operator--();
    Iterator operator--(int);

    bool operator==(const Iterator& other) const;
    bool operator!=(const Iterator& other) const;

    const unsigned int& operator*() const;
    const unsigned int* operator->() const;
  private:
    unsigned int value_, index_;
  };

  static Iterator begin();
  static Iterator end();
};

#endif
