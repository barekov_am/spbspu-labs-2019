#include "PhoneBook.hpp"

PhoneBook::PhoneBook() :
  current_contact_(contact_list_.end())
{
}

PhoneBook::iterator PhoneBook::insertBeforeContact(const iterator& position, const contact_t& contact)
{
  current_contact_ = contact_list_.insert(position, contact);
  return current_contact_;
}

PhoneBook::iterator PhoneBook::insertAfterContact(const iterator& position, const contact_t& contact)
{
  current_contact_ = contact_list_.insert(std::next(position), contact);
  return current_contact_;
}

PhoneBook::iterator PhoneBook::addContact(const contact_t& contact)
{
  contact_list_.push_back(contact);
  if (contact_list_.size() != 1) {
    current_contact_ = std::prev(contact_list_.end());
  } else {
    current_contact_ = contact_list_.begin();
  }
  return current_contact_;
}

PhoneBook::iterator PhoneBook::eraseContact(iterator& position)
{
  return contact_list_.erase(position);
}

PhoneBook::iterator PhoneBook::getBegin()
{
  return contact_list_.begin();
}

PhoneBook::iterator PhoneBook::getEnd()
{
  return contact_list_.end();
}

bool PhoneBook::empty() const
{
  return contact_list_.empty();
}
