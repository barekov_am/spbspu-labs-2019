#include <iostream>
#include <algorithm>
#include "PhoneInterface.hpp"
#include "FactorialContainer.hpp"

int main(const int args, char* argv[])
{
  try {
    if (args != 2) {
      std::cerr << "Invalid number of arguments!\n";
      return 1;
    }

    char* ptr = nullptr;
    const int task_number = strtol(argv[1], &ptr, 10);
    if (*ptr != '\0' || task_number < 1 || task_number > 2) {
      std::cerr << "Invalid task number!\n";
      return 1;
    }

    if (task_number == 1) {
      PhoneInterface phone_interface;
      phone_interface.readCommand();
    } else if (task_number == 2) {
      std::copy(FactorialContainer::begin(), FactorialContainer::end(),
                std::ostream_iterator<unsigned int>(std::cout, " "));
      std::cout << '\n';

      std::reverse_copy(FactorialContainer::begin(), FactorialContainer::end(),
                        std::ostream_iterator<unsigned int>(std::cout, " "));
      std::cout << '\n';
    } else {
      std::cerr << "Invalid task number!\n";
      return 1;
    }
  }
  catch (const std::exception& ex) {
    std::cerr << ex.what() << '\n';
    return 1;
  }

  return 0;
}
