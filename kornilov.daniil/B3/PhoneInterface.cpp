#include "PhoneInterface.hpp"
#include <iostream>
#include <sstream>
#include <algorithm>

std::istream& operator>>(std::istream& stream, PhoneBook::contact_t& contact)
{
  const std::istream::sentry sentry(stream);
  if (sentry) {
    stream >> std::ws >> contact.first;
    char special_symbol;
    stream >> special_symbol;
    if (stream.fail()) {
      return stream;
    }
    if (special_symbol != '\"') {
      stream.setstate(std::istream::failbit);
      return stream;
    }
    std::string temp;
    std::getline(stream, temp);
    temp = temp.substr(0, temp.rfind('\"'));
    temp.erase(std::remove(temp.begin(), temp.end(), '\\'), temp.end());
    contact.second = temp;
  }
  return stream;
}

PhoneInterface::PhoneInterface()
{
  bookmarks_.emplace("current", book_.getEnd());
}

void PhoneInterface::readCommand()
{
  while (std::cin) {
    std::string input;
    std::getline(std::cin, input);
    std::istringstream line(input);

    if (std::cin.fail()) {
      if (std::cin.eof()) {
        break;
      }
      throw std::runtime_error("Invalid input!\n");
    }
    std::string input_cmd, temp;
    line >> input_cmd;
    if (input_cmd == "add") {
      PhoneBook::contact_t record;
      line >> record;
      if (!line.fail()) {
        add(record);
      } else {
        std::cout << "<INVALID COMMAND>\n";
      }
    } else if (input_cmd == "store") {
      std::string mark_name, new_mark_name;
      line >> mark_name;
      line >> new_mark_name;
      store(mark_name, new_mark_name);
    } else if (input_cmd == "insert") {
      std::string mark_name;
      line >> temp;
      line >> mark_name;
      PhoneBook::contact_t contact;
      line >> contact;

      if (!line.fail()) {
        if (temp == "before") {
          insertBefore(mark_name, contact);
        } else if (temp == "after") {
          insertAfter(mark_name, contact);
        }
      } else {
        std::cout << "<INVALID COMMAND>\n";
      }
    } else if (input_cmd == "delete") {
      line >> temp;
      erase(temp);
    } else if (input_cmd == "show") {
      line >> temp;
      show(temp);
    } else if (input_cmd == "move") {
      line >> temp;
      std::string move_cmd;
      line >> move_cmd;
      if (move_cmd == "first") {
        moveFirst(temp);
      } else if (move_cmd == "last") {
        moveLast(temp);
      } else {
        std::istringstream stream(move_cmd);
        difference_type steps;
        stream >> steps;

        if (stream.fail()) {
          std::cout << "<INVALID STEP>\n";
        }
        move(temp, steps);
      }
    } else {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}

void PhoneInterface::show(const std::string& mark_name)
{
  const auto temp = bookmarks_.find(mark_name);
  if (temp != bookmarks_.end()) {
    if (temp->second != book_.getEnd()) {
      std::cout << temp->second->first << ' ' << temp->second->second << '\n';
    } else {
      std::cout << "<EMPTY>\n";
    }
  } else {
    std::cout << "<INVALID BOOKMARK>\n";
  }
}

void PhoneInterface::add(const PhoneBook::contact_t& contact)
{
  if (book_.empty()) {
    const auto iterator = book_.addContact(contact);
    auto current_position = bookmarks_.find("current");
    current_position->second = iterator;
  } else {
    book_.addContact(contact);
  }
}

void PhoneInterface::addWithBookmark(const std::string& mark_name, const std::string& number, const std::string& name)
{
  const auto record = std::make_pair(number, name);
  auto iterator = book_.addContact(record);
  bookmarks_.emplace(mark_name, iterator);
}

void PhoneInterface::store(const std::string& mark_name, const std::string& new_mark_name)
{
  auto iterator = bookmarks_.find(mark_name);
  if (iterator != bookmarks_.end()) {
    if (!bookmarks_.emplace(new_mark_name, iterator->second).second) {
      bookmarks_[new_mark_name] = iterator->second;
    }
  } else {
    std::cout << "<INVALID BOOKMARK>\n";
  }
}

void PhoneInterface::insertBefore(const std::string& mark_name, const PhoneBook::contact_t& contact)
{
  auto position = bookmarks_.find(mark_name);
  if (position != bookmarks_.end()) {
    const auto iterator = book_.insertBeforeContact(position->second, contact);
    if (bookmarks_.size() == 1) {
      position->second = iterator;
    }
  } else {
    std::cout << "<INVALID BOOKMARK>\n";
  }
}

void PhoneInterface::insertAfter(const std::string& mark_name, const PhoneBook::contact_t& contact)
{
  auto position = bookmarks_.find(mark_name);
  if (position != bookmarks_.end()) {
    const auto iterator = book_.insertAfterContact(position->second, contact);
    if (bookmarks_.size() == 1) {
      position->second = iterator;
    }
  } else {
    std::cout << "<INVALID BOOKMARK>\n";
  }
}

void PhoneInterface::move(const std::string& mark_name, const difference_type step_number)
{
  auto position = bookmarks_.find(mark_name);

  if (position != bookmarks_.end()) {
    for (auto count = 0; count < std::abs(step_number); ++count) {
      step_number > 0 ? ++position->second : --position->second;
    }
  }
}

void PhoneInterface::moveFirst(const std::string& mark_name)
{
  auto position = bookmarks_.find(mark_name);
  position->second = book_.getBegin();
}

void PhoneInterface::moveLast(const std::string& mark_name)
{
  auto position = bookmarks_.find(mark_name);
  position->second = std::prev(book_.getEnd());
}

void PhoneInterface::erase(const std::string& mark_name)
{
  if (!book_.empty()) {
    auto position = bookmarks_.find(mark_name);
    const auto erase_iterator = position->second;
    position->second = book_.eraseContact(position->second);
    if (position->second == book_.getEnd()) {
      std::advance(position->second, -1);
    }
    for (auto iterator = bookmarks_.begin(); iterator != bookmarks_.end(); ++iterator) {
      if (iterator->second == erase_iterator) {
        iterator->second = position->second;
      }
    }
  }
}
