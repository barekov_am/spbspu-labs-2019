#ifndef PHONE_INTERFACE_HPP
#define PHONE_INTERFACE_HPP

#include <map>
#include "PhoneBook.hpp"

class PhoneInterface {
public:
  typedef std::pair<std::string, PhoneBook::iterator> bookmark_t;
  typedef std::map<std::string, PhoneBook::iterator> bookmark_list_t;
  typedef bookmark_list_t::iterator iterator;
  typedef std::list<PhoneBook::contact_t>::difference_type difference_type;

  PhoneInterface();

  void readCommand();

private:
  bookmark_list_t bookmarks_;
  PhoneBook book_;
  void show(const std::string& mark_name);
  void add(const PhoneBook::contact_t& contact);
  void addWithBookmark(const std::string& mark_name, const std::string& number, const std::string& name);
  void store(const std::string& mark_name, const std::string& new_mark_name);
  void insertBefore(const std::string& mark_name, const PhoneBook::contact_t& contact);
  void insertAfter(const std::string& mark_name, const PhoneBook::contact_t& contact);
  void move(const std::string& mark_name, difference_type step_number);
  void moveFirst(const std::string& mark_name);
  void moveLast(const std::string& mark_name);
  void erase(const std::string& mark_name);
};

#endif
