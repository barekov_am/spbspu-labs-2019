#ifndef PHONE_BOOK_HPP
#define PHONE_BOOK_HPP

#include <string>
#include <list>

class PhoneBook {
public:
  typedef std::pair<std::string, std::string> contact_t;
  typedef std::list<contact_t>::iterator iterator;

  PhoneBook();

  iterator insertBeforeContact(const iterator& position, const contact_t& contact);
  iterator insertAfterContact(const iterator& position, const contact_t& contact);
  iterator addContact(const contact_t& contact);
  iterator eraseContact(iterator& position);
  iterator getBegin();
  iterator getEnd();
  bool empty() const;

private:
  std::list<contact_t> contact_list_;
  iterator current_contact_;
};

#endif
