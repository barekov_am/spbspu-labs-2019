#include "FactorialContainer.hpp"

FactorialContainer::Iterator FactorialContainer::begin()
{
  return {1, 1};
}

FactorialContainer::Iterator FactorialContainer::end()
{
  return {39916800, 11};
}

FactorialContainer::Iterator::Iterator(const unsigned int value, const unsigned int index):
  value_(value),
  index_(index)
{
}

FactorialContainer::Iterator& FactorialContainer::Iterator::operator++()
{
  if (index_ < 11) {
    index_ ++;
    value_ *= index_;
  }
  return *this;
}

FactorialContainer::Iterator FactorialContainer::Iterator::operator++(int)
{
  const auto old = *this;
  if (index_ < 11) {
    index_++;
    value_ *= index_;
  }
  return old;
}

FactorialContainer::Iterator& FactorialContainer::Iterator::operator--()
{
  if (index_ > 1) {
    value_ /= index_;
    --index_;
  }
  return *this;
}

FactorialContainer::Iterator FactorialContainer::Iterator::operator--(int)
{
  const auto old = *this;
  if (index_ > 1) {
    value_ /= index_;
    --index_;
  }
  return old;
}

bool FactorialContainer::Iterator::operator==(const Iterator& other) const
{
  return index_ == other.index_;
}

bool FactorialContainer::Iterator::operator!=(const Iterator& other) const
{
  return index_ != other.index_;
}

const unsigned int& FactorialContainer::Iterator::operator*() const
{
  return value_;
}

const unsigned int* FactorialContainer::Iterator::operator->() const
{
  return &value_;
}
