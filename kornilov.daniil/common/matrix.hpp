#ifndef MATRIX_H
#define MATRIX_H

#include <memory>
#include "composite-shape.hpp"

namespace kornilov
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix& rhs);
    Matrix(Matrix&& rhs);
    ~Matrix() = default;
    
    Matrix& operator=(const Matrix& rhs);
    Matrix& operator=(Matrix&& rhs);
    std::unique_ptr<std::shared_ptr<Shape>[]> operator[](const size_t row) const;

    size_t getRows() const;
    size_t getCols() const;
    size_t getSizeOfLayer(const size_t layer) const;
    void writeInfo() const;
    
  private:
    std::unique_ptr<std::shared_ptr<Shape>[]> shapeMatrix_;
    size_t rows_, cols_;

    void add(std::shared_ptr<Shape> shape, const size_t row, const size_t col);
    friend Matrix split(const CompositeShape& compShape);
  };
}

#endif
