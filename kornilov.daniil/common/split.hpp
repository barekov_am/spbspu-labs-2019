#ifndef SPLIT_H
#define SPLIT_H

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace kornilov
{
  bool intersect(const std::shared_ptr<Shape> shapeA, const std::shared_ptr<Shape> shapeB);
  Matrix split(const CompositeShape& compShape);
}

#endif
