#include "matrix.hpp"
#include <stdexcept>
#include <iostream>
#include <algorithm>

kornilov::Matrix::Matrix():
  rows_(0),
  cols_(0)
{
}

kornilov::Matrix::Matrix(const Matrix& rhs):
  shapeMatrix_(std::make_unique<std::shared_ptr<Shape>[]>(rhs.rows_ * rhs.cols_)),
  rows_(rhs.rows_),
  cols_(rhs.cols_)
{
  for (size_t i = 0; i < (rows_ * cols_); i++)
  {
    shapeMatrix_[i] = rhs.shapeMatrix_[i];
  }
}

kornilov::Matrix::Matrix(Matrix&& rhs):
  shapeMatrix_(std::move(rhs.shapeMatrix_)),
  rows_(rhs.rows_),
  cols_(rhs.cols_)
{
  rhs.rows_ = 0;
  rhs.cols_ = 0;
}

kornilov::Matrix& kornilov::Matrix::operator=(const Matrix& rhs)
{
  if (this != &rhs)
  {
    rows_ = rhs.rows_;
    cols_ = rhs.cols_;
    std::unique_ptr<std::shared_ptr<Shape>[]> tempMatrix(std::make_unique<std::shared_ptr<Shape>[]>(rows_ * cols_));
    for (size_t i = 0; i < (rows_ * cols_); i++)
    {
      tempMatrix[i] = rhs.shapeMatrix_[i];
    }
    shapeMatrix_.swap(tempMatrix);
  }
  return *this;
}

kornilov::Matrix& kornilov::Matrix::operator=(Matrix&& rhs)
{
  if (this != &rhs)
  {
    rows_ = rhs.rows_;
    cols_ = rhs.cols_;
    shapeMatrix_ = std::move(rhs.shapeMatrix_);
    rhs.rows_ = 0;
    rhs.cols_ = 0;
  }
  return *this;
}

std::unique_ptr<std::shared_ptr<kornilov::Shape>[]> kornilov::Matrix::operator[](const size_t row) const
{
  if (row >= rows_)
  {
    throw std::out_of_range("Invalid index!");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> tempMatrix(std::make_unique<std::shared_ptr<Shape>[]>(cols_));
  for (size_t i = 0; i < cols_; i++)
  {
    tempMatrix[i] = shapeMatrix_[row * cols_ + i];
  }
  return tempMatrix;
}

size_t kornilov::Matrix::getRows() const
{
  return rows_;
}

size_t kornilov::Matrix::getCols() const
{
  return cols_;
}

void kornilov::Matrix::writeInfo() const
{
  for (size_t i = 0; i < rows_; i++)
  {
    std::cout << "Layer " << i << ": " << std::endl;
    const size_t layerSize = getSizeOfLayer(i);
    for (size_t j = 0; j < layerSize; j++)
    {
      shapeMatrix_[i * cols_ + j]->writeInfo();
    }
  }
}

size_t kornilov::Matrix::getSizeOfLayer(const size_t layer) const
{
  if (layer >= rows_)
  {
    throw std::out_of_range("Invalid index!");
  }
  size_t layerSize = 0;
  for (size_t i = 0; i < cols_; i++)
  {
    if (shapeMatrix_[cols_ * layer + i] != nullptr)
    {
      layerSize++;
    }
  }
  return layerSize;
}

void kornilov::Matrix::add(std::shared_ptr<Shape> shape, const size_t row, const size_t col)
{
  size_t tempRows = std::max(rows_, row + 1);
  size_t tempCols = std::max(cols_, col + 1);
  std::unique_ptr<std::shared_ptr<Shape>[]> tempMatrix(std::make_unique<std::shared_ptr<Shape>[]>(tempRows * tempCols));
  for (size_t i = 0; i < rows_; i++)
  {
    for (size_t j = 0; j < cols_; j++)
    {
      tempMatrix[i * tempCols + j] = shapeMatrix_[i * cols_ + j];
    }
  }
  tempMatrix[row * tempCols + col] = shape;
  shapeMatrix_.swap(tempMatrix);
  rows_ = tempRows;
  cols_ = tempCols;
}
