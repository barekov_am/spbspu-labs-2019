#define _USE_MATH_DEFINES

#include "rectangle.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>

kornilov::Rectangle::Rectangle(const rectangle_t& rectangle):
  pos_(rectangle.pos),
  width_(rectangle.width),
  height_(rectangle.height),
  angle_(0)
{
  if ((width_ <= 0) || (height_ <= 0))
  {
    throw std::invalid_argument("Invalid height or width!");
  }
}

double kornilov::Rectangle::getArea() const
{
  return width_ * height_;
}

kornilov::rectangle_t kornilov::Rectangle::getFrameRect() const
{
  const double radAngle = angle_ * M_PI / 180;
  const double cosAngle = cos(radAngle);
  const double sinAngle = sin(radAngle);
  return 
  {
     pos_,
     std::abs(width_ * cosAngle) + std::abs(height_ * sinAngle),
     std::abs(width_ * sinAngle) + std::abs(height_ * cosAngle)
  };
}

void kornilov::Rectangle::move(const double dx, const double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void kornilov::Rectangle::move(const point_t& point)
{
  pos_ = point;
}

void kornilov::Rectangle::scale(const double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Invalid coefficient!");
  }
  width_ *= coefficient;
  height_ *= coefficient;
}

void kornilov::Rectangle::rotate(const double angle)
{
  angle_ += angle;
}

void kornilov::Rectangle::writeInfo() const
{
  std::cout << "Rectangle: " << "Width: " << width_ << " Height: " << height_
            << " Centre: " << '(' << pos_.x << ", " << pos_.y << ')' << std::endl;
}
