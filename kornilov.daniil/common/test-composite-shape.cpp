#include <boost/test/auto_unit_test.hpp>

#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

const double CHECKERROR = 0.0001;

BOOST_AUTO_TEST_SUITE(TestCompositeShape)

BOOST_AUTO_TEST_CASE(FrameRectPermanencyAfterMovingdXdY)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  const kornilov::rectangle_t rect = compositeShape.getFrameRect();
  compositeShape.move(10, 10);
  BOOST_CHECK_CLOSE(rect.height, compositeShape.getFrameRect().height, CHECKERROR);
  BOOST_CHECK_CLOSE(rect.width, compositeShape.getFrameRect().width, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(FrameRectPermanencyAfterMovingToPoint)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  const kornilov::rectangle_t rect = compositeShape.getFrameRect();
  compositeShape.move({10, 10});
  BOOST_CHECK_CLOSE(rect.height, compositeShape.getFrameRect().height, CHECKERROR);
  BOOST_CHECK_CLOSE(rect.width, compositeShape.getFrameRect().width, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(AreaPermanencyAfterMovingdXdY)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  const double compArea = compositeShape.getArea();
  compositeShape.move(10, 10);
  BOOST_CHECK_CLOSE(compArea, compositeShape.getArea(), CHECKERROR);
}

BOOST_AUTO_TEST_CASE(AreaPermanencyAfterMovingToPoint)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  const double compArea = compositeShape.getArea();
  compositeShape.move({10, 10});
  BOOST_CHECK_CLOSE(compArea, compositeShape.getArea(), CHECKERROR);
}

BOOST_AUTO_TEST_CASE(SquareChangingAfterScaling)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  const double compArea = compositeShape.getArea();
  const double coef = 2;
  compositeShape.scale(coef);
  BOOST_CHECK_CLOSE(coef * coef, compositeShape.getArea() / compArea, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(InvalidShapeException)
{
  kornilov::CompositeShape compositeShape;
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  compositeShape.add(p_rectangle);
  BOOST_CHECK_THROW(compositeShape.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.getIndex(nullptr), std::invalid_argument);
  compositeShape.remove(0);
  BOOST_CHECK_THROW(compositeShape.getIndex(p_rectangle), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(InvalidIndexException)
{
  size_t index = 4;
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  BOOST_CHECK_THROW(compositeShape[index], std::out_of_range);
  BOOST_CHECK_THROW(compositeShape.remove(index), std::out_of_range);
  index = -1;
  BOOST_CHECK_THROW(compositeShape[index], std::out_of_range);
  BOOST_CHECK_THROW(compositeShape.remove(index), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(InvalidCoefficientException)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  const double coef = 0;
  BOOST_CHECK_THROW(compositeShape.scale(coef), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestCopyConstructor)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  kornilov::CompositeShape compTest(compositeShape);
  BOOST_CHECK_CLOSE(compTest.getArea(), compositeShape.getArea(), CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().width, compositeShape.getFrameRect().width, CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().height, compositeShape.getFrameRect().height, CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().pos.x, compositeShape.getFrameRect().pos.x, CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().pos.y, compositeShape.getFrameRect().pos.y, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(TestMoveConstructor)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  const double compArea = compositeShape.getArea();
  const kornilov::rectangle_t rect = compositeShape.getFrameRect();
  kornilov::CompositeShape compTest(std::move(compositeShape));
  BOOST_CHECK_CLOSE(compTest.getArea(), compArea, CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().width, rect.width, CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().height, rect.height, CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().pos.x, rect.pos.x, CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().pos.y, rect.pos.y, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(TestCopyOperatorOfAssignment)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  kornilov::CompositeShape compTest;
  compTest = compositeShape;
  BOOST_CHECK_CLOSE(compTest.getArea(), compositeShape.getArea(), CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().width, compositeShape.getFrameRect().width, CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().height, compositeShape.getFrameRect().height, CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().pos.x, compositeShape.getFrameRect().pos.x, CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().pos.y, compositeShape.getFrameRect().pos.y, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(TestMoveOperatorOfAssignment)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  const double compArea = compositeShape.getArea();
  const kornilov::rectangle_t rect = compositeShape.getFrameRect();
  kornilov::CompositeShape compTest;
  compTest = std::move(compositeShape);
  BOOST_CHECK_CLOSE(compTest.getArea(), compArea, CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().width, rect.width, CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().height, rect.height, CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().pos.x, rect.pos.x, CHECKERROR);
  BOOST_CHECK_CLOSE(compTest.getFrameRect().pos.y, rect.pos.y, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(TestIndexOperator)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  BOOST_CHECK_CLOSE(circle.getArea(), compositeShape[1]->getArea(), CHECKERROR);
}

BOOST_AUTO_TEST_CASE(TestAdd)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  BOOST_CHECK_CLOSE(compositeShape.getArea(), rectangle.getArea() + circle.getArea(), CHECKERROR);
}

BOOST_AUTO_TEST_CASE(TestRemove)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  compositeShape.remove(1);
  BOOST_CHECK_CLOSE(rectangle.getArea(), compositeShape.getArea(), CHECKERROR);
}

BOOST_AUTO_TEST_CASE(TestGetIndex)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  const double compArea = compositeShape.getArea();
  const kornilov::rectangle_t rect = compositeShape[compositeShape.getIndex(p_circle)]->getFrameRect();
  BOOST_CHECK_CLOSE(circle.getFrameRect().width, rect.width, CHECKERROR);
  BOOST_CHECK_CLOSE(circle.getFrameRect().height, rect.height, CHECKERROR);
  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.x, rect.pos.x, CHECKERROR);
  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.y, rect.pos.y, CHECKERROR);
  const size_t shapeIndex = compositeShape.getIndex(p_circle);
  compositeShape.remove(shapeIndex);
  BOOST_CHECK_CLOSE(compArea, compositeShape.getArea() + circle.getArea(), CHECKERROR);
}

BOOST_AUTO_TEST_CASE(PermanencyAfterRotating)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  const double compArea = compositeShape.getArea();
  const kornilov::rectangle_t rect = compositeShape.getFrameRect();
  compositeShape.rotate(90);
  compositeShape.rotate(60);
  compositeShape.rotate(20);
  compositeShape.rotate(10);
  BOOST_CHECK_CLOSE(compArea, compositeShape.getArea(), CHECKERROR);
  BOOST_CHECK_CLOSE(rect.height, compositeShape.getFrameRect().height, CHECKERROR);
  BOOST_CHECK_CLOSE(rect.width, compositeShape.getFrameRect().width, CHECKERROR);
  BOOST_CHECK_CLOSE(rect.pos.x, compositeShape.getFrameRect().pos.x, CHECKERROR);
  BOOST_CHECK_CLOSE(rect.pos.y, compositeShape.getFrameRect().pos.y, CHECKERROR);
}

BOOST_AUTO_TEST_SUITE_END()
