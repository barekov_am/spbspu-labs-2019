#define _USE_MATH_DEFINES

#include "composite-shape.hpp"
#include <stdexcept>
#include <algorithm>
#include <cmath>
#include <iostream>

kornilov::CompositeShape::CompositeShape():
  size_(0)
{
}

kornilov::CompositeShape::CompositeShape(const CompositeShape& rhs):
  shapeArr_(std::make_unique<std::shared_ptr<Shape>[]>(rhs.size_)),
  size_(rhs.size_)
{
  for (size_t i = 0; i < size_; i++)
  {
    shapeArr_[i] = rhs.shapeArr_[i];
  }
}

kornilov::CompositeShape::CompositeShape(CompositeShape&& rhs):
  shapeArr_(std::move(rhs.shapeArr_)),
  size_(rhs.size_)
{
  rhs.size_ = 0;
}

kornilov::CompositeShape& kornilov::CompositeShape::operator=(const CompositeShape& rhs)
{
  if (this != &rhs)
  {
    size_ = rhs.size_;
    std::unique_ptr<std::shared_ptr<Shape>[]> tempArr(std::make_unique<std::shared_ptr<Shape>[]>(size_));
    for (size_t i = 0; i < size_; i++)
    {
      tempArr[i] = rhs.shapeArr_[i];
    }
    shapeArr_.swap(tempArr);
  }
  return *this;
}

kornilov::CompositeShape& kornilov::CompositeShape::operator=(CompositeShape&& rhs)
{
  if (this != &rhs)
  {
    size_ = rhs.size_;
    shapeArr_ = std::move(rhs.shapeArr_);
    rhs.size_ = 0;
  }
  return *this;
}

std::shared_ptr<kornilov::Shape> kornilov::CompositeShape::operator[](const size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Invalid index!");
  }
  return shapeArr_[index];
}

void kornilov::CompositeShape::add(std::shared_ptr<Shape> shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Invalid shape!");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> tempArr(std::make_unique<std::shared_ptr<Shape>[]>(size_ + 1));
  for (size_t i = 0; i < size_; i++)
  {
    tempArr[i] = shapeArr_[i];
  }
  tempArr[size_] = shape;
  size_++;
  shapeArr_.swap(tempArr);
}

void kornilov::CompositeShape::remove(const size_t index)
{
  if (index >= size_)
  {
    throw std::out_of_range("Invalid index!");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> tempArr(std::make_unique<std::shared_ptr<Shape>[]>(size_ - 1));
  for (size_t i = 0; i < index; i++)
  {
    tempArr[i] = shapeArr_[i];
  }
  for (size_t i = index; i < size_ - 1; i++)
  {
    tempArr[i] = shapeArr_[i + 1];
  }
  size_--;
  shapeArr_.swap(tempArr);
}

size_t kornilov::CompositeShape::getIndex(const std::shared_ptr<Shape> shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Invalid shape!");
  }
  for (size_t i = 0; i < size_; i++)
  {
    if (shapeArr_[i] == shape)
    {
      return i;
    }
  }
  throw std::invalid_argument("Invalid shape!");
}

double kornilov::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < size_; i++)
  {
    area += shapeArr_[i]->getArea();
  }
  return area;
}

kornilov::rectangle_t kornilov::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    return {{0, 0}, 0, 0};
  }
  rectangle_t rect = shapeArr_[0]->getFrameRect();
  double maxY = rect.pos.y + rect.height / 2;
  double minY = rect.pos.y - rect.height / 2;
  double maxX = rect.pos.x + rect.width / 2;
  double minX = rect.pos.x - rect.width / 2;
  for (size_t i = 1; i < size_; i++)
  {
    rect = shapeArr_[i]->getFrameRect();
    maxY = std::max(rect.pos.y + rect.height / 2, maxY);
    minY = std::min(rect.pos.y - rect.height / 2, minY);
    maxX = std::max(rect.pos.x + rect.width / 2, maxX);
    minX = std::min(rect.pos.x - rect.width / 2, minX);
  }
  return {{(minX + maxX) / 2, (minY + maxY) / 2}, maxX - minX, maxY - minY};
}

size_t kornilov::CompositeShape::getSize() const
{
  return size_;
}

void kornilov::CompositeShape::move(const double dx, const double dy)
{
  for (size_t i = 0; i < size_; i++)
  {
    shapeArr_[i]->move(dx, dy);
  }
}

void kornilov::CompositeShape::move(const point_t& point)
{
  const point_t frameRectCentre = getFrameRect().pos;
  const double dx = point.x - frameRectCentre.x;
  const double dy = point.y - frameRectCentre.y;
  move(dx, dy);
}

void kornilov::CompositeShape::scale(const double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Invalid coefficient!");
  }
  const point_t frameRectCentre = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    const double dx = shapeArr_[i]->getFrameRect().pos.x - frameRectCentre.x;
    const double dy = shapeArr_[i]->getFrameRect().pos.y - frameRectCentre.y;
    shapeArr_[i]->move({frameRectCentre.x + dx * coefficient, frameRectCentre.y + dy * coefficient});
    shapeArr_[i]->scale(coefficient);
  }
}

void kornilov::CompositeShape::rotate(const double angle)
{
  const point_t frameRectCentre = getFrameRect().pos;
  const double radAngle = angle * M_PI / 180;
  const double cosAngle = cos(radAngle);
  const double sinAngle = sin(radAngle);
  for (size_t i = 0; i < size_; i++)
  {
    const double dx = shapeArr_[i]->getFrameRect().pos.x - frameRectCentre.x;
    const double dy = shapeArr_[i]->getFrameRect().pos.y - frameRectCentre.y;
    const double moveDX = dx * cosAngle - dy * sinAngle;
    const double moveDY = dx * sinAngle + dy * cosAngle;
    shapeArr_[i]->move({frameRectCentre.x + moveDX, frameRectCentre.y + moveDY});
    shapeArr_[i]->rotate(angle);
  }
}

void kornilov::CompositeShape::writeInfo() const
{
  const rectangle_t frameRect = getFrameRect();
  std::cout << "Composite shape: " << "Frame rectangle: "
            << "Width: " << frameRect.width << " Height: " << frameRect.height
            << " Centre: " << '(' << frameRect.pos.x << ", " << frameRect.pos.y << ')' << std::endl;
}
