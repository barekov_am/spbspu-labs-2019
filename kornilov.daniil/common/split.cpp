#include "split.hpp"
#include <cmath>

bool kornilov::intersect(const std::shared_ptr<Shape> shapeA, const std::shared_ptr<Shape> shapeB)
{
  const rectangle_t frameRectA = shapeA->getFrameRect();
  const rectangle_t frameRectB = shapeB->getFrameRect();
  const double dx = std::abs(frameRectA.pos.x - frameRectB.pos.x);
  const double dy = std::abs(frameRectA.pos.y - frameRectB.pos.y);
  const double width = (frameRectA.width + frameRectB.width) / 2;
  const double height = (frameRectA.height + frameRectB.height) / 2;
  return ((dx <= width) && (dy <= height));
}

kornilov::Matrix kornilov::split(const CompositeShape& compShape)
{
  Matrix matrix;
  const size_t compSize = compShape.getSize();
  for (size_t i = 0; i < compSize; i++)
  {
    size_t tempRow = 0;
    size_t tempCol = 0;
    for (size_t j = 0; j < matrix.rows_; j++)
    {
      const size_t layerSize = matrix.getSizeOfLayer(j);
      for (size_t k = 0; k < layerSize; k++)
      {
        if (intersect(compShape[i], matrix[j][k]))
        {
          tempRow++;
          break;
        }
        if (k == layerSize - 1)
        {
          tempRow = j;
          tempCol = layerSize;
        }
      }
      if (tempRow == j)
      {
        break;
      }
    }
    matrix.add(compShape[i], tempRow, tempCol);
  }
  return matrix;
}
