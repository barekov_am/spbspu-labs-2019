#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

const double CHECKERROR = 0.0001;

BOOST_AUTO_TEST_SUITE(TestRectangle)

BOOST_AUTO_TEST_CASE(WidthPermanencyAfterMovingdXdY)
{
  const double width = 10.1;
  kornilov::Rectangle rectangle({{0, 0}, width, 11});
  rectangle.move(10, 10);
  BOOST_CHECK_CLOSE(width, rectangle.getFrameRect().width, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(WidthPermanencyAfterMovingToPoint)
{
  const double width = 10.1;
  kornilov::Rectangle rectangle({{0, 0}, width, 11});
  rectangle.move({10, 10});
  BOOST_CHECK_CLOSE(width, rectangle.getFrameRect().width, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(HeightPermanencyAfterMovingdXdY)
{
  const double height = 11.1;
  kornilov::Rectangle rectangle({{0, 0}, 10, height});
  rectangle.move(10, 10);
  BOOST_CHECK_CLOSE(height, rectangle.getFrameRect().height, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(HeightPermanencyAfterMovingToPoint)
{
  const double height = 11.1;
  kornilov::Rectangle rectangle({{0, 0}, 10, height});
  rectangle.move({10, 10});
  BOOST_CHECK_CLOSE(height, rectangle.getFrameRect().height, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(AreaPermanencyAfterMovingdXdY)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11.1});
  const double rectArea = rectangle.getArea();
  rectangle.move(10, 10);
  BOOST_CHECK_CLOSE(rectArea, rectangle.getArea(), CHECKERROR);
}

BOOST_AUTO_TEST_CASE(AreaPermanencyAfterMovingToPoint)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11.1});
  const double rectArea = rectangle.getArea();
  rectangle.move({10, 10});
  BOOST_CHECK_CLOSE(rectArea, rectangle.getArea(), CHECKERROR);
}

BOOST_AUTO_TEST_CASE(SquareChangingAfterScaling)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11.1});
  const double rectArea = rectangle.getArea();
  const double coef = 2.1;
  rectangle.scale(coef);
  BOOST_CHECK_CLOSE(coef * coef, rectangle.getArea() / rectArea, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(InvalidWidth)
{
  const double width = -0.1;
  BOOST_CHECK_THROW(const kornilov::Rectangle rectangle({{0, 0}, width, 11}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(InvalidHeight)
{
  const double height = -0.1;
  BOOST_CHECK_THROW(const kornilov::Rectangle rectangle({{0, 0}, 10, height}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(InvalidCoefficient)
{
  const double coef = -0.1;
  kornilov::Rectangle rectangle({{0, 0}, 10, 11});
  BOOST_CHECK_THROW(rectangle.scale(coef), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(AreaPermanencyAfterRotating)
{
  kornilov::Rectangle rectangle({{0, 0}, 10, 11});
  const double rectArea = rectangle.getArea();
  rectangle.rotate(90);
  rectangle.rotate(60);
  rectangle.rotate(20);
  BOOST_CHECK_CLOSE(rectArea, rectangle.getArea(), CHECKERROR);
}

BOOST_AUTO_TEST_CASE(WidthPermanencyAfterRotating)
{
  kornilov::Rectangle rectangle({{0, 0}, 10, 11});
  const double rectWidth = rectangle.getFrameRect().width;
  rectangle.rotate(90);
  rectangle.rotate(60);
  rectangle.rotate(20);
  rectangle.rotate(10);
  BOOST_CHECK_CLOSE(rectWidth, rectangle.getFrameRect().width, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(HeightPermanencyAfterRotating)
{
  kornilov::Rectangle rectangle({{0, 0}, 10, 11});
  const double rectHeight = rectangle.getFrameRect().height;
  rectangle.rotate(90);
  rectangle.rotate(60);
  rectangle.rotate(20);
  rectangle.rotate(10);
  BOOST_CHECK_CLOSE(rectHeight, rectangle.getFrameRect().height, CHECKERROR);
}

BOOST_AUTO_TEST_SUITE_END()
