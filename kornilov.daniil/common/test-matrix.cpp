#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"
#include "circle.hpp"
#include "split.hpp"

const double CHECKERROR = 0.0001;

BOOST_AUTO_TEST_SUITE(TestMatrix)

BOOST_AUTO_TEST_CASE(InvalidIndexException)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  kornilov::Matrix matrix = kornilov::split(compositeShape);
  BOOST_CHECK_THROW(matrix[2][0], std::out_of_range);
  BOOST_CHECK_THROW(matrix[-1][-1], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(TestCopyConstructor)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  kornilov::Matrix matrix = kornilov::split(compositeShape);
  kornilov::Matrix testMatrix(matrix);
  BOOST_CHECK_EQUAL(matrix[0][0], testMatrix[0][0]);
  BOOST_CHECK_EQUAL(matrix[1][0], testMatrix[1][0]);
}

BOOST_AUTO_TEST_CASE(TestMoveConstructor)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  kornilov::Matrix matrix = kornilov::split(compositeShape);
  std::shared_ptr<kornilov::Shape> shapeA = matrix[0][0];
  std::shared_ptr<kornilov::Shape> shapeB = matrix[1][0];
  kornilov::Matrix testMatrix(std::move(matrix));
  BOOST_CHECK_EQUAL(shapeA, testMatrix[0][0]);
  BOOST_CHECK_EQUAL(shapeB, testMatrix[1][0]);
}

BOOST_AUTO_TEST_CASE(TestCopyOperatorOfAssignment)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  kornilov::Matrix matrix = kornilov::split(compositeShape);
  kornilov::Matrix testMatrix = matrix;
  BOOST_CHECK_EQUAL(matrix[0][0], testMatrix[0][0]);
  BOOST_CHECK_EQUAL(matrix[1][0], testMatrix[1][0]);
}

BOOST_AUTO_TEST_CASE(TestMoveOperatorOfAssignment)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  kornilov::Matrix matrix = kornilov::split(compositeShape);
  std::shared_ptr<kornilov::Shape> shapeA = matrix[0][0];
  std::shared_ptr<kornilov::Shape> shapeB = matrix[1][0];
  kornilov::Matrix testMatrix = std::move(matrix);
  BOOST_CHECK_EQUAL(shapeA, testMatrix[0][0]);
  BOOST_CHECK_EQUAL(shapeB, testMatrix[1][0]);
}

BOOST_AUTO_TEST_CASE(TestIndexOperator)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  kornilov::CompositeShape compositeShape;
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  kornilov::Matrix matrix = kornilov::split(compositeShape);
  std::shared_ptr<kornilov::Shape> shapeA = matrix[0][0];
  std::shared_ptr<kornilov::Shape> shapeB = matrix[1][0];
  BOOST_CHECK_CLOSE(shapeA->getArea(), rectangle.getArea(), CHECKERROR);
  BOOST_CHECK_CLOSE(shapeB->getArea(), circle.getArea(), CHECKERROR);
}

BOOST_AUTO_TEST_SUITE_END()
