#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"
#include "circle.hpp"
#include "split.hpp"

BOOST_AUTO_TEST_SUITE(TestSplit)

BOOST_AUTO_TEST_CASE(TestSplit)
{
  kornilov::CompositeShape compShape;
  kornilov::Circle circle1({0, 10}, 5);
  kornilov::Circle circle2({0, 20}, 10);
  kornilov::Rectangle rectangle1({{20, 10}, 8, 6});
  kornilov::Rectangle rectangle2({{100, 100}, 11, 2});
  auto p_rectangle1 = std::make_shared<kornilov::Rectangle>(rectangle1);
  auto p_circle1 = std::make_shared<kornilov::Circle>(circle1);
  auto p_rectangle2 = std::make_shared<kornilov::Rectangle>(rectangle2);
  auto p_circle2 = std::make_shared<kornilov::Circle>(circle2);
  compShape.add(p_circle1);
  compShape.add(p_rectangle1);
  compShape.add(p_rectangle2);
  compShape.add(p_circle2);
  kornilov::Matrix matrix = kornilov::split(compShape);
  BOOST_CHECK_EQUAL(matrix[0][0], p_circle1);
  BOOST_CHECK_EQUAL(matrix[1][0], p_circle2);
  BOOST_CHECK_EQUAL(matrix[0][1], p_rectangle1);
  BOOST_CHECK_EQUAL(matrix[0][2], p_rectangle2);
}

BOOST_AUTO_TEST_CASE(TestIntersect)
{
  kornilov::Rectangle rectangle({{0, 0}, 10.1, 11});
  kornilov::Circle circle({0, 0}, 10.1);
  auto p_rectangle = std::make_shared<kornilov::Rectangle>(rectangle);
  auto p_circle = std::make_shared<kornilov::Circle>(circle);
  BOOST_CHECK_EQUAL(kornilov::intersect(p_rectangle, p_circle), true);
  p_rectangle->move(100, 100);
  BOOST_CHECK_EQUAL(kornilov::intersect(p_rectangle, p_circle), false);
}

BOOST_AUTO_TEST_SUITE_END()
