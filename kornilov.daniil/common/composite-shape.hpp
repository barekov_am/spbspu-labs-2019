#ifndef COMPOSITESHAPE_H
#define COMPOSITESHAPE_H

#include <memory>
#include "shape.hpp"

namespace kornilov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape& rhs);
    CompositeShape(CompositeShape&& rhs);
    ~CompositeShape() = default;

    CompositeShape& operator=(const CompositeShape& rhs);
    CompositeShape& operator=(CompositeShape&& rhs);
    std::shared_ptr<Shape> operator[](const size_t index) const;

    void add(std::shared_ptr<Shape> shape);
    void remove(const size_t index);
    size_t getIndex(const std::shared_ptr<Shape> shape) const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    size_t getSize() const;
    void move(const double dx, const double dy) override;
    void move(const point_t& point) override;
    void scale(const double coefficient) override;
    void rotate(const double angle) override;
    void writeInfo() const override;

  private:
    std::unique_ptr<std::shared_ptr<Shape>[]> shapeArr_;
    size_t size_;
  };
}

#endif
