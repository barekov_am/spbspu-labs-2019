#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

const double CHECKERROR = 0.0001;

BOOST_AUTO_TEST_SUITE(TestCircle)

BOOST_AUTO_TEST_CASE(RadiusPermanencyAfterMovingdXdY)
{
  const double radius = 10.1;
  kornilov::Circle circle({0, 0}, radius);
  circle.move(10, 10);
  BOOST_CHECK_CLOSE(radius, circle.getFrameRect().height / 2, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(RadiusPermanencyAfterMovingToPoint)
{
  const double radius = 10.1;
  kornilov::Circle circle({0, 0}, radius);
  circle.move({10, 10});
  BOOST_CHECK_CLOSE(radius, circle.getFrameRect().height / 2, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(AreaPermanencyAfterMovingdXdY)
{
  kornilov::Circle circle({0, 0}, 10.1);
  const double circArea = circle.getArea();
  circle.move(10, 10);
  BOOST_CHECK_CLOSE(circArea, circle.getArea(), CHECKERROR);
}

BOOST_AUTO_TEST_CASE(AreaPermanencyAfterMovingToPoint)
{
  kornilov::Circle circle({0, 0}, 10.1);
  const double circArea = circle.getArea();
  circle.move({10, 10});
  BOOST_CHECK_CLOSE(circArea, circle.getArea(), CHECKERROR);
}

BOOST_AUTO_TEST_CASE(SquareChangingAfterScaling)
{
  kornilov::Circle circle({0, 0}, 10.1);
  const double circArea = circle.getArea();
  const double coef = 2.1;
  circle.scale(coef);
  BOOST_CHECK_CLOSE(coef * coef, circle.getArea() / circArea, CHECKERROR);
}

BOOST_AUTO_TEST_CASE(InvalidRadius)
{
  const double radius = -0.1;
  BOOST_CHECK_THROW(const kornilov::Circle circle({0, 0}, radius), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(InvalidCoefficient)
{
  kornilov::Circle circle({0, 0}, 10);
  const double coef = -0.1;
  BOOST_CHECK_THROW(circle.scale(coef), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
