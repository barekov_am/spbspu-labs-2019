#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "split.hpp"

int main()
{
  try
  {
    kornilov::Circle circle1({0, 0}, 10);
    kornilov::Circle circle2({0, 5}, 20);
    kornilov::Rectangle rectangle1({{10, 0}, 5, 5});
    kornilov::Rectangle rectangle2({{20, 0}, 6, 10});
    kornilov::CompositeShape compositeShape;

    auto p_circle1 = std::make_shared<kornilov::Circle>(circle1);
    auto p_circle2 = std::make_shared<kornilov::Circle>(circle2);
    auto p_rectangle1 = std::make_shared<kornilov::Rectangle>(rectangle1);
    auto p_rectangle2 = std::make_shared<kornilov::Rectangle>(rectangle2);

    compositeShape.add(p_circle1);
    compositeShape.add(p_circle2);
    compositeShape.add(p_rectangle1);
    compositeShape.add(p_rectangle2);

    compositeShape[compositeShape.getIndex(p_circle1)]->writeInfo();
    compositeShape[compositeShape.getIndex(p_circle2)]->writeInfo();
    compositeShape[compositeShape.getIndex(p_rectangle1)]->writeInfo();
    compositeShape[compositeShape.getIndex(p_rectangle2)]->writeInfo();
    compositeShape.writeInfo();

    kornilov::Matrix matrix = kornilov::split(compositeShape);
    matrix.writeInfo();
  }

  catch (const std::exception& ex)
  {
    std::cerr << ex.what() << std::endl;
    return 1;
  }

  return 0;
}
