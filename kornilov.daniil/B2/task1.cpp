#include <iostream>
#include <sstream>
#include "Queue.hpp"

void doTask1()
{
  QueueWithPriority<std::string> queue;
  std::string line;
  size_t size_of_queue = 0;

  while (std::getline(std::cin, line)) {
    if (std::cin.fail()) {
      throw std::ios_base::failure("Failed to do task 1! Reading error!\n");
    }
    std::stringstream string_stream(line);
    std::string command;
    string_stream >> command;

    if (command == "add") {
      std::string priority;
      string_stream >> std::ws >> priority;

      std::string data;
      std::getline(string_stream >> std::ws, data);

      if (data.empty()) {
        std::cout << "<INVALID COMMAND>\n";
      } else if (priority == "high") {
        queue.putElementToQueue(data, ElementPriority::HIGH);
        ++size_of_queue;
      } else if (priority == "normal") {
        queue.putElementToQueue(data, ElementPriority::NORMAL);
        ++size_of_queue;
      } else if (priority == "low") {
        queue.putElementToQueue(data, ElementPriority::LOW);
        ++size_of_queue;
      } else {
        std::cout << "<INVALID COMMAND>\n";
      }

    } else if (command == "accelerate") {
      queue.accelerate();

    } else if (command == "get") {
      if (size_of_queue == 0) {
        std::cout << "<EMPTY>\n";
      } else {
        std::cout << queue.getElementFromQueue() << '\n';
        --size_of_queue;
      }
    } else {
      std::cout << "<INVALID COMMAND>\n";
    }

    if (std::cin.eof()) {
      break;
    }
  }
}
