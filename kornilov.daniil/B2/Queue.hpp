#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <list>
#include <iostream>

enum class ElementPriority {
  LOW,
  NORMAL,
  HIGH
};

template<typename T>
class QueueWithPriority {
public:
  void putElementToQueue(const T &element, ElementPriority priority);
  T getElementFromQueue();
  void accelerate();

private:
  std::list<T> high_p_;
  std::list<T> normal_p_;
  std::list<T> low_p_;
};

template<typename T>
void QueueWithPriority<T>::putElementToQueue(const T &element, ElementPriority priority)
{
  switch (priority) {
    case ElementPriority::HIGH:
      high_p_.push_back(element);
      break;

    case ElementPriority::NORMAL:
      normal_p_.push_back(element);
      break;

    case ElementPriority::LOW:
      low_p_.push_back(element);
      break;

    default:
      return;
  }
}

template<typename T>
T QueueWithPriority<T>::getElementFromQueue()
{
  if (!high_p_.empty()) {
    T front_element = high_p_.front();
    high_p_.pop_front();
    return front_element;
  }

  if (!normal_p_.empty()) {
    T front_element = normal_p_.front();
    normal_p_.pop_front();
    return front_element;
  }

  if (!low_p_.empty()) {
    T front_element = low_p_.front();
    low_p_.pop_front();
    return front_element;
  }

  throw std::runtime_error("Queue is empty!\n");
}

template<typename T>
void QueueWithPriority<T>::accelerate()
{
  high_p_.splice(high_p_.end(), low_p_);
}

#endif
