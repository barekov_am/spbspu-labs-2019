#include <iostream>

void doTask1();
void doTask2();

int main(int args, char *argv[])
{
  if (args != 2) {
    std::cerr << "Incorrect number of parameters!\n";
    return 1;
  }

  char *pEnd = nullptr;

  try {
    switch (std::strtol(argv[1], &pEnd, 10)) {
      case 1:
        doTask1();
        break;

      case 2:
        doTask2();
        break;

      default:
        std::cerr << "Invalid parameters!\n";
        return 1;
    }
  }

  catch (const std::exception &ex) {
    std::cerr << ex.what();
    return 1;
  }

  return 0;
}
