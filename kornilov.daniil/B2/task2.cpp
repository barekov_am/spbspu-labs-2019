#include <iostream>
#include <list>

void doTask2()
{
  std::list<int> list;
  int value = 0;

  while (std::cin >> value) {
    if ((value < 1) || (value > 20)) {
      throw std::out_of_range("Failed to do task 2! Incorrect value!\n");
    }
    list.push_back(value);
  }

  if ((std::cin.fail()) && (!std::cin.eof())) {
    throw std::invalid_argument("Failed to do task 2! Reading error!\n");
  }

  if (list.size() > 20) {
    throw std::invalid_argument("Failed to do task 2! Incorrect number of elements!\n");
  }

  auto it_next = list.begin();
  auto it_prev = list.rbegin();

  for (size_t i = 0; i < list.size() / 2; ++i) {
    std::cout << *it_next << " " << *it_prev << " ";
    it_next++;
    it_prev++;
  }

  if (list.size() % 2 == 1) {
    std::cout << *it_next;
  }

  std::cout << "\n";
}
