#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "Shape.hpp"

class Square final : public Shape {
public:
  Square(double x, double y);
  void draw() const override;
};

#endif
