#include "Square.hpp"
#include <iostream>

Square::Square(const double x, const double y):
  Shape(x, y)
{
}

void Square::draw() const
{
  std::cout << "SQUARE (" << x_ << "; " << y_ << ")\n";
}
