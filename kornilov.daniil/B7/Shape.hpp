#ifndef SHAPE_HPP
#define SHAPE_HPP

class Shape {
public:
  Shape(double x, double y);

  Shape(const Shape& shape) = default;
  Shape(Shape&& shape) = default;

  Shape& operator=(const Shape& shape) = default;
  Shape& operator=(Shape&& shape) = default;

  virtual ~Shape() = default;
  bool isMoreLeft(const Shape& shape) const;
  bool isUpper(const Shape& shape) const;
  virtual void draw() const = 0;

protected:
  double x_, y_;
};

#endif
