#include "functor-task1.hpp"

void ListOfDoubles::operator()(const double value)
{
  list_.push_back(value);
}

std::list<double>& ListOfDoubles::getListOfDoubles()
{
  return list_;
}

double Multiplies::operator()(const double a, const double b) const
{
  return a * b;
}
