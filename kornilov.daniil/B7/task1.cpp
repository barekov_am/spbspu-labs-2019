#include <iostream>
#include <algorithm>
#include <iterator>
#include <functional>
#include <cmath>
#include "functor-task1.hpp"

void doTask1()
{
  auto list_of_doubles = std::for_each(std::istream_iterator<double>(std::cin),
                                       std::istream_iterator<double>(), ListOfDoubles());
  if (std::cin.fail() && !std::cin.eof()) {
    throw std::runtime_error("Reading error!\n");
  }
  auto list = list_of_doubles.getListOfDoubles();
  if (!list.empty()) {
    std::transform(std::begin(list), std::end(list), std::begin(list),
                   std::bind(Multiplies(), M_PI, std::placeholders::_1));
    std::copy(list.begin(), list.end(), std::ostream_iterator<double>(std::cout, "\n"));
  }
}
