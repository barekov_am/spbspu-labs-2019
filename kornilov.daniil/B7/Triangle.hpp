#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "Shape.hpp"

class Triangle final : public Shape {
public:
  Triangle(double x, double y);
  void draw() const override;
};

#endif
