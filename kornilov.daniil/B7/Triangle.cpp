#include "Triangle.hpp"
#include <iostream>

Triangle::Triangle(const double x, const double y) :
  Shape(x, y)
{
}

void Triangle::draw() const
{
  std::cout << "TRIANGLE (" << x_ << "; " << y_ << ")\n";
}
