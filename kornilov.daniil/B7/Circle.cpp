#include "Circle.hpp"
#include <iostream>

Circle::Circle(const double x, const double y) :
  Shape(x, y)
{
}

void Circle::draw() const
{
  std::cout << "CIRCLE (" << x_ << "; " << y_ << ")\n";
}
