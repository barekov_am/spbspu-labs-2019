#ifndef FUNCTOR_TASK1_HPP
#define FUNCTOR_TASK1_HPP

#include <list>

class ListOfDoubles {
public:
  void operator()(double value);
  std::list<double>& getListOfDoubles();

private:
  std::list<double> list_;
};

class Multiplies {
public:
  double operator()(double a, double b) const;
};

#endif
