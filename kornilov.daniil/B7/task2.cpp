#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <memory>
#include "Shape.hpp"
#include "Circle.hpp"
#include "Square.hpp"
#include "Triangle.hpp"

void doTask2()
{
  using shape_ptr = std::shared_ptr<Shape>;
  std::vector<shape_ptr> vector;
  std::string line;
  while (std::getline(std::cin, line)) {
    if (line.empty()) {
      continue;
    }

    line.erase(remove(line.begin(), line.end(), ' '), line.end());
    line.erase(remove(line.begin(), line.end(), '\t'), line.end());

    if (line.empty() || line.size() == 1) {
      continue;
    }

    const auto open_bracket = line.find_first_of('(');
    const auto semicolon = line.find_first_of(';');
    const auto close_bracket = line.find_first_of(')');

    if (open_bracket == std::string::npos || semicolon == std::string::npos || close_bracket == std::string::npos) {
      throw std::runtime_error("Invalid input data!\n");
    }

    const auto shape = line.substr(0, open_bracket);

    auto x = std::stod(line.substr(open_bracket + 1, semicolon - open_bracket + 1));
    auto y = std::stod(line.substr(semicolon + 1, close_bracket - semicolon + 1));

    if (shape == "CIRCLE") {
      shape_ptr ptr = std::make_shared<Circle>(x, y);
      vector.push_back(ptr);
    } else if (shape == "SQUARE") {
      shape_ptr ptr = std::make_shared<Square>(x, y);
      vector.push_back(ptr);
    } else if (shape == "TRIANGLE") {
      shape_ptr ptr = std::make_shared<Triangle>(x, y);
      vector.push_back(ptr);
    } else {
      throw std::runtime_error("Invalid shape!\n");
    }
  }
  std::cout << "Original:\n";
  std::for_each(vector.begin(), vector.end(), [](const shape_ptr& ptr) { ptr->draw(); });
  std::cout << "Left-Right:\n";
  std::sort(vector.begin(), vector.end(), [](const shape_ptr& a, const shape_ptr& b) { return a->isMoreLeft(*b); });
  std::for_each(vector.begin(), vector.end(), [](const shape_ptr& ptr) { ptr->draw(); });
  std::cout << "Right-Left:\n";
  std::sort(vector.begin(), vector.end(), [](const shape_ptr& a, const shape_ptr& b) { return !a->isMoreLeft(*b); });
  std::for_each(vector.begin(), vector.end(), [](const shape_ptr& ptr) { ptr->draw(); });
  std::cout << "Top-Bottom:\n";
  std::sort(vector.begin(), vector.end(), [](const shape_ptr& a, const shape_ptr& b) { return a->isUpper(*b); });
  std::for_each(vector.begin(), vector.end(), [](const shape_ptr& ptr) { ptr->draw(); });
  std::cout << "Bottom-Top:\n";
  std::sort(vector.begin(), vector.end(), [](const shape_ptr& a, const shape_ptr& b) { return !a->isUpper(*b); });
  std::for_each(vector.begin(), vector.end(), [](const shape_ptr& ptr) { ptr->draw(); });
}
