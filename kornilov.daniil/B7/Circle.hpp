#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "Shape.hpp"

class Circle final : public Shape {
public:
  Circle(double x, double y);
  void draw() const override;
};

#endif
