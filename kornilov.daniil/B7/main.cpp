#include <iostream>

void doTask1();
void doTask2();

int main(const int args, char* argv[])
{
  try {
    if (args != 2) {
      std::cerr << "Invalid number of arguments!\n";
      return 1;
    }

    char* end = nullptr;
    const auto task = std::strtol(argv[1], &end, 10);
    if (*end != '\0' || task < 1 || task > 2) {
      std::cerr << "Invalid task number!\n";
      return 1;
    }

    switch (task) {
    case 1:
      doTask1();
      break;
    case 2:
      doTask2();
      break;
    default:
      std::cerr << "Invalid task number!\n";
      return 1;
    }
  }
  catch (const std::exception& exception) {
    std::cout << exception.what() << '\n';
    return 1;
  }

  return 0;
}
