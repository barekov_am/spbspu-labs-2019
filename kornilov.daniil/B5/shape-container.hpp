#ifndef SHAPE_CONTAINER_HPP
#define SHAPE_CONTAINER_HPP

#include <string>
#include "Geometry.hpp"

class GetPoint {
public:
  point_t operator ()(const shape& shape) const
  {
    return shape.at(0);
  }
};

class ShapeContainer {
public:

  ShapeContainer();

  void readShapesList();
  void printVertices() const;
  void printShapesNumbers() const;
  void erasePentagons();
  void printPoints() const;
  void printContainer() const;
  void changeContainer();

private:
  size_t triangles_;
  size_t squares_;
  size_t rectangles_;
  size_t vertices_;
  std::vector<shape> shapes_;
  shape readDots(std::string str) const;
  static bool comparator(const shape& a, const shape& b);
};

#endif
