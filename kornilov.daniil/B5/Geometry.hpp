#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP

#include <vector>

struct point_t {
  int x, y;
};

using shape = std::vector<point_t>;

double getSquareDistance(const point_t& point1, const point_t& point2);
bool checkSquare(const shape& shape);

#endif
