#include "Geometry.hpp"
#include <cmath>

double getSquareDistance(const point_t& point1, const point_t& point2)
{
  return pow(point1.x - point2.x, 2) + pow(point1.y - point2.y, 2);
}

bool checkSquare(const shape& shape)
{
  if (getSquareDistance(shape.at(0), shape.at(2)) == getSquareDistance(shape.at(1), shape.at(3))) {
    if (getSquareDistance(shape.at(0), shape.at(1)) != getSquareDistance(shape.at(1), shape.at(2))) {
      return false;
    }
    if (getSquareDistance(shape.at(1), shape.at(2)) != getSquareDistance(shape.at(2), shape.at(3))) {
      return false;
    }
    if (getSquareDistance(shape.at(2), shape.at(3)) != getSquareDistance(shape.at(3), shape.at(0))) {
      return false;
    }
    if (getSquareDistance(shape.at(0), shape.at(3)) != getSquareDistance(shape.at(0), shape.at(1))) {
      return false;
    }
    return true;
  }
  return false;
}
