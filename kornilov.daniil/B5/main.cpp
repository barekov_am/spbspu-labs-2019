#include <iostream>

void doTask1();
void doTask2();

int main(const int args, char* argv[])
{
  try {
    if (args != 2) {
      std::cerr << "Invalid number of arguments!\n";
      return 1;
    }

    char* end = nullptr;

    switch (std::strtol(argv[1], &end, 10)) {
    case 1:
      doTask1();
      break;
    case 2:
      doTask2();
      break;
    default:
      std::cerr << "Invalid parameters!\n";
      return 1;
    }

  }
  catch (const std::exception& ex) {
    std::cout << ex.what() << '\n';
    return 1;
  }

  return 0;
}
