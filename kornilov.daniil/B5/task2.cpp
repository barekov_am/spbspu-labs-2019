#include "shape-container.hpp"

void doTask2()
{
  ShapeContainer shape_container;
  shape_container.readShapesList();
  shape_container.printVertices();
  shape_container.printShapesNumbers();
  shape_container.erasePentagons();
  shape_container.printPoints();
  shape_container.changeContainer();
  shape_container.printContainer();
}
