#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include <vector>
#include "DataStruct.hpp"

std::ostream& operator<<(std::ostream& stream, const DataStruct& ds);
void printDataVector(const std::vector<DataStruct>& vector);
bool ascending(const DataStruct& ds1, const DataStruct& ds2);
std::vector<DataStruct> readDataStructs();

#endif
