#include <iostream>
#include <algorithm>
#include "Interface.hpp"

int main()
{
  try {
    auto vector = readDataStructs();
    if (!vector.empty()) {
      std::sort(vector.begin(), vector.end(), ascending);
      printDataVector(vector);
    }
  }
  catch (const std::exception& ex) {
    std::cout << ex.what() << '\n';
    return 1;
  }
  return 0;
}
