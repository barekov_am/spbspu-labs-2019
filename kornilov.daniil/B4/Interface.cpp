#include "Interface.hpp"
#include <iostream>

std::ostream& operator<<(std::ostream& stream, const DataStruct& ds)
{
  stream << ds.key1 << ',' << ds.key2 << ',' << ds.str;
  return stream;
}

void printDataVector(const std::vector<DataStruct>& vector)
{
  for (auto& i : vector) {
    std::cout << i << '\n';
  }
}

bool ascending(const DataStruct& ds1, const DataStruct& ds2)
{
  if (ds1.key1 == ds2.key1) {
    if (ds1.key2 == ds2.key2) {
      return ds1.str.length() < ds2.str.length();
    }
    return ds1.key2 < ds2.key2;
  }
  return ds1.key1 < ds2.key1;
}

std::vector<DataStruct> readDataStructs()
{
  std::vector<DataStruct> vector;
  std::string line;
  while (std::getline(std::cin, line)) {
    const auto position1 = line.find_first_of(',');
    auto key1 = line.substr(0, position1);

    line.erase(position1, 1);
    const auto position2 = line.find_first_of(',');
    auto key2 = line.substr(position1, position2 - position1);

    line.erase(position2, 1);
    const auto str = line.substr(position2, line.length());

    const auto key1_int = stoi(key1);
    const auto key2_int = stoi(key2);

    if (key1_int > 5 || key1_int < -5 || key2_int > 5 || key2_int < -5) {
      throw std::invalid_argument("Invalid key!\n");
    }

    DataStruct ds{key1_int, key2_int, str};

    vector.push_back(ds);
  }
  return vector;
}
