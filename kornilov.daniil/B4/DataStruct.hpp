#ifndef DATA_STRUCT_HPP
#define DATA_STRUCT_HPP

#include <string>

struct DataStruct {
  int key1{}, key2{};
  std::string str;
};

#endif
