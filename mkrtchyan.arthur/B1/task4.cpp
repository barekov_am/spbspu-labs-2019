#include <random>
#include <vector>
#include <algorithm>

#include "functions.hpp"

void fillRandom(double *array, int size)
{
  std::default_random_engine engine;
  std::uniform_real_distribution<double> distr(-1.0, 1.0);
  std::generate(array, array + size, [&](){ return distr(engine); });
}

void task4(const char *sortOption, int size)
{

  checkDirection(sortOption);

  if (size < 0)
  {
    std::invalid_argument("Incorrect size of array");
  }

  std::vector<double> vector(static_cast<size_t>(size));
  fillRandom(&vector[0], size);
  print(vector);

  if (size == 0)
  {
    return;
  }

  sort<Option<decltype(vector)>::Iterator>(vector, sortOption);
  print(vector);
}
