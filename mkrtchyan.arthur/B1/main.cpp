#include <iostream>

void task1(const char *sortOption);
void task2(const char *filename);
void task3();
void task4(const char *sortOption, int size);

int main(int argc, char *argv[])
{
  try
  {
    if (argc < 2)
    {
      std::cerr << "More arguments needed" << std::endl;
      return 1;
    }

    switch(std::stoi(argv[1]))
    {
    case 1:
    {
      if (argc != 3)
      {
        std::cerr << "Incorrect number of arguments" << std::endl;
        return 1;
      }

      task1(argv[2]);
      break;
    }

    case 2:
    {
      if (argc != 3)
      {
        std::cerr << "Incorrect number of arguments" << std::endl;
        return 1;
      }

      if (sizeof(argv[2]) == 0)
      {
          std::cerr << "File does not exist" << std::endl;
          return 1;
      }
      
      task2(argv[2]);
      break;
    }

    case 3:
    {
      if (argc != 2)
      {
        std::cerr << "Incorrect number of arguments" << std::endl;
        return 1;
      }

      task3();
      break;
    }

    case 4:
    {
      if (argc != 4)
      {
        std::cerr << "Incorrect number of arguments" << std::endl;
        return 1;
      }

      task4(argv[2], std::stoi(argv[3]));
      break;
    }

    default:
    {
      std::cerr << "Unknown argument" << std::endl;
      return 1;
    }
    }
  }
  
  catch (std::exception &exception)
  {
    std::cerr << exception.what() << std::endl;
    return 2;
  }
  catch(...)
  {
    return 2;
  }

  return 0;
}
