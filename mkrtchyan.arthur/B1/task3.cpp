#include <iostream>
#include <vector>

#include "functions.hpp"

void task3()
{
  std::vector<int> vector;
  int value = 0;

  while (std::cin >> value, !std::cin.eof())
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Did not manage to read input file\n");
    }

    if (value == 0)
    {
      break;
    }

    vector.push_back(value);
  }

  if (value != 0)
  {
    throw std::invalid_argument("Incorrect symbol of end of file");
  }

  if (vector.empty())
  {
    return;
  }

  std::vector<int>::iterator i = vector.begin();

  if (vector.back() == 1)
  {
    while (i != vector.end())
    {
      if (*i % 2 == 0)
      {
        i = vector.erase(i);
      }
      else
      {
        i++;
      }
    }
  }

  if (vector.back() == 2)
  {
    while (i != vector.end())
    {
      if (*i % 3 == 0)
      {
        i = vector.insert(++i, 3, 1);
        i += 2;
      }
      else
      {
        i++;
      }
    }
  }

  print(vector);
}
