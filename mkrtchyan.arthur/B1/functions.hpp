#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <cstring>
#include <iostream>

#include "options.hpp"

void checkDirection(const char *direction);

template<typename Access, typename Container>
void sort(Container &collection, const char *sortOption)
{
  const auto begin = Access::getBegin(collection);
  const auto end = Access::getEnd(collection);

  for (auto i = begin; i != end; i++)
  {
    for (auto j = Access::getNext(i); j != end; j++)
    {
      typename Container::value_type &a = Access::getValue(collection, i);
      typename Container::value_type &b = Access::getValue(collection, j);

      if ((std::strcmp(sortOption, "ascending") == 0 && (a > b))
          || (std::strcmp(sortOption, "descending") == 0 && (a < b)))
      {
        std::swap(a, b);
      }
    }
  }
}

template<typename Container>
void print(const Container &collection)
{
  for (typename Container::const_iterator i = collection.begin(); i != collection.end(); i++)
  {
    std::cout << *i << ' ';
  }

  std::cout << std::endl;
}

#endif //FUNCTIONS_HPP
