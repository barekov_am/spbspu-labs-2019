#include <vector>
#include <forward_list>

#include "functions.hpp"

void task1(const char *sortOption)
{
  checkDirection(sortOption);

  std::vector<int> vectorBrackets;
  int value = 0;

  while (std::cin >> value, !std::cin.eof())
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Did not manage to read input file\n");
    }

    vectorBrackets.push_back(value);
  }

  if (vectorBrackets.empty())
  {
    return;
  }

  std::vector<int> vectorAt = vectorBrackets;
  std::forward_list<int> simplyConnectedList(vectorBrackets.begin(), vectorBrackets.end());

  sort<Option<decltype(vectorBrackets)>::Operator>(vectorBrackets, sortOption);
  print(vectorBrackets);

  sort<Option<decltype(vectorAt)>::At>(vectorAt, sortOption);
  print(vectorAt);

  sort<Option<decltype(simplyConnectedList)>::Iterator>(simplyConnectedList, sortOption);
  print(simplyConnectedList);
}
