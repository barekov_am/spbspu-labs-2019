#include "functions.hpp"

void checkDirection(const char *direction)
{
  if ((std::strcmp(direction, "ascending") != 0)
      && (std::strcmp(direction, "descending") != 0))
  {
    throw std::invalid_argument("Incorrect type of sorting\n");
  }

}
