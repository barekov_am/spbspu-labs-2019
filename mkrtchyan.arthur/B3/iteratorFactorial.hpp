#ifndef ITERATOR_FACTORIAL_HPP
#define ITERATOR_FACTORIAL_HPP

#include <memory>

class IteratorFactorial: public std::iterator<std::bidirectional_iterator_tag,
    unsigned long long>
{
public:
  IteratorFactorial();
  IteratorFactorial(int index);

  unsigned long long& operator*();
  pointer operator ->();

  bool operator!=(const IteratorFactorial& other) const;
  bool operator==(const IteratorFactorial& other) const;

  IteratorFactorial& operator++();
  IteratorFactorial& operator--();

  IteratorFactorial operator++(int);
  IteratorFactorial operator--(int);

private:
  int index_;
  unsigned long long value_;
  const int maxRange = 11;

  unsigned long long getValue(int index) const;
};

#endif //ITERATOR_FACTORIAL_HPP
