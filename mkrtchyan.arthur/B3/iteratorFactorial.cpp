#include "iteratorFactorial.hpp"

#include <stdexcept>

IteratorFactorial::IteratorFactorial():
  index_(1),
  value_(1)
{}

IteratorFactorial::IteratorFactorial(int index):
  index_(index),
  value_(getValue(index))
{}

unsigned long long& IteratorFactorial::operator*()
{
  return value_;
}

IteratorFactorial::pointer IteratorFactorial::operator->()
{
  return& value_;
}


bool IteratorFactorial::operator==(const IteratorFactorial& source) const
{
  return (index_ == source.index_);
}

bool IteratorFactorial::operator!=(const IteratorFactorial& source) const
{
  return !(*this == source);
}

IteratorFactorial& IteratorFactorial::operator++()
{
  if (index_ == maxRange)
  {
    throw std::out_of_range("Index mustn't be more than maxRange");
  }

  ++index_;
  value_ *= index_;
  return* this;
}

IteratorFactorial& IteratorFactorial::operator--()
{
  if (index_ == 0)
  {
    throw std::out_of_range("Index mustn't be less than zero");
  }

  value_ /= index_;
  --index_;
  return* this;
}

IteratorFactorial IteratorFactorial::operator++(int)
{
  IteratorFactorial tmp = *this;
  ++(*this);
  return tmp;
}

IteratorFactorial IteratorFactorial::operator--(int)
{
  IteratorFactorial tmp = *this;
  --(*this);
  return tmp;
}

unsigned long long IteratorFactorial::getValue(int index) const
{
  if (index <= 1)
  {
    return 1;
  }
  else
  {
    return index * getValue(index - 1);
  }
}
