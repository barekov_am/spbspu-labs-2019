#include "containerFactorial.hpp"

IteratorFactorial ContainerFactorial::begin()
{
  return IteratorFactorial(minRange);
}

IteratorFactorial ContainerFactorial::end()
{
  return IteratorFactorial(maxRange);
}
