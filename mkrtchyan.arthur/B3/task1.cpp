#include <iostream>
#include <sstream>

#include "commands.hpp"
#include "phoneBookManager.hpp"

void task1()
{
  PhoneBookManager PhoneBook;
  std::string line;

  while (std::getline(std::cin, line))
  {
    if (!std::cin && !std::cin.eof()) {
      throw std::ios_base::failure("Failed input");
    }

    std::istringstream input(line);
    Commands::execute(PhoneBook, input);
  }
}


