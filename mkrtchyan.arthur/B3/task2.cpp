#include <iostream>
#include <algorithm>
#include <iterator>

#include "containerFactorial.hpp"

void task2()
{
  ContainerFactorial containerFactorial;

  std::copy(containerFactorial.begin(), containerFactorial.end(),
      std::ostream_iterator<unsigned long long>(std::cout, " "));
  std::cout << '\n';

  std::reverse_copy(containerFactorial.begin(), containerFactorial.end(),
      std::ostream_iterator<unsigned long long>(std::cout, " "));
  std::cout << "\n";
}
