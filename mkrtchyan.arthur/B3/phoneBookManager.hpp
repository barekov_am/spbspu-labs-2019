#ifndef PHONE_BOOK_MANAGER_HPP
#define PHONE_BOOK_MANAGER_HPP

#include <map>

#include "phoneBook.hpp"

class PhoneBookManager
{
public:

  enum class MovePosition
  {
    first,
    last
  };

  enum class InsertPosition
  {
    before,
    after
  };

  PhoneBookManager();

  void add(const PhoneBook::record_t& record);
  void store(const std::string& bookmark, const std::string& newBookmark);

  void move(const std::string& bookmark, MovePosition& position);
  void move(const std::string& bookmark, int steps);
  void remove(const std::string& bookmark);

  void insert(const std::string& bookmark, const PhoneBook::record_t& record,
      InsertPosition& position);
  void show(const std::string& bookmark);

private:

  using mapMark = std::map<std::string, PhoneBook::listIterator>;
  PhoneBook records_;
  mapMark bookmarks_;
};

#endif //PHONE_BOOK_MANAGER_HPP
