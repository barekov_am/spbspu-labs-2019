#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <stdexcept>
#include <functional>

#include "phoneBookManager.hpp"

namespace Commands
{
  void skipSpaces(std::istream& input);

  std::string readBookmark(std::istream& input);
  std::string readNumber(std::istream& input);
  std::string readName(std::istream& input);

  void execute(PhoneBookManager& phoneBook, std::istream& input);
  void executeAdd(PhoneBookManager& phoneBook, std::istream& input);
  void executeStore(PhoneBookManager& phoneBook, std::istream& input);
  void executeInsert(PhoneBookManager& phoneBook, std::istream& input);
  void executeDelete(PhoneBookManager& phoneBook, std::istream& input);
  void executeShow(PhoneBookManager& phoneBook, std::istream& input);
  void executeMove(PhoneBookManager& phoneBook, std::istream& input);
};

#endif //COMMANDS_HPP
