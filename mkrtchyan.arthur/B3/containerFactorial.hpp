#ifndef CONTAINER_FACTORIAL_HPP
#define CONTAINER_FACTORIAL_HPP

#include "iteratorFactorial.hpp"

class ContainerFactorial
{
public:
  ContainerFactorial() = default;
  ~ContainerFactorial() = default;
  
  IteratorFactorial begin();
  IteratorFactorial end();

private:
  const int minRange = 1;
  const int maxRange = 11;
};

#endif //CONTAINER_FACTORIAL_HPP
