#include <sstream>
#include <algorithm>
#include <map>
#include <iostream>

#include "queueWithPriority.hpp"

void executeAdd(QueueWithPriority<std::string>& queue, std::stringstream& in)
{
  using priorityType = QueueWithPriority<std::string>::ElementPriority;

  std::map<const std::string, priorityType> priorities =
    {
      {"low", priorityType::LOW},
      {"normal", priorityType::NORMAL},
      {"high", priorityType::HIGH}
    };

  std::string priority;
  in >> std::ws >> priority;

  auto check = [&](const std::pair<const std::string, priorityType>& pair)
  { return (pair.first == priority); };

  auto argPriority = std::find_if(std::begin(priorities), std::end(priorities), check);

  if (argPriority == std::end(priorities))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::string data;
  std::getline(in >> std::ws, data);

  if (data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.putElementToQueue(data, argPriority->second);
}

void executeGet(QueueWithPriority<std::string>& queue, std::stringstream& in)
{
  std::string data;
  std::getline(in >> std::ws, data);

  if (!data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (queue.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  std::string element = queue.getElementFromQueue();
  std::cout << element << "\n";
}

void executeAccelerate(QueueWithPriority<std::string>& queue, std::stringstream& in)
{
  std::string data;
  std::getline(in >> std::ws, data);

  if (!data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.accelerate();
}
