#ifndef QUEUE_WITH_PRIORITY_INTERFACE_HPP
#define QUEUE_WITH_PRIORITY_INTERFACE_HPP

#include <list>

template <typename T>
class QueueWithPriority
{
public:

  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  QueueWithPriority();

  void putElementToQueue(const T& element, ElementPriority priority);
  T getElementFromQueue();

  void accelerate();
  bool empty() const;

private:
  std::list<T> low;
  std::list<T> normal;
  std::list<T> high;
};

#endif //QUEUE_WITH_PRIORITY_INTERFACE_HPP
