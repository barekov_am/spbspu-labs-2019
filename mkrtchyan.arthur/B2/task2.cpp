#include <iostream>
#include <list>

const std::size_t MAXSIZE = 20;
const int MAX = 20;
const int MIN = 1;

void printList(std::list<int>::iterator begin, std::list<int>::iterator end)
{
  if (begin == end)
  {
    std::cout << "\n";
    return;
  }

  if (begin == std::prev(end))
  {
    std::cout << *begin << "\n";
    return;
  }

  std::cout << *begin << " " << *(std::prev(end)) << " ";

  ++begin;
  --end;

  printList(begin, end);
}

void task2()
{
  std::list<int> list;

  std::size_t size = 0;
  int tmp = 0;

  while (std::cin && !(std::cin >> tmp).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Did not manage to read input file");
    }

    if ((tmp > MAX) || (tmp < MIN))
    {
      throw std::ios_base::failure("Incorrect number");
    }

    ++size;
    if (size > MAXSIZE)
    {
      throw std::ios_base::failure("Incorrect number of numbers");
    }

    list.push_back(tmp);
  }

  printList(list.begin(), list.end());
};
