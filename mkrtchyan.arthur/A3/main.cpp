#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

void printInf(const mkrtchyan::Shape & shape)
{
  mkrtchyan::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "Shape parameters: " << std::endl;
  std::cout << "Center: (" << frameRect.pos.x << ";" << frameRect.pos.y << ")" << std::endl;
  std::cout << "Frame width: " << frameRect.width << std::endl;
  std::cout << "Frame height: " << frameRect.height << std::endl;
  std::cout << "Area: " << shape.getArea() << std::endl;
}

void movePoint(mkrtchyan::Shape & shape, double x, double y)
{
  printInf(shape);
  shape.move({ x, y });
  std::cout << "Shape parameters after move: " << std::endl;
  printInf(shape);
}

void moveXY(mkrtchyan::Shape & shape, double dx, double dy)
{
  printInf(shape);
  shape.move(dx, dy);
  std::cout << "Shape parameters after move: " << std::endl;
  printInf(shape);
}

void scale(mkrtchyan::Shape & shape, double coefficient)
{
  printInf(shape);
  shape.scale(coefficient);
  std::cout << "Shape parameters after scale: " << std::endl;
  printInf(shape);
}

int main()
{
  mkrtchyan::Triangle triangle({0, 0}, {5, 5}, {3, 3});
  mkrtchyan::Circle circle(1, {2, 3});
  mkrtchyan::Rectangle rectangle(1.5, 3, {6, 7});
  mkrtchyan::Shape::pointer circle1 = std::make_shared<mkrtchyan::Circle>(circle);
  mkrtchyan::Shape::pointer rectangle1 = std::make_shared<mkrtchyan::Rectangle>(rectangle);
  mkrtchyan::Shape::pointer rectangle2 = std::make_shared<mkrtchyan::Rectangle>(2, 4, mkrtchyan::point_t{4, 5});
  mkrtchyan::Shape::pointer circle2 = std::make_shared<mkrtchyan::Circle>(1, mkrtchyan::point_t{2, 3});
  mkrtchyan::Shape::pointer circle3 = std::make_shared<mkrtchyan::Circle>(7, mkrtchyan::point_t{5, 1.3});
  mkrtchyan::Shape::pointer triangle1 = std::make_shared<mkrtchyan::Triangle>(triangle);

  mkrtchyan::CompositeShape composite;
  composite.add(circle1);
  composite.add(triangle1);
  composite.add(rectangle1);
  movePoint(composite, 12, 2.4);
  composite.add(rectangle2);
  moveXY(composite, 2.6, 4.3);
  composite.add(circle2);
  scale(composite, 2.3);

  return 0;
}
