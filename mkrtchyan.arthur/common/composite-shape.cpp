#include "composite-shape.hpp"
#include <stdexcept>
#include <algorithm>
#include <cmath>

mkrtchyan::CompositeShape::CompositeShape():
  size_(0),
  shapes_()
{

}

mkrtchyan::CompositeShape::CompositeShape(const mkrtchyan::CompositeShape& sample):
  size_(sample.size_),
  shapes_(std::make_unique<Shape::pointer[]>(sample.size_))
{
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i] = sample.shapes_[i];
  }
}

mkrtchyan::CompositeShape::CompositeShape(mkrtchyan::CompositeShape&& sample):
  size_(sample.size_),
  shapes_(std::move(sample.shapes_))
{
  sample.size_ = 0;
}

mkrtchyan::CompositeShape& mkrtchyan::CompositeShape::operator =(const mkrtchyan::CompositeShape& sample)
{
  if (this == &sample)
  {
    return *this;
  }

  size_ = sample.size_;
  Shape::pointerArray shapesArray(std::make_unique<Shape::pointer[]>(sample.size_));
  for (size_t i = 0; i < sample.size_; i++)
  {
    shapesArray[i] = sample.shapes_[i];
  }

  shapes_.swap(shapesArray);
  return *this;
}

mkrtchyan::CompositeShape& mkrtchyan::CompositeShape::operator =(mkrtchyan::CompositeShape&& sample)
{
  if (this == &sample)
  {
    return *this;
  }

  size_ = sample.size_;
  shapes_ = std::move(sample.shapes_);
  sample.size_ = 0;
  return *this;
}

mkrtchyan::Shape::pointer mkrtchyan::CompositeShape::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::invalid_argument("Invalid index");
  }

  return shapes_[index];
}

double mkrtchyan::CompositeShape::getArea() const
{
  double area = 0;

  for (size_t i = 0; i < size_; i++)
  {
    area += shapes_[i]->getArea();
  }

  return area;
}

mkrtchyan::rectangle_t mkrtchyan::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::logic_error("There are no shapes in array");
  }

  rectangle_t frameRect = shapes_[0]->getFrameRect();
  double xMin = frameRect.pos.x - frameRect.width / 2;
  double xMax = frameRect.pos.x + frameRect.width / 2;
  double yMin = frameRect.pos.y - frameRect.height / 2;
  double yMax = frameRect.pos.y + frameRect.height / 2;

  for (size_t i = 1; i < size_; ++i)
  {
    const rectangle_t frameShape = shapes_[i]->getFrameRect();
    xMin = fmin(xMin, frameShape.pos.x - frameShape.width / 2);
    xMax = fmax(xMax, frameShape.pos.x + frameShape.width / 2);
    yMin = fmin(yMin, frameShape.pos.y - frameShape.height / 2);
    yMax = fmax(yMax, frameShape.pos.y + frameShape.height / 2);
  }

  double rectWidth = xMax - xMin;
  double rectHeight = yMax - yMin;
  return {rectWidth, rectHeight, {xMax - rectWidth / 2, yMax - rectHeight / 2}};
}

void mkrtchyan::CompositeShape::move(mkrtchyan::point_t point)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  const point_t center = getCenter();
  move(point.x - center.x, point.y - center.y);
}

void mkrtchyan::CompositeShape::move(double dx, double dy)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void mkrtchyan::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  const point_t point = getCenter();
  for (unsigned i = 0; i < size_; ++i)
  {
    shapes_[i]->scale(coefficient);
    double dx = point.x - shapes_[i]->getCenter().x;
    double dy = point.y - shapes_[i]->getCenter().y;
    shapes_[i]->move(dx * (coefficient - 1), dy * (coefficient - 1));
  }
}

mkrtchyan::point_t mkrtchyan::CompositeShape::getCenter() const
{
  return getFrameRect().pos;
}

void mkrtchyan::CompositeShape::add(Shape::pointer & shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Pointer is nullptr");
  }

  for (size_t i = 0; i < size_; i++)
  {
    if (shapes_[i] == shape)
    {
      return;
    }
  }

  Shape::pointerArray shapesArray(std::make_unique<Shape::pointer[]>(size_ + 1));

  for (size_t i = 0; i < size_; i++)
  {
    shapesArray[i] = shapes_[i];
  }

  shapesArray[size_] = shape;
  size_++;
  shapes_.swap(shapesArray);
}

void mkrtchyan::CompositeShape::remove(size_t index)
{
  if (index >= size_)
  {
    throw std::invalid_argument("Invalid index");
  }

  for (size_t j = index; j < size_ - 1; j++)
  {
    shapes_[j] = shapes_[j + 1];
  }

  shapes_[size_ - 1] = nullptr;
  size_--;
}

void mkrtchyan::CompositeShape::rotate(double angle)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }
  const double newCos = std::cos((2 * M_PI * angle) / 360);
  const double newSin = std::sin((2 * M_PI * angle) / 360);
  const point_t compCenter = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++)
  {
    const point_t center = shapes_[i]->getFrameRect().pos;
    const double dx = (center.x - compCenter.x) * newCos - (center.y - compCenter.y) * newSin;
    const double dy = (center.x - compCenter.x) * newSin + (center.y - compCenter.y) * newCos;
    shapes_[i]->move({compCenter.x + dx, compCenter.y + dy});
    shapes_[i]->rotate(angle);
  }
}

mkrtchyan::Shape::pointerArray mkrtchyan::CompositeShape::getShapes() const
{
  Shape::pointerArray tmpShapes(std::make_unique<Shape::pointer[]>(size_));

  for (size_t i = 0; i < size_; i++)
  {
    tmpShapes[i] = shapes_[i];
  }

  return tmpShapes;
}

size_t mkrtchyan::CompositeShape::getSize() const
{
  return size_;
}
