#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(compositeShapeTests)

BOOST_AUTO_TEST_CASE(constAfterMovingToPoint)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const double areaBefore = composite.getArea();
  const mkrtchyan::rectangle_t frameRectBefore = composite.getFrameRect();
  const double newPointX = 5.4;
  const double newPointY = -0.4;

  composite.move({ newPointX, newPointY });
  const double areaAfter = composite.getArea();
  const mkrtchyan::rectangle_t frameRectAfter = composite.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, newPointX, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, newPointY, ACCURACY);
}

BOOST_AUTO_TEST_CASE(constAfterMovingDxDy)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const double areaBefore = composite.getArea();
  const mkrtchyan::rectangle_t frameRectBefore = composite.getFrameRect();
  const double dx = 5.4;
  const double dy = -0.4;

  composite.move(dx, dy);
  const double areaAfter = composite.getArea();
  const mkrtchyan::rectangle_t frameRectAfter = composite.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x + dx, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y + dy, frameRectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(constAfterScalingWithCoefficientMoreThanOne)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const double areaBefore = composite.getArea();
  const double coefficient = 2.1;
  const mkrtchyan::rectangle_t frameRectBefore = composite.getFrameRect();

  composite.scale(coefficient);
  const double areaAfter = composite.getArea();
  const mkrtchyan::rectangle_t frameRectAfter = composite.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width * coefficient, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height * coefficient, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(constAfterScalingWithCoefficientLessThanOneButMoreThanNull)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const double areaBefore = composite.getArea();
  const double coefficient = 0.6;
  const mkrtchyan::rectangle_t frameRectBefore = composite.getFrameRect();

  composite.scale(coefficient);
  const double areaAfter = composite.getArea();
  const mkrtchyan::rectangle_t frameRectAfter = composite.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width * coefficient, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height * coefficient, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(changeAfterAdding)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::CompositeShape composite;
  double areaOfComposite = composite.getArea();
  int count = 0;
  BOOST_CHECK_CLOSE(areaOfComposite, 0, ACCURACY);

  composite.add(circle);
  count++;
  areaOfComposite += circle->getArea();
  BOOST_CHECK_EQUAL(count, composite.getSize());
  BOOST_CHECK_CLOSE(areaOfComposite , composite.getArea(), ACCURACY);
  composite.add(circle);
  BOOST_CHECK_EQUAL(count, composite.getSize());

  composite.add(rectangle);
  count++;
  areaOfComposite += rectangle->getArea();
  BOOST_CHECK_EQUAL(count, composite.getSize());
  BOOST_CHECK_CLOSE(areaOfComposite, composite.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(changeAfterDeleting)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{10.3, 13.2});
  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  double areaOfComposite = composite.getArea();
  int count = 2;

  composite.remove(0);
  count--;
  areaOfComposite -= circle->getArea();
  BOOST_CHECK_EQUAL(count, composite.getSize());
  BOOST_CHECK_CLOSE(areaOfComposite, composite.getArea(), ACCURACY);

  areaOfComposite = composite.getArea();

  composite.remove(0);
  count--;
  areaOfComposite -= rectangle->getArea();
  BOOST_CHECK_EQUAL(count, composite.getSize());
  BOOST_CHECK_CLOSE(areaOfComposite, composite.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(useCopyConstructorForCopyInEmpty)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const mkrtchyan::rectangle_t frameRectOfComposite = composite.getFrameRect();

  mkrtchyan::CompositeShape copyComposite = composite;
  const double areaOfCopyComposite = copyComposite.getArea();
  const double sizeOfCopyComposite = copyComposite.getSize();
  const mkrtchyan::rectangle_t frameRectOfCopyComposite = copyComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfCopyComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfCopyComposite, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfCopyComposite.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfCopyComposite.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfCopyComposite.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfCopyComposite.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(useCopyOperatorForCopyInEmpty)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const mkrtchyan::rectangle_t frameRectOfComposite = composite.getFrameRect();

  mkrtchyan::CompositeShape copyComposite;
  copyComposite = composite;
  const double areaOfCopyComposite = copyComposite.getArea();
  const double sizeOfCopyComposite = copyComposite.getSize();
  const mkrtchyan::rectangle_t frameRectOfCopyComposite = copyComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfCopyComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfCopyComposite, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfCopyComposite.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfCopyComposite.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfCopyComposite.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfCopyComposite.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(useMoveConstructorForMoveInEmpty)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const mkrtchyan::rectangle_t frameRectOfComposite = composite.getFrameRect();

  mkrtchyan::CompositeShape moveComposite = std::move(composite);
  const double areaOfMoveComposite = moveComposite.getArea();
  const double sizeOfMoveComposite = moveComposite.getSize();
  const mkrtchyan::rectangle_t frameRectOfMoveComposite = moveComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfMoveComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfMoveComposite, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfMoveComposite.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfMoveComposite.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfMoveComposite.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfMoveComposite.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(useMoveOperatorForMoveInEmpty)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const mkrtchyan::rectangle_t frameRectOfComposite = composite.getFrameRect();

  mkrtchyan::CompositeShape moveComposite;
  moveComposite = std::move(composite);
  const double areaOfMoveComposite = moveComposite.getArea();
  const double sizeOfMoveComposite = moveComposite.getSize();
  const mkrtchyan::rectangle_t frameRectOfMoveComposite = moveComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfMoveComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfMoveComposite, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfMoveComposite.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfMoveComposite.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfMoveComposite.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfMoveComposite.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(useCopyOperatorForCopyInNotEmpty)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const mkrtchyan::rectangle_t frameRectOfComposite = composite.getFrameRect();

  mkrtchyan::Shape::pointer copyCircle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer copyRectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::CompositeShape copyComposite;
  copyComposite.add(copyCircle);
  copyComposite.add(copyRectangle);
  copyComposite = composite;
  const double areaOfCopyComposite = copyComposite.getArea();
  const double sizeOfCopyComposite = copyComposite.getSize();
  const mkrtchyan::rectangle_t frameRectOfCopyComposite = copyComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfCopyComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfCopyComposite, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfCopyComposite.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfCopyComposite.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfCopyComposite.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfCopyComposite.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(useMoveOperatorForMoveInNotEmpty)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const mkrtchyan::rectangle_t frameRectOfComposite = composite.getFrameRect();

  mkrtchyan::Shape::pointer moveCircle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer moveRectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::CompositeShape moveComposite;
  moveComposite.add(moveCircle);
  moveComposite.add(moveRectangle);
  moveComposite = std::move(composite);
  const double areaOfMoveComposite = moveComposite.getArea();
  const double sizeOfMoveComposite = moveComposite.getSize();
  const mkrtchyan::rectangle_t frameRectOfMoveComposite = moveComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfMoveComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfMoveComposite, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfMoveComposite.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfMoveComposite.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfMoveComposite.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfMoveComposite.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testRotatingOn45)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(1, mkrtchyan::point_t{1, 1});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(5, 5, mkrtchyan::point_t{0, 0});
  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const mkrtchyan::rectangle_t frameRectBefore = composite.getFrameRect();
  const double areaBefore = composite.getArea();

  const double angle = 45;

  composite.rotate(angle);

  const mkrtchyan::rectangle_t frameRectAfter = composite.getFrameRect();
  const double areaAfter = composite.getArea();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width * std::sqrt(2), frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height * std::sqrt(2), frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testRotatingOn90)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(1, mkrtchyan::point_t{1, 1});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(5, 5, mkrtchyan::point_t{0, 0});
  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const mkrtchyan::rectangle_t frameRectBefore = composite.getFrameRect();
  const double areaBefore = composite.getArea();

  const double angle = 90;

  composite.rotate(angle);

  const mkrtchyan::rectangle_t frameRectAfter = composite.getFrameRect();
  const double areaAfter = composite.getArea();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testRotatingOn180)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(1, mkrtchyan::point_t{1, 1});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(5, 5, mkrtchyan::point_t{0, 0});
  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const mkrtchyan::rectangle_t frameRectBefore = composite.getFrameRect();
  const double areaBefore = composite.getArea();

  const double angle = 180;

  composite.rotate(angle);

  const mkrtchyan::rectangle_t frameRectAfter = composite.getFrameRect();
  const double areaAfter = composite.getArea();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidCoefficientOfScaling)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});

  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  BOOST_CHECK_THROW(composite.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(composite.scale(-5.3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(emptyCompositeShape)
{
  mkrtchyan::CompositeShape composite;

  BOOST_CHECK_THROW(composite.scale(4), std::logic_error);
  BOOST_CHECK_THROW(composite.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(composite.move(5.4, -3.2), std::logic_error);
  BOOST_CHECK_THROW(composite.move({ 5.4, -3.2 }), std::logic_error);
}

BOOST_AUTO_TEST_SUITE_END()


