#ifndef A4_TRIANGLE_HPP
#define A4_TRIANGLE_HPP
#include "shape.hpp"

namespace mkrtchyan
{
  class Triangle : public mkrtchyan::Shape
  {
  public:
    Triangle(point_t point1, point_t point2, point_t point3);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(point_t point) override;
    void move(double dx, double dy) override;
    void scale(double coefficient) override;
    void rotate(double angle) override;
    point_t getCenter() const override;

  private:
    point_t point1_;
    point_t point2_;
    point_t point3_;
  };
}

#endif
