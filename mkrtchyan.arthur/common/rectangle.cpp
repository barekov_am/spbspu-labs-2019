#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

mkrtchyan::Rectangle::Rectangle(double width, double height, point_t pos):
  width_(width),
  height_(height),
  pos_(pos),
  angle_(0)
{
  if (width <= 0)
  {
    throw std::invalid_argument("Invalid width");
  }
  if (height <= 0)
  {
    throw std::invalid_argument("Invalid height");
  }
}

double mkrtchyan::Rectangle::getArea() const
{
  return width_ * height_;
}

void mkrtchyan::Rectangle::move(point_t point)
{
  pos_ = point;
}

void mkrtchyan::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

mkrtchyan::rectangle_t mkrtchyan::Rectangle::getFrameRect() const
{
  double width = width_ * std::fabs(cos((M_PI * angle_) / 180)) + height_ * std::fabs(sin((M_PI * angle_) / 180));
  double height = height_ * std::fabs(cos((M_PI * angle_) / 180)) + width_ * std::fabs(sin((M_PI * angle_) / 180));
  return {width, height, pos_};
}

void mkrtchyan::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }
  width_ *= coefficient;
  height_ *= coefficient;
}

void mkrtchyan::Rectangle::rotate(double angle)
{
  angle_ += angle;
  angle = fmod(angle_, 360);
  if (angle_ < 0)
  {
    angle_ += 360;
  }
}

mkrtchyan::point_t mkrtchyan::Rectangle::getCenter() const
{
  return pos_;
}
