#include <boost/test/unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include "triangle.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(triangleTest)

BOOST_AUTO_TEST_CASE(triangleParametersPermanenceWithMovement)
{
  mkrtchyan::Triangle triangleSample({5, 5}, {16, 8}, {19, 15});
  const mkrtchyan::rectangle_t originalFrameRect = triangleSample.getFrameRect();
  const double originalFrameRectArea = triangleSample.getArea();

  triangleSample.move(3, 4);
  BOOST_CHECK_CLOSE(originalFrameRect.width, triangleSample.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(originalFrameRect.height, triangleSample.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(originalFrameRectArea, triangleSample.getArea(), ACCURACY);

  triangleSample.move({1, 2});
  BOOST_CHECK_CLOSE(originalFrameRect.width, triangleSample.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(originalFrameRect.height, triangleSample.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(originalFrameRectArea, triangleSample.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(triangleQuadraticChangeWhenScaled)
{
  mkrtchyan::Triangle triangleSample({5, 5}, {16, 8}, {19, 15});
  const double originalShapeArea = triangleSample.getArea();
  const double coefficient = 5;

  triangleSample.scale(coefficient);
  BOOST_CHECK_CLOSE(originalShapeArea * coefficient * coefficient, triangleSample.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(incorrectTriangleParametersInFunctions)
{
  mkrtchyan::Triangle triangleSample({5, 5}, {16, 8}, {19, 15});

  BOOST_CHECK_THROW(mkrtchyan::Triangle({0, 0}, {0, 0}, {0, 0}), std::invalid_argument);
  BOOST_CHECK_THROW(triangleSample.scale(-5), std::invalid_argument);
  BOOST_CHECK_THROW(triangleSample.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(parametersPermanenceWithRotate)
{
  mkrtchyan::Triangle triangle2({4, 5}, {6, 7}, {9, 8});
  const mkrtchyan::rectangle_t frameRectBefore2 = triangle2.getFrameRect();
  const double areaBefore2 = triangle2.getArea();

  const double angle2 = 90;
  triangle2.rotate(angle2);
  const mkrtchyan::rectangle_t frameRectAfter2 = triangle2.getFrameRect();
  const double areaAfter2 = triangle2.getArea();

  BOOST_CHECK_CLOSE(areaBefore2, areaAfter2, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore2.width, frameRectAfter2.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore2.height, frameRectAfter2.width, ACCURACY);
  BOOST_CHECK_CLOSE(triangle2.getCenter().x, triangle2.getCenter().x, ACCURACY);
  BOOST_CHECK_CLOSE(triangle2.getCenter().y, triangle2.getCenter().y, ACCURACY);


  mkrtchyan::Triangle triangle3({4, 5}, {6, 7}, {9, 8});
  const mkrtchyan::rectangle_t frameRectBefore3 = triangle3.getFrameRect();
  const double areaBefore3 = triangle3.getArea();

  const double angle3 = 180;
  triangle3.rotate(angle3);
  const mkrtchyan::rectangle_t frameRectAfter3 = triangle3.getFrameRect();
  const double areaAfter3 = triangle3.getArea();

  BOOST_CHECK_CLOSE(areaBefore3, areaAfter3, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore3.width, frameRectAfter3.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore3.height, frameRectAfter3.height, ACCURACY);
  BOOST_CHECK_CLOSE(triangle3.getCenter().x, triangle3.getCenter().x, ACCURACY);
  BOOST_CHECK_CLOSE(triangle3.getCenter().y, triangle3.getCenter().y, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
