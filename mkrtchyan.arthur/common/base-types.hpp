#ifndef A4_BASE_TYPES_HPP
#define A4_BASE_TYPES_HPP

namespace mkrtchyan
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    double width;
    double height;
    point_t pos;
  };
}

#endif
