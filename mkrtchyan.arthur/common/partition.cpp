#include "partition.hpp"
#include <cmath>

bool mkrtchyan::intersectionExists(const rectangle_t & shape1, const rectangle_t & shape2)
{
  return (std::abs(shape1.pos.x - shape2.pos.x) < (shape1.width + shape2.width) / 2) &&
      (std::abs(shape1.pos.y - shape2.pos.y) < (shape1.height + shape2.height) / 2);
}

mkrtchyan::Matrix mkrtchyan::division(const Shape::pointerArray & sample, size_t size)
{
  Matrix matrix;

  size_t lines = 0;
  size_t columns = 0;

  for (size_t i = 0; i < size; i++)
  {
    for (size_t j = 0; j < matrix.getLines(); j++)
    {
      for (size_t k = 0; k < matrix.getColumns(); k++)
      {
        if (matrix[j][k] == nullptr)
        {
          lines = j;
          columns = k;
          break;
        }

        if (mkrtchyan::intersectionExists(matrix[j][k]->getFrameRect(), sample[i]->getFrameRect()))
        {
          lines = j + 1;
          columns = 0;
          break;
        }

        lines = j;
        columns = k + 1;
      }

      if (lines == j)
      {
        break;
      }
    }

    matrix.addShape(sample[i], lines, columns);
  }

  return matrix;
}

mkrtchyan::Matrix mkrtchyan::division(const CompositeShape & sample)
{
  return division(sample.getShapes(), sample.getSize());
}
