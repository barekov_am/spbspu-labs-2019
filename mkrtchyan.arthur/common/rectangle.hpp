#ifndef A4_RECTANGLE_HPP
#define A4_RECTANGLE_HPP
#include "shape.hpp"

namespace mkrtchyan
{
  class Rectangle : public mkrtchyan::Shape
  {
  public:
    Rectangle(double width, double height, point_t pos);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(point_t point) override;
    void move(double dx, double dy) override;
    void scale(double coefficient) override;
    void rotate(double angle) override;
    point_t getCenter() const override;

  private:
    double width_;
    double height_;
    point_t pos_;
    double angle_;
  };
}

#endif
