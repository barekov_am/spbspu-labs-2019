#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include "matrix.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"
#include "shape.hpp"

BOOST_AUTO_TEST_SUITE(testMatrixSuite)

BOOST_AUTO_TEST_CASE(useCopyConstructor)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});

  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  mkrtchyan::Matrix matrix = mkrtchyan::division(composite);

  mkrtchyan::Matrix copyMatrix(matrix);

  BOOST_CHECK(matrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(useCopyOperator)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});

  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

   mkrtchyan::Matrix matrix = mkrtchyan::division(composite);

   mkrtchyan::Matrix copyMatrix;
  copyMatrix = matrix;

  BOOST_CHECK(matrix == copyMatrix);
}

BOOST_AUTO_TEST_CASE(useMoveConstructor)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});

  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  mkrtchyan::Matrix matrix = mkrtchyan::division(composite);

  mkrtchyan::Matrix copyMatrix(matrix);
  mkrtchyan::Matrix moveMatrix = std::move(matrix);

  BOOST_CHECK(copyMatrix == moveMatrix);
  BOOST_CHECK_EQUAL(matrix.getLines(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(useMoveOperator)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(4, mkrtchyan::point_t{1.3, -5.2});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(4, 6.4, mkrtchyan::point_t{1.3, -5.2});

  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  mkrtchyan::Matrix matrix = mkrtchyan::division(composite);

  mkrtchyan::Matrix copyMatrix(matrix);
  mkrtchyan::Matrix moveMatrix;
  moveMatrix = std::move(matrix);

  BOOST_CHECK(copyMatrix == moveMatrix);
  BOOST_CHECK_EQUAL(matrix.getLines(), 0);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(testCreationAndAndAddingElements)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(1, mkrtchyan::point_t{2, 3});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(1, 3, mkrtchyan::point_t{6, 7});
  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  size_t rows = 1;
  const size_t columns = 2;

  mkrtchyan::Matrix matrix = mkrtchyan::division(composite);
  BOOST_CHECK_EQUAL(matrix.getLines(), rows);
  BOOST_CHECK_EQUAL(matrix.getColumns(), columns);
  BOOST_CHECK_EQUAL(matrix[0][0], circle);
  BOOST_CHECK_EQUAL(matrix[0][1], rectangle);

  mkrtchyan::Shape::pointer rectangle1 = std::make_shared<mkrtchyan::Rectangle>(2, 2, mkrtchyan::point_t{2, 3});
  composite.add(rectangle1);
  matrix = mkrtchyan::division(composite);
  rows++;

  BOOST_CHECK_EQUAL(matrix.getLines(), rows);
  BOOST_CHECK_EQUAL(matrix.getColumns(), columns);
  BOOST_CHECK_EQUAL(matrix[0][0], circle);
  BOOST_CHECK_EQUAL(matrix[0][1], rectangle);
  BOOST_CHECK_EQUAL(matrix[1][0], rectangle1);
}

BOOST_AUTO_TEST_CASE(exeptionHandling)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(1, mkrtchyan::point_t{2, 3});
  mkrtchyan::Shape::pointer rectangle = std::make_shared<mkrtchyan::Rectangle>(1.5, 3, mkrtchyan::point_t{6, 7});

  mkrtchyan::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  mkrtchyan::Matrix matrix = mkrtchyan::division(composite);

  BOOST_CHECK_THROW(matrix[6], std::out_of_range);
  BOOST_CHECK_THROW(matrix.getLineSize(7), std::logic_error);
}

BOOST_AUTO_TEST_SUITE_END()


