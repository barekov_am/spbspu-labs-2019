#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(circleTest)

BOOST_AUTO_TEST_CASE(constAfterMovingToPoint)
{
  mkrtchyan::Circle circle(3.6, {-3.2, 5});
  const double radiusBefore = circle.getRadius();
  const double areaBefore = circle.getArea();

  circle.move({ 5.3, 7 });

  BOOST_CHECK_CLOSE(radiusBefore, circle.getRadius(), ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(constAfterMovingDxDy)
{
  mkrtchyan::Circle circle(3.6, {-3.2, 5});
  const double radiusBefore = circle.getRadius();
  const double areaBefore = circle.getArea();

  circle.move(5.3, 7);

  BOOST_CHECK_CLOSE(radiusBefore, circle.getRadius(), ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(constAfterScaling)
{
  mkrtchyan::Circle circle(6, {0, 0});
  const double areaBefore = circle.getArea();
  const double radiusBefore = circle.getRadius();
  const double coefficient = 5.6;

  circle.scale(coefficient);

  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, circle.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(radiusBefore * coefficient, circle.getRadius(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(testRotating)
{
  mkrtchyan::Circle circle(5.2, {3.2, 4.5});

  const mkrtchyan::rectangle_t frameRectBefore = circle.getFrameRect();
  const double areaBefore = circle.getArea();

  const size_t angle = 45;

  circle.rotate(angle);

  const mkrtchyan::rectangle_t frameRectAfter = circle.getFrameRect();
  const double areaAfter = circle.getArea();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidRadius)
{
  BOOST_CHECK_THROW(mkrtchyan::Circle circle(-6.3, {0.5, 2}), std::invalid_argument);
  BOOST_CHECK_THROW(mkrtchyan::Circle circle(0, {0.5, 2}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidCoefficientOfScaling)
{
  mkrtchyan::Circle circle(3, {0.3, 1.3});
  BOOST_CHECK_THROW(circle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(circle.scale(-4.5), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
