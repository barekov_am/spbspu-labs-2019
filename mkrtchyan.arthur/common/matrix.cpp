#include "matrix.hpp"
#include <stdexcept>
#include <iostream>

mkrtchyan::Matrix::Matrix():
  lines_(0),
  columns_(0)
{

}

mkrtchyan::Matrix::Matrix(const Matrix& sample):
  lines_(sample.lines_),
  columns_(sample.columns_),
  shapes_(std::make_unique<Shape::pointer []>(sample.lines_ * sample.columns_))
{
  for (size_t i = 0; i < (lines_  * columns_); i++)
  {
    shapes_[i] = sample.shapes_[i];
  }
}

mkrtchyan::Matrix::Matrix(mkrtchyan::Matrix&& sample):
  lines_(sample.lines_),
  columns_(sample.columns_),
  shapes_(std::move(sample.shapes_))
{
  sample.lines_ = 0;
  sample.columns_ = 0;
}

mkrtchyan::Matrix &mkrtchyan::Matrix::operator =(const Matrix& sample)
{
  if (this == &sample)
  {
    return *this;
  }

  Shape::pointerArray tmpMatrix(std::make_unique<Shape::pointer []>(sample.lines_ * sample.columns_));

  for (size_t i = 0; i < (sample.lines_ * sample.columns_); i++)
  {
    tmpMatrix[i] = sample.shapes_[i];
  }

  shapes_.swap(tmpMatrix);
  lines_ = sample.lines_;
  columns_ = sample.columns_;

  return *this;
}

mkrtchyan::Matrix &mkrtchyan::Matrix::operator =(Matrix&& sample)
{
  if (this == &sample)
  {
    return *this;
  }

  lines_ = sample.lines_;
  columns_ = sample.columns_;
  shapes_ = std::move(sample.shapes_);
  sample.lines_ = 0;
  sample.columns_ = 0;

  return *this;
}

mkrtchyan::Shape::pointer * mkrtchyan::Matrix::operator [](size_t index) const
{
  if (index >= lines_)
  {
    throw std::out_of_range("Index is out of range");
  }

  size_t tmpIndex = index * columns_;
  return &shapes_[tmpIndex];
}

bool mkrtchyan::Matrix::operator==(const Matrix &sample) const
{
  if ((lines_ != sample.lines_) || (columns_ != sample.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (lines_ * columns_); i++)
  {
    if (shapes_[i] != sample.shapes_[i])
    {
      return false;
    }
  }

  return true;
}

bool mkrtchyan::Matrix::operator!=(const Matrix &sample) const
{
  return !(*this == sample);
}

void mkrtchyan::Matrix::addShape(Shape::pointer shape, size_t line, size_t column)
{
  size_t tmpRows = (line >= lines_) ? (lines_ + 1) : (lines_);
  size_t tmpColumns = (column >= columns_) ? (columns_ + 1) : (columns_);

  Shape::pointerArray tmpMatrix(std::make_unique<Shape::pointer[]>(tmpRows * tmpColumns));

  for (size_t i = 0; i < tmpRows; i++)
  {
    for (size_t j = 0; j < tmpColumns; j++)
    {
      if ((lines_ == i) || (columns_ == j))
      {
        tmpMatrix[i * tmpColumns + j] = nullptr;
      }

      else
      {
        tmpMatrix[i * tmpColumns + j] = shapes_[i * columns_ + j];
      }
    }
  }

  tmpMatrix[line * tmpColumns + column] = shape;
  shapes_.swap(tmpMatrix);
  lines_ = tmpRows;
  columns_ = tmpColumns;
}

size_t mkrtchyan::Matrix::getLines() const
{
  return lines_;
}

size_t mkrtchyan::Matrix::getColumns() const
{
  return columns_;
}

size_t mkrtchyan::Matrix::getLineSize(size_t index) const
{
  if (index >= lines_)
  {
    throw std::logic_error("Invalid index");
  }

  for (size_t i = 0; i < columns_; i++)
  {
    if (shapes_[index * columns_ + i] == nullptr)
    {
      return i + 1;
    }
  }

  return columns_;
}
