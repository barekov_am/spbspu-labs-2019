#include "triangle.hpp"
#include <stdexcept>
#include <cmath>
#include <algorithm>

mkrtchyan::Triangle::Triangle(point_t point1, point_t point2, point_t point3):
  point1_(point1),
  point2_(point2),
  point3_(point3)
{
  if (getArea() <= 0)
  {
    throw std::invalid_argument("Invalid point coordinates");
  }
}

static double getSegment(mkrtchyan::point_t point1, mkrtchyan::point_t point2)
{
  const mkrtchyan::point_t vector = {(point2.x - point1.x), (point2.y - point1.y)};
  return sqrt(vector.x * vector.x + vector.y * vector.y);
}

double mkrtchyan::Triangle::getArea() const
{
  const double vector1Length = getSegment(point1_, point2_);
  const double vector2Length = getSegment(point1_, point3_);
  const double vector3Length = getSegment(point2_, point3_);
  const double halfPer = (vector1Length + vector2Length + vector3Length) / 2;
  return sqrt(halfPer * (halfPer - vector1Length) * (halfPer - vector2Length) * (halfPer - vector3Length));
}

mkrtchyan::rectangle_t mkrtchyan::Triangle::getFrameRect() const
{
  const double xMax = std::max({point1_.x, point2_.x, point3_.x});
  const double xMin = std::min({point1_.x, point2_.x, point3_.x});
  const double yMax = std::max({point1_.y, point2_.y, point3_.y});
  const double yMin = std::min({point1_.y, point2_.y, point3_.y});
  const double rectWidth = xMax - xMin;
  const double rectHeight = yMax - yMin;
  return {rectWidth, rectHeight, {xMax - rectWidth / 2, yMax - rectHeight / 2}};
}

void mkrtchyan::Triangle::move(point_t point)
{
  const point_t center = getCenter();
  move(point.x - center.x, point.y - center.y);
}

void mkrtchyan::Triangle::move(double dx, double dy)
{
  point1_.x += dx;
  point1_.y += dy;
  point2_.x += dx;
  point2_.y += dy;
  point3_.x += dx;
  point3_.y += dy;
}

void mkrtchyan::Triangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }
  const point_t center = getCenter();
  point1_.x = center.x + (point1_.x - center.x) * coefficient;
  point1_.y = center.y + (point1_.y - center.y) * coefficient;
  point2_.x = center.x + (point2_.x - center.x) * coefficient;
  point2_.y = center.y + (point2_.y - center.y) * coefficient;
  point3_.x = center.x + (point3_.x - center.x) * coefficient;
  point3_.y = center.y + (point3_.y - center.y) * coefficient;
}

mkrtchyan::point_t rotatePoint(mkrtchyan::point_t point, double angle)
{
  const double x = point.x * cos(M_PI * angle / 180) - point.y * sin(M_PI * angle / 180);
  const double y = point.y * cos(M_PI * angle / 180) + point.x * sin(M_PI * angle / 180);
  return {x, y};
}

void mkrtchyan::Triangle::rotate(double angle)
{
  if (angle < 0 )
  {
    angle = (360 + fmod(angle, 360));
  }
  point1_ = rotatePoint(point1_, angle);
  point2_ = rotatePoint(point2_, angle);
  point3_ = rotatePoint(point3_, angle);
}

mkrtchyan::point_t mkrtchyan::Triangle::getCenter() const
{
  return {(point1_.x + point2_.x + point3_.x) / 3, (point1_.y + point2_.y + point3_.y) / 3};
}
