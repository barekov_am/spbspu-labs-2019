#ifndef A4_CIRCLE_HPP
#define A4_CIRCLE_HPP
#include "shape.hpp"

namespace mkrtchyan
{
  class Circle : public mkrtchyan::Shape
  {
  public:
    Circle(double radius, point_t pos);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(point_t point) override;
    void move(double dx, double dy) override;
    void scale(double coefficient) override;
    void rotate(double /*angle*/) override;
    point_t getCenter() const override;
    double getRadius() const;

  private:
    double radius_;
    point_t pos_;
  };
}

#endif
