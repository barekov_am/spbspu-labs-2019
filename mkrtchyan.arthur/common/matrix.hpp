#ifndef A4_MATRIX_HPP
#define A4_MATRIX_HPP
#include "shape.hpp"

namespace mkrtchyan
{
 class Matrix
 {
 public:
   Matrix();
   Matrix(const Matrix&);
   Matrix(Matrix&&);
   ~Matrix() = default;

   Matrix &operator =(const Matrix&);
   Matrix &operator =(Matrix&&);
   Shape::pointer * operator [](size_t index) const;
   bool operator ==(const Matrix &sample) const;
   bool operator !=(const Matrix &sample) const;

   void addShape(Shape::pointer shape, size_t line, size_t column);
   size_t getLines() const;
   size_t getColumns() const;
   size_t getLineSize(size_t index) const;

 private:
   size_t lines_;
   size_t columns_;
   Shape::pointerArray shapes_;
 };
};

#endif
