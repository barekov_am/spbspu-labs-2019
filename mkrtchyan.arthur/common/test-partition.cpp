#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include "partition.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"

BOOST_AUTO_TEST_SUITE(testPartionSuite)

BOOST_AUTO_TEST_CASE(testOverlapping)
{
  mkrtchyan::Shape::pointer circle = std::make_shared<mkrtchyan::Circle>(1, mkrtchyan::point_t{2, 3});
  mkrtchyan::Shape::pointer rectangle1 = std::make_shared<mkrtchyan::Rectangle>(1.5, 3, mkrtchyan::point_t{6, 7});
  mkrtchyan::Shape::pointer rectangle2 = std::make_shared<mkrtchyan::Rectangle>(2, 2, mkrtchyan::point_t{2, 3});

  BOOST_CHECK(!mkrtchyan::intersectionExists(circle->getFrameRect(), rectangle1->getFrameRect()));
  BOOST_CHECK(mkrtchyan::intersectionExists(circle->getFrameRect(), rectangle2->getFrameRect()));
  BOOST_CHECK(!mkrtchyan::intersectionExists(rectangle1->getFrameRect(), rectangle2->getFrameRect()));
}

BOOST_AUTO_TEST_CASE(testPartition)
{
  mkrtchyan::Shape::pointer circle1 = std::make_shared<mkrtchyan::Circle>(1, mkrtchyan::point_t{2, 3});
  mkrtchyan::Shape::pointer rectangle1 = std::make_shared<mkrtchyan::Rectangle>(1.5, 3, mkrtchyan::point_t{6, 7});
  mkrtchyan::Shape::pointer circle2 = std::make_shared<mkrtchyan::Circle>(3, mkrtchyan::point_t{8, 7});
  mkrtchyan::Shape::pointer rectangle2 = std::make_shared<mkrtchyan::Rectangle>(2, 2, mkrtchyan::point_t{2, 3});

  mkrtchyan::CompositeShape composite;
  composite.add(circle1);
  composite.add(rectangle1);
  composite.add(circle2);
  composite.add(rectangle2);

  mkrtchyan::Matrix matrix = mkrtchyan::division(composite);

  BOOST_CHECK_EQUAL(matrix[0][0], circle1);
  BOOST_CHECK_EQUAL(matrix[0][1], rectangle1);
  BOOST_CHECK_EQUAL(matrix[1][0], circle2);
  BOOST_CHECK_EQUAL(matrix[1][1], rectangle2);
}

BOOST_AUTO_TEST_SUITE_END()


