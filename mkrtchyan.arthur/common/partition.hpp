#ifndef A4_PARTITION_HPP
#define A4_PARTITION_HPP
#include "composite-shape.hpp"
#include "matrix.hpp"

namespace mkrtchyan
{
  bool intersectionExists(const rectangle_t & shape1, const rectangle_t & shape2);
  Matrix division(const Shape::pointerArray & sample, size_t size);
  Matrix division(const CompositeShape & sample);
}

#endif
