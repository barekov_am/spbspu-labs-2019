#ifndef A4_COMPOSITE_SHAPE_HPP
#define A4_COMPOSITE_SHAPE_HPP
#include "shape.hpp"

namespace mkrtchyan
{
  class CompositeShape: public mkrtchyan::Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape&);
    CompositeShape(CompositeShape&&);
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape&);
    CompositeShape& operator =(CompositeShape&&);
    Shape::pointer operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(point_t point) override;
    void move(double dx, double dy) override;
    void scale(double coefficient) override;
    void rotate(double angle) override;
    point_t getCenter() const override;
    void add(Shape::pointer & shape);
    void remove(size_t index);
    Shape::pointerArray getShapes() const;
    size_t getSize() const;

  private:
    size_t size_;
    Shape::pointerArray shapes_;
  };
}

#endif
