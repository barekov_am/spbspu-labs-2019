#include "circle.hpp"
#include <stdexcept>
#include <cmath>

mkrtchyan::Circle::Circle(double radius, point_t pos):
  radius_(radius),
  pos_(pos)
{
  if (radius <= 0)
  {
    throw std::invalid_argument("Invalid radius");
  }
}

double mkrtchyan::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

mkrtchyan::rectangle_t mkrtchyan::Circle::getFrameRect() const
{
  return {2 * radius_, 2 * radius_, pos_};
}

void mkrtchyan::Circle::move(point_t point)
{
  pos_ = point;
}

void mkrtchyan::Circle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void mkrtchyan::Circle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }
  radius_ *= coefficient;
}

void mkrtchyan::Circle::rotate(double /*angle*/)
{

}

mkrtchyan::point_t mkrtchyan::Circle::getCenter() const
{
  return pos_;
}

double mkrtchyan::Circle::getRadius() const
{
  return radius_;
}
