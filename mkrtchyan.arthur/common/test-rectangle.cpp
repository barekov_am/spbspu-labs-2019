#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include "rectangle.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(constAfterMovingToPoint)
{
  mkrtchyan::Rectangle rectangle(2, 5, {-3, 2.5});
  const double widthBefore = rectangle.getFrameRect().width;
  const double heightBefore = rectangle.getFrameRect().height;
  const double areaBefore = rectangle.getArea();

  rectangle.move({2, 3});

  BOOST_CHECK_CLOSE(widthBefore, rectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, rectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(constAfterMovingToDxDy)
{
  mkrtchyan::Rectangle rectangle(2, 5, {-3, 2.5});
  const double widthBefore = rectangle.getFrameRect().width;
  const double heightBefore = rectangle.getFrameRect().height;
  const double areaBefore = rectangle.getArea();

  rectangle.move(2, 3);

  BOOST_CHECK_CLOSE(widthBefore, rectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, rectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(constAfterScaling)
{
  mkrtchyan::Rectangle rectangle(5, 6, {5.2, -8.1});
  const double widthBefore = rectangle.getFrameRect().width;
  const double heightBefore = rectangle.getFrameRect().height;
  const double areaBefore = rectangle.getArea();
  const double coefficient = 2.7;

  rectangle.scale(coefficient);

  BOOST_CHECK_CLOSE(widthBefore * coefficient, rectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(heightBefore * coefficient, rectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore * coefficient * coefficient, rectangle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(testRotatingOn45)
{
  mkrtchyan::Rectangle rectangle(4, 4, {12, 8});

  const mkrtchyan::rectangle_t frameRectBefore = rectangle.getFrameRect();
  const double areaBefore = rectangle.getArea();

  const double angle = 45;

  rectangle.rotate(angle);

  const mkrtchyan::rectangle_t frameRectAfter = rectangle.getFrameRect();
  const double areaAfter = rectangle.getArea();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width * std::sqrt(2), frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height * std::sqrt(2), frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testRotatingOn90)
{
  mkrtchyan::Rectangle rectangle(4, 4, {12, 8});

  const mkrtchyan::rectangle_t frameRectBefore = rectangle.getFrameRect();
  const double areaBefore = rectangle.getArea();

  const double angle = 90;

  rectangle.rotate(angle);

  const mkrtchyan::rectangle_t frameRectAfter = rectangle.getFrameRect();
  const double areaAfter = rectangle.getArea();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testRotatingOn180)
{
  mkrtchyan::Rectangle rectangle(4, 4, {12, 8});

  const mkrtchyan::rectangle_t frameRectBefore = rectangle.getFrameRect();
  const double areaBefore = rectangle.getArea();

  const double angle = 180;

  rectangle.rotate(angle);

  const mkrtchyan::rectangle_t frameRectAfter = rectangle.getFrameRect();
  const double areaAfter = rectangle.getArea();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidWidth)
{
  BOOST_CHECK_THROW(mkrtchyan::Rectangle rectangle(-12.4, 4, {0.5, 2}), std::invalid_argument);
  BOOST_CHECK_THROW(mkrtchyan::Rectangle rectangle(0, 4, {0.5, 2}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidHeight)
{
  BOOST_CHECK_THROW(mkrtchyan::Rectangle rectangle(18.4, -4.6, {0.5, 2}), std::invalid_argument);
  BOOST_CHECK_THROW(mkrtchyan::Rectangle rectangle(18.4, 0, {0.5, 2}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidCoefficientOfScaling)
{
  mkrtchyan::Rectangle rectangle(3, 6.3, {0.5, 2.3});
  BOOST_CHECK_THROW(rectangle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(rectangle.scale(-2.5), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

