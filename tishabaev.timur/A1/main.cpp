#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

int main()
{
  Rectangle shape_1({0, 0}, 30, 10);
  Shape *rectangle = &shape_1;

  rectangle->move(10, -5);
  rectangle->move({10, -5});

  Circle shape_2({3, -2}, 5);
  Shape *circle = &shape_2;

  circle->move(-7, 1);
  circle->move({12, 8});

  Triangle shape_3({0, 0}, {0, 3}, {4, 0});
  Shape *triangle = &shape_3;

  triangle->move(-6, 8);
  triangle->move({12, -3});

  Shape *shapes[] = {rectangle, circle, triangle};

  size_t i = 1;

  for (const Shape *shape : shapes)
  {
    rectangle_t frame_rect = shape->getFrameRect();
    shape->printData();

    std::cout << "  area = " << shape->getArea() << std::endl
        << "  framing rectangle" << std::endl
        << "    pos (" << frame_rect.pos.x << ", " << frame_rect.pos.y << ")" << std::endl
        << "    width = " << frame_rect.width << std::endl
        << "    height - " << frame_rect.height << std::endl << std::endl;

    i++;
  }

  return 0;
}
