#include "triangle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>

Triangle::Triangle(const point_t &pos_a, const point_t &pos_b, const point_t &pos_c) :
  pos_({(pos_a.x + pos_b.x + pos_c.x) / 3, (pos_a.y + pos_b.y + pos_c.y) / 3}),
  points_{pos_a, pos_b, pos_c}
{
  assert(getArea() > 0);
}

double Triangle::getArea() const
{
  return fabs(((points_[0].x - points_[2].x) * (points_[1].y - points_[2].y)
      - (points_[1].x - points_[2].x) * (points_[0].y - points_[2].y))) / 2;
}

rectangle_t Triangle::getFrameRect() const
{
  point_t top_left = pos_;
  point_t bottom_right = pos_;

  for (point_t point : points_)
  {
    top_left.x = std::min(point.x, top_left.x);
    top_left.y = std::max(point.y, top_left.y);
    bottom_right.x = std::max(point.x, bottom_right.x);
    bottom_right.y = std::min(point.y, bottom_right.y);
  }

  return {{(top_left.x + bottom_right.x) / 2, (top_left.y + bottom_right.y) / 2},
      bottom_right.x - top_left.x, top_left.y - bottom_right.y};
}

void Triangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;

  for (point_t &point : points_)
  {
    point.x += dx;
    point.y += dy;
  }
}

void Triangle::move(const point_t &pos)
{
  move(pos.x - pos_.x, pos.y - pos_.y);
}

void Triangle::printData() const
{
  std::cout << "Triangle" << std::endl
      << "  pos (" << pos_.x << ", " << pos_.y << ")" << std::endl
      << "  A (" << points_[0].x << ", " << points_[0].y << ")" << std::endl
      << "  B (" << points_[1].x << ", " << points_[1].y << ")" << std::endl
      << "  C (" << points_[2].x << ", " << points_[2].y << ")" << std::endl;
}
