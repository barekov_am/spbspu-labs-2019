#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "array.hpp"
#include "partition.hpp"

const double TOLERANCE = 0.01;

BOOST_AUTO_TEST_SUITE(testForArray)

BOOST_AUTO_TEST_CASE(copyAndMove)
{
  medvedev::Rectangle rectangle(4, 6, {6, 7});
  medvedev::CompositeShape compositeShape;
  compositeShape.add(&rectangle);
  medvedev::Array array = medvedev::division(compositeShape);

  medvedev::Array array3 = array;

  BOOST_CHECK_EQUAL(array3.getRows(), 1);
  BOOST_CHECK_EQUAL(array3.getColumns(),1);
  BOOST_CHECK_NO_THROW(medvedev::Array array1(array));
  BOOST_CHECK_NO_THROW(medvedev::Array array2(std::move(array)));

  medvedev::Array array4;
  medvedev::Array array5;

  BOOST_CHECK_NO_THROW(array4 = array);
  BOOST_CHECK_NO_THROW(array5 = std::move(array));
}

BOOST_AUTO_TEST_CASE(testForThrow)
{
  medvedev::Rectangle rectangle(2, 4.5, {1, 2});
  medvedev::Rectangle rectangle1(3.5, 2, {2, 5});
  medvedev::point_t center{6, 4};
  medvedev::Circle circle(center, 2);
  medvedev::Triangle triangle({3, 2}, {2, 0}, {4, 0});
  medvedev::Triangle triangle1({7, 4}, {10, 3}, {10, 5});
  medvedev::CompositeShape compositeShape;
  compositeShape.add(&rectangle);
  compositeShape.add(&rectangle1);
  compositeShape.add(&circle);
  compositeShape.add(&triangle);
  compositeShape.add(&triangle1);

  medvedev::Array array = medvedev::division(compositeShape);
  BOOST_CHECK_THROW(array[10][10], std::out_of_range);
  BOOST_CHECK_NO_THROW(array[0][1]);

}

BOOST_AUTO_TEST_CASE(testCorrectWorkingOperators)
{
  medvedev::point_t center1 = { 14, 56 };
  medvedev::point_t center2 = { -53, 123 };
  medvedev::point_t center3 = { 39, -58 };
  medvedev::point_t center4 = { 59, -68 };

  medvedev::Circle circle1(center1, 5);
  medvedev::Circle circle2(center2, 9);
  medvedev::Circle circle3(center4, 13);
  medvedev::Circle circle4(center1, 10);
  medvedev::Rectangle rect1(2, 7, center1);
  medvedev::Rectangle rect2(8, 10, center3);
  medvedev::Rectangle rect3(5, 15, center4);
  medvedev::Rectangle rect4(3, 5, center1);

  medvedev::Shape *p_circle1 = &circle1;
  medvedev::Shape *p_circle2 = &circle2;
  medvedev::Shape *p_circle3 = &circle3;
  medvedev::Shape *p_circle4 = &circle4;
  medvedev::Shape *p_rect1 = &rect1;
  medvedev::Shape *p_rect2 = &rect2;
  medvedev::Shape *p_rect3 = &rect3;
  medvedev::Shape *p_rect4 = &rect4;

  medvedev::CompositeShape composite_shape;
  composite_shape.add(p_circle1);
  composite_shape.add(p_rect1);
  composite_shape.add(p_circle2);
  composite_shape.add(p_rect2);
  composite_shape.add(p_circle3);
  composite_shape.add(p_rect3);
  medvedev::Array array = medvedev::division(composite_shape);

  BOOST_CHECK(array[0][0] == p_circle1);
  BOOST_CHECK(array[0][1] == p_circle2);
  BOOST_CHECK(array[0][2] == p_rect2);
  BOOST_CHECK(array[0][3] == p_circle3);
  BOOST_CHECK(array[1][0] == p_rect1);
  BOOST_CHECK(array[1][1] == p_rect3);
  BOOST_CHECK(array[0][0] != p_rect3);
  BOOST_CHECK(array[1][0] != p_rect4);
  BOOST_CHECK(array[0][3] != p_circle4);

  medvedev::CompositeShape composite_shape1;
  medvedev::CompositeShape composite_shape2;
  composite_shape.add(p_circle1);
  composite_shape.add(p_rect1);
  composite_shape1.add(p_circle2);
  composite_shape.add(p_rect2);
  composite_shape.add(p_circle3);
  composite_shape.add(p_rect3);
  medvedev::Array array1 = medvedev::division(composite_shape1);
  medvedev::Array array2 = medvedev::division(composite_shape2);

  BOOST_CHECK(array1 != array2);
  BOOST_CHECK(array1 != array);
  BOOST_CHECK(array != array2);

  array1 = array;
  BOOST_CHECK(array1 == array);
}

BOOST_AUTO_TEST_SUITE_END()
