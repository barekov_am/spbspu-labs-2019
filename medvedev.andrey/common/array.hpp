#ifndef ARRAY_HPP
#define ARRAY_HPP

#include "shape.hpp"
#include <memory>

namespace medvedev
{
 class Array
 {
 public:
   Array();
   Array(const Array &rhs);
   Array(Array &&rhs);
   ~Array() = default;

   Array &operator =(const Array &rhs);
   Array &operator =(Array &&rhs);
   std::unique_ptr<medvedev::Shape*[]>  operator [](size_t index) const;
   bool operator ==(const Array &shapesArray) const;
   bool operator !=(const Array &shapesArray) const;

   void add(Shape* shape, size_t row, size_t column);
   size_t getRows() const;
   size_t getColumns() const;
   size_t getRowSize(size_t row) const;

 private:
   size_t rows_;
   size_t columns_;
   std::unique_ptr<Shape*[]> shapes_;
 };
}

#endif
