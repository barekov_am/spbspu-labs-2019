#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

medvedev::Rectangle::Rectangle(double width, double height,const point_t &center) :
  width_(width),
  height_(height),
  center_(center)
{
  if ((width_ <= 0.0) && (height_ <= 0.0))
  {
    throw std::invalid_argument("Invalid width or height");
  }
}

double medvedev::Rectangle::getArea() const
{
  return width_ * height_;
}

medvedev::rectangle_t medvedev::Rectangle::getFrameRect() const
{
  return {width_, height_, center_};
}

void medvedev::Rectangle::move(const point_t &point)
{
  center_.x = point.x;
  center_.y = point.y;
}

void medvedev::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void medvedev::Rectangle::scale(double n)
{
  if (n<=0)
    throw std::invalid_argument("Invalid point");

  width_ = width_ * n;
  height_ = height_ * n;
}

void medvedev::Rectangle::rotate(double angle)
{
  rotationDegree_ += angle;
  while (std::abs(rotationDegree_) >= 360)
  {
    rotationDegree_ = (angle > 0) ? rotationDegree_ - 360 : rotationDegree_ + 360;
  }
}
