#ifndef BOOST_TEST_CIRCLE
#define BOOST_TEST_CIRCLE

#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include "circle.hpp"

const double TOLERANCE = 0.001;

BOOST_AUTO_TEST_SUITE(Circle_tests)

BOOST_AUTO_TEST_CASE(radius_after_move_on)
{
  medvedev::point_t center{0, 0};
  medvedev::Circle circle(center, 3);
  const double radius = circle.getRadius();
  circle.move(2, 3);
  BOOST_CHECK_CLOSE(circle.getRadius(), radius, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(radius_after_move_to)
{
  medvedev::point_t center{0, 0};
  medvedev::Circle circle(center, 3);
  const double radius = circle.getRadius();
  circle.move({5, 5});
  BOOST_CHECK_CLOSE(circle.getRadius(), radius, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(frame_rect_after_move_on)
{
  medvedev::point_t center{0, 0};
  medvedev::Circle circle(center, 3);

  const double width = circle.getFrameRect().width;
  const double height = circle.getFrameRect().height;
  circle.move(2, 3);
  BOOST_CHECK_CLOSE(circle.getFrameRect().width, width, TOLERANCE);
  BOOST_CHECK_CLOSE(circle.getFrameRect().height, height, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(frame_rect_after_move_to)
{
  medvedev::point_t center{0, 0};
  medvedev::Circle circle(center, 3);

  const double width = circle.getFrameRect().width;
  const double height = circle.getFrameRect().height;
  circle.move({2, 3});
  BOOST_CHECK_CLOSE(circle.getFrameRect().width, width, TOLERANCE);
  BOOST_CHECK_CLOSE(circle.getFrameRect().height, height, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(area_after_move_on)
{
  medvedev::point_t center{0, 0};
  medvedev::Circle circle(center, 3);
  const double area = circle.getArea();
  circle.move(2, 3);
  BOOST_CHECK_CLOSE(circle.getArea(), area, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(area_after_move_to)
{
  medvedev::point_t center{0, 0};
  medvedev::Circle circle(center, 3);
  const double area = circle.getArea();
  circle.move({5, 5});
  BOOST_CHECK_CLOSE(circle.getArea(), area, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(area_scaling_increase)
{
  medvedev::point_t center{0, 0};
  medvedev::Circle circle(center, 3);
  double area = circle.getArea();
  circle.scale(2);
  BOOST_CHECK_CLOSE(circle.getArea(), 4 * area, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(area_scaling_decrease)
{
  medvedev::point_t center{0, 0};
  medvedev::Circle circle(center, 3);
  double area = circle.getArea();
  circle.scale(0.5);
  BOOST_CHECK_CLOSE(circle.getArea(), area / 4, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(scale_invalid_argument_test)
{
  medvedev::point_t center{0, 0};
  medvedev::Circle circle(center, 3);
  BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(constructor_invalid_argument_test)
{
  medvedev::point_t center{0, 0};
  BOOST_CHECK_THROW(medvedev::Circle(center, 0), std::invalid_argument);
  BOOST_CHECK_THROW(medvedev::Circle(center, -1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

#endif
