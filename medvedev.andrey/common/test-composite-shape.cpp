#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double TOLERANCE = 0.00000001;

BOOST_AUTO_TEST_SUITE(Polygon_tests)

BOOST_AUTO_TEST_CASE(test_CompositeShape_one_figure_in_two_c_shapes)
{
  medvedev::point_t center{5, -1};
  medvedev::Circle circle(center, 1);
  medvedev::CompositeShape *c1_shape = new medvedev::CompositeShape;
  c1_shape->add(&circle);
  medvedev::CompositeShape c2_shape;
  c2_shape.add(&circle);

  BOOST_CHECK_EQUAL((*c1_shape)[0], c2_shape[0]);
  delete c1_shape;
  BOOST_CHECK_EQUAL(&circle, c2_shape[0]);
}

BOOST_AUTO_TEST_CASE(test_CompositeShape_copy_operator)
{
  medvedev::point_t center{5, -1};
  medvedev::Circle circle(center, 1);
  medvedev::CompositeShape *c1_shape = new medvedev::CompositeShape;
  c1_shape->add(&circle);
  medvedev::CompositeShape c2_shape;
  c2_shape = (*c1_shape);

  BOOST_CHECK_EQUAL((*c1_shape)[0], c2_shape[0]);
  delete c1_shape;
  BOOST_CHECK_EQUAL(&circle, c2_shape[0]);
}

BOOST_AUTO_TEST_CASE(test_CompositeShape_copy_operator_empty_c_shape)
{
  medvedev::CompositeShape *c1_shape = new medvedev::CompositeShape;
  medvedev::CompositeShape c2_shape = *c1_shape;

  BOOST_CHECK_EQUAL(c1_shape->getCount(), c2_shape.getCount());
  delete c1_shape;
}

BOOST_AUTO_TEST_CASE(test_CompositeShape_copy_constructor)
{
  medvedev::point_t center{5, -1};
  medvedev::Circle circle(center, 1);
  medvedev::CompositeShape *c1_shape = new medvedev::CompositeShape;
  c1_shape->add(&circle);
  medvedev::CompositeShape c2_shape(*c1_shape);

  BOOST_CHECK_EQUAL((*c1_shape)[0], c2_shape[0]);
  delete c1_shape;
  BOOST_CHECK_EQUAL(&circle, c2_shape[0]);
}

BOOST_AUTO_TEST_CASE(test_CompositeShape_copy_constructor_empty_c_shape)
{
  medvedev::CompositeShape c1_shape;
  medvedev::CompositeShape c2_shape(c1_shape);
  size_t x = 0;
  BOOST_CHECK((c1_shape.getCount() == x) && (c2_shape.getCount() == x));
}

BOOST_AUTO_TEST_CASE(test_CompositeShape_move_operator)
{
  medvedev::point_t center{5, -1};
  medvedev::Circle circle(center, 1);
  medvedev::CompositeShape c1_shape;
  c1_shape.add(&circle);
  medvedev::CompositeShape c2_shape;
  c2_shape = std::move(c1_shape);
  size_t x = 0;

  BOOST_CHECK_EQUAL(&circle, c2_shape[0]);
  BOOST_CHECK_EQUAL(c1_shape.getCount(), x);
}

BOOST_AUTO_TEST_CASE(test_CompositeShape_move_constructor)
{
  medvedev::point_t center{5, -1};
  medvedev::Circle circle(center, 1);
  medvedev::CompositeShape c1_shape;
  c1_shape.add(&circle);

  medvedev::CompositeShape c2_shape(std::move(c1_shape));

  BOOST_CHECK_EQUAL(&circle, c2_shape[0]);
}

BOOST_AUTO_TEST_CASE(test_CompositeShape_adding)
{
  medvedev::Rectangle rectangle(-1, 2, {4, 2});
  medvedev::point_t center{5, -1};
  medvedev::Circle circle(center, 1);

  medvedev::CompositeShape c_shape;
  c_shape.add(&rectangle);
  c_shape.add(&circle);
  size_t x = 2;
  BOOST_CHECK_EQUAL(c_shape[0], &rectangle);
  BOOST_CHECK_EQUAL(c_shape[1], &circle);
  BOOST_CHECK_EQUAL(c_shape.getCount(), x);
}

BOOST_AUTO_TEST_CASE(test_CompositeShape_invalid_add)
{
  medvedev::Shape *p_empty = nullptr;
  medvedev::CompositeShape c_shape;

  BOOST_CHECK_THROW(c_shape.add(p_empty), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(test_CompositeShape_removing)
{
  medvedev::Rectangle rectangle(-1, 2, {4, 2});
  medvedev::point_t center{5, -1};
  medvedev::Circle circle(center, 1);

  medvedev::CompositeShape c_shape;
  c_shape.add(&rectangle);
  c_shape.add(&circle);
  c_shape.remove(1);
  size_t x = 1;
  BOOST_CHECK_EQUAL(c_shape[0], &rectangle);
  BOOST_CHECK_EQUAL(c_shape.getCount(), x);
}

BOOST_AUTO_TEST_CASE(test_CompositeShape_area)
{
  medvedev::Rectangle rectangle(-1, 2, {4, 2});
  medvedev::point_t center{5, -1};
  medvedev::Circle circle(center, 1);

  medvedev::CompositeShape c_shape;
  c_shape.add(&rectangle);
  c_shape.add(&circle);

  BOOST_CHECK_CLOSE(circle.getArea() + rectangle.getArea(), c_shape.getArea(), TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_CompositeShape_FrameRect)
{
  medvedev::Rectangle rectangle(4, 2, {-1, 2});
  medvedev::point_t center{5, -1};
  medvedev::Circle circle(center, 1);

  medvedev::CompositeShape c_shape;
  c_shape.add(&rectangle);
  c_shape.add(&circle);
  medvedev::rectangle_t frameRect = c_shape.getFrameRect();

  //rectangle_t { { 2, 1 }, 6, 8 };
  BOOST_CHECK_CLOSE(frameRect.pos.x, 1.5, TOLERANCE);
  BOOST_CHECK_CLOSE(frameRect.pos.y, 0.5, TOLERANCE);
  BOOST_CHECK_CLOSE(frameRect.height, 5, TOLERANCE);
  BOOST_CHECK_CLOSE(frameRect.width, 9, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_CompositeShape_relative_move)
{
  medvedev::Rectangle rectangle(-1, 2, {4, 2});
  medvedev::point_t center{5, -1};
  medvedev::Circle circle(center, 1);

  medvedev::CompositeShape c_shape;
  c_shape.add(&rectangle);
  c_shape.add(&circle);

  medvedev::point_t cent1 = c_shape[0]->getFrameRect().pos;
  medvedev::point_t cent2 = c_shape[1]->getFrameRect().pos;

  c_shape.move(3, -4);

  medvedev::point_t cent1_new = c_shape[0]->getFrameRect().pos;
  medvedev::point_t cent2_new = c_shape[1]->getFrameRect().pos;

  BOOST_CHECK_CLOSE(cent1.x + 3, cent1_new.x, TOLERANCE);
  BOOST_CHECK_CLOSE(cent1.y - 4, cent1_new.y, TOLERANCE);
  BOOST_CHECK_CLOSE(cent2.x + 3, cent2_new.x, TOLERANCE);
  BOOST_CHECK_CLOSE(cent2.y - 4, cent2_new.y, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_CompositeShape_absolute_move)
{
  medvedev::Rectangle rectangle(-1, 2, {4, 2});
  medvedev::point_t center{5, -1};
  medvedev::Circle circle(center, 1);

  medvedev::CompositeShape c_shape;
  c_shape.add(&rectangle);
  c_shape.add(&circle);
  medvedev::point_t final_point{5, -3};

  c_shape.move(final_point);
  BOOST_CHECK_CLOSE(c_shape.getFrameRect().pos.x, final_point.x, TOLERANCE);
  BOOST_CHECK_CLOSE(c_shape.getFrameRect().pos.y, final_point.y, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_CompositeShape_scale)
{
  medvedev::Rectangle rectangle(-1, 2, {4, 2});
  medvedev::point_t center2{5, -1};
  medvedev::Circle circle(center2, 1);

  medvedev::CompositeShape c_shape;
  c_shape.add(&rectangle);
  c_shape.add(&circle);

  double S = c_shape.getArea();

  medvedev::point_t cent1 = c_shape[0]->getFrameRect().pos;
  medvedev::point_t cent2 = c_shape[1]->getFrameRect().pos;

  c_shape.scale(2);

  medvedev::point_t cent1_new = c_shape[0]->getFrameRect().pos;
  medvedev::point_t cent2_new = c_shape[1]->getFrameRect().pos;
  medvedev::point_t center = c_shape.getFrameRect().pos;

  BOOST_CHECK_CLOSE(c_shape.getArea(), S * 2 * 2, TOLERANCE);
  BOOST_CHECK_CLOSE(cent1.y - center.y, (cent1_new.y - center.y) / 2, TOLERANCE);
  BOOST_CHECK_CLOSE(cent1.x - center.x, (cent1_new.x - center.x) / 2, TOLERANCE);
  BOOST_CHECK_CLOSE(cent2.y - center.y, (cent2_new.y - center.y) / 2, TOLERANCE);
  BOOST_CHECK_CLOSE(cent2.x - center.x, (cent2_new.x - center.x) / 2, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_CompositeShape_scale_invalid_argument)
{
  medvedev::Rectangle rectangle(-1, 2, {4, 2});

  medvedev::CompositeShape c_shape;
  c_shape.add(&rectangle);
  BOOST_CHECK_THROW(c_shape.scale(-5), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
