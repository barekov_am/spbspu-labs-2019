#ifndef BOOST_TEST_RECTANGLE
#define BOOST_TEST_RECTANGLE

#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include <cmath>
#include <stdexcept>

const double TOLERANCE = 0.001;

BOOST_AUTO_TEST_SUITE(Rectangle_tests)

BOOST_AUTO_TEST_CASE(area_after_move_on)
{
  medvedev::Rectangle rectangle(6, 9, {3, 4});
  const double area = rectangle.getArea();
  rectangle.move(2, 3);
  BOOST_CHECK_CLOSE(rectangle.getArea(), area, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(area_after_move_to)
{
  medvedev::Rectangle rectangle(6, 9, {3, 4});
  const double area = rectangle.getArea();
  rectangle.move({5, 5});
  BOOST_CHECK_CLOSE(rectangle.getArea(), area, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(frame_rect_after_move_on)
{
  medvedev::Rectangle rectangle(6, 9, {3, 4});
  const double width = rectangle.getFrameRect().width;
  const double height = rectangle.getFrameRect().height;
  rectangle.move(2, 3);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, width, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, height, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(frame_rect_after_move_to)
{
  medvedev::Rectangle rectangle(6, 9, {3, 4});
  const double width = rectangle.getFrameRect().width;
  const double height = rectangle.getFrameRect().height;
  rectangle.move({2, 3});
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, width, TOLERANCE);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, height, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(area_scale_increase)
{
  medvedev::Rectangle rectangle(6, 9, {3, 4});
  double area = rectangle.getArea();
  rectangle.scale(2);
  BOOST_CHECK_CLOSE(rectangle.getArea(), area * 4, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(area_scale_decrease)
{
  medvedev::Rectangle rectangle(6, 9, {3, 4});
  double area = rectangle.getArea();
  rectangle.scale(0.5);
  BOOST_CHECK_CLOSE(rectangle.getArea(), area / 4, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(scale_invalid_argument_test)
{
  medvedev::Rectangle rectangle(6, 9, {3, 4});
  BOOST_CHECK_THROW(rectangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(constructor_invalid_argument_test)
{
  BOOST_CHECK_THROW(medvedev::Rectangle(0, 0, {3, 4}), std::invalid_argument);
  BOOST_CHECK_THROW(medvedev::Rectangle(-1, -1, {3, 4}), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

#endif
