#ifndef PARTITION_HPP
#define PARTITION_HPP

#include <memory>
#include "composite-shape.hpp"
#include "array.hpp"

namespace medvedev
{
  bool checkIntersect(medvedev::rectangle_t shape1, medvedev::rectangle_t shape2);
  Array division(std::unique_ptr<Shape*[]>, size_t size);
  Array division(CompositeShape &compositeShape);
}

#endif
