#include "composite-shape.hpp"
#include <stdexcept>
#include <math.h>

medvedev::CompositeShape::CompositeShape() :
  count_(0)
{
  container_.reset();
}


medvedev::CompositeShape::CompositeShape(const CompositeShape &c_shape) :
  count_(c_shape.getCount())
{
  container_.reset();
  *this = c_shape;
}

medvedev::CompositeShape::CompositeShape(CompositeShape &&c_shape) :
  count_(0)
{
  container_.reset();
  *this = std::move(c_shape);
}

medvedev::CompositeShape &medvedev::CompositeShape::operator=(const CompositeShape &c_shape)
{
  if (this != &c_shape)
  {
    count_ = c_shape.count_;
    container_.reset();
    if (count_ != 0)
    {
      container_ = boost::shared_array<Shape *>(new Shape *[count_]);
      for (size_t i = 0; i < count_; i++)
      {
        container_[i] = c_shape[i];
      }
    }
  }
  return *this;
}

medvedev::CompositeShape &medvedev::CompositeShape::operator=(CompositeShape &&c_shape)
{
  count_ = c_shape.count_;
  container_ = c_shape.container_;

  c_shape.count_ = 0;
  c_shape.container_.reset();

  return *this;
}

medvedev::Shape *medvedev::CompositeShape::operator[](size_t index) const
{
  if (index >= count_)
  {
    throw std::invalid_argument("This figure had no element with this number");
  }
  return container_[index];
}

size_t medvedev::CompositeShape::getCount() const
{
  return count_;
}

void medvedev::CompositeShape::add(Shape *p_figure)
{
  if (p_figure == nullptr)
  {
    throw std::invalid_argument("Added figure is empty");
  }
  boost::shared_array<Shape *> pointer(new Shape *[count_ + 1]);
  for (size_t i = 0; i < count_; i++)
  {
    pointer[i] = container_[i];
  }
  pointer[count_++] = p_figure;
  container_ = pointer;
}

void medvedev::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::invalid_argument("This figure had no element with this number");
  }
  boost::shared_array<Shape *> pointer(new Shape *[--count_]);
  for (size_t i = 0; i < index; i++)
  {
    pointer[i] = container_[i];
  }
  for (size_t i = index + 1; i < count_; i++)
  {
    pointer[i] = container_[i];
  }
  container_ = pointer;
}

medvedev::point_t medvedev::CompositeShape::getCenter() const
{
  return getFrameRect().pos;
}

double medvedev::CompositeShape::getArea() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Object is empty");
  }
  double s = 0;
  for (size_t i = 0; i < count_; i++)
  {
    s += container_[i]->getArea();
  }
  return s;
}

medvedev::rectangle_t medvedev::CompositeShape::getFrameRect() const
{
  double maxX = 0;
  double minX = 0;
  double maxY = 0;
  double minY = 0;

  if (count_ == 0)
  {
    throw std::logic_error("Object is empty");
  }
  rectangle_t rectangle = container_[0]->getFrameRect();

  maxX = rectangle.pos.x + rectangle.width / 2;
  minX = rectangle.pos.x - rectangle.width / 2;
  maxY = rectangle.pos.y + rectangle.height / 2;
  minY = rectangle.pos.y - rectangle.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    rectangle = container_[i]->getFrameRect();

    point_t position = rectangle.pos;
    maxX = std::max(position.x + rectangle.width / 2, maxX);
    minX = std::min(position.x - rectangle.width / 2, minX);
    maxY = std::max(position.y + rectangle.height / 2, maxY);
    minY = std::min(position.y - rectangle.height / 2, minY);
  }

  rectangle_t result{maxX - minX, maxY - minY, {(maxX + minX) / 2, (maxY + minY) / 2}};
  return result;
}

void medvedev::CompositeShape::move(double x, double y)
{
  if (count_ == 0)
  {
    throw std::logic_error("Object is empty");
  }
  for (size_t i = 0; i < count_; i++)
  {
    container_[i]->move(x, y);
  }
}

void medvedev::CompositeShape::move(const point_t &center)
{
  if (count_ == 0)
  {
    throw std::logic_error("Object is empty");
  }
  point_t old_center = getCenter();
  double dx = center.x - old_center.x;
  double dy = center.y - old_center.y;
  for (size_t i = 0; i < count_; i++)
  {
    container_[i]->move(dx, dy);
  }
}

void medvedev::CompositeShape::scale(double k)
{
  if (count_ == 0)
  {
    throw std::logic_error("Object is empty");
  }
  if (k <= 0)
  {
    throw std::invalid_argument("Сoefficient must be positive");
  }
  point_t center = getCenter();
  point_t fig_center;
  for (size_t i = 0; i < count_; i++)
  {
    container_[i]->scale(k);
    fig_center = container_[i]->getFrameRect().pos;
    container_[i]->move((fig_center.x - center.x) * (k - 1), (fig_center.y - center.y) * (k - 1));
  }
}

void medvedev::CompositeShape::rotate(double angle)
{
  const double cos = std::cos((2 * M_PI * angle) / 360);
  const double sin = std::sin((2 * M_PI * angle) / 360);
  const point_t compCenter = getFrameRect().pos;
  for (std::size_t i = 0; i < count_; i++)
  {
    const point_t shapeCenter = container_[i]->getFrameRect().pos;
    const double dx = (shapeCenter.x - compCenter.x) * cos - (shapeCenter.y - compCenter.y) * sin;
    const double dy = (shapeCenter.x - compCenter.x) * sin + (shapeCenter.y - compCenter.y) * cos;
    container_[i]->move({compCenter.x + dx, compCenter.y + dy});
    container_[i]->rotate(angle);
  }
}
