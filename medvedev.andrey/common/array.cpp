#include "array.hpp"
#include <stdexcept>
#include <cmath>

medvedev::Array::Array():
  rows_(0),
  columns_(0)
{}

medvedev::Array::Array(const Array &rhs):
  rows_(rhs.rows_),
  columns_(rhs.columns_),
  shapes_(std::make_unique<Shape*[]>(rhs.rows_ * rhs.columns_))
{
  for (size_t i = 0; i < (rows_  * columns_); i++)
  {
    shapes_[i] = rhs.shapes_[i];
  }
}

medvedev::Array::Array(medvedev::Array &&rhs):
  rows_(rhs.rows_),
  columns_(rhs.columns_),
  shapes_(std::move(rhs.shapes_))
{
  rhs.rows_ = 0;
  rhs.columns_ = 0;
}

medvedev::Array &medvedev::Array::operator =(const Array &rhs)
{
  if (this != &rhs)
  {
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
    std::unique_ptr<Shape*[]> temp(std::make_unique<Shape*[]>(rows_ * columns_));
    for (size_t i = 0; i < (rows_ * columns_); i++)
    {
      temp[i] = rhs.shapes_[i];
    }

    shapes_.swap(temp);
  }

  return *this;
}

medvedev::Array &medvedev::Array::operator =(Array &&rhs)
{
  if (this != &rhs)
  {
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
    shapes_ = std::move(rhs.shapes_);
    rhs.rows_ = 0;
    rhs.columns_ = 0;
  }

  return *this;
}

std::unique_ptr<medvedev::Shape*[]> medvedev::Array::operator [](size_t index) const
{
  if (index >= rows_)
  {
    throw std::out_of_range("Index out of range");
  }

  std::unique_ptr<Shape*[]> temp(std::make_unique<Shape*[]>(columns_));
  for (size_t i = 0; i < getRowSize(index); i++)
  {
    temp[i] = shapes_[index * columns_ + i];
  }

  return temp;
}

bool medvedev::Array::operator==(const Array &shapesArray) const
{
  if ((rows_ != shapesArray.rows_) || (columns_ != shapesArray.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (shapes_[i] != shapesArray.shapes_[i])
    {
      return false;
    }
  }

  return true;
}

bool medvedev::Array::operator!=(const Array &shapesArray) const
{
  return !(*this == shapesArray);
}

void medvedev::Array::add(Shape* shape, size_t row, size_t column)
{
  size_t newRow = (row == rows_) ? (rows_ + 1) : (rows_);
  size_t newColumn = (column == columns_) ? (columns_ + 1) : (columns_);
  
  std::unique_ptr<Shape*[]> temp(std::make_unique<Shape*[]>(newColumn * newRow));

  for (size_t i = 0; i < rows_; i++)
  {
    for (size_t j = 0; j < columns_; j++)
    {
      temp[i * newColumn + j] = shapes_[i * columns_ + j];
    }
  }
  temp[newColumn * row + column] = shape;
  shapes_ = std::move(temp);
  rows_ = newRow;
  columns_ = newColumn;
}

std::size_t medvedev::Array::getRows() const
{
  return rows_;
}

std::size_t medvedev::Array::getColumns() const
{
  return columns_;
}

std::size_t medvedev::Array::getRowSize(size_t row) const
{
  if (row >= rows_)
  {
    return 0;
  }

  for (size_t i = 0; i < columns_; i++)
  {
    if (shapes_[row * columns_ + i] == nullptr)
    {
      return i;
    }
  }

  return columns_;
}
