#include "circle.hpp"
#include <iostream>
#include <cmath>

medvedev::Circle::Circle(const point_t &center, double radius) :
  center_(center),
  radius_(radius)
{
  if (radius_ <= 0.0)
  {
    throw std::invalid_argument("Invalid radius");
  }
}

double medvedev::Circle::getArea() const
{
  return M_PI * (radius_ * radius_);
}

medvedev::rectangle_t medvedev::Circle::getFrameRect() const
{
  const double diameter = radius_ * 2;
  return rectangle_t{diameter, diameter, center_};
}

double medvedev::Circle::getRadius() const
{
  return radius_;
}

void medvedev::Circle::move(const point_t &point)
{
  center_.x = point.x;
  center_.y = point.y;
}

void medvedev::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void medvedev::Circle::scale(double n)
{
  if (n<=0)
    throw std::invalid_argument("Invalid point");
  radius_ = fabs(radius_ * n);
}

void medvedev::Circle::rotate(const double)
{
}
