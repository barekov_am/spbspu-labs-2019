#include "triangle.hpp"
#include <stdexcept>
#include <cmath>

double distance(medvedev::point_t a, medvedev::point_t b);

medvedev::Triangle::Triangle(const point_t &a,const point_t &b,const point_t &c) :
  a_(a),
  b_(b),
  c_(c)
{
  if (((distance(a_, b_) + distance(b_, c_)) <= distance(c_, a_))
      || (distance(a_, b_) >= (distance(b_, c_) + distance(c_, a_)))
      || ((distance(a_, b_) + distance(c_, a_)) <= distance(b_, c_)))
  {
    throw std::invalid_argument("Invalid point");
  }
}

double distance(medvedev::point_t a, medvedev::point_t b)
{
  return std::sqrt(std::pow(b.x - a.x, 2) + std::pow(b.y - a.y, 2));
}

double medvedev::Triangle::getArea() const
{
  double s = (distance(a_, b_) + distance(b_, c_) + distance(c_, a_)) / 2;

  return sqrt(s * (s - distance(a_, b_)) * (s - distance(b_, c_)) * (s - distance(c_, a_)));
}

medvedev::point_t medvedev::Triangle::getCenter() const
{
  return point_t{(a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3};
}

medvedev::rectangle_t medvedev::Triangle::getFrameRect() const
{
  double minX = std::min(std::min(a_.x, b_.x), c_.x);
  double minY = std::min(std::min(a_.y, b_.y), c_.y);

  double maxX = std::max(std::max(a_.x, b_.x), c_.x);
  double maxY = std::max(std::max(a_.y, b_.y), c_.y);

  rectangle_t result{0, 0, {0, 0}};
  result.pos.x = (minX + maxX) / 2;
  result.pos.y = (minY + maxY) / 2;
  result.width = maxX - minX;
  result.height = maxY - minY;

  return result;
}

void medvedev::Triangle::move(const point_t &point)
{
  if (std::isnan(point.x) || std::isnan(point.y))
  {
    throw std::invalid_argument("Invalid point");
  }
  point_t center = getCenter();
  move(point.x - center.x, point.y - center.y);
}

void medvedev::Triangle::move(double dx, double dy)
{
  if (std::isnan(dx) || std::isnan(dy))
  {
    throw std::invalid_argument("Invalid point");
  }
  a_.x += dx;
  a_.y += dy;

  b_.x += dx;
  b_.y += dy;

  c_.x += dx;
  c_.y += dy;
}

void medvedev::Triangle::scale(double n)
{

  if (n<=0)
  {
    throw std::invalid_argument("Invalid point");
  }

  point_t center = getCenter();
  a_.x = (a_.x - center.x) * n + center.x;
  a_.y = (a_.y - center.y) * n + center.y;

  b_.x = (b_.x - center.x) * n + center.x;
  b_.y = (b_.y - center.y) * n + center.y;

  c_.x = (c_.x - center.x) * n + center.x;
  c_.y = (c_.y - center.y) * n + center.y;
}

void medvedev::Triangle::rotate(double angle)
{
  const double cos = std::cos((2 * M_PI * angle) / 360);
  const double sin = std::sin((2 * M_PI * angle) / 360);
  const point_t center = getCenter();
  a_.x = center.x + (a_.x - center.x) * cos - (a_.y - center.y) * sin;
  a_.y = center.y + (a_.x - center.x) * sin + (a_.y - center.y) * cos;

  b_.x = center.x + (b_.x - center.x) * cos - (b_.y - center.y) * sin;
  b_.y = center.y + (b_.x - center.x) * sin + (b_.y - center.y) * cos;

  c_.x = center.x + (c_.x - center.x) * cos - (c_.y - center.y) * sin;
  c_.y = center.y + (c_.x - center.x) * sin + (c_.y - center.y) * cos;
}
