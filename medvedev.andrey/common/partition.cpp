#include "partition.hpp"
#include <cmath>

bool medvedev::checkIntersect(medvedev::rectangle_t shape1, medvedev::rectangle_t shape2)
{
  const double distanceBetweenCentersX = fabs(shape1.pos.x - shape2.pos.x);
  const double distanceBetweenCentersY = fabs(shape1.pos.y - shape2.pos.y);

  const double lengthX = (shape1.width + shape2.width) / 2;
  const double lengthY = (shape1.height + shape2.height) / 2;

  const bool firstCondition = (distanceBetweenCentersX < lengthX);
  const bool secondCondition = (distanceBetweenCentersY < lengthY);

  return firstCondition && secondCondition;

}

medvedev::Array medvedev::division(std::unique_ptr<Shape*[]> shapes, size_t size)
{
  Array array;

  for (size_t i = 0; i < size; i++)
  {
    size_t rows = 0;
    size_t columns = 0;
    size_t needfulColumns = 0;
    for (size_t j = 0; j < array.getRows(); j++)
    {
      needfulColumns = array.getRowSize(j);
      for (size_t k = 0; k < needfulColumns; k++)
      {
        if (checkIntersect(shapes[i]->getFrameRect(), array[j][k]->getFrameRect()))
        {
          rows++;
          break;
        }
        if (k == needfulColumns - 1)
        {
          rows = j;
          columns = needfulColumns;
        }
      }
      if (rows == j)
      {
        break;
      }
    }
    array.add(shapes[i], rows, columns);
  }
  return array;
}

medvedev::Array medvedev::division(medvedev::CompositeShape &compositeShape)
{
  std::unique_ptr<Shape*[]> temp(std::make_unique<Shape*[]>(compositeShape.getCount()));
  for (size_t i = 0; i < compositeShape.getCount(); i++)
  {
    temp[i] = compositeShape[i];
  }
  return division(std::move(temp), compositeShape.getCount());
}
