#include <boost/test/auto_unit_test.hpp>
#include "polygon.hpp"

const double TOLERANCE = 0.00000001;

BOOST_AUTO_TEST_SUITE(Polygon_tests)

BOOST_AUTO_TEST_CASE(test_Polygon_few_points)
{
  const int sides = 2;
  const medvedev::point_t vertices[sides] = {{0, 1}, {2, 4}};
  BOOST_CHECK_THROW(medvedev::Polygon polygon(sides, vertices), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(test_Polygon_not_convex_polygon)
{
  const int sides = 4;
  const medvedev::point_t vertices[sides] = {{0, 1}, {2, 4}, {0, -1}, {-2, 4}};
  BOOST_CHECK_THROW(medvedev::Polygon polygon(sides, vertices), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(test_Polygon_center)
{
  const int sides = 6;
  const medvedev::point_t vertices[sides] = {{0, 5}, {2, 5}, {5, 1}, {2, -3}, {0, -3}, {-3, 1}};
  medvedev::Polygon polygon{sides, vertices};//polygon with center(1,1)
  medvedev::point_t center = polygon.getCenter();
  BOOST_CHECK_CLOSE(center.x, 1, TOLERANCE);
  BOOST_CHECK_CLOSE(center.y, 1, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_Polygon_area)
{
  const int sides = 6;
  const medvedev::point_t vertices[sides] = {{0, 5}, {2, 5}, {5, 1}, {2, -3}, {0, -3}, {-3, 1}};
  medvedev::Polygon polygon{sides, vertices};//polygon with S = 40
  BOOST_CHECK_CLOSE(polygon.getArea(), 40, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_Polygon_relative_move)
{
  const int sides = 6;
  const medvedev::point_t vertices[sides] = {{0, 5}, {2, 5}, {5, 1}, {2, -3}, {0, -3}, {-3, 1}};
  medvedev::Polygon polygon{sides, vertices};
  polygon.move(2 , -1);
  medvedev::point_t center = polygon.getCenter();
  BOOST_CHECK_CLOSE(center.x, 3, TOLERANCE);
  BOOST_CHECK_CLOSE(center.y, 0, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_Polygon_absolute_move)
{
  const int sides = 6;
  const medvedev::point_t vertices[sides] = {{0, 5}, {2, 5}, {5, 1}, {2, -3}, {0, -3}, {-3, 1}};
  medvedev::Polygon polygon{sides, vertices};
  polygon.move({0, 0});
  medvedev::point_t center = polygon.getCenter();
  BOOST_CHECK_CLOSE(center.x, 0, TOLERANCE);
  BOOST_CHECK_CLOSE(center.y, 0, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(test_Polygon_scale)
{
  const int sides = 6;
  const medvedev::point_t vertices[sides] = {{0, 5}, {2, 5}, {5, 1}, {2, -3}, {0, -3}, {-3, 1}};
  medvedev::Polygon polygon{sides, vertices};
  double S = polygon.getArea();
  polygon.scale(5);
  BOOST_CHECK_CLOSE(polygon.getArea(), S * 5 * 5, TOLERANCE);
}

BOOST_AUTO_TEST_SUITE_END()
