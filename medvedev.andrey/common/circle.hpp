#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace medvedev
{
class Circle : public Shape
{
public:
  Circle(const point_t &center, double radius);
  double getArea() const override;
  double getRadius() const;
  rectangle_t getFrameRect() const override;
  void move(const point_t &point) override;
  void move(double dx, double dy) override;
  void scale(double n) override;
  void rotate(const double) override;
private:
  point_t center_;
  double radius_;
};
}

#endif //CIRCLE_HPP
