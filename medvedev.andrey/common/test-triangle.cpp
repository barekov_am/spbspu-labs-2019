#ifndef BOOST_TEST_TRIANGLE
#define BOOST_TEST_TRIANGLE

#include <boost/test/auto_unit_test.hpp>
#include "triangle.hpp"
#include <stdexcept>
#include <cmath>

const double TOLERANCE = 0.001;

BOOST_AUTO_TEST_SUITE(Triangle_tests)

BOOST_AUTO_TEST_CASE(area_after_move_on)
{
  medvedev::Triangle triangle({0, 0}, {5, 0}, {0, 5});
  const double area = triangle.getArea();
  triangle.move(2, 3);
  BOOST_CHECK_CLOSE(triangle.getArea(), area, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(area_after_move_to)
{
  medvedev::Triangle triangle({0, 0}, {5, 0}, {0, 5});
  const double area = triangle.getArea();
  triangle.move({5, 5});
  BOOST_CHECK_CLOSE(triangle.getArea(), area, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(frame_rect_after_move_on)
{
  medvedev::Triangle triangle({0, 0}, {5, 0}, {0, 5});
  const double width = triangle.getFrameRect().width;
  const double height = triangle.getFrameRect().height;
  triangle.move(2, 3);
  BOOST_CHECK_CLOSE(triangle.getFrameRect().width, width, TOLERANCE);
  BOOST_CHECK_CLOSE(triangle.getFrameRect().height, height, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(frame_rect_after_move_to)
{
  medvedev::Triangle triangle({0, 0}, {5, 0}, {0, 5});
  const double width = triangle.getFrameRect().width;
  const double height = triangle.getFrameRect().height;
  triangle.move({2, 3});
  BOOST_CHECK_CLOSE(triangle.getFrameRect().width, width, TOLERANCE);
  BOOST_CHECK_CLOSE(triangle.getFrameRect().height, height, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(area_scale_increase)
{
  medvedev::Triangle triangle({0, 0}, {5, 0}, {0, 5});
  double area = triangle.getArea();
  triangle.scale(2);
  BOOST_CHECK_CLOSE(triangle.getArea(), area * 4, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(area_scale_decrease)
{
  medvedev::Triangle triangle({0, 0}, {5, 0}, {0, 5});
  double area = triangle.getArea();
  triangle.scale(0.5);
  BOOST_CHECK_CLOSE(triangle.getArea(), area / 4, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(scale_invalid_argument_test)
{
  medvedev::Triangle triangle({0, 0}, {5, 0}, {0, 5});
  BOOST_CHECK_THROW(triangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(move_to_invalid_argument_test)
{
  medvedev::Triangle triangle({0, 0}, {5, 0}, {0, 5});
  BOOST_CHECK_THROW(triangle.move({std::nan(""), std::nan("")}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(move_invalid_argument_test)
{
  medvedev::Triangle triangle({0, 0}, {5, 0}, {0, 5});
  BOOST_CHECK_THROW(triangle.move(std::nan(""), std::nan("")), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(constructor_invalid_argument_test)
{
  BOOST_CHECK_THROW(medvedev::Triangle({0, 0}, {10, 0}, {-10, 0}), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

#endif
