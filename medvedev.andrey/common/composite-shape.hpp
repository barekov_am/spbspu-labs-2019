#ifndef A3_COMPOSITE_SHAPE_HPP
#define A3_COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include <cstdlib>
#include <boost/smart_ptr.hpp>

namespace medvedev
{
class CompositeShape : public Shape
{
public:
  CompositeShape();
  CompositeShape(const CompositeShape &c_shape);
  CompositeShape(CompositeShape &&c_shape);
  ~CompositeShape() = default;
  CompositeShape &operator=(const CompositeShape &c_shape);
  CompositeShape &operator=(CompositeShape &&c_shape);
  Shape *operator[](size_t index) const;
  size_t getCount() const;
  void add(Shape *p_figure);
  void remove(size_t index);
  point_t getCenter() const;
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(double x, double y) override;
  void move(const point_t &center) override;
  void scale(double k) override;
  void rotate(double angle) override;
private:
  size_t count_;
  boost::shared_array<Shape *> container_;
};
}

#endif //A3_COMPOSITE_SHAPE_HPP
