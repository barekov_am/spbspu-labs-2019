#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "shape.hpp"

namespace medvedev
{
class Polygon : public Shape
{
public:
  Polygon(const Polygon &other);
  Polygon(Polygon &&other) noexcept;
  Polygon(const int sides, const medvedev::point_t *points);
  ~Polygon() override;
  Polygon &operator=(const Polygon &other);
  Polygon &operator=(Polygon &&other) noexcept;
  medvedev::point_t getCenter() const;
  bool isConvex() const;
  double getArea() const override;
  medvedev::rectangle_t getFrameRect() const override;
  void move(const medvedev::point_t &point) override;
  void move(const double x, const double y) override;
  void scale(double k) override;
  void rotate(double angle) override;
private:
  int sides_;
  medvedev::point_t *vertices_;
};
}
#endif
