#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void printRect(const Shape &shape)
{
  rectangle_t rectangle1 = shape.getFrameRect();
  std::cout  << "width = " << rectangle1.width << ", height = " << rectangle1.height;
  std::cout << " center = " << "(" << rectangle1.pos.x << "," <<rectangle1.pos.y << ")\n" ;
}

void printInfo(const Shape &shape)
{
  std::cout << "Area =" << shape.getArea() << std::endl;
  const rectangle_t frame = shape.getFrameRect();
  std::cout << "Object frame :" << std::endl;
  std::cout << "middle : " << frame.pos.x << ", " << frame.pos.y << std::endl;
  std::cout << "width :" << frame.width << std::endl;
  std::cout << "height " << frame.height << std::endl;
  std::cout << '\n';
}


void showWork(Shape &shape)
{
  std::cout << "Rectangle frame: ";
  printRect(shape);
  std::cout << "Area = " << shape.getArea() << std::endl;
  shape.move({10, 10});
  std::cout << "After moving to a next point: " << std::endl;
  printRect(shape);
  shape.move(2, 2);
  std::cout << "After axial movement: " << std::endl;
  printRect(shape);
  std::cout << '\n';
}

void testCircle()
{
  std::cout << "Circle test: " << std::endl;
  point_t center {0, 0};
  Circle circle(center,3);
  showWork(circle);
}

void testRectangle()
{
  std::cout << "Rectangle test: " << std::endl;
  Rectangle rectangle(6, 9, {3, 4});
  showWork(rectangle);
}

void testTriangle()
{
  std::cout << "Triangle test: " << std::endl;
  Triangle triangle({-3, -3}, {-3, -6}, {-7, -3});
  showWork(triangle);
}

int main()
{
  testCircle();
  testRectangle();
  testTriangle();

  std::cout << "Polygon:" << std::endl;
  const int sides = 6;
  const point_t vertices[sides] = {{2.0, 3}, {4.3, 3}, {5, 4.75}, {4, 7.4}, {1.9, 7}, {-2.25, 4}};
  Polygon polygon(sides, vertices);
  printInfo(polygon);
  polygon.move({13.2, 10.1});
  printInfo(polygon);

  Polygon polygon1(polygon);
  printInfo(polygon1);
  polygon1.move(2.1, 5.5);

  Polygon polygon2(std::move(polygon1));
  printInfo(polygon2);

  polygon1 = polygon2;
  printInfo(polygon1);

  polygon2 = (std::move(polygon1));
  printInfo(polygon2);


  return 0;
}
