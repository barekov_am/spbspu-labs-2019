#include "circle.hpp"
#include <iostream>
#include <cmath>

Circle::Circle(const point_t &center, double radius):
  center_(center),
  radius_(radius)
{
  if (radius_ <= 0.0)
  {
    throw std::invalid_argument("Invalid radius");
  }
}

double Circle::getArea() const
{
  return M_PI * (radius_ * radius_);
}

rectangle_t Circle::getFrameRect() const
{
  const double diameter = radius_ * 2;
  return rectangle_t {diameter, diameter, center_};
}

void Circle::move(const point_t &point)
{
  center_.x = point.x;
  center_.y = point.y;
}

void Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}
