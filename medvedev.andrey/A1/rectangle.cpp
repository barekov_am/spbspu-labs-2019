#include "rectangle.hpp"
#include <cmath>
#include <stdexcept>

Rectangle::Rectangle(double width, double height,const point_t &center):
  width_(width),
  height_(height),
  center_(center)
{
  if ((width_ <= 0.0) && (height_ <= 0.0))
  {
    throw std::invalid_argument("Invalid width or height");
  }
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return {width_, height_, center_};
}

void Rectangle::move(const point_t &point)
{
  center_.x = point.x;
  center_.y = point.y;
}

void Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}
