#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

class Triangle : public Shape
{

public:
  Triangle(const point_t &a,const point_t &b,const point_t &c);
  point_t getCenter() const;
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &point) override;
  void move(double dx, double dy) override;
private:
  point_t a_;
  point_t b_;
  point_t c_;
};

#endif //TRIANGLE_HPP
