#include "triangle.hpp"
#include <stdexcept>
#include <cmath>

double distance(point_t a, point_t b);

Triangle::Triangle(const point_t &a,const point_t &b,const point_t &c) :
  a_(a),
  b_(b),
  c_(c)
{
  if (((distance(a_, b_) + distance(b_, c_)) <= distance(c_, a_))
      ||(distance(a_, b_) >= (distance(b_, c_) + distance(c_, a_)))
      ||((distance(a_, b_) + distance(c_, a_)) <= distance(b_, c_)))
  {
    throw std::invalid_argument("Invalid point");
  }
}

double distance(point_t a, point_t b)
{
  return std::sqrt(std::pow(b.x - a.x, 2) + std::pow(b.y - a.y, 2));
}

double Triangle::getArea() const
{
  double s = (distance(a_, b_) + distance(b_, c_) + distance(c_, a_)) / 2;

  return sqrt(s * (s - distance(a_, b_)) * (s - distance(b_, c_)) * (s - distance(c_, a_)));
}

point_t Triangle::getCenter() const
{
  return point_t {(a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3};
}

rectangle_t Triangle::getFrameRect() const
{
  double minX = std::min(std::min(a_.x, b_.x), c_.x);
  double minY = std::min(std::min(a_.y, b_.y), c_.y);

  double maxX = std::max(std::max(a_.x, b_.x), c_.x);
  double maxY = std::max(std::max(a_.y, b_.y), c_.y);

  rectangle_t result {0, 0, {0, 0}};
  result.pos.x = (minX + maxX) / 2;
  result.pos.y = (minY + maxY) / 2;
  result.width = maxX - minX;
  result.height = maxY - minY;

  return result;
}

void Triangle::move(const point_t &point)
{
  if (std::isnan(point.x) || std::isnan(point.y))
  {
    throw std::invalid_argument("moveTo must not be NaN");
  }
  point_t center = getCenter();
  move(point.x - center.x, point.y - center.y);
}

void Triangle::move(double dx, double dy)
{
  if (std::isnan(dx) || std::isnan(dy))
    throw std::invalid_argument("move must not be NaN");
  a_.x += dx;
  a_.y += dy;

  b_.x += dx;
  b_.y += dy;

  c_.x += dx;
  c_.y += dy;
}
