#include "polygon.hpp"
#include <iostream>
#include <ctime>
#include <cmath>

Polygon::Polygon(const Polygon &other):
    sides_(other.sides_),
    vertices_(new point_t[sides_])
{
  for (int i = 0; i < sides_; i++)
  {
    vertices_[i] = other.vertices_[i];
  }
}

Polygon::Polygon(Polygon &&other) noexcept:
    sides_(other.sides_),
    vertices_(other.vertices_)
{
  other.sides_ = 0;
  other.vertices_ = nullptr;
}

Polygon::Polygon(const int sides, const point_t *points):
    sides_(sides),
    vertices_(new point_t[sides])
{
  if (points == nullptr)
  {
    delete[] vertices_;
    throw std::invalid_argument("Pointer cannot be nullptr");
  }
  
  if (sides_ < 3)
  {
    delete [] vertices_;
    throw std::invalid_argument("Sides number must be more than 2");
  }

  for (int i = 0; i < sides_; i++)
  {
    vertices_[i] = points[i];
  }

  if (getArea() == 0) {
    throw std::invalid_argument("The polygon's area equal 0.");
  }

  if (!isConvex())
  {
    delete[] vertices_;
    throw std::invalid_argument("Polygon is concave");
  }
}

Polygon::~Polygon()
{
  delete[] vertices_;
}

Polygon &Polygon::operator=(Polygon &&other) noexcept
{
  if (this != &other)
  {
    sides_ = other.sides_;
    other.sides_ = 0;
    delete[] vertices_;
    vertices_ = other.vertices_;
    other.vertices_ = nullptr;
  }
  return *this;
}

Polygon &Polygon::operator=(const Polygon &other)
{
  if (this != &other)
  {
    sides_ = other.sides_;
    delete[] vertices_;
    vertices_ = new point_t[sides_];
    for (int i = 0; i < sides_; i++)
    {
      vertices_[i] = other.vertices_[i];
    }
  }
  return  *this;
}

point_t Polygon::getCenter() const
{
  point_t centermem = {0.0, 0.0};

  for (int i = 0; i < sides_; i++)
  {
    centermem.x += vertices_[i].x;
    centermem.y += vertices_[i].y;
  }
  return {centermem.x / sides_, centermem.y / sides_};
}

bool Polygon::isConvex() const
{
  int sign = 0;
  for (int i = 0; i < sides_; i++) {
    const int j = (i + 1) % sides_;
    const int k = (i + 2) % sides_;
    const double determinant = (vertices_[j].x - vertices_[i].x)
                             * (vertices_[k].y - vertices_[j].y)
                             - (vertices_[k].x - vertices_[j].x)
                             * (vertices_[j].y - vertices_[i].y);
    if (determinant != 0) {
      if (sign == 0) {
        sign = determinant > 0 ? 1 : -1;
      } else if ((sign * determinant) < 0) {
        return false;
      }
    }
  }
  return sign != 0;
}

double Polygon::getArea() const
{
  double sum = 0;

  for (int i = 0; i < sides_ - 1; i++)
  {
    sum += vertices_[i].x * vertices_[i + 1].y;
    sum -= vertices_[i + 1].x * vertices_[i].y;
  }
  sum += vertices_[sides_ - 1].x * vertices_[0].y;
  sum -= vertices_[0].x * vertices_[sides_ - 1].y;

  return fabs(sum) / 2;
}

rectangle_t Polygon::getFrameRect() const
{
  double min_y = vertices_[0].y;
  double max_y = vertices_[0].y;
  double min_x = vertices_[0].x;
  double max_x = vertices_[0].x;
  for (int i = 1; i < sides_; i++)
  {
    min_x = std::min(min_x, vertices_[i].x);
    max_x = std::max(max_x, vertices_[i].x);
    min_y = std::min(min_y, vertices_[i].y);
    max_y = std::max(max_y, vertices_[i].y);
  }
  return {max_x - min_x, max_y - min_y, {min_x + (max_x - min_x) / 2,min_y + (max_y - min_y) / 2}};
}

void Polygon::move(const point_t &point)
{
  const point_t center_ = getCenter();
  move(point.x - center_.x, point.y - center_.y);
}

void Polygon::move(double move_x, double move_y)
{
  for (int i = 0; i < sides_; i++)
  {
    vertices_[i].x += move_x;
    vertices_[i].y += move_y;
  }
}
