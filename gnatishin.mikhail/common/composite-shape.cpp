#include "composite-shape.hpp"

#include <iostream>
#include <memory>
#include <utility>
#include <cmath>
#include <math.h>

gnatishin::CompositeShape::CompositeShape() :
  count_(0)
{ }

gnatishin::CompositeShape::CompositeShape(const CompositeShape &source) :
  count_(source.count_),
  arrayOfShapes_(std::make_unique<gnatishin::Shape::pointer[]>(source.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i] = source.arrayOfShapes_[i];
  }
}

gnatishin::CompositeShape::CompositeShape(CompositeShape &&source) :
  count_(source.count_),
  arrayOfShapes_(std::move(source.arrayOfShapes_))
{ }

gnatishin::CompositeShape::CompositeShape(const Shape::pointer &shape):
  CompositeShape()
{
  add(shape);
}

gnatishin::CompositeShape &gnatishin::CompositeShape::operator =(const CompositeShape &rhs)
{
  if (this != &rhs)
  {
    arrayOfShapes_ = std::make_unique<gnatishin::Shape::pointer[]>(rhs.count_);
    count_ = rhs.count_;

    for (size_t i = 0; i < count_; i++)
    {
      arrayOfShapes_[i] = rhs.arrayOfShapes_[i];
    }
  }
  return *this;
}

gnatishin::CompositeShape &gnatishin::CompositeShape::operator =(CompositeShape &&rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    arrayOfShapes_ = std::move(rhs.arrayOfShapes_);
    rhs.count_ = 0;
    rhs.arrayOfShapes_ = nullptr;
  }

  return *this;
}

gnatishin::Shape::pointer gnatishin::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range of array");
  }

  return arrayOfShapes_[index];
}

double gnatishin::CompositeShape::getArea() const
{
  double globalArea = 0;
  for (size_t i = 0; i < count_; i++)
  {
    globalArea += arrayOfShapes_[i]->getArea();
  }

  return globalArea;
}

gnatishin::rectangle_t gnatishin::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    return {0, 0, {0, 0}};
  }

  rectangle_t tempFrameRect = arrayOfShapes_[0]->getFrameRect();

  double rightX = tempFrameRect.pos.x + tempFrameRect.width / 2;
  double leftX = tempFrameRect.pos.x - tempFrameRect.width / 2;
  double upperY = tempFrameRect.pos.y + tempFrameRect.height / 2;
  double lowerY = tempFrameRect.pos.y - tempFrameRect.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    tempFrameRect = arrayOfShapes_[i]->getFrameRect();

    rightX = std::max((tempFrameRect.pos.x + tempFrameRect.width / 2), rightX);
    leftX = std::min((tempFrameRect.pos.x - tempFrameRect.width / 2), leftX);
    upperY = std::max((tempFrameRect.pos.y + tempFrameRect.height / 2), upperY);
    lowerY = std::min((tempFrameRect.pos.y - tempFrameRect.height / 2), lowerY);
  }

  return {rightX - leftX, upperY - lowerY, {(rightX + leftX) / 2, (upperY + lowerY) / 2}};
}

gnatishin::point_t gnatishin::CompositeShape::getPosition() const
{
  return getFrameRect().pos;
}

void gnatishin::CompositeShape::move(const point_t &center)
{
  move(center.x - getPosition().x, center.y - getPosition().y);
}

void gnatishin::CompositeShape::move(double dx, double dy)
{
  if (count_ != 0)
  {
    for (size_t i = 0; i < count_; i++)
    {
      arrayOfShapes_[i]->move(dx, dy);
    }
  }
}

void gnatishin::CompositeShape::scale(double scaleFactor)
{
  if (scaleFactor <= 0.0)
  {
    throw std::invalid_argument("Scale factor of composite shape must be more than zero");
  }

  point_t center = getPosition();
  for (size_t i = 0; i < count_; i++)
  {
    point_t currentPosition = arrayOfShapes_[i]->getPosition();
    double dx = currentPosition.x - center.x;
    double dy = currentPosition.y - center.y;
    arrayOfShapes_[i]->move({center.x + dx * scaleFactor, center.y + dy * scaleFactor});
    arrayOfShapes_[i]->scale(scaleFactor);
  }
}

void gnatishin::CompositeShape::rotate(double angle)
{
  const double cos = std::cos((M_PI * angle) / 180);
  const double sin = std::sin((M_PI * angle) / 180);

  const point_t compCentre = getFrameRect().pos;

  for (std::size_t i = 0; i < count_; i++)
  {
    const point_t shapeCentre = arrayOfShapes_[i]->getFrameRect().pos;
    const double dx = (shapeCentre.x - compCentre.x) * cos - (shapeCentre.y - compCentre.y) * sin;
    const double dy = (shapeCentre.x - compCentre.x) * sin + (shapeCentre.y - compCentre.y) * cos;

    arrayOfShapes_[i]->move({ compCentre.x + dx, compCentre.y + dy });
    arrayOfShapes_[i]->rotate(angle);
  }
}

size_t gnatishin::CompositeShape::getSize() const
{
  return(count_);
}

void gnatishin::CompositeShape::add(const Shape::pointer &source)
{
  Shape::array shapes = std::make_unique<Shape::pointer[]>(count_ + 1);

  for (size_t i = 0; i < count_; i++)
  {
    shapes[i] = arrayOfShapes_[i];
  }

  shapes[count_] = source;
  count_++;
  arrayOfShapes_ = std::move(shapes);
}

void gnatishin::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::logic_error("deleted shape number is > than quantity of shapes in composite shape");
  }

  for (size_t i = index; i < (count_ - 1); i++)
  {
    arrayOfShapes_[i] = arrayOfShapes_[i+1];
  }

  arrayOfShapes_[--count_] = nullptr;
}
