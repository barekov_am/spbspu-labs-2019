#ifndef COMPOSITE_SHAPE_OF_MG
#define COMPOSITE_SHAPE_OF_MG

#include <memory>

#include "shape.hpp"
#include "base-types.hpp"

namespace gnatishin
{
  class CompositeShape : public Shape
  {
    public:
      CompositeShape();
      CompositeShape(const CompositeShape &);
      CompositeShape(CompositeShape &&);
      CompositeShape(const Shape::pointer &shape);
      ~CompositeShape() override = default;

      CompositeShape &operator =(const CompositeShape &rhs);
      CompositeShape &operator =(CompositeShape &&rhs);
      Shape::pointer operator [](size_t index) const;

      double getArea() const override;
      rectangle_t getFrameRect() const override;
      point_t getPosition() const override;
      void move(const point_t &center) override;
      void move(double dx, double dy) override;
      void scale(double scaleFactor) override;
      void rotate(double angle) override;
      size_t getSize() const;

      void add(const Shape::pointer &shape);
      void remove(size_t index);
    private:
      size_t count_;
      Shape::array arrayOfShapes_;
  };
}

#endif
