#include "partition.hpp"
#include "base-types.hpp"

gnatishin::Matrix gnatishin::part(const gnatishin::CompositeShape &composition)
{
  Matrix tmpMatrix;
  const std::size_t size = composition.getSize();

  for (std::size_t i = 0; i < size; i++)
  {
    std::size_t level = 0;
    for (std::size_t j = 0; j < tmpMatrix.getRows(); j++)
    {
      bool intersect = false;
      for (std::size_t k = 0; k < tmpMatrix.getLevelSize(j); k++)
      {
        if (gnatishin::isIntersected(composition[i]->getFrameRect(), tmpMatrix[j][k]->getFrameRect()))
        {
          level++;
          intersect = true;
          break;
        }
      }
      if (!intersect)
      {
        break;
      }
    }
    tmpMatrix.add(composition[i], level);
  }

  return tmpMatrix;
}
