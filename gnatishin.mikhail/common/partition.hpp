#ifndef PARTITION_OF_MG
#define PARTITION_OF_MG
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace gnatishin
{
  gnatishin::Matrix part(const gnatishin::CompositeShape &composition);
}

#endif
