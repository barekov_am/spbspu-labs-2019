#include <stdexcept>
#include <utility>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

const double CalcError = 0.01;

BOOST_AUTO_TEST_SUITE(compositeShapeTestSuite)

BOOST_AUTO_TEST_CASE(invariabilityOfCompositeShapeAfterMoving)
{
  gnatishin::Shape::pointer circle_ = std::make_shared<gnatishin::Circle>(gnatishin::point_t {2.2, 1.0}, 3.0);
  gnatishin::Shape::pointer rectangle_ = std::make_shared<gnatishin::Rectangle>(gnatishin::point_t {9.0, 12.0}, 6.0, 5.5);
  gnatishin::CompositeShape testCompShape(circle_);
  testCompShape.add(rectangle_);
  const gnatishin::rectangle_t frameBefore = testCompShape.getFrameRect();
  const double areaBefore = testCompShape.getArea();

  testCompShape.move({4.0, -5.0});
  gnatishin::rectangle_t frameNow = testCompShape.getFrameRect();
  double areaNow = testCompShape.getArea();
  BOOST_CHECK_CLOSE(frameBefore.width, frameNow.width, CalcError);
  BOOST_CHECK_CLOSE(frameBefore.height, frameNow.height, CalcError);
  BOOST_CHECK_CLOSE(areaBefore, areaNow, CalcError);

  testCompShape.move(-2.0, 8.6);
  frameNow = testCompShape.getFrameRect();
  areaNow = testCompShape.getArea();
  BOOST_CHECK_CLOSE(frameBefore.width, frameNow.width, CalcError);
  BOOST_CHECK_CLOSE(frameBefore.height, frameNow.height, CalcError);
  BOOST_CHECK_CLOSE(areaBefore, areaNow, CalcError);
}

BOOST_AUTO_TEST_CASE(changingAfterScalingCompositeShape)
{
  gnatishin::Shape::pointer circle_ = std::make_shared<gnatishin::Circle>(gnatishin::point_t {-3.0, 4.0}, 2.0);
  gnatishin::Shape::pointer rectangle_ = std::make_shared<gnatishin::Rectangle>(gnatishin::point_t {5.0, -8.0}, 2.0, 3.5);
  gnatishin::CompositeShape testCompShape(circle_);
  testCompShape.add(rectangle_);
  double areaBefore = testCompShape.getArea();

  double scaleFactor = 1.5;
  testCompShape.scale(scaleFactor);
  double areaNow = testCompShape.getArea();
  BOOST_CHECK_CLOSE(areaBefore * scaleFactor * scaleFactor, areaNow, CalcError);
  areaBefore = testCompShape.getArea();

  scaleFactor = 0.25;
  testCompShape.scale(scaleFactor);
  areaNow = testCompShape.getArea();
  BOOST_CHECK_CLOSE(areaBefore * scaleFactor * scaleFactor, areaNow, CalcError);
}

BOOST_AUTO_TEST_CASE(copyAndMove)
{
  gnatishin::Shape::pointer circle_ = std::make_shared<gnatishin::Circle>(gnatishin::point_t {2.2, 1.0}, 3.0);
  gnatishin::CompositeShape testComposite;
  testComposite.add(circle_);

  BOOST_CHECK_NO_THROW(gnatishin::CompositeShape testComposite2(testComposite));
  BOOST_CHECK_NO_THROW(gnatishin::CompositeShape testComposite2(std::move(testComposite)));

  gnatishin::CompositeShape testComposite2;
  testComposite2.add(circle_);

  gnatishin::CompositeShape testComposite3;

  BOOST_CHECK_NO_THROW(testComposite3 = testComposite2);
  BOOST_CHECK_NO_THROW(testComposite3 = std::move(testComposite2));

  gnatishin::CompositeShape testComposite4;
  testComposite4.add(circle_);
}

BOOST_AUTO_TEST_CASE(exceptionThrow)
{
  gnatishin::Shape::pointer circle_ = std::make_shared<gnatishin::Circle>(gnatishin::point_t {-3.0, 4.0}, 2.0);
  gnatishin::Shape::pointer rectangle_ = std::make_shared<gnatishin::Rectangle>(gnatishin::point_t {5.0, -8.0}, 2.0, 3.5);
  gnatishin::CompositeShape testComposite;
  testComposite.add(circle_);
  testComposite.add(rectangle_);
  BOOST_CHECK_THROW(testComposite.scale(-2), std::invalid_argument);

  BOOST_CHECK_THROW(testComposite[100], std::out_of_range);
  BOOST_CHECK_THROW(testComposite[-1], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
