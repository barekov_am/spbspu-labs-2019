#ifndef SHAPE_OF_MG
#define SHAPE_OF_MG

#include <memory>
#include "base-types.hpp"

namespace gnatishin
{
  class Shape
  {
    public:
      using pointer = std::shared_ptr<Shape>;
      using array = std::unique_ptr<pointer[]>;

      virtual ~Shape() = default;
      virtual double getArea() const = 0;
      virtual rectangle_t getFrameRect() const = 0;
      virtual point_t getPosition() const = 0;
      virtual void move(const point_t &center) = 0;
      virtual void move(double dx, double dy) = 0;
      virtual void scale(double scaleFactor) = 0;
      virtual void rotate(double degree) = 0;
  };
}

#endif
