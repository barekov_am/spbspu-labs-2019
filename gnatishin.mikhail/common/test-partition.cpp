#include <boost/test/auto_unit_test.hpp>

#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

const double PRECISION = 0.01;

BOOST_AUTO_TEST_SUITE(testPartition)

  BOOST_AUTO_TEST_CASE(intersectCorrectness)
  {
    gnatishin::Circle testCircle({ 2, 0 }, 3);
    gnatishin::Rectangle testRectangle({ 2, 4 }, 9, 5);
    gnatishin::Rectangle testSquare({ 12, 21 }, 1, 1);

    BOOST_CHECK(gnatishin::isIntersected(testCircle.getFrameRect(),testRectangle.getFrameRect()));
    BOOST_CHECK(!(gnatishin::isIntersected(testSquare.getFrameRect(),testRectangle.getFrameRect())));
    BOOST_CHECK(!(gnatishin::isIntersected(testCircle.getFrameRect(),testSquare.getFrameRect())));
  }

BOOST_AUTO_TEST_SUITE_END()
