#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

const double CalcError = 0.01;

BOOST_AUTO_TEST_SUITE(circleTestSuite)

BOOST_AUTO_TEST_CASE(invariabilityOfCircleAfterMoving)
{
  gnatishin::Circle testCircle({3.0, 5.5}, 2.0);
  const gnatishin::rectangle_t frameBefore = testCircle.getFrameRect();
  const double areaBefore = testCircle.getArea();

  testCircle.move({5.0, 7.8});
  gnatishin::rectangle_t frameNow = testCircle.getFrameRect();
  double areaNow = testCircle.getArea();
  BOOST_CHECK_CLOSE(frameBefore.width, frameNow.width, CalcError);
  BOOST_CHECK_CLOSE(frameBefore.height, frameNow.height, CalcError);
  BOOST_CHECK_CLOSE(areaBefore, areaNow, CalcError);

  testCircle.move(-2.0, 8.6);
  frameNow = testCircle.getFrameRect();
  areaNow = testCircle.getArea();
  BOOST_CHECK_CLOSE(frameBefore.width, frameNow.width, CalcError);
  BOOST_CHECK_CLOSE(frameBefore.height, frameNow.height, CalcError);
  BOOST_CHECK_CLOSE(areaBefore, areaNow, CalcError);
}

BOOST_AUTO_TEST_CASE(changingAfterScalingCircle)
{
  gnatishin::Circle testCircle({2.0, 4.0}, 3.0);
  const double areaBefore = testCircle.getArea();
  const double scaleFactor = 2.0;

  testCircle.scale(scaleFactor);
  double areaNow = testCircle.getArea();
  BOOST_CHECK_CLOSE(areaBefore * scaleFactor * scaleFactor, areaNow, CalcError);
}

BOOST_AUTO_TEST_CASE(invalidParametersValidationOfCircle)
{
  BOOST_CHECK_THROW(gnatishin::Circle({2.5, 3.5}, -3.0), std::invalid_argument);

  gnatishin::Circle testCircle({-6.0, 3.5}, 5.5);
  BOOST_CHECK_THROW(testCircle.scale(-8.8), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
