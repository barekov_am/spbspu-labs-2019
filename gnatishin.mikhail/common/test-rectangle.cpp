#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

const double CalcError = 0.01;

BOOST_AUTO_TEST_SUITE(rectangleTestSuite)

BOOST_AUTO_TEST_CASE(invariabilityOfRectangleAfterMoving)
{
  gnatishin::Rectangle testRectangle({6.3, 4.0}, 5.0, 10.0);
  const gnatishin::rectangle_t frameBefore = testRectangle.getFrameRect();
  const double areaBefore = testRectangle.getArea();

  testRectangle.move({5.0, 1.5});
  gnatishin::rectangle_t frameNow = testRectangle.getFrameRect();
  double areaNow = testRectangle.getArea();
  BOOST_CHECK_CLOSE(frameBefore.width, frameNow.width, CalcError);
  BOOST_CHECK_CLOSE(frameBefore.height, frameNow.height, CalcError);
  BOOST_CHECK_CLOSE(areaBefore, areaNow, CalcError);

  testRectangle.move(7.5, 4.0);
  frameNow = testRectangle.getFrameRect();
  areaNow = testRectangle.getArea();
  BOOST_CHECK_CLOSE(frameBefore.width, frameNow.width, CalcError);
  BOOST_CHECK_CLOSE(frameBefore.height, frameNow.height, CalcError);
  BOOST_CHECK_CLOSE(areaBefore, areaNow, CalcError);
}

BOOST_AUTO_TEST_CASE(changingAfterScalingRectangle)
{
  gnatishin::Rectangle testRectangle({2.0, 4.0}, 3.0, 5.0);
  const double areaBefore = testRectangle.getArea();
  const double scaleFactor = 2.5;

  testRectangle.scale(scaleFactor);
  double areaNow = testRectangle.getArea();
  BOOST_CHECK_CLOSE(areaBefore * scaleFactor * scaleFactor, areaNow, CalcError);
}

BOOST_AUTO_TEST_CASE(invalidParametersValidationOfRectangle)
{
  BOOST_CHECK_THROW(gnatishin::Rectangle({2.5, 3.5}, 3.0, -6.0), std::invalid_argument);
  BOOST_CHECK_THROW(gnatishin::Rectangle({2.5, 3.5}, -3.0, 6.0), std::invalid_argument);
  BOOST_CHECK_THROW(gnatishin::Rectangle({2.5, 3.5}, -3.0, -6.0), std::invalid_argument);

  gnatishin::Rectangle testRectangle({-6.0, 3.5}, 5.5, 8.0);
  BOOST_CHECK_THROW(testRectangle.scale(-0.1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
