#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  gnatishin::Circle circle{{5.0, 10.0}, 1.0};
  gnatishin::Shape *shapePointer = &circle;

  std::cout << " Circle Information:" << '\n';
  std::cout << "Area - " << shapePointer->getArea() << '\n';
  std::cout << "Width of Frame - " << shapePointer->getFrameRect().width << '\n';
  std::cout << "Height of Frame - " << shapePointer->getFrameRect().height << '\n';
  std::cout << "Position of the center is ("
      << shapePointer->getPosition().x << "; "
      << shapePointer->getPosition().y << ")\n";

  std::cout << "\n Actions:" << '\n';
  std::cout << " Move Circle for dx = 5, dy = -5" << '\n';
  shapePointer->move(5.0, -5.0);
  std::cout << "Position of the center is ("
      << shapePointer->getPosition().x << "; "
      << shapePointer->getPosition().y << ")\n";
  std::cout << " Move Circle to (0; 10)" << '\n';
  shapePointer->move({0.0, 10.0});
  std::cout << "Position of the center is ("
      << shapePointer->getPosition().x << "; "
      << shapePointer->getPosition().y << ")\n";
  std::cout << " Scaling circle by the factor of 2.0\n";
  shapePointer->scale(2.0);
  std::cout << "Area - " << shapePointer->getArea() << '\n';

  gnatishin::Rectangle rectangle{{10.0, 15.0}, 2.0, 1.0};
  shapePointer = &rectangle;

  std::cout << "\n Rectangle Information:" << '\n';
  std::cout << "Area - " << shapePointer->getArea() << '\n';
  std::cout << "Width of Frame - " << shapePointer->getFrameRect().width << '\n';
  std::cout << "Height of Frame - " << shapePointer->getFrameRect().height << '\n';
  std::cout << "Position of the center is ("
      << shapePointer->getPosition().x << "; "
      << shapePointer->getPosition().y << ")\n";

  std::cout << "\n Actions:" << '\n';
  std::cout << " Move Rectangle for dx = 7.9, dy = -4.5" << '\n';
  shapePointer->move(7.9, -4.5);
  std::cout << "Position of the center is ("
      << shapePointer->getPosition().x << "; "
      << shapePointer->getPosition().y << ")\n";
  std::cout << " Move Rectangle to (-3; 4)" << '\n';
  shapePointer->move({-3.0, 4.0});
  std::cout << "Position of the center is ("
      << shapePointer->getPosition().x << "; "
      << shapePointer->getPosition().y << ")\n";
  std::cout << " Scaling rectangle by the factor of 2.0\n";
  shapePointer->scale(2.0);
  std::cout << "Area - " << shapePointer->getArea() << '\n';

  gnatishin::CompositeShape compShape;
  gnatishin::Shape::pointer circle_ = std::make_shared<gnatishin::Circle>(circle);
  compShape.add(circle_);  //circle{{0, 10}, 2}
  gnatishin::Shape::pointer rectangle_ = std::make_shared<gnatishin::Rectangle>(rectangle);
  compShape.add(rectangle_);  //rectangle{{-3, 4}, 4, 2}
  shapePointer = &compShape;

  std::cout << "\n Composite Shape Information:" << '\n';
  std::cout << "Area - " << shapePointer->getArea() << '\n';
  std::cout << "Width of Frame - " << shapePointer->getFrameRect().width << '\n';
  std::cout << "Height of Frame - " << shapePointer->getFrameRect().height << '\n';
  std::cout << "Position of the center is ("
      << shapePointer->getPosition().x << "; "
      << shapePointer->getPosition().y << ")\n";

  std::cout << "\n Actions:" << '\n';
  std::cout << " Move Composite Shape for dx = 1, dy = 3" << '\n';
  shapePointer->move(1.0, 3.0);
  std::cout << "Position of the center is ("
      << shapePointer->getPosition().x << "; "
      << shapePointer->getPosition().y << ")\n";
  std::cout << " Move Composite Shape to (-3; 4)" << '\n';
  shapePointer->move({-3.0, 4.0});
  std::cout << "Position of the center is ("
      << shapePointer->getPosition().x << "; "
      << shapePointer->getPosition().y << ")\n";
  std::cout << " Scaling Composite Shape by the factor of 2.0\n";
  shapePointer->scale(2.0);
  std::cout << "Position of the center is ("
      << shapePointer->getPosition().x << "; "
      << shapePointer->getPosition().y << ")\n";
  std::cout << "Frame of Composite Shape - "
      << shapePointer->getFrameRect().width << 'x' << shapePointer->getFrameRect().height << '\n';
  std::cout << "Area - " << shapePointer->getArea() << '\n';

  std::cout << "\n Removing shape number 1\n";
  compShape.remove(1); //circle{{0, 20}, 2} remains
  std::cout << "Width of Frame: " << shapePointer->getFrameRect().width << '\n';
  std::cout << "Height of Frame: " << shapePointer->getFrameRect().height << '\n';
  std::cout << "Position of the center is ("
      << shapePointer->getPosition().x << "; "
      << shapePointer->getPosition().y << ")\n";

  return 0;
}
