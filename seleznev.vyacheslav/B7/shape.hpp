#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>
#include <vector>
#include <memory>

class Shape
{
public:
  bool isMoreLeft(const std::unique_ptr<Shape>& shape) const;
  bool isUpper(const std::unique_ptr<Shape>& shape) const;

  int getX() const;
  int getY() const;

  virtual void draw(std::ostream& out) const = 0;

protected:
  Shape(int x, int y);

private:
  int x_;
  int y_;
};

#endif
