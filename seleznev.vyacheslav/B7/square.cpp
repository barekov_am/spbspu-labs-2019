#include "square.hpp"

Square::Square(int x, int y):
  Shape(x, y)
{
}

void Square::draw(std::ostream& out) const
{
  out << "SQUARE (" << getX() << ";" << getY() << ")\n";
}
