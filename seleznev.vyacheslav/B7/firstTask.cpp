#include <cmath>
#include <algorithm>
#include <iostream>
#include <list>
#include <iterator>

void firstTask()
{
  std::list<double> list((std::istream_iterator<double>(std::cin)), std::istream_iterator<double>());

  if (!std::cin.eof())
  {
    throw std::invalid_argument("Errors in reading data");
  }

  std::for_each(list.begin(), list.end(), [](double& number)
  {
    number *= M_PI; 
    std::cout << number << " ";
  });
}
