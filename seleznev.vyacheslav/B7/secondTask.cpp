#include <sstream>
#include <memory>
#include <algorithm>

#include "circle.hpp"
#include "triangle.hpp"
#include "square.hpp"

bool isInvalidCharacter(char& ch)
{
  std::vector<char> invalidChars = { '\n', '\t', ' ' };

  return std::find(invalidChars.begin(), invalidChars.end(), ch) != invalidChars.end();
}

void removeInvalidChars(char& currentChar, std::stringstream& str)
{
  while (isInvalidCharacter(currentChar))
  {
    currentChar = str.get();
  }
}

void removeInvalidStartCharacters(std::string& str)
{
  while (!str.empty() && (isInvalidCharacter(str[0]) || str[0] == -1))
  {
    str = str.erase(0, 1);
  }
}

bool isEndCharacter(char& c)
{
  return c == -1;
}

void read(const std::string& stream, std::vector<std::unique_ptr<Shape>>& shapes)
{
  std::stringstream string(stream);
  std::string shape;
  char c = string.get();

  int x;
  int y;

  removeInvalidChars(c, string);

  while (c != ' ' && c != '(' && !isEndCharacter(c))
  {
    shape += c;
    c = string.get();
  }

  removeInvalidChars(c, string);

  if (c == '(')
  {
    c = string.get();
  }
  else
  {
    throw std::invalid_argument("Invalid parameters");
  }

  removeInvalidChars(c, string);

  std::string xStr;
  std::string yStr;

  while (c != ' ' && c != ';' && !isEndCharacter(c))
  {
    xStr += c;
    c = string.get();
  }

  removeInvalidChars(c, string);

  if (c == ';')
  {
    c = string.get();
  }

  removeInvalidChars(c, string);

  while (c != ' ' && c != ')' && !isEndCharacter(c))
  {
    yStr += c;
    c = string.get();
  }

  if (xStr.empty() || yStr.empty())
  {
    throw std::invalid_argument("Invalid parameters");
  }

  x = std::stoi(xStr);
  y = std::stoi(yStr);

  if (string.fail())
  {
    throw std::invalid_argument("Invalid parameters");
  }

  if (shape == "CIRCLE")
  {
    shapes.push_back(std::move(std::unique_ptr<Circle>(new Circle(x, y))));
  }
  else if (shape == "TRIANGLE")
  {
    shapes.push_back(std::move(std::unique_ptr<Shape>(new Triangle(x, y))));
  }
  else if (shape == "SQUARE")
  {
    shapes.push_back(std::move(std::unique_ptr<Shape>(new Square(x, y))));
  }
  else
  {
    throw std::runtime_error("Incorrect shape");
  }
}

void printData(const std::vector<std::unique_ptr<Shape>>& vector)
{
  std::for_each(vector.begin(), vector.end(), [](const std::unique_ptr<Shape>& shape) {shape->draw(std::cout); });
}

bool isMoreLeftComparision(const std::unique_ptr<Shape>& first, const std::unique_ptr<Shape>& second)
{
  return first->isMoreLeft(second);
}

bool isMoreRightComparision(const std::unique_ptr<Shape>& first, const std::unique_ptr<Shape>& second)
{
  return !first->isMoreLeft(second);
}

bool isTopBottomComparision(const std::unique_ptr<Shape>& first, const std::unique_ptr<Shape>& second)
{
  return first->isUpper(second);
}

bool isBottomTopComparision(const std::unique_ptr<Shape>& first, const std::unique_ptr<Shape>& second)
{
  return !first->isUpper(second);
}

void secondTask()
{
  std::vector<std::unique_ptr<Shape>> shapes;
  std::string line;

  while (std::getline(std::cin, line))
  {
    removeInvalidStartCharacters(line);

    if (!line.empty())
    {
      read(line, shapes);
    }
  }

  std::cout << "Original:\n";
  printData(shapes);

  std::sort(shapes.begin(), shapes.end(), isMoreLeftComparision);
  std::cout << "Left-Right:\n";
  printData(shapes);

  std::sort(shapes.begin(), shapes.end(), isMoreRightComparision);
  std::cout << "Right-Left:\n";
  printData(shapes);

  std::sort(shapes.begin(), shapes.end(), isTopBottomComparision);
  std::cout << "Top-Bottom:\n";
  printData(shapes);

  std::sort(shapes.begin(), shapes.end(), isBottomTopComparision);
  std::cout << "Bottom-Top:\n";
  printData(shapes);
}
