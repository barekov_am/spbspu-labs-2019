#include "triangle.hpp"

Triangle::Triangle(int x, int y):
  Shape(x, y)
{
}

void Triangle::draw(std::ostream& out) const
{
  out << "TRIANGLE (" << getX() << ";" << getY() << ")\n";
}
