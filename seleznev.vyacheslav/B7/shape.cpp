#include "shape.hpp"

Shape::Shape(int x, int y):
  x_(x),
  y_(y)
{
}

bool Shape::isMoreLeft(const std::unique_ptr<Shape>& shape) const
{
  return x_ < shape->x_;
}

bool Shape::isUpper(const std::unique_ptr<Shape>& shape) const
{
  return y_ > shape->y_;
}

int Shape::getX() const
{
  return x_;
}

int Shape::getY() const
{
  return y_;
}
