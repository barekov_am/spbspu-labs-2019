#include "text.hpp"

Text::Text(std::string& value, types type) :
  value_(value),
  type_(type)
{
}

types Text::getType()
{
  return type_;
}

std::string Text::toString()
{
  return value_;
}

size_t Text::getSize()
{
  return value_.size();
}
