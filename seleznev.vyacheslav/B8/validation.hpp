#ifndef VALIDATION_HPP
#define VALIDATION_HPP

#include <stdexcept>

#include "text.hpp"
#include "formatting.hpp"

bool canBePlacedAfterWord(Text& txt)
{
  const types type = txt.getType();

  return type == SPACE || type == HYPHEN || type == SEMICOLON
      || type == COLON || type == QUESTIONMARK || type == EXCLAMATIONMARK || type == COMMA || type == DOT;
}

bool canBePlacedAfterPunctuation(const types& previousType, std::list<Text>::iterator current, const std::list<Text>& data)
{
  while (current != data.end() && current->getType() == SPACE)
  {
    ++current;
  }

  if (current == data.end())
  {
    return false;
  }

  const types type = current->getType();

  if (previousType == COMMA)
  {
    return type == HYPHEN || type == WORD || type == DIGIT;
  }

  return type == WORD || type == DIGIT;
}

bool canBePlacedAfterDigit(Text& txt)
{
  const types type = txt.getType();

  return type == SPACE || type == TAB || type == NEWLINE || type == HYPHEN || type == SEMICOLON
      || type == COLON || type == QUESTIONMARK || type == EXCLAMATIONMARK || type == COMMA || type == DOT;
}

bool canBePlacedAfterWhitespace(Text& txt)
{
  const types type = txt.getType();

  return type == WORD || type == DIGIT || type == HYPHEN;
}

void validation(std::list<Text>& txt)
{
  if (txt.size() == 0)
  {
    return;
  }

  types previousType = txt.begin()->getType();

  if (previousType == HYPHEN || isPunctuation(previousType))
  {
    throw std::invalid_argument("Invalid start character");
  }

  for (auto i = ++txt.begin(); i != txt.end(); ++i)
  {
    if (previousType == WORD && !canBePlacedAfterWord(*i))
    {
      throw std::invalid_argument("Word validation error");
    }

    if ((isPunctuation(previousType) || previousType == HYPHEN) && !canBePlacedAfterPunctuation(previousType, i, txt))
    {
      throw std::invalid_argument("Punctuation validation error");
    }

    if (previousType == DIGIT && !canBePlacedAfterDigit(*i))
    {
      throw std::invalid_argument("Digit validation error");
    }

    auto tmp = i;
    ++tmp;

    if (tmp != txt.end() && (previousType == HYPHEN && !isWordOrDigit(tmp)))
    {
      throw std::invalid_argument("Punctuation validation error");
    }

    previousType = i->getType();
  }
}

#endif
