#ifndef WORD_HPP
#define WORD_HPP

#include "text.hpp"

class Word : public Text
{
public:
  Word(std::string& word);
};

#endif
