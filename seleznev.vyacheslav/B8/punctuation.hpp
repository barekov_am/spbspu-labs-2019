#ifndef PUNCTUATION_HPP
#define PUNCTUATION_HPP

#include "text.hpp"

class Punctuation : public Text
{
public:
  Punctuation(std::string& punct, types type);
};

#endif
