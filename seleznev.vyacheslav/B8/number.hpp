#ifndef NUMBER_HPP
#define NUMBER_HPP

#include "text.hpp"

class Number : public Text
{
public:
  Number(std::string& digit);
};

#endif
