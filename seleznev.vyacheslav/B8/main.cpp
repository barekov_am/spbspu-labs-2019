#include <iostream>
#include <string>

void task(const int& lineWidth);

int main(int argc, char* argv[])
{
  try
  {
    size_t standardLineWidth = 40;
    size_t minLineWidth = 25;

    if ((argc > 3) || (argc == 2))
    {
      std::cerr << "Invalid command\n";
      return 1;
    }

    if (argv[1] != NULL && !(std::string(argv[1]) == "--line-width"))
    {
      std::cerr << "Invalid command\n";
      return 1;
    }

    if (argc == 3 && argv[2] != NULL)
    {
      const size_t lineWidth = std::stoi(argv[2]);

      if (lineWidth >= minLineWidth)
      {
        task(lineWidth);
        return 0;
      }
      else
      {
        std::cerr << "Invalid parameters\n";
        return 1;
      }
    }

    task(standardLineWidth);
    return 0;
  }
  catch (const std::exception& err)
  {
    std::cerr << err.what() << "\n";
    return 1;
  }

  return 0;
}
