#ifndef TEXT_HPP
#define TEXT_HPP

#include <list>
#include <string>

enum types
{
  WORD,
  DOT,
  COMMA,
  EXCLAMATIONMARK,
  QUESTIONMARK,
  COLON,
  SEMICOLON,
  HYPHEN,
  DIGIT,
  SPACE,
  TAB,
  NEWLINE
};

class Text
{
public:
  types getType();
  std::string toString();
  size_t getSize();

protected:
  Text(std::string& value, types type);

private:
  std::string value_;
  types type_;
};

#endif
