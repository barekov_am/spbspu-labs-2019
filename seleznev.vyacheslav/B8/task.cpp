#include "textToString.hpp"
#include "validation.hpp"

void printText(std::list<std::string>& txt)
{
  auto i = txt.begin();

  while (i != txt.end())
  {
    std::cout << *i << "\n";
    ++i;
  }
}

void task(const int& lineWidth)
{
  auto listOfChars = getInitialList();
  auto listOfWords = convertToWordList(listOfChars);
  formatting(listOfWords);
  validation(listOfWords);
  auto newTextVec = textToString(listOfWords, lineWidth);
  printText(newTextVec);
}
