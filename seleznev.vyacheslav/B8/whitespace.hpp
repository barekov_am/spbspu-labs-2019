#ifndef WHITESPACE_HPP
#define WHITESPACE_HPP

#include "text.hpp"

class Whitespace : public Text
{
public:
  Whitespace(std::string& space, types type);
};

#endif
