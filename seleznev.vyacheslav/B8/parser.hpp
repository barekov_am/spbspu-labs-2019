#ifndef PARSER_HPP
#define PARSER_HPP

#include <sstream>
#include <iostream>
#include <stdexcept>

#include "word.hpp"
#include "punctuation.hpp"
#include "number.hpp"
#include "whitespace.hpp"

bool isPartOfWord(const std::list<char>& data, std::list<char>::iterator& iter)
{
  auto nextIter = iter;
  ++nextIter;

  if (*iter != '-')
  {
    return false;
  }

  if (nextIter == data.end())
  {
    return true;
  }

  if (std::isalpha(*nextIter))
  {
    return true;
  }

  if (std::isspace(*nextIter))
  {
    return true;
  }

  if (std::ispunct(*nextIter) && *nextIter != '-')
  {
    return true;
  }

  return false;
}

std::string getWord(const std::list<char>& data, std::list<char>::iterator& iter)
{
  std::string word = "";

  while (iter != data.end())
  {
    char c = *iter;

    if (!std::isalpha(c) && !isPartOfWord(data, iter))
    {
      break;
    }

    word += c;

    if (word.size() > 20)
    {
      throw std::invalid_argument("Too long word");
    }

    ++iter;
  }

  return word;
}

bool isDigit(const std::list<char>& data, std::list<char>::iterator& iter)
{
  auto nextIter = iter;
  ++nextIter;

  if (std::isdigit(*iter))
  {
    return true;
  }
  
  return (*iter == '-' || *iter == '+') && nextIter != data.end() && std::isdigit(*nextIter);
}

std::string getNum(const std::list<char>& data, std::list<char>::iterator& iter)
{
  std::string num = "";

  while (iter != data.end())
  {
    char c = *iter;

    if (!std::isdigit(c) && c != '-' && c != '+' && c != '.')
    {
      break;
    }

    num += c;

    if (num.size() > 20)
    {
      throw std::invalid_argument("Too long digit");
    }

    ++iter;
  }

  return num;
}

std::string getPunct(const std::list<char>& data, std::list<char>::iterator& iter)
{
  std::string punct = "";
  char c = *iter;

  if (c == '-')
  {
    while (iter != data.end())
    {
      c = *iter;

      if (c != '-')
      {
        break;
      }

      punct += c;
      ++iter;

      if (punct.size() > 3)
      {
        throw std::invalid_argument("Incorrect input data");
      }
    }

    return punct;
  }

  punct += c;
  ++iter;

  return punct;
}

types getTypeForPunctuation(const std::string& str)
{
  if (str == ".")
  {
    return DOT;
  }
  else if (str == ",")
  {
    return COMMA;
  }
  else if (str == "!")
  {
    return EXCLAMATIONMARK;
  }
  else if (str == "?")
  {
    return QUESTIONMARK;
  }
  else if (str == ":")
  {
    return COLON;
  }
  else if (str == ";")
  {
    return SEMICOLON;
  }
  else if (str == "---")
  {
    return HYPHEN;
  }
  else
  {
    throw std::invalid_argument("Type is not correct!");
  }
}

types getTypeForWhitespace(const std::string& str)
{
  if (str == " ")
  {
    return SPACE;
  }
  else if (str == "\t")
  {
    return TAB;
  }
  else if (str == "\n" || str == "\r")
  {
    return NEWLINE;
  }
  else
  {
    throw std::invalid_argument("Type is not correct");
  }
}

std::list<Text> convertToWordList(std::list<char>& data)
{
  std::string stream;
  std::list<Text> wordlist;
  std::list<char>::iterator iter = data.begin();
  char c;

  while (iter != data.end())
  {
    c = *iter;

    if (std::isalpha(c))
    {
      std::string wordStr = getWord(data, iter);
      Word wrd(wordStr);
      wordlist.push_back(wrd);
    }
    else if (isDigit(data, iter))
    {
      std::string numStr = getNum(data, iter);
      Number num(numStr);
      wordlist.push_back(num);
    }
    else if (std::ispunct(c))
    {
      std::string punctStr = getPunct(data, iter);
      types type = getTypeForPunctuation(punctStr);
      Punctuation punct(punctStr, type);
      wordlist.push_back(punct);
    }
    else if (std::isspace(c))
    {
      std::string whitespaceStr(1, c);
      types type = getTypeForWhitespace(whitespaceStr);
      Whitespace space(whitespaceStr, type);
      wordlist.push_back(space);
      ++iter;
    }
    else
    {
      throw std::invalid_argument("Incorrect parametrs");
    }
  }

  return wordlist;
}

std::list<char> getInitialList()
{
  std::list<char> data;
  char c = std::cin.get();
  
  while (c != -1)
  {
    data.push_back(c);
    c = std::cin.get();
  }

  return data;
}

#endif
