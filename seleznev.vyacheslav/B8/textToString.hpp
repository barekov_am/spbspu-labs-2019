#ifndef TEXTTOSTRING_HPP
#define TEXTTOSTRING_HPP

#include "formatting.hpp"

size_t getFragmentSize(const std::list<Text>& txt, const std::list<Text>::iterator& iter)
{
  auto originalSize = iter->getSize();

  if (!isWordOrDigit(iter))
  {
    return originalSize;
  }

  bool canPlaceOnTheSameLine = false;
  auto size = originalSize;
  auto next = iter;
  ++next;

  while (next != txt.end() && !isWordOrDigit(next))
  {
    size += next->getSize();

    if (next->getType() == HYPHEN || isPunctuation(next->getType()))
    {
      canPlaceOnTheSameLine = true;
      break;
    }

    ++next;
  }

  if (!canPlaceOnTheSameLine)
  {
    return originalSize;
  }

  return size;
}

std::list<std::string> textToString(std::list<Text>& txt, const size_t& lineWidth)
{
  std::list<std::string> newText;
  std::string current = "";
  auto i = txt.begin();

  while (i != txt.end())
  {
    auto fragmentSize = getFragmentSize(txt, i);

    if (current.size() + fragmentSize <= lineWidth)
    {
      if (i->getType() == SPACE)
      {
        auto next = i;
        ++next;

        if (next != txt.end())
        {
          auto nextFragentSize = getFragmentSize(txt, next);

          if (current.size() + 1 + nextFragentSize <= lineWidth && !current.empty())
          {
            current += i->toString();
          }
          else if (!current.empty())
          {
            newText.push_back(current);
            current = "";
          }
        }
      }
      else
      {
        current += i->toString();
      }

      ++i;
    }
    else
    {
      if (current.empty())
      {
        throw std::invalid_argument("Empty");
      }
      
      newText.push_back(current);
      current = "";
    }
  }

  if (!current.empty())
  {
    newText.push_back(current);
  }

  return newText;
}

#endif
