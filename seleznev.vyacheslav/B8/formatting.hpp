#ifndef FORMATTING_HPP
#define FORMATTING_HPP

#include "parser.hpp"

bool isPunctuation(const types& type)
{
  return type == DOT || type == COMMA || type == EXCLAMATIONMARK
      || type == QUESTIONMARK || type == COLON || type == SEMICOLON;
}

bool isWordOrDigit(const std::list<Text>::iterator& iter)
{
  auto type = iter->getType();

  return type == WORD || type == DIGIT;
}

std::list<Text>::iterator insertWhitespace(std::list<Text>& txt, const std::list<Text>::iterator& position)
{
  std::string whiteSpace = " ";
  Whitespace space(whiteSpace, SPACE);

  return txt.insert(position, space);
}

void removeInvalidWhitespaceChars(std::list<Text>& txt)
{
  auto previous = txt.begin();
  auto current = ++txt.begin();

  while (current != txt.end())
  {
    if (current->getType() == TAB || current->getType() == NEWLINE)
    {
      current = txt.erase(current);
    }
    else
    {
      previous = current;
      ++current;
    }
  }
}

void removeDuplicateWhiteSpace(std::list<Text>& txt)
{
  auto previous = txt.begin();
  auto current = ++txt.begin();

  while (current != txt.end())
  {
    if (previous->getType() == SPACE && current->getType() == SPACE)
    {
      current = txt.erase(current);
    }
    else
    {
      previous = current;
      ++current;
    }
  }
}

void formatting(std::list<Text>& txt)
{
  if (txt.size() == 0)
  {
    return;
  }

  removeInvalidWhitespaceChars(txt);
  removeDuplicateWhiteSpace(txt);

  auto previous = txt.begin();
  auto current = ++txt.begin();

  while (current != txt.end())
  {
    if ((previous->getType() == WORD || previous->getType() == DIGIT) && current->getType() == SPACE)
    {
      auto nextElement = current;
      ++nextElement;

      if (nextElement != txt.end() && isPunctuation(nextElement->getType()))
      {
        current = txt.erase(current);
      }
      else if (nextElement == txt.end())
      {
        throw std::invalid_argument("Errors in formatting");
      }
      else
      {
        previous = current;
        ++current;
      }
    }
    else if ((current->getType() == HYPHEN && previous->getType() != SPACE && !isPunctuation(previous->getType()))
        || (previous->getType() == HYPHEN && current->getType() != SPACE && !isPunctuation(current->getType())))
    {
      current = insertWhitespace(txt, current);
      previous = current;
      ++current;
    }
    else if (isPunctuation(previous->getType()) && !isPunctuation(current->getType())
        && current->getType() != HYPHEN && current->getType() != SPACE)
    {
      current = insertWhitespace(txt, current);
      previous = current;
      ++current;
    }
    else if (isWordOrDigit(previous) && isWordOrDigit(current))
    {
      current = insertWhitespace(txt, current);
      previous = current;
      ++current;
    }
    else
    {
      previous = current;
      ++current;
    }
  }
}

#endif
