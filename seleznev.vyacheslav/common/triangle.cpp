#include "triangle.hpp"

#include <cmath>
#include <stdexcept>
#include <algorithm>

seleznev::Triangle::Triangle(const point_t& pA, const point_t& pB, const point_t& pC):
  pointA_(pA),
  pointB_(pB),
  pointC_(pC),
  angle_(0)
{
  if (getArea() == 0)
  {
    throw std::out_of_range("Area is out of range");
  }
}

double seleznev::Triangle::getArea() const
{
  return std::abs((pointC_.x - pointA_.x) * (pointC_.y - pointB_.y)
      - (pointC_.y - pointA_.y) * (pointC_.x - pointB_.x)) / 2;
}

seleznev::rectangle_t seleznev::Triangle::getFrameRect() const
{
  double minX = std::min(pointA_.x, std::min(pointB_.x, pointC_.x));
  double minY = std::min(pointA_.y, std::min(pointB_.y, pointC_.y));
  double maxX = std::max(pointA_.x, std::max(pointB_.x, pointC_.x));
  double maxY = std::max(pointA_.y, std::max(pointB_.y, pointC_.y));
  double w = maxX - minX;
  double h = maxY - minY;
  double cenX = (minX + maxX) / 2;
  double cenY = (minY + maxY) / 2;

  return {w, h, {cenX, cenY}};
}

void seleznev::Triangle::move(const point_t& point)
{
  point_t centre = {(pointA_.x + pointB_.x + pointC_.x) / 3, (pointA_.y + pointB_.y + pointC_.y) / 3};

  move(point.x - centre.x, point.y - centre.y);
}

void seleznev::Triangle::move(const double dx, const double dy)
{
  point_t centre = {(pointA_.x + pointB_.x + pointC_.x) / 3, (pointA_.y + pointB_.y + pointC_.y) / 3};

  pointA_.x += dx;
  pointB_.x += dx;
  pointC_.x += dx;
  pointA_.y += dy;
  pointB_.y += dy;
  pointC_.y += dy;
  centre.x += dx;
  centre.y += dy;
}

std::string seleznev::Triangle::getName() const
{
  return "Triangle";
}

void seleznev::Triangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::out_of_range("Wrong coefficient");
  }

  point_t centre = {(pointA_.x + pointB_.x + pointC_.x) / 3, (pointA_.y + pointB_.y + pointC_.y) / 3};

  pointA_.y = centre.y + (pointA_.y - centre.y) * coefficient;
  pointA_.x = centre.x + (pointA_.x - centre.x) * coefficient;
  pointB_.y = centre.y + (pointB_.y - centre.y) * coefficient;
  pointB_.x = centre.x + (pointB_.x - centre.x) * coefficient;
  pointC_.y = centre.y + (pointC_.y - centre.y) * coefficient;
  pointC_.x = centre.x + (pointC_.x - centre.x) * coefficient;
}

void seleznev::Triangle::rotate(double angle)
{
  point_t centre = {(pointA_.x + pointB_.x + pointC_.x) / 3, (pointA_.y + pointB_.y + pointC_.y) / 3};

  pointA_ = rotatePoint(pointA_, centre, angle);
  pointB_ = rotatePoint(pointB_, centre, angle);
  pointC_ = rotatePoint(pointC_, centre, angle);
}

seleznev::point_t seleznev::Triangle::rotatePoint(point_t point, point_t centre, double angle)
{
  const double sinus = sin(angle * M_PI / 180);
  const double cosinus = cos(angle * M_PI / 180);

  double x = point.x;
  double y = point.y;

  x = centre.x + (x - centre.x) * cosinus - (y - centre.y) * sinus;
  y = centre.y + (y - centre.y) * cosinus + (x - centre.x) * sinus;

  return {x, y};
}
