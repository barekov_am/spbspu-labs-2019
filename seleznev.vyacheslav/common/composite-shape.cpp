#include "composite-shape.hpp"

#include <cmath>
#include <algorithm>
#include <stdexcept>

seleznev::CompositeShape::CompositeShape():
  count_(0),
  arrayOfShapes_(),
  angle_(0)
{
}

std::shared_ptr<seleznev::Shape> seleznev::CompositeShape::operator [](size_t i) const
{
  if (i >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }

  return arrayOfShapes_[i];
}

double seleznev::CompositeShape::getArea() const
{
  double area = 0;

  for(size_t i = 0; i < count_; i++)
  {
    area += arrayOfShapes_[i]->getArea();
  }

  return area;
}

seleznev::rectangle_t seleznev::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    return {0, 0, {0, 0}};
  }

  rectangle_t firstFrameRect = arrayOfShapes_[0]->getFrameRect();

  double minX = firstFrameRect.pos.x;
  double maxX = firstFrameRect.pos.x;
  double minY = firstFrameRect.pos.y;
  double maxY = firstFrameRect.pos.y;

  for(size_t i = 0; i < count_; i++)
  {
    rectangle_t currentFrameRect = arrayOfShapes_[i]->getFrameRect();

    double rightX = getMaxCoordinate(currentFrameRect.pos.x, currentFrameRect.width);
    double leftX = getMinCoordinate(currentFrameRect.pos.x, currentFrameRect.width);
    double topY = getMaxCoordinate(currentFrameRect.pos.y, currentFrameRect.height);
    double bottomY = getMinCoordinate(currentFrameRect.pos.y, currentFrameRect.height);

    minX = std::min(minX, leftX);
    maxX = std::max(maxX, rightX);
    minY = std::min(minY, bottomY);
    maxY = std::max(maxY, topY);
  }

  double width = (maxY - minY);
  double height = (maxX - minX);
  point_t rectCentre = {minX + (width / 2), minY + (height / 2)};

  return {width, height, rectCentre};
}

void seleznev::CompositeShape::move(const seleznev::point_t& point)
{
  rectangle_t frameRect = getFrameRect();
  double dx = point.x - frameRect.pos.x;
  double dy = point.y - frameRect.pos.y;

  for(size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i]->move(dx, dy);
  }
}

void seleznev::CompositeShape::move(const double dx, const double dy)
{
  for(size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i]->move(dx, dy);
  }
}

std::string seleznev::CompositeShape::getName() const
{
  return "CompositeShape";
}

void seleznev::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::out_of_range("Coefficient is out of range");
  }

  seleznev::point_t centre = getFrameRect().pos;
  for(size_t i = 0; i < count_; i++)
  {
    seleznev::point_t centreShape = arrayOfShapes_[i]->getFrameRect().pos;
    double x = (centreShape.x - centre.x) * (coefficient - 1);
    double y = (centreShape.y - centre.y) * (coefficient - 1);
    arrayOfShapes_[i]->move(x, y);
    arrayOfShapes_[i]->scale(coefficient);
  }
}

void seleznev::CompositeShape::add(std::shared_ptr<seleznev::Shape> shape)
{
  if (shape == nullptr)
  {
    throw std::out_of_range("Null shape");
  }

  int newCount = count_ + 1;

  std::unique_ptr<std::shared_ptr<Shape>[]> newArray(std::make_unique<std::shared_ptr<Shape>[]>(newCount));

  for(size_t i = 0; i < count_; i++)
  {
    newArray[i] = arrayOfShapes_[i];
  }

  newArray[count_] = shape;

  arrayOfShapes_.swap(newArray);
  count_ = newCount;
}

size_t seleznev::CompositeShape::getCount() const
{
  return count_;
}

double seleznev::CompositeShape::getMaxCoordinate(const double centreCoordinate, const double length) const
{
  return centreCoordinate + length / 2;
}

double seleznev::CompositeShape::getMinCoordinate(const double centreCoordinate, const double length) const
{
  return centreCoordinate - length / 2;
}

void seleznev::CompositeShape::copyArrayContent(const std::unique_ptr<std::shared_ptr<Shape>[]>& array)
{
  for(size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i] = array[i];
  }
}

void seleznev::CompositeShape::rotate(double angle)
{
  const point_t rect = getFrameRect().pos;
  
  const double cosinus = cos(angle * M_PI / 180);
  const double sinus = sin(angle * M_PI / 180);

  for (size_t i = 0; i < count_; i++)
  {
    const double dx = arrayOfShapes_[i]->getFrameRect().pos.x - rect.x;
    const double dy = arrayOfShapes_[i]->getFrameRect().pos.y - rect.y;
    const double newX = rect.x + dx * cosinus - dy * sinus;
    const double newY = rect.y + dx * sinus + dy * cosinus;

    arrayOfShapes_[i]->move({newX, newY});
    arrayOfShapes_[i]->rotate(angle);
  }
}
