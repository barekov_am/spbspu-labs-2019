#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"
#include "circle.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(partitionTests)

BOOST_AUTO_TEST_CASE(testPartition)
{
  seleznev::CompositeShape compShape;
  seleznev::Circle circle1(5, {0, 10});
  seleznev::Circle circle2(10, {0, 20});
  seleznev::Rectangle rectangle1({8, 6, {20, 10}});
  seleznev::Rectangle rectangle2({11, 2, {100, 100}});

  compShape.add(std::make_shared<seleznev::Circle>(circle1));
  compShape.add(std::make_shared<seleznev::Rectangle>(rectangle1));
  compShape.add(std::make_shared<seleznev::Rectangle>(rectangle2));
  compShape.add(std::make_shared<seleznev::Circle>(circle2));

  seleznev::Matrix matrix = seleznev::partition(compShape);

  BOOST_CHECK_EQUAL(matrix[0][0], compShape[0]);
  BOOST_CHECK_EQUAL(matrix[0][1], compShape[1]);
  BOOST_CHECK_EQUAL(matrix[0][2], compShape[2]);
  BOOST_CHECK_EQUAL(matrix[1][0], compShape[3]);
}

BOOST_AUTO_TEST_CASE(testIntersect)
{
  seleznev::Rectangle rectangle({10.1, 11, {0, 0}});
  seleznev::Circle circle(10.1, {0, 0});

  std::shared_ptr<seleznev::Rectangle> rectPtr = std::make_shared<seleznev::Rectangle>(rectangle);
  std::shared_ptr<seleznev::Circle> circlePtr = std::make_shared<seleznev::Circle>(circle);

  BOOST_CHECK_EQUAL(seleznev::intersect(rectPtr, circlePtr), true);
  rectPtr->move(100, 100);
  BOOST_CHECK_EQUAL(seleznev::intersect(rectPtr, circlePtr), false);
}

BOOST_AUTO_TEST_SUITE_END()
