#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "base-types.hpp"
#include "shape.hpp"

namespace seleznev
{
  class Rectangle : public Shape
  {
    public:
      Rectangle(const double width, const double height, const point_t& pos);
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const point_t& point) override;
      void move(double dx, double dy) override;
      std::string getName() const override;
      void scale(double coefficient) override;
      void rotate(double angle) override;

    private:
      point_t centre_;
      double width_;
      double height_;
      double angle_;
  };
}

#endif
