#include "polygon.hpp"

#include <cmath>
#include <stdexcept>
#include <algorithm>

seleznev::Polygon::Polygon(const point_t* points, const size_t pointsCount):
  pointsCount_(pointsCount),
  points_(getCopiedPoints(points, pointsCount)),
  centre_(getCentre()),
  angle_(0)
{
  if (points == nullptr)
  {
    throw std::out_of_range("Null pointer");
  }

  if (pointsCount < 3)
  {
    delete [] points_;

    throw std::out_of_range("Points count is out of range");
  }

  if (getArea() == 0)
  {
    delete [] points_;

    throw std::out_of_range("Area is out of range");
  }

  if (!isConvex())
  {
    delete [] points_;

    throw std::out_of_range("Polygon is not convex!");
  }
}

seleznev::Polygon::Polygon(const Polygon& source):
  pointsCount_(source.pointsCount_),
  points_(getCopiedPoints(source.points_, pointsCount_)),
  centre_(source.centre_)
{
}

seleznev::Polygon::Polygon(Polygon&& source):
  pointsCount_(source.pointsCount_),
  points_(source.points_),
  centre_(source.centre_)
{
  source.pointsCount_ = 0;
  source.points_ = nullptr;
  source.centre_ = {0, 0};
}

seleznev::Polygon::~Polygon()
{
  delete [] points_;
}

seleznev::Polygon& seleznev::Polygon::operator =(const Polygon& right)
{
  if (this == &right)
  {
    return *this;
  }

  pointsCount_ = right.pointsCount_;
  centre_ = right.centre_;

  delete [] points_;

  points_ = getCopiedPoints(right.points_, pointsCount_);

  return *this;
}

seleznev::Polygon& seleznev::Polygon::operator =(Polygon&& right)
{
  if (this == &right)
  {
    return *this;
  }

  delete [] points_;

  pointsCount_ = right.pointsCount_;
  points_ = right.points_;
  centre_ = right.centre_;

  right.pointsCount_ = 0;
  right.points_ = nullptr;
  right.centre_ = {0, 0};

  return *this;
}

double seleznev::Polygon::getArea() const
{
  double firstMember = 0;
  double secondMember = 0;

  point_t lastPoint = points_[pointsCount_ - 1];

  for (size_t i = 0; i < pointsCount_ - 1; i++)
  {
    firstMember += points_[i].x * points_[i + 1].y;
    secondMember += points_[i + 1].x * points_[i].y;
  }

  double doubleArea = firstMember + lastPoint.x * points_[0].y - secondMember - points_[0].x * lastPoint.y;

  return 0.5 * std::abs(doubleArea);
}

seleznev::rectangle_t seleznev::Polygon::getFrameRect() const
{
  double minX = points_[0].x;
  double maxX = points_[0].x;
  double minY = points_[0].y;
  double maxY = points_[0].y;

  for (size_t i = 1; i < pointsCount_; i++)
  {
    minX = std::min(minX, points_[i].x);
    maxX = std::max(maxX, points_[i].x);
    minY = std::min(minY, points_[i].y);
    maxY = std::max(maxY, points_[i].y);
  }

  double width = (maxY - minY);
  double height = (maxX - minX);
  point_t rectCentre = {minX + (width / 2), minY + (height / 2)};

  return {width, height, rectCentre};
}

void seleznev::Polygon::move(const seleznev::point_t& point)
{
  move(point.x - centre_.x, point.y - centre_.y);
}

void seleznev::Polygon::move(const double dx, const double dy)
{
  centre_.x += dx;
  centre_.y += dy;

  for (size_t i = 0; i < pointsCount_; i++)
  {
    points_[i].x += dx;
    points_[i].y += dy;
  }
}

std::string seleznev::Polygon::getName() const
{
  return "Polygon";
}

void seleznev::Polygon::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::out_of_range("Wrong coefficient");
  }

  for (size_t i = 0; i < pointsCount_; i++)
  {
    points_[i].y = coefficient * points_[i].y - (coefficient - 1) * centre_.y;
    points_[i].x = coefficient * points_[i].x - (coefficient - 1) * centre_.x;
  }
}

seleznev::point_t seleznev::Polygon::getCentre() const
{
  point_t centre = {0, 0};

  if (points_ == nullptr)
  {
    return centre;
  }

  for (size_t i = 0; i < pointsCount_; i++)
  {
    centre.x += points_[i].x / pointsCount_;
    centre.y += points_[i].y / pointsCount_;
  }

  return centre;
}

bool seleznev::Polygon::isConvex() const
{
  double oldAngle = 0;
  double anglesSum = 0;
  double angleDiff = 0;
  int direction = 0;

  point_t firstPoint = points_[pointsCount_ - 2];
  point_t secondPoint = points_[pointsCount_ - 1];

  double newAngle = getAngle(firstPoint, secondPoint);

  for (size_t i = 0; i < pointsCount_; i++)
  {
    firstPoint = secondPoint;
    secondPoint = points_[i];
    oldAngle = newAngle;

    newAngle = getAngle(firstPoint, secondPoint);

    angleDiff = newAngle - oldAngle;

    angleDiff = normalizeAngle(angleDiff);

    if (i == 0)
    {
      direction = angleDiff > 0 ? 1 : -1;
    }
    else if (direction * angleDiff <= 0)
    {
      return false;
    }

    anglesSum += angleDiff;
  }

  return std::abs(std::round(anglesSum / (2 * M_PI))) == 1;
}

double seleznev::Polygon::getAngle(const seleznev::point_t& start, const seleznev::point_t& end) const
{
  double yLength = end.y - start.y;
  double xLength = end.x - start.x;

  return std::atan2(yLength, xLength);
}

double seleznev::Polygon::normalizeAngle(const double radAngle) const
{
  double normalizedAngle = radAngle;

  if (radAngle <= -M_PI)
  {
    normalizedAngle += 2 * M_PI;
  }
  else if (radAngle > M_PI)
  {
    normalizedAngle -= 2 * M_PI;
  }

  return normalizedAngle;
}

seleznev::point_t* seleznev::Polygon::getCopiedPoints(const point_t* sourcePoints, const size_t sourcePointsCount)
{
  if (sourcePoints == nullptr)
  {
    return nullptr;
  }

  point_t* copiedPoints = new point_t[sourcePointsCount];

  for (size_t i = 0; i < sourcePointsCount; i++)
  {
    copiedPoints[i] = sourcePoints[i];
  }

  return copiedPoints;
}

void seleznev::Polygon::rotate(double angle)
{
  for (size_t i = 0; i < pointsCount_; i++)
  {
    points_[i] = rotatePoint(points_[i], angle);
  }
}

seleznev::point_t seleznev::Polygon::rotatePoint(point_t& point, double angle)
{
  const double rad = angle * M_PI / 180;
  const double sinus = sin(rad);
  const double cosinus = cos(rad);

  double x = point.x;
  double y = point.y;

  x = centre_.x + (x - centre_.x) * cosinus - (y - centre_.y) * sinus;
  y = centre_.y + (y - centre_.y) * cosinus + (x - centre_.x) * sinus;

  return {x, y};
}
