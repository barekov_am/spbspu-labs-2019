#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "base-types.hpp"
#include "shape.hpp"

namespace seleznev
{
  class Circle : public Shape
  {
    public:
      Circle(const double radius, const point_t& pos);
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const point_t& point) override;
      void move(double dx, double dy) override;
      std::string getName() const override;
      void scale(double coefficient) override;
      void rotate(double) override;

    private:
      point_t centre_;
      double radius_;
  };
}

#endif
