#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "base-types.hpp"
#include "shape.hpp"

namespace seleznev
{
  class Polygon : public Shape
  {
    public:
      Polygon(const point_t* points, const size_t pointsCount);
      Polygon(const Polygon& source);
      Polygon(Polygon&& source);
      virtual ~Polygon();

      Polygon& operator =(const Polygon& right);
      Polygon& operator =(Polygon&& right);

      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const point_t& point) override;
      void move(double dx, double dy) override;
      std::string getName() const override;
      void scale(double coefficient) override;
      void rotate(double angle) override;

    private:
      size_t pointsCount_;
      point_t* points_;
      point_t centre_;
      double angle_;

      point_t getCentre() const;
      bool isConvex() const;
      double getAngle(const point_t& start, const point_t& end) const;
      double normalizeAngle(const double radAngle) const;
      point_t* getCopiedPoints(const point_t* sourcePoints, const size_t sourcePointsCount);
      point_t rotatePoint(point_t& point, double angle);
  };
}

#endif
