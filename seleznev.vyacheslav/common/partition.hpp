#ifndef SPLIT_HPP
#define SPLIT_HPP

#include <memory>

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace seleznev
{
  bool intersect(const std::shared_ptr<Shape> shapeA, const std::shared_ptr<Shape> shapeB);
  Matrix partition(const CompositeShape& compShape);
}

#endif
