#include <boost/test/auto_unit_test.hpp>

#include <memory>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

const double INACCURACY = 0.00001;

BOOST_AUTO_TEST_SUITE(compositeShapeTests)

BOOST_AUTO_TEST_CASE(checkAreaAfterMovingDxDy)
{
  seleznev::Circle circle(10, {-7, 5});
  seleznev::Rectangle rectangle(10, 8, {-7, 5});
  seleznev::Triangle triangle({44, 23}, {12,11}, {76,30});

  const size_t polygonPointsCount = 4;
  const seleznev::point_t polygonPoints[] = {{3, 4}, {5, 11}, {12, 8}, {9, 5}};
  seleznev::Polygon polygon(polygonPoints, polygonPointsCount);

  seleznev::CompositeShape container;

  container.add(std::make_shared<seleznev::Circle>(circle));
  container.add(std::make_shared<seleznev::Rectangle>(rectangle));
  container.add(std::make_shared<seleznev::Triangle>(triangle));
  container.add(std::make_shared<seleznev::Polygon>(polygon));

  const double area = container.getArea();

  container.move(50, 20);

  BOOST_CHECK_CLOSE(container.getArea(), area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkAreaAfterMovingToCentrePoint)
{
  seleznev::Circle circle(10, {-7, 5});
  seleznev::Rectangle rectangle(10, 8, {-7, 5});
  seleznev::Triangle triangle({44, 23}, {12,11}, {76,30});

  const size_t polygonPointsCount = 4;
  const seleznev::point_t polygonPoints[] = {{3, 4}, {5, 11}, {12, 8}, {9, 5}};
  seleznev::Polygon polygon(polygonPoints, polygonPointsCount);

  seleznev::CompositeShape container;

  container.add(std::make_shared<seleznev::Circle>(circle));
  container.add(std::make_shared<seleznev::Rectangle>(rectangle));
  container.add(std::make_shared<seleznev::Triangle>(triangle));
  container.add(std::make_shared<seleznev::Polygon>(polygon));

  const double area = container.getArea();

  container.move({50, 20});

  BOOST_CHECK_CLOSE(container.getArea(), area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkFrameRectAfterMovingDxDy)
{
  seleznev::Circle circle(10, {-7, 5});
  seleznev::Rectangle rectangle(10, 8, {-7, 5});
  seleznev::Triangle triangle({44, 23}, {12,11}, {76,30});

  const size_t polygonPointsCount = 4;
  const seleznev::point_t polygonPoints[] = {{3, 4}, {5, 11}, {12, 8}, {9, 5}};
  seleznev::Polygon polygon(polygonPoints, polygonPointsCount);

  seleznev::CompositeShape container;

  container.add(std::make_shared<seleznev::Circle>(circle));
  container.add(std::make_shared<seleznev::Rectangle>(rectangle));
  container.add(std::make_shared<seleznev::Triangle>(triangle));
  container.add(std::make_shared<seleznev::Polygon>(polygon));

  seleznev::rectangle_t rectBefore = container.getFrameRect();

  container.move(50, 20);

  seleznev::rectangle_t rectAfter = container.getFrameRect();

  BOOST_CHECK_CLOSE(rectBefore.width, rectAfter.width, INACCURACY);
  BOOST_CHECK_CLOSE(rectBefore.height, rectAfter.height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkFrameRectAfterMovingToCentre)
{
  seleznev::Circle circle(10, {-7, 5});
  seleznev::Rectangle rectangle(10, 8, {-7, 5});
  seleznev::Triangle triangle({44, 23}, {12,11}, {76,30});

  const size_t polygonPointsCount = 4;
  const seleznev::point_t polygonPoints[] = {{3, 4}, {5, 11}, {12, 8}, {9, 5}};
  seleznev::Polygon polygon(polygonPoints, polygonPointsCount);

  seleznev::CompositeShape container;

  container.add(std::make_shared<seleznev::Circle>(circle));
  container.add(std::make_shared<seleznev::Rectangle>(rectangle));
  container.add(std::make_shared<seleznev::Triangle>(triangle));
  container.add(std::make_shared<seleznev::Polygon>(polygon));

  seleznev::rectangle_t rectBefore = container.getFrameRect();

  container.move({50, 20});

  seleznev::rectangle_t rectAfter = container.getFrameRect();

  BOOST_CHECK_CLOSE(rectBefore.width, rectAfter.width, INACCURACY);
  BOOST_CHECK_CLOSE(rectBefore.height, rectAfter.height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkScaling)
{
  seleznev::Circle circle(10, {-7, 5});
  seleznev::Rectangle rectangle(10, 8, {-7, 5});
  seleznev::Triangle triangle({44, 23}, {12,11}, {76,30});

  const size_t polygonPointsCount = 4;
  const seleznev::point_t polygonPoints[] = {{3, 4}, {5, 11}, {12, 8}, {9, 5}};
  seleznev::Polygon polygon(polygonPoints, polygonPointsCount);

  seleznev::CompositeShape container;

  container.add(std::make_shared<seleznev::Circle>(circle));
  container.add(std::make_shared<seleznev::Rectangle>(rectangle));
  container.add(std::make_shared<seleznev::Triangle>(triangle));
  container.add(std::make_shared<seleznev::Polygon>(polygon));

  const double area = container.getArea();

  const double coefficient = 7;

  container.scale(coefficient);

  const double newArea = container.getArea();
  
  seleznev::point_t centre = container.getFrameRect().pos;

  BOOST_CHECK_CLOSE(newArea, coefficient * coefficient * area, INACCURACY);
  BOOST_CHECK_CLOSE(centre.x, container.getFrameRect().pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(centre.y, container.getFrameRect().pos.y, INACCURACY);
}

BOOST_AUTO_TEST_CASE(addNullptr)
{
  std::shared_ptr<seleznev::Shape> shape = nullptr;

  seleznev::CompositeShape container;

  BOOST_CHECK_THROW(container.add(shape), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(invalidIndex)
{
  seleznev::CompositeShape container;

  BOOST_CHECK_THROW(container[10], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkScaleCoefficient)
{
  seleznev::CompositeShape container;

  BOOST_CHECK_THROW(container.scale(-4), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkRotation)
{
  seleznev::Rectangle testRectangle(4, 2, {5, 5});
  seleznev::Circle testCircle(1, {5, 5});

  seleznev::CompositeShape compositeTest;

  compositeTest.add(std::make_shared<seleznev::Rectangle>(testRectangle));
  compositeTest.add(std::make_shared<seleznev::Circle>(testCircle));

  const double testArea = compositeTest.getArea();
  const seleznev::rectangle_t testFrameRect = compositeTest.getFrameRect();
  const double angle = 180;

  compositeTest.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, compositeTest.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(testFrameRect.height, compositeTest.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(testArea, compositeTest.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
