#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "base-types.hpp"
#include "shape.hpp"

namespace seleznev
{
  class Triangle : public Shape
  {
    public:
      Triangle(const point_t& pA, const point_t& pB, const point_t& pC);
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const point_t& point) override;
      void move(double dx, double dy) override;
      std::string getName() const override;
      void scale(double coefficient) override;
      void rotate(double) override;

    private:
      point_t pointA_;
      point_t pointB_;
      point_t pointC_;
      double angle_;

      point_t rotatePoint(point_t point, point_t cenPos, double angle);
  };
}

#endif
