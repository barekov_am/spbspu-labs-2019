#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>

#include "shape.hpp"

namespace seleznev
{
  class CompositeShape : public Shape
  {
    public:
      CompositeShape();
      std::shared_ptr<Shape> operator [](size_t i) const;

      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const double dx, const double dy) override;
      void move(const seleznev::point_t& point) override;
      std::string getName() const override;
      void scale(double coefficient) override;
      void add(std::shared_ptr<Shape> shape);
      size_t getCount() const;
      void rotate(double angle) override;

    private:
      size_t count_;
      std::unique_ptr<std::shared_ptr<Shape>[]> arrayOfShapes_;
      double angle_;

      double getMaxCoordinate(const double centreCoordinate, const double length) const;
      double getMinCoordinate(const double centreCoordinate, const double length) const;
      void copyArrayContent(const std::unique_ptr<std::shared_ptr<Shape>[]>& array);
  };
}

#endif
