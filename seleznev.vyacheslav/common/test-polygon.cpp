#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>

#include "polygon.hpp"

const double INACCURACY = 0.00001;

BOOST_AUTO_TEST_SUITE(polygonTests)

BOOST_AUTO_TEST_CASE(checkMoving)
{
  const size_t polygonPointsCount = 4;
  const seleznev::point_t polygonPoints[] = {{3, 4}, {5, 11}, {12, 8}, {9, 5}};
  seleznev::Polygon polygonCheck(polygonPoints, polygonPointsCount);
  const double widthBefore = polygonCheck.getFrameRect().width;
  const double heightBefore = polygonCheck.getFrameRect().height;

  polygonCheck.move(65, 33);

  BOOST_CHECK_CLOSE(widthBefore, polygonCheck.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, polygonCheck.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkCentreMoving)
{
  const size_t polygonPointsCount = 4;
  const seleznev::point_t polygonPoints[] = {{3, 4}, {5, 11}, {12, 8}, {9, 5}};
  seleznev::Polygon polygonCheck(polygonPoints, polygonPointsCount);
  const double widthBefore = polygonCheck.getFrameRect().width;
  const double heightBefore = polygonCheck.getFrameRect().height;

  polygonCheck.move({65, 33});

  BOOST_CHECK_CLOSE(widthBefore, polygonCheck.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, polygonCheck.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkArea)
{
  const size_t polygonPointsCount = 4;
  const seleznev::point_t polygonPoints[] = {{3, 4}, {5, 11}, {12, 8}, {9, 5}};
  seleznev::Polygon polygonCheck(polygonPoints, polygonPointsCount);
  const double area = polygonCheck.getArea();

  polygonCheck.move(43, -11);

  BOOST_CHECK_CLOSE(polygonCheck.getArea(), area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkScaling)
{
  const size_t polygonPointsCount = 4;
  const seleznev::point_t polygonPoints[] = {{3, 4}, {5, 11}, {12, 8}, {9, 5}};
  seleznev::Polygon polygonCheck(polygonPoints, polygonPointsCount);
  const double area = polygonCheck.getArea();
  const double coefficient = 4.1;

  polygonCheck.scale(coefficient);

  BOOST_CHECK_CLOSE(polygonCheck.getArea(), coefficient * coefficient * area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkInvalidPointer)
{
  seleznev::point_t *polygonPoints = nullptr;
  BOOST_CHECK_THROW(seleznev::Polygon polygonCheck(polygonPoints, 1), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkInvalidPointsCount)
{
  const seleznev::point_t polygonPoints[] = {{10, 10}, {20, 20}};
  BOOST_CHECK_THROW(seleznev::Polygon polygonCheck(polygonPoints, 2), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkZeroArea)
{
  const seleznev::point_t polygonPoints[] = {{0, 0}, {0, 0}, {0, 0}};
  BOOST_CHECK_THROW(seleznev::Polygon polygonCheck(polygonPoints, 3), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkNotConvexPolygon)
{
  const size_t polygonPointsCount = 5;
  const seleznev::point_t polygonPoints[] = {{3, 4}, {5, 11}, {12, 8}, {9, 5}, {5, 6}};
  BOOST_CHECK_THROW(seleznev::Polygon polygonCheck(polygonPoints, polygonPointsCount), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkScaleCoefficient)
{
  const size_t polygonPointsCount = 4;
  const seleznev::point_t polygonPoints[] = {{3, 4}, {5, 11}, {12, 8}, {9, 5}};
  seleznev::Polygon polygonCheck(polygonPoints, polygonPointsCount);

  BOOST_CHECK_THROW(polygonCheck.scale(-9), std::out_of_range);
}
BOOST_AUTO_TEST_CASE(checkRotation)
{
  const seleznev::point_t polygonPoints[] = {{20, 10}, {20, -10}, {-10, -10}, {-10, 10}};
  size_t size = sizeof(polygonPoints) / sizeof(polygonPoints[0]);
  seleznev::Polygon polygonCheck(polygonPoints, size);
  const double testArea = polygonCheck.getArea();
  const double widthBefore = polygonCheck.getFrameRect().width;
  const double heightBefore = polygonCheck.getFrameRect().height;
  const seleznev::rectangle_t testFrameRect = polygonCheck.getFrameRect();
  const double angle = 180;

  polygonCheck.rotate(angle);

  BOOST_CHECK_CLOSE(widthBefore, polygonCheck.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, polygonCheck.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(testArea, polygonCheck.getArea(), INACCURACY);

  BOOST_CHECK_CLOSE(testFrameRect.pos.x, polygonCheck.getFrameRect().pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(testFrameRect.pos.y, polygonCheck.getFrameRect().pos.y, INACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
