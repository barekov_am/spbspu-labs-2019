#include "rectangle.hpp"

#include <cmath>
#include <stdexcept>

seleznev::Rectangle::Rectangle(const double w, const double h, const seleznev::point_t& pos):
  centre_(pos),
  width_(w),
  height_(h),
  angle_(0)
{
  if ((height_ <= 0) || (width_ <= 0))
  {
    throw std::out_of_range("Height/width is out of range");
  }
}

double seleznev::Rectangle::getArea() const
{
  return width_ * height_;
}

seleznev::rectangle_t seleznev::Rectangle::getFrameRect() const
{
  const double sinus = abs(sin(angle_ * M_PI / 180));
  const double cosinus = abs(cos(angle_ * M_PI / 180));

  return {
    std::abs(width_ * cosinus) + std::abs(height_ * sinus),
    std::abs(width_ * sinus) + std::abs(height_ * cosinus),
    centre_
  };
}

void seleznev::Rectangle::move(const seleznev::point_t& point)
{
  centre_ = point;
}

void seleznev::Rectangle::move(const double dx, const double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

std::string seleznev::Rectangle::getName() const
{
  return "Rectangle";
}

void seleznev::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::out_of_range("Coefficient is out of range");
  }

  width_ *= coefficient;
  height_ *= coefficient;
}

void seleznev::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
