#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>

#include "triangle.hpp"

const double INACCURACY = 0.00001;

BOOST_AUTO_TEST_SUITE(triangleTests)

BOOST_AUTO_TEST_CASE(checkMoving)
{
  seleznev::Triangle triangleCheck({44, 23}, {12,11}, {76,30});
  const double widthBefore = triangleCheck.getFrameRect().width;
  const double heightBefore = triangleCheck.getFrameRect().height;

  triangleCheck.move(20, 30);

  BOOST_CHECK_CLOSE(widthBefore, triangleCheck.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, triangleCheck.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkCentreMoving)
{
  seleznev::Triangle triangleCheck({44, 23}, {12,11}, {76,30});
  const double widthBefore = triangleCheck.getFrameRect().width;
  const double heightBefore = triangleCheck.getFrameRect().height;

  triangleCheck.move({-10.3, -15.5});

  BOOST_CHECK_CLOSE(widthBefore, triangleCheck.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, triangleCheck.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkArea)
{
  seleznev::Triangle triangleCheck({-7, 10}, {11, 44}, {5, -7});
  const double area = triangleCheck.getArea();

  triangleCheck.move(6, -11);

  BOOST_CHECK_CLOSE(triangleCheck.getArea(), area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkScaling)
{
  seleznev::Triangle triangleCheck({-7, 10}, {11, 44}, {5, -7});
  const double area = triangleCheck.getArea();
  const double coefficient = 4.1;

  triangleCheck.scale(coefficient);

  BOOST_CHECK_CLOSE(triangleCheck.getArea(), coefficient * coefficient * area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkZeroArea)
{
  BOOST_CHECK_THROW(seleznev::Triangle triangleCheck({-7, -7}, {-7, -7}, {-7, -7}), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkScaleCoefficient)
{
  seleznev::Triangle triangleCheck({-7, 10}, {11, 44}, {5, -7});

  BOOST_CHECK_THROW(triangleCheck.scale(-9), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkRotation)
{
  seleznev::Triangle triangleCheck({13,20}, {15, 22}, {12, 12});
  const double areaBefore = triangleCheck.getArea();
  const double widthBefore = triangleCheck.getFrameRect().width;
  const double heightBefore = triangleCheck.getFrameRect().height;
  const double angle = 180;

  triangleCheck.rotate(angle);

  BOOST_CHECK_CLOSE(widthBefore, triangleCheck.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, triangleCheck.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, triangleCheck.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
