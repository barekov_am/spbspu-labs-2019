#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"
#include "circle.hpp"
#include "partition.hpp"

const double INACCURACY = 0.0001;

BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(invalidIndexException)
{
  seleznev::Rectangle rectangle(10.1, 11, {0, 0});
  seleznev::Circle circle(10.1, {0, 0});
  seleznev::CompositeShape compositeShape;

  compositeShape.add(std::make_shared<seleznev::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<seleznev::Circle>(circle));

  seleznev::Matrix matrix = seleznev::partition(compositeShape);

  BOOST_CHECK_THROW(matrix[2][0], std::out_of_range);
  BOOST_CHECK_THROW(matrix[-1][-1], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(testCopyConstructor)
{
  seleznev::Rectangle rectangle({10.1, 11, {0, 0}});
  seleznev::Circle circle(10.1, {0, 0});
  seleznev::CompositeShape compositeShape;

  compositeShape.add(std::make_shared<seleznev::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<seleznev::Circle>(circle));

  seleznev::Matrix matrix = seleznev::partition(compositeShape);
  seleznev::Matrix testMatrix(matrix);

  BOOST_CHECK_EQUAL(matrix[0][0], testMatrix[0][0]);
  BOOST_CHECK_EQUAL(matrix[1][0], testMatrix[1][0]);
}

BOOST_AUTO_TEST_CASE(testMoveConstructor)
{
  seleznev::Rectangle rectangle(10.1, 11, {0, 0});
  seleznev::Circle circle(0.1, {0, 0});
  seleznev::CompositeShape compositeShape;

  compositeShape.add(std::make_shared<seleznev::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<seleznev::Circle>(circle));

  seleznev::Matrix matrix = seleznev::partition(compositeShape);
  std::shared_ptr<seleznev::Shape> shapeA = matrix[0][0];
  std::shared_ptr<seleznev::Shape> shapeB = matrix[1][0];
  seleznev::Matrix testMatrix(std::move(matrix));

  BOOST_CHECK_EQUAL(shapeA, testMatrix[0][0]);
  BOOST_CHECK_EQUAL(shapeB, testMatrix[1][0]);
}

BOOST_AUTO_TEST_CASE(testCopyOperatorOfAssignment)
{
  seleznev::Rectangle rectangle(10.1, 11, {0, 0});
  seleznev::Circle circle(0.1, {0, 0});
  seleznev::CompositeShape compositeShape;

  compositeShape.add(std::make_shared<seleznev::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<seleznev::Circle>(circle));

  seleznev::Matrix matrix = seleznev::partition(compositeShape);
  seleznev::Matrix testMatrix = matrix;

  BOOST_CHECK_EQUAL(matrix[0][0], testMatrix[0][0]);
  BOOST_CHECK_EQUAL(matrix[1][0], testMatrix[1][0]);
}

BOOST_AUTO_TEST_CASE(testMoveOperatorOfAssignment)
{
  seleznev::Rectangle rectangle(10.1, 11, {0, 0});
  seleznev::Circle circle(0.1, {0, 0});
  seleznev::CompositeShape compositeShape;

  compositeShape.add(std::make_shared<seleznev::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<seleznev::Circle>(circle));

  seleznev::Matrix matrix = seleznev::partition(compositeShape);
  std::shared_ptr<seleznev::Shape> shapeA = matrix[0][0];
  std::shared_ptr<seleznev::Shape> shapeB = matrix[1][0];
  seleznev::Matrix testMatrix = std::move(matrix);

  BOOST_CHECK_EQUAL(shapeA, testMatrix[0][0]);
  BOOST_CHECK_EQUAL(shapeB, testMatrix[1][0]);
}

BOOST_AUTO_TEST_CASE(testIndexOperator)
{
  seleznev::Rectangle rectangle(10.1, 11, {0, 0});
  seleznev::Circle circle(0.1, {0, 0});
  seleznev::CompositeShape compositeShape;

  compositeShape.add(std::make_shared<seleznev::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<seleznev::Circle>(circle));

  seleznev::Matrix matrix = seleznev::partition(compositeShape);
  std::shared_ptr<seleznev::Shape> shapeA = matrix[0][0];
  std::shared_ptr<seleznev::Shape> shapeB = matrix[1][0];

  BOOST_CHECK_CLOSE(shapeA->getArea(), rectangle.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE(shapeB->getArea(), circle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
