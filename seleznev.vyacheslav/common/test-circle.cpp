#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>

#include "circle.hpp"

const double INACCURACY = 0.00001;

BOOST_AUTO_TEST_SUITE(circleTests)

BOOST_AUTO_TEST_CASE(checkMoving)
{
  seleznev::Circle circleCheck(10, {-7, 5});
  const double widthBefore = circleCheck.getFrameRect().width;
  const double heightBefore = circleCheck.getFrameRect().height;

  circleCheck.move(65, 33);

  BOOST_CHECK_CLOSE(heightBefore, circleCheck.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(widthBefore, circleCheck.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkCentreMoving)
{
  seleznev::Circle circleCheck(10, {-7, 5});
  const double widthBefore = circleCheck.getFrameRect().width;
  const double heightBefore = circleCheck.getFrameRect().height;

  circleCheck.move({65, 33});

  BOOST_CHECK_CLOSE(heightBefore, circleCheck.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(widthBefore, circleCheck.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkArea)
{
  seleznev::Circle circleCheck(10, {-7, 5});
  const double area = circleCheck.getArea();

  circleCheck.move(43, -11);

  BOOST_CHECK_CLOSE(circleCheck.getArea(), area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkScaling)
{
  seleznev::Circle circleCheck(10, {-7, 5});
  const double area = circleCheck.getArea();
  const double coefficient = 7.8;

  circleCheck.scale(coefficient);

  BOOST_CHECK_CLOSE(circleCheck.getArea(), coefficient * coefficient * area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkInvalidRadius)
{
  BOOST_CHECK_THROW(seleznev::Circle circleCheck(-10, { -7, 5 }), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkScaleCoefficient)
{
  seleznev::Circle circleCheck(10, {-7, 5});

  BOOST_CHECK_THROW(circleCheck.scale(-4), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkRotation)
{
  seleznev::Circle circleCheck(2.5, {5.9, 3.0});
  const double testArea = circleCheck.getArea();
  const seleznev::rectangle_t testFrameRect = circleCheck.getFrameRect();
  const double angle = 90;

  circleCheck.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, circleCheck.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(testFrameRect.height, circleCheck.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(testArea, circleCheck.getArea(), INACCURACY);

  BOOST_CHECK_EQUAL(testFrameRect.pos.x, circleCheck.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, circleCheck.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
