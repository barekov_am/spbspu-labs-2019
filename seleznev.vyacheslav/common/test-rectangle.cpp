#include <boost/test/auto_unit_test.hpp>

#include <stdexcept>

#include "rectangle.hpp"

const double INACCURACY = 0.00001;

BOOST_AUTO_TEST_SUITE(rectangleTests)

BOOST_AUTO_TEST_CASE(checkMoving)
{
  seleznev::Rectangle rectangleCheck(10, 8, {-7, 5});
  const double widthBefore = rectangleCheck.getFrameRect().width;
  const double heightBefore = rectangleCheck.getFrameRect().height;

  rectangleCheck.move(16, -8);

  BOOST_CHECK_CLOSE(heightBefore, rectangleCheck.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(widthBefore, rectangleCheck.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkCentreMoving)
{
  seleznev::Rectangle rectangleCheck(10, 8, {-7, 5});
  const double widthBefore = rectangleCheck.getFrameRect().width;
  const double heightBefore = rectangleCheck.getFrameRect().height;

  rectangleCheck.move({16, -8});

  BOOST_CHECK_CLOSE(heightBefore, rectangleCheck.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(widthBefore, rectangleCheck.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkArea)
{
  seleznev::Rectangle rectangleCheck(10, 8, {-7, 5});
  const double area = rectangleCheck.getArea();

  rectangleCheck.move(15, 15);

  BOOST_CHECK_CLOSE(rectangleCheck.getArea(), area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkScaling)
{
  seleznev::Rectangle rectangleCheck(10, 8, {-7, 5});
  const double area = rectangleCheck.getArea();
  const double coefficient = 5.8;

  rectangleCheck.scale(coefficient);

  BOOST_CHECK_CLOSE(rectangleCheck.getArea(), coefficient * coefficient * area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkInvalidWidth)
{
  BOOST_CHECK_THROW(seleznev::Rectangle rectangleCheck(-10, 8, {-7, 5}), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkInvalidHeight)
{
  BOOST_CHECK_THROW(seleznev::Rectangle rectangleCheck(10, -8, {-7, 5}), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkScaleCoefficient)
{
  seleznev::Rectangle rectangleCheck(10, 8, {-7, 5});

  BOOST_CHECK_THROW(rectangleCheck.scale(-4), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkRotation)
{
  seleznev::Rectangle rectangleCheck(4.0, 2.0, {5.0, 5.0});
  const double testArea = rectangleCheck.getArea();
  const seleznev::rectangle_t testFrameRect = rectangleCheck.getFrameRect();
  const double angle = 90;

  rectangleCheck.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, rectangleCheck.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(testFrameRect.height, rectangleCheck.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(testArea, rectangleCheck.getArea(), INACCURACY);

  BOOST_CHECK_EQUAL(testFrameRect.pos.x, rectangleCheck.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, rectangleCheck.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
