#include "circle.hpp"

#include <cmath>
#include <stdexcept>

seleznev::Circle::Circle(const double radius, const point_t& pos):
  centre_(pos),
  radius_(radius)
{
  if (radius_ <= 0)
  {
    throw std::out_of_range("Radius is out of range");
  }
}

double seleznev::Circle::getArea() const
{
  return M_PI * pow(radius_, 2);
}

seleznev::rectangle_t seleznev::Circle::getFrameRect() const
{
  double diametr = 2 * radius_;

  return {diametr, diametr, centre_};
}

void seleznev::Circle::move(const point_t& point)
{
  centre_ = point;
}

void seleznev::Circle::move(const double dx, const double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

std::string seleznev::Circle::getName() const
{
  return "Circle";
}

void seleznev::Circle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::out_of_range("Coefficient is out of range");
  }

  radius_ *= coefficient;
}

void seleznev::Circle::rotate(double)
{
}
