#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "composite-shape.hpp"

#include <memory>

namespace seleznev
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix& matrix);
    Matrix(Matrix&& matrix);
    ~Matrix() = default;

    Matrix& operator=(const Matrix& matrix);
    Matrix& operator=(Matrix&& matrix);
    std::unique_ptr<std::shared_ptr<Shape>[]> operator[](const size_t row) const;

    size_t getRows() const;
    size_t getCols() const;
    size_t getSizeOfLayer(const size_t layer) const;

  private:
    std::unique_ptr<std::shared_ptr<Shape>[]> shapeMatrix_;
    size_t rows_;
    size_t columns_;

    void add(std::shared_ptr<Shape> shape, const size_t row, const size_t col);
    friend Matrix partition(const CompositeShape& compShape);
  };
}

#endif
