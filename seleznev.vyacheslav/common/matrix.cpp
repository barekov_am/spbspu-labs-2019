#include "matrix.hpp"

#include <stdexcept>
#include <algorithm>

seleznev::Matrix::Matrix():
  rows_(0),
  columns_(0)
{
}

seleznev::Matrix::Matrix(const Matrix& matrix):
  shapeMatrix_(std::make_unique<std::shared_ptr<Shape>[]>(matrix.rows_ * matrix.columns_)),
  rows_(matrix.rows_),
  columns_(matrix.columns_)
{
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    shapeMatrix_[i] = matrix.shapeMatrix_[i];
  }
}

seleznev::Matrix::Matrix(Matrix&& matrix):
  shapeMatrix_(std::move(matrix.shapeMatrix_)),
  rows_(matrix.rows_),
  columns_(matrix.columns_)
{
  matrix.rows_ = 0;
  matrix.columns_ = 0;
}

seleznev::Matrix& seleznev::Matrix::operator=(const Matrix& matrix)
{
  if (this != &matrix)
  {
    rows_ = matrix.rows_;
    columns_ = matrix.columns_;
    std::unique_ptr<std::shared_ptr<Shape>[]> tempMatrix(std::make_unique<std::shared_ptr<Shape>[]>(rows_ * columns_));
    for (size_t i = 0; i < (rows_ * columns_); i++)
    {
      tempMatrix[i] = matrix.shapeMatrix_[i];
    }
    shapeMatrix_.swap(tempMatrix);
  }
  return *this;
}

seleznev::Matrix& seleznev::Matrix::operator=(Matrix&& matrix)
{
  if (this != &matrix)
  {
    rows_ = matrix.rows_;
    columns_ = matrix.columns_;
    shapeMatrix_ = std::move(matrix.shapeMatrix_);
    matrix.rows_ = 0;
    matrix.columns_ = 0;
  }
  return *this;
}

std::unique_ptr<std::shared_ptr<seleznev::Shape>[]> seleznev::Matrix::operator[](const size_t row) const
{
  if (row >= rows_)
  {
    throw std::out_of_range("Invalid index!");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> tempMatrix(std::make_unique<std::shared_ptr<Shape>[]>(columns_));
  for (size_t i = 0; i < columns_; i++)
  {
    tempMatrix[i] = shapeMatrix_[row * columns_ + i];
  }

  return tempMatrix;
}

size_t seleznev::Matrix::getRows() const
{
  return rows_;
}

size_t seleznev::Matrix::getCols() const
{
  return columns_;
}

size_t seleznev::Matrix::getSizeOfLayer(const size_t layer) const
{
  if (layer >= rows_)
  {
    throw std::out_of_range("Invalid index!");
  }
  size_t layerSize = 0;
  for (size_t i = 0; i < columns_; i++)
  {
    if (shapeMatrix_[columns_ * layer + i] != nullptr)
    {
      layerSize++;
    }
  }

  return layerSize;
}

void seleznev::Matrix::add(std::shared_ptr<Shape> shape, const size_t row, const size_t col)
{
  size_t tempRows = std::max(rows_, row + 1);
  size_t tempCols = std::max(columns_, col + 1);
  std::unique_ptr<std::shared_ptr<Shape>[]> tempMatrix(std::make_unique<std::shared_ptr<Shape>[]>(tempRows * tempCols));
  for (size_t i = 0; i < rows_; i++)
  {
    for (size_t j = 0; j < columns_; j++)
    {
      tempMatrix[i * tempCols + j] = shapeMatrix_[i * columns_ + j];
    }
  }
  tempMatrix[row * tempCols + col] = shape;
  shapeMatrix_.swap(tempMatrix);
  rows_ = tempRows;
  columns_ = tempCols;
}
