#include <vector>

#include "print.hpp"

void thirdTask()
{
  std::vector<int> vector;
  std::vector<std::vector<int>::iterator> elementsToModify;

  int data = 0;

  while (std::cin >> data)
  {
    if (data == 0)
    {
      break;
    }

    vector.push_back(data);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Error reading data");
  }

  if (data != 0)
  {
    throw std::runtime_error("Last element must be 0");
  }

  if (vector.empty())
  {
    return;
  }

  for (auto it = vector.begin(); it != vector.end();)
  {
    if (vector.back() == 1)
    {
      if (*it % 2 == 0)
      {
        it = vector.erase(it);
      }
      else
      {
        ++it;
      }
    }
    else if (vector.back() == 2)
    {
      if (*it % 3 == 0)
      {
        it = vector.insert(++it, 3, 1);
      }
      else
      {
        ++it;
      }
    }
    else
    {
      ++it;
    }
  }

  print(vector);
}
