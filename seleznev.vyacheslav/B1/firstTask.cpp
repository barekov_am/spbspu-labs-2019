#include <forward_list>
#include <vector>
#include <stdexcept>

#include "sorting.hpp"
#include "print.hpp"

void firstTask(const std::string sortOrder)
{
  std::vector<int> vectorBr;

  int data = 0;
  
  while (std::cin >> data)
  {
    vectorBr.push_back(data);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Error reading data");
  }

  sequence seq;

  if (sortOrder == "ascending")
  {
    seq = ascending;
  }
  else if (sortOrder == "descending")
  {
    seq = descending;
  }
  else
  {
    throw std::runtime_error("Incorrect sort order");
  }

  if (vectorBr.empty())
  {
    return;
  }

  std::vector<int> vectorAt = vectorBr;

  std::forward_list<int> list(vectorBr.begin(), vectorBr.end());

  sortingByBrackets<std::vector<int>, int, size_t> byBrackets;
  sortingByMethodAt<std::vector<int>, int, size_t> byMethodAt;
  sortingByIterator<std::forward_list<int>, int, std::forward_list<int>::iterator> byIterator;

  sorting(vectorBr, byBrackets, seq);
  print(vectorBr);

  sorting(vectorAt, byMethodAt, seq);
  print(vectorAt);

  sorting(list, byIterator, seq);
  print(list);
 }
