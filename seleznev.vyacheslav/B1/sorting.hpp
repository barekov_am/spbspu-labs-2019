#ifndef SORT_HPP
#define SORT_HPP

#include "strategy.hpp"

enum sequence
{
  ascending,
  descending
};

template <typename TContainer, typename TStrategy>
void sorting(TContainer& container, TStrategy& strategy, sequence& seq)
{
  for (auto i = strategy.begin(container); i != strategy.end(container); i = strategy.next(i))
  {
    auto j = i;

    for (j = strategy.next(j); j != strategy.end(container); j = strategy.next(j))
    {
      bool canSwap = ((seq == ascending) && (strategy.getElem(container, i) > strategy.getElem(container, j))) ||
        ((seq == descending) && (strategy.getElem(container, i) < strategy.getElem(container, j)));

      if (canSwap)
      {
        auto temp = strategy.getElem(container, j);
        auto elem = strategy.getElem(container, i);
        strategy.setElem(container, j, elem);
        strategy.setElem(container, i, temp);
      }
    }
  }
}

#endif
