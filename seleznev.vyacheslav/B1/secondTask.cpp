#include <vector>
#include <fstream>
#include <stdexcept>
#include <memory>

#include "print.hpp"

void secondTask(const std::string flName)
{
  std::ifstream file(flName);

  if (!file.is_open())
  {
    throw std::out_of_range("File is not open");
  }

  size_t capacity = 2;
  std::unique_ptr<char[], decltype(free)*> arrayC(static_cast<char*>(calloc(capacity, 1)), free);

  if (!arrayC)
  {
    throw std::runtime_error("Could not allocate memory");
  }

  size_t count = 0;

  while (file)
  {
    file.read(&arrayC[count], capacity - count);
    count += file.gcount();

    if (count == capacity)
    {
      capacity *= 2;
      arrayC.reset(static_cast<char*>(realloc(arrayC.release(), capacity)));
      if (!arrayC)
      {
        file.close();
        throw std::runtime_error("Couldn not allocate memory");
      }
    }
  }

  if (!file.eof() && !file.fail())
  {
    throw std::runtime_error("Error reading data");
  }

  if (count == 0)
  {
    return;
  }

  file.close();

  std::vector<char> vector(&arrayC[0], &arrayC[count]);

  for (auto i = vector.begin(); i != vector.end(); ++i)
  {
    std::cout << *i;
  }
}
