#ifndef STRATEGY_HPP
#define STRATEGY_HPP

template <typename container, typename element, typename index>
struct sortingByBrackets
{
  static index begin(container&)
  {
    return 0;
  }

  static index end(container& cont)
  {
    return cont.size();
  }

  static index next(index& ind)
  {
    return ++ind;
  }

  static element getElem(container& cont, index& ind)
  {
    return cont[ind];
  }

  static void setElem(container& cont, index& ind, element& elem)
  {
    cont[ind] = elem;
  }
};

template <typename container, typename element, typename index>
struct sortingByMethodAt
{
  static index begin(container&)
  {
    return 0;
  }

  static index end(container& cont)
  {
    return cont.size();
  }

  static index next(index& ind)
  {
    return ++ind;
  }

  static element getElem(container& cont, index& ind)
  {
    return cont.at(ind);
  }

  static void setElem(container& cont, index& ind, element& elem)
  {
    cont.at(ind) = elem;
  }
};

template <typename container, typename element, typename index>
struct sortingByIterator
{
  static index begin(container& cont)
  {
    return cont.begin();
  }

  static index end(container& cont)
  {
    return cont.end();
  }

  static index next(index& ind)
  {
    return ++ind;
  }

  static element getElem(container&, index& ind)
  {
    return *ind;
  }

  static void setElem(container&, index& ind, element& elem)
  {
    *ind = elem;
  }
};

#endif
