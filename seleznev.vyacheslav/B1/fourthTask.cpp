#include <vector>
#include <random>

#include "sorting.hpp"
#include "print.hpp"

void fillRandom(double* array, int size)
{
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-1.0, 1.0);
  
  for (auto i = 0; i < size; ++i)
  {
    array[i] = dis(gen);
  }
}

void fourthTask(const int& size, const std::string sortOrder)
{
  sequence seq;

  if (sortOrder == "ascending")
  {
    seq = ascending;
  }
  else if (sortOrder == "descending")
  {
    seq = descending;
  }
  else
  {
    throw std::runtime_error("Incorrect sort order");
  }

  if (size <= 0)
  {
    throw std::invalid_argument("Size less than 0");
  }

  std::vector<double> vector(size);

  fillRandom(&vector[0], size);

  if (vector.empty())
  {
    return;
  }

  print(vector);

  sortingByMethodAt<std::vector<double>, double, size_t> byMethodAt;

  sorting(vector, byMethodAt, seq);

  print(vector);
}
