﻿#include <iostream>
#include <string>

void firstTask(const std::string sortOrder);
void secondTask(const std::string flName);
void thirdTask();
void fourthTask(const int& size, const std::string sortOrder);

int main(int argc, char* argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4))
    {
      std::cerr << "Invalid number of parameters" << std::endl;
      return 1;
    }

    const int taskId = std::stoi(argv[1]);

    switch (taskId)
    {
    case 1:
    {
      if (argc != 3)
      {
        std::cerr << "Incorrect number of arguments" << std::endl;
        return 1;
      }

      firstTask(argv[2]);
      return 0;
    }

    case 2:
    {
      if (argc != 3)
      {
        std::cerr << "Incorrect number of arguments" << std::endl;
        return 1;
      }

      secondTask(argv[2]);
      return 0;
    }

    case 3:
    {
      if (argc != 2)
      {
        std::cerr << "Incorrect number of arguments" << std::endl;
        return 1;
      }

      thirdTask();
      return 0;
    }

    case 4:
    {
      if (argc != 4)
      {
        std::cerr << "Incorrect number of arguments" << std::endl;
        return 1;
      }

      const int size = std::stoi(argv[3]);
      fourthTask(size, argv[2]);
      return 0;
    }

    default:
    {
      std::cerr << "Unknown command" << std::endl;
      return 1;
    }
    }
  }
  catch (const std::exception& err)
  {
    std::cerr << err.what() << std::endl;
    return 1;
  }

  return 0;
}
