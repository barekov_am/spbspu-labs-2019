#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void printShape(const seleznev::Shape* shape)
{
  if (shape == nullptr)
  {
    throw std::out_of_range("Null pointer");
  }

  std::cout << "Area = " << shape->getArea() << std::endl;

  seleznev::rectangle_t rect = shape->getFrameRect();

  std::cout << "Frame rect width = " << rect.width << " "
            << "height = " << rect.height << " "
            << "centreX = " << rect.pos.x << " "
            << "centreY = " << rect.pos.y << std::endl;
}

void demonstrateShapeCapabilities(seleznev::Shape* shape)
{
  if (shape == nullptr)
  {
    throw std::out_of_range("Null pointer");
  }

  std::cout << "=== " << shape->getName() << " ===" << std::endl;
  printShape(shape);
  std::cout << "Set new centre point" << std::endl;
  shape->move({100, 50});
  printShape(shape);
  std::cout << "Delta move" << std::endl;
  shape->move(10, 30);
  printShape(shape);
  std::cout << "Scale" << std::endl;
  shape->scale(4);
  printShape(shape);
  std::cout << "===" << std::endl;
}

int main()
{
  const seleznev::point_t circleCentre = {5, 8};
  const double circleRadius = 5;

  const seleznev::point_t rectangleCentre = {12, 30};
  const double rectangleWidth = 20;
  const double rectangleHeight = 10;

  const seleznev::point_t trianglePointA = {1, 20};
  const seleznev::point_t trianglePointB = {30, 4};
  const seleznev::point_t trianglePointC = {4, 50};

  const size_t polygonPointsCount = 4;
  const seleznev::point_t polygonPoints[] = {{3, 4}, {5, 11}, {12, 8}, {9, 5}};

  seleznev::Circle circle(circleRadius, circleCentre);
  seleznev::Rectangle rectangle(rectangleWidth, rectangleHeight, rectangleCentre);
  seleznev::Triangle triangle(trianglePointA, trianglePointB, trianglePointC);
  seleznev::Polygon polygon(polygonPoints, polygonPointsCount);

  demonstrateShapeCapabilities(&circle);
  demonstrateShapeCapabilities(&rectangle);
  demonstrateShapeCapabilities(&triangle);
  demonstrateShapeCapabilities(&polygon);

  return 0;
}
