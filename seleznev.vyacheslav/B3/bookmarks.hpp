#ifndef BOOKMARKS_HPP
#define BOOKMARKS_HPP

#include "phoneBook.hpp"

typedef struct
{
  std::string markName;
  container::iterator iter;
} markType;

class Bookmarks
{
public:
  Bookmarks() = default;
  Bookmarks(PhoneBook* phoneBook);

  void store(std::string& markName, std::string& newMarkName);
  void insertBefore(std::string& markName, std::string& number, std::string& name);
  void insertAfter(std::string& markName, std::string& number, std::string& name);
  void remove(std::string& markName);
  void show(std::string& markName);
  void move(std::string& markName, int& step);
  markType getCurrent();
  void replaceCurrent(markType& newCurrent);
  bool findBookmark(std::string& markName);
  void moveFirst(std::string& markName);
  void moveLast(std::string& markName);

private:
  std::list<markType> bookmarks_;
  PhoneBook* phoneBookPointer_;

  std::list<markType>::iterator getCurrentIterator();
};

#endif
