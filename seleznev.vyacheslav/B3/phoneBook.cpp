#include <iostream>

#include "phoneBook.hpp"

PhoneBook::PhoneBook():
  iterator_(phoneBook_.end())
{
}

void PhoneBook::viewCurrentRecord(iter& iter)
{
  if (!isEmpty() && iter != phoneBook_.end())
  {
    std::cout << iter->number << " " << iter->name << std::endl;
  }
  else
  {
    std::cout << "<EMPTY>\n";
  }
}

PhoneBook::iter PhoneBook::nextRecord()
{
  if (!isEmpty())
  {
    if (std::next(iterator_) == phoneBook_.end())
    {
      return iterator_;
    }

    return ++iterator_;
  }
  else
  {
    throw std::runtime_error("<EMPTY>\n");
  }
}

PhoneBook::iter PhoneBook::previousRecord()
{
  if (iterator_ == phoneBook_.begin())
  {
    return iterator_;
  }

  return --iterator_;
}

PhoneBook::iter PhoneBook::insertAfter(const recordType& record, iter& position)
{
  if (position == phoneBook_.end() || std::next(position) == phoneBook_.end())
  {
    phoneBook_.push_back(record);

    return --phoneBook_.end();
  }

  auto temp = position;

  return phoneBook_.insert(++temp, record);
}

PhoneBook::iter PhoneBook::insertBefore(const recordType& record, iter& position)
{
  if (position != phoneBook_.end() && position != phoneBook_.begin())
  {
    return phoneBook_.insert(position, record);
  }
  else
  {
    phoneBook_.push_front(record);
    return phoneBook_.begin();
  }
}

void PhoneBook::replaceCurrentRecord(const recordType& record)
{
  phoneBook_.erase(iterator_);
  PhoneBook::insertBefore(record, iterator_);
}

void PhoneBook::pushBack(const recordType& record)
{
  phoneBook_.push_back(record);

  if (iterator_ == phoneBook_.end())
  {
    iterator_ = phoneBook_.begin();
  }
}

void PhoneBook::deleteRecord(iter& element)
{
  if (isEmpty())
  {
    throw std::runtime_error("<EMPTY>\n");
  }

  phoneBook_.erase(element);
}

bool PhoneBook::isEmpty() const
{
  return phoneBook_.empty();
}

PhoneBook::iter PhoneBook::getRecord()
{
  return iterator_;
}

PhoneBook::iter PhoneBook::begin()
{
  return phoneBook_.begin();
}

PhoneBook::iter PhoneBook::end()
{
  return phoneBook_.end();
}
