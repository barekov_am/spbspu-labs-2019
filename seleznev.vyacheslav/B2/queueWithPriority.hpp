#ifndef QUEUEWITHPRIORITY_HPP
#define QUEUEWITHPRIORITY_HPP

#include <stdexcept>

#include "queueWithPriorityInterface.hpp"

template <typename T>
void QueueWithPriority<T>::addElementToQueue(const T& element, ElementPriority priority)
{
  switch (priority)
  {
    case ElementPriority::HIGH:
    {
      highPriority_.push_back(element);
      break;
    }

    case ElementPriority::NORMAL:
    {
      normalPriority_.push_back(element);
      break;
    }

    case ElementPriority::LOW:
    {
      lowPriority_.push_back(element);
      break;
    }

    default:
    {
      throw std::runtime_error("Incorrect priority.");
    }
  }
}

template <typename T>
bool QueueWithPriority<T>::isEmpty()
{
  return (lowPriority_.empty() && normalPriority_.empty() && highPriority_.empty());
}

template <typename T>
T QueueWithPriority<T>::getElementFromQueue()
{
  if (!highPriority_.empty())
  {
    T element = highPriority_.front();
    highPriority_.pop_front();

    return element;
  }

  else if (!normalPriority_.empty())
  {
    T element = normalPriority_.front();
    normalPriority_.pop_front();

    return element;
  }

  else if (!lowPriority_.empty())
  {
    T element = lowPriority_.front();
    lowPriority_.pop_front();

    return element;
  }

  else
  {
    throw std::runtime_error("Error in getting element.");
  }
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  highPriority_.splice(highPriority_.end(), lowPriority_);
}

#endif
