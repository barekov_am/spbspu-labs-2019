#ifndef QUEUEWITHPRIORITYINTERFACE_HPP
#define QUEUEWITHPRIORITYINTERFACE_HPP

#include <list>

template <typename T>
class QueueWithPriority
{
public:
  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  void addElementToQueue(const T& element, ElementPriority priority);

  T getElementFromQueue();

  void accelerate();

  bool isEmpty();

private:
  std::list<T> highPriority_;
  std::list<T> lowPriority_;
  std::list<T> normalPriority_;
};

#endif
