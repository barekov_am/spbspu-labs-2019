#include <iostream>
#include <string>
#include <sstream>

#include "queueWithPriority.hpp"

void firstTask()
{
  std::string line;

  QueueWithPriority<std::string> queue;

  while (getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error reading data.");
    }

    std::stringstream string(line);
    std::string command;
    std::string priority;
    std::string data;

    string >> command;

    if (command == "accelerate")
    {
      if (queue.isEmpty())
      {
        std::cout << "<EMPTY>\n";

        return;
      }

      queue.accelerate();
    }

    else if (command == "get")
    {
      if (queue.isEmpty())
      {
        std::cout << "<EMPTY>\n";

        return;
      }

      else
      {
        std::cout << queue.getElementFromQueue() << "\n";
      }
    }

    else if (command == "add")
    {
      string >> priority >> data;

      if (!string.eof())
      {
        std::getline(string, line);
        data += line;
      }

      if ((!string.eof()) || (data.empty()))
      {
        std::cout << "<INVALID COMMAND>\n";

        return;
      }

      if (priority == "high")
      {
        queue.addElementToQueue(data, QueueWithPriority<std::string>::ElementPriority::HIGH);
      }

      else if (priority == "normal")
      {
        queue.addElementToQueue(data, QueueWithPriority<std::string>::ElementPriority::NORMAL);
      }

      else if (priority == "low")
      {
        queue.addElementToQueue(data, QueueWithPriority<std::string>::ElementPriority::LOW);
      }

      else
      {
        std::cout << "<INVALID COMMAND>\n";

        return;
      }
    }

    else
    {
      std::cout << "<INVALID COMMAND>\n";

      return;
    }
  }
}
