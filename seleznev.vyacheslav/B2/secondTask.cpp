#include <list>
#include <stdexcept>
#include <iostream>

void secondTask()
{
  std::list<int> list;

  int data = 0;
  int counter = 0;
  const int minValue = 1;
  const int maxValue = 20;
  const int capacityList = 20;

  while (std::cin >> data)
  {
    counter++;

    if ((data < minValue) || (data > maxValue))
    {
      throw std::invalid_argument("Invalid value.");
    }

    if (counter > capacityList)
    {
      throw std::invalid_argument("Counter elements out of range.");
    }

    list.push_back(data);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Error reading data.");
  }

  if (list.empty())
  {
    return;
  }

  while (!list.empty())
  {
    std::cout << list.front() << " ";
    list.pop_front();

    if (list.empty())
    {
      std::cout << "\n";

      return;
    }

    std::cout << list.back() << " ";
    list.pop_back();
  }

  std::cout << "\n";
}
