#include "dataStruct.hpp"

#include <iostream>

void tasks()
{
  std::vector<DataStruct> vector;

  const int cr = '\n';
  const int eol = -1;
  
  while (!std::cin.eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Errors in reading data");
    }

    auto nextCharacter = std::cin.peek();

    const bool isValidLine = nextCharacter != cr && nextCharacter != eol;

    if (isValidLine)
    {
      vector.push_back(read());
    }
    else if (vector.empty() && nextCharacter == eol)
    {
      return;
    }
    else
    {
      std::cin.get();
    }
  }
  
  if (!vector.empty())
  {
    sort(vector);
  }
  else
  {
    throw std::runtime_error("Vector is empty");
  }
  
  for (auto i = vector.begin(); i != vector.end(); ++i)
  {
    std::cout << *i;
  }
}
