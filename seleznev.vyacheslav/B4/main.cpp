#include <iostream>

void tasks();

int main()
{
  try
  {
    tasks();
  }
  catch (const std::exception& err)
  {
    std::cerr << err.what();

    return 1;
  }

  return 0;
}
