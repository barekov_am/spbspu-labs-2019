#ifndef DATASTRUCTINTERFACE_HPP
#define DATASTRUCTINTERFACE_HPP

#include <string>
#include <vector>

struct DataStruct
{
  int key1;
  int key2;

  std::string str;
};

DataStruct read();

bool keyComparison(const DataStruct& first, const DataStruct& second);

void sort(std::vector<DataStruct>& vector);

std::ostream& operator << (std::ostream& stream, const DataStruct& data);

#endif
