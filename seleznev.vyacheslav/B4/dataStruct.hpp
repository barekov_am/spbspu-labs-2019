#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP

#include "dataStructInterface.hpp"

#include <iostream>
#include <algorithm>

DataStruct read()
{
  std::string stream;
  std::getline(std::cin, stream);

  const int amountOfElements = 2;
  const int maxValue = 5;
  const int minValue = -5;

  int keys[amountOfElements] = {0};

  for (int i = 0; i < amountOfElements; ++i)
  {
    size_t comma = stream.find_first_of(",");

    if (comma == std::string::npos)
    {
      throw std::invalid_argument("Invalid separator");
    }

    auto digit = stream.substr(0, comma);

    while (digit[0] == ' ')
    {
      digit = digit.erase(0, 1);
    }

    keys[i] = std::stoi(digit);

    if (keys[i] > maxValue || keys[i] < minValue)
    {
      throw std::invalid_argument("Incorrent value of keys");
    }

    stream = stream.erase(0, comma + 1);
  }

  while (stream.find_first_of(' ') == 0)
  {
    stream.erase(0, 1);
  }

  if (stream.empty())
  {
    throw std::runtime_error("Input stream is empty");
  }

  return { keys[0], keys[1], stream };
}

bool keyComparison(const DataStruct& first, const DataStruct& second)
{
  if (first.key1 < second.key1)
  {
    return true;
  }
  else if (first.key1 == second.key1)
  {
    if (first.key2 < second.key2)
    {
      return true;
    }
    else if (first.key2 == second.key2)
    {
      if (first.str.size() < second.str.size())
      {
        return true;
      }
    }
  }

  return false;
}

void sort(std::vector<DataStruct>& vector)
{
  std::sort(vector.begin(), vector.end(), keyComparison);
}

std::ostream& operator << (std::ostream& stream, const DataStruct& data)
{
  stream << data.key1 << ',' << data.key2 << ',' << data.str << "\n";
  return stream;
}

#endif
