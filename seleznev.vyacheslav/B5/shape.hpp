#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <vector>
#include <list>

struct figures
{
  int amountOfTriangles;
  int amountOfRectangles;
  int amountOfSquares;
};

struct Point
{
  int x, y;
};

using Shape = std::vector<Point>;
using ShapesContainer = std::list<Shape>;

#endif
