#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

void print(std::vector<std::string>& text)
{
  for (auto i = text.begin(); i != text.end(); ++i)
  {
    std::cout << *i << "\n";
  }
}

bool isDelimiter(char& ch)
{
  std::vector<char> invalidChars = { '\n', '\t', -1, ' ' };

  return std::find(invalidChars.begin(), invalidChars.end(), ch) != invalidChars.end();
}

void read(std::vector<std::string>& txt)
{
  char c = std::cin.get();
  std::string word = "";

  while (c != -1)
  {
    if (isDelimiter(c))
    {
      if (!word.empty())
      {
        txt.push_back(word);
        word.clear();
      }
    }
    else
    {
      word += c;
    }

    c = std::cin.get();
  }

  if (!word.empty())
  {
    txt.push_back(word);
    word.clear();
  }
}

void firstTask()
{
  std::vector<std::string> text;

  read(text);

  std::sort(text.begin(), text.end());

  auto findSameWords = std::unique(text.begin(), text.end());
  text.erase(findSameWords, text.end());

  print(text);
}
