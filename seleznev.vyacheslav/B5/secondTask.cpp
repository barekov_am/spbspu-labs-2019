﻿#include "shape.hpp"

#include <iostream>
#include <sstream>
#include <cmath>
#include <algorithm>

Shape read(const std::string& stream)
{
  std::stringstream string(stream);
  std::string numOfVertices;
  string >> numOfVertices;
  int numberOfVertices = std::stoi(numOfVertices);

  if (numberOfVertices < 3)
  {
    throw std::invalid_argument("Invalid parameters");
  }

  Shape shapeVector;

  for (int i = 0; i < numberOfVertices; ++i)
  {
    Point point = {0, 0};

    string.ignore(stream.length(), '(');
    string >> point.x;

    string.ignore(stream.length(), ';');
    string >> point.y;

    string.ignore(stream.length(), ')');

    shapeVector.push_back(point);
  }

  if (string.fail())
  {
    throw std::invalid_argument("Invalid parameters");
  }

  return shapeVector;
}

bool isInvalidCharacter(char& ch)
{
  std::vector<char> invalidChars = { '\n', '\t', -1, ' ' };

  return std::find(invalidChars.begin(), invalidChars.end(), ch) != invalidChars.end();
}

void removeInvalidStartCharacters(std::string& str)
{
  while (!str.empty() && isInvalidCharacter(str[0]))
  {
    str = str.erase(0);
  }
}

void fillContainer(ShapesContainer& shapesContainer)
{
  std::string line;

  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Errors in reading data");
    }

    removeInvalidStartCharacters(line);

    if (!line.empty())
    {
      shapesContainer.push_back(read(line));
    }
  }
}

int sumVertices(const ShapesContainer& shapesContainer)
{
  int amount = 0;

  for (auto i = shapesContainer.begin(); i != shapesContainer.end(); ++i)
  {
    amount += i->size();
  }

  return amount;
}

double getLength(const Point& firstPoint, const Point& secondPoint)
{
  return sqrt(pow((firstPoint.x - secondPoint.x), 2) + pow((firstPoint.y - secondPoint.y), 2));
}

bool areSame(double a, double b)
{
  return std::abs(a - b) < std::numeric_limits<double>::epsilon();
}

bool checkSquare(const Shape& shape)
{
  double firstSide = getLength(shape[0], shape[1]);
  double secondSide = getLength(shape[0], shape[3]);
  double thirdSide = getLength(shape[1], shape[2]);
  double fourthSide = getLength(shape[2], shape[3]);

  double firstDiagonal = getLength(shape[0], shape[2]);
  double secondDiagonal = getLength(shape[1], shape[3]);

  return areSame(firstSide, secondSide) && areSame(firstSide, thirdSide) &&
      areSame(firstSide, fourthSide) && areSame(firstDiagonal, secondDiagonal);
}

bool checkRectangle(const Shape& shape)
{
  double firstSide = getLength(shape[0], shape[1]);
  double secondSide = getLength(shape[1], shape[2]);
  double thirdSide = getLength(shape[2], shape[3]);
  double fourthSide = getLength(shape[0], shape[3]);

  return areSame(firstSide, thirdSide) && areSame(secondSide, fourthSide) && !areSame(firstSide, secondSide);
}

bool isTriangle(const Shape& shape)
{
  return shape.size() == 3;
}

bool isRectangle(const Shape& shape)
{
  return shape.size() == 4 && checkRectangle(shape);
}

bool isSquare(const Shape& shape)
{
  return shape.size() == 4 && checkSquare(shape);
}

figures amountOfFigures(ShapesContainer& shapesContainer)
{
  figures measurements = {0, 0, 0};

  for (auto i = shapesContainer.begin(); i != shapesContainer.end(); ++i)
  {
    if (isTriangle(*i))
    {
      ++measurements.amountOfTriangles;
    }
    else if (isRectangle(*i))
    {
      ++measurements.amountOfRectangles;
    }
    else if (isSquare(*i))
    {
      ++measurements.amountOfSquares;
      ++measurements.amountOfRectangles;
    }
  }

  return measurements;
}

void deletePentagons(ShapesContainer& shapesContainer)
{
  shapesContainer.erase(std::remove_if(shapesContainer.begin(), shapesContainer.end(),
      [](const Shape& shape) {return shape.size() == 5;}), shapesContainer.end());
}

std::vector<Point> makeNewVectorOfPoints(const ShapesContainer& shapesContainer)
{
  std::vector<Point> vectorOfPoints;

  for (auto i = shapesContainer.begin(); i != shapesContainer.end(); ++i)
  {
    vectorOfPoints.push_back((*i)[0]);
  }

  return vectorOfPoints;
}

ShapesContainer containerChange(const ShapesContainer& shapesContainer)
{
  ShapesContainer newContainer;
  ShapesContainer triangles;
  ShapesContainer rectangles;
  ShapesContainer squares;
  ShapesContainer other;

  for (auto i = shapesContainer.begin(); i != shapesContainer.end(); ++i)
  {
    if (isTriangle(*i))
    {
      triangles.push_back(*i);
    }
    else if (isSquare(*i))
    {
      squares.push_back(*i);
    }
    else if (isRectangle(*i))
    {
      rectangles.push_back(*i);
    }
    else
    {
      other.push_back(*i);
    }
  }

  newContainer.splice(newContainer.end(), triangles);
  newContainer.splice(newContainer.end(), squares);
  newContainer.splice(newContainer.end(), rectangles);
  newContainer.splice(newContainer.end(), other);

  return newContainer;
}

void print(const std::vector<Point>& vectorOfPoints, const int& vertNumber,
    const figures& measurements, const ShapesContainer& shapesContainer)
{
  std::cout << "Vertices: " << vertNumber << "\n";
  std::cout << "Triangles: " << measurements.amountOfTriangles << "\n";
  std::cout << "Squares: " << measurements.amountOfSquares << "\n";
  std::cout << "Rectangles: " << measurements.amountOfRectangles << "\n";

  std::cout << "Points:";

  for (const auto& output : vectorOfPoints)
  {
    std::cout << " (" << output.x << "; " << output.y << ")";
  }

  std::cout << "\n";
  std::cout << "Shapes:\n";

  for (const auto& shape: shapesContainer)
  {
    std::cout << shape.size() << " ";

    for (const auto& output: shape)
    {
      std::cout << "(" << output.x << "; " << output.y << ") ";
    }

    std::cout << "\n";
  }
}

void secondTask()
{
  Shape shape;
  ShapesContainer shapesContainer;

  fillContainer(shapesContainer);

  int vertNumber = sumVertices(shapesContainer);

  figures measurements = amountOfFigures(shapesContainer);

  deletePentagons(shapesContainer);

  std::vector<Point> vectorOfPoints = makeNewVectorOfPoints(shapesContainer);

  shapesContainer = containerChange(shapesContainer);

  print(vectorOfPoints, vertNumber, measurements, shapesContainer);
}
