#ifndef SHAPE_HPP
#define SHAPE_HPP
#include "base-types.hpp"
#include "iostream"

namespace ustinov
{
  class Shape
  {
  public:
    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;

    virtual void move(const point_t &goal) noexcept = 0;
    virtual void move(double dx, double dy) noexcept = 0;

    virtual void rotate(double angle) noexcept = 0;
    virtual void scale(double coefficient) = 0;

    virtual void show() const noexcept = 0;

  };
};
#endif /* Shape_hpp */
