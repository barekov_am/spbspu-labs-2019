#include "matrix.hpp"
#include <iostream>

ustinov::Matrix::Matrix():
  rows_(0),
  columns_(0),
  matrixShape(nullptr)
{}

ustinov::Matrix::Matrix(const Matrix &matrix):
  rows_(matrix.rows_),
  columns_(matrix.columns_),
  matrixShape(std::make_unique<shpPtr []>(matrix.rows_ * matrix.columns_))
{
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    matrixShape[i] = matrix.matrixShape[i];
  }
}

ustinov::Matrix::Matrix(Matrix &&matrix) noexcept:
  rows_(matrix.rows_),
  columns_(matrix.columns_),
  matrixShape(std::move(matrix.matrixShape))
{}

ustinov::Matrix &ustinov::Matrix::operator =(const Matrix &matrix)
{
  Matrix temp(matrix);
  swap(temp);

  return *this;
}

ustinov::Matrix &ustinov::Matrix::operator =(Matrix &&matrix) noexcept
{
  if (this != &matrix)
  {
    rows_ = matrix.rows_;
    columns_ = matrix.columns_;
    matrixShape = std::move(matrix.matrixShape);
  }

  return *this;
}

bool ustinov::Matrix::operator ==(const Matrix &matrix) const
{
  if ((rows_ != matrix.rows_) || (columns_ != matrix.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (matrixShape[i] != matrix.matrixShape[i])
    {
      return false;
    }
  }

  return true;
}

std::unique_ptr<ustinov::Matrix::shpPtr []> ustinov::Matrix::operator[](size_t row) const
{
  if (row >= rows_)
  {
    throw std::out_of_range("Row index error");
  }

  std::unique_ptr<shpPtr []> newTempMatrix(std::make_unique<shpPtr []>(columns_));

  for (size_t i = 0; i < columns_; ++i)
  {
    newTempMatrix[i] = matrixShape[row * columns_ + i];
  }

  return newTempMatrix;
}

bool ustinov::Matrix::operator !=(const Matrix &matrix) const
{
  return (this != &matrix);
}

void ustinov::Matrix::add(shpPtr shape, const size_t row, const size_t column)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("null ptr");
  }

  auto tempRow = (rows_ == row) ? (rows_ + 1) : (rows_);
  auto tempCol = (columns_ == column) ? (columns_ + 1) : (columns_);

  std::unique_ptr<shpPtr []> newTempMatrix(std::make_unique<shpPtr []>(tempRow * tempCol));

  for (size_t i = 0; i < tempRow; ++i)
  {
    for (size_t j = 0; j < tempCol; ++j)
    {
      newTempMatrix[i * tempCol + j] = (rows_ == i) || (columns_ == j) ? nullptr :
                                       matrixShape[i * columns_ + j];
    }
  }

  newTempMatrix[row * tempCol + column] = shape;
  matrixShape.swap(newTempMatrix);

  rows_ = tempRow;
  columns_ = tempCol;
}

void ustinov::Matrix::swap(ustinov::Matrix &matrix) noexcept
{
  std::swap(columns_, matrix.columns_);

  std::swap(rows_, matrix.rows_);

  std::swap(matrixShape, matrix.matrixShape);
}


void ustinov::Matrix::showInfo() const noexcept
{
  for (size_t i = 0; i < rows_; ++i)
  {
    for (size_t j = 0; j < columns_; ++j)
    {
      if (matrixShape[i * columns_ + j] != nullptr)
      {
        std::cout << "layer: " << i + 1
                  << ", column: " << j + 1 << std::endl
                  << "Information obj:" << std::endl;

        matrixShape[i * columns_ + j]->show();
      }
    }
  }
}
