
#include "division.hpp"
#include <cmath>

bool ustinov::superposition(shpPtr shape1, shpPtr shape2)
{
  if ((shape1 == nullptr) || (shape2 == nullptr))
  {
    throw std::invalid_argument("Error null ptr");
  }

  auto shape1FrameRect = shape1->getFrameRect();
  auto shape2FrameRect = shape2->getFrameRect();

  bool forX = fabs(shape1FrameRect.pos.x - shape2FrameRect.pos.x)
              < ((shape1FrameRect.width / 2) + (shape2FrameRect.width / 2));

  bool forY = fabs(shape1FrameRect.pos.y - shape2FrameRect.pos.y)
              < ((shape1FrameRect.height / 2) + (shape2FrameRect.height / 2));

  return forX && forY;
}


ustinov::Matrix ustinov::section(const ustinov::CompositeShape &shape)
{
  Matrix matrix;

  for (size_t i = 0; i < shape.getCount(); ++i)
  {
    size_t row = 0;
    size_t columns = 0;

    for (size_t j = 0; j < matrix.getRows(); ++j)
    {
      for (size_t k = 0; k < matrix.getColumns(); ++k)
      {
        if (matrix[j][k] == nullptr)
        {
          columns = k;
          row = j;
          break;
        }

        if (superposition(shape[i], matrix[j][k]))
        {
          columns = 0;
          row = j + 1;
          break;
        }

        columns = k + 1;
        row = j;
      }

      if (row == j)
      {
        break;
      }
    }

    matrix.add(shape[i], row, columns);
  }

  return matrix;
}
