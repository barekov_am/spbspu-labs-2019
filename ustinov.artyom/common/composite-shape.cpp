#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <limits>

ustinov::CompositeShape::CompositeShape() :
  counter_(0),
  angle_(0),
  arrayOfShape_(nullptr)
{}

ustinov::CompositeShape::CompositeShape(shpPtr shape) :
  counter_(1),
  angle_(0),
  arrayOfShape_(std::make_unique<shpPtr []>(counter_))
{
  if (shape != nullptr)
  {
    arrayOfShape_[0] = shape;
  }
  else
  {
    throw std::invalid_argument("Error nullptr!");
  }
}

ustinov::CompositeShape::CompositeShape(const ustinov::CompositeShape &compositeShape) :
  counter_(compositeShape.counter_),
  angle_(compositeShape.angle_),
  arrayOfShape_(std::make_unique<shpPtr []>(compositeShape.counter_))
{
  for (size_t i = 0; i < counter_; ++i)
  {
    arrayOfShape_[i] = compositeShape.arrayOfShape_[i];
  }
}

ustinov::CompositeShape::CompositeShape(CompositeShape &&composite) noexcept :
  counter_(composite.counter_),
  angle_(composite.angle_),
  arrayOfShape_(std::move(composite.arrayOfShape_))
{}

ustinov::CompositeShape &ustinov::CompositeShape::operator =(const ustinov::CompositeShape &composite)
{
  CompositeShape temp(composite);
  swap(temp);

  return *this;
}

ustinov::CompositeShape &ustinov::CompositeShape::operator =(ustinov::CompositeShape &&composite) noexcept
{
  if (this != &composite)
  {
    counter_ = composite.counter_;
    arrayOfShape_ = std::move(composite.arrayOfShape_);
  }

  return *this;
}

ustinov::CompositeShape::shpPtr ustinov::CompositeShape::operator[](size_t index) const
{
  if (index > counter_)
  {
    throw std::out_of_range("Error index");
  }

  return arrayOfShape_[index];
}

void ustinov::CompositeShape::move(double dx, double dy) noexcept
{
  for (size_t i = 0; i < counter_; ++i)
  {
    arrayOfShape_[i]->move(dx, dy);
  }
}

void ustinov::CompositeShape::move(const ustinov::point_t &goal) noexcept
{
  auto frameRect = getFrameRect().pos;

  auto dx = goal.x - frameRect.x; //double
  auto dy = goal.y - frameRect.y; //double

  move(dx, dy);
}

double ustinov::CompositeShape::getArea() const
{
  double sum_area = 0;

  for (size_t i = 0; i < counter_; i++)
  {
    sum_area += arrayOfShape_[i]->getArea();
  }

  return sum_area;
}

void ustinov::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Coefficient must be positive");
  }

  auto tmpFrameRect = getFrameRect().pos; //point_t

  for (size_t i = 0; i < counter_; i++)
  {
    auto centerOfShape = arrayOfShape_[i]->getFrameRect().pos;

    auto dx = (centerOfShape.x - tmpFrameRect.x) * (coefficient - 1);
    auto dy = (centerOfShape.y - tmpFrameRect.y) * (coefficient - 1);

    arrayOfShape_[i]->move(dx, dy);
    arrayOfShape_[i]->scale(coefficient);
  }
}

ustinov::rectangle_t ustinov::CompositeShape::getFrameRect() const
{
  rectangle_t frameRect = arrayOfShape_[0]->getFrameRect();

  auto top = frameRect.pos.y + frameRect.height / 2; //double
  auto bottom = frameRect.pos.y - frameRect.height / 2; //double

  auto left = frameRect.pos.x - frameRect.width / 2; //double
  auto right = frameRect.pos.x + frameRect.width / 2; //double

  for (size_t i = 1; i < counter_; i++)
  {
    rectangle_t tempRect = arrayOfShape_[i]->getFrameRect();

    auto leftLimit = tempRect.pos.x - tempRect.width / 2;//double
    left = std::min(left, leftLimit);

    auto rightLimit = tempRect.pos.x + tempRect.width / 2; //double
    right = std::max(right, rightLimit);

    auto topLimit = tempRect.pos.y + tempRect.height / 2; //double
    top = std::max(top, topLimit);

    auto bottomLimit = tempRect.pos.y - tempRect.height / 2; //double
    bottom = std::min(bottom, bottomLimit);
  }

  return {{(right + left) / 2, (top + bottom) / 2}, right - left, top - bottom};
}

void ustinov::CompositeShape::add(shpPtr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Error null ptr");
  }

  std::unique_ptr<shpPtr []> newArray(std::make_unique<shpPtr []>(counter_ + 1));

  for (size_t i = 0; i < counter_; i++)
  {
    newArray[i] = arrayOfShape_[i];
  }

  newArray[counter_] = shape;
  counter_++;
  arrayOfShape_.swap(newArray);
}

void ustinov::CompositeShape::remove(const size_t index)
{

  for (size_t i = index; i < counter_ - 1; i++)
  {
    arrayOfShape_[i] = arrayOfShape_[i + 1];
  }
  arrayOfShape_[--counter_] = nullptr;
}

size_t ustinov::CompositeShape::getCount() const noexcept
{
  return counter_;
}

void ustinov::CompositeShape::remove(shpPtr shape)
{
  if (counter_ == 0)
  {
    throw std::logic_error("Composite is empty");
  }

  bool flag = false;
  int pos_ = 0;

  for (size_t i = 0; i < counter_; i++)
  {
    if (arrayOfShape_[i] == shape)
    {
      pos_ = i;
      flag = true;
      break;
    }
  }

  for (size_t i = pos_; i < (counter_ - 1); i++)
  {
    arrayOfShape_[i] = arrayOfShape_[i + 1];
  }

  if (flag)
  {
    arrayOfShape_[--counter_] = nullptr;
  }
}

void ustinov::CompositeShape::rotate(double angle) noexcept
{

  const auto center = getFrameRect().pos; //double all

  const auto sinus = sin(angle * M_PI / 180);

  const auto cosine = cos(angle * M_PI / 180);

  for (size_t i = 0; i < counter_; i++)
  {
    const auto tempCenter = arrayOfShape_[i]->getFrameRect().pos;

    const auto newPosX = (tempCenter.x - center.x) * (fabs(cosine) - tempCenter.y - center.y * fabs(sinus));
    auto pointForX = center.x + newPosX;

    const auto newPosY = (tempCenter.y - center.y) * (fabs(sinus) + tempCenter.y - center.y * fabs(cosine));
    auto pointForY = center.y + newPosY;

    arrayOfShape_[i]->move(pointForX, pointForY);
    arrayOfShape_[i]->rotate(angle);
  }
}

void ustinov::CompositeShape::show() const noexcept
{
  auto frameRect = getFrameRect(); //ustinov::rectangle_t

  std::cout << "Center of frame rectangle, X: " << frameRect.pos.x
            << "; Y: " << frameRect.pos.y << std::endl;
  std::cout << "Wight of frame rectangle: " << frameRect.width << std::endl;
  std::cout << "Height of frame rectangle: " << frameRect.height << std::endl;
  std::cout << "Area: " << getArea() << std::endl
            << "Length: " << getCount() << std::endl;
}

void ustinov::CompositeShape::swap(ustinov::CompositeShape &composite) noexcept
{
  std::swap(counter_, composite.counter_);

  std::swap(arrayOfShape_, composite.arrayOfShape_);
}
