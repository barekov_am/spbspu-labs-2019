#include "rectangle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>

ustinov::Rectangle::Rectangle(const point_t &center, double width, double height)
{
  if ((width <= 0) || (height <= 0))
  {
    throw std::invalid_argument("Error width or height of rect");
  }

  corners_[0] = {center.x - width / 2, center.y + height / 2}, //limitTopLeft
  corners_[1] = {center.x + width / 2, center.y + height / 2}, //limitTopRight
  corners_[2] = {center.x + width / 2, center.y - height / 2}, //limitBotRight
  corners_[3] = {center.x - width / 2, center.y - height / 2}; //limitBotLeft
}

double ustinov::Rectangle::getArea() const noexcept
{
  double_t width = sqrt(pow(corners_[1].x - corners_[0].x, 2) + pow(corners_[1].y - corners_[0].y, 2));

  double_t height = sqrt(pow(corners_[0].x - corners_[3].x, 2) + pow(corners_[0].y - corners_[3].y, 2));

  return (width * height);
}

ustinov::rectangle_t ustinov::Rectangle::getFrameRect() const noexcept
{
  auto limitTop = corners_[0].y; //double all
  auto limitBottom = corners_[0].y;
  auto limitLeft = corners_[0].x;
  auto limitRight = corners_[0].x;

  for (auto corner : corners_)
  {
    limitTop = std::max(limitTop, corner.y);
    limitBottom = std::min(limitBottom, corner.y);
    limitLeft = std::min(limitLeft, corner.x);
    limitRight = std::max(limitRight, corner.x);
  }

  return {{(limitRight + limitLeft) / 2, (limitBottom + limitTop) / 2},
    limitRight - limitLeft, limitTop - limitBottom};
}

ustinov::point_t ustinov::Rectangle::getCenter() const
{
  return {(corners_[2].x - corners_[3].x) / 2, (corners_[0].y - corners_[3].y) / 2};
}

void ustinov::Rectangle::move(const point_t &goal) noexcept
{
  auto initialCenter = getCenter();

  const auto dx = goal.x - initialCenter.x; //double all
  const auto dy = goal.y - initialCenter.y;

  move(dx, dy);
}

void ustinov::Rectangle::move(double dx, double dy) noexcept
{
  for (int i = 0; i < 4; ++i)
  {
    corners_[i].x += dx;
    corners_[i].y += dy;
  }
}

void ustinov::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Invalid coefficient for circle");
  }

  auto initialCenter = getCenter();

  for (auto &corner : corners_)
  {
    corner.x = initialCenter.x + coefficient * (corner.x - initialCenter.x);
    corner.y = initialCenter.y + coefficient * (corner.y - initialCenter.y);
  }
}

void ustinov::Rectangle::rotate(double angle) noexcept
{
  auto angleCos = cos(angle * M_PI / 180);
  auto angleSin = sin(angle * M_PI / 180);

  point_t center = getFrameRect().pos;

  for (auto &corner : corners_)
  {
    auto pointForX = center.x + (corner.x - center.x) * angleCos
                     - (corner.y - center.y) * angleSin;

    auto pointForY = center.y + (corner.x - center.x) * angleSin
                     + (corner.y - center.y) * angleCos;

    corner = {pointForX, pointForY};
  }
}

void ustinov::Rectangle::show() const noexcept
{
  rectangle_t rectangle = getFrameRect();

  std::cout << std::endl
      << "Coordinates of centre Rect :(" << rectangle.pos.x
      << ";" << rectangle.pos.y << ")" << std::endl
      << "Frame rectangle width = " << rectangle.width
      << ", height = " << rectangle.height << std::endl
      << "Area = " << getArea() << std::endl;
}
