#include <stdexcept>
#include <iostream>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

#define IMPRECISION 0.001

BOOST_AUTO_TEST_SUITE(CircleTest)

BOOST_AUTO_TEST_CASE(areaAfterMovingCenter)
{
  ustinov::Circle testCircle({3, 5}, 6);
  const auto area = testCircle.getArea(); //double
  testCircle.move({12, 2});
  BOOST_CHECK_CLOSE(testCircle.getArea(), area, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(areaAfterMoovingCenter)
{
  ustinov::Circle testCircle({3, 5}, 6);
  const auto area = testCircle.getArea(); //double
  testCircle.move(5, 2);
  BOOST_CHECK_CLOSE(testCircle.getArea(), area, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(areaCircleIncrease)
{
  ustinov::Circle testCircle({3, 5}, 6);
  const auto area = testCircle.getArea(); //double
  const auto parameter = 3; //double
  testCircle.scale(parameter);
  BOOST_CHECK_CLOSE(testCircle.getArea(), area * parameter * parameter, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(areaCircleDecrease)
{
  ustinov::Circle testCircle({3, 5}, 6);
  const auto area = testCircle.getArea(); //double
  const auto parameter = 0.2;
  testCircle.scale(parameter);
  BOOST_CHECK_CLOSE(testCircle.getArea(), area * parameter * parameter, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(inncorrectRadius)
{
  BOOST_CHECK_THROW(ustinov::Circle testCircle({12, 3}, -3.4), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(inncorrectCoefficientScale)
{
  ustinov::Circle testCircle({12, 3}, 3.4);
  BOOST_CHECK_THROW(testCircle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testCircleRotation)
{
  ustinov::Circle testCircle({3, 5}, 6);
  const auto testAreaBefore = testCircle.getArea();
  const auto testPosBefore = testCircle.getFrameRect();

  testCircle.rotate(60);

  const auto testPosAfter = testCircle.getFrameRect();
  const auto testAreaAfter = testCircle.getArea();

  BOOST_CHECK_CLOSE(testAreaBefore, testAreaAfter, IMPRECISION);
  BOOST_CHECK_CLOSE(testPosBefore.pos.x, testPosAfter.pos.x, IMPRECISION);
  BOOST_CHECK_CLOSE(testPosBefore.pos.y, testPosAfter.pos.y, IMPRECISION);
  BOOST_CHECK_CLOSE(testPosBefore.width, testPosAfter.width, IMPRECISION);
  BOOST_CHECK_CLOSE(testPosBefore.height, testPosAfter.height, IMPRECISION);
}

BOOST_AUTO_TEST_SUITE_END()
