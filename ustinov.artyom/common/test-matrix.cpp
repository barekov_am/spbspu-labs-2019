#include <stdexcept>
#include <iostream>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "division.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "matrix.hpp"


BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(testMatrixCopyConstructor)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape comShp;

  comShp.add(rectPointer);
  comShp.add(circlePointer);
  const auto row = 2u;

  ustinov::Matrix matrix = ustinov::section(comShp);

  BOOST_CHECK_EQUAL(matrix.getRows(), row);
}

BOOST_AUTO_TEST_CASE(testMatrixCopyOperator)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape comShp;

  comShp.add(rectPointer);
  comShp.add(circlePointer);

  ustinov::Matrix matrix = ustinov::section(comShp);
  const ustinov::Matrix &matrixCopy = matrix;

  BOOST_CHECK(matrixCopy == matrix);
}

BOOST_AUTO_TEST_CASE(TestLayers)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape comShp;

  comShp.add(rectPointer);
  comShp.add(circlePointer);

  ustinov::Matrix matrix = ustinov::section(comShp);

  BOOST_CHECK_EQUAL(matrix.getRows(), 2);
}

BOOST_AUTO_TEST_CASE(TestThrowingExceptions)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape comShp;

  comShp.add(rectPointer);
  comShp.add(circlePointer);

  auto matrix = ustinov::section(comShp);

  BOOST_CHECK_THROW(matrix[matrix.getRows() + 1][matrix.getColumns() + 1], std::out_of_range);
  BOOST_CHECK_THROW(matrix.add(nullptr, 0, 0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testMoveMatrixAndCopy)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);
  ustinov::CompositeShape comShp;

  comShp.add(rectPointer);
  comShp.add(circlePointer);

  ustinov::Matrix matrix = ustinov::section(comShp);
  ustinov::Matrix matrixCopy = matrix;

  ustinov::Matrix moveMatrix(std::move(matrix));

  BOOST_CHECK_EQUAL(matrixCopy.getRows(), moveMatrix.getRows());
  BOOST_CHECK_EQUAL(matrixCopy.getColumns(), moveMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(testMatrixMoveOperator)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);
  ustinov::CompositeShape comShp;

  comShp.add(rectPointer);
  comShp.add(circlePointer);

  ustinov::Matrix matrix = ustinov::section(comShp);
  ustinov::Matrix matrixCopy = matrix;

  ustinov::Matrix moveMatrix(std::move(matrix));

  BOOST_CHECK(matrixCopy ==  moveMatrix);
}

BOOST_AUTO_TEST_SUITE_END()
