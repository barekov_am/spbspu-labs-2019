#include <stdexcept>
#include <iostream>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

#define IMPRECISION 0.001

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(constantWidthAndHeightAfterMovingToPosize_t)
{
  ustinov::Rectangle testRectangle({2, 3}, 6, 3);
  ustinov::rectangle_t frameBefore = testRectangle.getFrameRect();
  testRectangle.move({12, 2});
  ustinov::rectangle_t frameAfter = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, IMPRECISION);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(constantWidthAndHeightAfterMovingWithOffset)
{
  ustinov::Rectangle testRectangle({6, 7}, 12, 10);
  ustinov::rectangle_t frameBefore = testRectangle.getFrameRect();
  testRectangle.move(22, 21);
  ustinov::rectangle_t frameAfter = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, IMPRECISION);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(constantAreaMovingToPosize_t)
{
  ustinov::Rectangle testRectangle({12, 23}, 9, 15);
  const auto rectArea = testRectangle.getArea(); //double
  testRectangle.move({22, 21});
  BOOST_CHECK_CLOSE(rectArea, testRectangle.getArea(), IMPRECISION);
}

BOOST_AUTO_TEST_CASE(constantAreaMovingWithOffset)
{
  ustinov::Rectangle testRectangle({2, 3}, 4, 5);
  const auto rectArea = testRectangle.getArea(); //double
  testRectangle.move(22, 20);
  BOOST_CHECK_CLOSE(rectArea, testRectangle.getArea(), IMPRECISION);
}

BOOST_AUTO_TEST_CASE(changeAfterScalingIncrease)
{
  ustinov::Rectangle testRectangle({4, 7}, 14, 19);
  const auto testing_area = testRectangle.getArea(); //double
  const auto parameter = 2; //double
  testRectangle.scale(parameter);
  BOOST_CHECK_CLOSE(testRectangle.getArea(), testing_area * parameter * parameter, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(changeAfterScalingDecrease)
{
  ustinov::Rectangle testRectangle({4, 7}, 14, 19);
  const auto testing_area = testRectangle.getArea(); //double
  const auto parameter = 0.5; //double
  testRectangle.scale(parameter);
  BOOST_CHECK_CLOSE(testRectangle.getArea(), testing_area * parameter * parameter, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(incorrectWidth)
{
  BOOST_CHECK_THROW(ustinov::Rectangle testRectangle({4, 2}, -15, 2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(incorrectHeight)
{
  BOOST_CHECK_THROW(ustinov::Rectangle testRectangle({3, 1}, 33, -8), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(inncorrectCoefficientScale)
{
  ustinov::Rectangle testRectangle {{12, 3}, 5, 2};
  BOOST_CHECK_THROW(testRectangle.scale(-5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testRectangleRotation)
{
  ustinov::Rectangle rectangle({6, 4}, 10, 5);

  std::shared_ptr<ustinov::Rectangle> testRectangle = std::make_shared<ustinov::Rectangle>(rectangle);

  const auto areaBefore = testRectangle->getArea();
  ustinov::rectangle_t rectFrameBefore = testRectangle->getFrameRect();

  testRectangle->rotate(90);

  ustinov::rectangle_t rectFrameAfter = testRectangle->getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore, testRectangle->getArea(), IMPRECISION);
  BOOST_CHECK_CLOSE(rectFrameBefore.width, rectFrameAfter.height, IMPRECISION);
  BOOST_CHECK_CLOSE(rectFrameBefore.height, rectFrameAfter.width, IMPRECISION);
  BOOST_CHECK_CLOSE(rectFrameBefore.pos.x, rectFrameAfter.pos.x, IMPRECISION);
  BOOST_CHECK_CLOSE(rectFrameBefore.pos.y, rectFrameAfter.pos.y, IMPRECISION);
}


BOOST_AUTO_TEST_SUITE_END()
