#include <stdexcept>
#include <iostream>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

#define IMPRECISION 0.001

using shpPtr = std::shared_ptr<ustinov::Shape>;

BOOST_AUTO_TEST_SUITE(TestCompositeShape)

BOOST_AUTO_TEST_CASE(testConstructorAndFrameX)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);


  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape composite_shape;
  composite_shape.add(circlePointer);
  composite_shape.add(rectPointer);

  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, circle.getFrameRect().pos.x, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(changeAreaAfterAdd)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);


  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape composite_shape(circlePointer);
  double areaBefore = composite_shape.getArea();

  composite_shape.add(rectPointer);
  double areaAfter = composite_shape.getArea();

  BOOST_CHECK_CLOSE(areaBefore + rect.getArea(), areaAfter, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(changeAreaAfterDelete)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape composite_shape(circlePointer);
  composite_shape.add(rectPointer);
  double areaBefore = composite_shape.getArea();

  composite_shape.remove(rectPointer);
  double areaAfter = composite_shape.getArea();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter + rect.getArea(), IMPRECISION);
}

BOOST_AUTO_TEST_CASE(ConstantAreaAfterMovingTheCenter)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);


  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape composite_shape(circlePointer);
  composite_shape.add(rectPointer);

  const auto area = composite_shape.getArea(); //double
  composite_shape.move({2, 3});
  BOOST_CHECK_CLOSE(area, composite_shape.getArea(), IMPRECISION);
}

BOOST_AUTO_TEST_CASE(ConstantParamsAfterMovingOffSet)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape composite_shape(circlePointer);
  composite_shape.add(rectPointer);

  const auto area = composite_shape.getArea(); //double
  const ustinov::rectangle_t beforeRect = composite_shape.getFrameRect();
  composite_shape.move(2, 3);
  const ustinov::rectangle_t afterRect = composite_shape.getFrameRect();

  BOOST_CHECK_CLOSE(area, composite_shape.getArea(), IMPRECISION);
  BOOST_CHECK_CLOSE(beforeRect.height,afterRect.height, IMPRECISION);
  BOOST_CHECK_CLOSE(beforeRect.width,afterRect.width, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(inncorrectPointer)
{
  ustinov::Rectangle rect({2, 6}, 5, 10);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);
  ustinov::CompositeShape compositeShape(rectPointer);

  BOOST_CHECK_THROW(ustinov::CompositeShape composite_shape(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.add(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ConstantAreaScalingIncrease)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape composite_shape(circlePointer);
  composite_shape.add(rectPointer);

  const auto area = composite_shape.getArea(); //double
  const auto coefficient = 3;
  composite_shape.scale(coefficient);
  BOOST_CHECK_CLOSE(composite_shape.getArea(), area * coefficient * coefficient, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(ConstantAreaScalingDecrease)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape composite_shape(circlePointer);
  composite_shape.add(rectPointer);

  const auto area = composite_shape.getArea(); //double
  const auto coefficient = 0.5;
  composite_shape.scale(coefficient);
  BOOST_CHECK_CLOSE(composite_shape.getArea(), area * coefficient * coefficient, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(TestConstructorCopy)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape composite_shape(circlePointer);
  composite_shape.add(rectPointer);

  ustinov::CompositeShape compositeForTest(composite_shape);

  BOOST_CHECK_CLOSE(composite_shape.getArea(), compositeForTest.getArea(), IMPRECISION);
  BOOST_CHECK_EQUAL(composite_shape.getCount(), compositeForTest.getCount());
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, compositeForTest.getFrameRect().pos.x, IMPRECISION);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, compositeForTest.getFrameRect().pos.y, IMPRECISION);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, compositeForTest.getFrameRect().width, IMPRECISION);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, compositeForTest.getFrameRect().height, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(TestConstructorMove)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape composite_shape(circlePointer);
  composite_shape.add(rectPointer);

  const auto areaBefore = composite_shape.getArea(); //double
  const auto rectangleTest = composite_shape.getFrameRect(); //ustinov::rectangle_t
  ustinov::CompositeShape compositeForTest(std::move(composite_shape));

  BOOST_CHECK_CLOSE(areaBefore, compositeForTest.getArea(), IMPRECISION);
  BOOST_CHECK_CLOSE(rectangleTest.width, compositeForTest.getFrameRect().width, IMPRECISION);
  BOOST_CHECK_CLOSE(rectangleTest.height, compositeForTest.getFrameRect().height, IMPRECISION);
  BOOST_CHECK_CLOSE(rectangleTest.pos.x, compositeForTest.getFrameRect().pos.x, IMPRECISION);
  BOOST_CHECK_CLOSE(rectangleTest.pos.y, compositeForTest.getFrameRect().pos.y, IMPRECISION);
}


BOOST_AUTO_TEST_CASE(testCopyOperator)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape composite_shape;
  composite_shape.add(circlePointer);
  composite_shape.add(rectPointer);

  ustinov::CompositeShape compositeForTest;

  compositeForTest = composite_shape;

  BOOST_CHECK_CLOSE(composite_shape.getArea(), compositeForTest.getArea(), IMPRECISION);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, compositeForTest.getFrameRect().pos.x, IMPRECISION);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, compositeForTest.getFrameRect().pos.y, IMPRECISION);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, compositeForTest.getFrameRect().height, IMPRECISION);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, compositeForTest.getFrameRect().width, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(testMoveOperator)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape composite_shape;
  composite_shape.add(circlePointer);
  composite_shape.add(rectPointer);

  ustinov::CompositeShape compositeForTest;

  const auto areaBefore = composite_shape.getArea(); //double
  const auto rectangleTest = composite_shape.getFrameRect(); //ustinov::rectangle_t

  compositeForTest = std::move(composite_shape);

  BOOST_CHECK_CLOSE(areaBefore, compositeForTest.getArea(), IMPRECISION);
  BOOST_CHECK_CLOSE(rectangleTest.pos.x, compositeForTest.getFrameRect().pos.x, IMPRECISION);
  BOOST_CHECK_CLOSE(rectangleTest.pos.y, compositeForTest.getFrameRect().pos.y, IMPRECISION);
  BOOST_CHECK_CLOSE(rectangleTest.height, compositeForTest.getFrameRect().height, IMPRECISION);
  BOOST_CHECK_CLOSE(rectangleTest.width, compositeForTest.getFrameRect().width, IMPRECISION);
}

BOOST_AUTO_TEST_CASE(TestingNegativeParameters)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape composite_shape(circlePointer);
  composite_shape.add(rectPointer);

  BOOST_CHECK_THROW(composite_shape.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestingAmountAfterMoving)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);


  ustinov::CompositeShape composite_shape(circlePointer);
  composite_shape.add(rectPointer);

  auto amountBefore = composite_shape.getCount();
  composite_shape.move(2, 5);
  BOOST_CHECK_EQUAL(composite_shape.getCount(), amountBefore);
}

BOOST_AUTO_TEST_CASE(checkCompositeRotateArea)
{
  ustinov::Circle circle({2, 4}, 6);
  ustinov::Rectangle rect({2, 6}, 5, 10);

  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);
  shpPtr rectPointer = std::make_unique<ustinov::Rectangle>(rect);

  ustinov::CompositeShape composite_shape(circlePointer);
  composite_shape.add(rectPointer);

  composite_shape.rotate(90);
  const auto areaAfterRotate = composite_shape.getArea();

  BOOST_CHECK_CLOSE(areaAfterRotate, composite_shape.getArea(), IMPRECISION);
}

BOOST_AUTO_TEST_SUITE_END()
