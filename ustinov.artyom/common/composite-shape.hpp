#ifndef _COMPSHAPE_
#define _COMPSHAPE_

#include <memory>
#include "shape.hpp"

namespace ustinov
{
  class CompositeShape : public Shape
  {
  public:
    using shpPtr = std::shared_ptr<Shape>;

    CompositeShape();

    CompositeShape(const CompositeShape &compositeShape);
    CompositeShape(CompositeShape &&compositeShape) noexcept;

    explicit CompositeShape(shpPtr);

    ~CompositeShape() noexcept override = default;

    CompositeShape &operator =(const CompositeShape &);
    CompositeShape &operator =(CompositeShape &&) noexcept;
    shpPtr operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;

    void move(const point_t &goal)  noexcept override;
    void move(double dx, double dy) noexcept override;

    void scale(double coefficient) override;
    void rotate(double angle) noexcept override;

    void show() const noexcept override;

    void add(shpPtr shape);
    void remove(size_t index);
    void remove(shpPtr shape);
    size_t getCount() const noexcept;
    void swap(CompositeShape &composite) noexcept;

  private:
    size_t counter_;
    double angle_;
    std::unique_ptr<shpPtr []> arrayOfShape_;
  };
}

#endif //A3_COMPOSITESHAPE_HPP
