#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace ustinov
{
  class Circle: public Shape
  {
  public:
    Circle(const point_t &center, double radius);

    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;

    void move(const point_t &goal) noexcept override;
    void move(double dx, double dy) noexcept override;

    void scale(double coefficient) override;
    void rotate(double angle) noexcept override;

    void show() const noexcept override;

  private:
    point_t center_;
    double radius_;
  };
};
#endif /* Circle_hpp */
