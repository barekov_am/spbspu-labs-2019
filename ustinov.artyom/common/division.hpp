#ifndef SECTION_HPP
#define SECTION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

using shpPtr = std::shared_ptr<ustinov::Shape>;

namespace ustinov
{
  bool superposition(shpPtr shape1, shpPtr shape2);

  Matrix section(const CompositeShape &shape);
}

#endif
