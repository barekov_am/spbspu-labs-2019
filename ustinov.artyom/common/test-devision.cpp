#include <boost/test/auto_unit_test.hpp>
#include "matrix.hpp"
#include "division.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
BOOST_AUTO_TEST_SUITE(partitionTestSuite)

BOOST_AUTO_TEST_CASE(correctWorkingIsOverlapping)
{
  ustinov::Rectangle rectangle1({1, 1}, 2, 4);
  ustinov::Rectangle rectangle2({2, 2}, 4, 5);
  ustinov::Circle circle({20, 22}, 5);

  shpPtr rect1Pointer = std::make_unique<ustinov::Rectangle>(rectangle1);
  shpPtr rect2Pointer = std::make_unique<ustinov::Rectangle>(rectangle2);
  shpPtr circlePointer = std::make_unique<ustinov::Circle>(circle);

  BOOST_CHECK(ustinov::superposition(rect1Pointer, rect2Pointer));
  BOOST_CHECK(!ustinov::superposition(rect1Pointer, circlePointer));
}

BOOST_AUTO_TEST_CASE(sectionValidation)
{
  ustinov::Circle circle1({0, 0}, 3);
  ustinov::Rectangle rectangle1({10, 10}, 2, 1);

  ustinov::Circle circle2({0, 1}, 2);
  ustinov::Rectangle rectangle2({11, 10}, 2, 3);

  shpPtr rect1Pointer = std::make_unique<ustinov::Rectangle>(rectangle1);
  shpPtr rect2Pointer = std::make_unique<ustinov::Rectangle>(rectangle2);

  shpPtr circle1Pointer = std::make_unique<ustinov::Circle>(circle1);
  shpPtr circle2Pointer = std::make_unique<ustinov::Circle>(circle2);

  ustinov::CompositeShape testComposite;

  testComposite.add(rect1Pointer);
  testComposite.add(rect2Pointer);
  testComposite.add(circle1Pointer);
  testComposite.add(circle2Pointer);

  ustinov::Matrix testMatrix = ustinov::section(testComposite);

  BOOST_CHECK(testMatrix[0][0] == rect1Pointer);
  BOOST_CHECK(testMatrix[0][1] == circle1Pointer);
  BOOST_CHECK(testMatrix[1][0] == rect2Pointer);
  BOOST_CHECK(testMatrix[1][1] == circle2Pointer);
}

BOOST_AUTO_TEST_SUITE_END();
