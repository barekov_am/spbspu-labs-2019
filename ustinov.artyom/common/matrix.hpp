#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>

#include "shape.hpp"
#include "base-types.hpp"

namespace ustinov
{
  class Matrix
  {
  public:
    using shpPtr = std::shared_ptr<Shape>;

    Matrix();

    Matrix(const Matrix &);
    Matrix(Matrix &&) noexcept;

    ~Matrix() noexcept = default;

    Matrix &operator =(const Matrix &);
    Matrix &operator =(Matrix &&) noexcept;

    bool operator !=(const Matrix &) const;
    bool operator ==(const Matrix &) const;

    std::unique_ptr<shpPtr []> operator[](size_t) const;

    void add(shpPtr shape, size_t rows, size_t columns);
    void swap(Matrix &) noexcept;
    void showInfo() const noexcept;

    inline size_t getRows() const noexcept
    {
      return rows_;
    }

    inline size_t getColumns() const noexcept
    {
      return columns_;
    }

  private:
    size_t rows_, columns_;
    std::unique_ptr<shpPtr []>  matrixShape;
  };
}

#endif
