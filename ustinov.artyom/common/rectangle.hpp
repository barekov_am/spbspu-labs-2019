#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace ustinov
{
  class Rectangle: public Shape
  {
  public:
    Rectangle(const point_t &center, double width, double height);

    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;

    void move(const point_t &goal) noexcept override;
    void move(double dx, double dy) noexcept override;

    void scale(double coefficient) override;
    void rotate(double angle) noexcept override;
    point_t getCenter() const;

    void show() const noexcept override;

  private:
    point_t corners_[4]; //point array
  };
};
#endif /* Rectangle_hpp */
