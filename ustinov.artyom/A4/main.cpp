#include <iostream>
#include <memory>
#include <cmath>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "division.hpp"

int main()
{
  try
  {
    std::cout << std::endl << "________________CIRCLE_________________" << std::endl << std::endl;
    std::cout << "new Circle 1" << std::endl;
    ustinov::Circle circle1({2, 3}, 5);
    ustinov::CompositeShape::shpPtr p_circle1 = std::make_shared<ustinov::Circle>(circle1);
    circle1.move({2,3});
    circle1.rotate(90);
    circle1.scale(4);
    circle1.show();

    std::cout << "new Circle 2" << std::endl;
    ustinov::Circle circle2({-2, 11}, 9);
    ustinov::CompositeShape::shpPtr p_circle2 = std::make_shared<ustinov::Circle>(circle2);
    circle1.rotate(90);
    circle1.scale(4);
    circle1.show();

    std::cout << std::endl << "________________RECTANGLE______________" << std::endl << std::endl;

    std::cout << "new Rectangle1" << std::endl;
    ustinov::Rectangle rect1({2, 5}, 7, 9);
    ustinov::CompositeShape::shpPtr p_rect1 = std::make_shared<ustinov::Rectangle>(rect1);
    circle1.rotate(20);
    circle1.scale(2);
    circle1.show();

    std::cout << "new Rectangle 2" << std::endl;
    ustinov::Rectangle rect2({8, 8}, 8, 10);
    ustinov::CompositeShape::shpPtr p_rect2 = std::make_shared<ustinov::Rectangle>(rect2);
    circle1.rotate(50);
    circle1.scale(2);
    circle1.show();

    std::cout << std::endl << "________________COMPOSITE_SHAPE________" << std::endl;

    ustinov::CompositeShape composite_shape;

    composite_shape.add(p_circle2);
    composite_shape.add(p_rect2);

    composite_shape.show();

    std::cout << std::endl << "\"________________MATRIX_________________\"" << std::endl;

    auto matrix = ustinov::section(composite_shape); //ustinov::Matrix
    matrix.showInfo();
  }

  catch (std::exception &error)
  {
    std::cerr << error.what();
    return 1;
  }

  catch (...)
  {
    std::cerr << "Что - то явно пошло не так";
    return 1;
  }

  return 0;

}
