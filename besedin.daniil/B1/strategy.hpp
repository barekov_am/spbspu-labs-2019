#ifndef B1_STRATEGY_HPP
#define B1_STRATEGY_HPP

template <typename Container>
struct StrategyBraces
{
  using valueType = typename Container::value_type;

  static unsigned int begin(Container&)
  {
    return 0;
  }

  static unsigned int end(Container& container)
  {
    return container.size();
  }

  static valueType& get(Container& container, unsigned int index)
  {
    return container[index];
  }
};

template <typename Container>
struct StrategyAt
{
  using valueType = typename Container::value_type;

  static unsigned int begin(Container&)
  {
    return 0;
  }

  static unsigned int end(Container& container)
  {
    return container.size();
  }

  static valueType& get(Container& container, unsigned int index)
  {
    return container.at(index);
  }
};

template <typename Container>
struct StrategyIterator
{
  using valueType = typename Container::value_type;

  static auto begin(Container& container)
  {
    return container.begin();
  }

  static auto end(Container& container)
  {
    return container.end();
  }

  template <typename Iterator>
  static valueType& get(Container&, Iterator iterator)
  {
    return *iterator;
  }
};

#endif
