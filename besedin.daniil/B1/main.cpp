#include <iostream>
#include <stdexcept>
#include <cstdlib>
#include <random>
#include <ctime>

void task1(const char* direction);
void task2(const char* file);
void task3();
void task4(size_t size, const char* direction);

int main(int argc, char* argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4))
    {
      std::cerr << "Invalid number of arguments";
      return 1;
    }

    char* ptr = nullptr;
    int task = std::strtol(argv[1], &ptr, 10);

    if (*ptr)
    {
      std::cerr << "Invalid argument of direction";
      return 1;
    }

    switch(task)
    {
      case 1:
      {
        if (argc != 3)
        {
          std::cerr << "Invalid number of arguments";
          return 1;
        }

        task1(argv[2]);
        break;
      }

      case 2:
      {
        if (argc != 3)
        {
          std::cerr << "Invalid number of arguments";
          return 1;
        }

        if (sizeof(argv[2]) == 0)
        {
          std::cerr << "File is not exist";
          return 1;
        }

        task2(argv[2]);
        break;
      }

      case 3:
      {
        if (argc != 2)
        {
          std::cerr << "Invalid number of arguments";
          return 1;
        }

        task3();
        break;
      }

      case 4:
      {
        if (argc != 4)
        {
          std::cerr << "Invalid number of arguments";
          return 1;
        }

        char* sizePtr = nullptr;
        int size = std::strtol(argv[3], &sizePtr, 10);

        if (*sizePtr)
        {
          std::cerr << "Invalid argument of direction";
          return 1;
        }

        task4(size, argv[2]);
        break;
      }

      default:
      {
        std::cerr << "Invalid number of task";
        return 1;
      }
    }
  }

  catch(const std::exception& e)
  {
    std::cerr << e.what();
    return 1;
  }

  return 0;
}

void fillRandom(double* array, size_t size)
{
  std::mt19937 rng(time(0));
  std::uniform_real_distribution<double> distribution(-1.0, 1.0);

  for (size_t i = 0; i < size; i++)
  {
    array[i] = distribution(rng);
  }
}
