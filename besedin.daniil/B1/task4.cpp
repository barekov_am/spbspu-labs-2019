#include <stdexcept>
#include <vector>

#include "sort.hpp"
#include "strategy.hpp"

void fillRandom(double* array, size_t size);

void task4(size_t size, const char* direction)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size must be more than 0");
  }

  std::vector<double> vector;
  vector.resize(size);
  fillRandom(&vector[0], size);

  print(vector);
  sort<StrategyAt>(vector, direction);
  print(vector);
}
