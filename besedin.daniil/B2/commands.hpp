#ifndef B2_COMMANDS_HPP
#define B2_COMMANDS_HPP

#include <string>

template <typename T>
class QueueWithPriority;

void executeAdd(QueueWithPriority<std::string>& queue, std::stringstream& in);
void executeGet(QueueWithPriority<std::string>& queue, std::stringstream& in);
void executeAccelerate(QueueWithPriority<std::string>& queue, std::stringstream& in);

#endif //B2_COMMANDS_HPP
