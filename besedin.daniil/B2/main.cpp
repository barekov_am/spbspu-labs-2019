#include <iostream>

#include "tasks.hpp"

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Invalid number of arguments";
      return 1;
    }

    char *ptr = nullptr;
    int task = std::strtol(argv[1], &ptr, 10);

    if (*ptr)
    {
      std::cerr << "Invalid argument of task";
      return 1;
    }

    switch (task)
    {
      case 1:
      {
        task1();
        break;
      }

      case 2:
      {
        task2();
        break;
      }

      default:
      {
        std::cerr << "Invalid number of task";
        return 1;
      }
    }
  }

  catch (const std::ios_base::failure& e)
  {
    std::cerr << e.what();
    return 1;
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what();
    return 2;
  }

  return 0;
}
