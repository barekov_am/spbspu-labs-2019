#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

class Functor
{
public:
  Functor();
  void operator ()(long long int num);
  long long int getMin() const;
  long long int getMax() const;
  long double getMean() const;
  long long int getPositive() const;
  long long int getNegative() const;
  long long int getOddSum() const;
  long long int getEvenSum() const;
  bool compare() const;
  long long int getCount() const;

private:
  long long int max_;
  long long int min_;
  long long int count_;
  long long int positive_;
  long long int negative_;
  long long int oddSum_;
  long long int evenSum_;
  long long int first_;
  long long int last_;
};

#endif
