#include <iostream>
#include <algorithm>

#include "shape.hpp"
#include "functions.hpp"

const int NUM_OF_TRIANGLE_TOPS = 3;
const int NUM_OF_RECTANGLE_TOPS = 4;
const int NUM_OF_PENTAGON_TOPS = 5;

void task2()
{
  int totalCountOfTops = 0;
  int numOfTriangles = 0;
  int numOfRectangles = 0;
  int numOfSquares = 0;
  std::vector<Shape> container;
  Shape tops;
  std::string str;

  readShapes(container, str);

  std::for_each(container.begin(), container.end(), [&](const Shape& shape)
  {
    if (shape.size() == NUM_OF_TRIANGLE_TOPS)
    {
      ++numOfTriangles;
    }

    if (shape.size() == NUM_OF_RECTANGLE_TOPS)
    {
      if (isRectangle(shape))
      {
        if (isSquare(shape))
        {
          ++numOfSquares;
        }

        ++numOfRectangles;
      }
    }

    totalCountOfTops += shape.size();
  });

  container.erase(std::remove_if(container.begin(), container.end(), [](const Shape& shape)
  { return shape.size() == NUM_OF_PENTAGON_TOPS; }), container.end());

  for (std::vector<Shape>::size_type i = 0; i != container.size(); i++)
  {
    tops.push_back(container[i][1]);
  }

  std::sort(container.begin(), container.end(), isLess);

  std::cout << "Vertices: " << totalCountOfTops << std::endl;
  std::cout << "Triangles: " << numOfTriangles << std::endl;
  std::cout << "Squares: " << numOfSquares << std::endl;
  std::cout << "Rectangles: " << numOfRectangles << std::endl;
  std::cout << "Points: ";

  for (Shape::size_type i = 0; i != tops.size(); i++)
  {
    std::cout << '(' << tops[i].x << ';' << tops[i].y << ") ";
  }

  std::cout << std::endl;
  std::cout << "Shapes: " << std::endl;

  for (std::vector<Shape>::size_type i = 0; i != container.size(); i++)
  {
    std::cout << container[i].size();

    for (Shape::size_type j = 0; j != container[i].size(); j++)
    {
      std::cout << " (" << container[i][j].x << ';' << container[i][j].y << ") ";
    }

    std::cout << std::endl;
  }
}
