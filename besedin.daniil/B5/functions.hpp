#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <iostream>

#include "shape.hpp"

Shape readPoints(std::string& str, int tops);
void readShapes(std::vector<Shape>& vector, std::string& str);
int getSquaredLength(const Point_t& point1, const Point_t& point2);
bool isRectangle(const Shape& shape);
bool isSquare(const Shape& shape);
bool isLess(const Shape& l, const Shape& r);

#endif
