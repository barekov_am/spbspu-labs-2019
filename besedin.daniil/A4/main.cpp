#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

void printFrameRect(const besedin::Shape& shape)
{
  besedin::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "Width: " << frameRect.width << "  ";
  std::cout << "Height: " << frameRect.height << "  ";
  std::cout << "Center: ( " << shape.getCenter().x << " , " << shape.getCenter().y << " )" << std::endl;
}

void printArea(const besedin::Shape& shape)
{
  std::cout << "Area: " << shape.getArea() << std::endl;
}

void printCenter(const besedin::Shape& shape)
{
  std::cout << "Center: ( " << shape.getCenter().x << " , " << shape.getCenter().y << " )" << std::endl;
}

void printSize(const besedin::CompositeShape& compositeShape)
{
  std::cout << "Size composite shape: " << compositeShape.size() << std::endl;
}

int main()
{
  const besedin::point_t pointCenter = {5.0, 5.0};
  besedin::CompositeShape::shape_ptr p_rectangle = std::make_shared<besedin::Rectangle>(3.0, 3.0, pointCenter);
  besedin::CompositeShape::shape_ptr p_circle = std::make_shared<besedin::Circle>(7.0, pointCenter);
  const besedin::point_t pointNewCenter = {15.0, 15.0};
  const double scale = 2.0;

  std::cout << "Rectangle" << std::endl;
  printFrameRect(*p_rectangle);
  printArea(*p_rectangle);
  p_rectangle->scale(scale);
  printArea(*p_rectangle);
  p_rectangle->move(3.0, 3.0);
  printCenter(*p_rectangle);
  std::cout << std::endl;

  std::cout << "Circle" << std::endl;
  printFrameRect(*p_circle);
  printArea(*p_circle);
  p_circle->scale(scale);
  printArea(*p_circle);
  p_circle->move(pointNewCenter);
  printCenter(*p_circle);
  std::cout << std::endl;

  std::cout << "Composite Shape" << std::endl;
  besedin::CompositeShape compositeShape;
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  printSize(compositeShape);
  printFrameRect(compositeShape);
  printArea(compositeShape);
  compositeShape.scale(scale);
  printArea(compositeShape);
  compositeShape.move(pointCenter);
  printCenter(compositeShape);
  compositeShape.remove(1);
  printSize(compositeShape);

  besedin::CompositeShape::shape_ptr p_rectangle2 = std::make_shared<besedin::Rectangle>(3.0, 3.0, pointNewCenter);
  compositeShape.add(p_rectangle2);
  compositeShape.add(p_circle);

  besedin::Matrix matrix = besedin::partition(compositeShape);
  for (unsigned int i = 0; i < matrix.getRows(); i++)
  {
    for (unsigned int j = 0; j < matrix.getColumns(); j++)
    {
      if (matrix.get(i, j) != nullptr)
      {
        std::cout << "[" << i << ", " << j << "] FrameRect : ";
        printFrameRect(*matrix.get(i, j));
      }
    }
  }

  return 0;
}
