#include <utility>

#include "matrix.hpp"
#include <stdexcept>

besedin::Matrix::Matrix() :
  rows_(0),
  columns_(0)
{
}

besedin::Matrix::Matrix(const Matrix& other) :
  rows_(other.rows_),
  columns_(other.columns_),
  list_(std::make_unique<Shape::shape_ptr[]>(other.rows_ * other.columns_))
{
  for (unsigned int i = 0; i < rows_ * columns_; i++)
  {
    list_[i] = other.list_[i];
  }
}

besedin::Matrix::Matrix(Matrix&& other) :
  rows_(other.rows_),
  columns_(other.columns_),
  list_(std::move(other.list_))
{
}

besedin::Matrix& besedin::Matrix::operator =(const Matrix& other)
{
  if (this != &other)
  {
    std::unique_ptr<Shape::shape_ptr[]> tmpList(std::make_unique<Shape::shape_ptr[]>(other.rows_ * other.columns_));
    rows_ = other.rows_;
    columns_ = other.columns_;
    for (unsigned int i = 0; i < rows_ * columns_; i++)
    {
      list_[i] = other.list_[i];
    }
    list_.swap(tmpList);
  }
  return *this;
}

besedin::Matrix& besedin::Matrix::operator =(Matrix&& other)
{
  if (this != &other)
  {
    rows_ = other.rows_;
    columns_ = other.columns_;
    list_ = std::move(other.list_);
  }
  return *this;
}

std::unique_ptr<besedin::Shape::shape_ptr[]> besedin::Matrix::operator [](unsigned int index) const
{
  if (index >= rows_)
  {
    throw std::invalid_argument("Wrong index");
  }

  std::unique_ptr<Shape::shape_ptr[]> tmpList(std::make_unique<Shape::shape_ptr[]>(columns_));
  for (unsigned int i = 0; i < columns_; i++)
  {
    tmpList[i] = list_[index * columns_ + i];
  }
  return tmpList;
}

bool besedin::Matrix::operator ==(const Matrix& other) const
{
  if ((rows_ != other.rows_) || (columns_ != other.columns_))
  {
    return false;
  }
  for (unsigned int i = 0; i < (rows_ * columns_); i++)
  {
    if (list_[i] != other.list_[i])
    {
      return false;
    }
  }
  return true;
}

bool besedin::Matrix::operator !=(const Matrix& other) const
{
  return !(*this == other);
}

besedin::Shape::shape_ptr besedin::Matrix::get(unsigned int i, unsigned int j) const
{
  unsigned int index = i * columns_ + j;
  if (index > rows_ * columns_)
  {
    throw std::invalid_argument("Wrong index");
  }

  return list_[index];
}

void besedin::Matrix::add(Shape::shape_ptr shape, unsigned int row, unsigned int column)
{
  size_t tmpRows = rows_;
  if (rows_ == row)
  {
    tmpRows = rows_ + 1;
  }
  size_t tmpColumns = columns_;
  if (columns_ == column)
  {
    tmpColumns = columns_ + 1;
  }
  std::unique_ptr<Shape::shape_ptr[]> tmpList(std::make_unique<Shape::shape_ptr[]>(tmpRows * tmpColumns));

  for (unsigned int i = 0; i < tmpRows; i++)
  {
    for (unsigned int j = 0; j < tmpColumns; j++)
    {
      if ((i != rows_) && (j != columns_))
      {
        tmpList[i * tmpColumns + j] = list_[i * columns_ + j];
      }
    }
  }
  tmpList[row * tmpColumns + column] = std::move(shape);
  list_.swap(tmpList);
  rows_ = tmpRows;
  columns_ = tmpColumns;
}

size_t besedin::Matrix::getRows() const
{
  return rows_;
}

size_t besedin::Matrix::getColumns() const
{
  return columns_;
}
