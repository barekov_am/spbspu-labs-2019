#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace besedin
{
  class Shape
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void move(const point_t& newPos) = 0;
    virtual void scale(double scale) = 0;
    virtual point_t getCenter() const = 0;
    virtual void rotate(double angle) = 0;
  };
}

#endif
