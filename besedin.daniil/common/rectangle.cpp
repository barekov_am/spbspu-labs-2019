#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

const double FULL_ANGLE = 360.0;
const double RADIX_GRAD = 57.2957795;

besedin::Rectangle::Rectangle(const double width, const double height, const point_t& center) :
  width_(width),
  height_(height),
  center_(center),
  angle_(0.0)
{
  if (width_ <= 0.0)
  {
    throw std::invalid_argument("Wrong rectangle width");
  }
  if (height_ <= 0.0)
  {
    throw std::invalid_argument("Wrong rectangle height");
  }
}

double besedin::Rectangle::getArea() const
{
  return width_ * height_;
}

besedin::rectangle_t besedin::Rectangle::getFrameRect() const
{
  const double radixAngle = angle_ / RADIX_GRAD;
  const double width = fabs(width_ * cos(radixAngle)) + fabs(height_ * sin(radixAngle));
  const double height = fabs(width_ * sin(radixAngle)) + fabs(height_ * cos(radixAngle));
  return {width, height, center_};
}

void besedin::Rectangle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void besedin::Rectangle::move(const point_t& newPos)
{
  center_ = newPos;
}

void besedin::Rectangle::scale(const double scale)
{
  if (scale <= 0.0)
  {
    throw std::invalid_argument("Wrong scaling factor");
  }
  width_ *= scale;
  height_ *= scale;
}

besedin::point_t besedin::Rectangle::getCenter() const
{
  return center_;
}

void besedin::Rectangle::rotate(const double angle)
{
  angle_ += angle;
  angle_ = fmod(angle_, FULL_ANGLE);
}
