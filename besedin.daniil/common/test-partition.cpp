#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

const double EPSILON = 0.01;
using shape_ptr = std::shared_ptr<besedin::Shape>;

BOOST_AUTO_TEST_SUITE(testForPartition)

BOOST_AUTO_TEST_CASE(checkPartition)
{
  shape_ptr p_circle1 = std::make_shared<besedin::Circle>(1, besedin::point_t{10, 10});
  shape_ptr p_circle2 = std::make_shared<besedin::Circle>(1, besedin::point_t{100, 100});
  shape_ptr p_rectangle1 = std::make_shared<besedin::Rectangle>(1, 1, besedin::point_t{1000, 1000});
  shape_ptr p_rectangle2 = std::make_shared<besedin::Rectangle>(10, 10, besedin::point_t{10, 10});

  besedin::CompositeShape compositeShape;
  compositeShape.add(p_circle1);
  compositeShape.add(p_circle2);
  compositeShape.add(p_rectangle1);
  compositeShape.add(p_rectangle2);

  besedin::Matrix matrix = besedin::partition(compositeShape);

  BOOST_REQUIRE_EQUAL(2, matrix.getRows());
  BOOST_REQUIRE_EQUAL(3, matrix.getColumns());
  BOOST_CHECK(matrix[0][0] == p_circle1);
  BOOST_CHECK(matrix[0][1] == p_circle2);
  BOOST_CHECK(matrix[0][2] == p_rectangle1);
  BOOST_CHECK(matrix[1][0] == p_rectangle2);
  BOOST_CHECK(matrix[0][0] != p_circle2);
}

BOOST_AUTO_TEST_SUITE_END()
