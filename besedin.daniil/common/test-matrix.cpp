#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

using shape_ptr = std::shared_ptr<besedin::Shape>;

BOOST_AUTO_TEST_SUITE(testForMatrix)

BOOST_AUTO_TEST_CASE(throwExceptionForMatrix)
{
  shape_ptr p_circle1 = std::make_shared<besedin::Circle>(4, besedin::point_t {1, 1});
  shape_ptr p_circle2 = std::make_shared<besedin::Circle>(7, besedin::point_t {-11, 1});
  shape_ptr p_rectangle1 = std::make_shared<besedin::Rectangle>(6, 8, besedin::point_t {10,10});
  shape_ptr p_rectangle2 = std::make_shared<besedin::Rectangle>(5, 10, besedin::point_t {1, 1});
  besedin::CompositeShape compositeShape;
  compositeShape.add(p_circle2);
  compositeShape.add(p_rectangle1);
  compositeShape.add(p_circle1);
  compositeShape.add(p_rectangle2);
  besedin::Matrix matrix = besedin::partition(compositeShape);

  BOOST_CHECK_THROW(matrix[matrix.getRows()], std::invalid_argument);
  BOOST_CHECK_THROW(matrix[-1], std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestCopyOperator)
{
  shape_ptr p_circle1 = std::make_shared<besedin::Circle>(4, besedin::point_t {1, 1});
  shape_ptr p_circle2 = std::make_shared<besedin::Circle>(7, besedin::point_t {-11, 1});
  shape_ptr p_rectangle1 = std::make_shared<besedin::Rectangle>(6, 8, besedin::point_t {10,10});
  shape_ptr p_rectangle2 = std::make_shared<besedin::Rectangle>(5, 10, besedin::point_t {1, 1});
  besedin::CompositeShape compositeShape;
  compositeShape.add(p_circle2);
  compositeShape.add(p_rectangle1);
  compositeShape.add(p_circle1);
  compositeShape.add(p_rectangle2);
  besedin::Matrix matrix = besedin::partition(compositeShape);
  besedin::CompositeShape compositeShape1;
  besedin::CompositeShape compositeShape2;

  compositeShape1.add(p_circle2);
  compositeShape1.add(p_rectangle1);
  compositeShape1.add(p_circle1);
  compositeShape1.add(p_rectangle2);

  besedin::Matrix matrix1 = besedin::partition(compositeShape1);
  const besedin::Matrix& matrix2 = matrix1;
  BOOST_CHECK(matrix1 == matrix2);
}

BOOST_AUTO_TEST_CASE(TestMoveOperator)
{
  shape_ptr p_circle1 = std::make_shared<besedin::Circle>(4, besedin::point_t {1, 1});
  shape_ptr p_circle2 = std::make_shared<besedin::Circle>(7, besedin::point_t {-11, 1});
  shape_ptr p_rectangle1 = std::make_shared<besedin::Rectangle>(6, 8, besedin::point_t {10,10});
  shape_ptr p_rectangle2 = std::make_shared<besedin::Rectangle>(5, 10, besedin::point_t {1, 1});
  besedin::CompositeShape compositeShape;
  compositeShape.add(p_circle2);
  compositeShape.add(p_rectangle1);
  compositeShape.add(p_circle1);
  compositeShape.add(p_rectangle2);

  besedin::Matrix matrix1 = besedin::partition(compositeShape);
  besedin::Matrix matrix2;
  matrix1 = std::move(matrix2);
  besedin::Matrix matrix3 = matrix1;

  BOOST_CHECK(matrix2 == matrix3);
}

BOOST_AUTO_TEST_CASE(TestCopyConstructor)
{
  shape_ptr p_circle1 = std::make_shared<besedin::Circle>(4, besedin::point_t {1, 1});
  shape_ptr p_circle2 = std::make_shared<besedin::Circle>(7, besedin::point_t {-11, 1});
  shape_ptr p_rectangle1 = std::make_shared<besedin::Rectangle>(6, 8, besedin::point_t {10,10});
  shape_ptr p_rectangle2 = std::make_shared<besedin::Rectangle>(5, 10, besedin::point_t {1, 1});
  besedin::CompositeShape compositeShape;
  compositeShape.add(p_circle2);
  compositeShape.add(p_rectangle1);
  compositeShape.add(p_circle1);
  compositeShape.add(p_rectangle2);
  besedin::Matrix matrix1 = besedin::partition(compositeShape);
  besedin::Matrix matrix2(matrix1);

  BOOST_CHECK(matrix1 == matrix2);
}

BOOST_AUTO_TEST_CASE(TestMoveConstructor)
{
  shape_ptr p_circle1 = std::make_shared<besedin::Circle>(4, besedin::point_t {1, 1});
  shape_ptr p_circle2 = std::make_shared<besedin::Circle>(7, besedin::point_t {-11, 1});
  shape_ptr p_rectangle1 = std::make_shared<besedin::Rectangle>(6, 8, besedin::point_t {10,10});
  shape_ptr p_rectangle2 = std::make_shared<besedin::Rectangle>(5, 10, besedin::point_t {1, 1});
  besedin::CompositeShape compositeShape;
  compositeShape.add(p_circle2);
  compositeShape.add(p_rectangle1);
  compositeShape.add(p_circle1);
  compositeShape.add(p_rectangle2);
  besedin::Matrix matrix1 = besedin::partition(compositeShape);
  besedin::Matrix matrix3(matrix1);
  besedin::Matrix matrix2(std::move(matrix1));

  BOOST_CHECK(matrix2 == matrix3);
}

BOOST_AUTO_TEST_SUITE_END()
