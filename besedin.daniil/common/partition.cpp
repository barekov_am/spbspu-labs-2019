#include "partition.hpp"
#include <cmath>

besedin::Matrix besedin::partition(const besedin::CompositeShape& compositeShape)
{
  Matrix matrix;
  unsigned int correctRow = 0;
  unsigned int correctColumn = 0;
  for (unsigned int i = 0; i < compositeShape.size(); i++)
  {
    for (unsigned int j = 0; j < matrix.getRows(); j++)
    {
      for (unsigned int k = 0; k < matrix.getColumns(); k++)
      {
        if (matrix.get(j, k) == nullptr)
        {
          correctRow = j;
          correctColumn = k;
          break;
        }

        if (intersect(compositeShape[i], matrix.get(j, k)))
        {
          correctRow = j + 1;
          correctColumn = 0;
          break;
        }
        else
        {
          correctRow = j;
          correctColumn = k + 1;
        }
      }

      if (correctRow == j)
      {
        break;
      }
    }
    matrix.add(compositeShape[i], correctRow, correctColumn);
  }
  return matrix;
}

bool besedin::intersect(const Shape::shape_ptr shape1, const Shape::shape_ptr shape2)
{
  if ((shape1 == nullptr) || (shape2 == nullptr))
  {
    return false;
  }
  const rectangle_t rect1 = shape1->getFrameRect();
  const rectangle_t rect2 = shape2->getFrameRect();
  point_t horizon1 = {rect1.pos.x - rect1.width / 2, rect1.pos.x + rect1.width / 2};
  point_t horizon2 = {rect2.pos.x - rect2.width / 2, rect2.pos.x + rect2.width / 2};
  point_t vertical1 = {rect1.pos.y - rect1.height / 2, rect1.pos.y + rect1.height / 2};
  point_t vertical2 = {rect2.pos.y - rect2.height / 2, rect2.pos.y + rect2.height / 2};

  bool horizonIntersect = (horizon1.x <= horizon2.y && horizon1.y >= horizon2.x) ||
                          (horizon2.x <= horizon1.y && horizon2.y >= horizon1.x);
  bool verticalIntersect = (vertical1.x <= vertical2.y && vertical1.y >= vertical2.x) ||
                           (vertical2.x <= vertical1.y && vertical2.y >= vertical1.x);
  return horizonIntersect && verticalIntersect;
}
