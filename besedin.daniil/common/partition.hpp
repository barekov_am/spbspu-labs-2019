#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"

namespace besedin
{
  Matrix partition(const CompositeShape& compositeShape);
  bool intersect(Shape::shape_ptr shape1, Shape::shape_ptr shape2);
}

#endif
