#include "composite-shape.hpp"
#include <stdexcept>
#include <algorithm>
#include <cmath>

besedin::CompositeShape::CompositeShape() :
  counter_(0)
{
}

besedin::CompositeShape::CompositeShape(const CompositeShape& other) :
  counter_(other.counter_),
  shapes_(std::make_unique<shape_ptr []>(other.counter_))
{
  for (unsigned int i = 0; i < counter_; i++)
  {
    shapes_[i] = other.shapes_[i];
  }
}

besedin::CompositeShape::CompositeShape(CompositeShape&& other) :
  counter_(other.counter_),
  shapes_(std::move(other.shapes_))
{
  other.counter_ = 0;
}

besedin::CompositeShape& besedin::CompositeShape::operator =(const CompositeShape& other)
{
  if (this != &other)
  {
    std::unique_ptr<shape_ptr []> tmpArray(std::make_unique<shape_ptr []>(other.counter_));
    for (unsigned int i = 0; i < other.counter_; i++)
    {
      tmpArray[i] = other.shapes_[i];
    }
    shapes_.swap(tmpArray);
    counter_ = other.counter_;
  }
  return *this;
}

besedin::CompositeShape& besedin::CompositeShape::operator =(CompositeShape&& other)
{
  if (this != &other)
  {
    counter_ = other.counter_;
    shapes_ = std::move(other.shapes_);
  }
  return *this;
}

besedin::CompositeShape::shape_ptr besedin::CompositeShape::operator [](const unsigned int index) const
{
  if (counter_ <= index)
  {
    throw std::invalid_argument("Wrong index");
  }
  return shapes_[index];
}

double besedin::CompositeShape::getArea() const
{
  double area = 0;
  for (unsigned int i = 0; i < counter_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

besedin::rectangle_t besedin::CompositeShape::getFrameRect() const
{
  if (counter_ == 0)
  {
    return {0.0, 0.0, {0.0, 0.0}};
  }
  rectangle_t tmpFrameRect = shapes_[0]->getFrameRect();
  double leftX = tmpFrameRect.pos.x - tmpFrameRect.width / 2;
  double topY = tmpFrameRect.pos.y + tmpFrameRect.height / 2;
  double rightX = tmpFrameRect.pos.x + tmpFrameRect.width / 2;
  double bottomY = tmpFrameRect.pos.y - tmpFrameRect.height / 2;
  for (unsigned int i = 1; i < counter_; i++)
  {
    tmpFrameRect = shapes_[i]->getFrameRect();
    leftX = std::min(leftX, tmpFrameRect.pos.x - tmpFrameRect.width / 2);
    topY = std::max(topY, tmpFrameRect.pos.y + tmpFrameRect.height / 2);
    rightX = std::max(rightX, tmpFrameRect.pos.x + tmpFrameRect.width / 2);
    bottomY = std::min(bottomY, tmpFrameRect.pos.y - tmpFrameRect.height / 2);
  }
  return {rightX - leftX, topY - bottomY, {(leftX + rightX) / 2, (bottomY + topY) / 2}};
}

void besedin::CompositeShape::move(const double dx, const double dy)
{
  for (unsigned int i = 0; i < counter_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void besedin::CompositeShape::move(const point_t& newPos)
{
  const double dx = newPos.x - getFrameRect().pos.x;
  const double dy = newPos.y - getFrameRect().pos.y;
  move(dx, dy);
}

void besedin::CompositeShape::scale(const double scale)
{
  if (scale <= 0.0)
  {
    throw std::invalid_argument("Wrong scaling factor");
  }
  const point_t centerComposition = getFrameRect().pos;
  for (unsigned int i = 0; i < counter_; i++)
  {
    const double dx = shapes_[i]->getFrameRect().pos.x - centerComposition.x;
    const double dy = shapes_[i]->getFrameRect().pos.y - centerComposition.y;
    shapes_[i]->move({centerComposition.x + dx * scale, centerComposition.y + dy * scale});
    shapes_[i]->scale(scale);
  }
}

besedin::point_t besedin::CompositeShape::getCenter() const
{
  return getFrameRect().pos;
}

void besedin::CompositeShape::rotate(const double angle)
{
  const point_t center = getCenter();
  for (unsigned int i = 0; i < counter_; i++)
  {
    shapes_[i]->rotate(angle);
    point_t centerShape = shapes_[i]->getCenter();
    double x = (centerShape.x - center.x) * cos(angle * M_PI / 180) -
               (centerShape.y - center.y) * sin(angle * M_PI / 180);
    double y = (centerShape.y - center.y) * cos(angle * M_PI / 180) +
               (centerShape.x - center.x) * sin(angle * M_PI / 180);
    shapes_[i]->move({center.x + x, center.y + y});
  }
}

void besedin::CompositeShape::add(shape_ptr newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Shape is nullptr");
  }
  std::unique_ptr<shape_ptr []> tmpArray(std::make_unique<shape_ptr []>(counter_ + 1));

  for (unsigned int i = 0; i < counter_; i++)
  {
    tmpArray[i] = shapes_[i];
  }
  tmpArray[counter_++] = newShape;
  shapes_.swap(tmpArray);
}

void besedin::CompositeShape::remove(const unsigned int index)
{
  if (counter_ == 0)
  {
    throw std::invalid_argument("Composite shape is empty");
  }
  if (index >= counter_)
  {
    throw std::invalid_argument("Index is outside the array");
  }
  for (unsigned int i = index; i < counter_ - 1; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  counter_--;
}

size_t besedin::CompositeShape::size() const
{
  return counter_;
}
