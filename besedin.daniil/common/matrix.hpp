#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"

namespace besedin
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix& other);
    Matrix(Matrix&& other);
    ~Matrix() = default;
    Matrix& operator =(const Matrix& other);
    Matrix& operator =(Matrix&& other);
    std::unique_ptr<Shape::shape_ptr []> operator [](unsigned int index) const;
    bool operator ==(const Matrix& other) const;
    bool operator !=(const Matrix& other) const;

    Shape::shape_ptr get(unsigned int i, unsigned int j) const;

    void add(Shape::shape_ptr shape, unsigned int row, unsigned int column);
    size_t getRows() const;
    size_t getColumns() const;

  private:
    size_t rows_;
    size_t columns_;
    std::unique_ptr<Shape::shape_ptr []> list_;
  };
}

#endif
