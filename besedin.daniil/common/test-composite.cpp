#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double EPSILON = 0.01;

BOOST_AUTO_TEST_SUITE(TestForComposite)

void equalsShape(besedin::Shape& shape1, besedin::Shape& shape2)
{
  besedin::point_t center1 = shape1.getCenter();
  besedin::point_t center2 = shape2.getCenter();
  besedin::rectangle_t frameRect1 = shape1.getFrameRect();
  besedin::rectangle_t frameRect2 = shape2.getFrameRect();

  BOOST_CHECK_CLOSE(shape1.getArea(), shape2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(center1.x, center2.x, EPSILON);
  BOOST_CHECK_CLOSE(center1.y, center2.y, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.height, frameRect2.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.width, frameRect2.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.pos.x, frameRect2.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.pos.y, frameRect2.pos.y, EPSILON);
}

void equalsCompositeShape(besedin::CompositeShape& compositeShape1, besedin::CompositeShape& compositeShape2)
{
  BOOST_CHECK_EQUAL(compositeShape1.size(), compositeShape2.size());
  equalsShape(compositeShape1, compositeShape2);
  for(unsigned int i = 0; i < compositeShape1.size(); i++)
  {
    equalsShape(*compositeShape1[i], *compositeShape2[i]);
  }
}

BOOST_AUTO_TEST_CASE(frameCompositeConstantAfterMove)
{
  besedin::Shape::shape_ptr p_rectangle = std::make_shared<besedin::Rectangle>(3, 10, besedin::point_t { 3, 5 });
  besedin::Shape::shape_ptr p_circle = std::make_shared<besedin::Circle>(4, besedin::point_t { 1, 5 });

  besedin::CompositeShape compositeShape;
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  const besedin::rectangle_t frameRectBefore = compositeShape.getFrameRect();
  const double areaBefore = compositeShape.getArea();

  compositeShape.move(5, 5);
  BOOST_CHECK_CLOSE(frameRectBefore.width, compositeShape.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height, compositeShape.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, compositeShape.getArea(), EPSILON);

  compositeShape.move({20, 20});
  BOOST_CHECK_CLOSE(frameRectBefore.width, compositeShape.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height, compositeShape.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, compositeShape.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(CompositeScaleChange)
{
  besedin::Shape::shape_ptr p_rectangle = std::make_shared<besedin::Rectangle>(3, 10, besedin::point_t { 3, 5 });
  besedin::Shape::shape_ptr p_circle = std::make_shared<besedin::Circle>(4, besedin::point_t { 1, 5 });
  besedin::CompositeShape compositeShape;
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  const double areaBefore = compositeShape.getArea();
  const double scale = 2.0;
  const besedin::point_t centerBefore = compositeShape.getCenter();

  compositeShape.scale(scale);
  BOOST_CHECK_CLOSE(areaBefore * scale * scale, compositeShape.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(compositeShape.getCenter().x, centerBefore.x, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape.getCenter().y, centerBefore.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(areaAfterRotating)
{
  besedin::Shape::shape_ptr p_rectangle = std::make_shared<besedin::Rectangle>(3, 10, besedin::point_t { 3, 5 });
  besedin::Shape::shape_ptr p_circle = std::make_shared<besedin::Circle>(4, besedin::point_t { 1, 5 });
  besedin::CompositeShape compositeShape;
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  const double areaBefore = compositeShape.getArea();
  compositeShape.rotate(33.88);

  BOOST_CHECK_CLOSE(areaBefore, compositeShape.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(frameRectAfterRotating)
{
  besedin::Shape::shape_ptr p_rectangle = std::make_shared<besedin::Rectangle>(2, 2, besedin::point_t {3, 1});
  besedin::Shape::shape_ptr p_circle = std::make_shared<besedin::Circle>(1, besedin::point_t {1, 3});
  besedin::CompositeShape compositeShape;
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  compositeShape.rotate(90);

  BOOST_CHECK_CLOSE(p_circle->getCenter().x, 1, EPSILON);
  BOOST_CHECK_CLOSE(p_circle->getCenter().y, 1, EPSILON);

  const besedin::rectangle_t frameRectBefore = compositeShape.getFrameRect();
  compositeShape.rotate(90);

  BOOST_CHECK_CLOSE(compositeShape.getCenter().x, frameRectBefore.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape.getCenter().y, frameRectBefore.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().width, frameRectBefore.height, EPSILON);
  BOOST_CHECK_CLOSE(compositeShape.getFrameRect().height, frameRectBefore.width, EPSILON);
}

BOOST_AUTO_TEST_CASE(wrongParameteresComposite)
{
  besedin::Shape::shape_ptr p_rectangle = std::make_shared<besedin::Rectangle>(3, 10, besedin::point_t { 3, 5 });
  besedin::Shape::shape_ptr p_circle = std::make_shared<besedin::Circle>(4, besedin::point_t { 1, 5 });
  besedin::CompositeShape compositeShape;
  compositeShape.add(p_rectangle);
  compositeShape.add(p_circle);
  const double scale = -10;
  BOOST_CHECK_THROW(compositeShape.scale(scale), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.remove(compositeShape.size()), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.remove(-10), std::invalid_argument);
  compositeShape.remove(1);
  compositeShape.remove(0);
  BOOST_CHECK_THROW(compositeShape.remove(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestCopyOperatorEmpty)
{
  besedin::Shape::shape_ptr p_rectangle = std::make_shared<besedin::Rectangle>(3, 10, besedin::point_t { 3, 5 });
  besedin::Shape::shape_ptr p_circle = std::make_shared<besedin::Circle>(4, besedin::point_t { 1, 5 });

  besedin::CompositeShape compositeShape1;
  compositeShape1.add(p_rectangle);
  compositeShape1.add(p_circle);

  besedin::CompositeShape compositeShape2;
  compositeShape2 = compositeShape1;
  equalsCompositeShape(compositeShape1, compositeShape2);
}

BOOST_AUTO_TEST_CASE(TestCopyConstructor)
{
  besedin::Shape::shape_ptr p_rectangle = std::make_shared<besedin::Rectangle>(3, 10, besedin::point_t { 3, 5 });
  besedin::Shape::shape_ptr p_circle = std::make_shared<besedin::Circle>(4, besedin::point_t { 1, 5 });

  besedin::CompositeShape compositeShape1;
  compositeShape1.add(p_rectangle);
  compositeShape1.add(p_circle);

  besedin::CompositeShape compositeShape2(compositeShape1);
  equalsCompositeShape(compositeShape1, compositeShape2);
}

BOOST_AUTO_TEST_CASE(TestCopyOperatorNotEmpty)
{
  besedin::Shape::shape_ptr p_rectangle = std::make_shared<besedin::Rectangle>(3, 10, besedin::point_t { 3, 5 });
  besedin::Shape::shape_ptr p_circle = std::make_shared<besedin::Circle>(4, besedin::point_t { 1, 5 });
  besedin::Shape::shape_ptr p_circle2 = std::make_shared<besedin::Circle>(5, besedin::point_t { 3, 8 });

  besedin::CompositeShape compositeShape1;
  compositeShape1.add(p_rectangle);
  compositeShape1.add(p_circle);

  besedin::CompositeShape compositeShape2;
  compositeShape2.add(p_circle2);

  compositeShape2 = compositeShape1;
  equalsCompositeShape(compositeShape1, compositeShape2);
}

BOOST_AUTO_TEST_CASE(TestMoveConstructor)
{
  besedin::Shape::shape_ptr p_rectangle = std::make_shared<besedin::Rectangle>(3, 10, besedin::point_t { 3, 5 });
  besedin::Shape::shape_ptr p_circle = std::make_shared<besedin::Circle>(4, besedin::point_t { 1, 5 });

  besedin::CompositeShape compositeShape1;
  compositeShape1.add(p_rectangle);
  compositeShape1.add(p_circle);

  besedin::CompositeShape copyCompositeShape1 = compositeShape1;
  besedin::CompositeShape compositeShape2(std::move(compositeShape1));
  equalsCompositeShape(copyCompositeShape1, compositeShape2);
}

BOOST_AUTO_TEST_CASE(TestMoveOperator)
{
  besedin::Shape::shape_ptr p_rectangle = std::make_shared<besedin::Rectangle>(3, 10, besedin::point_t { 3, 5 });
  besedin::Shape::shape_ptr p_circle = std::make_shared<besedin::Circle>(4, besedin::point_t { 1, 5 });

  besedin::CompositeShape compositeShape1;
  compositeShape1.add(p_rectangle);
  compositeShape1.add(p_circle);

  besedin::CompositeShape copyCompositeShape1 = compositeShape1;
  besedin::CompositeShape compositeShape2;
  compositeShape2 = std::move(compositeShape1);
  equalsCompositeShape(copyCompositeShape1, compositeShape2);
}

BOOST_AUTO_TEST_SUITE_END()
