#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace besedin
{
  class Circle: public Shape
  {
  public:
    Circle(double radius, const point_t& center);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t& newPos) override;
    void scale(double scale) override;
    point_t getCenter() const override;
    void rotate(double) override;

  private:
    double radius_;
    point_t center_;
  };
}

#endif
