#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace besedin
{
  class Rectangle: public Shape
  {
  public:
    Rectangle(double width, double height, const point_t& center);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t& newPos) override;
    void scale(double scale) override;
    point_t getCenter() const override;
    void rotate(double angle) override;

  private:
    double width_;
    double height_;
    point_t center_;
    double angle_;
  };
}

#endif
