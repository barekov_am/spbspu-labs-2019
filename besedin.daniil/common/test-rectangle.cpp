#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double EPSILON = 0.01;

BOOST_AUTO_TEST_SUITE(TestForRectangle)

BOOST_AUTO_TEST_CASE(frameRectangleConstantAfterMove)
{
  besedin::Rectangle rectangle(5.0, 5.0, {10.0, 10.0});
  const besedin::rectangle_t frameRectBefore = rectangle.getFrameRect();
  const double areaBefore = rectangle.getArea();

  rectangle.move(5, 5);
  BOOST_CHECK_CLOSE(frameRectBefore.width, rectangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height, rectangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), EPSILON);

  rectangle.move({20, 20});
  BOOST_CHECK_CLOSE(frameRectBefore.width, rectangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectBefore.height, rectangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(rectangleScaleChange)
{
  besedin::Rectangle rectangle(5.0, 5.0, {10.0, 10.0});
  const double areaBefore = rectangle.getArea();
  const double scale = 2.0;

  rectangle.scale(scale);
  BOOST_CHECK_CLOSE(areaBefore * scale * scale, rectangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(areaAfterRotating)
{
  besedin::Rectangle testRectangle(5.0, 5.0, {10.0, 10.0});
  const double areaBefore = testRectangle.getArea();
  testRectangle.rotate(33.88);
  BOOST_CHECK_CLOSE(areaBefore, testRectangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(frameRectAfterRotating)
{
  const double widths[] = {22.0, 45.8, 55.8, 77.7, 84.8};
  const double heights[] = {23.0, 22.8, 88.8, 89.7, 1.8};
  const double angle = 90.0;
  for (unsigned int i = 0; i < 5; i++)
  {
    besedin::Rectangle rectangle(widths[i], heights[i], {5.0, 5.0});
    rectangle.rotate(angle);

    BOOST_CHECK_CLOSE(heights[i], rectangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(widths[i], rectangle.getFrameRect().height, EPSILON);

    rectangle.rotate(angle);
    BOOST_CHECK_CLOSE(widths[i], rectangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(heights[i], rectangle.getFrameRect().height, EPSILON);
  }
}

BOOST_AUTO_TEST_CASE(wrongRectangleParameteresAndScaling)
{
  BOOST_CHECK_THROW(besedin::Rectangle rectangle(-5.0, 5.0, {10.0, 10.0}), std::invalid_argument);
  BOOST_CHECK_THROW(besedin::Rectangle rectangle(5.0, -5.0, {10.0, 10.0}), std::invalid_argument);

  const double scale = -10;
  besedin::Rectangle rectangle(5.0, 5.0, {10.0, 10.0});
  BOOST_CHECK_THROW(rectangle.scale(scale), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
