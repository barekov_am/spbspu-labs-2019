#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace besedin
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape& other);
    CompositeShape(CompositeShape&& other);
    ~CompositeShape() = default;
    CompositeShape& operator =(const CompositeShape& other);
    CompositeShape& operator =(CompositeShape&& other);
    shape_ptr operator [](unsigned int index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t& newPos) override;
    void scale(double scale) override;
    point_t getCenter() const override;
    void rotate(double angle) override;

    void add(shape_ptr newShape);
    void remove(unsigned int index);
    size_t size() const;

  private:
    size_t counter_;
    std::unique_ptr<shape_ptr []> shapes_;
  };
}

#endif
