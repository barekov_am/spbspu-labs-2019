#include "factorial-container.hpp"

FactorialIterator FactorialContainer::begin()
{
  return FactorialIterator(FactorialContainer::MIN_ITERATOR_POSITION);
}

FactorialIterator FactorialContainer::end()
{
  return FactorialIterator(FactorialContainer::MAX_ITERATOR_POSITION);
}
