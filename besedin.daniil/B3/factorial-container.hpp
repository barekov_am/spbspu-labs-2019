#ifndef B3_FACTORIAL_CONTAINER_HPP
#define B3_FACTORIAL_CONTAINER_HPP

#include "factorial-iterator.hpp"

class FactorialContainer
{
public:
  static const int MAX_ITERATOR_POSITION = 11;
  static const int MIN_ITERATOR_POSITION = 1;
  FactorialContainer() = default;
  FactorialIterator begin();
  FactorialIterator end();
};

#endif
