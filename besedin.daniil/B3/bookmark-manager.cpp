#include <algorithm>
#include <iostream>

#include "bookmark-manager.hpp"

BookmarkManager::BookmarkManager()
{
  bookmarks_["current"] = records_.begin();
}

void BookmarkManager::add(const Phonebook::record_t& rec)
{
  records_.pushBack(rec);
  if (std::next(records_.begin()) == records_.end())
  {
    bookmarks_["current"] = records_.begin();
  }
}

void BookmarkManager::store(const std::string& bookmark, const std::string& newBookmark)
{
  auto iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    bookmarks_.emplace(newBookmark, iter->second);
  }
}

void BookmarkManager::insert(BookmarkManager::InsertLocation loc, const std::string& bookmark,
                             const Phonebook::record_t& rec)
{
  auto iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    if (iter->second == records_.end())
    {
      add(rec);
    }

    if (loc == InsertLocation::after)
    {
      records_.insert(std::next(iter->second), rec);
    }
    else
    {
      records_.insert(iter->second, rec);
    }

  }

}

void BookmarkManager::erase(const std::string& bookmark)
{

  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {
    auto eraseIter = iter->second;

    auto find = [&](auto& it)
    {
      if (it.second == eraseIter)
      {

        if (std::next(it.second) == records_.end())
        {
          it.second = records_.prev(eraseIter);
        }
        else
        {
          it.second = records_.next(eraseIter);
        }

      }
    };

    std::for_each(bookmarks_.begin(), bookmarks_.end(), find);

    records_.erase(eraseIter);

  }

}

void BookmarkManager::show(const std::string& bookmark)
{

  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {

    if (records_.empty())
    {
      std::cout << "<EMPTY>" << std::endl;
      return;
    }

    records_.view(iter->second);

  }

}

void BookmarkManager::move(const std::string& bookmark, int n)
{

  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {
    iter->second = records_.move(iter->second, n);
  }

}

void BookmarkManager::move(const std::string& bookmark, MoveLocation loc)
{

  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {

    if (loc == MoveLocation::first)
    {
      iter->second = records_.begin();
    }

    if (loc == MoveLocation::last)
    {
      iter->second = records_.prev(records_.end());
    }

  }

}

BookmarkManager::bookmarks::iterator BookmarkManager::getBookmarkIterator(const std::string& bookmark)
{

  auto iter = bookmarks_.find(bookmark);

  if (iter != bookmarks_.end())
  {
    return iter;
  }
  else
  {
    std::cout << "<INVALID BOOKMARK>" << std::endl;
    return bookmarks_.end();
  }

}
