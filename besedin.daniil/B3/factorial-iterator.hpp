#ifndef B3_FACTORIAL_ITERATOR_HPP
#define B3_FACTORIAL_ITERATOR_HPP

#include <iterator>

class FactorialIterator: public std::iterator<std::bidirectional_iterator_tag, size_t>
{
public:
  FactorialIterator();
  FactorialIterator(size_t);

  FactorialIterator& operator ++();
  FactorialIterator operator ++(int);
  FactorialIterator& operator --();
  FactorialIterator operator --(int);

  const size_t* operator ->();
  const size_t& operator *();
  bool operator ==(const FactorialIterator&);
  bool operator !=(const FactorialIterator&);

private:
  size_t position_, value_;
  size_t getValue(size_t number);
};

#endif
