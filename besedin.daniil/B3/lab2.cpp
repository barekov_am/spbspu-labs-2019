#include <iostream>
#include <algorithm>

#include "factorial-container.hpp"

void lab2()
{
  FactorialContainer container;
  std::copy(container.begin(), container.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << "\n";
  std::reverse_copy(container.begin(), container.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << "\n";
}
