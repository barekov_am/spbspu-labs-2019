#ifndef B3_COMMANDS_HPP
#define B3_COMMANDS_HPP

#include <string>

#include "bookmark-manager.hpp"

void executeAdd(BookmarkManager&, std::stringstream&);
void executeStore(BookmarkManager&, std::stringstream&);
void executeInsert(BookmarkManager&, std::stringstream&);
void executeDelete(BookmarkManager&, std::stringstream&);
void executeShow(BookmarkManager&, std::stringstream&);
void executeMove(BookmarkManager&, std::stringstream&);

#endif
