#include "factorial-iterator.hpp"
#include "factorial-container.hpp"

FactorialIterator::FactorialIterator() :
  FactorialIterator(1)
{
}

FactorialIterator::FactorialIterator(size_t position) :
  position_(position),
  value_(getValue(position))
{
}

const size_t* FactorialIterator::operator ->()
{
  return &value_;
};

const size_t& FactorialIterator::operator *()
{
  return value_;
}

FactorialIterator& FactorialIterator::operator ++()
{
  if (position_ < FactorialContainer::MAX_ITERATOR_POSITION)
  {
    ++position_;
    value_ *= position_;
  }

  return *this;
}

FactorialIterator FactorialIterator::operator ++(int)
{
  FactorialIterator it = *this;
  ++(it);
  return it;
}

FactorialIterator& FactorialIterator::operator --()
{
  if (position_ > FactorialContainer::MIN_ITERATOR_POSITION)
  {
    value_ /= position_;
    --position_;
  }

  return *this;
}

FactorialIterator FactorialIterator::operator --(int)
{
  FactorialIterator it = *this;
  --(it);
  return it;
}

bool FactorialIterator::operator ==(const FactorialIterator& it)
{
  return (position_ == it.position_);
}

bool FactorialIterator::operator !=(const FactorialIterator& it)
{
  return !(*this == it);
}

size_t FactorialIterator::getValue(size_t number)
{
  size_t result = 1;
  for (size_t i = 1; i <= number; i++)
  {
    result *= i;
  }
  return result;
}
