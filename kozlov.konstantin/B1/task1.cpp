#include <forward_list>
#include <vector>
#include <exception>

#include "tools.hpp"

void task1(const char* instruction)
{
  std::vector<int> vectBracket;
  int i = -1;
  while (std::cin >> i)
  {
    vectBracket.push_back(i);
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::invalid_argument("Wrong input data");
  }

  std::vector<int> vectAt(vectBracket);
  std::forward_list<int> list(vectBracket.begin(), vectBracket.end());
  auto order = getOrder<int>(instruction);
  sort<a_at>(vectAt, order);
  sort<a_bracket>(vectBracket, order);
  sort<a_iter>(list, order);

  print(vectBracket);
  print(vectAt);
  print(list);
}
