#include <random>
#include <exception>
#include <cmath>
#include <vector>
#include <ctime>

#include "tools.hpp"

void fillRandom(double* array, int size);

void task4(const char* instruction, int size)
{
  if (!(size > 0))
  {
    throw std::invalid_argument("Size value error");
  }
  std::vector<double> vect(size);
  fillRandom(vect.data(), size);
  auto order = getOrder<double>(instruction);
  print(vect);

  sort<a_at>(vect, order);
  print(vect);
}

void fillRandom(double* array, int size)
{
  std::uniform_real_distribution<double> rand(-1.0, 1.0);
  std::mt19937 range(time(0));
  for (int i = 0; i < size; ++i)
  {
    array[i] = rand(range);
  }
}
