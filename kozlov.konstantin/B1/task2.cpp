#include <iostream>
#include <vector>
#include <memory>
#include <fstream>

#include "tools.hpp"

const std::size_t Size = 1024;

void task2(const char* inputData)
{
  std::size_t index = 0;
  std::size_t size = Size;

  std::unique_ptr<char[], decltype(&free)> arr(static_cast<char*>(malloc(size)), &free);

  if (!arr)
  {
    throw std::runtime_error("Memory error");
  }

  std::ifstream inpStream(inputData);
  if (!inpStream)
  {
    throw std::invalid_argument("Unable to open the file");
  }

  while (inpStream)
  {
    inpStream.read(&arr[index], Size);
    index += inpStream.gcount();
    if (inpStream.gcount() == Size)
    {
      size += Size;
      std::unique_ptr<char[], decltype(&free)> new_array(static_cast<char*>(realloc(arr.get(), size)), &free);
      if (!new_array)
      {
        throw std::runtime_error("Can't allocate memory for new chars");
      }
      arr.release();
      std::swap(arr, new_array);
    }
    if (inpStream.fail() && !inpStream.eof())
    {
      throw std::runtime_error("Reading error");
    }
  }

  std::vector<char> vector(&arr[0], &arr[index]);
  for (const auto& ch : vector)
    {
      std::cout << ch;
    }


}
