#ifndef ACESS_HPP
#define ACESS_HPP

#include <iterator>
#include <cstddef>

template<typename C>
struct a_iter
{

  static typename C::iterator next(typename C::iterator& iter)
  {
    return std::next(iter);
  }

  static typename C::iterator begin(C& arr)
  {
    return arr.begin();
  }

  static typename C::iterator end(C& arr)
  {
    return arr.end();
  }

  static typename C::reference getElement(C&, typename C::iterator iter)
  {
    return *iter;
  }
};

template<typename C>
struct a_at
{
  typedef std::size_t typeIndex;

  static typeIndex next(const typeIndex index)
  {
    return index + 1;
  }

  static typeIndex begin(const C&)
  {
    return 0;
  }

  static typeIndex end(const C& arr)
  {
    return arr.size();
  }

  static typename C::reference getElement(C& arr, typeIndex index)
  {
    return arr.at(index);
  }
};

template<typename C>
struct a_bracket
{
  typedef std::size_t typeIndex;

  static typeIndex next(const std::size_t index)
  {
    return index + 1;
  }

  static typeIndex begin(const C&)
  {
    return 0;
  }

  static typeIndex end(const C& arr)
  {
    return arr.size();
  }

  static typename C::reference getElement(C& arr, typeIndex index)
  {
    return arr[index];
  }
};

#endif
