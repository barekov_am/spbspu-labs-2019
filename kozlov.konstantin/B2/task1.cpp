#include <vector>
#include <sstream>
#include <cstring>
#include <iostream>

#include "implementQueue.hpp"

void task1()
{
  std::string line;
  QueueWithPriority<std::string> queue;

  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Invalid reading");
    }

    std::string command;
    std::stringstream input(line);

    input >> command;
    if (command == "add")
    {
      std::string data;
      std::string priority;
      input >> priority;
      std::getline(input >> std::ws, data);

      if (data.empty())
      {
        std::cout << "<INVALID COMMAND>\n";
        continue;
      }
      if (priority == "high")
      {
        queue.add(QueueWithPriority<std::string>::ElementPriority::high, data);
      }
      else if (priority == "normal")
      {
        queue.add(QueueWithPriority<std::string>::ElementPriority::normal, data);
      }
      else if (priority == "low")
      {
        queue.add(QueueWithPriority<std::string>::ElementPriority::low, data);
      }
      else
      {
        std::cout << "<INVALID COMMAND>\n";
      }
    }

    else if (command == "accelerate")
    {
      queue.Accelerate();
    }

    else if (command == "get")
    {
      try
      {
        std::cout << queue.Get() << '\n';
      }
      catch (std::invalid_argument)
      {
        std::cout << "<EMPTY>\n";
      }
    }

    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
