#include <iostream>
#include <deque>

void task2()
{
  std::deque<int> deque;
  const int maxValue = 20;
  const int minValue = 1;
  int k  = -1;

  while (std::cin >> k)
  {
    if ((k > maxValue) || (k < minValue))
    {
      throw std::invalid_argument("Incorrect input elem");
    }
    deque.push_back(k);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Incorrect input data");
  }

  if  (maxValue < deque.size())
  {
    throw std::invalid_argument("Omitted due to size.");
  }

  if (deque.empty())
  {
    return;
  }

  std::deque<int>::iterator end = deque.end();
  std::deque<int>::iterator begin = deque.begin();

  while (end != begin)
  {
    std::cout << *begin << " ";
    begin++;
    if (begin == end)
    {
      break;
    }
    --end;
    std::cout << *end;
    std::cout << " ";
  }
  deque.clear();
  std::cout << std::endl;
}
