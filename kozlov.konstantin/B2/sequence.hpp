#ifndef SEQUENCE_HPP
#define SEQUENCE_HPP

#include <list>
#include <stdexcept>
#include <iostream>

template<typename T>
class QueueWithPriority
{
public:
  enum class ElementPriority
  {
    low,
    normal,
    high
  };

  bool empty();
  void Accelerate();
  T Get();
  void add(ElementPriority priority, const T& element);

private:
  struct ElementWithPriority
  {
    ElementPriority priority;
    T element;
  };

  std::list<ElementWithPriority> queue_;
};

#endif
