#include <iostream>

void task1();
void task2();

int main(int argc, char * argv[])
{
  try
  {
    if (argc != 2)
    {
      std::invalid_argument( "Wrong number of arguments");
      return 1;
    }
    const int taskNumber = atoi(argv[1]);
    switch (taskNumber)
    {
      case 1:
      {
        task1();
        break;
      }
      case 2:
      {
        task2();
        break;
      }
      default:
        std::cerr << "Wrong number of task\n";
        return 1;
    }
  }
  catch (const std::exception & e)
  {
    std::cerr << "error: " << e.what() << "\n";
    return 1;
  }
  return 0;
}
