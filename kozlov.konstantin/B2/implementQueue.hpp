#ifndef IMPLEMENTQUEUE_HPP
#define IMPLEMENTQUEUE_HPP

#include <iostream>
#include <exception>

#include "sequence.hpp"

template<typename T>
void QueueWithPriority<T>::add(ElementPriority priority, const T& element)
{
   ElementWithPriority ElementWithPriority_;
   ElementWithPriority_.priority = priority;
   ElementWithPriority_.element = element;

  if (queue_.empty())
  {
    queue_.push_back(ElementWithPriority_);
  }
  else if (priority == QueueWithPriority<T>::ElementPriority::low)
  {
    queue_.push_back(ElementWithPriority_);
  }
  else
  {
    decltype(queue_.begin()) i = queue_.begin();
    while(i != queue_.end())
    {
      if (priority <= (*i).priority)
      {
        i++;
      }
      else
      {
        queue_.insert(i, ElementWithPriority_);
        break;
      }
    }
  }
}

template<typename T>
T QueueWithPriority<T>::Get()
{
  if (queue_.empty())
  {
    throw std::invalid_argument("Cant get element from the empty queue ");
  }

  T elem = queue_.front().element;
  queue_.pop_front();
  return elem;
}

template<typename T>
void QueueWithPriority<T>::Accelerate()
{
  auto i = --queue_.end();

  while((*i).priority == ElementPriority::low)
  {
    ((*i).priority = ElementPriority::high);
    i--;
  }
  i++;

  auto j = queue_.begin();
  while((*j).priority == ElementPriority::high)
  {
    j++;
  }

  queue_.splice(j, queue_, i, queue_.end());
}

#endif
