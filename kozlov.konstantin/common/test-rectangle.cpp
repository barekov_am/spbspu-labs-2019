
#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(test_rectangle)

BOOST_AUTO_TEST_CASE(test_rectangle_moving)
{
  kozlov::Rectangle test_rectangle({5, 7, {10, 10}});
  const kozlov::rectangle_t frame_before_moving = test_rectangle.getFrameRect();
  const double area_before_moving = test_rectangle.getArea();

  test_rectangle.move({10, 10});

  kozlov::rectangle_t frame_after_moving = test_rectangle.getFrameRect();
  double area_after_moving = test_rectangle.getArea();

  BOOST_CHECK_CLOSE(frame_before_moving.height, frame_after_moving.height, ACCURACY);
  BOOST_CHECK_CLOSE(frame_before_moving.width, frame_after_moving.width, ACCURACY);
  BOOST_CHECK_CLOSE(area_before_moving, area_after_moving, ACCURACY);

  test_rectangle.move(10, 10);

  frame_after_moving = test_rectangle.getFrameRect();
  area_after_moving = test_rectangle.getArea();

  BOOST_CHECK_CLOSE(frame_before_moving.height, frame_after_moving.height, ACCURACY);
  BOOST_CHECK_CLOSE(frame_before_moving.width, frame_after_moving.width, ACCURACY);
  BOOST_CHECK_CLOSE(area_before_moving, area_after_moving, ACCURACY);
}

BOOST_AUTO_TEST_CASE(test_rectangle_scaling)
{
  kozlov::Rectangle test_rectangle({5, 7, {10, 10}});

  const double area_before_scaling = test_rectangle.getArea();
  const double factor = 12;

  test_rectangle.scale(factor);

  const double area_after_scaling = test_rectangle.getArea();
  BOOST_CHECK_CLOSE(area_before_scaling * pow(factor, 2), area_after_scaling, ACCURACY);
}

BOOST_AUTO_TEST_CASE(test_rectangle_after_rotation)
{
  kozlov::Rectangle test_rectangle({10.5, 6.9, {8.0, 9.0}});
  const double rectangle_area_before_rotation = test_rectangle.getArea();
  const kozlov::rectangle_t frame_rectangle_before_rotation = test_rectangle.getFrameRect();
  test_rectangle.rotate(90);

  BOOST_CHECK_CLOSE(rectangle_area_before_rotation, test_rectangle.getArea(), ACCURACY);
  kozlov::rectangle_t frame_rect_after_rotation = test_rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frame_rectangle_before_rotation.height, frame_rect_after_rotation.width, ACCURACY);
  BOOST_CHECK_CLOSE(frame_rectangle_before_rotation.width, frame_rect_after_rotation.height, ACCURACY);
  BOOST_CHECK_CLOSE(frame_rectangle_before_rotation.pos.x, frame_rect_after_rotation.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frame_rectangle_before_rotation.pos.y, frame_rect_after_rotation.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(test_rectangle_parameters)
{
  BOOST_CHECK_THROW(kozlov::Rectangle({-5, 7, {10, 10}}), std::invalid_argument);
  BOOST_CHECK_THROW(kozlov::Rectangle({5, -7, {10, 10}}), std::invalid_argument);
  kozlov::Rectangle test_rectangle({5, 7, {10, 10}});
  BOOST_CHECK_THROW(test_rectangle.scale(-9), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
