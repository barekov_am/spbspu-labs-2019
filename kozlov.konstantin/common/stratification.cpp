#include "stratification.hpp"
#include <cmath>

kozlov::Matrix<kozlov::Shape> kozlov::layer(const kozlov::CompositeShape & compShape)
{
  kozlov::Matrix<kozlov::Shape> matrix;
  for (std::size_t i = 0; i < compShape.getCount(); ++i)
  {
    std::size_t row = 0;
    std::size_t j = matrix.getRows();
    while (j-- > 0)
    {
      for (std::size_t k = 0; k < matrix[j].getCount(); ++k)
      {
        if (cross(* compShape[i], *matrix[j][k]))
        {
          row = j + 1;
          break;
        }
      }
      if (row)
      {
        break;
      }
    }
    matrix.add(row, compShape[i]);
  }
  return matrix;
}

bool kozlov::cross(const kozlov::Shape & Shape1, const kozlov::Shape & Shape2)
{
  const kozlov::rectangle_t FrameRect1 = Shape1.getFrameRect();
  const kozlov::rectangle_t FrameRect2 = Shape2.getFrameRect();
  if (std::abs(FrameRect1.pos.x - FrameRect2.pos.x) > (FrameRect1.width + FrameRect2.width) / 2)
  {
    return false;
  }
  if (std::abs(FrameRect1.pos.y - FrameRect2.pos.y) > (FrameRect1.height + FrameRect2.height) / 2)
  {
    return false;
  }
  return true;
}
