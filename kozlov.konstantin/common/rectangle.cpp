#include "rectangle.hpp"
#include <iostream>
#include <cassert>
#include <stdexcept>

kozlov::Rectangle::Rectangle(const rectangle_t & rect):
  rect_(rect),
  angle_(0)
{
  if (rect_.height <= 0)
  {
    throw std::invalid_argument("Height must be > 0");
  }
  if (rect_.width <= 0)
  {
    throw std::invalid_argument("Width must be > 0");
  }
}

double kozlov::Rectangle::getArea() const
{
  return (rect_.height * rect_.width);
}

kozlov::rectangle_t kozlov::Rectangle::getFrameRect() const
{
  const double cos = std::abs(std::cos(angle_ * M_PI / 180));
  const double sin = std::abs(std::sin(angle_ * M_PI / 180));
  const double width = std::abs(cos * rect_.width) + std::abs(sin * rect_.height);
  const double height = std::abs(cos * rect_.height) + std::abs(sin * rect_.width);
  return {width, height, rect_.pos};
}

void kozlov::Rectangle::move(double dx, double dy)
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

void kozlov::Rectangle::move(const point_t & pos)
{
  rect_.pos = pos;
}

void kozlov::Rectangle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Factor must be > 0");
  }
  rect_.height *= factor;
  rect_.width *= factor;
}
void kozlov::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
double kozlov::Rectangle::sine(double angle) const
{
  return std::abs(std::sin(angle * M_PI / 180));
}
double kozlov::Rectangle::cosine(double angle) const
{
  return std::abs(std::cos(angle * M_PI / 180));
}

void kozlov::Rectangle::printData() const
{
  std::cout << "Rectangle's width: " << rect_.width << std::endl;
  std::cout << "Rectangle's height: " << rect_.height << std::endl;
  std::cout << "Rectangle's area: " << getArea() << std::endl;
  std::cout << "Height of frame: " << getFrameRect().height << std::endl;
  std::cout << "Width of frame: " << getFrameRect().width << std::endl;
  std::cout << "Center of frame: (" << getFrameRect().pos.x
      << ';' << getFrameRect().pos.y << ')' << std::endl;
  std::cout << std::endl;
}
