#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "stratification.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testStratification)

BOOST_AUTO_TEST_CASE(testStratificationCorrectWorking)
{
  kozlov::Rectangle rect({5, 6, {0, 0}});
  kozlov::Rectangle rect1({1, 4, {0, -4}});
  kozlov::Circle circl(3, {20, 20});
  kozlov::CompositeShape compShape(std::make_shared<kozlov::Rectangle>(rect));
  compShape.add(std::make_shared<kozlov::Rectangle>(rect1));
  compShape.add(std::make_shared<kozlov::Circle>(circl));

  kozlov::Matrix<kozlov::Shape> matrix = kozlov::layer(compShape);
  BOOST_CHECK_EQUAL(matrix.getRows(), 2);
  BOOST_CHECK_EQUAL(matrix[0].getCount(), 2);
  BOOST_CHECK_EQUAL(matrix[1].getCount(), 1);
  BOOST_CHECK_EQUAL(matrix[0][0], compShape[0]);
  BOOST_CHECK_EQUAL(matrix[0][1], compShape[2]);
  BOOST_CHECK_EQUAL(matrix[1][0], compShape[1]);
}

BOOST_AUTO_TEST_CASE(testCrossing)
{
  kozlov::Rectangle rect1({5, 6, {0, 0}});
  kozlov::Rectangle rect2({1, 4, {0, -4}});
  kozlov::Circle circle1(1, {5, 0});
  kozlov::Rectangle rect3({1, 4, {0, 4}});
  kozlov::Circle circle2(2, {-3, 0});

  BOOST_CHECK(kozlov::cross(rect1, rect2));
  BOOST_CHECK(kozlov::cross(rect1, circle2));
  BOOST_CHECK(kozlov::cross(rect1, rect3));
  BOOST_CHECK(kozlov::cross(rect1, circle2));

  BOOST_CHECK(!kozlov::cross(rect1, circle1));
}

BOOST_AUTO_TEST_CASE(testEmptyStratification)
{
  kozlov::CompositeShape compShape;
  kozlov::Matrix<kozlov::Shape> matrix = kozlov::layer(compShape);

  BOOST_CHECK_EQUAL(matrix.getRows(), 0);
}

BOOST_AUTO_TEST_SUITE_END()

