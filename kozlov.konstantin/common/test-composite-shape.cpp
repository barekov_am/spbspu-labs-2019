#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(test_composite_shape)

BOOST_AUTO_TEST_CASE(test_composite_shape_add)
{
  kozlov::Circle test_circle(10, {4, 5});
  kozlov::Rectangle test_rectangle({5, 7, {10, 10}});
  kozlov::CompositeShape test_composite_shape(std::make_shared<kozlov::Rectangle>(test_rectangle));
  const double composite_shape_area_before_add_shape = test_composite_shape.getArea();
  test_composite_shape.add(std::make_shared<kozlov::Circle>(test_circle));

  BOOST_CHECK_CLOSE(test_composite_shape.getArea(), test_circle.getArea() + composite_shape_area_before_add_shape, ACCURACY);
}

BOOST_AUTO_TEST_CASE(test_composite_shape_delete)
{
  kozlov::Circle test_circle(5, {14, 15});
  kozlov::Rectangle test_rectangle({8, 9, {22, 30}});
  kozlov::CompositeShape test_composite_shape(std::make_shared<kozlov::Rectangle>(test_rectangle));
  test_composite_shape.add(std::make_shared<kozlov::Circle>(test_circle));
  const double composite_shape_area_before_delete_shape = test_composite_shape.getArea();
  test_composite_shape.remove(1);

  BOOST_CHECK_CLOSE(test_composite_shape.getArea(), composite_shape_area_before_delete_shape - test_circle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(test_composite_shape_delete_overcount)
{
  kozlov::Circle test_circle(5, {14, 15});
  kozlov::Rectangle test_rectangle({8, 9, {22, 30}});
  kozlov::CompositeShape test_composite_shape(std::make_shared<kozlov::Rectangle>(test_rectangle));
  test_composite_shape.add(std::make_shared<kozlov::Circle>(test_circle));

  BOOST_CHECK_THROW(test_composite_shape.remove(-1), std::out_of_range);
  BOOST_CHECK_THROW(test_composite_shape.remove(100), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(test_composite_shape_index)
{
  kozlov::Circle test_circle(5, {14, 15});
  kozlov::Rectangle test_rectangle({8, 9, {22, 30}});
  kozlov::CompositeShape test_composite_shape(std::make_shared<kozlov::Rectangle>(test_rectangle));
  test_composite_shape.add(std::make_shared<kozlov::Circle>(test_circle));

  BOOST_CHECK_THROW(test_composite_shape[-1], std::out_of_range);
  BOOST_CHECK_THROW(test_composite_shape[100], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(test_composite_shape_moving)

BOOST_AUTO_TEST_CASE(test_composite_shape_moving_to_point)
{
  kozlov::Circle test_circle(5, {14, 15});
  kozlov::Rectangle test_rectangle({8, 9, {22, 30}});
  kozlov::CompositeShape test_composite_shape(std::make_shared<kozlov::Rectangle>(test_rectangle));
  test_composite_shape.add(std::make_shared<kozlov::Circle>(test_circle));
  const double composite_shape_area_before_moving = test_composite_shape.getArea();
  const kozlov::rectangle_t frame_rect_before_moving = test_composite_shape.getFrameRect();
  test_composite_shape.move({2.0, 10.4});

  BOOST_CHECK_CLOSE(test_composite_shape.getArea(), composite_shape_area_before_moving, ACCURACY);
  kozlov::rectangle_t frame_rect_after_moving = test_composite_shape.getFrameRect();
  BOOST_CHECK_CLOSE(frame_rect_before_moving.height, frame_rect_after_moving.height, ACCURACY);
  BOOST_CHECK_CLOSE(frame_rect_before_moving.width, frame_rect_after_moving.width, ACCURACY);
}

BOOST_AUTO_TEST_CASE(test_composite_shape_moving_along_x_and_y)
{
  kozlov::Circle test_circle(5, {14, 15});
  kozlov::Rectangle test_rectangle({8, 9, {22, 30}});
  kozlov::CompositeShape test_composite_shape(std::make_shared<kozlov::Rectangle>(test_rectangle));
  test_composite_shape.add(std::make_shared<kozlov::Circle>(test_circle));
  const double composite_shape_area_before_moving = test_composite_shape.getArea();
  const kozlov::rectangle_t frame_rect_before_moving = test_composite_shape.getFrameRect();
  test_composite_shape.move(2.0, 10.4);

  BOOST_CHECK_CLOSE(test_composite_shape.getArea(), composite_shape_area_before_moving, ACCURACY);
  kozlov::rectangle_t frame_rect_after_moving = test_composite_shape.getFrameRect();
  BOOST_CHECK_CLOSE(frame_rect_before_moving.height, frame_rect_after_moving.height, ACCURACY);
  BOOST_CHECK_CLOSE(frame_rect_before_moving.width, frame_rect_after_moving.width, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(test_composite_shape_scaling)

BOOST_AUTO_TEST_CASE(test_composite_shape_scaling_increase)
{
  kozlov::Circle test_circle(5, {14, 15});
  kozlov::Rectangle test_rectangle({8, 9, {22, 30}});
  kozlov::CompositeShape test_composite_shape(std::make_shared<kozlov::Rectangle>(test_rectangle));
  test_composite_shape.add(std::make_shared<kozlov::Circle>(test_circle));
  const double composite_shape_area_before_scaling = test_composite_shape.getArea();
  const kozlov::rectangle_t frame_rect_before_scaling = test_composite_shape.getFrameRect();
  const double factor = 5.0;
  test_composite_shape.scale(factor);

  BOOST_CHECK_CLOSE(test_composite_shape.getArea(), composite_shape_area_before_scaling * pow(factor, 2), ACCURACY);
  kozlov::rectangle_t frame_rect_after_scaling = test_composite_shape.getFrameRect();
  BOOST_CHECK_CLOSE(frame_rect_before_scaling.height * factor, frame_rect_after_scaling.height, ACCURACY);
  BOOST_CHECK_CLOSE(frame_rect_before_scaling.width * factor, frame_rect_after_scaling.width, ACCURACY);
}

BOOST_AUTO_TEST_CASE(test_composite_shape_scaling_decrease)
{
  kozlov::Circle test_circle(5, {14, 15});
  kozlov::Rectangle test_rectangle({8, 9, {22, 30}});
  kozlov::CompositeShape test_composite_shape(std::make_shared<kozlov::Rectangle>(test_rectangle));
  test_composite_shape.add(std::make_shared<kozlov::Circle>(test_circle));
  const double composite_shape_area_before_scaling = test_composite_shape.getArea();
  const kozlov::rectangle_t frame_rect_before_scaling = test_composite_shape.getFrameRect();
  const double factor = 0.5;
  test_composite_shape.scale(factor);

  BOOST_CHECK_CLOSE(test_composite_shape.getArea(), composite_shape_area_before_scaling * pow(factor, 2), ACCURACY);
  kozlov::rectangle_t frame_rect_after_scaling = test_composite_shape.getFrameRect();
  BOOST_CHECK_CLOSE(frame_rect_before_scaling.height * factor, frame_rect_after_scaling.height, ACCURACY);
  BOOST_CHECK_CLOSE(frame_rect_before_scaling.width * factor, frame_rect_after_scaling.width, ACCURACY);
}

BOOST_AUTO_TEST_CASE(test_composite_shape_scaling_factor)
{
  kozlov::Circle test_circle(5, {14, 15});
  kozlov::Rectangle test_rectangle({8, 9, {22, 30}});
  kozlov::CompositeShape test_composite_shape(std::make_shared<kozlov::Rectangle>(test_rectangle));
  test_composite_shape.add(std::make_shared<kozlov::Circle>(test_circle));

  BOOST_CHECK_THROW(test_composite_shape.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(test_composite_shape_empty)

BOOST_AUTO_TEST_CASE(test_composite_shape_empty_move)
{
  kozlov::CompositeShape test_composite_shape;

  BOOST_CHECK_THROW(test_composite_shape.move(2.0, 2.0), std::logic_error);
  BOOST_CHECK_THROW(test_composite_shape.move({10.0, 10.0}), std::logic_error);
}

BOOST_AUTO_TEST_CASE(test_composite_shape_empty_parameters)
{
  kozlov::CompositeShape test_composite_shape;

  BOOST_CHECK_THROW(test_composite_shape.getArea(), std::logic_error);
  BOOST_CHECK_THROW(test_composite_shape.getFrameRect(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(test_composite_shape_copy_constructor)
{
  kozlov::Circle test_circle(5, {14, 15});
  kozlov::Rectangle test_rectangle({8, 9, {22, 30}});
  kozlov::CompositeShape test_composite_shape(std::make_shared<kozlov::Rectangle>(test_rectangle));
  test_composite_shape.add(std::make_shared<kozlov::Circle>(test_circle));
  const kozlov::rectangle_t frame_rectangle = test_composite_shape.getFrameRect();
  kozlov::CompositeShape copy_composite_shape(test_composite_shape);

  BOOST_CHECK_CLOSE(copy_composite_shape.getArea(), test_composite_shape.getArea(), ACCURACY);
  BOOST_CHECK_EQUAL(copy_composite_shape.getCount(), test_composite_shape.getCount());
  kozlov::rectangle_t copy_frame_rectangle = copy_composite_shape.getFrameRect();
  BOOST_CHECK_CLOSE(copy_frame_rectangle.width, frame_rectangle.width, ACCURACY);
  BOOST_CHECK_CLOSE(copy_frame_rectangle.height, frame_rectangle.height, ACCURACY);
  BOOST_CHECK_CLOSE(copy_frame_rectangle.pos.x, frame_rectangle.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(copy_frame_rectangle.pos.y, frame_rectangle.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(test_composite_shape_move_constructor)
{
  kozlov::Circle test_circle(5, {14, 15});
  kozlov::Rectangle test_rectangle({8, 9, {22, 30}});
  kozlov::CompositeShape test_composite_shape(std::make_shared<kozlov::Rectangle>(test_rectangle));
  test_composite_shape.add(std::make_shared<kozlov::Circle>(test_circle));
  const kozlov::rectangle_t frame_rectangle = test_composite_shape.getFrameRect();
  const double composite_shape_area = test_composite_shape.getArea();
  const std::size_t count_of_shapes = test_composite_shape.getCount();
  kozlov::CompositeShape move_composite_shape(std::move(test_composite_shape));

  BOOST_CHECK_CLOSE(move_composite_shape.getArea(), composite_shape_area, ACCURACY);
  kozlov::rectangle_t move_frame_rectangle = move_composite_shape.getFrameRect();
  BOOST_CHECK_CLOSE(move_frame_rectangle.width, frame_rectangle.width, ACCURACY);
  BOOST_CHECK_CLOSE(move_frame_rectangle.height, frame_rectangle.height, ACCURACY);
  BOOST_CHECK_CLOSE(move_frame_rectangle.pos.x, frame_rectangle.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(move_frame_rectangle.pos.y, frame_rectangle.pos.y, ACCURACY);
  BOOST_CHECK_EQUAL(move_composite_shape.getCount(), count_of_shapes);

  BOOST_CHECK_EQUAL(0, test_composite_shape.getCount());
}

BOOST_AUTO_TEST_CASE(test_composite_shape_copy_operator)
{
  kozlov::Circle test_circle(5, {14, 15});
  kozlov::Rectangle test_rectangle({8, 9, {22, 30}});
  kozlov::CompositeShape test_composite_shape(std::make_shared<kozlov::Rectangle>(test_rectangle));
  test_composite_shape.add(std::make_shared<kozlov::Circle>(test_circle));
  const kozlov::rectangle_t frame_rectangle = test_composite_shape.getFrameRect();
  kozlov::CompositeShape new_composite_shape = test_composite_shape;

  BOOST_CHECK_CLOSE(new_composite_shape.getArea(), test_composite_shape.getArea(), ACCURACY);
  BOOST_CHECK_EQUAL(new_composite_shape.getCount(), test_composite_shape.getCount());
  kozlov::rectangle_t copy_frame_rectangle = new_composite_shape.getFrameRect();
  BOOST_CHECK_CLOSE(copy_frame_rectangle.width, frame_rectangle.width,ACCURACY);
  BOOST_CHECK_CLOSE(copy_frame_rectangle.height, frame_rectangle.height, ACCURACY);
  BOOST_CHECK_CLOSE(copy_frame_rectangle.pos.x, frame_rectangle.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(copy_frame_rectangle.pos.y, frame_rectangle.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(test_composite_shape_move_operator)
{
  kozlov::Circle test_circle(5, {14, 15});
  kozlov::Rectangle test_rectangle({8, 9, {22, 30}});
  kozlov::CompositeShape test_composite_shape(std::make_shared<kozlov::Rectangle>(test_rectangle));
  test_composite_shape.add(std::make_shared<kozlov::Circle>(test_circle));
  const kozlov::rectangle_t frameRectangle = test_composite_shape.getFrameRect();
  const double composite_shape_area = test_composite_shape.getArea();
  const std::size_t count_of_shapes = test_composite_shape.getCount();
  kozlov::CompositeShape move_composite_shape = std::move(test_composite_shape);

  BOOST_CHECK_CLOSE(move_composite_shape.getArea(), composite_shape_area, ACCURACY);
  kozlov::rectangle_t move_frame_rectangle = move_composite_shape.getFrameRect();
  BOOST_CHECK_CLOSE(move_frame_rectangle.width, frameRectangle.width, ACCURACY);
  BOOST_CHECK_CLOSE(move_frame_rectangle.height, frameRectangle.height, ACCURACY);
  BOOST_CHECK_CLOSE(move_frame_rectangle.pos.x, frameRectangle.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(move_frame_rectangle.pos.y, frameRectangle.pos.y, ACCURACY);
  BOOST_CHECK_EQUAL(move_composite_shape.getCount(), count_of_shapes);

  BOOST_CHECK_EQUAL(0, test_composite_shape.getCount());
}


BOOST_AUTO_TEST_SUITE_END()
