#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"
#include <memory>
#include <cmath>

namespace kozlov
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual void printData() const = 0;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t & pos) = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void scale(double factor) = 0;
    virtual void rotate(double) = 0;
  };
  using shape_ptr = std::shared_ptr<Shape>;
  using array_ptr = std::unique_ptr<shape_ptr[]>;
}

#endif
