#include <stdexcept>
#include <cmath>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(test_circle)


BOOST_AUTO_TEST_CASE(test_circle_moving)
{
  kozlov::Circle test_circle(5, {2, 6});
  const kozlov::rectangle_t frame_before_moving = test_circle.getFrameRect();
  const double area_before_moving = test_circle.getArea();

  test_circle.move({10, 10});

  kozlov::rectangle_t frame_after_moving = test_circle.getFrameRect();
  double area_after_moving = test_circle.getArea();

  BOOST_CHECK_CLOSE(frame_before_moving.height, frame_after_moving.height, ACCURACY);
  BOOST_CHECK_CLOSE(frame_before_moving.width, frame_after_moving.width, ACCURACY);
  BOOST_CHECK_CLOSE(area_before_moving, area_after_moving, ACCURACY);

  test_circle.move(10, 10);

  frame_after_moving = test_circle.getFrameRect();
  area_after_moving = test_circle.getArea();

  BOOST_CHECK_CLOSE(frame_before_moving.height, frame_after_moving.height, ACCURACY);
  BOOST_CHECK_CLOSE(frame_before_moving.width, frame_after_moving.width, ACCURACY);
  BOOST_CHECK_CLOSE(area_before_moving, area_after_moving, ACCURACY);
}

BOOST_AUTO_TEST_CASE(test_circle_scaling)
{
  kozlov::Circle test_circle(5, {2, 6});
  const double area_before_scaling = test_circle.getArea();
  const double factor = 12;

  test_circle.scale(factor);

  const double area_after_scaling = test_circle.getArea();
  BOOST_CHECK_CLOSE(area_before_scaling * pow(factor, 2), area_after_scaling, ACCURACY);
}

BOOST_AUTO_TEST_CASE(test_circle_after_rotation)
{
  kozlov::Circle test_circle(5, {2, 6});
  const double circle_area_before_rotation = test_circle.getArea();
  const kozlov::rectangle_t frame_rectangle_before_rotation = test_circle.getFrameRect();
  test_circle.rotate(60);

  BOOST_CHECK_CLOSE(circle_area_before_rotation, test_circle.getArea(), ACCURACY);
  kozlov::rectangle_t frame_rect_after_rotation = test_circle.getFrameRect();
  BOOST_CHECK_CLOSE(frame_rectangle_before_rotation.height, frame_rect_after_rotation.height, ACCURACY);
  BOOST_CHECK_CLOSE(frame_rectangle_before_rotation.width, frame_rect_after_rotation.width, ACCURACY);
  BOOST_CHECK_CLOSE(frame_rectangle_before_rotation.pos.x, frame_rect_after_rotation.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frame_rectangle_before_rotation.pos.y, frame_rect_after_rotation.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(test_circle_parameters)
{
  BOOST_CHECK_THROW(kozlov::Circle(-3, {-7, 10}), std::invalid_argument);
  kozlov::Circle test_circle(5, {2, 6});
  BOOST_CHECK_THROW(test_circle.scale(-9), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
