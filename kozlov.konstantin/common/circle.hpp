#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace kozlov
{
  class Circle: public Shape
  {
  public:
    Circle(const double radius, const point_t & center);
    void printData() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & pos) override;
    void move(double dx, double dy) override;
    void scale(double factor) override;
    void rotate(double) override;


  private:
    point_t center_;
    double radius_;
  };
}

#endif
