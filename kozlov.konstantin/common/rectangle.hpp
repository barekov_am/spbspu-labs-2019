#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace kozlov
{
  class Rectangle: public Shape
  {
  public:
    Rectangle(const rectangle_t & rect);
    void printData() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & pos) override;
    void move(double dx, double dy) override;
    void scale(double factor) override;
    void rotate(double) override;
    double sine(double) const;
    double cosine(double) const;


 private:
    rectangle_t rect_;
    double angle_;
  };
}

#endif
