#include "circle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>
#include <stdexcept>

kozlov::Circle::Circle(const double radius, const point_t & center):
  center_(center),
  radius_(radius)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("Radius must be > 0");
  }
}

double kozlov::Circle::getArea() const
{
  return (M_PI * pow(radius_, 2));
}

kozlov::rectangle_t kozlov::Circle::getFrameRect() const
{
  return {2 * radius_ , 2 * radius_ , center_};
}

void kozlov::Circle::move(const point_t & pos)
{
  center_ = pos;
}

void kozlov::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void kozlov::Circle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Factor must be > 0");
  }
  radius_ *= factor;
}

void kozlov::Circle::rotate(double)
{
}

void kozlov::Circle::printData() const
{
  std::cout << "Circle's radius: " << radius_ << std::endl;
  std::cout << "Circle's area: " << getArea() << std::endl;
  std::cout << "Height of frame: " << getFrameRect().height << std::endl;
  std::cout << "Width of frame: " << getFrameRect().width << std::endl;
  std::cout << "Center of frame: (" << getFrameRect().pos.x
      << ';' << getFrameRect().pos.y << ')' << std::endl;
  std::cout << std::endl;
}
