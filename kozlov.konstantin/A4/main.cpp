#include <iostream>
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "matrix.hpp"
#include "stratification.hpp"

int main()
{
  kozlov::Rectangle rect({1, 3, {7, 5}});
  kozlov::Circle circl(20, {11, 8});

  kozlov::CompositeShape comp_shape(std::make_shared<kozlov::Rectangle>(rect));
  comp_shape.add(std::make_shared<kozlov::Circle> (circl));
  std::cout << "-> CompositeShape Info: \n";
  comp_shape.printData();
  std::cout << "\n";
  std::cout << "-> Moving to the point (" << 100 << "; " << 20 << ")\n";
  comp_shape.move({100, 20});
  comp_shape.printData();
  std::cout << "\n";
  std::cout << "-> Moving to x+= " << 10 << " and y+= " << 2 << std::endl;
  comp_shape.move(10, 2);
  comp_shape.printData();
  std::cout << "\n";
  std::cout << "-> Scaling with factor: " << 3 << std::endl;
  comp_shape.scale(3);
  comp_shape.printData();
  std::cout << "\n";
  std::cout << "-> Delete CompositeShape[1]: \n";
  comp_shape.remove(1);
  comp_shape.printData();
  std::cout << "\n";
  std::cout << "-> CompositeShape[0] Info:\n";
  std::shared_ptr<kozlov::Shape> selected_shape = comp_shape[0];
  selected_shape->printData();

  std::cout << "\n";



  comp_shape.add(std::make_shared<kozlov::Circle> (circl));

  std::cout << "Matrix:";
  std::cout << "\n";

  kozlov::Matrix<kozlov::Shape> matrix = kozlov::layer(comp_shape);

  for (std::size_t i = 0; i < matrix.getRows(); ++i)
  {
    std::cout << "Actual layer " << i + 1 << '\n';
    std::cout << "Shapes on this layer:\n";
    for (std::size_t j = 0; j < matrix[i].getCount(); ++j)
    {
      std::cout << j + 1 << '\n';
      std::cout << "Area of the shape " << matrix[i][j]->getArea() << '\n';
      const kozlov::point_t centerShape = matrix[i][j]->getFrameRect().pos;
      std::cout << "Center of the shape (" << centerShape.x << "," << centerShape.y << ")\n";
    }
  }

  std::cout << "\n";

  return 0;
}
