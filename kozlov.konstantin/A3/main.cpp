#include <iostream>
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

int main()
{
  kozlov::Circle circle_1(10, {4, 5});
  kozlov::Rectangle rectangle_1({5, 7, {10, 10}});
  kozlov::Rectangle rectangle_2({2, 4, {8, 6}});


  kozlov::CompositeShape comp_shape(std::make_shared<kozlov::Rectangle> (rectangle_1));
  comp_shape.add(std::make_shared<kozlov::Rectangle> (rectangle_2));
  comp_shape.add(std::make_shared<kozlov::Circle> (circle_1));
  std::cout << "-> CompositeShape Info: \n";
  comp_shape.printData();
  std::cout << "\n";
  std::cout << "-> Moving to the point (" << 100 << "; " << 20 << ")\n";
  comp_shape.move({100, 20});
  comp_shape.printData();
  std::cout << "\n";
  std::cout << "-> Moving to x+= " << 10 << " and y+= " << 2 << std::endl;
  comp_shape.move(10, 2);
  comp_shape.printData();
  std::cout << "\n";
  std::cout << "-> Scaling with factor: " << 3 << std::endl;
  comp_shape.scale(3);
  comp_shape.printData();
  std::cout << "\n";
  std::cout << "-> Delete CompositeShape[2]: \n";
  comp_shape.remove(2);
  comp_shape.printData();
  std::cout << "\n";
  std::cout << "-> CompositeShape[1] Info:\n";
  std::shared_ptr<kozlov::Shape> selected_shape = comp_shape[1];
  selected_shape->printData();

  return 0;
}
