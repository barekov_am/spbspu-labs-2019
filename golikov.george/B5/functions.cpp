#include "functions.hpp"
#include <iostream>
#include <sstream>
#include "shape.hpp"


Shape readPoints(std::string & line, std::size_t vertices)
{
  Shape shape;
  std::size_t posOpenBracket;
  std::size_t posCloseBracket;
  std::size_t posSemicolon;

  for (std::size_t i = 0; i < vertices; i++)
  {
    posOpenBracket = line.find_first_of('(');
    posCloseBracket = line.find_first_of(')');
    posSemicolon = line.find_first_of(';');
    if ((posOpenBracket == std::string::npos) || (posCloseBracket == std::string::npos) ||
        (posSemicolon == std::string::npos))
    {
      throw std::invalid_argument("Incorrect line!");
    }

    Point_t point = {std::stoi(line.substr(posOpenBracket + 1, posSemicolon - posOpenBracket - 1)),
                     std::stoi(line.substr(posSemicolon + 1, posCloseBracket - posSemicolon - 1))};

    line.erase(0, posCloseBracket + 1);
    shape.push_back(point);
  }

  return shape;
}

void readWords(std::vector<Shape> & shapeContainer, std::string & inputLine)
{
  while (std::getline(std::cin, inputLine))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed during reading!\n");
    }

    std::stringstream stream(inputLine);
    std::size_t numOfVertices;
    if (!(stream >> numOfVertices))
    {
      continue;
    }

    if (numOfVertices < 3)
    {
      throw std::invalid_argument("Invalid number of vertices!\n");
    }

    Shape shape = readPoints(inputLine, numOfVertices);

    shapeContainer.push_back(shape);
  }
}

void receiveInfo(ShapeInfo_t & shapeInfo, std::vector<Shape> & shapeContainer)
{
  for (const auto & shape : shapeContainer)
  {
    shapeInfo.countOfVertices += shape.size();
    if (shape.size() == TRIANGLE_VERTICES)
    {
      ++shapeInfo.countOfTriangles;
    }
    else if (shape.size() == RECTANGLE_VERTICES)
    {
      if (isRectangle(shape))
      {
        ++shapeInfo.countOfRectangles;
        if (isSquare(shape))
        {
          ++shapeInfo.countOfSquares;
        }
      }
    }
  }
}

void removePentagons(std::vector<Shape> & shapeContainer)
{
  auto newEnd = remove_if(shapeContainer.begin(), shapeContainer.end(),
                          [](const Shape & shape){return (shape.size() == PENTAGON_VERTICES);});

  shapeContainer.erase(newEnd, shapeContainer.end());
}

void createVectorPoints(std::vector<Shape> & shapeContainer, std::vector<Point_t> points)
{
  for (const auto& i : shapeContainer)
  {
    points.push_back(i.front());
  }
}

bool isRectangle(const Shape & shape)
{
  int squaredDiag1 = (shape[0].x - shape[2].x) * (shape[0].x - shape[2].x) +
                     (shape[0].y - shape[2].y) * (shape[0].y - shape[2].y);

  int squaredDiag2 = (shape[1].x - shape[3].x) * (shape[1].x - shape[3].x) +
                     (shape[1].y - shape[3].y) * (shape[1].y - shape[3].y);

  return squaredDiag1 == squaredDiag2;
}

bool isSquare(const Shape & shape)
{
  if (shape[1].x != shape[0].x)
  {
    if (std::abs(shape[1].x - shape[0].x) != std::abs(shape[2].y - shape[1].y))
    {
      return false;
    }
  }
  else
  {
    if (std::abs(shape[2].x - shape[1].x) != std::abs(shape[1].y - shape[0].y))
    {
      return false;
    }
  }
  return true;
}

bool isLess(const Shape & lhs, const Shape & rhs)
{
  if (lhs.size() < rhs.size())
  {
    return true;
  }
  if ((lhs.size() == RECTANGLE_VERTICES) && (rhs.size() == RECTANGLE_VERTICES))
  {
    if (isSquare(lhs))
    {
      if (isSquare(rhs))
      {
        return lhs[0].x < rhs[0].x;
      }
      return true;
    }
  }
  return false;
}

void printInfo(std::vector<Shape> & shapeContainer, std::vector<Point_t> points, ShapeInfo_t & shapeInfo)
{
  std::cout << "Vertices: " << shapeInfo.countOfVertices << '\n';
  std::cout << "Triangles: " << shapeInfo.countOfTriangles << '\n';
  std::cout << "Squares: " << shapeInfo.countOfSquares << '\n';
  std::cout << "Rectangles: " << shapeInfo.countOfRectangles << '\n';
  std::cout << "Points: ";

  for (const auto& i : points)
  {
    std::cout << '(' << i.x << ';' << i.y << ") ";
  }
  std::cout << '\n';

  std::cout << "Shapes: \n";
  for (const auto& shape : shapeContainer)
  {
    std::cout << shape.size();
    for (const auto& point : shape)
    {
      std::cout << " (" << point.x << ';' << point.y << ") ";
    }
    std::cout << '\n';
  }
}
