#include <iostream>
#include <sstream>
#include <algorithm>
#include "text_get.hpp"
#include "token.hpp"

void TextGet::readWord(char sym)
{
  std::string tmp;
  tmp.push_back(sym);

  while (std::isalpha(std::cin.peek()) || (std::cin.peek() == '-'))
  {
    char ch = static_cast<char>(std::cin.get());
    if (ch == '-' && std::cin.peek() == '-')
    {
      throw std::invalid_argument("More than one '-' nearby!\n");
    }
    tmp.push_back(ch);
  }

  if (tmp.length() > WORD_MAX_LEN)
  {
    throw std::invalid_argument("Length of the word more than 20!\n");
  }

  text_.push_back(token_t{tmp, token_t::WORD});
}

void TextGet::readNumber(char sym)
{
  std::string tmp;
  tmp.push_back(sym);
  int countOfDots = 0;

  while (std::isdigit(std::cin.peek()) || (std::cin.peek() == '.'))
  {
    char ch = static_cast<char>(std::cin.get());
    if (ch == '.')
    {
      ++countOfDots;
      if (countOfDots > 1)
      {
        throw std::invalid_argument("More than one dot in number!\n");
      }
    }
    tmp.push_back(ch);
  }

  if (tmp.length() > WORD_MAX_LEN)
  {
    throw std::invalid_argument("Length of the number more than 20!\n");
  }

  text_.push_back(token_t{tmp, token_t::NUMBER});
}

void TextGet::readDash(char sym)
{
  std::string tmp;
  tmp.push_back(sym);

  while (std::cin.peek() == '-')
  {
    tmp.push_back(static_cast<char>(std::cin.get()));
  }

  if (tmp.length() != DASH_MAX_LEN)
  {
    throw std::invalid_argument("Length of the dash should be 3!\n");
  }

  text_.push_back(token_t{tmp, token_t::DASH});
}

std::vector<token_t> TextGet::readText()
{
  while (std::cin)
  {
    std::cin >> std::ws;
    char firstChar = static_cast<char>(std::cin.get());
    char secondChar = static_cast<char>(std::cin.peek());

    if (std::isalpha(firstChar))
    {
      readWord(firstChar);
    }
    else if ((std::isdigit(firstChar)) || ((firstChar == '+' || firstChar == '-') && std::isdigit(secondChar)))
    {
      readNumber(firstChar);
    }
    else if (std::ispunct(firstChar))
    {
      if (firstChar == '-')
      {
        readDash(firstChar);
      }
      else
      {
        text_.push_back(token_t{std::string(1, firstChar), token_t::PUNCT});
      }
    }
  }

  return text_;
}
