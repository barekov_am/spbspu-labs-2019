#ifndef TEXT_GET_HPP
#define TEXT_GET_HPP

#include <string>
#include <vector>
#include "token.hpp"

const std::size_t WORD_MAX_LEN = 20;
const std::size_t DASH_MAX_LEN = 3;

class TextGet
{
public:
  std::vector<token_t> readText();

private:
  void readWord(char);
  void readNumber(char);
  void readDash(char);

  std::vector<token_t> text_;
};

#endif
