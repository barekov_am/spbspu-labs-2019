#include <iostream>
#include <stdexcept>
#include "text_get.hpp"

const size_t MIN_WIDTH = 25;
const size_t DEFAULT_WIDTH = 40;

void transform_print(std::vector<token_t> &, size_t);

int main(int argc, char * argv[])
{
  try
  {
    if ((argc != 1) && (argc != 3))
    {
      std::cerr << "Invalid number of arguments!\n";
      return 1;
    }

    size_t width = DEFAULT_WIDTH;

    if (argc == 3)
    {
      width = std::stoi(argv[2]);
      if ((std::string(argv[1]) != "--line-width") || (width < MIN_WIDTH))
      {
        std::cerr << "Invalid arguments!\n";
        return 1;
      }
    }

    TextGet text;
    std::vector<token_t> textOut = text.readText();
    transform_print(textOut, width);
  }

  catch (const std::exception & e)
  {
    std::cerr << e.what();
    return 2;
  }

  return 0;
}
