#ifndef TOKEN_HPP
#define TOKEN_HPP

#include <string>

struct token_t
{
  enum token_type
  {
    WORD,
    NUMBER,
    PUNCT,
    DASH,
  };

  std::string value;
  token_type type;
};

#endif
