#include <iostream>
#include <sstream>
#include <algorithm>
#include "token.hpp"
#include "text_get.hpp"

void transform_print(std::vector<token_t> & text, size_t outStrWidth)
{
  if (text.empty())
  {
    return;
  }
  if ((text.front().type == token_t::PUNCT) || (text.front().type == token_t::DASH))
  {
    throw std::invalid_argument("Cannot begin with punctuation!\n");
  }

  for (auto it = text.begin(); it != text.end() - 1; ++it)
  {
    if (it->type == token_t::PUNCT)
    {
      if ((it->value == ",") && ((it + 1)->type == token_t::DASH))
      {
        (it + 1)->value.insert(0, " ");
      }
      else if ((it + 1)->type == token_t::PUNCT || (it + 1)->type == token_t::DASH)
      {
        throw std::invalid_argument("Two or more punctuation symbols nearby!\n");
      }
    }
  }

  std::for_each(text.begin(), text.end(), [](token_t & token)
  {
    if (isalpha(token.value[0]) || isdigit(token.value[0]) || (token.value[0] == '+') || ((token.value[0] == '-')))
    {
      token.value.insert(0, " ");
    }
  });

  size_t tmpWidth = 0;
  bool isBeginOfLine = true;

  for (auto it = text.begin(); it != text.end(); ++it)
  {
    if (isBeginOfLine)
    {
      it->value.erase(0, 1);
      isBeginOfLine = false;
    }

    if (((it != text.end() - 1)) && ((it + 1)->type == token_t::DASH || (it + 1)->type == token_t::PUNCT))
    {
      if (it->value.size() + (it + 1)->value.size() + tmpWidth > outStrWidth)
      {
        std::cout << "\n" << it->value.erase(0, 1);
        tmpWidth = it->value.size();
        continue;
      }
    }

    if ((tmpWidth + it->value.size()) < outStrWidth)
    {
      std::cout << it->value;
      tmpWidth += it->value.size();
      isBeginOfLine = false;
    }
    else if (((tmpWidth + it->value.size()) > outStrWidth))
    {
      std::cout << "\n" << it->value.erase(0, 1);
      tmpWidth = it->value.size();
      isBeginOfLine = false;
    }
    else if (((tmpWidth + it->value.size()) == outStrWidth))
    {
      std::cout << it->value << "\n";
      tmpWidth = 0;
      isBeginOfLine = true;
    }
  }
  std::cout << "\n";
}
