#include <utility> // for std::move()
#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "split.hpp"

int main()
{
  golikov::ShapePointer mycircle(std::make_shared<golikov::Circle>(3, -4, -4));
  // create the object of the class Circle

  golikov::ShapePointer myrectangle(std::make_shared<golikov::Rectangle>(mycircle->getFrameRect()));
  // create the object of the class Rectangle
  // initialized the Rectangle object's field "m_figure" with circle's frame rectangle

  golikov::ShapePointer figures[] = {mycircle, myrectangle};
  int size = sizeof(figures)/sizeof(golikov::Shape);

  for (int i = 0; i < size; i++)
  {
    figures[i]->showParams();
  }

  for (int i = 0; i < size; i++)
  {
    figures[i]->move(5.0,5.0);
    figures[i]->showParams();
  }

  // demonstration of scaling
  for (int i = 0; i < size; i++)
  {
    figures[i]->scale(2.0);
    figures[i]->showParams();
  }

  // demonstration of composite shape abilities
  const golikov::rectangle_t rect{{1, 1}, 5, 6};
  golikov::ShapePointer rectangle1(std::make_shared<golikov::Rectangle>(rect));

  golikov::CompositeShape compShape1;
  compShape1.add(rectangle1);
  compShape1.add(mycircle);
  compShape1[0]->showParams(); // overloaded operator []

  compShape1.remove(1);

  golikov::CompositeShape compShape2(compShape1); // copied from 1-st to 2-nd
  golikov::CompositeShape compShape3(std::move(compShape2)); // moved from 2-nd to 3-rd
  golikov::CompositeShape compShape4 = compShape1; // copied from 1-st to 4-th
  golikov::CompositeShape compShape5 = std::move(compShape4); // moved from 4-th to 5-th

  compShape1.move({6, 6});
  compShape1.scale(1.5);

  compShape1.showParams();

  return 0;
}
