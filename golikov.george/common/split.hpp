#ifndef SPLIT_HPP
#define SPLIT_HPP

#include "matrix.hpp"

namespace golikov
{
  bool overlap(const rectangle_t &, const rectangle_t &);
  golikov::Matrix split(const golikov::CompositeShape &);
}

#endif
