#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace golikov
{
  class Rectangle :
          public golikov::Shape
  {
  public:
    Rectangle(const rectangle_t &);
    Rectangle(double, double, double, double);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(double, double) override;
    void scale(double) override;
    void rotate(double) override;
    void showParams() const override;
    double getAngle() const;

  private:
    rectangle_t m_figure;
    double m_curAngle;
  };
}

#endif
