#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace golikov
{
  class Circle :
          public golikov::Shape
  {
  public:
    Circle(double, const point_t &);
    Circle(double, double, double);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(double, double) override;
    void scale(double) override;
    void rotate(double) override;
    void showParams() const override;

  private:
    double m_radius;
    point_t m_center;
  };
}

#endif
