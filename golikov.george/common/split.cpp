#include "split.hpp"
#include <cmath>
#include <stdexcept>

bool golikov::overlap(const rectangle_t & rect1, const rectangle_t & rect2)
{
  return (((rect1.width / 2) + (rect2.width / 2) > fabs(rect1.pos.x - rect2.pos.x)) &&
          ((rect1.height / 2) + (rect2.height / 2) > fabs(rect1.pos.y - rect2.pos.y)));
}

golikov::Matrix golikov::split(const golikov::CompositeShape & compShape)
{
  size_t countOfFigures = compShape.getCount();
  if (countOfFigures != 0)
  {
    size_t arrOfLays[countOfFigures]; // assign each figure from an array "figures" a lay
    for (size_t i = 0; i < countOfFigures; i++)
    {
      arrOfLays[i] = 0; // all the figures are at least on lay 0
    }

    size_t numOfLays = 0; // max number in arrOfLays

    for (size_t i = 1; i < countOfFigures; i++) // the first figure is always on lay 0
    {
      golikov::rectangle_t frameRect = compShape[i]->getFrameRect();

      bool temp[numOfLays + 1]; // temporary array to determine the minimum lay where figure does not overlap others

      for (size_t j = 0; j < (numOfLays + 1); j++)
      {
        temp[j] = true;
      }

      for (size_t j = 0; j < i; j++)
      {
        if (overlap(frameRect, compShape[j]->getFrameRect()))
        {
          temp[arrOfLays[j]] = false;
        }
      }

      // find the minimum lay(min index k where the value of bool array[k] is true) for the figure
      for (size_t k = 0; k < (numOfLays + 1); k++)
      {
        if (temp[k])
        {
          arrOfLays[i] = k;
          break;
        }
        else // if all the figures are overlapped by the current figure, the latter is assigned to new lay(max + 1)
        {
          arrOfLays[i] = k + 1;
        }
      }

      if (arrOfLays[i] > numOfLays) // update max number of lays
      {
        numOfLays = arrOfLays[i];
      }
    }

  return golikov::Matrix(compShape, arrOfLays, (numOfLays + 1)); // rows are indexed from 1
  }
  else
  {
    return golikov::Matrix();
  }
}
