#include <utility> // for std::move()
#include <boost/test/auto_unit_test.hpp>
#include <cmath>
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

const double epsilon = 0.01;

golikov::ShapePointer circ1(std::make_shared<golikov::Circle>(5, 2, 3));
golikov::ShapePointer circ2(std::make_shared<golikov::Circle>(3, -4, -4));


BOOST_AUTO_TEST_SUITE(testsCompositeShape)

BOOST_AUTO_TEST_CASE(testCompositeShapeCopyConstructor)
{
  golikov::CompositeShape compShape1;

  compShape1.add(circ1);
  compShape1.add(circ2);
  golikov::CompositeShape compShape2(compShape1);

  BOOST_CHECK_EQUAL(compShape1.getCount(), compShape2.getCount());
  for (size_t i = 0; i < compShape1.getCount(); i++)
  {
    BOOST_CHECK_EQUAL(compShape1[i] == compShape2[i], true);
  }
}

BOOST_AUTO_TEST_CASE(testCompositeShapeMoveConstructor)
{
  golikov::CompositeShape compShape1;

  compShape1.add(circ1);
  compShape1.add(circ2);
  size_t countBeforeMoving = compShape1.getCount();
  golikov::ShapePointer figure1 = compShape1[0];
  golikov::ShapePointer figure2 = compShape1[1];

  golikov::CompositeShape compShape2(std::move(compShape1));

  BOOST_CHECK_EQUAL(compShape2.getCount(), countBeforeMoving);
  BOOST_CHECK_EQUAL(compShape2[0] == figure1, true);
  BOOST_CHECK_EQUAL(compShape2[1] == figure2, true);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeCopyOperator)
{
  golikov::CompositeShape compShape1;

  compShape1.add(circ1);
  compShape1.add(circ2);
  golikov::CompositeShape compShape2(compShape1);

  BOOST_CHECK_EQUAL(compShape1.getCount(), compShape2.getCount());
  for (size_t i = 0; i < compShape1.getCount(); i++)
  {
    BOOST_CHECK_EQUAL(compShape1[i] == compShape2[i], true);
  }
}

BOOST_AUTO_TEST_CASE(testCompositeShapeMoveOperator)
{
  golikov::CompositeShape compShape1;

  compShape1.add(circ1);
  compShape1.add(circ2);
  size_t countBeforeMoving = compShape1.getCount();
  golikov::ShapePointer figure1 = compShape1[0];
  golikov::ShapePointer figure2 = compShape1[1];

  golikov::CompositeShape compShape2 = std::move(compShape1);

  BOOST_CHECK_EQUAL(compShape2.getCount(), countBeforeMoving);
  BOOST_CHECK_EQUAL(compShape2[0] == figure1, true);
  BOOST_CHECK_EQUAL(compShape2[1] == figure2, true);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeArea)
{
  golikov::CompositeShape compShape1;

  compShape1.add(circ1);
  compShape1.add(circ2);

  BOOST_CHECK_CLOSE(compShape1.getArea(), circ1->getArea() + circ2->getArea(), epsilon);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeFrameRect)
{
  golikov::CompositeShape compShape1;
  BOOST_CHECK_THROW(compShape1.getFrameRect(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeMoving)
{
  golikov::CompositeShape compShape1;
  compShape1.add(circ1);
  compShape1.add(circ2);
  const double areaBeforeMoving = compShape1.getArea();
  const double widthBeforeMoving = compShape1.getFrameRect().width;
  const double heightBeforeMoving = compShape1.getFrameRect().height;
  compShape1.move({10, 10});

  BOOST_CHECK_CLOSE(areaBeforeMoving, compShape1.getArea(), epsilon);
  BOOST_CHECK_CLOSE(widthBeforeMoving, compShape1.getFrameRect().width, epsilon);
  BOOST_CHECK_CLOSE(heightBeforeMoving, compShape1.getFrameRect().height, epsilon);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeScaling)
{
  golikov::CompositeShape compShape1;
  const double coefficient = 3;

  compShape1.add(circ1);
  compShape1.add(circ2);
  // invalid parameter
  BOOST_CHECK_THROW(compShape1.scale(-1), std::invalid_argument);

  // isotropic scaling coef < 1
  double oldFrameRectCenterX = compShape1.getFrameRect().pos.x;
  double oldFrameRectCenterY = compShape1.getFrameRect().pos.y;
  compShape1.scale(0.3);
  BOOST_CHECK_CLOSE(oldFrameRectCenterX, compShape1.getFrameRect().pos.x, epsilon);
  BOOST_CHECK_CLOSE(oldFrameRectCenterY, compShape1.getFrameRect().pos.y, epsilon);

  // isotropic scaling coef > 1
  oldFrameRectCenterX = compShape1.getFrameRect().pos.x;
  oldFrameRectCenterY = compShape1.getFrameRect().pos.y;
  compShape1.scale(1.5);
  BOOST_CHECK_CLOSE(oldFrameRectCenterX, compShape1.getFrameRect().pos.x, epsilon);
  BOOST_CHECK_CLOSE(oldFrameRectCenterY, compShape1.getFrameRect().pos.y, epsilon);

  // area
  const double areaBeforeScaling = compShape1.getArea();
  compShape1.scale(coefficient);
  BOOST_CHECK_CLOSE(areaBeforeScaling * coefficient * coefficient, compShape1.getArea(), epsilon);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeRotating)
{
  golikov::CompositeShape compShape1;
  compShape1.add(circ1);
  compShape1.add(circ2);
  const double oldCenterX = compShape1[0]->getFrameRect().pos.x;
  const double oldCenterY = compShape1[0]->getFrameRect().pos.y;

  const double distanceX = compShape1.getFrameRect().pos.x - oldCenterX;
  const double distanceY = compShape1.getFrameRect().pos.y - oldCenterY;

  compShape1.rotate(45);

  BOOST_CHECK_CLOSE(compShape1[0]->getFrameRect().pos.x, oldCenterX + distanceX * cos((45 * M_PI) / 180)
                    - distanceY * sin((45 * M_PI) / 180), epsilon);
  BOOST_CHECK_CLOSE(compShape1[0]->getFrameRect().pos.y, oldCenterY + distanceX * sin((45 * M_PI) / 180)
                                                         + distanceY * cos((45 * M_PI) / 180), epsilon);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeAdding)
{
  golikov::CompositeShape compShape1;

  size_t oldCount = compShape1.getCount();
  double oldArea = compShape1.getArea();
  compShape1.add(circ1);

  BOOST_CHECK_EQUAL(compShape1[oldCount] == circ1, true);
  BOOST_CHECK_EQUAL(compShape1.getCount(), oldCount + 1);
  BOOST_CHECK_CLOSE(compShape1.getArea(), oldArea + circ1->getArea(), epsilon);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeRemoving)
{
  golikov::CompositeShape compShape1;

  size_t oldCount = compShape1.getCount();
  double oldArea = compShape1.getArea();
  compShape1.add(circ1);
  BOOST_CHECK_THROW(compShape1.remove(5), std::out_of_range);

  compShape1.remove(0);
  BOOST_CHECK_EQUAL(compShape1.getCount(), oldCount);
  BOOST_CHECK_CLOSE(compShape1.getArea(), oldArea, epsilon);
}

BOOST_AUTO_TEST_SUITE_END()
