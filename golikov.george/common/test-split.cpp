#include <boost/test/auto_unit_test.hpp>
#include "split.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

const double epsilon = 0.1;

BOOST_AUTO_TEST_SUITE(testsSplitting)

BOOST_AUTO_TEST_CASE(testOverlapping)
{
  BOOST_CHECK_EQUAL(golikov::overlap(golikov::Circle(2, 2, 5).getFrameRect(),
                                     golikov::Circle(4, 4, 5).getFrameRect()), true);

  BOOST_CHECK_EQUAL(golikov::overlap(golikov::Circle(2, 2, 2).getFrameRect(),
                                     golikov::Circle(2, 10, 10).getFrameRect()), false);
}

BOOST_AUTO_TEST_CASE(testMatrixSplitting)
{
  golikov::ShapePointer circle1(std::make_shared<golikov::Circle>(3, -4, -4));
  golikov::ShapePointer circle2(std::make_shared<golikov::Circle>(3, -5, -5));

  golikov::ShapePointer rect1(std::make_shared<golikov::Rectangle>(2, 2, 5 ,6));
  golikov::ShapePointer rect2(std::make_shared<golikov::Rectangle>(15, 15, 3 ,3));

  golikov::CompositeShape compShape1;
  BOOST_CHECK_EQUAL(compShape1.getCount(), golikov::split(compShape1).getCount());

  compShape1.add(rect1); // 0 lay
  compShape1.add(circle1); // 0 lay
  compShape1.add(circle2); // 1 lay
  compShape1.add(rect2); // 0 lay

  golikov::Matrix matrix1 = golikov::split(compShape1);

  BOOST_CHECK_EQUAL(matrix1.getRows(), 2);
  BOOST_CHECK_EQUAL(matrix1.getCount(), compShape1.getCount());

  BOOST_CHECK_EQUAL(matrix1.getColumns(0), 3);
  BOOST_CHECK_EQUAL(matrix1.getColumns(1), 1);

  BOOST_CHECK_EQUAL(matrix1[0][0] == compShape1[0], true);
  BOOST_CHECK_EQUAL(matrix1[0][1] == compShape1[1], true);
  BOOST_CHECK_EQUAL(matrix1[1][0] == compShape1[2], true);
  BOOST_CHECK_EQUAL(matrix1[0][2] == compShape1[3], true);
}

BOOST_AUTO_TEST_SUITE_END()
