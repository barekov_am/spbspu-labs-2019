#include <boost/test/auto_unit_test.hpp>
#include "split.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

const double epsilon = 0.1;

BOOST_AUTO_TEST_SUITE(testsMatrix)

BOOST_AUTO_TEST_CASE(testMatrixDefaultConstr)
{
  golikov::Matrix matrix1;
  BOOST_CHECK_EQUAL(matrix1.getCount(), 0);
  BOOST_CHECK_EQUAL(matrix1.getRows(), 0);
}

BOOST_AUTO_TEST_CASE(testMatrixCopyConstructor)
{
  golikov::ShapePointer circle1(std::make_shared<golikov::Circle>(3, -4, -4));
  golikov::ShapePointer circle2(std::make_shared<golikov::Circle>(3, -5, -5));

  golikov::ShapePointer rect1(std::make_shared<golikov::Rectangle>(2, 2, 5 ,6));
  golikov::ShapePointer rect2(std::make_shared<golikov::Rectangle>(15, 15, 3 ,3));

  golikov::CompositeShape compShape1;

  compShape1.add(rect1); // 0 lay
  compShape1.add(circle1); // 0 lay
  compShape1.add(circle2); // 1 lay
  compShape1.add(rect2); // 0 lay

  golikov::Matrix matrix1 = golikov::split(compShape1);
  golikov::Matrix matrix2(matrix1);

  BOOST_CHECK_EQUAL(matrix1.getCount(), matrix2.getCount());

  BOOST_CHECK_EQUAL(matrix1[0][0] == matrix2[0][0], true);
  BOOST_CHECK_EQUAL(matrix1[0][1] == matrix2[0][1], true);
  BOOST_CHECK_EQUAL(matrix1[1][0] == matrix2[1][0], true);
  BOOST_CHECK_EQUAL(matrix1[0][2] == matrix2[0][2], true);
}

BOOST_AUTO_TEST_CASE(testMatrixMoveConstructor)
{
  golikov::ShapePointer circle1(std::make_shared<golikov::Circle>(3, -4, -4));

  golikov::ShapePointer rect1(std::make_shared<golikov::Rectangle>(2, 2, 5 ,6));

  golikov::CompositeShape compShape1;

  compShape1.add(rect1); // 0 lay
  compShape1.add(circle1); // 0 lay

  golikov::Matrix matrix1 = golikov::split(compShape1);

  size_t countBeforeMoving = matrix1.getCount();
  golikov::ShapePointer figure1 = matrix1[0][0];
  golikov::ShapePointer figure2 = matrix1[0][1];

  golikov::Matrix matrix2(std::move(matrix1));


  BOOST_CHECK_EQUAL(matrix2.getCount(), countBeforeMoving);
  BOOST_CHECK_EQUAL(matrix2[0][0] == figure1, true);
  BOOST_CHECK_EQUAL(matrix2[0][1] == figure2, true);
}

BOOST_AUTO_TEST_CASE(testMatrixCopyOperator)
{
  golikov::ShapePointer circle1(std::make_shared<golikov::Circle>(3, -4, -4));
  golikov::ShapePointer circle2(std::make_shared<golikov::Circle>(3, -5, -5));

  golikov::ShapePointer rect1(std::make_shared<golikov::Rectangle>(2, 2, 5 ,6));
  golikov::ShapePointer rect2(std::make_shared<golikov::Rectangle>(15, 15, 3 ,3));

  golikov::CompositeShape compShape1;

  compShape1.add(rect1); // 0 lay
  compShape1.add(circle1); // 0 lay
  compShape1.add(circle2); // 1 lay
  compShape1.add(rect2); // 0 lay

  golikov::Matrix matrix1 = golikov::split(compShape1);
  golikov::Matrix matrix2 = matrix1;

  BOOST_CHECK_EQUAL(matrix1.getCount(), matrix2.getCount());

  BOOST_CHECK_EQUAL(matrix1[0][0] == matrix2[0][0], true);
  BOOST_CHECK_EQUAL(matrix1[0][1] == matrix2[0][1], true);
  BOOST_CHECK_EQUAL(matrix1[1][0] == matrix2[1][0], true);
  BOOST_CHECK_EQUAL(matrix1[0][2] == matrix2[0][2], true);
}

BOOST_AUTO_TEST_CASE(testMatrixMoveOperator)
{
  golikov::ShapePointer circle1(std::make_shared<golikov::Circle>(3, -4, -4));

  golikov::ShapePointer rect1(std::make_shared<golikov::Rectangle>(2, 2, 5 ,6));

  golikov::CompositeShape compShape1;

  compShape1.add(rect1); // 0 lay
  compShape1.add(circle1); // 0 lay

  golikov::Matrix matrix1 = golikov::split(compShape1);

  size_t countBeforeMoving = matrix1.getCount();
  golikov::ShapePointer figure1 = matrix1[0][0];
  golikov::ShapePointer figure2 = matrix1[0][1];

  golikov::Matrix matrix2 = std::move(matrix1);


  BOOST_CHECK_EQUAL(matrix2.getCount(), countBeforeMoving);
  BOOST_CHECK_EQUAL(matrix2[0][0] == figure1, true);
  BOOST_CHECK_EQUAL(matrix2[0][1] == figure2, true);
}

BOOST_AUTO_TEST_CASE(testFindNumOfFigsInLay)
{
  size_t testArr[] = {0, 1, 0, 0, 1, 2};

  BOOST_CHECK_EQUAL(golikov::findNumOfFigsInLay(testArr, 6, 0), 3);
}

BOOST_AUTO_TEST_SUITE_END()
