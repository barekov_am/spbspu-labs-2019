#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <cstddef>
#include "shape.hpp"
#include "pointer-types.hpp"

namespace golikov
{
  class CompositeShape :
          public golikov::Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&) noexcept;
    ~CompositeShape() override = default;
    CompositeShape & operator=(const CompositeShape &);
    CompositeShape & operator=(CompositeShape &&) noexcept;
    ShapePointer & operator[](size_t) const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(double, double) override;
    void scale(double) override;
    void rotate(double) override;
    void showParams() const override;
    void add(golikov::ShapePointer);
    void remove(size_t); // parameter - index of an element to remove
    size_t getCount() const;

  private:
    ShapeArray m_figures;
    size_t m_count;
  };
}

#endif
