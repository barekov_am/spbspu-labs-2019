#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <cstddef>
#include "shape.hpp"
#include "composite-shape.hpp"

namespace golikov
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const golikov::CompositeShape & compShape, size_t * arrOfLays, size_t rows);
    Matrix(const Matrix &);
    Matrix(Matrix &&) noexcept;
    ~Matrix() = default;
    Matrix & operator=(const Matrix &);
    Matrix & operator=(Matrix &&) noexcept;
    ShapePointer * operator[](size_t) const;
    size_t getRows() const;
    size_t getColumns(size_t) const;
    size_t getCount() const;
    void showParams() const;
  private:
    ShapeArray figures_;
    size_t rows_;
    SizeTArray columns_;
    size_t count_;
  };

  // count columns for matrix
  // the function finds the number of repeated numbers in arrOfLays
  size_t findNumOfFigsInLay(size_t * arrOfLays, size_t countOfFigures, size_t neededLay);
}

#endif
