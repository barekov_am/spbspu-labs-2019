#ifndef POINTER_TYPES_HPP
#define POINTER_TYPES_HPP

#include <memory>

#include "shape.hpp"

namespace golikov
{
  using ShapePointer = std::shared_ptr<Shape>;
  using ShapeArray = std::unique_ptr<ShapePointer[]>;
  using SizeTArray = std::unique_ptr<size_t[]>;
}

#endif
