#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

golikov::Rectangle::Rectangle(const rectangle_t & figure):
  m_figure(figure),
  m_curAngle(0)
{
  if ((figure.width <= 0) || (figure.height <= 0))
  {
    throw std::invalid_argument("Rectangle's width or height cannot be negative");
  }
}

golikov::Rectangle::Rectangle(double posX, double posY, double width, double height):
  Rectangle({{posX, posY}, width, height})
{Rectangle({{posX, posY}, width, height}); }

double golikov::Rectangle::getArea() const
{
  return m_figure.width * m_figure.height;
}

golikov::rectangle_t golikov::Rectangle::getFrameRect() const
{
  double angleRad = (m_curAngle * M_PI) / 180;
  double cosAngle = fabs(cos(angleRad));
  double sinAngle = fabs(sin(angleRad));

  return {m_figure.pos, m_figure.width * cosAngle + m_figure.height * sinAngle,
          m_figure.height * cosAngle + m_figure.width * sinAngle};
}

void golikov::Rectangle::move(const point_t & pos)
{
  m_figure.pos = pos;
}

void golikov::Rectangle::move(double dx, double dy)
{
  m_figure.pos.x += dx;
  m_figure.pos.y += dy;
}

void golikov::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Wouldn't multiply on a negative coefficient");
  }
  else
  {
    m_figure.height *= coefficient;
    m_figure.width *= coefficient;
  }
}

void golikov::Rectangle::rotate(double angle)
{
  m_curAngle += angle;
}

void golikov::Rectangle::showParams() const
{
  std::cout << "Rectangle: center - {" << m_figure.pos.x << ","
            << m_figure.pos.y << "}\n" << "Width - " << m_figure.width
            << '\n' << "Height - " << m_figure.height << '\n'
            << "Area - " << getArea() << '\n';
}

double golikov::Rectangle::getAngle() const
{
  return m_curAngle;
}
