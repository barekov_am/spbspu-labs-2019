#include <boost/test/auto_unit_test.hpp>
#include <cmath>
#include "rectangle.hpp"

const double epsilon = 0.1;

BOOST_AUTO_TEST_SUITE(testsRectangle)

BOOST_AUTO_TEST_CASE(testRectangleImmutabilityMoving)
{
  golikov::Rectangle rectangle1({{5, 5}, 6, 4});
  const double areaBeforeMoving = rectangle1.getArea();
  const double widthBeforeMoving = rectangle1.getFrameRect().width;
  const double heightBeforeMoving = rectangle1.getFrameRect().height;
  rectangle1.move({10, 10});

  BOOST_CHECK_CLOSE(areaBeforeMoving, rectangle1.getArea(), epsilon);
  BOOST_CHECK_CLOSE(widthBeforeMoving, rectangle1.getFrameRect().width, epsilon);
  BOOST_CHECK_CLOSE(heightBeforeMoving, rectangle1.getFrameRect().height, epsilon);
}

BOOST_AUTO_TEST_CASE(testRectangleImmutabilityScaling)
{
  golikov::Rectangle rectangle1({{5, 5}, 6, 4});
  const double coefficient = 3;
  const double areaBeforeScaling = rectangle1.getArea();
  rectangle1.scale(coefficient);

  BOOST_CHECK_CLOSE(areaBeforeScaling * coefficient * coefficient, rectangle1.getArea(), epsilon);
}

BOOST_AUTO_TEST_CASE(testRectangleImmutabilityRotating)
{
  golikov::Rectangle rect1({{5, 5}, 6, 4});
  const double areaBeforeRotating = rect1.getArea();
  rect1.rotate(45);

  BOOST_CHECK_CLOSE(areaBeforeRotating, rect1.getArea(), epsilon);
}

BOOST_AUTO_TEST_CASE(testRectangleRotating)
{
  golikov::Rectangle rect1({{5, 5}, 6, 4});
  const golikov::point_t posBeforeRotating = rect1.getFrameRect().pos;
  const double widthBeforeRotating = rect1.getFrameRect().width;
  const double heightBeforeRotating = rect1.getFrameRect().height;
  const double rotateAngle = 60;

  rect1.rotate(rotateAngle);
  double cosAngle = cos((rotateAngle * M_PI) / 180);
  double sinAngle = sin((rotateAngle * M_PI) / 180);

  BOOST_CHECK_CLOSE(posBeforeRotating.x, rect1.getFrameRect().pos.x, epsilon);
  BOOST_CHECK_CLOSE(posBeforeRotating.y, rect1.getFrameRect().pos.y, epsilon);

  BOOST_CHECK_CLOSE(widthBeforeRotating * cosAngle + heightBeforeRotating * sinAngle,
                    rect1.getFrameRect().width, epsilon);
  BOOST_CHECK_CLOSE(heightBeforeRotating * cosAngle + widthBeforeRotating * sinAngle,
                    rect1.getFrameRect().height, epsilon);
}

BOOST_AUTO_TEST_CASE(testRectangleIncorrectParams)
{
  golikov::Rectangle rectangle1({{5, 5}, 6, 4});
  BOOST_CHECK_THROW(golikov::Rectangle rectangle1({{5, 5}, -6, 4}), std::invalid_argument);
  // check not positive width or height
  BOOST_CHECK_THROW(rectangle1.scale(-1), std::invalid_argument);
  BOOST_CHECK_THROW(rectangle1.scale(0), std::invalid_argument);
  // check not positive coefficients when scaling
}

BOOST_AUTO_TEST_SUITE_END()
