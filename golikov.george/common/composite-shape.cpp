#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

golikov::CompositeShape::CompositeShape():
  m_count(0)
{ }

golikov::CompositeShape::CompositeShape(const CompositeShape & rhs):
  m_figures(std::make_unique<ShapePointer[]>(rhs.m_count)),
  m_count(rhs.m_count)
{
  for (size_t i = 0; i < rhs.m_count; i++)
  {
    m_figures[i] = rhs.m_figures[i];
  }
}

golikov::CompositeShape::CompositeShape(CompositeShape && rhs) noexcept:
  m_figures(std::move(rhs.m_figures)),
  m_count(rhs.m_count)
{
  rhs.m_count = 0;
}

golikov::CompositeShape & golikov::CompositeShape::operator=(const golikov::CompositeShape & rhs)
{
  if (this == &rhs)
  {
    return *this;
  }

  ShapeArray tempArr = std::make_unique<ShapePointer[]>(rhs.m_count);

  for (size_t i = 0; i < rhs.m_count; i++)
  {
    tempArr[i] = rhs.m_figures[i];
  }
  m_figures.swap(tempArr);
  m_count = rhs.m_count;
  return *this;
}

golikov::CompositeShape & golikov::CompositeShape::operator=(golikov::CompositeShape && rhs) noexcept
{
  if (this == &rhs)
  {
    return *this;
  }

  m_figures = std::move(rhs.m_figures);
  m_count = rhs.m_count;

  rhs.m_count = 0;
  return *this;
}

golikov::ShapePointer & golikov::CompositeShape::operator[](size_t i) const
{
  if (i >= m_count)
  {
    throw std::out_of_range("Out of range index.");
  }
  else
  {
    return m_figures[i];
  }
}

double golikov::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < m_count; i++)
  {
    area += m_figures[i]->getArea();
  }
  return area;
}

golikov::rectangle_t golikov::CompositeShape::getFrameRect() const
{
  if (m_figures != nullptr)
  {
    golikov::rectangle_t tempRect = m_figures[0]->getFrameRect();
    double xMax = (tempRect.pos.x) + (tempRect.width / 2);
    double xMin = (tempRect.pos.x) - (tempRect.width / 2);
    double yMax = (tempRect.pos.y) + (tempRect.height / 2);
    double yMin = (tempRect.pos.y) - (tempRect.height / 2);

    for (size_t i = 0; i < m_count; i++)
    {
      tempRect = m_figures[i]->getFrameRect();
      xMax = std::max((tempRect.pos.x + tempRect.width / 2), xMax);
      xMin = std::min((tempRect.pos.x - tempRect.width / 2), xMin);
      yMax = std::max((tempRect.pos.y + tempRect.height / 2), yMax);
      yMin = std::min((tempRect.pos.y - tempRect.height / 2), yMin);
    }
    return {{((xMax + xMin) / 2), ((yMax + yMin) / 2)}, xMax - xMin, yMax - yMin};
  }
  else
  {
    throw std::logic_error("The composite shape is empty. Cannot get frame rectangle.");
  }
}

void golikov::CompositeShape::move(const point_t & pos)
{
  golikov::rectangle_t tempRect = getFrameRect();
  move(pos.x - tempRect.pos.x, pos.y - tempRect.pos.y);
}

void golikov::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < m_count; i++)
  {
    m_figures[i]->move(dx, dy);
  }
}

void golikov::CompositeShape::scale(double coefficient)
{
  if ((coefficient > 0) && (coefficient != 1))
  {
    golikov::point_t composShapeCenter = getFrameRect().pos;
    golikov::point_t figureCenter = {0, 0};
    for (size_t i = 0; i < m_count; i++) // isotropic scaling of composite shape
    {
      m_figures[i]->scale(coefficient);
      figureCenter = m_figures[i]->getFrameRect().pos;

      /*If willing to scale composite shape isotropically, one has to move centers of internal figures
      according to the given scale coefficient. Formula:*/
      golikov::point_t newPos = {composShapeCenter.x + coefficient * (figureCenter.x - composShapeCenter.x),
                                 composShapeCenter.y + coefficient * (figureCenter.y - composShapeCenter.y)};
      m_figures[i]->move(newPos);
    }
  }
  else
  {
    if (coefficient == 1) // in purpose to optimize when scaling with coef = 1
    {
      return;
    }
    throw std::invalid_argument("Wouldn't multiply on a negative coefficient.");
  }
}

void golikov::CompositeShape::rotate(double angle)
{
  const point_t compShapeCenter = this->getFrameRect().pos;

  for (size_t i = 0; i < m_count; i++)
  {
    m_figures[i]->rotate(angle);

    // rotating composite shape according to task 4 requires moving centers of internal figures

    const point_t curFigureCenter = m_figures[i]->getFrameRect().pos;
    const double cosAngle = cos((angle * M_PI) / 180);
    const double sinAngle = sin((angle * M_PI) / 180);

    const double distanceX = compShapeCenter.x - curFigureCenter.x;
    const double distanceY = compShapeCenter.y - curFigureCenter.y;

    // new coordinates according to matrix of rotating,
    // center of the system of coordinates is the center of composite shape
    double newX = distanceX * cosAngle - distanceY * sinAngle;
    double newY = distanceX * sinAngle + distanceY * cosAngle;

    // back to the real system of coordinates
    newX += curFigureCenter.x;
    newY += curFigureCenter.y;

    m_figures[i]->move({newX, newY});
  }
}

void golikov::CompositeShape::showParams() const
{
  std::cout << "The composite shape center is {" << getFrameRect().pos.x << ","
            << getFrameRect().pos.y << "}\n" << "Width of frame rect - " << getFrameRect().width
            << '\n' << "Height of frame rect - " << getFrameRect().height << '\n'
            << "Area - " << getArea() << '\n' << "The composite consists of "
            << m_count << " figures.";
}

void golikov::CompositeShape::add(golikov::ShapePointer figure)
{
  if (m_figures != nullptr)
  {
    ShapeArray tempArr = std::make_unique<ShapePointer[]>(m_count + 1);
    for (size_t i = 0; i < m_count; i++)
    {
      tempArr[i] = m_figures[i];
    }
    tempArr[m_count] = figure;
    m_figures.swap(tempArr);
    m_count++;
  }
  else // in case when trying to add a figure to empty compositeShape
  {
    ShapeArray tempArr = std::make_unique<ShapePointer[]>(1);
    tempArr[0] = figure;
    m_figures.swap(tempArr);
    m_count = 1;
  }
}

void golikov::CompositeShape::remove(size_t index)
{
  if (index < m_count)
  {
    m_count--;
    for (size_t i = index; i < m_count; i++)
    {
      m_figures[i] = m_figures[i + 1];
    }
    m_figures[m_count] = nullptr;
  }
  else
  {
    throw std::out_of_range("Index shouldn't be negative or more than (the number of figures - 1) in composite.");
  }
}

size_t golikov::CompositeShape::getCount() const
{
  return m_count;
}
