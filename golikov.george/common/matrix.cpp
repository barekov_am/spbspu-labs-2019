#include "matrix.hpp"
#include <iostream>

golikov::Matrix::Matrix():
  rows_(0),
  count_(0)
{ }

golikov::Matrix::Matrix(const golikov::CompositeShape & compShape,
                        size_t * arrOfLays, size_t rows):
  figures_(std::make_unique<ShapePointer[]>(compShape.getCount())),
  rows_(rows),
  columns_(std::make_unique<size_t[]>(rows)),
  count_(compShape.getCount())
{
  size_t tempArr[rows]; // for figures to be correctly assigned to matrix
  for (size_t i = 0; i < rows; i++)
  {
    columns_[i] = golikov::findNumOfFigsInLay(arrOfLays, compShape.getCount(), i);
    tempArr[i] = columns_[i]; // copy of columns_
  }

  for (size_t i = 0; i < compShape.getCount(); i++)
  {
    size_t figuresIndex = 0; // index of a place in matrix to put the current figure into
    for (size_t j = 0; j < arrOfLays[i]; j++)
    {
      figuresIndex += columns_[j];
    }
    figuresIndex += columns_[arrOfLays[i]] - tempArr[arrOfLays[i]];
    tempArr[arrOfLays[i]]--;

    figures_[figuresIndex] = compShape[i];
  }
}

golikov::Matrix::Matrix(const golikov::Matrix & rhs):
  figures_(std::make_unique<ShapePointer[]>(rhs.getCount())),
  rows_(rhs.rows_),
  columns_(std::make_unique<size_t[]>(rhs.rows_)),
  count_(rhs.count_)
{
  for (size_t i = 0; i < getCount(); i++)
  {
    figures_[i] = rhs.figures_[i];
  }

  for (size_t i = 0; i < rows_; i++)
  {
    columns_[i] = rhs.columns_[i];
  }
}

golikov::Matrix::Matrix(golikov::Matrix && rhs) noexcept:
  figures_(std::move(rhs.figures_)),
  rows_(rhs.rows_),
  columns_(std::move(rhs.columns_)),
  count_(rhs.count_)
{
  rhs.rows_ = 0;
  rhs.count_ = 0;
}

golikov::Matrix & golikov::Matrix::operator=(const golikov::Matrix & rhs)
{
  if (this == &rhs)
  {
    return *this;
  }

  ShapeArray tempArr = std::make_unique<ShapePointer[]>(rhs.getCount());
  for (size_t i = 0; i < rhs.getCount(); i++)
  {
    tempArr[i] = rhs.figures_[i];
  }

  SizeTArray tempArr2 = std::make_unique<size_t []>(rhs.rows_);
  for (size_t i = 0; i < rhs.rows_; i++)
  {
    tempArr2[i] = rhs.columns_[i];
  }

  figures_.swap(tempArr);
  columns_.swap(tempArr2);

  rows_ = rhs.rows_;
  count_ = rhs.count_;

  return *this;
}

golikov::Matrix & golikov::Matrix::operator=(golikov::Matrix && rhs) noexcept
{
  if (this == &rhs)
  {
    return *this;
  }
  figures_ = std::move(rhs.figures_);
  columns_ = std::move(rhs.columns_);
  rows_ = rhs.rows_;
  count_ = rhs.count_;

  rhs.rows_ = 0;
  rhs.count_ = 0;

  return *this;
}

golikov::ShapePointer * golikov::Matrix::operator[](size_t indexOfRow) const
{
  if (indexOfRow > (rows_ - 1)) // indexing of rows from 1 and indexing figures_ from 0
  {
    throw std::out_of_range("Out of range index.");
  }
  else
  {
    size_t index = 0;
    for (size_t r = 0; r < indexOfRow; r++)
    {
      index += columns_[r];
    }

    return &figures_[index];
  }
}

size_t golikov::Matrix::getRows() const
{
  return rows_;
}

size_t golikov::Matrix::getColumns(size_t i) const
{
  return columns_[i];
}

size_t golikov::Matrix::getCount() const
{
  return count_;
}

void golikov::Matrix::showParams() const
{
  std::cout << "The number of lays is " << rows_ << '\n';
}


// count columns for matrix
// the function finds the number of repeated numbers in arrOfLays
size_t golikov::findNumOfFigsInLay(size_t * arrOfLays, size_t countOfFigures, size_t neededLay)
{
  size_t k = 0;
  for (size_t i = 0; i < countOfFigures; i++)
  {
    if (arrOfLays[i] == neededLay)
    {
      k++;
    }
  }
  return k;
}
