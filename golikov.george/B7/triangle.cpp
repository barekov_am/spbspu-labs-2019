#include "triangle.hpp"

Triangle::Triangle(const point_t & pos):
        Shape(pos)
{ }

Triangle::Triangle(int x, int y):
        Shape(x, y)
{ }

void Triangle::draw() const
{
  point_t center = getCenter();
  std::cout << "TRIANGLE (" << center.x << ";" << center.y << ")\n";
}
