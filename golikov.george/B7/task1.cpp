#include <functional>
#include <vector>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <cmath>

void task1()
{
  std::vector<double> vectorNumbers((std::istream_iterator<double>(std::cin)), std::istream_iterator<double>());

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::ios_base::failure("Failed during input reading!\n");
  }

  std::transform(vectorNumbers.begin(), vectorNumbers.end(), vectorNumbers.begin(),
                 std::bind1st(std::multiplies<double>(), M_PI));

  std::copy(vectorNumbers.begin(), vectorNumbers.end(), std::ostream_iterator<double>(std::cout, "\n"));
}
