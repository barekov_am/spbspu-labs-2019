#include "circle.hpp"

Circle::Circle(const point_t & pos):
        Shape(pos)
{ }

Circle::Circle(int x, int y):
        Shape(x, y)
{ }

void Circle::draw() const
{
  point_t center = getCenter();
  std::cout << "CIRCLE (" << center.x << ";" << center.y << ")\n";
}
