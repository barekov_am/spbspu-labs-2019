#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>

struct point_t
{
  int x, y;
};

class Shape
{
public:
  Shape(const point_t &);
  Shape(int, int);
  virtual ~Shape() = default;

  point_t getCenter() const;

  bool isMoreLeft(const Shape &) const;
  bool isUpper(const Shape &) const;
  virtual void draw() const = 0;

private:
  point_t center_;
};

#endif
