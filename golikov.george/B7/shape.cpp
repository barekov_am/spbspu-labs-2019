#include "shape.hpp"

Shape::Shape(const point_t & pos):
        center_(pos)
{ }

Shape::Shape(int x, int y):
        Shape(point_t{x, y})
{ }

point_t Shape::getCenter() const
{
  return center_;
}

bool Shape::isMoreLeft(const Shape & rhs) const
{
  return center_.x < rhs.center_.x;
}

bool Shape::isUpper(const Shape & rhs) const
{
  return center_.y > rhs.center_.y;
}
