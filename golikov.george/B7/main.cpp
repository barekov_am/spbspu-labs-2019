#include <exception>
#include <iostream>

void task1();
void task2();

int main(int argc, char * argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Invalid number of arguments!\n";
      return 1;
    }

    char * ptr = nullptr;
    int taskNum = std::strtol(argv[1], &ptr, 10);
    if (*ptr)
    {
      std::cerr << "Invalid task number!\n";
      return 1;
    }

    switch (taskNum)
    {
      case 1:
      {
        task1();
        break;
      }
      case 2:
      {
        task2();
        break;
      }
      default:
      {
        std::cerr << "Invalid task number!\n";
        return 1;
      }
    }
  }

  catch (const std::exception & e)
  {
    std::cerr << e.what() << "\n";
    return 2;
  }

  return 0;
}
