#include <memory>
#include <vector>
#include <iostream>
#include <algorithm>
#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"

using shapePtr = std::shared_ptr<Shape>;

std::vector<shapePtr> getShapes()
{
  std::vector<shapePtr> vectorShapes;
  std::string inputStr;

  while (std::getline(std::cin, inputStr))
  {
    inputStr.erase(std::remove_if(inputStr.begin(), inputStr.end(), isspace), inputStr.end());
    if (inputStr.empty() || inputStr.size() == 1)
    {
      continue;
    }

    auto posOpenBracket = inputStr.find_first_of('(');
    auto posSemicolon = inputStr.find_first_of(';');
    auto posCloseBracket = inputStr.find_first_of(')');

    if ((posOpenBracket == std::string::npos) || (posSemicolon == std::string::npos) ||
        (posCloseBracket == std::string::npos))
    {
      throw std::invalid_argument("Incorrect input string!\n");
    }

    std::string shapeName = inputStr.substr(0, posOpenBracket);
    point_t pos = {std::stoi(inputStr.substr(posOpenBracket + 1, posSemicolon - posOpenBracket + 1)),
                   std::stoi(inputStr.substr(posSemicolon + 1, posCloseBracket - posSemicolon + 1))};

    if (shapeName == "TRIANGLE")
    {
      vectorShapes.push_back(std::make_shared<Triangle>(Triangle(pos)));
    }
    else if (shapeName == "SQUARE")
    {
      vectorShapes.push_back(std::make_shared<Square>(Square(pos)));
    }
    else if (shapeName == "CIRCLE")
    {
      vectorShapes.push_back(std::make_shared<Circle>(Circle(pos)));
    }
    else
    {
      throw std::invalid_argument("Incorrect input string!\n");
    }
  }

  return vectorShapes;
}

void forwardPrint(const std::vector<shapePtr> & vectorShapes)
{
  std::for_each(vectorShapes.begin(), vectorShapes.end(),[](const shapePtr shape){shape->draw();});
}

void reversePrint(const std::vector<shapePtr> & vectorShapes)
{
  std::for_each(vectorShapes.rbegin(), vectorShapes.rend(),[](const shapePtr shape){shape->draw();});
}

void task2()
{
  std::vector<shapePtr> vectorShapes = getShapes();

  std::cout << "Original:\n";
  forwardPrint(vectorShapes);

  std::cout << "Left-Right:\n";
  std::sort(vectorShapes.begin(), vectorShapes.end(),[](const shapePtr lhs, const shapePtr rhs)
  {
    return lhs->isMoreLeft(*rhs);
  });
  forwardPrint(vectorShapes);

  std::cout << "Right-Left:\n";
  reversePrint(vectorShapes);

  std::cout << "Top-Bottom:\n";
  std::sort(vectorShapes.begin(), vectorShapes.end(),[](const shapePtr lhs, const shapePtr rhs)
  {
    return lhs->isUpper(*rhs);
  });
  forwardPrint(vectorShapes);

  std::cout << "Bottom-Top:\n";
  reversePrint(vectorShapes);
}
