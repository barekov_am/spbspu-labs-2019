#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "shape.hpp"

class Square : public Shape
{
public:
  Square(const point_t &);
  Square(int, int);

  void draw() const override;
};

#endif
