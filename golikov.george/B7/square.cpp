#include "square.hpp"

Square::Square(const point_t & pos):
        Shape(pos)
{ }

Square::Square(int x, int y):
        Shape(x, y)
{ }

void Square::draw() const
{
  point_t center = getCenter();
  std::cout << "SQUARE (" << center.x << ";" << center.y << ")\n";
}
