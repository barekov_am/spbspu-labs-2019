#include <iostream>
#include <sstream>

#include "commands.hpp"

void task1()
{
  PhoneBookInterface bookInter;
  std::string inputStr;

  while (std::getline(std::cin, inputStr))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed during reading!\n");
    }

    std::stringstream stream(inputStr);
    std::string streamCommand;
    stream >> streamCommand;

    if (streamCommand == "add")
    {
      add(bookInter, stream);
    }
    else if (streamCommand == "store")
    {
      store(bookInter, stream);
    }
    else if (streamCommand == "insert")
    {
      insert(bookInter, stream);
    }
    else if (streamCommand == "delete")
    {
      deleteRecord(bookInter, stream);
    }
    else if (streamCommand == "show")
    {
      show(bookInter, stream);
    }
    else if (streamCommand == "move")
    {
      move(bookInter, stream);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
