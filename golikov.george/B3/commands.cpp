#include <iostream>
#include <sstream>

#include "commands.hpp"

std::string getName(std::string & name)
{
  if ((name.front() != '\"') || (name.back() != '\"') || (name.empty()))
  {
    return "";
  }

  name.erase(name.begin());

  size_t i = 0;
  while ((i < name.size()) && (name[i] != '\"'))
  {
    if (name[i] == '\\')
    {
      if ((name[i + 1] == '\"') && ((i + 2) < name.size()))
      {
        name.erase(i, 1);
      }
      else
      {
        return "";
      }
    }
    ++i;
  }

  if (i == name.size())
  {
    return "";
  }
  name.erase(i);

  if (name.empty())
  {
    return "";
  }

  return name;
}

bool checkNumber(const std::string & number)
{
  if (number.empty())
  {
    return false;
  }

  for (size_t i = 0; i < number.size(); i++)
  {
    if (!std::isdigit(number[i]))
    {
      return false;
    }
  }

  return true;
}

bool checkMarkName(const std::string & name)
{
  if (name.empty())
  {
    return false;
  }

  for (size_t i = 0; i < name.size(); i++)
  {
    if (!isalnum(name[i]) && (name[i] != '-'))
    {
      return false;
    }
  }

  return true;
}

void add(PhoneBookInterface & bookInter, std::stringstream & inputStr)
{
  std::string number;
  std::string name;

  inputStr >> std::ws >> number;
  std::getline(inputStr >> std::ws, name);
  name = getName(name);

  if ((name.empty()) || (!checkNumber(number)))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  PhoneBook::record_t record = {name, number};
  bookInter.add(record);
}

void store(PhoneBookInterface & bookInter, std::stringstream & inputStr)
{
  std::string markName;
  std::string newMarkName;

  inputStr >> std::ws >> markName;
  inputStr >> std::ws >> newMarkName;

  if ((!checkMarkName(markName)) || (!checkMarkName(newMarkName)))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  bookInter.store(markName, newMarkName);
}

void insert(PhoneBookInterface & bookInter, std::stringstream & inputStr)
{
  std::string direction;
  std::string markName;
  std::string number;
  std::string name;

  inputStr >> std::ws >> direction;
  inputStr >> std::ws >> markName;
  inputStr >> std::ws >> number;
  std::getline(inputStr >> std::ws, name);
  name = getName(name);

  PhoneBookInterface::InsertPosition position;

  if ((!checkMarkName(markName)) || name.empty() || (!checkNumber(number)))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  PhoneBook::record_t record = {name, number};

  if (direction == "before")
  {
    position = PhoneBookInterface::InsertPosition::before;
    bookInter.insert(position, markName, record);
  }
  else if (direction == "after")
  {
    position = PhoneBookInterface::InsertPosition::after;
    bookInter.insert(position, markName, record);
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
  }
}

void deleteRecord(PhoneBookInterface & bookInter, std::stringstream & inputStr)
{
  std::string markName;
  inputStr >> std::ws >> markName;

  if (!checkMarkName(markName))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  bookInter.deleteNote(markName);
}

void show(PhoneBookInterface & bookInter, std::stringstream & inputStr)
{
  std::string markName;
  inputStr >> std::ws >> markName;
  if (!checkMarkName(markName))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  bookInter.show(markName);
}

void move(PhoneBookInterface & bookInter, std::stringstream & inputStr)
{
  std::string markName;
  std::string steps;
  inputStr >> std::ws >> markName;

  if (!checkMarkName(markName))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  inputStr >> std::ws >> steps;

  if (steps == "first")
  {
    PhoneBookInterface::MovePosition position = PhoneBookInterface::MovePosition::first;
    bookInter.move(markName, position);
  }
  else if (steps == "last")
  {
    PhoneBookInterface::MovePosition position = PhoneBookInterface::MovePosition::last;
    bookInter.move(markName, position);
  }
  else
  {
    int sign = 1;
    if (steps[0] == '-')
    {
      sign = -1;
      steps.erase(steps.begin());
    }
    else if (steps[0] == '+')
    {
      steps.erase(steps.begin());
    }

    if (!checkNumber(steps))
    {
      std::cout << "<INVALID STEP>\n";
      return;
    }

    bookInter.move(markName, (stoi(steps) * sign));
  }
}
