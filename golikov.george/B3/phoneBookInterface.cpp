#include <iostream>
#include <algorithm>
#include "phoneBookInterface.hpp"

PhoneBookInterface::PhoneBookInterface()
{
  bookmarks_["current"] = phoneBook_.begin();
}

void PhoneBookInterface::add(const PhoneBook::record_t & record)
{
  phoneBook_.pushBack(record);

  if (std::next(phoneBook_.begin()) == phoneBook_.end())
  {
    bookmarks_["current"] = phoneBook_.begin();
  }
};

void PhoneBookInterface::store(const std::string & markName, const std::string & newMarkName)
{
  auto iter = bookmarks_.find(markName);

  if (iter != bookmarks_.end())
  {
    bookmarks_[newMarkName] = iter->second;
    return;
  }
  std::cout << "<INVALID BOOKMARK>\n";
};


void PhoneBookInterface::insert(const InsertPosition & position, const std::string & markName,
                                const PhoneBook::record_t & record)
{
  auto iter = bookmarks_.find(markName);
  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  if (iter->second == phoneBook_.end())
  {
    add(record);
    return;
  }

  switch (position)
  {
    case (InsertPosition::before):
    {
      phoneBook_.insert(iter->second, record);
      break;
    }
    case (InsertPosition::after):
    {
      phoneBook_.insert(std::next(iter->second), record);
      break;
    }
  }
};

void PhoneBookInterface::deleteNote(const std::string & markName)
{
  auto iter = bookmarks_.find(markName);
  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  if (iter != bookmarks_.end())
  {
    auto deleteIter = iter->second;
    for (auto i = bookmarks_.begin(); i != bookmarks_.end(); i++)
    {
      if (i->second == deleteIter)
      {
        if (std::next(i->second) == phoneBook_.end())
        {
          i->second = phoneBook_.previous(deleteIter);
        }
        else
        {
          i->second = phoneBook_.next(deleteIter);
        }
      }
    }

    phoneBook_.remove(deleteIter);
  }
};


void PhoneBookInterface::show(const std::string & markName)
{
  auto iter = bookmarks_.find(markName);
  if (iter != bookmarks_.end())
  {
    if (phoneBook_.empty())
    {
      std::cout << "<EMPTY>\n";
      return;
    }

    return phoneBook_.showTemporaryRecord(iter->second);
  }
  std::cout << "<INVALID BOOKMARK>\n";
};


void PhoneBookInterface::move(const std::string & markName, int steps)
{
  auto iter = bookmarks_.find(markName);
  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  iter->second = phoneBook_.move(iter->second, steps);
};


void PhoneBookInterface::move(const std::string & markName, const MovePosition & movePosition)
{
  auto iter = bookmarks_.find(markName);
  if (iter == bookmarks_.end())
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  switch (movePosition)
  {
    case (MovePosition::first):
    {
      iter->second = phoneBook_.begin();
      break;
    }
    case (MovePosition::last):
    {
      iter->second = phoneBook_.previous(phoneBook_.end());
      break;
    }
  }
};
