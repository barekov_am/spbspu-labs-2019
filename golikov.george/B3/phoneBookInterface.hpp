#ifndef PHONE_BOOK_INTERFACE_HPP
#define PHONE_BOOK_INTERFACE_HPP

#include <map>
#include <string>

#include "phoneBook.hpp"

class PhoneBookInterface
{
public:
  enum class InsertPosition
  {
    before,
    after
  };
  enum class MovePosition
  {
    first,
    last
  };

  PhoneBookInterface();

  void add(const PhoneBook::record_t &);
  void store(const std::string &, const std::string &);
  void insert(const InsertPosition &, const std::string &, const PhoneBook::record_t &);
  void deleteNote(const std::string &);
  void show(const std::string &);
  void move(const std::string &, int);
  void move(const std::string &, const MovePosition &);

private:
  std::map<std::string, PhoneBook::iterator> bookmarks_;
  PhoneBook phoneBook_;
};

#endif
