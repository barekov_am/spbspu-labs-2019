#include <iostream>
#include <sstream>
#include "dataStruct.hpp"
#include "functions.hpp"

void task()
{
  std::vector<DataStruct> vector;
  while (std::cin.peek() != EOF)
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed during reading!\n");
    }

    getData(vector);
  }

  sortVector(vector);
  printVector(vector);
}
