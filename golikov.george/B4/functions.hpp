#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include "dataStruct.hpp"

void getData(std::vector<DataStruct> &);
void sortVector(std::vector<DataStruct> &);
void printVector(std::vector<DataStruct> &);

#endif
