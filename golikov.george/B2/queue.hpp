#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <list>
#include <stdexcept>

template <typename T>
class QueueWithPriority
{
public:
  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  void PutElementToQueue(const T &, ElementPriority);
  T GetElementFromQueue();
  void Accelerate();
  bool empty() const;

private:
  std::list<T> low_;
  std::list<T> normal_;
  std::list<T> high_;
};

template <typename T>
void QueueWithPriority<T>::PutElementToQueue(const T & element, ElementPriority priority)
{
  switch(priority)
  {
    case ElementPriority::LOW:
      low_.push_back(element);
      break;
    case ElementPriority::NORMAL:
      normal_.push_back(element);
      break;
    case ElementPriority::HIGH:
      high_.push_back(element);
      break;
  }
}

template <typename T>
T QueueWithPriority<T>::GetElementFromQueue()
{
  if (!high_.empty())
  {
    T element = high_.front();
    high_.pop_front();
    return element;
  }
  else if (!normal_.empty())
  {
    T element = normal_.front();
    normal_.pop_front();
    return element;
  }
  else if (!low_.empty())
  {
    T element = low_.front();
    low_.pop_front();
    return element;
  }
  else
  {
    throw std::invalid_argument("Queue is empty!\n");
  }
}

template <typename T>
void QueueWithPriority<T>::Accelerate()
{
  high_.splice(high_.end(), low_);
}

template <typename T>
bool QueueWithPriority<T>::empty() const
{
  return high_.empty() && normal_.empty() && low_.empty();
}

#endif
