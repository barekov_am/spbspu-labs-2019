#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <string>

template <typename T>
class QueueWithPriority;

void add(QueueWithPriority<std::string> &, std::stringstream &);
void get(QueueWithPriority<std::string> &, std::stringstream &);
void accelerate(QueueWithPriority<std::string> &, std::stringstream &);

#endif
