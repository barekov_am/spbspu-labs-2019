#include <iostream>
#include <sstream>
#include <map>
#include <functional>
#include <algorithm>

#include "commands.hpp"
#include "queue.hpp"

void task1()
{
  using command = std::function<void (QueueWithPriority<std::string> &, std::stringstream &)>;

  std::map<const std::string, command> commands =
  {
    {"add", add},
    {"get", get},
    {"accelerate", accelerate}
  };

  QueueWithPriority<std::string> queue;
  std::string inputStr;

  while (getline(std::cin, inputStr))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Read error!\n");
    }

    std::stringstream strStream(inputStr);
    std::string readCommand;
    strStream >> std::ws >> readCommand;

    auto key = std::find_if(std::begin(commands), std::end(commands),
                            [&](const std::pair<const std::string, command>& pair)
                               {return (pair.first == readCommand);});

    if (key != std::end(commands))
    {
      key->second(queue, strStream);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
