#include <fstream>
#include <memory>
#include <vector>

#include "print.hpp"

static const size_t INITIAL_SIZE = 64;

void task2(const char * fileName)
{
  std::ifstream file;
  file.open(fileName);

  if (!file.is_open())
  {
    throw std::runtime_error("Error opening the file!\n");
  }
  else
  {
    size_t arraySize = INITIAL_SIZE;
    std::unique_ptr<char[], decltype(&free)> array(static_cast<char *> (calloc(arraySize, 1)), free);

    if (!array)
    {
      file.close();
      throw std::runtime_error("Cannot allocate the memory!\n");
    }

    size_t arrayPointer = 0;

    while (file.read(&array[arrayPointer], 1) && !file.eof())
    {
      ++arrayPointer;
      if (arrayPointer == arraySize)
      {
        arraySize += 64;
        char * tmp = array.release();
        std::unique_ptr<char[], decltype(&free)> newArray(static_cast<char *> (realloc(tmp, arraySize)), free);
        if (!newArray)
        {
          file.close();
          throw std::runtime_error("Cannot allocate the memory!\n");
        }
        std::swap(array, newArray);
      }
      if (!file.eof() && file.fail())
      {
        file.close();
        throw std::runtime_error("Error reading the file!\n");
      }
    }

    file.close();

    if (arrayPointer == 0)
    {
      return;
    }

    std::vector<char> charVector(&array[0], &array[arrayPointer - 1]);
    for (char ch: charVector)
    {
      std::cout << ch;
    }
    std::cout << std::endl;
  }
}
