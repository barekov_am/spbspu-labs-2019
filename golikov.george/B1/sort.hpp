#ifndef SORT_HPP
#define SORT_HPP

#include <functional>
#include <cstring>
#include <stdexcept>

bool getDirection(const char * direction);

template <template <class T> class Strategy, typename T>
void sorting(T & collection, bool direction)
{
  const auto begin = Strategy<T>::begin(collection);
  const auto end = Strategy<T>::end(collection);

  for (auto i = begin; i != end; ++i)
  {
    for (auto j = i; j != end; ++j)
    {
      typename T::reference outer = Strategy<T>::getElement(collection, i);
      typename T::reference inner = Strategy<T>::getElement(collection, j);

      if ((inner < outer) == direction)
      {
        std::swap(inner, outer);
      }
    }
  }
}

#endif
