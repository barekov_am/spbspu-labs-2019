#ifndef TASKS_HPP
#define TASKS_HPP

#include <cstddef>

void task1(const char * direction);
void task2(const char * filename);
void task3();
void task4(const char * direction, std::size_t size);

#endif
