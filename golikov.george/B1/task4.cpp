#include <random>
#include <vector>
#include <ctime>

#include "strategy.hpp"
#include "print.hpp"
#include "sort.hpp"

void fillRandom(double * array, std::size_t size)
{
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-1.0, 1.0);
  for (std::size_t i = 0; i < size; ++i)
  {
    array[i] = dis(gen);
  }
}

void task4(const char * direction, std::size_t size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size must be more than 0!\n");
  }

  bool dir = getDirection(direction);

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);

  print(vector);
  sorting<AccessBrackets>(vector, dir);
  print(vector);
}
