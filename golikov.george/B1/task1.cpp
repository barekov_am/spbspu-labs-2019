#include <vector>
#include <forward_list>

#include "strategy.hpp"
#include "sort.hpp"
#include "print.hpp"

void task1(const char * direction)
{
  bool dir = getDirection(direction);

  std::vector<int> brackets;
  int num = 0;
  while (std::cin && !(std::cin >> num).eof())
  {
    if (std::cin.fail())
    {
      throw std::invalid_argument("Incorrect input data!\n");
    }

    brackets.push_back(num);
  }

  std::vector<int> at = brackets;
  std::forward_list<int> list(brackets.begin(), brackets.end());

  sorting<AccessBrackets>(brackets, dir);
  sorting<AccessAt>(at, dir);
  sorting<AccessIterator>(list, dir);

  print(brackets);
  print(at);
  print(list);
}
