#include <iostream>
#include <string>
#include "tasks.hpp"

int main(int argc, char * argv[])
{
  try
  {
    if ((argc >= 2) && (argc <= 4))
    {
      char * ptr = nullptr;
      const int taskNum = std::strtol(argv[1], &ptr, 10);

      if (*ptr)
      {
        std::cerr << "Invalid task number.";
        return 1;
      }

      switch (taskNum)
      {
        case 1:
        {
          if (argc != 3)
          {
            std::cerr << "Invalid args!\n";
            return 1;
          }
          task1(argv[2]);
          break;
        }
        case 2:
        {
          if (argc != 3)
          {
            std::cerr << "Invalid args!\n";
            return 1;
          }
          task2(argv[2]);
          break;
        }
        case 3:
        {
          if (argc != 2)
          {
            std::cerr << "Invalid args!\n";
            return 1;
          }
          task3();
          break;
        }
        case 4:
        {
          if (argc != 4)
          {
            std::cerr << "Invalid args!\n";
            return 1;
          }
          task4(argv[2], std::stoi(argv[3]));
          break;
        }
        default:
        {
          std::cerr << "Invalid args!\n";
          return 1;
        }
      }
    }
    else
    {
      std::cerr << "Invalid args!\n";
      return 1;
    }
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << '\n';
    return 1;
  }
  return 0;
}
