#include <iostream>
#include <stdexcept>

void task();

int main()
{
  try
  {
    task();
  }

  catch (const std::exception & e)
  {
    std::cerr << e.what() << "\n";
    return 2;
  }

  return 0;
}
