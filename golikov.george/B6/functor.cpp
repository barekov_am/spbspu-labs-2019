#include "functor.hpp"

void Functor::operator()(const int & num)
{
  numbers_.push_back(num);
}

int Functor::getMax() const
{
  return (*std::max_element(numbers_.begin(), numbers_.end()));
}

int Functor::getMin() const
{
  return (*std::min_element(numbers_.begin(), numbers_.end()));
}

double Functor::getAverage() const
{
  double average = 0;
  for (int num : numbers_)
  {
    average += num;
  }
  average = average / numbers_.size();

  return average;
}

int Functor::getNumOfPositive() const
{
  int count = 0;
  for (int i : numbers_)
  {
    if (i > 0)
    {
      ++count;
    }
  }

  return count;
}

int Functor::getNumOfNegative() const
{
  int count = 0;
  for (int i : numbers_)
  {
    if (i < 0)
    {
      ++count;
    }
  }

  return count;
}

long long int Functor::getOddSum() const
{
  long long int oddSum = 0;
  for (int number : numbers_)
  {
    if (number % 2 != 0)
    {
      oddSum += number;
    }
  }

  return oddSum;
}

long long int Functor::getEvenSum() const
{
  long long int evenSum = 0;
  for (int number : numbers_)
  {
    if (number % 2 == 0)
    {
      evenSum += number;
    }
  }

  return evenSum;
}

bool Functor::firstEqLast() const
{
  return numbers_.front() == numbers_.back();
}

bool Functor::empty() const
{
  return numbers_.empty();
}
