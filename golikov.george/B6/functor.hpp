#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <vector>
#include <algorithm>
#include <functional>

class Functor
{
public:
  void operator()(const int & num);

  int getMax() const;
  int getMin() const;
  double getAverage() const;
  int getNumOfPositive() const;
  int getNumOfNegative() const;
  long long int getOddSum() const;
  long long int getEvenSum() const;
  bool firstEqLast() const;

  bool empty() const;
private:
  std::vector<int> numbers_;
};

#endif
