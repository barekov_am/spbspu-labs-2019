#include <iostream>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

void printDataShape(const drozdov::CompositeShape::shape_ptr shape)
{
  if (shape == nullptr) {
    throw std::invalid_argument("\nShape's pointer is null.");
  }
  shape->printData();
  std::cout << "\nArea: " << shape->getArea() << std::endl;

  drozdov::rectangle_t frameRect = shape->getFrameRect();

  std::cout << "***Frame Rectangle";
  std::cout << "\nWidth: " << frameRect.width  << "\tHeight: " << frameRect.height;
  std::cout << "\nPosition: (" << frameRect.pos.x << ';' << frameRect.pos.y << ')' << std::endl;
}

int main()
{
  try {
    drozdov::CompositeShape composite1;
    std::cout << "\nСreated an empty first composition.";
    composite1.printData();

    // start circle demonstration
    drozdov::Circle circle1(5, {6, 7.2});
    composite1.add(circle1);
    printDataShape(composite1[1]);
    const drozdov::point_t position = {-1.8, 2.4};
    composite1[1]->move(position);
    std::cout << "\n===After move to (-1.8, 2.4)===";
    composite1[1]->printData();
    const double coefficientScaleCirc = 4.0;
    composite1[1]->scale(coefficientScaleCirc);
    std::cout << "=====After scale x" << coefficientScaleCirc << "=====";
    std::cout << "\nArea: " << composite1[1]->getArea();
    std::cout << std::endl;
    // end circle

    // start rectangle demonstration
    drozdov::Rectangle rectangle1(7.2, 5.7, {5, -2.4});
    double dx = 2.5, dy = -4.1;
    rectangle1.move(dx, dy);
    std::cout << "\n===After move: + (" << dx << ';' << dy << ")===";
    rectangle1.printData();
    const double coefficientScaleRect = 2.0;
    rectangle1.scale(coefficientScaleRect);
    std::cout << "=====After scale x" << coefficientScaleRect << "=====";
    std::cout << "\nArea: " << rectangle1.getArea();
    std::cout << std::endl;
    // end rectangle
    drozdov::Rectangle rectangle2(3.0, 5.0, {-1.0, -3.0});

    composite1.add(rectangle1);
    composite1.add(rectangle2);

    std::cout << "\nAfter add(circle1), add(rectangle1), add(rectangle2), in first composite:";

    drozdov::CompositeShape composite2(std::move(composite1));
    std::cout << "\nCreated a new composition that took the values from the first composition.";
    std::cout << "\nQuantity of shapes in the first composition: " << composite1.getQuantity();
    std::cout << "\nQuantity of shapes in the second composition: " << composite2.getQuantity();

    std::cout << "\nСopy assignment demonstration";
    composite1 = composite2;
    std::cout << "\nQuantity of shapes in the first composition: " << composite1.getQuantity();
    std::cout << "\nQuantity of shapes in the second composition: " << composite2.getQuantity();

    composite2.remove(2);
    dx = 4.3;
    dy = 10.0;
    composite2.move(dx, dy);
    std::cout << "\nAfter removing the first rectangle and moving the composition +(" << dx << ';' << dy << ')';
    std::cout << "\nAbout second composite";

    drozdov::CompositeShape composite3(composite2);
    std::cout << "\nCreated a new composite that copy the values from the second composition.";
    std::cout << "\nQuantity of shapes in the second composition: " << composite2.getQuantity();
    std::cout << "\nQuantity of shapes in the third composition: " << composite3.getQuantity();

    const double coefficientScale = 2.0;
    composite3.scale(coefficientScale);
    std::cout << "\nАfter scaling the third composition in " << coefficientScale;
    composite3.printData();
    composite3.move({dx, dy});
    std::cout << "\nAfter moving the composition on new position: (" << dx << ';' << dy << ')';
    composite3.printData();
    std::cout << std::endl;
    // end CompositeShape

    return 0;
  } catch (const std::invalid_argument& ex) {
    std::cerr << "\nError invalid-argument: \"" << ex.what() << "\"" << std::endl;

    return 1;
  } catch (const std::out_of_range& ex) {
    std::cerr << "\nError in index out of range: \"" << ex.what() << "\"" << std::endl;

    return 1;
  } catch (const std::logic_error& ex) {
    std::cerr << "\nError in logic of programm: \"" << "\"" << std::endl;

    return 1;
  } catch (const std::bad_alloc& ex) {
    std::cerr << "\nIncorrect use of memory: \"" << ex.what() << "\"" << std::endl;

    return 2;
  } catch (const std::exception& ex) {
    std::cerr << "\nSystem error:  \"" << ex.what() << "\"" << std::endl;

    return 2;
  }
}
