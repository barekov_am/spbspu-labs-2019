#define _USE_MATH_DEFINES
#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>

drozdov::Rectangle::Rectangle(double width, double height, const point_t& pos):
  width_(width),
  height_(height),
  angle_(0.0),
  pos_(pos)
{
  if ((width <= 0.0) || (height <= 0.0)) {
    throw std::invalid_argument("Rectangle parameters are not valid.");
  }
}

void drozdov::Rectangle::printData() const noexcept
{
  std::cout << "\nRectangle";
  std::cout << "\nWidth: " << width_;
  std::cout << "\nHeight: " << height_;
  std::cout << "\nAngle: " << angle_;
  std::cout << "\nPosition: (" << pos_.x << ';' << pos_.y << ')';
}

drozdov::rectangle_t drozdov::Rectangle::getFrameRect() const noexcept
{
  const double cosine = fabs(cos(angle_ * M_PI / 180));
  const double sinus = fabs(sin(angle_ * M_PI / 180));
  const double width = height_ * sinus + width_ * cosine;
  const double height = height_ * cosine + width_ * sinus;

  return {width, height, pos_};
}

void drozdov::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0.0) {
    throw std::invalid_argument("The rectangle scaling coefficient is entered incorrectly.");
  }
  width_ *= coefficient;
  height_ *= coefficient;
}

void drozdov::Rectangle::rotate(double angle) noexcept
{
  angle_ = fmod(angle_ + angle, 360.0);
}

void drozdov::Rectangle::move(double dx, double dy) noexcept
{
  pos_.x += dx;
  pos_.y += dy;
}
