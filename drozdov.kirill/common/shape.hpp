#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include <cmath>
#include "base-types.hpp"

namespace drozdov
{
  class Shape
  {
  public:
    virtual ~Shape() = default;

    virtual void printData() const noexcept = 0;
    virtual point_t getPos() const noexcept = 0;
    virtual double getArea() const noexcept = 0;
    virtual rectangle_t getFrameRect() const noexcept = 0;
    virtual std::shared_ptr<Shape> clone() const = 0;
    virtual void scale(double) = 0;
    virtual void rotate(double) noexcept = 0;
    virtual void move(double, double) noexcept = 0;
    virtual void move(const point_t&) noexcept = 0;
  };
}
#endif // SHAPE_HPP
