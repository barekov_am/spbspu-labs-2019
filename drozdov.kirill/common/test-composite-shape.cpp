#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double ACCURACY = 0.001;
const unsigned int ZERO_QUANTITY = 0;
const double INCORRECT_ARGUMENT = -10.0;

BOOST_AUTO_TEST_SUITE(testsCompositeShape)

  BOOST_AUTO_TEST_CASE(testDefaultConstructor)
  {
    drozdov::CompositeShape emptyComposite;
    BOOST_CHECK_EQUAL(emptyComposite.getQuantity(), ZERO_QUANTITY);
  } // testDefaultConstructor

  BOOST_AUTO_TEST_CASE(testMoveAndCopyConstructors)
  {
    drozdov::Circle circle(2.0, {-1.5, 2.0});
    drozdov::CompositeShape composite1;
    composite1.add(circle);
    unsigned int qtyBefore = composite1.getQuantity();

    drozdov::CompositeShape composite2(composite1);
    BOOST_CHECK_EQUAL(composite1.getQuantity(), qtyBefore);
    BOOST_CHECK_EQUAL(composite1.getQuantity(), composite2.getQuantity());

    qtyBefore = composite2.getQuantity();
    drozdov::CompositeShape composite3(std::move(composite2));
    BOOST_CHECK_EQUAL(composite2.getQuantity(), ZERO_QUANTITY);
    BOOST_CHECK_EQUAL(composite3.getQuantity(), qtyBefore);
  } // testMoveAndCopyConstructors

  BOOST_AUTO_TEST_CASE(testAssigmentOperators)
  {
    drozdov::CompositeShape composite1;
    drozdov::Circle circle(2.0, {-1.5, 2.0});
    composite1.add(circle);
    const int qtyBeforeCopy = composite1.getQuantity();
    drozdov::CompositeShape composite2;

    composite2 = composite1;

    BOOST_CHECK_EQUAL(composite2.getQuantity(), qtyBeforeCopy);
    BOOST_CHECK_EQUAL(composite1.getQuantity(), composite2.getQuantity());

    const int qtyBeforeRemove = composite1.getQuantity();
    composite2 = std::move(composite1);
    BOOST_CHECK_EQUAL(composite1.getQuantity(), ZERO_QUANTITY);
    BOOST_CHECK_EQUAL(composite2.getQuantity(), qtyBeforeRemove);
  } // testAssigmentOperators

  BOOST_AUTO_TEST_CASE(testCorrectnessMove)
  {
    drozdov::CompositeShape composite;
    drozdov::Circle circle(4.0, {2.3, 4.0});
    drozdov::Rectangle rectangle(3.0, 4.0, {2.5, 3.5});
    composite.add(circle);
    composite.add(rectangle);
    const double areaBeforeMove = composite.getArea();
    const drozdov::rectangle_t frameBeforeMove = composite.getFrameRect();

    composite.move(2.4, 5.6);

    const drozdov::rectangle_t frameAfterMoveOn = composite.getFrameRect();
    BOOST_CHECK_CLOSE(areaBeforeMove, composite.getArea(), ACCURACY);
    BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMoveOn.width, ACCURACY);
    BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMoveOn.height, ACCURACY);

    composite.move({2.4, 5.6});

    const drozdov::rectangle_t frameAfterMoveTo = composite.getFrameRect();
    BOOST_CHECK_CLOSE(areaBeforeMove, composite.getArea(), ACCURACY);
    BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMoveTo.width, ACCURACY);
    BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMoveTo.height, ACCURACY);
  } // testCorrectnessMove

  BOOST_AUTO_TEST_CASE(testCorrectnessScale)
  {
    drozdov::CompositeShape composite;
    drozdov::Circle circle(4.0, {2.3, 4.0});
    drozdov::Rectangle rectangle(3.0, 4.0, {2.5, 3.5});
    composite.add(circle);
    composite.add(rectangle);

    const double areaBeforeScale = composite.getArea();
    const double coefficientScale = 3.0;
    composite.scale(coefficientScale);
    const double areaAfterScale = composite.getArea();

    BOOST_CHECK_CLOSE(areaBeforeScale * coefficientScale * coefficientScale, areaAfterScale, ACCURACY);
  } // testCorrectnessScale

  BOOST_AUTO_TEST_CASE(testCorrectnessAfterRotate)
  {
    drozdov::CompositeShape composite;
    drozdov::Circle circle(4.0, {2.3, 4.0});
    drozdov::Rectangle rectangle(3.0, 4.0, {2.5, 3.5});
    composite.add(circle);
    composite.add(rectangle);
    const double areaBeforeRotate = composite.getArea();

    const double angle = 50.0;
    composite.rotate(angle);

    BOOST_CHECK_CLOSE(areaBeforeRotate, composite.getArea(), ACCURACY);
  } // testCorrectnessAfterRotate

  BOOST_AUTO_TEST_CASE(testsCorrectnessThrowMethods)
  {
    drozdov::CompositeShape composite;
    drozdov::Circle circle(4.0, {2.3, 4.0});
    composite.add(circle);
    BOOST_CHECK_THROW(composite[2]->getArea(), std::out_of_range);
    BOOST_CHECK_THROW(composite.remove(2), std::out_of_range);
    BOOST_CHECK_THROW(composite.scale(INCORRECT_ARGUMENT), std::invalid_argument);
  } // testsCorrectnessThrowMethods

BOOST_AUTO_TEST_SUITE_END() // testsCompositeShape
