#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double ACCURACY = 0.001;
const double INCORRECT_ARGUMENT = -10.0;

BOOST_AUTO_TEST_SUITE(testsCircle)

  BOOST_AUTO_TEST_CASE(testCorrectnessAfterMove)
  {
    drozdov::Circle circle(5.0, {4.3, 1.0});
    const double radiusBeforeMove = circle.getRadius();
    const double areaBeforeMove = circle.getArea();

    circle.move(2.4, 5.6);

    BOOST_CHECK_CLOSE(radiusBeforeMove, circle.getRadius(), ACCURACY);
    BOOST_CHECK_CLOSE(areaBeforeMove, circle.getArea(), ACCURACY);

    const drozdov::point_t newPos = {10.0, 10.0};
    circle.move(newPos);

    BOOST_CHECK_CLOSE(radiusBeforeMove, circle.getRadius(), ACCURACY);
    BOOST_CHECK_CLOSE(areaBeforeMove, circle.getArea(), ACCURACY);
  } // testCorrectnessAfterMove

  BOOST_AUTO_TEST_CASE(testCorrectnessAfterScale)
  {
    drozdov::Circle circle(5.0, {4.3, 1.0});
    const double areaBeforeScale = circle.getArea();

    const double coefficientScaleCirc = 4.0;
    circle.scale(coefficientScaleCirc);

    const double areaAfterScale = circle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale * coefficientScaleCirc * coefficientScaleCirc, areaAfterScale, ACCURACY);
  } // testCorrectnessAfterScale

  BOOST_AUTO_TEST_CASE(testCorrectnessAfterRotate)
  {
    drozdov::Circle circle(5.0, {4.3, 1.0});
    const double radiusBeforeMove = circle.getRadius();
    const double areaBeforeMove = circle.getArea();

    const double angle = 40.0;
    circle.rotate(angle);

    BOOST_CHECK_CLOSE(radiusBeforeMove, circle.getRadius(), ACCURACY);
    BOOST_CHECK_CLOSE(areaBeforeMove, circle.getArea(), ACCURACY);
  } // testCorrectnessAfterRotate

  BOOST_AUTO_TEST_CASE(testIncorrectParameters)
  {
    BOOST_CHECK_THROW(drozdov::Circle(INCORRECT_ARGUMENT, {4.3, 1.0}), std::invalid_argument);

    drozdov::Circle circle(5.0, {4.3, 1.0});
    BOOST_CHECK_THROW(circle.scale(INCORRECT_ARGUMENT), std::invalid_argument);
  } // testIncorrectParameters

BOOST_AUTO_TEST_SUITE_END() // testsCircle
