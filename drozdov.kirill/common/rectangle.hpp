#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace drozdov
{
  class Rectangle: public Shape
  {
  public:
    Rectangle(double, double, const point_t&);

    void printData() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void scale(double) override;
    void rotate(double) noexcept override;
    void move(double, double) noexcept override;

    void move(const point_t&newPos) noexcept override
    {
      pos_ = newPos;
    }
    double getArea() const noexcept override
    {
      return width_ * height_;
    }
    point_t getPos() const noexcept
    {
      return pos_;
    }
    std::shared_ptr<Shape> clone() const
    {
      return std::make_shared<Rectangle>(*this);
    }

  private:
    double width_;
    double height_;
    double angle_;
    point_t pos_;
  };
}
#endif // RECTANGLE_HPP
