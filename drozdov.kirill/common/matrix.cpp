#include "matrix.hpp"
#include <stdexcept>
#include <utility>

drozdov::Matrix::Matrix() :
  rows_(0),
  columns_(0)
{ }

drozdov::Matrix::Matrix(const Matrix& rhs) :
  rows_(rhs.rows_),
  columns_(rhs.columns_),
  shapes_(std::make_unique<shape_ptr[]>(rhs.rows_ * rhs.columns_))
{
  for (size_t i = 0; i < (rows_ * columns_); i++) {
    shapes_[i] = rhs.shapes_[i];
  }
}

drozdov::Matrix::Matrix(Matrix&& rhs) noexcept:
  rows_(rhs.rows_),
  columns_(rhs.columns_),
  shapes_(std::move(rhs.shapes_))
{ }

drozdov::Matrix& drozdov::Matrix::operator =(const Matrix& rhs)
{
  if (this != &rhs)
  {
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
    shape_array tmpList(std::make_unique<shape_ptr[]>(rhs.rows_ * rhs.columns_));
    for (size_t i = 0; i < (rows_ * columns_); i++) {
      tmpList[i] = rhs.shapes_[i];
    }
    shapes_.swap(tmpList);
  }
  return *this;
}

drozdov::Matrix& drozdov::Matrix::operator =(Matrix&& rhs) noexcept
{
  if (this != &rhs) {
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
    shapes_ = std::move(rhs.shapes_);
  }
  return *this;
}

drozdov::Matrix::shape_array drozdov::Matrix::operator [](size_t rhs) const
{
  if (rhs >= rows_) {
    throw std::out_of_range("Index is out of range");
  }

  shape_array tmpList(std::make_unique<shape_ptr[]>(columns_));
  for (size_t i = 0; i < columns_; i++) {
    tmpList[i] = shapes_[rhs * columns_ + i];
  }

  return tmpList;
}

void drozdov::Matrix::add(shape_ptr shape, size_t row, size_t column)
{
  size_t tmpRows = (row == rows_) ? (rows_ + 1) : (rows_);
  size_t tmpColumns = (column == columns_) ? (columns_ + 1) : (columns_);

  shape_array tmpList(std::make_unique<shape_ptr[]>(tmpRows * tmpColumns));

  for (size_t i = 0; i < tmpRows; i++) {
    for (size_t j = 0; j < tmpColumns; j++) {
      if ((i == rows_) || (j == columns_)) {
        tmpList[i * tmpColumns + j] = nullptr;
      } else {
        tmpList[i * tmpColumns + j] = shapes_[i * columns_ + j];
      }
    }
  }
  tmpList[row * tmpColumns + column] = shape;
  shapes_.swap(tmpList);
  rows_ = tmpRows;
  columns_ = tmpColumns;
}
