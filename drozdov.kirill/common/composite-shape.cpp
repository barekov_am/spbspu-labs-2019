#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>

drozdov::CompositeShape::CompositeShape():
  qty_(0),
  shapes_(nullptr)
{}

drozdov::CompositeShape::CompositeShape(const CompositeShape& rhs):
  qty_(rhs.qty_),
  shapes_(std::make_unique<shape_ptr[]>(qty_))
{
  for (size_t index = 0; index < qty_; index++) {
    shapes_[index] = rhs.shapes_[index];
  }
}

drozdov::CompositeShape::CompositeShape(CompositeShape&& rhs) noexcept:
  qty_(rhs.qty_),
  shapes_(std::move(rhs.shapes_))
{
  rhs.qty_ = 0;
}

const drozdov::CompositeShape& drozdov::CompositeShape::operator =(const CompositeShape& rhs)
{
  if (&rhs != this) {
    qty_ = rhs.qty_;
    shapes_ = std::make_unique<shape_ptr[]>(qty_);
    for (size_t index = 0; index < qty_; index++) {
      shapes_[index] = rhs.shapes_[index];
    }
  }
  return *this;
}

const drozdov::CompositeShape& drozdov::CompositeShape::operator =(CompositeShape&& rhs) noexcept
{
  if (&rhs != this) {
    qty_ = rhs.qty_;
    shapes_ = std::move(rhs.shapes_);

    rhs.qty_ = 0;
  }
  return *this;
}

drozdov::CompositeShape::shape_ptr drozdov::CompositeShape::operator [](size_t index) const
{
  if (index <= 0 || index > qty_) {
    throw std::out_of_range("Item Access Index is invalid.");
  }
  return shapes_[index - 1];
}

void drozdov::CompositeShape::printData() const noexcept
{
  if (qty_ != 0) {
    const point_t pos = getFrameRect().pos;
    std::cout << "\nComposite-Shape"
              << "\nPosition: (" << pos.x << ';' << pos.y << ')'
              << "\nQuantity of shapes: " << qty_
              << "\nContent";
    for (size_t index = 0; index < qty_; index++) {
      std::cout << "\n* * * * *"
                << "\nShape - №" << index + 1;
      shapes_[index]->printData();
    }
    std::cout << "\n* * * * *";
  } else {
    std::cout << "\nComposite-Shape is empty.";
  }
}

double drozdov::CompositeShape::getArea() const noexcept
{
  double area = 0.0;
  for (size_t index = 0; index < qty_; index++) {
    area += shapes_[index]->getArea();
  }
  return area;
}

drozdov::rectangle_t drozdov::CompositeShape::getFrameRect() const noexcept
{
  if (qty_ <= 0) {
    return {0.0, 0.0, {0.0, 0.0}};
  }

  rectangle_t frameShape = shapes_[0]->getFrameRect();

  point_t min = {frameShape.pos.x - frameShape.width / 2, frameShape.pos.y - frameShape.height / 2};
  point_t max = {frameShape.pos.x + frameShape.width / 2, frameShape.pos.y + frameShape.height / 2};

  for (size_t index = 1; index < qty_; index++) {
    frameShape = shapes_[index]->getFrameRect();
    min.x = std::min((frameShape.pos.x - frameShape.width / 2), min.x);
    min.y = std::min((frameShape.pos.y - frameShape.height / 2), min.y);

    max.x = std::max((frameShape.pos.x + frameShape.width / 2), max.x);
    max.y = std::max((frameShape.pos.y + frameShape.height / 2), max.y);
  }
  const double width = max.x - min.x;
  const double height = max.y - min.y;
  const point_t pos = {min.x + width / 2, min.y + height / 2};

  return {width, height, pos};
}

void drozdov::CompositeShape::scale(double coefficient)
{
  if (coefficient <= 0.0) {
    throw std::invalid_argument("The Composite Shape scaling coefficient is entered incorrectly.");
  }

  const point_t compositePos = getFrameRect().pos;
  for (size_t index = 0; index < qty_; index++) {
    const point_t shapePos = shapes_[index]->getFrameRect().pos;
    double dx = (coefficient - 1) * (shapePos.x - compositePos.x);
    double dy = (coefficient - 1) * (shapePos.y - compositePos.y);

    shapes_[index]->move(dx, dy);
    shapes_[index]->scale(coefficient);
  }
}

void drozdov::CompositeShape::rotate(double angle) noexcept
{
  const rectangle_t frameRect = getFrameRect();

  const double angleRad = M_PI * angle / 180;
  for (size_t index = 0; index < qty_; index++) {
    point_t delta = shapes_[index]->getFrameRect().pos;
    delta.x -= frameRect.pos.x;
    delta.y -= frameRect.pos.y;
    double dx = delta.x * (cos(angleRad) - 1) - delta.y * sin(angleRad);
    double dy = delta.y * (cos(angleRad) - 1) + delta.x * sin(angleRad);

    shapes_[index]->rotate(angle);
    shapes_[index]->move(dx, dy);
  }
}

void drozdov::CompositeShape::move(double dx, double dy) noexcept
{
  for (size_t index = 0; index < qty_; index++) {
    shapes_[index]->move(dx, dy);
  }
}

void drozdov::CompositeShape::move(const point_t& newPos) noexcept
{
  const point_t posComposite = getFrameRect().pos;
  move(newPos.x - posComposite.x, newPos.y - posComposite.y);
}

void drozdov::CompositeShape::add(const Shape& newShape)
{
  shape_array tmpArr = std::make_unique<shape_ptr[]>(qty_ + 1);
  for (size_t index = 0; index < qty_; index++) {
    tmpArr[index] = shapes_[index];
  }
  shapes_.swap(tmpArr);
  shapes_[qty_++] = newShape.clone();
}

void drozdov::CompositeShape::remove(size_t indexRem)
{
  if (indexRem <= 0 || indexRem > qty_) {
    throw std::out_of_range("When deleting, the index on the shape is incorrect.");
  }

  for (size_t index = indexRem; index < qty_ - 1; index++) {
    std::swap(shapes_[index], shapes_[index + 1]);
  }
  shapes_[--qty_].reset();
}

drozdov::CompositeShape::shape_array drozdov::CompositeShape::getList() const noexcept
{
  shape_array tmpArray(std::make_unique<shape_ptr[]>(qty_));
  for (size_t index = 0; index < qty_; index++) {
    tmpArray[index] = shapes_[index];
  }

  return tmpArray;
}
