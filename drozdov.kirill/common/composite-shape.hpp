#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace drozdov
{
  class CompositeShape: public Shape
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    using shape_array = std::unique_ptr<shape_ptr[]>;

    CompositeShape();
    CompositeShape(const CompositeShape&);
    CompositeShape(CompositeShape&&) noexcept;
    ~CompositeShape() = default;

    const CompositeShape& operator =(const CompositeShape&);
    const CompositeShape& operator =(CompositeShape&&) noexcept;
    shape_ptr operator [](size_t) const;

    void printData() const noexcept override;
    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void scale(double) override;
    void rotate(double) noexcept override;
    void move(double, double) noexcept override;
    void move(const point_t&) noexcept override;

    void add(const Shape&);
    void remove(size_t);
    shape_array getList() const noexcept;
    size_t getQuantity() const noexcept
    {
      return qty_;
    }
    point_t getPos() const noexcept
    {
      return getFrameRect().pos;
    }
    shape_ptr clone() const
    {
      return std::make_shared<CompositeShape>(*this);
    }

  private:
    size_t qty_;
    shape_array shapes_;
  };
}
#endif // COMPOSITE_SHAPE_HPP
