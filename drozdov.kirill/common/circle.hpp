#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace drozdov
{
  class Circle: public Shape
  {
  public:
    Circle(double, const point_t&);

    void printData() const noexcept override;
    double getArea() const noexcept override;
    void scale(double) override;
    void move(double, double) noexcept override;

    void rotate(double) noexcept override
    {}
    void move(const point_t& newPos) noexcept override
    {
      pos_ = newPos;
    }
    rectangle_t getFrameRect() const noexcept override
    {
      return {radius_ * 2, radius_ * 2, pos_};
    }
    point_t getPos() const noexcept
    {
      return pos_;
    }
    double getRadius() const noexcept
    {
      return radius_;
    }
    std::shared_ptr<Shape> clone() const
    {
      return std::make_shared<Circle>(*this);
    }

  private:
    double radius_;
    point_t pos_;
  };
}
#endif // CIRCLE_HPP
