#define _USE_MATH_DEFINES
#include "circle.hpp"
#include <iostream>
#include <stdexcept>

drozdov::Circle::Circle(double radius, const point_t& pos):
  radius_(radius),
  pos_(pos)
{
  if (radius <= 0.0) {
    throw std::invalid_argument("Circle's parameter are not valid.");
  }
}

void drozdov::Circle::printData() const noexcept
{
  std::cout << "\nCircle"
            << "\nRadius: " << radius_
            << "\nPosition: (" << pos_.x << ';' << pos_.y << ')';
}

double drozdov::Circle::getArea() const noexcept
{
  return pow(radius_, 2) * M_PI;
}

void drozdov::Circle::scale(double coefficient)
{
  if (coefficient <= 0.0) {
    throw std::invalid_argument("The circle scaling coefficient is entered incorrectly.");
  }
  radius_ *= coefficient;
}

void drozdov::Circle::move(double dx, double dy) noexcept
{
  pos_.x += dx;
  pos_.y += dy;
}
