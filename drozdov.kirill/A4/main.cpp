#include <iostream>
#include <memory>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

void printDataShape(const shape_ptr shape)
{
  if (shape == nullptr) {
    throw std::invalid_argument("\nShape's pointer is null.");
  }
  shape->printData();
  std::cout << "\nArea: " << shape->getArea() << std::endl;

  drozdov::rectangle_t frameRect = shape->getFrameRect();

  std::cout << "***Frame Rectangle";
  std::cout << "\nWidth: " << frameRect.width  << "\tHeight: " << frameRect.height;
  std::cout << "\nPosition: (" << frameRect.pos.x << ';' << frameRect.pos.y << ')' << std::endl;
}

void printDataMatrix(const drozdov::Matrix& matrix)
{
  std::cout << "\n\t";
  for (size_t index = 0; index < matrix.getColumns(); index++) {
    std::cout << index + 1 << "\t";
  }
  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    std::cout << "\n" << i + 1 << "\t";
    for (size_t j = 0; j < matrix.getColumns(); j++)
    {
      if (matrix[i][j] != nullptr)
      {
        std::cout << "*       ";
      } else {
        std::cout << "\t";
      }
    }
  }
  std::cout << "\n";
}

int main()
{
  drozdov::CompositeShape composite1;
  std::cout << "\nСreated an empty first composition.";
  composite1.printData();

  // start circle demonstration
  drozdov::Circle circle1(5, {6, 7.2});
  composite1.add(circle1);
  printDataShape(composite1[1]);
  const drozdov::point_t position = {-1.8, 2.4};
  composite1[1]->move(position);
  std::cout << "\n===After move to (-1.8, 2.4)===";
  composite1[1]->rotate(20);
  composite1[1]->printData();
  const double coefficientScaleCirc = 4.0;
  composite1[1]->scale(coefficientScaleCirc);
  std::cout << "=====After scale x" << coefficientScaleCirc << "=====";
  std::cout << "\nArea: " << composite1[1]->getArea();
  std::cout << std::endl;
  // end circle

  // start rectangle demonstration
  drozdov::Rectangle rectangle1(7.2, 5.7, {5, -17.2});
  composite1.add(rectangle1);
  printDataShape(composite1[2]);
  double dx = 2.5, dy = -4.1;
  composite1[2]->move(dx, dy);
  std::cout << "\n===After move: + (" << dx << ';' << dy << ")===";
  composite1[2]->rotate(40);
  composite1[2]->printData();
  const double coefficientScaleRect = 2.0;
  composite1[2]->scale(coefficientScaleRect);
  std::cout << "=====After scale x" << coefficientScaleRect << "=====";
  std::cout << "\nArea: " << composite1[2]->getArea();
  std::cout << std::endl;
  // end rectangle
  drozdov::Rectangle rectangle2(3.0, 5.0, {-1.0, -3.0});

  composite1.add(rectangle2);

  std::cout << "\nMatrix:";
  drozdov::Matrix matrix = drozdov::part(composite1);
  printDataMatrix(matrix);

  return 0;
}
