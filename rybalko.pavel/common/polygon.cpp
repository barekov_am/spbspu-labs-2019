#include "polygon.hpp"

#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <cmath>

rybalko::Polygon::Polygon() :
  quantityOfVertexes_(0),
  center_({ 0.0, 0.0 }),
  vertex_(nullptr)
{
}

rybalko::Polygon::Polygon(const Polygon & ptr) :
  quantityOfVertexes_(ptr.quantityOfVertexes_),
  center_(ptr.center_),
  vertex_(new rybalko::point_t[quantityOfVertexes_])
{
  for (std::size_t index = 0; index < quantityOfVertexes_; index++)
  {
    vertex_[index] = ptr.vertex_[index];
  }
}

rybalko::Polygon::Polygon(Polygon && ptr) :
  quantityOfVertexes_(ptr.quantityOfVertexes_),
  center_(ptr.center_),
  vertex_(ptr.vertex_)
{
  if (this != &ptr)
  {
    ptr.quantityOfVertexes_ = 0;
    ptr.center_ = { 0.0, 0.0 };
    ptr.vertex_ = nullptr;
  }
}

rybalko::Polygon::Polygon(std::size_t qVertexes, rybalko::point_t * vertexes) :
  quantityOfVertexes_(qVertexes),
  vertex_(new point_t [quantityOfVertexes_])
{
  if (quantityOfVertexes_ <= 2)
  {
    throw std::invalid_argument("invalid number for Polygon");
  }

  if (vertexes == nullptr)
  {
    throw std::invalid_argument("pointer mustn't be null");
  }
  
  for (std::size_t index = 0; index < quantityOfVertexes_; index++)
  {
    vertex_[index] = vertexes[index];
  }
  
  center_ = findCen();
  
  const double polyArea = getArea();
  if (polyArea <= 0.0)
  {
    throw std::invalid_argument("invalid area's value");
  }
  
  if (check())
  {
    throw std::invalid_argument("polygon must be convex");
  }
}

rybalko::Polygon::~Polygon()
{
  delete[] vertex_;
}

rybalko::Polygon & rybalko::Polygon::operator =(const Polygon & ptr)
{
  if (this == &ptr)
  {
    return *this;
  }

  quantityOfVertexes_ = ptr.quantityOfVertexes_;
  center_ = ptr.center_;
  vertex_ = new point_t[quantityOfVertexes_];
  
  for (std::size_t index = 0; index < quantityOfVertexes_; index++)
  {
    vertex_[index] = ptr.vertex_[index];
  }
  return *this;
}


rybalko::Polygon & rybalko::Polygon::operator =(Polygon && ptr)
{
  if (this == &ptr)
  {
    return *this;
  }
  quantityOfVertexes_ = ptr.quantityOfVertexes_;
  center_ = ptr.center_;

  ptr.quantityOfVertexes_ = 0;
  ptr.center_ = { 0.0, 0.0 };

  if (vertex_ != nullptr)
  {
    delete[] vertex_;
  }

  vertex_ = ptr.vertex_;
  ptr.vertex_ = nullptr;
  return *this;
}

rybalko::point_t rybalko::Polygon::findCen() const
{
  rybalko::point_t tp = { 0.0, 0.0 };
  for (std::size_t index = 0; index < quantityOfVertexes_; index++)
  {
    tp.x += vertex_[index].x;
    tp.y += vertex_[index].y;
  }
  return { tp.x / quantityOfVertexes_, tp.y / quantityOfVertexes_ };
}

std::size_t rybalko::Polygon::getQ() const
{
  return quantityOfVertexes_;
}

void rybalko::Polygon::print() const
{
  for (std::size_t index = 0; index < quantityOfVertexes_; index++)
  {
    std::cout << "Polygon's vertex #" << index + 1 << std::endl;
    std::cout << "X = " << vertex_[index].x << " ; " << " Y = " << vertex_[index].y << std::endl;
  }
  std::cout << "Area of polygon: " << getArea() << std::endl;
  std::cout << "Width of frame rectangle: " << getFrameRect().width << std::endl;
  std::cout << "Height of frame rectangle: " << getFrameRect().height << std::endl;
  std::cout << "Center point of polygon: (" << findCen().x 
      << "; " << findCen().y << ")";
}

double rybalko::Polygon::getArea() const
{
  double s = 0.0;
  
  for (std::size_t index = 0; index < quantityOfVertexes_ - 1; index++)
  {
    s += vertex_[index].x * vertex_[index + 1].y;
    s -= vertex_[index + 1].x * vertex_[index].y;
  }
  s += vertex_[quantityOfVertexes_ - 1].x * vertex_[0].y;
  s -= vertex_[0].x * vertex_[quantityOfVertexes_ - 1].y;

  return (0.5 * std::fabs(s));
}

rybalko::rectangle_t rybalko::Polygon::getFrameRect() const
{
  double maxX = vertex_[0].x;
  double minX = vertex_[0].x;
  double maxY = vertex_[0].y;
  double minY = vertex_[0].y;

  for (std::size_t index = 1; index < quantityOfVertexes_; index++)
  {
    maxX = std::max(maxX, vertex_[index].x);
    minX = std::min(minX, vertex_[index].x);
    maxY = std::max(maxY, vertex_[index].y);
    minY = std::min(minY, vertex_[index].y);
  }

  const double width = (maxX - minX);
  const double height = (maxY - minY);
  const point_t posFrameRect = { minX + (width / 2), minY + (height / 2) };

  return { width, height, posFrameRect };
}

void rybalko::Polygon::scale(double k)
{
  if (k <= 0.0)
  {
    throw std::invalid_argument("invalid value for 'k'");
  }
  else
  {
    for (std::size_t index = 0; index < quantityOfVertexes_; index++)
    {
      vertex_[index].x = center_.x + (vertex_[index].x - center_.x) * k;
      vertex_[index].y = center_.y + (vertex_[index].y - center_.y) * k;
    }
  }
}

bool rybalko::Polygon::check() const
{
  point_t one = { 0.0, 0.0 };
  point_t two = { 0.0, 0.0 };

  for (std::size_t index = 0; index <= quantityOfVertexes_ - 2; index++)
  {
    if (index == quantityOfVertexes_ - 2)
    {
      one.x = vertex_[index + 1].x - vertex_[index].x;
      one.y = vertex_[index + 1].y - vertex_[index].y;
      two.x = vertex_[0].x - vertex_[index + 1].x;
      two.y = vertex_[0].y - vertex_[index + 1].y;
    }
    else
    {
      one.x = vertex_[index + 1].x - vertex_[index].x;
      one.y = vertex_[index + 1].y - vertex_[index].y;
      two.x = vertex_[index + 2].x - vertex_[index + 1].x;
      two.y = vertex_[index + 2].y - vertex_[index + 1].y;
    }
    if (((one.x * two.y) - (one.y * two.x)) < 0.0)
    {
      return true;
    }
  }
  return false;
}

void rybalko::Polygon::move(double dx, double dy)
{
  for (std::size_t index = 0; index < quantityOfVertexes_; index++)
  {
    vertex_[index].x += dx;
    vertex_[index].y += dy;
  }
  center_.x += dx;
  center_.y += dy;
}

void rybalko::Polygon::move(const point_t & point)
{
  const point_t diff = { point.x - center_.x, point.y - center_.y };
  move(diff.x, diff.y);
}
