#include "rectangle.hpp"

#include <iostream>
#include <cmath>
#include <cassert>

rybalko::Rectangle::Rectangle(const rybalko::rectangle_t & rect) :
  rect_(rect)
{
  if (rect_.width <= 0.0)
  {
    throw std::invalid_argument("invalid value for rectangle's width");
  }
  if (rect_.height <= 0.0)
  {
    throw std::invalid_argument("invalid value for rectangle's height");
  }
}

double rybalko::Rectangle::getArea() const
{
  return (rect_.width * rect_.height);
}

rybalko::rectangle_t rybalko::Rectangle::getFrameRect() const
{
  return rect_;
}

void rybalko::Rectangle::move(const rybalko::point_t & point)
{
  rect_.pos = point;
}

void rybalko::Rectangle::move(const double dx, const double dy)
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

void rybalko::Rectangle::print() const
{
  std::cout << "Area of rectangle: " << getArea() << std::endl;
  std::cout << "Width of frame rectangle: " << rect_.width << std::endl;
  std::cout << "Height of frame rectangle: " << rect_.height << std::endl;
  std::cout << "Center point of frame rectangle: (" << rect_.pos.x
      << "; " << rect_.pos.y << ")" << std::endl;
}

void rybalko::Rectangle::scale(double k)
{
  if (k <= 0.0)
  {
    throw std::invalid_argument("Coefficient of scale must be a positive number");
  }
  else
  {
    rect_.width *= k;
    rect_.height *= k;
  }    
}
