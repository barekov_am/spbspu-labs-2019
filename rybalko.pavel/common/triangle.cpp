#include "triangle.hpp"

#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

rybalko::Triangle::Triangle(const rybalko::point_t & vertexA, const rybalko::point_t & vertexB, const rybalko::point_t & vertexC) :
  pos_({ (vertexA.x + vertexB.x + vertexC.x) / 3, (vertexA.y + vertexB.y + vertexC.y) / 3 }),
  vertexA_(vertexA),
  vertexB_(vertexB),
  vertexC_(vertexC)
{
  const double areaTriangle = getArea();
  if (areaTriangle == 0.0)
  {
    throw std::invalid_argument("invalid value for triangle's points");
  }
}

double rybalko::Triangle::getArea() const
{
  return (std::fabs(vertexA_.x - vertexC_.x) * (vertexB_.y - vertexC_.y) 
      - std::fabs(vertexB_.x - vertexC_.x)
        * (vertexA_.y - vertexC_.y)) / 2;
}

rybalko::rectangle_t rybalko::Triangle::getFrameRect() const
{
  const double maxX = std::max(std::max(vertexA_.x, vertexB_.x), vertexC_.x);
  const double minX = std::min(std::min(vertexA_.x, vertexB_.x), vertexC_.x);
  const double maxY = std::max(std::max(vertexA_.y, vertexB_.y), vertexC_.y);
  const double minY = std::min(std::min(vertexA_.y, vertexB_.y), vertexC_.y);
  const double width = maxX - minX;
  const double height = maxY - minY;
  const point_t position = { minX + width / 2, minY + height / 2 };

  return { width, height, position };
}

void rybalko::Triangle::print() const
{
  std::cout << "Area of triangle: " << getArea();
  std::cout << "\nWidth of frame rectangle: " << getFrameRect().width;
  std::cout << "\nHeight of frame rectangle: " << getFrameRect().height;
  std::cout << "\nTriangle's A : (" << vertexA_.x << ';' << vertexA_.y << ')';
  std::cout << "\nTriangle's B : (" << vertexB_.x << ';' << vertexB_.y << ')';
  std::cout << "\nTriangle's C : (" << vertexC_.x << ';' << vertexC_.y << ')';
  std::cout << "\nTriangle's center : (" << pos_.x << ';' << pos_.y << ')';
}

void rybalko::Triangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;

  vertexA_.x += dx;
  vertexA_.y += dy;
  vertexB_.x += dx;
  vertexB_.y += dy;
  vertexC_.x += dx;
  vertexC_.y += dy;
}

void rybalko::Triangle::move(const rybalko::point_t & nPos)
{
  const point_t diff = { nPos.x - pos_.x, nPos.y - pos_.y };
  move(diff.x, diff.y);
}

void rybalko::Triangle::scale(double k)
{
  if (k <= 0.0)
  {
    throw std::invalid_argument("invalid value for 'k'");
  }
  else
  {
    vertexA_.x *= k;
    vertexA_.y *= k;
    vertexB_.x *= k;
    vertexB_.y *= k;
    vertexC_.x *= k;
    vertexC_.y *= k;
    pos_.x *= k;
    pos_.y *= k;
  }
}
