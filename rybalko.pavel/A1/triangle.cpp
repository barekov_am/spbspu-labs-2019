#include "triangle.hpp"

#include <iostream>
#include <cmath>
#include <algorithm>

Triangle::Triangle(const point_t & vertexA, const point_t & vertexB, const point_t & vertexC) :
  pos_({ (vertexA.x + vertexB.x + vertexC.x) / 3, (vertexA.y + vertexB.y + vertexC.y) / 3 }),
  vertexA_(vertexA),
  vertexB_(vertexB),
  vertexC_(vertexC)
{
  if (getArea() == 0)
  {
    throw std::invalid_argument("Points of the triangle must be different!");
  }
}

double Triangle::getArea() const
{
  return (std::fabs(vertexA_.x - vertexC_.x) * (vertexB_.y - vertexC_.y)
      - std::fabs(vertexB_.x - vertexC_.x) * (vertexA_.y - vertexC_.y)) / 2;
}

rectangle_t Triangle::getFrameRect() const
{
  const double maxX = std::max(std::max(vertexA_.x, vertexB_.x), vertexC_.x);
  const double minX = std::min(std::min(vertexA_.x, vertexB_.x), vertexC_.x);
  const double maxY = std::max(std::max(vertexA_.y, vertexB_.y), vertexC_.y);
  const double minY = std::min(std::min(vertexA_.y, vertexB_.y), vertexC_.y);
  const double width = maxX - minX;
  const double height = maxY - minY;
  const point_t position = { minX + width / 2, minY + height / 2 };
  return { width, height, position };
}

void Triangle::print() const
{
  std::cout << "Area of triangle: " << getArea() << std::endl;
  std::cout << "Width of frame rectangle: " << getFrameRect().width
    << std::endl;
  std::cout << "Height of frame rectangle: " << getFrameRect().height
    << std::endl;
  std::cout << "Triangle's A : (" << vertexA_.x << ';' << vertexA_.y << ')'
    << std::endl;
  std::cout << "Triangle's B : (" << vertexB_.x << ';' << vertexB_.y << ')'
    << std::endl;
  std::cout << "Triangle's C : (" << vertexC_.x << ';' << vertexC_.y << ')'
    << std::endl;
  std::cout << "Triangle's center : (" << pos_.x << ';' << pos_.y << ')'
    << std::endl;
}

void Triangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
  vertexA_.x += dx;
  vertexA_.y += dy;
  vertexB_.x += dx;
  vertexB_.y += dy;
  vertexC_.x += dx;
  vertexC_.y += dy;
}

void Triangle::move(const point_t & nPos)
{
  const point_t diff = { nPos.x - pos_.x, nPos.y - pos_.y };
  move(diff.x, diff.y);
}
