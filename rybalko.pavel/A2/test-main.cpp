#define BOOST_TEST_MODULE A2
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <cmath>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

const double EPS = 0.001;

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(firstMove)
{
  rybalko::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  const double oldWidth = rectangle.getFrameRect().width;
  const double oldHeight = rectangle.getFrameRect().height;
  const double oldArea = rectangle.getArea();
  rectangle.move({ 5.0, 5.0 });
  BOOST_CHECK_CLOSE(oldWidth, rectangle.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(oldHeight, rectangle.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(oldArea, rectangle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(secondMove)
{
  rybalko::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  const double oldWidth = rectangle.getFrameRect().width;
  const double oldHeight = rectangle.getFrameRect().height;
  const double oldArea = rectangle.getArea();
  rectangle.move(5.0, 5.0);
  BOOST_CHECK_CLOSE(oldWidth, rectangle.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(oldHeight, rectangle.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(oldArea, rectangle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(scale)
{
  rybalko::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  double oldWidth = rectangle.getFrameRect().width;
  double oldHeight = rectangle.getFrameRect().height;
  double oldArea = rectangle.getArea();
  double k = 9;
  rectangle.scale(9);
  BOOST_CHECK_CLOSE(oldWidth * k, rectangle.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(oldHeight * k, rectangle.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(oldArea * k * k, rectangle.getArea(), EPS);
  oldWidth = rectangle.getFrameRect().width;
  oldHeight = rectangle.getFrameRect().height;
  oldArea = rectangle.getArea();
  k = 0.5;
  rectangle.scale(0.5);
  BOOST_CHECK_CLOSE(oldWidth * k, rectangle.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(oldHeight * k, rectangle.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(oldArea * k * k, rectangle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(errWidth)
{
  BOOST_CHECK_THROW(rybalko::Rectangle rectangle({ -3.0, 3.0, {1.0, 1.0} }), std::invalid_argument);
  BOOST_CHECK_THROW(rybalko::Rectangle rectangle({ 0, 3.0, {1.0, 1.0} }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(errHeihgt)
{
  BOOST_CHECK_THROW(rybalko::Rectangle rectangle({ 3.0, -3.0, {1.0, 1.0} }), std::invalid_argument);
  BOOST_CHECK_THROW(rybalko::Rectangle rectangle({ 3.0, 0, {1.0, 1.0} }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(errCoeff)
{
  rybalko::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  BOOST_CHECK_THROW(rectangle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(rectangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(circleTest)

BOOST_AUTO_TEST_CASE(firstMove)
{
  rybalko::Circle circle(5.0, { 3.0, 3.0 });
  const double oldRadius = 5.0;
  const double oldArea = circle.getArea();
  circle.move({ 5.0, 5.0 });
  BOOST_CHECK_CLOSE(oldRadius, 5.0, EPS);
  BOOST_CHECK_CLOSE(oldArea, circle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(secondMove)
{
  rybalko::Circle circle(6.0, { 3.0, 3.0 });
  const double oldRadius = 6.0;
  const double oldArea = circle.getArea();
  circle.move(5.0, 5.0);
  BOOST_CHECK_CLOSE(oldRadius, 6.0, EPS);
  BOOST_CHECK_CLOSE(oldArea, circle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(scale)
{
  rybalko::Circle circle(6.0, { 3.0, 3.0 });
  double oldRadius = 6.0;
  double oldArea = circle.getArea();
  double k = 9;
  circle.scale(9);
  BOOST_CHECK_CLOSE(oldRadius * k, 6.0 * k, EPS);
  BOOST_CHECK_CLOSE(oldArea * k * k, circle.getArea(), EPS);
  oldRadius = 54.0;
  oldArea = circle.getArea();
  k = 0.5;
  circle.scale(0.5);
  BOOST_CHECK_CLOSE(oldRadius * k, 54.0 * k, EPS);
  BOOST_CHECK_CLOSE(oldArea * k * k, circle.getArea(), EPS);

}

BOOST_AUTO_TEST_CASE(errRadius)
{
  BOOST_CHECK_THROW(rybalko::Circle circle(-6.0, { 3.0, 3.0 }), std::invalid_argument);
  BOOST_CHECK_THROW(rybalko::Circle circle(0.0, { 3.0, 3.0 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(errCoeff)
{
  rybalko::Circle circle(6.0, { 3.0, 3.0 });
  BOOST_CHECK_THROW(circle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(triangleTest)

BOOST_AUTO_TEST_CASE(firstMove)
{
  rybalko::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  const double oldArea = triangle.getArea();
  triangle.move({ 5.0, 5.0 });
  BOOST_CHECK_CLOSE(oldArea, triangle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(secondMove)
{
  rybalko::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  const double oldArea = triangle.getArea();
  triangle.move(5.0, 5.0);
  BOOST_CHECK_CLOSE(oldArea, triangle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(scale)
{
  rybalko::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  double oldArea = triangle.getArea();
  double k = 9;
  triangle.scale(9);
  BOOST_CHECK_CLOSE(oldArea * k * k, triangle.getArea(), EPS);
  oldArea = triangle.getArea();
  k = 0.5;
  triangle.scale(0.5);
  BOOST_CHECK_CLOSE(oldArea * k * k, triangle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(errCoeff)
{
  rybalko::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  BOOST_CHECK_THROW(triangle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(triangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(polygonTest)

BOOST_AUTO_TEST_CASE(firstMove)
{
  rybalko::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t qVertexes = sizeof(fig) / sizeof(fig[0]);
  rybalko::Polygon polygon(qVertexes, fig);
  const double oldA = polygon.getArea();
  polygon.move({ 5.0, 5.0 });
  BOOST_CHECK_CLOSE(oldA, polygon.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(secondMove)
{
  rybalko::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  rybalko::Polygon polygon(quantityOfVertexes, fig);
  const double oldArea = polygon.getArea();
  polygon.move(5.0, 5.0);
  BOOST_CHECK_CLOSE(oldArea, polygon.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(scale)
{
  rybalko::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  rybalko::Polygon polygon(quantityOfVertexes, fig);
  double oldArea = polygon.getArea();
  double k = 9;
  polygon.scale(9);
  BOOST_CHECK_CLOSE(oldArea * k * k, polygon.getArea(), EPS);
  oldArea = polygon.getArea();
  k = 0.5;
  polygon.scale(0.5);
  BOOST_CHECK_CLOSE(oldArea * k * k, polygon.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(errCoeff)
{
  rybalko::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  rybalko::Polygon polygon(quantityOfVertexes, fig);
  BOOST_CHECK_THROW(polygon.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(polygon.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testConstructor)
{
  rybalko::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  size_t zero = 0;
  rybalko::Polygon polygon1(quantityOfVertexes, fig);
  rybalko::Polygon polygon2(polygon1);
  BOOST_CHECK_EQUAL(polygon1.getQ(), polygon2.getQ());

  const size_t oldQuantity = polygon2.getQ();
  rybalko::Polygon polygon3(std::move(polygon2));
  BOOST_CHECK_EQUAL(polygon2.getQ(), zero);
  BOOST_CHECK_EQUAL(polygon3.getQ(), oldQuantity);
}

BOOST_AUTO_TEST_CASE(testOperator)
{
  rybalko::point_t fig[] = { {3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {6.0, 6.0}, {3.0, 5.0} };
  size_t quantityOfVertexes = sizeof(fig) / sizeof(fig[0]);
  size_t zero = 0;
  rybalko::Polygon polygon1(quantityOfVertexes, fig);
  rybalko::Polygon polygon2;
  polygon2 = polygon1;
  BOOST_CHECK_EQUAL(polygon1.getQ(), polygon2.getQ());

  const size_t oldQuantity = polygon2.getQ();
  rybalko::Polygon polygon3 = std::move(polygon2);
  BOOST_CHECK_EQUAL(polygon2.getQ(), zero);
  BOOST_CHECK_EQUAL(polygon3.getQ(), oldQuantity);
}

BOOST_AUTO_TEST_SUITE_END()
