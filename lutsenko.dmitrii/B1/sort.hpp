#ifndef SORT_HPP
#define SORT_HPP

#include <iostream>
#include <functional>
#include <string.h>
#include "access.hpp"

template <typename T>
std::function<bool(const T, const T)> chooseDirection(const char *direction)
{
  if (strcmp(direction, "ascending") == 0)
  {
    return [](T a, T b) {return a > b; };
  }
  else if (strcmp(direction, "descending") == 0)
  {
    return [](T a, T b) {return a < b; };
  }
  else
  {
    throw std::invalid_argument("Invalid direction parametr");
  }
}

template <template <class T> class Access, class T>
void sort(T &container, std::function<bool(typename T::value_type, typename T::value_type)> compare)
{
  using index = typename Access<T>::index;
  const index begin = Access<T>::begin(container);
  const index end = Access<T>::end(container);
  for (index i = begin; i != end; ++i)
  {
    for (index j = i; j != end; ++j)
    {
      if (compare(Access<T>::get(container, i), Access<T>::get(container, j)))
      {
        std::swap(Access<T>::get(container, i), Access<T>::get(container, j));
      }
    }
  }
}

template <typename T>
void print(const T &collection)
{
  if (!collection.empty())
  {
    for (auto i : collection)
    {
      std::cout << i << " ";
    }
  }
  std::cout << '\n';
}

#endif
