#include <vector>
#include <forward_list>
#include "sort.hpp"

void task1(const char *direction)
{
  std::vector<int> collectionA;
  int elem = 0;
  while (std::cin >> elem)
  {
    collectionA.push_back(elem);
  }
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Input failed");
  }
  std::vector<int> collectionB = collectionA;
  std::forward_list<int> collectionC(collectionA.begin(), collectionA.end());

  auto directionType = chooseDirection<int>(direction);

  sort<Brackets>(collectionA, directionType);
  sort<At>(collectionB, directionType);
  sort<Iterator>(collectionC, directionType);

  print(collectionA);
  print(collectionB);
  print(collectionC);
}
