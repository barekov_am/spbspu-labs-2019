#include <iostream>

void task1(const char *direction);
void task2(const char *fileName);
void task3();
void task4(std::size_t size, const char *direction);

int main(int argc, char *argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4))
    {
      std::cerr << "Invalid number of parametrs";
      return 1;
    }
    const unsigned int taskNumber = std::stoi(argv[1]);
    switch (taskNumber)
    {
    case 1:
      if (argc != 3)
      {
        std::cerr << "Invalid number of parametrs";
        return 1;
      }
      if (argv[2] == nullptr)
      {
        std::cerr << "Invalid direction parametr";
        return 1;
      }
      task1(argv[2]);
      break;

    case 2:
      if (argc != 3)
      {
        std::cerr << "Invalid number of parametrs";
        return 1;
      }
      if (argv[2] == nullptr)
      {
        std::cerr << "Invalid filename";
        return 1;
      }
      task2(argv[2]);
      break;

    case 3:
      if (argc != 2)
      {
        std::cerr << "Invalid number of parametrs";
        return 1;
      }
      task3();
      break;

    case 4:
      if (argc != 4)
      {
        std::cerr << "Invalid number of parametrs";
        return 1;
      }
      if (argv[2] == nullptr)
      {
        std::cerr << "Invalid direction parametr";
        return 1;
      }
      task4(std::stoi(argv[3]), argv[2]);
      break;

    default:
      std::cerr << "Incorrect number of task";
      return 1;
    }
  }
  catch (std::exception &exception)
  {
    std::cerr << exception.what();
    return 1;
  }
  return 0;
}
