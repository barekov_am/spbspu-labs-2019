#include <vector>
#include "sort.hpp"

void task3()
{
  int elem = 0;
  std::vector<int> vect;
  bool isZeroFlag = false;
  
  while (std::cin >> elem)
  {
    if (elem == 0)
    {
      isZeroFlag = true;
      break;
    }
    vect.push_back(elem);
  }
  
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Input failed");
  }
  
  if (vect.empty())
  {
    return;
  }
  
  if (!isZeroFlag)
  {
    throw std::runtime_error("Input must be ended by zero");
  }
  
  if (vect.back() == 1)
  {
    for (auto i = vect.begin(); i != vect.end();)
    {
      if (*i % 2 == 0)
      {
        i = vect.erase(i);
      }
      else
      {
        ++i;
      }
    }
  }
  else if (vect.back() == 2)
  {
    for (auto i = vect.begin(); i != vect.end();)
    {
      if (*i % 3 == 0)
      {
        i = vect.insert(++i, 3, 1) + 3;
      }
      else
      {
        ++i;
      }
    }
  }
  print(vect);
}
