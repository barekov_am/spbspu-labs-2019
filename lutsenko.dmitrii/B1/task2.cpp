#include <vector>
#include <memory>
#include <fstream>
#include <iostream>

const std::size_t INITIALSIZE = 256;

void task2(const char* fileName)
{
  std::ifstream file(fileName);
  
  if (!file)
  {
    throw std::runtime_error("Error opening a file");
  }

  std::size_t size = INITIALSIZE;

  std::unique_ptr<char[], decltype(&free)> array(static_cast<char*>(calloc(size, 1)), &free);

  std::size_t i = 0;
  while (file)
  {
    file.read(&array[i], INITIALSIZE);
    i += file.gcount();

    if (file.gcount() == INITIALSIZE) 
    {
      size += INITIALSIZE;
      std::unique_ptr<char[], decltype(&free)> temp(static_cast<char*>(realloc(array.get(), size)), &free);

      if (!temp)
      {
        throw std::runtime_error("Realloc failed");
      }
      array.release();
      std::swap(array, temp);
    }
  }
  
  file.close();

  if (file.is_open())
  {
    throw std::runtime_error("Closing file failed");
  }
  
  std::vector<char> vect(&array[0], &array[i]);

  for (auto i: vect)
  {
    std::cout << i;
  } 
}
