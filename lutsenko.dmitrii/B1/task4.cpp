#include <ctime>
#include <random>
#include "sort.hpp"

void fillRandom(double *array, std::size_t size)
{
  std::mt19937 generator(time(0));
  std::uniform_real_distribution<> distribution(-1.0, 1.0);
  for (size_t i = 0; i < size; i++)
  {
    array[i] = distribution(generator);
  }
}

void task4(std::size_t size, const char *direction)
{
  if (size == 0)
  {
    throw std::invalid_argument("size can't be 0");
  }
  
  std::vector<double> vect(size);
  fillRandom(&vect[0], size);
  print(vect);
  auto directionType = chooseDirection<double>(direction);
  sort<At>(vect, directionType);
  print(vect);
}
