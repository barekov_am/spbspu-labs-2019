#ifndef ACCESS_HPP
#define ACCESS_HPP

#include <cstddef>

template <typename T>
struct Brackets
{
  using index = std::size_t;
  static typename T::value_type &get(T &container, index i)
  {
    return container[i];
  }
  static index begin(const T &)
  {
    return 0;
  }
  static index end(T &container)
  {
    return container.size();
  }
};

template <typename T>
struct At
{
  using index = std::size_t;
  static typename T::value_type &get(T &container, index i)
  {
    return container[i];
  }
  static index begin(const T &)
  {
    return 0;
  }
  static index end(T &container)
  {
    return container.size();
  }
};

template <typename T>
struct Iterator
{
  using index = typename T::iterator;
  static typename T::value_type &get(const T &, index &iterator)
  {
    return *iterator;
  }
  static index begin(T &container)
  {
    return container.begin();
  }
  static index end(T &container)
  {
    return container.end();
  }
};

#endif
