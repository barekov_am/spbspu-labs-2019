#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "split.hpp"

void showRectangle(lutsenko::rectangle_t rect)
{
  std::cout << "width: " << rect.width << ", height: " << rect.height;
  std::cout << ", x: " << rect.pos.x << ", y: " << rect.pos.y << "\n";
}

void show(const lutsenko::Shape &shape)
{
  showRectangle(shape.getFrameRect());
  std::cout << "Area = " << shape.getArea() << "\n";
}

int main()
{
  std::cout << "        Demonstrate Rectangle" << "\n";
  lutsenko::Rectangle rect({2.0, 4.0, {3.0, 3.0}});
  show(rect);
  rect.rotate(90);
  show(rect);
  rect.move({100.0, 200.0});
  show(rect);
  rect.scale(3.0);
  rect.move(10.0, 20.0);
  show(rect); 
  lutsenko::Rectangle rect2({6.0, 9.0, {7.0, 7.0}});
  show(rect2);
  rect2.scale(10.0);
  show(rect2);
  std::cout << "        Demonstrate Circle" << "\n";
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  show(circle);
  circle.move({5.0, 7.0});
  show(circle);
  circle.move(10.0, 10.0);
  show(circle);
  lutsenko::Circle circle2(15.0, {1.0, 1.0});
  show(circle2);
  circle2.scale(3.0);
  circle2.move({8.0, 8.0});
  show(circle2);
  std::cout << "        Demonstrate CompositeShape" << "\n";
  lutsenko::CompositeShape composition(std::make_shared<lutsenko::Rectangle>(rect));
  composition.add(std::make_shared<lutsenko::Circle>(circle));
  lutsenko::CompositeShape composition1(composition);
  composition1.add(std::make_shared<lutsenko::Rectangle>(rect2));
  composition.add(std::make_shared<lutsenko::Circle>(circle2));
  show(composition1);
  composition1.rotate(360);
  show(composition1);
  composition.scale(2.0);
  show(composition);
  composition.move(10.0, 10.0);
  show(composition);
  composition.move({11.0, 12.0});
  show(composition);
  composition.scale(0.5);
  show(composition);
  composition.remove(0);
  show(composition1);
  composition1 = composition;
  show(composition1);
  std::cout << "        Demonstrate Matrix" << "\n";
  lutsenko::Rectangle rect3({4.0, 2.0, {3.0, 2.0}});
  lutsenko::Rectangle rect4({3.0, 5.0, {5.0, 4.0}});
  lutsenko::Circle circle3(1.0, {2.0, 5.0});
  lutsenko::Circle circle4(2.0, {1.0, 7.0});
  lutsenko::CompositeShape composition3(std::make_shared<lutsenko::Rectangle>(rect4));
  composition3.add(std::make_shared<lutsenko::Circle>(circle4));

  std::unique_ptr<lutsenko::shape_ptr[]> shape_array = std::make_unique<lutsenko::shape_ptr[]>(3);
  shape_array[0] = std::make_shared<lutsenko::Rectangle>(rect3);
  shape_array[1] = std::make_shared<lutsenko::Circle>(circle3);
  shape_array[2] = std::make_shared<lutsenko::CompositeShape>(composition3);
  lutsenko::Matrix matrix(std::move(lutsenko::split(shape_array, 3)));
  matrix.printInfo();
  
  lutsenko::split(composition3).printInfo();
  
  matrix.add(std::make_shared<lutsenko::Rectangle>(rect4));
  matrix.add(std::make_shared<lutsenko::Circle>(circle4));
  matrix.printInfo();
  
  std::cout << "\n" << matrix[0][0]->getArea() << "\n";
  std::cout << matrix[0][1]->getArea() << "\n";
  std::cout << matrix[2].getSize();

  return 0;
}
