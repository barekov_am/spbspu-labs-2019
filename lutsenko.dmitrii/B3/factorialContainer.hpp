#ifndef FACTORIAL_CONTAINER_HPP
#define FACTORIAL_CONTAINER_HPP

#include <iterator>

const size_t MAX_SIZE = 10;

class FactorialIterator :public std::iterator<std::bidirectional_iterator_tag, unsigned long long int>
{
public:
  FactorialIterator();
  FactorialIterator(size_t index);

  unsigned long long int &operator *();
  unsigned long long int *operator ->();

  FactorialIterator &operator ++();
  FactorialIterator &operator --();

  bool operator ==(FactorialIterator &) const;
  bool operator !=(FactorialIterator &) const;

private:
  unsigned long long int value_;
  size_t index_;
  unsigned long long int factorial(size_t index);
};


class FactorialContainer
{
public:
  FactorialIterator begin();
  FactorialIterator end();
};

#endif
