#include <functional>
#include <vector>
#include "task1Commands.hpp"

void task1()
{
  Book book;
  std::vector<bookmark> marks = { {"current", book.end_()} };

  std::function<void(std::istream &)> add = std::bind(parse_add, std::placeholders::_1, &book);
  std::function<void(std::istream &)> store = std::bind(parse_store, std::placeholders::_1, &marks);
  std::function<void(std::istream &)> insert = std::bind(parse_insert, std::placeholders::_1, &book, &marks);
  std::function<void(std::istream &)> deleteMark = std::bind(parse_deleteMark, std::placeholders::_1, &book, &marks);
  std::function<void(std::istream &)> show = std::bind(parse_show, std::placeholders::_1, &book, &marks);
  std::function<void(std::istream &)> move = std::bind(parse_move, std::placeholders::_1, &book, &marks);

  std::vector<std::string> commands = { "add", "store", "insert", "delete", "show", "move" };
  std::function<void(std::istream &)> functions[] = { add, store, insert, deleteMark, show, move };

  std::string line;

  while (std::getline(std::cin, line))
  {
    std::stringstream in(line);
    in >> line;
    size_t functionIndex = std::distance(commands.begin(), std::find(commands.begin(), commands.end(), line));
    if (functionIndex < commands.size())
    {
      functions[functionIndex](in);
    }
    else
    {
      std::cout << "<INVALID COMMAND>" << '\n';
    }
    if (!std::cin.eof() && std::cin.fail())
    {
      throw std::runtime_error("Input failed");
    }
  }
}
