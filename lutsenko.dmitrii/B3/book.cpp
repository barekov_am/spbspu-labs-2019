#include <iomanip>
#include "book.hpp"

void Book::show(record &record)
{
  std::cout << std::setw(12) << std::setfill('0') << record.phone << " " << record.name << '\n';
}

std::list<Book::record>::iterator Book::next(std::list<record>::iterator current)
{
  return ++current;
}

std::list<Book::record>::iterator Book::prev(std::list<record>::iterator current)
{
  return --current;
}

void Book::insertBefore(std::list<record>::iterator current, std::string name, unsigned long long int phone)
{
  book_.insert(current, {name, phone});
}

void Book::insertAfter(std::list<record>::iterator current, std::string name, unsigned long long int phone)
{
  book_.insert(++current, {name, phone});
}

void Book::replace(std::list<record>::iterator current, std::string name, unsigned long long int phone)
{
  current->name = name;
  current->phone = phone;
}

void Book::push_back(std::string name, unsigned long long int phone)
{
  book_.push_back({name, phone});
}

std::list<Book::record>::iterator Book::moveUpTo(std::list<record>::iterator current, const int n)
{
  return std::next(current, n);
}

std::list<Book::record>::iterator Book::moveDownTo(std::list<record>::iterator current, const int n)
{
  return std::prev(current, n);
}

std::list<Book::record>::iterator Book::erase(std::list<Book::record>::iterator current)
{
  return book_.erase(current);
}

bool Book::empty()
{
  return book_.empty();
}

std::list<Book::record>::iterator Book::begin_()
{
  return book_.begin();
}

std::list<Book::record>::iterator Book::end_()
{
  return book_.end();
}

void Book::print()
{
  for (auto i : book_)
  {
    std::cout << i.name << " " << i.phone << " " << '\n';
  }
}
