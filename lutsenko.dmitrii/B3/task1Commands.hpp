#ifndef TASK1_COMMANDS_HPP
#define TASK1_COMMANDS_HPP

#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
#include <vector>
#include "book.hpp"

struct bookmark
{
  std::string name;
  std::list<Book::record>::iterator recordIterator;
};

bool checkMarkName(std::string &mark_name)
{
  if (std::find_if_not(mark_name.begin(), mark_name.end(), [](const char s) { return ((s == '-') || std::isalnum(s)); }) == mark_name.end())
  {
    return true;  
  }
  std::cout << "<INVALID BOOKMARK>" << '\n';
  return false;
}

bool readMark(std::istream &in, std::vector<bookmark> *marks, std::vector<bookmark>::iterator &findedMark)
{
  std::string mark_name;
  in >> mark_name;
  if (checkMarkName(mark_name))
  {    
    findedMark = std::find_if(marks->begin(), marks->end(), [mark_name](const bookmark &mark) { return mark.name == mark_name; });
    if (findedMark != marks->end())
    {
      return true;
    }
    else
    {
      std::cout << "<INVALID BOOKMARK>" << '\n';
      return false;
    }
  }
  return false;
}

bool readName(std::istream &in, std::string &name)
{
  char* temp = new char;
  in.read(temp, 1);
  std::getline(in >> std::ws, name);
  if (name.empty() || (name.front() != '\"' || name.back() != '\"') || *temp != ' ')
  {
    std::cout << "<INVALID COMMAND>" << '\n';
    delete temp;
    return false;
  }
  delete temp;
  return true;
}

void parse_add(std::istream &in, Book *book)
{
  unsigned long long int phone;
  in >> phone;
  std::string name;
  if (readName(in, name))
  {
    book->push_back(name, phone);
  }
}

void parse_store(std::istream &in, std::vector<bookmark> *marks)
{
  std::vector<bookmark>::iterator findedMark;
  if (readMark(in, marks, findedMark))
  {
    std::string new_mark_name;
    in >> new_mark_name;
    if (checkMarkName(new_mark_name))
    {
      marks->push_back({ new_mark_name, findedMark->recordIterator });
    }
  }
}

void parse_insert(std::istream &in, Book *book, std::vector<bookmark> *marks)
{
  std::string direction;
  in >> direction;
  std::vector<bookmark>::iterator findedMark;  
  if (readMark(in, marks, findedMark))
  {
    unsigned long long int phone;
    in >> phone;
    std::string name;
    if (readName(in, name))
    {
      if (book->empty() && (direction == "before" || direction == "after"))
      {
        book->push_back(name, phone);
        marks->begin()->recordIterator = book->begin_();
        return;
      }
      if (direction == "before")
      {
        book->insertBefore(findedMark->recordIterator, name, phone);
      }
      else if (direction == "after")
      {
        book->insertAfter(findedMark->recordIterator, name, phone);
      }
      else
      {
        std::cout << "<INVALID COMMAND>" << '\n';
      }
    }
  }
}

void parse_deleteMark(std::istream &in, Book *book, std::vector<bookmark> *marks)
{
  std::vector<bookmark>::iterator findedMark;
  if (readMark(in, marks, findedMark))
  {
    auto findedMarkBefore = &findedMark->recordIterator->name;
    if (findedMark->recordIterator == --book->end_())
    {
      book->erase(--book->end_());
      findedMark->recordIterator = --book->end_();
    }
    else
    {
      findedMark->recordIterator = book->erase(findedMark->recordIterator);
    }
    std::for_each(marks->begin(), marks->end(), [findedMark,findedMarkBefore](bookmark &bm)
      {
        if (&bm.recordIterator->name == findedMarkBefore)
        {
          bm.recordIterator = findedMark->recordIterator;
        }
      });
  }
}

void parse_show(std::istream &in, Book *book, std::vector<bookmark> *marks)
{
  std::vector<bookmark>::iterator findedMark;
  if (readMark(in, marks, findedMark))
  {
    if (book->empty())
    {
      std::cout << "<EMPTY>" << '\n';
      return;
    }
    if (findedMark->recordIterator == book->end_())
    {
      findedMark->recordIterator = book->begin_();
    }
    std::string &temp = findedMark->recordIterator->name;
    if (temp.front() == '\"')
    {
      temp.erase(temp.begin());
    }
    for (auto i = temp.begin(); i != (--temp.end()); i++)
    {
      if (*i == '\\')
      {
        i = temp.erase(i);
      }
    }
    if (temp.back() == '\\' || temp.back() == '\"')
    {
      temp.pop_back();
    }
    book->show(*findedMark->recordIterator);
  }
}

void parse_move(std::istream &in, Book *book, std::vector<bookmark> *marks)
{
  std::vector<bookmark>::iterator findedMark;
  if (readMark(in, marks, findedMark))
  {
    std::string step;
    in >> step;
    std::string::iterator stepBegin = step.begin();
    bool checkFirst = ((*stepBegin == '-' || *stepBegin == '+') || std::isdigit(*stepBegin));
    if (checkFirst && std::find_if_not(++stepBegin, step.end(), [](const char s) { return std::isdigit(s); }) == step.end())
    {
      if (findedMark->recordIterator == book->end_())
      {
        findedMark->recordIterator = book->begin_();
      }
      findedMark->recordIterator = book->moveUpTo(findedMark->recordIterator, std::stoi(step));
    }
    else if (step == "first")
    {
      findedMark->recordIterator = book->begin_();
    }
    else if (step == "last")
    {
      findedMark->recordIterator = (--book->end_());
    }
    else
    {
      std::cout << "<INVALID STEP>" << '\n';
    }
  }
}

#endif
