#ifndef BOOK_HPP
#define BOOK_HPP

#include <list>
#include <string>
#include <iostream>
#include <iterator>

class Book
{
public:

  struct record
  {
    std::string name;
    unsigned long long int phone;
  };

  void show(record &record);
  std::list<record>::iterator next(std::list<record>::iterator current);
  std::list<record>::iterator prev(std::list<record>::iterator current);

  void insertBefore(std::list<record>::iterator current, std::string name, unsigned long long int phone);
  void insertAfter(std::list<record>::iterator current, std::string name, unsigned long long int phone);
  void replace(std::list<record>::iterator current, std::string name, unsigned long long int phone);
  void push_back(std::string name, unsigned long long int phone);

  std::list<Book::record>::iterator moveUpTo(std::list<record>::iterator current, const int n);
  std::list<Book::record>::iterator moveDownTo(std::list<record>::iterator current, const int n);
  std::list<Book::record>::iterator erase(std::list<Book::record>::iterator current);
  bool empty();

  std::list<record>::iterator begin_();
  std::list<record>::iterator end_();
  void print();
private:
  std::list<record> book_;
};

#endif
