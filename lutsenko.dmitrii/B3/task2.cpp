#include <iostream>
#include <algorithm>
#include "factorialContainer.hpp"

void task2()
{
  FactorialContainer factorialContainer;
  std::copy(factorialContainer.begin(), factorialContainer.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << '\n';

  std::reverse_copy(factorialContainer.begin(), factorialContainer.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << '\n';
}
