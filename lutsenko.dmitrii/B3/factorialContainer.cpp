#include "factorialContainer.hpp"

FactorialIterator::FactorialIterator() :
  value_(1),
  index_(1)
{
}

FactorialIterator::FactorialIterator(size_t index) :
  value_(factorial(index)),
  index_(index)
{
}

unsigned long long int &FactorialIterator::operator *()
{
  return value_;
}

unsigned long long int *FactorialIterator::operator ->()
{
  return &value_;
}

FactorialIterator &FactorialIterator::operator ++()
{
  if (index_ <= MAX_SIZE)
  {
    ++index_;
    value_ *= index_;
    return *this;
  }
  else
  {
    throw std::out_of_range("Cannot iterate more");
  }
}

FactorialIterator &FactorialIterator::operator --()
{
  if (index_ > 1)
  {
    value_ /= index_;
    --index_;
  }
  return *this;
}

bool FactorialIterator::operator ==(FactorialIterator &other) const
{
  return ((other.value_ == value_) && (other.index_ == index_));
}

bool FactorialIterator::operator !=(FactorialIterator &other) const
{
  return !(*this == other);
}

unsigned long long int FactorialIterator::factorial(size_t index)
{
  if (index > 1)
  {
    return index * factorial(index - 1);
  }
  return 1;
}

FactorialIterator FactorialContainer::begin()
{
  return FactorialIterator(1);
}

FactorialIterator FactorialContainer::end()
{
  return FactorialIterator(MAX_SIZE + 1);
}
