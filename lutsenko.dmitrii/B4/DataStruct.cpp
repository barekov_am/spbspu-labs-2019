#include <iostream>
#include <algorithm>
#include "DataStruct.hpp"

const auto compare = [](const DataStruct &a, const DataStruct &b)
{
  if (a.key1 != b.key1)
  {
    return a.key1 < b.key1;
  }
  if (a.key2 != b.key2)
  {
    return a.key2 < b.key2;
  }

  return a.str.length() < b.str.length();
};

void sort(std::vector<DataStruct> &structs)
{
  std::sort(structs.begin(), structs.end(), compare); 
}

void print(const std::vector<DataStruct> &structs)
{
  for (auto i : structs)
  {
    std::cout << i.key1 << "," << i.key2 << "," << i.str << '\n';
  }
}
