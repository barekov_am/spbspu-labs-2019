#include <iostream>
#include <algorithm>
#include "DataStruct.hpp"

bool checkKey(std::string &stringKey, int &key)
{
  const int maxAbsKeyValue = 5;
  if (stringKey[0] == ' ')
  {
    stringKey.erase(stringKey.begin());
  }
  bool checkFirst = ((stringKey[0] == '-' || stringKey[0] == '+') || std::isdigit(stringKey[0]));
  if (checkFirst && std::find_if_not(++stringKey.begin(), stringKey.end(), [](const char s) { return std::isdigit(s); }) == stringKey.end())
  {
    key = std::stoi(stringKey);
    if (std::abs(key) <= maxAbsKeyValue)
    {
      return true;
    }
  }
  return false;
}

void task1()
{
  std::vector<DataStruct> v;
  std::string line;
  while (std::getline(std::cin, line))
  {
    const size_t firstComaIndex = line.find(',');
    if (firstComaIndex == std::string::npos)
    {
      throw std::invalid_argument("Missing first coma");
    }
    std::string stringKey1 = line.substr(0, line.find(','));
    const size_t secondComaIndex = line.find(',', firstComaIndex + 1);
    if (secondComaIndex == std::string::npos)
    {
      throw std::invalid_argument("Missing second coma");
    }
    std::string stringKey2 = line.substr(firstComaIndex + 1, secondComaIndex - firstComaIndex - 1);
    std::string str = line.substr(secondComaIndex + 1, line.length());
    if (str[0] == ' ')
    {
      str.erase(str.begin());
    }
    int key1 = 0;
    int key2 = 0;
    if (checkKey(stringKey1, key1) && checkKey(stringKey2, key2))
    {
      v.push_back({ key1, key2, str });
    }
    else
    {
      throw std::invalid_argument("Invalid input format, key1 and key2 should be -5 to +5");
    }
  }
  if (!v.empty())
  {
    sort(v);
    print(v);
  }
}
