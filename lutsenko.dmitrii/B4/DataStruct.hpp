#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP

#include <string>
#include <vector>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

void sort(std::vector<DataStruct> &);
void print(const std::vector<DataStruct> &);

#endif
