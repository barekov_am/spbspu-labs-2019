#include <iostream>
#include "DataStruct.hpp"

void task1();

int main()
{
  try
  {
    task1();
  }
  catch (std::exception &exception)
  {
    std::cerr << exception.what() << '\n';
    return 1;
  }
  return 0;
}
