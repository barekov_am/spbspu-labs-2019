#ifndef TEXT_HPP
#define TEXT_HPP

#include <iostream>
#include <string>
#include <vector>

struct element
{
  enum
  {
    WORD, NUMBER, PUNCT, DASH, SPACE
  } type;
  std::string value;
};

class Text
{
public:
  Text(std::istream &in);
  Text(unsigned int width, std::istream &in);
  void readWord();
  void readNumber();
  void readDash();
  void read();
  void print();
private:
  std::istream &in_;
  element temp_;
  std::vector<element> text_;
  const unsigned int maxWordLength = 20;
  const unsigned int maxLineWidth = 40;
};

#endif
