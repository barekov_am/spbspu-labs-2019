#include "text.hpp"
#include <iostream>

int main(int argc, char *argv[])
{
  const unsigned int minLineLength = 25;
  try
  {
    if (argc == 3)
    {
      std::string paramName = argv[1];
      if (paramName != "--line-width")
      {
        throw std::invalid_argument("Invalid parametr name");
      }
      const unsigned int maxLineWidth = std::stoi(argv[2]);
      if (maxLineWidth < minLineLength)
      {
        throw std::invalid_argument("Max length of the line should be more than 25");
      }
      Text text(maxLineWidth, std::cin);
      text.read();
      text.print();
    }
    else if (argc == 1)
    {
      Text text(std::cin);
      text.read();
      text.print();
    }
    else
    {
      std::cerr << "Inavlid number input parametrs" << '\n';
      return 1;
    }
  }
  catch (std::exception &exception)
  {
    std::cerr << exception.what() << '\n';
    return 1;
  }
  return 0;
}
