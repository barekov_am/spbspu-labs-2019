#include "text.hpp"

Text::Text(std::istream &in) :
  in_(in)
{
}

Text::Text(unsigned int width, std::istream &in) :
  in_(in),
  maxLineWidth(width)
{
}

void Text::readWord()
{
  temp_ = { element::WORD, "" };
  while (std::isalpha(in_.peek()) || in_.peek() == '-')
  {
    temp_.value += in_.get();
    if (temp_.value.back() == '-')
    {
      if (!std::isalpha(in_.peek()))
      {
        break;
      }
    }
  }
  if (temp_.value.length() >= maxWordLength)
  {
    throw std::invalid_argument("Invalid word length");
  }
}


void Text::readNumber()
{
  temp_ = { element::NUMBER, "" };
  int dotCounter = 0;
  while ((std::isdigit(in_.peek()) || in_.peek() == '.') && dotCounter < 2)
  {
    if (in_.peek() == '.')
    {
      ++dotCounter;
    }
    temp_.value += in_.get();
  }
  if (temp_.value.length() >= maxWordLength)
  {
    throw std::invalid_argument("Invalid word length");
  }
}

void Text::readDash()
{
  temp_ = { element::DASH, "" };
  size_t counter = 1;
  while (in_.peek() == '-')
  {
    temp_.value += in_.get();
    ++counter;
  }
  if (counter == 2 && text_.back().value.back() == '-')
  {
    temp_.value += text_.back().value.back();
    text_.back().value.pop_back();
    return;
  }
  if (counter != 3)
  {
    throw std::invalid_argument("It should be only three '-' symbols in the dash");
  }
}

void Text::read()
{
  char begin = in_.get();
  if (std::ispunct(begin) && !((begin == '+' || begin == '-') && std::isdigit(in_.peek())))
  {
    throw std::invalid_argument("Text can`t start with punctuation");
  }
  in_.unget();
  while (in_)
  {
    char first = std::cin.get();
    while (std::isblank(in_.peek()))
    {
      in_.get();
    }
    if (std::isalpha(first))
    {
      readWord();
      temp_.value.insert(temp_.value.begin(), first);
      text_.push_back(temp_);
    }
    else if ((first == '+' || first == '-' || std::isdigit(first)) && (std::isdigit(std::cin.peek()) || std::cin.peek() == '.'))
    {
      readNumber();
      temp_.value.insert(temp_.value.begin(), first);
      text_.push_back(temp_);
    }
    else if (std::ispunct(first))
    {
      if (first == '-' && in_.peek() == '-')
      {
        readDash();
        while (in_.peek() == '\n' || std::isblank(in_.peek()))
        {
          in_.get();
        }
        if (std::ispunct(in_.peek()))
        {
          throw std::invalid_argument("It can`t be two punctuation symbols in a raw");
        }
        temp_.value.insert((temp_.value.begin()), first);
        text_.push_back(temp_);
      }
      else
      {
        std::string punctInString;
        punctInString += first;
        while (in_.peek() == '\n' || std::isblank(in_.peek()))
        {
          in_.get();
        }
        if ((std::ispunct(std::cin.peek()) && !(std::cin.peek() == '-' && first == ',')) || ((text_.back().value.back() == '-' && first == '-')))
        {
          throw std::invalid_argument("It can`t be two punctuation symbols in a raw");
        }
        temp_ = { element::PUNCT, punctInString };
        text_.push_back(temp_);
      }
    }
  }
}

void Text::print()
{
  if (!text_.empty())
  {
    unsigned int lineLengthCounter = 0;
    int spaceLengthCounter = -1;
    unsigned int punctCounter = 0;
    for (auto i = text_.begin(); i != text_.end() - 1; i++)
    {
      punctCounter = 0;
      i->type == element::PUNCT ? spaceLengthCounter : ++spaceLengthCounter;
      lineLengthCounter += i->value.length();
      if ((i + 1)->type == element::PUNCT)
      {
        punctCounter += (i + 1)->value.length();
      }
      if ((i + 1)->type == element::DASH)
      {
        punctCounter += (i + 1)->value.length() + 1;
      }
      if (lineLengthCounter + spaceLengthCounter + punctCounter > maxLineWidth)
      {
        std::cout << '\n';
        lineLengthCounter = i->value.length();
        spaceLengthCounter = 0;
      }
      if (lineLengthCounter + spaceLengthCounter + (i + 1)->value.length() > maxLineWidth)
      {
        std::cout << i->value;
      }
      else
      {
        (i + 1)->type == element::PUNCT ? std::cout << i->value : std::cout << i->value << ' ';
      }
    }
    lineLengthCounter + text_.back().value.length() + spaceLengthCounter > maxLineWidth ? std::cout << '\n' << text_.back().value : std::cout << text_.back().value;
    std::cout << '\n';
  }
}
