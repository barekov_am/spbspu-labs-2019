#ifndef STATISTIC
#define STATISTIC

#include <ostream>

class Statistic
{
public:
  long long int max;
  long long int min;
  double mean;
  long long int positiveCounter;
  long long int negativeCounter;
  long long int oddSum;
  long long int evenSum;
  bool isEqualFirstAndLast;

  Statistic();

  void operator()(long long int number);
  bool empty() const noexcept;

private:
  long long int first;
  long long int totalNumbers;
  bool isFirst;
};

std::ostream &operator<<(std::ostream &, const Statistic &);

#endif 
