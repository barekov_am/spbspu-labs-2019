#include <iostream>
#include <algorithm>
#include <iterator>
#include <list>

#include "statistic.hpp"

int main()
{
  std::list<long long int> numbers({ std::istream_iterator<long long int>(std::cin), std::istream_iterator<long long int>() });
  if (!std::cin.eof() && std::cin.fail())
  {
    std::cerr << "Input failed" << "\n";
    return 1;
  }
  std::cout << std::for_each(numbers.begin(), numbers.end(), Statistic());
  return 0;
}
