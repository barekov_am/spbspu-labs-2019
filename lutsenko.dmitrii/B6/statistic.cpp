#include <algorithm>
#include "statistic.hpp"

Statistic::Statistic() :
  mean(0),
  positiveCounter(0),
  negativeCounter(0),
  oddSum(0),
  evenSum(0),
  isEqualFirstAndLast(true),
  totalNumbers(0),
  isFirst(true)
{
}

void Statistic::operator()(long long int number)
{
  if (isFirst)
  {
    first = number;
    max = number;
    min = number;
    isFirst = false;
  }
  else
  {
    isEqualFirstAndLast = (first == number);
    max = std::max(max, number);
    min = std::min(min, number);
  }

  if (number > 0)
  {
    ++positiveCounter;
  }
  else if (number < 0)
  {
    ++negativeCounter;
  }

  (number % 2 ? oddSum : evenSum) += number;
  
  mean = (oddSum + evenSum) / (++totalNumbers);
}

bool Statistic::empty() const noexcept
{
  return (totalNumbers == 0);
}

std::ostream &operator<<(std::ostream &out, const Statistic &statistic)
{
  if (!statistic.empty())
  {
    out << "Max: " << statistic.max << "\n"
      << "Min: " << statistic.min << "\n"
      << "Mean: " << std::fixed << statistic.mean << "\n"
      << "Positive: " << statistic.positiveCounter << "\n"
      << "Negative: " << statistic.negativeCounter << "\n"
      << "Odd Sum: " << statistic.oddSum << "\n"
      << "Even Sum: " << statistic.evenSum << "\n"
      << "First/Last Equal: ";
      statistic.isEqualFirstAndLast ? out << "yes" : out << "no" << "\n";
  }
  else
  {
    out << "No Data" << "\n";
  }

  return out;
}
