#include "task2Details.hpp"

void task2()
{
  std::vector<Shape> vect = readShapes(std::cin);

  std::cout << "Vertices:" << std::accumulate(vect.begin(), vect.end(), 0, [](int sum, const Shape &s) {return sum + s.size(); }) << '\n';
  std::cout << "Triangles:" << std::count_if(vect.begin(), vect.end(), isTriangle) << '\n';
  std::cout << "Squares:" << std::count_if(vect.begin(), vect.end(), isSquare) << '\n';
  std::cout << "Rectangles:" << std::count_if(vect.begin(), vect.end(), isRectangle) << '\n';

  deleteAllPentagons(vect);

  std::vector<Point> points = createPointVector(vect);

  std::cout << "Points:";
  print(points);
  
  transform(vect);

  std::cout << "Shapes:" << '\n';
  print(vect); 
}
