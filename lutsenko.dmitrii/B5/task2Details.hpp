#ifndef TASK2Details_HPP
#define TASK2Details_HPP

#include <iostream>
#include <vector>
#include <string>
#include <numeric>
#include <algorithm>

struct Point
{
  int x, y;
};

using Shape = std::vector<Point>;

std::vector<Shape> readShapes(std::istream &in)
{
  std::vector<Shape> v;
  int n;
  while (in >> n)
  {
    if (in.fail() || in.peek() != ' ')
    {
      throw std::invalid_argument("Invalid number of vertices input");
    }
    Shape shape;
    for (int i = 0; i < n; i++)
    {
      Point point;
      in.ignore(std::numeric_limits<std::streamsize>::max(), '(');
      in >> point.x;

      in.ignore(std::numeric_limits<std::streamsize>::max(), ';');
      in >> point.y;

      in.ignore(std::numeric_limits<std::streamsize>::max(), ')');
      
      if (in.fail())
      {
        throw std::invalid_argument("Invalid input of coordinates");
      }
      shape.push_back(point);
    }
    std::string leftLine;
    std::getline(in, leftLine);
    if (std::find_if(leftLine.begin(), leftLine.end(), [](char c) {return !std::isspace(c); }) != leftLine.end())
    {
      throw std::invalid_argument("Number of coordinates is more than expected");
    }
    v.push_back(shape);
  }
  return v;
}

bool isTriangle(const Shape &shape)
{
  return (shape.size() == 3);
}

int squareDistanse(const Point &a, const Point &b)
{
  return ((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
}

bool isSquare(const Shape &shape)
{
  if (shape.size() == 4)
  {
    int sqDistanses[] = { squareDistanse(shape[0], shape[1]), squareDistanse(shape[0], shape[2]), squareDistanse(shape[0], shape[3]) };
    std::swap(sqDistanses[2], *std::max_element(std::begin(sqDistanses), std::end(sqDistanses)));
    return (sqDistanses[0] == sqDistanses[1]) && (sqDistanses[0] * 2 == sqDistanses[2]);
  }
  return false;
}

bool isRectangle(const Shape &shape)
{
  if (shape.size() == 4)
  {
    int sqDistanses[] = { squareDistanse(shape[0], shape[1]), squareDistanse(shape[0], shape[2]), squareDistanse(shape[0], shape[3]) };
    std::swap(sqDistanses[2], *std::max_element(std::begin(sqDistanses), std::end(sqDistanses)));
    return (sqDistanses[0] + sqDistanses[1] == sqDistanses[2]);
  }
  return false;
}

void deleteAllPentagons(std::vector<Shape> &vect)
{
  vect.erase(std::remove_if(vect.begin(), vect.end(), [](const Shape &sh) {return sh.size() == 5; }), vect.end());
}

std::vector<Point> createPointVector(std::vector<Shape> &vect)
{
  std::vector<Point> points;
  for (auto i : vect)
  {
    points.push_back(i[0]);
  }
  return points;
}

void transform(std::vector<Shape> &vect)
{
  auto triangleLast = std::partition(vect.begin(), vect.end(), [](Shape &a) {return isTriangle(a); });
  auto squareLast = std::partition(triangleLast, vect.end(), [](Shape &a) {return isSquare(a); });
  std::partition(squareLast, vect.end(), [](Shape &a) {return isRectangle(a); });
}

void print(const std::vector<Shape> &vect)
{
  if (!vect.empty())
  {
    for (auto i : vect)
    {
      std::cout << i.size();
      for (auto j : i)
      {
        std::cout << " " << "(" << j.x << ";" << j.y << ")";
      }
      std::cout << '\n';
    }
  }
}

void print(const std::vector<Point> &points)
{
  if (!points.empty())
  {
    for (auto i : points)
    {
      std::cout << " " << "(" << i.x << ";" << i.y << ")";
    }
  }
  std::cout << '\n';
}

#endif
