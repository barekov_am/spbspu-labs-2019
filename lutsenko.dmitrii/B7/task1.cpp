#define _USE_MATH_DEFINES
#include <list>
#include <cmath>
#include <iostream>
#include <iterator>
#include <algorithm>

void task1()
{
  std::list<double> numbers({ std::istream_iterator<double>(std::cin), std::istream_iterator<double>() });
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Input failed");
  }
  std::for_each(numbers.begin(), numbers.end(), [](double &elem) {elem *= M_PI; });
  std::copy(numbers.begin(), numbers.end(), std::ostream_iterator<double>(std::cout, " "));
  std::cout << '\n';
}
