#include <list>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <memory>
#include <string>
#include "figures.hpp"

void print(std::list<std::shared_ptr<Shape>> figures)
{
  std::for_each(figures.begin(), figures.end(), [&](std::shared_ptr<Shape> sh) { sh->draw(std::cout); });
}

void task2()
{
  std::list<std::shared_ptr<Shape>> figures;
  std::string figureName;
  while (std::getline(std::cin >> std::ws, figureName, '('))
  {
    figureName.erase(std::remove_if(figureName.begin(), figureName.end(), [](char s) {return std::isblank(s); }), figureName.end());
    std::string line;
    std::getline(std::cin, line);
    std::stringstream in(line);

    point_t center;
    in >> center.x;

    in.ignore(std::numeric_limits<std::streamsize>::max(), ';');
    in >> center.y;
    
    in.ignore(std::numeric_limits<std::streamsize>::max(), ')');

    if (!in)
    {
      throw std::invalid_argument("Invalid input");
    }

    using figureWithName = std::pair<std::string, std::shared_ptr<Shape>>;
    std::list<figureWithName> figuresWithName = { std::make_pair("CIRCLE", std::make_shared<Circle>(center)),
                                                  std::make_pair("TRIANGLE", std::make_shared<Triangle>(center)),
                                                  std::make_pair("SQUARE", std::make_shared<Square>(center)) };
  
    auto findedPair = std::find_if(figuresWithName.begin(), figuresWithName.end(), [&](figureWithName &pair) {return pair.first == figureName; });
    findedPair != figuresWithName.end() ? figures.push_back(findedPair->second) : throw std::invalid_argument("There is no figure with this name");
  }
  std::cout << "Original:" << '\n';
  print(figures);

  std::cout << "Left-Right:" << '\n';
  figures.sort([](std::shared_ptr<Shape> &a, std::shared_ptr<Shape> &b) { return a->isMoreLeft(*b); });
  print(figures);

  std::cout << "Right-Left:" << '\n';
  figures.sort([](std::shared_ptr<Shape> &a, std::shared_ptr<Shape> &b) { return b->isMoreLeft(*a); });
  print(figures);

  std::cout << "Top-Bottom:" << '\n';
  figures.sort([](std::shared_ptr<Shape> &a, std::shared_ptr<Shape> &b) { return a->isUpper(*b); });
  print(figures);

  std::cout << "Bottom-Top:" << '\n';
  figures.sort([](std::shared_ptr<Shape> &a, std::shared_ptr<Shape> &b) { return b->isUpper(*a); });
  print(figures);
}
