#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <ostream>
#include "point.hpp"

class Shape
{
public:
  Shape(const point_t &center);

  bool isMoreLeft(const Shape &shape) const;
  bool isUpper(const Shape &shape) const;

  virtual void draw(std::ostream &out) = 0;
protected:
  point_t center_;
};

#endif
