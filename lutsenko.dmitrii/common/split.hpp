#ifndef SPLIT_HPP
#define SPLIT_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace lutsenko
{
  Matrix split(const CompositeShape & composite);
  Matrix split(const std::unique_ptr<shape_ptr[]> & list, int size);
}

#endif
