#ifndef MATRIX_HPP
#define MATRIX_HPP
#include "base-types.hpp"
#include "check.hpp"

namespace lutsenko
{
  class Matrix
  {
  public:

    using shape_array = std::unique_ptr<shape_ptr[]>;

    class Layer
    {
    public:
      shape_ptr & operator [](int) const;
      int getSize() const
      {
        return size_;
      }
    private:
      int size_;
      shape_array shapesOnLayer_;
      Layer(const shape_array &, int);
      friend Matrix;
    };

    Matrix();
    Matrix(const Matrix &);
    Matrix(Matrix &&);
    ~Matrix() = default;

    Matrix & operator =(const Matrix &);
    Matrix & operator =(Matrix &&);
    Layer operator [](int) const;
    bool operator ==(const Matrix & other) const;
    bool operator !=(const Matrix & other) const;

    void add(shape_ptr);
    void printInfo();
  private:
    int columns_;
    int rows_;
    shape_array shapeArray_;    
  };
}

#endif
