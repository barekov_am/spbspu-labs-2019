#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testRectangle)

double INACCURACY = 0.001;

BOOST_AUTO_TEST_CASE(unalteredRectangleAfterMove1)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::rectangle_t rectBefore = rect.getFrameRect();
  double areaBefore = rect.getArea();
  rect.move({100.0, 200.0});
  BOOST_CHECK_CLOSE_FRACTION(areaBefore, rect.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rectBefore.height, rect.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rectBefore.width, rect.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(unalteredRectangleAfterMove2)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::rectangle_t rectBefore = rect.getFrameRect();
  double areaBefore = rect.getArea();
  rect.move(10.0, 20.0);
  BOOST_CHECK_CLOSE_FRACTION(areaBefore, rect.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rectBefore.height, rect.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rectBefore.width, rect.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(quadraticChangeRectangleArea)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  double areaBefore = rect.getArea();
  double coefficient = 3;
  rect.scale(coefficient);
  BOOST_CHECK_CLOSE_FRACTION(areaBefore * coefficient * coefficient, rect.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(corectRectangleParametrs)
{
  BOOST_CHECK_THROW(lutsenko::Rectangle({-10.0, 15.0, 20.0, 25.0}), std::invalid_argument);
  BOOST_CHECK_THROW(lutsenko::Rectangle({10.0, -15.0, 20.0, 25.0}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(corectRectangleScaling)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  BOOST_CHECK_THROW(rect.scale(-5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(corectRotate)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  double areaBefore = rect.getArea();
  rect.rotate(123);
  BOOST_CHECK_CLOSE_FRACTION(areaBefore, rect.getArea(), INACCURACY);
  lutsenko::Rectangle rect1({10.0, 5.0, 2.0, 25.0});
  lutsenko::rectangle_t rectBefore = rect1.getFrameRect();
  rect1.rotate(90);
  BOOST_CHECK_CLOSE_FRACTION(rectBefore.height, rect1.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rectBefore.width, rect1.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_SUITE_END();
