#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <math.h>
#include <iostream>
#include <stdexcept>
#include <algorithm>

lutsenko::CompositeShape::CompositeShape() :
  count_(0)
{
}

lutsenko::CompositeShape::CompositeShape(const lutsenko::CompositeShape & other) :
  count_(other.count_),
  shapeArray_(std::make_unique<shape_ptr[]>(other.count_))
{
  for (int i = 0; i < count_; i++)
  {
    shapeArray_[i] = other.shapeArray_[i];
  }
}

lutsenko::CompositeShape::CompositeShape(lutsenko::CompositeShape && other) :
  count_(other.count_),
  shapeArray_(std::move(other.shapeArray_))
{
}

lutsenko::CompositeShape::CompositeShape(lutsenko::shape_ptr shape) :
  count_(1),
  shapeArray_(std::make_unique<shape_ptr[]>(1))
{
  shapeArray_[0] = shape;
}

lutsenko::CompositeShape & lutsenko::CompositeShape::operator =(const lutsenko::CompositeShape & other)
{
  if (this != &other)
  {
    count_ = other.count_;
    std::unique_ptr<shape_ptr[]> newArray = std::make_unique<shape_ptr[]>(other.count_);
    for (int i = 0; i < count_; i++)
    {
      newArray[i] = other.shapeArray_[i];
    }
    shapeArray_.swap(newArray);
  }

  return *this;
}

lutsenko::CompositeShape & lutsenko::CompositeShape::operator =(lutsenko::CompositeShape && other)
{
  if (this != &other)
  {
    count_ = other.count_;
    shapeArray_ = std::move(other.shapeArray_);
  }

  return *this;
}

lutsenko::shape_ptr lutsenko::CompositeShape::operator [](int index) const
{
  if ((index < 0) || (index >= count_))
  {
    throw std::invalid_argument("there is no element with this index");
  }

  return shapeArray_[index];
}

void lutsenko::CompositeShape::add(shape_ptr shape)
{
  if (!shape)
  {
    throw std::invalid_argument("can't add this to composition");
  }
  std::unique_ptr<shape_ptr[]> newArray = std::make_unique<shape_ptr[]>(count_ + 1);
  for (int i = 0; i < count_; i++)
  {
    newArray[i] = shapeArray_[i];
  }
  newArray[count_++] = shape;
  shapeArray_.swap(newArray);
}

double lutsenko::CompositeShape::getArea() const
{
  if (count_ == 0)
  {
    throw std::invalid_argument("there is no figures inside");
  }
  double compositeShapeArea = 0;
  for (int i = 0; i < count_; i++)
  {
    compositeShapeArea += shapeArray_[i]->getArea();
  }

  return compositeShapeArea;
}

lutsenko::rectangle_t lutsenko::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::invalid_argument("there is no figures inside");
  }
  lutsenko::rectangle_t tempRect = shapeArray_[0]->getFrameRect();
  lutsenko::point_t upperLeft = {tempRect.pos.x - tempRect.width / 2, tempRect.pos.y + tempRect.height / 2};
  lutsenko::point_t bottomRight = {tempRect.pos.x + tempRect.width / 2, tempRect.pos.y - tempRect.height / 2};
  for (int i = 1; i < count_ ; i++)
  {
    tempRect = shapeArray_[i]->getFrameRect();
    upperLeft = {std::min(upperLeft.x, tempRect.pos.x - tempRect.width / 2), std::max(upperLeft.y, tempRect.pos.y + tempRect.height / 2)};
    bottomRight = {std::max(bottomRight.x, tempRect.pos.x + tempRect.width / 2), std::min(bottomRight.y, tempRect.pos.y - tempRect.height / 2)};
  }

  return {bottomRight.x - upperLeft.x, upperLeft.y - bottomRight.y, (bottomRight.x + upperLeft.x) / 2, (upperLeft.y + bottomRight.y) / 2};
}

void lutsenko::CompositeShape::move(double dx, double dy)
{
  if (count_ == 0)
  {
    throw std::invalid_argument("there is no figures inside");
  }
  for (int i = 0; i < count_; i++)
  {
    shapeArray_[i]->move(dx, dy);
  }
}

void lutsenko::CompositeShape::move(const point_t & point)
{
  if (count_ == 0)
  {
    throw std::invalid_argument("there is no figures inside");
  }
  const point_t frameRectPos = getFrameRect().pos;
  for (int i = 0; i < count_; i++)
  {
    shapeArray_[i]->move(point.x - frameRectPos.x, point.y - frameRectPos.y);
  }
}

void lutsenko::CompositeShape::scale(double coefficient)
{
  if (count_ == 0)
  {
    throw std::invalid_argument("there is no figures inside");
  }
  if (coefficient <= 0)
  {
    throw std::invalid_argument("coefficient should be greater than zero");
  }
  const point_t frameRectPos = getFrameRect().pos;
  for (int i = 0; i < count_; i++)
  {
    double dx = shapeArray_[i]->getFrameRect().pos.x - frameRectPos.x;
    double dy = shapeArray_[i]->getFrameRect().pos.y - frameRectPos.y;
    shapeArray_[i]->move(dx * (coefficient - 1), dy * (coefficient - 1));
    shapeArray_[i]->scale(coefficient);
  }
}

void lutsenko::CompositeShape::remove(int index)
{
  if ((index < 0) || (index > count_ - 1))
  {
    throw std::invalid_argument("there is no element with this index");
  }
  for (int i = index; i < count_ - 1; i++)
  {
    shapeArray_[i] = shapeArray_[i + 1];
  }
  count_--;
}

void lutsenko::CompositeShape::rotate(double angle)
{
  if (count_ == 0)
  {
    throw std::invalid_argument("there is no figures inside");
  }
  for (int i = 0; i < count_; i++)
  {
    angle *= M_PI / 180;
    point_t pos = shapeArray_[i]->getFrameRect().pos;
    point_t csPos = this->getFrameRect().pos;
    double tempX = pos.x;
    pos.x = (pos.x - csPos.x) * cos(angle) - (pos.y - csPos.y) * sin(angle) + csPos.x;
    pos.y = (tempX - csPos.x) * sin(angle) + (pos.y - csPos.y) * cos(angle) + csPos.y;
    shapeArray_[i]->move(pos);
    shapeArray_[i]->rotate(angle / M_PI * 180);
  }
}
