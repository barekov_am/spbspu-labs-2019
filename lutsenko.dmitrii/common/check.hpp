#ifndef CHECK_HPP
#define CHECK_HPP
#include "shape.hpp"

namespace lutsenko
{
  bool isCovering(const shape_ptr & shape1, const shape_ptr & shape2);
}

#endif
