#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testCircle)

double INACCURACY = 0.001;

BOOST_AUTO_TEST_CASE(unalteredCircleAfterMove1)
{
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::rectangle_t rectBefore = circle.getFrameRect();
  double areaBefore = circle.getArea();
  circle.move({5.0, 7.0});
  BOOST_CHECK_CLOSE_FRACTION(areaBefore, circle.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rectBefore.height, circle.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rectBefore.width, circle.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(unalteredCircleAfterMove2)
{
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::rectangle_t rectBefore = circle.getFrameRect();
  double areaBefore = circle.getArea();
  circle.move({10.0, 10.0});
  BOOST_CHECK_CLOSE_FRACTION(areaBefore, circle.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rectBefore.height, circle.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rectBefore.width, circle.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(quadraticChangeCircleArea)
{
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  double areaBefore = circle.getArea();
  double coefficient = 3;
  circle.scale(coefficient);
  BOOST_CHECK_CLOSE_FRACTION(areaBefore * coefficient * coefficient, circle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(corectCircleParametrs)
{
  BOOST_CHECK_THROW(lutsenko::Circle(-10.0, {1.0, 2.0}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(corectCircleScaling)
{
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  BOOST_CHECK_THROW(circle.scale(-5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(corectRotate)
{
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  double areaBefore = circle.getArea();
  lutsenko::rectangle_t rectBefore = circle.getFrameRect();
  circle.rotate(123);
  BOOST_CHECK_CLOSE_FRACTION(areaBefore, circle.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rectBefore.height, circle.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rectBefore.width, circle.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_SUITE_END();
