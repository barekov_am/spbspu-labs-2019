#include "split.hpp"

lutsenko::Matrix lutsenko::split(const CompositeShape & composition)
{
  Matrix newMatrix{};
  for (int i = 0; i < composition.getCount(); i++)
  {
    newMatrix.add(composition[i]);
  }

  return newMatrix;
}

lutsenko::Matrix lutsenko::split(const std::unique_ptr<shape_ptr[]> & list, int size)
{
  Matrix newMatrix{};
  for (int i = 0; i < size; i++)
  {
    newMatrix.add(list[i]);
  }

  return newMatrix;
}
