#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <memory>
#include "base-types.hpp"

namespace lutsenko
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void move(const point_t & point) = 0;
    virtual void scale(double coefficient) = 0;
    virtual void rotate(double angle) = 0;
    int getLayer() const
    {
      return layer_;
    }
    void setLayer(int layer)
    {
      layer_ = layer;
    }
  protected:
    int layer_;
  };

  using shape_ptr = std::shared_ptr<Shape>;
}

#endif
