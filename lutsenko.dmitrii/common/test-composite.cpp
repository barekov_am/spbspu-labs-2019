#include <stdexcept>
#include <memory>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(testCompositeShape)

double INACCURACY = 0.001;

BOOST_AUTO_TEST_CASE(unalteredCompositionAfterMove1)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::CompositeShape composition{};
  composition.add(std::make_shared<lutsenko::Rectangle>(rect));
  composition.add(std::make_shared<lutsenko::Circle>(circle));
  lutsenko::rectangle_t rect_before = composition.getFrameRect();
  double area_before = composition.getArea();
  composition.move({100.0, 200.0});
  BOOST_CHECK_CLOSE_FRACTION(area_before, composition.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rect_before.height, composition.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rect_before.width, composition.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(unalteredCompositionAfterMove2)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::CompositeShape composition{};
  composition.add(std::make_shared<lutsenko::Rectangle>(rect));
  composition.add(std::make_shared<lutsenko::Circle>(circle));
  lutsenko::rectangle_t rect_before = composition.getFrameRect();
  double area_before = composition.getArea();
  composition.move(10.0, 20.0);
  BOOST_CHECK_CLOSE_FRACTION(area_before, composition.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rect_before.height, composition.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rect_before.width, composition.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(quadraticChangeCompositionArea)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::CompositeShape composition{};
  composition.add(std::make_shared<lutsenko::Rectangle>(rect));
  composition.add(std::make_shared<lutsenko::Circle>(circle));
  double area_before = composition.getArea();
  double coefficient = 3;
  composition.scale(coefficient);
  BOOST_CHECK_CLOSE_FRACTION(area_before * coefficient * coefficient, composition.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(corectCompositionScaling)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::CompositeShape composition{};
  composition.add(std::make_shared<lutsenko::Rectangle>(rect));
  composition.add(std::make_shared<lutsenko::Circle>(circle));
  BOOST_CHECK_THROW(composition.scale(-5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(corectRemovingIndex)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::CompositeShape composition{};
  composition.add(std::make_shared<lutsenko::Rectangle>(rect));
  composition.add(std::make_shared<lutsenko::Circle>(circle));
  BOOST_CHECK_THROW(composition.remove(2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(shapeWithNoFiguresArea)
{
  lutsenko::CompositeShape composition{};
  BOOST_CHECK_THROW(composition.getArea(), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(shapeWithNoFiguresFrameRect)
{
  lutsenko::CompositeShape composition{};
  BOOST_CHECK_THROW(composition.getFrameRect(), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(shapeWithNoFiguresMove1)
{
  lutsenko::CompositeShape composition{};
  BOOST_CHECK_THROW(composition.move({10.0, 20.0}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(shapeWithNoFiguresMove2)
{
  lutsenko::CompositeShape composition{};
  BOOST_CHECK_THROW(composition.move(10.0, 20.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(shapeWithNoFiguresScale)
{
  lutsenko::CompositeShape composition{};
  BOOST_CHECK_THROW(composition.scale(3.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(corectCompositionRemoving)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::CompositeShape composition{};
  composition.add(std::make_shared<lutsenko::Rectangle>(rect));
  double area1 = composition.getArea();
  lutsenko::rectangle_t frameRect1 = composition.getFrameRect();
  composition.add(std::make_shared<lutsenko::Circle>(circle));
  composition.remove(1);
  BOOST_CHECK_CLOSE_FRACTION(area1, composition.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRect1.height, composition.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRect1.width, composition.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(corectCopyConstructor)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::CompositeShape composition1{};
  composition1.add(std::make_shared<lutsenko::Rectangle>(rect));
  composition1.add(std::make_shared<lutsenko::Circle>(circle));
  const lutsenko::rectangle_t frameRect1 = composition1.getFrameRect();
  double area1 = composition1.getArea();
  lutsenko::CompositeShape composition2(composition1);
  BOOST_CHECK_CLOSE_FRACTION(area1, composition2.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRect1.height, composition2.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRect1.width, composition2.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(corectCopyOperator)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Rectangle rect2({1.0, 1.0, 2.0, 2.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::Circle circle2(1.0, {11.0, 22.0});
  lutsenko::CompositeShape composition1{};
  composition1.add(std::make_shared<lutsenko::Rectangle>(rect));
  composition1.add(std::make_shared<lutsenko::Circle>(circle));
  lutsenko::CompositeShape composition2{};
  composition2.add(std::make_shared<lutsenko::Rectangle>(rect2));
  composition2.add(std::make_shared<lutsenko::Circle>(circle2));
  const lutsenko::rectangle_t frameRect2 = composition2.getFrameRect();
  double area2 = composition2.getArea();
  composition1 = composition2;
  BOOST_CHECK_CLOSE_FRACTION(area2, composition1.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRect2.height, composition1.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRect2.width, composition1.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(corectMoveConstructor)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::CompositeShape composition1{};
  composition1.add(std::make_shared<lutsenko::Rectangle>(rect));
  composition1.add(std::make_shared<lutsenko::Circle>(circle));
  const lutsenko::rectangle_t frameRect1 = composition1.getFrameRect();
  double area1 = composition1.getArea();
  lutsenko::CompositeShape composition2(std::move(composition1));
  BOOST_CHECK_CLOSE_FRACTION(area1, composition2.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRect1.height, composition2.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRect1.width, composition2.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(corectMoveOperator)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Rectangle rect2({20.0, 30.0, 5.0, 15.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::CompositeShape composition1(std::make_shared<lutsenko::Rectangle>(rect));
  lutsenko::CompositeShape composition2(std::make_shared<lutsenko::Circle>(circle));
  composition2.add(std::make_shared<lutsenko::Rectangle>(rect2));
  const lutsenko::rectangle_t frameRect2 = composition2.getFrameRect();
  double area2 = composition2.getArea();
  composition1 = std::move(composition2);
  BOOST_CHECK_CLOSE_FRACTION(area2, composition1.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRect2.height, composition1.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRect2.width, composition1.getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(corectIndexOperatorIndex)
{ 
  lutsenko::CompositeShape composition{};
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  composition.add(std::make_shared<lutsenko::Rectangle>(rect));
  BOOST_CHECK_THROW(composition[1]->getArea(), std::invalid_argument);
  BOOST_CHECK_THROW(composition[-1]->getArea(), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(corectIndexOperator)
{
  lutsenko::CompositeShape composition{};
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  composition.add(std::make_shared<lutsenko::Rectangle>(rect));
  BOOST_CHECK_CLOSE_FRACTION(rect.getArea(), composition[0]->getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rect.getFrameRect().height, composition[0]->getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rect.getFrameRect().width, composition[0]->getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(corectRotate)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Circle circle(10.0, {10.0, 20.0});
  lutsenko::CompositeShape composition(std::make_shared<lutsenko::Rectangle>(rect));
  composition.add(std::make_shared<lutsenko::Circle>(circle));
  double areaBefore = composition.getArea();
  composition.rotate(123);
  BOOST_CHECK_CLOSE_FRACTION(areaBefore, composition.getArea(), INACCURACY);
  lutsenko::CompositeShape composition1(composition);
  lutsenko::rectangle_t rectBefore = composition1.getFrameRect();
  composition.rotate(90);
  BOOST_CHECK_CLOSE_FRACTION(rectBefore.height, composition1.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rectBefore.width, composition1.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_SUITE_END();
