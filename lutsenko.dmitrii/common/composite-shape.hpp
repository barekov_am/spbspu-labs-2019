#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include "shape.hpp"

namespace lutsenko
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&);
    CompositeShape(shape_ptr);
    ~CompositeShape() = default;
    
    CompositeShape & operator =(const CompositeShape &);
    CompositeShape & operator =(CompositeShape &&);
    shape_ptr operator[](int) const;
    
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t & point) override;
    void scale(double coefficient) override;
    void add(shape_ptr shape);
    void remove(int index);
    void rotate(double angle) override;
    int getCount() const
    {
      return count_;
    }
  private:
    int count_;
    std::unique_ptr<shape_ptr[]> shapeArray_;
  };
}

#endif
