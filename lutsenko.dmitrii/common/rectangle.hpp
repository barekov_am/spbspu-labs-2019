#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace lutsenko
{
  class Rectangle :public Shape
  {
  public:
    Rectangle(const rectangle_t & rect);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t & point) override;
    void scale(double coefficient) override;
    void rotate(double angle) override;
  private:
    point_t pos_;
    point_t upperLeft_;
    point_t upperRight_;
    point_t bottomLeft_;
    point_t bottomRight_;
  };
}

#endif
