#include <stdexcept>
#include <memory>
#include <boost/test/auto_unit_test.hpp>
#include "shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "split.hpp"

BOOST_AUTO_TEST_SUITE(testMatrix)

double INACCURACY = 0.001;

BOOST_AUTO_TEST_CASE(corectCopyConstructor)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::Rectangle rect1({1.0, 17.0, 2.0, 6.0});
  lutsenko::Circle circle1(2.0, {11.0, 4.0});
  lutsenko::CompositeShape composition{};
  composition.add(std::make_shared<lutsenko::Rectangle>(rect1));
  composition.add(std::make_shared<lutsenko::Circle>(circle1));
  lutsenko::Matrix matrix{};
  matrix.add(std::make_shared<lutsenko::Rectangle>(rect));
  matrix.add(std::make_shared<lutsenko::CompositeShape>(composition));
  BOOST_CHECK_NO_THROW(lutsenko::Matrix matrix1(matrix));
  lutsenko::Matrix matrix2(matrix);
  BOOST_CHECK_EQUAL(true, matrix == matrix2);
}

BOOST_AUTO_TEST_CASE(corectCopyOperator)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::Rectangle rect1({1.0, 17.0, 2.0, 6.0});
  lutsenko::Circle circle1(2.0, {11.0, 4.0});
  lutsenko::CompositeShape composition{};
  composition.add(std::make_shared<lutsenko::Rectangle>(rect1));
  composition.add(std::make_shared<lutsenko::Circle>(circle1));
  lutsenko::Matrix matrix{};
  matrix.add(std::make_shared<lutsenko::Rectangle>(rect));
  matrix.add(std::make_shared<lutsenko::CompositeShape>(composition));
  lutsenko::Matrix matrix1{};
  BOOST_CHECK_NO_THROW(matrix1 = matrix);
  BOOST_CHECK_EQUAL(true, matrix == matrix1);
}

BOOST_AUTO_TEST_CASE(corectMoveConstructorAndOperator)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::Rectangle rect1({1.0, 17.0, 2.0, 6.0});
  lutsenko::Circle circle1(2.0, {11.0, 4.0});
  lutsenko::CompositeShape composition{};
  composition.add(std::make_shared<lutsenko::Rectangle>(rect1));
  composition.add(std::make_shared<lutsenko::Circle>(circle1));
  lutsenko::Matrix matrix{};
  matrix.add(std::make_shared<lutsenko::Rectangle>(rect));
  matrix.add(std::make_shared<lutsenko::Circle>(circle));
  matrix.add(std::make_shared<lutsenko::CompositeShape>(composition));
  BOOST_CHECK_NO_THROW(lutsenko::Matrix matrix1(std::move(matrix)));
  lutsenko::Matrix matrix2{};
  BOOST_CHECK_NO_THROW(matrix2 = std::move(matrix));  
}

BOOST_AUTO_TEST_CASE(corectIndexOperator)
{
  lutsenko::Rectangle rect({10.0, 15.0, 2.0, 2.0});
  lutsenko::Rectangle rect1({1.0, 1.0, 100.0, 100.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  
  lutsenko::Matrix matrix{};
  matrix.add(std::make_shared<lutsenko::Rectangle>(rect));
  matrix.add(std::make_shared<lutsenko::Circle>(circle));
  matrix.add(std::make_shared<lutsenko::Rectangle>(rect1));
  
  BOOST_CHECK_THROW(matrix[2], std::invalid_argument);
  BOOST_CHECK_THROW(matrix[-1], std::invalid_argument);
  BOOST_CHECK_THROW(matrix[0][2], std::invalid_argument);
  BOOST_CHECK_THROW(matrix[-1][-1], std::invalid_argument);
  
  BOOST_CHECK_NO_THROW(matrix[0][0]->getArea());  
  BOOST_CHECK_CLOSE_FRACTION(rect.getArea(), matrix[0][0]->getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rect.getFrameRect().height,  matrix[0][0]->getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rect.getFrameRect().width,  matrix[0][0]->getFrameRect().width, INACCURACY);
  
  BOOST_CHECK_NO_THROW(matrix[1][0]->getFrameRect());  
  BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), matrix[1][0]->getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().height,  matrix[1][0]->getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().width,  matrix[1][0]->getFrameRect().width, INACCURACY);
  
  BOOST_CHECK_NO_THROW(matrix[0][1]->getFrameRect());  
  BOOST_CHECK_CLOSE_FRACTION(rect1.getArea(), matrix[0][1]->getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rect1.getFrameRect().height,  matrix[0][1]->getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(rect1.getFrameRect().width,  matrix[0][1]->getFrameRect().width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(corectEqualOperator)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::Matrix matrix{};
  matrix.add(std::make_shared<lutsenko::Rectangle>(rect));
  lutsenko::Matrix matrix1(matrix);
  BOOST_CHECK_EQUAL(true, matrix == matrix1);
  lutsenko::Matrix matrix2{};
  lutsenko::Matrix matrix3{};
  BOOST_CHECK_EQUAL(true, matrix2 == matrix3);
}

BOOST_AUTO_TEST_CASE(corectNotEqualOperator)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Rectangle rect1({1.0, 17.0, 2.0, 6.0});
  lutsenko::Circle circle(10.0, {1.0, 2.0});
  lutsenko::Matrix matrix{};
  matrix.add(std::make_shared<lutsenko::Rectangle>(rect));
  lutsenko::Matrix matrix1(matrix);
  BOOST_CHECK_EQUAL(false, matrix != matrix1);
  matrix.add(std::make_shared<lutsenko::Circle>(circle));
  BOOST_CHECK_EQUAL(true, matrix != matrix1);
  matrix1.add(std::make_shared<lutsenko::Rectangle>(rect1));
  BOOST_CHECK_EQUAL(true, matrix != matrix1);
}

BOOST_AUTO_TEST_CASE(emptyMatrix)
{
  lutsenko::Matrix matrix{};
  BOOST_CHECK_THROW(matrix.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(matrix.printInfo(), std::invalid_argument);
  BOOST_CHECK_THROW(matrix[0][0], std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(corectSplit)
{
  lutsenko::Rectangle rect({10.0, 15.0, 20.0, 25.0});
  lutsenko::Circle circle(10.0, {19.0, 19.0});
  lutsenko::Rectangle rect1({2.0, 1.0, -20.0, -20.0});
  lutsenko::Circle circle1(7.0, {-32.0, -64.0});
  lutsenko::CompositeShape composition{};
  
  lutsenko::shape_ptr cs_rect1 = std::make_shared<lutsenko::Rectangle>(rect1);
  lutsenko::shape_ptr cs_circle1 = std::make_shared<lutsenko::Circle>(circle1);
  
  composition.add(cs_rect1);
  composition.add(cs_circle1);
  
  lutsenko::shape_ptr figure0 = std::make_shared<lutsenko::Rectangle>(rect);
  lutsenko::shape_ptr figure1 = std::make_shared<lutsenko::Circle>(circle);
  lutsenko::shape_ptr figure2 = std::make_shared<lutsenko::CompositeShape>(composition);

  std::unique_ptr<lutsenko::shape_ptr[]> testArray = std::make_unique<lutsenko::shape_ptr[]>(3);
  testArray[0] = figure0;
  testArray[1] = figure1;
  testArray[2] = figure2;

  lutsenko::Matrix matrix(std::move(lutsenko::split(testArray, 3)));
  BOOST_CHECK_EQUAL(true, matrix[0][0] == figure0);
  BOOST_CHECK_EQUAL(true, matrix[1][0] == figure1);
  BOOST_CHECK_EQUAL(true, matrix[0][1] == figure2);
  
  lutsenko::Matrix matrix1(std::move(lutsenko::split(composition)));
  BOOST_CHECK_EQUAL(true, matrix1[0][0] == cs_rect1);
  BOOST_CHECK_EQUAL(true, matrix1[0][1] == cs_circle1); 
}

BOOST_AUTO_TEST_SUITE_END();
