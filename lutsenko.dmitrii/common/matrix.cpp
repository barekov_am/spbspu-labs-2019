#include "matrix.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>

lutsenko::Matrix::Matrix() :
  columns_(0),
  rows_(0)
{
}

lutsenko::Matrix::Matrix(const lutsenko::Matrix & other) :
  columns_(other.columns_),
  rows_(other.rows_),
  shapeArray_(std::make_unique<shape_ptr[]>(other.columns_))
{
  for (int i = 0; i < columns_; i++)
  {
    shapeArray_[i] = other.shapeArray_[i];
  }
}

lutsenko::Matrix::Matrix(lutsenko::Matrix && other) :
  columns_(other.columns_),
  rows_(other.rows_),
  shapeArray_(std::move(other.shapeArray_))
{
}

lutsenko::Matrix & lutsenko::Matrix::operator =(const lutsenko::Matrix & other)
{
  if (this != &other)
  {
    columns_ = other.columns_;
    rows_ = other.rows_;
    shape_array newArray = std::make_unique<shape_ptr[]>(other.columns_);
    for (int i = 0; i < columns_; i++)
    {
      newArray[i] = other.shapeArray_[i];
    }
    shapeArray_.swap(newArray);
  }

  return *this;
}

lutsenko::Matrix & lutsenko::Matrix::operator =(lutsenko::Matrix && other)
{
  if (this != &other)
  {
    columns_ = other.columns_;
    rows_ = other.rows_;
    shapeArray_ = std::move(other.shapeArray_);
  }

  return *this;
}

lutsenko::Matrix::Layer lutsenko::Matrix::operator [](int index) const
{
  if ((index < 0) || (index >= rows_))
  {
    throw std::invalid_argument("there is no element with this index");
  }
  int shapesOnLayerCounter = 0;
  for (int i = 0; i < columns_; i++)
  {
    if (shapeArray_[i]->getLayer() == index + 1)
    {
      shapesOnLayerCounter++;
    }
  }
  shape_array shapesOnLayerArray(std::make_unique<shape_ptr[]>(shapesOnLayerCounter));
  int j = 0;
  for (int i = 0; i < columns_; i++)
  {
    if (shapeArray_[i]->getLayer() == index + 1)
    {
      shapesOnLayerArray[j++] = shapeArray_[i];
    }
  }

  return Layer{shapesOnLayerArray, shapesOnLayerCounter};
}

bool lutsenko::Matrix::operator ==(const Matrix & other) const
{
  if ((columns_ != other.columns_) || (rows_ != other.rows_))
  {
    return false;
  }
  for (int i = 0; i < columns_; i++)
  {
    if (shapeArray_[i] != other.shapeArray_[i])
    {
      return false;
    }
  }

  return true;
}

bool lutsenko::Matrix::operator !=(const Matrix & other) const
{
  return (!(*this == other));
}

void lutsenko::Matrix::add(shape_ptr shape)//��������� ����� �� ������ ����
{
  if (!shape)
  {
    throw std::invalid_argument("can't add nullptr to the Matrix");
  }
  shape_array newArray = std::make_unique<shape_ptr[]>(columns_ + 1);
  shape->setLayer(1);
  if (columns_ > 0)
  { 
    for (int i = 0; i < columns_; i++)
    {
      newArray[i] = shapeArray_[i];
    }
    int tempLayer = newArray[0]->getLayer();
    bool flag = isCovering(newArray[0], shape);
    for (int i = 1; i < columns_; i++)
    {
      if (isCovering(newArray[i], shape))
      {
        flag = true;
        tempLayer = std::max(tempLayer, newArray[i]->getLayer());
      }
    }
    if (flag)
    {
      tempLayer == rows_ ? rows_++ : rows_;
      shape->setLayer(tempLayer + 1);
    }
  }
  else
  {
    rows_ = 1;
  }
  newArray[columns_++] = shape;
  shapeArray_.swap(newArray);
}

void lutsenko::Matrix::printInfo()
{
  if (columns_ == 0)
  {
    throw std::invalid_argument("Matrix is empty, can't printInfo");
  }
  for (int i = 0; i < columns_; i++)
  {
    std::cout << "   " << i + 1;
  }
  std::cout << '\n';
  for (int i = 1; i <= rows_; i++)
  {
    std::cout << i;
    std::cout << "  ";
    for (int j = 0; j < columns_; j++)
    {
      if (shapeArray_[j]->getLayer() == i)
      {
        std::cout << "*";
        std::cout << "   ";
      }
      else
      {
        std::cout << "    ";
      }
    }
    std::cout << '\n';
  }
}

lutsenko::Matrix::Layer::Layer(const shape_array & shapeArray, int size) :
  size_(size),
  shapesOnLayer_(std::make_unique<shape_ptr[]>(size))
{
  for (int i = 0; i < size; i++)
  {
    shapesOnLayer_[i] = shapeArray[i];
  }
}

lutsenko::shape_ptr & lutsenko::Matrix::Layer::operator[](int index) const
{
  if ((index < 0) || (index >= size_))
  {
    throw std::invalid_argument("there is no element with this index in this layer");
  }

  return shapesOnLayer_[index];
}
