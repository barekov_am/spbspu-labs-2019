#include "check.hpp"

bool lutsenko::isCovering(const shape_ptr & shape1, const shape_ptr & shape2)
{
  bool condition1 = true;
  bool condition2 = true;
  const rectangle_t rect1 = shape1->getFrameRect();
  const rectangle_t rect2 = shape2->getFrameRect();
  if (rect1.pos.y <= rect2.pos.y)
  {
    if (rect1.pos.y + rect1.height / 2 < rect2.pos.y - rect2.height / 2)
    {
      condition1 = false;
    }
  }
  else if (rect1.pos.y - rect1.height / 2 > rect2.pos.y + rect2.height / 2)
  {
    condition1 = false;
  }

  if (rect1.pos.x <= rect2.pos.x)
  {
    if (rect1.pos.x + rect1.width / 2 < rect2.pos.x - rect2.width / 2)
    {
      condition2 = false;
    }
  }
  else if (rect1.pos.x - rect1.width / 2 > rect2.pos.x + rect2.width / 2)
  {
    condition2 = false;
  }

  return condition1 && condition2;
}
