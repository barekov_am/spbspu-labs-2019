#define _USE_MATH_DEFINES
#include "rectangle.hpp"
#include <iostream>
#include <math.h>
#include <stdexcept>
#include <algorithm>

lutsenko::Rectangle::Rectangle(const rectangle_t & rect) :
  pos_(rect.pos),
  upperLeft_({pos_.x - rect.width / 2, pos_.y + rect.height / 2}),
  upperRight_({pos_.x + rect.width / 2, pos_.y + rect.height / 2}),
  bottomLeft_({pos_.x - rect.width / 2, pos_.y - rect.height / 2}),
  bottomRight_({pos_.x + rect.width / 2, pos_.y - rect.height / 2})
{
  if (rect.width <= 0)
  {
    throw std::invalid_argument("rectangle width should be greater than zero");
  }
  if (rect.height <= 0)
  {
    throw std::invalid_argument("rectangle height should be greater than zero");
  };
}

double lutsenko::Rectangle::getArea() const
{
  double width = sqrt(pow(upperRight_.x - upperLeft_.x, 2) + pow(upperRight_.y - upperLeft_.y, 2));
  double height = sqrt(pow(upperLeft_.x - bottomLeft_.x, 2) + pow(upperLeft_.y - bottomLeft_.y, 2));

  return (height * width);
}

lutsenko::rectangle_t lutsenko::Rectangle::getFrameRect() const
{
  double height = std::max(upperLeft_.y, upperRight_.y) - std::min(bottomLeft_.y, bottomRight_.y);
  double width = std::max(upperRight_.x, bottomRight_.x) - std::min(upperLeft_.x, bottomLeft_.x);

  return {width, height, pos_};
}

void lutsenko::Rectangle::move(const point_t & point)
{
  double dx = point.x - pos_.x;
  double dy = point.y - pos_.y;

  upperLeft_ = {upperLeft_.x + dx, upperLeft_.y + dy};
  upperRight_ = {upperRight_.x + dx, upperRight_.y + dy};

  bottomLeft_ = {bottomLeft_.x + dx, bottomLeft_.y + dy};
  bottomRight_ = {bottomRight_.x + dx, bottomRight_.y + dy};
  pos_ = point;
}

void lutsenko::Rectangle::move(double dx, double dy)
{
  move({pos_.x + dx, pos_.y + dy});
}

void lutsenko::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("coefficient should be greater than zero");
  }
  double dx = --coefficient * (upperRight_.x - pos_.x);
  double dy = coefficient * (upperRight_.y - pos_.y);

  upperLeft_ = {upperLeft_.x - dx, upperLeft_.y + dy};
  upperRight_ = {upperRight_.x + dx, upperRight_.y + dy};

  bottomLeft_ = {bottomLeft_.x - dx, bottomLeft_.y - dy};
  bottomRight_ = {bottomRight_.x + dx, bottomRight_.y - dy};
}

void lutsenko::Rectangle::rotate(double angle)
{
  angle *= M_PI / 180;
  double tempX = upperLeft_.x;
  upperLeft_.x = (upperLeft_.x - pos_.x) * cos(angle) - (upperLeft_.y - pos_.y) * sin(angle) + pos_.x;
  upperLeft_.y = (tempX - pos_.x) * sin(angle) + (upperLeft_.y - pos_.y) * cos(angle) + pos_.y;
  
  tempX = upperRight_.x;
  upperRight_.x = (upperRight_.x - pos_.x) * cos(angle) - (upperRight_.y - pos_.y) * sin(angle) + pos_.x,
  upperRight_.y = (tempX - pos_.x) * sin(angle) + (upperRight_.y - pos_.y) * cos(angle) + pos_.y;

  tempX = bottomLeft_.x;
  bottomLeft_.x = (bottomLeft_.x - pos_.x) * cos(angle) - (bottomLeft_.y - pos_.y) * sin(angle) + pos_.x,
  bottomLeft_.y = (tempX - pos_.x) * sin(angle) + (bottomLeft_.y - pos_.y) * cos(angle) + pos_.y;

  tempX = bottomRight_.x;
  bottomRight_.x = (bottomRight_.x - pos_.x) * cos(angle) - (bottomRight_.y - pos_.y) * sin(angle) + pos_.x,
  bottomRight_.y = (tempX - pos_.x) * sin(angle) + (bottomRight_.y - pos_.y) * cos(angle) + pos_.y;
}
