#ifndef ITERATOR_HPP
#define ITERATOR_HPP

#include <iterator>

class Iterator: public std::iterator<std::bidirectional_iterator_tag, size_t>
{
public:
    Iterator();
    Iterator(size_t);

    Iterator& operator ++();
    Iterator operator ++(int);
    Iterator& operator --();
    Iterator operator --(int);

    const size_t* operator ->();
    const size_t& operator *();
    bool operator ==(const Iterator &);
    bool operator !=(const Iterator &);

private:
    size_t position_, value_;
    size_t getValue(size_t number);
};

#endif
