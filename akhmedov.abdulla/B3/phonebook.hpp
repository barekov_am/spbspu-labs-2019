#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <list>
#include <string>

class PhoneBook
{
  public:
    struct record_t
    {
      std::string name;
      std::string number;
    };

    using iterator = std::list<record_t>::iterator;

    void print(iterator) const;
    void add(const record_t &);

    bool empty() const;

    iterator begin();
    iterator end();
    iterator next(iterator);
    iterator previous(iterator);
    iterator insert(iterator, const record_t &);
    iterator erase(iterator);
    iterator move(iterator, int);

  private:
    std::list<record_t> list_;
};

#endif
