#include "instructions.hpp"
#include <cstring>
#include <sstream>
#include <iostream>

BookmarkHandler::Insert getInsert(const char *ins)
{
  if (std::strcmp(ins, "after") == 0)
  {
    return BookmarkHandler::Insert::after;
  }

  if (std::strcmp(ins, "before") == 0)
  {
    return BookmarkHandler::Insert::before;
  }

  throw std::invalid_argument("Invalid location");
}

BookmarkHandler::Move getMove(const char *move)
{
  if (std::strcmp(move, "first") == 0)
  {
    return BookmarkHandler::Move::first;
  }

  if (std::strcmp(move, "last") == 0)
  {
    return BookmarkHandler::Move::last;
  }

  throw std::invalid_argument("Invalid location");
}

std::string getName(std::string &name)
{
  if (name.empty())
  {
    throw std::invalid_argument("Invalid name");
  }

  if (name.front() != '\"')
  {
    throw std::invalid_argument("Invalid name");
  }

  name.erase(name.begin());

  size_t i = 0;
  while ((i < name.size()) && (name[i] != '\"'))
  {
    if (name[i] == '\\')
    {
      if ((name[i + 1] == '\"') && (i + 2 < name.size()))
      {
        name.erase(i, 1);
      }
      else
      {
        throw std::invalid_argument("Invalid name");
      }
    }

    i++;
  }

  if (i == name.size())
  {
    throw std::invalid_argument("Invalid name");
  }

  name.erase(i);

  if (name.empty())
  {
    throw std::invalid_argument("Invalid name");
  }

  return name;
}

std::string getNumber(std::string &number)
{
  if (number.empty())
  {
    throw std::invalid_argument("Invalid number");
  }

  for (size_t i = 0; i < number.size(); i++)
  {
    if (!std::isdigit(number[i]))
    {
      throw std::invalid_argument("Invalid number");
    }
  }

  return number;
}

std::string getBookmarkName(std::string &bookmarkName)
{
  if (bookmarkName.empty())
  {
    throw std::invalid_argument("Invalid bookmark name");
  }

  for (size_t i = 0; i < bookmarkName.size(); i++)
  {
    if (!isalnum(bookmarkName[i]) && (bookmarkName[i] != '-'))
    {
      throw std::invalid_argument("Invalid bookmark name");
    }

  }

  return bookmarkName;
}


void add(BookmarkHandler &handler, std::stringstream &str)
{
  try
  {
    std::string number;
    str >> std::ws >> number;
    number = getNumber(number);

    std::string name;
    std::getline(str >> std::ws, name);
    name = getName(name);

    handler.add({number, name});
  }

  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID COMMAND>" << std::endl;

    return;
  }
}

void store(BookmarkHandler &handler, std::stringstream& str)
{
  try
  {
    std::string bookmarkName;
    str >> std::ws >> bookmarkName;
    bookmarkName = getBookmarkName(bookmarkName);

    std::string newBookmarkName;
    str >> std::ws >> newBookmarkName;
    newBookmarkName = getBookmarkName(newBookmarkName);

    handler.store(bookmarkName, newBookmarkName);
  }

  catch (std::invalid_argument &)
  {
    std::cout << "<INVALID COMMAND>" << std::endl;

    return;
  }
}

void insert(BookmarkHandler &handler, std::stringstream &str)
{
  try
  {
    std::string locate;
    str >> std::ws >> locate;
    auto loc = getInsert(locate.c_str());

    std::string bookmarkName;
    str >> std::ws >> bookmarkName;
    bookmarkName = getBookmarkName(bookmarkName);

    std::string number;
    str >> std::ws >> number;
    number = getNumber(number);

    std::string name;
    std::getline(str >> std::ws, name);
    name = getName(name);

    handler.insert(loc, bookmarkName, {number, name});
  }

  catch (std::invalid_argument&)
  {
    std::cout << "<INVALID COMMAND>" << std::endl;

    return;
  }
}

void eDelete(BookmarkHandler &handler, std::stringstream &str)
{
  try
  {
    std::string bookmarkName;
    str >> std::ws >> bookmarkName;
    bookmarkName = getBookmarkName(bookmarkName);

    handler.erase(bookmarkName);
  }

  catch (std::invalid_argument&)
  {
    std::cout << "<INVALID COMMAND>" << std::endl;

    return;
  }
}

void show(BookmarkHandler &handler, std::stringstream &str)
{
  try
  {
    std::string bookmarkName;
    str >> std::ws >> bookmarkName;
    bookmarkName = getBookmarkName(bookmarkName);

    handler.show(bookmarkName);
  }

  catch (std::invalid_argument&)
  {
    std::cout << "<INVALID COMMAND>" << std::endl;

    return;
  }
}

void move(BookmarkHandler &handler, std::stringstream &str)
{
  try
  {
    std::string bookmarkName;
    str >> std::ws >> bookmarkName;
    bookmarkName = getBookmarkName(bookmarkName);

    std::string steps;
    str >> std::ws >> steps;

    if ((steps.front() == '-') || (steps.front() == '+') || (std::isdigit(steps.front())))
    {
      int sign = 1;

      if (steps.front() == '-')
      {
        sign = -1;
        steps.erase(steps.begin());
      }
      else if (steps.front() == '+')
      {
        steps.erase(steps.begin());
      }

      steps = getNumber(steps);
      handler.move(bookmarkName, std::stoi(steps) * sign);
    }
    else
    {
      auto loc = getMove(steps.c_str());
      handler.move(bookmarkName, loc);
    }
  }

  catch (std::invalid_argument&)
  {
    std::cout << "<INVALID STEP>" << std::endl;

    return;
  }
}
