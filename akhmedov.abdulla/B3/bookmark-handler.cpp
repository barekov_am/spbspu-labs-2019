#include "bookmark-handler.hpp"
#include <algorithm>
#include <iostream>

BookmarkHandler::BookmarkHandler()
{
  bookmarks_["current"] = records_.begin();
}

void BookmarkHandler::add(const PhoneBook::record_t &rec)
{
  records_.add(rec);

  if (std::next(records_.begin()) == records_.end())
  {
    bookmarks_["current"] = records_.begin();
  }
}

void BookmarkHandler::store(const std::string &bookmark, const std::string &newBookmark)
{
  auto iter = getBookmarkIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    bookmarks_.emplace(newBookmark, iter->second);
  }
}

void BookmarkHandler::insert(BookmarkHandler::Insert ins, const std::string &bookmark, const PhoneBook::record_t &rec)
{
  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {
    if (iter->second == records_.end())
    {
      add(rec);
    }

    if (ins == Insert::after)
    {
      records_.insert(std::next(iter->second), rec);
    }
    else
    {
      records_.insert(iter->second, rec);
    }
  }
}

void BookmarkHandler::erase(const std::string &bookmark)
{
  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {
    auto eraseIter = iter->second;

    auto find = [&](auto &it)
    {
        if (it.second == eraseIter)
        {

          if (std::next(it.second) == records_.end())
          {
            it.second = records_.previous(eraseIter);
          }
          else
          {
            it.second = records_.next(eraseIter);
          }
        }
    };

    std::for_each(bookmarks_.begin(), bookmarks_.end(), find);

    records_.erase(eraseIter);
  }
}

void BookmarkHandler::show(const std::string &bookmark)
{
  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {

    if (records_.empty())
    {
      std::cout << "<EMPTY>" << std::endl;
      return;
    }

    records_.print(iter->second);
  }
}

void BookmarkHandler::move(const std::string &bookmark, int n)
{
  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {
    iter->second = records_.move(iter->second, n);
  }
}

void BookmarkHandler::move(const std::string &bookmark, Move move)
{
  auto iter = getBookmarkIterator(bookmark);

  if (iter != bookmarks_.end())
  {

    if (move == Move::first)
    {
      iter->second = records_.begin();
    }

    if (move == Move::last)
    {
      iter->second = records_.previous(records_.end());
    }
  }
}

BookmarkHandler::bookmarks::iterator BookmarkHandler::getBookmarkIterator(const std::string &bookmark)
{
  auto iter = bookmarks_.find(bookmark);

  if (iter != bookmarks_.end())
  {
    return iter;
  }
  else
  {
    std::cout << "<INVALID BOOKMARK>" << std::endl;
    return bookmarks_.end();
  }
}
