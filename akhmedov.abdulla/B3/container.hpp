#ifndef CONTAINER_HPP
#define CONTAINER_HPP

#include "iterator.hpp"

class Container
{
  public:
    static const int MAX_POSITION = 11;
    static const int MIN_POSITION = 1;

    Container() = default;
    Iterator begin();
    Iterator end();
};

#endif
