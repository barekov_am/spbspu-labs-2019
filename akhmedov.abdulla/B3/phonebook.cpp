#include "phonebook.hpp"
#include <iostream>

void PhoneBook::print(iterator iter) const
{
  std::cout << iter->name << " " << iter->number << std::endl;
}

void PhoneBook::add(const PhoneBook::record_t &rec)
{
  return list_.push_back(rec);
}

bool PhoneBook::empty() const
{
  return list_.empty();
}

PhoneBook::iterator PhoneBook::begin()
{
  return list_.begin();
}

PhoneBook::iterator PhoneBook::end()
{
  return list_.end();
}

PhoneBook::iterator PhoneBook::next(iterator iter)
{
  if (std::next(iter) != list_.end())
  {
    return ++iter;
  }
  else
  {
    return iter;
  }
}

PhoneBook::iterator PhoneBook::previous(iterator iter)
{
  if (iter != list_.begin())
  {
    return --iter;
  }
  else
  {
    return iter;
  }
}

PhoneBook::iterator PhoneBook::insert(iterator iter, const PhoneBook::record_t &rec)
{
  return list_.insert(iter, rec);
}

PhoneBook::iterator PhoneBook::erase(iterator iter)
{
  return list_.erase(iter);
}

PhoneBook::iterator PhoneBook::move(iterator iter, int n)
{
  if (n >= 0)
  {
    while (std::next(iter) != list_.end() && (n > 0))
    {
      iter = next(iter);
      --n;
    }
  }
  else
  {
    while (iter != list_.begin() && (n < 0))
    {
      iter = previous(iter);
      ++n;
    }
  }
  return iter;
}
