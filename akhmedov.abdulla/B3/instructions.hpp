#ifndef INSTRUCTIONS_HPP
#define INSTRUCTIONS_HPP

#include "bookmark-handler.hpp"
#include <string>

void add(BookmarkHandler &, std::stringstream &);
void store(BookmarkHandler &, std::stringstream &);
void insert(BookmarkHandler &, std::stringstream &);
void eDelete(BookmarkHandler &, std::stringstream &);
void show(BookmarkHandler &, std::stringstream &);
void move(BookmarkHandler &, std::stringstream &);

#endif
