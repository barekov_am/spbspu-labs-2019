#include "matrix.hpp"
#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(exceptionsTest)
{
  akhmedov::Shape::shPtr circle = std::make_shared<akhmedov::Circle>(0, 0, 7.8);
  akhmedov::Shape::shPtr rectangle1 = std::make_shared<akhmedov::Rectangle>(0, 0, 5, 5);
  akhmedov::Shape::shPtr rectangle2 = std::make_shared<akhmedov::Rectangle>(19, 19, 6, 10);
  akhmedov::Shape::shPtr rectangle3 = std::make_shared<akhmedov::Rectangle>(0, 0, 4, 4);
  akhmedov::Shape::shPtr rectangle4 = std::make_shared<akhmedov::Rectangle>(0, 0, 20, 20);
  akhmedov::Matrix matrix;
  matrix.add(circle, 0);
  matrix.add(rectangle1, 1);
  matrix.add(rectangle2, 0);
  matrix.add(rectangle3, 2);
  matrix.add(rectangle4, 2);

  BOOST_CHECK_THROW(matrix[5][5], std::out_of_range);
  BOOST_CHECK_THROW(matrix.add(nullptr, 0), std::invalid_argument);
  BOOST_CHECK_THROW(matrix.add(rectangle3, 5), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(moveAndCopyConstructors)
{
  akhmedov::Shape::shPtr circle = std::make_shared<akhmedov::Circle>(0, 0, 7.8);
  akhmedov::Shape::shPtr rectangle1 = std::make_shared<akhmedov::Rectangle>(0, 0, 5, 5);
  akhmedov::Shape::shPtr rectangle2 = std::make_shared<akhmedov::Rectangle>(19, 19, 6, 10);
  akhmedov::Shape::shPtr rectangle3 = std::make_shared<akhmedov::Rectangle>(0, 0, 4, 4);
  akhmedov::Shape::shPtr rectangle4 = std::make_shared<akhmedov::Rectangle>(0, 0, 20, 20);
  akhmedov::Matrix matrix;
  matrix.add(circle, 0);
  matrix.add(rectangle1, 1);
  matrix.add(rectangle2, 0);
  matrix.add(rectangle3, 2);
  matrix.add(rectangle4, 2);

  akhmedov::Matrix matrix1(matrix);
  BOOST_CHECK(matrix == matrix1);

  akhmedov::Matrix matrix2(std::move(matrix));
  BOOST_CHECK(matrix2 == matrix1);
}

BOOST_AUTO_TEST_CASE(moveAndCopyOperators)
{
  akhmedov::Shape::shPtr circle = std::make_shared<akhmedov::Circle>(0, 0, 7.8);
  akhmedov::Shape::shPtr rectangle1 = std::make_shared<akhmedov::Rectangle>(0, 0, 5, 5);
  akhmedov::Shape::shPtr rectangle2 = std::make_shared<akhmedov::Rectangle>(19, 19, 6, 10);
  akhmedov::Shape::shPtr rectangle3 = std::make_shared<akhmedov::Rectangle>(0, 0, 4, 4);
  akhmedov::Shape::shPtr rectangle4 = std::make_shared<akhmedov::Rectangle>(0, 0, 20, 20);
  akhmedov::Matrix matrix;
  matrix.add(circle, 0);
  matrix.add(rectangle1, 1);
  matrix.add(rectangle2, 0);
  matrix.add(rectangle3, 2);
  matrix.add(rectangle4, 2);

  akhmedov::Matrix matrix1, matrix2;
  matrix1 = matrix;
  BOOST_CHECK(matrix1 == matrix);

  matrix2 = std::move(matrix);
  BOOST_CHECK(matrix2 == matrix1);
}

BOOST_AUTO_TEST_SUITE_END()
