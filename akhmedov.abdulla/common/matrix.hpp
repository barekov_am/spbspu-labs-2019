#ifndef MATRIX_H
#define MATRIX_H
#include <memory>
#include "shape.hpp"

namespace akhmedov
{
  class Matrix
  {
    public:
      Matrix();
      Matrix(const Matrix &rhs);
      Matrix(Matrix &&rhs);
      ~Matrix() = default;

      Matrix &operator =(const Matrix &rhs);
      Matrix &operator =(Matrix &&rhs);
      Shape::shapesVector operator [](size_t row) const;
      bool operator ==(const Matrix &rhs) const;
      bool operator !=(const Matrix &rhs) const;

      size_t getRows() const;
      size_t getColumns() const;
      size_t getRowSize(size_t row) const;
      void add(Shape::shPtr shape, size_t row);

    private:
      size_t rows_;
      size_t columns_;
      Shape::shapesVector vector_;
  };
}
#endif
