#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace akhmedov
{
  struct point_t
  {
    double x, y;
  };

  struct rectangle_t
  {
    point_t pos;
    double width, height;
  };

  bool intersect(const rectangle_t &lhs, const rectangle_t &rhs);
}

#endif
