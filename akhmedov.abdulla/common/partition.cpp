#include "partition.hpp"

akhmedov::Matrix akhmedov::part(const Shape::shapesVector &source, size_t size)
{
  Matrix matrix;

  for (size_t i = 0; i < size; i++)
  {
    size_t row = 0;
    for (size_t j = 0; j < matrix.getRows(); j++)
    {
      bool intrsct = false;
      for (size_t k = 0; k < matrix.getColumns(); k++)
      {
        if (matrix[j][k] != nullptr)
        {
          if (intersect(source[i]->getFrameRect(), matrix[j][k]->getFrameRect()))
          {
            row++;
            intrsct = true;
            break;
          }
        }
      }
      if (!intrsct)
      {
        break;
      }
    }
    matrix.add(source[i], row);
  }
  return matrix;
}
akhmedov::Matrix akhmedov::part(const CompositeShape &source)
{
  return part(source.vector(), source.getSize());
}
