#include "partition.hpp"
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "rectangle.hpp"

BOOST_AUTO_TEST_CASE(PartitionTest)
{
  akhmedov::Shape::shPtr Circle = std::make_shared<akhmedov::Circle>(0, 0, 7.8);
  akhmedov::Shape::shPtr Rectangle1 = std::make_shared<akhmedov::Rectangle>(0, 0, 5, 5);
  akhmedov::Shape::shPtr Rectangle2 = std::make_shared<akhmedov::Rectangle>(19, 19, 6, 10);
  akhmedov::Shape::shPtr Rectangle3 = std::make_shared<akhmedov::Rectangle>(0, 0, 4, 4);
  akhmedov::Shape::shPtr Rectangle4 = std::make_shared<akhmedov::Rectangle>(0, 0, 20, 20);
  akhmedov::CompositeShape composite;
  composite.add(Rectangle3);
  composite.add(Rectangle4);
  akhmedov::Shape::shPtr Composite1 = std::make_shared<akhmedov::CompositeShape>(composite);

  akhmedov::CompositeShape Composite2;
  Composite2.add(Circle);
  Composite2.add(Rectangle1);
  Composite2.add(Rectangle2);
  Composite2.add(Composite1);

  akhmedov::Matrix Matrix = akhmedov::part(Composite2);

  BOOST_REQUIRE_EQUAL(Matrix.getRows(), 3);
  BOOST_REQUIRE_EQUAL(Matrix.getColumns(), 2);

  BOOST_CHECK(Matrix[0][0] == Circle);
  BOOST_CHECK(Matrix[0][1] == Rectangle2);
  BOOST_CHECK(Matrix[1][0] == Rectangle1);
  BOOST_CHECK(Matrix[2][0] == Composite1);
}
