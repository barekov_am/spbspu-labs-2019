#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

const double FULLCIRCLE = 360;

akhmedov::Rectangle::Rectangle(const akhmedov::point_t &pos, double width, double height, double angle):
  pos_(pos),
  width_(width),
  height_(height),
  angle_(angle)
{
  if ((width <= 0) || (height <= 0))
  {
    throw std::invalid_argument("Arguments width and height are surely invalid");
  }
}

akhmedov::Rectangle::Rectangle(const point_t &pos, double width, double height):
  Rectangle(pos, width, height, 0)
{ }

akhmedov::Rectangle::Rectangle(double x, double y, double width, double height):
  Rectangle({ x, y }, width, height)
{ }

double akhmedov::Rectangle::getArea() const
{
  return width_ * height_;
}

akhmedov::rectangle_t akhmedov::Rectangle::getFrameRect() const
{
  const double sinA = sin(angle_ * M_PI / 180);
  const double cosA = cos(angle_ * M_PI / 180);
  const double width = width_ * cosA + height_ * sinA;
  const double height = width_ * sinA + height_ * cosA;
  return { pos_, width, height };
}

void akhmedov::Rectangle::move(const point_t &pos)
{
  pos_ = pos;
}

void akhmedov::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void akhmedov::Rectangle::printInfo() const
{
  std::cout << "X center is: " << pos_.x << std::endl;
  std::cout << "Y center is: " << pos_.y << std::endl;
  std::cout << "Width is: " << width_ << std::endl;
  std::cout << "Height is: " << height_ << std::endl;
}

akhmedov::point_t akhmedov::Rectangle::getCenter() const
{
  return pos_;
}

void akhmedov::Rectangle::scale(double multiplyCoefficient)
{
  if (multiplyCoefficient <= 0)
  {
    throw std::invalid_argument("Multiply Coefficient is surely invalid");
  }
  width_ *= multiplyCoefficient;
  height_ *= multiplyCoefficient;
}

double akhmedov::Rectangle::getWidth() const
{
  return width_;
}

double akhmedov::Rectangle::getHeight() const
{
  return height_;
}

double akhmedov::Rectangle::getAngle() const
{
  return angle_;
}

void akhmedov::Rectangle::rotate(double angle)
{
  angle_ += angle;
  if (fabs(angle_) >= FULLCIRCLE)
  {
    angle_ = fmod(angle_, FULLCIRCLE);
  }
}
