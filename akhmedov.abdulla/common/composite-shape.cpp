#include "composite-shape.hpp"
#include <utility>
#include <iostream>
#include <stdexcept>
#include <cmath>

akhmedov::CompositeShape::CompositeShape():
  size_(0)
{ }

akhmedov::CompositeShape::CompositeShape(const CompositeShape &rhs):
  size_(rhs.size_),
  shapesVector_(std::make_unique<shPtr[]>(rhs.size_))
{
  for (size_t i = 0; i < rhs.size_; i++)
  {
    shapesVector_[i] = rhs.shapesVector_[i];
  }
}

akhmedov::CompositeShape::CompositeShape(CompositeShape &&rhs):
  size_(rhs.size_),
  shapesVector_(std::move(rhs.shapesVector_))
{
  rhs.size_ = 0;
}

akhmedov::CompositeShape& akhmedov::CompositeShape::operator =(const akhmedov::CompositeShape &rhs)
{
  if (this == &rhs)
  {
    return *this;
  }
  shapesVector tmp(std::make_unique<shPtr[]>(rhs.size_));
  size_ = rhs.size_;
  for (size_t i = 0; i < rhs.size_; i++)
  {
    tmp[i] = rhs.shapesVector_[i];
  }
  shapesVector_.swap(tmp);
  return *this;
}

akhmedov::CompositeShape& akhmedov::CompositeShape::operator =(akhmedov::CompositeShape &&rhs)
{
  if (this == &rhs)
  {
    return *this;
  }
  size_ = rhs.size_;
  shapesVector_ = std::move(rhs.shapesVector_);
  rhs.size_ = 0;
  return *this;
}

akhmedov::CompositeShape::shPtr akhmedov::CompositeShape::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Unable to get shape: index is out of range");
  }
  return shapesVector_[index];
}

double akhmedov::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < size_; i++)
  {
    area += shapesVector_[i]->getArea();
  }
  return area;
}

bool akhmedov::CompositeShape::operator ==(const CompositeShape &rhs) const
{
  if (size_ != rhs.size_)
  {
    return false;
  }

  for (size_t i = 0; i < size_; i++)
  {
    if (shapesVector_[i] != rhs.shapesVector_[i])
    {
      return false;
    }
  }

  return true;
}

bool akhmedov::CompositeShape::operator !=(const CompositeShape &rhs) const
{
  return !(*this == rhs);
}


akhmedov::rectangle_t akhmedov::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Array is empty");
  }
  rectangle_t nullRect = shapesVector_[0]->getFrameRect();
  double xMin = nullRect.pos.x - nullRect.width / 2;
  double xMax = xMin + nullRect.width;
  double yMin = nullRect.pos.y - nullRect.height / 2;
  double yMax = yMin + nullRect.height;
  for (size_t i = 1; i < size_; i++)
  {
    rectangle_t tmp = shapesVector_[i]->getFrameRect();
    xMin = std::min(xMin, tmp.pos.x - tmp.width / 2);
    xMax = std::max(xMax, tmp.pos.x + tmp.width / 2);
    yMin = std::min(yMin, tmp.pos.y - tmp.height / 2);
    yMax = std::max(yMax, tmp.pos.y + tmp.height / 2);
  }
  return {{ (xMax + xMin) / 2, (yMax + yMin) / 2 }, xMax - xMin, yMax - yMin};
}

void akhmedov::CompositeShape::move(const akhmedov::point_t &point)
{
  const double dx = point.x - getCenter().x;
  const double dy = point.y - getCenter().y;
  move(dx, dy);
}

void akhmedov::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; i++)
  {
    shapesVector_[i]->move(dx, dy);
  }
}

void akhmedov::CompositeShape::printInfo() const
{
  std::cout << "X center is: " << getCenter().x << std::endl;
  std::cout << "Y center is: " << getCenter().y << std::endl;
  std::cout << "Amount of figures is: " << size_ << std::endl;
}

akhmedov::point_t akhmedov::CompositeShape::getCenter() const
{
  return getFrameRect().pos;
}

void akhmedov::CompositeShape::scale(double multiplyCoefficient)
{
  if (multiplyCoefficient <= 0)
  {
    throw std::invalid_argument("Multiply Coefficient is surely invalid");
  }
  point_t point = getCenter();
  for (size_t i = 0; i < size_; i++)
  {
    double dx = point.x - shapesVector_[i]->getCenter().x;
    double dy = point.y - shapesVector_[i]->getCenter().y;
    shapesVector_[i]->move(dx * (multiplyCoefficient - 1), dy * (multiplyCoefficient - 1));
    shapesVector_[i]->scale(multiplyCoefficient);
  }
}

void akhmedov::CompositeShape::add(shPtr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer must not be null");
  }
  shapesVector tmp(std::make_unique<shPtr[]>(size_ + 1));
  for (size_t i = 0; i < size_; i++)
  {
    tmp[i] = shapesVector_[i];
  }
  tmp[size_] = shape;
  size_++;
  shapesVector_.swap(tmp);
}

void akhmedov::CompositeShape::remove(size_t index)
{
  if (index >= size_)
  {
    throw std::out_of_range("Unable to delete a shape: index is out of range");
  }
  for (size_t i = index; i < (size_ - 1); i++)
  {
    shapesVector_[i] = shapesVector_[i + 1];
  }
  size_--;
}

size_t akhmedov::CompositeShape::getSize() const
{
  return size_;
}

void akhmedov::CompositeShape::rotate(double angle)
{
  const point_t compCenter = getFrameRect().pos;
  const double sinA = sin(angle * M_PI / 180);
  const double cosA = cos(angle * M_PI / 180);
  for (size_t i = 0; i < size_; i++)
  {
    const point_t shCenter = shapesVector_[i]->getFrameRect().pos;
    const double dx = (shCenter.x - compCenter.x) * cosA - (shCenter.y - compCenter.y) * sinA;
    const double dy = (shCenter.x - compCenter.x) * sinA + (shCenter.y - compCenter.y) * cosA;
    shapesVector_[i]->move({ compCenter.x + dx, compCenter.y + dy });
    shapesVector_[i]->rotate(angle);
  }
}

akhmedov::CompositeShape::shapesVector akhmedov::CompositeShape::vector() const
{
  shapesVector tmp(std::make_unique<shPtr[]>(size_));
  for (size_t i = 0; i < size_; i++)
  {
    tmp[i] = shapesVector_[i];
  }
  return tmp;
}
