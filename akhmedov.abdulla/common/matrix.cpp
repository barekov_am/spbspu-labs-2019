#include "matrix.hpp"
#include <stdexcept>
#include <utility>
#include <iostream>

akhmedov::Matrix::Matrix():
  rows_(0),
  columns_(0)
{ }

akhmedov::Matrix::Matrix(const akhmedov::Matrix &rhs):
  rows_(rhs.rows_),
  columns_(rhs.columns_),
  vector_(std::make_unique<Shape::shPtr[]>(rhs.rows_ * rhs.columns_))
{
  for (size_t i = 0; i < (rows_ * columns_) ; i++)
  {
    vector_[i] = rhs.vector_[i];
  }
}

akhmedov::Matrix::Matrix(akhmedov::Matrix &&rhs):
  rows_(rhs.rows_),
  columns_(rhs.columns_),
  vector_(std::move(rhs.vector_))
{
  rhs.rows_ = 0;
  rhs.columns_ = 0;
}

akhmedov::Matrix& akhmedov::Matrix::operator =(const akhmedov::Matrix &rhs)
{
  if (this == &rhs)
  {
    return  *this;
  }
  Shape::shapesVector tmp(std::make_unique<Shape::shPtr[]>(rhs.rows_ * rhs.columns_));
  for (size_t i = 0; i < (rhs.rows_ * rhs.columns_); i++)
  {
    tmp[i] = rhs.vector_[i];
  }
  rows_ = rhs.rows_;
  columns_ = rhs.columns_;
  vector_.swap(tmp);
  return *this;
}

akhmedov::Matrix& akhmedov::Matrix::operator =(akhmedov::Matrix &&rhs)
{
  if (this == &rhs)
  {
    return *this;
  }
  rows_ = rhs.rows_;
  columns_ = rhs.columns_;
  rhs.rows_ = 0;
  rhs.columns_ = 0;
  vector_ = std::move(rhs.vector_);
  return *this;
}

akhmedov::Shape::shapesVector akhmedov::Matrix::operator [](size_t row) const
{
  if (row >= rows_)
  {
    throw std::out_of_range("Index is out of range");
  }
  Shape::shapesVector tmp = std::make_unique<Shape::shPtr[]>(getRowSize(row));
  for (size_t i = 0; i < getRowSize(row); i++)
  {
    tmp[i] = vector_[row * columns_ + i];
  }
  return tmp;
}

bool akhmedov::Matrix::operator ==(const akhmedov::Matrix &rhs) const
{
  if ((rows_ != rhs.rows_) || (columns_ != rhs.columns_))
  {
    return false;
  }
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (vector_[i] != rhs.vector_[i])
    {
      return false;
    }
  }
  return true;
}

bool akhmedov::Matrix::operator !=(const akhmedov::Matrix &rhs) const
{
  return !(*this == rhs);
}

size_t akhmedov::Matrix::getRows() const
{
  return rows_;
}

size_t akhmedov::Matrix::getColumns() const
{
  return columns_;
}

size_t akhmedov::Matrix::getRowSize(size_t row) const
{
  if (row >= rows_)
  {
    return 0;
  }
  for (size_t i = 0; i < columns_; i++)
  {
    if (vector_[row * columns_ + i] == nullptr)
    {
      return i;
    }
  }
  return columns_;
}

void akhmedov::Matrix::add(Shape::shPtr shape, size_t row)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape pointer must not be null");
  }
  if (row > rows_)
  {
    throw std::out_of_range("Index is out of range");
  }

  size_t tmpRows = (row >= rows_) ? (rows_ + 1) : rows_;
  size_t tmpColumns = (getRowSize(row) == columns_) ? (columns_ + 1) : columns_;
  size_t column = (row < rows_) ? getRowSize(row) : 0;

  Shape::shapesVector tmpVector(std::make_unique<Shape::shPtr[]>(tmpRows * tmpColumns));

  for (size_t i = 0; i < tmpRows; i++)
  {
    for (size_t j = 0; j < tmpColumns; j++)
    {
      if ((i == rows_) || (j == columns_))
      {
        tmpVector[i * tmpColumns + j] = nullptr;
      }
      else
      {
        tmpVector[i * tmpColumns + j] = vector_[i * columns_ + j];
      }
    }
  }

  if (row < rows_)
  {
    tmpVector[row * tmpColumns + column] = shape;
  }
  else
  {
    tmpVector[rows_ * tmpColumns + column] = shape;
  }

  vector_.swap(tmpVector);
  rows_ = tmpRows;
  columns_ = tmpColumns;
}
