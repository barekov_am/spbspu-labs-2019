#include "composite-shape.hpp"
#include <stdexcept>
#include <utility>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include <cmath>

const double ACCURACY = 0.00001;
const double FULLCIRCLE = 360;

BOOST_AUTO_TEST_SUITE(compositeShapeTest)

BOOST_AUTO_TEST_CASE(consistencyOfWidthHeightArea)
{
  akhmedov::Shape::shPtr Circle1 = std::make_shared<akhmedov::Circle>(0, 0, 8);
  akhmedov::Shape::shPtr Rectangle1 = std::make_shared<akhmedov::Rectangle>(3, 3.8, 9, 1);
  akhmedov::Shape::shPtr Rectangle2 = std::make_shared<akhmedov::Rectangle>(0.534, -6.4, 20, 20);
  akhmedov::CompositeShape compSh1;
  compSh1.add(Circle1);
  compSh1.add(Rectangle1);
  compSh1.add(Rectangle2);
  const double areaB = compSh1.getArea();
  const double widthB = compSh1.getFrameRect().width;
  const double heightB = compSh1.getFrameRect().height;

  compSh1.move(20000, 200);
  BOOST_CHECK_CLOSE(areaB, compSh1.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(widthB, compSh1.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(heightB, compSh1.getFrameRect().height, ACCURACY);

  compSh1.move({ 0, 0 });
  BOOST_CHECK_CLOSE(areaB, compSh1.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(widthB, compSh1.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(heightB, compSh1.getFrameRect().height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(correctScaling)
{
  akhmedov::Shape::shPtr Circle1 = std::make_shared<akhmedov::Circle>(0, 0, 8);
  akhmedov::Shape::shPtr Rectangle1 = std::make_shared<akhmedov::Rectangle>(3, 3.8, 9, 1);
  akhmedov::Shape::shPtr Rectangle2 = std::make_shared<akhmedov::Rectangle>(0.534, -6.4, 20, 20);
  akhmedov::CompositeShape compSh1;
  compSh1.add(Circle1);
  compSh1.add(Rectangle1);
  compSh1.add(Rectangle2);
  akhmedov::CompositeShape compSh2(compSh1);
  const double multiplyCoefficient1 = 7.836;
  const double multiplyCoefficient2 = 0.836;
  double areaB = compSh1.getArea();

  compSh1.scale(multiplyCoefficient1);
  BOOST_CHECK_CLOSE(areaB * multiplyCoefficient1 * multiplyCoefficient1, compSh1.getArea(), ACCURACY);

  areaB = compSh2.getArea();
  compSh2.scale(multiplyCoefficient2);
  BOOST_CHECK_CLOSE(areaB * multiplyCoefficient2 * multiplyCoefficient2, compSh2.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(argumentsExceptionsTest)
{
  akhmedov::Shape::shPtr Circle1 = std::make_shared<akhmedov::Circle>(0, 0, 8);
  akhmedov::Shape::shPtr Rectangle1 = std::make_shared<akhmedov::Rectangle>(3, 3.8, 9, 1);
  akhmedov::Shape::shPtr Rectangle2 = std::make_shared<akhmedov::Rectangle>(0.534, -6.4, 20, 20);
  akhmedov::CompositeShape compSh1;
  BOOST_CHECK_THROW(compSh1.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(compSh1.move({ 2, 5 }), std::logic_error);
  BOOST_CHECK_THROW(compSh1.getCenter(), std::logic_error);
  BOOST_CHECK_THROW(compSh1.scale(7), std::logic_error);
  BOOST_CHECK_THROW(compSh1.remove(0), std::logic_error);
  BOOST_CHECK_THROW(compSh1[3], std::out_of_range);
  BOOST_CHECK_THROW(compSh1[0], std::out_of_range);

  compSh1.add(Circle1);
  compSh1.add(Rectangle1);
  compSh1.add(Rectangle2);
  BOOST_CHECK_THROW(compSh1[4], std::out_of_range);
  BOOST_CHECK_THROW(compSh1[-1], std::out_of_range);
  BOOST_CHECK_THROW(compSh1.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compSh1.remove(5), std::out_of_range);

  compSh1.remove(2);
  compSh1.remove(1);
  BOOST_CHECK_THROW(compSh1.remove(2), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(moveAndCopy)
{
  akhmedov::Shape::shPtr Circle1 = std::make_shared<akhmedov::Circle>(0, 0, 8);
  akhmedov::Shape::shPtr Rectangle1 = std::make_shared<akhmedov::Rectangle>(3, 3.8, 9, 1);
  akhmedov::Shape::shPtr Rectangle2 = std::make_shared<akhmedov::Rectangle>(0.534, -6.4, 20, 20);
  akhmedov::CompositeShape compSh1;
  compSh1.add(Circle1);
  compSh1.add(Rectangle1);
  compSh1.add(Rectangle2);
  const double areaB = compSh1.getArea();
  const akhmedov::rectangle_t frameRectB = compSh1.getFrameRect();
  const double areaFrameRectB = frameRectB.width * frameRectB.height;

  akhmedov::CompositeShape compSh2(compSh1);
  BOOST_CHECK_CLOSE(areaB, compSh2.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(areaFrameRectB, compSh2.getFrameRect().width * compSh2.getFrameRect().height, ACCURACY);

  akhmedov::CompositeShape compSh3(std::move(compSh2));
  BOOST_CHECK_CLOSE(areaB, compSh3.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(areaFrameRectB, compSh3.getFrameRect().width * compSh3.getFrameRect().height, ACCURACY);

  akhmedov::CompositeShape compSh4;
  compSh4 = compSh3;
  BOOST_CHECK_CLOSE(areaB, compSh4.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(areaFrameRectB, compSh4.getFrameRect().width * compSh4.getFrameRect().height, ACCURACY);

  akhmedov::CompositeShape compSh5;
  compSh5 = std::move(compSh4);
  BOOST_CHECK_CLOSE(areaB, compSh5.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(areaFrameRectB, compSh5.getFrameRect().width * compSh5.getFrameRect().height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(rotateTest)
{
  akhmedov::Shape::shPtr Circle1 = std::make_shared<akhmedov::Circle>(0, 0, 8);
  akhmedov::Shape::shPtr Rectangle1 = std::make_shared<akhmedov::Rectangle>(3, 3.8, 9, 1);
  akhmedov::Shape::shPtr Rectangle2 = std::make_shared<akhmedov::Rectangle>(0.534, -6.4, 20, 20);
  akhmedov::CompositeShape compSh1;
  compSh1.add(Circle1);
  compSh1.add(Rectangle1);
  const akhmedov::point_t compCen = compSh1.getCenter();
  const double angle = FULLCIRCLE / 8;
  const double sinA = sin(angle * M_PI / 180);
  const double cosA = cos(angle * M_PI / 180);

  const double newX1 = compCen.x + (Circle1->getCenter().x - compCen.x) * cosA - (Circle1->getCenter().y - compCen.y) * sinA;
  const double newY1 = compCen.y + (Circle1->getCenter().x - compCen.x) * sinA + (Circle1->getCenter().y - compCen.y) * cosA;

  const double newX2 = compCen.x + (Rectangle1->getCenter().x - compCen.x) * cosA - (Rectangle1->getCenter().y - compCen.y) * sinA;
  const double newY2 = compCen.y + (Rectangle1->getCenter().x - compCen.x) * sinA + (Rectangle1->getCenter().y - compCen.y) * cosA;

  const double areaB = compSh1.getArea();
  const akhmedov::rectangle_t frameRectB = compSh1.getFrameRect();

  compSh1.rotate(FULLCIRCLE);

  BOOST_CHECK_CLOSE(areaB, compSh1.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(frameRectB.width, compSh1.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectB.height, compSh1.getFrameRect().height, ACCURACY);

  compSh1.rotate(90);
  BOOST_CHECK_CLOSE(frameRectB.width, compSh1.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectB.height, compSh1.getFrameRect().width, ACCURACY);

  compSh1.rotate(FULLCIRCLE - 90);
  BOOST_CHECK_CLOSE(areaB, compSh1.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(frameRectB.width, compSh1.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectB.height, compSh1.getFrameRect().height, ACCURACY);

  compSh1.rotate(angle);
  BOOST_CHECK_CLOSE(compSh1[0]->getCenter().x, newX1, ACCURACY);
  BOOST_CHECK_CLOSE(compSh1[0]->getCenter().y, newY1, ACCURACY);
  BOOST_CHECK_CLOSE(compSh1[1]->getCenter().x, newX2, ACCURACY);
  BOOST_CHECK_CLOSE(compSh1[1]->getCenter().y, newY2, ACCURACY);
}

BOOST_AUTO_TEST_CASE(vectorTest)
{
  akhmedov::Shape::shPtr Circle1 = std::make_shared<akhmedov::Circle>(0, 0, 8);
  akhmedov::Shape::shPtr Rectangle1 = std::make_shared<akhmedov::Rectangle>(3, 3.8, 9, 1);
  akhmedov::Shape::shPtr Rectangle2 = std::make_shared<akhmedov::Rectangle>(0.534, -6.4, 20, 20);
  akhmedov::CompositeShape compSh1;
  akhmedov::Shape::shapesVector tmp = std::make_unique<akhmedov::Shape::shPtr[]>(3);
  compSh1.add(Circle1);
  compSh1.add(Rectangle1);
  compSh1.add(Rectangle2);

  tmp = compSh1.vector();
  BOOST_CHECK(tmp[0] == Circle1);
  BOOST_CHECK(tmp[1] == Rectangle1);
  BOOST_CHECK(tmp[2] == Rectangle2);
}

BOOST_AUTO_TEST_SUITE_END()
