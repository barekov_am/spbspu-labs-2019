#include "rectangle.hpp"
#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <cmath>

const double ACCURACY = 0.0001;
const double FULLCIRCLE = 360;

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(consistencyOfWidthHeightArea)
{
  akhmedov::Rectangle exampleRect({ 0, 0 }, 3.18, 9.6875);
  const double widthB = exampleRect.getWidth();
  const double heightB = exampleRect.getHeight();
  const double areaB = exampleRect.getArea();

  exampleRect.move(4, 5);
  BOOST_CHECK_CLOSE(widthB, exampleRect.getWidth(), ACCURACY);
  BOOST_CHECK_CLOSE(heightB, exampleRect.getHeight(), ACCURACY);
  BOOST_CHECK_CLOSE(areaB, exampleRect.getArea(), ACCURACY);

  exampleRect.move({ 6, 8.29 });
  BOOST_CHECK_CLOSE(widthB, exampleRect.getWidth(), ACCURACY);
  BOOST_CHECK_CLOSE(heightB, exampleRect.getHeight(), ACCURACY);
  BOOST_CHECK_CLOSE(areaB, exampleRect.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(correctScaling)
{
  akhmedov::Rectangle exampleRect({ 0, 0 }, 3.18, 9.6875);
  const double multiplyCoefficient = 7.836;
  const double areaB = exampleRect.getArea();

  exampleRect.scale(multiplyCoefficient);
  BOOST_CHECK_CLOSE(areaB * multiplyCoefficient * multiplyCoefficient, exampleRect.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(argumentsExceptionsTest)
{
  BOOST_CHECK_THROW(akhmedov::Rectangle({ 0, 0 }, -3, 4), std::invalid_argument);
  BOOST_CHECK_THROW(akhmedov::Rectangle({ 0, 0 }, 3, -4), std::invalid_argument);
  BOOST_CHECK_THROW(akhmedov::Rectangle({ 0, 0 }, -3, -4), std::invalid_argument);

  akhmedov::Rectangle exampleRectangle({ 0, 0 }, 3, 4);
  BOOST_CHECK_THROW(exampleRectangle.scale(-5), std::invalid_argument);
  BOOST_CHECK_THROW(exampleRectangle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rotateTest)
{
  akhmedov::Rectangle rectangle1({ 5, 6 }, 4 , 6);
  const double angle1 = 643;
  const double angle2 = -878;
  const double angle3 = 45;
  const double sinA = sin(angle3 * M_PI / 180);
  const double cosA = cos(angle3 * M_PI / 180);
  const double frameHeight = rectangle1.getHeight() * sinA + rectangle1.getWidth() * cosA;
  const double frameWidth = rectangle1.getWidth() * sinA + rectangle1.getHeight() * cosA;

  rectangle1.rotate(FULLCIRCLE);
  BOOST_CHECK_EQUAL(rectangle1.getAngle(), 0);

  rectangle1.rotate(angle1);
  BOOST_CHECK_EQUAL(rectangle1.getAngle(), fmod(angle1, FULLCIRCLE));

  rectangle1.rotate(-rectangle1.getAngle());
  rectangle1.rotate(angle2);
  BOOST_CHECK_EQUAL(rectangle1.getAngle(), fmod(angle2, FULLCIRCLE));
  rectangle1.rotate(-angle2);

  rectangle1.rotate(angle3);
  BOOST_CHECK_CLOSE(rectangle1.getFrameRect().height, frameHeight, ACCURACY);
  BOOST_CHECK_CLOSE(rectangle1.getFrameRect().width, frameWidth, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
