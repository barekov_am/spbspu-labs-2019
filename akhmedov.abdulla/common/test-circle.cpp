#include "circle.hpp"
#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

const double ACCURACY = 0.0001;

BOOST_AUTO_TEST_SUITE(circleTest)

BOOST_AUTO_TEST_CASE(consistencyOfWidthHeightArea)
{
  akhmedov::Circle exampleCircle({ 0, 0 }, 8.9756);
  akhmedov::rectangle_t circumscribedRectangle = exampleCircle.getFrameRect();
  const double widthB = circumscribedRectangle.width;
  const double heightB = circumscribedRectangle.height;
  const double areaB = exampleCircle.getArea();
  const double radiusB = exampleCircle.getRadius();

  exampleCircle.move({ 25, 3.569 });
  circumscribedRectangle = exampleCircle.getFrameRect();
  BOOST_CHECK_CLOSE(widthB, circumscribedRectangle.width, ACCURACY);
  BOOST_CHECK_CLOSE(heightB, circumscribedRectangle.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaB, exampleCircle.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(radiusB, exampleCircle.getRadius(), ACCURACY);

  exampleCircle.move(5.678, 8);
  circumscribedRectangle = exampleCircle.getFrameRect();
  BOOST_CHECK_CLOSE(widthB, circumscribedRectangle.width, ACCURACY);
  BOOST_CHECK_CLOSE(heightB, circumscribedRectangle.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaB, exampleCircle.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(radiusB, exampleCircle.getRadius(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(correctScaling)
{
  akhmedov::Circle exampleCircle({ 0, 0 }, 7);
  const double multiplyCoefficient = 6.576;
  const double areaB = exampleCircle.getArea();

  exampleCircle.scale(multiplyCoefficient);
  BOOST_CHECK_CLOSE(areaB * multiplyCoefficient * multiplyCoefficient, exampleCircle.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(argumentsExceptionsTest)
{
  BOOST_CHECK_THROW(akhmedov::Circle({ 0, 0 }, -4), std::invalid_argument);
  BOOST_CHECK_THROW(akhmedov::Circle({ 0, 0 }, 0), std::invalid_argument);

  akhmedov::Circle exampleCircle({ 0, 0 }, 3);
  BOOST_CHECK_THROW(exampleCircle.scale(-5), std::invalid_argument);
  BOOST_CHECK_THROW(exampleCircle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
