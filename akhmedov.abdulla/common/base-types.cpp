#include "base-types.hpp"

bool akhmedov::intersect(const akhmedov::rectangle_t &lhs, const akhmedov::rectangle_t &rhs)
{
  // TL means Top Left Spot and BR means Bottom Right spot
  const point_t lhsTL = {lhs.pos.x - lhs.width / 2, lhs.pos.y + lhs.height / 2};
  const point_t rhsTL = {rhs.pos.x - rhs.width / 2, rhs.pos.y + rhs.height / 2};
  const point_t lhsBR = {lhs.pos.x + lhs.width / 2, lhs.pos.y - lhs.height / 2};
  const point_t rhsBR = {rhs.pos.x + rhs.width / 2, rhs.pos.y - rhs.height / 2};
  const bool firstCond = (lhsTL.x < rhsBR.x) && (lhsTL.y > rhsBR.y);
  const bool secondCond = (lhsBR.x > rhsTL.x) && (lhsBR.y < rhsTL.y);
  return firstCond && secondCond;
}
