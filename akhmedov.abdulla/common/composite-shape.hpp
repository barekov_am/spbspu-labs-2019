#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include "shape.hpp"

namespace akhmedov
{
  class CompositeShape : public Shape
  {
    public:
      CompositeShape();
      CompositeShape(const CompositeShape &rhs);
      CompositeShape(CompositeShape &&rhs);
      ~CompositeShape() = default;

      CompositeShape &operator =(const CompositeShape &rhs);
      CompositeShape &operator =(CompositeShape &&rhs);
      shPtr operator [](size_t index) const;
      bool operator ==(const CompositeShape &rhs) const;
      bool operator !=(const CompositeShape &rhs) const;

      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const point_t &point) override;
      void move(double dx, double dy) override;
      void printInfo() const override;
      point_t getCenter() const override;
      void scale(double multiplyCoefficient) override;
      void add(shPtr shape);
      void remove(size_t index);
      size_t getSize() const;
      void rotate(double angle) override;
      shapesVector vector() const;

    private:
      size_t size_;
      shapesVector shapesVector_;
  };
}

#endif
