#ifndef PARTITION_HPP
#define PARTITION_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace akhmedov
{
  akhmedov::Matrix part(const Shape::shapesVector &source, size_t size);
  akhmedov::Matrix part(const akhmedov::CompositeShape &source);
}

#endif
