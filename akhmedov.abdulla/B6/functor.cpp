#include "functor.hpp"
#include <limits>

Functor::Functor():
  min_(std::numeric_limits<int>::max()),
  max_(std::numeric_limits<int>::min()),
  count_(0),
  positive_(0),
  negative_(0),
  oddSum_(0),
  evenSum_(0),
  first_(0),
  last_(0)
{ }

void Functor::operator ()(long long int num)
{
  count_++;

  if (num > max_)
  {
    max_ = num;
  }

  if (num < min_)
  {
    min_ = num;
  }

  if (num > 0)
  {
    positive_++;
  }

  if (num < 0)
  {
    negative_++;
  }

  if (num % 2 != 0)
  {
    oddSum_ += num;
  }
  else
  {
    evenSum_ += num;
  }

  if (count_ == 1)
  {
    first_ = num;
  }

  last_ = num;
}

long long int Functor::getMin() const
{
  return min_;
}

long long int Functor::getMax() const
{
  return max_;
}

long double Functor::getMean() const
{
  return static_cast<double>(oddSum_ + evenSum_) / static_cast<double>(count_);
}

long long int Functor::getPositive() const
{
  return positive_;
}

long long int Functor::getNegative() const
{
  return negative_;
}

long long int Functor::getOddSum() const
{
  return oddSum_;
}

long long int Functor::getEvenSum() const
{
  return evenSum_;
}

bool Functor::equals() const {
  return first_ == last_;
}

long long int Functor::getCount() const
{
  return count_;
}
