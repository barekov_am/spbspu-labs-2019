#include "dataStruct.hpp"
#include <iostream>
#include <sstream>

DataStruct read(const std::string &string)
{
  const int MAX = 5;
  const int MIN = -5;

  int key1 = 0;
  int key2 = 0;
  std::string str;
  std::stringstream stream(string);

  stream >> key1;
  stream.ignore(string.length(), ',');

  stream >> key2;
  stream.ignore(string.length(), ',');

  std::getline(stream, str);

  if (key1 < MIN || key1 > MAX || key2 < MIN || key2 > MAX || str.empty() || stream.fail())
  {
    throw std::invalid_argument("Keys must be between -5 and 5");
  }

  return {key1, key2, str};
}

void write(const DataStruct &data)
{
  std::cout << data.key1 << "," << data.key2 << "," << data.str << "\n";
}
