#ifndef DATA_STRUCT_HPP
#define DATA_STRUCT_HPP

#include <string>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

DataStruct read(const std::string &string);
void write(const DataStruct &data);

#endif
