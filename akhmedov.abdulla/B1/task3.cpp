#include <vector>
#include "print.hpp"

void task3()
{
  std::vector<int> vector;
  int in = 0;

  while (std::cin >> in)
  {
    if (in == 0)
    {
      break;
    }
    vector.push_back(in);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Could not read the data");
  }

  if (vector.empty())
  {
    return;
  }

  if (in != 0)
  {
    throw std::invalid_argument("Wrong end of line");
  }

  switch (vector.back())
  {
  case 1:
    for (std::vector<int>::iterator i = vector.begin(); i != vector.end();)
    {
      i = ((*i % 2) == 0) ? vector.erase(i) : ++i;
    }
    break;

  case 2:
    for (std::vector<int>::iterator i = vector.begin(); i != vector.end();)
    {
      i = ((*i % 3) == 0) ? (vector.insert(++i, 3, 1) + 3) : ++i;
    }
    break;

  default:
    break;
  }
  print(vector);
}
