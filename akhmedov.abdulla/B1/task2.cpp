#include <fstream>
#include <iostream>
#include <memory>
#include <vector>

const size_t INITSIZE = 1 << 20;

void task2(const char *file)
{
  std::ifstream inFile(file);
  if (!inFile)
  {
    throw std::ios_base::failure("Could not open the file");
  }

  size_t size = INITSIZE;
  using charArray = std::unique_ptr<char[], decltype(&free)>;
  charArray cArray(static_cast<char *>(malloc(size)), &free);

  size_t i = 0;
  while (inFile)
  {
    inFile.read(&cArray[i], INITSIZE);
    i += inFile.gcount();

    if (inFile.gcount() == INITSIZE)
    {
      size += INITSIZE;
      charArray tmp(static_cast<char *>(realloc(cArray.get(), size)), &free);

      if (tmp)
      {
        cArray.release();
        std::swap(cArray, tmp);
      }
      else
      {
        throw std::runtime_error("Could not reallocate");
      }
    }
  }

  std::vector<char> vctr(&cArray[0], &cArray[i]);

  for (std::vector<char>::iterator i = vctr.begin(); i != vctr.end(); ++i)
  {
    std::cout << *i;
  }
}
