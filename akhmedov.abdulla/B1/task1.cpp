#include <forward_list>
#include <vector>
#include "access.hpp"
#include "print.hpp"
#include "sort.hpp"

void task1(const char *direction)
{
  const Direction direction1 = getDirection(direction);

  std::vector<int> bracketVector;
  int in = 0;
  while (std::cin >> in)
  {
    bracketVector.push_back(in);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Could not read the data");
  }

  if (bracketVector.empty())
  {
    return;
  }

  std::vector<int> atVector = bracketVector;
  std::forward_list<int> iteratorList(bracketVector.begin(), bracketVector.end());

  sort<Bracket>(bracketVector, direction1);
  sort<At>(atVector, direction1);
  sort<Iterator>(iteratorList, direction1);
  print(bracketVector);
  print(atVector);
  print(iteratorList);
}
