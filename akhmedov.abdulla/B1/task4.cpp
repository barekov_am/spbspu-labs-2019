#include <random>
#include <vector>
#include "access.hpp"
#include "print.hpp"
#include "sort.hpp"

void fillRandom(double *array, size_t size)
{
  for (size_t i = 0; i < size; ++i) {
    array[i] = (rand() % 21 - 10) / 10.0;
  }
}

void task4(const char *direction, size_t size)
{
  if (size < 1)
  {
    throw std::invalid_argument("Wrong size must be greater than 0");
  }

  Direction direction1 = getDirection(direction);
  std::vector<double> vector(size);
  fillRandom(&vector[0], size);
  print(vector);
  sort<At>(vector, direction1);
  print(vector);
}
