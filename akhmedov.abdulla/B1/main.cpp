#include <iostream>
#include <stdexcept>
#include <string>

void task1(const char *direction);
void task2(const char *file);
void task3();
void task4(const char *direction, size_t size);

int main(int argc, char *argv[])
{
  if (argc <= 1) {
    std::cerr << "ARGC: wrong number";
    return 1;
  }

  try
  {
    char *ptr = nullptr;
    int task = std::strtol(argv[1], &ptr, 10);
    if (*ptr != '\x00')
    {
      std::cerr << "Wrong argument";
      return 1;
    }

    switch (task) {
    case 1:
      if (argc != 3) {
        std::cerr << "Wrong number of arguments";
        return 1;
      }
      task1(argv[2]);
      break;

    case 2:
      if (argc != 3) {
        std::cerr << "Wrong number of arguments";
        return 1;
      }
      task2(argv[2]);
      break;

    case 3:
      if (argc != 2) {
        std::cerr << "Wrong number of arguments";
        return 1;
      }
      task3();
      break;

    case 4:
      if (argc != 4) {
        std::cerr << "Wrong number of arguments";
        return 1;
      }
      srand(time(nullptr));
      task4(argv[2], std::stoi(argv[3]));
      break;

    default:
      std::cerr << "Wrong task";
      return 1;
    }
  }
  catch (const std::exception &e)
  {
    std::cerr << e.what() << "\n";
    return 1;
  }
  return 0;
}
