#include <iostream>
#include <stdexcept>

void task1();
void task2();

int main(int argc, char *argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Invalid number of argument" << std::endl;
      return 1;
    }

    switch(std::stoi(argv[1]))
    {
      case 1:
        task1();
        break;
      case 2:
        task2();
        break;
      default:
        std::cerr << "Invalid number of argument" << std::endl;
        return 1;
    }
  }

  catch (std::exception &e)
  {
    std::cerr << e.what() << std::endl;
    return 2;
  }

  return 0;
}
