#include <sstream>

#include "shapesHandling.hpp"
#include "shape.hpp"

Shape readPoints(std::string &str, int tops)
{
  Shape shape;
  size_t openBracket;
  size_t closeBracket;
  size_t colon;

  for (int i = 0; i < tops; i++)
  {
    if (str.empty())
    {
      throw std::invalid_argument("Invalid number of arguments");
    }

    openBracket = str.find_first_of('(');
    colon = str.find_first_of(';');
    closeBracket = str.find_first_of(')');
    size_t npos = std::string::npos;

    if ((openBracket == npos) || (colon == npos) || (closeBracket == npos))
    {
      throw std::invalid_argument("Invalid point");
    }

    int x = std::stoi(str.substr(openBracket + 1, colon - (openBracket + 1)));
    int y = std::stoi(str.substr(colon + 1, closeBracket - (colon + 1)));
    str.erase(0, closeBracket + 1);
    shape.push_back({x, y});
  }

  while (str.find_first_of(" \t") == 0)
  {
    str.erase(0, 1);
  }

  if (!str.empty())
  {
    throw std::invalid_argument("Invalid number of arguments");
  }

  return shape;
}

void readShapes(std::vector<Shape> &vector, std::string &str)
{
  while (std::getline(std::cin, str))
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Could not read");
    }

    std::stringstream stream(str);
    std::string strTops;
    stream >> strTops;

    if (strTops.empty())
    {
      continue;
    }

    if (stream.eof())
    {
      throw std::invalid_argument("Invalid number of arguments");
    }

    int tops = std::stoi(strTops);

    if (tops < 3)
    {
      throw std::invalid_argument("Invalid value of tops");
    }

    std::getline(stream, str);
    vector.push_back(readPoints(str, tops));
  }
}

int getSquaredLength(const Point_t &point1, const Point_t &point2)
{
  int x1x2 = point1.x - point2.x;
  int y1y2 = point1.y - point2.y;

  return x1x2 * x1x2 + y1y2 * y1y2;
}

bool isRectangle(const Shape& shape)
{
  bool compareDiags = getSquaredLength(shape[1], shape[2]) == getSquaredLength(shape[0], shape[3]);
  bool compSides1 = getSquaredLength(shape[0], shape[1]) == getSquaredLength(shape[2], shape[3]);
  bool compSides2 = getSquaredLength(shape[0], shape[2]) == getSquaredLength(shape[1], shape[3]);

  return compareDiags && compSides1 && compSides2;
}

bool isSquare(const Shape& shape)
{
  bool compSides = getSquaredLength(shape[0], shape[1]) == getSquaredLength(shape[1], shape[2]);

  return isRectangle(shape) && compSides;
}

bool isLess(const Shape& l, const Shape& r)
{
  if (l.size() < r.size())
  {
    return true;
  }

  if ((l.size() == RECTANGLE_PEAKS) && (r.size() == RECTANGLE_PEAKS))
  {
    if (isSquare(l))
    {
      if (isSquare(r))
      {
        return l[0].x < r[0].x;
      }

      return true;
    }
  }

  return false;
}
