#include <iostream>
#include <memory>
#include <string>

void task1();
void task2();

int main(int argc, char* argv[])
{
  try {
    if (argc != 2) {
      throw std::invalid_argument("Wrong task number");
    }

    int task = std::stoi(argv[1]);
    switch (task) {
      case 1:
        task1();
        break;
      case 2:
        task2();
        break;
      default:
        throw std::invalid_argument("Wrong task number");
    }

    return 0;
  } catch (const std::exception &e) {
    std::cerr << e.what() << '\n';
    return 1;
  }
}
