#ifndef READ_HPP
#define READ_HPP

#include <memory>
#include "shape.hpp"

std::shared_ptr<Shape> readShape(std::string &line);

#endif
