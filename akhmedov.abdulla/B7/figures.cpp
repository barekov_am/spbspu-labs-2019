#include "figures.hpp"

Triangle::Triangle(double x, double y):
  Shape(x, y)
{ }

void Triangle::draw(std::ostream &out)
{
  out << "TRIANGLE (" << x_ << ";" << y_ << ")" << '\n';
}

Square::Square(double x, double y):
  Shape(x, y)
{ }

void Square::draw(std::ostream &out)
{
  out << "SQUARE (" << x_ << ";" << y_ << ")" << '\n';
}

Circle::Circle(double x, double y):
  Shape(x, y)
{ }

void Circle::draw(std::ostream &out)
{
  out << "CIRCLE (" << x_ << ";" << y_ << ")" << '\n';
}
