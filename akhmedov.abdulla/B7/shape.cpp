#include "shape.hpp"
#include <stdexcept>

Shape::Shape():
  x_(0),
  y_(0)
{ }

Shape::Shape(double x, double y):
  x_(x),
  y_(y)
{ }

bool Shape::isMoreLeft(const Shape *shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("No shape found");
  }

  return shape->x_ > x_;
}

bool Shape::isUpper(const Shape *shape) const
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("No shape found");
  }

  return shape->y_ < y_;
}
