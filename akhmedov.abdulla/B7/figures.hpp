#ifndef FIGURES_HPP
#define FIGURES_HPP

#include "shape.hpp"

class Triangle : public Shape
{
  public:
    Triangle(double x, double y);
    void draw(std::ostream &out) override;
};

class Square : public Shape
{
  public:
    Square(double x, double y);
    void draw(std::ostream &out) override;
};

class Circle : public Shape
{
  public:
    Circle(double x, double y);
    void draw(std::ostream &out) override;
};

#endif
