#include "functor.hpp"
#include <cmath>
#include <algorithm>
#include <iostream>

void Functor::operator()(double element)
{
  data.push_back(element * M_PI);
}

void Functor::printInfo()
{
  if (data.empty())
  {
    return;
  }

  for (size_t i = 0; i < data.size(); ++i)
  {
    std::cout << data.at(i) << " ";
  }
}
