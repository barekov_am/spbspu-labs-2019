#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <iostream>
#include <vector>

class Functor
{
  public:
    void operator()(double element);

    void printInfo();
  private:
    std::vector<double> data;
};

#endif
