#include <functional>
#include <iterator>
#include <algorithm>
#include <memory>
#include <string>

#include "functor.hpp"
#include "shape.hpp"
#include "read.hpp"

void task1()
{
  Functor functor;

  std::for_each(std::istream_iterator<double>(std::cin), std::istream_iterator<double>(), std::ref(functor));

  if (!std::cin.eof())
  {
    throw std::ios_base::failure("Could not read data");
  }

  functor.printInfo();
}


void printVector(const std::vector<std::shared_ptr<Shape>> &vector)
{
  for (const auto &shape : vector)
  {
    shape->draw(std::cout);
  }
}

void task2()
{
  std::vector<std::shared_ptr<Shape>> shapes;
  std::string ss;

  while (getline(std::cin >> std::ws, ss))
  {
    std::shared_ptr<Shape> shape = readShape(ss);
    shapes.push_back(shape);
  }

  std::cout << "Original:" << std::endl;
  printVector(shapes);

  std::cout << "Left-Right:" << std::endl;
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape> &left, const std::shared_ptr<Shape> &right)
  {
      return left->isMoreLeft(right.get());
  });
  printVector(shapes);

  std::cout << "Right-Left:" <<std::endl;
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape> &left, const std::shared_ptr<Shape> &right)
  {
      return !left->isMoreLeft(right.get());
  });
  printVector(shapes);

  std::cout << "Top-Bottom:" << std::endl;
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape> &left, const std::shared_ptr<Shape> &right)
  {
      return left->isUpper(right.get());
  });
  printVector(shapes);

  std::cout << "Bottom-Top:" << std::endl;
  std::sort(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape> &left, const std::shared_ptr<Shape> &right)
  {
      return !left->isUpper(right.get());
  });
  printVector(shapes);
}
