#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

class Circle: public Shape
{
public:
  Circle(const point_t& pos, double radius);

  void printData() const noexcept override;
  double getArea() const noexcept override;
  rectangle_t getFrameRect() const noexcept override;
  void move(const point_t& newPosition) noexcept override;
  void move(double dx, double dy) noexcept override;
  point_t getPos() const noexcept override;

private:
  point_t pos_;
  double radius_;
};

#endif // CIRCLE_HPP
