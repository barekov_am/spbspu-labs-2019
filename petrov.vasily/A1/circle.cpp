#define _USE_MATH_DEFINES
#include "circle.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>

Circle::Circle(const point_t& pos, double radius):
  pos_(pos),
  radius_(radius)
{
  if (radius <= 0.0) {
    throw std::invalid_argument("Invalid circle's parameters, shall be greatet than 0.0.");
  }
}

void Circle::printData() const noexcept
{
  std::cout << "\nCircle";
  std::cout << "\nPosition: (" << pos_.x << ';' << pos_.y << ')';
  std::cout << "\nRadius: " << radius_;
}

double Circle::getArea() const noexcept
{
  return pow(radius_, 2.0) * M_PI;
}

rectangle_t Circle::getFrameRect() const noexcept
{
  return {pos_, radius_ * 2, radius_ * 2};
}

void Circle::move(const point_t& newPosition) noexcept
{
  pos_ = newPosition;
}

void Circle::move(double dx, double dy) noexcept
{
  pos_.x += dx;
  pos_.y += dy;
}

point_t Circle::getPos() const noexcept
{
  return pos_;
}
