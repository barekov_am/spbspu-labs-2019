#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

class Triangle: public Shape
{
public:
  Triangle(const point_t& vertexA, const point_t& vertexB, const point_t& vertexC);

  void printData() const noexcept override;
  double getArea() const noexcept override;
  rectangle_t getFrameRect() const noexcept override;
  void move(const point_t& newPosition) noexcept override;
  void move(double dx, double dy) noexcept override;
  point_t getPos() const noexcept override;

private:
  point_t vertices_[3];
};

#endif // TRIANGLE_HPP
