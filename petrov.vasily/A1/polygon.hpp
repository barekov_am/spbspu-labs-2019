#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "shape.hpp"
#include <cstdlib>

class Polygon: public Shape
{
public:
  Polygon(const point_t* vertices, const size_t& qtyVertex);

  Polygon(const Polygon& other);
  Polygon(Polygon&& other) noexcept;

  ~Polygon() override;

  Polygon& operator =(const Polygon& other);
  Polygon& operator =(Polygon&& other) noexcept;

  void printData() const noexcept override;
  double getArea() const noexcept override;
  rectangle_t getFrameRect() const noexcept override;
  void move(const point_t& newPosition) noexcept override;
  void move(double dx, double dy) noexcept override;
  point_t getPos() const noexcept override;

private:
  std::size_t qtyVertex_;
  point_t* vertices_;

  void swap(Polygon& other) noexcept;
  bool checkBump() const noexcept;
};

#endif // POLYGON_HPP
