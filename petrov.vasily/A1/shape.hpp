#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"

class Shape
{
public:
  virtual ~Shape() = default;
  virtual void printData() const noexcept = 0;
  virtual double getArea() const noexcept = 0;
  virtual rectangle_t getFrameRect() const noexcept = 0;
  virtual void move(const point_t& newPosition) noexcept = 0;
  virtual void move(double dx, double dy) noexcept = 0;
  virtual point_t getPos() const noexcept = 0;
};

#endif // SHAPE_HPP
