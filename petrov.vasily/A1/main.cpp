#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void printShapeData(const Shape* shape)
{
  if (shape == nullptr) {
    throw std::invalid_argument("Pointer on shape is null.");
  }

  shape->printData();
  std::cout << "\nArea: " << shape->getArea();

  const rectangle_t frameRect = shape->getFrameRect();
  std::cout << "\nShape's frame rect";
  std::cout << "\nCentre: (" << frameRect.pos.x << ';' << frameRect.pos.y << ')';
  std::cout << "\nWidth: " << frameRect.width;
  std::cout << "\nHeight: " << frameRect.height;
}

int main()
{
  try {
    Rectangle rectangle({0.0, 0.0}, 15.0, 50.0);
    printShapeData(&rectangle);
    const double dx = 10.0;
    const double dy = 20.0;
    rectangle.move(dx, dy);
    std::cout << "\nAfter move + (" << dx << ';' << dy << ')';
    rectangle.printData();

    std::cout << std::endl;

    Circle circle({0.0, 0.0}, 13.0);
    printShapeData(&circle);
    const point_t newPos = {-1.0, 3.0};
    circle.move(newPos);
    std::cout << "\nAfter move on new position (" << newPos.x << ';' << newPos.y << ')';
    circle.printData();

    std::cout << std::endl;

    Triangle triangle({1.0, 0.5}, {2.0, 2.0}, {3.0, 0.5});
    printShapeData(&triangle);
    triangle.move(newPos);
    std::cout << "\nAfter move on new position (" << newPos.x << ';' << newPos.y << ')';
    triangle.printData();

    std::cout << std::endl;

    const size_t qty = 5;
    const point_t vertices[qty] = {{1.0, 4.0}, {3.0, 2.0}, {5.0, 3.0}, {6.0, 5.0}, {3.0, 6.0}};
    Polygon polygon1(vertices, qty);
    std::cout << "\nAbout first polygon";
    printShapeData(&polygon1);
    polygon1.move(dx, dy);
    std::cout << "\nAfter move + (" << dx << ';' << dy << ')';
    polygon1.printData();

    Polygon polygon2(std::move(polygon1));
    std::cout << "\n\nCreated a new polygon that took the values from the first polygon.";
    std::cout << "\nAbout second polygon";
    polygon2.printData();
    std::cout << "\nAbout first polygon";
    polygon1.printData();

    Polygon polygon3(polygon2);
    std::cout << "\n\nCreated a new polygon that copy the values from the second polygon.";
    std::cout << "\nAbout second polygon";
    polygon2.printData();
    std::cout << "\nAbout third polygon";
    polygon3.printData();

    std::cout << "\n\nDemonstration of assignment with movement from third polygon.";
    polygon1 = std::move(polygon3);
    std::cout << "\nAbout third polygon.";
    polygon3.printData();
    std::cout << "\nAbout first polygon.";
    polygon1.printData();

    std::cout << "\n\nDemostration of assignment with copy from first polygon.";
    polygon3 = polygon1;
    std::cout << "\nAbout first polygon.";
    polygon1.printData();
    std::cout << "\nAbout third polygon.";
    polygon3.printData();

    std::cout << std::endl;

    return 0;
  } catch (const std::invalid_argument& ex) {
    std::cerr << "\nError invalid argument: \"" << ex.what() << "\"\n";

    return 1;
  } catch (const std::exception& ex) {
    std::cerr << "\nError: \"" << ex.what() << "\"\n";

    return 2;
  }
}
