#include "triangle.hpp"
#include <cmath>
#include <cstddef>
#include <iostream>
#include <stdexcept>
#include <algorithm>

Triangle::Triangle(const point_t& vertexA, const point_t& vertexB, const point_t& vertexC):
  vertices_({vertexA, vertexB, vertexC})
{
  if (getArea() == 0.0) {
    throw std::invalid_argument("Invalid trianle's area, shall be greatet than 0.0");
  }
}

void Triangle::printData() const noexcept
{
  std::cout << "\nTriangle";
  const point_t pos = getPos();
  std::cout << "\nPosition: (" << pos.x << ";" << pos.y << ')';
  std::cout << "\nVertices of triangle";
  for (int index = 0; index < 3; index++) {
    std::cout << "\nVertex №" << index + 1 << " (" << vertices_[index].x << ';' << vertices_[index].y << ')';
  }
}

double Triangle::getArea() const noexcept
{
  return 0.5 * (fabs(vertices_[0].x - vertices_[2].x) * (vertices_[1].y - vertices_[2].y)
        - fabs(vertices_[1].x - vertices_[2].x) * (vertices_[0].y - vertices_[2].y));
}

rectangle_t Triangle::getFrameRect() const noexcept
{
  point_t max = {0.0, 0.0};
  point_t min = {0.0, 0.0};

  max.x = std::max(std::max(vertices_[0].x, vertices_[1].x), vertices_[2].x);
  min.x = std::min(std::min(vertices_[0].x, vertices_[1].x), vertices_[2].x);
  max.y = std::max(std::max(vertices_[0].y, vertices_[1].y), vertices_[2].y);
  min.y = std::min(std::min(vertices_[0].y, vertices_[1].y), vertices_[2].y);

  const double width = max.x - min.y;
  const double height = max.y - min.y;
  const point_t pos = {min.x + width / 2, min.y + height / 2};

  return {pos, width, height};
}

void Triangle::move(const point_t& newPosition) noexcept
{
  const point_t pos = getPos();
  const point_t difference = {newPosition.x - pos.x, newPosition.y - pos.y};
  move(difference.x, difference.y);
}

void Triangle::move(double dx, double dy) noexcept
{
  for (std::size_t index = 0; index < 3; index++) {
    vertices_[index].x += dx;
    vertices_[index].y += dy;
  }
}

point_t Triangle::getPos() const noexcept
{
  point_t pos = {0.0, 0.0};
  for (std::size_t index = 0; index < 3; index++) {
    pos.x += vertices_[index].x;
    pos.y += vertices_[index].y;
  }
  return pos;
}
