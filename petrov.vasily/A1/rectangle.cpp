#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>

Rectangle::Rectangle(const point_t& pos, double width, double height):
  pos_(pos),
  width_(width),
  height_(height)
{
  if ((width <= 0.0) || (height <= 0.0)) {
    throw std::invalid_argument("Invalid rectangle's parameters, shall be greatet than 0.0");
  }
}

void Rectangle::printData() const noexcept
{
  std::cout << "\nRectangle";
  std::cout << "\nPosition: (" << pos_.x << ';' << pos_.y << ')';
  std::cout << "\nWidth: " << width_;
  std::cout << "\nHeight: " << height_;
}

double Rectangle::getArea() const noexcept
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const noexcept
{
  return {pos_, width_, height_};
}

void Rectangle::move(const point_t& newPosition) noexcept
{
  pos_ = newPosition;
}

void Rectangle::move(double dx, double dy) noexcept
{
  pos_.x += dx;
  pos_.y += dy;
}

point_t Rectangle::getPos() const noexcept
{
  return pos_;
}
