#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "triangle.hpp"

const double EPS = 0.001;

BOOST_AUTO_TEST_SUITE(triangleTest)

BOOST_AUTO_TEST_CASE(firstMove)
{
  petrov::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  const double oldArea = triangle.getArea();
  triangle.move({ 5.0, 5.0 });
  BOOST_CHECK_CLOSE(oldArea, triangle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(secondMove)
{
  petrov::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  const double oldArea = triangle.getArea();
  triangle.move(5.0, 5.0);
  BOOST_CHECK_CLOSE(oldArea, triangle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(scale)
{
  petrov::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  double oldArea = triangle.getArea();
  double k = 9;
  triangle.scale(9);
  BOOST_CHECK_CLOSE(oldArea * k * k, triangle.getArea(), EPS);
  oldArea = triangle.getArea();
  k = 0.5;
  triangle.scale(0.5);
  BOOST_CHECK_CLOSE(oldArea * k * k, triangle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(errCoeff)
{
  petrov::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  BOOST_CHECK_THROW(triangle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(triangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  petrov::Triangle triangle({ 4.0, 2.0 }, { 5.0, 5.0 }, { 3.0, 5.0 });
  const double oldArea = triangle.getArea();
  const double angle = 90;
  triangle.rotate(angle);
  BOOST_CHECK_CLOSE(oldArea, triangle.getArea(), EPS);
}

BOOST_AUTO_TEST_SUITE_END()
