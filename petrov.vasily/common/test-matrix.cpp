#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separation.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithCrossing)
{
  const size_t layersValue = 2;
  const size_t columnsValue = 1;

  petrov::Rectangle testRectangle({ 3.0, 3.0, {1.0, 1.0} });
  petrov::Circle testCircle(5.0, { 3.0, 3.0 });
  petrov::shap3 rectanglePtr = std::make_shared<petrov::Rectangle>(testRectangle);
  petrov::shap3 circlePtr = std::make_shared<petrov::Circle>(testCircle);

  petrov::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  petrov::Matrix testMatrix = petrov::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithoutCrossing)
{
  const size_t layersValue = 1;
  const size_t columnsValue = 2;

  petrov::Rectangle testRectangle({ 3.0, 3.0, {1.0, 1.0} });
  petrov::Circle testCircle(5.0, { 13.0, 3.0 });
  petrov::shap3 rectanglePtr = std::make_shared<petrov::Rectangle>(testRectangle);
  petrov::shap3 circlePtr = std::make_shared<petrov::Circle>(testCircle);

  petrov::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  petrov::Matrix testMatrix = petrov::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  petrov::Rectangle testRectangle({ 3.0, 3.0, {1.0, 1.0} });
  petrov::Circle testCircle(5.0, { 3.0, 3.0 });
  petrov::shap3 rectanglePtr = std::make_shared<petrov::Rectangle>(testRectangle);
  petrov::shap3 circlePtr = std::make_shared<petrov::Circle>(testCircle);

  petrov::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  petrov::Matrix testMatrix = petrov::part(testCompositeShape);

  BOOST_CHECK_THROW(testMatrix[5][0], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-5][0], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkCopyAndMove)
{
  petrov::Rectangle testRectangle({ 3.0, 3.0, {1.0, 1.0} });
  petrov::Circle testCircle(5.0, { 3.0, 3.0 });
  petrov::shap3 rectanglePtr = std::make_shared<petrov::Rectangle>(testRectangle);
  petrov::shap3 circlePtr = std::make_shared<petrov::Circle>(testCircle);

  petrov::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  petrov::Matrix testMatrix = petrov::part(testCompositeShape);
  petrov::Matrix copyMatrix(testMatrix);
  petrov::Matrix moveMatrix(std::move(testMatrix));

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyMatrix);
}

BOOST_AUTO_TEST_SUITE_END()
