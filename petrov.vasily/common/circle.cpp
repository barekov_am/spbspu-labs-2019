#define _USE_MATH_DEFINES
#include "circle.hpp"

#include <iostream>
#include <stdexcept>
#include <math.h>


petrov::Circle::Circle(const double radius, const petrov::point_t & center) :
  radius_(radius),
  center_(center)
{
  if (radius_ <= 0.0)
  {
    throw std::invalid_argument("invalid value for circle's radius");
  }
}

double petrov::Circle::getArea() const
{
  return (M_PI * radius_ * radius_);
}

petrov::rectangle_t petrov::Circle::getFrameRect() const
{
  return { 2 * radius_, 2 * radius_, center_ };
}

void petrov::Circle::move(const petrov::point_t & point)
{
  center_ = point;
}

void petrov::Circle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void petrov::Circle::print() const
{
  std::cout << "Area of circle: " << getArea() << std::endl;
  std::cout << "Width of frame rectangle: " << getFrameRect().width << std::endl;
  std::cout << "Height of frame rectangle: " << getFrameRect().height << std::endl;
  std::cout << "Center point of frame rectangle: (" << getFrameRect().pos.x
      << "; " << getFrameRect().pos.y << ")" << std::endl;
}

void petrov::Circle::scale(double k)
{
  if (k <= 0.0)
  {
    throw std::invalid_argument("invalid value for 'k'");
  }
  else
  {
    radius_ *= k;
  }
}

void petrov::Circle::rotate(const double)
{
}
