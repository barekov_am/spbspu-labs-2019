#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "rectangle.hpp"

const double EPS = 0.001;

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(firstMove)
{
  petrov::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  const double oldWidth = rectangle.getFrameRect().width;
  const double oldHeight = rectangle.getFrameRect().height;
  const double oldArea = rectangle.getArea();
  rectangle.move({ 5.0, 5.0 });
  BOOST_CHECK_CLOSE(oldWidth, rectangle.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(oldHeight, rectangle.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(oldArea, rectangle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(secondMove)
{
  petrov::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  const double oldWidth = rectangle.getFrameRect().width;
  const double oldHeight = rectangle.getFrameRect().height;
  const double oldArea = rectangle.getArea();
  rectangle.move(5.0, 5.0);
  BOOST_CHECK_CLOSE(oldWidth, rectangle.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(oldHeight, rectangle.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(oldArea, rectangle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(scale)
{
  petrov::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  double oldWidth = rectangle.getFrameRect().width;
  double oldHeight = rectangle.getFrameRect().height;
  double oldArea = rectangle.getArea();
  double k = 9;
  rectangle.scale(9);
  BOOST_CHECK_CLOSE(oldWidth * k, rectangle.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(oldHeight * k, rectangle.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(oldArea * k * k, rectangle.getArea(), EPS);
  oldWidth = rectangle.getFrameRect().width;
  oldHeight = rectangle.getFrameRect().height;
  oldArea = rectangle.getArea();
  k = 0.5;
  rectangle.scale(0.5);
  BOOST_CHECK_CLOSE(oldWidth * k, rectangle.getFrameRect().width, EPS);
  BOOST_CHECK_CLOSE(oldHeight * k, rectangle.getFrameRect().height, EPS);
  BOOST_CHECK_CLOSE(oldArea * k * k, rectangle.getArea(), EPS);
}

BOOST_AUTO_TEST_CASE(errWidth)
{
  BOOST_CHECK_THROW(petrov::Rectangle rectangle({ -3.0, 3.0, {1.0, 1.0} }), std::invalid_argument);
  BOOST_CHECK_THROW(petrov::Rectangle rectangle({ 0, 3.0, {1.0, 1.0} }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(errHeihgt)
{
  BOOST_CHECK_THROW(petrov::Rectangle rectangle({ 3.0, -3.0, {1.0, 1.0} }), std::invalid_argument);
  BOOST_CHECK_THROW(petrov::Rectangle rectangle({ 3.0, 0, {1.0, 1.0} }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(errCoeff)
{
  petrov::Rectangle rectangle({ 3.0, 3.0, {1.0, 1.0} });
  BOOST_CHECK_THROW(rectangle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(rectangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(constantParametersAfterRotation)
{
  petrov::Rectangle testRectangle({ 2.0, 4.0, {5.0, 5.0} });

  const double testArea = testRectangle.getArea();
  const petrov::rectangle_t testFrameRect = testRectangle.getFrameRect();
  const double angle = 90;

  testRectangle.rotate(angle);

  BOOST_CHECK_CLOSE(testFrameRect.width, testRectangle.getFrameRect().width / 2, EPS);
  BOOST_CHECK_CLOSE(testFrameRect.height, testRectangle.getFrameRect().height * 2, EPS);
  BOOST_CHECK_CLOSE(testArea, testRectangle.getArea(), EPS);
  BOOST_CHECK_EQUAL(testFrameRect.pos.x, testRectangle.getFrameRect().pos.x);
  BOOST_CHECK_EQUAL(testFrameRect.pos.y, testRectangle.getFrameRect().pos.y);
}

BOOST_AUTO_TEST_SUITE_END()
