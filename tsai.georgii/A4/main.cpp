#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "partition.hpp"
#include "matrix.hpp"


int main()
{
  tsai::Rectangle rectangle({ 4, 5 }, 6, 7);
  tsai::Circle circle({ 4, 5 }, 6);
  tsai::Triangle triangle({ 4, 6 }, { 6, 7 }, { 1, 10 });
  std::cout << rectangle.getArea() << std::endl;
  std::cout << circle.getArea() << std::endl;
  std::cout << triangle.getArea() << std::endl;
  tsai::CompositeShape compositeShape(std::make_shared<tsai::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<tsai::Circle>(circle));
  compositeShape.add(std::make_shared<tsai::Triangle>(triangle));
  std::cout << compositeShape.getArea() << std::endl;
  std::unique_ptr<tsai::Shape::shapePtr[]> shape = std::make_unique<tsai::Shape::shapePtr[]>(5);
  tsai::Rectangle rectangle1({ 2, 4.5 }, 1, 2);
  tsai::Rectangle rectangle2({ 3.5, 2 }, 2, 5);
  tsai::Circle circle1({ 6, 4 }, 2);
  tsai::Triangle triangle1({ 3, 2 }, { 2, 0 }, { 4, 0 });
  tsai::Triangle triangle2({ 7, 4 }, { 9, 3 }, { 9, 5 });
  shape[0] = std::make_shared<tsai::Rectangle>(rectangle1);
  shape[1] = std::make_shared<tsai::Rectangle>(rectangle2);
  shape[2] = std::make_shared<tsai::Circle>(circle1);
  shape[3] = std::make_shared<tsai::Triangle>(triangle1);
  shape[4] = std::make_shared<tsai::Triangle>(triangle2);
  tsai::Matrix matrix = tsai::split(shape, 5);
  std::cout << "\nSeparated composite shape:\n";
  for (size_t i = 0; i < matrix.getRows(); i++)
  {
    for (size_t j = 0; j < matrix.getColumns(); j++)
    {
      if (matrix[i][j] != nullptr)
      {
        std::cout << i << ", shape: " << j << "\n";
      }
    }
  }

  return 0;
}
