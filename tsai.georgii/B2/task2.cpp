#include <list>
#include <iterator>
#include <iostream>
#include <stdexcept>

const int MAX_VALUE = 20;

void print(std::list<int>::iterator begin, std::list<int>::iterator end)
{
  if (begin == end)
  {
    std::cout << '\n';
    return;
  }

  if (begin == std::prev(end))
  {
    std::cout << *begin << '\n';
    return;
  }

  std::cout << *begin << ' ' << *std::prev(end) << ' ';

  ++begin;
  --end;

  print(begin, end);
}

void task2()
{
  std::list<int> list;

  int value = 0;

  while (std::cin >> value)
  {
    if ((value < 1) || (value > MAX_VALUE))
    {
      throw std::out_of_range("Input value should be [1..20].\n");
    }

    list.push_back(value);

    if (list.size() > MAX_VALUE)
    {
      throw std::invalid_argument("Elements overflow.\n");
    }
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Data read failure.\n");
  }

  print(list.begin(), list.end());
}
