#include <iostream>
#include <stdexcept>

void task1();
void task2();

int main(int argc, char * argv[]) {
  try
  {
    if (argc != 2)
    {
      std::cerr << "Wrong number of arguments.\n";
      return 1;
    }

    int taskNumber = atoi(argv[1]);

    switch (taskNumber)
    {
    case 1:
      task1();
      break;

    case 2:
      task2();
      break;

    default:
      std::cerr << "Wrong task number.\n";
      return 1;
    }
  }
  catch (const std::exception & err)
  {
    std::cerr << err.what() << '\n';
    return 1;
  }

  return 0;
}
