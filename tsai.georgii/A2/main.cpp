#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

int main()
{
  tsai::Rectangle rectOne({ 0, 0 }, 100, 50);
  tsai::Shape *shapePointer = &rectOne;

  shapePointer->printInfo();
  shapePointer->move(10, 20);
  std::cout << "  Rectangle has been moved by dx and dy\n\n";
  shapePointer->printInfo();
  shapePointer->move({ 20, 40 });
  std::cout << "  Rectangle has been moved by coordinates\n\n";
  shapePointer->printInfo();
  shapePointer->scale(3);
  std::cout << "  Rectangle has been scaled\n\n";
  shapePointer->printInfo();

  tsai::Circle circleOne({ 0, 0 }, 50);
  shapePointer = &circleOne;

  shapePointer->printInfo();
  shapePointer->move(10, 20);
  std::cout << "  Circle has been moved by dx and dy\n\n";
  shapePointer->printInfo();
  shapePointer->move({ 20, 40 });
  std::cout << "  Circle has been moved by coordinates\n\n";
  shapePointer->printInfo();
  shapePointer->scale(2);
  std::cout << "  Circle has been scaled\n\n";
  shapePointer->printInfo();

  tsai::Triangle triangleOne({ 10, 10 }, { 20, 20 }, { 30, 10 });
  shapePointer = &triangleOne;

  shapePointer->printInfo();
  shapePointer->move(10, 20);
  std::cout << "  Triangle has been moved by dx and dy\n\n";
  shapePointer->printInfo();
  shapePointer->move({ 20, 40 });
  std::cout << "  Triangle has been moved by coordinates\n\n";
  shapePointer->printInfo();
  shapePointer->scale(2);
  std::cout << "  Triangle has been scaled\n\n";
  shapePointer->printInfo();

  return 0;
}
