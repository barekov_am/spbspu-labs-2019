#ifndef BOOKMARK_HANDLER_H
#define BOOKMARK_HANDLER_H

#include <map>
#include "phonebook.hpp"

class BookmarkHandler
{

public:
  enum class InsertLocation
  {
    before,
    after
  };

  enum class MoveLocation
  {
    first,
    last
  };

  BookmarkHandler();

  void add(const Phonebook::record_t&);
  void store(const std::string&, const std::string&);
  void insert(InsertLocation, const std::string&, const Phonebook::record_t&);
  void remove(const std::string&);
  void show(const std::string&);
  void move(const std::string&, int);
  void move(const std::string&, MoveLocation);

private:
  using bookmarks = std::map<std::string, Phonebook::iterator>;

  Phonebook records_;
  bookmarks bookmarks_;

  bookmarks::iterator getBookmarkIterator(const std::string&);

};

#endif //BOOKMARK_HANDLER_H
