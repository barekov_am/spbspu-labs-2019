#include <iostream>
#include <cstring>
#include <functional>
#include <algorithm>
#include <sstream>

#include "bookmark-handler.hpp"

void executeAdd(BookmarkHandler&, std::stringstream&);
void executeStore(BookmarkHandler&, std::stringstream&);
void executeInsert(BookmarkHandler&, std::stringstream&);
void executeDelete(BookmarkHandler&, std::stringstream&);
void executeShow(BookmarkHandler&, std::stringstream&);
void executeMove(BookmarkHandler&, std::stringstream&);

void task1()
{
  BookmarkHandler handler;
  std::string line;

  while (getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Input failed");
    }

    std::stringstream str(line);
    std::string command;
    str >> command;

    if (strcmp("add", command.c_str()) == 0)
    {
      executeAdd(handler, str);
    }
    else if (strcmp("store", command.c_str()) == 0)
    {
      executeStore(handler, str);
    }
    else if (strcmp("insert", command.c_str()) == 0)
    {
      executeInsert(handler, str);
    }
    else if (strcmp("delete", command.c_str()) == 0)
    {
      executeDelete(handler, str);
    }
    else if (strcmp("show", command.c_str()) == 0)
    {
      executeShow(handler, str);
    }
    else if (strcmp("move", command.c_str()) == 0)
    {
      executeMove(handler, str);
    }
    else
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }
  }
}
