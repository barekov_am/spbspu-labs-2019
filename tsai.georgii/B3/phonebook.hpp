#ifndef PHONEBOOK_H
#define PHONEBOOK_H

#include <list>
#include <string>

class Phonebook
{

public:
  struct record_t
  {
    std::string name;
    std::string phone;
  };

  using list = std::list<record_t>;
  using iterator = list::iterator;

  void view(iterator) const;
  void pushBack(const record_t&);

  bool empty() const;

  iterator begin();
  iterator end();
  iterator next(iterator);
  iterator prev(iterator);
  iterator insert(iterator, const record_t&);
  iterator remove(iterator);
  iterator move(iterator, int);

private:
  list list_;
  
};

#endif //PHONEBOOK_H
