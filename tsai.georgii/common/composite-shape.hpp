#ifndef COMPOSITE_SHAPE_HPP_INCLUDED
#define COMPOSITE_SHAPE_HPP_INCLUDED
#include "shape.hpp"
#include <memory>

namespace tsai
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &composite);
    CompositeShape(CompositeShape &&composite);
    CompositeShape(shapePtr shape);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &composite);
    CompositeShape &operator =(CompositeShape &&composite);
    shapePtr operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printInfo() const override;
    point_t getPos() const override;
    void scale(double times) override;
    void move(double dx, double dy) override;
    void move(const point_t& pos) override;
    void add(shapePtr shape);
    void remove(size_t index);
    void rotate(double angle) override;
    size_t getSize() const;

  private:
    size_t size_;
    std::unique_ptr<shapePtr[]> shapes_;
  };
}
#endif
