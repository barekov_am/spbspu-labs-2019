#ifndef TRIANGLE_HPP_INCLUDED
#define TRIANGLE_HPP_INCLUDED
#include "shape.hpp"

namespace tsai
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t& a, const point_t& b, const point_t& c);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printInfo() const override;
    point_t getPos() const override;
    void scale(double times) override;
    void move(double dx, double dy) override;
    void move(const point_t& pos) override;
    void rotate(double angle) override;

  private:
    point_t pos_;
    point_t a_;
    point_t b_;
    point_t c_;
  };
}
#endif
