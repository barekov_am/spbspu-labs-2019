#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

tsai::CompositeShape::CompositeShape() :
  size_(0)
{
}

tsai::CompositeShape::CompositeShape(const CompositeShape &composite) :
  size_(composite.size_),
  shapes_(std::make_unique<shapePtr[]>(composite.size_))
{
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i] = composite.shapes_[i];
  }
}

tsai::CompositeShape::CompositeShape(CompositeShape &&composite) :
  size_(composite.size_),
  shapes_(std::move(composite.shapes_))
{
  composite.size_ = 0;
}

tsai::CompositeShape::CompositeShape(shapePtr shape) :
  size_(1),
  shapes_(std::make_unique<shapePtr[]>(size_))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Pointer can not be 'null'");
  }
  shapes_[0] = shape;
}

tsai::CompositeShape &tsai::CompositeShape::operator = (const CompositeShape &composite)
{
  if (this != &composite)
  {
    size_ = composite.size_;
    std::unique_ptr<shapePtr[]> tmp(std::make_unique<shapePtr[]>(composite.size_));
    for (size_t i = 0; i < size_; i++)
    {
      tmp[i] = composite.shapes_[i];  
    }
    shapes_.swap(tmp);
  }

  return *this;
}

tsai::CompositeShape &tsai::CompositeShape::operator = (CompositeShape &&composite)
{
  if (this != &composite)
  {
    size_ = composite.size_;
    shapes_ = std::move(composite.shapes_);
    composite.size_ = 0;
  }

  return *this;
}

tsai::CompositeShape::shapePtr tsai::CompositeShape::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Array index out of bounds");
  }

  return shapes_[index];
}

void tsai::CompositeShape::add(shapePtr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Pointer can not be 'null'");
  }
  std::unique_ptr<shapePtr[]> tmp(std::make_unique<shapePtr[]>(size_ + 1));
  for (size_t i = 0; i < size_; i++)
  {
    tmp[i] = shapes_[i];
  }
  tmp[size_] = shape;
  size_++;
  shapes_.swap(tmp);
}

void tsai::CompositeShape::remove(size_t index)
{
  if (index > size_)
  {
    throw std::out_of_range("Array index out of bounds");
  }
  for (size_t i = index; i < size_ - 1; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  size_--;
}

tsai::rectangle_t tsai::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape must include at least 1 shape");
  }

  rectangle_t framingRect = shapes_[0]->getFrameRect();
  double minX = framingRect.pos.x - framingRect.width / 2;
  double minY = framingRect.pos.y - framingRect.height / 2;
  double maxX = framingRect.pos.x + framingRect.width / 2;
  double maxY = framingRect.pos.y + framingRect.height / 2;

  for (size_t i = 1; i < size_; i++)
  {
    framingRect = shapes_[i]->getFrameRect();
    minX = std::min(framingRect.pos.x - framingRect.width / 2, minX);
    minY = std::min(framingRect.pos.y - framingRect.height / 2, minY);
    maxX = std::max(framingRect.pos.x + framingRect.width / 2, maxX);
    maxY = std::max(framingRect.pos.y + framingRect.height / 2, maxY);
  }

  return rectangle_t{ { (minX + maxX) / 2, (minY + maxY) / 2 }, (maxY - minY), (maxX - minX) };
}

double tsai::CompositeShape::getArea() const
{
  double areaSum = 0;
  for (size_t i = 0; i < size_; i++)
  {
    areaSum += shapes_[i]->getArea();
  }
  return areaSum;
}

tsai::point_t tsai::CompositeShape::getPos() const
{
  return getFrameRect().pos;
}

size_t tsai::CompositeShape::getSize() const
{
  return size_;
}

void tsai::CompositeShape::printInfo() const
{
  const rectangle_t framingRect = getFrameRect();
  std::cout << "\n CompositeShape frame width : " << framingRect.width;
  std::cout << "\n CompositeShape frame height : " << framingRect.height;
  std::cout << "\n Centre : ( x: " << framingRect.pos.x << ", y: " << framingRect.pos.y << " )";
  std::cout << "\n Area is " << getArea() << "\n\n";
}

void tsai::CompositeShape::move(double dx, double dy)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape must include at least 1 shape");
  }

  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void tsai::CompositeShape::move(const point_t &pos)
{
  move(pos.x - getFrameRect().pos.x, pos.y - getFrameRect().pos.y);
}

void tsai::CompositeShape::scale(double times)
{
  if (times <= 0) 
  {
    throw std::invalid_argument("Enter correct 'times'");
  }
  const point_t pos = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    const double dx = shapes_[i]->getFrameRect().pos.x - pos.x;
    const double dy = shapes_[i]->getFrameRect().pos.y - pos.y;
    shapes_[i]->move({ pos.x + dx * times, pos.y + dy * times });
    shapes_[i]->scale(times);
  }
}

void tsai::CompositeShape::rotate(double angle)
{
  const double cos = std::cos((2 * M_PI * angle) / 360);
  const double sin = std::sin((2 * M_PI * angle) / 360);

  const point_t compositePos = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++)
  {
    const point_t shapePos = shapes_[i]->getFrameRect().pos;
    const double dx = (shapePos.x - compositePos.x) * cos - (shapePos.y - compositePos.y) * sin;
    const double dy = (shapePos.x - compositePos.x) * sin + (shapePos.y - compositePos.y) * cos;
    shapes_[i]->move({ compositePos.x + dx, compositePos.y + dy });
    shapes_[i]->rotate(angle);
  }
}
