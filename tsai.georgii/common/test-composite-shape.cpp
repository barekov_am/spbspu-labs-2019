#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double ERROR_VAL = 0.01;

BOOST_AUTO_TEST_SUITE(testCompositeShape)

BOOST_AUTO_TEST_CASE(compositeShapeParametersConstant)
{
  tsai::Rectangle rectTest({ 5, 6 }, 2, 14);
  tsai::Circle circleTest({ 4, 1 }, 10);
  tsai::Triangle triangleTest({ 0, 0 }, { 10, 10 }, { 20, -10 });
  tsai::CompositeShape compositeShape(std::make_shared<tsai::Rectangle>(rectTest));

  compositeShape.add(std::make_shared<tsai::Circle>(circleTest));
  compositeShape.add(std::make_shared<tsai::Triangle>(triangleTest));

  const double beforeArea = compositeShape.getArea();
  const tsai::rectangle_t beforeFrame = compositeShape.getFrameRect();

  compositeShape.move(2, 4);

  tsai::rectangle_t afterFrame = compositeShape.getFrameRect();
  double afterArea = compositeShape.getArea();

  BOOST_CHECK_CLOSE(afterArea, beforeArea, ERROR_VAL);
  BOOST_CHECK_CLOSE(beforeFrame.height, afterFrame.height, ERROR_VAL);
  BOOST_CHECK_CLOSE(beforeFrame.width, afterFrame.width, ERROR_VAL);

  compositeShape.move({ 3, 5 });
  afterFrame = compositeShape.getFrameRect();
  afterArea = compositeShape.getArea();

  BOOST_CHECK_CLOSE(afterArea, beforeArea, ERROR_VAL);
  BOOST_CHECK_CLOSE(beforeFrame.height, afterFrame.height, ERROR_VAL);
  BOOST_CHECK_CLOSE(beforeFrame.width, afterFrame.width, ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleIncreaseArea)
{
  tsai::Rectangle rectTest({ 5, 6 }, 2, 14);
  tsai::Circle circleTest({ 4, 1 }, 10);
  tsai::Triangle triangleTest({ 0, 0 }, { 10, 10 }, { 20, -10 });
  tsai::CompositeShape compositeShape(std::make_shared<tsai::Rectangle>(rectTest));

  compositeShape.add(std::make_shared<tsai::Circle>(circleTest));
  compositeShape.add(std::make_shared<tsai::Triangle>(triangleTest));

  const double beforeArea = compositeShape.getArea();
  const double timesTest = 4.2;

  compositeShape.scale(timesTest);

  BOOST_CHECK_CLOSE(compositeShape.getArea(), beforeArea * timesTest * timesTest, ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(compositeShapeScaleDecreaseArea)
{
  tsai::Rectangle rectTest({ 5, 6 }, 2, 14);
  tsai::Circle circleTest({ 4, 1 }, 10);
  tsai::Triangle triangleTest({ 0, 0 }, { 10, 10 }, { 20, -10 });
  tsai::CompositeShape compositeShape(std::make_shared<tsai::Rectangle>(rectTest));

  compositeShape.add(std::make_shared<tsai::Circle>(circleTest));
  compositeShape.add(std::make_shared<tsai::Triangle>(triangleTest));

  const double beforeArea = compositeShape.getArea();
  const double timesTest = 0.5;

  compositeShape.scale(timesTest);

  BOOST_CHECK_CLOSE(compositeShape.getArea(), beforeArea * timesTest * timesTest, ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(compositeShapeInvalidArguments)
{
  tsai::Rectangle rectTest({ 5, 6 }, 2, 14);
  tsai::Circle circleTest({ 4, 1 }, 10);
  tsai::Triangle triangleTest({ 0, 0 }, { 10, 10 }, { 20, -10 });
  tsai::CompositeShape compositeShape(std::make_shared<tsai::Rectangle>(rectTest));
  compositeShape.add(std::make_shared<tsai::Circle>(circleTest));
  compositeShape.add(std::make_shared<tsai::Triangle>(triangleTest));

  BOOST_CHECK_THROW(compositeShape.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShapeAreaAfterAddingAndDeletion)
{
  tsai::Rectangle rectTest({ 5, 6 }, 2, 14);
  tsai::Circle circleTest({ 4, 1 }, 10);
  tsai::Triangle triangleTest({ 0, 0 }, { 10, 10 }, { 20, -10 });
  tsai::CompositeShape compositeShape(std::make_shared<tsai::Rectangle>(rectTest));
  const double areaBeforeAdd = compositeShape.getArea();
  const double rectangleArea = rectTest.getArea();
  const double circleArea = circleTest.getArea();
  const double triangleArea = triangleTest.getArea();

  compositeShape.add(std::make_shared<tsai::Circle>(circleTest));
  compositeShape.add(std::make_shared<tsai::Triangle>(triangleTest));
  double areaAfterAdd = compositeShape.getArea();
  BOOST_CHECK_CLOSE(areaBeforeAdd + circleArea + triangleArea, areaAfterAdd, ERROR_VAL);

  compositeShape.remove(0);
  BOOST_CHECK_CLOSE(areaAfterAdd - rectangleArea, compositeShape.getArea(), ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(copyConstructor)
{
  tsai::Rectangle rectTest({ 5, 6 }, 2, 14);
  tsai::Circle circleTest({ 4, 1 }, 10);
  tsai::CompositeShape compositeShape(std::make_shared<tsai::Rectangle>(rectTest));
  compositeShape.add(std::make_shared<tsai::Circle>(circleTest));
  const tsai::rectangle_t frameRect = compositeShape.getFrameRect();

  tsai::CompositeShape copiedComposite(compositeShape);
  const tsai::rectangle_t copyFrameRect = copiedComposite.getFrameRect();

  BOOST_CHECK_CLOSE(compositeShape.getArea(), copiedComposite.getArea(), ERROR_VAL);
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, ERROR_VAL);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(moveConstructor)
{
  tsai::Rectangle rectTest({ 5, 6 }, 2, 14);
  tsai::Circle circleTest({ 4, 1 }, 10);
  tsai::CompositeShape compositeShape(std::make_shared<tsai::Rectangle>(rectTest));
  compositeShape.add(std::make_shared<tsai::Circle>(circleTest));
  const tsai::rectangle_t frameRect = compositeShape.getFrameRect();
  const double compositeArea = compositeShape.getArea();

  tsai::CompositeShape movedComposite(std::move(compositeShape));
  const tsai::rectangle_t movedFrameRect = movedComposite.getFrameRect();

  BOOST_CHECK_CLOSE(compositeArea, movedComposite.getArea(), ERROR_VAL);
  BOOST_CHECK_CLOSE(frameRect.height, movedFrameRect.height, ERROR_VAL);
  BOOST_CHECK_CLOSE(frameRect.width, movedFrameRect.width, ERROR_VAL);
  BOOST_CHECK_EQUAL(compositeShape.getSize(), 0);
}

BOOST_AUTO_TEST_CASE(moveOperator)
{
  tsai::Rectangle rectTest({ 5, 6 }, 2, 14);
  tsai::Circle circleTest({ 4, 1 }, 10);
  tsai::CompositeShape compositeShape(std::make_shared<tsai::Rectangle>(rectTest));
  compositeShape.add(std::make_shared<tsai::Circle>(circleTest));
  const tsai::rectangle_t frameRect = compositeShape.getFrameRect();
  const double compositeArea = compositeShape.getArea();

  tsai::Triangle triangleTest({ 0, 0 }, { 10, 10 }, { 20, -10 });
  tsai::CompositeShape movedComposite(std::make_shared<tsai::Triangle>(triangleTest));
  movedComposite = std::move(compositeShape);
  const tsai::rectangle_t moveFrameRect = movedComposite.getFrameRect();

  BOOST_CHECK_CLOSE(compositeArea, movedComposite.getArea(), ERROR_VAL);
  BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, ERROR_VAL);
  BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, ERROR_VAL);
  BOOST_CHECK_EQUAL(compositeShape.getSize(), 0);
}

BOOST_AUTO_TEST_CASE(copyOperator)
{
  tsai::Rectangle rectTest({ 5, 6 }, 2, 14);
  tsai::Circle circleTest({ 4, 1 }, 10);
  tsai::CompositeShape compositeShape(std::make_shared<tsai::Rectangle>(rectTest));
  compositeShape.add(std::make_shared<tsai::Circle>(circleTest));
  const tsai::rectangle_t frameRect = compositeShape.getFrameRect();

  tsai::Triangle triangleTest({ 0, 0 }, { 10, 10 }, { 20, -10 });
  tsai::CompositeShape copiedComposite(std::make_shared<tsai::Triangle>(triangleTest));
  copiedComposite = compositeShape;
  const tsai::rectangle_t copyFrameRect = copiedComposite.getFrameRect();

  BOOST_CHECK_CLOSE(compositeShape.getArea(), copiedComposite.getArea(), ERROR_VAL);
  BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, ERROR_VAL);
  BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(compositeShapeThrowingExeptions)
{
  tsai::Rectangle rectTest({ 5, 6 }, 2, 14);
  tsai::Circle circleTest({ 4, 1 }, 10);
  tsai::Triangle triangleTest({ 0, 0 }, { 10, 10 }, { 20, -10 });
  tsai::CompositeShape compositeShape(std::make_shared<tsai::Rectangle>(rectTest));
  compositeShape.add(std::make_shared<tsai::Circle>(circleTest));
  compositeShape.add(std::make_shared<tsai::Triangle>(triangleTest));

  BOOST_CHECK_THROW(compositeShape.remove(-5), std::out_of_range);
  BOOST_CHECK_THROW(compositeShape.remove(5), std::out_of_range);

  BOOST_CHECK_THROW(compositeShape[5], std::out_of_range);
  BOOST_CHECK_THROW(compositeShape[-5], std::out_of_range);

  compositeShape.remove(2);
  BOOST_CHECK_THROW(compositeShape[2], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(emptyCompositeShapeThrowingExeptions)
{
  tsai::CompositeShape emptyCompositeShape;

  BOOST_CHECK_THROW(emptyCompositeShape.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(emptyCompositeShape.move(4, 7), std::logic_error);
  BOOST_CHECK_THROW(emptyCompositeShape.move({ 1, 1 }), std::logic_error);
  BOOST_CHECK_THROW(emptyCompositeShape.scale(2), std::logic_error);
}

BOOST_AUTO_TEST_SUITE_END();
