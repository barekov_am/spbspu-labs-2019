#include "partition.hpp"
#include <cmath>

bool tsai::overlap(tsai::rectangle_t shape1, tsai::rectangle_t shape2)
{
  const double distanceBetweenCentersX = fabs(shape1.pos.x - shape2.pos.x);
  const double distanceBetweenCentersY = fabs(shape1.pos.y - shape2.pos.y);

  const double lengthX = (shape1.width + shape2.width) / 2;
  const double lengthY = (shape1.height + shape2.height) / 2;

  const bool firstCondition = (distanceBetweenCentersX < lengthX);
  const bool secondCondition = (distanceBetweenCentersY < lengthY);

  return firstCondition && secondCondition;
}

tsai::Matrix tsai::split(std::unique_ptr<tsai::Shape::shapePtr[]> & shapes, size_t size)
{
  Matrix matrix;

  for (size_t i = 0; i < size; i++)
  {
    size_t rows = 0;
    size_t columns = 0;
    for (size_t j = 0; j < matrix.getRows(); j++)
    {
      for (size_t k = 0; k < matrix.getRowSize(j); k++)
      {
        if (overlap(shapes[i]->getFrameRect(), matrix[j][k]->getFrameRect()))
        {
          rows++;
          break;
        }
        if (k == matrix.getRowSize(j) - 1)
        {
          rows = j;
          columns = matrix.getRowSize(j);
        }
      }
      if (rows == j)
      {
        break;
      }
    }
    matrix.add(shapes[i], rows, columns);
  }
  return matrix;
}

tsai::Matrix tsai::split(tsai::CompositeShape & compositeShape)
{
  std::unique_ptr<Shape::shapePtr[]> temp(std::make_unique<Shape::shapePtr[]>(compositeShape.getSize()));
  for (size_t i = 0; i < compositeShape.getSize(); i++)
  {
    temp[i] = compositeShape[i];
  }
  Matrix matrix = split(temp, compositeShape.getSize());
  return matrix;
}
