#include "matrix.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

tsai::Matrix::Matrix() :
  rows_(0),
  columns_(0)
{
}

tsai::Matrix::Matrix(const Matrix &shapesMatrix) :
  rows_(shapesMatrix.rows_),
  columns_(shapesMatrix.columns_),
  shapes_(std::make_unique<Shape::shapePtr[]>(shapesMatrix.rows_ *shapesMatrix.columns_))
{
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    shapes_[i] = shapesMatrix.shapes_[i];
  }
}

tsai::Matrix::Matrix(tsai::Matrix &&shapesMatrix) :
  rows_(shapesMatrix.rows_),
  columns_(shapesMatrix.columns_),
  shapes_(std::move(shapesMatrix.shapes_))
{
  shapesMatrix.rows_ = 0;
  shapesMatrix.columns_ = 0;
}

tsai::Matrix &tsai::Matrix::operator =(const Matrix &shapesMatrix)
{
  if (this != &shapesMatrix)
  {
    rows_ = shapesMatrix.rows_;
    columns_ = shapesMatrix.columns_;
    std::unique_ptr<Shape::shapePtr[]> temp(std::make_unique<Shape::shapePtr[]>(rows_ * columns_));
    for (size_t i = 0; i < (rows_ * columns_); i++)
    {
      temp[i] = shapesMatrix.shapes_[i];
    }

    shapes_.swap(temp);
  }

  return *this;
}

tsai::Matrix &tsai::Matrix::operator =(Matrix &&shapesMatrix)
{
  if (this != &shapesMatrix)
  {
    rows_ = shapesMatrix.rows_;
    columns_ = shapesMatrix.columns_;
    shapes_ = std::move(shapesMatrix.shapes_);
    shapesMatrix.rows_ = 0;
    shapesMatrix.columns_ = 0;
  }

  return *this;
}

std::unique_ptr<tsai::Shape::shapePtr[]> tsai::Matrix::operator [](size_t index) const
{
  if (index >= rows_)
  {
    throw std::out_of_range("Array index out of bounds");
  }

  std::unique_ptr<Shape::shapePtr[]> temp(std::make_unique<Shape::shapePtr[]>(columns_));
  for (size_t i = 0; i < getRowSize(index); i++)
  {
    temp[i] = shapes_[index * columns_ + i];
  }

  return temp;
}

bool tsai::Matrix::operator==(const Matrix &shapesMatrix) const
{
  if ((rows_ != shapesMatrix.rows_) || (columns_ != shapesMatrix.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (shapes_[i] != shapesMatrix.shapes_[i])
    {
      return false;
    }
  }

  return true;
}

bool tsai::Matrix::operator!=(const Matrix &shapesMatrix) const
{
  return !(*this == shapesMatrix);
}

void tsai::Matrix::add(Shape::shapePtr shape, size_t row, size_t column)
{
  if (!shape)
  {
    throw std::invalid_argument("Pointer have to be not equal nullptr");
  }
  size_t newRow = (row == rows_) ? (rows_ + 1) : (rows_);
  size_t newColumn = (column == columns_) ? (columns_ + 1) : (columns_);

  std::unique_ptr<Shape::shapePtr[]> temp(std::make_unique<Shape::shapePtr[]>(newColumn * newRow));

  for (size_t i = 0; i < rows_; i++)
  {
    for (size_t j = 0; j < columns_; j++)
    {
      temp[i * newColumn + j] = shapes_[i * columns_ + j];
    }
  }
  temp[newColumn * row + column] = shape;
  shapes_ = std::move(temp);
  rows_ = newRow;
  columns_ = newColumn;
}

std::size_t tsai::Matrix::getRows() const
{
  return rows_;
}

std::size_t tsai::Matrix::getColumns() const
{
  return columns_;
}

std::size_t tsai::Matrix::getRowSize(size_t row) const
{
  if (row >= rows_)
  {
    return 0;
  }

  for (size_t i = 0; i < columns_; i++)
  {
    if (shapes_[row * columns_ + i] == nullptr)
    {
      return i;
    }
  }

  return columns_;
}
