#ifndef SHAPE_HPP_INCLUDED
#define SHAPE_HPP_INCLUDED
#include "base-types.hpp"
#include <memory>

namespace tsai
{
  class Shape
  {
  public:
    using shapePtr = std::shared_ptr<Shape>;

    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual void printInfo() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual point_t getPos() const = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void move(const point_t &pos) = 0;
    virtual void scale(double times) = 0;
    virtual void rotate(double angle) = 0;
  };
}
#endif
