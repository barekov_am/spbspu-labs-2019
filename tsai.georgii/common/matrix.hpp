#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"
#include <memory>

namespace tsai
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &shapesMatrix);
    Matrix(Matrix &&shapesMatrix);
    ~Matrix() = default;

    Matrix &operator =(const Matrix &shapesMatrix);
    Matrix &operator =(Matrix &&shapesMatrix);
    std::unique_ptr<Shape::shapePtr[]>  operator [](size_t index) const;
    bool operator ==(const Matrix &shapesMatrix) const;
    bool operator !=(const Matrix &shapesMatrix) const;

    void add(Shape::shapePtr shape, size_t row, size_t column);
    size_t getRows() const;
    size_t getColumns() const;
    size_t getRowSize(size_t row) const;

  private:
    size_t rows_;
    size_t columns_;
    std::unique_ptr<Shape::shapePtr[]> shapes_;
  };
};

#endif
