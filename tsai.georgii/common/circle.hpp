#ifndef CIRCLE_HPP_INCLUDED
#define CIRCLE_HPP_INCLUDED
#include "shape.hpp"

namespace tsai
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t &pos, double radius);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printInfo() const override;
    point_t getPos() const override;
    double getRadius() const;
    void scale(double times) override;
    void move(double dx, double dy) override;
    void move(const point_t &pos) override;
    void rotate(double /*angle*/) override;

  private:
    point_t pos_;
    double radius_;
  };
}
#endif
