#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double ERROR_VAL = 0.01;

BOOST_AUTO_TEST_SUITE(testRectangle)

BOOST_AUTO_TEST_CASE(rectangleConstructor)
{
  BOOST_CHECK_THROW(tsai::Rectangle({ 0, 0 }, 100, -50), std::invalid_argument);
  BOOST_CHECK_THROW(tsai::Rectangle({ 0, 0 }, -100, 50), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleMoving)
{
  tsai::Rectangle rectTest({ 0, 0 }, 100, 50);
  tsai::point_t pointTest = { 20, 20 };
  rectTest.move(pointTest);
  BOOST_CHECK_CLOSE(rectTest.getPos().x, pointTest.x, ERROR_VAL);
  BOOST_CHECK_CLOSE(rectTest.getPos().y, pointTest.y, ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(rectangleIncorrectScaling)
{
  tsai::Rectangle rectTest({ 0, 0 }, 100, 50);
  BOOST_CHECK_THROW(rectTest.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangleScaling)
{
  tsai::Rectangle rectTest({ 0, 0 }, 100, 50);
  const double timesTest = 2;
  const double widthTest = rectTest.getWidth() * timesTest;
  const double heightTest = rectTest.getHeight() * timesTest;
  rectTest.scale(timesTest);
  BOOST_CHECK_CLOSE(rectTest.getWidth(), widthTest, ERROR_VAL);
  BOOST_CHECK_CLOSE(rectTest.getHeight(), heightTest, ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(rectangleMovedArea)
{
  tsai::Rectangle rectTest({ 0, 0 }, 100, 50);
  const double areaTest = rectTest.getArea();
  rectTest.move({ 20, 20 });
  BOOST_CHECK_CLOSE(areaTest, rectTest.getArea(), ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(rectangleScaledArea)
{
  tsai::Rectangle rectTest({ 0, 0 }, 100, 50);
  const double areaTest = rectTest.getArea();
  const double times = 2;
  rectTest.scale(times);
  BOOST_CHECK_CLOSE(rectTest.getArea(), areaTest * times * times, ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(rectangleFrameRect)
{
  tsai::Rectangle rectTest({ 0, 0 }, 100, 50);
  tsai::rectangle_t frameRectTest = rectTest.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectTest.pos.x, rectTest.getPos().x, ERROR_VAL);
  BOOST_CHECK_CLOSE(frameRectTest.pos.y, rectTest.getPos().y, ERROR_VAL);
  BOOST_CHECK_CLOSE(frameRectTest.width, rectTest.getWidth(), ERROR_VAL);
  BOOST_CHECK_CLOSE(frameRectTest.height, rectTest.getHeight(), ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(rectangleRotate)
{
  tsai::Rectangle rectOne({ 4, 5 }, 6, 7);
  double areaTest = rectOne.getArea();
  rectOne.rotate(45);
  BOOST_CHECK_CLOSE(areaTest, rectOne.getArea(), ERROR_VAL);
}


BOOST_AUTO_TEST_SUITE_END();
