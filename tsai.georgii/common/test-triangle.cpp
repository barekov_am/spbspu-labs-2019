#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "triangle.hpp"

const double ERROR_VAL = 0.01;

BOOST_AUTO_TEST_SUITE(testTriangle)

BOOST_AUTO_TEST_CASE(triangleConstructor)
{
  BOOST_CHECK_THROW(tsai::Triangle({ 10, 10 }, { 10, 10 }, { 20, 20 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(triangleMoving)
{
  tsai::Triangle triangleTest({ 10, 10 }, { 20, 20 }, { 30, 10 });
  tsai::point_t pointTest = { 20, 20 };
  triangleTest.move(pointTest);
  BOOST_CHECK_CLOSE(triangleTest.getPos().x, pointTest.x, ERROR_VAL);
  BOOST_CHECK_CLOSE(triangleTest.getPos().y, pointTest.y, ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(triangleIncorrectScaling)
{
  tsai::Triangle triangleTest({ 10, 10 }, { 20, 20 }, { 30, 10 });
  BOOST_CHECK_THROW(triangleTest.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(triangleScaling)
{
  tsai::Triangle triangleTest({ 10, 10 }, { 20, 20 }, { 30, 10 });
  const double areaTest = triangleTest.getArea();
  const double times = 2;
  triangleTest.scale(times);
  BOOST_CHECK_CLOSE(triangleTest.getArea(), areaTest * times * times, ERROR_VAL);

}

BOOST_AUTO_TEST_CASE(triangleMovedArea)
{
  tsai::Triangle triangleTest({ 10, 10 }, { 20, 20 }, { 30, 10 });
  const double areaTest = triangleTest.getArea();
  triangleTest.move({ 20, 20 });
  BOOST_CHECK_CLOSE(triangleTest.getArea(), areaTest, ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(triangleScaledArea)
{
  tsai::Triangle triangleTest({ 10, 10 }, { 20, 20 }, { 30, 10 });
  const double areaTest = triangleTest.getArea();
  const double times = 2;
  triangleTest.scale(times);
  BOOST_CHECK_CLOSE(triangleTest.getArea(), areaTest * times * times, ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(triangleRotate)
{
  tsai::Triangle triangleTest({ 4, 5 }, { 6, 7 }, { 9, 8 });
  double areaTest = triangleTest.getArea();
  triangleTest.rotate(45);
  BOOST_CHECK_CLOSE(areaTest, triangleTest.getArea(), ERROR_VAL);
}


BOOST_AUTO_TEST_SUITE_END();
