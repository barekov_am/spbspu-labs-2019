#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"
#include <memory>

namespace tsai
{
  bool overlap(tsai::rectangle_t shape1, tsai::rectangle_t shape2);
  Matrix split(std::unique_ptr<tsai::Shape::shapePtr[]> &shapes, size_t size);
  Matrix split(CompositeShape &compositeShape);
}

#endif
