#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(partitionMetodsTests)

BOOST_AUTO_TEST_CASE(partitionTest)
{
  tsai::Rectangle rectangle({ 2, 4.5 }, 1, 2);
  tsai::Rectangle rectangle1({ 3.5, 2 }, 2, 5);
  tsai::Circle circle({ 6, 4 }, 2);

  tsai::CompositeShape compositeShape(std::make_shared<tsai::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<tsai::Rectangle>(rectangle1));
  compositeShape.add(std::make_shared<tsai::Circle>(circle));

  tsai::Matrix matrix = tsai::split(compositeShape);

  BOOST_CHECK_EQUAL(matrix.getRows(), 2);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 2);
}

BOOST_AUTO_TEST_CASE(intersectionTest)
{
  tsai::Rectangle rectangle({ 2, 4.5 }, 1, 2);
  tsai::Rectangle rectangle1({ 3.5, 2 }, 2, 5);
  tsai::Circle circle({ 6, 4 }, 2);

  BOOST_CHECK(!tsai::overlap(rectangle.getFrameRect(), rectangle1.getFrameRect()));
  BOOST_CHECK(!tsai::overlap(rectangle.getFrameRect(), circle.getFrameRect()));
  BOOST_CHECK(tsai::overlap(rectangle1.getFrameRect(), circle.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
