#include "circle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

tsai::Circle::Circle(const point_t &pos, double radius) :
  pos_(pos),
  radius_(radius)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("Radius must be > 0");
  }
}

void tsai::Circle::rotate(double /*angle*/)
{
}

double tsai::Circle::getArea()const
{
  return M_PI * radius_ * radius_;
}

tsai::rectangle_t tsai::Circle::getFrameRect() const
{
  return { pos_, 2 * radius_, 2 * radius_ };
}

tsai::point_t tsai::Circle::getPos() const
{
  return pos_;
}

double tsai::Circle::getRadius() const
{
  return radius_;
}

void tsai::Circle::printInfo() const
{
  rectangle_t framingRect = getFrameRect();
  std::cout << "\n Circle frame width : " << framingRect.width;
  std::cout << "\n Circle frame height : " << framingRect.height;
  std::cout << "\n Centre : ( x: " << pos_.x << ", y: " << pos_.y << " )";
  std::cout << "\n Area is " << getArea() << "\n\n";
}

void tsai::Circle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void tsai::Circle::move(const point_t &pos)
{
  pos_ = pos;
}

void tsai::Circle::scale(double times)
{
  if (times <= 0)
  {
    throw std::invalid_argument("Enter correct 'times'");
  }
  else
  {
    radius_ *= times;
  }
}
