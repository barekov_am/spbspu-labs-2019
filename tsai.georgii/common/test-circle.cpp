#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

const double ERROR_VAL = 0.01;

BOOST_AUTO_TEST_SUITE(testCircle)

BOOST_AUTO_TEST_CASE(circleConstructor)
{
  BOOST_CHECK_THROW(tsai::Circle({ 0, 0 }, -50), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circleMoving)
{
  tsai::Circle circleTest({ 0, 0 }, 50);
  tsai::point_t pointTest = { 20, 20 };
  circleTest.move(pointTest);
  BOOST_CHECK_CLOSE(circleTest.getPos().x, pointTest.x, ERROR_VAL);
  BOOST_CHECK_CLOSE(circleTest.getPos().y, pointTest.y, ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(circleIncorrectScaling)
{
  tsai::Circle circleTest({ 0, 0 }, 50);
  BOOST_CHECK_THROW(circleTest.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circleScaling)
{
  tsai::Circle circleTest({ 0, 0 }, 50);
  const double timesTest = 2;
  const double radiusTest = circleTest.getRadius() * timesTest;
  circleTest.scale(timesTest);
  BOOST_CHECK_CLOSE(circleTest.getRadius(), radiusTest, ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(circleMovedArea)
{
  tsai::Circle circleTest({ 0, 0 }, 50);
  const double areaTest = circleTest.getArea();
  circleTest.move({ 20, 20 });
  BOOST_CHECK_CLOSE(areaTest, circleTest.getArea(), ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(circleScaledArea)
{
  tsai::Circle circleTest({ 0, 0 }, 50);
  const double areaTest = circleTest.getArea();
  const double times = 2;
  circleTest.scale(times);
  BOOST_CHECK_CLOSE(circleTest.getArea(), areaTest * times * times, ERROR_VAL);
}

BOOST_AUTO_TEST_CASE(circleFrameRect)
{
  tsai::Circle circleTest({ 0, 0 }, 50);
  tsai::rectangle_t frameRectTest = circleTest.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectTest.pos.x, circleTest.getPos().x, ERROR_VAL);
  BOOST_CHECK_CLOSE(frameRectTest.pos.y, circleTest.getPos().y, ERROR_VAL);
  BOOST_CHECK_CLOSE(frameRectTest.width, circleTest.getRadius() * 2, ERROR_VAL);
  BOOST_CHECK_CLOSE(frameRectTest.height, circleTest.getRadius() * 2, ERROR_VAL);
}

BOOST_AUTO_TEST_SUITE_END();
