#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

tsai::Rectangle::Rectangle(const point_t &pos, double width, double height) :
  pos_(pos),
  width_(width),
  height_(height),
  angle_(0)
{
  if ((width_ <= 0) || (height_ <= 0))
  {
    throw std::invalid_argument("Width and height must be > 0");
  }
}

double tsai::Rectangle::getArea()const
{
  return width_ * height_;
}

tsai::rectangle_t tsai::Rectangle::getFrameRect() const
{
  double width = width_ * std::fabs(cos((M_PI * angle_) / 180)) + height_ * std::fabs(sin((M_PI * angle_) / 180));
  double height = height_ * std::fabs(cos((M_PI * angle_) / 180)) + width_ * std::fabs(sin((M_PI * angle_) / 180));
  return { pos_, width, height };
}

void tsai::Rectangle::rotate(double angle)
{
  while (angle >= 360)
  {
    angle -= 360;
  }
  angle_ += angle;
}

tsai::point_t tsai::Rectangle::getPos() const
{
  return pos_;
}

double tsai::Rectangle::getWidth() const
{
  return width_;
}

double tsai::Rectangle::getHeight() const
{
  return height_;
}

void tsai::Rectangle::printInfo() const
{
  rectangle_t framingRect = getFrameRect();
  std::cout << "\n Frame width : " << framingRect.width;
  std::cout << "\n Frame height : " << framingRect.height;
  std::cout << "\n Centre : ( x: " << pos_.x << ", y: " << pos_.y << " )";
  std::cout << "\n Area : " << getArea() << "\n\n";
}

void tsai::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void tsai::Rectangle::move(const point_t &pos)
{
  pos_ = pos;
}

void tsai::Rectangle::scale(double times)
{
  if (times <= 0)
  {
    throw std::invalid_argument("Enter correct 'times'");
  }
  else
  {
    width_ *= times;
    height_ *= times;
  }
}
