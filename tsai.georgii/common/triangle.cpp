#include "triangle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>
#include <algorithm>

tsai::Triangle::Triangle(const point_t& a, const point_t& b, const point_t& c) :
  pos_({ (a.x + b.x + c.x) / 3, (a.y + b.y + c.y) / 3 }),
  a_(a),
  b_(b),
  c_(c)
{
  if (getArea() <= 0)
  {
    throw std::invalid_argument("Coordinates of vertexes can not be equal");
  }
}

tsai::point_t rotatePoint(tsai::point_t point, double angle)
{
  double x = point.x * cos(M_PI * angle / 180) - point.y * sin(M_PI * angle / 180);
  double y = point.y * cos(M_PI * angle / 180) + point.x * sin(M_PI * angle / 180);
  return { x, y };
}

void tsai::Triangle::rotate(double angle)
{
  while (angle >= 360)
  {
    angle -= 360;
  }
  a_ = rotatePoint(a_, angle);
  b_ = rotatePoint(b_, angle);
  c_ = rotatePoint(c_, angle);
}

double tsai::Triangle::getArea() const
{
  return std::fabs((b_.x - a_.x) * (c_.y - a_.y) - (b_.y - a_.y) * (c_.x - a_.x)) / 2;
}

tsai::rectangle_t tsai::Triangle::getFrameRect() const
{
  point_t max = { std::max(std::max(a_.x, b_.x), c_.x), std::max(std::max(a_.y, b_.y), c_.y) };
  point_t min = { std::min(std::min(a_.x, b_.x), c_.x), std::min(std::min(a_.y, b_.y), c_.y) };
  return { { (max.x + min.x) / 2, (max.y + min.y) / 2 }, max.x - min.x, max.y - min.y };
}

tsai::point_t tsai::Triangle::getPos() const
{
  return pos_;
}

void tsai::Triangle::printInfo() const
{
  rectangle_t framingRect = getFrameRect();
  std::cout << "Triangle vertex A is at ( " << a_.x << ';' << a_.y << " )";
  std::cout << "\n vertex B is at ( " << b_.x << ';' << b_.y << " )";
  std::cout << "\n vertex C is at ( " << c_.x << ';' << c_.y << " )";
  std::cout << "\n Centre is at ( " << pos_.x << "," << pos_.y << " )";
  std::cout << "\n Frame width : " << framingRect.width;
  std::cout << "\n Frame height : " << framingRect.height;
  std::cout << "\n Area : " << getArea() << "\n\n";
}

void tsai::Triangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;

  a_.x += dx;
  a_.y += dy;
  b_.x += dx;
  b_.y += dy;
  c_.x += dx;
  c_.y += dy;
}

void tsai::Triangle::move(const point_t& pos)
{
  move(pos.x - pos_.x, pos.y - pos_.y);
}

void tsai::Triangle::scale(double times)
{
  if (times <= 0)
  {
    throw std::invalid_argument("Enter correct 'times'");
  }
  else
  {
    a_.x = pos_.x + (a_.x - pos_.x) * times;
    a_.y = pos_.y + (a_.y - pos_.y) * times;
    b_.x = pos_.x + (b_.x - pos_.x) * times;
    b_.y = pos_.y + (b_.y - pos_.y) * times;
    c_.x = pos_.x + (c_.x - pos_.x) * times;
    c_.y = pos_.y + (c_.y - pos_.y) * times;
  }
}
