#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
  tsai::Rectangle rectOne({ 0, 0 }, 100, 50);
  tsai::Circle circleOne({ 0, 0 }, 50);
  tsai::Triangle triangleOne({ 10, 10 }, { 20, 20 }, { 30, 10 });

  tsai::CompositeShape compositeShape(std::make_shared<tsai::Rectangle>(rectOne));
  compositeShape.printInfo();

  compositeShape.add(std::make_shared<tsai::Circle>(circleOne));
  compositeShape.add(std::make_shared<tsai::Triangle>(triangleOne));
  std::cout << "Figures have been added: \n";
  compositeShape.printInfo();

  compositeShape.move(4.5, 1.5);
  std::cout << "Composite shape has been moved by dx and dy\n";
  compositeShape.printInfo();

  compositeShape.move({ 12.0, 4 });
  std::cout << "Composite shape has been moved by coordinates\n";
  compositeShape.printInfo();

  compositeShape.scale(4);
  std::cout << "Composite shape has been scaled\n";
  compositeShape.printInfo();

  compositeShape.remove(0);
  std::cout << "First figure in composite shape has been deleted\n";
  compositeShape.printInfo();

  return 0;
}
