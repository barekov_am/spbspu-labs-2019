#ifndef STRATEGY_H
#define STRATEGY_H
#include <iterator>

template  <class T>
struct Brackets
{
  static typename T::reference get(T &collection, size_t index)
  {
    return collection[index];
  }

  static size_t begin(const T &)
  {
    return 0;
  }

  static size_t end(const T &collection)
  {
    return collection.size();
  }

  static size_t next(size_t index)
  {
    return index + 1;
  }
};

template  <class T>
struct At
{
  static typename T::reference get(T &collection, size_t index)
  {
    return collection.at(index);
  }

  static size_t begin(const T &)
  {
    return 0;
  }

  static size_t end(const T &collection)
  {
    return collection.size();
  }

  static size_t next(size_t index)
  {
    return index + 1;
  }
};

template  <class T>
struct Iterator
{
  static typename T::reference get(T &, typename T::iterator &index)
  {
    return *index;
  }

  static typename T::iterator begin(T &collection)
  {
    return collection.begin();
  }

  static typename T::iterator end(T &collection)
  {
    return collection.end();
  }

  static typename T::iterator next(typename T::iterator index)
  {
    return ++index;
  }
};

#endif
