#include <iostream>
#include <fstream>
#include <memory>
#include <stdexcept>
#include <vector>

const size_t INITIAL_SIZE = 1;

void task2 (const char *file)
{
  std::ifstream input(file);
  if (!input)
  {
    throw std::ios_base::failure("Open failed");
  }

  size_t size = INITIAL_SIZE;

  using array_ptr = std::unique_ptr<char[], decltype(&free)>;
  array_ptr array(static_cast<char *>(malloc(size)), &free);

  if (!array)
  {
    throw std::runtime_error("Memory cannot be allocated");
  }

  size_t count = 0;

  while (!input.eof())
  {

    input.read(&array[count], INITIAL_SIZE);
    count += input.gcount();

    if (input.gcount() == INITIAL_SIZE)
    {

      size += INITIAL_SIZE;
      array_ptr tmpArray(static_cast<char *>(realloc(array.get(), size)), &free);

      if (!tmpArray)
      {
        throw std::runtime_error("Memory cannot be reallocated");
      }

      array.release();
      std::swap(array, tmpArray);

    }

    if (!input.eof() && input.fail())
    {
      throw std::runtime_error("Data reading failed");
    }

  }

  input.close();

  if (input.is_open())
  {
    throw std::runtime_error("Closing file failed");
  }

  std::vector<char>(&array[0], &array[count]);

  for(size_t i = 0; i < count; i++)
  {
    std::cout << array[i];
  }
}
