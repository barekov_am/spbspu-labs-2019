#include <vector>
#include <iterator>
#include "tool.hpp"

void task3()
{
  int inputNumber = -1;
  std::vector<int> vector;

  while (std::cin >> inputNumber)
  {
    if (inputNumber == 0)
    {
      break;
    }
    vector.push_back(inputNumber);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Input failed");
  }

  if (vector.empty())
  {
    return;
  }

  if (inputNumber != 0)
  {
    throw std::invalid_argument("Last input number must be 0");
  }

  std::vector<int>::iterator i = vector.begin();
  switch(vector.back())
  {
    case 1:
    {
      while (i != vector.end())
      {
         ((*i % 2) == 0) ? vector.erase(i) : ++i;
      }
      break;
    }
    case 2:
    {
      while (i != vector.end())
      {
        i = ((*i % 3) == 0) ? vector.insert(++i, 3, 1) + 3 : ++i;
      }
      break;
    }
  }

  print(vector);
}
