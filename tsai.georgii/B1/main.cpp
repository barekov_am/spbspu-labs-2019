#include <iostream>
#include <stdexcept>
#include <string>
#include <ctime>
#include <fstream>
#include <cstring>

void task1(const char *direction);
void task2(const char *file);
void task3();
void task4(const char *direction, const int size);

int main(int argc, char *argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4))
    {
      std::cerr << "Wrong number of parameters";
      return 1;
    }
    char *endStr = nullptr;
    int number = std::strtol(argv[1], &endStr, 10);
    switch (number)
    {
    case 1:
    {
      if (argc != 3)
      {
        std::cerr << "Wrong number of parameters";
        return 1;
      }
      task1(argv[2]);
      break;
    }

    case 2:
     {
      if (argc != 3)
      {
        std::cerr << "Wrong number of parameters";
        return 1;
      }
      task2(argv[2]);
      break;
    }

    case 3:
    {
      if (argc != 2)
      {
        std::cerr << "Wrong number of parameters";
        return 1;
      }
      task3();
      break;
    }

    case 4:
    {
      if (argc != 4)
      {
        std::cerr << "Wrong number of parameters";
        return 1;
      }
      int size = std::strtol(argv[3], &endStr, 10);
      task4(argv[2], size);
      break;
    }

    default:
    {
      std::cerr << "Wrong task number";
      return 1;
    }
    }
  return 0;
  }

  catch (const std::exception &e)
  {
    std::cerr << e.what();
    return 1;
  }
}
