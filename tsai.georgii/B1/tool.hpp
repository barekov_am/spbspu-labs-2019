#ifndef TOOLS_H
#define TOOLS_H

#include "strategy.hpp"
#include <cstring>
#include <ctime>
#include <cstdlib>
#include <iostream>

bool checkDirection(const char *);

void fillRandom(double *array, int size);

template <class T>
void print(T &collection)
{
  for (const auto &x: collection)
  {
    std::cout << x << " ";
  }
  std::cout << '\n';
}

template <template <class T> class Strategy, class T>
void sort(T &collection, bool ascending)
{
  const auto begin = Strategy<T>::begin(collection);
  const auto end = Strategy<T>::end(collection);
  for (auto i = begin; i != end; ++i)
  {
    auto tmp = i;
    for (auto j = Strategy<T>::next(i); j != end; ++j)
    {
      if ((ascending)
          && (Strategy<T>::get(collection, tmp) > Strategy<T>::get(collection, j)))
      {
        tmp = j;
      }
      if ((!ascending)
          && (Strategy<T>::get(collection, tmp) < Strategy<T>::get(collection, j)))
      {
        tmp = j;
      }
    }
    if (tmp != i)
    {
      std::swap(Strategy<T>::get(collection, tmp), Strategy<T>::get(collection, i));
    }
  }
}

#endif
