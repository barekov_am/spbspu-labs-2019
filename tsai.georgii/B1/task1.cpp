#include <vector>
#include <forward_list>
#include <fstream>
#include "tool.hpp"

void task1(const char *direction)
{
  bool dir = checkDirection(direction);
  std::vector<int> vector;
  int num = 0;

  while (std::cin >> num)
  {
    vector.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Input failed");
  }

  if (vector.empty())
  {
    return;
  }

  std::vector<int> vectorAt = vector;
  std::forward_list<int> list(vector.begin(), vector.end());

  sort<Brackets>(vector, dir);
  sort<At>(vectorAt, dir);
  sort<Iterator>(list, dir);

  print(vector);
  print(vectorAt);
  print(list);
}
