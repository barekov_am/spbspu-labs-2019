#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

int main()
{
  Rectangle rectangle({11, 5}, 10, 2);
  Shape *shape = &rectangle;
  shape->printInformation();
  shape->move(7, -2);
  shape->printInformation();
  shape->move({21, 20});
  shape->printInformation();

  Circle circle({9, 11}, 6);
  shape = &circle;
  shape->printInformation();
  shape->move(-3.11, 6.23);
  shape->printInformation();
  shape->move({10.5, 9.3});
  shape->printInformation();

  Triangle triangle({7, 1}, {1.25, 6.2}, {10, 11.3});
  shape = &triangle;
  shape->printInformation();
  shape->move(10, 11);
  shape->printInformation();
  shape->move({5, 9});
  shape->printInformation();

  const int num_of_sides = 6;
  const point_t vertices[num_of_sides] = {{1.0, 2}, {3.2, 2}, {4, 3.75}, {3, 6.3}, {0.9, 6}, {-1.25, 3}};
  Polygon polygon(num_of_sides, vertices);
  shape = &polygon;
  shape->printInformation();
  shape->move(5.1, 1.25);
  shape->printInformation();
  shape->move({-0.9, 0.3});
  shape->printInformation();

  return 0;
}
