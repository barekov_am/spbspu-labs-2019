#include "triangle.hpp"
#include <iostream>
#include <cmath>
#include <cassert>

Triangle::Triangle(const point_t &vertexA, const point_t &vertexB, const point_t &vertexC):
  vertexA_(vertexA),
  vertexB_(vertexB),
  vertexC_(vertexC)
{
  assert(getArea() > 0);
}

point_t Triangle::getCenter() const
{
  return {(vertexA_.x + vertexB_.x + vertexC_.x) / 3, (vertexA_.y + vertexB_.y + vertexC_.y) / 3};
}

double Triangle::getArea() const
{
  return std::abs(0.5 * ((vertexB_.x - vertexA_.x) * (vertexC_.y - vertexA_.y)
      - (vertexC_.x - vertexA_.x) * (vertexB_.y - vertexA_.y)));
}

rectangle_t Triangle::getFrameRect() const
{
  double x_min = std::min(std::min(vertexA_.x, vertexB_.x), vertexC_.x);
  double x_max = std::max(std::max(vertexA_.x, vertexB_.x), vertexC_.x);
  double y_min = std::min(std::min(vertexA_.y, vertexB_.y), vertexC_.y);
  double y_max = std::max(std::max(vertexA_.y, vertexB_.y), vertexC_.y);
  return {getCenter(), x_max - x_min, y_max - y_min};
}

void Triangle::move(const point_t &position)
{
  move(position.x - getCenter().x, position.y - getCenter().y);
}

void Triangle::move(const double &dx, const double &dy)
{
  vertexA_.x += dx;
  vertexA_.y += dy;
  vertexB_.x += dx;
  vertexB_.y += dy;
  vertexC_.x += dx;
  vertexC_.y += dy;
}

void Triangle::printInformation() const
{
  std::cout << "triangle information:\n";
  std::cout << "vertices: (" << vertexA_.x << "; " << vertexA_.y << "); ("
            << vertexB_.x << "; " << vertexB_.y << "); ("
            << vertexC_.x << "; " << vertexC_.y << ")\n";
  std::cout << "center: (" << getCenter().x << "; " << getCenter().y << ")\n";
  std::cout << "area = " << getArea() << "\n";
  std::cout << "triangle's frame rectangle information:" << "\n";
  rectangle_t frameRectangle = getFrameRect();
  std::cout << "center: (" << frameRectangle.pos.x << "; " << frameRectangle.pos.y << ")\n";
  std::cout << "width = " << frameRectangle.width << "\n";
  std::cout << "height = " << frameRectangle.height << "\n\n";
}
