#include "polygon.hpp"
#include <iostream>
#include <cmath>
#include "triangle.hpp"

Polygon::Polygon(const Polygon &other):
  num_of_sides_(other.num_of_sides_),
  vertices_(new point_t[num_of_sides_])
{
  for (int i = 0; i < num_of_sides_; i++) {
    vertices_[i] = other.vertices_[i];
  }
}

Polygon::Polygon(Polygon &&other) noexcept:
  num_of_sides_(other.num_of_sides_),
  vertices_(other.vertices_)
{
  other.num_of_sides_ = 0;
  other.vertices_ = nullptr;
}

Polygon::Polygon(const int &num_of_sides, const point_t *vertices):
  num_of_sides_(num_of_sides),
  vertices_(new point_t[num_of_sides])
{
  if (num_of_sides_ < 3) {
    delete[] vertices_;
    throw std::invalid_argument("Number of sides must be at least 3");
  }
  if (vertices == nullptr) {
    delete[] vertices_;
    throw std::invalid_argument("Input pointer is null");
  }
  for (int i = 0; i < num_of_sides_; i++) {
    vertices_[i] = vertices[i];
  }
  if (!isConvex()) {
    delete[] vertices_;
    throw std::invalid_argument("Polygon is concave");
  }
}

Polygon::~Polygon()
{
  delete[] vertices_;
}

Polygon &Polygon::operator=(const Polygon &other)
{
  if (this != &other) {
    num_of_sides_ = other.num_of_sides_;
    delete[] vertices_;
    vertices_ = new point_t[num_of_sides_];
    for (int i = 0; i < num_of_sides_; i++) {
      vertices_[i] = other.vertices_[i];
    }
  }
  return *this;
}

Polygon &Polygon::operator=(Polygon &&other) noexcept
{
  if (this != &other) {
    num_of_sides_ = other.num_of_sides_;
    other.num_of_sides_ = 0;
    delete[] vertices_;
    vertices_ = other.vertices_;
    other.vertices_ = nullptr;
  }
  return *this;
}

point_t Polygon::getCenter() const
{
  double sum_x = 0;
  double sum_y = 0;
  for (int i = 0; i < num_of_sides_; i++) {
    sum_x += vertices_[i].x;
    sum_y += vertices_[i].y;
  }
  return {sum_x / num_of_sides_, sum_y / num_of_sides_};
}

bool Polygon::isConvex() const
{
  int sign = 0;
  for (int i = 0; i < num_of_sides_; i++) {
    const int j = (i + 1) % num_of_sides_;
    const int k = (i + 2) % num_of_sides_;
    const double determinant = (vertices_[j].x - vertices_[i].x) * (vertices_[k].y - vertices_[j].y)
        - (vertices_[k].x - vertices_[j].x) * (vertices_[j].y - vertices_[i].y);
    if (determinant != 0) {
      if (sign == 0) {
        sign = determinant > 0 ? 1 : -1;
      } else if ((sign * determinant) < 0) {
        return false;
      }
    }
  }
  return sign != 0;
}

double Polygon::getArea() const
{
  double area = 0;
  for (int i = 0; i < num_of_sides_; i++) {
    Triangle triangle(getCenter(), vertices_[i], vertices_[(i + 1) % num_of_sides_]);
    area += triangle.getArea();
  }
  return area;
}

rectangle_t Polygon::getFrameRect() const
{
  double x_min = vertices_[0].x;
  double x_max = vertices_[0].x;
  double y_min = vertices_[0].y;
  double y_max = vertices_[0].y;
  for (int i = 1; i < num_of_sides_; i++) {
    if (vertices_[i].x < x_min) {
      x_min = vertices_[i].x;
    } else if (vertices_[i].x > x_max) {
      x_max = vertices_[i].x;
    }
    if (vertices_[i].y < y_min) {
      y_min = vertices_[i].y;
    } else if (vertices_[i].y > y_max) {
      y_max = vertices_[i].y;
    }
  }
  return {getCenter(), x_max - x_min, y_max - y_min};
}

void Polygon::move(const point_t &position)
{
  move(position.x - getCenter().x, position.y - getCenter().y);
}

void Polygon::move(const double &dx, const double &dy)
{
  for (int i = 0; i < num_of_sides_; i++) {
    vertices_[i].x += dx;
    vertices_[i].y += dy;
  }
}

void Polygon::printInformation() const
{
  std::cout << num_of_sides_ << "-gon information:\n";
  std::cout << "vertices:\n";
  for (int i = 0; i < num_of_sides_; i++) {
    std::cout << "vertex " << i << ": (" << vertices_[i].x << "; " << vertices_[i].y << ")\n";
  }

  std::cout << "center: (" << getCenter().x << "; " << getCenter().y << ")\n";
  std::cout << "area = " << getArea() << "\n";
  std::cout << num_of_sides_ << "-gon's frame rectangle information:" << "\n";
  rectangle_t frameRectangle = getFrameRect();
  std::cout << "center: (" << frameRectangle.pos.x << "; " << frameRectangle.pos.y << ")\n";
  std::cout << "width = " << frameRectangle.width << "\n";
  std::cout << "height = " << frameRectangle.height << "\n\n";
}
