#include "circle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>

Circle::Circle(const point_t &center, const double &radius):
  center_(center),
  radius_(radius)
{
  assert(radius_ > 0);
}

double Circle::getArea() const
{
  return (M_PI * radius_ * radius_);
}

rectangle_t Circle::getFrameRect() const
{
  return {center_, radius_ * 2, radius_ * 2};
}

void Circle::printInformation() const
{
  std::cout << "circle information:\n";
  std::cout << "center: (" << center_.x << "; " << center_.y << ")\n";
  std::cout << "radius = " << radius_ << "\n";
  std::cout << "area = " << getArea() << "\n";
  std::cout << "circle's frame rectangle information:" << "\n";
  rectangle_t frameRectangle = getFrameRect();
  std::cout << "center: (" << frameRectangle.pos.x << "; " << frameRectangle.pos.y << ")\n";
  std::cout << "width = " << frameRectangle.width << "\n";
  std::cout << "height = " << frameRectangle.height << "\n\n";
}

void Circle::move(const point_t &position)
{
  center_ = position;
}

void Circle::move(const double &dx, const double &dy)
{
  center_.x += dx;
  center_.y += dy;
}
