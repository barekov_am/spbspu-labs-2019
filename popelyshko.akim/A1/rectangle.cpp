#include "rectangle.hpp"
#include <iostream>
#include <cassert>

Rectangle::Rectangle(const point_t &center, const double &height, const double &width):
  center_(center),
  height_(height),
  width_(width)
{
  assert((width_ > 0) && (height_ > 0));
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return {center_, width_, height_};
}

void Rectangle::move(const double &dx, const double &dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Rectangle::move(const point_t &position)
{
  center_ = position;
}

void Rectangle::printInformation() const
{
  std::cout << "rectangle information:\n";
  std::cout << "center: (" << center_.x << "; " << center_.y << ")\n";
  std::cout << "width = " << width_ << "\n";
  std::cout << "height = " << height_ << "\n";
  std::cout << "area = " << getArea() << "\n";
  std::cout << "rectangle's frame rectangle information:" << "\n";
  rectangle_t frameRectangle = getFrameRect();
  std::cout << "center: (" << frameRectangle.pos.x << "; " << frameRectangle.pos.y << ")\n";
  std::cout << "width = " << frameRectangle.width << "\n";
  std::cout << "height = " << frameRectangle.height << "\n\n";
}
