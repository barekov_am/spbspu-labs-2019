#ifndef POLYGON_HPP 
#define POLYGON_HPP

#include "shape.hpp"

class Polygon: public Shape {
public:
  Polygon(const Polygon &other);
  Polygon(Polygon &&other) noexcept;
  Polygon(const int &, const point_t *vertex);
  ~Polygon() override;

  Polygon &operator=(const Polygon &);
  Polygon &operator=(Polygon &&) noexcept;

  point_t getCenter() const;
  bool isConvex() const;
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &) override;
  void move(const double &, const double &) override;
  void printInformation() const override;

private:
  int num_of_sides_;
  point_t *vertices_;
};

#endif //POLYGON_HPP
