#include <utility>
#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

void shapeTest(popelyshko::Shape *shape)
{
  shape->printInformation();
  shape->move(2.1, 3.3);
  shape->printInformation();
  shape->move({3.3, 3.2});
  shape->printInformation();
  shape->scale(2.5);
  shape->printInformation();
  shape->scale(0.4);
  shape->printInformation();
  shape->rotate(33.3);
  shape->printInformation();
  shape->rotate(-33.3);
  shape->printInformation();
}

int main()
{
  popelyshko::Rectangle rectangle({3.5, 4.5}, 2.5, 1.5);
  shapeTest(&rectangle);
  popelyshko::Circle circle({3.2, 4.2}, 2);
  shapeTest(&circle);
  popelyshko::Triangle triangle({1.5, 1.5}, {3.5, 1.5}, {1.5, 3.5});
  shapeTest(&triangle);
  popelyshko::point_t vertices[] = {{1, 1}, {4, 1}, {5, 3}, {4, 5}, {1, 5}, {0, 3}};
  popelyshko::Polygon polygon1(6, vertices);
  shapeTest(&polygon1);
  popelyshko::Polygon polygon2 = polygon1;
  polygon2.printInformation();
  polygon2.scale(2);
  polygon1 = polygon2;
  polygon1.printInformation();
  polygon2 = std::move(polygon1);
  popelyshko::Polygon polygon3 = std::move(polygon2);
  polygon3.printInformation();
  popelyshko::CompositeShape compositeShape1;
  popelyshko::CompositeShape compositeShape2(std::make_shared<popelyshko::Rectangle>(rectangle));
  compositeShape2.printInformation();
  popelyshko::CompositeShape compositeShape3(compositeShape2);
  compositeShape2.add(std::make_shared<popelyshko::Polygon>(polygon3));
  compositeShape3.printInformation();
  popelyshko::CompositeShape compositeShape4(std::move(compositeShape2));
  compositeShape4.printInformation();
  compositeShape1 = compositeShape4;
  compositeShape1.printInformation();
  compositeShape2 = std::move(compositeShape4);
  compositeShape2.printInformation();
  compositeShape1.remove(1);
  compositeShape1.printInformation();
  if (compositeShape1.getNumber() == 1) {
    shapeTest(&compositeShape2);
  }

  popelyshko::CompositeShape mainCompositeShape;
  mainCompositeShape.add(std::make_shared<popelyshko::Rectangle>(popelyshko::Rectangle({4, 2}, 2, 6)));
  popelyshko::point_t vertices1[] = {{-1, 4}, {4, 5}, {3, 9}, {-2, 10}, {-3, 6}};
  mainCompositeShape.add(std::make_shared<popelyshko::Polygon>(popelyshko::Polygon(5, vertices1)));
  mainCompositeShape.add(std::make_shared<popelyshko::Circle>(popelyshko::Circle({8, 4}, 3)));
  mainCompositeShape.add(std::make_shared<popelyshko::Rectangle>(popelyshko::Rectangle({17, 4}, 4, 2)));
  mainCompositeShape.add(std::make_shared<popelyshko::Triangle>(popelyshko::Triangle({14, 0}, {10, 1}, {13, 3})));
  popelyshko::CompositeShape
    compositeShape5(std::make_shared<popelyshko::Triangle>(popelyshko::Triangle({22, 5}, {20, 5}, {23, 8})));
  compositeShape5.add(std::make_shared<popelyshko::Circle>(popelyshko::Circle({18, 8}, 1)));
  mainCompositeShape.add(std::make_shared<popelyshko::CompositeShape>(compositeShape5));
  std::cout << "\n\nPartition:\n";
  popelyshko::Matrix<popelyshko::Shape> matrix = popelyshko::part(mainCompositeShape);
  size_t layer = matrix.getRows();
  for (size_t i = 0; i < layer; ++i) {
    std::cout << "Layer: " << i << "\n\n";
    size_t columns = matrix[i].getSize();
    for (size_t j = 0; j < columns; ++j) {
      if (matrix[i][j] != nullptr) {
        matrix[i][j]->printInformation();
      }
    }
  }
  return 0;
}
