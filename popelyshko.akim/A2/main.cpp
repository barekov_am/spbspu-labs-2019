#include <utility>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void shapeTest(popelyshko::Shape *shape)
{
  shape->printInformation();
  shape->move(2.1, 3.3);
  shape->printInformation();
  shape->move({3.3, 3.2});
  shape->printInformation();
  shape->scale(2.5);
  shape->printInformation();
  shape->scale(0.4);
  shape->printInformation();
}

int main()
{
  popelyshko::Rectangle rectangle({3.5, 4.5}, 2.5, 1.5);
  shapeTest(&rectangle);
  popelyshko::Circle circle({3.2, 4.2}, 2);
  shapeTest(&circle);
  popelyshko::Triangle triangle({1.5, 1.5}, {3.5, 1.5}, {1.5, 3.5});
  shapeTest(&triangle);
  popelyshko::point_t vertices[] = {{1, 1}, {4, 1}, {5, 3}, {4, 5}, {1, 5}, {0, 3}};
  popelyshko::Polygon polygon1(6, vertices);
  shapeTest(&polygon1);
  popelyshko::Polygon polygon2 = polygon1;
  polygon2.printInformation();
  polygon2.scale(2);
  polygon1 = polygon2;
  polygon1.printInformation();
  polygon2 = std::move(polygon1);
  popelyshko::Polygon polygon3 = std::move(polygon2);
  polygon3.printInformation();
  return 0;
}
