#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"

const double ERROR = 0.001;

BOOST_AUTO_TEST_SUITE(TestTriangle)

  BOOST_AUTO_TEST_CASE(TestTriangleMoveBy)
  {
    popelyshko::Triangle testTriangle({2.39, 3.92}, {9.23, 2.39}, {3.92, 9.23});
    const double areaBeforeMove = testTriangle.getArea();
    const popelyshko::rectangle_t frameBeforeMove = testTriangle.getFrameRect();
    testTriangle.move(1.4, 8.8);
    const double areaAfterMove = testTriangle.getArea();
    const popelyshko::rectangle_t frameAfterMove = testTriangle.getFrameRect();
    BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, ERROR);
    BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, ERROR);
    BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestTriangleMoveTo)
  {
    popelyshko::Triangle testTriangle({2.39, 3.92}, {9.23, 2.39}, {3.92, 9.23});
    const double areaBeforeMove = testTriangle.getArea();
    const popelyshko::rectangle_t frameBeforeMove = testTriangle.getFrameRect();
    testTriangle.move({1.4, 8.8});
    const double areaAfterMove = testTriangle.getArea();
    const popelyshko::rectangle_t frameAfterMove = testTriangle.getFrameRect();
    BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, ERROR);
    BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, ERROR);
    BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestTriangleScale)
  {
    popelyshko::Triangle testTriangle({0, 0}, {2, 0}, {0, 2});
    const double areaBeforeScale = testTriangle.getArea();
    double coefficient = 2;
    testTriangle.scale(coefficient);
    double areaAfterScale = testTriangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale * coefficient * coefficient, areaAfterScale, ERROR);
    coefficient = 0.5;
    testTriangle.scale(coefficient);
    areaAfterScale = testTriangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale, areaAfterScale, ERROR);
    coefficient = 1;
    testTriangle.scale(coefficient);
    areaAfterScale = testTriangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale, areaAfterScale, ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestTriangleInvalidVertices)
  {
    BOOST_CHECK_THROW(popelyshko::Triangle testTriangle({0.2, 3.9}, {0.2, 3.9}, {0.2, 3.9}), std::invalid_argument);
    BOOST_CHECK_THROW(popelyshko::Triangle testTriangle({0.2, 3.9}, {0.2, 3.9}, {2.3, 9.0}), std::invalid_argument);
    BOOST_CHECK_THROW(popelyshko::Triangle testTriangle({1.0, 2.0}, {2.0, 4.0}, {3.0, 6.0}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TestTriangleInvalidScaleCoefficient)
  {
    popelyshko::Triangle testTriangle({2.39, 3.92}, {9.23, 2.39}, {3.92, 9.23});
    BOOST_CHECK_THROW(testTriangle.scale(-20.19), std::invalid_argument);
    BOOST_CHECK_THROW(testTriangle.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TestTriangleRotate)
  {
    popelyshko::Triangle testTriangle({2.39, 3.92}, {9.23, 2.39}, {3.92, 9.23});
    const double areaBeforeRotation = testTriangle.getArea();
    testTriangle.rotate(22.2);
    double areaAfterRotation = testTriangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotation, areaAfterRotation, ERROR);
    testTriangle.rotate(-22.2);
    areaAfterRotation = testTriangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotation, areaAfterRotation, ERROR);
    testTriangle.rotate(0);
    areaAfterRotation = testTriangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotation, areaAfterRotation, ERROR);
  }

BOOST_AUTO_TEST_SUITE_END();
