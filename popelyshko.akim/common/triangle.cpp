#include "triangle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>

popelyshko::Triangle::Triangle(const point_t &vertexA, const point_t &vertexB, const point_t &vertexC):
  vertexA_(vertexA),
  vertexB_(vertexB),
  vertexC_(vertexC)
{
  if (getArea() <= 0) {
    throw std::invalid_argument("Vertices cannot lie on the same line");
  }
}

popelyshko::point_t popelyshko::Triangle::getCenter() const
{
  return {(vertexA_.x + vertexB_.x + vertexC_.x) / 3, (vertexA_.y + vertexB_.y + vertexC_.y) / 3};
}

void popelyshko::Triangle::move(const point_t &position)
{
  move(position.x - getCenter().x, position.y - getCenter().y);
}

void popelyshko::Triangle::move(const double &dx, const double &dy)
{
  vertexA_.x += dx;
  vertexA_.y += dy;
  vertexB_.x += dx;
  vertexB_.y += dy;
  vertexC_.x += dx;
  vertexC_.y += dy;
}

void popelyshko::Triangle::scale(const double coefficient)
{
  if (coefficient <= 0) {
    throw std::invalid_argument("Scale coefficient must be positive");
  }
  point_t center = getCenter();
  vertexA_.x = center.x + (vertexA_.x - center.x) * coefficient;
  vertexA_.y = center.y + (vertexA_.y - center.y) * coefficient;
  vertexB_.x = center.x + (vertexB_.x - center.x) * coefficient;
  vertexB_.y = center.y + (vertexB_.y - center.y) * coefficient;
  vertexC_.x = center.x + (vertexC_.x - center.x) * coefficient;
  vertexC_.y = center.y + (vertexC_.y - center.y) * coefficient;
}

void popelyshko::Triangle::rotate(double angle)
{
  point_t center = getCenter();
  const point_t shiftA = getShift(center, vertexA_, angle);
  const point_t shiftB = getShift(center, vertexB_, angle);
  const point_t shiftC = getShift(center, vertexC_, angle);
  vertexA_.x = center.x + shiftA.x;
  vertexA_.y = center.y + shiftA.y;
  vertexB_.x = center.x + shiftB.x;
  vertexB_.y = center.y + shiftB.y;
  vertexC_.x = center.x + shiftC.x;
  vertexC_.y = center.y + shiftC.y;
}

double popelyshko::Triangle::getArea() const
{
  return std::abs(0.5 * ((vertexB_.x - vertexA_.x) * (vertexC_.y - vertexA_.y)
                         - (vertexC_.x - vertexA_.x) * (vertexB_.y - vertexA_.y)));
}

popelyshko::rectangle_t popelyshko::Triangle::getFrameRect() const
{
  double x_min = std::min(std::min(vertexA_.x, vertexB_.x), vertexC_.x);
  double x_max = std::max(std::max(vertexA_.x, vertexB_.x), vertexC_.x);
  double y_min = std::min(std::min(vertexA_.y, vertexB_.y), vertexC_.y);
  double y_max = std::max(std::max(vertexA_.y, vertexB_.y), vertexC_.y);
  return {{0.5 * (x_max + x_min), 0.5 * (y_max + y_min)}, x_max - x_min, y_max - y_min};
}

void popelyshko::Triangle::printInformation() const
{
  std::cout << "triangle information:\n";
  std::cout << "center: (" << getCenter().x << ", " << getCenter().y << ")\n";
  std::cout << "vertices: (" << vertexA_.x << ", " << vertexA_.y << "); ("
            << vertexB_.x << ", " << vertexB_.y << "); ("
            << vertexC_.x << ", " << vertexC_.y << ")\n";
  std::cout << "area = " << getArea() << "\n";
  std::cout << "triangle's frame rectangle information:" << "\n";
  const rectangle_t frameRectangle = getFrameRect();
  std::cout << "center: (" << frameRectangle.pos.x << ", " << frameRectangle.pos.y << ")\n";
  std::cout << "width = " << frameRectangle.width << "\n";
  std::cout << "height = " << frameRectangle.height << "\n\n";
}
