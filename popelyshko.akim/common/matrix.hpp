#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>

namespace popelyshko {
  template<typename Type>
  class Matrix {
  public:
    using ptr = std::shared_ptr<Type>;

    class Array {
    public:
      Array(const Array &);
      Array(Array &&) noexcept;
      Array(ptr *, size_t);
      ~Array() = default;

      Array &operator=(const Array &);
      Array &operator=(Array &&) noexcept;
      ptr operator[](size_t) const;

      size_t getSize() const;
      void swap(Array &) noexcept;

    private:
      size_t size_;
      std::unique_ptr<ptr[]> array_;
    };

    Matrix();
    Matrix(const Matrix<Type> &);
    Matrix(Matrix<Type> &&) noexcept;
    ~Matrix() = default;

    Matrix<Type> &operator=(const Matrix<Type> &);
    Matrix<Type> &operator=(Matrix<Type> &&) noexcept;

    Array operator[](size_t) const;
    bool operator==(const Matrix<Type> &) const;
    bool operator!=(const Matrix<Type> &) const;

    size_t getRows() const;
    void add(ptr, size_t);
    void swap(Matrix<Type> &) noexcept;

  private:
    size_t rows_;
    size_t count_;
    std::unique_ptr<size_t[]> columns_;
    std::unique_ptr<ptr[]> data_;
  };

  template<typename Type>
  Matrix<Type>::Matrix() :
    rows_(0),
    count_(0)
  {
  }

  template<typename Type>
  Matrix<Type>::Matrix(const Matrix<Type> &other) :
    rows_(other.rows_),
    count_(other.count_),
    columns_(std::make_unique<size_t[]>(other.rows_)),
    data_(std::make_unique<ptr[]>(other.count_))
  {
    for (size_t i = 0; i < rows_; ++i) {
      columns_[i] = other.columns_[i];
    }

    for (size_t i = 0; i < count_; ++i) {
      data_[i] = other.data_[i];
    }
  }

  template<typename Type>
  Matrix<Type>::Matrix(Matrix<Type> &&other) noexcept :
    rows_(other.rows_),
    count_(other.count_),
    columns_(std::move(other.columns_)),
    data_(std::move(other.data_))
  {
    other.rows_ = 0;
    other.count_ = 0;
  }

  template<typename Type>
  Matrix<Type> &Matrix<Type>::operator=(const Matrix<Type> &other)
  {
    if (this != &other) {
      Matrix<Type>(other).swap(*this);
    }

    return *this;
  }

  template<typename Type>
  Matrix<Type> &Matrix<Type>::operator=(Matrix<Type> &&other) noexcept
  {
    if (this != &other) {
      rows_ = other.rows_;
      count_ = other.count_;
      columns_ = std::move(other.columns_);
      data_ = std::move(other.data_);

      other.rows_ = 0;
      other.count_ = 0;
    }

    return *this;
  }

  template<typename Type>
  typename Matrix<Type>::Array Matrix<Type>::operator[](size_t row) const
  {
    if (row >= rows_) {
      throw std::out_of_range("Index must be less than number of rows");
    }

    size_t firstIndex = 0;
    for (size_t i = 0; i < row; ++i) {
      firstIndex += columns_[i];
    }
    return Matrix<Type>::Array(&data_[firstIndex], columns_[row]);
  }

  template<typename Type>
  bool Matrix<Type>::operator==(const Matrix<Type> &other) const
  {
    if ((rows_ != other.rows_) || (count_ != other.count_)) {
      return false;
    }
    for (size_t i = 0; i < rows_; i++) {
      if (columns_[i] != other.columns_[i]) {
        return false;
      }
    }
    for (size_t i = 0; i < count_; ++i) {
      if (data_[i] != other.data_[i]) {
        return false;
      }
    }
    return true;
  }

  template<typename Type>
  bool Matrix<Type>::operator!=(const Matrix<Type> &other) const
  {
    return !(*this == other);
  }

  template<typename Type>
  size_t Matrix<Type>::getRows() const
  {
    return rows_;
  }

  template<typename Type>
  void Matrix<Type>::add(ptr type, size_t row)
  {
    if (row > rows_) {
      throw std::out_of_range("Index must be less than number of rows");
    }

    if (!type) {
      throw std::invalid_argument("Pointer must be not nullptr");
    }

    std::unique_ptr<ptr[]> newData = std::make_unique<ptr[]>(count_ + 1);
    size_t newIndex = 0;
    size_t oldIndex = 0;
    for (size_t i = 0; i < rows_; ++i) {
      for (size_t j = 0; j < columns_[i]; ++j) {
        newData[newIndex++] = data_[oldIndex++];
      }
      if (row == i) {
        newData[newIndex++] = type;
        ++columns_[row];
      }
    }

    if (row == rows_) {
      std::unique_ptr<size_t[]> newColumns = std::make_unique<size_t[]>(rows_ + 1);
      for (size_t i = 0; i < rows_; ++i) {
        newColumns[i] = columns_[i];
      }
      newColumns[row] = 1;
      newData[count_] = type;
      ++rows_;
      columns_.swap(newColumns);
    }

    data_.swap(newData);
    ++count_;
  }

  template<typename Type>
  void Matrix<Type>::swap(Matrix<Type> &other) noexcept
  {
    std::swap(rows_, other.rows_);
    std::swap(count_, other.count_);
    std::swap(columns_, other.columns_);
    std::swap(data_, other.data_);
  }

  template<typename Type>
  Matrix<Type>::Array::Array(const Matrix<Type>::Array &other):
    size_(other.size_),
    array_(std::make_unique<ptr[]>(other.size_))
  {
    for (size_t i = 0; i < size_; ++i) {
      array_[i] = other.array_[i];
    }
  }

  template<typename Type>
  Matrix<Type>::Array::Array(Matrix<Type>::Array &&other) noexcept :
    size_(other.size_),
    array_(std::move(other.array_))
  {
    other.size_ = 0;
  }

  template<typename Type>
  Matrix<Type>::Array::Array(ptr *array, size_t size):
    size_(size),
    array_(std::make_unique<ptr[]>(size))
  {
    for (size_t i = 0; i < size_; ++i) {
      array_[i] = array[i];
    }
  }

  template<typename Type>
  typename Matrix<Type>::Array &Matrix<Type>::Array::operator=(const Matrix<Type>::Array &other)
  {
    if (this != &other) {
      Matrix<Type>::Array(other).swap(*this);
    }

    return *this;
  }

  template<typename Type>
  typename Matrix<Type>::Array &Matrix<Type>::Array::operator=(Matrix<Type>::Array &&other) noexcept
  {
    if (this != &other) {
      size_ = other.size_;
      other.size_ = 0;
      array_ = std::move(other.array_);
    }
  }

  template<typename Type>
  typename Matrix<Type>::ptr Matrix<Type>::Array::operator[](size_t index) const
  {
    if (index >= size_) {
      throw std::out_of_range("Index must be less than size");
    }

    return array_[index];
  }

  template<typename Type>
  size_t Matrix<Type>::Array::getSize() const
  {
    return size_;
  }

  template<typename Type>
  void Matrix<Type>::Array::swap(Array &other) noexcept
  {
    std::swap(size_, other.size_);
    std::swap(array_, other.array_);
  }
}

#endif // MATRIX_HPP
