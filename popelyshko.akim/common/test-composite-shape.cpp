#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(testsForCompShape)

  const double ERROR = 0.001;

  BOOST_AUTO_TEST_CASE(testCompositeShapeMove)
  {
    popelyshko::Rectangle testingRectangle({2.3, 3.9}, 2.2, 4.8);
    popelyshko::Circle testingCircle({1.5, 2.4}, 1.2);
    popelyshko::CompositeShape testingCompShape(std::make_shared<popelyshko::Rectangle>(testingRectangle));
    testingCompShape.add(std::make_shared<popelyshko::Circle>(testingCircle));
    const popelyshko::rectangle_t testingFrameRect = testingCompShape.getFrameRect();
    const double testingArea = testingCompShape.getArea();
    const double width = testingFrameRect.width;
    const double height = testingFrameRect.height;
    testingCompShape.move(5.0, 5.5);
    BOOST_CHECK_CLOSE(testingCompShape.getFrameRect().width, width, ERROR);
    BOOST_CHECK_CLOSE(testingCompShape.getFrameRect().height, height, ERROR);
    BOOST_CHECK_CLOSE(testingCompShape.getArea(), testingArea, ERROR);
    testingCompShape.move({10.0, 12.0});
    BOOST_CHECK_CLOSE(testingCompShape.getFrameRect().width, width, ERROR);
    BOOST_CHECK_CLOSE(testingCompShape.getFrameRect().height, height, ERROR);
    BOOST_CHECK_CLOSE(testingCompShape.getArea(), testingArea, ERROR);
  }

  BOOST_AUTO_TEST_CASE(testCompositeShapeScale)
  {
    popelyshko::Rectangle testingRectangle({2.3, 3.9}, 2.2, 4.8);
    popelyshko::Circle testingCircle({1.5, 2.4}, 1.2);
    popelyshko::CompositeShape testingCompShape(std::make_shared<popelyshko::Rectangle>(testingRectangle));
    testingCompShape.add(std::make_shared<popelyshko::Circle>(testingCircle));
    const double area = testingCompShape.getArea();
    double scale = 5.0;
    testingCompShape.scale(scale);
    BOOST_CHECK_CLOSE(testingCompShape.getArea(), area * scale * scale, ERROR);
    scale = 0.2;
    testingCompShape.scale(scale);
    BOOST_CHECK_CLOSE(testingCompShape.getArea(), area, ERROR);
    scale = 1;
    testingCompShape.scale(scale);
    BOOST_CHECK_CLOSE(testingCompShape.getArea(), area, ERROR);
  }

  BOOST_AUTO_TEST_CASE(testCompositeShapeInvalidScaleCoefficient)
  {
    popelyshko::Rectangle testingRectangle({2.3, 3.9}, 2.2, 4.8);
    popelyshko::Circle testingCircle({1.5, 2.4}, 1.2);
    popelyshko::CompositeShape testingCompShape(std::make_shared<popelyshko::Rectangle>(testingRectangle));
    testingCompShape.add(std::make_shared<popelyshko::Circle>(testingCircle));
    BOOST_CHECK_THROW(testingCompShape.scale(-2.4), std::invalid_argument);
    BOOST_CHECK_THROW(testingCompShape.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(testCompositeShapeInvalidRemoveIndex)
  {
    popelyshko::Rectangle testingRectangle({2.3, 3.9}, 2.2, 4.8);
    popelyshko::Circle testingCircle({1.5, 2.4}, 1.2);
    popelyshko::CompositeShape testingCompShape(std::make_shared<popelyshko::Rectangle>(testingRectangle));
    testingCompShape.add(std::make_shared<popelyshko::Circle>(testingCircle));
    BOOST_CHECK_THROW(testingCompShape.remove(5), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(testCompositeShapeInvalidIndexInArraySubscriptingOperator)
  {
    popelyshko::Rectangle testingRectangle({2.3, 3.9}, 2.2, 4.8);
    popelyshko::Circle testingCircle({1.5, 2.4}, 1.2);
    popelyshko::CompositeShape testingCompShape(std::make_shared<popelyshko::Rectangle>(testingRectangle));
    testingCompShape.add(std::make_shared<popelyshko::Circle>(testingCircle));
    BOOST_CHECK_THROW(testingCompShape[3], std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(testCompositeShapeInvalidArgumentInGetFrameRectangle)
  {
    popelyshko::CompositeShape testingCompShape;
    BOOST_CHECK_THROW(testingCompShape.getFrameRect(), std::logic_error);
  }

  BOOST_AUTO_TEST_CASE(testCompositeShapeInvalidArgumentInGetArea)
  {
    popelyshko::CompositeShape testingCompShape;
    BOOST_CHECK_THROW(testingCompShape.getArea(), std::logic_error);
  }

  BOOST_AUTO_TEST_CASE(testCompositeShapeCopyConstructor)
  {
    popelyshko::Rectangle testingRectangle({2.3, 3.9}, 2.2, 4.8);
    popelyshko::Circle testingCircle({1.5, 2.4}, 1.2);
    popelyshko::CompositeShape testingCompShape(std::make_shared<popelyshko::Rectangle>(testingRectangle));
    testingCompShape.add(std::make_shared<popelyshko::Circle>(testingCircle));
    const popelyshko::rectangle_t frameRect = testingCompShape.getFrameRect();
    popelyshko::CompositeShape copyCompShape(testingCompShape);
    const popelyshko::rectangle_t copyFrameRect = copyCompShape.getFrameRect();
    BOOST_CHECK_CLOSE(testingCompShape.getArea(), copyCompShape.getArea(), ERROR);
    BOOST_CHECK_EQUAL(testingCompShape.getNumber(), copyCompShape.getNumber());
    BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, ERROR);
    BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, ERROR);
    BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, ERROR);
    BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, ERROR);
  }

  BOOST_AUTO_TEST_CASE(testCompositeShapeMoveConstructor)
  {
    popelyshko::Rectangle testingRectangle({2.3, 3.9}, 2.2, 4.8);
    popelyshko::Circle testingCircle({1.5, 2.4}, 1.2);
    popelyshko::CompositeShape testingCompShape(std::make_shared<popelyshko::Rectangle>(testingRectangle));
    testingCompShape.add(std::make_shared<popelyshko::Circle>(testingCircle));
    const popelyshko::rectangle_t frameRect = testingCompShape.getFrameRect();
    const double compositeArea = testingCompShape.getArea();
    const size_t compositeSize = testingCompShape.getNumber();
    popelyshko::CompositeShape moveCompShape(std::move(testingCompShape));
    const popelyshko::rectangle_t moveFrameRect = moveCompShape.getFrameRect();
    BOOST_CHECK_CLOSE(compositeArea, moveCompShape.getArea(), ERROR);
    BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, ERROR);
    BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, ERROR);
    BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, ERROR);
    BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, ERROR);
    BOOST_CHECK_EQUAL(compositeSize, moveCompShape.getNumber());
    BOOST_CHECK_EQUAL(testingCompShape.getNumber(), 0);
  }

  BOOST_AUTO_TEST_CASE(testCompositeShapeCopyOperator)
  {
    popelyshko::Rectangle testingRectangle({2.3, 3.9}, 2.2, 4.8);
    popelyshko::Circle testingCircle({1.5, 2.4}, 1.2);
    popelyshko::CompositeShape testingCompShape(std::make_shared<popelyshko::Rectangle>(testingRectangle));
    testingCompShape.add(std::make_shared<popelyshko::Circle>(testingCircle));
    const popelyshko::rectangle_t frameRect = testingCompShape.getFrameRect();
    popelyshko::Triangle testingTriangle({1.0, 1.0}, {2.0, 2.0}, {3.0, 1.0});
    popelyshko::CompositeShape copyCompShape(std::make_shared<popelyshko::Triangle>(testingTriangle));
    copyCompShape = testingCompShape;
    const popelyshko::rectangle_t copyFrameRect = copyCompShape.getFrameRect();
    BOOST_CHECK_CLOSE(testingCompShape.getArea(), copyCompShape.getArea(), ERROR);
    BOOST_CHECK_EQUAL(testingCompShape.getNumber(), copyCompShape.getNumber());
    BOOST_CHECK_CLOSE(frameRect.height, copyFrameRect.height, ERROR);
    BOOST_CHECK_CLOSE(frameRect.width, copyFrameRect.width, ERROR);
    BOOST_CHECK_CLOSE(frameRect.pos.x, copyFrameRect.pos.x, ERROR);
    BOOST_CHECK_CLOSE(frameRect.pos.y, copyFrameRect.pos.y, ERROR);
  }

  BOOST_AUTO_TEST_CASE(testCompositeShapeMoveOperator)
  {
    popelyshko::Rectangle testingRectangle({2.3, 3.9}, 2.2, 4.8);
    popelyshko::Circle testingCircle({1.5, 2.4}, 1.2);
    popelyshko::CompositeShape testingCompShape(std::make_shared<popelyshko::Rectangle>(testingRectangle));
    testingCompShape.add(std::make_shared<popelyshko::Circle>(testingCircle));
    const popelyshko::rectangle_t frameRect = testingCompShape.getFrameRect();
    const double compositeArea = testingCompShape.getArea();
    const size_t compositeSize = testingCompShape.getNumber();
    popelyshko::Triangle testingTriangle({1.0, 1.0}, {2.0, 2.0}, {3.0, 1.0});
    popelyshko::CompositeShape moveCompShape(std::make_shared<popelyshko::Triangle>(testingTriangle));
    moveCompShape = std::move(testingCompShape);
    const popelyshko::rectangle_t moveFrameRect = moveCompShape.getFrameRect();
    BOOST_CHECK_CLOSE(compositeArea, moveCompShape.getArea(), ERROR);
    BOOST_CHECK_CLOSE(frameRect.height, moveFrameRect.height, ERROR);
    BOOST_CHECK_CLOSE(frameRect.width, moveFrameRect.width, ERROR);
    BOOST_CHECK_CLOSE(frameRect.pos.x, moveFrameRect.pos.x, ERROR);
    BOOST_CHECK_CLOSE(frameRect.pos.y, moveFrameRect.pos.y, ERROR);
    BOOST_CHECK_EQUAL(compositeSize, moveCompShape.getNumber());
    BOOST_CHECK_EQUAL(testingCompShape.getNumber(), 0);
  }

  BOOST_AUTO_TEST_CASE(TestCompositeShapeRotate)
  {
    popelyshko::Rectangle testingRectangle({2.3, 3.9}, 2.2, 4.8);
    popelyshko::Circle testingCircle({1.5, 2.4}, 1.2);
    popelyshko::CompositeShape testingCompShape(std::make_shared<popelyshko::Rectangle>(testingRectangle));
    const double areaBeforeRotation = testingCompShape.getArea();
    testingCompShape.rotate(90);
    double areaAfterRotation = testingCompShape.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotation, areaAfterRotation, ERROR);
    testingCompShape.rotate(-90);
    areaAfterRotation = testingCompShape.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotation, areaAfterRotation, ERROR);
    testingCompShape.rotate(0);
    areaAfterRotation = testingCompShape.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotation, areaAfterRotation, ERROR);
  }

BOOST_AUTO_TEST_SUITE_END();
