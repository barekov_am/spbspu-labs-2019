#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

popelyshko::Rectangle::Rectangle(const point_t &center, const double &height, const double &width):
  center_(center),
  height_(height),
  width_(width),
  angle_(0)
{
  if (width_ <= 0) {
    throw std::invalid_argument("Width must be positive");
  }
  if (height_ <= 0) {
    throw std::invalid_argument("Height must be positive");
  }
}

popelyshko::Rectangle::Rectangle(const point_t &center, const double &height, const double &width, const double &angle):
  center_(center),
  height_(height),
  width_(width),
  angle_(angle)
{
  if (width_ <= 0) {
    throw std::invalid_argument("Width must be positive");
  }
  if (height_ <= 0) {
    throw std::invalid_argument("Height must be positive");
  }
}

void popelyshko::Rectangle::move(const double &dx, const double &dy)
{
  center_.x += dx;
  center_.y += dy;
}

void popelyshko::Rectangle::move(const point_t &position)
{
  center_ = position;
}

void popelyshko::Rectangle::scale(const double coefficient)
{
  if (coefficient <= 0) {
    throw std::invalid_argument("Scale coefficient must be positive");
  }
  width_ *= coefficient;
  height_ *= coefficient;
}

void popelyshko::Rectangle::rotate(const double angle)
{
  angle_ += angle;
}

double popelyshko::Rectangle::getArea() const
{
  return width_ * height_;
}

popelyshko::rectangle_t popelyshko::Rectangle::getFrameRect() const
{
  const double cos = std::abs(std::cos(angle_ * M_PI / 180));
  const double sin = std::abs(std::sin(angle_ * M_PI / 180));
  const double width = fabs(cos * width_) + fabs(sin * height_);
  const double height = fabs(cos * height_) + fabs(sin * width_);
  return {center_, width, height};
}

void popelyshko::Rectangle::printInformation() const
{
  std::cout << "rectangle information:\n";
  std::cout << "center: (" << center_.x << ", " << center_.y << ")\n";
  std::cout << "width = " << width_ << "\n";
  std::cout << "height = " << height_ << "\n";
  std::cout << "angle = " << angle_ << "\n";
  std::cout << "area = " << getArea() << "\n";
  std::cout << "rectangle's frame rectangle information:" << "\n";
  const rectangle_t frameRectangle = getFrameRect();
  std::cout << "center: (" << frameRectangle.pos.x << ", " << frameRectangle.pos.y << ")\n";
  std::cout << "width = " << frameRectangle.width << "\n";
  std::cout << "height = " << frameRectangle.height << "\n\n";
}
