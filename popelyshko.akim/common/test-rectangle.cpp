#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"

const double ERROR = 0.001;

BOOST_AUTO_TEST_SUITE(TestRectangle)

  BOOST_AUTO_TEST_CASE(TestRectangleMoveBy)
  {
    popelyshko::Rectangle testRectangle({2.39, 3.92}, 9.23, 2.39);
    const double areaBeforeMove = testRectangle.getArea();
    const popelyshko::rectangle_t frameBeforeMove = testRectangle.getFrameRect();
    testRectangle.move(1.4, 8.8);
    const double areaAfterMove = testRectangle.getArea();
    const popelyshko::rectangle_t frameAfterMove = testRectangle.getFrameRect();
    BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, ERROR);
    BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, ERROR);
    BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestRectangleMoveTo)
  {
    popelyshko::Rectangle testRectangle({2.39, 3.92}, 9.23, 2.39);
    const double areaBeforeMove = testRectangle.getArea();
    const popelyshko::rectangle_t frameBeforeMove = testRectangle.getFrameRect();
    testRectangle.move({1.4, 8.8});
    const double areaAfterMove = testRectangle.getArea();
    const popelyshko::rectangle_t frameAfterMove = testRectangle.getFrameRect();
    BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, ERROR);
    BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, ERROR);
    BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestRectangleScale)
  {
    popelyshko::Rectangle testRectangle({2.39, 3.92}, 9.23, 2.39);
    const double areaBeforeScale = testRectangle.getArea();
    double coefficient = 2;
    testRectangle.scale(coefficient);
    double areaAfterScale = testRectangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale * coefficient * coefficient, areaAfterScale, ERROR);
    coefficient = 0.5;
    testRectangle.scale(coefficient);
    areaAfterScale = testRectangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale, areaAfterScale, ERROR);
    coefficient = 1;
    testRectangle.scale(coefficient);
    areaAfterScale = testRectangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale, areaAfterScale, ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestRectangleInvalidHeight)
  {
    BOOST_CHECK_THROW(popelyshko::Rectangle testRectangle({2.39, 3.92}, -9.23, 2.39), std::invalid_argument);
    BOOST_CHECK_THROW(popelyshko::Rectangle testRectangle({2.39, 3.92}, 0, 2.39), std::invalid_argument);
    BOOST_CHECK_THROW(popelyshko::Rectangle testRectangle({2.39, 3.92}, 0, 0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TestRectangleInvalidWidth)
  {
    BOOST_CHECK_THROW(popelyshko::Rectangle testRectangle({2.39, 3.92}, 9.23, -2.39), std::invalid_argument);
    BOOST_CHECK_THROW(popelyshko::Rectangle testRectangle({2.39, 3.92}, 9.23, 0), std::invalid_argument);
    BOOST_CHECK_THROW(popelyshko::Rectangle testRectangle({2.39, 3.92}, 9.23, 0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TestRectangleInvalidScaleCoefficient)
  {
    popelyshko::Rectangle testRectangle({2.39, 3.92}, 9.23, 2.39);
    BOOST_CHECK_THROW(testRectangle.scale(-20.19), std::invalid_argument);
    BOOST_CHECK_THROW(testRectangle.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TestRectangleRotate)
  {
    popelyshko::Rectangle testRectangle({2.39, 3.92}, 9.23, 2.39);
    const double areaBeforeRotation = testRectangle.getArea();
    testRectangle.rotate(22.2);
    double areaAfterRotation = testRectangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotation, areaAfterRotation, ERROR);
    testRectangle.rotate(-22.2);
    areaAfterRotation = testRectangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotation, areaAfterRotation, ERROR);
    testRectangle.rotate(0);
    areaAfterRotation = testRectangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotation, areaAfterRotation, ERROR);
  }

BOOST_AUTO_TEST_SUITE_END();
