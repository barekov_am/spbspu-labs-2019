#include "base-types.hpp"
#include <cmath>

popelyshko::point_t popelyshko::getShift(popelyshko::point_t &center, popelyshko::point_t &vertex, double &angle)
{
  const double cos = std::abs(std::cos(angle * M_PI / 180));
  const double sin = std::abs(std::sin(angle * M_PI / 180));
  return {(vertex.x - center.x) * cos - (vertex.y - center.y) * sin,
          (vertex.x - center.x) * sin + (vertex.y - center.y) * cos};
}
