#include <memory>

#include <boost/test/auto_unit_test.hpp>

#include "matrix.hpp"
#include "partition.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testPartitionMethods)

  BOOST_AUTO_TEST_CASE(testPartition)
  {
    const popelyshko::Circle testCircle1({0.0, 0.0}, 3.0);
    const popelyshko::Circle testCircle2({5.0, 5.0}, 1.0);
    const popelyshko::Rectangle testRectangle1({-2.0, 1.0}, 4.0, 4.0);
    const popelyshko::Rectangle testRectangle2({-5.0, 4.0}, 3.0, 6.0);
    std::shared_ptr<popelyshko::Shape> circlePointer = std::make_shared<popelyshko::Circle>(testCircle1);
    std::shared_ptr<popelyshko::Shape> circlePointer2 = std::make_shared<popelyshko::Circle>(testCircle2);
    std::shared_ptr<popelyshko::Shape> rectPointer1 = std::make_shared<popelyshko::Rectangle>(testRectangle1);
    std::shared_ptr<popelyshko::Shape> rectPointer2 = std::make_shared<popelyshko::Rectangle>(testRectangle2);
    popelyshko::CompositeShape compositeShape(circlePointer);
    compositeShape.add(rectPointer1);
    compositeShape.add(circlePointer2);
    compositeShape.add(rectPointer2);
    popelyshko::Matrix<popelyshko::Shape> testMatrix = popelyshko::part(compositeShape);
    BOOST_CHECK(testMatrix[0][0] == circlePointer);
    BOOST_CHECK(testMatrix[0][1] == circlePointer2);
    BOOST_CHECK(testMatrix[1][0] == rectPointer1);
    BOOST_CHECK(testMatrix[2][0] == rectPointer2);
    BOOST_CHECK_EQUAL(testMatrix.getRows(), 3);
    BOOST_CHECK_EQUAL(testMatrix[0].getSize(), 2);
    BOOST_CHECK_EQUAL(testMatrix[1].getSize(), 1);
    BOOST_CHECK_EQUAL(testMatrix[2].getSize(), 1);
    const popelyshko::Rectangle rect3({6.0, 6.0}, 2.0, 2.0);
    std::shared_ptr<popelyshko::Shape> rectPointer3 = std::make_shared<popelyshko::Rectangle>(rect3);
    compositeShape.add(rectPointer3);
    testMatrix = popelyshko::part(compositeShape);
    BOOST_CHECK(testMatrix[1][1] == rectPointer3);
    BOOST_CHECK_EQUAL(testMatrix.getRows(), 3);
    BOOST_CHECK_EQUAL(testMatrix[0].getSize(), 2);
    BOOST_CHECK_EQUAL(testMatrix[1].getSize(), 2);
    BOOST_CHECK_EQUAL(testMatrix[2].getSize(), 1);
  }

  BOOST_AUTO_TEST_CASE(testIntersection)
  {
    const popelyshko::Rectangle rect({0.0, 0.0}, 6.0, 5.0);

    const popelyshko::Rectangle rect1({3.0, 3.0}, 4.0, 4.0);
    const popelyshko::Rectangle rect2({-3.0, 2.0}, 4.0, 2.0);
    const popelyshko::Rectangle rect3({-3.0, -2.0}, 2.0, 2.0);
    const popelyshko::Rectangle rect4({3.0, -2.0}, 2.0, 2.0);
    const popelyshko::Circle circle({30.0, -10.0}, 4.0);
    BOOST_CHECK(popelyshko::intersect(rect.getFrameRect(), rect1.getFrameRect()));
    BOOST_CHECK(popelyshko::intersect(rect.getFrameRect(), rect2.getFrameRect()));
    BOOST_CHECK(popelyshko::intersect(rect.getFrameRect(), rect3.getFrameRect()));
    BOOST_CHECK(popelyshko::intersect(rect.getFrameRect(), rect4.getFrameRect()));
    BOOST_CHECK(!popelyshko::intersect(rect.getFrameRect(), circle.getFrameRect()));
  }

BOOST_AUTO_TEST_SUITE_END()
