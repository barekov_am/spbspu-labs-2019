#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace popelyshko {
  class Circle: public Shape {
  public:
    Circle(const point_t &, const double &);

    void move(const point_t &) override;
    void move(const double &, const double &) override;
    void scale(const double) override;
    void rotate(const double) override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printInformation() const override;

  private:
    point_t center_;
    double radius_;
  };
}
#endif //CIRCLE_HPP
