#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace popelyshko {
  Matrix<Shape> part(const CompositeShape &);
  bool intersect(const rectangle_t &, const rectangle_t &);
}
#endif //PARTITION_HPP
