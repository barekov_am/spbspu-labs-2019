#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace popelyshko {
  class Rectangle: public Shape {
  public:
    Rectangle(const point_t &, const double &, const double &);
    Rectangle(const point_t &, const double &, const double &, const double &);

    void move(const point_t &) override;
    void move(const double &, const double &) override;
    void scale(const double) override;
    void rotate(const double) override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printInformation() const override;

  private:
    point_t center_;
    double height_;
    double width_;
    double angle_;
  };
}
#endif //RECTANGLE_HPP
