#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

popelyshko::CompositeShape::CompositeShape():
  num_of_shapes_(0),
  shapes_(nullptr)
{
}

popelyshko::CompositeShape::CompositeShape(const popelyshko::CompositeShape &other):
  num_of_shapes_(other.num_of_shapes_),
  shapes_(std::make_unique<std::shared_ptr<popelyshko::Shape>[]>(other.num_of_shapes_))
{
  for (size_t i = 0; i < num_of_shapes_; ++i) {
    shapes_[i] = other.shapes_[i];
  }
}

popelyshko::CompositeShape::CompositeShape(popelyshko::CompositeShape &&other) noexcept:
  num_of_shapes_(other.num_of_shapes_),
  shapes_(std::move(other.shapes_))
{
  other.num_of_shapes_ = 0;
}

popelyshko::CompositeShape::CompositeShape(const std::shared_ptr<popelyshko::Shape> &other):
  CompositeShape()
{
  add(other);
}

popelyshko::CompositeShape &popelyshko::CompositeShape::operator=(const popelyshko::CompositeShape &other)
{
  if (&other == this) {
    return *this;
  }
  num_of_shapes_ = other.num_of_shapes_;
  shapes_ = std::make_unique<std::shared_ptr<popelyshko::Shape>[]>(num_of_shapes_);
  for (size_t i = 0; i < num_of_shapes_; ++i) {
    shapes_[i] = other.shapes_[i];
  }
  return *this;
}

popelyshko::CompositeShape &popelyshko::CompositeShape::operator=(popelyshko::CompositeShape &&other) noexcept
{
  if (&other == this) {
    return *this;
  }
  num_of_shapes_ = other.num_of_shapes_;
  other.num_of_shapes_ = 0;
  shapes_ = std::move(other.shapes_);

  return *this;
}

std::shared_ptr<popelyshko::Shape> &popelyshko::CompositeShape::operator[](const size_t &index) const
{
  if (index >= num_of_shapes_) {
    throw std::invalid_argument("Index must be less than number of shapes");
  }
  return shapes_[index];
}

size_t popelyshko::CompositeShape::getNumber() const
{
  return num_of_shapes_;
}

void popelyshko::CompositeShape::add(const std::shared_ptr<popelyshko::Shape> &newShape)
{
  num_of_shapes_++;
  std::unique_ptr<std::shared_ptr<popelyshko::Shape>[]>
    tmpShapes(std::make_unique<std::shared_ptr<popelyshko::Shape>[]>(num_of_shapes_));
  for (size_t i = 0; i < num_of_shapes_ - 1; i++) {
    tmpShapes[i] = shapes_[i];
  }
  shapes_ = std::move(tmpShapes);
  shapes_[num_of_shapes_ - 1] = newShape;
}

void popelyshko::CompositeShape::remove(const size_t &index)
{
  if (index >= num_of_shapes_) {
    throw std::invalid_argument("Index must be less than number of shapes");
  }
  num_of_shapes_--;
  std::unique_ptr<std::shared_ptr<popelyshko::Shape>[]>
    tmpShapes(std::make_unique<std::shared_ptr<popelyshko::Shape>[]>(num_of_shapes_));
  for (size_t i = 0; i < index; i++) {
    tmpShapes[i] = shapes_[i];
  }
  for (size_t i = index + 1; i < num_of_shapes_ + 1; i++) {
    tmpShapes[i - 1] = shapes_[i];
  }
  shapes_ = std::move(tmpShapes);
}

double popelyshko::CompositeShape::getArea() const
{
  if (num_of_shapes_ == 0) {
    throw std::logic_error("CompositeShape is empty");
  }
  double area = 0;
  for (size_t i = 0; i < num_of_shapes_; i++) {
    area += shapes_[i]->getArea();
  }
  return area;
}

popelyshko::rectangle_t popelyshko::CompositeShape::getFrameRect() const
{
  if (num_of_shapes_ == 0) {
    throw std::logic_error("CompositeShape is empty");
  }
  popelyshko::rectangle_t frameRectangle = shapes_[0]->getFrameRect();
  double x_min = frameRectangle.pos.x - frameRectangle.width / 2;
  double x_max = frameRectangle.pos.x + frameRectangle.width / 2;
  double y_min = frameRectangle.pos.y - frameRectangle.height / 2;
  double y_max = frameRectangle.pos.y + frameRectangle.height / 2;

  for (size_t i = 1; i < num_of_shapes_; ++i) {
    frameRectangle = shapes_[i]->getFrameRect();
    x_min = std::min(frameRectangle.pos.x - frameRectangle.width / 2, x_min);
    x_max = std::max(frameRectangle.pos.x + frameRectangle.width / 2, x_max);
    y_min = std::min(frameRectangle.pos.y - frameRectangle.height / 2, y_min);
    y_max = std::max(frameRectangle.pos.y - frameRectangle.height / 2, y_max);
  }
  return {{(x_max + x_min) / 2, (y_max + y_min) / 2}, (x_max - x_min), (y_max - y_min)};
}

void popelyshko::CompositeShape::move(const double &dx, const double &dy)
{
  for (size_t i = 0; i < num_of_shapes_; i++) {
    shapes_[i]->move(dx, dy);
  }
}

void popelyshko::CompositeShape::move(const popelyshko::point_t &position)
{
  popelyshko::rectangle_t frameRectangle = getFrameRect();
  move(position.x - frameRectangle.pos.x, position.y - frameRectangle.pos.y);
}

void popelyshko::CompositeShape::scale(const double coefficient)
{
  if (coefficient <= 0) {
    throw std::invalid_argument("Scale coefficient must be positive");
  }
  popelyshko::point_t center = getFrameRect().pos;
  double distanceToCenterX = 0;
  double distanceToCenterY = 0;
  for (size_t i = 0; i < num_of_shapes_; ++i) {
    distanceToCenterX = shapes_[i]->getFrameRect().pos.x - center.x;
    distanceToCenterY = shapes_[i]->getFrameRect().pos.y - center.y;
    shapes_[i]->move({distanceToCenterX * coefficient + center.x, distanceToCenterY * coefficient + center.y});
    shapes_[i]->scale(coefficient);
  }
}

void popelyshko::CompositeShape::rotate(const double angle)
{
  const point_t oldCenter = getFrameRect().pos;
  const double cos = std::abs(std::cos(angle * M_PI / 180));
  const double sin = std::abs(std::sin(angle * M_PI / 180));
  for (size_t i = 0; i < num_of_shapes_; i++) {
    const point_t newCenter = shapes_[i]->getFrameRect().pos;
    shapes_[i]->move((newCenter.x - oldCenter.x) * (cos - 1) - (newCenter.y - oldCenter.y) * sin,
                     (newCenter.x - oldCenter.x) * sin + (newCenter.y - oldCenter.y) * (cos - 1));
    shapes_[i]->rotate(angle);
  }
}

void popelyshko::CompositeShape::printInformation() const
{
  std::cout << "CompositeShape information:\n";
  std::cout << "number of shapes = " << num_of_shapes_ << "\n";
  std::cout << "CompositeShape's frame rectangle information:" << "\n";
  const rectangle_t frameRectangle = getFrameRect();
  std::cout << "center: (" << frameRectangle.pos.x << ", " << frameRectangle.pos.y << ")\n";
  std::cout << "width = " << frameRectangle.width << "\n";
  std::cout << "height = " << frameRectangle.height << "\n";
  std::cout << "area = " << getArea() << "\n";
  std::cout << "shapes:\n";
  for (size_t i = 0; i < num_of_shapes_; i++) {
    std::cout << i << ". ";
    shapes_[i]->printInformation();
  }
  std::cout << "\n";
}
