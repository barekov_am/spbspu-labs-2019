#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include "shape.hpp"
#include <memory>

namespace popelyshko {
  class CompositeShape: public Shape {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&) noexcept;
    explicit CompositeShape(const std::shared_ptr<popelyshko::Shape> &);
    ~CompositeShape() override = default;

    CompositeShape &operator=(const CompositeShape &);
    CompositeShape &operator=(CompositeShape &&) noexcept;
    std::shared_ptr<popelyshko::Shape> &operator[](const size_t &) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printInformation() const override;
    void move(const point_t &) override;
    void move(const double &, const double &) override;
    void scale(const double) override;
    void rotate(const double) override;
    size_t getNumber() const;
    void add(const std::shared_ptr<popelyshko::Shape> &);
    void remove(const size_t &);

  private:
    size_t num_of_shapes_;
    std::unique_ptr<std::shared_ptr<Shape>[]> shapes_;
  };
}

#endif
