#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "polygon.hpp"

const double ERROR = 0.001;

BOOST_AUTO_TEST_SUITE(TestPolygon)

  BOOST_AUTO_TEST_CASE(TestPolygonMoveBy)
  {
    const popelyshko::point_t vertices[] = {{2, 3}, {3, 2}, {9, 2}, {9, 3}, {9, 9}, {3, 9}};
    popelyshko::Polygon testPolygon(6, vertices);
    const double areaBeforeMove = testPolygon.getArea();
    const popelyshko::rectangle_t frameBeforeMove = testPolygon.getFrameRect();
    testPolygon.move(1.4, 8.8);
    const double areaAfterMove = testPolygon.getArea();
    const popelyshko::rectangle_t frameAfterMove = testPolygon.getFrameRect();
    BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, ERROR);
    BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, ERROR);
    BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestPolygonMoveTo)
  {
    const popelyshko::point_t vertices[] = {{2, 3}, {3, 2}, {9, 2}, {9, 3}, {9, 9}, {3, 9}};
    popelyshko::Polygon testPolygon(6, vertices);
    const double areaBeforeMove = testPolygon.getArea();
    const popelyshko::rectangle_t frameBeforeMove = testPolygon.getFrameRect();
    testPolygon.move({1.4, 8.8});
    const double areaAfterMove = testPolygon.getArea();
    const popelyshko::rectangle_t frameAfterMove = testPolygon.getFrameRect();
    BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, ERROR);
    BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, ERROR);
    BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestPolygonScale)
  {
    const popelyshko::point_t vertices[] = {{2, 3}, {3, 2}, {9, 2}, {9, 3}, {9, 9}, {3, 9}};
    popelyshko::Polygon testPolygon(6, vertices);
    const double areaBeforeScale = testPolygon.getArea();
    double coefficient = 2;
    testPolygon.scale(coefficient);
    double areaAfterScale = testPolygon.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale * coefficient * coefficient, areaAfterScale, ERROR);
    coefficient = 0.5;
    testPolygon.scale(coefficient);
    areaAfterScale = testPolygon.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale, areaAfterScale, ERROR);
    coefficient = 1;
    testPolygon.scale(coefficient);
    areaAfterScale = testPolygon.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale, areaAfterScale, ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestPolygonInvalidScaleCoefficient)
  {
    const popelyshko::point_t vertices[] = {{2, 3}, {3, 2}, {9, 2}, {9, 3}, {9, 9}, {3, 9}};
    popelyshko::Polygon testPolygon(6, vertices);
    BOOST_CHECK_THROW(testPolygon.scale(-20.19), std::invalid_argument);
    BOOST_CHECK_THROW(testPolygon.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TestPolygonInvalidNumOfSides)
  {
    const popelyshko::point_t vertices[] = {{2, 3}, {3, 2}, {9, 2}, {9, 3}, {9, 9}, {3, 9}};
    BOOST_CHECK_THROW(popelyshko::Polygon testPolygon(2, vertices), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TestPolygonInvalidInputPointer)
  {
    BOOST_CHECK_THROW(popelyshko::Polygon testPolygon(3, nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TestPolygonIsNotConvex)
  {
    popelyshko::point_t vertices[] = {{2, 2}, {9, 2}, {3, 3}, {2, 9}};
    BOOST_CHECK_THROW(popelyshko::Polygon testPolygon(4, vertices), std::invalid_argument);
    vertices[3] = {9, 2};
    BOOST_CHECK_THROW(popelyshko::Polygon testPolygon(4, vertices), std::invalid_argument);
    vertices[2] = {7, 2};
    BOOST_CHECK_THROW(popelyshko::Polygon testPolygon(4, vertices), std::invalid_argument);
    vertices[2] = {9, 2};
    vertices[0] = {9, 2};
    BOOST_CHECK_THROW(popelyshko::Polygon testPolygon(4, vertices), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TestPolygonCopyConstructor)
  {
    const popelyshko::point_t vertices[] = {{3, 3}, {9, 2}, {9, 9}, {2, 9}};
    popelyshko::Polygon testPolygon1(4, vertices);
    popelyshko::Polygon testPolygon2 = testPolygon1;
    BOOST_CHECK_CLOSE(testPolygon1.getArea(), testPolygon2.getArea(), ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestPolygonCopyAssignmentOperator)
  {
    const popelyshko::point_t vertices1[] = {{3, 3}, {9, 2}, {9, 9}, {2, 9}};
    const popelyshko::point_t vertices2[] = {{0, 0}, {1, 1}, {2, 0}};
    popelyshko::Polygon testPolygon1(4, vertices1);
    popelyshko::Polygon testPolygon2(3, vertices2);
    testPolygon2 = testPolygon1;
    BOOST_CHECK_CLOSE(testPolygon1.getArea(), testPolygon2.getArea(), ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestPolygonMoveConstructor)
  {
    const popelyshko::point_t vertices[] = {{3, 3}, {9, 2}, {9, 9}, {2, 9}};
    popelyshko::Polygon testPolygon1(4, vertices);
    const double areaBeforeMove = testPolygon1.getArea();
    popelyshko::Polygon testPolygon2 = std::move(testPolygon1);
    BOOST_CHECK_CLOSE(areaBeforeMove, testPolygon2.getArea(), ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestPolygonMoveAssignmentOperator)
  {
    const popelyshko::point_t vertices1[] = {{3, 3}, {9, 2}, {9, 9}, {2, 9}};
    const popelyshko::point_t vertices2[] = {{0, 0}, {1, 1}, {2, 0}};
    popelyshko::Polygon testPolygon1(4, vertices1);
    const double areaBeforeMove = testPolygon1.getArea();
    popelyshko::Polygon testPolygon2(3, vertices2);
    testPolygon2 = std::move(testPolygon1);
    BOOST_CHECK_CLOSE(areaBeforeMove, testPolygon2.getArea(), ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestPolygonRotate)
  {
    const popelyshko::point_t vertices[] = {{2, 3}, {3, 2}, {9, 2}, {9, 3}, {9, 9}, {3, 9}};
    popelyshko::Polygon testPolygon(6, vertices);
    const double areaBeforeRotation = testPolygon.getArea();
    testPolygon.rotate(22.2);
    double areaAfterRotation = testPolygon.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotation, areaAfterRotation, ERROR);
    testPolygon.rotate(-22.2);
    areaAfterRotation = testPolygon.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotation, areaAfterRotation, ERROR);
    testPolygon.rotate(0);
    areaAfterRotation = testPolygon.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotation, areaAfterRotation, ERROR);
  }

BOOST_AUTO_TEST_SUITE_END();
