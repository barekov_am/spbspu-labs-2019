#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "shape.hpp"

namespace popelyshko {
  class Polygon: public Shape {
  public:
    Polygon(const Polygon &other);
    Polygon(Polygon &&other) noexcept;
    Polygon(const int &, const point_t *vertex);
    ~Polygon() override;

    Polygon &operator=(const Polygon &);
    Polygon &operator=(Polygon &&) noexcept;

    bool isConvex() const;
    point_t getCenter() const;
    void move(const point_t &) override;
    void move(const double &, const double &) override;
    void scale(const double) override;
    void rotate(const double) override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printInformation() const override;

  private:
    int num_of_sides_;
    point_t *vertices_;
  };
}
#endif //POLYGON_HPP
