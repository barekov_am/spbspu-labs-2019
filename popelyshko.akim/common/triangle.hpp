#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace popelyshko {
  class Triangle: public Shape {
  public:
    Triangle(const point_t &, const point_t &, const point_t &);

    point_t getCenter() const;
    void move(const point_t &) override;
    void move(const double &, const double &) override;
    void scale(const double) override;
    void rotate(const double) override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printInformation() const override;

  private:
    point_t vertexA_;
    point_t vertexB_;
    point_t vertexC_;
  };
}
#endif //TRIANGLE_HPP
