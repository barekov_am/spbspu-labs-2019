#include "circle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

popelyshko::Circle::Circle(const point_t &center, const double &radius):
  center_(center),
  radius_(radius)
{
  if (radius_ <= 0) {
    throw std::invalid_argument("Radius must be positive");
  }
}

void popelyshko::Circle::move(const point_t &position)
{
  center_ = position;
}

void popelyshko::Circle::move(const double &dx, const double &dy)
{
  center_.x += dx;
  center_.y += dy;
}

void popelyshko::Circle::scale(const double coefficient)
{
  if (coefficient <= 0) {
    throw std::invalid_argument("Scale coefficient must be positive");
  }
  radius_ *= coefficient;
}

void popelyshko::Circle::rotate(const double)
{
}

double popelyshko::Circle::getArea() const
{
  return (M_PI * radius_ * radius_);
}

popelyshko::rectangle_t popelyshko::Circle::getFrameRect() const
{
  return {center_, radius_ * 2, radius_ * 2};
}

void popelyshko::Circle::printInformation() const
{
  std::cout << "circle information:\n";
  std::cout << "center: (" << center_.x << ", " << center_.y << ")\n";
  std::cout << "radius = " << radius_ << "\n";
  std::cout << "area = " << getArea() << "\n";
  std::cout << "circle's frame rectangle information:" << "\n";
  const rectangle_t frameRectangle = getFrameRect();
  std::cout << "center: (" << frameRectangle.pos.x << ", " << frameRectangle.pos.y << ")\n";
  std::cout << "width = " << frameRectangle.width << "\n";
  std::cout << "height = " << frameRectangle.height << "\n\n";
}


