#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double ERROR = 0.001;

BOOST_AUTO_TEST_SUITE(TestCircle)

  BOOST_AUTO_TEST_CASE(TestCircleMoveBy)
  {
    popelyshko::Circle testCircle({2.39, 3.92}, 9.23);
    const double areaBeforeMove = testCircle.getArea();
    const popelyshko::rectangle_t frameBeforeMove = testCircle.getFrameRect();
    testCircle.move(1.4, 8.8);
    const double areaAfterMove = testCircle.getArea();
    const popelyshko::rectangle_t frameAfterMove = testCircle.getFrameRect();
    BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, ERROR);
    BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, ERROR);
    BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestCircleMoveTo)
  {
    popelyshko::Circle testCircle({2.39, 3.92}, 9.23);
    const double areaBeforeMove = testCircle.getArea();
    const popelyshko::rectangle_t frameBeforeMove = testCircle.getFrameRect();
    testCircle.move({1.4, 8.8});
    const double areaAfterMove = testCircle.getArea();
    const popelyshko::rectangle_t frameAfterMove = testCircle.getFrameRect();
    BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMove.height, ERROR);
    BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMove.width, ERROR);
    BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMove, ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestCircleScale)
  {
    popelyshko::Circle testCircle({2.39, 3.92}, 9.23);
    const double areaBeforeScale = testCircle.getArea();
    double coefficient = 2;
    testCircle.scale(coefficient);
    double areaAfterScale = testCircle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale * coefficient * coefficient, areaAfterScale, ERROR);
    coefficient = 0.5;
    testCircle.scale(coefficient);
    areaAfterScale = testCircle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale, areaAfterScale, ERROR);
    coefficient = 1;
    testCircle.scale(coefficient);
    areaAfterScale = testCircle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScale, areaAfterScale, ERROR);
  }

  BOOST_AUTO_TEST_CASE(TestCircleInvalidRadius)
  {
    BOOST_CHECK_THROW(popelyshko::Circle testCircle({2.39, 3.92}, -9.23), std::invalid_argument);
    BOOST_CHECK_THROW(popelyshko::Circle testCircle({2.39, 3.92}, 0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TestCircleInvalidScaleCoefficient)
  {
    popelyshko::Circle testCircle({2.39, 3.92}, 9.23);
    BOOST_CHECK_THROW(testCircle.scale(-20.19), std::invalid_argument);
    BOOST_CHECK_THROW(testCircle.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TestCircleRotate)
  {
    popelyshko::Circle testCircle({2.39, 3.92}, 9.23);
    const double areaBeforeRotation = testCircle.getArea();
    testCircle.rotate(22.2);
    double areaAfterRotation = testCircle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotation, areaAfterRotation, ERROR);
    testCircle.rotate(-22.2);
    areaAfterRotation = testCircle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotation, areaAfterRotation, ERROR);
    testCircle.rotate(0);
    areaAfterRotation = testCircle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeRotation, areaAfterRotation, ERROR);
  }

BOOST_AUTO_TEST_SUITE_END();
