#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace popelyshko {
  struct point_t {
    double x;
    double y;
  };

  struct rectangle_t {
    point_t pos;
    double width;
    double height;
  };

  point_t getShift(point_t &, point_t &, double &);
}
#endif //BASE_TYPES_HPP
