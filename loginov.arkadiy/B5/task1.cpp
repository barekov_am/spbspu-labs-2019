#include <iostream>
#include <set>
#include <algorithm>

void task1()
{
    std::set<std::string> words;
    std::string str;

    while (std::getline(std::cin, str))
    {
        if (std::cin.fail())
        {
            throw std::runtime_error("Could not read");
        }

        while (str.find_first_of(" \t\n") == 0)
        {
            str.erase(0, 1);
        }

        while (!str.empty())
        {
            size_t pos = str.find_first_of(" \t\n");

            words.emplace(str.substr(0, pos));
            str.erase(0, pos);

            while (str.find_first_of(" \t\n") == 0)
            {
                str.erase(0, 1);
            }
        }
    }

    for (std::set<std::string>::const_iterator it = words.begin(); it != words.end(); it++)
    {
        std::cout << *it << std::endl;
    }
}
