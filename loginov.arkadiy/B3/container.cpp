#include "container.hpp"

Iterator Container::begin()
{
    return Iterator(Container::MIN_POSITION);
}

Iterator Container::end()
{
    return Iterator(Container::MAX_POSITION);
}
