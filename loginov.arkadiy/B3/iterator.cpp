#include "iterator.hpp"
#include "container.hpp"

Iterator::Iterator() :
        Iterator(1)
{ }

Iterator::Iterator(size_t position) :
        position_(position),
        value_(getValue(position))
{ }

const size_t *Iterator::operator ->()
{
    return &value_;
};

const size_t &Iterator::operator *()
{
    return value_;
}

Iterator &Iterator::operator ++()
{
    if (position_ < Container::MAX_POSITION)
    {
        position_++;
        value_ *= position_;
    }

    return *this;
}

Iterator Iterator::operator ++(int)
{
    Iterator it = *this;
    (it)++;
    return it;
}

Iterator &Iterator::operator --()
{
    if (position_ > Container::MIN_POSITION)
    {
        value_ /= position_;
        position_--;
    }

    return *this;
}

Iterator Iterator::operator --(int)
{
    Iterator it = *this;
    --(it);
    return it;
}

bool Iterator::operator ==(const Iterator &it)
{
    return (position_ == it.position_);
}

bool Iterator::operator !=(const Iterator &it)
{
    return !(*this == it);
}

size_t Iterator::getValue(size_t number)
{
    size_t result = 1;
    for (size_t i = 1; i <= number; i++)
    {
        result *= i;
    }
    return result;
}
