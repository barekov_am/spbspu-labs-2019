#include <iostream>
#include <cstring>
#include <functional>
#include <sstream>
#include "instructions.hpp"

void task1()
{
    BookmarkHandler handler;
    std::string line;

    while (getline(std::cin, line))
    {
        if (std::cin.fail())
        {
            throw std::ios_base::failure("Input failed");
        }

        std::stringstream str(line);
        std::string command;
        str >> command;

        if (strcmp("add", command.c_str()) == 0)
        {
            add(handler, str);
        }
        else if (strcmp("store", command.c_str()) == 0)
        {
            store(handler, str);
        }
        else if (strcmp("insert", command.c_str()) == 0)
        {
            insert(handler, str);
        }
        else if (strcmp("delete", command.c_str()) == 0)
        {
            eDelete(handler, str);
        }
        else if (strcmp("show", command.c_str()) == 0)
        {
            show(handler, str);
        }
        else if (strcmp("move", command.c_str()) == 0)
        {
            move(handler, str);
        }
        else
        {
            std::cout << "<INVALID COMMAND>" << std::endl;
        }
    }
}
