#ifndef BookmarkHandler_HPP
#define BookmarkHandler_HPP

#include "phone_book.hpp"
#include <map>

class BookmarkHandler
{
public:
    enum class Insert
    {
        before,
        after
    };

    enum class Move
    {
        first,
        last
    };

    BookmarkHandler();

    void add(const PhoneBook::record_t &);
    void store(const std::string&, const std::string &);
    void insert(Insert, const std::string &, const PhoneBook::record_t &);
    void erase(const std::string &);
    void show(const std::string &);
    void move(const std::string &, int);
    void move(const std::string &, Move);

private:
    using bookmarks = std::map<std::string, PhoneBook::iterator>;

    PhoneBook records_;
    bookmarks bookmarks_;

    bookmarks::iterator getBookmarkIterator(const std::string &);
};

#endif
