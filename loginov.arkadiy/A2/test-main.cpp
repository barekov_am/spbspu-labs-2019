#define BOOST_TEST_MODULE A2

#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

const double EPSILON = 1e-3;

BOOST_AUTO_TEST_SUITE(TestSuiteA2)

void testShapeMove(loginov::Shape &shape, double dx, double dy)
{
  const loginov::rectangle_t frameRectPrev = shape.getFrameRect();
  const double areaPrev = shape.getArea();

  shape.move(dx, dy);
  BOOST_CHECK_CLOSE(frameRectPrev.width, shape.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectPrev.height, shape.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaPrev, shape.getArea(), EPSILON);
}

void testShapeMoveToPoint(loginov::Shape &shape, loginov::point_t point)
{
  const loginov::rectangle_t frameRectPrev = shape.getFrameRect();
  const double areaPrev = shape.getArea();

  shape.move(point);
  BOOST_CHECK_CLOSE(frameRectPrev.width, shape.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectPrev.height, shape.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaPrev, shape.getArea(), EPSILON);
}

void testShapeChangeArea(loginov::Shape &shape, double scaleCoef)
{
  const double areaPrev = shape.getArea();

  shape.scale(scaleCoef);
  BOOST_CHECK_CLOSE(areaPrev * scaleCoef * scaleCoef, shape.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(frameShapesConstantAfterMove)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);
  testShapeMove(rectangle, 1.0, 2.5);
  testShapeMove(rectangle, 10.0, 1.0);
  testShapeMoveToPoint(rectangle, {2.0, 1.0});
  testShapeMoveToPoint(rectangle, {5.0, 10.0});

  loginov::Circle circle({5.0, 5.0}, 2.0);
  testShapeMove(circle, 1.0, 2.5);
  testShapeMove(circle, 10.0, 1.0);
  testShapeMoveToPoint(circle, {2.0, 1.0});
  testShapeMoveToPoint(circle, {5.0, 10.0});

  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});
  testShapeMove(triangle, 1.0, 2.5);
  testShapeMove(triangle, 10.0, 1.0);
  testShapeMoveToPoint(triangle, {2.0, 1.0});
  testShapeMoveToPoint(triangle, {5.0, 10.0});
}

BOOST_AUTO_TEST_CASE(shapeAreaAfterScale)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);
  testShapeChangeArea(rectangle, 2.0);
  testShapeChangeArea(rectangle, 0.1);
  testShapeChangeArea(rectangle, 15.3);

  loginov::Circle circle({5.0, 5.0}, 2.0);
  testShapeChangeArea(circle, 2.0);
  testShapeChangeArea(circle, 0.1);
  testShapeChangeArea(circle, 15.2);

  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});
  testShapeChangeArea(triangle, 2.0);
  testShapeChangeArea(triangle, 0.1);
  testShapeChangeArea(triangle, 15.1);
}

BOOST_AUTO_TEST_CASE(shapeIllegalArgument)
{
  BOOST_CHECK_THROW(loginov::Rectangle({5.0, 5.0}, -2.0, 1.0), std::invalid_argument);
  BOOST_CHECK_THROW(loginov::Rectangle({5.0, 5.0}, 2.0, -1.0), std::invalid_argument);
  BOOST_CHECK_THROW(loginov::Rectangle({5.0, 5.0}, -2.0, -1.0), std::invalid_argument);

  BOOST_CHECK_THROW(loginov::Circle({5.0, 5.0}, 0.0), std::invalid_argument);
  BOOST_CHECK_THROW(loginov::Circle({5.0, 5.0}, -1.0), std::invalid_argument);
  BOOST_CHECK_THROW(loginov::Circle({5.0, 5.0}, -5.1), std::invalid_argument);

  BOOST_CHECK_THROW(loginov::Triangle({5.0, 5.0}, {2.0, 5.0}, {2.0, 5.0}), std::invalid_argument);
  BOOST_CHECK_THROW(loginov::Triangle({5.0, 5.0}, {5.0, 1.0}, {5.0, 1.0}), std::invalid_argument);
  BOOST_CHECK_THROW(loginov::Triangle({0.0, 0.0}, {0.0, 0.0}, {0.0, 0.0}), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
