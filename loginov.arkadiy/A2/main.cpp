#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "base-types.hpp"

void printFrameRect(const loginov::Shape &shape)
{
  loginov::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "Width = " << frameRect.width << "  ";
  std::cout << "Height = " << frameRect.height << "  ";
  std::cout << "Centre = [ " << frameRect.pos.x << ", " << frameRect.pos.y << " ]" << std::endl;
}

void printArea(const loginov::Shape &shape)
{
  std::cout << "Area = " << shape.getArea() << std::endl;
}

void printCentre(const loginov::Shape &shape)
{
  loginov::point_t centre = shape.getCentre();
  std::cout << "Centre = [ " << centre.x << ", " << centre.y << " ]" << std::endl;
}

int main()
{
  loginov::point_t pointBegin = {4.5, 3.5};
  loginov::Rectangle rect(pointBegin, 1.0, 1.0);
  loginov::Circle circle(pointBegin, 7.0);
  loginov::Triangle triangle({{0.0, 0.0}, {10.0, 10.0}, {10.0, 0.0}});
  loginov::point_t pointNew = {10.0, 10.0};

  std::cout << "Rectangle" << std::endl;
  printFrameRect(rect);
  std::cout << "Before scale: ";
  printArea(rect);
  rect.scale(2.0);
  std::cout << "After scale: ";
  printArea(rect);
  rect.move(pointNew);
  printCentre(rect);
  std::cout << std::endl;

  std::cout << "Circle" << std::endl;
  printFrameRect(circle);
  std::cout << "Before scale: ";
  printArea(circle);
  circle.scale(3.0);
  std::cout << "After scale: ";
  printArea(circle);
  circle.move(2.1, 2.5);
  printCentre(circle);
  std::cout << std::endl;

  std::cout << "Triangle" << std::endl;
  printFrameRect(triangle);
  std::cout << "Before scale: ";
  printArea(triangle);
  triangle.scale(3.0);
  std::cout << "After scale: ";
  printArea(triangle);
  triangle.move({3, 3});
  printCentre(triangle);
  std::cout << std::endl;

  return 0;
}
