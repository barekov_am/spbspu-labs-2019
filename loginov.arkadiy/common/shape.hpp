#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"

namespace loginov
{
  class Shape
  {
  public:
    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual point_t getCentre() const = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void move(const point_t&) = 0;
    virtual void scale(double scaleCoef) = 0;
    virtual void rotate(double angle) = 0;

    static bool intersect(const Shape& shape1, const Shape& shape2);
  };
}

#endif
