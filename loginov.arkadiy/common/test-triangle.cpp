#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double EPSILON = 1e-3;

BOOST_AUTO_TEST_SUITE(TestTriangle)
BOOST_AUTO_TEST_CASE(frameShapesConstantAfterMove)
{
  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});

  const loginov::rectangle_t frameRectPrev = triangle.getFrameRect();
  const double areaPrev = triangle.getArea();

  triangle.move(0.1, 2.5);
  BOOST_CHECK_CLOSE(frameRectPrev.width, triangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectPrev.height, triangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaPrev, triangle.getArea(), EPSILON);

  triangle.move({2.1, 3.2});
  BOOST_CHECK_CLOSE(frameRectPrev.width, triangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectPrev.height, triangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaPrev, triangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(shapeAreaAfterScale)
{
  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});

  const double areaPrev = triangle.getArea();
  const double scaleCoef = 0.4;

  triangle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(areaPrev * scaleCoef * scaleCoef, triangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(shapeAreaAfterRotate)
{
  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});
  const double areaPrev = triangle.getArea();
  const double angles[] = {1.0, 23.12, 323.1, 1234.1, -2134.4, 1.0, 23.12, 323.1, 1234.1, -2134.4};
  for (int i = 0; i < 10; ++i)
  {
    triangle.rotate(angles[i]);
    BOOST_CHECK_CLOSE(areaPrev , triangle.getArea(), EPSILON);
  }
}

BOOST_AUTO_TEST_CASE(shapeIllegalArgument)
{
  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});

  BOOST_CHECK_THROW(loginov::Triangle({5.0, 5.0}, {2.0, 5.0}, {2.0, 5.0}), std::invalid_argument);
  BOOST_CHECK_THROW(loginov::Triangle({5.0, 5.0}, {5.0, 1.0}, {5.0, 1.0}), std::invalid_argument);
  BOOST_CHECK_THROW(loginov::Triangle({0.0, 0.0}, {0.0, 0.0}, {0.0, 0.0}), std::invalid_argument);
  BOOST_CHECK_THROW(triangle.scale(-4.1), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()
