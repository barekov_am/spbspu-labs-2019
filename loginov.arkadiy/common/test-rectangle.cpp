#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double EPSILON = 1e-2;

BOOST_AUTO_TEST_SUITE(TestRectangle)
BOOST_AUTO_TEST_CASE(frameShapesConstantAfterMove)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);
  const loginov::rectangle_t frameRectPrev = rectangle.getFrameRect();
  const double areaPrev = rectangle.getArea();

  rectangle.move({4.5, -9.1});
  BOOST_CHECK_CLOSE(frameRectPrev.width, rectangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectPrev.height, rectangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaPrev, rectangle.getArea(), EPSILON);

  rectangle.move(-4.5, 1.2);
  BOOST_CHECK_CLOSE(frameRectPrev.width, rectangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectPrev.height, rectangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaPrev, rectangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(shapeAreaAfterScale)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);

  const double areaPrev = rectangle.getArea();
  const double scaleCoef = 2.3;

  rectangle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(areaPrev * scaleCoef * scaleCoef, rectangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(shapeAreaAfterRotate)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);

  const double areaPrev = rectangle.getArea();
  const double angles[] = {1.0, 23.12, 323.1, 1234.1, -2134.4, 1.0, 23.12, 323.1, 1234.1, -2134.4};
  for (int i = 0; i < 10; ++i)
  {
    rectangle.rotate(angles[i]);
    BOOST_CHECK_CLOSE(areaPrev , rectangle.getArea(), EPSILON);
  }
}

BOOST_AUTO_TEST_CASE(shapeFrameRectAfterRotate)
{
  const double width = 10.0;
  const double height = 6.0;
  const double angle = 90.0;
  loginov::Rectangle rectangle({5.0, 5.0}, width, height);
  rectangle.rotate(angle);

  BOOST_CHECK_CLOSE(height, rectangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(width, rectangle.getFrameRect().height, EPSILON);

  rectangle.rotate(angle);
  BOOST_CHECK_CLOSE(width, rectangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height, rectangle.getFrameRect().height, EPSILON);
}

BOOST_AUTO_TEST_CASE(shapeIllegalArgument)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);
  BOOST_CHECK_THROW(loginov::Rectangle({5.0, 5.0}, -2.0, 1.0), std::invalid_argument);
  BOOST_CHECK_THROW(loginov::Rectangle({5.0, 5.0}, 2.0, -1.0), std::invalid_argument);
  BOOST_CHECK_THROW(loginov::Rectangle({5.0, 5.0}, -2.0, -1.0), std::invalid_argument);
  BOOST_CHECK_THROW(rectangle.scale(-1), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()

