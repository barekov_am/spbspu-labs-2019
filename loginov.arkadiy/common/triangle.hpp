#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace loginov
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t &, const point_t &, const point_t &);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    point_t getCentre() const override;
    void move(double dx, double dy) override;
    void move(const point_t &) override;
    void scale(double scaleCoef) override;
    void rotate(double angle) override;

  private:
    point_t p1_, p2_, p3_;
  };
}

#endif
