#ifndef A4_MATRIX_HPP
#define A4_MATRIX_HPP

#include <bits/unique_ptr.h>
#include <shape.hpp>
#include "composite-shape.hpp"

namespace loginov
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(size_t, size_t);
    Matrix(const Matrix& other);
    Matrix(Matrix&& other);
    ~Matrix() = default;

    Matrix& operator =(const Matrix& other);
    Matrix& operator =(Matrix&& other);

    std::unique_ptr<Shape* []> operator [](unsigned int index) const;
    bool operator ==(const Matrix& other) const;
    bool operator !=(const Matrix& other) const;

    void add(Shape& shape, unsigned int row, unsigned int column);
    void clear();

    size_t getRows() const;
    size_t getColumns() const;
    size_t size() const;
    size_t capacity() const;

    std::unique_ptr<Shape* []> array() const;
    static Matrix separate(const CompositeShape& compositeShape);

  private:
    const size_t DEFAULT_CAP_ROWS = 5;
    const size_t DEFAULT_CAP_COLUMNS = 5;
    size_t rows_;
    size_t columns_;
    size_t cap_rows_;
    size_t cap_columns_;
    std::unique_ptr<Shape* []> matrix_;

    void increaseMatrix(size_t, size_t);
  };
}

#endif //A4_MATRIX_HPP
