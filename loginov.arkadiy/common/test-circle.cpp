#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double EPSILON = 1e-3;

BOOST_AUTO_TEST_SUITE(TestCircle)

BOOST_AUTO_TEST_CASE(circleAreaConstantAfterMove)
{
  loginov::Circle circle({5.0, 5.0}, 2.0);

  const loginov::rectangle_t frameRectPrev = circle.getFrameRect();
  const double areaPrev = circle.getArea();

  circle.move(1.0, 2.5);
  BOOST_CHECK_CLOSE(frameRectPrev.width, circle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectPrev.height, circle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaPrev, circle.getArea(), EPSILON);

  circle.move({5.7, 2.4});

  BOOST_CHECK_CLOSE(frameRectPrev.width, circle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectPrev.height, circle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaPrev, circle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(circleAreaAfterScale)
{
  loginov::Circle circle({5.0, 5.0}, 2.0);

  const double scaleCoef = 0.6;
  const double areaPrev = circle.getArea();

  circle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(areaPrev * scaleCoef * scaleCoef, circle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(circleAreaAfterRotate)
{
  loginov::Circle circle({5.0, 5.0}, 2.0);
  const double areaPrev = circle.getArea();
  const double angles[] = {1.0, 23.12, 323.1, 1234.1, -2134.4, 1.0, 23.12, 323.1, 1234.1, -2134.4};
  for (int i = 0; i < 10; ++i)
  {
    circle.rotate(angles[i]);
    BOOST_CHECK_CLOSE(areaPrev, circle.getArea(), EPSILON);
  }
}

BOOST_AUTO_TEST_CASE(shapeIllegalArgument)
{
  loginov::Circle circle({5.0, 5.0}, 2.0);

  BOOST_CHECK_THROW(loginov::Circle({5.0, 5.0}, 0.0), std::invalid_argument);
  BOOST_CHECK_THROW(loginov::Circle({5.0, 5.0}, -1.0), std::invalid_argument);
  BOOST_CHECK_THROW(loginov::Circle({5.0, 5.0}, -5.1), std::invalid_argument);
  BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
