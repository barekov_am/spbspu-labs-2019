#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double EPSILON = 1e-2;

BOOST_AUTO_TEST_SUITE(pointRotate)

  BOOST_AUTO_TEST_CASE(testRotate)
  {
    const loginov::point_t centre = {0.0, 0.0};
    loginov::point_t point1 = {5.0, 10.0};
    loginov::point_t point2 = loginov::rotatePoint(centre, point1, 180.0);
    BOOST_CHECK_CLOSE(point1.x, -point2.x, EPSILON);
    BOOST_CHECK_CLOSE(point1.y, -point2.y, EPSILON);

    point2 = loginov::rotatePoint(centre, point2, 180.0);

    BOOST_CHECK_CLOSE(point1.x, point2.x, EPSILON);
    BOOST_CHECK_CLOSE(point1.y, point2.y, EPSILON);
  }

BOOST_AUTO_TEST_SUITE_END()

