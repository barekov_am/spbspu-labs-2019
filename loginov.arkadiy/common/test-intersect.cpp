#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double EPSILON = 1e-3;

BOOST_AUTO_TEST_SUITE(TestIntersect)
BOOST_AUTO_TEST_CASE(ShapeIntersect)
  {
    loginov::Rectangle rectangle1({5.0, 5.0}, 2.0, 1.0);
    loginov::Rectangle rectangle2({5.0, 5.0}, 3.0, 1.0);
    loginov::Triangle triangle({105.0, 105.0}, {102.0, 101.0}, {110.0, 110.0});
    loginov::Circle circle({5.0, 5.0}, 1000.0);

    BOOST_CHECK(loginov::Shape::intersect(rectangle1, rectangle2));
    BOOST_CHECK(loginov::Shape::intersect(circle, rectangle1));
    BOOST_CHECK(loginov::Shape::intersect(circle, rectangle2));
    BOOST_CHECK(loginov::Shape::intersect(circle, triangle));
    BOOST_CHECK(!loginov::Shape::intersect(rectangle1, triangle));
    BOOST_CHECK(!loginov::Shape::intersect(rectangle2, triangle));
  }

BOOST_AUTO_TEST_SUITE_END()
