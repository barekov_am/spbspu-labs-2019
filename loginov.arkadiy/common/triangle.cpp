#include "triangle.hpp"
#include <cassert>
#include <cmath>
#include <stdexcept>

loginov::Triangle::Triangle(const point_t& p1, const point_t& p2, const point_t& p3) :
  p1_(p1),
  p2_(p2),
  p3_(p3)
{
  const double EPSILON = 1e-3;
  if (getArea() < EPSILON)
  {
    throw std::invalid_argument("Area of triangle must be positive");
  }
}

double loginov::Triangle::getArea() const
{
  return fabs(((p1_.x - p3_.x) * (p2_.y - p3_.y) - (p2_.x - p3_.x) * (p1_.y - p3_.y))) / 2;
}

loginov::rectangle_t loginov::Triangle::getFrameRect() const
{
  double left = fmin(p1_.x, fmin(p2_.x, p3_.x));
  double right = fmax(p1_.x, fmax(p2_.x, p3_.x));
  double top = fmin(p1_.y, fmin(p2_.y, p3_.y));
  double bottom = fmax(p1_.y, fmax(p2_.y, p3_.y));
  double width = right - left;
  double height = bottom - top;
  double x0 = (left + right) / 2;
  double y0 = (top + bottom) / 2;
  return {width, height, {x0, y0}};
}

void loginov::Triangle::move(double dx, double dy)
{
  p1_.x += dx;
  p2_.x += dx;
  p3_.x += dx;
  p1_.y += dy;
  p2_.y += dy;
  p3_.y += dy;
}

void loginov::Triangle::move(const point_t& point)
{
  point_t preCentre = getCentre();
  move(point.x - preCentre.x, point.y - preCentre.y);
}

loginov::point_t loginov::Triangle::getCentre() const
{
  return {(p1_.x + p2_.x + p3_.x) / 3, (p1_.y + p2_.y + p3_.y) / 3};
}

void loginov::Triangle::scale(double scaleCoef)
{
  if (scaleCoef <= 0)
  {
    throw std::invalid_argument("ScaleCoef must be positive");
  }
  point_t centre = getCentre();
  p2_ = {p1_.x + (p2_.x - p1_.x) * scaleCoef, p1_.y + (p2_.y - p1_.y) * scaleCoef};
  p3_ = {p1_.x + (p3_.x - p1_.x) * scaleCoef, p1_.y + (p3_.y - p1_.y) * scaleCoef};
  move(centre);
}

void loginov::Triangle::rotate(double angle)
{
  const point_t centre = getCentre();
  p1_ = loginov::rotatePoint(centre, p1_, angle);
  p2_ = loginov::rotatePoint(centre, p2_, angle);
  p3_ = loginov::rotatePoint(centre, p3_, angle);
}
