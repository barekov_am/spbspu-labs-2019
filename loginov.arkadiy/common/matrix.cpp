
#include "matrix.hpp"
#include <memory>

loginov::Matrix::Matrix() :
  rows_(0),
  columns_(0),
  cap_rows_(DEFAULT_CAP_ROWS),
  cap_columns_(DEFAULT_CAP_COLUMNS),
  matrix_(std::make_unique<Shape * []>(DEFAULT_CAP_ROWS * DEFAULT_CAP_COLUMNS))
{

}

loginov::Matrix::Matrix(size_t rows, size_t columns) :
  rows_(0),
  columns_(0),
  cap_rows_(rows),
  cap_columns_(columns),
  matrix_(std::make_unique<Shape * []>(rows * columns))
{

}

loginov::Matrix::Matrix(const loginov::Matrix& other) :
  rows_(other.rows_),
  columns_(other.columns_),
  cap_rows_(other.cap_rows_),
  cap_columns_(other.cap_columns_),
  matrix_(std::make_unique<Shape * []>(other.capacity()))
{
  for (unsigned int i = 0; i < other.capacity(); ++i)
  {
    matrix_[i] = other.matrix_[i];
  }
}

loginov::Matrix::Matrix(loginov::Matrix&& other) :
  rows_(other.rows_),
  columns_(other.columns_),
  cap_rows_(other.cap_rows_),
  cap_columns_(other.cap_columns_),
  matrix_(std::move(other.matrix_))
{
  other.clear();
}

loginov::Matrix& loginov::Matrix::operator =(const loginov::Matrix& other)
{
  if (*this != other)
  {
    matrix_ = std::make_unique<Shape* []>(other.capacity());
    for (unsigned int i = 0; i < other.capacity(); ++i)
    {
      matrix_[i] = other.matrix_[i];
    }
    rows_ = other.rows_;
    columns_ = other.columns_;
    cap_rows_ = other.cap_rows_;
    cap_columns_ = other.cap_columns_;
  }

  return *this;
}

loginov::Matrix& loginov::Matrix::operator =(loginov::Matrix&& other)
{
  if (*this != other)
  {
    matrix_ = std::move(other.matrix_);
    rows_ = other.rows_;
    columns_ = other.columns_;
    cap_rows_ = other.cap_rows_;
    cap_columns_ = other.cap_columns_;
    other.clear();
  }

  return *this;
}

std::unique_ptr<loginov::Shape* []> loginov::Matrix::operator [](unsigned int index) const
{
  if (index >= size())
  {
    throw std::invalid_argument("Index out of range!");
  }

  std::unique_ptr<Shape* []> result(std::make_unique<Shape* []>(columns_));
  for (unsigned int i = 0; i < columns_; ++i)
  {
    result[i] = matrix_[cap_rows_ * index + i];
  }

  return result;
}

bool loginov::Matrix::operator ==(const loginov::Matrix& other) const
{
  if ((rows_ != other.rows_) || (columns_ != other.columns_) || (cap_rows_ != other.cap_rows_) ||
      (cap_columns_ != other.cap_columns_))
  {
    return false;
  }

  for (unsigned int i = 0; i < capacity(); ++i)
  {
    if (matrix_[i] != other.matrix_[i])
    {
      return false;
    }
  }

  return true;
}

bool loginov::Matrix::operator !=(const loginov::Matrix& other) const
{
  return !(*this == other);
}

void loginov::Matrix::add(Shape& shape, unsigned int row, unsigned int column)
{
  if (row >= cap_rows_ && column >= cap_columns_)
  {
    increaseMatrix(cap_rows_ << 1, cap_columns_ << 1);
  }
  else if (row >= cap_rows_)
  {
    increaseMatrix(cap_rows_ << 1, cap_columns_);
  }
  else if (column >= cap_columns_)
  {
    increaseMatrix(cap_rows_, cap_columns_ << 1);
  }

  matrix_[row * cap_columns_ + column] = &shape;
  if (row + 1 > rows_)
  {
    rows_ = row + 1;
  }
  if (column + 1 > columns_)
  {
    columns_ = column + 1;
  }
}

void loginov::Matrix::clear()
{
  rows_ = 0;
  columns_ = 0;
  cap_rows_ = 0;
  cap_columns_ = 0;
  matrix_ = nullptr;
}

size_t loginov::Matrix::getRows() const
{
  return rows_;
}

size_t loginov::Matrix::getColumns() const
{
  return columns_;
}

size_t loginov::Matrix::size() const
{
  return rows_ * columns_;
}

size_t loginov::Matrix::capacity() const
{
  return cap_rows_ * cap_columns_;
}

std::unique_ptr<loginov::Shape* []> loginov::Matrix::array() const
{
  std::unique_ptr<Shape* []> array = std::make_unique<Shape* []>(size());

  for (unsigned int i = 0, next = 0; i < capacity() && next < size(); ++i)
  {
    if (matrix_[i] != nullptr)
    {
      array[next++] = matrix_[i];
    }
  }

  return array;
}

loginov::Matrix loginov::Matrix::separate(const loginov::CompositeShape& compositeShape)
{
  Matrix matrix;

  for (unsigned int k = 0; k < compositeShape.size(); k++)
  {
    unsigned int currentRow = 0;
    unsigned int currentColumn = 0;
    for (unsigned int i = 0; i < matrix.getRows(); i++)
    {
      std::unique_ptr<Shape* []> row = matrix[i];
      for (unsigned int j = 0; j < matrix.getColumns(); j++)
      {
        if (row[j] == nullptr)
        {
          currentRow = i;
          currentColumn = j;
          break;
        }

        if (Shape::intersect(compositeShape[k], *row[j]))
        {
          currentRow = i + 1;
          currentColumn = 0;
          break;
        }
        else
        {
          currentRow = i;
          currentColumn = j + 1;
        }
      }

      if (currentRow == i)
      {
        break;
      }
    }

    matrix.add(compositeShape[k], currentRow, currentColumn);
  }

  return matrix;
}

void loginov::Matrix::increaseMatrix(const size_t cap_rows, const size_t cap_columns)
{
  if (cap_rows < cap_rows_ || cap_columns < cap_columns_)
  {
    throw std::invalid_argument("Matrix cannot decrease!");
  }

  std::unique_ptr<Shape* []> newMatrix = std::make_unique<Shape* []>(cap_rows * cap_columns);

  for (unsigned int i = 0; i < cap_rows; ++i)
  {
    for (unsigned int j = 0; j < cap_columns; ++j)
    {
      if (i < cap_rows_ && j < cap_columns_)
      {
        newMatrix[i * cap_columns + j] = matrix_[i * cap_columns_ + j];
      }
    }
  }

  matrix_.swap(newMatrix);
  cap_rows_ = cap_rows;
  cap_columns_ = cap_columns;
}
