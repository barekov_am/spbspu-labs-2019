#include "circle.hpp"
#include <cassert>
#include <cmath>
#include <stdexcept>

loginov::Circle::Circle(const loginov::point_t &point, double radius) :
  centre_(point),
  radius_(radius)
{
  if(radius <= 0.0){
    throw std::invalid_argument("Radius must be positive");
  }
}

double loginov::Circle::getArea() const
{
  return  3.14 * radius_ * radius_;
}

loginov::rectangle_t loginov::Circle::getFrameRect() const
{
  return { 2 * radius_, 2 * radius_, centre_ };
}

void loginov::Circle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

void loginov::Circle::move(const loginov::point_t &point)
{
  centre_ = point;
}

loginov::point_t loginov::Circle::getCentre() const
{
  return getFrameRect().pos;
}

void loginov::Circle::scale(double scaleCoef)
{
  if(scaleCoef <= 0){
    throw std::invalid_argument("ScaleCoef must be positive");
  }
  radius_ *= scaleCoef;
}

void loginov::Circle::rotate(double)
{

}
