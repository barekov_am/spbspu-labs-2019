#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double EPSILON = 1e-3;

BOOST_AUTO_TEST_SUITE(TestCompositeShape)

void equalsShape(const loginov::Shape& shape1, const loginov::Shape& shape2)
{
  loginov::point_t centre1 = shape1.getCentre();
  loginov::point_t centre2 = shape2.getCentre();
  loginov::rectangle_t frameRect1 = shape1.getFrameRect();
  loginov::rectangle_t frameRect2 = shape2.getFrameRect();

  BOOST_CHECK_CLOSE(shape1.getArea(), shape2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(centre1.x, centre2.x, EPSILON);
  BOOST_CHECK_CLOSE(centre1.y, centre2.y, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.height, frameRect2.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.width, frameRect2.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.pos.x, frameRect2.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.pos.y, frameRect2.pos.y, EPSILON);
}

void
equalsCompositeShape(const loginov::CompositeShape& compositeShape1, const loginov::CompositeShape& compositeShape2)
{
  BOOST_CHECK_EQUAL(compositeShape1.size(), compositeShape2.size());
  equalsShape(compositeShape1, compositeShape2);
  for (unsigned int i = 0; i < compositeShape1.size(); i++)
  {
    equalsShape(compositeShape1[i], compositeShape2[i]);
  }
}

BOOST_AUTO_TEST_CASE(frameShapesConstantAfterMove)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);
  loginov::Circle circle({5.0, 5.0}, 2.0);
  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});

  loginov::CompositeShape compositeShape;
  compositeShape.add(rectangle);
  compositeShape.add(triangle);
  compositeShape.add(circle);

  const loginov::rectangle_t frameRectPrev = compositeShape.getFrameRect();
  const double areaPrev = compositeShape.getArea();

  compositeShape.move({0.1, 3.5});

  BOOST_CHECK_CLOSE(frameRectPrev.width, compositeShape.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectPrev.height, compositeShape.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaPrev, compositeShape.getArea(), EPSILON);

  compositeShape.move(0.1, 3.5);

  BOOST_CHECK_CLOSE(frameRectPrev.width, compositeShape.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(frameRectPrev.height, compositeShape.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(areaPrev, compositeShape.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(shapeAreaAfterScale)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);
  loginov::Circle circle({5.0, 5.0}, 2.0);
  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});

  loginov::CompositeShape compositeShape;
  compositeShape.add(rectangle);
  compositeShape.add(triangle);
  compositeShape.add(circle);

  const double areaPrev = compositeShape.getArea();
  const double scaleCoef = 1.6;

  compositeShape.scale(scaleCoef);
  BOOST_CHECK_CLOSE(areaPrev * scaleCoef * scaleCoef, compositeShape.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(shapeAreaAfterRotate)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);
  loginov::Circle circle({5.0, 5.0}, 2.0);
  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});

  loginov::CompositeShape compositeShape;
  compositeShape.add(rectangle);
  compositeShape.add(triangle);
  compositeShape.add(circle);

  const double areaPrev = compositeShape.getArea();
  const double angles[] = {1.0, 23.12, 323.1, 1234.1, -2134.4, 1.0, 23.12, 323.1, 1234.1, -2134.4};
  for (int i = 0; i < 10; ++i)
  {
    compositeShape.rotate(angles[i]);
    BOOST_CHECK_CLOSE(areaPrev , compositeShape.getArea(), EPSILON);
  }
}

BOOST_AUTO_TEST_CASE(compositeShapeRemoveTest)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);
  loginov::Circle circle({5.0, 5.0}, 2.0);
  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});

  loginov::CompositeShape compositeShape;
  compositeShape.add(rectangle);
  compositeShape.add(triangle);
  compositeShape.add(circle);

  while (compositeShape.size() > 0)
  {
    unsigned int preSize = compositeShape.size();
    compositeShape.remove(preSize - 1);
    BOOST_CHECK_EQUAL(preSize, compositeShape.size() + 1);
  }
}

BOOST_AUTO_TEST_CASE(TestCopyOperatorEmpty)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);
  loginov::Circle circle({5.0, 5.0}, 2.0);
  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});

  loginov::CompositeShape compositeShape1;
  compositeShape1.add(rectangle);
  compositeShape1.add(triangle);
  compositeShape1.add(circle);

  loginov::CompositeShape compositeShape2;
  compositeShape2 = compositeShape1;
  equalsCompositeShape(compositeShape1, compositeShape2);
}

BOOST_AUTO_TEST_CASE(TestCopyConstructor)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);
  loginov::Circle circle({5.0, 5.0}, 2.0);
  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});

  loginov::CompositeShape compositeShape1;
  compositeShape1.add(rectangle);
  compositeShape1.add(triangle);
  compositeShape1.add(circle);

  loginov::CompositeShape compositeShape2(compositeShape1);
  equalsCompositeShape(compositeShape1, compositeShape2);
}

BOOST_AUTO_TEST_CASE(TestCopyOperatorNotEmpty)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);
  loginov::Circle circle({5.0, 5.0}, 2.0);
  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});

  loginov::CompositeShape compositeShape1;
  compositeShape1.add(rectangle);
  compositeShape1.add(triangle);
  compositeShape1.add(circle);

  loginov::CompositeShape compositeShape2;
  compositeShape2.add(triangle);

  compositeShape2 = compositeShape1;
  equalsCompositeShape(compositeShape1, compositeShape2);
}

BOOST_AUTO_TEST_CASE(TestMoveConstructor)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);
  loginov::Circle circle({5.0, 5.0}, 2.0);
  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});

  loginov::CompositeShape compositeShape1;
  compositeShape1.add(rectangle);
  compositeShape1.add(triangle);
  compositeShape1.add(circle);

  loginov::CompositeShape copyCompositeShape1 = compositeShape1;
  loginov::CompositeShape compositeShape2(std::move(compositeShape1));
  equalsCompositeShape(copyCompositeShape1, compositeShape2);
}

BOOST_AUTO_TEST_CASE(TestMoveOperator)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);
  loginov::Circle circle({5.0, 5.0}, 2.0);
  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});

  loginov::CompositeShape compositeShape1;
  compositeShape1.add(rectangle);
  compositeShape1.add(triangle);
  compositeShape1.add(circle);

  loginov::CompositeShape copyCompositeShape1 = compositeShape1;
  loginov::CompositeShape compositeShape2;
  compositeShape2 = std::move(compositeShape1);
  equalsCompositeShape(copyCompositeShape1, compositeShape2);
}

BOOST_AUTO_TEST_CASE(shapeIllegalArgument)
{
  loginov::Rectangle rectangle({5.0, 5.0}, 2.0, 1.0);
  loginov::Circle circle({5.0, 5.0}, 2.0);
  loginov::Triangle triangle({5.0, 5.0}, {2.0, 1.0}, {10.0, 10.0});

  loginov::CompositeShape compositeShape;
  compositeShape.add(rectangle);
  compositeShape.add(triangle);
  compositeShape.add(circle);

  BOOST_CHECK_THROW(compositeShape.scale(-1), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.scale(0), std::invalid_argument);

  BOOST_CHECK_THROW(compositeShape.remove(5), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.remove(6), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()

