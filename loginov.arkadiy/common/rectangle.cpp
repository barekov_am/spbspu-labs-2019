#include "rectangle.hpp"
#include <cassert>
#include <cmath>
#include <stdexcept>

loginov::Rectangle::Rectangle(const point_t& point, double width, double height) :
  rectangle_({width, height, point}),
  angle_(0.0)
{
  if ((width <= 0.0) || (height <= 0.0))
  {
    throw std::invalid_argument("Width and height must be positive");
  }
}

double loginov::Rectangle::getArea() const
{
  return rectangle_.width * rectangle_.height;
}

loginov::rectangle_t loginov::Rectangle::getFrameRect() const
{
  double width = fabs(rectangle_.width * cos(angle_ / RADIX_GRAD)) + fabs(rectangle_.height * sin(angle_ / RADIX_GRAD));
  double height = fabs(rectangle_.width * sin(angle_ / RADIX_GRAD)) + fabs(rectangle_.height * cos(angle_ / RADIX_GRAD));
  return {width, height, rectangle_.pos};
}

void loginov::Rectangle::move(double dx, double dy)
{
  rectangle_.pos.x += dx;
  rectangle_.pos.y += dy;
}

void loginov::Rectangle::move(const point_t& point)
{
  rectangle_.pos = point;
}

loginov::point_t loginov::Rectangle::getCentre() const
{
  return rectangle_.pos;
}

void loginov::Rectangle::scale(double scaleCoef)
{
  if (scaleCoef <= 0)
  {
    throw std::invalid_argument("ScaleCoef must be positive");
  }
  rectangle_.width *= scaleCoef;
  rectangle_.height *= scaleCoef;
}

void loginov::Rectangle::rotate(double angle)
{
  angle_ += angle;
  angle_ = fmod(angle_, FULL_ANGLE);
}
