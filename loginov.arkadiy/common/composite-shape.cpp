#include "composite-shape.hpp"
#include <stdexcept>
#include <cmath>
#include <limits>

loginov::CompositeShape::CompositeShape() :
  size_(0),
  shapes_(std::make_unique<Shape * []>(0))
{

}

loginov::CompositeShape::CompositeShape(const loginov::CompositeShape& other) :
  size_(other.size_),
  shapes_(std::make_unique<Shape * []>(other.size_))
{
  for (unsigned int i = 0; i < size_; ++i)
  {
    shapes_[i] = other.shapes_[i];
  }
}

loginov::CompositeShape::CompositeShape(loginov::CompositeShape&& other) :
  size_(other.size_),
  shapes_(std::move(other.shapes_))
{
  other.size_ = 0;
  other.shapes_ = nullptr;
}

loginov::CompositeShape& loginov::CompositeShape::operator =(const loginov::CompositeShape& other)
{
  if (this == &other)
  {
    return *this;
  }

  shapes_ = std::make_unique<Shape* []>(other.size());
  for (unsigned int i = 0; i < other.size(); ++i)
  {
    shapes_[i] = other.shapes_[i];
  }
  size_ = other.size();

  return *this;
}

loginov::CompositeShape& loginov::CompositeShape::operator =(loginov::CompositeShape&& other)
{
  if (this == &other)
  {
    return *this;
  }

  size_ = other.size();
  shapes_ = std::move(other.shapes_);
  other.size_ = 0;
  return *this;
}

loginov::Shape& loginov::CompositeShape::operator [](unsigned int index) const
{
  if (index >= size_)
  {
    throw std::invalid_argument("Index out of range");
  }

  return *shapes_[index];
}

void loginov::CompositeShape::add(loginov::Shape& shape)
{
  std::unique_ptr<Shape* []> tmp(std::make_unique<Shape* []>(size_ + 1));
  for (unsigned int i = 0; i < size_; ++i)
  {
    tmp[i] = shapes_[i];
  }

  tmp[size_++] = &shape;
  shapes_.swap(tmp);
}

void loginov::CompositeShape::remove(unsigned int index)
{
  if (index >= size_)
  {
    throw std::invalid_argument("Index out of range");
  }

  for (unsigned int i = index; i < size_ - 1; ++i)
  {
    shapes_[i] = shapes_[i + 1];
  }
  size_--;
}

size_t loginov::CompositeShape::size() const
{
  return size_;
}

double loginov::CompositeShape::getArea() const
{
  double area = 0;
  for (unsigned int i = 0; i < size_; ++i)
  {
    area += shapes_[i]->getArea();
  }

  return area;
}

loginov::rectangle_t loginov::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    return {0.0, 0.0, {0.0, 0.0}};
  }

  double left = std::numeric_limits<double>::max();
  double right = std::numeric_limits<double>::lowest();
  double top = std::numeric_limits<double>::max();
  double bottom = std::numeric_limits<double>::lowest();

  for (unsigned int i = 0; i < size_; ++i)
  {
    const rectangle_t frameShape = shapes_[i]->getFrameRect();
    left = fmin(left, frameShape.pos.x - frameShape.width / 2);
    right = fmax(right, frameShape.pos.x + frameShape.width / 2);
    top = fmin(top, frameShape.pos.y - frameShape.height / 2);
    bottom = fmax(bottom, frameShape.pos.y + frameShape.height / 2);
  }

  double width = right - left;
  double height = bottom - top;
  const point_t centre = {left + width / 2, top + height / 2};

  return {width, height, centre};
}

loginov::point_t loginov::CompositeShape::getCentre() const
{
  return getFrameRect().pos;
}

void loginov::CompositeShape::move(double dx, double dy)
{
  for (unsigned int i = 0; i < size_; ++i)
  {
    shapes_[i]->move(dx, dy);
  }
}

void loginov::CompositeShape::move(const loginov::point_t& centre)
{
  const point_t preCentre = getCentre();
  move(centre.x - preCentre.x, centre.y - preCentre.y);
}

void loginov::CompositeShape::scale(double scaleCoef)
{
  if (scaleCoef <= 0.0)
  {
    throw std::invalid_argument("scaleCoef must be positive");
  }

  const point_t point = getCentre();
  for (unsigned i = 0; i < size_; ++i)
  {
    shapes_[i]->scale(scaleCoef);
    double dx = point.x - shapes_[i]->getCentre().x;
    double dy = point.y - shapes_[i]->getCentre().y;
    shapes_[i]->move(dx * (scaleCoef - 1), dy * (scaleCoef - 1));
  }
}

void loginov::CompositeShape::rotate(double angle)
{
  const point_t centre = getCentre();
  for (unsigned int i = 0; i < size_; ++i)
  {
    shapes_[i]->rotate(angle);
    point_t centreShape = shapes_[i]->getCentre();
    loginov::rotatePoint(centre, centreShape, angle);
  }
}
