#include "base-types.hpp"

loginov::point_t loginov::rotatePoint(const loginov::point_t& centre, const loginov::point_t& point, double angle)
{
  angle /=  RADIX_GRAD;
  double x = centre.x + (point.x - centre.x) * cos(angle) - (point.y - centre.y) * sin(angle);
  double y = centre.y + (point.y - centre.y) * cos(angle) + (point.x - centre.x) * sin(angle);
  return {x, y};
}
