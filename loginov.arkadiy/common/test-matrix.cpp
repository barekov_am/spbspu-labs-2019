#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

const double EPSILON = 1e-3;

BOOST_AUTO_TEST_SUITE(TestMatrix)

void equalsShape(const loginov::Shape& shape1, const loginov::Shape& shape2)
{
  loginov::point_t centre1 = shape1.getCentre();
  loginov::point_t centre2 = shape2.getCentre();
  loginov::rectangle_t frameRect1 = shape1.getFrameRect();
  loginov::rectangle_t frameRect2 = shape2.getFrameRect();

  BOOST_CHECK_CLOSE(shape1.getArea(), shape2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(centre1.x, centre2.x, EPSILON);
  BOOST_CHECK_CLOSE(centre1.y, centre2.y, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.height, frameRect2.height, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.width, frameRect2.width, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.pos.x, frameRect2.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameRect1.pos.y, frameRect2.pos.y, EPSILON);
}

void equalsMatrix(const loginov::Matrix& matrix1, const loginov::Matrix& matrix2)
{
  BOOST_CHECK_EQUAL(matrix1.size(), matrix2.size());
  std::unique_ptr<loginov::Shape* []> array1 = matrix1.array();
  std::unique_ptr<loginov::Shape* []> array2 = matrix2.array();

  for (unsigned int i = 0; i < matrix1.size(); ++i)
  {
    equalsShape(*array1[i], *array2[i]);
  }
}

BOOST_AUTO_TEST_CASE(TestSeparate)
{
  loginov::Rectangle rectangle1({-2.0, 1.5}, 2.0, 2.0);
  loginov::Circle circle({5.0, 3.5}, 3.0);
  loginov::Triangle triangle({1.0, 3.5}, {4.0, 3.5}, {2.0, 5.5});
  loginov::Rectangle rectangle2({6.5, 2.0}, 2.5, 2.0);

  loginov::CompositeShape compositeShape;
  compositeShape.add(rectangle1);
  compositeShape.add(circle);
  compositeShape.add(triangle);
  compositeShape.add(rectangle2);

  loginov::Matrix matrix = loginov::Matrix::separate(compositeShape);

  BOOST_REQUIRE_EQUAL(matrix.getRows(), 2);
  BOOST_REQUIRE_EQUAL(matrix.getColumns(), 2);
  equalsShape(rectangle1, *matrix[0][0]);
  equalsShape(circle, *matrix[0][1]);
  equalsShape(triangle, *matrix[1][0]);
  equalsShape(rectangle2, *matrix[1][1]);
}

BOOST_AUTO_TEST_CASE(TestCopyOperatorEmpty)
{
  loginov::Rectangle rectangle1({-2.0, 1.5}, 2.0, 2.0);
  loginov::Circle circle({5.0, 3.5}, 3.0);
  loginov::Triangle triangle({1.0, 3.5}, {4.0, 3.5}, {2.0, 5.5});
  loginov::Rectangle rectangle2({6.5, 2.0}, 2.5, 2.0);

  loginov::CompositeShape compositeShape;
  compositeShape.add(rectangle1);
  compositeShape.add(circle);
  compositeShape.add(triangle);
  compositeShape.add(rectangle2);

  loginov::Matrix matrix1 = loginov::Matrix::separate(compositeShape);

  loginov::Matrix matrix2;
  matrix2 = matrix1;

  equalsMatrix(matrix1, matrix2);
}

BOOST_AUTO_TEST_CASE(TestCustomConstructor)
{
  size_t raws = 10;
  size_t columns = 12;
  loginov::Matrix matrix(raws, columns);
  BOOST_CHECK_EQUAL(0, matrix.getRows());
  BOOST_CHECK_EQUAL(0, matrix.getColumns());
  BOOST_CHECK_EQUAL(raws*columns, matrix.capacity());
}

BOOST_AUTO_TEST_CASE(TestCopyConstructor)
{
  loginov::Rectangle rectangle1({-2.0, 1.5}, 2.0, 2.0);
  loginov::Circle circle({5.0, 3.5}, 3.0);
  loginov::Triangle triangle({1.0, 3.5}, {4.0, 3.5}, {2.0, 5.5});
  loginov::Rectangle rectangle2({6.5, 2.0}, 2.5, 2.0);

  loginov::CompositeShape compositeShape;
  compositeShape.add(rectangle1);
  compositeShape.add(circle);
  compositeShape.add(triangle);
  compositeShape.add(rectangle2);

  loginov::Matrix matrix1 = loginov::Matrix::separate(compositeShape);

  loginov::Matrix matrix2(matrix1);
  equalsMatrix(matrix1, matrix2);
}

BOOST_AUTO_TEST_CASE(TestCopyOperatorNotEmpty)
{

  loginov::Rectangle rectangle1({-2.0, 1.5}, 2.0, 2.0);
  loginov::Circle circle({5.0, 3.5}, 3.0);
  loginov::Triangle triangle({1.0, 3.5}, {4.0, 3.5}, {2.0, 5.5});
  loginov::Rectangle rectangle2({6.5, 2.0}, 2.5, 2.0);

  loginov::CompositeShape compositeShape;
  compositeShape.add(rectangle1);
  compositeShape.add(circle);
  compositeShape.add(triangle);
  compositeShape.add(rectangle2);

  loginov::Matrix matrix1 = loginov::Matrix::separate(compositeShape);
  compositeShape.remove(1);
  loginov::Matrix matrix2 = loginov::Matrix::separate(compositeShape);
  matrix2 = matrix1;

  equalsMatrix(matrix1, matrix2);
}

BOOST_AUTO_TEST_CASE(TestMoveConstructor)
{
  loginov::Rectangle rectangle1({-2.0, 1.5}, 2.0, 2.0);
  loginov::Circle circle({5.0, 3.5}, 3.0);
  loginov::Triangle triangle({1.0, 3.5}, {4.0, 3.5}, {2.0, 5.5});
  loginov::Rectangle rectangle2({6.5, 2.0}, 2.5, 2.0);

  loginov::CompositeShape compositeShape;
  compositeShape.add(rectangle1);
  compositeShape.add(circle);
  compositeShape.add(triangle);
  compositeShape.add(rectangle2);

  loginov::Matrix matrix1 = loginov::Matrix::separate(compositeShape);
  loginov::Matrix copyMatrix1 = matrix1;

  loginov::Matrix matrix2(std::move(matrix1));
  equalsMatrix(copyMatrix1, matrix2);
}

BOOST_AUTO_TEST_CASE(TestMoveOperator)
{
  loginov::Rectangle rectangle1({-2.0, 1.5}, 2.0, 2.0);
  loginov::Circle circle({5.0, 3.5}, 3.0);
  loginov::Triangle triangle({1.0, 3.5}, {4.0, 3.5}, {2.0, 5.5});
  loginov::Rectangle rectangle2({6.5, 2.0}, 2.5, 2.0);

  loginov::CompositeShape compositeShape;
  compositeShape.add(rectangle1);
  compositeShape.add(circle);
  compositeShape.add(triangle);
  compositeShape.add(rectangle2);

  loginov::Matrix matrix1 = loginov::Matrix::separate(compositeShape);
  loginov::Matrix copyMatrix1 = matrix1;

  loginov::Matrix matrix2;
  matrix2 = std::move(matrix1);
  equalsMatrix(copyMatrix1, matrix2);
}
BOOST_AUTO_TEST_SUITE_END()

