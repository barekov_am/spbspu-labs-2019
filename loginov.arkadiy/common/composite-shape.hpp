#ifndef A2_COMPOSITESHAPE_HPP
#define A2_COMPOSITESHAPE_HPP

#include <memory>

#include "shape.hpp"

namespace loginov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape&);
    CompositeShape(CompositeShape&&);
    ~CompositeShape() = default;
    CompositeShape& operator =(const CompositeShape&);
    CompositeShape& operator =(CompositeShape&&);
    Shape& operator [](unsigned int index) const;

    void add(Shape&);
    void remove(unsigned int index);
    size_t size() const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    point_t getCentre() const override;
    void move(double dx, double dy) override;
    void move(const point_t&) override;
    void scale(double scaleCoef) override;
    void rotate(double angle) override;


  private:
    size_t size_;
    std::unique_ptr<Shape* []> shapes_;
  };
}
#endif //A2_COMPOSITESHAPE_HPP
