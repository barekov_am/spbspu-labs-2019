#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

#include <cmath>

namespace loginov
{

  struct point_t
  {
    double x, y;
  };

  struct rectangle_t
  {
    double width, height;
    point_t pos;
  };

  loginov::point_t rotatePoint(const loginov::point_t& centre, const loginov::point_t& point, double angle);

  const double FULL_ANGLE = 360.0;
  const double RADIX_GRAD = 57.296;
}
#endif
