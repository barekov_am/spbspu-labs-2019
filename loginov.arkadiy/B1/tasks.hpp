#ifndef TASKS_HPP
#define TASKS_HPP

#include <cstddef>

void task1(const char *strDirection);
void task2(const char *file);
void task3();
void task4(const char *strRoute, size_t size);

#endif
