#include <forward_list>
#include <vector>
#include "access.hpp"
#include "print.hpp"
#include "sort.hpp"

void task1(const char *strDirection)
{
    const Route direction = getRoute(strDirection);

    std::vector<int> atVector;
    int in = 0;
    while (std::cin >> in)
    {
        atVector.push_back(in);
    }

    if (!std::cin.eof() && std::cin.fail())
    {
        throw std::ios_base::failure("Could not read the data");
    }

    if (atVector.empty())
    {
        return;
    }

    std::vector<int> bracketVector = atVector;
    std::forward_list<int> forwardList(atVector.begin(), atVector.end());

    sort<Iterator>(forwardList, direction);
    sort<At>(bracketVector, direction);
    sort<Bracket>(atVector, direction);

    writeln(forwardList);
    writeln(bracketVector);
    writeln(atVector);
}
