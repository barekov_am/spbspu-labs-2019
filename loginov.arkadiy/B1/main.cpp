#include <iostream>
#include <string>
#include "tasks.hpp"

int main(int argc, char *argv[])
{
    if (argc <= 1 || argc >= 5) {
        std::cerr << "args: wrong arg count";
        return 1;
    }

    try
    {
        char *ptr = nullptr;
        int task = std::strtol(argv[1], &ptr, 10);
        if (*ptr != '\x00')
        {
            std::cerr << "Invalid argument";
            return 1;
        }

        switch (task) {
            case 1:
                if (argc != 3) {
                    std::cerr << "Invalid number of args";
                    return 1;
                }
                task1(argv[2]);
                break;

            case 2:
                if (argc != 3) {
                    std::cerr << "Invalid number of args";
                    return 1;
                }
                task2(argv[2]);
                break;

            case 3:
                if (argc != 2) {
                    std::cerr << "Invalid number of args";
                    return 1;
                }
                task3();
                break;

            case 4:
                if (argc != 4) {
                    std::cerr << "Invalid number of args";
                    return 1;
                }
                srand(time(nullptr));
                task4(argv[2], std::stoi(argv[3]));
                break;

            default:
                std::cerr << "Invalid task";
                return 1;
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << "\n";
        return 1;
    }

    return 0;
}
