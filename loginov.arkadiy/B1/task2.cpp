#include <fstream>
#include <iostream>
#include <vector>
#include <memory>

const size_t INITIAL_SIZE = 1 << 13;

void task2(const char *file)
{
    std::ifstream inFile(file);
    if (!inFile)
    {
        throw std::ios_base::failure("Could not open the file");
    }

    size_t size = INITIAL_SIZE;
    using charArray = std::unique_ptr<char[], decltype(&free)>;
    charArray chars(static_cast<char *>(malloc(size)), &free);

    size_t i = 0;
    while (inFile)
    {
        inFile.read(&chars[i], INITIAL_SIZE);
        i += inFile.gcount();

        if (inFile.gcount() == INITIAL_SIZE)
        {
            size += INITIAL_SIZE;
            charArray tmp(static_cast<char *>(realloc(chars.get(), size)), &free);

            if (tmp)
            {
                chars.release();
                std::swap(chars, tmp);
            }
            else
            {
                throw std::runtime_error("Could not reallocate");
            }
        }
    }

    std::vector<char> vector(&chars[0], &chars[i]);

    for (std::vector<char>::iterator i = vector.begin(); i != vector.end(); ++i)
    {
        std::cout << *i;
    }
}
