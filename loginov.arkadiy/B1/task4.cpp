#include <random>
#include <vector>
#include "print.hpp"
#include "sort.hpp"
#include "access.hpp"

void fillRandom(double *array, size_t size)
{
    for (size_t i = 0; i < size; ++i) {
        array[i] = (rand() % 21 - 10) / 10.0;
    }
}

void task4(const char *strRoute, size_t size)
{
    if (size < 1)
    {
        throw std::invalid_argument("Wrong size must be greater than 0");
    }

    Route route = getRoute(strRoute);
    std::vector<double> vector(size);
    fillRandom(&vector[0], size);
    writeln(vector);
    sort<At>(vector, route);
    writeln(vector);
}
