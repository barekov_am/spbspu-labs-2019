#ifndef PRINT_HPP
#define PRINT_HPP

#include <iostream>

template <typename T>
void writeln(const T &collection)
{
    for (auto i = collection.cbegin(); i != collection.cend(); ++i)
    {
        std::cout << *i << " ";
    }

    std::cout << "\n";
}

#endif
