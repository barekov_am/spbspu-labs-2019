#include "sort.hpp"
#include <cstring>

Route getRoute(const char *route)
{
    if (std::strcmp(route, "ascending") == 0)
    {
        return Route::asc;
    }

    if (std::strcmp(route, "descending") == 0)
    {
        return Route::desc;
    }

    throw std::invalid_argument("args - Invalid route");
}
