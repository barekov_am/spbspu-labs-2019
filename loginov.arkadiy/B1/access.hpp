#ifndef ACCESS_HPP
#define ACCESS_HPP

#include <iterator>

template  <typename T>
struct Bracket
{
    static size_t begin(const T &)
    {
        return 0;
    }

    static size_t end(const T &collection)
    {
        return collection.size();
    }

    static typename T::reference get(T &collection, size_t index)
    {
        if (index < 0 || index >= collection.size())
        {
            throw std::invalid_argument("Index out of range");
        }

        return collection[index];
    }

    static size_t next(size_t index)
    {
        return index + 1;
    }
};

template  <typename T>
struct At
{
    static size_t begin(const T &)
    {
        return 0;
    }

    static size_t end(const T &collection)
    {
        return collection.size();
    }

    static typename T::reference get(T &collection, size_t index)
    {
        return collection.at(index);
    }

    static size_t next(size_t index)
    {
        return index + 1;
    }
};

template  <typename T>
struct Iterator
{
    static typename T::iterator begin(T &collection)
    {
        return collection.begin();
    }

    static typename T::iterator end(T &collection)
    {
        return collection.end();
    }

    static typename T::reference get(T &, typename T::iterator &index)
    {
        return *index;
    }

    static typename T::iterator next(typename T::iterator &index)
    {
        return std::next(index);
    }
};

#endif
