#ifndef SORT_HPP
#define SORT_HPP

#include <functional>

enum class Route
{
    asc,
    desc
};

Route getRoute(const char *route);

template <template <class T> class Access, typename T>
void sort(T &collection, Route route)
{
    typedef typename T::value_type value_type;
    std::function<bool(value_type, value_type)> compare;

    if (route == Route::asc)
    {
        compare = std::less<value_type>();
    }
    else
    {
        compare = std::greater<value_type>();
    }

    const auto begin = Access<T>::begin(collection);
    const auto end = Access<T>::end(collection);

    for (auto i = begin; i != end ; ++i)
    {
        auto tmp = i;
        for (auto j = Access<T>::next(i); j != end ; ++j)
        {
            if (compare(Access<T>::get(collection, j), Access<T>::get(collection, tmp)))
            {
                tmp = j;
            }
        }

        if (tmp != i)
        {
            std::swap(Access<T>::get(collection, i), Access<T>::get(collection, tmp));
        }
    }
}

#endif
