#include <vector>
#include <algorithm>
#include "dataStruct.hpp"

struct compare
{
    bool operator()(const DataStruct &data1, const DataStruct &data2)
    {

        if (data1.key1 < data2.key1)
        {
            return true;
        }

        if (data1.key1 > data2.key1)
        {
            return false;
        }

        if (data1.key2 < data2.key2)
        {
            return true;
        }

        if (data1.key2 > data2.key2)
        {
            return false;
        }

        return data1.str.length() < data2.str.length();
    }
};

void sort(std::vector<DataStruct> &vector)
{
    std::sort(vector.begin(), vector.end(), compare());
}
