#include "dataStruct.hpp"
#include <iostream>
#include <sstream>

DataStruct read(const std::string &string)
{
    const int MAX_ABS = 5;

    int key1 = 0;
    int key2 = 0;
    std::string str;
    std::stringstream stream(string);

    stream >> key1;
    stream.ignore(string.length(), ',');

    stream >> key2;
    stream.ignore(string.length(), ',');

    std::getline(stream, str);

    if (abs(key1) > MAX_ABS || abs(key2) > MAX_ABS || str.empty() || stream.fail())
    {
        throw std::invalid_argument("Keys must be between -5 and 5");
    }

    return {key1, key2, str};
}

void write(const DataStruct &data)
{
    std::cout << data.key1 << "," << data.key2 << "," << data.str << "\n";
}
