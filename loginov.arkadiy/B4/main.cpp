#include <algorithm>
#include <vector>
#include <iostream>
#include "dataStruct.hpp"

void sort(std::vector<DataStruct> &vector);

int main()
{
    try
    {
        std::vector<DataStruct> vector;
        std::string line;

        while (std::getline(std::cin, line))
        {
            if (std::cin.fail())
            {
                throw std::runtime_error("Could not read");
            }

            vector.push_back(read(line));
        }

        sort(vector);
        std::for_each(vector.begin(), vector.end(), write);
    }
    catch (std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}
