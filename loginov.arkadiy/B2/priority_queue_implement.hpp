#ifndef PRIORITY_QUEUE_IMPLEMENT_HPP
#define PRIORITY_QUEUE_IMPLEMENT_HPP

#include "priority_queue.hpp"

template<typename T>
void PriorityQueue<T>::add(PriorityQueue::Priority priority, const T &element)
{
    switch (priority)
    {
        case Priority::LOW:
        {
            lowList_.push_back(element);
            break;
        }

        case Priority::NORMAL:
        {
            normalList_.push_back(element);
            break;
        }

        case Priority::HIGH:
        {
            highList_.push_back(element);
            break;
        }
    }
}

template<typename T>
T PriorityQueue<T>::get()
{
    if (isEmpty())
    {
        throw std::invalid_argument("Queue is empty");
    }

    if (!highList_.empty())
    {
        const T element = highList_.front();
        highList_.pop_front();
        return element;
    }

    if (!normalList_.empty())
    {
        const T element = normalList_.front();
        normalList_.pop_front();
        return element;
    }

    const T element = lowList_.front();
    lowList_.pop_front();
    return element;
}

template<typename T>
void PriorityQueue<T>::accelerate()
{
    highList_.splice(highList_.end(), lowList_);
}

template<typename T>
bool PriorityQueue<T>::isEmpty() const
{
    return lowList_.empty() && normalList_.empty() && highList_.empty();
}

#endif
