#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "base-types.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

void printFrameRect(const loginov::Shape& shape)
{
  loginov::rectangle_t frameRect = shape.getFrameRect();
  std::cout << "Width = " << frameRect.width << "  ";
  std::cout << "Height = " << frameRect.height << "  ";
  std::cout << "Centre = [ " << frameRect.pos.x << ", " << frameRect.pos.y << " ]" << std::endl;
}

void printArea(const loginov::Shape& shape)
{
  std::cout << "Area = " << shape.getArea() << std::endl;
}

void printCentre(const loginov::Shape& shape)
{
  loginov::point_t centre = shape.getCentre();
  std::cout << "Centre = [ " << centre.x << ", " << centre.y << " ]" << std::endl;
}

void printMoveCall(loginov::Shape& shape, loginov::point_t point, double dx, double dy)
{
  std::cout << "Call move to point { " << point.x << ", " << point.y << " } -> ";
  shape.move(point);
  printCentre(shape);

  std::cout << "Call move, where dx = " << dx << "," << " dy = " << dy << " -> ";
  shape.move(dx, dy);
  printCentre(shape);
}

void printScaleCall(loginov::Shape& shape, double scaleCoef)
{
  std::cout << "Before scale: ";
  printArea(shape);
  shape.scale(scaleCoef);
  std::cout << "After scale (" << scaleCoef << "): ";
  printArea(shape);
}

void printMatrix()
{
  std::cout << "Matrix" << std::endl;
  loginov::Rectangle rectangle1({-2.0, 1.5}, 2.0, 2.0);
  loginov::Circle circle({5.0, 3.5}, 3.0);
  loginov::Triangle triangle({1.0, 3.5}, {4.0, 3.5}, {2.0, 5.5});
  loginov::Rectangle rectangle2({6.5, 2.0}, 2.5, 2.0);

  loginov::CompositeShape compositeShape;
  compositeShape.add(rectangle1);
  compositeShape.add(circle);
  compositeShape.add(triangle);
  compositeShape.add(rectangle2);

  loginov::Matrix matrix = loginov::Matrix::separate(compositeShape);
  for (unsigned i = 0; i < matrix.getRows(); i++)
  {
    std::unique_ptr<loginov::Shape* []> row = matrix[i];
    for (unsigned j = 0; j < matrix.getColumns(); j++)
    {
      if (row[j] != nullptr)
      {
        std::cout << "[" << i << ", " << j << "] FrameRect : ";
        printFrameRect(*row[j]);
      }
    }
  }
}


int main()
{
  loginov::point_t pointBegin = {4.5, 3.5};
  loginov::Rectangle rect(pointBegin, 1.0, 1.0);
  loginov::Circle circle(pointBegin, 7.0);
  loginov::Triangle triangle({{0.0, 0.0}, {10.0, 10.0}, {10.0, 0.0}});

  std::cout << "Rectangle" << std::endl;
  printFrameRect(rect);
  printScaleCall(rect, 2.0);
  printMoveCall(rect, {5.1, 5.2}, 2.0, 1.0);
  std::cout << std::endl;

  std::cout << "Circle" << std::endl;
  printFrameRect(circle);
  printScaleCall(circle, 3.5);
  printMoveCall(circle, {5.0, 5.5}, 2.1, 1.0);
  std::cout << std::endl;

  std::cout << "Triangle" << std::endl;
  printFrameRect(triangle);
  printScaleCall(triangle, 5.0);
  printMoveCall(triangle, {10.0, 5.0}, -2.0, 1.0);
  std::cout << std::endl;

  loginov::CompositeShape compositeShape;
  compositeShape.add(rect);
  compositeShape.add(circle);
  compositeShape.add(triangle);

  std::cout << "CompositeShape" << std::endl;
  std::cout << "{ Rect, Circle, Triangle } size = " << compositeShape.size() << std::endl;
  for (unsigned int i = 0; i < compositeShape.size(); ++i)
  {
    printCentre(compositeShape[i]);
  }
  compositeShape.remove(2);
  std::cout << "{ Rect, Circle } size = " << compositeShape.size() << std::endl;
  printFrameRect(compositeShape);
  printScaleCall(compositeShape, 2.0);
  printMoveCall(compositeShape, {5, 5}, 2, 1);
  std::cout << std::endl;

  printMatrix();

  return 0;
}
