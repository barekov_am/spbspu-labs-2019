#include <iostream>
#include <stdexcept>
#include "task-h.hpp"

int main (int argc, char** argv)
{
  try{
    if (argc < 2)
    {
      throw std::invalid_argument("Invalid number of input parameters");
    }
    switch (*argv[1])
    {
    case '1':
      if (argc == 3){
          if (argv[2] == nullptr)
          {
              std:: cerr << "Invalid";
              return 1;
          }
        task_1(argv[2]);
      } else {
        throw std::invalid_argument("Invalid number of input parameters");
      }
      break;

    case '2':
      if (argc == 3){
          if (argv[2] == nullptr)
          {
              std:: cerr << "Invalid";
              return 1;
          }
        task_2(argv[2]);
      } else {
        throw std::invalid_argument("Invalid number of input parameters");
      }
      break;

    case '3':
      if (argc == 2){
        task_3();
      } else {
        throw std::invalid_argument("Invalid number of input parameters");
      }
      break;

    case '4':
      if (argc == 4){
          if (argv[2] == nullptr)
          {
              std:: cerr << "Invalid";
              return 1;
          }
        task_4(argv[2],argv[3]);
      } else {
        throw std::invalid_argument("Invalid number of input parameters");
      }
      break;

    default:
      throw std::invalid_argument("Invalid first input parameter");
      break;
    }
  }
  catch (std::exception & e)
  {
    std::cerr << e.what() << "\n";
    return 1;
  }
  return 0;
}
