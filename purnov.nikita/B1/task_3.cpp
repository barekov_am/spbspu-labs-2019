#include <iostream>
#include <stdexcept>
#include <vector>
#include "addition.hpp"

void task_3()
{
  std::vector<int> vector;

  int n = 1;
  while (std::cin >> n)
  {
    vector.push_back(n);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Invalid data in standard input stream");
  }

  if (vector.empty())
  {
    return;
  }

  if (vector.back()!=0){
    throw std::invalid_argument("Zero is expected by the end of the input");
  } else {
    vector.pop_back();
  }

  std::vector<int>::iterator iter = vector.begin();

  if (vector.back() == 1){
    while (iter != vector.end())
    {
      if (*iter % 2 == 0){
        iter = vector.erase(iter);
      } else {
        iter++;
      }
    }
  } else if (vector.back() == 2){
    while (iter != vector.end())
    {
      if (*iter % 3 == 0){
        iter = vector.insert(++iter,3,1);
        iter +=2;
      }
      iter++;
    }
  }

  addition::printContainer(vector);
}
