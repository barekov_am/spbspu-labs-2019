#include <iostream>
#include <stdexcept>
#include <vector>
#include <random>
#include <ctime>
#include <cmath>
#include <cstring>
#include "addition.hpp"

void fillRandom(double * array, int size)
{
  std::mt19937 Rng(time(0));
  std::uniform_real_distribution<double> distrib (-1.0, 1.0);
  for (int i = 0; i < size; ++i)
  {
    array[i] = distrib(Rng);
  }
}

void task_4(const char* sort_direction, const char* array_size)
{
  int size = atoi(array_size);
  if (size == 0)
  {
    throw std::invalid_argument("Invalid array size");
  }

  std::vector<double> vector (size);
  fillRandom(&vector[0],size);

  bool (*pt_comp) (double&,double&) = nullptr;
  if (std::strcmp(sort_direction,"ascending")==0){
    pt_comp = addition::compare_for_more;
  } else if (std::strcmp(sort_direction,"descending")==0){
    pt_comp = addition::compare_for_less;
  } else {
    throw std::invalid_argument("Invalid sorting direction");
  }

  addition::printContainer<std::vector<double> >(vector);
  addition::sort<addition::by_brackets,std::vector<double> >(vector,pt_comp);
  addition::printContainer<std::vector<double> >(vector);
}
