#include <iostream>
#include <cstring>
#include "InputTask.hpp"
#include "OutputTask.hpp"

bool checkNumber(char *number)
{
  while (*number != 0)
  {
    if (*number < '0' || *number > '9') return false;
    number++;
  }
  return true;
}

int main(int argc, char ** argv)
{
  unsigned long long width = 40;
  constexpr unsigned long minWidth = 25;
  if (argc == 3 && (strcmp(argv[1],"--line-width") == 0) && checkNumber(argv[2]))
  {
    width = std::stoi(argv[2]);
    if (width < minWidth) return 1;
  }
  else if (argc != 1)
  {
    std::cerr << "invalid arguments";
    return 1;
  }
    
  try
  {
    InputParser parser(std::cin);
    parser.parseInput();

    std::list<element_t> vector(parser.begin(), parser.end());
    if (!trueOrder(vector))
    {
      std::cerr << "Invalid input.";
      return 1;
    }
    formatText(std::cout, width, vector);
  }

  catch (const std::invalid_argument &e)
  {
    std::cerr << e.what();
    return 1;
  }

  return 0;
}
