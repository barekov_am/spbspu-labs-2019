#include <iostream>
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "circle.hpp"
#include "triangle.hpp"

void showParametrsShape(purnov::CompositeShape &compositeShape_)
{
  std::cout << "Area of Shape= " << compositeShape_.getArea() << "\n";
  std::cout << "Frame: " << "\n";
  purnov::rectangle_t frame = compositeShape_.getFrameRect();
  std::cout << "Height= " << frame.height << "\n";
  std::cout << "Width= " << frame.width << "\n";
  std::cout << "Point of Center in x-axis= " << frame.pos.x << "\n";
  std::cout << "Point of Center in y-axis= " << frame.pos.y << "\n";
  compositeShape_.showPoint();
  compositeShape_.move(4, 5);
  compositeShape_.showPoint();
  compositeShape_.move({4, 5});
  compositeShape_.showPoint();
  compositeShape_.scale(2);
  std::cout << "Area after scale = " << compositeShape_.getArea() << "\n";
  compositeShape_.remove(0);
  std::cout << "Area after delete one Shape= " << compositeShape_.getArea() << "\n\n";
}

int main()
{
  purnov::CompositeShape compositeShape5;
  purnov::Rectangle rectangle({4, 5}, 4, 4);
  compositeShape5.add(std::make_shared<purnov::Rectangle>(rectangle));
  std::cout << "Area of Composite Shape= " << compositeShape5.getArea() << "\n";
  purnov::Circle circle({6, 9}, 3);
  purnov::CompositeShape compositeShape(std::make_shared<purnov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<purnov::Circle>(circle));
  purnov::Triangle triangle({4, 10},{1, 1},{10, 1});
  std::cout << "Area of Triangle= " << triangle.getArea() << "\n";
  compositeShape.add(std::make_shared<purnov::Triangle>(triangle));
  std::cout << "Area of triangle in Composite= " << compositeShape[2]->getArea() << "\n";
  showParametrsShape(compositeShape);
  purnov::CompositeShape compositeShape2 = compositeShape;
  showParametrsShape(compositeShape2);
  purnov::CompositeShape compositeShape3(compositeShape);
  if (compositeShape3.getArea() != compositeShape2.getArea())
  {
    std::cout << "This is different objects!\n";
  }
  return 0;
}
