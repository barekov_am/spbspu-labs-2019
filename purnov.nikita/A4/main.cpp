#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "partition.hpp"
#include "matrix.hpp"


int main()
{
  purnov::Rectangle rectangle({4, 5}, 6, 7);
  purnov::Circle circle({4, 5}, 6);
  purnov::Triangle triangle({4, 6}, {6, 7}, {1, 10});
  std::cout << rectangle.getArea() << std::endl;
  std::cout << circle.getArea() << std::endl;
  std::cout << triangle.getArea() << std::endl;
  purnov::CompositeShape compositeShape(std::make_shared<purnov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<purnov::Circle>(circle));
  compositeShape.add(std::make_shared<purnov::Triangle>(triangle));
  std::cout << compositeShape.getArea() << std::endl;
  std::unique_ptr<purnov::Shape::shapePtr []> shape = std::make_unique<purnov::Shape::shapePtr []>(5);
  purnov::Rectangle rectangle1({2, 4.5}, 1, 2);
  purnov::Rectangle rectangle2({3.5, 2}, 2, 5);
  purnov::Circle circle1({6, 4}, 2);
  purnov::Triangle triangle1({3, 2}, {2, 0}, {4, 0});
  purnov::Triangle triangle2({7, 4}, {9, 3}, {9, 5});
  shape[0] = std::make_shared<purnov::Rectangle>(rectangle1);
  shape[1] = std::make_shared<purnov::Rectangle>(rectangle2);
  shape[2] = std::make_shared<purnov::Circle>(circle1);
  shape[3] = std::make_shared<purnov::Triangle>(triangle1);
  shape[4] = std::make_shared<purnov::Triangle>(triangle2);
  purnov::Matrix matrix = purnov::division(shape, 5);
  matrix.showAll();
  return 0;
}
