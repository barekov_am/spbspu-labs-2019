#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(partitionMetodsTests)

BOOST_AUTO_TEST_CASE(partitionTest)
{
  purnov::Rectangle rectangle({2, 4.5}, 1, 2);
  purnov::Rectangle rectangle1({3.5, 2}, 2, 5);
  purnov::Circle circle({6, 4}, 2);

  purnov::CompositeShape compositeShape(std::make_shared<purnov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<purnov::Rectangle>(rectangle1));
  compositeShape.add(std::make_shared<purnov::Circle>(circle));

  purnov::Matrix matrix = purnov::division(compositeShape);

  BOOST_CHECK_EQUAL(matrix.getLines(), 2);
  BOOST_CHECK_EQUAL(matrix.getColumns(), 2);
}

BOOST_AUTO_TEST_CASE(intersectionTest)
{
  purnov::Rectangle rectangle({2, 4.5}, 1, 2);
  purnov::Rectangle rectangle1({3.5, 2}, 2, 5);
  purnov::Circle circle({6, 4}, 2);

  BOOST_CHECK(!purnov::checkIntersect(rectangle.getFrameRect(), rectangle1.getFrameRect()));
  BOOST_CHECK(!purnov::checkIntersect(rectangle.getFrameRect(), circle.getFrameRect()));
  BOOST_CHECK(purnov::checkIntersect(rectangle1.getFrameRect(), circle.getFrameRect()));
}

BOOST_AUTO_TEST_SUITE_END()
