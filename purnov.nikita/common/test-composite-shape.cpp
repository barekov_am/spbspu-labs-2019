#include <boost/test/unit_test.hpp>
#include <stdexcept>
#include "triangle.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

const double INACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testOfCompositeShape)

BOOST_AUTO_TEST_CASE(widthAndHeightAfterMoving)
{
  purnov::Circle circle({3, 4}, 5);
  purnov::Rectangle rectangle({5, 6}, 2, 6);
  purnov::Triangle triangle({4, 6}, {5, 3}, {4, 9});
  purnov::CompositeShape compositeShape(std::make_shared<purnov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<purnov::Circle>(circle));
  compositeShape.add(std::make_shared<purnov::Triangle>(triangle));
  purnov::rectangle_t frameBeforeMoving = compositeShape.getFrameRect();
  double area = compositeShape.getArea();
  compositeShape.move({4, 5});

  BOOST_CHECK_CLOSE(frameBeforeMoving.height, compositeShape.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, compositeShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(area, compositeShape.getArea(), INACCURACY);

  compositeShape.move(4, 5);

  BOOST_CHECK_CLOSE(frameBeforeMoving.height, compositeShape.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(frameBeforeMoving.width, compositeShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(area, compositeShape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkCorectAddAndRemove)
{
  purnov::Circle circle({3, 4}, 5);
  purnov::Rectangle rectangle({4, 5}, 6, 8);
  purnov::CompositeShape compositeShape(std::make_shared<purnov::Circle>(circle));
  double areaBeforeAdd = compositeShape.getArea();

  purnov::Shape::shapePtr shape = std::make_shared<purnov::Rectangle>(rectangle);

  compositeShape.add(shape);
  BOOST_CHECK_CLOSE(areaBeforeAdd + rectangle.getArea(), compositeShape.getArea(), INACCURACY);

  compositeShape.remove(shape);
  BOOST_CHECK_CLOSE(areaBeforeAdd, compositeShape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(outOfRangeException)
{
  purnov::Rectangle rectangle({5, 6}, 7, 7);
  purnov::CompositeShape compositeShape(std::make_shared<purnov::Rectangle>(rectangle));
  BOOST_CHECK_THROW(compositeShape[100], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(areaAfterScaling)
{
  purnov::Circle circle({3, 4}, 5);
  purnov::Rectangle rectangle({5, 6}, 2, 6);
  purnov::Triangle triangle({4, 6}, {5, 3}, {4, 9});
  purnov::CompositeShape compositeShape(std::make_shared<purnov::Circle>(circle));
  compositeShape.add(std::make_shared<purnov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<purnov::Triangle>(triangle));

  double area = compositeShape.getArea();
  compositeShape.scale(2);

  BOOST_CHECK_CLOSE(area * 4, compositeShape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(checkCorrectGetFrameRect)
{
  purnov::Circle circle({6, 5}, 3);
  purnov::Rectangle rectangle({6, 5}, 6, 6);
  purnov::CompositeShape compositeShape(std::make_shared<purnov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<purnov::Circle>(circle));
  purnov::rectangle_t frame = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frame.height, compositeShape.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(frame.width, compositeShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(frame.pos.x, compositeShape.getFrameRect().pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(frame.pos.y, compositeShape.getFrameRect().pos.y, INACCURACY);
}

BOOST_AUTO_TEST_CASE(exceptionThrow)
{
  BOOST_CHECK_THROW(purnov::CompositeShape(nullptr), std::invalid_argument);

  purnov::Circle circle({2, 2}, 9);
  purnov::Rectangle rectangle({4, 2}, 3, 5);
  purnov::Triangle triangle({4, 3}, {5, 6}, {3, 8});
  purnov::CompositeShape compositeShape(std::make_shared<purnov::Circle>(circle));
  compositeShape.add(std::make_shared<purnov::Rectangle>(rectangle));
  compositeShape.add(std::make_shared<purnov::Triangle>(triangle));
  purnov::CompositeShape compositeShape1;

  BOOST_CHECK_THROW(compositeShape1.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(compositeShape.scale(-1), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.remove(10), std::out_of_range);
  BOOST_CHECK_THROW(compositeShape.remove(nullptr), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(copyAndMove)
{
  purnov::Rectangle rect1({4, 1}, 8, 4);
  purnov::CompositeShape compositeShape1(std::make_shared<purnov::Rectangle>(rect1));
  purnov::CompositeShape compositeShape4;

  BOOST_CHECK_NO_THROW(purnov::CompositeShape compositeShape2(compositeShape1));

  purnov::CompositeShape compositeShape2;

  BOOST_CHECK_NO_THROW(compositeShape2 = compositeShape1);
  BOOST_CHECK_NO_THROW(compositeShape2 = purnov::CompositeShape(std::make_shared<purnov::Rectangle>(rect1)));

  BOOST_CHECK_NO_THROW(purnov::CompositeShape compositeShape3(std::move(compositeShape2)));
  BOOST_CHECK_NO_THROW(compositeShape4 = std::move(compositeShape1));
}

BOOST_AUTO_TEST_SUITE_END()
