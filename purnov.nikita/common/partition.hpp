#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "composite-shape.hpp"
#include "matrix.hpp"
#include <memory>

namespace purnov
{
  bool checkIntersect(purnov::rectangle_t shape1, purnov::rectangle_t shape2);
  Matrix division(std::unique_ptr<purnov::Shape::shapePtr []> &shapes, size_t size);
  Matrix division(CompositeShape &compositeShape);
}

#endif
