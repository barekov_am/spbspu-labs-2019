#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"

const double INACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(testForCircle)

BOOST_AUTO_TEST_CASE(radiusPermanenceAfterMovingToPoint)
{
  purnov::Circle circle({3, 3}, 5);
  circle.move({0, 0});
  BOOST_CHECK_CLOSE(5, circle.getFrameRect().width / 2, INACCURACY);
}

BOOST_AUTO_TEST_CASE(radiusPermanenceAfterMoving)
{
  purnov::Circle circle({3, 3}, 5);
  circle.move(2, 2);
  BOOST_CHECK_CLOSE(5, circle.getFrameRect().width / 2, INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterMovingToPoint)
{
  purnov::Circle circle({3, 5}, 7);
  double area = circle.getArea();
  circle.move({2, 2});
  BOOST_CHECK_CLOSE(area, circle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaAfterMoving)
{
  purnov::Circle circle({3, 5}, 7);
  double area = circle.getArea();
  circle.move(4, 2);
  BOOST_CHECK_CLOSE(area, circle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(areaCheckAfterScalingOfCircle)
{
  purnov::Circle circle({5, 4}, 3);
  const double areaBeforeScale = circle.getArea();
  const double testScale = 5;
  circle.scale(testScale);
  BOOST_CHECK_CLOSE(areaBeforeScale * testScale * testScale, circle.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(invalidArgumentsInCircle)
{
  BOOST_CHECK_THROW(purnov::Circle circle({6, 5}, -9), std::invalid_argument);
  purnov::Circle circle({4, 3}, 4);
  BOOST_CHECK_THROW(circle.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
