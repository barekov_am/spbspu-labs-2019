#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <algorithm>

purnov::Triangle::Triangle(const point_t &point0, const point_t &point1, const point_t &point2) :
  point0_(point0),
  point1_(point1),
  point2_(point2),
  center_({(point0_.x + point1_.x + point2_.x) / 3, (point0_.y + point1_.y + point2_.y) / 3})
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("Wrong point");
  }
}

static double getDistance(const purnov::point_t &point1, const purnov::point_t &point2)
{
  const purnov::point_t vector = {(point2.x - point1.x), (point2.y - point1.y)};
  return sqrt(vector.x * vector.x + vector.y * vector.y);
}

double purnov::Triangle::getArea() const
{
  const double lengthVector1 = getDistance(point0_, point1_);
  const double lengthVector2 = getDistance(point0_, point2_);
  const double lengthVector3 = getDistance(point1_, point2_);
  const double halfPer = (lengthVector1 + lengthVector2 + lengthVector3) / 2;
  return sqrt(halfPer * (halfPer - lengthVector1) * (halfPer - lengthVector2) * (halfPer - lengthVector3));
}

purnov::rectangle_t purnov::Triangle::getFrameRect() const
{
  const double maxX = std::max({point0_.x, point1_.x, point2_.x});
  const double maxY = std::max({point0_.y, point1_.y, point2_.y});
  const double minX = std::min({point0_.x, point1_.x, point2_.x});
  const double minY = std::min({point0_.y, point1_.y, point2_.y});
  const double height = maxY - minY;
  const double width = maxX - minX;
  const point_t center = {minX + width / 2, minY + height / 2};
  return {center , height, width};
}

void purnov::Triangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;

  point0_.x += dx;
  point0_.y += dy;

  point1_.x += dx;
  point1_.y += dy;

  point2_.x += dx;
  point2_.y += dy;
}

void purnov::Triangle::move(const point_t &center)
{
  move((center.x - center_.x), (center.y - center_.y));
}

void purnov::Triangle::showPoint() const
{
  std::cout << "the point on the x-axis= " << center_.x << "\n";
  std::cout << "the point on the y-axis= " << center_.y << "\n";
}

double scaleOneCoordinate(double point, double center, const double factor)
{
  return (center + point * factor - factor * center);
}

void purnov::Triangle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Factor must be positive!");
  }
  point0_.x = scaleOneCoordinate(point0_.x, center_.x, factor);
  point0_.y = scaleOneCoordinate(point0_.y, center_.y, factor);
  point1_.x = scaleOneCoordinate(point1_.x, center_.x, factor);
  point1_.y = scaleOneCoordinate(point1_.y, center_.y, factor);
  point2_.x = scaleOneCoordinate(point2_.x, center_.x, factor);
  point2_.y = scaleOneCoordinate(point2_.y, center_.y, factor);
}

purnov::point_t rotateOneCoordinate(purnov::point_t point, double angle)
{
  double x = point.x * cos(M_PI * angle / 180) - point.y * sin(M_PI * angle / 180);
  double y = point.y * cos(M_PI * angle / 180) + point.x * sin(M_PI * angle / 180);
  return {x, y};
}

void purnov::Triangle::rotate(double angle)
{
  if (angle < 0 )
  {
    angle = (360 + fmod(angle, 360));
  }
  point0_ = rotateOneCoordinate(point0_, angle);
  point1_ = rotateOneCoordinate(point1_, angle);
  point2_ = rotateOneCoordinate(point2_, angle);
}
