#ifndef PARSERCOMMANDS_HPP
#define PARSERCOMMANDS_HPP
#include <functional>
#include "queue-impl.hpp"

using queueStr = QueueWithPriority<std::string>;
using executeCommand = std::function<void(queueStr&, std::stringstream&)>;
executeCommand parseCommand(std::string&);

#endif // PARSERCOMMANDS_HPP
