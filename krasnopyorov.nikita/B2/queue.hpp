#ifndef QUEUE_HPP
#define QUEUE_HPP
#include <list>
#include <stdexcept>

typedef enum
{
  LOW,
  NORMAL,
  HIGH
} ElementPriority;

template<typename T>
class QueueWithPriority
{
public:
  bool isEmpty();
  void PutElementToQueue(const T& element, ElementPriority priority);
  T GetElementFromQueue();
  void Accelerate();

private:
  std::list<T> low_;
  std::list<T> normal_;
  std::list<T> high_;

};


#endif // QUEUE_HPP
