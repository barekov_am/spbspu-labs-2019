#include <iostream>
#include <list>
#include <exception>

void printList(std::list<int>& listInt)
{
  std::list<int>::iterator headIt = listInt.begin();
  std::list<int>::iterator tailIt = --listInt.end();

  while (std::distance(headIt, tailIt) > 1)
  {
    std::cout << *headIt << " " << *tailIt << " ";
    ++headIt;
    --tailIt;
  }

  if (std::distance(headIt, tailIt) == 0)
  {
    std::cout << *headIt << "\n";
  }
  else
  {
    std::cout << *headIt << " " << *tailIt << "\n";
  }
}

void task2()
{
  const size_t capacity = 20;
  size_t count = 0;
  const int max = 20;
  const int min = 1;
  int num = 0;
  std::list<int> listInt;
  while ((std::cin >> num) && (count < capacity))
  {
    if ((num > max) || (num < min))
    {
      throw std::invalid_argument("Number must be more or equal 1 and less or equal 20\n");
    }
    listInt.push_back(num);
    ++count;
  }

  if ((!std::cin.eof()) && (std::cin.fail()))
  {
    throw std::invalid_argument("Error while reading data from input thread\n");
  }

  if (!std::cin.eof())
  {
    throw std::invalid_argument("Count of numbers must be less or equal then 20\n");
  }

  if (listInt.empty())
  {
    return;
  }

  printList(listInt);
}
