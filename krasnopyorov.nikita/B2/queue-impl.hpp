#ifndef QUEUEIMPL_HPP
#define QUEUEIMPL_HPP
#include "queue.hpp"

template<typename T>
void QueueWithPriority<T>::PutElementToQueue(const T& element, ElementPriority priority)
{
  switch(priority)
  {
  case ElementPriority::LOW:
    low_.push_back(element);
    break;
  case ElementPriority::NORMAL:
    normal_.push_back(element);
    break;
  case ElementPriority::HIGH:
    high_.push_back(element);
    break;
  }
}

template<typename T>
T QueueWithPriority<T>::GetElementFromQueue()
{
  if (!high_.empty())
  {
    T tmp = *high_.begin();
    high_.pop_front();
    return tmp;
  }

  if (!normal_.empty())
  {
    T tmp = *normal_.begin();
    normal_.pop_front();
    return tmp;
  }

  if (!low_.empty())
  {
    T tmp = *low_.begin();
    low_.pop_front();
    return tmp;
  }

  throw std::runtime_error("Can't pop from empty queue!\n");
}

template<typename T>
bool QueueWithPriority<T>::isEmpty()
{
  return low_.empty() && normal_.empty() && high_.empty();
}

template<typename T>
void QueueWithPriority<T>::Accelerate()
{
  high_.splice(high_.end(), low_);
}
#endif // QUEUEIMPL_HPP
