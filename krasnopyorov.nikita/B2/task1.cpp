#include <sstream>
#include <iostream>
#include "parser-commands.hpp"

void task1()
{
  std::string str;
  queueStr queue;
  while (std::getline(std::cin, str))
  {
    std::stringstream strStream(str);
    std::string command;
    strStream >> command;
    parseCommand(command)(queue, strStream);

  }

}
