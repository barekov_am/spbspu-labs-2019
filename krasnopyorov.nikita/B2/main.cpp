#include <iostream>

void task1();
void task2();

int main(int argc, char *argv[])
{
  if (argc < 2)
  {
    std::cerr << "Incorrect count of argumnets\n";
    return 1;
  }
  try
  {
    int taskNumber = std::stoi(argv[1]);
    switch (taskNumber)
    {
    case 1:
      task1();
      break;

    case 2:
      task2();
      break;
    default:
      std::cerr << "Unknown number of task\n";
      return 1;
    }

  }
  catch (std::exception& exception)
  {
    exception.what();
    return 1;
  }

  return 0;
}
