#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <list>
#include <string>


class PhoneBook
{
public:
  using data = std::pair<std::string, std::string>;
  using iterator = std::list<data>::iterator;
  using constIterator = std::list<data>::const_iterator;

  iterator begin() noexcept;
  iterator end() noexcept;

  constIterator cbegin() const noexcept;
  constIterator cend() const noexcept;

  void insertAfter(iterator iter, data& newRecord);
  void insertBefore(iterator iter, data& newRecord);

  iterator replace(iterator iter, data& newRecord);
  void pushBack(data& newRecord);

  iterator remove(iterator iter);
  void show(iterator iter);
  iterator move(iterator iter, int step);




private:

  std::list<data> records_;
};

#endif // PHONEBOOK_HPP
