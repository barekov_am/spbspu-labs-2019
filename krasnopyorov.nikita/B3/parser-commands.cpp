#include "parser-commands.hpp"
#include <string>
#include <iostream>
#include <utility>
#include <cstdlib>


const std::string invCommand = "<INVALID COMMAND>\n";

std::string extractName(std::string& str)
{
  size_t current = 1;
  const size_t length = str.length();
  while (current != length)
  {
    if ((str[current] == '\"') && (str[current - 1] != '\\'))
    {
      break;
    }
    ++current;
  }
  if (current == length)
  {
    throw std::runtime_error("Name must end with \"\n");
  }

  size_t end = current + 1;
  while ((isspace(str[end])) && (end != length))
  {
    ++end;
  }
  if (end != length)
  {
    throw std::runtime_error("Command contains unnecessary symbol\n");
  }

  std::string name = str.substr(1, current - 1);
  std::string::iterator iter = ++name.begin();
  while (iter != name.end())
  {
    if (iter != name.begin())
    {
      if ((*iter == '\"') && (*(std::prev(iter)) == '\\'))
      {
        iter = name.erase(--iter);
      }
      else
      {
        ++iter;
      }
    }
    else
    {
      ++iter;
    }
  }

  return name;
}

bool varifyMarkName(std::string& markName)
{
  for (size_t i = 0; i != markName.length(); ++i)
  {
    if ((!isalpha(markName[i])) && (!isdigit(markName[i])) && (markName[i] != '-'))
    {
      return false;
    }
  }

  return true;
}

bool varifyNumber(std::string& number)
{
  for (size_t i = 0; i != number.length();  ++i)
  {
    if (!isdigit(number[i]))
    {
      return false;
    }
  }
  return true;
}

void add(PhoneBookInterface& bookmarks, std::stringstream& strStream)
{
  std::string number;
  strStream >> number;
  if (number.empty())
  {
    std::cout << invCommand;
    return;
  }
  if (!varifyNumber(number))
  {
    std::cout << invCommand;
    return;
  }

  std::string str;
  strStream >> std::ws;
  std::getline(strStream, str);
  if (str[0] != '\"')
  {
    std::cout << invCommand;
    return;
  }
  std::string name = extractName(str);
  PhoneBook::data pair = std::make_pair(number, name);
  bookmarks.add(pair);
}

void store(PhoneBookInterface& bookmarks, std::stringstream& strStream)
{
  std::string oldMarkName;
  strStream >> oldMarkName;
  if (!varifyMarkName(oldMarkName))
  {
    throw std::runtime_error("Invalid mark name symbol\n");
  }

  std::string newMarkName;
  strStream >> newMarkName;
  if (!varifyMarkName(newMarkName))
  {
    throw std::runtime_error("Invalid mark name symbol\n");
  }

  strStream >> std::ws;
  if (!strStream.eof())
  {
    throw std::runtime_error("Command contains unnecessary symbol\n");
  }

  bookmarks.store(oldMarkName, newMarkName);
}


void insert(PhoneBookInterface& bookmarks, std::stringstream& strStream)
{
  std::string position;
  strStream >> position;
  std::function<void(PhoneBookInterface&, std::string&, PhoneBook::data&)> insertInPos;
  if (position.compare("after") == 0)
  {
    insertInPos = &PhoneBookInterface::insertAfter;
  }
  else if (position.compare("before") == 0)
  {
    insertInPos = &PhoneBookInterface::insertBefore;
  }
  else
  {
    std::cout << invCommand;
    return;
  }

  std::string markName;
  strStream >> markName;
  if (markName.empty())
  {
    throw std::runtime_error("Mark name is empty\n");
  }
  if (!varifyMarkName(markName))
  {
    throw std::runtime_error("Invalid mark name symbol\n");
  }

  std::string number;
  strStream >> number;
  if (number.empty())
  {
    std::cout << invCommand;
    return;
  }
  if (!varifyNumber(number))
  {
    std::cout << invCommand;
    return;;
  }

  std::string str;
  strStream >> std::ws;
  std::getline(strStream, str);
  if (str[0] != '\"')
  {
    std::cout << invCommand;
    return;
  }
  std::string name = extractName(str);
  PhoneBook::data pair = std::make_pair(number, name);
  insertInPos(bookmarks, markName, pair);
}

void erase(PhoneBookInterface& bookmarks, std::stringstream& strStream)
{
  std::string markName;
  strStream >> markName;
  if (markName.empty())
  {
    throw std::runtime_error("Mark name is empty\n");
  }
  if (!varifyMarkName(markName))
  {
    throw std::runtime_error("Invalid mark name symbol\n");
  }
  if (!strStream.eof())
  {
    throw std::runtime_error("Command contains unnecessary symbol\n");
  }

  bookmarks.remove(markName);
}

void show(PhoneBookInterface& bookmarks, std::stringstream& strStream)
{
  std::string markName;
  strStream >> markName;
  if (markName.empty())
  {
    throw std::runtime_error("Mark name is empty\n");
  }
  if (!varifyMarkName(markName))
  {
    throw std::runtime_error("Invalid mark name symbol\n");
  }
  if (!strStream.eof())
  {
    throw std::runtime_error("Command contains unnecessary symbol\n");
  }
  bookmarks.show(markName);
}

void move(PhoneBookInterface& bookmarks, std::stringstream& strStream)
{
  std::string markName;
  strStream >> markName;
  if (markName.empty())
  {
    throw std::runtime_error("Mark name is empty\n");
  }
  if (!varifyMarkName(markName))
  {
    throw std::runtime_error("Invalid mark name symbol\n");
  }

  std::string step;
  strStream >> step;

  if (!strStream.eof())
  {
    throw std::runtime_error("Command contains unnecessary symbol\n");
  }

  if ((step.compare("first") == 0) || (step.compare("last") == 0))
  {
    bookmarks.move(markName, step);
    return;
  }
  char* pointer = nullptr;
  int intStep = std::strtol(step.c_str(), &pointer, 10);
  if (*pointer != 0x00)
  {
    std::cout << "<INVALID STEP>\n";
    return;
  }
  bookmarks.move(markName, intStep);
}

executeCommand parseCommand(std::string& command)
{
  if (command.compare("add") == 0)
   {
     return add;
   }
   else if (command.compare("store") == 0)
   {
     return store;
   }
   else if (command.compare("insert") == 0)
   {
     return insert;
   }
   else if (command.compare("delete") == 0)
   {
     return erase;
   }
   else if (command.compare("show") == 0)
   {
     return show;
   }
   else if (command.compare("move") == 0)
   {
     return move;
   }
   else
   {
     return [](PhoneBookInterface&, std::stringstream&)
     {
       std::cout << invCommand;
     };
}
}
