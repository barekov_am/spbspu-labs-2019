#ifndef PHONEBOOKINTERFACE_HPP
#define PHONEBOOKINTERFACE_HPP
#include <map>
#include "phone-book.hpp"

class PhoneBookInterface
{

public:
  using bookmarks = std::map<std::string, PhoneBook::iterator>;
  using iterator = bookmarks::iterator;

  PhoneBookInterface();
  void add(PhoneBook::data& newData);
  void store(std::string& oldMarkName, std::string& newMarkName);
  void insertAfter(std::string& markName, PhoneBook::data& newData);
  void insertBefore(std::string& markName, PhoneBook::data& newData);
  void remove(std::string& markName);
  void show(std::string& markName);
  void move(std::string& markName, int step);
  void move(std::string& markName, std::string& firstOrLast);
  void moveNext(std::string& markName);
  void movePrev(std::string& markName);
private:
  PhoneBook records_;
  bookmarks marks_;
};

#endif // PHONEBOOKINTERFACE_HPP
