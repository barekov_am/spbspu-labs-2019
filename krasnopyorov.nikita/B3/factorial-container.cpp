#include "factorial-container.hpp"

FactorialContainer::Iterator::Iterator():
  order_(1),
  value_(1)
{

}

FactorialContainer::Iterator::Iterator(value_type maxOrder, value_type valueEndIter):
  order_(maxOrder),
  value_(valueEndIter)
{

}

bool FactorialContainer::Iterator::operator==(const FactorialContainer::Iterator& rhs) const
{
  return value_ == rhs.value_;
}

bool FactorialContainer::Iterator::operator!=(const FactorialContainer::Iterator& rhs) const
{
  return value_ != rhs.value_;
}

const FactorialContainer::Iterator::value_type& FactorialContainer::Iterator::operator *() const
{
  if (order_ == maxOrder_)
  {
    std::runtime_error("Try to get 11th el\n");
  }
  return value_;
}

FactorialContainer::Iterator& FactorialContainer::Iterator::operator++()
{
  if (order_ == maxOrder_)
  {
    throw std::overflow_error("Try to overcome top border\n");
  }

  ++order_;
  value_ *= order_;
  return *this;
}

FactorialContainer::Iterator FactorialContainer::Iterator::operator++(int)
{
  const Iterator tmp = *this;
  this->operator++();

  return tmp;
}

FactorialContainer::Iterator& FactorialContainer::Iterator::operator--()
{
  if (order_ == 1)
  {
    throw std::overflow_error("Container does not stores el less then 1\n");
  }

  value_ /= order_;
  --order_;
  return *this;
}

FactorialContainer::Iterator FactorialContainer::Iterator::operator--(int)
{
  const Iterator tmp = *this;
  this->operator--();

  return tmp;
}


FactorialContainer::Iterator FactorialContainer::begin() const
{
  return Iterator();
}

FactorialContainer::Iterator FactorialContainer::end() const
{
  return Iterator(maxOrder_, valueEndIter_);
}

FactorialContainer::ReverseIterator FactorialContainer::rbegin() const
{
  return ReverseIterator((Iterator(maxOrder_, valueEndIter_)));
}

FactorialContainer::ReverseIterator FactorialContainer::rend() const
{
  return ReverseIterator((Iterator()));
}
