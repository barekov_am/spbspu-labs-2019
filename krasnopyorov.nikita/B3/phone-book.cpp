#include "phone-book.hpp"
#include <iostream>

PhoneBook::iterator PhoneBook::begin() noexcept
{
  return records_.begin();
}

PhoneBook::iterator PhoneBook::end() noexcept
{
  return records_.end();
}

PhoneBook::constIterator PhoneBook::cbegin() const noexcept
{
  return records_.cbegin();
}

PhoneBook::constIterator PhoneBook::cend() const noexcept
{
  return records_.cend();
}

void PhoneBook::insertBefore(iterator iter, data& newRecord)
{
  records_.insert(iter, newRecord);
}

void PhoneBook::insertAfter(iterator iter, data& newRecord)
{
  records_.insert(++iter, newRecord);
}

PhoneBook::iterator PhoneBook::replace(iterator iter, data& newRecord)
{
  *iter = newRecord;
  return iter;
}

void PhoneBook::pushBack(data& newRecord)
{
  records_.push_back(newRecord);
}

PhoneBook::iterator PhoneBook::remove(iterator iter)
{
  return records_.erase(iter);
}

void PhoneBook::show(PhoneBook::iterator iter)
{
  std::cout << iter->first << " " << iter->second << "\n";
}

PhoneBook::iterator PhoneBook::move(PhoneBook::iterator iter, int step)
{
  if (step >= 0)
  {
    while ((iter != records_.end()) && (step != 0))
    {
      ++iter;
      --step;
    }
  }
  else
  {
    while ((iter != records_.begin()) && (step != 0))
    {
      --iter;
      ++step;
    }
  }
  if (step != 0)
  {
    throw std::runtime_error("Can't move on setted step\n");
  }
  return iter;
}


