#include <iostream>
#include <string>
#include <sstream>
#include "parser-commands.hpp"

void task1()
{
  std::string str;
  PhoneBookInterface bookmarks;
  while (std::getline(std::cin, str))
  {
    std::stringstream strStream(str);
    std::string command;
    strStream >> command;
    parseCommand(command)(bookmarks, strStream);
  }
}
