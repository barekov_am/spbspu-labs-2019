#ifndef FACTORIALCONTAINER_HPP
#define FACTORIALCONTAINER_HPP

#include <iterator>

class FactorialContainer
{
public:
  class Iterator: public std::iterator<std::bidirectional_iterator_tag, size_t>
  {
  public:

    bool operator ==(const Iterator& rhs) const;
    bool operator !=(const Iterator& rhs) const;

    const value_type& operator *() const;

    Iterator& operator ++();
    Iterator operator ++(int);

    Iterator& operator --();
    Iterator operator --(int);

  private:
    friend FactorialContainer;
    Iterator();
    Iterator(value_type maxOrder, value_type valueEndIter);
    value_type order_;
    value_type value_;
  };

  using ReverseIterator = std::reverse_iterator<Iterator>;
  using value_type = Iterator::value_type;

  Iterator begin() const;
  Iterator end() const;

  ReverseIterator rbegin() const;
  ReverseIterator rend() const;

private:
  static const value_type maxOrder_ = 11;
  static const value_type valueEndIter_ = 39916800;
};

#endif // FACTORIALCONTAINER_HPP
