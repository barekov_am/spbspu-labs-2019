#include "phone-book-interface.hpp"
#include <iostream>
#include <algorithm>

const std::string invBookmark = "<INVALID BOOKMARK>\n";
const std::string empty = "<EMPTY>\n";

PhoneBookInterface::PhoneBookInterface()
{
  marks_["current"] = records_.end();
}

void PhoneBookInterface::add(PhoneBook::data& newData)
{
  records_.pushBack(newData);
  if (++records_.begin() == records_.end())
  {
    marks_["current"] = records_.begin();
  }
}

void PhoneBookInterface::store(std::string& oldMarkName, std::string& newMarkName)
{
  PhoneBookInterface::iterator iter = marks_.find(oldMarkName);
  if (iter == marks_.end())
  {
    std::cout << invBookmark;
    return;
  }

  if (iter->second == records_.end())
  {
    std::cout << invBookmark;
    return;
  }

  marks_[newMarkName] = marks_[oldMarkName];
}

void PhoneBookInterface::insertAfter(std::string& markName, PhoneBook::data& newData)
{
  PhoneBookInterface::iterator iter = marks_.find(markName);
  if (iter == marks_.end())
  {
    std::cout << invBookmark;
    return;
  }

  records_.insertAfter(iter->second, newData);

  if (++records_.begin() == records_.end())
  {
    marks_["current"] = records_.begin();
  }
}

void PhoneBookInterface::insertBefore(std::string &markName, PhoneBook::data &newData)
{
  PhoneBookInterface::iterator iter = marks_.find(markName);
  if (iter == marks_.end())
  {
    std::cout << invBookmark;
    return;
  }

  records_.insertBefore(iter->second, newData);
  if (++records_.begin() == records_.end())
  {
    marks_["current"] = records_.begin();
  }
}

void PhoneBookInterface::remove(std::string& markName)
{
  PhoneBookInterface::iterator deletedIterMark = marks_.find(markName);
  if (deletedIterMark == marks_.end())
  {
    std::cout << invBookmark;
    return;
  }

  PhoneBook::iterator deletedIter = deletedIterMark->second;

  if (deletedIter == records_.end())
  {
    throw std::runtime_error("Try to delete empty bookmark\n");
  }


  for (PhoneBookInterface::iterator iterMark = marks_.begin(); iterMark != marks_.end();  ++iterMark)
  {
    if (deletedIter == iterMark->second)
    {
      if (deletedIterMark != iterMark)
      {
      ++(iterMark->second);
      }
    }
  }

  deletedIterMark->second = records_.remove(deletedIterMark->second);

  deletedIter = deletedIterMark->second;
  if ((deletedIter == records_.end()) && (deletedIter != records_.begin()))
  {
    for (PhoneBookInterface::iterator iterMark = marks_.begin(); iterMark != marks_.end();  ++iterMark)
    {
      if (deletedIter == iterMark->second)
      {
        --(iterMark->second);
      }
    }
  }
}

void PhoneBookInterface::show(std::string &markName)
{
  PhoneBookInterface::iterator iter = marks_.find(markName);
  if (iter == marks_.end())
  {
    std::cout << invBookmark;
    return;
  }

  if (iter->second == records_.end())
  {
    std::cout << empty;
    return;
  }
  records_.show(iter->second);
}

void PhoneBookInterface::move(std::string& markName, int step)
{
  PhoneBookInterface::iterator iter = marks_.find(markName);
  if (iter == marks_.end())
  {
    std::cout << invBookmark;
    return;
  }
  iter->second = records_.move(iter->second, step);
}

void PhoneBookInterface::move(std::string& markName, std::string& firstOrLast)
{
  PhoneBookInterface::iterator iter = marks_.find(markName);
  if (iter == marks_.end())
  {
    std::cout << invBookmark;
    return;
  }

  if (firstOrLast.compare("first") == 0)
  {
    iter->second = records_.begin();
  }
  else if (firstOrLast.compare("last") == 0)
  {
    iter->second = --records_.end();
  }
  else
  {
    std::cout << "<INVALID STEP>\n";
  }
}

void PhoneBookInterface::moveNext(std::string& markName)
{
  PhoneBookInterface::iterator iter = marks_.find(markName);
  if (iter == marks_.end())
  {
    std::cout << invBookmark;
    return;
  }

  PhoneBook::iterator movedIter = iter->second;

  if (movedIter == records_.end())
  {
    throw std::runtime_error("Try to move empty bookmark\n");
  }

  if (movedIter == --records_.end())
  {
    throw std::runtime_error("Try to move forward last record\n");
  }

  movedIter++;

}

void PhoneBookInterface::movePrev(std::string& markName)
{
  PhoneBookInterface::iterator iter = marks_.find(markName);
  if (iter == marks_.end())
  {
    std::cout << invBookmark;
    return;
  }

  PhoneBook::iterator movedIter = iter->second;

  if (movedIter == records_.end())
  {
    throw std::runtime_error("Try to move empty bookmark\n");
  }

  if (movedIter == records_.begin())
  {
    throw std::runtime_error("Try to move back first record\n");
  }

  movedIter--;

}
