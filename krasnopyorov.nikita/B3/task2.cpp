#include <algorithm>
#include <iostream>
#include "factorial-container.hpp"

void task2()
{
  FactorialContainer factCont;

  std::copy(factCont.begin(), factCont.end(), std::ostream_iterator<size_t>(std::cout, " "));
  std::cout << "\n";

  std::reverse_copy(factCont.begin(), factCont.end(), std::ostream_iterator<size_t>(std::cout, " "));
  std::cout << "\n";
}
