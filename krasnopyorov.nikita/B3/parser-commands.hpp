#ifndef PARSERCOMMANDS_HPP
#define PARSERCOMMANDS_HPP
#include <functional>
#include <sstream>
#include "phone-book-interface.hpp"

using executeCommand = std::function<void(PhoneBookInterface&, std::stringstream&)>;
executeCommand parseCommand(std::string&);
#endif // PARSERCOMMANDS_HPP
