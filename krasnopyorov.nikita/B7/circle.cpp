#include "circle.hpp"

Circle::Circle(Point &center):
  Shape(center)
{
}

std::ostream& Circle::draw(std::ostream& cout) const 
{
  cout << circle << " " << center_ << "\n";
  return cout;
}
