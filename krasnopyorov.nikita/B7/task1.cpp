#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <math.h>


void task1()
{
  std::vector<double> vector{std::istream_iterator<double>(std::cin), std::istream_iterator<double>()};
  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::runtime_error("Thread contains not only double num\n");
  }

  auto functor = std::bind(std::multiplies<double>(), std::placeholders::_1, M_PI);

  std::transform(vector.begin(), vector.end(), vector.begin(), functor);

  std::copy(vector.begin(), vector.end(), std::ostream_iterator<double>(std::cout, " "));
}
