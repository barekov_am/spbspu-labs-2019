#include <list>
#include <algorithm>
#include <iterator>
#include <functional>
#include "shape.hpp"

void print(std::list<Shape::shapePtr>& list)
{
  std::for_each(list.begin(), list.end(), [](Shape::shapePtr& ptr){ptr->draw(std::cout);});
}

void task2()
{
  Shape::shapePtr shapePtr;
  std::list<Shape::shapePtr> list;

  while (std::cin >> shapePtr)
  {
    list.push_back(shapePtr);
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::runtime_error("Error while reading data from input thread\n");
  }

   std::cout << "Original:\n";
   print(list);

   list.sort([](const Shape::shapePtr& lhs, const Shape::shapePtr& rhs){return lhs->isMoreLeft(*rhs);});
   std::cout << "Left-Right:\n";
   print(list);
   std::cout << "Right-Left:\n";
   list.reverse();
   print(list);

   list.sort([](const Shape::shapePtr& lhs, const Shape::shapePtr& rhs){return lhs->isUpper(*rhs);});
   std::cout << "Top-Bottom:\n";
   print(list);
   std::cout << "Bottom-Top:\n";
   list.reverse();
   print(list);
}
