#include "triangle.hpp"

Triangle::Triangle(Point& center):
  Shape(center)
{
}

std::ostream& Triangle::draw(std::ostream& cout) const 
{
  cout << triangle << " " << center_ << "\n";
  return cout;
}
