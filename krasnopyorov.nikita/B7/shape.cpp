#include "shape.hpp"
#include <unordered_map>
#include <functional>
#include "circle.hpp"
#include "triangle.hpp"
#include "square.hpp"
#include "extract-number.hpp"

Shape::Shape(Point &center):
  center_(center)
{
}

bool Shape::isUpper(const Shape& rhs) const 
{
  return center_.y > rhs.center_.y;
}

bool Shape::isMoreLeft(const Shape& rhs) const 
{
  return center_.x < rhs.center_.x;
}

std::istream& operator>>(std::istream& cin, Shape::shapePtr& shape)
{
  std::cin >> std::ws;
  if (cin.eof())
  {
    cin.setstate(std::ios_base::failbit);
    return cin;
  }

  std::string name;
  while ((cin.peek() != SeparatingCharacters::LPARENTHESIS) && (!cin.eof()) && (!isspace(cin.peek())))
  {
    char symbol;
    cin.get(symbol);
    name.push_back(symbol);
  }

  if (cin.eof())
  {
    cin.clear(std::ios_base::failbit);
    return cin;
  }

  static std::unordered_map<std::string, std::function<Shape::shapePtr(Point&)>> map = {
      {circle, [](Point& point){return std::make_shared<Circle>(point);}},
      {triangle, [](Point& point){return std::make_shared<Triangle>(point);}},
      {square, [](Point& point){return std::make_shared<Square>(point);}}
  };

  auto iter = map.find(name);

  if (iter == map.end())
  {
    cin.clear(std::ios_base::failbit);
    return cin;
  }

  Point center;
  cin >> center;

  if (cin.fail())
  {
    return cin;
  }

  Shape::shapePtr newShape = iter->second(center);
  shape.swap(newShape);
  if (!cin.eof())
  {
    cin >> blank;
  }

  if (!cin.eof() && !isspace(cin.peek()))
  {
    cin.clear(std::ios_base::failbit);
    return cin;
  }


  return cin;
}

