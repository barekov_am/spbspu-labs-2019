#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

const std::string circle = "CIRCLE";

class Circle: public Shape
{
public:
  Circle(Point& center);

  std::ostream& draw(std::ostream& cout) const override;
};

#endif // CIRCLE_HPP
