#include "square.hpp"

Square::Square(Point& center):
  Shape(center)
{
}

std::ostream& Square::draw(std::ostream& cout) const 
{ 
  cout << square << " " << center_ << "\n";
  return cout;
}
