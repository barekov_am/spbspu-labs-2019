#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <memory>
#include "../common/point.hpp"

class Shape
{
public:
  using shapePtr = std::shared_ptr<Shape>;

  Shape(Point& center);
  virtual ~Shape() = default;

  bool isMoreLeft(const Shape& rhs) const ;
  bool isUpper(const Shape& rhs) const ;

  virtual std::ostream& draw(std::ostream& cout) const = 0;

protected:
  Point center_;
};

std::istream& operator>>(std::istream& cout, std::shared_ptr<Shape>& shape);

#endif // SHAPE_HPP
