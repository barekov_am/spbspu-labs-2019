#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

const std::string triangle = "TRIANGLE";

class Triangle: public Shape
{
public:
  Triangle(Point &center);

  std::ostream& draw(std::ostream& cout) const  override;
};

#endif // TRIANGLE_HPP
