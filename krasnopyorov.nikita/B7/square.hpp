#ifndef SQUARE_HPP
#define SQUARE_HPP
#include "shape.hpp"

const std::string square = "SQUARE";

class Square: public Shape
{
public:
  Square(Point& center);

  std::ostream& draw(std::ostream& cout) const override;
};

#endif // SQUARE_HPP
