#include "shape.hpp"
#include <cmath>
#include <iterator>
#include <algorithm>
#include "manipulator.hpp"

const double epsilon = 0.00001;
const int minCountAngles = 2;
const int quadVert = 4;

std::ostream& operator<<(std::ostream& cout, const Shape& shape)
{
  cout << shape.size() << "  ";
  std::copy(shape.begin(), shape.end(), std::ostream_iterator<Point>(cout, "  "));
  std::cout << "\n";

  return cout;
}

std::istream& operator>>(std::istream& cin, Shape& shape)
{
  cin >> std::ws;
  if (cin.eof())
  {
    cin.setstate(std::ios_base::failbit);
    return cin;
  }

  int count = 0;

  cin >> count;

  if (cin.fail())
  {
    return cin;
  }

  if (count <= minCountAngles)
  {
    cin.clear(std::ios_base::failbit);
    return cin;
  }

  Point point;

  while((count != 0) && (cin >> point))
  {
    shape.push_back(point);
    --count;
  }

  if (cin.fail() && !cin.eof())
  {
    return cin;
  }

  if (count != 0)
  {
    cin.clear(std::ios_base::failbit);
    return cin;
  }

  cin >> blank;

  if (!cin.eof() && !isspace(cin.peek()))
  {

    cin.clear(std::ios_base::failbit);
    return cin;
  }

  return cin;
}

bool isShapeRectOrSquare(const Shape& shape)
{
  if (shape.size() != quadVert)
  {
    return false;
  }

  int firstDiagX = shape[0].x - shape[2].x;
  int firstDiagY = shape[0].y - shape[2].y;
  int secondDiagX = shape[1].x - shape[3].x;
  int secondDiagY = shape[1].y - shape[3].y;

  if (std::abs(std::sqrt(firstDiagX * firstDiagX + firstDiagY * firstDiagY)
      - std::sqrt(secondDiagX * secondDiagX + secondDiagY * secondDiagY)) > epsilon)
  {
    return false;
  }

  return true;
}

bool isShapeRect(const Shape& shape)
{
  if (isShapeRectOrSquare(shape))
  {
    if (std::abs(shape[0].x - shape[1].x) == 0)
    {
      if (std::abs(shape[0].y - shape[1].y) == std::abs(shape[2].y - shape[3].y)
          && std::abs(shape[1].x - shape[2].x) == std::abs(shape[3].x - shape[0].x))
      {
        return true;
      }
    }
    else
    {
      if (std::abs(shape[0].x - shape[1].x) == std::abs(shape[2].x - shape[3].x)
          && std::abs(shape[1].y - shape[2].y) == std::abs(shape[3].y - shape[0].y))
      {
        return true;
      }
    }
  }

  return false;
}
bool isShapeSquare(const Shape& shape)
{
  if(isShapeRectOrSquare(shape))
  {
    if (isShapeRect(shape))
    {
      if (std::abs(shape[0].x - shape[1].x) == 0)
      {
        if (std::abs(shape[0].y - shape[1].y) == std::abs(shape[1].x - shape[2].x)
              && std::abs(shape[1].x - shape[2].x) == std::abs(shape[2].y - shape[3].y)
                  && std::abs(shape[2].y - shape[3].y) == std::abs(shape[3].x - shape[0].x))
        {
          return true;
        }
      }
      else
      {
        if ((std::abs(shape[0].x - shape[1].x) == std::abs(shape[1].y - shape[2].y)
             && std::abs(shape[1].y - shape[2].y) == std::abs(shape[2].x - shape[3].x)
                 && std::abs(shape[2].x - shape[3].x) == std::abs(shape[3].y - shape[0].y)))
        {
          return true;
        }
      }
    }
  }

  return false;
}
