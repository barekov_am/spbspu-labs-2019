#ifndef ALGORITHMS_HPP
#define ALGORITHMS_HPP
#include <memory>
#include "shape.hpp"

using vectorPtr = std::shared_ptr<std::vector<Point>>;

int countVertices(shapesContainer &shapes);
int countTriangles(shapesContainer &shapes);
int countSquares(shapesContainer &shapes);
int countRects(shapesContainer &shapes);
void removePentagons(shapesContainer &shapes);
vectorPtr makePointsVector(const shapesContainer &);
void sort(shapesContainer& shapes);

#endif // ALGORITHMS_HPP
