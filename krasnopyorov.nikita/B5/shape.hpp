#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <vector>
#include <iostream>
#include <list>
#include "point.hpp"

using Shape = std::vector<Point>;
using shapesContainer = std::list<Shape>;

std::ostream& operator<<(std::ostream&, const Shape&);
std::istream& operator>>(std::istream&, Shape&);


bool isShapeRectOrSquare(const Shape &);
bool isShapeRect(const Shape&);
bool isShapeSquare(const Shape&);


#endif // SHAPE_HPP
