#include <string>
#include <iostream>
#include <unordered_set>
#include <algorithm>
#include <iterator>

using istreamIt = std::istream_iterator<std::string>;
using ostreamIt = std::ostream_iterator<std::string>;

void task1()
{
  std::unordered_set<std::string> set{istreamIt(std::cin), istreamIt()};
  std::copy(set.begin(), set.end(), ostreamIt(std::cout, "\n"));
}
