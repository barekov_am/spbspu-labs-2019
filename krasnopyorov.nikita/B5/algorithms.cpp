#include "algorithms.hpp"
#include <algorithm>
#include <numeric>

const int triangleVert = 3;
const int pentaVert = 5;

bool compare(const Shape& lhs, const Shape& rhs)
{
  if (rhs.size() != triangleVert)
  {
    if ((lhs.size() == triangleVert))
    {
      return true;
    }

    if (isShapeSquare(lhs) && (!isShapeSquare(rhs)))
    {
      return true;
    }

    if ((isShapeRect(lhs)) && (!isShapeRectOrSquare(rhs)))
    {
      return true;
    }
  }

  return false;
}

int countVertices(shapesContainer& shapes)
{

  return std::accumulate(shapes.begin(), shapes.end(), 0,
      [](const size_t& result, const Shape& shape){return std::move(result) + shape.size();});
}

int countTriangles(shapesContainer& shapes)
{
  return std::count_if(shapes.begin(), shapes.end(),
      [](Shape& shape){return (shape.size() == triangleVert);});
}

int countSquares(shapesContainer& shapes)
{
  return std::count_if(shapes.begin(), shapes.end(),
      [](Shape& shape){return isShapeSquare(shape);});
}

int countRects(shapesContainer& shapes)
{
  return std::count_if(shapes.begin(), shapes.end(),
      [](Shape& shape){return isShapeRect(shape);});
}

void removePentagons(shapesContainer& shapes)
{
  shapes.remove_if([](Shape& shape){return shape.size() == pentaVert;});
}

vectorPtr makePointsVector(const shapesContainer& shapes)
{
  vectorPtr ptr = std::make_shared<Shape>(shapes.size());
  std::transform(shapes.begin(), shapes.end(), ptr->begin(), [](const Shape& shape){return *shape.begin();});
  return ptr;
}

void sort(shapesContainer& shapes)
{
  shapes.sort(compare);
}






