#include "algorithms.hpp"
#include <iterator>
void task2()
{
  shapesContainer container;
  Shape shape;
  while (std::cin >> shape)
  {
    container.push_back(shape);
    shape.clear();
  }

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::runtime_error("Error while reading data from input thread\n");
  }
  std::cout << "Vertices: " <<countVertices(container) << "\n";
  std::cout << "Triangles: " << countTriangles(container) << "\n";
  std::cout << "Squares: " << countSquares(container) << "\n";
  std::cout << "Rectangles: " << countRects(container) << "\n";
  removePentagons(container);
  vectorPtr ptr(makePointsVector(container));
  std::cout << "Points: ";
  std::copy(ptr->begin(), ptr->end(), std::ostream_iterator<Point>(std::cout, "  "));
  std::cout << "\n";
  sort(container);
  std::cout << "Shapes:\n";
  std::copy(container.begin(), container.end(), std::ostream_iterator<Shape>(std::cout));
}
