#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

void writeFrameRect(const krasnopyorov::rectangle_t &frameRect)
{
  std::cout << "Frame Rectangle: " << "\n";
  std::cout << "Coordinates of centre: (" << frameRect.pos.x << "," << frameRect.pos.y << ")" << "\n";
  std::cout << "Size of frame rectagle: " << "\n";
  std::cout << "Height: " << frameRect.height << "\n";
  std::cout << "Width: " << frameRect.width << "\n";
}

void demonstrateScaling(const std::shared_ptr<krasnopyorov::Shape> pointerShape, double k)
{
  if (pointerShape == nullptr)
  {
    throw std::invalid_argument("Pointer to object can't be nullptr");
  }

  std::cout << "Area before scaling: " << pointerShape->getArea() << "\n";
  pointerShape->scale(k);
  std::cout << "Area after scaling: " << pointerShape->getArea() << "\n";
}

void demonstrateRotating(const std::shared_ptr<krasnopyorov::Shape> pointerShape, double angle)
{
  std::cout << "Before rotate:\n";
  writeFrameRect(pointerShape->getFrameRect());
  pointerShape->rotate(angle);
  std::cout << "After first rotate:\n";
  writeFrameRect(pointerShape->getFrameRect());
  angle = angle + 370;
  pointerShape->rotate(angle);
  std::cout << "After second rotate:\n";
  writeFrameRect(pointerShape->getFrameRect());
}

int main()
{
  try
  {
    std::cout << "Rectangle:\n";
    krasnopyorov::Rectangle objRect1({2.0, 3.0, {0.0, 0.0}});
    std::shared_ptr<krasnopyorov::Shape> pointerRect1 = std::make_shared<krasnopyorov::Rectangle>(objRect1);
    const double angle = 20;
    demonstrateRotating(pointerRect1, angle);

    std::cout << "Circle:\n";
    krasnopyorov::Circle objCircle(1.0, {-1.0, 2.0});
    std::shared_ptr<krasnopyorov::Shape> pointerCircle = std::make_shared<krasnopyorov::Circle>(objCircle);
    demonstrateScaling(pointerCircle, angle);

    krasnopyorov::Rectangle objRect2({1.0, 1.0, {0.0, 1.0}});
    std::shared_ptr<krasnopyorov::Shape> pointerRect2 = std::make_shared<krasnopyorov::Rectangle>(objRect2);


    krasnopyorov::CompositeShape objCompShape(pointerRect1);
    objCompShape.add(pointerRect2);
    objCompShape.add(pointerCircle);

    krasnopyorov::Matrix objMatrix = krasnopyorov::partition(objCompShape);

    for (size_t i = 0; i < objMatrix.getRows(); i++)
    {
      std::cout << "Current row: " << i << "\n";
      for (size_t j = 0; j < objMatrix.getColumns(i); j++)
      {
        std::cout << "Current column: " << j << "\n";
        std::cout << "Information about element:\n";
        writeFrameRect(objMatrix[i][j]->getFrameRect());
      }
    }
  }

  catch (const std::exception &ex)
  {
    std::cerr << ex.what() << "\n";
    return 1;
  }

  return 0;
}
