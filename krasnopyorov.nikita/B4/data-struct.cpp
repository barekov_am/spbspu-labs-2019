#include "data-struct.hpp"

int extractNumber(std::string&, std::istream&);
void skipSpaces(std::string&);

 bool DataStruct::operator<(const DataStruct& rhs)
 {
   if (key1_ < rhs.key1_)
   {
     return true;
   }

   if (key1_ == rhs.key1_)
   {
     if (key2_ < rhs.key2_)
     {
       return true;
     }

     if (key2_ == rhs.key2_)
     {
       if (str_.length() < rhs.str_.length())
       {
         return true;
       }
     }
   }

   return false;
 }

 bool DataStruct::operator>(const DataStruct &rhs)
 {
   if (key1_ > rhs.key1_)
   {
     return true;
   }

   if (key1_ == rhs.key1_)
   {
     if (key2_ > rhs.key2_)
     {
       return true;
     }

     if (key2_ == rhs.key2_)
     {
       if (str_.length() > rhs.str_.length())
       {
         return true;
       }
     }
   }

   return false;
 }

 bool DataStruct::operator<=(const DataStruct &rhs)
 {
   return !operator>(rhs);
 }

 bool DataStruct::operator>=(const DataStruct &rhs)
 {
   return !operator<(rhs);
 }

 bool DataStruct::operator==(const DataStruct &rhs)
 {
   if (key1_ == rhs.key1_)
   {
     return true;
   }

   if (key2_ == rhs.key2_)
   {
     return true;
   }

   if (str_.length() == rhs.str_.length())
   {
     return true;
   }

   return false;
 }

 bool DataStruct::operator!=(const DataStruct &rhs)
 {
   return !operator==(rhs);
 }

 std::ostream& operator<<(std::ostream& cout, const DataStruct& data)
 {
   cout << data.key1_ << "," << data.key2_ << "," << data.str_;
   return cout;
 }

 std::istream& operator>>(std::istream& cin, DataStruct& data)
 {
   if (cin.eof())
   {
     cin.setstate(std::ios_base::failbit);
     return cin;
   }
   std::string str;
   std::getline(cin, str);
   skipSpaces(str);
   if (str.empty())
   {
     cin.setstate(std::ios_base::failbit);
     return cin;
   }
   data.key1_ = extractNumber(str, cin);
   if (cin.fail())
   {
     return cin;
   }
   skipSpaces(str);
   data.key2_ = extractNumber(str, cin);
   if (cin.fail())
   {
     return cin;
   }

   skipSpaces(str);
   if (str.empty())
   {
     cin.clear(std::ios_base::failbit);
   }
   data.str_ = str;

   return cin;
 }


