#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

void writeFrameRect(const krasnopyorov::rectangle_t &frameRect)
{
  std::cout << "Frame Rectangle: " << "\n";
  std::cout << "Coordinates of centre: (" << frameRect.pos.x << "," << frameRect.pos.y << ")" << "\n";
  std::cout << "Size of frame rectagle: " << "\n";
  std::cout << "Height: " << frameRect.height << "\n";
  std::cout << "Width: " << frameRect.width << "\n";
}

void demonstrateScaling(std::shared_ptr<krasnopyorov::Shape> pointerShape, double k)
{
  if (pointerShape == nullptr)
  {
    throw std::invalid_argument("Pointer to object can't be nullptr");
  }

  std::cout << "Area before scaling: " << pointerShape->getArea() << "\n";
  pointerShape->scale(k);
  std::cout << "Area after scaling: " << pointerShape->getArea() << "\n";
}

int main()
{
  krasnopyorov::Circle objCircle(1.0, {0, 0});
  std::shared_ptr<krasnopyorov::Shape> pointerCircle = std::make_shared<krasnopyorov::Circle>(objCircle);

  std::cout << "Square of circle: " << pointerCircle->getArea() << "\n";
  krasnopyorov::rectangle_t frameRect = objCircle.getFrameRect();
  writeFrameRect(frameRect);

  const double dx = 2.1;
  const double dy = 3.3;
  pointerCircle->move(dx, dy);
  objCircle.writePosition();

  krasnopyorov::point_t dot = {0, 0};
  pointerCircle->move(dot);
  objCircle.writePosition();

  const double k = 2.0;
  demonstrateScaling(pointerCircle, k);

  std::cout << "\n\n";

  krasnopyorov::Rectangle objRect1({2.5, 1.0, {0, 0}});
  std::shared_ptr<krasnopyorov::Shape> pointerRect1 = std::make_shared<krasnopyorov::Rectangle>(objRect1);


  krasnopyorov::CompositeShape compShape(pointerCircle);
  compShape.add(pointerRect1);
  krasnopyorov::Rectangle objRect2({1.2, 1.0, {1, 0}});
  std::shared_ptr<krasnopyorov::Shape> pointerRect2 = std::make_shared<krasnopyorov::Rectangle>(objRect2);
  compShape.add(pointerRect2);
  compShape.remove(compShape.getIndex(pointerRect1));
  compShape.add(pointerRect1);

  frameRect = compShape.getFrameRect();
  writeFrameRect(frameRect);

  compShape.move(dx, dy);
  compShape.writePosition();

  std::shared_ptr<krasnopyorov::Shape> pointerCompShape =
      std::make_shared<krasnopyorov::CompositeShape>(compShape);
  demonstrateScaling(pointerCompShape, k);

  std::cout << "\n";

  return 0;
}
