#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP
#include <iostream>

class Functor
{
public:
  Functor();
  void operator()(int number);
  friend std::ostream& operator<<(std::ostream& cout, const Functor& obj);

private:
  int max_;
  int  min_;
  long long int  sum_;
  size_t positive_;
  size_t negative_;
  long long int oddSum_;
  long long int evenSum_;
  size_t count_;
  int first_;
  int last_;
};

std::ostream& operator<<(std::ostream& cout, const Functor& obj);

#endif // FUNCTOR_HPP
