#include "functor.hpp"
#include "limits.h"

Functor::Functor():
  max_(INT_MIN),
  min_(INT_MAX),
  sum_(),
  positive_(),
  negative_(),
  oddSum_(),
  evenSum_(),
  count_(),
  first_(),
  last_()
{
}

void Functor::operator()(int number)
{
  if (count_ == 0)
  {
    first_ = number;
  }
  ++count_;

  if (number > max_)
  {
    max_ = number;
  }

  if (number < min_)
  {
    min_ = number;
  }

  sum_ += number;

  if (number > 0)
  {
    ++positive_;
  }

  if (number < 0)
  {
    ++negative_;
  }

  if ((number % 2) == 0)
  {
    evenSum_ += number;
  }
  else
  {
    oddSum_ += number;
  }

  last_ = number;
}

std::ostream& operator<<(std::ostream& cout, const Functor& func)
{
  if (func.count_ <= 0)
  {
    std::cout << "No Data\n";
    return cout;
  }

  cout << "Max: " << func.max_ << "\n";
  cout << "Min: " << func.min_ << "\n";
  cout << "Mean: " << std::fixed << static_cast<double>(func.sum_) / func.count_ << "\n";
  cout << "Positive: " << func.positive_ << "\n";
  cout << "Negative: " << func.negative_ << "\n";
  cout << "Odd Sum: " << func.oddSum_ << "\n";
  cout << "Even Sum: " << func.evenSum_ << "\n";
  cout << "First/Last Equal: " << (func.first_ == func.last_ ? "yes\n" : "no\n");

  return cout;
}
