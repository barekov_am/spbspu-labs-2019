#include <iostream>
#include <algorithm>
#include <iterator>
#include "functor.hpp"

int main()
{
  try
  {
    Functor func;
    func = std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), func);

    if (std::cin.fail() && !std::cin.eof())
    {
      std::cerr << "Error while reading data from input thread\n";
      return 1;
    }

    std::cout << func;
  }

  catch(std::exception& exception)
  {
    exception.what();
    return 1;
  }

  return 0;
}
