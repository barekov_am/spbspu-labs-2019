#include <iostream>
#include <cstdlib>
#include <ctime>

void task1(const char *);
void task2(const char *);
void task3();
void task4(const char *, const int);

int main(int argc, char *argv[])
{
  try
  {
    if ((argc <= 1) || (argc >= 5))
    {
      std::cerr << "Incorrect count of arguments\n";
      return 1;
    }

    int taskNumber =  std::stoi(argv[1]);
    switch (taskNumber)
    {
    case 1:
      if (argc != 3)
      {
        std::cerr << "Incorrect count of arguments\n";
        return 1;
      }

      task1(argv[2]);
      break;

    case 2:
      if (argc != 3)
      {
        std::cerr << "Incorrect count of arguments\n";
        return 1;
      }

      task2(argv[2]);
      break;

    case 3:
      if (argc != 2)
      {
        std::cerr << "Incorrect count of argument\n";
        return 1;
      }

      task3();
      break;

    case 4:
      if (argc != 4)
      {
        std::cerr << "Incorrect count of argument\n";
        return 1;
      }
      std::srand(std::time(nullptr));

      task4(argv[2], std::stoi(argv[3]));
      break;
    default:
      std::cerr << "Incorrect number of task\n";
      return 1;
    }
  }

  catch (std::exception& exception)
  {
    std::cerr << exception.what() << "\n";
    return 1;
  }

  return 0;
}
