#include <iostream>
#include <vector>

#include "access-to-data.hpp"
#include "print-result.hpp"
#include "select-sort.hpp"



void fillRandom(double *array, const int size)
{
  for (int i = 0; i < size; i++)
  {
    array[i] = (-10 + rand() % 21) / 10.0;
  }
}

void task4(const char *direction, const int size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size must be over 0");
  }

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);

  printResult<>(vector);
  sort<AccessByIterator>(vector, getComparator<double>(direction));
  printResult<>(vector);
}
