#ifndef SELECTSORT_HPP
#define SELECTSORT_HPP

#include <iostream>
#include <cstring>
#include <functional>

template<typename T>
std::function<bool(const T, const T)> getComparator(const char *direction)
{
  if (direction != nullptr)
  {
    if (std::strcmp(direction, "ascending") == 0)
    {
      return std::less<const T>();
    }
    else if (std::strcmp(direction, "descending") == 0)
    {
      return std::greater<const T>();
    }
    throw std::invalid_argument("Unknown direction\n");
  }

  throw std::invalid_argument("Pointer is nullptr!\n");
}


template<template<class> class Access, class T>
void sort(T& container, std::function<bool(const typename T::value_type,
    const typename T::value_type)> compare)
{
  using index = typename Access<T>::index;
  index begin = Access<T>::begin(container);
  index end = Access<T>::end(container);

  for (index i = begin; i != end; ++i)
  {
    auto tmp = i;
    for (index j = i; j != end; ++j)
    {      
      if (compare(Access<T>::getElement(container, j), Access<T>::getElement(container, tmp)))
      {
        tmp = j;
      }
    }

    if (i != tmp)
    {
      std::swap(Access<T>::getElement(container, i), Access<T>::getElement(container, tmp));
    }
  }
}

#endif // SELECTSORT_HPP


