#ifndef PRINTRESULT_HPP
#define PRINTRESULT_HPP

#include <iostream>
#include <iterator>

template <class T>
void printResult(const T& container)
{
  std::copy(container.begin(), container.end(),
      std::ostream_iterator<typename T::value_type>(std::cout, " "));
  std::cout << "\n";
}

#endif // PRINTRESULT_HPP
