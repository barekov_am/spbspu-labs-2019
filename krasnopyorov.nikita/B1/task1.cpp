#include <iostream>
#include <vector>
#include <forward_list>
#include <functional>
#include "access-to-data.hpp"
#include "print-result.hpp"
#include "select-sort.hpp"


void task1(const char *direction)
{
  std::vector<int> vectorBracket;
  int number = 0;
  while (std::cin >> number)
  {
    vectorBracket.push_back(number);
  }

  if ((!std::cin.eof()) && (std::cin.fail()))
  {
    throw std::runtime_error("Error while reading data from input thread\n");
  }

  std::function<bool(int, int)> comparator = getComparator<int>(direction);

  if(vectorBracket.empty())
  {
    return;
  }

  std::vector<int> vectorAt(vectorBracket);
  std::forward_list<int> list(vectorBracket.begin(), vectorBracket.end());

  sort<AccessByBracker>(vectorBracket, comparator);
  sort<AccessByAt>(vectorAt, comparator);
  sort<AccessByIterator>(list, comparator);

  printResult<>(vectorBracket);
  printResult<>(vectorAt);
  printResult<>(list);
}
