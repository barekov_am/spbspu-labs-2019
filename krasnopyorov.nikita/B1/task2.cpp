#include <iostream>
#include <fstream>
#include <memory>
#include <vector>

void task2(const char *file)
{
  std::ifstream in(file);
  if (!in.is_open())
  {
    throw std::invalid_argument("Can't open the file\n");
  }

  size_t capacity = 5;
  std::unique_ptr<char[], decltype(free)*> array(static_cast<char*>(calloc(capacity, 1)), free);

  if (!array)
  {
    throw std::runtime_error("Can't allocate memory\n");
  }

  size_t count = 0;
  while (!in.eof())
  {
    in.read(&array[count], capacity - count);
    count += in.gcount();
    capacity = static_cast<size_t>(capacity * 1.8);
    std::unique_ptr<char[], decltype(free)*> tmpArray(static_cast<char*>(realloc(array.get(), capacity)), free);

    if(!tmpArray)
    {
      throw std::runtime_error("Can't reallocate memory\n");
    }

    array.release();
    std::swap(array, tmpArray);
  }
  in.close();

  std::vector<char> vector(&array[0], &array[count]);

  for (size_t i = 0; i < count; i++)
  {
    std::cout << vector[i];
  }
}
