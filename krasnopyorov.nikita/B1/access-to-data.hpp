#ifndef ACCESSTODATA_HPP
#define ACCESSTODATA_HPP

#include <iostream>

template <class T>
struct AccessByBracker
{
  using index = size_t;

  static index begin(const T& /*container*/)
  {
    return 0;
  }

  static index end(const T& container)
  {
    return container.size();
  }

  static typename T::reference getElement(T& container, index i)
  {
    return container[i];
  }
};


template <class T>
struct AccessByAt
{
  using index = size_t;
  static index begin(const T& /*container*/)
  {
    return 0;
  }

  static index end(const T& container)
  {
    return container.size();
  }

  static typename T::reference getElement(T& container, index i)
  {
    return container.at(i);
  }
};


template <class T>
struct AccessByIterator
{
  using index = typename T::iterator;

  static index begin(T& container)
  {
    return container.begin();
  }

  static index end(T& container)
  {
    return container.end();
  }

  static typename T::reference getElement(T&, index it)
  {
    return *it;
  }
};

#endif // ACCESSTODATA_HPP
