#include <iostream>
#include <vector>
#include <iterator>
#include "print-result.hpp"
#include "access-to-data.hpp"

void task3()
{
  int number = 0;
  std::vector<int> vector;
  while(std::cin >> number)
  {
    if (number == 0)
    {
      break;
    }
    vector.push_back(number);
  }

  if((!std::cin.eof()) && (std::cin.fail()))
  {
    throw std::runtime_error("Error while reading data from input thread\n");
  }

  if (vector.empty())
  {
    return;
  }

  if (number != 0)
  {
    throw std::runtime_error("Last symbol must be zero!\n");
  }

  std::vector<int>::iterator iter = vector.begin();

  switch (vector.back())
  {
  case 1:
    while (iter < vector.end())
    {
      iter = (*iter % 2 == 0) ? vector.erase(iter) : ++iter;
    }
    break;

  case 2:
    while (iter < vector.end())
    {
      iter = (*iter % 3 == 0) ? vector.insert(++iter, 3, 1) : ++iter;
    }
    break;
  }

  printResult<>(vector);
}
