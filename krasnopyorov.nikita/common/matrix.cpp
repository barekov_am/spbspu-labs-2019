#include "matrix.hpp"

krasnopyorov::Matrix::Matrix():
  sizeOfMatrix_(0),
  rows_(0)
{
}

krasnopyorov::Matrix::Matrix(const Matrix &copiedMatrix):
  matrixOfShapes_(std::make_unique<std::shared_ptr<Shape> []>(copiedMatrix.sizeOfMatrix_)),
  countColumns_(std::make_unique<size_t []>(copiedMatrix.rows_)),
  sizeOfMatrix_(copiedMatrix.sizeOfMatrix_),
  rows_(copiedMatrix.rows_)
{
  for (size_t i = 0; i < sizeOfMatrix_; i++)
  {
    matrixOfShapes_[i] = copiedMatrix.matrixOfShapes_[i];
  }

  for (size_t i = 0; i < rows_; i++)
  {
    countColumns_[i] = copiedMatrix.countColumns_[i];
  }
}

krasnopyorov::Matrix::Matrix(Matrix &&movedMatrix):
  matrixOfShapes_(std::move(movedMatrix.matrixOfShapes_)),
  countColumns_(std::move(movedMatrix.countColumns_)),
  sizeOfMatrix_(movedMatrix.sizeOfMatrix_),
  rows_(movedMatrix.rows_)
{
  movedMatrix.sizeOfMatrix_ = 0;
  movedMatrix.rows_ = 0;
}

krasnopyorov::Matrix & krasnopyorov::Matrix::operator =(const Matrix &copiedMatrix)
{
  if (this != &copiedMatrix)
  {
    std::unique_ptr<std::shared_ptr<Shape> []> copyMatrix =
        std::make_unique<std::shared_ptr<Shape> []>(copiedMatrix.sizeOfMatrix_);
    for (size_t i = 0; i < copiedMatrix.sizeOfMatrix_; i++)
    {
      copyMatrix[i] = copiedMatrix.matrixOfShapes_[i];
    }

    std::unique_ptr<size_t []> copyCountColumns = std::make_unique<size_t []>(copiedMatrix.rows_);
    for (size_t i = 0; i < copiedMatrix.rows_; i++)
    {
      copyCountColumns[i] = copiedMatrix.countColumns_[i];
    }

    std::swap(matrixOfShapes_, copyMatrix);
    std::swap(countColumns_, copyCountColumns);
    sizeOfMatrix_ = copiedMatrix.sizeOfMatrix_;
    rows_ = copiedMatrix.rows_;
  }

  return *this;
}

krasnopyorov::Matrix & krasnopyorov::Matrix::operator =(Matrix &&movedMatrix)
{
  if (this != &movedMatrix)
  {
    matrixOfShapes_ = std::move(movedMatrix.matrixOfShapes_);
    countColumns_ = std::move(movedMatrix.countColumns_);
    sizeOfMatrix_ = movedMatrix.sizeOfMatrix_;
    movedMatrix.sizeOfMatrix_ = 0;
    rows_ = movedMatrix.rows_;
    movedMatrix.rows_ = 0;
  }

  return *this;
}

std::shared_ptr<krasnopyorov::Shape> * krasnopyorov::Matrix::operator [](size_t row) const
{
  if (row >= rows_)
  {
    throw std::invalid_argument("Matrix does not consits row with this index");
  }

  return &matrixOfShapes_[getCountElBehind(row)];
}

bool krasnopyorov::Matrix::operator ==(const Matrix &matrix) const
{
  if ((sizeOfMatrix_ != matrix.sizeOfMatrix_) || (rows_ != matrix.rows_))
  {
    return false;
  }

  for (size_t i = 0; i < rows_; i++)
  {
    if (countColumns_[i] != matrix.countColumns_[i])
    {
      return false;
    }
  }

  for (size_t i = 0; i < sizeOfMatrix_; i++)
  {
    if (matrixOfShapes_[i] != matrix.matrixOfShapes_[i])
    {
      return false;
    }
  }

  return true;
}

bool krasnopyorov::Matrix::operator !=(const Matrix &matrix) const
{
  return !(*this == matrix);
}

size_t krasnopyorov::Matrix::getRows() const
{
  return rows_;
}

size_t krasnopyorov::Matrix::getColumns(size_t row) const
{
  if (row >= rows_)
  {
    throw std::invalid_argument("Matrix does not consits row with this index");
  }
  return countColumns_[row];
}

size_t krasnopyorov::Matrix::getCountElBehind(size_t row) const
{
  if (row >= rows_)
  {
    throw std::invalid_argument("Matrix does not consits row with this index");
  }
  size_t count = 0;
  for (size_t i = 0; i < row; i++)
  {
    count += countColumns_[i];
  }

  return count;
}

size_t krasnopyorov::Matrix::getSize() const
{
  return sizeOfMatrix_;
}

void krasnopyorov::Matrix::add(std::shared_ptr<Shape> shape, size_t row)
{
  std::unique_ptr<std::shared_ptr<Shape> []> copyMatrix =
      std::make_unique<std::shared_ptr<Shape> []>(sizeOfMatrix_ + 1);
  if (row >= rows_)
  {
    for (size_t i = 0; i < sizeOfMatrix_; i++)
    {
      copyMatrix[i] = matrixOfShapes_[i];
    }
    copyMatrix[sizeOfMatrix_] = shape;

    std::unique_ptr<size_t []> copyCountColumns = std::make_unique<size_t []>(rows_ + 1);
    for (size_t i = 0; i < rows_; i++)
    {
      copyCountColumns[i] = countColumns_[i];
    }
    copyCountColumns[rows_] = 1;
    std::swap(countColumns_, copyCountColumns);
    rows_++;
  }
  else
  {
    const size_t count = getCountElBehind(row) + getColumns(row);
    for (size_t i = 0; i < count; i++)
    {
      copyMatrix[i] = matrixOfShapes_[i];
    }
    copyMatrix[count] = shape;
    for (size_t i = count; i < sizeOfMatrix_; i++)
    {
      copyMatrix[i + 1] = matrixOfShapes_[i];
    }

    countColumns_[row]++;
  }

  std::swap(matrixOfShapes_, copyMatrix);
  sizeOfMatrix_++;
}


