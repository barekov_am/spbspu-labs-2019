#include "point.hpp"
#include "extract-number.hpp"



std::ostream& operator<<(std::ostream& cout, const Point& point)
{
  cout << static_cast<unsigned char>(SeparatingCharacters::LPARENTHESIS) << point.x
       << static_cast<unsigned char>(SeparatingCharacters::SEMICOLON) << " " << point.y
       << static_cast<unsigned char>(SeparatingCharacters::RPARENTHESIS);
  return cout;
}

std::istream& operator>>(std::istream& cin, Point& point)
{
  int xCoord = extractNumber(cin, SeparatingCharacters::SEMICOLON);
  if (cin.fail())
  {
    return cin;
  }

  int yCoord = extractNumber(cin, SeparatingCharacters::RPARENTHESIS);
  if (cin.fail())
  {
    return cin;
  }

  point = {xCoord, yCoord};

  return cin;

}
