#ifndef MATRIX_HPP
#define MATRIX_HPP
#include <memory>
#include "shape.hpp"

namespace krasnopyorov
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &copiedMatrix);
    Matrix(Matrix &&movedMatrix);
    ~Matrix() = default;

    Matrix & operator =(const Matrix &copiedMatrix);
    Matrix & operator =(Matrix &&movedMatrix);
    std::shared_ptr<Shape> * operator [](size_t row) const;
    bool operator ==(const Matrix &matrix) const;
    bool operator !=(const Matrix &matrix) const;

    size_t getRows() const;
    size_t getColumns(size_t row) const;
    size_t getCountElBehind(size_t row) const;
    size_t getSize() const;
    void add(std::shared_ptr<Shape> shape, size_t row);

  private:
    std::unique_ptr<std::shared_ptr<Shape> []> matrixOfShapes_;
    std::unique_ptr<size_t []> countColumns_;
    size_t sizeOfMatrix_;
    size_t rows_;
  };
}

#endif // MATRIX_HPP
