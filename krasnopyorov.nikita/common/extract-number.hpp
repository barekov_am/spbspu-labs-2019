#ifndef EXTRACTNUMBER_HPP
#define EXTRACTNUMBER_HPP

#include <iostream>
#include <string>

enum  SeparatingCharacters
{
  LPARENTHESIS = '(',
  RPARENTHESIS = ')',
  SEMICOLON = ';'
};

int extractNumber(std::istream&, SeparatingCharacters);

#endif // EXTRACTNUMBER_HPP
