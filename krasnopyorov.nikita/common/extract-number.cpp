#include "extract-number.hpp"
#include <algorithm>
#include "manipulator.hpp"

int extractNumber(std::istream& cin, SeparatingCharacters separating)
{
  cin >> blank;
  if (separating == SeparatingCharacters::SEMICOLON)
  {
    if (cin.peek() != SeparatingCharacters::LPARENTHESIS)
    {
      cin.clear(std::ios_base::failbit);
      return 0;
    }
    cin.get();
    cin >> blank;
  }

  std::string countStr;
  while ((cin.peek() != separating) && (!cin.eof()) && (!isspace(cin.peek())))
  {
    char symbol;
    cin.get(symbol);
    countStr.push_back(symbol);
  }

  if (cin.eof())
  {
    cin.clear(std::ios_base::failbit);
    return 0;
  }

  if (isspace(cin.peek()))
  {
    cin >> blank;
  }

  if (cin.peek() != separating)
  {
    cin.clear(std::ios_base::failbit);
    return 0;
  }

  cin.get();

  char* ptr = nullptr;
  int count = std::strtol(countStr.c_str(), &ptr, 10);
  if (*ptr != 0x00)
  {
    cin.clear(std::ios_base::failbit);
    return 0;
  }

  return count;
}
