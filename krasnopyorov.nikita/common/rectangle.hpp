#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"


namespace krasnopyorov
{
  class Rectangle: public Shape
  {
  public:
    Rectangle(rectangle_t characteristics);
    void move(const point_t &dot) override;
    void move(double deltax, double deltay) override;
    void scale(double k) override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void writePosition() const override;
    double getHeight() const;
    double getWidth() const;
    void rotate(double angle) override;

  private:
    rectangle_t characteristics_;
    double angle_;
  };
}


#endif
