#include "partition.hpp"
#include <base-types.hpp>
#include <math.h>

bool krasnopyorov::intersect(const std::shared_ptr<Shape> shape1, const std::shared_ptr<Shape> shape2)
{
  const rectangle_t frameRect1 = shape1->getFrameRect();
  const rectangle_t frameRect2 = shape2->getFrameRect();

  const double xLengthBetweenCentres = fabs(frameRect1.pos.x - frameRect2.pos.x);
  const double yLengthBetweenCentres = fabs(frameRect2.pos.y - frameRect2.pos.y);

  const double xIntersectionBorder = (frameRect1.width + frameRect2.width) / 2;
  const double yIntersectionBorder = (frameRect1.height + frameRect2.height) / 2;

  return((xLengthBetweenCentres < xIntersectionBorder) && (yLengthBetweenCentres < yIntersectionBorder));
}

krasnopyorov::Matrix krasnopyorov::partition(const CompositeShape &compShape)
{
  const size_t size = compShape.getCount();
  Matrix matrix;

  for (size_t i = 0; i < size; i++)
  {
    bool addStatus = false;
    std::shared_ptr<Shape> currentShape = compShape[i];

    for (size_t j = 0; j < matrix.getRows(); j++)
    {
      addStatus = true;
      size_t sizeOfRow = matrix.getColumns(j);

      for (size_t k = 0; k < sizeOfRow; k++)
      {
        if (intersect(currentShape, matrix[j][k]))
        {
          addStatus = false;
          break;
        }
      }

      if (addStatus)
      {
        matrix.add(currentShape, j);
        break;
      }
    }
    if (!addStatus)
    {
      matrix.add(currentShape, matrix.getRows());
    }
  }
  return matrix;
}

krasnopyorov::Matrix krasnopyorov::partition(const std::unique_ptr<std::shared_ptr<Shape> []> arrayOfShapes)
{
  if (!arrayOfShapes)
  {
    throw std::invalid_argument("Can't get partition for empty array");
  }

  CompositeShape compShape(arrayOfShapes[0]);
  size_t size = (sizeof(arrayOfShapes) / sizeof(arrayOfShapes[0]));

  for (size_t i = 1; i < size; i++)
  {
    compShape.add(arrayOfShapes[i]);
  }

  return partition(compShape);

}
