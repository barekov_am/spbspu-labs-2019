#ifndef POINT_HPP
#define POINT_HPP
#include <iostream>
#include "manipulator.hpp"

struct Point
{
  int x, y;
};

std::ostream& operator<<(std::ostream&, const Point&);
std::istream& operator>>(std::istream&, Point&);

#endif // POINT_HPP
