#include "composite-shape.hpp"
#include <stdexcept>
#include <math.h>

krasnopyorov::CompositeShape::CompositeShape(const CompositeShape &copiedCompositeShape):
  arrayOfShapes_(std::make_unique<std::shared_ptr<Shape> []>(copiedCompositeShape.count_)),
  count_(copiedCompositeShape.count_)
{
  for (size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i] = copiedCompositeShape.arrayOfShapes_[i];
  }
}

krasnopyorov::CompositeShape::CompositeShape(CompositeShape &&movedCompositeShape):
  arrayOfShapes_(std::move(movedCompositeShape.arrayOfShapes_)),
  count_(movedCompositeShape.count_)
{
  movedCompositeShape.count_ = 0;
}

krasnopyorov::CompositeShape::CompositeShape(std::shared_ptr<Shape> shape):
  arrayOfShapes_(std::make_unique<std::shared_ptr<Shape> []>(1)),
  count_(1)
{
  if (!shape)
  {
    throw std::invalid_argument("Composite Shape can't be initialize by empty");
  }

  arrayOfShapes_[0] = shape;
}

krasnopyorov::CompositeShape & krasnopyorov::CompositeShape::operator =(const CompositeShape &copiedCompositeShape)
{
  if (this != &copiedCompositeShape)
  {
    std::unique_ptr<std::shared_ptr<Shape> []> copyArray =
        std::make_unique<std::shared_ptr<Shape> []>(copiedCompositeShape.count_);

    for (size_t i = 0; i < copiedCompositeShape.count_; i++)
    {
      copyArray[i] = copiedCompositeShape.arrayOfShapes_[i];
    }
    std::swap(arrayOfShapes_, copyArray);
    count_ = copiedCompositeShape.count_;
  }

  return *this;
}

krasnopyorov::CompositeShape & krasnopyorov::CompositeShape::operator =(CompositeShape &&movedCompositeShape)
{
  if (this != &movedCompositeShape)
  {
    std::swap(arrayOfShapes_, movedCompositeShape.arrayOfShapes_);
    std::swap(count_, movedCompositeShape.count_);

    movedCompositeShape.count_ = 0;
  }

  return *this;
}

std::shared_ptr<krasnopyorov::Shape> krasnopyorov::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::range_error("Have no shape with this index");
  }

  return arrayOfShapes_[index];
}

void krasnopyorov::CompositeShape::add(std::shared_ptr<Shape> shape)
{
  if (!shape)
  {
    throw std::invalid_argument("Pointer on shape can't be nullptr");
  }

  std::unique_ptr<std::shared_ptr<Shape> []> copyArray = std::make_unique<std::shared_ptr<Shape> []>(count_ + 1);

  for (size_t i = 0; i < count_; i++)
  {
    copyArray[i] = arrayOfShapes_[i];
  }
  copyArray[count_] = shape;

  std::swap(arrayOfShapes_, copyArray);
  count_++;
}

void krasnopyorov::CompositeShape::remove(size_t index)
{
  if (index > (count_ - 1))
  {
    throw std::out_of_range("Have no shape with this number");
  }

  if (count_ == 1)
  {
    throw std::logic_error("It's last elemt of composite shape, must not delete him");
  }

  std::unique_ptr<std::shared_ptr<Shape> []> copyArray =
      std::make_unique<std::shared_ptr<Shape> []>(count_ - 1);

  for (size_t i = 0; i < index; i++)
  {
    copyArray[i] = arrayOfShapes_[i];
  }

  for(size_t i = (index + 1); i < count_; i++)
  {
    copyArray[i - 1] = arrayOfShapes_[i];
  }

  std::swap(arrayOfShapes_, copyArray);
  count_--;
}

krasnopyorov::rectangle_t krasnopyorov::CompositeShape::getFrameRect() const
{
  rectangle_t shapeFrameRect = arrayOfShapes_[0]->getFrameRect();
  double xRight = shapeFrameRect.pos.x + (shapeFrameRect.width / 2);
  double xLeft = shapeFrameRect.pos.x - (shapeFrameRect.width / 2);
  double yTop = shapeFrameRect.pos.y + (shapeFrameRect.height / 2);
  double yBottom = shapeFrameRect.pos.y - (shapeFrameRect.height / 2);

  for (size_t i = 1; i < count_; i++)
  {
    shapeFrameRect = arrayOfShapes_[i]->getFrameRect();
    xRight = std::max(xRight, shapeFrameRect.pos.x + (shapeFrameRect.width / 2));
    xLeft = std::min(xLeft, shapeFrameRect.pos.x - (shapeFrameRect.width / 2));
    yTop = std::max(yTop, shapeFrameRect.pos.y + (shapeFrameRect.height / 2));
    yBottom = std::min(yBottom, shapeFrameRect.pos.y - (shapeFrameRect.height / 2));
  }

  return {xRight - xLeft, yTop - yBottom, {(xRight + xLeft) / 2, (yTop + yBottom) / 2}};
}

size_t krasnopyorov::CompositeShape::getIndex(const std::shared_ptr<Shape> selectedShape) const
{
  for (size_t i = 0; i < count_; i++)
  {
    if (selectedShape == arrayOfShapes_[i])
    {
      return (i);
    }
  }

  throw std::invalid_argument("Composite shape doesn't сontain shape with this pointer ");
}

size_t krasnopyorov::CompositeShape::getCount() const
{
  return count_;
}

void krasnopyorov::CompositeShape::scale(double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("Inccorect scaling coefficient");
  }

  const point_t centerCompShape = getFrameRect().pos;

  for (size_t i = 0; i < count_; i++)
  {
    double dx = arrayOfShapes_[i]->getFrameRect().pos.x - centerCompShape.x;
    double dy = arrayOfShapes_[i]->getFrameRect().pos.y - centerCompShape.y;
    arrayOfShapes_[i]->move(centerCompShape.x + (dx * k), centerCompShape.y + (dy * k));
    arrayOfShapes_[i]->scale(k);
  }
}

double krasnopyorov::CompositeShape::getArea() const
{
  double area = arrayOfShapes_[0]->getArea();
  for (size_t i = 1; i < count_; i++)
  {
    area += arrayOfShapes_[i]->getArea();
  }

  return area;
}

void krasnopyorov::CompositeShape::move(double deltax, double deltay)
{
  for (size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i]->move(deltax, deltay);
  }
}

void krasnopyorov::CompositeShape::move(const point_t &dot)
{
  point_t center = getFrameRect().pos;
  double dx = (dot.x - center.x);
  double dy = (dot.y - center.y);

  move(dx, dy);
}

void krasnopyorov::CompositeShape::writePosition() const
{
  for (size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i]->writePosition();
  }
}

void krasnopyorov::CompositeShape::rotate(double angle)
{
  const double angleInRad = M_PI * angle / 180;
  const double sin = std::sin(angleInRad);
  const double cos = std::cos(angleInRad);
  const point_t rotationCenter = getFrameRect().pos;

  for (size_t i = 0; i < count_; i++)
  {
    point_t rotatingDot = arrayOfShapes_[i]->getFrameRect().pos;
    double xBefore = rotatingDot.x - rotationCenter.x;
    double yBefore = rotatingDot.y - rotationCenter.y;

    double xAfter = (xBefore * cos) - (yBefore * sin);
    double yAfter = (xBefore + sin) + (yBefore * cos);

    arrayOfShapes_[i]->move(xAfter - xBefore, yAfter - yBefore);
    arrayOfShapes_[i]->rotate(angle);
  }
}
