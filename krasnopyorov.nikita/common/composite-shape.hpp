#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP
#include "shape.hpp"
#include <memory>

namespace krasnopyorov
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape(CompositeShape const &copiedShape);
    CompositeShape(CompositeShape &&movedShape);
    CompositeShape(std::shared_ptr<Shape> shape);
    ~CompositeShape() = default;

    CompositeShape & operator =(const CompositeShape &copiedShape);
    CompositeShape & operator =(CompositeShape &&movedShape);
    std::shared_ptr<Shape> operator [](size_t index) const;

    void add(std::shared_ptr<Shape> shape);
    size_t getIndex(const std::shared_ptr<Shape> selectedShape) const;
    size_t getCount() const;
    void remove(size_t index);
    void move(double deltax, double deltay) override;
    void move(const point_t &dot) override;
    void scale(double k) override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void writePosition() const override ;
    void rotate(double angle) override;

  private:
    std::unique_ptr<std::shared_ptr<Shape> []> arrayOfShapes_;
    size_t count_;
  };
}
#endif // COMPOSITESHAPE_HPP
