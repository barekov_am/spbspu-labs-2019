#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"


const double INACCURACY = 0.0001;


BOOST_AUTO_TEST_SUITE(testCompositeShape)

BOOST_AUTO_TEST_CASE(testConstansy)
{
  krasnopyorov::Rectangle testRect({3.0, 2.0, {0, 0}});
  krasnopyorov::Circle testCircle(1.5, {1, 0});
  std::shared_ptr<krasnopyorov::Shape> pointerRect= std::make_shared<krasnopyorov::Rectangle>(testRect);
  std::shared_ptr<krasnopyorov::Shape> pointerCircle = std::make_shared<krasnopyorov::Circle>(testCircle);
  krasnopyorov::CompositeShape testCompShape(pointerCircle);
  testCompShape.add(pointerRect);

  const double areaBefore = testCompShape.getArea();
  const krasnopyorov::rectangle_t frameRectBefore = testCompShape.getFrameRect();
  const krasnopyorov::point_t dot = {1, 2};
  testCompShape.move(dot);

  BOOST_CHECK_CLOSE_FRACTION(areaBefore, testCompShape.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRectBefore.width, testCompShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRectBefore.height, testCompShape.getFrameRect().height, INACCURACY);

  const double dx = 2.0;
  const double dy = 1.0;
  testCompShape.move(dx, dy);

  BOOST_CHECK_CLOSE_FRACTION(areaBefore, testCompShape.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRectBefore.width, testCompShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRectBefore.height, testCompShape.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(testAreaAfterScale)
{
  krasnopyorov::Rectangle testRect({2.0, 3.5, {0, 0}});
  std::shared_ptr<krasnopyorov::Shape> pointerRect = std::make_shared<krasnopyorov::Rectangle>(testRect);
  krasnopyorov::CompositeShape testCompShape(pointerRect);
  krasnopyorov::Circle testCircle(2.0, {1, 1});
  std::shared_ptr<krasnopyorov::Shape> pointerCircle = std::make_shared<krasnopyorov::Circle>(testCircle);
  testCompShape.add(pointerCircle);

  const double k = 2.5;
  const double areaAfterScaling = testCompShape.getArea() * k * k;
  testCompShape.scale(k);

  BOOST_CHECK_CLOSE_FRACTION(areaAfterScaling, testCompShape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(testAdd)
{
  krasnopyorov::Rectangle testRect({2.0, 4.0, {0, 0}});
  std::shared_ptr<krasnopyorov::Shape> pointerRect = std::make_shared<krasnopyorov::Rectangle>(testRect);
  krasnopyorov::CompositeShape testCompShape(pointerRect);
  const double areaBefore = testCompShape.getArea();
  krasnopyorov::Circle testCircle(2.0, {0, 0});
  std::shared_ptr<krasnopyorov::Shape> pointerCircle = std::make_shared<krasnopyorov::Circle>(testCircle);
  testCompShape.add(pointerCircle);

  BOOST_CHECK_CLOSE_FRACTION(areaBefore + testCircle.getArea(), testCompShape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(testRemove)
{
  krasnopyorov::Rectangle testRect({2.0, 3.0, {0, 0}});
  std::shared_ptr<krasnopyorov::Shape> pointerRect = std::make_shared<krasnopyorov::Rectangle>(testRect);
  krasnopyorov::CompositeShape testCompShape(pointerRect);
  krasnopyorov::Circle testCircle(3.3, {0, 0});
  std::shared_ptr<krasnopyorov::Shape> pointerCircle = std::make_shared<krasnopyorov::Circle>(testCircle);
  testCompShape.add(pointerCircle);
  const double areaBefore = testCompShape.getArea();
  testCompShape.remove(1);

  BOOST_CHECK_CLOSE_FRACTION(areaBefore - testCircle.getArea(), testCompShape.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(testIncorrectArgumets)
{
  BOOST_CHECK_THROW(krasnopyorov::CompositeShape(nullptr), std::invalid_argument);

  krasnopyorov::Circle testCircle(1.0, {0, 0});
  std::shared_ptr<krasnopyorov::Shape> pointerCircle = std::make_shared<krasnopyorov::Circle>(testCircle);
  krasnopyorov::CompositeShape testCompShape(pointerCircle);
  krasnopyorov::Rectangle testRect({1.0, 2.5, {0, 0}});
  std::shared_ptr<krasnopyorov::Shape> pointerRect = std::make_shared<krasnopyorov::Rectangle>(testRect);
  testCompShape.add(pointerRect);

  const size_t index = 15;
  BOOST_CHECK_THROW(testCompShape[index], std::range_error);

  BOOST_CHECK_THROW(testCompShape.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(testCompShape.remove(index), std::out_of_range);
  testCompShape.remove(1);
  BOOST_CHECK_THROW(testCompShape.getIndex(pointerRect), std::invalid_argument);
  BOOST_CHECK_THROW(testCompShape.remove(testCompShape.getIndex(pointerCircle)), std::logic_error);

  const double k = -1.0;
  BOOST_CHECK_THROW(testCompShape.scale(k), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(testCopyConstructor)
{
  krasnopyorov::Rectangle testRect({2.0, 3.0, {1, 0}});
  std::shared_ptr<krasnopyorov::Shape> pointerRect = std::make_shared<krasnopyorov::Rectangle>(testRect);
  krasnopyorov::CompositeShape testCompShape(pointerRect);
  krasnopyorov::Circle testCircle(4.0, {0, 0});
  std::shared_ptr<krasnopyorov::Shape> pointerCircle = std::make_shared<krasnopyorov::Circle>(testCircle);
  testCompShape.add(pointerCircle);

  krasnopyorov::CompositeShape compShape(testCompShape);

  BOOST_CHECK_CLOSE_FRACTION(compShape.getArea(), testCompShape.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(compShape.getFrameRect().height, testCompShape.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(compShape.getFrameRect().width, testCompShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(compShape.getFrameRect().pos.x, testCompShape.getFrameRect().pos.x, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(compShape.getFrameRect().pos.y, testCompShape.getFrameRect().pos.y, INACCURACY);
}

BOOST_AUTO_TEST_CASE(testMoveConstructor)
{
  krasnopyorov::Circle testCircle(3.0, {2, 3});
  std::shared_ptr<krasnopyorov::Shape> pointerCircle = std::make_shared<krasnopyorov::Circle>(testCircle);
  krasnopyorov::CompositeShape testCompShape1(pointerCircle);
  krasnopyorov::Rectangle testRect({1.0, 5.0, {3, 0}});
  std::shared_ptr<krasnopyorov::Shape> pointerRect = std::make_shared<krasnopyorov::Rectangle>(testRect);
  testCompShape1.add(pointerRect);

  const double areaBefore = testCompShape1.getArea();
  const krasnopyorov::rectangle_t frameRectBefore = testCompShape1.getFrameRect();

  krasnopyorov::CompositeShape testCompShape2(std::move(testCompShape1));

  BOOST_CHECK_CLOSE_FRACTION(areaBefore, testCompShape2.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRectBefore.height, testCompShape2.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRectBefore.width, testCompShape2.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRectBefore.pos.x, testCompShape2.getFrameRect().pos.x, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRectBefore.pos.y, testCompShape2.getFrameRect().pos.y, INACCURACY);
}

BOOST_AUTO_TEST_CASE(testCopyAssignmentOperator)
{
  krasnopyorov::Circle testCircle1(1.2, {0, 0});
  std::shared_ptr<krasnopyorov::Shape> pointerCircle1 = std::make_shared<krasnopyorov::Circle>(testCircle1);
  krasnopyorov::CompositeShape testCompShape1(pointerCircle1);
  krasnopyorov::Circle testCircle2(3.0, {2, 3});
  std::shared_ptr<krasnopyorov::Shape> pointerCircle2 = std::make_shared<krasnopyorov::Circle>(testCircle2);
  testCompShape1.add(pointerCircle2);

  krasnopyorov::CompositeShape testCompShape2(pointerCircle1);
  testCompShape2 = testCompShape1;

  BOOST_CHECK_CLOSE_FRACTION(testCompShape1.getArea(), testCompShape2.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(testCompShape1.getFrameRect().height, testCompShape2.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(testCompShape1.getFrameRect().width, testCompShape2.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(testCompShape1.getFrameRect().pos.x, testCompShape2.getFrameRect().pos.x, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(testCompShape1.getFrameRect().pos.y, testCompShape2.getFrameRect().pos.y, INACCURACY);
}

BOOST_AUTO_TEST_CASE(testMoveAssignmentOperator)
{
  krasnopyorov::Rectangle testRect1({2.0, 2.0, {0, 0}});
  std::shared_ptr<krasnopyorov::Shape> pointerRect1 = std::make_shared<krasnopyorov::Rectangle>(testRect1);
  krasnopyorov::CompositeShape testCompShape1(pointerRect1);
  krasnopyorov::Rectangle testRect2({3.0, 4.0, {10, -2}});
  std::shared_ptr<krasnopyorov::Shape> pointerRect2 = std::make_shared<krasnopyorov::Rectangle>(testRect2);
  testCompShape1.add(pointerRect2);

  const double areaBefore = testCompShape1.getArea();
  const krasnopyorov::rectangle_t frameRectBefore = testCompShape1.getFrameRect();

  krasnopyorov::CompositeShape testCompShape2(pointerRect1);
  testCompShape2 = std::move(testCompShape1);

  BOOST_CHECK_CLOSE_FRACTION(areaBefore, testCompShape2.getArea(), INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRectBefore.height, testCompShape2.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRectBefore.width, testCompShape2.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRectBefore.pos.x, testCompShape2.getFrameRect().pos.x, INACCURACY);
  BOOST_CHECK_CLOSE_FRACTION(frameRectBefore.pos.y, testCompShape2.getFrameRect().pos.y, INACCURACY);
}

BOOST_AUTO_TEST_SUITE_END();
