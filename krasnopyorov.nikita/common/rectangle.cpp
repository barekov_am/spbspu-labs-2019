#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <math.h>

krasnopyorov::Rectangle::Rectangle(rectangle_t characteristics):
  characteristics_(characteristics),
  angle_(0)
{
  if (characteristics_.height <= 0)
  {
    throw std::invalid_argument("Incorrect height value");
  }

  if (characteristics_.width <= 0)
  {
    throw std::invalid_argument("Incorrect width value");
  }
}

double krasnopyorov::Rectangle::getArea() const
{
  return characteristics_.width * characteristics_.height;
}

double krasnopyorov::Rectangle::getHeight() const
{
  return characteristics_.height;
}

double krasnopyorov::Rectangle::getWidth() const
{
  return characteristics_.width;
}

krasnopyorov::rectangle_t krasnopyorov::Rectangle::getFrameRect() const
{
  const double angle = M_PI * angle_ / 180;
  const double sin = fabs(std::sin(angle));
  const double cos = fabs(std::cos(angle));

  double heightFrameRect = (characteristics_.width * sin) + (characteristics_.height * cos);
  double widthFarmeRect = (characteristics_.width * cos) + (characteristics_.height * sin);

  return {widthFarmeRect, heightFrameRect, characteristics_.pos};
}

void krasnopyorov::Rectangle::move(const point_t &dot)
{
  characteristics_.pos = dot;
}

void krasnopyorov::Rectangle::move(double deltax, double deltay)
{
  characteristics_.pos.x += deltax;
  characteristics_.pos.y += deltay;
}

void krasnopyorov::Rectangle::scale(double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("Inccorect scaling coefficient");
  }
  characteristics_.width *= k;
  characteristics_.height *= k;
}

void krasnopyorov::Rectangle::writePosition() const
{
  std::cout << "Coordinates of the centre (" << characteristics_.pos.x << ","
      << characteristics_.pos.y << ")" << "\n";
}

void krasnopyorov::Rectangle::rotate(double angle)
{
  const double oneTurn = 360;

  angle_ += angle;
  angle_ = (angle_ < 0) ? (oneTurn + fmod(angle_, oneTurn)) : (fmod(angle_, oneTurn));
}
