#include "manipulator.hpp"

std::istream& blank(std::istream& cin)
{
  while(isblank(cin.peek()))
  {
    cin.get();
  }

  return cin;
}
