#include "control-unit.hpp"
#include <unordered_map>
#include <functional>

bool ControlUnit::prevPunctFlag_ = true;
bool ControlUnit::prevCommaFlag_ = false;
size_t ControlUnit::maxStringLength_ = 40;

void ControlUnit::setMaxStringLength(size_t length)
{
  maxStringLength_ = length;
}

void ControlUnit::clear()
{
  prevCommaFlag_ = false;
  prevPunctFlag_ = false;
}

void ControlUnit::clear(istream_flag flag)
{
  clear();
  setState(flag);
}

void ControlUnit::setState(istream_flag flag)
{
  std::unordered_map<istream_flag, std::function<void()>> flagsMap = {
    {istream_flag::prevPunct, [&](){prevPunctFlag_ = true;}},
    {istream_flag::prevComma, [&](){prevCommaFlag_ = true;}}
  };

  flagsMap.find(flag)->second();
}

bool ControlUnit::prevComma()
{
  return prevCommaFlag_;
}

bool ControlUnit::prevPunct()
{
  return prevPunctFlag_;
}

size_t ControlUnit::getMaxLength()
{
  return maxStringLength_;
}




