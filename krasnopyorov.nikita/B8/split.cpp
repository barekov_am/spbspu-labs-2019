#include "token.hpp"

void split(std::list<Token>& tokensList)
{
  std::list<Token>::iterator iterList = tokensList.begin();
  size_t count = iterList->token_.second.length();
  ++iterList;

  while (iterList != tokensList.end())
  {
    std::list<Token>::iterator copyIter = iterList;
    if (copyIter->token_.first != Token::PUNCT)
    {
      size_t lexemeLength = copyIter->token_.second.length();
      count += lexemeLength + 1;

      if (count > copyIter->getMaxLength())
      {
        (--copyIter)->token_.second.push_back('\n');
        count = lexemeLength;
      }
      else
      {
        (--copyIter)->token_.second.push_back(' ');
      }
    }
    else
    {
      std::list<Token>::iterator punctIter = copyIter;
      size_t lexemeLength = copyIter->token_.second.length();

      if (copyIter->token_.second.compare(dash) == 0)
      {
         count += 1;
         (--copyIter)->token_.second.push_back(' ');
      }

      count += lexemeLength;

      if (count > copyIter->getMaxLength())
      {
        if (copyIter->token_.first == Token::PUNCT)
        {
          --copyIter;
        }
        --copyIter;
        copyIter->token_.second.pop_back();
        copyIter->token_.second.push_back('\n');
        count = lexemeLength + (++copyIter)->token_.second.length();
        if (++copyIter != punctIter)
        {
          count += copyIter->token_.second.length();
        }
      }
    }
    ++iterList;
  }
  if (tokensList.begin() != tokensList.end())
  {
      (--tokensList.end())->token_.second.push_back('\n');
  }
}
