#include <iostream>
#include <list>
#include <iterator>
#include <cstring>
#include "token.hpp"

const size_t minStringLength = 25;

void split(std::list<Token>&);

int main(int argc, char *argv[])
{
  try
  {
    if ((argc != 1) && (argc != 3))
    {
      std::cerr << "Invalid count of command line argumnets\n";
      return 1;
    }


    Token token;

    if (argc == 3)
    {
      if (strcmp(argv[1], "--line-width") == 0)
      {
        size_t stringLength = std::stoi(argv[2]);
        if (stringLength < minStringLength)
        {
          std::cerr << "string length must be more or eq then 25\n";
          return 1;
        }
        token.setMaxStringLength(stringLength);
      }
      else
      {
        std::cerr << "Invalid command line arguments\n";
        return 1;
      }
    }

    std::list<Token> tokensList;
    while (std::cin >> token)
    {
      tokensList.push_back(token);
    }

    if (!std::cin.eof() && std::cin.fail())
    {
      return 1;
    }

    split(tokensList);
    std::copy(tokensList.begin(), tokensList.end(), std::ostream_iterator<Token>(std::cout));
    return 0;
  }
  catch (std::exception& exception)
  {
    exception.what();
    return 1;
  }
}
