#include "token.hpp"
#include <unordered_map>
#include <algorithm>
#include <locale>
#include <iterator>

const char comma = ',';
const char hyphen = '-';
const char plus = '+';

const size_t wordMaxLength = 20;
const size_t numMaxLenth = 20;

using tokenMap = std::unordered_map<Token::tokenName, std::function<bool(char)>>;
using streamBufIt = std::istreambuf_iterator<char>;

Token::Token():
  ControlUnit()
{
}

void Token::setMaxStringLength(size_t length)
{
  ControlUnit::setMaxStringLength(length);
}

std::istream& operator>>(std::istream& cin, Token& token)
{
  cin >> std::ws;
  if (cin.eof())
  {
    cin.setstate(std::ios_base::failbit);
    return cin;
  }

  std::locale locale = cin.getloc();

  static tokenMap map = {
    {Token::PUNCT, std::bind(std::ispunct<char>, std::placeholders::_1, locale)},
    {Token::HYPHEN, [&](char symbol){return symbol == hyphen;}},
    {Token::DIGIT, [&](char symbol){return std::isdigit(symbol, locale) || symbol == plus;}},
    {Token::WORD, std::bind(std::isalpha<char>, std::placeholders::_1, locale)}
   };

  streamBufIt inputIter(cin);

  std::string lexeme;
  tokenMap::iterator tokenIter = std::find_if(map.begin(), map.end(),
      [&](const std::pair<Token::tokenName, std::function<bool(char)>>& curr)
      {
        if (curr.second(*inputIter))
        {;
          lexeme.push_back(*inputIter);
          ++inputIter;
          return true;
        }
        return false;
      });

  if (tokenIter == map.end())
  {
    std::cerr << "Unkown symbol\n";
    cin.setstate(std::ios_base::failbit);
    return cin;
  }

  if (tokenIter->first == Token::HYPHEN)
  {
    if (inputIter != streamBufIt())
    {
      if (std::isdigit(*inputIter, locale))
      {
        tokenIter = map.find(Token::DIGIT);
      }
      else if (*inputIter == hyphen)
      {
        tokenIter = map.find(Token::PUNCT);
      }
      else
      {
        std::cerr << "Unkown symbol '-'\n";
        cin.setstate(std::ios_base::failbit);
        return cin;
      }

    }
  }

  if (tokenIter->first == Token::DIGIT)
  {
    while (inputIter != streamBufIt() && std::isdigit(*inputIter, locale))
    {
      lexeme.push_back(*inputIter);
      ++inputIter;
    }

    if ((inputIter != streamBufIt()))
    {
      if (*inputIter == std::use_facet<std::numpunct<char>>(locale).decimal_point())
      {
        lexeme.push_back(*inputIter);
        ++inputIter;
        while (inputIter != streamBufIt() && std::isdigit(*inputIter, locale))
        {
          lexeme.push_back(*inputIter);
          ++inputIter;
        }
      }
    }

    if ((inputIter != streamBufIt()))
    {
      if (!std::isspace(*inputIter, locale))
      {
        std::cerr << "Unkown symbol in the digit\n";
        cin.setstate(std::ios_base::failbit);
        return cin;
      }
    }

    if (lexeme.length() > numMaxLenth)
    {
      std::cerr << "Incorrect length of the digit\n";
      cin.setstate(std::ios_base::failbit);
      return cin;
    }

    token.clear();
  }
  else if (tokenIter->first == Token::PUNCT)
  {
    if (*lexeme.begin() == hyphen)
    {
      while (inputIter != streamBufIt() && *inputIter == hyphen)
      {
        lexeme.push_back(*inputIter);
        ++inputIter;
      }

      if (inputIter != streamBufIt())
      {
        if (lexeme.compare(dash) != 0)
        {
          std::cerr << "Dash must contains 3 hyphen\n";
          cin.setstate(std::ios_base::failbit);
          return cin;
        }

        if (token.prevPunct() && !token.prevComma())
        {
          std::cerr << "Punct before dash differing from comma\n";
          cin.setstate(std::ios_base::failbit);
          return cin;
        }
      }
    }
    else
    {
      if (token.prevPunct())
      {
        std::cerr << "Punct before other punct\n";
        cin.setstate(std::ios_base::failbit);
        return cin;
      }
    }

    token.clear(istream_flag::prevPunct);
    if (*lexeme.begin() == comma)
    {
      token.setState(istream_flag::prevComma);
    }

  }
  else if (tokenIter->first == Token::WORD)
  {
    while (inputIter != streamBufIt())
    {
      while (inputIter != streamBufIt() && tokenIter->second(*inputIter))
      {
        lexeme.push_back(*inputIter);
        ++inputIter;
      }

      if (inputIter != streamBufIt())
      {
        if ((*inputIter == hyphen) && (*(--lexeme.end()) != hyphen))
        {
          lexeme.push_back(*inputIter);
          ++inputIter;
        }
        else if (std::ispunct(*inputIter, locale) || std::isspace(*inputIter, locale))
        {
          break;
        }
        else
        {
          std::cerr << "Unknown symbol in the word\n";
          cin.setstate(std::ios_base::failbit);
          return cin;
        }
      }
    }

    if (lexeme.length() > wordMaxLength)
    {
      std::cerr << "Incorrect length of the word\n";
      cin.setstate(std::ios_base::failbit);
      return cin;
    }

    token.clear();
  }

  token.token_ = std::make_pair(tokenIter->first, lexeme);
  return cin;
}

std::ostream& operator<<(std::ostream& cout, const Token& token)
{
  cout << token.token_.second;
  return cout;
}
