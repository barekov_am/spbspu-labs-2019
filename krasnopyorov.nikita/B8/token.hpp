#ifndef TOKEN_HPP
#define TOKEN_HPP
#include <list>
#include <functional>
#include "control-unit.hpp"

const std::string dash = "---";

struct Token: public ControlUnit
{
public:
  enum tokenName
  {
    WORD,
    DIGIT,
    PUNCT,
    HYPHEN
  };
  using token = std::pair<tokenName, std::string>;

  Token();

  friend std::istream& operator>>(std::istream&, Token&);
  friend std::ostream& operator<<(std::ostream&, const Token&);

  void setMaxStringLength(size_t length);

  friend void split(std::list<Token>&);
private:
  token token_;
};

std::istream& operator>>(std::istream&, Token&);
std::ostream& operator<<(std::ostream&, const Token&);


#endif // TOKEN_HPP
