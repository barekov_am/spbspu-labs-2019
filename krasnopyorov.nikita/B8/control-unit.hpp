#ifndef CONTROLUNIT_HPP
#define CONTROLUNIT_HPP
#include <iostream>



enum istream_flag
{
    prevPunct,
    prevComma
};

class ControlUnit
{

protected:
  static void setMaxStringLength(size_t length);
  static void setState(istream_flag);
  static void clear();
  static void clear(istream_flag flag);
  static bool prevComma();
  static bool prevPunct();
  static size_t getMaxLength();
private:
  static bool prevPunctFlag_;
  static bool prevCommaFlag_;
  static size_t maxStringLength_;
};

#endif // CONTROLUNIT_HPP
