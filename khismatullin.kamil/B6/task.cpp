#include "task.hpp"

#include <iostream>
#include <iterator>
#include <vector>
#include <exception>
#include <algorithm>
#include "functor-b6.hpp"

void task()
{
  Functor functor;
  std::istream_iterator<int> end;
  std::istream_iterator<int> input(std::cin);

  functor = std::for_each(input, end, functor);

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::ios::failure("Failed while reading data");
  }

  if (functor.isEmpty())
  {
    std::cout << "No Data\n";
    return;
  }

  std::cout << "Max: " << functor.getMax() << "\n";
  std::cout << "Min: " << functor.getMin() << "\n";
  std::cout << "Mean: " << std::fixed << functor.getMean() << "\n";
  std::cout << "Positive: " << functor.getPositiveCount() << "\n";
  std::cout << "Negative: " << functor.getNegativeCount() << "\n";
  std::cout << "Odd Sum: " << functor.getOddSum() << "\n";
  std::cout << "Even Sum: " << functor.getEvenSum() << "\n";
  std::cout << "First/Last Equal: ";

  if (functor.isFirstEqualLast())
  {
    std::cout << "yes\n";
  }
  else
  {
    std::cout << "no\n";
  }
}
