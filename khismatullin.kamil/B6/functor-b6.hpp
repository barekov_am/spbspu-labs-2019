#ifndef FUNCTOR_B6_HPP
#define FUNCTOR_B6_HPP

class Functor
{
public:
  Functor();
  void operator()(const int& number);
  long int getMax() const;
  long int getMin() const;
  double getMean() const;
  long getPositiveCount() const;
  long getNegativeCount() const;
  long int getEvenSum() const;
  long int getOddSum() const;
  bool isFirstEqualLast() const;
  bool isEmpty() const;

private:
  long int max_;
  long int min_;
  long int first_;
  long pos_;
  long neg_;
  long counter_;
  long int oddSum_;
  long int evenSum_;
  bool firstEqualLast_;
  bool empty_;
};

#endif
