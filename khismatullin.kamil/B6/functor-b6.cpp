#include "functor-b6.hpp"

Functor::Functor() :
  max_(0),
  min_(0),
  first_ (0),
  pos_(0),
  neg_(0),
  counter_(0),
  oddSum_(0),
  evenSum_(0),
  firstEqualLast_(false),
  empty_(true)
{ }

void Functor::operator()(const int& number)
{
  if (empty_)
  {
    first_ = number;
    min_ = number;
    max_ = number;
    empty_ = false;
  }

  if (number > max_)
  {
    max_ = number;
  }

  if (number < min_)
  {
    min_ = number;
  }

  if (number > 0)
  {
    pos_++;
  }

  if (number < 0)
  {
    neg_++;
  }

  if (number % 2 == 0)
  {
    evenSum_ += number;
  }
  else
  {
    oddSum_ += number;
  }

  if (first_ == number)
  {
    firstEqualLast_ = true;
  }
  else
  {
    firstEqualLast_ = false;
  }

  counter_++;
}

long int Functor::getMax() const 
{
  return max_;
}

long int Functor::getMin() const 
{
  return min_;
}

double Functor::getMean() const 
{
  return (static_cast<double>(oddSum_ + evenSum_) / counter_);
}

long Functor::getPositiveCount() const 
{
  return pos_;
}

long Functor::getNegativeCount() const 
{
  return neg_;
}

long int Functor::getEvenSum() const 
{
  return evenSum_;
}

long int Functor::getOddSum() const 
{
  return oddSum_;
}

bool Functor::isFirstEqualLast() const 
{
  return firstEqualLast_;
}

bool Functor::isEmpty() const 
{
  return empty_;
}
