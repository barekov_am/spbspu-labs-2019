#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "shape.hpp"

int main()
{
  khismatullin::Circle circle({1, 4}, 3);
  khismatullin::Rectangle rectangle({2, 1}, 1, 2);
  khismatullin::CompositeShape composite;

  khismatullin::shape_ptr shape2 = std::make_shared<khismatullin::Rectangle>(rectangle);
  khismatullin::shape_ptr shape1 = std::make_shared<khismatullin::Circle>(circle);
  composite.add(shape1);
  composite.add(shape2);

  composite.show();
  composite.showShapes();

  composite.move({2, -1});
  composite.show();
  composite.showShapes();

  composite.move(1, 4);
  composite.show();
  composite.showShapes();

  composite.scale(2.5);
  composite.show();
  std::cout << "-----------------------------\n";
  for (size_t i = 0; i < composite.getCount(); i++)
  {
    composite[i]->show();
  }
  std::cout << "-----------------------------\n";

  composite.remove(0);
  composite.show();
  composite.showShapes();

  composite.remove(shape2);
  composite.showShapes();

  return 0;
}
