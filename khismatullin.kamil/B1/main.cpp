#include <iostream>
#include <stdexcept>
#include <string>
#include "tasks.hpp"

int main(int argc, char* argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4))
    {
      std::cerr << "Invalid number of arguments (taskNumber and 0-2 arg)";
      return 1;
    }

    char* ptr = nullptr;
    int taskNumber = std::strtol(argv[1], &ptr, 10);
    if (*ptr != '\x00')
    {
      std::cerr << "Invalid argument (taskNumber 1-4)";
      return 1;
    }

    switch (taskNumber)
    {
      case 1:
        if (argc != 3)
        {
          std::cerr << "Invalid number of arguments (1:sorting direction)";
          return 1;
        }
        task1(argv[2]);
        break;

      case 2:
        if (argc != 3)
        {
          std::cerr << "Invalid number of arguments (2:filename)";
          return 1;
        }
        task2(argv[2]);
        break;

      case 3:
        if (argc != 2)
        {
          std::cerr << "Invalid number of arguments (3:without)";
          return 1;
        }
        task3();
        break;

      case 4:
        if (argc != 4)
        {
          std::cerr << "Invalid number of arguments (4:direction and number)";
          return 1;
        }
        task4(argv[2], std::stoi(argv[3]));
        break;

      default:
        std::cerr << "Invalid argument (taskNumber 1-4)";
        return 1;
    }
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << '\n';
    return 1;
  }

  return 0;
}
