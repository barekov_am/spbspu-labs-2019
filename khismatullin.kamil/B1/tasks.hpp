#ifndef TASKS_HPP
#define TASKS_HPP

#include <string>

void task1(const char* direction);
void task2(const char* file);
void task3();
void task4(const char* direction, size_t size);

#endif
