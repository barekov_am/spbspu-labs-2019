#ifndef SORT_HPP
#define SORT_HPP

#include "access.hpp"

bool getDirection(const char* direction);

template <template <class T> class Access, typename T>
void sort(T& collection, bool direction)
{
  typedef typename Access<T>::index index;
  const index begin = Access<T>::begin(collection);
  const index end = Access<T>::end(collection);

  for (index i = begin; i != end; i++)
  {
    index tmp = i;
    for (index j = Access<T>::next(i); j != end; j++)
    {
      bool less = (Access<T>::getElement(collection, j) < Access<T>::getElement(collection, tmp));
      if ((direction && less) || (!direction && !less))
      {
        tmp = j;
      }
    }

    if (tmp != i)
    {
      std::swap(Access<T>::getElement(collection, tmp), Access<T>::getElement(collection, i));
    }
  }
}

#endif
