#include "tasks.hpp"

#include <forward_list>
#include <vector>
#include <memory>
#include <fstream>
#include <iostream>
#include <exception>
#include <random>
#include <ctime>

#include "access.hpp"
#include "print.hpp"
#include "sort.hpp"

//------------------------------#-TASK1-#-------------------------------- 

void task1(const char* direction)
{
  bool dir = getDirection(direction);

  std::vector<int> vectorBracket;
  int num = 0;

  while (!(std::cin >> num).eof())
  {
    if (std::cin.fail())
    {
      throw std::invalid_argument("Fail while reading data (should be only int)");
    }
    vectorBracket.push_back(num);
  }

  if (vectorBracket.empty())
  {
    return;
  }

  std::vector<int> vectorAt = vectorBracket;
  std::forward_list<int> list(vectorBracket.begin(), vectorBracket.end());

  sort<BracketAccess>(vectorBracket, dir);
  sort<AtAccess>(vectorAt, dir);
  sort<IteratorAccess>(list, dir);

  print(vectorBracket);
  print(vectorAt);
  print(list);
}

//------------------------------#-TASK2-#-------------------------------- 

void task2(const char* filename)
{
  const size_t InitialSize = 128;
  using charArray = std::unique_ptr<char[], decltype(&free)>;

  std::ifstream input(filename);
  if (!input)
  {
    throw std::runtime_error("Failed to open file");
  }

  size_t size = InitialSize;

  charArray array(static_cast<char*>(malloc(size)), &free);

  size_t i = 0;
  while (input)
  {
    input.read(&array[i], InitialSize);
    i += input.gcount();

    if (input.gcount() == InitialSize)
    {
      size += InitialSize;
      charArray temp(static_cast<char*>(realloc(array.get(), size)), &free);

      if (temp)
      {
        array.release();
        std::swap(array, temp);
      }
      else
      {
        throw std::runtime_error("Failed to reallocate");
      }
    }
  }
  input.close();

  std::vector<char> vector(&array[0], &array[i]);

  for (const char& it: vector)
  {
    std::cout << it;
  }
}

//------------------------------#-TASK3-#-------------------------------- 

void task3()
{
  std::vector<int> vector;
  bool conditionZero = false;
  int num = 0;

  while (!(std::cin >> num).eof())
  {
    if (std::cin.fail())
    {
      throw std::invalid_argument("Fail while reading data (should be only int)");
    }

    if (num == 0)
    {
      conditionZero = true;
      break;
    }

    vector.push_back(num);
  }

  if (vector.empty())
  {
    return;
  }
  
  if (!conditionZero)
  {
    throw std::runtime_error("No zero");
  }

  switch (vector.back())
  {
    case 1:
      for (auto i = vector.begin(); i != vector.end();)
      {
        if ((*i % 2) == 0)
        {
          vector.erase(i);
        }
        else
        {
          i++;
        }
      }
      break;

    case 2:
      for (auto i = vector.begin(); i != vector.end();)
      {
        if ((*i % 3) == 0)
        {
          i = vector.insert(++i, 3, 1) + 3;
        }
        else
        {
          i++;
        }
      }
      break;

    default:
      break;
  }

  print(vector);
}

//------------------------------#-TASK4-#-------------------------------- 

void fillRandom(double* array, size_t size)
{
  srand(time(0));
  const int Exp = 100;
  for (size_t i = 0; i < size; i++)
  {
    array[i] = (rand() % (Exp * 2 - 1) - Exp + 1) / (Exp * 1.0);
  }
}

void task4(const char* direction, size_t size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size should be more than 0");
  }

  bool dir = getDirection(direction);

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);

  print(vector);
  sort<AtAccess>(vector, dir);
  print(vector);
}

