#ifndef PRINT_HPP
#define PRINT_HPP

#include <iostream>

template <typename T>
void print(const T& collection)
{
  if (collection.empty())
  {
    return;
  }

  for (const typename T::value_type& it: collection)
  {
    std::cout << it << " ";
  }

  std::cout << '\n';
}

#endif
