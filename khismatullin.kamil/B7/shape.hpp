#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>

class Shape
{
public:
  Shape(int x, int y);

  bool isMoreLeft(const Shape &shape) const;
  bool isUpper(const Shape &shape) const;
  virtual void draw() const = 0;

protected:
  int x_, y_;
};

#endif
