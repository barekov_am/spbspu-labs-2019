#include "pi-functor.hpp"

void PiFunctor::operator()(const double& number)
{
  list_.push_back(number * M_PI);
}

std::list<double>& PiFunctor::getList()
{
  return list_;
}
