#ifndef PI_FUNCTOR_HPP
#define PI_FUNCTOR_HPP

#include <cmath>
#include <list>

class PiFunctor
{
public:
  void operator()(const double& number);
  std::list<double>& getList();

private:
  std::list<double> list_;
};

#endif
