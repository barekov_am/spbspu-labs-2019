#include "circle.hpp"

Circle::Circle(const int x, const int y) :
  Shape(x, y)
{ }

void Circle::draw() const
{
  std::cout << "CIRCLE (" << x_ << ";" << y_ << ")\n";
}
