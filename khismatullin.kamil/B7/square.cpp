#include "square.hpp"

Square::Square(const int x, const int y) :
  Shape(x, y)
{ }

void Square::draw() const
{
  std::cout << "SQUARE (" << x_ << ";" << y_ << ")\n";
}
