#include "tasks.hpp"

#include <list>
#include <algorithm>
#include <iterator>
#include <exception>
#include <memory>
#include <sstream>

#include "pi-functor.hpp"
#include "triangle.hpp"
#include "square.hpp"
#include "circle.hpp"

void task1()
{
  PiFunctor functor;
  std::istream_iterator<double> end;
  std::istream_iterator<double> input(std::cin);

  functor = std::for_each(input, end, functor);

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::ios::failure("Failed while reading data");
  }

  std::list<double> list = functor.getList();
  
  if (!list.empty())
  {
    std::ostream_iterator<double> out(std::cout, " "); 
    std::copy(list.begin(), list.end(), out);
  }
}

void task2()
{
  using shape_ptr = std::shared_ptr<Shape>;
  std::list<shape_ptr> list;
  std::string line;
  while (std::getline(std::cin >> std::ws, line))
  {
    if (line.empty())
    {
      continue;
    }
    
    std::stringstream input(line);
    std::string shape;
    int x, y;
    std::string trash;
    
    std::getline(input >> std::ws, shape, '(');
    auto spacesAfter = shape.find(' ');
    if (spacesAfter != std::string::npos)
    {
      shape.erase(spacesAfter, std::string::npos);
    }
    input >> x;
    std::getline(input, trash, ';');
    input >> y;
    std::getline(input, trash, ')');

    if (input.fail())
    {
      throw std::invalid_argument("Incorrect input");
    }

    bool shapeFound = false;

    if (shape == "CIRCLE")
    {
      shape_ptr ptr = std::make_shared<Circle>(x, y);
      list.push_back(ptr);
      shapeFound = true;
    }

    if (shape == "SQUARE")
    {
      shape_ptr ptr = std::make_shared<Square>(x, y);
      list.push_back(ptr);
      shapeFound = true;
    }

    if (shape == "TRIANGLE")
    {
      shape_ptr ptr = std::make_shared<Triangle>(x, y);
      list.push_back(ptr);
      shapeFound = true;
    }

    if (!shapeFound)
    {
      throw std::invalid_argument("Invalid shape");
    }
  }

  std::cout << "Original:\n";
  std::for_each(list.begin(), list.end(), [](const shape_ptr& ptr) { ptr->draw(); });

  std::cout << "Left-Right:\n";
  list.sort([](const shape_ptr& lhs, const shape_ptr& rhs) { return lhs->isMoreLeft(*rhs); });
  std::for_each(list.begin(), list.end(), [](const shape_ptr& ptr) { ptr->draw(); });

  std::cout << "Right-Left:\n";
  list.sort([](const shape_ptr& lhs, const shape_ptr& rhs) { return rhs->isMoreLeft(*lhs); });
  std::for_each(list.begin(), list.end(), [](const shape_ptr& ptr) { ptr->draw(); });

  std::cout << "Top-Bottom:\n";
  list.sort([](const shape_ptr& lhs, const shape_ptr& rhs) { return lhs->isUpper(*rhs); });
  std::for_each(list.begin(), list.end(), [](const shape_ptr& ptr) { ptr->draw(); });

  std::cout << "Bottom-Top:\n";
  list.sort([](const shape_ptr& lhs, const shape_ptr& rhs) { return rhs->isUpper(*lhs); });
  std::for_each(list.begin(), list.end(), [](const shape_ptr& ptr) { ptr->draw(); });
}
