#include <iostream>
#include <stdexcept>
#include <string>

#include "tasks.hpp"

int main(int argc, char* argv[])
{
  try
  { 
    if (argc != 2)
    {
      std::cerr << "Invalid number of arguments (only taskNumber 1-2)";
      return 1;
    }

    char* ptr = nullptr;
    int taskNumber = std::strtol(argv[1], &ptr, 10);
    if (*ptr != '\x00')
    {
      std::cerr << "Incorrect argument (taskNumber int 1-2)";
      return 1;
    }

    switch (taskNumber)
    {
    case 1:
      task1();
      break;

    case 2:
      task2();
      break;

    default:
      std::cerr << "Incorrect argument (taskNumber 1-2)";
      return 1;
    }
  }
  catch (const std::invalid_argument& e)
  {
    std::cerr << e.what() << '\n';
    return 1;
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << '\n';
    return 2;
  }

  return 0;
}
