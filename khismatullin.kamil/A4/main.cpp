#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

int main()
{
  //rotation demonstration
  khismatullin::Circle circle({1, 4}, 3);
  khismatullin::Rectangle rectangle({3, 1}, 1, 2);

  khismatullin::CompositeShape composite;

  khismatullin::shape_ptr shape2 = std::make_shared<khismatullin::Rectangle>(rectangle);
  khismatullin::shape_ptr shape1 = std::make_shared<khismatullin::Circle>(circle);
  composite.add(shape1);
  composite.add(shape2);

  composite.show();
  composite.showShapes();

  shape2->rotate(30);
  composite.show();
  composite.showShapes();

  composite.rotate(60);
  composite.show();
  composite.showShapes();

  //matrix demonstration
  khismatullin::Rectangle rectangle1({5, 7}, 3, 2);

  khismatullin::CompositeShape composite1;
  composite1.add(std::make_shared<khismatullin::Rectangle>(rectangle));
  composite1.add(std::make_shared<khismatullin::Circle>(circle));
  composite1.add(std::make_shared<khismatullin::CompositeShape>(composite));
  composite1.add(std::make_shared<khismatullin::Rectangle>(rectangle1));

  khismatullin::Matrix testMatrix = khismatullin::partition(composite1);

  testMatrix.show();

  return 0;
}
