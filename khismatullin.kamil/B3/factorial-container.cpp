#include "factorial-container.hpp"

FactorialContainer::FactorialIterator::FactorialIterator() :
  value_(1),
  index_(1)
{ }

FactorialContainer::FactorialIterator::FactorialIterator(size_t index) :
  value_(factorial(index)),
  index_(index)
{ }

unsigned int& FactorialContainer::FactorialIterator::operator *()
{
  return value_;
}

FactorialContainer::FactorialIterator& FactorialContainer::FactorialIterator::operator ++()
{
  if (index_ <= SIZE)
  {
    ++index_;
    value_ *= index_;
  }
  
  return *this;
}

FactorialContainer::FactorialIterator FactorialContainer::FactorialIterator::operator ++(int)
{
  FactorialIterator temp = *this;
  ++(*this);

  return temp;
}

FactorialContainer::FactorialIterator& FactorialContainer::FactorialIterator::operator --()
{
  if (index_ > 1)
  {
    value_ /= index_;
    --index_;
  }

  return *this;
}

FactorialContainer::FactorialIterator FactorialContainer::FactorialIterator::operator --(int)
{
  FactorialIterator temp = *this;
  --(*this);

  return temp;
}

bool FactorialContainer::FactorialIterator::operator ==(const FactorialIterator& rhs) const
{
  return ((value_ == rhs.value_) && (index_ == rhs.index_));
}

bool FactorialContainer::FactorialIterator::operator !=(const FactorialIterator& rhs) const
{
  return !(*this == rhs);
}

unsigned int FactorialContainer::FactorialIterator::factorial(size_t index) const
{
  unsigned int result = 1;

  for (size_t i = 1; i <= index; i++)
  {
    result *= i;
  }

  return result;
}

FactorialContainer::FactorialIterator FactorialContainer::begin()
{
  return FactorialIterator(1);
}

FactorialContainer::FactorialIterator FactorialContainer::end()
{
  return FactorialIterator(SIZE + 1);
}
