#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <string>
#include <list>

class Phonebook
{
public:

  struct record_t
  {
    std::string name;
    std::string phone;
  };

  using recordList = std::list<record_t>;
  using iterator = recordList::iterator;

  iterator begin();
  iterator end();

  void push_back(const record_t & record); 
  iterator insert(const iterator position, const record_t & record);
  iterator erase(iterator position);
  
  bool empty() const;

private:
  recordList records_;
};

#endif
