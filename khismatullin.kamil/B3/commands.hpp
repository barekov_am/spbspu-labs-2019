#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <string>
#include "bookmark-manager.hpp"

void cmdAdd(BookmarkManager &manager, std::stringstream &input);
void cmdStore(BookmarkManager &manager, std::stringstream &input);
void cmdInsert(BookmarkManager &manager, std::stringstream &input);
void cmdDelete(BookmarkManager &manager, std::stringstream &input);
void cmdShow(BookmarkManager &manager, std::stringstream &input);
void cmdMove(BookmarkManager &manager, std::stringstream &input);

#endif
