#include "tasks.hpp"

#include <iostream>
#include <sstream>
#include <cstring>
#include <algorithm>

#include "bookmark-manager.hpp"
#include "commands.hpp"
#include "factorial-container.hpp"

void task1()
{
  BookmarkManager manager;
  std::string line;

  while (getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Failed while reading data");
    }

    std::stringstream input(line);
    std::string command;
    input >> command;
    bool cmdFound = false;

    if (strcmp(command.data(), "add") == 0)
    {
      cmdAdd(manager, input);
      cmdFound = true;
    }

    if (strcmp(command.data(), "store") == 0)
    {
      cmdStore(manager, input);
      cmdFound = true;
    }

    if (strcmp(command.data(), "insert") == 0)
    {
      cmdInsert(manager, input);
      cmdFound = true;
    }

    if (strcmp(command.data(), "delete") == 0)
    {
      cmdDelete(manager, input);
      cmdFound = true;
    }

    if (strcmp(command.data(), "show") == 0)
    {
      cmdShow(manager, input);
      cmdFound = true;
    }

    if (strcmp(command.data(), "move") == 0)
    {
      cmdMove(manager, input);
      cmdFound = true;
    }

    if (!cmdFound)
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }

  if (!std::cin.eof())
  {
    throw std::runtime_error("Failed while reading data");
  }
}

void task2()
{
  FactorialContainer fc;
  std::ostream_iterator<int> out(std::cout, " ");

  std::copy(fc.begin(), fc.end(), out);
  std::cout << "\n";

  std::reverse_copy(fc.begin(), fc.end(), out);
  std::cout << "\n";
}
