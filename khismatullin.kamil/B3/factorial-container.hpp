#ifndef FACTORIAL_CONTAINER_HPP
#define FACTORIAL_CONTAINER_HPP

#include <iterator>

class FactorialContainer
{
public:
  class FactorialIterator : public std::iterator<std::bidirectional_iterator_tag, unsigned int>
  {
  public:
    FactorialIterator();
    FactorialIterator(size_t index);

    unsigned int& operator *();

    FactorialIterator& operator ++();
    FactorialIterator operator ++(int);

    FactorialIterator& operator --();
    FactorialIterator operator --(int);

    bool operator ==(const FactorialIterator& rhs) const;
    bool operator !=(const FactorialIterator& rhs) const;

  private:
    unsigned int value_;
    size_t index_;

    unsigned int factorial(size_t index) const;
  };

  static const size_t SIZE = 10;

  FactorialContainer() = default;
  
  FactorialIterator begin();
  FactorialIterator end();
};

#endif
