#include "commands.hpp"

#include <iostream>
#include <sstream>
#include <cstring>
#include <cctype>

#include "bookmark-manager.hpp"
std::string getNumber(std::string &num)
{
  char* ptr = nullptr;
  std::strtol(num.c_str(), &ptr, 10);
  if (*ptr != '\x00')
  {
    return "";
  }

  return num;
}

std::string getName(std::string &name)
{
  if ((name.empty()) || (name.front() != '\"'))
  {
    return "";
  }

  name.erase(name.begin());

  size_t i = 0;
  while (i < name.size() && name[i] != '\"')
  {
    if (name[i] == '\\')
    {
      if ((name[i + 1] == '\"') && (i + 2 < name.size()))
      {
        name.erase(i, 1);
      }
      else
      {
        return "";
      }
    }
    i++;
  }

  if (i == name.size())
  {
    return "";
  }

  name.erase(i);

  if (name.empty())
  {
    return "";
  }

  return name;
}

std::string getMarkName(std::string &markName)
{
  if (markName.empty())
  {
    return "";
  }

  for (size_t i = 0; i < markName.size(); i++)
  {
    if (!isalnum(markName[i]) && (markName[i] != '-'))
    {
      return "";
    }
  }

  return markName;
}

void cmdAdd(BookmarkManager &manager, std::stringstream &input)
{
  std::string number;
  input >> std::ws >> number;
  number = getNumber(number);
  std::string name;
  std::getline(input >> std::ws, name);
  name = getName(name);

  if (name.empty() || number.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  manager.add({name, number});
}

void cmdStore(BookmarkManager &manager, std::stringstream &input)
{
  std::string bookmark;
  input >> std::ws >> bookmark;
  bookmark = getMarkName(bookmark);
  std::string newBookmark;
  input >> std::ws >> newBookmark;
  newBookmark = getMarkName(newBookmark);

  if (bookmark.empty() || newBookmark.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (!manager.bookmarkExists(bookmark))
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  manager.store(bookmark, newBookmark);
}

void cmdInsert(BookmarkManager &manager, std::stringstream &input)
{
  std::string strPos;
  BookmarkManager::InsertPosition insertPos;
  input >> std::ws >> strPos;
  bool positionFound = false;

  if (strcmp(strPos.data(), "after") == 0)
  {
    insertPos = BookmarkManager::InsertPosition::after;
    positionFound = true;
  }

  if (strcmp(strPos.data(), "before") == 0)
  {
    insertPos = BookmarkManager::InsertPosition::before;
    positionFound = true;
  }

  if (!positionFound)
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::string bookmark;
  input >> std::ws >> bookmark;
  bookmark = getMarkName(bookmark);
  std::string number;
  input >> std::ws >> number;
  number = getNumber(number);
  std::string name;
  std::getline(input >> std::ws, name);
  name = getName(name);

  if (bookmark.empty() || number.empty() || name.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (!manager.bookmarkExists(bookmark))
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  manager.insert(bookmark, {name, number}, insertPos);
}

void cmdDelete(BookmarkManager &manager, std::stringstream &input)
{
  std::string bookmark;
  input >> std::ws >> bookmark;
  bookmark = getMarkName(bookmark);
  if (bookmark.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (!manager.bookmarkExists(bookmark))
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  manager.remove(bookmark);
}

void cmdShow(BookmarkManager &manager, std::stringstream &input)
{
  std::string bookmark;
  input >> std::ws >> bookmark;
  bookmark = getMarkName(bookmark);
  if (bookmark.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (!manager.bookmarkExists(bookmark))
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  if (manager.recordsEmpty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  manager.show(bookmark);
}

void cmdMove(BookmarkManager &manager, std::stringstream &input)
{
  std::string bookmark;
  input >> std::ws >> bookmark;
  bookmark = getMarkName(bookmark);

  if (bookmark.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (!manager.bookmarkExists(bookmark))
  {
    std::cout << "<INVALID BOOKMARK>\n";
    return;
  }

  std::string steps;
  input >> std::ws >> steps;
  BookmarkManager::MovePosition movePos;
  int numStep;
  int validStep = 0;

  if (strcmp(steps.data(), "first") == 0)
  {
    movePos = BookmarkManager::MovePosition::first;
    validStep = 1;
  }

  if (strcmp(steps.data(), "last") == 0)
  {
    movePos = BookmarkManager::MovePosition::last;
    validStep = 1;
  }

  if (validStep != 1)
  {
    char* ptr = nullptr;
    numStep = std::strtol(steps.data(), &ptr, 10);
    if (*ptr == '\x00')
    {
      validStep = 2;
    }
  } 

  switch (validStep)
  {
  case 1:
    manager.move(bookmark, movePos);
    break;
  case 2:
    manager.move(bookmark, numStep);
    break;

  default:
    std::cout << "<INVALID STEP>\n";
  }
}
