#include "phonebook.hpp"

#include <iostream>

Phonebook::iterator Phonebook::begin()
{
  return records_.begin();
}

Phonebook::iterator Phonebook::end()
{
  return records_.end();
}

void Phonebook::push_back(const record_t &record)
{
  records_.push_back(record);
}

Phonebook::iterator Phonebook::insert(const iterator position, const record_t & record)
{
  return records_.insert(position, record);
}

Phonebook::iterator Phonebook::erase(iterator position)
{
  return records_.erase(position);
}

bool Phonebook::empty() const
{
  return records_.empty();
}
