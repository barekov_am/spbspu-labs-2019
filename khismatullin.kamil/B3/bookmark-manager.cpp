#include "bookmark-manager.hpp"

#include <iostream>

BookmarkManager::BookmarkManager()
{
  bookmarks_["current"] = records_.end();
}

void BookmarkManager::add(const Phonebook::record_t& record)
{
  records_.push_back(record);

  if (std::next(records_.begin()) == records_.end())
  {
    bookmarks_["current"] = records_.begin();
  }
}

void BookmarkManager::store(const std::string& bookmark, const std::string& newBookmark)
{
  iterator it = getBookmark(bookmark);
  bookmarks_.emplace(newBookmark, it->second);
}

void BookmarkManager::insert(const std::string& bookmark, const Phonebook::record_t& record, InsertPosition position)
{
  iterator it = getBookmark(bookmark);

  if (it->second == records_.end())
  {
    add(record);
    return;
  }
  
  if (position == InsertPosition::before)
  {
    records_.insert(it->second, record);
  }
  else
  {
    records_.insert(std::next(it->second), record);
  }
}

void BookmarkManager::remove(const std::string& bookmark)
{
  iterator it = getBookmark(bookmark);

  if (it->second == records_.end())
  {
    throw std::invalid_argument("Removing element doesn't exist");
  }
  
  iterator begin = bookmarks_.begin();
  iterator end = bookmarks_.end();
  bookmarks::mapped_type current = it->second;

  for (iterator bookmark = begin; bookmark != end; ++bookmark)
  {
    if (bookmark->second == current)
    {
      bookmark->second = std::next(current);
    }
  }

  records_.erase(current);

  if (records_.empty())
  {
    return;
  }

  for (iterator bookmark = begin; bookmark != end; ++bookmark)
  {
    if (bookmark->second == records_.end())
    {
      bookmark->second = std::prev(records_.end());
    }
  }
}

void BookmarkManager::show(const std::string& bookmark)
{
  iterator it = getBookmark(bookmark);

  if (it->second == records_.end())
  {
    throw std::invalid_argument("Records list is empty");
  }

  std::cout << it->second->phone << " " << it->second->name << "\n";
}

void BookmarkManager::move(const std::string& bookmark, MovePosition position)
{
  iterator it = getBookmark(bookmark);
  
  if (position == MovePosition::first)
  {
    it->second = records_.begin();
  }
  else
  {
    it->second = records_.end();
    std::advance(it->second, -1);
  }
}

void BookmarkManager::move(const std::string& bookmark, int steps)
{
  iterator it = getBookmark(bookmark);

  if (steps >= 0)
  {
    if (steps >= std::distance(it->second, records_.end()))
    {
      move(bookmark, MovePosition::last);
      return;
    }
  }
  else
  {
    if (steps * (-1) >= std::distance(records_.begin(), it->second))
    {
      move(bookmark, MovePosition::first);
      return;
    }
  }

  std::advance(it->second, steps);
}

void BookmarkManager::moveNext(const std::string& bookmark)
{
  move(bookmark, 1);
}

void BookmarkManager::movePrev(const std::string& bookmark)
{
  move(bookmark, -1);
}

void BookmarkManager::replace(const std::string& bookmark, const Phonebook::record_t& record)
{
  iterator it = getBookmark(bookmark);

  *(it->second) = record;
}


bool BookmarkManager::bookmarkExists(const std::string& bookmark) const
{
  return bookmarks_.find(bookmark) != bookmarks_.end();
}

bool BookmarkManager::recordsEmpty() const
{
  return records_.empty();
}

BookmarkManager::iterator BookmarkManager::getBookmark(const std::string& bookmark)
{
  iterator it = bookmarks_.find(bookmark);

  if (it != bookmarks_.end())
  {
    return it;
  }

  throw std::invalid_argument("Bookmark doesn't exist");
}
