#include <iostream>
#include <exception>
#include <cstring>
#include "text-parser.hpp"

const int MIN_WIDTH = 25;
const int DEFAULT_WIDTH = 40;

int main(int argc, char * argv[])
{
  try
  {
    if ((argc != 3) && (argc != 1))
    {
      std::cerr << "Invalid number of arguments (empty or 2)";
      return 1;
    }

    int width = DEFAULT_WIDTH;

    if (argc == 3)
    {
      if (strcmp(argv[1], "--line-width") != 0)
      {
        std::cerr << "Invalid first argument (try key --line-width)";
        return 1;
      }

      char * ptr;
      width = std::strtol(argv[2], &ptr, 10);
      if ((*ptr != '\x00') || (width < MIN_WIDTH))
      {
        std::cerr << "Invalid second argument (int width < 25)";
        return 1;
      }
    }

    TextParser parser(width);
    parser.readText();
    parser.printText();
  }
  catch (std::invalid_argument& e)
  {
    std::cerr << e.what() << '\n';
    return 1;
  }
  catch (std::exception& e)
  {
    std::cerr << e.what() << '\n';
    return 2;
  }

  return 0;
}
