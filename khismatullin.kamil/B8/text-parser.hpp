#ifndef TEXT_PARSER_HPP
#define TEXT_PARSER_HPP

#include <iostream>
#include <list>
#include <cstring>

const std::size_t MAX_WORD_SIZE = 20;
const std::size_t HYPHEN_SIZE = 3;

enum class text_type_t
{
  WORD,
  PUNCT,
  NUMBER,
  HYPHEN,
  SPACE
};

struct text_t
{
  text_type_t type;
  std::string string;
};

class TextParser
{
public:
  TextParser(std::size_t width);
  void readText();
  void printText();

private:
  using textList = std::list<text_t>;

  std::size_t width_;
  textList text_;
  text_t lastElem_;

  void readWord();
  void readNumber();
  void readPunct();
  void readHyphen();
  void printLine(const textList &line);
  void reformatLine(textList &line, std::size_t &length);
};

#endif
