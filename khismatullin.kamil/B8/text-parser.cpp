#include "text-parser.hpp"

TextParser::TextParser(std::size_t width) :
  width_(width),
  text_(),
  lastElem_()
{ }

void TextParser::readText()
{
  while (std::cin)
  {
    std::cin >> std::ws;
    char first = std::cin.get();
    char second = std::cin.peek();
    std::cin.unget();

    if (std::isalpha(first))
    {
      readWord();
    }
    else if ((std::isdigit(first)) || ((first == '+' || first == '-') && std::isdigit(second)))
    {
      readNumber();
    }
    else if (std::ispunct(first))
    {
      if (first == '-')
      {
        readHyphen();
      }
      else
      {
        readPunct();
      }
    }
  }
}

void TextParser::readWord()
{
  text_t word = {text_type_t::WORD, ""};

  while (std::isalpha(std::cin.peek()) || (std::cin.peek() == '-'))
  {
    char sym = std::cin.get();
    if (sym == '-' && std::cin.peek() == '-')
    {
      std::cin.unget();
      break;
    }
    word.string.push_back(sym);
  }

  if (word.string.length() > MAX_WORD_SIZE)
  {
    throw std::invalid_argument("Length of \"" + word.string + "\" more than 20 symbols");
  }

  lastElem_ = word;
  text_.push_back(word);
}

void TextParser::readNumber()
{
  text_t number = {text_type_t::NUMBER, ""};

  bool dotFound = false;
  number.string.push_back(std::cin.get());

  while (std::isdigit(std::cin.peek()) || (std::cin.peek() == '.'))
  {
    char sym = std::cin.get();
    if (sym == '.')
    {
      if (dotFound)
      {
        std::cin.unget();
        break;
      }
      dotFound = true;
    }
    number.string.push_back(sym);
  }

  if (number.string.length() > MAX_WORD_SIZE)
  {
    throw std::invalid_argument("Length of \"" + number.string + "\" more than 20 symbols");
  }

  lastElem_ = number;
  text_.push_back(number);
}

void TextParser::readPunct()
{
  if (text_.empty())
  {
    throw std::invalid_argument("Text can't start with punctuation");
  }

  text_t punct = {text_type_t::PUNCT, ""};

  punct.string.push_back(std::cin.get());

  if (lastElem_.type == text_type_t::HYPHEN)
  {
    throw std::invalid_argument("Punctuation can't be after hyphen");
  }
  if (lastElem_.type == text_type_t::PUNCT)
  {
    throw std::invalid_argument("Punctuation can't be after another punctuation");
  }

  lastElem_ = punct;
  text_.push_back(punct);
}

void TextParser::readHyphen()
{
  if (text_.empty())
  {
    throw std::invalid_argument("Text can't start with hyphen");
  }

  text_t hyphen = {text_type_t::HYPHEN, ""};

  while (std::cin.peek() == '-')
  {
    hyphen.string.push_back(std::cin.get());
  }

  if (hyphen.string.length() != HYPHEN_SIZE)
  {
    throw std::invalid_argument("Invalid symbol: " + hyphen.string);
  }
  if (lastElem_.type == text_type_t::HYPHEN)
  {
    throw std::invalid_argument("Hyphen can't be after another hyphen");
  }
  if (lastElem_.type == text_type_t::PUNCT && text_.back().string != ",")
  {
    throw std::invalid_argument("Hyphen can't be after punctuation (except ',')");
  }

  lastElem_ = hyphen;
  text_.push_back(hyphen);
}

void TextParser::printText()
{
  textList line;
  std::size_t lineLength = 0;

  for (auto &elem : text_)
  {
    switch (elem.type)
    {
    case text_type_t::NUMBER :
    case text_type_t::WORD :
      if ((lineLength + elem.string.length() + 1) > width_)
      {
        printLine(line);
        line.clear();
        lineLength = 0;
      }
      else if (!line.empty())
      {
        line.push_back({text_type_t::SPACE, " "});
        lineLength++;
      }
      line.push_back(elem);
      lineLength += elem.string.length();
      break;
    case text_type_t::PUNCT :
      if (lineLength + 1 > width_)
      {
        reformatLine(line, lineLength);
      }
      line.push_back(elem);
      lineLength += elem.string.length();
      break;
    case text_type_t::HYPHEN :
      if (lineLength + HYPHEN_SIZE + 1 > width_)
      {
        reformatLine(line, lineLength);
      }
      line.push_back({text_type_t::SPACE, " "});
      line.push_back(elem);
      lineLength += elem.string.length() + 1;
      break;
    case text_type_t::SPACE :
      break;
    }
  }
  if (!line.empty())
  {
    printLine(line);
  }
}

void TextParser::printLine(const textList &line)
{
  for (const auto &elem : line)
  {
    std::cout << elem.string;
  }
  std::cout << '\n';
}

void TextParser::reformatLine(textList &line, std::size_t &length)
{
  std::size_t tmpLength = 0;
  textList tmpLine;

  while (!line.empty())
  {
    tmpLine.push_front(line.back());
    tmpLength += line.back().string.length();
    line.pop_back();

    if (tmpLine.front().type == text_type_t::WORD || tmpLine.front().type == text_type_t::NUMBER)
    {
      break;
    }
  }
  printLine(line);
  line = tmpLine;
  length = tmpLength;
}
