#ifndef QUEUE_WITH_PRIORITY_HPP
#define QUEUE_WITH_PRIORITY_HPP

#include <list>
#include <iostream>

template <typename T>
class QueueWithPriority
{
public:
  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  void pushElement(const T& element, ElementPriority priority);
  bool popElement(T& element);

  void accelerate();
  bool empty() const;
  void clear();

private:
  std::list<T> high_;
  std::list<T> normal_;
  std::list<T> low_;
};

template <typename T>
void QueueWithPriority<T>::pushElement(const T& element, ElementPriority priority)
{
  switch(priority)
  {
  case ElementPriority::HIGH:
    high_.insert(high_.end(), element);
    break;

  case ElementPriority::NORMAL:
    normal_.insert(normal_.end(), element);
    break;

  case ElementPriority::LOW:
    low_.insert(low_.end(), element);
    break;
  }
}

template <typename T>
bool QueueWithPriority<T>::popElement(T& element)
{
  if (!high_.empty())
  {
    element = high_.front();
    high_.pop_front();
    return true;
  }

  if (!normal_.empty())
  {
    element = normal_.front();
    normal_.pop_front();
    return true;
  }

  if (!low_.empty())
  {
    element = low_.front();
    low_.pop_front();
    return true;
  }

  return false;
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  high_.splice(high_.end(), low_);
}

template <typename T>
bool QueueWithPriority<T>::empty() const
{
  return high_.empty() && normal_.empty() && low_.empty();
}

template <typename T>
void QueueWithPriority<T>::clear()
{
  high_.clear();
  normal_.clear();
  low_.clear();
}

#endif
