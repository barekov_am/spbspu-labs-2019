#include "tasks.hpp"

#include <cstring>
#include <sstream>
#include <stdexcept>
#include <iostream>

#include <list>
#include <iterator>

#include "queue_with_priority.hpp"
#include "commands.hpp"

void task1()
{
  QueueWithPriority<std::string> queue;

  std::string line;
  while (getline(std::cin, line))
  {
    if (!std::cin.eof() && std::cin.fail())
    {
      throw std::runtime_error("Failed while reading data");
    }

    std::stringstream input(line);
    std::string command;
    std::getline(input >> std::ws, command, ' ');

    bool commandFound = false;

    if (strcmp(command.data(), "add") == 0)
    {
      add(queue, input);
      commandFound = true;
    }

    if (strcmp(command.data(), "get") == 0)
    {
      get(queue, input);
      commandFound = true;
    }

    if (strcmp(command.data(), "accelerate") == 0)
    {
      accelerate(queue, input);
      commandFound = true;
    }

    if (!commandFound)
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}

void print(std::list<int>::iterator begin, std::list<int>::iterator end)
{
  if (begin == end)
  {
    std::cout << "\n";
    return;
  }

  if (begin == std::prev(end))
  {
    std::cout << *begin << "\n";
    return;
  }

  std::cout << *begin << " " << *std::prev(end) << " ";

  begin++;
  end--;

  print(begin, end);
}

void task2()
{
  const int min = 1;
  const int max = 20;
  const int size = 20;

  std::list<int> list;
  int num = 0;

  while (std::cin >> num)
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Failed while reading data");
    }
    
    if ((num < min) || (num > max))
    {
      throw std::invalid_argument("Every number should be in range 1-20");
    }
    list.push_back(num);

    if (list.size() > size)
    {
      throw std::invalid_argument("Count of numbers should be < 20");
    }
  }
  
  if (!std::cin.eof())
  {
    throw std::runtime_error("Failed while reading data");
  }


  print(list.begin(), list.end());
}
