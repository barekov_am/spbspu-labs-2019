#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include "queue_with_priority.hpp"

void add(QueueWithPriority<std::string> & queue, std::stringstream & input);
void get(QueueWithPriority<std::string> & queue, std::stringstream & input);
void accelerate(QueueWithPriority<std::string> & queue, std::stringstream & input);

#endif
