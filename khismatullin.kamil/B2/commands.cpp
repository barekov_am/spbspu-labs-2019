#include "commands.hpp"

#include <sstream>
#include <cstring>

void add(QueueWithPriority<std::string> & queue, std::stringstream & input)
{
  std::string priority;
  input >> std::ws >> priority;
  std::string data;
  std::getline(input >> std::ws, data);

  if (data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  bool priorityFound = false;

  if (strcmp(priority.data(), "low") == 0)
  {
    queue.pushElement(data, QueueWithPriority<std::string>::ElementPriority::LOW);
    priorityFound = true;
  }

  if (strcmp(priority.data(), "normal") == 0)
  {
    queue.pushElement(data, QueueWithPriority<std::string>::ElementPriority::NORMAL);
    priorityFound = true;
  }

  if (strcmp(priority.data(), "high") == 0)
  {
    queue.pushElement(data, QueueWithPriority<std::string>::ElementPriority::HIGH);
    priorityFound = true;
  }

  if (!priorityFound)
  {
    std::cout << "<INVALID COMMAND>\n";
  }
}

void get(QueueWithPriority<std::string> &queue, std::stringstream &input)
{
  std::string line;
  std::getline(input >> std::ws, line);

  if (!line.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (queue.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  std::string element;
  queue.popElement(element);
  std::cout << element << '\n';
}

void accelerate(QueueWithPriority<std::string> & queue, std::stringstream & input)
{
  std::string line;
  std::getline(input >> std::ws, line);

  if (!line.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.accelerate();
}
