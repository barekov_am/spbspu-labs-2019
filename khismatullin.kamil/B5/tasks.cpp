#include "tasks.hpp"

#include <set>
#include <iostream>

#include "shape-parser.hpp"

void task1()
{
  std::set<std::string> words;
  std::string tmp;

  while (std::cin >> tmp)
  {
    words.emplace(tmp);
  }
  
  for (auto word : words)
  {
    std::cout << word << '\n';
  }
}

void task2()
{
  ShapesContainer shapes;
  readShapes(shapes);

  std::cout << "Vertices: " << getVerticesCount(shapes) << '\n';
  std::cout << "Triangles: " << getTriangleCount(shapes) << '\n';
  std::cout << "Squares: " << getSquareCount(shapes) << '\n';
  std::cout << "Rectangles: " << getRectangleCount(shapes) << '\n';

  deletePentagons(shapes);

  Shape points = getFirstPoints(shapes);

  std::cout << "Points:";
  for (auto point : points)
  {
    std::cout << " (" << point.x << ";" << point.y << ")";
  }
  std::cout << '\n';

  sortShapesContainer(shapes);

  std::cout << "Shapes:\n";
  for (auto shape : shapes)
  {
    std::cout << shape.size();
    for (auto point : shape)
    {
      std::cout << " (" << point.x << ";" << point.y << ")";
    }
    std::cout << '\n';
  }
}
