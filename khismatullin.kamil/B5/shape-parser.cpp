#include "shape-parser.hpp"

#include <sstream>
#include <iostream>
#include <algorithm>

void readShapes(ShapesContainer& shapesContainer)
{
  std::string line;
  std::string trash;

  while (std::getline(std::cin >> std::ws, line))
  {
    if (line.empty())
    {
      continue;
    }

    std::stringstream input(line);
    
    int vertNum = 0;
    input >> vertNum;

    if (vertNum < 3)
    {
      throw std::invalid_argument("Incorrect number of vertices");
    }

    Shape shape;
    for (int i = 0; i < vertNum; i++)
    {
      Point tmp_point;

      std::getline(input, trash, '(');
      input >> tmp_point.x;

      std::getline(input, trash, ';');
      input >> tmp_point.y;

      std::getline(input, trash, ')');
      shape.push_back(tmp_point);
    }

    if (input.fail())
    {
      throw std::invalid_argument("Incorrect input");
    }

    shapesContainer.push_back(shape);
  }
}

int getVerticesCount(const ShapesContainer& shapesContainer)
{
  int total = 0;
  for (auto shape : shapesContainer)
  {
    total += shape.size();
  }

  return total;
}

int sqrDistance(const Point& rhs, const Point& lhs)
{
  return ((rhs.x - lhs.x) * (rhs.x - lhs.x) + (rhs.y - lhs.y) * (rhs.y - lhs.y));
}

bool cmpX(const Point& rhs, const Point& lhs)
{
  return (rhs.x < lhs.x);
}

bool cmpY(const Point& rhs, const Point& lhs)
{
  return (rhs.y < lhs.y);
}

Shape sortRectangle(const Shape& shape)
{
  Shape sortedByX = shape;
  std::sort(sortedByX.begin(), sortedByX.end(), cmpX);

  Point a = std::min(sortedByX[0], sortedByX[1], cmpY);
  Point b = std::max(sortedByX[0], sortedByX[1], cmpY);
  Point c = std::max(sortedByX[2], sortedByX[3], cmpY);
  Point d = std::min(sortedByX[2], sortedByX[3], cmpY);
  
  Shape sortedRectangle;
  sortedRectangle.push_back(a);
  sortedRectangle.push_back(b);
  sortedRectangle.push_back(c);
  sortedRectangle.push_back(d);

  return sortedRectangle;
}

bool isRectangle(const Shape& shape)
{
  if (shape.size() != RECTANGLE_VERTICES)
  {
    return false;
  }
  
  Shape rect = sortRectangle(shape);
  int diag1 = sqrDistance(rect[0], rect[2]);
  int diag2 = sqrDistance(rect[1], rect[3]);

  return (diag1 == diag2);
}

bool isSquare(const Shape& shape)
{
  if (!isRectangle(shape))
  {
    return false;
  }

  Shape rect = sortRectangle(shape);
  
  int side1 = sqrDistance(rect[0], rect[1]);
  int side2 = sqrDistance(rect[1], rect[2]);
  int side3 = sqrDistance(rect[2], rect[3]);
  int side4 = sqrDistance(rect[3], rect[0]);

  return ((side1 == side2) && (side2 == side3) && (side3 == side4) && (side4 == side1));
}

int getTriangleCount(const ShapesContainer& shapesContainer)
{
  int count = 0;
  for (auto shape : shapesContainer)
  {
    if (shape.size() == TRIANGLE_VERTICES)
    {
      count++;
    }
  }
  return count;
}

int getRectangleCount(const ShapesContainer& shapesContainer)
{
  int count = 0;
  for (auto shape : shapesContainer)
  {
    if (isRectangle(shape))
    {
      count++;
    }
  }
  return count;
}

int getSquareCount(const ShapesContainer& shapesContainer)
{
  int count = 0;
  for (auto shape : shapesContainer)
  {
    if (isSquare(shape))
    {
      count++;
    }
  }
  return count;
}

void deletePentagons(ShapesContainer& shapesContainer)
{
  bool found = false;
  ShapesContainer::iterator trash;

  for (auto shape = shapesContainer.begin(); shape != shapesContainer.end(); shape++)
  {
    if (found)
    {
      shapesContainer.erase(trash);
      found = false;
    }
    if (shape->size() == PENTAGON_VERTICES)
    {
      found = true;
      trash = shape;
    }
  }

  if (found)
  {
    shapesContainer.erase(trash);
  }
}

Shape getFirstPoints(const ShapesContainer& shapesContainer)
{
  Shape firstPoints;
  for (auto shape : shapesContainer)
  {
    firstPoints.push_back(shape[0]);
  }
  
  return firstPoints;
}

void sortShapesContainer(ShapesContainer& shapesContainer)
{
  bool found = false;
  ShapesContainer::iterator trash;

  ShapesContainer triangles;
  ShapesContainer rectangles;
  ShapesContainer squares;

  for (auto shape = shapesContainer.begin(); shape != shapesContainer.end(); shape++)
  {
    if (found)
    {
      shapesContainer.erase(trash);
      found = false;
    }
    if (shape->size() == TRIANGLE_VERTICES)
    {
      found = true;
      trash = shape;
      triangles.push_back(*shape);
      continue;
    }
    if (isSquare(*shape))
    {
      found = true;
      trash = shape;
      squares.push_back(*shape);
      continue;
    }
    if (isRectangle(*shape))
    {
      found = true;
      trash = shape;
      rectangles.push_back(*shape);
    }
  }

  if (found)
  {
    shapesContainer.erase(trash);
  }

  shapesContainer.splice(shapesContainer.begin(), rectangles);
  shapesContainer.splice(shapesContainer.begin(), squares);
  shapesContainer.splice(shapesContainer.begin(), triangles);
}
