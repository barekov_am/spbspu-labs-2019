#include "matrix.hpp"
#include <stdexcept>
#include <iostream>

//ROW

khismatullin::Matrix::Row::Row():
  size_(0)
{

}

khismatullin::Matrix::Row::Row(const Row& rhs):
  shapes_(copyShapes(rhs)),
  size_(rhs.size_)
{

}

khismatullin::Matrix::Row::Row(Row&& rhs):
  shapes_(std::move(rhs.shapes_)),
  size_(rhs.size_)
{
  rhs.size_ = 0;
}

khismatullin::Matrix::Row& khismatullin::Matrix::Row::operator =(const Row& rhs)
{
  if (this != &rhs)
  {
    shapes_ = copyShapes(rhs);
    size_ = rhs.size_;
  }

  return *this;
}

khismatullin::Matrix::Row& khismatullin::Matrix::Row::operator =(Row&& rhs)
{
  if (this != &rhs)
  {
    shapes_ = std::move(rhs.shapes_);
    size_ = rhs.size_;
    rhs.size_ = 0;
  }

  return *this;
}

khismatullin::shape_ptr khismatullin::Matrix::Row::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Index out of range");
  }

  return shapes_[index];
}

void khismatullin::Matrix::Row::add(khismatullin::shape_ptr shape)
{
  shapes_array shapesTmp(std::make_unique<shape_ptr[]>(size_ + 1));
  for (size_t i = 0; i < size_; i++)
  {
    shapesTmp[i] = shapes_[i];
  }
  shapesTmp[size_] = shape;
  shapes_ = std::move(shapesTmp);
  size_++;
}

size_t khismatullin::Matrix::Row::getSize() const
{
  return size_;
}

khismatullin::shapes_array khismatullin::Matrix::Row::copyShapes(const khismatullin::Matrix::Row& row)
{
  shapes_array shapes = std::make_unique<shape_ptr[]>(row.size_);
  for (size_t i = 0; i < row.size_; i++)
  {
    shapes[i] = row.shapes_[i];
  }
  return shapes;
}

//MATRIX

khismatullin::Matrix::Matrix() :
  rows_(0),
  columns_(0)
{

}

khismatullin::Matrix::Matrix(const Matrix& rhs) :
  list_(copyShapes(rhs)),
  rows_(rhs.rows_),
  columns_(rhs.columns_)
{

}

khismatullin::Matrix::Matrix(Matrix&& rhs) :
  list_(std::move(rhs.list_)),
  rows_(rhs.rows_),
  columns_(rhs.columns_)
{
  rhs.rows_ = 0;
  rhs.columns_ = 0;
}

khismatullin::Matrix& khismatullin::Matrix::operator =(const Matrix& rhs)
{
  if (this != &rhs)
  {
    list_ = copyShapes(rhs);
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
  }

  return *this;
}

khismatullin::Matrix& khismatullin::Matrix::operator =(Matrix&& rhs)
{
  if (this != &rhs)
  {
    list_ = std::move(rhs.list_);
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
    rhs.rows_ = 0;
    rhs.columns_ = 0;
  }

  return *this;
}

khismatullin::Matrix::Row khismatullin::Matrix::operator [](size_t index) const
{
  if (index >= rows_)
  {
    throw std::out_of_range("Index out of range");
  }

  khismatullin::Matrix::Row row;

  for (size_t i = 0; i < columns_; i++)
  {
    if (list_[index * columns_ + i] != nullptr)
    {
      row.add(list_[index * columns_ + i]);
    }
  }

  return row;
}

bool khismatullin::Matrix::operator ==(const Matrix& rhs) const
{
  if ((rows_ != rhs.rows_) || (columns_ != rhs.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (list_[i] != rhs.list_[i])
    {
      return false;
    }
  }

  return true;
}

bool khismatullin::Matrix::operator !=(const Matrix& rhs) const
{
  return !(*this == rhs);
}


void khismatullin::Matrix::show() const
{
  if ((columns_ == 0) && (rows_ == 0))
  {
    std::cout << "[Matrix]: empty\n";
  }
  else
  {
    std::cout << "[Matrix]:\n";
    for (size_t i = 0; i < rows_; i++)
    {
      std::cout << i << " row:\n";
      for (size_t j = 0; j < columns_; j++)
      {
        shape_ptr shape = list_[i * columns_ + j];
        if (shape != nullptr)
        {
          const point_t pos = shape->getCenter();
          std::cout << "   " << j << ") " << shape->getShapeName() << " (" << pos.x << ";" << pos.y << ")\n";
        }
      }
    }
  }
}

void khismatullin::Matrix::add(const shape_ptr & shape, size_t row, size_t column)
{
  if (shape == nullptr)
  {
    throw std::logic_error("Shape is empty");
  }

  size_t rowsTmp = (row == rows_) ? (rows_ + 1) : (rows_);
  size_t columnsTmp = (column == columns_) ? (columns_ + 1) : (columns_);

  shapes_array shapesTmp(std::make_unique<shape_ptr[]>(rowsTmp * columnsTmp));

  for (size_t i = 0; i < rowsTmp; i++)
  {
    for (size_t j = 0; j < columnsTmp; j++)
    {
      if ((i == rows_) || (j == columns_))
      {
        shapesTmp[i * columnsTmp + j] = nullptr;
      }
      else
      {
        shapesTmp[i * columnsTmp + j] = list_[i * columns_ + j];
      }
    }
  }
  shapesTmp[row * columnsTmp + column] = shape;
  list_ = std::move(shapesTmp);
  rows_ = rowsTmp;
  columns_ = columnsTmp;
}

size_t khismatullin::Matrix::getRows() const
{
  return rows_;
}

size_t khismatullin::Matrix::getColumns() const
{
  return columns_;
}

khismatullin::shapes_array khismatullin::Matrix::copyShapes(const khismatullin::Matrix& matrix)
{
  size_t count = matrix.rows_ * matrix.columns_;
  shapes_array shapes = std::make_unique<shape_ptr[]>(count);
  for (size_t i = 0; i < count; i++) {
    shapes[i] = matrix.list_[i];
  }
  return shapes;
}

khismatullin::Matrix khismatullin::partition(const shapes_array & arr, size_t size)
{
  if (arr == nullptr)
  {
    throw std::logic_error("Shapes array is empty");
  }

  Matrix matrix;

  for (size_t i = 0; i < size; i++)
  {
    size_t rightRow = 0;
    size_t rightColumn = 0;
    for (size_t j = 0; j < matrix.getRows(); j++)
    {
      for (size_t k = 0; k < matrix.getColumns(); k++)
      {
        if (matrix[j][k] == nullptr)
        {
          rightRow = j;
          rightColumn = k;
          break;
        }

        if (intersection(arr[i]->getFrameRect(), matrix[j][k]->getFrameRect()))
        {
          rightRow = j + 1;
          rightColumn = 0;
          break;
        }
        else
        {
          rightRow = j;
          rightColumn = k + 1;
        }
      }

      if (rightRow == j)
      {
        break;
      }
    }

    matrix.add(arr[i], rightRow, rightColumn);
  }

  return matrix;
}

khismatullin::Matrix khismatullin::partition(const CompositeShape& composite)
{
  return partition(composite.getShapes(), composite.getCount());
}


bool khismatullin::intersection(const rectangle_t& rect1, const rectangle_t& rect2)
{
  const point_t rect1TL = {rect1.pos.x - rect1.width / 2, rect1.pos.y + rect1.height / 2};
  const point_t rect1BR = {rect1.pos.x + rect1.width / 2, rect1.pos.y - rect1.height / 2};
  const point_t rect2TL = {rect2.pos.x - rect2.width / 2, rect2.pos.y + rect2.height / 2};
  const point_t rect2BR = {rect2.pos.x + rect2.width / 2, rect2.pos.y - rect2.height / 2};

  return ((rect1TL.x < rect2BR.x) && (rect2BR.y < rect1TL.y)) && ((rect2TL.x < rect1BR.x) && (rect1BR.y < rect2TL.y));
}
