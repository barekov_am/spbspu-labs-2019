#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "composite-shape.hpp"

namespace khismatullin
{
  class Matrix
  {
  public:
    class Row
    {
    public:
      Row();

      Row(const Row& rhs);
      Row(Row&& rhs);

      Row& operator =(const Row& rhs);
      Row& operator =(Row&& rhs);
      shape_ptr operator [](size_t index) const;

      void add(shape_ptr shape);
      size_t getSize() const;
    private:
      shapes_array shapes_;
      size_t size_;
      shapes_array copyShapes(const Row& row);
    };

    Matrix();
    Matrix(const Matrix& rhs);
    Matrix(Matrix&& rhs);
    ~Matrix() = default;

    Matrix& operator =(const Matrix& rhs);
    Matrix& operator =(Matrix&& rhs);

    Row operator [](size_t index) const;
    bool operator ==(const Matrix& rhs) const;
    bool operator !=(const Matrix& rhs) const;

    void show() const;
    void add(const shape_ptr & shape, size_t row, size_t column);

    size_t getRows() const;
    size_t getColumns() const;

  private:
    shapes_array list_;
    size_t rows_;
    size_t columns_;
    shapes_array copyShapes(const Matrix& matrix);
  };

  Matrix partition(const shapes_array & arr, size_t size);
  Matrix partition(const CompositeShape& composite);
  bool intersection(const rectangle_t&, const rectangle_t&);
}

#endif
