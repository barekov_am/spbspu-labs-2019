#include "composite-shape.hpp"
#include <algorithm>
#include <exception>
#include <math.h>
#include <iostream>

const double FullRotation = 360;

khismatullin::CompositeShape::CompositeShape() :
  count_(0)
{

}

khismatullin::CompositeShape::CompositeShape(const khismatullin::CompositeShape& rhs) :
  shapes_(copyShapes(rhs)),
  count_(rhs.count_)
{

}

khismatullin::CompositeShape::CompositeShape(khismatullin::CompositeShape&& rhs) :
  shapes_(std::move(rhs.shapes_)),
  count_(rhs.count_)
{
  rhs.count_ = 0;
}

khismatullin::CompositeShape::CompositeShape(shape_ptr shape) :
  shapes_(std::make_unique<shape_ptr[]>(1)),
  count_(1)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("You can't create composite shape with nullptr");
  }

  shapes_[0] = shape;
}

khismatullin::CompositeShape& khismatullin::CompositeShape::operator =(const khismatullin::CompositeShape& rhs)
{
  if (this != &rhs)
  {
    shapes_ = copyShapes(rhs);
    count_ = rhs.count_;
  }
  return *this;
}

khismatullin::CompositeShape& khismatullin::CompositeShape::operator =(khismatullin::CompositeShape&& rhs)
{
  if (this != &rhs)
  {
    shapes_ = std::move(rhs.shapes_);
    count_ = rhs.count_;
    rhs.count_ = 0;
  }
  return *this;
}

khismatullin::shape_ptr khismatullin::CompositeShape::operator [](const size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Incorrect index of shape");
  }
  return shapes_[index];
}

double khismatullin::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

khismatullin::rectangle_t khismatullin::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty");
  }

  rectangle_t rectTmp = shapes_[0]->getFrameRect();
  double minX = rectTmp.pos.x - rectTmp.width / 2;
  double minY = rectTmp.pos.y - rectTmp.height / 2;
  double maxX = rectTmp.pos.x + rectTmp.width / 2;
  double maxY = rectTmp.pos.y + rectTmp.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    rectTmp = shapes_[i]->getFrameRect();

    minX = std::min((rectTmp.pos.x - rectTmp.width / 2), minX);
    minY = std::min((rectTmp.pos.y - rectTmp.height / 2), minY);
    maxX = std::max((rectTmp.pos.x + rectTmp.width / 2), maxX);
    maxY = std::max((rectTmp.pos.y + rectTmp.height / 2), maxY);
  }

  return  rectangle_t{point_t{(maxX + minX) / 2, (maxY + minY) / 2}, maxX - minX, maxY - minY};
}

void khismatullin::CompositeShape::move(const double dx, const double dy)
{
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void khismatullin::CompositeShape::move(const point_t &point)
{
  point_t position = getCenter();
  move(point.x - position.x, point.y - position.y);
}

khismatullin::point_t khismatullin::CompositeShape::getCenter() const
{
  return getFrameRect().pos;
}

void khismatullin::CompositeShape::show() const
{
  rectangle_t rectTmp = getFrameRect();
  std::cout << "[CompositeShape]: \n";
  std::cout << "Center : (" << rectTmp.pos.x << ", " << rectTmp.pos.y << ")\n";
  std::cout << "Height : " << rectTmp.height << "\n";
  std::cout << "Width  : " << rectTmp.width << "\n";
  std::cout << "Count  : " << count_ << "\n";
  std::cout << "Area   : " << getArea() << "\n";
}

void khismatullin::CompositeShape::scale(const double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("Multiplier of scale should be > 0");
  }
  if (count_ == 0)
  {
    throw std::logic_error("You can't scale empty complex shape");
  }

  point_t point = getCenter();
  for (size_t i = 0; i < count_; i++)
  {
    point_t currentPosition = shapes_[i]->getCenter();
    double dx = currentPosition.x - point.x;
    double dy = currentPosition.y - point.y;
    shapes_[i]->move({point.x + (dx * multiplier), point.y + (dy * multiplier)});
    shapes_[i]->scale(multiplier);
  }
}

void khismatullin::CompositeShape::showShapes() const
{
  std::cout << "-----------------------------\n";
  if (count_ == 0)
  {
    std::cout << "Complex shape is empty\n";
  }
  for (size_t i = 0; i < count_; i++)
  {
    std::cout << i + 1 << ". ";
    shapes_[i]->show();
  }
  std::cout << "-----------------------------\n";

}

void khismatullin::CompositeShape::add(shape_ptr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Adding shape shouldn't be nullptr");
  }

  bool mentioned = false;

  for (size_t i = 0; i < count_; i++)
  {
    if (shapes_[i].get() == shape.get())
    {
      mentioned = true;
    }
  }

  if (!mentioned)
  {
    shapes_array shapesTmp = std::make_unique<shape_ptr[]>(count_ + 1);
    for (size_t i = 0; i < count_; i++)
    {
      shapesTmp[i] = shapes_[i];
    }
    shapesTmp[count_] = shape;
    shapes_ = std::move(shapesTmp);
    count_++;
  }
}

void khismatullin::CompositeShape::remove(size_t n)
{
  if (n >= count_)
  {
    throw std::invalid_argument("Not correct number of shape");
  }

  for (size_t i = n; i < count_ - 1; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  shapes_[count_ - 1] = nullptr;
  count_--;
}

void khismatullin::CompositeShape::remove(shape_ptr shape)
{
  bool detected = false;
  size_t j = 0;
  for (size_t i = 0; i < count_; i++)
  {
    if (shapes_[i].get() == shape.get())
    {
      detected = true;
      continue;
    }
    shapes_[j] = shapes_[i];
    j++;
  }
  if (detected)
  {
    shapes_[count_ - 1] = nullptr;
    count_--;
  }
}

size_t khismatullin::CompositeShape::getCount() const
{
  return count_;
}

void khismatullin::CompositeShape::rotate(double angle)
{
  point_t center = getCenter();
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->rotate(angle);

    point_t shapePos = shapes_[i]->getCenter();
    double currentX = shapePos.x - center.x;
    double currentY = shapePos.y - center.y;

    double sinus = std::sin(angle * M_PI / 180);
    double cosin = std::cos(angle * M_PI / 180);

    double dX = currentX * (cosin - 1) - currentY * sinus;
    double dY = currentX * sinus + currentY * (cosin - 1);

    shapes_[i]->move(dX, dY);
  }
}

khismatullin::shapes_array khismatullin::CompositeShape::getShapes() const
{
  shapes_array shapesTmp(std::make_unique<shape_ptr[]>(count_));
  for (size_t i = 0; i < count_; i++)
  {
    shapesTmp[i] = shapes_[i];
  }

  return shapesTmp;
}

const char * khismatullin::CompositeShape::getShapeName() const
{
  return "CompositeShape";
}

khismatullin::shapes_array khismatullin::CompositeShape::copyShapes(const khismatullin::CompositeShape& composite)
{
  shapes_array shapes = std::make_unique<shape_ptr[]>(composite.count_);
  for (size_t i = 0; i < composite.count_; i++) {
    shapes[i] = composite.shapes_[i];
  }
  return shapes;
}
