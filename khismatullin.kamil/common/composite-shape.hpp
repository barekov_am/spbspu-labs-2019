#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include "shape.hpp"

namespace khismatullin
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape&);
    CompositeShape(CompositeShape&&);
    CompositeShape(shape_ptr shape);
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape&);
    CompositeShape& operator =(CompositeShape&&);
    shape_ptr operator [](const size_t index) const;

    double getArea() const;
    rectangle_t getFrameRect() const;
    void move(const double dx, const double dy);
    void move(const point_t &point);
    point_t getCenter() const;
    void show() const;
    void scale(const double multiplier);
    void showShapes() const;
    void add(shape_ptr shape);
    void remove(size_t n);
    void remove(shape_ptr shape);
    size_t getCount() const;
    void rotate(double angle);
    shapes_array getShapes() const;
    const char * getShapeName() const;

  private:
    shapes_array shapes_;
    size_t count_;
    shapes_array copyShapes(const CompositeShape& composite);
  };
}
#endif
