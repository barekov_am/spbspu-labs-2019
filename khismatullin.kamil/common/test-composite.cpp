#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(A3TestSuite)

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_CASE(compositeConstantcyAfterMoving)
{
  khismatullin::Circle testCircle({1, 4}, 3);
  khismatullin::Rectangle testRectangle({2 ,1}, 1, 2);
  khismatullin::CompositeShape testComposite(std::make_shared<khismatullin::Circle>(testCircle));
  testComposite.add(std::make_shared<khismatullin::Rectangle>(testRectangle));
  const khismatullin::rectangle_t InitialFrame = testComposite.getFrameRect();
  const double InitialArea = testComposite.getArea();

  testComposite.move({2, -1});
  khismatullin::rectangle_t CurrentFrame = testComposite.getFrameRect();
  double CurrentArea = testComposite.getArea();
  BOOST_CHECK_CLOSE(InitialFrame.width, CurrentFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(InitialFrame.height, CurrentFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(InitialArea, CurrentArea, ACCURACY);

  testComposite.move(1, 4);
  CurrentFrame = testComposite.getFrameRect();
  CurrentArea = testComposite.getArea();
  BOOST_CHECK_CLOSE(InitialFrame.width, CurrentFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(InitialFrame.height, CurrentFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(InitialArea, CurrentArea, ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeScaling)
{
  khismatullin::Circle testCircle({1, 4}, 3);
  khismatullin::Rectangle testRectangle({2 ,1}, 1, 2);
  khismatullin::CompositeShape testComposite;
  testComposite.add(std::make_shared<khismatullin::Circle>(testCircle));
  testComposite.add(std::make_shared<khismatullin::Rectangle>(testRectangle));
  const double InitialArea = testComposite.getArea();

  const double ScaleMultiplier = 2.5;
  testComposite.scale(ScaleMultiplier);
  double CurrentArea = testComposite.getArea();
  BOOST_CHECK_CLOSE(InitialArea * ScaleMultiplier * ScaleMultiplier, CurrentArea, ACCURACY);
}

BOOST_AUTO_TEST_CASE(compositeRotate)
{
  khismatullin::Circle circle({1, 3}, 3);
  khismatullin::Rectangle rectangle({2, 0}, 2, 5);
  khismatullin::CompositeShape composite(std::make_shared<khismatullin::Circle>(circle));
  composite.add(std::make_shared<khismatullin::Rectangle>(rectangle));

  const double compositeInitialArea = composite.getArea();
  const khismatullin::rectangle_t compositeInitialFrame = composite.getFrameRect();

  const double angle = 90;
  composite.rotate(angle);

  const double compositeCurrentArea = composite.getArea();
  const khismatullin::rectangle_t compositeCurrentFrame = composite.getFrameRect();

  BOOST_CHECK_CLOSE(compositeInitialArea, compositeCurrentArea, ACCURACY);
  BOOST_CHECK_CLOSE(compositeInitialFrame.pos.x, compositeCurrentFrame.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(compositeInitialFrame.pos.y, compositeCurrentFrame.pos.y, ACCURACY);

  BOOST_CHECK_CLOSE(compositeInitialFrame.height, compositeCurrentFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(compositeInitialFrame.width, compositeCurrentFrame.height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(testCopyMoveCompositeShape)
{
  khismatullin::CompositeShape testComposite1;
  khismatullin::Rectangle rectangle({1.6, 2.5}, 5.2, 6.1);
  khismatullin::Circle circle({7.77, 1.45}, 1.1);

  testComposite1.add(std::make_shared<khismatullin::Rectangle>(rectangle));
  testComposite1.add(std::make_shared<khismatullin::Circle>(circle));

  khismatullin::CompositeShape testComposite2;

  BOOST_CHECK_NO_THROW(khismatullin::CompositeShape copyComposite(testComposite1));
  BOOST_CHECK_NO_THROW(testComposite2 = testComposite1);
  BOOST_CHECK_NO_THROW(khismatullin::CompositeShape moveComposite(std::move(testComposite1)));
  testComposite2.remove(0);
  BOOST_CHECK_NO_THROW(testComposite1 = std::move(testComposite2));
}

BOOST_AUTO_TEST_CASE(throwingExceptionsEmptyCompositeShape)
{
  khismatullin::CompositeShape emptyComposite;

  BOOST_CHECK_THROW(emptyComposite.remove(0), std::invalid_argument);
  BOOST_CHECK_THROW(emptyComposite.scale(3.3), std::logic_error);
  BOOST_CHECK_THROW(emptyComposite.getCenter(), std::logic_error);
  BOOST_CHECK_THROW(emptyComposite.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(emptyComposite.show(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(throwingExceptions)
{
  khismatullin::Circle testCircle({1, 4}, 3);
  khismatullin::Rectangle testRectangle({2 ,1}, 1, 2);

  khismatullin::shape_ptr circle = std::make_shared<khismatullin::Circle>(testCircle);

  khismatullin::CompositeShape testComposite(circle);
  testComposite.add(std::make_shared<khismatullin::Rectangle>(testRectangle));

  khismatullin::shape_ptr nullShape = nullptr;

  BOOST_CHECK_THROW(khismatullin::CompositeShape composite(nullShape), std::invalid_argument);
  BOOST_CHECK_THROW(testComposite.add(nullShape), std::invalid_argument);

  BOOST_CHECK_THROW(testComposite[10], std::out_of_range);
  BOOST_CHECK_THROW(testComposite[-1], std::out_of_range);

  testComposite.remove(circle);
  testComposite.remove(0);

  BOOST_CHECK_THROW(testComposite.remove(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
