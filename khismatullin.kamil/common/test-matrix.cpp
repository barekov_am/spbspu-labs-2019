#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

BOOST_AUTO_TEST_SUITE(A4TestSuite)

BOOST_AUTO_TEST_CASE(rowTest)
{
  khismatullin::Rectangle shape1({0, 0}, 2, 4);
  khismatullin::Circle shape2({0, 0}, 6);
  khismatullin::shape_ptr rectangle = std::make_shared<khismatullin::Rectangle>(shape1);
  khismatullin::shape_ptr circle = std::make_shared<khismatullin::Circle>(shape2);

  //Adding

  khismatullin::Matrix::Row testRow1;

  BOOST_CHECK_NO_THROW(testRow1.add(rectangle));
  BOOST_CHECK_EQUAL(testRow1.getSize(), 1);
  BOOST_CHECK_NO_THROW(testRow1.add(circle));
  BOOST_CHECK_EQUAL(testRow1.getSize(), 2);
  BOOST_CHECK(testRow1[0] == rectangle);
  BOOST_CHECK(testRow1[1] == circle);

  //CopyAndMove

  khismatullin::Matrix::Row testRow2;
  testRow2.add(circle);

  BOOST_CHECK_NO_THROW(khismatullin::Matrix::Row copyRow(testRow1));
  BOOST_CHECK_NO_THROW(khismatullin::Matrix::Row moveRow(std::move(testRow1)));
  BOOST_CHECK_NO_THROW(testRow1 = testRow2);
  BOOST_CHECK_EQUAL(testRow1.getSize(), 1);
  testRow2.add(rectangle);
  BOOST_CHECK_EQUAL(testRow2.getSize(), 2);
  BOOST_CHECK_NO_THROW(testRow2 = std::move(testRow1));
  BOOST_CHECK_EQUAL(testRow2.getSize(), 1);
}

BOOST_AUTO_TEST_CASE(correctPartition)
{
  khismatullin::Circle circle({-3, 5}, 4);
  khismatullin::Rectangle rectangle({0, 3}, 2, 5);
  khismatullin::Rectangle rectangle1({-5, -1}, 3, 3);
  khismatullin::Rectangle rectangle2({-6, 5}, 1, 3);
  khismatullin::Rectangle rectangle3({-1, 1}, 2, 2);

  khismatullin::CompositeShape composite;
  composite.add(std::make_shared<khismatullin::Rectangle>(rectangle2));
  composite.add(std::make_shared<khismatullin::Rectangle>(rectangle3));

  khismatullin::shape_ptr testCircle = std::make_shared<khismatullin::Circle>(circle);
  khismatullin::shape_ptr testComposite = std::make_shared<khismatullin::CompositeShape>(composite);
  khismatullin::shape_ptr testRectangle = std::make_shared<khismatullin::Rectangle>(rectangle);
  khismatullin::shape_ptr testRectangle1 = std::make_shared<khismatullin::Rectangle>(rectangle1);


  khismatullin::CompositeShape testComposite1;
  testComposite1.add(testCircle);
  testComposite1.add(testComposite);
  testComposite1.add(testRectangle);
  testComposite1.add(testRectangle1);

  khismatullin::Matrix testMatrix = khismatullin::partition(testComposite1);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 3);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 2);

  BOOST_CHECK(testMatrix[0][0] == testCircle);
  BOOST_CHECK(testMatrix[0][1] == testRectangle1);
  BOOST_CHECK(testMatrix[1][0] == testComposite);
  BOOST_CHECK(testMatrix[2][0] == testRectangle);
}

BOOST_AUTO_TEST_CASE(copyAndMove)
{
  khismatullin::Circle circle({2, 3}, 1);
  khismatullin::Rectangle rectangle({2, 2}, 2, 4);
  khismatullin::CompositeShape testComposite;
  testComposite.add(std::make_shared<khismatullin::Circle>(circle));
  testComposite.add(std::make_shared<khismatullin::Rectangle>(rectangle));

  khismatullin::Matrix testMatrix = khismatullin::partition(testComposite);

  BOOST_CHECK_NO_THROW(khismatullin::Matrix newMatrix(testMatrix));
  BOOST_CHECK_NO_THROW(khismatullin::Matrix newMatrix1(std::move(testMatrix)));

  khismatullin::Matrix testMatrix1 = khismatullin::partition(testComposite);
  khismatullin::Matrix testMatrix2;

  BOOST_CHECK_NO_THROW(testMatrix2 = testMatrix1);
  BOOST_CHECK_NO_THROW(testMatrix2 = std::move(testMatrix1));

  khismatullin::Matrix testMatrix3;

  testMatrix3 = testMatrix2;
  BOOST_CHECK(testMatrix3 == testMatrix2);
  testMatrix = testMatrix2;
  testMatrix3 = std::move(testMatrix2);
  BOOST_CHECK(testMatrix3 == testMatrix);

  khismatullin::Matrix testMatrix4(testMatrix3);
  BOOST_CHECK(testMatrix4 == testMatrix3);
  khismatullin::Matrix testMatrix5(std::move(testMatrix3));
  BOOST_CHECK(testMatrix5 == testMatrix4);
}

BOOST_AUTO_TEST_CASE(exceptionThrow)
{
  khismatullin::Circle circle({1, 1}, 2);
  khismatullin::Rectangle rectangle({3, 2}, 2, 3);

  khismatullin::shape_ptr testCircle = std::make_shared<khismatullin::Circle>(circle);
  khismatullin::shape_ptr testRectangle = std::make_shared<khismatullin::Rectangle>(rectangle);

  khismatullin::CompositeShape testComposite;
  testComposite.add(testCircle);
  testComposite.add(testRectangle);

  khismatullin::Matrix testMatrix = khismatullin::partition(testComposite);

  BOOST_CHECK_THROW(testMatrix[2], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-5], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
