#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(A2RectangleTestSuite)

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_CASE(rectangleConstancyAfterMoving)
{
  khismatullin::Rectangle testRectangle({2, 4}, 7, 10);
  const khismatullin::rectangle_t InitialFrame = testRectangle.getFrameRect();
  const double InitialArea = testRectangle.getArea();

  testRectangle.move({-3.2, 3.3});
  khismatullin::rectangle_t CurrentFrame = testRectangle.getFrameRect();
  double CurrentArea = testRectangle.getArea();
  BOOST_CHECK_CLOSE(InitialFrame.width, CurrentFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(InitialFrame.height, CurrentFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(InitialArea, CurrentArea, ACCURACY);

  testRectangle.move(4.3, 2.2);
  CurrentFrame = testRectangle.getFrameRect();
  CurrentArea = testRectangle.getArea();
  BOOST_CHECK_CLOSE(InitialFrame.width, CurrentFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(InitialFrame.height, CurrentFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(InitialArea, CurrentArea, ACCURACY);
}

BOOST_AUTO_TEST_CASE(rectangleScaling)
{
  khismatullin::Rectangle testRectangle({4, 2}, 10, 7);
  const double InitialArea = testRectangle.getArea();

  const double ScaleMultiplier = 3.3;
  testRectangle.scale(ScaleMultiplier);
  double CurrentArea = testRectangle.getArea();
  BOOST_CHECK_CLOSE(InitialArea * ScaleMultiplier * ScaleMultiplier, CurrentArea, ACCURACY);
}

BOOST_AUTO_TEST_CASE(rectangleRotation)
{
  khismatullin::Rectangle testRectangle({4, 2}, 10, 7);
  const double InitialArea = testRectangle.getArea();
  const khismatullin::rectangle_t InitialFrame = testRectangle.getFrameRect();

  const double angle = 90;
  testRectangle.rotate(angle);

  double CurrentArea = testRectangle.getArea();
  const khismatullin::rectangle_t CurrentFrame = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(InitialArea, CurrentArea, ACCURACY);
  BOOST_CHECK_CLOSE(InitialFrame.pos.x, CurrentFrame.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(InitialFrame.pos.y, CurrentFrame.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(InitialFrame.height, CurrentFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(InitialFrame.width, CurrentFrame.height, ACCURACY);
}

BOOST_AUTO_TEST_CASE(throwingExceptions)
{
  BOOST_CHECK_THROW(khismatullin::Rectangle({4, 9}, -7, 3), std::invalid_argument);
  BOOST_CHECK_THROW(khismatullin::Rectangle({4, 9}, 7, -3), std::invalid_argument);
  BOOST_CHECK_THROW(khismatullin::Rectangle({4, 9}, -7, -3), std::invalid_argument);

  khismatullin::Rectangle testRectangle({15, 25}, 5, 6);
  BOOST_CHECK_THROW(testRectangle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
