#include "rectangle.hpp"
#include <iostream>
#include <math.h>

const double FullRotation = 360;

khismatullin::Rectangle::Rectangle(const point_t &pos, const double height, const double width) :
  center_(pos),
  height_(height),
  width_(width),
  angle_(0)
{
  if (height <= 0)
  {
    throw std::invalid_argument("Height should be > 0");
  }
  if (width <= 0)
  {
    throw std::invalid_argument("Width should be > 0");
  }
}

double khismatullin::Rectangle::getArea() const
{
  return height_ * width_;
}

khismatullin::rectangle_t khismatullin::Rectangle::getFrameRect() const
{
  double cosin = fabs(std::cos(angle_ * M_PI / 180));
  double sinus = fabs(std::sin(angle_ * M_PI / 180));
  return rectangle_t {center_, (height_ * sinus + width_ * cosin), (height_ * cosin + width_ * sinus)};
}

void khismatullin::Rectangle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void khismatullin::Rectangle::move(const point_t &point)
{
  center_ = point;
}

khismatullin::point_t khismatullin::Rectangle::getCenter() const
{
  return center_;
}

void khismatullin::Rectangle::show() const
{
  rectangle_t frame = getFrameRect();
  std::cout << "[Rectangle]: \n";
  std::cout << "Center    : (" << center_.x << ", " << center_.y << ")\n";
  std::cout << "Height    : " << height_ << "\n";
  std::cout << "Width     : " << width_ << "\n";
  std::cout << "Area      : " << getArea() << "\n";
  std::cout << "Angle     : " << angle_ << " grad \n";
  std::cout << "Frame HxW : " << frame.height << " x " << frame.width << "\n";
}

void khismatullin::Rectangle::scale(const double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("Multiplier of scale should be > 0");
  }
  height_ *= multiplier;
  width_ *= multiplier;
}

void khismatullin::Rectangle::rotate(double angle)
{
  angle_ += angle;
  angle_ = (angle_ >= 0) ? fmod(angle_, FullRotation) : (FullRotation + fmod(angle_, FullRotation));
}

const char * khismatullin::Rectangle::getShapeName() const
{
  return "Rectangle";
}
