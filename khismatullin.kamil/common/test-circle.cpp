#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(A2CircleTestSuite)

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_CASE(circleConstancyAfterMoving)
{
  khismatullin::Circle testCircle({2, 4}, 5);
  const khismatullin::rectangle_t InitialFrame = testCircle.getFrameRect();
  const double InitialArea = testCircle.getArea();

  testCircle.move({3.2, 3.5});
  khismatullin::rectangle_t CurrentFrame = testCircle.getFrameRect();
  double CurrentArea = testCircle.getArea();
  BOOST_CHECK_CLOSE(InitialFrame.width, CurrentFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(InitialFrame.height, CurrentFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(InitialArea, CurrentArea, ACCURACY);

  testCircle.move(3.8, 3.5);
  CurrentFrame = testCircle.getFrameRect();
  CurrentArea = testCircle.getArea();
  BOOST_CHECK_CLOSE(InitialFrame.width, CurrentFrame.width, ACCURACY);
  BOOST_CHECK_CLOSE(InitialFrame.height, CurrentFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(InitialArea, CurrentArea, ACCURACY);
}

BOOST_AUTO_TEST_CASE(circleScaling)
{
  khismatullin::Circle testCircle({5, 4}, 2);
  const double InitialArea = testCircle.getArea();

  const double ScaleMultiplier = 2.5;
  testCircle.scale(ScaleMultiplier);
  double CurrentArea = testCircle.getArea();
  BOOST_CHECK_CLOSE(InitialArea * ScaleMultiplier * ScaleMultiplier, CurrentArea, ACCURACY);
}

BOOST_AUTO_TEST_CASE(circleRotation)
{
  khismatullin::Circle testCircle({5, 4}, 2);
  const double InitialArea = testCircle.getArea();
  const khismatullin::rectangle_t InitialFrame = testCircle.getFrameRect();

  const double angle = 77.7;
  testCircle.rotate(angle);
  const double CurrentArea = testCircle.getArea();
  const khismatullin::rectangle_t CurrentFrame = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(InitialArea, CurrentArea, ACCURACY);
  BOOST_CHECK_CLOSE(InitialFrame.pos.x, CurrentFrame.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(InitialFrame.pos.y, CurrentFrame.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(InitialFrame.height, CurrentFrame.height, ACCURACY);
  BOOST_CHECK_CLOSE(InitialFrame.width, CurrentFrame.width, ACCURACY);
}

BOOST_AUTO_TEST_CASE(throwingExceptions)
{
  BOOST_CHECK_THROW(khismatullin::Circle({5, 25}, -22.8), std::invalid_argument);

  khismatullin::Circle testCircle({15, 25}, 5.6);
  BOOST_CHECK_THROW(testCircle.scale(-6), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
