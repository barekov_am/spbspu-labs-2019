#include "VectorEditor.hpp"
#include <iostream>

int main()
{
  try {
    std::vector<DataStruct> vector;
    fill(vector);
    sort(vector);
    print(vector);
  } catch (std::exception &err) {
    std::cerr << err.what();
    return 1;
  }

  return 0;
}
