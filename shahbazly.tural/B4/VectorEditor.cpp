#include <iostream>
#include <iterator>
#include <algorithm>
#include "VectorEditor.hpp"

void fill(std::vector<DataStruct> &vector)
{
  std::string line, element;
  DataStruct data;

  while (std::getline(std::cin >> std::ws, line)) {
    if (line.find_first_of(",") != line.npos) {
      element.insert(element.begin(), line.begin(), line.begin() + line.find_first_of(",") + 1);
    } else {
      throw std::invalid_argument("Incorrect input.");
    }

    auto iterator = line.begin();
    line.erase(iterator, iterator + line.find_first_of(",") + 1);
    element.pop_back();

    data.key1 = std::stoi(element);
    if ((data.key1 < -5) || (data.key1 > 5)) {
      throw std::invalid_argument ("Incorrect input. Key1 must be between -5 and 5.");
    }
    element.erase();

    if (line.find_first_of(",") != line.npos) {
      element.insert(element.begin(), line.begin(), line.begin() + line.find_first_of(",") + 1);
    } else {
      throw std::invalid_argument("Incorrect input.");
    }

    iterator = line.begin();
    line.erase(iterator, iterator + line.find_first_of(",") + 1);
    element.pop_back();

    data.key2 = std::stoi(element);
    if ((data.key2 < -5) || (data.key2 > 5)) {
      throw std::invalid_argument("Incorrect input. Key2 must be between -5 and 5.");
    }
    element.erase();

    element.insert(element.begin(), line.begin(), line.end());
    if (element != "") {
      data.str = element;
    } else {
     throw std::invalid_argument("Incorrect input.");
    }
    element.erase();

    vector.push_back(data);
  }
}

bool cmp(const DataStruct &data1, const DataStruct &data2)
{
  if (data1.key1 == data2.key1) {
    if (data1.key2 == data2.key2) {
      return (data1.str.size() < data2.str.size());
    } else {
      return (data1.key2 < data2.key2);
    }
  } else {
    return (data1.key1 < data2.key1);
  }
}

void sort(std::vector<DataStruct> &vector)
{
 std::sort(vector.begin(), vector.end(), cmp);
}

void print(const std::vector<DataStruct> &vector)
{
  DataStruct data;

  for (auto i = vector.begin(); i < vector.end(); i++) {
    data = *i;
    std::cout << data.key1 << "," << data.key2 << "," << data.str << "\n";
  }
}
