#ifndef VECTOR_EDITOR_HPP
#define VECTOR_EDITOR_HPP
#include <string>
#include <vector>

struct DataStruct {
  int key1;
  int key2;
  std::string str;
};

  void fill(std::vector<DataStruct> &vector);
  void sort(std::vector<DataStruct> &vector);
  bool cmp(const DataStruct &data1, const DataStruct &data2);
  void print(const std::vector<DataStruct> &vector);

#endif
