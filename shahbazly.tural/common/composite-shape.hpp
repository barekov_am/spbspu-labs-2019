#ifndef COMPOSITE_SHAPE
#define COMPOSITE_SHAPE

#include <memory>

#include "shape.hpp"

namespace shahbazly
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape & rhs);
    CompositeShape(CompositeShape && rhs);
    CompositeShape(Shape::ptr shape);

    ~CompositeShape() = default;

    CompositeShape & operator=(const CompositeShape & rhs);
    CompositeShape & operator=(CompositeShape && rhs);
    Shape::ptr operator [](size_t index) const;

    size_t getCount() const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;

    void add(Shape::ptr shape);

    void scale(double scaleFactor) override;
    void rotate(double degrees) override;

    void move(const point_t & pos) override;
    void move(double dx, double dy) override;

    void remove(size_t index);
    void remove(Shape::ptr shape);

    void showParameters() const override;
  private:
    size_t m_count;
    Shape::arrayPtr m_figures;
  };
}

#endif
