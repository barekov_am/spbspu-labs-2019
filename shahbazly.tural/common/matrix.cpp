#include "matrix.hpp"

#include <iostream>

shahbazly::Matrix::Layer::Layer(shahbazly::Shape::ptr *arr, size_t size) :
  m_size(size),
  m_figures(std::make_unique<Shape::ptr[]>(size))
{
  for (size_t i = 0; i < m_size; i++)
  {
    m_figures[i] = arr[i];
  }
}

shahbazly::Shape::ptr shahbazly::Matrix::Layer::operator [](size_t index) const
{
  if (index >= m_size)
  {
    throw std::invalid_argument("Index is out of array");
  }

  return m_figures[index];
}

size_t shahbazly::Matrix::Layer::getSize() const
{
  return m_size;
}

shahbazly::Matrix::Matrix() :
  m_rows(0),
  numberOfFigures(0)
{ }

shahbazly::Matrix::Matrix(const shahbazly::Matrix & rhs) :
  m_rows(rhs.m_rows),
  m_columns(new size_t[rhs.m_rows]),
  m_figures(std::make_unique<Shape::ptr[]>(rhs.numberOfFigures)),
  numberOfFigures(rhs.numberOfFigures)
{
  for (size_t i = 0; i < numberOfFigures; i++)
  {
    m_figures[i] = rhs.m_figures[i];
  }

  for (size_t i = 1; i < m_rows; i++)
  {
    m_columns[i] = rhs.m_columns[i];
  }
}

shahbazly::Matrix::Matrix(shahbazly::Matrix &&  rhs) :
  m_rows(rhs.m_rows),
  m_columns(std::move(rhs.m_columns)),
  m_figures(std::move(rhs.m_figures)),
  numberOfFigures(rhs.numberOfFigures)
{ }

shahbazly::Matrix & shahbazly::Matrix::operator =(const shahbazly::Matrix & rhs)
{
  if (this != & rhs)
  {
    m_rows = rhs.m_rows;
    numberOfFigures = rhs.numberOfFigures;

    Shape::arrayPtr mFigures = std::make_unique<Shape::ptr[]>(rhs.numberOfFigures);
    for (size_t i = 0; i < numberOfFigures; i++)
    {
      mFigures[i] = rhs.m_figures[i];
    }
    m_figures.swap(mFigures);

    std::unique_ptr<size_t[]> tmpColumns = std::make_unique<size_t[]>(rhs.m_rows);
    for (size_t i = 1; i < m_rows; i++)
    {
      tmpColumns[i] = rhs.m_columns[i];
    }
    m_columns.swap(tmpColumns);
  }

  return *this;
}

shahbazly::Matrix & shahbazly::Matrix::operator =(shahbazly::Matrix &&  rhs)
{
  if (this != & rhs)
  {
    m_rows = rhs.m_rows;
    m_columns = std::move(rhs.m_columns);
    m_figures = std::move(rhs.m_figures);
    numberOfFigures = rhs.numberOfFigures;
  }

  return *this;
}

shahbazly::Matrix::Layer shahbazly::Matrix::operator [](size_t row) const
{
  if (row >= m_rows)
  {
    throw std::invalid_argument("Index is out of array");
  }

  size_t index = 0;
  for (size_t i = 0; i < row; i++)
  {
    index += m_columns[i];
  }

  return Layer(& m_figures[index], m_columns[row]);
}

void shahbazly::Matrix::add(Shape::ptr shape, size_t row)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Can't be empty");
  }

  if (row > m_rows)
  {
    throw std::invalid_argument("Argument must be less than or equal to the number of lines");
  }

  std::unique_ptr<Shape::ptr[]> mFigures = std::make_unique<Shape::ptr[]>(numberOfFigures + 1);

  size_t figureCounter = 0;
  size_t tmpCounter = 0;

  for (size_t tmpRow = 0; tmpRow < m_rows; tmpRow++)
  {
    for (size_t column = 0; column < m_columns[tmpRow]; column++)
    {
      mFigures[tmpCounter++] = m_figures[figureCounter++];
    }

    if (row == tmpRow)
    {
      mFigures[tmpCounter++] = shape;
      m_columns[tmpRow]++;
    }
  }

  if (row == m_rows)
  {
    std::unique_ptr<size_t[]> tmpColumns = std::make_unique<size_t[]>(m_rows + 1);

    for (size_t tmpRow = 0; tmpRow < m_rows; tmpRow++)
    {
      tmpColumns[tmpRow] = m_columns[tmpRow];
    }

    mFigures[tmpCounter] = shape;
    tmpColumns[m_rows] = 1;
    m_rows++;
    m_columns.swap(tmpColumns);
  }

  numberOfFigures++;
  m_figures.swap(mFigures);
}

size_t shahbazly::Matrix::getRows() const
{
  return m_rows;
}

size_t shahbazly::Matrix::getNumberOfFigures() const
{
  return numberOfFigures;
}

void shahbazly::Matrix::showParameters() const
{
  std::cout << "MATRIX" << std::endl;
  std::cout << "Number of rows: " << m_rows << std::endl;
  std::cout << "Number of figures: " << numberOfFigures << std::endl;
}
