#include "rectangle.hpp"

#include <iostream>
#include <stdexcept>
#include <cmath>

shahbazly::Rectangle::Rectangle(const point_t & pos, const double & width, const double & height):
  m_height(height),
  m_width(width),
  m_center(pos),
  m_corners{{pos.x - width / 2, pos.y + height / 2},
           {pos.x + width / 2, pos.y + height / 2},
           {pos.x + width / 2, pos.y - height / 2},
           {pos.x - width / 2, pos.y - height / 2}}
{
  if ((m_width <= 0.0) || (m_height <= 0.0)) {
    throw std::invalid_argument("Width and height must be greater than zero.");
  }
}

double shahbazly::Rectangle::getArea() const
{
  return m_width * m_height;
}

shahbazly::rectangle_t shahbazly::Rectangle::getFrameRect() const
{
  point_t topLeft = m_corners[0];
  point_t bottomRight = m_corners[2];

  for (size_t i = 0; i < 4; i++)
  {
    topLeft.x = std::min(m_corners[i].x, topLeft.x);
    topLeft.y = std::max(m_corners[i].y, topLeft.y);
    bottomRight.x = std::max(m_corners[i].x, bottomRight.x);
    bottomRight.y = std::min(m_corners[i].y, bottomRight.y);
  }

  point_t center = {(topLeft.x + bottomRight.x) / 2, (topLeft.y + bottomRight.y) / 2};

  return {bottomRight.x - topLeft.x, topLeft.y - bottomRight.y, center};
}

void shahbazly::Rectangle::move(const point_t & pos)
{
  const int RectangleCornersCount = 4;
  for (size_t i = 0; i < RectangleCornersCount; i++)
  {
    m_corners[i] = {pos.x + m_corners[i].x - m_center.x, pos.y + m_corners[i].y - m_center.y};
  }

  m_center = pos;
}

void shahbazly::Rectangle::move(double dx, double dy)
{
  m_center.x += dx;
  m_center.y += dy;

  for (size_t i = 0; i < 4; i++)
  {
    m_corners[i].x += dx;
    m_corners[i].y += dy;
  }
}

void shahbazly::Rectangle::scale(double scaleFactor)
{
  if (scaleFactor <= 0.0) {
    throw std::invalid_argument("Rectangle's scale factor must be greater than zero.");
  }
  const int RectangleCornersCount = 4;
  m_width *= scaleFactor;
  m_height *= scaleFactor;

  for (size_t i = 0; i < RectangleCornersCount; i++)
  {
    m_corners[i] = {m_center.x + (m_corners[i].x - m_center.x) * scaleFactor,
                    m_center.y + (m_corners[i].y - m_center.y) * scaleFactor};
  }
}

void shahbazly::Rectangle::rotate(double degrees)
{
  const double angleRadian = M_PI * degrees / 180;
  const double sin = std::sin(angleRadian);
  const double cos = std::cos(angleRadian);
  const int RectangleCornersCount = 4;

  for (size_t i = 0; i < RectangleCornersCount; i++)
  {
    m_corners[i] = {m_center.x + (m_corners[i].x - m_center.x) * cos - (m_corners[i].y - m_center.y) * sin,
                    m_center.y + (m_corners[i].y - m_center.y) * cos + (m_corners[i].x - m_center.x) * sin};
  }
}


void shahbazly::Rectangle::showParameters() const
{
  std::cout << "Rectangle Parameters:\n"
      << "Center - {" << m_center.x << ", " << m_center.y << "}\n"
      << "Width - " << m_width << '\n'
      << "Height - " << m_height << '\n'
      << "Area - " << getArea() << '\n'
      << "Size - " << m_width << "x" << m_height << "\n"
      
      << "Coordinates of the corners:\n" << '\n'
      << "Top left: {" << m_corners[0].x << ", " << m_corners[0].y << "}\n"
      << "Top right: {" << m_corners[1].x << ", " << m_corners[1].y << "}\n"
      << "Bottom right: {" << m_corners[2].x << ", " << m_corners[2].y << "}\n"
      << "Bottom left: {" << m_corners[3].x << ", " << m_corners[3].y << "}\n\n";
}
