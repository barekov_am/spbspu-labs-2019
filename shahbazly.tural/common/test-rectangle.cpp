#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"
#include "base-types.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(rectangleTests)

//Тест на неизменность после перемещения прямоугольника
BOOST_AUTO_TEST_CASE(invariabilityAfterMovingRectangle)
{
  shahbazly::Rectangle testRectangle({0, 0}, 7.2, 6.4);
  const shahbazly::rectangle_t rectangleFrameBeforeMoving = testRectangle.getFrameRect();
  const double areaBeforeMoving = testRectangle.getArea();

  testRectangle.move({6.9, 9.6});
  BOOST_CHECK_CLOSE(rectangleFrameBeforeMoving.width, testRectangle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleFrameBeforeMoving.height, testRectangle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMoving, testRectangle.getArea(), ACCURACY);
}

//Тест на неизменность площади прямоугольника после его масштабирования
BOOST_AUTO_TEST_CASE(areaAfterScalingRectangle)
{
  shahbazly::Rectangle testRectangle({0, 0}, 2, 8);
  const double areaBeforeMoving = testRectangle.getArea();
  const double scaleFactor = 6.8;
  testRectangle.scale(scaleFactor);
  BOOST_CHECK_CLOSE(testRectangle.getArea(), scaleFactor * scaleFactor * areaBeforeMoving, ACCURACY);
}

//Тест на неправильные аргументы прямоугольника
BOOST_AUTO_TEST_CASE(invalidArgumentsRectangle)
{
  BOOST_CHECK_THROW(shahbazly::Rectangle({1, 2}, -3, -3), std::invalid_argument);
  BOOST_CHECK_THROW(shahbazly::Rectangle({1, 2}, -1, 2), std::invalid_argument);
  BOOST_CHECK_THROW(shahbazly::Rectangle({1, 2}, 1, -2), std::invalid_argument);

  shahbazly::Rectangle testRectangle({1, 2}, 3, 4);
  BOOST_CHECK_THROW(testRectangle.scale(0), std::invalid_argument);
}

//Тест на неизменность после вращения прямоугольника
BOOST_AUTO_TEST_CASE(RectangleAreaAfterRotate)
{
  shahbazly::Rectangle rec({2.0, 2.0}, 4.0, 5.0);
  const double areaBefore = rec.getArea();
  const double widthBefore = rec.getFrameRect().width;
  const double heightBefore = rec.getFrameRect().height;

  rec.rotate(90);
  BOOST_CHECK_CLOSE(areaBefore, rec.getArea(), ACCURACY);
  BOOST_CHECK_CLOSE(widthBefore, rec.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(heightBefore, rec.getFrameRect().width, ACCURACY);
}

BOOST_AUTO_TEST_SUITE_END()
