#ifndef MATRIX_A4
#define MATRIX_A4

#include <memory>
#include "shape.hpp"

namespace shahbazly
{
  class Matrix
  {
  public:
    class Layer
    {
    public:
      Layer(Shape::ptr *arr, size_t size);
      Shape::ptr operator [](size_t index) const;
      size_t getSize() const;

    private:
      size_t m_size;
      Shape::arrayPtr m_figures;
    };

    Matrix();
    Matrix(const Matrix & rhs);
    Matrix(Matrix && rhs);

    ~Matrix() = default;

    Matrix & operator =(const Matrix & rhs);
    Matrix & operator =(Matrix && rhs);
    Layer operator [](size_t row) const;

    void add(Shape::ptr shape, size_t row);
    size_t getRows() const;
    size_t getNumberOfFigures() const;
    void showParameters() const;

  private:
    size_t m_rows;
    std::unique_ptr<size_t[]> m_columns;
    Shape::arrayPtr m_figures;
    size_t numberOfFigures;
  };
}

#endif
