#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(CompositeShapeTestSuite)

BOOST_AUTO_TEST_CASE(invariabilityAfterMovingCompositeShape)
{
  shahbazly::Rectangle rec({0.0, 0.0}, 1.0, 5.0);
  shahbazly::Circle cir(7.5, {8.4, 1.2});
  shahbazly::CompositeShape comp(std::make_shared<shahbazly::Rectangle>(rec));

  comp.add(std::make_shared<shahbazly::Circle>(cir));

  const shahbazly::rectangle_t compBefore = comp.getFrameRect();
  const double areaBefore = comp.getArea();
  const size_t countBefore = comp.getCount();

  comp.move(2.7, 5.8);
  BOOST_CHECK_CLOSE(compBefore.width, comp.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(compBefore.height, comp.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, comp.getArea(), ACCURACY);
  BOOST_CHECK_EQUAL(countBefore, comp.getCount());

  comp.move({3.2, 7.4});
  BOOST_CHECK_CLOSE(compBefore.width, comp.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(compBefore.height, comp.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, comp.getArea(), ACCURACY);
  BOOST_CHECK_EQUAL(countBefore, comp.getCount());
}

BOOST_AUTO_TEST_CASE(CompositeShapeAreaAfterScale)
{
  shahbazly::Rectangle rec({0.0, 0.0}, 4.9, 5.6);
  shahbazly::Circle cir(6.0, {2.2, 4.0});
  shahbazly::CompositeShape comp(std::make_shared<shahbazly::Rectangle>(rec));

  comp.add(std::make_shared<shahbazly::Circle>(cir));

  const double areaBefore1 = comp.getArea();
  const double scaleFactor1 = 25.6;

  comp.scale(scaleFactor1);
  BOOST_CHECK_CLOSE(areaBefore1 * scaleFactor1 * scaleFactor1, comp.getArea(), ACCURACY);

  const double areaBefore2 = comp.getArea();
  const double scaleFactor2 = 0.1;

  comp.scale(scaleFactor2);
  BOOST_CHECK_CLOSE(areaBefore2 * scaleFactor2 * scaleFactor2, comp.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(CompositeShapeFrameRectAfterScale)
{
  shahbazly::Rectangle rec({0.0, 0.0}, 6.0, 7.0);
  shahbazly::Circle cir(1.0, {2.0, 3.0});
  shahbazly::CompositeShape comp(std::make_shared<shahbazly::Rectangle>(rec));

  comp.add(std::make_shared<shahbazly::Circle>(cir));

  const shahbazly::rectangle_t frameRectBefore = comp.getFrameRect();
  const double scaleFactor = 8.9;

  comp.scale(scaleFactor);
  const shahbazly::rectangle_t frameRectAfter = comp.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBefore.width * scaleFactor, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height * scaleFactor, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
}

BOOST_AUTO_TEST_CASE(CompositeShapeIncorrectParameters)
{
  shahbazly::Rectangle rec({0.0, 0.0}, 4.1, 1.4);
  shahbazly::Circle cir(1.1, {3.3, 2.2});
  shahbazly::CompositeShape comp(std::make_shared<shahbazly::Rectangle>(rec));

  comp.add(std::make_shared<shahbazly::Circle>(cir));

  BOOST_CHECK_THROW(comp.scale(-1.0), std::invalid_argument);
  BOOST_CHECK_THROW(comp.add(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(comp.remove(5), std::invalid_argument);
  BOOST_CHECK_THROW(comp[2], std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(CompositeShapeAfterAddShape)
{
  shahbazly::Rectangle rec({0.0, 0.0}, 4.0, 7.0);
  shahbazly::Circle cir(1.0, {2.0, 3.0});
  shahbazly::CompositeShape comp(std::make_shared<shahbazly::Rectangle>(rec));

  const double areaBefore = comp.getArea();
  const size_t countBefore = comp.getCount();

  comp.add(std::make_shared<shahbazly::Circle>(cir));
  
  const double areaCir = comp[1]->getArea();
  BOOST_CHECK_CLOSE(areaBefore + areaCir, comp.getArea(), ACCURACY);
  BOOST_CHECK_EQUAL(countBefore + 1, comp.getCount());
}

BOOST_AUTO_TEST_CASE(CompositeShapeAfterDeleteByIndexShape)
{
  shahbazly::Rectangle rec({9.8, 7.6}, 5.4, 3.2);
  shahbazly::Circle cir(9.6, {1.0, 8.7});
  shahbazly::CompositeShape comp(std::make_shared<shahbazly::Rectangle>(rec));

  comp.add(std::make_shared<shahbazly::Circle>(cir));

  const double areaBefore = comp.getArea();
  const double areaSecondShape = comp[1]->getArea();
  const size_t countBefore = comp.getCount();

  comp.remove(1);
  BOOST_CHECK_CLOSE(comp.getArea(), areaBefore - areaSecondShape, ACCURACY);
  BOOST_CHECK_EQUAL(countBefore - 1, comp.getCount());
}

BOOST_AUTO_TEST_CASE(CompositeShapeAfterDeleteByReferenceShape)
{
  shahbazly::Rectangle rec({0.0, 0.0}, 2.0, 4.0);
  shahbazly::Circle cir(1.0, {2.0, 3.0});
  shahbazly::CompositeShape composite(std::make_shared<shahbazly::Rectangle>(rec));
  composite.add(std::make_shared<shahbazly::Circle>(cir));
  const double areaBefore = composite.getArea();
  const double areaSecondShape = composite[1]->getArea();
  const size_t countBefore = composite.getCount();

  composite.remove(1);
  BOOST_CHECK_CLOSE(composite.getArea(), areaBefore - areaSecondShape, ACCURACY);
  BOOST_CHECK_EQUAL(countBefore - 1, composite.getCount());
}


BOOST_AUTO_TEST_CASE(CompositeShapeFrameRectAfterRotate)
{
  shahbazly::Rectangle rec({0.0, 0.0}, 2.0, 4.0);
  shahbazly::Circle cir(1.0, {2.0, 3.0});
  shahbazly::CompositeShape comp(std::make_shared<shahbazly::Rectangle>(rec));
  comp.add(std::make_shared<shahbazly::Circle>(cir));
  const shahbazly::rectangle_t frameRectBefore = comp.getFrameRect();
  const double areaBefore = comp.getArea();

  comp.rotate(90);
  const shahbazly::rectangle_t frameRectAfter = comp.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, comp.getArea(), ACCURACY);
}

BOOST_AUTO_TEST_CASE(CopyConstructor)
{
  shahbazly::Rectangle rec({7.7, 8.8}, 8.8, 8.8);
  shahbazly::Circle cir(1.0, {2.0, 2.2});
  shahbazly::CompositeShape comp(std::make_shared<shahbazly::Rectangle>(rec));

  comp.add(std::make_shared<shahbazly::Circle>(cir));

  const double compArea = comp.getArea();
  const shahbazly::rectangle_t compFrameRect = comp.getFrameRect();
  const size_t compCount = comp.getCount();

  shahbazly::CompositeShape test_comp(comp);

  const double testCompArea = test_comp.getArea();
  const shahbazly::rectangle_t testCompFrameRect = test_comp.getFrameRect();
  const size_t testCompCount = test_comp.getCount();

  BOOST_CHECK_CLOSE(compArea, testCompArea, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.width, testCompFrameRect.width, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.height, testCompFrameRect.height, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.pos.x, testCompFrameRect.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.pos.y, testCompFrameRect.pos.y, ACCURACY);
  BOOST_CHECK_EQUAL(compCount, testCompCount);
}

BOOST_AUTO_TEST_CASE(CopyOperator)
{
  shahbazly::Rectangle rec({0.0, 0.0}, 5.0, 6.0);
  shahbazly::Circle cir(3.0, {7.0, 6.0});
  shahbazly::CompositeShape comp(std::make_shared<shahbazly::Rectangle>(rec));

  comp.add(std::make_shared<shahbazly::Circle>(cir));

  const double compArea = comp.getArea();
  const shahbazly::rectangle_t compFrameRect = comp.getFrameRect();
  const size_t compCount = comp.getCount();

  shahbazly::CompositeShape test_comp = comp;

  const double testCompArea = test_comp.getArea();
  const shahbazly::rectangle_t testCompFrameRect = test_comp.getFrameRect();
  const size_t testCompCount = test_comp.getCount();

  BOOST_CHECK_CLOSE(compArea, testCompArea, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.width, testCompFrameRect.width, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.height, testCompFrameRect.height, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.pos.x, testCompFrameRect.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(compFrameRect.pos.y, testCompFrameRect.pos.y, ACCURACY);
  BOOST_CHECK_EQUAL(compCount, testCompCount);
}

BOOST_AUTO_TEST_CASE(MoveConstructor)
{
  shahbazly::Rectangle rec({0.0, 0.0}, 5.0, 6.0);
  shahbazly::Circle cir(3.0, {7.0, 6.0});
  shahbazly::CompositeShape comp(std::make_shared<shahbazly::Rectangle>(rec));

  comp.add(std::make_shared<shahbazly::Circle>(cir));

  size_t countBeforeMoving = comp.getCount();
  shahbazly::Shape::ptr figure1 = comp[0];
  shahbazly::Shape::ptr figure2 = comp[1];

  shahbazly::CompositeShape comp2(std::move(comp));

  BOOST_CHECK_EQUAL(comp2.getCount(), countBeforeMoving);
  BOOST_CHECK_EQUAL(comp2[0] == figure1, true);
  BOOST_CHECK_EQUAL(comp2[1] == figure2, true);
}

BOOST_AUTO_TEST_CASE(MoveOperator)
{
  shahbazly::Rectangle rec({7.7, 8.8}, 8.8, 8.8);
  shahbazly::Circle cir(1.0, {2.0, 2.2});
  shahbazly::CompositeShape comp(std::make_shared<shahbazly::Rectangle>(rec));

  comp.add(std::make_shared<shahbazly::Circle>(cir));

  size_t countBeforeMoving = comp.getCount();
  shahbazly::Shape::ptr figure1 = comp[0];
  shahbazly::Shape::ptr figure2 = comp[1];

  shahbazly::CompositeShape comp2 = std::move(comp);

  BOOST_CHECK_EQUAL(comp2.getCount(), countBeforeMoving);
  BOOST_CHECK_EQUAL(comp2[0] == figure1, true);
  BOOST_CHECK_EQUAL(comp2[1] == figure2, true);
}

BOOST_AUTO_TEST_SUITE_END()
