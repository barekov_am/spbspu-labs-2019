#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"
#include <memory>

namespace shahbazly
{
  class Shape
  {
  public:
    using ptr = std::shared_ptr<Shape>;
    using arrayPtr = std::unique_ptr<ptr[]>;

    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t & pos) = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void scale(double scaleFactor) = 0;
    virtual void rotate(double degrees) = 0;
    virtual void showParameters() const = 0;

    int intersection(const ptr shape) const;
  };
}
#endif // SHAPE_HPP
