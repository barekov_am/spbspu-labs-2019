#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(circleTests)

//Тест на неизменность после перемещения круга
BOOST_AUTO_TEST_CASE(invariabilityAfterMovingCircle)
{
  shahbazly::Circle testCircle(2.5, {0, 0});
  const shahbazly::rectangle_t rectangleFrameBeforeMoving  = testCircle.getFrameRect();
  const double areaBeforeMoving = testCircle.getArea();

  testCircle.move({2.3, 8.6});
  BOOST_CHECK_CLOSE(rectangleFrameBeforeMoving.width, testCircle.getFrameRect().width, ACCURACY);
  BOOST_CHECK_CLOSE(rectangleFrameBeforeMoving.height, testCircle.getFrameRect().height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMoving, testCircle.getArea(), ACCURACY);
}

//Тест на неизменность площади кргуа после его масштабирования
BOOST_AUTO_TEST_CASE(areaAfterScalingCircle)
{
  shahbazly::Circle testCircle(4, {0, 0});
  const double areaBeforeMoving = testCircle.getArea();
  const double scaleFactor = 6.8;
  testCircle.scale(scaleFactor);
  BOOST_CHECK_CLOSE(testCircle.getArea(), scaleFactor * scaleFactor * areaBeforeMoving, ACCURACY);
}

//Тест на неправильные аргументы круга
BOOST_AUTO_TEST_CASE(invalidArgumentsCircle)
{
  BOOST_CHECK_THROW(shahbazly::Circle(-6, {3, 5}), std::invalid_argument);

  shahbazly::Circle testCircle(2, {5, 3});
  BOOST_CHECK_THROW(testCircle.scale(-9), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
