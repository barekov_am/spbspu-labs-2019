#include "composite-shape.hpp"

#include <utility>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <stdexcept>

shahbazly::CompositeShape::CompositeShape() :
  m_count(0)
{ }

shahbazly::CompositeShape::CompositeShape(Shape::ptr shape) :
  m_count(1),
  m_figures(std::make_unique<Shape::ptr[]>(1))
{
  if (shape == nullptr) {
    throw std::invalid_argument("Shape pointer is empty");
  }

  m_figures[0] = shape;
}

shahbazly::CompositeShape::CompositeShape(const CompositeShape & rhs) :
  m_count(rhs.m_count),
  m_figures(std::make_unique<ptr[]>(rhs.m_count))
{
  for (size_t i = 0; i < m_count; i++)
  {
    m_figures[i] = rhs.m_figures[i];
  }
}

shahbazly::CompositeShape::CompositeShape(CompositeShape && rhs) :
  m_count(rhs.m_count),
  m_figures(std::move(rhs.m_figures))
{ 
  rhs.m_count = 0;
  rhs.m_figures = nullptr;
}

shahbazly::CompositeShape & shahbazly::CompositeShape::operator=(const CompositeShape & rhs)
{
  if (this != & rhs) {
    m_count = rhs.m_count;
    Shape::arrayPtr m_array(std::make_unique<Shape::ptr[]>(rhs.m_count));
    for (size_t i = 0; i < m_count; i++)
    {
      m_array[i] = rhs.m_figures[i];
    }

    m_figures.swap(m_array);
  }

  return *this;
}

shahbazly::CompositeShape & shahbazly::CompositeShape::operator=(CompositeShape && rhs)
{
  if (this != &rhs) {
    m_count = rhs.m_count;
    m_figures = std::move(rhs.m_figures);
  }
  rhs.m_count = 0;
  rhs.m_figures = nullptr;
  return *this;
}

shahbazly::Shape::ptr shahbazly::CompositeShape::operator [](size_t index) const
{
  if (index >= m_count) {
    throw std::invalid_argument("Index is out of array");
  }

  return (m_figures[index]);
}

size_t shahbazly::CompositeShape::getCount() const
{
  return m_count;
}

double shahbazly::CompositeShape::getArea() const
{
  double totalArea = 0;
  for (size_t i = 0; i < m_count; i++)
  {
    totalArea += m_figures[i]->getArea();
  }

  return totalArea;
}

shahbazly::rectangle_t shahbazly::CompositeShape::getFrameRect() const
{
  double maxX = 0;
  double maxY = 0;
  double minX = 0;
  double minY = 0;

  if (m_figures != nullptr) {
    rectangle_t rectangle = m_figures[0]->getFrameRect();
    maxX = rectangle.pos.x + rectangle.width / 2;
    minX = rectangle.pos.x - rectangle.width / 2;
    maxY = rectangle.pos.y + rectangle.height / 2;
    minY = rectangle.pos.y - rectangle.height / 2;

    for (size_t i = 1; i < m_count; i++)
    {
      rectangle = m_figures[i]->getFrameRect();

      point_t position = rectangle.pos;
      maxX = std::max(position.x + rectangle.width / 2, maxX);
      minX = std::min(position.x - rectangle.width / 2, minX);
      maxY = std::max(position.y + rectangle.height / 2, maxY);
      minY = std::min(position.y - rectangle.height / 2, minY);
    }
  }

  return {maxX - minX, maxY - minY, {(maxX + minX) / 2, (maxY + minY) / 2}};
}

void shahbazly::CompositeShape::add(Shape::ptr shape)
{
  if (shape == nullptr) {
    throw std::invalid_argument("Shape is empty");
  }

  Shape::arrayPtr m_array(std::make_unique<Shape::ptr[]>(m_count + 1));

  for (size_t i = 0; i < m_count; i++)
  {
    m_array[i] = m_figures[i];
  }

  m_array[m_count] = shape;
  m_count++;
  m_figures.swap(m_array);
}

void shahbazly::CompositeShape::scale(double scaleFactor)
{
  if (scaleFactor <= 0.0) {
    throw std::invalid_argument("Scale factor must be greater than zero.");
  }

  point_t position = getFrameRect().pos;

  for (size_t i = 0; i < m_count; i++)
  {
    m_figures[i]->move({position.x + (m_figures[i]->getFrameRect().pos.x - position.x) * scaleFactor,
                        position.y + (m_figures[i]->getFrameRect().pos.y - position.y) * scaleFactor});
    m_figures[i]->scale(scaleFactor);
  }
}

void shahbazly::CompositeShape::rotate(double degrees)
{
  const double angleRadian = M_PI * degrees / 180;
  const double sin = std::sin(angleRadian);
  const double cos = std::cos(angleRadian);
  const point_t compositeShapeCenter = this->getFrameRect().pos;

  for (size_t i = 0; i < m_count; i++)
  {
    const point_t figureCenter = m_figures[i]->getFrameRect().pos;
    double newX = compositeShapeCenter.x + (figureCenter.x - compositeShapeCenter.x) * cos
                  - (figureCenter.y - compositeShapeCenter.y) * sin;
    double newY = compositeShapeCenter.y + (figureCenter.x - compositeShapeCenter.x) * sin
                  + (figureCenter.y - compositeShapeCenter.y) * cos;

    m_figures[i]->move({newX, newY});
    m_figures[i]->rotate(degrees);
  }
}

void shahbazly::CompositeShape::move(const point_t & pos)
{
  double dx = pos.x - getFrameRect().pos.x;
  double dy = pos.y - getFrameRect().pos.y;

  move(dx, dy);
}

void shahbazly::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < m_count; i++)
  {
    m_figures[i]->move(dx, dy);
  }
}

void shahbazly::CompositeShape::remove(size_t index)
{
  if (index >= m_count) {
    throw std::invalid_argument("Index is out of array");
  }

  if (m_figures == nullptr) {
    throw std::invalid_argument("The Composite shape is empty");
  }

  for (size_t i = index; i < m_count - 1; i++)
  {
    m_figures[i] = m_figures[i + 1];
  }
  m_count--;
}

void shahbazly::CompositeShape::remove(Shape::ptr shape)
{
  if (m_figures == nullptr) {
    throw std::invalid_argument("The Composite shape is empty");
  }

  Shape::arrayPtr arr(std::make_unique<Shape::ptr[]>(m_count + 1));
  for (size_t i = 0; i < m_count; i++)
  {
    arr[i] = m_figures[i];
  }

  arr[m_count] = shape;
  m_count++;
  m_figures.swap(arr);
}

void shahbazly::CompositeShape::showParameters() const
{
  std::cout << "Composite shape Parameters:\n"
      << "Center - {" << getFrameRect().pos.x << ", " << getFrameRect().pos.y <<"}\n"
      << "Count: " << m_count << '\n'
      << "Width - " << getFrameRect().width << '\n'
      << "Height - " << getFrameRect().height << '\n'
      << "Area - " << getArea() << "\n\n";
}
