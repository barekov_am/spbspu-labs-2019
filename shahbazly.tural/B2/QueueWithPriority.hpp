#ifndef B2_QUEUE_WITH_PRIORITY_HPP
#define B2_QUEUE_WITH_PRIORITY_HPP

#include <list>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <vector>
#include <memory>

enum class ElementPriority {
  high,
  normal,
  low
};

template<typename T>
class QueueWithPriority {
public:
  void add(const T &element, const ElementPriority &elementPriority);
  void accelerate();
  T get();
  bool isEmpty();

private:
  std::list<T> high, normal, low;
};

#endif //B2_QUEUE_WITH_PRIORITY_HPP
