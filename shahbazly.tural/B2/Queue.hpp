#ifndef QUEUE_HPP
#define QUEUE_HPP

#include "QueueWithPriority.hpp"

template<typename T>
bool QueueWithPriority<T>::isEmpty() {
  return high.empty() && normal.empty() && low.empty();
}

template<typename T>
void QueueWithPriority<T>::add(const T &element, const ElementPriority &elementPriority) {
  switch (elementPriority) {
    case ElementPriority::high:
      high.push_back(element);
      break;
    case ElementPriority::normal:
      normal.push_back(element);
      break;
    case ElementPriority::low:
      low.push_back(element);
      break;
  }
}

template<typename T>
T QueueWithPriority<T>::get() {
  if (!high.empty()) {
    const auto element = high.front();
    high.pop_front();
    return element;
  }

  if (!normal.empty()) {
    const auto element = normal.front();
    normal.pop_front();
    return element;
  }

  if (!low.empty()) {
    const auto element = low.front();
    low.pop_front();
    return element;
  }

  throw std::out_of_range("<EMPTY>");
}

template<typename T>
void QueueWithPriority<T>::accelerate() {
  high.insert(std::cend(high), std::cbegin(low), std::cend(low));
  low.clear();
}

#endif
