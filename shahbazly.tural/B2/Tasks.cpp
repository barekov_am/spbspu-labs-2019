#include "Tasks.hpp"
#include "Queue.hpp"

void Tasks::task1()
{
  QueueWithPriority<std::string> queue;

  std::string line;
  while (std::getline(std::cin >> std::ws, line)) {
    std::stringstream ss(line);
    std::string action;
    ss >> action;

    if (action == "add") {
      std::string priority;
      ss >> priority;

      ss.ignore();
      std::string data;
      std::getline(ss >> std::ws, data);

      if (data.empty()) {
        std::cout << "<INVALID COMMAND>" << std::endl;
      } else if (priority == "high") {
        queue.add(data, ElementPriority::high);
      } else if (priority == "normal") {
        queue.add(data, ElementPriority::normal);
      } else if (priority == "low") {
        queue.add(data, ElementPriority::low);
      } else {
        std::cout << "<INVALID COMMAND>" << std::endl;
      }
    } else if (action == "accelerate") {
      queue.accelerate();
    } else if (action == "get") {
      try {
        std::cout << queue.get() << std::endl;
      } catch (const std::exception &exception) {
        std::cout << exception.what() << std::endl;
      }
    } else {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }
  }
}

void Tasks::task2()
{
  std::list<int> list;
  const int minValue = 1;
  const int maxValue = 20;
  const size_t maxSize = 20;

  int num;
  while (std::cin >> num) {
    if ((num < minValue) || (num > maxValue)) {
      throw std::out_of_range("Out of range");
    }
    list.push_back(num);
  }

  if (!std::cin.eof()) {
    throw std::invalid_argument("Incorrect input");
  }

  if (list.size() > maxSize) {
    throw std::invalid_argument("too many element");
  }

  auto left = list.begin();
  auto right = list.rbegin();

  auto pivot = list.begin();
  std::advance(pivot, list.size() / 2);

  while (left != pivot) {
    std::cout << *left++ << ' ' << *right++ << ' ';
  }

  if (list.size() % 2 == 1) {
    std::cout << *pivot;
  }
  
  std::cout << std::endl;
}
