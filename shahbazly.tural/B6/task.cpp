#include <iostream>
#include <algorithm>
#include <iterator>
#include "statistics.hpp"

void task()
{
  Statistics stats = std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), Statistics());

  if (!std::cin.eof()) {
    throw std::invalid_argument("Couldn't read this data");
  }

  if (stats.num_counter_ == 0) {
    std::cout << "No Data\n";
    return;
  }

  std::cout << "Max: " << stats.maximum_num_ << std::endl
         << "Min: " << stats.minimum_num_ << std::endl
         << "Mean: " << stats.average_ << std::endl
         << "Positive: " << stats.quantity_positive_ << std::endl
         << "Negative: " << stats.quantity_negative_ << std::endl
         << "Odd Sum: " << stats.amount_odd_ << std::endl
         << "Even Sum: " << stats.amount_even_ << std::endl
         << "First/Last Equal: "
         << ((stats.first_number_ == stats.last_number_) ? "yes" : "no") << std::endl;
}
