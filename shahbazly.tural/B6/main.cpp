#include <stdexcept>
#include "statistics.hpp"

void task();

int main()
{
  try {
    task();
  } catch (const std::exception &err) {
    std::cerr << err.what();
    return 2;
  }

  return 0;
}
