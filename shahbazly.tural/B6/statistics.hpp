#include <iostream>
#include <cstdio>

class Statistics {
  public:
    Statistics();
    void operator()(int num);

    long long num_counter_;

    long long maximum_num_;
    long long minimum_num_;

    long long quantity_positive_;
    long long quantity_negative_;

    long long amount_odd_;
    long long amount_even_;

    long long first_number_;
    long long last_number_;

    double  average_;
};
