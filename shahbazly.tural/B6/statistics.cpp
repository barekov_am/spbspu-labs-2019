#include <limits>
#include <stdexcept>
#include "statistics.hpp"

Statistics::Statistics() :
  num_counter_(0),
  maximum_num_(0),
  minimum_num_(0),
  quantity_positive_(0),
  quantity_negative_(0),
  amount_odd_(0),
  amount_even_(0),
  first_number_(0),
  last_number_(0),
  average_(0.0)
{}

void Statistics::operator()(int number)
{
  if (num_counter_ == 0) {
    first_number_ = number;

    minimum_num_ = number;
    maximum_num_ = number;
  }

  num_counter_++;


  if (number < minimum_num_) {
    minimum_num_ = number;
  } else if (number > maximum_num_) {
    maximum_num_ = number;
  }

  if (!number == 0){
    (number % 2 ? amount_odd_ : amount_even_) += number;
    (number > 0) ? quantity_positive_++ : quantity_negative_++;
  }
  
  average_ = static_cast<double>(amount_even_ + amount_odd_) / num_counter_;

  last_number_ = number;
}
