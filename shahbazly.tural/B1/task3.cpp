#include <vector>
#include <iostream>
#include <stdexcept>
#include <string>
#include "declarations.hpp"

void executeTask3(int args, char*[]) {
  if (args != 2) {
    throw std::invalid_argument("Incorrect arguments for task 3");
  }

  std::vector<int> vec;
  int temp;
  while (std::cin >> temp) {
    vec.push_back(temp);
  }

  if (std::cin.fail() && !std::cin.eof()) {
    throw std::invalid_argument("Incorrect input - expected integer type");
  }

  if (!vec.empty()) {
    if (vec.back() != 0) {
      throw std::invalid_argument("Incorrect input - expected zero before end of input");
    } else {
      vec.pop_back();
    }
  }

  if (!vec.empty()) {
    int lastNum = vec.back();
    if (lastNum == 1) {
      for (auto i = vec.begin(); i != vec.end();) {
        if (*i % 2 == 0) {
          i = vec.erase(i);
        } else {
          i++;
        }
      }
    } else if (lastNum == 2) {
      for (auto i = vec.begin(); i != vec.end();) {
        if (*i % 3 == 0) {
          i = vec.insert(i + 1, {1, 1, 1});
        } else {
          i++;
        }
      }
    }
    detail::printContainer(vec);
  }
}
