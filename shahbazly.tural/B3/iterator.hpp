#ifndef ITERATOR_HPP
#define ITERATOR_HPP

#include <iterator>

class Iterator: public std::iterator<std::bidirectional_iterator_tag, unsigned long long> {
public:
  Iterator();
  Iterator(int index);

  long long &operator *();
  long long *operator ->();

  Iterator &operator ++();
  Iterator operator ++(int);

  Iterator &operator --();
  Iterator operator --(int);

  bool operator ==(Iterator iter) const;
  bool operator !=(Iterator iter) const;

private:
  long long value_;
  int index_;

  unsigned long long getValue(int index) const;
};

#endif
