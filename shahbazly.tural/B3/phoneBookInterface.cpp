#include <iostream>
#include "phoneBookInterface.hpp"

void PhoneBook::showTemporaryRecord(iterator iter)
{
  std::cout << (*iter).number << " " << (*iter).name << "\n";
}

PhoneBook::iterator PhoneBook::next(iterator iter)
{
  return ++iter;
}

PhoneBook::iterator PhoneBook::previous(iterator iter)
{
  return --iter;
}

PhoneBook::iterator PhoneBook::move(iterator iter, const int steps)
{
  int counter = 0;
  if (steps >= 0) {
    while ((std::next(iter) != list_.end()) && (counter != steps)) {
      ++iter;
      ++counter;
    }
  } else {
    while ((iter != list_.begin()) && (counter != steps)) {
      --iter;
      --counter;
    }
  }
  return iter;
}

PhoneBook::iterator PhoneBook::insert(iterator iter, record_t &record)
{
  return list_.insert(iter, record);
}

void PhoneBook::pushBack(record_t &record)
{
  return list_.push_back(record);
}

PhoneBook::iterator PhoneBook::replaceTemporaryRecord(iterator iter, record_t &record)
{
  *iter = record;
  return iter;
}

PhoneBook::iterator PhoneBook::remove(iterator iter)
{
  return list_.erase(iter);
}

PhoneBook::iterator PhoneBook::begin()
{
  return list_.begin();
}

PhoneBook::iterator PhoneBook::end()
{
  return list_.end();
}

bool PhoneBook::empty()
{
  return list_.empty();
}
