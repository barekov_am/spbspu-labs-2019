#include "iterator.hpp"

const int MIN = 1;
const int MAX = 10;

Iterator::Iterator():
  value_(1),
  index_(1)
{}

Iterator::Iterator(int index):
  value_(getValue(index)),
  index_(index)
{
  if ((index < MIN) || (index > MAX + 1)) {
    throw std::out_of_range("Index is out of range");
  }
}

unsigned long long Iterator::getValue(int index) const
{
  if (index <= MIN) {
    return 1;
  }

  return index * getValue(index - 1);
}


long long &Iterator::operator*()
{
  return value_;
}

long long *Iterator::operator->()
{
  return &value_;
}

Iterator &Iterator::operator++()
{
  if (index_ > MAX) {
    throw std::out_of_range("Index is out of range");
  }

  ++index_;
  value_ *= index_;
  return *this;
}

Iterator Iterator::operator++(int)
{
  Iterator tempIterator = *this;
  ++(*this);
  return tempIterator;
}

Iterator &Iterator::operator--()
{
  if (index_ <= MIN) {
    throw std::out_of_range("Index is out of range");
  }

  value_ /= index_;
  --index_;
  return *this;
}

Iterator Iterator::operator--(int)
{
  Iterator tempIterator = *this;
  --(*this);
  return tempIterator;
}

bool Iterator::operator ==(Iterator iter) const {
  return (value_ == iter.value_) && (index_ == iter.index_);
}

bool Iterator::operator !=(Iterator iter) const {
  return !(*this == iter);
}
