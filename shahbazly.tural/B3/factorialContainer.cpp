#include "factorialContainer.hpp"

const int MIN = 1;
const int MAX = 10;


Iterator FactorialContainer::begin()
{
  return Iterator(MIN);
}

Iterator FactorialContainer::end()
{
  return Iterator(MAX + 1);
}
