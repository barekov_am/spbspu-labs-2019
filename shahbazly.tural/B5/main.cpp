#include <iostream>

void task1();
void task2();

int main(int argc, char *argv[])
{
  if (argc != 2) {
    std::cerr << "Incorrect number of parameters number! \n" << std::endl;
    return 1;
  }

  try {
    char * error;
    int variant = std::strtol(argv[1], &error, 10);
    if (*error) {
      std::cerr <<"Invalid task number argument" <<std::endl;
      return 1;
    }    
    switch (variant) { 
      case 1: {
        task1();
        break;
      }
      case 2: {
        task2();
        break;
      }
      default: {
        std::cerr << "Wrong number of task\n";
        return 1;
      }
    }
  } catch (const std::exception &exception) {
    std::cerr << exception.what() << std::endl;
    return 2;
  }

  return 0;
}
