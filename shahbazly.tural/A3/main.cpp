#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

#include <iostream>

int main()
{
  // Создание объекта типа Rectangle с центром в точке (6.0, 9.0) и шириной - 20, высотой - 30
  shahbazly::Rectangle rectangle({6.0, 9.0}, 20.0, 30.0);

  // Создание объекта типа Circle с радиусом 3 и центром в точке (4; 6)
  shahbazly::Circle circle(3.0, {4.0, 6.0});

  shahbazly::Shape* figures[] = {&circle, &rectangle};
  int SIZE = sizeof(figures) / sizeof(shahbazly::Shape);

  for (int i = 0; i < SIZE; i++)
  {
    figures[i]->showParameters();
  }
  
  std::cout << "-> move figures to (6, 9)\n\n";
  for (int i = 0; i < SIZE; i++)
  {
    figures[i]->move(6.0, 9.0);
    figures[i]->showParameters();
  }

  std::cout << "-> shift figures to (77, 7)\n\n";
  for (int i = 0; i < SIZE; i++)
  {
    figures[i]->move({77, 7});
    figures[i]->showParameters();
  }

  // Демонстрация функции масштабирования
  std::cout << "-> scale figures by factor = 4.7\n\n";
  for (int i = 0; i < SIZE; i++)
  {
    figures[i]->scale(4.7);
    figures[i]->showParameters();
  }

  // Демонстрация функций составных фигур
  shahbazly::Rectangle rec({2.0, -2.0}, 2.0, 1.0);
  shahbazly::Circle cir1(1.0, {4.0, -2.0});
  shahbazly::Circle cir2(3.0, {-4.0, 1.0});

  shahbazly::CompositeShape compositeShape(std::make_shared<shahbazly::Rectangle>(rec));

  compositeShape.add(std::make_shared<shahbazly::Circle>(cir1));
  compositeShape.add(std::make_shared<shahbazly::Circle>(cir2));

  std::cout << "Composite Shape before changes:" << std::endl;
  compositeShape.showParameters();
  compositeShape.move(2.0, 2.0);
  compositeShape.move({0.0, 0.0});
  compositeShape.scale(2.0);

  std::cout << "Composite Shape after changes:" << std::endl;
  compositeShape.showParameters();

  compositeShape.remove(1);
  std::cout << "Composite Shape after delete shape" << std::endl;
  compositeShape.showParameters();

  return 0;
}
