#include "shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "split.hpp"

#include <iostream>

int main()
{
  shahbazly::Rectangle rec({4.0, 3.0}, 4.0, 2.0);
  shahbazly::Circle cir1(2.0, {7.0, 5.0});
  shahbazly::Circle cir2(1.0, {11.0, 2.0});

  shahbazly::Shape *shape = &rec;
  std::cout << "Rectangle before changes:" << std::endl;
  shape->showParameters();
  shape->move(1.0, 2.0);
  shape->move({5.0, 3.0});
  shape->scale(2.0);
  shape->rotate(90);
  std::cout << "Rectangle after changes:" << std::endl;
  shape->showParameters();

  shape = &cir1;
  std::cout << "Circle before changes:" << std::endl;
  shape->showParameters();
  shape->move(3.0, 2.0);
  shape->move({1.0, 1.0});
  shape->scale(4.0);
  std::cout << "Circle after changes:" << std::endl;
  shape->showParameters();

  shahbazly::CompositeShape compositeShape(std::make_shared<shahbazly::Rectangle>(rec));
  compositeShape.add(std::make_shared<shahbazly::Circle>(cir1));
  compositeShape.add(std::make_shared<shahbazly::Circle>(cir2));
  std::cout << "Composite Shape before changes:" << std::endl;
  compositeShape.showParameters();
  compositeShape.move(2.0, 2.0);
  compositeShape.move({0.0, 0.0});
  compositeShape.scale(2.0);
  compositeShape.rotate(180);
  std::cout << "Composite Shape after changes:" << std::endl;
  compositeShape.showParameters();

  compositeShape.remove(1);
  std::cout << "Composite Shape after delete shape:" << std::endl;
  compositeShape.showParameters();

  shahbazly::Matrix matrix = shahbazly::split(compositeShape);
  matrix[0][0]->showParameters();
  matrix[0][1]->showParameters();

  matrix.add(std::make_shared<shahbazly::Circle>(cir1), 1);
  matrix.showParameters();

  return 0;
}
