#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP
#include <iostream>

namespace goncharova
{
  class Functor
  {
  public:
    Functor() noexcept;

    void operator()(const int& number) noexcept;
    friend std::ostream& operator<<(std::ostream& stream, const Functor& functor);

  private:
    long long int max_;
    long long int min_;
    long long int first_;
    size_t positive_;
    size_t negative_;
    size_t total_;
    long long int odd_sum_;
    long long int even_sum_;
    bool equals_;
  };

  std::ostream& operator<<(std::ostream& stream, const Functor& functor);
}
#endif
