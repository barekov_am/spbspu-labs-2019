#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace goncharova
{
  class Triangle : public Shape
  {
  public:
    // In triangle we set 3 dots: pointA_, pointB_ and pointC_
    Triangle(const point_t &, const point_t &, const point_t &);
    double getArea() const;
    rectangle_t getFrameRect() const;
    void shift(double dx, double dy);
    void move(const point_t &newPoint);
    void move(double dx, double dy);
    void inform() const;
    point_t getCenter() const;
    void scale(double scaleCoef);
    void rotate(const double);
    void rotatePoint(point_t &, const double);

  private:
    point_t pointA_, pointB_, pointC_;
    point_t center_;
    double angle_;
  };
}

#endif
