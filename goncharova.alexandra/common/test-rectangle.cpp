#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(movingRectangle)
{
  goncharova::Rectangle rectangle({{5.0, 5.0}, 2.3, 6.9});
  const goncharova::rectangle_t oldRect = rectangle.getFrameRect();
  const double oldSquare = rectangle.getArea();

  rectangle.move(6.0, 6.0);
  goncharova::rectangle_t newRect = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(rectangle.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newRect.height, oldRect.height, EPSILON);
  BOOST_CHECK_CLOSE(newRect.width, oldRect.width, EPSILON);

  rectangle.move({2.0, 2.0});
  newRect = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(rectangle.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newRect.height, oldRect.height, EPSILON);
  BOOST_CHECK_CLOSE(newRect.width, oldRect.width, EPSILON);
}

BOOST_AUTO_TEST_CASE(scalingRectangle)
{
  goncharova::Rectangle rectangle({{5.0, 5.0}, 3.5, 6.5});
  const double oldSquare = rectangle.getArea();

  const double scaleCoef = 3.0;
  rectangle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(rectangle.getArea(), oldSquare * scaleCoef * scaleCoef, EPSILON);
}

BOOST_AUTO_TEST_CASE(validRectangle)
{
  BOOST_CHECK_THROW(goncharova::Rectangle({{1.0, 1.0}, -2.0, 5.0}), std::invalid_argument);
  BOOST_CHECK_THROW(goncharova::Rectangle({{1.0, 1.0}, 2.0, -5.0}), std::invalid_argument);

  goncharova::Rectangle rectangle({{2.0, 2.0}, 2.0, 3.0});
  BOOST_CHECK_THROW(rectangle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rotation)
{
  goncharova::Rectangle rectangle({{5.0, 5.0}, 3.5, 6.5});
  double area = rectangle.getArea();
  rectangle.rotate(45);
  BOOST_CHECK_CLOSE(area, rectangle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()
