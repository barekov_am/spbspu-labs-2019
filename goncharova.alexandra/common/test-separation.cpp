#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separation.hpp"

BOOST_AUTO_TEST_SUITE(separationTest)

BOOST_AUTO_TEST_CASE(checkCorrectCrossing)
{
  goncharova::Rectangle testRectangle( {5.0, 5.0},4.0, 2.0);
  goncharova::Rectangle crossingTestRectangle({8.0, 5.0},6.0, 2.0);
  goncharova::Circle testCircle({11.0, 5.0},1.0);

  const bool falseResult = goncharova::cross(testRectangle.getFrameRect(), testCircle.getFrameRect());
  const bool trueResult1 = goncharova::cross(testRectangle.getFrameRect(), crossingTestRectangle.getFrameRect());
  const bool trueResult2 = goncharova::cross(testCircle.getFrameRect(), crossingTestRectangle.getFrameRect());

  BOOST_CHECK_EQUAL(falseResult, false);
  BOOST_CHECK_EQUAL(trueResult1, true);
  BOOST_CHECK_EQUAL(trueResult2, true);

}

BOOST_AUTO_TEST_CASE(checkCorrectSeparation)
{
  goncharova::Rectangle testRectangle({5.0, 5.0}, 4.0, 2.0);
  goncharova::Rectangle crossingTestRectangle({8.0, 5.0}, 6.0, 2.0);
  goncharova::Circle testCircle({11.0, 5.0}, 1.0);
  goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
  goncharova::shapePtr rectangleCrossingPtr = std::make_shared<goncharova::Rectangle>(crossingTestRectangle);
  goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

  goncharova::CompositeShape testCompositeShape(rectanglePtr);
  testCompositeShape.add(rectangleCrossingPtr);
  testCompositeShape.add(circlePtr);

  goncharova::Matrix testMatrix = goncharova::part(testCompositeShape);

  BOOST_CHECK(testMatrix[0][0] == rectanglePtr);
  BOOST_CHECK(testMatrix[0][1] == circlePtr);
  BOOST_CHECK(testMatrix[1][0] == rectangleCrossingPtr);
}

BOOST_AUTO_TEST_SUITE_END()
