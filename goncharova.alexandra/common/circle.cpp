#include "circle.hpp"
#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>
#include <stdexcept>

goncharova::Circle::Circle(const point_t &center, const double radius) :
  center_(center),
  radius_(radius)
{
  if (radius_ <= 0.0)
  {
    throw std::invalid_argument("Radius must be positive number");
  }
}

goncharova::rectangle_t goncharova::Circle::getFrameRect() const
{
  return { center_, radius_ * 2, radius_ * 2 };
}

double goncharova::Circle::getArea() const
{
  return radius_ * radius_* M_PI;
}

void goncharova::Circle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void goncharova::Circle::move(const point_t &newPoint)
{
  center_ = newPoint;
}

void goncharova::Circle::inform() const
{
  std::cout << "\n X: " << center_.x;
  std::cout << "\n Y: " << center_.y;
  std::cout << "\n Radius: " << radius_;
  std::cout << "\n Area: " << getArea();
}

void goncharova::Circle::scale(double scaleCoef)
{
  if (scaleCoef < 0.0)
  {
    throw std::invalid_argument("scaleCoef should be positive value!");
  }
  radius_ *= scaleCoef;
}

void goncharova::Circle::rotate(const double)
{
}
