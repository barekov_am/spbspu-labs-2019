#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace goncharova
{
    class Shape
    {
    public:
        virtual ~Shape() = default;
        virtual double getArea() const = 0;
        virtual rectangle_t getFrameRect() const = 0;
        virtual void move(const point_t &newPoint) = 0;
        virtual void move(double dx, double dy) = 0;
        virtual void inform() const = 0;
        virtual void scale(double scaleCoef) = 0;
        virtual void rotate(const double) = 0;
    };

    using shapePtr = std::shared_ptr<Shape>;
    using dynamicArray = std::unique_ptr<shapePtr[]>;
}

#endif
