#include "polygon.hpp"
#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <cmath>

const double FullAngle = 360.0;

goncharova::Polygon::Polygon() :
  vertex_(nullptr),
  num_(0),
  angle_(0.0)
{
}

goncharova::Polygon::Polygon(const point_t *vertex, int num) :
  num_(num),
  angle_(0.0)
{
  vertex_ = new point_t[num_];
  for (int i = 0; i < num_; ++i)
  {
    vertex_[i] = vertex[i];
  }

  center_ = getCenter();
  if ((num_ <= 2) || (getArea() <= 0.0))
  {
    delete[] vertex_;
    throw std::invalid_argument("Area must be >0,there should be 3+ corners");
  }
}

goncharova::Polygon::Polygon(const Polygon &other) :
  num_(other.num_),
  center_(other.center_)
{
  vertex_ = new point_t[num_];
  for (int i = 0; i < num_; ++i)
  {
    vertex_[i] = other.vertex_[i];
  }
}

goncharova::Polygon::Polygon(Polygon &&other) :
  vertex_(other.vertex_),
  num_(other.num_),
  center_(other.center_)
{
  other.vertex_ = nullptr;
  other.num_ = 0;
  other.center_ = {0.0, 0.0};
}

goncharova::Polygon &goncharova::Polygon::operator=(const Polygon &other) {
  if (this != &other)
  {
    delete[] vertex_;
    num_ = other.num_;
    center_ = other.center_;
    vertex_ = new point_t[num_];

    for (int i = 0; i < num_; ++i)
    {
      vertex_[i] = other.vertex_[i];
    }
  }

  return *this;
}

goncharova::Polygon &goncharova::Polygon::operator=(Polygon &&other) {
  if (this != &other)
  {
    num_ = other.num_;
    center_ = other.center_;
    delete[] vertex_;
    other.num_ = 0;
    other.center_ = {0.0, 0.0};

    vertex_ = other.vertex_;
    other.vertex_ = nullptr;
  }
  return *this;
}

goncharova::Polygon::~Polygon() {
  delete[] vertex_;
}

goncharova::rectangle_t goncharova::Polygon::getFrameRect() const {
  point_t min = vertex_[0];
  point_t max = vertex_[0];

  for (int i = 1; i < num_; ++i)
  {
    min.x = std::min(vertex_[i].x, min.x);
    min.y = std::min(vertex_[i].y, min.y);

    max.x = std::max(vertex_[i].x, max.x);
    max.y = std::max(vertex_[i].y, max.y);
  }

  double width = max.x - min.x;
  double height = max.y - min.y;

  point_t trCenter = {(max.x + min.x) / 2, (max.y + min.y) / 2};

  return {trCenter, width, height};
}

double goncharova::Polygon::getArea() const {
  double sum = 0;

  for (int i = 0; i < num_; ++i)
  {
    point_t topNext = {0, 0};
    if (i == num_ - 1)
    {
      topNext = vertex_[0];
    }
    else
    {
      topNext = vertex_[i + 1];
    }

    goncharova::Triangle triangle1(vertex_[i], topNext, center_);
    sum += triangle1.getArea();
  }
  return sum;
}

goncharova::point_t goncharova::Polygon::getCenter() const {
  point_t sum = {0, 0};
  for (int i = 0; i < num_; ++i)
  {
    sum.x += vertex_[i].x;
    sum.y += vertex_[i].y;
  }

  return {sum.x / num_, sum.y / num_};
}

void goncharova::Polygon::move(const double dx, const double dy) {
  for (int i = 0; i < num_; ++i)
  {
    vertex_[i].x += dx;
    vertex_[i].y += dy;
  }

  center_.x += dx;
  center_.y += dy;
}

void goncharova::Polygon::move(const point_t &newPoint) {
  double dx = newPoint.x - center_.x;
  double dy = newPoint.y - center_.y;

  move(dx, dy);
}

void goncharova::Polygon::inform() const {
  std::cout << "X: " << center_.x;
  std::cout << "\n Y: " << center_.y;

  for (int i = 0; i < num_; ++i)
  {
    std::cout << "Point " << i << " X:" << vertex_[i].x;
    std::cout << "Point " << i << " Y:" << vertex_[i].y;
  }

  std::cout << "\n Area: " << getArea();
}

void goncharova::Polygon::scale(double scaleCoef) {
  if (scaleCoef < 0)
  {
    throw std::invalid_argument("scaleCoef should be positive value!");
  }

  point_t prev = vertex_[0];
  point_t current;

  for (int i = 1; i < num_; ++i)
  {
    current = vertex_[i];
    vertex_[i] = {vertex_[i - 1].x + scaleCoef * (current.x - prev.x),
        vertex_[i - 1].y + scaleCoef * (current.y - prev.y)};
    prev = current;
  }

  point_t oldCenter = center_;
  center_ = getCenter();
  move(oldCenter.x - center_.x, oldCenter.y - center_.y);
}

void goncharova::Polygon::rotate(const double angle)
{
  angle_ = angle;

  if (angle_ < 0.0)
  {
    angle_ = FullAngle + fmod(angle_, FullAngle);
  }
  else
  {
    angle_ = fmod(angle_, FullAngle);
  }

  const double radAngle = angle_ * M_PI / 180;

  for (int i = 0; i < num_; i++)
  {
    const double oldX = vertex_[i].x - center_.x;
    const double oldY = vertex_[i].y - center_.y;

    const double newX = oldX * fabs(cos(radAngle)) - oldY * fabs(sin(radAngle));
    const double newY = oldX * fabs(sin(radAngle)) + oldY * fabs(cos(radAngle));

    vertex_[i] = {center_.x + newX, center_.y + newY};
  }
}
