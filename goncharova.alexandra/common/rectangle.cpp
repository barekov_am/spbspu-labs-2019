#define _USE_MATH_DEFINES
#include <cmath>
#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>

const double FullAngle = 360.0;

goncharova::Rectangle::Rectangle(const point_t &center, const double width, const double height) :
  center_(center),
  width_(width),
  height_(height),
  angle_(0.0)
{
  if ((width_ <= 0.0) || (height_ <= 0.0))
  {
    throw std::invalid_argument("Width and height must be positive numbers");
  }
}

goncharova::rectangle_t goncharova::Rectangle::getFrameRect() const
{
  const double radAngle = angle_ * M_PI / 180;
  const double width = width_ * fabs(cos(radAngle)) + height_ * fabs(sin(radAngle));
  const double height = width_ * fabs(sin(radAngle)) + height_ * fabs(cos(radAngle));

  return {center_, width, height,};
}

double goncharova::Rectangle::getArea() const
{
  return width_ * height_;
}

void goncharova::Rectangle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void goncharova::Rectangle::move(const point_t &newPoint)
{
  center_ = newPoint;
}

void goncharova::Rectangle::inform() const
{
  std::cout << "X: " << center_.x;
  std::cout << "\n Y: " << center_.y;
  std::cout << "\n Height: " << height_;
  std::cout << "\n Width: " << width_;
  std::cout << "\n Area: " << getArea();
}

void goncharova::Rectangle::scale(double scaleCoef)
{
  if (scaleCoef < 0)
  {
    throw std::invalid_argument("scaleCoef should be positive value!");
  }
  height_ *= scaleCoef;
  width_ *= scaleCoef;
}

void goncharova::Rectangle::rotate(const double angle)
{
  angle_ += angle;
  if (angle_ < 0.0)
  {
    angle_ = FullAngle + fmod(angle_, FullAngle);
  }
  else
  {
    angle_ = fmod(angle_, FullAngle);
  }
}
