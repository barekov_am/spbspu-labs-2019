#include "separation.hpp"

#include <cmath>

bool goncharova::cross(const rectangle_t &lhs, const rectangle_t &rhs)
{
  const bool firstCondition = (fabs(lhs.pos.x - rhs.pos.x) < (lhs.width + rhs.width) / 2);
  const bool secondCondition = (fabs(lhs.pos.y - rhs.pos.y) < (lhs.height + rhs.height) / 2);

  return firstCondition && secondCondition;
}

goncharova::Matrix goncharova::part(const dynamicArray &array, size_t size)
{
  Matrix tmpMatrix;

  for (size_t i = 0; i < size; i++)
  {
    size_t rightLine = 0;
    size_t rightColumn = 0;

    for (size_t j = 0; j < tmpMatrix.getLines(); j++)
    {

      for (size_t k = 0; k < tmpMatrix.getColumns(); k++)
      {

        if (tmpMatrix[j][k] == nullptr)
        {
          rightLine = j;
          rightColumn = k;
          break;
        }

        if (cross(array[i]->getFrameRect(), tmpMatrix[j][k]->getFrameRect()))
        {
          rightLine = j + 1;
          rightColumn = 0;
          break;
        }
        else
        {
          rightLine = j;
          rightColumn = k + 1;
        }
      }

      if (rightLine == j)
      {
        break;
      }
    }

    tmpMatrix.add(array[i], rightLine, rightColumn);
  }

  return tmpMatrix;
}

goncharova::Matrix goncharova::part(const CompositeShape &composite)
{
  return part(composite.getList(), composite.getSize());
}
