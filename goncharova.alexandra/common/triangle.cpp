#include "triangle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>

const double FullAngle = 360.0;

goncharova::Triangle::Triangle(const point_t &pointA, const point_t &pointB, const point_t &pointC) :
  pointA_(pointA),
  pointB_(pointB),
  pointC_(pointC),
  center_(getCenter()),
  angle_(0.0)
{
  if (fabs((pointB_.x - pointA_.x) * (center_.y - pointA_.y)
      - (center_.x - pointA_.x) * (pointB_.y - pointA_.y)) <= pow(10, -8))
  {
    throw std::invalid_argument("Points cant be on the same line");
  }
}

double goncharova::Triangle::getArea() const
{
  double square = fabs((pointA_.x - pointC_.x) * (pointB_.y - pointC_.y)
      - (pointB_.x - pointC_.x) * (pointA_.y - pointC_.y)) / 2;
  return square;
}

goncharova::rectangle_t goncharova::Triangle::getFrameRect() const
{
  point_t point1 = {std::min(pointA_.x, std::min(pointB_.x, pointC_.x)),
      std::min(pointA_.y, std::min(pointB_.y, pointC_.y))};
  point_t point2 = {std::max(pointA_.x, std::max(pointB_.x, pointC_.x)),
      std::max(pointA_.y, std::max(pointB_.y, pointC_.y))};

  double width = point2.x - point1.x;
  double height = point2.y - point1.y;
  point_t trCenter = {(point1.x + point2.x) / 2, (point1.y + point2.y) / 2};

  return {trCenter, width, height};
}

void goncharova::Triangle::shift(const double dx, const double dy)
{
  pointA_.x += dx;
  pointB_.x += dx;
  pointC_.x += dx;

  pointA_.y += dy;
  pointB_.y += dy;
  pointC_.y += dy;
}

void goncharova::Triangle::move(const double dx, const double dy)
{
  shift(dx, dy);

  center_.x += dx;
  center_.y += dy;
}

void goncharova::Triangle::move(const point_t &newPoint)
{
  double dx = newPoint.x - center_.x;
  double dy = newPoint.y - center_.y;
  center_  = newPoint;

  shift(dx, dy);
}

void goncharova::Triangle::inform() const
{
  std::cout << "X: " << round(center_.x * 100) / 100;
  std::cout << "\n Y: " << round(center_.y * 100) / 100;
  std::cout << "\n A: " << pointA_.x << "; " << pointA_.y;
  std::cout << "\n B: " << pointB_.x << "; " << pointB_.y;
  std::cout << "\n C: " << pointC_.x << "; " << pointC_.y;
  std::cout << "\n Area: " << getArea();
}

goncharova::point_t goncharova::Triangle::getCenter() const
{
  point_t trCenter = {(pointA_.x + pointB_.x + pointC_.x) / 3, (pointA_.y + pointB_.y + pointC_.y) / 3};
  return trCenter;
}

void goncharova::Triangle::scale(double scaleCoef)
{
  if (scaleCoef < 0)
  {
    throw std::invalid_argument("scaleCoef should be positive value!");
  }

  pointB_={pointA_.x + scaleCoef * (pointB_.x - pointA_.x), pointA_.y + scaleCoef * (pointB_.y - pointA_.y)};
  pointC_={pointA_.x + scaleCoef * (pointC_.x - pointA_.x), pointA_.y + scaleCoef * (pointC_.y - pointA_.y)};

  point_t oldCenter = center_;
  center_ = getCenter();
  move(oldCenter.x - center_.x, oldCenter.y - center_.y);
}

void goncharova::Triangle::rotate(const double angle)
{
  angle_ = angle;

  if (angle_ < 0.0)
  {
    angle_ = FullAngle + fmod(angle_, FullAngle);
  }
  else
  {
    angle_ = fmod(angle_, FullAngle);
  }

  const double radAngle = angle_ * M_PI / 180;

  rotatePoint(pointA_, radAngle);
  rotatePoint(pointB_, radAngle);
  rotatePoint(pointC_, radAngle);
}

void goncharova::Triangle::rotatePoint( point_t &point, const double radAngle)
{
  const double oldX = point.x - center_.x;
  const double oldY = point.y - center_.y;

  const double newX = oldX * fabs(cos(radAngle)) - oldY * fabs(sin(radAngle));
  const double newY = oldX * fabs(sin(radAngle)) + oldY * fabs(cos(radAngle));

  point = {center_.x + newX, center_.y + newY};
}
