#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "separation.hpp"

BOOST_AUTO_TEST_SUITE(matrixTest)

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithCrossing)
{
  const size_t layersValue = 2;
  const size_t columnsValue = 1;

  goncharova::Rectangle testRectangle({5.0, 5.0}, 4.0, 2.0);
  goncharova::Circle testCircle({5.0, 5.0}, 2.0);
  goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
  goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

  goncharova::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  goncharova::Matrix testMatrix = goncharova::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(correctValueOfLayersAndColumnsWithoutCrossing)
{
  const size_t layersValue = 1;
  const size_t columnsValue = 2;

  goncharova::Rectangle testRectangle({5.0, 5.0}, 4.0, 2.0);
  goncharova::Circle testCircle({10.0, 5.0}, 2.0);
  goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
  goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

  goncharova::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  goncharova::Matrix testMatrix = goncharova::part(testCompositeShape);

  BOOST_CHECK_EQUAL(layersValue, testMatrix.getLines());
  BOOST_CHECK_EQUAL(columnsValue, testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(invalidValues)
{
  goncharova::Rectangle testRectangle({5.0, 5.0}, 4.0, 2.0);
  goncharova::Circle testCircle({5.0, 5.0}, 2.0);
  goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
  goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

  goncharova::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  goncharova::Matrix testMatrix = goncharova::part(testCompositeShape);

  BOOST_CHECK_THROW(testMatrix[5][0], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-5][0], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(checkCopyAndMove)
{
  goncharova::Rectangle testRectangle({5.0, 5.0}, 4.0, 2.0);
  goncharova::Circle testCircle({5.0, 5.0}, 2.0);
  goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
  goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

  goncharova::CompositeShape testCompositeShape(rectanglePtr);

  testCompositeShape.add(circlePtr);

  goncharova::Matrix testMatrix = goncharova::part(testCompositeShape);
  goncharova::Matrix copyMatrix(testMatrix);
  goncharova::Matrix moveMatrix(std::move(testMatrix));

  BOOST_CHECK_EQUAL(moveMatrix.getLines(), copyMatrix.getLines());
  BOOST_CHECK_EQUAL(moveMatrix.getColumns(), copyMatrix.getColumns());
  BOOST_CHECK(moveMatrix == copyMatrix);
}

BOOST_AUTO_TEST_SUITE_END()
