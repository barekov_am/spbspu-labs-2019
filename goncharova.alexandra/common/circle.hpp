#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace goncharova
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t &center, double radius);
    double getArea() const;
    rectangle_t getFrameRect() const;
    void move(const point_t &newPoint);
    void move(double dx, double dy);
    void inform() const;
    void scale(double scaleCoef);
    void rotate(const double);

  private:
    point_t center_;
    double radius_;
  };
}

#endif
