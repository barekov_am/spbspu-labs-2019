#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "triangle.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(triangleTest)

BOOST_AUTO_TEST_CASE(movingTriangle)
{
  goncharova::Triangle triangle({ 1, 1 }, { 3, 3 }, { 0, 5 });
  const goncharova::rectangle_t oldFrame = triangle.getFrameRect();
  const double oldSquare = triangle.getArea();

  triangle.move(6.0, 6.0);
  goncharova::rectangle_t newFrame = triangle.getFrameRect();
  BOOST_CHECK_CLOSE(triangle.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.height, oldFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.width, oldFrame.width, EPSILON);

  triangle.move({2.0, 2.0});
  newFrame = triangle.getFrameRect();
  BOOST_CHECK_CLOSE(triangle.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.height, oldFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.width, oldFrame.width, EPSILON);
}

BOOST_AUTO_TEST_CASE(scalingTriangle)
{
  goncharova::Triangle triangle({ 1, 1 }, { 3, 3 }, { 0, 5 });
  const double oldSquare = triangle.getArea();
  const double scaleCoef = 3.0;
  triangle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(triangle.getArea(), oldSquare * scaleCoef * scaleCoef, EPSILON);
}

BOOST_AUTO_TEST_CASE(validTriangle)
{
  BOOST_CHECK_THROW (goncharova::Triangle({ 1, 1 }, { 2, 2 }, { 3, 3 }), std::invalid_argument);

  goncharova::Triangle triangle({ 3, 1 }, { 2, 2 }, { 4, 3 });
  BOOST_CHECK_THROW(triangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rotation)
    {
        goncharova::Triangle triangle({ 1, 1 }, { 3, 3 }, { 0, 5 });
    double area = triangle.getArea();
    triangle.rotate(45);
    BOOST_CHECK_CLOSE(area, triangle.getArea(), EPSILON);
    }

BOOST_AUTO_TEST_SUITE_END()
