#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(circleTest)

BOOST_AUTO_TEST_CASE(movingCircle) {
  goncharova::Circle circle({{5.0, 5.0}, 5.5});
  const goncharova::rectangle_t oldFrame = circle.getFrameRect();
  const double oldSquare = circle.getArea();

  circle.move(6.0, 6.0);
  goncharova::rectangle_t newFrame = circle.getFrameRect();
  BOOST_CHECK_CLOSE(circle.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.height, oldFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.width, oldFrame.width, EPSILON);

  circle.move({2.0, 2.0});
  newFrame = circle.getFrameRect();
  BOOST_CHECK_CLOSE(circle.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.height, oldFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.width, oldFrame.width, EPSILON);
}

BOOST_AUTO_TEST_CASE(scalingCircle) {
  goncharova::Circle circle({{5.0, 5.0}, 6.5});
  const double oldSquare = circle.getArea();

  const double scaleCoef = 3.0;
  circle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(circle.getArea(), oldSquare * scaleCoef * scaleCoef, EPSILON);
}

BOOST_AUTO_TEST_CASE(validCircle) {
  BOOST_CHECK_THROW(goncharova::Circle({2.0, 6.0}, -5.0), std::invalid_argument);

  goncharova::Circle circle({2.0, 7.0}, 6.5);
  BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rotation)
{
  goncharova::Circle circle({{5.0, 5.0}, 6.5});
  double area = circle.getArea();
  circle.rotate(45);
  BOOST_CHECK_CLOSE(area, circle.getArea(), EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()
