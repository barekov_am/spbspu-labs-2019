#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace goncharova
{
    class CompositeShape: public Shape
    {
    public:
        CompositeShape();
        CompositeShape(const CompositeShape &);
        CompositeShape(CompositeShape &&);
        CompositeShape(const shapePtr &);
        ~CompositeShape() = default;
        CompositeShape &operator =(const CompositeShape &);
        CompositeShape &operator =(CompositeShape &&);
        shapePtr operator [](size_t) const;
        double getArea() const ;
        rectangle_t getFrameRect() const ;
        void move(const point_t &point) ;
        void move(const double, const double) ;
        void inform() const;
        void scale(const double) ;
        void rotate(const double) ;
        size_t getSize() const;
        void add(const shapePtr &);
        void remove(size_t);
        dynamicArray getList() const;

    private:
        size_t size_;
        dynamicArray shapeArray_;
        double angle_;
    };
}

#endif
