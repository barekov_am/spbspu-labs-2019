#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "shape.hpp"

namespace goncharova
{
  class Polygon : public Shape {
  public:
    Polygon();
    Polygon(const Polygon &other);
    Polygon(Polygon &&other);
    Polygon(const point_t *vertex, int num);
    virtual ~Polygon();
    Polygon &operator =(const Polygon &other);
    Polygon &operator =(Polygon &&other);

    double getArea() const;
    point_t getCenter() const;
    rectangle_t getFrameRect() const;
    void move(const point_t &newPoint);
    void move(double dx, double dy);
    void inform() const;
    void scale(double scaleCoef);
    void rotate(const double);

  private:
    point_t *vertex_;
    int num_;
    point_t center_;
    double angle_;
  };
}

#endif
