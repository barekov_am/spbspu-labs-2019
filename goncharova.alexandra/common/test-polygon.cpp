#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "polygon.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(polygonTest)

BOOST_AUTO_TEST_CASE(movingPolygon)
{
  goncharova::point_t *dots = new goncharova::point_t[5]{{1, 1}, {4, 2}, {4, 4}, {3, 4}, {1, 3}};
  goncharova::Polygon polygon(dots, 5);
  const goncharova::rectangle_t oldFrame = polygon.getFrameRect();
  const double oldSquare = polygon.getArea();

  polygon.move(6.0, 6.0);
  goncharova::rectangle_t newFrame = polygon.getFrameRect();
  BOOST_CHECK_CLOSE(polygon.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.height, oldFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.width, oldFrame.width, EPSILON);

  polygon.move({2.0, 2.0});
  newFrame = polygon.getFrameRect();
  BOOST_CHECK_CLOSE(polygon.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.height, oldFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.width, oldFrame.width, EPSILON);

  delete [] dots;
}

BOOST_AUTO_TEST_CASE(scalingPolygon)
{
  goncharova::point_t *dots = new goncharova::point_t[5]{{1, 1}, {4, 2}, {4, 4}, {3, 4}, {1, 3}};
  goncharova::Polygon polygon(dots, 5);
  const double oldSquare = polygon.getArea();

  const double scaleCoef = 3.0;
  polygon.scale(scaleCoef);
  BOOST_CHECK_CLOSE(polygon.getArea(), oldSquare * scaleCoef * scaleCoef, EPSILON);
  delete [] dots;
}

BOOST_AUTO_TEST_CASE(validPolygon)
{
  goncharova::point_t *dots = new goncharova::point_t[2]{{3, 4}, {1, 3}};
  BOOST_CHECK_THROW (goncharova::Polygon(dots, 2), std::invalid_argument);
  delete[] dots;

  goncharova::point_t *dots1 = new goncharova::point_t[5]{{1, 1}, {4, 2}, {4, 4}, {3, 4}, {1, 3}};
  goncharova::Polygon polygon(dots1, 5);
  BOOST_CHECK_THROW(polygon.scale(-1), std::invalid_argument);
  delete[] dots1;
}

BOOST_AUTO_TEST_CASE(rotation)
    {
        goncharova::point_t *dots = new goncharova::point_t[5]{{1, 1}, {4, 2}, {4, 4}, {3, 4}, {1, 3}};
    goncharova::Polygon polygon(dots, 5);
    double area = polygon.getArea();
    polygon.rotate(45);
    BOOST_CHECK_CLOSE(area, polygon.getArea(), EPSILON);
    delete[] dots;
    }

BOOST_AUTO_TEST_SUITE_END()
