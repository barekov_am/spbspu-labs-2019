#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(compositeShapeTest)

BOOST_AUTO_TEST_CASE(constantParametersAfterMovingToDistance)
    {
        goncharova::Rectangle testRectangle({5.0, 3.9}, 2.1, 3.3);
    goncharova::Circle testCircle({5.9, 3.0}, 2.5);
    goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
    goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

    goncharova::CompositeShape testCompositeShape(rectanglePtr);

    testCompositeShape.add(circlePtr);

    const double testArea = testCompositeShape.getArea();
    const goncharova::rectangle_t testFrameRect = testCompositeShape.getFrameRect();

    testCompositeShape.move(1.0, 8.3);

    BOOST_CHECK_CLOSE(testFrameRect.width, testCompositeShape.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.height, testCompositeShape.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(testArea, testCompositeShape.getArea(), EPSILON);
    }

BOOST_AUTO_TEST_CASE(constantParametersAfterMovingToPoint)
    {
        goncharova::Rectangle testRectangle({5.0, 3.9}, 2.1, 3.3);
    goncharova::Circle testCircle({5.9, 3.0}, 2.5);
    goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
    goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

    goncharova::CompositeShape testCompositeShape(rectanglePtr);

    testCompositeShape.add(circlePtr);

    const double testArea = testCompositeShape.getArea();
    const goncharova::rectangle_t testFrameRect = testCompositeShape.getFrameRect();

    testCompositeShape.move({3.1, 5.0});

    BOOST_CHECK_CLOSE(testFrameRect.width, testCompositeShape.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.height, testCompositeShape.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(testArea, testCompositeShape.getArea(), EPSILON);
    }

BOOST_AUTO_TEST_CASE(squareIncreaseAreaAfterScale)
    {
        goncharova::Rectangle testRectangle({5.0, 3.9}, 2.1, 3.3);
    goncharova::Circle testCircle({5.9, 3.0}, 2.5);
    goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
    goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

    goncharova::CompositeShape testCompositeShape(rectanglePtr);

    testCompositeShape.add(circlePtr);

    const double testArea = testCompositeShape.getArea();
    const double coefficient = 2.1;

    testCompositeShape.scale(coefficient);

    BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testArea * coefficient * coefficient, EPSILON);
    }

BOOST_AUTO_TEST_CASE(squareDecreaseAreaAfterScale)
    {
        goncharova::Rectangle testRectangle({5.0, 3.9}, 2.1, 3.3);
    goncharova::Circle testCircle({5.9, 3.0}, 2.5);
    goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
    goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

    goncharova::CompositeShape testCompositeShape(rectanglePtr);

    testCompositeShape.add(circlePtr);

    const double testArea = testCompositeShape.getArea();
    const double coefficient = 0.6;

    testCompositeShape.scale(coefficient);

    BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testArea * coefficient * coefficient, EPSILON);
    }

BOOST_AUTO_TEST_CASE(areaAfterRotation)
    {
    goncharova::Rectangle testRectangle({5.0, 5.0}, 4.0, 2.0);
    goncharova::Circle testCircle({5.0, 5.0}, 1.0);
    goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
    goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

    goncharova::CompositeShape testCompositeShape(rectanglePtr);

    testCompositeShape.add(circlePtr);

    const double testArea = testCompositeShape.getArea();
    const double angle = 45;

    testCompositeShape.rotate(angle);

    BOOST_CHECK_CLOSE(testArea, testCompositeShape.getArea(), EPSILON);
    }

BOOST_AUTO_TEST_CASE(parametersAfterAddingAndDeletion)
    {
        goncharova::Rectangle testRectangle({5.0, 3.9}, 2.1, 3.3);
    goncharova::Circle testCircle({5.9, 3.0}, 2.5);
    goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
    goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

    goncharova::CompositeShape testCompositeShape(rectanglePtr);

    const double testCompositeArea = testCompositeShape.getArea();
    const double testCircleArea = testCircle.getArea();

    testCompositeShape.add(circlePtr);

    BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea + testCircleArea, EPSILON);

    testCompositeShape.remove(1);

    BOOST_CHECK_CLOSE(testCompositeShape.getArea(), testCompositeArea, EPSILON);
    }

BOOST_AUTO_TEST_CASE(invalidValues)
    {
        goncharova::Rectangle testRectangle({5.0, 3.9}, 2.1, 3.3);
    goncharova::Circle testCircle({5.9, 3.0}, 2.5);
    goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
    goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

    goncharova::CompositeShape testCompositeShape(rectanglePtr);

    testCompositeShape.add(circlePtr);

    BOOST_CHECK_THROW(testCompositeShape.add(nullptr), std::invalid_argument);

    BOOST_CHECK_THROW(testCompositeShape.scale(-3.0), std::invalid_argument);
    BOOST_CHECK_THROW(testCompositeShape.scale(0.0), std::invalid_argument);

    BOOST_CHECK_THROW(testCompositeShape.remove(4), std::out_of_range);
    BOOST_CHECK_THROW(testCompositeShape.remove(-2), std::out_of_range);

    BOOST_CHECK_THROW(testCompositeShape[4], std::out_of_range);
    BOOST_CHECK_THROW(testCompositeShape[-2], std::out_of_range);
    }

BOOST_AUTO_TEST_CASE(checkCopyConstructor)
    {
        goncharova::Rectangle testRectangle({5.0, 3.9}, 2.1, 3.3);
    goncharova::Circle testCircle({5.9, 3.0}, 2.5);
    goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
    goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

    goncharova::CompositeShape testCompositeShape(rectanglePtr);

    testCompositeShape.add(circlePtr);

    const goncharova::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
    const double testCompositeArea = testCompositeShape.getArea();
    const int testCompositeSize = testCompositeShape.getSize();

    goncharova::CompositeShape copyCompositeShape(testCompositeShape);

    const goncharova::rectangle_t copyFrameRect = copyCompositeShape.getFrameRect();

    BOOST_CHECK_CLOSE(testCompositeArea, copyCompositeShape.getArea(), EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.width, copyFrameRect.width, EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.height, copyFrameRect.height, EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.pos.x, copyFrameRect.pos.x, EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.pos.y, copyFrameRect.pos.y, EPSILON);
    BOOST_CHECK_EQUAL(testCompositeSize, copyCompositeShape.getSize());
    }

BOOST_AUTO_TEST_CASE(checkMoveConstructor)
    {
        goncharova::Rectangle testRectangle({5.0, 3.9}, 2.1, 3.3);
    goncharova::Circle testCircle({5.9, 3.0}, 2.5);
    goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
    goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

    goncharova::CompositeShape testCompositeShape(rectanglePtr);

    testCompositeShape.add(circlePtr);

    const goncharova::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
    const double testCompositeArea = testCompositeShape.getArea();
    const int testCompositeSize = testCompositeShape.getSize();

    goncharova::CompositeShape moveCompositeShape(std::move(testCompositeShape));

    const goncharova::rectangle_t moveFrameRect = moveCompositeShape.getFrameRect();

    BOOST_CHECK_CLOSE(testCompositeArea, moveCompositeShape.getArea(), EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.width, moveFrameRect.width, EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.height, moveFrameRect.height, EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.pos.x, moveFrameRect.pos.x, EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.pos.y, moveFrameRect.pos.y, EPSILON);
    BOOST_CHECK_EQUAL(testCompositeSize, moveCompositeShape.getSize());
    }

BOOST_AUTO_TEST_CASE(checkCopyOperator)
    {
        goncharova::Rectangle testRectangle({5.0, 3.9}, 2.1, 3.3);
    goncharova::Circle testCircle({5.9, 3.0}, 2.5);
    goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
    goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

    goncharova::CompositeShape testCompositeShape(rectanglePtr);

    testCompositeShape.add(circlePtr);

    const goncharova::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
    const double testCompositeArea = testCompositeShape.getArea();
    const int testCompositeSize = testCompositeShape.getSize();

    goncharova::Circle testCircleNew({3.1, 4.7}, 3.1);
    goncharova::shapePtr circleNewPtr = std::make_shared<goncharova::Circle>(testCircle);

    goncharova::CompositeShape copyCompositeShape(circleNewPtr);

    copyCompositeShape = testCompositeShape;

    const goncharova::rectangle_t copyFrameRect = copyCompositeShape.getFrameRect();

    BOOST_CHECK_CLOSE(testCompositeArea, copyCompositeShape.getArea(), EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.width, copyFrameRect.width, EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.height, copyFrameRect.height, EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.pos.x, copyFrameRect.pos.x, EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.pos.y, copyFrameRect.pos.y, EPSILON);
    BOOST_CHECK_EQUAL(testCompositeSize, copyCompositeShape.getSize());
    }

BOOST_AUTO_TEST_CASE(checkMoveOperator)
    {
        goncharova::Rectangle testRectangle({5.0, 3.9}, 2.1, 3.3);
    goncharova::Circle testCircle({5.9, 3.0}, 2.5);
    goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(testRectangle);
    goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(testCircle);

    goncharova::CompositeShape testCompositeShape(rectanglePtr);

    testCompositeShape.add(circlePtr);

    const goncharova::rectangle_t testFrameRect = testCompositeShape.getFrameRect();
    const double testCompositeArea = testCompositeShape.getArea();
    const int testCompositeSize = testCompositeShape.getSize();

    goncharova::CompositeShape moveCompositeShape;

    moveCompositeShape = std::move(testCompositeShape);

    const goncharova::rectangle_t moveFrameRect = moveCompositeShape.getFrameRect();

    BOOST_CHECK_CLOSE(testCompositeArea, moveCompositeShape.getArea(), EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.width, moveFrameRect.width, EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.height, moveFrameRect.height, EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.pos.x, moveFrameRect.pos.x, EPSILON);
    BOOST_CHECK_CLOSE(testFrameRect.pos.y, moveFrameRect.pos.y, EPSILON);
    BOOST_CHECK_EQUAL(testCompositeSize, moveCompositeShape.getSize());
    }

BOOST_AUTO_TEST_SUITE_END()
