#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

int main() {
  goncharova::point_t dot = {3, 4};
  goncharova::Circle circle1(dot, 15);
  goncharova::Rectangle rectangle1(dot, 10, 10);
  goncharova::Triangle triangle1({0, 0}, {3, 2}, {1, 6});

  goncharova::point_t *dots = new goncharova::point_t[5]{{1, 1}, {4, 2}, {4, 4}, {3, 4}, {1, 3}};
  goncharova::Polygon polygon1(dots, 5);

  std::cout << "Area of circle1: ";
  std::cout << circle1.getArea() << "\n";
  std::cout << "Height of frame rectangle: ";
  std::cout << circle1.getFrameRect().height << "\n";
  std::cout << "Width of frame rectangle: ";
  std::cout << circle1.getFrameRect().width << "\n";

  goncharova::point_t movePoint = {5, 6};
  circle1.move(movePoint);
  std::cout << "Circle moved to: ";
  circle1.inform();
  std::cout << "\n";

  circle1.move(7.0, 8.0);
  std::cout << "Circle moved to: ";
  circle1.inform();
  std::cout << "\n";

  circle1.scale(2);
  std::cout << "Circle x2: ";
  circle1.inform();
  std::cout << "\n";

  std::cout << "-----" << "\n";

  std::cout << "Area of rectangle1: ";
  std::cout << rectangle1.getArea() << "\n";
  std::cout << "Height of frame rectangle: ";
  std::cout << rectangle1.getFrameRect().height << "\n";
  std::cout << "Width of frame rectangle: ";
  std::cout << rectangle1.getFrameRect().width << "\n";

  rectangle1.move(movePoint);
  std::cout << "Rectangle moved to: ";
  rectangle1.inform();
  std::cout << "\n";

  rectangle1.move(7.0, 8.0);
  std::cout << "Rectangle moved to: ";
  rectangle1.inform();
  std::cout << "\n";

  rectangle1.scale(2);
  std::cout << "Rectangle x2: ";
  rectangle1.inform();
  std::cout << "\n";

  std::cout << "-----" << "\n";

  std::cout << "Area of triangle1: ";
  std::cout << triangle1.getArea() << "\n";
  std::cout << "Height of frame triangle: ";
  std::cout << triangle1.getFrameRect().height << "\n";
  std::cout << "Width of frame triangle: ";
  std::cout << triangle1.getFrameRect().width << "\n";

  triangle1.move(movePoint);
  std::cout << "Triangle moved to: ";
  triangle1.inform();
  std::cout << "\n";

  triangle1.move(7.0, 8.0);
  std::cout << "Triangle moved to: ";
  triangle1.inform();
  std::cout << "\n";

  triangle1.scale(2);
  std::cout << "Triangle x2: ";
  triangle1.inform();
  std::cout << "\n";

  std::cout << "-----" << "\n";

  std::cout << "Area of polygon1: ";
  std::cout << polygon1.getArea() << "\n";
  std::cout << "Height of frame polygon: ";
  std::cout << polygon1.getFrameRect().height << "\n";
  std::cout << "Width of frame polygon: ";
  std::cout << polygon1.getFrameRect().width << "\n";
  std::cout << "info:";
  polygon1.inform();

  polygon1.move(movePoint);
  std::cout << "Polygon moved to: ";
  polygon1.inform();
  std::cout << "\n";

  polygon1.move(7.0, 8.0);
  std::cout << "Polygon moved to: ";
  polygon1.inform();
  std::cout << "\n";

  polygon1.scale(2);
  std::cout << "Polygon x2: ";
  polygon1.inform();
  std::cout << "\n";

  delete[] dots;

  //demonstration for Composite Shape
  goncharova::shapePtr rectanglePtr = std::make_shared<goncharova::Rectangle>(rectangle1);
  goncharova::shapePtr circlePtr = std::make_shared<goncharova::Circle>(circle1);

  goncharova::CompositeShape compositeShape(rectanglePtr);
  compositeShape.inform();

  compositeShape.add(circlePtr);
  std::cout << "After adding new figure: \n";
  compositeShape.inform();

  compositeShape.move(3.7, 1.0);
  std::cout << "After moving dx = 3.7, dy = 1.0: \n";
  compositeShape.inform();

  compositeShape.move({10.0, 2.3});
  std::cout << "After moving to a point with coordinates {10.0, 2.3}: \n";
  compositeShape.inform();

  compositeShape.scale(2.0);
  std::cout << "After scaling with the coefficient 2.0: \n";
  compositeShape.inform();

  std::cout << "After deleting the first figure: \n";
  compositeShape.remove(0);
  compositeShape.inform();

  return 0;
}
