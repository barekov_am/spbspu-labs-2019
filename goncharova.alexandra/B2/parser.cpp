#include "parser.hpp"

#include <algorithm>
#include <sstream>

const Parser::commandExecution Parser::invalidCommand = [](stringQueue &, std::ostream & out) { out << "<INVALID COMMAND>\n"; };

Parser::commandExecution Parser::parseCommand(std::string & args)
{
  using parserExecution = std::function< Parser::commandExecution(std::string &) >;

  struct parsers_t {
    const char * name;
    parserExecution execution;
  };

  static const parsers_t parsers[] {
    { "add", &parseAdd },
    { "get", &parseGet },
    { "accelerate", &parseAccelerate }
  };

  skipSpaces(args);
  std::string parserWord = extractLexeme(args);

  auto parser = std::find_if(parsers, std::end(parsers),
      [&](const parsers_t & parser) { return parser.name == parserWord; });

  if (parser == std::end(parsers)) {
    return invalidCommand;
  } else {
    return parser->execution(args);
  }
}

void Parser::skipSpaces(std::string & str)
{
  size_t numws = 0;
  while (isblank(str[numws])) {
    ++numws;
  }

  str.erase(0, numws);
}

std::string Parser::extractLexeme(std::string & str)
{
  if (str.empty()) {
    return "";
  }

  size_t lexemeLength = 0;

  while (!isblank(str[lexemeLength])) {
    ++lexemeLength;
    if (lexemeLength == str.length()) {
      break;
    }
  }

  std::string lexeme = str.substr(0, lexemeLength);
  str.erase(0, lexemeLength);

  return lexeme;
}

Parser::commandExecution Parser::parseAdd(std::string & args)
{
  using queuePriorities = stringQueue::priority_t;

  struct priority_t {
    const char * name;
    queuePriorities priority;
  };

  static const priority_t priorities[] {
    { "low", queuePriorities::LOW },
    { "normal", queuePriorities::NORMAL },
    { "high", queuePriorities::HIGH }
  };

  skipSpaces(args);
  std::string argPriority = extractLexeme(args);

  auto priority = std::find_if(priorities, std::end(priorities),
      [&](const priority_t & priority) { return priority.name == argPriority; });

  if (priority == std::end(priorities)) {
    return invalidCommand;
  }

  skipSpaces(args);
  std::string data = args;

  if (data.empty()) {
    return invalidCommand;
  }

  return [=](stringQueue & queue, std::ostream &) { queue.putElement(data, priority->priority); };
}

Parser::commandExecution Parser::parseGet(std::string & args)
{
  skipSpaces(args);
  std::string line = args;

  if (!line.empty()) {
    return invalidCommand;
  }

  return [](stringQueue & queue, std::ostream & out) {
      if (queue.empty()) {
        out << "<EMPTY>\n";
      } else {
        queue.pullFront([&](std::string element) { out << element << "\n"; });
      }
  };
}

Parser::commandExecution Parser::parseAccelerate(std::string & args)
{
  skipSpaces(args);
  std::string line = args;

  if (!line.empty()) {
    return invalidCommand;
  }

  return [](stringQueue & q, std::ostream & out) {
      if (q.empty()) {
        out << "<EMPTY>\n";
      } else {
        q.accelerate();
      }
  };
}
