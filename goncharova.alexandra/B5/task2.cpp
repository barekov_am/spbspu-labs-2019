#include "tasks.hpp"
#include <sstream>
#include <iostream>
#include <algorithm>
#include "functions.hpp"
#include "shape.hpp"

void task2()
{
  std::string nextLine;
  std::list<Shape> shapes;

  while (std::getline(std::cin, nextLine))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error has occured while the comman was written!\n");
    }

    std::stringstream stream(nextLine);
    std::string str;
    stream >> str;
    if (str == "")
    {
      continue;
    }
    int amountOfVertices = checkNumber(str);
    std::vector<point_t> points = formPointVector(stream, amountOfVertices);

    shapes.push_back(points);
  }

  int amountOfAllVertices = 0;
  int amountOfTriangles = 0;
  int amountOfSquares = 0;
  int amountOfRectangles = 0;
  std::for_each(std::begin(shapes), std::end(shapes), [&](const Shape &shape) {
    amountOfAllVertices += shape.size();
    if (shape.size() == AMOUNT_OF_TRIANGLE_VERTICES)
    {
      ++amountOfTriangles;
    }
    else if (isRectangle(shape))
    {
      ++amountOfRectangles;
      if (isSquare(shape))
      {
        ++amountOfSquares;
      }
    }
  });

  removePentagons(shapes);

  std::vector<point_t> oneOfShapePoints = formPointVector(shapes);

  shapes.sort([&](const Shape &shape1, const Shape &shape2) {
    if (shape1.size() < shape2.size())
    {
      return true;
    }
    if ((shape1.size() == AMOUNT_OF_RECTANGLE_VERTICES) && (shape2.size() == AMOUNT_OF_RECTANGLE_VERTICES) && isSquare(shape1))
    {
      return true;
    }

    return false;
  });

  std::cout << "Vertices: " << amountOfAllVertices << "\n";
  std::cout << "Triangles: " << amountOfTriangles << "\n";
  std::cout << "Squares: " << amountOfSquares << "\n";
  std::cout << "Rectangles: " << amountOfRectangles << "\n";

  std::cout << "Points:";
  for (point_t nextPoint : oneOfShapePoints)
  {
    std::cout << " (" << nextPoint.x << "; " << nextPoint.y << ")";
  }
  std::cout << "\n";

  std::cout << "Shapes:\n";
  for (Shape nextShape : shapes)
  {
    std::cout << nextShape.size();
    for (point_t nextPoint : nextShape)
    {
      std::cout << " (" << nextPoint.x << "; " << nextPoint.y << ")";
    }
    std::cout << "\n";
  }
}
