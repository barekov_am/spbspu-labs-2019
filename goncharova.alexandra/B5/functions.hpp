#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <string>
#include <functional>
#include <list>
#include "shape.hpp"

const int AMOUNT_OF_TRIANGLE_VERTICES = 3;
const int AMOUNT_OF_RECTANGLE_VERTICES = 4;
const int AMOUNT_OF_PENTAGON_VERTICES = 5;

int checkNumber(std::string &number);
std::pair<int, int> getCoordinates(std::string &coordinates, int colonPosition);
Shape formPointVector(std::stringstream &stream, int amountOfVertices);
Shape formPointVector(std::list<Shape> &shapes);
void removePentagons(std::list<Shape> &shapes);
int getLineLength(const point_t &point1, const point_t &point2);
inline bool isRectangle(const Shape &shape)
{
  return ((shape.size() == AMOUNT_OF_RECTANGLE_VERTICES) && (getLineLength(shape[0], shape[2]) == getLineLength(shape[1], shape[3])));
}

inline bool isSquare(const Shape &shape)
{
  return isRectangle(shape) && getLineLength(shape[0], shape[1]) == getLineLength(shape[1], shape[2]);
}

#endif //FUNCTIONS_HPP
