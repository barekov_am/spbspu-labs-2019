#include "functions.hpp"
#include <algorithm>
#include <stdexcept>
#include <sstream>
#include <iostream>

int checkNumber(std::string &number)
{
  number.erase(std::remove_if(std::begin(number), std::end(number), [&](char c)
      { return ((c == ' ') || (c == '\t')); }), std::end(number));
  char* checkedPtr = nullptr;
  const int num = std::strtoll(number.c_str(), &checkedPtr, 10);
  if (*checkedPtr != '\x00')
  {
    throw std::invalid_argument("The wrong format of number!\n");
    return 1;
  }

  return num;
}

std::pair<int, int> getCoordinates(std::string &coordinates, int colonPosition)
{
  std::string strX = coordinates.substr(1, colonPosition - 1);
  std::string strY = coordinates.substr(colonPosition + 1, coordinates.length() - colonPosition - 2);

  return std::make_pair(checkNumber(strX), checkNumber(strY));
}

Shape formPointVector(std::stringstream &stream, int amountOfVertices)
{
  std::vector<point_t> points;
  std::string str;
  for (int i = 0; i < amountOfVertices; ++i)
  {
    stream >> std::ws;
    if (!std::getline(stream, str, ')'))
    {
      throw std::invalid_argument("Not enough points for input shape!\n");
    }
    str += ')';
    std::size_t colonPosition = str.find_first_of(';');
    if ((str[0] != '(') || (str[str.length() - 1] != ')') || (colonPosition == std::string::npos))
    {
      throw std::invalid_argument("Wrong format of point sent to the console!\n");
    }
    std::pair<int, int> coordinates = getCoordinates(str, colonPosition);
    points.push_back({coordinates.first, coordinates.second});
  }
  char c;
  stream >> c;

  if (!stream.eof())
  {
    throw std::invalid_argument("The extra point was included into line!\n");
  }

  return points;
}

Shape formPointVector(std::list<Shape> &shapes)
{
  std::vector<point_t> oneOfShapePoints;
  for (Shape shape : shapes)
  {
    oneOfShapePoints.push_back(shape.front());
  }

  return oneOfShapePoints;
}

void removePentagons(std::list<Shape> &shapes)
{
  shapes.remove_if([](const Shape &shape) { return shape.size() == AMOUNT_OF_PENTAGON_VERTICES; });
}

int getLineLength(const point_t &point1, const point_t &point2)
{
  return (point1.x - point2.x) * (point1.x - point2.x) + (point1.y - point2.y) * (point1.y - point2.y);
}
