#include <forward_list>
#include <vector>

#include "print.hpp"
#include "sort.hpp"

void task1(const char* direction)
{
  bool dir = getDirection(direction);

  std::vector<int> vectorOperator;
  int num = 0;
  while (std::cin >> num)
  {
    vectorOperator.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Fail while reading data.");
  }

  if (vectorOperator.empty())
  {
    return;
  }

  std::vector<int> vectorAt = vectorOperator;
  std::forward_list<int> listIterator(vectorOperator.begin(), vectorOperator.end());

  selectionSort<OperatorAccess>(vectorOperator, dir);
  selectionSort<AtAccess>(vectorAt, dir);
  selectionSort<IteratorAccess>(listIterator, dir);

  print(vectorOperator);
  print(vectorAt);
  print(listIterator);
}
