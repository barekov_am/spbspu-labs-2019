#include <random>
#include <vector>

#include "print.hpp"
#include "sort.hpp"
#include <ctime>

void Randomizer (double* array, size_t size)
{
  std::mt19937 rng(time(0));
  std::uniform_real_distribution<double> urd(-1.0, 1.0);
  for (size_t i = 0; i < size; ++i)
  {
    array[i] = urd(rng);
  }
}

void task4(const char* direction, size_t size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Size must be > 0");
  }

  bool dir = getDirection(direction);

  std::vector<double> vector(size);
  Randomizer(&vector[0], size);

  print(vector);
  selectionSort<OperatorAccess>(vector, dir);
  print(vector);
}
