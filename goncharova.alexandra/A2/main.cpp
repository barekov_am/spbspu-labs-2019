#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

int main()
{
  goncharova::point_t dot = { 3, 4 };
  goncharova::Circle circle1(dot, 15);
  goncharova::Rectangle rectangle1(dot, 10, 10);
  goncharova::Triangle triangle1({ 0, 0 }, { 3, 2 }, { 1, 6 });

  goncharova::point_t *dots = new goncharova::point_t[5]{{1, 1}, {4, 2}, {4, 4}, {3, 4}, {1, 3}};
  goncharova::Polygon polygon1(dots, 5);

  goncharova::Shape* shape1 = &circle1;
  std::cout << "Area of circle1: ";
  std::cout << shape1->getArea() << "\n";
  std::cout << "Height of frame rectangle: ";
  std::cout << shape1->getFrameRect().height << "\n";
  std::cout << "Width of frame rectangle: ";
  std::cout << shape1->getFrameRect().width << "\n";

  goncharova::point_t movePoint = { 5,6 };
  shape1->move(movePoint);
  std::cout << "Circle moved to: ";
  shape1->inform();
  std::cout << "\n";

  shape1->move(7.0, 8.0);
  std::cout << "Circle moved to: ";
  shape1->inform();
  std::cout << "\n";

  shape1->scale(2);
  std::cout << "Circle x2: ";
  shape1->inform();
  std::cout << "\n";

  std::cout << "-----" << "\n";

  shape1 = &rectangle1;
  std::cout << "Area of rectangle1: ";
  std::cout << shape1->getArea() << "\n";
  std::cout << "Height of frame rectangle: ";
  std::cout << shape1->getFrameRect().height << "\n";
  std::cout << "Width of frame rectangle: ";
  std::cout << shape1->getFrameRect().width << "\n";

  shape1->move(movePoint);
  std::cout << "Rectangle moved to: ";
  shape1->inform();
  std::cout << "\n";

  shape1->move(7.0, 8.0);
  std::cout << "Rectangle moved to: ";
  shape1->inform();
  std::cout << "\n";

  shape1->scale(2);
  std::cout << "Rectangle x2: ";
  shape1->inform();
  std::cout << "\n";

  std::cout << "-----" << "\n";

  shape1 = &triangle1;
  std::cout << "Area of triangle1: ";
  std::cout << shape1->getArea() << "\n";
  std::cout << "Height of frame triangle: ";
  std::cout << shape1->getFrameRect().height << "\n";
  std::cout << "Width of frame triangle: ";
  std::cout << shape1->getFrameRect().width << "\n";

  shape1->move(movePoint);
  std::cout << "Triangle moved to: ";
  shape1->inform();
  std::cout << "\n";

  shape1->move(7.0, 8.0);
  std::cout << "Triangle moved to: ";
  shape1->inform();
  std::cout << "\n";

  shape1->scale(2);
  std::cout << "Triangle x2: ";
  shape1->inform();
  std::cout << "\n";

  std::cout << "-----" << "\n";

  shape1 = &polygon1;
  std::cout << "Area of polygon1: ";
  std::cout << shape1->getArea() << "\n";
  std::cout << "Height of frame polygon: ";
  std::cout << shape1->getFrameRect().height << "\n";
  std::cout << "Width of frame polygon: ";
  std::cout << shape1->getFrameRect().width << "\n";
  std::cout << "info:";
  shape1->inform();

  shape1->move(movePoint);
  std::cout << "Polygon moved to: ";
  shape1->inform();
  std::cout << "\n";

  shape1->move(7.0, 8.0);
  std::cout << "Polygon moved to: ";
  shape1->inform();
  std::cout << "\n";

  shape1->scale(2);
  std::cout << "Polygon x2: ";
  shape1->inform();
  std::cout << "\n";

  delete[] dots;

  return 0;
}
