#define BOOST_TEST_MODULE A2

#include <stdexcept>

#include <boost/test/included/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteShapes)

  const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(movingRectangle)
{
  goncharova::Rectangle rectangle({{5.0, 5.0}, 2.3, 6.9});
  const goncharova::rectangle_t oldRect = rectangle.getFrameRect();
  const double oldSquare = rectangle.getArea();

  rectangle.move(6.0, 6.0);
  goncharova::rectangle_t newRect = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(rectangle.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newRect.height, oldRect.height, EPSILON);
  BOOST_CHECK_CLOSE(newRect.width, oldRect.width, EPSILON);

  rectangle.move({2.0, 2.0});
  newRect = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(rectangle.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newRect.height, oldRect.height, EPSILON);
  BOOST_CHECK_CLOSE(newRect.width, oldRect.width, EPSILON);
}

BOOST_AUTO_TEST_CASE(movingCircle)
{
  goncharova::Circle circle({{5.0, 5.0}, 5.5});
  const goncharova::rectangle_t oldFrame = circle.getFrameRect();
  const double oldSquare = circle.getArea();

  circle.move(6.0, 6.0);
  goncharova::rectangle_t newFrame = circle.getFrameRect();
  BOOST_CHECK_CLOSE(circle.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.height, oldFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.width, oldFrame.width, EPSILON);

  circle.move({2.0, 2.0});
  newFrame = circle.getFrameRect();
  BOOST_CHECK_CLOSE(circle.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.height, oldFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.width, oldFrame.width, EPSILON);
}

BOOST_AUTO_TEST_CASE(movingTriangle)
{
  goncharova::Triangle triangle({ 1, 1 }, { 3, 3 }, { 0, 5 });
  const goncharova::rectangle_t oldFrame = triangle.getFrameRect();
  const double oldSquare = triangle.getArea();

  triangle.move(6.0, 6.0);
  goncharova::rectangle_t newFrame = triangle.getFrameRect();
  BOOST_CHECK_CLOSE(triangle.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.height, oldFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.width, oldFrame.width, EPSILON);

  triangle.move({2.0, 2.0});
  newFrame = triangle.getFrameRect();
  BOOST_CHECK_CLOSE(triangle.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.height, oldFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.width, oldFrame.width, EPSILON);
}

BOOST_AUTO_TEST_CASE(movingPolygon)
{
  goncharova::point_t *dots = new goncharova::point_t[5]{{1, 1}, {4, 2}, {4, 4}, {3, 4}, {1, 3}};
  goncharova::Polygon polygon(dots, 5);
  const goncharova::rectangle_t oldFrame = polygon.getFrameRect();
  const double oldSquare = polygon.getArea();

  polygon.move(6.0, 6.0);
  goncharova::rectangle_t newFrame = polygon.getFrameRect();
  BOOST_CHECK_CLOSE(polygon.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.height, oldFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.width, oldFrame.width, EPSILON);

  polygon.move({2.0, 2.0});
  newFrame = polygon.getFrameRect();
  BOOST_CHECK_CLOSE(polygon.getArea(), oldSquare, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.height, oldFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(newFrame.width, oldFrame.width, EPSILON);

  delete [] dots;
}

BOOST_AUTO_TEST_CASE(scalingRectangle)
{
  goncharova::Rectangle rectangle({{5.0, 5.0}, 3.5, 6.5});
  const double oldSquare = rectangle.getArea();

  const double scaleCoef = 3.0;
  rectangle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(rectangle.getArea(), oldSquare * scaleCoef * scaleCoef, EPSILON);
}

BOOST_AUTO_TEST_CASE(scalingCircle)
{
  goncharova::Circle circle({{5.0, 5.0}, 6.5});
  const double oldSquare = circle.getArea();

  const double scaleCoef = 3.0;
  circle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(circle.getArea(), oldSquare * scaleCoef * scaleCoef, EPSILON);
}

BOOST_AUTO_TEST_CASE(scalingTriangle)
{
  goncharova::Triangle triangle({ 1, 1 }, { 3, 3 }, { 0, 5 });
  const double oldSquare = triangle.getArea();

  const double scaleCoef = 3.0;
  triangle.scale(scaleCoef);
  BOOST_CHECK_CLOSE(triangle.getArea(), oldSquare * scaleCoef * scaleCoef, EPSILON);
}

BOOST_AUTO_TEST_CASE(scalingPolygon)
{
  goncharova::point_t *dots = new goncharova::point_t[5]{{1, 1}, {4, 2}, {4, 4}, {3, 4}, {1, 3}};
  goncharova::Polygon polygon(dots, 5);
  const double oldSquare = polygon.getArea();

  const double scaleCoef = 3.0;
  polygon.scale(scaleCoef);
  BOOST_CHECK_CLOSE(polygon.getArea(), oldSquare * scaleCoef * scaleCoef, EPSILON);
  delete [] dots;
}

BOOST_AUTO_TEST_CASE(validRectangle)
{
  BOOST_CHECK_THROW(goncharova::Rectangle({{1.0, 1.0}, -2.0, 5.0}), std::invalid_argument);
  BOOST_CHECK_THROW(goncharova::Rectangle({{1.0, 1.0}, 2.0, -5.0}), std::invalid_argument);

  goncharova::Rectangle rectangle({{2.0, 2.0}, 2.0, 3.0});
  BOOST_CHECK_THROW(rectangle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(validCircle)
{
  BOOST_CHECK_THROW(goncharova::Circle({2.0, 6.0}, -5.0), std::invalid_argument);

  goncharova::Circle circle({2.0, 7.0}, 6.5);
  BOOST_CHECK_THROW(circle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(validTriangle)
{
  BOOST_CHECK_THROW (goncharova::Triangle({ 1, 1 }, { 2, 2 }, { 3, 3 }), std::invalid_argument);

  goncharova::Triangle triangle({ 3, 1 }, { 2, 2 }, { 4, 3 });
  BOOST_CHECK_THROW(triangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(validPolygon)
{
  goncharova::point_t *dots = new goncharova::point_t[2]{{3, 4}, {1, 3}};
  BOOST_CHECK_THROW (goncharova::Polygon(dots, 2), std::invalid_argument);
  delete[] dots;

  goncharova::point_t *dots1 = new goncharova::point_t[5]{{1, 1}, {4, 2}, {4, 4}, {3, 4}, {1, 3}};
  goncharova::Polygon polygon(dots1, 5);
  BOOST_CHECK_THROW(polygon.scale(-1), std::invalid_argument);
  delete[] dots1;
}

BOOST_AUTO_TEST_SUITE_END()
