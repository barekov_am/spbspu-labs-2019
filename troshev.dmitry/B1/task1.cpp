#include "tasks.hpp"

void troshev::tasks::itemOne(const char* sortParameter, std::istream &istream)
{
  bool spaceSwitch = true;

  std::vector<int> vecInt, vecIntPrimary;

  int number;
  while (istream >> number)
  {
    vecInt.insert(vecInt.end(), number);
  }
  if (!istream.eof())
  {
    throw std::ios_base::failure("Wrong characters.");
  }

  vecIntPrimary = vecInt;

  if (!vecInt.empty())
  {
    bool ascending;

    if (sortParameter == troshev::details::ASCENDING)
    {
      ascending = true;
    }
    else if (sortParameter == troshev::details::DESCENDING)
    {
      ascending = false;
    }
    else
    {
      throw std::invalid_argument("Wrong sorting parameter.");
    }

    troshev::details::sortVecT(vecInt, troshev::details::getByBrackets, ascending);
    troshev::details::VectorPrinter<int>(vecInt, spaceSwitch);
    std::cout << std::endl;

    vecInt = vecIntPrimary;

    troshev::details::sortVecT(vecInt, troshev::details::getByAt, ascending);
    troshev::details::VectorPrinter<int>(vecInt, spaceSwitch);
    std::cout << std::endl;

    vecInt = vecIntPrimary;

    std::vector<int>::iterator it1, it2;

    troshev::details::sortVecT(vecInt, troshev::details::getByIterator, ascending);
    troshev::details::VectorPrinter<int>(vecInt, spaceSwitch);
  }
  else
  {
    if ((sortParameter == troshev::details::ASCENDING) || (sortParameter == troshev::details::DESCENDING))
    {
      return;
    }
    else
    {
      throw std::invalid_argument("Wrong sorting parameter.");
    }
  }
}
