#include <iostream>
#include "tasks.hpp"

int main(int argc, char* argv[])
{
  if (argc <= 1)
  {
    std::cerr << "Not enough arguments." << std::endl;
    return 1;
  }
  char * ptr = nullptr;
  int task = std::strtol(argv[1], &ptr, 10);
  try {
    if (task == 1 && argc == 3)
    {
      troshev::tasks::itemOne(argv[2], std::cin);
    }

    else if (task == 2 && argc == 3)
    {
      troshev::tasks::itemTwo(argv[2]);
    }
    else if (task == 3 && argc == 2)
    {
      troshev::tasks::itemThree(std::cin);
    }
    else if (task == 4 && argc == 4)
    {
      troshev::tasks::itemFour(argv[2], argv[3]);
    }
    else
    {
      throw std::invalid_argument("Invalid arguments.");
    }
  }

  catch (const std::exception &exc)
  {
    std::cerr << exc.what() << std::endl;
    return 1;
  }

  return 0;
}
