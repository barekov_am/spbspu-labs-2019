#ifndef UTILS_HPP
#define UTILS_HPP

#include <iostream>
#include <vector>
#include <iterator>
#include <fstream>
#include <stdexcept>

namespace troshev
{
  namespace details
  {
    template<typename T>
    void VectorPrinter(std::vector<T> vecT, bool spaceOrNot)
    {
      if (spaceOrNot == true)
      {
        copy(vecT.begin(),
             vecT.end(),
             std::ostream_iterator<T>(std::cout, " ")
        );
      }
      else if (spaceOrNot == false)
      {
        copy(vecT.begin(),
             vecT.end(),
             std::ostream_iterator<T>(std::cout, "")
        );
      }
      else
      {
        std::cerr << "Code Error. Wrong parameter ( void VectorOutput )" << std::endl;
      }
    }
    inline void fillRandom(double * array, int size)
    {

      const double min = -1.0;
      const double max = 1.0;
      for (int i = 0; i < size; i++) {
        double fraction = 1.0 / (RAND_MAX);
        array[i] = rand() * fraction * (max - min + 1) + min;
      }
    }

    inline void fillRandom_ForVector(std::vector<double> &vecDouble, int size)
    {
      double *arrForVector = new double[size];
      troshev::details::fillRandom(arrForVector, size);
      for (int i(0); i < size; i++)
      {
        vecDouble.push_back(arrForVector[i]);
      }
      delete[]arrForVector;
    }

    template <typename T>
    typename T::value_type getByBrackets(const T& vecT, const typename T::iterator& it)
    {
      size_t index = std::distance<typename T::const_iterator>(vecT.begin(), it);

      if(index <= vecT.size())
      {
        return vecT[index];
      }
      else
      {
        throw std::invalid_argument("Out of range!");
      }
    }

    template <typename T>
    typename T::value_type getByAt(const T& vecT, const typename T::iterator& it) noexcept
    {
      long index = std::distance<typename T::const_iterator>(vecT.begin(), it);
      return vecT.at(index);
    }

    template <typename T>
    typename T::value_type getByIterator(const T&, const typename T::iterator& it) noexcept
    {
      return *it;
    }

    template <typename T>
    void sortVecT(T& vecT,
                  typename T::value_type(&get_elem)(const T&, const typename T::iterator&),
                  bool ascending) noexcept
    {
      using iterator_type = typename T::iterator;

      for (iterator_type it1 = vecT.begin(); it1 != vecT.end(); ++it1)
      {
        for (iterator_type it2 = std::next(it1); it2 != vecT.end(); ++it2)
        {
          if ((get_elem(vecT, it1) > get_elem(vecT, it2)) == ascending)
          {
            std::swap(*it1, *it2);
          }
        }
      }
    }

    const std::string ASCENDING = "ascending";
    const std::string DESCENDING = "descending";
  }
}
#endif
