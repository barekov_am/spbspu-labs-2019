#include "tasks.hpp"
#include <string>

void troshev::tasks::itemFour(const char* sortParameter, const char* sizeChar)
{
  bool spaceSwitch = true;

  std::vector<double> vecDouble;

  unsigned int vecSize(0);
  try
  {
    vecSize = std::stoi(sizeChar);
  }
  catch (...)
  {
    throw std::invalid_argument("Wrong length.");
  }

  if ((sortParameter != troshev::details::ASCENDING) && (sortParameter != troshev::details::DESCENDING))
  {
    throw std::invalid_argument("Wrong sorting parameter.");
  }

  troshev::details::fillRandom_ForVector(vecDouble, vecSize);

  troshev::details::VectorPrinter(vecDouble, spaceSwitch);
  std::cout << std::endl;

  if (sortParameter == troshev::details::ASCENDING)
  {
    troshev::details::sortVecT(vecDouble, troshev::details::getByIterator, true);
  }
  else if (sortParameter == troshev::details::DESCENDING)
  {
    troshev::details::sortVecT(vecDouble, troshev::details::getByIterator, false);
  }

  troshev::details::VectorPrinter(vecDouble, spaceSwitch);
}
