#include <iostream>

#include "circle.hpp"
#include "rectangle.hpp"
#include "base-types.hpp"
#include "composite-shape.hpp"

void movePoint(troshev::Shape & shape, double x, double y)
{
  shape.getInfo();
  shape.move({ x, y });
  std::cout << "Shape parameters after move: " << std::endl;
  shape.getInfo();
}

void moveXY(troshev::Shape & shape, double dx, double dy)
{
  shape.getInfo();
  shape.move(dx, dy);
  std::cout << "Shape parameters after move: " << std::endl;
  shape.getInfo();
}

void scale(troshev::Shape & shape, double number)
{
  shape.getInfo();
  shape.scale(number);
  std::cout << "Shape parameters after move: " << std::endl;
  shape.getInfo();
}

int main()
{
  troshev::Circle circle1({ -8, 10 }, 12.5);
  troshev::Circle circle2({ 5.3, -6.3 }, 7.3);
  troshev::Rectangle rectangle1({ 1.4, -2.4 }, 3.4, 4.4);
  troshev::Rectangle rectangle2({ 8.5, -9 }, 10.5, 11.5);
  troshev::CompositeShape composite;

  troshev::CompositeShape::shape_ptr part1 = std::make_shared<troshev::Circle>(circle1);
  composite.add(part1);
  composite.getInfo();

  troshev::CompositeShape::shape_ptr part2 = std::make_shared<troshev::Rectangle>(rectangle1);
  composite.add(part2);
  movePoint(composite, 10, 15);

  troshev::CompositeShape::shape_ptr part3 = std::make_shared<troshev::Rectangle>(rectangle2);
  composite.add(part3);
  moveXY(composite, 5, 5);

  troshev::CompositeShape::shape_ptr part4 = std::make_shared<troshev::Circle>(circle2);
  composite.add(part4);
  scale(composite, 7.2);

  composite.remove(1);
  movePoint(composite, -3, 3.5);

  return 0;
}
