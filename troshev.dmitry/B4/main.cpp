#include <iostream>
#include "task.hpp"

int main()
{
  try
  {
    task();
  }
  catch (const std::exception &err)
  {
    std::cerr << err.what();
    return 2;
  }
  return 0;
}
