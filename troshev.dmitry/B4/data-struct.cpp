#include "data-struct.hpp"

#include <iostream>
#include <string>

const int KEY_MIN_VALUE = -5;
const int KEY_MAX_VALUE = 5;

void troshev::printDataStruct(const troshev::DataStruct &data)
{
  std::cout << data.key1 << "," << data.key2 << "," << data.str << "\n";
}

troshev::DataStruct troshev::readDataStruct(std::istream &input)
{
  troshev::DataStruct data;
  if (input.eof())
  {
    throw std::invalid_argument("Invalid argument");
  }

  input >> data.key1;

  if ((data.key1 > KEY_MAX_VALUE) || (data.key1 < KEY_MIN_VALUE) || input.eof() || input.get() != ',' || input.eof())
  {
    throw std::invalid_argument("Invalid argument");
  }

  char tmp = input.get();

  if (input.eof() || tmp == '\r' || tmp == '\n')
  {
    throw std::invalid_argument("Invalid argument");
  }

  input.unget();
  input >> data.key2;

  if ((data.key2 > KEY_MAX_VALUE) || (data.key2 < KEY_MIN_VALUE) || input.eof() || input.get() != ',' || input.eof())
  {
    throw std::invalid_argument("Invalid argument");
  }

  while (input.peek() == ' ')
  {
    input.get();
  }

  std::getline(input, data.str);
  return data;
}
