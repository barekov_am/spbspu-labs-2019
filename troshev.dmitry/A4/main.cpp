#include <iostream>

#include "circle.hpp"
#include "rectangle.hpp"
#include "base-types.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

void movePoint(troshev::Shape & shape, double x, double y)
{
  shape.getInfo();
  shape.move({ x, y });
  std::cout << "Shape parameters after move: " << std::endl;
  shape.getInfo();
}

void moveXY(troshev::Shape & shape, double dx, double dy)
{
  shape.getInfo();
  shape.move(dx, dy);
  std::cout << "Shape parameters after move: " << std::endl;
  shape.getInfo();
}

void scale(troshev::Shape & shape, double number)
{
  shape.getInfo();
  shape.scale(number);
  std::cout << "Shape parameters after move: " << std::endl;
  shape.getInfo();
}

void rotate(troshev::Shape & shape, double angle)
{
  shape.getInfo();
  shape.rotate(angle);
  std::cout << "Shape parameters after rotate: " << std::endl;
  shape.getInfo();
}

int main()
{
  troshev::Rectangle rectangle({ 5, 25 }, 10, 10);
  std::cout << "RECTANGLE" << std::endl;
  troshev::Shape *figure = &rectangle;
  figure->getInfo();
  std::cout << "Move rectangle on dx = 3 and dy = 3" << std::endl;
  figure->move(3, 3);
  figure->getInfo();
  std::cout << "Move rectangle to point (7, 7)" << std::endl;
  figure->move({ 7, 7 });
  figure->getInfo();
  std::cout << "Area : " << figure->getArea() << std::endl;
  std::cout << "Scaling" << std::endl;
  figure->scale(2);
  figure->getInfo();
  std::cout << "Rotating" << std::endl;
  figure->rotate(46.3);
  figure->getInfo();


  troshev::Circle circle({ 20, 15 }, 6);  
  std::cout << "CIRCLE" << std::endl;
  figure = &circle;
  figure->getInfo();
  std::cout << "Move circle on dx = 5 and dy = 6" << std::endl;
  figure->move(5, 6);
  figure->getInfo();
  std::cout << "Move circle to point (10, 10)" << std::endl;
  figure->move({ 10, 10 });
  figure->getInfo();
  std::cout << "Area : " << figure->getArea() << std::endl;
  std::cout << "Scaling " << std::endl;
  figure->scale(4);
  figure->getInfo();
  std::cout << "Rotating" << std::endl;
  figure->rotate(92);
  figure->getInfo();

  troshev::Circle circle1({ -8, 10 }, 12.5);
  troshev::Circle circle2({ 5.3, -6.3 }, 7.3);
  troshev::Rectangle rectangle1({ 1.4, -2.4 }, 3.4, 4.4);
  troshev::Rectangle rectangle2({ 8.5, -9 }, 10.5, 11.5);
  troshev::CompositeShape composite;

  troshev::CompositeShape::shape_ptr part1 = std::make_shared<troshev::Circle>(circle1);
  composite.add(part1);
  composite.getInfo();

  troshev::CompositeShape::shape_ptr part2 = std::make_shared<troshev::Rectangle>(rectangle1);
  composite.add(part2);
  movePoint(composite, 10, 15);

  troshev::CompositeShape::shape_ptr part3 = std::make_shared<troshev::Rectangle>(rectangle2);
  composite.add(part3);
  moveXY(composite, 5, 5);

  troshev::CompositeShape::shape_ptr part4 = std::make_shared<troshev::Circle>(circle2);
  composite.add(part4);
  scale(composite, 7.2);

  rotate(composite, 45.72);

  composite.remove(1);
  movePoint(composite, -3, 3.5);

  std::cout << "\n  <<  Matrix and layer-by-layer partition:  >>\n";
  troshev::Matrix matrix = troshev::part(composite);
  for (unsigned int i = 0; i < matrix.getRows(); i++) {
    for (unsigned int j = 0; j < matrix.getColumns(); j++) {
      if (matrix[i][j] != nullptr) {
        std::cout << "Layer no. " << i << ":\n" << "Figure no. " << j << ":\n";
        std::cout << "Position: (" << matrix[i][j]->getFrameRect().pos.x << "; "
          << matrix[i][j]->getFrameRect().pos.y << ")\n";
      }
    }
  }

  return 0;
}

