#include <iostream>
#include <vector>
#include <string>
#include <iterator>
#include <algorithm>

void doTask1()
{
  const auto space = ' ';
  const auto tab = '\t';

  std::vector<std::string> vector;
  std::string line;

  while (std::getline(std::cin, line)) {
    size_t separator_position;
    auto separator_space = std::string::npos;
    auto separator_tab = std::string::npos;
    while (!line.empty()) {
      if (separator_space == std::string::npos) {
        separator_space = line.find(space);
      }
      if (separator_tab == std::string::npos) {
        separator_tab = line.find(tab);
      }
      if (separator_space < separator_tab) {
        separator_position = separator_space;
      } else {
        separator_position = separator_tab;
      }
      auto word = line.substr(0, separator_position);
      if (!word.empty()) {
        vector.push_back(word);
      }
      line.erase(0, separator_position + 1);
      if (separator_position == std::string::npos) {
        line.clear();
      }
      separator_space = std::string::npos;
      separator_tab = std::string::npos;
    }
  }

  if (!vector.empty()) {
    std::sort(vector.begin(), vector.end());
    vector.erase(std::unique(vector.begin(), vector.end()), vector.end());
    for (const auto& i : vector) {
      std::cout << i << '\n';
    }
  }
}
