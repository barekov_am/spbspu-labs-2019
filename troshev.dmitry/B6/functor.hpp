#ifndef FUNCTOR_B6
#define FUNCTOR_B6

#include <vector>
#include <algorithm>
#include <functional>

class Functor
{
public:
  void operator()(const int& num);

  int getMaxNumber() const;
  int getMinNumber() const;
  double getAverageNumber() const;
  int getNumOfPositive() const;
  int getNumOfNegative() const;
  long long int getEvenSum() const;
  long long int getOddSum() const;
  bool isFirstEqLast() const;

  bool isEmpty() const;

private:
  std::vector< int > numbers_;
};

#endif //FUNCTOR_B6
