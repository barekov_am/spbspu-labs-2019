#include <iostream>
#include <stdexcept>

void task();

int main()
{
  try
  {
    task();
  }

  catch (const std::exception& exception)
  {
    std::cerr << exception.what() << "\n";
    return 1;
  }

  return 0;
}
