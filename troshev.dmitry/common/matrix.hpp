#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>

#include "shape.hpp"

namespace troshev
{
  class Matrix
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    using shape_array = std::unique_ptr<shape_ptr[]>;

    Matrix();
    Matrix(const Matrix& other);
    Matrix(Matrix&& other);
    ~Matrix() = default;

    Matrix& operator =(const Matrix& other);
    Matrix& operator =(Matrix&& other);

    shape_array operator [](unsigned int index) const;
    bool operator ==(const Matrix& rhs) const;
    bool operator !=(const Matrix& rhs) const;

    void add(shape_ptr shape, unsigned int row, unsigned int column);

    unsigned int getRows() const;
    unsigned int getColumns() const;
    unsigned int getSizeLayer(unsigned int) const;

  private:
    unsigned int rows_;
    unsigned int columns_;
    shape_array list_;
  };
}

#endif

