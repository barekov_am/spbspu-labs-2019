#include <stdexcept>
#include <utility>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include "rectangle.hpp"

using shape_ptr = std::shared_ptr<troshev::Shape>;

BOOST_AUTO_TEST_SUITE(matrixTestSuite)

BOOST_AUTO_TEST_CASE(copyAndMove)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ 1, 2 }, 3);
  troshev::CompositeShape composite;
  composite.add(circle);

  troshev::Matrix matrix = troshev::part(composite);

  BOOST_CHECK_NO_THROW(troshev::Matrix matrix2(matrix));
  BOOST_CHECK_NO_THROW(troshev::Matrix matrix2(std::move(matrix)));

  troshev::Matrix matrix2 = troshev::part(composite);
  troshev::Matrix matrix3;

  BOOST_CHECK_NO_THROW(matrix3 = matrix2);
  BOOST_CHECK_NO_THROW(matrix3 = std::move(matrix2));

  troshev::Matrix matrix4 = troshev::part(composite);
  troshev::Matrix matrix5;

  matrix5 = matrix4;
  BOOST_CHECK(matrix5 == matrix4);
  matrix5 = std::move(matrix4);
  BOOST_CHECK(matrix5 == matrix3);

  troshev::Matrix matrix6(matrix3);
  BOOST_CHECK(matrix6 == matrix3);
  troshev::Matrix matrix7(std::move(matrix3));
  BOOST_CHECK(matrix7 == matrix6);
}

BOOST_AUTO_TEST_CASE(exceptionThrow)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ -3, 2.5 }, 5);
  shape_ptr rectangle = std::make_shared<troshev::Rectangle>(troshev::point_t{ 2, -4.5 }, 2, 6);
  troshev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  troshev::Matrix matrix = troshev::part(composite);

  BOOST_CHECK_THROW(matrix[3], std::out_of_range);
  BOOST_CHECK_THROW(matrix[-1], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
