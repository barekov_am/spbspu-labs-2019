#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

const double Epsilon = 0.01;

BOOST_AUTO_TEST_SUITE(rectangleTest)

BOOST_AUTO_TEST_CASE(constAfterMovingToPoint)
{
  troshev::Rectangle rectangle({ -3, 4.9 }, 8, 5);
  const double widthBefore = rectangle.getFrameRect().width;
  const double heightBefore = rectangle.getFrameRect().height;
  const double areaBefore = rectangle.getArea();

  rectangle.move({ 2, 2 });

  BOOST_CHECK_CLOSE(widthBefore, rectangle.getFrameRect().width, Epsilon);
  BOOST_CHECK_CLOSE(heightBefore, rectangle.getFrameRect().height, Epsilon);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), Epsilon);
}

BOOST_AUTO_TEST_CASE(constAfterMovingToDxDy)
{
  troshev::Rectangle rectangle({ -3, 4.9 }, 8, 5);
  const double widthBefore = rectangle.getFrameRect().width;
  const double heightBefore = rectangle.getFrameRect().height;
  const double areaBefore = rectangle.getArea();

  rectangle.move(2, 2);

  BOOST_CHECK_CLOSE(widthBefore, rectangle.getFrameRect().width, Epsilon);
  BOOST_CHECK_CLOSE(heightBefore, rectangle.getFrameRect().height, Epsilon);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), Epsilon);
}

BOOST_AUTO_TEST_CASE(constAfterScaling)
{
  troshev::Rectangle rectangle({ 5.7, -5.7 }, 5, 6);
  const double widthBefore = rectangle.getFrameRect().width;
  const double heightBefore = rectangle.getFrameRect().height;
  const double areaBefore = rectangle.getArea();
  const double number = 7.1;

  rectangle.scale(number);

  BOOST_CHECK_CLOSE(widthBefore * number, rectangle.getFrameRect().width, Epsilon);
  BOOST_CHECK_CLOSE(heightBefore * number, rectangle.getFrameRect().height, Epsilon);
  BOOST_CHECK_CLOSE(areaBefore * number * number, rectangle.getArea(), Epsilon);
}

BOOST_AUTO_TEST_CASE(invalidWidth)
{
  BOOST_CHECK_THROW(troshev::Rectangle rectangle({ 1, 2 }, -12.4, 4), std::invalid_argument);
  BOOST_CHECK_THROW(troshev::Rectangle rectangle({ 1, 2 }, 0, 4), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidHeight)
{
  BOOST_CHECK_THROW(troshev::Rectangle rectangle({ 0.5, 2 }, 18.4, -4.6), std::invalid_argument);
  BOOST_CHECK_THROW(troshev::Rectangle rectangle({ 0.5, 2 }, 18.4, 0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidNumberOfScaling)
{
  troshev::Rectangle rectangle({ 0.5, 2.3 }, 3, 6.3);
  BOOST_CHECK_THROW(rectangle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(rectangle.scale(-2.5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(areaAfterRotating)
{
  troshev::Rectangle rectangle({4, 4}, 5, 8);
  double areaBefore = rectangle.getArea();

  double angle = 56.2;
  rectangle.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), Epsilon);
  areaBefore = rectangle.getArea();

  angle = -34.3;
  rectangle.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), Epsilon);
  areaBefore = rectangle.getArea();

  angle = 1102.6;
  rectangle.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), Epsilon);
  areaBefore = rectangle.getArea();

  angle = -571.1;
  rectangle.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, rectangle.getArea(), Epsilon);
}

BOOST_AUTO_TEST_SUITE_END()

