#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>

#include "shape.hpp"

namespace troshev
{
  class CompositeShape : public Shape {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    using shape_array = std::unique_ptr<shape_ptr[]>;

    CompositeShape();
    CompositeShape(const CompositeShape & other);
    CompositeShape(CompositeShape && other);
    ~CompositeShape() = default;

    CompositeShape & operator =(const CompositeShape & other);
    CompositeShape & operator =(CompositeShape && other);
    shape_ptr operator [](const unsigned int index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void getInfo() const override;
    void move(double dx, double dy) override;
    void move(const point_t & newPoint) override;
    void scale(double number) override;
    void rotate(double angle) override;

    void add(shape_ptr shape);
    void remove(unsigned int index);
    unsigned int getSize() const;
    shape_array list() const;

  private:
    unsigned int count_;
    shape_array shapes_;

  };
}

#endif


