#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
#include "rectangle.hpp"

using shape_ptr = std::shared_ptr<troshev::Shape>;

BOOST_AUTO_TEST_SUITE(partitionTestSuite)

BOOST_AUTO_TEST_CASE(correctPartition)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ -3, 2.5 }, 5);
  shape_ptr rectangle1 = std::make_shared<troshev::Rectangle>(troshev::point_t{ 2, -4.5 }, 2, 6);
  shape_ptr rectangle2 = std::make_shared<troshev::Rectangle>(troshev::point_t{ 0.5, -5.5 }, 11, 2);
  shape_ptr partOfComposite1 = std::make_shared<troshev::Rectangle>(troshev::point_t{ 2.5, -3 }, 5, 1);
  shape_ptr partOfComposite2 = std::make_shared<troshev::Rectangle>(troshev::point_t{ -4, 0 }, 2, 17);
  troshev::CompositeShape composite;
  composite.add(partOfComposite1);
  composite.add(partOfComposite2);
  shape_ptr testComposite = std::make_shared<troshev::CompositeShape>(composite);

  troshev::CompositeShape testComposite1;
  testComposite1.add(circle);
  testComposite1.add(rectangle1);
  testComposite1.add(rectangle2);
  testComposite1.add(testComposite);

  troshev::Matrix testMatrix = troshev::part(testComposite1);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 3);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 2);

  BOOST_CHECK(testMatrix[0][0] == circle);
  BOOST_CHECK(testMatrix[0][1] == rectangle2);
  BOOST_CHECK(testMatrix[1][0] == rectangle1);
  BOOST_CHECK(testMatrix[2][0] == testComposite);
}

BOOST_AUTO_TEST_SUITE_END()

