#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

const double Epsilon = 0.01;

BOOST_AUTO_TEST_SUITE(circleTest)

BOOST_AUTO_TEST_CASE(constAfterMovingToPoint)
{
  troshev::Circle circle({ -1, 1 }, 3.5);
  const double areaBefore = circle.getArea();

  circle.move({ 2, 2 });

  BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), Epsilon);
}

BOOST_AUTO_TEST_CASE(constAfterMovingDxDy)
{
  troshev::Circle circle({ -1, 1 }, 5.5);
  const double areaBefore = circle.getArea();

  circle.move(6.5, 7.1);

  BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), Epsilon);
}

BOOST_AUTO_TEST_CASE(constAfterScaling)
{
  troshev::Circle circle({ 0, 0 }, 6);
  const double areaBefore = circle.getArea();
  const double number = 2.3;

  circle.scale(number);

  BOOST_CHECK_CLOSE(areaBefore * number * number, circle.getArea(), Epsilon);
}

BOOST_AUTO_TEST_CASE(invalidRadius)
{
  BOOST_CHECK_THROW(troshev::Circle circle({ 0.5, 2 }, -6.3), std::invalid_argument);
  BOOST_CHECK_THROW(troshev::Circle circle({ 0.5, 2 }, 0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalidNumberOfScaling)
{
  troshev::Circle circle({ 0.3, 1.3 }, 3);
  BOOST_CHECK_THROW(circle.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(circle.scale(-4.5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(areaAfterRotating)
{
  troshev::Circle circle({ 4, 4 }, 5);
  double areaBefore = circle.getArea();

  double angle = 37.8;
  circle.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), Epsilon);
  areaBefore = circle.getArea();

  angle = -56.88;
  circle.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), Epsilon);
  areaBefore = circle.getArea();

  angle = 384.2;
  circle.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), Epsilon);
  areaBefore = circle.getArea();

  angle = -887.3;
  circle.rotate(angle);
  BOOST_CHECK_CLOSE(areaBefore, circle.getArea(), Epsilon);
}

BOOST_AUTO_TEST_SUITE_END()
