#include "matrix.hpp"

#include <stdexcept>
#include <utility>

troshev::Matrix::Matrix() :
  rows_(0),
  columns_(0)
{
}

troshev::Matrix::Matrix(const Matrix& other) :
  rows_(other.rows_),
  columns_(other.columns_),
  list_(std::make_unique<shape_ptr[]>(other.rows_ * other.columns_))
{
  for (unsigned int i = 0; i < (rows_ * columns_); i++) {
    list_[i] = other.list_[i];
  }
}

troshev::Matrix::Matrix(Matrix&& other) :
  rows_(other.rows_),
  columns_(other.columns_),
  list_(std::move(other.list_))
{
}

troshev::Matrix& troshev::Matrix::operator =(const Matrix& other)
{
  if (this != &other) {
    rows_ = other.rows_;
    columns_ = other.columns_;
    shape_array tmpList(std::make_unique<shape_ptr[]>(other.rows_ * other.columns_));
      for (unsigned int i = 0; i < (rows_ * columns_); i++) {
        tmpList[i] = other.list_[i];
      }
  list_.swap(tmpList);
  }

  return *this;
}

troshev::Matrix& troshev::Matrix::operator =(Matrix&& other)
{
  if (this != &other) {
    rows_ = other.rows_;
    columns_ = other.columns_;
    list_ = std::move(other.list_);
  }

  return *this;
}

troshev::Matrix::shape_array troshev::Matrix::operator [](unsigned int index) const
{
  if (index >= rows_) {
    throw std::out_of_range("Index is out of range");
  }

  shape_array tmpList(std::make_unique<shape_ptr[]>(columns_));
  for (unsigned int i = 0; i < columns_; i++) {
    tmpList[i] = list_[index * columns_ + i];
  }

  return tmpList;
}

bool troshev::Matrix::operator ==(const Matrix& rhs) const
{
  if ((rows_ != rhs.rows_) || (columns_ != rhs.columns_)) {
    return false;
  }

  for (unsigned int i = 0; i < (rows_ * columns_); i++) {
    if (list_[i] != rhs.list_[i]) {
      return false;
    }
  }

  return true;
}

bool troshev::Matrix::operator !=(const Matrix& rhs) const
{
  return !(*this == rhs);
}

void troshev::Matrix::add(shape_ptr shape, unsigned int row, unsigned int column)
{
  unsigned int tmpRows = (row == rows_) ? (rows_ + 1) : (rows_);
  unsigned int tmpColumns = (column == columns_) ? (columns_ + 1) : (columns_);

  shape_array tmpList(std::make_unique<shape_ptr[]>(tmpRows * tmpColumns));

  for (unsigned int i = 0; i < rows_; i++) {
    for (unsigned int j = 0; j < columns_; j++) {
        tmpList[i * tmpColumns + j] = list_[i * columns_ + j];
      }
    }

  tmpList[row * tmpColumns + column] = shape;
  list_.swap(tmpList);
  rows_ = tmpRows;
  columns_ = tmpColumns;
}

unsigned int troshev::Matrix::getRows() const
{
  return rows_;
}

unsigned int troshev::Matrix::getColumns() const
{
  return columns_;
}

unsigned int troshev::Matrix::getSizeLayer(unsigned int row) const
{
  if (row >= rows_) {
    return 0;
  }
  for (unsigned int i = 0; i < columns_; i++) {
    if (list_[row * columns_ + i] == nullptr) {
      return i;
    }
  }
  return columns_;
}
