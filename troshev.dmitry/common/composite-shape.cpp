#include "composite-shape.hpp"

#include <stdexcept>
#include <iostream>
#include <cmath>

const double FullCircle = 360.0;

troshev::CompositeShape::CompositeShape() :
  count_(0)
{
}

troshev::CompositeShape::CompositeShape(const CompositeShape & other) :
  count_(other.count_),
  shapes_(std::make_unique<shape_ptr[]>(other.count_))
{
  for (unsigned int i = 0; i < count_; i++) {
    shapes_[i] = other.shapes_[i];
  }
}

troshev::CompositeShape::CompositeShape(CompositeShape && other) :
  count_(other.count_),
  shapes_(std::move(other.shapes_))
{
  other.count_ = 0;
}

troshev::CompositeShape & troshev::CompositeShape::operator =(const CompositeShape & other)
{
  if (this != &other) {
    count_ = other.count_;
    shape_array tmpArray(std::make_unique<shape_ptr[]>(other.count_));
    for (unsigned int i = 0; i < count_; i++) {
      tmpArray[i] = other.shapes_[i];
    }
  shapes_.swap(tmpArray);
  }

  return *this;
}

troshev::CompositeShape & troshev::CompositeShape::operator =(CompositeShape && other)
{
  if (this != &other) {
    count_ = other.count_;
    shapes_ = std::move(other.shapes_);
    other.count_ = 0;
  }

  return *this;
}


troshev::CompositeShape::shape_ptr troshev::CompositeShape::operator [](const unsigned int index) const
{
  if (index >= count_) {
    throw std::out_of_range("Index is out of range");
  }

  return shapes_[index];
}

double troshev::CompositeShape::getArea() const
{
  double area = 0;
  for (unsigned int i = 0; i < count_; i++) {
    area += shapes_[i]->getArea();
  }

  return area;
}

void troshev::CompositeShape::getInfo() const
{
  std::cout << "Composite Shape: " << std::endl;
  std::cout << "Point x : " << getFrameRect().pos.x << std::endl;
  std::cout << "Point y : " << getFrameRect().pos.y << std::endl;
  std::cout << "Width: " << getFrameRect().width << std::endl;
  std::cout << "Height: " << getFrameRect().height << std::endl;
  std::cout << "Area: " << getArea() << std::endl;
}

troshev::rectangle_t troshev::CompositeShape::getFrameRect() const
{
  if (count_ == 0) {
    return { { 0, 0 }, 0, 0 };
  }

  rectangle_t frameRect = shapes_[0]->getFrameRect();

  double minX = frameRect.pos.x - frameRect.width / 2;
  double maxX = frameRect.pos.x + frameRect.width / 2;
  double minY = frameRect.pos.y - frameRect.height / 2;
  double maxY = frameRect.pos.y + frameRect.height / 2;

  for (unsigned int i = 1; i < count_; i++) {
    frameRect = shapes_[i]->getFrameRect();
    minX = std::min(frameRect.pos.x - frameRect.width / 2, minX);
    maxX = std::max(frameRect.pos.x + frameRect.width / 2, maxX);
    minY = std::min(frameRect.pos.y - frameRect.height / 2, minY);
    maxY = std::max(frameRect.pos.y - frameRect.height / 2, maxY);
  }

  return rectangle_t{ { (maxX + minX) / 2, (maxY + minY) / 2 }, maxX - minX, maxY
      - minY };
}

unsigned int troshev::CompositeShape::getSize() const
{
  return count_;
}

void troshev::CompositeShape::move(double dx, double dy)
{
  if (count_ == 0) {
    throw std::logic_error("Composite shape must be not empty");
  }

  for (unsigned int i = 0; i < count_; i++) {
    shapes_[i]->move(dx, dy);
  }
}

void troshev::CompositeShape::move(const point_t & newPoint)
{
  if (count_ == 0) {
    throw std::logic_error("Composite shape must be not empty");
  }

  rectangle_t frameRect = getFrameRect();

  double dx = newPoint.x - frameRect.pos.x;
  double dy = newPoint.y - frameRect.pos.y;
  move(dx, dy);
}

void troshev::CompositeShape::scale(double number)
{
  if (number <= 0) {
    throw std::invalid_argument("Number of scale must be positive");
  }

  if (count_ == 0) {
    throw std::logic_error("Composite shape must be not empty");
  }

  rectangle_t frameRect = getFrameRect();

  for (unsigned int i = 0; i < count_; i++) {
    double dx = (shapes_[i]->getFrameRect().pos.x - frameRect.pos.x) * number;
    double dy = (shapes_[i]->getFrameRect().pos.y - frameRect.pos.y) * number;
    shapes_[i]->move({ frameRect.pos.x + dx, frameRect.pos.y + dy });
    shapes_[i]->scale(number);
  }
}

void troshev::CompositeShape::add(shape_ptr shape)
{
  if (shape == nullptr) {
    throw std::invalid_argument("Pointer must be not null!");
  }

  for (unsigned int i = 0; i < count_; i++) {
    if (shapes_[i] == shape) {
    return;
    }
  }

  shape_array tmpArray(std::make_unique<shape_ptr[]>(count_ + 1));
  for (unsigned int i = 0; i < count_; i++) {
    tmpArray[i] = shapes_[i];
  }
  tmpArray[count_] = shape;
  count_++;
  shapes_.swap(tmpArray);
}


void troshev::CompositeShape::remove(unsigned int index)
{
  if (index >= count_) {
    throw std::invalid_argument("Index must be less than count of shapes");
  }

  count_--;
  for (unsigned int i = index; i < count_; i++) {
    shapes_[i] = shapes_[i + 1];
  }

  shapes_[count_] = nullptr;
}

troshev::CompositeShape::shape_array troshev::CompositeShape::list() const
{
  shape_array tmpArray(std::make_unique<shape_ptr[]>(count_));
  for (unsigned int i = 0; i < count_; i++) {
    tmpArray[i] = shapes_[i];
  }

  return tmpArray;
}

void troshev::CompositeShape::rotate(double angle)
{
  const point_t centre = getFrameRect().pos;
  const double sine = sin(angle * M_PI / 180);
  const double cosine = cos(angle * M_PI / 180);

  for (unsigned int i = 0; i < count_; i++) {
    const double oldX = shapes_[i]->getFrameRect().pos.x - centre.x;
    const double oldY = shapes_[i]->getFrameRect().pos.y - centre.y;

    const double newX = oldX * fabs(cosine) - oldY * fabs(sine);
    const double newY = oldX * fabs(sine) + oldY * fabs(cosine);

    shapes_[i]->move({ centre.x + newX, centre.y + newY });
    shapes_[i]->rotate(angle);
  }
}

