#include <stdexcept>
#include <utility>

#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

using shape_ptr = std::shared_ptr<troshev::Shape>;

const double Epsilon = 0.01;

BOOST_AUTO_TEST_SUITE(compositeShapeTests)

BOOST_AUTO_TEST_CASE(constAfterMovingToPoint)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ 1.2, -3.4 }, 5.6);
  shape_ptr rectangle = std::make_shared<troshev::Rectangle>(troshev::point_t{ -7.8, 9.1 }, 2.3, 4.5);
  troshev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const troshev::rectangle_t frameRectBefore = composite.getFrameRect();

  const double areaBefore = composite.getArea();
  const double newPointX = 6.7;
  const double newPointY = -8.9;

  composite.move({ newPointX, newPointY });
  const double areaAfter = composite.getArea();
  const troshev::rectangle_t frameRectAfter = composite.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, Epsilon);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, Epsilon);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, Epsilon);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, newPointX, Epsilon);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, newPointY, Epsilon);
}

BOOST_AUTO_TEST_CASE(constAfterMovingDxDy)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ -1.2, 3.4 }, 5);
  shape_ptr rectangle = std::make_shared<troshev::Rectangle>(troshev::point_t{ -5.4, 3.2 }, 1, 2.3);
  troshev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const double areaBefore = composite.getArea();
  const troshev::rectangle_t frameRectBefore = composite.getFrameRect();
  const double dx = 5.4;
  const double dy = -4.3;

  composite.move(dx, dy);
  const double areaAfter = composite.getArea();
  const troshev::rectangle_t frameRectAfter = composite.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, Epsilon);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, Epsilon);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, Epsilon);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x + dx, frameRectAfter.pos.x, Epsilon);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y + dy, frameRectAfter.pos.y, Epsilon);
}

BOOST_AUTO_TEST_CASE(constAfterScalingWithNumberMoreThanOne)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ 2.3, 4.5 }, 4);
  shape_ptr rectangle = std::make_shared<troshev::Rectangle>(troshev::point_t{ 2.4, -6.8 }, 1.3, 5.7);
  troshev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const double areaBefore = composite.getArea();
  const double number = 2.1;
  const troshev::rectangle_t frameRectBefore = composite.getFrameRect();

  composite.scale(number);
  const double areaAfter = composite.getArea();
  const troshev::rectangle_t frameRectAfter = composite.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore * number * number, areaAfter, Epsilon);
  BOOST_CHECK_CLOSE(frameRectBefore.width * number, frameRectAfter.width, Epsilon);
  BOOST_CHECK_CLOSE(frameRectBefore.height * number, frameRectAfter.height, Epsilon);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, Epsilon);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, Epsilon);
}

BOOST_AUTO_TEST_CASE(constAfterScalingWithNumberLessThanOneButMoreThanNull)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ 4.3, 4.5 }, 4);
  shape_ptr rectangle = std::make_shared<troshev::Rectangle>(troshev::point_t{ 3.2, -1.4 }, 5.6, 5.4);
  troshev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);

  const double areaBefore = composite.getArea();
  const double number = 0.8;
  const troshev::rectangle_t frameRectBefore = composite.getFrameRect();

  composite.scale(number);
  const double areaAfter = composite.getArea();
  const troshev::rectangle_t frameRectAfter = composite.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore * number * number, areaAfter, Epsilon);
  BOOST_CHECK_CLOSE(frameRectBefore.width * number, frameRectAfter.width, Epsilon);
  BOOST_CHECK_CLOSE(frameRectBefore.height * number, frameRectAfter.height, Epsilon);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, Epsilon);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, Epsilon);
}

BOOST_AUTO_TEST_CASE(changeAfterAdding)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ 3.2, 7.2 }, 4);
  shape_ptr rectangle = std::make_shared<troshev::Rectangle>(troshev::point_t{ -4.6, 7.5 }, 5, 2.2);
  troshev::CompositeShape composite;
  double areaOfComposite = composite.getArea();
  int count = 0;
  BOOST_CHECK_CLOSE(areaOfComposite, 0, Epsilon);

  composite.add(circle);
  count++;
  areaOfComposite += circle->getArea();
  BOOST_CHECK_EQUAL(count, composite.getSize());
  BOOST_CHECK_CLOSE(areaOfComposite, composite.getArea(), Epsilon);
  composite.add(circle);
  BOOST_CHECK_EQUAL(count, composite.getSize());

  composite.add(rectangle);
  count++;
  areaOfComposite += rectangle->getArea();
  BOOST_CHECK_EQUAL(count, composite.getSize());
  BOOST_CHECK_CLOSE(areaOfComposite, composite.getArea(), Epsilon);
}

BOOST_AUTO_TEST_CASE(changeAfterDeleting)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ 1.3, -6.1 }, 3.6);
  shape_ptr rectangle = std::make_shared<troshev::Rectangle>(troshev::point_t{ 1.8, -3 }, 2.2, 4.2);
  troshev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  double areaOfComposite = composite.getArea();
  int count = 2;

  composite.remove(0);
  count--;
  areaOfComposite -= circle->getArea();
  BOOST_CHECK_EQUAL(count, composite.getSize());
  BOOST_CHECK_CLOSE(areaOfComposite, composite.getArea(), Epsilon);

  composite.remove(0);
  count--;
  areaOfComposite -= rectangle->getArea();
  BOOST_CHECK_EQUAL(count, composite.getSize());
  BOOST_CHECK_CLOSE(areaOfComposite, composite.getArea(), Epsilon);
}

BOOST_AUTO_TEST_CASE(useCopyConstructorForCopyInEmpty)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ 5.1, 2.5 }, 1);
  shape_ptr rectangle = std::make_shared<troshev::Rectangle>(troshev::point_t{ -6.5, 8.5 }, 3, 3.2);
  troshev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const troshev::rectangle_t frameRectOfComposite = composite.getFrameRect();

  troshev::CompositeShape copyComposite = composite;
  const double areaOfCopyComposite = copyComposite.getArea();
  const double sizeOfCopyComposite = copyComposite.getSize();
  const troshev::rectangle_t frameRectOfCopyComposite = copyComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfCopyComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfCopyComposite, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfCopyComposite.width, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfCopyComposite.height, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfCopyComposite.pos.x, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfCopyComposite.pos.y, Epsilon);
}

BOOST_AUTO_TEST_CASE(useCopyOperatorForCopyInEmpty)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ 6.5, -7.3 }, 1);
  shape_ptr rectangle = std::make_shared<troshev::Rectangle>(troshev::point_t{ -3, 4.3 }, 5.3, 4);
  troshev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const troshev::rectangle_t frameRectOfComposite = composite.getFrameRect();

  troshev::CompositeShape copyComposite;
  copyComposite = composite;
  const double areaOfCopyComposite = copyComposite.getArea();
  const double sizeOfCopyComposite = copyComposite.getSize();
  const troshev::rectangle_t frameRectOfCopyComposite = copyComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfCopyComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfCopyComposite, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfCopyComposite.width, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfCopyComposite.height, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfCopyComposite.pos.x, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfCopyComposite.pos.y, Epsilon);
}

BOOST_AUTO_TEST_CASE(useMoveConstructorForMoveInEmpty)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ 1.4, 5.3 }, 7);
  shape_ptr rectangle = std::make_shared<troshev::Rectangle>(troshev::point_t{ -2.4, 5.7 }, 3, 6.3);
  troshev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const troshev::rectangle_t frameRectOfComposite = composite.getFrameRect();

  troshev::CompositeShape moveComposite = std::move(composite);
  const double areaOfMoveComposite = moveComposite.getArea();
  const double sizeOfMoveComposite = moveComposite.getSize();
  const troshev::rectangle_t frameRectOfMoveComposite = moveComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfMoveComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfMoveComposite, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfMoveComposite.width, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfMoveComposite.height, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfMoveComposite.pos.x, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfMoveComposite.pos.y, Epsilon);
}

BOOST_AUTO_TEST_CASE(useMoveOperatorForMoveInEmpty)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ 5.5, 6.7 }, 2);
  shape_ptr rectangle = std::make_shared<troshev::Rectangle>(troshev::point_t{ -3.5, 6.2 }, 1, 3.2);
  troshev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const troshev::rectangle_t frameRectOfComposite = composite.getFrameRect();

  troshev::CompositeShape moveComposite;
  moveComposite = std::move(composite);
  const double areaOfMoveComposite = moveComposite.getArea();
  const double sizeOfMoveComposite = moveComposite.getSize();
  const troshev::rectangle_t frameRectOfMoveComposite = moveComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfMoveComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfMoveComposite, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfMoveComposite.width, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfMoveComposite.height, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfMoveComposite.pos.x, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfMoveComposite.pos.y, Epsilon);
}

BOOST_AUTO_TEST_CASE(useCopyOperatorForCopyInNotEmpty)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ -7.1, 2.3 }, 5.2);
  shape_ptr rectangle = std::make_shared<troshev::Rectangle>(troshev::point_t{ 1.7, -2.9 }, 9.2, 6.2);
  troshev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const troshev::rectangle_t frameRectOfComposite = composite.getFrameRect();

  shape_ptr circle1 = std::make_shared<troshev::Circle>(troshev::point_t{ 1.4, -6.1 }, 1.3);
  shape_ptr rectangle1 = std::make_shared<troshev::Rectangle>(troshev::point_t{ -2.2, 5.3 }, 5.2, 2);
  troshev::CompositeShape copyComposite;
  copyComposite.add(circle1);
  copyComposite.add(rectangle1);
  copyComposite = composite;
  const double areaOfCopyComposite = copyComposite.getArea();
  const double sizeOfCopyComposite = copyComposite.getSize();
  const troshev::rectangle_t frameRectOfCopyComposite = copyComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfCopyComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfCopyComposite, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfCopyComposite.width, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfCopyComposite.height, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfCopyComposite.pos.x, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfCopyComposite.pos.y, Epsilon);
}

BOOST_AUTO_TEST_CASE(useMoveOperatorForMoveInNotEmpty)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ 4.4, 3 }, 1.2);
  shape_ptr rectangle = std::make_shared<troshev::Rectangle>(troshev::point_t{ 3.2, -9 }, 2.3, 8.1);
  troshev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  const double areaOfComposite = composite.getArea();
  const double sizeOfComposite = composite.getSize();
  const troshev::rectangle_t frameRectOfComposite = composite.getFrameRect();

  shape_ptr circle1 = std::make_shared<troshev::Circle>(troshev::point_t{ 3.2, 6.1 }, 1);
  shape_ptr rectangle1 = std::make_shared<troshev::Rectangle>(troshev::point_t{ -1.5, 2.6 }, 7, 3.2);
  troshev::CompositeShape moveComposite;
  moveComposite.add(circle1);
  moveComposite.add(rectangle1);
  moveComposite = std::move(composite);
  const double areaOfMoveComposite = moveComposite.getArea();
  const double sizeOfMoveComposite = moveComposite.getSize();
  const troshev::rectangle_t frameRectOfMoveComposite = moveComposite.getFrameRect();

  BOOST_CHECK_EQUAL(sizeOfComposite, sizeOfMoveComposite);
  BOOST_CHECK_CLOSE(areaOfComposite, areaOfMoveComposite, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.width, frameRectOfMoveComposite.width, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.height, frameRectOfMoveComposite.height, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.x, frameRectOfMoveComposite.pos.x, Epsilon);
  BOOST_CHECK_CLOSE(frameRectOfComposite.pos.y, frameRectOfMoveComposite.pos.y, Epsilon);
}

BOOST_AUTO_TEST_CASE(invalidNumberOfScaling)
{
  shape_ptr circle = std::make_shared<troshev::Circle>(troshev::point_t{ 1.3, -5.2 }, 3);
  shape_ptr rectangle = std::make_shared<troshev::Rectangle>(troshev::point_t{ -6.2, 3.2 }, 3, 7.3);
  troshev::CompositeShape composite;
  composite.add(circle);
  composite.add(rectangle);
  BOOST_CHECK_THROW(composite.scale(0), std::invalid_argument);
  BOOST_CHECK_THROW(composite.scale(-5.6), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
