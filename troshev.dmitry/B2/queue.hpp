#ifndef QUEUE_HPP
#define QUEUE_HPP
#include <list>
#include <iostream>

template <typename T>
class QueueWithPriority
{
public:

  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  QueueWithPriority();

  void putElementToQueue(const T& element, ElementPriority priority);
  T getElementFromQueue();

  void accelerate();
  bool empty() const;

private:
  std::list<T> low;
  std::list<T> normal;
  std::list<T> high;
};

template <typename T>
QueueWithPriority<T>::QueueWithPriority() :
        low(),
        normal(),
        high()
{}

template <typename T>
void QueueWithPriority<T>::putElementToQueue(const T& element, ElementPriority priority)
{
  switch(priority)
  {
    case ElementPriority::LOW:
    {
      low.push_back(element);
      break;
    }
    case ElementPriority::NORMAL:
    {
      normal.push_back(element);
      break;
    }
    case ElementPriority::HIGH:
    {
      high.push_back(element);
      break;
    }
  }
}

template <typename T>
T QueueWithPriority<T>::getElementFromQueue()
{
  if (!high.empty())
  {
    T tmp = high.front();
    high.pop_front();
    return tmp;
  }
  else if (!normal.empty())
  {
    T tmp = normal.front();
    normal.pop_front();
    return tmp;
  }
  else
  {
    T tmp = low.front();
    low.pop_front();
    return tmp;
  }
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  high.splice(high.end(), low);
}

template <typename T>
bool QueueWithPriority<T>::empty() const
{
  return (low.empty() && normal.empty() && high.empty());
}

#endif //B2_QUEUE_HPP
