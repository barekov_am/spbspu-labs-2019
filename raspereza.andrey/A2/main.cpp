#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void showFrameRect(const raspereza::Shape *shape)
{
  raspereza::rectangle_t rect_ = shape->getFrameRect();
  std::cout << "Width = " << rect_.width << "Height = " << rect_.height 
      << "Vertex = (" << rect_.pos.x << ";" << rect_.pos.y << ")" << '\n';
}

void showArea(const raspereza::Shape *shape)
{
  std::cout << "Area = " << shape->getArea() << '\n';
}

int main()
{
  raspereza::Circle circ1({4.0, 2.2}, 5.0);
  raspereza::Shape *shapePointer = &circ1;

  std::cout << "Circle\n";
  shapePointer->printInfo();
  std::cout << "S = " << shapePointer->getArea() << "\n";

  raspereza::rectangle_t temp_rect = shapePointer->getFrameRect();
  std::cout << "Center = " << temp_rect.pos.x << ";" << temp_rect.pos.y
    << " Width = " << temp_rect.width << " Height = " << temp_rect.height << "\n";

  shapePointer->move({2.3, 3.5});
  shapePointer->printInfo();
  shapePointer->scale(2);
  shapePointer->printInfo();

  raspereza::Rectangle rect1({3.4, 3.4}, 6.2, 6.2);
  shapePointer = &rect1;

  std::cout << "Rectangle\n";
  shapePointer->printInfo();
  std::cout << "S = " << shapePointer->getArea() << "\n";
  shapePointer->move(2.1, 4.0);
  shapePointer->printInfo();
  shapePointer->scale(3);
  shapePointer->printInfo();

  raspereza::Triangle trian1({2.2, 3.3}, {1.8, 3.7}, {5.0, 5.0});
  shapePointer = &trian1;

  std::cout << "Triangle\n";
  shapePointer->printInfo();
  std::cout << "S = " << shapePointer->getArea() << "\n";

  temp_rect = shapePointer->getFrameRect();
  std::cout << "Center = " << temp_rect.pos.x << ";" << temp_rect.pos.y
    << " Width = " << temp_rect.width << " Height = " << temp_rect.height << "\n";

  shapePointer->move(2.1, 4.2);
  shapePointer->printInfo();
  shapePointer->move({6.0, 6.0});
  shapePointer->printInfo();
  shapePointer->scale(4);
  shapePointer->printInfo();

  raspereza::point_t shape[] = {{3.0, 2.0}, {9.0, 2.0}, {9.0, 5.0}, {3.0, 5.0}};
  size_t qVertex = sizeof(shape) / sizeof(shape[0]);

  raspereza::Polygon poly1(qVertex, shape);
  shapePointer = &poly1;

  std::cout << "Polygon\n";
  shapePointer->printInfo();
  std::cout << "S = " << shapePointer->getArea() << "\n";

  temp_rect = shapePointer->getFrameRect();
  std::cout << "Center = " << temp_rect.pos.x << ";" << temp_rect.pos.y
    << " Width = " << temp_rect.width << " Height = " << temp_rect.height << "\n";

  shapePointer->move(4.0, 2.0);
  shapePointer->printInfo();
  shapePointer->move({3.0, 3.0});
  shapePointer->printInfo();
  shapePointer->scale(2);
  shapePointer->printInfo();

  std::cout << "Copy constructor\n";
  raspereza::Polygon polygonCopyConstructor(poly1);
  polygonCopyConstructor.printInfo();

  std::cout << "Copy assignment\n";
  raspereza::Polygon polygonCopyAssignment(poly1);
  polygonCopyAssignment.printInfo();

  std::cout << "Move constructor\n";
  raspereza::Polygon polygonMoveConstructor(std::move(polygonCopyAssignment));
  polygonMoveConstructor.printInfo();

  std::cout << "Move assignment\n";
  raspereza::Polygon polygonMoveAssignment(std::move(poly1));
  polygonMoveAssignment.printInfo();

  return 0;
}
