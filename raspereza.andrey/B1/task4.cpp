#include <random>
#include <vector>
#include "sort.hpp"

void fillRandom(double* array, size_t size)
{
  std::mt19937 rng(time(0));
  std::uniform_real_distribution<double> uid(-1.0, 1.0);
  for (size_t i = 0; i < size; ++i)
  {
    array[i] = uid(rng);
  }
}

void task4(const char* direction, size_t size)
{
  auto dir = getDirection<double>(direction);
  if (size <= 0)
  {
    throw std::invalid_argument("Size not less than 0");
  }

  std::vector<double> vector(size);
  fillRandom(&vector[0], size);
  print(vector);
  sort<bracketsAccess>(vector, dir);
  print(vector);
}
