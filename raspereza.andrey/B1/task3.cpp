#include <vector>
#include "sort.hpp"

void task3()
{
  std::vector<int> vector;
  int number = 0;

  while (std::cin >> number, !std::cin.eof())
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Error reading input file \n");
    }

    if (number == 0)
    {
      break;
    }

    vector.push_back(number);
  }

  if (number != 0)
  {
    throw std::invalid_argument("Incorrect symbol of end of file");
  }


  if (vector.empty())
  {
    return;
  }

  switch (vector.back())
  {
    case 1:
    for (auto i = vector.begin(); i != vector.end();)
    {
      i = ((*i % 2) == 0) ? vector.erase(i) : ++i;
    }
    break;

    case 2:
    for (auto i = vector.begin(); i != vector.end();)
    {
      i = ((*i % 3) == 0) ? (vector.insert(++i, 3, 1) + 3) : ++i;
    }
    break;

    default:
    break;
  }

  print(vector);
}
