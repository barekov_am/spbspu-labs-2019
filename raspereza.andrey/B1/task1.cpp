#include <cstring>
#include <forward_list>
#include <vector>
#include "sort.hpp"

void task1(const char* direction)
{
  std::vector<int> vectorBr;
  auto dir = getDirection<int>(direction);
  int number = 0;
  while (std::cin >> number, !std::cin.eof())
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Error reading input file \n");
    }

    vectorBr.push_back(number);
  }

  if (vectorBr.empty())
  {
    return;
  }

  std::vector<int> vectorAt(vectorBr);
  std::forward_list<int> listIter(vectorBr.begin(), vectorBr.end());

  sort<bracketsAccess>(vectorBr, dir);
  sort<atAccess>(vectorAt, dir);
  sort<iteratorAccess>(listIter, dir);

  print(vectorBr);
  print(vectorAt);
  print(listIter);
}
