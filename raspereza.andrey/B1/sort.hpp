#ifndef SORT_HPP
#define SORT_HPP

#include <iostream>
#include <functional>
#include <cstring>
#include "accesses.hpp"

template <typename T>
std::function<bool(T, T)> getDirection(const char * order)
{
  if (std::strcmp(order, "ascending") == 0)
  {
    return [](T lhs, T rhs) { return lhs < rhs; };
  }

  if (std::strcmp(order, "descending") == 0)
  {
    return [](T lhs, T rhs) { return lhs > rhs; };
  }
  throw std::invalid_argument("Illegal order ");
}

template <template <class T> class Access, typename T>
void sort(T &collection, std::function<bool(typename T::value_type, typename T::value_type)> compare)
{
  const auto begin = Access<T>::begin(collection);
  const auto end = Access<T>::end(collection);

  for (auto i = begin; i != end; i++)
  {
    for (auto j = Access<T>::next(i); j != end; j++)
    {
      typename T::reference elemA = Access<T>::getElement(collection, j);
      typename T::reference elemB = Access<T>::getElement(collection, i);
      if (compare(elemA, elemB))
      {
        std::swap(elemA, elemB);
      }
    }
  }
}

template<class T>
void print(const T& arr)
{
  for (const auto& elem : arr)
  {
    std::cout << elem << " ";
  }
  std::cout << '\n';
}

#endif
