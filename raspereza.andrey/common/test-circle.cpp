#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(TestCircle)

const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(CoordinateMoveTest)
{
  raspereza::Circle testCircle({5.1, 4.2}, 4.0);
  raspereza::rectangle_t getFrameBeforeMove = testCircle.getFrameRect();
  double getAreaBeforeMove = testCircle.getArea();

  testCircle.move(4.0, 2.0);
  raspereza::rectangle_t getFrameAfterMove = testCircle.getFrameRect();
  double getAreaAfterMove = testCircle.getArea();

  BOOST_CHECK_CLOSE(getFrameBeforeMove.width, getFrameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(getFrameBeforeMove.height, getFrameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(getAreaBeforeMove, getAreaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(CenterMoveTest)
{
  raspereza::Circle testCircle({5.1, 4.2}, 4.0);
  raspereza::rectangle_t getFrameBeforeMove = testCircle.getFrameRect();
  double getAreaBeforeMove = testCircle.getArea();

  testCircle.move({1.0, 1.0});
  raspereza::rectangle_t getFrameAfterMove = testCircle.getFrameRect();
  double getAreaAfterMove = testCircle.getArea();

  BOOST_CHECK_CLOSE(getFrameBeforeMove.width, getFrameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(getFrameBeforeMove.height, getFrameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(getAreaBeforeMove, getAreaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(TestScale)
{
  raspereza::Circle testCircle({5.1, 4.2}, 4.0);
  double getAreaBeforeScale = testCircle.getArea();
  double k = 2;
  testCircle.scale(k);
  double getAreaAfterScale = testCircle.getArea();

  BOOST_CHECK_CLOSE(getAreaBeforeScale * k * k, getAreaAfterScale, EPSILON);
}

BOOST_AUTO_TEST_CASE(IncorrectRadiusTest)
{
  BOOST_CHECK_THROW(raspereza::Circle({5.1, 4.2}, -4.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(IncorrectScaleTest)
{
  raspereza::Circle testCircle({5.1, 4.2}, 4.0);

  BOOST_CHECK_THROW(testCircle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestRotate)
{
  raspereza::Circle testCircle({5.1, 4.2}, 4.0);
  const double getAreaBefore = testCircle.getArea();
  const raspereza::rectangle_t getFrameBeforeRotate = testCircle.getFrameRect();

  double degree = 90;
  testCircle.rotate(degree);
  double getAreaAfter = testCircle.getArea();
  raspereza::rectangle_t getFrameAfterRotate = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(getFrameAfterRotate.height, getFrameBeforeRotate.width, EPSILON);
  BOOST_CHECK_CLOSE(getFrameAfterRotate.width, getFrameBeforeRotate.height, EPSILON);
  BOOST_CHECK_CLOSE(getFrameAfterRotate.pos.x, getFrameBeforeRotate.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(getFrameAfterRotate.pos.y, getFrameBeforeRotate.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(getAreaAfter, getAreaBefore, EPSILON);

  degree = -150;
  testCircle.rotate(degree);
  getAreaAfter = testCircle.getArea();
  getFrameAfterRotate = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(getFrameAfterRotate.height, getFrameBeforeRotate.width, EPSILON);
  BOOST_CHECK_CLOSE(getFrameAfterRotate.width, getFrameBeforeRotate.height, EPSILON);
  BOOST_CHECK_CLOSE(getFrameAfterRotate.pos.x, getFrameBeforeRotate.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(getFrameAfterRotate.pos.y, getFrameBeforeRotate.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(getAreaAfter, getAreaBefore, EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()
