#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

namespace raspereza
{
  class Circle: public Shape
  {
  public:
    Circle(const point_t &, double radius);

    double getArea() const override;
    void printInfo() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(double dx, double dy) override;
    void scale(double ratio) override;
    void rotate(double) override;

  private:
    point_t center_;
    double radius_;
  };
}
#endif
