#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>
#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(testTriangle)

const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(CoordinateMoveTest)
{
  raspereza::Triangle testTriangle({6.0, 6.0}, {1.0, 3.0}, {4.0, 5.0});
  raspereza::rectangle_t getFrameBeforeMove = testTriangle.getFrameRect();
  double getAreaBeforeMove = testTriangle.getArea();

  testTriangle.move(2.0, 1.0);
  raspereza::rectangle_t getFrameAfterMove = testTriangle.getFrameRect();
  double getAreaAfterMove = testTriangle.getArea();

  BOOST_CHECK_CLOSE(getFrameBeforeMove.width, getFrameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(getFrameBeforeMove.height, getFrameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(getAreaBeforeMove, getAreaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(CenterMoveTest)
{
  raspereza::Triangle testTriangle({6.0, 6.0}, {1.0, 3.0}, {4.0, 5.0});
  raspereza::rectangle_t getFrameBeforeMove = testTriangle.getFrameRect();
  double getAreaBeforeMove = testTriangle.getArea();

  testTriangle.move({1.0, 2.0});
  raspereza::rectangle_t getFrameAfterMove = testTriangle.getFrameRect();
  double getAreaAfterMove = testTriangle.getArea();

  BOOST_CHECK_CLOSE(getFrameBeforeMove.width, getFrameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(getFrameBeforeMove.height, getFrameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(getAreaBeforeMove, getAreaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(TestScale)
{
  raspereza::Triangle testTriangle({6.0, 6.0}, {1.0, 3.0}, {4.0, 5.0});
  double getAreaBeforeScale = testTriangle.getArea();
  const double k = 2;
  testTriangle.scale(k);
  double getAreaAfterScale = testTriangle.getArea();

  BOOST_CHECK_CLOSE(getAreaBeforeScale * k * k, getAreaAfterScale, EPSILON);
}

BOOST_AUTO_TEST_CASE(IncorrectYInitialisation)
{
  BOOST_CHECK_THROW(raspereza::Triangle({2.0, 3.0}, {7.0, 3.0}, {4.0, 3.0}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(IncorrectXInitialisation)
{
  BOOST_CHECK_THROW(raspereza::Triangle({8.0, 2.0}, {8.0, 3.0}, {8.0, 4.0}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(IncorrectScaleTest)
{
  raspereza::Triangle testTriangle({6.0, 6.0}, {1.0, 3.0}, {4.0, 5.0});

  BOOST_CHECK_THROW(testTriangle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
