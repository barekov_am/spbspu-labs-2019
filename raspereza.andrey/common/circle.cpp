#include "circle.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

raspereza::Circle::Circle(const point_t &center, double radius):
  center_(center),
  radius_(radius)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("Radius <= 0, please swap");
  }
}

double raspereza::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

void raspereza::Circle::printInfo() const
{
  std::cout << "Center = " << center_.x << ";" << center_.y << " Radius = " << radius_ << "\n";
}

raspereza::rectangle_t raspereza::Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, center_};
}

void raspereza::Circle::move(const point_t &newCenter)
{
  center_ = newCenter;
}

void raspereza::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void raspereza::Circle::scale(double ratio)
{
  if (ratio <= 0)
  {
    throw std::invalid_argument("Scale ratio <= 0, please swap");
  }
  else
  {
    radius_ *= ratio;
  }
}

void raspereza::Circle::rotate(double)
{
}
