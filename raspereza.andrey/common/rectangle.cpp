#include "rectangle.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

raspereza::Rectangle::Rectangle(const point_t &center, double width, double height):
  center_(center),
  width_(width),
  height_(height),
  degree_(0)
{
  if (width_ <= 0)
  {
    throw std::invalid_argument("Width <= 0, please swap");
  }
  if (height_ <= 0)
  {
    throw std::invalid_argument("Height <= 0, please swap");
  }
}

double raspereza::Rectangle::getArea() const
{
  return width_ * height_;
}

void raspereza::Rectangle::printInfo() const
{
  std::cout << "Center = " << center_.x << ";" << center_.y
    << " Width = " << width_ << " Height = " << height_ << " Degree = " << degree_ << "\n";
}

raspereza::rectangle_t raspereza::Rectangle::getFrameRect() const
{
  const double cos_ = cos(degree_ * M_PI / 180);
  const double sin_ = sin(degree_ * M_PI / 180);
  const double width = height_ * std::fabs(sin_) + width_ * std::fabs(cos_);
  const double height = height_ * std::fabs(cos_) + width_ * std::fabs(sin_);

  return {width, height, center_};
}

void raspereza::Rectangle::move(const point_t &newCenter)
{
  center_ = newCenter;
}

void raspereza::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void raspereza::Rectangle::scale(double ratio)
{
  if (ratio <= 0)
  {
    throw std::invalid_argument("Scale ratio <=0, please swap");
  }
  else
  {
    width_ *= ratio;
    height_ *= ratio;
  }
}

void raspereza::Rectangle::rotate(double degree) 
{
  degree_ += degree;
  while (degree_ >= 360.0)
  {
    degree_-= 360.0;
  }
}
