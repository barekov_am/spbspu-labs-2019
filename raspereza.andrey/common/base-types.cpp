#include "base-types.hpp"
#include <cmath>

raspereza::point_t raspereza::rotatePoint(const raspereza::point_t& center, const raspereza::point_t& point, double degree)
{
  double degree_ = degree * M_PI / 180;
  double x = center.x + (point.x - center.x) * cos(degree_) - (point.y - center.y) * sin(degree_);
  double y = center.y + (point.y - center.y) * cos(degree_) + (point.x - center.x) * sin(degree_);

  return {x, y};
}
