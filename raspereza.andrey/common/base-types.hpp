#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace raspereza
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    double height;
    double width;
    point_t pos;
  };

  raspereza::point_t rotatePoint(const raspereza::point_t& centre, const raspereza::point_t& point, double degree);
}
#endif
