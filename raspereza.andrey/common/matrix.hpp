#ifndef MATRIX_HPP
#define MATRIX_HPP
#include <memory>
#include <cstddef>
#include "shape.hpp"
#include "composite-shape.hpp"

namespace raspereza
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix& newMatrix);
    Matrix(Matrix&& newMatrix);

    ~Matrix() = default;

    Matrix& operator =(const Matrix& newMatrix);
    Matrix& operator =(Matrix&& newMatrix);

    CompositeShape::array_ operator [](const size_t index) const;
    bool operator ==(const Matrix& newMatrix) const;
    bool operator !=(const Matrix& newMatrix) const;

    std::size_t getRows() const;
    std::size_t getColumns() const;
    void add(CompositeShape::ptr shape, size_t row, size_t column);

  private:
    std::size_t rows_;
    std::size_t columns_;
    CompositeShape::array_ matrix_;
  };
}

#endif
