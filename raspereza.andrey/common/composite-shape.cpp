#include "composite-shape.hpp"
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <cmath>

raspereza::CompositeShape::CompositeShape():
  numShapes_(0),
  shapes_(nullptr)
{ 
}

raspereza::CompositeShape::CompositeShape(const CompositeShape& newShape):
  numShapes_(newShape.numShapes_),
  shapes_(std::make_unique<ptr[]>(newShape.numShapes_))
{
  for (std::size_t i = 0; i < numShapes_; i++)
  {
    shapes_[i] = newShape.shapes_[i];
  }
}

raspereza::CompositeShape::CompositeShape(CompositeShape&& newShape):
  numShapes_(newShape.numShapes_),
  shapes_(std::move(newShape.shapes_))
{
  newShape.numShapes_ = 0;
  newShape.shapes_.reset();
}

raspereza::CompositeShape::CompositeShape(const ptr &shape):
  numShapes_(1),
  shapes_(std::make_unique<ptr[]>(1))
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape: not exist");
  }
  shapes_[0] = shape;
}

raspereza::CompositeShape& raspereza::CompositeShape::operator =(const CompositeShape& newShape)
{
  if (this != &newShape)
  {
    numShapes_ = newShape.numShapes_;
    shapes_ = std::make_unique<CompositeShape::ptr[]>(newShape.numShapes_);
    for (size_t i = 0; i < newShape.numShapes_; ++i)
    {
      shapes_[i] = newShape.shapes_[i];
    }
  }
  return *this;
}

raspereza::CompositeShape& raspereza::CompositeShape::operator =(CompositeShape&& newShape)
{
  if (this != &newShape)
  {
    numShapes_ = newShape.numShapes_;
    shapes_ = std::move(newShape.shapes_);
    newShape.numShapes_ = 0;
  }
  return *this;
}

raspereza::CompositeShape::ptr raspereza::CompositeShape::operator [](std::size_t index) const
{
  if (index >= numShapes_)
  {
    throw std::out_of_range("Index: Out of range");
  }
  return shapes_[index];
}

bool raspereza::CompositeShape::operator ==(const CompositeShape& newShape)
{
  if (numShapes_ != newShape.numShapes_)
  {
    return false;
  }

  for (size_t i = 0; i < numShapes_; i++)
  {
    if (shapes_[i] != newShape.shapes_[i])
    {
      return false;
    }
  }
  return true;
}

bool raspereza::CompositeShape::operator !=(const CompositeShape& newShape)
{
  return !(*this == newShape);
}


std::size_t raspereza::CompositeShape::getQuantity() const
{
  return(numShapes_);
}

void raspereza::CompositeShape::printInfo() const
{
  if (numShapes_ == 0)
  {
    std::cout << "Composite Shape: Empty";
  }
  else
  {
    rectangle_t frameRect = getFrameRect();
    std::cout << "Composite Shape position = " << frameRect.pos.x << ";" << frameRect.pos.y << "\n";
    std::cout << " Shapes: " << "\n";
    for (std::size_t i = 0; i < numShapes_; i++)
    {
      shapes_[i]->printInfo();
    }
  }
}

double raspereza::CompositeShape::getArea() const
{
  double area = 0;
  for (std::size_t i = 0; i < numShapes_; i++)
  {
    area += shapes_[i]->getArea();
  }

  return area;
}

raspereza::rectangle_t raspereza::CompositeShape::getFrameRect() const
{
  if (numShapes_ == 0)
  {
    throw std::logic_error("Composite Shape: Empty");
  }

  rectangle_t tempRect = shapes_[0]->getFrameRect();

  double minX = tempRect.pos.x - tempRect.width / 2;
  double maxX = tempRect.pos.x + tempRect.width / 2;
  double minY = tempRect.pos.y - tempRect.height / 2;
  double maxY = tempRect.pos.y + tempRect.height / 2;

  for (size_t i = 1; i < numShapes_; i++)
  {
    tempRect = shapes_[i]->getFrameRect();

    minX = std::min(minX, tempRect.pos.x - tempRect.width / 2);
    maxX = std::max(maxX, tempRect.pos.x + tempRect.width / 2);
    minY = std::min(minY, tempRect.pos.y - tempRect.height / 2);
    maxY = std::max(maxY, tempRect.pos.y + tempRect.height / 2);
  }

  double width = maxX - minX;
  double height = maxY - minY;
  point_t centerRect = {minX + width / 2, minY + height / 2};

  return rectangle_t {width, height, centerRect};
}

void raspereza::CompositeShape::move(double dx, double dy)
{
  if (numShapes_ != 0)
  {
    for (std::size_t i = 0; i < numShapes_; i++)
    {
      shapes_[i]->move(dx, dy);
    }
  }
}

void raspereza::CompositeShape::move(const point_t& newCentre)
{
  rectangle_t frameRect = getFrameRect();
  move(newCentre.x - frameRect.pos.x, newCentre.y - frameRect.pos.y);
}

void raspereza::CompositeShape::scale(double ratio)
{
  if (ratio <= 0)
  {
    throw std::invalid_argument("Scale ratio <=0, please swap");
  }

  if (numShapes_ != 0)
  {
    rectangle_t frameRect = getFrameRect();

    for (std::size_t i = 0; i < numShapes_; i++)
    {
      rectangle_t currentFrameRect = shapes_[i]->getFrameRect();
      shapes_[i]->move((currentFrameRect.pos.x - frameRect.pos.x) * (ratio - 1), (currentFrameRect.pos.y - frameRect.pos.y) * (ratio - 1));
      shapes_[i]->scale(ratio);
    }
  }
}

raspereza::CompositeShape::array_ raspereza::CompositeShape::getShapes() const
{
  array_ tempArray(std::make_unique<ptr[]>(numShapes_));
  for (std::size_t i = 0; i < numShapes_; i++)
  {
    tempArray[i] = shapes_[i];
  }

  return tempArray;
}

void raspereza::CompositeShape::add(const ptr& shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Adding shape shouldn't be nullptr");
  }

  CompositeShape::array_ newShape(std::make_unique<CompositeShape::ptr[]>(numShapes_ + 1));
  for (std::size_t i = 0; i < numShapes_; i++)
  {
    newShape[i] = shapes_[i];
  }

  newShape[numShapes_] = shape;
  numShapes_++;
  shapes_.swap(newShape);
}

void raspereza::CompositeShape::remove(size_t index)
{
  if (index >= numShapes_)
  {
    throw std::logic_error("Remote Shape number > numbers of shapes in Composite Shape");
  }

  for (std::size_t i = index; i < (numShapes_ - 1); i++)
  {
    shapes_[i] = shapes_[i + 1];
  }

  shapes_[--numShapes_] = nullptr;
}

void raspereza::CompositeShape::rotate(double degree)
{
  const point_t compCenter = getFrameRect().pos;

  for (std::size_t i = 0; i < numShapes_; i++)
  {
    const point_t shapeCenter = shapes_[i]->getFrameRect().pos;
    shapes_[i]->move(raspereza::rotatePoint(compCenter, shapeCenter, degree));
    shapes_[i]->rotate(degree);
  }
}
