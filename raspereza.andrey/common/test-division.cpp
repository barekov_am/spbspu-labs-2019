#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "division.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(testDivision)

BOOST_AUTO_TEST_CASE(overlapingOfShapes)
{
  raspereza::Rectangle rectangle1({0.0, 0.0}, 4.0, 4.0);
  raspereza::Circle circle1({-5.0, 1.0}, 1.0);
  raspereza::Rectangle rectangle2({-2.0, 1.0}, 4.0, 4.0);
  raspereza::Circle circle2({-2.0, 0.0}, 1.0);

  BOOST_CHECK(raspereza::overlap(rectangle1, circle2));
  BOOST_CHECK(raspereza::overlap(rectangle1, rectangle2));
  BOOST_CHECK(raspereza::overlap(rectangle2, circle1));
  BOOST_CHECK(raspereza::overlap(rectangle2, circle2));
  BOOST_CHECK(!raspereza::overlap(rectangle1, circle1));
  BOOST_CHECK(!raspereza::overlap(circle1, circle2));
}

BOOST_AUTO_TEST_CASE(emptyCompositedivideing)
{
  raspereza::Matrix testMatrix = raspereza::divide(raspereza::CompositeShape());

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 0);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(correctCompositeSeparation) 

{
  raspereza::Rectangle rectangle1({-1.0, -1.0}, 3.1, 2.4);
  raspereza::Circle circle1({3.0, 2.0}, 1.0);
  raspereza::Rectangle rectangle2({1.0, 2.0}, 3.1, 2.4);
  raspereza::Rectangle rectangle3({1.0, -4.0}, 3.1, 2.4);

  raspereza::CompositeShape testShapes(std::make_shared<raspereza::Rectangle>(rectangle1));
  testShapes.add(std::make_shared<raspereza::Circle>(circle1));
  testShapes.add(std::make_shared<raspereza::Rectangle>(rectangle2));
  testShapes.add(std::make_shared<raspereza::Rectangle>(rectangle3));

  raspereza::Matrix testMatrix = raspereza::divide(testShapes);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 2);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 2);
  BOOST_CHECK_EQUAL(testMatrix[0][0], testShapes[0]);
  BOOST_CHECK_EQUAL(testMatrix[0][1], testShapes[1]);
  BOOST_CHECK_EQUAL(testMatrix[1][0], testShapes[2]);
  BOOST_CHECK_EQUAL(testMatrix[1][1], testShapes[3]);
}

BOOST_AUTO_TEST_SUITE_END()
