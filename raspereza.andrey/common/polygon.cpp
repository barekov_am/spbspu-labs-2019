#include "polygon.hpp"
#include <cstddef>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <cmath>

raspereza::Polygon::Polygon(const Polygon &other):
  qVertex_t(other.qVertex_t),
  centroid_t(other.centroid_t),
  vertex_t(new raspereza::point_t[qVertex_t])
{
  for (std::size_t i = 0; i < qVertex_t; i++)
  {
    vertex_t[i] = other.vertex_t[i];
  }
}

raspereza::Polygon::Polygon(Polygon &&other):
  qVertex_t(other.qVertex_t),
  centroid_t(other.centroid_t),
  vertex_t(other.vertex_t)
{
  if (this != &other)
  {
    other.qVertex_t = 0;
    other.centroid_t = {0.0, 0.0};
    other.vertex_t = nullptr;
  }
}

raspereza::Polygon::Polygon(std::size_t qVertex, raspereza::point_t *vertex):
  qVertex_t(qVertex)
{
  if (qVertex_t <= 2)
  {
    throw std::invalid_argument("Vertex <= 2, please add more");
  }

  if (vertex == nullptr)
  {
    throw std::invalid_argument("Pointer = null");
  }
  else
  {
    vertex_t = new point_t[qVertex_t];
    for (std::size_t i = 0; i < qVertex_t; i++)
    {
      vertex_t[i] = vertex[i];
    }

    centroid_t = calcCentroid();
  }
}

raspereza::Polygon::~Polygon()
{
  delete[] vertex_t;
}

raspereza::Polygon & raspereza::Polygon::operator =(const Polygon &other)
{
  if (this == &other)
  {
    return *this;
  }

  qVertex_t = other.qVertex_t;
  centroid_t = other.centroid_t;

  delete[] vertex_t;

  vertex_t = new point_t[qVertex_t];
  for (std::size_t i = 0; i < qVertex_t; i++)
  {
    vertex_t[i] = other.vertex_t[i];
  }
  return *this;
}

raspereza::Polygon & raspereza::Polygon::operator =(Polygon &&other)
{
  if (this == &other)
  {
    return *this;
  }
  qVertex_t = other.qVertex_t;
  centroid_t = other.centroid_t;

  other.qVertex_t = 0;
  other.centroid_t = {0.0, 0.0};

  delete[] vertex_t;

  vertex_t = other.vertex_t;
  other.vertex_t = nullptr;
  return *this;
}

raspereza::point_t raspereza::Polygon::calcCentroid() const
{
  raspereza::point_t temp_p = {0.0, 0.0};
  for (std::size_t i = 0; i < qVertex_t; i++)
  {
    temp_p.x += vertex_t[i].x;
    temp_p.y += vertex_t[i].y;
  }
  return {temp_p.x / qVertex_t, temp_p.y / qVertex_t};
}

std::size_t raspereza::Polygon::getQuantity() const
{
  return qVertex_t;
}

bool raspereza::Polygon::checkConvex() const
{
  point_t side_A = {0.0, 0.0};
  point_t side_B = {0.0, 0.0};

  for (std::size_t i = 0; i <= qVertex_t - 2; i++)
  {
    if (i == qVertex_t - 2)
    {
      side_A.x = vertex_t[i + 1].x - vertex_t[i].x;
      side_A.y = vertex_t[i + 1].y - vertex_t[i].y;
      side_B.x = vertex_t[0].x - vertex_t[i + 1].x;
      side_B.y = vertex_t[0].y - vertex_t[i + 1].y;
    }
    else
    {
      side_A.x = vertex_t[i + 1].x - vertex_t[i].x;
      side_A.y = vertex_t[i + 1].y - vertex_t[i].y;
      side_B.x = vertex_t[i + 2].x - vertex_t[i + 1].x;
      side_B.y = vertex_t[i + 2].y - vertex_t[i + 1].y;
    }
    if (((side_A.x * side_B.y) - (side_A.y * side_B.x)) < 0.0)
    {
      return true;
    }
  }
  return false;
}

void raspereza::Polygon::printInfo() const
{
  for (std::size_t i = 0; i < qVertex_t; i++)
  {
    std::cout << "Vertex number:" << i + 1 << "Vertex (" << vertex_t[i].x << ";" << vertex_t[i].y << ")" << '\n';
  }
}

double raspereza::Polygon::getArea() const
{
  double sum = 0.0;

  for (std::size_t i = 0; i < qVertex_t - 1; i++)
  {
    sum += vertex_t[i].x * vertex_t[i + 1].y;
    sum -= vertex_t[i + 1].x * vertex_t[i].y;
  }

  sum += vertex_t[qVertex_t - 1].x * vertex_t[0].y;
  sum -= vertex_t[0].x * vertex_t[qVertex_t - 1].y;

  return (0.5 * std::fabs(sum));
}

raspereza::rectangle_t raspereza::Polygon::getFrameRect() const
{
  double maxX = vertex_t[0].x;
  double minX = vertex_t[0].x;
  double maxY = vertex_t[0].y;
  double minY = vertex_t[0].y;

  for (std::size_t i = 0; i < qVertex_t; i++)
  {
    maxX = std::max(maxX, vertex_t[i].x);
    minX = std::min(minX, vertex_t[i].x);
    maxY = std::max(maxY, vertex_t[i].y);
    minY = std::min(minY, vertex_t[i].y);
  }

  const double width = (maxY - minY);
  const double height = (maxX - minX);
  const point_t centerRect = {minX + width / 2, minY + height / 2};

  return {width, height, centerRect};
}

void raspereza::Polygon::move(double dx, double dy)
{
  for (std::size_t i = 0; i < qVertex_t; i++)
  {
    vertex_t[i].x += dx;
    vertex_t[i].y += dy;
  }
  centroid_t.x += dx;
  centroid_t.y += dy;
}

void raspereza::Polygon::move(const point_t &new_p)
{
  move(new_p.x - centroid_t.x, new_p.y - centroid_t.y);
}

void raspereza::Polygon::scale(const double ratio)
{
  if (ratio <= 0)
  {
    throw std::invalid_argument("Scale ratio <=0, please swap");
  }
  else
  {
    for (std::size_t i = 0; i < qVertex_t; i++)
    {
      vertex_t[i].x = centroid_t.x + (vertex_t[i].x - centroid_t.x) * ratio;
      vertex_t[i].y = centroid_t.y + (vertex_t[i].y - centroid_t.y) * ratio;
    }
  }
}

void raspereza::Polygon::rotate(double degree)
{
  const point_t center = calcCentroid();

  for (std::size_t i = 0; i < qVertex_t; i++)
  {
    raspereza::rotatePoint(center, vertex_t[i], degree);
  }
}
