#ifndef DIVISION_HPP
#define DIVISION_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace raspereza
{
  bool overlap(const Shape &firstShape, const Shape &secondShape);
  Matrix divide(const CompositeShape&);
}
#endif 
