#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>
#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testRectangle)

const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(CoordinateMoveTest)
{
  raspereza::Rectangle testRectangle({2.0, 4.0}, 5.0, 5.0);
  raspereza::rectangle_t getFrameBeforeMove = testRectangle.getFrameRect();
  double getAreaBeforeMove = testRectangle.getArea();

  testRectangle.move(4.0, 4.0);
  raspereza::rectangle_t getFrameAfterMove = testRectangle.getFrameRect();
  double getAreaAfterMove = testRectangle.getArea();

  BOOST_CHECK_CLOSE(getFrameBeforeMove.width, getFrameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(getFrameBeforeMove.height, getFrameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(getAreaBeforeMove, getAreaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(CenterMoveTest)
{
  raspereza::Rectangle testRectangle({2.0, 4.0}, 5.0, 5.0);
  raspereza::rectangle_t getFrameBeforeMove = testRectangle.getFrameRect();
  double getAreaBeforeMove = testRectangle.getArea();

  testRectangle.move({1.0, 1.0});
  raspereza::rectangle_t getFrameAfterMove = testRectangle.getFrameRect();
  double getAreaAfterMove = testRectangle.getArea();

  BOOST_CHECK_CLOSE(getFrameBeforeMove.width, getFrameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(getFrameBeforeMove.height, getFrameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(getAreaBeforeMove, getAreaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(TestScale)
{
  raspereza::Rectangle testRectangle({2.0, 4.0}, 5.0, 5.0);
  double getAreaBeforeScale = testRectangle.getArea();
  double k = 2;
  testRectangle.scale(k);
  double getAreaAfterScale = testRectangle.getArea();

  BOOST_CHECK_CLOSE(getAreaBeforeScale * k * k, getAreaAfterScale, EPSILON);
}

BOOST_AUTO_TEST_CASE(IncorrectWidthTest)
{
  BOOST_CHECK_THROW(raspereza::Rectangle({2.0, 4.0}, -5.0, 5.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(IncorrectHeightTest)
{
  BOOST_CHECK_THROW(raspereza::Rectangle({2.0, 4.0}, 5.0, -5.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(IncorrectScaleTest)
{
  raspereza::Rectangle testRectangle({2.0, 4.0}, 5.0, 5.0);

  BOOST_CHECK_THROW(testRectangle.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
