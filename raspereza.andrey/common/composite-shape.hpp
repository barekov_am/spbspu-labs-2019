#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP
#include <memory>
#include <cstddef>
#include "shape.hpp"

namespace raspereza
{
  class CompositeShape: public Shape
  {
  public:
    using ptr = std::shared_ptr<Shape>;
    using array_ = std::unique_ptr<ptr[]>;

    CompositeShape();
    CompositeShape(const CompositeShape &newShape);
    CompositeShape(CompositeShape &&newShape);
    CompositeShape(const ptr &shape);
    ~CompositeShape() override = default;

    CompositeShape& operator =(const CompositeShape& newShape);
    CompositeShape& operator =(CompositeShape&& newShape);

    ptr operator [](std::size_t index) const;
    bool operator ==(const CompositeShape& newShape);
    bool operator !=(const CompositeShape& newShape);

    std::size_t getQuantity() const;
    void printInfo() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t&) override;
    void move(double dx, double dy) override;
    void scale(double ratio) override;
    array_ getShapes() const;
    void add(const ptr &shape);
    void remove(std::size_t index);
    void rotate(double degree) override;

  private:
    std::size_t numShapes_;
    array_ shapes_;
  };
}
#endif
