#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

namespace raspereza
{
  class Triangle: public Shape
  {
  public:
    Triangle(const point_t &, const point_t &, const point_t &);

    double getArea() const override;
    void printInfo() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(double dx, double dy) override;
    void scale(double ratio) override;
    void rotate(double degree) override;

  private:
    point_t vertexA_;
    point_t vertexB_;
    point_t vertexC_;
    point_t center_;
  };
}
#endif
