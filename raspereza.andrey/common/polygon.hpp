#ifndef POLYGON_HPP
#define POLYGON_HPP
#include <cstddef>
#include "shape.hpp"

namespace raspereza
{
  class Polygon: public Shape
  {
  public:
    Polygon(const Polygon &other);
    Polygon(Polygon &&other);
    Polygon(std::size_t qVertex, point_t *vertex);

    ~Polygon() override;

    Polygon &operator =(const Polygon &other);
    Polygon &operator =(Polygon &&other);

    point_t calcCentroid() const;
    std::size_t getQuantity() const;
    bool checkConvex() const;
    void printInfo() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &) override;
    void move(double dx, double dy) override;
    void scale(double ratio) override;
    void rotate(double degree) override;

  private:
    std::size_t qVertex_t;
    point_t centroid_t;
    point_t *vertex_t;
  };
}
#endif
