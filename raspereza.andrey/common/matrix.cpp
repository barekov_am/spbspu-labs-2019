#include "matrix.hpp"
#include <stdexcept>

raspereza::Matrix::Matrix():
  rows_(0),
  columns_(0),
  matrix_()
{ 
}

raspereza::Matrix::Matrix(const Matrix &newMatrix) :
  rows_(newMatrix.rows_),
  columns_(newMatrix.columns_),
  matrix_(std::make_unique<CompositeShape::ptr[]>(newMatrix.rows_ * newMatrix.columns_))
{
  for (std::size_t i = 0; i < (rows_ * columns_); i++)
  {
    matrix_[i] = newMatrix.matrix_[i];
  }
}

raspereza::Matrix::Matrix(Matrix &&newMatrix):
  rows_(newMatrix.rows_),
  columns_(newMatrix.columns_),
  matrix_(std::move(newMatrix.matrix_))
{
}

raspereza::Matrix &raspereza::Matrix::operator =(const Matrix &newMatrix)
{
  if (this != &newMatrix)
  {
    rows_ = newMatrix.rows_;
    columns_ = newMatrix.columns_;
    CompositeShape::array_ tempMatrix(std::make_unique<CompositeShape::ptr[]>(newMatrix.rows_ * newMatrix.columns_));
    for (size_t i = 0; i < (rows_ * columns_); i++)
    {
      tempMatrix[i] = newMatrix.matrix_[i];
    }
    matrix_.swap(tempMatrix);
  }

  return *this;
}

raspereza::Matrix &raspereza::Matrix::operator =(Matrix &&newMatrix)
{
  if (this != &newMatrix)
  {
    rows_ = newMatrix.rows_;
    columns_ = newMatrix.columns_;
    matrix_ = std::move(newMatrix.matrix_);
  }

  return *this;
}

std::unique_ptr<std::shared_ptr<raspereza::Shape>[]> raspereza::Matrix::operator [](size_t index) const
{
  if (index >= rows_)
  {
    throw std::out_of_range("Index: out of range");
  }

  CompositeShape::array_ tempMatrix(std::make_unique<std::shared_ptr<Shape>[]>(columns_));
  for (size_t i = 0; i < columns_; i++)
  {
    tempMatrix[i] = matrix_[index * columns_ + i];
  }

  return tempMatrix;
}

bool raspereza::Matrix::operator ==(const Matrix& newMatrix) const
{
  if ((rows_ != newMatrix.rows_) || (columns_ != newMatrix.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    if (matrix_[i] != newMatrix.matrix_[i])
    {
      return false;
    }
  }

  return true;
}

bool raspereza::Matrix::operator !=(const Matrix& newMatrix) const
{
  return !(*this == newMatrix);
}

std::size_t raspereza::Matrix::getRows() const
{
  return rows_;
}

std::size_t raspereza::Matrix::getColumns() const
{
  return columns_;
}

void raspereza::Matrix::add(std::shared_ptr<Shape> shape, size_t row, size_t column)
{
  if (!shape)
  {
    throw std::invalid_argument("Shouldn't add nullptr");
  }

  size_t newRows = (row == rows_) ? (rows_ + 1) : (rows_);
  size_t newColumns = (column == columns_) ? (columns_ + 1) : (columns_);

  CompositeShape::array_ tempMatrix(std::make_unique<CompositeShape::ptr[]>(newRows * newColumns));
  for (size_t i = 0; i < newRows; i++)
  {
    for (size_t j = 0; j < newColumns; j++)
    {
      if ((i != rows_) && (j != columns_))
      {
        tempMatrix[i * newColumns + j] = matrix_[i * columns_ + j];
      }
      else
      {
        tempMatrix[i * newColumns + j] = nullptr;
      }
    }
  }

  tempMatrix[row * newColumns + column] = shape;
  matrix_.swap(tempMatrix);
  rows_ = newRows;
  columns_ = newColumns;
}
