#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>
#include "polygon.hpp"

BOOST_AUTO_TEST_SUITE(testPolygon)

const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(CopyConstructor)
{
  raspereza::point_t shape[] = {{3.0, 2.0}, {7.0, 4.0}, {4.0, 6.0}, {1.0, 8.0}};
  std::size_t qVertex = sizeof(shape) / sizeof(shape[0]);
  raspereza::Polygon polygon1_(qVertex, shape);
  raspereza::Polygon polygon2_(polygon1_);

  BOOST_CHECK_CLOSE(polygon1_.getFrameRect().pos.x, polygon2_.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(polygon1_.getFrameRect().pos.y, polygon2_.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(polygon1_.getFrameRect().width, polygon2_.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(polygon1_.getFrameRect().height, polygon2_.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(polygon1_.getArea(), polygon2_.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(polygon1_.getQuantity(), polygon2_.getQuantity());
}

BOOST_AUTO_TEST_CASE(MoveConstructor)
{
  raspereza::point_t shape[] = {{3.0, 2.0}, {7.0, 4.0}, {4.0, 6.0}, {1.0, 8.0}};
  std::size_t qVertex = sizeof(shape) / sizeof(shape[0]);
  raspereza::Polygon polygon1_(qVertex, shape);

  const raspereza::rectangle_t testFrame = polygon1_.getFrameRect();
  const double testArea = polygon1_.getArea();
  const size_t testQuantity = polygon1_.getQuantity();

  raspereza::Polygon polygon2_(std::move(polygon1_));

  BOOST_CHECK_CLOSE(testFrame.pos.x, polygon2_.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.pos.y, polygon2_.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.width, polygon2_.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.height, polygon2_.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(testArea, polygon2_.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(testQuantity, polygon2_.getQuantity());
}

BOOST_AUTO_TEST_CASE(CopyOperator)
{
  raspereza::point_t shape[] = {{3.0, 2.0}, {7.0, 4.0}, {4.0, 6.0}, {1.0, 8.0}};
  std::size_t qVertex = sizeof(shape) / sizeof(shape[0]);
  raspereza::Polygon polygon1_(qVertex, shape);
  raspereza::Polygon polygon2_(qVertex, shape);
  polygon2_ = polygon1_;

  BOOST_CHECK_CLOSE(polygon1_.getFrameRect().pos.x, polygon2_.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(polygon1_.getFrameRect().pos.y, polygon2_.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(polygon1_.getFrameRect().width, polygon2_.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(polygon1_.getFrameRect().height, polygon2_.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(polygon1_.getArea(), polygon2_.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(polygon1_.getQuantity(), polygon2_.getQuantity());
}

BOOST_AUTO_TEST_CASE(MoveOperator)
{
  raspereza::point_t shape[] = {{3.0, 2.0}, {7.0, 4.0}, {4.0, 6.0}, {1.0, 8.0}};
  std::size_t qVertex = sizeof(shape) / sizeof(shape[0]);
  raspereza::Polygon polygon1_(qVertex, shape);
  raspereza::Polygon polygon2_(qVertex, shape);

  const raspereza::rectangle_t testFrame = polygon1_.getFrameRect();
  const double testArea = polygon1_.getArea();
  const size_t testQuantity = polygon1_.getQuantity();

  polygon2_ = std::move(polygon1_);

  BOOST_CHECK_CLOSE(testFrame.pos.x, polygon2_.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.pos.y, polygon2_.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.width, polygon2_.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.height, polygon2_.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(testArea, polygon2_.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(testQuantity, polygon2_.getQuantity());
}


BOOST_AUTO_TEST_CASE(CoordinateMoveTest)
{
  raspereza::point_t shape[] = {{3.0, 2.0}, {7.0, 4.0}, {4.0, 6.0}, {1.0, 8.0}};
  std::size_t qVertex = sizeof(shape) / sizeof(shape[0]);
  raspereza::Polygon testPolygon(qVertex, shape);
  const double getAreaBeforeMove = testPolygon.getArea();
  const raspereza::rectangle_t getFrameBeforeMove = testPolygon.getFrameRect();

  testPolygon.move(3.0, 1.0);
  const double getAreaAfterMove = testPolygon.getArea();
  const raspereza::rectangle_t getFrameAfterMove = testPolygon.getFrameRect();

  BOOST_CHECK_CLOSE(getFrameBeforeMove.width, getFrameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(getFrameBeforeMove.height, getFrameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(getAreaBeforeMove, getAreaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(CenterMoveTest)
{
  raspereza::point_t shape[] = {{3.0, 2.0}, {7.0, 4.0}, {4.0, 6.0}, {1.0, 8.0}};
  std::size_t qVertex = sizeof(shape) / sizeof(shape[0]);
  raspereza::Polygon testPolygon(qVertex, shape);
  const double getAreaBeforeMove = testPolygon.getArea();
  const raspereza::rectangle_t getFrameBeforeMove = testPolygon.getFrameRect();

  testPolygon.move({2.0, 1.0});
  const double getAreaAfterMove = testPolygon.getArea();
  const raspereza::rectangle_t getFrameAfterMove = testPolygon.getFrameRect();

  BOOST_CHECK_CLOSE(getFrameBeforeMove.width, getFrameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(getFrameBeforeMove.height, getFrameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(getAreaBeforeMove, getAreaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(TestScale)
{
  raspereza::point_t shape[] = {{3.0, 2.0}, {7.0, 4.0}, {4.0, 6.0}, {1.0, 8.0}};
  std::size_t qVertex = sizeof(shape) / sizeof(shape[0]);
  raspereza::Polygon testPolygon(qVertex, shape);
  double getAreaBeforeScale = testPolygon.getArea();
  double k = 2;
  testPolygon.scale(k);
  const double getAreaAfterScale = testPolygon.getArea();

  BOOST_CHECK_CLOSE(getAreaBeforeScale * k * k, getAreaAfterScale, EPSILON);
}

BOOST_AUTO_TEST_CASE(IncorrectParametrsTest)
{
  raspereza::point_t shape[] = {{2.0, 5.0}, {3.0, 8.0}};
  std::size_t qVertex = sizeof(shape) / sizeof(shape[0]);

  BOOST_CHECK_THROW(raspereza::Polygon testPolygon(qVertex, shape), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(IncorrectScaleTest)
{
  raspereza::point_t shape[] = {{3.0, 2.0}, {7.0, 4.0}, {4.0, 6.0}, {1.0, 8.0}};
  std::size_t qVertex = sizeof(shape) / sizeof(shape[0]);
  raspereza::Polygon testPolygon(qVertex, shape);

  BOOST_CHECK_THROW(testPolygon.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
