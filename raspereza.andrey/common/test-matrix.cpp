#include <boost/test/auto_unit_test.hpp>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "division.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(copyAndMoveConstructor)
{
  raspereza::Rectangle testRectangle({1.4, 0.7}, 3.1, 2.4);
  raspereza::Circle testCircle({3.5, 4.5}, 3.2);

  raspereza::CompositeShape testShapes(std::make_shared<raspereza::Rectangle>(testRectangle));
  testShapes.add(std::make_shared<raspereza::Circle>(testCircle));
  raspereza::Matrix testMatrix = raspereza::divide(testShapes);

  BOOST_CHECK_NO_THROW(raspereza::Matrix testMatrix2(testMatrix));
  BOOST_CHECK_NO_THROW(raspereza::Matrix testMatrix3(std::move(testMatrix)));
}

BOOST_AUTO_TEST_CASE(operatorTests)
{
  raspereza::Rectangle rectangle1({1.4, 0.7}, 3.1, 2.4);
  raspereza::Circle circle1({3.5, 4.5}, 3.2);

  raspereza::CompositeShape testShapes1(std::make_shared<raspereza::Rectangle>(rectangle1));
  testShapes1.add(std::make_shared<raspereza::Circle>(circle1));
  raspereza::Matrix matrix1 = raspereza::divide(testShapes1);

  BOOST_CHECK_THROW(matrix1[100], std::out_of_range);
  BOOST_CHECK_THROW(matrix1[-1], std::out_of_range);

  BOOST_CHECK_NO_THROW(raspereza::Matrix matrix2 = matrix1);
  BOOST_CHECK_NO_THROW(raspereza::Matrix matrix3 = std::move(matrix1));

  raspereza::Rectangle rectangle2({4.2, 2.5}, 1.9, 3.7);
  raspereza::CompositeShape testShapes2(std::make_shared<raspereza::Rectangle>(rectangle2));

  raspereza::Matrix matrix4 = raspereza::divide(testShapes2);
  raspereza::Matrix matrix5 = raspereza::divide(testShapes2);

  BOOST_CHECK(matrix4 == matrix5);

  BOOST_CHECK(matrix4 != matrix1);
  BOOST_CHECK(matrix5 != matrix1);
}

BOOST_AUTO_TEST_SUITE_END()
