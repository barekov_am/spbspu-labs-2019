#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(testCompositeShape)

const double EPSILON = 0.001;

BOOST_AUTO_TEST_CASE(CopyConstructor)
{
  raspereza::Rectangle testRectangle({2.0, 4.0}, 5.0, 5.0);
  raspereza::Circle testCircle({5.1, 4.2}, 4.0);

  raspereza::CompositeShape composite1_(std::make_shared<raspereza::Rectangle>(testRectangle));
  composite1_.add(std::make_shared<raspereza::Circle>(testCircle));
  raspereza::CompositeShape composite2_(composite1_);

  BOOST_CHECK_CLOSE(composite1_.getFrameRect().pos.x, composite2_.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(composite1_.getFrameRect().pos.y, composite2_.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(composite1_.getFrameRect().width, composite2_.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(composite1_.getFrameRect().height, composite2_.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(composite1_.getArea(), composite2_.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(composite1_.getQuantity(), composite2_.getQuantity());
}

BOOST_AUTO_TEST_CASE(MoveConstructor)
{
  raspereza::Rectangle testRectangle({2.0, 4.0}, 5.0, 5.0);
  raspereza::Circle testCircle({5.1, 4.2}, 4.0);

  raspereza::CompositeShape composite1_(std::make_shared<raspereza::Rectangle>(testRectangle));
  composite1_.add(std::make_shared<raspereza::Circle>(testCircle));

  const raspereza::rectangle_t testFrame = composite1_.getFrameRect();
  const double testArea = composite1_.getArea();
  const size_t testQuantity = composite1_.getQuantity();

  raspereza::CompositeShape composite2_(std::move(composite1_));

  BOOST_CHECK_CLOSE(testFrame.pos.x, composite2_.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.pos.y, composite2_.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.width, composite2_.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.height, composite2_.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(testArea, composite2_.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(testQuantity, composite2_.getQuantity());
}

BOOST_AUTO_TEST_CASE(CopyOperator)
{
  raspereza::Rectangle testRectangle({2.0, 4.0}, 5.0, 5.0);
  raspereza::Circle testCircle({5.1, 4.2}, 4.0);

  raspereza::CompositeShape composite1_(std::make_shared<raspereza::Rectangle>(testRectangle));
  composite1_.add(std::make_shared<raspereza::Circle>(testCircle));
  raspereza::CompositeShape composite2_;
  composite2_ = composite1_;

  BOOST_CHECK_CLOSE(composite1_.getFrameRect().pos.x, composite2_.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(composite1_.getFrameRect().pos.y, composite2_.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(composite1_.getFrameRect().width, composite2_.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(composite1_.getFrameRect().height, composite2_.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(composite1_.getArea(), composite2_.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(composite1_.getQuantity(), composite2_.getQuantity());
}

BOOST_AUTO_TEST_CASE(MoveOperator)
{
  raspereza::Rectangle testRectangle({2.0, 4.0}, 5.0, 5.0);
  raspereza::Circle testCircle({5.1, 4.2}, 4.0);

  raspereza::CompositeShape composite1_(std::make_shared<raspereza::Rectangle>(testRectangle));
  composite1_.add(std::make_shared<raspereza::Circle>(testCircle));

  const raspereza::rectangle_t testFrame = composite1_.getFrameRect();
  const double testArea = composite1_.getArea();
  const size_t testQuantity = composite1_.getQuantity();

  raspereza::CompositeShape composite2_;
  composite2_ = std::move(composite1_);

  BOOST_CHECK_CLOSE(testFrame.pos.x, composite2_.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.pos.y, composite2_.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.width, composite2_.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(testFrame.height, composite2_.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(testArea, composite2_.getArea(), EPSILON);
  BOOST_CHECK_EQUAL(testQuantity, composite2_.getQuantity());
}

BOOST_AUTO_TEST_CASE(MoveTests)
{
  raspereza::Rectangle testRectangle({2.0, 4.0}, 5.0, 5.0);
  raspereza::Circle testCircle({5.1, 4.2}, 4.0);
  raspereza::Triangle testTriangle({6.0, 6.0}, {1.0, 3.0}, {4.0, 5.0});
  raspereza::point_t shape[] = {{3.0, 2.0}, {7.0, 4.0}, {4.0, 6.0}, {1.0, 8.0}};
  std::size_t qVertex = sizeof(shape) / sizeof(shape[0]);
  raspereza::Polygon testPolygon({qVertex, shape});

  raspereza::CompositeShape testComposite(std::make_shared<raspereza::Rectangle>(testRectangle));
  testComposite.add(std::make_shared<raspereza::Circle>(testCircle));
  testComposite.add(std::make_shared<raspereza::Triangle>(testTriangle));
  testComposite.add(std::make_shared<raspereza::Polygon>(testPolygon));

  const raspereza::rectangle_t getFrameBeforeMove = testComposite.getFrameRect();
  const double getAreaBeforeMove = testComposite.getArea();

  testComposite.move({4, 5});
  raspereza::rectangle_t getFrameAfterMove = testComposite.getFrameRect();
  double getAreaAfterMove = testComposite.getArea();

  BOOST_CHECK_CLOSE(getFrameBeforeMove.width, getFrameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(getFrameBeforeMove.height, getFrameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(getAreaBeforeMove, getAreaAfterMove, EPSILON);

  testComposite.move(-2, 3);
  getFrameAfterMove = testComposite.getFrameRect();
  getAreaAfterMove = testComposite.getArea();

  BOOST_CHECK_CLOSE(getFrameBeforeMove.width, getFrameAfterMove.width, EPSILON);
  BOOST_CHECK_CLOSE(getFrameBeforeMove.height, getFrameAfterMove.height, EPSILON);
  BOOST_CHECK_CLOSE(getAreaBeforeMove, getAreaAfterMove, EPSILON);
}

BOOST_AUTO_TEST_CASE(TestScale)
{
  raspereza::Rectangle testRectangle({2.0, 4.0}, 5.0, 5.0);
  raspereza::Circle testCircle({5.1, 4.2}, 4.0);
  raspereza::Triangle testTriangle({6.0, 6.0}, {1.0, 3.0}, {4.0, 5.0});
  raspereza::point_t shape[] = {{3.0, 2.0}, {7.0, 4.0}, {4.0, 6.0}, {1.0, 8.0}};
  std::size_t qVertex = sizeof(shape) / sizeof(shape[0]);
  raspereza::Polygon testPolygon({qVertex, shape});

  raspereza::CompositeShape testComposite(std::make_shared<raspereza::Rectangle>(testRectangle));
  testComposite.add(std::make_shared<raspereza::Circle>(testCircle));
  testComposite.add(std::make_shared<raspereza::Triangle>(testTriangle));
  testComposite.add(std::make_shared<raspereza::Polygon>(testPolygon));

  const double getAreaBeforeScale = testComposite.getArea();
  const double ratio = 3;
  testComposite.scale(ratio);
  double getAreaAfterScale = testComposite.getArea();

  BOOST_CHECK_CLOSE(getAreaBeforeScale * ratio * ratio, getAreaAfterScale, EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidParametrs)
{
  raspereza::Rectangle testRectangle({2.0, 4.0}, 5.0, 5.0);
  raspereza::Triangle testTriangle({6.0, -6.0}, {-1.0, -3.0}, {-4.0, 5.0});

  raspereza::CompositeShape testComposite(std::make_shared<raspereza::Rectangle>(testRectangle));
  testComposite.add(std::make_shared<raspereza::Triangle>(testTriangle));

  BOOST_CHECK_THROW(testComposite.scale(-3), std::invalid_argument);
  BOOST_CHECK_THROW(testComposite.remove(8), std::logic_error);
  BOOST_CHECK_THROW(testComposite.remove(-6), std::logic_error);
  BOOST_CHECK_THROW(testComposite[8], std::out_of_range);
  BOOST_CHECK_THROW(testComposite[-6], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
