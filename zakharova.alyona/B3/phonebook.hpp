#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <string>
#include <list>
#include <memory>

class Phonebook
{
public:
  struct record_t
  {
    std::string name;
    std::string number;
  };

  using iterator = std::list<record_t>::iterator;

  Phonebook();

  iterator begin();
  iterator end();

  bool empty() const;

  iterator insert_before(iterator iter, const record_t& record);
  iterator insert_after(iterator iter, const record_t& record);

  iterator erase(iterator iter);

  void push_back(const record_t& record);

  iterator move(iterator iter, int n);

private:
  std::shared_ptr<std::list<record_t>> records_;
};

#endif // !PHONEBOOK_HPP
