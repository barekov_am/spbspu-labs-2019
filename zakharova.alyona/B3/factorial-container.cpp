#include "factorial-container.hpp"
#include <stdexcept>

size_t factorial(size_t n)
{
  if (n == 1)
  {
    return 1;
  }

  return n * factorial(n - 1);
}

factorial_container::iterator::iterator() :
  pos_(1)
{}

factorial_container::iterator::iterator(size_t pos) :
  pos_(pos)
{}

bool factorial_container::iterator::operator ==(const iterator& rhs) const
{
  return pos_ == rhs.pos_;
}

bool factorial_container::iterator::operator !=(const iterator& rhs) const
{
  return pos_ != rhs.pos_;
}


size_t factorial_container::iterator::operator *() const
{
  if ((pos_ == 0) || (pos_ == 11))
  {
    throw std::out_of_range("Out of range!");
  }

  return factorial(pos_);
}

factorial_container::iterator& factorial_container::iterator::operator ++()
{
  if (pos_ == 11)
  {
    throw std::out_of_range("Out of range");
  }

  pos_++;
  return *this;
}

factorial_container::iterator factorial_container::iterator::operator ++(int)
{
  iterator temp(*this);
  this->operator++();
  return temp;
}

factorial_container::iterator& factorial_container::iterator::operator --()
{
  if (pos_ == 0)
  {
    throw std::out_of_range("Out of range");
  }

  pos_--;
  return *this;
}

factorial_container::iterator factorial_container::iterator::operator --(int)
{
  iterator temp(*this);
  this->operator--();
  return temp;
}

factorial_container::factorial_container()
{}

factorial_container::iterator factorial_container::begin()
{
  return iterator(1);
}

factorial_container::iterator factorial_container::end()
{
  return iterator(11);
}

factorial_container::reverse_iterator factorial_container::rbegin()
{
  return reverse_iterator(10);
}

factorial_container::reverse_iterator factorial_container::rend()
{
  return reverse_iterator(0);
}

factorial_container::reverse_iterator::reverse_iterator() :
  pos_(10)
{}

factorial_container::reverse_iterator::reverse_iterator(size_t pos) :
  pos_(pos)
{}

factorial_container::reverse_iterator& factorial_container::reverse_iterator::operator ++()
{
  if (pos_ == 0)
  {
    throw std::out_of_range("Out of range");
  }

  pos_--;
  return *this;
}

factorial_container::reverse_iterator& factorial_container::reverse_iterator::operator --()
{
  if (pos_ == 11)
  {
    throw std::out_of_range("Out of range");
  }

  pos_++;
  return *this;
}

size_t factorial_container::reverse_iterator::operator *() const
{
  if ((pos_ < 1) || (pos_ > 10))
  {
    throw std::out_of_range("Out of range");
  }

  return factorial(pos_);
}

bool factorial_container::reverse_iterator::operator ==(const reverse_iterator& rhs) const
{
  return pos_ == rhs.pos_;
}

bool factorial_container::reverse_iterator::operator !=(const reverse_iterator& rhs) const
{
  return pos_ != rhs.pos_;
}
