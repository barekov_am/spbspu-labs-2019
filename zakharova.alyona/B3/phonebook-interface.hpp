#ifndef PHONEBOOK_INTERFACE_HPP
#define PHONEBOOK_INTERFACE_HPP

#include <map>
#include <string>
#include <sstream>
#include "phonebook.hpp"

class PhonebookInterface
{
public:
  PhonebookInterface();

  void readCommand(std::stringstream& input);

  void add(const Phonebook::record_t& record);
  void insert(const std::string& place, const std::string& markName, const Phonebook::record_t& record);
  void erase(const std::string& markName);
  void show(const std::string& markName);
  void move(const std::string& markName, const int steps);
  void moveToFirst(const std::string& markName);
  void moveToLast(const std::string& markName);

private:
  using bookmarks = std::map<std::string, Phonebook::iterator>;
  bookmarks bookmarks_;
  Phonebook phonebook_;

  std::string getName(std::string& name);
  std::string getNumber(std::string& number);
  std::string getMarkName(std::string& markName);

  bookmarks::iterator getIterator(const std::string& markName);
};

#endif // !PHONEBOOK_INTERFACE_HPP

