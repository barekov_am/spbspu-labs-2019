#include "phonebook.hpp"
#include <stdexcept>

Phonebook::Phonebook():
  records_(new std::list<record_t>)
{}

Phonebook::iterator Phonebook::begin()
{
  return records_->begin();
}

Phonebook::iterator Phonebook::end()
{
  return records_->end();
}

bool Phonebook::empty() const
{
  return records_->empty();
}

Phonebook::iterator Phonebook::insert_before(Phonebook::iterator iter, const record_t& record)
{
  return records_->insert(iter, record);
}

Phonebook::iterator Phonebook::insert_after(Phonebook::iterator iter, const record_t& record)
{
  return records_->insert(std::next(iter), record);
}

Phonebook::iterator Phonebook::erase(Phonebook::iterator iter)
{
  iter = records_->erase(iter);

  if ((iter == records_->end()) && !(records_->empty()))
  {
    --iter;
  }
  
  return iter;
}

void Phonebook::push_back(const record_t& record)
{
  records_->push_back(record);
}

Phonebook::iterator Phonebook::move(Phonebook::iterator iter, int n)
{
  if (n > 0)
  {
    for (int i = 0; i < n; ++i)
    {
      if (iter++ == records_->end())
      {
        throw std::out_of_range("Out of range");
      }
    }
  }
  else
  {
    for (int i = 0; n < i; --i)
    {
      if (iter-- == records_->begin())
      {
        throw std::out_of_range("Out of range");
      }
    }
  }

  return iter;
}
