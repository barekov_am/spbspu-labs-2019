#include <iostream>
#include <algorithm>
#include "factorial-container.hpp"

void task2()
{
  factorial_container container;
  factorial_container::iterator iterator;

  for (iterator = container.begin(); iterator != container.end(); iterator++)
  {
    std::cout << *iterator << " ";
  }

  std::cout << "\n";

  std::copy(container.rbegin(), container.rend(), std::ostream_iterator<size_t>(std::cout, " "));

  std::cout << "\n";
}
