#ifndef TEXT_HPP
#define TEXT_HPP

#include <list>
#include <string>
#include <iostream>

struct Word
{
  std::string word;
  size_t size;
  bool isPunct;
};

class Text
{
public:
  Text(size_t width);

  void read();
  void printStrings();

private:
  size_t width_;
  std::list<Word> text_;

  char sign_;

  char point_;

  Word readWord();
  Word readNumber();

  bool isPunct(char& ch);
};

#endif // !TEXT_HPP
