#include <iostream>
#include <cstring>
#include "text.hpp"
#include "constants.hpp"

int main(int argc, char* argv[])
{
  try
  {
    size_t width;

    if (argc == 1)
    {
      width = DEFAULT_WIDTH;
    }
    else if (argc == 3)
    {
      if (strcmp(argv[1], "--line-width"))
      {
        std::cerr << "Invalid parameter";
        return 1;
      }

      width = std::stoi(argv[2]);
      if (width < MIN_WIDTH)
      {
        std::cerr << "Width is too short";
        return 1;
      }
    }

    else
    {
      std::cerr << "Invalid number of arguments";
      return 1;
    }

    Text text(width);
    text.read();
    text.printStrings();
  }
  catch (std::exception& e)
  {
    std::cout << e.what();
    return 1;
  }
  return 0;
}
