#ifndef CONSTANTS_HPP
#define CONSTATNTS_HPP

#include <cstddef>

const std::size_t MAX_SIZE = 20;
const std::size_t MIN_WIDTH = 25;
const std::size_t DEFAULT_WIDTH = 40;

#endif // !CONSTANTS_HPP
