#include "text.hpp"

#include <stdexcept>
#include <cctype>
#include <locale>

#include "constants.hpp"

Text::Text(size_t width):
  width_(width),
  sign_(' '),
  point_(std::use_facet<std::numpunct<char>>(std::cout.getloc()).decimal_point())
{}

void Text::read()
{
  char currentSymbol;
  std::cin >> std::ws;

  if (!std::cin.eof())
  {
    currentSymbol = std::cin.peek();
  }
  else
  {
    return;
  }

  if (isPunct(currentSymbol))
  {
    throw std::invalid_argument("Invalid input. Text cannot begin with punctuiation");
  }
  else if (currentSymbol == '-')
  {
    std::cin.get();
    currentSymbol = std::cin.peek();
    if (std::isdigit(currentSymbol))
    {
      sign_ = '-';
      text_.push_back(readNumber());
      sign_ = ' ';
    }
    else
    {
      throw std::invalid_argument("Invalid input. Text cannot begin with punctuiation"); //dash
    }
  }
  
  while (std::cin)
  {
    if (std::isalpha(currentSymbol))
    {
      text_.push_back(readWord());
    }
    else if (std::isdigit(currentSymbol))
    {
      text_.push_back(readNumber());
    }
    else if ((currentSymbol == '-') || (currentSymbol == '+'))
    {
      std::cin.get();
      if (std::isdigit(std::cin.peek()))
      {
        sign_ = currentSymbol;
        text_.push_back(readNumber());
        sign_ = ' ';
      }
      else if (currentSymbol == '-')
      {
        if (std::cin.peek() == '-')
        {
          std::cin.get();
          if (std::cin.peek() == '-')
          {
            std::cin.get();
            Word previous = text_.back();
            if ((previous.word == ",") || (!previous.isPunct))
            {
              text_.push_back({ "---", 3, true });
            }
            else
            {
              throw std::invalid_argument("Punctuation can't be in a row");
            }
          }
          else
          {
            throw std::invalid_argument("Invalid input");
          }
        }
        else
        {
          throw std::invalid_argument("Invalid input");
        }
      }  
    }
    else if (isPunct(currentSymbol))
    {
      std::cin.get();

      Word previous = text_.back();
      if (previous.isPunct)
      {
        throw std::invalid_argument("Punctuation can't be in a row");
      }

      std::string punctuation;
      punctuation += currentSymbol;
      text_.push_back({ punctuation, 1, true });
    }
    else if (std::isspace(currentSymbol))
    {
      std::cin >> std::ws;
    }

    currentSymbol = std::cin.peek();
  }
}

bool Text::isPunct(char& ch)
{
  if ((ch == '.') || (ch == ',') || (ch == '!') || (ch == '?') || (ch == ':') || (ch == ';'))
  {
    return true;
  }
  return false;
}

Word Text::readWord()
{
  std::string word;
  size_t wordSize = 1;
  word += std::cin.get();

  char symbol = std::cin.peek();

  while (!(isPunct(symbol) || std::isspace(symbol) || (symbol == EOF)))
  {
    std::cin.get();

    if (std::isalpha(symbol))
    {
      word += symbol;
    }
    else if (symbol == '-')
    {
      if (std::isalpha(std::cin.peek()) || std::isspace(std::cin.peek()))
      {
        word += symbol;
      }
      else if (std::cin.peek() == '-')
      {
        std::cin.get();
        if (std::cin.get() == '-')
        {
          text_.push_back({ word, wordSize, false });
          return { "---", 3, true };
        }
        else
        {
          throw std::invalid_argument("Invalid word"); // 2 dashes in a row
        }
      }
    }
    else if (std::isdigit(symbol))
    {
      throw std::invalid_argument("Invalid word");
    }
    else
    {
      break;
    }

    wordSize++;

    if (wordSize > MAX_SIZE)
    {
      throw std::invalid_argument("Word length can't be more than 20 characters");
    }

    symbol = std::cin.peek();
  }

  return { word, wordSize, false };
}

Word Text::readNumber()
{
  std::string number;
  size_t numberSize = 0;

  if (sign_ != ' ')
  {
    number += sign_;
    numberSize++;
    sign_ = ' ';
  }

  number += std::cin.get();
  numberSize++;

  bool real = false;

  char symbol = std::cin.peek();

  while (!(std::isspace(symbol)) || (symbol != EOF))
  {
    std::cin.get();

    if (std::isdigit(symbol))
    {
      number += symbol;
      numberSize++;
    }
    else if (symbol == point_)
    {
      if (!real)
      {
        if (std::isdigit(std::cin.peek()))
        {
          number += symbol;
          numberSize++;
          real = true;
        }
        else
        {
          break;
        }
      }
      else
      {
        break;
      }
    }
    else if (std::isalpha(symbol))
    {
      throw std::invalid_argument("Invalid number");
    }
    else
    {
      break;
    }

    if (numberSize > MAX_SIZE)
    {
      throw std::invalid_argument("Number length can't be more than 20 characters");
    }

    symbol = std::cin.peek();
  }

  return { number, numberSize, false };
}

void Text::printStrings()
{
  if (text_.empty())
  {
    return;
  }

  std::string currentStr = text_.begin()->word;
  size_t currentWidth = text_.begin()->size;

  std::string lastWord = text_.begin()->word;

  auto iter = ++text_.begin();

  while (iter != text_.end())
  {
    if (!iter->isPunct) // word
    {
      if (currentWidth + 1 + iter->size <= width_)
      {
        if (currentWidth != 0)
        {
          currentStr += " ";
          currentWidth++;
        }
        currentStr += iter->word;
        currentWidth += iter->size;
      }
      else
      {
        std::cout << currentStr << "\n";

        currentStr = iter->word;
        currentWidth = iter->size;
      }

      lastWord = iter->word;
    }
    else // if punctuation
    {
      if (iter->size == 1) //not dash
      {
        if ((iter->word == ",") && (std::next(iter)->isPunct)) // dash after comma
        {
          iter++;
          if (currentWidth + 5 < width_)
          {
            currentStr += ", ---";
            currentWidth += 5;
          }
          else if (currentWidth + 5 == width_)
          {
            std::cout << currentStr << ", ---\n";

            currentStr.clear();
            currentWidth = 0;
          }
          else
          {
            currentStr.erase(currentWidth - lastWord.length() - 1, lastWord.length() + 1);
            std::cout << currentStr << "\n";

            currentStr = lastWord;
            currentStr += ", ---";
            currentWidth = lastWord.length() + 5;
          }
        }
        else
        {
          if (currentWidth + 1 < width_)
          {
            currentStr += iter->word;
            currentWidth++;
          }
          else if (currentWidth + 1 == width_)
          {
            std::cout << currentStr << iter->word << "\n";

            currentStr.clear();
            currentWidth = 0;
          }
          else
          {
            currentStr.erase(currentWidth - lastWord.length() - 1, lastWord.length() + 1);
            std::cout << currentStr << "\n";

            currentStr = lastWord;
            currentStr += iter->word;
            currentWidth = lastWord.length() + 1;
          }
        }
      }
      else
      {
        if (currentWidth + 4 < width_)
        {
          currentStr += " ---";
          currentWidth += 4;
        }
        else if (currentWidth + 4 == width_)
        {
          std::cout << currentStr << " ---\n";

          currentStr.clear();
          currentWidth = 0;
        }
        else
        {
          currentStr.erase(currentWidth - lastWord.length() - 1, lastWord.length() + 1);
          std::cout << currentStr << "\n";
          
          currentStr = lastWord;
          currentStr += " ---";
          currentWidth = lastWord.length() + 4;
        }
      }
    }
    iter++;
  }

  if (!currentStr.empty())
  {
    std::cout << currentStr << "\n";
  }
}
