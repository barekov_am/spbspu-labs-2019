#include "tasks.hpp"
#include "auxiliary-functions.hpp"
#include <fstream>
#include <stdexcept>
#include <vector>
#include <memory>

void task2(const char* fileName)
{
  std::ifstream inputStream(fileName);

  if (!inputStream)
  {
    throw std::invalid_argument("Invalid file path.");
  }

  size_t size = 100;
  size_t index = 0;

  std::unique_ptr<char[], decltype(&free)> array(static_cast<char*>(malloc(size)), &free);

  if (!array)
  {
    throw std::runtime_error("Cannot allocate memory.");
  }

  while (inputStream)
  {
    inputStream.read(&array[index], size - index);
    index += inputStream.gcount();
    
    if (index == size)
    {
      size *= 1.8;
      std::unique_ptr<char[], decltype(&free)> newArray(static_cast<char*>(realloc(array.get(), size)), &free);

      if (!newArray)
      {
        throw std::runtime_error("Cannot allocate memory.");
      }

      array.release();
      std::swap(array, newArray);
    }
  }

  inputStream.close();

  std::vector<char> vector(&array[0], &array[index]);
  print(vector, false);
}
