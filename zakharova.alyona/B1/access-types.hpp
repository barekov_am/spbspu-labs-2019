#ifndef ACCESS_TYPES_HPP
#define ACCESS_TYPES_HPP

template <typename Container>
struct byBrackets
{
  typedef typename Container::size_type index;

  static typename Container::reference getElement(Container& container, const index index)
  {
    return container[index];
  }
  static index getBegin(const Container&)
  {
    return 0;
  }
  static index getEnd(const Container& container)
  {
    return container.size();
  }

};

template <typename Container>
struct byAt
{
  typedef typename Container::size_type index;

  static typename Container::reference getElement(Container& container, const index index)
  {
    return container.at(index);
  }
  static index getBegin(const Container&)
  {
    return 0;
  }
  static index getEnd(const Container& container)
  {
    return container.size();
  }

};

template <typename Container>
struct byIterator
{
  typedef typename Container::iterator index;

  static typename Container::reference getElement(Container&, const index iter)
  {
    return *iter;
  }
  static index getBegin(Container& container)
  {
    return container.begin();
  }
  static index getEnd(Container& container)
  {
    return container.end();
  }
};

#endif // !ACCESS_TYPES_HPP
