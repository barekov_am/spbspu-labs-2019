#include <stdexcept>
#include <iostream>
#include "tasks.hpp"

int main(int argc, char* argv[])
{
  try
  {
    if ((argc < 2) || (argc > 4))
    {
      std::cerr << "Invalid number of parameters.";
      return 1;
    }

    int task = atoi(argv[1]);

    switch (task)
    {
    case 1:
      if (argc == 3)
      {
        task1(argv[2]);
      }
      else
      {
        std::cerr << "Invalid number of parameters.";
        return 1;
      }
      break;

    case 2:
      if (argc == 3)
      {
        task2(argv[2]);
      }
      else
      {
        std::cerr << "Invalid number of parameters.";
        return 1;
      }
      break;

    case 3:
      if (argc == 2)
      {
        task3();
      }
      else
      {
        std::cerr << "Invalid number of parameters.";
        return 1;
      }
      break;

    case 4:
      if (argc == 4)
      {
        task4(argv[2], argv[3]);
      }
      else
      {
        std::cerr << "Invalid number of parameters.";
        return 1;
      }
      break;

    default:
      std::cerr << "Invalid parameter.";
      return 1;
    }
  }
  catch (std::invalid_argument& e)
  {
    std::cerr << e.what();
    return 1;
  }
  catch (std::exception& e)
  {
    std::cerr << e.what();
    return 2;
  }

  return 0;
}
