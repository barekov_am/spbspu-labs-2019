#include "tasks.hpp"
#include "auxiliary-functions.hpp"
#include <iostream>
#include <stdexcept>
#include <vector>

void task3()
{
  std::vector<int> vector;
  int value = 0;

  while (std::cin && !(std::cin >> value).eof())
  {
    if (std::cin.fail() || std::cin.bad())
    {
      throw std::invalid_argument("Invalid input. Only integers are allowed.");
    }

    if (value == 0)
    {
      break;
    }

    vector.push_back(value);
  }

  if (value != 0)
  {
    throw std::invalid_argument("Invalid input. The sequence must end with zero.");
  }

  if (vector.empty())
  {
    return;
  }

  int lastValue = vector.back();

  if (lastValue == 1)
  {
    auto iter = vector.begin();
    while (iter != vector.end())
    {
      if (*iter % 2 == 0)
      {
        iter = vector.erase(iter);
      }
      else
      {
        iter++;
      }
    }
  }

  else if (lastValue == 2)
  {
    auto iter = vector.begin();
    while (iter != vector.end())
    {
      if (*iter % 3 == 0)
      {
        iter = vector.insert(iter + 1, 3, 1) + 3;
      }
      else
      {
        iter++;
      }
    }
  }

  print(vector, true);
}
