#include "auxiliary-functions.hpp"
#include <cstring>
#include <stdexcept>

bool isAscending(const char* order)
{
  if (strcmp(order, "ascending") == 0)
  {
    return true;
  }
  else if (strcmp(order, "descending") == 0)
  {
    return false;
  }
  else
  {
    throw std::invalid_argument("Invalid order.");
  }
}
