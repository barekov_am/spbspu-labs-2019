#ifndef AUXILIARY_FUNCTIONS_HPP
#define AUXILIARY_FUNCTIONS_HPP

#include <iostream>
#include <memory>

bool isAscending(const char* order);

template <template <typename Container> typename Access, typename Container>
void sort(Container& container, const bool ascending)
{
  typedef typename Access<Container>::index index;

  for (index i = Access<Container>::getBegin(container) ; i != Access<Container>::getEnd(container); ++i)
  {
    for (index j = i; j != Access<Container>::getEnd(container); ++j)
    {
      if (ascending)
      {
        if (Access<Container>::getElement(container, i) > Access<Container>::getElement(container, j))
        {
          std::swap(Access<Container>::getElement(container, i), Access<Container>::getElement(container, j));
        }
      }
      else
      {
        if (Access<Container>::getElement(container, i) < Access<Container>::getElement(container, j))
        {
          std::swap(Access<Container>::getElement(container, i), Access<Container>::getElement(container, j));
        }
      }
    }
  }
}

template <typename Container>
void print(const Container& container, const bool withSpaces)
{
  if (container.empty())
  {
    return;
  }

  if (withSpaces == true)
  {
    for (auto iter = container.begin(); iter != container.end(); ++iter)
    {
      std::cout << *iter << " ";
    }
    std::cout << "\n";
  }
  else
  {
    for (auto iter = container.begin(); iter != container.end(); ++iter)
    {
      std::cout << *iter;
    }
  }
}

#endif // !AUXILIARY_FUNCTIONS_HPP
