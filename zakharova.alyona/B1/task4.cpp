#include "tasks.hpp"
#include "auxiliary-functions.hpp"
#include "access-types.hpp"
#include <stdexcept>
#include <ctime>
#include <cmath>
#include <vector>

void fillRandom(double* array, const int size)
{
  srand(time(nullptr));
  double value = 0;

  for (int i = 0; i < size; ++i)
  {
    value = rand() * 2.0 / RAND_MAX - 1.0;
    array[i] = round(value * 10) / 10;
  }
}
void task4(const char* order, const char* vectorSize)
{
  bool ascending = isAscending(order);
  int size = atoi(vectorSize);

  if (size <= 0)
  {
    throw std::invalid_argument("Invalid input. Vector size must be positive.");
  }

  std::vector<double> vector(size);
  fillRandom(vector.data(), size);
  print(vector, true);

  sort<byBrackets>(vector, ascending);
  print(vector, true);
}
