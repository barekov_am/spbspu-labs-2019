#include "tasks.hpp"
#include "access-types.hpp"
#include "auxiliary-functions.hpp"
#include <iostream>
#include <stdexcept>
#include <vector>
#include <forward_list>

void task1(const char* order)
{
  bool ascending = isAscending(order);

  std::vector<int> vector1;
  int value = 0;

  while (std::cin && !(std::cin >> value).eof())
  {
    if (std::cin.fail() || std::cin.bad())
    {
      throw std::invalid_argument("Invaalid input. Only integers allowed.");
    }

    vector1.push_back(value);
  }

  if (vector1.empty())
  {
    return;
  }

  std::vector<int> vector2(vector1);
  std::forward_list<int> list(vector1.begin(), vector1.end());
  
  sort<byBrackets>(vector1, ascending);
  sort<byAt>(vector2, ascending);
  sort<byIterator>(list, ascending);

  print(vector1, true);
  print(vector2, true);
  print(list, true);
}
