#include "tasks.hpp"
#include "queue-with-priotrity.hpp"
#include <string>
#include <sstream>
#include <iostream>
#include <stdexcept>

void task1()
{
  QueueWithPriority<std::string> queue;

  std::string line;
  
  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::runtime_error("Error while readling");
    }

    std::stringstream stream(line);

    std::string command;
    std::string priority;
    std::string data;

    stream >> command;

    if (command == "add")
    {
      stream >> priority >> data;

      if (!stream.eof())
      {
        std::getline(stream, line);
        data += line;
      }

      if (!stream.eof() || data.empty())
      {
        std::cout << "<INVALID COMMAND>\n";
        break;
      }

      if (priority == "low")
      {
        queue.PutElementToQueue(data, LOW);
      }
      else if (priority == "normal")
      {
        queue.PutElementToQueue(data, NORMAL);
      }
      else if (priority == "high")
      {
        queue.PutElementToQueue(data, HIGH);
      }
      else
      {
        std::cout << "<INVALID COMMAND>\n";
      }
    }

    else if (command == "get")
    {
      if (queue.empty())
      {
        std::cout << "<EMPTY>\n";
      }
      else
      {
        std::cout << queue.GetElementFromQueue() << "\n";
      }
    }

    else if (command == "accelerate")
    {
      if (queue.empty())
      {
        std::cout << "<EMPTY>\n";
      }
      else
      {
        queue.Accelerate();
      }
    }

    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
