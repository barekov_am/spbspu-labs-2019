#ifndef QUEUE_WITH_PRIORITY_HPP
#define QUEUE_WITH_PRIORITY_HPP

#include <deque>
#include <mutex>
#include <iostream>

typedef enum
{
  LOW,
  NORMAL,
  HIGH
} ElementPriority;

template <typename T>
class QueueWithPriority
{
public:
  void PutElementToQueue(const T& element, ElementPriority priority);
  T GetElementFromQueue();
  void Accelerate();

  bool empty() const;

private:
  std::deque<T> highPriority_;
  std::deque<T> normalPriority_;
  std::deque<T> lowPriority_;

  std::mutex mutex_;
};



template <typename T>
void QueueWithPriority<T>::PutElementToQueue(const T& element, ElementPriority priority)
{
  std::lock_guard<std::mutex> guard(mutex_);

  switch (priority)
  {
  case ElementPriority::LOW:
    lowPriority_.push_back(element);
    break;
  case ElementPriority::NORMAL:
    normalPriority_.push_back(element);
    break;
  case ElementPriority::HIGH:
    highPriority_.push_back(element);
    break;
  }
}

template <typename T>
T QueueWithPriority<T>::GetElementFromQueue()
{
  std::lock_guard<std::mutex> guard(mutex_);

  if (!highPriority_.empty())
  {
    T element = highPriority_.front();
    highPriority_.pop_front();
    return element;
  }
  else if (!normalPriority_.empty())
  {
    T element = normalPriority_.front();
    normalPriority_.pop_front();
    return element;
  }
  else
  {
    T element = lowPriority_.front();
    lowPriority_.pop_front();
    return element;
  }
}

template <typename T>
void QueueWithPriority<T>::Accelerate()
{
  std::lock_guard<std::mutex> guard(mutex_);

  highPriority_.insert(highPriority_.end(), lowPriority_.begin(), lowPriority_.end());
  lowPriority_.clear();
}

template <typename T>
bool QueueWithPriority<T>::empty() const
{
  return (highPriority_.empty() && normalPriority_.empty() && lowPriority_.empty());
}

#endif // !QUEUE_WITH_PRIORITY_HPP
