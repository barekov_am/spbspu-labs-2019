#include "tasks.hpp"
#include <list>
#include <iostream>
#include <stdexcept>

void print(std::list<int>::iterator it, std::list<int>::reverse_iterator rit, size_t count)
{
  if (count == 1)
  {
    std::cout << *it;
  }
  else if (count >= 2)
  {
    std::cout << *it << " ";
    std::cout << *rit << " ";
    count -= 2;
    it++;
    rit++;
    print(it, rit, count);
  }
}

void task2()
{
  std::list<int> list;
  int value = -1;
  size_t count = 0;

  while (std::cin && !(std::cin >> value).eof())
  {
    if (std::cin.bad() || std::cin.fail())
    {
      throw std::invalid_argument("Invalid input. The value must be an integer");
    }

    count++;

    if (count > 20)
    {
      throw std::invalid_argument("Invalid input. There must be no more than 20 values");
    }

    if ((value >= 1) && (value <= 20))
    {
      list.push_back(value);
    }
    else
    {
      throw std::invalid_argument("Invalid input. The value must be in the range of 1 to 20");
    }
  }

  if (list.empty())
  {
    return;
  }

  std::list<int>::iterator it = list.begin();
  std::list<int>::reverse_iterator rit = list.rbegin();
  print(it, rit, count);
  std::cout << "\n";
}
