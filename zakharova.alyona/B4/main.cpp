#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <iterator>

#include "data-struct.hpp"

int main()
{
  std::vector<DataStruct> vector;
  std::string line;

  try
  {
    while (std::getline(std::cin, line))
    {
      std::stringstream stream(line);
      vector.push_back(readData(stream));
    }
    
    if (!vector.empty())
    {
      std::sort(vector.begin(), vector.end(), ascending);

      std::copy(vector.begin(), vector.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));
    }

    return 0;
  }
  catch (std::invalid_argument& e)
  {
    std::cout << e.what();
    return 1;
  }
  catch (std::exception& e)
  {
    std::cout << e.what();
    return 2;
  }
}
