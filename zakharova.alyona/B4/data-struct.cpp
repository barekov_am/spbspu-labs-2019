#include "data-struct.hpp"

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <cctype>
#include <stdexcept>
#include <cmath>

const int MAX_VALUE = 5;

DataStruct readData(std::stringstream& stream)
{
  std::string first;
  std::string second;
  std::string str;
  char symbol;

  while (!stream.eof())
  {
    symbol = stream.get();

    if ((symbol == '-') || (symbol == '+'))
    {
      if (first.empty())
      {
        first += symbol;
      }
      else
      {
        throw std::invalid_argument("Invalid input. Key must be an integer from -5 to 5");
      }
    }
    else if (isdigit(symbol))
    {
      if (first.size() <= 1)
      { 
        first += symbol;
      }
      else
      {
        throw std::invalid_argument("Invalid input. Key must be an integer from -5 to 5");
      } 
    }
    else if (symbol == ',')
    {
      break;
    }
    else if (!isspace(symbol))
    {
      throw std::invalid_argument("Invalid input. Key must be an integer from -5 to 5");
    }
  }

  while (!stream.eof())
  {
    symbol = stream.get();
    if ((symbol == '-') || (symbol == '+'))
    {
      if (second.empty())
      {
        second += symbol;
      }
      else
      {
        throw std::invalid_argument("Invalid input. Key must be an integer from -5 to 5");
      }
    }
    else if (isdigit(symbol))
    {
      if (second.size() <= 1)
      {
        second += symbol;
      }
      else
      {
        throw std::invalid_argument("Invalid input. Key must be an integer from -5 to 5");
      }
    }
    else if (symbol == ',')
    {
      break;
    }
    else if (!isspace(symbol))
    {
      throw std::invalid_argument("Invalid input. Key must be an integer from -5 to 5");
    }
  }

  std::getline(stream, str);

  int key1 = stoi(first);
  int key2 = stoi(second);

  if ((std::abs(key1) > MAX_VALUE) || (std::abs(key2) > MAX_VALUE))
  {
    throw std::invalid_argument("Key values must be in [-5; 5]");
  }

  return { key1, key2, str };
}

bool ascending(DataStruct& dataStruct1, DataStruct& dataStruct2)
{
  if (dataStruct1.key1 == dataStruct2.key1)
  {
    if (dataStruct1.key2 == dataStruct2.key2)
    {
      return dataStruct1.str.length() < dataStruct2.str.length();
    }
    else
    {
      return dataStruct1.key2 < dataStruct2.key2;
    }
  }
  else
  {
    return dataStruct1.key1 < dataStruct2.key1;
  }
}

std::ostream& operator<<(std::ostream& cout, const DataStruct& dataStruct)
{
  cout << dataStruct.key1 << "," << dataStruct.key2 << "," << dataStruct.str;
  return cout;
}
