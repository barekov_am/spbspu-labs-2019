#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP

#include <string>
#include <sstream>
#include <iostream>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

DataStruct readData(std::stringstream& stream);

bool ascending(DataStruct& dataStruct1, DataStruct& dataStruct2);

std::ostream& operator<<(std::ostream& cout, const DataStruct& dataStruct);

#endif // !DATASTRUCT_HPP
