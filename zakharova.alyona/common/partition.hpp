#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

using pointer = std::shared_ptr<zakharova::Shape>;
using array = std::unique_ptr<pointer[]>;

namespace zakharova
{
  bool overlap(const rectangle_t &frame1, const rectangle_t &frame2);

  Matrix part(const CompositeShape &compShape);
}

#endif //PARTITION_HPP
