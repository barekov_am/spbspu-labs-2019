#include "circle.hpp"
#include <iostream>
#include <stdexcept>
#define _USE_MATH_DEFINES
#include <math.h>

zakharova::Circle::Circle(double radius, point_t pos) :
  radius_(radius),
  pos_(pos)
{
  if (radius <= 0)
  {
    throw std::invalid_argument("Radius must be greater than zero.");
  }
}

zakharova::rectangle_t zakharova::Circle::getFrameRect() const
{
  return { radius_ * 2, radius_ * 2, pos_ };
}

double zakharova::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

void zakharova::Circle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void zakharova::Circle::move(point_t newpos)
{
  pos_ = newpos;
}

void zakharova::Circle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Scaling factor must be greater than zero.");
  }

  radius_ *= factor;
}

void zakharova::Circle::rotate(double /*angle*/)
{}

void zakharova::Circle::getInfo() const
{
  std::cout << "---CIRCLE---\n";
  std::cout << "radius: " << radius_ << "\n";
  std::cout << "center: (" << pos_.x << ", " << pos_.y << ")\n";
  std::cout << "area: " << getArea() << "\n";
  std::cout << "frame width: " << getFrameRect().width << "\n";
  std::cout << "frame height: " << getFrameRect().height << "\n";
}
