#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "partition.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

using pointer = std::shared_ptr<zakharova::Shape>;

BOOST_AUTO_TEST_SUITE(PartitionTests)

BOOST_AUTO_TEST_CASE(OverlapTest)
{
  zakharova::Circle testCircle1(5, { 0, 0 });
  zakharova::Circle testCircle2(5, { -5, -5 });
  zakharova::Circle testCircle3(4, { 8, 8 });

  pointer circle1 = std::make_shared<zakharova::Circle>(testCircle1);
  pointer circle2 = std::make_shared<zakharova::Circle>(testCircle2);
  pointer circle3 = std::make_shared<zakharova::Circle>(testCircle3);

  BOOST_CHECK(zakharova::overlap(circle1->getFrameRect(), circle2->getFrameRect()));
  BOOST_CHECK(!zakharova::overlap(circle2->getFrameRect(), circle3->getFrameRect()));
}

BOOST_AUTO_TEST_CASE(PartTest)
{
  zakharova::Rectangle testRectangle1(4, 2, { 0, 0 });
  zakharova::Rectangle testRectangle2(4, 6, { 5, 5 });
  zakharova::Circle testCircle1(2, { 5, 5 });
  zakharova::Circle testCircle2(2, { 3, -2 });

  pointer rectangle1 = std::make_shared<zakharova::Rectangle>(testRectangle1);
  pointer rectangle2 = std::make_shared<zakharova::Rectangle>(testRectangle2);
  pointer circle1 = std::make_shared<zakharova::Circle>(testCircle1);
  pointer circle2 = std::make_shared<zakharova::Circle>(testCircle2);

  zakharova::CompositeShape testCompShape;

  testCompShape.add(rectangle1);
  testCompShape.add(rectangle2);
  testCompShape.add(circle1);
  testCompShape.add(circle2);

  zakharova::Matrix testMatrix = zakharova::part(testCompShape);

  BOOST_CHECK(testMatrix[0][0] == rectangle1);
  BOOST_CHECK(testMatrix[0][1] == rectangle2);
  BOOST_CHECK(testMatrix[1][0] == circle1);
  BOOST_CHECK(testMatrix[1][1] == circle2);
}

BOOST_AUTO_TEST_SUITE_END()
