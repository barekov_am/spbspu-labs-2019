#include "partition.hpp"
#define _USE_MATH_DEFINES
#include <math.h>

bool zakharova::overlap(const rectangle_t &frame1, const rectangle_t &frame2)
{
  bool overlapX = fabs(frame1.pos.x - frame2.pos.x) <= (frame1.width / 2 + frame2.width / 2);
  bool overlapY = fabs(frame1.pos.y - frame2.pos.y) <= (frame1.height / 2 + frame2.height / 2);

  return overlapX && overlapY;
}

zakharova::Matrix zakharova::part(const CompositeShape &compShape)
{
  Matrix matrix;

  size_t size = compShape.getSize();
  array shapes = compShape.getShapes();

  for (size_t i = 0; i < size; i++)
  {
    size_t rightRow = 0;
    size_t rightColumn = 0;

    for (size_t j = 0; j < matrix.getRows(); j++)
    {
      for (size_t k = 0; k < matrix.getColumns(); k++)
      {
        if (matrix[j][k] == nullptr)
        {
          rightRow = j;
          rightColumn = k;
          break;
        }

        if (overlap(shapes[i]->getFrameRect(), matrix[j][k]->getFrameRect()))
        {
          rightRow = j + 1;
          rightColumn = 0;
          break;
        }
        else
        {
          rightRow = j;
          rightColumn = k + 1;
        }
      }

      if (rightRow == j)
      {
        break;
      }
    }

    matrix.add(shapes[i], rightRow, rightColumn);
  }

  return matrix;
}
