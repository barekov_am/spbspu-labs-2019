#include "matrix.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdexcept>
#include <iostream>

zakharova::Matrix::Matrix():
  rows_(0),
  columns_(0)
{}

zakharova::Matrix::Matrix(const Matrix &matrix):
  rows_(matrix.rows_),
  columns_(matrix.columns_),
  matrixShapes_(std::make_unique<pointer[]>(matrix.rows_ * matrix.columns_))
{
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    matrixShapes_[i] = matrix.matrixShapes_[i];
  }
}

zakharova::Matrix::Matrix(Matrix &&matrix):
  rows_(matrix.rows_),
  columns_(matrix.columns_),
  matrixShapes_(std::move(matrix.matrixShapes_))
{
  matrix.rows_ = 0;
  matrix.columns_ = 0;
}

zakharova::Matrix &zakharova::Matrix::operator =(const Matrix &matrix)
{
  if (this != &matrix)
  {
    rows_ = matrix.rows_;
    columns_ = matrix.columns_;
    array tmpShapes(std::make_unique<pointer[]>(matrix.rows_ * matrix.columns_));
    for (size_t i = 0; i < (rows_ * columns_); i++)
    {
      tmpShapes[i] = matrix.matrixShapes_[i];
    }
    matrixShapes_.swap(tmpShapes);
  }

  return *this;
}

zakharova::Matrix &zakharova::Matrix::operator =(Matrix &&matrix)
{
  if (this != &matrix)
  {
    rows_ = matrix.rows_;
    columns_ = matrix.columns_;
    matrixShapes_ = std::move(matrix.matrixShapes_);
    matrix.rows_ = 0;
    matrix.columns_ = 0;
  }

  return *this;
}

bool zakharova::Matrix::operator ==(const Matrix &matrix) const
{
  if ((rows_ != matrix.rows_) || (columns_ != matrix.columns_))
  {
    return false;
  }

  for (size_t i = 0; i < rows_ * columns_; i++)
  {
    if (matrixShapes_[i] != matrix.matrixShapes_[i])
    {
      return false;
    }
  }

  return true;
}

bool zakharova::Matrix::operator !=(const Matrix &matrix) const
{
  if (this == &matrix)
  {
    return false;
  }

  return true;
}

zakharova::Matrix::pointer *zakharova::Matrix::operator [](size_t row) const
{
  if (row >= rows_)
  {
    throw std::out_of_range("There is no such row index.");
  }

  return &matrixShapes_[row * columns_];
}

void zakharova::Matrix::add(pointer shape, size_t row, size_t column)
{
  size_t tmpRows = (row == rows_) ? (rows_ + 1) : (rows_);
  size_t tmpColumns = (column == columns_) ? (columns_ + 1) : (columns_);

  array tmpMatrix(std::make_unique<pointer[]>(tmpRows * tmpColumns));

  for (size_t i = 0; i < tmpRows; i++)
  {
    for (size_t j = 0; j < tmpColumns; j++)
    {
      if ((rows_ == i) || (columns_ == j))
      {
        tmpMatrix[i * tmpColumns + j] = nullptr;
      }
      else
      {
        tmpMatrix[i * tmpColumns + j] = matrixShapes_[i * columns_ + j];
      }
    }
  }
  tmpMatrix[row * tmpColumns + column] = shape;
  matrixShapes_.swap(tmpMatrix);
  rows_ = tmpRows;
  columns_ = tmpColumns;
}

size_t zakharova::Matrix::getRows() const
{
  return rows_;
}

size_t zakharova::Matrix::getColumns() const
{
  return columns_;
}

void zakharova::Matrix::printInfo() const
{
  std::cout << "---MATRIX---" << std::endl;
  std::cout << "rows: " << rows_ << std::endl;
  std::cout << "columns: " << columns_ << std::endl;
  
  for (size_t i = 0; i < rows_; i++)
  {
    for (size_t j = 0; j < columns_; j++)
    {
      if (matrixShapes_[i * columns_ + j] != nullptr)
      {
        std::cout << "layer: " << i << " position: " << j << std::endl;
        matrixShapes_[i * columns_ + j]->getInfo();
      }
      else
      {
        std::cout << "layer: " << i << " position: " << j << std::endl;
        std::cout << "NULLPTR" << std::endl;
      }
    }
  }
}
