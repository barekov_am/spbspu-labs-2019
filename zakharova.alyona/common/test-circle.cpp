#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"

const double TOLERANCE = 0.001;

BOOST_AUTO_TEST_SUITE(CircleTests)

BOOST_AUTO_TEST_CASE(IncorrectRadiusTest)
{
  BOOST_CHECK_THROW(zakharova::Circle(-5, { 6, 7 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(InvariabilityAfterAxialMovingTest)
{
  zakharova::Circle testCircle(5, {6, 7});
  zakharova::rectangle_t frameBefore = testCircle.getFrameRect();
  double areaBefore = testCircle.getArea();

  testCircle.move(5, 5);
  zakharova::rectangle_t frameAfter = testCircle.getFrameRect();
  double areaAfter = testCircle.getArea();

  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, TOLERANCE);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(InvariabilityAfterMovingCenterTest)
{
  zakharova::Circle testCircle(5, {6, 7});
  zakharova::rectangle_t frameBefore = testCircle.getFrameRect();
  double areaBefore = testCircle.getArea();

  testCircle.move({0, 0});
  zakharova::rectangle_t frameAfter = testCircle.getFrameRect();
  double areaAfter = testCircle.getArea();

  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, TOLERANCE);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(ScalingTest)
{
  zakharova::Circle testCircle(5, {6, 7});
  double areaBefore = testCircle.getArea();
  zakharova::point_t centerBefore = testCircle.getFrameRect().pos;

  double factor = 1.5;
  testCircle.scale(factor);

  double areaAfter = testCircle.getArea();
  zakharova::point_t centerAfter = testCircle.getFrameRect().pos;

  BOOST_CHECK_CLOSE(areaBefore * factor * factor, areaAfter, TOLERANCE);
  BOOST_CHECK_CLOSE(centerBefore.x, centerAfter.x, TOLERANCE);
  BOOST_CHECK_CLOSE(centerBefore.y, centerAfter.y, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(IncorrectScaleArgumentTest)
{
  zakharova::Circle testCircle(5, {6, 7});

  BOOST_CHECK_THROW(testCircle.scale(-1.5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(CenterInvariabilityAfterRotationTest)
{
  zakharova::Circle testCircle(5, { 6, 7 });
  zakharova::point_t centerBefore = testCircle.getFrameRect().pos;

  testCircle.rotate(30);
  zakharova::point_t centerAfter = testCircle.getFrameRect().pos;

  BOOST_CHECK_CLOSE(centerBefore.x, centerAfter.x, TOLERANCE);
  BOOST_CHECK_CLOSE(centerBefore.y, centerAfter.y, TOLERANCE);
}

BOOST_AUTO_TEST_SUITE_END()
