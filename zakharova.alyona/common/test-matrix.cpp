#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "matrix.hpp"
#include "partition.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

using pointer = std::shared_ptr<zakharova::Shape>;


BOOST_AUTO_TEST_SUITE(MatrixTests)

BOOST_AUTO_TEST_CASE(CopyingAndMovingCtorsTest)
{
  zakharova::Rectangle testRectangle(4, 2, { 0, 0 });
  zakharova::Circle testCircle(2, { 5, 5 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  zakharova::Matrix testMatrix = part(testCompShape);
  zakharova::Matrix testMatrix2(testMatrix);

  BOOST_CHECK(testMatrix2 == testMatrix);

  zakharova::Matrix testMatrix3(std::move(testMatrix));

  BOOST_CHECK(testMatrix3 == testMatrix2);
}

BOOST_AUTO_TEST_CASE(CopyingAndMovingOperatorsTest)
{
  zakharova::Rectangle testRectangle(4, 2, { 0, 0 });
  zakharova::Circle testCircle(2, { 5, 5 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  zakharova::Matrix testMatrix = part(testCompShape);
  zakharova::Matrix testMatrix2 = testMatrix;

  BOOST_CHECK(testMatrix2 == testMatrix);

  zakharova::Matrix testMatrix3 = std::move(testMatrix);

  BOOST_CHECK(testMatrix3 == testMatrix2);
}

BOOST_AUTO_TEST_CASE(NumberOfRowsTest)
{
  zakharova::Rectangle testRectangle(4, 2, { 0, 0 });
  zakharova::Circle testCircle(2, { 3, -2 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  zakharova::Matrix testMatrix = part(testCompShape);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 2);
}

BOOST_AUTO_TEST_CASE(NumberOfColumnsTest)
{
  zakharova::Rectangle testRectangle(4, 2, { 0, 0 });
  zakharova::Circle testCircle(2, { 3, -2 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  zakharova::Matrix testMatrix = part(testCompShape);

  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 1);
}

BOOST_AUTO_TEST_CASE(OperatorExceptionTest)
{
  zakharova::Rectangle testRectangle(4, 2, { 0, 0 });
  zakharova::Circle testCircle(2, { 3, -2 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  zakharova::Matrix testMatrix = part(testCompShape);

  BOOST_CHECK_THROW(testMatrix[2], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(ComparisonOperatorsTest)
{
  zakharova::Rectangle testRectangle(4, 2, { 0, 0 });
  zakharova::Circle testCircle(2, { 3, -2 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  zakharova::CompositeShape testCompShape2(testCompShape);

  zakharova::Matrix testMatrix = zakharova::part(testCompShape);
  zakharova::Matrix testMatrix2 = zakharova::part(testCompShape2);
  zakharova::Matrix testMatrix3;

  BOOST_CHECK(testMatrix == testMatrix2);
  BOOST_CHECK(testMatrix != testMatrix3);
}

BOOST_AUTO_TEST_SUITE_END()
