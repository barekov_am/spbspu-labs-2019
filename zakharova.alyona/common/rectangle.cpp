#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#define _USE_MATH_DEFINES
#include <math.h>

zakharova::Rectangle::Rectangle(double width, double height, point_t pos) :
  width_(width),
  height_(height),
  pos_(pos),
  angle_(0)
{
  if ((width <= 0) || (height <= 0))
  {
    throw std::invalid_argument("Width and height must be greater than zero.");
  }
}

double zakharova::Rectangle::getArea() const
{
  return width_ * height_;
}

zakharova::rectangle_t zakharova::Rectangle::getFrameRect() const
{
  double cosA = cos(angle_ * M_PI / 180);
  double sinA = sin(angle_ * M_PI / 180);

  double width = width_ * fabs(cosA) + height_ * fabs(sinA);
  double height = width_ * fabs(sinA) + height_ * fabs(cosA);

  return { width, height, pos_ };
}

void zakharova::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void zakharova::Rectangle::move(point_t newpos)
{
  pos_ = newpos;
}

void zakharova::Rectangle::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Scaling factor must be greater than zero.");
  }

  width_ *= factor;
  height_ *= factor;
}

void zakharova::Rectangle::rotate(double angle)
{
  angle_ += angle;
}

void zakharova::Rectangle::getInfo() const
{
  std::cout << "---RECTANGLE---\n";
  std::cout << "width: " << width_ << "\n";
  std::cout << "height: " << height_ << "\n";
  std::cout << "center: (" << pos_.x << ", " << pos_.y << ")\n";
  std::cout << "area: " << getArea() << "\n";
  std::cout << "angle: " << angle_ << "\n";
  std::cout << "frame width: " << getFrameRect().width << "\n";
  std::cout << "frame height: " << getFrameRect().height << "\n";
}
