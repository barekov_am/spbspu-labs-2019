#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace zakharova
{
  class CompositeShape : public Shape
  {
  public:

    using pointer = std::shared_ptr<Shape>;
    using array = std::unique_ptr<pointer[]>;

    CompositeShape();
    CompositeShape(const CompositeShape &copyingCompShape);
    CompositeShape(CompositeShape &&movingCompShape);
    ~CompositeShape() = default;

    CompositeShape &operator =(const CompositeShape &copyingCompShape);
    CompositeShape &operator =(CompositeShape &&movingCompShape);
    pointer operator [](size_t index) const;

    void add(pointer shape);
    void remove(size_t index);
    void remove(pointer shape);
    void clear();

    double getArea() const override;
    rectangle_t getFrameRect() const override;

    void move(double dx, double dy) override;
    void move(point_t newpos) override;
    void scale(double factor) override;
    void rotate(double angle) override;

    array getShapes() const;
    size_t getSize() const;
    void getInfo() const override;

  private:
    array shapes_;
    size_t size_;
  };
}

#endif // COMPOSITESHAPE_HPP
