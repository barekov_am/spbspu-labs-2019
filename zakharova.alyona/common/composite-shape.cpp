#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>
#define _USE_MATH_DEFINES
#include <math.h>

zakharova::CompositeShape::CompositeShape():
  shapes_(nullptr),
  size_(0)
{}

zakharova::CompositeShape::CompositeShape(const CompositeShape &copyingCompShape):
  shapes_(std::make_unique<pointer[]>(copyingCompShape.size_)),
  size_(copyingCompShape.size_)
{
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i] = copyingCompShape.shapes_[i];
  }
}

zakharova::CompositeShape::CompositeShape(CompositeShape &&movingCompShape):
  shapes_(std::move(movingCompShape.shapes_)),
  size_(movingCompShape.size_)
{
  movingCompShape.size_ = 0;
}

zakharova::CompositeShape &zakharova::CompositeShape::operator =(const CompositeShape &copyingCompShape)
{
  if (this != &copyingCompShape)
  {
    size_ = copyingCompShape.size_;
    array tmpShapes(std::make_unique<pointer[]>(copyingCompShape.size_));
    for (size_t i = 0; i < size_; i++)
    {
      tmpShapes[i] = copyingCompShape.shapes_[i];
    }
    shapes_.swap(tmpShapes);
  }

  return *this;
}

zakharova::CompositeShape &zakharova::CompositeShape::operator =(CompositeShape &&movingCompShape)
{
  if (this != &movingCompShape)
  {
    size_ = movingCompShape.size_;
    shapes_ = std::move(movingCompShape.shapes_);
    movingCompShape.size_ = 0;
  }
  return *this;
}

zakharova::CompositeShape::pointer zakharova::CompositeShape::operator [](size_t index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("There is no such index");
  }

  return shapes_[index];
}

void zakharova::CompositeShape::add(pointer shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Null pointer cannot be the argument.");
  }

  array tmpShapes = std::make_unique<pointer[]>(size_ + 1);
  for (size_t i = 0; i < size_; i++)
  {
    tmpShapes[i] = shapes_[i];
  }
  tmpShapes[size_] = shape;
  size_++;
  shapes_.swap(tmpShapes);
}

void zakharova::CompositeShape::remove(size_t index)
{
  if (index >= size_)
  {
    throw std::out_of_range("There is no such shape index.");
  }

  size_--;
  array tmpShapes = std::make_unique<pointer[]>(size_);
  for (size_t i = 0; i < index; i++)
  {
    tmpShapes[i] = shapes_[i];
  }
  for (size_t i = index; i < size_; i++)
  {
    tmpShapes[i] = shapes_[i + 1];
  }
  shapes_.swap(tmpShapes);
}

void zakharova::CompositeShape::remove(pointer shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Null pointer cannot be the argument.");
  }

  for (size_t i = 0; i < size_; i++)
  {
    if (shapes_[i] == shape)
    {
      remove(i);
      return;
    }
  }

  throw std::invalid_argument("There is no such shape.");
}

void zakharova::CompositeShape::clear()
{
  shapes_ = nullptr;
  size_ = 0;
}

double zakharova::CompositeShape::getArea() const
{
  double sum = 0;
  for (size_t i = 0; i < size_; i++)
  {
    sum += shapes_[i]->getArea();
  }

  return sum;
}

zakharova::rectangle_t zakharova::CompositeShape::getFrameRect() const
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty.");
  }

  rectangle_t frameRect = shapes_[0]->getFrameRect();
  double minX = frameRect.pos.x - frameRect.width / 2;
  double maxY = frameRect.pos.y + frameRect.height / 2;
  double maxX = frameRect.pos.x + frameRect.width / 2;
  double minY = frameRect.pos.y - frameRect.height / 2;

  for (size_t i = 1; i < size_; i++)
  {
    frameRect = shapes_[i]->getFrameRect();
    minX = std::min(minX, frameRect.pos.x - frameRect.width / 2);
    maxY = std::max(maxY, frameRect.pos.y + frameRect.height / 2);
    maxX = std::max(maxX, frameRect.pos.x + frameRect.width / 2);
    minY = std::min(minY, frameRect.pos.y - frameRect.height / 2);
  }

  return { maxX - minX, maxY - minY, { (minX + maxX) / 2, (minY + maxY) / 2} };
}

void zakharova::CompositeShape::move(double dx, double dy)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty.");
  }

  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void zakharova::CompositeShape::move(point_t newpos)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty.");
  }

  zakharova::rectangle_t frame = getFrameRect();

  double dx = newpos.x - frame.pos.x;
  double dy = newpos.y - frame.pos.y;

  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void zakharova::CompositeShape::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Scaling factor must be greater than zero.");
  }

  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty.");
  }

  point_t center = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i]->scale(factor);
    double dx = shapes_[i]->getFrameRect().pos.x - center.x;
    double dy = shapes_[i]->getFrameRect().pos.y - center.y;
    shapes_[i]->move({ center.x + dx * factor, center.y + dy * factor });
  }
}

void zakharova::CompositeShape::rotate(double angle)
{
  if (size_ == 0)
  {
    throw std::logic_error("Composite shape is empty.");
  }

  point_t center = getFrameRect().pos;
  double cosA = cos(angle * M_PI / 180);
  double sinA = sin(angle * M_PI / 180);

  for (size_t i = 0; i < size_; i++)
  {
    point_t shapeCenter = shapes_[i]->getFrameRect().pos;
   
    double x = shapeCenter.x - center.x;
    double y = shapeCenter.y - center.y;
    double newX = x * fabs(cosA) - y * fabs(sinA);
    double newY = x * fabs(sinA) + y * fabs(cosA);

    shapes_[i]->move({ center.x + newX, center.y + newY });
    shapes_[i]->rotate(angle);
  }
}

zakharova::CompositeShape::array zakharova::CompositeShape::getShapes() const
{
  array tmpShapes(std::make_unique<pointer[]>(size_));
  for (size_t i = 0; i < size_; i++)
  {
    tmpShapes[i] = shapes_[i];
  }

  return tmpShapes;
}

size_t zakharova::CompositeShape::getSize() const
{
  return size_;
}

void zakharova::CompositeShape::getInfo() const
{
  if (size_ == 0)
  {
    std::cout << "The composite shape is empty." << std::endl;
  }
  else
  {
    std::cout << "The composite shape consists of the following shapes:" << std::endl;
    for (size_t i = 0; i < size_; i++)
    {
      std::cout << i << ")" << std::endl;
      shapes_[i]->getInfo();
    }
    std::cout << "-----" << std::endl;
    std::cout << "composite shape area: " << getArea() << "\n";
    std::cout << "frame width: " << getFrameRect().width << "\n";
    std::cout << "frame height: " << getFrameRect().height << "\n";
    std::cout << "center: (" << getFrameRect().pos.x << ", " << getFrameRect().pos.y << ")\n";
    std::cout << "-----" << std::endl;
  }
}
