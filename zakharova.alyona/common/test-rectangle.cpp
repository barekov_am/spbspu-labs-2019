#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"

const double TOLERANCE = 0.001;

BOOST_AUTO_TEST_SUITE(RectangleTests)

BOOST_AUTO_TEST_CASE(InvariabilityAfterAxialMovingTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  zakharova::rectangle_t frameBefore = testRectangle.getFrameRect();
  double areaBefore = testRectangle.getArea();

  testRectangle.move(5, 5);
  zakharova::rectangle_t frameAfter = testRectangle.getFrameRect();
  double areaAfter = testRectangle.getArea();

  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, TOLERANCE);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(InvariabilityAfterMovingCenterTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  zakharova::rectangle_t frameBefore = testRectangle.getFrameRect();
  double areaBefore = testRectangle.getArea();

  testRectangle.move({0, 0});
  zakharova::rectangle_t frameAfter = testRectangle.getFrameRect();
  double areaAfter = testRectangle.getArea();

  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, TOLERANCE);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(ScalingTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  double areaBefore = testRectangle.getArea();
  zakharova::point_t centerBefore = testRectangle.getFrameRect().pos;

  double factor = 1.5;
  testRectangle.scale(factor);

  double areaAfter = testRectangle.getArea();
  zakharova::point_t centerAfter = testRectangle.getFrameRect().pos;

  BOOST_CHECK_CLOSE(areaBefore * factor * factor, areaAfter, TOLERANCE);
  BOOST_CHECK_CLOSE(centerBefore.x, centerAfter.x, TOLERANCE);
  BOOST_CHECK_CLOSE(centerBefore.y, centerAfter.y, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(IncorrectWidthTest)
{
  BOOST_CHECK_THROW(zakharova::Rectangle(-5, 6, { 7, 8 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(IncorrectHeightTest)
{
  BOOST_CHECK_THROW(zakharova::Rectangle(5, -6, { 7, 8 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(IncorrectScaleArgumentTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });

  BOOST_CHECK_THROW(testRectangle.scale(-1.5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(CenterInvariabilityAfterRotationTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  zakharova::point_t centerBefore = testRectangle.getFrameRect().pos;

  testRectangle.rotate(30);
  zakharova::point_t centerAfter = testRectangle.getFrameRect().pos;

  BOOST_CHECK_CLOSE(centerBefore.x, centerAfter.x, TOLERANCE);
  BOOST_CHECK_CLOSE(centerBefore.y, centerAfter.y, TOLERANCE);
}

BOOST_AUTO_TEST_SUITE_END()
