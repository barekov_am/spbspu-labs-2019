#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"

namespace zakharova
{
  class Matrix
  {
  public:

    using pointer = std::shared_ptr<Shape>;
    using array = std::unique_ptr<pointer[]>;

    Matrix();
    Matrix(const Matrix &matrix);
    Matrix(Matrix &&matrix);
    ~Matrix() = default;
    
    Matrix &operator =(const Matrix &matrix);
    Matrix &operator =(Matrix &&matrix);

    bool operator ==(const Matrix &matrix) const;
    bool operator !=(const Matrix &matrix) const;

    pointer *operator [](size_t row) const;

    void add(pointer shape, size_t row, size_t column);
    
    size_t getRows() const;
    size_t getColumns() const;
    void printInfo() const;

  private:
    size_t rows_;
    size_t columns_;
    array matrixShapes_;
  };
}

#endif // MATRIX_HPP
