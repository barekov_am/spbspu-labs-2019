#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

const double TOLERANCE = 0.001;

using pointer = std::shared_ptr<zakharova::Shape>;


BOOST_AUTO_TEST_SUITE(CompositeShapeTests)


BOOST_AUTO_TEST_CASE(InvariabilityAfterAxialMovingTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  zakharova::Circle testCircle(9, { 10, 11 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  zakharova::rectangle_t frameBefore = testCompShape.getFrameRect();
  double areaBefore = testCompShape.getArea();

  testCompShape.move(5, 5);
  zakharova::rectangle_t frameAfter = testCompShape.getFrameRect();
  double areaAfter = testCompShape.getArea();

  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, TOLERANCE);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, TOLERANCE);
}


BOOST_AUTO_TEST_CASE(InvariabilityAfterMovingCenterTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  zakharova::Circle testCircle(9, { 10, 11 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  zakharova::rectangle_t frameBefore = testCompShape.getFrameRect();
  double areaBefore = testCompShape.getArea();

  testCompShape.move({ 0, 0 });
  zakharova::rectangle_t frameAfter = testCompShape.getFrameRect();
  double areaAfter = testCompShape.getArea();

  BOOST_CHECK_CLOSE(frameBefore.width, frameAfter.width, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.height, frameAfter.height, TOLERANCE);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, TOLERANCE);
}


BOOST_AUTO_TEST_CASE(ScalingTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  zakharova::Circle testCircle(9, { 10, 11 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  double areaBefore = testCompShape.getArea();
  zakharova::rectangle_t frameBefore = testCompShape.getFrameRect();

  double factor = 1.5;
  testCompShape.scale(factor);
  double areaAfter = testCompShape.getArea();
  zakharova::rectangle_t frameAfter = testCompShape.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore * factor * factor, areaAfter, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.width * factor, frameAfter.width, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.height * factor, frameAfter.height, TOLERANCE);
}


BOOST_AUTO_TEST_CASE(DecreasingScalingTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  zakharova::Circle testCircle(9, { 10, 11 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  double areaBefore = testCompShape.getArea();
  zakharova::rectangle_t frameBefore = testCompShape.getFrameRect();

  double factor = 0.5;
  testCompShape.scale(factor);
  double areaAfter = testCompShape.getArea();
  zakharova::rectangle_t frameAfter = testCompShape.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore * factor * factor, areaAfter, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.width * factor, frameAfter.width, TOLERANCE);
  BOOST_CHECK_CLOSE(frameBefore.height * factor, frameAfter.height, TOLERANCE);
}


BOOST_AUTO_TEST_CASE(CenterInvariabilityAfterScalingTest)
{
  zakharova::Rectangle testRectangle(5, 50, { 7, 8 });
  zakharova::Circle testCircle(20, { 100, 11 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  zakharova::point_t centerBefore = testCompShape.getFrameRect().pos;
  testCompShape.scale(2);
  zakharova::point_t centerAfter = testCompShape.getFrameRect().pos;

  BOOST_CHECK_CLOSE(centerBefore.x, centerAfter.x, TOLERANCE);
  BOOST_CHECK_CLOSE(centerBefore.y, centerAfter.y, TOLERANCE);
}


BOOST_AUTO_TEST_CASE(IncorrectScaleArgumentTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  zakharova::Circle testCircle(9, { 10, 11 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  BOOST_CHECK_THROW(testCompShape.scale(-1.5), std::invalid_argument);
}


BOOST_AUTO_TEST_CASE(CopyingCtorTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  zakharova::Circle testCircle(9, { 10, 11 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  zakharova::CompositeShape testCompShape2(testCompShape);

  BOOST_CHECK_CLOSE(testCompShape.getArea(), testCompShape2.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(testCompShape.getFrameRect().width, testCompShape2.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(testCompShape.getFrameRect().height, testCompShape2.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE(testCompShape.getFrameRect().pos.x, testCompShape2.getFrameRect().pos.x, TOLERANCE);
  BOOST_CHECK_CLOSE(testCompShape.getFrameRect().pos.y, testCompShape2.getFrameRect().pos.y, TOLERANCE);
}


BOOST_AUTO_TEST_CASE(MovingCtorTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  zakharova::Circle testCircle(9, { 10, 11 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  zakharova::rectangle_t frame = testCompShape.getFrameRect();
  double area = testCompShape.getArea();

  zakharova::CompositeShape testCompShape2(std::move(testCompShape));

  BOOST_CHECK_CLOSE(area, testCompShape2.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(frame.width, testCompShape2.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(frame.height, testCompShape2.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE(frame.pos.x, testCompShape2.getFrameRect().pos.x, TOLERANCE);
  BOOST_CHECK_CLOSE(frame.pos.y, testCompShape2.getFrameRect().pos.y, TOLERANCE);
}


BOOST_AUTO_TEST_CASE(CopyingOperatorTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  zakharova::Circle testCircle(9, { 10, 11 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  zakharova::CompositeShape testCompShape2;
  testCompShape2 = testCompShape;

  BOOST_CHECK_CLOSE(testCompShape.getArea(), testCompShape2.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(testCompShape.getFrameRect().width, testCompShape2.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(testCompShape.getFrameRect().height, testCompShape2.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE(testCompShape.getFrameRect().pos.x, testCompShape2.getFrameRect().pos.x, TOLERANCE);
  BOOST_CHECK_CLOSE(testCompShape.getFrameRect().pos.y, testCompShape2.getFrameRect().pos.y, TOLERANCE);
}


BOOST_AUTO_TEST_CASE(MovingOperatorTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  zakharova::Circle testCircle(9, { 10, 11 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  zakharova::rectangle_t frame = testCompShape.getFrameRect();
  double area = testCompShape.getArea();

  zakharova::CompositeShape testCompShape2;
  testCompShape2 = std::move(testCompShape);

  BOOST_CHECK_CLOSE(area, testCompShape2.getArea(), TOLERANCE);
  BOOST_CHECK_CLOSE(frame.width, testCompShape2.getFrameRect().width, TOLERANCE);
  BOOST_CHECK_CLOSE(frame.height, testCompShape2.getFrameRect().height, TOLERANCE);
  BOOST_CHECK_CLOSE(frame.pos.x, testCompShape2.getFrameRect().pos.x, TOLERANCE);
  BOOST_CHECK_CLOSE(frame.pos.y, testCompShape2.getFrameRect().pos.y, TOLERANCE);
}


BOOST_AUTO_TEST_CASE(IncorrectAddShapeArgumentTest)
{
  zakharova::CompositeShape testCompShape;

  BOOST_CHECK_THROW(testCompShape.add(nullptr), std::invalid_argument);
}


BOOST_AUTO_TEST_CASE(RemoveOutOfRangeIndexTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  zakharova::Circle testCircle(9, { 10, 11 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  BOOST_CHECK_THROW(testCompShape.remove(3), std::out_of_range);
}


BOOST_AUTO_TEST_CASE(SizeChangingTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  zakharova::Circle testCircle(9, { 10, 11 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  BOOST_CHECK_EQUAL(testCompShape.getSize(), 0);

  testCompShape.add(rectangle);
  testCompShape.add(circle);
  BOOST_CHECK_EQUAL(testCompShape.getSize(), 2);

  testCompShape.remove(circle);
  BOOST_CHECK_EQUAL(testCompShape.getSize(), 1);

  testCompShape.remove(0);
  BOOST_CHECK_EQUAL(testCompShape.getSize(), 0);
}


BOOST_AUTO_TEST_CASE(CenterInvariabilityAfterRotationTest)
{
  zakharova::Rectangle testRectangle(5, 6, { 7, 8 });
  zakharova::Circle testCircle(9, { 10, 11 });

  pointer rectangle = std::make_shared<zakharova::Rectangle>(testRectangle);
  pointer circle = std::make_shared<zakharova::Circle>(testCircle);

  zakharova::CompositeShape testCompShape;
  testCompShape.add(rectangle);
  testCompShape.add(circle);

  zakharova::point_t centerBefore = testCompShape.getFrameRect().pos;

  testCompShape.rotate(30);
  zakharova::point_t centerAfter = testCompShape.getFrameRect().pos;

  BOOST_CHECK_CLOSE(centerBefore.x, centerAfter.x, TOLERANCE);
  BOOST_CHECK_CLOSE(centerBefore.x, centerAfter.x, TOLERANCE);
}


BOOST_AUTO_TEST_SUITE_END()
