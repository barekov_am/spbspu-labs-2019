#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "partition.hpp"
#include "matrix.hpp"

using pointer = std::shared_ptr<zakharova::Shape>;

int main()
{
  zakharova::Rectangle rectangle1(4, 2, { 0, 0 });
  zakharova::Rectangle rectangle2(4, 6, { 5, 5 });
  zakharova::Circle circle1(2, { 5, 5 });
  zakharova::Circle circle2(2, { 3, -2 });
  zakharova::Rectangle rectangle3(20, 20, { 4, -2 });
  zakharova::Circle circle3(1, { 30, 30 });

  pointer rectanglePtr1 = std::make_shared<zakharova::Rectangle>(rectangle1);
  pointer rectanglePtr2 = std::make_shared<zakharova::Rectangle>(rectangle2);
  pointer circlePtr1 = std::make_shared<zakharova::Circle>(circle1);
  pointer circlePtr2 = std::make_shared<zakharova::Circle>(circle2);
  pointer rectanglePtr3 = std::make_shared<zakharova::Rectangle>(rectangle3);
  pointer circlePtr3 = std::make_shared<zakharova::Circle>(circle3);

  zakharova::CompositeShape compShape;
  compShape.add(rectanglePtr1);
  compShape.add(rectanglePtr2);
  compShape.add(circlePtr1);
  compShape.add(circlePtr2);
  compShape.add(rectanglePtr3);
  compShape.add(circlePtr3);
  compShape.getInfo();

  zakharova::Matrix matrix = zakharova::part(compShape);
  matrix.printInfo();

  return 0;
}
