#include <iostream>

void task();

int main()
{
  try
  {
    task();
  }
  catch (std::exception& e)
  {
    std::cout << e.what();
    return 1;
  }

  return 0;
}
