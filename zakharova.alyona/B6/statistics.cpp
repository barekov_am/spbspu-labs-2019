#include "statistics.hpp"
#include <iostream>

Statistics::Statistics() :
  total_(0),
  first_(0),
  max_(0),
  min_(0),
  positive_(0),
  negative_(0),
  odd_(0),
  even_(0),
  equal_(false)
{}

void Statistics::operator()(int value)
{
  if (total_ == 0)
  {
    first_ = value;
    max_ = value;
    min_ = value;
  }
  else if (value > max_)
  {
    max_ = value;
  }
  else if (value < min_)
  {
    min_ = value;
  }

  total_++;

  if (value > 0)
  {
    positive_++;
  }
  else if (value < 0)
  {
    negative_++;
  }

  value % 2 == 0 ? even_ += value : odd_ += value;

  first_ == value ? equal_ = true : equal_ = false;
}

void Statistics::printResults() const
{ 
  if (total_ == 0)
  {
    std::cout << "No Data\n";
  }
  else
  {
    std::cout << "Max: " << max_ << "\n";
    std::cout << "Min: " << min_ << "\n";
    std::cout << "Mean: " << static_cast<double> (odd_ + even_) / total_ << "\n";
    std::cout << "Positive: " << positive_ << "\n";
    std::cout << "Negative: " << negative_ << "\n";
    std::cout << "Odd Sum: " << odd_ << "\n";
    std::cout << "Even Sum: " << even_ << "\n";
    std::cout << "First/Last Equal: ";
    equal_ == true ? std::cout << "yes\n" : std::cout << "no\n";
  }
}
