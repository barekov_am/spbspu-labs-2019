#ifndef STATISTICS_HPP
#define STATISTICS_HPP

class Statistics
{
public:
  Statistics();

  void operator()(int value);

  void printResults() const;

private:
  long int total_;
  int first_;
  int max_;
  int min_;
  long int positive_;
  long int negative_;
  long long int odd_;
  long long int even_;
  bool equal_;
};

#endif // !STATISTICS_HPP
