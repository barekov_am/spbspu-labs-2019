#include <iostream>
#include <algorithm>
#include <iterator>
#include <stdexcept>
#include "statistics.hpp"

void task()
{
  Statistics statistics;
  statistics = std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), Statistics());

  if (!std::cin.eof())
  {
    throw std::invalid_argument("Invalid input");
  }

  statistics.printResults();
}
