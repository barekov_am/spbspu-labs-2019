#include <iostream>
#include <string>

void task1();
void task2();

int main(int argc, char* argv[])
{
  try
  {
    if ((argc < 2) || (argc > 3))
    {
      std::cerr << "Invalid number of arguments";
      return 1;
    }

    int task = std::stoi(argv[1]);

    switch (task)
    {
    case 1:
      task1();
      break;
    case 2:
      task2();
      break;
    default:
      std::cerr << "Invalid argument";
      return 1;
    }
  }
  catch (std::invalid_argument& e)
  {
    std::cout << e.what();
    return 1;
  }
  catch (std::exception& e)
  {
    std::cout << e.what();
    return 2;
  }

  return 0;
}
