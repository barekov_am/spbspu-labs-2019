#include "shape.hpp"
#include <cmath>

const double EPSILON = 0.00001;

std::ostream& operator<<(std::ostream& cout, const Shape& shape)
{
  cout << shape.size() << " ";
  for (size_t i = 0; i < shape.size(); ++i)
  {
    cout << '(' << shape[i].x << ';' << shape[i].y << ')' << ' ';
  }

  return cout;
}

bool isSquare(const Shape& shape)
{
  double side1, side2, side3, side4, diag1, diag2;

  diag1 = sqrt((shape[2].x - shape[0].x) * (shape[2].x - shape[0].x) + (shape[2].y - shape[0].y) * (shape[2].y - shape[0].y));
  diag2 = sqrt((shape[3].x - shape[1].x) * (shape[3].x - shape[1].x) + (shape[3].y - shape[1].y) * (shape[3].y - shape[1].y));
  if (abs(diag1 - diag2) > EPSILON)
  {
    return false;
  }

  side1 = sqrt((shape[1].x - shape[0].x) * (shape[1].x - shape[0].x) + (shape[1].y - shape[0].y) * (shape[1].y - shape[0].y));
  side2 = sqrt((shape[2].x - shape[1].x) * (shape[2].x - shape[1].x) + (shape[2].y - shape[1].y) * (shape[2].y - shape[1].y));
  if (abs(side1 - side2) > EPSILON)
  {
    return false;
  }

  side3 = sqrt((shape[3].x - shape[2].x) * (shape[3].x - shape[2].x) + (shape[3].y - shape[2].y) * (shape[3].y - shape[2].y));
  if (abs(side1 - side3) > EPSILON)
  {
    return false;
  }

  side4 = sqrt((shape[0].x - shape[3].x) * (shape[0].x - shape[3].x) + (shape[0].y - shape[3].y) * (shape[0].y - shape[3].y));
  if (abs(side1 - side4) > EPSILON)
  {
    return false;
  }

  return true;
}

bool isRectangle(const Shape& shape)
{
  double side1, side2, side3, side4, diag1, diag2;

  diag1 = sqrt((shape[2].x - shape[0].x) * (shape[2].x - shape[0].x) + (shape[2].y - shape[0].y) * (shape[2].y - shape[0].y));
  diag2 = sqrt((shape[3].x - shape[1].x) * (shape[3].x - shape[1].x) + (shape[3].y - shape[1].y) * (shape[3].y - shape[1].y));
  if (abs(diag1 - diag2) > EPSILON)
  {
    return false;
  }

  side1 = sqrt((shape[1].x - shape[0].x) * (shape[1].x - shape[0].x) + (shape[1].y - shape[0].y) * (shape[1].y - shape[0].y));
  side3 = sqrt((shape[3].x - shape[2].x) * (shape[3].x - shape[2].x) + (shape[3].y - shape[2].y) * (shape[3].y - shape[2].y));
  if (abs(side1 - side3) > EPSILON)
  {
    return false;
  }

  side2 = sqrt((shape[2].x - shape[1].x) * (shape[2].x - shape[1].x) + (shape[2].y - shape[1].y) * (shape[2].y - shape[1].y));
  side4 = sqrt((shape[0].x - shape[3].x) * (shape[0].x - shape[3].x) + (shape[0].y - shape[3].y) * (shape[0].y - shape[3].y));
  if (abs(side2 - side4) > EPSILON)
  {
    return false;
  }

  return true;
}
