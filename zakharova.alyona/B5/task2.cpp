#include <vector>
#include <list>
#include <iostream>
#include <string>
#include <sstream>
#include <cctype>
#include <stdexcept>
#include <iterator>
#include <algorithm>

#include "shape.hpp"

void task2()
{
  std::string line;
  size_t numberOfVertices;
  int x = 0;
  int y = 0;
  char ch;

  std::list<Shape> list;

  while (std::getline(std::cin, line))
  {
    std::stringstream stream(line);
    stream >> std::ws;
    if (!stream.eof())
    {
      Shape pointsVector;

      stream >> numberOfVertices;

      for (size_t i = 0; i < numberOfVertices; ++i)
      {
        stream >> std::ws;
        if (stream.eof())
        {
          throw std::invalid_argument("Invalid input");
        }
        stream.get(ch);
        if (ch == '(')
        {
          stream >> x;
          stream >> ch;
          if (ch == ';')
          {
            stream >> y;
            stream >> ch;
            if (ch != ')')
            {
              throw std::invalid_argument("Invalid input");
            }
          }
          else
          {
            throw std::invalid_argument("Invalid input");
          }
        }

        pointsVector.push_back({ x, y });
      }

      stream >> std::ws;
      if (!stream.eof())
      {
        throw std::invalid_argument("Invalid input");
      }

      list.push_back(pointsVector);
    }
  }

  numberOfVertices = 0;
  size_t triangles = 0;
  size_t squares = 0;
  size_t rectangles = 0;

  std::vector<Point> points;

  std::list<Shape> triangleContainer;
  std::list<Shape> squareContainer;
  std::list<Shape> rectangleContainer;
  std::list<Shape> polygonContainer;

  auto iter = list.begin();
  
  while (iter != list.end())
  {
    Shape& shape = *iter;
    numberOfVertices += shape.size();
    switch (shape.size())
    {
    case 3:
      points.push_back(shape.front());
      triangleContainer.push_back(shape);
      triangles++;
      iter++;
      break;

    case 4:
      points.push_back(shape.front());
      if (isSquare(shape))
      {
        squareContainer.push_back(shape);
        squares++;
        rectangles++;
      }
      else if (isRectangle(shape))
      {
        rectangleContainer.push_back(shape);
        rectangles++;
      }
      else
      {
        polygonContainer.push_back(shape);
      }
      iter++;
      break;

    case 5:
      iter = list.erase(iter);
      break;

    default:
      points.push_back(shape.front());
      polygonContainer.push_back(shape);
      iter++;
    }
  }

  list.clear();
  list.splice(list.end(), triangleContainer);
  list.splice(list.end(), squareContainer);
  list.splice(list.end(), rectangleContainer);
  list.splice(list.end(), polygonContainer);

  std::cout << "Vertices: " << numberOfVertices << "\n";
  std::cout << "Triangles: " << triangles << "\n";
  std::cout << "Squares: " << squares << "\n";
  std::cout << "Rectangles: " << rectangles << "\n";
  std::cout << "Points: ";
  for (size_t i = 0; i < points.size(); ++i)
  {
    std::cout << '(' << points[i].x << ';' << points[i].y << ')' << " ";
  }
  std::cout << "\nShapes:\n";
  std::copy(list.begin(), list.end(), std::ostream_iterator<Shape>(std::cout, "\n"));
}
