#include <string>
#include <sstream>
#include <cctype>
#include <set>
#include <iterator>
#include <iostream>
#include <algorithm>

void task1()
{
  std::set<std::string> set;
  std::string line;
  std::string word;

  while (std::getline(std::cin, line))
  {
    std::stringstream stream(line);
    while (stream >> word)
    {
      if (set.find(word) == set.end())
      {
        set.insert(word);
      }
    }
  }

  for (auto iter = set.begin(); iter != set.end(); iter++)
  {
    std::cout << *iter << "\n";
  }
}
