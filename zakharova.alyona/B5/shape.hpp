#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>
#include <vector>

struct Point
{
  int x, y;
};

using Shape = std::vector<Point>;

bool isSquare(const Shape& shape);
bool isRectangle(const Shape& shape);

std::ostream& operator<<(std::ostream& cout, const Shape& shape);

#endif
