#include <cassert>
#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

using pointer = std::shared_ptr<zakharova::Shape>;

void demo(zakharova::Shape *figure)
{
  assert(figure != nullptr);
  figure->getInfo();
  figure->move(10, 15);
  std::cout << "* * * Move 10 by Ox, 15 by Oy * * *" << std::endl;
  figure->getInfo();
  figure->move({ 0, 0 });
  std::cout << "* * * Move to (0,0) * * *" << std::endl;
  figure->getInfo();
  figure->scale(2);
  std::cout << "* * * Scale 2 * * *" << std::endl;
  figure->getInfo();
  std::cout << "* * *" << std::endl;
}

int main()
{
  zakharova::Rectangle rectangle(15, 10, { 5, 5 });
  zakharova::Circle circle(5, { 10, 10 });
  pointer rectanglePtr = std::make_shared<zakharova::Rectangle>(rectangle);
  pointer circlePtr = std::make_shared<zakharova::Circle>(circle);

  zakharova::CompositeShape compShape;
  compShape.add(rectanglePtr);
  compShape.add(circlePtr);
  compShape.getInfo();

  zakharova::Rectangle rectangle2(6, 9, { 2, 0 });
  pointer rectanglePtr2 = std::make_shared<zakharova::Rectangle>(rectangle2);

  compShape.add(rectanglePtr2);
  std::cout << "* * * One more shape was added to the composite shape* * *" << std::endl;
  compShape.getInfo();

  compShape.remove(1);
  std::cout << "* * * The second shape was removed from the composite shape * * *" << std::endl;
  compShape.getInfo();

  demo(&compShape);

  std::cout << "Copying:" << std::endl;
  zakharova::CompositeShape compShape2(compShape);
  compShape2.getInfo();
  std::cout << "Moving:" << std::endl;
  zakharova::CompositeShape compShape3(std::move(compShape2));
  compShape3.getInfo();

  return 0;
}
