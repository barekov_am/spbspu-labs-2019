#include "square.hpp"

Square::Square(const double x, const double y) :
  Shape(x, y)
{}

void Square::draw(std::ostream& out) const
{
  out << "SQUARE (" << center_.first << ";" << center_.second << ")\n";
}
