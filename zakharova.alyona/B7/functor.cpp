#include "functor.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <iterator>
#include <iostream>

void MultByPI::operator()(double value)
{
  list_.push_back(value * M_PI);
}

void MultByPI::print() const
{
  std::copy(list_.begin(), list_.end(), std::ostream_iterator<double>(std::cout, " "));
}
