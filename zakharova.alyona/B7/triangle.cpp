#include "triangle.hpp"

Triangle::Triangle(const double x, const double y) :
  Shape(x, y)
{}

void Triangle::draw(std::ostream& out) const
{
  out << "TRIANGLE (" << center_.first << ";" << center_.second << ")\n";
}
