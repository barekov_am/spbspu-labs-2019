#include <vector>
#include <memory>
#include <string>
#include <iostream>
#include <cctype>
#include <sstream>
#include <stdexcept>
#include "shape.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "square.hpp"
#include "adapter.hpp"

void task2()
{
  std::vector<std::shared_ptr<Shape>> shapeList;

  std::string line;
  std::string name;
  double x, y;

  char symbol;

  while (std::getline(std::cin, line))
  {
    std::stringstream stream(line);

    stream >> std::ws;

    if (!stream.eof())
    {
      name.clear();
      symbol = stream.get();
      while ((symbol != '(') && !std::isspace(symbol) && !stream.eof())
      {
        name += symbol;
        symbol = stream.get();
      }

      if (std::isspace(symbol))
      {
        stream >> std::ws;
        symbol = stream.get();
      }

      if (symbol == '(')
      {
        stream >> x;
        stream >> std::ws;
        if (stream.get() == ';')
        {
          stream >> y;
          stream >> std::ws;
          if (stream.get() == ')')
          {
            if (name == "CIRCLE")
            {
              shapeList.push_back(std::make_shared<Circle>(x, y));
            }
            else if (name == "TRIANGLE")
            {
              shapeList.push_back(std::make_shared<Triangle>(x, y));
            }
            else if (name == "SQUARE")
            {
              shapeList.push_back(std::make_shared<Square>(x, y));
            }
            else
            {
              throw std::invalid_argument("Invalid shape");
            }
          }
          else
          {
            throw std::invalid_argument("Invalid center coordinates");
          }
        }
        else
        {
          throw std::invalid_argument("Invalid center coordinates");
        }
      }
      else
      {
        throw std::invalid_argument("Invalid center coordinates");
      }

      stream >> std::ws;

      if (!stream.eof())
      {
        throw std::invalid_argument("Invalid input");
      }
    }
  }

  Adapter adapter;
  adapter.print(shapeList, Adapter::ORIGINAL);
  adapter.print(shapeList, Adapter::LEFT_RIGHT);
  adapter.print(shapeList, Adapter::RIGTH_LEFT);
  adapter.print(shapeList, Adapter::TOP_BOTTOM);
  adapter.print(shapeList, Adapter::BOTTOM_TOP);
}
