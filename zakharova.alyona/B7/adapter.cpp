#include "adapter.hpp"

#include <algorithm>

void Adapter::print(Shapes& shapes, Adapter::Direction direction)
{
  if (direction == ORIGINAL)
  {
    std::cout << "Original:\n";
    std::for_each(shapes.begin(), shapes.end(), [](const std::shared_ptr<Shape> shape)
      {
        shape->draw(std::cout);
      }
    );
  }

  else if (direction == LEFT_RIGHT)
  {
    std::cout << "Left-Right:\n";
    
    sorted_ = shapes;
    std::sort(sorted_.begin(), sorted_.end(), [](const std::shared_ptr<Shape> shape1, const std::shared_ptr<Shape> shape2)
      {
        return shape1->isMoreLeft(*shape2);
      });

    std::for_each(sorted_.begin(), sorted_.end(), [](const std::shared_ptr<Shape> shape)
      {
        shape->draw(std::cout);
      });
  }

  else if (direction == RIGTH_LEFT)
  {
    std::cout << "Right-Left:\n";

    sorted_ = shapes;
    std::sort(sorted_.begin(), sorted_.end(), [](const std::shared_ptr<Shape> shape1, const std::shared_ptr<Shape> shape2)
      {
        return !shape1->isMoreLeft(*shape2);
      });

    std::for_each(sorted_.begin(), sorted_.end(), [](const std::shared_ptr<Shape> shape)
      {
        shape->draw(std::cout);
      });
  }

  else if (direction == TOP_BOTTOM)
  {
    std::cout << "Top-Bottom:\n";

    sorted_ = shapes;
    std::sort(sorted_.begin(), sorted_.end(), [](const std::shared_ptr<Shape> shape1, const std::shared_ptr<Shape> shape2)
      {
        return shape1->isUpper(*shape2);
      });

    std::for_each(sorted_.begin(), sorted_.end(), [](const std::shared_ptr<Shape> shape)
      {
        shape->draw(std::cout);
      });
  }

  else if (direction == BOTTOM_TOP)
  {
    std::cout << "Bottom-Top:\n";
    sorted_ = shapes;
    std::sort(sorted_.begin(), sorted_.end(), [](const std::shared_ptr<Shape> shape1, const std::shared_ptr<Shape> shape2)
      {
        return !shape1->isUpper(*shape2);
      });

    std::for_each(sorted_.begin(), sorted_.end(), [](const std::shared_ptr<Shape> shape)
      {
        shape->draw(std::cout);
      });
  }
}
