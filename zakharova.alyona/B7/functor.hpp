#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <list>

class MultByPI
{
public:
  MultByPI() = default;
  void operator()(double value);
  void print() const;

private:
  std::list<double> list_;
};

#endif // !FUNCTOR_HPP
