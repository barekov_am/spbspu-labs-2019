#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>

class Shape
{
public:
  Shape(const double x, const double y);
  virtual ~Shape() = default;

  bool isMoreLeft(const Shape& shape) const;
  bool isUpper(const Shape& shape) const;

  virtual void draw(std::ostream& out) const = 0;

protected:
  std::pair<double, double> center_;
};

#endif // !SHAPE_HPP
