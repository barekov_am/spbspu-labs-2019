#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

class Circle : public Shape
{
public:
  Circle(const double x, const double y);

  void draw(std::ostream& out) const override;
};

#endif // !CIRCLE_HPP
