#include <list>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include "functor.hpp"

void task1()
{
  std::list<double> list;
  double value;

  while (std::cin && !(std::cin >> value).eof())
  {
    if (std::cin.bad() || std::cin.fail())
    {
      throw std::invalid_argument("Invalid input");
    }

    list.push_back(value);
  }

  MultByPI functor;
  functor = std::for_each(list.begin(), list.end(), functor);
  functor.print();
}
