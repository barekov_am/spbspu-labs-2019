#include "circle.hpp"

Circle::Circle(const double x, const double y) :
  Shape(x, y)
{}

void Circle::draw(std::ostream& out) const
{
  out << "CIRCLE (" << center_.first << ";" << center_.second << ")\n";
}
