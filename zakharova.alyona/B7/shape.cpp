#include "shape.hpp"

Shape::Shape(const double x, const double y) :
  center_({ x, y })
{}

bool Shape::isMoreLeft(const Shape& shape) const
{
  return this->center_.first < shape.center_.first;
}

bool Shape::isUpper(const Shape& shape) const
{
  return this->center_.second > shape.center_.second;
}
