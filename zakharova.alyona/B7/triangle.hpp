#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

class Triangle : public Shape
{
public:
  Triangle(const double x, const double y);

  void draw(std::ostream& out) const override;
};

#endif // !TRIANGLE_HPP
