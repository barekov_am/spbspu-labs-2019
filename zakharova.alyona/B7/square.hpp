#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "shape.hpp"

class Square : public Shape
{
public:
  Square(const double x, const double y);

  void draw(std::ostream& out) const override;
};

#endif // !SQUARE_HPP
