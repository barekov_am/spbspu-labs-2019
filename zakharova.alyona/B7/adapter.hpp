#ifndef ADAPTER_HPP
#define ADAPTER_HPP

#include <vector>
#include <memory>
#include <iostream>

#include "shape.hpp"

using Shapes = std::vector<std::shared_ptr<Shape>>;

class Adapter
{
public:
  enum Direction
  {
    ORIGINAL,
    LEFT_RIGHT,
    RIGTH_LEFT,
    TOP_BOTTOM,
    BOTTOM_TOP
  };

  Adapter() = default;

  void print(Shapes& shapes, Direction directrion);

private:
  Shapes sorted_;
};

#endif // !ADAPTER_HPP
