#include <vector>

#include "details.hpp"
#include "tasks.hpp"


void task3()
{
  std::vector< int > intVector;
  
  int num = -1;
  while(std::cin && !(std::cin >> num).eof()) {
    if (std::cin.fail()) {
      throw std::ios_base::failure("Fail reading data\n");
    }

    if (num == 0) {
      break;
    }
    
    intVector.push_back(num);
  }

  if (intVector.empty()) {
    return;
  }

  if (num != 0) {
    throw std::runtime_error("End of line character is invalid. Expected 0\n");
  }

  std::vector< int >::iterator iter = intVector.begin();

  switch (intVector.back()) {
  case 1:
    {
      while (iter != intVector.end()) {
        iter = (((*iter) % 2 == 0) ? intVector.erase(iter) : ++iter);
      }

      break;
    }

  case 2:
    {
      while (iter != intVector.end()) {
        iter = (((*iter) % 3 == 0) ? (intVector.insert(++iter, 3, 1) + 3) : ++iter);
      }

      break;
    }
  }

  print(intVector);
}
