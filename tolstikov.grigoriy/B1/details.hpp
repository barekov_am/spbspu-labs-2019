#ifndef DETAILS_HPP
#define DETAILS_HPP

#include <functional>
#include <iostream>

#include "strategy.hpp"

enum class direction_t {
  ascending,
  descending
};

direction_t getDirection(const char * direction);

template < template < typename Collection > class AccessStrategy, typename Collection >
void sort(Collection & collection, direction_t direction)
{
  using value_type = typename Collection::value_type;

  std::function< bool(value_type, value_type) > compare = (direction == direction_t::ascending)
      ? std::function < bool(value_type, value_type) >(std::less< value_type >())
      : std::function < bool(value_type, value_type) >(std::greater< value_type >());

  using Strategy = AccessStrategy< Collection >;

  const auto begin = Strategy::begin(collection);
  const auto end = Strategy::end(collection);

  for (auto i = begin; i != end; ++i) {
    for (auto j = Strategy::next(i); j != end; ++j) {
      if (compare(Strategy::getElement(collection, j), Strategy::getElement(collection, i))) {
        std::swap(Strategy::getElement(collection, j), Strategy::getElement(collection, i));
      }
    }
  }
}

template < typename Collection >
void print(const Collection & collection)
{
  for (auto iter = collection.begin(); iter != collection.end(); ++iter) {
    std::cout << *iter << ' ';
  }

  std::cout << '\n';

}

#endif
