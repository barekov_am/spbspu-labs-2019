#include <vector>
#include <forward_list>

#include "tasks.hpp"
#include "details.hpp"

void task1(const char * direction)
{
  const direction_t dir = getDirection(direction);

  std::vector< int > vectorBracket;
  int num = -1;
  while (std::cin && !(std::cin >> num).eof()) {
    if (std::cin.fail()) {
      throw std::ios_base::failure("Fail reading data\n");
    }

    vectorBracket.push_back(num);
  }

  if (vectorBracket.empty()){
    return;
  }
  
  std::vector< int > vectorAt(vectorBracket);
  std::forward_list< int > forwardList(vectorBracket.begin(), vectorBracket.end());

  sort< AccessByBrackets >(vectorBracket, dir);
  print(vectorBracket);

  sort< AccessByAt >(vectorAt, dir);
  print(vectorAt);

  sort< AccessByIterator >(forwardList, dir);
  print(forwardList);

}
