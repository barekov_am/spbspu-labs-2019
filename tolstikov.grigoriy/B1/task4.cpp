#include <vector>

#include "details.hpp"
#include "tasks.hpp"

void fillRandom(double * array, int size)
{
  for (int i = 0; i < size; ++i) {
    array[i] = (rand() % 21 - 10) / 10.0;
  }
}
void task4(const char * direction, int size)
{
  direction_t dir = getDirection(direction);

  if (size <  0) {
    throw std::invalid_argument("Size can't be negative\n");
  }

  std::vector< double > doubleVector(size);
  fillRandom(&doubleVector[0], size);

  print(doubleVector);
  sort< AccessByBrackets >(doubleVector,dir);
  print(doubleVector);
}
