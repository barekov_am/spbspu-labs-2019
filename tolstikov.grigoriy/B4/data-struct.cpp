#include "data-struct.hpp"
#include "manipulator.hpp"

dataStruct::DataStruct::DataStruct(int k1, int k2, const std::string & s):
  key1(k1),
  key2(k2),
  str(s)
{
  if (k1 > dataStruct::maxKey || k2 > dataStruct::maxKey) {
    throw std::out_of_range("Key is greater than max value");
  }

  if (k1 < dataStruct::minKey || k2 < dataStruct::minKey) {
    throw std::out_of_range("Key is less than min value");
  }
}

std::istream & dataStruct::operator >>(std::istream & in, const char character)
{
  if (in.peek() != character) {
    in.setstate(std::istream::failbit);
  }

  in.ignore();
  return in;
}

std::istream & dataStruct::operator >>(std::istream & in, DataStruct & dataStruct)
{
  std::istream::sentry sentry(in);

  if (sentry) {
    if (in.peek() == EOF) {
      in.ignore();
      in.setstate(std::ios_base::eofbit);
      return in;
    }

    int key[2] = { 0, 0 };
    std::string str;
    in >> manipulator::blank >> key[0] >> manipulator::blank >> dataStruct::delimiter
       >> manipulator::blank >> key[1] >> manipulator::blank >> dataStruct::delimiter
       >> manipulator::blank;

    if (!in || in.eof()) {
      in.clear(in.rdstate() & ~std::ios_base::eofbit);
      in.setstate(std::istream::failbit);
      return in;
    }

    if (!getline(in, str)) {
      in.setstate(std::istream::failbit);
      return in;
    }

    dataStruct = DataStruct(key[0], key[1], str);
  }

  return in;
}


std::ostream & dataStruct::operator <<(std::ostream & out, const DataStruct & dataStruct)
{
  std::ostream::sentry sentry(out);

  if (sentry) {
    out << dataStruct.key1 << dataStruct::delimiter << ' '
        << dataStruct.key2 << dataStruct::delimiter << ' '
        << dataStruct.str << "\n";
  }
  return out;
}

bool dataStruct::compare_data_struct::operator ()(const DataStruct & lhs, const DataStruct & rhs) const
{
  if (lhs.key1 != rhs.key1) {
    return lhs.key1 < rhs.key1;
  }
  if (lhs.key2 != rhs.key2) {
    return lhs.key2 < rhs.key2;
  }

  return lhs.str.length() < rhs.str.length();
}
