#include <iostream>

void task();

int main(int argc, char **)
{
  if (argc != 1) {
    std::cerr << "Wrong number of arguments\n";
    return 1;
  }

  try {
    task();

  } catch (const std::out_of_range & e) {
    std::cerr << "error: " << e.what() << "\n";
    return 1;
  } catch (const std::ios_base::failure & e) {
    std::cerr << "error: " << e.what() << "\n";
    return 1;
  } catch (const std::exception & e) {
    std::cerr << "error: " << e.what() << "\n";
    return 2;
  }

  return 0;
}
