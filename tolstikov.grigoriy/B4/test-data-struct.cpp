#include <boost/test/auto_unit_test.hpp>

#include "algorithm"
#include "data-struct.hpp"

static void checkException(dataStruct::vector & d, std::istream & in)
{
  in >> std::noskipws;
  std::for_each(dataStruct::inputIterator(in), dataStruct::inputIterator(), [&](auto & elem) {
      d.push_back(elem);
  });

  if (!in.good() && !in.eof()) {
    throw std::ios_base::failure("Fail reading data");
  }
}

BOOST_AUTO_TEST_SUITE(testDataStructIostreamOperatorsSuite)

BOOST_AUTO_TEST_CASE(checkEmptyInput)
{
  dataStruct::vector data;
  std::stringstream in("");
  std::for_each(dataStruct::inputIterator(in), dataStruct::inputIterator(), [&](auto & elem) {
      data.push_back(elem);
  });
  std::stringstream out("");
  std::copy(data.begin(), data.end(), std::ostream_iterator< dataStruct::DataStruct >(out));
  BOOST_CHECK_MESSAGE(out.str().empty(), "Empty input failed.");
}

BOOST_AUTO_TEST_CASE(singleCorrectData)
{
  dataStruct::vector data;
  std::stringstream in("1, 2, Data");
  std::for_each(dataStruct::inputIterator(in), dataStruct::inputIterator(), [&](auto & elem) {
      data.push_back(elem);
  });
  std::stringstream out("");
  std::copy(data.begin(), data.end(), std::ostream_iterator< dataStruct::DataStruct >(out));
  BOOST_CHECK_MESSAGE(out.str() == "1, 2, Data\n", "Single correct data failed.");
}

BOOST_AUTO_TEST_CASE(checkBlankChars)
{
  dataStruct::vector data;
  std::stringstream in(" \t\t -1 ,  \t 2 \t \t, \t Data \t\n"
                       "3,4,data");
  std::for_each(dataStruct::inputIterator(in), dataStruct::inputIterator(), [&](auto & elem) {
      data.push_back(elem);
  });
  std::stringstream out("");
  std::copy(data.begin(), data.end(), std::ostream_iterator< dataStruct::DataStruct >(out));
  BOOST_CHECK_MESSAGE(out.str() == "-1, 2, Data \t\n3, 4, data\n", "Check blank characters failed.");
}


BOOST_AUTO_TEST_CASE(exceptIncorrectDelimeter)
{
  dataStruct::vector data1;
  std::stringstream in1("3. 4, Data");
  BOOST_CHECK_THROW(checkException(data1, in1), std::ios_base::failure);

  dataStruct::vector data2;
  std::stringstream in2("3, 4; Data");
  BOOST_CHECK_THROW(checkException(data2, in2), std::ios_base::failure);
}

BOOST_AUTO_TEST_CASE(exceptNoDelimiter)
{
  dataStruct::vector data1;
  std::stringstream in1("\n");
  BOOST_CHECK_THROW(checkException(data1, in1), std::ios_base::failure);

  dataStruct::vector data2;
  std::stringstream in2("3, 4 Data");
  BOOST_CHECK_THROW(checkException(data2, in2), std::ios_base::failure);
}

BOOST_AUTO_TEST_CASE(exceptNoParameters)
{
  dataStruct::vector data1;
  std::stringstream in1("3, , Data\n");
  BOOST_CHECK_THROW(checkException(data1, in1), std::ios_base::failure);

  dataStruct::vector data2;
  std::stringstream in2(", , Data\n");
  BOOST_CHECK_THROW(checkException(data2, in2), std::ios_base::failure);

  dataStruct::vector data3;
  std::stringstream in3(" ,5 \n");
  BOOST_CHECK_THROW(checkException(data3, in3), std::ios_base::failure);
}

BOOST_AUTO_TEST_CASE(exceptKeysOutOfRange)
{
  dataStruct::vector data1;
  std::stringstream in1("5, 7, Data");
  BOOST_CHECK_THROW(checkException(data1, in1), std::out_of_range);

  dataStruct::vector data2;
  std::stringstream in2("5, -7, Data");
  BOOST_CHECK_THROW(checkException(data2, in2), std::out_of_range);

  dataStruct::vector data3;
  std::stringstream in3("7, 5, Data");
  BOOST_CHECK_THROW(checkException(data3, in3), std::out_of_range);

  dataStruct::vector data4;
  std::stringstream in4("-7, 5, Data");
  BOOST_CHECK_THROW(checkException(data4, in4), std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(testSortDataStructSuite)

BOOST_AUTO_TEST_CASE(checkSortByKey1)
{
  dataStruct::vector data;
  std::stringstream in("-1 , 0, Data01\n"
                       "-5, 1, Data2\n"
                       "3, 0, Data3\n"
                       "1, 3, Data2\n"
                       "-4, 2, Data01\n"
                       "5, 2, Data3\n"
                       "0, 3, Data01\n");
  std::for_each(dataStruct::inputIterator(in), dataStruct::inputIterator(), [&](auto & elem) {
      data.push_back(elem);
  });

  std::sort(data.begin(), data.end(), dataStruct::compare_data_struct());

  std::stringstream out("");
  std::copy(data.begin(), data.end(), std::ostream_iterator< dataStruct::DataStruct >(out));

  BOOST_CHECK_MESSAGE(out.str() == "-5, 1, Data2\n"
                                   "-4, 2, Data01\n"
                                   "-1, 0, Data01\n"
                                   "0, 3, Data01\n"
                                   "1, 3, Data2\n"
                                   "3, 0, Data3\n"
                                   "5, 2, Data3\n", "Check sort by key1 failed.");
}

BOOST_AUTO_TEST_CASE(checkSortByKey2)
{
  dataStruct::vector data;
  std::stringstream in("0 , -1, Data01\n"
                       "0, -5, Data2\n"
                       "0, 3, Data3\n"
                       "0, 1, Data2\n"
                       "0, -4, Data01\n"
                       "0, 5, Data3\n"
                       "0, 0, Data01\n");
  std::for_each(dataStruct::inputIterator(in), dataStruct::inputIterator(), [&](auto & elem) {
      data.push_back(elem);
  });

  std::sort(data.begin(), data.end(), dataStruct::compare_data_struct());

  std::stringstream out("");
  std::copy(data.begin(), data.end(), std::ostream_iterator< dataStruct::DataStruct >(out));

  BOOST_CHECK_MESSAGE(out.str() == "0, -5, Data2\n"
                                   "0, -4, Data01\n"
                                   "0, -1, Data01\n"
                                   "0, 0, Data01\n"
                                   "0, 1, Data2\n"
                                   "0, 3, Data3\n"
                                   "0, 5, Data3\n", "Check sort by key2 failed.");
}

BOOST_AUTO_TEST_CASE(checkSortByStrLength)
{
  dataStruct::vector data;
  std::stringstream in("0 , 1, s1\n"
                       "0, 1, s\n"
                       "0, 1, s12345\n"
                       "0, 1, s123\n"
                       "0, 1, s12345 7\n"
                       "0, 1, s12\n"
                       "0, 1, s12 4\n");
  std::for_each(dataStruct::inputIterator(in), dataStruct::inputIterator(), [&](auto & elem) {
      data.push_back(elem);
  });

  std::sort(data.begin(), data.end(), dataStruct::compare_data_struct());

  std::stringstream out("");
  std::copy(data.begin(), data.end(), std::ostream_iterator< dataStruct::DataStruct >(out));

  BOOST_CHECK_MESSAGE(out.str() == "0, 1, s\n"
                                   "0, 1, s1\n"
                                   "0, 1, s12\n"
                                   "0, 1, s123\n"
                                   "0, 1, s12 4\n"
                                   "0, 1, s12345\n"
                                   "0, 1, s12345 7\n", "Check sort by str length failed.");
}

BOOST_AUTO_TEST_SUITE_END()
