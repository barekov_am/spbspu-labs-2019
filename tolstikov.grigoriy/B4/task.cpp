#include <algorithm>

#include "data-struct.hpp"

void task()
{
  std::cin >> std::noskipws;
  dataStruct::vector data((dataStruct::inputIterator(std::cin)), dataStruct::inputIterator());

  if (!std::cin.good() && !std::cin.eof()) {
    throw std::ios_base::failure("Fail reading data");
  }

  std::sort(data.begin(), data.end(), dataStruct::compare_data_struct());
  std::copy(data.begin(), data.end(), std::ostream_iterator< dataStruct::DataStruct >(std::cout));
}
