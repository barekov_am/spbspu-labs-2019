#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP

#include <iostream>
#include <iterator>
#include <vector>

namespace dataStruct {
  const int maxKey = 5;
  const int minKey = -5;
  const char delimiter = ',';

  struct DataStruct {
    int key1;
    int key2;
    std::string str;

    DataStruct() = default;
    DataStruct(int, int, const std::string &);
  };

  using inputIterator = std::istream_iterator< dataStruct::DataStruct >;
  using vector = std::vector< dataStruct::DataStruct >;

  std::istream & operator >>(std::istream & in, const char character);
  std::istream & operator >>(std::istream & in, DataStruct & dataStruct);
  std::ostream & operator <<(std::ostream & out, const DataStruct & dataStruct);

  struct compare_data_struct {
    bool operator ()(const DataStruct & lhs, const DataStruct & rhs) const;
  };
}

#endif
