#include <boost/test/auto_unit_test.hpp>

#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

const double DELTA = 0.00001;

BOOST_AUTO_TEST_SUITE(testPartition)

BOOST_AUTO_TEST_CASE(intersectCorrectness)
{
  tolstikov::Circle testCircle({ 2, 0 }, 3);
  tolstikov::Rectangle testRectangle({ 2, 4 }, 9, 5);
  tolstikov::Rectangle testSquare({ 12, 21 }, 1);

  BOOST_CHECK(tolstikov::isIntersected(testCircle.getFrameRect(),testRectangle.getFrameRect()));
  BOOST_CHECK(!(tolstikov::isIntersected(testSquare.getFrameRect(),testRectangle.getFrameRect())));
  BOOST_CHECK(!(tolstikov::isIntersected(testCircle.getFrameRect(),testSquare.getFrameRect())));
}

BOOST_AUTO_TEST_CASE(partitionCorrectness)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t{ 1, 1 }, 2);
  tolstikov::Shape::pointer testRectangle1 = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t{ 3, -1 }, 5, 4);
  tolstikov::Shape::pointer testSquare1 = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t{ 6, 15 }, 4);
  tolstikov::Shape::pointer testRectangle2 = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t{ 10, 2 }, 2, 3);
  tolstikov::Shape::pointer testRectangle3 = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t{ 8, 15 }, 6, 30);
  tolstikov::Shape::pointer testSquare2 = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t{ -20, 15 }, 5);

  tolstikov::CompositeShape testComposition;
  testComposition.add(testCircle);
  testComposition.add(testRectangle1);
  testComposition.add(testSquare1);
  testComposition.add(testRectangle2);
  testComposition.add(testRectangle3);
  testComposition.add(testSquare2);

  tolstikov::Matrix testMatrix = tolstikov::part(testComposition);

  const std::size_t correctRows = 3;
  const std::size_t correctColumns = 4;

  BOOST_CHECK_EQUAL(testMatrix.getRows(), correctRows);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), correctColumns);
  BOOST_CHECK(testMatrix[0][0] == testCircle);
  BOOST_CHECK(testMatrix[0][1] == testSquare1);
  BOOST_CHECK(testMatrix[0][2] == testRectangle2);
  BOOST_CHECK(testMatrix[0][3] == testSquare2);
  BOOST_CHECK(testMatrix[1][0] == testRectangle1);
  BOOST_CHECK(testMatrix[2][0] == testRectangle3);
}

BOOST_AUTO_TEST_SUITE_END()
