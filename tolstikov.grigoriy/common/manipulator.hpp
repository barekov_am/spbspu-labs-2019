#ifndef MANIPULATOR_HPP
#define MANIPULATOR_HPP

#include <istream>

namespace manipulator {
  template < typename Char, typename CharTraits >
  std::basic_istream< Char, CharTraits > & blank(std::basic_istream< Char, CharTraits > & in)
  {
    while (std::isblank(in.peek())) {
      in.ignore();
    }

    return in;
  }
}

#endif
