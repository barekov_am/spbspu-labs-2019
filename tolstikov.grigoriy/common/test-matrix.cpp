#include <boost/test/auto_unit_test.hpp>

#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "partition.hpp"

const double DELTA = 0.00001;

BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(testCopyConstructor)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 3.5, 4.5 }, 3);
  tolstikov::Shape::pointer testRectangle = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 1, 0 }, 1, 4);
  tolstikov::CompositeShape testComposition;

  testComposition.add(testCircle);
  testComposition.add(testRectangle);

  tolstikov::Matrix testMatrix = tolstikov::part(testComposition);
  tolstikov::Matrix testMatrixCopy(testMatrix);

  BOOST_CHECK(testMatrix == testMatrixCopy);
  BOOST_CHECK_EQUAL(testMatrixCopy.getRows(), testMatrix.getRows());
  BOOST_CHECK_EQUAL(testMatrixCopy.getColumns(), testMatrix.getColumns());
}

BOOST_AUTO_TEST_CASE(testMoveConstructor)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 1, 2 }, 3);
  tolstikov::Shape::pointer testRectangle = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 1, 2 }, 1, 6);
  tolstikov::CompositeShape testComposition;

  testComposition.add(testCircle);
  testComposition.add(testRectangle);

  tolstikov::Matrix testMatrix = tolstikov::part(testComposition);
  tolstikov::Matrix testMatrixCopy(testMatrix);
  tolstikov::Matrix testMatrixMove = std::move(testMatrix);

  BOOST_CHECK(testMatrixMove == testMatrixCopy);
  BOOST_CHECK_EQUAL(testMatrixMove.getRows(), testMatrixCopy.getRows());
  BOOST_CHECK_EQUAL(testMatrixMove.getColumns(), testMatrixCopy.getColumns());;
}

BOOST_AUTO_TEST_CASE(testCopyOperator)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 3, 7.2 }, 11);
  tolstikov::Shape::pointer testRectangle = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 2, 4 }, 9, 2);
  tolstikov::CompositeShape testComposition;

  testComposition.add(testCircle);
  testComposition.add(testRectangle);

  tolstikov::Matrix testMatrix = tolstikov::part(testComposition);
  tolstikov::Matrix testMatrixCopy;
  testMatrixCopy = testMatrix;

  BOOST_CHECK(testMatrixCopy == testMatrix);
  BOOST_CHECK_EQUAL(testMatrixCopy.getRows(), testMatrix.getRows());
  BOOST_CHECK_EQUAL(testMatrixCopy.getColumns(), testMatrix.getColumns());;
}

BOOST_AUTO_TEST_CASE(testMoveOperator)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 2, 0 }, 3);
  tolstikov::Shape::pointer testRectangle = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 2, 4 }, 9, 5);
  tolstikov::CompositeShape testComposition;

  testComposition.add(testCircle);
  testComposition.add(testRectangle);

  tolstikov::Matrix testMatrix = tolstikov::part(testComposition);
  tolstikov::Matrix testMatrixCopy(testMatrix);
  tolstikov::Matrix testMatrixMove;
  testMatrixMove = std::move(testMatrix);

  BOOST_CHECK(testMatrixMove == testMatrixCopy);
  BOOST_CHECK_EQUAL(testMatrixMove.getRows(), testMatrixCopy.getRows());
  BOOST_CHECK_EQUAL(testMatrixMove.getColumns(), testMatrixCopy.getColumns());;
}

BOOST_AUTO_TEST_CASE(exceptOutOfRange)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 2, 0 }, 3);
  tolstikov::Shape::pointer testRectangle = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 2, 4 }, 9, 5);
  tolstikov::CompositeShape testComposition;

  testComposition.add(testCircle);
  testComposition.add(testRectangle);

  tolstikov::Matrix testMatrix = tolstikov::part(testComposition);

  BOOST_CHECK_THROW(testMatrix[105], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-2], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
