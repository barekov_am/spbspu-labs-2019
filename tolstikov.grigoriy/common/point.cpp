#include "point.hpp"

#include <boost/io/ios_state.hpp>
#include "manipulator.hpp"
#include "delimiters.hpp"

std::istream & operator >>(std::istream & in, point_t & point)
{
  boost::io::ios_flags_saver flags(in);
  in >> std::noskipws;
  std::istream::sentry sentry(in);
  if (sentry) {
    point_t tmpPoint;
    in >> manipulator::blank >> Delimiters(Delimiters::openBracket) >> manipulator::blank >> tmpPoint.x
       >> manipulator::blank >> Delimiters(Delimiters::semicolon) >> manipulator::blank >> tmpPoint.y
       >> manipulator::blank >> Delimiters(Delimiters::closeBracket) >> manipulator::blank;

    if (in.fail()) {
      return in;
    }

    point = tmpPoint;
  }

  return in;
}

std::ostream & operator <<(std::ostream & out, const point_t & point)
{
  std::ostream::sentry sentry(out);

  if (sentry) {
    out << Delimiters::openBracket << point.x << Delimiters::semicolon << point.y << Delimiters::closeBracket;
  }
  return out;
}
