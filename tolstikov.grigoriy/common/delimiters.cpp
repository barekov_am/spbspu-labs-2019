#include "delimiters.hpp"

Delimiters::Delimiters(char character):
  character_(character)
{
}

std::istream & operator >>(std::istream & in, const Delimiters & d)
{
  if (in.peek() != d.character_) {
    in.setstate(std::istream::failbit);
    return in;
  }

  in.ignore();
  return in;
}
