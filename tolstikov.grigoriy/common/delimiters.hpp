#ifndef DELIMITERS_HPP
#define DELIMITERS_HPP

#include <istream>

class Delimiters {
public:
  static const char semicolon = ';';
  static const char openBracket = '(';
  static const char closeBracket = ')';

  Delimiters(char );

  friend std::istream & operator >>(std::istream &, const Delimiters &);
private:
  char character_;
};

std::istream & operator >>(std::istream &, const Delimiters &);

#endif
