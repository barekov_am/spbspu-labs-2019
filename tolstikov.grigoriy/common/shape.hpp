#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "base-types.hpp"

namespace tolstikov {

  class Shape {
  public:
    using pointer = std::shared_ptr<Shape>;
    using array = std::unique_ptr<pointer[]>;

    virtual ~Shape() = default;

    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;

    virtual void scale(double scaleFactor) = 0;
    virtual void move(point_t point) = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void rotate(double angle) = 0;
  };

}

#endif
