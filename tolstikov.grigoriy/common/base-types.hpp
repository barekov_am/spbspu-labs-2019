#ifndef BASETYPES_HPP
#define BASETYPES_HPP

namespace tolstikov {

  struct point_t {
    double x;
    double y;
  };

  struct rectangle_t {
    point_t pos;
    double width;
    double height;
  };

  bool isIntersected(const rectangle_t & lhs, const rectangle_t & rhs);
}

#endif
