#include "rectangle.hpp"
#include <math.h>

const double FULLCIRCLE = 360;

tolstikov::Rectangle::Rectangle(point_t pos, double width, double height):
  pos_(pos),
  width_(width),
  height_(height),
  rotationAngle_(0)
{
  if ((width <= 0) || (height <= 0)) {
    throw std::invalid_argument("Width and height must be a positive number!\n");
  }
}

tolstikov::Rectangle::Rectangle(point_t pos, double width):
  pos_(pos),
  width_(width),
  height_(width),
  rotationAngle_(0)
{
  if (width <= 0) {
    throw std::invalid_argument("Width must be a positive number!\n");
  }
}

double tolstikov::Rectangle::getArea() const
{
  return width_ * height_;
}

tolstikov::rectangle_t tolstikov::Rectangle::getFrameRect() const
{
  const double cosA = std::cos((2 * M_PI * rotationAngle_) / FULLCIRCLE);
  const double sinA = std::sin((2 * M_PI * rotationAngle_) / FULLCIRCLE);
  const double width = width_ * std::fabs(cosA) + height_ * std::fabs(sinA);
  const double height = height_ * std::fabs(cosA) + width_ * std::fabs(sinA);
  return { pos_, width, height };
}

void tolstikov::Rectangle::scale(double scaleFactor)
{
  if (scaleFactor <= 0) {
    throw std::invalid_argument("Factor of sacle must be a positive number!\n");
  }
  width_ *= scaleFactor;
  height_ *= scaleFactor;
}

void tolstikov::Rectangle::move(point_t point)
{
  pos_ = point;
}

void tolstikov::Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void tolstikov::Rectangle::rotate(double angle)
{
  rotationAngle_ += angle;

  while (std::abs(rotationAngle_) >= FULLCIRCLE) {
    rotationAngle_ = (angle > 0) ? rotationAngle_ - FULLCIRCLE : rotationAngle_ + FULLCIRCLE;
  }
}
