#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace tolstikov {

  tolstikov::Matrix part(const tolstikov::CompositeShape & composition);

}

#endif
