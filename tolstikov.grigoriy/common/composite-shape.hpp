#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include <iostream>
#include "shape.hpp"

namespace tolstikov {

  class CompositeShape : public Shape {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape & rhs);
    CompositeShape(CompositeShape && rhs);
    ~CompositeShape() override = default;

    CompositeShape & operator =(const CompositeShape & rhs);
    CompositeShape & operator =(CompositeShape && rhs);
    Shape::pointer operator [](std::size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;

    void scale(double scaleFactor) override;
    void move(point_t point) override;
    void move(double dx, double dy) override;
    void rotate(double angle) override;

    std::size_t getSize() const;
    std::size_t getIndex(const Shape::pointer & shape) const;

    void add(const Shape::pointer & shape);
    void remove(std::size_t index);

  private:
    Shape::array shapes_;
    std::size_t count_;
  };

}

#endif // COMPOSITESHAPE_HPP
