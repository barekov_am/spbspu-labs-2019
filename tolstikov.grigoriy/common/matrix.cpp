#include "matrix.hpp"

tolstikov::Matrix::Matrix():
  rows_(0),
  columns_(0)
{
}

tolstikov::Matrix::Matrix(const Matrix & rhs):
  data_(std::make_unique<Shape::pointer[]>(rhs.rows_ * rhs.columns_)),
  rows_(rhs.rows_),
  columns_(rhs.columns_)
{
  for (std::size_t i = 0; i < (rows_ * columns_); i++) {
    data_[i] = rhs.data_[i];
  }
}

tolstikov::Matrix::Matrix(Matrix && rhs):
  data_(std::move(rhs.data_)),
  rows_(rhs.rows_),
  columns_(rhs.columns_)
{
  rhs.rows_ = 0;
  rhs.columns_ = 0;
}

tolstikov::Matrix & tolstikov::Matrix::operator =(const Matrix & rhs)
{
  if (&rhs == this) {
    return *this;
  }

  Shape::array tmpData = std::make_unique<Shape::pointer[]>(rhs.rows_ * rhs.columns_);
  for (std::size_t i = 0; i < (rhs.rows_ * rhs.columns_); i++) {
    tmpData[i] = rhs.data_[i];
  }

  data_.swap(tmpData);
  rows_ = rhs.rows_;
  columns_ = rhs.columns_;

  return *this;
}

tolstikov::Matrix & tolstikov::Matrix::operator =(Matrix && rhs)
{
  if (&rhs == this) {
    return *this;
  }
  rows_ = rhs.rows_;
  columns_ = rhs.columns_;
  data_ = std::move(rhs.data_);
  rhs.rows_ = 0;
  rhs.columns_ = 0;

  return *this;
}

tolstikov::Shape::array tolstikov::Matrix::operator [](std::size_t layer) const
{
  if (layer >= rows_) {
    throw std::out_of_range("Index is out of range!\n");
  }

  Shape::array tmpData = std::make_unique<Shape::pointer[]>(getLayerSize(layer));

  for (std::size_t i = 0; i < getLayerSize(layer); i++) {
    tmpData[i] = data_[layer * columns_ + i];
  }

  return tmpData;
}

bool tolstikov::Matrix::operator ==(const Matrix & rhs) const
{
  if ((rows_ != rhs.rows_) || (columns_ != rhs.columns_)) {
    return false;
  }

  for (std::size_t i = 0; i < (rows_ * columns_); i++){
    if (data_[i] != rhs.data_[i]) {
        return false;
    }
  }

  return true;
}

bool tolstikov::Matrix::operator !=(const Matrix &rhs) const
{
  return !(*this == rhs);
}

std::size_t tolstikov::Matrix::getRows() const
{
  return rows_;
}

std::size_t tolstikov::Matrix::getColumns() const
{
  return columns_;
}

std::size_t tolstikov::Matrix::getLayerSize(size_t layer) const
{
  if (layer >= rows_) {
    return 0;
  }

  for (std::size_t i = 0; i < columns_; i++) {
    if (data_[layer * columns_ + i] == nullptr) {
      return i;
    }
  }

  return columns_;
}

void tolstikov::Matrix::add(const Shape::pointer & shape, std::size_t layer)
{
  if (shape == nullptr) {
    throw std::invalid_argument("Pointer can't be null!\n");
  }
  /*
     s.130: (layer > rows_) is protection from inappropriate value
     new layer is always created afrer the previous one
     so there aren't empty layers
  */
  std::size_t tmpRows = (layer >= rows_) ? (rows_ + 1) : rows_;
  std::size_t tmpColumns = (getLayerSize(layer) == columns_) ? (columns_ + 1) : columns_;
  const std::size_t column = (layer < rows_) ? getLayerSize(layer) : 0;

  Shape::array tmpData = std::make_unique<Shape::pointer[]>(tmpRows * tmpColumns);

  for (std::size_t i = 0; i < tmpRows; i++) {
    for (std::size_t j = 0; j < tmpColumns; j++) {
      if ((i == rows_) || (j == columns_)) {
        tmpData[i * tmpColumns + j] = nullptr;
      } else {
        tmpData[i * tmpColumns + j] = data_[i * columns_ + j];
      }
    }
  }

  if (layer < rows_) {
    tmpData[layer * tmpColumns + column] = shape;
  } else {
    tmpData[rows_ * tmpColumns + column] = shape;
  }

  data_.swap(tmpData);
  rows_ = tmpRows;
  columns_ = tmpColumns;
}
