#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

const double DELTA = 0.00001;

BOOST_AUTO_TEST_SUITE(testCircleSuite)

BOOST_AUTO_TEST_CASE(constCircleAfterMovingByValue)
{
  tolstikov::Circle testCircle({ 2.5, 3.5 }, 3);
  const double radiusBefore = testCircle.getRadius();
  const double areaBefore = testCircle.getArea();

  testCircle.move(5, 10);
  const double radiusAfter = testCircle.getRadius();
  const double areaAfter = testCircle.getArea();
  BOOST_CHECK_CLOSE(radiusAfter, radiusBefore, DELTA);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, DELTA);
}

BOOST_AUTO_TEST_CASE(constCircleAfterMovingToAPoint)
{
  tolstikov::Circle testCircle({ 1.5, 1.5 }, 5);
  const double radiusBefore = testCircle.getRadius();
  const double areaBefore = testCircle.getArea();

  testCircle.move({ 1, 5 });
  const double radiusAfter = testCircle.getRadius();
  const double areaAfter = testCircle.getArea();
  BOOST_CHECK_CLOSE(radiusAfter, radiusBefore, DELTA);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, DELTA);
}

BOOST_AUTO_TEST_CASE(quadraticIncreaseAreaAfterScaling)
{
  tolstikov::Circle testCircle({ 0.5, 1 }, 2);
  const double scaleFactor = 3;
  const double areaBefore = testCircle.getArea();

  testCircle.scale(scaleFactor);
  const double areaAfter = testCircle.getArea();
  BOOST_CHECK_CLOSE(areaAfter, areaBefore * scaleFactor * scaleFactor, DELTA);
}

BOOST_AUTO_TEST_CASE(quadraticDecreaseAreaAfterScaling)
{
  tolstikov::Circle testCircle({ -2, 1 }, 10);
  const double scaleFactor = 0.2;
  const double areaBefore = testCircle.getArea();

  testCircle.scale(scaleFactor);
  const double areaAfter = testCircle.getArea();
  BOOST_CHECK_CLOSE(areaAfter, areaBefore * scaleFactor * scaleFactor, DELTA);
}

BOOST_AUTO_TEST_CASE(testCircleRotation)
{
  tolstikov::Circle testCircle({5.0, 5.0}, 2.0);
  const double areaBefore = testCircle.getArea();
  const tolstikov::rectangle_t frameRectBefore = testCircle.getFrameRect();

  double angle = 90;
  testCircle.rotate(angle);
  double areaAfter = testCircle.getArea();
  tolstikov::rectangle_t frameRectAfter = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, DELTA);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, DELTA);

  angle = -150;
  testCircle.rotate(angle);
  areaAfter = testCircle.getArea();
  frameRectAfter = testCircle.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, DELTA);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, DELTA);
}

BOOST_AUTO_TEST_CASE(exceptInvalidRadius)
{
  BOOST_CHECK_THROW(tolstikov::Circle testCircle1({ -2, 1 }, -6), std::invalid_argument);
  BOOST_CHECK_THROW(tolstikov::Circle testCircle2({ -2, 1 }, 0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(exceptInvalidScaleFactor)
{
  tolstikov::Circle testCircle({ 0.5, 1 }, 2);

  const double scaleFactor1 = 0;
  const double scaleFactor2 = -11;

  BOOST_CHECK_THROW(testCircle.scale(scaleFactor1), std::invalid_argument);
  BOOST_CHECK_THROW(testCircle.scale(scaleFactor2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
