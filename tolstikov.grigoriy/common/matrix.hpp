#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

namespace tolstikov {

  class Matrix {
  public:
    Matrix();
    Matrix(const Matrix & rhs);
    Matrix(Matrix && rhs);
    ~Matrix() = default;

    Matrix & operator =(const Matrix & rhs);
    Matrix & operator =(Matrix && rhs);

    Shape::array operator [](std::size_t layer) const;
    bool operator ==(const Matrix & rhs) const;
    bool operator !=(const Matrix & rhs) const;

    std::size_t getRows() const;
    std::size_t getColumns() const;
    std::size_t getLayerSize(size_t layer) const;

    void add(const Shape::pointer & shape, std::size_t layer);

  private:
    Shape::array data_;
    std::size_t rows_;
    std::size_t columns_;
  };

}

#endif
