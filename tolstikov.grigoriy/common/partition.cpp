#include "partition.hpp"

tolstikov::Matrix tolstikov::part(const tolstikov::CompositeShape & composition)
{
  Matrix tmpMatrix;
  const std::size_t size = composition.getSize();

  for (std::size_t i = 0; i < size; i++) {
    std::size_t layer = 0;
    for (std::size_t j = 0; j < tmpMatrix.getRows(); j++) {
      bool intersect = false;
      for (std::size_t k = 0; k < tmpMatrix.getLayerSize(j); k++) {
        if (isIntersected(composition[i]->getFrameRect(), tmpMatrix[j][k]->getFrameRect())) {
          layer++;
          intersect = true;
          break;
        }
      }
      if (!intersect) {
        break;
      }
    }

    tmpMatrix.add(composition[i], layer);
  }

  return tmpMatrix;
}
