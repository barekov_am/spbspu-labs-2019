#include "composite-shape.hpp"
#include <iostream>
#include <math.h>

const double FULLCIRCLE = 360;

tolstikov::CompositeShape::CompositeShape():
  shapes_(nullptr),
  count_(0)
{
}

tolstikov::CompositeShape::CompositeShape(const CompositeShape & rhs):
  shapes_(std::make_unique<tolstikov::Shape::pointer[]>(rhs.count_)),
  count_(rhs.count_)
{
  for (std::size_t i = 0; i < count_; i++) {
    shapes_[i] = rhs.shapes_[i];
  }
}

tolstikov::CompositeShape::CompositeShape(CompositeShape && rhs):
  shapes_(std::move(rhs.shapes_)),
  count_(rhs.count_)
{
  rhs.count_ = 0;
}

tolstikov::CompositeShape & tolstikov::CompositeShape::operator =(const CompositeShape & rhs)
{
  if (&rhs == this) {
    return *this;
  }

  shapes_ = std::make_unique<tolstikov::Shape::pointer[]>(rhs.count_);
  count_ = rhs.count_;

  for (std::size_t i = 0; i < count_; i++) {
    shapes_[i] = rhs.shapes_[i];
  }

  return *this;
}

tolstikov::CompositeShape & tolstikov::CompositeShape::operator =(CompositeShape && rhs)
{
  if (&rhs == this) {
    return *this;
  }

  count_ = rhs.count_;
  shapes_ = std::move(rhs.shapes_);
  rhs.count_ = 0;
  rhs.shapes_ = nullptr;

  return *this;
}

tolstikov::Shape::pointer tolstikov::CompositeShape::operator [](std::size_t index) const
{
  if (index >= count_) {
    throw std::out_of_range("Index is out of range!\n");
  }

  return shapes_[index];
}

double tolstikov::CompositeShape::getArea() const
{
  double area = 0;

  for (std::size_t i = 0; i < count_; i++) {
    area += shapes_[i]->getArea();
  }

  return area;
}

tolstikov::rectangle_t tolstikov::CompositeShape::getFrameRect() const
{
  if (count_ == 0) {
    throw std::logic_error("No figures!\n");
  }

  rectangle_t tmpRect = shapes_[0]->getFrameRect();

  double minX = tmpRect.pos.x - (tmpRect.width) / 2;
  double maxX = tmpRect.pos.x + (tmpRect.width) / 2;

  double minY = tmpRect.pos.y - (tmpRect.height) / 2;
  double maxY = tmpRect.pos.y + (tmpRect.height) / 2;

  for (std::size_t i = 1; i < count_; i++) {
    tmpRect = shapes_[i]->getFrameRect();

    minX = std::min(minX, tmpRect.pos.x - (tmpRect.width) / 2);
    maxX = std::max(maxX, tmpRect.pos.x + (tmpRect.width) / 2);

    minY = std::min(minY, tmpRect.pos.y - (tmpRect.height) / 2);
    maxY = std::max(maxY, tmpRect.pos.y + (tmpRect.height) / 2);
  }

  return { { (maxX + minX) / 2, (maxY + minY) / 2 }, (maxX - minX), (maxY - minY) };
}

void tolstikov::CompositeShape::scale(double scaleFactor)
{
  if (scaleFactor <= 0) {
    throw std::invalid_argument("Factor of scale must be a positive number!\n");
  }

  const point_t frameCentre = getFrameRect().pos;

  for (std::size_t i = 0; i < count_; i++) {
    const double dx = shapes_[i]->getFrameRect().pos.x - frameCentre.x;
    const double dy = shapes_[i]->getFrameRect().pos.y - frameCentre.y;

    shapes_[i]->move(dx * (scaleFactor - 1), dy * (scaleFactor - 1));
    shapes_[i]->scale(scaleFactor);
  }
}

void tolstikov::CompositeShape::move(point_t point)
{
  const point_t frameCentre = getFrameRect().pos;

  const double dx = point.x - frameCentre.x;
  const double dy = point.y - frameCentre.y;

  move(dx, dy);
}

void tolstikov::CompositeShape::move(double dx, double dy)
{
  if (count_ == 0) {
    throw std::logic_error("No figures!\n");
  }

  for (std::size_t i = 0; i < count_; i++) {
    shapes_[i]->move(dx, dy);
  }
}

void tolstikov::CompositeShape::rotate(double angle)
{
  const double cosA = std::cos((2 * M_PI * angle) / FULLCIRCLE);
  const double sinA = std::sin((2 * M_PI * angle) / FULLCIRCLE);

  const point_t compCentre = getFrameRect().pos;

  for (std::size_t i = 0; i < count_; i++) {
    const point_t shapeCenter = shapes_[i]->getFrameRect().pos;

    const double dx = (shapeCenter.x - compCentre.x) * cosA - (shapeCenter.y - compCentre.y) * sinA;
    const double dy = (shapeCenter.x - compCentre.x) * sinA + (shapeCenter.y - compCentre.y) * cosA;

    shapes_[i]->move({ compCentre.x + dx, compCentre.y + dy });
    shapes_[i]->rotate(angle);
  }
}

std::size_t tolstikov::CompositeShape::getSize() const
{
  return count_;
}

std::size_t tolstikov::CompositeShape::getIndex(const Shape::pointer & shape) const
{
  if (!shape) {
    throw std::invalid_argument("Pointer can't be null!\n");
  }

  for (std::size_t i = 0; i < count_; i++) {
    if (shapes_[i] == shape) {
      return i;
    } 
  }

  return count_;
}

void tolstikov::CompositeShape::add(const Shape::pointer & shape)
{
  if (!shape) {
    throw std::invalid_argument("Pointer can't be null!\n");
  }

  if (getIndex(shape) != getSize()) {
    return;
  }

  Shape::array tmpShapes = std::make_unique<Shape::pointer[]>(count_ + 1);

  for (std::size_t i = 0; i < count_; i++) {
    tmpShapes[i] = shapes_[i];
  }

  tmpShapes[count_] = shape;
  ++count_;
  shapes_ = std::move(tmpShapes);
}

void tolstikov::CompositeShape::remove(std::size_t index)
{
  if (index >= count_) {
    throw std::out_of_range("Index is out of range!\n");
  }

  for (std::size_t i = index; i < (count_ - 1); i++) {
    shapes_[i] = shapes_[i+1];
  }

  shapes_[--count_] = nullptr;
}
