#include <boost/test/auto_unit_test.hpp>

#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double DELTA = 0.00001;

BOOST_AUTO_TEST_SUITE(testCompositeShapeSuite)

BOOST_AUTO_TEST_CASE(testCopyConstructor)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 3.5, 4.5 }, 3);
  tolstikov::Shape::pointer testRectangle = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 1, 0 }, 1, 4);
  tolstikov::CompositeShape testComposition;

  testComposition.add(testCircle);
  testComposition.add(testRectangle);

  tolstikov::CompositeShape testCompositionCopy(testComposition);
  BOOST_CHECK_EQUAL(testCompositionCopy.getSize(), testComposition.getSize());
  BOOST_CHECK_CLOSE(testCompositionCopy.getArea(), testComposition.getArea(), DELTA);


  tolstikov::Shape::pointer testSquare = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 1, 4 }, 2);
  testCompositionCopy.add(testSquare);
  BOOST_CHECK_EQUAL(testCompositionCopy.getSize(), testComposition.getSize() + 1);
  BOOST_CHECK_CLOSE(testCompositionCopy.getArea(), testComposition.getArea() + testSquare->getArea() , DELTA);
}

BOOST_AUTO_TEST_CASE(testMoveConstructor)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 2, 0 }, 3);
  tolstikov::CompositeShape testComposition;

  testComposition.add(testCircle);
  std::size_t testCompositionSize = testComposition.getSize();
  double testCompositionArea = testComposition.getArea();

  tolstikov::CompositeShape testCompositionMove = std::move(testComposition);
  BOOST_CHECK_EQUAL(testCompositionMove.getSize(), testCompositionSize);
  BOOST_CHECK_CLOSE(testCompositionMove.getArea(), testCompositionArea, DELTA);
  BOOST_CHECK_EQUAL(testComposition.getSize(), 0);
  BOOST_CHECK_CLOSE(testComposition.getArea(), 0, DELTA);
}

BOOST_AUTO_TEST_CASE(testCopyOperator)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 3, 7.2 }, 11);
  tolstikov::Shape::pointer testRectangle = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 2, 4 }, 9, 2);

  tolstikov::CompositeShape testComposition;


  testComposition.add(testCircle);
  testComposition.add(testRectangle);

  tolstikov::CompositeShape testCompositionCopy;
  testCompositionCopy = testComposition;
  BOOST_CHECK_EQUAL(testCompositionCopy.getSize(), testComposition.getSize());
  BOOST_CHECK_CLOSE(testCompositionCopy.getArea(), testComposition.getArea(), DELTA);

  tolstikov::Shape::pointer testSquare = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 5, 1 }, 3);
  testCompositionCopy.add(testSquare);
  BOOST_CHECK_EQUAL(testCompositionCopy.getSize(), testComposition.getSize() + 1);
  BOOST_CHECK_CLOSE(testCompositionCopy.getArea(), testComposition.getArea() + testSquare->getArea() , DELTA);

  std::size_t testCompositionCopySize = testCompositionCopy.getSize();
  double testCompositionCopyArea = testCompositionCopy.getArea();
  testCompositionCopy = testCompositionCopy;
  BOOST_CHECK_EQUAL(testCompositionCopy.getSize(), testCompositionCopySize);
  BOOST_CHECK_CLOSE(testCompositionCopy.getArea(), testCompositionCopyArea, DELTA);
}

BOOST_AUTO_TEST_CASE(testMoveOperator)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 2, 0 }, 3);
  tolstikov::CompositeShape testComposition;

  testComposition.add(testCircle);
  std::size_t testCompositionSize = testComposition.getSize();
  double testCompositionArea = testComposition.getArea();

  tolstikov::CompositeShape testCompositionMove;
  testCompositionMove = std::move(testComposition);
  BOOST_CHECK_EQUAL(testCompositionMove.getSize(), testCompositionSize);
  BOOST_CHECK_CLOSE(testCompositionMove.getArea(), testCompositionArea, DELTA);
  BOOST_CHECK_EQUAL(testComposition.getSize(), 0);
  BOOST_CHECK_CLOSE(testComposition.getArea(), 0, DELTA);
  testCompositionMove = std::move(testCompositionMove);
  BOOST_CHECK_EQUAL(testCompositionMove.getSize(), testCompositionSize);
  BOOST_CHECK_CLOSE(testCompositionMove.getArea(), testCompositionArea, DELTA);
}

BOOST_AUTO_TEST_CASE(constCompositeShapeAfterMovingByValue)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 2.5, 3.5 }, 3);
  tolstikov::Shape::pointer testRectangle = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 2, 1 }, 1, 4);
  tolstikov::Shape::pointer testSquare = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 5, 0 }, 7);
  tolstikov::CompositeShape testComposition;


  testComposition.add(testCircle);
  testComposition.add(testRectangle);
  testComposition.add(testSquare);

  const tolstikov::rectangle_t frameRectBefore = testComposition.getFrameRect();
  const double areaBefore = testComposition.getArea();

  const double dx = 5;
  const double dy = 10;
  testComposition.move(dx, dy);

  const tolstikov::rectangle_t frameRectAfter = testComposition.getFrameRect();
  const double areaAfter = testComposition.getArea();

  BOOST_CHECK_CLOSE(areaAfter, areaBefore, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x + dx, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y + dy, DELTA);
}

BOOST_AUTO_TEST_CASE(constCompositShapeAfterMovingToAPoint)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 4, 5 }, 2);
  tolstikov::Shape::pointer testRectangle = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 1, 4 }, 1, 3);
  tolstikov::Shape::pointer testSquare = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 0, 2 }, 4);
  tolstikov::CompositeShape testComposition;

  testComposition.add(testCircle);
  testComposition.add(testRectangle);
  testComposition.add(testSquare);

  const tolstikov::rectangle_t frameRectBefore = testComposition.getFrameRect();
  const double areaBefore = testComposition.getArea();

  const double x = 4;
  const double y = 6;
  testComposition.move({ x, y });

  const tolstikov::rectangle_t frameRectAfter = testComposition.getFrameRect();
  const double areaAfter = testComposition.getArea();

  BOOST_CHECK_CLOSE(areaAfter, areaBefore, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, x, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, y, DELTA);
}

BOOST_AUTO_TEST_CASE(quadraticIncreaseAreaAndLinearIncreaseFrameRectAfterScaling)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 2, 7 }, 1);
  tolstikov::Shape::pointer testRectangle = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 1, 4 }, 2, 3);
  tolstikov::Shape::pointer testSquare = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 1, 0 }, 5);
  tolstikov::CompositeShape testComposition;

  testComposition.add(testCircle);
  testComposition.add(testRectangle);
  testComposition.add(testSquare);

  const tolstikov::rectangle_t frameRectBefore = testComposition.getFrameRect();
  const double areaBefore = testComposition.getArea();

  const double scaleFactor = 3;
  testComposition.scale(scaleFactor);

  const tolstikov::rectangle_t frameRectAfter = testComposition.getFrameRect();
  const double areaAfter = testComposition.getArea();

  BOOST_CHECK_CLOSE(areaAfter, areaBefore * scaleFactor * scaleFactor, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width * scaleFactor, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height * scaleFactor, DELTA);
}

BOOST_AUTO_TEST_CASE(quadraticDecreaseAreaAndLinearDecreaseFrameRectAfterScaling)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 5, 3 }, 2);
  tolstikov::Shape::pointer testRectangle = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 11, 4 }, 1, 5);
  tolstikov::Shape::pointer testSquare = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 1, 6 }, 1);
  tolstikov::CompositeShape testComposition;

  testComposition.add(testCircle);
  testComposition.add(testRectangle);
  testComposition.add(testSquare);

  const tolstikov::rectangle_t frameRectBefore = testComposition.getFrameRect();
  const double areaBefore = testComposition.getArea();

  const double scaleFactor = 0.3;
  testComposition.scale(scaleFactor);

  const tolstikov::rectangle_t frameRectAfter = testComposition.getFrameRect();
  const double areaAfter = testComposition.getArea();

  BOOST_CHECK_CLOSE(areaAfter, areaBefore * scaleFactor * scaleFactor, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width * scaleFactor, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height * scaleFactor, DELTA);
}

BOOST_AUTO_TEST_CASE(addAndRemoveTest)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 5, 3 }, 2);
  tolstikov::Shape::pointer testRectangle = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 11, 4 }, 1, 5);
  tolstikov::Shape::pointer testSquare = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 1, 6 }, 1);

  tolstikov::CompositeShape testComposition;

  int count = 0;
  const double areaBefore = testComposition.getArea();
  const double areaCircle = testCircle->getArea();
  const double areaRectangle = testRectangle->getArea();
  const double areaSquare = testSquare->getArea();

  BOOST_CHECK_CLOSE(areaBefore, 0, DELTA);
  testComposition.add(testCircle);
  count++;
  BOOST_CHECK_EQUAL(testComposition.getSize(), count);
  testComposition.add(testCircle);
  BOOST_CHECK_EQUAL(testComposition.getSize(), count);
  double area = testComposition.getArea();
  BOOST_CHECK_CLOSE(area, areaBefore + areaCircle, DELTA);

  testComposition.add(testRectangle);
  count++;
  BOOST_CHECK_EQUAL(testComposition.getSize(), count);
  area = testComposition.getArea();
  BOOST_CHECK_CLOSE(area, areaBefore + areaCircle + areaRectangle, DELTA);

  testComposition.add(testSquare);
  count++;
  BOOST_CHECK_EQUAL(testComposition.getSize(), count);
  testComposition.add(testRectangle);
  BOOST_CHECK_EQUAL(testComposition.getSize(), count);
  area = testComposition.getArea();
  BOOST_CHECK_CLOSE(area, areaBefore + areaCircle + areaRectangle + areaSquare, DELTA);

  testComposition.remove(testComposition.getIndex(testRectangle));
  count--;

  area = testComposition.getArea();
  BOOST_CHECK_CLOSE(area, areaBefore + areaCircle + areaSquare, DELTA);
  BOOST_CHECK_EQUAL(testComposition.getSize(), count);

  testComposition.remove(testComposition.getIndex(testCircle));
  count--;
  testComposition.remove(testComposition.getIndex(testSquare));
  count--;

  area = testComposition.getArea();
  BOOST_CHECK_CLOSE(area, 0, DELTA);
  BOOST_CHECK_EQUAL(testComposition.getSize(), count);
}

BOOST_AUTO_TEST_CASE(testCompositeShapeRotation)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 3, 2.2 }, 3);
  tolstikov::Shape::pointer testRectangle = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 4, 5 }, 9, 2);
  tolstikov::CompositeShape testComposition;

  testComposition.add(testCircle);
  testComposition.add(testRectangle);

  const double areaBefore = testComposition.getArea();
  const tolstikov::rectangle_t frameRectBefore = testComposition.getFrameRect();

  double angle = 360;
  testComposition.rotate(angle);

  double areaAfter = testComposition.getArea();
  tolstikov::rectangle_t frameRectAfter = testComposition.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, DELTA);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, DELTA);

  angle = 90;
  testComposition.rotate(angle);

  areaAfter = testComposition.getArea();
  frameRectAfter = testComposition.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.width, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.height, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, DELTA);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, DELTA);

  angle = -450;
  testComposition.rotate(angle);

  areaAfter = testComposition.getArea();
  frameRectAfter = testComposition.getFrameRect();

  BOOST_CHECK_CLOSE(frameRectAfter.height, frameRectBefore.height, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.width, frameRectBefore.width, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.x, frameRectBefore.pos.x, DELTA);
  BOOST_CHECK_CLOSE(frameRectAfter.pos.y, frameRectBefore.pos.y, DELTA);
  BOOST_CHECK_CLOSE(areaAfter, areaBefore, DELTA);
}

BOOST_AUTO_TEST_CASE(exceptInvalidScaleFactor)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 0.5, 1 }, 2);
  tolstikov::CompositeShape testComposition;

  testComposition.add(testCircle);
  const double scaleFactor1 = 0;
  const double scaleFactor2 = -11;
  BOOST_CHECK_THROW(testComposition.scale(scaleFactor1), std::invalid_argument);
  BOOST_CHECK_THROW(testComposition.scale(scaleFactor2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(exceptNullPointer)
{
  tolstikov::CompositeShape testComposition;
  tolstikov::Shape::pointer nullShape;

  BOOST_CHECK_THROW(testComposition.add(nullShape), std::invalid_argument);
  BOOST_CHECK_THROW(testComposition.getIndex(nullShape), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(exceptOutOfRange)
{
  tolstikov::Shape::pointer testCircle = std::make_shared<tolstikov::Circle> (tolstikov::point_t{ 1, 2 }, 4);
  tolstikov::CompositeShape testComposition;

  testComposition.add(testCircle);

  BOOST_CHECK_THROW(testComposition[2], std::out_of_range);
  BOOST_CHECK_THROW(testComposition.remove(13), std::out_of_range);

  tolstikov::Shape::pointer testRectangle = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 0, 0 }, 1, 5);

  BOOST_CHECK_EQUAL(testComposition.getIndex(testRectangle), testComposition.getSize());
  BOOST_CHECK_THROW(testComposition.remove(testComposition.getIndex(testRectangle)), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(exceptLogicError)
{
  tolstikov::CompositeShape testComposition;

  BOOST_CHECK_THROW(testComposition.scale(2.5), std::logic_error);
  BOOST_CHECK_THROW(testComposition.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(testComposition.move(1,4), std::logic_error);
  BOOST_CHECK_THROW(testComposition.move({ 2, 5 }), std::logic_error);
}
BOOST_AUTO_TEST_SUITE_END()
