#include <iterator>
#include <algorithm>

#include "stats.hpp"

int main(int argc, char **)
{
  if (argc != 1) {
    std::cerr << "Wrong number of arguments\n";
    return 1;
  }

  try {
    const stats::collect_stats statistics = std::for_each(std::istream_iterator< int >(std::cin),
                                                          std::istream_iterator< int >(),
                                                          stats::collect_stats());
    if (!std::cin.eof()) {
      std::cerr << "Fail reading data\n";
      return 1;
    }

    std::cout << statistics;

  } catch (const std::exception & e) {
    std::cerr << "error: " << e.what() << "\n";
    return 2;
  }

  return 0;
}
