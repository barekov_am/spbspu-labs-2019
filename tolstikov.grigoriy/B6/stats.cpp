#include "stats.hpp"

#include <limits>

stats::collect_stats::collect_stats():
  max(std::numeric_limits< int >::min()),
  min(std::numeric_limits< int >::max()),
  average(0),
  numOfPositive(0),
  numOfNegative(0),
  sumOfOdd(0),
  sumOfEven(0),
  firstEqualToLast(true),
  first(0),
  count(0)
{
}

void stats::collect_stats::operator ()(int number)
{
  if (count == 0) {
    first = number;
  }

  ++count;

  max = std::max(max, number);
  min = std::min(min, number);

  if (number > 0) {
    ++numOfPositive;
  } else if (number < 0) {
    ++numOfNegative;
  }

  number % 2 ? sumOfOdd += number : sumOfEven += number;

  average = static_cast< double >(sumOfOdd + sumOfEven) / count;

  firstEqualToLast = (first == number);
}

std::ostream & stats::operator <<(std::ostream & out, const collect_stats & stats)
{
  std::ostream::sentry sentry(out);
  if (sentry) {
    if (stats.count == 0) {
      out << "No Data\n";
      return out;
    }
    out << "Max: " << stats.max << '\n'
        << "Min: " << stats.min << '\n'
        << "Mean: " << stats.average << '\n'
        << "Positive: " << stats.numOfPositive << '\n'
        << "Negative: " << stats.numOfNegative << '\n'
        << "Odd Sum: " << stats.sumOfOdd << '\n'
        << "Even Sum: " << stats.sumOfEven << '\n'
        << "First/Last Equal: " << (stats.firstEqualToLast ? "yes\n" : "no\n");
  }

  return out;
}
