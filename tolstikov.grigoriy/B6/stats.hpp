#ifndef STATS_HPP
#define STATS_HPP
#include <iostream>

namespace stats {
  class collect_stats {
  public:
    collect_stats();
    void operator ()(int);
    friend std::ostream & operator <<(std::ostream &, const collect_stats &);

  private:
    int max;
    int min;
    double average;
    size_t numOfPositive;
    size_t numOfNegative;
    long long sumOfOdd;
    long long sumOfEven;
    bool firstEqualToLast;

    int first;
    size_t count;
  };

  std::ostream & operator <<(std::ostream &, const collect_stats &);
}

#endif
