#include <iostream>
#include <list>

void task2()
{
  const size_t maxSize = 20;
  const int minNum = 1;
  const int maxNum = 20;

  std::list< int > intList;

  size_t size = 0;
  int num = 0;

  while(!(std::cin >> num).eof()) {
    if (std::cin.fail()) {
      throw std::ios_base::failure("Fail reading data");
    }

    if ((num > maxNum) || (num < minNum)) {
      throw std::ios_base::failure("Invalid number. Only from 1 to 20 are available.");
    }

    if (size == maxSize) {
      throw std::ios_base::failure("List overflow. Max size is 20.");
    }

    ++size;
    intList.push_back(num);
  }

  if (intList.empty()) {
    std::cout << "\n";
    return;
  }

  std::list< int >::iterator begin = intList.begin();
  std::list< int >::iterator end = intList.end();

  while(begin != end) {
    std::cout << *begin;
    ++begin;

    if (begin == end) {
      break;
    }

    std::cout << " " << *(--end);

    if (begin != end) {
      std::cout << " ";
    }
  }

  std::cout << "\n";
}
