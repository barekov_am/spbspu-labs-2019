#ifndef PARSER_HPP
#define PARSER_HPP

#include <functional>

#include "queue-with-priority.hpp"

class Parser {
public:
  using stringQueue = QueueWithPriority< std::string >;
  using commandExecution = std::function< void(stringQueue &, std::ostream &) >;

  static commandExecution parseCommand(std::string & args);

private:
  static void skipSpaces(std::string &);
  static std::string extractLexeme(std::string &);

  static commandExecution parseAdd(std::string &);
  static commandExecution parseGet(std::string &);
  static commandExecution parseAccelerate(std::string &);
  static const commandExecution invalidCommand;
};

#endif
