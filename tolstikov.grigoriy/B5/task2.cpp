#include <iterator>
#include <iostream>
#include <algorithm>
#include <numeric>

#include "shapes.hpp"

void task2()
{
  using shapeInputIterator = std::istream_iterator< shapes::Shape >;

  shapes::shapeList shapes((shapeInputIterator(std::cin)), shapeInputIterator());

  if (!std::cin.eof()) {
    throw std::ios_base::failure("Fail reading data");
  }

  size_t numOfVertices = std::accumulate(shapes.begin(), shapes.end(), 0, [](size_t sum, const shapes::Shape & sh) {
      return sum += sh.vertices.size(); });

  size_t numOfTriangles = std::count_if(shapes.begin(), shapes.end(), [&](const shapes::Shape & sh) {
      return sh.vertices.size() == shapes::TRIANGLE; });

  size_t numOfSquares = std::count_if(shapes.begin(), shapes.end(), [&](const shapes::Shape & sh) {
      return sh.isSquare(); });

  size_t numOfRectangles = std::count_if(shapes.begin(), shapes.end(), [&](const shapes::Shape & sh) {
      return sh.isRectangle(); });

  std::cout << "Vertices: " << numOfVertices
            << "\nTriangles: " << numOfTriangles
            << "\nSquares: " << numOfSquares
            << "\nRectangles: " << numOfRectangles
            << "\nPoints: ";

  shapes.erase(std::remove_if(shapes.begin(), shapes.end(), [](const shapes::Shape & shape) {
      return shape.vertices.size() == shapes::PENTAGON; }), shapes.end());

  shapes::shape points;
  std::for_each(shapes.begin(), shapes.end(), [&](const shapes::Shape & sh) { points.push_back(sh.vertices[0]); } );

  shapes.sort(shapes::compare_shapes());

  std::copy(points.begin(), points.end(), std::ostream_iterator< point_t >(std::cout, " "));
  std::cout << "\nShapes:\n";
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator< shapes::Shape >(std::cout, "\n"));
}
