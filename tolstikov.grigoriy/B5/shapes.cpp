#include "shapes.hpp"

#include <iterator>
#include <boost/io/ios_state.hpp>

#include "manipulator.hpp"
#include "point.hpp"

bool shapes::Shape::isRectangle() const
{
  if (vertices.size() != shapes::TETRAGON) {
    return false;
  }
  using intPair = std::pair< int, int >;
  intPair side1 = std::make_pair< int, int >(vertices[0].x - vertices[1].x, vertices[0].y - vertices[1].y);
  intPair side2 = std::make_pair< int, int >(vertices[1].x - vertices[2].x, vertices[1].y - vertices[2].y);
  intPair side3 = std::make_pair< int, int >(vertices[2].x - vertices[3].x, vertices[2].y - vertices[3].y);
  intPair side4 = std::make_pair< int, int >(vertices[3].x - vertices[0].x, vertices[3].y - vertices[0].y);

  return (side1.first * side2.first + side1.second * side2.second) == 0
      && (side1.first * side4.first + side1.second * side4.second) == 0
      && (side2.first * side3.first + side2.second * side3.second) == 0;
}

bool shapes::Shape::isSquare() const
{
  if (vertices.size() != shapes::TETRAGON) {
    return false;
  }

  using intPair = std::pair< int, int >;
  intPair diagonal1 = std::make_pair< int, int >(vertices[3].x - vertices[1].x, vertices[3].y - vertices[1].y);
  intPair diagonal2 = std::make_pair< int, int >(vertices[2].x - vertices[0].x, vertices[2].y - vertices[0].y);

  return (this->isRectangle() && (diagonal1.first * diagonal2.first + diagonal1.second * diagonal2.second) == 0);
}

std::istream & shapes::operator >>(std::istream & in, shapes::Shape & shape)
{
  boost::io::ios_flags_saver flags(in);
  in >> std::noskipws;
  std::istream::sentry sentry(in);

  if (sentry) {
    size_t vertices = 0;
    in >> std::ws >> vertices;

    if (!in) {
      return in;
    }

    shapes::shape tmpShape(vertices);
    for (auto & elem : tmpShape) {
      in >> elem;
    }

    if (!in) {
      if (in.eof()) {
        in.clear(in.rdstate() & ~std::ios_base::eofbit);
      }
      return in;
    }

    shape.vertices = tmpShape;
  }

  return in;
}

std::ostream & shapes::operator <<(std::ostream & out, const shapes::Shape & shape)
{
  std::ostream::sentry sentry(out);

  if (out) {
    out << shape.vertices.size() << ' ';
    std::copy(shape.vertices.begin(), shape.vertices.end(), std::ostream_iterator< point_t >(out, " "));
  }

  return out;
}

bool shapes::compare_shapes::operator ()(const Shape & lhs, const Shape & rhs) const
{
  if (lhs.vertices.size() != rhs.vertices.size()) {
    return lhs.vertices.size() < rhs.vertices.size();
  }

  if (lhs.isSquare()) {
    return true;
  }

  if (rhs.isSquare()) {
    return false;
  }

  return !rhs.isRectangle();
}
