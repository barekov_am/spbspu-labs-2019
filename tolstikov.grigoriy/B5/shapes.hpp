#ifndef SHAPES_HPP
#define SHAPES_HPP

#include <list>
#include <vector>
#include "point.hpp"

namespace shapes {
  using shape = std::vector< point_t >;

  enum figures_t {
    TRIANGLE = 3,
    TETRAGON = 4,
    PENTAGON = 5
  };

  struct Shape {
    shape vertices;
    bool isSquare() const;
    bool isRectangle() const;
  };

  std::istream & operator >>(std::istream & in, Shape &);
  std::ostream & operator <<(std::ostream & out, const Shape &);

  using shapeList = std::list< Shape >;

  struct compare_shapes {
    bool operator ()(const Shape & lhs, const Shape & rhs) const;
  };
}
#endif
