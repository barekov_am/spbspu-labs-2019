#include <iterator>
#include <unordered_set>
#include <iostream>

void task1()
{
  using stringInputIter = std::istream_iterator< std::string >;
  std::unordered_set< std::string > words((stringInputIter(std::cin)), stringInputIter());

  if (!std::cin.eof()) {
    throw std::ios_base::failure("Fail reading data");
  }

  std::copy(words.begin(), words.end(), std::ostream_iterator< std::string >(std::cout, "\n"));
}
