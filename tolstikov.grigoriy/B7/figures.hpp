#ifndef FIGURES_HPP
#define FIGURES_HPP

#include <functional>
#include "shape.hpp"

class Circle : public Shape {
public:
  Circle(const point_t &);
  virtual void draw(std::ostream &) const override;
};

class Triangle : public Shape {
public:
  Triangle(const point_t &);
  virtual void draw(std::ostream &) const override;
};

class Square : public Shape {
public:
  Square(const point_t &);
  virtual void draw(std::ostream &) const override;
};

class ShapeFactory {
public:
  friend std::istream & operator >>(std::istream &, ShapeFactory &);
  Shape::sharedPtr create(const point_t &) const;

private:
  std::string figure_;

  struct creator_t {
    const char * shapeName;
    std::function< Shape::sharedPtr(const point_t &) > creation;
  };

  static const creator_t creators[3];
};

#endif
