#include <list>
#include <iostream>
#include <algorithm>
#include <functional>
#include <iterator>
#include "math.h"

void task1()
{
  using doubleInIter = std::istream_iterator< double >;
  using doubleOutIter = std::ostream_iterator< double >;

  std::transform((doubleInIter(std::cin)), doubleInIter(),
      doubleOutIter(std::cout, "\n"), std::bind(std::multiplies< double >(), std::placeholders::_1, M_PI));

  if (!std::cin.eof()) {
    throw std::ios_base::failure("Fail reading data");
  }
}
