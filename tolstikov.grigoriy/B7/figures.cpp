#include "figures.hpp"

#include <algorithm>
#include <boost/io/ios_state.hpp>

#include "manipulator.hpp"

Circle::Circle(const point_t & centre):
  Shape(centre)
{
}

void Circle::draw(std::ostream & out) const
{
  out << "CIRCLE " << centre_;
}

Triangle::Triangle(const point_t & centre):
  Shape(centre)
{
}

void Triangle::draw(std::ostream & out) const
{
  out << "TRIANGLE " << centre_;
}

Square::Square(const point_t & centre):
  Shape(centre)
{
}

void Square::draw(std::ostream & out) const
{
  out << "SQUARE " << centre_;
}

std::istream & operator >>(std::istream & in, Shape::sharedPtr & shape)
{
  boost::io::ios_flags_saver flags(in);
  in >> std::noskipws;
  std::istream::sentry sentry(in);

  if (sentry) {
    ShapeFactory sFactory;
    point_t centre;
    in >> std::ws >> sFactory >> centre;

    if (!in) {
      return in;
    }

    shape = sFactory.create(centre);
  }

  return in;
}

std::istream & operator >>(std::istream & in, ShapeFactory & factory)
{
  boost::io::ios_flags_saver flags(in);
  in >> std::noskipws;
  std::istream::sentry sentry(in);

  if (sentry) {
    std::string figure;

    while (std::isalpha(in.peek())) {
      figure += in.get();
    }

    if (!in) {
      return in;
    }

    if (std::find_if(factory.creators, std::end(factory.creators),[&](auto & creator) {
        return creator.shapeName == figure; }) == std::end(factory.creators)) {

      in.setstate(std::istream::failbit);
      return in;
    }

    factory.figure_ = figure;
  }

  return in;
}

Shape::sharedPtr ShapeFactory::create(const point_t & centre) const
{
  auto creator = std::find_if(creators, std::end(creators),
      [&](auto & creator) { return creator.shapeName == figure_; });

  return creator->creation(centre);
}

const ShapeFactory::creator_t ShapeFactory::creators[3] = {
  { "CIRCLE", [](const point_t & point) { return std::make_shared< Circle >(point); } },
  { "TRIANGLE", [](const point_t & point) { return std::make_shared< Triangle >(point); } },
  { "SQUARE", [](const point_t & point) { return std::make_shared< Square >(point); } }
};
