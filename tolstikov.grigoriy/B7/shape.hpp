#ifndef B7_SHAPE_HPP
#define B7_SHAPE_HPP

#include <memory>
#include "point.hpp"

class Shape {
public:
  using sharedPtr = std::shared_ptr< Shape >;
  using constSharedPtr = std::shared_ptr< const Shape >;

  Shape(const point_t &);
  virtual ~Shape() = default;

  bool isMoreLeft(const Shape::constSharedPtr &) const;
  bool isUpper(const Shape::constSharedPtr &) const;

  virtual void draw(std::ostream &) const = 0;

protected:
  point_t centre_;
};

std::istream & operator >>(std::istream &, Shape::sharedPtr &);
std::ostream & operator <<(std::ostream &, const Shape::constSharedPtr &);

#endif
