#include <iterator>
#include <list>
#include <iostream>

#include "figures.hpp"

void task2() {
  using shapeInputIterator = std::istream_iterator< Shape::sharedPtr >;
  std::list< Shape::sharedPtr > shapes((shapeInputIterator(std::cin)), shapeInputIterator());

  if (!std::cin.eof()) {
    throw std::ios_base::failure("Fail reading data");
  }

  std::cout << "Original:\n";
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator< Shape::constSharedPtr >(std::cout, "\n"));

  shapes.sort(std::bind(&Shape::isMoreLeft, std::placeholders::_1, std::placeholders::_2));
  std::cout << "Left-Right:\n";
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator< Shape::constSharedPtr >(std::cout, "\n"));

  shapes.sort(std::bind(&Shape::isMoreLeft, std::placeholders::_2, std::placeholders::_1));
  std::cout << "Right-Left:\n";
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator< Shape::constSharedPtr >(std::cout, "\n"));

  shapes.sort(std::bind(&Shape::isUpper, std::placeholders::_1, std::placeholders::_2));
  std::cout << "Top-Bottom:\n";
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator< Shape::constSharedPtr >(std::cout, "\n"));

  shapes.sort(std::bind(&Shape::isUpper, std::placeholders::_2, std::placeholders::_1));
  std::cout << "Bottom-Top:\n";
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator< Shape::constSharedPtr >(std::cout, "\n"));
}
