#include "shape.hpp"

Shape::Shape(const point_t & centre):
  centre_(centre)
{
}

bool Shape::isMoreLeft(const Shape::constSharedPtr & shape) const
{
  return centre_.x < shape->centre_.x;
}

bool Shape::isUpper(const Shape::constSharedPtr & shape) const
{
  return centre_.y > shape->centre_.y;
}

std::ostream & operator <<(std::ostream & out, const Shape::constSharedPtr & shape)
{
  shape->draw(out);
  return out;
}
