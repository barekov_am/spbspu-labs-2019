#include "printing-iterator.hpp"

PrintingIterator::PrintingIterator(std::ostream & out, size_t linewidth):
  state_(std::make_shared< iteratorState_t >(out, linewidth))
{
  if (linewidth <= 24) {
    throw std::ios_base::failure("Invalid --line-width parameter\n");
  }
}

PrintingIterator & PrintingIterator::operator *()
{
  return *this;
}

PrintingIterator & PrintingIterator::operator ++()
{
  return *this;
}

PrintingIterator PrintingIterator::operator ++(int)
{
  return *this;
}

PrintingIterator & PrintingIterator::operator =(const token_t & token)
{
  state_->printToken(token);
  return  *this;
}

PrintingIterator::iteratorState_t::iteratorState_t(std::ostream & out, size_t linewidth):
  stream_(out),
  last_(),
  lineData_(),
  linewidth_(linewidth),
  isFirst(true)
{
}

PrintingIterator::iteratorState_t::~iteratorState_t()
{
  if (!lineData_.empty()) {
    stream_ << lineData_ << "\n";
  }
}

void PrintingIterator::iteratorState_t::printToken(const token_t & token)
{
  size_t expectedSize = token.data.size() + lineData_.size();
  size_t transferPoint = 0;

  if (token.tokenType == token_t::tokenType_t::INVALID) {
    stream_.setstate(std::ostream::failbit);
    return;
  }

  if (token.tokenType != token_t::tokenType_t::PUNCTUATION && token.tokenType != token_t::tokenType_t::DASH) {
    transferPoint = lineData_.size();
    ++expectedSize;
  } else if (token.tokenType == token_t::tokenType_t::DASH) {
    transferPoint = lineData_.size() - 1 - last_.data.size();
    ++expectedSize;
  } else {
    transferPoint = lineData_.size() - 1 - last_.data.size();
  }

  if (expectedSize > linewidth_) {
    stream_.write(lineData_.c_str(), transferPoint);
    stream_ << "\n";
    lineData_.erase(0, transferPoint);

    if (std::isblank(lineData_.front())) {
      lineData_.erase(0, 1);
    }
    if (lineData_.empty()) {
      isFirst = true;
    }
  }

  if (token.tokenType != token_t::tokenType_t::PUNCTUATION) {

    if (isFirst) {
      isFirst = false;
      lineData_ += token.data;
    } else {
      lineData_ += ' ' + token.data;
    }

  } else {
    lineData_ += token.data;
  }

  last_ = token;
}

