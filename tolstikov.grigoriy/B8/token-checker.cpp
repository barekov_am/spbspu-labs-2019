#include "token-checker.hpp"

TokenChecker::TokenChecker():
  last_()
{
}

token_t TokenChecker::operator ()(const token_t & token)
{
  if (last_.data.empty()) {
    if (token.tokenType == token_t::tokenType_t::PUNCTUATION || token.tokenType == token_t::tokenType_t::DASH) {
      throw std::ios_base::failure("Text can't start with punctuation");
    }
  }

  if (last_.tokenType == token_t::tokenType_t::PUNCTUATION || last_.tokenType == token_t::tokenType_t::DASH) {
    if (token.tokenType == token_t::tokenType_t::PUNCTUATION) {
      throw std::ios_base::failure("Punctuation can't follow puntuation");
    }
    if (token.tokenType == token_t::tokenType_t::DASH && last_.data != ",") {
      throw std::ios_base::failure("Punctuation before dash is prohibited, except ','");
    }
  }

  if (token.tokenType == token_t::tokenType_t::INVALID) {
    throw std::ios_base::failure("Invalid data");
  }

  if ((token.tokenType == token_t::tokenType_t::WORD || token.tokenType == token_t::tokenType_t::NUMBER)
      && token.data.size() > token_t::maxTokenDataSize) {

    throw std::ios_base::failure("Length of data is more than max");
  }

  last_ = token;
  return token;
}
