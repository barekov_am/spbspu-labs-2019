#include "reading-iterator.hpp"
#include <string>

ReadingIterator::ReadingIterator():
  state_(nullptr)
{
}

ReadingIterator::ReadingIterator(std::istream & in):
  state_(std::make_shared< iteratorState_t >(in))
{
  operator ++();
}

bool ReadingIterator::operator ==(const ReadingIterator & other) const
{
 return state_ == other.state_;
}

bool ReadingIterator::operator !=(const ReadingIterator & other) const
{
  return !(*this == other);
}

ReadingIterator::reference ReadingIterator::operator *() const
{
  return state_->current_;
}

ReadingIterator & ReadingIterator::operator ++()
{
  if (state_->stream_.eof()) {
    state_ = nullptr;
    return *this;
  }

  if (state_->stream_.fail()) {
    throw std::ios_base::failure("Fail stream in opertator ++()");
  }

  state_->current_.data.clear();
  state_->current_.tokenType = token_t::tokenType_t::INVALID;
  state_->current_.column = state_->column_;
  state_->current_.line = state_->line_;

  TokenGuard tokenState;
  readToken(tokenState);

  if (state_->current_.data.empty()) {
    state_ = nullptr;
    return *this;
  }

  if (state_->stream_) {
    restoreState(tokenState);
  }

  return *this;
}

ReadingIterator ReadingIterator::operator ++(int)
{
  ReadingIterator tmp(*this);
  this->operator ++();
  return tmp;
}

void ReadingIterator::readToken(TokenGuard & tokenState)
{
  char tmp = 0;
  state_->stream_.get(tmp);

  while (state_->stream_) {
    ++(state_->column_);

    if (std::isspace(tmp)) {

      if (tmp == '\n') {
        ++(state_->line_);
        state_->column_ = 0;
      }
      if (tmp == '\r') {
        state_->column_ = 0;
      }
      if (!state_->current_.data.empty()) {

        if (state_->current_.tokenType != token_t::tokenType_t::INVALID) {
          state_->stream_.get();
          tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
        }

        return;
      } else {
        state_->current_.column = state_->column_;
        state_->current_.line = state_->line_;
      }

    } else if (std::isalpha(tmp)) {

      switch (state_->current_.tokenType) {
      case (token_t::tokenType_t::INVALID):
        {
          if (state_->current_.data == "--") {
            return;
          }
          if (!state_->current_.data.empty() && state_->current_.data.back() == state_->decimalDelimiter) {
            return;
          }
          state_->current_.tokenType = token_t::tokenType_t::WORD;
          state_->current_.data += tmp;
          tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
          break;
        }
      case (token_t::tokenType_t::WORD):
        {
          state_->current_.data += tmp;
          tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
          break;
        }
      case (token_t::tokenType_t::PUNCTUATION):
      case (token_t::tokenType_t::DASH):
        {
          return;
        }
      default:
        {
          state_->current_.tokenType = token_t::tokenType_t::INVALID;
          tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
          return;
        }
      }

    } else if (std::isdigit(tmp)) {

      switch (state_->current_.tokenType) {
      case (token_t::tokenType_t::INVALID):
        {
          if (state_->current_.data == "--") {
            return;
          }
          if (!state_->current_.data.empty()
              && state_->current_.data.find_first_of(state_->decimalDelimiter) != (state_->current_.data.size() - 1)
              && state_->current_.data.back() == state_->decimalDelimiter) {

            state_->current_.data += tmp;
            tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
            return;
          }

          state_->current_.tokenType = token_t::tokenType_t::NUMBER;
          state_->current_.data += tmp;
          tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
          break;
        }
      case (token_t::tokenType_t::NUMBER):
        {
          state_->current_.data += tmp;
          tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
          break;
        }
      case (token_t::tokenType_t::PUNCTUATION):
        {
          if (state_->current_.data.back() == '+' || state_->current_.data.back() == '-') {
            state_->current_.tokenType = token_t::tokenType_t::NUMBER;
            state_->current_.data += tmp;
            tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
            break;
          }
          return;
        }
      case (token_t::tokenType_t::DASH):
        {
          return;
        }
      default:
        {
          state_->current_.tokenType = token_t::tokenType_t::INVALID;
          tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
          return;
        }
      }

    } else if (std::ispunct(tmp)) {

      switch (state_->current_.tokenType) {
      case (token_t::tokenType_t::INVALID):
        {
          if (state_->current_.data == "--") {
            if (tmp == '-') {
              state_->current_.tokenType = token_t::tokenType_t::DASH;
              state_->current_.data += tmp;
              tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
              break;
            }
            return;
          }
          if (!state_->current_.data.empty() && state_->current_.data.back() == state_->decimalDelimiter) {
            return;
          }
          state_->current_.tokenType = token_t::tokenType_t::PUNCTUATION;
          state_->current_.data += tmp;
          tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
          break;
        }
      case (token_t::tokenType_t::DASH):
        {
          if (tmp == '-') {
            state_->current_.tokenType = token_t::tokenType_t::INVALID;
            tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
          }
          return;
        }
      case (token_t::tokenType_t::NUMBER):
        {
          if (tmp == state_->decimalDelimiter) {
            state_->current_.tokenType = token_t::tokenType_t::INVALID;
            state_->current_.data += tmp;
            break;
          }
          return;
        }
      case (token_t::tokenType_t::WORD):
        {
          if (tmp == '-') {

            if (state_->current_.data.back() != '-') {
              state_->current_.data += tmp;
              tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
              break;
            } else {
              state_->current_.tokenType = token_t::tokenType_t::INVALID;
              tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
            }

          }
          return;
        }
      case (token_t::tokenType_t::PUNCTUATION):
        {
          if (tmp == '-' && state_->current_.data.back() == '-') {
            state_->current_.data += tmp;
            state_->current_.tokenType = token_t::tokenType_t::INVALID;
            tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
            break;
          }
          return;
        }
      }

    } else {
      state_->current_.tokenType = token_t::tokenType_t::INVALID;
      tokenState.setState(token_t{ state_->current_.tokenType, state_->current_.data, state_->line_, state_->column_ });
      return;
    }

    state_->stream_.get(tmp);
  }
}

void ReadingIterator::restoreState(const TokenGuard & tokenState)
{
  state_->stream_.unget();

  if (state_->current_.data.back() == state_->decimalDelimiter
      && state_->current_.data.front() != state_->decimalDelimiter) {

    state_->stream_.putback(state_->current_.data.back());
  }

  state_->current_.tokenType = tokenState.getTokenType();
  state_->current_.data = tokenState.getData();
  state_->column_ = tokenState.getColumn();
  state_->line_ = tokenState.getLine();
}

ReadingIterator::iteratorState_t::iteratorState_t(std::istream & in):
  stream_(in),
  current_(),
  line_(1),
  column_(0),
  decimalDelimiter(std::use_facet< std::numpunct< char > >(in.getloc()).decimal_point())
{
  if (!in) {
    throw std::ios_base::failure("Fail stream in constructor of reading iterator");
  }
}
