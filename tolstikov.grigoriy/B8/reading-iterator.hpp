#ifndef READINGITERATOR_HPP
#define READINGITERATOR_HPP

#include <iterator>
#include <memory>
#include "token.hpp"

class ReadingIterator : public std::iterator< std::input_iterator_tag, token_t > {
public:
  ReadingIterator();
  ReadingIterator(std::istream &);

  bool operator ==(const ReadingIterator & other) const;
  bool operator !=(const ReadingIterator & other) const;

  reference operator *() const;

  ReadingIterator & operator ++();
  ReadingIterator operator ++(int);

private:
  struct iteratorState_t {
    std::istream & stream_;
    token_t current_;
    size_t line_;
    size_t column_;
    char decimalDelimiter;

    iteratorState_t(std::istream &);
  };

  std::shared_ptr< iteratorState_t > state_;

  void readToken(TokenGuard & tokenState);
  void restoreState(const TokenGuard & tokenState);
};


#endif
