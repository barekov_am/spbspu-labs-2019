#ifndef TOKENCHECKER_HPP
#define TOKENCHECKER_HPP

#include "token.hpp"

class TokenChecker {
public:
  TokenChecker();
  token_t operator()(const token_t & token);

private:
  token_t last_;
};

#endif
