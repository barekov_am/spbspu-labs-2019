#ifndef PRINTINGITERATOR_HPP
#define PRINTINGITERATOR_HPP

#include <memory>

#include "token.hpp"

class PrintingIterator : public std::iterator< std::output_iterator_tag, void, void, void, void > {
public:
  PrintingIterator(std::ostream &, size_t linewidth = 40);

  PrintingIterator & operator *();

  PrintingIterator & operator ++();
  PrintingIterator operator ++(int);

  PrintingIterator & operator =(const token_t & token);

private:
  struct iteratorState_t {
    std::ostream & stream_;
    token_t last_;
    std::string lineData_;
    size_t linewidth_;
    bool isFirst;

    iteratorState_t(std::ostream &, size_t);
    ~iteratorState_t();

    void printToken(const token_t & token);
  };

  std::shared_ptr< iteratorState_t > state_;
};

#endif
