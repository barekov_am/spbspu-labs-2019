#include <iostream>
#include <algorithm>
#include "reading-iterator.hpp"
#include "printing-iterator.hpp"
#include "token-checker.hpp"

int main(int argc, char * argv[])
{
  if ((argc != 1) && (argc != 3)) {
    std::cerr << "Wrong number of arguments\n";
    return 1;
  }

  try {
    if (argc == 3) {
      std::string secondArgument = argv[1];

      if (secondArgument != "--line-width") {
        std::cerr << "Invalid argument\n";
        return 1;
      }

      char * ptrEnd = nullptr;
      size_t linewidth = std::strtol(argv[2], &ptrEnd, 10);

      if (*ptrEnd) {
        std::cerr << "Invalid --line-width parameter\n";
        return 1;
      }
      std::transform(ReadingIterator(std::cin), ReadingIterator(), PrintingIterator(std::cout, linewidth), TokenChecker());
    } else {
      std::transform(ReadingIterator(std::cin), ReadingIterator(), PrintingIterator(std::cout), TokenChecker());
    }

    if (!std::cin.eof()) {
      throw std::ios_base::failure("Failed reading data");
    }

    if (!std::cout.good()) {
      throw std::ios_base::failure("Failed printing data");
    }

  } catch (const std::ios_base::failure & e) {
    std::cerr << "error: " << e.what() << "\n";
    return 1;
  } catch (const std::exception & e) {
    std::cerr << "error: " << e.what() << "\n";
    return 2;
  }

  return 0;
}
