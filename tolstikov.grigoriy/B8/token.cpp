#include "token.hpp"

token_t::token_t():
  tokenType(tokenType_t::INVALID),
  data(""),
  line(0),
  column(0)
{
}

token_t::token_t(token_t::tokenType_t type, std::string data, size_t line, size_t column):
  tokenType(type),
  data(data),
  line(line),
  column(column)
{
}

TokenGuard::TokenGuard():
  token_()
{
}

void TokenGuard::setState(const token_t & token)
{
  token_.line = token.line;
  token_.column = token.column;
  token_.tokenType = token.tokenType;
  token_.data = token.data;
}

size_t TokenGuard::getLine() const
{
  return token_.line;
}

size_t TokenGuard::getColumn() const
{
  return token_.column;
}

std::string TokenGuard::getData() const
{
  return token_.data;
}

token_t::tokenType_t TokenGuard::getTokenType() const
{
  return token_.tokenType;
}
