#ifndef TOKEN_HPP
#define TOKEN_HPP

#include <iostream>

struct token_t {
  enum class tokenType_t {
    WORD,
    PUNCTUATION,
    DASH,
    NUMBER,
    INVALID
  };

  static const char maxTokenDataSize = 20;

  tokenType_t tokenType;
  std::string data;
  size_t line;
  size_t column;

  token_t();
  token_t(token_t::tokenType_t type, std::string data, size_t line, size_t column);
};

class TokenGuard {
public:
  TokenGuard();

  void setState(const token_t &);

  size_t getLine() const;
  size_t getColumn() const;
  std::string getData() const;
  token_t::tokenType_t getTokenType() const;

private:
  token_t token_;
};

#endif
