#include "phone-book.hpp"

#include <iostream>

PhoneBook::iterator PhoneBook::begin()
{
  return recordsList_.begin();
}

PhoneBook::iterator PhoneBook::end()
{
  return recordsList_.end();
}

PhoneBook::constIterator PhoneBook::cbegin() const
{
  return recordsList_.cbegin();
}

PhoneBook::constIterator PhoneBook::cend() const
{
  return recordsList_.cend();
}

PhoneBook::iterator PhoneBook::move(iterator position, int steps)
{
  if (steps >= 0) {
    while (std::next(position) != recordsList_.end() && (steps > 0)) {
      position = std::next(position);
      --steps;
    }
  } else {
    while (position != recordsList_.begin() && (steps < 0)) {
      position = std::prev(position);
      ++steps;
    }
  }

  return position;
}

void PhoneBook::show(const iterator position, std::ostream & out) const
{
  out << position->name << ' ' << position->number << '\n';
}

void PhoneBook::pushBack(const record_t &record)
{
  recordsList_.push_back(record);
}

PhoneBook::iterator PhoneBook::insert(const iterator position, const record_t & record)
{
  return recordsList_.insert(position, record);
}

PhoneBook::iterator PhoneBook::erase(iterator position)
{
  return recordsList_.erase(position);
}

PhoneBook::iterator PhoneBook::change(iterator position, const record_t & record)
{
  iterator pos = recordsList_.erase(position);
  return recordsList_.insert(pos, record);
}

size_t PhoneBook::size() const
{
  return recordsList_.size();
}

bool PhoneBook::empty() const
{
  return recordsList_.empty();
}
