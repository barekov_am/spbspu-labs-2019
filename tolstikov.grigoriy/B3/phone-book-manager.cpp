#include "phone-book-manager.hpp"
#include <ostream>
#include "algorithm"

const static char * empty = "<EMPTY>\n";

PhoneBookManager::PhoneBookManager()
{
  bookmarks_["current"] = records_.end();
}

void PhoneBookManager::add(const PhoneBook::record_t & record)
{
  records_.pushBack(record);
  if (std::next(records_.begin()) == records_.end()) {
    rebaseBookmarks();
  }
}

bool PhoneBookManager::store(const std::string & bookmark, const std::string & newBookmark, std::ostream & out)
{
  auto bookmarkIter = bookmarks_.find(bookmark);
  if (bookmarkIter == bookmarks_.end()) {
    return false;
  }

  if (records_.empty()) {
    out << empty;
    return true;
  }

  if (bookmarkIter->second == records_.end()) {
    return false;
  }

  bookmarks_[newBookmark] = bookmarkIter->second;
  return true;
}

bool PhoneBookManager::insert(const insertPosition_t position, const std::string & bookmark, const PhoneBook::record_t & record)
{
  auto bookmarkIter = bookmarks_.find(bookmark);
  if (bookmarkIter == bookmarks_.end()) {
    return false;
  }

  if (bookmarkIter->second == records_.end()) {
    add(record);
    return true;
  }

  auto pos = (position == insertPosition_t::before) ? bookmarkIter->second : std::next(bookmarkIter->second);

  if (pos == records_.end()) {
    add(record);
    return true;
  }

  records_.insert(pos, record);
  return true;
}

bool PhoneBookManager::remove(const std::string & bookmark, std::ostream & out)
{
  auto bookmarkIter = bookmarks_.find(bookmark);
  if (bookmarkIter == bookmarks_.end()) {
    return false;
  }

  if (records_.empty()) {
    out << empty;
    return true;
  }

  if (bookmarkIter->second == records_.end()) {
    return false;
  }

  auto deletePos = bookmarkIter->second;

  std::for_each(bookmarks_.begin(), bookmarks_.end(), [&](auto & pbm) {
      if (pbm.second == deletePos) {
        pbm.second = std::next(pbm.second);
      }
  });

  records_.erase(deletePos);
  rebaseBookmarks();

  return true;
}

bool PhoneBookManager::show(const std::string & bookmark, std::ostream & out) const
{
  auto bookmarkIter = bookmarks_.find(bookmark);
  if (bookmarkIter == bookmarks_.end()) {
    return false;
  }

  if (records_.empty()) {
    out << empty;
    return true;
  }

  if (bookmarkIter->second == records_.cend()) {
    return false;
  }

  records_.show(bookmarkIter->second, out);
  return true;
}

bool PhoneBookManager::move(const std::string & bookmark, int steps, std::ostream & out)
{
  auto bookmarkIter = bookmarks_.find(bookmark);
  if (bookmarkIter == bookmarks_.end()) {
    return false;
  }

  if (records_.empty()) {
    out << empty;
    return true;
  }

  if (bookmarkIter->second == records_.end()) {
    return false;
  }

  bookmarks_[bookmark] = records_.move(bookmarkIter->second, steps);
  return true;
}

bool PhoneBookManager::move(const std::string & bookmark, const movePosition_t movePosition, std::ostream & out)
{
  auto bookmarkIter = bookmarks_.find(bookmark);
  if (bookmarkIter == bookmarks_.end()) {
    return false;
  }

  if (records_.empty()) {
    out << empty;
    return true;
  }

  auto position = (movePosition == movePosition_t::first) ? records_.begin() : std::prev(records_.end());

  bookmarkIter->second = position;
  return true;
}

void PhoneBookManager::rebaseBookmarks()
{
  if (records_.empty()) {
    std::for_each(bookmarks_.begin(), bookmarks_.end(), [&](auto & pbm) {
        pbm.second = records_.end();
    });
    return;
  }

  std::for_each(bookmarks_.begin(), bookmarks_.end(), [&](auto & pbm) {
      if (pbm.second == records_.end()) {
        pbm.second = std::prev(records_.end());
      }
  });
}

PhoneBook::iterator PhoneBookManager::prev(PhoneBook::iterator position)
{
  if (position == records_.begin()) {
    return position;
  }
  return std::prev(position);
}

PhoneBook::iterator PhoneBookManager::next(PhoneBook::iterator position)
{
  if (std::next(position) == records_.end()) {
    return position;
  }
  return std::next(position);
}
