#ifndef PARSER_HPP
#define PARSER_HPP

#include <functional>
#include <ostream>
#include "phone-book-manager.hpp"

class Parser {
public:
  using commandExecution = std::function< void(PhoneBookManager &, std::ostream &) >;
  static commandExecution parseCommand(std::string & args);
private:
  static void skipSpaces(std::string &);

  static std::string extractLexeme(std::string &);
  static std::string extractNumber(std::string &);
  static std::string extractName(std::string &);
  static std::string extractBookmark(std::string &);

  static commandExecution parseAdd(std::string & args);
  static commandExecution parseStore(std::string & args);
  static commandExecution parseInsert(std::string & args);
  static commandExecution parseDelete(std::string & args);
  static commandExecution parseShow(std::string & args);
  static commandExecution parseMove(std::string & args);

  static const commandExecution invalidCommand;
};

#endif
