#ifndef FACTORIAL_HPP
#define FACTORIAL_HPP

#include <iterator>
#include <limits>

class FactorialContainer {
public:
  using valueType = size_t;
  using sizeType = size_t;

  class FactorialIterator;
  using reverseIterator = std::reverse_iterator< FactorialIterator >;

  FactorialContainer(sizeType end);
  FactorialContainer(sizeType begin, sizeType end);

  FactorialIterator begin() const;
  FactorialIterator end() const;

  FactorialIterator cbegin() const;
  FactorialIterator cend() const;

  reverseIterator rbegin() const;
  reverseIterator rend() const;

private:
  sizeType begin_;
  sizeType end_;
  valueType beginValue_;
  valueType endValue_;

  static constexpr FactorialContainer::sizeType getMaxIndex();
};

class FactorialContainer::FactorialIterator :
    public std::iterator< std::bidirectional_iterator_tag,
                          FactorialContainer::valueType,
                          std::ptrdiff_t,
                          FactorialContainer::valueType *,
                          FactorialContainer::valueType > {
public:
  bool operator ==(const FactorialIterator & rhs) const;
  bool operator !=(const FactorialIterator & rhs) const;

  value_type operator *() const;

  FactorialIterator & operator ++();
  FactorialIterator operator ++(int);
  FactorialIterator & operator --();
  FactorialIterator operator --(int);

protected:
  FactorialIterator(size_t index, size_t value);

private:
  friend class FactorialContainer;
  sizeType index_;
  value_type value_;
};

#endif
