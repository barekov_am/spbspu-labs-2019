#ifndef PHONEBOOKMANAGER_HPP
#define PHONEBOOKANAGER_HPP

#include <map>
#include "phone-book.hpp"

class PhoneBookManager {
public:
  enum class insertPosition_t {
    before,
    after
  };

  enum class movePosition_t {
    first,
    last
  };

  using bookmarks = std::map< std::string, PhoneBook::iterator >;

  PhoneBookManager();

  void add(const PhoneBook::record_t & record);

  bool store(const std::string & bookmark, const std::string & newBookmark, std::ostream & out);
  bool insert(const insertPosition_t position, const std::string & bookmark, const PhoneBook::record_t & record);
  bool remove(const std::string & bookmark, std::ostream & out);
  bool show(const std::string & bookmark, std::ostream & out) const;

  bool move(const std::string & bookmark, int steps, std::ostream & out);
  bool move(const std::string & bookmark, const movePosition_t movePosition, std::ostream & out);
  
private:
  bookmarks bookmarks_;
  PhoneBook records_;

  void rebaseBookmarks();
  PhoneBook::iterator prev(PhoneBook::iterator);
  PhoneBook::iterator next(PhoneBook::iterator);
};



#endif
