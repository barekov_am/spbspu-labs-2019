#include <iostream>

#include "parser.hpp"

void task1()
{
  PhoneBookManager pbManager;
  std::string line;

  while (getline(std::cin, line)) {
    if (std::cin.fail() && !std::cin.eof()) {
      throw std::ios_base::failure("Fail reading data");
    }

    Parser::parseCommand(line)(pbManager, std::cout);
  }
}
