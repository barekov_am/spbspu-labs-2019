#include "factorial.hpp"

constexpr FactorialContainer::sizeType FactorialContainer::getMaxIndex()
{
  sizeType n = 0;
  valueType fact = 1;
  while (fact < (std::numeric_limits< valueType >::max() / ++n)) {
    fact *= n;
  }
  return n - 2;
}

FactorialContainer::FactorialContainer(size_t end):
  FactorialContainer(1, end)
{
}

FactorialContainer::FactorialContainer(size_t begin, size_t end):
  begin_(begin),
  end_(end)
{
  if (begin > end) {
    throw std::invalid_argument("Index of end must be not less then index of begin");
  }

  constexpr auto maxIndex = getMaxIndex();
  if (maxIndex < end) {
    throw std::overflow_error("Overflow. Index of end is too much.");
  }

  size_t n = 0;
  size_t fact = 1;
  /*(end + 1) because reverse_iterator needs `end` as `rbegin` element*/
  while (n != (end + 1)) {
    if (n == begin) {
      beginValue_ = fact;
    }
    fact *= ++n;
  }
  endValue_ = fact;
}

FactorialContainer::FactorialIterator FactorialContainer::begin() const
{
  return FactorialIterator(begin_, beginValue_);
}

FactorialContainer::FactorialIterator FactorialContainer::end() const
{
  return FactorialIterator(end_ + 1,  endValue_);
}

FactorialContainer::FactorialIterator FactorialContainer::cbegin() const
{
  return FactorialIterator(begin_, beginValue_);
}

FactorialContainer::FactorialIterator FactorialContainer::cend() const
{
  return FactorialIterator(end_ + 1,  endValue_);
}

FactorialContainer::reverseIterator FactorialContainer::rbegin() const
{
  return FactorialContainer::reverseIterator(cend());
}

FactorialContainer::reverseIterator FactorialContainer::rend() const
{
  return FactorialContainer::reverseIterator(cbegin());
}

bool FactorialContainer::FactorialIterator::operator ==(const FactorialIterator & rhs) const
{
  return (index_ == rhs.index_);
}

bool FactorialContainer::FactorialIterator::operator !=(const FactorialIterator & rhs) const
{
  return !(*this == rhs);
}

FactorialContainer::FactorialIterator::value_type FactorialContainer::FactorialIterator::operator *() const
{
  return value_;
}

FactorialContainer::FactorialIterator & FactorialContainer::FactorialIterator::operator ++()
{
  value_ *= ++index_;
  return *this;
}

FactorialContainer::FactorialIterator FactorialContainer::FactorialIterator::operator ++(int)
{
  FactorialIterator tmp(index_, value_);
  operator ++();
  return tmp;
}

FactorialContainer::FactorialIterator & FactorialContainer::FactorialIterator::operator --()
{
  if (index_ == 0) {
    throw std::runtime_error("Divide by zero exception.");
  }
  value_ /= index_--;
  return *this;
}

FactorialContainer::FactorialIterator FactorialContainer::FactorialIterator::operator --(int)
{
  FactorialIterator tmp(index_, value_);
  operator --();
  return tmp;
}

FactorialContainer::FactorialIterator::FactorialIterator(size_t index, size_t value):
  index_(index),
  value_(value)
{
};
