#include <algorithm>
#include <iostream>
#include "factorial.hpp"

void task2()
{
  FactorialContainer container(10);

  std::copy(container.begin(), container.end(),
      std::ostream_iterator< size_t >(std::cout, " "));
  std::cout << "\n";

  std::copy(container.rbegin(), container.rend(),
      std::ostream_iterator< size_t >(std::cout, " "));
  std::cout << "\n";
}
