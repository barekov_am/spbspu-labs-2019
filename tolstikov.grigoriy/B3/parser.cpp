#include "parser.hpp"

#include <algorithm>

const Parser::commandExecution Parser::invalidCommand = [](PhoneBookManager &,
    std::ostream & out) { out << "<INVALID COMMAND>\n"; };

const static char * invalidBookmark = "<INVALID BOOKMARK>\n";

Parser::commandExecution Parser::parseCommand(std::string & args)
{
  using parserExecution = std::function< Parser::commandExecution(std::string &) >;

  struct parsers_t {
    const char * name;
    parserExecution execution;
  };

  static const parsers_t parsers[] {
    { "add", &parseAdd },
    { "store", &parseStore },
    { "insert", &parseInsert },
    { "delete", &parseDelete },
    { "show", &parseShow },
    { "move", &parseMove}
  };

  std::string parserWord = extractLexeme(args);

  auto parser = std::find_if(parsers, std::end(parsers),
      [&](const parsers_t & parser) { return parser.name == parserWord; });

  if (parser == std::end(parsers)) {
    return invalidCommand;
  } else {
    return parser->execution(args);
  }
}

void Parser::skipSpaces(std::string & str)
{
  size_t numws = 0;
  while (numws != str.length() && isblank(str[numws])) {
    ++numws;
  }

  str.erase(0, numws);
}

std::string Parser::extractLexeme(std::string & str)
{
  if (str.empty()) {
    return "";
  }

  skipSpaces(str);

  size_t lexemeLength = 0;

  while (lexemeLength != str.length() && !isblank(str[lexemeLength])) {
    ++lexemeLength;
  }

  std::string lexeme = str.substr(0, lexemeLength);
  str.erase(0, lexemeLength);
  skipSpaces(str);

  return lexeme;
}

std::string Parser::extractNumber(std::string & str)
{
  std::string number = extractLexeme(str);

  auto iter = std::find_if(number.begin(), number.end(), [&](auto digit) { return !isdigit(digit); });

  if (iter != number.end()) {
    return "";
  }

  return number;
}

std::string Parser::extractName(std::string & str)
{
  skipSpaces(str);

  if (str.front() != '\"') {
    return "";
  }

  str.erase(0,1);
  size_t pos = 0;

  while (pos < str.length() && str[pos] != '\"') {
    if (str[pos] == '\\') {
      if ((pos + 1 < str.length()) && (str[pos + 1] == '\"') && (pos + 2 < str.length())) {
        ++pos;
      } else {
        return "";
      }
    }
    ++pos;
  }

  if (pos == str.length()) {
    return "";
  }

  str.erase(pos, 1);

  std::string name = str.substr(0, pos);
  str.erase(0, pos);
  name.erase(std::remove(name.begin(), name.end(), '\\'), name.end());
  skipSpaces(str);

  return name;
}

std::string Parser::extractBookmark(std::string & str)
{
  std::string bookmark = extractLexeme(str);

  auto iter = std::find_if(bookmark.begin(), bookmark.end(),
      [&](auto ch) { return !isalnum(ch) && ch != '-'; });

  if (iter != bookmark.end()) {
    return "";
  }

  return bookmark;
}

Parser::commandExecution Parser::parseAdd(std::string & args)
{
  std::string number = extractNumber(args);

  if (number.empty()) {
    return invalidCommand;
  }

  std::string name = extractName(args);

   if (name.empty() || !args.empty()) {
    return invalidCommand;
  }

  PhoneBook::record_t record = { number, name };
  
  return [record](PhoneBookManager & pbm, std::ostream &) { pbm.add(record); };
}

Parser::commandExecution Parser::parseStore(std::string & args)
{
  std::string oldBookmark = extractBookmark(args);

  if (oldBookmark.empty()) {
    return invalidCommand;
  }

  std::string newBookmark = extractBookmark(args);

  if (newBookmark.empty() || !args.empty()) {
    return invalidCommand;
  }

  return [oldBookmark, newBookmark](PhoneBookManager & pbm, std::ostream & out) {
      if (!pbm.store(oldBookmark, newBookmark, out)) {
        out << invalidBookmark;
      }
  };
}

Parser::commandExecution Parser::parseInsert(std::string & args)
{
  using insertPosition = PhoneBookManager::insertPosition_t;

  struct position_t {
    const char * word;
    insertPosition position;
  };

  static const position_t positions[] {
    { "before", insertPosition::before },
    { "after", insertPosition::after }
  };

  std::string positionWord = extractLexeme(args);

  auto position = std::find_if(positions, std::end(positions),
    [&](const position_t & position) { return position.word == positionWord; });

  if (position == std::end(positions)) {
    return invalidCommand;
  }

  std::string bookmark = extractBookmark(args);

  if (bookmark.empty()) {
    return invalidCommand;
  }

  std::string number = extractNumber(args);

  if (number.empty()) {
    return invalidCommand;
  }

  std::string name = extractName(args);

  if (name.empty() || !args.empty()) {
    return invalidCommand;
  }

  PhoneBook::record_t record = { number, name };

  return [position, bookmark, record](PhoneBookManager & pbm, std::ostream & out) {
      if (!pbm.insert(position->position, bookmark, record)) {
        out << invalidBookmark;
      }
  };
}

Parser::commandExecution Parser::parseDelete(std::string & args)
{
  std::string bookmark = extractBookmark(args);

  if (bookmark.empty() || !args.empty()) {
    return invalidCommand;
  }

  return [bookmark](PhoneBookManager & pbm, std::ostream & out) {
      if (!pbm.remove(bookmark, out)) {
        out << invalidBookmark;
      }
  };
}

Parser::commandExecution Parser::parseShow(std::string & args)
{
  std::string bookmark = extractBookmark(args);

  if (bookmark.empty() || !args.empty()) {
    return invalidCommand;
  }

  return [bookmark](PhoneBookManager & pbm, std::ostream & out) {
      if (!pbm.show(bookmark, out)) {
        out << invalidBookmark;
      }
  };
}

Parser::commandExecution Parser::parseMove(std::string & args)
{
  using movePosition = PhoneBookManager::movePosition_t;

  struct position_t {
    const char * word;
    movePosition position;
  };

  static const position_t positions[] {
    { "first", movePosition::first },
    { "last", movePosition::last }
  };

  std::string bookmark = extractBookmark(args);

  if (bookmark.empty()) {
    return invalidCommand;
  }

  std::string moveArg = extractLexeme(args);

  if (moveArg.empty() || !args.empty()) {
    return invalidCommand;
  }

  auto position = std::find_if(positions, std::end(positions),
    [&](const position_t & position) { return position.word == moveArg; });

  if (position == std::end(positions)) {
    char * ptrEnd = nullptr;
    int steps = std::strtol(moveArg.c_str(), &ptrEnd, 10);

    if (*ptrEnd || steps == 0) {
      return [](PhoneBookManager &, std::ostream & out) { out << "<INVALID STEP>\n"; };
    }

    return [bookmark, steps](PhoneBookManager & pbm, std::ostream & out) {
        if(!pbm.move(bookmark, steps, out)) {
          out << invalidBookmark;
        }
    };
  }

  return [bookmark, position](PhoneBookManager & pbm, std::ostream & out) {
      if (!pbm.move(bookmark, position->position, out)) {
        out << invalidBookmark;
      }
  };
}
