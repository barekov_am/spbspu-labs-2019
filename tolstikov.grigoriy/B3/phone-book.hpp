#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <list>
#include <string>


class PhoneBook {
public:
  struct record_t {
    std::string name;
    std::string number;
  };

  using recordList = std::list < record_t >;
  using iterator = recordList::iterator;
  using constIterator = recordList::const_iterator;

  iterator begin();
  iterator end();

  constIterator cbegin() const;
  constIterator cend() const;

  iterator move(iterator position, int steps);
  
  void show(const iterator position, std::ostream & out) const;
  void pushBack(const record_t & record);
   
  iterator insert(const iterator position, const record_t & record);
  iterator erase(iterator position);
  iterator change(iterator position, const record_t & record);
  
  size_t size() const;
  bool empty() const;

private:
  recordList recordsList_;
};

#endif
