#include <iostream>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
#include "partition.hpp"

void printInfo(const tolstikov::Shape::pointer & shapePointer)
{
  if (!shapePointer) {
    throw std::invalid_argument("Pointer can't be null!");
  }

  tolstikov::rectangle_t frameRect = shapePointer->getFrameRect();
  std::cout << "Area is " << shapePointer->getArea() << ";\nCentre is [" << frameRect.pos.x << ", ";
  std::cout << frameRect.pos.y << "];\n";
  std::cout << "Width of frame rectangle is " << frameRect.width << ", height is " << frameRect.height << ";\n";
}

int main()
{
  std::cout << "\n-----------------COMPOSITE_SHAPE---------------\n";
  /*
    Circle with centre at point [1, 5] and radius 4
  */
  tolstikov::Shape::pointer circlePtr = std::make_shared<tolstikov::Circle>(tolstikov::point_t { 1, 5 }, 4);
  /*
    Rectangle with centre at point [3, -1], width 5 and height 4
  */
  tolstikov::Shape::pointer rectanglePtr = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 3, -1 }, 5, 4);
  /*
    Square with centre at point [6, 5] and side 4
  */
  tolstikov::Shape::pointer squarePtr = std::make_shared<tolstikov::Rectangle>(tolstikov::point_t { 6, 5 }, 4);
  tolstikov::CompositeShape composition;

  std::cout << "\n--------------------ADD_NEW--------------------\n";
  composition.add(circlePtr);
  std::cout << "\nNew element of composite shape:\n";
  printInfo(composition[composition.getIndex(circlePtr)]);

  std::cout << "\n-----------------COMPOSITE_SHAPE---------------\n";
  std::cout << "\nNumber of shapes in composite shape : " << composition.getSize() << ";\n";
  printInfo(std::make_shared<tolstikov::CompositeShape>(composition));

  std::cout << "\nScale composite shape:\n";
  composition.scale(2);
  printInfo(std::make_shared<tolstikov::CompositeShape>(composition));

  std::cout << "\nReturn:\n";
  composition.scale(0.5);
  printInfo(std::make_shared<tolstikov::CompositeShape>(composition));

  std::cout << "\n--------------------ADD_NEW--------------------\n";
  composition.add(rectanglePtr);
  std::cout << "\nNew element of composite shape:\n";
  printInfo(composition[composition.getIndex(rectanglePtr)]);

  std::cout << "\n-----------------COMPOSITE_SHAPE---------------\n";
  std::cout << "\nNumber of shapes in composite shape : " << composition.getSize() << ";\n";
  printInfo(std::make_shared<tolstikov::CompositeShape>(composition));

  std::cout << "\n--------------------ADD_NEW--------------------\n";
  composition.add(squarePtr);
  std::cout << "\nNew element of composite shape:\n";
  printInfo(composition[composition.getIndex(squarePtr)]);

  std::cout << "\n-----------------COMPOSITE_SHAPE---------------\n";
  std::cout << "\nNumber of shapes in composite shape : " << composition.getSize() << ";\n";
  printInfo(std::make_shared<tolstikov::CompositeShape>(composition));

  std::cout << "\n--------------------ADD_NEW--------------------\n";
  std::cout << "\nTry to add elements again.";
  composition.add(squarePtr);
  composition.add(circlePtr);
  composition.add(rectanglePtr);

  std::cout << "\nNumber of shapes in composite shape : " << composition.getSize() << ";\n";

  std::cout << "\n-----------------COMPOSITE_SHAPE---------------\n";
  std::cout << "\nRotate composite shape on 90 degrees:\n";
  composition.rotate(90);
  printInfo(std::make_shared<tolstikov::CompositeShape>(composition));

  std::cout << "\nReturn:\n";
  composition.rotate(-450);
  printInfo(std::make_shared<tolstikov::CompositeShape>(composition));

  std::cout << "\nMove composite shape by value:\n";
  composition.move(2,3);
  printInfo(std::make_shared<tolstikov::CompositeShape>(composition));

  std::cout << "\nMove composite to the point:\n";
  composition.move({ 0, 0 });
  printInfo(std::make_shared<tolstikov::CompositeShape>(composition));

  std::cout << "\n-------------------PARTITION-------------------\n";
  tolstikov::Matrix matrix = tolstikov::part(composition);

  std::cout << "\nThere are " << matrix.getRows() << " layers and " << matrix.getColumns() << " columns;\n";

  for (size_t i = 0; i < matrix.getRows(); i++) {
    for (size_t j = 0; j < matrix.getLayerSize(i); j++) {
      std::cout << "\nLayer " << i << ";\n" << "Figure " << j << ";\n";
      std::cout << "Centre is [";
      std::cout << matrix[i][j]->getFrameRect().pos.x << ", " << matrix[i][j]->getFrameRect().pos.y << "];\n";
    }
  }

  std::cout << "\nRemove rectangle from composite shape.\n";
  composition.remove(composition.getIndex(rectanglePtr));
  std::cout << "\n-----------------COMPOSITE_SHAPE---------------\n";
  std::cout << "\nNumber of shapes in composite shape : " << composition.getSize() << ";\n";
  printInfo(std::make_shared<tolstikov::CompositeShape>(composition));

  std::cout << "\nRemove first element from composite shape.\n";
  composition.remove(0);
  std::cout << "\n-----------------COMPOSITE_SHAPE---------------\n";
  std::cout << "\nNumber of shapes in composite shape : " << composition.getSize() << ";\n";
  printInfo(std::make_shared<tolstikov::CompositeShape>(composition));

  return 0;
}
