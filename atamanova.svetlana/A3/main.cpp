#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

void printFrameRect(const atamanova::Shape& shape)
{
  atamanova::rectangle_t frameRect = shape.getFrameRect();
  std::cout << frameRect.pos.x << " "
      << frameRect.pos.y << " "
      << frameRect.width << " "
      << frameRect.height << std::endl;
}

int main()
{
  atamanova::Circle circle1({4.0, 5.0}, 6.0);
  circle1.move(2.0, 2.0);
  std::cout << circle1.getArea() << std::endl;

  atamanova::Circle circle2({5, 6}, 7);
  circle2.move({3.0, 3.0});

  atamanova::Rectangle rect1({1.0, 2.0}, 4.0, 5.0);
  rect1.move({5.0, 5.0});
  std::cout << rect1.getArea() << std::endl;

  atamanova::Rectangle rect2({2.0, 3.0}, 5.0, 6.0);
  rect2.move(2.0, 2.0);

  circle2.scale(2.0);
  rect2.scale(3.0);

  printFrameRect(circle2);
  printFrameRect(rect2);

  atamanova::CompositeShape compShape;
  compShape.add(std::make_shared<atamanova::Circle>(circle1));
  compShape.add(std::make_shared<atamanova::Circle>(circle2));
  std::cout << "Add rectangle" << std::endl;
  printFrameRect(compShape);

  compShape.move({ 5.0, 5.0 });
  std::cout << "After moving to point" << std::endl;
  printFrameRect(compShape);

  compShape.move(6.0, 6.0);
  std::cout << "After moving" << std::endl;
  printFrameRect(compShape);

  compShape.scale(2.0);
  std::cout << "After scaling" << std::endl;
  printFrameRect(compShape);

  compShape.remove(2);
  std::cout << "After deleting third" << std::endl;
  printFrameRect(compShape);

  return 0;
}
