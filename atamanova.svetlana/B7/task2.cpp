#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>

#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"

std::shared_ptr<Shape> readShape(std::string &line);

void printShapes(const std::vector<std::shared_ptr<Shape>> &shapes);

void task2()
{
  std::string line;
  std::vector<std::shared_ptr<Shape>> shapes;
  while (std::getline(std::cin, line))
  {
    auto predicate = [](const char sym) { return std::isspace(sym); };
    line.erase(std::remove_if(line.begin(), line.end(), predicate), line.end());
    if (line.empty())
    {
      continue;
    }
    shapes.push_back(readShape(line));
  }

  std::cout << "Original:\n";
  printShapes(shapes);

  std::cout << "Left-Right:\n";

  auto leftRightSort = [](const std::shared_ptr<Shape> &lhs, const std::shared_ptr<Shape> &rhs)
  {
    return lhs->isMoreLeft(rhs.get());
  };

  std::sort(shapes.begin(), shapes.end(), leftRightSort);
  printShapes(shapes);

  std::cout << "Right-Left:\n";

  auto rightLeftSort = [](const std::shared_ptr<Shape> &lhs, const std::shared_ptr<Shape> &rhs)
  {
    return !lhs->isMoreLeft(rhs.get());
  };

  std::sort(shapes.begin(), shapes.end(), rightLeftSort);
  printShapes(shapes);

  std::cout << "Top-Bottom:\n";

  auto topBottomSort = [](const std::shared_ptr<Shape> &lhs, const std::shared_ptr<Shape> &rhs)
  {
    return lhs->isUpper(rhs.get());
  };

  std::sort(shapes.begin(), shapes.end(), topBottomSort);
  printShapes(shapes);

  std::cout << "Bottom-Top:\n";

  auto bottomTopSort = [](const std::shared_ptr<Shape> &lhs, const std::shared_ptr<Shape> &rhs)
  {
    return !lhs->isUpper(rhs.get());
  };

  std::sort(shapes.begin(), shapes.end(), bottomTopSort);
  printShapes(shapes);
}
