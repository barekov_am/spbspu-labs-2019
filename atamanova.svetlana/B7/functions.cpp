#include <iostream>
#include <string>
#include <memory>
#include <vector>
#include <algorithm>

#include "circle.hpp"
#include "square.hpp"
#include "triangle.hpp"

std::shared_ptr<Shape> readShape(std::string &line)
{
  size_t openBracket = line.find_first_of('(');

  std::string name = line.substr(0, openBracket);
  line.erase(0, openBracket);

  if (line.empty())
  {
    throw std::invalid_argument("Incorrect center point declaration");
  }

  openBracket = line.find_first_of('(');
  size_t semicolon = line.find_first_of(';');
  size_t closeBracket = line.find_first_of(')');

  if ((openBracket != 0) || (semicolon == std::string::npos) || (closeBracket == std::string::npos)
      || !((openBracket < semicolon) && (closeBracket > semicolon)))
  {
    throw std::invalid_argument("Incorrect point declaration");
  }

  int x = std::stoi(line.substr(1, semicolon - 1));
  int y = std::stoi(line.substr(semicolon + 1, closeBracket - semicolon - 1));

  point_t point = { x, y };

  if (name == "CIRCLE")
  {
    return std::make_shared<Circle>(Circle(point));
  }
  if (name == "TRIANGLE")
  {
    return std::make_shared<Triangle>(Triangle(point));
  }
  if (name == "SQUARE")
  {
    return std::make_shared<Square>(Square(point));
  }

  throw std::invalid_argument("Incorrect shape name");
}

void printShapes(const std::vector<std::shared_ptr<Shape>> &shapes)
{
  auto predicate = [](const std::shared_ptr<Shape> &shape) { shape->draw(std::cout); };
  std::for_each(shapes.begin(), shapes.end(), predicate);
}
