#include <algorithm>
#include <iostream>
#include <iterator>

#include "functor.hpp"

void task(std::istream& istream)
{
  Functor func;
  func = std::for_each(std::istream_iterator<int>(istream), std::istream_iterator<int>(), func);

  if (!istream.eof())
  {
    throw std::ios::failure("Invalid input");
  }

  if (func.isEmpty())
  {
    std::cout << "No Data\n";
  }
  else
  {
    std::cout << "Max: " << func.getMax() << "\n";
    std::cout << "Min: " << func.getMin() << "\n";
    std::cout << "Mean: " << std::fixed << func.getAverageNumber() << "\n";
    std::cout << "Positive: " << func.getNumOfPositive() << "\n";
    std::cout << "Negative: " << func.getNumOfNegative() << "\n";
    std::cout << "Odd Sum: " << func.getOddSum() << "\n";
    std::cout << "Even Sum: " << func.getEvenSum() << "\n";
    std::cout << "First/Last Equal: ";

    if (func.isFirstEqualLast())
    {
      std::cout << "yes\n";
    }
    else
    {
      std::cout << "no\n";
    }
  }
}
