#include <iostream>
#include "data_struct.hpp"

DataStruct readData()
{
  std::string line;
  std::getline(std::cin, line);

  int keys[2] = {0};
  for (int i = 0; i < 2; i++)
  {
    size_t comma = line.find_first_of(',');
    if (comma == std::string::npos)
    {
      throw std::invalid_argument("Wrong data.");
    }

    keys[i] = std::stoi(line.substr(0, comma));
    if (std::abs(keys[i]) > 5)
    {
      throw std::invalid_argument("Wrong key.");
    }
    line = line.erase(0, comma + 1);
  }

  while (line.find_first_of(' ') == 0)
  {
    line.erase(0, 1);
  }

  if (line.empty())
  {
    throw std::invalid_argument("Wrong data.");
  }

  return { keys[0], keys[1], line };
}


bool compare(const DataStruct &lhs, const DataStruct &rhs)
{
  if (lhs.key1 < rhs.key1)
  {
    return true;
  }
  else if (lhs.key1 == rhs.key1)
  {
    if (lhs.key2 < rhs.key2)
    {
      return true;
    }
    else if (lhs.key2 == rhs.key2)
    {
      if (lhs.str.size() < rhs.str.size())
      {
        return true;
      }
    }
  }
  return false;
}



