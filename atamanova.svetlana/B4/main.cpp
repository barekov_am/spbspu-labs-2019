#include <iostream>
#include <vector>
#include <algorithm>

#include "data_struct.hpp"

DataStruct readData();
bool compare(const DataStruct &lhs, const DataStruct &rhs);



int main()
{
  try
  {
    std::vector<DataStruct> vector;

    while (std::cin.peek() != EOF)
    {
      if (std::cin.fail())
      {
        throw std::ios_base::failure("Reading failed");
      }
      vector.push_back(readData());
    }

    if (!vector.empty())
    {
      std::sort(vector.begin(), vector.end(), compare);
    }

    for (auto i = vector.begin(); i != vector.end(); i++)
    {
      std::cout << i->key1 << "," << i->key2 << "," << i->str << "\n";
    }
  }
  catch (const std::exception &exception)
  {
    std::cerr << exception.what();
    return 1;
  }

  return 0;
}
