#ifndef BOOKMARKSMANAGER_HPP
#define BOOKMARKSMANAGER_HPP

#include <map>
#include "phone-book.hpp"

class BookmarksManager
{
public:
  enum class insertPosition
  {
    before,
    after
  };

  enum class movePosition
  {
    first,
    last
  };

  BookmarksManager();

  void add(const PhoneBook::record_t &record);
  void store(const std::string &bookmark, const std::string &name);
  void insert(const std::string &bookmark, const PhoneBook::record_t &record, insertPosition point);
  void remove(const std::string &bookmark);
  void show(const std::string &bookmark);
  void move(const std::string &bookmark, int n);
  void move(const std::string &bookmark, movePosition position);
  void nextRecord(const std::string &bookmark);
  void prevRecord(const std::string &bookmark);
private:
  using bookmarkType = std::map<std::string, PhoneBook::iterator>;
  using bookmarkIterator = bookmarkType::iterator;

  PhoneBook records_;
  bookmarkType bookmarks_;

  bookmarkIterator getBookmarkIterator(const std::string &bookmark);
};

#endif // BOOKMARKSMANAGER_HPP
