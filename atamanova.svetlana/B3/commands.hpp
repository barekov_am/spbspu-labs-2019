#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <string>

class BookmarksManager;

void add(BookmarksManager &manager, std::stringstream &ss);
void store(BookmarksManager &manager, std::stringstream &ss);
void insert(BookmarksManager &manager, std::stringstream &ss);
void deleteBookmark(BookmarksManager &manager, std::stringstream &ss);
void show(BookmarksManager &manager, std::stringstream &ss);
void move(BookmarksManager &manager, std::stringstream &ss);

#endif // COMMANDS_HPP
