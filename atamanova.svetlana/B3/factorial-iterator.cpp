#include "factorial-iterator.hpp"

const int MIN = 1;
const int MAX = 11;

FactorialIterator::FactorialIterator():
  value_(1),
  index_(1)
{
}

FactorialIterator::FactorialIterator(int index)
{
  if (index < MIN || index > MAX)
    {
      throw std::invalid_argument("Index is out of range");
    }
  value_ = getValue(index);
  index_ = index;
}

FactorialIterator::reference FactorialIterator::operator *()
{
  return value_;
}

FactorialIterator::pointer FactorialIterator::operator ->()
{
  return &value_;
}

FactorialIterator &FactorialIterator::operator ++()
{
  if (index_ > MAX)
    {
      throw std::out_of_range("Index is out of range");
    }

  ++index_;
  value_ *= index_;
  return * this;
}

FactorialIterator FactorialIterator::operator ++(int)
{
  FactorialIterator tmp = *this;
  ++(*this);
  return tmp;
}

FactorialIterator &FactorialIterator::operator --()
{
  if (index_ <= MIN)
    {
      throw std::out_of_range("Index is out of range");
    }

  value_ /= index_;
  --index_;
  return * this;
}

FactorialIterator FactorialIterator::operator --(int)
{
  FactorialIterator tmp = *this;
  --(*this);
  return tmp;
}

bool FactorialIterator::operator ==(FactorialIterator iter) const
{
  return (value_ == iter.value_) && (index_ == iter.index_);
}

bool FactorialIterator::operator !=(FactorialIterator iter) const
{
  return !(*this == iter);
}


unsigned long long FactorialIterator::getValue(int index) const
{
  return (index <= MIN) ? 1 : (index * getValue(index - 1));
}

FactorialIterator FactorialContainer::begin()
{
  return FactorialIterator(MIN);
}

FactorialIterator FactorialContainer::end()
{
  return FactorialIterator(MAX);
}
