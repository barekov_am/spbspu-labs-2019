#include "text-element-reader.hpp"
#include <iostream>

const std::size_t MAX_WORD_LENGTH = 20u;
const std::size_t DASH_LENGTH = 3u;

TextElementReader::TextElementReader(std::istream & in) :
  in_(in),
  lastReadElement_()
{}

void TextElementReader::readWord(std::list<TextElement> & line, std::size_t lineLength, std::locale locale)
{
  std::string tmpWord;
  const std::ctype<char> & ct = std::use_facet<std::ctype<char>>(locale);
  while (ct.is(std::ctype_base::alpha, in_.peek()) || (in_.peek() == '-'))
  {
    char ch = static_cast<char>(in_.get());
    if ((ch == '-') && (in_.peek() == '-'))
    {
      in_.unget();
      break;
    }
    tmpWord += ch;
  }

  if (tmpWord.length() > MAX_WORD_LENGTH)
  {
    throw std::invalid_argument("Length of \"" + tmpWord + "\" more than 20 symbols");
  }

  lastReadElement_ = {TextElement::WORD, tmpWord};
  line.push_back(lastReadElement_);
  lineLength += tmpWord.length();
}

void TextElementReader::readPunctuation(std::list<TextElement> & line, std::size_t lineLength)
{
  if (line.empty())
  {
    throw std::invalid_argument("Text can't start with punctuation");
  }

  std::string tmpPunc;
  if (lastReadElement_.type == TextElement::DASH)
  {
    throw std::invalid_argument("Symbol of punctuation after dash");
  }
  if (lastReadElement_.type == TextElement::PUNCTUATION)
  {
    throw std::invalid_argument("Symbol of punctuation after symbol of punctuation");
  }
  if ((lastReadElement_.type == TextElement::THREEPOINTS) && (in_.peek() != ','))
  {
    throw std::invalid_argument("Symbol of punctuation after symbol of three points");
  }

  if (in_.peek() != ',')
  {
    tmpPunc += static_cast<char>(in_.get());
    lastReadElement_ = {TextElement::PUNCTUATION, tmpPunc};
    line.push_back(lastReadElement_);
    lineLength += tmpPunc.length();
  }
  else
  {
    in_.get();
    in_ >> std::ws;
    if (!(in_.peek() == '.') && !(lastReadElement_.type == TextElement::THREEPOINTS))
    {
      tmpPunc += ',';
      lastReadElement_ = {TextElement::PUNCTUATION, tmpPunc};
      line.push_back(lastReadElement_);
      lineLength += tmpPunc.length();
    }
    else if (in_.peek() == '.')
    {
      in_.get();
      if (in_.peek() != '.')
      {
        lastReadElement_ = {TextElement::PUNCTUATION, tmpPunc};
      }
      in_.unget();
    }
  }
}

void TextElementReader::readNumber(std::list<TextElement> & line, std::size_t lineLength, std::locale locale)
{
  std::string tmpNumber;
  static const char decimalDot = std::use_facet<std::numpunct<char>>(locale).decimal_point();
  bool wasDot = false;
  tmpNumber += in_.get();

  const std::ctype<char> & ct = std::use_facet<std::ctype<char>>(locale);
  while (ct.is(std::ctype_base::digit, in_.peek()) || (in_.peek() == decimalDot))
  {
    char ch = static_cast<char>(in_.get());
    if (ch == decimalDot)
    {
      if (wasDot)
      {
        in_.unget();
        break;
      }
      wasDot = true;
    }
    tmpNumber += ch;
  }

  if (tmpNumber.length() > MAX_WORD_LENGTH)
  {
    throw std::invalid_argument("Length of \"" + tmpNumber + "\" more than 20 symbols");
  }

  lastReadElement_ = {TextElement::NUMBER, tmpNumber};
  line.push_back(lastReadElement_);
  lineLength += tmpNumber.length();
}

void TextElementReader::readDash(std::list<TextElement> & line, std::size_t lineLength)
{
  if (line.empty())
  {
    throw std::invalid_argument("Text can't start whit hyphen");
  }

  std::string tmpDash;
  while (in_.peek() == '-')
  {
    tmpDash += static_cast<char>(in_.get());
  }

  if (tmpDash.length() != DASH_LENGTH)
  {
    throw std::invalid_argument("Invalid symbol: " + tmpDash);
  }
  if (lastReadElement_.type == TextElement::DASH)
  {
    throw std::invalid_argument("Dash after dash");
  }
  if ((lastReadElement_.type == TextElement::PUNCTUATION) && (line.back().content != ","))
  {
    throw std::invalid_argument("Dash after punctuation");
  }

  lastReadElement_ = {TextElement::DASH, tmpDash};
  line.push_back(lastReadElement_);
  lineLength += tmpDash.length();
}

void TextElementReader::readThreePoints(std::list<TextElement> & line, std::size_t lineLength){
  std::string tmpThreePoints;
  while (in_.peek() == '.')
  {
    tmpThreePoints += static_cast<char>(in_.get());
  }
  if (tmpThreePoints.length() != 3)
  {
    throw std::invalid_argument("Invalid symbol: " + tmpThreePoints);
  }
  if (lastReadElement_.type == TextElement::THREEPOINTS)
  {
    throw std::invalid_argument("Three points after three points");
  }

  lastReadElement_ = {TextElement::THREEPOINTS, tmpThreePoints};
  line.push_back(lastReadElement_);
  lineLength += tmpThreePoints.length();
}
