#ifndef TEXTELEMENTREADER_HPP
#define TEXTELEMENTREADER_HPP

#include <iosfwd>
#include <list>
#include "text-element.hpp"

class TextElementReader
{
public:
  TextElementReader(std::istream & in);
  void readWord(std::list<TextElement> & line, std::size_t lineLength, std::locale locale);
  void readPunctuation(std::list<TextElement> & line, std::size_t lineLength);
  void readNumber(std::list<TextElement> & line, std::size_t lineLength, std::locale locale);
  void readDash(std::list<TextElement> & line, std::size_t lineLength);
  void readThreePoints(std::list<TextElement> & line, std::size_t lineLength);

private:
  std::istream & in_;
  TextElement lastReadElement_;
};

#endif // TEXTELEMENTREADER_HPP
