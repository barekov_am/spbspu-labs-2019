#include <iostream>
#include <exception>
#include <cstring>
#include "text-formatter.hpp"

const std::size_t MAX_WORD_LENGTH = 20u;
const std::size_t DASH_LENGTH = 3u;
const std::size_t COMMA_LENGTH = 1u;
const std::size_t SPACE_LENGTH = 1u;
const std::size_t DEFAULT_WIDTH = 40u;
const std::size_t MIN_LINE_WIDTH = MAX_WORD_LENGTH + DASH_LENGTH + COMMA_LENGTH + SPACE_LENGTH;

int main(int argc, char * argv[])
{
  try
  {
    if ((argc != 3) && (argc != 1))
    {
      std::cerr << "Invalid number of arguments.\n";
      return 1;
    }
    std::size_t width = DEFAULT_WIDTH;
    if (argc == 3)
    {
      if (strcmp(argv[1], "--line-width") != 0)
      {
        std::cerr << "Invalid first argument.\n";
        return 1;
      }

      char * end;
      width = std::strtol(argv[2], &end, 10);
      if ((end == nullptr) || (*end != '\0') || (width < MIN_LINE_WIDTH))
      {
        std::cerr << "Invalid width argument.\n";
        return 1;
      }
    }

    TextElementReader reader(std::cin);
    TextFormatter textFormatter(std::cin, std::cout, width, reader);
    textFormatter.readText();
    textFormatter.printText();
  }
  catch (std::exception & err)
  {
    std::cerr << err.what() << '\n';
    return 1;
  }
  return 0;
}
