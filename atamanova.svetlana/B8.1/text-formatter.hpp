#ifndef TEXTFORMATTER_HPP
#define TEXTFORMATTER_HPP

#include <iosfwd>
#include <list>
#include <locale>
#include "text-element-reader.hpp"


class TextFormatter
{
public:
  TextFormatter(std::istream & in, std::ostream & out, std::size_t width, TextElementReader & reader);
  void readText();
  void printText();

private:
  std::istream & in_;
  std::ostream & out_;
  std::size_t width_;
  std::list<TextElement> line_;
  std::size_t lineLength_;
  TextElementReader reader_;
  std::locale locale_;

  void printLine(const std::list<TextElement> & line);
  void printReformatLine(std::list<TextElement> & line, std::size_t & lineLength);
};

#endif // TEXTFORMATTER_HPP
