#ifndef TEXTELEMENT_HPP
#define TEXTELEMENT_HPP
#include <string>

struct TextElement
{
  enum {
    WORD,
    PUNCTUATION,
    NUMBER,
    DASH,
    THREEPOINTS,
    SPACE
  } type;
  std::string content;
};

#endif // TEXTELEMENT_HPP
