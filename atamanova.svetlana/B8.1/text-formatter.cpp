#include "text-formatter.hpp"
#include <iostream>
#include <locale>

const std::size_t SPACE_LENGTH = 1u;
const std::size_t DASH_LENGTH = 3u;
const std::size_t THREE_POINTS_LENGTH = 3u;

TextFormatter::TextFormatter(std::istream & in, std::ostream & out, std::size_t width, TextElementReader & textElementReader) :
  in_(in),
  out_(out),
  width_(width),
  line_(),
  lineLength_(0),
  reader_(textElementReader),
  locale_(in.getloc())
{}

void TextFormatter::readText()
{
  while (in_)
  {
    in_ >> std::ws;
    char firstChar = static_cast<char>(in_.get());
    char secondChar = static_cast<char>(in_.peek());
    in_.unget();
    const std::ctype<char> & ct = std::use_facet<std::ctype<char>>(locale_);
    if (ct.is(std::ctype_base::alpha, firstChar))
    {
      reader_.readWord(line_, lineLength_, locale_);
    }
    else if ((ct.is(std::ctype_base::digit, firstChar)) ||
          ((firstChar == '+' || firstChar == '-') && ct.is(std::ctype_base::digit, secondChar)))
    {
      reader_.readNumber(line_, lineLength_, locale_);
    }
    else if (ct.is(std::ctype_base::punct, firstChar))
    {
      if (firstChar == '-')
      {
        reader_.readDash(line_, lineLength_);
      }
      else if ((firstChar == '.') && (secondChar == '.'))
      {
        reader_.readThreePoints(line_, lineLength_);
      }
      else
      {
        reader_.readPunctuation(line_, lineLength_);
      }
    }
    if (lineLength_ >= width_)
    {
      printText();
    }
  }
}


void TextFormatter::printText()
{
  std::size_t lineLength = 0;
  std::list<TextElement> line;

  for (const TextElement & element : line_)
  {
    switch (element.type)
    {
    case TextElement::NUMBER:
      if ((lineLength + element.content.length() + SPACE_LENGTH) > width_)
      {
        printLine(line);
        line.clear();
        lineLength = 0;
      }
      else if (!line.empty())
      {
        if (!(std::prev(line.end())->type == TextElement::THREEPOINTS))
        {
          line.push_back(TextElement{TextElement::SPACE, " "});
          lineLength++;
        }
      }
      line.push_back(element);
      lineLength += element.content.length();
      break;

    case TextElement::WORD:
      if ((lineLength + element.content.length() + SPACE_LENGTH) > width_)
      {
        printLine(line);
        line.clear();
        lineLength = 0;
      }
      else if (!line.empty())
      {
        if (!(std::prev(line.end())->type == TextElement::THREEPOINTS))
        {
          line.push_back(TextElement{TextElement::SPACE, " "});
          lineLength++;
        }
      }
      line.push_back(element);
      lineLength += element.content.length();
      break;

    case TextElement::PUNCTUATION:
      if (lineLength + SPACE_LENGTH > width_)
      {
        printReformatLine(line, lineLength);
      }
      line.push_back(element);
      lineLength += element.content.length();
      break;

    case TextElement::DASH:
      if (lineLength + DASH_LENGTH + SPACE_LENGTH > width_)
      {
        printReformatLine(line, lineLength);
      }
      line.push_back(TextElement{TextElement::SPACE, " "});
      line.push_back(element);
      lineLength += element.content.length() + 1;
      break;

    case TextElement::THREEPOINTS:
      if (lineLength + THREE_POINTS_LENGTH + SPACE_LENGTH > width_)
      {
        printReformatLine(line, lineLength);
      }
      if (std::prev(line.end())->type == TextElement::PUNCTUATION)
      {
        line.push_back(TextElement{TextElement::SPACE, " "});
        lineLength += 1;
      }
      line.push_back(element);
      lineLength += element.content.length();
      break;

    case TextElement::SPACE:
      break;
    }
  }
  if (!line.empty())
  {
    printLine(line);
  }
}

void TextFormatter::printLine(const std::list<TextElement> & line)
{
  for (const TextElement & element : line)
  {
    out_ << element.content;
  }
  out_ << '\n';
}

void TextFormatter::printReformatLine(std::list< TextElement > & line, std::size_t & lineLength)
{
  std::size_t tmpLength = 0;
  std::list<TextElement> tmpLine;

  while (!line.empty())
  {
    tmpLine.push_front(line.back());
    tmpLength += line.back().content.length();
    line.pop_back();

    if ((tmpLine.front().type == TextElement::WORD) || (tmpLine.front().type == TextElement::NUMBER))
    {
      break;
    }
  }
  printLine(line);
  line = tmpLine;
  lineLength = tmpLength;
}
