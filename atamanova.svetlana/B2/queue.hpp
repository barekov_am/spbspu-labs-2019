#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <list>
#include <stdexcept>
#include <functional>

template<typename T>
class QueueWithPriority
{
public:
  enum ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  void put(const T& element, ElementPriority priority);
  template<typename Handler>
  void get(Handler handle);
  void accelerate();
  bool empty() const;

private:
  std::list<T> low_;
  std::list<T> normal_;
  std::list<T> high_;
};

template<typename T>
void QueueWithPriority<T>::put(const T& element, ElementPriority priority)
{
  switch (priority)
  {
  case ElementPriority::LOW:
    low_.push_back(element);
    break;
  case ElementPriority::NORMAL:
    normal_.push_back(element);
    break;
  case ElementPriority::HIGH:
    high_.push_back(element);
    break;
  }
}

template<typename T>
template<typename Handler>
void QueueWithPriority<T>::get(Handler handle)
{
  if (!high_.empty())
  {
    handle(high_.front());
    high_.pop_front();
  }
  else if (!normal_.empty())
  {
    handle(normal_.front());
    normal_.pop_front();
  }
  else if (!low_.empty())
  {
    handle(low_.front());
    low_.pop_front();
  }
  else
  {
    throw std::invalid_argument("Queue is empty!");
  }
}

template<typename T>
void QueueWithPriority<T>::accelerate()
{
  high_.splice(high_.end(), low_);
}

template<typename T>
bool QueueWithPriority<T>::empty() const
{
  return (high_.empty() && normal_.empty() && low_.empty());
}

#endif // QUEUE_HPP
