#include <list>
#include <iostream>

void printNumbers(std::list<int> &list);

void printNumbers(std::list<int> &list)
{
  if (list.empty())
  {
    return;
  }
  std::list<int>::const_iterator left = list.cbegin();
  std::list<int>::const_iterator right = list.cend();
  while (left != right)
  {
    --right;
    if (right == left) {
      break;
    }
    if (left != list.cbegin()) {
      std::cout << " ";
    }
    std::cout << *left << " " << *right;
    ++left;
  }
  if (list.size() % 2) {
    if (list.size() != 1) {
      std::cout << " ";
    }
    std::cout << *left;
  }
  std::cout << "\n";
}


void task2()
{
  const int min = 1;
  const int max = 20;
  const std::size_t maxLength = 20;

  std::list<int> list;

  int currNum = -1;
  while (std::cin && !(std::cin >> currNum).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading failed");
    }

    if (currNum < min || currNum > max)
    {
      throw std::invalid_argument("Number out of range");
    }

    if (list.size() == maxLength)
    {
      throw std::invalid_argument("Too many numbers");
    }

    list.push_back(currNum);
  }

  printNumbers(list);
}
