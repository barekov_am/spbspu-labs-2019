#include <iostream>
#include <sstream>
#include <algorithm>
#include <locale>
#include "queue.hpp"

template<typename Char, typename CharTraits>
std::basic_istream<Char, CharTraits>& blank(std::basic_istream<Char, CharTraits>& is)
{
  while (std::isblank(static_cast<char>(is.peek()), std::locale("")))
  {
    is.ignore();
  }

  return is;
}

void add(QueueWithPriority<std::string>& queue, std::istream& args)
{
  static const struct
  {
    const char* name;
    QueueWithPriority<std::string>::ElementPriority priority;
  } priorities[] = {
      {"high", QueueWithPriority<std::string>::HIGH},
      {"normal", QueueWithPriority<std::string>::NORMAL},
      {"low", QueueWithPriority<std::string>::LOW}};

  std::string priority;
  args >> priority;

  auto findPriority =
      std::find_if(std::begin(priorities), std::end(priorities), [&](const auto& el) { return priority == el.name; });

  if (findPriority == std::end(priorities))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::string data;
  getline(args >> blank, data);
  if (data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.put(data, findPriority->priority);
}

void get(QueueWithPriority<std::string>& queue, std::istream& args)
{
  if (!args.eof())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (queue.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  queue.get([](const auto& el) { std::cout << el << '\n'; });
}

void accelerate(QueueWithPriority<std::string>& queue, std::istream& args)
{
  if (!args.eof())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.accelerate();
}

