#include <iostream>
#include <algorithm>
#include <sstream>
#include "queue.hpp"

void add(QueueWithPriority<std::string>& queue, std::istream& args);
void get(QueueWithPriority<std::string>& queue, std::istream& args);
void accelerate(QueueWithPriority<std::string>& queue, std::istream& args);

template<typename Char, typename CharTraits>
std::basic_istream<Char, CharTraits>& blank(std::basic_istream<Char, CharTraits>& is);

void task1()
{
  QueueWithPriority<std::string> queue;

  static const struct
  {
    const char* name;
    std::function<void(QueueWithPriority<std::string>&, std::istream& args)> function;
  } commands[] = {
      {"add", &add},
      {"get", &get},
      {"accelerate", &accelerate}};

  std::string input;
  while (getline(std::cin, input))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Reading failed");
    }

    std::stringstream inStream(input);
    inStream >> std::noskipws;

    std::string command;
    inStream >> blank >> command;

    auto findCommand =
        std::find_if(std::begin(commands), std::end(commands), [&](const auto& el) { return command == el.name; });

    if (findCommand != std::end(commands))
    {
      findCommand->function(queue, inStream >> blank);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
