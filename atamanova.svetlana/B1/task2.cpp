#include <fstream>
#include <memory>
#include <vector>
#include "tasks.hpp"

const std::size_t initSize = 256;

void task2(const char * fileName)
{
  std::ifstream inputFile(fileName);
  if (!inputFile)
  {
    throw std::runtime_error("No such file");
  }

  std::size_t size = initSize;

  std::unique_ptr<char[], decltype(&free)> arr(static_cast<char *>(malloc(size)), &free);

  if (!arr)
  {
    throw std::runtime_error("Could not allocate memory.");
  }

  std::size_t index = 0;
  while (inputFile)
  {
    inputFile.read(&arr[index], initSize);
    index += inputFile.gcount();
    if (inputFile.gcount() == initSize)
    {
      size += initSize;
      std::unique_ptr<char[], decltype(&free)> newArr(static_cast<char *>(realloc(arr.get(), size)), &free);

      if (!newArr)
      {
        throw std::runtime_error("Could not reallocate memory.");
      }

      arr.release();
      std::swap(arr, newArr);
    }
    if (!inputFile.eof() && inputFile.fail())
    {
      throw std::runtime_error("Reading failed");
    }
  }

  std::vector<char> vector(&arr[0], &arr[index]);
  for (auto element : vector)
  {
    std::cout << element;
  }
}
