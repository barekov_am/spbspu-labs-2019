#ifndef SORT_HPP
#define SORT_HPP

#include <iostream>
#include <functional>
#include <cstring>
#include "strategy.hpp"

template <typename T>
std::function<bool(T, T)> getDirection(const char * dir)
{
  if (strcmp(dir, "ascending") == 0)
  {
    return [](T a, T b) { return a < b; };
  }
  if (strcmp(dir, "descending") == 0)
  {
    return [](T a, T b) { return a > b; };
  }
  throw std::invalid_argument("Incorrect direction.");
}

template <template <class Container> class Access, typename Container>
void sort(Container &arr, std::function <bool(typename Container::value_type, typename Container::value_type)> compare)
{
  const auto begin = Access<Container>::begin(arr);
  const auto end = Access<Container>::end(arr);

  for (auto i = begin; i != end; i++)
    {
      for (auto j = Access<Container>::next(i); j != end; j++)
      {
        if (compare(Access<Container>::get(arr, j), Access<Container>::get(arr, i)))
        {
          std::swap(Access<Container>::get(arr, j), Access<Container>::get(arr, i));
        }
      }
    }
}

template <typename Container>
void print(const Container &arr)
{
  if (arr.empty())
  {
    return;
  }

  for (const auto &elem: arr)
  {
    std::cout << elem << " ";
  }
  std::cout << "\n";
}

#endif // SORT_HPP
