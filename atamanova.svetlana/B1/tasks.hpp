#ifndef TASKS_HPP
#define TASKS_HPP

#include <iostream>

void task1(const char * direction);
void task2(const char * fileName);
void task3();
void task4(const char * direction, int size);

#endif // TASKS_HPP
