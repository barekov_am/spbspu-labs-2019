#include <vector>
#include <forward_list>

#include "sort.hpp"
#include "tasks.hpp"

void task1(const char * dir)
{
  auto order = getDirection<int>(dir);
  std::vector<int> vector;

  int i = -1;
  while (std::cin && !(std::cin >> i).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Incorrect input");
    }

    vector.push_back(i);
  }

  if (vector.empty())
  {
    return;
  }

  std::vector<int> vectAt(vector);
  std::forward_list<int> listIter(vector.begin(), vector.end());

  sort<bracketStrtg>(vector, order);
  sort<atStrtg>(vectAt, order);
  sort<iterStrtg>(listIter, order);

  print(vector);
  print(vectAt);
  print(listIter);
}
