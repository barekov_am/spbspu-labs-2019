#ifndef STRATEGY_HPP
#define STRATEGY_HPP

#include <cstddef>
#include <iterator>

template <typename Container>
struct bracketStrtg
{
  static std::size_t begin(const Container &)
  {
    return 0;
  }

  static std::size_t end(const Container &cont)
  {
    return cont.size();
  }

  static std::size_t next(size_t index)
  {
    return index + 1;
  }

  static typename Container::reference get(Container &cont, size_t index)
  {
    return cont[index];
  }
};

template <typename Container>
struct atStrtg
{
  static std::size_t begin(const Container &)
  {
    return 0;
  }

  static std::size_t end(const Container &cont)
  {
    return cont.size();
  }

  static std::size_t next(size_t index)
  {
    return index + 1;
  }

  static typename Container::reference get(Container &cont, size_t index)
  {
    return cont.at(index);
  }
};

template <typename Container>
struct iterStrtg
{
  static typename Container::iterator begin(Container &cont)
  {
    return cont.begin();
  }

  static typename Container::iterator end(Container &cont)
  {
    return cont.end();
  }

  static typename Container::iterator next(typename Container::iterator iter)
  {
    return ++iter;
  }

  static typename Container::reference get(Container &, typename Container::iterator iter)
  {
    return * iter;
  }

};

#endif // STRATEGY_HPP
