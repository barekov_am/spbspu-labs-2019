#include <random>
#include <vector>

#include "sort.hpp"
#include "tasks.hpp"


void fillRandom(double * array, int size)
{
  for (int i = 0; i < size; i++)
  {
    array[i] = (rand() % 21 - 10) / 10.0;
  }
}

void task4(const char * dir, int size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Incorrect size");
  }
  auto order = getDirection<double>(dir);

  std::vector<double> vector(static_cast<unsigned int>(size));
  fillRandom(&vector[0], size);
  print(vector);

  sort<bracketStrtg>(vector, order);

  print(vector);
}
