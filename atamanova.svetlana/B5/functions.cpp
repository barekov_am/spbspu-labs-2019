#include <string>
#include <iostream>

#include "shape.hpp"

Shape readPoints(std::string line, int vertices)
{
  Shape shape;
  std::size_t openBracket;
  std::size_t semicolon;
  std::size_t closeBracket;

  if (line.empty())
  {
    throw std::invalid_argument("Incorrect number of vertices\n");
  }

  for (int i = 0; i < vertices; i++)
  {
    openBracket = line.find_first_of('(');
    semicolon = line.find_first_of(';');
    closeBracket = line.find_first_of(')');

    if ((openBracket == std::string::npos) || (semicolon == std::string::npos)
        || (closeBracket == std::string::npos))
    {
      throw std::invalid_argument("Incorrect separator\n");
    }

    Point_t point
    {
      std::stoi(line.substr(openBracket + 1, semicolon - openBracket - 1)),
      std::stoi(line.substr(semicolon + 1, closeBracket - semicolon - 1))
    };

    line.erase(0, closeBracket + 1);

    shape.push_back(point);
  }

  line.erase(line.find_last_not_of(" \t") + 1);

  if (!line.empty())
  {
    throw std::invalid_argument("Incorrect input");
  }

  return shape;
}
