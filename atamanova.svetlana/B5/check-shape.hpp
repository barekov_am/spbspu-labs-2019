#ifndef CHECKSHAPE_HPP
#define CHECKSHAPE_HPP

#include "shape.hpp"

int getLength(const Point_t& lhs, const Point_t& rhs);
bool isSquare(const Shape& shape);
bool isRectangle(const Shape& shape);

#endif // CHECKSHAPE_HPP
