#include "check-shape.hpp"

int getLength(const Point_t& lhs, const Point_t& rhs)
{
  return (lhs.x - rhs.x) * (lhs.x - rhs.x) + (lhs.y - rhs.y) * (lhs.y - rhs.y);
}

bool isRectangle(const Shape& shape)
{
  int diag1 = getLength(shape[0], shape[2]);
  int diag2 = getLength(shape[1], shape[3]);

  if (diag1 == diag2)
    {
      return true;
    }
  return false;
}

bool isSquare(const Shape& shape)
{
  if (isRectangle(shape))
  {
    int side1 = getLength(shape[0], shape[1]);
    int side2 = getLength(shape[1], shape[2]);

    if (side1 == side2)
    {
      return true;
    }
  }

  return false;
}
