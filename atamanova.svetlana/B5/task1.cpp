#include <iostream>
#include <set>
#include <string>

void task1()
{
  std::string line;
  std::set<std::string> set;

  while (std::getline(std::cin >> std::ws, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed during reading");
    }

    auto pos = line.find_first_not_of(" \t\n");
    if (pos != std::string::npos)
    {
      line.erase(0, pos);
    }

    while (!line.empty())
    {
      pos = line.find_first_of(" \t");
      set.insert(line.substr(0, pos));
      line.erase(0, pos);

      pos = line.find_first_not_of(" \t\n");
      if (pos != std::string::npos)
      {
        line.erase(0, pos);
      }
      else
      {
        line.clear();
      }
    }
  }
  for (const auto& it : set)
  {
    std::cout << it << '\n';
  }
}
