#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "layering.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(layeringTestSuite)

BOOST_AUTO_TEST_CASE(emptyCompositeLayering)
{
  atamanova::Matrix<atamanova::Shape> testMatrix = atamanova::layer(atamanova::CompositeShape());

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 0);
}

BOOST_AUTO_TEST_CASE(correctCompositeLayering)
{
  atamanova::CompositeShape testComposite;
  testComposite.add(std::make_shared<atamanova::Rectangle>(atamanova::point_t{0, 0}, 6, 4));
  testComposite.add(std::make_shared<atamanova::Circle>(atamanova::point_t{4, 3}, 3));
  testComposite.add(std::make_shared<atamanova::Rectangle>(atamanova::point_t{-5, 4}, 2, 1));

  atamanova::Matrix<atamanova::Shape> testMatrix = atamanova::layer(testComposite);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 2);
  BOOST_CHECK_EQUAL(testMatrix[0].size(), 2);
  BOOST_CHECK_EQUAL(testMatrix[1].size(), 1);
  BOOST_CHECK_EQUAL(testMatrix[0][0], testComposite[0]);
  BOOST_CHECK_EQUAL(testMatrix[0][1], testComposite[2]);
  BOOST_CHECK_EQUAL(testMatrix[1][0], testComposite[1]);
}

BOOST_AUTO_TEST_CASE(intercetionTseting)
{
  atamanova::Rectangle rectangle({5, 5}, 6, 4);
  atamanova::Circle circle1({3, 2}, 2);
  atamanova::Circle circle2({-4, -3}, 2);

  BOOST_CHECK(atamanova::intersect(rectangle, circle1));
  BOOST_CHECK(!atamanova::intersect(rectangle, circle2));

}


BOOST_AUTO_TEST_SUITE_END()
