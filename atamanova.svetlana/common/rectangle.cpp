#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

atamanova::Rectangle::Rectangle(point_t center, double width, double height) :
  center_(center),
  width_(width),
  height_(height),
  angle_(0)
{
  if (width_ <= 0.0)
  {
    throw std::invalid_argument("Rectangle width is less or equal zero.");
  }
  if (height_ <= 0.0)
  {
    throw std::invalid_argument("Rectangle height is less or equal zero.");
  }
}

atamanova::Rectangle::Rectangle(point_t center, double width, double height, double angle) :
  Rectangle(center, width, height)
{
  rotate(angle);
}

double atamanova::Rectangle::getArea() const
{
  return width_ * height_;
}

atamanova::rectangle_t atamanova::Rectangle::getFrameRect() const
{
  const double sinAngle = std::sin(angle_ * M_PI / 180);
  const double cosAngle = std::cos(angle_ * M_PI / 180);
  const double width = std::abs(width_ * cosAngle) + std::abs(height_ * sinAngle);
  const double height = std::abs(width_ * sinAngle) + std::abs(height_ * cosAngle);

  return {width, height, center_};
}

void atamanova::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void atamanova::Rectangle::move(point_t point)
{
  center_ = point;
}

void atamanova::Rectangle::scale(double scale)
{
  if (scale <= 0)
  {
    throw std::invalid_argument("Scale is less or equal 0");
  }
  else
  {
    width_ *= scale;
    height_ *= scale;
  }
}

void atamanova::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
