#include "composite-shape.hpp"

#include <iostream>
#include <algorithm>
#include <stdexcept>
#include <cmath>


atamanova::CompositeShape::CompositeShape() :
  count_(0),
  shapes_(nullptr)
{
}

atamanova::CompositeShape::CompositeShape(const CompositeShape& compShape) :
  count_(compShape.count_),
  shapes_(std::make_unique<atamanova::Shape::ptr[]>(compShape.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i] = compShape.shapes_[i];
  }
}

atamanova::CompositeShape::CompositeShape(CompositeShape&& compShape) :
  count_(compShape.count_),
  shapes_(std::move(compShape.shapes_))
{
  compShape.count_ = 0;
}

atamanova::CompositeShape& atamanova::CompositeShape::operator =(const CompositeShape& compShape)
{
  if (this != &compShape)
  {
    count_ = compShape.count_;
    shapes_ = std::make_unique<atamanova::Shape::ptr[]>(count_);
    for (size_t i = 0; i < count_; i++)
    {
      shapes_[i] = compShape.shapes_[i];
    }
  }
  return *this;
}

atamanova::CompositeShape& atamanova::CompositeShape::operator =(atamanova::CompositeShape&& compShape)
{
  if (this != &compShape)
  {
    count_ = compShape.count_;
    compShape.count_ = 0;
    shapes_ = std::move(compShape.shapes_);
  }
  return *this;
}

atamanova::Shape::ptr atamanova::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index out of range");
  }

  return shapes_[index];
}


double atamanova::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

atamanova::rectangle_t atamanova::CompositeShape::getFrameRect() const
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("Composite shape is empty");
  }
  rectangle_t currRectangle = shapes_[0]->getFrameRect();
  double minX = currRectangle.pos.x - currRectangle.width / 2;
  double maxX = currRectangle.pos.x + currRectangle.width / 2;
  double minY = currRectangle.pos.y - currRectangle.height / 2;
  double maxY = currRectangle.pos.y + currRectangle.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    currRectangle = shapes_[i]->getFrameRect();

    double frameRectangle = currRectangle.pos.x - currRectangle.width / 2;
    minX = std::min(frameRectangle, minX);
    frameRectangle = currRectangle.pos.y - currRectangle.height / 2;
    minY = std::min(frameRectangle, minY);
    frameRectangle = currRectangle.pos.x + currRectangle.width / 2;
    maxX = std::max(frameRectangle, maxX);
    frameRectangle = currRectangle.pos.y + currRectangle.height / 2;
    maxY = std::max(frameRectangle, maxY);
  }
  return {(maxX - minX), (maxY - minY), {(minX + maxX) / 2, (minY + maxY) / 2}};
}

void atamanova::CompositeShape::move(point_t point)
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("Composite shape is empty");
  }
  double dx = point.x - getFrameRect().pos.x;
  double dy = point.y - getFrameRect().pos.y;
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void atamanova::CompositeShape::move(double dx, double dy)
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("Composite shape is empty");
  }
  for (size_t i = 0; i < count_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void atamanova::CompositeShape::scale(double scale)
{
  if (shapes_ == nullptr)
    {
      throw std::logic_error("Composite shape is empty");
    }

  if (scale <= 0)
  {
    throw std::invalid_argument("Scale must be > 0");
  }

  rectangle_t frameRect = getFrameRect();
  for (size_t i = 0; i < count_; i++)
  {
    double dx = shapes_[i]->getFrameRect().pos.x - frameRect.pos.x;
    double dy = shapes_[i]->getFrameRect().pos.y - frameRect.pos.y;
    shapes_[i]->move({frameRect.pos.x + dx * scale, frameRect.pos.y + dy * scale});
    shapes_[i]->scale(scale);
  }
}

void atamanova::CompositeShape::rotate(double angle)
{
  const atamanova::point_t framePos = getFrameRect().pos;
  const double sinAngle = std::sin(angle * M_PI / 180);
  const double cosAngle = std::cos(angle * M_PI / 180);
  for (size_t i = 0; i < count_; i++)
  {
    const atamanova::point_t shapePos = shapes_[i]->getFrameRect().pos;
    const double dx = shapePos.x - framePos.x;
    const double dy = shapePos.y - shapePos.y;
    shapes_[i]->move(dx * (cosAngle - 1) - dy * sinAngle, dx * sinAngle + dy * (cosAngle - 1));
    shapes_[i]->rotate(angle);
  }
}

void atamanova::CompositeShape::add(atamanova::Shape::ptr shape)
{
  if (shape == nullptr)
    {
      throw std::invalid_argument("Can't add nullptr.");
    }

  atamanova::Shape::array currShapes(std::make_unique<std::shared_ptr<atamanova::Shape>[]>(count_ + 1));
  for (size_t i = 0; i < count_; i++)
  {
    currShapes[i] = shapes_[i];
  }
  currShapes[count_] = shape;
  count_++;
  shapes_ = std::move(currShapes);
}

void atamanova::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }
  if (shapes_ == nullptr)
  {
    throw std::out_of_range("Composite shape is already empty");
  }

  count_--;
  for (size_t i = index; i < count_; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }

}

size_t atamanova::CompositeShape::getSize() const
{
  return count_;
}
