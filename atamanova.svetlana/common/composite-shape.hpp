#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include "shape.hpp"

namespace atamanova
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape& compShape);
    CompositeShape(CompositeShape&& compShape);
    ~CompositeShape() = default;

    CompositeShape& operator =(const CompositeShape& compShape);
    CompositeShape& operator =(CompositeShape&& compShape);

    Shape::ptr operator [](size_t index) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(point_t point) override;
    void move(double dx, double dy) override;
    void scale(double scale) override;
    void rotate(double angle);

    void add(Shape::ptr shape);
    void remove(size_t index);
    void remove(const Shape& shape);
    size_t getSize() const;


  private:
    size_t count_;
    Shape::array shapes_;
  };

}
#endif // COMPOSITESHAPE_HPP
