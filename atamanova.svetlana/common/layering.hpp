#ifndef LAYERING_HPP
#define LAYERING_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

namespace atamanova
{
  bool intersect(const Shape&, const Shape&);
  Matrix<Shape> layer(const CompositeShape&);
}

#endif // LAYERING_HPP
