#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteForCircle)

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_CASE(immutabilityCircleAfterMoving)
{
  atamanova::Circle testCircle({5.0, 5.0}, 4.0);
  const atamanova::rectangle_t rectBeforeMoving = testCircle.getFrameRect();
  const double areaBeforeMoving = testCircle.getArea();

  testCircle.move({5.0, 5.0});
  atamanova::rectangle_t rectAfterMoving = testCircle.getFrameRect();
  double areaAfterMoving = testCircle.getArea();
  BOOST_CHECK_CLOSE(rectBeforeMoving.width, rectAfterMoving.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectBeforeMoving.height, rectAfterMoving.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, ACCURACY);

  testCircle.move(5.0, 5.0);
  rectAfterMoving = testCircle.getFrameRect();
  areaAfterMoving = testCircle.getArea();
  BOOST_CHECK_CLOSE(rectBeforeMoving.width, rectAfterMoving.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectBeforeMoving.height, rectAfterMoving.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, ACCURACY);
}

BOOST_AUTO_TEST_CASE(checkAreaOfCircleAfterScaling)
{
  atamanova::Circle testCircle({5.0, 5.0}, 4.0);
  const double areaBeforeScaling = testCircle.getArea();
  const double coefficientScale = 2.0;
  testCircle.scale(coefficientScale);
  double areaAfterScaling = testCircle.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScaling * coefficientScale * coefficientScale, areaAfterScaling, ACCURACY);
}

BOOST_AUTO_TEST_CASE(circleRotation)
{
  atamanova::Circle circle({1, 1}, 2);
  const atamanova::rectangle_t frameRectBefore = circle.getFrameRect();
  const double areaBefore = circle.getArea();
  circle.rotate(10);
  const double areaAfter = circle.getArea();
  const atamanova::rectangle_t frameRectAfter = circle.getFrameRect();

  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
}


BOOST_AUTO_TEST_CASE(incorrrectFunctionParametersCircle)
{
  BOOST_CHECK_THROW(atamanova::Circle({2.0, 3.0}, -2.0), std::invalid_argument);
  BOOST_CHECK_THROW(atamanova::Circle({2.0, -2.0}, -1.0), std::invalid_argument);

  atamanova::Circle testCircle1({1.0, 1.0}, 1.0);
  BOOST_CHECK_THROW(testCircle1.scale(-2.0), std::invalid_argument);

  atamanova::Circle testCircle2({1.0, 1.0}, 1.0);
  BOOST_CHECK_THROW(testCircle2.scale(-1.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
