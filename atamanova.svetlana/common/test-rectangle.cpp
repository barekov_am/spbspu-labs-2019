#include <stdexcept>

#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteForShapes)

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_CASE(immutabilityRectangleAfterMoving)
{
  atamanova::Rectangle testRectangle({5.0, 5.0}, 4.0, 4.0);
  const atamanova::rectangle_t rectBeforeMoving = testRectangle.getFrameRect();
  const double areaBeforeMoving = testRectangle.getArea();

  testRectangle.move({5.0, 5.0});
  atamanova::rectangle_t rectAfterMoving = testRectangle.getFrameRect();
  double areaAfterMoving = testRectangle.getArea();
  BOOST_CHECK_CLOSE(rectBeforeMoving.width, rectAfterMoving.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectBeforeMoving.height, rectAfterMoving.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, ACCURACY);

  testRectangle.move(5.0, 5.0);
  rectAfterMoving = testRectangle.getFrameRect();
  areaAfterMoving = testRectangle.getArea();
  BOOST_CHECK_CLOSE(rectBeforeMoving.width, rectAfterMoving.width, ACCURACY);
  BOOST_CHECK_CLOSE(rectBeforeMoving.height, rectAfterMoving.height, ACCURACY);
  BOOST_CHECK_CLOSE(areaBeforeMoving, areaAfterMoving, ACCURACY);
}

BOOST_AUTO_TEST_CASE(checkAreaOfRectangleAfterScaling)
{
  atamanova::Rectangle testRectangle({5.0, 5.0}, 4.0, 4.0);
  const double areaBeforeScaling = testRectangle.getArea();
  const double coefficientScale = 3.0;
  testRectangle.scale(coefficientScale);
  double areaAfterScaling = testRectangle.getArea();
  BOOST_CHECK_CLOSE(areaBeforeScaling * coefficientScale * coefficientScale, areaAfterScaling, ACCURACY);
}

BOOST_AUTO_TEST_CASE(immutabilityRectangleAfterRotation)
{
  atamanova::Rectangle testRectangle({5.0, 5.0}, 4.0, 4.0);
  const double areaBefore = testRectangle.getArea();
  const atamanova::rectangle_t frameRectBefore = testRectangle.getFrameRect();
  double angle = 90.0;
  testRectangle.rotate(angle);
  double areaAfter = testRectangle.getArea();
  atamanova::rectangle_t frameRectAfter = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);

  angle = 180;
  testRectangle.rotate(angle);
  areaAfter = testRectangle.getArea();
  frameRectAfter = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);

  angle = 360;
  testRectangle.rotate(angle);
  areaAfter = testRectangle.getArea();
  frameRectAfter = testRectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frameRectBefore.height, frameRectAfter.height, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.width, frameRectAfter.width, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.x, frameRectAfter.pos.x, ACCURACY);
  BOOST_CHECK_CLOSE(frameRectBefore.pos.y, frameRectAfter.pos.y, ACCURACY);
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, ACCURACY);
}


BOOST_AUTO_TEST_CASE(incorrectFunctionParametersRectangle)
{
  BOOST_CHECK_THROW(atamanova::Rectangle({6.0, 3.0}, -4.0, 6.0), std::invalid_argument);
  BOOST_CHECK_THROW(atamanova::Rectangle({5.0, -3.0}, -4.0, -6.0), std::invalid_argument);

  atamanova::Rectangle testRect1({1.0, 1.0}, 1.0, 1.0);
  BOOST_CHECK_THROW(testRect1.scale(-1.0), std::invalid_argument);

  atamanova::Rectangle testRect2({1.0, 1.0}, 1.0, 1.0);
  BOOST_CHECK_THROW(testRect2.scale(-2.0), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()
