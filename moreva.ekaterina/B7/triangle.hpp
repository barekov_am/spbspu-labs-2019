#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

class Triangle: public Shape
{
public:
  Triangle(Point center);
  ~Triangle() override = default;
  void draw() const override;
};

#endif // TRIANGLE_HPP
