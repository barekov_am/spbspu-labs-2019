#ifndef DATASTUCT_HPP
#define DATASTUCT_HPP

#include <string>

struct DataStruct
{
  int key1{};
  int key2{};
  std::string str;
};

#endif // DATASTUCT_HPP

