#ifndef SORT_HPP
#define SORT_HPP

#include <functional>
#include "accesses.hpp"

enum class Direction
{
  ascending,
  descending
};

Direction getDirection(const char* direction);

template <template <class T> class Access, typename T>
void sort(T& collection, Direction direction)
{
  using value_type = typename T::value_type;

  std::function<bool(value_type, value_type)>compare;
  if (direction == Direction::ascending)
  {
    compare = std::greater<value_type>();
  }
  else
  {
    compare = std::less<value_type>();
  }

  const auto begin = Access<T>::begin(collection);
  const auto end = Access<T>::end(collection);

  for (auto i = begin; i != end; ++i)
  {
    for (auto j = i; j != end; ++j)
    {
      if (compare(Access<T>::element(collection, i), Access<T>::element(collection, j)))
      {
        std::swap(Access<T>::element(collection, i), Access<T>::element(collection, j));
      }
    }
  }
}

#endif //SORT_HPP
