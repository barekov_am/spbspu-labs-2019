#ifndef WRITE_HPP
#define WRITE_HPP

#include <iostream>

template <typename T>
void write(const T& collection)
{
  if(collection.empty())
  {
    return;
  }

  for (const auto& i: collection)
  {
    std::cout << i << " ";
  }
  std::cout << std::endl;
}

#endif // WRITE_HPP
