#ifndef PHONE_BOOK_MANAGER_HPP
#define PHONE_BOOK_MANAGER_HPP

#include <map>
#include "phone-book.hpp"

class PhoneBookManager
{
  public:
    enum class InsertPosition
    {
      before,
      after
    };

    enum class MovePosition
    {
      first,
      last
    };

    PhoneBookManager();

    void add(const PhoneBook::record_t& record);
    void store(const std::string& bookmark, const std::string& newBookmark);
    void insert(const std::string& bookmark, const PhoneBook::record_t& record, InsertPosition position);
    void remove(const std::string& bookmark);
    void show(const std::string& bookmark) ;
    void move(const std::string& bookmark, int steps);
    void move(const std::string& bookmark, MovePosition position);

  private:
    using bookmarks = std::map<std::string, PhoneBook::iterator>;
    PhoneBook records_;
    bookmarks bookmarks_;
    bookmarks::iterator getElementIterator(const std::string& bookmark);
};

#endif //PHONE-BOOK-MANAGER_HPP
