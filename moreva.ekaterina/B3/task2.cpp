#include <algorithm>
#include <iostream>
#include "container-factorial.hpp"

void task2()
{
  ContainerFactorial fc;

  std::copy(fc.begin(), fc.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;

  std::reverse_copy(fc.begin(), fc.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;
}
