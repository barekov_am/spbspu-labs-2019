#ifndef CONTEINERFACTORIAL_HPP
#define CONTEINERFACTORIAL_HPP

#include <iterator>
#include "iterator.hpp"

class ContainerFactorial
{
  public:
    ContainerFactorial() = default;
    ContainerFactorial(const ContainerFactorial &) = delete;
    ContainerFactorial(ContainerFactorial &&) = delete;
    ~ContainerFactorial() = default;
    ContainerFactorial & operator=(const ContainerFactorial &) = delete;
    ContainerFactorial & operator=(ContainerFactorial &&) = delete;
    Iterator begin();
    Iterator end();
  private:
};

#endif //CONTEINERFACTORIAL_HPP
