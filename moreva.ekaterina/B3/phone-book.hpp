#ifndef PHONE_BOOK_HPP
#define PHONE_BOOK_HPP

#include <list>
#include <string>

class PhoneBook
{
  public:
    struct record_t
    {
      std::string number;
      std::string name;
    };

    using container = std::list<record_t>;
    using iterator = container::iterator;

    void view(iterator) const;
    void pushBack(const record_t&);

    bool empty() const;

    iterator begin();
    iterator end();
    iterator next(iterator);
    iterator prev(iterator);
    iterator insert(iterator, const record_t&);
    iterator remove(iterator);
    iterator move(iterator, int);

  private:
    container cont_;
};

#endif //PHONE_BOOK_HPP
