#ifndef COMMANDS_H
#define COMMANDS_H

#include <string>

#include "phone-book-manager.hpp"

void executeAdd(PhoneBookManager&, std::stringstream&);
void executeStore(PhoneBookManager&, std::stringstream&);
void executeInsert(PhoneBookManager&, std::stringstream&);
void executeDelete(PhoneBookManager&, std::stringstream&);
void executeShow(PhoneBookManager&, std::stringstream&);
void executeMove(PhoneBookManager&, std::stringstream&);

#endif //COMMANDS_H
