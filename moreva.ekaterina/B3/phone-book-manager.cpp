#include <algorithm>
#include <iostream>
#include "phone-book-manager.hpp"

PhoneBookManager::PhoneBookManager()
{
  bookmarks_["current"] = records_.end();
}

void PhoneBookManager::add(const PhoneBook::record_t& record)
{
  records_.pushBack(record);

  if (std::next(records_.begin()) == records_.end())
  {
    bookmarks_["current"] = records_.begin();
  }
}

void PhoneBookManager::store(const std::string& bookmark, const std::string& newBookmark)
{
  auto iter = getElementIterator(bookmark);

  if (iter != bookmarks_.end())
    {
      bookmarks_.emplace(newBookmark, iter->second);
    }
}

void PhoneBookManager::insert(const std::string& bookmark, const PhoneBook::record_t& record, InsertPosition position)
{
  auto iter = getElementIterator(bookmark);

  if (iter != bookmarks_.end())
  {
    if (iter->second == records_.end())
    {
      add(record);
    }

    if (position == InsertPosition::after)
    {
      records_.insert(std::next(iter->second), record);
    }
    else
    {
      records_.insert(iter->second, record);
    }
  }
}

void PhoneBookManager::remove(const std::string& bookmark)
{
  auto iter = getElementIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    auto eraseIter = iter->second;

    auto find = [&](auto & it)
    {
      if (it.second == eraseIter)
      {
        if (std::next(it.second) == records_.end())
        {
          it.second = records_.prev(eraseIter);
        }
        else
        {
          it.second = records_.next(eraseIter);
        }
      }
    };
    std::for_each(bookmarks_.begin(), bookmarks_.end(), find);
    records_.remove(eraseIter);
  }
}

void PhoneBookManager::show(const std::string& bookmark)
{
  auto iter = getElementIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    if (records_.empty())
    {
      std::cout << "<EMPTY>" << std::endl;
      return;
    }
    records_.view(iter->second);
  }
}

void PhoneBookManager::move(const std::string& bookmark, int steps)
{
  auto iter = getElementIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    iter->second = records_.move(iter->second, steps);
  }
}

void PhoneBookManager::move(const std::string& bookmark, MovePosition position)
{
  auto iter = getElementIterator(bookmark);
  if (iter != bookmarks_.end())
  {
    if (position == MovePosition::first)
    {
      iter->second = records_.begin();
    }
    if (position == MovePosition::last)
    {
      iter->second = records_.prev(records_.end());
    }
  }
}

PhoneBookManager::bookmarks::iterator PhoneBookManager::getElementIterator(const std::string& bookmark)
{
  auto iter = bookmarks_.find(bookmark);
  if (iter != bookmarks_.end())
  {
    return iter;
  }
  std::cout << "<INVALID BOOKMARK>" << std::endl;
  return bookmarks_.end();
}
