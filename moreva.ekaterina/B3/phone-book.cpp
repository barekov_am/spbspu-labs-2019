#include <iostream>
#include "phone-book.hpp"

void PhoneBook::view(iterator iter) const
{
  std::cout << iter->number << " " << iter->name << std::endl;
}

void PhoneBook::pushBack(const PhoneBook::record_t& rec)
{
  return cont_.push_back(rec);
}

bool PhoneBook::empty() const
{
  return cont_.empty();
}

PhoneBook::iterator PhoneBook::begin()
{
  return cont_.begin();
}

PhoneBook::iterator PhoneBook::end()
{
  return cont_.end();
}

PhoneBook::iterator PhoneBook::next(iterator iter)
{
  if (std::next(iter) != cont_.end())
  {
    return ++iter;
  }
  else
  {
    return iter;
  }
}

PhoneBook::iterator PhoneBook::prev(iterator iter)
{
  if (iter != cont_.begin())
  {
    return --iter;
  }
  else
  {
    return iter;
  }
}

PhoneBook::iterator PhoneBook::insert(iterator iter, const PhoneBook::record_t& rec)
{
  return cont_.insert(iter, rec);
}

PhoneBook::iterator PhoneBook::remove(iterator iter)
{
  return cont_.erase(iter);
}

PhoneBook::iterator PhoneBook::move(iterator iter, int n)
{
  if (n >= 0)
  {
    while (std::next(iter) != cont_.end() && (n > 0))
    {
      iter = next(iter);
      --n;
    }
  }
  else
  {
    while (iter != cont_.begin() && (n < 0))
    {
      iter = prev(iter);
      ++n;
    }
  }
  return iter;
}
