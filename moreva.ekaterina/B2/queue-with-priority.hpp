#ifndef QUEUE_WITH_PRIORITY_HPP
#define QUEUE_WITH_PRIORITY_HPP

#include <list>
#include <iostream>

template<typename T>
class QueueWithPriority
{
public:

  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  QueueWithPriority();

  QueueWithPriority(const QueueWithPriority &queue) = default;
  QueueWithPriority(QueueWithPriority &&queue) noexcept = default;
  ~QueueWithPriority() = default;
  QueueWithPriority &operator=(const QueueWithPriority<T> &queue) = default;
  QueueWithPriority &operator=(QueueWithPriority<T> &&queue) noexcept = default;

  void putElement(const T &element, ElementPriority priority);
  T getElement();
  void accelerate();
  bool empty() const;

private:
  std::list <T> low_;
  std::list <T> normal_;
  std::list <T> high_;
};

template<typename T>
QueueWithPriority<T>::QueueWithPriority() :
  low_(),
  normal_(),
  high_()
{}

template<typename T>
void QueueWithPriority<T>::putElement(const T &element, ElementPriority priority)
{
  switch (priority)
  {
    case ElementPriority::LOW:
    {
      low_.push_back(element);
      break;
    }
    case ElementPriority::NORMAL:
    {
      normal_.push_back(element);
      break;
    }
    case ElementPriority::HIGH:
    {
      high_.push_back(element);
      break;
    }
  }
}

template<typename T>
T QueueWithPriority<T>::getElement()
{
  if (!high_.empty())
  {
    T temp = high_.front();
    high_.pop_front();
    return temp;
  }
  else if (!normal_.empty())
  {
    T temp = normal_.front();
    normal_.pop_front();
    return temp;
  }
  else
  {
    T temp = low_.front();
    low_.pop_front();
    return temp;
  }
}

template<typename T>
void QueueWithPriority<T>::accelerate()
{
  high_.splice(high_.end(), low_);
}

template<typename T>
bool QueueWithPriority<T>::empty() const
{
  return (low_.empty() && normal_.empty() && high_.empty());
}

#endif //QUEUE_WITH_PRIORITY_HTTP
