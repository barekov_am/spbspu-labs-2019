#include <algorithm>
#include <functional>
#include <sstream>
#include <map>
#include "queue-with-priority.hpp"

void addArguments(QueueWithPriority<std::string> &queue, std::stringstream &input);
void getArguments(QueueWithPriority<std::string> &queue, std::stringstream &in);
void accelerateArguments(QueueWithPriority<std::string> &queue, std::stringstream &in);

void task1()
{
  using executeCommand = std::function<void(QueueWithPriority<std::string> &, std::stringstream &)>;

  std::map<const std::string, executeCommand> commands =
    {
      {"add", addArguments},
      {"get", getArguments},
      {"accelerate", accelerateArguments}
    };

  QueueWithPriority<std::string> queue;

  std::string line;

  while (std::getline(std::cin, line))
  {
    std::stringstream input(line);
    std::string command_name;
    input >> command_name;

    auto check = [&](const std::pair<const std::string, executeCommand> &pair)
    { return (pair.first == command_name); };

    auto command = std::find_if(std::begin(commands), std::end(commands), check);

    if (command != std::end(commands))
    {
      command->second(queue, input);
    }
    else
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }
  }
}
