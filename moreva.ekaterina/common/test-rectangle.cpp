#include <boost/test/auto_unit_test.hpp>
#include <iostream>
#include <cmath>
#include "rectangle.hpp"

#define INACCURACY 0.001

BOOST_AUTO_TEST_SUITE(TestForRectangle)

BOOST_AUTO_TEST_CASE(PermanenceOfWidthAfterMoving)
{
  moreva::Rectangle rectangle({10, 10}, 8, 8);
  const double width = rectangle.getFrameRect().width;
  rectangle.move({20, 20});
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, width, INACCURACY);

  rectangle.move(30, 30);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, width, INACCURACY);
}

BOOST_AUTO_TEST_CASE(PermanenceOfHeightAfterMoving)
{
  moreva::Rectangle rectangle({10, 10}, 8, 8);
  const double height = rectangle.getFrameRect().height;
  rectangle.move({20, 20});
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, height, INACCURACY);

  rectangle.move(30, 30);
  BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(PermanenceOfAreaAfterMoving)
{
  moreva::Rectangle rectangle({10, 10}, 8, 8);
  const double area = rectangle.getArea();
  rectangle.move({20, 20});
  BOOST_CHECK_CLOSE(rectangle.getArea(), area, INACCURACY);

  rectangle.move(30, 30);
  BOOST_CHECK_CLOSE(rectangle.getArea(), area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(ScalingIncreaseOfArea)
{
  moreva::Rectangle rectangle({10, 10}, 8, 8);
  const double area = rectangle.getArea();
  const double increase_value = 4;
  rectangle.scale(increase_value);
  BOOST_CHECK_CLOSE(rectangle.getArea(), increase_value * increase_value * area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(ScalingDecreaseOfArea)
{
  moreva::Rectangle rectangle({10, 10}, 8, 8);
  const double area = rectangle.getArea();
  const double decrease_value = 0.5;
  rectangle.scale(decrease_value);
  BOOST_CHECK_CLOSE(rectangle.getArea(), area * decrease_value * decrease_value, INACCURACY);
}

BOOST_AUTO_TEST_CASE(ScaleTest)
{
  moreva::Rectangle rectangle({ 10, 10}, 8, 8);
  BOOST_CHECK_THROW(rectangle.scale(-10), std::invalid_argument);
  BOOST_CHECK_THROW(rectangle.scale(0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(rectangleRotate)
{
  moreva::Rectangle rectangle({0, 0}, 2, 2);
  const double areaBefore = rectangle.getArea();
  const moreva::rectangle_t frameBefore = rectangle.getFrameRect();

  const double angle = 90;
  rectangle.rotate(angle);

  double areaAfter = rectangle.getArea();
  moreva::rectangle_t frameAfter = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, INACCURACY);
  BOOST_CHECK_CLOSE(frameBefore.pos.x, frameAfter.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(frameBefore.pos.y, frameAfter.pos.y, INACCURACY);
}


BOOST_AUTO_TEST_CASE(TestOfIncorrectWidth)
{
  BOOST_CHECK_THROW(moreva::Rectangle rectangle({10, 10}, -8, 8), std::invalid_argument);
  BOOST_CHECK_THROW(moreva::Rectangle rectangle({10, 10}, 0, 8), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestOfIncorrectHeight)
{
  BOOST_CHECK_THROW(moreva::Rectangle rectangle({10, 10}, 8, -8), std::invalid_argument);
  BOOST_CHECK_THROW(moreva::Rectangle rectangle({10, 10}, 8, 0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
