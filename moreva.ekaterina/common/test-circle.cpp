#include <boost/test/auto_unit_test.hpp>
#include <iostream>
#include <cmath>
#include "circle.hpp"

#define INACCURACY 0.001

BOOST_AUTO_TEST_SUITE(TestForCircle)

BOOST_AUTO_TEST_CASE(permanenceOfRadiusAfterMovingCenter)
{
  moreva::Circle circle({8, 8}, 10);
  const double radius = circle.getRadius();
  circle.move({30, 30});
  BOOST_CHECK_CLOSE(circle.getRadius(), radius, INACCURACY);

  circle.move(20, 20);
  BOOST_CHECK_CLOSE(circle.getRadius(), radius, INACCURACY);
}

BOOST_AUTO_TEST_CASE(permanenceOfAreaAfterMovingCenter)
{
  moreva::Circle circle({8, 8}, 10);
  const double area = circle.getArea();
  circle.move({30, 30});
  BOOST_CHECK_CLOSE(circle.getArea(), area, INACCURACY);

  circle.move(20, 20);
  BOOST_CHECK_CLOSE(circle.getArea(), area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(ScalingIncreaseOfArea)
{
  moreva::Circle circle({8, 8}, 10);
  const double area = circle.getArea();
  const double increase_value = 4;
  circle.scale(increase_value);
  BOOST_CHECK_CLOSE(circle.getArea(), increase_value * increase_value * area, INACCURACY);
}

BOOST_AUTO_TEST_CASE(ScalingDecreaseOfArea)
{
  moreva::Circle circle({8, 8}, 10);
  const double area = circle.getArea();
  const double decrease_value = 0.5;
  circle.scale(decrease_value);
  BOOST_CHECK_CLOSE(circle.getArea(), area * decrease_value * decrease_value, INACCURACY);
}

BOOST_AUTO_TEST_CASE(ScaleTest)
{
  moreva::Circle circle({8, 8}, 10);
  BOOST_CHECK_THROW(circle.scale(-5), std::invalid_argument);
  BOOST_CHECK_THROW(circle.scale(0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(circleRotate)
{
  moreva::Circle circle({0, 0}, 2);
  const double areaBefore = circle.getArea();
  const moreva::rectangle_t frameBefore = circle.getFrameRect();

  const double angle = 90;
  circle.rotate(angle);

  double areaAfter = circle.getArea();
  moreva::rectangle_t frameAfter = circle.getFrameRect();
  BOOST_CHECK_CLOSE(areaBefore, areaAfter, INACCURACY);
  BOOST_CHECK_CLOSE(frameBefore.pos.x, frameAfter.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(frameBefore.pos.y, frameAfter.pos.y, INACCURACY);
}


BOOST_AUTO_TEST_CASE(TestOfIncorrectRadius)
{
  BOOST_CHECK_THROW(moreva::Circle circle({8, 8}, -10), std::invalid_argument);
  BOOST_CHECK_THROW(moreva::Circle circle({8, 8}, 0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
