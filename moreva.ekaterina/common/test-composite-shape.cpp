#include <boost/test/auto_unit_test.hpp>
#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

#define INACCURACY 0.001
using p_shape = std::shared_ptr<moreva::Shape>;
BOOST_AUTO_TEST_SUITE(TestForCompositeShape)

BOOST_AUTO_TEST_CASE(CopyConstructor)
{
  moreva::Circle circle(10, 10, 8);
  moreva::Rectangle rectangle({10, 10}, 8, 8);
  moreva::CompositeShape compositeShape1;
  p_shape p_circle = std::make_shared<moreva::Circle>(circle);
  p_shape p_rect = std::make_shared<moreva::Rectangle>(rectangle);

  compositeShape1.add(p_circle);
  compositeShape1.add(p_rect);


  moreva::CompositeShape compositeShape2(compositeShape1);
  moreva::rectangle_t boundedRect1 = compositeShape1.getFrameRect();
  moreva::rectangle_t boundedRect2 = compositeShape2.getFrameRect();

  BOOST_CHECK_EQUAL(compositeShape1.getCount(), compositeShape2.getCount());
  BOOST_CHECK_CLOSE(boundedRect1.pos.x, boundedRect2.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(boundedRect1.pos.y, boundedRect2.pos.y, INACCURACY);
  BOOST_CHECK_CLOSE(boundedRect1.width, boundedRect2.width, INACCURACY);
  BOOST_CHECK_CLOSE(boundedRect1.height, boundedRect2.height, INACCURACY);
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), compositeShape2.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(movingConstructor)
{
  moreva::Circle circle(10, 10, 8);
  moreva::Rectangle rectangle({10, 10}, 8, 8);
  moreva::CompositeShape compositeShape;
  p_shape p_circle = std::make_shared<moreva::Circle>(circle);
  p_shape p_rect = std::make_shared<moreva::Rectangle>(rectangle);

  compositeShape.add(p_circle);
  compositeShape.add(p_rect);

  moreva::CompositeShape compositeShape1;
  compositeShape1.add(p_circle);
  compositeShape1.add(p_rect);

  moreva::CompositeShape compositeShape2(std::move(compositeShape));

  moreva::rectangle_t boundedRect1 = compositeShape1.getFrameRect();
  moreva::rectangle_t boundedRect2 = compositeShape2.getFrameRect();

  BOOST_CHECK_EQUAL(compositeShape1.getCount(), compositeShape2.getCount());
  BOOST_CHECK_CLOSE(boundedRect1.pos.x, boundedRect2.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(boundedRect1.pos.y, boundedRect2.pos.y, INACCURACY);
  BOOST_CHECK_CLOSE(boundedRect1.width, boundedRect2.width, INACCURACY);
  BOOST_CHECK_CLOSE(boundedRect1.height, boundedRect2.height, INACCURACY);
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), compositeShape2.getArea(), INACCURACY);
}


BOOST_AUTO_TEST_CASE(CopyingOperatorTest)
{
  moreva::Circle circle(10, 10, 8);
  moreva::CompositeShape compositeShape1;
  p_shape p_circle = std::make_shared<moreva::Circle>(circle);
  compositeShape1.add(p_circle);
  moreva::CompositeShape compositeShape2 = compositeShape1;
  moreva::rectangle_t boundedRect1 = compositeShape1.getFrameRect();
  moreva::rectangle_t boundedRect2 = compositeShape2.getFrameRect();

  BOOST_CHECK_EQUAL(compositeShape1.getCount(), compositeShape2.getCount());
  BOOST_CHECK_CLOSE(boundedRect1.pos.x, boundedRect2.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(boundedRect1.pos.y, boundedRect2.pos.y, INACCURACY);
  BOOST_CHECK_CLOSE(boundedRect1.width, boundedRect2.width, INACCURACY);
  BOOST_CHECK_CLOSE(boundedRect1.height, boundedRect2.height, INACCURACY);
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), compositeShape2.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(MovingOperatorTest)
{
  moreva::Circle circle(10, 10, 8);
  moreva::Rectangle rectangle({10, 10}, 8, 8);
  moreva::CompositeShape compositeShape;
  p_shape p_circle = std::make_shared<moreva::Circle>(circle);
  compositeShape.add(p_circle);

  moreva::CompositeShape compositeShape1;
  compositeShape1.add(p_circle);

  moreva::CompositeShape compositeShape2 = std::move(compositeShape);

  moreva::rectangle_t boundedRect1 = compositeShape1.getFrameRect();
  moreva::rectangle_t boundedRect2 = compositeShape2.getFrameRect();

  BOOST_CHECK_EQUAL(compositeShape1.getCount(), compositeShape2.getCount());
  BOOST_CHECK_CLOSE(boundedRect1.pos.x, boundedRect2.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(boundedRect1.pos.y, boundedRect2.pos.y, INACCURACY);
  BOOST_CHECK_CLOSE(boundedRect1.width, boundedRect2.width, INACCURACY);
  BOOST_CHECK_CLOSE(boundedRect1.height, boundedRect2.height, INACCURACY);
  BOOST_CHECK_CLOSE(compositeShape1.getArea(), compositeShape2.getArea(), INACCURACY);
}

BOOST_AUTO_TEST_CASE(PermanenceOfTheParametersAfterMoving)
{
  moreva::Circle circle(10, 10, 8);
  moreva::Rectangle rectangle({10, 10}, 8, 8);
  moreva::CompositeShape compositeShape;
  p_shape p_circle = std::make_shared<moreva::Circle>(circle);
  p_shape p_rect = std::make_shared<moreva::Rectangle>(rectangle);
  compositeShape.add(p_circle);
  compositeShape.add(p_rect);

  const moreva::rectangle_t InitFrameRect = compositeShape.getFrameRect();
  const double InitArea = compositeShape.getArea();
  const int number = compositeShape.getCount();

  compositeShape.move({20, 20});
  BOOST_CHECK_EQUAL(compositeShape.getCount(), number);
  BOOST_CHECK_CLOSE(InitFrameRect.width, compositeShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(InitFrameRect.height, compositeShape.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(InitArea, compositeShape.getArea(), INACCURACY);

  compositeShape.move(30, 30);
  BOOST_CHECK_EQUAL(compositeShape.getCount(), number);
  BOOST_CHECK_CLOSE(InitFrameRect.width, compositeShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(InitFrameRect.height, compositeShape.getFrameRect().height, INACCURACY);
  BOOST_CHECK_CLOSE(InitArea, compositeShape.getArea(), INACCURACY);
}


BOOST_AUTO_TEST_CASE(ScalingIncreaseOfArea)
{
  const double factor = 3;
  moreva::Circle circle(10, 10, 8);
  moreva::Rectangle rectangle({10, 10}, 8, 8);
  moreva::CompositeShape compositeShape;

  p_shape p_circle = std::make_shared<moreva::Circle>(circle);
  p_shape p_rect = std::make_shared<moreva::Rectangle>(rectangle);
  compositeShape.add(p_circle);
  compositeShape.add(p_rect);

  const double InitArea = compositeShape.getArea();
  const moreva::rectangle_t InitFrameRect = compositeShape.getFrameRect();
  const int number = compositeShape.getCount();

  compositeShape.scale(factor);
  BOOST_CHECK_CLOSE(InitArea * factor * factor, compositeShape.getArea(), INACCURACY);

  BOOST_CHECK_EQUAL(compositeShape.getCount(), number);
  BOOST_CHECK_CLOSE(InitFrameRect.width * factor, compositeShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(InitFrameRect.height * factor, compositeShape.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(ScalingDecreaseOfArea)
{
  const double factor = 0.5;
  moreva::Circle circle(10, 10, 8);
  moreva::Rectangle rectangle({10, 10}, 8, 8);
  moreva::CompositeShape compositeShape;

  p_shape p_circle = std::make_shared<moreva::Circle>(circle);
  p_shape p_rect = std::make_shared<moreva::Rectangle>(rectangle);
  compositeShape.add(p_circle);
  compositeShape.add(p_rect);

  const double InitArea = compositeShape.getArea();
  const moreva::rectangle_t InitFrameRect = compositeShape.getFrameRect();
  const int number = compositeShape.getCount();

  compositeShape.scale(factor);
  BOOST_CHECK_CLOSE(InitArea * factor * factor, compositeShape.getArea(), INACCURACY);

  BOOST_CHECK_EQUAL(compositeShape.getCount(), number);
  BOOST_CHECK_CLOSE(InitFrameRect.width * factor, compositeShape.getFrameRect().width, INACCURACY);
  BOOST_CHECK_CLOSE(InitFrameRect.height * factor, compositeShape.getFrameRect().height, INACCURACY);
}

BOOST_AUTO_TEST_CASE(AdditingFigure)
{
  moreva::Circle circle(10, 10, 8);
  moreva::CompositeShape compositeShape;
  p_shape p_circle = std::make_shared<moreva::Circle>(circle);
  compositeShape.add(p_circle);

  moreva::rectangle_t boundedRect = compositeShape.getFrameRect();

  BOOST_CHECK_EQUAL(compositeShape.getCount(), 1);
  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.x, boundedRect.pos.x, INACCURACY);
  BOOST_CHECK_CLOSE(circle.getFrameRect().pos.y, boundedRect.pos.y, INACCURACY);
  BOOST_CHECK_CLOSE(circle.getFrameRect().width, boundedRect.width, INACCURACY);
  BOOST_CHECK_CLOSE(circle.getFrameRect().height, boundedRect.height, INACCURACY);
  BOOST_CHECK_CLOSE(circle.getArea(), compositeShape.getArea(), INACCURACY);

  BOOST_CHECK_THROW(compositeShape.add(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(DeletingFigures)
{
  moreva::Circle circle1(10, 10, 8);
  moreva::Circle circle2(10, 10, 10);
  moreva::Rectangle rectangle1({10, 10}, 8, 8);
  moreva::Rectangle rectangle2({10, 10}, 8, 8);
  moreva::CompositeShape compositeShape;
  p_shape p_circle1 = std::make_shared<moreva::Circle>(circle1);
  p_shape p_rect1 = std::make_shared<moreva::Rectangle>(rectangle1);
  p_shape p_circle2 = std::make_shared<moreva::Circle>(circle2);
  p_shape p_rect2 = std::make_shared<moreva::Rectangle>(rectangle2);
  compositeShape.add(p_circle1);
  compositeShape.add(p_rect1);
  compositeShape.add(p_circle2);

  compositeShape.remove(0);
  BOOST_CHECK_THROW(compositeShape.remove(p_circle1), std::invalid_argument);

  compositeShape.remove(p_circle2);
  BOOST_CHECK_THROW(compositeShape.remove(p_circle2), std::invalid_argument);

  BOOST_CHECK_THROW(compositeShape.remove(p_rect2), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.remove(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.remove(-3), std::out_of_range);
  BOOST_CHECK_THROW(compositeShape.remove(3), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(ScaleExceptions)
{
  moreva::Circle circle(10, 10, 8);
  moreva::CompositeShape compositeShape;

  const double wrongFactor1 = -3;
  const double wrongFactor2 = 0;
  p_shape p_circle = std::make_shared<moreva::Circle>(circle);
  compositeShape.add(p_circle);
  BOOST_CHECK_THROW(compositeShape.scale(wrongFactor1), std::invalid_argument);
  BOOST_CHECK_THROW(compositeShape.scale(wrongFactor2), std::invalid_argument);

}

BOOST_AUTO_TEST_CASE(EmptyFigure)
{
  moreva::CompositeShape compositeShape;
  BOOST_CHECK_THROW(compositeShape.move(1, 2), std::logic_error);
  BOOST_CHECK_THROW(compositeShape.move({-1, 0}), std::logic_error);

  const double Factor = 3;
  const double wrongFactor1 = -3;
  const double wrongFactor2 = 0;
  BOOST_CHECK_THROW(compositeShape.scale(Factor), std::logic_error);
  BOOST_CHECK_THROW(compositeShape.scale(wrongFactor1), std::logic_error);
  BOOST_CHECK_THROW(compositeShape.scale(wrongFactor2), std::logic_error);
}

BOOST_AUTO_TEST_SUITE_END()

