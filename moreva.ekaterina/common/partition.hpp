#ifndef PARTITION_HPP
#define PARTITION_HPP

#include "matrix.hpp"
#include "composite-shape.hpp"

using p_shape = std::shared_ptr<moreva::Shape>;
namespace moreva
{
  bool isOverlapping(p_shape shape1, p_shape shape2);
  Matrix part(const CompositeShape & composite_shape);
}

#endif // PARTITION_HPP
