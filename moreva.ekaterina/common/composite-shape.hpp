#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace moreva
{
  class CompositeShape : public Shape
  {
  public:
    using p_shape = std::shared_ptr<Shape>;

    CompositeShape();
    CompositeShape(const CompositeShape & shapes);
    CompositeShape(CompositeShape && shapes);
    ~CompositeShape() = default;

    CompositeShape & operator =(const CompositeShape & shapes);
    CompositeShape & operator =(CompositeShape && shapes);
    p_shape operator [](size_t index) const;

    bool operator ==(const CompositeShape & rhs) const;
    bool operator !=(const CompositeShape & rhs) const;

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & pos) override;
    void move(double dltX, double dltY) override;
    void scale(double factor) override;
    void rotate(double angle) override;
    void printInfo() const override;

    void add(p_shape shape);
    void remove(p_shape shape);
    void remove(size_t index);
    size_t getCount() const;
    point_t getCenter() const override;

  private:
   size_t count_;
   std::unique_ptr<p_shape[]> figures_;
  };
}

#endif // COMPOSITESHAPE_HPP
