#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP
#include <memory>
namespace moreva
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    double width, height;
    point_t pos;
  };

struct pair_t
{
    size_t line, column;
};
}

#endif //BASE_TYPES_HPP
