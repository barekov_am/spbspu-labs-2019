#include "composite-shape.hpp"
#include <algorithm>
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <utility>


moreva::CompositeShape::CompositeShape() :
  count_(0)

{}

moreva::CompositeShape::CompositeShape(const moreva::CompositeShape & shapes) :
  count_(shapes.count_),
  figures_(std::make_unique<p_shape[]>(shapes.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    figures_[i] = shapes.figures_[i];
  }
}


moreva::CompositeShape::CompositeShape(moreva::CompositeShape && shapes) :
  count_(shapes.count_),
  figures_(std::move(shapes.figures_))
{
  shapes.count_ = 0;
}

moreva::CompositeShape & moreva::CompositeShape::operator =(const CompositeShape & shapes)
{
  if (this != &shapes)
  {
    count_ = shapes.count_;
    std::unique_ptr<p_shape[]> newShapeArray(std::make_unique<p_shape[]>(shapes.count_));

    for (size_t i = 0; i < count_; i++)
    {
      figures_[i] = shapes.figures_[i];
    }
    figures_.swap(newShapeArray);
  }
  return *this;
}

moreva::CompositeShape & moreva::CompositeShape::operator =(CompositeShape && shapes)
{
  if (this != &shapes)
  {
    count_ = shapes.count_;
    figures_ = std::move(shapes.figures_);
    shapes.count_ = 0;
  }
  return *this;
}

moreva::CompositeShape::p_shape moreva::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range.");
  }
  return figures_[index];
}


double moreva::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < count_; i++)
  {
    area += figures_[i]->getArea();
  }
  return area;
}

moreva::rectangle_t moreva::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty. You can't get a frame rectangle.");
  }

  const rectangle_t Rect1 = figures_[0]->getFrameRect();
  point_t topRight_ = {Rect1.pos.x + (Rect1.width / 2), Rect1.pos.y + (Rect1.height / 2)};
  point_t bottomLeft_ = {Rect1.pos.x - (Rect1.width / 2), Rect1.pos.y - (Rect1.height / 2)};

  for (size_t i = 0; i < count_; i++)
  {
    rectangle_t rect = figures_[i]->getFrameRect();

    topRight_.x = std::max(topRight_.x, rect.pos.x + (rect.width / 2));
    topRight_.y = std::max(topRight_.y, rect.pos.y + (rect.height / 2));
    bottomLeft_.x = std::min(bottomLeft_.x, rect.pos.x - (rect.width / 2));
    bottomLeft_.y = std::min(bottomLeft_.y, rect.pos.y - (rect.height / 2));
  }
  return {topRight_.x - bottomLeft_.x, topRight_.y - bottomLeft_.y,
      {(topRight_.x + bottomLeft_.x) / 2, (topRight_.y + bottomLeft_.y) / 2 }};
}

void moreva::CompositeShape::move(const moreva::point_t & pos)
{
  rectangle_t boundedRect = getFrameRect();
  double dltX = pos.x - boundedRect.pos.x;
  double dltY = pos.y - boundedRect.pos.y;

  move(dltX, dltY);
}

void moreva::CompositeShape::move(double dltX, double dltY)
{
  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty. You can't move it.");
  }

  for (size_t i = 0; i < count_; i++)
  {
    figures_[i]->move(dltX, dltY);
  }
}

void moreva::CompositeShape::scale(double factor)
{
  if (factor <= 0)
  {
    throw std::invalid_argument("Scale amount must be positive.");
  }

  if (count_ == 0)
  {
    throw std::logic_error("Composite shape is empty. You can't scale it.");
  }

  rectangle_t boundedRect = getFrameRect();

  for (size_t i = 0; i < count_; i++)
  {
    double dltX = figures_[i]->getFrameRect().pos.x - boundedRect.pos.x;
    double dltY = figures_[i]->getFrameRect().pos.y - boundedRect.pos.y;

    figures_[i]->move(dltX * (factor - 1), dltY * (factor - 1));
    figures_[i]->scale(factor);
  }
}

void moreva::CompositeShape::rotate(double angle)
{
  const point_t center = getCenter();
  const double cos_angle = cos(angle);
  const double sin_angle = sin(angle);

  for (size_t i = 0; i < count_; i++)
  {
    const point_t other_center = figures_[i]->getCenter();
    double point_x = {center.x + (other_center.x - center.x) * cos_angle
        - (other_center.y - center.y) * sin_angle};
    double point_y = {center.y + (other_center.x - center.x) * sin_angle
        - (other_center.y - center.y) * cos_angle};

    figures_[i]->move({point_x, point_y});
    figures_[i]->rotate(angle);
  }
}

moreva::point_t moreva::CompositeShape::getCenter() const
{
  moreva::rectangle_t frame_rect = figures_[0]->getFrameRect();
  double max_x = frame_rect.pos.x + frame_rect.width / 2;
  double min_x = frame_rect.pos.x - frame_rect.width / 2;
  double max_y = frame_rect.pos.y + frame_rect.height / 2;
  double min_y = frame_rect.pos.y - frame_rect.height / 2;

  for (size_t i = 0; i < count_; i++)
  {
    frame_rect = figures_[i]->getFrameRect();
    max_x = std::max(max_x, frame_rect.pos.x + frame_rect.width / 2);
    min_x = std::min(min_x, frame_rect.pos.x - frame_rect.width / 2);
    max_y = std::max(max_y, frame_rect.pos.y + frame_rect.height / 2);
    min_y = std::min(min_y, frame_rect.pos.y - frame_rect.height / 2);
  }
  return {(max_x + min_x) / 2, (max_y + min_y) / 2};
}


void moreva::CompositeShape::printInfo() const
{
  if (count_ == 0)
  {
    std::cout << "Composite shape is empty." << std::endl << std::endl;
    return;
  }
  const rectangle_t boundedRect = getFrameRect();
  std::cout << "Composite shape. It's center is at: (" << boundedRect.pos.x << "; " << boundedRect.pos.y << ")"
            << std::endl << "It includes " << count_ << " figures"
            << std::endl << "Area is: " << getArea()
            << std::endl << "Bounded rectangle has width = " << boundedRect.width
            << std::endl << "Bounded rectangle has height = " << boundedRect.height
            << std::endl << std::endl;
}

void moreva::CompositeShape::add(p_shape shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape must not be a null pointer.");
  }
  std::unique_ptr<p_shape[]> newShapeArray(std::make_unique<p_shape[]>(count_ + 1));

  for (size_t i = 0; i < count_; i++)
  {
    newShapeArray[i] = figures_[i];
  }
  newShapeArray[count_] = shape;
  count_++;
  figures_.swap(newShapeArray);
}

void moreva::CompositeShape::remove(p_shape shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape must not be a null pointer.");
  }

  for (size_t i = 0; i < count_; i++)
  {
    if (shape == figures_[i])
    {
      remove(i);
      return;
    }
  }
  throw std::invalid_argument("This shape does not exist in composite shape.");
}


void moreva::CompositeShape::remove(size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }

 std::unique_ptr<p_shape[]> newShapeArray(std::make_unique<p_shape[]>(count_ - 1));

  for (size_t i = 0; i < index; i++)
  {
    newShapeArray[i] = figures_[i];
  }
  count_--;

  for (size_t i = index; i < count_; i++)
  {
    newShapeArray[i] = figures_[i + 1];
  }
  figures_.swap(newShapeArray);
}

size_t moreva::CompositeShape::getCount() const
{
  return count_;
}
