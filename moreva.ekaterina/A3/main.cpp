#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  moreva::Circle circle1(5, 5, 5);
  moreva::Circle circle2(5, 5, 4);
  moreva::Circle circle3(5, 5, 6);
  moreva::Rectangle rectangle1({5, 5}, 20, 30);
  moreva::Rectangle rectangle2({5, 5}, 10, 20);
  moreva::Rectangle rectangle3({5, 5}, 30, 40);
  
  moreva::CompositeShape::p_shape p_circle1 = std::make_shared<moreva::Circle>(circle1);
  moreva::CompositeShape::p_shape p_circle2 = std::make_shared<moreva::Circle>(circle2);
  moreva::CompositeShape::p_shape p_circle3 = std::make_shared<moreva::Circle>(circle3);
  moreva::CompositeShape::p_shape p_rect1 = std::make_shared<moreva::Rectangle>(rectangle1);
  moreva::CompositeShape::p_shape p_rect2 = std::make_shared<moreva::Rectangle>(rectangle2);
  moreva::CompositeShape::p_shape p_rect3 = std::make_shared<moreva::Rectangle>(rectangle3);

  moreva::CompositeShape compositeShape;
  compositeShape.add(p_circle1);
  compositeShape.add(p_circle2);
  compositeShape.add(p_circle3);
  compositeShape.add(p_rect1);
  compositeShape.add(p_rect2);
  compositeShape.add(p_rect3);
  compositeShape.printInfo();

  compositeShape.scale(3);
  compositeShape.move({15, 15});
  compositeShape.printInfo();

  compositeShape.move(-15, -15);
  compositeShape.printInfo();

  moreva::CompositeShape compositeShape2(std::move(compositeShape));
  compositeShape2.printInfo();

  compositeShape2.remove(p_rect3);
  compositeShape2.remove(0);
  compositeShape2.printInfo();

  compositeShape2.move({20, 30});
  compositeShape2.scale(0.5);
  compositeShape2.printInfo();

  moreva::CompositeShape compositeShape3(compositeShape2);
  compositeShape3.move(-10, -10);
  compositeShape3.printInfo();

  return 0;
}
