#include <iostream>
#include <cstring>

const size_t defaultLength = 40;
const size_t minLength = 25;

void task(size_t length, std::istream& input, std::ostream& output);

int main(int argc, char *argv[])
{
  try
  {
    if ((argc != 1 && argc != 3) || (argc == 3 && std::strcmp(argv[1], "--line-width") != 0))
    {
      throw std::invalid_argument("Invalid number of arguments.");
    }
    size_t length = defaultLength;
    if (argc == 3)
    {
      length = std::stoi(argv[2]);
      if (length < minLength)
      {
        throw std::invalid_argument("Invalid requested string size.");
      }
    }
    task(length, std::cin, std::cout);
  }

  catch (std::invalid_argument &e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (std::ios_base::failure &e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}
