#include "symbol.hpp"
#include <iostream>
#include <stdexcept>

const size_t maxWorldLength = 20;
const char PlUS = '+';
const char HYPHEN = '-';


Symbol::Symbol()
{ }

std::istream& operator >>(std::istream& input, Symbol& symbol)
{
  char prevCar = 0;
  prevCar = input.get();
  symbol.type = WORD;
  symbol.data_.clear();
  if (input.fail())
  {
    return input;
  }
  auto semi = std::use_facet<std::numpunct<char>>(std::locale()).decimal_point();
  while (std::isspace(prevCar, std::locale()))
  {
    prevCar = input.get();
  }
  if (std::isalpha(prevCar, std::locale()))
  {
    input.unget();
    std::string str;
    while (std::isalpha<char>(input.peek(), std::locale()) || input.peek() == HYPHEN)
    {
      const char next = input.get();
      if(next != HYPHEN)
      {
        str += next;
        continue;
      }
      if(input.peek() != HYPHEN)
      {
        str += next;

      } else {
        input.unget();
        break;
      }

    }
    symbol.data_ = str;
    symbol.type = WORD;
  }
  else if(prevCar == HYPHEN)
  {
    if (input.peek() == HYPHEN)
    {
      input.unget();
      std::string str;
      while ((input.peek() == HYPHEN))
      {
        const char ch = input.get();
        str += ch;
      }
      symbol.data_ = str;
      symbol.type = DASH;

    }
    else
    {
      input.unget();
      std::string str;
      symbol.data_= str;
      char ch = input.get();
      if (ch == HYPHEN)
      {
        str += ch;
      }
      else
      {
        input.unget();
      }
      auto isPoint = false;
      while (std::isdigit<char>(input.peek(), std::locale()) || input.peek() == semi)
      {
        ch = input.get();
        if (ch == semi)
        {
          if (isPoint)
          {
            input.unget();
            break;
          }
          isPoint = true;
        }
        str += ch;
      }
      symbol.data_ = str;
      symbol.type = DIGIT;
    }
  }
  else if (prevCar == PlUS || isdigit(prevCar, std::locale()))
  {
    input.unget();
    std::string str;
    char ch = input.get();
    if (ch == PlUS)
    {
      str += ch;
    }
    else
    {
      input.unget();
    }
    auto isPoint = false;
    while (std::isdigit<char>(input.peek(), std::locale()) || input.peek() == semi)
    {
      ch = input.get();
      if (ch == semi)
      {
        if (isPoint)
        {
          input.unget();
          break;
        }
        isPoint = true;
      }
      str += ch;
    }
    symbol.data_ = str;
    symbol.type = DIGIT;
  }
  else if (ispunct(prevCar, std::locale()))
  {
    std::string str;
    str+=prevCar;
    symbol.type = PUNCT;
    symbol.data_ = str;
  }
  return input;
}


bool Symbol::isTextCorrect(std::list<Symbol>& text)
{
  for(auto iterator = text.begin(); iterator != text.end(); ++iterator)
  {
    if(iterator->type == symbolType::DASH)
    {
      if (iterator->data_.size() != 3)
      {
        return false;
      }

      if (iterator != text.begin())
      {
        const Symbol & prev = *std::prev(iterator);
        const Symbol & next = *std::next(iterator);
        if ((prev.type == symbolType::DASH || (prev.type == symbolType::PUNCT && prev.data_ != ","))
            || (next.type == symbolType::DASH || next.type == symbolType::PUNCT))
        {
          return false;
        }
      }
      else
      {
        return false;
      }
    }

    if (iterator->type == symbolType::WORD)
    {
      if (iterator->data_.size() > maxWorldLength)
      {
        return false;

      }
    }

    if (iterator->type == symbolType::DIGIT)
    {
      if (iterator->data_.size() > maxWorldLength)
      {
        return false;

      }
    }
    if (iterator->type == symbolType::PUNCT)
    {
      if (iterator != text.begin())
      {
        const Symbol & prev = *std::prev(iterator);
        const Symbol & next = *std::next(iterator);
        if (prev.type == symbolType::DASH || prev.type == symbolType::PUNCT
            || (iterator->data_ == "-" && next.type == symbolType::WORD))
        {
          return false;
        }
      }
      else
      {
        return false;
      }
    }
  }
  return true;
}

std::size_t Symbol::remakeLine(std::list<Symbol>& line)
{
  std::size_t length = 0;
  std::list<Symbol> tmpLine;
  while (!line.empty())
  {
    tmpLine.insert(tmpLine.begin(), line.back());
    length += line.back().data_.size();
    line.pop_back();
    if (tmpLine.front().type == symbolType::WORD || tmpLine.front().type == symbolType::DIGIT)
    {
      break;
    }
  }
  writeLine(line);
  line = tmpLine;
  return length;
}

void Symbol::reformatText(const std::size_t length, std::list<Symbol>& text)
{
  std::size_t current = 0;
  std::list<Symbol> line;
  for (auto & iterator : text)
  {
    if (iterator.type == symbolType::PUNCT)
    {
      if (current + 1 > length)
      {
        current = remakeLine(line);
      }
      line.push_back(iterator);
      current += iterator.data_.size();
    }

    if (iterator.type == symbolType::DASH)
    {
      if (current + 4 > length)
      {
        current = remakeLine(line);
      }
      Symbol symbol;
      symbol.data_= " ";
      symbol.type = DASH;
      line.push_back(symbol);
      line.push_back(iterator);
      current += iterator.data_.size() + 1;
    }

    if (iterator.type == symbolType::DIGIT || iterator.type == symbolType::WORD)
    {
      if (current + iterator.data_.size() + 1 > length)
      {
        writeLine(line);
        line.clear();
        current = 0;
      }
      else if (!line.empty())
      {
        Symbol symbol;
        symbol.data_= " ";
        symbol.type = DIGIT;
        line.push_back(symbol);
        ++current;
      }
      line.push_back(iterator);
      current += iterator.data_.size();
    }
  }

  if(!line.empty())
  {
    writeLine(line);
  }
}

void writeLine (const std::list<Symbol>& line)
{
  for (const auto & iter : line)
  {
    std::cout << iter.data_;
  }
  std::cout << std::endl;
}
