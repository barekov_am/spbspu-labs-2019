#include <iostream>
#include <cstring>
#include <list>
#include "symbol.hpp"

void task(size_t length, std::istream& input, std::ostream& output)
{
  Symbol symbol;
  std::list<Symbol> text;
  while (input)
  {
    input >> symbol;
    text.push_back(symbol);
  }
  if (input.fail() && !input.eof())
  {
    throw std::ios_base::failure("Fail while reading data.");
  }

  bool isCorrect = symbol.isTextCorrect(text);

  if (!isCorrect)
  {
    throw std::invalid_argument("Invalid input");
  }
  symbol.reformatText(length, text);

  if (output.fail())
  {
    throw std::ios_base::failure("Fail while writing data");
  }
}

