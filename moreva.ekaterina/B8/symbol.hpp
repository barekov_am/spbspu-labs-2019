#ifndef SYMBOLS_HPP
#define SYMBOLS_HPP

#include <list>
#include <iterator>
#include <memory>

enum symbolType
{
  WORD,
  DIGIT,
  PUNCT,
  DASH
};

class Symbol
{
public:

  using pionter = std::pair<symbolType, std::string>;
  Symbol();
  ~Symbol() = default;
  static bool isTextCorrect(std::list<Symbol>& text);
  static std::size_t remakeLine(std::list<Symbol> &line);
  static void reformatText(std::size_t length, std::list<Symbol>& text);
  std::string data_;
  symbolType type;
};

void writeLine (const std::list<Symbol> & line);
std::istream& operator >>(std::istream& input, Symbol& symbol);

#endif // SYMBOLS_H
