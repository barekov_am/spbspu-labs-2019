#include <iostream>
#include <cmath>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"
int main()
{
  moreva::Circle circle1({5, 5}, 2);
  moreva::Circle circle2({8, 8}, 3);
  moreva::Circle circle3({8, 8}, 2);
  moreva::Circle circle4({7, 3}, 1);

  moreva::Rectangle rect1({15, 5}, 6, 6);
  moreva::Rectangle rect2({15, 5}, 4, 4);
  moreva::Rectangle rect3({20, 15}, 4, 4);
  moreva::Rectangle rect4({16, 11}, 9, 9);

  moreva::CompositeShape::p_shape p_circle1 = std::make_shared<moreva::Circle>(circle1);
  moreva::CompositeShape::p_shape p_circle2 = std::make_shared<moreva::Circle>(circle2);
  moreva::CompositeShape::p_shape p_circle3 = std::make_shared<moreva::Circle>(circle3);
  moreva::CompositeShape::p_shape p_circle4 = std::make_shared<moreva::Circle>(circle4);

  moreva::CompositeShape::p_shape p_rect1 = std::make_shared<moreva::Rectangle>(rect1);
  moreva::CompositeShape::p_shape p_rect2 = std::make_shared<moreva::Rectangle>(rect2);
  moreva::CompositeShape::p_shape p_rect3 = std::make_shared<moreva::Rectangle>(rect3);
  moreva::CompositeShape::p_shape p_rect4 = std::make_shared<moreva::Rectangle>(rect4);

  moreva::CompositeShape composite_shape;
  composite_shape.add(p_circle1);
  composite_shape.add(p_circle2);
  composite_shape.add(p_circle3);
  composite_shape.add(p_circle4);
  composite_shape.add(p_rect1);
  composite_shape.add(p_rect2);
  composite_shape.add(p_rect3);
  composite_shape.add(p_rect4);
  composite_shape.printInfo();

  moreva::Matrix matrix = moreva::part(composite_shape);
  matrix.printInfo();

  composite_shape.rotate(M_PI / 2);
  composite_shape.printInfo();

  composite_shape.scale(2);
  composite_shape.printInfo();

  matrix.printInfo();
  return 0;
}
