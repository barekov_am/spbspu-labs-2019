#include <iostream>
#include <algorithm>
#include <iterator>
#include "statistics.hpp"

int main()
{
  try
  {
    GetStatistics stat;
    stat = std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), stat);

    if (std::cin.fail() && !std::cin.eof())
    {
      std::cerr << "Error while reading data" << std::endl;
      return 1;
    }
    stat.printInfo();
  }

  catch(std::exception& exception)
  {
    exception.what();
    return 1;
  }

  return 0;
}
