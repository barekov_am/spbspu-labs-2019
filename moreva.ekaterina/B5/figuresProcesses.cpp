#include "figuresProcesses.hpp"
#include <algorithm>

Point Point::operator -() const
{
  return {-x, -y};
}

Point Point::operator -(const Point& rhs) const
{
  return *this + (-rhs);
}

Point Point::operator +(const Point& rhs) const
{
  return {x + rhs.x, y + rhs.y};
}

bool isTriangle(const Shape & shape)
{
  return (shape.size() == 3);
}

bool isRectangle(const Shape & shape)
{
  if (shape.size() != 4)
  {
    return false;
  }

  const bool horizontal1 = ((shape[0].y == shape[1].y) && (shape[3].y == shape[2].y));
  const bool horizontal2 = ((shape[0].y == shape[3].y) && (shape[1].y == shape[2].y));
  const bool vertical1 = ((shape[0].x == shape[3].x) && (shape[1].x == shape[2].x));
  const bool vertical2 = ((shape[0].x == shape[1].x) && (shape[3].x == shape[2].x));

  return (horizontal1 && vertical1) || (horizontal2 && vertical2);
}

bool isSquare(const Shape & shape)
{
  if (!isRectangle(shape))
  {
    return false;
  }

  const bool areEqual1 = (abs(shape[1].x - shape[0].x) == abs(shape[3].y - shape[0].y));
  const bool areEqual2 = (abs(shape[3].x - shape[0].x) == abs(shape[1].y - shape[0].y));

  return areEqual1 && areEqual2;
}

std::istream & operator >>(std::istream & in, Shape & shape)
{
  shape.clear();

  int n;
  in >> n;
  if (in.fail())
  {
    return in;
  }

  std::string str;
  std::getline(in, str, '\n');
  str.erase(std::remove_if(str.begin(), str.end(), [](const char c) { return isspace(c); }), str.end());

  int pos = 0;
  for (int i = 0; i < n; ++i)
  {
    Point point{};
    const int delimiterIndex = str.find(';', pos);
    const int closeBracket = str.find(')', delimiterIndex);
    if (str[pos] != '(' || delimiterIndex == -1 || closeBracket == -1)
    {
      in.setstate(std::ios::failbit);
      return in;
    }

    try
    {
      point.x = std::stoi(str.substr(pos + 1, delimiterIndex));
      point.y = std::stoi(str.substr(delimiterIndex + 1, closeBracket - 1));
    }
    catch (std::invalid_argument &)
    {
      in.setstate(std::ios::failbit);
      return in;
    }

    pos = closeBracket + 1;
    shape.push_back(point);
  }

  if (str[pos] == '(')
  {
    in.setstate(std::ios::failbit);
  }

  return in;
}

std::ostream & operator <<(std::ostream & out, const Shape & shape)
{
  out << shape.size() << " ";
  std::copy(shape.begin(), shape.end(), std::ostream_iterator<Point>(out, " "));
  return out;
}

std::ostream & operator <<(std::ostream & out, const Point & point)
{
  out << '(' << point.x << "; " << point.y << ')';
  return out;
}
