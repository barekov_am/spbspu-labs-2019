#ifndef FIGURES_HPP
#define FIGURES_HPP

#include <vector>
#include <iostream>
#include <iterator>
#include <memory>

struct Point
{
  int x;
  int y;

  Point operator -() const;
  Point operator -(const Point& rhs) const;
  Point operator +(const Point& rhs) const;

};

using Segment = Point;
using Shape = std::vector<Point>;

double squaresSum(const Segment& vector1, const Segment& vector2);
bool isTriangle(const Shape& shape);
bool isRectangle(const Shape& shape);
bool isSquare(const Shape& shape);

std::istream& operator >>(std::istream& in, Shape& shape);
std::ostream& operator <<(std::ostream& out, const Shape& shape);
std::ostream& operator <<(std::ostream& out, const Point& point);

#endif // FIGURES_HPP
