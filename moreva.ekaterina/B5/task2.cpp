#include <algorithm>
#include <vector>
#include <list>
#include <istream>
#include <iostream>
#include <iterator>

#include "figuresProcesses.hpp"

void task2()
{
  std::list<Shape> shapes;
  Shape shape;
  while (std::cin >> shape)
  {
    shapes.push_back(shape);
  }

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Error reading the data");
  }

  unsigned long long sum = 0;
  std::for_each(shapes.cbegin(), shapes.cend(), [&sum](const Shape & shape) {sum += shape.size();});

  std::cout << "Vertices: " << sum << std::endl;

  unsigned long long triangles = 0;
  triangles = std::count_if(shapes.cbegin(), shapes.cend(), [](const Shape & shape) {return isTriangle(shape);});

  std::cout << "Triangles: " << triangles << std::endl;

  unsigned long long squares = 0;
  squares = std::count_if(shapes.cbegin(), shapes.cend(), [](const Shape & shape) {return isSquare(shape);});

  std::cout << "Squares: " << squares << std::endl;

  unsigned long long rectangles = 0;
  rectangles = std::count_if(shapes.cbegin(), shapes.cend(), [](const Shape & shape) {return isRectangle(shape);});

  std::cout << "Rectangles: " << rectangles << std::endl;

  shapes.erase(
      std::remove_if(
          shapes.begin(),
          shapes.end(),
          [](const Shape & shape)
          {
            return shape.size() == 5;
          }
      ),
      shapes.end()
  );

  std::vector<Point> points;
  std::for_each(
      shapes.cbegin(),
      shapes.cend(),
      [&points](const Shape & shape)
      {
        points.push_back(*shape.begin());
      }
  );

  std::cout << "Points: ";
  std::copy(points.cbegin(), points.cend(), std::ostream_iterator<Point>(std::cout, " "));
  std::cout << std::endl;

  auto ShapesBegin = shapes.begin();
  auto ShapesEnd = shapes.end();
  shapes.erase(
      std::remove_if(
          ShapesBegin,
          ShapesEnd,
          [&shapes](const Shape & shape)
          {
            const bool result = isRectangle(shape);
            if (result)
            {
              shapes.push_front(shape);
            }
            return result;
          }
      ),
      shapes.end()
  );

  ShapesBegin = shapes.begin();
  ShapesEnd = shapes.end();
  shapes.erase(
      std::remove_if(
          ShapesBegin,
          ShapesEnd,
          [&shapes](const Shape & shape)
          {
            const bool result = isSquare(shape);
            if (result)
            {
              shapes.push_front(shape);
            }
            return result;
          }
      ),
      shapes.end()
  );

  ShapesBegin = shapes.begin();
  ShapesEnd = shapes.end();
  shapes.erase(
      std::remove_if(
          ShapesBegin,
          ShapesEnd,
          [&shapes](const Shape & shape)
          {
            const bool result = shape.size() == 3;
            if (result)
            {
              shapes.push_front(shape);
            }
            return result;
          }
      ),
      shapes.end()
  );

  std::cout << "Shapes:" << std::endl;
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator<Shape>(std::cout, "\n"));
}
