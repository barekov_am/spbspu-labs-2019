#include <iostream>
#include <set>
#include <iterator>

void task1()
{
  std::set<std::string> words;

  std::copy(std::istream_iterator<std::string>(std::cin), std::istream_iterator<std::string>(),
      std::inserter(words, words.begin()));

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::runtime_error("Error reading the data");
  }

  std::copy(words.begin(), words.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
}
