#include <iostream>
#include <string>
#include "functions.hpp"

void task2()
{
  std::vector<Shape> shapeCont;
  std::string line;
  ShapeInfo shapeInfo_;
  std::vector<Point_t> points;

  readWords(shapeCont, line);
  recieveInfo(shapeInfo_, shapeCont);
  removePentagons(shapeCont);
  createVectorPoints(shapeCont, points);
  std::sort(shapeCont.begin(), shapeCont.end(), isLess);
  printInfo(shapeCont, points, shapeInfo_);
}
