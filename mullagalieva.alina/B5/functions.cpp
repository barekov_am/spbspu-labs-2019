#include <iostream>
#include <sstream>
#include "shape.hpp"
#include "functions.hpp"


Shape readPoints(std::string& line, std::size_t vertices)
{
  Shape shape;
  std::size_t openBracketPos;
  std::size_t closeBracketPos;
  std::size_t semicolonPos;

  for (std::size_t i = 0; i < vertices; i++)
  {
    openBracketPos = line.find_first_of('(');
    closeBracketPos = line.find_first_of(')');
    semicolonPos = line.find_first_of(';');
    if ((openBracketPos == std::string::npos) || (closeBracketPos == std::string::npos) || (semicolonPos == std::string::npos))
    {
      throw std::invalid_argument("Incorrect line!");
    }

    Point_t point
    {
      std::stoi(line.substr(openBracketPos + 1, semicolonPos - openBracketPos - 1)),
          std::stoi(line.substr(semicolonPos + 1, closeBracketPos - semicolonPos - 1))
    };
    line.erase(0, closeBracketPos + 1);
    shape.push_back(point);
  }
  return shape;
}

void readWords(std::vector<Shape>& vector, std::string& line)
{
  while (std::getline(std::cin, line))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Failed during reading");
    }

    std::stringstream stream(line);
    std::size_t numOfVertices;
    if (!(stream >> numOfVertices))
    {
      continue;
    }

    if (numOfVertices < 3)
    {
      throw std::invalid_argument("Invalid number of vertices");
    }

    Shape shape = readPoints(line, numOfVertices);

    vector.push_back(shape);
  }
}

void recieveInfo(ShapeInfo& shapeInfo_, std::vector<Shape>& shapeCont)
{
  for (const auto& shape : shapeCont)
  {
    shapeInfo_.countOfVertices += shape.size();
    if (shape.size() == TRIANGLE_VERTICES)
    {
      ++shapeInfo_.countOfTriangles;
    }
    else if (shape.size() == RECTANGLE_VERTICES)
    {
      if (isRectangle(shape))
      {
        ++shapeInfo_.countOfRectangle;
        if (isSquare(shape))
        {
          ++shapeInfo_.countOfSquares;
        }
      }
    }
  }
}

void removePentagons(std::vector<Shape>& shapeCont)
{
  auto newEnd = remove_if(shapeCont.begin(), shapeCont.end(), [](const Shape& shape) {return (shape.size() == PENTAGON_VERTICES); });
  shapeCont.erase(newEnd, shapeCont.end());
}

void createVectorPoints(std::vector<Shape>& shapeCont, std::vector<Point_t> points)
{
  for (auto i = shapeCont.begin(); i != shapeCont.end(); i++)
  {
    points.push_back((*i).front());
  }
}

bool isRectangle(const Shape& shape)
{
  int diag1 = (shape[0].x - shape[2].x) * (shape[0].x - shape[2].x) + (shape[0].y - shape[2].y) * (shape[0].y - shape[2].y);
  int diag2 = (shape[1].x - shape[3].x) * (shape[1].x - shape[3].x) + (shape[1].y - shape[3].y) * (shape[1].y - shape[3].y);
  return diag1 == diag2;
}

bool isSquare(const Shape& shape)
{
  if (shape[1].x != shape[0].x)
  {
    if (std::abs(shape[1].x - shape[0].x) != std::abs(shape[2].y - shape[1].y))
    {
      return false;
    }
  }
  else
  {
    if (std::abs(shape[2].x - shape[1].x) != std::abs(shape[1].y - shape[0].y))
    {
      return false;
    }
  }
  return true;
}

bool isLess(const Shape& lhs, const Shape& rhs)
{
  if (lhs.size() < rhs.size())
  {
    return true;
  }
  if ((lhs.size() == RECTANGLE_VERTICES) && (rhs.size() == RECTANGLE_VERTICES))
  {
    if (isSquare(lhs))
    {
      if (isSquare(rhs))
      {
        return lhs[0].x < rhs[0].x;
      }
      return true;
    }
  }
  return false;
}

void printInfo(std::vector<Shape>& shapeCont, std::vector<Point_t> points, ShapeInfo& shapeInfo_)
{
  std::cout << "Vertices: " << shapeInfo_.countOfVertices << '\n';
  std::cout << "Triangles: " << shapeInfo_.countOfTriangles << '\n';
  std::cout << "Squares: " << shapeInfo_.countOfSquares << '\n';
  std::cout << "Rectangles: " << shapeInfo_.countOfRectangle << '\n';
  std::cout << "Points: ";
  for (auto& i : points)
  {
    std::cout << '(' << i.x << ';' << i.y << ") ";
  }
  std::cout << '\n';

  std::cout << "Shapes: \n";
  for (const auto& shape : shapeCont)
  {
    std::cout << shape.size();
    for (const auto point : shape)
    {
      std::cout << " (" << point.x << ';' << point.y << ") ";
    }
    std::cout << '\n';
  }

}
