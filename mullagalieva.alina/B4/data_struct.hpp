#ifndef DATA_STRUCT
#define DATA_STRUCT

#include <string>
#include <vector>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

DataStruct read(std::istream& input);
void print(const std::vector<DataStruct>& vector);

#endif //DATA_STRUCT
