#ifndef FUNCTORS_HPP
#define FUNCTORS_HPP

#include <iostream>

class Functor
{
public:
  Functor();
  void operator()(int number);
  int getMax() const;
  int getMin() const;
  double getMean() const;
  unsigned int getPositiveCount() const;
  unsigned int getNegativeCount() const;
  long long int getOddSum() const;
  long long int getEvenSum() const;
  bool getFirstEqualLast() const;

  void print() const;

private:
  int max_;
  int min_;
  unsigned int positive_;
  unsigned int negative_;
  long long int oddSum_;
  long long int evenSum_;
  bool firstEqualLast_;
  int first_;
  int last_;
  unsigned int counter_;
};

#endif // FUNCTORS_HPP
