#include "functors.hpp"

Functor::Functor() :
  max_(0),
  min_(0),
  positive_(0),
  negative_(0),
  oddSum_(0),
  evenSum_(0),
  firstEqualLast_(false),
  first_(0),
  last_(0),
  counter_(0)
{}

void Functor::operator ()(const int number)
{
  if (counter_ == 0)
  {
    first_ = number;
    min_ = number;
    max_ = number;
  }

  last_ = number;
  ++counter_;

  firstEqualLast_ = (first_ == last_);

  if (number < min_)
  {
    min_ = number;
  }
  else if (number > max_)
  {
    max_ = number;
  }
  if (number < 0)
  {
    ++negative_;
  }
  else if (number > 0)
  {
    ++positive_;
  }

  (number % 2 ? oddSum_ : evenSum_) += number;
}

int Functor::getMax() const
{
  return max_;
}

int Functor::getMin() const
{
  return min_;
}

double Functor::getMean() const
{
  return (static_cast<double>(oddSum_ + evenSum_) / counter_);
}

unsigned int Functor::getPositiveCount() const
{
  return positive_;
}

unsigned int Functor::getNegativeCount() const
{
  return negative_;
}

long long int Functor::getOddSum() const
{
  return oddSum_;
}

long long int Functor::getEvenSum() const
{
  return evenSum_;
}

bool Functor::getFirstEqualLast() const
{
  return firstEqualLast_;
}

void Functor::print() const
{
  if (counter_ <= 0)
  {
    std::cout << "No Data" << std::endl;
    return;
  }

  std::cout << "Max: " << getMax() << std::endl
    << "Min: " << getMin() << std::endl
    << "Mean: " << std::fixed << getMean() << std::endl
    << "Positive: " << getPositiveCount() << std::endl
    << "Negative: " << getNegativeCount() << std::endl
    << "Odd Sum: " << getOddSum() << std::endl
    << "Even Sum: " << getEvenSum() << std::endl
    << "First/Last Equal: " << (getFirstEqualLast() ? "yes" : "no") << std::endl;
}
