#include <iostream>
#include <algorithm>
#include <iterator>
#include "functors.hpp"

int main()
{
  try
  {
    Functor stat;
    stat = std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), stat);

    if ((std::cin.fail()) && (!std::cin.eof()))
    {
      std::cerr << "Reading failed" << std::endl;
      return 1;
    }

    stat.print();
  }

  catch (std::exception& e)
  {
    e.what();
    return 1;
  }

  return 0;
}
