#ifndef PHONE_BOOK_HPP
#define PHONE_BOOK_HPP

#include <map>
#include <string>

#include "phoneBookInterface.hpp"

class PhoneBookInterface {
public:
  enum class InsertPosition {
    before,
    after
  };
  enum class MovePosition {
    first,
    last
  };

  PhoneBookInterface();

  void add(PhoneBook::record_t& record);
  void store(std::string& bookmark, std::string& newBookmark);
  void insert(InsertPosition& position, std::string& bookmark, PhoneBook::record_t& record);
  void deleteNote(std::string& bookmark);
  void show(std::string& bookmark);
  void move(std::string& bookmark, int steps);
  void move(std::string& bookmark, MovePosition& movePosition);

private:
  std::map<std::string, PhoneBook::iterator> bookmarks_;
  PhoneBook phoneBook_;
};

#endif
