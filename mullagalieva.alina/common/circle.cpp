#define _USE_MATH_DEFINES

#include "circle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

const double FullCircle = 360.0;

mullagalieva::Circle::Circle(const point_t& pos, double radius) :
  centre_(pos),
  radius_(radius)
{
  if (radius_ < 0.0)
  {
    throw std::invalid_argument("Radius must be a non-negative number");
  }
}

mullagalieva::Circle::Circle(double x, double y, double radius) :
  centre_({x, y}),
  radius_(radius)
{
  if (radius_ < 0.0)
  {
    throw std::invalid_argument("Radius must be a non-negative number");
  }
}

double mullagalieva::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

mullagalieva::rectangle_t mullagalieva::Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, centre_};
}

void mullagalieva::Circle::move(const point_t& point)
{
  centre_ = point;
}

void mullagalieva::Circle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

void mullagalieva::Circle::scale(double multiplier)
{
  if (multiplier <= 0.0)
  {
    throw std::invalid_argument("Circle multiplier must be a positive number");
  }
  radius_ *= multiplier;
}

void mullagalieva::Circle::rotate(double)
{}

void mullagalieva::Circle::show() const
{
  std::cout << "Circle" << "\n";
  std::cout << "Position of the figure: ("
      << centre_.x << "; " << centre_.y << ")" << "\n";
  std::cout << "Radius = " << radius_ << "\n";
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "Frame Rectangle centre: ("
      << getFrameRect().pos.x << "; " << getFrameRect().pos.y << ")" << "\n";
  std::cout << "Frame Rectangle width: " << getFrameRect().width << "\n";
  std::cout << "Frame Rectangle height: " << getFrameRect().height << "\n";
}
