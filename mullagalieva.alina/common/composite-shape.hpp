#ifndef COMPOSITE_SHAPE
#define COMPOSITE_SHAPE

#include <memory>

#include "shape.hpp"

namespace mullagalieva
{
  class CompositeShape : public Shape
  {
  public:
    using shape_ptr = std::shared_ptr<Shape>;
    using shape_array = std::unique_ptr<shape_ptr[]>;

    CompositeShape();
    CompositeShape(const CompositeShape& source);
    CompositeShape(CompositeShape&& source);

    ~CompositeShape() = default;

    CompositeShape& operator=(const CompositeShape& rhs);
    CompositeShape& operator=(CompositeShape&& rhs);

    shape_ptr operator [](size_t index) const;
    bool operator ==(const CompositeShape &) const;
    bool operator !=(const CompositeShape &) const;
    
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& point) override;
    void move(double dx, double dy) override;
    void scale(double multiplier) override;
    void rotate(double angle) override;
    void show() const override;
    void addShape(shape_ptr shape);
    void deleteShape(size_t index);
    shape_array list() const;
    size_t size() const;

  private:
    size_t count_;
    double angle_;
    shape_array arrayOfShapes_;
  };
}

#endif
