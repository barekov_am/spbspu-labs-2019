#define _USE_MATH_DEFINES

#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

const double FullCircle = 360.0;

mullagalieva::Rectangle::Rectangle(const point_t& pos, double width, double height) :
  centre_(pos),
  width_(width),
  height_(height),
  angle_(0.0)
{
  if ((width_ <= 0.0) || (height_ <= 0.0))
  {
    throw std::invalid_argument("Width and height must be positive numbers");
  }
}

mullagalieva::Rectangle::Rectangle(double x, double y, double width, double height) :
  centre_({x, y}),
  width_(width),
  height_(height),
  angle_(0.0)
{
  if ((width_ <= 0.0) || (height_ <= 0.0))
  {
    throw std::invalid_argument("Width and height must be positive numbers");
  }
}

double mullagalieva::Rectangle::getArea() const
{
  return width_ * height_;
}

mullagalieva::rectangle_t mullagalieva::Rectangle::getFrameRect() const
{
  const double sine = sin(angle_ * M_PI / 180);
  const double cosine = cos(angle_ * M_PI / 180);
  const double width = height_ * std::fabs(sine) + width_ * std::fabs(cosine);
  const double height = height_ * std::fabs(cosine) + width_ * std::fabs(sine);
  return {width, height, centre_};
}

void mullagalieva::Rectangle::move(const point_t& point)
{
  centre_ = point;
}

void mullagalieva::Rectangle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

void mullagalieva::Rectangle::scale(double multiplier)
{
  if (multiplier <= 0.0)
  {
    throw std::invalid_argument("Rectangle multiplier must be a positive number");
  }
  width_ *= multiplier;
  height_ *= multiplier;
}

void mullagalieva::Rectangle::rotate(double angle)
{
  angle_ += angle;
  angle_ = (angle_ < 0.0) ? (FullCircle + fmod(angle_, FullCircle))
      : fmod(angle_, FullCircle);
}

void mullagalieva::Rectangle::show() const
{
  std::cout << "Rectangle" << "\n";
  std::cout << "Position of the figure: ("
      << centre_.x << "; " << centre_.y << ")" << "\n";
  std::cout << "Width = " << width_ << "\n";
  std::cout << "Height = " << height_ << "\n";
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "Frame Rectangle centre: ("
      << getFrameRect().pos.x << "; " << getFrameRect().pos.y << ")" << "\n";
  std::cout << "Frame Rectangle width: " << getFrameRect().width << "\n";
  std::cout << "Frame Rectangle height: " << getFrameRect().height << "\n";
}
