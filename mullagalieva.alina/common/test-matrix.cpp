#include <boost/test/auto_unit_test.hpp>
#include <utility>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testForMatrix)

using shape_ptr = std::shared_ptr<mullagalieva::Shape>;
using mullagalieva::point_t;
using mullagalieva::Circle;
using mullagalieva::Rectangle;
using mullagalieva::Triangle;

BOOST_AUTO_TEST_CASE(copyAndMove)
{
  shape_ptr testCircle = std::make_shared<Circle>(point_t{5.2, 3.4}, 3.2);
  mullagalieva::CompositeShape testCompositeShape;
  testCompositeShape.addShape(testCircle);

  mullagalieva::Matrix testMatrix = mullagalieva::part(testCompositeShape);

  BOOST_CHECK_NO_THROW(mullagalieva::Matrix testMatrix2(testMatrix));
  BOOST_CHECK_NO_THROW(mullagalieva::Matrix testMatrix2(std::move(testMatrix)));

  mullagalieva::Matrix testMatrix2 = mullagalieva::part(testCompositeShape);
  mullagalieva::Matrix testMatrix3;

  BOOST_CHECK_NO_THROW(testMatrix3 = testMatrix2);
  BOOST_CHECK_NO_THROW(testMatrix3 = std::move(testMatrix2));

  mullagalieva::Matrix testMatrix4 = mullagalieva::part(testCompositeShape);
  mullagalieva::Matrix testMatrix5;

  testMatrix5 = testMatrix4;
  BOOST_CHECK(testMatrix5 == testMatrix4);

  testMatrix5 = std::move(testMatrix4);
  BOOST_CHECK(testMatrix5 == testMatrix3);

  mullagalieva::Matrix testMatrix6(testMatrix3);
  BOOST_CHECK(testMatrix6 == testMatrix3);

  mullagalieva::Matrix testMatrix7(std::move(testMatrix3));
  BOOST_CHECK(testMatrix7 == testMatrix6);
}

BOOST_AUTO_TEST_CASE(exceptionThrow)
{
  mullagalieva::CompositeShape testCompositeShape;
  shape_ptr testCircle = std::make_shared<Circle>(point_t{5.2, 3.4}, 3.2);
  shape_ptr testRectangle = std::make_shared<Rectangle>(3.12, 6.5, 8.7, 9.6);
  shape_ptr testTriangle = std::make_shared<Triangle>(point_t{4.5, -1.2}, point_t{1.3, 3.5}, point_t{5.6, 3.2});

  testCompositeShape.addShape(testCircle);
  testCompositeShape.addShape(testRectangle);
  testCompositeShape.addShape(testTriangle);

  mullagalieva::Matrix testMatrix = mullagalieva::part(testCompositeShape);

  BOOST_CHECK_THROW(testMatrix[3], std::out_of_range);
  BOOST_CHECK_THROW(testMatrix[-1], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
