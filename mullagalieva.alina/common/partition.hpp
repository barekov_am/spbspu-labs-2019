#ifndef PARTITION
#define PARTITION

#include "composite-shape.hpp"
#include "matrix.hpp"

using shape_ptr = std::shared_ptr<mullagalieva::Shape>;
using shape_array = std::unique_ptr<shape_ptr[]>;

namespace mullagalieva
{
  Matrix part(const shape_array & array, size_t size);
  Matrix part(const CompositeShape & composite);
}

#endif
