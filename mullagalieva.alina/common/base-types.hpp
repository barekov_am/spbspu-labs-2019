#ifndef BASE_TYPES
#define BASE_TYPES

namespace mullagalieva
{
  struct point_t
  {
    double x;
    double y;
  };

  struct rectangle_t
  {
    double width;
    double height;
    point_t pos;
  };

  bool intersect(const rectangle_t& lhs, const rectangle_t& rhs);
}

#endif
