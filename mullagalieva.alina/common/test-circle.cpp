#include <boost/test/auto_unit_test.hpp>

#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(testForCircle)

const double MyError = 0.000001;

BOOST_AUTO_TEST_CASE(immutabilityAfterMoving)
{
  mullagalieva::Circle testCircle({9.23, 7.45}, 5.3);
  const double width = testCircle.getFrameRect().width;
  const double height = testCircle.getFrameRect().height;
  const double area = testCircle.getArea();

  testCircle.move({3.4, 4.3});
  BOOST_CHECK_CLOSE_FRACTION(width, testCircle.getFrameRect().width, MyError);
  BOOST_CHECK_CLOSE_FRACTION(height, testCircle.getFrameRect().height, MyError);
  BOOST_CHECK_CLOSE_FRACTION(area, testCircle.getArea(), MyError);

  testCircle.move(6, -2.1);
  BOOST_CHECK_CLOSE_FRACTION(width, testCircle.getFrameRect().width, MyError);
  BOOST_CHECK_CLOSE_FRACTION(height, testCircle.getFrameRect().height, MyError);
  BOOST_CHECK_CLOSE_FRACTION(area, testCircle.getArea(), MyError);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterScaling)
{
  mullagalieva::Circle testCircle({9.23, 7.45}, 5.3);
  double area = testCircle.getArea();
  double multiplier = 1.42;

  testCircle.scale(multiplier);
  BOOST_CHECK_CLOSE_FRACTION(area * multiplier * multiplier, testCircle.getArea(), MyError);

  area = testCircle.getArea();
  multiplier = 0.45;

  testCircle.scale(multiplier);
  BOOST_CHECK_CLOSE_FRACTION(area * multiplier * multiplier, testCircle.getArea(), MyError);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterRotation)
{
  mullagalieva::Circle testCircle({4.4, 2.3}, 3.4);
  const double area = testCircle.getArea();
  const double angle = 44.4;

  testCircle.rotate(angle);
  BOOST_CHECK_CLOSE_FRACTION(testCircle.getArea(), area, MyError);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  BOOST_CHECK_THROW(mullagalieva::Circle(2.1, -3.3, -5.1), std::invalid_argument);
  mullagalieva::Circle testCircle({-4.34, 5.6}, 3.2);
  BOOST_CHECK_THROW(testCircle.scale(-4.3), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
