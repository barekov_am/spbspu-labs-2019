#include <boost/test/auto_unit_test.hpp>

#include "triangle.hpp"

BOOST_AUTO_TEST_SUITE(testForTriangle)

const double MyError = 0.000001;

BOOST_AUTO_TEST_CASE(immutabilityAfterMoving)
{
  mullagalieva::Triangle testTriangle({4.5, -1.2}, {1.3, 3.5}, {5.6, 3.2});
  const double width = testTriangle.getFrameRect().width;
  const double height = testTriangle.getFrameRect().height;
  const double area = testTriangle.getArea();

  testTriangle.move({3.4, 4.3});
  BOOST_CHECK_CLOSE_FRACTION(width, testTriangle.getFrameRect().width, MyError);
  BOOST_CHECK_CLOSE_FRACTION(height, testTriangle.getFrameRect().height, MyError);
  BOOST_CHECK_CLOSE_FRACTION(area, testTriangle.getArea(), MyError);

  testTriangle.move(6, -2.1);
  BOOST_CHECK_CLOSE_FRACTION(width, testTriangle.getFrameRect().width, MyError);
  BOOST_CHECK_CLOSE_FRACTION(height, testTriangle.getFrameRect().height, MyError);
  BOOST_CHECK_CLOSE_FRACTION(area, testTriangle.getArea(), MyError);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterScaling)
{
  mullagalieva::Triangle testTriangle({4.5, -1.2}, {1.3, 3.5}, {5.6, 3.2});
  double area = testTriangle.getArea();
  double multiplier = 6.7;

  testTriangle.scale(multiplier);
  BOOST_CHECK_CLOSE_FRACTION(area * multiplier * multiplier, testTriangle.getArea(), MyError);

  area = testTriangle.getArea();
  multiplier = 0.23;

  testTriangle.scale(multiplier);
  BOOST_CHECK_CLOSE_FRACTION(area * multiplier * multiplier, testTriangle.getArea(), MyError);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterRotation)
{
  mullagalieva::Triangle testTriangle({4.5, -1.2}, {1.3, 3.5}, {5.6, 3.2});
  const double area = testTriangle.getArea();
  const double angle = 36.5;

  testTriangle.rotate(angle);
  BOOST_CHECK_CLOSE_FRACTION(testTriangle.getArea(), area, MyError);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  BOOST_CHECK_THROW(mullagalieva::Triangle({0, -1.2}, {0, 3.5}, {0, 3.2}), std::invalid_argument);
  mullagalieva::Triangle testTriangle({4.5, -1.2}, {1.3, 3.5}, {5.6, 3.2});
  BOOST_CHECK_THROW(testTriangle.scale(-4.5), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
