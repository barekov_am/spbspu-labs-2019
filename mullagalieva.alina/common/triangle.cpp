#define _USE_MATH_DEFINES

#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

const double MyError = 0.000001;
const double FullCircle = 360.0;

mullagalieva::Triangle::Triangle(const point_t& posA, const point_t& posB, const point_t& posC) :
  centre_({(posA.x + posB.x + posC.x) / 3, (posA.y + posB.y + posC.y) / 3}),
  posA_(posA),
  posB_(posB),
  posC_(posC),
  angle_(0.0)
{
  if (std::abs((posB.x - posA.x) * (posC.y - posA.y)
      - (posB.y - posA.y) * (posC.x - posA.x)) < MyError)
  {
    throw std::invalid_argument("The points can't be on the same line");
  }
}

double mullagalieva::Triangle::getArea() const
{
  return std::abs((posC_.x - posA_.x) * (posC_.y - posB_.y)
      - (posC_.y - posA_.y) * (posC_.x - posB_.x)) / 2;
}

mullagalieva::rectangle_t mullagalieva::Triangle::getFrameRect() const
{
  const double max_x = std::fmax(std::fmax(posA_.x, posB_.x), posC_.x);
  const double min_x = std::fmin(std::fmin(posA_.x, posB_.x), posC_.x);
  const double max_y = std::fmax(std::fmax(posA_.y, posB_.y), posC_.y);
  const double min_y = std::fmin(std::fmin(posA_.y, posB_.y), posC_.y);
  return {max_x - min_x, max_y - min_y, {(max_x + min_x) / 2, (max_y + min_y) / 2}};
}

void mullagalieva::Triangle::move(const point_t& point)
{
  const double dx = point.x - centre_.x;
  const double dy = point.y - centre_.y;
  move(dx, dy);
}

void mullagalieva::Triangle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
  posA_.x += dx;
  posB_.x += dx;
  posC_.x += dx;
  posA_.y += dy;
  posB_.y += dy;
  posC_.y += dy;
}

void mullagalieva::Triangle::scale(double multiplier)
{
  if (multiplier <= 0.0)
  {
    throw std::invalid_argument("Triangle multiplier must be a positive number");
  }
  posA_.x = centre_.x + multiplier * (posA_.x - centre_.x);
  posA_.y = centre_.y + multiplier * (posA_.y - centre_.y);
  posB_.x = centre_.x + multiplier * (posB_.x - centre_.x);
  posB_.y = centre_.y + multiplier * (posB_.y - centre_.y);
  posC_.x = centre_.x + multiplier * (posC_.x - centre_.x);
  posC_.y = centre_.y + multiplier * (posC_.y - centre_.y);
}

void mullagalieva::Triangle::rotate(double angle)
{
  angle_ += angle;
  angle_ = (angle_ < 0.0) ? (FullCircle + std::fmod(angle_, FullCircle))
    : std::fmod(angle_, FullCircle);

  rotation(posA_);
  rotation(posB_);
  rotation(posC_);
}

void mullagalieva::Triangle::rotation(point_t& vertex)
{
  const double sine = sin(angle_ * M_PI / 180);
  const double cosine = cos(angle_ * M_PI / 180);

  const double distanceX = vertex.x - centre_.x;
  const double distanceY = vertex.y - centre_.y;

  vertex.x = distanceX * cosine - distanceY * sine;
  vertex.y = distanceX * sine + distanceY * cosine;

  vertex.x += centre_.x;
  vertex.y += centre_.y;
}

void mullagalieva::Triangle::show() const
{
  std::cout << "Triangle" << "\n";
  std::cout << "Position of the figure: ("
      << centre_.x << "; " << centre_.y << ")" << "\n";
  std::cout << "Side1 = " << sqrt((posB_.x - posA_.x) * (posB_.x - posA_.x)
      + (posB_.y - posA_.y) * (posB_.y - posA_.y)) << "\n";
  std::cout << "Side2 = " << sqrt((posC_.x - posB_.x) * (posC_.x - posB_.x)
      + (posC_.y - posB_.y) * (posC_.y - posB_.y)) << "\n";
  std::cout << "Side1 = " << sqrt((posA_.x - posC_.x) * (posA_.x - posC_.x)
      + (posA_.y - posC_.y) * (posA_.y - posC_.y)) << "\n";
  std::cout << "Coordinates:" << "\n";
  std::cout << "A: (" << posA_.x << "; " << posA_.y << ")" << "\n";
  std::cout << "B: (" << posB_.x << "; " << posB_.y << ")" << "\n";
  std::cout << "C: (" << posC_.x << "; " << posC_.y << ")" << "\n";
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "Frame Rectangle centre: ("
      << getFrameRect().pos.x << "; " << getFrameRect().pos.y << ")" << "\n";
  std::cout << "Frame Rectangle width: " << getFrameRect().width << "\n";
  std::cout << "Frame Rectangle height: " << getFrameRect().height << "\n";
}
