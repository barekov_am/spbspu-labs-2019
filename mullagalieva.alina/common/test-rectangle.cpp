#include <boost/test/auto_unit_test.hpp>

#include "rectangle.hpp"

BOOST_AUTO_TEST_SUITE(testForRectangle)

const double MyError = 0.000001;

BOOST_AUTO_TEST_CASE(immutabilityAfterMoving)
{
  mullagalieva::Rectangle testRectangle(8.7, 15.3, 6.8, 1.2);
  const double width = testRectangle.getFrameRect().width;
  const double height = testRectangle.getFrameRect().height;
  const double area = testRectangle.getArea();

  testRectangle.move({3.4, 4.3});
  BOOST_CHECK_CLOSE_FRACTION(width, testRectangle.getFrameRect().width, MyError);
  BOOST_CHECK_CLOSE_FRACTION(height, testRectangle.getFrameRect().height, MyError);
  BOOST_CHECK_CLOSE_FRACTION(area, testRectangle.getArea(), MyError);

  testRectangle.move(6, -2.1);
  BOOST_CHECK_CLOSE_FRACTION(width, testRectangle.getFrameRect().width, MyError);
  BOOST_CHECK_CLOSE_FRACTION(height, testRectangle.getFrameRect().height, MyError);
  BOOST_CHECK_CLOSE_FRACTION(area, testRectangle.getArea(), MyError);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterScaling)
{
  mullagalieva::Rectangle testRectangle(8.7, 15.3, 6.8, 1.2);
  double area = testRectangle.getArea();
  double multiplier = 5.3;

  testRectangle.scale(multiplier);
  BOOST_CHECK_CLOSE_FRACTION(area * multiplier * multiplier, testRectangle.getArea(), MyError);

  area = testRectangle.getArea();
  multiplier = 0.53;

  testRectangle.scale(multiplier);
  BOOST_CHECK_CLOSE_FRACTION(area * multiplier * multiplier, testRectangle.getArea(), MyError);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterRotation)
{
  mullagalieva::Rectangle testRectangle(8.7, 15.3, 6.8, 1.2);
  const double area = testRectangle.getArea();
  const double angle = 54.3;

  testRectangle.rotate(angle);
  BOOST_CHECK_CLOSE_FRACTION(testRectangle.getArea(), area, MyError);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  BOOST_CHECK_THROW(mullagalieva::Rectangle(-5.1, 4.2, -8.1, 6.7), std::invalid_argument);
  mullagalieva::Rectangle testRectangle(-5.1, 4.2, 8.1, 6.7);
  BOOST_CHECK_THROW(testRectangle.scale(-3.4), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
