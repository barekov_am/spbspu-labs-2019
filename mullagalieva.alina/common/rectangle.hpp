#ifndef RECTANGLE
#define RECTANGLE

#include "shape.hpp"

namespace mullagalieva
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t& pos, double width, double height);
    Rectangle(double x, double y, double width, double height);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& point) override;
    void move(double dx, double dy) override;
    void scale(double multiplier) override;
    void rotate(double angle) override;
    void show() const override;

  private:
    point_t centre_;
    double width_;
    double height_;
    double angle_;
  };
}

#endif
