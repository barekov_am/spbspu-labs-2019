#define _USE_MATH_DEFINES

#include "composite-shape.hpp"

#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <utility>
#include <cmath>

const double FullCircle = 360.0;

mullagalieva::CompositeShape::CompositeShape() :
  count_(0),
  angle_(0.0),
  arrayOfShapes_()
{}

mullagalieva::CompositeShape::CompositeShape(const CompositeShape& source) :
  count_(source.count_),
  angle_(source.angle_),
  arrayOfShapes_(std::make_unique<shape_ptr[]>(source.count_))
{
  for (size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i] = source.arrayOfShapes_[i];
  }
}

mullagalieva::CompositeShape::CompositeShape(CompositeShape&& source) :
  count_(source.count_),
  angle_(source.angle_),
  arrayOfShapes_(std::move(source.arrayOfShapes_))
{
  source.count_ = 0;
  source.angle_ = 0.0;
  source.arrayOfShapes_.reset();
}

mullagalieva::CompositeShape& mullagalieva::CompositeShape::operator=(const CompositeShape& rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    angle_ = rhs.angle_;
    shape_array tmpArray(std::make_unique<shape_ptr[]>(rhs.count_));
    for (size_t i = 0; i < count_; i++)
    {
      tmpArray[i] = rhs.arrayOfShapes_[i];
    }
    arrayOfShapes_.swap(tmpArray);
  }
  return *this;
}

mullagalieva::CompositeShape& mullagalieva::CompositeShape::operator=(CompositeShape&& rhs)
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    angle_ = rhs.angle_;
    arrayOfShapes_ = std::move(rhs.arrayOfShapes_);
  }
  return *this;
}

mullagalieva::CompositeShape::shape_ptr mullagalieva::CompositeShape::operator [](size_t index) const
{
  if (index >= count_)
  {
    throw std::out_of_range("There is no figure!");
  }
  return arrayOfShapes_[index];
}

bool mullagalieva::CompositeShape::operator ==(const CompositeShape & rhs) const
{
  if (count_ != rhs.count_)
  {
    return false;
  }

  for (size_t i = 0; i < count_; i++)
  {
    if (arrayOfShapes_[i] != rhs.arrayOfShapes_[i])
    {
      return false;
    }
  }
  return true;
}

bool mullagalieva::CompositeShape::operator !=(const CompositeShape & rhs) const
{
  return !(*this == rhs);
}


double mullagalieva::CompositeShape::getArea() const
{
  double totalArea = 0;
  for (size_t i = 0; i < count_; i++)
  {
    totalArea += arrayOfShapes_[i]->getArea();
  }
  return totalArea;
}

mullagalieva::rectangle_t mullagalieva::CompositeShape::getFrameRect() const
{
  if (count_ == 0)
  {
    return {0, 0, {0, 0}};
  }

  rectangle_t tmpFrameRect = arrayOfShapes_[0]->getFrameRect();

  double lftX = tmpFrameRect.pos.x - tmpFrameRect.width / 2;
  double rgtX = tmpFrameRect.pos.x + tmpFrameRect.width / 2;
  double btmY = tmpFrameRect.pos.y - tmpFrameRect.height / 2;
  double topY = tmpFrameRect.pos.y + tmpFrameRect.height / 2;

  for (size_t i = 1; i < count_; i++)
  {
    tmpFrameRect = arrayOfShapes_[i]->getFrameRect();

    double tmpValue = tmpFrameRect.pos.x - tmpFrameRect.width / 2;
    lftX = std::min(tmpValue, lftX);

    tmpValue = tmpFrameRect.pos.x + tmpFrameRect.width / 2;
    rgtX = std::max(tmpValue, rgtX);

    tmpValue = tmpFrameRect.pos.y - tmpFrameRect.height / 2;
    btmY = std::min(tmpValue, btmY);

    tmpValue = tmpFrameRect.pos.y + tmpFrameRect.height / 2;
    topY = std::max(tmpValue, topY);
  }

  return {rgtX - lftX, topY - btmY, {(lftX + rgtX) / 2, (btmY + topY) / 2}};
}

void mullagalieva::CompositeShape::move(const point_t& point)
{
  const double deltaX = point.x - getFrameRect().pos.x;
  const double deltaY = point.y - getFrameRect().pos.y;
  for (size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i]->move(deltaX, deltaY);
  }
}

void mullagalieva::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < count_; i++)
  {
    arrayOfShapes_[i]->move(dx, dy);
  }
}

void mullagalieva::CompositeShape::scale(double multiplier)
{
  if (multiplier <= 0.0)
  {
    throw std::invalid_argument("Composite shape multiplier must be a positive number");
  }

  const point_t centre = getFrameRect().pos;
  for (size_t i = 0; i < count_; i++)
  {
    const double deltaX = arrayOfShapes_[i]->getFrameRect().pos.x - centre.x;
    const double deltaY = arrayOfShapes_[i]->getFrameRect().pos.y - centre.y;
    arrayOfShapes_[i]->move({centre.x + deltaX * multiplier, centre.y + deltaY * multiplier});
    arrayOfShapes_[i]->scale(multiplier);
  }
}

void mullagalieva::CompositeShape::rotate(double angle)
{
  angle_ += angle;
  angle_ = (angle_ < 0.0) ? (FullCircle + fmod(angle_, FullCircle))
      : fmod(angle_, FullCircle);

  const point_t centre = getFrameRect().pos;
  const double sine = sin(angle_ * M_PI / 180);
  const double cosine = cos(angle_ * M_PI / 180);

  for (size_t i = 0; i < count_; i++)
  {
    const double oldX = arrayOfShapes_[i]->getFrameRect().pos.x - centre.x;
    const double oldY = arrayOfShapes_[i]->getFrameRect().pos.y - centre.y;

    const double newX = oldX * cosine - oldY * sine;
    const double newY = oldX * sine + oldY * cosine;

    arrayOfShapes_[i]->move({centre.x + newX, centre.y + newY});
    arrayOfShapes_[i]->rotate(angle_);
  }
}

void mullagalieva::CompositeShape::show() const
{
  std::cout << "Composite Shape" << "\n";
  std::cout << "Position of the figure: ("
    << getFrameRect().pos.x << "; " << getFrameRect().pos.y << ")" << "\n";
  std::cout << "Count: " << count_ << "\n";
  std::cout << "Area: " << getArea() << "\n";
  std::cout << "Frame Rectangle centre: ("
    << getFrameRect().pos.x << "; " << getFrameRect().pos.y << ")" << "\n";
  std::cout << "Frame Rectangle width: " << getFrameRect().width << "\n";
  std::cout << "Frame Rectangle height: " << getFrameRect().height << "\n";
}

void mullagalieva::CompositeShape::addShape(shape_ptr shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Added shape pointer must not be null");
  }

  shape_array tmpArray(std::make_unique<shape_ptr[]>(count_ + 1));
  for (size_t i = 0; i < count_; i++)
  {
    tmpArray[i] = arrayOfShapes_[i];
  }
  tmpArray[count_] = shape;
  count_++;
  arrayOfShapes_.swap(tmpArray);
}

void mullagalieva::CompositeShape::deleteShape(size_t index)
{
  if (index >= count_)
  {
    throw std::out_of_range("Index is out of range");
  }
  if (count_ == 0)
  {
    throw std::out_of_range("Cannot delete a figure from an empty composite shape");
  }

  for (size_t i = index; i < count_ - 1; i++)
  {
    arrayOfShapes_[i] = arrayOfShapes_[i + 1];
  }
  count_--;
}

mullagalieva::CompositeShape::shape_array mullagalieva::CompositeShape::list() const
{
  shape_array tmpArray(std::make_unique<shape_ptr[]>(count_));
  for (size_t i = 0; i < count_; i++)
  {
    tmpArray[i] = arrayOfShapes_[i];
  }

  return tmpArray;
}

size_t mullagalieva::CompositeShape::size() const
{
  return count_;
}
