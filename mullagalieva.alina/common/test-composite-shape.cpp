#include <boost/test/auto_unit_test.hpp>
#include <utility>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(testForCompositeShape)

using shape_ptr = std::shared_ptr<mullagalieva::Shape>;
using mullagalieva::point_t;
using mullagalieva::Circle;
using mullagalieva::Rectangle;
using mullagalieva::Triangle;

const double MyError = 0.000001;

BOOST_AUTO_TEST_CASE(immutabilityAfterMoving)
{
  mullagalieva::CompositeShape testCompositeShape;
  shape_ptr testCircle = std::make_shared<Circle>(point_t{5.2, 3.4}, 3.2);
  shape_ptr testRectangle = std::make_shared<Rectangle>(3.12, 6.5, 8.7, 9.6);
  shape_ptr testTriangle = std::make_shared<Triangle>(point_t{4.5, -1.2}, point_t{1.3, 3.5}, point_t{5.6, 3.2});

  testCompositeShape.addShape(testCircle);
  testCompositeShape.addShape(testRectangle);
  testCompositeShape.addShape(testTriangle);

  const double width = testCompositeShape.getFrameRect().width;
  const double height = testCompositeShape.getFrameRect().height;
  const double area = testCompositeShape.getArea();

  testCompositeShape.move({3.4, 4.3});
  BOOST_CHECK_CLOSE_FRACTION(width, testCompositeShape.getFrameRect().width, MyError);
  BOOST_CHECK_CLOSE_FRACTION(height, testCompositeShape.getFrameRect().height, MyError);
  BOOST_CHECK_CLOSE_FRACTION(area, testCompositeShape.getArea(), MyError);

  testCompositeShape.move(6, -2.1);
  BOOST_CHECK_CLOSE_FRACTION(width, testCompositeShape.getFrameRect().width, MyError);
  BOOST_CHECK_CLOSE_FRACTION(height, testCompositeShape.getFrameRect().height, MyError);
  BOOST_CHECK_CLOSE_FRACTION(area, testCompositeShape.getArea(), MyError);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterScaling)
{
  mullagalieva::CompositeShape testCompositeShape;
  shape_ptr testCircle = std::make_shared<Circle>(point_t{5.2, 3.4}, 3.2);
  shape_ptr testRectangle = std::make_shared<Rectangle>(3.12, 6.5, 8.7, 9.6);
  shape_ptr testTriangle = std::make_shared<Triangle>(point_t{4.5, -1.2}, point_t{1.3, 3.5}, point_t{5.6, 3.2});

  testCompositeShape.addShape(testCircle);
  testCompositeShape.addShape(testRectangle);
  testCompositeShape.addShape(testTriangle);

  double area = testCompositeShape.getArea();
  double multiplier = 4.1;

  testCompositeShape.scale(multiplier);
  BOOST_CHECK_CLOSE_FRACTION(area * multiplier * multiplier, testCompositeShape.getArea(), MyError);

  area = testCompositeShape.getArea();
  multiplier = 0.23;

  testCompositeShape.scale(multiplier);
  BOOST_CHECK_CLOSE_FRACTION(area * multiplier * multiplier, testCompositeShape.getArea(), MyError);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterRotation)
{
  mullagalieva::CompositeShape testCompositeShape;
  shape_ptr testCircle = std::make_shared<Circle>(point_t{5.2, 3.4}, 3.2);
  shape_ptr testRectangle = std::make_shared<Rectangle>(3.12, 6.5, 8.7, 9.6);
  shape_ptr testTriangle = std::make_shared<Triangle>(point_t{4.5, -1.2}, point_t{1.3, 3.5}, point_t{5.6, 3.2});


  double area = testCompositeShape.getArea();
  double angle = 76.3;

  testCompositeShape.rotate(angle);
  BOOST_CHECK_CLOSE_FRACTION(testCompositeShape.getArea(), area, MyError);

  area = testCompositeShape.getArea();
  angle = -45.6;

  testCompositeShape.rotate(angle);
  BOOST_CHECK_CLOSE_FRACTION(testCompositeShape.getArea(), area, MyError);
}

BOOST_AUTO_TEST_CASE(copyAndMove)
{
  shape_ptr testCircle = std::make_shared<Circle>(5.2, 3.4, 3.2);
  mullagalieva::CompositeShape testComposite;
  testComposite.addShape(testCircle);

  BOOST_CHECK_NO_THROW(mullagalieva::CompositeShape testComposite2(testComposite));
  BOOST_CHECK_NO_THROW(mullagalieva::CompositeShape testComposite2(std::move(testComposite)));

  mullagalieva::CompositeShape testComposite2;
  testComposite2.addShape(testCircle);

  mullagalieva::CompositeShape testComposite3;

  BOOST_CHECK_NO_THROW(testComposite3 = testComposite2);
  BOOST_CHECK_NO_THROW(testComposite3 = std::move(testComposite2));

  mullagalieva::CompositeShape testComposite4;
  testComposite4.addShape(testCircle);

  mullagalieva::CompositeShape testComposite5;

  testComposite5 = testComposite4;
  BOOST_CHECK(testComposite5 == testComposite4);
  testComposite5 = std::move(testComposite4);
  BOOST_CHECK(testComposite5 == testComposite3);

  mullagalieva::CompositeShape testComposite6(testComposite3);
  BOOST_CHECK(testComposite6 == testComposite3);
  mullagalieva::CompositeShape testComposite7(std::move(testComposite3));
  BOOST_CHECK(testComposite7 == testComposite6);
}

BOOST_AUTO_TEST_CASE(invalidArguments)
{
  mullagalieva::CompositeShape testCompositeShape;
  shape_ptr testCircle = std::make_shared<Circle>(point_t{5.2, 3.4}, 3.2);
  shape_ptr testRectangle = std::make_shared<Rectangle>(3.12, 6.5, 8.7, 9.6);
  shape_ptr testTriangle = std::make_shared<Triangle>(point_t{4.5, -1.2}, point_t{1.3, 3.5}, point_t{5.6, 3.2});

  testCompositeShape.addShape(testCircle);
  testCompositeShape.addShape(testRectangle);
  testCompositeShape.addShape(testTriangle);

  BOOST_CHECK_THROW(testCompositeShape.scale(-2), std::invalid_argument);

  BOOST_CHECK_THROW(testCompositeShape.addShape(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(testCompositeShape.deleteShape(5), std::out_of_range);
  BOOST_CHECK_THROW(testCompositeShape.deleteShape(-2), std::out_of_range);

  testCompositeShape.deleteShape(2);
  testCompositeShape.deleteShape(1);
  testCompositeShape.deleteShape(0);
  BOOST_CHECK_THROW(testCompositeShape.deleteShape(0), std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
