#include <boost/test/auto_unit_test.hpp>
#include <utility>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "partition.hpp"

BOOST_AUTO_TEST_SUITE(testForPartition)

using shape_ptr = std::shared_ptr<mullagalieva::Shape>;
using mullagalieva::Circle;
using mullagalieva::Rectangle;
using mullagalieva::Triangle;

BOOST_AUTO_TEST_CASE(correctPartition)
{
  mullagalieva::CompositeShape testCompositeShape;
  shape_ptr testCircle = std::make_shared<Circle>(-3, 2.5, 5);
  shape_ptr testRectangle1 = std::make_shared<Rectangle>(2, -4.5, 2, 6);
  shape_ptr testRectangle2 = std::make_shared<Rectangle>(0.5, -5.5, 11, 2);
  shape_ptr partOfComposite1 = std::make_shared<Rectangle>(2.5, -3, 5, 1);
  shape_ptr partOfComposite2 = std::make_shared<Rectangle>(-4, 0, 2, 17);

  testCompositeShape.addShape(partOfComposite1);
  testCompositeShape.addShape(partOfComposite2);
  shape_ptr testComposite = std::make_shared<mullagalieva::CompositeShape>(testCompositeShape);

  mullagalieva::CompositeShape testComposite1;
  testComposite1.addShape(testCircle);
  testComposite1.addShape(testRectangle1);
  testComposite1.addShape(testRectangle2);
  testComposite1.addShape(testComposite);

  mullagalieva::Matrix testMatrix = mullagalieva::part(testComposite1);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 3);
  BOOST_CHECK_EQUAL(testMatrix.getColumns(), 2);

  BOOST_CHECK(testMatrix[0][0] == testCircle);
  BOOST_CHECK(testMatrix[0][1] == testRectangle2);
  BOOST_CHECK(testMatrix[1][0] == testRectangle1);
  BOOST_CHECK(testMatrix[2][0] == testComposite);
}

BOOST_AUTO_TEST_SUITE_END()
