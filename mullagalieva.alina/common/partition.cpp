#include "partition.hpp"

mullagalieva::Matrix mullagalieva::part(const shape_array & array, size_t size)
{
  Matrix matrix;
  for (size_t i = 0; i < size; i++)
  {
    size_t rightRow = 0;
    size_t rightColumn = 0;
    size_t necessaryColumn = 0;
    for (size_t j = 0; j < matrix.getRows(); j++)
    {
      necessaryColumn = matrix.getSizeLayer(j);
      for (size_t k = 0; k < matrix.getColumns(); k++)
      {
        if (intersect(array[i]->getFrameRect(), matrix[j][k]->getFrameRect()))
        {
          rightRow++;
          break;
        }
        if (k == necessaryColumn - 1)
        {
          rightRow = j;
          rightColumn = necessaryColumn;
        }
      }
      if (rightRow == j)
      {
        break;
      }
    }
    matrix.add(array[i], rightRow, rightColumn);
  }
  return matrix;
}

mullagalieva::Matrix mullagalieva::part(const CompositeShape & composite)
{
  return part(composite.list(), composite.size());
}
