#include "triangle.hpp"
#include <iostream>

Triangle::Triangle(const Point center) :
  Shape(center)
{}

void Triangle::draw() const
{
  std::cout << "TRIANGLE (" << center_.x << "; " << center_.y << ")" << std::endl;
}
