#include <iostream>
#include <algorithm>
#include <list>
#include <iterator>
#include <cmath>

void task1()
{
  std::list<double> list(std::istream_iterator<double>(std::cin), std::istream_iterator<double>());

  if ((!std::cin.eof()) && (std::cin.fail()))
  {
    throw std::runtime_error("Reading failed");
  }

  std::transform(list.begin(), list.end(), std::ostream_iterator<double>(std::cout, " "), [](double& list) { return list * M_PI; });
  std::cout << std::endl;
}
