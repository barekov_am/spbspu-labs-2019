#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "shape.hpp"

class Square : public Shape
{
public:
  Square(Point center);
  ~Square() override = default;
  void draw() const override;
};

#endif // SQUARE_HPP
