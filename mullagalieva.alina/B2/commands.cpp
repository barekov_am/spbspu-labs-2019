#include "commands.hpp"

#include <iostream>
#include <string>


void add(QueueWithPriority<std::string>& queue, std::stringstream& in)
{
  std::string priority;
  in >> priority >> std::ws;
  std::string command;
  getline(in, command);

  if ((priority.empty()) || (command.empty()))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  QueueWithPriority<std::string>::ElementPriority elementPriority;
  if (priority == "low")
  {
    elementPriority = QueueWithPriority<std::string>::ElementPriority::LOW;
  }
  else if (priority == "normal")
  {
    elementPriority = QueueWithPriority<std::string>::ElementPriority::NORMAL;
  }
  else if (priority == "high")
  {
    elementPriority = QueueWithPriority<std::string>::ElementPriority::HIGH;
  }
  else
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  queue.putElementToQueue(command, elementPriority);
}

void get(QueueWithPriority<std::string>& queue, std::stringstream& in)
{
  if (queue.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  std::string command;
  in >> command;
  if (!command.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  std::cout << queue.getFrontElement() << "\n";
}

void accelerate(QueueWithPriority<std::string>& queue, std::stringstream& in)
{
  std::string command;
  in >> command;
  if (!command.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }
  queue.accelerate();
}
