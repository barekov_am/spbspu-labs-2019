#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <sstream>
#include "queue-with-priority.hpp"

void add(QueueWithPriority<std::string>& queue, std::stringstream& in);
void get(QueueWithPriority<std::string>& queue, std::stringstream& in);
void accelerate(QueueWithPriority<std::string>& queue, std::stringstream& in);

#endif
