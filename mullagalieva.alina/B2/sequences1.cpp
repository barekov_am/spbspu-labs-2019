#include <iostream>
#include <string>
#include <sstream>

#include "commands.hpp"
#include "queue-with-priority.hpp"

void sequences1()
{
  QueueWithPriority<std::string> queue;
  std::string tmp;

  while (getline(std::cin, tmp))
  {
    if (!std::cin.eof() && std::cin.fail())
    {
      throw std::runtime_error("Input failed.\n");
    }
    std::stringstream in(tmp);
    std::string command;
    in >> command;

    if (command == "add")
    {
      add(queue, in);
    }
    else if (command == "get")
    {
      get(queue, in);
    }
    else if (command == "accelerate")
    {
      accelerate(queue, in);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
