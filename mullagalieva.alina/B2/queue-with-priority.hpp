#ifndef AP_QUEUE_WITH_PRIORITY
#define AP_QUEUE_WITH_PRIORITY

#include <list>
#include <stdexcept>

template <typename T>
class QueueWithPriority
{
public:

  enum class ElementPriority
  {
    LOW,
    NORMAL,
    HIGH
  };

  void putElementToQueue(const T& element, ElementPriority priority);
  T getFrontElement();
  void accelerate();
  bool empty() const;

private:
  std::list<T> low;
  std::list<T> normal;
  std::list<T> high;
};

template <typename T>
void QueueWithPriority<T>::putElementToQueue(const T& element, ElementPriority priority)
{
  switch (priority)
  {
  case ElementPriority::HIGH:
  {
    high.push_back(element);
    break;
  }
  case ElementPriority::NORMAL:
  {
    normal.push_back(element);
    break;
  }
  case ElementPriority::LOW:
  {
    low.push_back(element);
    break;
  }
  }
}

template <typename T>
T QueueWithPriority<T>::getFrontElement()
{
  if (!high.empty())
  {
    T element = high.front();
    high.pop_front();
    return element;
  }
  if (!normal.empty())
  {
    T element = normal.front();
    normal.pop_front();
    return element;
  }
  if (!low.empty())
  {
    T element = low.front();
    low.pop_front();
    return element;
  }
  throw std::invalid_argument("Queue is empty.\n");
}

template <typename T>
void QueueWithPriority<T>::accelerate()
{
  if (!low.empty())
  {
    high.splice(high.end(), low);
  }
}

template <typename T>
bool QueueWithPriority<T>::empty() const
{
  return (low.empty() && normal.empty() && high.empty());
}

#endif // AP_QUEUE_WITH_PRIORITY
