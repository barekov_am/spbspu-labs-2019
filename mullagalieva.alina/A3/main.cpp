#include <iostream>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
  mullagalieva::Circle circle({4.12, 6.7}, 3.5);
  mullagalieva::Rectangle rectangle(7.65, 3.65, 8.91, 13.01);
  mullagalieva::Triangle triangle({3.17, 8.1}, {14.1, 1.7}, {18.9, 7.3});

  mullagalieva::Shape* shapePointer = &circle;
  shapePointer->show();
  std::cout << "Moving to the point (1; 2.9)" << "\n";
  std::cout << "Scaling by a multiplier of 2.5" << "\n";
  shapePointer->move({1, 2.9});
  shapePointer->scale(2.5);
  shapePointer->show();

  shapePointer = &rectangle;
  shapePointer->show();
  std::cout << "Moving dx = 3.4, dy = 5.6" << "\n";
  std::cout << "Scaling by a multiplier of 1.5" << "\n";
  shapePointer->move(3.4, 5.6);
  shapePointer->scale(1.5);
  shapePointer->show();

  shapePointer = &triangle;
  shapePointer->show();
  std::cout << "Moving to the point (6.12; 7.34)" << "\n";
  std::cout << "Scaling by a multiplier of 2" << "\n";
  shapePointer->move({6.12, 7.34});
  shapePointer->scale(2);
  shapePointer->show();

  mullagalieva::CompositeShape::shape_ptr part1 = std::make_shared<mullagalieva::Circle>(circle);
  mullagalieva::CompositeShape::shape_ptr part2 = std::make_shared<mullagalieva::Rectangle>(rectangle);
  mullagalieva::CompositeShape::shape_ptr part3 = std::make_shared<mullagalieva::Triangle>(triangle);

  mullagalieva::CompositeShape compositeShape;
  compositeShape.addShape(part1);
  compositeShape.addShape(part2);
  compositeShape.addShape(part3);
  compositeShape.show();

  std::cout << "Moving dx = 4.5, dy = 7.2" << "\n";
  std::cout << "Scaling by a multiplier of 2.1" << "\n";
  compositeShape.move(4.5, 7.2);
  compositeShape.scale(2.1);
  compositeShape.show();
    
  return 0;
}
