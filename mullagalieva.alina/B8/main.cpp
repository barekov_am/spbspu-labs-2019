#include <iostream>
#include <stdexcept>
#include "task.hpp"

const int MIN_WIDTH = 25;

int main(int argc, char* argv[])
{
  try
  {
    if ((argc > 3) || (argc == 2))
    {
      throw std::invalid_argument("Invalid arguments amount");
    }

    size_t width = 40;

    if (argc == 3)
    {
      if (std::string(argv[1]) != "--line-width")
      {
        throw std::invalid_argument("Invalid arguments");
      }

      std::string value = argv[2];
      for (char ch : value)
      {
        if (!std::isdigit(ch))
        {
          throw std::invalid_argument("Invalid width argument");
        }
      }
      width = std::stoul(value);
      if (width < MIN_WIDTH)
      {
        throw std::invalid_argument("Invalid width");
      }
    }

    Text text(width);
    text.formatText();
  }

  catch (const std::exception& e)
  {
    std::cerr << e.what() << '\n';
    return 1;
  }

  return 0;
}
