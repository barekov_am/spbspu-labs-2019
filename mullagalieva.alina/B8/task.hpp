#ifndef B8_TASK_HPP
#define B8_TASK_HPP

#include <vector>
#include <string>

class Text
{
public:
  Text(size_t width);

  void readText();
  void formatText();

private:
  std::vector<std::string> text_;
  size_t maxWidth_;
};

std::string readWord(char c);
std::string readNumber(char c);
std::string readPunctuation(char c);
std::string readHyphen(char c);

bool isNotOnePunctuation(std::vector<std::string>& vector);

#endif
