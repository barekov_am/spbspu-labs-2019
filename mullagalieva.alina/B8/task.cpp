#include <iostream>
#include <sstream>
#include <algorithm>
#include <cctype>
#include "task.hpp"

const std::size_t WORD_MAX_LENGTH = 20;

std::string readWord(char c)
{
  std::string tmp;
  tmp.push_back(c);
  while (std::isalpha(std::cin.peek()) || (std::cin.peek() == '-'))
  {
    char ch = static_cast<char>(std::cin.get());
    if ((ch == '-') && (std::cin.peek() == '-'))
    {
      throw std::invalid_argument("Two '-' in a row is not allowed!");
    }
    tmp.push_back(ch);
  }

  if (tmp.length() > WORD_MAX_LENGTH)
  {
    throw std::invalid_argument("Word length is more than 20");
  }

  return  tmp;
}

std::string readNumber(char c)
{
  std::string tmp;
  tmp.push_back(c);
  int counterOfDots = 0;
  while (std::isdigit(std::cin.peek()) || (std::cin.peek() == '.'))
  {
    char ch = static_cast<char>(std::cin.get());
    if (ch == '.')
    {
      ++counterOfDots;
      if (counterOfDots > 1)
      {
        throw std::invalid_argument("Invalid input");
      }
    }
    tmp.push_back(ch);
  }

  if (tmp.length() > WORD_MAX_LENGTH)
  {
    throw std::invalid_argument("Number length is more than 20");
  }

  return tmp;
}

std::string readHyphen(char c)
{
  const std::size_t maxLength = 3;
  std::string tmp;
  tmp.push_back(c);
  while (std::cin.peek() == '-')
  {
    tmp.push_back(static_cast<char>(std::cin.get()));
  }

  if (tmp.length() != maxLength)
  {
    throw std::invalid_argument("Invalid input");
  }

  return tmp;
}

std::string readPunctuation(char c)
{
  std::string tmp;
  tmp.push_back(c);
  return tmp;
}

bool isNotOnePunctuation(std::vector<std::string>& vector)
{
  size_t j = vector.size() - 1;
  for (size_t i = 0; i < j; i++)
  {
    if ((vector[i][0] == ',') && (vector[i + 1][0] == '-'))
    {
      vector[i + 1].insert(0, " ");
    }
    else if (ispunct(vector[i][0]) && (ispunct(vector[i + 1][0])))
    {
      return true;
    }
  }
  return false;
}

Text::Text(size_t width) :
  text_(),
  maxWidth_(width)
{}

void Text::readText()
{
  std::string line;

  while (std::cin)
  {
    std::cin >> std::ws;
    char currentChar = static_cast<char>(std::cin.get());
    char nextChar = static_cast<char>(std::cin.peek());

    if (std::isalpha(currentChar))
    {
      text_.push_back(readWord(currentChar));
    }
    else if ((std::isdigit(currentChar)) || ((currentChar == '+' || currentChar == '-') && std::isdigit(nextChar)))
    {
      text_.push_back(readNumber(currentChar));
    }
    else if (std::ispunct(currentChar))
    {
      if (currentChar == '-')
      {
        text_.push_back(readHyphen(currentChar));
      }
      else
      {
        text_.push_back(readPunctuation(currentChar));
      }
    }
  }
}

void Text::formatText()
{
  readText();
  if (text_.empty())
  {
    return;
  }

  if ((ispunct(text_[0][0]) && (text_[0][0] != '+') && (text_[0][0] != '-')) || ((text_[0][0] == '-') && (text_[0][1] == '-')))
  {
    throw std::invalid_argument("Text beginning with punctuation is not allowed!");
  }

  if (isNotOnePunctuation(text_))
  {
    throw std::invalid_argument("2 punctuations in a row is now allowed!");
  }

  std::for_each(text_.begin(), text_.end(), [&](std::string& input)
    {
      if (isalpha(input[0]) || isdigit(input[0]) || (input[0] == '+') || ((input[0] == '-')))
      {
        input.insert(0, " ");
      }
    });

  size_t currentWidth = 0;
  bool isLineBeginning = true;
  size_t j = text_.size() - 1;
  for (size_t i = 0; i < j; i++)
  {
    if (isLineBeginning)
    {
      text_[i].erase(0, 1);
      isLineBeginning = false;
    }

    if ((text_[i + 1][1] == '-') && (text_[i].size() + text_[i + 1].size() + currentWidth > maxWidth_))
    {
      std::cout << "\n" << text_[i].erase(0, 1);
      currentWidth = text_[i].size();
      continue;
    }
    else if (ispunct(text_[i + 1][0]) && (text_[i].size() + text_[i + 1].size() + currentWidth > maxWidth_))
    {
      std::cout << "\n" << text_[i].erase(0, 1);
      currentWidth = text_[i].size();
      continue;
    }

    if ((currentWidth + text_[i].size()) < maxWidth_)
    {
      std::cout << text_[i];
      currentWidth += text_[i].size();
      isLineBeginning = false;
    }
    else if (((currentWidth + text_[i].size()) > maxWidth_))
    {
      std::cout << "\n" << text_[i].erase(0, 1);
      currentWidth = text_[i].size();
      isLineBeginning = false;
    }
    else if (((currentWidth + text_[i].size()) == maxWidth_))
    {
      std::cout << text_[i] << "\n";
      currentWidth = 0;
      isLineBeginning = true;
    }
  }

  if (currentWidth + text_[text_.size() - 1].size() <= maxWidth_)
  {
    isLineBeginning ? std::cout << text_[text_.size() - 1].erase(0, 1) : std::cout << text_.back();
  }
  else if (text_[text_.size() - 1].size() + currentWidth > maxWidth_)
  {
    std::cout << "\n" << text_[text_.size() - 1].erase(0, 1);
  }

  std::cout << "\n";
}
