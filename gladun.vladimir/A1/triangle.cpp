#include "triangle.hpp"
#include <stdexcept>
#include <cmath>
#include <algorithm>

Triangle::Triangle(const point_t &pos_a, const point_t &pos_b, const point_t &pos_c) :
  pos_a_(pos_a),
  pos_b_(pos_b),
  pos_c_(pos_c)
{
  if (getArea() == 0)
  {
    throw std::invalid_argument("Area mustn't be = 0");
  }
}

point_t Triangle::getCenter() const
{
  return {(pos_a_.x + pos_b_.x + pos_c_.x) / 3, (pos_a_.y + pos_b_.y + pos_c_.y) / 3};
}

double Triangle::getArea() const
{
  return fabs((pos_b_.x - pos_a_.x) * (pos_c_.y - pos_a_.y)
      - (pos_c_.x - pos_a_.x) * (pos_b_.y - pos_a_.y)) / 2;
}

rectangle_t Triangle::getFrameRect() const
{
  const double x_max = std::max({pos_a_.x, pos_b_.x, pos_c_.x});
  const double x_min = std::min({pos_a_.x, pos_b_.x, pos_c_.x});
  const double y_max = std::max({pos_a_.y, pos_b_.y, pos_c_.y});
  const double y_min = std::min({pos_a_.y, pos_b_.y, pos_c_.y});

  const double rect_width = x_max - x_min;
  const double rect_height = y_max - y_min;

  return {rect_width, rect_height, {x_max - rect_width / 2, y_max - rect_height / 2}};
}

void Triangle::move(const point_t &point)
{
  const point_t center = getCenter();
  move(point.x - center.x, point.y - center.y);
}

void Triangle::move(double dx, double dy)
{
  pos_a_.x += dx;
  pos_a_.y += dy;

  pos_b_.x += dx;
  pos_b_.y += dy;

  pos_c_.x += dx;
  pos_c_.y += dy;
}
