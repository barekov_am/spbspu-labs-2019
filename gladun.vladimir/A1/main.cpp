#include <iostream>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "polygon.hpp"

void printInfo(const Shape *shape)
{
  std::cout << "Area: " << shape->getArea() << "\n"
      << "Frame rectangle's centre: " << "(" << shape->getFrameRect().pos.x
      << ", " << shape->getFrameRect().pos.y << ")\n"
      << "Frame rectangle's width - " << shape->getFrameRect().width
      << " & height - " << shape->getFrameRect().height << "\n";
}

int main()
{
  Circle shape_1(12.34, {5, 6});
  Shape *circle = &shape_1;

  Rectangle shape_2({7, 8, {9, 10}});
  Shape *rectangle = &shape_2;

  Triangle shape_3({{5, 5}, {16, 8}, {19, 15}});
  Shape *triangle = &shape_3;

  point_t points[] = {{2, 1}, {2, 5}, {4, 7}, {8, 7}, {8, 1}};
  Polygon shape_4(points, 5);
  Shape *polygon = &shape_4;

  Shape *shapes[] = {circle, rectangle, triangle, polygon};
  int i = 1;

  for (Shape *shape : shapes)
  {
    std::cout << "Shape " << i << "\n";
    printInfo(shape);

    std::cout << "Move shape to a point (2, 2)\n";
    shape->move({2, 2});
    printInfo(shape);

    std::cout << "Shift shape's x by 5 & y by 4\n";
    shape->move(5, 4);
    printInfo(shape);
    std::cout << "\n\n";

    i++;
  }

  return 0;
}
