#include "rectangle.hpp"
#include <stdexcept>

Rectangle::Rectangle(double width, double height, const point_t &pos) :
  width_(width),
  height_(height),
  pos_(pos)
{
  if (width <= 0)
  {
    throw std::invalid_argument("Width must be > 0");
  }

  if (height <= 0)
  {
    throw std::invalid_argument("Height must be > 0");
  }
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return {width_, height_, pos_};
}

void Rectangle::move(const point_t &pos)
{
  pos_ = pos;
}

void Rectangle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}
