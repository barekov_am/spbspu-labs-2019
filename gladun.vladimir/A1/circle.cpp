#include "circle.hpp"
#include <stdexcept>
#include <cmath>

Circle::Circle(double radius, const point_t &pos) :
  radius_(radius),
  pos_(pos)
{
  if (radius <= 0)
  {
    throw std::invalid_argument("Radius must be > 0");
  }
}

double Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

rectangle_t Circle::getFrameRect() const
{
  return {2 * radius_, 2 * radius_, pos_};
}

void Circle::move(const point_t &pos)
{
  pos_ = pos;
}

void Circle::move(double dx, double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}
