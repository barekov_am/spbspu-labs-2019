#ifndef COMPOSITESHAPE_HPP_INCLUDED
#define COMPOSITESHAPE_HPP_INCLUDED
#include "shape.hpp"
#include <memory>

namespace gladun
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const std::shared_ptr<Shape> &);
    CompositeShape(const CompositeShape &);
    CompositeShape(CompositeShape &&);
    CompositeShape & operator=(const CompositeShape &);
    CompositeShape & operator=(CompositeShape &&);
    friend bool operator==(const CompositeShape &, const CompositeShape &);
    ~CompositeShape() = default;
    void addShape(const std::shared_ptr<Shape> &);
    std::string getName() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void printFeatures() const override {};
    void move(const double dx, const double dy) override;
    void move(const point_t &pos) override;
    void scale(const double k) override;
    void rotate(double angle) override;

  private:
    std::unique_ptr <std::shared_ptr<Shape>[]> shapes_;
    size_t size_;
  };

  bool operator==(const CompositeShape &, const CompositeShape &);
}

#endif
