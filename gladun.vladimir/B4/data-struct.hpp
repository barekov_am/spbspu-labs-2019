#ifndef DATA_STRUCT_HPP
#define DATA_STRUCT_HPP

#include <stdexcept>

namespace gladun
{
  struct DataStruct
  {
    int key1;
    int key2;
    std::string str;
  };

  void printDataStruct(const DataStruct &data);

  DataStruct readDataStruct(std::istream &input);
};

#endif
