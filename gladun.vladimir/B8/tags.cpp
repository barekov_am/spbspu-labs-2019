#include "tags.hpp"

gladun::TagsGenerator::TagsGenerator(std::istream & istream) :
   istream(istream)
{
}

std::vector<gladun::tags_t> gladun::TagsGenerator::parseText()
{
  if (!istream)
  {
    throw std::invalid_argument("Invalid input stream");
  }
  genTags();
  checkInput();
  return tagsVector;
}

void gladun::TagsGenerator::readWord()
{
  tags_t tags{
     "", tags_type_t::WORD
  };

  do
  {
    auto chr = static_cast<char>(istream.get());
    tags.value.push_back(chr);
    if (chr == '-' && istream.peek() == '-')
    {
      tags.value.pop_back();
      istream.unget();
      break;
    }
  } while ((std::isalpha(static_cast<char>(istream.peek()), std::locale())) || (istream.peek() == '-'));
  tagsVector.push_back(tags);
}

void gladun::TagsGenerator::readNum()
{
  tags_t tags{
     "", tags_type_t::NUMBER
  };

  char decimalPoint = std::use_facet<std::numpunct<char>>(std::locale()).decimal_point();
  bool decimalPointRead = false;

  do
  {
    auto chr = static_cast<char>(istream.get());
    if (chr == decimalPoint)
    {
      if (decimalPointRead)
      {
        istream.unget();
        break;
      }
      decimalPointRead = true;
    }
    tags.value.push_back(chr);
  } while ((std::isdigit<char>(static_cast<char>(istream.peek()), std::locale()) || (istream.peek() == decimalPoint)));
  tagsVector.push_back(tags);
}

void gladun::TagsGenerator::readDash()
{
  tags_t tags{
     "", tags_type_t::DASH
  };

  while (istream.peek() == '-')
  {
    auto chr = static_cast<char>(istream.get());
    tags.value.push_back(chr);
  }
  tagsVector.push_back(tags);
}

void gladun::TagsGenerator::genTags()
{
  while (istream)
  {
    auto chr = static_cast<char>(istream.get());
    while (std::isspace(chr, std::locale()))
    {
      chr = static_cast<char>(istream.get());
    }

    if (isalpha(chr, std::locale()))
    {
      istream.unget();
      readWord();
    }
    else if (chr == '-')
    {
      if (istream.peek() == '-')
      {
        istream.unget();
        readDash();
      }
      else
      {
        istream.unget();
        readNum();
      }
    }
    else if ((chr == '+') || (isdigit(chr, std::locale())))
    {
      istream.unget();
      readNum();
    }
    else if (ispunct(chr, std::locale()))
    {
      tags_t tags{
         "", tags_type_t::PUNCT
      };
      tags.value.push_back(chr);
      tagsVector.push_back(tags);
    }
  }
}

void gladun::TagsGenerator::checkInput()
{
  if (!tagsVector.empty() &&
      (tagsVector.front().type != tags_type_t::WORD)
      && (tagsVector.front().type != tags_type_t::NUMBER))
  {
    throw std::invalid_argument("Input starts with punctuation or dash");
  }

  for (auto tags = tagsVector.begin(); tags != tagsVector.end(); ++tags)
  {
    switch (tags->type)
    {
      case tags_type_t::WORD:
      case tags_type_t::NUMBER:
        if (tags->value == "-" || tags->value == "+")
        {
          throw std::invalid_argument("Invalid dash sign");
        }
        if (tags->value.size() > 20)
        {
          throw std::invalid_argument("Length of char sequence must be more than 20");
        }
        break;
      case tags_type_t::DASH:
        if (tags->value.size() != 3)
        {
          throw std::invalid_argument("Invalid dash sign");
        }
        if (tags != tagsVector.begin())
        {
          const auto prev = std::prev(tags);
          if ((prev->type == tags_type_t::DASH) || ((prev->type == tags_type_t::PUNCT) && (prev->value != ",")))
          {
            throw std::invalid_argument("Invalid char sequence");
          }
        }
        break;
      case tags_type_t::PUNCT:
        if (tags != tagsVector.begin()) {
          const auto prev = std::prev(tags);
          if ((prev->type == tags_type_t::DASH) || (prev->type == tags_type_t::PUNCT))
          {
            throw std::invalid_argument("Invalid char sequence");
          }
        }
        break;
      case tags_type_t::WHITESPACE:
        break;
    }
  }
}
