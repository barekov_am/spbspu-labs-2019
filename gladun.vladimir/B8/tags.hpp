#ifndef B8_GLADUN_VLADIMIR_OUTPUT_TAGS_HPP
#define B8_GLADUN_VLADIMIR_OUTPUT_TAGS_HPP

#include <iostream>
#include <vector>
#include <locale>

namespace gladun
{
  enum class tags_type_t
  {
    WORD,
    PUNCT,
    DASH,
    NUMBER,
    WHITESPACE
  };

  struct tags_t
  {
    std::string value;
    tags_type_t type;
  };

  class TagsGenerator
  {
    public:
    explicit TagsGenerator(std::istream &istream);
    std::vector<tags_t> parseText();

    private:
    std::istream &istream;
    std::vector<gladun::tags_t> tagsVector;
    void readWord();
    void readNum();
    void readDash();
    void genTags();
    void checkInput();
  };
}
#endif
