#include "output.hpp"

gladun::OutputHandler::OutputHandler(std::ostream& ostream, size_t width) :
   ostream(ostream),
   lineWidth(width)
{
}

void gladun::OutputHandler::printLines(const std::vector<tags_t> & tags)
{
  formatLines(tags);
  for (const auto &line : linesVector)
  {
    ostream << line << std::endl;
  }
}

void gladun::OutputHandler::formatLines(const std::vector<tags_t> & tags)
{
  std::vector<gladun::tags_t> temp;
  size_t currentWidth = 0;

  if (tags.empty())
  {
    temp.push_back(tags_t{
       " ", tags_type_t::WHITESPACE
    });
  }

  for (size_t i = 0; i < tags.size(); i++)
  {
    if (currentWidth + tags[i].value.size() > lineWidth)
    {
      connectTags(temp, i, tags[i].type);
      i -= 1;
      currentWidth = 0;
    }
    else
    {
      temp.push_back(tags[i]);
      currentWidth += tags[i].value.size();

      if (currentWidth + 1 <= lineWidth)
      {
        switch (tags[i].type)
        {
          case tags_type_t::NUMBER:
          case tags_type_t::WORD:
            if (i + 1 < tags.size())
            {
              if (tags[i + 1].type != tags_type_t::PUNCT)
              {
                temp.push_back(tags_t{
                   " ", tags_type_t::WHITESPACE
                });
                currentWidth++;
              }
            }
            break;
          case tags_type_t::PUNCT:
          case tags_type_t::DASH:
            temp.push_back(tags_t{
               " ", tags_type_t::WHITESPACE
            });
            currentWidth++;
            break;
          case tags_type_t::WHITESPACE:
            break;
        }
      }
    }
  }
  linesVector.push_back(createLine(temp));
}

std::string gladun::OutputHandler::createLine(const std::vector<gladun::tags_t> &tags)
{
  std::string buffer = "";
  for (const auto &t : tags)
  {
    buffer += t.value;
  }
  if (tags.back().type == tags_type_t::WHITESPACE)
  {
    buffer.pop_back();
  }
  return buffer;
}

void gladun::OutputHandler::connectTags(std::vector<gladun::tags_t>& tags, size_t& pos, tags_type_t type)
{
  switch (type)
  {
    case tags_type_t::WORD:
    case tags_type_t::NUMBER:
      linesVector.push_back(createLine(tags));
      break;
    case tags_type_t::PUNCT:
    case tags_type_t::DASH:
      while (tags.back().type != tags_type_t::NUMBER
             && tags.back().type != tags_type_t::WORD
             && !tags.empty())
      {
        if (tags.back().type != tags_type_t::WHITESPACE && pos > 0)
        {
          pos--;
        }
        tags.pop_back();
      }
      if (pos > 0)
      {
        pos--;
      }
      tags.pop_back();
      linesVector.push_back(createLine(tags));
      break;
    case tags_type_t::WHITESPACE:
      break;
  }
  tags.clear();
}
