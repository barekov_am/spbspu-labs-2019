#ifndef B8_GLADUN_VLADIMIR_OUTPUT_HPP
#define B8_GLADUN_VLADIMIR_OUTPUT_HPP

#include <string>

#include "tags.hpp"

namespace gladun
{
  class OutputHandler
  {
    public:
    OutputHandler(std::ostream& ostream, size_t width);
    void printLines(const std::vector<tags_t> & tagsArray);

    private:
    std::ostream & ostream;
    size_t lineWidth;
    std::vector<std::string> linesVector;
    void formatLines(const std::vector<tags_t> & tags);
    void connectTags(std::vector<gladun::tags_t>& tags, size_t& pos, tags_type_t type);
    std::string createLine(const std::vector<gladun::tags_t> &tags);
  };
}

#endif
