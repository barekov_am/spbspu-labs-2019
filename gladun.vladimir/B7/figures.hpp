#ifndef FIGURES_HPP
#define FIGURES_HPP

#include "shape.hpp"

class Triangle:public Shape
{
public:
  Triangle();
  Triangle(int x, int y);

  void draw() const override;
};

class Square:public Shape
{
public:
  Square();
  Square(int x, int y);

  void draw() const override;
};

class Circle:public Shape
{
public:
  Circle();
  Circle(int x, int y);

  void draw() const override;

};

#endif
