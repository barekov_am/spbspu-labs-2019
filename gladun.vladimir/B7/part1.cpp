#include <functional>
#include <iostream>
#include <iterator>
#include <algorithm>
#include "functor.hpp"


void part1()
{
  Functor numbers;
  std::for_each(std::istream_iterator<double>(std::cin), std::istream_iterator<double>(), std::ref(numbers));

  if (!std::cin.eof())
  {
    throw std::ios_base::failure("Trouble with std::cin\n");
  }

  numbers.showAll();
}
