#ifndef FUNCTOR_HPP
#define FUNCTOR_HPP

#include <vector>

class Functor
{
public:
  void operator()(double num);
  void showAll();

private:
  std::vector<double> vec;
};


#endif
