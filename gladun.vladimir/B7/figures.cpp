#include "figures.hpp"
#include <iostream>

Triangle::Triangle():
  Shape()
{}

Triangle::Triangle(int x, int y):
  Shape(x, y)
{}

void Triangle::draw() const
{
  std::cout << "TRIANGLE (" << x_ << ";" << y_ << ")\n";
}

Square::Square():
  Shape()
{}

Square::Square(int x, int y):
  Shape(x, y)
{}

void Square::draw() const
{
  std::cout << "SQUARE (" << x_ << ";" << y_ << ")\n";
}

Circle::Circle():
  Shape()
{}

Circle::Circle(int x, int y):
  Shape(x, y)
{}

void Circle::draw() const
{
  std::cout << "CIRCLE (" << x_ << ";" << y_ << ")\n";
}
