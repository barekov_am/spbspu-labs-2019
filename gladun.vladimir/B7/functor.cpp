#include "functor.hpp"
#include <cmath>
#include <algorithm>
#include <iostream>

void Functor::operator()(double num)
{
  double numAfter = M_PI * num;
  vec.push_back(numAfter);
}

void Functor::showAll()
{
  if (vec.empty())
  {
    return;
  }
  std::for_each(vec.begin(), vec.end(),[](double num){std::cout << num << " ";});
}
