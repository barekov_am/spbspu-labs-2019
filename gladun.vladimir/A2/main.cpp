#include <iostream>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

using namespace gladun;

int main()
{
  try
  {
    Rectangle shape_1({0, 0}, 30, 10);
    Shape *rectangle = &shape_1;

    rectangle->move(10, -5);
    rectangle->move({10, -5});

    Circle shape_2({3, -2}, 5);
    Shape *circle = &shape_2;

    circle->move(-7, 1);
    circle->move({12, 8});

    Triangle shape_3({0, 0}, {0, 3}, {4, 0});
    Shape *triangle = &shape_3;

    triangle->move(-6, 8);
    triangle->move({12, -3});

    Shape *shapes[] = {rectangle, circle, triangle};

    for (const Shape *shape : shapes)
    {
      shape->printData();
      rectangle_t frame_rect = shape->getFrameRect();

      std::cout << "  area = " << shape->getArea() << std::endl
          << "  framing rectangle" << std::endl
          << "    pos (" << frame_rect.pos.x << ", " << frame_rect.pos.y << ")" << std::endl
          << "    width = " << frame_rect.width << std::endl
          << "    height - " << frame_rect.height << std::endl << std::endl;
    }
  }
  catch (const std::invalid_argument &e)
  {
    std::cerr << e.what() << "\n";
    return 1;
  }

  return 0;
}
