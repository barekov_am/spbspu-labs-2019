#include "shape-container.hpp"
#include <string>
#include <algorithm>
#include <iterator>
#include <iostream>

ShapeContainer::ShapeContainer() :
  triangles_(0),
  squares_(0),
  rectangles_(0),
  vertices_(0)
{
}

void ShapeContainer::readShapesList()
{
  std::string line;
  while (std::getline(std::cin, line)) {
    if (line.empty()) {
      continue;
    }

    line.erase(remove(line.begin(), line.end(), ' '), line.end());
    line.erase(remove(line.begin(), line.end(), '\t'), line.end());

    const auto open_bracket = line.find_first_of('(');
    if (open_bracket == std::string::npos) {
      continue;
    }
    const size_t number_of_points = std::stoi(line.substr(0, open_bracket));

    if (number_of_points < 1) {
      throw std::runtime_error("Incorrect number of dots!\n");
    }

    line.erase(0, open_bracket);
    const auto vector = readDots(line);

    if (vector.size() != number_of_points) {
      throw std::runtime_error("Incorrect arguments!\n");
    }

    if (number_of_points == 3) {
      shapes_.push_back(vector);
      vertices_ += vector.size();
      ++triangles_;
      continue;
    }
    if (number_of_points == 4) {
      if (checkSquare(vector)) {
        shapes_.push_back(vector);
        vertices_ += vector.size();
        ++squares_;
        ++rectangles_;
        continue;
      }
      shapes_.push_back(vector);
      vertices_ += vector.size();
      ++rectangles_;
      continue;
    }
    shapes_.push_back(vector);
    vertices_ += vector.size();
  }
}

void ShapeContainer::printVertices() const
{
  std::cout << "Vertices: " << vertices_ << '\n';
}

void ShapeContainer::printShapesNumbers() const
{
  std::cout << "Triangles: " << triangles_ << '\n';
  std::cout << "Squares: " << squares_ << '\n';
  std::cout << "Rectangles: " << rectangles_ << '\n';
}

void ShapeContainer::erasePentagons()
{
  shapes_.erase(std::remove_if(shapes_.begin(), shapes_.end(), [](const shape& tmp) {
    return tmp.size() == 5;
  }), shapes_.end());
}

std::ostream& operator<<(std::ostream& stream, const point_t& point)
{
  stream << " (" << point.x << "; " << point.y << ") ";
  return stream;
}

void ShapeContainer::printPoints() const
{
  shape vector(shapes_.size());
  std::transform(shapes_.begin(), shapes_.end(), vector.begin(), GetPoint());
  std::cout << "Points:";
  std::copy(vector.begin(), vector.end(), std::ostream_iterator<point_t>(std::cout));
}

std::ostream& operator<<(std::ostream& stream, const std::vector<shape>& tmp)
{
  stream << "\nShapes:\n";
  for (const auto& i : tmp) {
    stream << i.size() << " ";
    for (auto j : i) {
      stream << " (" << j.x << "; " << j.y << ") ";
    }
    stream << '\n';
  }
  return stream;
}

void ShapeContainer::printContainer() const
{
  std::cout << shapes_;
}

bool ShapeContainer::comparator(const shape& a, const shape& b)
{
  if (a.size() < b.size()) {
    return true;
  }
  if ((a.size() == 4) && (b.size() == 4)) {
    if (checkSquare(a)) {
      if (checkSquare(b)) {
        return a.at(0).x < b.at(0).x;
      }
      return true;
    }
  }
  return false;
}

void ShapeContainer::changeContainer()
{
  std::sort(shapes_.begin(), shapes_.end(), comparator);
}

shape ShapeContainer::readDots(std::string str) const
{
  std::vector<point_t> vector;
  while (!str.empty()) {
    const auto open_bracket = str.find_first_of('(');
    const auto semicolon = str.find_first_of(';');
    const auto close_bracket = str.find_first_of(')');
    if (close_bracket == std::string::npos || semicolon == std::string::npos || close_bracket == std::string::npos) {
      str.clear();
      break;
    }
    auto first_number = str.substr(open_bracket + 1, semicolon - open_bracket - 1);
    auto second_number = str.substr(semicolon + 1, close_bracket - semicolon - 1);
    point_t point{std::stoi(first_number), std::stoi(second_number)};
    vector.push_back(point);
    str.erase(open_bracket, close_bracket - open_bracket + 1);
  }
  return vector;
}
