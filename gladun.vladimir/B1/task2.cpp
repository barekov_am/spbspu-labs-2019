#include <fstream>
#include <memory>
#include <vector>

#include "output.hpp"

void theSecondTask(const char* filename)
{
  std::ifstream file(filename);
  if (!file) {
    throw std::ios_base::failure("Cannot open file.");
  }

  size_t bufferSize = 1024;
  size_t readPos = 0;

  std::unique_ptr<char[], decltype(&free)> arr(static_cast<char*>(malloc(bufferSize)), &free);
  if (!arr) {
    throw std::runtime_error("Failed to allocate space.");
  }

  while (file) {
    file.read(&arr[readPos], bufferSize - readPos);
    readPos += file.gcount();

    if (readPos == bufferSize) {
      bufferSize *= 2;
      std::unique_ptr<char[], decltype(&free)> tmp(static_cast<char*>(realloc(arr.get(), bufferSize)), &free);

      if (!tmp) {
        throw std::runtime_error("Failed to reallocate space.");
      }

      arr.release();
      std::swap(arr, tmp);
    }
  }

  if (!file.eof() && file.fail()) {
    throw std::ios_base::failure("Reading from file failed.");
  }

  std::vector<char> vector(&arr[0], &arr[readPos]);
  for (auto symbol : vector) {
    std::cout << symbol;
  }
}
