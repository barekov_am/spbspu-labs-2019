#ifndef B1_OUTPUT
#define B1_OUTPUT

#include <iostream>

template <typename T, typename = typename T::iterator>
std::ostream& operator<<(std::ostream& out, const T& container)
{
  for (const auto& i : container) {
    out << i << ' ';
  }

  return out;
}

#endif
