#include <forward_list>
#include <vector>

#include "output.hpp"
#include "sort.hpp"
#include "strategy.hpp"

void theFirstTask(const char* direction)
{
  std::vector<int> vectorBr;
  auto comparator = getComparator<int>(direction);

  int num = 0;
  while (std::cin >> num) {
    vectorBr.push_back(num);
  }

  if (!std::cin.eof() && std::cin.fail()) {
    throw std::ios_base::failure("Reading from std::cin failed.");
  }

  if (vectorBr.empty()) {
    return;
  }

  std::vector<int> vectorAt = vectorBr;
  std::forward_list<int> list(vectorBr.begin(), vectorBr.end());

  sort<ViaBrackets>(vectorBr, comparator);
  std::cout << vectorBr << '\n';

  sort<ViaAt>(vectorAt, comparator);
  std::cout << vectorAt << '\n';

  sort<ViaIterator>(list, comparator);
  std::cout << list << '\n';
}
