#ifndef B1_SORT
#define B1_SORT

#include <cstring>
#include <functional>

template <typename T>
std::function<bool (T, T)> getComparator(const char* order)
{
  if (std::strcmp(order, "ascending") == 0) {
    return [] (T lhs, T rhs) { return lhs < rhs; };
  }

  if (std::strcmp(order, "descending") == 0) {
    return [] (T lhs, T rhs) { return lhs > rhs; };
  }

  throw std::invalid_argument("Illegal order. Must be ""ascending"" or ""descending"".");
}

template <template <class T> class Strategy, typename T>
void sort(T& container, std::function<bool (typename T::value_type, typename T::value_type)> comparator)
{
  const auto begin = Strategy<T>::begin(container);
  const auto end = Strategy<T>::end(container);

  for (auto i = begin; i != end; i++) {
    for (auto j = begin; j != end; j++) {
      typename T::value_type& a = Strategy<T>::get(container, i);
      typename T::value_type& b = Strategy<T>::get(container, j);

      if (comparator(a, b)) {
        std::swap(a, b);
      }
    }
  }
}

#endif
