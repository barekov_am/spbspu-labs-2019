#ifndef PARAMETERS_HPP
#define PARAMETERS_HPP

#include "iostream"

class GetParameters
{
public:
  GetParameters();
  void operator()(int val);
  void printParameters(std::ostream &out);

private:
  int max_{}, min_{}, positive_{}, negative_{}, first_{}, last_{}, numElements_{};
  long long oddSum_{}, evenSum_{};
};
#endif 
