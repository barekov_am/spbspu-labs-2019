#include <iostream>
#include <algorithm>
#include <iterator>
#include <functional>
#include "parameters.hpp"

int main()
{
  GetParameters getParameters;
  std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), std::ref(getParameters));

  if (!std::cin.eof())
  {
    std::cerr << "Invalid input";
    return 1;
  }

  getParameters.printParameters(std::cout);

  return 0;
}
