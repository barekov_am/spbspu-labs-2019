#include <stdexcept>
#include <vector>
#include <random>
#include <ctime>
#include "sort.hpp"
#include "strategy.hpp"

void fillRandom(double* array, size_t size)
{
  for (size_t i = 0; i < size; ++i)
  {
    array[i] = -1.0 + static_cast<double>(rand()) / RAND_MAX * 2.0;
  }
}


void task4(size_t size, const char* direction)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Invalid size");
  }

  std::vector<double> vector;
  vector.resize(size);
  fillRandom(&vector[0], size);

  print(vector);
  sort<StrategyAt>(vector, direction);
  print(vector);
}

