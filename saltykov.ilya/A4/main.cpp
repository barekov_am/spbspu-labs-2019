#include <iostream>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "Polygon.hpp"
#include "composite-shape.hpp"

int main()
{
  const double dX = 2.5;
  const double dY = 1.5;
  const saltykov::point_t dot = {20, 10};

  std::cout << "CIRCLE\n\n";

  saltykov::Circle circle({0, 0}, 3.0);
  saltykov::Shape *ptrShape = &circle;
  ptrShape->show();
  std::cout << "> Moving along axes. dX: " << dX << " dY: " << dY << '\n';
  ptrShape->move(dX, dY);
  ptrShape->show();
  std::cout << "> Moving to a point. X: " << dot.x << " Y: " << dot.y << '\n';
  ptrShape->move(dot);
  ptrShape->show();
  std::cout << "> Scaling by a factor of 1.7" << '\n';
  ptrShape->scale(2);
  ptrShape->show();

  std::cout << "RECTANGLE\n\n";

  saltykov::Rectangle rectangle({{0, 0}, 3.0, 2.0});
  ptrShape = &rectangle;
  ptrShape->show();
  std::cout << "> Moving along axes. dX: " << dX << " dY: " << dY << '\n';
  ptrShape->move(dX, dY);
  ptrShape->show();
  std::cout << "> Moving to a point. X: " << dot.x << " Y: " << dot.y << '\n';
  ptrShape->move(dot);
  ptrShape->show();
  std::cout << "> Scaling by a factor of 1.7" << '\n';
  ptrShape->scale(1.5);
  ptrShape->show();

  std::cout << "TRIANGLE\n\n";

  saltykov::Triangle triangle({0, 0}, {0, 3}, {4, 0});
  ptrShape = &triangle;
  ptrShape->show();
  std::cout << "> Moving along axes. dX: " << dX << " dY: " << dY << '\n';
  ptrShape->move(dX, dY);
  ptrShape->show();
  std::cout << "> Moving to a point. X: " << dot.x << " Y: " << dot.y << '\n';
  ptrShape->move(dot);
  ptrShape->show();
  std::cout << "> Scaling by factor of 1.7" << '\n';
  ptrShape->scale(1.7);
  ptrShape->show();

  std::cout << "POLYGON\n\n";

  const size_t size = 6;
  saltykov::point_t points[size] = {{2, 2}, {6, 2}, {7, 3}, {7, 6}, {3, 6}, {1, 4}};

  saltykov::Polygon polygon(size, points);
  ptrShape = &polygon;
  ptrShape->show();
  std::cout << "> Moving along axes. dX: " << dX << " dY: " << dY << '\n';
  ptrShape->move(dX, dY);
  ptrShape->show();
  std::cout << "> Moving to a point. X: " << dot.x << " Y: " << dot.y << '\n';
  ptrShape->move(dot);
  ptrShape->show();
  std::cout << "> Scaling by factor of 1.7" << '\n';
  ptrShape->scale(1.7);
  ptrShape->show();

  std::cout << "COMPOSITESHAPE/n/n" << std::endl;
  std::cout << "creating empty composite shape" << std::endl;
  saltykov::CompositeShape composite1;
  std::cout << "adding a rectangle" << std::endl;
  saltykov::Shape::pointer rectangle_ = std::make_shared<saltykov::Rectangle>(rectangle);
  composite1.add(rectangle_);

  composite1.show();
  std::cout << "adding a triangle" << std::endl;
  saltykov::Shape::pointer triangle_ = std::make_shared<saltykov::Triangle>(triangle);
  composite1.add(triangle_);

  composite1.show();
  std::cout << "adding a circle" << std::endl;
  saltykov::Shape::pointer circle_ = std::make_shared<saltykov::Circle>(circle);
  composite1.add(circle_);

  composite1.show();
  std::cout << "adding a polygon" << std::endl;
  saltykov::Shape::pointer polygon_ = std::make_shared<saltykov::Polygon>(polygon);
  composite1.add(polygon_);

  composite1.remove(2);
  std::cout << "removing a triangle" << std::endl;
  composite1.show();

  //string
  return 0;
}
