#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "triangle.hpp"

const double ERROR = 0.0000001;

BOOST_AUTO_TEST_SUITE(TestsForTriangle)

BOOST_AUTO_TEST_CASE(TrinagleTestForFrameRectangle)
{
  saltykov::Triangle test_triangle({0, 0}, {3, 0}, {0, 4});
  saltykov::rectangle_t test_frame_rect = test_triangle.getFrameRect();
  test_triangle.move(2, 1);
  saltykov::rectangle_t test_frame_rect_after_moving = test_triangle.getFrameRect();
  BOOST_CHECK_CLOSE_FRACTION(test_frame_rect.width, test_frame_rect_after_moving.width, ERROR);
  BOOST_CHECK_CLOSE_FRACTION(test_frame_rect.height, test_frame_rect_after_moving.height, ERROR);
}

BOOST_AUTO_TEST_CASE(TriangleTestForTriangle)
{
  double scale_factor = 5.5;
  saltykov::Triangle test_triangle({4.35, 5.35}, {14.35, 5.35}, {15, 15});
  double test_area = test_triangle.getArea();
  test_triangle.scale(scale_factor);
  double test_area_after_scale = test_triangle.getArea();
  BOOST_CHECK_CLOSE_FRACTION(scale_factor * scale_factor * test_area, test_area_after_scale, ERROR);
}

BOOST_AUTO_TEST_CASE(TriangleInvalidScaleFactor)
{
  saltykov::Triangle test_triangle({1, 1}, {2, 0}, {0, 3});
  BOOST_CHECK_THROW(test_triangle.scale(-4.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TrianglePointsLieOnTheSameLine)
{
  BOOST_CHECK_THROW(saltykov::Triangle test_triangle({1, 1}, {2, 2}, {3, 3}), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
