#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace saltykov
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t& pos, double radius);
    Circle(double x, double y, double radius);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& centre) override;
    void move(double deltaX, double deltaY) override;
    void scale(double coefficient) override;
    void show() const override;
    void rotate(double) override;

  private:
    point_t centre_;
    double radius_;
  };
};

#endif // CIRCLE_HPP
