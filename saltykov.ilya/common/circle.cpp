#include "circle.hpp"

#include <iostream>
#include <stdexcept>
#include <cmath>

saltykov::Circle::Circle(const point_t& pos, double radius) :
  centre_(pos),
  radius_(radius)
{
  if (radius_ <= 0.0)
  {
    throw std::invalid_argument("radius must be positive");
  }
}

saltykov::Circle::Circle(double x, double y, double radius) :
  Circle({x, y}, radius)
{}

double saltykov::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

saltykov::rectangle_t saltykov::Circle::getFrameRect() const
{
  return {radius_ * 2, radius_ * 2, centre_};
}

void saltykov::Circle::move(const saltykov::point_t& centre)
{
  centre_ = centre;
}

void saltykov::Circle::move(double deltaX, double deltaY)
{
  centre_.x += deltaX;
  centre_.y += deltaY;
}

void saltykov::Circle::scale(double coefficient)
{
  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Circle scale coefficient must be positive");
  }
  radius_ *= coefficient;
}
void saltykov::Circle::show() const
{
  const rectangle_t frameRect = getFrameRect();
  std::cout << "Center coordinates (" << centre_.x << ","
    << centre_.y << ")" << '\n';
  std::cout << "Area: " << getArea() << '\n';
  std::cout << "Frame's height: " << frameRect.height << '\n';
  std::cout << "Frame's width: " << frameRect.width << '\n';
  std::cout << "Frame's centre coordinates: (" << frameRect.pos.x << ","
    << frameRect.pos.y << ")" << "\n\n";
}

void saltykov::Circle::rotate(double)
{ }

