#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "Polygon.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(testSuiteCompositeShape)

  const double ERROR = 0.0000001;

  BOOST_AUTO_TEST_CASE(CompositeShapeCopyConstructor)
  {
    saltykov::Shape::pointer rectangle_ = std::make_shared<saltykov::Rectangle>(saltykov::point_t {2, 2}, 2, 3);
    saltykov::Shape::pointer circle_ = std::make_shared<saltykov::Circle>(saltykov::point_t {1, 1}, 1);
    saltykov::CompositeShape composite1_(rectangle_);
    composite1_.add(circle_);

    const saltykov::rectangle_t frame1_ = composite1_.getFrameRect();
    const double area1_ = composite1_.getArea();

    saltykov::CompositeShape composite2_(composite1_);

    BOOST_CHECK_CLOSE_FRACTION(frame1_.pos.x, composite2_.getFrameRect().pos.x, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(frame1_.pos.y, composite2_.getFrameRect().pos.y, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(frame1_.width, composite2_.getFrameRect().width, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(frame1_.height, composite2_.getFrameRect().height, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(area1_, composite2_.getArea(), ERROR);
    BOOST_CHECK_EQUAL(composite1_.getSize(), composite2_.getSize());
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeMoveConstructor)
  {
    saltykov::Shape::pointer rectangle_ = std::make_shared<saltykov::Rectangle>(saltykov::point_t {2, 2}, 2, 3);
    saltykov::Shape::pointer circle_ = std::make_shared<saltykov::Circle>(saltykov::point_t {1, 1}, 1);
    saltykov::CompositeShape composite1_(rectangle_);
    composite1_.add(circle_);

    const saltykov::rectangle_t frame1_ = composite1_.getFrameRect();
    const double area_ = composite1_.getArea();
    const size_t size_ = composite1_.getSize();

    saltykov::CompositeShape composite2_(std::move(composite1_));

    BOOST_CHECK_CLOSE_FRACTION(frame1_.pos.x, composite2_.getFrameRect().pos.x, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(frame1_.pos.y, composite2_.getFrameRect().pos.y, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(frame1_.width, composite2_.getFrameRect().width, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(frame1_.height, composite2_.getFrameRect().height, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(area_, composite2_.getArea(), ERROR);
    BOOST_CHECK_EQUAL(size_, composite2_.getSize());
    BOOST_CHECK_CLOSE_FRACTION(composite1_.getArea(), 0, ERROR);
    BOOST_CHECK_EQUAL(composite1_.getSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeCopyOperator)
  {
    saltykov::Shape::pointer rectangle_ = std::make_shared<saltykov::Rectangle>(saltykov::point_t {2, 2}, 2, 3);
    saltykov::Shape::pointer circle_ = std::make_shared<saltykov::Circle>(saltykov::point_t {1, 1}, 1);
    saltykov::CompositeShape composite_(rectangle_);
    composite_.add(circle_);

    const saltykov::rectangle_t frame1_ = composite_.getFrameRect();
    const double area_ = composite_.getArea();

    saltykov::CompositeShape composite2_;
    composite2_ = composite_;

    BOOST_CHECK_CLOSE_FRACTION(frame1_.pos.x, composite2_.getFrameRect().pos.x, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(frame1_.pos.y, composite2_.getFrameRect().pos.y, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(frame1_.width, composite2_.getFrameRect().width, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(frame1_.height, composite2_.getFrameRect().height, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(area_, composite2_.getArea(), ERROR);
    BOOST_CHECK_EQUAL(composite_.getSize(), composite2_.getSize());
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeMoveOperator)
  {
    saltykov::Shape::pointer rectangle_ = std::make_shared<saltykov::Rectangle>(saltykov::point_t {2, 2}, 2, 3);
    saltykov::Shape::pointer circle_ = std::make_shared<saltykov::Circle>(saltykov::point_t {1, 1}, 1);
    saltykov::CompositeShape composite1_(rectangle_);
    composite1_.add(circle_);

    const saltykov::rectangle_t frame1_ = composite1_.getFrameRect();
    const double area_ = composite1_.getArea();
    const size_t size_ = composite1_.getSize();

    saltykov::CompositeShape composite2_;
    composite2_ = std::move(composite1_);

    BOOST_CHECK_CLOSE_FRACTION(frame1_.pos.x, composite2_.getFrameRect().pos.x, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(frame1_.pos.y, composite2_.getFrameRect().pos.y, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(frame1_.width, composite2_.getFrameRect().width, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(frame1_.height, composite2_.getFrameRect().height, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(area_, composite2_.getArea(), ERROR);
    BOOST_CHECK_EQUAL(size_, composite2_.getSize());
    BOOST_CHECK_CLOSE_FRACTION(composite1_.getArea(), 0, ERROR);
    BOOST_CHECK_EQUAL(composite1_.getSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeConstAfterMoving)
  {
    saltykov::CompositeShape composite_;
    saltykov::Shape::pointer circle_ = std::make_shared<saltykov::Circle>(saltykov::point_t {1, -1}, 2);
    saltykov::Shape::pointer rectangle_ = std::make_shared<saltykov::Rectangle>(saltykov::point_t {1, -1}, 2, 3);
    saltykov::Shape::pointer triangle_ = std::make_shared<saltykov::Triangle>(saltykov::Triangle {{1, -1}, {2, 2}, {3, -2}});
    saltykov::point_t pointArray[] {{4, 5}, {6, 4}, {7, -1}, {5, -3}, {2, 1}};
    saltykov::Shape::pointer polygon_ = std::make_shared<saltykov::Polygon>(saltykov::Polygon {5, pointArray});
    composite_.add(circle_);
    composite_.add(rectangle_);
    composite_.add(triangle_);
    composite_.add(polygon_);

    const saltykov::rectangle_t firstFrame = composite_.getFrameRect();
    const double firstArea = composite_.getArea();

    composite_.move({4, 5});
    saltykov::rectangle_t secondFrame = composite_.getFrameRect();
    double secondArea = composite_.getArea();

    BOOST_CHECK_CLOSE_FRACTION(firstFrame.width, secondFrame.width, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(firstFrame.height, secondFrame.height, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(firstArea, secondArea, ERROR);

    composite_.move(-2, 3);
    secondFrame = composite_.getFrameRect();
    secondArea = composite_.getArea();

    BOOST_CHECK_CLOSE_FRACTION(firstFrame.width, secondFrame.width, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(firstFrame.height, secondFrame.height, ERROR);
    BOOST_CHECK_CLOSE_FRACTION(firstArea, secondArea, ERROR);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeScaling)
  {
    saltykov::CompositeShape composite_;
    saltykov::Shape::pointer circle_ = std::make_shared<saltykov::Circle>(saltykov::point_t {1, -1}, 2);
    saltykov::Shape::pointer rectangle_ = std::make_shared<saltykov::Rectangle>(saltykov::point_t {1, -1}, 2, 3);
    saltykov::Shape::pointer triangle_ = std::make_shared<saltykov::Triangle>(saltykov::Triangle {{1, -1}, {2, 2}, {3, -2}});
    saltykov::point_t pointArray[] {{4, 5}, {6, 4}, {7, -1}, {5, -3}, {2, 1}};
    saltykov::Shape::pointer polygon_ = std::make_shared<saltykov::Polygon>(saltykov::Polygon {5, pointArray});
    composite_.add(circle_);
    composite_.add(rectangle_);
    composite_.add(triangle_);
    composite_.add(polygon_);

    const double firstArea = composite_.getArea();
    const double multiplier = 6;
    composite_.scale(multiplier);
    double secondArea = composite_.getArea();

    BOOST_CHECK_CLOSE_FRACTION(firstArea *  multiplier * multiplier, secondArea, ERROR);
  }

BOOST_AUTO_TEST_SUITE_END()
