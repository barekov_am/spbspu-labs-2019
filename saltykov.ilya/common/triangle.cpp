#include "triangle.hpp"

#include <stdexcept>
#include <cmath>
#include <iostream>

saltykov::Triangle::Triangle(const point_t& pointA, const point_t& pointB, const point_t& pointC) :
  centre_({(pointA.x + pointB.x + pointC.x) / 3, (pointA.y + pointB.y + pointC.y) / 3}),
//a + 2/3m = a + 2/3 ((b + c)/2-a) = a + 1/3(b + c) - 2/3a = 1/3(a + b + c)
  pointA_(pointA),
  pointB_(pointB),
  pointC_(pointC)
{
  if (getArea() < 0.000000001)
  {
    throw std::invalid_argument("Points should not lie on the same line");
  }
}

double saltykov::Triangle::getArea() const
{
  return std::abs((pointB_.x - pointA_.x) * (pointC_.y - pointA_.y)
      - (pointC_.x - pointA_.x) * (pointB_.y - pointA_.y)) / 2;
}

saltykov::rectangle_t saltykov::Triangle::getFrameRect() const
{
  double max_x = fmax(fmax(pointA_.x, pointB_.x), pointC_.x);
  double min_x = fmin(fmin(pointA_.x, pointB_.x), pointC_.x);
  double max_y = fmax(fmax(pointA_.y, pointB_.y), pointC_.y);
  double min_y = fmin(fmin(pointA_.y, pointB_.y), pointC_.y);
  return {max_x - min_x, max_y - min_y, {(max_x + min_x) / 2, (max_y + min_y) / 2}};
}

void saltykov::Triangle::move(const point_t& centre)
{
  move(centre.x - centre_.x, centre.y - centre_.y);
}

void saltykov::Triangle::move(double deltaX, double deltaY)
{
  centre_ = {centre_.x + deltaX, centre_.y + deltaY};
  pointA_ = {pointA_.x + deltaX, pointA_.y + deltaY};
  pointB_ = {pointB_.x + deltaX, pointB_.y + deltaY};
  pointC_ = {pointC_.x + deltaX, pointC_.y + deltaY};
}

void saltykov::Triangle::scale(double coefficient)
{
  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Triangle scale coefficient must be positive");
  }
  pointA_ = {centre_.x + coefficient * (pointA_.x - centre_.x),
    centre_.y + coefficient * (pointA_.y - centre_.y)};
  pointB_ = {centre_.x + coefficient * (pointB_.x - centre_.x),
    centre_.y + coefficient * (pointB_.y - centre_.y)};
  pointC_ = {centre_.x + coefficient * (pointC_.x - centre_.x),
    centre_.y + coefficient * (pointC_.y - centre_.y)};
  //increase the distance from the point to the center by m times
}

void saltykov::Triangle::show() const
{
  const rectangle_t frameRect = getFrameRect();
  std::cout << "Center coordinates (" << centre_.x << ","
    << centre_.y << ")" << '\n';
  std::cout << "Area: " << getArea() << '\n';
  std::cout << "Frame's height: " << frameRect.height << '\n';
  std::cout << "Frame's width: " << frameRect.width << '\n';
  std::cout << "Frame's centre coordinates: (" << frameRect.pos.x << ","
    << frameRect.pos.y << ")" << "\n\n";
}

void saltykov::Triangle::rotate(double degree)
{
  const double cos = std::cos((2 * M_PI * degree) / 360);
  const double sin = std::sin((2 * M_PI * degree) / 360);

  pointA_.x = centre_.x + (pointA_.x - centre_.x) * cos - (pointA_.y - centre_.y) * sin;
  pointA_.y = centre_.y + (pointA_.x - centre_.x) * sin + (pointA_.y - centre_.y) * cos;

  pointB_.x = centre_.x + (pointB_.x - centre_.x) * cos - (pointB_.y - centre_.y) * sin;
  pointB_.y = centre_.y + (pointB_.x - centre_.x) * sin + (pointB_.y - centre_.y) * cos;

  pointC_.x = centre_.x + (pointC_.x - centre_.x) * cos - (pointC_.y - centre_.y) * sin;
  pointC_.y = centre_.y + (pointC_.x - centre_.x) * sin + (pointC_.y - centre_.y) * cos;
}
