#include "Polygon.hpp"
#include <iostream>
#include <cmath>

saltykov::Polygon::Polygon(size_t numVertices, const saltykov::point_t* vertex):
  numVertices_(numVertices),
  vertex_(new point_t[numVertices_])
{
  try
  {
    if (numVertices_ <= 2)
    {
      throw std::invalid_argument("number of polygon's vertices can't be <=2");
    }

    if (vertex == nullptr)
    {
      throw std::invalid_argument("pointer to vertex is null");
    }

    if (!checkConvexity(numVertices, vertex))
    {
      throw std::invalid_argument("polygon must be convex");
    }
  }
  catch(...)
  {
    delete[] vertex_;
    throw;
  }

  for (size_t i = 0; i < numVertices_; i++)
  {
    vertex_[i] = vertex[i];
  }
}


saltykov::Polygon::Polygon(const Polygon& other):
  numVertices_(other.numVertices_),
  vertex_(new saltykov::point_t[numVertices_])
{
  for (size_t i = 0; i < numVertices_; i++)
  {
    vertex_[i] = other.vertex_[i];
  }
}

saltykov::Polygon::Polygon(Polygon&& other):
  numVertices_(other.numVertices_),
  vertex_(other.vertex_)
{
  other.numVertices_ = 0;
  other.vertex_ = nullptr;
}

saltykov::Polygon& saltykov::Polygon::operator =(const Polygon& other)
{
  if (this != &other)
  {
    delete[] vertex_;
    numVertices_ = other.numVertices_;
    vertex_ = new point_t[numVertices_];

    for (size_t index = 0; index < numVertices_; index++)
    {
      vertex_[index] = other.vertex_[index];
    }
  }
  return *this;
}

saltykov::Polygon& saltykov::Polygon::operator =(Polygon&& other)
{
  if (this != &other)
  {
    numVertices_ = other.numVertices_;
    other.numVertices_ = 0;

    if (vertex_ != nullptr)
    {
      delete[] vertex_;
    }

    vertex_ = other.vertex_;
    other.vertex_ = nullptr;
  }
  return *this;
}

saltykov::Polygon::~Polygon()
{
  delete[] vertex_;
}

saltykov::point_t saltykov::Polygon::getCentre() const
{
  point_t centre = {0, 0};
  for (size_t i = 0; i < numVertices_; i++)
  {
    centre.x += vertex_[i].x;
    centre.y += vertex_[i].y;
  }
  return {centre.x / numVertices_, centre.y / numVertices_};
}

void saltykov::Polygon::show() const
{
  for (size_t i = 0; i < numVertices_; i++)
  {
    std::cout << "vertex number:" << i << std::endl;
    std::cout << "(" << vertex_[i].x << ", " << vertex_[i].y << "); " << std::endl;

  }
  std::cout << std::endl << std::endl;
}

double saltykov::Polygon::getArea() const
{
  if (numVertices_ == 0)
  {
    return 0;
  }
  double area = 0;
  for (size_t i = 0; i < numVertices_ - 1; i++)
  {
    area += vertex_[i].x * vertex_[i + 1].y;
    area -= vertex_[i + 1].x * vertex_[i].y;
  }

  area += vertex_[numVertices_ - 1].x * vertex_[0].y;
  area -= vertex_[0].x * vertex_[numVertices_ - 1].y;

  return (0.5 * std::fabs(area));
}

saltykov::rectangle_t saltykov::Polygon::getFrameRect() const
{
  double maxX = vertex_[0].x;
  double minX = vertex_[0].x;
  double maxY = vertex_[0].y;
  double minY = vertex_[0].y;

  for (size_t i = 0; i < numVertices_; i++)
  {
    maxX = std::max(maxX, vertex_[i].x);
    minX = std::min(minX, vertex_[i].x);
    maxY = std::max(maxY, vertex_[i].y);
    minY = std::min(minY, vertex_[i].y);
  }

  const double width = (maxX - minX);
  const double height = (maxY - minY);
  const point_t posFrameRect = {minX + (width / 2), minY + (height / 2)};

  return {width, height, posFrameRect};
}

void saltykov::Polygon::move(double dx, double dy)
{
  for (size_t i = 0; i < numVertices_; i++)
  {
    vertex_[i].x += dx;
    vertex_[i].y += dy;
  }
}

void saltykov::Polygon::move(const point_t& newCentre)
{
  point_t oldCentre = getCentre();
  double dx = newCentre.x - oldCentre.x;
  double dy = newCentre.y - oldCentre.y;

  move(dx, dy);
}

void saltykov::Polygon::scale(double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("scale multiplier can't be <=0");
  }
  else
  {
    point_t centre = getCentre();
    for (size_t i = 0; i < numVertices_; i++)
    {
      vertex_[i].x = centre.x + multiplier * (vertex_[i].x - centre.x);
      vertex_[i].y = centre.y + multiplier * (vertex_[i].y - centre.y);
    }
  }
}

void saltykov::Polygon::rotate(double degree)
{
  const double cos = std::cos((2 * M_PI * degree) / 360);
  const double sin = std::sin((2 * M_PI * degree) / 360);

  const point_t centre = getCentre();

  for (std::size_t i = 0; i < numVertices_; i++)
  {
    vertex_[i].x = centre.x + (vertex_[i].x - centre.x) * cos - (vertex_[i].y - centre.y) * sin;
    vertex_[i].y = centre.y + (vertex_[i].x - centre.x) * sin + (vertex_[i].y - centre.y) * cos;
  }
}

bool saltykov::Polygon::checkConvexity(size_t numVertices, const point_t* vertex) const
{
  point_t previous = {0, 0};
  point_t next = {0, 0};
  for (size_t i = 0; i <= numVertices - 2; i++)
  {
    if (i == numVertices - 2)
    {
      previous.x = vertex[i + 1].x - vertex[i].x;
      previous.y = vertex[i + 1].y - vertex[i].y;
      next.x = vertex[0].x - vertex[i + 1].x;
      next.y = vertex[0].y - vertex[i + 1].y;
    }
    else
    {
      previous.x = vertex[i + 1].x - vertex[i].x;
      previous.y = vertex[i + 1].y - vertex[i].y;
      next.x = vertex[i + 2].x - vertex[i + 1].x;
      next.y = vertex[i + 2].y - vertex[i + 1].y;
    }
    if (((previous.x * next.y) - (previous.y * next.x)) > 0)
    {
      return false;
    }
  }
  return true;
}

size_t saltykov::Polygon::getSize() const
{
  return(numVertices_);
}
