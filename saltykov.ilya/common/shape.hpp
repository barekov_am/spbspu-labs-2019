#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"
#include <memory>

namespace saltykov
{
  class Shape
  {
  public:
    using pointer = std::shared_ptr<Shape>;
    using array = std::unique_ptr<pointer[]>;

    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t& centre) = 0;
    virtual void move(double deltaX, double deltaY) = 0;
    virtual void scale(double coefficient) = 0;
    virtual void show() const = 0;
    virtual void rotate(double degree) = 0;
  };
};

#endif // SHAPE_HPP
