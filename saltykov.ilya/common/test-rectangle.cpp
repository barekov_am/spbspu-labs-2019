#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "rectangle.hpp"

const double ERROR = 0.0000001;

BOOST_AUTO_TEST_SUITE(TestsForRectangle)

BOOST_AUTO_TEST_CASE(RectangleTestForArea)
{
  double scale_factor = 2.67;
  saltykov::Rectangle test_rectangle({0, 0}, 2, 10);
  double test_area = test_rectangle.getArea();
  test_rectangle.scale(scale_factor);
  double test_area_after_scale = test_rectangle.getArea();
  BOOST_CHECK_CLOSE_FRACTION(scale_factor * scale_factor * test_area, test_area_after_scale, ERROR);
}

BOOST_AUTO_TEST_CASE(RectangleInvalidScaleFactor)
{
  saltykov::Rectangle test_rectangle({2, 2}, 3, 3);
  BOOST_CHECK_THROW(test_rectangle.scale(-2.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(RectangleInvalidWidth)
{
  BOOST_CHECK_THROW(saltykov::Rectangle test_rectangle({2, 5}, -2, 3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(RectangleInvalidHeight)
{
  BOOST_CHECK_THROW(saltykov::Rectangle test_rectangle({2, 5}, 2, -3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(RectangleInvalidHeightAndWidth)
{
  BOOST_CHECK_THROW(saltykov::Rectangle test_rectangle({2, 5}, -2, -3), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
