#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "Polygon.hpp"

const double ERROR = 0.0000001;

BOOST_AUTO_TEST_SUITE(TestsForPolygon)

BOOST_AUTO_TEST_CASE(PolygonTestForFrameRectangle)
{
  saltykov::point_t points[4];
  points[0] = {0, 0};
  points[1] = {0, 3};
  points[2] = {1, 4};
  points[3] = {2, 0};
  saltykov::Polygon test_polygon(4, points);
  saltykov::rectangle_t test_frame_rect = test_polygon.getFrameRect();
  test_polygon.move(2, 0);
  saltykov::rectangle_t test_frame_rect_after_moving = test_polygon.getFrameRect();
  BOOST_CHECK_CLOSE_FRACTION(test_frame_rect.width, test_frame_rect_after_moving.width, ERROR);
  BOOST_CHECK_CLOSE_FRACTION(test_frame_rect.height, test_frame_rect_after_moving.height, ERROR);
}

BOOST_AUTO_TEST_CASE(PolygonTestForArea)
{
  double scale_factor = 3;
  saltykov::point_t points[4];
  points[0] = {0, 0};
  points[1] = {0, 3};
  points[2] = {1, 4};
  points[3] = {2, 0};
  saltykov::Polygon test_polygon(4, points);
  double test_area = test_polygon.getArea();
  test_polygon.scale(scale_factor);
  double test_area_after_scale = test_polygon.getArea();
  BOOST_CHECK_CLOSE_FRACTION(scale_factor * scale_factor * test_area, test_area_after_scale, ERROR);
}

BOOST_AUTO_TEST_CASE(PolygonInvalidCountOfPoints)
{
  saltykov::point_t points[2];
  points[0] = {0, 0};
  points[1] = {0, 3};
  BOOST_CHECK_THROW(saltykov::Polygon test_polygon(2, points), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(PolygonInvalidScaleFactor)
{
  saltykov::point_t points[4];
  points[0] = {0, 0};
  points[1] = {0, 3};
  points[2] = {1, 4};
  points[3] = {2, 0};
  saltykov::Polygon test_polygon(4, points);
  BOOST_CHECK_THROW(test_polygon.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(PolygonNonConvex)
{
  saltykov::point_t points[4];
  points[0] = {0, 0};
  points[1] = {0, 3};
  points[2] = {1, 4};
  points[3] = {1, 6};
  BOOST_CHECK_THROW(saltykov::Polygon test_polygon(4, points), std::logic_error);
}

BOOST_AUTO_TEST_SUITE_END()
