#ifndef A3_PARTITION_HPP
#define A3_PARTITION_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace saltykov
{
  saltykov::Matrix part(const saltykov::CompositeShape& composition);
}

#endif // PARTITION_HPP
