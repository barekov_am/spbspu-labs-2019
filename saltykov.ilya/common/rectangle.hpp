#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"
#include "base-types.hpp"

namespace saltykov
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t& pos, double width, double height);
    Rectangle(double x, double y, double width, double height);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& centre) override;
    void move(double deltaX, double deltaY) override;
    void scale(double coefficient) override;
    void show() const override;
    void rotate(double degree) override;

  private:
    point_t centre_;
    double width_;
    double height_;
    double rotationDegree_;
  };
};

#endif // RECTANGLE_HPP
