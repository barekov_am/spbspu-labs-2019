#include "composite-shape.hpp"
#include <iostream>
#include <cmath>

saltykov::CompositeShape::CompositeShape() :
  numShapes_(0),
  shapes_(nullptr)
{ }

saltykov::CompositeShape::CompositeShape(const Shape::pointer& shape):
  CompositeShape()
{
  add(shape);
}

saltykov::CompositeShape::CompositeShape(const CompositeShape& newShape):
  numShapes_(newShape.numShapes_),
  shapes_(std::make_unique<saltykov::Shape::pointer[]>(newShape.numShapes_))
{
  for (size_t i = 0; i < numShapes_; i++)
  {
    shapes_[i] = newShape.shapes_[i];
  }
}

saltykov::CompositeShape::CompositeShape(CompositeShape&& newShape) :
  numShapes_(newShape.numShapes_),
  shapes_(std::move(newShape.shapes_))
{
  newShape.numShapes_ = 0;
  newShape.shapes_ = nullptr;
}

saltykov::CompositeShape& saltykov::CompositeShape::operator =(const CompositeShape& newShape)
{
  if (this != &newShape)
  {
    shapes_ = std::make_unique<saltykov::Shape::pointer[]>(newShape.numShapes_);
    numShapes_ = newShape.numShapes_;
    for (size_t i = 0; i < numShapes_; i++)
    {
      shapes_[i] = newShape.shapes_[i];
    }
  }
  return *this;
}

saltykov::CompositeShape& saltykov::CompositeShape::operator =(CompositeShape&& newShape)
{
  if (this != &newShape)
  {
    numShapes_ = newShape.numShapes_;
    shapes_ = std::move(newShape.shapes_);
    newShape.numShapes_ = 0;
    newShape.shapes_ = nullptr;
  }
  return *this;
}

saltykov::Shape::pointer saltykov::CompositeShape::operator [](size_t index) const
{
  if (index >= numShapes_)
  {
    throw std::out_of_range("index is out of range");
  }

  return shapes_[index];
}

double saltykov::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < numShapes_; i++)
  {
    area += shapes_[i]->getArea();
  }

  return area;
}

saltykov::rectangle_t saltykov::CompositeShape::getFrameRect() const
{
  if (numShapes_ == 0)
  {
    return rectangle_t{0, 0, point_t{0, 0}};
  }

  rectangle_t tempRect = shapes_[0]->getFrameRect();

  double minX = tempRect.pos.x - tempRect.width / 2;
  double maxX = tempRect.pos.x + tempRect.width / 2;
  double minY = tempRect.pos.y - tempRect.height / 2;
  double maxY = tempRect.pos.y + tempRect.height / 2;

  for (size_t i = 1; i < numShapes_; i++)
  {
    tempRect = shapes_[i]->getFrameRect();

    minX = std::fmin(minX, tempRect.pos.x - tempRect.width / 2);
    maxX = std::fmax(maxX, tempRect.pos.x + tempRect.width / 2);
    minY = std::fmin(minY, tempRect.pos.y - tempRect.height / 2);
    maxY = std::fmax(maxY, tempRect.pos.y + tempRect.height / 2);
  }

  return rectangle_t{maxX - minX, maxY - minY, point_t{(maxX + minX) / 2, (maxY + minY) / 2}};
}

void saltykov::CompositeShape::move(double dx, double dy)
{
  if (numShapes_ != 0)
  {
    for (size_t i = 0; i < numShapes_; i++)
    {
      shapes_[i]->move(dx, dy);
    }
  }
}

void saltykov::CompositeShape::move(const point_t& newCentre)
{
  rectangle_t frameRect = getFrameRect();
  double dx = newCentre.x - frameRect.pos.x;
  double dy = newCentre.y - frameRect.pos.y;
  move(dx, dy);
}

void saltykov::CompositeShape::scale(double multiplier)
{
  if (multiplier <= 0)
  {
    throw std::invalid_argument("scale multiplier can't be <=0");
  }

  if (numShapes_ != 0)
  {
    rectangle_t frameRect = getFrameRect();

    for (size_t i = 0; i < numShapes_; i++)
    {
      rectangle_t currentFrameRect = shapes_[i]->getFrameRect();
      double dx = (currentFrameRect.pos.x - frameRect.pos.x) * (multiplier - 1);
      double dy = (currentFrameRect.pos.y - frameRect.pos.y) * (multiplier - 1);
      shapes_[i]->move(dx, dy);
      shapes_[i]->scale(multiplier);
    }
  }
}

void saltykov::CompositeShape::rotate(double degree)
{
  const double cos = std::cos((2 * M_PI * degree) / 360);
  const double sin = std::sin((2 * M_PI * degree) / 360);

  const point_t compCentre = getFrameRect().pos;

  for (std::size_t i = 0; i < numShapes_; i++)
  {
    const point_t shapeCentre = shapes_[i]->getFrameRect().pos;
    const double dx = (shapeCentre.x - compCentre.x) * cos - (shapeCentre.y - compCentre.y) * sin;
    const double dy = (shapeCentre.x - compCentre.x) * sin + (shapeCentre.y - compCentre.y) * cos;

    shapes_[i]->move({ compCentre.x + dx, compCentre.y + dy });
    shapes_[i]->rotate(degree);
  }
}

size_t saltykov::CompositeShape::getSize() const
{
  return(numShapes_);
}

void saltykov::CompositeShape::add(const Shape::pointer& shape)
{
  Shape::array shapes = std::make_unique<Shape::pointer[]>(numShapes_ + 1);

  for (size_t i = 0; i < numShapes_; i++)
  {
    shapes[i] = shapes_[i];
  }

  shapes[numShapes_] = shape;
  numShapes_++;
  shapes_ = std::move(shapes);
}

void saltykov::CompositeShape::remove(size_t index)
{
  if (index >= numShapes_)
  {
    throw std::logic_error("deleted shape number is > than quantity of shapes in composite shape");
  }

  for (size_t i = index; i < (numShapes_ - 1); i++)
  {
    shapes_[i] = shapes_[i + 1];
  }

  shapes_[--numShapes_] = nullptr;
}

void saltykov::CompositeShape::show() const
{
  const rectangle_t frameRect = getFrameRect();
  std::cout << "Area: " << getArea() << '\n';
  std::cout << "Frame's height: " << frameRect.height << '\n';
  std::cout << "Frame's width: " << frameRect.width << '\n';
  std::cout << "Frame's centre coordinates: (" << frameRect.pos.x << ","
    << frameRect.pos.y << ")" << "\n\n";
}
