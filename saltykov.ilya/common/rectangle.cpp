#include "rectangle.hpp"

#include <stdexcept>
#include <iostream>
#include <cmath>

saltykov::Rectangle::Rectangle(const point_t& pos, double width, double height) :
  centre_(pos),
  width_(width),
  height_(height),
  rotationDegree_(0)
{
  if ((width_ <= 0.0) || (height_ <= 0.0))
  {
    throw std::invalid_argument("Sides of rectangle must be positive");
  }
}

saltykov::Rectangle::Rectangle(double x, double y, double width, double height) :
  Rectangle({x, y}, width, height)
{}

double saltykov::Rectangle::getArea() const
{
  return width_ * height_;
}

saltykov::rectangle_t saltykov::Rectangle::getFrameRect() const
{
  const double cos = std::cos((2 * M_PI * rotationDegree_) / 360);
  const double sin = std::sin((2 * M_PI * rotationDegree_) / 360);
  const double width = width_ * std::fabs(cos) + height_ * std::fabs(sin);
  const double height = height_ * std::fabs(cos) + width_ * std::fabs(sin);
  return {width, height, centre_};
}


void saltykov::Rectangle::move(const point_t& centre)
{
  centre_ = centre;
}

void saltykov::Rectangle::move(double deltaX, double deltaY)
{
  centre_.x += deltaX;
  centre_.y += deltaY;
}

void saltykov::Rectangle::scale(double coefficient)
{
  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Rectangle scale coefficient must be positive");
  }
  width_ *= coefficient;
  height_ *= coefficient;
}

void saltykov::Rectangle::show() const
{
  const rectangle_t frameRect = getFrameRect();
  std::cout << "Center coordinates (" << centre_.x << ","
    << centre_.y << ")" << '\n';
  std::cout << "Area: " << getArea() << '\n';
  std::cout << "Frame's height: " << frameRect.height << '\n';
  std::cout << "Frame's width: " << frameRect.width << '\n';
  std::cout << "Degree = " << rotationDegree_ << '\n';
  std::cout << "Frame's centre coordinates: (" << frameRect.pos.x << ","
    << frameRect.pos.y << ")" << "\n\n";
}

void saltykov::Rectangle::rotate(double degree) {
  rotationDegree_ += degree;

  while (std::abs(rotationDegree_) >= 360)
  {
    rotationDegree_ = (degree > 0) ? rotationDegree_ - 360 : rotationDegree_ + 360;
  }
}

