#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "circle.hpp"

const double ERROR = 0.0000001;

BOOST_AUTO_TEST_SUITE(TestsForCircle)

BOOST_AUTO_TEST_CASE(CircleTestForFrameRectangle)
{
  saltykov::Circle test_circle({4.35, 5.35}, 2);
  saltykov::rectangle_t test_frame_rect = test_circle.getFrameRect();
  test_circle.move(2.0, 1.0);
  saltykov::rectangle_t test_frame_rect_after_moving = test_circle.getFrameRect();
  BOOST_CHECK_CLOSE_FRACTION(test_frame_rect.width, test_frame_rect_after_moving.width, ERROR);
  BOOST_CHECK_CLOSE_FRACTION(test_frame_rect.height, test_frame_rect_after_moving.height, ERROR);
}

BOOST_AUTO_TEST_CASE(CircleTestForArea)
{
  double scale_factor = 1.5;
  saltykov::Circle test_circle({4.35, 5.35}, 2);
  double test_area = test_circle.getArea();
  test_circle.scale(scale_factor);
  double test_area_after_scale = test_circle.getArea();
  BOOST_CHECK_CLOSE_FRACTION(scale_factor * scale_factor * test_area, test_area_after_scale, ERROR);
}

BOOST_AUTO_TEST_CASE(CircleInvalidScaleFactor)
{
  saltykov::Circle test_circle({5, 5}, 2);
  BOOST_CHECK_THROW(test_circle.scale(-3.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(CircleInvalidRadius)
{
  BOOST_CHECK_THROW(saltykov::Circle test_circle({5, 5}, -2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
