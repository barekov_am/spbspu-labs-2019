#include <iostream>
#include <vector>
#include <algorithm>
#include "data-struct.hpp"
#include <iterator>

int main()
{
  std::vector<DataStruct> vec((std::istream_iterator<DataStruct>(std::cin)), std::istream_iterator<DataStruct>());

  if (!std::cin.eof() && std::cin.fail())
  {
    std::cerr << "Error while reading data\n";
    return 2;
  }

  std::sort(vec.begin(), vec.end(), [](const DataStruct& lhs, const DataStruct& rhs)
  {
    if (lhs.key1 != rhs.key1)
    {
      return lhs.key1 < rhs.key1;
    }
    if (lhs.key2 != rhs.key2)
    {
      return lhs.key2 < rhs.key2;
    }
    return lhs.str.length() < rhs.str.length();
  });

  std::copy(vec.begin(), vec.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));
}
