#include "data-struct.hpp"
#include <stdexcept>
#include "blank.hpp"

const int MAX_ABS = 5;

int getKey(std::istream& is)
{
  if ((is >> blank).peek() == '\n')
  {
    throw std::ios_base::failure("No key.");
  }

  int key;
  if (!(is >> key))
  {
    throw std::ios_base::failure("No key.");
  }


  if ((is >> blank).get() != ',')
  {
    throw std::ios_base::failure("No delimiter");
  }

  if (std::abs(key) > MAX_ABS)
  {
    throw std::ios_base::failure("Invalid key");
  }

  return key;
}

std::istream& operator>>(std::istream& is, DataStruct& data)
{
  std::istream::sentry sentry(is);

  if (sentry)
  {
    int key1;
    int key2;
    try
    {
      key1 = getKey(is);
      key2 = getKey(is);
    }
    catch (std::ios_base::failure& e)
    {
      is.clear(std::ios::failbit);
      return is;
    }

    std::string str;
    getline((is >> blank), str);

    if (str.empty())
    {
      is.clear(std::ios::failbit);
      return is;
    }

    data = {key1, key2, str};
  }

  return is;
}

std::ostream& operator<<(std::ostream& os, const DataStruct& data)
{
  os << data.key1 << ", " << data.key2 << ", " << data.str;
  return os;
}
