#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP

#include <string>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

std::istream& operator>>(std::istream& is, DataStruct& data);
std::ostream& operator<<(std::ostream& os, const DataStruct& data);

#endif // DATASTRUCT_HPP
