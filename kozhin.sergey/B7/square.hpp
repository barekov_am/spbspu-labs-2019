#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "shape.hpp"

class Square : public Shape
{
public:
  Square(const Point& center);
  void draw(std::ostream& os) const override;
};

#endif // SQUARE_HPP
