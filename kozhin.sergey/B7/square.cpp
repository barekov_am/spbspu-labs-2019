#include "square.hpp"
#include <iostream>

Square::Square(const Point& center) :
  Shape(center)
{
}

void Square::draw(std::ostream& os) const
{
  os << "SQUARE " << center_;
}
