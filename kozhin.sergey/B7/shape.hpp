#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "point.hpp"

class Shape
{
public:
  typedef std::shared_ptr<Shape> ptr;
  typedef std::shared_ptr<const Shape> const_ptr;

  Shape(const Point& center);
  virtual ~Shape() = default;
  bool isMoreLeft(const Shape& rhs) const;
  bool isUpper(const Shape& rhs) const;
  virtual void draw(std::ostream& os) const = 0;

protected:
  Point center_;
};

#endif // SHAPE_HPP
