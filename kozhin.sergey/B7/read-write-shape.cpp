#include <iostream>
#include <algorithm>
#include "blank.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "square.hpp"

std::istream& operator>>(std::istream& is, Shape::ptr& shape)
{
  std::istream::sentry sentry(is);

  if (sentry)
  {
    const static struct
    {
      const char* name;
      Shape::ptr (*create)(const Point& center);
    } shapes[] = {
        {"CIRCLE", [](const Point& center) { return Shape::ptr(new Circle(center)); }},
        {"TRIANGLE", [](const Point& center) { return Shape::ptr(new Triangle(center)); }},
        {"SQUARE", [](const Point& center) { return Shape::ptr(new Square(center)); }}};

    std::string shapeName;
    while (std::isalpha(is.peek()))
    {
      shapeName += is.get();
    }

    // auto - pointer to anonymous struct
    auto foundShape =
        std::find_if(std::begin(shapes), std::end(shapes), [&](const auto& el) { return shapeName == el.name; });

    if (foundShape == std::end(shapes))
    {
      is.clear(std::ios::failbit);
      return is;
    }

    if ((is >> blank).peek() == '\n')
    {
      is.clear(std::ios::failbit);
      return is;
    }

    Point center;
    if (!(is >> center))
    {
      is.clear(std::ios::failbit);
      return is;
    }

    shape = foundShape->create(center);
  }

  return is;
}

std::ostream& operator<<(std::ostream& os, const Shape& shape)
{
  shape.draw(os);
  return os;
}

std::ostream& operator<<(std::ostream& os, const Shape::const_ptr& shape)
{
  if (shape)
  {
    os << *shape;
  }

  return os;
}
