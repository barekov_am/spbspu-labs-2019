#include "shape.hpp"

Shape::Shape(const Point& center) :
  center_(center)
{
}

bool Shape::isMoreLeft(const Shape& rhs) const
{
  return center_.x < rhs.center_.x;
}

bool Shape::isUpper(const Shape& rhs) const
{
  return center_.y > rhs.center_.y;
}
