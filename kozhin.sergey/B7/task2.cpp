#include <iostream>
#include <list>
#include <iterator>
#include <algorithm>
#include <stdexcept>
#include "shape.hpp"

typedef std::istream_iterator<Shape::ptr> shapeIstreamIterator;
typedef std::ostream_iterator<Shape::const_ptr> shapeOstreamIterator;

std::istream& operator>>(std::istream& is, Shape::ptr& shape);
std::ostream& operator<<(std::ostream& os, const Shape::const_ptr& shape);

void task2()
{
  std::list<Shape::const_ptr> shapes((shapeIstreamIterator(std::cin)), shapeIstreamIterator());

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Error while reading shape data");
  }

  std::cout << "Original:\n";
  std::copy(shapes.begin(), shapes.end(), shapeOstreamIterator(std::cout, "\n"));

  shapes.sort([](const Shape::const_ptr& lhs, const Shape::const_ptr& rhs) { return lhs->isMoreLeft(*rhs); });
  std::cout << "Left-Right:\n";
  std::copy(shapes.begin(), shapes.end(), shapeOstreamIterator(std::cout, "\n"));

  shapes.reverse();
  std::cout << "Right-Left:\n";
  std::copy(shapes.begin(), shapes.end(), shapeOstreamIterator(std::cout, "\n"));

  shapes.sort([](const Shape::const_ptr& lhs, const Shape::const_ptr& rhs) { return lhs->isUpper(*rhs); });
  std::cout << "Top-Bottom:\n";
  std::copy(shapes.begin(), shapes.end(), shapeOstreamIterator(std::cout, "\n"));

  shapes.reverse();
  std::cout << "Bottom-Top:\n";
  std::copy(shapes.begin(), shapes.end(), shapeOstreamIterator(std::cout, "\n"));
}
