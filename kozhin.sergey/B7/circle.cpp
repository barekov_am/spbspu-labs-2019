#include "circle.hpp"
#include <iostream>

Circle::Circle(const Point& center) :
  Shape(center)
{
}

void Circle::draw(std::ostream& os) const
{
  os << "CIRCLE " << center_;
}
