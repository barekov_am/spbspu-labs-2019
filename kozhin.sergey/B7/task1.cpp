#include <iostream>
#include <iterator>
#include <algorithm>
#include <cmath>
#include <stdexcept>

void task1()
{
  std::transform(std::istream_iterator<float>(std::cin), std::istream_iterator<float>(),
      std::ostream_iterator<float>(std::cout, "\n"), std::bind1st(std::multiplies<float>(), M_PI));

  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::ios_base::failure("Error while reading float");
  }
}
