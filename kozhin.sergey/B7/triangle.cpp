#include "triangle.hpp"
#include <iostream>

Triangle::Triangle(const Point& center) :
  Shape(center)
{
}

void Triangle::draw(std::ostream& os) const
{
  os << "TRIANGLE " << center_;
}
