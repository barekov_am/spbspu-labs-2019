#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

class Triangle : public Shape
{
public:
  Triangle(const Point& center);
  void draw(std::ostream& os) const override;
};

#endif // TRIANGLE_HPP
