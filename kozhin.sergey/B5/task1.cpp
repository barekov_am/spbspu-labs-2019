#include <iostream>
#include <unordered_set>
#include <iterator>

void task1()
{
  std::unordered_set<std::string> set(
      (std::istream_iterator<std::string>(std::cin)), std::istream_iterator<std::string>());

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Error while reading strings");
  }

  std::copy(set.begin(), set.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
}
