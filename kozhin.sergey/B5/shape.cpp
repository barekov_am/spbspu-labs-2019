#include "shape.hpp"
#include <iostream>
#include <stdexcept>
#include <iterator>
#include "blank.hpp"

const int TRIANGLE_VERTICES = 3;
const int RECTANGLE_VERTICES = 4;
const int PENTAGON_VERTICES = 5;

std::istream& operator>>(std::istream& is, Shape& shape)
{
  std::istream::sentry sentry(is);

  if (sentry)
  {
    std::string sizeStr;
    is >> sizeStr;

    char* end;
    long size = std::strtol(sizeStr.c_str(), &end, 10);
    if (sizeStr.empty() || *end != '\0')
    {
      is.clear(std::ios::failbit);
      return is;
    }

    if (size < 1)
    {
      is.clear(std::ios::failbit);
      return is;
    }

    Shape tmpShape(size);
    for (long i = 0; i < size; ++i)
    {
      if ((is >> blank).peek() == '\n')
      {
        is.clear(std::ios_base::failbit);
        return is;
      }
      Point point;
      if (!(is >> point))
      {
        is.clear(std::ios::failbit);
        return is;
      }
      tmpShape[i] = point;
    }

    shape = tmpShape;
  }

  return is;
}

std::ostream& operator<<(std::ostream& os, const Shape& shape)
{
  os << shape.size() << ' ';
  std::copy(shape.begin(), shape.end(), std::ostream_iterator<Point>(os, " "));

  return os;
}

bool isOrthogonal(const Point& p1, const Point& p2, const Point& p3)
{
  return (p2.x - p1.x) * (p2.x - p3.x) + (p2.y - p1.y) * (p2.y - p3.y) == 0;
}

int distSqr(const Point& p1, const Point& p2)
{
  return (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
}

bool isRectangle(const Shape& shape)
{
  return shape.size() == RECTANGLE_VERTICES && isOrthogonal(shape[0], shape[1], shape[2])
         && isOrthogonal(shape[1], shape[2], shape[3]) && isOrthogonal(shape[2], shape[3], shape[0]);
}

ShapeType getType(const Shape& shape)
{
  if (shape.size() == TRIANGLE_VERTICES)
  {
    return ShapeType::TRIANGLE;
  }

  if (shape.size() == PENTAGON_VERTICES)
  {
    return ShapeType::PENTAGON;
  }

  if (isRectangle(shape))
  {
    if ((distSqr(shape[0], shape[1]) == distSqr(shape[1], shape[2])))
    {
      return ShapeType::SQUARE;
    }

    return ShapeType::RECTANGLE;
  }

  return ShapeType::OTHER;
}
