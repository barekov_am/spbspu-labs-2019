#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <vector>
#include <iostream>
#include "point.hpp"

enum class ShapeType
{
  TRIANGLE,
  SQUARE,
  RECTANGLE,
  PENTAGON,
  OTHER
};

typedef std::vector<Point> Shape;

std::istream& operator>>(std::istream& is, Shape& shape);
std::ostream& operator<<(std::ostream& os, const Shape& shape);

ShapeType getType(const Shape& shape);

#endif // SHAPE_HPP
