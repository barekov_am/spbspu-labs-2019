#include <list>
#include <stdexcept>
#include <iterator>
#include <algorithm>
#include <numeric>
#include "shape.hpp"

void task2()
{
  std::locale locale("");
  std::cin.imbue(locale);
  std::cout.imbue(locale);

  // Reading Shapes
  std::list<Shape> shapes((std::istream_iterator<Shape>(std::cin)), std::istream_iterator<Shape>());
  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::ios_base::failure("Incorrect shape data");
  }

  // Counting vertices
  int numberOfVeritcies =
      std::accumulate(shapes.begin(), shapes.end(), 0, [](int sum, const Shape& el) { return sum += el.size(); });

  // Counting different shapes
  int numberOfTriangles = 0;
  int numberOfRectangles = 0;
  int numberOfSquares = 0;
  std::for_each(shapes.begin(), shapes.end(), [&](const Shape& el)
  {
    if (getType(el) == ShapeType::TRIANGLE)
    {
      ++numberOfTriangles;
    }
    else if (getType(el) == ShapeType::SQUARE)
    {
      ++numberOfSquares;
      ++numberOfRectangles;
    }
    else if (getType(el) == ShapeType::RECTANGLE)
    {
      ++numberOfRectangles;
    }
  });

  // Removing pentagons
  shapes.remove_if([](const Shape& el) { return getType(el) == ShapeType::PENTAGON; });

  // Creating vector of points
  std::vector<Point> points(shapes.size());
  std::transform(shapes.begin(), shapes.end(), points.begin(), [](const Shape& el) { return el[0]; });

  // Rearranging shapes
  shapes.sort([](const Shape& lhs, const Shape& rhs) { return getType(lhs) < getType(rhs); });

  // Printing results
  std::cout << "Vertices: " << numberOfVeritcies << '\n';
  std::cout << "Triangles: " << numberOfTriangles << '\n';
  std::cout << "Squares: " << numberOfSquares << '\n';
  std::cout << "Rectangles: " << numberOfRectangles << '\n';
  std ::cout << "Points: ";
  std::copy(points.begin(), points.end(), std::ostream_iterator<Point>(std::cout, " "));
  std::cout << '\n';
  std ::cout << "Shapes: " << '\n';
  std::copy(shapes.begin(), shapes.end(), std::ostream_iterator<Shape>(std::cout, "\n"));
}
