#include "iostream"

void task1();
void task2();

int main(int argc, char* argv[])
{
  if (argc != 2)
  {
    std::cerr << "No task number.\n";
    return 1;
  }

  try
  {
    int taskNum = atoi(argv[1]);
    if (taskNum == 1)
    {
      task1();
    }
    else if (taskNum == 2)
    {
      task2();
    }
    else
    {
      std::cerr << "Incorrect task umber.\n";
      return 1;
    }
  }
  catch (std::exception& e)
  {
    std::cerr << e.what() << '\n';
    return 2;
  }

  return 0;
}
