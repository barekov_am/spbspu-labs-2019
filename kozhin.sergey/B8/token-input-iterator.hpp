#ifndef TOKENINPUTITERATOR_HPP
#define TOKENINPUTITERATOR_HPP

#include <iterator>
#include "token.hpp"

class TokenInputIterator : public std::iterator<std::input_iterator_tag, Token>
{
public:
  TokenInputIterator();
  explicit TokenInputIterator(std::istream& is);

  bool operator==(const TokenInputIterator& rhs) const noexcept;
  bool operator!=(const TokenInputIterator& rhs) const noexcept;

  const Token& operator*() const;
  const Token* operator->() const;

  TokenInputIterator& operator++();
  TokenInputIterator operator++(int);

private:
  std::istream* is_;
  Token currToken_;
  Token prevToken_;
  bool isOk_;
  bool firstTime;
};

#endif // TOKENINPUTITERATOR_HPP
