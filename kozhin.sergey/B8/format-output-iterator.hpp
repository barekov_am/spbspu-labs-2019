#ifndef FORMATOUTPUTITERATOR_HPP
#define FORMATOUTPUTITERATOR_HPP

#include <iterator>
#include <memory>
#include "token.hpp"

class FormatOutputIterator : public std::iterator<std::output_iterator_tag, Token>
{
public:
  FormatOutputIterator(std::ostream& os, std::size_t lineWidth);

  FormatOutputIterator& operator*();
  void operator=(const Token& token);

  void operator++();
  void operator++(int);

private:
  class Impl;
  std::shared_ptr<Impl> impl;
};

#endif // FORMATOUTPUTITERATOR_HPP
