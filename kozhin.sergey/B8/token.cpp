#include "token.hpp"

bool Token::isPunctuation() const
{
  return type == Type::PUNCTUATION || type == Type::DASH;
}
