#ifndef TOKEN_HPP
#define TOKEN_HPP

#include <string>

struct Token
{
  enum class Type
  {
    WORD,
    PUNCTUATION,
    DASH,
    NUMBER,
    INVALID
  };

  Type type;
  std::string data;

  bool isPunctuation() const;
};

#endif // TOKEN_HPP
