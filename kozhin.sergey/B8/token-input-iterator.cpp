#include "token-input-iterator.hpp"
#include <iostream>
#include <stdexcept>

const int MAX_WORD_LENGTH = 20;
const int MAX_NUM_LENGTH = 20;
const std::locale LOCALE = std::locale("");

void continueReadingWord(std::istream& is, std::string& buff);
void continueReadingNumber(std::istream& is, std::string& buff);

TokenInputIterator::TokenInputIterator() :
  is_(nullptr),
  currToken_({Token::Type::INVALID, ""}),
  prevToken_(currToken_),
  isOk_(false),
  firstTime(true)
{
}

TokenInputIterator::TokenInputIterator(std::istream& is) :
  is_(&is),
  currToken_({Token::Type::INVALID, ""}),
  prevToken_(currToken_),
  isOk_(true),
  firstTime(true)
{
  operator++();
  firstTime = false;
}

bool TokenInputIterator::operator==(const TokenInputIterator& rhs) const noexcept
{
  return (isOk_ == rhs.isOk_) && (!isOk_ || is_ == rhs.is_);
}

bool TokenInputIterator::operator!=(const TokenInputIterator& rhs) const noexcept
{
  return !(*this == rhs);
}

const Token& TokenInputIterator::operator*() const
{
  if (currToken_.type == Token::Type::INVALID)
  {
    throw std::ios_base::failure("Invalid token.");
  }
  return currToken_;
}

const Token* TokenInputIterator::operator->() const
{
  if (currToken_.type == Token::Type::INVALID)
  {
    throw std::ios_base::failure("Invalid token.");
  }
  return &currToken_;
}

TokenInputIterator& TokenInputIterator::operator++()
{
  std::istream::sentry sentry(*is_);

  if (sentry)
  {
    prevToken_ = currToken_;
    currToken_ = {Token::Type::INVALID, ""};

    std::string buff;
    buff += is_->get();

    if (buff.back() == '-' || buff.back() == '+')
    {
      buff += is_->get();
      if (std::isdigit(buff.back(), LOCALE))
      {
        continueReadingNumber(*is_, buff);
        if (buff.size() > MAX_NUM_LENGTH)
        {
          return *this;
        }
        currToken_ = {Token::Type::NUMBER, buff};
        return *this;
      }

      if (buff[0] == '-' && buff.back() == '-')
      {
        if ((prevToken_.isPunctuation() && prevToken_.data != ",") || is_->peek() != '-' || firstTime)
        {
          return *this;
        }
        buff += is_->get();
        currToken_ = {Token::Type::DASH, buff};
        return *this;
      }

      return *this;
    }

    if (std::isdigit(buff.back(), LOCALE))
    {
      continueReadingNumber(*is_, buff);
      if (buff.size() > MAX_NUM_LENGTH)
      {
        return *this;
      }
      currToken_ = {Token::Type::NUMBER, buff};
      return *this;
    }

    if (std::ispunct(buff.back(), LOCALE))
    {
      if (firstTime || prevToken_.isPunctuation())
      {
        return *this;
      }
      currToken_ = {Token::Type::PUNCTUATION, buff};
      return *this;
    }

    if (std::isalpha(buff.back(), LOCALE))
    {
      continueReadingWord(*is_, buff);
      if (buff.size() > MAX_WORD_LENGTH)
      {
        return *this;
      }
      currToken_ = {Token::Type::WORD, buff};
      return *this;
    }
  }

  isOk_ = false;
  return *this;
}

TokenInputIterator TokenInputIterator::operator++(int)
{
  TokenInputIterator tmp = *this;
  operator++();
  return tmp;
}

void continueReadingWord(std::istream& is, std::string& buff)
{
  char nextChar = is.peek();
  while (std::isalpha(nextChar, LOCALE) || (buff.back() != '-' && nextChar == '-'))
  {
    is.get();
    buff += nextChar;
    nextChar = is.peek();
  }
}

void continueReadingNumber(std::istream& is, std::string& buff)
{
  static const char separator = std::use_facet<std::numpunct<char>>(LOCALE).decimal_point();

  bool hadSeparator = false;
  char nextChar = is.peek();
  while (std::isdigit(nextChar, LOCALE) || (!hadSeparator && nextChar == separator))
  {
    if (nextChar == separator)
    {
      hadSeparator = true;
    }
    is.get();
    buff += nextChar;
    nextChar = is.peek();
  }
}
