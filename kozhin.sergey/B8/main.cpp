#include <iostream>
#include <algorithm>
#include <iterator>
#include <cstring>
#include "token-input-iterator.hpp"
#include "format-output-iterator.hpp"

const std::size_t MAX_WORD_LENGTH = 20;
const std::size_t COMMA_LENGTH = 1;
const std::size_t SPACE_LENGTH = 1;
const std::size_t DASH_LENGTH = 3;

const std::size_t DEFAULT_LINE_WIDTH = 40;
const std::size_t MIN_LINE_WIDTH = MAX_WORD_LENGTH + COMMA_LENGTH + SPACE_LENGTH + DASH_LENGTH;

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 1 && argc != 3)
    {
      std::cerr << "Incorrect number of parameters.\n";
      return 1;
    }

    if (argc == 1)
    {
      std::copy(
          TokenInputIterator(std::cin), TokenInputIterator(), FormatOutputIterator(std::cout, DEFAULT_LINE_WIDTH));
    }
    else
    {
      if (strcmp(argv[1], "--line-width") != 0)
      {
        std::cerr << "Unknown option" << argv[1] << '\n';
        return 2;
      }

      char* end;
      std::size_t lineWidth = strtol(argv[2], &end, 10);
      if (*end != '\0' || lineWidth < MIN_LINE_WIDTH)
      {
        std::cerr << "Invalid line width\n";
        return 2;
      }

      std::copy(TokenInputIterator(std::cin), TokenInputIterator(), FormatOutputIterator(std::cout, lineWidth));
    }
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << '\n';
    return 2;
  }
  return 0;
}
