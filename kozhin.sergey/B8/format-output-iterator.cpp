#include "format-output-iterator.hpp"
#include <iostream>

class FormatOutputIterator::Impl
{
public:
  Impl(std::ostream& os, std::size_t lineLength);
  ~Impl();

  void operator==(const Token& token);

private:
  std::ostream* os_;
  std::size_t lineWidth_;
  std::string currentLine_;
  std::string tmpBuff_;
};

FormatOutputIterator::FormatOutputIterator(std::ostream& os, std::size_t lineWidth) :
  impl(std::make_shared<Impl>(os, lineWidth))
{
}

FormatOutputIterator& FormatOutputIterator::operator*()
{
  return *(this);
}

void FormatOutputIterator::operator=(const Token& token)
{
  *impl == token;
}

void FormatOutputIterator::operator++()
{
}

void FormatOutputIterator::operator++(int)
{
}

FormatOutputIterator::Impl::Impl(std::ostream& os, std::size_t lineLength) :
  os_(&os),
  lineWidth_(lineLength),
  currentLine_(""),
  tmpBuff_("")
{
}

FormatOutputIterator::Impl::~Impl()
{
  if (currentLine_.empty())
  {
    *os_ << tmpBuff_ << '\n';
    return;
  }

  if (currentLine_.size() + tmpBuff_.size() < lineWidth_)
  {
    *os_ << currentLine_ << ' ' << tmpBuff_ << '\n';
    return;
  }

  *os_ << currentLine_ << '\n' << tmpBuff_ << '\n';
}

void FormatOutputIterator::Impl::operator==(const Token& token)
{
  if (!token.isPunctuation())
  {
    if (currentLine_.size() + tmpBuff_.size() >= lineWidth_)
    {
      if (!currentLine_.empty())
      {
        *os_ << currentLine_ << '\n';
      }
      currentLine_ = tmpBuff_;
      tmpBuff_ = token.data;
      return;
    }

    if (!currentLine_.empty())
    {
      currentLine_ += ' ';
    }
    currentLine_ += tmpBuff_;
    tmpBuff_ = token.data;
    return;
  }

  if (token.type == Token::Type::DASH)
  {
    tmpBuff_ += " ";
  }

  tmpBuff_ += token.data;
}
