#include <iostream>
#include "phonebook-parser.hpp"

void task1()
{
  BookmarkHandler handler;
  PhoneBookParser parser(std::cin, std::cout, handler);
  parser.execute();
}
