#ifndef PHONEBOOKPARSER_HPP
#define PHONEBOOKPARSER_HPP

#include "bookmark-handler.hpp"

class PhoneBookParser
{
public:
  PhoneBookParser(std::istream& in, std::ostream& out, BookmarkHandler& bookmarks);

  void execute();

private:
  std::istream& in_;
  std::ostream& out_;
  BookmarkHandler& bookmarks_;

  std::string getEntryNumber();
  std::string getEntryName();
  std::string getMarkName();

  void checkForExtraArgs();

  void parseAdd();
  void parseStore();
  void parseInsert();
  void parseDelete();
  void parseShow();
  void parseMove();
};

#endif // BOOKPARSER_HPP
