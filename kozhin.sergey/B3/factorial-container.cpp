#include "factorial-container.hpp"
#include <stdexcept>
#include <limits>

constexpr FactorialContainer::iterator::value_type calculateMax()
{
  FactorialContainer::size_type index = 1;
  FactorialContainer::value_type factorial = 1;
  while (factorial <= std::numeric_limits<FactorialContainer::value_type>::max() / (index + 1))
  {
    ++index;
    factorial *= index;
  }

  return index;
}

constexpr FactorialContainer::size_type MIN_INDEX = 1;
constexpr FactorialContainer::size_type MAX_INDEX = calculateMax();

FactorialContainer::FactorialContainer(std::size_t size) :
  size_(size),
  begin_(iterator(1)),
  end_(iterator(size + 1))
{
}

FactorialContainer::const_iterator FactorialContainer::begin() const noexcept
{
  return begin_;
}

FactorialContainer::const_iterator FactorialContainer::end() const noexcept
{
  return end_;
}

FactorialContainer::const_reverse_iterator FactorialContainer::rbegin() const noexcept
{
  return std::make_reverse_iterator(end());
}

FactorialContainer::const_reverse_iterator FactorialContainer::rend() const noexcept
{
  return std::make_reverse_iterator(begin());
}

FactorialContainer::size_type FactorialContainer::size() const noexcept
{
  return size_;
}

FactorialContainer::size_type FactorialContainer::max_size() const noexcept
{
  return size_;
}

bool FactorialContainer::operator==(const FactorialContainer& rhs) const noexcept
{
  return size_ == rhs.size_;
}

bool FactorialContainer::operator!=(const FactorialContainer& rhs) const noexcept
{
  return !(*this == rhs);
}

FactorialContainer::iterator::iterator(unsigned int index) :
  index_(index),
  value_(factorial(index))
{
}

bool FactorialContainer::iterator::operator==(const iterator& rhs) const noexcept
{
  return (index_ == rhs.index_) && (value_ == rhs.value_);
}

bool FactorialContainer::iterator::operator!=(const iterator& rhs) const noexcept
{
  return !(*this == rhs);
}

FactorialContainer::iterator::value_type FactorialContainer::iterator::operator*() const noexcept
{
  return value_;
}

FactorialContainer::iterator& FactorialContainer::iterator::operator++()
{
  if (index_ == MAX_INDEX)
  {
    throw std::out_of_range("Next factorial can't be stored");
  }
  ++index_;
  value_ *= index_;

  return *this;
}

FactorialContainer::iterator FactorialContainer::iterator::operator++(int)
{
  iterator tmp(*this);
  operator++();
  return tmp;
}

FactorialContainer::iterator& FactorialContainer::iterator::operator--()
{
  if (value_ == MIN_INDEX)
  {
    throw std::out_of_range("No previous factorial");
  }
  value_ /= index_;
  --index_;

  return *this;
}

FactorialContainer::iterator FactorialContainer::iterator::operator--(int)
{
  iterator tmp(*this);
  operator--();
  return tmp;
}

FactorialContainer::iterator::value_type FactorialContainer::iterator::factorial(unsigned int n)
{
  if (n < MIN_INDEX || n > MAX_INDEX)
  {
    throw std::invalid_argument("Factorial index is out of range");
  }
  return factorialImpl(n, 1);
}

FactorialContainer::iterator::value_type FactorialContainer::iterator::factorialImpl(
    unsigned int n, FactorialContainer::iterator::value_type acc)
{
  return (n == 0) ? acc : factorialImpl(n - 1, acc * n);
}
