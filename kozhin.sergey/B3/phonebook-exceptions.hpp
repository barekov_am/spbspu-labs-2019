#ifndef PHONEBOOKEXCEPTIONS_HPP
#define PHONEBOOKEXCEPTIONS_HPP

#include <stdexcept>

class InvalidBookmarkException : public std::exception
{
public:
  const char* what() const throw() override
  {
    return "Access to invalid bookmark";
  }
};

class InvalidStepException : public std::exception
{
public:
  const char* what() const throw() override
  {
    return "Invalid number of steps.";
  }
};

class ParsingException : public std::exception
{
public:
  const char* what() const throw() override
  {
    return "Error while parsing input";
  }
};

class EmptyPhoneBookException : public std::exception
{
public:
  const char* what() const throw() override
  {
    return "Phonebook is empty";
  }
};

#endif // PHONEBOOKEXCEPTIONS_HPP
