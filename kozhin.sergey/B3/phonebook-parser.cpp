#include "phonebook-parser.hpp"
#include <algorithm>
#include "phonebook-exceptions.hpp"
#include "blank.hpp"

PhoneBookParser::PhoneBookParser(std::istream& in, std::ostream& out, BookmarkHandler& bookmarks) :
  in_(in),
  out_(out),
  bookmarks_(bookmarks)
{
}

void PhoneBookParser::execute()
{
  const static struct
  {
    const char* name;
    void (PhoneBookParser::*function)();
  } commands[] = {
      {"add", &PhoneBookParser::parseAdd},
      {"store", &PhoneBookParser::parseStore},
      {"insert", &PhoneBookParser::parseInsert},
      {"delete", &PhoneBookParser::parseDelete},
      {"show", &PhoneBookParser::parseShow},
      {"move", &PhoneBookParser::parseMove}};

  std::string command;
  while (in_ >> blank >> command)
  {
    auto foundCommand = std::find_if(
        std::begin(commands), std::end(commands), [&command](const auto& el) { return command == el.name; });

    if (foundCommand == std::end(commands))
    {
      in_.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      out_ << "<INVALID COMMAND>\n";
      continue;
    }

    try
    {
      (this->*foundCommand->function)();
    }
    catch (ParsingException& e)
    {
      out_ << "<INVALID COMMAND>\n";
    }
    catch (InvalidStepException& e)
    {
      out_ << "<INVALID STEP>\n";
    }
    catch (InvalidBookmarkException& e)
    {
      out_ << "<INVALID BOOKMARK>\n";
    }
    catch (EmptyPhoneBookException& e)
    {
      out_ << "<EMPTY>\n";
    }

    in_.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }
  if (!in_.eof() && in_.fail())
  {
    throw std::ios_base::failure("Errors while reading data");
  }
}

std::string PhoneBookParser::getEntryNumber()
{
  if ((in_ >> blank).peek() == '\n')
  {
    throw ParsingException();
  }

  std::string number;
  in_ >> number;

  if (std::find_if(std::begin(number), std::end(number), [](char c) { return !isdigit(c); }) != std::end(number))
  {
    throw ParsingException();
  }

  return number;
}

std::string PhoneBookParser::getEntryName()
{
  if ((in_ >> blank).peek() != '\"')
  {
    throw ParsingException();
  }

  in_.ignore();
  std::string name;
  while (in_)
  {
    const char c = in_.get();
    if (c == '\\')
    {
      const char next = in_.get();
      if (next == '\"' || next == '\\')
      {
        name += next;
        continue;
      }
      else
      {
        throw ParsingException();
      }
    }
    if (c == '\"')
    {
      return name;
    }

    name += c;
  }

  throw ParsingException();
}

std::string PhoneBookParser::getMarkName()
{
  if ((in_ >> blank).peek() == '\n')
  {
    throw ParsingException();
  }
  std::string markName;
  in_ >> markName;

  if (std::find_if(std::begin(markName), std::end(markName), [](char c) { return !isalnum(c) && c != '-'; })
      != std::end(markName))
  {
    throw ParsingException();
  }

  return markName;
}

void PhoneBookParser::checkForExtraArgs()
{
  if ((in_ >> blank).peek() != '\n' && !in_.eof())
  {
    throw ParsingException();
  }
}

void PhoneBookParser::parseAdd()
{
  std::string number = getEntryNumber();
  std::string name = getEntryName();
  checkForExtraArgs();

  bookmarks_.addEntry({name, number});
}

void PhoneBookParser::parseStore()
{
  std::string markName = getMarkName();
  std::string newMarkName = getMarkName();
  checkForExtraArgs();

  bookmarks_.store(markName, newMarkName);
}

void PhoneBookParser::parseInsert()
{
  if ((in_ >> blank).peek() == '\n')
  {
    throw ParsingException();
  }
  std::string placement;
  in_ >> placement;
  if (placement != "before" && placement != "after")
  {
    throw ParsingException();
  }

  std::string markName = getMarkName();
  std::string number = getEntryNumber();
  std::string name = getEntryName();
  checkForExtraArgs();

  if (placement == "before")
  {
    bookmarks_.insertBefore(markName, {name, number});
    return;
  }
  bookmarks_.insertAfter(markName, {name, number});
}

void PhoneBookParser::parseDelete()
{
  std::string markName = getMarkName();
  checkForExtraArgs();

  bookmarks_.deleteEntry(markName);
}

void PhoneBookParser::parseShow()
{
  std::string markName = getMarkName();
  checkForExtraArgs();

  PhoneBook::value_type entry = bookmarks_.getEntry(markName);
  out_ << entry.number << ' ' << entry.name << '\n';
}

void PhoneBookParser::parseMove()
{
  std::string markName = getMarkName();
  if ((in_ >> blank).peek() == '\n')
  {
    throw ParsingException();
  }
  std::string step;
  in_ >> step;
  checkForExtraArgs();

  if (step == "first")
  {
    bookmarks_.move(markName, BookmarkHandler::FIRST);
    return;
  }
  if (step == "last")
  {
    bookmarks_.move(markName, BookmarkHandler::LAST);
    return;
  }

  char* end;
  long numStep = std::strtol(step.c_str(), &end, 10);
  if (step.empty() || *end != '\0')
  {
    throw InvalidStepException();
  }

  bookmarks_.move(markName, numStep);
}
