#ifndef BOOKMARKHANDLER_HPP
#define BOOKMARKHANDLER_HPP

#include <map>
#include "phonebook.hpp"

class BookmarkHandler
{
public:
  enum MovePosition
  {
    FIRST,
    LAST
  };

  BookmarkHandler();

  void addEntry(const PhoneBook::value_type& value);

  void store(const std::string& markName, const std::string& newMarkName);

  void insertBefore(const std::string& markName, const PhoneBook::value_type& value);
  void insertAfter(const std::string& markName, const PhoneBook::value_type& value);

  void deleteEntry(const std::string& markName);

  PhoneBook::value_type& getEntry(const std::string& markName);
  const PhoneBook::value_type& getEntry(const std::string& markName) const;

  void move(const std::string& markName, long steps);
  void move(const std::string& markName, MovePosition position);

  bool empty() const;

private:
  typedef std::map<std::string, PhoneBook::iterator> bookmarks_type;

  std::map<std::string, PhoneBook::iterator> bookmarks_;
  PhoneBook book_;

  bookmarks_type::iterator find(const std::string& markName);
  bookmarks_type::const_iterator find(const std::string& markName) const;
};

#endif // BOOKMARKHANDLER_HPP
