#include <iostream>
#include "factorial-container.hpp"

void task2()
{
  FactorialContainer factorials;

  std::ostream_iterator<unsigned long> outIt(std::cout, " ");

  std::copy(factorials.begin(), factorials.end(), outIt);
  std::cout << '\n';

  std::copy(factorials.rbegin(), factorials.rend(), outIt);
  std::cout << '\n';
}
