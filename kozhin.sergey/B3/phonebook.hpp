#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <list>
#include <string>

class PhoneBook
{
public:
  typedef struct
  {
    std::string name;
    std::string number;
  } value_type;
  typedef std::list<value_type>::size_type size_type;
  typedef std::list<value_type>::difference_type difference_type;
  typedef std::list<value_type>::reference reference;
  typedef std::list<value_type>::const_reference const_reference;
  typedef std::list<value_type>::iterator iterator;
  typedef std::list<value_type>::const_iterator const_iterator;
  typedef std::list<value_type>::reverse_iterator reverse_iterator;
  typedef std::list<value_type>::const_iterator const_reverse_iterator;

  iterator begin() noexcept;
  const_iterator begin() const noexcept;

  iterator end() noexcept;
  const_iterator end() const noexcept;

  void push_back(const_reference value);
  iterator insert(iterator pos, const_reference value);
  void replace(iterator pos, const_reference value);
  iterator erase(iterator pos);

  bool empty() const noexcept;
  size_type size() const noexcept;
  size_type max_size() const noexcept;

private:
  std::list<value_type> books_;
};

#endif // PHONEBOOK_HPP
