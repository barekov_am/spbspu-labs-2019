#ifndef FACTORIALCONTAINER_HPP
#define FACTORIALCONTAINER_HPP

#include <iterator>

class FactorialContainer
{
public:
  class iterator : public std::iterator<std::bidirectional_iterator_tag, unsigned long long, std::ptrdiff_t,
                                const unsigned long long*, unsigned long long>
  {
  public:
    iterator(unsigned int index = 1);

    bool operator==(const iterator& rhs) const noexcept;
    bool operator!=(const iterator& rhs) const noexcept;

    value_type operator*() const noexcept;

    iterator& operator++();
    iterator operator++(int);

    iterator& operator--();
    iterator operator--(int);

  private:
    unsigned int index_;
    value_type value_;

    value_type factorial(unsigned int n);
    value_type factorialImpl(unsigned int n, value_type acc);
  };

  typedef iterator::value_type value_type;
  typedef std::size_t size_type;
  typedef iterator::difference_type difference_type;
  typedef iterator const_iterator;
  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<iterator> const_reverse_iterator;

  FactorialContainer(std::size_t size = 10);

  const_iterator begin() const noexcept;
  const_iterator end() const noexcept;

  const_reverse_iterator rbegin() const noexcept;
  const_reverse_iterator rend() const noexcept;

  size_type size() const noexcept;
  size_type max_size() const noexcept;

  bool operator==(const FactorialContainer& rhs) const noexcept;
  bool operator!=(const FactorialContainer& rhs) const noexcept;

private:
  size_type size_;
  const_iterator begin_;
  const_iterator end_;
};

#endif // FACTORIALCONTAINER_HPP
