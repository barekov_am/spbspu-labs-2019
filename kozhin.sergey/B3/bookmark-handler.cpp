#include "bookmark-handler.hpp"
#include "phonebook-exceptions.hpp"

const char* CURRENT_BOOKMARK = "current";

BookmarkHandler::BookmarkHandler() :
  bookmarks_(),
  book_()
{
  bookmarks_[CURRENT_BOOKMARK] = book_.end();
}

void BookmarkHandler::addEntry(const PhoneBook::value_type& value)
{
  book_.push_back(value);
  if (book_.size() == 1)
  {
    bookmarks_[CURRENT_BOOKMARK] = book_.begin();
  }
}

void BookmarkHandler::store(const std::string& markName, const std::string& newMarkName)
{
  bookmarks_[newMarkName] = find(markName)->second;
}

void BookmarkHandler::insertBefore(const std::string& markName, const PhoneBook::value_type& value)
{
  // auto = bookmarks_type::iterator
  auto foundMark = find(markName);
  if (empty())
  {
    addEntry(value);
  }
  book_.insert(foundMark->second, value);
}

void BookmarkHandler::insertAfter(const std::string& markName, const PhoneBook::value_type& value)
{
  // auto = bookmarks_type::iterator
  auto foundMark = find(markName);
  if (empty())
  {
    addEntry(value);
  }
  book_.insert(std::next(foundMark->second), value);
}

void BookmarkHandler::deleteEntry(const std::string& markName)
{
  // auto = PhoneBook::iterator
  auto entryToDelete = find(markName)->second;
  if (entryToDelete == book_.end())
  {
    throw EmptyPhoneBookException();
  }
  // auto = PhoneBook;
  auto nextEntry = book_.erase(entryToDelete);
  if (nextEntry == book_.end())
  {
    nextEntry = book_.begin();
  }
  // auto = std::pair<std::string, PhoneBook::iterator>
  for (auto& el : bookmarks_)
  {
    if (el.second == entryToDelete)
    {
      el.second = nextEntry;
    }
  }
}

PhoneBook::value_type &BookmarkHandler::getEntry(const std::string &markName)
{
  // auto = bookmarks_type::iterator
  auto foundMark = find(markName);
  if (foundMark->second == book_.end())
  {
    throw EmptyPhoneBookException();
  }
  return *foundMark->second;
}

const PhoneBook::value_type& BookmarkHandler::getEntry(const std::string& markName) const
{
  // auto = bookmarks_type::iterator
  auto foundMark = find(markName);
  if (foundMark->second == book_.end())
  {
    throw EmptyPhoneBookException();
  }
  return *foundMark->second;
}

void BookmarkHandler::move(const std::string& markName, long steps)
{
  if (empty())
  {
    throw EmptyPhoneBookException();
  }

  // auto = bookmarks_type::iterator
  auto foundMark = find(markName);
  if (steps > 0)
  {
    if (std::distance(foundMark->second, --book_.end()) < steps)
    {
      foundMark->second = --book_.end();
    }
    foundMark->second = std::next(foundMark->second, steps);
  }
  else
  {
    if (std::distance(book_.begin(), foundMark->second) < -steps)
    {
      foundMark->second = book_.begin();
    }
    foundMark->second = std::prev(foundMark->second, -steps);
  }
}

void BookmarkHandler::move(const std::string& markName, BookmarkHandler::MovePosition position)
{
  // auto = bookmarks_type::iterator
  auto foundMark = find(markName);
  if (position == BookmarkHandler::FIRST)
  {
    foundMark->second = book_.begin();
  }
  else
  {
    foundMark->second = --book_.end();
  }
}

bool BookmarkHandler::empty() const
{
  return book_.empty();
}

BookmarkHandler::bookmarks_type::const_iterator BookmarkHandler::find(const std::string& markName) const
{
  // auto = bookmarks_type::iterator
  auto foundMark = bookmarks_.find(markName);
  if (foundMark == bookmarks_.end())
  {
    throw InvalidBookmarkException();
  }

  return foundMark;
}

BookmarkHandler::bookmarks_type::iterator BookmarkHandler::find(const std::string& markName)
{
  // auto = bookmarks_type::iterator
  auto foundMark = bookmarks_.find(markName);
  if (foundMark == bookmarks_.end())
  {
    throw InvalidBookmarkException();
  }

  return foundMark;
}
