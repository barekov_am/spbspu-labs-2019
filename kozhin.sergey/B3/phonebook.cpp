#include "phonebook.hpp"

PhoneBook::iterator PhoneBook::begin() noexcept
{
  return books_.begin();
}

PhoneBook::const_iterator PhoneBook::begin() const noexcept
{
  return books_.begin();
}

PhoneBook::iterator PhoneBook::end() noexcept
{
  return books_.end();
}

PhoneBook::const_iterator PhoneBook::end() const noexcept
{
  return books_.end();
}

void PhoneBook::push_back(PhoneBook::const_reference value)
{
  books_.push_back(value);
}

PhoneBook::iterator PhoneBook::insert(PhoneBook::iterator pos, PhoneBook::const_reference value)
{
  return books_.insert(pos, value);
}

void PhoneBook::replace(PhoneBook::iterator pos, PhoneBook::const_reference value)
{
  *pos = value;
}

PhoneBook::iterator PhoneBook::erase(PhoneBook::iterator pos)
{
  return books_.erase(pos);
}

bool PhoneBook::empty() const noexcept
{
  return books_.empty();
}

PhoneBook::size_type PhoneBook::max_size() const noexcept
{
  return books_.max_size();
}

std::size_t PhoneBook::size() const noexcept
{
  return books_.size();
}
