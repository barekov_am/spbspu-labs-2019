#include <iostream>
#include <sstream>
#include <algorithm>
#include <locale>
#include "queue-with-priority.hpp"

template<typename Char, typename CharTraits>
std::basic_istream<Char, CharTraits>& blank(std::basic_istream<Char, CharTraits>& is)
{
  while (std::isblank(static_cast<char>(is.peek()), std::locale("")))
  {
    is.ignore();
  }

  return is;
}

std::istream& operator>>(std::istream& is, QueueWithPriority<std::string>::ElementPriority& priority)
{
  static const struct
  {
    const char* name;
    QueueWithPriority<std::string>::ElementPriority priority;
  } priorities[] = {
      {"high", QueueWithPriority<std::string>::HIGH},
      {"normal", QueueWithPriority<std::string>::NORMAL},
      {"low", QueueWithPriority<std::string>::LOW}};

  std::string priorityStr;
  is >> priorityStr;

  auto findPriority = std::find_if(
      std::begin(priorities), std::end(priorities), [&](const auto& el) { return priorityStr == el.name; });

  if (findPriority == std::end(priorities))
  {
    is.setstate(std::ios::failbit);
    return is;
  }

  priority = findPriority->priority;
  return is;
}

void addToQueue(QueueWithPriority<std::string>& queue, std::istream& args)
{
  QueueWithPriority<std::string>::ElementPriority priority;
  if (!(args >> priority))
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  std::string data;
  getline(args >> blank, data);
  if (data.empty())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.add(data, priority);
}

void getFromQueue(QueueWithPriority<std::string>& queue, std::istream& args)
{
  if (!args.eof())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  if (queue.empty())
  {
    std::cout << "<EMPTY>\n";
    return;
  }

  queue.process([](const auto& el) { std::cout << el << '\n'; });
}

void accelerateQueue(QueueWithPriority<std::string>& queue, std::istream& args)
{
  if (!args.eof())
  {
    std::cout << "<INVALID COMMAND>\n";
    return;
  }

  queue.accelerate();
}
