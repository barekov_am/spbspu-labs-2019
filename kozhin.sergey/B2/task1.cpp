#include <iostream>
#include <algorithm>
#include <sstream>
#include "queue-with-priority.hpp"

void addToQueue(QueueWithPriority<std::string>& queue, std::istream& args);
void getFromQueue(QueueWithPriority<std::string>& queue, std::istream& args);
void accelerateQueue(QueueWithPriority<std::string>& queue, std::istream& args);

template<typename Char, typename CharTraits>
std::basic_istream<Char, CharTraits>& blank(std::basic_istream<Char, CharTraits>& is);

void task1()
{
  QueueWithPriority<std::string> queue;

  static const struct
  {
    const char* name;
    std::function<void(QueueWithPriority<std::string>&, std::istream& args)> function;
  } commands[] = {
      {"add", &addToQueue},
      {"get", &getFromQueue},
      {"accelerate", &accelerateQueue}};

  std::string input;
  while (getline(std::cin, input))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error while reading data.");
    }

    std::stringstream inputStream(input);
    inputStream >> std::noskipws;

    std::string command;
    inputStream >> blank >> command;

    auto findCommand =
        std::find_if(std::begin(commands), std::end(commands), [&](const auto& el) { return command == el.name; });

    if (findCommand != std::end(commands))
    {
      findCommand->function(queue, inputStream >> blank);
    }
    else
    {
      std::cout << "<INVALID COMMAND>\n";
    }
  }
}
