#ifndef TASKS_HPP
#define TASKS_HPP

#include <iostream>

void firstTask(const char*);
void secondTask(const char*);
void thirdTask();
void fourthTask(const char*, int);

#endif // TASKS_HPP
