#ifndef PRINT_HPP
#define PRINT_HPP

#include <iostream>
#include <vector>
#include <forward_list>

template<typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
  for (const T& el : vec)
  {
    os << el << ' ';
  }

  return os;
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const std::forward_list<T>& list)
{
  for (const T& el : list)
  {
    os << el << ' ';
  }

  return os;
}

#endif // PRINT_HPP
