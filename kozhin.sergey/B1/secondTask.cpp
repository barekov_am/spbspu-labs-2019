#include "tasks.hpp"
#include <fstream>
#include <memory>
#include <vector>

const std::size_t initialSize = 1024;

void secondTask(const char* fileName)
{
  std::ifstream file(fileName);
  if (!file)
  {
    throw std::invalid_argument("No such file.");
  }

  std::size_t size = initialSize;

  std::unique_ptr<char[], decltype(&free)> array(static_cast<char*>(malloc(size)), &free);

  if (!array)
  {
    throw std::runtime_error("Could not allocate memory");
  }

  std::size_t ind = 0;
  while (file)
  {
    file.read(&array[ind], initialSize);
    ind += file.gcount();
    if (file.gcount() == initialSize)
    {
      size += initialSize;
      std::unique_ptr<char[], decltype(&free)> new_array(static_cast<char*>(realloc(array.get(), size)), &free);
      if (!new_array)
      {
        throw std::runtime_error("Could not allocate memory for new chars");
      }
      array.release();
      std::swap(array, new_array);
    }
    if (!file.eof() && file.fail())
    {
      throw std::runtime_error("Chars reading failed");
    }
  }

  std::vector<char> vec(&array[0], &array[ind]);

  for (char c : vec)
  {
    std::cout << c;
  }
}
