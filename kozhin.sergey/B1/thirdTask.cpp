#include "tasks.hpp"
#include "print.hpp"

void procces(std::vector<int>& arr)
{
  auto it = arr.begin();
  if (*(arr.end() - 1) == 1)
  {
    while (it != arr.end())
    {
      if (*it % 2 == 0)
      {
        it = arr.erase(it);
        continue;
      }
      ++it;
    }
  }
  else if (*(arr.end() - 1) == 2)
  {
    while (it != arr.end())
    {
      if (*it % 3 == 0)
      {
        it = arr.insert(it + 1, 3, 1);
        it += 2;
      }
      ++it;
    }
  }
}

void thirdTask()
{
  std::vector<int> arr;

  int currNum = -1;
  while ((std::cin && !(std::cin >> currNum).eof()))
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error while reading data.");
    }
    if (currNum == 0)
    {
      break;
    }
    arr.push_back(currNum);
  }
  if (arr.empty())
  {
    return;
  }
  if (std::cin.eof() && currNum != 0)
  {
    throw std::invalid_argument("No zero at the end of input.");
  }

  procces(arr);

  std::cout << arr << '\n';
}
