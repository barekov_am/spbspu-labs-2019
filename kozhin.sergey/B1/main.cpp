#include "tasks.hpp"

int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    std::cerr << "No task number.\n";
    return 1;
  }

  try
  {
    switch (atoi(argv[1]))
    {
    case 1:
    {
      if (argc < 3)
      {
        std::cerr << "No sorting order.\n";
        return 1;
      }

      firstTask(argv[2]);

      break;
    }
    case 2:
    {
      if (argc < 3)
      {
        std::cerr << "No file name specified.\n";
        return 1;
      }

      secondTask(argv[2]);

      break;
    }
    case 3:
    {
      thirdTask();
      break;
    }
    case 4:
    {
      if (argc < 3)
      {
        std::cerr << "No sorting order.\n";
        return 1;
      }

      if (argc < 4)
      {
        std::cerr << "No size specified\n";
        return 1;
      }

      int size = atoi(argv[3]);
      if (size < 1)
      {
        std::cerr << "Invalid size.\n";
        return 1;
      }

      fourthTask(argv[2], size);
      break;
    }
    default:
      std::cerr << "Unknown task number.\n";
      return 1;
    }
  }
  catch (std::exception& e)
  {
    std::cerr << e.what() << '\n';
    return 1;
  }

  return 0;
}
