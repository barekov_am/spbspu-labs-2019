#ifndef ACCESSPOLICY_HPP
#define ACCESSPOLICY_HPP

#include <cstddef>

template<class Container>
struct operator_access_policy
{
  typedef typename Container::value_type value_type;
  typedef typename std::size_t index_type;

  static index_type begin(const Container&)
  {
    return 0;
  }

  static index_type end(const Container& cont)
  {
    return cont.size();
  }

  static value_type& getEl(Container& cont, index_type ind)
  {
    return cont[ind];
  }
};

template<class Container>
struct method_access_policy
{
  typedef typename Container::value_type value_type;
  typedef typename std::size_t index_type;

  static index_type begin(const Container&)
  {
    return 0;
  }

  static index_type end(const Container& cont)
  {
    return cont.size();
  }

  static value_type& getEl(Container& cont, index_type ind)
  {
    return cont.at(ind);
  }
};

template<class Container>
struct iterator_access_policy
{
  typedef typename Container::value_type value_type;
  typedef typename Container::iterator index_type;

  static index_type begin(Container& cont)
  {
    return cont.begin();
  }

  static index_type end(Container& cont)
  {
    return cont.end();
  }

  static value_type& getEl(Container&, index_type ind)
  {
    return *ind;
  }
};

#endif // ACCESSPOLICY_HPP
