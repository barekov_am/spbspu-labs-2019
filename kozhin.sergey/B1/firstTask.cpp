#include "tasks.hpp"
#include "sort.hpp"
#include "print.hpp"

void firstTask(const char* order)
{
  const auto compare = getCompare<int>(order);

  std::vector<int> arrByOperator;

  int currNum = -1;
  while (std::cin && !(std::cin >> currNum).eof())
  {
    if (std::cin.fail())
    {
      throw std::ios_base::failure("Error while reading data.");
    }
    arrByOperator.push_back(currNum);
  }

  std::vector<int> arrByMethod = arrByOperator;
  std::forward_list<int> arrByIterator(arrByOperator.begin(), arrByOperator.end());

  sort<operator_access_policy>(arrByOperator, compare);
  sort<method_access_policy>(arrByMethod, compare);
  sort<iterator_access_policy>(arrByIterator, compare);

  std::cout << arrByOperator << '\n';
  std::cout << arrByMethod << '\n';
  std::cout << arrByIterator << '\n';
}
