#include "tasks.hpp"
#include <memory>
#include <random>
#include "sort.hpp"
#include "print.hpp"

void fillRandom(double* array, int size)
{
  std::random_device device;
  std::mt19937 generator(device());
  std::uniform_real_distribution<double> distribution(-1.0, 1.0);

  for (int i = 0; i < size; ++i)
  {
    array[i] = distribution(generator);
  }
}

void fourthTask(const char* order, int size)
{
  const auto compare = getCompare<double>(order);

  std::unique_ptr<double[]> arr = std::make_unique<double[]>(size);

  fillRandom(arr.get(), size);

  std::vector<double> vec(arr.get(), arr.get() + size);
  std::cout << vec << '\n';

  sort<operator_access_policy>(vec, compare);
  std::cout << vec << '\n';
}
