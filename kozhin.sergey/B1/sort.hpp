#ifndef SORT_HPP
#define SORT_HPP

#include <functional>
#include <cstring>
#include "access-policy.hpp"

template<typename T>
std::function<bool(const T&, const T&)> getCompare(const char* order)
{
  if (std::strcmp(order, "ascending") == 0)
  {
    return [](const T& lhs, const T& rhs) { return lhs < rhs; };
  }
  else if (std::strcmp(order, "descending") == 0)
  {
    return [](const T& lhs, const T& rhs) { return lhs > rhs; };
  }
  else
  {
    throw std::invalid_argument("Unknown sorting order");
  }
}

template<template<class> class AccessPolicy, typename Container, typename T>
void sort(Container& cont, const std::function<bool(const T&, const T&)>& compare)
{
  typedef AccessPolicy<Container> Policy;

  for (auto i = Policy::begin(cont); i != Policy::end(cont); ++i)
  {
    auto findInd = i;
    for (auto j = i; j != Policy::end(cont); ++j)
    {
      if (compare(Policy::getEl(cont, j), Policy::getEl(cont, findInd)))
      {
        findInd = j;
      }
    }
    std::swap(Policy::getEl(cont, i), Policy::getEl(cont, findInd));
  }
}

#endif // SORT_HPP
