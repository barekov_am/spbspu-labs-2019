#include <iostream>
#include <algorithm>
#include <iterator>
#include "statistic.hpp"

int main()
{
  std::locale locale("");
  std::cin.imbue(locale);
  std::cout.imbue(locale);

  Statistic statistic;
  statistic = std::for_each(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), statistic);

  if (std::cin.fail() && !std::cin.eof())
  {
    std::cerr << "Error while reading data.\n";
    return 2;
  }

  std::cout << statistic;
}
