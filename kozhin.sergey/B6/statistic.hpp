#ifndef STATISTIC_HPP
#define STATISTIC_HPP

#include <iosfwd>

class Statistic
{
public:
  Statistic();

  void operator()(const long num);

private:
  long count_;
  long max_;
  long min_;
  long posCount_;
  long negCount_;
  long oddSum_;
  long evenSum_;
  long first_;
  long last_;
  bool empty_;

  friend std::ostream& operator<<(std::ostream& os, const Statistic& stat);
};

#endif // STATISTIC_HPP
