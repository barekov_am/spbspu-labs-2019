#include "statistic.hpp"
#include <algorithm>
#include <iostream>

Statistic::Statistic() :
  count_(0),
  max_(0),
  min_(0),
  posCount_(0),
  negCount_(0),
  oddSum_(0),
  evenSum_(0),
  first_(0),
  last_(0),
  empty_(true)
{
}

void Statistic::operator()(const long num)
{
  ++count_;
  if (empty_)
  {
    first_ = num;
    max_ = num;
    min_ = num;
    empty_ = false;
  }
  else
  {
    max_ = std::max(num, max_);
    min_ = std::min(num, min_);
  }

  if (num > 0)
  {
    ++posCount_;
  }
  else if (num != 0)
  {
    ++negCount_;
  }

  if (num % 2 == 0)
  {
    evenSum_ += num;
  }
  else
  {
    oddSum_ += num;
  }

  last_ = num;
}

std::ostream& operator<<(std::ostream& os, const Statistic& stat)
{
  if (stat.empty_)
  {
    os << "No Data\n";
    return os;
  }

  os << "Max: " << stat.max_ << '\n';
  os << "Min: " << stat.min_ << '\n';
  os << "Mean: " << 1.0 * (stat.evenSum_ + stat.oddSum_) / stat.count_ << '\n';
  os << "Positive: " << stat.posCount_ << '\n';
  os << "Negative: " << stat.negCount_ << '\n';
  os << "Odd Sum: " << stat.oddSum_ << '\n';
  os << "Even Sum: " << stat.evenSum_ << '\n';
  os << "First/Last Equal: " << (stat.first_ == stat.last_ ? "yes" : "no") << '\n';

  return os;
}
