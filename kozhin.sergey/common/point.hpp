#ifndef POINT_HPP
#define POINT_HPP

#include <iosfwd>

struct Point
{
  int x, y;
};

std::istream& operator>>(std::istream& is, Point& point);
std::ostream& operator<<(std::ostream& os, const Point& point);

#endif // POINT_HPP
