#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <memory>
#include "base-types.hpp"

namespace kozhin
{
  class Shape
  {
  public:
    using ptr = std::shared_ptr<Shape>;
    using array = std::unique_ptr<ptr[]>;

    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual point_t getCenter() const = 0;
    virtual void move(double, double) = 0;
    virtual void move(const point_t&) = 0;
    virtual void scale(double) = 0;
    virtual void rotate(double) = 0;
  };
} // namespace kozhin
#endif // SHAPE_HPP
