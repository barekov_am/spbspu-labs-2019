#include "composite-shape.hpp"
#include <algorithm>
#include <stdexcept>
#include <cmath>

kozhin::CompositeShape::CompositeShape() :
  count_(0),
  size_(0)
{
}

kozhin::CompositeShape::CompositeShape(const kozhin::CompositeShape& rhs) :
  count_(rhs.count_),
  size_(rhs.size_),
  shapes_(std::make_unique<kozhin::Shape::ptr[]>(rhs.size_))
{
  for (size_t i = 0; i < size_; ++i)
  {
    shapes_[i] = rhs.shapes_[i];
  }
}

kozhin::CompositeShape::CompositeShape(kozhin::CompositeShape&& rhs) noexcept :
  count_(rhs.count_),
  size_(rhs.size_),
  shapes_(std::move(rhs.shapes_))
{
  rhs.count_ = 0;
  rhs.size_ = 0;
}

kozhin::CompositeShape& kozhin::CompositeShape::operator=(const kozhin::CompositeShape& rhs)
{
  if (this != &rhs)
  {
    shapes_ = std::make_unique<kozhin::Shape::ptr[]>(rhs.size_);
    for (size_t i = 0; i < rhs.size_; ++i)
    {
      shapes_[i] = rhs.shapes_[i];
    }

    count_ = rhs.count_;
    size_ = rhs.size_;
  }

  return *this;
}

kozhin::CompositeShape& kozhin::CompositeShape::operator=(kozhin::CompositeShape&& rhs) noexcept
{
  if (this != &rhs)
  {
    count_ = rhs.count_;
    rhs.count_ = 0;

    size_ = rhs.size_;
    rhs.size_ = 0;

    shapes_ = std::move(rhs.shapes_);
  }

  return *this;
}

kozhin::Shape::ptr kozhin::CompositeShape::operator[](size_t ind) const
{
  if (ind >= count_)
  {
    throw std::out_of_range("Index out of range.");
  }

  return shapes_[ind];
}

void kozhin::CompositeShape::add(kozhin::Shape::ptr newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Can't add nullptr.");
  }

  if (count_ == size_)
  {
    kozhin::Shape::array tmp = std::make_unique<kozhin::Shape::ptr[]>(size_ + 1);
    for (size_t i = 0; i < size_; ++i)
    {
      tmp[i] = shapes_[i];
    }
    shapes_ = std::move(tmp);
    ++size_;
  }

  shapes_[count_++] = newShape;
}

void kozhin::CompositeShape::remove(size_t ind)
{
  if (ind >= count_)
  {
    throw std::out_of_range("Index out of range.");
  }

  for (size_t i = ind + 1; i < count_; ++i)
  {
    shapes_[i - 1] = shapes_[i];
  }

  shapes_[--count_] = nullptr;
}

double kozhin::CompositeShape::getArea() const
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("Composite shape is empty");
  }

  double area = 0.0;
  for (size_t i = 0; i < count_; ++i)
  {
    area += shapes_[i]->getArea();
  }

  return area;
}

kozhin::rectangle_t kozhin::CompositeShape::getFrameRect() const
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("Composite shape is empty");
  }

  kozhin::rectangle_t currShapeRect = shapes_[0]->getFrameRect();

  double left = currShapeRect.pos.x - currShapeRect.width / 2;
  double right = currShapeRect.pos.x + currShapeRect.width / 2;
  double up = currShapeRect.pos.y + currShapeRect.height / 2;
  double down = currShapeRect.pos.y - currShapeRect.height / 2;

  for (size_t i = 1; i < count_; ++i)
  {
    currShapeRect = shapes_[i]->getFrameRect();
    left = std::min(left, currShapeRect.pos.x - currShapeRect.width / 2);
    right = std::max(right, currShapeRect.pos.x + currShapeRect.width / 2);
    up = std::max(up, currShapeRect.pos.y + currShapeRect.height / 2);
    down = std::min(down, currShapeRect.pos.y - currShapeRect.height / 2);
  }

  return {right - left, up - down, {(left + right) / 2, (up + down) / 2}};
}

kozhin::point_t kozhin::CompositeShape::getCenter() const
{
  return getFrameRect().pos;
}

size_t kozhin::CompositeShape::size() const
{
  return count_;
}

void kozhin::CompositeShape::move(double dx, double dy)
{
  if (shapes_ == nullptr)
  {
    throw std::logic_error("Composite shape is empty");
  }
  for (size_t i = 0; i < count_; ++i)
  {
    shapes_[i]->move(dx, dy);
  }
}

void kozhin::CompositeShape::move(const kozhin::point_t& point)
{
  const kozhin::point_t center = getCenter();
  const double dx = point.x - center.x;
  const double dy = point.y - center.y;
  move(dx, dy);
}

void kozhin::CompositeShape::scale(double rate)
{
  if (rate <= 0)
  {
    throw std::invalid_argument("Scale rate must be positive.");
  }
  const kozhin::point_t center = getCenter();
  for (size_t i = 0; i < count_; ++i)
  {
    const kozhin::point_t shapeCenter = shapes_[i]->getCenter();
    shapes_[i]->move((shapeCenter.x - center.x) * (rate - 1), (shapeCenter.y - center.y) * (rate - 1));
    shapes_[i]->scale(rate);
  }
}

void kozhin::CompositeShape::rotate(double angle)
{
  const kozhin::point_t center = getCenter();
  const double sinAngle = std::sin(angle * M_PI / 180);
  const double cosAngle = std::cos(angle * M_PI / 180);
  for (size_t i = 0; i < count_; ++i)
  {
    const kozhin::point_t shapeCenter = shapes_[i]->getCenter();
    const double dx = shapeCenter.x - center.x;
    const double dy = shapeCenter.y - center.y;
    shapes_[i]->move(dx * (cosAngle - 1) - dy * sinAngle, dx * sinAngle + dy * (cosAngle - 1));
    shapes_[i]->rotate(angle);
  }
}
