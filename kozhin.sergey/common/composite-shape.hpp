#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP
#include "shape.hpp"

namespace kozhin
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape&);
    CompositeShape(CompositeShape&&) noexcept;
    ~CompositeShape() = default;

    CompositeShape& operator=(const CompositeShape&);
    CompositeShape& operator=(CompositeShape&&) noexcept;
    Shape::ptr operator[](size_t) const;

    void add(Shape::ptr);
    void remove(size_t);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    point_t getCenter() const override;
    size_t size() const;
    void move(double, double) override;
    void move(const point_t&) override;
    void scale(double) override;
    void rotate(double) override;

  private:
    size_t count_;
    size_t size_;
    Shape::array shapes_;
  };

} // namespace kozhin

#endif // COMPOSITESHAPE_HPP
