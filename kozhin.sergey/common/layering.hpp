#ifndef LAYERING_HPP
#define LAYERING_HPP
#include "matrix.hpp"
#include "composite-shape.hpp"

namespace kozhin
{
  bool intersect(const Shape&, const Shape&);
  Matrix<Shape> layer(const CompositeShape&);
} // namespace kozhin

#endif // LAYERING_HPP
