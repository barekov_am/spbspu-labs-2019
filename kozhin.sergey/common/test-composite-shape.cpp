#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

const double DIFF = 0.001;

BOOST_AUTO_TEST_SUITE(compositeShapeTestSuite)

BOOST_AUTO_TEST_CASE(compositeCopyConstrucor)
{
  kozhin::CompositeShape testComposite;
  testComposite.add(std::make_shared<kozhin::Circle>(kozhin::point_t{5, -6}, 4));
  testComposite.add(std::make_shared<kozhin::Rectangle>(kozhin::point_t{-10, 6}, 3, 7));

  kozhin::CompositeShape newComposite(testComposite);

  const kozhin::rectangle_t initialRect = testComposite.getFrameRect();
  const kozhin::rectangle_t newRect = newComposite.getFrameRect();

  BOOST_CHECK_CLOSE(initialRect.width, newRect.width, DIFF);
  BOOST_CHECK_CLOSE(initialRect.height, newRect.height, DIFF);
  BOOST_CHECK_CLOSE(initialRect.pos.x, newRect.pos.x, DIFF);
  BOOST_CHECK_CLOSE(initialRect.pos.y, newRect.pos.y, DIFF);
  BOOST_CHECK_CLOSE(testComposite.getArea(), newComposite.getArea(), DIFF);
  BOOST_CHECK_EQUAL(testComposite.size(), newComposite.size());
}

BOOST_AUTO_TEST_CASE(compositeMoveConstrucor)
{
  kozhin::CompositeShape testComposite;
  testComposite.add(std::make_shared<kozhin::Circle>(kozhin::point_t{5, -6}, 4));
  testComposite.add(std::make_shared<kozhin::Rectangle>(kozhin::point_t{-10, 6}, 3, 7));

  const kozhin::rectangle_t initialRect = testComposite.getFrameRect();
  const double initialArea = testComposite.getArea();
  const size_t initialSize = testComposite.size();

  kozhin::CompositeShape newComposite(std::move(testComposite));
  const kozhin::rectangle_t newRect = newComposite.getFrameRect();

  BOOST_CHECK_CLOSE(initialRect.width, newRect.width, DIFF);
  BOOST_CHECK_CLOSE(initialRect.height, newRect.height, DIFF);
  BOOST_CHECK_CLOSE(initialRect.pos.x, newRect.pos.x, DIFF);
  BOOST_CHECK_CLOSE(initialRect.pos.y, newRect.pos.y, DIFF);
  BOOST_CHECK_CLOSE(initialArea, newComposite.getArea(), DIFF);
  BOOST_CHECK_EQUAL(initialSize, newComposite.size());

  BOOST_CHECK_THROW(testComposite.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(testComposite.getArea(), std::logic_error);
  BOOST_CHECK_EQUAL(testComposite.size(), 0);
}

BOOST_AUTO_TEST_CASE(compositeCopyOperator)
{
  kozhin::CompositeShape testComposite;
  testComposite.add(std::make_shared<kozhin::Circle>(kozhin::point_t{5, -6}, 4));
  testComposite.add(std::make_shared<kozhin::Rectangle>(kozhin::point_t{-10, 6}, 3, 7));

  kozhin::CompositeShape newComposite = testComposite;

  const kozhin::rectangle_t initialRect = testComposite.getFrameRect();
  const kozhin::rectangle_t newRect = newComposite.getFrameRect();

  BOOST_CHECK_CLOSE(initialRect.width, newRect.width, DIFF);
  BOOST_CHECK_CLOSE(initialRect.height, newRect.height, DIFF);
  BOOST_CHECK_CLOSE(initialRect.pos.x, newRect.pos.x, DIFF);
  BOOST_CHECK_CLOSE(initialRect.pos.y, newRect.pos.y, DIFF);
  BOOST_CHECK_CLOSE(testComposite.getArea(), newComposite.getArea(), DIFF);
  BOOST_CHECK_EQUAL(testComposite.size(), newComposite.size());
}

BOOST_AUTO_TEST_CASE(compositeMoveOperator)
{
  kozhin::CompositeShape testComposite;
  testComposite.add(std::make_shared<kozhin::Circle>(kozhin::point_t{5, -6}, 4));
  testComposite.add(std::make_shared<kozhin::Rectangle>(kozhin::point_t{-10, 6}, 3, 7));

  const kozhin::rectangle_t initialRect = testComposite.getFrameRect();
  const double initialArea = testComposite.getArea();
  const size_t initialSize = testComposite.size();

  kozhin::CompositeShape newComposite = std::move(testComposite);
  const kozhin::rectangle_t newRect = newComposite.getFrameRect();

  BOOST_CHECK_CLOSE(initialRect.width, newRect.width, DIFF);
  BOOST_CHECK_CLOSE(initialRect.height, newRect.height, DIFF);
  BOOST_CHECK_CLOSE(initialRect.pos.x, newRect.pos.x, DIFF);
  BOOST_CHECK_CLOSE(initialRect.pos.y, newRect.pos.y, DIFF);
  BOOST_CHECK_CLOSE(initialArea, newComposite.getArea(), DIFF);
  BOOST_CHECK_EQUAL(initialSize, newComposite.size());

  BOOST_CHECK_THROW(testComposite.getFrameRect(), std::logic_error);
  BOOST_CHECK_THROW(testComposite.getArea(), std::logic_error);
  BOOST_CHECK_EQUAL(testComposite.size(), 0);
}

BOOST_AUTO_TEST_CASE(sameParamsAfterMovingShapeRelatively)
{
  kozhin::CompositeShape testComposite;
  testComposite.add(std::make_shared<kozhin::Circle>(kozhin::point_t{5, -6}, 4));
  testComposite.add(std::make_shared<kozhin::Rectangle>(kozhin::point_t{-10, 6}, 3, 7));
  const double initialArea = testComposite.getArea();
  const kozhin::rectangle_t initialFrameRect = testComposite.getFrameRect();

  testComposite.move(3, -5);
  const kozhin::rectangle_t newFrameRect = testComposite.getFrameRect();
  BOOST_CHECK_CLOSE(newFrameRect.width, initialFrameRect.width, DIFF);
  BOOST_CHECK_CLOSE(newFrameRect.height, initialFrameRect.height, DIFF);
  BOOST_CHECK_CLOSE(testComposite.getArea(), initialArea, DIFF);
}

BOOST_AUTO_TEST_CASE(sameParamsAfterMovingShapeAbsolutely)
{
  kozhin::CompositeShape testComposite;
  testComposite.add(std::make_shared<kozhin::Circle>(kozhin::point_t{5, -6}, 4));
  testComposite.add(std::make_shared<kozhin::Rectangle>(kozhin::point_t{-10, 6}, 3, 7));

  const double initialArea = testComposite.getArea();
  const kozhin::rectangle_t initialFrameRect = testComposite.getFrameRect();

  testComposite.move({-5, 3});
  const kozhin::rectangle_t newFrameRect = testComposite.getFrameRect();
  BOOST_CHECK_CLOSE(newFrameRect.width, initialFrameRect.width, DIFF);
  BOOST_CHECK_CLOSE(newFrameRect.height, initialFrameRect.height, DIFF);
  BOOST_CHECK_CLOSE(testComposite.getArea(), initialArea, DIFF);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterShapeScaling)
{
  kozhin::CompositeShape testComposite;
  testComposite.add(std::make_shared<kozhin::Circle>(kozhin::point_t{-7, 8}, 3));
  testComposite.add(std::make_shared<kozhin::Rectangle>(kozhin::point_t{8, -3}, 3, 7));

  const double scaleRate = 3.2;
  const double initialArea = testComposite.getArea();

  testComposite.scale(scaleRate);
  BOOST_CHECK_CLOSE(testComposite.getArea(), initialArea * scaleRate * scaleRate, DIFF);
}

BOOST_AUTO_TEST_CASE(frameRectChangeAfterShapeScaling)
{
  kozhin::CompositeShape testComposite;
  testComposite.add(std::make_shared<kozhin::Circle>(kozhin::point_t{-7, 8}, 3));
  testComposite.add(std::make_shared<kozhin::Rectangle>(kozhin::point_t{8, -3}, 3, 7));

  const double scaleRate = 3.2;
  const kozhin::rectangle_t initialFrame = testComposite.getFrameRect();

  testComposite.scale(scaleRate);

  const kozhin::rectangle_t newFrame = testComposite.getFrameRect();
  BOOST_CHECK_CLOSE(initialFrame.width * scaleRate, newFrame.width, DIFF);
  BOOST_CHECK_CLOSE(initialFrame.height * scaleRate, newFrame.height, DIFF);
  BOOST_CHECK_CLOSE(initialFrame.pos.x, newFrame.pos.x, DIFF);
  BOOST_CHECK_CLOSE(initialFrame.pos.y, newFrame.pos.y, DIFF);
}

BOOST_AUTO_TEST_CASE(paramsChangeAfterCompositeRotation)
{
  kozhin::CompositeShape testComposite;
  testComposite.add(std::make_shared<kozhin::Circle>(kozhin::point_t{-7, 8}, 3));
  testComposite.add(std::make_shared<kozhin::Rectangle>(kozhin::point_t{8, -3}, 3, 7));

  kozhin::rectangle_t initialRect = testComposite.getFrameRect();
  testComposite.rotate(90);
  kozhin::rectangle_t newRect = testComposite.getFrameRect();

  BOOST_CHECK_CLOSE(initialRect.width, newRect.height, DIFF);
  BOOST_CHECK_CLOSE(initialRect.height, newRect.width, DIFF);
  BOOST_CHECK_CLOSE(initialRect.pos.x, newRect.pos.x, DIFF);
  BOOST_CHECK_CLOSE(initialRect.pos.y, newRect.pos.y, DIFF);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterAdding)
{
  kozhin::CompositeShape testComposite;
  testComposite.add(std::make_shared<kozhin::Circle>(kozhin::point_t{10, -5}, 5));
  const double initialArea = testComposite.getArea();

  kozhin::Shape::ptr testRectangle = std::make_shared<kozhin::Rectangle>(kozhin::point_t{-7, 6}, 5, 4);
  testComposite.add(testRectangle);
  const double newArea = testComposite.getArea();

  BOOST_CHECK_CLOSE(initialArea + testRectangle->getArea(), newArea, DIFF);
}

BOOST_AUTO_TEST_CASE(areaChangeAfterDeleting)
{
  kozhin::Shape::ptr testCircle = std::make_shared<kozhin::Circle>(kozhin::point_t{10, -5}, 5);
  kozhin::CompositeShape testComposite;
  testComposite.add(testCircle);
  testComposite.add(std::make_shared<kozhin::Rectangle>(kozhin::point_t{-7, 6}, 5, 4));
  const double initialArea = testComposite.getArea();

  testComposite.remove(0);
  const double newArea = testComposite.getArea();

  BOOST_CHECK_CLOSE(initialArea - testCircle->getArea(), newArea, DIFF);
}

BOOST_AUTO_TEST_CASE(movingEmptyShapeHandling)
{
  kozhin::CompositeShape testComposite;

  BOOST_CHECK_THROW(testComposite.move(3, 5), std::logic_error);
  BOOST_CHECK_THROW(testComposite.move({5, 6}), std::logic_error);
}

BOOST_AUTO_TEST_CASE(gettingEmptyShapeAreaHandling)
{
  kozhin::CompositeShape testComposite;

  BOOST_CHECK_THROW(testComposite.getArea(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(gettingEmptyShapeFrameHandling)
{
  kozhin::CompositeShape testComposite;

  BOOST_CHECK_THROW(testComposite.getFrameRect(), std::logic_error);
}

BOOST_AUTO_TEST_CASE(nonPositiveScaleRateHandling)
{
  kozhin::CompositeShape testComposite;
  testComposite.add(std::make_shared<kozhin::Circle>(kozhin::point_t{5, -6}, 4));
  testComposite.add(std::make_shared<kozhin::Rectangle>(kozhin::point_t{-10, 6}, 3, 7));

  BOOST_CHECK_THROW(testComposite.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(accesingOutOfRangeHandling)
{
  kozhin::CompositeShape testComposite;
  testComposite.add(std::make_shared<kozhin::Circle>(kozhin::point_t{5, -6}, 4));
  testComposite.add(std::make_shared<kozhin::Rectangle>(kozhin::point_t{-10, 6}, 3, 7));

  BOOST_CHECK_THROW(testComposite[-1], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(addingNullptrHandling)
{
  kozhin::CompositeShape testComposite;
  BOOST_CHECK_THROW(testComposite.add(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
