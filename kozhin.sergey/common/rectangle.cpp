#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

kozhin::Rectangle::Rectangle(const kozhin::point_t& center, double width, double height) :
  center_(center),
  width_(width),
  height_(height),
  angle_(0)
{
  if (width_ <= 0.0)
  {
    throw std::invalid_argument("Rectangle width must be positive.");
  }
  if (height_ <= 0.0)
  {
    throw std::invalid_argument("Rectangle height must be positive.");
  }
}

kozhin::Rectangle::Rectangle(const kozhin::point_t& center, double width, double height, double angle) :
  Rectangle(center, width, height)
{
  rotate(angle);
}

double kozhin::Rectangle::getArea() const
{
  return width_ * height_;
}

kozhin::rectangle_t kozhin::Rectangle::getFrameRect() const
{
  const double sinAngle = std::sin(angle_ * M_PI / 180);
  const double cosAngle = std::cos(angle_ * M_PI / 180);
  const double rectWidth = std::abs(width_ * cosAngle) + std::abs(height_ * sinAngle);
  const double rectHeight = std::abs(width_ * sinAngle) + std::abs(height_ * cosAngle);

  return {rectWidth, rectHeight, center_};
}

kozhin::point_t kozhin::Rectangle::getCenter() const
{
  return center_;
}

void kozhin::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void kozhin::Rectangle::move(const kozhin::point_t& point)
{
  center_ = point;
}

void kozhin::Rectangle::scale(double rate)
{
  if (rate <= 0.0)
  {
    throw std::invalid_argument("Scale rate must be positive.");
  }
  width_ *= rate;
  height_ *= rate;
}

void kozhin::Rectangle::rotate(double angle)
{
  angle_ += angle;
}
