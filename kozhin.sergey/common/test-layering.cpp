#include <stdexcept>
#include <boost/test/auto_unit_test.hpp>
#include "layering.hpp"
#include "rectangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(layeringTestSuite)

BOOST_AUTO_TEST_CASE(emptyCompositeLayering)
{
  kozhin::Matrix<kozhin::Shape> testMatrix = kozhin::layer(kozhin::CompositeShape());

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 0);
}

BOOST_AUTO_TEST_CASE(correctCompositeLayering)
{
  kozhin::CompositeShape testComposite;
  testComposite.add(std::make_shared<kozhin::Rectangle>(kozhin::point_t{0, 0}, 6, 4));
  testComposite.add(std::make_shared<kozhin::Circle>(kozhin::point_t{4, 3}, 3));
  testComposite.add(std::make_shared<kozhin::Rectangle>(kozhin::point_t{-5, 4}, 2, 1));

  kozhin::Matrix<kozhin::Shape> testMatrix = kozhin::layer(testComposite);

  BOOST_CHECK_EQUAL(testMatrix.getRows(), 2);
  BOOST_CHECK_EQUAL(testMatrix[0].size(), 2);
  BOOST_CHECK_EQUAL(testMatrix[1].size(), 1);
  BOOST_CHECK_EQUAL(testMatrix[0][0], testComposite[0]);
  BOOST_CHECK_EQUAL(testMatrix[0][1], testComposite[2]);
  BOOST_CHECK_EQUAL(testMatrix[1][0], testComposite[1]);
}

BOOST_AUTO_TEST_CASE(intercetionTseting)
{
  kozhin::Rectangle testRect({0, 0}, 6, 4);
  kozhin::Circle testCircle1({-4, 0}, 3);
  kozhin::Circle testCircle2({0, 4}, 3);
  kozhin::Circle testCircle3({0, -4}, 3);
  kozhin::Circle testCircle4({4, 0}, 3);
  kozhin::Circle testCircle5({10, 10}, 1);

  BOOST_CHECK(kozhin::intersect(testRect, testCircle1));
  BOOST_CHECK(kozhin::intersect(testRect, testCircle2));
  BOOST_CHECK(kozhin::intersect(testRect, testCircle3));
  BOOST_CHECK(kozhin::intersect(testRect, testCircle4));

  BOOST_CHECK(!kozhin::intersect(testRect, testCircle5));
}


BOOST_AUTO_TEST_SUITE_END()
