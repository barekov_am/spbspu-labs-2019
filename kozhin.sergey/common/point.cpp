#include "point.hpp"
#include "blank.hpp"

int readCoordinate(std::istream& is, char delim)
{
  if ((is >> blank).peek() == '\n')
  {
    throw std::ios_base::failure("No coordinate");
  }

  int coord;
  if (!(is >> coord))
  {
    throw std::ios_base::failure("No coordinate");
  }

  if ((is >> blank).get() != delim)
  {
    throw std::ios_base::failure("Incorrect coordinate");
  }

  return coord;
}

std::istream& operator>>(std::istream& is, Point& point)
{
  std::istream::sentry sentry(is);

  if (sentry)
  {
    if (is.get() != '\(')
    {
      is.clear(std::ios::failbit);
      return is;
    }

    int x;
    int y;
    try
    {
      x = readCoordinate(is, ';');
      y = readCoordinate(is, ')');
    }
    catch (std::ios_base::failure& e)
    {
      is.clear(std::ios::failbit);
      return is;
    }

    point = {x, y};
  }

  return is;
}

std::ostream& operator<<(std::ostream& os, const Point& point)
{
  os << '(' << point.x << ';' << point.y << ')';
  return os;
}
