#ifndef ACCESSORS_HPP
#define ACCESSORS_HPP

#include <cstddef>

namespace pashchenko
{
  namespace accessors
  {
    template<typename Container>
    struct AtMethodAccessor
    {
      typedef typename Container::reference containerRef;

      static size_t getBegin(const Container&)
      {
        return 0;
      }
      
      static size_t getEnd(const Container& container)
      {
        return container.size();
      }
      
      static containerRef getElement(Container& container, size_t i)
      {
        return container.at(i);
      }
      
    };

    template<typename Container>
    struct BracketsAccessor
    {
      typedef typename Container::reference containerRef;

      static size_t getBegin(const Container&)
      {
        return 0;
      }

      static size_t getEnd(const Container& container)
      {
        return container.size();
      }

      static containerRef getElement(Container& container, size_t i)
      {
        return container[i];
      }
    };

    template<typename Container>
    struct IteratorAccessor
    {
      typedef typename Container::reference containerRef;
      typedef typename Container::iterator containerIter;

      static containerIter getBegin(Container& container)
      {
        return container.begin();
      }

      static containerIter getEnd(Container& container)
      {
        return container.end();
      }

      static containerRef getElement(const Container&, containerIter& iter)
      {
        return *iter;
      }
    };
  }
}

#endif // !ACCESSORS_HPP
