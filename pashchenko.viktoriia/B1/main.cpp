#include <iostream>
#include <stdexcept>
#include <cstring>

#include "tasks.hpp"

int main(int argc, char* argv[])
{
  try
  {
    if (argc < 2)
    {
      std::cerr << "Wrong arguments count" << std::endl;

      return 1;
    }

    int taskNum;
    try
    {
      taskNum = std::atoi(argv[1]);
    }
    catch(std::invalid_argument & e)
    {
      std::cerr << "Invalid task number argument passed";
      std::cerr << "Should be an integer in between 1 and 4" << std::endl;

      return 2;
    }
    catch(std::out_of_range & e)
    {
      std::cerr << "Task number argument is out of range" << std::endl;

      return 3;
    }

    if (taskNum < 1 || taskNum > 4)
    {
      std::cerr << "Invalid task number: should be in between 1 and 4" << std::endl;
      
      return 4;
    }

    bool isAscending;

    switch(taskNum)
    {
    case 1:
      if (argc != 3)
      {
        std::cerr << "Wrong arguments count for selected task" << std::endl;

        return 5;
      }

      if (argv[2] == nullptr)
      {
        std::cerr << "Sorting order wasn't provided. Try pass either 'ascending' or 'descending'" << std::endl;

        return 6;
      }

      if (!strcmp("ascending", argv[2]))
      {
        isAscending = true;
      }
      else if (!strcmp("descending", argv[2]))
      {
        isAscending = false;
      }
      else
      {
        std::cerr << "Wrong sorting order provided. Should be either 'ascending' or 'descending'" << std::endl;
      
        return 7;
      }

      pashchenko::taskFirst(isAscending);
      break;

    case 2:
      if (argc != 3)
      {
        std::cerr << "Wrong arguments count for selected test" << std::endl;

        return 5;
      }

      pashchenko::taskSecond(argv[2]);
      break;

    case 3:
      if (argc != 2)
      {
        std::cerr << "Wrong arguments count for selected test" << std::endl;

        return 5;
      }

      pashchenko::taskThird();
      break;

    case 4:
      if (argc != 4)
      {
        std::cerr << "Wrong arguments count for selected test" << std::endl;

        return 5;
      }

      if (argv[2] == nullptr)
      {
        std::cerr << "Sorting order wasn't provided. Try pass either 'ascending' or 'descending'" << std::endl;

        return 6;
      }

      if (!strcmp("ascending", argv[2]))
      {
        isAscending = true;
      }
      else if (!strcmp("descending", argv[2]))
      {
        isAscending = false;
      }
      else
      {
        std::cerr << "Wrong sorting order provided. Should be either 'ascending' or 'descending'" << std::endl;

        return 7;
      }

      int size;
      try
      {
        size = std::atoi(argv[3]);
      }
      catch(std::invalid_argument & e)
      {
        std::cerr << "Invalid task number argument passed";
        std::cerr << "Should be an integer in between 1 and 4" << std::endl;

        return 2;
      }
      catch(std::out_of_range & e)
      {
        std::cerr << "Task number argument is out of range" << std::endl;

        return 3;
      }
      
      if (size == 0)
      {
        std::cerr << "Size of the array is given incorrectly" << std::endl;
        
        return 4;
      }

      pashchenko::taskFourth(isAscending, size);
      break;

    default:
      std::cerr << "Wrong task number argument";

      return 1;
    }

  }
  catch (std::exception & e)
  {
    std::cerr << e.what() << std::endl;
    return 10;
  }

  return 0;
}
