#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <functional>
#include <iosfwd>
#include <utility>
#include <vector>
#include <fstream>

namespace pashchenko
{
  void fillDoubleRandomArray(double* array, unsigned int size);

  namespace functions
  {
    template< template<typename Container> class Accessor,
      typename Container, typename Comparator >
    void sort(Container & cont, const Comparator & comparator)
    {
      typedef Accessor<Container> accessor;
      
      for (auto iter = accessor::getBegin(cont); iter != accessor::getEnd(cont); ++iter)
      {
        for (auto iter2 = iter; iter2 != accessor::getEnd(cont); ++iter2)
        {
          if (comparator(accessor::getElement(cont, iter), accessor::getElement(cont, iter2)))
          {
            std::swap(accessor::getElement(cont, iter), accessor::getElement(cont, iter2));  
          }
        }
      }
    }

    template<typename Container, typename Char>
    void printValues(std::basic_ostream<Char>& ostr, const Container& cont,
        const char & delimeter, bool newline)
    {
      for (typename Container::const_reference value : cont)
      {
        ostr << value << delimeter;
      }
      if (newline)
      {
        ostr << '\n';
      }
    }

    template<template<typename> class Accessor, typename Container>
    void selectSort(Container& cont, bool ascending)
    {
      if (ascending)
      {
        sort<Accessor>(cont, std::greater<typename Container::value_type>());
      }
      else
      {
        sort<Accessor>(cont, std::less<typename Container::value_type>());
      }
    }

    template<typename T, typename Char>
    std::vector<T> readValues(std::basic_istream<Char>& istr)
    {
      std::vector<T> input;
      std::istream::sentry inputSentry(istr, true);
      if (!inputSentry)
      {
        throw std::istream::failure("readValues: stream is closed");
      }

      T value;
      while(true)
      {
        istr >> value;
        if (!istr && !istr.eof())
        {
          throw std::istream::failure("readValues: can't read value");
        }
        else if (istr.eof())
        {
          break;
        }

        input.push_back(value);
      }

      return input;
    }
  }
}

#endif // !FUNCTIONS_HPP
