#include "functions.hpp"
#include <random>
#include <time.h>

void pashchenko::fillDoubleRandomArray(double* array, unsigned int size)
{
  std::mt19937 generator(time(0));
  std::uniform_real_distribution<double> distr(-1.0, 1.0);

  for (unsigned int i = 0; i < size; ++i)
  {
    array[i] = distr(generator);
  }
}
