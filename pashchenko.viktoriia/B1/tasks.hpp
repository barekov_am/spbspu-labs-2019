#ifndef TASKS_HPP
#define TASKS_HPP

namespace pashchenko
{
  void taskFirst(bool ascending);
  void taskSecond(const char* const filename);
  void taskThird();
  void taskFourth(bool ascending, int size);
}

#endif // !TASKS_HPP
