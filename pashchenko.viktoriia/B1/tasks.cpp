#include <iostream>
#include <fstream>
#include <vector>
#include <forward_list>
#include <string>
#include <cstring>
#include <memory>
#include <iomanip>

#include "tasks.hpp"
#include "accessors.hpp"
#include "functions.hpp"

void pashchenko::taskFirst(bool ascending)
{
  std::vector<int> values = pashchenko::functions::readValues<int>(std::cin);

  std::vector<int> tmp = values;
  pashchenko::functions::selectSort<pashchenko::accessors::BracketsAccessor>(tmp, ascending);
  pashchenko::functions::printValues(std::cout, tmp, ' ', true);
  
  tmp = values;
  pashchenko::functions::selectSort<pashchenko::accessors::AtMethodAccessor>(tmp, ascending);
  pashchenko::functions::printValues(std::cout, tmp, ' ', true);

  std::forward_list<int> list(values.begin(), values.end());
  pashchenko::functions::selectSort<pashchenko::accessors::IteratorAccessor>(tmp, ascending);
  pashchenko::functions::printValues(std::cout, tmp, ' ', true);
}

void pashchenko::taskSecond(const char* const filename)
{
  std::ifstream currFile(filename);
  if (!currFile.good())
  {
    throw std::istream::failure(std::string("Invalid input file ") + filename);
  }
  
  if (!currFile)
  {
    throw std::istream::failure(std::string("Can't read file ") + filename);
  }

  unsigned int count = 0;
  unsigned int length = 4;

  std::unique_ptr<char, decltype(std::free)*> tempSmartPtr(static_cast<char*>(std::malloc(length * sizeof(char))), std::free);
  
  while (currFile)
  {
    currFile.read(tempSmartPtr.get() + count, length - count);
    count += currFile.gcount();
    
    if (count == length)
    {
      length = length * 2;

      char* tempPtr = static_cast<char *>(std::realloc(tempSmartPtr.get(), length*sizeof(char)));
      tempSmartPtr.release();
      tempSmartPtr.reset(tempPtr);
    }
  }
  currFile.close();
  
  std::vector<char> vec(tempSmartPtr.get(), tempSmartPtr.get() + count);
  
  for (auto i = vec.begin(); i != vec.end(); i++)
  {
    std::cout << *i;
  }
}

void pashchenko::taskThird()
{
  std::istream::sentry sentry(std::cin);
  if (!sentry && !std::cin.eof())
  {
    throw std::istream::failure("Std::cin is closed");
  }
  else if (std::cin.eof())
  {
    return;
  }

  std::vector<int> values;
  int value;

  while (true)
  {
    std::cin >> value;
    if (!std::cin && !std::cin.eof())
    {
      throw std::istream::failure("Can't read value from std::cin");
    }
    else if (value == 0)
    {
      break;
    }
    else if (std::cin.eof())
    {
      throw std::istream::failure("Not found 0 at the end");
    }
    
    values.push_back(value);
  }

  if (values.empty())
  {
    return;
  }

  if (values.back() == 1)
  {
    for (auto i = values.begin(); i != values.end();)
    {
      if (*i % 2 == 0)
      {
        i = values.erase(i);
      }
      else
      {
        ++i;
      }
    }
  }
  else if (values.back() == 2)
  {
    for (auto i = values.begin(); i != values.end();)
    {
      if (*i % 3 == 0)
      {
        i = values.insert(i + 1, 3, 1);
        i += 2;
      }
      else
      {
        ++i;
      }
    }
  }

  pashchenko::functions::printValues(std::cout, values, ' ', true);
}

void pashchenko::taskFourth(bool ascending, int size)
{
  std::vector<double> vector(size);
  pashchenko::fillDoubleRandomArray(&vector[0], vector.size());

  std::cout << std::setprecision(2) << std::fixed;
  pashchenko::functions::printValues(std::cout, vector, ' ', true);

  pashchenko::functions::selectSort<pashchenko::accessors::BracketsAccessor>(vector, ascending);
  pashchenko::functions::printValues(std::cout, vector, ' ', true);
}
