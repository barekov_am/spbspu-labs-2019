#ifndef TASKS_HPP
#define TASKS_HPP

namespace pashchenko
{
  void taskFirst();
  void taskSecond();
}

#endif // !TASKS_HPP
