#ifndef USER_INTERFACE_HPP
#define USER_INTERFACE_HPP

#include "phoneBook.hpp"

namespace pashchenko
{
  PhoneBookRecord readRecord(std::istream& inputReader);
  std::string readBookmark(std::istream& inputReader);
  std::string readSteps(std::istream& inputReader);

  inline void validateInputStream(std::istream& in)
  {
    if (!in.eof() && in.fail())
    {
      throw std::ios::failure("An error occurred while reading from the input stream");
    }
  }

  inline void insertRecord(std::istream& inputReader, PhoneBook& phoneBook)
  {
    std::string insertType;
    inputReader >> insertType >> std::ws;
    validateInputStream(inputReader);
    std::string bookmark = readBookmark(inputReader);
    PhoneBookRecord record = readRecord(inputReader);

    if (insertType == "before")
    {
      phoneBook.insertBefore(bookmark, record);
    }
    else if (insertType == "after")
    {
      phoneBook.insertAfter(bookmark, record);
    }
    else
    {
      throw std::invalid_argument("<INVALID COMMAND>");
    }
  }

  inline void showBookmark(std::istream& inputReader, PhoneBook& phoneBook)
  {
    PhoneBookRecord record = phoneBook.getRecord(readBookmark(inputReader));
    std::cout << record.number << ' ' << record.name << std::endl;
  }

  inline void executeInstruction(std::istream& inputReader, PhoneBook& phoneBook, const std::string& instruction)
  {
    if (instruction == "add")
    {
      phoneBook.addRecord(readRecord(inputReader));
    }
    else if (instruction == "store")
    {
      std::string bookmarkLast = readBookmark(inputReader);
      std::string bookmarkNew = readBookmark(inputReader);
      phoneBook.storeBookmark(bookmarkLast, bookmarkNew);
    }
    else if (instruction == "insert")
    {
      insertRecord(inputReader, phoneBook);
    }
    else if (instruction == "delete")
    {
      phoneBook.removeRecord(readBookmark(inputReader));
    }
    else if (instruction == "show")
    {
      showBookmark(inputReader, phoneBook);
    }
    else if (instruction == "move")
    {
      std::string bookmark = readBookmark(inputReader);
      std::string steps = readSteps(inputReader);
      phoneBook.moveRecord(bookmark, steps);
    }
    else if (!instruction.empty())
    {
      throw std::invalid_argument("INVALID COMMAND");
    }
  }
}

#endif // !USER_INTERFACE_HPP
