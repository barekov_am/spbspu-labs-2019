#include <algorithm>
#include <iostream>
#include <sstream>

#include "tasks.hpp"
#include "factorials.hpp"

void pashchenko::taskSecond()
{
  Factorials factorials;

  std::copy(factorials.begin(), factorials.end(), std::ostream_iterator<unsigned long>(std::cout, " "));
  std::cout << std::endl;

  std::reverse_copy(factorials.begin(), factorials.end(), std::ostream_iterator<unsigned long>(std::cout, " "));
  std::cout << std::endl;
}
