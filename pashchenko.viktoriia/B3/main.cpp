#include <iostream>
#include <sstream>

#include "tasks.hpp"

int main(int argc, char* argv[])
{
  try {
    if (argc != 2)
    {
      throw std::invalid_argument("Invalid number of input parameters");
    }

    int taskNum = std::stoi(argv[1]);
    switch (taskNum)
    {
      case 1:
        pashchenko::taskFirst();
        break;

      case 2:
        pashchenko::taskSecond();
        break;

      default:
        throw std::invalid_argument("Invalid task number");
    }
  }
  catch (std::exception & err)
  {
    std::cerr << err.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cerr << "Something was wrong" << std::endl;
    return 2;
  }
  
  return 0;
}
