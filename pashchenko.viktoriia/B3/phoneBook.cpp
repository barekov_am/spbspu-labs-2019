#include "phoneBook.hpp"

pashchenko::PhoneBook::PhoneBook()
{
  bookmarks_["current"] = records_.end();
}

void pashchenko::PhoneBook::addRecord(const PhoneBookRecord& record)
{
  if (!insertOnInvalidBookmark("current", record))
  {
    records_.push_back(record);
  }
}

void pashchenko::PhoneBook::storeBookmark(const std::string& bookmark, const std::string& newBookmark)
{
  validateBookmarkEntry(bookmark);
  bookmarks_[newBookmark] = bookmarks_[bookmark];
}

void pashchenko::PhoneBook::insertBefore(const std::string& bookmark, const PhoneBookRecord& record)
{
  validateBookmarkEntry(bookmark);

  if (!insertOnInvalidBookmark(bookmark, record))
  {
    records_.insert(bookmarks_[bookmark], record);
  }
}

void pashchenko::PhoneBook::insertAfter(const std::string& bookmark, const PhoneBookRecord& record)
{
  validateBookmarkEntry(bookmark);

  if (!insertOnInvalidBookmark(bookmark, record))
  {
    records_.insert(std::next(bookmarks_[bookmark]), record);
  }
}

void pashchenko::PhoneBook::removeRecord(const std::string& bookmark)
{
  validateBookmarkEntry(bookmark);

  for (auto& item : bookmarks_)
  {
    if ((item.second == bookmarks_[bookmark]) && (item.first != bookmark))
    {
      ++(item.second);
    }
  }
  
  records_.erase(bookmarks_[bookmark]++);
}

void pashchenko::PhoneBook::moveRecord(const std::string& bookmark, const std::string& steps)
{
  validateBookmarkEntry(bookmark);
  
  if (steps == "first")
  {
    bookmarks_[bookmark] = records_.begin();
    return;
  }
  
  if (steps == "last")
  {
    bookmarks_[bookmark] = --records_.end();
    return;
  }

  int distance = std::stoi(steps);
  if ((distance < 0) && (std::distance(records_.begin(), bookmarks_[bookmark]) < std::abs(distance)))
  {
    return;
  }
  
  if ((distance > 0) && (std::distance(bookmarks_[bookmark], --records_.end()) < std::abs(distance)))
  {
    return;
  }

  std::advance(bookmarks_[bookmark], distance);
}

pashchenko::PhoneBookRecord pashchenko::PhoneBook::getRecord(const std::string& bookmark)
{
  validateBookmarkEntry(bookmark);
  
  if (records_.empty())
  {
    throw std::invalid_argument("<EMPTY>");
  }
  
  return (bookmarks_[bookmark] != records_.end()) ? *bookmarks_[bookmark] : *--bookmarks_[bookmark];
}

inline void pashchenko::PhoneBook::validateBookmarkEntry(const std::string& bookmark)
{
  if (bookmarks_.find(bookmark) == bookmarks_.end())
  {
    throw std::invalid_argument("<INVALID BOOKMARK>");
  }
}

inline bool pashchenko::PhoneBook::insertOnInvalidBookmark(const std::string& bookmark, const PhoneBookRecord& record)
{
  if (bookmarks_[bookmark] == records_.end())
  {
    records_.push_back(record);
    bookmarks_[bookmark] = --records_.end();
    return true;
  }
  
  return false;
}
