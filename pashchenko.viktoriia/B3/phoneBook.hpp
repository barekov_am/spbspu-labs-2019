#ifndef PHONE_BOOK_HPP
#define PHONE_BOOK_HPP

#include <iostream>
#include <string>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <algorithm>

namespace pashchenko
{
  struct PhoneBookRecord
  {
    std::string name;
    std::string number;
  };
  
  class PhoneBook
  {
  public:
    PhoneBook();
    
    void addRecord(const PhoneBookRecord& record);
    void storeBookmark(const std::string& bookmark, const std::string& newBookmark);
    void insertBefore(const std::string& bookmark, const PhoneBookRecord& record);
    void insertAfter(const std::string& bookmark, const PhoneBookRecord& record);
    void removeRecord(const std::string& bookmark);
    void moveRecord(const std::string& bookmark, const std::string& steps);

    PhoneBookRecord getRecord(const std::string& bookmark);

  private:
    std::list<PhoneBookRecord> records_;
    std::map<std::string, std::list<PhoneBookRecord>::iterator> bookmarks_;

    inline void validateBookmarkEntry(const std::string& bookmark);
    inline bool insertOnInvalidBookmark(const std::string& bookmark, const PhoneBookRecord& record);
  };
}

#endif // !PHONE_BOOK_HPP
