#ifndef FACTORIALS_HPP
#define FACTORIALS_HPP

#include <iterator>

namespace pashchenko
{
  class FactorialsIterator;

  class Factorials
  {
  public:
    FactorialsIterator begin();
    FactorialsIterator end();
  };

  class FactorialsIterator : public std::iterator<std::bidirectional_iterator_tag, unsigned long>
  {
    friend class Factorials;

  public:
    unsigned long operator*() const;
    FactorialsIterator & operator++();
    FactorialsIterator & operator--();

    bool operator!=(const FactorialsIterator& other) const;
    bool operator==(const FactorialsIterator& other) const;

  private:
    int position_;
    FactorialsIterator(int position);
  };
}

#endif // !FACTORIALS_HPP
