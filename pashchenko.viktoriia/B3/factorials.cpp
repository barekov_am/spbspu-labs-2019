#include "factorials.hpp"

pashchenko::FactorialsIterator pashchenko::Factorials::begin()
{
  return { 1 };
}

pashchenko::FactorialsIterator pashchenko::Factorials::end()
{
  return { 11 };
}

unsigned long pashchenko::FactorialsIterator::operator*() const
{
  unsigned long factorial = 1;
  for (int i = 2; i <= position_; ++i)
  {
    factorial *= i;
  }
  
  return factorial;
}

pashchenko::FactorialsIterator & pashchenko::FactorialsIterator::operator++()
{
  position_++;
  return *this;
}

pashchenko::FactorialsIterator & pashchenko::FactorialsIterator::operator--()
{
  position_--;
  return *this;
}

bool pashchenko::FactorialsIterator::operator!=(const FactorialsIterator& other) const
{
  return position_ != other.position_;
}

bool pashchenko::FactorialsIterator::operator==(const FactorialsIterator& other) const
{
  return position_ == other.position_;
}

pashchenko::FactorialsIterator::FactorialsIterator(int position) :
  position_(position)
{
}
