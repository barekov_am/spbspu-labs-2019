#include "userInterface.hpp"

pashchenko::PhoneBookRecord pashchenko::readRecord(std::istream& inputReader)
{
  PhoneBookRecord record;
  inputReader >> record.number >> std::ws;
  getline(inputReader, record.name);
  validateInputStream(inputReader);
  
  if (record.name.empty())
  {
    throw std::invalid_argument("<INVALID COMMAND>");
  }

  if ((*record.name.begin() != '\"')
      || ((record.name.back() != '\"'))
      || (record.name.length() < 3))
  {
    throw std::invalid_argument("<INVALID COMMAND>");
  }
  else
  {
    record.name.erase(record.name.begin());
    record.name.erase(record.name.end() - 1);
  }

  for (size_t pos = record.name.rfind(R"(\")");
       pos != std::string::npos;
       pos = record.name.rfind(R"(\")"))
  {
    record.name.replace(pos, 2, "\"");
  }

  for (char& c : record.number)
  {
    if (!std::isdigit(c))
    {
      throw std::invalid_argument("<INVALID COMMAND>");
    }
  }

  return record;
}

std::string pashchenko::readBookmark(std::istream& inputReader)
{
  std::string bookmark;
  inputReader >> bookmark >> std::ws;
  validateInputStream(inputReader);
  
  if (bookmark.empty())
  {
    throw std::invalid_argument("<INVALID BOOKMARK>");
  }

  for (char& c : bookmark)
  {
    if (!std::isalpha(c) && !std::isdigit(c) && (c != '-'))
    {
      throw std::invalid_argument("<INVALID BOOKMARK>");
    }
  }

  return bookmark;
}

std::string pashchenko::readSteps(std::istream& inputReader)
{
  std::string steps;
  getline(inputReader, steps);
  validateInputStream(inputReader);

  if ((steps == "first") || (steps == "last"))
  {
    return steps;
  }

  for (auto c = steps.begin(); c != steps.end(); c++)
  {
    if (!std::isdigit(*c)
        && ((c != steps.begin()) || ((*c != '-') && (*c != '+'))))
    {
      throw std::invalid_argument("<INVALID STEP>");
    }
  }

  return steps;
}
