#include "tasks.hpp"
#include "userInterface.hpp"

void pashchenko::taskFirst()
{
  std::string inputLine;
  PhoneBook phoneBook;

  while (getline(std::cin, inputLine))
  {
    std::istringstream inputReader(inputLine);
    std::string currInstruction;
    
    inputReader >> currInstruction;
    validateInputStream(inputReader);

    try
    {
      executeInstruction(inputReader, phoneBook, currInstruction);
    }
    catch (std::invalid_argument & e)
    {
      std::cout << e.what() << std::endl;
    }
  }
}
