#ifndef QUEUE_WITH_PRIORITY_HPP
#define QUEUE_WITH_PRIORITY_HPP

#include <iostream>
#include <list>

enum ElementPriority
{
  LOW,
  NORMAL,
  HIGH
};

template<typename T>
struct QueueElement
{
  T value;
  ElementPriority priority;
};

template<typename T>
class QueueWithPriority
{
public:
  void putElementToQueue(const T& element, ElementPriority priority);
  T get();
  void accelerate();
  bool empty();

private:
  typedef typename std::list<QueueElement<T>>::iterator iterator;
  std::list<QueueElement<T>> data_;

  iterator findEqualPriority(ElementPriority priority);
};

template<typename T>
void QueueWithPriority<T>::putElementToQueue(const T& element, ElementPriority priority)
{
  iterator placeForInsert = findEqualPriority(priority);
  data_.insert(placeForInsert, {element, priority});
}

template<typename T>
T QueueWithPriority<T>::get()
{
  if (data_.begin() == data_.end())
  {
    throw std::range_error("<EMPTY>");
  }

  T tmp = std::move(data_.back().value);
  data_.pop_back();
  
  return tmp;
}

template<typename T>
void QueueWithPriority<T>::accelerate()
{
  iterator begOfLowPriority = findEqualPriority(ElementPriority::LOW);
  iterator endOfLowPriority = findEqualPriority(ElementPriority::NORMAL);
  iterator placeForInsert = findEqualPriority(ElementPriority::HIGH);

  for (iterator it = begOfLowPriority; it != endOfLowPriority; ++it)
  {
    it->priority = ElementPriority::HIGH;
  }
  data_.splice(placeForInsert, data_, begOfLowPriority, endOfLowPriority);
}

template<typename T>
bool QueueWithPriority<T>::empty()
{
  return data_.empty();
}

template<typename T>
typename QueueWithPriority<T>::iterator QueueWithPriority<T>::findEqualPriority(ElementPriority priority)
{
  for (iterator it = data_.begin(); it != data_.end(); it++)
  {
    if (priority <= it->priority)
    {
      return it;
    }
  }
  
  return data_.end();
}

#endif // !QUEUE_WITH_PRIORITY_HPP
