#include <string>
#include <sstream>

#include "tasks.hpp"
#include "queueWithPriority.hpp"

namespace pashchenko
{
  template<typename T>
  void addElement(std::istream& in, QueueWithPriority<T>& priorityQueue)
  {
    std::string priority_str;
    in >> priority_str;
    in >> std::ws;
    T elementValue;
    getline(in, elementValue);
    
    if (elementValue.empty())
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
      return;
    }

    pashchenko::validateInputStream(in);

    ElementPriority elementPriority;
    if (priority_str == "low")
    {
      elementPriority = ElementPriority::LOW;
    }
    else if (priority_str == "normal")
    {
      elementPriority = ElementPriority::NORMAL;
    }
    else if (priority_str == "high")
    {
      elementPriority = ElementPriority::HIGH;
    }
    else
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
      return;
    }

    priorityQueue.putElementToQueue(elementValue, elementPriority);
  }
}

void pashchenko::taskFirst()
{
  QueueWithPriority<std::string> priorityQueue;
  std::string inputLine;

  while (getline(std::cin, inputLine))
  {
    std::istringstream keywordReader(inputLine);
    std::string currInstruction;
    keywordReader >> currInstruction;

    pashchenko::validateInputStream(keywordReader);

    if (currInstruction == "add")
    {
      pashchenko::addElement(keywordReader, priorityQueue);
    }
    else if (currInstruction == "get")
    {
      if (priorityQueue.empty())
      {
        std::cout << "<EMPTY>" << std::endl;
      }
      else
      {
        std::cout << priorityQueue.get() << std::endl;
      }
    }
    else if (currInstruction == "accelerate")
    {
      priorityQueue.accelerate();
    }
    else if (!currInstruction.empty())
    {
      std::cout << "<INVALID COMMAND>" << std::endl;
    }
  }
}
