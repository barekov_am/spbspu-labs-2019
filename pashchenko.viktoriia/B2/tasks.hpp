#ifndef TASKS_HPP
#define TASKS_HPP

#include <iostream>

namespace pashchenko
{
  void taskFirst();
  void taskSecond();

  inline void validateInputStream(std::istream& in)
  {
    if (!in.eof() && in.fail())
    {
      throw std::ios::failure("An error occurred while reading from the input stream");
    }
  }
}

#endif // !TASKS_HPP
