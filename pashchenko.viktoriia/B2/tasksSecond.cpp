#include <list>

#include "tasks.hpp"

namespace pashchenko
{
  template<typename It>
  void displayListInStrangeOrder(It first, It second, size_t length)
  {
    if (length == 1)
    {
      std::cout << *first << std::endl;
    }

    if (length == 2)
    {
      std::cout << *first++ << ' ' << *--second << std::endl;
    }

    if (length > 2)
    {
      std::cout << *first++ << ' ' << *--second << ' ';
      displayListInStrangeOrder(first, second, length - 2);
    }
  }
}

void pashchenko::taskSecond()
{
  std::list<int> ints;
  int value = 0;
  size_t listLength = ints.size();
  
  const int minValue = 1;
  const int maxValue = 20;
  const int maxListLength = 20;
  
  while (std::cin >> value)
  {
    if ((value < minValue) || (value > maxValue))
    {
      throw std::out_of_range("Value out of range. Valid range: from 1 to 20");
    }
    
    if (listLength >= maxListLength)
    {
      throw std::length_error("List overflow. Valid length: from 0 to 20");
    }

    ints.push_back(value);
    ++listLength;
  }

  pashchenko::validateInputStream(std::cin);
  pashchenko::displayListInStrangeOrder(ints.begin(), ints.end(), ints.size());
}
